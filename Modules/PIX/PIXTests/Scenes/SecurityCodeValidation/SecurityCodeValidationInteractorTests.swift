import AnalyticsModule
import Core
@testable import PIX
import XCTest

final private class SecurityCodeValidationServicingMock: SecurityCodeValidationServicing {
    var sendConfirmationCodeResult: Result<NoContent, ApiError>?
    var requestNewCodeResult: Result<SecurityCodeRequestResponse, ApiError>?

    // MARK: - sendConfirmationCode
    private(set) var sendConfirmationCodeCallsCount = 0

    func sendConfirmationCode(inputType: SecurityCodeValidationType, codeValue: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        sendConfirmationCodeCallsCount += 1
        guard let result = sendConfirmationCodeResult else {
            XCTFail("Mocked result for sendConfirmationCode method is nil")
            return
        }
        completion(result)
    }

    // MARK: - requestNewCode
    private(set) var requestNewCodeCallsCount = 0

    func requestNewCode(inputType: SecurityCodeValidationType, completion: @escaping (Result<SecurityCodeRequestResponse, ApiError>) -> Void) {
        requestNewCodeCallsCount += 1
        guard let result = requestNewCodeResult else {
            XCTFail("Mocked result for requestNewCode method is nil")
            return
        }
        completion(result)
    }
}

private final class SecurityCodeValidationPresentingSpy: SecurityCodeValidationPresenting {
    var viewController: SecurityCodeValidationDisplaying?

    // MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var action: SecurityCodeValidationAction?

    func didNextStep(action: SecurityCodeValidationAction) {
        didNextStepActionCallsCount += 1
        self.action = action
    }

    // MARK: - populateView
    private(set) var populateViewCallsCount = 0
    private(set) var inputTypePopulateViewCall: SecurityCodeValidationType?
    private(set) var inputedValue: String?

    func populateView(for inputType: SecurityCodeValidationType, inputedValue: String) {
        populateViewCallsCount += 1
        inputTypePopulateViewCall = inputType
        self.inputedValue = inputedValue
    }

    // MARK: - updateResendCodeButton
    private(set) var updateResendCodeButtonCallsCount = 0
    private(set) var butonState: SecurityCodeResendButtonState?
    private(set) var inputTypeUpdateButtoncall: SecurityCodeValidationType?

    func updateResendCodeButton(state: SecurityCodeResendButtonState, inputType: SecurityCodeValidationType) {
        updateResendCodeButtonCallsCount += 1
        butonState = state
        inputTypeUpdateButtoncall = inputType
    }

    // MARK: - presentGeneralError
    private(set) var presentGeneralErrorCallsCount = 0
    private(set) var generalErrorTitle: String?
    private(set) var generalErrorMessage: String?

    func presentGeneralError(title: String, message: String) {
        presentGeneralErrorCallsCount += 1
        generalErrorTitle = title
        generalErrorMessage = message
    }
    
    // MARK: - presentCodeError
    private(set) var presentCodeErrorCallsCount = 0
    private(set) var codeErrorMessage: String?
    
    func presentCodeError(message: String) {
        presentCodeErrorCallsCount += 1
        codeErrorMessage = message
    }

    // MARK: - hideError
    private(set) var hideCodeErrorCallsCount = 0

    func hideCodeError() {
        hideCodeErrorCallsCount += 1
    }

    // MARK: - showLoading
    private(set) var showLoadingCallsCount = 0

    func showLoading() {
        showLoadingCallsCount += 1
    }

    // MARK: - hideLoading
    private(set) var hideLoadingCallsCount = 0

    func hideLoading() {
        hideLoadingCallsCount += 1
    }

    // MARK: - cleanCodeTextField
    private(set) var cleanCodeTextFieldCallsCount = 0

    func cleanCodeTextField() {
        cleanCodeTextFieldCallsCount += 1
    }

    //MARK: - animateResendSuccess
    private(set) var animateResendSuccessCallsCount = 0

    func animateResendSuccess() {
        animateResendSuccessCallsCount += 1
    }
}

final class SecurityCodeValidationInteractorTests: XCTestCase {
    private let serviceMock = SecurityCodeValidationServicingMock()
    private let presenterSpy = SecurityCodeValidationPresentingSpy()
    private let analyticsSpy = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(analyticsSpy)

    private let mockedPhoneNumber = "(27) 94534-0938"
    private let mockedEmail = "augusto.pereira@gmail.com"
    private let mockedResendDelay = Int.random(in: 0...60)

    private func createInteractor(inputType: SecurityCodeValidationType) -> SecurityCodeValidationInteracting {
        SecurityCodeValidationInteractor(
            service: serviceMock,
            presenter: presenterSpy,
            dependencies: dependenciesMock,
            inputType: inputType,
            inputedValue: inputType == .phone ? mockedPhoneNumber : mockedEmail,
            codeResendDelay: mockedResendDelay
        )
    }
    
    private func createMockedRequestNewCodeResult() throws -> SecurityCodeRequestResponse {
        let newDelay = Int.random(in: 0...60)
        let payload = """
            { "smsDelay": \(newDelay) }
        """
        let responseData = try XCTUnwrap(payload.data(using: .utf8))
        return try JSONDecoder().decode(SecurityCodeRequestResponse.self, from: responseData)
    }

    func testPopulateView_WhenInputTypeIsPhone_ShouldPopulateView() {
        let sut = createInteractor(inputType: .phone)

        sut.populateView()

        XCTAssertEqual(presenterSpy.populateViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.inputTypePopulateViewCall, .phone)
        XCTAssertEqual(presenterSpy.inputedValue, mockedPhoneNumber)
    }

    func testPopulateView_WhenInputTypeIsEmail_ShouldPopulateView() {
        let sut = createInteractor(inputType: .email)

        sut.populateView()

        XCTAssertEqual(presenterSpy.populateViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.inputTypePopulateViewCall, .email)
        XCTAssertEqual(presenterSpy.inputedValue, mockedEmail)
    }

    func testDidAskToResendCode_WhenSuccessResponse_ShouldStartCounter() throws {
        serviceMock.requestNewCodeResult = .success(try createMockedRequestNewCodeResult())
        let sut = createInteractor(inputType: .phone)

        sut.didAskToResendCode()

        XCTAssertEqual(serviceMock.requestNewCodeCallsCount, 1)
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
    }

    func testDidAskToResendCode_WhenResponseIsError_ShouldPresentGeneralError() {
        let requestError: RequestError = {
            var requestError = RequestError()
            requestError.title = "Ops!"
            requestError.message = "Código inválido"
            requestError.code = "1001"
            return requestError
        }()
        serviceMock.requestNewCodeResult = .failure(ApiError.badRequest(body: requestError))
        let sut = createInteractor(inputType: .phone)

        sut.didAskToResendCode()

        XCTAssertEqual(serviceMock.requestNewCodeCallsCount, 1)
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentGeneralErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.generalErrorTitle, requestError.title)
        XCTAssertEqual(presenterSpy.generalErrorMessage, requestError.message)
    }


    func testUpdateCodeValue_WhenCodeHasLessThanFourCharacters_ShouldNotCallApi() {
        let sut = createInteractor(inputType: .phone)

        sut.updateCodeValue("843")

        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 0)
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 0)
        XCTAssertEqual(serviceMock.sendConfirmationCodeCallsCount, 0)
    }

    func testUpdateCodeValue_WhenCodeIsNil_ShouldNotCallApi() {
        let sut = createInteractor(inputType: .phone)

        sut.updateCodeValue(nil)

        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 0)
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 0)
        XCTAssertEqual(serviceMock.sendConfirmationCodeCallsCount, 0)
    }

    func testUpdateCodeValue_WhenCodeHasFourCharactersAndApiRespondsSuccess_ShouldPerformSuccess() {
        serviceMock.sendConfirmationCodeResult = .success(NoContent())
        let sut = createInteractor(inputType: .phone)

        sut.updateCodeValue("6534")

        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(serviceMock.sendConfirmationCodeCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        let expectedAction: SecurityCodeValidationAction = .successfulValidation(for: .phone, inputValue: mockedPhoneNumber)
        XCTAssertEqual(presenterSpy.action, expectedAction)
    }

    func testUpdateCodeValue_WhenCodeHasFourCharactersAndApiRespondsCodeError_ShouldPresentCodeError() {
        let requestError: RequestError = {
            var requestError = RequestError()
            requestError.title = "Ops!"
            requestError.message = "Código inválido"
            requestError.code = "1000"
            return requestError
        }()
        serviceMock.sendConfirmationCodeResult = .failure(ApiError.badRequest(body: requestError))
        let sut = createInteractor(inputType: .phone)

        sut.updateCodeValue("6534")

        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentCodeErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.codeErrorMessage, requestError.message)
        XCTAssertEqual(presenterSpy.updateResendCodeButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.butonState, .hidden)
    }

    func testUpdateCodeValue_WhenCodeHasFourCharactersAndApiRespondsGeneralError_ShouldPresentGeneralError() {
        serviceMock.sendConfirmationCodeResult = .failure(ApiError.serverError)
        let sut = createInteractor(inputType: .phone)

        sut.updateCodeValue("6534")

        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentGeneralErrorCallsCount, 1)
        XCTAssertEqual(presenterSpy.updateResendCodeButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.butonState, .hidden)
    }
    
    // Events
    
    func testPopulateView_WhenInputTypeIsPhone_ShouldSendScreenViewedEvent() {
        let inputType: SecurityCodeValidationType = .phone
        let sut = createInteractor(inputType: inputType)

        sut.populateView()

        let expectedEvent = SecureCodeValidationEvent.viewed(type: inputType).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testPopulateView_WhenInputTypeIsEmail_ShouldSendScreenViewedEvent() {
        let inputType: SecurityCodeValidationType = .email
        let sut = createInteractor(inputType: inputType)

        sut.populateView()

        let expectedEvent = SecureCodeValidationEvent.viewed(type: inputType).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testUpdateCodeValue_WhenInputTypeIsPhone_ShouldSendCodeTypedEvent() {
        serviceMock.sendConfirmationCodeResult = .success(NoContent())
        let inputType: SecurityCodeValidationType = .phone
        let sut = createInteractor(inputType: inputType)

        sut.updateCodeValue("2345")

        let expectedEvent = SecureCodeValidationEvent.codeInputed(type: inputType).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testUpdateCodeValue_WhenInputTypeIsEmail_ShouldSendCodeTypedEvent() {
        serviceMock.sendConfirmationCodeResult = .success(NoContent())
        let inputType: SecurityCodeValidationType = .email
        let sut = createInteractor(inputType: inputType)

        sut.updateCodeValue("7896")

        let expectedEvent = SecureCodeValidationEvent.codeInputed(type: inputType).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testDidAskToResendCode_WhenInputTypeIsPhone_ShouldSendResendCodeTappedEvent() throws {
        serviceMock.requestNewCodeResult = .success(try createMockedRequestNewCodeResult())
        let inputType: SecurityCodeValidationType = .phone
        let sut = createInteractor(inputType: inputType)

        sut.didAskToResendCode()

        let expectedEvent = SecureCodeValidationEvent.resendCodeTapped(type: inputType).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testDidAskToResendCode_WhenInputTypeIsEmail_ShouldSendResendCodeTappedEvent() throws {
        serviceMock.requestNewCodeResult = .success(try createMockedRequestNewCodeResult())
        let inputType: SecurityCodeValidationType = .email
        let sut = createInteractor(inputType: inputType)

        sut.didAskToResendCode()

        let expectedEvent = SecureCodeValidationEvent.resendCodeTapped(type: inputType).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testUpdateCodeValue_WhenInputTypeIsPhoneAndResponseIsError_ShouldSendEvent() {
        serviceMock.sendConfirmationCodeResult = .success(NoContent())
        let inputType: SecurityCodeValidationType = .phone
        let sut = createInteractor(inputType: inputType)

        sut.updateCodeValue("2345")

        let expectedEvent = SecureCodeValidationEvent.codeInputed(type: inputType).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testUpdateCodeValue_WhenApiRespondsCodeError_ShouldSendTexfieldErrorPresentedEvent() {
        let requestError: RequestError = {
            var requestError = RequestError()
            requestError.title = "Ops!"
            requestError.message = "Código inválido"
            requestError.code = "1000"
            return requestError
        }()
        serviceMock.sendConfirmationCodeResult = .failure(ApiError.badRequest(body: requestError))
        let sut = createInteractor(inputType: .phone)

        sut.updateCodeValue("6534")

        let expectedEvent = SecureCodeValidationEvent.errorPresented(
            message: requestError.message,
            type: .phone,
            presentation: .textfield
        ).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }

    func testUpdateCodeValue_WhenApiRespondsGeneralError_ShouldSendDialogErrorPresentedEvent() {
        serviceMock.sendConfirmationCodeResult = .failure(ApiError.connectionFailure)
        let sut = createInteractor(inputType: .email)

        sut.updateCodeValue("6534")

        let expectedEvent = SecureCodeValidationEvent.errorPresented(
            message: "Sem conexão com a internet",
            type: .email,
            presentation: .dialog
        ).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testDidAskToResendCode_WhenResponseIsError_ShouldSendDialogErrorPresentedEvent() {
        let requestError: RequestError = {
            var requestError = RequestError()
            requestError.title = "Ops!"
            requestError.message = "Erro genérico"
            requestError.code = "1000"
            return requestError
        }()
        serviceMock.requestNewCodeResult = .failure(ApiError.badRequest(body: requestError))
        let sut = createInteractor(inputType: .email)

        sut.didAskToResendCode()

        let expectedEvent = SecureCodeValidationEvent.errorPresented(
            message: requestError.message,
            type: .email,
            presentation: .dialog
        ).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
}
