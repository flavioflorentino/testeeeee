@testable import PIX
import XCTest

private final class SecurityValidationCodeDelegateMock: SecurityValidationCodeDelegate {
    private(set) var didValidateKeyWithSuccessCalls = 0
    private(set) var keyValue: String?
    private(set) var keyType: PixKeyType?
    
    func didValidateKeyWithSuccess(keyValue: String, keyType: PixKeyType) {
        self.keyValue = keyValue
        self.keyType = keyType
        didValidateKeyWithSuccessCalls += 1
    }
}

final class SecurityCodeValidationCoordinatorTests: XCTestCase {
    private let navControllerSpy = SpyNavigationController(rootViewController: UIViewController())
    private let delegate = SecurityValidationCodeDelegateMock()
    
    private lazy var sut: SecurityCodeValidationCoordinating = {
        let coordinator = SecurityCodeValidationCoordinator(delegate: delegate)
        coordinator.viewController = navControllerSpy.topViewController
        return coordinator
    }()
    
    func testPerform_WhenActionIsSuccessfulValidationAndInputTypeIsPhone_ShouldPopAndCallDelegate() {
        let inputType = SecurityCodeValidationType.phone
        let inputValue = "(27) 94736-8474"
        sut.perform(action: .successfulValidation(for: inputType, inputValue: inputValue))
        
        XCTAssertEqual(navControllerSpy.poppedViewControllerCount, 1)
        XCTAssertEqual(delegate.didValidateKeyWithSuccessCalls, 1)
        XCTAssertEqual(delegate.keyType, .phone)
        XCTAssertEqual(delegate.keyValue, inputValue)
    }
    
    func testPerform_WhenActionIsSuccessfulValidationAndInputTypeIsEmaile_ShouldPopAndCallDelegate() {
        let inputValue = "fulano.oliveira@yahoo.com"
        sut.perform(action: .successfulValidation(for: .phone, inputValue: inputValue))
        
        XCTAssertEqual(navControllerSpy.poppedViewControllerCount, 1)
        XCTAssertEqual(delegate.didValidateKeyWithSuccessCalls, 1)
        XCTAssertEqual(delegate.keyType, .phone)
    }
}
