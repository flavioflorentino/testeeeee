@testable import PIX
import UI
import XCTest

private final class ManualInsertionCoordinatorSpy: ManualInsertionCoordinating {
    var viewController: UIViewController?
    
    private(set) var performCallsCount = 0
    private(set) var action: ManualInsertionAction?
    private(set) var goToPaymentDetailsWithPaymentDetailsCallsCount = 0
    private(set) var details: PIXPaymentDetails?
    
    func perform(action: ManualInsertionAction) {
        performCallsCount += 1
        self.action = action
    }
    
    func goToPaymentDetailsWithPaymentDetails(_ details: PIXPaymentDetails) {
        goToPaymentDetailsWithPaymentDetailsCallsCount += 1
        self.details = details
    }
}

private final class ManualInsertionViewControllerSpy: ManualInsertionDisplay {
    private(set) var displayCpfTextMaskCallsCount = 0
    private(set) var cpfTextMask: TextMask?
    private(set) var displayCnpjTextMaskCallsCount = 0
    private(set) var cnpjTextMask: TextMask?
    private(set) var displayTextFieldCallsCount = 0
    private(set) var isSelected: Bool?
    private(set) var textFieldType: ManualInsertionContent?
    private(set) var displayCpfCnpjErrorMessageCallsCount = 0
    private(set) var errorMessage: String?
    private(set) var removeCpfCnpjErrorMessageCallsCount = 0
    private(set) var toggleForwardButtonStateCallsCount = 0
    private(set) var displayInstitutionSelectionCallsCount = 0
    private(set) var institutionText: String?
    private(set) var displayAccountTypeSelectionCallsCount = 0
    private(set) var displayOwnershipSelectionCallsCount = 0
    private(set) var beginLoadingCallsCount = 0
    private(set) var endLoadingCallsCount = 0
    private(set) var displayDialogCallsCount = 0
    private(set) var alertData: StatusAlertData?
    private(set) var displayErrorCallsCount = 0
    private(set) var errorTitle: String?
    private(set) var errorSubtitle: String?
    private(set) var errorActions: [PopupAction]?
    private(set) var hideCpfCnpjAndNicknameFieldsCallsCount = 0
    private(set) var displayCpfCnpjAndNicknameFieldsCallsCount = 0
    
    func displayCpfTextMask(_ textMask: TextMask) {
        displayCpfTextMaskCallsCount += 1
        cpfTextMask = textMask
    }
    
    func displayCnpjTextMask(_ textMask: TextMask) {
        displayCnpjTextMaskCallsCount += 1
        cnpjTextMask = textMask
    }
    
    func displayTextField(isSelected: Bool, type: ManualInsertionContent) {
        displayTextFieldCallsCount += 1
        self.isSelected = isSelected
        textFieldType = type
    }
    
    func displayCpfCnpjErrorMessage(message: String) {
        displayCpfCnpjErrorMessageCallsCount += 1
        errorMessage = message
    }
    
    func removeCpfCnpjErrorMessage() {
        removeCpfCnpjErrorMessageCallsCount += 1
    }
    
    func toggleForwardButtonState() {
        toggleForwardButtonStateCallsCount += 1
    }
    
    func displayInstitutionSelection(text: String) {
        displayInstitutionSelectionCallsCount += 1
        institutionText = text
    }
    
    func displayAccountTypeSelection() {
        displayAccountTypeSelectionCallsCount += 1
    }
    
    func displayOwnershipSelection() {
        displayOwnershipSelectionCallsCount += 1
    }
    
    func beginLoading() {
        beginLoadingCallsCount += 1
    }
    
    func endLoading() {
        endLoadingCallsCount += 1
    }
    
    func displayDialog(with alertData: StatusAlertData) {
        displayDialogCallsCount += 1
        self.alertData = alertData
    }
    
    func displayError(title: String, subtitle: String, actions: [PopupAction]) {
        displayErrorCallsCount += 1
        errorTitle = title
        errorSubtitle = subtitle
        errorActions = actions
    }
    
    func hideCpfCnpjAndNicknameFields() {
        hideCpfCnpjAndNicknameFieldsCallsCount += 1
    }
    
    func displayCpfCnpjAndNicknameFields() {
        displayCpfCnpjAndNicknameFieldsCallsCount += 1
    }
}

final class ManualInsertionPresenterTests: XCTestCase {
    private let coordinator = ManualInsertionCoordinatorSpy()
    private let viewController = ManualInsertionViewControllerSpy()
    private lazy var presenter: ManualInsertionPresenter = {
        let presenter = ManualInsertionPresenter(coordinator: coordinator)
        presenter.viewController = viewController
        return presenter
    }()
    
    // MARK: - presentCpfMasker
    func testPresentCpfMasker_ShouldCallDisplayCpfTextMask() {
        presenter.presentCpfMasker()
        XCTAssertEqual(viewController.displayCpfTextMaskCallsCount, 1)
        XCTAssertNotNil(viewController.cpfTextMask)
    }
    
    // MARK: - presentCNPJMasker
    func testPresentCnpjMasker_ShouldCallDisplayCnpjTextMask() {
        presenter.presentCNPJMasker()
        XCTAssertEqual(viewController.displayCnpjTextMaskCallsCount, 1)
        XCTAssertNotNil(viewController.cnpjTextMask)
    }
    
    // MARK: - presentTextFieldValidState
    func testPresentTextFieldValidState_ShouldCallDisplayTextFieldWithIsSelectedTrueAndGivenType() {
        let type = ManualInsertionContent.agency
        presenter.presentTextFieldValidState(type: type)
        XCTAssertEqual(viewController.displayTextFieldCallsCount, 1)
        XCTAssertEqual(viewController.textFieldType, type)
        XCTAssertEqual(viewController.isSelected, true)
    }
    
    // MARK: - presentTextFieldNormalState
    func testPresentTextFieldNormalState_ShouldCallDisplayTextFieldWithIsSelectedFalseAndGivenType() {
        let type = ManualInsertionContent.agency
        presenter.presentTextFieldNormalState(type: type)
        XCTAssertEqual(viewController.displayTextFieldCallsCount, 1)
        XCTAssertEqual(viewController.textFieldType, type)
        XCTAssertEqual(viewController.isSelected, false)
    }
    
    // MARK: - removeCpfCnpjErrorMessage
    func testRemoveCpfCnpjErrorMessage_ShouldCallRemoveCpfCnpjErrorMessage() {
        presenter.removeCpfCnpjErrorMessage()
        XCTAssertEqual(viewController.removeCpfCnpjErrorMessageCallsCount, 1)
    }
    
    // MARK: - presentCpfErrorMessage
    func testPresentCpfErrorMessage_ShouldCallDisplayCpfCnpjErrorMessageWithInvalidFormatErrorMessage() {
        presenter.presentCpfErrorMessage()
        XCTAssertEqual(viewController.displayCpfCnpjErrorMessageCallsCount, 1)
        XCTAssertEqual(viewController.errorMessage, Strings.ManualInsertion.invalidFormat)
    }
    
    // MARK: - presentCnpjErrorMessage
    func testPresentCnpjErrorMessage_ShouldCallDisplayCpfCnpjErrorMessageWithInvalidFormatErrorMessage() {
        presenter.presentCnpjErrorMessage()
        XCTAssertEqual(viewController.displayCpfCnpjErrorMessageCallsCount, 1)
        XCTAssertEqual(viewController.errorMessage, Strings.ManualInsertion.invalidFormat)
    }
    
    // MARK: - toggleForwardButtonState
    func testToggleForwardButtonState_ShouldCallToggleForwardButtonState() {
        presenter.toggleForwardButtonState()
        XCTAssertEqual(viewController.toggleForwardButtonStateCallsCount, 1)
    }
    
    // MARK: - presentInstitutionSelection
    func testPresentInstitutionSelection_ShouldCallDisplayInstitutionSelection() {
        let bankId = "1"
        let bankName = "test"
        presenter.presentInstitutionSelection(id: bankId, name: bankName)
        XCTAssertEqual(viewController.displayInstitutionSelectionCallsCount, 1)
        XCTAssertEqual(viewController.institutionText, "\(bankId) - \(bankName)")
    }
    
    // MARK: - presentAccountTypeSelection
    func testPresentAccountTypeSelection_ShouldCallDisplayAccountTypeSelection() {
        presenter.presentAccountTypeSelection()
        XCTAssertEqual(viewController.displayAccountTypeSelectionCallsCount, 1)
    }
    
    // MARK: - presentOwnershipSelection
    func testPresentOwnershipSelection_ShouldCallDisplayOwnershipSelection() {
        presenter.presentOwnershipSelection()
        XCTAssertEqual(viewController.displayOwnershipSelectionCallsCount, 1)
    }
    
    // MARK: - presentPaymentDetails
    func testPresentPaymentDetails_WhenHasNotAgencyAndAccountNumberValue_ShouldCallEndLoadingAndGoToPaymentDetailsWithPaymentDetailsWithProperDescription() throws {
        let account = PixAccount(
            name: "name",
            cpfCnpj: "cpfCnpj",
            bank: "bank",
            agency: nil,
            accountNumber: nil,
            imageUrl: nil,
            id: nil
        )
        let originType = PIXOriginType.manual
        let keyType = "keyType"
        let params = PIXParams(originType: originType, keyType: keyType, key: nil, receiverData: nil)
        presenter.presentPaymentDetails(account: account, params: params)
        let detail = try XCTUnwrap(coordinator.details)
        XCTAssertEqual(viewController.endLoadingCallsCount, 1)
        XCTAssertEqual(coordinator.goToPaymentDetailsWithPaymentDetailsCallsCount, 1)
        XCTAssertEqual(detail.payee.description, "\(account.cpfCnpj) | \(account.bank)")
        XCTAssertEqual(detail.pixParams.keyType, keyType)
        XCTAssertEqual(detail.pixParams.originType, originType.rawValue)
    }
    
    func testPresentPaymentDetails_WhenHasAgencyAndAccountNumberValue_ShouldCallEndLoadingAndGoToPaymentDetailsWithPaymentDetailsWithProperDescription() throws {
        let account = PixAccount(
            name: "name",
            cpfCnpj: "cpfCnpj",
            bank: "bank",
            agency: "agency",
            accountNumber: "accountNumber",
            imageUrl: nil,
            id: nil
        )
        let params = PIXParams(originType: .manual, keyType: "keyType", key: nil, receiverData: nil)
        presenter.presentPaymentDetails(account: account, params: params)
        let detail = try XCTUnwrap(coordinator.details)
        let agency = try XCTUnwrap(account.agency)
        let accountNumber = try XCTUnwrap(account.accountNumber)
        XCTAssertEqual(viewController.endLoadingCallsCount, 1)
        XCTAssertEqual(coordinator.goToPaymentDetailsWithPaymentDetailsCallsCount, 1)
        XCTAssertEqual(detail.payee.description, "\(account.cpfCnpj) | \(account.bank)\n\(agency) | \(accountNumber)")
    }
    
    // MARK: - presentLoader
    func testPresentLoader_ShouldCallBeginLoading() {
        presenter.presentLoader()
        XCTAssertEqual(viewController.beginLoadingCallsCount, 1)
    }
    
    // MARK: - presentGenericError
    func testPresentGenericError_ShouldCallDisplayDialogAndCallEndLoading() {
        presenter.presentGenericError()
        XCTAssertEqual(viewController.displayDialogCallsCount, 1)
        XCTAssertEqual(viewController.endLoadingCallsCount, 1)
    }
    
    // MARK: - presentError
    func testPresentError_WhenJsonButtonContentDoesntContainsTitle_ShouldNotCreateActionAndCallDisplayErrorAndEndLoading() {
        let title = "title"
        let subtitle = "subtitle"
        let jsonButtons: [[String: Any]] = [[:]]
        presenter.presentError(title: title, subtitle: subtitle, jsonButtons: jsonButtons)
        XCTAssertEqual(viewController.displayErrorCallsCount, 1)
        XCTAssertEqual(viewController.endLoadingCallsCount, 1)
        XCTAssertEqual(viewController.errorTitle, title)
        XCTAssertEqual(viewController.errorSubtitle, subtitle)
        XCTAssertEqual(viewController.errorActions?.count, 0)
    }
    
    func testPresentError_WhenJsonButtonContentContainsTitle_ShouldNotCreateActionAndCallDisplayErrorAndEndLoading() {
        let title = "title"
        let subtitle = "subtitle"
        let jsonButtons: [[String: Any]] = [["title":"test"]]
        presenter.presentError(title: title, subtitle: subtitle, jsonButtons: jsonButtons)
        XCTAssertEqual(viewController.displayErrorCallsCount, 1)
        XCTAssertEqual(viewController.endLoadingCallsCount, 1)
        XCTAssertEqual(viewController.errorTitle, title)
        XCTAssertEqual(viewController.errorSubtitle, subtitle)
        XCTAssertEqual(viewController.errorActions?.count, 1)
    }
    
    // MARK: - hideCpfCnpjAndNicknameFields
    func testHideCpfCnpjAndNicknameFields_ShouldCallHideCpfCnpjAndNicknameFields() {
        presenter.hideCpfCnpjAndNicknameFields()
        XCTAssertEqual(viewController.hideCpfCnpjAndNicknameFieldsCallsCount, 1)
    }
    
    // MARK: - presentCpfCnpjAndNicknameFields
    func testPresentCpfCnpjAndNicknameFields_ShouldCallDisplayCpfCnpjAndNicknameFields() {
        presenter.presentCpfCnpjAndNicknameFields()
        XCTAssertEqual(viewController.displayCpfCnpjAndNicknameFieldsCallsCount, 1)
    }
}
