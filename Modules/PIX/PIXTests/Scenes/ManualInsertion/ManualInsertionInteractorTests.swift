import AnalyticsModule
import Core
@testable import PIX
import XCTest

private final class ManualInsertionServiceMock: ManualInsertionServicing {
    private(set) var validateCallsCount = 0
    private(set) var params: PIXParams?
    var result: Result<PixAccount, ApiError> = .failure(.unknown(nil))
    
    func validate(params: PIXParams, completion: @escaping (Result<PixAccount, ApiError>) -> Void) {
        validateCallsCount += 1
        self.params = params
        completion(result)
    }
}

private final class ManualInsertionPresenterSpy: ManualInsertionPresenting {
    var viewController: ManualInsertionDisplay?
    
    var bankSelection: (id: String, name: String)?
    private(set) var didNextStepCallsCount = 0
    private(set) var action: ManualInsertionAction?
    private(set) var presentCpfMaskerCallsCount = 0
    private(set) var presentCNPJMaskerCallsCount = 0
    private(set) var presentTextFieldValidStateCallsCount = 0
    private(set) var validType: ManualInsertionContent?
    private(set) var presentTextFieldNormalStateCallsCount = 0
    private(set) var normalType: ManualInsertionContent?
    private(set) var removeCpfCnpjErrorMessageCallsCount = 0
    private(set) var presentCpfErrorMessageCallsCount = 0
    private(set) var presentCnpjErrorMessageCallsCount = 0
    private(set) var toggleForwardButtonStateCallsCount = 0
    private(set) var presentInstitutionSelectionCallsCount = 0
    private(set) var bankId: String?
    private(set) var bankName: String?
    private(set) var presentAccountTypeSelectionCallsCount = 0
    private(set) var presentOwnershipSelectionCallsCount = 0
    private(set) var presentPaymentDetailsCallsCount = 0
    private(set) var account: PixAccount?
    private(set) var params: PIXParams?
    private(set) var presentLoaderCallsCount = 0
    private(set) var presentGenericErrorCallsCount = 0
    private(set) var presentErrorCallsCount = 0
    private(set) var hideCpfCnpjAndNicknameFieldsCallsCount = 0
    private(set) var presentCpfCnpjAndNicknameFieldsCallsCount = 0
    
    func didNextStep(action: ManualInsertionAction) {
        didNextStepCallsCount += 1
        self.action = action
        switch action {
        case let .bankSelection(completion):
            let id = bankSelection?.id ?? ""
            let name = bankSelection?.name ?? ""
            completion(id, name)
        case .close:
            break
        }
    }
    
    func presentCpfMasker() {
        presentCpfMaskerCallsCount += 1
    }
    
    func presentCNPJMasker() {
        presentCNPJMaskerCallsCount += 1
    }
    
    func presentTextFieldValidState(type: ManualInsertionContent) {
        presentTextFieldValidStateCallsCount += 1
        validType = type
    }
    
    func presentTextFieldNormalState(type: ManualInsertionContent) {
        presentTextFieldNormalStateCallsCount += 1
        normalType = type
    }
    
    func removeCpfCnpjErrorMessage() {
        removeCpfCnpjErrorMessageCallsCount += 1
    }
    
    func presentCpfErrorMessage() {
        presentCpfErrorMessageCallsCount += 1
    }
    
    func presentCnpjErrorMessage() {
        presentCnpjErrorMessageCallsCount += 1
    }
    
    func toggleForwardButtonState() {
        toggleForwardButtonStateCallsCount += 1
    }
    
    func presentInstitutionSelection(id: String, name: String) {
        presentInstitutionSelectionCallsCount += 1
        bankId = id
        bankName = name
    }
    
    func presentAccountTypeSelection() {
        presentAccountTypeSelectionCallsCount += 1
    }
    
    func presentOwnershipSelection() {
        presentOwnershipSelectionCallsCount += 1
    }
    
    func presentPaymentDetails(account: PixAccount, params: PIXParams) {
        presentPaymentDetailsCallsCount += 1
        self.account = account
        self.params = params
    }
    
    func presentLoader() {
        presentLoaderCallsCount += 1
    }
    
    func presentGenericError() {
        presentGenericErrorCallsCount += 1
    }
    
    func presentError(title: String, subtitle: String, jsonButtons: [[String : Any]]) {
        presentErrorCallsCount += 1
    }
    
    func hideCpfCnpjAndNicknameFields() {
        hideCpfCnpjAndNicknameFieldsCallsCount += 1
    }
    
    func presentCpfCnpjAndNicknameFields() {
        presentCpfCnpjAndNicknameFieldsCallsCount += 1
    }
}

final class ManualInsertionInteractorTests: XCTestCase {
    private let service = ManualInsertionServiceMock()
    private let presenter = ManualInsertionPresenterSpy()
    private let analytics = AnalyticsSpy()
    
    private lazy var interactor = ManualInsertionInteractor(
        service: service,
        presenter: presenter,
        dependencies: DependencyContainerMock(analytics)
    )
    
    // MARK: - loadInitialConfig
    func testLoadInitialConfig_ShouldLogEventUserNavigated() {
        interactor.loadInitialConfig()
        XCTAssertEqual(analytics.event?.name, ManualInsertionUserEvent.userNavigated.event().name)
    }
    
    // MARK: - selectPickerOption
    func testSelectPickerOption_WhenOptionIsAccountType_ShouldCallPresentAccountTypeSelection() {
        interactor.selectPickerOption(AccountType.checkingAccount)
        XCTAssertEqual(presenter.presentAccountTypeSelectionCallsCount, 1)
    }
    
    func testSelectPickerOption_WhenOptionIsOwnershipAndTypeIsPersonal_ShouldCallPresentOwnershipSelectionAndCallHideCpfCnpjAndNicknameFields() {
        interactor.selectPickerOption(Ownership.personal)
        XCTAssertEqual(presenter.presentOwnershipSelectionCallsCount, 1)
        XCTAssertEqual(presenter.hideCpfCnpjAndNicknameFieldsCallsCount, 1)
    }
    
    func testSelectPickerOption_WhenOptionIsOwnershipAndTypeIsOtherPersonal_ShouldCallPresentOwnershipSelectionAndCallPresentCpfCnpjAndNicknameFields() {
        interactor.selectPickerOption(Ownership.otherPersonal)
        XCTAssertEqual(presenter.presentOwnershipSelectionCallsCount, 1)
        XCTAssertEqual(presenter.presentCpfCnpjAndNicknameFieldsCallsCount, 1)
    }
    
    // MARK: - verifyText
    func testVerifyText_WhenTypeIsAgencyAntTextIsEmpty_ShouldCallRemoveTextFieldErrorMessageAndPresentTextFieldNormalState() {
        let text = ""
        interactor.verifyText(text, type: .agency)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 1)
        XCTAssertEqual(presenter.normalType, ManualInsertionContent.agency)
    }
    
    func testVerifyText_WhenTypeIsAgencyAndTextCountIsRangeFrom1To5_ShouldCallRemoveTextFieldErrorMessageAndPresentTextFieldValidState() {
        let text = "1"
        interactor.verifyText(text, type: .agency)
        XCTAssertEqual(presenter.presentTextFieldValidStateCallsCount, 1)
        XCTAssertEqual(presenter.validType, ManualInsertionContent.agency)
    }
    
    func testVerifyText_WhenTypeIsAgencyAndTextCountIsGreaterThan5_ShouldCallpresentTextFieldNormalStateAndPresentTextFieldErrorMessage() {
        let text = "123456"
        interactor.verifyText(text, type: .agency)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 1)
        XCTAssertEqual(presenter.normalType, ManualInsertionContent.agency)
    }
    
    func testVerifyText_WhenTypeIsAccountAndTextCountIsLessThan3_ShouldCallRemoveTextFieldErrorMessageAndPresentTextFieldNormalState() {
        let text = ""
        interactor.verifyText(text, type: .account)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 1)
        XCTAssertEqual(presenter.normalType, ManualInsertionContent.account)
    }
    
    func testVerifyText_WhenTypeIsAccountAndTextCountIsRangeFrom3To14_ShouldCallRemoveTextFieldErrorMessageAndPresentTextFieldValidState() {
        let text = "1-1"
        interactor.verifyText(text, type: .account)
        XCTAssertEqual(presenter.presentTextFieldValidStateCallsCount, 1)
        XCTAssertEqual(presenter.validType, ManualInsertionContent.account)
    }
    
    func testVerifyText_WhenTypeIsAccountAndTextCountIsGreaterThan15_ShouldCallpresentTextFieldNormalStateAndPresentTextFieldErrorMessage() {
        let text = "12345678901234-5"
        interactor.verifyText(text, type: .account)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 1)
        XCTAssertEqual(presenter.normalType, ManualInsertionContent.account)
    }
    
    func testVerifyText_WhenTypeIsCpfCnpjAndTextCountIsLessThan14_ShouldCallRemoveTextFieldErrorMessageAndPresentTextFieldNormalState() {
        let text = ""
        interactor.verifyText(text, type: .cpfCnpj)
        XCTAssertEqual(presenter.removeCpfCnpjErrorMessageCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 1)
        XCTAssertEqual(presenter.normalType, ManualInsertionContent.cpfCnpj)
    }
    
    func testVerifyText_WhenTypeIsCpfCnpjAndTextCountIsEqualTo14AndCpfValid_ShouldCallRemoveTextFieldErrorMessageAndPresentTextFieldValidState() {
        let text = "000.000.001-91"
        interactor.verifyText(text, type: .cpfCnpj)
        XCTAssertEqual(presenter.removeCpfCnpjErrorMessageCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldValidStateCallsCount, 1)
        XCTAssertEqual(presenter.validType, ManualInsertionContent.cpfCnpj)
    }
    
    func testVerifyText_WhenTypeIsCpfCnpjAndTextCountIsEqualTo14AndCpfInvalid_ShouldCallPresentTextFieldNormalStateAndPresentTextFieldErrorMessage() {
        let text = "000.000.001-92"
        interactor.verifyText(text, type: .cpfCnpj)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 1)
        XCTAssertEqual(presenter.presentCpfErrorMessageCallsCount, 1)
        XCTAssertEqual(presenter.normalType, ManualInsertionContent.cpfCnpj)
    }
    
    func testVerifyText_WhenTypeIsCpfCnpjAndTextCountIsInRangeFrom15To17_ShouldCallremoveTextFieldErrorMessageAndPresentTextFieldNormalState() {
        let text = "12.345.678/9101-4"
        interactor.verifyText(text, type: .cpfCnpj)
        XCTAssertEqual(presenter.removeCpfCnpjErrorMessageCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 1)
        XCTAssertEqual(presenter.normalType, ManualInsertionContent.cpfCnpj)
    }
    
    func testVerifyText_WhenTypeIsCpfCnpjAndTextCountIsEqualTo18AndCnpjValid_ShouldCallRemoveTextFieldErrorMessageAndPresentTextFieldValidState() {
        let text = "12.345.678/9101-40"
        interactor.verifyText(text, type: .cpfCnpj)
        XCTAssertEqual(presenter.removeCpfCnpjErrorMessageCallsCount, 1)
        XCTAssertEqual(presenter.presentTextFieldValidStateCallsCount, 1)
        XCTAssertEqual(presenter.validType, ManualInsertionContent.cpfCnpj)
    }
    
    func testVerifyText_WhenTypeIsCpfCnpjAndTextCountIsEqualTo18AndCnpjInvalid_ShouldCallPresentTextFieldNormalStateAndPresentTextFieldErrorMessage() {
        let text = "12.345.678/9101-41"
        interactor.verifyText(text, type: .cpfCnpj)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 1)
        XCTAssertEqual(presenter.presentCnpjErrorMessageCallsCount, 1)
        XCTAssertEqual(presenter.normalType, ManualInsertionContent.cpfCnpj)
    }
    
    func testVerifyText_WhenTypeIsCpfCnpjAndTextCountIsGreaterThan18_ShouldCallpresentTextFieldNormalStateAndPresentTextFieldErrorMessage() {
        let text = "12.345.678/9101-401"
        interactor.verifyText(text, type: .cpfCnpj)
        XCTAssertEqual(presenter.presentTextFieldNormalStateCallsCount, 1)
        XCTAssertEqual(presenter.presentCnpjErrorMessageCallsCount, 1)
        XCTAssertEqual(presenter.normalType, ManualInsertionContent.cpfCnpj)
    }
    
    // MARK: - selectBank
    func testSelectBank_ShouldCallDidNextStepAndPresentInstitutionSelectionWithGivenInfo() throws {
        let bankId = "1"
        let bankName = "test"
        presenter.bankSelection = (bankId, bankName)
        interactor.selectBank()
        XCTAssertEqual(presenter.didNextStepCallsCount, 1)
        XCTAssertEqual(presenter.presentInstitutionSelectionCallsCount, 1)
        XCTAssertEqual(presenter.bankId, bankId)
        XCTAssertEqual(presenter.bankName, bankName)
    }
    
    // MARK: - selectForward
    func testSelectForward_WhenBankIdIsNil_ShouldNotCallServiceValidateAndPresentLoader() {
        interactor.selectForward()
        XCTAssertEqual(service.validateCallsCount, 0)
        XCTAssertEqual(presenter.presentPaymentDetailsCallsCount, 0)
        XCTAssertEqual(presenter.presentLoaderCallsCount, 0)
    }
    
    func testSelectForward_WhenBankIdIsNotNilAndAccountTypeIsNil_ShouldNotCallServiceValidateAndPresentLoader() {
        presenter.bankSelection = ("123", "bankName")
        interactor.selectBank()
        interactor.selectForward()
        XCTAssertEqual(service.validateCallsCount, 0)
        XCTAssertEqual(presenter.presentPaymentDetailsCallsCount, 0)
        XCTAssertEqual(presenter.presentLoaderCallsCount, 0)
    }
    
    func testSelectForward_WhenBankIdAndAccountTypeIsNotNilAndAgencyIsNil_ShouldNotCallServiceValidateAndPresentLoader() {
        presenter.bankSelection = ("123", "bankName")
        interactor.selectBank()
        interactor.selectPickerOption(AccountType.checkingAccount)
        interactor.selectForward()
        XCTAssertEqual(service.validateCallsCount, 0)
        XCTAssertEqual(presenter.presentPaymentDetailsCallsCount, 0)
        XCTAssertEqual(presenter.presentLoaderCallsCount, 0)
    }
    
    func testSelectForward_WhenBankIdAndAccountTypeAndAgencyIsNotNilAndAccountNumberIsNil_ShouldNotCallServiceValidateAndPresentLoader() {
        presenter.bankSelection = ("123", "bankName")
        interactor.selectBank()
        interactor.selectPickerOption(AccountType.checkingAccount)
        interactor.verifyText("0001", type: .agency)
        interactor.selectForward()
        XCTAssertEqual(service.validateCallsCount, 0)
        XCTAssertEqual(presenter.presentPaymentDetailsCallsCount, 0)
        XCTAssertEqual(presenter.presentLoaderCallsCount, 0)
    }
    
    func testSelectForward_WhenBankIdAndAccountTypeAndAgencyAndAccountNumberIsNotNilAndCpfCpnjIsNil_ShouldNotCallServiceValidateAndPresentLoader() {
        presenter.bankSelection = ("123", "bankName")
        interactor.selectBank()
        interactor.selectPickerOption(AccountType.checkingAccount)
        interactor.verifyText("0001", type: .agency)
        interactor.verifyText("12345-6", type: .account)
        interactor.selectForward()
        XCTAssertEqual(service.validateCallsCount, 0)
        XCTAssertEqual(presenter.presentPaymentDetailsCallsCount, 0)
        XCTAssertEqual(presenter.presentLoaderCallsCount, 0)
    }
    
    func testSelectForward_WhenBankIdAndAccountTypeAndAgencyAndAccountNumberAndOwnershipIsNotNilAndOwnershipTypeIsOtherPersonal_ShouldNotCallServiceValidateAndPresentLoader() {
        presenter.bankSelection = ("123", "bankName")
        interactor.selectBank()
        interactor.selectPickerOption(AccountType.checkingAccount)
        interactor.verifyText("0001", type: .agency)
        interactor.verifyText("12345-6", type: .account)
        interactor.selectPickerOption(Ownership.otherPersonal)
        interactor.selectForward()
        XCTAssertEqual(service.validateCallsCount, 0)
        XCTAssertEqual(presenter.presentPaymentDetailsCallsCount, 0)
        XCTAssertEqual(presenter.presentLoaderCallsCount, 0)
    }
    
    func testSelectForward_WhenBankIdAndAccountTypeAndAgencyAndAccountNumberAndOwnershipAndCpfCnpjIsNotNilAndOwnershipTypeIsOtherPersonalAndSomeDataIsNotValid_ShouldNotCallServiceValidateAndPresentLoader() {
        presenter.bankSelection = ("123", "bankName")
        interactor.selectBank()
        interactor.selectPickerOption(AccountType.checkingAccount)
        interactor.verifyText("0001", type: .agency)
        interactor.verifyText("12345-6", type: .account)
        interactor.selectPickerOption(Ownership.otherPersonal)
        interactor.verifyText("0", type: .cpfCnpj)
        interactor.selectForward()
        XCTAssertEqual(service.validateCallsCount, 0)
        XCTAssertEqual(presenter.presentPaymentDetailsCallsCount, 0)
        XCTAssertEqual(presenter.presentLoaderCallsCount, 0)
    }
    
    func testSelectForward_WhenBankIdAndAccountTypeAndAgencyAndAccountNumberAndOwnershipIsNotNilAndOwnershipTypeIsPersonalAndIsAllValidAndResultIsSuccess_ShouldCallServiceValidateAndPresentLoaderAndPresentPaymentDetails() {
        let account = PixAccount(
            name: "name",
            cpfCnpj: "cpfCnpj",
            bank: "bank",
            agency: "agency",
            accountNumber: "accountNumber",
            imageUrl: nil,
            id: nil
        )
        service.result = .success(account)
        presenter.bankSelection = ("123", "bankName")
        interactor.selectBank()
        interactor.selectPickerOption(AccountType.checkingAccount)
        interactor.verifyText("0001", type: .agency)
        interactor.verifyText("12345-6", type: .account)
        interactor.selectPickerOption(Ownership.personal)
        interactor.selectForward()
        XCTAssertEqual(service.validateCallsCount, 1)
        XCTAssertEqual(presenter.presentPaymentDetailsCallsCount, 1)
        XCTAssertEqual(presenter.presentLoaderCallsCount, 1)
        XCTAssertNotNil(presenter.account)
        XCTAssertNotNil(presenter.params)
    }

    func testSelectForward_WhenBankIdAndAccountTypeAndAgencyAndAccountNumberAndCpfCpnjIsNotNilAndOwnershipAndContentValidityIsAllValidAndResultIsSuccess_ShouldCallServiceValidateAndPresentLoaderAndPresentPaymentDetails() {
        let account = PixAccount(
            name: "name",
            cpfCnpj: "cpfCnpj",
            bank: "bank",
            agency: "agency",
            accountNumber: "accountNumber",
            imageUrl: nil,
            id: nil
        )
        service.result = .success(account)
        presenter.bankSelection = ("123", "bankName")
        interactor.selectBank()
        interactor.selectPickerOption(AccountType.checkingAccount)
        interactor.verifyText("0001", type: .agency)
        interactor.verifyText("12345-6", type: .account)
        interactor.verifyText("000.000.001-91", type: .cpfCnpj)
        interactor.verifyText("Teste Unitário", type: .nickname)
        interactor.selectPickerOption(Ownership.otherPersonal)
        interactor.selectForward()
        XCTAssertEqual(presenter.presentLoaderCallsCount, 1)
        XCTAssertEqual(service.validateCallsCount, 1)
        XCTAssertEqual(presenter.presentPaymentDetailsCallsCount, 1)
        XCTAssertNotNil(presenter.account)
        XCTAssertNotNil(presenter.params)
    }
    
    func testSelectForward_WhenBankIdAndAccountTypeAndAgencyAndAccountNumberAndOwnershipIsNotNilAndOwnershipTypeIsPersonalAndIsAllValidAndResultIsFailure_ShouldCallServiceValidateAndPresentLoaderAndPresentGenericError() {
        presenter.bankSelection = ("123", "bankName")
        interactor.selectBank()
        interactor.selectPickerOption(AccountType.checkingAccount)
        interactor.verifyText("0001", type: .agency)
        interactor.verifyText("12345-6", type: .account)
        interactor.selectPickerOption(Ownership.personal)
        interactor.selectForward()
        XCTAssertEqual(service.validateCallsCount, 1)
        XCTAssertEqual(presenter.presentGenericErrorCallsCount, 1)
        XCTAssertEqual(presenter.presentLoaderCallsCount, 1)
    }
    
    func testSelectForward_WhenBankIdAndAccountTypeAndAgencyAndAccountNumberAndOwnershipIsNotNilAndOwnershipTypeIsPersonalAndIsAllValidAndResultIsFailureWithAlert_ShouldCallServiceValidateAndPresentLoaderAndPresentError() throws {
        let decoder = JSONFileDecoder<RequestError>()
        let requestError = try decoder.load(
            resource: "RequestErrorUIAlert",
            typeDecoder: .convertFromSnakeCase
        )
        service.result = .failure(.badRequest(body: requestError))
        presenter.bankSelection = ("123", "bankName")
        interactor.selectBank()
        interactor.selectPickerOption(AccountType.checkingAccount)
        interactor.verifyText("0001", type: .agency)
        interactor.verifyText("12345-6", type: .account)
        interactor.selectPickerOption(Ownership.personal)
        interactor.selectForward()
        XCTAssertEqual(service.validateCallsCount, 1)
        XCTAssertEqual(presenter.presentErrorCallsCount, 1)
        XCTAssertEqual(presenter.presentLoaderCallsCount, 1)
    }
}
