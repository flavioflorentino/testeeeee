@testable import PIX
import XCTest

private final class BankSelectorSpy: BankSelector {
    private(set) static var makeCallsCount = 0
    private(set) static var title: String?
    private(set) static var searchPlaceholder: String?
    private(set) static var onBankSelectionCompletion: ((String, String) -> Void)?
    
    static func make(title: String, searchPlaceholder: String, onBankSelectionCompletion: @escaping (String, String) -> Void) -> UIViewController {
        self.title = title
        self.searchPlaceholder = searchPlaceholder
        self.onBankSelectionCompletion = onBankSelectionCompletion
        makeCallsCount += 1
        return UIViewController()
    }
}

final class ManualInsertionCoordinatorTests: XCTestCase {
    private lazy var viewController: SpyViewController = {
        let controller = SpyViewController()
        self.navigationController.pushViewController(controller, animated: false)
        return controller
    }()
    private let navigationController = SpyNavigationController()
    private let paymentOpening = FakePaymentOpening()
    private lazy var coordinator: ManualInsertionCoordinating = {
        let coordinator = ManualInsertionCoordinator(bankSelectorType: BankSelectorSpy.self, paymentOpening: paymentOpening)
        coordinator.viewController = viewController
        return coordinator
    }()
    
    // MARK: - perform
    func testPerform_WhenActionIsBankSelection_ShouldCallPushViewController() {
        coordinator.perform(action: .bankSelection(completion: { _, _ in }))
        XCTAssertEqual(navigationController.pushViewControllerCallCount, 2)
    }
    
    // MARK: - goToPaymentDetailsWithPaymentDetails
    func testGoToPaymentDetailsWithPaymentDetails_ShouldCallOpenPaymentWithDetails() {
        let payee = PIXPayee(name: "name", description: "description", imageURLString: nil, id: nil)
        let params = PIXParams(originType: .manual, keyType: "keyType", key: nil, receiverData: nil)
        let details = PIXPaymentDetails(
            title: "title",
            screenType: .default(commentPlaceholder: "commentPlaceholder"),
            payee: payee,
            pixParams: params
        )
        coordinator.goToPaymentDetailsWithPaymentDetails(details)
        XCTAssertEqual(paymentOpening.openPaymentWithDetailsCallsCount, 1)
        XCTAssertEqual(paymentOpening.details, details)
    }
}
