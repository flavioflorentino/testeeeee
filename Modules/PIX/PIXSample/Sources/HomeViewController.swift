import PIX
import SnapKit
import UI
import UIKit
import Foundation

class HomeViewController: UIViewController {
    private lazy var buttonsStackview: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var pixHubButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle("PIX Hub", for: .normal)
        button.addTarget(self, action: #selector(tapPixHubButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var pixHubPjButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(SecondaryButtonStyle())
        button.setTitle("PIX Hub PJ", for: .normal)
        button.addTarget(self, action: #selector(tapPixBizHubButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var pixBizTableviewButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(SecondaryButtonStyle())
        button.setTitle("PIX Biz Tableview Hub", for: .normal)
        button.addTarget(self, action: #selector(tapPixBixButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var keyManagementButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(SecondaryButtonStyle())
        button.setTitle("Gerenciamento de chaves", for: .normal)
        button.addTarget(self, action: #selector(tapKeyManagementButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var receiptBizButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(SecondaryButtonStyle())
        button.setTitle("Receipt Biz Screen", for: .normal)
        button.addTarget(self, action: #selector(tapReceiptBizButton), for: .touchUpInside)
        return button
    }()

    private lazy var customizeQRCodeButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(SecondaryButtonStyle())
        button.setTitle("Customize QRCode", for: .normal)
        button.addTarget(self, action: #selector(tapCustomizeQRCodeButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var confirmPayment: UIButton = {
        let button = UIButton()
        button.buttonStyle(SecondaryButtonStyle())
        button.setTitle("Confirm Payment", for: .normal)
        button.addTarget(self, action: #selector(tapConfirmPayment), for: .touchUpInside)
        return button
    }()
  
    private lazy var paymentAmountPjButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(SecondaryButtonStyle())
        button.setTitle("Valor do Pagamento PJ", for: .normal)
        button.addTarget(self, action: #selector(tapBizPaymentAmountButton), for: .touchUpInside)
        return button
    }()
  
    private lazy var loadingCashoutBizButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(SecondaryButtonStyle())
        button.setTitle("Loading Cashout", for: .normal)
        button.addTarget(self, action: #selector(tapLoadingCashoutBizButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var manualInsertionBizButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(SecondaryButtonStyle())
        button.setTitle("Inserção Manual", for: .normal)
        button.addTarget(self, action: #selector(tapBizManualInsertionButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var consumerKeysButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(SecondaryButtonStyle())
        button.setTitle("Nova tela de chaves", for: .normal)
        button.addTarget(self, action: #selector(tapConsumerKeysButton), for: .touchUpInside)
        return button
    }()
      
    init() {
        super.init(nibName: nil, bundle: nil)
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    private func tapPixHubButton() {
        PIXConsumerFlowCoordinator(
            originViewController: self,
            originFlow: .deeplink,
            destination: .hub(
                origin: "Origem",
                paymentOpening: SamplePaymentOpening(),
                bankSelectorType: SampleBankSelector.self,
                qrCodeScannerType: SampleQrCodeScanning.self
            )
        ).start()
    }
    
    @objc
    private func tapPixBizHubButton() {
        PIXBizFlowCoordinator(with: navigationController ?? UINavigationController()).perform(flow: .hub)
    }
    
    @objc
    private func tapPixBixButton() {
        let userInfo = KeyManagerBizUserInfo(name: "", cnpj: "24.894.589/0001-87", mail: "miralta@picpay.com", phone: "(27) 91222 1929", userId: "1")
        let coordinator = PIXBizFlowCoordinator(with: navigationController ?? UINavigationController(), userInfo: userInfo)
        coordinator.perform(flow: .optin(action: .keyManager(input: .none)))
    }
    
    @objc
    private func tapKeyManagementButton() {
        PIXConsumerFlowCoordinator(
            originViewController: self,
            originFlow: .deeplink,
            destination: .keyManagement
        ).start()
    }
    
    @objc
    private func tapReceiptBizButton() {
        let userInfo = KeyManagerBizUserInfo(name: "", cnpj: "24.894.589/0001-87", mail: "miralta@picpay.com", phone: "(27) 91222 1929", userId: "1")
        let coordinator = PIXBizFlowCoordinator(with: navigationController ?? UINavigationController(), userInfo: userInfo)
        coordinator.perform(flow: .receipt(transactionId: "aaaaaa", isRefundAllowed: true))
    }
    
    @objc
    private func tapBizPaymentAmountButton() {
        let transactionId = "0"
        let refund = PIXBizCashoutAction.refund(transactionId: transactionId)        
        PIXBizFlowCoordinator(with: navigationController ?? UINavigationController()).perform(flow: .cashout(action: refund))
    }

    @objc
    private func tapCustomizeQRCodeButton() {
        let userInfo = KeyManagerBizUserInfo(name: "", cnpj: "24.894.589/0001-87", mail: "miralta@picpay.com", phone: "(27) 91222 1929", userId: "1")
        
        guard let pixKeysData = decodePixKeyList(fileName: "pixKey") else { return }
        
        let pixKeys = pixKeysData.data.filter { !$0.registeredKeys.isEmpty }
        let registeredKeys = pixKeys.map { $0.registeredKeys.first }.compactMap { $0 }
        guard let qrCode = decodeQRCode(fileName: "qrCode") else {
            return
        }
        let coordinator = PIXBizFlowCoordinator(with: navigationController ?? UINavigationController(), userInfo: userInfo)
        coordinator.perform(flow: .cashin(action: .receive(keys: registeredKeys, customQRCode: qrCode)))
    }
    
    @objc
    private func tapConfirmPayment() {
        let value = 100.0
        guard let pixAccount = decodePixAccount(fileName: "pixAccount") else {
            return
        }
        let coordinator = PIXBizFlowCoordinator(with: navigationController ?? UINavigationController())
//        coordinator.perform(flow: .cashout(action: .confirmPayment(input: .none, pixAccount: pixAccount, value: value)))
    }
  
    @objc
    private func tapLoadingCashoutBizButton() {
        PIXBizFlowCoordinator(with: navigationController ?? UINavigationController())
            .perform(flow: .cashout(action: .keys))
    }
    
    @objc
    private func tapBizManualInsertionButton() {
        let userInfo = KeyManagerBizUserInfo(
            name: "Padaria do Manuel",
            cnpj: "24.894.589/0001-87",
            mail: "miralta@picpay.com",
            phone: "(27) 91222 1929",
            userId: "1"
        )
        let bankAccount = PIXBizCashoutAction.bankAccount
        let coordinator = PIXBizFlowCoordinator(with: navigationController ?? UINavigationController(), userInfo: userInfo)
        
        coordinator.perform(flow: .cashout(action: bankAccount))
    }
    
    @objc
    private func tapConsumerKeysButton() {
        let consumerKeys = ConsumerKeysFactory.make()
        let navController = UINavigationController(rootViewController: consumerKeys)
        
        navigationController?.present(navController, animated: true)
    }
}

private extension HomeViewController {
    func decodePixKeyList(fileName: String) -> PixKeyDataList? {
        let decoder = JSONDecoder()
        guard let data = getData(fileName) else { return nil }
        
        let requestSuccess = try? decoder.decode(PixKeyDataList.self, from: data)
        
        return requestSuccess
    }

    func decodePixAccount(fileName: String) -> PixAccount? {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        guard let data = getData(fileName) else { return nil }
  
        do {
            let requestSuccess = try decoder.decode(PixAccount.self, from: data)
            return requestSuccess
        } catch(let error) {
            print(error.localizedDescription)
        }
        return nil
    }
    
    func decodeQRCode(fileName: String) -> PixQRCode? {
        let decoder = JSONDecoder()
        guard let data = getData(fileName) else { return nil }
        
        let requestSuccess = try? decoder.decode(PixQRCode.self, from: data)
        
        return requestSuccess
    }
    
    func getData(_ fileName: String) -> Data? {
        guard
            let file = Bundle(for: type(of: self)).path(forResource: fileName, ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: file))
            else {
                return nil
        }
        return data
    }
}

final class SamplePaymentOpening: PIXPaymentOpening {

    func openPaymentWithDetails(
        _ details: PIXPaymentDetails,
        onViewController controller: UIViewController,
        origin: PixPaymentOrigin,
        flowType: PixPaymentFlowType
    ) {
        controller.navigationController?.pushViewController(UIViewController(), animated: true)
    }
}

enum SampleBankSelector: BankSelector {
    static func make(title: String, searchPlaceholder: String, onBankSelectionCompletion: @escaping (String, String) -> Void) -> UIViewController {
        let controller = UIViewController()
        controller.title = title
        return controller
    }
}

final class SampleQrCodeScanning: UIViewController, QrCodeScanning {
    init(backButtonSide: BackButtonSide, hasMyCodeBottomSheet: Bool, scannerType: ScannerType) {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension HomeViewController: ViewConfiguration {
    func buildViewHierarchy() {
        buttonsStackview.addArrangedSubview(pixHubButton)
        buttonsStackview.addArrangedSubview(pixHubPjButton)
        buttonsStackview.addArrangedSubview(pixBizTableviewButton)
        buttonsStackview.addArrangedSubview(keyManagementButton)
        buttonsStackview.addArrangedSubview(receiptBizButton)
        buttonsStackview.addArrangedSubview(paymentAmountPjButton)
        buttonsStackview.addArrangedSubview(customizeQRCodeButton)
        buttonsStackview.addArrangedSubview(confirmPayment)
        buttonsStackview.addArrangedSubview(loadingCashoutBizButton)
        buttonsStackview.addArrangedSubview(manualInsertionBizButton)
        buttonsStackview.addArrangedSubview(consumerKeysButton)
        view.addSubview(buttonsStackview)
    }
    
    func setupConstraints() {
        buttonsStackview.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
    }
    
    func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}
