import PIX
import UI
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication,
                     // swiftlint:disable:next discouraged_optional_collection
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupAppearance()
        setupDependencies()
        
        if #available(iOS 13.0, *) {
            return true
        }
        
        window = UIWindow()
        window?.rootViewController = UINavigationController(rootViewController: HomeViewController())
        window?.makeKeyAndVisible()
        return true
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication,
                     configurationForConnecting connectingSceneSession: UISceneSession,
                     options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
}

extension AppDelegate {
    func setupAppearance() {
        UIToolbar.appearance().tintColor = Colors.branding400.color
        UIToolbar.appearance().barTintColor = Colors.white.color

        UINavigationBar.appearance().barTintColor = Colors.grayscale100.color
        UINavigationBar.appearance().tintColor = Colors.branding400.color
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: Colors.grayscale700.color]
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
    }
    
    func setupDependencies() {
        PIXSetup.inject(instance: PixConsumerMock())
    }
}

struct PixConsumerMock: PixConsumerContract {
    var imageURL: URL?
    
    var consumerId: Int {
        4928860
    }
    
    var name: String {
        "João Francisco Oliveira"
    }
}
