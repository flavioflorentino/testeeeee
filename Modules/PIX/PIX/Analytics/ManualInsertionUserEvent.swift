import AnalyticsModule

private extension AccountType {
    var event: String {
        switch self {
        case .checkingAccount:
            return "current_account"
        case .savingAccount:
            return "savings_account"
        }
    }
}

private extension Ownership {
    var event: String {
        switch self {
        case .personal:
            return "same_person"
        case .otherPersonal:
            return "other_person"
        }
    }
}

enum ManualInsertionUserEvent: AnalyticsKeyProtocol {
    case userNavigated
    case forwarded(institution: String, accountType: AccountType, ownership: Ownership)
    case sendError
    
    private var name: String {
        switch self {
        case .userNavigated:
            return "User Navigated"
        case .forwarded:
            return "Manual Send Fowarded"
        case .sendError:
            return "Manual Send Error"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .userNavigated:
            return ["from": "pix_hub", "to": "pix_send_manual"]
        case let .forwarded(institution, accountType, ownership):
            return ["financial_institution": institution, "account_type": accountType.event, "ownership": ownership.event]
        case .sendError:
            return ["error_type": "invalid_format"]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("Pix \(name)", properties: properties, providers: [.mixPanel, .appsFlyer, .firebase])
    }
}
