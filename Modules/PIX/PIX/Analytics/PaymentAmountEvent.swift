import AnalyticsModule
import Foundation

enum PaymentAmountEvent: AnalyticsKeyProtocol {
    case insufficientFunds(sellerId: String, userName: String, date: String, valor: Double)
    case maxRefundAmountExceeded(sellerId: String, userName: String, date: String)
    
    var name: String {
        switch self {
        case .insufficientFunds:
            return "Pix - Insufficient Funds"
            
        case .maxRefundAmountExceeded:
            return "Pix - Error in Payment"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .insufficientFunds(sellerId, userName, date, valor):
            return [
                "data": date,
                "seller_id": sellerId,
                "user_name": userName,
                "valor": valor
            ]
            
        case let .maxRefundAmountExceeded(sellerId, userName, date):
            return [
                "data": date,
                "seller_id": sellerId,
                "user_name": userName,
                "error_type": "higher_value"
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.mixPanel])
    }
}
