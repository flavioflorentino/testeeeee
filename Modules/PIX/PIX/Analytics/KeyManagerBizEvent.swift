import AnalyticsModule

extension PixKeyType {
    var analyticsMethod: String {
        switch self {
        case .random:
            return "random_key"
        case .phone:
            return "cellphone"
        default:
            return self.rawValue
        }   
    }
}
