import AnalyticsModule

struct KeyBizTracker: AnalyticsKeyProtocol {
    var companyName: String
    var eventType: KeyBizEvent
    
    private var properties: [String: Any] {
        var properties: [String: Any]
        switch eventType {
        case let .keyManagerSelected(method),
             let .successfulRegistration(method),
             let .deletionModal(method),
             let .deletionPending(method),
             let .deletionSuccess(method),
             let .portability(method),
             let .portabilityComeBack(method),
             let .portabilityStart(method),
             let .portabilityDeletion(method),
             let .portabilityRequestReceived(method),
             let .portabilityOutOfDate(method),
             let .portabilityReceivedStart(method),
             let .portabilityReceivedComeBack(method),
             let .portabilityPendingRegistration(method),
             let .claim(method),
             let .claimStart(method),
             let .claimComeBack(method),
             let .claimInfo(method),
             let .claimDeletion(method),
             let .claimRefused(method),
             let .claimSuccess(method),
             let .claimRequestReceived(method),
             let .claimOutOfDate(method),
             let .claimAllowed(method),
             let .claimPendingRegistration(method),
             let .claimRequestUndone(method),
             let .cancelClaimModal(method),
             let .claimKeyAccepted(method),
             let .validationNotCompleted(method):
            properties = ["method": method.analyticsMethod]
        case let .portabilitySuccess(keyValue),
             let .pendingRegistration(keyValue),
             let .portabilityInfo(keyValue),
             let .portabilityRequestDone(keyValue),
             let .portabilityRefused(keyValue),
             let .claimRequestDone(keyValue):
            properties = ["method": keyValue]
        case let .sendWithKeys(method):
            properties = ["method": method.serviceValue]
        case let .agreementSendValidationStatus(type, status):
            properties = [
                "method": type.analyticsMethod,
                "status": status.rawValue
            ]
        case let .communicationError(type, error):
            properties = [
                "method": type.analyticsMethod,
                "error": error
            ]
        case let .receipt(value):
            properties = ["value": value]
        case let .randomKeyGenerationDone(description):
            properties = ["description": description]
        case let .internalError(error):
            properties = ["error": error]
        case let .paymentReceiptRefund(value, description, identifier):
            properties = [
                "valor": value,
                "description": description,
                "identificador": identifier
            ]
        case let .createCustomQRCode(type, value, description, identifier):
            properties = [
                "method": type.analyticsMethod,
                "value": value,
                "description": description,
                "identifier": identifier
            ]
        case let .errorInPayment(errorType),
             let .keyError(errorType):
            properties = ["error_type": errorType]
        default:
            properties = [:]
        }
        
        properties["user_name"] = companyName
        return properties
    }
    
    private var name: String {
        eventType.name
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("Pix - \(name)", properties: properties, providers: [.mixPanel])
    }
}

enum KeyBizEvent {
    case buttonReceivePayment
    case pixPopupButtonRegister
    case pixPopupNotNow
    case welcomeCarouselComeBack
    case welcomeCarouselSkip
    case welcomeCarousel(page: Int)
    case internalError(error: String)
    case keyManagerInfoTapped
    case keyManagerViewed
    case keyManagerSelected(method: PixKeyType)
    case randomKeyQuestionTapped
    case randomKeyGenerationInfoViewed
    case randomKeyGenerationViewed
    case randomKeyGenerationDone(description: String)
    case agreementViewed
    case agreementDone
    case agreementSendValidationStatus(method: PixKeyType, status: PixKeyStatus)
    case pendingRegistration(keyValue: String)
    case communicationError(method: PixKeyType, error: String)
    case successfulRegistration(method: PixKeyType)
    case deletionModal(method: PixKeyType)
    case deletionPending(method: PixKeyType)
    case deletionSuccess(method: PixKeyType)
    case portability(method: PixKeyType)
    case portabilityComeBack(method: PixKeyType)
    case portabilityStart(method: PixKeyType)
    case portabilityInfo(keyValue: String)
    case portabilityDeletion(method: PixKeyType)
    case portabilityRefused(keyValue: String)
    case portabilitySuccess(keyValue: String)
    case portabilityRequestReceived(method: PixKeyType)
    case portabilityOutOfDate(method: PixKeyType)
    case portabilityReceivedStart(method: PixKeyType)
    case portabilityReceivedComeBack(method: PixKeyType)
    case portabilityPendingRegistration(method: PixKeyType)
    case portabilityRequestDone(keyValue: String)
    case claim(method: PixKeyType)
    case claimStart(method: PixKeyType)
    case claimComeBack(method: PixKeyType)
    case claimInfo(method: PixKeyType)
    case claimDeletion(method: PixKeyType)
    case claimRefused(method: PixKeyType)
    case claimSuccess(method: PixKeyType)
    case claimRequestReceived(method: PixKeyType)
    case claimOutOfDate(method: PixKeyType)
    case claimAllowed(method: PixKeyType)
    case claimPendingRegistration(method: PixKeyType)
    case claimRequestUndone(method: PixKeyType)
    case claimRequestDone(keyValue: String)
    case cancelClaimModal(method: PixKeyType)
    case cancelRequestPortability(method: PixKeyType)
    case claimKeyAccepted(method: PixKeyType)
    case validationNotCompleted(method: PixKeyType)
    case paymentReceiptOptions
    case paymentReceiptShare
    case paymentReceiptRefund(value: String, description: String, identifier: String)
    case paymentReceiptSaveImage
    case buttonKeyChangedPaymentHub
    case buttonShareQRCodePaymentHub
    case customQRCodePaymentHub
    case createCustomQRCode(method: PixKeyType, value: Double, description: String, identifier: String)
    case buttonShareCustomQRCode
    case saveCustomQRCodeImage
    case buttonSendWithKeys
    case buttonSendWithQrCode
    case buttonSendWithBankAcount
    case buttonSendWithCopyPaste
    case sendWithKeys(method: KeyType)
    case faqInQrCode
    case receipt(value: Double)
    case keyError(error: String)
    case errorInPayment(errorType: String)
    case outdatedReturn
    
    var name: String {
        switch self {
        case .pixPopupButtonRegister:
            return "Button Register"
        case .pixPopupNotNow:
            return "Button Not now"
        case .welcomeCarouselComeBack:
            return "Carousel Come back"
        case .welcomeCarouselSkip:
            return "Button Jump"
        case let .welcomeCarousel(page):
            return "Carousel \(page)"
        case .internalError:
            return "Internal Error"
        case .keyManagerInfoTapped:
            return "Button Question"
        case .keyManagerViewed:
            return "Send Keys Viewed"
        case .keyManagerSelected:
            return "Send Keys Type Selected"
        case .randomKeyQuestionTapped:
            return "Link Random Key"
        case .randomKeyGenerationInfoViewed:
            return "Random Key Generation Info"
        case .randomKeyGenerationViewed:
            return "Random Key Generation"
        case .randomKeyGenerationDone:
            return "Random Key Generation Done"
        case .agreementViewed:
            return "Agreement Viewed"
        case .agreementDone:
            return "Agreement Done"
        case .agreementSendValidationStatus:
            return "Send Keys Validation Status"
        case .pendingRegistration:
            return "Pending Registration"
        case .communicationError:
            return "Communication Error"
        case .successfulRegistration:
            return "Successful Registration"
        case .deletionModal:
            return "Deletion"
        case .deletionPending:
            return "Pending Deletion"
        case .deletionSuccess:
            return "Successful Deletion"
        case .portability:
            return "Portability"
        case .portabilityComeBack:
            return "Portability Come Back"
        case .portabilityStart:
            return "Portability Start"
        case .portabilityInfo:
            return "Portability Info"
        case .portabilityDeletion:
            return "Portability Deletion"
        case .portabilityRefused:
            return "Refused Portability"
        case .portabilitySuccess:
            return "Successful Portability"
        case .portabilityRequestReceived:
            return "Portability Request Received"
        case .portabilityOutOfDate:
            return "Portability Out of date"
        case .portabilityReceivedStart:
            return "Portability Received Start"
        case .portabilityReceivedComeBack:
            return "Portability Received Come Back"
        case .portabilityPendingRegistration:
            return "Portability Pending Registration"
        case .portabilityRequestDone:
            return "Portability Request Done"
        case .claim:
            return "Claim"
        case .claimStart:
            return "Claim Start"
        case .claimComeBack:
            return "Claim Come Back"
        case .claimInfo:
            return "Claim Info"
        case .claimDeletion:
            return "Claim Deletion"
        case .claimRefused:
            return "Refused Claim"
        case .claimSuccess:
            return "Successful Claim"
        case .claimRequestReceived:
            return "Claim Request Received"
        case .claimOutOfDate:
            return "Claim Out of Date"
        case .claimAllowed:
            return "Allowed Claim"
        case .claimPendingRegistration:
            return "Claim Pending Registration"
        case .claimRequestUndone:
            return "Claim Request Undone"
        case .claimRequestDone:
            return "Claim Request Done"
        case .cancelClaimModal:
            return "Claim Cancellation"
        case .cancelRequestPortability:
            return "Portability Cancellation"
        case .claimKeyAccepted:
            return "Claim Accepted"
        case .validationNotCompleted:
            return "Validation Not Completed"
        case .buttonReceivePayment:
            return "Button Receive PIX Payment"
        case .paymentReceiptOptions:
            return "Payment Receipt Options"
        case .paymentReceiptShare:
            return "Payment Receipt Share"
        case .paymentReceiptRefund:
            return "Payment Receipt Refund"
        case .paymentReceiptSaveImage:
            return "Payment Receipt Save Image"
        case .buttonKeyChangedPaymentHub:
            return "Button Key Change - Payment Hub"
        case .buttonShareQRCodePaymentHub:
            return "Button Share QR Code - Payment Hub"
        case .customQRCodePaymentHub:
            return "Link Create Custom QR Code"
        case .createCustomQRCode:
            return "Button Create Custom QR Code"
        case .buttonShareCustomQRCode:
            return "Button Share Custom QR Code"
        case .saveCustomQRCodeImage:
            return "Link Save Custom QR Code Image"
        case .buttonSendWithKeys:
            return "Button Send with Keys"
        case .buttonSendWithQrCode:
            return "Button Send with Qr Code"
        case .buttonSendWithBankAcount:
            return "Button Send with Bank Account"
        case .buttonSendWithCopyPaste:
            return "Button Send with Copy Paste"
        case .sendWithKeys:
            return "Send with Keys"
        case .faqInQrCode:
            return "FAQ in Qr Code"
        case .receipt:
            return "Receipt"
        case .keyError:
            return "Key Error"
        case .errorInPayment:
            return "Error in Payment"
        case .outdatedReturn:
            return "Outdated Return"
        }
    }
}
