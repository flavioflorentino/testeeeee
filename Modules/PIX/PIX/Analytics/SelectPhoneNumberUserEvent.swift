import AnalyticsModule

enum SelectPhoneNumberUserEvent: AnalyticsKeyProtocol {
    case userNavigated(permission: PhonebookPermission)
    case phoneBookError(type: PhoneBookErrorType)
    case userSelected
    
    private var name: String {
        switch self {
        case .userNavigated:
            return "Pix User Navigated"
        case .phoneBookError:
            return "Pix Phonebook Error"
        case .userSelected:
            return "Phonebook User Selected"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .userNavigated(permission):
            return ["from": "phonebook", "to": "pix_cellphone", "phonebook_permission": permission.rawValue]
        case let .phoneBookError(type):
            return ["error_type": type.rawValue]
        case .userSelected:
            return [:]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.eventTracker])
    }
}

enum PhoneBookErrorType: String {
    case notFound = "not_found"
    case noContact = "no_contact"
}

enum PhonebookPermission: String {
    case allowed
    case denied
}
