import AnalyticsModule

public enum PixPaymentOrigin: String {
    case cpf = "pix_cpf"
    case cnpj = "pix_cnpj"
    case phone = "pix_cellphone"
    case email = "pix_email"
    case random = "pix_randomkey"
    case qrcode = " pix_qrcode"
    case manual = "pix_manual"
    case `return` = "pix_return"
    case copyPaste = "pix_copy_and_paste"
}

public enum PixPaymentUserEvent: AnalyticsKeyProtocol {
    case transactionViewed(origin: PixPaymentOrigin)
    case didTapInstallments
    case didChangePaymentMethod

    private var name: String {
        switch self {
        case .transactionViewed:
            return "Checkout Screen Viewed"
        case .didTapInstallments:
            return "Installment Accessed"
        case .didChangePaymentMethod:
            return "Checkout Payment Method Accessed"
        }
    }

    private var properties: [String: Any] {
        switch self {
        case let .transactionViewed(origin):
            return ["origin": origin.rawValue]
        case .didChangePaymentMethod:
            return ["alert": "change_unavailable"]
        case .didTapInstallments:
            return ["alert": "installment_unavailable"]
        }
    }

    private var providers: [AnalyticsProvider] {
        [.firebase, .mixPanel]
    }

    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
