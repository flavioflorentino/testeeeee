import AnalyticsModule

private extension KeyType {
    var typeSelectedEvent: String {
        switch self {
        case .cpf, .cnpj:
            return "cpf_cnpj"
        case .phone:
            return "cellphone"
        case .email:
            return "email"
        case .random:
            return "random_key"
        }
    }
    
    var forwardedEvent: String {
        switch self {
        case .cpf:
            return "cpf"
        case .cnpj:
            return "cnpj"
        case .phone:
            return "cellphone"
        case .email:
            return "email"
        case .random:
            return "random_key"
        }
    }
}

enum KeySelectorUserEvent: AnalyticsKeyProtocol {
    case userNavigated
    case typeSelected(item: KeyType)
    case errorViewed
    case forwarded(keyType: KeyType)
    
    private var name: String {
        switch self {
        case .userNavigated:
            return "User Navigated"
        case .typeSelected:
            return "Send Keys Type Selected"
        case .errorViewed:
            return "Type Error Viewed"
        case .forwarded:
            return "Send Keys Forwarded"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .userNavigated:
            return ["from": "pix_hub", "to": "send_keys_screen"]
        case let .typeSelected(type):
            return ["method": type.typeSelectedEvent]
        case .errorViewed:
            return ["error_type": "invalid_format"]
        case let .forwarded(keyType):
            return ["type": keyType.forwardedEvent]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("Pix \(name)", properties: properties, providers: [.mixPanel, .appsFlyer, .firebase])
    }
}
