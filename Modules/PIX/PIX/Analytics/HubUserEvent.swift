import AnalyticsModule

extension HubItemType {
    var analyticsMethod: String {
        switch self {
        case .keySelector:
            return "send_keys"
        case .qrCode:
            return "send_qrcode"
        case .manualInsert:
            return "send_manual"
        case .receivePayment:
            return "receive_payment"
        case .keyManagement:
            return "manage_keys"
        case .copyPaste:
            return "send_copy_and_paste"
        case .dailyLimit:
            return "daily_limit"
        }
    }
}

enum HubUserEvent: AnalyticsKeyProtocol {
    case userNavigated(origin: String)
    case itemSelected(item: HubItemType)
    
    private var name: String {
        switch self {
        case .userNavigated:
            return "User Navigated"
        case .itemSelected:
            return "Item Selected"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .userNavigated(origin):
            return ["from": origin, "to": "pix_hub"]
        case let .itemSelected(item):
            return ["method": item.analyticsMethod]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("Pix \(name)", properties: properties, providers: [.mixPanel, .appsFlyer, .firebase])
    }
}
