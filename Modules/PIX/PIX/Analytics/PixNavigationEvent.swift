import AnalyticsModule
import Foundation

public enum PixNavigationEvent: AnalyticsKeyProtocol {
    case userNavigated(from: PixUserNavigation, to: PixUserNavigation)
    
    private var properties: [String: Any] {
        switch self {
        case let .userNavigated(origin, destination):
            return ["from": origin.rawValue, "to": destination.rawValue]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }
    
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("Pix User Navigated", properties: properties, providers: providers)
    }
}
