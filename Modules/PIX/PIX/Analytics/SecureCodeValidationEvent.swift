import AnalyticsModule

extension SecureCodeValidationEvent {
    enum ErrorPresentationStyle: String {
        case textfield = "text_field"
        case dialog
    }
}

enum SecureCodeValidationEvent: AnalyticsKeyProtocol {
    case viewed(type: SecurityCodeValidationType)
    case resendCodeTapped(type: SecurityCodeValidationType)
    case codeInputed(type: SecurityCodeValidationType)
    case errorPresented(message: String, type: SecurityCodeValidationType, presentation: ErrorPresentationStyle)
    
    private var name: String {
        switch self {
        case .viewed:
            return "Viewed"
        case .resendCodeTapped:
            return "Code Resend Tapped"
        case .codeInputed:
            return "Code Typed"
        case .errorPresented:
            return "Error"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .viewed(type):
            return ["in": type.rawValue]
        case let .resendCodeTapped(type):
            return ["in": type.rawValue]
        case let .codeInputed(type):
            return ["in": type.rawValue]
        case let .errorPresented(message, type, presentation):
            return ["error_feedback": message, "in": type.rawValue, "type": presentation.rawValue]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.firebase, .mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("Pix Security Validation \(name)", properties: properties, providers: providers)
    }
}
