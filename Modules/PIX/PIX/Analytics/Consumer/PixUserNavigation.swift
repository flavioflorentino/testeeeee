import Foundation

public enum PixUserNavigation: String {
    case undefined
    
    // External
    case suggestions
    case cardsOpportunitiesFeed = "cards_opportunities_feed"
    case notification
    case settings
    case deeplink
    case withdraw
    case home
    case paymentRequest = "pix_cobrar"
    
    // Internal
    case hub
    case keyAuthentication = "key_authentication"
    case keyEmailVerification = "key_email_verification"
    case keyPhoneVerification = "key_phone_verification"
    case dialogUpdateRegistration = "dialog_update_registration"
    case keyRegistrationInProgress = "key_registration_in_progress"
    case registeredKey = "registered_key"
    case dialogPortability = "dialog_portability"
    case dialogClaimUse = "dialog_claim_use"
    case keyManagementScreen = "key_management_screen"
    case keyPortabilityRequestReceived = "key_portability_request_received"
    case keyClaimReceived = "key_claim_received"
    case keyWasClaimed = "key_was_claimed"
    case keyPortabilityInProgress = "key_portability_in_progress"
    case keyPortabilityCompleted = "key_portability_completed"
    case keyPortabilityNotCompleted = "key_portability_not_completed"
    case keyPortabilityDialog = "key_portability_dialog"
    case keyPortabilityDonorCompleted = "key_portability_donor_completed"
    case keyPortabilityDonorNotCompleted = "key_portability_donor_not_completed"
    case revalidationEmailVerification = "revalidation_email_verification"
    case revalidationPhoneVerification = "revalidation_phone_verification"
    case revalidationNotCompleted = "revalidation_not_completed"
    case revalidationCompleted = "revalidation_completed"
    case keyClaimAcceptedInProgress = "key_claim_accepted_in_progress"
    case keyClaimAcceptedNotCompleted = "key_claim_accepted_not_completed"
    case revalidationInProgress = "revalidation_in_progress"
    case keyClaimAcceptedPermitted = "key_claim_accepted_permitted"
    case keyDeclinedClaim = "key_declined_claim"
    case keyClaimCompleted = "key_claim_completed"
    case keyClaimInProgress = "key_claim_in_progress"
    case keyExclusionDialog = "key_exclusion_dialog"
    case keyExclusionInProgress = "key_exclusion_in_progress"
    case identityValidation = "identity_validation"
    case receive
    case keyClaimAvailableNeedsValidation = "key_claim_available_needs_validation"
    case keyClaimVerificationCompleted = "key_claim_verification_completed"
    case keyClaimVerificationNotCompleted = "key_claim_verification_not_completed"
    case keyClaimTimeValidationOver = "key_claim_time_validation_over"
    case bottomSheetPixKey = "bottom_sheet_pix_key"
    case shareKey = "share_key"
    case cancelClaimOrder = "cancel_claim_order"
    case cancelClaimOrderInProgress = "cancel_claim_order_in_progress"
    case cancelPortabilityOrder = "cancel_portability_order"
    case cancelPortabilityOrderInProgress = "cancel_portability_order_in_progress"
    case keyAuthenticationExclusion = "key_authentication_exclusion"
    case keyAuthenticationPortability = "key_authentication_portability"
    case copyKey = "copy_key"
    case hubScreen = "pix_hub"
    case sendQrCodeScreen = "send_qrcode_screen"
    case sendCopyPasteScreen = "send_copy_and_paste_screen"
    case sendQrCodeHowItWorks = "send_qrcode_how_it_works"
    
    // swiftlint:disable cyclomatic_complexity function_body_length
    init(_ feedbackType: FeedbackViewType) {
        switch feedbackType {
        case .portabilityDonorReceived:
            self = .keyPortabilityRequestReceived
        case .portabilityDonorInProgress:
            self = .keyPortabilityInProgress
        case .portabilityDonorCompleted:
            self = .keyPortabilityDonorCompleted
        case .portabilityDonorRefused:
            self = .keyPortabilityDonorNotCompleted
        case .portabilityInProgress:
            self = .keyPortabilityInProgress
        case .portabilityCompleted:
            self = .keyPortabilityCompleted
        case .portabilityRefused:
            self = .keyPortabilityNotCompleted
        case .keyRecordRegistered:
            self = .registeredKey
        case .keyRecordInProgress:
            self = .keyRegistrationInProgress
        case .claimDonorRequested:
            self = .keyWasClaimed
        case .claimDonorReceived:
            self = .keyClaimReceived
        case .claimDonorRevalidationNotCompleted:
            self = .revalidationNotCompleted
        case .claimDonorRevalidationCompleted:
            self = .revalidationCompleted
        case .claimDonorRevalidationInProgress:
            self = .revalidationInProgress
        case .claimDonorNotCompleted:
            self = .keyClaimAcceptedNotCompleted
        case .claimDonorInProgress:
            self = .keyClaimAcceptedInProgress
        case .claimDonorPermitted:
            self = .keyClaimAcceptedPermitted
        case .claimRefused:
            self = .keyDeclinedClaim
        case .claimCompleted:
            self = .keyClaimCompleted
        case .claimInProgress:
            self = .keyClaimInProgress
        case .pendingKeyDeletion:
            self = .keyExclusionInProgress
        case .claimNewValidationKeyAvailable:
            self = .keyClaimAvailableNeedsValidation
        case .claimNewValidationCompleted:
            self = .keyClaimVerificationCompleted
        case .claimNewValidationNotCompleted:
            self = .keyClaimVerificationNotCompleted
        case .claimNewValidationDeadlineFinished:
            self = .keyClaimTimeValidationOver
        case .claimKeyCancel:
            self = .cancelClaimOrder
        case .claimCancellationInProgress:
            self = .cancelClaimOrderInProgress
        case .portabilityKeyCancel:
            self = .cancelPortabilityOrder
        case .portabilityCancellationInProgress:
            self = .cancelPortabilityOrderInProgress
        default:
            self = .undefined
        }
    }
    
    init(validationOf keyType: PixKeyType) {
        switch keyType {
        case .phone:
            self = .revalidationPhoneVerification
        case .email:
            self = .revalidationEmailVerification
        default:
            self = .undefined
        }
    }
}
