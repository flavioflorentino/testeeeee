import AnalyticsModule
import Foundation

enum ModalRegistrationEvent: AnalyticsKeyProtocol {
    case dialogInteracted(action: Action, title: String)
    
    private var name: String {
        switch self {
        case .dialogInteracted:
            return "Pix Dialog Interacted"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .dialogInteracted(action, title):
            return ["action": action.rawValue, "title": title]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
    
    enum Action: String {
        case register, close
    }
}
