import Foundation
import AnalyticsModule

enum ReceiveConsumerEvent: AnalyticsKeyProtocol {
    case changeReceivingKey
    case qrCodeShared
    case createCustomQRCode
    case userNavigated(origin: PIXReceiveOrigin, screen: ReceiveScreen)
    case customQRCodeCreated(id: String?, description: String?, value: String?)
    case customQRCodeCreationError
    case customQRCodeShared
    case customQRCodeSaved
    case receiveNotValid(type: ReceiveNotValidType)
    
    private var name: String {
        switch self {
        case .changeReceivingKey:
            return "Change Receiving Key"
        case .qrCodeShared:
            return "QR Code Shared"
        case .createCustomQRCode:
            return "Create Custom QR Code Accessed"
        case .userNavigated:
            return "User Navigated"
        case .customQRCodeCreated:
            return "Custom QR Code Created"
        case .customQRCodeCreationError:
            return "Custom QR Code Creation Error"
        case .customQRCodeShared:
            return "Custom QR Code Shared"
        case .customQRCodeSaved:
            return "Custom Qr Code Saved"
        case .receiveNotValid:
            return "Receive Not Valid Button Clicked"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .userNavigated(origin, screen):
            return ["from": origin.rawValue, "to": screen.rawValue]
        case let .customQRCodeCreated(id, description, value):
            return [
                "id": id ?? "",
                "description": description ?? "",
                "value": value ?? ""
            ]
        case .customQRCodeCreationError:
            return ["error_type": "communication_problem"]
        case let .receiveNotValid(type):
            return ["type": type.rawValue]
        default:
            return [:]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("Pix \(name)", properties: properties, providers: [.mixPanel])
    }
}

public enum PIXReceiveOrigin: String {
    case pixHub = "pix_hub"
    case paymentRequestHub = "hub_cobrar"
    case deeplink = "deeplink"
}

enum ReceiveScreen: String {
    case registerKey = "pix_receive_no_keys"
    case identityValidation = "pix_receive_invalid_identity"
    case mainQRCode = "pix_receive_hub"
    case customizeQRCode = "pix_custom_qr_code"
}

enum ReceiveNotValidType: String {
    case registerKey = "register_key"
    case identity = "validate_identity"
    
    init(action: ReceiveWallDisplayAction) {
        switch action {
        case .identityNotValidated:
            self = .identity
        case .noKeysRegistered:
            self = .registerKey
        }
    }
}
