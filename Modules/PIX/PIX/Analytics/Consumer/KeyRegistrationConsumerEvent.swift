import AnalyticsModule

enum KeyRegistrationConsumerEvent: AnalyticsKeyProtocol {
    case keysViewed
    case keyAccessed(type: PixConsumerKeyType, section: PixKeySection)
    
    private var name: String {
        switch self {
        case .keysViewed:
            return "Keys Management Screen Viewed"
        case .keyAccessed:
            return "Key Accessed"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .keyAccessed(type, section):
            return ["item_name": type.rawValue, "section": section.value]
        default:
            return [:]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("Pix \(name)", properties: properties, providers: providers)
    }
}

extension PixKeySection {
    var value: String {
        switch self {
        case .registeredKey:
            return "registered_key"
        case .unregisteredKey:
            return "available_key"
        }
    }
}
