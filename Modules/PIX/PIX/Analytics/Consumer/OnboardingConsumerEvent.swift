import AnalyticsModule

enum OnboardingConsumerEvent: AnalyticsKeyProtocol {
    case viewed(WelcomeScreenDescription)
    case closed(WelcomeScreenDescription)
    
    private var name: String {
        switch self {
        case .viewed:
            return "Viewed"
        case .closed:
            return "Closed"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .viewed(pageDescripiton),
             let .closed(pageDescripiton):
            return ["in": pageDescripiton.rawValue]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PIX Onboarding \(name)", properties: properties, providers: providers)
    }
}

extension OnboardingConsumerEvent {
    enum WelcomeScreenDescription: String {
        case picpayAndPix = "picpay_and_pix"
        case keyRegisterExplanation = "key_register_explanation"
        case explanationNumberOfKeys = "explanation_number_of_keys"
        case none = ""
        
        init(pageIndex: Int) {
            switch pageIndex {
            case 0:
                self = .picpayAndPix
            case 1:
                self = .keyRegisterExplanation
            case 2:
                self = .explanationNumberOfKeys
            default:
                self = .none
            }
        }
    }
}
