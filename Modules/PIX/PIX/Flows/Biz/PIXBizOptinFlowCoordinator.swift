import FactorAuthentication
import UI
import UIKit

public enum PIXBizOptinAction: Equatable {
    case onboarding
    case keyManager(input: KeyManagerInputAction)
    case createKey(userInfo: KeyManagerBizUserInfo, key: CreateKey, hash: String? = nil)
    case createRandomKey
    case feedback(type: FeedbackViewType)
    case keyDescription( userInfo: KeyManagerBizUserInfo, createKey: CreateKey)
}

protocol PIXBizOptinFlowCoordinating: Coordinating {
    func perform(action: PIXBizOptinAction)
}

final class PIXBizOptinFlowCoordinator: PIXBizOptinFlowCoordinating {
    typealias Dependencies = HasBizAuth
    private let dependencies: Dependencies
    
    var childViewController: [UIViewController] = []
    var viewController: UIViewController?
    var navigationController: UINavigationController?
    var userInfo: KeyManagerBizUserInfo?
    private var tfaDelegate: FactorAuthenticationDelegate?
    private var coordinatorTFA: FactorAuthenticationCoordinator?
    
    init(from navigationController: UINavigationController?,
         userInfo: KeyManagerBizUserInfo?,
         dependencies: Dependencies = DependencyContainer()) {
        self.navigationController = navigationController
        self.userInfo = userInfo
        self.dependencies = dependencies
    }
    
    func perform(action: PIXBizOptinAction) {
        let controller: UIViewController
        
        switch action {
        case .onboarding:
            controller = KeyManagementWelcomeFactory.make(.biz(didReceiveOnboardingOutput), userInfo: userInfo)
        case .keyManager(let inputAction):
            if let keyManager = findKeyManagerInStack() {
                navigationController?.popToViewController(keyManager, animated: true)
                keyManager.input(action: inputAction)
                return
            }
            controller = KeyManagerBizFactory.make(userInfo: nil, keyManagerOuput: didReceiveKeyManagerOutput, inputAction: inputAction)
        case let .keyDescription(userInfo, createKey):
            controller = KeyDescriptionFactory.make(userInfo: userInfo, createKey: createKey, output: didReceiveKeyFromKeyDescription)
        case let .createKey(userInfo, key, hash):
            controller = CreateBizKeyFactory.make(with: userInfo, key: key, authenticationHash: hash, createKeyOutput: handleCreateKeyOutput)
        case .feedback(let type):
            openFeedbackScreen(with: type)
            return
        default:
            controller = UIViewController()
        }
        controller.hidesBottomBarWhenPushed = true
        if navigationController == nil {
            presentNavigation(with: controller)
            return
        }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    private func presentNavigation(with viewController: UIViewController) {
        let navigation = UINavigationController(rootViewController: viewController)
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
              while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                   }
            topController.present(navigation, animated: true, completion: nil)
        }
        navigationController = navigation
    }
    
    private func openFeedbackScreen(with type: FeedbackViewType) {
        let feedback = FeedbackFactory.make(with: type, delegate: self)
        switch type {
        case .portabilityDonorReceived,
             .claimDonorReceived,
             .claimDonorRevalidationNotCompleted,
             .claimDonorNotCompleted,
             .claimDonorPendent,
             .claimDonorInProgress,
             .portabilityDonorInProgress,
             .claimKeyAccepted,
             .unauthorized:
            navigationController?.pushViewController(feedback, animated: true)
        default:
            navigationController?.present(feedback, animated: true)
        }
    }
    
    private func didReceiveKeyFromKeyDescription(createKey: CreateKey, userInfo: KeyManagerBizUserInfo) {
        perform(action: .createKey(userInfo: userInfo, key: createKey))
    }
}

private extension PIXBizOptinFlowCoordinator {
    func handleCreateKeyOutput(with action: CreateBizKeyOutputAction) {
        popKeyManagerInStack()
        switch action {
        case let .pending(pixKey):
            let id = pixKey.registeredKeys.first?.id ?? ""
            perform(action: .feedback(type: .keyRecordInProgress(uuid: id)))
        case let .processed(_, pixKey):
            let id = pixKey.registeredKeys.first?.id ?? ""
            perform(action: .feedback(type: .keyRecordRegistered(uuid: id)))
        case let .claim(key):
            let claimKey = transform(createKey: key, type: .claim)
            perform(action: .keyManager(input: .claim(key: claimKey)))
        case let .portability(key):
            let claimKey = transform(createKey: key, type: .portability)
            perform(action: .keyManager(input: .portability(key: claimKey)))
        default:
            perform(action: .feedback(type: .keyRecordNotRegistered(uuid: "")))
        }
    }
    
    func popKeyManagerInStack() {
        if let keyManager = findKeyManagerInStack() {
            navigationController?.popToViewController(keyManager, animated: true)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    func transform(createKey: CreateKey, type: ClaimKeyType) -> ClaimKey {
        ClaimKey(userId: createKey.userId, userType: createKey.userType, keyValue: createKey.value, keyType: createKey.type, type: type)
    }
    
    func didReceiveKeyManagerOutput(action: KeyManagerBizOutputAction) {
        switch action {
        case .openInformation:
            perform(action: .onboarding)
        case let .openCreateKeyFlow(userInfo, key):
            perform(action: .createKey(userInfo: userInfo, key: key))
        case let .openTFACreateKeyFlow(_, key):
            var typeFlow: FactorAuthenticationFlow {
                switch key.type {
                case .email:
                    return .pixOptinEmail
                default:
                    return .pixOptinSMS
                }
            }
            
            startTFAFlow(input: .createKey(key), flowType: typeFlow)
        case let .openRandomCreateKeyFlow(userInfo, key):
            perform(action: .keyDescription(userInfo: userInfo, createKey: key))
        case let .openFeedback(feedbackType):
            perform(action: .feedback(type: feedbackType))
        case let .dismissPortabilityFeedbackAndShowError(error):
            navigationController?.popViewController(animated: true)
            perform(action: .keyManager(input: .error(description: error)))
        case let .updateUserInfoInKeyManager(userInfo):
            self.userInfo = userInfo
        }
    }
    
    func didReceiveOnboardingOutput(action: KeyManagementWelcomeAction) {
        switch action {
        case .close:
            navigationController?.dismiss(animated: true)
        case .finishOnboarding:
            perform(action: .keyManager(input: .none))
        case .tapCustomButton:
            break
        }
    }
    
    func findKeyManagerInStack() -> KeyManagerBizViewController? {
        navigationController?.viewControllers.first { viewController -> Bool in
            viewController is KeyManagerBizViewController
        } as? KeyManagerBizViewController
    }
    
    func startTFAFlow(input: TFAInput, flowType: FactorAuthenticationFlow) {
        guard let userInfo = userInfo else {
            return
        }
        let delegate = createTFADelegate(userInfo: userInfo, input: input)
        self.tfaDelegate = delegate
        if let keyManager = findKeyManagerInStack() {
            self.coordinatorTFA = FactorAuthenticationCoordinator(from: keyManager, delegate: delegate, dataSource: TFADataSource(flowType: flowType))
            self.coordinatorTFA?.start()
        }
    }
    
    private func createTFADelegate(userInfo: KeyManagerBizUserInfo, input: TFAInput) -> FactorAuthenticationDelegate {
            switch input {
            case let .claimDonor(uuid):
                return ClaimTFADelegate(uuid: uuid, output: didOutputClaimDonorTFADelegate)
            case let .createKey(key):
                return CreateKeyTFADelegate(key: key, output: didOutputCreateKeyTFADelegate)
            case let .revalidateClaimKeyAccepted(uuid):
                return ClaimTFADelegate(uuid: uuid, output: didOutputRevalidateClaimKeyAcceptedTFADelegate)
            }
    }
    
    func didOutputClaimDonorTFADelegate(output: TFADelegateOuputClaimKey) {
        switch output {
        case .factorAuthenticationWasClosed:
            popKeyManagerInStack()
        case let .factorClaimKeyAuthenticationCompleted(hash, uuid):
            navigationController?.popViewController(animated: true)
            if let keyManager = findKeyManagerInStack() {
                keyManager.input(action: .revalidateClaim(keyId: uuid))
            } else {
                perform(action: .keyManager(input: .error(description: "mobile - tela de gerenciamento de chaves não encontrada")))
            }
        }
    }
    
    func didOutputCreateKeyTFADelegate(output: TFADelegateOutputCreateKey) {
        switch output {
        case .factorAuthenticationWasClosed:
            popKeyManagerInStack()
        case let .factorCreateKeyAuthenticationCompleted(hash, key):
            guard let userInfo = userInfo else {
                return
            }
            perform(action: .createKey(userInfo: userInfo, key: key, hash: hash))
        }
    }

    func didOutputRevalidateClaimKeyAcceptedTFADelegate(output: TFADelegateOuputClaimKey) {
        switch output {
        case .factorAuthenticationWasClosed:
            popKeyManagerInStack()
        case let .factorClaimKeyAuthenticationCompleted(hash, uuid):
            navigationController?.popViewController(animated: true)
            if let keyManager = findKeyManagerInStack() {
                keyManager.input(action: .revalidateClaimKeyAccepted(keyId: uuid, hash: hash))
            } else {
                perform(action: .keyManager(input: .error(description: "mobile - tela de gerenciamento de chaves não encontrada")))
            }
        }
    }
}
extension PIXBizOptinFlowCoordinator: FeedbackDelegate {
    //swiftlint:disable cyclomatic_complexity function_body_length
    func didAction(action: FeedbackAction, feedbackType type: FeedbackViewType) {
        switch action {
        case .revalidateKey:
            guard case let .claimDonorReceived(uuid) = type,
                  let keyManagerViewController = self.findKeyManagerInStack() else {
                let errorDescription = "mobile - tela de gerenciamento de chaves não encontrados"
                perform(action: .keyManager(input: .error(description: errorDescription)))
                return
            }
            let keyType = keyManagerViewController.findKeyType(uuid: uuid)
            switch keyType {
            case .email:
                startTFAFlow(input: .claimDonor(uuid: uuid), flowType: .pixOptinEmail)
            case .phone:
                startTFAFlow(input: .claimDonor(uuid: uuid), flowType: .pixOptinSMS)
            default:
                keyManagerViewController.input(action: .revalidateClaim(keyId: uuid))
            }
        case .grantPortability:
            guard let keyManagerViewController = findKeyManagerInStack(),
                  case let FeedbackViewType.portabilityDonorReceived(uuid) = type else {
                perform(action: .keyManager(input: .error(description: "mobile - tela de gerenciamento de chaves não encontrada")))
                return
            }
            keyManagerViewController.input(action: .confirmPortability(keyId: uuid))
        case .tryAgain:
            guard case .tryAgain = action, let keyManagerViewController = self.findKeyManagerInStack() else {
                perform(action: .keyManager(input: .error(description: "mobile - tela de gerenciamento de chaves não encontrada")))
                return
            }
            if case let .claimDonorRevalidationNotCompleted(uuid) = type {
                switch keyManagerViewController.findKeyType(uuid: uuid) {
                case .email:
                    startTFAFlow(input: .claimDonor(uuid: uuid), flowType: .pixOptinEmail)
                case .phone:
                    startTFAFlow(input: .claimDonor(uuid: uuid), flowType: .pixOptinSMS)
                default:
                    keyManagerViewController.input(action: .revalidateClaim(keyId: uuid))
                }
                return
            }
            if case let .portabilityDonorRefused(uuid) = type {
                navigationController?.dismiss(animated: true) {
                    self.perform(action: .feedback(type: .portabilityDonorReceived(uuid: uuid)))
                }
                return
            }
            if case let .claimDonorNotCompleted(uuid) = type {
                keyManagerViewController.input(action: .confirmClaim(keyId: uuid))
                return
            }
            navigationController?.dismiss(animated: true) {
                keyManagerViewController.input(action: .tryAgain(feedbackType: type))
            }
        case .allowClaim:
            guard let keyManagerViewController = findKeyManagerInStack(),
                  case let FeedbackViewType.claimDonorReceived(uuid) = type else {
                perform(action: .keyManager(input: .error(description: "mobile - tela de gerenciamento de chaves não encontrada")))
                return
            }
            keyManagerViewController.input(action: .confirmClaim(keyId: uuid))
        case .close, .ignore, .tryPix:
            guard let keyManager = findKeyManagerInStack() else {
                return
            }
            if case let .portabilityDonorReceived(uuid) = type {
                keyManager.sendIgnorePortabilityEvent(uuid: uuid)
            }
            if type != .unauthorized {
                keyManager.input(action: .reload)
            }

            switch type {
            case .claimDonorPendent, .portabilityDonorInProgress, .claimDonorInProgress, .claimDonorRevalidationNotCompleted, .claimKeyAccepted:
                navigationController?.popToViewController(keyManager, animated: true)
            case .claimDonorReceived, .portabilityDonorReceived, .claimDonorNotCompleted:
                navigationController?.popViewController(animated: true)
            case .unauthorized:
                navigationController?.popToRootViewController(animated: true)
            default:
                navigationController?.dismiss(animated: true)
            }
        case .revalidateClaimKeyAccepted:
            guard case let .claimKeyAccepted(uuid) = type,
                  let keyManagerViewController = self.findKeyManagerInStack() else {
                perform(action: .keyManager(input: .error(description: "mobile - tela de gerenciamento de chaves não encontrada")))
                navigationController?.dismiss(animated: true)
                return
            }
            let keyType = keyManagerViewController.findKeyType(uuid: uuid)
            switch keyType {
            case .email:
                startTFAFlow(input: .revalidateClaimKeyAccepted(uuid: uuid), flowType: .pixOptinEmail)
            case .phone:
                startTFAFlow(input: .revalidateClaimKeyAccepted(uuid: uuid), flowType: .pixOptinSMS)
            default:
                break // Should not contain other cases
            }
        case .requestAgain:
            guard let keyManagerViewController = findKeyManagerInStack() else {
                return
            }
            keyManagerViewController.input(action: .reload)
            navigationController?.dismiss(animated: true)
        case .cancel:
            navigationController?.dismiss(animated: true)
        default:
            break
        }
    }
}
