import FactorAuthentication
import UI
import UIKit

public typealias PIXBizCashinOutput = (PIXBizCashinOutputAction) -> Void

public enum PIXBizCashinOutputAction: Equatable {
    case keyManager
}

public enum PIXBizCashinAction: Equatable {
    case onboarding
    case receive(keys: [RegisteredKey], customQRCode: PixQRCode? = nil)
    case needsRegistration
    case updateKeys
    case faq(viewController: UIViewController)
    case customizeQRCode(keys: [RegisteredKey])
}

protocol PIXBizCashinFlowCoordinating: Coordinating {
    func perform(action: PIXBizCashinAction)
}

final class PIXBizCashinFlowCoordinator: PIXBizCashinFlowCoordinating {
    typealias Dependencies = HasCustomerSupport
    private var dependencies: Dependencies

    var childViewController: [UIViewController] = []
    var viewController: UIViewController?
    var navigationController: UINavigationController
    var userInfo: KeyManagerBizUserInfo?
    private var onboardingPresented: Bool
    private var keys: [RegisteredKey] = []
    
    private let cashinOutput: PIXBizCashinOutput

    init(navigationController: UINavigationController,
         userInfo: KeyManagerBizUserInfo?,
         cashinOutput: @escaping PIXBizCashinOutput,
         onboardingPresented: Bool,
         dependencies: Dependencies = DependencyContainer()) {
        self.userInfo = userInfo
        self.navigationController = navigationController
        self.cashinOutput = cashinOutput
        self.dependencies = dependencies
        self.onboardingPresented = onboardingPresented
    }

    func perform(action: PIXBizCashinAction) {
        let controller: UIViewController
        switch action {
        case .onboarding:
            controller = KeyManagementWelcomeFactory.make(.bizReceivement(didOutputOnboarding), userInfo: userInfo)
        case .updateKeys:
            inputHub(action: .checkRegisteredKeys)
            return
        case let .receive(registeredKeys, qrCode):
            keys = registeredKeys
            if !onboardingPresented {
                perform(action: .onboarding)
                return
            }
            guard let userInfo = userInfo else {
                return
            }
            controller = ReceivePaymentsBizFactory.make(keys: registeredKeys,
                                                        customQRCode: qrCode,
                                                        userInfo: userInfo,
                                                        receiveOutput: didOutputReceivePayments)
        case .needsRegistration:
            controller = FeedbackFactory.make(with: .needsPixKeyRegistration, delegate: self)
        case .faq(let controller):
            dependencies.costumeSupportContract?.openFAQ(in: controller)
            return
        case let .customizeQRCode(keys):
            controller = CustomizeQRCodeFactory.make(keys: keys, output: didCustomQRCodeOutput)
        }
        controller.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(controller, animated: true)
    }
}

private extension PIXBizCashinFlowCoordinator {    
    func didOutputReceivePayments(action: ReceivePaymentsBizAction) {
        switch action {
        case let .customizeQRCode(keys):
            perform(action: .customizeQRCode(keys: keys))
        case let .faq(viewController):
            perform(action: .faq(viewController: viewController))
        }
    }

    func didOutputOnboarding(action: KeyManagementWelcomeAction) {
        switch action {
        case .close:
            navigationController.popViewController(animated: true)
        case .finishOnboarding:
            onboardingPresented = true
            if !keys.isEmpty {
                perform(action: .receive(keys: keys))
                return
            }
            perform(action: .needsRegistration)
        case .tapCustomButton:
            onboardingPresented = true
            perform(action: .faq(viewController: navigationController))
        }
    }
    
    func inputHub(action: HubBizInputAction) {
        if let hubController = findHubInStack() as? HubBizViewController {
            hubController.input(action: action)
        }
    }
    
    func didCustomQRCodeOutput(action: CustomizeQRCodeAction) {
        switch action {
        case .keyManager:
            guard let hubController = findHubInStack() else { return }
            navigationController.popToViewController(hubController, animated: true)
            cashinOutput(.keyManager)
        case let .customizedQRCode(qrCode, keys):
            perform(action: .receive(keys: keys, customQRCode: qrCode))
        }
    }
    
    func findHubInStack() -> UIViewController? {
        navigationController.viewControllers.first { $0 is HubBizViewController }
    }
}

extension PIXBizCashinFlowCoordinator: FeedbackDelegate {
    func didAction(action: FeedbackAction, feedbackType: FeedbackViewType) {
        if case .needsPixKeyRegistration = feedbackType {
            switch action {
            case .ignore, .close, .notNow:
                navigationController.popViewController(animated: true)
            case .registerKey:
                navigationController.popViewController(animated: true)
                cashinOutput(.keyManager)
            default:
                break
            }
        }
    }
}
