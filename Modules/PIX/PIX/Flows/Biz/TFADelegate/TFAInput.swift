public enum TFAInput {
    case createKey(CreateKey)
    case claimDonor(uuid: String)
    case revalidateClaimKeyAccepted(uuid: String)
}
