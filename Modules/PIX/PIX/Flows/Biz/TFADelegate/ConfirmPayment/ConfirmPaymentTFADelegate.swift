import FactorAuthentication

enum TFADelegateOutputConfirmPayment {
    case factorAuthenticationWasClosed
    case factorClaimKeyAuthenticationCompleted(hash: String)
}

final class ConfirmPaymentTFADelegate: FactorAuthenticationDelegate {
    let output: (TFADelegateOutputConfirmPayment) -> Void
    
    init(output: @escaping (TFADelegateOutputConfirmPayment) -> Void) {
        self.output = output
    }
    
    func factorAuthenticationWasClosed() {
        output(.factorAuthenticationWasClosed)
    }
    
    func factorAuthenticationWasCompleted(with hash: String) {
        output(.factorClaimKeyAuthenticationCompleted(hash: hash))
    }
}
