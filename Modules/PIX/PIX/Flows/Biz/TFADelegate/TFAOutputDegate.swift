protocol TFAOutputDelegate: AnyObject {
    func didTFAOutput(output: TFADelegateOutputConfirmPayment)
}
