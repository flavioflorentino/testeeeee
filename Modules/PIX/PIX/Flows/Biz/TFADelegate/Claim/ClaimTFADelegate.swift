import FactorAuthentication

enum TFADelegateOuputClaimKey {
    case factorAuthenticationWasClosed
    case factorClaimKeyAuthenticationCompleted(hash: String, uuid: String)
}

final class ClaimTFADelegate: FactorAuthenticationDelegate {
    let output: (TFADelegateOuputClaimKey) -> Void
    let uuid: String
    
    init(uuid: String, output: @escaping (TFADelegateOuputClaimKey) -> Void) {
        self.output = output
        self.uuid = uuid
    }
    
    func factorAuthenticationWasClosed() {
        output(.factorAuthenticationWasClosed)
    }
    
    func factorAuthenticationWasCompleted(with hash: String) {
        output(.factorClaimKeyAuthenticationCompleted(hash: hash, uuid: uuid))
    }
}
