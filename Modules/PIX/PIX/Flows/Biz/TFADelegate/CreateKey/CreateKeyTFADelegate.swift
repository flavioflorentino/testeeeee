import FactorAuthentication

enum TFADelegateOutputCreateKey {
    case factorAuthenticationWasClosed
    case factorCreateKeyAuthenticationCompleted(hash: String, key: CreateKey)
}

class CreateKeyTFADelegate: FactorAuthenticationDelegate {
    let output: (TFADelegateOutputCreateKey) -> Void
    let key: CreateKey
    
    init(key: CreateKey, output: @escaping (TFADelegateOutputCreateKey) -> Void) {
        self.output = output
        self.key = key
    }
    
    func factorAuthenticationWasClosed() {
        output(.factorAuthenticationWasClosed)
    }
    
    func factorAuthenticationWasCompleted(with hash: String) {
        output(.factorCreateKeyAuthenticationCompleted(hash: hash, key: key))
    }
}
