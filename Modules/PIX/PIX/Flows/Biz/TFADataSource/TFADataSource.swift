import FactorAuthentication

class TFADataSource: FactorAuthenticationDataSource {
    let flowType: FactorAuthenticationFlow
    
    var flow: FactorAuthenticationFlow {
        flowType
    }
    
    init(flowType: FactorAuthenticationFlow) {
        self.flowType = flowType
    }
}
