import FactorAuthentication
import Core
import UI
import UIKit

public typealias PIXBizCashoutOutput = (PIXBizCashoutOutputAction) -> Void

public enum PIXBizCashoutOutputAction {
    case receipt(transactionId: String?, isRefundAllowed: Bool, data: ReceiptResponse?)
}

public enum PIXBizCashoutAction: Equatable {
    case keys
    case qrCode
    case bankAccount
    case copyPaste
    case onboarding
    case faq(viewController: UIViewController)
    case refund(transactionId: String)
    
    var typeString: String {
        switch self {
        case .refund:
            return "return"
        case .keys:
            return "keys"
        case .qrCode:
            return "qr_code"
        case .bankAccount:
            return "back_account"
        case .copyPaste:
            return "copy_paste"
        default:
            return ""
        }
    }
}

protocol PIXBizCashoutFlowCoordinating: Coordinating {
    func perform(action: PIXBizCashoutAction)
}

final class PIXBizCashoutFlowCoordinator: PIXBizCashoutFlowCoordinating {
    typealias Dependencies = HasCustomerSupport & HasKVStore
    private var dependencies: Dependencies
    
    var childViewController: [UIViewController] = []
    var viewController: UIViewController?
    var navigationController: UINavigationController
    private var childNavigationController: UINavigationController?
    private var userInfo: KeyManagerBizUserInfo?
    private let cashoutOutput: PIXBizCashoutOutput
    private var tfaOutput: TFAOutputDelegate?
    private var coordinatorTFA: FactorAuthenticationCoordinator?
    private var tfaDelegateManager: FactorAuthenticationDelegate?
    private let container = DependencyContainer()
    private var onboardingPresented: Bool
    private var actualAction: PIXBizCashoutAction?
    private let cashoutActions: [PIXBizCashoutAction] = [.keys, .copyPaste, .bankAccount, .qrCode]
     
    init(navigationController: UINavigationController,
         cashoutOutput: @escaping PIXBizCashoutOutput,
         userInfo: KeyManagerBizUserInfo?,
         onboardingPresented: Bool,
         dependencies: Dependencies = DependencyContainer()) {
        self.userInfo = userInfo
        self.navigationController = navigationController
        self.cashoutOutput = cashoutOutput
        self.onboardingPresented = onboardingPresented
        self.dependencies = dependencies
    }

    func perform(action: PIXBizCashoutAction) {
        updateActualAction(action: action)
        switch action {
        case let .refund(transactionId):
            let flowCoordinator = PIXBizRefundFlowCoordinator(navigationController: navigationController,
                                                              childCashoutOutput: didOutputFlowCoordinator,
                                                              userInfo: userInfo)
            tfaOutput = flowCoordinator
            flowCoordinator.perform(action: .loadPaymentValue(transactionId: transactionId))
        case .keys:
            guard onboardingPresented else {
                perform(action: .onboarding)
                return
            }
            let flowCoordinator = PIXBizKeysCashoutFlowCoordinator(navigationController: navigationController,
                                                                   childCashoutOutput: didOutputFlowCoordinator,
                                                                   userInfo: userInfo)
            tfaOutput = flowCoordinator
            flowCoordinator.perform(action: .keySelector)
        case .qrCode:
            guard onboardingPresented else {
                perform(action: .onboarding)
                return
            }
            let flowCoordinator = PIXBizQRCodeFlowCoordinator(navigationController: navigationController,
                                                              childCashoutOutput: didOutputFlowCoordinator,
                                                              userInfo: userInfo,
                                                              dependencies: container)
            tfaOutput = flowCoordinator
            flowCoordinator.perform(action: .qrCode)
        case .bankAccount:
            guard onboardingPresented else {
                perform(action: .onboarding)
                return
            }
            let flowCoordinator = PIXBizBankAccountFlowCoordinator(navigationController: navigationController,
                                                                   childCashoutOutput: didOutputFlowCoordinator,
                                                                   userInfo: userInfo)
            tfaOutput = flowCoordinator
            flowCoordinator.perform(action: .manualInsertion)
        case .copyPaste:
            guard onboardingPresented else {
                perform(action: .onboarding)
                return
            }
            let flowCoordinator = PIXBizCopyPasteFlowCoordinator(navigationController: navigationController,
                                                                 childCashoutOutput: didOutputFlowCoordinator,
                                                                 userInfo: userInfo)
            tfaOutput = flowCoordinator
            flowCoordinator.perform(action: .copyPaste)
        case .onboarding:
            let controller = KeyManagementWelcomeFactory.make(.bizCashout(didOutputOnboarding), userInfo: userInfo)
            navigationController.pushViewController(controller, animated: true)
        case .faq(let controller):
            dependencies.costumeSupportContract?.openFAQ(in: controller)
        }
    }
    
    private func updateActualAction(action: PIXBizCashoutAction) {
        if cashoutActions.contains(action) {
            actualAction = action
        }
    }
    
    private func didOutputFlowCoordinator(action: PIXBizChildCashoutOutputAction) {
        switch action {
        case let .receipt(transactionId, isRefundAllowed, data):
            cashoutOutput(.receipt(transactionId: transactionId, isRefundAllowed: isRefundAllowed, data: data))
        case let .tfa(topViewController, tfaFlow):
            startTFAFlow(topViewController: topViewController,
                         flowType: tfaFlow)
        }
    }
    
    private func startTFAFlow(topViewController: UIViewController?,
                              flowType: FactorAuthenticationFlow) {
        let delegate = ConfirmPaymentTFADelegate(output: didTFAOutput)
        self.tfaDelegateManager = delegate
        if let confirm = topViewController {
            self.coordinatorTFA = FactorAuthenticationCoordinator(
                from: confirm,
                delegate: delegate,
                dataSource: TFADataSource(flowType: flowType)
            )
            self.coordinatorTFA?.start()
        }
    }
    
    func didTFAOutput(output: TFADelegateOutputConfirmPayment) {
        tfaOutput?.didTFAOutput(output: output)
    }
    
    func didOutputOnboarding(action: KeyManagementWelcomeAction) {
        switch action {
        case .close:
            navigationController.popViewController(animated: true)
        case .finishOnboarding:
            dependencies.kvStore.setBool(true, with: KVKey.isPixCashoutWelcomeVisualized)
            onboardingPresented = true
            navigationController.popViewController(animated: true)
            guard let action = actualAction else {
                return
            }
            perform(action: action)
        case .tapCustomButton:
            dependencies.kvStore.setBool(true, with: KVKey.isPixCashoutWelcomeVisualized)
            perform(action: .faq(viewController: navigationController))
        }
    }
}
