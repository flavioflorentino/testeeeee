import UI
import UIKit

public enum PIXBizQRcodeAction: Equatable {
    case qrCode
    case loadPaymentValue(qrCode: String)
    case paymentValue(paymentAccountViewModel: PaymentAmountViewModel)
    case confirmPayment(input: ConfirmBizPaymentInputAction,
                        flowType: PaymentAmountFlowType,
                        value: Double,
                        pixAccount: PixAccount,
                        qrCodeAccount: PixQrCodeAccount)
}

final class PIXBizQRCodeFlowCoordinator: TFAOutputDelegate {
    typealias Dependencies = HasCustomerSupport
    private var dependencies: Dependencies
    
    private var navigationController: UINavigationController
    private var userInfo: KeyManagerBizUserInfo?
    private let childCashoutOutput: PIXBizChildCashoutOutput
    private let cashoutType = PIXBizCashoutAction.qrCode
    
    init(navigationController: UINavigationController,
         childCashoutOutput: @escaping PIXBizChildCashoutOutput,
         userInfo: KeyManagerBizUserInfo?,
         dependencies: Dependencies) {
        self.userInfo = userInfo
        self.navigationController = navigationController
        self.childCashoutOutput = childCashoutOutput
        self.dependencies = dependencies
    }
    
    func perform(action: PIXBizQRcodeAction) {
        let controller: UIViewController
        switch action {
        case let .loadPaymentValue(qrCode):
            controller = ReturnLoadingFactory.make(flowType: .qrCode(code: qrCode),
                                                   output: didReturnLoadingOutput)
        case let .paymentValue(paymentAmountViewModel):
            controller = PaymentAmountFactory.makeSendAmount(
                paymentAmountViewModel: paymentAmountViewModel,
                paymentAmountBizOutput: didPaymentAmountOutput,
                userInfo: userInfo
            )
        case let .confirmPayment(input, flowType, value, pixAccount, qrCodeAccount):
            if let confirmPaymentController = findConfirmPaymentInStack() {
                confirmPaymentController.input(action: input)
                return
            }
            guard let userInfo = userInfo else { return }
            
            switch flowType {
            case let .send(pixParams):
                let sendInfo = ConfirmBizPaymentSendInfo(
                    userInfo: userInfo,
                    pixParams: pixParams,
                    pixAccount: pixAccount,
                    value: value,
                    qrCodeAccount: qrCodeAccount)
                controller = ConfirmBizPaymentFactory.makeSend(
                    sendInfo: sendInfo,
                    confirmOutput: didOutputConfirmPayment
                )
            case let .refund(transactionId, maxRefundAmount, sendAmount):
                let refundInfo = ConfirmBizPaymentRefundInfo(
                    userInfo: userInfo,
                    transactionId: transactionId,
                    maxRefundAmount: maxRefundAmount,
                    sendAmount: sendAmount,
                    pixAccount: pixAccount,
                    value: value,
                    qrCodeAccount: qrCodeAccount)
                controller = ConfirmBizPaymentFactory.makeRefund(
                    refundInfo: refundInfo,
                    confirmOutput: didOutputConfirmPayment
                )
            }
        case .qrCode:
            controller = QRCodeScannerFactory.make(userInfo: userInfo, qrCodeOutput: didQRCodeOutput)
        }
        controller.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(controller, animated: true)
        dismissLoading(with: controller)
    }
    
    private func dismissLoading(with actualController: UIViewController) {
        if actualController is ReturnLoadingViewController {
            return
        }
        navigationController.viewControllers.removeAll(where: { $0 is ReturnLoadingViewController })
    }
    
    func didQRCodeOutput(output: QRCodeScannerAction) {
        switch output {
        case let .decode(code):
            perform(action: .loadPaymentValue(qrCode: code))
        case let .faq(controller):
            dependencies.costumeSupportContract?.openFAQ(in: controller)
        }
    }
    
    func didOutputConfirmPayment(action: ConfirmBizPaymentAction) {
        switch action {
        case .tfa:
            childCashoutOutput(.tfa(topViewController: findConfirmPaymentInStack(), flowTFA: .pixCashOut))
        case let .receipt(transactionId, data):
            navigationController.dismiss(animated: false) {
                self.childCashoutOutput(.receipt(transactionId: transactionId, isRefundAllowed: false, data: data))
            }
        }
    }
    
    func didTFAOutput(output: TFADelegateOutputConfirmPayment) {
        switch output {
        case .factorAuthenticationWasClosed:
            if let confirm = findConfirmPaymentInStack() {
                navigationController.popToViewController(confirm, animated: true)
            } else {
                navigationController.popViewController(animated: true)
            }
        case let .factorClaimKeyAuthenticationCompleted(hash):
            if let confirm = findConfirmPaymentInStack() {
                confirm.input(action: .confirmPayment(hash: hash))
            }
        }
    }
    
    func didReturnLoadingOutput(action: ReturnLoadingAction) {
        switch action {
        case let .successQRCodeSend(pixParams,
                                    amount,
                                    availableAmount,
                                    pixAccount,
                                    qrPixAccount,
                                    userInfo):
            self.userInfo = userInfo
            guard let qrPixAccount = qrPixAccount, let pixAccount = pixAccount else {
                navigationController.popViewController(animated: true)
                return
            }
            let viewModel = PaymentAmountViewModel(pixAccount: pixAccount,
                                                   availableAmount: availableAmount,
                                                   flowType: .send(pixParams: pixParams),
                                                   qrCodePixAccount: qrPixAccount,
                                                   amount: amount)
            perform(action: .paymentValue(paymentAccountViewModel: viewModel))
        case .error:
            navigationController.popViewController(animated: true)
        default:
            break
        }
    }
    
    func findConfirmPaymentInStack() -> ConfirmBizPaymentViewController? {
        navigationController.viewControllers.first(
            where: { $0 is ConfirmBizPaymentViewController }
        ) as? ConfirmBizPaymentViewController
    }
    
    func didPaymentAmountOutput(action: PaymentAmountAction) {
        guard case let .paymentConfirmation(pixAccount, sendAmount, flowType, qrCodeAccount) = action else { return }
        guard let qrCodePixAccount = qrCodeAccount else { return }
        perform(action: .confirmPayment(input: .none,
                                        flowType: flowType,
                                        value: sendAmount,
                                        pixAccount: pixAccount,
                                        qrCodeAccount: qrCodePixAccount))
    }
}
