import UI
import UIKit

public enum PIXBizCopyPasteCashoutAction: Equatable {
    case copyPaste
    case loadPaymentValue(code: String)
    case paymentValue(paymentAccountViewModel: PaymentAmountViewModel)
    case confirmPayment(input: ConfirmBizPaymentInputAction,
                        flowType: PaymentAmountFlowType,
                        value: Double,
                        pixAccount: PixAccount,
                        qrCodeAccount: PixQrCodeAccount)
}

final class PIXBizCopyPasteFlowCoordinator: TFAOutputDelegate {
    private var navigationController: UINavigationController
    private var userInfo: KeyManagerBizUserInfo?
    private let childCashoutOutput: PIXBizChildCashoutOutput
    private let cashoutType = PIXBizCashoutAction.copyPaste
     
    init(navigationController: UINavigationController,
         childCashoutOutput: @escaping PIXBizChildCashoutOutput,
         userInfo: KeyManagerBizUserInfo?) {
        self.userInfo = userInfo
        self.navigationController = navigationController
        self.childCashoutOutput = childCashoutOutput
    }

    func perform(action: PIXBizCopyPasteCashoutAction) {
        let controller: UIViewController
        switch action {
        case .copyPaste:
            controller = CopyPasteFactory.make(copyPasteOutput: copyPasteOutput, userInfo: userInfo)
        case let .loadPaymentValue(code):
            controller = ReturnLoadingFactory.make(flowType: .copyPaste(code: code),
                                                   output: didReturnLoadingOutput)
        case .paymentValue(let paymentAmountViewModel):
            controller = PaymentAmountFactory.makeSendAmount(
                paymentAmountViewModel: paymentAmountViewModel,
                paymentAmountBizOutput: didPaymentAmountOutput,
                userInfo: userInfo
            )
        case let .confirmPayment(input, flowType, value, pixAccount, qrCodeAccount):
            if let confirmPaymentController = findConfirmPaymentInStack() {
                confirmPaymentController.input(action: input)
                return
            }
            guard let userInfo = userInfo else { return }
            
            switch flowType {
            case let .send(pixParams):
                let sendInfo = ConfirmBizPaymentSendInfo(
                    userInfo: userInfo,
                    pixParams: pixParams,
                    pixAccount: pixAccount,
                    value: value,
                    qrCodeAccount: qrCodeAccount)
                controller = ConfirmBizPaymentFactory.makeSend(
                    sendInfo: sendInfo,
                    confirmOutput: didOutputConfirmPayment
                )
            case let .refund(transactionId, maxRefundAmount, sendAmount):
                let refundInfo = ConfirmBizPaymentRefundInfo(
                    userInfo: userInfo,
                    transactionId: transactionId,
                    maxRefundAmount: maxRefundAmount,
                    sendAmount: sendAmount,
                    pixAccount: pixAccount,
                    value: value,
                    qrCodeAccount: qrCodeAccount)
                controller = ConfirmBizPaymentFactory.makeRefund(
                    refundInfo: refundInfo,
                    confirmOutput: didOutputConfirmPayment
                )
            }
        }
        controller.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(controller, animated: true)
        dismissLoading(with: controller)
    }
    
    private func dismissLoading(with actualController: UIViewController) {
        if actualController is ReturnLoadingViewController {
            return
        }
        navigationController.viewControllers.removeAll(where: { $0 is ReturnLoadingViewController })
    }
    
    func didOutputConfirmPayment(action: ConfirmBizPaymentAction) {
        switch action {
        case .tfa:
            childCashoutOutput(.tfa(topViewController: findConfirmPaymentInStack(), flowTFA: .pixCashOut))
        case let .receipt(transactionId, data):
            navigationController.dismiss(animated: false) {
                self.childCashoutOutput(.receipt(transactionId: transactionId, isRefundAllowed: false, data: data))
            }
        }
    }
    
    func didReturnLoadingOutput(action: ReturnLoadingAction) {
        switch action {
        case let .successQRCodeSend(pixParams,
                                    amount,
                                    availableAmount,
                                    pixAccount,
                                    qrPixAccount,
                                    userInfo):
            self.userInfo = userInfo
            guard let pixAccount = pixAccount else {
                navigationController.popViewController(animated: true)
                return
            }
            let viewModel = PaymentAmountViewModel(pixAccount: pixAccount,
                                                   availableAmount: availableAmount,
                                                   flowType: .send(pixParams: pixParams),
                                                   qrCodePixAccount: qrPixAccount,
                                                   amount: amount)
            perform(action: .paymentValue(paymentAccountViewModel: viewModel))
        case .error:
            navigationController.popViewController(animated: true)
        default:
            break
        }
    }
    
    func didTFAOutput(output: TFADelegateOutputConfirmPayment) {
        switch output {
        case .factorAuthenticationWasClosed:
            guard let confirm = findConfirmPaymentInStack() else {
                navigationController.popViewController(animated: true)
                return
            }
            navigationController.popToViewController(confirm, animated: true)
        case let .factorClaimKeyAuthenticationCompleted(hash):
            if let confirm = findConfirmPaymentInStack() {
                confirm.input(action: .confirmPayment(hash: hash))
            }
        }
    }
    
    func findConfirmPaymentInStack() -> ConfirmBizPaymentViewController? {
        navigationController.viewControllers.first(
            where: { $0 is ConfirmBizPaymentViewController }
        ) as? ConfirmBizPaymentViewController
    }
    
    func copyPasteOutput(action: CopyPastePaymentAction) {
        switch action {
        case let .continuePayment(code):
            perform(action: .loadPaymentValue(code: code))
        }
    }
    
    func didPaymentAmountOutput(action: PaymentAmountAction) {
        guard case let .paymentConfirmation(pixAccount, sendAmount, flowType, qrCodeAccount) = action else { return }
        guard let qrCodePixAccount = qrCodeAccount else { return }
        perform(action: .confirmPayment(input: .none,
                                        flowType: flowType,
                                        value: sendAmount,
                                        pixAccount: pixAccount,
                                        qrCodeAccount: qrCodePixAccount))
    }
}
