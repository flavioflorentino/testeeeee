import UI
import UIKit

public enum PIXBizBankAccountCashoutAction: Equatable {
    case manualInsertion
    case loadPaymentValue(pixParams: PIXParams)
    case paymentValue(paymentAccountViewModel: PaymentAmountViewModel)
    case confirmPayment(input: ConfirmBizPaymentInputAction,
                        flowType: PaymentAmountFlowType,
                        value: Double,
                        pixAccount: PixAccount)
}

final class PIXBizBankAccountFlowCoordinator: TFAOutputDelegate {
    private var navigationController: UINavigationController
    private var userInfo: KeyManagerBizUserInfo?
    private let childCashoutOutput: PIXBizChildCashoutOutput
    private let cashoutType = PIXBizCashoutAction.bankAccount
    
    init(navigationController: UINavigationController,
         childCashoutOutput: @escaping PIXBizChildCashoutOutput,
         userInfo: KeyManagerBizUserInfo?) {
        self.userInfo = userInfo
        self.navigationController = navigationController
        self.childCashoutOutput = childCashoutOutput
    }
    
    func perform(action: PIXBizBankAccountCashoutAction) {
        let controller: UIViewController
        switch action {
        case .manualInsertion:
            controller = ManualInsertionBizFactory.make(
                manualInsertionBizOutput: didManualInsertionBizOutput,
                userInfo: userInfo
            )
        case let .loadPaymentValue(pixParams):
            controller = ReturnLoadingFactory.make(flowType: .bankAccount(pixParams: pixParams),
                                                   output: didReturnLoadingOutput)
        case .paymentValue(let paymentAmountViewModel):
            controller = PaymentAmountFactory.makeSendAmount(
                paymentAmountViewModel: paymentAmountViewModel,
                paymentAmountBizOutput: didPaymentAmountOutput,
                userInfo: userInfo
            )
        case let .confirmPayment(input, flowType, value, pixAccount):
            if let confirmPaymentController = findConfirmPaymentInStack() {
                confirmPaymentController.input(action: input)
                return
            }
            guard let userInfo = userInfo else { return }
            
            switch flowType {
            case let .send(pixParams):
                let sendInfo = ConfirmBizPaymentSendInfo(
                    userInfo: userInfo,
                    pixParams: pixParams,
                    pixAccount: pixAccount,
                    value: value)
                controller = ConfirmBizPaymentFactory.makeSend(
                    sendInfo: sendInfo,
                    confirmOutput: didOutputConfirmPayment
                )
            case let .refund(transactionId, maxRefundAmount, sendAmount):
                let refundInfo = ConfirmBizPaymentRefundInfo(
                    userInfo: userInfo,
                    transactionId: transactionId,
                    maxRefundAmount: maxRefundAmount,
                    sendAmount: sendAmount,
                    pixAccount: pixAccount,
                    value: value)
                controller = ConfirmBizPaymentFactory.makeRefund(
                    refundInfo: refundInfo,
                    confirmOutput: didOutputConfirmPayment
                )
            }
        }
        controller.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(controller, animated: true)
        dismissLoading(with: controller)
    }
    
    private func dismissLoading(with actualController: UIViewController) {
        if actualController is ReturnLoadingViewController {
            return
        }
        if navigationController.viewControllers.contains(where: { $0 is ReturnLoadingViewController }) {
            navigationController.viewControllers.removeAll(where: { $0 is ReturnLoadingViewController })
        }
    }
    
    func didOutputConfirmPayment(action: ConfirmBizPaymentAction) {
        switch action {
        case .tfa:
            childCashoutOutput(.tfa(topViewController: findConfirmPaymentInStack(), flowTFA: .pixCashOut))
        case let .receipt(transactionId, data):
            navigationController.dismiss(animated: false) {
                self.childCashoutOutput(.receipt(transactionId: transactionId, isRefundAllowed: false, data: data))
            }
        }
    }
    
    func didReturnLoadingOutput(action: ReturnLoadingAction) {
        switch action {
        case let .successSend(pixParams, _, availableAmount, pixAccount, userInfo):
            self.userInfo = userInfo
            guard let pixAccount = pixAccount else {
                navigationController.popViewController(animated: true)
                return
            }
            let viewModel = PaymentAmountViewModel(pixAccount: pixAccount,
                                                   availableAmount: availableAmount,
                                                   flowType: .send(pixParams: pixParams))
            perform(action: .paymentValue(paymentAccountViewModel: viewModel))
        case .error:
            navigationController.popViewController(animated: true)
        default:
            break
        }
    }
    
    func didTFAOutput(output: TFADelegateOutputConfirmPayment) {
        switch output {
        case .factorAuthenticationWasClosed:
            if let confirm = findConfirmPaymentInStack() {
                navigationController.popToViewController(confirm, animated: true)
            } else {
                navigationController.popViewController(animated: true)
            }
        case let .factorClaimKeyAuthenticationCompleted(hash):
            if let confirm = findConfirmPaymentInStack() {
                confirm.input(action: .confirmPayment(hash: hash))
            }
        }
    }
    
    func findConfirmPaymentInStack() -> ConfirmBizPaymentViewController? {
        navigationController.viewControllers.first(
            where: { $0 is ConfirmBizPaymentViewController }
        ) as? ConfirmBizPaymentViewController
    }
    
    func didManualInsertionBizOutput(action: ManualInsertionBizAction) {
        switch action {
        case let .bankSelection(completion):
            let controller = BankListFactory.make(output: didBankListOutput, completion: completion)
            navigationController.pushViewController(controller, animated: true)
        case let .nextStep(params):
            perform(action: .loadPaymentValue(pixParams: params))
        }
    }
    
    func didPaymentAmountOutput(action: PaymentAmountAction) {
        guard case let .paymentConfirmation(pixAccount, sendAmount, flowType, _) = action else { return }
        perform(action: .confirmPayment(input: .none,
                                        flowType: flowType,
                                        value: sendAmount,
                                        pixAccount: pixAccount))
    }
    
    func didBankListOutput(action: BankListAction) {
        if case .dismiss = action {
            navigationController.popViewController(animated: true)
        }
    }
}
