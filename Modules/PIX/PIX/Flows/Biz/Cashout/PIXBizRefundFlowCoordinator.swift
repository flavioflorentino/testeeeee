import FactorAuthentication
import UI
import UIKit

public typealias PIXBizChildCashoutOutput = (PIXBizChildCashoutOutputAction) -> Void

public enum PIXBizChildCashoutOutputAction {
    case receipt(transactionId: String?, isRefundAllowed: Bool, data: ReceiptResponse?)
    case tfa(topViewController: UIViewController?, flowTFA: FactorAuthenticationFlow)
}

public enum PIXBizReturnAction: Equatable {
    case loadPaymentValue(transactionId: String)
    case paymentValue(paymentAccountViewModel: PaymentAmountViewModel)
    case confirmPayment(input: ConfirmBizPaymentInputAction,
                        flowType: PaymentAmountFlowType,
                        value: Double,
                        pixAccount: PixAccount)
}

final class PIXBizRefundFlowCoordinator: TFAOutputDelegate {
    private var navigationController: UINavigationController
    private var userInfo: KeyManagerBizUserInfo?
    private var childNavigationController: UINavigationController?
    private let childCashoutOutput: PIXBizChildCashoutOutput
    private let cashoutType = PIXBizCashoutAction.refund
     
    init(navigationController: UINavigationController,
         childCashoutOutput: @escaping PIXBizChildCashoutOutput,
         userInfo: KeyManagerBizUserInfo?) {
        self.userInfo = userInfo
        self.navigationController = navigationController
        self.childCashoutOutput = childCashoutOutput
    }

    func perform(action: PIXBizReturnAction) {
        let controller: UIViewController
        switch action {
        case let .loadPaymentValue(transactionId):
            controller = ReturnLoadingFactory.make(flowType: .refund(transactionId: transactionId), output: didReturnLoadingOutput)
            childNavigationController = UINavigationController(rootViewController: controller)
            guard let navigation = self.childNavigationController else { return }
            navigationController.present(navigation, animated: true)
            return
        case let .paymentValue(paymentAmountViewModel):
            controller = PaymentAmountFactory.makeRefundAmount(
                paymentAmountViewModel: paymentAmountViewModel,
                paymentAmountBizOutput: didPaymentAmountOutput,
                userInfo: userInfo
            )
        case let .confirmPayment(input, flowType, value, pixAccount):
            if let confirmPaymentController = findConfirmPaymentInStack() {
                confirmPaymentController.input(action: input)
                return
            }
            guard let userInfo = userInfo else { return }
            
            switch flowType {
            case let .send(pixParams):
                let sendInfo = ConfirmBizPaymentSendInfo(
                    userInfo: userInfo,
                    pixParams: pixParams,
                    pixAccount: pixAccount,
                    value: value)
                controller = ConfirmBizPaymentFactory.makeSend(
                    sendInfo: sendInfo,
                    confirmOutput: didOutputConfirmPayment
                )
            case let .refund(transactionId, maxRefundAmount, sendAmount):
                let refundInfo = ConfirmBizPaymentRefundInfo(
                    userInfo: userInfo,
                    transactionId: transactionId,
                    maxRefundAmount: maxRefundAmount,
                    sendAmount: sendAmount,
                    pixAccount: pixAccount,
                    value: value)
                controller = ConfirmBizPaymentFactory.makeRefund(
                    refundInfo: refundInfo,
                    confirmOutput: didOutputConfirmPayment
                )
            }
        }
        controller.hidesBottomBarWhenPushed = true
        childNavigationController?.pushViewController(controller, animated: false)
        dismissLoading(with: controller)
    }
    
    private func dismissLoading(with actualController: UIViewController) {
        if actualController is ReturnLoadingViewController {
            return
        }
        childNavigationController?.viewControllers.removeAll(where: { $0 is ReturnLoadingViewController })
    }
    
    func didOutputConfirmPayment(action: ConfirmBizPaymentAction) {
        switch action {
        case .tfa:
            childCashoutOutput(.tfa(topViewController: findConfirmPaymentInStack(), flowTFA: .pixCashOutChargeback))
        case let .receipt(transactionId, data):
            navigationController.dismiss(animated: false) {
                self.childCashoutOutput(.receipt(transactionId: transactionId, isRefundAllowed: false, data: data))
            }
        }
    }
    
    func didPaymentAmountOutput(action: PaymentAmountAction) {
        guard case let .paymentConfirmation(pixAccount, sendAmount, flowType, _) = action else { return }
        perform(action: .confirmPayment(input: .none,
                                        flowType: flowType,
                                        value: sendAmount,
                                        pixAccount: pixAccount))
    }
    
    func findConfirmPaymentInStack() -> ConfirmBizPaymentViewController? {
        childNavigationController?.viewControllers.first(
            where: { $0 is ConfirmBizPaymentViewController }
        ) as? ConfirmBizPaymentViewController
    }
    
    func didReturnLoadingOutput(action: ReturnLoadingAction) {
        switch action {
        case let .successRefund(result, availableAmount, userInfo):
            self.userInfo = userInfo
            let viewModel = PaymentAmountViewModel(pixAccount: result.receiverData,
                                                   availableAmount: availableAmount,
                                                   flowType: .refund(transactionId: result.originalTransactionId,
                                                                     maxRefundAmount: result.valueAvailable,
                                                                     sendAmount: result.originalValue))
            perform(action: .paymentValue(paymentAccountViewModel: viewModel))
        case .error:
            navigationController.dismiss(animated: true)
        default:
            break
        }
    }
    
    func didTFAOutput(output: TFADelegateOutputConfirmPayment) {
        switch output {
        case .factorAuthenticationWasClosed:
            if let confirm = findConfirmPaymentInStack() {
                childNavigationController?.popToViewController(confirm, animated: true)
            } else {
                childNavigationController?.popViewController(animated: true)
            }
        case let .factorClaimKeyAuthenticationCompleted(hash):
            if let confirm = findConfirmPaymentInStack() {
                confirm.input(action: .confirmPayment(hash: hash))
            }
        }
    }
}
