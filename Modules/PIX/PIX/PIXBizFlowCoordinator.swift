import Core
import UI
import UIKit

public enum PIXBizFlow: Equatable {
    case welcomePage
    case hub
    case optin(action: PIXBizOptinAction, dismissChild: Bool = false)
    case cashin(action: PIXBizCashinAction)
    case cashout(action: PIXBizCashoutAction)
    case faq(controller: UIViewController)
    case receipt(transactionId: String?, isRefundAllowed: Bool, data: ReceiptResponse? = nil)
    case newReceipt(movementCode: Int?, transactionId: String?, isRefundAllowed: Bool, data: ReceiptResponse? = nil)
    case dailyLimit
}

public protocol PIXBizFlowCoordinating: Coordinating {
    func perform(flow: PIXBizFlow)
    func finish()
}

public final class PIXBizFlowCoordinator: PIXBizFlowCoordinating {
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    private let navigationController: UINavigationController
    private var childNavigationController: UINavigationController?
    private var childCoordinators: [Coordinating] = []
    private var userInfo: KeyManagerBizUserInfo?
    private var presentedWelcome: Bool = true
    private var presentCashOutWelcome: Bool = true
    private var dependencies: Dependencies
    
    public typealias Dependencies = HasCustomerSupport & HasKVStore
    
    public init(with navigationController: UINavigationController,
                dependencies: Dependencies? = nil,
                userInfo: KeyManagerBizUserInfo? = nil) {
        self.dependencies = dependencies ?? DependencyContainer()
        self.navigationController = navigationController
        self.userInfo = userInfo
    }
    
    public func perform(flow: PIXBizFlow) {
        switch flow {
        case let .receipt(receiptId, isRefundAllowed, data):
            let dataType = createReceiptType(transactionId: receiptId, data: data)
            let controller = ReceiptBizFactory.make(dataType: dataType,
                                                    isRefundAllowed: isRefundAllowed,
                                                    output: didReceiptBizOutput)
            self.childNavigationController = UINavigationController(rootViewController: controller)
            guard let navigation = self.childNavigationController else { return }
            navigationController.present(navigation, animated: true)
            controller.hidesBottomBarWhenPushed = true
        case let .newReceipt(movementCode, transactionId, isRefundAllowed, data):
            let dataType = createReceiptType(transactionId: transactionId, data: data, movementCode: movementCode)
            let controller = ReceiptBizFactory.make(dataType: dataType,
                                                    isRefundAllowed: isRefundAllowed,
                                                    output: didReceiptBizOutput)
            childNavigationController = UINavigationController(rootViewController: controller)
            guard let navigation = childNavigationController else { return }
            navigationController.present(navigation, animated: true)
            controller.hidesBottomBarWhenPushed = true
        case .welcomePage:
            presentedWelcome = false
            let controller = PixAvailableFactory.make(pixAvailableOutput: didPixAvailableOutput)
            self.childNavigationController = UINavigationController(rootViewController: controller)
            guard let navigation = self.childNavigationController else { return }
            navigationController.present(navigation, animated: true)
            controller.hidesBottomBarWhenPushed = true
        case .hub:
            let controller = HubBizFactory.make(hubOutput: didHubOutput, userInfo: userInfo)
            self.childNavigationController = UINavigationController(rootViewController: controller)
            guard let navigation = self.childNavigationController else { return }
            navigationController.present(navigation, animated: true)
            controller.hidesBottomBarWhenPushed = true
        case let .optin(action, _):
            let optin = PIXBizOptinFlowCoordinator(from: self.childNavigationController,
                                                   userInfo: self.userInfo)
            optin.perform(action: action)
            self.childCoordinators.append(optin)
        case let .cashin(action):
            guard let userInfo = self.userInfo else {
                return
            }
            let cashin = PIXBizCashinFlowCoordinator(navigationController: self.childNavigationController ?? UINavigationController(),
                                                     userInfo: userInfo,
                                                     cashinOutput: didCashinOutput,
                                                     onboardingPresented: presentedWelcome)
            cashin.perform(action: action)
            childCoordinators.append(cashin)
        case let .cashout(action):
            presentCashOutWelcome = self.dependencies.kvStore.boolFor(KVKey.isPixCashoutWelcomeVisualized)
            let cashout = PIXBizCashoutFlowCoordinator(navigationController: self.childNavigationController ?? navigationController,
                                                       cashoutOutput: didCashoutOutput,
                                                       userInfo: userInfo,
                                                       onboardingPresented: presentCashOutWelcome)
            cashout.perform(action: action)
            childCoordinators.append(cashout)
        case let .faq(controller):
            dependencies.costumeSupportContract?.openFAQ(in: controller)
        case .dailyLimit:
            let controller = DailyLimitFactory.makePJ()
            if let childNavigationController = childNavigationController {
                childNavigationController.pushViewController(controller, animated: true)
            } else {
                let navigation = UINavigationController(rootViewController: controller)
                self.childNavigationController = navigation
                navigationController.present(navigation, animated: true)
                controller.hidesBottomBarWhenPushed = true
            }
        }
    }
    
    private func createReceiptType(transactionId: String?, data: ReceiptResponse?, movementCode: Int? = nil) -> ReceiptType {
        if let movementCode = movementCode, let transactionId = transactionId {
            return .movementTransaction(movementCode: movementCode, transactionId: transactionId)
        } else if let transactionId = transactionId {
            return .transactionId(transactionId)
        } else if let data = data {
            return .data(data)
        } else {
            return .transactionId("")
        }
    }
    
    public func finish() {
        childCoordinators.removeAll()
        navigationController.dismiss(animated: true) {
            self.navigationController.popToRootViewController(animated: true)
        }
    }
}

private extension PIXBizFlowCoordinator {
    func didCashoutOutput(action: PIXBizCashoutOutputAction) {
        switch action {
        case let .receipt(transactionId, isRefundAllowed, data):
            perform(flow: .receipt(transactionId: transactionId, isRefundAllowed: isRefundAllowed, data: data))
        }
    }
    
    func didReceiptBizOutput(action: ReceiptBizAction) {
        switch action {
        case let .refund(transactionId):
            perform(flow: .cashout(action: .refund(transactionId: transactionId)))
        default:
            break
        }
    }
    func dismissChildFlowCoordinator() {
        childCoordinators.removeAll()
    }
    
    func didPixAvailableOutput(output: FeedbackAction) {
        navigationController.dismiss(animated: true)
        if case .tryPix = output {
            perform(flow: .hub)
        }
    }
    
    func didHubOutput(hubOutput: HubBizOutputAction) {
        switch hubOutput {
        case let .cashOut(action):
            perform(flow: .cashout(action: action))
        case let .cashIn(action):
            perform(flow: .cashin(action: action))
        case .keys:
            perform(flow: .optin(action: .keyManager(input: .none)))
        case .close:
            navigationController.dismiss(animated: true)
        case let .faq(viewController):
            perform(flow: .faq(controller: viewController))
        case let .updateUserInfo(userInfo):
            self.userInfo = userInfo
        case .dailyLimit:
            perform(flow: .dailyLimit)
        }
    }
    
    func didCashinOutput(cashinOutput: PIXBizCashinOutputAction) {
        switch cashinOutput {
        case .keyManager:
            dismissChildFlowCoordinator()
            perform(flow: .optin(action: .keyManager(input: .none)))
        }
    }
}
