import Foundation

// swiftlint:disable convenience_type
final class PIXResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: PIXResources.self).url(forResource: "PIXResources", withExtension: "bundle") else {
            return Bundle(for: PIXResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: PIXResources.self)
    }()
}
