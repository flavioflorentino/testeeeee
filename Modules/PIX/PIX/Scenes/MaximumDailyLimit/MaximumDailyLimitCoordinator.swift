import CustomerSupport
import UIKit

enum MaximumDailyLimitAction {
    case ticketList
}

protocol MaximumDailyLimitCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: MaximumDailyLimitAction)
}

final class MaximumDailyLimitCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - MaximumDailyLimitCoordinating
extension MaximumDailyLimitCoordinator: MaximumDailyLimitCoordinating {
    func perform(action: MaximumDailyLimitAction) {
        switch action {
        case .ticketList:
            let navigationViewController = viewController?.navigationController
            navigationViewController?.popViewController(animated: true)
            let controller = TicketListFactory.make()
            navigationViewController?.pushViewController(controller, animated: true)
        }
    }
}
