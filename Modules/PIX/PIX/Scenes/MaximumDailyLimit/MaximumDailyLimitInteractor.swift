import Core
import CustomerSupport
import Foundation

protocol MaximumDailyLimitInteracting: AnyObject {
    func handleInputValue(_ value: String)
    func openTicketWith(value: String)
}

final class MaximumDailyLimitInteractor {
    typealias Dependencies = HasZendesk & HasMainQueue

    #if DEBUG
    private let valueCustomFieldId: Int64 = 360_042_290_952
    private let subjectCustomFieldId: Int64 = 360_032_442_351
    #else
    private let valueCustomFieldId: Int64 = 360_042_290_992
    private let subjectCustomFieldId: Int64 = 360_032_397_472
    #endif

    private let appType: AppType
    private let presenter: MaximumDailyLimitPresenting
    private let maxAllowedValue: Double = 10_000_000
    private let dependencies: Dependencies
    private var currentMaxDailyLimit: Double
    private var maxDailyLimit: Double = .zero

    private lazy var limitTag: String = {
        switch appType {
        case .pf:
            return "clientepf__pix_aumento_de_limite"
        case .pj:
            return "clientepj__pix_aumento_de_limite"
        }
    }()

    init(
        presenter: MaximumDailyLimitPresenting,
        currentMaxDailyLimit: Double,
        appType: AppType,
        dependencies: Dependencies = DependencyContainer()
    ) {
        self.presenter = presenter
        self.currentMaxDailyLimit = currentMaxDailyLimit
        self.appType = appType
        self.dependencies = dependencies
    }
}

// MARK: - MaximumDailyLimitInteracting
extension MaximumDailyLimitInteractor: MaximumDailyLimitInteracting {
    func handleInputValue(_ value: String) {
        let textNumber = String(value.unicodeScalars.filter(CharacterSet.decimalDigits.contains))
        let value = Double(textNumber) ?? .zero
        guard value < maxAllowedValue else {
            presenter.updateInputField(with: maxDailyLimit)
            return
        }
        handleNewMaxDailyLimit(value)
    }
    
    private func handleNewMaxDailyLimit(_ value: Double) {
        if value <= currentMaxDailyLimit {
            presenter.presentUnavailableLimitAmountError()
            presenter.disableForwardButtonState()
        } else {
            presenter.hideUnavailableLimitAmountError()
            presenter.enableForwardButtonState()
        }
        maxDailyLimit = value
        presenter.updateInputField(with: value)
    }

    func openTicketWith(value: String) {
        presenter.presentLoading()
        dependencies.zendeskContainer?.requestTicket(
            title: Strings.DailyLimit.pixLimit,
            message: value,
            tags: [limitTag],
            extraCustomFields: [
                TicketCustomField(id: valueCustomFieldId, value: value),
                TicketCustomField(id: subjectCustomFieldId, value: Strings.DailyLimit.pixLimit)
            ]
        ) { [weak self] result in
            self?.dependencies.mainQueue.async {
                self?.presenter.hideLoading()
                switch result {
                case .success:
                    self?.presenter.presentTicketList()
                case .failure:
                    self?.presenter.presentAlertGenericErrorWith(value: value)
                }
            }
        }
    }
}
