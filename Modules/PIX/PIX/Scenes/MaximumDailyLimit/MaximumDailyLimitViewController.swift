import UI
import SnapKit
import UIKit

protocol MaximumDailyLimitDisplaying: AnyObject {
    func enableForwardButtonState()
    func disableForwardButtonState()
    func updateInputField(with value: String?)
    func displayUnavailableLimitAmountError()
    func hideUnavailableLimitAmountError()
    func showLoading()
    func hideLoading()
    func displayGenericErrorAlertWith(value: String)
}

final class MaximumDailyLimitViewController: ViewController<MaximumDailyLimitInteracting, UIView> {
    private typealias Localizable = Strings.MaximumDailyLimit

    // MARK: - Visual Components
    private lazy var scrollView = UIScrollView()
    private lazy var containerView = UIView()
    
    private lazy var descriptionTextView: UILabel = {
        let label = UILabel()
        label.numberOfLines = .zero
        label.backgroundColor = Colors.backgroundPrimary.color

        label.attributedText = Localizable.description.attributedStringWith(
            normalFont: Typography.bodyPrimary().font(),
            highlightFont: Typography.bodyPrimary(.highlight).font(),
            normalColor: Colors.grayscale850.color,
            highlightColor: Colors.grayscale850.color,
            underline: false
        )

        return label
    }()

    private lazy var inputValueTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.desiredValue
        textField.keyboardType = .numberPad
        textField.addTarget(self, action: #selector(inputValueChanged), for: .editingChanged)
        textField.inputAccessoryView = doneToolbar
        return textField
    }()
    
    private let unavailableAmountLabel: UILabel = {
        let label = UILabel()
        label.typography = Typography.bodySecondary()
        label.textColor = Colors.critical900.color
        label.text = Strings.MaximumDailyLimit.amountAlreadyAvailable
        label.numberOfLines = 0
        label.isHidden = true
        return label
    }()

    private lazy var doneToolbar = DoneToolBar(doneText: Strings.General.ok)

    private lazy var forwardButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Localizable.askForLimitIncrease, for: .normal)
        button.isEnabled = false
        button.addTarget(self, action: #selector(tapOnForward), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Variables
    private var bottomButton: Constraint?

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    override func buildViewHierarchy() {
        view.addSubviews(scrollView)
        view.addSubview(forwardButton)
        scrollView.addSubview(containerView)
        containerView.addSubview(descriptionTextView)
        containerView.addSubview(inputValueTextField)
        containerView.addSubview(unavailableAmountLabel)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
        }
        
        forwardButton.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(scrollView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            bottomButton = $0.bottom.equalTo(view.compatibleSafeArea.bottom).inset(Spacing.base03).constraint
        }
        
        containerView.snp.makeConstraints {
            $0.edges.width.equalToSuperview()
            $0.height.equalToSuperview().priority(.low)
        }
        
        descriptionTextView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        inputValueTextField.snp.makeConstraints {
            $0.top.equalTo(descriptionTextView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        unavailableAmountLabel.snp.makeConstraints {
            $0.top.equalTo(inputValueTextField.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().inset(Spacing.base01)
        }
    }
    
    override func configureViews() {
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
        title = Strings.MaximumDailyLimit.increaseMyPixLimit
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - MaximumDailyLimitDisplaying
extension MaximumDailyLimitViewController: MaximumDailyLimitDisplaying {
    func enableForwardButtonState() {
        forwardButton.isEnabled = true
    }

    func disableForwardButtonState() {
        forwardButton.isEnabled = false
    }

    func updateInputField(with value: String?) {
        inputValueTextField.text = value
    }
    
    func displayUnavailableLimitAmountError() {
        unavailableAmountLabel.isHidden = false
    }
    
    func hideUnavailableLimitAmountError() {
        unavailableAmountLabel.isHidden = true
    }

    func showLoading() {
        inputValueTextField.isUserInteractionEnabled = false
        forwardButton.startLoading()
    }

    func hideLoading() {
        inputValueTextField.isUserInteractionEnabled = true
        forwardButton.stopLoading()
    }

    func displayGenericErrorAlertWith(value: String) {
        let title = Strings.General.somethingWentWrong
        let subtitle = Strings.DailyLimit.errorDescription
        let attributedSubtitle = NSAttributedString(
            string: subtitle,
            attributes: [
                .foregroundColor: Colors.grayscale600.color,
                .font: Typography.bodyPrimary().font()
            ]
        )
        let primaryButtonTitle = Strings.General.tryAgain
        let secondaryButtonTitle = Strings.General.notNow
        let primaryAction = ApolloAlertAction(title: primaryButtonTitle, completion: { [weak self] in
            self?.interactor.openTicketWith(value: value)
        })
        let linkAction = ApolloAlertAction(title: secondaryButtonTitle, completion: {})
        showApolloAlert(
            type: .sticker(style: .error),
            title: title,
            attributedSubtitle: attributedSubtitle,
            primaryButtonAction: primaryAction,
            linkButtonAction: linkAction
        )
    }
}

// MARK: - Actions
@objc
private extension MaximumDailyLimitViewController {
    func inputValueChanged() {
        interactor.handleInputValue(inputValueTextField.text ?? String())
    }

    func tapOnForward() {
        interactor.openTicketWith(value: inputValueTextField.text ?? String())
    }
}

// MARK: - Keyboard
@objc
private extension MaximumDailyLimitViewController {
    func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        let result = Spacing.base02 + keyboardSize.height
        let currentValue = bottomButton?.layoutConstraints.first?.constant
        if currentValue == -Spacing.base02 || currentValue != result {
            bottomButton?.update(inset: result)
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }

    func keyboardWillHide(notification: NSNotification) {
        guard bottomButton?.layoutConstraints.first?.constant != -Spacing.base02 else {
            return
        }
        bottomButton?.update(inset: Spacing.base02)
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
}
