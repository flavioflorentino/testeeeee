import Foundation
import UI

protocol MaximumDailyLimitPresenting: AnyObject {
    var viewController: MaximumDailyLimitDisplaying? { get set }
    func enableForwardButtonState()
    func disableForwardButtonState()
    func updateInputField(with number: Double)
    func didNextStep(action: MaximumDailyLimitAction)
    func presentUnavailableLimitAmountError()
    func hideUnavailableLimitAmountError()
    func presentTicketList()
    func presentLoading()
    func hideLoading()
    func presentAlertGenericErrorWith(value: String)
}

final class MaximumDailyLimitPresenter {
    private let coordinator: MaximumDailyLimitCoordinating
    weak var viewController: MaximumDailyLimitDisplaying?

    init(coordinator: MaximumDailyLimitCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - MaximumDailyLimitPresenting
extension MaximumDailyLimitPresenter: MaximumDailyLimitPresenting {
    func enableForwardButtonState() {
        viewController?.enableForwardButtonState()
    }

    func disableForwardButtonState() {
        viewController?.disableForwardButtonState()
    }

    func updateInputField(with number: Double) {
        let formattedValue = number.convertToCurrency(hasCurrencySymbol: true, decimalDigits: 0)
        viewController?.updateInputField(with: formattedValue)
    }

    func didNextStep(action: MaximumDailyLimitAction) {
        coordinator.perform(action: action)
    }
    
    func presentUnavailableLimitAmountError() {
        viewController?.displayUnavailableLimitAmountError()
    }
    
    func hideUnavailableLimitAmountError() {
        viewController?.hideUnavailableLimitAmountError()
    }

    func presentTicketList() {
        coordinator.perform(action: .ticketList)
    }

    func presentLoading() {
        viewController?.showLoading()
    }

    func hideLoading() {
        viewController?.hideLoading()
    }

    func presentAlertGenericErrorWith(value: String) {
        viewController?.displayGenericErrorAlertWith(value: value)
    }
}
