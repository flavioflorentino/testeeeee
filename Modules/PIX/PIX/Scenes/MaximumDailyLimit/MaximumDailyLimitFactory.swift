import Core
import UIKit

enum MaximumDailyLimitFactory {
    static func make(currentMaxDailyLimit: Double) -> MaximumDailyLimitViewController {
        let coordinator: MaximumDailyLimitCoordinating = MaximumDailyLimitCoordinator()
        let presenter: MaximumDailyLimitPresenting = MaximumDailyLimitPresenter(coordinator: coordinator)
        let interactor = MaximumDailyLimitInteractor(presenter: presenter, currentMaxDailyLimit: currentMaxDailyLimit, appType: Environment.appType)
        let viewController = MaximumDailyLimitViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
