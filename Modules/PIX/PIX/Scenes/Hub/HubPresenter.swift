import Foundation

protocol HubPresenting: AnyObject {
    var viewController: HubDisplay? { get set }
    func didNextStepWithType(_ type: HubItemType)
    func presentSections(_ sections: [HubSection])
    func close()
}

final class HubPresenter {
    private let coordinator: HubCoordinating
    weak var viewController: HubDisplay?

    init(coordinator: HubCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - HubPresenting
extension HubPresenter: HubPresenting {
    func didNextStepWithType(_ type: HubItemType) {
        coordinator.performWithType(type)
    }

    func presentSections(_ sections: [HubSection]) {
        viewController?.showSections(sections)
    }

    func close() {
        coordinator.close()
    }
}
