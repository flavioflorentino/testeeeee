import UI
import UIKit

protocol HubVersionBDisplaying: AnyObject {
    func displaySomething()
}

private extension HubVersionBViewController.Layout {
    enum Size {
        static let imageHeight: CGFloat = 90.0
    }
}

final class HubVersionBViewController: ViewController<HubVersionBInteracting, UIView> {
    fileprivate enum Layout { }

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.doSomething()
    }

    override func buildViewHierarchy() { }
    
    override func setupConstraints() { }

    override func configureViews() { }
}

// MARK: - HubVersionBDisplaying
extension HubVersionBViewController: HubVersionBDisplaying {
    func displaySomething() { }
}
