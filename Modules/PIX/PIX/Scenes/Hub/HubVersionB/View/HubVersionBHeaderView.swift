import UI
import UIKit

final class HubVersionBHeaderView: UIView {
    // MARK: - Visual Component
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        label.text = Strings.HubVersionB.description
        return label
    }()
    
    // MARK: - Life Cycle
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration
extension HubVersionBHeaderView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(messageLabel)
    }
    
    func setupConstraints() {
        messageLabel.snp.makeConstraints {
            $0.top.equalToSuperview().inset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().inset(Spacing.base03)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}
