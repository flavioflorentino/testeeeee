import UI
import UIKit

private enum Layout {
    static let iconSide: CGFloat = 24
}

final class HubCollectionViewCell: UICollectionViewCell {
    // MARK: - Visual Components
    private let cardView: UIView = {
        let view = UIView()
        view.viewStyle(CardViewStyle())
        view.backgroundColor = Colors.backgroundPrimary.color
        return view
    }()
    
    private let iconImageView = UIImageView()
    
    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary().font()
        label.textColor = Colors.grayscale700.color
        label.numberOfLines = 2
        return label
    }()
    
    // MARK: - Life Cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setContent(icon: UIImage, description: String) {
        iconImageView.image = icon
        descriptionLabel.text = description
    }
}

// MARK: - ViewConfiguration
extension HubCollectionViewCell: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(cardView)
        cardView.addSubview(iconImageView)
        cardView.addSubview(descriptionLabel)
    }
    
    func setupConstraints() {
        cardView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.bottom.top.equalToSuperview().inset(Spacing.base00)
        }
        
        iconImageView.snp.makeConstraints {
            $0.width.height.equalTo(Layout.iconSide)
            $0.leading.top.equalToSuperview().inset(Spacing.base01)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(iconImageView).inset(Spacing.base01)
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base01)
        }
    }
}
