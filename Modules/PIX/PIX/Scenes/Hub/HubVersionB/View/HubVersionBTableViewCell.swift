import UI
import UIKit

private enum Layout {
    enum Size {
        static let icon = CGSize(width: 20, height: 20)
    }
}

public final class HubVersionBTableViewCell: UITableViewCell {
    // MARK: - Visual Components
    private lazy var containerView: UIView = {
        let view = UIView()
        view.viewStyle(CardViewStyle())
        view.backgroundColor = Colors.backgroundPrimary.color
        return view
    }()
    
    private lazy var iconImageView = UIImageView()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
        label.textColor = Colors.grayscale700.color
        return label
    }()
    
    // MARK: - Life Cycle
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureCellWithImage(_ image: UIImage, text: String) {
        descriptionLabel.text = text
        iconImageView.image = image
    }
}

// MARK: - ViewConfiguration
extension HubVersionBTableViewCell: ViewConfiguration {
    public func buildViewHierarchy() {
        contentView.addSubview(containerView)
        containerView.addSubview(iconImageView)
        containerView.addSubview(descriptionLabel)
    }
    
    public func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.bottom.equalToSuperview().inset(Spacing.base00)
            $0.height.equalTo(Sizing.base09)
        }
        iconImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.icon)
            $0.top.equalTo(descriptionLabel)
            $0.leading.equalToSuperview().inset(Spacing.base01)
        }
        descriptionLabel.snp.makeConstraints {
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base02)
            $0.trailing.top.equalToSuperview().inset(Spacing.base02)
            $0.bottom.lessThanOrEqualToSuperview().inset(Spacing.base02)
        }
    }
    
    public func configureViews() {
        contentView.backgroundColor = Colors.backgroundPrimary.color
        selectionStyle = .none
    }
}
