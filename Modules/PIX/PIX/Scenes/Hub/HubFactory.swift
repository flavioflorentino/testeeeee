import Foundation
import UIKit

public enum HubFactory {
    public static func make(
        origin: String,
        paymentOpening: PIXPaymentOpening,
        bankSelectorType: BankSelector.Type,
        qrCodeScannerType: QrCodeScanning.Type
    ) -> UIViewController {
        let container = DependencyContainer()
        let coordinator: HubCoordinating = HubCoordinator(
            paymentOpening: paymentOpening,
            bankSelectorType: bankSelectorType,
            qrCodeScannerType: qrCodeScannerType
        )
        let presenter: HubPresenting = HubPresenter(coordinator: coordinator)
        let interactor = HubInteractor(presenter: presenter, dependencies: container, origin: origin)
        let viewController = HubViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController
        
        return viewController
    }
}
