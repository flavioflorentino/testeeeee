import Core
import UIKit

protocol HubCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func performWithType(_ type: HubItemType)
    func close()
}

final class HubCoordinator {
    typealias Dependencies = HasKVStore
    private let dependencies: Dependencies
    
    weak var viewController: UIViewController?
    private let paymentOpening: PIXPaymentOpening
    private let bankSelectorType: BankSelector.Type
    private let qrCodeScannerType: QrCodeScanning.Type
    
    init(paymentOpening: PIXPaymentOpening, bankSelectorType: BankSelector.Type, qrCodeScannerType: QrCodeScanning.Type, dependencies: Dependencies? = nil) {
        self.paymentOpening = paymentOpening
        self.bankSelectorType = bankSelectorType
        self.qrCodeScannerType = qrCodeScannerType
        self.dependencies = dependencies ?? DependencyContainer()
    }
}

// MARK: - HubCoordinating
extension HubCoordinator: HubCoordinating {
    func performWithType(_ type: HubItemType) {
        switch type {
        case .keySelector:
            let controller = KeySelectorFactory.make(paymentOpening: paymentOpening)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .manualInsert:
            let controller = ManualInsertionFactory.make(bankSelectorType: bankSelectorType, paymentOpening: paymentOpening)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .qrCode:
            let controller = qrCodeScannerType.init(backButtonSide: .left, hasMyCodeBottomSheet: false, scannerType: .pix)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .receivePayment:
            let controller = ReceiveLoadingFactory.make(origin: .pixHub)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .copyPaste:
            let controller = CopyPastePFFactory.make(paymentOpening: paymentOpening, origin: .hubScreen)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .keyManagement:
            guard let navController = viewController?.navigationController else { return }
            let flowCoordinator = PIXConsumerFlowCoordinator(
                originViewController: navController,
                originFlow: .hub,
                destination: .keyManagement,
                dependencies: dependencies
            )
            flowCoordinator.start(isModalPresentation: false)
        case .dailyLimit:
            let controller = DailyLimitFactory.makePF()
            viewController?.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func close() {
        viewController?.dismiss(animated: true)
    }
}
