import AnalyticsModule
import FeatureFlag
import Foundation

protocol HubInteracting: AnyObject {
    func callAccessAnalytics()
    func buildHubItems()
    func didNextStepWithType(_ type: HubItemType)
    func close()
    func openFaq()
}

extension HubInteracting {
    func openFaq() {}
}

final class HubInteractor {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    
    private let dependencies: Dependencies
    private let presenter: HubPresenting
    private let origin: String
    private var hubSections = [HubSection]()

    init(presenter: HubPresenting, dependencies: Dependencies, origin: String) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.origin = origin
    }
}

// MARK: - HubInteracting
extension HubInteractor: HubInteracting {
    func callAccessAnalytics() {
        dependencies.analytics.log(HubUserEvent.userNavigated(origin: origin))
    }

    func buildHubItems() {
        var sections = [HubSection]()

        var sendSection = HubSection(title: Strings.General.send, rows: [])
        if dependencies.featureManager.isActive(.featurePixHubKeys) {
            sendSection.rows.append(.keySelector)
        }
        if dependencies.featureManager.isActive(.featurePixHubCopyPaste) {
            sendSection.rows.append(.copyPaste)
        }
        if dependencies.featureManager.isActive(.featurePixHubQrCode) {
            sendSection.rows.append(.qrCode)
        }
        if dependencies.featureManager.isActive(.featurePixHubManualBank) {
            sendSection.rows.append(.manualInsert)
        }
        if sendSection.rows.isNotEmpty {
            sections.append(sendSection)
        }

        if dependencies.featureManager.isActive(.featurePixHubReceiving) {
            sections.append(HubSection(title: Strings.Hub.receive, rows: [.receivePayment]))
        }

        if dependencies.featureManager.isActive(.featurePixHubManageKeys) {
            sections.append(HubSection(title: Strings.Hub.keyManagement, rows: [.keyManagement]))
        }
        
        if dependencies.featureManager.isActive(.isPixDailyLimitAvailable) {
            sections.append(HubSection(title: Strings.Hub.myLimitPix, rows: [.dailyLimit]))
        }

        hubSections = sections
        presenter.presentSections(sections)
    }
    
    func didNextStepWithType(_ type: HubItemType) {
        dependencies.analytics.log(HubUserEvent.itemSelected(item: type))
        presenter.didNextStepWithType(type)
    }
    
    func close() {
        presenter.close()
    }
}
