import UI
import UIKit

protocol HubBizCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func performWithType(_ type: HubItemType)
    func perform(_ type: HubBizOutputAction)
    func close()
    func updateUserInfo(with userInfo: KeyManagerBizUserInfo)
    func openFaq()
}

final class HubBizCoordinator {
    weak var viewController: UIViewController?
    private let hubOutput: HubBizOutput
    
    init(hubOutput: @escaping HubBizOutput) {
        self.hubOutput = hubOutput
    }
}

// MARK: - KeyManagerCoordinating
extension HubBizCoordinator: HubBizCoordinating {
    func performWithType(_ type: HubItemType) {
        switch type {
        case .keyManagement:
            hubOutput(.keys)
        case .keySelector:
            hubOutput(.cashOut(.keys))
        case .qrCode:
            hubOutput(.cashOut(.qrCode))
        case .copyPaste:
            hubOutput(.cashOut(.copyPaste))
        case .manualInsert:
            hubOutput(.cashOut(.bankAccount))
        case .dailyLimit:
            hubOutput(.dailyLimit)
        default:
            break
        }
    }
    
    func perform(_ type: HubBizOutputAction) {
        hubOutput(type)
    }
    
    func updateUserInfo(with userInfo: KeyManagerBizUserInfo) {
        hubOutput(.updateUserInfo(userInfo))
    }
    
    func close() {
        hubOutput(.close)
    }
    
    func openFaq() {
        guard let viewController = viewController else {
            return
        }
        hubOutput(.faq(viewController: viewController))
    }
}
