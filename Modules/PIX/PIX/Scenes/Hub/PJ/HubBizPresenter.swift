import Foundation
import UIKit
import AssetsKit

protocol HubBizPresenting: AnyObject {
    var viewController: HubBizDisplay? { get set }
    func didNextStepWithType(_ type: HubItemType)
    func didNextStepWithAction(_ type: HubBizOutputAction)
    func close()
    func openFaq()
    func startLoading()
    func stopLoading()
    func updateUserInfo(with userInfo: KeyManagerBizUserInfo)
    func presentGenericError()
}

final class HubBizPresenter {
    private let coordinator: HubBizCoordinating
    weak var viewController: HubBizDisplay?

    init(coordinator: HubBizCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - HubPresenting
extension HubBizPresenter: HubBizPresenting {
    func didNextStepWithAction(_ type: HubBizOutputAction) {
        coordinator.perform(type)
    }
    
    func updateUserInfo(with userInfo: KeyManagerBizUserInfo) {
        coordinator.updateUserInfo(with: userInfo)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func didNextStepWithType(_ type: HubItemType) {
        coordinator.performWithType(type)
    }
    
    func close() {
        coordinator.close()
    }
    
    func openFaq() {
        coordinator.openFaq()
    }
    
    func presentGenericError() {
        viewController?.presentError(title: Strings.Receive.ReceiveLoading.Error.title,
                                     message: Strings.Receive.ReceiveLoading.Error.message,
                                     buttonTitle: Strings.General.back)
    }
}
