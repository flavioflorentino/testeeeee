import AnalyticsModule
import Foundation

protocol HubBizInteracting: AnyObject {
    func callAccessAnalytics()
    func didNextStepWithType(indexPath: IndexPath)
    func receiveFlow()
    func close()
    func openFaq()
    func updateUserInfo()
}

final class HubBizInteractor {
    typealias Dependencies = HasAnalytics & HasUserAuthManager
    
    private let dependencies: Dependencies
    private let presenter: HubBizPresenting
    private let service: PixBizKeyServicing
    private var userInfo: KeyManagerBizUserInfo?
    private let sections: [HubSection]
    
    init(presenter: HubBizPresenting,
         dependencies: Dependencies,
         userInfo: KeyManagerBizUserInfo?,
         service: PixBizKeyServicing,
         sections: [HubSection]) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.userInfo = userInfo
        self.service = service
        self.sections = sections
    }
}

// MARK: - HubInteracting
extension HubBizInteractor: HubBizInteracting {
    func callAccessAnalytics() {
        //será implementado no próximo PR
    }
    
    func updateUserInfo() {
        if userInfo != nil {
            return
        }
        presenter.startLoading()
        dependencies.userAuthManagerContract?.getUserInfo(completion: { [weak self] userInfo in
            self?.presenter.stopLoading()
            self?.userInfo = userInfo
            guard let userInfo = userInfo else {
                self?.presenter.presentGenericError()
                return
            }
            self?.presenter.updateUserInfo(with: userInfo)
        })
    }
    
    func didNextStepWithType(indexPath: IndexPath) {
        let section = sections[indexPath.section]
        guard indexPath.row < section.rows.count else {
            return
        }
        
        let type = section.rows[indexPath.row]
        switch type {
        case .receivePayment:
            sendEvent(.buttonReceivePayment)
            receiveFlow()
            return
        case .keySelector:
            sendEvent(.buttonSendWithKeys)
        case .manualInsert:
            sendEvent(.buttonSendWithBankAcount)
        case .qrCode:
            sendEvent(.buttonSendWithQrCode)
        case .copyPaste:
            sendEvent(.buttonSendWithCopyPaste)
        default:
            break
        }
        presenter.didNextStepWithType(type)
    }
    
    func close() {
        presenter.close()
    }
    
    func openFaq() {
        presenter.openFaq()
    }
    
    func receiveFlow() {
        presenter.startLoading()
        service.listPixKeys(with: userInfo?.id ?? "", and: .company, completion: { [weak self] result in
            guard let self = self else { return }
            self.presenter.stopLoading()
            switch result {
            case let .success(keys):
                let pixKeys = keys.data.filter { !$0.registeredKeys.isEmpty }
                let registeredKeys = pixKeys.map { $0.registeredKeys.first }.compactMap { $0 }
                let processedKeys = registeredKeys.filter { $0.statusSlug == .processed }
                if processedKeys.isEmpty {
                    self.presenter.didNextStepWithAction(.cashIn(.needsRegistration))
                    return
                }
                self.presenter.didNextStepWithAction(.cashIn(.receive(keys: processedKeys)))
            case .failure:
                self.presenter.presentGenericError()
            }
        })
    }
    
    private func sendEvent(_ event: KeyBizEvent) {
        guard let userInfo = userInfo else { return }
        dependencies.analytics.log(KeyBizTracker(companyName: userInfo.name, eventType: event))
    }
}
