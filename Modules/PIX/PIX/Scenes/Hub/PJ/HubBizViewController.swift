import AssetsKit
import UI
import UIKit

public enum HubBizInputAction {
    case checkRegisteredKeys
    case none
}

protocol HubBizDisplay: AnyObject {
    func startLoading()
    func stopLoading()
    func input(action: HubBizInputAction)
    func presentError(title: String, message: String, buttonTitle: String)
}

private extension HubBizViewController.Layout {
    enum Size {
        static let header: CGFloat = 256
    }
}

final class HubBizViewController: ViewController<HubBizInteracting, UIView>, StatefulTransitionViewing {
    fileprivate enum Layout { }
    
    private lazy var headerView = HubHeaderView(
        frame: CGRect(x: 0, y: 0, width: view.frame.width, height: Layout.Size.header)
    )
    private var inputAction: HubBizInputAction

    private lazy var tableViewDataSource: TableViewDataSource<HubSection, HubItemType>? = {
        let dataSource = TableViewDataSource<HubSection, HubItemType>(view: tableView)
        dataSource.itemProvider = { tableView, indexPath, item -> HubTableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: HubTableViewCell.identifier, for: indexPath) as? HubTableViewCell
            cell?.configureCellWithImage(item.image, text: item.description)
            return cell
        }
        return dataSource
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.backgroundColor = Colors.backgroundPrimary.color
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.register(HubTableViewCell.self, forCellReuseIdentifier: HubTableViewCell.identifier)
        return tableView
    }()
    
    private let sections: [HubSection]
    
    init(interactor: HubBizInteracting, sections: [HubSection], inputAction: HubBizInputAction = .none) {
        self.sections = sections
        self.inputAction = inputAction
        super.init(interactor: interactor)
    }
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.updateUserInfo()
        interactor.callAccessAnalytics()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
    }
    
    func displayFAQButton() {
        let button = UIButton()
        button.setImage(Resources.Icons.icoQuestion.image, for: .normal)
        button.addTarget(self, action: #selector(didTapInfoButton), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: button)
        navigationItem.rightBarButtonItem = barButton
    }
    
    override func buildViewHierarchy() {
        view.addSubview(tableView)
        tableView.tableHeaderView = headerView
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        tableView.dataSource = tableViewDataSource
        tableView.delegate = self
        buildTableViewItems()
        displayFAQButton()
    }
}

// MARK: HubDisplay
extension HubBizViewController: HubBizDisplay {
    func input(action: HubBizInputAction) {
        switch action {
        case .checkRegisteredKeys:
            interactor.receiveFlow()
        default:
            break
        }
    }
    
    func startLoading() {
        beginState(model: StateLoadingViewModel(message: Strings.KeyManager.loading))
    }
    
    func stopLoading() {
        endState()
    }
    
    func presentError(title: String, message: String, buttonTitle: String) {
        let popupViewController = PopupViewController(title: title,
                                                      description: message,
                                                      preferredType: .business)
        let okAction = PopupAction(title: buttonTitle, style: .link)
        popupViewController.addAction(okAction)
        
        showPopup(popupViewController)
    }
}

extension HubBizViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionView = HubCustomSectionView()
        sectionView.configureWithText(sections[section].title)
        return sectionView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor.didNextStepWithType(indexPath: indexPath)
    }
}

private extension HubBizViewController {
    func buildTableViewItems() {
        sections.forEach {
            tableViewDataSource?.add(items: $0.rows, to: $0)
        }
    }
    
    func configureNavigation() {
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.backgroundColor = .clear
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.tintColor = Colors.branding400.color
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.view.backgroundColor = .white
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: Strings.General.close, style: .plain, target: self, action: #selector(close))
        navigationController?.navigationBar.barTintColor = .white
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationController?.navigationBar.largeTitleTextAttributes = [
                .foregroundColor: Colors.grayscale050.color,
                .font: Typography.title(.large).font()
            ]
        }
    }
    
    @objc
    private func close() {
        interactor.close()
    }
    
    @objc
    private func didTapInfoButton() {
        interactor.openFaq()
    }
}
