import Foundation

struct HubBizSection: Hashable {
    let title: String
    var rows: [HubItemType]
}
