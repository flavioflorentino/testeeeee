import Foundation
import UIKit

public typealias HubBizOutput = (HubBizOutputAction) -> Void

public enum HubBizOutputAction: Equatable {
    case cashOut(PIXBizCashoutAction)
    case cashIn(PIXBizCashinAction)
    case keys
    case close
    case faq(viewController: UIViewController)
    case updateUserInfo(KeyManagerBizUserInfo)
    case dailyLimit
}

public enum HubBizFactory {
    public static func make(hubOutput: @escaping HubBizOutput, userInfo: KeyManagerBizUserInfo?) -> UIViewController {
        let container = DependencyContainer()
        let sections: [HubSection] = HubBizFactory.generateHubSections(container: container)
        let coordinator: HubBizCoordinating = HubBizCoordinator(hubOutput: hubOutput)
        let presenter: HubBizPresenting = HubBizPresenter(coordinator: coordinator)
        let service = PixBizKeyService(dependencies: container)
        let interactor = HubBizInteractor(presenter: presenter,
                                          dependencies: container,
                                          userInfo: userInfo,
                                          service: service,
                                          sections: sections)
        let viewController = HubBizViewController(interactor: interactor, sections: sections)

        coordinator.viewController = viewController
        presenter.viewController = viewController
        
        return viewController
    }
    
    private static func generateHubSections(container: DependencyContainer) -> [HubSection] {
        var sections = [HubSection]()
        
        if container.featureManager.isActive(.featurePixHubReceiving) {
            sections.append(HubSection(title: Strings.Hub.receive, rows: [.receivePayment]))
        }
        
        var sendSection = HubSection(title: Strings.General.send, rows: [])
        if container.featureManager.isActive(.featurePixHubKeys) {
            sendSection.rows.append(.keySelector)
        }
        if container.featureManager.isActive(.featurePixHubCopyPaste) {
            sendSection.rows.append(.copyPaste)
        }
        if container.featureManager.isActive(.featurePixHubQrCode) {
            sendSection.rows.append(.qrCode)
        }
        if container.featureManager.isActive(.featurePixHubManualBank) {
            sendSection.rows.append(.manualInsert)
        }
        if sendSection.rows.isNotEmpty {
            sections.append(sendSection)
        }

        if container.featureManager.isActive(.featurePixHubManageKeys) {
            sections.append(HubSection(title: Strings.Hub.keyManagement, rows: [.keyManagement]))
        }
        
        if container.featureManager.isActive(.isPixDailyLimitAvailable) {
            sections.append(HubSection(title: Strings.Hub.myLimitPix, rows: [.dailyLimit]))
        }
        return sections
    }
}
