import Foundation

public enum HubItemType {
    case keySelector
    case qrCode
    case manualInsert
    case receivePayment
    case keyManagement
    case copyPaste
    case dailyLimit
}

enum HubSectionType {
    case send
    case receive
    case keyManagement
    case dailyLimit
}
