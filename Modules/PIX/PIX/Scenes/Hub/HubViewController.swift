import AssetsKit
import FeatureFlag
import UI
import UIKit

protocol HubDisplay: AnyObject {
    func showSections(_ sections: [HubSection])
}

private extension HubViewController.Layout {
    enum Size {
        static let estimatedRowHeight: CGFloat = 70
        static let estimatedHeaderHeight: CGFloat = 30
    }
}

public extension HubItemType {
    var image: UIImage {
        switch self {
        case .keySelector:
            return Assets.person.image
        case .qrCode:
            return Assets.qrCode.image
        case .manualInsert:
            return Assets.bank.image
        case .receivePayment:
            return Assets.receive.image
        case .keyManagement:
            return Assets.engine.image
        case .copyPaste:
            return Assets.copyPaste.image
        case .dailyLimit:
            return Assets.pencil.image
        }
    }
    
    var description: String {
        switch self {
        case .keySelector:
            return Strings.Hub.sendByKeysDescription
        case .qrCode:
            return Strings.Hub.scanQrCode
        case .manualInsert:
            return Strings.Hub.insertManually
        case .receivePayment:
            return Strings.Hub.receivePayment
        case .keyManagement:
            return Strings.Hub.insertAndManageYourReceiveMethods
        case .copyPaste:
            return Strings.Hub.copyPaste
        case .dailyLimit:
            return Strings.Hub.defineDailyLimit
        }
    }
}

struct HubSection: Hashable {
    let title: String
    var rows: [HubItemType]
}

final class HubViewController: ViewController<HubInteracting, UIView> {
    fileprivate enum Layout { }

    private var items = [HubSection]()
    
    private lazy var headerView = HubHeaderView()
    
    private lazy var tableViewDataSource: TableViewDataSource<HubSection, HubItemType>? = {
        let dataSource = TableViewDataSource<HubSection, HubItemType>(view: tableView)
        dataSource.itemProvider = { tableView, indexPath, item -> HubTableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: HubTableViewCell.identifier, for: indexPath) as? HubTableViewCell
            cell?.configureCellWithImage(item.image, text: item.description)
            return cell
        }
        return dataSource
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.backgroundColor = Colors.backgroundPrimary.color
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.register(HubTableViewCell.self, forCellReuseIdentifier: HubTableViewCell.identifier)
        tableView.estimatedRowHeight = Layout.Size.estimatedRowHeight
        tableView.estimatedSectionHeaderHeight = Layout.Size.estimatedHeaderHeight
        return tableView
    }()
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.largeTitleTextAttributes = [
                .foregroundColor: Colors.grayscale700.color,
                .font: Typography.title(.large).font()
            ]
            navigationController?.navigationBar.backgroundColor = Colors.backgroundPrimary.color
        }
        interactor.callAccessAnalytics()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if tableView.tableHeaderView == nil {
            setAndLayoutHeader()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.view.backgroundColor = Colors.backgroundPrimary.color
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    override func configureViews() {
        configureNavigation()
        view.backgroundColor = Colors.backgroundPrimary.color
        tableView.dataSource = tableViewDataSource
        tableView.delegate = self
        interactor.buildHubItems()
    }
    
    private func setAndLayoutHeader() {
        let expectedSize = headerView.systemLayoutSizeFitting(
            CGSize(width: view.frame.width, height: .leastNonzeroMagnitude),
            withHorizontalFittingPriority: .required,
            verticalFittingPriority: .defaultLow
        )
        headerView.frame.size = expectedSize
        tableView.tableHeaderView = headerView
        tableView.layoutIfNeeded()
    }
}

// MARK: HubDisplay
extension HubViewController: HubDisplay {
    func showSections(_ sections: [HubSection]) {
        items = sections
        sections.forEach {
            tableViewDataSource?.add(items: $0.rows, to: $0)
        }
    }
}

extension HubViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionView = HubCustomSectionView()
        sectionView.configureWithText(items[section].title)
        return sectionView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor.didNextStepWithType(items[indexPath.section].rows[indexPath.row])
    }
}

private extension HubViewController {
    func configureNavigation() {
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.tintColor = Colors.branding400.color
        navigationItem.backBarButtonItem = UIBarButtonItem(title: Strings.General.pix, style: .plain, target: nil, action: nil)
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: Strings.General.close, style: .plain, target: self, action: #selector(close))
    }
    
    @objc
    private func close() {
        interactor.close()
    }
}
