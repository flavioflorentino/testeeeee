import UI
import UIKit

final class HubCustomSectionView: UIView {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale750())
        label.backgroundColor = Colors.backgroundPrimary.color
        return label
    }()
    
    func configureWithText(_ text: String) {
        titleLabel.text = text
        buildLayout()
    }
}

extension HubCustomSectionView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(titleLabel)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.top.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-Spacing.base00)
        }
    }
}
