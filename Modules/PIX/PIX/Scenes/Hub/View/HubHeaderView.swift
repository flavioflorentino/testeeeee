import UI
import UIKit

final class HubHeaderView: UIView {
    fileprivate enum Layout { }
    
    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView(image: Assets.pixLogo.image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
        label.textColor = Colors.grayscale700.color
        label.text = Strings.Hub.sendOrReceivePayments
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension HubHeaderView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(logoImageView)
        addSubview(messageLabel)
    }
    
    func setupConstraints() {
        logoImageView.snp.makeConstraints {
            $0.top.equalToSuperview().inset(Spacing.base02)
            $0.centerX.equalToSuperview()
        }
        messageLabel.snp.makeConstraints {
            $0.top.equalTo(logoImageView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base05)
            $0.bottom.equalToSuperview().inset(Spacing.base03)
        }
    }
}
