import UI
import UIKit

private extension HubTableViewCell.Layout {
    enum Size {
        static let icon = CGSize(width: 48, height: 48)
    }
}

public final class HubTableViewCell: UITableViewCell {
    fileprivate enum Layout { }
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view
            .viewStyle(CardViewStyle())
            .with(\.backgroundColor, .backgroundPrimary())
        return view
    }()
    
    private lazy var iconImageView = UIImageView()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
        label.backgroundColor = Colors.backgroundPrimary.color
        return label
    }()
    
    func configureCellWithImage(_ image: UIImage, text: String) {
        descriptionLabel.text = text
        iconImageView.image = image
        buildLayout()
    }
}

extension HubTableViewCell: ViewConfiguration {
    public func buildViewHierarchy() {
        contentView.addSubview(containerView)
        containerView.addSubview(iconImageView)
        containerView.addSubview(descriptionLabel)
    }
    
    public func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.bottom.equalToSuperview().inset(Spacing.base00)
        }
        iconImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.icon)
            $0.centerY.equalToSuperview()
            $0.top.greaterThanOrEqualToSuperview().inset(Spacing.base02)
            $0.leading.equalToSuperview().inset(Spacing.base02)
        }
        descriptionLabel.snp.makeConstraints {
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base02)
            $0.trailing.top.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    public func configureViews() {
        contentView.backgroundColor = Colors.backgroundPrimary.color
        selectionStyle = .none
    }
}
