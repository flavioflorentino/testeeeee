public struct PaymentAmountViewModel: Equatable {
    let qrCodePixAccount: PixQrCodeAccount?
    let pixAccount: PixAccount
    let amount: Double?
    let availableAmount: String
    let flowType: PaymentAmountFlowType
    
    public init(pixAccount: PixAccount,
                availableAmount: String,
                flowType: PaymentAmountFlowType,
                qrCodePixAccount: PixQrCodeAccount? = nil,
                amount: Double? = nil) {
        self.pixAccount = pixAccount
        self.qrCodePixAccount = qrCodePixAccount
        self.availableAmount = availableAmount
        self.flowType = flowType
        self.amount = amount
    }
}
