import Foundation

extension String {
    var onlyNumbers: String {
        components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    }
    
    static let brazillianNumberFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "pt_BR")
        formatter.numberStyle = .decimal
        formatter.decimalSeparator = "."
        return formatter
    }()
    
    static let brazillianCurrencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "pt_BR")
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        return formatter
    }()
    
    func currency(hideSymbol hide: Bool = false) -> String {
        guard
            let number = convertToNumber(),
            let formatted = convertToCurrency(number, hideSymbol: hide)
        else {
            return String()
        }
        
        return formatted
    }
    
    func convertToNumber() -> NSNumber? {
        String.brazillianNumberFormatter.number(from: self)
    }
    
    private func convertToCurrency(_ number: NSNumber, hideSymbol: Bool = false) -> String? {
        String.brazillianCurrencyFormatter.numberStyle = hideSymbol ? .decimal : .currency
        return String.brazillianCurrencyFormatter.string(from: number)
    }
    
    func convertIntToDouble() -> Double {
        (onlyNumbers as NSString).doubleValue
    }
    
    func convertCurrencyToDouble() -> Double {
        Double(self) ?? 0
    }
    
    var doubleValue: Double {
        let converter = NumberFormatter()
        converter.decimalSeparator = ","
        
        if let result = converter.number(from: self) {
            return result.doubleValue
        } else {
            converter.decimalSeparator = "."
            if let result = converter.number(from: self) {
                return result.doubleValue
            }
        }
        return 0.0
    }
}
