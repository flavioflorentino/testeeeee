import Foundation
import AnalyticsModule

protocol PaymentAmountInteracting: AnyObject {
    func setupViewInfo()
    func validateSendAmount(with sendAmount: Double)
    func showNextStep(sendAmount: Double)
}

final class PaymentAmountInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let presenter: PaymentAmountPresenting
    private let paymentAmountViewModel: PaymentAmountViewModel
    private let userInfo: KeyManagerBizUserInfo?
    
    init(paymentAmountViewModel: PaymentAmountViewModel, presenter: PaymentAmountPresenting, userInfo: KeyManagerBizUserInfo?, dependencies: Dependencies) {
        self.paymentAmountViewModel = paymentAmountViewModel
        self.presenter = presenter
        self.userInfo = userInfo
        self.dependencies = dependencies
    }
}

// MARK: - PaymentAmountInteracting
extension PaymentAmountInteractor: PaymentAmountInteracting {
    func setupViewInfo() {
        setupAvailableAmountAndEnabledButton()
        presenter.setupViewInfo(pixAccount: paymentAmountViewModel.pixAccount,
                                qrCodeAmount: paymentAmountViewModel.amount ?? 0,
                                flowType: paymentAmountViewModel.flowType)
    }
    
    func validateSendAmount(with sendAmount: Double) {
        let convertedAvailableAmount = paymentAmountViewModel.availableAmount.convertCurrencyToDouble()
        let sufficientFunds = isSufficientFunds(availableAmount: convertedAvailableAmount, sendAmount: sendAmount)
        
        presenter.showAvailableAmount(paymentAmountViewModel.availableAmount, isAvailable: sufficientFunds)
        updateConfirmButton(sufficientFunds: sufficientFunds, sendAmount: sendAmount)
    }
    
    func showNextStep(sendAmount: Double) {
        presenter.didNextStep(
            action: .paymentConfirmation(pixAccount: paymentAmountViewModel.pixAccount,
                                         sendAmount: sendAmount,
                                         flowType: paymentAmountViewModel.flowType,
                                         qrCodeAccount: paymentAmountViewModel.qrCodePixAccount)
        )
    }
}

private extension PaymentAmountInteractor {
    enum PaymentAmountEventType {
        case insufficientFunds
        case maxRefundAmountExceeded
    }

    func setupAvailableAmountAndEnabledButton() {
        let convertedAvailableAmount = paymentAmountViewModel.availableAmount.convertIntToDouble()
        let sufficientFunds = isSufficientFunds(availableAmount: convertedAvailableAmount,
                                                sendAmount: paymentAmountViewModel.amount ?? 0)
        
        presenter.showAvailableAmount(paymentAmountViewModel.availableAmount, isAvailable: sufficientFunds)
        updateConfirmButton(sufficientFunds: sufficientFunds, sendAmount: paymentAmountViewModel.amount ?? 0)
    }
    
    func updateConfirmButton(sufficientFunds: Bool, sendAmount: Double) {
        let enableButton = shouldEnableConfirmButton(sufficientFunds: sufficientFunds, sendAmount: sendAmount)
        presenter.enableConfirmButton(enableButton)
    }
    
    func shouldEnableConfirmButton(sufficientFunds: Bool, sendAmount: Double) -> Bool {
        sufficientFunds && sendAmount != 0.0
    }
    
    func isSufficientFunds(availableAmount: Double, sendAmount: Double) -> Bool {
        availableAmount != 0.0 && sendAmount <= availableAmount
    }
    
    func getFormattedDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.string(from: Date())
    }
    
    func logEvent(eventType: PaymentAmountEventType, sendAmount: Double? = nil) {
        guard let userInfo = userInfo else {
            return
        }
        var event: AnalyticsKeyProtocol
        
        if case .insufficientFunds = eventType, let sendAmount = sendAmount {
            event = PaymentAmountEvent.insufficientFunds(
                sellerId: userInfo.id,
                userName: userInfo.name,
                date: getFormattedDate(),
                valor: sendAmount
            )
        } else {
            event = PaymentAmountEvent.maxRefundAmountExceeded(
                sellerId: userInfo.id,
                userName: userInfo.name,
                date: getFormattedDate()
            )
        }
        dependencies.analytics.log(event)
    }
}
