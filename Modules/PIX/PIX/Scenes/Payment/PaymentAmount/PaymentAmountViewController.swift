import UI
import UIKit

protocol PaymentAmountDisplaying: AnyObject {
    func displayQRCodeAmount(_ amount: Double)
    func enableConfirmButton(_ enable: Bool)
    func updateAvailableAmountLayout(availableAmountTitle: NSAttributedString)
    func displayUserInfoHeader(_ header: AvatarTitleDetailsHeaderModel)
    func addObservers()
    func tapGestureDismissKeyboard()
    func displayTitle(_ text: String)
    func displayAmountDescription(_ text: String)
}

final class PaymentAmountViewController: ViewController<PaymentAmountInteracting, UIView> {
    private lazy var userInfoHeaderView = AvatarTitleDetailsHeaderView()
    
    private lazy var questionLabel: UILabel = {
        var label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale600())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var closeBarButton: UIBarButtonItem = {
        let item = UIBarButtonItem(
            title: Strings.Receipt.Button.Title.close,
            style: .plain,
            target: self,
            action: #selector(didTapClose)
        )
        item.tintColor = Colors.branding300.color
        return item
    }()
    
    private lazy var amountTextField: CurrencyField = {
        let field = CurrencyField()
        field.fontCurrency = Typography.currency(.medium).font()
        field.fontValue = Typography.currency(.medium).font()
        field.placeholderColor = Colors.grayscale300.color
        field.textColor = Colors.branding600.color
        field.spacing = 2
        field.delegate = self
        field.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        return field
    }()
    
    private lazy var availableAmountLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale500())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.General.continue, for: .normal)
        button.addTarget(self, action: #selector(confirmButtonTap), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.setupViewInfo()
    }
    
    override func buildViewHierarchy() {
        view.addSubviews(
            userInfoHeaderView,
            questionLabel,
            amountTextField,
            availableAmountLabel,
            confirmButton
        )
    }
    
    override func setupConstraints() {
        userInfoHeaderView.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top)
            $0.leading.trailing.equalToSuperview()
        }
        
        questionLabel.snp.makeConstraints {
            $0.top.equalTo(userInfoHeaderView.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        amountTextField.snp.makeConstraints {
            $0.top.equalTo(questionLabel.snp.bottom).offset(Spacing.base06)
            $0.leading.greaterThanOrEqualToSuperview().offset(Spacing.base02)
            $0.trailing.lessThanOrEqualToSuperview().offset(-Spacing.base02)
            $0.centerX.equalToSuperview()
        }

        availableAmountLabel.snp.makeConstraints {
            $0.top.equalTo(amountTextField.snp.bottom).offset(Spacing.base06)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        confirmButton.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(availableAmountLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base02)
        }
    }

    override func configureViews() {
        configureNavigation()
    }
    
    func configureNavigation() {
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.backgroundColor = .clear
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.tintColor = Colors.branding400.color
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.view.backgroundColor = .white
        if navigationController?.viewControllers.count == 1 {
            navigationItem.leftBarButtonItem = closeBarButton
        }
        navigationController?.navigationBar.barTintColor = .white
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
            navigationController?.navigationBar.largeTitleTextAttributes = [
                .foregroundColor: Colors.grayscale050.color,
                .font: Typography.title(.large).font()
            ]
        }
    }
}

@objc
extension PaymentAmountViewController {
    func keyboardWillShow(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo,
            let keyboardSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            else {
                return
        }
        
        updateKeyboardConstraint(userInfo: userInfo, inset: keyboardSize.size.height)
    }
    
    func keyboardWillHide(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        updateKeyboardConstraint(userInfo: userInfo, inset: 0.0)
    }
    
    private func updateKeyboardConstraint(userInfo: [AnyHashable: Any], inset: CGFloat) {
        guard let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
            let animationCurve = (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue
            else {
                return
        }
        
        let keyboardHeightWithMargin = inset + Spacing.base02
        
        confirmButton.snp.updateConstraints {
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-keyboardHeightWithMargin)
        }
        
        UIView.animate(
            withDuration: animationDuration,
            delay: 0,
            options: UIView.AnimationOptions(rawValue: animationCurve),
            animations: {
                self.view.layoutIfNeeded()
            },
            completion: nil
        )
    }
    
    func confirmButtonTap() {
        interactor.showNextStep(sendAmount: amountTextField.value)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

// MARK: - PaymentAmountDisplaying
extension PaymentAmountViewController: PaymentAmountDisplaying {
    func displayTitle(_ text: String) {
        title = text
    }
    
    func displayAmountDescription(_ text: String) {
        questionLabel.text = text
        view.layoutIfNeeded()
    }
    
    func displayQRCodeAmount(_ amount: Double) {
        amountTextField.value = amount
    }
    
    func enableConfirmButton(_ enable: Bool) {
        confirmButton.isEnabled = enable
    }

    func updateAvailableAmountLayout(availableAmountTitle: NSAttributedString) {
        availableAmountLabel.attributedText = availableAmountTitle
    }
    
    func displayUserInfoHeader(_ header: AvatarTitleDetailsHeaderModel) {
        userInfoHeaderView.setupView(model: header)
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(_:)),
            name: UIWindow.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: UIWindow.keyboardWillHideNotification,
            object: nil
        )
    }
    
    @objc
    func didTapClose() {
        navigationController?.dismiss(animated: true)
    }
    
    func tapGestureDismissKeyboard() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
}

extension PaymentAmountViewController: CurrencyFieldDelegate {
    func onValueChange(value: Double) {
        interactor.validateSendAmount(with: value)
    }
}
