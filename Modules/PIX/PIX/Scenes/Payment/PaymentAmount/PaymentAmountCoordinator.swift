import UIKit
import UI

enum PaymentAmountAction {
    case paymentConfirmation(pixAccount: PixAccount,
                             sendAmount: Double,
                             flowType: PaymentAmountFlowType,
                             qrCodeAccount: PixQrCodeAccount? = nil)
    case displayExceededRefundPopUp(_ popUp: PopupViewController)
}

protocol PaymentAmountCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: PaymentAmountAction)
}

final class PaymentAmountCoordinator {
    private let paymentAmountBizOutput: PaymentAmountBizOutput

    weak var viewController: UIViewController?

    init(paymentAmountBizOutput: @escaping PaymentAmountBizOutput) {
        self.paymentAmountBizOutput = paymentAmountBizOutput
    }
}

// MARK: - PaymentAmountCoordinating
extension PaymentAmountCoordinator: PaymentAmountCoordinating {
    func perform(action: PaymentAmountAction) {
        switch action {
        case let .displayExceededRefundPopUp(popUp):
            viewController?.showPopup(popUp)
            
        case let .paymentConfirmation(pixAccount, sendAmount, flowType, qrCodeAccount):
            paymentAmountBizOutput(.paymentConfirmation(pixAccount: pixAccount,
                                                        sendAmount: sendAmount,
                                                        flowType: flowType,
                                                        qrCodeAccount: qrCodeAccount))
        }
    }
}
