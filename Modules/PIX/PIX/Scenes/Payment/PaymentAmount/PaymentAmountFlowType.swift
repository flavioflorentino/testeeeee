import Foundation

public enum PaymentAmountFlowType: Equatable {
    case send(pixParams: PIXParams)
    case refund(transactionId: String, maxRefundAmount: Double? = nil, sendAmount: Double? = nil)
}
