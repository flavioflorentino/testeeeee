import Foundation
import Core
import UI
import UIKit

protocol PaymentAmountPresenting: AnyObject {
    var viewController: PaymentAmountDisplaying? { get set }
    func didNextStep(action: PaymentAmountAction)
    func showAvailableAmount(_ availableAmount: String, isAvailable: Bool)
    func enableConfirmButton(_ shouldEnable: Bool)
    func showExceededRefundMessage()
    func setupViewInfo(pixAccount: PixAccount, qrCodeAmount: Double?, flowType: PaymentAmountFlowType)
}

private extension PaymentAmountFlowType {
    var title: String {
        switch self {
        case .send:
            return Strings.Payment.PaymentAmount.Send.title
        case .refund:
            return Strings.Payment.PaymentAmount.Refund.title
        }
    }
    
    var amountDescription: String {
        switch self {
        case .send:
            return Strings.Payment.PaymentAmount.Send.amountQuestion
        case .refund:
            return Strings.Payment.PaymentAmount.Refund.amountQuestion
        }
    }
}

final class PaymentAmountPresenter {
    private let coordinator: PaymentAmountCoordinating
    weak var viewController: PaymentAmountDisplaying?

    init(coordinator: PaymentAmountCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - PaymentAmountPresenting
extension PaymentAmountPresenter: PaymentAmountPresenting {
    func didNextStep(action: PaymentAmountAction) {
        coordinator.perform(action: action)
    }
    
    func showAvailableAmount(_ availableAmount: String, isAvailable: Bool) {
        let availableAmount = availableAmount.currency(hideSymbol: false)
        
        if isAvailable {
            configureAvailableAmount(amount: availableAmount)
        } else {
            configureUnavailableAmount(amount: availableAmount)
        }
    }
    
    func enableConfirmButton(_ shouldEnable: Bool) {
        viewController?.enableConfirmButton(shouldEnable)
    }
    
    func showExceededRefundMessage() {
        let popUp = PopupViewController(
            title: Strings.Payment.PaymentAmount.unallowedAmountTitle,
            description: Strings.Payment.PaymentAmount.unallowedAmountMessage,
            preferredType: .business
        )
        
        let okAction = PopupAction(title: Strings.General.gotIt, style: .fill)
        popUp.addAction(okAction)
        
        coordinator.perform(action: .displayExceededRefundPopUp(popUp))
    }
    
    func setupViewInfo(pixAccount: PixAccount, qrCodeAmount: Double?, flowType: PaymentAmountFlowType) {
        displayQRCodeAmount(qrCodeAmount)
        displayUserInfoHeader(pixAccount: pixAccount)
        
        viewController?.displayTitle(flowType.title)
        viewController?.displayAmountDescription(flowType.amountDescription)
        viewController?.tapGestureDismissKeyboard()
        viewController?.addObservers()
    }
}

private extension PaymentAmountPresenter {
    func displayQRCodeAmount(_ qrCodeAmount: Double?) {
        if let qrCodeAmount = qrCodeAmount {
            viewController?.displayQRCodeAmount(qrCodeAmount)
        }
    }
    
    func displayUserInfoHeader(pixAccount: PixAccount) {
        let header = AvatarTitleDetailsHeaderModelFactory.make(from: pixAccount)
        viewController?.displayUserInfoHeader(header)
    }
    
    func configureAvailableAmount(amount: String) {
        let title = Strings.Payment.PaymentAmount.availableAmountTitle(amount)
        let attributedAvailableAmountTitle = getAmountAttributedString(title: title,
                                                                       boldText: amount,
                                                                       color: Colors.grayscale500.color)
        viewController?.updateAvailableAmountLayout(availableAmountTitle: attributedAvailableAmountTitle)
    }
    
    func configureUnavailableAmount(amount: String) {
        let title = Strings.Payment.PaymentAmount.insufficientFundsMessage(amount)
        let attributedAvailableAmountTitle = getAmountAttributedString(title: title,
                                                                       boldText: amount,
                                                                       color: Colors.critical900.color)
        viewController?.updateAvailableAmountLayout(availableAmountTitle: attributedAvailableAmountTitle)
    }
    
    func getAmountAttributedString(title: String, boldText: String, color: UIColor) -> NSAttributedString {
        let boldAttributes: [NSAttributedString.Key: Any] = [
            .font: Typography.bodySecondary(.highlight).font()
        ]
        let mutableAttributedString = NSMutableAttributedString(string: title)
        mutableAttributedString.textColor(text: title, color: color)
        let attributedRange = (mutableAttributedString.string as NSString).range(of: boldText)
        mutableAttributedString.addAttributes(boldAttributes, range: attributedRange)
        return mutableAttributedString
    }
}
