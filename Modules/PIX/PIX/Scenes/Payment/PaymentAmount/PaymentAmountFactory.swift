import UIKit

typealias PaymentAmountBizOutput = (PaymentAmountAction) -> Void

enum PaymentAmountFactory {
    static func makeSendAmount(
        paymentAmountViewModel: PaymentAmountViewModel,
        paymentAmountBizOutput: @escaping PaymentAmountBizOutput,
        userInfo: KeyManagerBizUserInfo?
    ) -> PaymentAmountViewController {
        let dependencies = DependencyContainer()
        
        let coordinator: PaymentAmountCoordinating = PaymentAmountCoordinator(paymentAmountBizOutput: paymentAmountBizOutput)
        let presenter: PaymentAmountPresenting = PaymentAmountPresenter(coordinator: coordinator)
        let interactor: PaymentAmountInteracting = PaymentAmountInteractor(
            paymentAmountViewModel: paymentAmountViewModel,
            presenter: presenter,
            userInfo: userInfo,
            dependencies: dependencies
        )
        let viewController = PaymentAmountViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
    
    static func makeRefundAmount(
        paymentAmountViewModel: PaymentAmountViewModel,
        paymentAmountBizOutput: @escaping PaymentAmountBizOutput,
        userInfo: KeyManagerBizUserInfo?
    ) -> PaymentAmountViewController {
        let dependencies = DependencyContainer()
        
        let coordinator: PaymentAmountCoordinating = PaymentAmountCoordinator(paymentAmountBizOutput: paymentAmountBizOutput)
        let presenter: PaymentAmountPresenting = PaymentAmountPresenter(coordinator: coordinator)
        let interactor: PaymentAmountInteracting = PaymentAmountRefundInteractor(
            paymentAmountViewModel: paymentAmountViewModel,
            presenter: presenter,
            userInfo: userInfo,
            dependencies: dependencies
        )
        let viewController = PaymentAmountViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
