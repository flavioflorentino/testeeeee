import Foundation

extension Double {
    func convertToCurrency(hasCurrencySymbol: Bool, decimalDigits: Int = 2) -> String? {
        let number = NSNumber(value: self)
        String.brazillianCurrencyFormatter.numberStyle = hasCurrencySymbol ? .currency : .decimal
        String.brazillianCurrencyFormatter.minimumFractionDigits = decimalDigits
        String.brazillianCurrencyFormatter.maximumFractionDigits = decimalDigits
        return String.brazillianCurrencyFormatter.string(from: number)
    }
}
