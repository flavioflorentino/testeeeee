import Foundation
import AnalyticsModule

final class PaymentAmountRefundInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let presenter: PaymentAmountPresenting
    private let paymentAmountViewModel: PaymentAmountViewModel
    private let userInfo: KeyManagerBizUserInfo?
    private var transactionId: String?
    private var maxRefundAmount: Double?
    private var sendAmount: Double?
    
    init(paymentAmountViewModel: PaymentAmountViewModel, presenter: PaymentAmountPresenting, userInfo: KeyManagerBizUserInfo?, dependencies: Dependencies) {
        self.paymentAmountViewModel = paymentAmountViewModel
        self.presenter = presenter
        self.userInfo = userInfo
        self.dependencies = dependencies
        if case let .refund(transactionId, maxRefundAmount, sendAmount) = paymentAmountViewModel.flowType {
            self.transactionId = transactionId
            self.maxRefundAmount = maxRefundAmount
            self.sendAmount = sendAmount
        }
    }
}

// MARK: - PaymentAmountInteracting
extension PaymentAmountRefundInteractor: PaymentAmountInteracting {
    func setupViewInfo() {
        setupAvailableAmountAndEnabledButton()
        presenter.setupViewInfo(pixAccount: paymentAmountViewModel.pixAccount,
                                qrCodeAmount: maxRefundAmount,
                                flowType: paymentAmountViewModel.flowType)
    }
    
    func validateSendAmount(with sendAmount: Double) {
        let convertedAvailableAmount = paymentAmountViewModel.availableAmount.convertCurrencyToDouble()
        let sufficientFunds = isSufficientFunds(availableAmount: convertedAvailableAmount, sendAmount: sendAmount)
        
        presenter.showAvailableAmount(paymentAmountViewModel.availableAmount, isAvailable: sufficientFunds)
        updateConfirmButton(sufficientFunds: sufficientFunds, sendAmount: sendAmount)
    }
    
    func showNextStep(sendAmount: Double) {
        if let maxRefundAmount = maxRefundAmount, sendAmount > maxRefundAmount {
            logEvent(eventType: .maxRefundAmountExceeded)
            presenter.showExceededRefundMessage()
            return
        }
        
        presenter.didNextStep(
            action: .paymentConfirmation(pixAccount: paymentAmountViewModel.pixAccount,
                                         sendAmount: sendAmount,
                                         flowType: paymentAmountViewModel.flowType)
        )
    }
}

private extension PaymentAmountRefundInteractor {
    enum PaymentAmountEventType {
        case insufficientFunds
        case maxRefundAmountExceeded
    }

    func setupAvailableAmountAndEnabledButton() {
        let sendAmount = self.sendAmount ?? 0.0
        let convertedAvailableAmount = paymentAmountViewModel.availableAmount.convertIntToDouble()
        let sufficientFunds = isSufficientFunds(availableAmount: convertedAvailableAmount, sendAmount: sendAmount)
        
        if !sufficientFunds {
            logEvent(eventType: .insufficientFunds, sendAmount: sendAmount)
        }
        
        presenter.showAvailableAmount(paymentAmountViewModel.availableAmount, isAvailable: sufficientFunds)
        updateConfirmButton(sufficientFunds: sufficientFunds, sendAmount: sendAmount)
    }
    
    func updateConfirmButton(sufficientFunds: Bool, sendAmount: Double) {
        let enableButton = shouldEnableConfirmButton(sufficientFunds: sufficientFunds, sendAmount: sendAmount)
        presenter.enableConfirmButton(enableButton)
    }
    
    func shouldEnableConfirmButton(sufficientFunds: Bool, sendAmount: Double) -> Bool {
        sufficientFunds && sendAmount != 0.0
    }
    
    func isSufficientFunds(availableAmount: Double, sendAmount: Double) -> Bool {
        availableAmount != 0.0 && sendAmount <= availableAmount
    }
    
    func getFormattedDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.string(from: Date())
    }
    
    func logEvent(eventType: PaymentAmountEventType, sendAmount: Double? = nil) {
        guard let userInfo = userInfo else {
            return
        }
        var event: AnalyticsKeyProtocol
        
        if case .insufficientFunds = eventType, let sendAmount = sendAmount {
            event = PaymentAmountEvent.insufficientFunds(
                sellerId: userInfo.id,
                userName: userInfo.name,
                date: getFormattedDate(),
                valor: sendAmount
            )
        } else {
            event = PaymentAmountEvent.maxRefundAmountExceeded(
                sellerId: userInfo.id,
                userName: userInfo.name,
                date: getFormattedDate()
            )
        }
        dependencies.analytics.log(event)
    }
}
