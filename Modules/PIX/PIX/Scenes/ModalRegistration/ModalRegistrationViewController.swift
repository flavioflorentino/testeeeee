import UI
import UIKit

protocol ModalRegistrationDisplay: AnyObject {
    func displayDetails(items: [PixModalDetailItem])
    func displayRegisterButton()
    func displayCloseButton()
}

private extension ModalRegistrationViewController.Layout {
    enum Size {
        static let imageHeight = 54
        static let closeButton = CGSize(width: 24, height: 24)
    }
}

public final class ModalRegistrationViewController: ViewController<ModalRegistrationInteracting, UIView> {
    fileprivate enum Layout { }
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.white.color
        view.layer.cornerRadius = CornerRadius.strong
        return view
    }()
    
    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = isSmallScreenDevice ? Spacing.base02 : Spacing.base03
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView(image: Assets.pixLogo.image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.Modal.title
        label.labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var registerButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.Modal.wantToRegister, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(tapRegisterButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.General.notNow, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(tapCloseButton), for: .touchUpInside)
        return button
    }()
    
    private let isSmallScreenDevice = UIScreen.main.bounds.height <= 568
    
    override public func loadView() {
        view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        interactor.fetchModalInformation()
    }

    override public func buildViewHierarchy() {
        view.addSubview(containerView)
        
        containerView.addSubview(containerStackView)
        
        containerStackView.addArrangedSubview(imageView)
        containerStackView.addArrangedSubview(titleLabel)
    }
    
    override public func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.leading.equalToSuperview().offset(isSmallScreenDevice ? Spacing.base01 : Spacing.base02)
            $0.trailing.equalToSuperview().offset(-(isSmallScreenDevice ? Spacing.base01 : Spacing.base02))
        }
        
        containerStackView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base04)
            $0.leading.equalToSuperview().offset(isSmallScreenDevice ? Spacing.base02 : Spacing.base03)
            $0.trailing.bottom.equalToSuperview().offset(-(isSmallScreenDevice ? Spacing.base02 : Spacing.base03))
        }
        
        imageView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.imageHeight)
        }
    }
    
    @objc
    private func tapCloseButton() {
        interactor.didClose()
    }
    
    @objc
    private func tapRegisterButton() {
        interactor.didRegister()
    }
}

// MARK: ModalRegistrationDisplay
extension ModalRegistrationViewController: ModalRegistrationDisplay {
    func displayDetails(items: [PixModalDetailItem]) {
       items.map { PixModalItemView(icon: $0.icon, text: $0.text) }
            .forEach(self.containerStackView.addArrangedSubview)
    }
    
    func displayRegisterButton() {
        containerStackView.addArrangedSubview(registerButton)
    }
    
    func displayCloseButton() {
        containerStackView.addArrangedSubview(closeButton)
    }
}
