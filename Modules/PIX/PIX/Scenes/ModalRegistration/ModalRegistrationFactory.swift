import Foundation
import UIKit

public enum ModalRegistrationFactory {
    public static func make(delegate: ModalRegistrationDelegate?) -> ModalRegistrationViewController {
        let container = DependencyContainer()
        let service: ModalRegistrationServicing = ModalRegistrationService(dependencies: container)
        let coordinator: ModalRegistrationCoordinating = ModalRegistrationCoordinator()
        let presenter: ModalRegistrationPresenting = ModalRegistrationPresenter(coordinator: coordinator)
        let interactor = ModalRegistrationInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = ModalRegistrationViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
