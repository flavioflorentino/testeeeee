import Foundation
import Core

protocol ModalRegistrationServicing {
    func setModalHomeVisualized()
}

final class ModalRegistrationService {
    typealias Dependencies = HasKVStore
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ModalRegistrationServicing
extension ModalRegistrationService: ModalRegistrationServicing {
    func setModalHomeVisualized() {
        dependencies.kvStore.setBool(true, with: KVKey.isPixModalHomeVisualized)
    }
}
