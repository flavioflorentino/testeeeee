import Foundation
import UI

struct PixModalDetailItem {
    let icon: Iconography
    let text: String
}

protocol ModalRegistrationPresenting: AnyObject {
    var viewController: ModalRegistrationDisplay? { get set }
    func presentDetails()
    func presentRegisterButton()
    func presentCloseButton()
    func didNextStep(action: ModalRegistrationAction)
}

final class ModalRegistrationPresenter {
    private let coordinator: ModalRegistrationCoordinating
    weak var viewController: ModalRegistrationDisplay?

    init(coordinator: ModalRegistrationCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ModalRegistrationPresenting
extension ModalRegistrationPresenter: ModalRegistrationPresenting {
    func presentDetails() {
        let items = [
            PixModalDetailItem(icon: Iconography.moneyStack, text: Strings.Modal.Item.payAndReceive),
            PixModalDetailItem(icon: Iconography.keySkeleton, text: Strings.Modal.Item.youCanUse),
            PixModalDetailItem(icon: Iconography.moneyBillSlash, text: Strings.Modal.Item.noTaxes)
        ]
        
        viewController?.displayDetails(items: items)
    }
    
    func presentRegisterButton() {
        viewController?.displayRegisterButton()
    }
    
    func presentCloseButton() {
        viewController?.displayCloseButton()
    }
    
    func didNextStep(action: ModalRegistrationAction) {
        coordinator.perform(action: action)
    }
}
