import UIKit

public protocol ModalRegistrationDelegate: AnyObject {
    func didTapRegisterKeyOnPix()
}

enum ModalRegistrationAction {
    case register
    case close
}

protocol ModalRegistrationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: ModalRegistrationDelegate? { get set }
    func perform(action: ModalRegistrationAction)
}

final class ModalRegistrationCoordinator {
    weak var viewController: UIViewController?
    weak var delegate: ModalRegistrationDelegate?
}

// MARK: - ModalRegistrationCoordinating
extension ModalRegistrationCoordinator: ModalRegistrationCoordinating {
    func perform(action: ModalRegistrationAction) {
        switch action {
        case .register:
            viewController?.dismiss(animated: true) { [weak self] in
                self?.delegate?.didTapRegisterKeyOnPix()
            }
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
