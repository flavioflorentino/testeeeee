import Foundation
import AnalyticsModule

public protocol ModalRegistrationInteracting: AnyObject {
    func fetchModalInformation()
    func didRegister()
    func didClose()
}

final class ModalRegistrationInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: ModalRegistrationServicing
    private let presenter: ModalRegistrationPresenting

    init(service: ModalRegistrationServicing, presenter: ModalRegistrationPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - ModalRegistrationInteracting
extension ModalRegistrationInteractor: ModalRegistrationInteracting {
    func fetchModalInformation() {
        presenter.presentDetails()
        presenter.presentRegisterButton()
        presenter.presentCloseButton()
        service.setModalHomeVisualized()
    }
    
    func didRegister() {
        dependencies.analytics.log(ModalRegistrationEvent.dialogInteracted(action: .register, title: Strings.Modal.title))
        presenter.didNextStep(action: .register)
    }
    
    func didClose() {
        dependencies.analytics.log(ModalRegistrationEvent.dialogInteracted(action: .close, title: Strings.Modal.title))
        presenter.didNextStep(action: .close)
    }
}
