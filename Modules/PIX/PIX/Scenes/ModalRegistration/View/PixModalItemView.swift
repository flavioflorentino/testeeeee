import UI
import UIKit
import SnapKit

private extension PixModalItemView.Layout {
    static let iconLabelSize: CGFloat = 48
}

final class PixModalItemView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private let icon: Iconography
    private let text: String
    
    private var containerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base01
        stackView.alignment = .center
        return stackView
    }()
    
    private lazy var iconLabel: UILabel = {
        let label = UILabel()
        label.text = icon.rawValue
        label.labelStyle(IconLabelStyle(type: .medium))
            .with(\.backgroundColor, .grayscale050())
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.branding600.color)
            .with(\.clipsToBounds, true)
            .with(\.layer.cornerRadius, Layout.iconLabelSize / 2)
        return label
    }()
    
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.text = text
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale600())
        return label
    }()
    
    init(icon: Iconography, text: String) {
        self.icon = icon
        self.text = text
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(containerStackView)
        containerStackView.addArrangedSubview(iconLabel)
        containerStackView.addArrangedSubview(textLabel)
    }
    
    func setupConstraints() {
        containerStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        iconLabel.snp.makeConstraints {
            $0.size.equalTo(Layout.iconLabelSize)
        }
    }
}
