import Foundation

struct SecurityCodeValidationRequestModel: Encodable {
    let type: SecurityCodeValidationType
    let code: String
}
