import Foundation

struct SecurityCodeResendRequestModel: Encodable {
    let type: SecurityCodeValidationType
}
