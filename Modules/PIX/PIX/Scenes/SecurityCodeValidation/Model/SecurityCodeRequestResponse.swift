import Foundation

struct SecurityCodeRequestResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case smsDelay
        case emailDelay
    }
    
    let smsDelay: Int
    let emailDelay: Int
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let parsedSmsDelay = try? container.decode(Int.self, forKey: .smsDelay)
        let parsedEmailDelay = try? container.decode(Int.self, forKey: .emailDelay)
        smsDelay = parsedSmsDelay ?? .zero
        emailDelay = parsedEmailDelay ?? .zero
    }
}
