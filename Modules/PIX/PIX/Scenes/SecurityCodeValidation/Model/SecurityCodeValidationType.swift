import Foundation

enum SecurityCodeValidationType: String, Encodable, Equatable {
    case email
    case phone = "cellphone"
    
    var validationWay: String {
        switch self {
        case .phone:
            return Strings.General.sms
        case .email:
            return Strings.General.email
        }
    }
    
    var description: String {
        switch self {
        case .phone:
            return Strings.General.number
        case .email:
            return Strings.General.email
        }
    }
    
    var delayKey: String {
        switch self {
        case .phone:
            return "smsDelay"
        case .email:
            return "emailDelay"
        }
    }
}
