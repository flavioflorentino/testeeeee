import UIKit

enum SecurityCodeValidationFactory {
    static func make(
        inputedValue: String,
        inputType: SecurityCodeValidationType,
        codeResendDelay: Int?,
        delegate: SecurityValidationCodeDelegate
    ) -> SecurityCodeValidationViewController {
        let container = DependencyContainer()
        let service: SecurityCodeValidationServicing = SecurityCodeValidationService(dependencies: container)
        let coordinator: SecurityCodeValidationCoordinating = SecurityCodeValidationCoordinator(delegate: delegate)
        let presenter: SecurityCodeValidationPresenting = SecurityCodeValidationPresenter(
            coordinator: coordinator,
            dependencies: container
        )
        let interactor = SecurityCodeValidationInteractor(
            service: service,
            presenter: presenter,
            dependencies: container,
            inputType: inputType,
            inputedValue: inputedValue,
            codeResendDelay: codeResendDelay
        )
        let viewController = SecurityCodeValidationViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
