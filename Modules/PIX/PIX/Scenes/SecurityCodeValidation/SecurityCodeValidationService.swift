import Core

protocol SecurityCodeValidationServicing {
    func sendConfirmationCode(
        inputType: SecurityCodeValidationType,
        codeValue: String,
        completion: @escaping (Result<NoContent, ApiError>
    ) -> Void)
    func requestNewCode(
        inputType: SecurityCodeValidationType,
        completion: @escaping (Result<SecurityCodeRequestResponse, ApiError>) -> Void
    )
}

final class SecurityCodeValidationService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - SecurityCodeValidationServicing
extension SecurityCodeValidationService: SecurityCodeValidationServicing {
    func sendConfirmationCode(
        inputType: SecurityCodeValidationType,
        codeValue: String,
        completion: @escaping (Result<NoContent, ApiError>
    ) -> Void) {
        let requestModel = SecurityCodeValidationRequestModel(type: inputType, code: codeValue)
        let endpoint = PixCodeValidationEndpoint.validateCode(requestModel)
        
        Api<NoContent>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func requestNewCode(
        inputType: SecurityCodeValidationType,
        completion: @escaping (Result<SecurityCodeRequestResponse, ApiError>) -> Void
    ) {
        let requestModel = SecurityCodeResendRequestModel(type: inputType)
        let endpoint = PixCodeValidationEndpoint.resendCode(requestModel)
        let decoder = JSONDecoder(.convertFromSnakeCase)

        Api<SecurityCodeRequestResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
