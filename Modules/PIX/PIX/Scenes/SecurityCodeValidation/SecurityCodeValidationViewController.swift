import SkyFloatingLabelTextField
import SnapKit
import UI
import UIKit

protocol SecurityCodeValidationDisplaying: AnyObject {
    func display(message: NSAttributedString)
    func updateResendCodeButton(isHidden: Bool, isEnabled: Bool, title: String?)
    func showLoading()
    func hideLoading()
    func displayGeneralError(title: String, message: String)
    func displayCodeError(message: String)
    func hideCodeErrorMessage()
    func cleanCodeTextField()
    func animateResendSuccess()
}

private extension SecurityCodeValidationViewController.Layout {
    enum Size {
        static let textFieldHeight: CGFloat = 48.0
        static let smallScreenHeight: CGFloat = 568
    }
}

final class SecurityCodeValidationViewController: ViewController<SecurityCodeValidationInteracting, UIView>, LoadingViewProtocol {
    fileprivate enum Layout { }
    
    private let isSmallScreenDevice = UIScreen.main.bounds.height <= Layout.Size.smallScreenHeight
    
    var loadingView = LoadingView()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        return label
    }()
    
    private lazy var codeTextField: SkyFloatingLabelTextField = {
        let textField = SkyFloatingLabelTextField()
        textField.lineHeight = 1
        textField.lineColor = Colors.grayscale300.color
        textField.titleColor = Colors.grayscale300.color
        textField.selectedTitleColor = Colors.grayscale300.color
        textField.selectedLineColor = Colors.branding400.color
        textField.textColor = Colors.grayscale700.color
        textField.titleFormatter = { $0 }
        textField.placeholder = Strings.CodeValidation.textFieldPlaceholder
        
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        textField.keyboardType = .numberPad
        textField.addTarget(self, action: #selector(textFieldEditingChanged(_:)), for: .editingChanged)
        
        return textField
    }()
    
    private lazy var errorLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .critical900())
        return label
    }()
    
    private lazy var resendCodeButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(tapResendCodeButton), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    private lazy var successResendLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.CodeValidation.codeResent
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .success600())
            .with(\.backgroundColor, .backgroundPrimary())
            .with(\.textAlignment, .center)
        label.isHidden = true
        return label
    }()
    
    private let codeMask = CustomStringMask(mask: "0000", maskTokenSet: [.decimalDigits])

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.populateView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !UIAccessibility.isVoiceOverRunning {
            codeTextField.becomeFirstResponder()
        }
    }
    
    override func buildViewHierarchy() {
        view.addSubviews(messageLabel, codeTextField, errorLabel, resendCodeButton, successResendLabel)
    }
    
    override func setupConstraints() {
        messageLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base02)
        }
        
        codeTextField.snp.makeConstraints {
            $0.top.equalTo(messageLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.Size.textFieldHeight)
        }
        
        errorLabel.snp.makeConstraints {
            $0.top.equalTo(codeTextField.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        resendCodeButton.snp.makeConstraints {
            $0.top.equalTo(errorLabel.snp.bottom).offset(isSmallScreenDevice ? Spacing.base02 : Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        successResendLabel.snp.makeConstraints {
            $0.edges.equalTo(resendCodeButton)
        }
    }

    override func configureViews() {
        title = Strings.CodeValidation.title
        view.backgroundColor = Colors.backgroundPrimary.color
        setupNavigationBar()
    }
    
    private func setupNavigationBar() {
        navigationController?.navigationBar.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.isTranslucent = false
        
        if #available(iOS 13.0, *) {
            extendedLayoutIncludesOpaqueBars = true
            navigationController?.navigationItem.largeTitleDisplayMode = .always
            navigationController?.navigationBar.prefersLargeTitles = true
            
            let titleSize: CGFloat = isSmallScreenDevice ? 26 : 30
            navigationController?.navigationBar.largeTitleTextAttributes = [.font: UIFont.boldSystemFont(ofSize: titleSize)]
        }
    }
}

@objc
extension SecurityCodeValidationViewController {
    private func tapResendCodeButton() {
        interactor.didAskToResendCode()
    }
    
    private func textFieldEditingChanged(_ textField: UITextField) {
        codeTextField.text = codeMask.maskedText(from: textField.text)
        interactor.updateCodeValue(textField.text)
    }
}

// MARK: - SecurityCodeValidationDisplaying
extension SecurityCodeValidationViewController: SecurityCodeValidationDisplaying {
    func display(message: NSAttributedString) {
        messageLabel.attributedText = message
    }
    
    func updateResendCodeButton(isHidden: Bool, isEnabled: Bool, title: String?) {
        resendCodeButton.setTitle(title, for: .normal)
        resendCodeButton.buttonStyle(LinkButtonStyle(size: .small))
        resendCodeButton.isEnabled = isEnabled
        resendCodeButton.isHidden = isHidden
    }
    
    func showLoading() {
        codeTextField.isEnabled = false
        startLoadingView()
    }
    
    func hideLoading() {
        codeTextField.isEnabled = true
        stopLoadingView()
    }
    
    func displayGeneralError(title: String, message: String) {
        let popupViewController = PopupViewController(
            title: title,
            description: message,
            preferredType: .text
        )
        
        let confirmAction = PopupAction(title: Strings.General.gotIt, style: .fill)
        popupViewController.addAction(confirmAction)
        
        showPopup(popupViewController)
    }
    
    func displayCodeError(message: String) {
        errorLabel.isHidden = false
        errorLabel.text = message
        codeTextField.selectedLineColor = Colors.critical900.color
        codeTextField.lineColor = Colors.critical900.color
    }
    
    func hideCodeErrorMessage() {
        errorLabel.isHidden = true
        codeTextField.selectedLineColor = Colors.branding400.color
        codeTextField.lineColor = Colors.grayscale300.color
    }
    
    func cleanCodeTextField() {
        codeTextField.text = nil
    }
    
    func animateResendSuccess() {
        successResendLabel.alpha = 0
        successResendLabel.isHidden = false
        UIView.animate(
            withDuration: 0.3,
            animations: {
                self.successResendLabel.alpha = 1
            }, completion: { _ in
                UIView.animate(
                    withDuration: 0.3,
                    delay: 1.2,
                    animations: {
                        self.successResendLabel.alpha = 0
                    }, completion: { _ in
                        self.successResendLabel.isHidden = true
                    }
                )
            }
        )
    }
}
