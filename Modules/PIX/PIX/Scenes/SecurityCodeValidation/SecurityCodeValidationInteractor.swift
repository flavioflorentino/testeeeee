import AnalyticsModule
import Core

protocol SecurityCodeValidationInteracting: AnyObject {
    func populateView()
    func didAskToResendCode()
    func updateCodeValue(_ inputValue: String?)
}

final class SecurityCodeValidationInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let service: SecurityCodeValidationServicing
    private let presenter: SecurityCodeValidationPresenting
    
    private let validationCodeFullLength = 4
    private let inputType: SecurityCodeValidationType
    private let inputedValue: String
    private let initialCodeResendDelay: Int
    private var timer: Timer?

    init(
        service: SecurityCodeValidationServicing,
        presenter: SecurityCodeValidationPresenting,
        dependencies: Dependencies,
        inputType: SecurityCodeValidationType,
        inputedValue: String,
        codeResendDelay: Int?
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.inputType = inputType
        self.inputedValue = inputedValue
        initialCodeResendDelay = codeResendDelay ?? 0
    }
    
    private func startDecreasingCounter(from initialValue: Int) {
        timer?.invalidate()
        let initialTime = Date()
        
        guard initialValue > .zero else {
            return
        }
        
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { [weak self] _ in
            guard let self = self else {
                return
            }
            
            self.scheduledTimer(
                initialTime: initialTime,
                initialValue: initialValue,
                update: { interval in
                    self.presenter.updateResendCodeButton(state: .disabled(seconds: interval), inputType: self.inputType)
                },
                completion: {
                    self.timer?.invalidate()
                    self.presenter.updateResendCodeButton(state: .enabled, inputType: self.inputType)
                }
            )
        })
        
        timer?.fire()
    }
    
    private func scheduledTimer(initialTime: Date, initialValue: Int, update: (Int) -> Void, completion: () -> Void) {
        let timeFromStartUntilNow = -Int(initialTime.timeIntervalSinceNow)
        let timeLeft = initialValue - timeFromStartUntilNow
        timeLeft <= .zero ? completion() : update(timeLeft)
    }
    
    private func validateCode(_ code: String) {
        presenter.showLoading()
        service.sendConfirmationCode(inputType: inputType, codeValue: code) { [weak self] result in
            guard let self = self else { return }
            self.presenter.hideLoading()
            
            switch result {
            case .success:
                self.presenter.didNextStep(action: .successfulValidation(for: self.inputType, inputValue: self.inputedValue))
            case .failure(let error):
                self.processCodeValidationError(error: error)
            }
        }
    }
    
    private func processCodeValidationError(error: ApiError) {
        presenter.updateResendCodeButton(state: .hidden, inputType: inputType)
        presenter.cleanCodeTextField()
        startCounterIfResendDelay(in: error)
        
        if case .badRequest(body: let requestError) = error {
            presenter.presentCodeError(message: requestError.message)
            trackError(message: requestError.message, presentation: .textfield)
        } else {
            presenter.hideCodeError()
            let requestError = error.requestError ?? RequestError()
            presenter.presentGeneralError(title: requestError.title, message: requestError.message)
            trackError(message: requestError.message, presentation: .dialog)
        }
    }
    
    private func startCounterIfResendDelay(in error: ApiError) {
        guard
            let data = error.requestError?.data,
            let delay = data[inputType.delayKey] as? Int
            else {
                return
        }
        startDecreasingCounter(from: delay)
    }
    
    private func trackScreenViewed() {
        dependencies.analytics.log(SecureCodeValidationEvent.viewed(type: inputType))
    }
    
    private func trackCodeTyped() {
        dependencies.analytics.log(SecureCodeValidationEvent.codeInputed(type: inputType))
    }
    
    private func trackResendCodeTapped() {
        dependencies.analytics.log(SecureCodeValidationEvent.resendCodeTapped(type: inputType))
    }
    
    private func trackError(message: String, presentation: SecureCodeValidationEvent.ErrorPresentationStyle) {
        let errorEvent = SecureCodeValidationEvent.errorPresented(
            message: message,
            type: inputType,
            presentation: presentation
        )
        dependencies.analytics.log(errorEvent)
    }
}

// MARK: - SecurityCodeValidationInteracting
extension SecurityCodeValidationInteractor: SecurityCodeValidationInteracting {
    func populateView() {
        presenter.populateView(for: inputType, inputedValue: inputedValue)
        startDecreasingCounter(from: initialCodeResendDelay)
        trackScreenViewed()
    }
    
    func didAskToResendCode() {
        trackResendCodeTapped()
        presenter.showLoading()
        presenter.cleanCodeTextField()
        
        service.requestNewCode(inputType: inputType) { [weak self] result in
            guard let self = self else { return }
            
            self.presenter.hideLoading()
            
            switch result {
            case .success (let response):
                self.presenter.hideCodeError()
                self.presenter.animateResendSuccess()
                let delay = self.inputType == .email ? response.emailDelay : response.smsDelay
                self.startDecreasingCounter(from: delay)
            case .failure(let error):
                let requestError = error.requestError ?? RequestError()
                self.presenter.presentGeneralError(title: requestError.title, message: requestError.message)
                self.trackError(message: requestError.message, presentation: .dialog)
            }
        }
    }
    
    func updateCodeValue(_ inputValue: String?) {
        guard let code = inputValue, code.count == validationCodeFullLength else {
            return
        }
        
        trackCodeTyped()
        validateCode(code)
    }
}
