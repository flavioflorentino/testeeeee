import FeatureFlag
import UI

enum SecurityCodeResendButtonState: Equatable {
    case enabled
    case disabled(seconds: Int)
    case hidden
}

protocol SecurityCodeValidationPresenting: AnyObject {
    var viewController: SecurityCodeValidationDisplaying? { get set }
    func didNextStep(action: SecurityCodeValidationAction)
    func populateView(for inputType: SecurityCodeValidationType, inputedValue: String)
    func updateResendCodeButton(state: SecurityCodeResendButtonState, inputType: SecurityCodeValidationType)
    func presentGeneralError(title: String, message: String)
    func presentCodeError(message: String)
    func hideCodeError()
    func showLoading()
    func hideLoading()
    func cleanCodeTextField()
    func animateResendSuccess()
}

final class SecurityCodeValidationPresenter {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    private let coordinator: SecurityCodeValidationCoordinating
    weak var viewController: SecurityCodeValidationDisplaying?

    init(coordinator: SecurityCodeValidationCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    private func timeDescription(seconds: Int) -> String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.minute, .second]
        formatter.zeroFormattingBehavior = [ .pad ]
        formatter.unitsStyle = .positional
        return formatter.string(from: TimeInterval(seconds)) ?? String(seconds)
    }
    
    private func obfuscateDataIfNeeded(for inputType: SecurityCodeValidationType, inputedValue: String) -> String {
        guard dependencies.featureManager.isActive(.featureAppSecObfuscationMask) else {
            return inputedValue
        }
        
        switch inputType {
        case .email:
            return StringObfuscationMasker.mask(email: inputedValue) ?? inputedValue
        case .phone:
            return StringObfuscationMasker.mask(phone: removeCellPhoneMask(inputedValue)) ?? inputedValue
        }
    }
    
    private func removeCellPhoneMask(_ value: String) -> String {
        value.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    }
}

// MARK: - SecurityCodeValidationPresenting
extension SecurityCodeValidationPresenter: SecurityCodeValidationPresenting {
    func didNextStep(action: SecurityCodeValidationAction) {
        coordinator.perform(action: action)
    }
    
    func populateView(for inputType: SecurityCodeValidationType, inputedValue: String) {
        let obfuscatedValue = obfuscateDataIfNeeded(for: inputType, inputedValue: inputedValue)
        let formatedValueText = inputType == .email ? obfuscatedValue : "\(Strings.CodeValidation.yourPhone) \(obfuscatedValue)"
        let message = Strings.CodeValidation.message(formatedValueText, inputType.description)
        let attributedMessage = message.attributedStringWithFont(
            primary: Typography.bodyPrimary(.default).font(),
            secondary: Typography.bodyPrimary(.highlight).font(),
            separator: "[b]"
        )
        viewController?.display(message: attributedMessage)
    }
    
    func updateResendCodeButton(state: SecurityCodeResendButtonState, inputType: SecurityCodeValidationType) {
        var isHidden = false
        var isEnabled = false
        let title: String
        
        switch state {
        case .enabled:
            isEnabled = true
            title = Strings.CodeValidation.resentCode
        case .disabled(let seconds):
            title = Strings.CodeValidation.requestNewCode(timeDescription(seconds: seconds))
        case .hidden:
            isHidden = true
            title = String()
        }
        viewController?.updateResendCodeButton(isHidden: isHidden, isEnabled: isEnabled, title: title)
    }
    
    func presentGeneralError(title: String, message: String) {
        viewController?.displayGeneralError(title: title, message: message)
    }
    
    func presentCodeError(message: String) {
        viewController?.displayCodeError(message: message)
    }
    
    func hideCodeError() {
        viewController?.hideCodeErrorMessage()
    }
    
    func showLoading() {
        viewController?.showLoading()
    }
    
    func hideLoading() {
        viewController?.hideLoading()
    }
    
    func cleanCodeTextField() {
        viewController?.cleanCodeTextField()
    }
    
    func animateResendSuccess() {
        viewController?.animateResendSuccess()
    }
}
