import UIKit

protocol SecurityValidationCodeDelegate: AnyObject {
    func didValidateKeyWithSuccess(keyValue: String, keyType: PixKeyType)
}

enum SecurityCodeValidationAction: Equatable {
    case successfulValidation(for: SecurityCodeValidationType, inputValue: String)
}

protocol SecurityCodeValidationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: SecurityCodeValidationAction)
}

final class SecurityCodeValidationCoordinator {
    weak var viewController: UIViewController?
    private weak var delegate: SecurityValidationCodeDelegate?
    
    init(delegate: SecurityValidationCodeDelegate) {
        self.delegate = delegate
    }
}

// MARK: - SecurityCodeValidationCoordinating
extension SecurityCodeValidationCoordinator: SecurityCodeValidationCoordinating {
    func perform(action: SecurityCodeValidationAction) {
        if case let .successfulValidation(inputType, inputValue) = action {
            viewController?.navigationController?.popViewController(animated: true)
            
            let keyType: PixKeyType = inputType == .phone ? .phone : .email
            delegate?.didValidateKeyWithSuccess(keyValue: inputValue, keyType: keyType)
        }
    }
}
