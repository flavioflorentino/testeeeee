import Foundation
import UI
import UIKit

protocol CopyPasteDisplaying: AnyObject {
    func displaySuccessFeedBackField()
    func displayErrorFeedBackField()
}

final class CopyPasteViewController: ViewController<CopyPasteInteracting, UIView> {
    fileprivate typealias Localizable = Strings.Payment.CopyPaste
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, Colors.grayscale800.color)
            .with(\.text, Localizable.title)
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale600.color)
            .with(\.text, Localizable.subtitle)
        return label
    }()
    
    private lazy var copyPasteTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.placeholder
        textField.placeholderColor = Colors.grayscale300.color
        textField.title = Localizable.placeholder
        textField.titleColor = Colors.grayscale300.color
        textField.selectedTitle = Localizable.placeholder
        textField.titleErrorColor = Colors.grayscale300.color
        textField.selectedTitleColor = Colors.grayscale300.color
        textField.textColor = Colors.grayscale700.color
        textField.font = Typography.bodyPrimary(.default).font()
        textField.layer.borderWidth = Border.none
        textField.delegate = self
        textField.addTarget(self, action: #selector(checkCopyPasteTextField), for: .editingChanged)
        return textField
    }()
    
    private lazy var errorMessageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.critical600.color)
            .with(\.text, Localizable.invalidCode)
            .with(\.isHidden, true)
        return label
    }()
    
    private lazy var continueButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.General.continue, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapContinue), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    override func configureViews() {
        addObservers()
        addTapGesture()
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.isTranslucent = false
        view.backgroundColor = Colors.backgroundPrimary.color
        title = Localizable.View.title
    }
    
    override func buildViewHierarchy() {
        view.addSubviews(titleLabel,
                         subtitleLabel,
                         copyPasteTextField,
                         errorMessageLabel,
                         continueButton)
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        copyPasteTextField.snp.makeConstraints {
            $0.top.equalTo(subtitleLabel.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        errorMessageLabel.snp.makeConstraints {
            $0.top.equalTo(copyPasteTextField.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        continueButton.snp.makeConstraints {                    $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base02)
        }
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIWindow.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIWindow.keyboardWillHideNotification,
            object: nil
        )
    }
    
    func addTapGesture() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyBoard))
        view.addGestureRecognizer(gesture)
    }
}

extension CopyPasteViewController: CopyPasteDisplaying {
    func displaySuccessFeedBackField() {
        applyTextFieldFeedback(errorMessage: "")
    }
    
    func displayErrorFeedBackField() {
        applyTextFieldFeedback(errorMessage: Localizable.placeholder)
    }
    
    private func applyTextFieldFeedback(errorMessage: String) {
        errorMessageLabel.isHidden = errorMessage.isEmpty
        copyPasteTextField.errorMessage = errorMessage
        continueButton.isEnabled = errorMessage.isEmpty
    }
}

private extension CopyPasteViewController {
    func updateKeyboardConstraint(userInfo: [AnyHashable: Any], inset: CGFloat) {
        guard let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
              let animationCurve = (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue
        else {
            return
        }
        let keyboardHeightWithMargin = inset + Spacing.base02
        continueButton.snp.updateConstraints {
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-keyboardHeightWithMargin)
        }
        UIView.animate(
            withDuration: animationDuration,
            delay: 0,
            options: UIView.AnimationOptions(rawValue: animationCurve),
            animations: {
                self.view.layoutIfNeeded()
            },
            completion: nil
        )
    }
}

@objc
extension CopyPasteViewController {
    func checkCopyPasteTextField(_ textField: UIPPFloatingTextField) {
        interactor.validatePixKey(textField.text ?? "")
    }
    
    func didTapContinue(_ button: UIButton) {
        interactor.sendCopyPaste(with: copyPasteTextField.text ?? "")
    }
    
    func keyboardWillShow(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo,
              let keyboardSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else {
            return
        }
        updateKeyboardConstraint(userInfo: userInfo, inset: keyboardSize.size.height)
    }
    
    func keyboardWillHide(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        updateKeyboardConstraint(userInfo: userInfo, inset: 0.0)
    }
    
    func hideKeyBoard() {
        copyPasteTextField.resignFirstResponder()
    }
}

// MARK: - UITextFieldDelegate
extension CopyPasteViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
