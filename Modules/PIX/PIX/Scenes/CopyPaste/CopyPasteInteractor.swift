import Foundation

extension CopyPasteInteractor.Layout {
    enum PixKey {
        static let codeVerificator: String = "BR.GOV.BCB.PIX"
    }
}

protocol CopyPasteInteracting: AnyObject {
    func validatePixKey(_ key: String)
    func sendCopyPaste(with code: String)
}

final class CopyPasteInteractor {
    fileprivate enum Layout { }
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies
    
    private let presenter: CopyPastePresenting
    private let userInfo: KeyManagerBizUserInfo?
    
    init(presenter: CopyPastePresenting, userInfo: KeyManagerBizUserInfo?, dependencies: Dependencies) {
        self.presenter = presenter
        self.userInfo = userInfo
        self.dependencies = dependencies
    }
}

extension CopyPasteInteractor: CopyPasteInteracting {
    func validatePixKey(_ key: String) {
        key.lowercased().contains(Layout.PixKey.codeVerificator.lowercased()) ?
            presenter.presentSuccessFeedBackField() :
            presenter.presentErrorFeedBackField()
    }
    
    func sendCopyPaste(with code: String) {
        presenter.sendCopyPaste(with: code)
    }
}
