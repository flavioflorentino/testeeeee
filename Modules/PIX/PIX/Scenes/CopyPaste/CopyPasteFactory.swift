import UIKit

typealias CopyPasteOutput = (CopyPastePaymentAction) -> Void

enum CopyPasteFactory {
    static func make(copyPasteOutput: @escaping CopyPasteOutput,
                     userInfo: KeyManagerBizUserInfo?) -> CopyPasteViewController {
        let dependencies = DependencyContainer()
        
        let coordinator: CopyPasteCoordinating = CopyPasteCoordinator(copyPasteOutput: copyPasteOutput)
        let presenter: CopyPastePresenting = CopyPastePresenter(coordinator: coordinator)
        
        let interactor: CopyPasteInteracting = CopyPasteInteractor(presenter: presenter,
                                                                   userInfo: userInfo,
                                                                   dependencies: dependencies)
        
        let viewController = CopyPasteViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
