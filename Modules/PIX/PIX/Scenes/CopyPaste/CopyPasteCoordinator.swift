import Foundation

enum CopyPastePaymentAction {
    case continuePayment(code: String)
}

protocol CopyPasteCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: CopyPastePaymentAction)
}

final class CopyPasteCoordinator {
    weak var viewController: UIViewController?
    private let copyPasteOutput: CopyPasteOutput

    init(copyPasteOutput: @escaping CopyPasteOutput) {
        self.copyPasteOutput = copyPasteOutput
    }
}

extension CopyPasteCoordinator: CopyPasteCoordinating {
    func perform(action: CopyPastePaymentAction) {
        copyPasteOutput(action)
    }
}
