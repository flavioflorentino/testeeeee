import Foundation

protocol CopyPastePresenting: AnyObject {
    var viewController: CopyPasteDisplaying? { get set }
    func presentSuccessFeedBackField()
    func presentErrorFeedBackField()
    func sendCopyPaste(with code: String)
}

final class CopyPastePresenter {
    private let coordinator: CopyPasteCoordinating
    weak var viewController: CopyPasteDisplaying?

    init(coordinator: CopyPasteCoordinating) {
        self.coordinator = coordinator
    }
}

extension CopyPastePresenter: CopyPastePresenting {
    func presentSuccessFeedBackField() {
        viewController?.displaySuccessFeedBackField()
    }
    
    func presentErrorFeedBackField() {
        viewController?.displayErrorFeedBackField()
    }
    
    func sendCopyPaste(with code: String) {
        coordinator.perform(action: .continuePayment(code: code))
    }
}
