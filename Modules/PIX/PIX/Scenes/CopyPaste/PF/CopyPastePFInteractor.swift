import AnalyticsModule
import Core
import Foundation

protocol CopyPastePFInteracting: AnyObject {
    func callScreenAnalytics()
    func forwardAction(withCode code: String)
    func validateBrCode(_ code: String)
    func close()
    func showCopiedCode()
}

final class CopyPastePFInteractor {
    // MARK: - Typealias
    typealias Dependencies = HasAnalytics

    // MARK: - Variables
    private let presenter: CopyPastePFPresenting
    private let service: PixQrCodeServicing
    private let brCodeDomain = "br.gov.bcb.pix"
    private let origin: PixUserNavigation
    private let dependencies: Dependencies
    private let copiedCode: String
    // MARK: - Life Cycle
    init(
        presenter: CopyPastePFPresenting,
        service: PixQrCodeServicing,
        origin: PixUserNavigation,
        copiedCode: String,
        dependencies: Dependencies = DependencyContainer()
    ) {
        self.presenter = presenter
        self.service = service
        self.origin = origin
        self.dependencies = dependencies
        self.copiedCode = copiedCode
    }
}

// MARK: - CopyPastePFInteracting
extension CopyPastePFInteractor: CopyPastePFInteracting {
    func callScreenAnalytics() {
        dependencies.analytics.log(PixNavigationEvent.userNavigated(from: origin, to: .sendCopyPasteScreen))
    }

    func validateBrCode(_ code: String) {
        if code.lowercased().contains(brCodeDomain) {
            presenter.enableForwardButton()
        } else {
            presenter.disableForwardButton()
        }
    }

    func forwardAction(withCode code: String) {
        service.validate(scannedText: code) { [weak self] result in
            switch result {
            case let .success(account):
                self?.handleBrCodeSuccess(scannedText: code, account: account)
            case let .failure(error):
                self?.handleBrCodeFailure(error: error)
            }
        }
    }

    private func handleBrCodeSuccess(scannedText: String, account: PixQrCodeAccount) {
        let payee = PIXPayee(
            name: account.name,
            description: "\(account.cpfCnpj) | \(account.bank)",
            imageURLString: account.imageUrl,
            id: account.id
        )
        let pixParams = PIXParams(
            originType: .qrCode,
            keyType: "QRCODE",
            key: scannedText,
            receiverData: nil
        )
        var qrCodeItems = [account.qrcodeInfos.payerData]
        if let receiverData = account.qrcodeInfos.receiverData {
            qrCodeItems.append(receiverData)
        }
        let details = PIXPaymentDetails(
            title: Strings.PaymentScreen.title,
            screenType: .qrCode(items: qrCodeItems),
            payee: payee,
            pixParams: pixParams,
            value: account.value,
            isEditable: account.editable
        )
        presenter.presentPayment(with: details)
    }

    private func handleBrCodeFailure(error: ApiError) {
        guard
            let alert = error.requestError?.alert,
            let title = alert.title?.string,
            let subtitle = alert.text?.string
            else {
                presenter.presentGenericError()
                return
            }
        presenter.presentError(
            title: title,
            subtitle: subtitle,
            jsonButtons: alert.buttonsJson
        )
    }
    
    func showCopiedCode() {
        if copiedCode.isNotEmpty {
            presenter.presentCopiedCode(code: copiedCode)
        }
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
}
