import UIKit

public enum CopyPastePFFactory {
    public static func make(paymentOpening: PIXPaymentOpening, origin: PixUserNavigation, code: String = String()) -> UIViewController {
        let coordinator: CopyPastePFCoordinating = CopyPastePFCoordinator(paymentOpening: paymentOpening)
        let presenter: CopyPastePFPresenting = CopyPastePFPresenter(coordinator: coordinator)
        let service = PixQrCodeService(dependencies: DependencyContainer())
        let interactor = CopyPastePFInteractor(presenter: presenter, service: service, origin: origin, copiedCode: code)
        let viewController = CopyPastePFViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController
        return viewController
    }
}
