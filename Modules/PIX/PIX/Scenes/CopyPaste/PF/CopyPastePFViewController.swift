import Core
import Foundation
import UI
import UIKit

protocol CopyPastePFDisplaying: AnyObject {
    func displayError(title: String, subtitle: String, actions: [PopupAction])
    func displayDialog(with alertData: StatusAlertData)
    func displayEndState()
    func enableForwardButton()
    func disableForwardButton()
    func displayCopiedCode(code: String)
}

final class CopyPastePFViewController: ViewController<CopyPastePFInteracting, UIView> {
    fileprivate typealias Localizable = Strings.Payment.CopyPaste

    // MARK: - Visual Components
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, Colors.grayscale800.color)
            .with(\.text, Localizable.title)
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale600.color)
            .with(\.text, Localizable.Pf.subtitle)
        return label
    }()

    private lazy var copyPasteTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = Localizable.placeholder
        textField.selectedTitle = Localizable.placeholder
        textField.addTarget(self, action: #selector(textFieldEditingChanged), for: .editingChanged)
        return textField
    }()
    
    private lazy var continueButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.General.continue, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(tapOnContinue), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.callScreenAnalytics()
        interactor.showCopiedCode()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.backgroundColor = Colors.backgroundPrimary.color
    }
    
    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(subtitleLabel)
        view.addSubview(copyPasteTextField)
        view.addSubview(continueButton)
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        copyPasteTextField.snp.makeConstraints {
            $0.top.equalTo(subtitleLabel.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        continueButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).inset(Spacing.base02)
        }
    }
    
    override func configureViews() {
        addObservers()
        configNavigation()
        navigationController?.navigationBar.isTranslucent = false
        view.backgroundColor = Colors.backgroundPrimary.color
        title = Localizable.View.title
    }
    
    private func configNavigation() {
        guard navigationController?.viewControllers.count == 1 else {
            return
        }
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: Strings.General.close, style: .plain, target: self, action: #selector(close))
    }
    
    // MARK: - Keyboard
    private func addObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIWindow.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIWindow.keyboardWillHideNotification,
            object: nil
        )
    }
    
    @objc
    private func keyboardWillShow(_ notification: NSNotification) {
        guard
            let userInfo = notification.userInfo,
            let keyboardSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else {
            return
        }
        updateKeyboardConstraint(userInfo: userInfo, inset: keyboardSize.size.height)
    }
    
    @objc
    private func keyboardWillHide(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        updateKeyboardConstraint(userInfo: userInfo, inset: 0.0)
    }
    
    private func updateKeyboardConstraint(userInfo: [AnyHashable: Any], inset: CGFloat) {
        guard
            let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
            let animationCurve = (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue
        else {
            return
        }
        let keyboardHeightWithMargin = inset + Spacing.base02
        continueButton.snp.updateConstraints {
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-keyboardHeightWithMargin)
        }
        UIView.animate(
            withDuration: animationDuration,
            delay: 0,
            options: UIView.AnimationOptions(rawValue: animationCurve),
            animations: {
                self.view.layoutIfNeeded()
            },
            completion: nil
        )
    }
    
    // MARK: - Actions
    @objc
    private func tapOnContinue() {
        beginState()
        copyPasteTextField.resignFirstResponder()
        interactor.forwardAction(withCode: copyPasteTextField.text ?? "")
    }

    @objc
    private func textFieldEditingChanged() {
        interactor.validateBrCode(copyPasteTextField.text ?? "")
    }
    
    @objc
    private func close() {
        interactor.close()
    }
}

// MARK: - CopyPastePFDisplaying
extension CopyPastePFViewController: CopyPastePFDisplaying {
    func displayDialog(with alertData: StatusAlertData) {
        endState()
        StatusAlertView.show(on: view, data: alertData)
    }

    func displayEndState() {
        endState()
    }
    
    func displayError(title: String, subtitle: String, actions: [PopupAction]) {
        let controller = PopupViewController(title: title,
                                             description: subtitle,
                                             preferredType: .text)
        actions.forEach(controller.addAction)
        showPopup(controller)
        endState()
    }

    func enableForwardButton() {
        continueButton.isEnabled = true
    }

    func disableForwardButton() {
        continueButton.isEnabled = false
    }
    
    func displayCopiedCode(code: String) {
        copyPasteTextField.text = code
        copyPasteTextField.sendActions(for: .editingChanged)
    }
}

// MARK: - UITextFieldDelegate
extension CopyPastePFViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

// MARK: StatefulProviding
extension CopyPastePFViewController: StatefulProviding {}
