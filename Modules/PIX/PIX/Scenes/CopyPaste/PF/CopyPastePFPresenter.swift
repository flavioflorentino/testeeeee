import AssetsKit
import Foundation
import Core
import UI

protocol CopyPastePFPresenting: AnyObject {
    var viewController: CopyPastePFDisplaying? { get set }
    func didNextStep(action: CopyPastePFAction)
    func presentPayment(with details: PIXPaymentDetails)
    func presentGenericError()
    func presentError(title: String, subtitle: String, jsonButtons: [[String: Any]])
    func enableForwardButton()
    func disableForwardButton()
    func presentCopiedCode(code: String)
}

final class CopyPastePFPresenter {
    // MARK: - Variables
    private let coordinator: CopyPastePFCoordinating
    weak var viewController: CopyPastePFDisplaying?

    // MARK: - Life Cycle
    init(coordinator: CopyPastePFCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - CopyPastePFPresenting
extension CopyPastePFPresenter: CopyPastePFPresenting {
    func didNextStep(action: CopyPastePFAction) {
        coordinator.perform(action: action)
    }
    
    func presentPayment(with details: PIXPaymentDetails) {
        viewController?.displayEndState()
        coordinator.goToPayment(with: details)
    }

    func presentGenericError() {
        let alertData = StatusAlertData(
            icon: Resources.Illustrations.iluFalling.image,
            title: Strings.General.somethingWentWrong,
            text: Strings.General.requestNotCompleted,
            buttonTitle: Strings.General.tryAgain
        )
        viewController?.displayDialog(with: alertData)
    }

    func presentError(title: String, subtitle: String, jsonButtons: [[String: Any]]) {
        let actions = jsonButtons.compactMap(createPopupAction)
        viewController?.displayError(title: title, subtitle: subtitle, actions: actions)
    }

    private func createPopupAction(jsonButton: [String: Any]) -> PopupAction? {
        guard let title = jsonButton["title"] as? String else {
            return nil
        }
        return PopupAction(title: title, style: .fill)
    }

    func enableForwardButton() {
        viewController?.enableForwardButton()
    }

    func disableForwardButton() {
        viewController?.disableForwardButton()
    }
    
    func presentCopiedCode(code: String) {
        viewController?.displayCopiedCode(code: code)
    }
}
