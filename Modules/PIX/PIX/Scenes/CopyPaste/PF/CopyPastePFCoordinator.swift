import Foundation

enum CopyPastePFAction {
    case close
}

protocol CopyPastePFCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: CopyPastePFAction)
    func goToPayment(with details: PIXPaymentDetails)
}

final class CopyPastePFCoordinator {
    // MARK: - Variables
    weak var viewController: UIViewController?
    let paymentOpening: PIXPaymentOpening

    init(paymentOpening: PIXPaymentOpening) {
        self.paymentOpening = paymentOpening
    }
}

// MARK: - CopyPastePFCoordinating
extension CopyPastePFCoordinator: CopyPastePFCoordinating {
    func perform(action: CopyPastePFAction) {
        switch action {
        case .close:
            viewController?.navigationController?.dismiss(animated: true)
        }
    }
    
    func goToPayment(with details: PIXPaymentDetails) {
        guard let viewController = viewController else {
            return
        }

        paymentOpening.openPaymentWithDetails(details, onViewController: viewController, origin: .copyPaste, flowType: .default)
    }
}
