import UIKit

struct AvatarTitleDetailsHeaderModel {
    let title: String
    let avatarUrl: String?
    let details: [String]
}
