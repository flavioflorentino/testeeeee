enum AvatarTitleDetailsHeaderModelFactory {
    static func make(from pixAccount: PixAccount) -> AvatarTitleDetailsHeaderModel {
        var details = [pixAccount.cpfCnpj, pixAccount.bank]
        if let branch = pixAccount.agency, !branch.isEmpty {
            if branch.contains(Strings.Header.Bank.branchPlaceholder) {
                details.append(branch)
            } else {
                details.append(Strings.Header.Bank.branch(branch))
            }
        }
        if let accountNumber = pixAccount.accountNumber, !accountNumber.isEmpty {
            if accountNumber.contains(Strings.Header.Bank.accountNumberPlaceholder) {
                details.append(accountNumber)
            } else {
                details.append(Strings.Header.Bank.accountNumber(accountNumber))
            }
        }
        return AvatarTitleDetailsHeaderModel(title: pixAccount.name,
                                             avatarUrl: pixAccount.imageUrl,
                                             details: details)
    }
}
