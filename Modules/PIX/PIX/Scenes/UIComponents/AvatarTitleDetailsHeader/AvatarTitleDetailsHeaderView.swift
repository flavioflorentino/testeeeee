import AssetsKit
import SnapKit
import UI
import UIKit

final class AvatarTitleDetailsHeaderView: UIView {
    private lazy var contentStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [avatarView, labelStackView])
        stack.spacing = Spacing.base02
        stack.alignment = .center
        return stack
    }()
    
    private lazy var avatarView: UIImageView = {
        let imageView = UIImageView()
        imageView.imageStyle(AvatarImageStyle(size: .medium, hasBorder: false))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var labelStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [titleLabel])
        stack.axis = .vertical
        stack.distribution = .fillProportionally
        if #available(iOS 11.0, *) {
            stack.setCustomSpacing(Spacing.base00, after: titleLabel)
        }
        return stack
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
            .with(\.numberOfLines, 0)
        return label
    }()
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView(model: AvatarTitleDetailsHeaderModel) {
        avatarView.setImage(url: URL(string: model.avatarUrl ?? ""),
                            placeholder: Resources.Placeholders.greenAvatarPlaceholder.image)
        titleLabel.text = model.title
        
        model.details.forEach { detail in
            let label = UILabel()
            label
                .labelStyle(BodySecondaryLabelStyle(type: .default))
                .with(\.textColor, .grayscale500())
                .with(\.numberOfLines, 0)
                .with(\.text, detail)
            labelStackView.addArrangedSubview(label)
        }
    }
}

extension AvatarTitleDetailsHeaderView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(contentStackView)
    }
    
    func setupConstraints() {
        contentStackView.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureStyles() {
        layer.borderWidth = Border.light
        layer.borderColor = Colors.grayscale200.color.cgColor
    }
}
