import Foundation
import SnapKit
import UI

private extension TransactionsPIXButtonView.Layout {
    enum CardView {
        static let shadowColor = Colors.grayscale100.color.cgColor
        static let shadowOffset = CGSize.zero
        static let shadowRadius: CGFloat = 2
        static let shadowOpacity: Float = 1
    }
    
    enum Constants {
        static let buttonSpacing = 12.0
        static let desiredCircleLineWidth: CGFloat = 1.2
    }
}

public class TransactionsPIXButtonView: UIView {
    fileprivate enum Layout { }
    
    private let didTap: () -> Void
    
    private lazy var button: UIButton = {
        let button = UIButton()
        button.viewStyle(RoundedViewStyle(cornerRadius: .medium))
        button.addTarget(self, action: #selector(buttonTap(button:)), for: .touchUpInside)
        button.backgroundColor = Colors.white.color
        button.layer.shadowColor = Layout.CardView.shadowColor
        button.layer.shadowOffset = Layout.CardView.shadowOffset
        button.layer.shadowRadius = Layout.CardView.shadowRadius
        button.layer.shadowOpacity = Layout.CardView.shadowOpacity
        return button
    }()
    
    private lazy var circleView: UIView = {
        let view = UIView()
        let halfSize: CGFloat = Spacing.base03
        let desiredLineWidth: CGFloat = Layout.Constants.desiredCircleLineWidth
        
        let circlePath = UIBezierPath(
            arcCenter: CGPoint(x: halfSize, y: halfSize),
            radius: CGFloat( halfSize - (desiredLineWidth / 2) ),
            startAngle: CGFloat(0),
            endAngle: CGFloat(Double.pi * 2),
            clockwise: true)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = Colors.grayscale100.color.cgColor
        shapeLayer.lineWidth = desiredLineWidth
        
        view.layer.addSublayer(shapeLayer)
        return view
    }()
    
    private lazy var buttonText: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.tintColor, Colors.grayscale700.color)
        label.text = Strings.TransactionsPIXButton.title
        return label
    }()
    
    private lazy var buttonDescription: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
            .with(\.tintColor, Colors.grayscale600.color)
            .with(\.text, Strings.TransactionsPIXButton.description)
        return label
    }()
    
    private lazy var illustrationImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.icoPix.image
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var labelStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Sizing.base01
        stackView.isUserInteractionEnabled = false
        return stackView
    }()
    
    public init(frame: CGRect = .zero, didTap: @escaping () -> Void) {
        self.didTap = didTap
        super.init(frame: frame)

        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("not implemented yet!!!")
    }
    
    @objc
    func buttonTap(button: UIButton) {
        didTap()
    }
}

extension TransactionsPIXButtonView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(button)
        button.addSubview(circleView)
        button.addSubview(labelStackView)
        labelStackView.addArrangedSubview(buttonText)
        labelStackView.addArrangedSubview(buttonDescription)
        circleView.addSubview(illustrationImageView)
    }
    
    public func setupConstraints() {
        button.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
            $0.top.bottom.equalToSuperview()
        }
        
        labelStackView.snp.makeConstraints {
            $0.leading.equalTo(circleView.snp.trailing).offset(Spacing.base02)
            $0.centerY.equalTo(button)
        }
        
        circleView.snp.makeConstraints {
            $0.height.width.equalTo(Spacing.base06)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.top.bottom.equalToSuperview().inset(Layout.Constants.buttonSpacing)
        }
        
        illustrationImageView.snp.makeConstraints {
            $0.height.width.equalTo(Spacing.base03)
            $0.center.equalTo(circleView)
        }
    }
}
