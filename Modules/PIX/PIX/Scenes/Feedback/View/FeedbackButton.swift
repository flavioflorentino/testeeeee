import UI
import UIKit

protocol FeedbackButtonDelegate: AnyObject {
    func feedbackButtonDidTap(action: FeedbackAction)
}

final class FeedbackButton: UIButton {
    private let viewModel: FeedbackButtonViewModel
    weak var delegate: FeedbackButtonDelegate?
    
    init(viewModel: FeedbackButtonViewModel, delegate: FeedbackButtonDelegate? = nil) {
        self.viewModel = viewModel
        self.delegate = delegate
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func buildLayout() {
        setTitle(viewModel.title, for: .normal)
        buttonStyle(viewModel.style)
        addTarget(self, action: #selector(didTap), for: .touchUpInside)
    }
    
    @objc
    private func didTap() {
        delegate?.feedbackButtonDidTap(action: viewModel.action)
    }
}
