import UIKit

enum FeedbackFactory {
    static func make(
        with type: FeedbackViewType,
        delegate: FeedbackDelegate? = nil,
        from origin: PixUserNavigation = .undefined
    ) -> FeedbackViewController {
        let container = DependencyContainer()
        let coordinator: FeedbackCoordinating = FeedbackCoordinator()
        let presenter: FeedbackPresenting = FeedbackPresenter(coordinator: coordinator, type: type)
        let interactor = FeedbackInteractor(type: type, presenter: presenter, dependencies: container, origin: origin)
        let viewController = FeedbackViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
