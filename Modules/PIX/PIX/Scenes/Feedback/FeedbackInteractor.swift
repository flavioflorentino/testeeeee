import AnalyticsModule
import Foundation

protocol FeedbackInteracting: AnyObject {
    func fetchData()
    func execute(action: FeedbackAction)
    func didClose()
    func trackScreenOpened()
}

final class FeedbackInteractor {
    typealias Dependencies = HasAnalytics & HasBizAuth
    private let dependencies: Dependencies

    private let type: FeedbackViewType
    private let presenter: FeedbackPresenting
    private let origin: PixUserNavigation

    init(type: FeedbackViewType, presenter: FeedbackPresenting, dependencies: Dependencies, origin: PixUserNavigation) {
        self.type = type
        self.presenter = presenter
        self.dependencies = dependencies
        self.origin = origin
    }
}

// MARK: - FeedbackInteracting
extension FeedbackInteractor: FeedbackInteracting {
    func fetchData() {
        presenter.presentData(with: type)
    }
    
    func execute(action: FeedbackAction) {
        presenter.execute(action: action)
    }
    
    func didClose() {
        presenter.execute(action: .close)
    }
    
    func trackScreenOpened() {
        let destination = PixUserNavigation(type)
        guard origin != .undefined, destination != .undefined else { return }
        
        let event = PixNavigationEvent.userNavigated(from: origin, to: destination)
        dependencies.analytics.log(event)
    }
}
