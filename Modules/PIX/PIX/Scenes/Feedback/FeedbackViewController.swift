import UI
import UIKit
import SnapKit

protocol FeedbackDisplaying: AnyObject {
    func display(viewModel: FeedbackViewModeling)
    func display(notification: String)
}

private extension FeedbackViewController.Layout {
    enum Size {
        static let image: CGFloat = 145
        static let imageSmaller: CGFloat = 120
        static let smallScreenHeight: CGFloat = 568
    }
}

final class FeedbackViewController: ViewController<FeedbackInteracting, UIView> {
    fileprivate enum Layout {}
    
    private lazy var closeButton = UIBarButtonItem(title: Strings.General.close, style: .plain, target: self, action: #selector(didTapCloseButton))
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var textsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = isSmallScreenDevice ? Spacing.base01 : Spacing.base02
        return stackView
    }()
    
    private lazy var actionsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = isSmallScreenDevice ? Spacing.base01 : Spacing.base02
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale600())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var notificationLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .neutral600())
            .with(\.textAlignment, .center)
        label.isHidden = true
        return label
    }()
    
    private let isSmallScreenDevice = UIScreen.main.bounds.height <= Layout.Size.smallScreenHeight
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.fetchData()
        interactor.trackScreenOpened()
    }
    
    override func buildViewHierarchy() {
        navigationItem.leftBarButtonItem = closeButton
        
        view.addSubview(imageView)
        view.addSubview(textsStackView)
        view.addSubview(notificationLabel)
        view.addSubview(actionsStackView)
        
        textsStackView.addArrangedSubview(titleLabel)
        textsStackView.addArrangedSubview(descriptionLabel)
    }
    
    override func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top).offset(isSmallScreenDevice ? Spacing.base04 : Spacing.base06)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(isSmallScreenDevice ? Layout.Size.imageSmaller : Layout.Size.image)
        }
        
        textsStackView.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(isSmallScreenDevice ? Spacing.base02 : Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        notificationLabel.snp.makeConstraints {
            $0.bottom.equalTo(actionsStackView.snp.top).offset(-Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base04)
        }
        
        actionsStackView.snp.makeConstraints {
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).inset(isSmallScreenDevice ? Spacing.base02 : Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        navigationController?.presentationController?.delegate = self
    }
    
    @objc
    private func didTapCloseButton() {
        interactor.didClose()
    }
}

// MARK: - FeedbackDisplaying
extension FeedbackViewController: FeedbackDisplaying {
    func display(viewModel: FeedbackViewModeling) {
        imageView.image = viewModel.image
        titleLabel.text = viewModel.title
        descriptionLabel.text = viewModel.description
        
        viewModel.buttons.forEach { [weak self] viewModel in
            self?.actionsStackView.addArrangedSubview(FeedbackButton(viewModel: viewModel, delegate: self))
        }
    }
    
    func display(notification: String) {
        notificationLabel.text = notification
        notificationLabel.isHidden = false
    }
}

extension FeedbackViewController: FeedbackButtonDelegate {
    func feedbackButtonDidTap(action: FeedbackAction) {
        interactor.execute(action: action)
    }
}

extension FeedbackViewController: UIAdaptivePresentationControllerDelegate {
    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        interactor.execute(action: .modalDismissed)
    }
}
