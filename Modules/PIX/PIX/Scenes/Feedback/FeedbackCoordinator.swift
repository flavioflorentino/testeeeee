import UIKit

protocol FeedbackDelegate: AnyObject {
    func didAction(action: FeedbackAction, feedbackType: FeedbackViewType)
}

enum FeedbackAction: Equatable {
    case close
    case tryAgain
    case ignore
    case grantPortability
    case cancelPortability
    case revalidateKey
    case allowClaim
    case cancelClaim
    case registerKey
    case revalidateClaimKeyAccepted
    case requestAgain
    case cancel
    case modalDismissed
    case tryPix
    case validateKey
    case notNow
    case validateIdentity
}

protocol FeedbackCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: FeedbackDelegate? { get set }
    func execute(action: FeedbackAction, feedbackType: FeedbackViewType)
}

final class FeedbackCoordinator {
    weak var viewController: UIViewController?
    weak var delegate: FeedbackDelegate?
}

// MARK: - FeedbackCoordinating
extension FeedbackCoordinator: FeedbackCoordinating {
    func execute(action: FeedbackAction, feedbackType: FeedbackViewType) {
        delegate?.didAction(action: action, feedbackType: feedbackType)
    }
}
