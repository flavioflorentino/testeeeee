import Foundation

protocol FeedbackPresenting: AnyObject {
    var viewController: FeedbackDisplaying? { get set }
    func execute(action: FeedbackAction)
    func presentData(with type: FeedbackViewType)
}

final class FeedbackPresenter {
    private let coordinator: FeedbackCoordinating
    weak var viewController: FeedbackDisplaying?
    var feedbackType: FeedbackViewType

    init(coordinator: FeedbackCoordinating, type: FeedbackViewType) {
        self.feedbackType = type
        self.coordinator = coordinator
    }
}

// MARK: - FeedbackPresenting
extension FeedbackPresenter: FeedbackPresenting {
    func presentData(with type: FeedbackViewType) {
        if let notification = type.viewModel.notification {
            viewController?.display(notification: notification)
        }
        
        viewController?.display(viewModel: type.viewModel)
    }
    
    func execute(action: FeedbackAction) {
        coordinator.execute(action: action, feedbackType: feedbackType)
    }
}
