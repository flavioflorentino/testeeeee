import AssetsKit
import Foundation
import UI
import UIKit

extension FeedbackViewType {
    var viewModel: FeedbackViewModeling {
        switch self {
        case let .keyRecordInProgress(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluClock2.image,
                title: Strings.Feedback.KeyRecordInProgress.title,
                description: Strings.Feedback.KeyRecordInProgress.description,
                buttons: [.gotIt]
            )
        case let .keyRecordNotRegistered(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluPeople.image,
                title: Strings.Feedback.KeyRecordNotRegistered.title,
                description: Strings.Feedback.KeyRecordNotRegistered.description,
                buttons: [.tryAgain]
            )
        case let .keyRecordRegistered(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluVictory.image,
                title: Strings.Feedback.KeyRecordRegistered.title,
                description: Strings.Feedback.KeyRecordRegistered.description,
                buttons: [.gotIt]
            )
        case .identityValidationPending:
            return FeedbackViewModel(
                keyId: "",
                image: Resources.Illustrations.iluWarning.image,
                title: Strings.Feedback.IdentityValidationPending.title,
                description: Strings.Feedback.IdentityValidationPending.description,
                buttons: [.gotIt]
            )
        case let .portabilityCompleted(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluVictory.image,
                title: Strings.Feedback.PortabilityCompleted.title,
                description: Strings.Feedback.PortabilityCompleted.description,
                buttons: [.gotIt]
            )
        case let .portabilityRefused(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluPeople.image,
                title: Strings.Feedback.PortabilityRefused.title,
                description: Strings.Feedback.PortabilityRefused.description,
                buttons: [.tryAgain]
            )
        case let .portabilityInProgress(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluEmail.image,
                title: Strings.Feedback.PortabilityInProgress.title,
                description: Strings.Feedback.PortabilityInProgress.description,
                buttons: [.gotIt]
            )
        case let .portabilityKeyCancel(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluEmail.image,
                title: Strings.Feedback.PortabilityKeyCancel.title,
                description: Strings.Feedback.PortabilityKeyCancel.description,
                buttons: [.cancelPortability, .notNow]
            )
        case .portabilityCancellationInProgress:
            return FeedbackViewModel(
                image: Resources.Illustrations.iluClock2.image,
                title: Strings.Feedback.PortabilityCancellationInProgress.title,
                description: Strings.Feedback.PortabilityCancellationInProgress.description,
                buttons: [.gotIt]
            )
        case let .portabilityDonorReceived(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluEmail.image,
                title: Strings.Feedback.PortabilityDonorReceived.title,
                description: Strings.Feedback.PortabilityDonorReceived.description,
                buttons: [.continuePixKey, .grantPortability]
            )
        case let .portabilityDonorInProgress(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluClock2.image,
                title: Strings.Feedback.PortabilityDonorInProgress.title,
                description: Strings.Feedback.PortabilityDonorInProgress.description,
                buttons: [.gotIt]
            )
        case let .portabilityDonorRefused(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluPeople.image,
                title: Strings.Feedback.PortabilityDonorRefused.title,
                description: Strings.Feedback.PortabilityDonorRefused.description,
                buttons: [.gotIt]
            )
        case let .portabilityDonorCompleted(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluVictory.image,
                title: Strings.Feedback.PortabilityDonorCompleted.title,
                description: Strings.Feedback.PortabilityDonorCompleted.description,
                buttons: [.gotIt]
            )
        case let .claimInProgress(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluEmail.image,
                title: Strings.Feedback.ClaimInProgress.title,
                description: Strings.Feedback.ClaimInProgress.description,
                buttons: [.gotIt]
            )
        case let .claimRefused(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluPeople.image,
                title: Strings.Feedback.ClaimRefused.title,
                description: Strings.Feedback.ClaimRefused.description,
                buttons: [.tryAgain]
            )
        case let .claimCompleted(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluVictory.image,
                title: Strings.Feedback.ClaimCompleted.title,
                description: Strings.Feedback.ClaimCompleted.description,
                buttons: [.gotIt]
            )
        case let .claimKeyCancel(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluEmail.image,
                title: Strings.Feedback.ClaimKeyCancel.title,
                description: Strings.Feedback.ClaimKeyCancel.description,
                buttons: [.cancelClaim, .notNow]
            )
        case .claimCancellationInProgress:
            return FeedbackViewModel(
                image: Resources.Illustrations.iluClock2.image,
                title: Strings.Feedback.ClaimCancellationInProgress.title,
                description: Strings.Feedback.ClaimCancellationInProgress.description,
                buttons: [.gotIt]
            )
        case let .claimDonorPermitted(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluEmail.image,
                title: Strings.Feedback.ClaimDonorPermitted.title,
                description: Strings.Feedback.ClaimDonorPermitted.description,
                buttons: [.gotIt]
            )
        case let .claimDonorNotCompleted(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluPeople.image,
                title: Strings.Feedback.ClaimDonorNotCompleted.title,
                description: Strings.Feedback.ClaimDonorNotCompleted.description,
                buttons: [.tryAgain]
            )
        case let .claimDonorRequested(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluEmail.image,
                title: Strings.Feedback.ClaimDonorRequested.title,
                description: Strings.Feedback.ClaimDonorRequested.description,
                buttons: [.gotIt]
            )
        case let .claimDonorReceived(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluEmail.image,
                title: Strings.Feedback.ClaimDonorReceived.title,
                description: Strings.Feedback.ClaimDonorReceived.description,
                buttons: [.revalidateKey, .allowClaim]
            )
        case let .claimDonorInProgress(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluClock2.image,
                title: Strings.Feedback.ClaimDonorInProgress.title,
                description: Strings.Feedback.ClaimDonorInProgress.description,
                buttons: [.gotIt]
            )
        case let .claimDonorPendent(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluClock2.image,
                title: Strings.Feedback.ClaimDonorPendent.title,
                description: Strings.Feedback.ClaimDonorPendent.description,
                buttons: [.gotIt],
                notification: Strings.Feedback.notification
            )
        case let .claimDonorRevalidationInProgress(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluClock2.image,
                title: Strings.Feedback.ClaimDonorRevalidationInProgress.title,
                description: Strings.Feedback.ClaimDonorRevalidationInProgress.description,
                buttons: [.gotIt]
            )
        case let .claimDonorRevalidationCompleted(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluVictory.image,
                title: Strings.Feedback.ClaimDonorRevalidationCompleted.title,
                description: Strings.Feedback.ClaimDonorRevalidationCompleted.description,
                buttons: [.gotIt]
            )
        case let .claimDonorRevalidationNotCompleted(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluPeople.image,
                title: Strings.Feedback.ClaimDonorRevalidationNotCompleted.title,
                description: Strings.Feedback.ClaimDonorRevalidationNotCompleted.description,
                buttons: [.tryAgain]
            )
        case let .pendingKeyDeletion(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluClock2.image,
                title: Strings.Feedback.PendingKeyDeletion.title,
                description: Strings.Feedback.PendingKeyDeletion.description,
                buttons: [.gotIt]
            )
        case let .keyDeletionError(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluClock.image,
                title: Strings.Feedback.KeyDeletionError.title,
                description: Strings.Feedback.KeyDeletionError.description,
                buttons: [.tryAgain],
                notification: Strings.Feedback.notification
            )
        case let .cancelClaimError(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluClock.image,
                title: Strings.KeyManager.error,
                description: Strings.KeyManager.errorDescription,
                buttons: [.tryAgain]
            )
        case .needsPixKeyRegistration:
            return FeedbackViewModel(
                image: Resources.Illustrations.iluFillingForm.image,
                title: Strings.Feedback.NeedsKeyRegistration.title,
                description: Strings.Feedback.NeedsKeyRegistration.description,
                buttons: [.registerKey, .notNow]
            )
        case let .claimKeyAccepted(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluEmail.image,
                title: Strings.Feedback.ClaimKeyAccepted.title,
                description: Strings.Feedback.ClaimKeyAccepted.description,
                buttons: [.revalidateClaimKeyAccepted]
            )
        case let .validationNotCompleted(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluEmail.image,
                title: Strings.Feedback.ValidationNotCompleted.title,
                description: Strings.Feedback.ValidationNotCompleted.description,
                buttons: [.requestAgain, .cancel]
            )
        case let .claimNewValidationKeyAvailable(uuid):
            return FeedbackViewModel(
                keyId: uuid,
                image: Resources.Illustrations.iluEmail.image,
                title: Strings.Feedback.ClaimNewValidationKeyAvailable.title,
                description: Strings.Feedback.ClaimNewValidationKeyAvailable.description,
                buttons: [.validateKey, .notNow]
            )
        case .claimNewValidationDeadlineFinished:
            return FeedbackViewModel(
                image: Resources.Illustrations.iluPeople.image,
                title: Strings.Feedback.ClaimNewValidationDeadlineFinished.title,
                description: Strings.Feedback.ClaimNewValidationDeadlineFinished.description,
                buttons: [.gotIt]
            )
        case .claimNewValidationCompleted:
            return FeedbackViewModel(
                image: Resources.Illustrations.iluVictory.image,
                title: Strings.Feedback.ClaimNewValidationCompleted.title,
                description: Strings.Feedback.ClaimNewValidationCompleted.description,
                buttons: [.gotIt]
            )
        case .claimNewValidationNotCompleted:
            return FeedbackViewModel(
                image: Resources.Illustrations.iluPeople.image,
                title: Strings.Feedback.ClaimNewValidationNotCompleted.title,
                description: Strings.Feedback.ClaimNewValidationNotCompleted.description,
                buttons: [.tryAgain]
            )
        case .unauthorized:
            return FeedbackViewModel(
                image: Resources.Illustrations.iluPeople.image,
                title: Strings.Feedback.Unauthorized.title,
                description: Strings.Feedback.Unauthorized.description,
                buttons: [.gotIt]
            )
        case .identityValidationCompleted:
            return FeedbackViewModel(
                image: Resources.Illustrations.iluSuccess.image,
                title: Strings.Feedback.IdentityValidationCompleted.title,
                description: Strings.Feedback.IdentityValidationCompleted.description,
                buttons: [.continue]
            )
        case .identityValidationNotCompleted:
            return FeedbackViewModel(
                image: Resources.Illustrations.iluError.image,
                title: Strings.Feedback.IdentityValidationNotCompleted.title,
                description: Strings.Feedback.IdentityValidationNotCompleted.description,
                buttons: [.gotIt]
            )
        case .identityValidationInReview:
            return FeedbackViewModel(
                image: Resources.Illustrations.iluWarning.image,
                title: Strings.Feedback.IdentityValidationInReview.title,
                description: Strings.Feedback.IdentityValidationInReview.description,
                buttons: [.gotIt]
            )
        case .noContacts:
            return FeedbackViewModel(
                image: Resources.Illustrations.iluNoContacts.image,
                title: Strings.Feedback.NoContacts.title,
                description: Strings.Feedback.NoContacts.description,
                buttons: []
            )
        case .noFetchingContacts:
            return FeedbackViewModel(
                image: Resources.Illustrations.iluEmptyCircledBackground.image,
                title: Strings.Feedback.NoFetchingContacts.title,
                description: Strings.Feedback.NoFetchingContacts.description,
                buttons: []
            )
        }
    }
}
