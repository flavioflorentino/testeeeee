import Foundation

public enum FeedbackViewType: Equatable {
    // Registro de chaves
    case keyRecordInProgress(uuid: String) // Cadastro de chave em andamento
    case keyRecordNotRegistered(uuid: String) // Sua chave não foi cadastrada
    case keyRecordRegistered(uuid: String) // Chave cadastrada

    // Validação de identidade
    case identityValidationPending
    
    // Portabilidade
    case portabilityCompleted(uuid: String) // Portabilidade de chave concluída
    case portabilityRefused(uuid: String) // Pedido de portabilidade não concluído
    case portabilityInProgress(uuid: String) // Portabilidade de chave em andamento
    case portabilityKeyCancel(uuid: String) // Cancelar a portabilidade da chave
    case portabilityCancellationInProgress // Cancelamento da portabilidade em andamento
    
    // Portabilidade doador
    case portabilityDonorReceived(uuid: String) // Pedido de portabilidade recebido
    case portabilityDonorInProgress(uuid: String) // Portabilidade de chave em andamento
    case portabilityDonorRefused(uuid: String) // Sua portabilidade não foi concluída
    case portabilityDonorCompleted(uuid: String) // Portabilidade concluída
    
    // Reivindicação
    case claimInProgress(uuid: String) // Reivindicação de chave em andamento 
    case claimRefused(uuid: String) // Reivindicação de chave recusada
    case claimCompleted(uuid: String) // Reivindicação de chave concluída
    case claimKeyAccepted(uuid: String) // Reivindicação aceita
    case validationNotCompleted(uuid: String) // Validação não completada
    case claimKeyCancel(uuid: String) // Cancelar a reivindicação da chave
    case claimCancellationInProgress // Cancelamento da reivindicação em andamento
    
    // Reivindicação doador
    case claimDonorPermitted(uuid: String) // Reivindicação de chave permitida
    case claimDonorNotCompleted(uuid: String) // Reivindicação de chave não concluída
    case claimDonorRequested(uuid: String) // Chave reivindicada por outra pessoa
    case claimDonorReceived(uuid: String) // Reivindicação de chave recebida
    case claimDonorInProgress(uuid: String) // Reivindicação de chave em andamento
    case claimDonorPendent(uuid: String) // Reivindicação de posse pendente
    case claimDonorRevalidationInProgress(uuid: String) // Revalidação de chave em andamento
    case claimDonorRevalidationCompleted(uuid: String) // Revalidação de chave concluída
    case claimDonorRevalidationNotCompleted(uuid: String) // Revalidação de chave não concluída
    
    // Exclusão de chave pendente
    case pendingKeyDeletion(uuid: String)
    case keyDeletionError(uuid: String)
    
    // Cancelamento de pedido de reivindicação
    case cancelClaimError(id: String)
    
    // Cadastre uma chave Pix para receber pagamentos
    case needsPixKeyRegistration
    
    // Reivindicação após 14 dias - Nova validação
    case claimNewValidationKeyAvailable(uuid: String) // Chave Pix disponível
    case claimNewValidationDeadlineFinished // Seu prazo de validação terminou
    case claimNewValidationCompleted // Validação concluída
    case claimNewValidationNotCompleted // A validação de segurança não foi feita

    // Não autorizado
    case unauthorized
    
    // Validação de identidade
    case identityValidationCompleted // Validamos sua identidade
    case identityValidationNotCompleted // Não conseguimos validar sua identidade
    case identityValidationInReview // Suas informações estão em análise
    
    // Agenda
    case noContacts
    case noFetchingContacts
}
