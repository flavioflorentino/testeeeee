import Foundation
import UI

enum ButtonViewModelFeedbackType {
    case gotIt
    case tryAgain
    case ignore
    case grantPortability
    case cancelPortability
    case revalidateKey
    case allowClaim
    case cancelClaim
    case registerKey
    case revalidateClaimKeyAccepted
    case requestAgain
    case cancel
    case notNow
    case validateKey
    case doItLater
    case continuePixKey
    case `continue`
    
    var button: FeedbackButtonViewModel {
        var button: FeedbackButtonViewModel
        
        switch self {
        case .gotIt:
            button = FeedbackButtonViewModel(
                title: Strings.General.gotIt,
                action: .close,
                style: AnyButtonStyle<UIButton>(style: PrimaryButtonStyle())
            )
        case .tryAgain:
            button = FeedbackButtonViewModel(
                title: Strings.General.tryAgain,
                action: .tryAgain,
                style: AnyButtonStyle<UIButton>(style: PrimaryButtonStyle())
            )
        case .ignore:
            button = FeedbackButtonViewModel(
                title: Strings.General.ignore,
                action: .ignore,
                style: AnyButtonStyle<UIButton>(style: SecondaryButtonStyle())
            )
        case .grantPortability:
            button = FeedbackButtonViewModel(
                title: Strings.Feedback.grantPortability,
                action: .grantPortability,
                style: AnyButtonStyle<UIButton>(style: SecondaryButtonStyle())
            )
        case .cancelPortability:
            button = FeedbackButtonViewModel(
                title: Strings.Feedback.cancelPortability,
                action: .cancelPortability,
                style: AnyButtonStyle<UIButton>(style: DangerButtonStyle(icon: (name: .exclamationCircle, alignment: .left)))
            )
        case .revalidateKey:
            button = FeedbackButtonViewModel(
                title: Strings.General.revalidateKey,
                action: .revalidateKey,
                style: AnyButtonStyle<UIButton>(style: PrimaryButtonStyle())
            )
        case .allowClaim:
            button = FeedbackButtonViewModel(
                title: Strings.General.allowClaim,
                action: .allowClaim,
                style: AnyButtonStyle<UIButton>(style: SecondaryButtonStyle())
            )
        case .cancelClaim:
            button = FeedbackButtonViewModel(
                title: Strings.Feedback.cancelClaim,
                action: .cancelClaim,
                style: AnyButtonStyle<UIButton>(style: DangerButtonStyle(icon: (name: .exclamationCircle, alignment: .left)))
            )
        case .registerKey:
            button = FeedbackButtonViewModel(
                title: Strings.General.registerKey,
                action: .registerKey,
                style: AnyButtonStyle<UIButton>(style: PrimaryButtonStyle())
            )
        case .revalidateClaimKeyAccepted:
            button = FeedbackButtonViewModel(
                title: Strings.General.revalidateKey,
                action: .revalidateClaimKeyAccepted,
                style: AnyButtonStyle<UIButton>(style: PrimaryButtonStyle())
            )
        case .requestAgain:
            button = FeedbackButtonViewModel(
                title: Strings.General.requestAgain,
                action: .requestAgain,
                style: AnyButtonStyle<UIButton>(style: PrimaryButtonStyle())
            )
        case .cancel:
            button = FeedbackButtonViewModel(
                title: Strings.General.cancel,
                action: .cancel,
                style: AnyButtonStyle<UIButton>(style: SecondaryButtonStyle())
            )
        case .notNow:
            button = FeedbackButtonViewModel(
                title: Strings.General.notNow,
                action: .notNow,
                style: AnyButtonStyle<UIButton>(style: SecondaryButtonStyle())
            )
        case .validateKey:
            button = FeedbackButtonViewModel(
                title: Strings.General.validateKey,
                action: .validateKey,
                style: AnyButtonStyle<UIButton>(style: PrimaryButtonStyle())
            )
        case .doItLater:
            button = FeedbackButtonViewModel(
                title: Strings.General.doItLater,
                action: .close,
                style: AnyButtonStyle<UIButton>(style: SecondaryButtonStyle())
            )
        case .continuePixKey:
            button = FeedbackButtonViewModel(
                title: Strings.General.continuePixKey,
                action: .ignore,
                style: AnyButtonStyle<UIButton>(style: PrimaryButtonStyle())
            )
        case .continue:
            button = FeedbackButtonViewModel(
                title: Strings.General.continue,
                action: .close,
                style: AnyButtonStyle<UIButton>(style: PrimaryButtonStyle())
            )
        }
        
        return button
    }
}
