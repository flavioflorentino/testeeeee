import Foundation
import UIKit

protocol FeedbackViewModeling {
    var keyId: String? { get }
    var image: UIImage { get }
    var title: String { get }
    var description: String { get }
    var buttons: [FeedbackButtonViewModel] { get }
    var notification: String? { get }
}

struct FeedbackViewModel: FeedbackViewModeling {
    let keyId: String?
    let image: UIImage
    let title: String
    let description: String
    let buttons: [FeedbackButtonViewModel]
    let notification: String?
    
    init(
        keyId: String?,
        image: UIImage,
        title: String,
        description: String,
        buttons: [FeedbackButtonViewModel],
        notification: String? = nil
    ) {
        self.keyId = keyId
        self.image = image
        self.title = title
        self.description = description
        self.buttons = buttons
        self.notification = notification
    }
    
    init(
        keyId: String?,
        image: UIImage,
        title: String,
        description: String,
        buttons: [ButtonViewModelFeedbackType],
        notification: String? = nil
    ) {
        self.init(
            keyId: keyId,
            image: image,
            title: title,
            description: description,
            buttons: buttons.map { $0.button },
            notification: notification
        )
    }
    
    init(image: UIImage, title: String, description: String, buttons: [ButtonViewModelFeedbackType], notification: String? = nil) {
        self.init(keyId: nil, image: image, title: title, description: description, buttons: buttons, notification: notification)
    }
}
