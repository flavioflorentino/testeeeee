import Foundation
import UIKit

struct FeedbackButtonViewModel {
    let title: String
    let action: FeedbackAction
    let style: AnyButtonStyle<UIButton>
}
