import Core
import Foundation

public extension FeedbackViewType {
    // swiftlint:disable cyclomatic_complexity function_body_length
    static func type(with url: URL) -> FeedbackViewType? {
        let uuid = url.queryParameters["uuid"] as? String ?? ""
        switch url.lastPathComponent {
        case "keyRecordInProgress":
            return .keyRecordInProgress(uuid: uuid)
        case "keyRecordNotRegistered":
            return .keyRecordNotRegistered(uuid: uuid)
        case "keyRecordRegistered", "keyRecordRegisteredAndIdValidation":
            return .keyRecordRegistered(uuid: uuid)
        case "portabilityRefused":
            return .portabilityRefused(uuid: uuid)
        case "portabilityCompleted":
            return .portabilityCompleted(uuid: uuid)
        case "claimRefused":
            return .claimRefused(uuid: uuid)
        case "claimCompleted":
            return .claimCompleted(uuid: uuid)
        case "portabilityDonorReceived":
            return .portabilityDonorReceived(uuid: uuid)
        case "portabilityDonorRefused":
            return .portabilityDonorRefused(uuid: uuid)
        case "portabilityDonorCompleted":
            return .portabilityDonorCompleted(uuid: uuid)
        case "claimDonorReceived":
            return .claimDonorReceived(uuid: uuid)
        case "claimDonorRequested":
            return .claimDonorRequested(uuid: uuid)
        case "claimDonorPermitted":
            return .claimDonorPermitted(uuid: uuid)
        case "claimDonorNotCompleted":
            return .claimDonorNotCompleted(uuid: uuid)
        case "claimDonorRevalidationCompleted":
            return .claimDonorRevalidationCompleted(uuid: uuid)
        case "claimDonorRevalidationNotCompleted":
            return .claimDonorRevalidationNotCompleted(uuid: uuid)
        case "claimKeyAccepted":
            return .claimKeyAccepted(uuid: uuid)
        case "validationNotCompleted":
            return .validationNotCompleted(uuid: uuid)
        default:
            return nil
        }
    }
}
