import Core
import Foundation

protocol KeyManagerConsumerPresenting: AnyObject {
    func hideLoading()
    func presentLoading()
    func presentErrorScreen(message: String)
    func didNextStep(action: KeyManagerConsumerAction)
    func presentErrorPopup(popup: PixCreateKeyErrorModel)
    func presentErrorPopup(title: String, message: String)
    func presentUpdateRegistrationPopup(title: String, message: String)
    func presentDeleteKeyPopup(keyId: String)
    func presentKeySections(_ sections: [KeyManagerConsumerSection], userData: PixUserData)
    func presentBottomSheetMenu(userData: PixUserData, key: PixConsumerKey, actions: [BottomSheetAction])
    func presentShareMenu(keyValue: String)
    func presentKeyCopiedAlert()
    func presentCloseButton()
    var viewController: KeyManagerConsumerDisplaying? { get set }
}

final class KeyManagerConsumerPresenter: KeyManagerConsumerPresenting {
    private let coordinator: KeyManagerConsumerCoordinating
    weak var viewController: KeyManagerConsumerDisplaying?
    
    init(coordinator: KeyManagerConsumerCoordinating) {
        self.coordinator = coordinator
    }
    
    func presentKeySections(_ sections: [KeyManagerConsumerSection], userData: PixUserData) {
        let sectionsToDisplay = sections.map { section -> KeyManagerSection in
            let title: String
            let description: String
            let keyItems = section.keys.map { KeyManagerItem(key: $0, userData: userData) }
            
            switch section.type {
            case .registered:
                title = Strings.KeyManager.Consumer.Section.RegisteredKeys.title
                description = ""
            case .unregistered:
                title = Strings.KeyManager.Consumer.Section.UnregisteredKeys.title
                description = Strings.KeyManager.Consumer.Section.UnregisteredKeys.description
            }
            
            return KeyManagerSection(title: title, items: keyItems, description: description)
        }
        viewController?.displayKeys(sections: sectionsToDisplay)
    }
    
    func presentErrorScreen(message: String) {
        viewController?.displayConsumerDataError(message: message)
    }
    
    func presentErrorPopup(popup: PixCreateKeyErrorModel) {
        viewController?.displayErrorPopup(popup: popup)
    }
    
    func presentErrorPopup(title: String, message: String) {
        viewController?.displayErrorPopup(title: title, message: message)
    }
    
    func presentUpdateRegistrationPopup(title: String, message: String) {
        viewController?.displayUpdateRegistrationPopup(title: title, message: message)
    }
    
    func presentDeleteKeyPopup(keyId: String) {
        viewController?.displayDeleteKeyPopup(title: Strings.KeyManager.Consumer.DeleteKey.popupTitle,
                                              message: Strings.KeyManager.Consumer.DeleteKey.popupDescription,
                                              keyId: keyId)
    }
    
    func presentLoading() {
        viewController?.displayLoading()
    }
    
    func hideLoading() {
        viewController?.hideLoading()
    }
    
    func didNextStep(action: KeyManagerConsumerAction) {
        coordinator.perform(action: action)
    }
    
    func presentBottomSheetMenu(userData: PixUserData, key: PixConsumerKey, actions: [BottomSheetAction]) {
        viewController?.displayBottomSheetMenu(
            with: KeyManagerItem(key: key, userData: userData),
            actions: actions
        )
    }
    
    func presentShareMenu(keyValue: String) {
        let sharingText = Strings.KeyManager.Consumer.sharingText(keyValue)
        viewController?.displayShareMenu(sharingText: sharingText)
    }
    
    func presentKeyCopiedAlert() {
        viewController?.displayKeyCopiedAlert(Strings.KeyManager.keyCopiedMessage)
    }
    
    func presentCloseButton() {
        viewController?.displayCloseButton()
    }
}
