import AnalyticsModule
import Core
import Foundation
import IdentityValidation

protocol KeyManagerConsumerInteracting: AnyObject {
    func loadAndPresentKeys()
    func reloadKeys()
    func didTapDelete(keyId: String)
    func didTapCell(at indexPath: IndexPath)
    func sendFeedbackEventForCloseAction(feedbackType: FeedbackViewType)

    func didClose()
    func trackScreenOpened()
    func trackNavigationFromDismissedFeedback(feedbackType: FeedbackViewType)

    func requestPortability(claimKey: ClaimKey)
    func requestClaim(claimKey: ClaimKey)
    func updateUserRegistration()
    
    func trackDialog(title: String, action: DialogsEvent.Action)
    func setupCloseButton()
}

final class KeyManagerConsumerInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let consumerService: KeyManagerConsumerServicing
    private let pixKeyService: PixConsumerKeyServicing
    private let presenter: KeyManagerConsumerPresenting
    
    private var userData: PixUserData?
    private var pixKeySections: [KeyManagerConsumerSection] = []
    
    private let sceneOrigin: PixUserNavigation
    private let checkForPreRegisteredKeys: Bool
    private let feedbackViewType: FeedbackViewType?
    private var showedPreRegisteredFlow = false
    private var keysLoaded = false
    
    init(consumerService: KeyManagerConsumerServicing,
         pixKeyService: PixConsumerKeyServicing,
         presenter: KeyManagerConsumerPresenting,
         dependencies: Dependencies,
         origin: PixUserNavigation,
         checkForPreRegisteredKeys: Bool,
         feedbackViewType: FeedbackViewType? = nil) {
        self.consumerService = consumerService
        self.pixKeyService = pixKeyService
        self.presenter = presenter
        self.dependencies = dependencies
        self.checkForPreRegisteredKeys = checkForPreRegisteredKeys
        self.sceneOrigin = origin
        self.feedbackViewType = feedbackViewType
    }
    
    private func getUserData() {
        consumerService.getConsumerData { [weak self] result in
            switch result {
            case let .success(userData):
                self?.userData = userData
                self?.getKeys()
                
            case .failure:
                self?.presenter.hideLoading()
                self?.presenter.presentErrorScreen(message: Strings.KeyManager.Consumer.Error.UserData.description)
            }
        }
    }
    
    private func getKeys() {
        guard let userId = consumerService.consumerId else { return }
        pixKeyService.listPixKeys(with: "\(userId)") { [weak self] result in
            self?.presenter.hideLoading()
            
            switch result {
            case let .success(pixKeyListResponse):
                self?.prepareKeys(pixKeyListResponse.data)
                
            case .failure:
                self?.presenter.presentErrorScreen(message: Strings.KeyManager.Consumer.Error.Request.description)
            }
        }
    }
    
    private func prepareKeys(_ keys: [PixConsumerKey]) {
        guard let data = userData else {
            presenter.presentErrorScreen(message: Strings.KeyManager.Consumer.Error.UserData.description)
            return
        }
        
        var registeredKeys: [PixConsumerKey] = []
        var unregisteredKeys: [PixConsumerKey] = []
        var preRegisteredKeys: [PixConsumerKey] = []
        
        keys.forEach { key in
            if key.attributes.statusSlug == .preRegistered {
                preRegisteredKeys.append(key)
            } else if key.type == .registeredKey {
                registeredKeys.append(key)
            } else {
                unregisteredKeys.append(key)
            }
        }
        
        pixKeySections = [KeyManagerConsumerSection]()
        
        if !registeredKeys.isEmpty {
            pixKeySections.append(KeyManagerConsumerSection(type: .registered, keys: registeredKeys))
        }
        
        if !preRegisteredKeys.isEmpty || !unregisteredKeys.isEmpty {
            pixKeySections.append(KeyManagerConsumerSection(type: .unregistered, keys: preRegisteredKeys + unregisteredKeys))
        }
        
        presenter.presentKeySections(pixKeySections, userData: data)
        trackKeysPresented()
        getPreRegisteredKeys(keys: keys, userData: data)
    }
    
    private func getPreRegisteredKeys(keys: [PixConsumerKey], userData: PixUserData) {
        guard checkForPreRegisteredKeys && !showedPreRegisteredFlow else { return }
        showedPreRegisteredFlow = true
        
        let preRegisteredKeys = keys.filter { $0.attributes.statusSlug == .preRegistered }
        if preRegisteredKeys.contains(where: { $0.attributes.keyType == .cpf }) {
            performKeyValidation(for: .cpf, userData: userData)
        } else if preRegisteredKeys.contains(where: { $0.attributes.keyType == .phone }) {
            performKeyValidation(for: .phone, userData: userData)
        } else if preRegisteredKeys.contains(where: { $0.attributes.keyType == .email }) {
            performKeyValidation(for: .email, userData: userData)
        }
    }
    
    private func didTapRegisteredKeyCell(indexPath: IndexPath) {
        guard
            let selectedKey = getKeyAt(sectionIndex: indexPath.section, row: indexPath.row),
            let keyId = selectedKey.id
        else {
            return
        }
        
        switch selectedKey.attributes.statusSlug {
        case .processed:
            didTapProcessedKey(selectedKey)
        case .processPending:
            presenter.didNextStep(action: .feedback(.keyRecordInProgress(uuid: keyId), origin: .keyManagementScreen))
        case .claimInProgress:
            presenter.didNextStep(
                action: .cancelClaimOrPortability(
                    type: .claimKeyCancel(uuid: keyId),
                    delegate: self,
                    origin: .keyManagementScreen
                )
            )
        case .portabilityInProgress:
            presenter.didNextStep(
                action: .cancelClaimOrPortability(
                    type: .portabilityKeyCancel(uuid: keyId),
                    delegate: self,
                    origin: .keyManagementScreen
                )
            )
        case .awaitingClaimConfirm, .inactiveButCanRevalidate:
            presenter.didNextStep(action: .feedback(.claimDonorReceived(uuid: keyId), origin: .keyManagementScreen))
        case .awaitingPortabilityConfirm:
            presenter.didNextStep(action: .feedback(.portabilityDonorReceived(uuid: keyId), origin: .keyManagementScreen))
        case .deleteFromBacen, .deletedFromBacen:
            presenter.didNextStep(action: .feedback(.pendingKeyDeletion(uuid: keyId), origin: .keyManagementScreen))
        default:
            break
        }
        
        trackKeyAccessed(selectedKey)
    }
    
    private func didTapProcessedKey(_ key: PixConsumerKey) {
        guard
            let userData = userData,
            let keyId = key.id,
            let keyValue = key.attributes.keyValue
            else {
                return
        }
        
        let deleteAction = BottomSheetAction(type: .delete) { [weak self] _ in
            self?.trackNavigation(to: .keyExclusionDialog, from: .bottomSheetPixKey)
            self?.presenter.presentDeleteKeyPopup(keyId: keyId)
        }
        
        let shareAction = BottomSheetAction(type: .share) { [weak self] _ in
            self?.trackNavigation(to: .shareKey, from: .bottomSheetPixKey)
            self?.presenter.presentShareMenu(keyValue: keyValue)
        }
        
        let copyAction = BottomSheetAction(type: .copy) { [weak self] _ in
            self?.trackNavigation(to: .copyKey, from: .bottomSheetPixKey)
            self?.consumerService.setOnClipboard(keyValue: keyValue)
            self?.presenter.presentKeyCopiedAlert()
        }
        
        trackNavigation(to: .bottomSheetPixKey)
        presenter.presentBottomSheetMenu(userData: userData, key: key, actions: [deleteAction, shareAction, copyAction])
    }
    
    private func hasValueForSelectedCellKey(key: PixConsumerKey, userData: PixUserData) -> Bool {
        userData.valueForType(keyType: key.attributes.keyType)?.isNotEmpty ?? false
    }
    
    private func didTapUnregisteredKeyCell(indexPath: IndexPath) {
        guard
            let selectedKey = getKeyAt(sectionIndex: indexPath.section, row: indexPath.row),
            let userData = userData
        else {
            return
        }
        
        if !hasValueForSelectedCellKey(key: selectedKey, userData: userData), selectedKey.attributes.keyType != .random {
            guard let keyName = selectedKey.attributes.keyType.keyName else { return }
            let title = Strings.KeyManager.Consumer.Error.Popup.title(keyName)
            let message = Strings.KeyManager.Consumer.Error.Popup.description
            presenter.presentUpdateRegistrationPopup(title: title, message: message)
            return
        }
        
        trackKeyAccessed(selectedKey)
        
        if consumerService.identityValidationIsEnabled {
            checkIfNeedIdentityValidation(for: selectedKey.attributes.keyType, userData: userData)
        } else {
            performKeyValidation(for: selectedKey.attributes.keyType, userData: userData)
        }
    }
    
    private func checkIfNeedIdentityValidation(for keyType: PixConsumerKeyType, userData: PixUserData) {
        presenter.presentLoading()
        consumerService.getIdValidationStatus { [weak self] result in
            self?.presenter.hideLoading()
            
            switch result {
            case .success(let response):
                self?.processIdentityValidationStatusResponse(response, keyType: keyType, userData: userData)
            case .failure:
                self?.performIdentityValidation(for: keyType, userData: userData)
            }
        }
    }
    
    private func processIdentityValidationStatusResponse(
        _ response: IdentityValidationStatus,
        keyType: PixConsumerKeyType,
        userData: PixUserData
    ) {
        switch response.status {
        case .approved, .verified:
            performKeyValidation(for: keyType, userData: userData)
        default:
            performIdentityValidation(for: keyType, userData: userData)
        }
    }
    
    private func performIdentityValidation(for keyType: PixConsumerKeyType, userData: PixUserData) {
        let action: KeyManagerConsumerAction = .identityValidation { [weak self] idValidationStatus in
            guard idValidationStatus == .approved || idValidationStatus == .verified else {
                return
            }
            self?.performKeyValidation(for: keyType, userData: userData, from: .identityValidation)
        }
        
        presenter.didNextStep(action: action)
        trackNavigation(to: .identityValidation)
    }
    
    private func performKeyValidation(
        for keyType: PixConsumerKeyType,
        userData: PixUserData,
        from origin: PixUserNavigation = .keyManagementScreen
    ) {
        switch keyType {
        case .email:
            validateKey(keyType: .email, keyValue: userData.email, from: origin)
        case .phone:
            guard let phoneNumber = userData.phoneNumber?.formattedFullNumber else { return }
            validateKey(keyType: .phone, keyValue: phoneNumber, from: origin)
        case .cpf:
            guard let cpf = userData.cpf else { return }
            authenticateUserAndRegisterKey(keyType: .cpf, keyValue: cpf)
            trackNavigation(to: .keyAuthentication, from: origin)
        case .random:
            authenticateUserAndRegisterKey(keyType: .random, keyValue: "")
            trackNavigation(to: .keyAuthentication, from: origin)
        }
    }
    
    private func validateKey(keyType: SecurityCodeValidationType, keyValue: String, from origin: PixUserNavigation) {
        presenter.presentLoading()
        
        consumerService.requestNewValidationCode(inputType: keyType) { [weak self] result in
            guard let self = self else { return }
            self.presenter.hideLoading()
            
            switch result {
            case .success (let response):
                self.trackNavigation(to: keyType == .phone ? .keyPhoneVerification : .keyEmailVerification)
                
                let delay = keyType == .email ? response.emailDelay : response.smsDelay
                self.presenter.didNextStep(action:
                                            .validateKey(
                                                keyType: keyType,
                                                keyValue: keyValue,
                                                codeResendDelay: delay,
                                                validationDelegate: self)
                )
            case .failure(let error):
                self.processErrorResponse(error: error, key: nil)
            }
        }
    }
    
    private func authenticateUserAndRegisterKey(keyType: PixKeyType, keyValue: String) {
        if !consumerService.authenticationIsEnabled {
            registerKey(password: nil, keyValue: keyValue, keyType: keyType, from: .keyManagementScreen)
            return
        }
        
        authenticateUser { [weak self] password in
            self?.registerKey(password: password, keyValue: keyValue, keyType: keyType, from: .keyAuthentication)
        }
    }
    
    private func authenticateUser(complete: @escaping (String) -> Void) {
        consumerService.requestPasswordFromUser { result in
            switch result {
            case .success(let password):
                complete(password)
            case .failure(let error):
                guard error == .nilPassword else { return }
                self.presenter.presentErrorPopup(
                    title: Strings.KeyManager.AuthError.title,
                    message: Strings.KeyManager.AuthError.message
                )
            }
        }
    }
    
    private func registerKey(password: String?, keyValue: String, keyType: PixKeyType, from origin: PixUserNavigation) {
        guard let consumerId = consumerService.consumerId else { return }
        
        presenter.presentLoading()
        
        let keyName = keyType == .random ? Strings.KeyManager.Consumer.KeyTitle.random : nil
        let value = keyType == .phone ? keyValue.components(separatedBy: CharacterSet.decimalDigits.inverted).joined() : keyValue
        let requestModel = CreateKey(
            userId: String(consumerId),
            userType: .person,
            value: value,
            type: keyType,
            name: keyName
        )
        pixKeyService.createNewPixKey(key: requestModel, password: password) { [weak self] result in
            self?.presenter.hideLoading()
            
            switch result {
            case let .success(response):
                if response.data.attributes.statusSlug == .processPending {
                    self?.getKeysAndRecheckNewKey(response.data, from: origin)
                    return
                }
                self?.processSuccessRegistrationResponse(key: response.data, from: origin)
                self?.getKeys()
                
            case let .failure(error):
                self?.presenter.hideLoading()
                self?.processErrorResponse(error: error, key: requestModel, from: origin)
            }
        }
    }
    
    private func deleteKey(keyId: String, password: String) {
        presenter.presentLoading()
        
        pixKeyService.deleteKey(keyId: keyId, password: password) { [weak self] result in
            self?.presenter.hideLoading()
            
            switch result {
            case .success:
                self?.presenter.didNextStep(action: .feedback(.pendingKeyDeletion(uuid: keyId), origin: .keyAuthenticationExclusion))
            case let .failure(error):
                let requestError = error.requestError ?? RequestError()
                self?.presenter.presentErrorPopup(title: requestError.title,
                                                  message: requestError.message)
            }
        }
    }
    
    private func getKeyAt(sectionIndex: Int, row: Int) -> PixConsumerKey? {
        guard
            let section = getSectionAt(sessionIndex: sectionIndex),
            row < section.keys.count,
            row >= 0
        else {
            return nil
        }
        return section.keys[row]
    }
    
    private func getSectionAt(sessionIndex: Int) -> KeyManagerConsumerSection? {
        guard sessionIndex < pixKeySections.count, sessionIndex >= 0 else {
            return nil
        }
        return pixKeySections[sessionIndex]
    }
    
    private func processSuccessRegistrationResponse(key: PixConsumerKey, from origin: PixUserNavigation) {
        switch key.attributes.statusSlug {
        case .processed:
            presentKeyRegisteredFeedback(keyId: key.id ?? "", userData: userData, origin: origin)
            
        case .processPending:
            presenter.didNextStep(action: .feedback(.keyRecordInProgress(uuid: key.id ?? ""), origin: origin))
            
        default:
            guard let keyId = key.id else { return }
            presenter.didNextStep(action: .feedback(.keyRecordNotRegistered(uuid: keyId), origin: origin))
        }
    }
    
    private func getKeysAndRecheckNewKey(_ newKey: PixConsumerKey, from origin: PixUserNavigation) {
        guard let userId = consumerService.consumerId else { return }
        presenter.presentLoading()
        
        pixKeyService.listPixKeys(with: "\(userId)") { [weak self] result in
            self?.presenter.hideLoading()
            
            switch result {
            case let .success(pixKeyListResponse):
                if newKey.id != nil, let updatedNewKey = pixKeyListResponse.data.first(where: { $0.id == newKey.id }) {
                    self?.processSuccessRegistrationResponse(key: updatedNewKey, from: origin)
                } else {
                    self?.processSuccessRegistrationResponse(key: newKey, from: origin)
                }
                
                self?.prepareKeys(pixKeyListResponse.data)
                
            case .failure:
                self?.processSuccessRegistrationResponse(key: newKey, from: origin)
            }
        }
    }
    
    private func presentFeedbackIfNecessary() {
        guard let type = feedbackViewType else { return }
        presenter.didNextStep(action: .feedback(type, origin: sceneOrigin))
    }
    
    private func presentKeyRegisteredFeedback(keyId: String, userData: PixUserData?, origin: PixUserNavigation) {
        presenter.didNextStep(action: .feedback(.keyRecordRegistered(uuid: keyId), origin: origin))
    }
}

// MARK: - Process APIError
extension KeyManagerConsumerInteractor: PixCreateKeyErrorHandler {
    private func processErrorResponse(error: ApiError, key: CreateKey?, from origin: PixUserNavigation = .keyManagementScreen) {
        guard
            let key = key,
            let popup = handleError(error: error, key: key)
            else {
                presenter.presentErrorPopup(popup: parseApiError(error: error))
                return
        }
        presenter.presentErrorPopup(popup: popup)
        
        switch popup.errorStatus {
        case .claim:
            trackNavigation(to: .dialogClaimUse, from: origin)
        case .portability:
            trackNavigation(to: .dialogPortability, from: origin)
        case .none:
            return
        }
    }
    
    private func parseApiError(error: ApiError) -> PixCreateKeyErrorModel {
        switch error {
        case .unauthorized:
            return PixCreateKeyErrorModel(
                image: nil,
                title: Strings.KeyManager.AuthError.title,
                description: Strings.KeyManager.AuthError.message,
                buttonTitle: Strings.General.gotIt,
                errorStatus: .none
            )
        default:
            return createGenericError()
        }
    }
}

// MARK: - Analytics methods
extension KeyManagerConsumerInteractor {
    private func trackKeysPresented() {
        guard !keysLoaded else { return }
        keysLoaded = true
        dependencies.analytics.log(KeyRegistrationConsumerEvent.keysViewed)
    }
    
    private func trackKeyAccessed(_ selectedKey: PixConsumerKey) {
        let event = KeyRegistrationConsumerEvent.keyAccessed(type: selectedKey.attributes.keyType, section: selectedKey.type)
        dependencies.analytics.log(event)
    }
    
    private func trackNavigation(to destination: PixUserNavigation, from origin: PixUserNavigation = .keyManagementScreen) {
        let event = PixNavigationEvent.userNavigated(from: origin, to: destination)
        dependencies.analytics.log(event)
    }
    
    func trackScreenOpened() {
        guard feedbackViewType == nil else { return }
        let event = PixNavigationEvent.userNavigated(from: sceneOrigin, to: .keyManagementScreen)
        dependencies.analytics.log(event)
    }
    
    func trackNavigationFromDismissedFeedback(feedbackType: FeedbackViewType) {
        let originFeedback = PixUserNavigation(feedbackType)
        guard originFeedback != .undefined else { return }
        let event = PixNavigationEvent.userNavigated(from: originFeedback, to: .keyManagementScreen)
        dependencies.analytics.log(event)
    }
    
    func trackDialog(title: String, action: DialogsEvent.Action) {
        dependencies.analytics.log(DialogsEvent.dialogInteracted(action: action, title: title))
    }
    
    func setupCloseButton() {
        if sceneOrigin != .hub, sceneOrigin != .settings {
            presenter.presentCloseButton()
        }
    }
}

// MARK: - SecurityValidationCodeDelegate
extension KeyManagerConsumerInteractor: SecurityValidationCodeDelegate {
    func didValidateKeyWithSuccess(keyValue: String, keyType: PixKeyType) {
        let origin: PixUserNavigation = keyType == .phone ? .keyPhoneVerification : .keyEmailVerification
        trackNavigation(to: .keyAuthentication, from: origin)
        
        authenticateUserAndRegisterKey(keyType: keyType, keyValue: keyValue)
    }
}

// MARK: - KeyManagerConsumerInteracting
extension KeyManagerConsumerInteractor: KeyManagerConsumerInteracting {
    func loadAndPresentKeys() {
        presenter.presentLoading()
        reloadKeys()
        
        presentFeedbackIfNecessary()
    }
    
    func reloadKeys() {
        if userData != nil {
            getKeys()
            return
        }
        
        getUserData()
    }
    
    func didTapCell(at indexPath: IndexPath) {
        guard let section = getSectionAt(sessionIndex: indexPath.section) else { return }
        
        switch section.type {
        case .registered:
            didTapRegisteredKeyCell(indexPath: indexPath)
        case .unregistered:
            didTapUnregisteredKeyCell(indexPath: indexPath)
        }
    }
    
    func didTapDelete(keyId: String) {
        trackNavigation(to: .keyAuthenticationExclusion, from: .keyExclusionDialog)
        authenticateUser { [weak self] password in
            self?.deleteKey(keyId: keyId, password: password)
        }
    }
    
    func sendFeedbackEventForCloseAction(feedbackType: FeedbackViewType) {
        switch feedbackType {
        case .pendingKeyDeletion:
            presenter.presentLoading()
            reloadKeys()
        default:
            break
        }
    }
    
    func didClose() {
        presenter.didNextStep(action: .close)
	}
	
    // MARK: - Feedback Action for Claim and Portability
    func requestPortability(claimKey: ClaimKey) {
        pixKeyService.createClaim(claimKey: claimKey) { [weak self] result in
            self?.presenter.hideLoading()
            
            switch result {
            case .success:
                self?.presenter.didNextStep(action: .feedback(.portabilityInProgress(uuid: ""), origin: .keyPortabilityDialog))
            case let .failure(error):
                self?.processErrorResponse(error: error, key: nil)
            }
        }
    }
    
    func requestClaim(claimKey: ClaimKey) {
        pixKeyService.createClaim(claimKey: claimKey) { [weak self] result in
            self?.presenter.hideLoading()
            
            switch result {
            case .success:
                self?.presenter.didNextStep(action: .feedback(.claimInProgress(uuid: ""), origin: .dialogClaimUse))
            case let .failure(error):
                self?.processErrorResponse(error: error, key: nil)
            }
        }
    }
    
    // MARK: - When key has no value redirect to UserRegistration
    func updateUserRegistration() {
        presenter.didNextStep(action: .registrationRenewal)
    }
}

extension KeyManagerConsumerInteractor: FeedbackCancelContainterDelegate {
    func didClose(feedbackType: FeedbackViewType) {
        trackNavigationFromDismissedFeedback(feedbackType: feedbackType)
        switch feedbackType {
        case .claimCancellationInProgress, .portabilityCancellationInProgress:
            reloadKeys()
        default:
            break
        }
    }
}
