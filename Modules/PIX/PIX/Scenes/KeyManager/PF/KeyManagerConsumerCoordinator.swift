import IdentityValidation
import UI
import UIKit

enum KeyManagerConsumerAction {
    case validateKey(
            keyType: SecurityCodeValidationType,
            keyValue: String,
            codeResendDelay: Int?,
            validationDelegate: SecurityValidationCodeDelegate
         )
    case feedback(FeedbackViewType, origin: PixUserNavigation = .undefined)
    case cancelClaimOrPortability(
            type: FeedbackCancelContainerType,
            delegate: FeedbackCancelContainterDelegate,
            origin: PixUserNavigation = .undefined
         )
    case registrationRenewal
    case identityValidation((_ status: IDValidationStatus?) -> Void)
    case close
}

protocol KeyManagerConsumerCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: KeyManagerConsumerAction)
}

final class KeyManagerConsumerCoordinator {
    typealias FeedbacksDelegate = FeedbackContainterDelegate & FeedbackDelegate
    
    enum Deeplink: String {
        case registrationRenewal = "picpay://picpay/registrationrenewal"
        case identityValidation = "picpay://picpay/identityanalysis"
    }
    
    weak var viewController: UIViewController?
    private var idValidationCoordinator: IDValidationFlowCoordinator?
}

// MARK: - KeyManagerCoordinating
extension KeyManagerConsumerCoordinator: KeyManagerConsumerCoordinating {
    func perform(action: KeyManagerConsumerAction) {
        switch action {
        case let .validateKey(keyType, keyValue, codeResendDelay, validationDelegate):
            let validationController = SecurityCodeValidationFactory.make(
                inputedValue: keyValue,
                inputType: keyType,
                codeResendDelay: codeResendDelay,
                delegate: validationDelegate
            )
            viewController?.navigationController?.pushViewController(validationController, animated: true)
            
        case let .feedback(feedbackViewType, origin):
          presentFeedback(feedbackViewType, from: origin)
            
        case .registrationRenewal:
            openDeeplink(.registrationRenewal)
        
        case let .identityValidation(completion):
            openIdentityValidation(completion: completion)
            
        case .close:
            viewController?.navigationController?.dismiss(animated: true)
            
        case let .cancelClaimOrPortability(type, delegate, origin):
            let feedbackCancelContainer = FeedbackCancelContainerFactory.make(
                feedbackType: type,
                delegate: delegate,
                from: origin
            )
            viewController?.navigationController?.present(
                UINavigationController(rootViewController: feedbackCancelContainer),
                animated: true
            )
        }
    }
    
    private func presentFeedback(_ feedbackViewType: FeedbackViewType, from origin: PixUserNavigation) {
        guard let feedbackDelegate = viewController as? FeedbacksDelegate else { return }
        
        var targetViewController: UIViewController
        
        switch feedbackViewType {
        case .portabilityDonorReceived(let uuid):
            targetViewController = FeedbackContainerFactory.make(
                with: .portabilityDonorReceived(uuid: uuid),
                delegate: feedbackDelegate,
                from: origin
            )
        case .claimDonorReceived(let uuid):
            targetViewController = FeedbackContainerFactory.make(
                with: .claimDonorReceived(uuid: uuid),
                delegate: feedbackDelegate,
                from: origin
            )
        case .claimNewValidationKeyAvailable(let uuid):
            targetViewController = FeedbackContainerFactory.make(
                with: .claimNewValidationKeyAvailable(uuid: uuid),
                delegate: feedbackDelegate,
                from: origin
            )
        default:
            targetViewController = FeedbackFactory.make(with: feedbackViewType, delegate: feedbackDelegate, from: origin)
        }
        
        viewController?.navigationController?.present(UINavigationController(rootViewController: targetViewController), animated: true)
    }
    
    private func openDeeplink(_ deeplink: Deeplink) {
        guard let deeplinkURL = URL(string: deeplink.rawValue) else { return }
        viewController?.navigationController?.dismiss(animated: true) {
            PIXSetup.deeplinkInstance?.open(url: deeplinkURL)
        }
    }
    
    private func openIdentityValidation(completion: @escaping (_ status: IDValidationStatus?) -> Void) {
        guard let viewController = viewController, let token = PIXSetup.tokenManager?.token else {
            return
        }
        
        idValidationCoordinator = IDValidationFlowCoordinator(from: viewController, token: token, flow: "PIX") { validationStatus in
            completion(validationStatus)
        }
        idValidationCoordinator?.start()
    }
}
