import Core
import Foundation
import UIKit

enum KeyManagerConsumerFactory {
    static func make(
      from origin: PixUserNavigation,
      checkForPreRegisteredKeys: Bool,
      feedbackType: FeedbackViewType? = nil
    ) -> UIViewController {
        let container = DependencyContainer()
        let coordinator = KeyManagerConsumerCoordinator()
        let consumerService: KeyManagerConsumerServicing = KeyManagerConsumerService(dependencies: container)
        let pixKeyService: PixConsumerKeyServicing = PixConsumerKeyService(dependencies: container)
        let presenter: KeyManagerConsumerPresenting = KeyManagerConsumerPresenter(coordinator: coordinator)
        let interactor = KeyManagerConsumerInteractor(
            consumerService: consumerService,
            pixKeyService: pixKeyService,
            presenter: presenter,
            dependencies: container,
            origin: origin,
            checkForPreRegisteredKeys: checkForPreRegisteredKeys,
            feedbackViewType: feedbackType
        )
        let viewController = KeyManagerConsumerViewController(interactor: interactor)

        presenter.viewController = viewController
        coordinator.viewController = viewController
        
        return viewController
    }
}
