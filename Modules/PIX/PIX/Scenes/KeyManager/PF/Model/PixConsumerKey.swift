import Foundation

struct PixConsumerKeyListResponse: Decodable {
    let data: [PixConsumerKey]
}

struct PixConsumerKeyDataResponse: Decodable {
    let data: PixConsumerKey
}

struct PixConsumerKey: Decodable, Equatable {
    let type: PixKeySection
    let id: String?
    let attributes: PixConsumerKeyAttributes
}

struct PixConsumerKeyAttributes: Decodable, Equatable {
    let statusSlug: PixKeyStatus?
    let keyType: PixConsumerKeyType
    let status, name, keyValue, createdAt: String?
}
