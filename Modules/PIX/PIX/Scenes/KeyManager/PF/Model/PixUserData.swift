import Core
import Foundation
import UI
import UIKit

struct PixUserData: Decodable, Equatable {
    enum CodingKeys: String, CodingKey {
        case email
        case cpf
        case phoneNumber = "phone_number"
    }
    
    let email: String
    let cpf: String?
    let phoneNumber: KeyManagerConsumerPhoneNumber?
    
    private var formattedCPF: String {
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf)
        return mask.maskedText(from: cpf) ?? ""
    }
    
    func formatForType(keyType: PixConsumerKeyType) -> String {
        switch keyType {
        case .cpf:
            return formattedCPF
        case .email:
            return email
        case .phone:
            return phoneNumber?.formattedFullNumber ?? ""
        case .random:
            return ""
        }
    }
    
    func valueForType(keyType: PixConsumerKeyType) -> String? {
        switch keyType {
        case .cpf:
            return cpf
        case .email:
            return email
        case .phone:
            return phoneNumber?.fullNumber
        case .random:
            return nil
        }
    }
}

struct KeyManagerConsumerPhoneNumber: Decodable, Equatable {
    private let country: Int
    private let area: Int
    private let number: Int

    init(country: Int, area: Int, number: Int) {
        self.country = country
        self.area = area
        self.number = number
    }

    var fullNumber: String {
        String(describing: "+\(country)\(area)\(number)")
    }
    
    var formattedFullNumber: String {
        if area == 0 && number == 0 {
            return ""
        }
        return "\(area)\(number)".cellphoneFormatting()
    }
}
