import Foundation
import UIKit

struct PixCreateKeyErrorModel: Equatable {
    let image: UIImage?
    let title: String
    let description: String
    let buttonTitle: String
    let errorStatus: CreateKeyClaimStatus
}
