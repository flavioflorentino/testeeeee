import AssetsKit
import Core
import Foundation

public enum CreteKeyBadRequestStatus: String {
    case claim = "claim_required"
    case portability = "portability_required"
    case none
}

public enum CreateKeyClaimStatus: Equatable {
    case claim(_ claimKey: ClaimKey)
    case portability(_ claimKey: ClaimKey)
    case none
}

protocol PixCreateKeyErrorHandler {
    func handleError(error: ApiError, key: CreateKey) -> PixCreateKeyErrorModel?
    func createGenericError() -> PixCreateKeyErrorModel
}

extension PixCreateKeyErrorHandler {
    func handleError(error: ApiError, key: CreateKey) -> PixCreateKeyErrorModel? {
        guard case let .badRequest(body: body) = error, let requestError = body.errors.first else {
            return nil
        }
        
        switch requestError.code {
        case CreteKeyBadRequestStatus.claim.rawValue:
            let claimKey = ClaimKey(userId: key.userId, userType: .person, keyValue: key.value, keyType: key.type, type: .claim)
            
            let popup = PixCreateKeyErrorModel(image: nil,
                                               title: Strings.KeyManager.Consumer.Popup.Claim.title,
                                               description: Strings.KeyManager.Popup.Claim.description,
                                               buttonTitle: Strings.KeyManager.Popup.Claim.buttonText,
                                               errorStatus: .claim(claimKey))
            return popup
        case CreteKeyBadRequestStatus.portability.rawValue:
            let claimKey = ClaimKey(userId: key.userId, userType: .person, keyValue: key.value, keyType: key.type, type: .portability)
            
            let popup = PixCreateKeyErrorModel(image: nil,
                                               title: Strings.KeyManager.Consumer.Popup.Portability.title,
                                               description: Strings.KeyManager.Popup.Portability.description,
                                               buttonTitle: Strings.KeyManager.Popup.Portability.buttonText,
                                               errorStatus: .portability(claimKey))
            return popup
        default:
            return nil
        }
    }
    
    func createGenericError() -> PixCreateKeyErrorModel {
        PixCreateKeyErrorModel(image: Resources.Illustrations.iluConstruction.image,
                               title: Strings.KeyManager.error,
                               description: Strings.KeyManager.errorDescription,
                               buttonTitle: Strings.KeyManager.errorAction,
                               errorStatus: .none)
    }
}
