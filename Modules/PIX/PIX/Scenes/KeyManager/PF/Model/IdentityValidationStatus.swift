import Foundation
import IdentityValidation

struct IdentityValidationStatus: Decodable {
    let status: IDValidationStatus
}
