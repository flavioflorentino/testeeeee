import Foundation
import UI
import UIKit

public enum PixConsumerKeyType: String, Codable {
    case cpf
    case phone
    case email
    case random
    
    var keyName: String? {
        switch self {
        case .cpf, .email:
            return self.rawValue
        case .phone:
            return "telefone"
        default:
            return nil
        }
    }
    
    var keyTitle: String {
        switch self {
        case .cpf:
            return Strings.KeyManager.Consumer.KeyTitle.cpf
        case .phone:
            return Strings.KeyManager.Consumer.KeyTitle.phone
        case .email:
            return Strings.KeyManager.Consumer.KeyTitle.email
        case .random:
            return Strings.KeyManager.Consumer.KeyTitle.random
        }
    }
    
    var infoMessage: String {
        switch self {
        case .cpf:
            return Strings.KeyManager.Consumer.KeyType.cpf
        case .phone:
            return Strings.KeyManager.Consumer.KeyType.phone
        case .email:
            return Strings.KeyManager.Consumer.KeyType.email
        case .random:
            return Strings.KeyManager.Consumer.KeyType.random
        }
    }
    
    var icon: Iconography {
        switch self {
        case .cpf:
            return .postcard
        case .email:
            return .envelope
        case .phone:
            return .mobileAndroid
        case .random:
            return .keySkeleton
        }
    }
}
