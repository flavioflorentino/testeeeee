import Foundation

struct KeyManagerConsumerSection: Equatable {
    enum SectionType: Equatable {
        case registered, unregistered
    }
    
    let type: SectionType
    let keys: [PixConsumerKey]
}
