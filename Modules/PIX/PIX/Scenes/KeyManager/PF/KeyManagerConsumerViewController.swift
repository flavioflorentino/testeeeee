import AssetsKit
import Core
import UI
import UIKit

protocol KeyManagerConsumerDisplaying: AnyObject {
    func displayKeys(sections: [KeyManagerSection])
    func displayConsumerDataError(message: String)
    func displayErrorPopup(popup: PixCreateKeyErrorModel)
    func displayErrorPopup(title: String, message: String)
    func displayUpdateRegistrationPopup(title: String, message: String)
    func displayLoading()
    func hideLoading()
    func displayDeleteKeyPopup(title: String, message: String, keyId: String)
    func displayDeleteKeyConfirmationPopup(title: String, message: String)
    func displayBottomSheetMenu(with keyItem: KeyManagerItem, actions: [BottomSheetAction])
    func displayShareMenu(sharingText: String)
    func displayKeyCopiedAlert(_ message: String)
    func displayCloseButton()
}

final class KeyManagerConsumerViewController: ViewController<KeyManagerConsumerInteracting, UIView>, LoadingViewProtocol {
    private lazy var footer = KeyManagerConsumerTableViewFooter()
    private lazy var header = KeyManagerConsumerTableViewHeader()
    lazy var loadingView = LoadingView()
    
    private lazy var errorView: GenericErrorView = {
        let view = GenericErrorView()
        view.delegate = self
        view.isHidden = true
        return view
    }()
    
    private lazy var keyManagerTableViewController = KeyManagerTableViewFactory.make(
        keySections: [],
        header: header,
        footer: footer,
        keyManagerOutput: didKeyManagerOutput
    )
     
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadAndPresentKeys()
        interactor.setupCloseButton()
        interactor.trackScreenOpened()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        configureNavigationBar()
    }
    
    override func buildViewHierarchy() {
        addChild(keyManagerTableViewController)
        keyManagerTableViewController.willMove(toParent: self)
        view.addSubview(keyManagerTableViewController.view)
        
        view.addSubview(errorView)
    }
    
    override func setupConstraints() {
        keyManagerTableViewController.view.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        errorView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.top.equalTo(view.compatibleSafeArea.top)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom)
        }
    }

    override func configureViews() {
        title = Strings.KeyManager.Consumer.title
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    private func didKeyManagerOutput(output: KeyManagerTableViewOutput) {
        switch output {
        case let .didTapCell(_, indexPath):
            interactor.didTapCell(at: indexPath)
        case .didRefresh:
            interactor.reloadKeys()
        }
    }
    
    private func configureNavigationBar() {
        navigationController?.navigationBar.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        
        let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        backButton.accessibilityLabel = Strings.General.back
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        if #available(iOS 13.0, *) {
            extendedLayoutIncludesOpaqueBars = true
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationController?.navigationItem.largeTitleDisplayMode = .always
        }
    }
    
    @objc
    private func didTapClose() {
        interactor.didClose()
    }
}

extension KeyManagerConsumerViewController: KeyManagerConsumerDisplaying {
    func displayKeys(sections: [KeyManagerSection]) {
        keyManagerTableViewController.displayNewItems(fromSections: sections)
        keyManagerTableViewController.endRefreshing()
        keyManagerTableViewController.view.isHidden = false
    }
    
    func displayConsumerDataError(message: String) {
        errorView.configure(message: message)
        errorView.isHidden = false
        keyManagerTableViewController.view.isHidden = true
    }
    
    func displayLoading() {
        startLoadingView()
        errorView.isHidden = true
        keyManagerTableViewController.view.isHidden = true
    }
    
    func hideLoading() {
        stopLoadingView()
        keyManagerTableViewController.view.isHidden = false
        keyManagerTableViewController.endRefreshing()
    }
    
    func displayErrorPopup(popup: PixCreateKeyErrorModel) {
        switch popup.errorStatus {
        case let .claim(claimKey):
            showPopup(configure(popup: popup, with: claimAction(key: claimKey, popup: popup)))
        case let .portability(claimKey):
            showPopup(configure(popup: popup, with: portabilityAction(key: claimKey, popup: popup)))
        case .none:
            let controller = PopupViewController(title: popup.title,
                                                 description: popup.description,
                                                 preferredType: .text)
            controller.addAction(confirmAction())
            showPopup(controller)
        }
    }
    
    func displayErrorPopup(title: String, message: String) {
        let controller = PopupViewController(title: title,
                                             description: message,
                                             preferredType: .text)
        controller.addAction(confirmAction())
        showPopup(controller)
    }
    
    func displayUpdateRegistrationPopup(title: String, message: String) {
        let controller = PopupViewController(title: title,
                                             description: message,
                                             preferredType: .text)
        
        let confirmAction = PopupAction(title: Strings.General.gotIt, style: .fill) { [weak self] in
            self?.interactor.updateUserRegistration()
            self?.interactor.trackDialog(title: title, action: .updateRegistration)
        }
        
        let cancelAction = PopupAction(title: Strings.General.cancel, style: .default) { [weak self] in
            self?.interactor.trackDialog(title: title, action: .cancel)
        }
        
        controller.addAction(confirmAction)
        controller.addAction(cancelAction)
        showPopup(controller)
    }
    
    func displayDeleteKeyPopup(title: String, message: String, keyId: String) {
        let controller = PopupViewController(title: title, description: message, preferredType: .text)
        controller.addAction(deleteKeyAction(keyId: keyId))
        controller.addAction(cancelAction())
        showPopup(controller)
    }
    
    func displayDeleteKeyConfirmationPopup(title: String, message: String) {
        let controller = PopupViewController(title: title, description: message, preferredType: .text)
        controller.addAction(deleteSuccessLinkAction())
        controller.addAction(confirmAction())
        showPopup(controller)
    }
    
    func displayBottomSheetMenu(with keyItem: KeyManagerItem, actions: [BottomSheetAction]) {
        let controller = KeyBottomSheetViewController(keyManagerItem: keyItem, actions: actions)
        let transitionDelegate = BottomSheetTransitioningDelegate()
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = transitionDelegate
        present(controller, animated: true)
    }
    
    func displayShareMenu(sharingText: String) {
        let activityController = UIActivityViewController(activityItems: [sharingText], applicationActivities: nil)
        present(activityController, animated: true)
    }
    
    func displayKeyCopiedAlert(_ message: String) {
        let snackbar = ApolloSnackbar(text: message, iconType: .success)
        showSnackbar(snackbar, duration: 3)
    }
    
    func displayCloseButton() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: Strings.General.close,
            style: .plain,
            target: self,
            action: #selector(didTapClose)
        )
    }
}

extension KeyManagerConsumerViewController {
    func configure(popup: PixCreateKeyErrorModel, with action: PopupAction) -> PopupViewController {
        let popupViewController = PopupViewController(title: popup.title,
                                                      description: popup.description,
                                                      preferredType: .business)

        let cancelAction = PopupAction(title: Strings.KeyManager.Popup.notNow, style: .link) { [weak self] in
            self?.interactor.trackDialog(title: popup.title, action: .notNow)
        }

        popupViewController.addAction(action)
        popupViewController.addAction(cancelAction)

        return popupViewController
    }
    
    func confirmAction() -> PopupAction {
        PopupAction(title: Strings.General.gotIt, style: .fill)
    }
    
    func deleteKeyAction(keyId: String) -> PopupAction {
        PopupAction(title: Strings.KeyManager.confirmDeleteAction, style: .destructive) { [weak self] in
            self?.interactor.didTapDelete(keyId: keyId)
        }
    }
    
    func deleteSuccessLinkAction() -> PopupAction {
        PopupAction(title: Strings.KeyManager.Consumer.DeleteKeySuccess.popupLinkDescription, style: .link)
    }
    
    func cancelAction() -> PopupAction {
        PopupAction(title: Strings.General.cancel, style: .default)
    }
    
    func claimAction(key: ClaimKey, popup: PixCreateKeyErrorModel) -> PopupAction {
        PopupAction(title: popup.buttonTitle, style: .fill) { [weak self] in
            self?.displayLoading()
            self?.interactor.requestClaim(claimKey: key)
            self?.interactor.trackDialog(title: popup.title, action: .claimUse)
        }
    }

    func portabilityAction(key: ClaimKey, popup: PixCreateKeyErrorModel) -> PopupAction {
        PopupAction(title: popup.buttonTitle, style: .fill) { [weak self] in
            self?.displayLoading()
            self?.interactor.requestPortability(claimKey: key)
            self?.interactor.trackDialog(title: popup.title, action: .portability)
        }
    }
}

// MARK: - KeyManagerGenericErrorViewDelegate
extension KeyManagerConsumerViewController: GenericErrorViewDelegate {
    func didTryAgain() {
        errorView.isHidden = true
        interactor.loadAndPresentKeys()
    }
}

// MARK: - FeedbackDelegate
extension KeyManagerConsumerViewController: FeedbackDelegate {
    func didAction(action: FeedbackAction, feedbackType: FeedbackViewType) {
        dismiss(animated: true)
        switch action {
        case .close, .tryAgain:
            interactor.sendFeedbackEventForCloseAction(feedbackType: feedbackType)
            interactor.trackNavigationFromDismissedFeedback(feedbackType: feedbackType)
        case .modalDismissed:
            interactor.trackNavigationFromDismissedFeedback(feedbackType: feedbackType)
        default:
            break
        }
    }
}

// MARK: - FeedbackContainterDelegate
extension KeyManagerConsumerViewController: FeedbackContainterDelegate {
    func didClose(feedbackType: FeedbackViewType) {
        interactor.trackNavigationFromDismissedFeedback(feedbackType: feedbackType)
    }
}
