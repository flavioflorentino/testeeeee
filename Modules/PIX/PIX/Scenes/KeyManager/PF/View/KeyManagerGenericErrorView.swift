import AssetsKit
import UI
import UIKit

protocol GenericErrorViewDelegate: AnyObject {
    func didTryAgain()
}

extension GenericErrorView.Layout {
    enum Size {
        static let image: CGFloat = 230
        static let imageSmaller: CGFloat = 180
        static let smallScreenHeight: CGFloat = 568
    }
}

final class GenericErrorView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private let isSmallScreenDevice = UIScreen.main.bounds.height <= Layout.Size.smallScreenHeight
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView(image: Resources.Illustrations.iluFalling.image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .center)
        label.text = Strings.KeyManager.Consumer.Error.title
        return label
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale600())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var textStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = isSmallScreenDevice ? Spacing.base01 : Spacing.base02
        return stackView
    }()
    
    private lazy var tryAgainButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.KeyManager.Consumer.Error.Button.title, for: .normal)
        button.addTarget(self, action: #selector(didTapTryAgainButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = isSmallScreenDevice ? Spacing.base02 : Spacing.base04
        return stackView
    }()
    
    weak var delegate: GenericErrorViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        textStackView.addArrangedSubview(titleLabel)
        textStackView.addArrangedSubview(messageLabel)
        
        contentStackView.addArrangedSubview(imageView)
        contentStackView.addArrangedSubview(textStackView)
        contentStackView.addArrangedSubview(tryAgainButton)
        addSubview(contentStackView)
    }
    
    func setupConstraints() {
        contentStackView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.centerY.equalToSuperview()
            $0.top.greaterThanOrEqualToSuperview()
            $0.bottom.lessThanOrEqualToSuperview()
        }
        
        imageView.snp.makeConstraints {
            $0.height.equalTo(isSmallScreenDevice ? Layout.Size.imageSmaller : Layout.Size.image)
        }
    }

    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
    
    func configure(message: String?) {
        messageLabel.text = message
    }
    
    @objc
    private func didTapTryAgainButton() {
        delegate?.didTryAgain()
    }
}
