import Core
import FeatureFlag
import UIKit

protocol KeyManagerConsumerServicing {
    var consumerId: Int? { get }
    var authenticationIsEnabled: Bool { get }
    var identityValidationIsEnabled: Bool { get }
    func getConsumerData(completion: @escaping (Result<PixUserData, ApiError>) -> Void)
    func getIdValidationStatus(completion: @escaping (Result<IdentityValidationStatus, ApiError>) -> Void)
    func requestNewValidationCode(
        inputType: SecurityCodeValidationType,
        completion: @escaping (Result<SecurityCodeRequestResponse, ApiError>) -> Void
    )
    func requestPasswordFromUser(completion: @escaping (Result<String, AuthenticationStatus>) -> Void)
    func setOnClipboard(keyValue: String)
}

final class KeyManagerConsumerService {
    typealias Dependencies = HasMainQueue & HasFeatureManager
    private var dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension KeyManagerConsumerService: KeyManagerConsumerServicing {
    var consumerId: Int? {
        PIXSetup.consumerInstance?.consumerId
    }
    
    var authenticationIsEnabled: Bool {
        dependencies.featureManager.isActive(.releasePixCreateKeyValidationEnableBool)
    }
    
    var identityValidationIsEnabled: Bool {
        dependencies.featureManager.isActive(.releasePixIdentityValidationBool)
    }
    
    func getConsumerData(completion: @escaping (Result<PixUserData, ApiError>) -> Void) {
        guard let id = consumerId else {
            completion(.failure(.cancelled))
            return
        }
        
        let endpoint = PixKeyManagerConsumerEndpoint.getConsumerData(id)
        Api<PixUserData>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func getIdValidationStatus(completion: @escaping (Result<IdentityValidationStatus, ApiError>) -> Void) {
        guard let id = consumerId else {
            completion(.failure(.cancelled))
            return
        }
        
        let endpoint = PixKeyManagerConsumerEndpoint.getIdValidationStatus(userId: id)
        Api<IdentityValidationStatus>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func requestNewValidationCode(
        inputType: SecurityCodeValidationType,
        completion: @escaping (Result<SecurityCodeRequestResponse, ApiError>) -> Void
    ) {
        let requestModel = SecurityCodeResendRequestModel(type: inputType)
        let endpoint = PixCodeValidationEndpoint.resendCode(requestModel)
        let decoder = JSONDecoder(.convertFromSnakeCase)
        
        Api<SecurityCodeRequestResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func requestPasswordFromUser(completion: @escaping (Result<String, AuthenticationStatus>) -> Void) {
        PIXSetup.authInstance?.authenticate { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let password):
                    completion(.success(password))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    func setOnClipboard(keyValue: String) {
        UIPasteboard.general.string = keyValue
    }
}
