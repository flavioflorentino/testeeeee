import Foundation

public protocol KeyManagerTableviewPresenting: AnyObject {
    var viewController: KeyManagerTableviewDisplay? { get set }
    func displayData()
    func getCell(with item: KeyManagerItem, tableView: UITableView, indexPath: IndexPath) -> UITableViewCell?
    func getSectionHeader(with keyManagerSection: KeyManagerSection, tableView: UITableView) -> UIView?
}

public final class KeyManagerTableviewPresenter {
    public weak var viewController: KeyManagerTableviewDisplay?
    
    private(set) var keySections: [KeyManagerSection]
    
    public init(keySections: [KeyManagerSection]) {
        self.keySections = keySections
    }
    
    public func isItemInfoKeyManager(item: KeyManagerItem) -> Bool {
        !item.infoText.isEmpty
    }
    
    public func isItemDescriptionManager(item: KeyManagerItem) -> Bool {
        !item.secondDescription.isEmpty
    }
    
    public func getCell(with item: KeyManagerItem, tableView: UITableView, indexPath: IndexPath) -> UITableViewCell? {
        if isItemInfoKeyManager(item: item) {
            let cell = tableView.dequeueReusableCell(withIdentifier: KeyManagerInfoTableViewCell.identifier, for: indexPath)
            let listCell = cell as? KeyManagerInfoTableViewCell
            listCell?.configure(with: item)
            return listCell
        }
        
        if isItemDescriptionManager(item: item) {
            let cell = tableView.dequeueReusableCell(withIdentifier: KeyManagerDescriptionTableViewCell.identifier, for: indexPath)
            let listCell = cell as? KeyManagerDescriptionTableViewCell
            listCell?.configure(with: item)
            return listCell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: KeyManagerTableViewCell.identifier, for: indexPath)
        let listCell = cell as? KeyManagerTableViewCell
        listCell?.configure(with: item)
        return listCell
    }
    
    public func getSectionHeader(with keyManagerSection: KeyManagerSection, tableView: UITableView) -> UIView? {
        let title = keyManagerSection.title
        let description = keyManagerSection.description
        
        guard !title.isEmpty, let descriptionText = description else {
            return createSectionHeader(with: title, and: tableView)
        }
        return createSectionHeader(with: title, description: descriptionText, and: tableView)
    }
    
    private func createSectionHeader(with title: String, and tableView: UITableView) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: KeyManagerTableViewSectionHeader.identifier)
        
        guard let headerSectionView = headerView as? KeyManagerTableViewSectionHeader else {
            let currentView = KeyManagerTableViewSectionHeader(reuseIdentifier: KeyManagerTableViewSectionHeader.identifier)
            currentView.sectionTitle = title
            return currentView
        }
        
        headerSectionView.sectionTitle = title
        return headerSectionView
    }
    
    private func createSectionHeader(with title: String, description: String, and tableView: UITableView) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: KeyManagerConsumerTableViewSectionHeader.identifier)
        
        guard let headerSectionView = headerView as? KeyManagerConsumerTableViewSectionHeader else {
            let currentView = KeyManagerConsumerTableViewSectionHeader(reuseIdentifier: KeyManagerConsumerTableViewSectionHeader.identifier)
            currentView.sectionTitle = title
            currentView.sectionDescription = description
            return currentView
        }
        
        headerSectionView.sectionTitle = title
        headerSectionView.sectionDescription = description
        return headerSectionView
    }
}

// MARK: - KeyManagerTableviewPresenting
extension KeyManagerTableviewPresenter: KeyManagerTableviewPresenting {
    public func displayData() {
        viewController?.displayNewItems(fromSections: keySections)
    }
}
