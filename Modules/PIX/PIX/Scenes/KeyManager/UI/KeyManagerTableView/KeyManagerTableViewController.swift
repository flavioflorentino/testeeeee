import UI
import UIKit

public protocol KeyManagerTableviewDisplay: AnyObject {
    func displayNewItems(fromSections sections: [KeyManagerSection])
}

private extension KeyManagerTableViewController.Layout {
    enum TableView {
        static let estimatedRowHeight: CGFloat = 160
    }
}

public final class KeyManagerTableViewController: UIViewController, ViewConfiguration {
    fileprivate enum Layout { }

    private var presenter: KeyManagerTableviewPresenting
    private var keyManagerOutput: (KeyManagerTableViewOutput) -> Void
    
    public var tableHeaderView: UIView? {
        didSet {
            tableView.tableHeaderView = nil
            tableView.layoutIfNeeded()
        }
    }
    
    public var tableFooterView: UIView? {
        didSet {
            tableView.tableFooterView = nil
            tableView.layoutIfNeeded()
        }
    }
    
    private typealias ItemProviderType = TableViewDataSource<KeyManagerSection, KeyManagerItem>.ItemProvider
    
    private lazy var tableViewDataSource: TableViewDataSource<KeyManagerSection, KeyManagerItem> = {
        let dataSource = TableViewDataSource<KeyManagerSection, KeyManagerItem>(view: tableView)
        dataSource.itemProvider = itemProvider
        return dataSource
    }()
    
    private lazy var refreshControl: UIRefreshControl = {
        let control = UIRefreshControl()
        control.addTarget(self, action: #selector(refreshKeyManager), for: .valueChanged)
        return control
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.estimatedRowHeight = Layout.TableView.estimatedRowHeight
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = Spacing.base04
        tableView.sectionFooterHeight = CGFloat.leastNormalMagnitude
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = Colors.backgroundPrimary.color
        tableView.delegate = self
        tableView.refreshControl = refreshControl
        return tableView
    }()
    
    private lazy var itemProvider: ItemProviderType = { tableView, indexPath, item -> UITableViewCell? in
        self.presenter.getCell(with: item, tableView: tableView, indexPath: indexPath)
    }
    
    public init(presenter: KeyManagerTableviewPresenting,
                tableHeaderView: UIView?,
                tableFooterView: UIView?,
                keyManagerOutput: @escaping (KeyManagerTableViewOutput) -> Void) {
        self.presenter = presenter
        self.keyManagerOutput = keyManagerOutput
        super.init(nibName: nil, bundle: nil)
        self.tableHeaderView = tableHeaderView
        self.tableFooterView = tableFooterView
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
        registerCells()
        tableView.dataSource = tableViewDataSource
        presenter.displayData()
    }
    
    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if tableView.tableHeaderView == nil {
            displayTableViewHeader(with: tableHeaderView)
        }
        
        if tableView.tableFooterView == nil {
            displayTableFooterView(tableFooterView)
        }
    }
    
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        tableView.reloadData()
    }
    
    public func endRefreshing() {
        refreshControl.endRefreshing()
    }

    public func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    public func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    private func registerCells() {
        tableView.register(KeyManagerTableViewCell.self,
                           forCellReuseIdentifier: KeyManagerTableViewCell.identifier)
        tableView.register(KeyManagerInfoTableViewCell.self,
                           forCellReuseIdentifier: KeyManagerInfoTableViewCell.identifier)
        tableView.register(KeyManagerDescriptionTableViewCell.self,
                           forCellReuseIdentifier: KeyManagerDescriptionTableViewCell.identifier)
        tableView.register(KeyManagerTableViewSectionHeader.self,
                           forHeaderFooterViewReuseIdentifier: KeyManagerTableViewSectionHeader.identifier)
        tableView.register(KeyManagerConsumerTableViewSectionHeader.self,
                           forHeaderFooterViewReuseIdentifier: KeyManagerConsumerTableViewSectionHeader.identifier)
    }
    
    private func displayTableFooterView(_ footerView: UIView?) {
        let footerView = footerView ?? UIView(frame: .zero)
        updateViewHeight(for: footerView)
        tableView.tableFooterView = footerView
        tableView.layoutIfNeeded()
    }
    
    private func displayTableViewHeader(with header: UIView?) {
        let header = header ?? UIView(frame: .zero)
        updateViewHeight(for: header)
        tableView.tableHeaderView = header
        tableView.layoutIfNeeded()
    }
    
    private func updateViewHeight(for view: UIView) {
        let expectedSize = view.systemLayoutSizeFitting(
            CGSize(width: tableView.frame.width, height: .leastNonzeroMagnitude),
            withHorizontalFittingPriority: .required,
            verticalFittingPriority: .defaultLow)
        if view.frame.size.height != expectedSize.height {
            view.frame.size.height = expectedSize.height
        }
    }
    
    @objc
    private func refreshKeyManager() {
        keyManagerOutput(.didRefresh)
    }
}

// MARK: KeyManagerTableviewDataSource
public extension KeyManagerTableViewController {
    func update(sections: [KeyManagerSection]) {
        for section in sections {
            tableViewDataSource.update(items: section.items, from: section)
        }
    }
    
    func remove(section: KeyManagerSection) {
        tableViewDataSource.remove(section: section)
    }
    
    func removeSection(with name: String) {
        let section = KeyManagerSection(title: name, items: [])
        tableViewDataSource.remove(section: section)
    }
}

// MARK: KeyManagerTableviewDelegate
extension KeyManagerTableViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        self.presenter.getSectionHeader(with: tableViewDataSource.sections[section], tableView: tableView)
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let keyManagerItem = tableViewDataSource.item(at: indexPath) {
            keyManagerOutput(.didTapCell(keyItem: keyManagerItem, indexPath: indexPath))
        }
    }
}

// MARK: KeyManagerTableviewDisplay
extension KeyManagerTableViewController: KeyManagerTableviewDisplay {
    public func displayNewItems(fromSections sections: [KeyManagerSection]) {
        tableViewDataSource.sections.forEach {
            tableViewDataSource.remove(section: $0)
        }
        
        for section in sections where !section.items.isEmpty {
            tableViewDataSource.add(items: section.items, to: section)
        }
    }
}
