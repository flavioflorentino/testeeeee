import Foundation
import UIKit

public enum KeyManagerTableViewFactory {
    public static func make(keySections: [KeyManagerSection],
                            header: UIView? = nil,
                            footer: UIView? = nil,
                            keyManagerOutput: @escaping (KeyManagerTableViewOutput) -> Void) -> KeyManagerTableViewController {
        let presenter: KeyManagerTableviewPresenting = KeyManagerTableviewPresenter(keySections: keySections)
        let viewController = KeyManagerTableViewController(
            presenter: presenter,
            tableHeaderView: header,
            tableFooterView: footer,
            keyManagerOutput: keyManagerOutput)

        presenter.viewController = viewController

        return viewController
    }
}

public enum KeyManagerTableViewOutput {
    case didTapCell(keyItem: KeyManagerItem, indexPath: IndexPath)
    case didRefresh
}
