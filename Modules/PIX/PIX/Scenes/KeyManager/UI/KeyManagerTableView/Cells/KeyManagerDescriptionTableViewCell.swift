import SnapKit
import UI
import UIKit

private extension KeyManagerDescriptionTableViewCell.Layout {
    enum Size {
        static let separatorHeight: CGFloat = 1
        static let arrowHeight: CGFloat = 24
        static let icon = CGSize(width: 48, height: 48)
    }
}

final class KeyManagerDescriptionTableViewCell: UITableViewCell {
    fileprivate struct Layout { }
    
    enum Accessibility: String {
        case descriptionCell
    }
    
    private lazy var backgroundCellView = UIView()
    
    private lazy var iconLabel: UILabel =  {
        let label = UILabel()
        label.labelStyle(IconLabelStyle(type: .medium))
            .with(\.textAlignment, .center)
            .with(\.border, .light())
        label.layer.cornerRadius = Layout.Size.icon.width / 2
        label.accessibilityElementsHidden = true
        return label
    }()
    
    private lazy var arrowImage = UIImageView(image: Assets.arrow.image)
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
             .with(\.textColor, Colors.grayscale500.color)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var secondDescriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
             .with(\.textColor, Colors.grayscale400.color)
        return label
    }()
    
    func configure(with keyItem: KeyManagerItem) {
        configureIcon(with: keyItem)
        descriptionLabel.text = keyItem.description
        titleLabel.text = keyItem.title
        secondDescriptionLabel.text = keyItem.secondDescription
        arrowImage.isHidden = !keyItem.disclosureIndicator
        backgroundCellView.viewStyle(CardViewStyle())
        buildLayout()
        accessibilityIdentifier = Accessibility.descriptionCell.rawValue
        selectionStyle = .none
    }
    
    private func configureIcon(with keyItem: KeyManagerItem) {
        iconLabel.text = keyItem.icon.rawValue
        if keyItem.isFilledBackgroundIcon {
            iconLabel.layer.backgroundColor = Colors.grayscale700.color.cgColor
            iconLabel.layer.borderColor = Colors.grayscale700.color.cgColor
            iconLabel.textColor = Colors.white.color
        } else {
            iconLabel.layer.backgroundColor = Colors.backgroundPrimary.change(.dark, to: Colors.grayscale700.lightColor).color.cgColor
            iconLabel.layer.borderColor = Colors.grayscale100.change(.dark, to: Colors.white.lightColor).color.cgColor
            iconLabel.textColor = Colors.branding600.color
        }
    }
}

extension KeyManagerDescriptionTableViewCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(backgroundCellView)
        backgroundCellView.addSubviews(titleLabel,
                                       descriptionLabel,
                                       secondDescriptionLabel,
                                       iconLabel,
                                       arrowImage)
    }
    
    func setupConstraints() {
        backgroundCellView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.bottom.equalToSuperview().offset(-Spacing.base02)
        }
        
        iconLabel.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.icon)
            $0.leading.equalTo(backgroundCellView).offset(Spacing.base02)
            $0.centerY.equalToSuperview()
        }

        titleLabel.snp.makeConstraints {
            $0.top.equalTo(backgroundCellView).offset(Spacing.base02)
            $0.leading.equalTo(iconLabel.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalTo(arrowImage.snp.leading).offset(-Spacing.base00)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalTo(iconLabel.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalTo(backgroundCellView).offset(-Spacing.base00)
        }
        
        secondDescriptionLabel.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base00)
            $0.bottom.trailing.equalTo(backgroundCellView).inset(Spacing.base02)
            $0.leading.equalTo(iconLabel.snp.trailing).offset(Spacing.base02)
        }
        
        arrowImage.snp.makeConstraints {
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.centerY.equalTo(iconLabel)
            $0.height.width.equalTo(Layout.Size.arrowHeight)
        }
    }
}
