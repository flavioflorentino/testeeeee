import SnapKit
import UI
import UIKit

private extension KeyManagerTableViewSectionHeader.Layout {
    enum Font {
        static let titleFontSize: CGFloat = 18
    }
}

final class KeyManagerTableViewSectionHeader: UITableViewHeaderFooterView {
    fileprivate enum Layout { }
    
    private lazy var customBackgroundView: UIView = {
        let view = UIView()
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale750())
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale750())
        return label
    }()
    
    var sectionTitle: String? {
        didSet {
            titleLabel.text = sectionTitle
        }
    }
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension KeyManagerTableViewSectionHeader: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(titleLabel)
        backgroundView = customBackgroundView
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.trailing.top.bottom.equalToSuperview().inset(Spacing.base02)
            $0.leading.equalToSuperview().inset(Spacing.base03)
        }
    }
}
