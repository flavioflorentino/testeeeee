import SnapKit
import UI
import UIKit

private extension KeyManagerConsumerTableViewSectionHeader.Layout {
    enum Font {
        static let titleFontSize: CGFloat = 18
    }
}

final class KeyManagerConsumerTableViewSectionHeader: UITableViewHeaderFooterView {
    fileprivate enum Layout { }
    
    private lazy var customBackgroundView: UIView = {
        let view = UIView()
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.font, UIFont.systemFont(ofSize: Layout.Font.titleFontSize, weight: .semibold))
            .with(\.textColor, .grayscale750())
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale600())
        return label
    }()
    
    var sectionTitle: String? {
        didSet {
            titleLabel.text = sectionTitle
        }
    }
    
    var sectionDescription: String? {
        didSet {
            descriptionLabel.text = sectionDescription
        }
    }
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension KeyManagerConsumerTableViewSectionHeader: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubviews(titleLabel, descriptionLabel)
        backgroundView = customBackgroundView
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalTo(titleLabel)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
        }
    }
}
