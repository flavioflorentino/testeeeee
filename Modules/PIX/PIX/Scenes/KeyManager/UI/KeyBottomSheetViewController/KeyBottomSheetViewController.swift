import AssetsKit
import UI
import UIKit

extension KeyBottomSheetViewController.Layout {
    enum Size {
        static let headerHeight: CGFloat = 187
        static let buttonHeight = 48
        static let buttonSeparatorHeight = 1
        static let icon = CGSize(width: 68, height: 68)
        static let barIndicator = CGSize(width: 44, height: 4)
    }
}

final class KeyBottomSheetViewController: UIViewController, ViewConfiguration {
    fileprivate enum Layout { }

    private lazy var barIndicator: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle())
            .with(\.layer.cornerRadius, Layout.Size.barIndicator.height / 2)
            .with(\.backgroundColor, Colors.grayscale200.color)
        return view
    }()

    private lazy var iconLabel: UILabel =  {
        let label = UILabel()
        label.labelStyle(IconLabelStyle(type: .large))
            .with(\.textAlignment, .center)
            .with(\.border, .light())
            .with(\.clipsToBounds, true)
            .with(\.layer.cornerRadius, Layout.Size.icon.height / 2)
        label.accessibilityElementsHidden = true
        return label
    }()

    private lazy var titleText: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        return label
    }()

    private lazy var descriptionText: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, Colors.grayscale600.color)
        return label
    }()

    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            iconLabel,
            titleText,
            descriptionText
        ])
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var buttonsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        return stackView
    }()

    private let keyManagerItem: KeyManagerItem

    init(keyManagerItem: KeyManagerItem,
         action: KeyManagerInfoAction,
         actionButtonOutput: @escaping (KeyManagerItem) -> Void) {
        self.keyManagerItem = keyManagerItem
        super.init(nibName: nil, bundle: nil)
        
        let actions = [BottomSheetAction(type: action, handler: actionButtonOutput)]
        setupButtons(actions: actions, keyManagerItem: keyManagerItem)
        preferredContentSize = CGSize(width: self.view.frame.width, height: viewHeight(numberOfButtons: actions.count))
    }
    
    init(keyManagerItem: KeyManagerItem,
         actions: [BottomSheetAction]) {
        self.keyManagerItem = keyManagerItem
        super.init(nibName: nil, bundle: nil)
        
        setupButtons(actions: actions, keyManagerItem: keyManagerItem)
        preferredContentSize = CGSize(width: self.view.frame.width, height: viewHeight(numberOfButtons: actions.count))
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
    }
    
    private func setupButtons(actions: [BottomSheetAction], keyManagerItem: KeyManagerItem) {
        var numberOfSeparators = actions.count - 1
        
        actions.forEach { action in
            buttonsStackView.addArrangedSubview(
                BottomSheetButtonView(action: action, keyManagerItem: keyManagerItem, delegate: self)
            )
            
            if numberOfSeparators > 0 {
                let separatorView = SeparatorView(style: BackgroundViewStyle(color: .grayscale100()))
                buttonsStackView.addArrangedSubview(separatorView)
                numberOfSeparators -= 1
            }
        }
    }
    
    private func viewHeight(numberOfButtons: Int) -> CGFloat {
        let buttonsHeight = (numberOfButtons * Layout.Size.buttonHeight) + (numberOfButtons - 1) * Layout.Size.buttonSeparatorHeight
        var bottomHeight: CGFloat = 0
        
        if #available(iOS 11.0, *), let window = UIApplication.shared.windows.first {
            let safeFrame = window.safeAreaLayoutGuide.layoutFrame
            bottomHeight = window.frame.maxY - safeFrame.maxY
        }
        
        return Layout.Size.headerHeight + CGFloat(buttonsHeight) + bottomHeight
    }

    func buildViewHierarchy() {
        view.addSubviews(barIndicator, contentStackView, buttonsStackView)
    }

    func setupConstraints() {
        barIndicator.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.barIndicator)
        }

        iconLabel.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.icon)
        }

        contentStackView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.top.equalTo(barIndicator.snp.bottom).offset(Spacing.base02)
        }
        
        buttonsStackView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.equalTo(contentStackView.snp.bottom).offset(Spacing.base03)
        }
    }

    func configureViews() {
        view.viewStyle(RoundedViewStyle(cornerRadius: .medium))
        view.backgroundColor = Colors.backgroundPrimary.color
        configureIcon()
        descriptionText.text = keyManagerItem.description
        titleText.text = keyManagerItem.title
    }

    private func configureIcon() {
        iconLabel.text = keyManagerItem.icon.rawValue
        if keyManagerItem.isFilledBackgroundIcon {
            iconLabel.layer.backgroundColor = Colors.grayscale700.color.cgColor
            iconLabel.layer.borderColor = Colors.grayscale700.change(.dark, to: Colors.white.lightColor).color.cgColor
            iconLabel.textColor = Colors.white.color
        } else {
            iconLabel.layer.borderColor = Colors.grayscale100.change(.dark, to: Colors.white.lightColor).color.cgColor
            iconLabel.textColor = Colors.branding600.color
        }
    }
}

extension KeyBottomSheetViewController: BottomSheetButtonViewDelegate {
    func dismissBottomSheet(completion: (() -> Void)?) {
        dismiss(animated: true, completion: completion)
    }
}
