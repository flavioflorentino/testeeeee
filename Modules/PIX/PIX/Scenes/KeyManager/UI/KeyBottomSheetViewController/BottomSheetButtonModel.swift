import Foundation

struct BottomSheetAction {
    let type: KeyManagerInfoAction
    let handler: (KeyManagerItem) -> Void
}

enum KeyManagerInfoAction {
    case delete
    case share
    case copy
    case cancelClaim
    case cancelPortabilityRequest

    var text: String {
        switch self {
        case .delete:
            return Strings.KeyManager.deleteButton
        case .share:
            return Strings.General.share
        case .copy:
            return Strings.KeyManager.copyKey
        case .cancelClaim:
            return Strings.KeyManager.Claim.action
        case .cancelPortabilityRequest:
            return Strings.KeyManager.Cancel.Request.Portability.action
        }
    }
}
