import SnapKit
import UI
import UIKit

protocol BottomSheetButtonViewDelegate: AnyObject {
    func dismissBottomSheet(completion: (() -> Void)?)
}

protocol BottomSheetButtonDisplay: AnyObject {
    func displayTitle(_ title: String)
}

extension BottomSheetButtonView.Layout {
    enum Font {
        static let actionButton = UIFont.systemFont(ofSize: 14)
    }
    
    enum Size {
        static let height: CGFloat = 48
    }
}

final class BottomSheetButtonView: UIView {
    fileprivate enum Layout {}
    
    private let presenter: BottomSheetButtonPresenting
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.font, Layout.Font.actionButton)
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var disclosureIcon: UILabel = {
        let label = UILabel()
        label.labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, .grayscale400())
            .with(\.textAlignment, .center)
        label.numberOfLines = 1
        label.text = Iconography.angleRightB.rawValue
        return label
    }()
    
    init(action: BottomSheetAction, keyManagerItem: KeyManagerItem, delegate: BottomSheetButtonViewDelegate) {
        presenter = BottomSheetButtonPresenter(action: action, keyManagerItem: keyManagerItem, delegate: delegate)
        super.init(frame: .zero)
        
        buildLayout()
        presenter.view = self
        presenter.setupTitle()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    private func didTap() {
        presenter.didTap()
    }
    
    private func configureTapGesture() {
        isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTap))
        addGestureRecognizer(tap)
    }
    
    private func configureAccessibilityElement() {
        isAccessibilityElement = true
    }
}

extension BottomSheetButtonView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(titleLabel)
        addSubview(disclosureIcon)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.leading.centerY.equalToSuperview()
            $0.trailing.equalTo(disclosureIcon.snp.leading)
            $0.top.greaterThanOrEqualTo(snp.top)
            $0.top.lessThanOrEqualTo(snp.bottom)
        }
        
        disclosureIcon.snp.makeConstraints {
            $0.trailing.centerY.equalToSuperview()
        }
        
        snp.makeConstraints {
            $0.height.equalTo(Layout.Size.height)
        }
    }
    
    func configureViews() {
        configureTapGesture()
        configureAccessibilityElement()
    }
}

extension BottomSheetButtonView: BottomSheetButtonDisplay {
    func displayTitle(_ title: String) {
        titleLabel.text = title
        accessibilityLabel = title
    }
}
