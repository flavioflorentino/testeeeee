import Foundation

protocol BottomSheetButtonPresenting: AnyObject {
    var view: BottomSheetButtonDisplay? { get set }
    func setupTitle()
    func didTap()
}

final class BottomSheetButtonPresenter {
    weak var view: BottomSheetButtonDisplay?
    
    private let action: BottomSheetAction
    private let keyManagerItem: KeyManagerItem
    private weak var delegate: BottomSheetButtonViewDelegate?
    
    init(action: BottomSheetAction, keyManagerItem: KeyManagerItem, delegate: BottomSheetButtonViewDelegate) {
        self.action = action
        self.keyManagerItem = keyManagerItem
        self.delegate = delegate
    }
}

extension BottomSheetButtonPresenter: BottomSheetButtonPresenting {
    func setupTitle() {
        view?.displayTitle(action.type.text)
    }
    
    func didTap() {
        delegate?.dismissBottomSheet { [weak self] in
            guard let self = self else { return }
            self.action.handler(self.keyManagerItem)
        }
    }
}
