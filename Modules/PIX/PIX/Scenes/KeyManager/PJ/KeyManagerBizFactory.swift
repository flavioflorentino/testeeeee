import Core
import Foundation
import UIKit

public typealias KeyManagerBizOutput = (KeyManagerBizOutputAction) -> Void

public enum KeyManagerBizOutputAction: Equatable {
    case openCreateKeyFlow(userInfo: KeyManagerBizUserInfo, key: CreateKey)
    case openTFACreateKeyFlow(userInfo: KeyManagerBizUserInfo, key: CreateKey)
    case openRandomCreateKeyFlow(userInfo: KeyManagerBizUserInfo, key: CreateKey)
    case openInformation
    case openFeedback(feedbackType: FeedbackViewType)
    case dismissPortabilityFeedbackAndShowError(errorDescription: String)
    case updateUserInfoInKeyManager(userInfo: KeyManagerBizUserInfo)
}

public enum KeyManagerBizFactory {
    public static func make(userInfo: KeyManagerBizUserInfo?,
                            keyManagerOuput: @escaping KeyManagerBizOutput,
                            inputAction: KeyManagerInputAction,
                            maxAllowedKeys: Int = 20) -> KeyManagerBizViewController {
        let container = DependencyContainer()
        let service: PixBizKeyServicing = PixBizKeyService(dependencies: container)
        let coordinator = KeyManagerBizCoordinator(keyManagerOutput: keyManagerOuput)
        let presenter: KeyManagerBizPresenting = KeyManagerBizPresenter(coordinator: coordinator, dependencies: container)
        let interactor = KeyManagerBizInteractor(service: service, presenter: presenter, dependencies: container, userInfo: userInfo)
        let viewController = KeyManagerBizViewController(interactor: interactor,
                                                         maxAllowedKeys: maxAllowedKeys,
                                                         inputAction: inputAction)

        presenter.viewController = viewController

        return viewController
    }
}
