import AnalyticsModule
import Core
import Foundation
import UI

public protocol KeyManagerBizInteracting: AnyObject {
    func requestKeyManagerData(shouldTrackEvent: Bool)
    func createPixItem(item: KeyManagerItem, isPlaceholder: Bool)
    func openInformation()
    func openRandonKeyInformation()
    func openRandonKeyGenerationInfo()
    func deleteKey(keyManagerItem: KeyManagerItem)
    func cancelClaim(keyManagerItem: KeyManagerItem)
    func findKeyValue(keyManagerItem: KeyManagerItem) -> String
    func findKeyType(uuid: String) -> PixKeyType
    func sendShowDeleteModalEvent(keyManagerItem: KeyManagerItem)
    func sendShowCancelClaimModalEvent(keyType: PixKeyType)
    func sendShowCancelRequestPortabilityEvent(keyType: PixKeyType)
    func sendSuccessfulDeletionEvent()
    func sendComeBackClaimEvent(with key: ClaimKey)
    func sendIgnorePortabilityEvent(uuid: String)
    func tryAgainRequest(of feedbackType: FeedbackViewType)
    func claim(claimKey: ClaimKey)
    func authenticateUser(keyId: String, claimKeyType: ClaimKeyType)
    func openFeedbackScreen(with feedbackType: FeedbackViewType)
    func revalidate(keyId: String)
    func presentError(errorDescription: String)
    func complete(keyId: String, hash: String)
}

public enum KeyManagerAwaitAction {
    case none
    case feedback(feedbackType: FeedbackViewType)
}

public final class KeyManagerBizInteractor {
    typealias Dependencies = HasAnalytics & HasBizAuth & HasUserAuthManager
    private let dependencies: Dependencies
    
    private let service: PixBizKeyServicing
    private let presenter: KeyManagerBizPresenting
    
    private var userInfo: KeyManagerBizUserInfo? {
        didSet {
            guard let userInfo = userInfo else {
                return
            }
            presenter.updateUserInfoInFlow(with: userInfo)
        }
    }
    
    private var pixKeys: [PixKey] = [] {
        didSet {
            runAwaitAction(with: awaitAction)
            awaitAction = .none
        }
    }
    
    private var awaitAction: KeyManagerAwaitAction = .none
    
    init(service: PixBizKeyServicing,
         presenter: KeyManagerBizPresenting,
         dependencies: Dependencies,
         userInfo: KeyManagerBizUserInfo?,
         pixKeys: [PixKey] = []) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.pixKeys = pixKeys
        self.userInfo = userInfo
    }
}

// MARK: - KeyManagerBizInteracting
extension KeyManagerBizInteractor: KeyManagerBizInteracting {
    public func sendIgnorePortabilityEvent(uuid: String) {
        sendEvent(eventType: .portabilityComeBack(method: findKeyType(uuid: uuid)))
    }
    
    public func tryAgainRequest(of feedbackType: FeedbackViewType) {
        switch feedbackType {
        case let .keyDeletionError(id), let .claimDonorReceived(id), let .portabilityDonorReceived(id):
            authenticateUserToDeleteKey(with: id)
        case let .portabilityRefused(uuid):
            guard let userInfo = userInfo else {
                return
            }
            let pixKey = pixKeys.first(where: { $0.registeredKeys.first?.id == uuid })
            guard let registered = pixKey?.registeredKeys.first else {
                presenter.presentError(errorDescription: "mobile - chave não encontrada na lista de chaves do usuário")
                return
            }
            let claimKey = ClaimKey(userId: userInfo.id,
                                    userType: .company,
                                    keyValue: registered.keyValue,
                                    keyType: registered.keyType,
                                    type: .portability)
            claim(claimKey: claimKey)
        case .cancelClaimError(let id):
            cancelClaim(with: id)
        case let .claimRefused(uuid):
            guard let userInfo = userInfo else {
                return
            }
            let pixKey = pixKeys.first(where: { $0.registeredKeys.first?.id == uuid })
            guard let registered = pixKey?.registeredKeys.first else {
                presenter.presentError(errorDescription: "mobile - chave não encontrada na lista de chaves do usuário")
                return
            }
            let claimKey = ClaimKey(userId: userInfo.id,
                                    userType: .company,
                                    keyValue: registered.keyValue,
                                    keyType: registered.keyType,
                                    type: .claim)
            claim(claimKey: claimKey)
        default:
            break
        }
    }
    
    public func openRandonKeyInformation() {
        let event = KeyBizTracker(companyName: userInfo?.name ?? "", eventType: .randomKeyQuestionTapped)
        dependencies.analytics.log(event)
    }
    
    public func openRandonKeyGenerationInfo() {
        let event = KeyBizTracker(companyName: userInfo?.name ?? "", eventType: .randomKeyGenerationInfoViewed)
        dependencies.analytics.log(event)
    }
    
    public func openInformation() {
        presenter.openInformation()
        let event = KeyBizTracker(companyName: userInfo?.name ?? "", eventType: .keyManagerInfoTapped)
        dependencies.analytics.log(event)
    }
    
    public func createPixItem(item: KeyManagerItem, isPlaceholder: Bool) {
        if !isPlaceholder {
            didOpenKey(item: item)
            return
        }
        didOpenPlaceholderKey(item: item)
    }
    
    public func deleteKey(keyManagerItem: KeyManagerItem) {
        guard let pixKey = find(keyManagerItem: keyManagerItem), let id = getId(from: pixKey) else {
            return
        }
        authenticateUserToDeleteKey(with: id)
    }

    public func cancelClaim(keyManagerItem: KeyManagerItem) {
        guard let pixKey = find(keyManagerItem: keyManagerItem), let id = getId(from: pixKey) else {
            presentError(errorDescription: "mobile - chave \(keyManagerItem.title) não encontrada")
            return
        }

        cancelClaim(with: id)
    }
    
    public func openFeedbackScreen(with feedbackType: FeedbackViewType) {
        awaitAction = .feedback(feedbackType: feedbackType)
        requestKeyManagerData()
    }
    
    private func authenticateUserToDeleteKey(with idToDelete: String) {
        authenticateUser { [weak self] hash, errorMessage in
            guard let hash = hash else {
                guard let error = errorMessage else {
                    return
                }
                self?.presenter.presentError(errorDescription: "backend - \(error)")
                return
            }
            self?.deleteKey(with: idToDelete, pin: hash)
        }
    }
    
    private func sendEvent(eventType: KeyBizEvent) {
        let event = KeyBizTracker(companyName: userInfo?.name ?? "", eventType: eventType)
        dependencies.analytics.log(event)
    }
    
    private func openCorrectFeedback(with feedbackType: FeedbackViewType) {
        var correctFeedbackScreen = feedbackType
        switch feedbackType {
        case .claimDonorReceived(let uuid):
            if let pixKey = findPixKey(with: uuid),
               let status = pixKey.registeredKeys.first?.statusSlug,
               status == .awaitingClaimConfirm {
                correctFeedbackScreen = feedbackType
            } else {
                correctFeedbackScreen = .claimDonorRequested(uuid: uuid)
            }
        case let .portabilityDonorReceived(uuid):
            if let pixKey = findPixKey(with: uuid),
               let status = pixKey.registeredKeys.first?.statusSlug,
               status == .awaitingPortabilityConfirm {
                correctFeedbackScreen = feedbackType
            } else {
                correctFeedbackScreen = .portabilityDonorRefused(uuid: uuid)
            }
        default:
            break
        }
        presenter.openFeedbackScreen(with: correctFeedbackScreen)
        sendFeedbackEvent(with: feedbackType)
    }
    
    // swiftlint:disable cyclomatic_complexity function_body_length
    private func sendFeedbackEvent(with feedbackType: FeedbackViewType) {
        var event: KeyBizEvent?
        switch feedbackType {
        case .portabilityCompleted(let uuid):
            event = .portabilitySuccess(keyValue: findKeyValue(uuid: uuid))
        case .portabilityRefused(let uuid):
            event = .portabilityRefused(keyValue: findKeyValue(uuid: uuid))
        case .portabilityInProgress(let uuid):
            event = .portabilityInfo(keyValue: findKeyValue(uuid: uuid))
        case .claimRefused(let uuid):
            event = .claimRefused(method: findKeyType(uuid: uuid))
        case .claimCompleted(let uuid):
            event = .claimSuccess(method: findKeyType(uuid: uuid))
        case .keyRecordNotRegistered(let uuid):
            event = .communicationError(method: findKeyType(uuid: uuid), error: "")
        case .keyRecordRegistered(let uuid):
            event = .successfulRegistration(method: findKeyType(uuid: uuid))
        case .keyRecordInProgress(let uuid):
            event = .pendingRegistration(keyValue: findKeyValue(uuid: uuid))
        case .portabilityDonorCompleted(let uuid):
            event = .portabilityRequestDone(keyValue: findKeyValue(uuid: uuid))
        case .claimDonorRequested(let uuid):
            event = .claimOutOfDate(method: findKeyType(uuid: uuid))
        case .claimDonorReceived(let uuid):
            event = .claimAllowed(method: findKeyType(uuid: uuid))
        case .claimDonorNotCompleted(let uuid):
            event = .claimRequestUndone(method: findKeyType(uuid: uuid))
        case .claimDonorInProgress(let uuid):
            event = .claimPendingRegistration(method: findKeyType(uuid: uuid))
        case .claimDonorRevalidationNotCompleted(let uuid):
            event = .claimRequestUndone(method: findKeyType(uuid: uuid))
        case .claimDonorRevalidationCompleted(let uuid):
            event = .claimRequestDone(keyValue: findKeyValue(uuid: uuid))
        case .pendingKeyDeletion(let uuid):
            event = .deletionPending(method: findKeyType(uuid: uuid))
        case .portabilityDonorRefused(let uuid):
            event = .portabilityOutOfDate(method: findKeyType(uuid: uuid))
        case .claimKeyAccepted(let uuid):
            event = .claimKeyAccepted(method: findKeyType(uuid: uuid))
        case .validationNotCompleted(let uuid):
            event = .validationNotCompleted(method: findKeyType(uuid: uuid))
        default:
            break
        }
        if let event = event {
            dependencies.analytics.log(KeyBizTracker(companyName: userInfo?.name ?? "", eventType: event))
        }
    }

    private func cancelClaim(with id: String) {
        guard let pixKey = findPixKey(with: id) else {
            presentError(errorDescription: "mobile - chave com id \(id) não encontrada")
            return
        }
        presenter.startLoading()
        service.cancelClaim(withKeyId: id, hash: nil) { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success:
                switch pixKey.registeredKeys.first?.statusSlug {
                case .awaitingClaimConfirm:
                    self?.presenter.displayCancelClaimSuccess(with: pixKey)
                case .awaitingPortabilityConfirm:
                    self?.presenter.displayCancelRequestPortabilitySuccess(with: pixKey)
                default:
                    return
                }
            case .failure:
                let id = pixKey.registeredKeys.first?.id ?? ""
                self?.presenter.openFeedbackScreen(with: .cancelClaimError(id: id))
            }
        }
    }
    
    public func requestKeyManagerData(shouldTrackEvent: Bool = false) {
        if let userInfo = userInfo {
            getPixKeys(userInfo: userInfo)
            return
        }
        guard let userAuthManagerContract = dependencies.userAuthManagerContract else {
            presenter.presentError(errorDescription: "mobile - user auth manager não inicializado")
            return
        }
        presenter.startLoading()
        userAuthManagerContract.getUserInfo(completion: { [weak self] userInfo in
            if let userInfo = userInfo {
                self?.userInfo = userInfo
                if shouldTrackEvent {
                    self?.sendOpenScreenEvent()
                }
                self?.getPixKeys(userInfo: userInfo)
            } else {
                self?.presenter.presentError(errorDescription: "mobile - informação do usuário não encontrada")
            }
        })
    }
    
    public func presentError(errorDescription: String) {
        presenter.presentError(errorDescription: errorDescription)
    }
    
    private func getPixKeys(userInfo: KeyManagerBizUserInfo) {
        service.listPixKeys(with: userInfo.id, and: .company) { [weak self] result in
            switch result {
            case let .success(pixKeys):
                self?.presenter.stopLoading()
                self?.pixKeys = pixKeys.data
                self?.presenter.populateData(with: pixKeys.data, userInfo: userInfo)
            case .failure(.unauthorized):
                self?.presenter.openFeedbackScreen(with: .unauthorized)
            case let .failure(error):
                self?.presenter.presentError(errorDescription: "backend - \(error.localizedDescription)")
            }
        }
    }
    
    public func sendShowDeleteModalEvent(keyManagerItem: KeyManagerItem) {
        guard let pixKeyType = getType(from: find(keyManagerItem: keyManagerItem)) else {
            return
        }
        sendEvent(eventType: .deletionModal(method: pixKeyType))
    }
    
    public func sendSuccessfulDeletionEvent() {
        sendEvent(eventType: .deletionSuccess(method: .random))
    }

    public func sendShowCancelClaimModalEvent(keyType: PixKeyType) {
        sendEvent(eventType: .cancelClaimModal(method: keyType))
    }

    public func sendShowCancelRequestPortabilityEvent(keyType: PixKeyType) {
        sendEvent(eventType: .cancelRequestPortability(method: keyType))
    }
    
    public func sendComeBackClaimEvent(with key: ClaimKey) {
        switch key.type {
        case .claim:
            sendEvent(eventType: .claimComeBack(method: key.keyType))
        case .portability:
            sendEvent(eventType: .portabilityComeBack(method: key.keyType))
        }
    }
    
    public func findKeyValue(keyManagerItem: KeyManagerItem) -> String {
        find(keyManagerItem: keyManagerItem)?.registeredKeys.first?.keyValue ?? ""
    }
    
    public func findKeyType(uuid: String) -> PixKeyType {
        findPixKey(with: uuid)?.registeredKeys.first?.keyType ?? .random
    }
    
    public func findKeyValue(uuid: String) -> String {
        findPixKey(with: uuid)?.registeredKeys.first?.keyValue ?? ""
    }
    
    public func claim(claimKey: ClaimKey) {
        sendClaimEvent(with: claimKey)
        presenter.startLoading()
        service.claim(claimKey: claimKey) { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success:
                if claimKey.type == .portability {
                    self?.openFeedbackScreen(with: .portabilityInProgress(uuid: ""))
                } else {
                    self?.sendEvent(eventType: .claimInfo(method: claimKey.keyType))
                    self?.openFeedbackScreen(with: .claimInProgress(uuid: ""))
                }
            case let .failure(error):
                self?.presenter.presentError(errorDescription: "backend - \(error.localizedDescription)")
            }
        }
    }
    
    public func revalidate(keyId: String) {
        authenticateUser { [weak self] hash, errorMessage in
            guard let hash = hash else {
                guard let error = errorMessage else {
                    return
                }
                self?.presenter.presentError(errorDescription: "backend - \(error)")
                return
            }
            self?.service.cancelClaim(withKeyId: keyId, hash: hash) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success:
                    self.presenter.openFeedbackScreen(with: .claimDonorInProgress(uuid: keyId))
                case let .failure(error):
                    self.presenter.presentError(errorDescription: "backend - \(error.localizedDescription)")
                }
            }
        }
    }
    
    public func authenticateUser(completion: @escaping (_ pin: String?, _ errorMessage: String?) -> Void) {
        guard let businessAuthContract = dependencies.businessAuthContract else {
            presenter.presentError(errorDescription: "mobile - user auth manager não inicializado")
            return
        }
        businessAuthContract.performActionWithAuthorization(nil) { result in
            switch result {
            case let .success(pin):
                completion(pin, nil)
            case let .failure(errorMessage):
                completion(nil, errorMessage)
            case .canceled:
                completion(nil, nil)
            }
        }
    }

    public func complete(keyId: String, hash: String) {
        service.completeClaim(uuid: keyId, hash: hash) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success:
                self.presenter.openFeedbackScreen(with: .claimCompleted(uuid: keyId))
            case let .failure(error):
                self.presenter.presentError(errorDescription: "backend - \(error.localizedDescription)")
            }
        }
    }

    public func authenticateUser(keyId: String, claimKeyType: ClaimKeyType) {
        if claimKeyType == .portability {
            sendEvent(eventType: .portabilityReceivedStart(method: findKeyType(uuid: keyId)))
        }
        authenticateUser { [weak self] hash, errorMessage in
            guard let hash = hash else {
                guard let error = errorMessage else {
                    return
                }
                self?.presenter.presentError(errorDescription: "backend - \(error)")
                return
            }
            self?.confirmClaim(withId: keyId, pin: hash, claimKeyType: claimKeyType)
        }
    }
}

private extension KeyManagerBizInteractor {
    func getId(from pixKey: PixKey) -> String? {
        pixKey.registeredKeys.first?.id
    }
    
    func getType(from pixKey: PixKey?) -> PixKeyType? {
        pixKey?.registeredKeys.first?.keyType
    }
    
    func didOpenPlaceholderKey(item: KeyManagerItem) {
        if let cellTapType = findPlaceholderKeyType(with: item),
           let userInfo = userInfo {
            let event = KeyBizTracker(companyName: userInfo.name, eventType: .keyManagerSelected(method: cellTapType))
            dependencies.analytics.log(event)
            let createdKey = createKey(with: item)
            presenter.openCreateCellFlow(userInfo: userInfo, key: createdKey, validated: false)
        }
    }
    
    func didOpenKey(item: KeyManagerItem) {
        let keytype = find(keyManagerItem: item)?.registeredKeys.first?.keyType ?? .random
        let status = find(keyManagerItem: item)?.registeredKeys.first?.statusSlug
        
        switch status {
        case .awaitingClaimConfirm:
            presenter.prepareToCancelClaim(keyItem: item)
            sendEvent(eventType: .claimDeletion(method: keytype))
        case .awaitingPortabilityConfirm:
            presenter.prepareToCancelRequestPortability(keyItem: item)
        case .processed:
            presenter.prepareToDelete(keyItem: item)
        default:
            return
        }
    }
    
    func isRandomKey(keyManagerItem: KeyManagerItem) -> Bool {
        keyManagerItem.title == Strings.KeySelection.randomKeyTitle || !keyManagerItem.secondDescription.isEmpty
    }
    
    func find(keyManagerItem: KeyManagerItem) -> PixKey? {
        pixKeys.first(where: { $0.registeredKeys.first?.id == keyManagerItem.itemId })
    }
    
    func findPlaceholderKeyType(with item: KeyManagerItem) -> PixKeyType? {
        switch item.title {
        case Strings.KeyManager.phoneCell:
            return .phone
        case Strings.KeyManager.mailCell:
            return .email
        case Strings.KeyManager.cnpjCell:
            return .cnpj
        case Strings.KeyManager.randomCell:
            return .random
        default:
            return nil
        }
    }
    
    func findItemValue(with type: PixKeyType, item: KeyManagerItem) -> String {
        type == .random ? "" : item.description
    }
    
    func findItemValueForCreate(with type: PixKeyType, item: KeyManagerItem) -> String {
        switch type {
        case .cnpj, .phone:
            return item.description.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        case .email:
            return item.description
        default:
            return ""
        }
    }
    
    func sendOpenScreenEvent() {
        let event = KeyBizTracker(companyName: userInfo?.name ?? "", eventType: .keyManagerViewed)
        dependencies.analytics.log(event)
    }
    
    func createKey(with item: KeyManagerItem) -> CreateKey {
        let keyType = findPlaceholderKeyType(with: item)
        let itemValue = findItemValueForCreate(with: keyType ?? .random, item: item)
        let userId = userInfo?.id ?? ""
        let createBizKey = CreateKey(userId: userId, userType: .company, value: itemValue, type: keyType ?? .random)
        return createBizKey
    }
    
    func createKeyFromPixKey(with key: PixKey) -> CreateKey? {
        var createdKey: CreateKey?
        if let userId = userInfo?.id, let pixKey = key.registeredKeys.first {
            let keyType = pixKey.keyType
            let keyValue = pixKey.keyValue
            let name = pixKey.name
            createdKey = CreateKey(userId: userId, userType: .company, value: keyValue, type: keyType, name: name)
        }
        return createdKey
    }
    
    func findPixKey(with id: String) -> PixKey? {
        pixKeys.first(where: { $0.registeredKeys.first?.id == id })
    }
    
    func deleteKey(with id: String, pin: String) {
        guard let userId = userInfo?.id else {
            return
        }
        presenter.startLoading()
        service.deleteKey(userId: userId, keyId: id, userType: .company, pin: pin) { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success:
                let feedbackViewType = FeedbackViewType.pendingKeyDeletion(uuid: id)
                self?.openFeedbackScreen(with: feedbackViewType)
            case .failure:
                let feedbackViewType = FeedbackViewType.keyDeletionError(uuid: id)
                self?.openFeedbackScreen(with: feedbackViewType)
            }
        }
    }
    
    func runAwaitAction(with action: KeyManagerAwaitAction) {
        switch action {
        case let .feedback(feedbackType):
            openCorrectFeedback(with: feedbackType)
        case .none:
            return
        }
    }
    
    func confirmClaim(withId keyId: String, pin: String, claimKeyType: ClaimKeyType) {
        guard let userId = userInfo?.id else { return }
        presenter.startLoading()
        service.confirmClaim(userId: userId, keyId: keyId, pin: pin) { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success:
                switch claimKeyType {
                case .portability:
                    self?.presenter.openFeedbackScreen(with: .portabilityDonorInProgress(uuid: keyId))
                    self?.sendEvent(eventType: .portabilityPendingRegistration(method: self?.findKeyType(uuid: keyId) ?? .random))
                case .claim:
                    self?.presenter.openFeedbackScreen(with: .claimDonorPendent(uuid: keyId))
                }
                
            case let .failure(error):
                self?.presenter.dismissPortabilityFeedbackAndShowError(errorDescription: "backend - \(error.localizedDescription)")
            }
        }
    }
    
    func sendClaimEvent(with key: ClaimKey) {
        switch key.type {
        case .claim:
            sendEvent(eventType: .claimStart(method: key.keyType))
        case .portability:
            sendEvent(eventType: .portabilityStart(method: key.keyType))
        }
    }
}
