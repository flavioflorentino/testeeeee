import AssetsKit
import UI
import UIKit

public enum KeyManagerInputAction: Equatable {
    case reload
    case none
    case portability(key: ClaimKey)
    case claim(key: ClaimKey)
    case feedback(feedbackType: FeedbackViewType)
    case tryAgain(feedbackType: FeedbackViewType)
    case revalidateClaim(keyId: String)
    case revalidateClaimKeyAccepted(keyId: String, hash: String)
    case confirmPortability(keyId: String)
    case confirmClaim(keyId: String)
    case error(description: String)
    case removalCompleted
}

protocol KeyManagerBizDisplay: AnyObject {
    func displayData(with keySections: [KeyManagerSection])
    func changeFooterCount(with keyCount: Int)
    func displayError(message: String)
    func showDeleteAlert(keyItem: KeyManagerItem)
    func showCancelClaimAlert(keyItem: KeyManagerItem)
    func showCancelRequestPortability(keyItem: KeyManagerItem)
    func startLoading()
    func stopLoading()
    func showPopup(with popUpType: KeyManagerPopUpType)
    func input(action: KeyManagerInputAction)
    func findKeyType(uuid: String) -> PixKeyType
    func sendIgnorePortabilityEvent(uuid: String)
}

public final class KeyManagerBizViewController: ViewController<KeyManagerBizInteracting, UIView>, StatefulTransitionViewing {
    private lazy var footer = KeyManagerTableViewFooter(didOpenLink: didOpenLinkInFooter)
    private lazy var header = KeyManagerTableViewHeader()
    private let maxAllowedKeys: Int
    private var inputAction: KeyManagerInputAction
    
    private var keySections: [KeyManagerSection] = [] {
        didSet {
            self.keyManagerTableViewController.displayNewItems(fromSections: keySections)
        }
    }
    
    private lazy var infoButton = {
        UIBarButtonItem(image: Resources.Icons.icoQuestion.image,
                        style: .plain,
                        target: self,
                        action: #selector(tapInfoButton(sender:)))
    }()
    
    private lazy var keyManagerTableViewController: KeyManagerTableViewController = {
        KeyManagerTableViewFactory.make(keySections: keySections,
                                        header: header,
                                        footer: footer,
                                        keyManagerOutput: didKeyManagerOutput)
    }()
    
    public init(interactor: KeyManagerBizInteracting, maxAllowedKeys: Int, inputAction: KeyManagerInputAction) {
        self.maxAllowedKeys = maxAllowedKeys
        self.inputAction = inputAction
        super.init(interactor: interactor)
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        input(action: inputAction)
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.barTintColor = .white
    }
    
    override public func buildViewHierarchy() {
        view.addSubview(keyManagerTableViewController.view)
    }
    
    @objc
    private func tapInfoButton(sender: UIBarButtonItem) {
        interactor.openInformation()
    }
    
    override public func setupConstraints() {
        keyManagerTableViewController.view.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    override public func configureViews() {
        configureNavigation()
        title = Strings.KeyManager.title
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationItem.rightBarButtonItem = infoButton
    }
    
    public func didOpenLinkInFooter() {
        interactor.openRandonKeyInformation()
        showPopup(with: .random)
    }
    
    private func didKeyManagerOutput(output: KeyManagerTableViewOutput) {
        switch output {
        case let .didTapCell(keyItem, _):
            let isPlaceholder: Bool = keyItem.section == PixKeySection.unregisteredKey
            interactor.createPixItem(item: keyItem, isPlaceholder: isPlaceholder)
        case .didRefresh:
            interactor.requestKeyManagerData(shouldTrackEvent: false)
        }
    }
    
    public func didTryAgain() {
        endState()
        interactor.requestKeyManagerData(shouldTrackEvent: false)
    }
    
    public func startLoading() {
        beginState(model: StateLoadingViewModel(message: Strings.KeyManager.loading))
    }
    
    public func stopLoading() {
        endState()
    }
    
    private func didTapKeyManagerDeleteButton(keyManagerItem: KeyManagerItem) {
        dismiss(animated: true) {
            self.showPopup(with: .delete(keyManagerItem: keyManagerItem))
        }
    }
}

// MARK: KeyManagerBizDisplay
extension KeyManagerBizViewController: KeyManagerBizDisplay {
    func sendIgnorePortabilityEvent(uuid: String) {
        interactor.sendIgnorePortabilityEvent(uuid: uuid)
    }
    
    // swiftlint:disable:next cyclomatic_complexity
    func input(action: KeyManagerInputAction) {
        switch action {
        case let .claim(claimKey):
            showPopup(with: .claim(claimKey: claimKey))
        case let .portability(claimKey):
            showPopup(with: .portability(claimKey: claimKey))
        case let .feedback(feedbackType):
            interactor.openFeedbackScreen(with: feedbackType)
        case let .tryAgain(feedbackType):
            interactor.tryAgainRequest(of: feedbackType)
        case let .confirmPortability(keyId):
            interactor.authenticateUser(keyId: keyId, claimKeyType: .portability)
        case let .error(description):
            interactor.presentError(errorDescription: description)
        case let .revalidateClaim(keyId):
            interactor.revalidate(keyId: keyId)
        case let .revalidateClaimKeyAccepted(keyId, hash):
            interactor.complete(keyId: keyId, hash: hash)
        case .none:
            interactor.requestKeyManagerData(shouldTrackEvent: true)
        case let .confirmClaim(keyId):
            interactor.authenticateUser(keyId: keyId, claimKeyType: .claim)
        case .reload:
            interactor.requestKeyManagerData(shouldTrackEvent: false)
        case .removalCompleted:
            interactor.requestKeyManagerData(shouldTrackEvent: true)
            interactor.sendSuccessfulDeletionEvent()
        }
    }
    
    // swiftlint:disable:next function_body_length
    func showPopup(with popUpType: KeyManagerPopUpType) {
        switch popUpType {
        case .claim(let claimKey):
            let model = KeyManagerModal(image: nil,
                                        title: Strings.KeyManager.Popup.Claim.title,
                                        description: Strings.KeyManager.Popup.Claim.description,
                                        buttonTitle: Strings.KeyManager.Popup.Claim.buttonText)
            let popUp = configure(popup: model,
                                  with: claimAction(title: model.buttonTitle, claimKey: claimKey),
                                  and: claimCancelAction(claimKey: claimKey))
            showPopup(popUp)
        case .portability(let claimKey):
            let model = KeyManagerModal(image: nil,
                                        title: Strings.KeyManager.Popup.Portability.title,
                                        description: Strings.KeyManager.Popup.Portability.description,
                                        buttonTitle: Strings.KeyManager.Popup.Portability.buttonText)
            let popUp = configure(popup: model,
                                  with: claimAction(title: model.buttonTitle, claimKey: claimKey),
                                  and: claimCancelAction(claimKey: claimKey))
            showPopup(popUp)
        case .random:
            let model = KeyManagerModal(image: nil,
                                        title: Strings.KeyManager.RandomModal.title,
                                        description: Strings.KeyManager.RandomModal.description,
                                        buttonTitle: Strings.KeyManager.gotIt)
            let popUpAction = PopupAction(title: Strings.KeyManager.gotIt, style: .fill)
            let popUp = configure(popup: model,
                                  with: popUpAction,
                                  preferredType: .image(Resources.Illustrations.iluSign.image),
                                  enableNotNowButton: false)
            interactor.openRandonKeyGenerationInfo()
            showPopup(popUp)
        case .delete(let keyManager):
            let keyValue = interactor.findKeyValue(keyManagerItem: keyManager)
            let model = KeyManagerModal(image: nil,
                                        title: Strings.KeyManager.deleteKey,
                                        description: Strings.KeyManager.confirmDeleteMessage(keyValue),
                                        buttonTitle: Strings.KeyManager.confirmDeleteAction)
            let popUp = configure(popup: model,
                                  with: deleteAction(title: Strings.KeyManager.confirmDeleteAction, keyManagerItem: keyManager))
            showPopup(popUp)
        case .generic:
            let popup = createGenericError()
            let popUp = configure(popup: popup, with: PopupAction(title: "", style: .default, completion: {}))
            showPopup(popUp)
        case .cancelClaim(let keyManager):
            let action = PopupAction(title: Strings.KeyManager.Claim.confirm,
                                     style: .destructive) { [weak self] in
                self?.interactor.cancelClaim(keyManagerItem: keyManager)
            }
            let model = KeyManagerModal(image: nil,
                                        title: Strings.KeyManager.Claim.action,
                                        description: Strings.KeyManager.Claim.message,
                                        buttonTitle: Strings.KeyManager.Claim.confirm)
            let popUp = configure(popup: model, with: action)
            showPopup(popUp)
        case .cancelClaimSuccess(let keyType):
            let action = PopupAction(title: Strings.KeyManager.gotIt, style: .fill)
            let model = KeyManagerModal(image: nil,
                                        title: Strings.KeyManager.Claim.Success.title,
                                        description: Strings.KeyManager.Claim.Success.message,
                                        buttonTitle: "")
            let popUp = configure(popup: model, with: action, enableNotNowButton: false)
            showPopup(popUp)
            interactor.sendShowCancelClaimModalEvent(keyType: keyType)
        case .cancelRequestPortability(let keyManager):
            typealias Portability = Strings.KeyManager.Cancel.Request.Portability
            let action = PopupAction(title: Portability.confirm,
                                     style: .destructive) { [weak self] in
                self?.interactor.cancelClaim(keyManagerItem: keyManager)
            }
            let model = KeyManagerModal(image: nil,
                                        title: Portability.action,
                                        description: Portability.message,
                                        buttonTitle: Portability.confirm)
            let popUp = configure(popup: model, with: action)
            showPopup(popUp)
        case .cancelRequestPortabilitySuccess(let keyType):
            typealias Portability = Strings.KeyManager.Cancel.Request.Portability.Success
            let action = PopupAction(title: Strings.KeyManager.gotIt, style: .fill)
            let model = KeyManagerModal(image: nil,
                                        title: Portability.title,
                                        description: Portability.message,
                                        buttonTitle: "")
            let popUp = configure(popup: model, with: action, enableNotNowButton: false)
            showPopup(popUp)
            interactor.sendShowCancelClaimModalEvent(keyType: keyType)
        }
    }
    
    func claimAction(title: String, claimKey: ClaimKey) -> PopupAction {
        PopupAction(title: title, style: .fill) { [weak self] in
            self?.interactor.claim(claimKey: claimKey)
        }
    }
    
    func claimCancelAction(claimKey: ClaimKey) -> PopupAction {
        PopupAction(title: Strings.KeyManager.Popup.notNow, style: .link) { [weak self] in
            self?.interactor.sendComeBackClaimEvent(with: claimKey)
        }
    }
    
    func deleteAction(title: String, keyManagerItem: KeyManagerItem) -> PopupAction {
        PopupAction(title: title, style: .destructive) { [weak self] in
            self?.interactor.deleteKey(keyManagerItem: keyManagerItem)
        }
    }
    
    func createGenericError() -> KeyManagerModal {
        KeyManagerModal(image: Resources.Illustrations.iluConstruction.image,
                        title: Strings.KeyManager.error,
                        description: Strings.KeyManager.errorDescription,
                        buttonTitle: Strings.KeyManager.errorAction)
    }
    
    func findKeyType(uuid: String) -> PixKeyType {
        interactor.findKeyType(uuid: uuid)
    }
    
    private func bottomSheet(with keyItem: KeyManagerItem,
                             action: KeyManagerInfoAction,
                             handle: @escaping (KeyManagerItem) -> Void) {
        let controller = KeyBottomSheetViewController(keyManagerItem: keyItem,
                                                      action: action,
                                                      actionButtonOutput: handle)
        let transitionDelegate = BottomSheetTransitioningDelegate()
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = transitionDelegate
        present(controller, animated: true)
    }
    
    func displayData(with keySections: [KeyManagerSection]) {
        self.keySections = keySections
        keyManagerTableViewController.endRefreshing()
    }
    
    func changeFooterCount(with keyCount: Int) {
        footer.setItemCount(actualKeysCount: keyCount, maxKeys: maxAllowedKeys)
    }
    
    @objc
    func close() {
        navigationController?.dismiss(animated: true)
    }
    
    func configureNavigation() {
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.backgroundColor = .clear
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.tintColor = Colors.branding400.color
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.view.backgroundColor = .white
        navigationController?.navigationBar.barTintColor = .white
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        if navigationController?.viewControllers.count == 1 {
            navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: Strings.General.close, style: .plain, target: self, action: #selector(close))
        }
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
            navigationController?.navigationBar.largeTitleTextAttributes = [
                .foregroundColor: Colors.grayscale050.color,
                .font: Typography.title(.large).font()
            ]
        }
    }
    
    func displayError(message: String) {
        endState(animated: true) {
            DispatchQueue.main.async {
                self.endState(model: StatefulErrorViewModel(
                                image: Resources.Illustrations.iluConstruction.image,
                                content: (
                                    title: Strings.KeyManager.error,
                                    description: message
                                ),
                                button: (image: nil, title: Strings.KeyManager.errorAction))
                )
            }
        }
    }
    
    func showDeleteAlert(keyItem: KeyManagerItem) {
        self.interactor.sendShowDeleteModalEvent(keyManagerItem: keyItem)
        bottomSheet(with: keyItem, action: .delete) { keyManagerItem in
            self.showPopup(with: .delete(keyManagerItem: keyManagerItem))
        }
    }
    
    func showCancelClaimAlert(keyItem: KeyManagerItem) {
        bottomSheet(with: keyItem, action: .cancelClaim) { keyManagerItem in
            self.showPopup(with: .cancelClaim(keyManagerItem: keyManagerItem))
        }
    }
    
    func showCancelRequestPortability(keyItem: KeyManagerItem) {
        bottomSheet(with: keyItem, action: .cancelPortabilityRequest) { keyManagerItem in
            self.showPopup(with: .cancelRequestPortability(keyManagerItem: keyManagerItem))
        }
    }
    
    func configure(popup: KeyManagerModal,
                   with action: PopupAction,
                   and cancelAction: PopupAction? = nil,
                   preferredType: PopupType = .business,
                   enableNotNowButton: Bool = true) -> PopupViewController {
        let popupViewController = PopupViewController(title: popup.title,
                                                      description: popup.description,
                                                      preferredType: preferredType)
        popupViewController.hideCloseButton = true
        let cancelAction = cancelAction ?? PopupAction(title: Strings.KeyManager.Popup.notNow, style: .link)
        
        popupViewController.addAction(action)
        enableNotNowButton ? popupViewController.addAction(cancelAction) : ()
        
        return popupViewController
    }
}
