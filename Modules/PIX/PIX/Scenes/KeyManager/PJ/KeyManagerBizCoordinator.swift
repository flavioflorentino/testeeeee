import UI
import UIKit

protocol KeyManagerBizCoordinating: AnyObject {
    func perform(action: KeyManagerBizOutputAction)
}

final class KeyManagerBizCoordinator {
    private let keyManagerOutput: KeyManagerBizOutput
    
    init(keyManagerOutput: @escaping KeyManagerBizOutput) {
        self.keyManagerOutput = keyManagerOutput
    }
}

// MARK: - KeyManagerCoordinating
extension KeyManagerBizCoordinator: KeyManagerBizCoordinating {
    func perform(action: KeyManagerBizOutputAction) {
        keyManagerOutput(action)
    }
}
