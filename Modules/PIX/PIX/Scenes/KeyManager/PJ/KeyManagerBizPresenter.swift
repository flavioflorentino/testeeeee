import AssetsKit
import AnalyticsModule
import Core
import Foundation
import UI

protocol KeyManagerBizPresenting: AnyObject {
    var viewController: KeyManagerBizDisplay? { get set }
    func openCreateCellFlow(userInfo: KeyManagerBizUserInfo, key: CreateKey, validated: Bool)
    func populateData(with pixKeys: [PixKey], userInfo: KeyManagerBizUserInfo)
    func presentError(errorDescription: String)
    func openInformation()
    func displayCancelClaimSuccess(with pixKey: PixKey)
    func displayCancelRequestPortabilitySuccess(with pixKey: PixKey)
    func prepareToDelete(keyItem: KeyManagerItem)
    func prepareToCancelClaim(keyItem: KeyManagerItem)
    func prepareToCancelRequestPortability(keyItem: KeyManagerItem)
    func startLoading()
    func stopLoading()
    func showPopup(with popUpType: KeyManagerPopUpType)
    func openFeedbackScreen(with feedbackType: FeedbackViewType)
    func dismissPortabilityFeedbackAndShowError(errorDescription: String)
    func updateUserInfoInFlow(with userInfo: KeyManagerBizUserInfo)
}

final class KeyManagerBizPresenter {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let coordinator: KeyManagerBizCoordinating
    weak var viewController: KeyManagerBizDisplay?
    
    var userInfo: KeyManagerBizUserInfo?

    init(coordinator: KeyManagerBizCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - KeyManagerBizPresenting
extension KeyManagerBizPresenter: KeyManagerBizPresenting {
    func updateUserInfoInFlow(with userInfo: KeyManagerBizUserInfo) {
        self.userInfo = userInfo
        coordinator.perform(action: .updateUserInfoInKeyManager(userInfo: userInfo))
    }
    
    func presentError(errorDescription: String) {
        let errorDescription = Strings.KeyManager.errorDescription
        let event = KeyBizTracker(companyName: userInfo?.name ?? "", eventType: .internalError(error: errorDescription))
        dependencies.analytics.log(event)
        viewController?.displayError(message: errorDescription)
    }
    
    func displayCancelClaimSuccess(with pixKey: PixKey) {
        let pixKeyType = pixKey.registeredKeys.first?.keyType ?? .random
        viewController?.showPopup(with: .cancelClaimSuccess(keyType: pixKeyType))
    }

    func displayCancelRequestPortabilitySuccess(with pixKey: PixKey) {
        let pixKeyType = pixKey.registeredKeys.first?.keyType ?? .random
        viewController?.showPopup(with: .cancelRequestPortabilitySuccess(keyType: pixKeyType))
    }

    func prepareToDelete(keyItem: KeyManagerItem) {
        viewController?.showDeleteAlert(keyItem: keyItem)
    }

    func prepareToCancelClaim(keyItem: KeyManagerItem) {
        viewController?.showCancelClaimAlert(keyItem: keyItem)
    }

    func prepareToCancelRequestPortability(keyItem: KeyManagerItem) {
        viewController?.showCancelRequestPortability(keyItem: keyItem)
    }
    
    func openInformation() {
        coordinator.perform(action: .openInformation)
    }
    
    func openCreateCellFlow(userInfo: KeyManagerBizUserInfo, key: CreateKey, validated: Bool) {
        if case .random = key.type {
            coordinator.perform(action: .openRandomCreateKeyFlow(userInfo: userInfo, key: key))
            return
        }
        
        if !validated, key.type == .phone || key.type == .email {
            coordinator.perform(action: .openTFACreateKeyFlow(userInfo: userInfo, key: key))
            return
        }
        
        coordinator.perform(action: .openCreateKeyFlow(userInfo: userInfo, key: key))
    }
    
    func openFeedbackScreen(with feedbackType: FeedbackViewType) {
        coordinator.perform(action: .openFeedback(feedbackType: feedbackType))
    }
    
    func populateData(with pixKeys: [PixKey], userInfo: KeyManagerBizUserInfo) {
        let registeredList = pixKeys.filter { !$0.registeredKeys.isEmpty }
        let notRegisteredList = pixKeys.filter { !$0.unregisteredKeys.isEmpty }
        let keyManagerCells: [KeyManagerItem] = registeredList.map { createKeyManagerItem(with: $0) }.compactMap { $0 }
        let keyManagerPlaceholderCells = createKeyManagerPlaceholderItens(with: notRegisteredList, userInfo: userInfo)
        let firstSectionTitle = registeredList.isEmpty ? "" : Strings.KeyManager.firstSection
        let secondSectionTitle = registeredList.isEmpty ? "" : Strings.KeyManager.secondSection
        let firstSection = KeyManagerSection(title: firstSectionTitle, items: keyManagerCells)
        let secondSection = KeyManagerSection(title: secondSectionTitle, items: keyManagerPlaceholderCells)
        viewController?.displayData(with: [firstSection, secondSection])
        viewController?.changeFooterCount(with: firstSection.items.count)
    }
    
    func dismissPortabilityFeedbackAndShowError(errorDescription: String) {
        coordinator.perform(action: .dismissPortabilityFeedbackAndShowError(errorDescription: errorDescription))
    }
    
    private func createKeyManagerItem(with pixKey: PixKey) -> KeyManagerItem? {
        guard let registeredKey = pixKey.registeredKeys.first else {
            return nil
        }
        var infoText: String {
            if case .processed = registeredKey.statusSlug {
                return ""
            }
            return registeredKey.status
        }
        switch registeredKey.keyType {
        case .cnpj:
            return KeyManagerItem(title: Strings.KeyManager.cnpjCell,
                                  description: formatCNPJ(registeredKey.keyValue),
                                  icon: .postcard,
                                  isFilledBackgroundIcon: true,
                                  infoText: infoText,
                                  section: .registeredKey,
                                  itemId: registeredKey.id)
        case .phone:
            return KeyManagerItem(title: Strings.KeyManager.phoneCell,
                                  description: formatPhone(registeredKey.keyValue),
                                  icon: .mobileAndroid,
                                  isFilledBackgroundIcon: true,
                                  infoText: infoText,
                                  section: .registeredKey,
                                  itemId: registeredKey.id)
        case .email:
            return KeyManagerItem(title: Strings.KeyManager.mailCell,
                                  description: registeredKey.keyValue,
                                  icon: .envelope,
                                  isFilledBackgroundIcon: true,
                                  infoText: infoText,
                                  section: .registeredKey,
                                  itemId: registeredKey.id)
        case .random:
            return KeyManagerItem(title: Strings.KeyManager.randomCell,
                                  description: registeredKey.name,
                                  icon: .cube,
                                  isFilledBackgroundIcon: true,
                                  secondDescription: registeredKey.keyValue,
                                  infoText: infoText,
                                  section: .registeredKey,
                                  itemId: registeredKey.id)
        default:
            return nil
        }
    }
    
    private func createKeyManagerPlaceholderItens(with items: [PixKey], userInfo: KeyManagerBizUserInfo) -> [KeyManagerItem] {
        let unregisteredKeys = items.map { $0.unregisteredKeys.first }.compactMap { $0 }
        return unregisteredKeys.map { createPlaceholderCell(with: $0.keyType, userInfo: userInfo) }.compactMap { $0 }
    }
    
    private func createPlaceholderCell(with type: PixKeyType, userInfo: KeyManagerBizUserInfo) -> KeyManagerItem? {
        switch type {
        case .cnpj:
            return KeyManagerItem(title: Strings.KeyManager.cnpjCell,
                                  description: formatCNPJ(userInfo.cnpj),
                                  icon: .postcard,
                                  isFilledBackgroundIcon: false,
                                  disclosureIndicator: true,
                                  section: .unregisteredKey)
        case .phone:
            return KeyManagerItem(title: Strings.KeyManager.phoneCell,
                                  description: formatPhone(userInfo.phone),
                                  icon: .mobileAndroid,
                                  isFilledBackgroundIcon: false,
                                  disclosureIndicator: true,
                                  section: .unregisteredKey)
        case .email:
            return KeyManagerItem(title: Strings.KeyManager.mailCell,
                                  description: userInfo.mail,
                                  icon: .envelope,
                                  isFilledBackgroundIcon: false,
                                  disclosureIndicator: true,
                                  section: .unregisteredKey)
        case .random:
            return KeyManagerItem(title: Strings.KeyManager.randomCell,
                                  description: Strings.KeyManager.randomCellDescription,
                                  icon: .cube,
                                  isFilledBackgroundIcon: false,
                                  disclosureIndicator: true,
                                  section: .unregisteredKey)
        default:
            return nil
        }
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func didNextStep(action: KeyManagerBizOutputAction) {
        coordinator.perform(action: action)
    }
}

enum KeyManagerPopUpType: Equatable {
    case portability(claimKey: ClaimKey)
    case claim(claimKey: ClaimKey)
    case generic
    case random
    case delete(keyManagerItem: KeyManagerItem)
    case cancelClaim(keyManagerItem: KeyManagerItem)
    case cancelClaimSuccess(keyType: PixKeyType)
    case cancelRequestPortability(keyManagerItem: KeyManagerItem)
    case cancelRequestPortabilitySuccess(keyType: PixKeyType)
}

extension KeyManagerBizPresenter {
    func showPopup(with popUpType: KeyManagerPopUpType) {
        viewController?.showPopup(with: popUpType)
    }
    
    private func formatCNPJ(_ cnpj: String) -> String {
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cnpj)
        return mask.maskedText(from: cnpj) ?? ""
    }
    
    private func formatPhone(_ phone: String) -> String {
        phone.cellphoneFormatting()
    }
}
