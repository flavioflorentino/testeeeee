import SnapKit
import UI
import UIKit

private extension KeyManagerConsumerTableViewFooter.Layout {
    enum Inset {
        static let textView = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
}

public final class KeyManagerConsumerTableViewFooter: UIView {
    fileprivate enum Layout {}
    
    private lazy var informationTextView: UITextView = {
        let textView = UITextView()
        textView.text = Strings.KeyManagerFooter.Consumer.description
        textView.backgroundColor = Colors.backgroundSecondary.color
        textView.cornerRadius = .medium
        textView.isScrollEnabled = false
        textView.textContainerInset = Layout.Inset.textView
        textView.isUserInteractionEnabled = false
        return textView
    }()
    
    public init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension KeyManagerConsumerTableViewFooter: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(informationTextView)
    }
    
    public func setupConstraints() {
        informationTextView.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base02)
        }
    }
}
