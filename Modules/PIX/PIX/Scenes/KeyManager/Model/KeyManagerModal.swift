struct KeyManagerModal: Equatable {
    let image: UIImage?
    let title: String
    let description: String
    let buttonTitle: String
}
