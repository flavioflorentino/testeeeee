import SnapKit
import UI
import UIKit

public final class KeyManagerTableViewHeader: UIView {    
    private lazy var label: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
             .with(\.textColor, Colors.grayscale600.color)
        label.text = Strings.KeyManagerHeader.text
        return label
    }()
    
    public init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension KeyManagerTableViewHeader: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(label)
    }
    
    public func setupConstraints() {
        label.snp.makeConstraints {
            $0.leading.trailing.top.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview()
        }
    }
}
