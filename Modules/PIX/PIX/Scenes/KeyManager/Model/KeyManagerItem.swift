import UI
import UIKit

public struct KeyManagerItem: Hashable, Equatable {
    var title: String
    var description: String
    var icon: Iconography
    var isFilledBackgroundIcon: Bool
    var infoText: String
    var secondDescription: String
    var disclosureIndicator: Bool
    var section: PixKeySection?
    var itemId: String?
    
    public init(title: String,
                description: String,
                icon: Iconography,
                isFilledBackgroundIcon: Bool,
                secondDescription: String = "",
                infoText: String = "",
                disclosureIndicator: Bool = false,
                section: PixKeySection? = nil,
                itemId: String? = nil) {
        self.title = title
        self.description = description
        self.icon = icon
        self.isFilledBackgroundIcon = isFilledBackgroundIcon
        self.infoText = infoText
        self.secondDescription = secondDescription
        self.disclosureIndicator = disclosureIndicator
        self.section = section
        self.itemId = itemId
    }
}

extension KeyManagerItem {
    init(key: PixConsumerKey, userData: PixUserData) {
        if key.type == .registeredKey {
            self.title = key.attributes.keyType.keyTitle
        } else {
            self.title = key.attributes.keyType.infoMessage
        }
        
        if let description = key.attributes.keyValue, !description.isEmpty {
            if let statusSlug = key.attributes.statusSlug, statusSlug != .preRegistered {
                self.description = KeyManagerItem.obfuscateData(for: key.attributes.keyType, text: description)
            } else {
                self.description = description
            }
        } else if key.attributes.keyType == .random, key.type == .unregisteredKey {
            self.description = Strings.KeyManager.Consumer.KeyDescription.random
        } else {
            let description = userData.formatForType(keyType: key.attributes.keyType)
            self.description = description.isEmpty ? "---" : description
        }
        
        self.icon = key.attributes.keyType.icon
        self.isFilledBackgroundIcon = key.type == .registeredKey && key.attributes.statusSlug != .preRegistered
        self.disclosureIndicator = true
        self.secondDescription = ""
        self.section = key.type
        
        if key.type == .registeredKey {
            guard let statusSlug = key.attributes.statusSlug, statusSlug != .processed,
                  let status = key.attributes.status, !status.isEmpty else {
                self.infoText = ""
                return
            }
            
            self.infoText = status
        } else {
            let keyName = key.attributes.keyType.keyName ?? ""
            self.infoText = self.description == "---" ?
                Strings.KeyManager.Consumer.EmptyCell.message(keyName) : ""
        }
    }
    
    private static func obfuscateData(for type: PixConsumerKeyType, text: String) -> String {
        switch type {
        case .cpf:
            return StringObfuscationMasker.mask(word: text, prefix: 3, suffix: 2)
        case .email:
            return StringObfuscationMasker.mask(email: text) ?? text
        case .phone:
            return StringObfuscationMasker.mask(phone: text) ?? text
        default:
            return text
        }
    }
}
