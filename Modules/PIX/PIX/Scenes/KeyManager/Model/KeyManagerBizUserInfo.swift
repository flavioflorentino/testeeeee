public struct KeyManagerBizUserInfo: Equatable {
    var name: String
    var cnpj: String
    var mail: String
    var phone: String
    var id: String
    
    public init(name: String, cnpj: String, mail: String, phone: String, userId: String) {
        self.name = name
        self.cnpj = cnpj
        self.mail = mail
        self.phone = phone
        self.id = userId
    }
}
