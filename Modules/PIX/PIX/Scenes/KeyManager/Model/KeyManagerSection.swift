public struct KeyManagerSection: Hashable, Equatable {
    var title: String
    var items: [KeyManagerItem]
    var description: String?
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(title)
    }
    
    public init(title: String, items: [KeyManagerItem], description: String? = nil) {
        self.title = title
        self.items = items
        self.description = description
    }
    
    public static func == (lhs: KeyManagerSection, rhs: KeyManagerSection) -> Bool {
        lhs.title == rhs.title
    }
}
