import SnapKit
import UI
import UIKit

public final class KeyManagerConsumerTableViewHeader: UIView {
    fileprivate enum Layout {}
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale600.color)
        label.text = Strings.KeyManagerHeader.Consumer.description
        return label
    }()
    
    public init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension KeyManagerConsumerTableViewHeader: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubviews(descriptionLabel)
    }
    
    public func setupConstraints() {
        descriptionLabel.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base03)
        }
    }
}
