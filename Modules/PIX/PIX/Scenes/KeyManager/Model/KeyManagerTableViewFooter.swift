import SnapKit
import UI
import UIKit

private extension KeyManagerTableViewFooter.Layout {
    static let lineHeight: CGFloat = 1
}

public final class KeyManagerTableViewFooter: UIView {
    fileprivate enum Layout {}

    enum Accessibility: String {
        case randomKeyButton
    }
    
    private var didOpenLink: () -> Void
    
    private lazy var button: UIButton = {
        let button = UIButton()
        button.buttonStyle(LinkButtonStyle(size: .small))
              .with(\.textAlignment, .center)
        button.setTitle(Strings.KeyManagerFooter.buttonText, for: .normal)
        button.addTarget(self, action: #selector(didClickLink), for: .touchUpInside)
        button.accessibilityIdentifier = Accessibility.randomKeyButton.rawValue
        return button
    }()
    
    private lazy var keysCountLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
             .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var separatorLine: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale050.color
        return view
    }()
    
    @objc
    private func didClickLink() {
        self.didOpenLink()
    }
    
    public init(didOpenLink: @escaping () -> Void) {
        self.didOpenLink = didOpenLink
        super.init(frame: .zero)
        buildLayout()
    }
    
    public func setItemCount(actualKeysCount: Int, maxKeys: Int) {
        keysCountLabel.text = Strings.KeyManagerFooter.keysCountText(actualKeysCount, maxKeys)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension KeyManagerTableViewFooter: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(button)
        addSubview(separatorLine)
        addSubview(keysCountLabel)
    }
    
    public func setupConstraints() {
        button.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalToSuperview().offset(Spacing.base00)
        }
        
        separatorLine.snp.makeConstraints {
            $0.height.equalTo(Layout.lineHeight)
            $0.top.equalTo(button.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        keysCountLabel.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.height.equalTo(Sizing.base02)
            $0.top.equalTo(separatorLine).offset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base01)
        }
    }
}
