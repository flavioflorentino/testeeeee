import Foundation
import Core

protocol PixBizKeyServicing {
    func createNewPixKey(key: CreateKey, pin: String, completion: @escaping(Result<PixKeyData, ApiError>) -> Void)
    func getKeysInfos(key: String, completion: @escaping(Result<PixKeyData, ApiError>) -> Void)
    func listPixKeys(with userId: String,
                     and type: PixKeysUserType,
                     completion: @escaping (Result<PixKeyDataList, ApiError>) -> Void)
    func claim(claimKey: ClaimKey, completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func confirmClaim(userId: String, keyId: String, pin: String, completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func completeClaim(uuid: String, hash: String, completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func deleteKey(userId: String,
                   keyId: String,
                   userType: PixKeysUserType,
                   pin: String,
                   completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func cancelClaim(withKeyId keyId: String, hash: String?, completion: @escaping(Result<NoContent, ApiError>) -> Void)
}

final class PixBizKeyService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private var userId: Int? {
        PIXSetup.consumerInstance?.consumerId
    }
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - CreateBizKeyServicing
extension PixBizKeyService: PixBizKeyServicing {
    func createNewPixKey(key: CreateKey, pin: String, completion: @escaping(Result<PixKeyData, ApiError>) -> Void) {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        let endpoint = PixBizKeyServiceEndpoint.createKey(key: key, pin: pin)
        Core.Api<PixKeyData>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func getKeysInfos(key: String, completion: @escaping(Result<PixKeyData, ApiError>) -> Void) {
        let endpoint = PixBizKeyServiceEndpoint.getKeysInfos(key: key)
        Core.Api<PixKeyData>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func listPixKeys(with userId: String,
                     and type: PixKeysUserType,
                     completion: @escaping (Result<PixKeyDataList, ApiError>) -> Void) {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        Core.Api<PixKeyDataList>(endpoint: PixBizKeyServiceEndpoint.keys(userId, type))
            .execute(jsonDecoder: decoder) { [weak self] result in
                self?.dependencies.mainQueue.async {
                    completion(result.map(\.model))
                }
            }
    }
    
    func claim(claimKey: ClaimKey, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        Core.Api<NoContent>(endpoint: PixBizKeyServiceEndpoint.claim(claimKey: claimKey))
            .execute(jsonDecoder: decoder) { [weak self] result in
                self?.dependencies.mainQueue.async {
                    completion(result.map(\.model))
                }
            }
    }
    
    func confirmClaim(userId: String, keyId: String, pin: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        Core.Api<NoContent>(endpoint: PixBizKeyServiceEndpoint.confirmClaim(userId: userId, uuid: keyId, pin: pin))
            .execute(jsonDecoder: decoder) { [weak self] result in
                self?.dependencies.mainQueue.async {
                    completion(result.map(\.model))
                }
            }
    }
    
    func completeClaim(uuid: String, hash: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let endpoint = PixBizKeyServiceEndpoint.completeClaim(uuid: uuid, hash: hash)
        Core.Api<NoContent>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }

    func deleteKey(userId: String,
                   keyId: String,
                   userType: PixKeysUserType,
                   pin: String,
                   completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        Api<NoContent>(endpoint: PixBizKeyServiceEndpoint.deleteKey(uuid: keyId, type: userType, pin: pin))
            .execute { [weak self] result in
                self?.dependencies.mainQueue.async {
                    completion(result.map(\.model))
                }
            }
    }

    func cancelClaim(withKeyId keyId: String, hash: String?, completion: @escaping(Result<NoContent, ApiError>) -> Void) {
        Core.Api<NoContent>(endpoint: PixBizKeyServiceEndpoint.cancelClaim(key: keyId, hash: hash))
            .execute { [weak self] result in
                self?.dependencies.mainQueue.async {
                    completion(result.map(\.model))
                }
            }
    }
}
