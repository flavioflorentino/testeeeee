import Foundation
import Core

protocol PixConsumerKeyServicing {
    func createNewPixKey(
        key: CreateKey,
        password: String?,
        completion: @escaping(Result<PixConsumerKeyDataResponse, ApiError>) -> Void
    )
    func listPixKeys(with userId: String, completion: @escaping (Result<PixConsumerKeyListResponse, ApiError>) -> Void)
    func deleteKey(keyId: String, password: String, completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func createClaim(claimKey: ClaimKey, completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func confirmClaim(keyId: String, password: String, completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func completeClaim(keyId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func cancelClaim(keyId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void)
}

final class PixConsumerKeyService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private var userId: Int? {
        PIXSetup.consumerInstance?.consumerId
    }
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - PixConsumerKeyService
extension PixConsumerKeyService: PixConsumerKeyServicing {
    func createNewPixKey(
        key: CreateKey,
        password: String?,
        completion: @escaping(Result<PixConsumerKeyDataResponse, ApiError>) -> Void
    ) {
        let endpoint: PixConsumerKeyServiceEndpoint
        if let password = password {
            endpoint = .createKey(key: key, password: password)
        } else {
            endpoint = .createKeyNonAuthenticated(key: key)
        }
        
        let decoder = JSONDecoder(.convertFromSnakeCase)
        Core.Api<PixConsumerKeyDataResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func listPixKeys(with userId: String, completion: @escaping (Result<PixConsumerKeyListResponse, ApiError>) -> Void) {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        let endpoint = PixConsumerKeyServiceEndpoint.keys(userId)
        Core.Api<PixConsumerKeyListResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func deleteKey(keyId: String, password: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        guard let id = userId else {
            completion(.failure(.cancelled))
            return
        }
        let endpoint = PixConsumerKeyServiceEndpoint.deleteKey(userId: id, uuid: keyId, password: password)
        Api<NoContent>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func createClaim(claimKey: ClaimKey, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let endpoint = PixConsumerKeyServiceEndpoint.createClaim(claimKey: claimKey)
        Api<NoContent>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func confirmClaim(keyId: String, password: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        guard let id = userId else {
            completion(.failure(.cancelled))
            return
        }
        let endpoint = PixConsumerKeyServiceEndpoint.confirmClaim(keyId, userId: id, password: password)
        Api<NoContent>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func completeClaim(keyId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        guard let id = userId else {
            completion(.failure(.cancelled))
            return
        }
        let endpoint = PixConsumerKeyServiceEndpoint.completeClaim(keyId, userId: id)
        Api<NoContent>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func cancelClaim(keyId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        guard let id = userId else {
            completion(.failure(.cancelled))
            return
        }
        let endpoint = PixConsumerKeyServiceEndpoint.cancelClaim(keyId, userId: id)
        Api<NoContent>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
