import AnalyticsModule
import UIKit

protocol FeedbackContainterDelegate: AnyObject {
    func didClose(feedbackType: FeedbackViewType)
}

enum FeedbackContainerAction {
    case close(feedbackType: FeedbackViewType?)
    case showFeedback(type: FeedbackViewType, from: PixUserNavigation)
    case revalidateKey(key: RegisteredKey, delegate: SecurityValidationCodeDelegate)
    case modalDismissed(feedbackType: FeedbackViewType?)
    case back
}

protocol FeedbackContainerCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: FeedbackContainterDelegate? { get set }
    func perform(action: FeedbackContainerAction)
}

final class FeedbackContainerCoordinator {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    weak var viewController: UIViewController?
    weak var delegate: FeedbackContainterDelegate?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - FeedbackContainerCoordinating
extension FeedbackContainerCoordinator: FeedbackContainerCoordinating {
    func perform(action: FeedbackContainerAction) {
        switch action {
        case let .showFeedback(type, origin):
            let feedbackController = FeedbackFactory.make(with: type, delegate: self, from: origin)
            viewController?.navigationController?.pushViewController(feedbackController, animated: true)
            
        case let .revalidateKey(registeredKey, delegate):
            let securityCodeValidationController = SecurityCodeValidationFactory.make(
                inputedValue: registeredKey.keyValue,
                inputType: registeredKey.keyType == .phone ? .phone : .email,
                codeResendDelay: 0,
                delegate: delegate
            )
            viewController?.navigationController?.pushViewController(securityCodeValidationController, animated: true)
            
        case let .close(feedbackType):
            viewController?.dismiss(animated: true)
            
            guard let feedback = feedbackType else { return }
            delegate?.didClose(feedbackType: feedback)
            
        case let .modalDismissed(feedbackType):
            guard let feedback = feedbackType else { return }
            delegate?.didClose(feedbackType: feedback)
            
        case .back:
            viewController?.navigationController?.popToRootViewController(animated: true)
        }
    }
}

extension FeedbackContainerCoordinator: FeedbackDelegate {
    func didAction(action: FeedbackAction, feedbackType: FeedbackViewType) {
        switch (action, feedbackType) {
        case (.tryAgain, .claimNewValidationNotCompleted):
            dependencies.analytics.log(PixNavigationEvent.userNavigated(from: .keyClaimVerificationNotCompleted, to: .keyClaimAvailableNeedsValidation))
            viewController?.navigationController?.popToRootViewController(animated: true)
        default:
            viewController?.dismiss(animated: true)
            delegate?.didClose(feedbackType: feedbackType)
        }
    }
}
