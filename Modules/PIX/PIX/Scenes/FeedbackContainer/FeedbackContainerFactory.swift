import UIKit

enum FeedbackContainerFactory {
    static func make(
        with type: FeedbackContainerType,
        delegate: FeedbackContainterDelegate,
        from origin: PixUserNavigation = .undefined
    ) -> FeedbackContainerViewController {
        let container = DependencyContainer()
        let service: FeedbackContainerServicing = FeedbackContainerService(dependencies: container)
        let coordinator: FeedbackContainerCoordinating = FeedbackContainerCoordinator(dependencies: container)
        let presenter: FeedbackContainerPresenting = FeedbackContainerPresenter(coordinator: coordinator)
        let interactor = FeedbackContainerInteractor(
            service: service,
            presenter: presenter,
            dependencies: container,
            type: type,
            origin: origin
        )
        let viewController = FeedbackContainerViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
