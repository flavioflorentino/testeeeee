import Core
import Foundation

protocol FeedbackContainerServicing {
    var consumerId: Int? { get }
    func getKey(id: String, completion: @escaping (Result<PixKey, ApiError>) -> Void)
    func confirmClaimNotAuthenticated(keyId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func confirmClaim(keyId: String, password: String, completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func completeClaim(keyId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func cancelClaim(keyId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func requestPasswordFromUser(completion: @escaping (Result<String, AuthenticationStatus>) -> Void)
}

final class FeedbackContainerService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - FeedbackContainerServicing
extension FeedbackContainerService: FeedbackContainerServicing {
    var consumerId: Int? {
        PIXSetup.consumerInstance?.consumerId
    }
    
    func getKey(id: String, completion: @escaping (Result<PixKey, ApiError>) -> Void) {
        Api<PixKeyData>(endpoint: PixConsumerKeyServiceEndpoint.key(id))
            .execute { [weak self] result in
                self?.dependencies.mainQueue.async {
                    completion(result.map(\.model).map(\.data))
                }
            }
    }
    
    func confirmClaimNotAuthenticated(keyId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        guard let consumerId = consumerId else {
            completion(.failure(.cancelled))
            return
        }
        
        Api<NoContent>(endpoint: PixConsumerKeyServiceEndpoint.confirmClaimNotAuthenticated(keyId, userId: consumerId))
            .execute { [weak self] result in
                self?.dependencies.mainQueue.async {
                    completion(result.map(\.model))
                }
            }
    }
    
    func confirmClaim(keyId: String, password: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        guard let consumerId = consumerId else {
            completion(.failure(.cancelled))
            return
        }
        
        Api<NoContent>(endpoint: PixConsumerKeyServiceEndpoint.confirmClaim(keyId, userId: consumerId, password: password))
            .execute { [weak self] result in
                self?.dependencies.mainQueue.async {
                    completion(result.map(\.model))
                }
            }
    }
    
    func completeClaim(keyId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        guard let id = consumerId else {
            completion(.failure(.cancelled))
            return
        }
        
        let endpoint = PixConsumerKeyServiceEndpoint.completeClaim(keyId, userId: id)
        Api<NoContent>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func cancelClaim(keyId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        guard let id = consumerId else {
            completion(.failure(.cancelled))
            return
        }
        
        let endpoint = PixConsumerKeyServiceEndpoint.cancelClaim(keyId, userId: id)
        Api<NoContent>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func requestPasswordFromUser(completion: @escaping (Result<String, AuthenticationStatus>) -> Void) {
        PIXSetup.authInstance?.authenticate { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let password):
                    completion(.success(password))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}
