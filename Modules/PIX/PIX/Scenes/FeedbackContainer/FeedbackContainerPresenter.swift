import Foundation

protocol FeedbackContainerPresenting: AnyObject {
    var viewController: FeedbackContainerDisplaying? { get set }
    func presentFeedback(type: FeedbackViewType, from origin: PixUserNavigation)
    func didNextStep(action: FeedbackContainerAction)
    func presentLoading()
    func hideLoading()
    func disableCloseButton()
    func enableCloseButton()
    func presentError(title: String, message: String, buttonText: String)
}

final class FeedbackContainerPresenter {
    private let coordinator: FeedbackContainerCoordinating
    weak var viewController: FeedbackContainerDisplaying?

    init(coordinator: FeedbackContainerCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - FeedbackContainerPresenting
extension FeedbackContainerPresenter: FeedbackContainerPresenting {
    func presentFeedback(type: FeedbackViewType, from origin: PixUserNavigation) {
        viewController?.displayFeedbackView(type: type, from: origin)
    }
    
    func presentLoading() {
        viewController?.displayLoaderView()
    }
    
    func hideLoading() {
        viewController?.hideLoaderView()
    }
    
    func disableCloseButton() {
        viewController?.disableCloseButton()
    }
    
    func enableCloseButton() {
        viewController?.enableCloseButton()
    }
    
    func didNextStep(action: FeedbackContainerAction) {
        coordinator.perform(action: action)
    }
    
    func presentError(title: String, message: String, buttonText: String) {
        viewController?.displayError(
            title: title,
            message: message,
            buttonText: buttonText
        )
    }
}
