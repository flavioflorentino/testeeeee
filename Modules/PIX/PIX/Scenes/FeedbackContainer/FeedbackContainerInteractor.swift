import AnalyticsModule
import Core
import Foundation

protocol FeedbackContainerInteracting: AnyObject {
    func configureContainer()
    func didClose(feedbackType: FeedbackViewType?)
    func execute(action: FeedbackAction, feedbackType: FeedbackViewType)
}

enum FeedbackContainerType: Equatable {
    case portabilityDonorReceived(uuid: String) // Pedido de portabilidade recebido
    case claimDonorReceived(uuid: String) // Reivindicação de posse recebida
    case claimNewValidationKeyAvailable(uuid: String) // Chave Pix disponível
}

final class FeedbackContainerInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: FeedbackContainerServicing
    private let presenter: FeedbackContainerPresenting
    private let type: FeedbackContainerType
    private let feedbackContainerOrigin: PixUserNavigation
    private var keyId: String = ""
    private var currentKey: RegisteredKey?

    init(
        service: FeedbackContainerServicing,
        presenter: FeedbackContainerPresenting,
        dependencies: Dependencies,
        type: FeedbackContainerType,
        origin: PixUserNavigation
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.type = type
        feedbackContainerOrigin = origin
        self.keyId = getKeyId()
    }
    
    private func getKeyId() -> String {
        switch type {
        case .claimDonorReceived(let keyId), .portabilityDonorReceived(let keyId), .claimNewValidationKeyAvailable(let keyId):
            return keyId
        }
    }
    
    private func authenticateUser(complete: @escaping (String) -> Void) {
        service.requestPasswordFromUser { [weak self] result in
            switch result {
            case .success(let password):
                complete(password)
            case .failure(let error):
                guard error == .nilPassword else { return }
                self?.presenter.presentError(
                    title: Strings.KeyManager.AuthError.title,
                    message: Strings.KeyManager.AuthError.message,
                    buttonText: Strings.General.gotIt
                )
            }
        }
    }
    
    private func verifyUserPasswordAndPortability(keyId: String, from origin: PixUserNavigation) {
        dependencies.analytics.log(PixNavigationEvent.userNavigated(from: origin, to: .keyAuthenticationPortability))
        
        authenticateUser { [weak self] password in
            self?.verifyPortability(keyId: keyId, password: password, from: .keyAuthenticationPortability)
        }
    }
    
    private func verifyPortability(keyId: String, password: String, from origin: PixUserNavigation) {
        presenter.disableCloseButton()
        presenter.presentLoading()
        
        service.getKey(id: keyId) { [weak self] result in
            switch result {
            case .success(let key):
                self?.verifyPortabilityKeyStatus(key: key, password: password, from: origin)
            case .failure:
                self?.presenter.didNextStep(action: .showFeedback(type: .portabilityDonorRefused(uuid: keyId), from: origin))
            }
        }
    }
    
    private func verifyPortabilityKeyStatus(key: PixKey, password: String, from origin: PixUserNavigation) {
        guard let validKey = key.registeredKeys.first else {
            presenter.didNextStep(action: .showFeedback(type: .portabilityDonorRefused(uuid: keyId), from: origin))
            return
        }
        
        switch validKey.statusSlug {
        case .awaitingPortabilityConfirm:
            executeGrantPortability(keyId: validKey.id, password: password, from: origin)
        case .awaitingPortabilityComplete:
            presenter.didNextStep(action: .showFeedback(type: .portabilityDonorCompleted(uuid: keyId), from: origin))
        default:
            presenter.didNextStep(action: .showFeedback(type: .portabilityDonorRefused(uuid: keyId), from: origin))
        }
    }
    
    private func executeGrantPortability(keyId: String, password: String, from origin: PixUserNavigation) {
        service.confirmClaim(keyId: keyId, password: password) { [weak self] result in
            switch result {
            case .success:
                self?.presenter.didNextStep(action: .showFeedback(type: .portabilityDonorInProgress(uuid: keyId), from: origin))
            case .failure(let error):
                self?.parseGrantPortabilityError(error: error, uuid: keyId, origin: origin)
            }
        }
    }
    
    private func parseGrantPortabilityError(error: ApiError, uuid: String, origin: PixUserNavigation) {
        switch error {
        case .unauthorized:
            presenter.presentError(
                title: Strings.KeyManager.AuthError.title,
                message: Strings.KeyManager.AuthError.message,
                buttonText: Strings.General.gotIt
            )
        default:
            presenter.didNextStep(action: .showFeedback(type: .portabilityDonorRefused(uuid: keyId), from: origin))
        }
    }
    
    private func verifyClaim(keyId: String, from origin: PixUserNavigation) {
        presenter.disableCloseButton()
        presenter.presentLoading()
        
        service.getKey(id: keyId) { [weak self] result in
            self?.hideLoadingAndEnableCloseButton()
            
            switch result {
            case .success(let key):
                self?.verifyClaimKeyStatus(key: key, from: origin)
            case .failure:
                self?.presenter.presentFeedback(type: .claimDonorRequested(uuid: keyId), from: origin)
            }
        }
    }
    
    private func verifyClaimKeyStatus(key: PixKey, from origin: PixUserNavigation) {
        guard let validKey = key.registeredKeys.first else {
            presenter.presentFeedback(type: .claimDonorRequested(uuid: keyId), from: origin)
            return
        }
        
        switch validKey.statusSlug {
        case .awaitingClaimConfirm, .inactiveButCanRevalidate:
            currentKey = validKey
            presenter.presentFeedback(type: .claimDonorReceived(uuid: validKey.id), from: origin)
        default:
            presenter.presentFeedback(type: .claimDonorRequested(uuid: validKey.id), from: origin)
        }
    }
    
    private func verifyClaimNewValidation(keyId: String, from origin: PixUserNavigation) {
        presenter.disableCloseButton()
        presenter.presentLoading()
        
        service.getKey(id: keyId) { [weak self] result in
            self?.hideLoadingAndEnableCloseButton()
            
            switch result {
            case .success(let key):
                self?.verifyClaimNewValidationKeyStatus(key: key, from: origin)
            case .failure:
                self?.presenter.presentError(
                    title: Strings.Feedback.AlertError.title,
                    message: Strings.Feedback.AlertError.message,
                    buttonText: Strings.Feedback.AlertError.buttonText
                )
            }
        }
    }
    
    private func verifyClaimNewValidationKeyStatus(key: PixKey, from origin: PixUserNavigation) {
        guard let validKey = key.registeredKeys.first else {
            presenter.presentFeedback(type: .claimDonorRequested(uuid: keyId), from: origin)
            return
        }
        
        switch validKey.statusSlug {
        case .claimNeedsValidation:
            currentKey = validKey
            presenter.presentFeedback(type: .claimNewValidationKeyAvailable(uuid: validKey.id), from: origin)
        default:
            presenter.presentFeedback(type: .claimNewValidationDeadlineFinished, from: origin)
        }
    }
    
    private func executeAllowClaim(keyId: String, from origin: PixUserNavigation) {
        presenter.disableCloseButton()
        presenter.presentLoading()
        
        service.confirmClaimNotAuthenticated(keyId: keyId) { [weak self] result in
            switch result {
            case .success:
                self?.presenter.didNextStep(action: .showFeedback(type: .claimDonorInProgress(uuid: keyId), from: origin))
            case .failure:
                self?.presenter.didNextStep(action: .showFeedback(type: .claimDonorNotCompleted(uuid: keyId), from: origin))
            }
        }
    }
    
    private func executeClaimCancel(keyId: String, from origin: PixUserNavigation) {
        presenter.disableCloseButton()
        presenter.presentLoading()
        
        service.cancelClaim(keyId: keyId) { [weak self] result in
            switch result {
            case .success:
                self?.presenter.presentFeedback(type: .claimDonorRevalidationInProgress(uuid: keyId), from: origin)
            case .failure:
                self?.presenter.didNextStep(
                    action: .showFeedback(type: .claimDonorRevalidationNotCompleted(uuid: keyId), from: origin)
                )
            }
        }
    }
    
    private func executeClaimComplete(keyId: String, from origin: PixUserNavigation) {
        presenter.disableCloseButton()
        presenter.presentLoading()
        
        service.completeClaim(keyId: keyId) { [weak self] result in
            switch result {
            case .success:
                self?.presenter.presentFeedback(type: .claimNewValidationCompleted, from: origin)
            case .failure:
                self?.presenter.didNextStep(
                    action: .showFeedback(type: .claimNewValidationNotCompleted, from: origin)
                )
            }
            
            self?.hideLoadingAndEnableCloseButton()
        }
    }
    
    private func hideLoadingAndEnableCloseButton() {
        presenter.enableCloseButton()
        presenter.hideLoading()
    }
    
    private func trackNavitationToKeyRevalidation(key: RegisteredKey, from origin: PixUserNavigation) {
        let destination = PixUserNavigation(validationOf: key.keyType)
        guard destination != .undefined else { return }
        let event = PixNavigationEvent.userNavigated(from: origin, to: destination)
        dependencies.analytics.log(event)
    }
}

// MARK: - FeedbackContainerInteracting
extension FeedbackContainerInteractor: FeedbackContainerInteracting {
    func configureContainer() {
        switch type {
        case .portabilityDonorReceived(let uuid):
            presenter.presentFeedback(type: .portabilityDonorReceived(uuid: uuid), from: feedbackContainerOrigin)
        case .claimDonorReceived(let keyId):
            verifyClaim(keyId: keyId, from: feedbackContainerOrigin)
        case .claimNewValidationKeyAvailable(let keyId):
            verifyClaimNewValidation(keyId: keyId, from: feedbackContainerOrigin)
        }
    }
    
    func execute(action: FeedbackAction, feedbackType: FeedbackViewType) {
        let feedback = PixUserNavigation(feedbackType)
        
        switch action {
        case .grantPortability:
            verifyUserPasswordAndPortability(keyId: keyId, from: feedback)
        case .allowClaim:
            executeAllowClaim(keyId: keyId, from: feedback)
        case .revalidateKey, .validateKey:
            guard let key = currentKey else { return }
            trackNavitationToKeyRevalidation(key: key, from: feedback)
            presenter.didNextStep(action: .revalidateKey(key: key, delegate: self))
        default:
            didClose(feedbackType: feedbackType)
        }
    }
    
    func didClose(feedbackType: FeedbackViewType?) {
        presenter.didNextStep(action: .close(feedbackType: feedbackType))
    }
}

// MARK: - SecurityValidationCodeDelegate
extension FeedbackContainerInteractor: SecurityValidationCodeDelegate {
    func didValidateKeyWithSuccess(keyValue: String, keyType: PixKeyType) {
        switch type {
        case .claimDonorReceived(let keyId):
            executeClaimCancel(keyId: keyId, from: PixUserNavigation(validationOf: keyType))
        case .claimNewValidationKeyAvailable(let keyId):
            executeClaimComplete(keyId: keyId, from: PixUserNavigation(validationOf: keyType))
        default:
            break
        }
    }
}
