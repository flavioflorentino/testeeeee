import UI
import UIKit
import SnapKit

protocol FeedbackContainerDisplaying: AnyObject {
    func displayLoaderView()
    func hideLoaderView()
    func disableCloseButton()
    func enableCloseButton()
    func displayFeedbackView(type: FeedbackViewType, from origin: PixUserNavigation)
    func displayError(title: String, message: String, buttonText: String)
}

final class FeedbackContainerViewController: ViewController<FeedbackContainerInteracting, UIView>, LoadingViewProtocol {
    let loadingView = LoadingView()
    private var feedbackTypePresented: FeedbackViewType?
    
    private lazy var closeButton = UIBarButtonItem(
        title: Strings.General.close,
        style: .plain,
        target: self,
        action: #selector(didTapCloseButton)
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.configureContainer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }

    override func buildViewHierarchy() {
        navigationItem.leftBarButtonItem = closeButton
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        navigationController?.presentationController?.delegate = self
    }
    
    private func setupNavigationBar() {
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
    }
    
    @objc
    private func didTapCloseButton() {
        interactor.didClose(feedbackType: feedbackTypePresented)
    }
    
    private func configureChildController(_ child: UIViewController) {
        removeLastChildViewControllerIfNeeded()
        
        addChild(child)
        view.addSubview(child.view)
        child.didMove(toParent: self)
        
        child.view.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    private func removeLastChildViewControllerIfNeeded() {
        guard let lastChild = children.first else {
            return
        }
        
        lastChild.willMove(toParent: nil)
        lastChild.removeFromParent()
        lastChild.view.removeFromSuperview()
    }
}

// MARK: - FeedbackContainerDisplaying
extension FeedbackContainerViewController: FeedbackContainerDisplaying {
    func displayLoaderView() {
        startLoadingView()
    }
    
    func hideLoaderView() {
        stopLoadingView()
    }
    
    func disableCloseButton() {
        closeButton.isEnabled = false
    }
    
    func enableCloseButton() {
        closeButton.isEnabled = true
    }
    
    func displayFeedbackView(type: FeedbackViewType, from origin: PixUserNavigation) {
        let feedbackViewController = FeedbackFactory.make(with: type, delegate: self, from: origin)
        configureChildController(feedbackViewController)
        feedbackTypePresented = type
    }
    
    func displayError(title: String, message: String, buttonText: String) {
        let popup = PopupViewController(title: title, description: message, preferredType: .text)
        let action = PopupAction(title: buttonText, style: .fill) { [weak self] in
            self?.didTapCloseButton()
        }
        popup.addAction(action)
        showPopup(popup)
    }
}

// MARK: - FeedbackDelegate
extension FeedbackContainerViewController: FeedbackDelegate {
    func didAction(action: FeedbackAction, feedbackType: FeedbackViewType) {
        interactor.execute(action: action, feedbackType: feedbackType)
    }
}

// MARK: - UIAdaptivePresentationControllerDelegate
extension FeedbackContainerViewController: UIAdaptivePresentationControllerDelegate {
    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        guard let feedback = feedbackTypePresented else { return }
        interactor.execute(action: .modalDismissed, feedbackType: feedback)
    }
}
