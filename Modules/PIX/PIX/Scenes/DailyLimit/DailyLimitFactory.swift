import FeatureFlag
import UIKit

public enum DailyLimitFactory {
    public static func makePJ() -> DailyLimitViewController {
        DailyLimitFactory.make(faqFlag: .pixDailyLimitFaqUrlPj)
    }
    
    public static func makePF() -> DailyLimitViewController {
        DailyLimitFactory.make(faqFlag: .pixDailyLimitFaqUrlPf)
    }
    
    private static func make(faqFlag: FeatureConfig) -> DailyLimitViewController {
        let container = DependencyContainer()
        let service: DailyLimitServicing = DailyLimitService(dependencies: container)
        let coordinator: DailyLimitCoordinating = DailyLimitCoordinator(dependencies: container)
        let presenter: DailyLimitPresenting = DailyLimitPresenter(coordinator: coordinator, dependencies: container)
        let interactor = DailyLimitInteractor(service: service, presenter: presenter, dependencies: container, faqFlag: faqFlag)
        let viewController = DailyLimitViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
