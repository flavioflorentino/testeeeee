import AssetsKit
import Foundation
import UI

protocol DailyLimitPresenting: AnyObject {
    var viewController: DailyLimitDisplaying? { get set }
    func didNextStep(action: DailyLimitAction)
    func presentSkeletonLoader()
    func hideSkeletonLoader()
    func presentLimitData(dailyLimit: PixDailyLimit, minValue: Double)
    func presentGenericError()
    func presentNewDailyLimit(value: Double)
    func presentSlider(value: Float)
    func presentUnavailableLimitAmountError()
    func hideUnavailableLimitAmountError()
    func presentUpdateButtonEnabled()
    func presentUpdateButtonDisabled()
    func presentUpdateLimitConfirmation()
    func presentUpdateLimitSuccess()
    func presentAlertGenericError()
    func presentUpdateLimitLoader()
    func hideUpdateLimitLoader()
    func presentLoading()
    func hideLoading()
    func presentExistingTicketAlertWith(id: String)
    func presentTicketChatWith(id: String)
}

final class DailyLimitPresenter {
    typealias Dependencies = HasNoDependency
    
    private let dependencies: Dependencies
    private let coordinator: DailyLimitCoordinating
    weak var viewController: DailyLimitDisplaying?

    init(coordinator: DailyLimitCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - DailyLimitPresenting
extension DailyLimitPresenter: DailyLimitPresenting {
    func didNextStep(action: DailyLimitAction) {
        coordinator.perform(action: action)
    }
    
    func presentSkeletonLoader() {
        viewController?.displaySkeletonLoader()
    }
    
    func hideSkeletonLoader() {
        viewController?.hideSkeletonLoader()
    }
    
    func presentLimitData(dailyLimit: PixDailyLimit, minValue: Double) {
        guard
            let minStringValue = minValue.convertToCurrency(hasCurrencySymbol: false, decimalDigits: 0),
            let maxStringValue = dailyLimit.max.convertToCurrency(hasCurrencySymbol: false, decimalDigits: 0),
            let maxAmount = dailyLimit.max.convertToCurrency(hasCurrencySymbol: true, decimalDigits: 0),
            let currentAmount = dailyLimit.current.convertToCurrency(hasCurrencySymbol: true, decimalDigits: 0),
            let usedTodayAmount = dailyLimit.usedToday.convertToCurrency(hasCurrencySymbol: true)
        else {
            return
        }
        let increaseMaxDailyLimit = NSAttributedString(
            string: Strings.DailyLimit.increaseMaxDailyLimit(maxAmount),
            attributes: [
                .foregroundColor: Colors.branding600.color,
                .font: Typography.bodySecondary().font(),
                .underlineStyle: NSUnderlineStyle.thick.rawValue
            ]
        )
        viewController?.displayMinimumLimit(value: Float(minValue), stringValue: minStringValue)
        viewController?.displayMaximumLimit(value: Float(dailyLimit.max), stringValue: maxStringValue)
        viewController?.displayCurrentDailyLimit(value: Float(dailyLimit.current), amount: currentAmount)
        viewController?.displayUsedDailyLimit(amount: usedTodayAmount)
        viewController?.displayIncreaseMaxLimit(text: increaseMaxDailyLimit)
    }
    
    func presentGenericError() {
        let image = Resources.Illustrations.iluFalling.image
        let title = Strings.General.somethingWentWrong
        let description = Strings.DailyLimit.errorDescription
        let buttonTitle = Strings.General.gotIt
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        let attributedDescription = NSAttributedString(
            string: description,
            attributes: [
                .foregroundColor: Colors.grayscale600.color,
                .font: Typography.bodyPrimary().font(),
                .paragraphStyle: paragraph
            ]
        )
        viewController?.displayFullscreenError(image: image, title: title, description: attributedDescription, buttonTitle: buttonTitle)
    }
    
    func presentNewDailyLimit(value: Double) {
        guard let amount = value.convertToCurrency(hasCurrencySymbol: true, decimalDigits: 0) else {
            return
        }
        viewController?.displayNewLimit(amount: amount)
    }
    
    func presentSlider(value: Float) {
        viewController?.displaySlider(value: value)
    }
    
    func presentUnavailableLimitAmountError() {
        viewController?.displayUnavailableLimitAmountError()
    }
    
    func hideUnavailableLimitAmountError() {
        viewController?.hideUnavailableLimitAmountError()
    }
    
    func presentUpdateButtonEnabled() {
        viewController?.displayUpdateButtonEnabled()
    }
    
    func presentUpdateButtonDisabled() {
        viewController?.displayUpdateButtonDisabled()
    }
    
    func presentUpdateLimitConfirmation() {
        let title = Strings.DailyLimit.zeroedDailyLimitTitle
        let subtitle = Strings.DailyLimit.zeroedDailyLimitDescription
        let attributedSubtitle = subtitle.attributedStringWith(
            normalFont: Typography.bodyPrimary().font(),
            highlightFont: Typography.bodyPrimary(.highlight).font(),
            normalColor: Colors.grayscale600.color,
            highlightColor: Colors.grayscale600.color,
            textAlignment: .center,
            underline: false
        )
        let primaryButtonTitle = Strings.DailyLimit.maintainZeroedDailyLimit
        let secondaryButtonTitle = Strings.DailyLimit.ajustValue
        viewController?.displayAlert(
            type: .sticker(style: .info),
            title: title,
            subtitle: attributedSubtitle,
            primaryButtonTitle: primaryButtonTitle,
            secondaryButtonTitle: secondaryButtonTitle
        )
    }
    
    func presentUpdateLimitSuccess() {
        let title = Strings.DailyLimit.succesfulUpdate
        viewController?.displaySnackbar(text: title, iconType: .success, emphasis: .high)
    }
    
    func presentAlertGenericError() {
        let title = Strings.General.somethingWentWrong
        let subtitle = Strings.DailyLimit.errorDescription
        let attributedSubtitle = NSAttributedString(
            string: subtitle,
            attributes: [
                .foregroundColor: Colors.grayscale600.color,
                .font: Typography.bodyPrimary().font()
            ]
        )
        let primaryButtonTitle = Strings.General.tryAgain
        let secondaryButtonTitle = Strings.General.notNow
        viewController?.displayAlert(
            type: .sticker(style: .error),
            title: title,
            subtitle: attributedSubtitle,
            primaryButtonTitle: primaryButtonTitle,
            secondaryButtonTitle: secondaryButtonTitle
        )
    }
    
    func presentUpdateLimitLoader() {
        viewController?.displayUpdateLimitLoader()
    }
    
    func hideUpdateLimitLoader() {
        viewController?.hideUpdateLimitLoader()
    }

    func presentLoading() {
        viewController?.displayLoading()
    }

    func hideLoading() {
        viewController?.hideLoading()
    }

    func presentExistingTicketAlertWith(id: String) {
        viewController?.displayExistingTicketAlertWith(id: id)
    }

    func presentTicketChatWith(id: String) {
        coordinator.perform(action: .ticketChat(id: id))
    }
}
