import Core
import CustomerSupport
import FeatureFlag
import Foundation

public protocol DailyLimitInteracting: AnyObject {
    func getLimitData()
    func changeDailyLimit(value: Float)
    func changeDailyLimit(amount: String?)
    func close()
    func updateDailyLimit()
    func requestUpdateDailyLimit()
    func showFAQ()
    func showMaximumDailyLimit()
    func openTicketChatWith(id: String)
}

final class DailyLimitInteractor {
    typealias Dependencies = HasPFAuth & HasBizAuth & HasZendesk & HasMainQueue & HasFeatureManager
    
    private let dependencies: Dependencies
    private let service: DailyLimitServicing
    private let presenter: DailyLimitPresenting
    private let minValue: Double = .zero
    private var initialDailyLimit: Double = .zero
    private var dailyLimit: Double = .zero
    private var maxDailyLimit: Double = .zero
    private let maxAllowedValue: Double = 10_000_000
    private let roundValue: Double = 100.0
    private let faqFlag: FeatureConfig

    init(service: DailyLimitServicing, presenter: DailyLimitPresenting, dependencies: Dependencies, faqFlag: FeatureConfig) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.faqFlag = faqFlag
    }
}

// MARK: - DailyLimitInteracting
extension DailyLimitInteractor: DailyLimitInteracting {
    func getLimitData() {
        presenter.presentSkeletonLoader()
        service.getLimit { [weak self] result in
            guard let self = self else {
                return
            }
            self.presenter.hideSkeletonLoader()
            switch result {
            case let .success(dailyLimit):
                self.initialDailyLimit = dailyLimit.current
                self.dailyLimit = dailyLimit.current
                self.maxDailyLimit = dailyLimit.max
                self.presenter.presentLimitData(dailyLimit: dailyLimit, minValue: self.minValue)
            case .failure:
                self.presenter.presentGenericError()
            }
        }
    }
    
    func changeDailyLimit(value: Float) {
        let doubleValue = Double(value)
        let roundedValue = doubleValue - (doubleValue).truncatingRemainder(dividingBy: roundValue)
        handleNewDailyLimit(roundedValue)
    }
    
    func changeDailyLimit(amount: String?) {
        let amount = amount ?? ""
        let textNumber = String(amount.unicodeScalars.filter(CharacterSet.decimalDigits.contains))
        let value = Double(textNumber) ?? .zero
        guard value < maxAllowedValue else {
            presenter.presentNewDailyLimit(value: dailyLimit)
            return
        }
        handleNewDailyLimit(value)
        presenter.presentSlider(value: Float(value))
    }
    
    private func handleNewDailyLimit(_ value: Double) {
        if value > maxDailyLimit {
            presenter.presentUnavailableLimitAmountError()
            presenter.presentUpdateButtonDisabled()
        } else if value == initialDailyLimit {
            presenter.hideUnavailableLimitAmountError()
            presenter.presentUpdateButtonDisabled()
        } else {
            presenter.hideUnavailableLimitAmountError()
            presenter.presentUpdateButtonEnabled()
        }
        dailyLimit = value
        presenter.presentNewDailyLimit(value: value)
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }

    func updateDailyLimit() {
        guard dailyLimit > .zero else {
            presenter.presentUpdateLimitConfirmation()
            return
        }
        requestUpdateDailyLimit()
    }
    
    func requestUpdateDailyLimit() {
        if let auth = dependencies.authContract {
            auth.authenticate { [weak self] result in
                switch result {
                case let .success(password):
                    self?.contactServiceToUpdateDailyLimit(password: password)
                default:
                    break
                }
            }
        } else {
            dependencies.businessAuthContract?.performActionWithAuthorization(nil) { [weak self] result in
                switch result {
                case let .success(password):
                    self?.contactServiceToUpdateDailyLimit(password: password)
                default:
                    break
                }
            }
        }
    }
    
    private func contactServiceToUpdateDailyLimit(password: String) {
        let amount = String(format: "%.2f", dailyLimit)
        presenter.presentUpdateLimitLoader()
        service.updateLimit(newAmount: amount, password: password) { [weak self] result in
            guard let self = self else { return }
            self.presenter.hideUpdateLimitLoader()
            switch result {
            case .success:
                self.initialDailyLimit = self.dailyLimit
                self.presenter.presentUpdateLimitSuccess()
                self.presenter.presentUpdateButtonDisabled()
            case .failure:
                self.presenter.presentAlertGenericError()
            }
        }
    }
    
    func showFAQ() {
        let stringUrl = dependencies.featureManager.text(faqFlag)
        guard let url = URL(string: stringUrl) else {
            return
        }
        presenter.didNextStep(action: .faq(link: url))
    }

    func showMaximumDailyLimit() {
        presenter.presentLoading()
        dependencies.zendeskContainer?.isAnyTicketOpen([Strings.DailyLimit.pixLimit]) { [weak self] id in
            self?.dependencies.mainQueue.async {
                self?.presenter.hideLoading()
                if let id = id {
                    self?.presenter.presentExistingTicketAlertWith(id: id)
                } else {
                    guard let maxDailyLimit = self?.maxDailyLimit else { return }
                    self?.presenter.didNextStep(action: .maximumDailyLimit(value: maxDailyLimit))
                }
            }
        }
    }

    func openTicketChatWith(id: String) {
        self.presenter.presentTicketChatWith(id: id)
    }
}
