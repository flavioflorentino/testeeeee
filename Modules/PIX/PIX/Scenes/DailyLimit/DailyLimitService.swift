import Core
import Foundation

protocol DailyLimitServicing {
    func getLimit(completion: @escaping (Result<PixDailyLimit, ApiError>) -> Void)
    func updateLimit(newAmount: String, password: String, completion: @escaping (Result<NoContent, ApiError>) -> Void)
}

final class DailyLimitService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - DailyLimitServicing
extension DailyLimitService: DailyLimitServicing {
    func getLimit(completion: @escaping (Result<PixDailyLimit, ApiError>) -> Void) {
        let endpoint = PixDailyLimitEndpoint.getLimit
        Api<PixDailyLimit>(endpoint: endpoint).execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                completion(mappedResult)
            }
        }
    }
    
    func updateLimit(newAmount: String, password: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let endpoint = PixDailyLimitEndpoint.updateLimit(amount: newAmount, password: password)
        Api<NoContent>(endpoint: endpoint).execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                completion(mappedResult)
            }
        }
    }
}
