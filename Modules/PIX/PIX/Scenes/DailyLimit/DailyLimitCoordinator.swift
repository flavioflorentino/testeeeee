import CustomerSupport
import UIKit

enum DailyLimitAction {
    case faq(link: URL)
    case close
    case maximumDailyLimit(value: Double)
    case ticketChat(id: String)
}

protocol DailyLimitCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: DailyLimitAction)
}

final class DailyLimitCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - DailyLimitCoordinating
extension DailyLimitCoordinator: DailyLimitCoordinating {
    func perform(action: DailyLimitAction) {
        switch action {
        case let .faq(link):
            guard let controller = viewController else {
                return
            }
            HelpcenterDeeplinkResolver().open(url: link, isAuthenticated: true, from: controller)
        case .close:
            viewController?.navigationController?.dismiss(animated: true)
        case let .maximumDailyLimit(value):
            let controller = MaximumDailyLimitFactory.make(currentMaxDailyLimit: value)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case let .ticketChat(id):
            let controller = TicketListFactory.make(ticketId: id)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
