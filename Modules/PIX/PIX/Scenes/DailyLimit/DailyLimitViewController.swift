import AssetsKit
import SkeletonView
import SnapKit
import UI
import UIKit

public protocol DailyLimitDisplaying: AnyObject {
    func displaySkeletonLoader()
    func hideSkeletonLoader()
    func displayFullscreenError(image: UIImage?, title: String, description: NSAttributedString, buttonTitle: String)
    func displayMinimumLimit(value: Float, stringValue: String)
    func displayMaximumLimit(value: Float, stringValue: String)
    func displayCurrentDailyLimit(value: Float, amount: String)
    func displayUsedDailyLimit(amount: String)
    func displayIncreaseMaxLimit(text: NSAttributedString)
    func displayNewLimit(amount: String)
    func displaySlider(value: Float)
    func displayUnavailableLimitAmountError()
    func hideUnavailableLimitAmountError()
    func displayUpdateButtonEnabled()
    func displayUpdateButtonDisabled()
    func displaySnackbar(text: String, iconType: ApolloFeedbackCardViewIconType, emphasis: ApolloSnackbarEmphasisStyle)
    func displayAlert(type: ApolloAlertType, title: String, subtitle: NSAttributedString, primaryButtonTitle: String, secondaryButtonTitle: String)
    func displayUpdateLimitLoader()
    func displayLoading()
    func hideLoading()
    func hideUpdateLimitLoader()
    func displayExistingTicketAlertWith(id: String)
}

public final class DailyLimitViewController: ViewController<DailyLimitInteracting, UIView> {
    // MARK: - Visual Components
    private let contentScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = .clear
        scrollView.showsHorizontalScrollIndicator = false
        return scrollView
    }()
    
    private let contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.typography = Typography.bodyPrimary()
        label.textColor = Colors.grayscale600.color
        label.text = Strings.DailyLimit.description
        label.numberOfLines = 0
        label.isSkeletonable = true
        label.layer.masksToBounds = true
        return label
    }()
    
    private let cardView: UIView = {
        let view = UIView()
        view.layer.borderWidth = Border.light
        view.layer.cornerRadius = CornerRadius.medium
        view.layer.borderColor = Colors.grayscale100.color.cgColor
        view.isSkeletonable = true
        view.layer.masksToBounds = true
        return view
    }()
    
    private let cardContentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private let dailyLimitTitleLabel: UILabel = {
        let label = UILabel()
        label.typography = Typography.bodySecondary()
        label.textColor = Colors.grayscale700.color
        label.text = Strings.DailyLimit.dailyLimit
        label.textAlignment = .center
        return label
    }()
    
    private lazy var amountTextField: UITextField = {
        let textField = UITextField()
        textField.textColor = Colors.branding600.color
        textField.font = Typography.title(.xLarge).font()
        textField.textAlignment = .center
        textField.addTarget(self, action: #selector(textFieldValueChanged), for: .editingChanged)
        textField.keyboardType = .numberPad
        textField.inputAccessoryView = doneToolbar
        return textField
    }()
    
    private let unavailableAmountLabel: UILabel = {
        let label = UILabel()
        label.typography = Typography.bodySecondary()
        label.textColor = Colors.critical900.color
        label.text = Strings.DailyLimit.unavailableAmount
        label.textAlignment = .center
        label.numberOfLines = 0
        label.isHidden = true
        return label
    }()
    
    private let amountSlider: UISlider = {
        let slider = UISlider()
        slider.minimumValue = 0
        slider.thumbTintColor = Colors.branding400.color
        slider.tintColor = Colors.branding400.color
        slider.maximumTrackTintColor = Colors.grayscale100.color
        slider.isContinuous = true
        slider.addTarget(self, action: #selector(sliderValueChanged), for: .valueChanged)
        return slider
    }()
    
    private let minimumAmountDailyLimitLabel: UILabel = {
        let label = UILabel()
        label.typography = Typography.bodySecondary()
        label.textColor = Colors.grayscale700.color
        label.textAlignment = .left
        label.setContentHuggingPriority(.defaultLow, for: .horizontal)
        label.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        return label
    }()
    
    private let maximumAmountDailyLimitLabel: UILabel = {
        let label = UILabel()
        label.typography = Typography.bodySecondary()
        label.textColor = Colors.grayscale700.color
        label.textAlignment = .right
        return label
    }()
    
    private let usedDailyLimitTitleLabel: UILabel = {
        let label = UILabel()
        label.typography = Typography.bodySecondary()
        label.textColor = Colors.grayscale700.color
        label.text = Strings.DailyLimit.usedDailyLimit
        label.textAlignment = .center
        return label
    }()
    
    private let usedDailyLimitAmountLabel: UILabel = {
        let label = UILabel()
        label.typography = Typography.bodySecondary(.highlight)
        label.textColor = Colors.grayscale700.color
        label.textAlignment = .center
        return label
    }()
    
    private let increaseMaxDailyLimitButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(touchOnMaximumDailyLimit), for: .touchUpInside)
        return button
    }()
    
    private let updateButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.DailyLimit.updateLimit, for: .normal)
        button.isEnabled = false
        button.addTarget(self, action: #selector(triggerUpdateDailyLimit), for: .touchUpInside)
        return button
    }()
    
    private lazy var doneToolbar = DoneToolBar(doneText: Strings.General.ok)
    
    // MARK: - Variables
    private var bottomButton: Constraint?
    
    // MARK: - Life Cycle
    override public func viewDidLoad() {
        super.viewDidLoad()
        interactor.getLimitData()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationController?.navigationBar.largeTitleTextAttributes = [
                .foregroundColor: Colors.grayscale700.color,
                .font: Typography.title(.large).font()
            ]
        }
        navigationController?.navigationBar.tintColor = Colors.success600.color
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        descriptionLabel.layoutSkeletonIfNeeded()
        cardView.layoutSkeletonIfNeeded()
    }
    
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if !cardView.isSkeletonActive {
            cardView.layer.borderColor = Colors.grayscale100.color.cgColor
        }
        let gradient = SkeletonGradient(baseColor: Colors.grayscale100.color)
        descriptionLabel.updateAnimatedGradientSkeleton(usingGradient: gradient)
        cardView.updateAnimatedGradientSkeleton(usingGradient: gradient)
    }
    
    override public func buildViewHierarchy() {
        view.addSubview(contentScrollView)
        contentScrollView.addSubview(contentView)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(cardView)
        cardView.addSubview(cardContentView)
        cardContentView.addSubview(dailyLimitTitleLabel)
        cardContentView.addSubview(amountTextField)
        cardContentView.addSubview(unavailableAmountLabel)
        cardContentView.addSubview(amountSlider)
        cardContentView.addSubview(minimumAmountDailyLimitLabel)
        cardContentView.addSubview(maximumAmountDailyLimitLabel)
        cardContentView.addSubview(usedDailyLimitTitleLabel)
        cardContentView.addSubview(usedDailyLimitAmountLabel)
        cardContentView.addSubview(increaseMaxDailyLimitButton)
        view.addSubview(updateButton)
    }
    
    override public func setupConstraints() {
        contentScrollView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.width.equalToSuperview()
            $0.height.equalToSuperview().priority(.low)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalToSuperview().inset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        cardView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
        }
        
        setupConstraintsInsideCardView()
        
        updateButton.snp.makeConstraints {
            $0.top.equalTo(contentScrollView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
            bottomButton = $0.bottom.equalTo(view.compatibleSafeArea.bottom).inset(Spacing.base03).constraint
        }
    }
    
    private func setupConstraintsInsideCardView() {
        cardContentView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.top.greaterThanOrEqualToSuperview()
            $0.centerY.equalToSuperview()
        }
        
        dailyLimitTitleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().inset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        amountTextField.snp.makeConstraints {
            $0.top.equalTo(dailyLimitTitleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        unavailableAmountLabel.snp.makeConstraints {
            $0.top.equalTo(amountTextField.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        amountSlider.snp.makeConstraints {
            $0.top.equalTo(unavailableAmountLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        minimumAmountDailyLimitLabel.snp.makeConstraints {
            $0.top.equalTo(amountSlider.snp.bottom).offset(Spacing.base02)
            $0.leading.equalTo(amountSlider)
            $0.trailing.equalTo(maximumAmountDailyLimitLabel.snp.leading).inset(Spacing.base01)
        }
        
        maximumAmountDailyLimitLabel.snp.makeConstraints {
            $0.top.equalTo(amountSlider.snp.bottom).offset(Spacing.base02)
            $0.trailing.equalTo(amountSlider)
        }
        
        usedDailyLimitTitleLabel.snp.makeConstraints {
            $0.top.equalTo(maximumAmountDailyLimitLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        usedDailyLimitAmountLabel.snp.makeConstraints {
            $0.top.equalTo(usedDailyLimitTitleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        increaseMaxDailyLimitButton.snp.makeConstraints {
            $0.top.equalTo(usedDailyLimitAmountLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base03)
        }
    }

    override public func configureViews() {
        title = Strings.DailyLimit.title
        view.backgroundColor = Colors.backgroundPrimary.color
        configNavigation()
    }
    
    private func configNavigation() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Resources.Icons.icoQuestion.image,
            style: .plain,
            target: self,
            action: #selector(triggerFAQ)
        )
        guard navigationController?.viewControllers.count == 1 else {
            return
        }
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: Strings.General.close, style: .plain, target: self, action: #selector(close))
    }
}

// MARK: - Keyboard
@objc
extension DailyLimitViewController {
    func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        let result = Spacing.base02 + keyboardSize.height
        let currentValue = bottomButton?.layoutConstraints.first?.constant
        if currentValue == -Spacing.base02 || currentValue != result {
            bottomButton?.update(inset: result)
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }

    func keyboardWillHide(notification: NSNotification) {
        guard bottomButton?.layoutConstraints.first?.constant != -Spacing.base02 else {
            return
        }
        bottomButton?.update(inset: Spacing.base02)
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
}

// MARK: - Actions
@objc
private extension DailyLimitViewController {
    func sliderValueChanged(_ sender: UISlider) {
        interactor.changeDailyLimit(value: sender.value)
    }
    
    func textFieldValueChanged(_ sender: UITextField) {
        interactor.changeDailyLimit(amount: sender.text)
    }
    
    private func close() {
        interactor.close()
    }

    func triggerUpdateDailyLimit() {
        amountTextField.resignFirstResponder()
        interactor.updateDailyLimit()
    }
    
    private func triggerFAQ() {
        interactor.showFAQ()
    }

    private func touchOnMaximumDailyLimit() {
        interactor.showMaximumDailyLimit()
    }
}

// MARK: - DailyLimitDisplaying
extension DailyLimitViewController: DailyLimitDisplaying {
    public func displaySkeletonLoader() {
        cardView.layer.borderColor = UIColor.clear.cgColor
        updateButton.isHidden = true
        let gradient = SkeletonGradient(baseColor: Colors.grayscale100.color)
        descriptionLabel.showAnimatedGradientSkeleton(usingGradient: gradient)
        cardView.showAnimatedGradientSkeleton(usingGradient: gradient)
    }
    
    public func hideSkeletonLoader() {
        cardView.layer.borderColor = Colors.grayscale100.color.cgColor
        updateButton.isHidden = false
        descriptionLabel.hideSkeleton()
        cardView.hideSkeleton()
    }
    
    public func displayFullscreenError(image: UIImage?, title: String, description: NSAttributedString, buttonTitle: String) {
        let content = ApolloFeedbackViewContent(
            image: image,
            title: title,
            description: description,
            primaryButtonTitle: buttonTitle,
            secondaryButtonTitle: String()
        )
        let errorView = ApolloFeedbackView(content: content)
        errorView.secondaryButtonIsHidden(true)
        errorView.didTapPrimaryButton = { [weak self] in
            errorView.removeFromSuperview()
            self?.interactor.getLimitData()
        }
        view.addSubview(errorView)
        errorView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    public func displayMinimumLimit(value: Float, stringValue: String) {
        amountSlider.minimumValue = value
        minimumAmountDailyLimitLabel.text = stringValue
    }
    
    public func displayMaximumLimit(value: Float, stringValue: String) {
        amountSlider.maximumValue = value
        maximumAmountDailyLimitLabel.text = stringValue
    }
    
    public func displayCurrentDailyLimit(value: Float, amount: String) {
        amountSlider.value = value
        amountTextField.text = amount
    }
    
    public func displayUsedDailyLimit(amount: String) {
        usedDailyLimitAmountLabel.text = amount
    }
    
    public func displayIncreaseMaxLimit(text: NSAttributedString) {
        increaseMaxDailyLimitButton.setAttributedTitle(text, for: .normal)
    }
    
    public func displayNewLimit(amount: String) {
        amountTextField.text = amount
    }
    
    public func displaySlider(value: Float) {
        amountSlider.value = value
    }
    
    public func displayUnavailableLimitAmountError() {
        unavailableAmountLabel.isHidden = false
    }
    
    public func hideUnavailableLimitAmountError() {
        unavailableAmountLabel.isHidden = true
    }
    
    public func displayUpdateButtonEnabled() {
        updateButton.isEnabled = true
    }
    
    public func displayUpdateButtonDisabled() {
        updateButton.isEnabled = false
    }

    public func displaySnackbar(text: String, iconType: ApolloFeedbackCardViewIconType, emphasis: ApolloSnackbarEmphasisStyle) {
        let snackbar = ApolloSnackbar(text: text, iconType: iconType, emphasis: emphasis)
        showSnackbar(snackbar)
    }
    
    public func displayAlert(type: ApolloAlertType, title: String, subtitle: NSAttributedString, primaryButtonTitle: String, secondaryButtonTitle: String) {
        let primaryAction = ApolloAlertAction(title: primaryButtonTitle, completion: { [weak self] in
            self?.interactor.requestUpdateDailyLimit()
        })
        let linkAction = ApolloAlertAction(title: secondaryButtonTitle, completion: {})
        showApolloAlert(type: type, title: title, attributedSubtitle: subtitle, primaryButtonAction: primaryAction, linkButtonAction: linkAction)
    }
    
    public func displayUpdateLimitLoader() {
        updateButton.startLoading()
        amountTextField.isUserInteractionEnabled = false
        amountSlider.isUserInteractionEnabled = false
    }
    
    public func hideUpdateLimitLoader() {
        updateButton.stopLoading()
        amountTextField.isUserInteractionEnabled = true
        amountSlider.isUserInteractionEnabled = true
    }

    public func displayLoading() {
        startApolloLoader(loadingText: Strings.DailyLimit.wait)
    }

    public func hideLoading() {
        stopApolloLoader()
    }

    public func displayExistingTicketAlertWith(id: String) {
        let primaryButtonAction = ApolloAlertAction(title: Strings.DailyLimit.openChat) { [weak self] in
            self?.interactor.openTicketChatWith(id: id)
        }
        let linkButtonAction = ApolloAlertAction(title: Strings.DailyLimit.notNowThanks, completion: {})
        showApolloAlert(
            title: Strings.DailyLimit.helpIsComing,
            subtitle: Strings.DailyLimit.weReceivedYourHelpRequest,
            primaryButtonAction: primaryButtonAction,
            linkButtonAction: linkButtonAction
        )
    }
}

extension DailyLimitViewController: ApolloLoadable { }
