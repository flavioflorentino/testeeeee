import UI
import UIKit

class ReceiptWidgetItem: NSObject {
    struct ListItem {
        var label: String
        var value: String
        var labelTextColor: UIColor?
        var valueTextColor: UIColor?
        
        init(label: String, value: String, labelTextColor: UIColor? = nil, valueTextColor: UIColor? = nil) {
            self.label = label
            self.value = value
            self.labelTextColor = labelTextColor
            self.valueTextColor = valueTextColor
        }
        
        init?(jsonDict: [AnyHashable: Any]) {
            if let labelStr = jsonDict["label"] as? String {
                self.label = labelStr
            } else if let labelObj = jsonDict["label"] as? NSDictionary {
                if let labelStr = labelObj["value"] as? String {
                    self.label = labelStr
                } else {
                    return nil
                }
                if let labelColorHex = labelObj["color"] as? String {
                    self.labelTextColor = Palette.hexColor(with: labelColorHex)
                }
            } else {
                return nil
            }
            
            if let valueStr = jsonDict["text"] as? String {
                self.value = valueStr
            } else if let valueObj = jsonDict["value"] as? NSDictionary {
                if let valueStr = valueObj["value"] as? String {
                    self.value = valueStr
                } else {
                    return nil
                }
                if let valueColorHex = valueObj["color"] as? String {
                    self.valueTextColor = Palette.hexColor(with: valueColorHex)
                }
            } else {
                return nil
            }
        }
    }
    
    var order = 0
}
