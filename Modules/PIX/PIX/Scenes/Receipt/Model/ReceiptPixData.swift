import Foundation

struct ReceiptPixData: Decodable {
    let transactionId: String
    let timestamp: String
    let recipientUser: String
    let recipientKeyType: String
    let recipientKeyValue: String
    let recipientInstitution: String
    let originUser: String
    let originKeyType: String
    let originKeyValue: String
    let originInstitution: String
    let receiptDescription: String
    let receiptValueDetails: [String: String]
    let totalValue: String
}
