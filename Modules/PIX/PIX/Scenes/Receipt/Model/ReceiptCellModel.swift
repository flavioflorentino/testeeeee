import Foundation
import UI
import UIKit

enum ReceiptCellType {
    case bottom, body, detail, header
}

protocol ReceiptCellIdentifier {
    var type: ReceiptCellType { get }
    var identifier: String { get }
}

class ReceiptCellModel: ReceiptCellIdentifier {
    let type: ReceiptCellType
    let identifier: String
    
    init(_ type: ReceiptCellType, _ identifier: String) {
        self.type = type
        self.identifier = identifier
    }
}

final class ReceiptHeaderCellModel: ReceiptCellModel {
    let title: String
    let transactionId: String
    let timestamp: String
    
    init(_ transactionId: String, timestamp: String, title: String) {
        self.title = title
        self.transactionId = transactionId
        self.timestamp = timestamp
        super.init(.header, ReceiptHeaderCell.identifier)
    }
}

final class ReceiptBodyCellModel: ReceiptCellModel {
    let model: [[TransactionReceiptWidgetItem.SectionInfo]]
    
    init(_ model: [[TransactionReceiptWidgetItem.SectionInfo]]) {
        self.model = model
        super.init(.body, ReceiptBodyCell.identifier)
    }
}

struct ReceiptBodyUserModel {
    let user: String
    let key: String
    let keyType: String
    let institution: String
}

final class ReceiptBottomCellModel: ReceiptCellModel {
    let key: String
    let value: String
    
    init(_ key: String, value: String) {
        self.key = key
        self.value = value
        super.init(.bottom, ReceiptBottomCell.identifier)
    }
}

final class ReceiptDetailCellModel: ReceiptCellModel {
    let key: String
    let value: String
    
    init(_ key: String, value: String) {
        self.key = key
        self.value = value
        super.init(.detail, ReceiptValueDetailsCell.identifier)
    }
}
