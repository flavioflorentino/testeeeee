import UI
import UIKit

enum ReceiptSectionItem {
    case content(text: String, styleType: Typography.Style.BodySecondary)
    case space(isLineHidden: Bool)
}

final class ReceiptSectionItemCell: UITableViewCell {
    // MARK: - Visual Components
    private let contentLabel = UILabel()

    // MARK: - Life Cycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setContent(text: String, styleType: Typography.Style.BodySecondary) {
        contentLabel.text = text
        contentLabel.labelStyle(BodySecondaryLabelStyle(type: styleType))
            .with(\.textColor, Colors.grayscale500.color)
    }
}

// MARK: - ViewConfiguration
extension ReceiptSectionItemCell: ViewConfiguration {
    func setupConstraints() {
        contentLabel.snp.makeConstraints {
            $0.bottom.top.equalToSuperview().inset(Spacing.base00)
            $0.leading.trailing.equalToSuperview()
        }
    }

    func buildViewHierarchy() {
        contentView.addSubview(contentLabel)
    }

    func configureViews() {
        backgroundColor = .clear
    }
}
