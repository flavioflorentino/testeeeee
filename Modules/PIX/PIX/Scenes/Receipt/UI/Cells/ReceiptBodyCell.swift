import Foundation
import SnapKit
import UI
import UIKit

extension ReceiptBodyCell.Layout {
    enum LineView {
        static let height: CGFloat = 1
    }
    
    enum Size {
        static let estimatedRowHeight: CGFloat = 23
    }
}

final class ReceiptBodyCell: UITableViewCell {
    fileprivate enum Layout { }
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.isUserInteractionEnabled = false
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = Layout.Size.estimatedRowHeight
        tableView.register(ReceiptSectionItemCell.self, forCellReuseIdentifier: ReceiptSectionItemCell.identifier)
        tableView.register(ReceiptLineCell.self, forCellReuseIdentifier: ReceiptLineCell.identifier)
        return tableView
    }()
    
    private lazy var dataSource: TableViewDataSource<String, ReceiptSectionItem>? = {
        let dataSource = TableViewDataSource<String, ReceiptSectionItem>(view: tableView)
        dataSource.itemProvider = { tableView, indexPath, item -> UITableViewCell? in
            switch item {
            case let .content(text, styleType):
                let cell = tableView.dequeueReusableCell(withIdentifier: ReceiptSectionItemCell.identifier, for: indexPath) as? ReceiptSectionItemCell
                cell?.setContent(text: text, styleType: styleType)
                return cell
            case let .space(isLineHidden):
                let cell = tableView.dequeueReusableCell(withIdentifier: ReceiptLineCell.identifier, for: indexPath) as? ReceiptLineCell
                cell?.setContent(isLineHidden: isLineHidden)
                return cell
            }
        }
        return dataSource
    }()
    
    // MARK: - Inits
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
}

// MARK: - Internal Methods

extension ReceiptBodyCell {
    func configCell(_ body: ReceiptBodyCellModel) {
        var items: [ReceiptSectionItem] = []
        items.append(.space(isLineHidden: false))
        body.model.forEach { contentSection in
            contentSection.enumerated().forEach { index, sectionItem in
                items.append(.content(text: sectionItem.title, styleType: .highlight))
                sectionItem.list.forEach { text in
                    items.append(.content(text: text, styleType: .default))
                }
                if index != contentSection.count - 1 {
                    items.append(.space(isLineHidden: true))
                }
            }
            items.append(.space(isLineHidden: false))
        }
        dataSource?.add(items: items, to: "")
        tableView.layoutIfNeeded()
        tableView.snp.makeConstraints {
            $0.height.equalTo(tableView.contentSize.height)
        }
    }
}

// MARK: - ViewConfiguration

extension ReceiptBodyCell: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(tableView)
    }
    
    func setupConstraints() {
        tableView.snp.makeConstraints {
                    $0.top.equalToSuperview().offset(-Spacing.base03)
                    $0.bottom.equalToSuperview().offset(Spacing.base04)
                    $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.white.color
        tableView.dataSource = dataSource
    }
}
