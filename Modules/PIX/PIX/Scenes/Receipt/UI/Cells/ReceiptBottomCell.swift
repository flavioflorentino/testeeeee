import SnapKit
import UI
import UIKit

extension ReceiptBottomCell.Layout {
    enum LineView {
        static let height: CGFloat = 1
    }
}

final class ReceiptBottomCell: UITableViewCell {
    fileprivate enum Layout { }
    
    private lazy var lineView = UIView()
    
    private lazy var keyLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.grayscale800.color)
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.grayscale800.color)
            .with(\.textAlignment, .right)
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .equalCentering
        stack.spacing = Spacing.base02
        stack.addArrangedSubview(keyLabel)
        stack.addArrangedSubview(valueLabel)
        return stack
    }()
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    func configCell(_ model: ReceiptBottomCellModel) {
        keyLabel.text = model.key
        valueLabel.text = model.value
    }
}

extension ReceiptBottomCell: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(lineView)
        addSubview(stackView)
    }
    
    func setupConstraints() {
        lineView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(-Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.LineView.height)
        }
        
        stackView.snp.makeConstraints {
            $0.top.equalTo(lineView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        lineView.backgroundColor = Colors.grayscale100.color
        backgroundColor = Colors.white.color
    }
}
