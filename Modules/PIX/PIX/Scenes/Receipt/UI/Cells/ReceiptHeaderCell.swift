import AssetsKit
import UI
import UIKit
import SnapKit

extension ReceiptHeaderCell.Layout {
    enum ImageView {
        static let size = CGSize(width: 60, height: 60)
        static let bottomMargin = 20
    }
}

final class ReceiptHeaderCell: UITableViewCell {
    fileprivate enum Layout { }
    
    private lazy var receiptLogoImage = UIImageView(image: Resources.Icons.icoPixFill.image)
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.grayscale800.color)
        return label
    }()
    
    private lazy var transactionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, Colors.grayscale500.color)
        return label
    }()
    
    private lazy var timestampValue: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, Colors.grayscale500.color)
        return label
    }()
    
    private lazy var transactionTimestampStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Spacing.base01
        stack.alignment = .leading
        stack.addArrangedSubview(transactionLabel)
        stack.addArrangedSubview(timestampValue)
        return stack
    }()
    
    private lazy var receiptBaseInfoStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Spacing.base01
        stack.addArrangedSubview(titleLabel)
        stack.addArrangedSubview(transactionTimestampStack)
        return stack
    }()
    
    // MARK: - Inits
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
}

// MARK: - Internal Methods

extension ReceiptHeaderCell {
    func configCell(_ model: ReceiptHeaderCellModel) {
        titleLabel.text = model.title
        transactionLabel.text = model.transactionId
        timestampValue.text = model.timestamp
    }
}

// MARK: - ViewConfiguration

extension ReceiptHeaderCell: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(receiptLogoImage)
        addSubview(receiptBaseInfoStack)
    }
    
    func setupConstraints() {
        receiptLogoImage.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.ImageView.size)
            $0.bottom.equalToSuperview().offset(Layout.ImageView.bottomMargin)
        }
        
        receiptBaseInfoStack.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalTo(receiptLogoImage.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.white.color
    }
}
