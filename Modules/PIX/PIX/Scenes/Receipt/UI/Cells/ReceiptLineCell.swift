import UI
import UIKit

private enum Layout {
    enum Size {
        static let lineHeight: CGFloat = 1
    }
}

final class ReceiptLineCell: UITableViewCell {
    // MARK: - Visual Components
    private let lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        return view
    }()

    // MARK: - Life Cycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setContent(isLineHidden: Bool) {
        lineView.isHidden = isLineHidden
    }
}

// MARK: - ViewConfiguration
extension ReceiptLineCell: ViewConfiguration {
    func setupConstraints() {
        lineView.snp.makeConstraints {
            $0.bottom.top.equalToSuperview().inset(Spacing.base01)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Size.lineHeight)
        }
    }

    func buildViewHierarchy() {
        contentView.addSubview(lineView)
    }

    func configureViews() {
        backgroundColor = .clear
    }
}
