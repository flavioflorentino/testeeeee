import Core
import Foundation

public protocol ReceiptServicing {
    func getReceiptPix(transactionId: String, completion: @escaping (Result<(model: ReceiptResponse, data: Data?), ApiError>) -> Void)
    func getReceiverReceiptPixPF(transactionId: String, completion: @escaping (Result<(model: ReceiptResponse, data: Data?), ApiError>) -> Void)
}

public final class ReceiptService {
    private let dependencies: HasMainQueue

    public init(dependencies: HasMainQueue) {
        self.dependencies = dependencies
    }
}

// MARK: - ReceiptServicing
extension ReceiptService: ReceiptServicing {
    public func getReceiptPix(transactionId: String, completion: @escaping (Result<(model: ReceiptResponse, data: Data?), ApiError>) -> Void) {
        let endpoint = PixServiceEndpoint.receipt(transactionId: transactionId)
        Api<ReceiptResponse>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result)
            }
        }
    }
    
    public func getReceiverReceiptPixPF(transactionId: String, completion: @escaping (Result<(model: ReceiptResponse, data: Data?), ApiError>) -> Void) {
        let endpoint = PixServiceEndpoint.receiverReceiptPF(transactionId: transactionId)
        Api<ReceiptResponse>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result)
            }
        }
    }
}
