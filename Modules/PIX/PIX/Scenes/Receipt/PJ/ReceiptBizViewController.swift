import AssetsKit
import UI
import UIKit

protocol ReceiptBizViewDisplay: AnyObject {
    func showReceipt(_ items: [ReceiptCellModel])
    func displayError(message: String)
    func startLoading()
    func stopLoading()
}

public final class ReceiptBizViewController: ViewController<ReceiptBizViewInteractor, UIView> {
    typealias Localizable = Strings.Receipt
    
    private let isRefundAllowed: Bool
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = Colors.white.color
        return tableView
    }()
    
    private lazy var tableViewDataSource = TableViewDataSource<ReceiptCellType, ReceiptCellModel>(view: tableView)
    
    private lazy var closeBarButton: UIBarButtonItem = {
        let item = UIBarButtonItem(
            title: Localizable.Button.Title.close,
            style: .plain,
            target: self,
            action: #selector(didTapClose)
        )
        item.tintColor = Colors.branding300.color
        return item
    }()
    
    private lazy var optionsBarButton: UIBarButtonItem = {
        let item = UIBarButtonItem(
            title: Localizable.Button.Title.options,
            style: .plain,
            target: self,
            action: #selector(didTapOptions)
        )
        item.tintColor = Colors.branding300.color
        item.isEnabled = false
        return item
    }()
    
    public init(interactor: ReceiptBizViewInteractor, isRefundAllowed: Bool) {
        self.isRefundAllowed = isRefundAllowed
        super.init(interactor: interactor)
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        interactor.getReceiptInfo()
    }
    
    override public func buildViewHierarchy() {
       view.addSubview(tableView)
   }
   
    override public func configureViews() {
        navigationItem.leftBarButtonItem = closeBarButton
        navigationItem.rightBarButtonItem = optionsBarButton
        title = Localizable.title
        view.backgroundColor = Colors.white.color
   }
   
    override public func setupConstraints() {
       tableView.snp.makeConstraints {
           $0.edges.equalToSuperview()
       }
   }
}

extension ReceiptBizViewController: StatefulTransitionViewing {
    func registerCells() {
        tableView.register(ReceiptHeaderCell.self, forCellReuseIdentifier: ReceiptHeaderCell.identifier)
        tableView.register(ReceiptBodyCell.self, forCellReuseIdentifier: ReceiptBodyCell.identifier)
        tableView.register(ReceiptValueDetailsCell.self, forCellReuseIdentifier: ReceiptValueDetailsCell.identifier)
        tableView.register(ReceiptBottomCell.self, forCellReuseIdentifier: ReceiptBottomCell.identifier)
    }
    
    func getCell(_ view: UITableView, _ indexPath: IndexPath, _ item: ReceiptCellModel) -> UITableViewCell? {
        switch item.type {
        case .header:
            guard let model = item as? ReceiptHeaderCellModel else {
                return nil
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: item.identifier, for: indexPath) as? ReceiptHeaderCell
            cell?.configCell(model)
            return cell
        case .body:
            guard let model = item as? ReceiptBodyCellModel else {
                return nil
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: item.identifier, for: indexPath) as? ReceiptBodyCell
            cell?.configCell(model)
            return cell
        case .detail:
            guard let model = item as? ReceiptDetailCellModel else {
                return nil
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: item.identifier, for: indexPath) as? ReceiptValueDetailsCell
            cell?.configCell(model)
            return cell
        case .bottom:
            guard let model = item as? ReceiptBottomCellModel else {
                return nil
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: item.identifier, for: indexPath) as? ReceiptBottomCell
            cell?.configCell(model)
            return cell
        }
    }
}

extension ReceiptBizViewController: ReceiptBizViewDisplay {
    public func startLoading() {
        beginState(model: StateLoadingViewModel(message: Strings.KeyManager.loading))
    }
    
    public func stopLoading() {
        endState()
    }
    
    func displayError(message: String) {
        endState(animated: true) {
            DispatchQueue.main.async {
                self.endState(model: StatefulErrorViewModel(
                            image: Resources.Illustrations.iluConstruction.image,
                            content: (
                                title: Strings.KeyManager.error,
                                description: message
                            ),
                            button: (image: nil, title: Strings.KeyManager.errorAction))
                )
            }
        }
    }
    
    func showReceipt(_ items: [ReceiptCellModel]) {
        optionsBarButton.isEnabled = true
        items.forEach { item in
            self.tableViewDataSource.add(items: [item], to: item.type)
        }
        
        tableViewDataSource.itemProvider = getCell(_:_:_:)
        
        tableView.dataSource = tableViewDataSource
        tableView.reloadData()
    }
}

@objc
extension ReceiptBizViewController {
    func didTapClose() {
        interactor.dissmiss()
    }
    
    func didTapOptions() {
        let optionsAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let actions = getAlertActions()
        
        actions.forEach { action in
            optionsAlert.addAction(action)
        }
        
        interactor.showOptionsAlert(optionsAlert)
    }
    
    func getAlertActions() -> [UIAlertAction] {
        let shareAction = UIAlertAction(title: Localizable.ActionSheet.Title.share, style: .default) { _ in
            let image = self.view.snapshot
            let shareSheet = UIActivityViewController(activityItems: [image as Any], applicationActivities: nil)

            self.interactor.shareReceipt(controller: shareSheet)
        }
        let cancelAction = UIAlertAction(title: Localizable.ActionSheet.Title.cancel, style: .cancel)
        
        if isRefundAllowed {
            let giveBackAction = UIAlertAction(title: Localizable.ActionSheet.Title.giveback, style: .destructive) { _ in
                guard let navigation = self.navigationController else {
                    return
                }
                self.interactor.giveBackPayment(with: navigation)
            }
            return [shareAction, giveBackAction, cancelAction]
        }
        return [shareAction, cancelAction]
    }
}
