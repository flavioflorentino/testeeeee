import UI
import UIKit

enum ReceiptBizAction: Equatable {
    case close
    case options
    case showOptionsAlert(_ alert: UIAlertController)
    case refund(transactionId: String)
}

protocol ReceiptBizCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ReceiptBizAction)
    func shareReceipt(activityController: UIActivityViewController)
}

final class ReceiptBizCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - ReceiptBizCoordinating
extension ReceiptBizCoordinator: ReceiptBizCoordinating {
    func perform(action: ReceiptBizAction) {
        switch action {
        case .close:
            viewController?.dismiss(animated: true)
        case let .showOptionsAlert(alert):
            viewController?.navigationController?.present(alert, animated: true)
        default:
            viewController?.navigationController?.popViewController(animated: true)
        }
    }
    
    func shareReceipt(activityController: UIActivityViewController) {
        viewController?.present(activityController, animated: true)
    }
}
