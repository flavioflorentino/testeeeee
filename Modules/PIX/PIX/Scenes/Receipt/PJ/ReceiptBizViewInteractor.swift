import AnalyticsModule
import Core
import Foundation
import UI
import UIKit

public protocol ReceiptBizViewInteracting: AnyObject {
    func dissmiss()
    func getReceiptInfo()
    func shareReceipt(controller: UIActivityViewController)
    func giveBackPayment(with navigationController: UINavigationController)
    func showOptionsAlert(_ alert: UIAlertController)
}

public final class ReceiptBizViewInteractor {
    typealias Dependencies = HasAnalytics & HasUserAuthManager
    private let dependencies: Dependencies
    
    private var userInfo: KeyManagerBizUserInfo?
    private let service: ReceiptBizServicing
    private let presenter: ReceiptBizPresenting
    private let dataType: ReceiptType
    private let output: ReceiptBizOutput
    private var transactionId: String = ""
    private var value: String = ""
    private var description: String = ""
    
    init(dataType: ReceiptType,
         service: ReceiptBizServicing,
         presenter: ReceiptBizPresenting,
         dependencies: Dependencies,
         output: @escaping ReceiptBizOutput) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.dataType = dataType
        self.output = output
    }
}

extension ReceiptBizViewInteractor: ReceiptBizViewInteracting {
    public func showOptionsAlert(_ alert: UIAlertController) {
        sendEvent(eventType: .paymentReceiptOptions)
        presenter.showOptionsAlert(alert)
    }
    
    public func shareReceipt(controller: UIActivityViewController) {
        sendEvent(eventType: .paymentReceiptShare)
        presenter.shareReceipt(controller: controller)
    }
    
    public func getReceiptInfo() {
        presenter.startLoading()
        
        guard let userAuthManagerContract = dependencies.userAuthManagerContract else {
            let error = ApiError.bodyNotFound
            presenter.showError(apiError: error)
            return
        }
        presenter.startLoading()
        userAuthManagerContract.getUserInfo(completion: { [weak self] userInfo in
            guard let self = self else { return }
            if let userInfo = userInfo {
                self.userInfo = userInfo
                switch self.dataType {
                case let .data(data):
                    self.transactionId = data.data.transactionId
                    self.presenter.stopLoading()
                    self.presenter.presentReceiptInfo(data)
                case let .transactionId(id):
                    self.transactionId = id
                    self.presentReceipt(with: id)
                case let .movementTransaction(movementCode, transactionId):
                    self.transactionId = transactionId
                    self.presentReceipt(movementCode: movementCode, transactionId: transactionId)
                }
            } else {
                let error = ApiError.bodyNotFound
                self.presenter.showError(apiError: error)
            }
        })
    }
    
    private func presentReceipt(with transactionId: String) {
        service.getReceiptPix(transactionId: transactionId) {[weak self] result in
            guard let self = self else { return }
            self.presenter.stopLoading()
            switch result {
            case let .success(data):
                let transactionReceipt = TransactionReceiptWidgetItem(jsonDict: data.model.data.receipt.first ?? [:])
                self.value = transactionReceipt?.total ?? ""
                let currency = self.value.replacingOccurrences(of: "R$ ", with: "")
                let doubleValue = currency.doubleValue
                self.sendEvent(eventType: .receipt(value: doubleValue))
                self.presenter.presentReceiptInfo(data.model)
            case let .failure(apiError):
                if let requestError = apiError.requestError {
                    self.presenter.showError(requestError: requestError)
                } else {
                    self.presenter.showError(apiError: apiError)
                }
            }
        }
    }
    
    private func presentReceipt(movementCode: Int, transactionId: String) {
        service.getReceiptPix(movementCode: movementCode, transactionId: transactionId) {[weak self] result in
            guard let self = self else { return }
            self.presenter.stopLoading()
            switch result {
            case let .success(data):
                let transactionReceipt = TransactionReceiptWidgetItem(jsonDict: data.model.data.receipt.first ?? [:])
                self.value = transactionReceipt?.total ?? ""
                let currency = self.value.replacingOccurrences(of: "R$ ", with: "")
                let doubleValue = currency.doubleValue
                self.sendEvent(eventType: .receipt(value: doubleValue))
                self.presenter.presentReceiptInfo(data.model)
            case let .failure(apiError):
                if let requestError = apiError.requestError {
                    self.presenter.showError(requestError: requestError)
                } else {
                    self.presenter.showError(apiError: apiError)
                }
            }
        }
    }
    
    public func dissmiss() {
        presenter.closeReceipt()
    }
    
    public func giveBackPayment(with navigationController: UINavigationController) {
        sendEvent(eventType: .paymentReceiptRefund(value: value, description: description, identifier: transactionId))
        output(.refund(transactionId: transactionId))
    }
    
    private func sendEvent(eventType: KeyBizEvent) {
        let event = KeyBizTracker(companyName: userInfo?.name ?? "", eventType: eventType)
        dependencies.analytics.log(event)
    }
}
