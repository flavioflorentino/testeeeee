import Foundation
import UIKit

typealias ReceiptBizOutput = (ReceiptBizAction) -> Void

enum ReceiptType {
    case transactionId(String)
    case data(ReceiptResponse)
    case movementTransaction(movementCode: Int, transactionId: String)
}

enum ReceiptBizFactory {
    static func make(dataType: ReceiptType, isRefundAllowed: Bool = true, output: @escaping ReceiptBizOutput) -> ReceiptBizViewController {
        let container = DependencyContainer()
        let service: ReceiptBizServicing = ReceiptBizService(dependencies: container)
        let coordinator: ReceiptBizCoordinating = ReceiptBizCoordinator()
        let presenter: ReceiptBizPresenting = ReceiptBizPresenter(coordinator: coordinator)
        let interactor = ReceiptBizViewInteractor(dataType: dataType, service: service, presenter: presenter, dependencies: container, output: output)
        let viewController = ReceiptBizViewController(interactor: interactor, isRefundAllowed: isRefundAllowed)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
