import Core
import Foundation
import UI
import UIKit

protocol ReceiptBizPresenting: AnyObject {
    var viewController: ReceiptBizViewDisplay? { get set }
    func presentReceiptInfo(_ response: ReceiptResponse)
    func closeReceipt()
    func startLoading()
    func stopLoading()
    func shareReceipt(controller: UIActivityViewController)
    func showError(apiError: ApiError)
    func showError(requestError: RequestError)
    func showOptionsAlert(_ alert: UIAlertController)
}

final class ReceiptBizPresenter {
    private let coordinator: ReceiptBizCoordinating
    weak var viewController: ReceiptBizViewDisplay?

    init(coordinator: ReceiptBizCoordinating) {
        self.coordinator = coordinator
    }
}

extension ReceiptBizPresenter: ReceiptBizPresenting {
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func showError(apiError: ApiError) {
        viewController?.displayError(message: apiError.localizedDescription)
    }
    
    func showError(requestError: RequestError) {
        viewController?.displayError(message: requestError.message)
    }
    
    func presentReceiptInfo(_ response: ReceiptResponse) {
        var sections: [ReceiptCellModel] = []
        let transactionReceipt = TransactionReceiptWidgetItem(jsonDict: response.data.receipt.first ?? [:])

        let headerSection = ReceiptHeaderCellModel(transactionReceipt?.header.id ?? "", timestamp: transactionReceipt?.header.date ?? "", title: transactionReceipt?.header.title ?? "")
        sections.append(headerSection)
        
        let bodySection = ReceiptBodyCellModel(transactionReceipt?.body ?? [[]])
        sections.append(bodySection)
        
        transactionReceipt?.list.forEach { item in
            sections.append(ReceiptDetailCellModel(item.label, value: item.value))
        }
        
        let bottomSection = ReceiptBottomCellModel(Strings.Receipt.FooterCell.value, value: transactionReceipt?.total ?? "")
        sections.append(bottomSection)
        
        viewController?.showReceipt(sections)
    }
    
    func closeReceipt() {
        coordinator.perform(action: .close)
    }
    
    func shareReceipt(controller: UIActivityViewController) {
        coordinator.shareReceipt(activityController: controller)
    }
    
    func showOptionsAlert(_ alert: UIAlertController) {
        coordinator.perform(action: .showOptionsAlert(alert))
    }
}
