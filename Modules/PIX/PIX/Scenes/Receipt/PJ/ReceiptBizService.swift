import Core
import Foundation

protocol ReceiptBizServicing {
    func getReceiptPix(transactionId: String, completion: @escaping (Result<(model: ReceiptResponse, data: Data?), ApiError>) -> Void)
    func getReceiptPix(movementCode: Int, transactionId: String, completion: @escaping (Result<(model: ReceiptResponse, data: Data?), ApiError>) -> Void)
}

final class ReceiptBizService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ReceiptServicing
extension ReceiptBizService: ReceiptBizServicing {
    func getReceiptPix(transactionId: String, completion: @escaping (Result<(model: ReceiptResponse, data: Data?), ApiError>) -> Void) {
        let endpoint = PixServiceEndpoint.receiptBiz(transactionId: transactionId)
        Api<ReceiptResponse>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result)
            }
        }
    }
    func getReceiptPix(movementCode: Int, transactionId: String, completion: @escaping (Result<(model: ReceiptResponse, data: Data?), ApiError>) -> Void) {
        let endpoint = PixServiceEndpoint.newReceiptBiz(movementCode: movementCode, transactionId: transactionId)
        Api<ReceiptResponse>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result)
            }
        }
    }
}
