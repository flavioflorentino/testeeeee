import Foundation
import AnalyticsModule

protocol KeyDescriptionInteracting: AnyObject {
    func viewDidAppear()
    func keyDescriptionChanged(_ text: String)
    func goToCreateKey(with description: String)
}

final class KeyDescriptionInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let presenter: KeyDescriptionPresenting
    private let createKey: CreateKey
    private let userInfo: KeyManagerBizUserInfo

    init(userInfo: KeyManagerBizUserInfo, presenter: KeyDescriptionPresenting, dependencies: Dependencies, createKey: CreateKey) {
        self.userInfo = userInfo
        self.presenter = presenter
        self.createKey = createKey
        self.dependencies = dependencies
    }
}

// MARK: - KeyDescriptionInteracting
extension KeyDescriptionInteractor: KeyDescriptionInteracting {
    func viewDidAppear() {
        let event = KeyBizTracker(companyName: userInfo.name, eventType: .randomKeyGenerationViewed)
        dependencies.analytics.log(event)
    }
    
    func goToCreateKey(with description: String) {
        let event = KeyBizTracker(companyName: userInfo.name, eventType: .randomKeyGenerationDone(description: description))
        dependencies.analytics.log(event)
        
        let randomCreateKey = CreateKey(userId: createKey.userId,
                                        userType: createKey.userType,
                                        value: createKey.value,
                                        type: createKey.type,
                                        name: description)
        presenter.didNextStep(action: .createBizKey(key: randomCreateKey, userInfo: userInfo))
    }
    
    func keyDescriptionChanged(_ text: String) {
        let textChanged = text.trimmingCharacters(in: .whitespacesAndNewlines)
        presenter.enableContinueButton(!textChanged.isEmpty)
    }
}
