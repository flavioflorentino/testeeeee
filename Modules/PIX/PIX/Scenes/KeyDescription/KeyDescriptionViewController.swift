import UI
import UIKit

protocol KeyDescriptionDisplaying: AnyObject {
    func enableContinueButton(_ isEnable: Bool)
}

private extension KeyDescriptionViewController.Layout {
    enum Size {
        static let buttonHeight: CGFloat = 48
    }
}

final class KeyDescriptionViewController: ViewController<KeyDescriptionInteracting, UIView> {
    fileprivate enum Layout { }
    
    enum Accessibility: String {
        case textField
        case nextButton
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.KeyDescription.subTitle
        label
            .labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .grayscale600())
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.KeyDescription.description
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale600())
        return label
    }()
    
    private lazy var descriptionTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.accessibilityIdentifier = Accessibility.textField.rawValue
        textField.title = Strings.KeyDescription.TextField.helper
        textField.setTitleVisible(true)
        textField.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
        return textField
    }()
    
    private lazy var nextButton: UIButton = {
        let button = UIButton()
        button.accessibilityIdentifier = Accessibility.nextButton.rawValue
        button.setTitle(Strings.KeyDescription.button, for: .normal)
        button.addTarget(self, action: #selector(nextStep), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        button.isEnabled = false
        return button
    }()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        interactor.viewDidAppear()
    }

    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(subtitleLabel)
        view.addSubview(descriptionTextField)
        view.addSubview(nextButton)
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        descriptionTextField.snp.makeConstraints {
            $0.top.equalTo(subtitleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        nextButton.snp.makeConstraints {
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.Size.buttonHeight)
        }
    }

    override func configureViews() {
        title = Strings.KeyDescription.title
        view.backgroundColor = Colors.backgroundPrimary.color
        configureKeyboard()
    }
}

@objc
private extension KeyDescriptionViewController {
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textFieldDidChanged(_ textField: UITextField) {
        interactor.keyDescriptionChanged(textField.text ?? "")
    }
    
    func keyboardWillShow(notification: NSNotification) {
        guard
            let userInfo = notification.userInfo,
            let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        else {
            return
        }
        
        updateKeyboardConstraint(userInfo: userInfo, inset: keyboardSize.cgRectValue.height)
    }
    
    func keyboardWillHide(notification: NSNotification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        updateKeyboardConstraint(userInfo: userInfo, inset: 0)
    }
    
    func nextStep() {
        interactor.goToCreateKey(with: descriptionTextField.text ?? "")
    }
}

private extension KeyDescriptionViewController {
    func configureKeyboard() {
        tapGesture()
        keyboardObservers()
    }
    
    func tapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func keyboardObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    func updateKeyboardConstraint(userInfo: [AnyHashable: Any], inset: CGFloat) {
        guard let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
            let animationCurve = (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue
            else {
                return
        }
        
        nextButton.snp.updateConstraints {
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).inset(inset)
        }
        
        UIView.animate(
            withDuration: animationDuration,
            delay: 0,
            options: UIView.AnimationOptions(rawValue: animationCurve),
            animations: {
                self.view.layoutIfNeeded()
            },
            completion: nil
        )
    }
}

extension KeyDescriptionViewController: KeyDescriptionDisplaying {
    func enableContinueButton(_ isEnable: Bool) {
        nextButton.isEnabled = isEnable
    }
}
