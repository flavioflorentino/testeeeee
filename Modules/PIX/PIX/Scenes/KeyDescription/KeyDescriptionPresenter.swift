import Foundation

protocol KeyDescriptionPresenting: AnyObject {
    var viewController: KeyDescriptionDisplaying? { get set }
    func enableContinueButton(_ isEnabled: Bool)
    func didNextStep(action: KeyDescriptionAction)
}

final class KeyDescriptionPresenter {
    private let coordinator: KeyDescriptionCoordinating
    weak var viewController: KeyDescriptionDisplaying?

    init(coordinator: KeyDescriptionCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - KeyDescriptionPresenting
extension KeyDescriptionPresenter: KeyDescriptionPresenting {
    func enableContinueButton(_ isEnabled: Bool) {
        viewController?.enableContinueButton(isEnabled)
    }
    
    func didNextStep(action: KeyDescriptionAction) {
        coordinator.perform(action: action)
    }
}
