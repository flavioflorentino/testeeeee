import Foundation
import UIKit

enum KeyDescriptionFactory {
    static func make(userInfo: KeyManagerBizUserInfo,
                     createKey: CreateKey,
                     output: @escaping (CreateKey, KeyManagerBizUserInfo) -> Void) -> KeyDescriptionViewController {
        let container = DependencyContainer()
        let coordinator: KeyDescriptionCoordinating = KeyDescriptionCoordinator(output: output)
        let presenter: KeyDescriptionPresenting = KeyDescriptionPresenter(coordinator: coordinator)
        let interactor = KeyDescriptionInteractor(userInfo: userInfo, presenter: presenter, dependencies: container, createKey: createKey)
        let viewController = KeyDescriptionViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
