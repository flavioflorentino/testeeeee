import UIKit

enum KeyDescriptionAction: Equatable {
    case createBizKey(key: CreateKey, userInfo: KeyManagerBizUserInfo)
}

protocol KeyDescriptionCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: KeyDescriptionAction)
}

final class KeyDescriptionCoordinator {
    weak var viewController: UIViewController?
    let output: (CreateKey, KeyManagerBizUserInfo) -> Void
    
    init(output: @escaping (CreateKey, KeyManagerBizUserInfo) -> Void) {
        self.output = output
    }
}

// MARK: - KeyDescriptionCoordinating
extension KeyDescriptionCoordinator: KeyDescriptionCoordinating {
    func perform(action: KeyDescriptionAction) {
        if case let .createBizKey(key, userInfo) = action {
            output(key, userInfo)
        }
    }
}
