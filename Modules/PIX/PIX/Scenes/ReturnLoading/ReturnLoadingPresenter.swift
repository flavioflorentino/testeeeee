import Core
import Foundation

protocol ReturnLoadingPresenting: AnyObject {
    var viewController: ReturnLoadingDisplay? { get set }
    func didNextStep(action: ReturnLoadingAction)
    func showError(error: ApiError)
}

final class ReturnLoadingPresenter {
    private let coordinator: ReturnLoadingCoordinating
    weak var viewController: ReturnLoadingDisplay?

    init(coordinator: ReturnLoadingCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ReturnLoadingPresenting
extension ReturnLoadingPresenter: ReturnLoadingPresenting {
    func didNextStep(action: ReturnLoadingAction) {
        coordinator.perform(action: action)
    }
    
    func showError(with title: String, description: String, buttonTitle: String) {
        viewController?.showError(with: title, description: description, buttonTitle: buttonTitle)
    }
    
    func showError(error: ApiError) {
        guard let title = error.requestError?.alert?.title?.string,
              let description = error.requestError?.alert?.text?.string
        else {
            showError(with: Strings.General.somethingWentWrong,
                      description: Strings.General.failedComms,
                      buttonTitle: Strings.General.gotIt)
            return
        }
        let buttonTitle: String? = error.requestError?.alert?.buttonsJson.first?["title"] as? String
        
        showError(with: title, description: description, buttonTitle: buttonTitle ?? "")
    }
}
