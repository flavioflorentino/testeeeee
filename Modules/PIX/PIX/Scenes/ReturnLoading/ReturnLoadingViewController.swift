import UI
import UIKit

protocol ReturnLoadingDisplay: AnyObject {
    func showError(with title: String, description: String, buttonTitle: String)
}

final class ReturnLoadingViewController: ViewController<ReturnLoadingInteracting, UIView>, LoadingViewProtocol {
    lazy var loadingView = LoadingView()

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadPixData()
        startLoadingView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}

// MARK: ReturnLoadingDisplay
extension ReturnLoadingViewController: ReturnLoadingDisplay {
    func showError(with title: String, description: String, buttonTitle: String) {
        let controller = PopupViewController(title: title,
                                             description: description,
                                             preferredType: .text)
        controller.addAction(confirmAction(buttonTitle: buttonTitle))
        showPopup(controller)
    }
    
    func confirmAction(buttonTitle: String) -> PopupAction {
        PopupAction(title: buttonTitle, style: .fill) {
            self.interactor.sendErrorToCoordinator()
        }
    }
}
