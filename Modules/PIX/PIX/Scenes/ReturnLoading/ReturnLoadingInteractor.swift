import AnalyticsModule
import Core
import Foundation

protocol ReturnLoadingInteracting: AnyObject {
    func loadPixData()
    func sendErrorToCoordinator()
}

final class ReturnLoadingInteractor {
    typealias Dependencies = HasNoDependency & HasUserAuthManager & HasAnalytics
    private let dependencies: Dependencies
    private let outdatedRefund = "devolução não permitida"
    private let service: ReturnLoadingServicing
    private let presenter: ReturnLoadingPresenting
    private let flowType: CashoutLoadingFlowType
    private var userInfo: KeyManagerBizUserInfo?
    
    init(service: ReturnLoadingServicing,
         presenter: ReturnLoadingPresenting,
         dependencies: Dependencies,
         flowType: CashoutLoadingFlowType) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.flowType = flowType
    }
}

// MARK: - ReturnLoadingInteracting
extension ReturnLoadingInteractor: ReturnLoadingInteracting {
    func loadPixData() {
        switch flowType {
        case let .refund(transactionId):
            refundFlow(with: transactionId)
        case let .qrCode(code):
            qrCodeFlow(with: code)
        case let .pixKey(key, keyType):
            pixKeyFlow(with: key, pixType: keyType)
        case let .bankAccount(pixParams):
            bankAccountFlow(with: pixParams)
        case let .copyPaste(code):
            copyPasteFlow(with: code)
        }
    }
    
    func refundFlow(with transactionId: String) {
        getUserInfo { [weak self] userInfo in
            self?.getUserBalance(completion: { amount, apiError in
                if let error = apiError {
                    self?.handleError(error: error)
                    return
                }
                self?.getRefundPixAccount(with: transactionId, completion: { result, apiError in
                    if let error = apiError {
                        self?.handleError(error: error)
                        return
                    }
                    guard let result = result, let amount = amount else {
                        self?.handleError(error: ApiError.bodyNotFound)
                        return
                    }
                    self?.presenter.didNextStep(action: .successRefund(pixAccount: result,
                                                                       availableAmount: "\(amount.available)",
                                                                       userInfo: userInfo))
                })
            })
        }
    }
    
    func qrCodeFlow(with qrCode: String) {
        getUserInfo { [weak self] userInfo in
            self?.getUserBalance(completion: { amount, apiError in
                if let error = apiError {
                    self?.handleError(error: error)
                    return
                }
                self?.getQRCode(with: qrCode, completion: { [weak self] result, apiError in
                    guard let self = self else {
                        return
                    }
                    if let error = apiError {
                        self.handleError(error: error)
                        return
                    }
                    guard let result = result, let amount = amount else {
                        self.handleError(error: ApiError.bodyNotFound)
                        return
                    }
                    let pixAccount = self.createPixAccount(with: result)
                    let pixParams = PIXParams(originType: .qrCode, keyType: "QRCODE", key: qrCode, receiverData: nil)
                    self.presenter.didNextStep(action: .successQRCodeSend(pixParams: pixParams,
                                                                          amount: result.value,
                                                                          availableAmount: "\(amount.available)",
                                                                          pixAccount: pixAccount,
                                                                          qrPixAccount: result,
                                                                          userInfo: userInfo))
                })
            })
        }
    }
    
    func pixKeyFlow(with key: String, pixType: KeyType) {
        getUserInfo { [weak self] userInfo in
            self?.getUserBalance(completion: { amount, apiError in
                if let error = apiError {
                    self?.handleError(error: error)
                    return
                }
                self?.getPixAccount(with: key, keyType: pixType, completion: { result, apiError in
                    if let error = apiError {
                        self?.handleError(error: error)
                        return
                    }
                    guard let result = result, let amount = amount else {
                        self?.handleError(error: ApiError.bodyNotFound)
                        return
                    }
                    let pixParams = PIXParams(originType: .key,
                                              keyType: pixType.serviceValue,
                                              key: key,
                                              receiverData: nil)
                    self?.presenter.didNextStep(action: .successSend(pixParams: pixParams,
                                                                     amount: nil,
                                                                     availableAmount: "\(amount.available)",
                                                                     pixAccount: result,
                                                                     userInfo: userInfo))
                })
            })
        }
    }
    
    func bankAccountFlow(with pixParams: PIXParams) {
        getUserInfo { [weak self] userInfo in
            self?.getUserBalance(completion: { amount, apiError in
                if let error = apiError {
                    self?.handleError(error: error)
                    return
                }
                self?.getPixAccount(with: pixParams, completion: { result, apiError in
                    if let error = apiError {
                        self?.handleError(error: error)
                        return
                    }
                    guard let result = result, let amount = amount else {
                        self?.handleError(error: ApiError.bodyNotFound)
                        return
                    }
                    self?.presenter.didNextStep(action: .successSend(pixParams: pixParams,
                                                                     amount: nil,
                                                                     availableAmount: "\(amount.available)",
                                                                     pixAccount: result,
                                                                     userInfo: userInfo))
                })
            })
        }
    }
    
    func copyPasteFlow(with code: String) {
        getUserInfo { [weak self] userInfo in
            self?.getUserBalance(completion: { amount, apiError in
                if let error = apiError {
                    self?.handleError(error: error)
                    return
                }
                self?.getPixAccount(with: code, completion: { result, apiError in
                    guard let self = self else {
                        return
                    }
                    if let error = apiError {
                        self.handleError(error: error)
                        return
                    }
                    guard let result = result, let amount = amount else {
                        self.handleError(error: ApiError.bodyNotFound)
                        return
                    }
                    
                    let pixAccount = self.createPixAccount(with: result)
                    let pixParams = PIXParams(originType: .qrCode, keyType: "QRCODE", key: code, receiverData: nil)
                    self.presenter.didNextStep(action: .successQRCodeSend(pixParams: pixParams,
                                                                          amount: result.value,
                                                                          availableAmount: "\(amount.available)",
                                                                          pixAccount: pixAccount,
                                                                          qrPixAccount: result,
                                                                          userInfo: userInfo))
                })
            })
        }
    }
    
    private func createPixAccount(with qrCode: PixQrCodeAccount) -> PixAccount {
        PixAccount(name: qrCode.name,
                   cpfCnpj: qrCode.cpfCnpj,
                   bank: qrCode.bank,
                   agency: "",
                   accountNumber: "",
                   imageUrl: qrCode.imageUrl,
                   id: nil)
    }
    
    private func getQRCode(with qrCode: String, completion: @escaping (PixQrCodeAccount?, ApiError?) -> Void) {
        service.getQrCodeData(with: qrCode) { result in
            switch result {
            case let .success(qrCodeData):
                completion(qrCodeData, nil)
            case let .failure(error):
                self.sendEvent(.keyError(error: error.requestError?.alert?.text?.string ?? ""))
                completion(nil, error)
            }
        }
    }
    
    private func getUserBalance(completion: @escaping (AvailableAmountViewModel?, ApiError?) -> Void) {
        service.userBalance { result in
            switch result {
            case let .success(amount):
                completion(amount, nil)
            case let .failure(apiError):
                completion(nil, apiError)
            }
        }
    }
    
    private func getRefundPixAccount(with transactionId: String, completion: @escaping (ReturnLoadingResult?, ApiError?) -> Void) {
        service.getRefundPixAccount(transactionId: transactionId) { [weak self] result in
            switch result {
            case let .success(pixResult):
                completion(pixResult, nil)
            case let .failure(error):
                if error.requestError?.alert?.title?.string.lowercased() == self?.outdatedRefund {
                    self?.sendEvent(.outdatedReturn)
                }
                
                self?.sendEvent(.errorInPayment(errorType: error.requestError?.alert?.text?.string ?? ""))
                completion(nil, error)
            }
        }
    }
    
    private func getPixAccount(with key: String, keyType: KeyType, completion: @escaping (PixAccount?, ApiError?) -> Void) {
        service.getPixAccount(key: key, keyType: keyType) { [weak self] result in
            switch result {
            case let .success(pixResult):
                completion(pixResult, nil)
            case let .failure(error):
                self?.sendEvent(.errorInPayment(errorType: error.requestError?.alert?.text?.string ?? ""))
                completion(nil, error)
            }
        }
    }
    
    private func getPixAccount(with pastedCode: String, completion: @escaping (PixQrCodeAccount?, ApiError?) -> Void) {
        service.getCopyPasteData(with: pastedCode) { [weak self] result in
            switch result {
            case let .success(pixResult):
                completion(pixResult, nil)
            case let .failure(error):
                self?.sendEvent(.errorInPayment(errorType: error.requestError?.alert?.text?.string ?? ""))
                completion(nil, error)
            }
        }
    }
    
    private func getPixAccount(with pixParams: PIXParams, completion: @escaping (PixAccount?, ApiError?) -> Void) {
        service.getPixAccount(pixParams: pixParams) { [weak self] result in
            switch result {
            case let .success(pixResult):
                completion(pixResult, nil)
            case let .failure(error):
                self?.sendEvent(.errorInPayment(errorType: error.requestError?.alert?.text?.string ?? ""))
                completion(nil, error)
            }
        }
    }
    
    private func getUserInfo(completion: @escaping (KeyManagerBizUserInfo?) -> Void) {
        guard let userAuthManagerContract = dependencies.userAuthManagerContract else {
            let error = ApiError.bodyNotFound
            handleError(error: error)
            return
        }
        userAuthManagerContract.getUserInfo(completion: { [weak self] userInfo in
            self?.userInfo = userInfo
            completion(userInfo)
        })
    }
    
    private func handleError(error: ApiError) {
        presenter.showError(error: error)
    }
    
    private func sendEvent(_ event: KeyBizEvent) {
        self.dependencies.analytics.log(KeyBizTracker(companyName: userInfo?.name ?? "", eventType: event))
    }
    
    func sendErrorToCoordinator() {
        presenter.didNextStep(action: .error)
    }
}
