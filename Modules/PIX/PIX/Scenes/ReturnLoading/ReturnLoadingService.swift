import Core
import Foundation

protocol ReturnLoadingServicing {
    func getRefundPixAccount(transactionId: String, completion: @escaping(Result<ReturnLoadingResult, ApiError>) -> Void)
    func userBalance(_ completion: @escaping (Result<AvailableAmountViewModel?, ApiError>) -> Void)
    func getQrCodeData(with qrCode: String, completion: @escaping (Result<PixQrCodeAccount, ApiError>) -> Void)
    func getCopyPasteData(with code: String, completion: @escaping (Result<PixQrCodeAccount, ApiError>) -> Void)
    func getPixAccount(key: String, keyType: KeyType, completion: @escaping (Result<PixAccount, ApiError>) -> Void)
    func getPixAccount(pixParams: PIXParams, completion: @escaping (Result<PixAccount, ApiError>) -> Void)
}

final class ReturnLoadingService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ReturnLoadingServicing
extension ReturnLoadingService: ReturnLoadingServicing {
    func getRefundPixAccount(transactionId: String, completion: @escaping (Result<ReturnLoadingResult, ApiError>) -> Void) {
        let endpoint = PixBizPaymentServiceEndpoint.devolution(transactionId: transactionId)
        let decoder = JSONDecoder(.convertFromSnakeCase)
        Api<ReturnLoadingResult>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func userBalance(_ completion: @escaping (Result<AvailableAmountViewModel?, ApiError>) -> Void) {
        Api<AvailableAmountViewModel>(endpoint: UserWalletBalanceEndpoint.getBalance).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map { $0.model })
            }
        }
    }
    
    func getQrCodeData(with qrCode: String, completion: @escaping (Result<PixQrCodeAccount, ApiError>) -> Void) {
        let endpoint = PixBizPaymentServiceEndpoint.qrCode(scannedText: qrCode)
        let decoder = JSONDecoder(.convertFromSnakeCase)
        Api<PixQrCodeAccount>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func getCopyPasteData(with code: String, completion: @escaping (Result<PixQrCodeAccount, ApiError>) -> Void) {
        let endpoint = PixBizPaymentServiceEndpoint.copyPaste(code: code)
        let decoder = JSONDecoder(.convertFromSnakeCase)
        Api<PixQrCodeAccount>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func getPixAccount(key: String, keyType: KeyType, completion: @escaping (Result<PixAccount, ApiError>) -> Void) {
        let endpoint = PixBizPaymentServiceEndpoint.keyValidator(key: key, type: keyType)
        let decoder = JSONDecoder(.convertFromSnakeCase)
        Api<PixAccount>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func getPixAccount(pixParams: PIXParams, completion: @escaping (Result<PixAccount, ApiError>) -> Void) {
        let endpoint = PixBizPaymentServiceEndpoint.manualInsertion(params: pixParams)
        let decoder = JSONDecoder(.convertFromSnakeCase)
        Api<PixAccount>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
