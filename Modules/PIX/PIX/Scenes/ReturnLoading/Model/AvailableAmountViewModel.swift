struct AvailableAmountViewModel: Decodable, Equatable {
    private enum CodingKeys: String, CodingKey {
        case data
        case available = "available_balance"
    }
    
    let available: String
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let dataContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        available = try dataContainer.decode(String.self, forKey: .available)
    }
}
