struct ReturnLoadingResult: Equatable, Decodable {
    var receiverData: PixAccount
    var originalTransactionId: String
    var originalValue: Double
    var valueAvailable: Double
}
