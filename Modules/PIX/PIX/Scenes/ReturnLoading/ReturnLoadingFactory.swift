import Foundation
import UIKit

typealias ReturnLoadingOutput = (ReturnLoadingAction) -> Void

enum CashoutLoadingFlowType {
    case qrCode(code: String)
    case pixKey(key: String, keyType: KeyType)
    case refund(transactionId: String)
    case bankAccount(pixParams: PIXParams)
    case copyPaste(code: String)
}

enum ReturnLoadingFactory {
    static func make(flowType: CashoutLoadingFlowType, output: @escaping ReturnLoadingOutput) -> ReturnLoadingViewController {
        let container = DependencyContainer()
        let service: ReturnLoadingServicing = ReturnLoadingService(dependencies: container)
        let coordinator: ReturnLoadingCoordinating = ReturnLoadingCoordinator(output: output)
        let presenter: ReturnLoadingPresenting = ReturnLoadingPresenter(coordinator: coordinator)
        let interactor = ReturnLoadingInteractor(service: service,
                                                 presenter: presenter,
                                                 dependencies: container,
                                                 flowType: flowType)
        let viewController = ReturnLoadingViewController(interactor: interactor)

        presenter.viewController = viewController

        return viewController
    }
}
