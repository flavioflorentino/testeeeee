import UIKit

enum ReturnLoadingAction: Equatable {
    case successRefund(pixAccount: ReturnLoadingResult,
                       availableAmount: String,
                       userInfo: KeyManagerBizUserInfo?)
    case successSend(pixParams: PIXParams,
                     amount: Double?,
                     availableAmount: String,
                     pixAccount: PixAccount?,
                     userInfo: KeyManagerBizUserInfo?)
    case successQRCodeSend(pixParams: PIXParams,
                           amount: Double?,
                           availableAmount: String,
                           pixAccount: PixAccount?,
                           qrPixAccount: PixQrCodeAccount?,
                           userInfo: KeyManagerBizUserInfo?)
    case error
}

protocol ReturnLoadingCoordinating: AnyObject {
    func perform(action: ReturnLoadingAction)
}

final class ReturnLoadingCoordinator {
    private let output: ReturnLoadingOutput

    init(output: @escaping ReturnLoadingOutput) {
        self.output = output
    }
}

// MARK: - ReturnLoadingCoordinating
extension ReturnLoadingCoordinator: ReturnLoadingCoordinating {
    func perform(action: ReturnLoadingAction) {
        output(action)
    }
}
