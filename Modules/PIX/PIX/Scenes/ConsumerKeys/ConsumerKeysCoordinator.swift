import UIKit

enum ConsumerKeysAction {
    case close
}

protocol ConsumerKeysCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ConsumerKeysAction)
}

final class ConsumerKeysCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - ConsumerKeysCoordinating
extension ConsumerKeysCoordinator: ConsumerKeysCoordinating {
    func perform(action: ConsumerKeysAction) {
        switch action {
        case .close:
            viewController?.navigationController?.dismiss(animated: true)
        }
    }
}
