import Core
import Foundation

protocol ConsumerKeysServicing {
}

final class ConsumerKeysService {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ConsumerKeysServicing
extension ConsumerKeysService: ConsumerKeysServicing {
}
