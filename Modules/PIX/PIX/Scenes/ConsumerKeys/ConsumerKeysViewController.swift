import UI
import UIKit
import SnapKit
import AssetsKit

protocol ConsumerKeysDisplaying: AnyObject {
}

final class ConsumerKeysViewController: ViewController<ConsumerKeysInteracting, UIView> {
    private typealias ItemProviderType = TableViewDataSource<Int, PixConsumerKey>.ItemProvider
    
    private lazy var headerView = ConsumerKeysHeaderView()
    
    private lazy var itemProvider: ItemProviderType = { tableView, indexPath, item -> UITableViewCell? in
        let cell = tableView.dequeueReusableCell(withIdentifier: ConsumerKeysTableViewCell.identifier, for: indexPath)
        
        let keyCell = cell as? ConsumerKeysTableViewCell
        keyCell?.configure(with: KeyManagerItem(key: item, userData: PixUserData(email: "svfvs", cpf: "", phoneNumber: nil)))
        return keyCell
    }
    
    private lazy var tableViewDataSource: TableViewDataSource<Int, PixConsumerKey> = {
        let dataSource = TableViewDataSource<Int, PixConsumerKey>(view: tableView)
        dataSource.itemProvider = itemProvider
        dataSource.add(section: 0)
        return dataSource
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Sizing.base08
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.delegate = self
        tableView.register(ConsumerKeysTableViewCell.self, forCellReuseIdentifier: ConsumerKeysTableViewCell.identifier)
        return tableView
    }()
    
    private lazy var newKeyButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.ConsumerKeys.registerKeyButton, for: .normal)
        button.addTarget(self, action: #selector(tapNewKeyButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var faqButton: UIBarButtonItem = {
        let button = UIButton()
        button.setImage(Resources.Icons.icoQuestion.image, for: .normal)
        button.addTarget(self, action: #selector(didTapInfoButton), for: .touchUpInside)
        return UIBarButtonItem(customView: button)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = tableViewDataSource
        
        tableViewDataSource.add(
            items: [
                PixConsumerKey(type: .registeredKey, id: "1234", attributes: .init(statusSlug: .automaticConfirmed, keyType: .cpf, status: "", name: "", keyValue: "123456789", createdAt: "")),
                PixConsumerKey(type: .registeredKey, id: "12345", attributes: .init(statusSlug: .automaticConfirmed, keyType: .email, status: "", name: "", keyValue: "aa@aa.com", createdAt: "")),
                PixConsumerKey(type: .registeredKey, id: "123456", attributes: .init(statusSlug: .automaticConfirmed, keyType: .phone, status: "", name: "", keyValue: "118123456789", createdAt: "")),
                PixConsumerKey(type: .registeredKey, id: "45678", attributes: .init(statusSlug: .automaticConfirmed, keyType: .random, status: "", name: "", keyValue: "a1b2c3d4", createdAt: "")),
                PixConsumerKey(type: .registeredKey, id: "56789", attributes: .init(statusSlug: .automaticConfirmed, keyType: .random, status: "", name: "", keyValue: "e5f6g7h8", createdAt: "")),
                PixConsumerKey(type: .registeredKey, id: "78910", attributes: .init(statusSlug: .automaticConfirmed, keyType: .random, status: "", name: "", keyValue: "i9j1k0l1", createdAt: ""))
            ], to: 0)
        tableView.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if tableView.tableHeaderView == nil {
            updateViewHeight(for: headerView)
            tableView.tableHeaderView = headerView
            tableView.layoutIfNeeded()
        }
    }
    override func buildViewHierarchy() {
        view.addSubviews(tableView, newKeyButton)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
        }
        newKeyButton.snp.makeConstraints {
            $0.top.equalTo(tableView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base02)
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        tableView.backgroundColor = Colors.backgroundPrimary.color
        setupNavigationBar()
        title = Strings.ConsumerKeys.title
        extendedLayoutIncludesOpaqueBars = true
    }
}
private extension ConsumerKeysViewController {
    func updateViewHeight(for view: UIView) {
        let expectedSize = view.systemLayoutSizeFitting(
            CGSize(width: tableView.frame.width, height: .leastNonzeroMagnitude),
            withHorizontalFittingPriority: .required,
            verticalFittingPriority: .defaultLow
        )
        if view.frame.size.height != expectedSize.height {
            view.frame.size.height = expectedSize.height
        }
    }
    
    func setupNavigationBar() {
        navigationController?.navigationBar.navigationStyle(LargeNavigationStyle())
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: Strings.General.close, style: .plain, target: self, action: #selector(close))
        navigationItem.rightBarButtonItem = faqButton
    }
    
    @objc
    private func close() {
        interactor.close()
    }
    
    @objc
    private func tapNewKeyButton() {
//    TODO
    }
    
    @objc
    private func didTapInfoButton() {
//    TODO
    }
}

extension ConsumerKeysViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//    TODO
    }
}

// MARK: - ConsumerKeysDisplaying
extension ConsumerKeysViewController: ConsumerKeysDisplaying {
}
