import UIKit

public enum ConsumerKeysFactory {
    public static func make() -> UIViewController {
//        Optei por manter público por enquanto para poder usar no PIXSample
//        TODO: Remover public quando integrar com o app principal pelo flowCoordinator
        let container = DependencyContainer()
        let service: ConsumerKeysServicing = ConsumerKeysService(dependencies: container)
        let coordinator: ConsumerKeysCoordinating = ConsumerKeysCoordinator()
        let presenter: ConsumerKeysPresenting = ConsumerKeysPresenter(coordinator: coordinator)
        let interactor = ConsumerKeysInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = ConsumerKeysViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
