import Foundation

protocol ConsumerKeysPresenting: AnyObject {
    var viewController: ConsumerKeysDisplaying? { get set }
    func didNextStep(action: ConsumerKeysAction)
}

final class ConsumerKeysPresenter {

    private let coordinator: ConsumerKeysCoordinating
    weak var viewController: ConsumerKeysDisplaying?

    init(coordinator: ConsumerKeysCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ConsumerKeysPresenting
extension ConsumerKeysPresenter: ConsumerKeysPresenting {
    func didNextStep(action: ConsumerKeysAction) {
        coordinator.perform(action: action)
    }
}
