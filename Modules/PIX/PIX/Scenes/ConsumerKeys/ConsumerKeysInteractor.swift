import Foundation

protocol ConsumerKeysInteracting: AnyObject {
    func registerKey()
    func close()
    func openFAQ()
}

final class ConsumerKeysInteractor {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let service: ConsumerKeysServicing
    private let presenter: ConsumerKeysPresenting

    init(service: ConsumerKeysServicing, presenter: ConsumerKeysPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - ConsumerKeysInteracting
extension ConsumerKeysInteractor: ConsumerKeysInteracting {
    func registerKey() {
//        TODO
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
    
    func openFAQ() {
//        TODO
    }
}
