import SnapKit
import UI
import UIKit
import AssetsKit

private extension ConsumerKeysTableViewCell.Layout {
    enum Size {
        static let moreButton: CGFloat = Sizing.base05
        static let icon = CGSize(width: Sizing.base06, height: Sizing.base06)
    }
}

final class ConsumerKeysTableViewCell: UITableViewCell {
    fileprivate struct Layout { }
    
    enum Accessibility: String {
        case defaultCell
    }
    
    private lazy var backgroundCellView: UIView = {
        let view = UIView()
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
            .with(\.cornerRadius, .medium)
            .with(\.border, .light(color: .grayscale100()))
        return view
    }()
    
    private lazy var iconLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(IconLabelStyle(type: .medium))
            .with(\.textAlignment, .center)
        label.accessibilityElementsHidden = true
        return label
    }()
    
    private lazy var moreButton: UIImageView = {
        let view = UIImageView(image: Resources.Icons.icoMoreVert.image)
        view.tintColor = Colors.branding600.color
        view.contentMode = .center
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .left)
            .with(\.textColor, Colors.grayscale700.color)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textAlignment, .left)
            .with(\.textColor, Colors.grayscale700.color)
        label.numberOfLines = 1
        return label
    }()
    
    func configure(with keyItem: KeyManagerItem) {
        configureIcon(with: keyItem)
        descriptionLabel.text = keyItem.description
        titleLabel.text = keyItem.title
        moreButton.isHidden = !keyItem.disclosureIndicator
        buildLayout()
    }
}
private extension ConsumerKeysTableViewCell {
    func configureIcon(with keyItem: KeyManagerItem) {
        iconLabel.text = keyItem.icon.rawValue
        iconLabel.textColor = Colors.branding600.color
        if !keyItem.isFilledBackgroundIcon {
            iconLabel.layer.backgroundColor = Colors.backgroundPrimary.change(.dark, to: Colors.grayscale700.lightColor).color.cgColor
        }
    }
}

extension ConsumerKeysTableViewCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(backgroundCellView)
        backgroundCellView.addSubview(titleLabel)
        backgroundCellView.addSubview(descriptionLabel)
        backgroundCellView.addSubview(iconLabel)
        backgroundCellView.addSubview(moreButton)
    }
    
    func setupConstraints() {
        backgroundCellView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.bottom.equalToSuperview().offset(-Spacing.base01)
        }
        
        iconLabel.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.icon)
            $0.top.leading.equalTo(backgroundCellView).offset(Spacing.base01)
            $0.bottom.equalToSuperview().offset(-Spacing.base01)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(backgroundCellView).offset(Spacing.base02)
            $0.leading.equalTo(iconLabel.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalTo(moreButton.snp.leading).offset(-Spacing.base00)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(titleLabel.snp.bottom).priority(.high)
            $0.bottom.equalTo(iconLabel).offset(-Spacing.base00)
            $0.leading.equalTo(iconLabel.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalTo(moreButton.snp.leading).offset(-Spacing.base00)
        }
        
        moreButton.snp.makeConstraints {
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.centerY.equalTo(iconLabel)
            $0.height.width.equalTo(Layout.Size.moreButton)
        }
    }
    
    func configureViews() {
        backgroundColor = .clear
        selectionStyle = .none
        accessibilityIdentifier = Accessibility.defaultCell.rawValue
        selectionStyle = .none
        accessibilityTraits = .allowsDirectInteraction
    }
}
