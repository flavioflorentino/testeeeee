import Foundation
import UI
import UIKit
import SnapKit

final class ConsumerKeysHeaderView: UIView, ViewConfiguration {    
    private lazy var headerLabel: UILabel = {
        let headerLabel = UILabel()
        headerLabel.labelStyle(BodyPrimaryLabelStyle())
            .with(\.text, Strings.ConsumerKeys.Header.description)
            .with(\.textColor, Colors.grayscale600.color)
        return headerLabel
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(headerLabel)
    }
    
    func setupConstraints() {
        headerLabel.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base02)
        }
    }
}
