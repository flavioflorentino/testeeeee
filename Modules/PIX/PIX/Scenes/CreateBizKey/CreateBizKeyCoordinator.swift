import UIKit

protocol CreateBizKeyCoordinating: AnyObject {
    func perform(action: CreateBizKeyOutputAction)
}

final class CreateBizKeyCoordinator {
    private let createBizKeyOutput: CreateBizKeyOutput
    
    init(createBizKeyOutput: @escaping CreateBizKeyOutput) {
        self.createBizKeyOutput = createBizKeyOutput
    }
}

// MARK: - CreateBizKeyCoordinating
extension CreateBizKeyCoordinator: CreateBizKeyCoordinating {
    func perform(action: CreateBizKeyOutputAction) {
        createBizKeyOutput(action)
    }
}
