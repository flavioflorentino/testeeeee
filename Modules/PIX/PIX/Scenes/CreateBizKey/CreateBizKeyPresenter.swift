import Foundation
import AssetsKit
import Core

protocol CreateBizKeyPresenting: AnyObject {
    var viewController: CreateBizKeyDisplaying? { get set }
    func startLoading()
    func stopLoading()
    func didNextStep(action: CreateBizKeyOutputAction)
    func presentGenericError()
    func presentNotRegisteredKeyError()
    func presentClaimFlow(createKey: CreateKey)
    func presentPortabilityFlow(createKey: CreateKey)
}

final class CreateBizKeyPresenter {
    private let coordinator: CreateBizKeyCoordinating
    weak var viewController: CreateBizKeyDisplaying?
    
    init(coordinator: CreateBizKeyCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - CreateBizKeyPresenting
extension CreateBizKeyPresenter: CreateBizKeyPresenting {
    func presentClaimFlow(createKey: CreateKey) {
        coordinator.perform(action: .claim(key: createKey))
    }
    
    func presentPortabilityFlow(createKey: CreateKey) {
        coordinator.perform(action: .portability(key: createKey))
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }

    func present(createKey: CreateKey, error: ApiError?) {
        handleError(error: error, createKey: createKey)
    }
    
    func didNextStep(action: CreateBizKeyOutputAction) {
        coordinator.perform(action: action)
    }
    
    func presentGenericError() {
        viewController?.display(error: createGenericError())
    }
    
    func presentNotRegisteredKeyError() {
        viewController?.display(error: createNotRegisteredKeyError())
    }
}

private extension CreateBizKeyPresenter {
    func handleError(error: ApiError?, createKey: CreateKey) {
        guard case let .badRequest(body: body) = error, let requestError = body.errors.first else {
            viewController?.display(error: createGenericError())
            return
        }
        
        switch requestError.code {
        case BadRequestError.claim.rawValue:
            coordinator.perform(action: .claim(key: createKey))
        case BadRequestError.portability.rawValue:
            coordinator.perform(action: .portability(key: createKey))
        default:
            viewController?.display(error: createGenericError())
        }
    }
    
    func createGenericError() -> CreateBizKeyViewModel {
         CreateBizKeyViewModel(image: Resources.Illustrations.iluConstruction.image,
                               title: Strings.KeyManager.error,
                               description: Strings.KeyManager.errorDescription,
                               buttonTitle: Strings.KeyManager.errorAction)
     }
    
    func createNotRegisteredKeyError() -> CreateBizKeyViewModel {
         CreateBizKeyViewModel(image: Resources.Illustrations.iluPeople.image,
                               title: Strings.KeyManager.keyNotRegistered,
                               description: Strings.KeyManager.Consumer.Error.Request.description,
                               buttonTitle: Strings.KeyManager.Consumer.Error.Button.title)
     }
}
