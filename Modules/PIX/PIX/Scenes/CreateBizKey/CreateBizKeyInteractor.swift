import AnalyticsModule
import Core
import Foundation

enum BadRequestError: String {
    case claim = "claim_required"
    case portability = "portability_required"
    case none
}

public protocol CreateBizKeyInteracting: AnyObject {
    func createKey()
    func didPresentScreen()
    func dismiss()
}

final class CreateBizKeyInteractor {
    typealias Dependencies = HasAnalytics & HasBizAuth
    private let dependencies: Dependencies
    
    private let service: PixBizKeyServicing
    private let presenter: CreateBizKeyPresenting
    private let userInfo: KeyManagerBizUserInfo
    private let key: CreateKey
    private let authenticationHash: String?
    
    init(_ userInfo: KeyManagerBizUserInfo, 
         dependencies: Dependencies,
         service: PixBizKeyServicing,
         presenter: CreateBizKeyPresenting,
         key: CreateKey,
         authenticationHash: String?) {
        self.service = service
        self.presenter = presenter
        self.key = key
        self.authenticationHash = authenticationHash
        self.dependencies = dependencies
        self.userInfo = userInfo
    }
}

// MARK: - CreateBizKeyInteracting
extension CreateBizKeyInteractor: CreateBizKeyInteracting {
    func didPresentScreen() {
        let event = KeyBizTracker(companyName: userInfo.name, eventType: .agreementViewed)
        dependencies.analytics.log(event)
    }
    
    func createKey() {
        let event = KeyBizTracker(companyName: userInfo.name, eventType: .agreementDone)
        dependencies.analytics.log(event)
        
        guard let hash = authenticationHash else {
            authenticateUser()
            return
        }
        
        createKey(withPin: hash)
    }
    
    func dismiss() {
        presenter.didNextStep(action: .dismiss)
    }
}

private extension CreateBizKeyInteractor {
    func handleSuccess(with status: PixKeyStatus, and value: String, pixKey: PixKey) {
        switch status {
        case .processed:
            presenter.didNextStep(action: .processed(keyValue: value, pixKey: pixKey))
        default:
            presenter.didNextStep(action: .pending(pixKey: pixKey))
        }
    }
    
    func handleError(error: ApiError?) {
        guard case let .badRequest(body: body) = error, let requestError = body.errors.first else {
            presenter.presentGenericError()
            return
        }
        
        switch requestError.code {
        case BadRequestError.claim.rawValue:
            sendEvent(eventType: .claim(method: key.type))
            presenter.presentClaimFlow(createKey: key)
        case BadRequestError.portability.rawValue:
            sendEvent(eventType: .portability(method: key.type))
            presenter.presentPortabilityFlow(createKey: key)
        default:
            presenter.presentGenericError()
        }
    }
    
    func authenticateUser() {
        guard let businessAuthContract = dependencies.businessAuthContract else {
            presenter.presentGenericError()
            return
        }
        businessAuthContract.performActionWithAuthorization(nil) { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .canceled:
                break
            case let .success(pin):
                self.createKey(withPin: pin)
            case .failure:
                self.presenter.presentGenericError()
            }
        }
    }
    
    func createKey(withPin pin: String) {
        presenter.startLoading()
        service.createNewPixKey(key: key, pin: pin) { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case let .success(infoKey):
                guard let key = infoKey.data.registeredKeys.first else {
                    self.presenter.didNextStep(action: .error)
                    return
                }
                let event = KeyBizTracker(
                    companyName: self.userInfo.name,
                    eventType: .agreementSendValidationStatus(method: key.keyType,
                                                              status: key.statusSlug))
                self.dependencies.analytics.log(event)
                self.getKeyInfo(key.id)
            case .failure:
                self.presenter.stopLoading()
                self.presenter.presentNotRegisteredKeyError()
            }
        }
    }
    
    private func getKeyInfo(_ key: String) {
        service.getKeysInfos(key: key) { [weak self] result in
            guard let self = self else { return }
            self.presenter.stopLoading()
            switch result {
            case let .success(infoKey):
                guard let key = infoKey.data.registeredKeys.first else {
                    self.presenter.didNextStep(action: .error)
                    return
                }
                self.handleSuccess(with: key.statusSlug, and: key.keyValue, pixKey: infoKey.data)
            case .failure:
                self.presenter.presentNotRegisteredKeyError()
            }
        }
    }
    
    private func sendEvent(eventType: KeyBizEvent) {
        let event = KeyBizTracker(companyName: userInfo.name, eventType: eventType)
        dependencies.analytics.log(event)
    }
}
