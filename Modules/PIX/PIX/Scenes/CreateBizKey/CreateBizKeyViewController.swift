import UI
import UIKit
import AssetsKit

protocol CreateBizKeyDisplaying: AnyObject {
    func startLoading()
    func stopLoading()
    func display(error: CreateBizKeyViewModel)
}

private extension CreateBizKeyViewController.Layout {
    enum Size {
        static let image = CGSize(width: 165, height: 170)
        static let spacing: CGFloat = 20
    }
}

public final class CreateBizKeyViewController: ViewController<CreateBizKeyInteracting, UIView> {
    fileprivate enum Layout { }
    
    enum Accessibility: String {
        case confirmButton
    }
    
    private lazy var illustrationImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Resources.Illustrations.iluSecurity.image
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        label.text = Strings.CreateBizKey.title
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        label.text = Strings.CreateBizKey.description
        return label
    }()

    private lazy var numberedList: NumberedDottedList = {
        let listNames = [
            Strings.CreateBizKey.Item.first,
            Strings.CreateBizKey.Item.second,
            Strings.CreateBizKey.Item.third
        ]
        let list = NumberedDottedList(with: listNames, spacing: Layout.Size.spacing)
        return list
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.accessibilityIdentifier = Accessibility.confirmButton.rawValue
        button.setTitle(Strings.General.gotIt, for: .normal)
        button.addTarget(self, action: #selector(createKey), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()

    override public func viewDidLoad() {
        super.viewDidLoad()
        interactor.didPresentScreen()
    }

    override public func buildViewHierarchy() {
        view.addSubviews(illustrationImageView, titleLabel, descriptionLabel, numberedList, confirmButton)
    }
    
    override public func setupConstraints() {
        illustrationImageView.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base06)
            $0.size.equalTo(Layout.Size.image)
            $0.centerX.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(illustrationImageView.snp.bottom).offset(Spacing.base08)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        numberedList.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        confirmButton.snp.makeConstraints {
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Sizing.base06)
        }
    }

    override public func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    @objc
    private func createKey() {
        interactor.createKey()
    }
}

extension CreateBizKeyViewController: StatefulTransitionViewing {
    public func didTryAgain() {
        interactor.createKey()
    }
}

// MARK: - CreateBizKeyDisplaying
extension CreateBizKeyViewController: CreateBizKeyDisplaying {
    func startLoading() {
        beginState(model: StateLoadingViewModel(message: Strings.KeyManager.loading))
    }
    
    func stopLoading() {
        endState()
    }
    
    func display(error: CreateBizKeyViewModel) {
        let content = (title: error.title, description: error.description)
        let button = (image: UIImage(), title: error.buttonTitle)
        let error = StatefulErrorViewModel(image: error.image, content: content, button: button)
        
        endState {
            DispatchQueue.main.async {
                self.endState(model: error)
            }
        }
    }
}
