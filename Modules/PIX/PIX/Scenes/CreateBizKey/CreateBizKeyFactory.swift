import UIKit

public typealias CreateBizKeyOutput = (CreateBizKeyOutputAction) -> Void

public enum CreateBizKeyOutputAction: Equatable {
    case pending(pixKey: PixKey)
    case processed(keyValue: String, pixKey: PixKey)
    case claim(key: CreateKey)
    case portability(key: CreateKey)
    case error
    case dismiss
}

public enum CreateBizKeyFactory {
    public static func make(with userInfo: KeyManagerBizUserInfo, key: CreateKey, authenticationHash: String?, createKeyOutput: @escaping CreateBizKeyOutput) -> CreateBizKeyViewController {
        let container = DependencyContainer()
        let service: PixBizKeyServicing = PixBizKeyService(dependencies: container)
        let coordinator: CreateBizKeyCoordinating = CreateBizKeyCoordinator(createBizKeyOutput: createKeyOutput)
        let presenter: CreateBizKeyPresenting = CreateBizKeyPresenter(coordinator: coordinator)
        let interactor = CreateBizKeyInteractor(userInfo, 
                                                dependencies: container,
                                                service: service,
                                                presenter: presenter,
                                                key: key,
                                                authenticationHash: authenticationHash)
        let viewController = CreateBizKeyViewController(interactor: interactor)

        presenter.viewController = viewController

        return viewController
    }
}
