import Core

enum PixKeyManagerConsumerEndpoint {
    case getConsumerData(_ userId: Int)
    case getIdValidationStatus(userId: Int)
}

extension PixKeyManagerConsumerEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .getConsumerData(let userId):
            return "consumers/\(userId)"
        case .getIdValidationStatus:
            return "verifications/identity/status?isVerified=true"
        }
    }
    
    var method: HTTPMethod {
        .get
    }
    
    var customHeaders: [String: String] {
        guard case .getIdValidationStatus(let userId) = self else {
            return [:]
        }
        return ["consumer_id": String(userId), "flow": "PIX"]
    }
}
