import Core

public enum PixKeysUserType: String, Codable {
    case person
    case company
}

enum PixBizKeyServiceEndpoint {
    case getKeysInfos(key: String)
    case keys(_ userId: String, _ type: PixKeysUserType)
    case createKey(key: CreateKey, pin: String)
    case claim(claimKey: ClaimKey)
    case confirmClaim(userId: String, uuid: String, pin: String)
    case completeClaim(uuid: String, hash: String)
    case deleteKey(uuid: String, type: PixKeysUserType, pin: String)
    case cancelClaim(key: String, hash: String?)
}

// MARK: - ApiEndpointExposable
extension PixBizKeyServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .createKey, .keys:
            return "/keys"
        case .claim:
            return "/claims"
        case .confirmClaim:
            return "/claims/confirm"
        case let .deleteKey(uuid, _, _):
            return "/keys/\(uuid)"
        case .completeClaim:
            return "/claims/complete"
        case .cancelClaim:
            return "/claims/cancel"
        case let .getKeysInfos(key):
            return "/keys/\(key)"
        }
    }
    
    var shouldAppendBody: Bool {
        true
    }
    
    var body: Data? {
        switch self {
        case let .createKey(key, _):
            var dict: [String: Any] = [
                "type": key.type.rawValue,
                "userId": key.userId,
                "userType": key.userType.rawValue,
                "value": key.value
            ]
            if let name = key.name {
                dict["name"] = name
            }
            return dict.toData()
        case let .claim(claimKey):
            return prepareBody(with: claimKey, strategy: .useDefaultKeys)
        case let .confirmClaim(userId, uuid, _):
            return [
                "userId": "\(userId)",
                "keyId": "\(uuid)"
            ].toData()
        case let .deleteKey(_, type, _):
            return [
                "reason": "client_request",
                "userType": type.rawValue
            ].toData()
        case let .completeClaim(keyId, _), let .cancelClaim(keyId, _):
            return ["keyId": keyId].toData()
        default:
            return nil
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .createKey, .claim, .confirmClaim, .completeClaim, .cancelClaim:
            return .post
        case .deleteKey:
            return .delete
        default:
            return .get
        }
    }

    var customHeaders: [String: String] {
        var headers: [String: String] = [:]
        switch self {
        case let .confirmClaim(_, _, pin),
             let .createKey(_, pin),
             let .completeClaim(_, pin),
             let .deleteKey(_, _, pin):
            
            headers = headerWith(pin: pin)
        case let .cancelClaim(_, hash):
            if let pin = hash {
                headers = headerWith(pin: pin)
            }
        default:
            break
        }
        return headers
    }
}
