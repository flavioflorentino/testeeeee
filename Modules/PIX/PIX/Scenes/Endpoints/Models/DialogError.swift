import Core

enum DialogError: Error {
    case `default`(ApiError)
    case dialog(DialogResponseError)
    case message(MessageResponseError)
    
    static func transformApiError(_ error: ApiError) -> DialogError {
        guard let requestError = error.requestError else {
            return .default(error)
        }
        if let data = requestError.dictionaryObject["data"] as? [String: String], let buttonText = data["button_text"] {
            let dialogModel = DialogErrorModel(
                title: requestError.title,
                message: requestError.message,
                buttonText: buttonText
            )
            return .dialog(DialogResponseError(code: requestError.code, model: dialogModel))
        } else {
            let message = MessageResponseError(code: requestError.code, message: requestError.message)
            return .message(message)
        }
    }
}

struct DialogResponseError {
    let code: String
    let model: DialogErrorModel
}

struct MessageResponseError {
    let code: String
    let message: String
}

struct DialogErrorModel {
    let title: String
    let message: String
    let buttonText: String
}
