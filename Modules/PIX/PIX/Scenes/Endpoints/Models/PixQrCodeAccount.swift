import Foundation

public struct PixQrCodeAccount: Decodable, Equatable {
    public let name: String
    public let cpfCnpj: String
    public let bank: String
    public let imageUrl: String?
    public let id: String?
    public let qrcodeInfos: PixQRCodeInfo
    public let value: Double
    public let editable: Bool

    private enum CodingKeys: String, CodingKey {
        case name = "userName"
        case cpfCnpj
        case bank
        case imageUrl
        case id
        case qrcodeInfos
        case value
        case editable
    }
}

public struct PixQRCodeInfo: Decodable, Equatable {
    public let payerData: [QRCodeAcessoryPaymentModel]
    public let receiverData: [QRCodeAcessoryPaymentModel]?
}
