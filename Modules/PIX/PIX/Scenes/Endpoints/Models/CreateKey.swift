public struct CreateKey: Equatable {
    let userId: String
    let userType: PixKeysUserType
    let value: String
    let type: PixKeyType
    let name: String?
    
    public init(userId: String, userType: PixKeysUserType, value: String, type: PixKeyType, name: String? = nil) {
        self.userId = userId
        self.userType = userType
        self.value = value
        self.type = type
        self.name = name
    }
}
