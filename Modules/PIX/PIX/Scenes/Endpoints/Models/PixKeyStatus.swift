public enum PixKeyStatus: String, Decodable {
    case processed
    case inactive
    case processPending = "process_pending"
    case deleteFromBacen = "delete_from_bacen"
    case deletedFromBacen = "deleted_from_bacen"
    case claimInProgress = "claim_in_progress"
    case claimNeedsValidation = "claim_needs_validation"
    case portabilityInProgress = "portability_in_progress"
    case awaitingPortabilityConfirm = "awaiting_portability_confirm"
    case awaitingClaimConfirm = "awaiting_claim_confirm"
    case awaitingClaimComplete = "awaiting_claim_complete"
    case awaitingPortabilityComplete = "awaiting_portability_complete"
    case automaticConfirmed = "automatic_confirmed"
    case updateRegistration = "update_registration"
    case preRegistered = "pre_registered"
    case inactiveButCanRevalidate = "inactive_but_can_revalidate"
    case receiverClaimCompleted = "receiver_claim_completed"
    case donorClaimConfirmed = "donor_claim_confirmed"
    case donorPortabilityConfirmed = "donor_portability_confirmed"
}
