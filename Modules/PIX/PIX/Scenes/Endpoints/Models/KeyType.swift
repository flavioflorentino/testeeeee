import Foundation
import UI
import UIKit

public enum KeyType: Int, CaseIterable, Decodable {
    case phone
    case email
    case cpf
    case random
    case cnpj

    var serviceValue: String {
        switch self {
        case .cpf:
            return "CPF"
        case .cnpj:
            return "CNPJ"
        case .phone:
            return "PHONE_NUMBER"
        case .email:
            return "EMAIL"
        case .random:
            return "RANDOM"
        }
    }
}

public enum PixKeyType: String, Codable {
    case cpf
    case cnpj
    case phone
    case email
    case random
    
    var keyButtonIndex: Int {
        switch self {
        case .cpf:
            return 0
        case .cnpj:
            return 1
        case .phone:
            return 2
        case .email:
            return 3
        case .random:
            return 4
        }
    }
    
    var infoMessage: String {
        switch self {
        case .cpf:
            return Strings.KeyManager.Consumer.KeyType.cpf
        case .cnpj:
            return Strings.KeyManager.cnpjCell
        case .phone:
            return Strings.KeyManager.Consumer.KeyType.phone
        case .email:
            return Strings.KeyManager.Consumer.KeyType.email
        case .random:
            return Strings.KeyManager.Consumer.KeyType.random
        }
    }
}
