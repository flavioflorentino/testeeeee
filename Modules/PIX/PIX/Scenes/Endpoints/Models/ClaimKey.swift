public struct ClaimKey: Equatable, Codable {
    let userId: String
    let userType: PixKeysUserType
    let keyValue: String
    let keyType: PixKeyType
    let type: ClaimKeyType
    
    public init(userId: String, userType: PixKeysUserType, keyValue: String, keyType: PixKeyType, type: ClaimKeyType) {
        self.userId = userId
        self.userType = userType
        self.keyValue = keyValue
        self.keyType = keyType
        self.type = type
    }
}

public enum ClaimKeyType: String, Codable {
    case portability = "portability"
    case claim = "possession_claim"
}
