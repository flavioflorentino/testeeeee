public enum PixKeySection: String, Decodable, Equatable {
    case registeredKey = "key"
    case unregisteredKey = "unregistered_key"
}
