import Core
import Foundation

struct UnregisteredKey: Equatable {
    let keyType: PixKeyType
}

public struct RegisteredKey: Equatable {
    let id: String
    let statusSlug: PixKeyStatus
    let status: String
    let name: String
    let keyType: PixKeyType
    var keyValue: String
}

public struct PixKeyDataList: Decodable {
    public let data: [PixKey]
}

public struct PixKeyData: Decodable {
    let data: PixKey
}

public struct PixKey: Decodable, Equatable {
    // PixKey can only represent a registered or unregistered key, never both at the same time. A key will be parsed
    // and added in only one of the arrays. So unregisteredKeys and registeredKeys can have, each one, only 0 or 1 elements.
    var unregisteredKeys: [UnregisteredKey] = []
    public var registeredKeys: [RegisteredKey] = []
    private let type: PixKeySection
    
    enum CodingKeys: String, CodingKey {
        case type, id, statusSlug, status, name, keyType, keyValue, attributes
    }
    
    public init(type: PixKeySection) {
        self.type = type
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let attributesContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .attributes)
        type = try container.decode(PixKeySection.self, forKey: .type)
        
        switch type {
        case .registeredKey:
            let id = try container.decode(String.self, forKey: .id)
            let statusSlug = try attributesContainer.decode(PixKeyStatus.self, forKey: .statusSlug)
            let status = try attributesContainer.decode(String.self, forKey: .status)
            let name = try? attributesContainer.decode(String.self, forKey: .name)
            let keyType = try attributesContainer.decode(PixKeyType.self, forKey: .keyType)
            let keyValue = try? attributesContainer.decode(String.self, forKey: .keyValue)
            registeredKeys.append(RegisteredKey(id: id, statusSlug: statusSlug, status: status, name: name ?? "", keyType: keyType, keyValue: keyValue ?? ""))
        case .unregisteredKey:
            let keyType = try attributesContainer.decode(PixKeyType.self, forKey: .keyType)
            unregisteredKeys.append(UnregisteredKey(keyType: keyType))
        }
    }
}
