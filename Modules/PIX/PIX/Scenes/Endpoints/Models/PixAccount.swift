import Foundation

public struct PixAccount: Decodable, Equatable {
    public let name: String
    public let cpfCnpj: String
    public let bank: String
    public let agency: String?
    public let accountNumber: String?
    public let imageUrl: String?
    public let id: String?

    private enum CodingKeys: String, CodingKey {
        case name = "userName"
        case cpfCnpj
        case bank
        case agency = "branchNumber"
        case accountNumber
        case imageUrl
        case id
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        cpfCnpj = try container.decodeIfPresent(String.self, forKey: .cpfCnpj) ?? ""
        bank = try container.decodeIfPresent(String.self, forKey: .bank) ?? ""
        agency = try container.decodeIfPresent(String.self, forKey: .agency)
        accountNumber = try container.decodeIfPresent(String.self, forKey: .accountNumber)
        imageUrl = try container.decodeIfPresent(String.self, forKey: .imageUrl)
        id = try container.decodeIfPresent(String.self, forKey: .id)
    }
    
    public init(name: String,
                cpfCnpj: String,
                bank: String,
                agency: String?,
                accountNumber: String?,
                imageUrl: String?,
                id: String?) {
        self.name = name
        self.cpfCnpj = cpfCnpj
        self.bank = bank
        self.agency = agency
        self.accountNumber = accountNumber
        self.imageUrl = imageUrl
        self.id = id
    }
}
