public struct PixQRCode: Decodable, Equatable {
    let qrCodeInBase64: String
    let qrCodeText: String
    let keyValue: String
    let keyType: PixKeyType?
    let description: String?
    let identifier: String?
    let value: String?
    
    private enum CodingKeys: String, CodingKey {
        case qrCodeInBase64, qrCodeText, keyValue, keyType, identifier, data
        case description = "additionalData"
        case value = "amount"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let dataContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        let base64 = try dataContainer.decode(String.self, forKey: .qrCodeInBase64)
        qrCodeInBase64 = base64.replacingOccurrences(of: "\\", with: "")
        qrCodeText = try dataContainer.decode(String.self, forKey: .qrCodeText)
        keyValue = try dataContainer.decode(String.self, forKey: .keyValue)
        value = try dataContainer.decodeIfPresent(String.self, forKey: .value)
        description = try dataContainer.decodeIfPresent(String.self, forKey: .description)
        keyType = try dataContainer.decodeIfPresent(PixKeyType.self, forKey: .keyType)
        identifier = try dataContainer.decodeIfPresent(String.self, forKey: .identifier)
    }
}
