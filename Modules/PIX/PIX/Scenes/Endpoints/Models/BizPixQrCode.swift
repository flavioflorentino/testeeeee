struct BizPixQrCode: Decodable {
    let qrCodePayload: String
    let qrCodeType: String?
    let endToEndId: String?
    let staticQrCode: BizPixStaticQrCode?
    
    private enum CodingKeys: String, CodingKey {
        case qrCodePayload, qrCodeType, endToEndId
        case staticQrCode = "dadosQrCodeEstatico"
    }
}
