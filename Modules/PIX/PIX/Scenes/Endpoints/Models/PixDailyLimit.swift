import Foundation

struct PixDailyLimit: Decodable, Equatable {
    let current: Double
    let max: Double
    let usedToday: Double
}
