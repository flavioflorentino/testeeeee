import Core
import Foundation

public struct ReceiptResponse: Decodable, Equatable {
    public let data: ReceiptData
}

public struct ReceiptData: Decodable, Equatable {
    public let transactionId: String
    public let receipt: [[AnyHashable: Any]]
    
    private enum CodingKeys: String, CodingKey {
        case receipt
        case transactionId
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let data = try container.decode(AnyCodable.self, forKey: .receipt)
        
        transactionId = ReceiptData.getTransaction(container: container)
        receipt = data.value as? [[AnyHashable: Any]] ?? []
    }
    
    public static func == (lhs: ReceiptData, rhs: ReceiptData) -> Bool {
        lhs.transactionId == rhs.transactionId
    }
    
    private static func getTransaction(container: KeyedDecodingContainer<CodingKeys>) -> String {
        do {
            let transactionId = try container.decode(Int.self, forKey: .transactionId)
            return "\(transactionId)"
        } catch {
            do {
                return try container.decode(String.self, forKey: .transactionId)
            } catch {
                return ""
            }
        }
    }
}
