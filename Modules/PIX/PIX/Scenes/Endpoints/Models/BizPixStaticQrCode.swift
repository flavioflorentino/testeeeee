struct BizPixStaticQrCode: Decodable {
    let qrCodeFormat: Double?
    let accountType: String?
    let accountNumber: String?
    let key: String?
    let value: Double?
    let receiverName: String?
    let personType: String?
    let cpfCnpj: String?
    let city: String?
    let cep: String?
    let idReceiverConciliation: String?
    let additionalData: String?
}
