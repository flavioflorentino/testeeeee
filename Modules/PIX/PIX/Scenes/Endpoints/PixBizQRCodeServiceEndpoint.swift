import Core

enum PixBizQRCodeServiceEndpoint {
    case generateQRCode(key: RegisteredKey, amount: Double?, description: String?, identifier: String?)
    case decodeQRCode(qrCode: String)
}

extension PixBizQRCodeServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .generateQRCode:
            return "/brcode/static"
        case .decodeQRCode:
            return "/brcode/decode"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .decodeQRCode:
            return .get
        case .generateQRCode:
            return .post
        }
    }
    
    var body: Data? {
        switch self {
        case let .decodeQRCode(qrCode):
            return ["qrCodePayload": qrCode].toData()
        case let .generateQRCode(key, amount, description, identifier):
            var dict: [String: Any] = [
                "keyValue": key.keyValue,
                "keyType": key.keyType.rawValue
            ]
            if let doubleAmount = amount {
                dict["amount"] = doubleAmount
            }
            if let descriptionString = description {
                dict["additionalData"] = descriptionString
            }
            if let identifierString = identifier {
                dict["identifier"] = identifierString
            }
            return dict.toData()
        }
    }
}
