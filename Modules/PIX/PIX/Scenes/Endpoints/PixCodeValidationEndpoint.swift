import Core

enum PixCodeValidationEndpoint: ApiEndpointExposable {
    case validateCode(SecurityCodeValidationRequestModel)
    case resendCode(SecurityCodeResendRequestModel)

    var path: String {
        switch self {
        case .validateCode:
            return "consumers/contact-confirmation/check-confirmation-code"
        case .resendCode:
            return "consumers/contact-confirmation/new-confirmation-code"
        }
    }

    var body: Data? {
        switch self {
        case let .validateCode(codeModel):
            return prepareBody(with: codeModel)
        case let .resendCode(itemTypeModel):
            return prepareBody(with: itemTypeModel)
        }
    }
    
    var method: HTTPMethod {
        .post
    }
}
