import Core

enum PixServiceEndpoint: ApiEndpointExposable {
    case keyValidator(key: String, type: KeyType)
    case manualInsertion(params: PIXParams)
    case receipt(transactionId: String)
    case receiptBiz(transactionId: String)
    case receiverReceiptPF(transactionId: String)
    case qrCode(scannedText: String)
    case newReceiptBiz(movementCode: Int, transactionId: String)

    var path: String {
        switch self {
        case .keyValidator, .manualInsertion, .qrCode:
            return "dict-verifier/key"
        case let .receipt(transactionId):
            return "pix-transaction/receipt/\(transactionId)"
        case let .receiptBiz(transactionId):
            return "/pix-transactions/receipt/\(transactionId)"
        case let .receiverReceiptPF(transactionId):
            return "/pix-receivement/transaction/receipt/\(transactionId)"
        case let .newReceiptBiz(movementCode, transactionId):
            return "/pix-transactions/receipt/\(movementCode)/\(transactionId)"
        }
    }

    var body: Data? {
        switch self {
        case let .keyValidator(key, type):
            return [
                "key_type": type.serviceValue,
                "key": key
            ].toData()
        case let .manualInsertion(params):
            let encoder = JSONEncoder()
            encoder.keyEncodingStrategy = .convertToSnakeCase
            return try? encoder.encode(params)
        case let .qrCode(scannedText):
            return [
                "key_type": "QRCODE",
                "key": scannedText
            ].toData()
        default:
            return nil
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .receipt, .receiptBiz, .receiverReceiptPF, .newReceiptBiz:
            return .get
        case .qrCode:
            return .put
        default:
            return .post
        }
    }
}
