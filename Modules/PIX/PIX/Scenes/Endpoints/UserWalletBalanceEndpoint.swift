import Core

enum UserWalletBalanceEndpoint {
    case getBalance
}

extension UserWalletBalanceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .getBalance:
            return "/seller/wallet-balance"
        }
    }
    
    var method: HTTPMethod {
        .get
    }
}
