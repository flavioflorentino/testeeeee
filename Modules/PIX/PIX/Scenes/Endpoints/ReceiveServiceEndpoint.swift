import Core

enum ReceiveServiceEndpoint {
    case keys(_ userId: String, _ type: PixKeysUserType)
    case generateQRCode(request: ReceiveQRCodeRequest)
}

extension ReceiveServiceEndpoint: ApiEndpointExposable {
    var method: HTTPMethod {
        switch self {
        case .generateQRCode:
            return .post
        default:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .generateQRCode:
            return "pix-receivement/qrcode/generate"
        case .keys:
            return "keys"
        }
    }
    
    var body: Data? {
        switch self {
        case let .generateQRCode(params):
            let encoder = JSONEncoder()
            encoder.keyEncodingStrategy = .convertToSnakeCase
            return try? encoder.encode(params)
        default:
            return nil
        }
    }
}

struct ReceiveQRCodeRequest: Encodable {
    let key: String
    var identifier: String?
    var description: String?
    var amount: Double?
    
    init(registeredKey: RegisteredKey) {
        if registeredKey.keyType == .phone {
            self.key = "+55\(registeredKey.keyValue)"
        } else {
            self.key = registeredKey.keyValue
        }
    }
}

struct ReceiveQRCodeResponse: Decodable {
    let qrCodeInBase64: String
    let qrCodeText: String
}
