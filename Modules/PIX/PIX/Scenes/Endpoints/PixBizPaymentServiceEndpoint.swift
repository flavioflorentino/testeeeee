import Core

enum PixBizPaymentServiceEndpoint: ApiEndpointExposable {
    case keyValidator(key: String, type: KeyType)
    case manualInsertion(params: PIXParams)
    case qrCode(scannedText: String)
    case devolution(transactionId: String)
    case copyPaste(code: String)

    var path: String {
        switch self {
        case let .devolution(transactionId):
            return "/pix-transactions/devolution/\(transactionId)"
        case .manualInsertion, .keyValidator, .qrCode, .copyPaste:
            return "/pix-transactions/payments/key"
        }
    }
    
    var body: Data? {
        switch self {
        case let .keyValidator(key, type):
            return [
                "key_type": type.serviceValue,
                "key": key
            ].toData()
        case let .manualInsertion(params):
            let encoder = JSONEncoder()
            encoder.keyEncodingStrategy = .convertToSnakeCase
            return try? encoder.encode(params)
        case let .qrCode(scannedText):
            return [
                "key_type": "QR_CODE",
                "key": scannedText
            ].toData()
        case let .copyPaste(code):
            return [
                "key_type": "QR_CODE",
                "key": code
            ].toData()
        default:
            return nil
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .qrCode, .copyPaste:
            return .put
        case .devolution:
            return .get
        case .keyValidator, .manualInsertion:
            return .post
        }
    }
}
