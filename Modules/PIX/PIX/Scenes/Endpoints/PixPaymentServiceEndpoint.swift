import Core

public enum PixPaymentServiceEndpoint: ApiEndpointExposable {
    case sendPayment(PIXPaymentParams, PIXParams, password: String)
    case returnPayment(params: PIXReturnParams, password: String)

    public var path: String {
        switch self {
        case .sendPayment:
            return "pix-transaction/consumer/transactions"
        case .returnPayment:
            return "pix-transaction/consumer/transactions/devolution"
        }
    }

    public var body: Data? {
        let jsonEncoder = JSONEncoder()
        jsonEncoder.keyEncodingStrategy = .convertToSnakeCase

        switch self {
        case let .sendPayment(pixPaymentParams, pixParams, _):
            let pixRequestModel = PIXPaymentRequestModel(pixParams: pixParams, params: pixPaymentParams)
            return try? jsonEncoder.encode(pixRequestModel)
        case let .returnPayment(returnParams, _):
            return try? jsonEncoder.encode(returnParams)
        }
    }

    public var method: HTTPMethod {
        .post
    }

    public var customHeaders: [String: String] {
        switch self {
        case let .sendPayment(_, _, password),
             let .returnPayment(_, password):
            return ["password": password]
        }
    }
}
