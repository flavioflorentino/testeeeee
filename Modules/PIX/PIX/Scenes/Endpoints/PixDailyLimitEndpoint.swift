import Core
import Foundation

public enum PixDailyLimitEndpoint: ApiEndpointExposable {
    case getLimit
    case updateLimit(amount: String, password: String)

    public var path: String {
        switch self {
        case .getLimit:
            return "pix-transaction/user/limit"
        case .updateLimit:
            return "pix-transaction/change/limit"
        }
    }

    public var body: Data? {
        switch self {
        case let .updateLimit(amount, _):
            return [
                "limit": amount
            ].toData()
        default:
            return nil
        }
    }
    
    public var customHeaders: [String: String] {
        switch self {
        case let .updateLimit(_, password):
            return [
                "password": password
            ]
        default:
            return [:]
        }
    }

    public var method: HTTPMethod {
        switch self {
        case .getLimit:
            return .get
        case .updateLimit:
            return .post
        }
    }
}
