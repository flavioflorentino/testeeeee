import Core

enum PixConsumerKeyServiceEndpoint {
    case keys(_ userId: String)
    case key(_ keyId: String)
    case createKey(key: CreateKey, password: String)
    case createKeyNonAuthenticated(key: CreateKey)
    case deleteKey(userId: Int, uuid: String, password: String)
    case createClaim(claimKey: ClaimKey)
    case confirmClaimNotAuthenticated(_ keyId: String, userId: Int)
    case confirmClaim(_ keyId: String, userId: Int, password: String)
    case completeClaim(_ keyId: String, userId: Int)
    case cancelClaim(_ keyId: String, userId: Int)
}

// MARK: - ApiEndpointExposable
extension PixConsumerKeyServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .createKey:
            return "pix-keys/keys-create"
        case .keys,
             .createKeyNonAuthenticated:
            return "pix-keys/keys"
        case let .deleteKey(_, uuid, _):
            return "pix-keys/keys-delete/\(uuid)?userType=person"
        case .key(let id):
            return "pix-keys/keys/\(id)"
        case .createClaim:
            return "pix-keys/claims"
        case .confirmClaimNotAuthenticated:
            return "pix-keys/claims/confirm"
        case .confirmClaim:
            return "pix-keys/claims-keys/confirm"
        case .completeClaim:
            return "pix-keys/claims/complete"
        case .cancelClaim:
            return "pix-keys/claims/cancel"
        }
    }
    
    var customHeaders: [String: String] {
        switch self {
        case let .createKey(_, password), let .deleteKey(_, _, password), let .confirmClaim(_, _, password):
            return ["password": password]
        default:
            return [:]
        }
    }
    
    var parameters: [String: Any] {
        guard case let .keys(userId) = self else {
            return [:]
        }
        return ["userId": userId, "userType": "person"]
    }
    
    var body: Data? {
        switch self {
        case let .createKey(key, _),
             let .createKeyNonAuthenticated(key):
            var dict: [String: Any] = [
                "type": key.type.rawValue,
                "userId": key.userId,
                "userType": key.userType.rawValue,
                "value": key.value
            ]
            if let name = key.name {
                dict["name"] = name
            }
            return dict.toData()
        case let .deleteKey(userId, _, _):
            return [
                "userId": "\(userId)",
                "reason": "CLIENT_REQUEST"
            ].toData()
        case let .createClaim(claimKey):
            return [
                "userId": claimKey.userId,
                "userType": claimKey.userType.rawValue,
                "type": claimKey.type.rawValue,
                "keyType": claimKey.keyType.rawValue,
                "keyValue": claimKey.keyValue
            ].toData()
        case let .confirmClaimNotAuthenticated(keyId, userId),
             let .confirmClaim(keyId, userId, _),
             let .completeClaim(keyId, userId),
             let .cancelClaim(keyId, userId):
            return ["userId": "\(userId)", "keyId": "\(keyId)"].toData()
        default:
            return nil
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .createKey,
             .createKeyNonAuthenticated,
             .createClaim,
             .completeClaim,
             .confirmClaimNotAuthenticated,
             .confirmClaim,
             .cancelClaim:
            return .post
        case .deleteKey:
            return .delete
        default:
            return .get
        }
    }
    
    var shouldAppendBody: Bool {
        switch self {
        case .createKey,
             .createKeyNonAuthenticated,
             .deleteKey,
             .createClaim,
             .confirmClaimNotAuthenticated,
             .confirmClaim,
             .completeClaim,
             .cancelClaim:
            return true
        default:
            return false
        }
    }
}
