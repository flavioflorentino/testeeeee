import UIKit

enum HubVersionBAction {
}

protocol HubVersionBCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: HubVersionBAction)
}

final class HubVersionBCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - HubVersionBCoordinating
extension HubVersionBCoordinator: HubVersionBCoordinating {
    func perform(action: HubVersionBAction) {
    }
}
