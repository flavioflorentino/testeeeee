import Foundation

protocol HubVersionBInteracting: AnyObject {
    func doSomething()
}

final class HubVersionBInteractor {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let service: HubVersionBServicing
    private let presenter: HubVersionBPresenting

    init(service: HubVersionBServicing, presenter: HubVersionBPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - HubVersionBInteracting
extension HubVersionBInteractor: HubVersionBInteracting {
    func doSomething() {
        presenter.displaySomething()
    }
}
