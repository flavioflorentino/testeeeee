import UIKit

enum HubVersionBFactory {
    static func make() -> HubVersionBViewController {
        let container = DependencyContainer()
        let service: HubVersionBServicing = HubVersionBService(dependencies: container)
        let coordinator: HubVersionBCoordinating = HubVersionBCoordinator(dependencies: container)
        let presenter: HubVersionBPresenting = HubVersionBPresenter(coordinator: coordinator, dependencies: container)
        let interactor = HubVersionBInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = HubVersionBViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
