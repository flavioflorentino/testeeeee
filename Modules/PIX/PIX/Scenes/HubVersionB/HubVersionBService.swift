import Core
import Foundation

protocol HubVersionBServicing {
}

final class HubVersionBService {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - HubVersionBServicing
extension HubVersionBService: HubVersionBServicing {
}
