import Foundation

protocol HubVersionBPresenting: AnyObject {
    var viewController: HubVersionBDisplaying? { get set }
    func displaySomething()
    func didNextStep(action: HubVersionBAction)
}

final class HubVersionBPresenter {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let coordinator: HubVersionBCoordinating
    weak var viewController: HubVersionBDisplaying?

    init(coordinator: HubVersionBCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - HubVersionBPresenting
extension HubVersionBPresenter: HubVersionBPresenting {
    func displaySomething() {
        viewController?.displaySomething()
    }
    
    func didNextStep(action: HubVersionBAction) {
        coordinator.perform(action: action)
    }
}
