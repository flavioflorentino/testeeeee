import Core
import Foundation

protocol FeedbackCancelContainerServicing {
    var consumerId: Int? { get }
    func cancelClaim(keyId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void)
}

final class FeedbackCancelContainerService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - FeedbackCancelContainerServicing
extension FeedbackCancelContainerService: FeedbackCancelContainerServicing {
    var consumerId: Int? {
        PIXSetup.consumerInstance?.consumerId
    }
    
    func cancelClaim(keyId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        guard let id = consumerId else {
            completion(.failure(.cancelled))
            return
        }
        
        let endpoint = PixConsumerKeyServiceEndpoint.cancelClaim(keyId, userId: id)
        Api<NoContent>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
