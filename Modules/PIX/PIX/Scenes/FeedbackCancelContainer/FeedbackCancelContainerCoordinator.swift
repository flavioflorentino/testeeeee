import UIKit

protocol FeedbackCancelContainterDelegate: AnyObject {
    func didClose(feedbackType: FeedbackViewType)
}

enum FeedbackCancelContainerAction {
    case close(feedbackType: FeedbackViewType?)
    case feedback(feedbackType: FeedbackViewType, from: PixUserNavigation)
}

protocol FeedbackCancelContainerCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: FeedbackCancelContainterDelegate? { get set }
    func perform(action: FeedbackCancelContainerAction)
}

final class FeedbackCancelContainerCoordinator {
    weak var viewController: UIViewController?
    weak var delegate: FeedbackCancelContainterDelegate?
}

// MARK: - FeedbackCancelContainerCoordinating
extension FeedbackCancelContainerCoordinator: FeedbackCancelContainerCoordinating {
    func perform(action: FeedbackCancelContainerAction) {
        switch action {
        case .close(let feedbackType):
            viewController?.dismiss(animated: true)
            guard let currentFeedbackType = feedbackType else { return }
            delegate?.didClose(feedbackType: currentFeedbackType)
        case let .feedback(feedbackType, from):
            let feedbackController = FeedbackFactory.make(with: feedbackType, delegate: self, from: from)
            viewController?.navigationController?.pushViewController(feedbackController, animated: true)
        }
    }
}

extension FeedbackCancelContainerCoordinator: FeedbackDelegate {
    func didAction(action: FeedbackAction, feedbackType: FeedbackViewType) {
        viewController?.dismiss(animated: true)
        delegate?.didClose(feedbackType: feedbackType)
    }
}
