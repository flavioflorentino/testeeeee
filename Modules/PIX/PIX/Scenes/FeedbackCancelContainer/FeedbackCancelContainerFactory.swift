import UIKit

enum FeedbackCancelContainerFactory {
    static func make(
        feedbackType: FeedbackCancelContainerType,
        delegate: FeedbackCancelContainterDelegate? = nil,
        from origin: PixUserNavigation = .undefined
    ) -> FeedbackCancelContainerViewController {
        let container = DependencyContainer()
        let service: FeedbackCancelContainerServicing = FeedbackCancelContainerService(
            dependencies: container
        )
        let coordinator: FeedbackCancelContainerCoordinating = FeedbackCancelContainerCoordinator()
        let presenter: FeedbackCancelContainerPresenting = FeedbackCancelContainerPresenter(
            coordinator: coordinator
        )
        let interactor = FeedbackCancelContainerInteractor(
            service: service,
            presenter: presenter,
            type: feedbackType,
            origin: origin,
            dependencies: container
        )
        let viewController = FeedbackCancelContainerViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
