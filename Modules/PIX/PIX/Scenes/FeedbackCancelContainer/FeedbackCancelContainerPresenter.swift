import Foundation

protocol FeedbackCancelContainerPresenting: AnyObject {
    var viewController: FeedbackCancelContainerDisplaying? { get set }
    func presentFeedback(type: FeedbackViewType, from origin: PixUserNavigation)
    func didNextStep(action: FeedbackCancelContainerAction)
    func presentLoaderView()
    func hideLoaderView()
    func disableCloseButton()
    func enableCloseButton()
    func presentError(title: String, message: String, buttonText: String)
}

final class FeedbackCancelContainerPresenter {
    private let coordinator: FeedbackCancelContainerCoordinating
    weak var viewController: FeedbackCancelContainerDisplaying?

    init(coordinator: FeedbackCancelContainerCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - FeedbackCancelContainerPresenting
extension FeedbackCancelContainerPresenter: FeedbackCancelContainerPresenting {
    func presentFeedback(type: FeedbackViewType, from origin: PixUserNavigation) {
        viewController?.displayFeedbackView(type: type, from: origin)
    }
    
    func didNextStep(action: FeedbackCancelContainerAction) {
        coordinator.perform(action: action)
    }
    
    func presentLoaderView() {
        viewController?.displayLoaderView()
    }
    
    func hideLoaderView() {
        viewController?.hideLoaderView()
    }
    
    func enableCloseButton() {
        viewController?.enableCloseButton()
    }
    
    func disableCloseButton() {
        viewController?.disableCloseButton()
    }
    
    func presentError(title: String, message: String, buttonText: String) {
        viewController?.displayError(title: title, message: message, buttonText: buttonText)
    }
}
