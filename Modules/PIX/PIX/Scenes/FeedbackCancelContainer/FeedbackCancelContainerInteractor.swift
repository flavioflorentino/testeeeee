import AnalyticsModule
import Foundation

protocol FeedbackCancelContainerInteracting: AnyObject {
    func configureContainer()
    func execute(action: FeedbackAction, feedbackType: FeedbackViewType)
    func didClose(feedbackType: FeedbackViewType?)
}

enum FeedbackCancelContainerType: Equatable {
    case claimKeyCancel(uuid: String) // Cancelar a reivindicação da chave
    case portabilityKeyCancel(uuid: String) // Cancelar a portabilidade da chave
}

final class FeedbackCancelContainerInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: FeedbackCancelContainerServicing
    private let presenter: FeedbackCancelContainerPresenting
    private let type: FeedbackCancelContainerType
    private let feedbackOrigin: PixUserNavigation

    init(
        service: FeedbackCancelContainerServicing,
        presenter: FeedbackCancelContainerPresenting,
        type: FeedbackCancelContainerType,
        origin: PixUserNavigation,
        dependencies: Dependencies
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.type = type
        feedbackOrigin = origin
    }
    
    private func getKeyId() -> String? {
        switch type {
        case let .claimKeyCancel(uuid), let .portabilityKeyCancel(uuid):
            return uuid
        }
    }
    
    private func transformFeedbackType(type: FeedbackCancelContainerType) -> FeedbackViewType {
        switch type {
        case .claimKeyCancel(let uuid):
            return .claimKeyCancel(uuid: uuid)
        case .portabilityKeyCancel(let uuid):
            return .portabilityKeyCancel(uuid: uuid)
        }
    }
    
    private func cancel(keyId: String) {
        presenter.presentLoaderView()
        presenter.disableCloseButton()
        
        service.cancelClaim(keyId: keyId) { [weak self] result in
            switch result {
            case .success:
                self?.presentFeedback()
            case .failure:
                self?.presentError()
            }
            
            self?.presenter.hideLoaderView()
            self?.presenter.enableCloseButton()
        }
    }
    
    private func presentFeedback() {
        let origin = PixUserNavigation(transformFeedbackType(type: type))
        switch type {
        case .claimKeyCancel:
            presenter.didNextStep(action: .feedback(feedbackType: .claimCancellationInProgress, from: origin))
        case .portabilityKeyCancel:
            presenter.didNextStep(action: .feedback(feedbackType: .portabilityCancellationInProgress, from: origin))
        }
    }
    
    private func presentError() {
        switch type {
        case .claimKeyCancel:
            presenter.presentError(
                title: Strings.Feedback.ClaimKeyCancel.title,
                message: Strings.Feedback.ClaimKeyCancel.message,
                buttonText: Strings.General.tryAgain
            )
        case .portabilityKeyCancel:
            presenter.presentError(
                title: Strings.Feedback.PortabilityKeyCancel.title,
                message: Strings.Feedback.PortabilityKeyCancel.message,
                buttonText: Strings.General.tryAgain
            )
        }
    }
}

// MARK: - FeedbackCancelContainerInteracting
extension FeedbackCancelContainerInteractor: FeedbackCancelContainerInteracting {
    func configureContainer() {
        presenter.presentFeedback(type: transformFeedbackType(type: type), from: feedbackOrigin)
    }
    
    func execute(action: FeedbackAction, feedbackType: FeedbackViewType) {
        switch action {
        case .cancelClaim, .cancelPortability:
            guard let keyId = getKeyId() else { return }
            cancel(keyId: keyId)
        default:
            didClose(feedbackType: feedbackType)
        }
    }
    
    func didClose(feedbackType: FeedbackViewType?) {
        presenter.didNextStep(action: .close(feedbackType: feedbackType))
    }
}
