import Foundation

public enum PIXOriginType: String {
    case manual = "MANUAL"
    case key = "KEY"
    case qrCode = "QRCODE"
    case copyPaste = "COPY_PASTE"
}

public struct PIXReceiverData: Encodable, Equatable {
    public let name: String?
    public let bankId: String
    public let bankName: String
    public let accountType: String
    public let agency: String
    public let accountNumber: String
    public let cpfCnpj: String?
    public let ownership: String

    private enum CodingKeys: String, CodingKey {
        case name
        case bankId
        case bankName
        case accountType
        case agency = "branchNumber"
        case accountNumber
        case cpfCnpj
        case ownership
    }
    
    public init(
        name: String?,
        bankId: String,
        bankName: String,
        accountType: String,
        agency: String,
        accountNumber: String,
        cpfCnpj: String?,
        ownership: String
    ) {
        self.name = name
        self.bankId = bankId
        self.bankName = bankName
        self.accountType = accountType
        self.agency = agency
        self.accountNumber = accountNumber
        self.cpfCnpj = cpfCnpj
        self.ownership = ownership
    }
}

public struct PIXParams: Encodable, Equatable {
    public let originType: String
    public let keyType: String
    public let key: String?
    public let receiverData: PIXReceiverData?

    public init(originType: PIXOriginType, keyType: String, key: String?, receiverData: PIXReceiverData?) {
        self.originType = originType.rawValue
        self.keyType = keyType
        self.key = key
        self.receiverData = receiverData
    }
}
