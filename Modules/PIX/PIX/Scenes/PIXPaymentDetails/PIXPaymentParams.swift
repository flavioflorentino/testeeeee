import Foundation

public struct PIXPaymentParams: Encodable {
    public let biometry: Bool
    public let consumerValue: String
    public let credit: String
    public let installments: Int
    public let creditCardId: String
    public let feedVisibility: String
    public let description: String
    public let ignoreBalance: Bool

    public init(
        biometry: Bool,
        consumerValue: String,
        credit: String,
        installments: Int,
        creditCardId: String,
        feedVisibility: String,
        description: String,
        ignoreBalance: Bool
    ) {
        self.biometry = biometry
        self.consumerValue = consumerValue
        self.credit = credit
        self.installments = installments
        self.creditCardId = creditCardId
        self.feedVisibility = feedVisibility
        self.description = description
        self.ignoreBalance = ignoreBalance
    }
}
