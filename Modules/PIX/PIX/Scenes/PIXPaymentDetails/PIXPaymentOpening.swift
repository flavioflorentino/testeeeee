import UIKit

public protocol PIXPaymentOpening: AnyObject {
    func openPaymentWithDetails(
        _ details: PIXPaymentDetails,
        onViewController controller: UIViewController,
        origin: PixPaymentOrigin,
        flowType: PixPaymentFlowType
    )
}
