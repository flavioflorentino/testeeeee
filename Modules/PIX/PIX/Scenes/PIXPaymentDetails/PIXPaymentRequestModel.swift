import Foundation

struct PIXPaymentRequestModel: Encodable {
    let pixParams: PIXParams
    let params: PIXPaymentParams
}
