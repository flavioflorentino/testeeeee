import UIKit

public enum PIXPaymentScreenType: Equatable {
    case `default`(commentPlaceholder: String)
    case qrCode(items: [[QRCodeAcessoryPaymentModel]])
}

public struct QRCodeAcessoryPaymentModel: Decodable, Equatable {
    public let title: String
    public let value: String
}

public struct PIXPayee: Equatable {
    public let name: String
    public let description: String
    public let imageURLString: String?
    public let id: String?
    
    public init(name: String, description: String, imageURLString: String?, id: String?) {
        self.name = name
        self.description = description
        self.imageURLString = imageURLString
        self.id = id
    }
}

public struct PIXPaymentDetails: Equatable {
    public let title: String
    public let screenType: PIXPaymentScreenType
    public let payee: PIXPayee
    public let pixParams: PIXParams
    public let value: Double
    public let isEditable: Bool
    
    public init(
        title: String,
        screenType: PIXPaymentScreenType,
        payee: PIXPayee,
        pixParams: PIXParams,
        value: Double = 0.0,
        isEditable: Bool = true
    ) {
        self.title = title
        self.screenType = screenType
        self.payee = payee
        self.pixParams = pixParams
        self.value = value
        self.isEditable = isEditable
    }
}

public enum PixPaymentFlowType {
    case `default`
    case pixReturn(transactionId: String)
}
