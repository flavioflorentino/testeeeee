import UIKit

enum KeyManagementWelcomeAction: Equatable {
    case close
    case finishOnboarding
    case tapCustomButton
}

protocol KeyManagementWelcomeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: KeyManagementWelcomeAction)
}

final class KeyManagementWelcomeCoordinator {
    weak var viewController: UIViewController?
    var pages: KeyManagementWelcomePages?
    private let sceneOrigin: PixUserNavigation
    
    init(pages: KeyManagementWelcomePages, from origin: PixUserNavigation) {
        self.pages = pages
        sceneOrigin = origin
    }
}

// MARK: - KeyManagementWelcomeCoordinating
extension KeyManagementWelcomeCoordinator: KeyManagementWelcomeCoordinating {
    private var isFirstControllerInNavigation: Bool {
        viewController?.navigationController?.viewControllers.first == viewController
    }
    
    func perform(action: KeyManagementWelcomeAction) {
        if case let .biz(output) = pages {
            output(action)
            return
        }
        if case let .bizReceivement(output) = pages {
            output(action)
            return
        }
        if case let .bizCashout(output) = pages {
            output(action)
            return
        }
        switch action {
        case .close:
            if isFirstControllerInNavigation {
                viewController?.navigationController?.dismiss(animated: true)
                return
            }
            viewController?.navigationController?.popViewController(animated: true)
        case .finishOnboarding:
            guard let navController = viewController?.navigationController else { return }
            let keyManagementController = KeyManagerConsumerFactory.make(from: sceneOrigin, checkForPreRegisteredKeys: false)
            
            var viewControllers = navController.viewControllers
            _ = viewControllers.popLast()
            viewControllers.append(keyManagementController)
            navController.setViewControllers(viewControllers, animated: true)
        case .tapCustomButton:
            break
        }
    }
}
