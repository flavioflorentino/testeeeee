import UI
import UIKit
import SnapKit

extension KeyManagementWelcomePageView.Layout {
    enum Size {
        static let imageHeight: CGFloat = 170
    }
}

final class KeyManagementWelcomePageView: UICollectionViewCell {
    fileprivate enum Layout {}
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, Colors.grayscale700.color)
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var containerView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with model: KeyManagementWelcomePageModel) {
        imageView.image = model.image
        titleLabel.text = model.title
        messageLabel.attributedText = model.message.attributedStringWithFont(
            primary: Typography.bodySecondary(.default).font(),
            secondary: Typography.bodySecondary(.highlight).font(),
            textAlignment: .center
        )
    }
}

extension KeyManagementWelcomePageView: ViewConfiguration {
    func buildViewHierarchy() {
        containerView.addSubview(imageView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(messageLabel)
        addSubview(containerView)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.centerY.equalToSuperview()
            $0.top.greaterThanOrEqualToSuperview()
            $0.bottom.lessThanOrEqualToSuperview()
        }
        
        imageView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Size.imageHeight)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base05)
            $0.leading.trailing.equalToSuperview()
        }
        
        messageLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
}
