import UIKit
import UI

final class KeyManagementWelcomeButton: UIButton {
    private let viewModel: KeyManagementWelcomeButtonModel
    private let didTapButton: (KeyManagementWelcomeButtonAction) -> Void
    
    init(viewModel: KeyManagementWelcomeButtonModel, didTapButton: @escaping (KeyManagementWelcomeButtonAction) -> Void) {
        self.viewModel = viewModel
        self.didTapButton = didTapButton
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func buildLayout() {
        setTitle(viewModel.title, for: .normal)
        buttonStyle(viewModel.style)
        addTarget(self, action: #selector(didTap), for: .touchUpInside)
    }
    
    @objc
    private func didTap() {
        didTapButton(viewModel.action)
    }
}
