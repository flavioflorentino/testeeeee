import Foundation
import AnalyticsModule

protocol KeyManagementWelcomeInteracting: AnyObject {
    func configureWelcomePages()
    func didScrollToPage(at pageIndex: Int)
    func didClose()
    func skipOnboard()
    func backButtonPressed()
    func configureNavigationBarButtons()
    func didTap(action: KeyManagementWelcomeButtonAction)
}

enum KeyManagementWelcomeButtonAction {
    case `continue`
    case custom
}

final class KeyManagementWelcomeInteractor {
    private typealias PageStrings = Strings.KeyManagement.Welcome
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: KeyManagementWelcomeServicing
    private let presenter: KeyManagementWelcomePresenting
    private let pages: KeyManagementWelcomePages
    private let userInfo: KeyManagerBizUserInfo?
    private var currentPage: Int = 0
    
    init(
        service: KeyManagementWelcomeServicing,
        presenter: KeyManagementWelcomePresenting,
        dependencies: Dependencies,
        pages: KeyManagementWelcomePages,
        userInfo: KeyManagerBizUserInfo?
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.pages = pages
        self.userInfo = userInfo
    }
    
    var isLastPage: Bool {
        currentPage == pages.model.count - 1
    }
    
    private func updateActionButtonAndPageControl(currentPageIndex: Int) {
        self.currentPage = currentPageIndex
        
        guard currentPageIndex < pages.model.count else {
            return
        }
        
        if case .biz = pages {
            pages.model[currentPage].hideSkipButton ? presenter.hideSkipButton() : presenter.presentNavigationSkipButton()
        }
        
        presenter.presentButtons(pages.model[currentPageIndex].buttons)
        presenter.presentPageControl(currentPageIndex: currentPageIndex)
        
        trackViewedEvent()
    }
    
    private func goToNextPage() {
        logContinueButtonEvent(currentPage)
        
        if isLastPage {
            service.setWelcomePagesVisualized()
            presenter.didNextStep(action: .finishOnboarding)
            return
        }
        
        presenter.scrollToNextPage()
    }
}

// MARK: - KeyManagementWelcomeInteracting
extension KeyManagementWelcomeInteractor: KeyManagementWelcomeInteracting {
    func backButtonPressed() {
        guard case .biz = pages else { return }
        let event = KeyBizTracker(companyName: userInfo?.name ?? "", eventType: .welcomeCarouselComeBack)
        dependencies.analytics.log(event)
    }
    
    func configureNavigationBarButtons() {
        if case .biz = pages {
          presenter.presentNavigationSkipButton()
        } else {
          presenter.presentNavigationCloseButton()
        }
    }
    
    func skipOnboard() {
        let event = KeyBizTracker(companyName: userInfo?.name ?? "", eventType: .welcomeCarouselSkip)
        dependencies.analytics.log(event)
        
        service.setWelcomePagesVisualized()
        presenter.didNextStep(action: .finishOnboarding)
    }
    
    func configureWelcomePages() {
        presenter.present(welcomePages: pages.model)
        updateActionButtonAndPageControl(currentPageIndex: .zero)
    }
    
    func didScrollToPage(at pageIndex: Int) {
        updateActionButtonAndPageControl(currentPageIndex: pageIndex)
    }
    
    func didClose() {
        presenter.didNextStep(action: .close)
        trackClosedEvent()
    }
    
    func didTap(action: KeyManagementWelcomeButtonAction) {
        switch action {
        case .continue:
            goToNextPage()
        case .custom:
            presenter.didNextStep(action: .tapCustomButton)
        }
    }
    
    private func trackViewedEvent() {
        guard case .pf = pages else { return }
        let pageDescripition = OnboardingConsumerEvent.WelcomeScreenDescription(pageIndex: currentPage)
        let event = OnboardingConsumerEvent.viewed(pageDescripition)
        dependencies.analytics.log(event)
    }
    
    private func trackClosedEvent() {
        guard case .pf = pages else { return }
        let pageDescripition = OnboardingConsumerEvent.WelcomeScreenDescription(pageIndex: currentPage)
        let event = OnboardingConsumerEvent.closed(pageDescripition)
        dependencies.analytics.log(event)
    }
    
    private func logContinueButtonEvent(_ currentPage: Int) {
        guard case .biz = pages else { return }
        let event = KeyBizTracker(companyName: userInfo?.name ?? "", eventType: .welcomeCarousel(page: (currentPage + 1)))
        dependencies.analytics.log(event)
    }
}
