import Core
import Foundation

protocol KeyManagementWelcomeServicing {
    func setWelcomePagesVisualized()
}

final class KeyManagementWelcomeService {
    typealias Dependencies = HasKVStore
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - KeySelectorServicing
extension KeyManagementWelcomeService: KeyManagementWelcomeServicing {
    func setWelcomePagesVisualized() {
        dependencies.kvStore.setBool(true, with: KVKey.isPixKeyManagementWelcomeVisualized)
    }
}
