import AssetsKit
import UI
import Foundation

struct KeyManagementWelcomePageModel: Equatable {
    struct ActionButton: Equatable {
        let title: String
        let action: KeyManagementWelcomeButtonAction
        let style: Style
    }
    
    enum Style {
        case primary
        case secundary
    }
    
    let image: UIImage?
    let title: String
    let message: String
    let buttons: [ActionButton]
    let hideSkipButton: Bool
}

enum KeyManagementWelcomePages {
    case pf
    case biz((KeyManagementWelcomeAction) -> Void)
    case bizReceivement((KeyManagementWelcomeAction) -> Void)
    case bizCashout((KeyManagementWelcomeAction) -> Void)
    
    var model: [KeyManagementWelcomePageModel] {
        typealias PageStrings = Strings.KeyManagement.Welcome
        
        switch self {
        case .pf:
            return [
                KeyManagementWelcomePageModel(
                    image: Assets.iluPicpayPlusPix.image,
                    title: PageStrings.DoMorePage.title,
                    message: PageStrings.DoMorePage.message,
                    buttons: [.init(title: Strings.General.continue, action: .continue, style: .secundary)],
                    hideSkipButton: false
                ),
                KeyManagementWelcomePageModel(
                    image: Resources.Illustrations.iluFillingForm.image,
                    title: PageStrings.RegisterPage.title,
                    message: PageStrings.RegisterPage.message,
                    buttons: [.init(title: Strings.General.continue, action: .continue, style: .secundary)],
                    hideSkipButton: false
                ),
                KeyManagementWelcomePageModel(
                    image: Resources.Illustrations.iluHoldingQrcode.image,
                    title: PageStrings.ManagePage.title,
                    message: PageStrings.ManagePage.message,
                    buttons: [.init(title: Strings.General.gotIt, action: .continue, style: .primary)],
                    hideSkipButton: false
                )
            ]
        case .biz:
            return [
                KeyManagementWelcomePageModel(
                    image: Resources.Illustrations.iluTakingSelfie.image,
                    title: PageStrings.PayAndReceiveWithPix.title,
                    message: PageStrings.PayAndReceiveWithPix.message,
                    buttons: [.init(title: Strings.General.continue, action: .continue, style: .secundary)],
                    hideSkipButton: false
                ),
                KeyManagementWelcomePageModel(
                    image: Resources.Illustrations.iluHoldingQrcode.image,
                    title: PageStrings.RegisterToUse.title,
                    message: PageStrings.RegisterToUse.message,
                    buttons: [.init(title: Strings.General.continue, action: .continue, style: .secundary)],
                    hideSkipButton: false
                ),
                KeyManagementWelcomePageModel(
                    image: Resources.Illustrations.iluHoldingPadlock.image,
                    title: PageStrings.ManageHowReceive.title,
                    message: PageStrings.ManageHowReceive.message,
                    buttons: [.init(title: Strings.General.gotIt, action: .continue, style: .primary)],
                    hideSkipButton: true
                )
            ]
        case .bizReceivement:
            return [
                KeyManagementWelcomePageModel(
                    image: Resources.Illustrations.iluPersonHappy.image,
                    title: PageStrings.Receivement.FirstPage.title,
                    message: PageStrings.Receivement.FirstPage.description,
                    buttons: [.init(title: Strings.General.forward, action: .continue, style: .secundary)],
                    hideSkipButton: false
                ),
                KeyManagementWelcomePageModel(
                    image: Resources.Illustrations.iluHoldingQrcode.image,
                    title: PageStrings.Receivement.SecondPage.title,
                    message: PageStrings.Receivement.SecondPage.description,
                    buttons: [.init(title: Strings.General.forward, action: .continue, style: .secundary)],
                    hideSkipButton: false
                ),
                KeyManagementWelcomePageModel(
                    image: Resources.Illustrations.iluFeedbackWhatsapp.image,
                    title: PageStrings.Receivement.ThirdPage.title,
                    message: PageStrings.Receivement.ThirdPage.description,
                    buttons: [
                        .init(title: Strings.General.gotIt, action: .continue, style: .primary),
                        .init(title: Strings.General.knowMore, action: .custom, style: .secundary)
                    ],
                    hideSkipButton: false
                )
            ]
        case .bizCashout:
            return [
                KeyManagementWelcomePageModel(
                    image: Resources.Illustrations.iluPersonHappy.image,
                    title: PageStrings.CashOut.FirstPage.title,
                    message: PageStrings.CashOut.FirstPage.description,
                    buttons: [.init(title: Strings.General.forward, action: .continue, style: .secundary)],
                    hideSkipButton: false
                ),
                KeyManagementWelcomePageModel(
                    image: Resources.Illustrations.iluFeedbackWhatsapp.image,
                    title: PageStrings.CashOut.SecondPage.title,
                    message: PageStrings.CashOut.SecondPage.description,
                    buttons: [.init(title: Strings.General.forward, action: .continue, style: .secundary)],
                    hideSkipButton: false
                ),
                KeyManagementWelcomePageModel(
                    image: Resources.Illustrations.iluHoldingQrcode.image,
                    title: PageStrings.CashOut.ThirdPage.title,
                    message: PageStrings.CashOut.ThirdPage.description,
                    buttons: [
                        .init(title: Strings.General.gotIt, action: .continue, style: .primary),
                        .init(title: Strings.General.knowMore, action: .custom, style: .secundary)
                    ],
                    hideSkipButton: false
                )
            ]
        }
    }
}
