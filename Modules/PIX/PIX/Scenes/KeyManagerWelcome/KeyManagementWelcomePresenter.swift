import Foundation
import UI

struct KeyManagementWelcomeButtonModel {
    let title: String
    let style: AnyButtonStyle<UIButton>
    let action: KeyManagementWelcomeButtonAction
}

protocol KeyManagementWelcomePresenting: AnyObject {
    var viewController: KeyManagementWelcomeDisplay? { get set }
    func present(welcomePages: [KeyManagementWelcomePageModel])
    func presentPageControl(currentPageIndex: Int)
    func presentButtons(_ buttons: [KeyManagementWelcomePageModel.ActionButton])
    func hideSkipButton()
    func scrollToNextPage()
    func presentNavigationSkipButton()
    func presentNavigationCloseButton()
    func didNextStep(action: KeyManagementWelcomeAction)
}

final class KeyManagementWelcomePresenter {
    private let coordinator: KeyManagementWelcomeCoordinating
    weak var viewController: KeyManagementWelcomeDisplay?

    init(coordinator: KeyManagementWelcomeCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - KeyManagementWelcomePresenting
extension KeyManagementWelcomePresenter: KeyManagementWelcomePresenting {
    func presentNavigationCloseButton() {
        viewController?.showNavigationCloseButton()
    }
    
    func presentNavigationSkipButton() {
        viewController?.showNavigationSkipButton()
    }
    
    func hideSkipButton() {
        viewController?.hideSkipButton()
    }
    
    func present(welcomePages: [KeyManagementWelcomePageModel]) {
        viewController?.display(welcomePages: welcomePages)
    }
    
    func presentPageControl(currentPageIndex: Int) {
        viewController?.displayPageControl(currentPageIndex: currentPageIndex)
    }
    
    func presentButtons(_ buttons: [KeyManagementWelcomePageModel.ActionButton]) {
        let mappedButtons = buttons.map {
            KeyManagementWelcomeButtonModel(
                title: $0.title,
                style: $0.style == .primary ?
                    AnyButtonStyle(style: PrimaryButtonStyle()) : AnyButtonStyle(style: SecondaryButtonStyle()),
                action: $0.action
            )
        }
        
        viewController?.displayButtons(mappedButtons)
    }
    
    func scrollToNextPage() {
        viewController?.scrollToNextPage()
    }
    
    func didNextStep(action: KeyManagementWelcomeAction) {
        coordinator.perform(action: action)
    }
}
