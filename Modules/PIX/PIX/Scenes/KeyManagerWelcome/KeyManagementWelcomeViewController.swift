import UI
import UIKit

protocol KeyManagementWelcomeDisplay: AnyObject {
    func display(welcomePages: [KeyManagementWelcomePageModel])
    func scrollToNextPage()
    func hideSkipButton()
    func showNavigationCloseButton()
    func showNavigationSkipButton()
    func displayPageControl(currentPageIndex: Int)
    func displayButtons(_ buttons: [KeyManagementWelcomeButtonModel])
}

final class KeyManagementWelcomeViewController: ViewController<KeyManagementWelcomeInteracting, UIView> {
    private lazy var carouselPageView: CarouselPageView<KeyManagementWelcomePageModel, KeyManagementWelcomePageView> = {
        let caroselView = CarouselPageView<KeyManagementWelcomePageModel, KeyManagementWelcomePageView>()
        caroselView.carouselPageDelegate = self
        return caroselView
    }()
    
    private lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = Colors.branding600.color
        pageControl.pageIndicatorTintColor = Colors.grayscale100.color
        pageControl.backgroundColor = .clear
        pageControl.isAccessibilityElement = false
        pageControl.isUserInteractionEnabled = false
        return pageControl
    }()
    
    private lazy var actionButtonsStack: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var skipNavButton = UIBarButtonItem(
        title: Strings.General.skip,
        style: .plain,
        target: self,
        action: #selector(tapSkipOnboard)
    )

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.configureWelcomePages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParent {
            interactor.backButtonPressed()
        }
    }
    override func buildViewHierarchy() {
        view.addSubview(carouselPageView)
        view.addSubview(pageControl)
        view.addSubview(actionButtonsStack)
    }
    
    override func setupConstraints() {
        carouselPageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
        }
        
        pageControl.snp.makeConstraints {
            $0.top.equalTo(carouselPageView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        actionButtonsStack.snp.makeConstraints {
            $0.top.equalTo(pageControl.snp.bottom).offset(Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.tintColor = Colors.branding400.color
        interactor.configureNavigationBarButtons()
    }
    
    private func removeAllArrangedSubviews() {
        actionButtonsStack.arrangedSubviews.forEach {
            actionButtonsStack.removeArrangedSubview($0)
            $0.removeFromSuperview()
        }
    }
}

@objc
private extension KeyManagementWelcomeViewController {
    func tapCloseButton() {
        interactor.didClose()
    }
    
    func tapSkipOnboard() {
        interactor.skipOnboard()
    }
}

extension KeyManagementWelcomeViewController: CarouselPageViewDelegate {
    func didScrollToPage(at pageIndex: Int) {
        interactor.didScrollToPage(at: pageIndex)
    }
}

// MARK: KeyManagementWelcomeDisplay
extension KeyManagementWelcomeViewController: KeyManagementWelcomeDisplay {
    func showNavigationCloseButton() {
        guard navigationController?.viewControllers.first == self else {
            return
        }
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: Strings.General.close,
            style: .plain,
            target: self,
            action: #selector(tapCloseButton)
        )
    }
    
    func showNavigationSkipButton() {
        navigationItem.rightBarButtonItem = skipNavButton
    }
    
    func hideSkipButton() {
        navigationItem.rightBarButtonItem = nil
    }
    
    func display(welcomePages: [KeyManagementWelcomePageModel]) {
        pageControl.numberOfPages = welcomePages.count
        
        carouselPageView.setup(data: welcomePages) {  _, model, cell in
            cell.configure(with: model)
        }
    }
    
    func scrollToNextPage() {
        carouselPageView.scrollToNextPage()
    }
    
    func displayPageControl(currentPageIndex: Int) {
        pageControl.currentPage = currentPageIndex
    }
    
    func displayButtons(_ buttons: [KeyManagementWelcomeButtonModel]) {
        removeAllArrangedSubviews()
        
        buttons.forEach { viewModel in
            let button = KeyManagementWelcomeButton(viewModel: viewModel) { [weak self] action in
                self?.interactor.didTap(action: action)
            }
            actionButtonsStack.addArrangedSubview(button)
        }
    }
}
