import Foundation
import UIKit

enum KeyManagementWelcomeFactory {
    static func make(
        _ pages: KeyManagementWelcomePages,
        userInfo: KeyManagerBizUserInfo? = nil,
        from origin: PixUserNavigation = .hub
    ) -> KeyManagementWelcomeViewController {
        let container = DependencyContainer()
        let coordinator: KeyManagementWelcomeCoordinating = KeyManagementWelcomeCoordinator(pages: pages, from: origin)
        let service: KeyManagementWelcomeServicing = KeyManagementWelcomeService(dependencies: container)
        let presenter: KeyManagementWelcomePresenting = KeyManagementWelcomePresenter(coordinator: coordinator)
        let interactor = KeyManagementWelcomeInteractor(
            service: service,
            presenter: presenter,
            dependencies: container,
            pages: pages,
            userInfo: userInfo
        )
        let viewController = KeyManagementWelcomeViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
