import Foundation
import UI
import UIKit

protocol CustomQRCodePresenting {
    var viewController: ReceivePaymentsDisplay? { get set }
    func presentQRCode(image: UIImage, profileImageURL: URL?, qrCodeText: String)
    func presentSelectedKey(key: RegisteredKey)
    func presentUserName(name: String)
    func presentSaveImageButton()
    func presentTexts()
    func shareQRCode(controller: UIActivityViewController)
    func hideChangeKeysButton()
    func presentImageAlert(didSave: Bool)
    func presentReceivementSelectionStyle(newType: ReceivementPixType, previousType: ReceivementPixType?)
    func presentQRCodeContainer()
    func presentCopyPasteContainer()
    func presentCopyPasteNotification()
}

final class CustomQRCodePresenter: CustomQRCodePresenting {
    private typealias Localizable = Strings.Receive.CustomizedReceivePayments
    private let coordinator: CustomQRCodeCoordinating
    weak var viewController: ReceivePaymentsDisplay?

    init(coordinator: CustomQRCodeCoordinating) {
        self.coordinator = coordinator
    }

    func presentQRCode(image: UIImage, profileImageURL: URL?, qrCodeText: String) {
        viewController?.displayQRCode(image: image, profileImageURL: profileImageURL, qrCodeText: qrCodeText)
    }

    func presentSelectedKey(key: RegisteredKey) {
        let viewModel = createViewModel(from: key)
        if let formattedKey = viewModel?.rowTitle {
            viewController?.displaySelectedKey(key: formattedKey)
        }
    }

    func presentUserName(name: String) {
        viewController?.displayUserName(name: name)
    }

    func shareQRCode(controller: UIActivityViewController) {
        coordinator.shareQRCode(activityController: controller)
    }

    func presentSaveImageButton() {
        let title = Localizable.saveImage
        viewController?.displaySecondaryButton(type: .saveImage(title: title))
    }

    func presentTexts() {
        viewController?.displayTexts(title: Localizable.title, description: Localizable.description)
    }

    func hideChangeKeysButton() {
        viewController?.hideChangeKeyButton()
    }

    func presentImageAlert(didSave: Bool) {
        let title = didSave ? Localizable.SaveImage.Success.alertTile : Localizable.SaveImage.Failure.alertTile
        let description = didSave ? Localizable.SaveImage.Success.alertDescription : Localizable.SaveImage.Failure.alertDescription
        viewController?.displayImageSavedAlert(title: title, description: description)
    }
    
    func presentReceivementSelectionStyle(newType: ReceivementPixType, previousType: ReceivementPixType?) {
        viewController?.displayButtonStyle(
            backgroundColor: Colors.grayscale750.color,
            titleColor: Colors.white.color,
            index: newType.rawValue
        )
        guard let previousType = previousType else {
            return
        }
        viewController?.displayButtonStyle(
            backgroundColor: Colors.backgroundTertiary.color,
            titleColor: Colors.grayscale600.color,
            index: previousType.rawValue
        )
    }
    
    func presentQRCodeContainer() {
        viewController?.displayQRCodeContainer()
    }
    
    func presentCopyPasteContainer() {
        viewController?.displayCopyPasteContainer()
    }
    
    func presentCopyPasteNotification() {
        let message = Strings.Receive.ReceivePayments.copiedCode
        viewController?.displayCopyPasteNotification(message: message)
    }
}

private extension CustomQRCodePresenter {
    func createViewModel(from key: RegisteredKey) -> PIXReceiveKeyViewModel? {
        let value = configureValue(for: key.keyType, value: key.keyValue)
        return PIXReceiveKeyViewModel(id: key.id, type: key.keyType, value: value)
    }

    func configureValue(for type: PixKeyType, value: String) -> String {
        let descriptor: PersonalDocumentMaskDescriptor
        switch type {
        case .cnpj:
            descriptor = .cnpj
        case .cpf:
            descriptor = .cpf
        case .phone:
            descriptor = .cellPhoneWithDDD
        default:
            return value
        }

        let mask = CustomStringMask(descriptor: descriptor)
        return mask.maskedText(from: value) ?? value
    }
}
