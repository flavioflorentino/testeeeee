import UIKit

enum CustomQRCodeFactory {
    static func make(qrCodeImage: UIImage, selectedKey: RegisteredKey, qrCodeText: String) -> ReceivePaymentsViewController {
        let container = DependencyContainer()
        let coordinator = CustomQRCodeCoordinator()
        let presenter = CustomQRCodePresenter(coordinator: coordinator)
        let consumerInstance: PixConsumerContract? = PIXSetup.consumerInstance
        let interactor = CustomQRCodeInteractor(
            presenter: presenter,
            dependencies: container,
            consumerInstance: consumerInstance,
            selectedKey: selectedKey,
            qrCodeImage: qrCodeImage,
            qrCodeText: qrCodeText
        )
        let viewController = ReceivePaymentsViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
