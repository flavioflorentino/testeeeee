import Foundation
import UIKit
import AnalyticsModule

final class CustomQRCodeInteractor: NSObject, ReceivePaymentsInteracting {
    typealias Dependencies = HasAnalytics
    private let presenter: CustomQRCodePresenting
    private let consumerInstance: PixConsumerContract?
    private let dependencies: Dependencies
    private let selectedKey: RegisteredKey
    private let qrCodeImage: UIImage
    private let qrCodeText: String
    private var selectedReceivementType: ReceivementPixType = .qrcode

    init(presenter: CustomQRCodePresenting,
         dependencies: Dependencies,
         consumerInstance: PixConsumerContract?,
         selectedKey: RegisteredKey,
         qrCodeImage: UIImage,
         qrCodeText: String) {
        self.presenter = presenter
        self.consumerInstance = consumerInstance
        self.selectedKey = selectedKey
        self.qrCodeImage = qrCodeImage
        self.qrCodeText = qrCodeText
        self.dependencies = dependencies
    }

    func viewDidLoad() {
        presentUserName()
        presenter.presentTexts()
        presenter.presentSaveImageButton()
        presenter.presentSelectedKey(key: selectedKey)
        presenter.presentQRCode(image: qrCodeImage, profileImageURL: self.getProfileImageURL(), qrCodeText: qrCodeText)
        presenter.hideChangeKeysButton()
        presenter.presentReceivementSelectionStyle(newType: selectedReceivementType, previousType: nil)
        presentReceivementTypeContainer(selectedReceivementType)
    }
    
    func shareQRCode(image: UIImage) {
        dependencies.analytics.log(ReceiveConsumerEvent.qrCodeShared)
        let shareSheet = UIActivityViewController(activityItems: [image as Any], applicationActivities: nil)
        presenter.shareQRCode(controller: shareSheet)
    }

    func saveImage(_ image: UIImage) {
        dependencies.analytics.log(ReceiveConsumerEvent.customQRCodeSaved)
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(CustomQRCodeInteractor.image), nil)
    }

    @objc
    func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        presenter.presentImageAlert(didSave: error == nil)
    }

    private func presentUserName() {
        let userName = consumerInstance?.name ?? ""
        presenter.presentUserName(name: userName)
    }

    private func getProfileImageURL() -> URL? {
        consumerInstance?.imageURL
    }

    func registerNewKey() { }

    func customizeQRCode() { }
    
    func openFAQ(in viewController: UIViewController) {  }

    func didSelectKey(viewModel: PIXReceiveKeyViewModel) {  }
    
    func selectReceivementType(_ newType: ReceivementPixType) {
        guard selectedReceivementType != newType else {
            return
        }
        presenter.presentReceivementSelectionStyle(newType: newType, previousType: selectedReceivementType)
        presentReceivementTypeContainer(newType)
        selectedReceivementType = newType
    }
    
    private func presentReceivementTypeContainer(_ type: ReceivementPixType) {
        switch type {
        case .qrcode:
            presenter.presentQRCodeContainer()
        case .copyPaste:
            presenter.presentCopyPasteContainer()
        }
    }
    
    func copyCode() {
        UIPasteboard.general.string = qrCodeText
        presenter.presentCopyPasteNotification()
    }
    
    func close() {}
}
