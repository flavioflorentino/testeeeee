import Foundation
import UIKit

enum CustomQRCodeAction: Equatable {
    case shareQRCode(activityController: UIActivityViewController)
}

protocol CustomQRCodeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func shareQRCode(activityController: UIActivityViewController)
}

final class CustomQRCodeCoordinator: CustomQRCodeCoordinating {
    weak var viewController: UIViewController?
    func shareQRCode(activityController: UIActivityViewController) {
        viewController?.present(activityController, animated: true)
    }
}
