import Core
import Foundation

protocol ReceiveServicing {
    func listPixKeys(with userId: String, completion: @escaping (Result<PixKeyDataList, ApiError>) -> Void)
    func generateQRCode(request: ReceiveQRCodeRequest, completion: @escaping (Result<ReceiveQRCodeResponse, ApiError>) -> Void)
}

final class ReceiveService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension ReceiveService: ReceiveServicing {
    func listPixKeys(with userId: String, completion: @escaping (Result<PixKeyDataList, ApiError>) -> Void) {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        let endpoint = PixConsumerKeyServiceEndpoint.keys(userId)
        Core.Api<PixKeyDataList>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }

    func generateQRCode(request: ReceiveQRCodeRequest, completion: @escaping (Result<ReceiveQRCodeResponse, ApiError>) -> Void) {
        let endpoint = ReceiveServiceEndpoint.generateQRCode(request: request)
        Api<ReceiveQRCodeResponse>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
