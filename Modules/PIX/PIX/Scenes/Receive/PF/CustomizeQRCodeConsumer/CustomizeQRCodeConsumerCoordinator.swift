import Foundation
import UIKit

enum CustomizeQRCodeConsumerAction: Equatable {
    case close
    case keyManager
    case qrCode(image: UIImage, selectedKey: RegisteredKey, qrCodeText: String)
}

protocol CustomizeQRCodeConsumerCoordinating: AnyObject {
    var callQrCodeFlowClosure: ((UIImage, RegisteredKey, String) -> Void)? { get set }
    func perform(action: CustomizeQRCodeConsumerAction)
}

final class CustomizeQRCodeConsumerCoordinator: CustomizeQRCodeConsumerCoordinating {
    weak var viewController: UIViewController?
    var callQrCodeFlowClosure: ((UIImage, RegisteredKey, String) -> Void)?

    func perform(action: CustomizeQRCodeConsumerAction) {
        switch action {
        case let .qrCode(image, selectedKey, qrCodeText):
            callQrCodeFlowClosure?(image, selectedKey, qrCodeText)
            viewController?.dismiss(animated: true)
        case .keyManager:
            let controller = KeyManagerConsumerFactory.make(from: .receive, checkForPreRegisteredKeys: false)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
