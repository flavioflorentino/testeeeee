import Foundation
import Core
import UI

protocol CustomizeQRCodeConsumerPresenting: AnyObject {
    var viewController: CustomizeQRCodeConsumerDisplaying? { get set }
    func presentUserKeys(keys: [RegisteredKey])
    func presentSelectedKey(key: RegisteredKey)
    func presentDescriptionTextField(currentTextLength: Int, maxDescriptionLength: Int)
    func presentIdentifierTextField(currentTextLength: Int, maxDescriptionLength: Int)
    func deleteDescriptionTextFieldBackward()
    func deleteIdentifierTextFieldBackward()
    func presentValueTextField(with text: String)
    func startLoading()
    func stopLoading()
    func presentError()
    func didNextStep(action: CustomizeQRCodeConsumerAction)
}

final class CustomizeQRCodeConsumerPresenter {
    private let coordinator: CustomizeQRCodeConsumerCoordinating
    weak var viewController: CustomizeQRCodeConsumerDisplaying?

    init(coordinator: CustomizeQRCodeConsumerCoordinating) {
        self.coordinator = coordinator
    }
}

extension CustomizeQRCodeConsumerPresenter: CustomizeQRCodeConsumerPresenting {
    func presentUserKeys(keys: [RegisteredKey]) {
        let viewModels = keys.compactMap { createViewModel(from: $0) }
        viewController?.displayUserKeys(keys: viewModels)
    }

    func presentSelectedKey(key: RegisteredKey) {
        let viewModel = createViewModel(from: key)
        if let formattedKey = viewModel?.rowTitle {
            viewController?.displayKeySelected(text: formattedKey)
        }
    }

    func presentDescriptionTextField(currentTextLength: Int, maxDescriptionLength: Int) {
        let descriptionCountText = Strings.Receive.CustomizeQRCode.descriptionRule(currentTextLength, maxDescriptionLength)
        viewController?.displayDescriptionCount(text: descriptionCountText)
    }

    func presentIdentifierTextField(currentTextLength: Int, maxDescriptionLength: Int) {
        let descriptionCountText = Strings.Receive.CustomizeQRCode.descriptionRule(currentTextLength, maxDescriptionLength)
        viewController?.displayIdentifierCount(text: descriptionCountText)
    }

    func deleteDescriptionTextFieldBackward() {
        viewController?.deleteDescriptionTextFieldBackward()
    }

    func deleteIdentifierTextFieldBackward() {
        viewController?.deleteIdentifierTextFieldBackward()
    }

    func presentValueTextField(with text: String) {
        guard let amount = Int(text) else {
            viewController?.deleteValueTextFieldBackward()
            return
        }

        let number = Decimal(amount) / Decimal(100)
        let currencyString = NumberFormatter.toCurrencyString(with: number)

        viewController?.displayValueTextField(text: currencyString ?? Strings.General.zeroAmount)
    }

    func startLoading() {
        viewController?.displayLoading()
    }

    func stopLoading() {
        viewController?.stopLoading()
    }

    func presentError() {
        viewController?.displayError()
    }

    func didNextStep(action: CustomizeQRCodeConsumerAction) {
        coordinator.perform(action: action)
    }
}

private extension CustomizeQRCodeConsumerPresenter {
    func createViewModel(from key: RegisteredKey) -> PIXReceiveKeyViewModel? {
        let value = configureValue(for: key.keyType, value: key.keyValue)
        return PIXReceiveKeyViewModel(id: key.id, type: key.keyType, value: value)
    }

    func configureValue(for type: PixKeyType, value: String) -> String {
        let descriptor: PersonalDocumentMaskDescriptor
        switch type {
        case .cpf:
            descriptor = .cpf
        case .phone:
            descriptor = .cellPhoneWithDDD
        default:
            return value
        }

        let mask = CustomStringMask(descriptor: descriptor)
        return mask.maskedText(from: value) ?? value
    }
}
