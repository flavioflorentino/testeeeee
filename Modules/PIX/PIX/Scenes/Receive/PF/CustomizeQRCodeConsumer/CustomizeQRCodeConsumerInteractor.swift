import Foundation
import AnalyticsModule
import Core
import UIKit

protocol CustomizeQRCodeConsumerInteracting: AnyObject {
    func loadPixKeys()
    func didSelectKey(viewModel: PIXReceiveKeyViewModel)
    func registerNewKey()
    func editIdentifierText(with text: String)
    func checkDescriptionLength(with text: String)
    func editValueText(with text: String)
    func createQRCode(identifier: String?, description: String?, value: String?)
    func close()
}

final class CustomizeQRCodeConsumerInteractor: CustomizeQRCodeConsumerInteracting {
    typealias Dependencies = HasAnalytics
    private let service: ReceiveServicing
    private let presenter: CustomizeQRCodeConsumerPresenting
    private let userKeys: [RegisteredKey]
    private let dependencies: Dependencies
    private var selectedKey: RegisteredKey {
        didSet {
            presenter.presentSelectedKey(key: selectedKey)
        }
    }
    private let origin: PIXReceiveOrigin
    private let maxQRCodeDescriptionLength = 100
    private let maxQRCodeIdentifierLength = 50

    init(
        userKeys: [RegisteredKey],
        selectedKey: RegisteredKey,
        service: ReceiveServicing,
        presenter: CustomizeQRCodeConsumerPresenting,
        dependencies: Dependencies,
        origin: PIXReceiveOrigin
    ) {
        self.userKeys = userKeys
        self.selectedKey = selectedKey
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.origin = origin
    }

    func loadPixKeys() {
        presenter.presentUserKeys(keys: userKeys)
        presenter.presentSelectedKey(key: selectedKey)
        dependencies.analytics.log(ReceiveConsumerEvent.userNavigated(origin: origin, screen: .customizeQRCode))
    }

    func didSelectKey(viewModel: PIXReceiveKeyViewModel) {
        guard let key = userKeys.first(where: { $0.id == viewModel.id }) else {
            return presenter.presentError()
        }

        if key != selectedKey {
            selectedKey = key
        }
    }

    func registerNewKey() {
        presenter.didNextStep(action: .keyManager)
    }

    func checkDescriptionLength(with text: String) {
        let currentTextCount = text.count
        guard currentTextCount <= maxQRCodeDescriptionLength else {
            return presenter.deleteDescriptionTextFieldBackward()
        }

        presenter.presentDescriptionTextField(currentTextLength: currentTextCount, maxDescriptionLength: maxQRCodeDescriptionLength)
    }

    func editIdentifierText(with text: String) {
        let currentTextCount = text.count
        guard currentTextCount <= maxQRCodeIdentifierLength else {
            return presenter.deleteIdentifierTextFieldBackward()
        }

        presenter.presentIdentifierTextField(currentTextLength: currentTextCount, maxDescriptionLength: maxQRCodeIdentifierLength)
    }

    func editValueText(with text: String) {
        let valueText = text.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        presenter.presentValueTextField(with: valueText)
    }

    func createQRCode(identifier: String?, description: String?, value: String?) {
        var request = ReceiveQRCodeRequest(registeredKey: selectedKey)
        if let amount = value {
            request.amount = NumberFormatter.toNumber(from: amount)
        }
        request.description = description
        request.identifier = identifier
        let event = ReceiveConsumerEvent.customQRCodeCreated(id: identifier, description: description, value: value)
        dependencies.analytics.log(event)

        presenter.startLoading()
        service.generateQRCode(request: request) { result in
            self.presenter.stopLoading()
            switch result {
            case .success(let response):
                guard let image = UIImage(base64: response.qrCodeInBase64) else {
                    return self.presenter.presentError()
                }
                let qrCodeText = response.qrCodeText
                self.presenter.didNextStep(action: .qrCode(image: image, selectedKey: self.selectedKey, qrCodeText: qrCodeText))
            case .failure:
                self.dependencies.analytics.log(ReceiveConsumerEvent.customQRCodeCreationError)
                self.presenter.presentError()
            }
        }
    }

    func close() {
        presenter.didNextStep(action: .close)
    }
}
