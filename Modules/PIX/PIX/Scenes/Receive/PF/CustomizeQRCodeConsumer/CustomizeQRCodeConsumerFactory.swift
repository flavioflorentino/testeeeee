import UIKit

enum CustomizeQRCodeConsumerFactory {
    static func make(
        userKeys: [RegisteredKey],
        selectedKey: RegisteredKey,
        origin: PIXReceiveOrigin,
        callQrCodeFlowClosure: ((UIImage, RegisteredKey, String) -> Void)?
    ) -> CustomizeQRCodeConsumerViewController {
        let container = DependencyContainer()
        let service = ReceiveService(dependencies: container)
        let coordinator = CustomizeQRCodeConsumerCoordinator()
        coordinator.callQrCodeFlowClosure = callQrCodeFlowClosure
        let presenter = CustomizeQRCodeConsumerPresenter(coordinator: coordinator)
        let interactor = CustomizeQRCodeConsumerInteractor(
            userKeys: userKeys,
            selectedKey: selectedKey,
            service: service,
            presenter: presenter,
            dependencies: container,
            origin: origin
        )
        let viewController = CustomizeQRCodeConsumerViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
