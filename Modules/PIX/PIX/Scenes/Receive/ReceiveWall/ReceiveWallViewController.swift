import Foundation
import UI

protocol ReceiveWallDisplay: AnyObject {
    func display(viewModel: ReceiveWallViewModel)
}

struct ReceiveWallViewModel {
    var title: String
    var description: String
    var image: UIImage
    var buttonTitle: String
}

final class ReceiveWallViewController: ViewController<ReceiveWallInteracting, UIView> {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .grayscale700())
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale600())
        return label
    }()

    private lazy var imageContainerView: UIView = {
        let view = UIView()
        view.layer.borderWidth = Border.light
        view.layer.borderColor = Colors.grayscale100.color.cgColor
        view.layer.cornerRadius = CornerRadius.medium
        return view
    }()

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var actionButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didNextStep), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()

    convenience init(interactor: ReceiveWallInteracting, hasCloseButton: Bool) {
        self.init(interactor: interactor)
        if hasCloseButton {
            configNavigation()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.viewDidLoad()
    }

    override func buildViewHierarchy() {
        view.addSubviews(titleLabel, descriptionLabel, imageContainerView, imageView, actionButton)
    }

    override func configureViews() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationItem.backBarButtonItem = UIBarButtonItem()
    }

    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.lessThanOrEqualToSuperview().offset(-Spacing.base02)
        }

        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }

        imageContainerView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(actionButton.snp.top).offset(-Spacing.base03)
        }
        
        imageContainerView.snp.contentHuggingVerticalPriority = UILayoutPriority.defaultLow.rawValue

        imageView.snp.makeConstraints {
            $0.center.equalTo(imageContainerView)
        }

        actionButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base02)
        }
    }
    
    private func configNavigation() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: Strings.General.close, style: .plain, target: self, action: #selector(close))
    }

    @objc
    private func didNextStep() {
        interactor.didNextStep()
    }
    
    @objc
    private func close() {
        interactor.close()
    }
}

extension ReceiveWallViewController: ReceiveWallDisplay {
    func display(viewModel: ReceiveWallViewModel) {
        titleLabel.text = viewModel.title
        descriptionLabel.text = viewModel.description
        actionButton.setTitle(viewModel.buttonTitle, for: .normal)
        imageView.image = viewModel.image
    }
}
