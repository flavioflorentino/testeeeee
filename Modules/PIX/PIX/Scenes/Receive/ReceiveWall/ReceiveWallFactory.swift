import Foundation
import UIKit

enum ReceiveWallFactory {
    static func make(type: ReceiveWallDisplayAction, hasCloseButton: Bool, origin: PIXReceiveOrigin) -> ReceiveWallViewController {
        let coordinator = ReceiveWallCoordinator()
        let presenter = ReceiveWallPresenter(coordinator: coordinator)
        let dependencies = DependencyContainer()
        let interactor = ReceiveWallInteractor(presenter: presenter, dependencies: dependencies, type: type, origin: origin)
        let viewController = ReceiveWallViewController(interactor: interactor, hasCloseButton: hasCloseButton)

        presenter.viewController = viewController
        coordinator.viewController = viewController
        return viewController
    }
}
