import Foundation
import AnalyticsModule

protocol ReceiveWallInteracting {
    func viewDidLoad()
    func didNextStep()
    func close()
}

final class ReceiveWallInteractor: ReceiveWallInteracting {
    typealias Dependencies = HasAnalytics
    private let wallType: ReceiveWallDisplayAction
    private let presenter: ReceiveWallPresenting
    private let dependencies: Dependencies
    private let origin: PIXReceiveOrigin

    init(presenter: ReceiveWallPresenting, dependencies: Dependencies, type: ReceiveWallDisplayAction, origin: PIXReceiveOrigin) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.wallType = type
        self.origin = origin
    }

    func viewDidLoad() {
        let screen: ReceiveScreen = wallType == .identityNotValidated ? .identityValidation : .registerKey
        dependencies.analytics.log(ReceiveConsumerEvent.userNavigated(origin: origin, screen: screen))
        presenter.present(type: wallType)
    }

    func didNextStep() {
        dependencies.analytics.log(ReceiveConsumerEvent.receiveNotValid(type: .init(action: wallType)))
        switch wallType {
        case .identityNotValidated:
            presenter.didNextStep(action: .identityNotValidated)
        case .noKeysRegistered:
            presenter.didNextStep(action: .noKeysRegistered)
        }
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
}
