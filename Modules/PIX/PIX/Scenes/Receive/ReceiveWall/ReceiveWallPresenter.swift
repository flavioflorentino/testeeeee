import Foundation
import UIKit

enum ReceiveWallDisplayAction {
    case noKeysRegistered
    case identityNotValidated
}

protocol ReceiveWallPresenting {
    var viewController: ReceiveWallDisplay? { get set }
    func didNextStep(action: ReceiveWallAction)
    func present(type: ReceiveWallDisplayAction)
}

final class ReceiveWallPresenter: ReceiveWallPresenting {
    private let coordinator: ReceiveWallCoordinating
    weak var viewController: ReceiveWallDisplay?

    init(coordinator: ReceiveWallCoordinating) {
        self.coordinator = coordinator
    }

    func didNextStep(action: ReceiveWallAction) {
        coordinator.perform(action: action)
    }
    
    func present(type: ReceiveWallDisplayAction) {
        let viewModel: ReceiveWallViewModel

        switch type {
        case .noKeysRegistered:
            viewModel = .init(title: Strings.Receive.ReceiveWall.RegisterKey.title,
                              description: Strings.Receive.ReceiveWall.RegisterKey.description,
                              image: Assets.registerKey.image,
                              buttonTitle: Strings.Receive.ReceiveWall.RegisterKey.buttonTitle)
        case .identityNotValidated:
            viewModel = .init(title: Strings.Receive.ReceiveWall.ConfirmIdentity.title,
                              description: Strings.Receive.ReceiveWall.ConfirmIdentity.description,
                              image: Assets.identity.image,
                              buttonTitle: Strings.Receive.ReceiveWall.ConfirmIdentity.buttonTitle)
        }
        viewController?.display(viewModel: viewModel)
    }
}
