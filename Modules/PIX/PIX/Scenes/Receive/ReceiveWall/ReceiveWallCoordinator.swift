import UIKit
import Foundation

enum ReceiveWallAction: Equatable {
    case noKeysRegistered
    case identityNotValidated
    case close
}

protocol ReceiveWallCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: ReceiveWallAction)
}

final class ReceiveWallCoordinator {
    weak var viewController: UIViewController?
}

extension ReceiveWallCoordinator: ReceiveWallCoordinating {
    func perform(action: ReceiveWallAction) {
        switch action {
        case .identityNotValidated:
            viewController?.navigationController?.dismiss(animated: true) {
                PIXSetup.identityValidation?.openIdentityValidation()
            }
        case .noKeysRegistered:
            let controller = KeyManagerConsumerFactory.make(from: .receive, checkForPreRegisteredKeys: false)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .close:
            viewController?.navigationController?.dismiss(animated: true)
        }
    }
}
