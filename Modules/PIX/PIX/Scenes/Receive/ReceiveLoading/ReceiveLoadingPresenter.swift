import Foundation
import UIKit
import AssetsKit

protocol ReceiveLoadingPresenting {
    var viewController: ReceiveLoadingDisplay? { get set }
    func presentLoading()
    func presentError()
    func didNextStep(action: ReceiveLoadingAction)
}

final class ReceiveLoadingPresenter: ReceiveLoadingPresenting {
    weak var viewController: ReceiveLoadingDisplay?
    private let coordinator: ReceiveLoadingCoordinating

    init(coordinator: ReceiveLoadingCoordinating) {
        self.coordinator = coordinator
    }

    func presentLoading() {
        viewController?.displayLoading()
    }

    func presentError() {
        let viewModel = ReceiveLoadingErrorViewModel(image: Resources.Illustrations.iluFalling.image,
                                                     title: Strings.Receive.ReceiveLoading.Error.title,
                                                     description: Strings.Receive.ReceiveLoading.Error.message,
                                                     buttonTitle: Strings.Receive.ReceiveLoading.Error.button)
        viewController?.displayError(viewModel: viewModel)
    }

    func didNextStep(action: ReceiveLoadingAction) {
        coordinator.perform(action: action)
    }
}
