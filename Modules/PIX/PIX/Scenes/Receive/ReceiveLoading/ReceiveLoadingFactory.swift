import Foundation

enum ReceiveLoadingFactory {
    static func make(origin: PIXReceiveOrigin) -> ReceiveLoadingViewController {
        let container = DependencyContainer()
        let coordinator = ReceiveLoadingCoordinator()
        let presenter = ReceiveLoadingPresenter(coordinator: coordinator)
        let service = ReceiveService(dependencies: container)
        let consumerService = KeyManagerConsumerService(dependencies: container)
        let interactor = ReceiveLoadingInteractor(
            presenter: presenter,
            service: service,
            consumerService: consumerService,
            origin: origin
        )
        let viewController = ReceiveLoadingViewController(interactor: interactor)

        presenter.viewController = viewController
        coordinator.viewController = viewController
        return viewController
    }
}
