import Foundation
import UIKit

struct ReceiveLoadingErrorViewModel: Equatable {
    let image: UIImage?
    let title: String
    let description: String
    let buttonTitle: String
}
