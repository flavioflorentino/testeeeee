import Foundation
import UIKit

struct ReceivePaymentsData: Equatable {
    var keys: [RegisteredKey]
    var userData: PixUserData
    var selectedKey: RegisteredKey
    var qrCodeImage: UIImage
    var qrCodeText: String

    static func == (lhs: ReceivePaymentsData, rhs: ReceivePaymentsData) -> Bool {
        lhs.keys == rhs.keys &&
        lhs.userData == rhs.userData &&
        lhs.selectedKey == rhs.selectedKey
    }
}
