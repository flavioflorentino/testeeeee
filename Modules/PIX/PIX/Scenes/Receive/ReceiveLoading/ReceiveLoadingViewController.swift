import Foundation
import UI

protocol ReceiveLoadingDisplay: AnyObject {
    func displayLoading()
    func displayError(viewModel: ReceiveLoadingErrorViewModel)
}

final class ReceiveLoadingViewController: ViewController<ReceiveLoadingInteracting, UIView> {
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadReceiveRequirements()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.backgroundColor = Colors.backgroundPrimary.color
    }

    override func configureViews() {
        configNavigation()
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationItem.backBarButtonItem = UIBarButtonItem()
    }
    
    private func configNavigation() {
        guard navigationController?.viewControllers.count == 1 else {
            return
        }
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: Strings.General.close, style: .plain, target: self, action: #selector(close))
    }
    
    // MARK: - Actions
    @objc
    private func close() {
        interactor.close()
    }
}

extension ReceiveLoadingViewController: ReceiveLoadingDisplay {
    func displayLoading() {
        let model = StateLoadingViewModel(message: Strings.Receive.ReceiveLoading.title)
        beginState(model: model)
    }

    func displayError(viewModel: ReceiveLoadingErrorViewModel) {
        let model = StatefulErrorViewModel(image: viewModel.image,
                                           content: (title: viewModel.title, description: viewModel.description),
                                           button: (image: UIImage(), title: viewModel.buttonTitle))

        endState {
            self.endState(model: model)
        }
    }
}

extension ReceiveLoadingViewController: StatefulTransitionViewing {
    func didTryAgain() {
        interactor.loadReceiveRequirements()
    }
}
