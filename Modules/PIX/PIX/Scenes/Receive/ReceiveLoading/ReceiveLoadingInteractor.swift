import Foundation

protocol ReceiveLoadingInteracting {
    func loadReceiveRequirements()
    func close()
}

final class ReceiveLoadingInteractor: ReceiveLoadingInteracting {
    private let presenter: ReceiveLoadingPresenting
    private let service: ReceiveServicing
    private let consumerService: KeyManagerConsumerServicing
    private var userData: PixUserData?
    private var registeredKeys = [RegisteredKey]()
    private var selectedKey: RegisteredKey? {
        registeredKeys.first
    }
    private var qrCodeImage: UIImage?
    private var qrCodeText: String?
    private let origin: PIXReceiveOrigin

    init(
        presenter: ReceiveLoadingPresenting,
        service: ReceiveServicing,
        consumerService: KeyManagerConsumerServicing,
        origin: PIXReceiveOrigin
    ) {
        self.presenter = presenter
        self.service = service
        self.consumerService = consumerService
        self.origin = origin
    }

    func loadReceiveRequirements() {
        presenter.presentLoading()
        getUserData {
            self.getRegisteredKeys {
                self.loadQRCode()
            }
        }
    }

    private func getUserData(then completion: @escaping () -> Void) {
        consumerService.getConsumerData { result in
            switch result {
            case let .success(userData):
                self.userData = userData
                completion()
            case .failure:
                self.presenter.presentError()
            }
        }
    }

    private func getRegisteredKeys(completion: @escaping () -> Void) {
        guard let userId = consumerService.consumerId else {
            return self.presenter.presentError()
        }

        self.service.listPixKeys(with: "\(userId)") { keyResult in
            switch keyResult {
            case .success(let response):
                let registeredKeys = response.data.map { $0.registeredKeys.first }.compactMap { $0 }
                self.registeredKeys = registeredKeys.filter { $0.statusSlug == .processed }

                if self.registeredKeys.isEmpty {
                    self.presenter.didNextStep(action: .registerKeys(origin: self.origin))
                } else {
                    completion()
                }
            case .failure:
                self.presenter.presentError()
            }
        }
    }

    private func loadQRCode() {
        guard let selectedKey = self.selectedKey else { return presenter.presentError() }
        let request = ReceiveQRCodeRequest(registeredKey: selectedKey)
        self.service.generateQRCode(request: request) { result in
            switch result {
            case .success(let response):
                self.qrCodeImage = UIImage(base64: response.qrCodeInBase64)
                self.qrCodeText = response.qrCodeText
                self.didNextStep()
            case .failure:
                self.presenter.presentError()
            }
        }
    }

    private func didNextStep() {
        guard registeredKeys.isNotEmpty else {
            return presenter.didNextStep(action: .registerKeys(origin: origin))
        }

        guard
            let userData = self.userData,
            let qrCodeImage = self.qrCodeImage,
            let selectedKey = self.selectedKey,
            let qrCodeText = self.qrCodeText
        else {
            return presenter.presentError()
        }

        let output = ReceivePaymentsData(keys: registeredKeys, userData: userData, selectedKey: selectedKey, qrCodeImage: qrCodeImage, qrCodeText: qrCodeText)
        let action: ReceiveLoadingAction = .receivePayment(output: output, origin: origin)
        self.presenter.didNextStep(action: action)
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
}
