import Foundation
import UIKit

enum ReceiveLoadingAction: Equatable {
    case registerKeys(origin: PIXReceiveOrigin)
    case receivePayment(output: ReceivePaymentsData, origin: PIXReceiveOrigin)
    case close
}

protocol ReceiveLoadingCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: ReceiveLoadingAction)
}

final class ReceiveLoadingCoordinator: ReceiveLoadingCoordinating {
    weak var viewController: UIViewController?

    func perform(action: ReceiveLoadingAction) {
        switch action {
        case let .registerKeys(origin):
            let hasCloseButton = viewController?.navigationController?.viewControllers.count == 1
            let controller = ReceiveWallFactory.make(type: .noKeysRegistered, hasCloseButton: hasCloseButton, origin: origin)
            pushViewController(controller)
        case let .receivePayment(output, origin):
            let hasCloseButton = viewController?.navigationController?.viewControllers.count == 1
            let controller = ReceivePaymentsFactory.make(data: output, hasCloseButton: hasCloseButton, origin: origin)
            pushViewController(controller)
        case .close:
            viewController?.navigationController?.dismiss(animated: true)
        }
    }
    
    private func pushViewController(_ controller: UIViewController) {
        guard let viewController = viewController,
              let navigationController = viewController.navigationController,
              let index = navigationController.viewControllers.firstIndex(of: viewController) else {
            return
        }

        navigationController.pushViewController(controller, animated: true)
        navigationController.viewControllers.remove(at: index)
    }
}
