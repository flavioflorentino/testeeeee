import UIKit

public typealias ReceivePaymentsBizOutput = (ReceivePaymentsBizAction) -> Void

public enum ReceivePaymentsBizAction {
    case customizeQRCode(keys: [RegisteredKey])
    case faq(viewController: UIViewController)
}

public enum ReceivePaymentsBizFactory {
    static func make(keys: [RegisteredKey],
                     customQRCode: PixQRCode?,
                     userInfo: KeyManagerBizUserInfo,
                     receiveOutput: @escaping ReceivePaymentsBizOutput) -> ReceivePaymentsViewController {
        let container = DependencyContainer()
        let service: ReceiveBizServicing = ReceiveBizService(dependencies: container)
        let coordinator: ReceivePaymentsBizCoordinating = ReceivePaymentsBizCoordinator(receivePaymentsBizOutput: receiveOutput)
        var presenter: ReceivePaymentsBizPresenting = ReceivePaymentsBizPresenter(coordinator: coordinator)
        let interactor: ReceivePaymentsInteracting = ReceivePaymentsBizInteractor(service: service,
                                                                                  presenter: presenter,
                                                                                  dependencies: container,
                                                                                  userInfo: userInfo,
                                                                                  keys: keys,
                                                                                  customQRCode: customQRCode)
        let viewController = ReceivePaymentsViewController(customQRCode: customQRCode, interactor: interactor, hasCloseButton: false)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
