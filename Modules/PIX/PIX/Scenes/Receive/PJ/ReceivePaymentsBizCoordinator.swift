import UIKit

protocol ReceivePaymentsBizCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ReceivePaymentsBizAction)
    func shareQRCode(activityController: UIActivityViewController)
}

final class ReceivePaymentsBizCoordinator {
    private let receivePaymentsBizOutput: ReceivePaymentsBizOutput
    weak var viewController: UIViewController?
    
    init(receivePaymentsBizOutput: @escaping ReceivePaymentsBizOutput) {
        self.receivePaymentsBizOutput = receivePaymentsBizOutput
    }
}

// MARK: - ReceivePaymentsBizCoordinating
extension ReceivePaymentsBizCoordinator: ReceivePaymentsBizCoordinating {
    func perform(action: ReceivePaymentsBizAction) {
        receivePaymentsBizOutput(action)
    }
    
    func shareQRCode(activityController: UIActivityViewController) {
        viewController?.present(activityController, animated: true)
    }
}
