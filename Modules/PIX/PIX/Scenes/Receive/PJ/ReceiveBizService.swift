import Core
import Foundation

protocol ReceiveBizServicing {
    func generateQRCode(key: RegisteredKey, completion: @escaping (Result<PixQRCode, ApiError>) -> Void)
    func generateQRCode(key: RegisteredKey,
                        amount: Double?,
                        description: String?,
                        identifier: String?,
                        completion: @escaping (Result<PixQRCode, ApiError>) -> Void)
}

final class ReceiveBizService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension ReceiveBizService: ReceiveBizServicing {
    func generateQRCode(key: RegisteredKey, completion: @escaping (Result<PixQRCode, ApiError>) -> Void) {
        generateQRCode(key: key, amount: nil, description: nil, identifier: nil, completion: completion)
    }
    
    func generateQRCode(key: RegisteredKey,
                        amount: Double?,
                        description: String?,
                        identifier: String?,
                        completion: @escaping (Result<PixQRCode, ApiError>) -> Void) {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        let endpoint = PixBizQRCodeServiceEndpoint.generateQRCode(key: key,
                                                                  amount: amount,
                                                                  description: description,
                                                                  identifier: identifier)
        Core.Api<PixQRCode>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
