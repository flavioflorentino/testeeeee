import AnalyticsModule
import Core
import Foundation

final class ReceivePaymentsBizInteractor: NSObject {
    typealias Dependencies = HasAnalytics & HasUserAuthManager & HasMainQueue
    private let dependencies: Dependencies
    private let service: ReceiveBizServicing
    private let presenter: ReceivePaymentsBizPresenting
    private let userKeys: [RegisteredKey]
    private var userInfo: KeyManagerBizUserInfo?
    private let customQRCode: PixQRCode?
    private var selectedReceivementType: ReceivementPixType = .qrcode
    private var qrCodeText: String?
    private var selectedKey: RegisteredKey? {
        didSet {
            presentSelectedKey()
            loadQRCode()
        }
    }

    init(service: ReceiveBizServicing,
         presenter: ReceivePaymentsBizPresenting,
         dependencies: Dependencies,
         userInfo: KeyManagerBizUserInfo,
         keys: [RegisteredKey],
         customQRCode: PixQRCode?) {
        self.service = service
        self.presenter = presenter
        self.userKeys = keys
        self.userInfo = userInfo
        self.customQRCode = customQRCode
        self.dependencies = dependencies
        super.init()
        self.selectedKey = findSelectedKey(keys: keys)
    }
    
    private func findSelectedKey(keys: [RegisteredKey]) -> RegisteredKey? {
        guard let custom = customQRCode else {
            return userKeys.first
        }
        return userKeys.first(where: { $0.keyValue == custom.keyValue })
    }
}

// MARK: - ReceivePaymentsBizInteracting
extension ReceivePaymentsBizInteractor: ReceivePaymentsInteracting {
    func viewDidLoad() {
        getUserInfo()
        presenter.presentStaticTexts()
        showUsername()
        presenter.presentUserKeys(keys: userKeys)
        presentSelectKey()
        presenter.presentReceivementSelectionStyle(newType: selectedReceivementType, previousType: nil)
        presentReceivementTypeContainer(selectedReceivementType)
        guard let customizedQRCode = customQRCode else {
            handleReceivePayment()
            return
        }
        handleCustomizedQRCode(with: customizedQRCode)
    }
    
    func presentSelectKey() {
        guard let selectedKey = selectedKey else {
            return
        }
        presenter.presentSelectedKey(key: selectedKey)
    }
    
    func didSelectKey(viewModel: PIXReceiveKeyViewModel) {
        dependencies.analytics.log(KeyBizTracker(companyName: userInfo?.name ?? "", eventType: .buttonKeyChangedPaymentHub))
        let key = userKeys.first { $0.id == viewModel.id }
        if key != selectedKey {
            selectedKey = key
        }
    }
    
    func shareQRCode(image: UIImage) {
        let event: KeyBizEvent = customQRCode == nil ? .buttonShareQRCodePaymentHub : .buttonShareCustomQRCode
        dependencies.analytics.log(KeyBizTracker(companyName: userInfo?.name ?? "", eventType: event))
        // TODO: arrumar isso
        let text = "Pague para \(userInfo?.name ?? "") pelo Pix, escaneando esse QR Code ou através do \(selectedKey?.keyValue ?? "")"
        let shareSheet = UIActivityViewController(activityItems: [image as Any], applicationActivities: nil)
        presenter.shareQRCode(activityController: shareSheet)
    }
    
    func openFAQ(in viewController: UIViewController) {
        presenter.didNextStep(action: .faq(viewController: viewController))
    }
    
    func loadQRCode() {
        self.presenter.presentLoadingQRCode()
        guard let key = selectedKey else {
            presenter.presentErrorQRCode()
            return
        }
        
        let value = Double(customQRCode?.value ?? "")
        
        service.generateQRCode(key: key,
                               amount: value,
                               description: customQRCode?.description,
                               identifier: customQRCode?.identifier) { [weak self] result in
            switch result {
            case let .success(qrCode):
                self?.loadQRCode(from: qrCode)
            case .failure:
                self?.presenter.presentErrorQRCode()
            }
        }
    }

    func customizeQRCode() {
        dependencies.analytics.log(KeyBizTracker(companyName: userInfo?.name ?? "", eventType: .customQRCodePaymentHub))
        presenter.didNextStep(action: .customizeQRCode(keys: userKeys))
    }

    func saveImage(_ image: UIImage) {
        dependencies.analytics.log(KeyBizTracker(companyName: userInfo?.name ?? "", eventType: .saveCustomQRCodeImage))
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(ReceivePaymentsBizInteractor.image), nil)
    }
    
    func selectReceivementType(_ newType: ReceivementPixType) {
        guard selectedReceivementType != newType else {
            return
        }
        presenter.presentReceivementSelectionStyle(newType: newType, previousType: selectedReceivementType)
        presentReceivementTypeContainer(newType)
        selectedReceivementType = newType
    }
    
    private func presentReceivementTypeContainer(_ type: ReceivementPixType) {
        switch type {
        case .qrcode:
            handleQrCodeType(customQRCode)
        case .copyPaste:
            presenter.presentCopyPasteContainer()
        }
    }
    
    private func handleQrCodeType(_ customQRCode: PixQRCode?) {
        if customQRCode != nil {
            presenter.presentCustomQRCodeContainer()
        } else {
            presenter.presentQRCodeContainer()
        }
    }
    
    func copyCode() {
        guard let qrCodeText = qrCodeText else {
            return
        }
        UIPasteboard.general.string = qrCodeText
        presenter.presentCopyPasteNotification()
    }
    
    func close() {}
}

private extension ReceivePaymentsBizInteractor {
    @objc
    func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        presenter.presentImageAlert(didSave: error == nil)
    }
    
    func handleReceivePayment() {
        loadFAQButton()
        loadQRCode()
        presenter.presentCustomizedQRCodeButton()
    }
    
    func handleCustomizedQRCode(with qrCode: PixQRCode) {
        loadQRCode(from: qrCode)
        guard let key = selectedKey else { return }
        presenter.presentSelectedKey(key: key)
        presenter.presentSaveButton()
    }
    
    func getUserInfo() {
        dependencies.userAuthManagerContract?.getUserInfo(completion: { [weak self] userInfo in
            guard let userInfo = userInfo else { return }
            self?.userInfo = userInfo
        })
    }
    
    func showUsername() {
        guard let userInfo = userInfo else {
            presenter.presentUserName(name: "QR Code")
            return
        }
        presenter.presentUserName(name: userInfo.name)
    }
    
    func loadQRCode(from qrCode: PixQRCode) {
        if let image = UIImage(base64: qrCode.qrCodeInBase64) {
            qrCodeText = qrCode.qrCodeText
            self.presenter.presentQRCode(image: image, profileImageURL: nil, qrCodeText: qrCode.qrCodeText)
        }
    }
    
    func presentSelectedKey() {
        if let selectedKey = selectedKey {
            presenter.presentSelectedKey(key: selectedKey)
        }
    }
    
    func loadFAQButton() {
        presenter.showFAQButton()
    }
}
