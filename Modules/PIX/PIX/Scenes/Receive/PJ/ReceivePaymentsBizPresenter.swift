import Foundation
import UI

protocol ReceivePaymentsBizPresenting {
    var viewController: ReceivePaymentsDisplay? { get set }
    func presentLoadingQRCode()
    func presentErrorQRCode()
    func presentQRCode(image: UIImage, profileImageURL: URL?, qrCodeText: String)
    func presentSelectedKey(key: RegisteredKey)
    func presentUserName(name: String)
    func presentUserKeys(keys: [RegisteredKey])
    func didNextStep(action: ReceivePaymentsBizAction)
    func showFAQButton()
    func shareQRCode(activityController: UIActivityViewController)
    func presentStaticTexts()
    func presentSaveButton()
    func presentCustomizedQRCodeButton()
    func presentImageAlert(didSave: Bool)
    func presentReceivementSelectionStyle(newType: ReceivementPixType, previousType: ReceivementPixType?)
    func presentQRCodeContainer()
    func presentCustomQRCodeContainer()
    func presentCopyPasteContainer()
    func presentCopyPasteNotification()
}

final class ReceivePaymentsBizPresenter: ReceivePaymentsBizPresenting {
    private typealias Localizable = Strings.Receive.CustomizedReceivePayments
    private let coordinator: ReceivePaymentsBizCoordinating
    weak var viewController: ReceivePaymentsDisplay?

    init(coordinator: ReceivePaymentsBizCoordinating) {
        self.coordinator = coordinator
    }
    
    func presentErrorQRCode() {
        viewController?.displayErrorQRCode()
    }

    func presentLoadingQRCode() {
        viewController?.displayLoadingQRCode()
    }

    func presentQRCode(image: UIImage, profileImageURL: URL?, qrCodeText: String) {
        viewController?.displayQRCode(image: image, profileImageURL: profileImageURL, qrCodeText: qrCodeText)
    }

    func presentSelectedKey(key: RegisteredKey) {
        let formattedKey = createViewModel(from: key)?.rowTitle ?? ""
        viewController?.displaySelectedKey(key: formattedKey)
    }

    func presentUserName(name: String) {
        viewController?.displayUserName(name: name)
    }

    func presentUserKeys(keys: [RegisteredKey]) {
        let viewModels = keys.compactMap { createViewModel(from: $0) }
        viewController?.displayUserKeys(keys: viewModels)
    }

    func didNextStep(action: ReceivePaymentsBizAction) {
        coordinator.perform(action: action)
    }
    
    func showFAQButton() {
        viewController?.displayFAQButton()
    }
    
    func shareQRCode(activityController: UIActivityViewController) {
        coordinator.shareQRCode(activityController: activityController)
    }
    
    func presentImageAlert(didSave: Bool) {
        let title = didSave ? Localizable.SaveImage.Success.alertTile : Localizable.SaveImage.Failure.alertTile
        let description = didSave ? Localizable.SaveImage.Success.alertDescription : Localizable.SaveImage.Failure.alertDescription
        viewController?.displayImageSavedAlert(title: title, description: description)
    }
    
    func presentStaticTexts() {
        let title = Strings.Receive.ReceivePayments.title
        let description = Strings.Receive.ReceivePayments.description
        viewController?.displayTexts(title: title, description: description)
    }
    
    func presentSaveButton() {
        let title = Strings.Receive.CustomizedReceivePayments.saveImage
        viewController?.displaySecondaryButton(type: .saveImage(title: title))
    }
    
    func presentCustomizedQRCodeButton() {
        let title = Strings.Receive.ReceivePayments.customizeQRCode
        viewController?.displaySecondaryButton(type: .customizeQRCode(title: title))
    }
    
    func presentReceivementSelectionStyle(newType: ReceivementPixType, previousType: ReceivementPixType?) {
        viewController?.displayButtonStyle(
            backgroundColor: Colors.grayscale750.color,
            titleColor: Colors.white.color,
            index: newType.rawValue
        )
        guard let previousType = previousType else {
            return
        }
        viewController?.displayButtonStyle(
            backgroundColor: Colors.backgroundTertiary.color,
            titleColor: Colors.grayscale600.color,
            index: previousType.rawValue
        )
    }
    
    func presentQRCodeContainer() {
        viewController?.displayQRCodeContainer()
    }
    
    func presentCustomQRCodeContainer() {
        viewController?.displayCustomQRCodeContainer()
    }
    
    func presentCopyPasteContainer() {
        viewController?.displayCopyPasteContainer()
    }
    
    func presentCopyPasteNotification() {
        let message = Strings.Receive.ReceivePayments.copiedCode
        viewController?.displayCopyPasteNotification(message: message)
    }
}

private extension ReceivePaymentsBizPresenter {
    func createViewModel(from key: RegisteredKey) -> PIXReceiveKeyViewModel? {
        let value = configureValue(for: key.keyType, value: key.keyValue)
        let viewModel = PIXReceiveKeyViewModel(id: key.id, type: key.keyType, value: value)
        return viewModel
    }
    
    func configureValue(for type: PixKeyType, value: String) -> String {
        let mask: CustomStringMask
        switch type {
        case .cnpj:
            mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cnpj)
        case .cpf:
            mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf)
        case .phone:
            mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cellPhoneWithDDD)
        default:
            return value
        }
        
        return mask.maskedText(from: value) ?? ""
    }
}
