import Foundation
import UI
import UIKit
import AssetsKit

private extension SharePIXQRCodeView.Layout {
    enum Sizes {
        static let viewWidth: CGFloat = 376
        static let viewHeight: CGFloat = 667
        static let profileImageSize: CGFloat = 45
        static let qrCodeImagSize: CGFloat = 232
        static let logoWidth: CGFloat = 92
        static let logoHeight: CGFloat = 34
        static let containerCornerRadius: CGFloat = 8
    }
}

final class SharePIXQRCodeView: UIView {
    fileprivate enum Layout { }
    
    var profileImage: UIImage? {
        didSet {
            profileImageView.image = profileImage
        }
    }

    var name: String? {
        didSet {
            nameLabel.text = name
        }
    }

    var qrCodeImage: UIImage? {
        didSet {
            qrCodeImageView.image = qrCodeImage
        }
    }

    private lazy var backgroundImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.backgroundQrcode.image
        return imageView
    }()

    private lazy var picPayLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Resources.Logos.consumerLogoWhite.image
        return imageView
    }()

    private lazy var mainContainer: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.white.color
        view.layer.cornerRadius = Layout.Sizes.containerCornerRadius
        return view
    }()

    private lazy var pixLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.pixLogo.image
        return imageView
    }()

    private lazy var qrCodeImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView
            .imageStyle(RoundedImageStyle(size: .small, cornerRadius: .full))
            .with(\.border, .medium(color: .white(.light)))
        imageView.image = Resources.Placeholders.greenAvatarPlaceholder.image
        imageView.clipsToBounds = true
        return imageView
    }()

    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, .grayscale800())
        return label
    }()

    init() {
        let frame = CGRect(x: 0, y: 0, width: Layout.Sizes.viewWidth, height: Layout.Sizes.viewHeight)
        super.init(frame: frame)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        nil
    }
}

extension SharePIXQRCodeView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(backgroundImageView, picPayLogo, mainContainer)
        mainContainer.addSubviews(pixLogo, qrCodeImageView, nameLabel)
        qrCodeImageView.addSubview(profileImageView)
        sendSubviewToBack(backgroundImageView)
    }

    func setupConstraints() {
        snp.makeConstraints {
            $0.width.equalTo(Layout.Sizes.viewWidth)
            $0.height.equalTo(Layout.Sizes.viewHeight)
        }

        backgroundImageView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        picPayLogo.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base06)
            $0.centerX.equalToSuperview()
            $0.width.equalTo(Layout.Sizes.logoWidth)
            $0.height.equalTo(Layout.Sizes.logoHeight)
        }

        mainContainer.snp.makeConstraints {
            $0.top.equalTo(picPayLogo.snp.bottom).offset(Spacing.base07)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.lessThanOrEqualToSuperview()
        }

        pixLogo.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base04)
            $0.centerX.equalToSuperview()
            $0.width.equalTo(Layout.Sizes.logoWidth)
            $0.height.equalTo(Layout.Sizes.logoHeight)
        }

        qrCodeImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Sizes.qrCodeImagSize)
            $0.top.equalTo(pixLogo.snp.bottom).offset(Spacing.base03)
            $0.centerX.equalToSuperview()
        }

        profileImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Sizes.profileImageSize)
            $0.center.equalToSuperview()
        }

        nameLabel.snp.makeConstraints {
            $0.top.equalTo(qrCodeImageView.snp.bottom).offset(Spacing.base04)
            $0.centerX.equalToSuperview()
            $0.leading.greaterThanOrEqualToSuperview().offset(Spacing.base00)
            $0.trailing.lessThanOrEqualToSuperview().offset(Spacing.base00)
            $0.bottom.greaterThanOrEqualToSuperview().offset(-Spacing.base06)
        }
    }
}
