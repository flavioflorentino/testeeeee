import Foundation
import AssetsKit
import UIKit
import UI

final class QRCodeContainerView: UIView {
    private enum Layout {
        static let qrCodeMaxSize: CGFloat = 190
        static let profileImageProportionToQRCode: CGFloat = 0.19
    }

    enum State {
        case loading
        case success(qrcode: UIImage, profileImageURL: URL?)
    }

    enum ButtonType {
        case changeKey
        case registerKey
        case none
    }

    var state: State = .loading {
        didSet {
            setNeedsLayoutUpdate()
        }
    }

    var buttonType: ButtonType = .changeKey {
        didSet {
            configureKeyButton()
        }
    }

    var name: String? {
        set {
            nameLabel.text = newValue
        }
        get {
            nameLabel.text
        }
    }

    var key: String? {
        didSet {
            keyLabel.text = key
        }
    }

    var profileImageURL: URL? {
        didSet {
            profileImageView.setImage(url: profileImageURL, placeholder: placeholderImage)
        }
    }

    var changeKeyAction: (() -> Void)?

    var registerKeyAction: (() -> Void)?

    var currentQRCodeImage: UIImage? {
        qrCodeImageView.image
    }

    var currentProfileImage: UIImage? {
        profileImageView.image
    }

    private lazy var loadingView = LoadingView()

    private lazy var qrCodeImageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        let initialSize = CGSize(width: Layout.qrCodeMaxSize, height: Layout.qrCodeMaxSize)
        view.image = UIGraphicsImageRenderer(size: initialSize).image { rendererContext in
            UIColor.clear.setFill()
            rendererContext.fill(CGRect(origin: .zero, size: initialSize))
        }
        return view
    }()

    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, .grayscale800())
        return label
    }()

    private lazy var keyLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale800())
        return label
    }()

    lazy var changeKeyButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(LinkButtonStyle(size: .small))
        button.buttonStyle(LinkButtonStyle(size: .small))
        return button
    }()

    private lazy var profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.borderColor = Colors.white.lightColor.cgColor
        imageView.layer.borderWidth = 2
        imageView.clipsToBounds = true
        imageView.image = Resources.Placeholders.greenAvatarPlaceholder.image
        return imageView
    }()

    private let placeholderImage: UIImage = Resources.Placeholders.greenAvatarPlaceholder.image
    private let topLayoutGuide = UILayoutGuide()
    private let bottomLayoutGuide = UILayoutGuide()

    init() {
        super.init(frame: .zero)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        profileImageView.layer.cornerRadius = (qrCodeImageView.frame.height * Layout.profileImageProportionToQRCode) / 2
    }

    @objc
    private func changeKey() {
        self.changeKeyAction?()
    }

    @objc
    private func registerKey() {
        self.registerKeyAction?()
    }

    private func setNeedsLayoutUpdate() {
        switch state {
        case .loading:
            changeKeyButton.isHidden = false
            loadingView.isHidden = false
            qrCodeImageView.isHidden = true
        case let .success(qrCodeImage, profileImageURL):
            changeKeyButton.isHidden = false
            loadingView.isHidden = true
            qrCodeImageView.isHidden = false
            qrCodeImageView.image = qrCodeImage
            self.profileImageURL = profileImageURL
        }
    }

    private func configureKeyButton() {
        switch buttonType {
        case .changeKey:
            changeKeyButton.removeTarget(self, action: #selector(registerKey), for: .touchUpInside)
            changeKeyButton.addTarget(self, action: #selector(changeKey), for: .touchUpInside)
            changeKeyButton.setTitle(Strings.Receive.ReceivePayments.changeKey, for: .normal)
            changeKeyButton.buttonStyle(LinkButtonStyle())
        case .registerKey:
            changeKeyButton.removeTarget(self, action: #selector(changeKey), for: .touchUpInside)
            changeKeyButton.addTarget(self, action: #selector(registerKey), for: .touchUpInside)
            changeKeyButton.setTitle(Strings.Receive.ReceivePayments.registerKey, for: .normal)
            changeKeyButton.buttonStyle(LinkButtonStyle())
        case .none:
            changeKeyButton.isHidden = true
        }
    }
}

extension QRCodeContainerView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(qrCodeImageView, nameLabel, keyLabel, changeKeyButton, loadingView)
        qrCodeImageView.addSubview(profileImageView)
        [topLayoutGuide, bottomLayoutGuide].forEach(addLayoutGuide)
    }

    func configureViews() {
        layer.borderWidth = Border.light
        layer.borderColor = Colors.grayscale100.color.cgColor
        layer.cornerRadius = CornerRadius.medium
        configureKeyButton()
    }

    func setupConstraints() {
        topLayoutGuide.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.bottom.equalTo(qrCodeImageView.snp.top)
            $0.height.greaterThanOrEqualTo(Sizing.base00)
            $0.height.equalTo(bottomLayoutGuide)
        }

        qrCodeImageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.leading.greaterThanOrEqualToSuperview().offset(Spacing.base02)
            $0.trailing.lessThanOrEqualToSuperview().offset(-Spacing.base02)
            $0.height.lessThanOrEqualTo(snp.height).multipliedBy(0.55)
            $0.height.lessThanOrEqualTo(Layout.qrCodeMaxSize)
        }

        loadingView.snp.makeConstraints {
            $0.center.equalTo(qrCodeImageView)
        }

        nameLabel.snp.makeConstraints {
            $0.top.equalTo(qrCodeImageView.snp.bottom).offset(Spacing.base01)
            $0.centerX.equalToSuperview()
            $0.leading.greaterThanOrEqualToSuperview().offset(Spacing.base02)
            $0.trailing.lessThanOrEqualToSuperview().offset(-Spacing.base02)
        }

        keyLabel.snp.makeConstraints {
            $0.top.equalTo(nameLabel.snp.bottom).offset(Spacing.base01)
            $0.centerX.equalToSuperview()
            $0.leading.greaterThanOrEqualToSuperview().offset(Spacing.base02)
            $0.trailing.lessThanOrEqualToSuperview().offset(-Spacing.base02)
        }

        keyLabel.snp.contentCompressionResistanceVerticalPriority = UILayoutPriority.required.rawValue

        changeKeyButton.snp.makeConstraints {
            $0.top.equalTo(keyLabel.snp.bottom)
            $0.centerX.equalToSuperview()
            $0.leading.greaterThanOrEqualToSuperview().offset(Spacing.base02)
            $0.trailing.lessThanOrEqualToSuperview().offset(-Spacing.base02)
        }

        bottomLayoutGuide.snp.makeConstraints {
            $0.top.equalTo(changeKeyButton.snp.bottom)
            $0.bottom.equalToSuperview()
        }

        snp.contentHuggingVerticalPriority = UILayoutPriority.defaultLow.rawValue

        profileImageView.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.size.equalTo(qrCodeImageView.snp.height).multipliedBy(Layout.profileImageProportionToQRCode)
        }
    }
}
