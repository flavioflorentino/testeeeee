import AssetsKit
import Foundation
import UIKit
import UI

private enum Layout {
    enum Size {
        static let textMaxHeight: CGFloat = 190
    }
}

final class CopyPasteContainerView: UIView {
    enum State {
        case loading
        case success(qrcodeText: String)
    }
    
    // MARK: - Visual Components
    private lazy var loadingView = LoadingView()

    private lazy var qrcodeTextBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale050.color
        view.cornerRadius = .medium
        return view
    }()
    
    private lazy var qrcodeContentLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var copyButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(LinkButtonStyle(size: .small, icon: nil))
        let attributedText = NSAttributedString(
            string: Strings.Receive.ReceivePayments.copyCode,
            attributes: [
                .underlineStyle: NSUnderlineStyle.thick.rawValue,
                .underlineColor: Colors.brandingBase.color,
                .foregroundColor: Colors.brandingBase.color
            ]
        )
        button.setAttributedTitle(attributedText, for: .normal)
        button.addTarget(self, action: #selector(tapOnCopyCode), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Variables
    var copyCodeAction: (() -> Void)?
    var state: State = .loading {
        didSet {
            setNeedsLayoutUpdate()
        }
    }
    private let topLayoutGuide = UILayoutGuide()
    private let bottomLayoutGuide = UILayoutGuide()

    // MARK: - Life Cycle
    init() {
        super.init(frame: .zero)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setNeedsLayoutUpdate() {
        switch state {
        case .loading:
            loadingView.isHidden = false
            qrcodeTextBackgroundView.isHidden = true
            copyButton.isHidden = true
        case let .success(qrCodeText):
            loadingView.isHidden = true
            qrcodeTextBackgroundView.isHidden = false
            copyButton.isHidden = false
            qrcodeContentLabel.text = qrCodeText
        }
    }
    
    // MARK: - Actions
    @objc
    private func tapOnCopyCode() {
        copyCodeAction?()
    }
}

// MARK: - ViewConfiguration
extension CopyPasteContainerView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(loadingView)
        addSubview(qrcodeTextBackgroundView)
        addSubview(copyButton)
        qrcodeTextBackgroundView.addSubview(qrcodeContentLabel)
        [topLayoutGuide, bottomLayoutGuide].forEach(addLayoutGuide)
    }

    func configureViews() {
        layer.borderWidth = Border.light
        layer.borderColor = Colors.grayscale100.color.cgColor
        layer.cornerRadius = CornerRadius.medium
    }

    func setupConstraints() {
        topLayoutGuide.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.bottom.equalTo(qrcodeTextBackgroundView.snp.top).offset(-Spacing.base02)
            $0.height.greaterThanOrEqualTo(Sizing.base00)
            $0.height.equalTo(bottomLayoutGuide)
        }
        
        loadingView.snp.makeConstraints {
            $0.center.equalTo(qrcodeTextBackgroundView)
        }
        
        qrcodeTextBackgroundView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        qrcodeContentLabel.snp.makeConstraints {
            $0.height.lessThanOrEqualTo(Layout.Size.textMaxHeight)
            $0.top.bottom.equalToSuperview().inset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        copyButton.snp.makeConstraints {
            $0.top.equalTo(qrcodeTextBackgroundView.snp.bottom).offset(Spacing.base03)
            $0.centerX.equalTo(qrcodeTextBackgroundView)
        }
        
        bottomLayoutGuide.snp.makeConstraints {
            $0.top.equalTo(copyButton.snp.bottom).offset(Spacing.base02)
            $0.bottom.equalToSuperview()
        }
        
        snp.contentHuggingVerticalPriority = UILayoutPriority.defaultLow.rawValue
    }
}
