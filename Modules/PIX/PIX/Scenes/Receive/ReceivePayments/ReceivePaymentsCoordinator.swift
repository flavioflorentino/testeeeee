import Foundation
import UIKit

enum ReceivePaymentsAction: Equatable {
    case shareQRCode(activityController: UIActivityViewController)
    case customizeQRCode(userKeys: [RegisteredKey], selectedKey: RegisteredKey, origin: PIXReceiveOrigin)
    case registerNewKey
    case close
}

protocol ReceivePaymentsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ReceivePaymentsAction)
}

final class ReceivePaymentsCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - ReceivePaymentsCoordinating
extension ReceivePaymentsCoordinator: ReceivePaymentsCoordinating {
    func perform(action: ReceivePaymentsAction) {
        switch action {
        case .shareQRCode(let activityController):
            viewController?.present(activityController, animated: true)
        case let .customizeQRCode(userKeys, selectedKey, origin):
            let controller = CustomizeQRCodeConsumerFactory.make(
                userKeys: userKeys,
                selectedKey: selectedKey,
                origin: origin,
                callQrCodeFlowClosure: { [weak self] image, selectedKey, qrCodeText in
                    self?.callCustomQrCodeFlow(image: image, selectedKey: selectedKey, qrCodeText: qrCodeText)
                }
            )
            viewController?.present(UINavigationController(rootViewController: controller), animated: true)
        case .registerNewKey:
            let controller = KeyManagerConsumerFactory.make(from: .receive, checkForPreRegisteredKeys: false)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .close:
            viewController?.navigationController?.dismiss(animated: true)
        }
    }

    private func callCustomQrCodeFlow(image: UIImage, selectedKey: RegisteredKey, qrCodeText: String) {
        let controller = CustomQRCodeFactory.make(qrCodeImage: image, selectedKey: selectedKey, qrCodeText: qrCodeText)
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}
