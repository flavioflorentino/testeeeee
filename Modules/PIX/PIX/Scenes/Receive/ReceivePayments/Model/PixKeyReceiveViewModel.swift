import Foundation
import UI
import UIKit

struct PIXReceiveKeyViewModel {
    var id: String
    var type: PixKeyType
    var value: String
}

extension PIXReceiveKeyViewModel: PickerOption {
    var rowTitle: String? {
        "\(type.receiveTitle): \(value)"
    }

    var rowValue: String? {
        value
    }
}

extension PixConsumerKeyType {
    init?(keyType: PixKeyType) {
        switch keyType {
        case .cnpj:
            return nil
        case .cpf:
            self = .cpf
        case .email:
            self = .email
        case .random:
            self = .random
        case .phone:
            self = .phone
        }
    }
}

private extension PixKeyType {
    var receiveTitle: String {
        switch self {
        case .cpf:
            return Strings.KeySelection.cpf
        case .cnpj:
            return Strings.KeySelection.cnpj
        case .email:
            return Strings.KeySelection.email
        case .phone:
            return Strings.KeySelection.cellphone
        case .random:
            return Strings.KeySelection.randomKeyTitle
        }
    }
}
