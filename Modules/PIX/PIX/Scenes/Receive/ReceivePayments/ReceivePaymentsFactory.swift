import Core
import Foundation
import UIKit

enum ReceivePaymentsFactory {
    static func make(data: ReceivePaymentsData, hasCloseButton: Bool, origin: PIXReceiveOrigin) -> ReceivePaymentsViewController {
        let coordinator = ReceivePaymentsCoordinator()
        let presenter = ReceivePaymentsPresenter(coordinator: coordinator)
        let consumerInstance: PixConsumerContract? = PIXSetup.consumerInstance
        let container = DependencyContainer()
        let service = ReceiveService(dependencies: container)
        let interactor = ReceivePaymentsInteractor(
            presenter: presenter,
            service: service,
            dependencies: container,
            consumerInstance: consumerInstance,
            data: data,
            isCustomQRCodeEnabled: false,
            origin: origin
        )
        let viewController = ReceivePaymentsViewController(customQRCode: nil, interactor: interactor, hasCloseButton: hasCloseButton)

        presenter.viewController = viewController
        coordinator.viewController = viewController
        return viewController
    }
}
