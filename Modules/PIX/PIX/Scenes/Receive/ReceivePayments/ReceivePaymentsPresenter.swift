import Foundation
import UI
import UIKit

protocol ReceivePaymentsPresenting {
    var viewController: ReceivePaymentsDisplay? { get set }
    func presentLoadingQRCode()
    func presentErrorQRCode()
    func presentQRCode(image: UIImage, profileImageURL: URL?, qrCodeText: String)
    func presentSelectedKey(key: RegisteredKey, userData: PixUserData)
    func presentUserName(name: String)
    func presentUserKeys(keys: [RegisteredKey], userData: PixUserData)
    func presentRegisterKeyButton()
    func presentChangeKeyButton()
    func presentCustomizeQRCodeButton()
    func presentTexts()
    func didNextStep(action: ReceivePaymentsAction)
    func presentReceivementSelectionStyle(newType: ReceivementPixType, previousType: ReceivementPixType?)
    func presentQRCodeContainer()
    func presentCustomQRCodeContainer()
    func presentCopyPasteContainer()
    func presentCopyPasteNotification()
}

final class ReceivePaymentsPresenter: ReceivePaymentsPresenting {
    private let coordinator: ReceivePaymentsCoordinating
    weak var viewController: ReceivePaymentsDisplay?

    init(coordinator: ReceivePaymentsCoordinating) {
        self.coordinator = coordinator
    }
    
    func presentErrorQRCode() {
        viewController?.displayErrorQRCode()
    }

    func presentLoadingQRCode() {
        viewController?.displayLoadingQRCode()
    }

    func presentQRCode(image: UIImage, profileImageURL: URL?, qrCodeText: String) {
        viewController?.displayQRCode(image: image, profileImageURL: profileImageURL, qrCodeText: qrCodeText)
    }

    func presentSelectedKey(key: RegisteredKey, userData: PixUserData) {
        let viewModel = createViewModel(from: key, userData: userData)
        if let formattedKey = viewModel?.rowTitle {
            viewController?.displaySelectedKey(key: formattedKey)
        }
    }

    func presentUserName(name: String) {
        viewController?.displayUserName(name: name)
    }

    func presentUserKeys(keys: [RegisteredKey], userData: PixUserData) {
        let viewModels = keys.compactMap { createViewModel(from: $0, userData: userData) }
        viewController?.displayUserKeys(keys: viewModels)
    }

    func didNextStep(action: ReceivePaymentsAction) {
        coordinator.perform(action: action)
    }

    func presentRegisterKeyButton() {
        viewController?.displayRegisterKeyButton()
    }

    func presentChangeKeyButton() {
        viewController?.displayChangeKeyButton()
    }

    func presentCustomizeQRCodeButton() {
        let title = Strings.Receive.ReceivePayments.customizeQRCode
        viewController?.displaySecondaryButton(type: .customizeQRCode(title: title))
    }

    func presentTexts() {
        let title = Strings.Receive.ReceivePayments.title
        let description = Strings.Receive.ReceivePayments.description
        viewController?.displayTexts(title: title, description: description)
    }

    private func createViewModel(from key: RegisteredKey, userData: PixUserData) -> PIXReceiveKeyViewModel? {
        guard let consumerKeyType = PixConsumerKeyType(keyType: key.keyType) else { return nil }
        let value = key.keyType == .random ? key.keyValue : userData.formatForType(keyType: consumerKeyType)
        let viewModel = PIXReceiveKeyViewModel(id: key.id, type: key.keyType, value: value)
        return viewModel
    }
    
    func presentReceivementSelectionStyle(newType: ReceivementPixType, previousType: ReceivementPixType?) {
        viewController?.displayButtonStyle(
            backgroundColor: Colors.grayscale750.color,
            titleColor: Colors.white.color,
            index: newType.rawValue
        )
        guard let previousType = previousType else {
            return
        }
        viewController?.displayButtonStyle(
            backgroundColor: Colors.backgroundTertiary.color,
            titleColor: Colors.grayscale600.color,
            index: previousType.rawValue
        )
    }
    
    func presentQRCodeContainer() {
        viewController?.displayQRCodeContainer()
    }
    
    func presentCustomQRCodeContainer() {
        viewController?.displayCustomQRCodeContainer()
    }
    
    func presentCopyPasteContainer() {
        viewController?.displayCopyPasteContainer()
    }
    
    func presentCopyPasteNotification() {
        let message = Strings.Receive.ReceivePayments.copiedCode
        viewController?.displayCopyPasteNotification(message: message)
    }
}
