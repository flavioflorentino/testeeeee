import Foundation
import UIKit
import AnalyticsModule

protocol ReceivePaymentsInteracting {
    func viewDidLoad()
    func didSelectKey(viewModel: PIXReceiveKeyViewModel)
    func shareQRCode(image: UIImage)
    func customizeQRCode()
    func saveImage(_ image: UIImage)
    func registerNewKey()
    func openFAQ(in viewController: UIViewController)
    func selectReceivementType(_ newType: ReceivementPixType)
    func copyCode()
    func close()
}

extension ReceivePaymentsInteracting {
    func openFAQ(in viewController: UIViewController) {}
    func saveImage(_ image: UIImage) {}
    func registerNewKey() {}
}

final class ReceivePaymentsInteractor: ReceivePaymentsInteracting {
    typealias Dependencies = HasAnalytics
    private let presenter: ReceivePaymentsPresenting
    private let service: ReceiveServicing
    private let userKeys: [RegisteredKey]
    private let userData: PixUserData
    private let consumerInstance: PixConsumerContract?
    private let dependencies: Dependencies
    private var selectedReceivementType: ReceivementPixType = .qrcode
    private let isCustomQRCodeEnabled: Bool
    private let origin: PIXReceiveOrigin

    private var qrCodeText: String
    private var currentQRCodeImage: UIImage {
        didSet {
            presentSelectedQRCode()
        }
    }

    private var selectedKey: RegisteredKey {
        didSet {
            presentSelectedKey()
        }
    }

    init(
        presenter: ReceivePaymentsPresenting,
        service: ReceiveServicing,
        dependencies: Dependencies,
        consumerInstance: PixConsumerContract?,
        data: ReceivePaymentsData,
        isCustomQRCodeEnabled: Bool,
        origin: PIXReceiveOrigin
    ) {
        self.userKeys = data.keys
        self.userData = data.userData
        self.presenter = presenter
        self.service = service
        self.consumerInstance = consumerInstance
        self.qrCodeText = data.qrCodeText
        self.selectedKey = data.selectedKey
        self.currentQRCodeImage = data.qrCodeImage
        self.dependencies = dependencies
        self.isCustomQRCodeEnabled = isCustomQRCodeEnabled
        self.origin = origin
    }

    func viewDidLoad() {
        getUserName()
        presenter.presentTexts()
        presenter.presentCustomizeQRCodeButton()
        presenter.presentUserKeys(keys: userKeys, userData: userData)
        presenter.presentQRCode(image: currentQRCodeImage, profileImageURL: self.getProfileImageURL(), qrCodeText: qrCodeText)
        presentSelectedKey()
        checkNumberOfAvailableKeys()
        dependencies.analytics.log(ReceiveConsumerEvent.userNavigated(origin: origin, screen: .mainQRCode))
        presentReceivementTypeContainer(selectedReceivementType)
        presenter.presentReceivementSelectionStyle(newType: selectedReceivementType, previousType: nil)
    }
    
    func shareQRCode(image: UIImage) {
        dependencies.analytics.log(ReceiveConsumerEvent.qrCodeShared)
        let shareSheet = UIActivityViewController(activityItems: [image as Any], applicationActivities: nil)
        presenter.didNextStep(action: .shareQRCode(activityController: shareSheet))
    }

    func didSelectKey(viewModel: PIXReceiveKeyViewModel) {
        dependencies.analytics.log(ReceiveConsumerEvent.changeReceivingKey)
        guard let key = userKeys.first(where: { $0.id == viewModel.id }) else {
            return self.presenter.presentErrorQRCode()
        }
        if key != selectedKey {
            loadQRCode(forKey: key)
        }
    }

    func customizeQRCode() {
        dependencies.analytics.log(ReceiveConsumerEvent.createCustomQRCode)
        presenter.didNextStep(action: .customizeQRCode(userKeys: userKeys, selectedKey: selectedKey, origin: origin))
    }

    func registerNewKey() {
        presenter.didNextStep(action: .registerNewKey)
    }

    private func presentSelectedKey() {
        presenter.presentSelectedKey(key: selectedKey, userData: userData)
    }

    private func presentSelectedQRCode() {
        presenter.presentQRCode(image: currentQRCodeImage, profileImageURL: self.getProfileImageURL(), qrCodeText: qrCodeText)
    }

    private func loadQRCode(forKey key: RegisteredKey) {
        presenter.presentLoadingQRCode()
        let request = ReceiveQRCodeRequest(registeredKey: key)
        self.service.generateQRCode(request: request) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let response):
                guard let image = UIImage(base64: response.qrCodeInBase64) else {
                    return self.presenter.presentErrorQRCode()
                }

                self.currentQRCodeImage = image
                self.selectedKey = key
                self.qrCodeText = response.qrCodeText
            case .failure:
                self.presentSelectedQRCode()
                self.presenter.presentErrorQRCode()
            }
        }
    }

    private func getUserName() {
        let userName = consumerInstance?.name ?? ""
        presenter.presentUserName(name: userName)
    }

    private func getProfileImageURL() -> URL? {
        consumerInstance?.imageURL
    }

    private func checkNumberOfAvailableKeys() {
        userKeys.count == 1 ? presenter.presentRegisterKeyButton() : presenter.presentChangeKeyButton()
    }
    
    func selectReceivementType(_ newType: ReceivementPixType) {
        guard selectedReceivementType != newType else {
            return
        }
        presenter.presentReceivementSelectionStyle(newType: newType, previousType: selectedReceivementType)
        presentReceivementTypeContainer(newType)
        selectedReceivementType = newType
    }
    
    private func presentReceivementTypeContainer(_ type: ReceivementPixType) {
        switch type {
        case .qrcode:
            if isCustomQRCodeEnabled {
                presenter.presentCustomQRCodeContainer()
            } else {
                presenter.presentQRCodeContainer()
            }
        case .copyPaste:
            presenter.presentCopyPasteContainer()
        }
    }
    
    func copyCode() {
        UIPasteboard.general.string = qrCodeText
        presenter.presentCopyPasteNotification()
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
}
