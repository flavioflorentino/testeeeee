import Core
import Foundation
import AssetsKit
import UIKit
import UI

final class QRCodeContainerViewInfo: UIView, ViewConfiguration {
    lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale800.color)
        return label
    }()
    
    lazy var identifierLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale300.color)
        return label
    }()
    
    lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.branding600.color)
        return label
    }()
    
    lazy var infoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        return stackView
    }()
    
    var isEnabled = false
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    func configure(with qrCode: PixQRCode) {
        isEnabled = true
        descriptionLabel.text = qrCode.description
        identifierLabel.text = qrCode.identifier
        
        guard let qrCodeValue = qrCode.value, let value = Double(qrCodeValue) else { return }
        let currencyString = NumberFormatter.toCurrencyString(with: value)
        valueLabel.text = currencyString
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        infoStackView.addArrangedSubview(descriptionLabel)
        infoStackView.addArrangedSubview(identifierLabel)
        addSubviews(infoStackView, valueLabel)
    }
    
    func configureViews() {
        layer.borderWidth = Border.light
        layer.borderColor = Colors.grayscale100.color.cgColor
        layer.cornerRadius = CornerRadius.medium
    }
    
    func setupConstraints() {
        infoStackView.snp.makeConstraints {
            $0.leading.bottom.top.equalToSuperview().inset(Spacing.base02)
        }
        
        valueLabel.snp.makeConstraints {
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.centerY.equalToSuperview()
        }
    }
}
