import AssetsKit
import Foundation
import UI

protocol ReceivePaymentsDisplay: AnyObject {
    func displayUserKeys(keys: [PIXReceiveKeyViewModel])
    func displayLoadingQRCode()
    func displayErrorQRCode()
    func displayQRCode(image: UIImage, profileImageURL: URL?, qrCodeText: String)
    func displaySelectedKey(key: String)
    func displayUserName(name: String)
    func displayRegisterKeyButton()
    func displayChangeKeyButton()
    func displayFAQButton()
    func displayTexts(title: String, description: String)
    func displaySecondaryButton(type: ReceivePaymentsViewController.SecondaryButtonAction)
    func hideChangeKeyButton()
    func displayImageSavedAlert(title: String, description: String)
    func displayButtonStyle(backgroundColor: UIColor, titleColor: UIColor, index: Int)
    func displayQRCodeContainer()
    func displayCustomQRCodeContainer()
    func displayCopyPasteContainer()
    func displayCopyPasteNotification(message: String)
}

enum ReceivementPixKey {
    case cellphone
    case cpf
    case email
    case random
}

enum ReceivementPixType: Int, CaseIterable {
    case qrcode
    case copyPaste
    
    var text: String {
        switch self {
        case .qrcode:
            return Strings.Receive.ReceivePayments.qrCode
        case .copyPaste:
            return Strings.Receive.ReceivePayments.copyPaste
        }
    }
}

private enum Layout {
    enum Size {
        static let buttonHeight: CGFloat = 56
    }
}

final class ReceivePaymentsViewController: ViewController<ReceivePaymentsInteracting, UIView> {
    enum SecondaryButtonAction: Equatable {
        case saveImage(title: String)
        case customizeQRCode(title: String)
    }

    // MARK: - Visual Components
    private lazy var contentScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = Colors.backgroundPrimary.color
        scrollView.showsHorizontalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        stackView.compatibleLayoutMargins = EdgeInsets(
            top: Spacing.base01,
            leading: Spacing.base02,
            bottom: Spacing.base02,
            trailing: Spacing.base02
        )
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale600())
        label.numberOfLines = 2
        label.adjustsFontSizeToFitWidth = true
        label.setContentHuggingPriority(.required, for: .vertical)
        return label
    }()

    private lazy var qrCodeContainerView = QRCodeContainerView()
    private lazy var customQRCodeView = QRCodeContainerViewInfo()
    private lazy var copyPasteView = CopyPasteContainerView()
    
    private lazy var qrCodeStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.spacing = -1
        stackView.setContentHuggingPriority(.defaultLow, for: .vertical)
        return stackView
    }()
    
    private lazy var buttonsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base01
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private lazy var sharedButtonsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base02
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var receivementTypeButtons = ReceivementPixType.allCases.map(createButton)

    private lazy var shareQRCodeButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.Receive.ReceivePayments.shareQRCode, for: .normal)
        button.addTarget(self, action: #selector(shareQRCode), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()

    private lazy var secondaryButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(LinkButtonStyle())
        return button
    }()

    /// Not actually shown on screen. Used only to display picker upon change key button press
    private lazy var dummyTextField: UITextField = {
        let textfield = UITextField()
        textfield.frame = .zero
        textfield.inputView = self.keysPicker
        textfield.inputAccessoryView = doneToolbar
        return textfield
    }()

    private lazy var keysPicker = PickerInputView()

    private lazy var doneToolbar: DoneToolBar = {
        let toolbar = DoneToolBar(doneText: Strings.General.ok)
        toolbar.doneDelegate = self
        return toolbar
    }()
    
    // MARK: - Life Cycle
    convenience init(customQRCode: PixQRCode?, interactor: ReceivePaymentsInteracting, hasCloseButton: Bool) {
        self.init(interactor: interactor)
        if hasCloseButton {
            configNavigation()
        }
        guard let customQRCode = customQRCode else {
            return
        }
        customQRCodeView.configure(with: customQRCode)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        bindEvents()
        interactor.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    override func buildViewHierarchy() {
        view.addSubview(contentScrollView)
        view.addSubview(dummyTextField)
        contentScrollView.addSubview(contentStackView)
        contentStackView.addArrangedSubview(descriptionLabel)
        contentStackView.addArrangedSubview(buttonsStackView)
        contentStackView.addArrangedSubview(qrCodeStackView)
        contentStackView.addArrangedSubview(sharedButtonsStackView)
        sharedButtonsStackView.addArrangedSubview(shareQRCodeButton)
        sharedButtonsStackView.addArrangedSubview(secondaryButton)
        qrCodeStackView.addArrangedSubview(copyPasteView)
        qrCodeStackView.addArrangedSubview(qrCodeContainerView)
        qrCodeStackView.addArrangedSubview(customQRCodeView)
        receivementTypeButtons.forEach(buttonsStackView.addArrangedSubview)
    }

    override func setupConstraints() {
        contentScrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentStackView.snp.makeConstraints {
            $0.edges.width.equalToSuperview()
            $0.height.greaterThanOrEqualTo(view.layoutMarginsGuide)
        }
        
        buttonsStackView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.buttonHeight)
        }
        
        qrCodeContainerView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
        }
        
        copyPasteView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
        }
        
        customQRCodeView.snp.makeConstraints {
            $0.height.equalTo(Sizing.base09)
            $0.leading.trailing.equalToSuperview()
        }
    }

    private func bindEvents() {
        qrCodeContainerView.changeKeyAction = { [weak self] in
            self?.dummyTextField.becomeFirstResponder()
        }

        qrCodeContainerView.registerKeyAction = { [weak self] in
            self?.interactor.registerNewKey()
        }
        
        copyPasteView.copyCodeAction = { [weak self] in
            self?.interactor.copyCode()
        }
    }
    
    private func createButton(_ type: ReceivementPixType) -> UIButton {
        let button = UIButton()
        button.setTitle(type.text, for: .normal)
        button.titleLabel?.lineBreakMode = .byWordWrapping
        button.textAlignment = .center
        button.titleLabel?.font = Typography.caption(.highlight).font()
        button.setTitleColor(Colors.grayscale600.color, for: .normal)
        button.tag = type.rawValue
        button.addTarget(self, action: #selector(tapOnButton), for: .touchUpInside)
        button.viewStyle(CardViewStyle())
        return button
    }

    private func configNavigation() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: Strings.General.close, style: .plain, target: self, action: #selector(close))
    }
    
    // MARK: - Actions
    @objc
    private func close() {
        interactor.close()
    }

    @objc
    private func tapOnButton(_ sender: UIButton) {
        guard let type = ReceivementPixType(rawValue: sender.tag) else {
            return
        }
        interactor.selectReceivementType(type)
    }
    
    @objc
    private func shareQRCode() {
        guard let qrCodeImage = createQRCodeImage() else { return }
        interactor.shareQRCode(image: qrCodeImage)
    }

    @objc
    private func customizeQRCode() {
        interactor.customizeQRCode()
    }

    @objc
    private func saveImage() {
        if let image = createQRCodeImage() {
            interactor.saveImage(image)
        }
    }
    
    @objc
    private func didTapFAQ() {
        interactor.openFAQ(in: self)
    }
}

extension ReceivePaymentsViewController: DoneToolBarDelegate {
    public func didTouchOnDone() {
        if let key = keysPicker.selectedOption as? PIXReceiveKeyViewModel {
            interactor.didSelectKey(viewModel: key)
        }
    }
}

// MARK: ReceivePaymentsDisplay
extension ReceivePaymentsViewController: ReceivePaymentsDisplay {
    func displayErrorQRCode() {
        let popUp = createPopUp()
        showPopup(popUp)
    }

    func displayQRCode(image: UIImage, profileImageURL: URL?, qrCodeText: String) {
        shareQRCodeButton.isEnabled = true
        qrCodeContainerView.state = .success(qrcode: image, profileImageURL: profileImageURL)
        copyPasteView.state = .success(qrcodeText: qrCodeText)
    }

    func displayUserKeys(keys: [PIXReceiveKeyViewModel]) {
        keysPicker.options = keys
    }

    func displayLoadingQRCode() {
        shareQRCodeButton.isEnabled = false
        qrCodeContainerView.state = .loading
        copyPasteView.state = .loading
    }

    func displaySelectedKey(key: String) {
        qrCodeContainerView.key = key
    }

    func displayUserName(name: String) {
        qrCodeContainerView.name = name
    }

    func displayRegisterKeyButton() {
        qrCodeContainerView.buttonType = .registerKey
    }

    func displayChangeKeyButton() {
        qrCodeContainerView.buttonType = .changeKey
    }

    func hideChangeKeyButton() {
        qrCodeContainerView.buttonType = .none
    }

    private func createPopUp() -> PopupViewController {
        let popUp = PopupViewController(title: Strings.Receive.ReceivePayments.QrCode.Error.title,
                                        description: Strings.General.failedComms)

        let okAction = PopupAction(title: Strings.General.gotIt, style: .fill)
        popUp.addAction(okAction)
        return popUp
    }

    private func createQRCodeImage() -> UIImage? {
        let shareQRCodeView = SharePIXQRCodeView()
        shareQRCodeView.qrCodeImage = qrCodeContainerView.currentQRCodeImage
        shareQRCodeView.profileImage = qrCodeContainerView.currentProfileImage
        shareQRCodeView.name = qrCodeContainerView.name
        view.addSubview(shareQRCodeView)
        shareQRCodeView.snp.makeConstraints { $0.top.equalTo(view.snp.bottom) }
        let image = shareQRCodeView.snapshot
        shareQRCodeView.removeFromSuperview()
        return image
    }

    func displayFAQButton() {
        let button = UIButton()
        button.setImage(Resources.Icons.icoQuestion.image, for: .normal)
        button.addTarget(self, action: #selector(didTapFAQ), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: button)
        navigationItem.rightBarButtonItem = barButton
    }
    
    func displayTexts(title: String, description: String) {
        self.title = title
        descriptionLabel.text = description
    }

    func displaySecondaryButton(type: SecondaryButtonAction) {
        switch type {
        case let .customizeQRCode(title):
            secondaryButton.setTitle(title, for: .normal)
            secondaryButton.buttonStyle(LinkButtonStyle())
            secondaryButton.removeTarget(self, action: #selector(saveImage), for: .touchUpInside)
            secondaryButton.addTarget(self, action: #selector(customizeQRCode), for: .touchUpInside)
        case let .saveImage(title):
            secondaryButton.setTitle(title, for: .normal)
            secondaryButton.buttonStyle(LinkButtonStyle())
            secondaryButton.removeTarget(self, action: #selector(customizeQRCode), for: .touchUpInside)
            secondaryButton.addTarget(self, action: #selector(saveImage), for: .touchUpInside)
        }
    }

    func displayImageSavedAlert(title: String, description: String) {
        let popup = PopupViewController(title: title, description: description)
        let okAction = PopupAction(title: Strings.General.gotIt, style: .fill)
        popup.addAction(okAction)
        showPopup(popup)
    }
    
    func displayButtonStyle(backgroundColor: UIColor, titleColor: UIColor, index: Int) {
        let button = receivementTypeButtons[index]
        button.backgroundColor = backgroundColor
        button.setTitleColor(titleColor, for: .normal)
    }
    
    func displayQRCodeContainer() {
        qrCodeContainerView.isHidden = false
        customQRCodeView.isHidden = true
        copyPasteView.isHidden = true
    }
    
    func displayCustomQRCodeContainer() {
        qrCodeContainerView.isHidden = false
        customQRCodeView.isHidden = false
        copyPasteView.isHidden = true
    }
    
    func displayCopyPasteContainer() {
        qrCodeContainerView.isHidden = true
        customQRCodeView.isHidden = true
        copyPasteView.isHidden = false
    }
    
    func displayCopyPasteNotification(message: String) {
        let snackBar = ApolloSnackbar(text: message, iconType: .success)
        showSnackbar(snackBar)
    }
}
