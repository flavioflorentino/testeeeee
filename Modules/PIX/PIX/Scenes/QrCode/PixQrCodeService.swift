import Core
import Foundation

public protocol PixQrCodeServicing {
    func validate(scannedText: String, completion: @escaping (Result<PixQrCodeAccount, ApiError>) -> Void)
}

public final class PixQrCodeService {
    private let dependencies: HasMainQueue

    public init(dependencies: HasMainQueue) {
        self.dependencies = dependencies
    }
}

// MARK: - PixQrCodeServicing
extension PixQrCodeService: PixQrCodeServicing {
    public func validate(scannedText: String, completion: @escaping (Result<PixQrCodeAccount, ApiError>) -> Void) {
        let endpoint = PixServiceEndpoint.qrCode(scannedText: scannedText)
        Api<PixQrCodeAccount>(endpoint: endpoint).execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                completion(mappedResult)
            }
        }
    }
}
