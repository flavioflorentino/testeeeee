import UIKit

public enum BackButtonSide {
    case left
    case right
}

public enum ScannerType {
    case `default`
    case pix
    case parking
    case p2m
}

public protocol QrCodeScanning: UIViewController {
    init(backButtonSide: BackButtonSide, hasMyCodeBottomSheet: Bool, scannerType: ScannerType)
}
