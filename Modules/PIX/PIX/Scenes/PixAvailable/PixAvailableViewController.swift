import UI
import UIKit

protocol PixAvailableDisplaying: AnyObject {
}

private extension PixAvailableViewController.Layout {
    enum Font {
        static let normal = UIFont.systemFont(ofSize: 14)
        static let bold = UIFont.boldSystemFont(ofSize: 14)
    }
    enum Size {
        static let image: CGFloat = 145
        static let imageSmaller: CGFloat = 120
        static let smallScreenHeight: CGFloat = 568
    }
}

final class PixAvailableViewController: ViewController<PixAvailableInteracting, UIView> {
    fileprivate enum Layout {}
    
    private lazy var closeButton = UIBarButtonItem(title: Strings.General.close,
                                                   style: .plain,
                                                   target: self,
                                                   action: #selector(didTapCloseButton))
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var textsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = isSmallScreenDevice ? Spacing.base01 : Spacing.base02
        return stackView
    }()
    
    private lazy var actionsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = isSmallScreenDevice ? Spacing.base01 : Spacing.base02
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale600())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private let isSmallScreenDevice = UIScreen.main.bounds.height <= Layout.Size.smallScreenHeight
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func buildViewHierarchy() {
        navigationItem.leftBarButtonItem = closeButton
        
        view.addSubview(imageView)
        view.addSubview(textsStackView)
        view.addSubview(actionsStackView)
        
        textsStackView.addArrangedSubview(titleLabel)
        textsStackView.addArrangedSubview(descriptionLabel)
    }
    
    override func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top).offset(isSmallScreenDevice ? Spacing.base04 : Spacing.base06)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(isSmallScreenDevice ? Layout.Size.imageSmaller : Layout.Size.image)
        }
        
        textsStackView.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(isSmallScreenDevice ? Spacing.base02 : Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        actionsStackView.snp.makeConstraints {
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).inset(isSmallScreenDevice ? Spacing.base02 : Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        
        imageView.image = Assets.iluPicpayCompanyPlusPix.image
        titleLabel.text = Strings.Hub.Pj.title
        descriptionLabel.attributedText = createDescriptionAttributedString()
        
        let firstButton = FeedbackButtonViewModel(title: Strings.Hub.Pj.firstButton,
                                                  action: .tryPix,
                                                  style: AnyButtonStyle<UIButton>(style: PrimaryButtonStyle()))
        let secondButton = FeedbackButtonViewModel(title: Strings.Hub.Pj.secondButton,
                                                   action: .close,
                                                   style: AnyButtonStyle<UIButton>(style: SecondaryButtonStyle()))
        let buttons: [FeedbackButtonViewModel] = [firstButton, secondButton]
                                                   
        buttons.forEach { [weak self] viewModel in
            self?.actionsStackView.addArrangedSubview(FeedbackButton(viewModel: viewModel, delegate: self))
        }
    }
    
    func createDescriptionAttributedString() -> NSAttributedString {
        let text = Strings.Hub.Pj.description
        let attText = text.attributedStringWithFont(primary: Layout.Font.normal, secondary: Layout.Font.bold)
        attText.paragraph(aligment: .center, lineSpace: Sizing.base00)
        attText.font(text: Strings.Hub.Pj.Description.firstBold, font: Layout.Font.bold)
        attText.font(text: Strings.Hub.Pj.Description.secondBold, font: Layout.Font.bold)
        return attText
    }
    
    @objc
    private func didTapCloseButton() {
        interactor.didClose()
    }
}

extension PixAvailableViewController: FeedbackButtonDelegate {
    func feedbackButtonDidTap(action: FeedbackAction) {
        interactor.execute(action: action)
    }
}
