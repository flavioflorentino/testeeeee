import Foundation

protocol PixAvailablePresenting: AnyObject {
    var viewController: PixAvailableDisplaying? { get set }
    func didNextStep(action: FeedbackAction)
}

final class PixAvailablePresenter {
    private let coordinator: PixAvailableCoordinating
    weak var viewController: PixAvailableDisplaying?

    init(coordinator: PixAvailableCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - PixAvailablePresenting
extension PixAvailablePresenter: PixAvailablePresenting {
    func didNextStep(action: FeedbackAction) {
        coordinator.perform(action: action)
    }
}
