import AnalyticsModule
import Core
import Foundation

protocol PixAvailableInteracting: AnyObject {
    func didClose()
    func execute(action: FeedbackAction)
}

final class PixAvailableInteractor {
    typealias Dependencies = HasAnalytics & HasKVStore
    private let dependencies: Dependencies

    private let presenter: PixAvailablePresenting

    init(presenter: PixAvailablePresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
    
    func setWelcomePagesVisualized() {
        dependencies.kvStore.setBool(true, with: KVKey.isPixAvailableFeedbackVisualized)
    }
}

// MARK: - PixAvailableInteracting
extension PixAvailableInteractor: PixAvailableInteracting {
    func execute(action: FeedbackAction) {
        if action == .tryPix {
            setWelcomePagesVisualized()
        }
        presenter.didNextStep(action: action)
    }
    
    func didClose() {
        presenter.didNextStep(action: .close)
    }
}
