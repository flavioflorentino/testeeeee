import Foundation
import UIKit

typealias PixAvailableOutput = (FeedbackAction) -> Void

enum PixAvailableFactory {
    static func make(pixAvailableOutput: @escaping PixAvailableOutput) -> PixAvailableViewController {
        let container = DependencyContainer()
        let coordinator: PixAvailableCoordinating = PixAvailableCoordinator(pixAvailableOutput: pixAvailableOutput)
        let presenter: PixAvailablePresenting = PixAvailablePresenter(coordinator: coordinator)
        let interactor = PixAvailableInteractor(presenter: presenter, dependencies: container)
        let viewController = PixAvailableViewController(interactor: interactor)

        coordinator.viewController = viewController

        return viewController
    }
}
