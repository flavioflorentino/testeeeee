import UIKit

protocol PixAvailableCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: FeedbackAction)
}

final class PixAvailableCoordinator {
    weak var viewController: UIViewController?
    private var pixAvailableOutput: PixAvailableOutput

    init(pixAvailableOutput: @escaping PixAvailableOutput) {
        self.pixAvailableOutput = pixAvailableOutput
    }
}

// MARK: - PixAvailableCoordinating
extension PixAvailableCoordinator: PixAvailableCoordinating {
    func perform(action: FeedbackAction) {
        pixAvailableOutput(action)
    }
}
