import UIKit

protocol QRCodeScannerCoordinating: AnyObject {
    func perform(action: QRCodeScannerAction)
}

final class QRCodeScannerCoordinator {
    private let output: QRCodeScannerOutput

    init(output: @escaping QRCodeScannerOutput) {
        self.output = output
    }
}

// MARK: - QRCodeScannerCoordinating
extension QRCodeScannerCoordinator: QRCodeScannerCoordinating {
    func perform(action: QRCodeScannerAction) {
        output(action)
    }
}
