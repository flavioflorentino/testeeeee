import AssetsKit
import UI
import UIKit

protocol QRCodeScannerDisplaying: AnyObject {
    func displayError()
}

private extension QRCodeScannerViewController.Layout {
    enum Offset {
        static let minimumY: CGFloat = UIScreen.main.bounds.height * 0.75
        static let maximumY: CGFloat = UIScreen.main.bounds.height * 0.54
    }
}

final class QRCodeScannerViewController: ViewController<QRCodeScannerInteracting, UIView> {
    fileprivate enum Layout { }
    
    private lazy var qrCodeBackgroundView = QRCodeScannerBackgroundView()
    
    private lazy var scannerView: QRCodeScanner = {
        let scanner = QRCodeScanner()
        scanner.delegate = self
        return scanner
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        scannerView.resetSetup()
    }

    override func buildViewHierarchy() {
        view.addSubview(scannerView)
        view.addSubview(qrCodeBackgroundView)
        let controller = QRCodeFAQViewController {
            self.interactor.faq(viewController: self)
        }

        controller.setupSheetPresentation(minOffsetY: Layout.Offset.minimumY,
                                          maxOffsetY: Layout.Offset.maximumY)
        addChild(controller)
        view.addSubview(controller.view)
        controller.didMove(toParent: self)
    }
    
    override func setupConstraints() {
        scannerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        qrCodeBackgroundView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        title = Strings.QRCodeScanner.title
        navigationController?.navigationBar.tintColor = Colors.white.color
    }
}

extension QRCodeScannerViewController: QRCodeScannerDelegate {
    func scanningDidFail() {
        displayError()
    }
    
    func scanningSucceeded(with code: String?) {
        interactor.decode(code: code)
    }
}

extension QRCodeScannerViewController: QRCodeScannerDisplaying {
    func displayError() {
        let popup = PopupViewController(
            title: Strings.General.somethingWentWrong,
            description: Strings.KeyManager.errorDescription,
            preferredType: .image(Resources.Illustrations.iluError.image)
        )
        
        let confirmation = PopupAction(
            title: Strings.General.gotIt,
            style: .fill
        ) {
            self.scannerView.resetSetup()
        }
        
        popup.didCloseDismiss = {
            self.scannerView.resetSetup()
        }
        
        popup.addAction(confirmation)
        
        present(popup, animated: true, completion: nil)
    }
}
