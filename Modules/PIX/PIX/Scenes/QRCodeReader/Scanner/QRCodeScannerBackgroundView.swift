import UI
import UIKit

private extension QRCodeScannerBackgroundView.Layout {
    enum Multiplier {
        static let qrCodeSize: CGFloat = 0.51
        static let topHeight: CGFloat = 0.23
        static let background: CGFloat = 0.5
    }
}

final class QRCodeScannerBackgroundView: UIView {
    fileprivate enum Layout {}
    
    private lazy var topBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.black.color.withAlphaComponent(Layout.Multiplier.background)
        return view
    }()
    
    private lazy var bottomBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.black.color.withAlphaComponent(Layout.Multiplier.background)
        return view
    }()
    
    private lazy var leftBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.black.color.withAlphaComponent(Layout.Multiplier.background)
        return view
    }()
    
    private lazy var rightBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.black.color.withAlphaComponent(Layout.Multiplier.background)
        return view
    }()
    
    private lazy var qrCodeImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.qrCodeScanner.image
        return imageView
    }()
    
    private lazy var imageStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [leftBackgroundView, qrCodeImageView, rightBackgroundView])
        stackView.axis = .horizontal
        stackView.distribution = .fill
        return stackView
    }()
    
    private lazy var backgroundStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [topBackgroundView, imageStackView, bottomBackgroundView])
        stackView.axis = .vertical
        stackView.distribution = .fill
        return stackView
    }()
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension QRCodeScannerBackgroundView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(backgroundStackView)
    }
    
    func setupConstraints() {
        backgroundStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        topBackgroundView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalToSuperview().multipliedBy(Layout.Multiplier.topHeight)
        }

        qrCodeImageView.snp.makeConstraints {
            $0.size.equalTo(self.snp.width).multipliedBy(Layout.Multiplier.qrCodeSize)
            $0.centerX.equalToSuperview()
        }

        bottomBackgroundView.snp.makeConstraints {
            $0.top.equalTo(qrCodeImageView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
}
