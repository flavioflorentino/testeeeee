import AnalyticsModule
import Foundation

protocol QRCodeScannerInteracting: AnyObject {
    func decode(code: String?)
    func faq(viewController: UIViewController)
}

final class QRCodeScannerInteractor {
    typealias Dependencies = HasUserAuthManager & HasAnalytics
    private let dependencies: Dependencies

    private let presenter: QRCodeScannerPresenting
    private let userInfo: KeyManagerBizUserInfo?

    init(presenter: QRCodeScannerPresenting, dependencies: Dependencies, userInfo: KeyManagerBizUserInfo?) {
        self.userInfo = userInfo
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - QRCodeScannerInteracting
extension QRCodeScannerInteractor: QRCodeScannerInteracting {
    func decode(code: String?) {
        guard
            let code = code,
            code.lowercased().contains("br.gov.bcb.pix")
        else {
            presenter.showGenericError()
            return
        }
        presenter.didNextStep(action: .decode(code: code))
    }
    
    func faq(viewController: UIViewController) {
        sendEvent(.faqInQrCode)
        presenter.didNextStep(action: .faq(viewController))
    }

    func sendEvent(_ event: KeyBizEvent) {
        self.dependencies.analytics.log(KeyBizTracker(companyName: userInfo?.name ?? "", eventType: event))
    }
}
