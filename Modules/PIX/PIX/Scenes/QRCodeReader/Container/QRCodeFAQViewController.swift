import UI
import UIKit

enum QRCodeFAQState {
    case initial
    case full
}

private extension QRCodeFAQViewController.Layout {
    enum Multiplier {
        static let finalVelocity: CGFloat = 0.1
        static let duration: Double = 0.5
        static let springDamping: CGFloat = 0.8
        static let initialVelocity: CGFloat = 1.0
    }
}

final class QRCodeFAQViewController: UIViewController {
    fileprivate enum Layout {}
    
    private lazy var barIndicator: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle(cornerRadius: .light))
            .with(\.backgroundColor, .grayscale400())
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .grayscale700())
            .with(\.text, Strings.QRCodeScanner.Faq.title)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale700())
            .with(\.text, Strings.QRCodeScanner.Faq.description)
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var faqButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(LinkButtonStyle())
        button.setTitle(Strings.QRCodeScanner.Faq.button, for: .normal)
        button.addTarget(self, action: #selector(didTapFAQButton), for: .touchUpInside)
        return button
    }()
    
    private var distanceThingerToTop: CGFloat = 0
    private var minOffsetY: CGFloat = 0
    private var maxOffsetY: CGFloat = 0
    
    private var midY: CGFloat {
        (maxOffsetY + minOffsetY) / 2
    }
    
    private var amplitude: CGFloat {
        maxOffsetY - minOffsetY
    }
    
    private var state: QRCodeFAQState = .initial
    private let actionButtonOutput: () -> Void
    
    init(actionButtonOutput: @escaping () -> Void) {
        self.actionButtonOutput = actionButtonOutput
        super.init(nibName: nil, bundle: nil)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupSheetPresentation(minOffsetY: CGFloat, maxOffsetY: CGFloat) {
        self.minOffsetY = minOffsetY
        self.maxOffsetY = maxOffsetY
        
        let initialPoint = CGPoint(x: 0, y: maxOffsetY)
        view.frame = CGRect(origin: initialPoint, size: .zero)
        
        view.frame.origin.y = minOffsetY
                
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerHandler(_:)))
        view.addGestureRecognizer(panGesture)
    }
}

extension QRCodeFAQViewController: ViewConfiguration {
    func buildViewHierarchy() {
        view.addSubviews(barIndicator, titleLabel, descriptionLabel, faqButton)
    }
    
    func setupConstraints() {
        barIndicator.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.centerX.equalToSuperview()
            $0.height.equalTo(Sizing.base00)
            $0.width.equalTo(Sizing.base05)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(barIndicator.snp.bottom).offset(Spacing.base03)
            $0.centerX.equalToSuperview()
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        faqButton.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(-Spacing.base03)
            $0.centerX.equalToSuperview()
        }
    }
    
    func configureViews() {
        view.viewStyle(RoundedViewStyle(cornerRadius: .strong))
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    private func gestureRecognizerStateBegan(touchPoint: CGPoint) {
        distanceThingerToTop = touchPoint.y - view.frame.origin.y
    }
    
    private func gestureRecognizerStateChanged(touchPoint: CGPoint) {
        let y = touchPoint.y - distanceThingerToTop
        view.frame.origin.y = limitedValue(y, toRange: 0.0...view.frame.height)
    }
    
    private func limitedValue(_ value: CGFloat, toRange range: ClosedRange<CGFloat>) -> CGFloat {
        max(range.lowerBound, min(range.upperBound, value))
    }
    
    private func gestureRecognizerStateEnded(touchPoint: CGPoint, velocity: CGFloat) {
        let y = touchPoint.y - distanceThingerToTop
        let tendency = y + velocity * Layout.Multiplier.finalVelocity
        let finalPosition: QRCodeFAQState = tendency < midY ? .initial : .full
        animateView(to: finalPosition)
    }
    
    private func animateView(to finalPosition: QRCodeFAQState) {
        let finalPositionY: CGFloat = finalPosition == .full ? minOffsetY : maxOffsetY
        
        UIView.animate(withDuration: Layout.Multiplier.duration,
                       delay: 0,
                       usingSpringWithDamping: Layout.Multiplier.springDamping,
                       initialSpringVelocity: Layout.Multiplier.initialVelocity,
                       options: .allowUserInteraction) {
            self.view.frame.origin.y = finalPositionY
        }
    }
}

@objc
private extension QRCodeFAQViewController {
    func didTapFAQButton() {
        actionButtonOutput()
    }
    
    func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        view.endEditing(true)
        let touchPoint = sender.location(in: view?.window)
        
        switch sender.state {
        case .began:
            gestureRecognizerStateBegan(touchPoint: touchPoint)
        case .changed:
            gestureRecognizerStateChanged(touchPoint: touchPoint)
        case .ended, .cancelled:
            gestureRecognizerStateEnded(touchPoint: touchPoint, velocity: sender.velocity(in: view.window).y)
        default:
            break
        }
    }
}
