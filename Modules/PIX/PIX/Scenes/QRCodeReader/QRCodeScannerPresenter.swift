import Foundation

protocol QRCodeScannerPresenting: AnyObject {
    var viewController: QRCodeScannerDisplaying? { get set }
    func showGenericError()
    func didNextStep(action: QRCodeScannerAction)
}

final class QRCodeScannerPresenter {
    private let coordinator: QRCodeScannerCoordinating
    weak var viewController: QRCodeScannerDisplaying?

    init(coordinator: QRCodeScannerCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - QRCodeScannerPresenting
extension QRCodeScannerPresenter: QRCodeScannerPresenting {
    func showGenericError() {
        viewController?.displayError()
    }
    
    func didNextStep(action: QRCodeScannerAction) {
        coordinator.perform(action: action)
    }
}
