import UIKit

typealias QRCodeScannerOutput = (QRCodeScannerAction) -> Void

enum QRCodeScannerAction {
    case decode(code: String)
    case faq(UIViewController)
}

enum QRCodeScannerFactory {
    static func make(userInfo: KeyManagerBizUserInfo?, qrCodeOutput: @escaping QRCodeScannerOutput) -> QRCodeScannerViewController {
        let container = DependencyContainer()
        let coordinator: QRCodeScannerCoordinating = QRCodeScannerCoordinator(output: qrCodeOutput)
        let presenter: QRCodeScannerPresenting = QRCodeScannerPresenter(coordinator: coordinator)
        let interactor = QRCodeScannerInteractor(presenter: presenter, dependencies: container, userInfo: userInfo)
        let viewController = QRCodeScannerViewController(interactor: interactor)

        presenter.viewController = viewController

        return viewController
    }
}
