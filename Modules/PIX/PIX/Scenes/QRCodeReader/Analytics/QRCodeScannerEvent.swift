import AnalyticsModule

enum QRCodeScannerEvent: AnalyticsKeyProtocol {
    case faq(sellerId: String, userName: String, date: String)
    
    var name: String {
        "Pix - FAQ in Qr Code"
    }
    
    private var properties: [String: Any] {
        if case let .faq(sellerId, userName, date) = self {
            return [
                "seller_id": sellerId,
                "user_name": userName,
                "data": date
            ]
        }
        return [:]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.mixPanel])
    }
}
