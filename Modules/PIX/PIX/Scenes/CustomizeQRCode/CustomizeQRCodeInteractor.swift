import Foundation
import AnalyticsModule
import Core

protocol CustomizeQRCodeInteracting: AnyObject {
    func loadPixKeys()
    func didSelectKey(viewModel: PIXReceiveKeyViewModel)
    func registerNewKey()
    func checkDescriptionLenght(with text: String)
    func editValueText(with text: String)
    func createQRCode(identifier: String, value: String)
}

final class CustomizeQRCodeInteractor {
    typealias Dependencies = HasAnalytics & HasUserAuthManager
    private let dependencies: Dependencies

    private let service: ReceiveBizServicing
    private let presenter: CustomizeQRCodePresenting
    
    private let userKeys: [RegisteredKey]
    private var selectedKey: RegisteredKey?
    
    private var currentQRCodeDescription: String?
    private var userInfo: KeyManagerBizUserInfo?
    
    private let maxQRCodeDescriptionLength = 100

    init(keys: [RegisteredKey],
         service: ReceiveBizServicing,
         presenter: CustomizeQRCodePresenting,
         dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.userKeys = keys
    }
}

// MARK: - CustomizeQRCodeInteracting
extension CustomizeQRCodeInteractor: CustomizeQRCodeInteracting {
    func loadPixKeys() {
        presenter.presentUserKeys(keys: userKeys)
        getUserInfo()
    }
    
    func didSelectKey(viewModel: PIXReceiveKeyViewModel) {
        let key = userKeys.first { $0.id == viewModel.id }
        if key != selectedKey {
            selectedKey = key
            presenter.presentSelectedKey(key: viewModel)
            presenter.toggleCreateQRCodeButton(shouldEnable: true)
        }
    }
    
    func registerNewKey() {
        presenter.openRegisterNewKey()
    }
    
    func checkDescriptionLenght(with text: String) {
        let count: Int = {
            let currentTextCount = currentQRCodeDescription?.count ?? 0
            if text.isEmpty {
                return currentTextCount - 1
            }
            return currentTextCount + text.count
        }()
        if count > maxQRCodeDescriptionLength {
            presenter.deleteDescriptionTextFieldBackward()
            return
        }
        
        currentQRCodeDescription = text
        presenter.presentDescriptionTextField(with: text, maxLength: maxQRCodeDescriptionLength)
    }
    
    func editValueText(with text: String) {
        let valueText = text.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        presenter.presentValueTextField(with: valueText)
    }
    
    func createQRCode(identifier: String, value: String) {
        guard let selectedKey = self.selectedKey else { return }
        let valueDouble = NumberFormatter.toNumber(from: value)
        
        let event = KeyBizEvent.createCustomQRCode(method: selectedKey.keyType,
                                                   value: valueDouble,
                                                   description: currentQRCodeDescription ?? "",
                                                   identifier: identifier)
        dependencies.analytics.log(KeyBizTracker(companyName: userInfo?.name ?? "", eventType: event))
        
        presenter.startLoading()
        service.generateQRCode(key: selectedKey,
                               amount: valueDouble,
                               description: currentQRCodeDescription ?? "",
                               identifier: identifier) { [weak self] result in
            guard let self = self else { return }
            self.presenter.stopLoading()
            switch result {
            case let .success(qrCode):
                self.presenter.presentCustomizedQRCode(with: qrCode, keys: self.userKeys)
            case .failure:
                self.presenter.presentError()
            }
        }
    }
}

private extension CustomizeQRCodeInteractor {
    func getUserInfo() {
        dependencies.userAuthManagerContract?.getUserInfo { [weak self] userInfo in
            guard let userInfo = userInfo else { return }
            self?.userInfo = userInfo
        }
    }
}
