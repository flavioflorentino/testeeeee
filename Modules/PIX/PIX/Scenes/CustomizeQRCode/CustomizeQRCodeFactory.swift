import UIKit

typealias CustomQRCodeOutput = (CustomizeQRCodeAction) -> Void

enum CustomizeQRCodeFactory {
    static func make(keys: [RegisteredKey], output: @escaping CustomQRCodeOutput) -> CustomizeQRCodeViewController {
        let container = DependencyContainer()
        let service: ReceiveBizServicing = ReceiveBizService(dependencies: container)
        let coordinator: CustomizeQRCodeCoordinating = CustomizeQRCodeCoordinator(output: output)
        let presenter: CustomizeQRCodePresenting = CustomizeQRCodePresenter(coordinator: coordinator)
        let interactor = CustomizeQRCodeInteractor(keys: keys, service: service, presenter: presenter, dependencies: container)
        let viewController = CustomizeQRCodeViewController(interactor: interactor)

        presenter.viewController = viewController

        return viewController
    }
}
