import UI
import UIKit

protocol CustomizeQRCodeDisplaying: AnyObject {
    func displayUserKeys(keys: [PIXReceiveKeyViewModel])
    func displayKeySelected(text: String)
    func displayDescriptionCount(text: String)
    func displayDescriptionTextField(text: String)
    func deleteDescriptionTextFieldBackward()
    func displayValueTextField(text: String)
    func deleteValueTextFieldBackward()
    func enableCreateQRCodeButton(shouldEnable: Bool)
    func startLoading()
    func stopLoading()
    func presentError()
}

private extension CustomizeQRCodeViewController.Layout {
    enum Size {
        static let offsetKeyboard: CGFloat = 50
    }
    
    enum DescriptionRange {
        static let min = 0
        static let max = 100
    }
}

final class CustomizeQRCodeViewController: ViewController<CustomizeQRCodeInteracting, UIView>, LoadingViewProtocol {
    fileprivate typealias Localizable = Strings.Receive.CustomizeQRCode
    fileprivate enum Layout { }
    
    var loadingView = LoadingView()
    
    private var errorPopUp: PopupViewController = {
        let popUp = PopupViewController(title: Strings.KeyManager.error,
                                        description: Strings.KeyManager.errorDescription)

        let okAction = PopupAction(title: Strings.General.gotIt, style: .fill)
        popUp.addAction(okAction)
        return popUp
    }()
    
    private lazy var descriptionLabel: UILabel = {
        var label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale600())
            .with(\.textAlignment, .left)
            .with(\.text, Localizable.description)
        return label
    }()
    
    private lazy var arrowDownIconLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, .grayscale700())
            .with(\.text, Iconography.angleDown.rawValue)
        return label
    }()
    
    private var inputs: [UIPPFloatingTextField] = []
    
    private lazy var pixKeyTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.inputView = self.keysPicker
        textField.inputAccessoryView = doneToolbar
        textField.rightView = arrowDownIconLabel
        textField.rightViewMode = .always
        return textField
    }()
    
    private lazy var keysPicker = PickerInputView()
    
    private lazy var doneToolbar: DoneToolBar = {
        let toolbar = DoneToolBar(doneText: Strings.General.ok)
        toolbar.doneDelegate = self
        return toolbar
    }()
    
    private lazy var registerNewKeyButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.registerNewKey, for: .normal)
        button
            .buttonStyle(LinkButtonStyle(size: .small))
            .with(\.textAlignment, .left)
        button.addTarget(self, action: #selector(didTapCreateNewKey), for: .touchUpInside)
        return button
    }()
    
    private lazy var optionalInformationLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .grayscale800())
        label.text = Localizable.optionalInformation
        return label
    }()
    
    private lazy var identifierTextField = UIPPFloatingTextField()
    
    private lazy var descriptionTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.maxlength = Layout.DescriptionRange.max
        textField.addTarget(self, action: #selector(checkDescriptionTextField), for: .editingChanged)
        return textField
    }()
    
    private lazy var descriptionLengthLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale700())
        label.text = Localizable.descriptionRule(Layout.DescriptionRange.min, Layout.DescriptionRange.max)
        return label
    }()
    
    private lazy var valueTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.keyboardType = .numberPad
        textField.addTarget(self, action: #selector(beginEditingValueTextField), for: .editingChanged)
        return textField
    }()
    
    private lazy var informationView: UIView = {
        let view = UIView()
        view
            .viewStyle(BackgroundViewStyle(color: .grayscale050()))
            .with(\.cornerRadius, .medium)
        return view
    }()
    
    private lazy var informationLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .left)
            .with(\.text, Localizable.additionalInformation)
            .with(\.numberOfLines, 2)
        return label
    }()
    
    private lazy var createQRCodeButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.createQRCode, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapCreateQRCode), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.keyboardDismissMode = .onDrag
        return scrollView
    }()
    
    private lazy var contentView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.loadPixKeys()
        addObservers()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        informationView.addSubview(informationLabel)
        contentView.addSubviews(descriptionLabel,
                                pixKeyTextField,
                                registerNewKeyButton,
                                optionalInformationLabel,
                                identifierTextField,
                                descriptionTextField,
                                descriptionLengthLabel,
                                valueTextField,
                                informationView,
                                createQRCodeButton)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom)
            $0.leading.trailing.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.width.equalTo(view.compatibleSafeArea.width)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        pixKeyTextField.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        registerNewKeyButton.snp.makeConstraints {
            $0.top.equalTo(pixKeyTextField.snp.bottom).offset(Spacing.base03)
            $0.leading.equalToSuperview().inset(Spacing.base02)
            $0.trailing.lessThanOrEqualTo(view.snp.trailing).offset(-Spacing.base02)
        }
        
        optionalInformationLabel.snp.makeConstraints {
            $0.top.equalTo(registerNewKeyButton.snp.bottom).offset(Spacing.base05)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        identifierTextField.snp.makeConstraints {
            $0.top.equalTo(optionalInformationLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        descriptionTextField.snp.makeConstraints {
            $0.top.equalTo(identifierTextField.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        descriptionLengthLabel.snp.makeConstraints {
            $0.top.equalTo(descriptionTextField.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        valueTextField.snp.makeConstraints {
            $0.top.equalTo(descriptionLengthLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        informationView.snp.makeConstraints {
            $0.top.equalTo(valueTextField.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        informationLabel.snp.makeConstraints {
            $0.edges.equalTo(informationView).inset(Spacing.base02)
        }
        
        createQRCodeButton.snp.makeConstraints {
            $0.top.equalTo(informationView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
        }
    }

    override func configureViews() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.isTranslucent = false
        title = Localizable.title
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        configure(textField: pixKeyTextField, placeholder: Localizable.pixKeyPlaceholder, type: .pixKey)
        configure(textField: identifierTextField, placeholder: Localizable.identifierPlaceholder, type: .identifier)
        configure(textField: descriptionTextField, placeholder: Localizable.descriptionPlaceholder, type: .description)
        configure(textField: valueTextField, placeholder: Localizable.valuePlaceholder, type: .value)
    }
}

// MARK: - Private Extension
private extension CustomizeQRCodeViewController {
    enum TextFieldType: Int {
        case pixKey = 0
        case identifier
        case description
        case value
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIWindow.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIWindow.keyboardWillHideNotification,
            object: nil
        )
    }
    
    func configure(textField: UIPPFloatingTextField, placeholder: String, type: TextFieldType) {
        textField.placeholder = placeholder
        textField.placeholderColor = Colors.grayscale300.color
        textField.title = placeholder
        textField.titleColor = Colors.grayscale300.color
        textField.selectedTitle = placeholder
        textField.selectedTitleColor = Colors.branding400.color
        textField.textColor = Colors.grayscale700.color
        textField.font = Typography.bodyPrimary(.default).font()
        textField.layer.borderWidth = Border.none
        textField.delegate = self
        textField.tag = type.rawValue
        if case .value = type {
            textField.returnKeyType = .done
        } else {
            textField.returnKeyType = .next
        }
        inputs.append(textField)
    }
    
    private func updateKeyboardConstraint(userInfo: [AnyHashable: Any], inset: CGFloat) {
        guard let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
            let animationCurve = (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue
            else {
                return
        }
        
        scrollView.snp.updateConstraints {
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).inset(inset)
        }
        
        UIView.animate(
            withDuration: animationDuration,
            delay: 0,
            options: UIView.AnimationOptions(rawValue: animationCurve),
            animations: {
                self.view.layoutIfNeeded()
            },
            completion: nil
        )
    }
    
    @objc
    func keyboardWillShow(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo,
            let keyboardSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            else {
                return
        }
        
        updateKeyboardConstraint(userInfo: userInfo, inset: keyboardSize.size.height)
    }
    
    @objc
    func keyboardWillHide(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        updateKeyboardConstraint(userInfo: userInfo, inset: 0.0)
    }
    
    @objc
    func checkDescriptionTextField(_ textField: UIPPFloatingTextField) {
        interactor.checkDescriptionLenght(with: textField.text ?? "")
    }
    
    @objc
    func beginEditingValueTextField(_ textField: UIPPFloatingTextField) {
        interactor.editValueText(with: textField.text ?? "")
    }
    
    @objc
    func didTapCreateNewKey() {
        interactor.registerNewKey()
    }
    
    @objc
    func didTapCreateQRCode() {
        interactor.createQRCode(identifier: identifierTextField.text ?? "", value: valueTextField.text ?? "")
    }
}

// MARK: - CustomizeQRCodeDisplaying
extension CustomizeQRCodeViewController: CustomizeQRCodeDisplaying {
    func displayUserKeys(keys: [PIXReceiveKeyViewModel]) {
        keysPicker.options = keys
        keysPicker.selectedOption = keys.first
    }
    
    func displayKeySelected(text: String) {
        pixKeyTextField.text = text
    }
    
    func displayDescriptionCount(text: String) {
        descriptionLengthLabel.text = text
    }
    
    func displayDescriptionTextField(text: String) {
        descriptionTextField.text = text
    }
    
    func deleteDescriptionTextFieldBackward() {
        descriptionTextField.deleteBackward()
    }
    
    func displayValueTextField(text: String) {
        valueTextField.text = text
    }
    
    func deleteValueTextFieldBackward() {
        valueTextField.deleteBackward()
    }
    
    func enableCreateQRCodeButton(shouldEnable: Bool) {
        createQRCodeButton.isEnabled = shouldEnable
    }
    
    func startLoading() {
        startLoadingView()
    }
    
    func stopLoading() {
        stopLoadingView()
    }
    
    func presentError() {
        showPopup(errorPopUp)
    }
}

// MARK: - DoneToolBarDelegate
extension CustomizeQRCodeViewController: DoneToolBarDelegate {
    func didTouchOnDone() {
        if let key = keysPicker.selectedOption as? PIXReceiveKeyViewModel {
            interactor.didSelectKey(viewModel: key)
        }
    }
}

// MARK: - UITextFieldDelegate
extension CustomizeQRCodeViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case TextFieldType.value.rawValue:
            textField.resignFirstResponder()
            return true
        default:
            inputs[textField.tag + 1].becomeFirstResponder()
            return false
        }
    }
}
