import Foundation
import Core
import UI

protocol CustomizeQRCodePresenting: AnyObject {
    var viewController: CustomizeQRCodeDisplaying? { get set }
    func presentUserKeys(keys: [RegisteredKey])
    func openRegisterNewKey()
    func presentSelectedKey(key: PIXReceiveKeyViewModel)
    func presentDescriptionTextField(with text: String, maxLength: Int)
    func deleteDescriptionTextFieldBackward()
    func presentValueTextField(with text: String)
    func toggleCreateQRCodeButton(shouldEnable: Bool)
    func startLoading()
    func stopLoading()
    func presentCustomizedQRCode(with qrCode: PixQRCode, keys: [RegisteredKey])
    func presentError()
}

final class CustomizeQRCodePresenter {
    private let coordinator: CustomizeQRCodeCoordinating
    weak var viewController: CustomizeQRCodeDisplaying?

    init(coordinator: CustomizeQRCodeCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - CustomizeQRCodePresenting
extension CustomizeQRCodePresenter: CustomizeQRCodePresenting {
    func presentUserKeys(keys: [RegisteredKey]) {
        let viewModels = keys.compactMap { createViewModel(from: $0) }
        viewController?.displayUserKeys(keys: viewModels)
    }
    
    func openRegisterNewKey() {
        coordinator.perform(action: .keyManager)
    }
    
    func presentSelectedKey(key: PIXReceiveKeyViewModel) {
        viewController?.displayKeySelected(text: key.rowTitle ?? "")
    }
    
    func presentDescriptionTextField(with text: String, maxLength: Int) {
        viewController?.displayDescriptionTextField(text: text)
        
        let descriptionCountText = Strings.Receive.CustomizeQRCode.descriptionRule(text.count, maxLength)
        viewController?.displayDescriptionCount(text: descriptionCountText)
    }
    
    func deleteDescriptionTextFieldBackward() {
        viewController?.deleteDescriptionTextFieldBackward()
    }
    
    func presentValueTextField(with text: String) {
        guard let amount = Int(text) else {
            viewController?.deleteValueTextFieldBackward()
            return
        }
        
        let number = Decimal(amount) / Decimal(100)
        let currencyString = NumberFormatter.toCurrencyString(with: number)
        
        viewController?.displayValueTextField(text: currencyString ?? Strings.General.zeroAmount)
    }
    
    func toggleCreateQRCodeButton(shouldEnable: Bool) {
        viewController?.enableCreateQRCodeButton(shouldEnable: shouldEnable)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func presentCustomizedQRCode(with qrCode: PixQRCode, keys: [RegisteredKey]) {
        coordinator.perform(action: .customizedQRCode(qrCode: qrCode, keys: keys))
    }
    
    func presentError() {
        viewController?.presentError()
    }
}

private extension CustomizeQRCodePresenter {
    func createViewModel(from key: RegisteredKey) -> PIXReceiveKeyViewModel? {
        let value = configureValue(for: key.keyType, value: key.keyValue)
        let viewModel = PIXReceiveKeyViewModel(id: key.id, type: key.keyType, value: value)
        return viewModel
    }
    
    func configureValue(for type: PixKeyType, value: String) -> String {
        let mask: CustomStringMask
        switch type {
        case .cnpj:
            mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cnpj)
        case .cpf:
            mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf)
        case .phone:
            mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cellPhoneWithDDD)
        default:
            return value
        }
        
        return mask.maskedText(from: value) ?? ""
    }
}
