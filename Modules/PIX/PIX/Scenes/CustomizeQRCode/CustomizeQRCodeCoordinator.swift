import UIKit

enum CustomizeQRCodeAction {
    case keyManager
    case customizedQRCode(qrCode: PixQRCode, keys: [RegisteredKey])
}

protocol CustomizeQRCodeCoordinating: AnyObject {
    func perform(action: CustomizeQRCodeAction)
}

final class CustomizeQRCodeCoordinator {
    var output: CustomQRCodeOutput
    
    init(output: @escaping CustomQRCodeOutput) {
        self.output = output
    }
}

// MARK: - CustomizeQRCodeCoordinating
extension CustomizeQRCodeCoordinator: CustomizeQRCodeCoordinating {
    func perform(action: CustomizeQRCodeAction) {
        output(action)
    }
}
