import Foundation

enum ReturnLoadingConsumerFactory {
    static func make(paymentOpening: PIXPaymentOpening, transactionId: String) -> ReturnLoadingConsumerViewController {
        let coordinator = ReturnLoadingConsumerCoordinator(paymentOpening: paymentOpening)
        let presenter = ReturnLoadingConsumerPresenter(coordinator: coordinator)
        let container = DependencyContainer()
        let service = ReturnLoadingConsumerService(dependencies: container)
        let interactor = ReturnLoadingConsumerInteractor(presenter: presenter,
                                                         service: service,
                                                         transactionId: transactionId)

        let viewController = ReturnLoadingConsumerViewController(interactor: interactor)
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
