import Foundation
import UI
import UIKit

protocol ReturnLoadingConsumerPresenting {
    var viewController: ReturnLoadingConsumerDisplay? { get set }
    func presentPaymentScreen(details: PIXPaymentDetails, transactionId: String)
    func presentLoading()
    func stopLoading()
    func presentError(with title: String, description: String, buttonTitle: String)
}

final class ReturnLoadingConsumerPresenter: ReturnLoadingConsumerPresenting {
    weak var viewController: ReturnLoadingConsumerDisplay?

    private let coordinator: ReturnLoadingConsumerCoordinating

    init(coordinator: ReturnLoadingConsumerCoordinating) {
        self.coordinator = coordinator
    }
    
    func presentPaymentScreen(details: PIXPaymentDetails, transactionId: String) {
        coordinator.goToPayment(details: details, transactionId: transactionId)
    }

    func presentLoading() {
        viewController?.displayLoading()
    }

    func stopLoading() {
        viewController?.stopLoading()
    }

    func presentError(with title: String, description: String, buttonTitle: String) {
        viewController?.displayError(with: title, description: description, buttonTitle: buttonTitle)
    }
}
