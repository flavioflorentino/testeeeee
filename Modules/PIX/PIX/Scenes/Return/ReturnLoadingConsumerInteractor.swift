import Foundation
import Core

protocol ReturnLoadingConsumerInteracting {
    func loadPixData()
}

final class ReturnLoadingConsumerInteractor: ReturnLoadingConsumerInteracting {
    private let presenter: ReturnLoadingConsumerPresenting
    private let service: ReturnLoadingConsumerServicing
    private let transactionId: String

    init(presenter: ReturnLoadingConsumerPresenting,
         service: ReturnLoadingConsumerServicing,
         transactionId: String) {
        self.presenter = presenter
        self.service = service
        self.transactionId = transactionId
    }

    func loadPixData() {
        presenter.presentLoading()
        service.getPixAccount(transactionId: transactionId) { [weak self] result in
            guard let self = self else { return }
            self.presenter.stopLoading()
            switch result {
            case .success(let response):
                let details = self.getPaymentDetails(from: response)
                self.presenter.presentPaymentScreen(details: details, transactionId: self.transactionId)
            case .failure(let error):
                self.handleError(error: error)
            }
        }
    }

    private func getPaymentDetails(from response: ReturnLoadingResponse) -> PIXPaymentDetails {
        let account = response.receiverData
        let payee = getPayee(from: account)
        
        let details = PIXPaymentDetails(
            title: Strings.Return.Payment.title,
            screenType: .default(commentPlaceholder: Strings.Return.Payment.placeholder),
            payee: payee,
            pixParams: getPixParams(account: account),
            value: response.valueAvailable
        )

        return details
    }

    private func getPayee(from account: PixAccount) -> PIXPayee {
        .init(name: account.name,
              description: "\(account.cpfCnpj) | \(account.bank)",
              imageURLString: account.imageUrl,
              id: account.id)
    }

    private func getPixParams(account: PixAccount) -> PIXParams {
        let pixReceiverData = PIXReceiverData(
            name: "",
            bankId: "",
            bankName: "",
            accountType: "",
            agency: "",
            accountNumber: "",
            cpfCnpj: nil,
            ownership: ""
        )
        return PIXParams(originType: .key, keyType: "", key: "", receiverData: pixReceiverData)
    }

    private func handleError(error: ApiError) {
        guard let title = error.requestError?.alert?.title?.string,
              let description = error.requestError?.alert?.text?.string else {
            return presentGeneralError()
        }

        let buttonTitle = error.requestError?.alert?.buttonsJson.first?["title"] as? String
        presenter.presentError(with: title, description: description, buttonTitle: buttonTitle ?? "")
    }

    private func presentGeneralError() {
        presenter.presentError(with: Strings.General.somethingWentWrong,
                               description: Strings.General.failedComms,
                               buttonTitle: Strings.General.gotIt)
    }
}
