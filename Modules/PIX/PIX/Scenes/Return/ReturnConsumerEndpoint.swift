import Core

enum ReturnConsumerEndpoint: ApiEndpointExposable {
     case getPixAccount(transactionId: String)

     var path: String {
         switch self {
         case let .getPixAccount(transactionId):
             return "pix-receivement/transaction/refund/\(transactionId)"
         }
     }

     var method: HTTPMethod {
         .get
     }
 }
