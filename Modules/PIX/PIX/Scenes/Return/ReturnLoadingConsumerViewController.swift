import Foundation
import UI
import UIKit

protocol ReturnLoadingConsumerDisplay: AnyObject {
    func displayLoading()
    func stopLoading()
    func displayError(with title: String, description: String, buttonTitle: String)
}

final class ReturnLoadingConsumerViewController: ViewController<ReturnLoadingConsumerInteracting, UIView> {
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadPixData()
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationItem.backBarButtonItem = UIBarButtonItem()
    }
}

extension ReturnLoadingConsumerViewController: ReturnLoadingConsumerDisplay {
    func displayLoading() {
        beginState()
    }

    func stopLoading() {
        endState()
    }
    
    func displayError(with title: String, description: String, buttonTitle: String) {
        let controller = PopupViewController(title: title,
                                             description: description,
                                             preferredType: .text)
        controller.addAction(confirmAction(buttonTitle: buttonTitle))
        showPopup(controller)
    }

    private func confirmAction(buttonTitle: String) -> PopupAction {
        PopupAction(title: buttonTitle, style: .fill) {
            self.navigationController?.dismiss(animated: true)
        }
    }
}

extension ReturnLoadingConsumerViewController: StatefulTransitionViewing { }
