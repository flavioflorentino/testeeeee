import Foundation

protocol ReturnLoadingConsumerCoordinating {
    var viewController: UIViewController? { get set }
    func goToPayment(details: PIXPaymentDetails, transactionId: String)
}

class ReturnLoadingConsumerCoordinator: ReturnLoadingConsumerCoordinating {
    weak var viewController: UIViewController?
    private let paymentOpening: PIXPaymentOpening

    init(paymentOpening: PIXPaymentOpening) {
        self.paymentOpening = paymentOpening
    }

    func goToPayment(details: PIXPaymentDetails, transactionId: String) {
        guard let viewController = self.viewController else { return }
        paymentOpening.openPaymentWithDetails(details, onViewController: viewController, origin: .return, flowType: .pixReturn(transactionId: transactionId))
    }
}
