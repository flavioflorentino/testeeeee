import Core
 import Foundation

 protocol ReturnLoadingConsumerServicing {
     func getPixAccount(transactionId: String, completion: @escaping(Result<ReturnLoadingResponse, ApiError>) -> Void)
 }

final class ReturnLoadingConsumerService: ReturnLoadingConsumerServicing {
     typealias Dependencies = HasMainQueue
     private let dependencies: Dependencies

     init(dependencies: Dependencies) {
         self.dependencies = dependencies
     }

    func getPixAccount(transactionId: String, completion: @escaping (Result<ReturnLoadingResponse, ApiError>) -> Void) {
        let endpoint = ReturnConsumerEndpoint.getPixAccount(transactionId: transactionId)
        let decoder = JSONDecoder(.convertFromSnakeCase)
        Api<ReturnLoadingResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
 }

struct ReturnLoadingResponse: Decodable {
     var receiverData: PixAccount
     var originalTransactionId: String
     var originalValue: Double
     var valueAvailable: Double
 }
