import Core
import Foundation

protocol ConfirmBizPaymentPresenting: AnyObject {
    var viewController: ConfirmBizPaymentDisplaying? { get set }
    func setupTitle()
    func setupTableView(with value: Double, dynamicCells: [[ConfirmCellModel]])
    func displayHeaderAndFooter(with model: AvatarTitleDetailsHeaderModel)
    func showError(error: ApiError)
    func startLoading()
    func stopLoading()
    func didNextStep(action: ConfirmBizPaymentAction)
    func close()
}

private extension PaymentAmountFlowType {
    var description: String {
        switch self {
        case .send:
            return Strings.ConfirmPaymentBiz.Send.description
        case .refund:
            return Strings.ConfirmPaymentBiz.Refund.description
        }
    }
    
    var title: String {
        switch self {
        case .send:
            return Strings.ConfirmPaymentBiz.Send.title
        case .refund:
            return Strings.ConfirmPaymentBiz.Refund.title
        }
    }
}

final class ConfirmBizPaymentPresenter {
    private let coordinator: ConfirmBizPaymentCoordinating
    private let flowType: PaymentAmountFlowType
    weak var viewController: ConfirmBizPaymentDisplaying?

    init(coordinator: ConfirmBizPaymentCoordinating, flowType: PaymentAmountFlowType) {
        self.coordinator = coordinator
        self.flowType = flowType
    }
}

// MARK: - ConfirmBizPaymentPresenting
extension ConfirmBizPaymentPresenter: ConfirmBizPaymentPresenting {
    func setupTitle() {
        viewController?.displayTitle(flowType.title)
    }
    
    func showError(error: ApiError) {
        guard let title = error.requestError?.alert?.title?.string,
              let description = error.requestError?.alert?.text?.string
        else {
            showError(with: Strings.General.somethingWentWrong,
                      description: Strings.General.failedComms,
                      buttonTitle: Strings.General.gotIt)
            return
        }
        let buttonTitle: String? = error.requestError?.alert?.buttonsJson.first?["title"] as? String
        
        showError(with: title, description: description, buttonTitle: buttonTitle ?? "")
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func close() {
        coordinator.close()
    }
    
    func didNextStep(action: ConfirmBizPaymentAction) {
        coordinator.perform(action: action)
    }
    
    func setupTableView(with value: Double, dynamicCells: [[ConfirmCellModel]]) {
        guard let stringValue = NumberFormatter.toCurrencyString(with: value) else { return }
        let valueModel = ConfirmCellValueModel(with: Strings.ConfirmPaymentBiz.Cell.valueTitle, and: stringValue)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let formattedDate = dateFormatter.string(from: Date())
        let dateModel = ConfirmCellDetailModel(with: Strings.ConfirmPaymentBiz.Cell.dateTitle, and: formattedDate)
        
        let paymentModel = ConfirmCellDetailModel(with: Strings.ConfirmPaymentBiz.Cell.paymentTitle,
                                                  and: Strings.ConfirmPaymentBiz.Cell.paymentValue)
        let empyCell = ConfirmCellEmptyViewModel()
        let dynamicCells = getDynamicCellsModels(with: dynamicCells)
        let confirmCellModels = [empyCell] + [valueModel, dateModel, paymentModel] + dynamicCells
        viewController?.displayCells(with: confirmCellModels)
    }
    
    func displayHeaderAndFooter(with model: AvatarTitleDetailsHeaderModel) {
        let titleFooter = flowType.description
        let placeholderFooter = Strings.ConfirmPaymentBiz.Description.placeholder
        viewController?.displayTableViewHeaderFooter(with: model, title: titleFooter, placeholder: placeholderFooter)
    }
    
    private func getDynamicCellsModels(with inputModels: [[ConfirmCellModel]]) -> [ConfirmCellModel] {
        var outputModels: [ConfirmCellModel] = []
        guard !inputModels.flatMap({ $0 }) .isEmpty else {
            return []
        }
        outputModels.append(ConfirmCellSeparatortModel())
        outputModels.append(contentsOf: inputModels.flatMap { $0 })
        return outputModels
    }
    
    private func showError(with title: String, description: String, buttonTitle: String) {
        viewController?.showError(title: title, message: description, buttonTitle: buttonTitle)
    }
}
