import Core

enum ConfirmBizPaymentEndpoint {
    case confirmRefundPayment(model: ConfirmRefundPaymentModel, pin: String)
    case confirmPayment(model: ConfirmPaymentModel, pin: String)
}

extension ConfirmBizPaymentEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .confirmRefundPayment:
            return "/pix-transactions/devolution"
        case .confirmPayment:
            return "/pix-transactions/confirm"
        }
    }
    
    var shouldAppendBody: Bool {
        true
    }
    
    var body: Data? {
        switch self {
        case let .confirmRefundPayment(model, _):
            return prepareBody(with: model, strategy: .convertToSnakeCase)
        case let .confirmPayment(model, _):
            return prepareBody(with: model, strategy: .convertToSnakeCase)
        }
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var customHeaders: [String: String] {
        switch self {
        case let .confirmRefundPayment(_, pin):
            return headerWith(pin: pin)
        case let .confirmPayment(_, pin):
            return headerWith(pin: pin)
        }
    }
}
