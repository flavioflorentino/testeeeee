struct ConfirmPaymentModel: Encodable {
    let params: BizPaymentParams
    let pixParams: PIXParams
    
    private enum CodingKeys: String, CodingKey {
        case pixParams = "pix_params"
        case params
    }
}

struct BizPaymentParams: Encodable {
    let value: String?
}
