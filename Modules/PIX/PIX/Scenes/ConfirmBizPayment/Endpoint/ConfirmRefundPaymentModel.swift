struct ConfirmRefundPaymentModel: Encodable {
    let amount: Double
    let description: String?
    let originalTransactionId: String
}
