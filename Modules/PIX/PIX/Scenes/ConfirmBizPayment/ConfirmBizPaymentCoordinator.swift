import FactorAuthentication
import UIKit

protocol ConfirmBizPaymentCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ConfirmBizPaymentAction)
    func close()
}

final class ConfirmBizPaymentCoordinator {
    weak var viewController: UIViewController?
    private let confirmBizOutput: ConfirmPaymentOutput
    private var coordinatorTFA: FactorAuthenticationCoordinator?
    private var tfaDelegate: FactorAuthenticationDelegate?
    
    init(confirmBizOutput: @escaping ConfirmPaymentOutput) {
        self.confirmBizOutput = confirmBizOutput
    }
}

// MARK: - ConfirmBizPaymentCoordinating
extension ConfirmBizPaymentCoordinator: ConfirmBizPaymentCoordinating {
    func perform(action: ConfirmBizPaymentAction) {
        confirmBizOutput(action)
    }
    
    func close() {
        viewController?.navigationController?.popViewController(animated: true)
    }
}
