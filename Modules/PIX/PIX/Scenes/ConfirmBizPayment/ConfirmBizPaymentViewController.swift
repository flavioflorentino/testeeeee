import UI
import UIKit

public enum ConfirmBizPaymentInputAction: Equatable {
    case confirmPayment(hash: String)
    case none
}

protocol ConfirmBizPaymentDisplaying: AnyObject {
    func displayTitle(_ title: String)
    func displayCells(with models: [ConfirmCellModel])
    func displayTableViewHeaderFooter(with model: AvatarTitleDetailsHeaderModel, title: String, placeholder: String)
    func showError(title: String, message: String, buttonTitle: String)
    func input(action: ConfirmBizPaymentInputAction)
    func startLoading()
    func stopLoading()
}

private extension ConfirmBizPaymentViewController.Layout {
    enum Size {
        static let tableViewHeight: CGFloat = 132
    }
    enum Margin {
        static let footerMargins = UIEdgeInsets(top: 0, left: Spacing.base03, bottom: 0, right: Spacing.base03)
    }
}

final class ConfirmBizPaymentViewController: ViewController<ConfirmBizPaymentInteracting, UIView>, StatefulTransitionViewing {
    fileprivate enum Layout { }
    
    private lazy var dataSource = TableViewDataSource<Int, ConfirmCellModel>(view: tableView)
    
    private lazy var scrollView = UIScrollView()
    
    private var tableHeaderView: UIView? {
        didSet {
            tableView.tableHeaderView = nil
            tableView.layoutIfNeeded()
        }
    }
    
    private var tableFooterView: UIView? {
        didSet {
            tableView.tableFooterView = nil
            tableView.layoutIfNeeded()
        }
    }
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = .zero
        tableView.backgroundColor = Colors.backgroundPrimary.color
        return tableView
    }()
    
    private lazy var continueButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.General.continue, for: .normal)
        button.addTarget(self, action: #selector(didTapContinue), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        interactor.viewDidLoad()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(tableView)
        view.addSubview(continueButton)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
        }
        
        continueButton.snp.makeConstraints {
            $0.top.equalTo(tableView.snp.bottom)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Sizing.base06)
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        configureKeyboard()
    }
}

// MARK: - ConfirmBizPaymentDisplaying
extension ConfirmBizPaymentViewController: ConfirmBizPaymentDisplaying {
    func displayTitle(_ title: String) {
        self.title = title
    }
    
    func startLoading() {
        beginState(model: StateLoadingViewModel(message: Strings.KeyManager.loading))
    }
    
    func stopLoading() {
        endState()
    }
    
    func showError(title: String, message: String, buttonTitle: String) {
        let popupViewController = PopupViewController(title: title,
                                                      description: message,
                                                      preferredType: .business)
        let okAction = PopupAction(title: buttonTitle, style: .link)
        popupViewController.addAction(okAction)
        
        showPopup(popupViewController)
    }
    
    func input(action: ConfirmBizPaymentInputAction) {
        switch action {
        case let .confirmPayment(hash):
            guard let footer = tableView.tableFooterView as? DescriptionFooterView else { return }
            interactor.confirmPayment(description: footer.text, hash: hash)
        default:
            break
        }
    }
    
    func displayTableViewHeaderFooter(with model: AvatarTitleDetailsHeaderModel, title: String, placeholder: String) {
        let avatarHeaderView = AvatarTitleDetailsHeaderView()
        avatarHeaderView.setupView(model: model)
        let descriptionFooterView = DescriptionFooterView(with: title, and: placeholder)
        updateHeaderView(for: avatarHeaderView)
        updateHeaderView(for: descriptionFooterView)
        tableView.tableHeaderView = avatarHeaderView
        tableView.tableFooterView = descriptionFooterView
        tableView.layoutIfNeeded()
    }
    
    func displayCells(with models: [ConfirmCellModel]) {
        models.forEach { item in
            self.dataSource.add(items: [item], to: 0)
        }
        
        dataSource.itemProvider = getCell(_:_:_:)
        tableView.dataSource = dataSource
        tableView.reloadData()
    }
    
    private func updateHeaderView(for view: UIView) {
        let expectedSize = view.systemLayoutSizeFitting(CGSize(width: tableView.frame.width, height: .leastNonzeroMagnitude), withHorizontalFittingPriority: .required, verticalFittingPriority: .defaultLow)
        if view.frame.size.height != expectedSize.height {
            view.frame.size.height = expectedSize.height
        }
    }
}

extension ConfirmBizPaymentViewController {
    func registerCells() {
        tableView.register(ConfirmDetailCell.self, forCellReuseIdentifier: ConfirmDetailCell.identifier)
        tableView.register(ConfirmDetailHighlightCell.self, forCellReuseIdentifier: ConfirmDetailHighlightCell.identifier)
        tableView.register(ConfirmValueCell.self, forCellReuseIdentifier: ConfirmValueCell.identifier)
        tableView.register(ConfirmSeparatorCell.self, forCellReuseIdentifier: ConfirmSeparatorCell.identifier)
        tableView.register(ConfirmEmptyCell.self, forCellReuseIdentifier: ConfirmEmptyCell.identifier)
    }
    
    func getCell(_ view: UITableView, _ indexPath: IndexPath, _ item: ConfirmCellModel) -> UITableViewCell? {
        switch item.type {
        case .detail:
            guard let model = item as? ConfirmCellDetailModel else {
                return nil
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: item.identifier, for: indexPath) as? ConfirmDetailCell
            cell?.configCell(with: model)
            return cell
        case .hightlight:
            guard let model = item as? ConfirmCellHighlightModel else {
                return nil
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: item.identifier, for: indexPath) as? ConfirmDetailHighlightCell
            cell?.configCell(with: model)
            return cell
        case .value:
            guard let model = item as? ConfirmCellValueModel else {
                return nil
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: item.identifier, for: indexPath) as? ConfirmValueCell
            cell?.configCell(with: model)
            return cell
        case .separator:
            return tableView.dequeueReusableCell(withIdentifier: item.identifier, for: indexPath) as? ConfirmSeparatorCell
        case .empty:
            return tableView.dequeueReusableCell(withIdentifier: item.identifier, for: indexPath) as? ConfirmEmptyCell
        }
    }
}

@objc
private extension ConfirmBizPaymentViewController {
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        guard
            let userInfo = notification.userInfo,
            let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        else {
            return
        }
        
        updateKeyboardConstraint(userInfo: userInfo, inset: keyboardSize.cgRectValue.height)
    }
    
    func keyboardWillHide(notification: NSNotification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        updateKeyboardConstraint(userInfo: userInfo, inset: 0)
    }
    
    func didTapContinue() {
        interactor.tfaFlow()
    }
}

private extension ConfirmBizPaymentViewController {
    func configureKeyboard() {
        tapGesture()
        keyboardObservers()
    }
    
    func tapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func keyboardObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    func updateKeyboardConstraint(userInfo: [AnyHashable: Any], inset: CGFloat) {
        guard let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
              let animationCurve = (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue
        else {
            return
        }
        
        continueButton.snp.updateConstraints {
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).inset(inset)
        }
        
        UIView.animate(
            withDuration: animationDuration,
            delay: 0,
            options: UIView.AnimationOptions(rawValue: animationCurve),
            animations: {
                self.view.layoutIfNeeded()
            },
            completion: nil
        )
    }
}

extension ConfirmBizPaymentViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.cellForRow(at: indexPath) is ConfirmValueCell {
            interactor.pop()
        }
    }
}
