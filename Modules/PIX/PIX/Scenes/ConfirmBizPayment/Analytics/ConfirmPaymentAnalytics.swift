import AnalyticsModule

enum ConfirmPaymentAnalytics: AnalyticsKeyProtocol {
    case confirmationReceipt(sellerId: String, userName: String, date: String)
    
    var name: String {
        "Pix - Confirmation Receipt"
    }
    
    private var properties: [String: Any] {
        if case let .confirmationReceipt(sellerId, userName, date) = self {
            return [
                "data": date,
                "seller_id": sellerId,
                "user_name": userName,
                "method": "manual",
                "flow": "return"
            ]
        }
        return [:]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.mixPanel])
    }
}
