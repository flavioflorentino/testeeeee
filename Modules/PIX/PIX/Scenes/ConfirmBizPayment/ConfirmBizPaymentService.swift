import Foundation
import Core

protocol ConfirmBizPaymentServicing {
    func createRefundPayment(with model: ConfirmRefundPaymentModel,
                             pin: String,
                             completion: @escaping (Result<ReceiptResponse, ApiError>) -> Void)
    func createPayment(with model: ConfirmPaymentModel,
                       pin: String,
                       completion: @escaping (Result<ReceiptResponse, ApiError>) -> Void)
}

final class ConfirmBizPaymentService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension ConfirmBizPaymentService: ConfirmBizPaymentServicing {
    func createRefundPayment(with model: ConfirmRefundPaymentModel,
                             pin: String,
                             completion: @escaping (Result<ReceiptResponse, ApiError>) -> Void) {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        let endpoint = ConfirmBizPaymentEndpoint.confirmRefundPayment(model: model, pin: pin)
        Core.Api<ReceiptResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func createPayment(with model: ConfirmPaymentModel,
                       pin: String,
                       completion: @escaping (Result<ReceiptResponse, ApiError>) -> Void) {
        let decoder = JSONDecoder()
        let endpoint = ConfirmBizPaymentEndpoint.confirmPayment(model: model, pin: pin)
        Core.Api<ReceiptResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
