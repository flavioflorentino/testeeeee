import UIKit

typealias ConfirmPaymentOutput = (ConfirmBizPaymentAction) -> Void

enum ConfirmBizPaymentAction {
    case tfa
    case receipt(transactionId: String? = nil, data: ReceiptResponse? = nil)
}

enum ConfirmBizPaymentFactory {
    static func makeSend(
        sendInfo: ConfirmBizPaymentSendInfo,
        confirmOutput: @escaping ConfirmPaymentOutput
    ) -> ConfirmBizPaymentViewController {
        let container = DependencyContainer()
        let service = ConfirmBizPaymentService(dependencies: container)
        let coordinator: ConfirmBizPaymentCoordinating = ConfirmBizPaymentCoordinator(confirmBizOutput: confirmOutput)
        let presenter: ConfirmBizPaymentPresenting = ConfirmBizPaymentPresenter(coordinator: coordinator, flowType: .send(pixParams: sendInfo.pixParams))
        let interactor: ConfirmBizPaymentInteracting = ConfirmBizPaymentInteractor(
            sendInfo,
            dependencies: container,
            service: service,
            presenter: presenter
        )
        
        let viewController = ConfirmBizPaymentViewController(interactor: interactor)
        
        coordinator.viewController = viewController
        presenter.viewController = viewController
        
        return viewController
    }
    
    static func makeRefund(
        refundInfo: ConfirmBizPaymentRefundInfo,
        confirmOutput: @escaping ConfirmPaymentOutput
    ) -> ConfirmBizPaymentViewController {
        let container = DependencyContainer()
        let service = ConfirmBizPaymentService(dependencies: container)
        let coordinator: ConfirmBizPaymentCoordinating = ConfirmBizPaymentCoordinator(confirmBizOutput: confirmOutput)
        let presenter: ConfirmBizPaymentPresenting = ConfirmBizPaymentPresenter(coordinator: coordinator, flowType: .refund(transactionId: refundInfo.transactionId, maxRefundAmount: refundInfo.maxRefundAmount, sendAmount: refundInfo.sendAmount))
        let interactor: ConfirmBizPaymentInteracting = ConfirmBizRefundPaymentInteractor(
            refundInfo,
            dependencies: container,
            service: service,
            presenter: presenter
        )
        
        let viewController = ConfirmBizPaymentViewController(interactor: interactor)
        
        coordinator.viewController = viewController
        presenter.viewController = viewController
        
        return viewController
    }
}
