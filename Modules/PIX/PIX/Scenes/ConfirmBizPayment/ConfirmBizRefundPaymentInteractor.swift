import AnalyticsModule
import Core
import Foundation

final class ConfirmBizRefundPaymentInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let service: ConfirmBizPaymentServicing
    private let presenter: ConfirmBizPaymentPresenting
    private let userInfo: KeyManagerBizUserInfo
    
    private let value: Double
    private let pixAccount: PixAccount
    private let transactionId: String
    
    init(_ refundInfo: ConfirmBizPaymentRefundInfo,
         dependencies: Dependencies,
         service: ConfirmBizPaymentServicing,
         presenter: ConfirmBizPaymentPresenting) {
        self.service = service
        self.presenter = presenter
        self.value = refundInfo.value
        self.pixAccount = refundInfo.pixAccount
        self.dependencies = dependencies
        self.userInfo = refundInfo.userInfo
        self.transactionId = refundInfo.transactionId
    }
}

// MARK: - ConfirmBizPaymentInteracting
extension ConfirmBizRefundPaymentInteractor: ConfirmBizPaymentInteracting {
    func confirmPayment(description: String?, hash: String) {
        let model = ConfirmRefundPaymentModel(amount: value,
                                              description: description,
                                              originalTransactionId: transactionId)
        presenter.startLoading()
        service.createRefundPayment(with: model, pin: hash) { [weak self] result in
            guard let self = self else { return }
            
            self.presenter.stopLoading()
            switch result {
            case .success:
                self.presenter.didNextStep(action: .receipt(transactionId: self.transactionId))
            case let .failure(error):
                self.presenter.showError(error: error)
            }
        }
    }
    
    func viewDidLoad() {
        presenter.setupTitle()
        configureAvatarTitleHeader()
        presenter.setupTableView(with: value, dynamicCells: [[]])
        sendAnalytics()
    }
    
    func pop() {
        presenter.close()
    }
    
    func tfaFlow() {
        presenter.didNextStep(action: .tfa)
    }
}

private extension ConfirmBizRefundPaymentInteractor {
    func configureAvatarTitleHeader() {
        let headerModel = AvatarTitleDetailsHeaderModelFactory.make(from: pixAccount)
        presenter.displayHeaderAndFooter(with: headerModel)
    }
    
    func sendAnalytics() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let dateString = dateFormatter.string(from: Date())
        let event = ConfirmPaymentAnalytics.confirmationReceipt(sellerId: userInfo.id,
                                                                userName: userInfo.name,
                                                                date: dateString)
        dependencies.analytics.log(event)
    }
}
