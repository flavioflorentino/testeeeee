import AnalyticsModule
import Core
import Foundation

protocol ConfirmBizPaymentInteracting: AnyObject {
    func viewDidLoad()
    func pop()
    func confirmPayment(description: String?, hash: String)
    func tfaFlow()
}

final class ConfirmBizPaymentInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let service: ConfirmBizPaymentServicing
    private let presenter: ConfirmBizPaymentPresenting
    private let userInfo: KeyManagerBizUserInfo
    
    private let value: Double
    private let pixAccount: PixAccount
    private let qrCodeAccount: PixQrCodeAccount?
    private let pixParams: PIXParams
    
    init(_ sendInfo: ConfirmBizPaymentSendInfo,
         dependencies: Dependencies,
         service: ConfirmBizPaymentServicing,
         presenter: ConfirmBizPaymentPresenting) {
        self.service = service
        self.presenter = presenter
        self.value = sendInfo.value
        self.pixAccount = sendInfo.pixAccount
        self.dependencies = dependencies
        self.userInfo = sendInfo.userInfo
        self.pixParams = sendInfo.pixParams
        self.qrCodeAccount = sendInfo.qrCodeAccount
    }
}

// MARK: - ConfirmBizPaymentInteracting
extension ConfirmBizPaymentInteractor: ConfirmBizPaymentInteracting {
    func confirmPayment(description: String?, hash: String) {
        let params = BizPaymentParams(value: "\(value)")
        let confirmModel = ConfirmPaymentModel(params: params, pixParams: pixParams)
        presenter.startLoading()
        service.createPayment(with: confirmModel, pin: hash) { [weak self] result in
            guard let self = self else { return }
        
            self.presenter.stopLoading()
            switch result {
            case let .success(receipt):
                self.presenter.didNextStep(action: .receipt(transactionId: nil, data: receipt))
            case let .failure(error):
                self.presenter.showError(error: error)
            }
        }
    }
    
    func viewDidLoad() {
        presenter.setupTitle()
        configureAvatarTitleHeader()
        presenter.setupTableView(with: value, dynamicCells: configureQRCodeDynamicCells())
        sendAnalytics()
    }
    
    func pop() {
        presenter.close()
    }
    
    func tfaFlow() {
        presenter.didNextStep(action: .tfa)
    }
}

private extension ConfirmBizPaymentInteractor {
    func configureQRCodeDynamicCells() -> [[ConfirmCellModel]] {
        let payerData = qrCodeAccount?.qrcodeInfos.payerData ?? []
        let payerCells = payerData.map { payerInfo in
            ConfirmCellDetailModel(with: payerInfo.title, and: payerInfo.value)
        }
        let receiverData = qrCodeAccount?.qrcodeInfos.receiverData ?? []
        let receiverCells = receiverData.map { receiverInfo in
            ConfirmCellDetailModel(with: receiverInfo.title, and: receiverInfo.value)
        }
        return [payerCells, receiverCells]
    }
    
    func configureAvatarTitleHeader() {
        let headerModel = AvatarTitleDetailsHeaderModelFactory.make(from: pixAccount)
        presenter.displayHeaderAndFooter(with: headerModel)
    }
    
    func sendAnalytics() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let dateString = dateFormatter.string(from: Date())
        let event = ConfirmPaymentAnalytics.confirmationReceipt(sellerId: userInfo.id,
                                                                userName: userInfo.name,
                                                                date: dateString)
        dependencies.analytics.log(event)
    }
}
