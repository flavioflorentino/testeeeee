import Foundation

struct ConfirmBizPaymentSendInfo {
    let userInfo: KeyManagerBizUserInfo
    let pixParams: PIXParams
    let pixAccount: PixAccount
    let value: Double
    let qrCodeAccount: PixQrCodeAccount?
    
    init(
        userInfo: KeyManagerBizUserInfo,
        pixParams: PIXParams,
        pixAccount: PixAccount,
        value: Double,
        qrCodeAccount: PixQrCodeAccount? = nil
    ) {
        self.userInfo = userInfo
        self.pixParams = pixParams
        self.pixAccount = pixAccount
        self.value = value
        self.qrCodeAccount = qrCodeAccount
    }
}
