import Foundation

struct ConfirmBizPaymentRefundInfo {
    let userInfo: KeyManagerBizUserInfo
    let transactionId: String
    let maxRefundAmount: Double?
    let sendAmount: Double?
    let pixAccount: PixAccount
    let value: Double
    let qrCodeAccount: PixQrCodeAccount?
    
    init(
        userInfo: KeyManagerBizUserInfo,
        transactionId: String,
        maxRefundAmount: Double?,
        sendAmount: Double?,
        pixAccount: PixAccount,
        value: Double,
        qrCodeAccount: PixQrCodeAccount? = nil
    ) {
        self.userInfo = userInfo
        self.transactionId = transactionId
        self.maxRefundAmount = maxRefundAmount
        self.sendAmount = sendAmount
        self.pixAccount = pixAccount
        self.value = value
        self.qrCodeAccount = qrCodeAccount
    }
}
