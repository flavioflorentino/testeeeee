import UI
import UIKit

final class ConfirmValueCell: UITableViewCell {
    private lazy var keyLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(LinkPrimaryLabelStyle())
            .with(\.textColor, .branding600())
        label.textAlignment = .right
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [keyLabel, valueLabel])
        stackView.axis = .horizontal
        stackView.distribution = .equalCentering
        stackView.spacing = Spacing.base00
        return stackView
    }()
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildLayout()
    }
    
    func configCell(with model: ConfirmCellValueModel) {
        keyLabel.text = model.key
        valueLabel.text = model.value
    }
}

extension ConfirmValueCell: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(stackView)
    }
    
    func setupConstraints() {
        keyLabel.snp.makeConstraints {
            $0.top.leading.bottom.equalToSuperview()
        }
        
        valueLabel.snp.makeConstraints {
            $0.top.trailing.bottom.equalToSuperview()
            $0.leading.equalTo(keyLabel.snp.trailing).offset(Spacing.base00)
        }
        
        stackView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
    }
}
