import UI
import UIKit

final class ConfirmSeparatorCell: UITableViewCell {
    private lazy var separatorView = SeparatorView(style: BackgroundViewStyle(color: .grayscale300()))
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
}

extension ConfirmSeparatorCell: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(separatorView)
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
    
    func setupConstraints() {
        separatorView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
    }
}
