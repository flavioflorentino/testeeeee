import UI
import UIKit

final class ConfirmEmptyCell: UITableViewCell {
    private lazy var emptyView = UIView()
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
}

extension ConfirmEmptyCell: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(emptyView)
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
    
    func setupConstraints() {
        emptyView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.equalTo(Sizing.base02)
        }
    }
}
