import UI
import UIKit

final class DescriptionFooterView: UIView {
    fileprivate enum Layout {}
    
    var text: String? {
        descriptionTextField.text
    }
    
    private lazy var separatorView = SeparatorView(style: BackgroundViewStyle(color: .grayscale200()))
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var emptyView = UIView()
    
    private lazy var descriptionTextField = UIPPFloatingTextField() 
    
    private lazy var descriptionStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [separatorView, emptyView, descriptionLabel, descriptionTextField])
        stackView.axis = .vertical
        return stackView
    }()
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("not implemented yet!!!")
    }
    
    init(with title: String, and placeholder: String) {
        super.init(frame: .zero)
        self.descriptionLabel.text = title
        self.descriptionTextField.placeholder = placeholder
        buildLayout()
    }
}

extension DescriptionFooterView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(descriptionStackView)
    }
    
    func setupConstraints() {
        emptyView.snp.makeConstraints {
            $0.height.equalTo(Spacing.base03)
        }
        
        descriptionStackView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}
