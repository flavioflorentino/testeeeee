import Foundation
import UI
import UIKit

enum ConfirmCellType: Equatable {
    case detail, hightlight, value, separator, empty
}

protocol ConfirmCellIdentifier: Equatable {
    var type: ConfirmCellType { get }
    var identifier: String { get }
}

class ConfirmCellModel: ConfirmCellIdentifier, Equatable {
    let type: ConfirmCellType
    let identifier: String
    
    init(_ type: ConfirmCellType, _ identifier: String) {
        self.type = type
        self.identifier = identifier
    }
    
    static func == (lhs: ConfirmCellModel, rhs: ConfirmCellModel) -> Bool {
        lhs.identifier == rhs.identifier && lhs.type == rhs.type
    }
}

final class ConfirmCellDetailModel: ConfirmCellModel {
    let key: String
    let value: String
    
    init(with key: String, and value: String) {
        self.key = key
        self.value = value
        super.init(.detail, ConfirmDetailCell.identifier)
    }
}

final class ConfirmCellValueModel: ConfirmCellModel {
    let key: String
    let value: String
    
    init(with key: String, and value: String) {
        self.key = key
        self.value = value
        super.init(.value, ConfirmValueCell.identifier)
    }
}

final class ConfirmCellHighlightModel: ConfirmCellModel {
    let key: String
    let value: String
    
    init(with key: String, and value: String) {
        self.key = key
        self.value = value
        super.init(.hightlight, ConfirmDetailHighlightCell.identifier)
    }
}

final class ConfirmCellSeparatortModel: ConfirmCellModel {
    init() {
        super.init(.separator, ConfirmSeparatorCell.identifier)
    }
}

final class ConfirmCellEmptyViewModel: ConfirmCellModel {
    init() {
        super.init(.empty, ConfirmEmptyCell.identifier)
    }
}
