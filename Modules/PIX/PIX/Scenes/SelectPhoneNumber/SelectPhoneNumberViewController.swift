import AssetsKit
import PermissionsKit
import UI
import UIKit

protocol SelectPhoneNumberDisplaying: AnyObject {
    func displayContacts(_ contacts: [ContactModel])
    func displayRequestContactsPermission()
    func displayNoContacts()
    func displayNoFetchingContacts()
}

final class SelectPhoneNumberViewController: ViewController<SelectPhoneNumberInteracting, UIView> {
    enum Section {
        case main
    }
    
    private lazy var noContactsView: ApolloFeedbackView = {
        let content = ApolloFeedbackViewContent(
            image: Resources.Illustrations.iluNoContacts.image,
            title: Strings.Feedback.NoContacts.title,
            description: NSAttributedString(string: Strings.Feedback.NoContacts.description),
            primaryButtonTitle: String(),
            secondaryButtonTitle: String()
        )
        let feedbackView = ApolloFeedbackView(content: content)
        feedbackView.primaryButtonIsHidden(true)
        feedbackView.secondaryButtonIsHidden(true)
        return feedbackView
    }()
    
    private lazy var noFetchingContactsView: ApolloFeedbackView = {
        let content = ApolloFeedbackViewContent(
            image: Resources.Illustrations.iluEmptyCircledBackground.image,
            title: Strings.Feedback.NoFetchingContacts.title,
            description: NSAttributedString(string: Strings.Feedback.NoFetchingContacts.description),
            primaryButtonTitle: String(),
            secondaryButtonTitle: String()
        )
        let feedbackView = ApolloFeedbackView(content: content)
        feedbackView.primaryButtonIsHidden(true)
        feedbackView.secondaryButtonIsHidden(true)
        return feedbackView
    }()

    private lazy var contactsTableView: UITableView = {
        let tableView = UITableView()
        tableView.register(
            ContactTableViewCell.self,
            forCellReuseIdentifier: String(describing: ContactTableViewCell.self)
        )
        tableView.separatorStyle = .none
        tableView.backgroundColor = Colors.backgroundPrimary.color
        return tableView
    }()

    private lazy var tableViewDataSource: TableViewDataSource<Section, ContactModel>? = {
        let dataSource = TableViewDataSource<Section, ContactModel>(view: contactsTableView)
        dataSource.itemProvider = { tableView, indexPath, contact -> ContactTableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ContactTableViewCell.self), for: indexPath) as? ContactTableViewCell
            cell?.configureCell(withTitle: contact.name, subtitle: contact.phoneNumber)
            return cell
        }
        dataSource.add(section: .main)
        return dataSource
    }()

    private lazy var searchView: SearchComponentView = {
        let searchView = SearchComponentView(
            placeholder: Strings.SelectPhoneNumber.fetchContacts,
            color: Colors.grayscale700.color
        )
        searchView.textField.layer.borderWidth = Border.light
        searchView.textField.layer.borderColor = Colors.grayscale200.color.cgColor
        searchView.textField.delegate = self
        return searchView
    }()
    
    private lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(LinkButtonStyle())
        button.setTitle(Strings.General.cancel, for: .normal)
        button.setContentHuggingPriority(.defaultLow, for: .horizontal)
        button.addTarget(self, action: #selector(didTapCancel), for: .touchUpInside)
        return button
    }()

    private lazy var sectionTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.grayscale700.color
        label.font = Typography.title(.small).font()
        return label
    }()
    
    private lazy var headerView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.backgroundPrimary.color
    
        view.addSubview(sectionTitleLabel)
        sectionTitleLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.bottom.equalToSuperview()
            $0.top.equalToSuperview().offset(Spacing.base03)
        }
        return view
    }()
    
    private lazy var requestPermissionView = PermissionRequestView(
        setup: .full(requestType: .contactsPix, buttonStyle: .default, authorizeHandler: { [weak self] in
            self?.interactor.requestContactsPermission()
        }, notNowHandler: { [weak self] in
            self?.interactor.permissionDenied()
        })
    )
    
    override func buildViewHierarchy() {
        view.addSubview(contactsTableView)
        view.addSubview(searchView)
        view.addSubview(cancelButton)
    }
    
    override func setupConstraints() {
        searchView.snp.makeConstraints {
            $0.top.equalToSuperview().inset(Spacing.base03)
            $0.leading.equalToSuperview()
        }
        
        cancelButton.snp.makeConstraints {
            $0.leading.equalTo(searchView.snp.trailing)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.centerY.equalTo(searchView)
        }
        
        contactsTableView.snp.makeConstraints {
            $0.top.equalTo(searchView.snp.bottom)
            $0.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview()
        }
    }

    override func configureViews() {
        contactsTableView.dataSource = tableViewDataSource
        contactsTableView.delegate = self
        searchView.textField.addTarget(self, action: #selector(searchContact), for: .editingChanged)
        view.backgroundColor = Colors.backgroundPrimary.color
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.verifyContactsPermission()
    }
}

@objc
private extension SelectPhoneNumberViewController {
    func searchContact() {
        interactor.searchContact(by: searchView.textField.text ?? String())
    }
    
    func didTapCancel() {
        interactor.close()
    }
}

// MARK: - SelectPhoneNumberDisplaying
extension SelectPhoneNumberViewController: SelectPhoneNumberDisplaying {
    func displayContacts(_ contacts: [ContactModel]) {
        tableViewDataSource?.update(items: contacts, from: .main)
        sectionTitleLabel.text = Strings.SelectPhoneNumber.allContacts
        contactsTableView.backgroundView = nil
        searchView.isUserInteractionEnabled = true
    }
    
    func displayRequestContactsPermission() {
        sectionTitleLabel.text = String()
        contactsTableView.backgroundView = requestPermissionView
        searchView.isUserInteractionEnabled = false
    }
    
    func displayNoContacts() {
        contactsTableView.backgroundView = noContactsView
        sectionTitleLabel.text = String()
        searchView.isUserInteractionEnabled = false
    }
    
    func displayNoFetchingContacts() {
        tableViewDataSource?.update(items: [], from: .main)
        contactsTableView.backgroundView = noFetchingContactsView
        sectionTitleLabel.text = String()
    }
}

extension SelectPhoneNumberViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchView.textField.resignFirstResponder()
        return true
    }
}

extension SelectPhoneNumberViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor.didSelectContact(at: indexPath)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchView.textField.resignFirstResponder()
    }
}
