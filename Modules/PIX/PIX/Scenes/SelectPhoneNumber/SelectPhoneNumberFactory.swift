import UIKit

enum SelectPhoneNumberFactory {
    static func make(numberHandler: @escaping (String) -> Void) -> SelectPhoneNumberViewController {
        let container = DependencyContainer()
        let coordinator: SelectPhoneNumberCoordinating = SelectPhoneNumberCoordinator(numberHandler: numberHandler)
        let presenter: SelectPhoneNumberPresenting = SelectPhoneNumberPresenter(coordinator: coordinator)
        let interactor = SelectPhoneNumberInteractor(presenter: presenter, dependencies: container)
        let viewController = SelectPhoneNumberViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
