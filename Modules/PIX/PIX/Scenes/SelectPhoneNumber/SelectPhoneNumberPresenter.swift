import Foundation

protocol SelectPhoneNumberPresenting: AnyObject {
    var viewController: SelectPhoneNumberDisplaying? { get set }
    func didNextStep(action: SelectPhoneNumberAction)
    func presentContacts(_ contacts: [ContactModel])
    func presentRequestPermission()
    func presentNoContacts()
    func presentFetchingNoContacts()
}

final class SelectPhoneNumberPresenter {
    private let coordinator: SelectPhoneNumberCoordinating
    weak var viewController: SelectPhoneNumberDisplaying?

    init(coordinator: SelectPhoneNumberCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - SelectPhoneNumberPresenting
extension SelectPhoneNumberPresenter: SelectPhoneNumberPresenting {
    func didNextStep(action: SelectPhoneNumberAction) {
        coordinator.perform(action: action)
    }

    func presentContacts(_ contacts: [ContactModel]) {
        viewController?.displayContacts(contacts)
    }
    
    func presentRequestPermission() {
        viewController?.displayRequestContactsPermission()
    }
    
    func presentNoContacts() {
        viewController?.displayNoContacts()
    }
    
    func presentFetchingNoContacts() {
        viewController?.displayNoFetchingContacts()
    }
}
