import AnalyticsModule
import Foundation
import PermissionsKit

protocol SelectPhoneNumberInteracting: AnyObject {
    func close()
    func searchContact(by string: String)
    func requestContactsPermission()
    func verifyContactsPermission()
    func didSelectContact(at indexPath: IndexPath)
    func permissionDenied()
}

final class SelectPhoneNumberInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let service: SelectPhoneNumberServicing
    private let presenter: SelectPhoneNumberPresenting
    private var contacts: [ContactModel]
    private var filteredContacts: [ContactModel] = []

    init(
        presenter: SelectPhoneNumberPresenting,
        dependencies: Dependencies,
        service: SelectPhoneNumberServicing = SelectPhoneNumberService(),
        contacts: [ContactModel] = []
    ) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.service = service
        self.contacts = contacts
    }
}

// MARK: - SelectPhoneNumberInteracting
extension SelectPhoneNumberInteractor: SelectPhoneNumberInteracting {
    func searchContact(by searchText: String) {
        guard searchText.isNotEmpty else {
            self.filteredContacts = []
            handleContacts(contacts: contacts)
            return
        }
        let filteredContacts = contacts.filter {
            $0.name.lowercased().contains(searchText.lowercased()) || $0.phoneNumber.contains(searchText)
        }
        self.filteredContacts = filteredContacts
        guard filteredContacts.isNotEmpty else {
            dependencies.analytics.log(SelectPhoneNumberUserEvent.phoneBookError(type: .notFound))
            presenter.presentFetchingNoContacts()
            return
        }
        presenter.presentContacts(filteredContacts)
    }
    
    func requestContactsPermission() {
        service.requestContacts { [weak self] status in
            self?.handleContactsPermission(status)
        }
    }
    
    func verifyContactsPermission() {
        handleContactsPermission(service.status)
    }
    
    func permissionDenied() {
        dependencies.analytics.log(SelectPhoneNumberUserEvent.userNavigated(permission: .denied))
        close()
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
    
    func didSelectContact(at indexPath: IndexPath) {
        let contact: ContactModel
        if filteredContacts.isNotEmpty {
            contact = filteredContacts[indexPath.row]
        } else {
            contact = contacts[indexPath.row]
        }
        dependencies.analytics.log(SelectPhoneNumberUserEvent.userSelected)
        presenter.didNextStep(action: .phoneSelection(number: contact.phoneNumber))
    }
}

private extension SelectPhoneNumberInteractor {
    func handleContacts(contacts: [ContactModel]) {
        if contacts.isEmpty {
            dependencies.analytics.log(SelectPhoneNumberUserEvent.phoneBookError(type: .noContact))
            presenter.presentNoContacts()
        } else {
            presenter.presentContacts(contacts)
        }
    }
    
    func fetchContacts() {
        service.fetchContacts { [weak self] contacts in
            self?.contacts = contacts
            handleContacts(contacts: contacts)
        }
    }
    
    func handleContactsPermission(_ status: PermissionStatus) {
        guard status == .authorized else {
            presenter.presentRequestPermission()
            return
        }
        dependencies.analytics.log(SelectPhoneNumberUserEvent.userNavigated(permission: .allowed))
        fetchContacts()
    }
}
