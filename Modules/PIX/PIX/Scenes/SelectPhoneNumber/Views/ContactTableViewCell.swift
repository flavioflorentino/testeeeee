import AssetsKit
import UI

private extension ContactTableViewCell.Layout {
    static let imageSize = 40.0
}

final class ContactTableViewCell: UITableViewCell {
    fileprivate enum Layout { }

    private lazy var contactImageView = UIImageView(image: Resources.Placeholders.greenAvatarPlaceholder.image)

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.caption(.highlight).font()
        label.textColor = Colors.black.color
        return label
    }()

    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.caption().font()
        label.textColor = Colors.grayscale600.color
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configureCell(withTitle title: String, subtitle: String) {
        titleLabel.text = title
        subtitleLabel.text = subtitle
        buildLayout()
    }
}

extension ContactTableViewCell: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(contactImageView)
        addSubview(titleLabel)
        addSubview(subtitleLabel)
    }

    func setupConstraints() {
        contactImageView.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.height.width.equalTo(Layout.imageSize)
            $0.centerY.equalToSuperview()
        }

        titleLabel.snp.makeConstraints {
            $0.leading.equalTo(contactImageView.snp.trailing).offset(Spacing.base02)
            $0.top.equalToSuperview().offset(Spacing.base02)
        }

        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base00)
            $0.leading.equalTo(contactImageView.snp.trailing).offset(Spacing.base02)
            $0.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        contentView.backgroundColor = Colors.backgroundPrimary.color
    }
}
