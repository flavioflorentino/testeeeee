import Contacts
import Foundation
import PermissionsKit

protocol SelectPhoneNumberServicing {
    var status: PermissionStatus { get }
    func fetchContacts(result: ([ContactModel]) -> Void)
    func requestContacts(completion: @escaping Permission.Callback)
}

struct SelectPhoneNumberService: SelectPhoneNumberServicing {
    var status: PermissionStatus {
        Permission.contacts.status
    }
    
    func fetchContacts(result: ([ContactModel]) -> Void) {
        var contacts = [ContactModel]()
        let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
        let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
        do {
            try CNContactStore().enumerateContacts(with: request) { contact, _ in
                contact.phoneNumbers.forEach { phoneNumber in
                    let name = contact.givenName + " " + contact.familyName
                    let contactModel = ContactModel(name: name, phoneNumber: phoneNumber.value.stringValue)
                    contacts.append(contactModel)
                }
            }
        } catch {
            result([])
        }
        result(contacts.sorted { $0.name < $1.name })
    }

    func requestContacts(completion: @escaping Permission.Callback) {
        Permission.contacts.request(completion)
    }
}
