import Foundation

struct ContactModel: Equatable {
    let name: String
    let phoneNumber: String
}
