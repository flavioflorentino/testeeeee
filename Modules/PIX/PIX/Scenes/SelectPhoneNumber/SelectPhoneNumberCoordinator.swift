import UIKit

enum SelectPhoneNumberAction {
    case close
    case phoneSelection(number: String)
}

protocol SelectPhoneNumberCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: SelectPhoneNumberAction)
}

final class SelectPhoneNumberCoordinator {
    weak var viewController: UIViewController?
    let numberHandler: (String) -> Void
    
    init(numberHandler: @escaping (String) -> Void) {
        self.numberHandler = numberHandler
    }
}

// MARK: - SelectPhoneNumberCoordinating
extension SelectPhoneNumberCoordinator: SelectPhoneNumberCoordinating {
    func perform(action: SelectPhoneNumberAction) {
        switch action {
        case let .phoneSelection(number):
            numberHandler(number)
            viewController?.dismiss(animated: true)
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
