import AnalyticsModule
import Core
import Foundation
import UI
import Validations

protocol ManualInsertionInteracting: AnyObject {
    func loadInitialConfig()
    func selectPickerOption(_ option: PickerOption?)
    func verifyText(_ text: String?, type: ManualInsertionContent)
    func selectBank()
    func selectForward()
    func close()
}

private enum ContentType: Int, CaseIterable {
    case institution
    case accountType
    case agency
    case account
    case ownership
    case cpfCnpj
    case nickname
}

final class ManualInsertionInteractor {
    typealias Dependencies = HasAnalytics
    
    // MARK: - Variables
    private let dependencies: Dependencies
    private let service: ManualInsertionServicing
    private let presenter: ManualInsertionPresenting
    private let agencyValidator = RangeValidator(range: 1...5)
    private let accountValidator = RangeValidator(range: 3...15)
    private let cpfValidator = CPFValidator()
    private let cnpjValidator = CNPJValidator()
    private let nicknameValidator = RangeValidator(range: 1...100)
    private let keyType = "MANUAL"
    private var isFowardButtonEnabled = false
    private var isCurrentMaskCPF = true
    private var selectedAccountType: AccountType?
    private var selectedOwnershipType: Ownership?
    private var contentValidity: [Bool] = { ContentType.allCases.map { _ in false } }()
    private var bankId: String?
    private var bankName: String?
    private var agency: String?
    private var accountNumber: String?
    private var cpfCnpj: String?
    private var nickname: String?
    private var isAllFieldsValid: Bool {
        if let selectedOwnership = selectedOwnershipType, selectedOwnership == .personal {
            return contentValidity.enumerated().first(where: { value -> Bool in
                if value.offset == ContentType.cpfCnpj.rawValue || value.offset == ContentType.nickname.rawValue {
                    return false
                } else {
                    return value.element == false
                }
                })?.element ?? true
        } else {
            return contentValidity.first(where: { $0 == false }) ?? true
        }
    }

    init(service: ManualInsertionServicing, presenter: ManualInsertionPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - ManualInsertionInteracting
extension ManualInsertionInteractor: ManualInsertionInteracting {
    func loadInitialConfig() {
        dependencies.analytics.log(ManualInsertionUserEvent.userNavigated)
    }
    
    func selectPickerOption(_ option: PickerOption?) {
        if let type = option as? AccountType {
            selectedAccountType = type
            contentValidity[ContentType.accountType.rawValue] = true
            presenter.presentAccountTypeSelection()
        } else if let type = option as? Ownership {
            handleOwnershipSelection(type: type)
        }
        handleFowardButtonState()
    }
    
    private func handleOwnershipSelection(type: Ownership) {
        selectedOwnershipType = type
        contentValidity[ContentType.ownership.rawValue] = true
        presenter.presentOwnershipSelection()
        if type == .personal {
            presenter.hideCpfCnpjAndNicknameFields()
        } else {
            presenter.presentCpfCnpjAndNicknameFields()
        }
    }
    
    func verifyText(_ text: String?, type: ManualInsertionContent) {
        guard let text = text else {
            return
        }
        do {
            try handleTextValidation(text: text, type: type)
        } catch GenericValidationError.incompleteData {
            handleTextIncomplete(type: type)
        } catch {
            handleTextInvalid(text: text, type: type)
        }
        handleFowardButtonState()
    }
    
    private func handleTextValidation(text: String, type: ManualInsertionContent) throws {
        switch type {
        case .agency:
            agency = text
            try agencyValidator.validate(text)
        case .account:
            accountNumber = text
            try accountValidator.validate(text)
        case .cpfCnpj:
            try handleCpfAndCnpjValidation(text: text)
            presenter.removeCpfCnpjErrorMessage()
        case .nickname:
            nickname = text
            try nicknameValidator.validate(text)
        }
        validateType(isValid: true, type: type)
        presenter.presentTextFieldValidState(type: type)
    }
    
    private func handleCpfAndCnpjValidation(text: String) throws {
        cpfCnpj = text
        if text.count <= PersonalDocumentMaskDescriptor.cpf.format.count {
            if isCurrentMaskCPF == false {
                isCurrentMaskCPF.toggle()
                presenter.presentCpfMasker()
            }
            try cpfValidator.validate(text)
        } else {
            if isCurrentMaskCPF {
                isCurrentMaskCPF.toggle()
                presenter.presentCNPJMasker()
            }
            try cnpjValidator.validate(text)
        }
    }
    
    private func handleTextIncomplete(type: ManualInsertionContent) {
        validateType(isValid: false, type: type)
        presenter.presentTextFieldNormalState(type: type)
        if type == .cpfCnpj {
            presenter.removeCpfCnpjErrorMessage()
        }
    }
    
    private func handleTextInvalid(text: String, type: ManualInsertionContent) {
        validateType(isValid: false, type: type)
        presenter.presentTextFieldNormalState(type: type)
        guard type == .cpfCnpj else {
            return
        }
        dependencies.analytics.log(ManualInsertionUserEvent.sendError)
        if text.count <= PersonalDocumentMaskDescriptor.cpf.format.count {
            presenter.presentCpfErrorMessage()
        } else {
            presenter.presentCnpjErrorMessage()
        }
    }
    
    private func validateType(isValid: Bool, type: ManualInsertionContent) {
        switch type {
        case .agency:
            contentValidity[ContentType.agency.rawValue] = isValid
        case .account:
            contentValidity[ContentType.account.rawValue] = isValid
        case .cpfCnpj:
            contentValidity[ContentType.cpfCnpj.rawValue] = isValid
        case .nickname:
            contentValidity[ContentType.nickname.rawValue] = isValid
        }
    }
    
    private func handleFowardButtonState() {
        guard isAllFieldsValid != isFowardButtonEnabled else {
            return
        }
        isFowardButtonEnabled.toggle()
        presenter.toggleForwardButtonState()
    }
    
    func selectBank() {
        let completion: (String, String) -> Void = { [weak self] wsId, name in
            self?.bankId = wsId
            self?.bankName = name
            self?.presenter.presentInstitutionSelection(id: wsId, name: name)
            self?.contentValidity[ContentType.institution.rawValue] = true
        }
        presenter.didNextStep(action: .bankSelection(completion: completion))
    }
    
    func selectForward() {
        guard
            let params = createRequestParams(),
            isAllFieldsValid
            else {
                return
        }
        presenter.presentLoader()
        service.validate(params: params) { [weak self] response in
            switch response {
            case let .success(account):
                self?.presenter.presentPaymentDetails(account: account, params: params)
            case let .failure(error):
                self?.handleFailure(error: error)
            }
        }
    }
    
    private func createRequestParams() -> PIXParams? {
        guard
            let bankId = bankId,
            let bankName = bankName,
            let accountType = selectedAccountType,
            let accountTypeString = accountType.rowValue,
            let agency = agency,
            let accountNumber = accountNumber,
            let ownershipType = selectedOwnershipType,
            let ownership = ownershipType.rowValue
        else {
            return nil
        }
        var cpfCnpj: String?
        if ownershipType == .otherPersonal {
            cpfCnpj = self.cpfCnpj?.filter { $0.unicodeScalars.allSatisfy(CharacterSet.decimalDigits.contains) }
        }
        dependencies.analytics.log(ManualInsertionUserEvent.forwarded(institution: bankId, accountType: accountType, ownership: ownershipType))
        let receiverData = PIXReceiverData(
            name: nickname,
            bankId: bankId,
            bankName: bankName,
            accountType: accountTypeString,
            agency: agency,
            accountNumber: accountNumber,
            cpfCnpj: cpfCnpj,
            ownership: ownership
        )
        return PIXParams(originType: .manual, keyType: keyType, key: nil, receiverData: receiverData)
    }
    
    private func handleFailure(error: ApiError) {
        guard
            let alert = error.requestError?.alert,
            let title = alert.title?.string,
            let subtitle = alert.text?.string
            else {
                presenter.presentGenericError()
            return
        }
        presenter.presentError(
            title: title,
            subtitle: subtitle,
            jsonButtons: alert.buttonsJson
        )
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
}
