import Core
import Foundation
import UIKit

enum ManualInsertionFactory {
    static func make(bankSelectorType: BankSelector.Type, paymentOpening: PIXPaymentOpening) -> ManualInsertionViewController {
        let container = DependencyContainer()
        let service: ManualInsertionServicing = ManualInsertionService(dependencies: container)
        let coordinator: ManualInsertionCoordinating = ManualInsertionCoordinator(bankSelectorType: bankSelectorType, paymentOpening: paymentOpening)
        let presenter: ManualInsertionPresenting = ManualInsertionPresenter(coordinator: coordinator)
        let interactor = ManualInsertionInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = ManualInsertionViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
