import UIKit

enum ManualInsertionAction {
    case bankSelection(completion: (String, String) -> Void)
    case close
}

protocol ManualInsertionCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ManualInsertionAction)
    func goToPaymentDetailsWithPaymentDetails(_ details: PIXPaymentDetails)
}

final class ManualInsertionCoordinator {
    weak var viewController: UIViewController?
    private let bankSelectorType: BankSelector.Type
    private let paymentOpening: PIXPaymentOpening
    
    init(bankSelectorType: BankSelector.Type, paymentOpening: PIXPaymentOpening) {
        self.bankSelectorType = bankSelectorType
        self.paymentOpening = paymentOpening
    }
}

// MARK: - ManualInsertionCoordinating
extension ManualInsertionCoordinator: ManualInsertionCoordinating {
    func perform(action: ManualInsertionAction) {
        switch action {
        case let .bankSelection(completion):
            let controller = bankSelectorType.make(
                title: Strings.ManualInsertion.institution,
                searchPlaceholder: Strings.ManualInsertion.search,
                onBankSelectionCompletion: completion
            )
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .close:
            viewController?.navigationController?.dismiss(animated: true)
        }
    }
    
    func goToPaymentDetailsWithPaymentDetails(_ details: PIXPaymentDetails) {
        guard let viewController = viewController else {
            return
        }
        if #available(iOS 11.0, *) {
            viewController.navigationController?.navigationBar.prefersLargeTitles = false
        }
        paymentOpening.openPaymentWithDetails(details, onViewController: viewController, origin: .manual, flowType: .default)
    }
}
