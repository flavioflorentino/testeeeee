import UIKit

public protocol BankSelector {
    static func make(title: String, searchPlaceholder: String, onBankSelectionCompletion: @escaping (_ wsId: String, _ name: String) -> Void) -> UIViewController
}
