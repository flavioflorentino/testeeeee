import Core
import Foundation

protocol ManualInsertionServicing {
    func validate(params: PIXParams, completion: @escaping (Result<PixAccount, ApiError>) -> Void)
}

final class ManualInsertionService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ManualInsertionServicing
extension ManualInsertionService: ManualInsertionServicing {
    func validate(params: PIXParams, completion: @escaping (Result<PixAccount, ApiError>) -> Void) {
        let endpoint = PixServiceEndpoint.manualInsertion(params: params)
        Api<PixAccount>(endpoint: endpoint).execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                completion(mappedResult)
            }
        }
    }
}
