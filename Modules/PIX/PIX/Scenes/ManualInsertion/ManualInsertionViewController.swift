import AssetsKit
import SnapKit
import UI

protocol ManualInsertionDisplay: AnyObject {
    func displayCpfTextMask(_ textMask: TextMask)
    func displayCnpjTextMask(_ textMask: TextMask)
    func displayTextField(isSelected: Bool, type: ManualInsertionContent)
    func displayCpfCnpjErrorMessage(message: String)
    func removeCpfCnpjErrorMessage()
    func toggleForwardButtonState()
    func displayInstitutionSelection(text: String)
    func displayAccountTypeSelection()
    func displayOwnershipSelection()
    func beginLoading()
    func endLoading()
    func displayDialog(with alertData: StatusAlertData)
    func displayError(title: String, subtitle: String, actions: [PopupAction])
    func hideCpfCnpjAndNicknameFields()
    func displayCpfCnpjAndNicknameFields()
}

private enum Layout {
}

enum AccountType: CaseIterable, PickerOption {
    case checkingAccount
    case savingAccount
    
    var rowTitle: String? {
        switch self {
        case .checkingAccount:
            return Strings.ManualInsertion.checkingAccount
        case .savingAccount:
            return Strings.ManualInsertion.savingAccount
        }
    }
    
    var rowValue: String? {
        switch self {
        case .checkingAccount:
            return "CHECKING_ACCOUNT"
        case .savingAccount:
            return "SAVING_ACCOUNT"
        }
    }
}

enum Ownership: CaseIterable, PickerOption {
    case personal
    case otherPersonal
    
    var rowTitle: String? {
        switch self {
        case .personal:
            return Strings.ManualInsertion.personal
        case .otherPersonal:
            return Strings.ManualInsertion.otherPersonal
        }
    }
    
    var rowValue: String? {
        switch self {
        case .personal:
            return "PERSONAL"
        case .otherPersonal:
            return "OTHER_PERSONAL"
        }
    }
}

enum ManualInsertionContent: Int {
    case agency = 1
    case account
    case cpfCnpj
    case nickname
}

final class ManualInsertionViewController: ViewController<ManualInsertionInteracting, UIView> {
    typealias KeyboardInfo = (size: CGRect, animationDuration: TimeInterval, animationOptions: UIView.AnimationOptions)
    // MARK: - Visual Components
    private lazy var contentScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = Colors.backgroundPrimary.color
        scrollView.showsHorizontalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        stackView.distribution = .equalSpacing
        let rootEdges = EdgeInsets.rootView
        stackView.compatibleLayoutMargins = EdgeInsets(
            top: Spacing.base01,
            leading: rootEdges.leading,
            bottom: rootEdges.bottom,
            trailing: rootEdges.trailing
        )
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    private lazy var headerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        return stackView
    }()
    
    private lazy var cpfCnpjStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        stackView.isHidden = true
        return stackView
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale600.color)
        label.text = Strings.ManualInsertion.description
        return label
    }()
    
    private lazy var institutionTextField: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.delegate = self
        textfield.title = Strings.ManualInsertion.financialInstitution
        textfield.placeholder = Strings.ManualInsertion.financialInstitution
        textfield.rightViewMode = .always
        textfield.rightView = UIImageView(image: Assets.icoSearch.image)
        textfield.inputAccessoryView = doneToolbar
        return textfield
    }()
    
    private lazy var accountTypeTextField: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.delegate = self
        textfield.title = Strings.ManualInsertion.accountType
        textfield.placeholder = Strings.ManualInsertion.select
        textfield.setTitleVisible(true)
        textfield.rightViewMode = .always
        textfield.rightView = UIImageView(image: Assets.icoArrowDown.image)
        textfield.inputView = accountTypePicker
        textfield.inputAccessoryView = doneToolbar
        return textfield
    }()
    
    private lazy var agencyTextField: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.delegate = self
        textfield.title = Strings.ManualInsertion.agency
        textfield.placeholder = Strings.ManualInsertion.agency
        textfield.inputAccessoryView = doneToolbar
        textfield.keyboardType = .numberPad
        textfield.tag = ManualInsertionContent.agency.rawValue
        return textfield
    }()
    
    private lazy var bankAccountTextField: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.delegate = self
        textfield.title = Strings.ManualInsertion.accountWithDigit
        textfield.placeholder = Strings.ManualInsertion.accountWithDigit
        textfield.inputAccessoryView = doneToolbar
        textfield.keyboardType = .numberPad
        textfield.tag = ManualInsertionContent.account.rawValue
        return textfield
    }()
    
    private lazy var ownershipTextField: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.delegate = self
        textfield.title = Strings.ManualInsertion.ownership
        textfield.placeholder = Strings.ManualInsertion.select
        textfield.setTitleVisible(true)
        textfield.rightViewMode = .always
        textfield.rightView = UIImageView(image: Assets.icoArrowDown.image)
        textfield.inputView = ownershipTypePicker
        textfield.inputAccessoryView = doneToolbar
        return textfield
    }()
    
    private lazy var cpfOrCpnjTextField: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.delegate = self
        textfield.title = Strings.ManualInsertion.cpfOrCnpj
        textfield.placeholder = Strings.ManualInsertion.cpfOrCnpj
        textfield.inputAccessoryView = doneToolbar
        textfield.keyboardType = .numberPad
        textfield.tag = ManualInsertionContent.cpfCnpj.rawValue
        return textfield
    }()
    
    private lazy var cpfCnpjErrorLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, .critical900())
        label.isHidden = true
        return label
    }()
    
    private lazy var nicknameTextField: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.delegate = self
        textfield.title = Strings.ManualInsertion.name
        textfield.placeholder = Strings.ManualInsertion.name
        textfield.inputAccessoryView = doneToolbar
        textfield.tag = ManualInsertionContent.nickname.rawValue
        textfield.isHidden = true
        return textfield
    }()
    
    private lazy var forwardButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.General.forward, for: .normal)
        button.isEnabled = false
        button.addTarget(self, action: #selector(touchOnForward), for: .touchUpInside)
        return button
    }()
    
    private lazy var doneToolbar = DoneToolBar(doneText: Strings.General.ok)
    
    private lazy var accountTypePicker: PickerInputView = {
        let picker = PickerInputView(options: AccountType.allCases)
        picker.delegate = self
        return picker
    }()
    
    private lazy var ownershipTypePicker: PickerInputView = {
        let picker = PickerInputView(options: Ownership.allCases)
        picker.delegate = self
        return picker
    }()
    
    // MARK: - Variables
    private weak var focusedTextField: UITextField?
    private var heightConstant: Constraint?
    private lazy var handler = KeyboardScrollViewHandler(scrollView: contentScrollView, keyboardMargin: Spacing.base03)
    private lazy var agencyMasker = TextFieldMasker(textMask: CharacterLimitMask(maximumNumberOfCharacters: 5))
    private lazy var bankAccountMasker = TextFieldMasker(textMask: BankAccountMask())
    private lazy var cpfOrCnpjMasker = TextFieldMasker(textMask: CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf))
    private lazy var nicknameMasker = TextFieldMasker(textMask: CharacterLimitMask(maximumNumberOfCharacters: 100))
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadInitialConfig()
        handler.registerForKeyboardNotifications()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func buildViewHierarchy() {
        view.addSubview(contentScrollView)
        contentScrollView.addSubview(contentStackView)
        
        contentStackView.addArrangedSubview(headerStackView)
        contentStackView.addArrangedSubview(forwardButton)
        
        headerStackView.addArrangedSubview(descriptionLabel)
        headerStackView.addArrangedSubview(institutionTextField)
        headerStackView.addArrangedSubview(accountTypeTextField)
        headerStackView.addArrangedSubview(agencyTextField)
        headerStackView.addArrangedSubview(bankAccountTextField)
        headerStackView.addArrangedSubview(ownershipTextField)
        headerStackView.addArrangedSubview(cpfCnpjStackView)
        headerStackView.addArrangedSubview(nicknameTextField)
        
        cpfCnpjStackView.addArrangedSubview(cpfOrCpnjTextField)
        cpfCnpjStackView.addArrangedSubview(cpfCnpjErrorLabel)
    }
    
    override func setupConstraints() {
        contentScrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentStackView.snp.makeConstraints {
            $0.edges.width.equalToSuperview()
            heightConstant = $0.height.greaterThanOrEqualTo(view.layoutMarginsGuide).constraint
        }
    }

    override func configureViews() {
        configNavigation()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.title = Strings.ManualInsertion.title
        view.backgroundColor = Colors.backgroundPrimary.color
        agencyMasker.bind(to: agencyTextField)
        bankAccountMasker.bind(to: bankAccountTextField)
        cpfOrCpnjTextField.addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
        cpfOrCnpjMasker.bind(to: cpfOrCpnjTextField)
        nicknameMasker.bind(to: nicknameTextField)
        agencyTextField.addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
        bankAccountTextField.addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
        nicknameTextField.addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
        accountTypePicker.inputInterfaceElement = accountTypeTextField
        ownershipTypePicker.inputInterfaceElement = ownershipTextField
    }
    
    private func configNavigation() {
        guard navigationController?.viewControllers.count == 1 else {
            return
        }
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: Strings.General.close, style: .plain, target: self, action: #selector(close))
    }
    
    // MARK: - Actions
    @objc
    private func touchOnInstitutionSelection() {
        interactor.selectBank()
    }
    
    @objc
    private func textDidChanged(_ textField: UITextField) {
        guard let type = ManualInsertionContent(rawValue: textField.tag) else {
            return
        }
        interactor.verifyText(textField.text, type: type)
    }
    
    @objc
    private func touchOnForward() {
        interactor.selectForward()
    }
    
    @objc
    private func close() {
        interactor.close()
    }
}

// MARK: - TextField
private extension ManualInsertionViewController {
    func textFieldBy(type: ManualInsertionContent) -> UIPPFloatingTextField {
        switch type {
        case .agency:
            return agencyTextField
        case .account:
            return bankAccountTextField
        case .cpfCnpj:
            return cpfOrCpnjTextField
        case .nickname:
            return nicknameTextField
        }
    }
    
    func removeCpfOrCnpjTextFieldBinds() {
        cpfOrCnpjMasker.unbind(from: cpfOrCpnjTextField)
        cpfOrCpnjTextField.removeTarget(self, action: #selector(textDidChanged), for: .editingChanged)
    }
    
    func changeCpfOrCnpjTextMask(_ textMask: TextMask) {
        cpfOrCnpjMasker.textMask = textMask
        cpfOrCpnjTextField.sendActions(for: .editingChanged)
    }
}

// MARK: - Keyboard
private extension ManualInsertionViewController {
    @objc
     func keyboardDidShow(_ notification: Notification) {
        guard let keyboardFrame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else {
            return
        }
        heightConstant?.update(inset: keyboardFrame.height)
    }
    
    @objc
    func keyboardWillHide(_ notification: Notification) {
        heightConstant?.update(inset: 0)
    }
}

// MARK: ManualInsertionDisplay
extension ManualInsertionViewController: ManualInsertionDisplay {
    func displayCpfTextMask(_ textMask: TextMask) {
        removeCpfOrCnpjTextFieldBinds()
        cpfOrCpnjTextField.addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
        cpfOrCnpjMasker.bind(to: cpfOrCpnjTextField)
        changeCpfOrCnpjTextMask(textMask)
    }
    
    func displayCnpjTextMask(_ textMask: TextMask) {
        removeCpfOrCnpjTextFieldBinds()
        cpfOrCnpjMasker.bind(to: cpfOrCpnjTextField)
        cpfOrCpnjTextField.addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
        changeCpfOrCnpjTextMask(textMask)
    }
    
    func displayTextField(isSelected: Bool, type: ManualInsertionContent) {
        textFieldBy(type: type).isSelected = isSelected
    }
    
    func displayCpfCnpjErrorMessage(message: String) {
        cpfCnpjErrorLabel.text = message
        cpfCnpjErrorLabel.isHidden = false
    }
    
    func removeCpfCnpjErrorMessage() {
        cpfCnpjErrorLabel.text = nil
        cpfCnpjErrorLabel.isHidden = true
    }
    
    func toggleForwardButtonState() {
        forwardButton.isEnabled.toggle()
    }
    
    func displayInstitutionSelection(text: String) {
        institutionTextField.text = text
        institutionTextField.isSelected = true
    }
    
    func displayAccountTypeSelection() {
        accountTypeTextField.isSelected = true
    }
    
    func displayOwnershipSelection() {
        ownershipTextField.isSelected = true
    }
    
    func beginLoading() {
        focusedTextField?.resignFirstResponder()
        beginState(model: StateLoadingViewModel(message: Strings.General.waitProcessingPix))
    }
    
    func endLoading() {
        endState()
    }
    
    func displayDialog(with alertData: StatusAlertData) {
        let alert = StatusAlertView.show(on: view, data: alertData)
        alert.statusAlertDelegate = self
    }

    func displayError(title: String, subtitle: String, actions: [PopupAction]) {
        let controller = PopupViewController(title: title,
                                             description: subtitle,
                                             preferredType: .text)
        actions.forEach(controller.addAction)
        showPopup(controller)
    }
    
    func hideCpfCnpjAndNicknameFields() {
        cpfCnpjStackView.isHidden = true
        nicknameTextField.isHidden = true
    }
    
    func displayCpfCnpjAndNicknameFields() {
        cpfCnpjStackView.isHidden = false
        nicknameTextField.isHidden = false
    }
}

// MARK: - UITextFieldDelegate
extension ManualInsertionViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        guard textField !== institutionTextField else {
            touchOnInstitutionSelection()
            return false
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        focusedTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        focusedTextField = nil
    }
}

// MARK: - PickerInputViewDelegate
extension ManualInsertionViewController: PickerInputViewDelegate {
    func didSelectOption(_ option: PickerOption?, on pickerInputView: PickerInputView) {
        interactor.selectPickerOption(option)
    }
}

// MARK: StatefulProviding
extension ManualInsertionViewController: StatefulProviding {}

// MARK: StatusAlertViewDelegate
extension ManualInsertionViewController: StatusAlertViewDelegate {}
