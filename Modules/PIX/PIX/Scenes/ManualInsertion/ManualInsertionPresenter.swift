import AssetsKit
import Foundation
import UI
import UIKit

protocol ManualInsertionPresenting: AnyObject {
    var viewController: ManualInsertionDisplay? { get set }
    func didNextStep(action: ManualInsertionAction)
    func presentCpfMasker()
    func presentCNPJMasker()
    func presentTextFieldValidState(type: ManualInsertionContent)
    func presentTextFieldNormalState(type: ManualInsertionContent)
    func removeCpfCnpjErrorMessage()
    func presentCpfErrorMessage()
    func presentCnpjErrorMessage()
    func toggleForwardButtonState()
    func presentInstitutionSelection(id: String, name: String)
    func presentAccountTypeSelection()
    func presentOwnershipSelection()
    func presentPaymentDetails(account: PixAccount, params: PIXParams)
    func presentLoader()
    func presentGenericError()
    func presentError(title: String, subtitle: String, jsonButtons: [[String: Any]])
    func hideCpfCnpjAndNicknameFields()
    func presentCpfCnpjAndNicknameFields()
}

final class ManualInsertionPresenter {
    private let coordinator: ManualInsertionCoordinating
    weak var viewController: ManualInsertionDisplay?

    init(coordinator: ManualInsertionCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ManualInsertionPresenting
extension ManualInsertionPresenter: ManualInsertionPresenting {
    func didNextStep(action: ManualInsertionAction) {
        coordinator.perform(action: action)
    }
    
    func presentCpfMasker() {
        let textMask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf)
        viewController?.displayCpfTextMask(textMask)
    }
    
    func presentCNPJMasker() {
        let textMask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cnpj)
        viewController?.displayCnpjTextMask(textMask)
    }
    
    func presentTextFieldValidState(type: ManualInsertionContent) {
        viewController?.displayTextField(isSelected: true, type: type)
    }
    
    func presentTextFieldNormalState(type: ManualInsertionContent) {
        viewController?.displayTextField(isSelected: false, type: type)
    }
    
    func removeCpfCnpjErrorMessage() {
        viewController?.removeCpfCnpjErrorMessage()
    }
    
    func presentCpfErrorMessage() {
        let message = Strings.ManualInsertion.invalidFormat
        viewController?.displayCpfCnpjErrorMessage(message: message)
    }
    
    func presentCnpjErrorMessage() {
        let message = Strings.ManualInsertion.invalidFormat
        viewController?.displayCpfCnpjErrorMessage(message: message)
    }
    
    func toggleForwardButtonState() {
        viewController?.toggleForwardButtonState()
    }
    
    func presentInstitutionSelection(id: String, name: String) {
        let text = "\(id) - \(name)"
        viewController?.displayInstitutionSelection(text: text)
    }
    
    func presentAccountTypeSelection() {
        viewController?.displayAccountTypeSelection()
    }
    
    func presentOwnershipSelection() {
        viewController?.displayOwnershipSelection()
    }
    
    func presentPaymentDetails(account: PixAccount, params: PIXParams) {
        let firstPartDescription = "\(account.cpfCnpj) | \(account.bank)"
        var secondPartDescription = ""
        if let agency = account.agency, let accountNumber = account.accountNumber {
            secondPartDescription = "\n\(agency) | \(accountNumber)"
        }
        let description = firstPartDescription + secondPartDescription
        let payee = PIXPayee(
            name: account.name,
            description: description,
            imageURLString: account.imageUrl,
            id: account.id
        )
        let details = PIXPaymentDetails(
            title: Strings.PaymentScreen.title,
            screenType: .default(commentPlaceholder:Strings.PaymentScreen.commentPlaceholder),
            payee: payee,
            pixParams: params
        )
        viewController?.endLoading()
        coordinator.goToPaymentDetailsWithPaymentDetails(details)
    }
    
    func presentLoader() {
        viewController?.beginLoading()
    }
    
    func presentGenericError() {
        let alertData = StatusAlertData(
            icon: Resources.Illustrations.iluFalling.image,
            title: Strings.General.somethingWentWrong,
            text: Strings.General.requestNotCompleted,
            buttonTitle: Strings.General.tryAgain
        )
        viewController?.displayDialog(with: alertData)
        viewController?.endLoading()
    }
    
    func presentError(title: String, subtitle: String, jsonButtons: [[String: Any]]) {
        let actions = jsonButtons.compactMap(createPopupAction)
        viewController?.displayError(title: title, subtitle: subtitle, actions: actions)
        viewController?.endLoading()
    }
    
    private func createPopupAction(jsonButton: [String: Any]) -> PopupAction? {
        guard let title = jsonButton["title"] as? String else {
            return nil
        }
        return PopupAction(title: title, style: .fill)
    }
    
    func hideCpfCnpjAndNicknameFields() {
        viewController?.hideCpfCnpjAndNicknameFields()
    }
    
    func presentCpfCnpjAndNicknameFields() {
        viewController?.displayCpfCnpjAndNicknameFields()
    }
}
