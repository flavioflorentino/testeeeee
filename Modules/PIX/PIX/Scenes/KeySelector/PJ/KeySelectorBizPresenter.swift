import FeatureFlag
import Foundation
import UI
import UIKit
import AssetsKit

protocol KeySelectorBizPresenting: AnyObject {
    var viewController: KeySelectorDisplay? { get set }
    func didNextStep(action: KeySelectorBizAction)
    func presentViewTitle()
    func presentKeySelectionStyle(newType: KeyType, previousType: KeyType?)
    func presentMask(keyType: KeyType)
    func presentTextFieldBindAndContent(newType: KeyType, previousType: KeyType?)
    func presentTextFieldNormalState()
    func presentTextFieldFormatError()
    func presentTextFieldPhoneFormatError()
    func presentTextFieldNonexistentError()
    func presentTextFieldServiceError(message: String)
    func presentForwardButtonState(isEnabled: Bool)
    func presentTextFieldSelection(isSelected: Bool)
    func validateSelectedKey(_ key: String, keyType: KeyType)
    func cleanTextFieldContent()
    func presentTextFieldRightView(keyType: KeyType)
    func presentSelectedNumber(_ number: String)
}

final class KeySelectorBizPresenter {
    typealias Dependencies = HasFeatureManager

    private let coordinator: KeySelectorBizCoordinating
    private let dependencies: Dependencies
    weak var viewController: KeySelectorDisplay?
    
    private let emailMaskCount = 77
    private let brazilianPhonePrefix = "+55"

    init(coordinator: KeySelectorBizCoordinating,
         dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - KeySelectorPresenting
extension KeySelectorBizPresenter: KeySelectorBizPresenting {
    func didNextStep(action: KeySelectorBizAction) {
        coordinator.perform(action: action)
    }
    
    func presentViewTitle() {
        viewController?.displayViewTitle(text: Strings.KeySelection.chooseAKey, color: Colors.grayscale700.lightColor)
    }
    
    func presentKeySelectionStyle(newType: KeyType, previousType: KeyType?) {
        handleKeySelectionTexts(type: newType)
        handleKeySelectionColors(newType: newType, previousType: previousType)
    }
    
    func presentMask(keyType: KeyType) {
        let mask: TextMask
        switch keyType {
        case .cpf:
            mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf)
        case .cnpj:
            mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cnpj)
        case .phone:
            if dependencies.featureManager.isActive(.featurePixkeyPhoneWithDDI) {
                mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cellPhoneWithDDDAndDDI)
            } else {
                mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cellPhoneWithDDD)
            }
        case .email:
            mask = CharacterLimitMask(maximumNumberOfCharacters: emailMaskCount)
        case .random:
            mask = CustomStringMask(descriptor: PixMaskDescriptor.randomKey)
        }
        viewController?.displayMask(mask: mask)
    }
    
    func presentTextFieldBindAndContent(newType: KeyType, previousType: KeyType?) {
        switch newType {
        case .cpf:
            if let previousType = previousType, previousType != .cnpj {
                viewController?.cleanTextFieldContent()
            }
            viewController?.bindTextFieldInverted()
        case .cnpj:
            viewController?.bindTextFieldNormally()
        default:
            viewController?.cleanTextFieldContent()
            viewController?.bindTextFieldNormally()
        }
    }
    
    func presentTextFieldNormalState() {
        let color = Colors.branding400.color
        viewController?.displayTextFieldState(color: color, errorMessage: nil)
    }
    
    func presentTextFieldFormatError() {
        let color = Colors.critical900.color
        let errorMessage = Strings.KeySelection.formatErrorMessage
        viewController?.displayTextFieldState(color: color, errorMessage: errorMessage)
    }
    
    func presentTextFieldPhoneFormatError() {
        let color = Colors.critical900.color
        let errorMessage = Strings.KeySelection.phoneFormatErrorMessage
        viewController?.displayTextFieldState(color: color, errorMessage: errorMessage)
    }
    
    func presentTextFieldNonexistentError() {
        let color = Colors.critical900.color
        let errorMessage = Strings.KeySelection.nonexistentErrorMessage
        viewController?.displayTextFieldState(color: color, errorMessage: errorMessage)
    }
    
    func presentTextFieldServiceError(message: String) {
        let color = Colors.critical900.color
        viewController?.displayTextFieldState(color: color, errorMessage: message)
        viewController?.displayEndState()
    }
    
    func presentForwardButtonState(isEnabled: Bool) {
        viewController?.displayForwardButtonState(isEnabled: isEnabled)
    }
    
    func presentTextFieldSelection(isSelected: Bool) {
        viewController?.displayTextFieldSelection(isSelected: isSelected)
    }
    
    func validateSelectedKey(_ key: String, keyType: KeyType) {
        coordinator.perform(action: .validateKey(key: key, keyType: keyType))
        viewController?.displayEndState()
    }

    func cleanTextFieldContent() {
        viewController?.cleanTextFieldContent()
    }
    
    func presentTextFieldRightView(keyType: KeyType) {
        switch keyType {
        case .phone:
            viewController?.displayTextFieldRightView()
        default:
            viewController?.hideTextFieldRightView()
        }
    }

    func presentSelectedNumber(_ number: String) {
        viewController?.displaySelectedNumber(number)
    }
}

private extension KeySelectorBizPresenter {
    func handleKeySelectionTexts(type: KeyType) {
        let title: String
        var text = String()
        let keyboardType: UIKeyboardType
        let keyboardContentType: UITextContentType?
        switch type {
        case .cpf, .cnpj:
            title = Strings.KeySelection.cpfCnpjTitle
            keyboardType = .numberPad
            keyboardContentType = nil
        case .phone:
            title = Strings.KeySelection.cellphoneTitle
            keyboardContentType = .telephoneNumber
            if dependencies.featureManager.isActive(.featurePixkeyPhoneWithDDI) {
                text = brazilianPhonePrefix
                keyboardType = .phonePad
            } else {
                keyboardType = .numberPad
            }
        case .email:
            title = Strings.KeySelection.emailTitle
            keyboardType = .emailAddress
            keyboardContentType = .emailAddress
        case .random:
            title = Strings.KeySelection.randomKeyTitle
            keyboardType = .default
            keyboardContentType = nil
        }
        viewController?.displayKeySelection(
            title: title,
            text: text,
            keyboardType: keyboardType,
            keyboardContentType: keyboardContentType
        )
    }
    
    func handleKeySelectionColors(newType: KeyType, previousType: KeyType?) {
        viewController?.displayButtonStyle(
            backgroundColor: Colors.grayscale750.color,
            titleColor: Colors.white.color,
            index: newType.rawValue
        )
        guard let previousType = previousType else {
            return
        }
        viewController?.displayButtonStyle(
            backgroundColor: Colors.backgroundTertiary.color,
            titleColor: Colors.grayscale600.color,
            index: previousType.rawValue
        )
    }
}
