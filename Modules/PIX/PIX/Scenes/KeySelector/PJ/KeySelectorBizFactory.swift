import UIKit

typealias KeySelectorBizOutput = (KeySelectorBizAction) -> Void

enum KeySelectorBizFactory {
    static func make(userInfo: KeyManagerBizUserInfo?, output: @escaping KeySelectorBizOutput) -> KeySelectorViewController {
        let container = DependencyContainer()
        let coordinator = KeySelectorBizCoordinator(output: output)
        let presenter = KeySelectorBizPresenter(coordinator: coordinator, dependencies: container)
        let interactor = KeySelectorBizInteractor(presenter: presenter, dependencies: container, userInfo: userInfo)
        let viewController = KeySelectorViewController(interactor: interactor)

        presenter.viewController = viewController

        return viewController
    }
}
