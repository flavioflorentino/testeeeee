import UIKit

enum KeySelectorBizAction {
    case validateKey(key: String, keyType: KeyType)
    case agenda(completion: (String) -> Void)
}

protocol KeySelectorBizCoordinating: AnyObject {
    func perform(action: KeySelectorBizAction)
}

final class KeySelectorBizCoordinator {
    private let output: KeySelectorBizOutput

    init(output: @escaping KeySelectorBizOutput) {
        self.output = output
    }
}

// MARK: - KeySelectorBizCoordinating
extension KeySelectorBizCoordinator: KeySelectorBizCoordinating {
    func perform(action: KeySelectorBizAction) {
        output(action)
    }
}
