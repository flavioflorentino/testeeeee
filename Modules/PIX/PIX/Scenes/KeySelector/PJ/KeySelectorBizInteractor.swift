import AnalyticsModule
import Core
import FeatureFlag
import Foundation
import UI
import Validations

final class KeySelectorBizInteractor {
    typealias Dependencies = HasAnalytics & HasFeatureManager & HasUserAuthManager
    
    // MARK: - Variables
    private let dependencies: Dependencies
    private let presenter: KeySelectorBizPresenting
    private var selectedKeyType: KeyType = .cpf
    private var currentTextMaskType: KeyType = .cpf
    private lazy var validators: [KeyType: StringValidationRule] = {
        var validators: [KeyType: StringValidationRule] = [:]
        KeyType.allCases.forEach({ validators[$0] = self.validator(with: $0) })
        return validators
    }()
    private let brazilianPhonePrefix = "+55"
    private var key = ""
    private var userInfo: KeyManagerBizUserInfo?
    private lazy var isFeaturePixkeyPhoneWithDDIOn = dependencies.featureManager.isActive(.featurePixkeyPhoneWithDDI)

    private lazy var isPixKeysAgendaAvailable = dependencies.featureManager.isActive(.isPixKeysAgendaAvailable)

    // MARK: - Life Cycle
    init(presenter: KeySelectorBizPresenting, dependencies: Dependencies, userInfo: KeyManagerBizUserInfo?) {
        self.userInfo = userInfo
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - KeySelectorInteracting
extension KeySelectorBizInteractor: KeySelectorInteracting {
    func loadInitialData() {
        presenter.presentViewTitle()
        handleAgenda(keyType: selectedKeyType)
        presenter.presentKeySelectionStyle(newType: selectedKeyType, previousType: nil)
        presenter.presentMask(keyType: selectedKeyType)
        presenter.presentTextFieldBindAndContent(newType: selectedKeyType, previousType: nil)
    }
    
    func selectKey(type newType: KeyType) {
        guard selectedKeyType != newType else { return }
        let previousType = selectedKeyType
        selectedKeyType = newType
        currentTextMaskType = newType
        handleAgenda(keyType: selectedKeyType)
        presenter.presentTextFieldBindAndContent(newType: newType, previousType: previousType)
        presenter.presentKeySelectionStyle(newType: newType, previousType: previousType)
        presenter.presentMask(keyType: newType)
        presenter.presentTextFieldNormalState()
        presenter.presentForwardButtonState(isEnabled: false)
    }
    
    func verifyKey(_ key: String?) {
        guard let key = key else { return }
        verifyKeyContent(key)
    }
    
    func forwardAction() {
        dependencies.analytics.log(KeyBizTracker(companyName: userInfo?.name ?? "", eventType: .sendWithKeys(method: selectedKeyType)))
        let serviceKeyType: KeyType = handleSelectedKey(selectedKeyType, currentTextMaskType: currentTextMaskType)
        let finalKey = clearKey(key, type: serviceKeyType)
        presenter.validateSelectedKey(finalKey, keyType: serviceKeyType)
    }

    func processTextInput(_ text: String) {
        guard isFeaturePixkeyPhoneWithDDIOn else {
            return
        }

        if selectedKeyType == .phone, text.count > 1, let firstChar = text.first, firstChar == "+" {
            presenter.cleanTextFieldContent()
        }
    }

    func close() {
    }
    
    func showContactList() {
        presenter.didNextStep(action: .agenda(completion: didSelectNumberInAgenda))
    }
    
    private func handleAgenda(keyType: KeyType) {
        guard isPixKeysAgendaAvailable else {
            return
        }
        presenter.presentTextFieldRightView(keyType: keyType)
    }
}

private extension KeySelectorBizInteractor {
    func verifyKeyContent(_ key: String) {
        do {
            selectedKeyType == .cpf ? try handleCPFValidation(key: key) : try validators[selectedKeyType]?.validate(key)
            self.key = key
            presenter.presentTextFieldNormalState()
            presenter.presentTextFieldSelection(isSelected: true)
            presenter.presentForwardButtonState(isEnabled: true)
        } catch GenericValidationError.incompleteData {
            presenter.presentTextFieldNormalState()
            presenter.presentTextFieldSelection(isSelected: false)
            presenter.presentForwardButtonState(isEnabled: false)
        } catch {
            if selectedKeyType == .phone {
                presenter.presentTextFieldPhoneFormatError()
            } else {
                presenter.presentTextFieldFormatError()
            }
            presenter.presentTextFieldSelection(isSelected: false)
            presenter.presentForwardButtonState(isEnabled: false)
        }
    }
    
    func handleCPFValidation(key: String) throws {
        if key.count <= PersonalDocumentMaskDescriptor.cpf.format.count {
            verifyCurrentMask(newType: .cpf, previousType: .cnpj)
            try validators[.cpf]?.validate(key)
        } else {
            verifyCurrentMask(newType: .cnpj, previousType: .cpf)
            try validators[.cnpj]?.validate(key)
        }
    }
    
    func verifyCurrentMask(newType: KeyType, previousType: KeyType) {
        if currentTextMaskType != newType {
            currentTextMaskType = newType
            presenter.presentMask(keyType: newType)
            presenter.presentTextFieldBindAndContent(newType: newType, previousType: previousType)
        }
    }
    
    func handleSelectedKey(_ selectedKey: KeyType, currentTextMaskType: KeyType) -> KeyType {
        if selectedKey == .cpf && currentTextMaskType == .cnpj {
            return .cnpj
        } else {
            return selectedKey
        }
    }
    
    func clearKey(_ key: String, type: KeyType) -> String {
        switch type {
        case .cpf, .cnpj:
            return key.filter { $0.unicodeScalars.allSatisfy(CharacterSet.decimalDigits.contains) }
        case .phone:
            if isFeaturePixkeyPhoneWithDDIOn {
                return key
            } else {
                let numbersString = key.filter { $0.unicodeScalars.allSatisfy(CharacterSet.decimalDigits.contains) }
                return "\(brazilianPhonePrefix)\(numbersString)"
            }
        default:
            return key
        }
    }

    func validator(with keytype: KeyType) -> StringValidationRule {
        switch keytype {
        case .cpf:
            return CPFValidator()
        case .cnpj:
            return CNPJValidator()
        case .phone:
            if isFeaturePixkeyPhoneWithDDIOn {
                return CellphoneWithDDDAndDDIValidator()
            } else {
                return CellphoneWithDDDValidator()
            }
        case .email:
            return RegexValidator(descriptor: AccountRegexDescriptor.email)
        case .random:
            return RangeValidator(count: PixMaskDescriptor.randomKey.format.count)
        }
    }

    func didSelectNumberInAgenda(_ number: String) {
        guard isFeaturePixkeyPhoneWithDDIOn else {
            presenter.presentSelectedNumber(number)
            return
        }
        let finalNumber = number.first == "+" ? number : brazilianPhonePrefix + number
        presenter.presentSelectedNumber(finalNumber)
    }
}
