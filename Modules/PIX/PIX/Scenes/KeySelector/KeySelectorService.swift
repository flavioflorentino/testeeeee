import Core
import Foundation

protocol KeySelectorServicing {
    func validate(key: String, type: KeyType, completion: @escaping (Result<PixAccount, ApiError>) -> Void)
}

final class KeySelectorService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - KeySelectorServicing
extension KeySelectorService: KeySelectorServicing {
    func validate(key: String, type: KeyType, completion: @escaping (Result<PixAccount, ApiError>) -> Void) {
        let endpoint = PixServiceEndpoint.keyValidator(key: key, type: type)
        Api<PixAccount>(endpoint: endpoint).execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                completion(mappedResult)
            }
        }
    }
}
