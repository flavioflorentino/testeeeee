import AssetsKit
import Core
import Foundation
import SnapKit
import UI
import UIKit

protocol KeySelectorDisplay: AnyObject {
    func displayKeySelection(
        title: String,
        text: String,
        keyboardType: UIKeyboardType,
        keyboardContentType: UITextContentType?
    )
    func displayMask(mask: TextMask)
    func cleanTextFieldContent()
    func bindTextFieldNormally()
    func bindTextFieldInverted()
    func displayButtonStyle(backgroundColor: UIColor, titleColor: UIColor, index: Int)
    func displayTextFieldState(color: UIColor, errorMessage: String?)
    func displayForwardButtonState(isEnabled: Bool)
    func displayDialog(with alertData: StatusAlertData)
    func displayEndState()
    func displayTextFieldSelection(isSelected: Bool)
    func displayError(title: String, subtitle: String, actions: [PopupAction])
    func displayViewTitle(text: String, color: UIColor)
    func displayTextFieldRightView()
    func hideTextFieldRightView()
    func displaySelectedNumber(_ number: String)
    func displayCopiedKey(key: String)
}

private extension KeySelectorViewController.Layout {
    enum Size {
        static let buttonHeight: CGFloat = 56
        static let lineHeight: CGFloat = 1
    }
}

private extension KeyType {
    var text: String {
        switch self {
        case .cpf, .cnpj:
            return Strings.KeySelection.cpfCnpj
        case .phone:
            return Strings.KeySelection.cellphone
        case .email:
            return Strings.KeySelection.email
        case .random:
            return Strings.KeySelection.randomKey
        }
    }
}

final class KeySelectorViewController: ViewController<KeySelectorInteracting, UIView> {
    fileprivate enum Layout { }
    
    // MARK: - Variables
    private lazy var buttonsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base01
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale600())
        label.text = Strings.KeySelection.insertKey
        return label
    }()
    
    private lazy var keyTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.delegate = self
        return textField
    }()
    
    private lazy var errorLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, .critical900())
        return label
    }()
    
    private lazy var forwardButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.General.forward, for: .normal)
        button.isEnabled = false
        button.addTarget(self, action: #selector(tapOnForward), for: .touchUpInside)
        return button
    }()
    
    private lazy var agendaButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(tapAgenda), for: .touchUpInside)
        button.setImage(Resources.Icons.icoAgenda.image, for: .normal)
        return button
    }()
    
    private lazy var keyButtons: [UIButton] = {
        let types: [KeyType] = [.phone, .email, .cpf, .random]
        return types.map(createButton)
    }()
    
    private lazy var contentMask: TextFieldMasker = {
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf)
        let masker = TextFieldMasker(textMask: mask)
        masker.bind(to: keyTextField)
        return masker
    }()
    
    private var forwardButtonBottom: Constraint?
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadInitialData()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func buildViewHierarchy() {
        view.addSubviews(messageLabel, buttonsStackView, keyTextField, errorLabel, forwardButton)
        keyButtons.forEach(buttonsStackView.addArrangedSubview)
    }
    
    override func setupConstraints() {
        messageLabel.snp.makeConstraints {
            $0.top.equalTo(view.layoutMarginsGuide).offset(Spacing.base01).priority(.medium)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        buttonsStackView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.buttonHeight)
            $0.top.equalTo(messageLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        keyTextField.snp.makeConstraints {
            $0.top.equalTo(buttonsStackView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        errorLabel.snp.makeConstraints {
            $0.top.equalTo(keyTextField.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        forwardButton.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(errorLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            forwardButtonBottom = $0.bottom.equalTo(view.compatibleSafeArea.bottom).inset(Spacing.base02).constraint
        }
    }
    
    override func configureViews() {
        configNavigation()
        title = Strings.KeySelection.selectAKey
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    private func configNavigation() {
        guard navigationController?.viewControllers.count == 1 else {
            return
        }
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: Strings.General.close, style: .plain, target: self, action: #selector(close))
    }
    
    private func createButton(_ type: KeyType) -> UIButton {
        let button = UIButton()
        button.setTitle(type.text, for: .normal)
        button.titleLabel?.lineBreakMode = .byWordWrapping
        button.textAlignment = .center
        button.titleLabel?.font = Typography.caption(.highlight).font()
        button.setTitleColor(Colors.grayscale600.color, for: .normal)
        button.tag = type.rawValue
        button.addTarget(self, action: #selector(tapOnButton), for: .touchUpInside)
        button.viewStyle(CardViewStyle())
        return button
    }
    
    private func unbindTextField() {
        contentMask.unbind(from: keyTextField)
        keyTextField.removeTarget(self, action: #selector(textDidChanged), for: .editingChanged)
    }
    
    // MARK: - Keyboard
    @objc
    func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        let result = Spacing.base02 + keyboardSize.height
        let currentValue = forwardButtonBottom?.layoutConstraints.first?.constant
        if currentValue == -Spacing.base02 || currentValue != result {
            forwardButtonBottom?.update(inset: result)
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }

    @objc
    func keyboardWillHide(notification: NSNotification) {
        guard forwardButtonBottom?.layoutConstraints.first?.constant != -Spacing.base02 else {
            return
        }
        forwardButtonBottom?.update(inset: Spacing.base02)
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - Actions
    @objc
    private func tapOnButton(_ sender: UIButton) {
        guard let keyType = KeyType(rawValue: sender.tag) else {
            return
        }
        interactor.selectKey(type: keyType)
    }
    
    @objc
    private func textDidChanged(_ textField: UITextField) {
        interactor.verifyKey(textField.text)
    }
    
    @objc
    private func tapOnForward(_ sender: UIButton) {
        beginState(model: StateLoadingViewModel(message: Strings.General.waitProcessingPix))
        keyTextField.resignFirstResponder()
        interactor.forwardAction()
    }
    
    @objc
    private func close() {
        interactor.close()
    }
    
    @objc
    private func tapAgenda() {
        interactor.showContactList()
    }
}

// MARK: KeySelectorDisplay
extension KeySelectorViewController: KeySelectorDisplay {
    func displayKeySelection(
        title: String,
        text: String,
        keyboardType: UIKeyboardType,
        keyboardContentType: UITextContentType?
    ) {
        keyTextField.title = title
        keyTextField.text = text
        keyTextField.placeholder = title
        keyTextField.textContentType = keyboardContentType
        keyTextField.keyboardType = keyboardType
        keyTextField.reloadInputViews()
    }
    
    func displayMask(mask: TextMask) {
        contentMask.textMask = mask
        keyTextField.sendActions(for: .editingChanged)
    }
    
    func cleanTextFieldContent() {
        keyTextField.text = nil
        keyTextField.isSelected = false
    }
    
    func bindTextFieldNormally() {
        unbindTextField()
        contentMask.bind(to: keyTextField)
        keyTextField.addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
    }
    
    func bindTextFieldInverted() {
        unbindTextField()
        keyTextField.addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
        contentMask.bind(to: keyTextField)
    }
    
    func displayButtonStyle(backgroundColor: UIColor, titleColor: UIColor, index: Int) {
        let button = keyButtons[index]
        button.backgroundColor = backgroundColor
        button.setTitleColor(titleColor, for: .normal)
    }
    
    func displayTextFieldState(color: UIColor, errorMessage: String?) {
        keyTextField.selectedLineColor = color
        errorLabel.text = errorMessage
    }
    
    func displayForwardButtonState(isEnabled: Bool) {
        forwardButton.isEnabled = isEnabled
    }
    
    func displayDialog(with alertData: StatusAlertData) {
        let alert = StatusAlertView.show(on: view, data: alertData)
        alert.statusAlertDelegate = self
    }
    
    func displayEndState() {
        endState()
    }
    
    func displayTextFieldSelection(isSelected: Bool) {
        keyTextField.isSelected = isSelected
    }

    func displayError(title: String, subtitle: String, actions: [PopupAction]) {
        let controller = PopupViewController(title: title,
                                             description: subtitle,
                                             preferredType: .text)
        actions.forEach(controller.addAction)
        showPopup(controller)

        endState()
    }
    
    func displayViewTitle(text: String, color: UIColor) {
        title = text
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: Colors.grayscale700.lightColor]
        }
    }
    
    func displayTextFieldRightView() {
        keyTextField.rightView = agendaButton
        keyTextField.rightViewMode = .always
    }
    
    func hideTextFieldRightView() {
        keyTextField.rightView = nil
        keyTextField.rightViewMode = .never
    }
    
    func displaySelectedNumber(_ number: String) {
        keyTextField.text = number
        keyTextField.sendActions(for: .editingChanged)
    }
    
    func displayCopiedKey(key: String) {
        keyTextField.text = key
        keyTextField.sendActions(for: .editingChanged)
    }
}

extension KeySelectorViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        interactor.processTextInput(string)
        return true
    }
}

// MARK: StatefulProviding
extension KeySelectorViewController: StatefulProviding {}

// MARK: StatusAlertViewDelegate
extension KeySelectorViewController: StatusAlertViewDelegate {}
