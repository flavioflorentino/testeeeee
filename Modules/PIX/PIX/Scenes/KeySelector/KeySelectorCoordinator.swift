import UIKit

enum KeySelectorAction {
    case close
    case agenda(completion: (String) -> Void)
}

protocol KeySelectorCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: KeySelectorAction)
    func goToPaymentDetailsWithPaymentDetails(_ details: PIXPaymentDetails, origin: PixPaymentOrigin)
}

final class KeySelectorCoordinator {
    weak var viewController: UIViewController?
    let paymentOpening: PIXPaymentOpening
    
    init(paymentOpening: PIXPaymentOpening) {
        self.paymentOpening = paymentOpening
    }
}

// MARK: - KeySelectorCoordinating
extension KeySelectorCoordinator: KeySelectorCoordinating {
    func perform(action: KeySelectorAction) {
        switch action {
        case .close:
            viewController?.navigationController?.dismiss(animated: true)
        case let .agenda(completion):
            let controller = SelectPhoneNumberFactory.make(numberHandler: completion)
            viewController?.present(controller, animated: true)
        }
    }
    
    func goToPaymentDetailsWithPaymentDetails(_ details: PIXPaymentDetails, origin: PixPaymentOrigin) {
        guard let viewController = viewController else {
            return
        }
        if #available(iOS 11.0, *) {
            viewController.navigationController?.navigationBar.prefersLargeTitles = false
        }
        paymentOpening.openPaymentWithDetails(details, onViewController: viewController, origin: origin, flowType: .default)
    }
}
