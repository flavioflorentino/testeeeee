import AnalyticsModule
import Core
import FeatureFlag
import Foundation
import UI
import Validations

protocol KeySelectorInteracting: AnyObject {
    func loadInitialData()
    func selectKey(type newType: KeyType)
    func verifyKey(_ key: String?)
    func forwardAction()
    func processTextInput(_ text: String)
    func close()
    func showContactList()
}

final class KeySelectorInteractor {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    
    // MARK: - Variables
    private let dependencies: Dependencies
    private let service: KeySelectorServicing
    private let presenter: KeySelectorPresenting
    private var selectedKeyType: KeyType
    private var currentTextMaskType: KeyType
    private lazy var validators: [KeyType: StringValidationRule] = {
        var validators: [KeyType: StringValidationRule] = [:]
        KeyType.allCases.forEach({ validators[$0] = self.validator(with: $0) })
        return validators
    }()
    private let brazilianPhonePrefix = "+55"
    private var key = ""
    private var copiedKey: String

    // MARK: - Life Cycle
    init(service: KeySelectorServicing, presenter: KeySelectorPresenting, dependencies: Dependencies, selectedKeyType: KeyType, copiedKey: String) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.selectedKeyType = selectedKeyType
        self.currentTextMaskType = selectedKeyType
        self.copiedKey = copiedKey
    }
}

// MARK: - KeySelectorInteracting
extension KeySelectorInteractor: KeySelectorInteracting {
    func loadInitialData() {
        currentTextMaskType = selectedKeyType
        dependencies.analytics.log(KeySelectorUserEvent.userNavigated)
        dependencies.analytics.log(KeySelectorUserEvent.typeSelected(item: selectedKeyType))
        handleAgenda(keyType: selectedKeyType)
        presenter.presentMask(keyType: selectedKeyType)
        presenter.presentTextFieldBindAndContent(newType: selectedKeyType, previousType: nil)
        handleKeyTypeIsCNPJ()
        presenter.presentKeySelectionStyle(newType: selectedKeyType, previousType: nil)
        showCopiedKey()
    }
    
    func selectKey(type newType: KeyType) {
        guard selectedKeyType != newType else {
            return
        }
        let previousType = selectedKeyType
        selectedKeyType = newType
        currentTextMaskType = newType
        dependencies.analytics.log(KeySelectorUserEvent.typeSelected(item: newType))
        handleAgenda(keyType: selectedKeyType)
        presenter.presentTextFieldBindAndContent(newType: newType, previousType: previousType)
        presenter.presentKeySelectionStyle(newType: newType, previousType: previousType)
        presenter.presentMask(keyType: newType)
        presenter.presentTextFieldNormalState()
        presenter.presentForwardButtonState(isEnabled: false)
    }
    
    func verifyKey(_ key: String?) {
        guard let key = key else {
            return
        }
        verifyKeyContent(key)
    }
    
    private func verifyKeyContent(_ key: String) {
        do {
            if selectedKeyType == .cpf {
                try handleCPFValidation(key: key)
            } else {
                try validators[selectedKeyType]?.validate(key)
            }
            self.key = key
            presenter.presentTextFieldNormalState()
            presenter.presentTextFieldSelection(isSelected: true)
            presenter.presentForwardButtonState(isEnabled: true)
        } catch GenericValidationError.incompleteData {
            presenter.presentTextFieldNormalState()
            presenter.presentTextFieldSelection(isSelected: false)
            presenter.presentForwardButtonState(isEnabled: false)
        } catch {
            dependencies.analytics.log(KeySelectorUserEvent.errorViewed)
            if selectedKeyType == .phone {
                presenter.presentTextFieldPhoneFormatError()
            } else {
                presenter.presentTextFieldFormatError()
            }
            presenter.presentTextFieldSelection(isSelected: false)
            presenter.presentForwardButtonState(isEnabled: false)
        }
    }
    
    private func handleCPFValidation(key: String) throws {
        if key.count <= PersonalDocumentMaskDescriptor.cpf.format.count {
            verifyCurrentMask(newType: .cpf, previousType: .cnpj)
            try validators[.cpf]?.validate(key)
        } else {
            verifyCurrentMask(newType: .cnpj, previousType: .cpf)
            try validators[.cnpj]?.validate(key)
        }
    }
    
    private func verifyCurrentMask(newType: KeyType, previousType: KeyType) {
        if currentTextMaskType != newType {
            currentTextMaskType = newType
            presenter.presentMask(keyType: newType)
            presenter.presentTextFieldBindAndContent(newType: newType, previousType: previousType)
        }
    }
    
    func forwardAction() {
        let serviceKey = handleSelectedKey(selectedKeyType, currentTextMaskType: currentTextMaskType)
        let finalKey = clearKey(key, type: serviceKey)
        dependencies.analytics.log(KeySelectorUserEvent.forwarded(keyType: serviceKey))
        service.validate(key: finalKey, type: serviceKey) { [weak self] result in
            switch result {
            case let .success(response):
                self?.validateSuccess(account: response)
            case let .failure(error):
                self?.validateFailure(error: error)
            }
        }
    }

    func processTextInput(_ text: String) {
        guard dependencies.featureManager.isActive(.featurePixkeyPhoneWithDDI) else {
            return
        }

        if selectedKeyType == .phone, text.count > 1, let firstChar = text.first, firstChar == "+" {
            presenter.cleanTextFieldContent()
        }
    }
    
    private func handleSelectedKey(_ selectedKey: KeyType, currentTextMaskType: KeyType) -> KeyType {
        if selectedKey == .cpf && currentTextMaskType == .cnpj {
            return .cnpj
        } else {
            return selectedKey
        }
    }
    
    private func clearKey(_ key: String, type: KeyType) -> String {
        switch type {
        case .cpf, .cnpj:
            return key.filter { $0.unicodeScalars.allSatisfy(CharacterSet.decimalDigits.contains) }
        case .phone:
            if dependencies.featureManager.isActive(.featurePixkeyPhoneWithDDI) {
                return key
            } else {
                let numbersString = key.filter { $0.unicodeScalars.allSatisfy(CharacterSet.decimalDigits.contains) }
                return "\(brazilianPhonePrefix)\(numbersString)"
            }
        default:
            return key
        }
    }
    
    private func validateSuccess(account: PixAccount) {
        let keyType = handleSelectedKey(selectedKeyType, currentTextMaskType: currentTextMaskType)
        let finalKey = clearKey(key, type: keyType)
        presenter.presentPaymentDetailsWithAccount(account, keyType: keyType, key: finalKey)
    }
    
    private func validateFailure(error: ApiError) {
        guard
            let alert = error.requestError?.alert,
            let title = alert.title?.string,
            let subtitle = alert.text?.string
            else {
                presenter.presentGenericError()
                return
            }
        presenter.presentError(
            title: title,
            subtitle: subtitle,
            jsonButtons: alert.buttonsJson
        )
    }

    private func validator(with keytype: KeyType) -> StringValidationRule {
        switch keytype {
        case .cpf:
            return CPFValidator()
        case .cnpj:
            return CNPJValidator()
        case .phone:
            if dependencies.featureManager.isActive(.featurePixkeyPhoneWithDDI) {
                return CellphoneWithDDDAndDDIValidator()
            } else {
                return CellphoneWithDDDValidator()
            }
        case .email:
            return RegexValidator(descriptor: AccountRegexDescriptor.email)
        case .random:
            return RangeValidator(count: PixMaskDescriptor.randomKey.format.count)
        }
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
    
    func showContactList() {
        presenter.didNextStep(action: .agenda(completion: didSelectNumberInAgenda))
    }
    
    private func didSelectNumberInAgenda(_ number: String) {
        guard dependencies.featureManager.isActive(.featurePixkeyPhoneWithDDI) else {
            presenter.presentSelectedNumber(number)
            return
        }
        let finalNumber = number.first == "+" ? number : brazilianPhonePrefix + number
        presenter.presentSelectedNumber(finalNumber)
    }
    
    private func handleAgenda(keyType: KeyType) {
        guard dependencies.featureManager.isActive(.isPixKeysAgendaAvailable) else {
            return
        }
        presenter.presentTextFieldRightView(keyType: keyType)
    }
    
    private func showCopiedKey() {
        if copiedKey.isNotEmpty {
            presenter.presentCopiedKey(key: copiedKey)
        }
    }
    
    private func handleKeyTypeIsCNPJ() {
        if selectedKeyType == .cnpj {
            selectedKeyType = .cpf
        }
    }
}
