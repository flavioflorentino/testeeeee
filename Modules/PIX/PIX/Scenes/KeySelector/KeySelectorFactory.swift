import Foundation
import UIKit

public enum KeySelectorFactory {
    public static func make(paymentOpening: PIXPaymentOpening, selectedKeyType: KeyType = .cpf, key: String = String()) -> UIViewController {
        let container = DependencyContainer()
        let service: KeySelectorServicing = KeySelectorService(dependencies: container)
        let coordinator: KeySelectorCoordinating = KeySelectorCoordinator(paymentOpening: paymentOpening)
        let presenter: KeySelectorPresenting = KeySelectorPresenter(coordinator: coordinator, dependencies: container)
        let interactor = KeySelectorInteractor(service: service, presenter: presenter, dependencies: container, selectedKeyType: selectedKeyType, copiedKey: key)
        let viewController = KeySelectorViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
