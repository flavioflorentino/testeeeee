import AssetsKit
import SnapKit
import UI

protocol ManualInsertionBizDisplay: AnyObject {
    func displayCpfTextMask(_ textMask: TextMask)
    func displayCnpjTextMask(_ textMask: TextMask)
    func displayTextField(isSelected: Bool, type: ManualInsertionBizContent)
    func displayCpfCnpjErrorMessage(message: String)
    func removeCpfCnpjErrorMessage()
    func toggleForwardButtonState()
    func displayInstitutionSelection(text: String)
    func displayAccountTypeSelection()
    func displayOwnershipSelection()
    func beginLoading()
    func endLoading()
    func displayDialog(with alertData: StatusAlertData)
    func displayError(title: String, subtitle: String, actions: [PopupAction])
    func displayCpfCnpjAndNicknameFields()
    func enableCpfCnpjAndNicknameFields(_ enable: Bool)
    func fillCpfCnpjNicknameField(cnpj: String?, nickname: String?)
}

enum ManualInsertionBizContent: Int {
    case agency = 1
    case account
    case cpfCnpj
    case nickname
}

enum OwnershipBiz: CaseIterable, PickerOption {
    case sameOwnership
    case otherOwnership
    
    var rowTitle: String? {
        switch self {
        case .sameOwnership:
            return Strings.ManualInsertionBiz.sameOwnership
        case .otherOwnership:
            return Strings.ManualInsertionBiz.otherOwnership
        }
    }
    
    var rowValue: String? {
        switch self {
        case .sameOwnership:
            return "PERSONAL"
        case .otherOwnership:
            return "OTHER_PERSONAL"
        }
    }
}

final class ManualInsertionBizViewController: ViewController<ManualInsertionBizInteracting, UIView> {
    typealias KeyboardInfo = (size: CGRect, animationDuration: TimeInterval, animationOptions: UIView.AnimationOptions)
    // MARK: - Visual Components
    private lazy var contentScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = Colors.backgroundPrimary.color
        scrollView.showsHorizontalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var closeBarButton: UIBarButtonItem = {
        let item = UIBarButtonItem(
            title: Strings.Receipt.Button.Title.close,
            style: .plain,
            target: self,
            action: #selector(didTapClose)
        )
        item.tintColor = Colors.branding400.color
        return item
    }()
    
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        stackView.distribution = .equalSpacing
        let rootEdges = EdgeInsets.rootView
        stackView.compatibleLayoutMargins = EdgeInsets(
            top: Spacing.base01,
            leading: rootEdges.leading,
            bottom: rootEdges.bottom,
            trailing: rootEdges.trailing
        )
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    private lazy var headerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        return stackView
    }()
    
    private lazy var cpfCnpjStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        stackView.isHidden = true
        return stackView
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale600.color)
        label.text = Strings.ManualInsertion.description
        return label
    }()
    
    private lazy var institutionTextField: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.delegate = self
        textfield.title = Strings.ManualInsertion.financialInstitution
        textfield.placeholder = Strings.ManualInsertion.financialInstitution
        textfield.rightViewMode = .always
        textfield.rightView = UIImageView(image: Assets.icoSearch.image)
        textfield.inputAccessoryView = doneToolbar
        return textfield
    }()
    
    private lazy var accountTypeTextField: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.delegate = self
        textfield.title = Strings.ManualInsertion.accountType
        textfield.placeholder = Strings.ManualInsertion.select
        textfield.setTitleVisible(true)
        textfield.rightViewMode = .always
        textfield.rightView = UIImageView(image: Assets.icoArrowDown.image)
        textfield.inputView = accountTypePicker
        textfield.inputAccessoryView = doneToolbar
        return textfield
    }()
    
    private lazy var agencyTextField: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.delegate = self
        textfield.title = Strings.ManualInsertion.agency
        textfield.placeholder = Strings.ManualInsertion.agency
        textfield.inputAccessoryView = doneToolbar
        textfield.keyboardType = .numberPad
        textfield.tag = ManualInsertionContent.agency.rawValue
        return textfield
    }()
    
    private lazy var bankAccountTextField: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.delegate = self
        textfield.title = Strings.ManualInsertion.accountWithDigit
        textfield.placeholder = Strings.ManualInsertion.accountWithDigit
        textfield.inputAccessoryView = doneToolbar
        textfield.keyboardType = .numberPad
        textfield.tag = ManualInsertionContent.account.rawValue
        return textfield
    }()
    
    private lazy var ownershipTextField: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.delegate = self
        textfield.title = Strings.ManualInsertion.ownership
        textfield.placeholder = Strings.ManualInsertion.select
        textfield.setTitleVisible(true)
        textfield.rightViewMode = .always
        textfield.rightView = UIImageView(image: Assets.icoArrowDown.image)
        textfield.inputView = ownershipTypePicker
        textfield.inputAccessoryView = doneToolbar
        return textfield
    }()
    
    private lazy var cpfOrCpnjTextField: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.delegate = self
        textfield.title = Strings.ManualInsertion.cpfOrCnpj
        textfield.placeholder = Strings.ManualInsertion.cpfOrCnpj
        textfield.inputAccessoryView = doneToolbar
        textfield.keyboardType = .numberPad
        textfield.tag = ManualInsertionContent.cpfCnpj.rawValue
        return textfield
    }()
    
    private lazy var cpfCnpjErrorLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, .critical900())
        label.isHidden = true
        return label
    }()
    
    private lazy var nicknameTextField: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.delegate = self
        textfield.title = Strings.ManualInsertion.name
        textfield.placeholder = Strings.ManualInsertion.name
        textfield.inputAccessoryView = doneToolbar
        textfield.tag = ManualInsertionContent.nickname.rawValue
        textfield.isHidden = true
        return textfield
    }()
    
    private lazy var forwardButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.General.forward, for: .normal)
        button.isEnabled = false
        button.addTarget(self, action: #selector(touchOnForward), for: .touchUpInside)
        return button
    }()
    
    private lazy var doneToolbar = DoneToolBar(doneText: Strings.General.ok)
    
    private lazy var accountTypePicker: PickerInputView = {
        let picker = PickerInputView(options: AccountType.allCases)
        picker.delegate = self
        return picker
    }()
    
    private lazy var ownershipTypePicker: PickerInputView = {
        let picker = PickerInputView(options: OwnershipBiz.allCases)
        picker.delegate = self
        return picker
    }()
    
    // MARK: - Variables
    private weak var focusedTextField: UITextField?
    private lazy var agencyMasker = TextFieldMasker(textMask: CharacterLimitMask(maximumNumberOfCharacters: 5))
    private lazy var bankAccountMasker = TextFieldMasker(textMask: BankAccountMask())
    private lazy var cpfOrCnpjMasker = TextFieldMasker(textMask: CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf))
    private lazy var nicknameMasker = TextFieldMasker(textMask: CharacterLimitMask(maximumNumberOfCharacters: 100))
    
    override func buildViewHierarchy() {
        view.addSubview(contentScrollView)
        contentScrollView.addSubview(contentStackView)
        
        contentStackView.addArrangedSubview(headerStackView)
        contentStackView.addArrangedSubview(forwardButton)
        
        headerStackView.addArrangedSubview(descriptionLabel)
        headerStackView.addArrangedSubview(institutionTextField)
        headerStackView.addArrangedSubview(accountTypeTextField)
        headerStackView.addArrangedSubview(agencyTextField)
        headerStackView.addArrangedSubview(bankAccountTextField)
        headerStackView.addArrangedSubview(ownershipTextField)
        headerStackView.addArrangedSubview(cpfCnpjStackView)
        headerStackView.addArrangedSubview(nicknameTextField)
        
        cpfCnpjStackView.addArrangedSubview(cpfOrCpnjTextField)
        cpfCnpjStackView.addArrangedSubview(cpfCnpjErrorLabel)
    }
    
    override func setupConstraints() {
        contentScrollView.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom)
            $0.leading.trailing.equalToSuperview()
        }
        
        contentStackView.snp.makeConstraints {
            $0.edges.width.equalToSuperview()
            $0.height.greaterThanOrEqualTo(view.layoutMarginsGuide)
        }
    }

    override func configureViews() {
        navigationItem.title = Strings.ManualInsertion.title
        view.backgroundColor = Colors.backgroundPrimary.color
        agencyMasker.bind(to: agencyTextField)
        bankAccountMasker.bind(to: bankAccountTextField)
        cpfOrCpnjTextField.addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
        cpfOrCnpjMasker.bind(to: cpfOrCpnjTextField)
        nicknameMasker.bind(to: nicknameTextField)
        agencyTextField.addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
        bankAccountTextField.addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
        nicknameTextField.addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
        accountTypePicker.inputInterfaceElement = accountTypeTextField
        ownershipTypePicker.inputInterfaceElement = ownershipTextField
        
        tapGesture()
        keyboardObservers()
    }
    
    func keyboardObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    func tapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    
    // MARK: - Actions
    @objc
    private func touchOnInstitutionSelection() {
        interactor.selectBank()
    }
    
    @objc
    private func textDidChanged(_ textField: UITextField) {
        guard let type = ManualInsertionBizContent(rawValue: textField.tag) else {
            return
        }
        interactor.verifyText(textField.text, type: type)
    }
    
    @objc
    private func touchOnForward() {
        interactor.selectForward()
    }
    
    @objc
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc
    func didTapClose() {
        dismiss(animated: true)
    }
    
    private func updateKeyboardConstraint(userInfo: [AnyHashable: Any], inset: CGFloat) {
        guard let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
            let animationCurve = (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue
            else {
                return
        }
        
        contentScrollView.snp.updateConstraints {
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).inset(inset)
        }
        
        UIView.animate(
            withDuration: animationDuration,
            delay: 0,
            options: UIView.AnimationOptions(rawValue: animationCurve),
            animations: {
                self.view.layoutIfNeeded()
            },
            completion: nil
        )
    }
    
    @objc
    func keyboardWillShow(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo,
            let keyboardSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            else {
                return
        }
        
        updateKeyboardConstraint(userInfo: userInfo, inset: keyboardSize.size.height)
    }
    
    @objc
    func keyboardWillHide(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        updateKeyboardConstraint(userInfo: userInfo, inset: 0.0)
    }
}

// MARK: - TextField
private extension ManualInsertionBizViewController {
    func textFieldBy(type: ManualInsertionBizContent) -> UIPPFloatingTextField {
        switch type {
        case .agency:
            return agencyTextField
        case .account:
            return bankAccountTextField
        case .cpfCnpj:
            return cpfOrCpnjTextField
        case .nickname:
            return nicknameTextField
        }
    }
    
    func removeCpfOrCnpjTextFieldBinds() {
        cpfOrCnpjMasker.unbind(from: cpfOrCpnjTextField)
        cpfOrCpnjTextField.removeTarget(self, action: #selector(textDidChanged), for: .editingChanged)
    }
    
    func changeCpfOrCnpjTextMask(_ textMask: TextMask) {
        cpfOrCnpjMasker.textMask = textMask
        cpfOrCpnjTextField.sendActions(for: .editingChanged)
    }
}

// MARK: ManualInsertionDisplay
extension ManualInsertionBizViewController: ManualInsertionBizDisplay {
    func displayCpfTextMask(_ textMask: TextMask) {
        removeCpfOrCnpjTextFieldBinds()
        cpfOrCpnjTextField.addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
        cpfOrCnpjMasker.bind(to: cpfOrCpnjTextField)
        changeCpfOrCnpjTextMask(textMask)
    }
    
    func displayCnpjTextMask(_ textMask: TextMask) {
        removeCpfOrCnpjTextFieldBinds()
        cpfOrCnpjMasker.bind(to: cpfOrCpnjTextField)
        cpfOrCpnjTextField.addTarget(self, action: #selector(textDidChanged), for: .editingChanged)
        changeCpfOrCnpjTextMask(textMask)
    }
    
    func displayTextField(isSelected: Bool, type: ManualInsertionBizContent) {
        textFieldBy(type: type).isSelected = isSelected
    }
    
    func displayCpfCnpjErrorMessage(message: String) {
        cpfCnpjErrorLabel.text = message
        cpfCnpjErrorLabel.isHidden = false
        cpfOrCpnjTextField.lineColor = Colors.critical900.color
        cpfOrCpnjTextField.selectedLineColor = Colors.critical900.color
    }
    
    func removeCpfCnpjErrorMessage() {
        cpfCnpjErrorLabel.text = nil
        cpfCnpjErrorLabel.isHidden = true
        cpfOrCpnjTextField.lineColor = Colors.branding400.color
        cpfOrCpnjTextField.selectedLineColor = Colors.branding400.color
    }
    
    func toggleForwardButtonState() {
        forwardButton.isEnabled.toggle()
    }
    
    func displayInstitutionSelection(text: String) {
        institutionTextField.text = text
        institutionTextField.isSelected = true
    }
    
    func displayAccountTypeSelection() {
        accountTypeTextField.isSelected = true
    }
    
    func displayOwnershipSelection() {
        ownershipTextField.isSelected = true
    }
    
    func beginLoading() {
        focusedTextField?.resignFirstResponder()
        beginState()
    }
    
    func endLoading() {
        endState()
    }
    
    func displayDialog(with alertData: StatusAlertData) {
        let alert = StatusAlertView.show(on: view, data: alertData)
        alert.statusAlertDelegate = self
    }

    func displayError(title: String, subtitle: String, actions: [PopupAction]) {
        let controller = PopupViewController(title: title,
                                             description: subtitle,
                                             preferredType: .text)
        actions.forEach(controller.addAction)
        showPopup(controller)
    }
    
    func displayCpfCnpjAndNicknameFields() {
        cpfCnpjStackView.isHidden = false
        nicknameTextField.isHidden = false
    }
    
    func enableCpfCnpjAndNicknameFields(_ enable: Bool) {
        cpfOrCpnjTextField.isEnabled = enable
        nicknameTextField.isEnabled = enable
    }
    
    func fillCpfCnpjNicknameField(cnpj: String?, nickname: String?) {
        cpfOrCpnjTextField.text = cnpj
        nicknameTextField.text = nickname
    }
}

// MARK: - UITextFieldDelegate
extension ManualInsertionBizViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        guard textField !== institutionTextField else {
            touchOnInstitutionSelection()
            return false
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        focusedTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        focusedTextField = nil
    }
}

// MARK: - PickerInputViewDelegate
extension ManualInsertionBizViewController: PickerInputViewDelegate {
    func didSelectOption(_ option: PickerOption?, on pickerInputView: PickerInputView) {
        interactor.selectPickerOption(option)
    }
}

// MARK: StatefulProviding
extension ManualInsertionBizViewController: StatefulProviding {}

// MARK: StatusAlertViewDelegate
extension ManualInsertionBizViewController: StatusAlertViewDelegate {}
