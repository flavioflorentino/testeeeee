import UIKit

typealias ManualInsertionBizOutput = (ManualInsertionBizAction) -> Void

enum ManualInsertionBizFactory {
    static func make(
        manualInsertionBizOutput: @escaping ManualInsertionBizOutput,
        userInfo: KeyManagerBizUserInfo?
    ) -> ManualInsertionBizViewController {
        let coordinator: ManualInsertionBizCoordinating = ManualInsertionBizCoordinator(
            manualInsertionBizOutput: manualInsertionBizOutput
        )
        let presenter: ManualInsertionBizPresenting = ManualInsertionBizPresenter(coordinator: coordinator)
        let interactor = ManualInsertionBizInteractor(presenter: presenter, userInfo: userInfo)
        let viewController = ManualInsertionBizViewController(interactor: interactor)

        presenter.viewController = viewController

        return viewController
    }
}
