import Core

enum BankListServiceEndpoint: ApiEndpointExposable {
    case fetch

    var path: String {
        "/list-banks"
    }
}
