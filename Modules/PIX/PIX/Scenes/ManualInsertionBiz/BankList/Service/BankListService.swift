import Core

protocol BankListServicing {
    func fetchBanks(completion: @escaping (Result<BankListResponseModel, ApiError>) -> Void)
}

final class BankListService: BankListServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func fetchBanks(completion: @escaping (Result<BankListResponseModel, ApiError>) -> Void) {
        let endpoint = BankListServiceEndpoint.fetch
        Api<BankListResponseModel>(endpoint: endpoint).execute(jsonDecoder: JSONDecoder()) { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                completion(mappedResult)
            }
        }
    }
}
