struct BankListResponseModel: Decodable {
    let list: [BankItemResponseModel]
    
    private enum CodingKeys: String, CodingKey {
        case list, data
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let dataContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        
        list = try dataContainer.decode([BankItemResponseModel].self, forKey: .list)
    }
}

struct BankItemResponseModel: Decodable {
    let name: String?
    let imageUrl: String?
    let code: String?
    
    private enum CodingKeys: String, CodingKey {
        case name
        case imageUrl = "img_url"
        case code = "id"
    }
}

struct BankListItemModel {
    let logoURL: URL?
    let name: String
    
    init?(item: BankItemResponseModel) {
        guard let name = item.name, let code = item.code else {
            return nil
        }
        
        self.logoURL = URL(string: item.imageUrl ?? "")
        self.name = "\(code) - \(name)"
    }
}
