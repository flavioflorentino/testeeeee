import Foundation

protocol BankListInteracting: AnyObject {
    func fetchBanks()
    func filterBanks(query: String?)
    func selectBank(at indexPath: IndexPath)
    func dismiss()
}

final class BankListInteractor {
    private let service: BankListServicing
    private let presenter: BankListPresenting
    
    private var banks: [BankItemResponseModel] = []
    private var filteredBanks: [BankItemResponseModel] = []

    init(service: BankListServicing, presenter: BankListPresenting) {
        self.service = service
        self.presenter = presenter
    }
    
    private func bankMatchesQuery(_ bank: BankItemResponseModel, query: String) -> Bool {
        guard let name = bank.name, let id = bank.code else {
            return false
        }
        return "\(id)\(name)".localizedStandardContains(query)
    }
}

extension BankListInteractor: BankListInteracting {
    func fetchBanks() {
        presenter.startLoading()
        service.fetchBanks { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success(let banks):
                self?.banks = banks.list
                self?.presenter.presentBanks(banks.list)
            case .failure(let error):
                self?.presenter.present(error: error)
            }
        }
    }
    
    func filterBanks(query: String?) {
        guard let query = query, !query.isEmpty else {
            filteredBanks = []
            presenter.presentBanks(banks)
            return
        }
        let filteredBanks = banks.filter { bank -> Bool in
            bankMatchesQuery(bank, query: query)
        }
        presenter.presentBanks(filteredBanks)
        self.filteredBanks = filteredBanks
    }
    
    func selectBank(at indexPath: IndexPath) {
        let source = filteredBanks.isEmpty ? banks : filteredBanks
        guard source.indices.contains(indexPath.section) else { return }
        let bank = source[indexPath.row]
        presenter.didSelect(bank: bank)
    }
    
    func dismiss() {
        presenter.dismiss()
    }
}
