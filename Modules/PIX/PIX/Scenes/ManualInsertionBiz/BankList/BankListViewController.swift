import UI
import UIKit

protocol BankListDisplay: AnyObject {
    func display(banks: Section<String, BankListItemModel>)
    func displayPopupError(title: String, message: String, linkTitle: String, primaryTitle: String?)
    func startLoading()
    func stopLoading()
}

extension BankListDisplay {
    func displayPopupError(title: String, message: String, linkTitle: String, primaryTitle: String? = nil) {
        displayPopupError(title: title, message: message, linkTitle: linkTitle, primaryTitle: primaryTitle)
    }
}

final class BankListViewController: ViewController<BankListInteracting, UIView> {
    enum AccessibilityIdentifier: String {
        case banksTableView
        case banksSearchBar
        case errorView
        case emptySearchResultsView
    }
    // MARK: - Layout components
    private(set) lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.searchBar.barTintColor = Colors.grayscale050.lightColor
        searchController.searchBar.tintColor = Colors.branding300.color
        searchController.searchBar.isTranslucent = false
        searchController.searchBar.backgroundImage = UIImage()
        searchController.searchBar.placeholder = Strings.ManualInsertion.search
        searchController.searchBar.accessibilityIdentifier = AccessibilityIdentifier.banksSearchBar.rawValue
        return searchController
    }()
    
    private(set) lazy var activityIndicatorView = UIActivityIndicatorView(style: .gray)
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.register(BankListTableViewCell.self, forCellReuseIdentifier: BankListTableViewCell.identifier)
        tableView.accessibilityIdentifier = AccessibilityIdentifier.banksTableView.rawValue
        tableView.tableFooterView = UIView()
        tableView.layoutMargins = .zero
        tableView.separatorInset = .zero
        return tableView
    }()
    
    // MARK: - Instance variables
    private var tableViewHandler: TableViewHandler<String, BankListItemModel, BankListTableViewCell>?
    private lazy var keyboardScrollViewHandler = KeyboardScrollViewHandler(scrollView: tableView)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.fetchBanks()
        keyboardScrollViewHandler.registerForKeyboardNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clearsTableViewSelection(animated: animated)
    }
    
    private func setupSearchPosition() {
        definesPresentationContext = true
        if #available(iOS 11, *) {
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            tableView.tableHeaderView = searchController.searchBar
        }
    }
 
    override func buildViewHierarchy() {
        view.addSubview(tableView)
        setupSearchPosition()
    }
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    override func configureViews() {
        title = Strings.ManualInsertion.institution
        let searchFieldColor = searchController.searchBar.value(forKey: "searchField") as? UITextField
        searchFieldColor?.textColor = Colors.grayscale750.lightColor
    }
    
    private func clearsTableViewSelection(animated: Bool) {
        guard let indexPathForSelectedRow = tableView.indexPathForSelectedRow else {
            return
        }
        tableView.deselectRow(at: indexPathForSelectedRow, animated: animated)
    }
}

// MARK: - Search results updating
extension BankListViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        interactor.filterBanks(query: searchController.searchBar.text)
    }
}

// MARK: - View Model Outputs
extension BankListViewController: BankListDisplay {
    func display(banks: Section<String, BankListItemModel>) {
        tableViewHandler = TableViewHandler(
            data: [banks],
            cellType: BankListTableViewCell.self,
            configureCell: { _, bank, cell in
                cell.setup(for: bank)
            },
            configureDidSelectRow: { indexPath, _ in
                self.interactor.selectBank(at: indexPath)
            }
        )
        tableView.dataSource = tableViewHandler
        tableView.delegate = tableViewHandler
        tableView.backgroundView = nil
        tableView.reloadData()
    }
    
    func displayPopupError(title: String, message: String, linkTitle: String, primaryTitle: String?) {
        let popupViewController = PopupViewController(title: title,
                                                      description: message,
                                                      preferredType: .business)
        var shouldDismissViewController = false
        
        if let primaryTitle = primaryTitle {
            let okAction = PopupAction(title: primaryTitle, style: .tryAgain) { [weak self] in
                self?.interactor.fetchBanks()
            }
            popupViewController.addAction(okAction)
            shouldDismissViewController = true
        }
        
        let cancelAction = PopupAction(title: linkTitle, style: .link) { [weak self] in
            guard shouldDismissViewController else { return }
            self?.interactor.dismiss()
        }
        
        popupViewController.addAction(cancelAction)
        
        showPopup(popupViewController)
    }
    
    func startLoading() {
        tableView.backgroundView = activityIndicatorView
        activityIndicatorView.startAnimating()
    }
    
    func stopLoading() {
        activityIndicatorView.stopAnimating()
    }
}
