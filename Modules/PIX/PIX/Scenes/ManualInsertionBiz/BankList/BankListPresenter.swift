import Core
import UI
import UIKit

protocol BankListPresenting: AnyObject {
    var viewController: BankListDisplay? { get set }
    func presentBanks(_ banks: [BankItemResponseModel])
    func present(error: ApiError)
    func startLoading()
    func stopLoading()
    func didSelect(bank: BankItemResponseModel)
    func dismiss()
}

final class BankListPresenter: BankListPresenting {
    private let coordinator: BankListCoordinating
    weak var viewController: BankListDisplay?
    private let completion: BankListSelection

    init(coordinator: BankListCoordinating, completion: @escaping BankListSelection) {
        self.coordinator = coordinator
        self.completion = completion
    }
    
    func presentBanks(_ banks: [BankItemResponseModel]) {
        let banksSection = section(for: banks)
        viewController?.display(banks: banksSection)
    }
    
    func present(error: ApiError) {
        viewController?.displayPopupError(title: error.requestError?.title ?? Strings.General.somethingWentWrong,
                                          message: error.requestError?.message ?? Strings.General.failedComms,
                                          linkTitle: Strings.General.back,
                                          primaryTitle: Strings.General.tryAgain)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func didSelect(bank: BankItemResponseModel) {
        guard let code = bank.code, let name = bank.name else {
            viewController?.displayPopupError(title: Strings.General.somethingWentWrong,
                                              message: Strings.General.failedComms,
                                              linkTitle: Strings.General.back)
            return
        }
        
        completion(code, name)
        coordinator.dismiss()
    }
    
    func dismiss() {
        coordinator.dismiss()
    }
    
    private func section(for banks: [BankItemResponseModel]) -> Section<String, BankListItemModel> {
        let bankItems = banks.compactMap(BankListItemModel.init(item:))
        return Section(items: bankItems)
    }
}
