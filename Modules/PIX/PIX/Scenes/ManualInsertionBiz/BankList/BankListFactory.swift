import UIKit

typealias BankListSelection = (String, String) -> Void

enum BankListFactory {
    static func make(output: @escaping BankListOutput, completion: @escaping BankListSelection) -> BankListViewController {
        let container = DependencyContainer()
        let coordinator: BankListCoordinating = BankListCoordinator(output: output)
        let presenter: BankListPresenting = BankListPresenter(coordinator: coordinator, completion: completion)
        let service: BankListServicing = BankListService(dependencies: container)
        let interactor: BankListInteracting = BankListInteractor(service: service, presenter: presenter)
        let viewController = BankListViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
