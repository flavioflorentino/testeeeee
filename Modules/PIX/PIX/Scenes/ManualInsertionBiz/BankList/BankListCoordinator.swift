import UIKit

typealias BankListOutput = (BankListAction) -> Void

enum BankListAction {
    case dismiss
}

protocol BankListCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func dismiss()
}

final class BankListCoordinator {
    weak var viewController: UIViewController?
    private let output: BankListOutput
    
    init(output: @escaping BankListOutput) {
        self.output = output
    }
}

// MARK: - ManualInsertionCoordinating
extension BankListCoordinator: BankListCoordinating {
    func dismiss() {
        output(.dismiss)
    }
}
