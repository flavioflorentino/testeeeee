import AssetsKit
import UI
import UIKit

final class BankListTableViewCell: UITableViewCell {    
    static let identifier = String(describing: BankListTableViewCell.self)
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, Colors.grayscale750.color)
        return label
    }()
    
    private lazy var logoImageView = UIImageView()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [logoImageView, nameLabel])
        stackView.spacing = Spacing.base01
        stackView.axis = .horizontal
        stackView.alignment = .center
        return stackView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        buildLayout()
    }
    
    func setup(for bank: BankListItemModel) {
        logoImageView.setImage(url: bank.logoURL, placeholder: Resources.Placeholders.bank.image)
        nameLabel.text = bank.name
    }
}

extension BankListTableViewCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.bottom.equalToSuperview().inset(Spacing.base01)
        }
        
        logoImageView.snp.makeConstraints {
            $0.size.equalTo(Sizing.base05)
        }
    }
}
