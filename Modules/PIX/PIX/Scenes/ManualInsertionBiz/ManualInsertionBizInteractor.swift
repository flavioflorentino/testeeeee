import Core
import Foundation
import UI
import Validations

protocol ManualInsertionBizInteracting: AnyObject {
    func selectPickerOption(_ option: PickerOption?)
    func verifyText(_ text: String?, type: ManualInsertionBizContent)
    func selectBank()
    func selectForward()
}

private enum ContentType: Int, CaseIterable {
    case institution
    case accountType
    case agency
    case account
    case ownership
    case cpfCnpj
    case nickname
}

final class ManualInsertionBizInteractor {
    // MARK: - Variables
    private let presenter: ManualInsertionBizPresenting
    private let userInfo: KeyManagerBizUserInfo?
    private let agencyValidator = RangeValidator(range: 1...5)
    private let accountValidator = RangeValidator(range: 3...15)
    private let cpfValidator = CPFValidator()
    private let cnpjValidator = CNPJValidator()
    private let nicknameValidator = RangeValidator(range: 1...100)
    private let keyType = "MANUAL"
    private var isFowardButtonEnabled = false
    private var isCurrentMaskCPF = true
    private var selectedAccountType: AccountType?
    private var selectedOwnershipType: OwnershipBiz?
    private var contentValidity: [Bool] = { ContentType.allCases.map { _ in false } }()
    private var bankId: String?
    private var bankName: String?
    private var agency: String?
    private var accountNumber: String?
    private var cpfCnpj: String?
    private var nickname: String?
    private var isAllFieldsValid: Bool {
        checkAllFieldsValid()
    }

    init(presenter: ManualInsertionBizPresenting, userInfo: KeyManagerBizUserInfo?) {
        self.presenter = presenter
        self.userInfo = userInfo
    }
}

// MARK: - ManualInsertionInteracting
extension ManualInsertionBizInteractor: ManualInsertionBizInteracting {
    func selectPickerOption(_ option: PickerOption?) {
        if let type = option as? AccountType {
            selectedAccountType = type
            contentValidity[ContentType.accountType.rawValue] = true
            presenter.presentAccountTypeSelection()
            handleFowardButtonState()
        } else if let type = option as? OwnershipBiz {
            handleOwnershipSelection(type: type)
        }
    }
    
    private func handleOwnershipSelection(type: OwnershipBiz) {
        selectedOwnershipType = type
        contentValidity[ContentType.ownership.rawValue] = true
        presenter.presentOwnershipSelection()
        
        if type == .sameOwnership {
            contentValidity[ContentType.cpfCnpj.rawValue] = true
            contentValidity[ContentType.nickname.rawValue] = true
            handleFowardButtonState()
            presenter.presentCpfCnpjAndNicknameFieldsFilled(userInfo: userInfo)
        } else {
            contentValidity[ContentType.cpfCnpj.rawValue] = false
            contentValidity[ContentType.nickname.rawValue] = false
            handleFowardButtonState()
            presenter.presentCpfCnpjAndNicknameFields()
        }
    }
    
    func verifyText(_ text: String?, type: ManualInsertionBizContent) {
        guard let text = text else {
            return
        }
        do {
            try handleTextValidation(text: text, type: type)
        } catch GenericValidationError.incompleteData {
            handleTextIncomplete(text, type: type)
        } catch {
            handleTextInvalid(text: text, type: type)
        }
        handleFowardButtonState()
    }
    
    private func handleTextValidation(text: String, type: ManualInsertionBizContent) throws {
        switch type {
        case .agency:
            agency = text
            try agencyValidator.validate(text)
        case .account:
            accountNumber = text
            try accountValidator.validate(text)
        case .cpfCnpj:
            try handleCpfAndCnpjValidation(text: text)
            presenter.removeCpfCnpjErrorMessage()
        case .nickname:
            nickname = text
            try nicknameValidator.validate(text)
        }
        validateType(isValid: true, type: type)
        presenter.presentTextFieldValidState(type: type)
    }
    
    private func handleCpfAndCnpjValidation(text: String) throws {
        cpfCnpj = text
        if text.count <= PersonalDocumentMaskDescriptor.cpf.format.count {
            if !isCurrentMaskCPF {
                isCurrentMaskCPF.toggle()
                presenter.presentCpfMasker()
            }
            try cpfValidator.validate(text)
        } else {
            if isCurrentMaskCPF {
                isCurrentMaskCPF.toggle()
                presenter.presentCNPJMasker()
            }
            try cnpjValidator.validate(text)
        }
    }
    
    private func handleTextIncomplete(_ text: String?, type: ManualInsertionBizContent) {
        validateType(isValid: false, type: type)
        
        guard
            type == .cpfCnpj,
            let text = text,
            let ownershipType = selectedOwnershipType,
            ownershipType == .otherOwnership,
            !text.isEmpty,
            text.count != PersonalDocumentMaskDescriptor.cpf.format.count,
            text.count != PersonalDocumentMaskDescriptor.cnpj.format.count
        else {
            return
        }
        
        if text.count < PersonalDocumentMaskDescriptor.cpf.format.count {
            presenter.presentCpfErrorMessage()
        } else {
            presenter.presentCnpjErrorMessage()
        }
    }
    
    private func handleTextInvalid(text: String, type: ManualInsertionBizContent) {
        validateType(isValid: false, type: type)
        presenter.presentTextFieldNormalState(type: type)
        
        guard type == .cpfCnpj else {
            return
        }
        
        if text.count <= PersonalDocumentMaskDescriptor.cpf.format.count {
            presenter.presentCpfErrorMessage()
        } else {
            presenter.presentCnpjErrorMessage()
        }
    }
    
    private func validateType(isValid: Bool, type: ManualInsertionBizContent) {
        switch type {
        case .agency:
            contentValidity[ContentType.agency.rawValue] = isValid
        case .account:
            contentValidity[ContentType.account.rawValue] = isValid
        case .cpfCnpj:
            contentValidity[ContentType.cpfCnpj.rawValue] = isValid
        case .nickname:
            contentValidity[ContentType.nickname.rawValue] = isValid
        }
    }
    
    private func handleFowardButtonState() {
        guard isAllFieldsValid != isFowardButtonEnabled else {
            return
        }
        isFowardButtonEnabled.toggle()
        presenter.toggleForwardButtonState()
    }
    
    func selectBank() {
        let completion: (String, String) -> Void = { [weak self] wsId, name in
            self?.bankId = wsId
            self?.bankName = name
            self?.presenter.presentInstitutionSelection(id: wsId, name: name)
            self?.contentValidity[ContentType.institution.rawValue] = true
            self?.handleFowardButtonState()
        }
        presenter.didNextStep(action: .bankSelection(completion: completion))
    }
    
    func selectForward() {
        guard
            let params = createRequestParams(),
            isAllFieldsValid
            else {
                return
        }
        presenter.didNextStep(action: .nextStep(params: params))
    }
    
    private func createRequestParams() -> PIXParams? {
        guard
            let bankId = bankId,
            let bankName = bankName,
            let accountType = selectedAccountType,
            let accountTypeString = accountType.rowValue,
            let agency = agency,
            let accountNumber = accountNumber,
            let ownershipType = selectedOwnershipType,
            let ownership = ownershipType.rowValue
        else {
            return nil
        }
        var cpfCnpj: String?
        
        if ownershipType == .otherOwnership {
            cpfCnpj = self.cpfCnpj?.filter { $0.unicodeScalars.allSatisfy(CharacterSet.decimalDigits.contains) }
        } else {
            cpfCnpj = userInfo?.cnpj
            nickname = userInfo?.name
        }
        
        let receiverData = PIXReceiverData(
            name: nickname,
            bankId: bankId,
            bankName: bankName,
            accountType: accountTypeString,
            agency: agency,
            accountNumber: accountNumber,
            cpfCnpj: cpfCnpj,
            ownership: ownership
        )
        return PIXParams(originType: .manual, keyType: keyType, key: nil, receiverData: receiverData)
    }
    
    private func handleFailure(error: ApiError) {
        guard
            let alert = error.requestError?.alert,
            let title = alert.title?.string,
            let subtitle = alert.text?.string
            else {
                presenter.presentGenericError()
            return
        }
        presenter.presentError(
            title: title,
            subtitle: subtitle,
            jsonButtons: alert.buttonsJson
        )
    }
    
    private func checkAllFieldsValid() -> Bool {
        guard let selectedOwnership = selectedOwnershipType, selectedOwnership == .sameOwnership else {
            return contentValidity.first(where: { $0 == false }) ?? true
        }
        let validation = contentValidity.enumerated().first(where: {
            if $0.offset == ContentType.cpfCnpj.rawValue || $0.offset == ContentType.nickname.rawValue {
                return false
            }
            return $0.element == false
        })
        return validation?.element ?? true
    }
}
