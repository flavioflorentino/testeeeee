import AssetsKit
import Foundation
import UI
import UIKit

protocol ManualInsertionBizPresenting: AnyObject {
    var viewController: ManualInsertionBizDisplay? { get set }
    func didNextStep(action: ManualInsertionBizAction)
    func presentCpfMasker()
    func presentCNPJMasker()
    func presentTextFieldValidState(type: ManualInsertionBizContent)
    func presentTextFieldNormalState(type: ManualInsertionBizContent)
    func removeCpfCnpjErrorMessage()
    func presentCpfErrorMessage()
    func presentCnpjErrorMessage()
    func toggleForwardButtonState()
    func presentInstitutionSelection(id: String, name: String)
    func presentAccountTypeSelection()
    func presentOwnershipSelection()
    func presentGenericError()
    func presentError(title: String, subtitle: String, jsonButtons: [[String: Any]])
    func presentCpfCnpjAndNicknameFields()
    func presentCpfCnpjAndNicknameFieldsFilled(userInfo: KeyManagerBizUserInfo?)
}

final class ManualInsertionBizPresenter {
    private let coordinator: ManualInsertionBizCoordinating
    weak var viewController: ManualInsertionBizDisplay?
    
    private let cnpjSize = 14

    init(coordinator: ManualInsertionBizCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ManualInsertionPresenting
extension ManualInsertionBizPresenter: ManualInsertionBizPresenting {
    func didNextStep(action: ManualInsertionBizAction) {
        coordinator.perform(action: action)
    }
    
    func presentCpfMasker() {
        let textMask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf)
        viewController?.displayCpfTextMask(textMask)
    }
    
    func presentCNPJMasker() {
        let textMask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cnpj)
        viewController?.displayCnpjTextMask(textMask)
    }
    
    func presentTextFieldValidState(type: ManualInsertionBizContent) {
        viewController?.displayTextField(isSelected: true, type: type)
    }
    
    func presentTextFieldNormalState(type: ManualInsertionBizContent) {
        viewController?.displayTextField(isSelected: false, type: type)
    }
    
    func removeCpfCnpjErrorMessage() {
        viewController?.removeCpfCnpjErrorMessage()
    }
    
    func presentCpfErrorMessage() {
        let message = Strings.ManualInsertion.invalidFormat
        viewController?.displayCpfCnpjErrorMessage(message: message)
    }
    
    func presentCnpjErrorMessage() {
        let message = Strings.ManualInsertion.invalidFormat
        viewController?.displayCpfCnpjErrorMessage(message: message)
    }
    
    func toggleForwardButtonState() {
        viewController?.toggleForwardButtonState()
    }
    
    func presentInstitutionSelection(id: String, name: String) {
        let text = "\(id) - \(name)"
        viewController?.displayInstitutionSelection(text: text)
    }
    
    func presentAccountTypeSelection() {
        viewController?.displayAccountTypeSelection()
    }
    
    func presentOwnershipSelection() {
        viewController?.displayOwnershipSelection()
    }
    
    func presentGenericError() {
        let alertData = StatusAlertData(
            icon: Resources.Illustrations.iluFalling.image,
            title: Strings.General.somethingWentWrong,
            text: Strings.General.requestNotCompleted,
            buttonTitle: Strings.General.tryAgain
        )
        viewController?.displayDialog(with: alertData)
        viewController?.endLoading()
    }
    
    func presentError(title: String, subtitle: String, jsonButtons: [[String: Any]]) {
        let actions = jsonButtons.compactMap(createPopupAction)
        viewController?.displayError(title: title, subtitle: subtitle, actions: actions)
        viewController?.endLoading()
    }
    
    private func createPopupAction(jsonButton: [String: Any]) -> PopupAction? {
        guard let title = jsonButton["title"] as? String else {
            return nil
        }
        return PopupAction(title: title, style: .fill)
    }
    
    func presentCpfCnpjAndNicknameFields() {
        viewController?.enableCpfCnpjAndNicknameFields(true)
        viewController?.displayCpfCnpjAndNicknameFields()
        viewController?.fillCpfCnpjNicknameField(cnpj: "", nickname: "")
    }
    
    func presentCpfCnpjAndNicknameFieldsFilled(userInfo: KeyManagerBizUserInfo?) {
        viewController?.enableCpfCnpjAndNicknameFields(false)
        viewController?.displayCpfCnpjAndNicknameFields()
        viewController?.fillCpfCnpjNicknameField(cnpj: formatDocument(userInfo?.cnpj), nickname: userInfo?.name)
    }
    
    private func formatDocument(_ text: String?) -> String? {
        guard let document = text else {
            return text
        }
        let mask = CustomStringMask(descriptor: document.count >= cnpjSize ?
                                        PersonalDocumentMaskDescriptor.cnpj :
                                        PersonalDocumentMaskDescriptor.cpf)
        return mask.maskedText(from: document) ?? ""
    }
}
