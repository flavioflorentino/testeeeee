import UIKit

enum ManualInsertionBizAction {
    case bankSelection(completion: (String, String) -> Void)
    case nextStep(params: PIXParams)
}

protocol ManualInsertionBizCoordinating: AnyObject {
    func perform(action: ManualInsertionBizAction)
}

final class ManualInsertionBizCoordinator {
    private let manualInsertionBizOutput: ManualInsertionBizOutput
    
    init(manualInsertionBizOutput: @escaping ManualInsertionBizOutput) {
        self.manualInsertionBizOutput = manualInsertionBizOutput
    }
}

// MARK: - ManualInsertionCoordinating
extension ManualInsertionBizCoordinator: ManualInsertionBizCoordinating {
    func perform(action: ManualInsertionBizAction) {
        switch action {
        case let .bankSelection(completion):
            manualInsertionBizOutput(.bankSelection(completion: completion))
        case let .nextStep(params):
            manualInsertionBizOutput(.nextStep(params: params))
        }
    }
}
