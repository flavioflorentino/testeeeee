import Foundation
import UI
import UIKit

struct AnyButtonStyle<Target>: ButtonStyle {
    private let makeStyleClosure: (StyleCore<Target>) -> Void
    
    init<S: ButtonStyle>(style: S) where S.View == Target {
        makeStyleClosure = style.makeStyle
    }
     
    func makeStyle(_ style: StyleCore<Target>) {
        makeStyleClosure(style)
    }
}
