import AnalyticsModule
import CustomerSupport
import Core
import FeatureFlag

typealias PIXDependencies =
    HasNoDependency &
    HasAnalytics &
    HasMainQueue &
    HasKVStore &
    HasFeatureManager &
    HasBizAuth &
    HasUserAuthManager &
    HasCustomerSupport &
    HasZendesk &
    HasPFAuth

final class DependencyContainer: PIXDependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var kvStore: KVStoreContract = KVStore()
    lazy var bizFeatureManager: BizFeatureManagerContract? = PIXSetup.bizFeatureManager
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var userAuthManagerContract: UserAuthManagerContract? = PIXSetup.userAuthManager
    lazy var businessAuthContract: BizAuthManagerContract? = PIXSetup.businessAuthManagerInstance
    lazy var costumeSupportContract: CostumeSupportContract? = PIXSetup.costumeSupportContract
    lazy var zendeskContainer: ThirdPartySupportSDKContract? = PIXSetup.zendeskContainer
    lazy var authContract: AuthenticationContract? = PIXSetup.authInstance
}
