import Core
import FeatureFlag
import UI

public struct PIXDeeplinkResolver: DeeplinkResolver {
    public typealias Dependencies = HasFeatureManager
    
    private let dependencies: Dependencies
    private let paymentOpener: PIXPaymentOpening
    private let bankSelectorType: BankSelector.Type
    private let qrCodeScannerType: QrCodeScanning.Type
    private let receiptType: TransactionReceipting.Type
    
    public init(
        dependencies: Dependencies,
        paymentOpener: PIXPaymentOpening,
        bankSelectorType: BankSelector.Type,
        qrCodeScannerType: QrCodeScanning.Type,
        receiptType: TransactionReceipting.Type
    ) {
        self.dependencies = dependencies
        self.paymentOpener = paymentOpener
        self.bankSelectorType = bankSelectorType
        self.qrCodeScannerType = qrCodeScannerType
        self.receiptType = receiptType
    }
    
    public func canHandle(url: URL, isAuthenticated: Bool) -> DeeplinkResolverResult {
        guard let pixDeeplinkType = PIXDeeplinkType(url: url) else {
            return .notHandleable
        }
        
        if pixDeeplinkType.needAuthentication && !isAuthenticated {
            return .onlyWithAuth
        }
        
        return .handleable
    }
    
    public func open(url: URL, isAuthenticated: Bool) -> Bool {
        guard
            let deeplinkType = PIXDeeplinkType(url: url),
            canHandle(url: url, isAuthenticated: isAuthenticated) == .handleable
            else {
                return false
        }
        
        return PIXDeeplinkHelper(
            dependencies: dependencies,
            deeplinkType: deeplinkType,
            paymentOpener: paymentOpener,
            bankSelectorType: bankSelectorType,
            qrCodeScannerType: qrCodeScannerType,
            receiptType: receiptType
        ).handleDeeplink()
    }
}
