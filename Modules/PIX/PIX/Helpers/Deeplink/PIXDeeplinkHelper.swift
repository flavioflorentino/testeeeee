import Core
import FeatureFlag
import UIKit
import UI

final class PIXDeeplinkHelper {
    typealias Dependencies = HasFeatureManager
    
    private let dependencies: Dependencies
    private let deeplinkType: PIXDeeplinkType
    private let paymentOpener: PIXPaymentOpening
    private let bankSelectorType: BankSelector.Type
    private let qrCodeScannerType: QrCodeScanning.Type
    private let receiptType: TransactionReceipting.Type
    
    init(
        dependencies: Dependencies,
        deeplinkType: PIXDeeplinkType,
        paymentOpener: PIXPaymentOpening,
        bankSelectorType: BankSelector.Type,
        qrCodeScannerType: QrCodeScanning.Type,
        receiptType: TransactionReceipting.Type
    ) {
        self.dependencies = dependencies
        self.deeplinkType = deeplinkType
        self.paymentOpener = paymentOpener
        self.bankSelectorType = bankSelectorType
        self.qrCodeScannerType = qrCodeScannerType
        self.receiptType = receiptType
    }
  
    func handleDeeplink() -> Bool {
        guard let navigationController = PIXSetup.navigationInstance?.getCurrentNavigation() else {
            return false
        }
        
        let pixDestination: PIXConsumerFlow
        var needNavigationInModalPresentation = true
        
        switch deeplinkType {
        case .preRegistration:
            pixDestination = .preRegistration
        case .keyManagement:
            pixDestination = .keyManagement
        case .removalCompleted:
            pixDestination = .removalCompleted
        case .hub:
            pixDestination = .hub(origin: "Deeplink", paymentOpening: paymentOpener, bankSelectorType: bankSelectorType, qrCodeScannerType: qrCodeScannerType)
        case .keySelector:
            pixDestination = .keySelector(paymentOpening: paymentOpener)
        case .copyPaste:
            pixDestination = .copyPaste(paymentOpening: paymentOpener)
        case .qrCode:
            pixDestination = .qrCode(qrCodeScannerType: qrCodeScannerType)
        case .manualInsertion:
            pixDestination = .manualInsertion(paymentOpening: paymentOpener, bankSelectorType: bankSelectorType)
        case .receivement:
            pixDestination = .receivement(origin: .deeplink)
        case .dailyLimit:
            guard dependencies.featureManager.isActive(.isPixDailyLimitAvailable) else {
                return false
            }
            pixDestination = .dailyLimit
        case let .receipt(id, type):
            pixDestination = .receipt(receiptType: receiptType, id:id, type: type)
            needNavigationInModalPresentation = false
        case .feedback(let type):
            pixDestination = .feedback(type: type)
        }
        
        PIXConsumerFlowCoordinator(
            originViewController: navigationController,
            originFlow: .deeplink,
            destination: pixDestination
        ).start(
            needNavigationInModalPresentation: needNavigationInModalPresentation
        )
        return true
    }
}
