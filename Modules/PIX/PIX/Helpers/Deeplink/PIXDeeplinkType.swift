import Foundation

enum PIXDeeplinkType {
    static let PIX_PATH = "pix"
    
    case hub
    case keySelector
    case copyPaste
    case qrCode
    case manualInsertion
    case receivement
    case keyManagement
    case dailyLimit
    case receipt(id: String, type: String)
    case preRegistration
    case removalCompleted
    case feedback(feedbackType: FeedbackViewType)
    
    var needAuthentication: Bool {
        true
    }
    
    init?(url: URL) {
        guard PIXDeeplinkType.PIX_PATH == url.pathComponents.first(where: { $0 != "/" }) else {
            return nil
        }
        
        let params = url.pathComponents.filter { $0 != "/" && $0 != PIXDeeplinkType.PIX_PATH }
        
        guard let path = params.first else {
            return nil
        }
        
        switch path {
        case "feedback":
            if params.contains("removalCompleted") {
                self = .removalCompleted
                return
            }
            
            if params.contains("claimKeyAccepted") {
                let uuid = url.queryParameters["uuid"] as? String ?? ""
                self = .feedback(feedbackType: .claimNewValidationKeyAvailable(uuid: uuid))
                return
            }
            
            if params.contains("validationNotCompleted") {
                self = .feedback(feedbackType: .claimNewValidationDeadlineFinished)
                return
            }
            
            guard let feedbackType = FeedbackViewType.type(with: url) else {
                return nil
            }
            
            self = .feedback(feedbackType: feedbackType)
        case "hub":
            self = .hub
        case "keySelector":
            self = .keySelector
        case "copyPaste":
            self = .copyPaste
        case "qrCode":
            self = .qrCode
        case "manualInsertion":
            self = .manualInsertion
        case "receivement":
            self = .receivement
        case "keyManagement":
            if params.contains("preRegistration") {
                self = .preRegistration
            } else {
                self = .keyManagement
            }
        case "dailyLimit":
            self = .dailyLimit
        case "receipt":
            guard
                let id = url.queryParameters["id"] as? String,
                let type = url.queryParameters["type"] as? String
            else {
                return nil
            }
            self = .receipt(id: id, type: type)
        default:
            return nil
        }
    }
}
