import FeatureFlag
import UI

final class PIXBizDeeplinkHelper {
    typealias Dependencies = HasFeatureManager
    
    private let dependencies: Dependencies
    private let deeplinkType: PIXBizDeeplinkType
    
    init(dependencies: Dependencies, deeplinkType: PIXBizDeeplinkType) {
        self.dependencies = dependencies
        self.deeplinkType = deeplinkType
    }
  
    func handleDeeplink(_ navigationController: UINavigationController) -> Bool {
        let pixDestination: PIXBizFlow
        switch deeplinkType {
        case let .feedback(feedbackType):
            let feedbackAction = KeyManagerInputAction.feedback(feedbackType: feedbackType)
            pixDestination = .optin(action: .keyManager(input: feedbackAction))
        case .keyManagement:
            pixDestination = .optin(action: .keyManager(input: .none))
        case .removalCompleted:
            pixDestination = .optin(action: .keyManager(input: .removalCompleted))
        case .hub:
            pixDestination = .hub
        case let .receipt(transactionId):
            pixDestination = .receipt(transactionId: transactionId, isRefundAllowed: true, data: nil)
        case .dailyLimit:
            guard dependencies.featureManager.isActive(.isPixDailyLimitAvailable) else {
                return false
            }
            pixDestination = .dailyLimit
        }
        
        PIXBizFlowCoordinator(with: navigationController)
            .perform(flow: pixDestination)
        return true
    }
}
