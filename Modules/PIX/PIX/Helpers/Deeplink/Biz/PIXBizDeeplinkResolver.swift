import Core
import FeatureFlag
import UI
import UIKit

public struct PIXBizDeeplinkResolver: DeeplinkResolver {
    public typealias Dependencies = HasFeatureManager
    
    private let dependencies: Dependencies
    private let navigationControler: UINavigationController
    
    public init(dependencies: Dependencies, navigation: UINavigationController) {
        self.dependencies = dependencies
        self.navigationControler = navigation
    }
    
    public func canHandle(url: URL, isAuthenticated: Bool) -> DeeplinkResolverResult {
        guard let pixDeeplinkType = PIXBizDeeplinkType(url: url) else {
            return .notHandleable
        }
        
        if pixDeeplinkType.needsAuthentication && !isAuthenticated {
            return .onlyWithAuth
        }
        
        return .handleable
    }
    
    public func open(url: URL, isAuthenticated: Bool) -> Bool {
        guard
            let deeplinkType = PIXBizDeeplinkType(url: url),
            canHandle(url: url, isAuthenticated: isAuthenticated) == .handleable
            else {
                return false
        }
        
        return PIXBizDeeplinkHelper(dependencies: dependencies, deeplinkType: deeplinkType).handleDeeplink(navigationControler)
    }
}
