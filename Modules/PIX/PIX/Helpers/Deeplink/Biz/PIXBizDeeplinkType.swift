import Foundation

enum PIXBizDeeplinkType {
    case hub
    case receipt(transactionId: String)
    case feedback(feedbackType: FeedbackViewType)
    case keyManagement
    case removalCompleted
    case dailyLimit
    
    var needsAuthentication: Bool {
        true
    }
    
    init?(url: URL) {
        let PIX_PATH = "pix"
        
        guard PIX_PATH == url.pathComponents.first(where: { $0 != "/" }) else {
            return nil
        }
        
        let params = url.pathComponents.filter { $0 != "/" && $0 != PIX_PATH }
        
        if params.count > 1,
           params[1] == "removalCompleted" {
            self = .removalCompleted
            return
        }
        
        if let paramater = params.first {
            switch paramater {
            case "feedback":
                guard let feedbackType = FeedbackViewType.type(with: url) else {
                    break
                }
                self = .feedback(feedbackType: feedbackType)
                return
            case "hub":
                self = .hub
                return
            case "receive-receipt", "payment-receipt":
                let transactionId = url.queryParameters["transactionId"] as? String ?? ""
                self = .receipt(transactionId: transactionId)
                return
            case "dailyLimit":
                self = .dailyLimit
                return
            default:
                break
            }
        }
        self = .keyManagement
    }
}
