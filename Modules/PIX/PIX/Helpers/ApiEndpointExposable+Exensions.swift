import Core

extension ApiEndpointExposable {
    func headerWith(pin: String) -> [String: String] {
        ["pin": pin]
    }
}
