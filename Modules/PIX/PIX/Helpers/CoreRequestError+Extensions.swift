import Core

extension Core.RequestError {
    var dictionaryObject: [String: Any] {
        let dict = try? JSONSerialization.jsonObject(with: jsonData ?? Data(), options: JSONSerialization.ReadingOptions.allowFragments) as? [String: Any] ?? [:]
        return dict ?? [:]
    }
}
