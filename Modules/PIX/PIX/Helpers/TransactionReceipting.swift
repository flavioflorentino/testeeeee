import Foundation

public protocol TransactionReceipting {
    static func createReceipt(id: String, type: String) -> UIViewController
}
