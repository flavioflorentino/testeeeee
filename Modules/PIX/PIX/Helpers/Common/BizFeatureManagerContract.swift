import Foundation
import Core

// swiftlint:disable discouraged_optional_boolean
public protocol BizFeatureManagerContract {
    func isActive(key: BizFeatureConfig) -> Bool?
}

public enum BizFeatureConfig: String {
    case releasePIXCashoutPresenting = "release_pix_cashout_presenting_bool"
    case releasePIXCopyPastePresenting = "release_pix_copy_paste_presenting_bool"
}
