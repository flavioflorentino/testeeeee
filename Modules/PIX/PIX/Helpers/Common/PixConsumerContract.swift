public protocol PixConsumerContract {
    var consumerId: Int { get }
    var name: String { get }
    var imageURL: URL? { get }
}
