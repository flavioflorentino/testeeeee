import Foundation
import Core

public protocol CostumeSupportContract {
    func openFAQ(in controller: UIViewController)
}
