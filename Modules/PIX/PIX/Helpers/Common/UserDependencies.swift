import Core

public protocol HasUserAuthManager {
    var userAuthManagerContract: UserAuthManagerContract? { get }
}

public protocol HasBizAuth {
    var businessAuthContract: BizAuthManagerContract? { get }
}

public protocol HasCustomerSupport {
    var costumeSupportContract: CostumeSupportContract? { get }
}

public protocol HasPFAuth {
    var authContract: AuthenticationContract? { get }
}

protocol HasNoDependency {}
