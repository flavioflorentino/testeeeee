public protocol UserAuthManagerContract {
    func getUserInfo(completion: @escaping (KeyManagerBizUserInfo?) -> Void)
}
