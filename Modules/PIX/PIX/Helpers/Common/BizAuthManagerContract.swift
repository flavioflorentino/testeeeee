import Foundation
import Core
    
public enum PerformActionResult {
    case canceled
    case success(String)
    case failure(errorMessage: String)
}

public protocol BizAuthManagerContract {
    func performActionWithAuthorization(_ message: String?,
                                        _ action: @escaping ((_ result: PerformActionResult) -> Void))
    func getAuthorizationToken() -> String
}
