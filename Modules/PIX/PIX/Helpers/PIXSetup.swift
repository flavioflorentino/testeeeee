import Core
import CustomerSupport
import UI

public enum PIXSetup {
    public static var deeplinkInstance: DeeplinkContract?
    public static var navigationInstance: NavigationContract?
    public static var consumerInstance: PixConsumerContract?
    public static var authInstance: AuthenticationContract?
    public static var userAuthManager: UserAuthManagerContract?
    public static var businessAuthManagerInstance: BizAuthManagerContract?
    public static var costumeSupportContract: CostumeSupportContract?
    public static var zendeskContainer: ThirdPartySupportSDKContract?
    public static var identityValidation: IdentityValidationContract?
    public static var bizFeatureManager: BizFeatureManagerContract?
    public static var tokenManager: TokenManagerContract?
    
    public static func inject(instance: DeeplinkContract) {
        deeplinkInstance = instance
    }
    
    public static func inject(instance: NavigationContract) {
        navigationInstance = instance
    }
    
    public static func inject(instance: PixConsumerContract) {
        consumerInstance = instance
    }
    
    public static func inject(instance: AuthenticationContract) {
        authInstance = instance
    }

    public static func inject(instance: UserAuthManagerContract) {
        userAuthManager = instance
    }
    
    public static func inject(instance: BizAuthManagerContract) {
        businessAuthManagerInstance = instance
    }
    
    public static func inject(instance: CostumeSupportContract) {
        costumeSupportContract = instance
    }

    public static func inject(instance: ThirdPartySupportSDKContract) {
        zendeskContainer = instance
    }

    public static func inject(instance: IdentityValidationContract) {
        identityValidation = instance
    }
    
    public static func inject(instance: BizFeatureManagerContract) {
        bizFeatureManager = instance
    }
    
    public static func inject(instance: TokenManagerContract) {
        tokenManager = instance
    }
}
