import Core
import UI
import UIKit

public enum PIXConsumerFlow {
    case hub(origin: String, paymentOpening: PIXPaymentOpening, bankSelectorType: BankSelector.Type, qrCodeScannerType: QrCodeScanning.Type)
    case keySelector(paymentOpening: PIXPaymentOpening)
    case copyPaste(paymentOpening: PIXPaymentOpening)
    case qrCode(qrCodeScannerType: QrCodeScanning.Type)
    case manualInsertion(paymentOpening: PIXPaymentOpening, bankSelectorType: BankSelector.Type)
    case receivement(origin: PIXReceiveOrigin)
    case keyManagement
    case receipt(receiptType: TransactionReceipting.Type, id: String, type: String)
    case feedback(type: FeedbackViewType)
    case preRegistration
    case removalCompleted
    case refund(paymentOpening: PIXPaymentOpening, transactionId: String)
    case dailyLimit
}

public final class PIXConsumerFlowCoordinator: Coordinating {
    public typealias Dependencies = HasKVStore
    private let dependencies: Dependencies
    
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    
    private let originViewController: UIViewController
    private let origin: PixUserNavigation
    private let destination: PIXConsumerFlow
    
    private var isKeyManagerWelcomeVisualized: Bool {
        dependencies.kvStore.boolFor(KVKey.isPixKeyManagementWelcomeVisualized)
    }
    
    public init(
        originViewController: UIViewController,
        originFlow: PixUserNavigation,
        destination: PIXConsumerFlow,
        dependencies: Dependencies? = nil
    ) {
        self.originViewController = originViewController
        self.origin = originFlow
        self.destination = destination
        self.dependencies = dependencies ?? DependencyContainer()
    }
    
    public func start(isModalPresentation: Bool = true, needNavigationInModalPresentation: Bool = true) {
        let viewController = destinationController(for: destination)
        
        guard isModalPresentation else {
            push(viewController: viewController)
            return
        }
        
        present(viewController: viewController, needNavigationInModalPresentation: needNavigationInModalPresentation)
    }
    
    private func present(viewController: UIViewController, needNavigationInModalPresentation: Bool) {
        let presentableController: UIViewController
        if needNavigationInModalPresentation {
            presentableController = UINavigationController(rootViewController: viewController)
        } else {
            presentableController = viewController
        }
        originViewController.present(presentableController, animated: true)
    }
    
    private func push(viewController: UIViewController) {
        guard let originNavigationController = originViewController as? UINavigationController else {
            return
        }
        originNavigationController.pushViewController(viewController, animated: true)
    }
    
    private func destinationController(for destination: PIXConsumerFlow) -> UIViewController {
        switch destination {
        case let .hub(origin, paymentOpening, bankSelectionType, qrCodeScannerType):
            return HubFactory.make(
                origin: origin,
                paymentOpening: paymentOpening,
                bankSelectorType: bankSelectionType,
                qrCodeScannerType: qrCodeScannerType
            )
        case let .keySelector(paymentOpening):
            return KeySelectorFactory.make(paymentOpening: paymentOpening)
        case let .copyPaste(paymentOpening):
            return CopyPastePFFactory.make(paymentOpening: paymentOpening, origin: .deeplink)
        case let .qrCode(qrCodeScannerType):
            return qrCodeScannerType.init(backButtonSide: .left, hasMyCodeBottomSheet: false, scannerType: .pix)
        case let .manualInsertion(paymentOpening, bankSelectorType):
            return ManualInsertionFactory.make(bankSelectorType: bankSelectorType, paymentOpening: paymentOpening)
        case let .receivement(origin):
            return ReceiveLoadingFactory.make(origin: origin)
        case .keyManagement:
            guard isKeyManagerWelcomeVisualized else {
                return KeyManagementWelcomeFactory.make(.pf, from: origin)
            }
            return KeyManagerConsumerFactory.make(from: origin, checkForPreRegisteredKeys: false)
        case .dailyLimit:
            return DailyLimitFactory.makePF()
        case let .receipt(receiptType, id, type):
            return receiptType.createReceipt(id: id, type: type)
        case .preRegistration:
            guard isKeyManagerWelcomeVisualized else {
                return KeyManagementWelcomeFactory.make(.pf)
            }
            return KeyManagerConsumerFactory.make(from: origin, checkForPreRegisteredKeys: true)
            
        case .removalCompleted:
            guard isKeyManagerWelcomeVisualized else {
                return KeyManagementWelcomeFactory.make(.pf)
            }
            return KeyManagerConsumerFactory.make(from: .deeplink, checkForPreRegisteredKeys: false)
            
        case let .feedback(type):
            guard isKeyManagerWelcomeVisualized else {
                return KeyManagementWelcomeFactory.make(.pf)
            }
            return KeyManagerConsumerFactory.make(from: .deeplink, checkForPreRegisteredKeys: false, feedbackType: type)
        case let .refund(paymentOpening, transactionId):
            return ReturnLoadingConsumerFactory.make(paymentOpening: paymentOpening, transactionId: transactionId)
        }
    }
}
