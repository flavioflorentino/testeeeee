import UIKit
import Advertising
import UI

class HomeViewController: UIViewController {
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var refreshButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = Palette.ppColorBranding300.color
        button.addTarget(self, action: #selector(didTapRefreshButton), for: .touchUpInside)
        button.setTitle("Refresh", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16.0, weight: .semibold)
        button.setTitleColor(Palette.ppColorGrayscale100.color(withCustomDark: .ppColorGrayscale100), for: .normal)
        button.titleLabel?.textAlignment = .center
        button.layer.cornerRadius = 8
        return button
    }()
    
    let containerBanner = ContainerPinnedBannersFactory.make(source: .home)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(containerBanner)
        view.addSubview(refreshButton)
        view.backgroundColor = .white
        
        containerBanner.layout {
            $0.top == view.compatibleSafeAreaLayoutGuide.topAnchor
            $0.leading == view.leadingAnchor + Spacing.base01
            $0.trailing == view.trailingAnchor - Spacing.base01
        }
        
        refreshButton.layout {
            $0.top == containerBanner.bottomAnchor + Spacing.base00
            $0.leading == view.leadingAnchor + Spacing.base01
            $0.trailing == view.trailingAnchor - Spacing.base01
            $0.height == 50
        }
    }
    
    @objc
    private func didTapRefreshButton() {
        containerBanner.refresh()
    }
}
