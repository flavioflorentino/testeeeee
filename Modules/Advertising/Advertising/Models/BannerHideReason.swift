public enum BannerHideReason: String, Codable {
    case notInterested = "NOT_INTERESTED"
    case remindLater = "REMIND_LATER"
}
