struct InlineBanner: BannerBuilderView {
    let key: String
    let title: String
    let image: String
    let action: AdvertisingAction
    let backgroundColor: Banner.Color?
    let textColor: Banner.Color?
    
    init?(banner: Banner) {
        guard
            case .inline = banner.type,
            let key = banner.key,
            let title = banner.customTitle ?? banner.title,
            let image = banner.image,
            banner.action.type.isValid
            else {
                return nil
        }
        
        self.key = key
        self.title = title
        self.image = image
        self.action = banner.action
        self.backgroundColor = banner.backgroundColor
        self.textColor = banner.textColor
    }
    
    func buildView() -> BannerView {
        InlineBannerView(banner: self)
    }
}
