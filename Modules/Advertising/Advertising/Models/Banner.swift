public struct Banner: Codable {
    let key: String?
    let type: BannerType
    let title: String?
    let customTitle: String?
    let image: String?
    let action: AdvertisingAction
    let backgroundColor: Color?
    let textColor: Color?
    
    public struct Color: Codable {
        let light: String?
        let dark: String?
    }
    
    public enum BannerType: String, Codable {
        case inline = "INLINE"
        case notExists = "NOT_EXISTS"
        
        public init(from decoder: Decoder) throws {
            self = try BannerType(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .notExists
        }
        
        var isValid: Bool {
            self != .notExists
        }
    }
}
