public struct InAppDialog: Codable {
    let key: String?
    let customTitle: String?
    let subtitle: String?
    let image: String?
    let firstButton: InAppDialogButton
    let secondButton: InAppDialogButton
    
    public struct InAppDialogButton: Codable {
        let text: String?
        let action: AdvertisingAction
    }
}
