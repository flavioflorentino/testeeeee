public struct Badge: Codable {
    public let key: BadgeType
    public let label: String?
}

public enum BadgeType: String, Codable {
    case wallet = "WALLET"
}
