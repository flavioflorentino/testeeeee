public struct BadgePayload: Codable {
    public let badges: [Badge]
}
