public struct AdvertisingAction: Codable {
    let type: ActionType
    let url: String?
    
    public enum ActionType: String, Codable {
        case close = "CLOSE"
        case deeplink = "DEEPLINK"
        case notExists = "NOT_EXISTS"
        
        public init(from decoder: Decoder) throws {
            self = try ActionType(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .notExists
        }
        
        var isValid: Bool {
            self != .notExists
        }
    }
}
