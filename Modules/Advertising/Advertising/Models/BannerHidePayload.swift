public struct BannerHidePayload: Codable {
    let key: String
    let reason: BannerHideReason
}
