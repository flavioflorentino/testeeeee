struct BannersPayload: Codable {
    let pinned: [Banner]?
    let inappDialog: InAppDialog?
}
