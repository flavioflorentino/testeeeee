import AnalyticsModule
import UI
import UIKit

public enum BannerActionSheet {
    public typealias CompletionSheet = ((BannerHideReason) -> Void)
    
    public static func dismiss(key: String, view: UIView, completion: @escaping CompletionSheet) {
        let sheet = UIAlertController(title: nil, message: Strings.Banner.Action.message, preferredStyle: .actionSheet)
        
        let notInterest = UIAlertAction(title: Strings.Banner.Action.notInterested, style: .default, handler: { _ in
            Analytics.shared.log(AdvertisingBannerEvent.closed(key: key, action: .notInterested))
            completion(.notInterested)
        })
        
        let remindLater = UIAlertAction(title: Strings.Banner.Action.remindLate, style: .default, handler: { _ in
            Analytics.shared.log(AdvertisingBannerEvent.closed(key: key, action: .rememberLate))
            completion(.remindLater)
        })
        
        let dismiss = UIAlertAction(title: Strings.Banner.Action.dismiss, style: .cancel, handler: { _ in
            Analytics.shared.log(AdvertisingBannerEvent.closed(key: key, action: .cancel))
        })
        
        sheet.addAction(notInterest)
        sheet.addAction(remindLater)
        sheet.addAction(dismiss)
        
        view.window?.rootViewController?.present(sheet, animated: true, completion: nil)
    }
}
