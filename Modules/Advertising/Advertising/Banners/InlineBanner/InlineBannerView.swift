import UI
import UIKit

extension InlineBannerView.Layout {
    enum Length {
        static let titleMaxLines = 5
    }
    enum Font {
        static let title = Typography.bodyPrimary(.default).font()
        static let highlight = Typography.bodyPrimary(.highlight).font()
    }
    enum Spacing {
        static let closeButtonMargin: CGFloat = 5
        static let titleMargin: CGFloat = 20
    }
    enum Insets {
        static let closeButton = UIEdgeInsets(top: 6, left: 6, bottom: 6, right: 6)
    }
    enum Size {
        static let image = CGSize(width: 42, height: 42)
    }
}

class InlineBannerView: UIView {
    fileprivate enum Layout {}
    private var banner: InlineBanner
    
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.attributedText = banner.title.attributedStringWith(
            normalFont: Layout.Font.title,
            highlightFont: Layout.Font.highlight,
            normalColor: bannerTextColor ?? Colors.white.color,
            highlightColor: bannerTextColor ?? Colors.white.color,
            underline: false
        )
        label.numberOfLines = Layout.Length.titleMaxLines
        return label
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        if let url = URL(string: banner.image) {
            imageView.setImage(url: url)
        }
        
        return imageView
    }()
    
    private lazy var closeButton: UIButton = {
        let button = UIButton()
        let icon = Assets.icoClose.image.withRenderingMode(.alwaysTemplate)
        button.setImage(icon, for: .normal)
        button.imageEdgeInsets = Layout.Insets.closeButton
        button.imageView?.tintColor = bannerTextColor ?? Palette.ppColorGrayscale000.color
        button.setContentCompressionResistancePriority(.init(999), for: .horizontal)
        button.setContentCompressionResistancePriority(.init(999), for: .vertical)
        button.setContentHuggingPriority(.init(999), for: .horizontal)
        button.setContentHuggingPriority(.init(999), for: .vertical)
        return button
    }()
    
    init(banner: InlineBanner) {
        self.banner = banner
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTapAction))
        addGestureRecognizer(tapGesture)
        closeButton.addTarget(self, action: #selector(onTapCloseAction), for: .touchUpInside)
    }
}

@objc
private extension InlineBannerView {
    func onTapAction(_ sender: UITapGestureRecognizer) {
        tapAction()
    }
    func onTapCloseAction() {
       dismiss()
    }
}

extension InlineBannerView: BannerView {
    var builderBannerView: BannerBuilderView { banner }
}

extension InlineBannerView: ViewConfiguration {
    func setupConstraints() {
        imageView.layout {
            $0.top >= topAnchor + Spacing.base01
            $0.bottom <= bottomAnchor - Spacing.base01
            $0.leading == leadingAnchor + Spacing.base01
            $0.centerY == centerYAnchor
            $0.width == Layout.Size.image.width
            $0.height == Layout.Size.image.height
        }

        textLabel.layout {
            $0.top >= topAnchor + Spacing.base01
            $0.bottom <= bottomAnchor - Spacing.base01
            $0.centerY == centerYAnchor
            $0.leading == imageView.trailingAnchor + Spacing.base00
        }

        closeButton.layout {
            $0.top == self.topAnchor + Layout.Spacing.closeButtonMargin
            $0.leading >= textLabel.trailingAnchor + Spacing.base00
            $0.trailing == self.trailingAnchor - Layout.Spacing.closeButtonMargin
        }
    }
    
    func buildViewHierarchy() {
        addSubview(textLabel)
        addSubview(closeButton)
        addSubview(imageView)
    }
    
    func configureViews() {
        layer.masksToBounds = true
        isUserInteractionEnabled = true
        setupGestures()
    }
    
    func configureStyles() {
        backgroundColor = bannerBackgroundColor ?? Colors.grayscale800.color
        layer.cornerRadius = CornerRadius.medium
    }
}
