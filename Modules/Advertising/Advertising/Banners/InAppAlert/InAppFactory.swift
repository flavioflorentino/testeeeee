enum InAppFactory {
    static func make(_ payload: BannersPayload) {
        let container = DependencyContainer()
        let service = AdvertisingService()
        let presenter = InAppPresenter()
        let interactor = InAppInteractor(service: service, presenter: presenter, dependencies: container)
        let controller = InAppController(interactor: interactor)
        
        presenter.controller = controller
        controller.start(payload)
    }
}
