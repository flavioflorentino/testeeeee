import UI

protocol InAppPresenting {
    func presentPopup(inApp: InAppDialog, navigation: UINavigationController)
}

final class InAppPresenter {
    var controller: InAppDisplaying?
}

extension InAppPresenter: InAppPresenting {
    func presentPopup(inApp: InAppDialog, navigation: UINavigationController) {
        guard let title = inApp.customTitle,
              let description = inApp.subtitle,
              let primaryAction = inApp.firstButton.text,
              let secondAction = inApp.secondButton.text else {
            return
        }
        
        let popup = PopupViewController(title: title, description: description, preferredType: popupPreferedType(inApp))
        popup.setAttributeDescription(setupDescription(description))
        
        controller?.displayInApp(navigation, popup: popup, primaryAction: primaryAction, secondAction: secondAction)
    }
}

// MARK: - InApp Setup
extension InAppPresenter {
    private func popupPreferedType(_ inApp: InAppDialog) -> PopupType {
        if let image = inAppImage(inApp) {
            return .image(image)
        } else {
            return .text
        }
    }
    
    private func inAppImage(_ inApp: InAppDialog) -> UIImage? {
        guard let image = inApp.image, let imageURL = URL(string: image) else {
            return nil
        }
        
        do {
            let imageData = try Data(contentsOf: imageURL as URL)
            return UIImage(data: imageData)
        } catch {
            return nil
        }
    }
    
    private func setupDescription(_ description: String) -> NSMutableAttributedString {
        let attributedText = NSMutableAttributedString(string: description)
        attributedText.font(text: description, font: Typography.bodyPrimary().font())
        attributedText.paragraph(aligment: .center, lineSpace: 4.0)
        attributedText.textColor(text: description, color: Colors.grayscale700.color)
        return attributedText
    }
}
