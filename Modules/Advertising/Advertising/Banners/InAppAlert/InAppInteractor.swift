import Core
import FeatureFlag
import UI

protocol InAppInteracting: AnyObject {
    func execute(_ payload: BannersPayload)
    func primaryAction()
    func secondaryAction()
}

final class InAppInteractor {
    private var inAppDialog: InAppDialog?
    
    private let service: AdvertisingServicing
    private let presenter: InAppPresenting
    
    typealias Dependencies = HasMainQueue & HasFeatureManager
    private let dependencies: Dependencies
    
    init(service: AdvertisingServicing, presenter: InAppPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

extension InAppInteractor: InAppInteracting {
    func execute(_ payload: BannersPayload) {
        guard let inapp = payload.inappDialog,
              let navigation = AdvertisingSetup.navigationInstance?.getCurrentNavigation(),
              dependencies.featureManager.isActive(.isAdvertisingInAppDialogAvailable) else {
            return
        }
        
        inAppDialog = inapp
        
        dependencies.mainQueue.asyncAfter(deadline: .now() + .seconds(3)) {
            self.presenter.presentPopup(inApp: inapp, navigation: navigation)
        }
    }
    
    func primaryAction() {
        if let inApp = inAppDialog,
           case .deeplink = inApp.firstButton.action.type,
            let urlText = inApp.firstButton.action.url,
            let url = URL(string: urlText) {
            AdvertisingSetup.deeplinkInstance?.open(url: url)
        }
    }
    
    func secondaryAction() {
        if let inApp = inAppDialog,
           case .close = inApp.secondButton.action.type,
           let key = inApp.key {
            service.hideBannerWith(key: key, reason: .notInterested, completion: .none)
        }
    }
}
