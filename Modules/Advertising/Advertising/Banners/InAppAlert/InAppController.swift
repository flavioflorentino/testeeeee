import UI

public protocol InAppDisplaying {
    func displayInApp(_ navigation: UINavigationController, popup: PopupViewController, primaryAction: String, secondAction: String)
}

final class InAppController {
    private var interactor: InAppInteractor
    
    init(interactor: InAppInteractor) {
        self.interactor = interactor
    }
    
    func start(_ payload: BannersPayload) {
        interactor.execute(payload)
    }
}

extension InAppController: InAppDisplaying {
    public func displayInApp(_ navigation: UINavigationController, popup: PopupViewController, primaryAction: String, secondAction: String) {
        popup.addAction(.init(title: primaryAction, style: .fill, completion: {
            self.interactor.primaryAction()
        }))
        
        popup.addAction(.init(title: secondAction, style: .link, completion: {
            self.interactor.secondaryAction()
        }))
        
        navigation.showPopup(popup)
    }
}
