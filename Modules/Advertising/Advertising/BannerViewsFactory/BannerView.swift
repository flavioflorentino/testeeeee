import UIKit
import UI
import AnalyticsModule

protocol BannerView: UIView {
    var builderBannerView: BannerBuilderView { get }
    var bannerBackgroundColor: UIColor? { get }
    var bannerTextColor: UIColor? { get }
    func dismiss()
    func tapAction()
}

extension BannerView {
    var bannerBackgroundColor: UIColor? {
        guard
            let backgroundColor = builderBannerView.backgroundColor,
            let color = getColor(color: backgroundColor) 
            else {
                return nil
        }
        
        return color
    }
    
    var bannerTextColor: UIColor? {
        guard
            let textColor = builderBannerView.textColor,
            let color = getColor(color: textColor) 
            else {
                return nil
        }
        
        return color
    }
    
    func tapAction() {
        Analytics.shared.log(AdvertisingBannerEvent.clicked(key: builderBannerView.key))
        if case .deeplink = builderBannerView.action.type,
            let urlText = builderBannerView.action.url,
            let url = URL(string: urlText) {
            AdvertisingSetup.deeplinkInstance?.open(url: url)
        }
    }
    
    func dismiss() {
        Analytics.shared.log(AdvertisingBannerEvent.closed(key: builderBannerView.key, action: .closed))
        BannerActionSheet.dismiss(key: builderBannerView.key, view: self) { [weak self] reason in
            guard let self = self else {
                return
            }
            AdvertisingService().hideBannerWith(key: self.builderBannerView.key, reason: reason)
            self.hideBanner()
        }
    }
    
    private func hideBanner() {
        UIView.animate(
            withDuration: 0.3,
            animations: { [weak self] in
                self?.isHidden = true
            },
            completion: { [weak self] _ in
                self?.removeFromSuperview()
            }
        )
    }
    
    private func getColor(color: Banner.Color) -> UIColor? {
        guard
            let lightColorHex = color.light,
            let darkColorHex = color.dark,
            let lightColor = Colors.hexColor(lightColorHex)?.color,
            let darkColor = Colors.hexColor(darkColorHex)?.color
            else {
                return nil
        }
        return DynamicColor(lightColor: lightColor, darkColor: darkColor).color
    }
}
