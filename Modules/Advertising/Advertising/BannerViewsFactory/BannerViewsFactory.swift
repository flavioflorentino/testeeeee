enum BannerViewsFactory {
    // For each new Banner, we need to register the type here, on bannerTypes array
    private static let bannerTypes: [BannerBuilderView.Type] = [InlineBanner.self]
    
    static func make(banners: [Banner]) -> [BannerView] {
        let buildersViews: [BannerBuilderView] = banners
            .compactMap { findValidBannerType($0) }
            
        return buildersViews.map { $0.buildView() }
    }
    
    private static func findValidBannerType(_ banner: Banner) -> BannerBuilderView? {
        for bannerType in bannerTypes {
            if let bannerBuilderView = bannerType.init(banner: banner) {
                return bannerBuilderView
            }
        }
        return nil
    }
}
