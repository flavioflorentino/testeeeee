protocol BannerBuilderView {
    init?(banner: Banner)
    func buildView() -> BannerView
    var backgroundColor: Banner.Color? { get }
    var textColor: Banner.Color? { get }
    var key: String { get }
    var action: AdvertisingAction { get }
}
