import Core
import UI

public enum AdvertisingSetup {
    public static var deeplinkInstance: DeeplinkContract?
    public static var navigationInstance: NavigationContract?
    
    public static func inject(instance: DeeplinkContract) {
        deeplinkInstance = instance
    }
    
    public static func inject(instance: NavigationContract) {
        navigationInstance = instance
    }
}
