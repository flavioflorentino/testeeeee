import AnalyticsModule
import Core
import FeatureFlag
import Foundation

protocol HasNoDependency {}

typealias AppDependencies =
    HasNoDependency &
    HasAnalytics &
    HasFeatureManager &
    HasMainQueue

final class DependencyContainer: AppDependencies {
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var mainQueue = DispatchQueue.main
}
