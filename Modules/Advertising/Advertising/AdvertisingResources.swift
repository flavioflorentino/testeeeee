import Foundation

// swiftlint:disable convenience_type
final class AdvertisingResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: AdvertisingResources.self).url(forResource: "AdvertisingResources", withExtension: "bundle") else {
            return Bundle(for: AdvertisingResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: AdvertisingResources.self)
    }()
}
