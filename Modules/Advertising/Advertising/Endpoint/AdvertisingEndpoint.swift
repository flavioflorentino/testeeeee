import Foundation
import Core

enum AdvertisingEndpoint {
    case bannersWith(source: BannerSource)
    case hideBannerWith(bannerHidePayload: BannerHidePayload)
    case badgeGet
    case badgeDelete(badgePaylodDelete: BadgePaylodDelete)
}

extension AdvertisingEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .bannersWith(let source):
            return "advertising/banner?source=\(source)"
        case .hideBannerWith:
            return "advertising/banner/hide"
        case
            .badgeGet,
            .badgeDelete:
            return "advertising/badge"
        }
    }
        
    var method: HTTPMethod {
        switch self {
        case
            .bannersWith,
            .badgeGet:
            return .get
        case .hideBannerWith:
            return .post
        case .badgeDelete:
            return .delete
        }
    }

    var shouldAppendBody: Bool {
        body != nil
    }
    
    var body: Data? {
        switch self {
        case .hideBannerWith(let bannerHidePayload):
            return prepareBody(with: bannerHidePayload)
        case .badgeDelete(let badgePaylodDelete):
            return prepareBody(with: badgePaylodDelete)
        default:
            return nil
        }
    }
}
