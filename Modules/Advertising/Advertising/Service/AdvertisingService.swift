import Core
import Foundation

protocol AdvertisingServicing {
    func bannersWith(source: BannerSource, completion: @escaping CompletionBannersResult)
    func hideBannerWith(key: String, reason: BannerHideReason, completion: CompletionBannerHide?)
}

public protocol AdvertisingServicingPublic {
    func getBadges(completion: @escaping CompletionGetBadge)
    func delBadge(key: BadgeType, completion: CompletionDelBadge?)
}

typealias CompletionBannersResult = (Result<BannersPayload, ApiError>) -> Void
typealias CompletionBannerHide = (Result<NoContent, ApiError>) -> Void
public typealias CompletionGetBadge = (Result<BadgePayload, ApiError>) -> Void
public typealias CompletionDelBadge = (Result<NoContent, ApiError>) -> Void

public final class AdvertisingService: AdvertisingServicing {
    public init() { }

    internal func bannersWith(source: BannerSource, completion: @escaping CompletionBannersResult) {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        Api<BannersPayload>(endpoint: AdvertisingEndpoint.bannersWith(source: source)).execute(jsonDecoder: decoder) { result in
            DispatchQueue.main.async {
                let mappedResult = result
                    .mapError { $0 as ApiError }
                    .map { $0.model }
                switch mappedResult {
                case .success(let banners):
                    InAppFactory.make(banners)
                case .failure(let error):
                    debugPrint(error)
                }
                completion(mappedResult)
            }
        }
    }

    internal func hideBannerWith(key: String, reason: BannerHideReason, completion: CompletionBannerHide? = nil) {
        let bannerHidePayload = BannerHidePayload(key: key, reason: reason)
        Api<NoContent>(endpoint: AdvertisingEndpoint.hideBannerWith(bannerHidePayload: bannerHidePayload)).execute { result in
            DispatchQueue.main.async {
                let mappedResult = result
                    .mapError { $0 as ApiError }
                    .map { $0.model }
                completion?(mappedResult)
            }
        }
    }
}

extension AdvertisingService: AdvertisingServicingPublic {
    public func getBadges(completion: @escaping CompletionGetBadge) {
        Api<BadgePayload>(endpoint: AdvertisingEndpoint.badgeGet).execute { result in
            DispatchQueue.main.async {
                let mappedResult = result
                    .mapError { $0 as ApiError }
                    .map { $0.model }
                completion(mappedResult)
            }
        }
    }
    public func delBadge(key: BadgeType, completion: CompletionDelBadge? = nil) {
        let badgePaylodDelete = BadgePaylodDelete(key: key.rawValue)
        Api<NoContent>(endpoint: AdvertisingEndpoint.badgeDelete(badgePaylodDelete: badgePaylodDelete)).execute { result in
            DispatchQueue.main.async {
                let mappedResult = result
                    .mapError { $0 as ApiError }
                    .map { $0.model }
                completion?(mappedResult)
            }
        }
    }
}
