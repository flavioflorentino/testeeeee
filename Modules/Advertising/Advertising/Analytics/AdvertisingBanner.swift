import AnalyticsModule

enum AdvertisingBannerEvent: AnalyticsKeyProtocol {
    case clicked(key: String)
    case closed(key: String, action: ClosedEventAction)
    
    private var name: String {
        switch self {
        case .clicked:
            return "Banner Clicked"
        case .closed:
            return "Banner Closed"
        }
    }
    
    private var providers: [AnalyticsProvider] {
        guard case .closed = self else {
            return [.mixPanel, .firebase]
        }
        return [.mixPanel, .firebase, .appsFlyer]
    }
    
    private var properties: [String: Any] {
        switch self {
        case .clicked(let key):
            return [
                "banner_key": key,
                "action": "act-banner"
            ]
        case let .closed(key, action):
            return [
                "banner_key": key,
                "action": action.rawValue
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PicPay General - Advertising Banner - \(name)", properties: properties, providers: providers)
    }
}

extension AdvertisingBannerEvent {
    enum ClosedEventAction: String {
        case closed = "act-closed"
        case notInterested = "act-not-interested"
        case rememberLate = "act-remember-late"
        case cancel = "act-cancel"
    }
}
