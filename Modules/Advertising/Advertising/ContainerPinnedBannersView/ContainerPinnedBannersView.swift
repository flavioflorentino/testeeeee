import UIKit
import UI

public final class ContainerPinnedBannersView: UIStackView {
    let viewModel: ContainerPinnedBannersViewModelInputs
    
    override public var bounds: CGRect {
        didSet {
            guard bounds.height != oldValue.height else { return }
            notifyFrameUpdate()
        }
    }
    
    public var onRefresh: ((CGFloat) -> Void)?
    public init(viewModel: ContainerPinnedBannersViewModelInputs) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        configureViews()
    }

    @available(*, unavailable)
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureViews() {
        axis = .vertical
        distribution = .equalSpacing
        spacing = Spacing.base01
    }
    
    private func notifyFrameUpdate() {
        guard !frame.height.isZero && arrangedSubviews.isNotEmpty else {
            onRefresh?(0)
            return
        }
        onRefresh?(frame.height + Spacing.base01)
    }
}

extension ContainerPinnedBannersView: ContainerPinnedBannersDisplay {
    func updateBanners(banners: [BannerView]) {
        arrangedSubviews.forEach { $0.removeFromSuperview() }
        banners.forEach { addArrangedSubview($0) }
    }
}

extension ContainerPinnedBannersView: ContainerPinnedBannersViewRefreshing {
    public func refresh() {
        viewModel.refresh()
    }
}
