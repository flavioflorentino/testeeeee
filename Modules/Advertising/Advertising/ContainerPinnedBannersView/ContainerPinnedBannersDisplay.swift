protocol ContainerPinnedBannersDisplay: AnyObject {
    func updateBanners(banners: [BannerView])
}
