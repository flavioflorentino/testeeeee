protocol ContainerPinnedBannersPresenting: AnyObject {
    var view: ContainerPinnedBannersDisplay? { get set }
    func updateBanners(banners: [Banner])
}

public class ContainerPinnedBannersPresenter: ContainerPinnedBannersPresenting {
    weak var view: ContainerPinnedBannersDisplay?
    
    func updateBanners(banners: [Banner]) {
        let bannerViews = BannerViewsFactory.make(banners: banners)
        view?.updateBanners(banners: bannerViews)
    }
}
