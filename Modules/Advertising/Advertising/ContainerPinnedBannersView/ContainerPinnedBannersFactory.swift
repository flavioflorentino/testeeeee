public enum ContainerPinnedBannersFactory {
    public static func make(source: BannerSource) -> ContainerPinnedBannersView {
        let service = AdvertisingService()
        let presenter: ContainerPinnedBannersPresenting = ContainerPinnedBannersPresenter()
        let viewModel: ContainerPinnedBannersViewModelInputs = ContainerPinnedBannersViewModel(source: source, service: service, presenter: presenter)
        let view = ContainerPinnedBannersView(viewModel: viewModel)
        
        presenter.view = view
        
        return view
    }
}
