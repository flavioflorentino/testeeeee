import Core

public protocol ContainerPinnedBannersViewModelInputs: AnyObject {
    func refresh()
}

final class ContainerPinnedBannersViewModel: ContainerPinnedBannersViewModelInputs {
    private let source: BannerSource
    private let service: AdvertisingServicing
    private let presenter: ContainerPinnedBannersPresenting
    init(source: BannerSource, service: AdvertisingServicing, presenter: ContainerPinnedBannersPresenting) {
        self.source = source
        self.service = service
        self.presenter = presenter
    }
    
    func refresh() {
        service.bannersWith(source: source) { [weak self] result in
            switch result {
            case .success(let bannersPayload):
                self?.presenter.updateBanners(banners: bannersPayload.pinned ?? [])
            case .failure(let error):
                // TODO Log error, no feedback is needed
                debugPrint(error)
            }
        }
    }
}
