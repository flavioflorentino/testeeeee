import XCTest
@testable import Advertising

private final class ContainerPinnedBannersPresentingSpy: ContainerPinnedBannersPresenting {
    var view: ContainerPinnedBannersDisplay?
    // MARK: - Variables
    private(set) var callUpdateBanners = 0
    private(set) var banners: [Banner]?
    
    // MARK: - Tests
    func updateBanners(banners: [Banner]) {
        callUpdateBanners += 1
        self.banners = banners
    }
}

private final class AdvertisingServiceSpy: AdvertisingServicing {
    func bannersWith(source: BannerSource, completion: @escaping CompletionBannersResult) {
        completion(.success(BannersPayload(pinned: [], inappDialog: nil)))
    }
    
    func hideBannerWith(key: String, reason: BannerHideReason, completion: CompletionBannerHide?) {}
}

final class ContainerPinnedBannersViewModelTests: XCTestCase {
    private var presenterSpy = ContainerPinnedBannersPresentingSpy()
    private var serviceSpy = AdvertisingServiceSpy()
    
    private lazy var sut = ContainerPinnedBannersViewModel(
        source: .home,
        service: serviceSpy,
        presenter: presenterSpy
    )
    
    // MARK: - Public Methods
    func testRefresh_WhenCalledFromViewModel_ShouldUpdatePresenter() {
        sut.refresh()
        XCTAssertEqual(presenterSpy.callUpdateBanners, 1)
        XCTAssertNotNil(presenterSpy.banners)
    }
}
