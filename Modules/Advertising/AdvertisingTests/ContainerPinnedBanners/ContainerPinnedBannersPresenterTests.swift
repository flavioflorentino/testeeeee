import XCTest
@testable import Advertising

private final class ContainerPinnedBannersViewSpy: ContainerPinnedBannersDisplay {
    // MARK: - Variables
    private(set) var callRefresh = 0
    private(set) var callUpdateBanners = 0
    private(set) var banners: [BannerView]?
    
    // MARK: - Tests
    func refresh() {
        callRefresh += 1
    }
    
    func updateBanners(banners: [BannerView]) {
        callUpdateBanners += 1
        self.banners = banners
    }
}

final class ContainerPinnedBannersPresenterTests: XCTestCase {
    // MARK: - Variables
    private var viewSpy = ContainerPinnedBannersViewSpy()
    
    private lazy var sut: ContainerPinnedBannersPresenter = {
        let sut = ContainerPinnedBannersPresenter()
        sut.view = viewSpy
        return sut
    }()
    
    // MARK: - Tests
    func testUpdateBanners_WhenCalledFromPresenter_ShouldUpdateBannersViews() {
        sut.updateBanners(banners: [])
        XCTAssertEqual(viewSpy.callUpdateBanners, 1)
    }
}
