import UIKit
import SettingsBiz

class HomeViewController: UIViewController {
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let settings = SettingsFlowCoordinator(navigationController: self.navigationController ?? UINavigationController())
        settings.start()
    }
}
