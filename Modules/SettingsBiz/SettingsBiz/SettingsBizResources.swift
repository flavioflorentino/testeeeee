import Foundation

// swiftlint:disable convenience_type
final class SettingsBizResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: SettingsBizResources.self).url(forResource: "SettingsBizResources", withExtension: "bundle") else {
            return Bundle(for: SettingsBizResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: SettingsBizResources.self)
    }()
}
