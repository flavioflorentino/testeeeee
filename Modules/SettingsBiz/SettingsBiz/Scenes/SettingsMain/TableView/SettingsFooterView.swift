import UIKit
import UI

final class SettingsFooterView: UIView {
    private lazy var versionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Internal Methods
    func setup(version: String) {
        versionLabel.text = version
    }
}

extension SettingsFooterView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(versionLabel)
    }
    
    func setupConstraints() {
        versionLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.centerX.equalToSuperview()
            $0.bottom.equalToSuperview().priority(999)
        }
    }
}
