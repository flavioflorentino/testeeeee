import UIKit
import UI

enum SettingsSection: String {
    case profile
    case storeSettings
    case qrCode
    case personalSettings
    case computerTransactions
    case promotions
    case more
    case disconnect
    
    var name: String {
        switch self {
        case .profile:
            return Strings.Main.Profile.titleSection
        case .storeSettings:
            return Strings.Main.StoreSettings.titleSection
        case .qrCode:
            return Strings.Main.QrCode.titleSection
        case .personalSettings:
            return Strings.Main.PersonalSettings.titleSection
        case .computerTransactions:
            return Strings.Main.ComputerTransactions.titleSection
        case .promotions:
            return Strings.Main.Promotions.titleSection
        case .more:
            return Strings.Main.More.titleSection
        case .disconnect:
            return ""
        }
    }
}

struct SettingsItem {
    let title: String
    let subtitle: String?
    let icon: Iconography?
    let iconColor: UIColor?
    let type: SettingsCellType
    let action: SettingsMainAction
    
    init(title: String,
         type: SettingsCellType,
         action: SettingsMainAction,
         subtitle: String? = nil,
         icon: Iconography? = nil,
         iconColor: UIColor? = nil) {
        self.title = title
        self.type = type
        self.subtitle = subtitle
        self.action = action
        self.icon = icon
        self.iconColor = iconColor
    }
}

enum SettingsCellType: String {
    case normal
    case subtitle
    case logout
    case configSwitch
}
