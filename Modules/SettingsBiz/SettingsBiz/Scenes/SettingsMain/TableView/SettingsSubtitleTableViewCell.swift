import UI
import UIKit

final class SettingsSubtitleTableViewCell: UITableViewCell, SettingsCell {
    // MARK: - Inits
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        configureViews()
    }
    
    // MARK: - Internal Methods
    func setup(model: SettingsItem) {
        textLabel?.text = model.title
        
        if let description = model.subtitle {
            detailTextLabel?.text = description
            detailTextLabel?.textColor = Colors.grayscale300.color
        }
    }
}

// MARK: - ViewConfiguration
extension SettingsSubtitleTableViewCell {
    func configureViews() {
        backgroundColor = Colors.groupedBackgroundSecondary.color
        accessoryType = .disclosureIndicator
    }
}
