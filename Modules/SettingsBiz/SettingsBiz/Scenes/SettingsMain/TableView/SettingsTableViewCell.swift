import UI
import UIKit

protocol SettingsCell where Self: UITableViewCell {
    func setup(model: SettingsItem)
}

class SettingsTableViewCell: UITableViewCell, SettingsCell {
    // MARK: - Inits
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .value1, reuseIdentifier: reuseIdentifier)
        configureViews()
    }
    
    // MARK: - Internal Methods
    func setup(model: SettingsItem) {
        textLabel?.text = model.title
        
        if let description = model.subtitle {
            detailTextLabel?.text = description
            detailTextLabel?.textColor = Colors.grayscale300.color
        }
        
        if let icon = model.icon {
            configure(icon: icon, iconColor: model.iconColor ?? Colors.grayscale300.color)
        }
    }
}

// MARK: - ViewConfiguration
private extension SettingsTableViewCell {
    func configureViews() {
        backgroundColor = Colors.groupedBackgroundSecondary.color
        accessoryType = .disclosureIndicator
    }
    
    func configure(icon: Iconography, iconColor: UIColor) {
        detailTextLabel?.text?.append(" \(icon.rawValue)")
        let iconAttributes = [
            NSAttributedString.Key.font: Typography.icons(.small).font(),
            NSAttributedString.Key.foregroundColor: iconColor
        ]
        
        detailTextLabel?.attributedText = detailTextLabel?.text?.attributedString(highlights: [icon.rawValue],
                                                                                  attrs: iconAttributes)
    }
}
