import UI
import UIKit

final class SettingsSwitchTableViewCell: SettingsTableViewCell {
    // MARK: - Properties
    private lazy var switchControl: UISwitch = {
        let switchControl = UISwitch()
        switchControl.addTarget(self, action: #selector(switchDidChangeValue), for: .valueChanged)
        return switchControl
    }()
    
    // MARK: - Inits
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    override func setup(model: SettingsItem) {
        super.setup(model: model)
        if case SettingsMainAction.none = model.action {
            accessoryType = .none
        }
    }
}

@objc
private extension SettingsSwitchTableViewCell {
    func switchDidChangeValue(_ sender: UISwitch) {
        // TODO: delegate
    }
}

// MARK: - ViewConfiguration
extension SettingsSwitchTableViewCell: ViewConfiguration {
    func configureViews() {
        backgroundColor = Colors.groupedBackgroundSecondary.color
    }
    
    func buildViewHierarchy() {
        contentView.addSubview(switchControl)
    }
    
    func setupConstraints() {
        switchControl.snp.makeConstraints {
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
            $0.centerY.equalToSuperview()
        }
    }
}
