import UIKit
import UI

final class SettingsLogoutTableViewCell: UITableViewCell, SettingsCell {
    // MARK: - Properties
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.critical900.color)
        return label
    }()
    
    // MARK: - Inits
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    // MARK: - Internal Methods
    func setup(model: SettingsItem) {
        titleLabel.text = model.title
    }
}

// MARK: - ViewConfiguration
extension SettingsLogoutTableViewCell: ViewConfiguration {
    func configureViews() {
        backgroundColor = Colors.groupedBackgroundSecondary.color
    }
    
    func buildViewHierarchy() {
        addSubview(titleLabel)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
}
