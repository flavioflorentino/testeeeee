import UIKit
import UI

final class SettingsHeaderView: UIView {
    private lazy var view = UIView()
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fill
        stack.alignment = .center
        stack.spacing = Spacing.base01
        return stack
    }()
    
    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView
            .imageStyle(RoundedImageStyle(size: .large, cornerRadius: .full))
            .with(\.contentMode, .scaleAspectFit)
        return imageView
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
        return label
    }()
    
    private lazy var addressLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SettingsHeaderView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(view)
        view.addSubview(stackView)
        stackView.addArrangedSubview(logoImageView)
        stackView.addArrangedSubview(nameLabel)
        stackView.addArrangedSubview(addressLabel)
    }
    
    func setupConstraints() {
        view.snp.makeConstraints {
            $0.trailing.top.equalToSuperview()
            $0.bottom.leading.equalToSuperview().priority(999)
        }
        
        stackView.snp.makeConstraints {
            $0.trailing.leading.bottom.equalToSuperview()
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.centerX.equalToSuperview()
        }
    }
}
