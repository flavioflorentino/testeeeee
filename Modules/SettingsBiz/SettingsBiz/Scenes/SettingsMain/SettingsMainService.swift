import Core
import Foundation

protocol SettingsMainServicing {
}

final class SettingsMainService {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - SettingsMainServicing
extension SettingsMainService: SettingsMainServicing {
}
