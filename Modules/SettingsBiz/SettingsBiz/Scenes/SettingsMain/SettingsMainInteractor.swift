import Foundation

protocol SettingsMainInteracting: AnyObject {
    func doSomething()
}

final class SettingsMainInteractor {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let service: SettingsMainServicing
    private let presenter: SettingsMainPresenting

    init(service: SettingsMainServicing, presenter: SettingsMainPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - SettingsMainInteracting
extension SettingsMainInteractor: SettingsMainInteracting {
    func doSomething() {
        presenter.displaySomething()
    }
}
