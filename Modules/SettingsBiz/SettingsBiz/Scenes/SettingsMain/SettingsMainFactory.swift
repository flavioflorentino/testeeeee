import UIKit

enum SettingsMainFactory {
    static func make() -> SettingsMainViewController {
        let container = DependencyContainer()
        let service: SettingsMainServicing = SettingsMainService(dependencies: container)
        let coordinator: SettingsMainCoordinating = SettingsMainCoordinator(dependencies: container)
        let presenter: SettingsMainPresenting = SettingsMainPresenter(coordinator: coordinator, dependencies: container)
        let interactor = SettingsMainInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = SettingsMainViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
