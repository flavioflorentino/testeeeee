import UIKit

enum SettingsMainAction {
    case none
}

protocol SettingsMainCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: SettingsMainAction)
}

final class SettingsMainCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - SettingsMainCoordinating
extension SettingsMainCoordinator: SettingsMainCoordinating {
    func perform(action: SettingsMainAction) {
    }
}
