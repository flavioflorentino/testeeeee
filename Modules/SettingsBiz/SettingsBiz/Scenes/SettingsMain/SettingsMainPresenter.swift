import Foundation

protocol SettingsMainPresenting: AnyObject {
    var viewController: SettingsMainDisplaying? { get set }
    func displaySomething()
    func didNextStep(action: SettingsMainAction)
}

final class SettingsMainPresenter {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let coordinator: SettingsMainCoordinating
    weak var viewController: SettingsMainDisplaying?

    init(coordinator: SettingsMainCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - SettingsMainPresenting
extension SettingsMainPresenter: SettingsMainPresenting {
    func displaySomething() {
        viewController?.displaySomething()
    }
    
    func didNextStep(action: SettingsMainAction) {
        coordinator.perform(action: action)
    }
}
