import UI
import UIKit

protocol SettingsMainDisplaying: AnyObject {
    func displaySomething()
}

private extension SettingsMainViewController.Layout { }

final class SettingsMainViewController: ViewController<SettingsMainInteracting, UIView> {
    fileprivate enum Layout { }
    
    private lazy var dataSource: TableViewDataSource<SettingsSection, SettingsItem> = {
        let dataSource = TableViewDataSource<SettingsSection, SettingsItem>(view: tableView)
        dataSource.itemProvider = getCell(_:_:_:)
        dataSource.headerSectionTitleProvider = getSectionName(_:_:_:)
        return dataSource
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.backgroundColor = Colors.groupedBackgroundPrimary.color
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = footerView
        tableView.sectionFooterHeight = .leastNonzeroMagnitude
        tableView.register(SettingsTableViewCell.self, forCellReuseIdentifier: SettingsTableViewCell.identifier)
        tableView.register(SettingsLogoutTableViewCell.self, forCellReuseIdentifier: SettingsLogoutTableViewCell.identifier)
        tableView.register(SettingsSubtitleTableViewCell.self, forCellReuseIdentifier: SettingsSubtitleTableViewCell.identifier)
        tableView.register(SettingsSwitchTableViewCell.self, forCellReuseIdentifier: SettingsSwitchTableViewCell.identifier)
        tableView.delegate = self
        return tableView
    }()
    
    private lazy var headerView = SettingsHeaderView()
    
    private lazy var footerView = SettingsFooterView()

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.doSomething()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        setupTableHeaderAndFooterView()
    }

    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    override func configureViews() {
        tableView.dataSource = dataSource
    }
    
    func getCell(_ view: UITableView, _ indexPath: IndexPath, _ item: SettingsItem) -> UITableViewCell? {
        let cell: UITableViewCell
        switch item.type {
        case .logout:
            cell = tableView.dequeueReusableCell(withIdentifier: SettingsLogoutTableViewCell.identifier, for: indexPath)
        case .subtitle:
            cell = tableView.dequeueReusableCell(withIdentifier: SettingsSubtitleTableViewCell.identifier, for: indexPath)
        case .configSwitch:
            cell = tableView.dequeueReusableCell(withIdentifier: SettingsSwitchTableViewCell.identifier, for: indexPath)
        case .normal:
            cell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.identifier, for: indexPath)
        }
        
        (cell as? SettingsCell)?.setup(model: item)
        return cell
    }
    
    func getSectionName(_ view: UITableView, _ sectionIndex: Int, _ section: SettingsSection) -> String? {
        section.name
    }
    
    func setupTableHeaderAndFooterView() {
        if let headerView = tableView.tableHeaderView {
            updateTableHeaderOrFooterHeight(headerView)
            tableView.tableHeaderView = headerView
        }

        if let footerView = tableView.tableFooterView {
            updateTableHeaderOrFooterHeight(footerView)
            tableView.tableFooterView = footerView
        }

        tableView.layoutIfNeeded()
    }

    func updateTableHeaderOrFooterHeight(_ headerOrFooter: UIView) {
        let targetSize = CGSize(width: tableView.bounds.width, height: UIView.layoutFittingCompressedSize.height)
        let size = headerOrFooter.systemLayoutSizeFitting(targetSize)

        guard headerOrFooter.frame.height != size.height else {
            return
        }

        headerOrFooter.frame.size = CGSize(width: tableView.bounds.width, height: size.height)
    }
}

// MARK: UITableViewDelegate
extension SettingsMainViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        // TODO: Reutilizar action do coordinator
    }
}

// MARK: - SettingsMainDisplaying
extension SettingsMainViewController: SettingsMainDisplaying {
    func displaySomething() { }
    // TODO: Funcao para adicionar items numa section especifica
}
