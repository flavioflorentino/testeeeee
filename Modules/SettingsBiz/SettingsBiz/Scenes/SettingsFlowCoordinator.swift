import UIKit
import UI

public protocol SettingsFlowCoordinating: AnyObject {
    func perform()
    func close()
}

public final class SettingsFlowCoordinator: Coordinating {
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    
    private let navigationController: UINavigationController
    
    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    public func start() {
        let controller = SettingsMainFactory.make()
        
        navigationController.viewControllers = [controller]
    }
}
