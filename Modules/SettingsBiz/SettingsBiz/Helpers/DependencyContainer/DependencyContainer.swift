import Foundation
import AnalyticsModule
import Core

typealias SettingsDependencies = HasNoDependency & HasMainQueue & HasAnalytics

final class DependencyContainer: SettingsDependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var analytics: AnalyticsProtocol = Analytics.shared
}
