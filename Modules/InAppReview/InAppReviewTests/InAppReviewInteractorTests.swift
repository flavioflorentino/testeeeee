import AnalyticsModule
import XCTest
@testable import InAppReview

private final class InAppReviewServiceMock: InAppReviewServicing {
    var tagsList: [String] = []
    
    func getTags() -> [String] {
        tagsList
    }
    
    func sendSelected(tags: [String], rating: Int) {
        print(tagsList)
    }
}

private final class InAppReviewPresenterSpy: InAppReviewPresenting {
    var viewController: InAppReviewDisplay?
    
    private(set) var didNextStepCount = 0
    private(set) var setMutableContentViewInitialConfigurationCount = 0
    private(set) var setNoRatingConfigurationCount = 0
    private(set) var setLowRatingConfigurationCount = 0
    private(set) var setLowRatingFeedbackConfigurationCount = 0
    private(set) var setHighRatingConfigurationCount = 0
    private(set) var setSubmitButtonCount = 0
    
    var isSubmitButtonEnabled = false
    
    func setSubmitButton(title: String, isEnabled: Bool) {
        isSubmitButtonEnabled = isEnabled
        setSubmitButtonCount += 1
    }
    
    func didNextStep(action: InAppReviewAction) {
        didNextStepCount += 1
    }
    
    func setMutableContentViewInitialConfiguration(viewElements: InAppReviewElements, tagsList: [String]) {
        setMutableContentViewInitialConfigurationCount += 1
    }
    
    func setNoRatingConfiguration(viewElements: InAppReviewElements) {
        setNoRatingConfigurationCount += 1
    }
    
    func setLowRatingConfiguration(viewElements: InAppReviewElements, configuredTags: [TagsCollectionViewItem]) {
        setLowRatingConfigurationCount += 1
    }
    
    func setLowRatingFeedbackConfiguration(viewElements: InAppReviewElements) {
        setLowRatingFeedbackConfigurationCount += 1
    }
    
    func setHighRatingConfiguration(viewElements: InAppReviewElements) {
        setHighRatingConfigurationCount += 1
    }
}

final class InAppReviewInteractorTests: XCTestCase {
    private let elementsMock = InAppReviewElements(headerImage: UIImage(),
                                                   title: "MockTitle",
                                                   feedbackTitle: "MockFeedbackTitle",
                                                   subtitle: "MockSubtitle",
                                                   feedbackSubtitle: "MockFeedbackSubtitle")
    private let serviceMock = InAppReviewServiceMock()
    private let presenterSpy = InAppReviewPresenterSpy()
    private let analyticsSpy = AnalyticsSpy()
    private lazy var sut = InAppReviewInteractor(presenter: presenterSpy,
                                                 service: serviceMock,
                                                 viewElements: elementsMock,
                                                 dependencies: DependencyContainerMock(analyticsSpy))
    
    func testSetSubmitButton_ShouldEnable_WithSelectedTagsCountGreaterThan0() {
        serviceMock.tagsList = ["tag1", "tag2", "tag3"]
        sut.configureSelectedTagsIndices(index: 1)
        
        XCTAssertEqual(presenterSpy.setSubmitButtonCount, 1)
        XCTAssertTrue(presenterSpy.isSubmitButtonEnabled)
    }
    
    func testSelectedCellsIndices_ShouldRemoveIndex_IfTryingToAddExistingIndexToArray() {
        serviceMock.tagsList = ["tag1", "tag2", "tag3"]
        sut.configureSelectedTagsIndices(index: 1)
        sut.configureSelectedTagsIndices(index: 2)
        XCTAssertEqual(sut.selectedCellsIndices.count, 2)
        
        sut.configureSelectedTagsIndices(index: 1)
        XCTAssertEqual(sut.selectedCellsIndices.count, 1)
    }
    
    func testStartInitialConfiguration_ShouldCallPresenter_StartMutableContentViewInitialConfig() {
        serviceMock.tagsList = []
        
        sut.startInitialConfiguration()
        
        XCTAssertEqual(presenterSpy.setMutableContentViewInitialConfigurationCount, 1)
    }
    
    func testDidUpdateStarRating_ShouldPresentNoRatingView_WhenRatingIs0() {
        sut.didUpdateStarRating(rating: 0)
        
        XCTAssertEqual(presenterSpy.setNoRatingConfigurationCount, 1)
        XCTAssertEqual(sut.currentStep, .noRatingStep)
    }
    
    func testDidUpdateStarRating_ShouldPresentLowRatingView_WhenRatingIs1() {
        sut.didUpdateStarRating(rating: 1)
        
        XCTAssertEqual(presenterSpy.setLowRatingConfigurationCount, 1)
        XCTAssertEqual(sut.currentStep, .lowRatingStep)
    }
    
    func testDidUpdateStarRating_ShouldPresentLowRatingView_WhenRatingIs2() {
        sut.didUpdateStarRating(rating: 2)
        
        XCTAssertEqual(presenterSpy.setLowRatingConfigurationCount, 1)
        XCTAssertEqual(sut.currentStep, .lowRatingStep)
    }
    
    func testDidUpdateStarRating_ShouldPresentLowRatingView_WhenRatingIs3() {
        sut.didUpdateStarRating(rating: 3)
        
        XCTAssertEqual(presenterSpy.setLowRatingConfigurationCount, 1)
        XCTAssertEqual(sut.currentStep, .lowRatingStep)
    }
    
    func testDidUpdateStarRating_ShouldPresentHighRatingView_WhenRatingIs4() {
        sut.didUpdateStarRating(rating: 4)
        
        XCTAssertEqual(presenterSpy.setHighRatingConfigurationCount, 1)
        XCTAssertEqual(sut.currentStep, .highRatingStep)
    }
    
    func testDidUpdateStarRating_ShouldPresentHighRatingView_WhenRatingIs5() {
        sut.didUpdateStarRating(rating: 5)
        
        XCTAssertEqual(presenterSpy.setHighRatingConfigurationCount, 1)
        XCTAssertEqual(sut.currentStep, .highRatingStep)
    }
    
    func testSubmitButton_ShouldPresentLowRatingFeedbackView_WhenCurrentStepIsLowRating() {
        sut.didUpdateStarRating(rating: 2)
        sut.didTapSubmitButton()
        
        XCTAssertEqual(sut.currentStep, .lowRatingStepFeedback)
        XCTAssertEqual(presenterSpy.setLowRatingFeedbackConfigurationCount, 1)
    }
    
    func testSubmitButton_ShouldDismissCurrentView_WhenCurrentStepIsLowRatingFeedback() {
        sut.didUpdateStarRating(rating: 2)
        sut.didTapSubmitButton()
        XCTAssertEqual(sut.currentStep, .lowRatingStepFeedback)
        
        sut.didTapSubmitButton()
        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
    }
    
    func testDismissButton_ShouldDismissCurrentView() {
        sut.didTapDismissButton()
        
        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
    }
}
