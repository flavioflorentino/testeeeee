import AnalyticsModule
import XCTest
@testable import InAppReview

private final class InAppReviewDisplayableSpy: InAppReviewDisplay {
    private(set) var setMutableContentViewInitialConfigurationCount = 0
    private(set) var setNoRatingViewConfigurationCount = 0
    private(set) var setLowRatingViewConfigurationCount = 0
    private(set) var setLowRatingFeedbackViewConfigurationCount = 0
    private(set) var setHighRatingViewConfigurationCount = 0
    private(set) var setDismissButtonCount = 0
    private(set) var setSubmitButtonCount = 0
    
    private(set) var headerImage = UIImage()
    private(set) var title: String = ""
    private(set) var tagsList: [String] = []
    private(set) var descriptionText: String = ""
    private(set) var feedbackTitle: String = ""
    private(set) var feedbackSubtitle: String = ""
    private(set) var isHidden = false
    private(set) var isEnabled = false
    private(set) var submitButtonTitle: String = ""
    
    func setMutableContentViewInitialConfiguration(headerImage: UIImage, title: String, tagsList: [String]) {
        setMutableContentViewInitialConfigurationCount += 1
        self.headerImage = headerImage
        self.title = title
        self.tagsList = tagsList
    }
    
    func setNoRatingViewConfiguration(viewElements: InAppReviewElements) {
        setNoRatingViewConfigurationCount += 1
    }
    
    func setLowRatingViewConfiguration(descriptionText: String, configuredTags: [TagsCollectionViewItem]) {
        self.descriptionText = descriptionText
        setLowRatingViewConfigurationCount += 1
    }
    
    func setLowRatingFeedbackViewConfiguration(feedbackTitle: String, feedbackSubtitle: String) {
        self.feedbackTitle = feedbackTitle
        self.feedbackSubtitle = feedbackSubtitle
        setLowRatingFeedbackViewConfigurationCount += 1
    }
    
    func setHighRatingViewConfiguration(descriptionText: String) {
        self.descriptionText = descriptionText
        setHighRatingViewConfigurationCount += 1
    }
    
    func setDismissButton(isHidden: Bool) {
        self.isHidden = isHidden
        setDismissButtonCount += 1
    }
    
    func setSubmitButton(_ title: String, _ isEnabled: Bool) {
        self.submitButtonTitle = title
        self.isEnabled = isEnabled
        setSubmitButtonCount += 1
    }
}

private final class InAppReviewCoordinatorSpy: InAppReviewCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformCount = 0
    private(set) var action: InAppReviewAction?
    
    func perform(action: InAppReviewAction) {
        callPerformCount += 1
        self.action = action
    }
}

final class InAppReviewPresenterTests: XCTestCase {
    private let elements = InAppReviewElements(headerImage: Assets.ota.image,
                                               title: "title",
                                               feedbackTitle: "feedback-title",
                                               subtitle: "subtitle",
                                               feedbackSubtitle: "feedback-subtitle")
    private let tagsList = ["mock-tag-1", "mock-tag-2", "mock-tag-3", "mock-tag-4"]
    private let viewControllerSpy = InAppReviewDisplayableSpy()
    private let coordinatorSpy = InAppReviewCoordinatorSpy()
    
    private lazy var sut: InAppReviewPresenting = {
        let sut = InAppReviewPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    func testPresentMutableContentViewInitialConfig_ShouldCallViewControllerWithCorrectImage() {
        sut.setMutableContentViewInitialConfiguration(viewElements: elements, tagsList: tagsList)
        
        XCTAssertEqual(viewControllerSpy.setMutableContentViewInitialConfigurationCount, 1)
        XCTAssertEqual(viewControllerSpy.headerImage, elements.headerImage)
    }
    
    func testPresentMutableContentViewInitialConfig_ShouldCallViewControllerWithCorrectTitle() {
        sut.setMutableContentViewInitialConfiguration(viewElements: elements, tagsList: tagsList)
        
        XCTAssertEqual(viewControllerSpy.setMutableContentViewInitialConfigurationCount, 1)
        XCTAssertEqual(viewControllerSpy.title, elements.title)
    }
    
    func testPresentMutableContentViewInitialConfig_ShouldCallViewControllerWithCorrectTagsList() {
        sut.setMutableContentViewInitialConfiguration(viewElements: elements, tagsList: tagsList)
        
        XCTAssertEqual(viewControllerSpy.setMutableContentViewInitialConfigurationCount, 1)
        XCTAssertEqual(viewControllerSpy.tagsList[1], tagsList[1])
        XCTAssertEqual(viewControllerSpy.tagsList.count, tagsList.count)
    }
    
    func testSetNoRatingViewConfiguration_ShouldCallViewController() {
        sut.setNoRatingConfiguration(viewElements: elements)

        XCTAssertEqual(viewControllerSpy.setNoRatingViewConfigurationCount, 1)
    }
    
    func testSetLowRatingViewConfiguration_ShouldCallViewController_WithCorrectDescriptionText() {
        sut.setLowRatingConfiguration(viewElements: elements, configuredTags: [])
        
        XCTAssertEqual(viewControllerSpy.setLowRatingViewConfigurationCount, 1)
        XCTAssertEqual(viewControllerSpy.descriptionText, Strings.Common.improvementQuestion)
    }
    
    func testSetHighRatingViewConfiguration_ShouldCallViewController_WithCorrectDescriptionText() {
        sut.setHighRatingConfiguration(viewElements: elements)
        
        XCTAssertEqual(viewControllerSpy.setHighRatingViewConfigurationCount, 1)
        XCTAssertEqual(viewControllerSpy.descriptionText, elements.subtitle)
    }
    
    func testSetLowRateFeedbackViewConfiguration_ShouldCallViewController_WithCorrectFeedbackTitleSub() {
        sut.setLowRatingFeedbackConfiguration(viewElements: elements)
        
        XCTAssertEqual(viewControllerSpy.setLowRatingFeedbackViewConfigurationCount, 1)
        XCTAssertEqual(viewControllerSpy.feedbackTitle, elements.feedbackTitle)
        XCTAssertEqual(viewControllerSpy.feedbackSubtitle, elements.feedbackSubtitle)
    }
    
    func testSetLowRatingFeedback_SouldSetDismissButton() {
        sut.setLowRatingFeedbackConfiguration(viewElements: elements)
        
        XCTAssertEqual(viewControllerSpy.setDismissButtonCount, 1)
        XCTAssertTrue(viewControllerSpy.isHidden)
    }
    
    func testSubmitButton_ShouldSetCorrectly_ForNoRatingViewConfiguration() {
        sut.setSubmitButton(title: Strings.Common.ratingButton, isEnabled: false)
        
        XCTAssertEqual(viewControllerSpy.setSubmitButtonCount, 1)
        XCTAssertEqual(viewControllerSpy.submitButtonTitle, Strings.Common.ratingButton)
        XCTAssertFalse(viewControllerSpy.isEnabled)
    }
    
    func testSubmitButton_ShouldSetCorrectly_ForLowRatingViewConfiguration() {
        sut.setSubmitButton(title: Strings.Common.ratingButton, isEnabled: false)
        
        XCTAssertEqual(viewControllerSpy.setSubmitButtonCount, 1)
        XCTAssertEqual(viewControllerSpy.submitButtonTitle, Strings.Common.ratingButton)
        XCTAssertFalse(viewControllerSpy.isEnabled)
    }
    
    func testSubmitButton_ShouldSetCorrectly_ForLowRatingFeedbackViewConfiguration() {
        sut.setSubmitButton(title: Strings.Common.okButton, isEnabled: true)
        
        XCTAssertEqual(viewControllerSpy.setSubmitButtonCount, 1)
        XCTAssertEqual(viewControllerSpy.submitButtonTitle, Strings.Common.okButton)
        XCTAssertTrue(viewControllerSpy.isEnabled)
    }
    
    func testSubmitButton_ShouldSetCorrectly_ForHighRatingViewConfiguration() {
        sut.setSubmitButton(title: Strings.Common.ratingAppStoreButton, isEnabled: true)
        
        XCTAssertEqual(viewControllerSpy.setSubmitButtonCount, 1)
        XCTAssertEqual(viewControllerSpy.submitButtonTitle, Strings.Common.ratingAppStoreButton)
        XCTAssertTrue(viewControllerSpy.isEnabled)
    }
    
    func testSetSubmitButton_ShouldSetSubmitButtonCorrectly_WithDisabledOption() {
        sut.setSubmitButton(title: Strings.Common.ratingButton, isEnabled: true)

        XCTAssertTrue(viewControllerSpy.isEnabled)
        XCTAssertEqual(viewControllerSpy.submitButtonTitle, Strings.Common.ratingButton)
    }
    
    func testSetSubmitButton_ShouldSetSubmitButtonCorrectly_WithEnabledOption() {
        sut.setSubmitButton(title: Strings.Common.ratingButton, isEnabled: false)
        
        XCTAssertFalse(viewControllerSpy.isEnabled)
        XCTAssertEqual(viewControllerSpy.submitButtonTitle, Strings.Common.ratingButton)
    }
}
