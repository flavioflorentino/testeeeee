import AnalyticsModule
import Foundation

typealias TagsCollectionViewItem = (element: String, isSelected: Bool)

protocol InAppReviewInteracting: AnyObject {
    var selectedCellsIndices: [Int] { get }
    var rating: Int { get }
    var currentStep: InAppReviewStep { get }
    var tagsList: [String] { get }
    func startInitialConfiguration()
    func didUpdateStarRating(rating: Int)
    func didTapSubmitButton()
    func didTapDismissButton()
    func configureSelectedTagsIndices(index: Int)
}

final class InAppReviewInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let presenter: InAppReviewPresenting
    private let service: InAppReviewServicing
    private let viewElements: InAppReviewElements
    private(set) var rating: Int
    private(set) var currentStep: InAppReviewStep
    private(set) var tagsList: [String] = []
    private(set) var selectedCellsIndices: [Int] {
        didSet {
            presenter.setSubmitButton(title: Strings.Common.ratingButton,
                                      isEnabled: !selectedCellsIndices.isEmpty)
        }
    }

    init(presenter: InAppReviewPresenting,
         service: InAppReviewServicing,
         viewElements: InAppReviewElements,
         dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.viewElements = viewElements
        self.dependencies = dependencies
        self.currentStep = .noRatingStep
        self.rating = 0
        self.selectedCellsIndices = []
        tagsList = service.getTags()
    }
    
    private func getSelectedElements() -> [TagsCollectionViewItem] {
        let test = tagsList.enumerated().map { selectedCellsIndices.contains($0.offset) }
        return tagsList.enumerated().map { (element: $0.element, isSelected: test[$0.offset]) }
    }
}

// MARK: - InAppReviewInteracting
extension InAppReviewInteractor: InAppReviewInteracting {
    func startInitialConfiguration() {
        presenter.setSubmitButton(title: Strings.Common.ratingButton, isEnabled: false)
        presenter.setMutableContentViewInitialConfiguration(viewElements: viewElements,
                                                            tagsList: tagsList)
    }
    
    func didUpdateStarRating(rating: Int) {
        self.rating = rating
        switch rating {
        case 0:
            currentStep = .noRatingStep
            presenter.setSubmitButton(title: Strings.Common.ratingButton, isEnabled: false)
            presenter.setNoRatingConfiguration(viewElements: viewElements)
        case 1...3:
            currentStep = .lowRatingStep
            presenter.setSubmitButton(title: Strings.Common.ratingButton,
                                      isEnabled: !selectedCellsIndices.isEmpty)
            presenter.setLowRatingConfiguration(viewElements: viewElements,
                                                configuredTags: getSelectedElements())
        case 4...5:
            currentStep = .highRatingStep
            presenter.setSubmitButton(title: Strings.Common.ratingAppStoreButton, isEnabled: true)
            presenter.setHighRatingConfiguration(viewElements: viewElements)
        default:
            break
        }
    }
    
    func configureSelectedTagsIndices(index: Int) {
        guard let thisIndex = selectedCellsIndices.firstIndex(of: index) else {
            selectedCellsIndices.append(index)
            return
        }
        selectedCellsIndices.remove(at: thisIndex)
    }
    
    func didTapSubmitButton() {
        switch currentStep {
        case .lowRatingStep:
            let tags = tagsList.enumerated()
                .filter { selectedCellsIndices.contains($0.offset) }
                .map { $0.element }
            dependencies.analytics.log(InAppReviewEvent.reviewInAppSubmitSelected(rating,
                                                                                  tags,
                                                                                  .reviewInApp))
            currentStep = .lowRatingStepFeedback
            presenter.setSubmitButton(title: Strings.Common.okButton, isEnabled: true)
            presenter.setLowRatingFeedbackConfiguration(viewElements: viewElements)
        case .lowRatingStepFeedback:
            presenter.didNextStep(action: .dismiss)
        case .highRatingStep:
            dependencies.analytics.log(InAppReviewEvent.reviewInAppSubmitSelected(rating,
                                                                                  [],
                                                                                  .sentToAppStore))
        default:
            break
        }
    }
    
    func didTapDismissButton() {
        presenter.didNextStep(action: .dismiss)
    }
}
