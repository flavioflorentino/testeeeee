import UI

protocol InAppReviewPresenting: AnyObject {
    var viewController: InAppReviewDisplay? { get set }
    func didNextStep(action: InAppReviewAction)
    func setMutableContentViewInitialConfiguration(viewElements: InAppReviewElements,
                                                   tagsList: [String])
    func setNoRatingConfiguration(viewElements: InAppReviewElements)
    func setLowRatingConfiguration(viewElements: InAppReviewElements,
                                   configuredTags: [TagsCollectionViewItem])
    func setLowRatingFeedbackConfiguration(viewElements: InAppReviewElements)
    func setHighRatingConfiguration(viewElements: InAppReviewElements)
    func setSubmitButton(title: String, isEnabled: Bool)
}

final class InAppReviewPresenter: InAppReviewPresenting {
    weak var viewController: InAppReviewDisplay?
    private var coordinator: InAppReviewCoordinating
    
    init(coordinator: InAppReviewCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: InAppReviewAction) {
        coordinator.perform(action: action)
    }
    
    func setNoRatingConfiguration(viewElements: InAppReviewElements) {
        viewController?.setNoRatingViewConfiguration(viewElements: viewElements)
    }
    
    func setLowRatingConfiguration(viewElements: InAppReviewElements,
                                   configuredTags: [TagsCollectionViewItem]) {
        viewController?.setLowRatingViewConfiguration(descriptionText: Strings.Common.improvementQuestion,
                                                      configuredTags: configuredTags)
    }
    
    func setLowRatingFeedbackConfiguration(viewElements: InAppReviewElements) {
        viewController?.setDismissButton(isHidden: true)
        viewController?.setLowRatingFeedbackViewConfiguration(feedbackTitle: viewElements.feedbackTitle,
                                                              feedbackSubtitle: viewElements.feedbackSubtitle)
    }
    
    func setHighRatingConfiguration(viewElements: InAppReviewElements) {
        viewController?.setHighRatingViewConfiguration(descriptionText: viewElements.subtitle)
    }
    
    func setMutableContentViewInitialConfiguration(viewElements: InAppReviewElements, tagsList: [String]) {
        viewController?.setMutableContentViewInitialConfiguration(headerImage: viewElements.headerImage, title: viewElements.title, tagsList: tagsList)
    }
    
    func setSubmitButton(title: String, isEnabled: Bool) {
        viewController?.setSubmitButton(title, isEnabled)
    }
}
