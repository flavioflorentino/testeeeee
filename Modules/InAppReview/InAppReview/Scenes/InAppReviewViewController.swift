import Core
import SnapKit
import UI
import UIKit

protocol InAppReviewDisplay: AnyObject {
    func setLowRatingViewConfiguration(descriptionText: String,
                                       configuredTags: [TagsCollectionViewItem])
    func setMutableContentViewInitialConfiguration(headerImage: UIImage,
                                                   title: String,
                                                   tagsList: [String])
    func setNoRatingViewConfiguration(viewElements: InAppReviewElements)
    func setLowRatingFeedbackViewConfiguration(feedbackTitle: String, feedbackSubtitle: String)
    func setHighRatingViewConfiguration(descriptionText: String)
    func setDismissButton(isHidden: Bool)
    func setSubmitButton(_ title: String, _ isEnabled: Bool)
}

final class InAppReviewViewController: ViewController<InAppReviewInteracting, UIView> {
    private lazy var imageView = UIImageView()
    
    private lazy var mutableContentView: MutableContentView = {
        let view = MutableContentView()
        
        view.didUpdateStarRating = { [weak self] rate in
            guard let self = self else { return }
            self.interactor.didUpdateStarRating(rating: rate)
        }
        
        view.didSelectItem = { [weak self] index in
            guard let self = self else { return }
            self.interactor.configureSelectedTagsIndices(index: index)
        }
        
        return  view
    }()
    
    private lazy var submitButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle(size: .default))
        button.addTarget(self, action: #selector(sendRatingButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private lazy var dismissButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(LinkButtonStyle(size: .default))
        button.setTitle(Strings.Common.dismiss, for: .normal)
        button.addTarget(self, action: #selector(didTapNotNowButton), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.startInitialConfiguration()
    }
    
    // MARK: - ViewConfiguration
    override func buildViewHierarchy() {
        view.addSubviews(imageView,
                         mutableContentView,
                         submitButton,
                         dismissButton)
    }
    
    override func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base05)
            $0.centerX.equalToSuperview()
        }
        
        mutableContentView.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base02)
            $0.centerX.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base00)
        }

        submitButton.snp.makeConstraints {
            $0.top.equalTo(mutableContentView.snp.bottom).offset(Spacing.base03)
            $0.centerX.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
            $0.height.equalTo(Spacing.base06)
        }

        dismissButton.snp.makeConstraints {
            $0.top.equalTo(submitButton.snp.bottom).offset(Spacing.base01)
            $0.centerX.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
            $0.height.equalTo(Spacing.base06)
        }
    }
    
    override func configureViews() {
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
}

// MARK: - @OBJC
extension InAppReviewViewController {
    @objc
    private func sendRatingButtonTapped() {
        interactor.didTapSubmitButton()
    }
    
    @objc
    private func didTapNotNowButton() {
        interactor.didTapDismissButton()
    }
}

// MARK: - InAppReviewDisplaying
extension InAppReviewViewController: InAppReviewDisplay {
    func setDismissButton(isHidden: Bool) {
        dismissButton.isHidden = isHidden
    }
    
    func setSubmitButton(_ title: String, _ isEnabled: Bool) {
        submitButton.setTitle(title, for: .normal)
        submitButton.isEnabled = isEnabled
    }
    
    func setMutableContentViewInitialConfiguration(headerImage: UIImage,
                                                   title: String,
                                                   tagsList: [String]) {
        imageView.image = headerImage
        mutableContentView.setInitialView?(title, tagsList)
    }
    
    func setNoRatingViewConfiguration(viewElements: InAppReviewElements) {
        mutableContentView.setNoRateView?()
    }
    
    func setLowRatingViewConfiguration(descriptionText: String,
                                       configuredTags: [TagsCollectionViewItem]) {
        mutableContentView.setLowRatingView?(descriptionText, configuredTags)
    }
    
    func setLowRatingFeedbackViewConfiguration(feedbackTitle: String, feedbackSubtitle: String) {
        mutableContentView.setLowRatingFeedbackView?(feedbackTitle, feedbackSubtitle)
    }
    
    func setHighRatingViewConfiguration(descriptionText: String) {
        mutableContentView.setHighRatingView?(descriptionText)
    }
}
