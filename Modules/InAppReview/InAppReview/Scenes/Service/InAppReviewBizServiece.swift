import Core
import Foundation
import FeatureFlag

// PJ Service
final class InAppReviewBizService {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - InAppReviewServicing
extension InAppReviewBizService: InAppReviewServicing {
    func getTags() -> [String] {
        [""]
    }
    
    func sendSelected(tags: [String], rating: Int) {
        // todo: in the next PR 
    }
}
