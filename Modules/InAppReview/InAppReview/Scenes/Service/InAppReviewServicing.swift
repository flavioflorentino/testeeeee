public protocol InAppReviewServicing {
    func getTags() -> [String]
    func sendSelected(tags: [String], rating: Int)
}
