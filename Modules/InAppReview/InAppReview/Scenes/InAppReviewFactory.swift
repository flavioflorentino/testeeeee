import UIKit

enum InAppReviewFactory {
    static func make(viewElements: InAppReviewElements, service: InAppReviewServicing) -> InAppReviewViewController {
        let container = DependencyContainer()
        let coordinator: InAppReviewCoordinating = InAppReviewCoordinator(dependencies: container)
        let presenter: InAppReviewPresenting = InAppReviewPresenter(coordinator: coordinator)
        let interactor = InAppReviewInteractor(presenter: presenter,
                                               service: service,
                                               viewElements: viewElements,
                                               dependencies: container)
        let viewController = InAppReviewViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
