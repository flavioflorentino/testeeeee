import UI
import UIKit

enum InAppReviewAction {
    case dismiss
}

enum InAppReviewStep {
    case noRatingStep
    case lowRatingStep
    case lowRatingStepFeedback
    case highRatingStep
}

protocol InAppReviewCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: InAppReviewAction)
}

final class InAppReviewCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies
    
    weak var viewController: UIViewController?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - InAppReviewCoordinating
extension InAppReviewCoordinator: InAppReviewCoordinating {
    func perform(action: InAppReviewAction) {
        switch action {
        case .dismiss:
            viewController?.dismiss(animated: true)
        }
    }
}
