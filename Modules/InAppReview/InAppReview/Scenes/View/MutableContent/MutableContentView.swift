import SnapKit
import UI
import UIKit

final class MutableContentView: UIView {
    private lazy var headerInformationStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var starRatingView: StarsRatingView = {
        let view = StarsRatingView()
        view.delegate = self
        return view
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
            .with(\.textAlignment, .center)
            .with(\.text, Strings.Common.improvementQuestion)
        return label
    }()
    
    private lazy var tagsCollectionView = TagListCollectionView()
    
    private lazy var tagsDataSource: CollectionViewDataSource<Int, TagsCollectionViewItem> = {
        let dataSource = CollectionViewDataSource<Int, TagsCollectionViewItem>(view: tagsCollectionView)
        dataSource.itemProvider = { collection, indexPath, item in
            guard let cell = collection.dequeueReusableCell(withReuseIdentifier: TagListCell.identifier,
                                                            for: indexPath) as? TagListCell else {
                return UICollectionViewCell()
            }
            
            cell.setup(text: item.element, isSelected: item.isSelected)
            cell.didSelectItem = { [weak self] in
                guard let self = self else { return }
                self.didSelectItem?(indexPath.row)
            }
            
            return cell
        }
        
        return dataSource
    }()
    
    private var tagsCollectionViewHeightConstraint: Constraint?
    private var descriptionLabelHeightConstraint: Constraint?
    
    var didUpdateStarRating: ((Int) -> Void)?
    var didSelectItem: ((Int) -> Void)?
    
    lazy var setInitialView: ((String, [String]) -> Void)? = { title, tagsList in
        self.titleLabel.text = title
        self.initDataSource(tagsList: tagsList)
    }
    
    lazy var setNoRateView: (() -> Void)? = {
        self.configureNoRateViews()
    }
    
    lazy var setLowRatingView: ((String, [TagsCollectionViewItem]) -> Void)? = { description, configuredTags in
        self.configureLowRateViews(description)
        self.tagsDataSource.update(items: configuredTags, from: 0)
    }
    
    lazy var setLowRatingFeedbackView: ((String, String) -> Void)? = { title, subtitle in
        self.configureLowRateFeedbackViews(title, subtitle)
    }
    
    lazy var setHighRatingView: ((String) -> Void)? = { description in
        self.configureHighRateViews(description)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
        tagsCollectionView.dataSource = tagsDataSource
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initDataSource(tagsList: [String]) {
        tagsDataSource.add(items: tagsList.map { TagsCollectionViewItem($0, false) }, to: 0)
        tagsCollectionView.layoutIfNeeded()
    }
    
    // MARK: - Rating Views Configuration
    private func configureNoRateViews() {
        tagsCollectionViewHeightConstraint?.activate()
        descriptionLabelHeightConstraint?.activate()
        
        descriptionLabel.snp.updateConstraints {
            $0.height.equalTo(0)
        }
    }
    
    private func configureLowRateViews(_ descriptionText: String) {
        descriptionLabelHeightConstraint?.deactivate()
        tagsCollectionViewHeightConstraint?.deactivate()
        descriptionLabel.text = descriptionText
    }
    
    private func configureLowRateFeedbackViews(_ feedbackTitle: String, _ feedbackSubtitle: String) {
        descriptionLabelHeightConstraint?.deactivate()
        tagsCollectionViewHeightConstraint?.activate()
        
        titleLabel.text = feedbackTitle
        descriptionLabel.text = feedbackSubtitle
        starRatingView.isHidden = true
    }
    
    private func configureHighRateViews(_ descriptionText: String) {
        descriptionLabelHeightConstraint?.deactivate()
        tagsCollectionViewHeightConstraint?.activate()
        descriptionLabel.text = descriptionText
    }
}

extension MutableContentView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(headerInformationStackView,
                    descriptionLabel,
                    tagsCollectionView)
        
        headerInformationStackView.addArrangedSubviews(titleLabel,
                                                       starRatingView)
    }

    func setupConstraints() {
        headerInformationStackView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.centerX.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(headerInformationStackView.snp.bottom).offset(Spacing.base03)
            $0.centerX.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            descriptionLabelHeightConstraint = $0.height.equalTo(0).constraint
        }
        
        tagsCollectionView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base02)
            $0.centerX.leading.trailing.equalToSuperview()
            tagsCollectionViewHeightConstraint = $0.height.equalTo(0).constraint
            $0.bottom.greaterThanOrEqualToSuperview()
        }
    }
}

// MARK: - StarRatingViewDelegate
extension MutableContentView: StarsRatingViewDelegate {
    func didTapStar(rating: Int) {
        didUpdateStarRating?(rating)
    }
}
