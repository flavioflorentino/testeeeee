import Foundation

public struct InAppReviewElements {
    let headerImage: UIImage
    let title: String
    let feedbackTitle: String
    let subtitle: String
    let feedbackSubtitle: String
    
    public init(headerImage: UIImage, title: String, feedbackTitle: String, subtitle: String, feedbackSubtitle: String) {
        self.headerImage = headerImage
        self.title = title
        self.feedbackTitle = feedbackTitle
        self.subtitle = subtitle
        self.feedbackSubtitle = feedbackSubtitle
    }
}
