import Foundation

// swiftlint:disable convenience_type
final class InAppReviewResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: InAppReviewResources.self).url(forResource: "InAppReviewResources", withExtension: "bundle") else {
            return Bundle(for: InAppReviewResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: InAppReviewResources.self)
    }()
}
