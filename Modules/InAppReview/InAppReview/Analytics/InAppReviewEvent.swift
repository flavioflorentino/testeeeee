import AnalyticsModule

enum InAppReviewEventSubmitType: String {
    case sentToAppStore = "sent_to_app_store"
    case reviewInApp = "review_in_app"
}

enum InAppReviewEvent: AnalyticsKeyProtocol {
    case reviewInAppSubmitSelected(_ rating: Int,
                                   _ tags: [String],
                                   _ type: InAppReviewEventSubmitType)
    
    private var name: String {
        "Review in App Submit Selected"
    }
    
    private var properties: [String: Any] {
        if case let .reviewInAppSubmitSelected(rating, tags, type) = self {
            return [
                "review_number": rating,
                "tags": tags,
                "type": type.rawValue
            ]
        }
        return [:]
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
