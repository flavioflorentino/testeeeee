import UI
import UIKit

public final class InAppReviewFlowCoordinator: Coordinating {
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    
    public typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies
    private let originViewController: UIViewController
    
    public init(originViewController: UIViewController, dependencies: Dependencies? = nil) {
        self.originViewController = originViewController
        self.dependencies = dependencies ?? DependencyContainer()
    }
    
    public func start(service: InAppReviewServicing, viewElements: InAppReviewElements) {
        let controller = InAppReviewFactory.make(viewElements: viewElements, service: service)
        originViewController.present(controller, animated: true)
    }
}
