import AnalyticsModule
import Core
import FeatureFlag

public protocol HasNoDependency { }

typealias InAppReviewDependencies =
    HasNoDependency &
    HasAnalytics &
    HasMainQueue &
    HasFeatureManager

final class DependencyContainer: InAppReviewDependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
}
