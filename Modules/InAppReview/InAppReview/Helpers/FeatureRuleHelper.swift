public enum FeatureRuleHelper {
    public static func shouldAskForReview(days: Int) -> Bool {
        guard let lastReviewDate = UserDefaults.standard.object(forKey: "lastDateInAppReview") as? Date else {
            return true
        }
        
        guard let estimatedDate = Calendar.current.date(byAdding: .day, value: days, to: Date()) else {
            return false
        }
        
        return lastReviewDate < estimatedDate
    }
}
