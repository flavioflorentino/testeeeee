import UI
import UIKit
import AccountManagementPJ

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            let navigationController = StyledNavigationController()
            window.rootViewController = navigationController
            self.window = window
            window.makeKeyAndVisible()
        }
    }
}

struct Wrapper: AccountUserDataCoordinatorWrapping {
    func navigateToName() { }
    func navigateToEmail() { }
    func navigateToPhone() { }
    func navigateToAddress() { }
    func navigateToCompanyAddress() { }
}
