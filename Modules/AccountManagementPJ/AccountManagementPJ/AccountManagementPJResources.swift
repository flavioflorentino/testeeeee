import Foundation

// swiftlint:disable convenience_type
final class AccountManagementPJResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: AccountManagementPJResources.self).url(forResource: "AccountManagementPJResources", withExtension: "bundle") else {
            return Bundle(for: AccountManagementPJResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: AccountManagementPJResources.self)
    }()
}
