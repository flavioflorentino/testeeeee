import Core
import CustomerSupport
import FeatureFlag
import Foundation

typealias Dependencies =
    HasFeatureManager &
    HasMainQueue &
    HasZendesk

final class DependencyContainer: Dependencies {
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var mainQueue = DispatchQueue.main
    lazy var zendeskContainer = CustomerSupportManager.shared.thirdPartyInstance
}
