import Foundation

private extension Notification.Name {
    enum UserData {
        public static let updated = Notification.Name(rawValue: "UserDataHasUpdated")
    }
}

public enum AccountUserDataNotification {
    private typealias UserDataNotification = Notification.Name.UserData
    
    static func addObserver(_ observer: Any, selector: Selector) {
        NotificationCenter.default.addObserver(observer, selector: selector, name: UserDataNotification.updated, object: nil)
    }
    
    static func removeObserver(_ observer: Any) {
        NotificationCenter.default.removeObserver(observer)
    }
    
    public static func updated(_ action: AccountUserDataAction) {
        NotificationCenter.default.post(name: UserDataNotification.updated, object: nil, userInfo: ["action": action])
    }
}
