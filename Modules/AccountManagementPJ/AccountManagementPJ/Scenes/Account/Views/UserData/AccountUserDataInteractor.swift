import FeatureFlag
import Foundation

public struct AccountUserDataViewModel: Equatable {
    let admin: String
    let email: String
    let phone: String?
    let address: String?
    let companyAddress: String?
    
    init(account: Account) {
        admin = account.adminName
        email = account.email
        
        let phones: [String?] = [account.phones.companyPhone, account.phones.mobilePhone]
        let result = phones.compactMap({ $0 }).joined(separator: "\n")
        phone = result.isEmpty ? nil : result
        
        address = account.partnerAddress?.addressFormatted
        companyAddress = account.companyAddress?.addressFormatted
    }
}

protocol AccountUserDataInteracting: AnyObject {
    func loadUserData()
    func didSelectCell(indexPath: IndexPath)
}

final class AccountUserDataInteractor {
    typealias Dependencies = HasFeatureManager
    private typealias Localizable = Strings.Account.UserData
    
    private let dependencies: Dependencies
    private let presenter: AccountUserDataPresenting
    private let viewModel: AccountUserDataViewModel
    private var userDataCellModels: [AccountUserDataCellViewModel] = []

    init(presenter: AccountUserDataPresenting, viewModel: AccountUserDataViewModel, dependencies: Dependencies) {
        self.presenter = presenter
        self.viewModel = viewModel
        self.dependencies = dependencies

        AccountUserDataNotification.addObserver(self, selector: #selector(didReceiveUpdates(_:)))
    }
    
    deinit {
        AccountUserDataNotification.removeObserver(self)
    }
}

// MARK: - AccountUserDataInteracting

extension AccountUserDataInteractor: AccountUserDataInteracting {
    func loadUserData() {
        var array = [
            AccountUserDataCellViewModel(title: Localizable.admin, value: viewModel.admin, action: .admin, cellType: .defaultCell),
            AccountUserDataCellViewModel(title: Localizable.email, value: viewModel.email, action: .email, cellType: .defaultCell)
        ]
        
        let phoneCell = cell(for: Localizable.phone, value: viewModel.phone, action: .phone)
        array.append(phoneCell)
        
        if dependencies.featureManager.isActive(.isAccountInfoRenewalAvailable) {
            let addressCell = cell(for: Localizable.address, value: viewModel.address, action: .address)
            let companyAddressCell = cell(for: Localizable.companyAddress, value: viewModel.companyAddress, action: .companyAddress)
            array.append(contentsOf: [addressCell, companyAddressCell])
        }
        
        userDataCellModels.append(contentsOf: array)
        presenter.present(userData: userDataCellModels)
    }
    
    func didSelectCell(indexPath: IndexPath) {
        let cellModel = userDataCellModels[indexPath.row]
        presenter.didNextStep(action: cellModel.action)
    }
}

// MARK: - Private Methods

private extension AccountUserDataInteractor {
    func cell(for title: String, value: String?, action: AccountUserDataAction) -> AccountUserDataCellViewModel {
        guard let value = value else {
            return AccountUserDataCellViewModel(title: title, value: Localizable.empty, action: action, cellType: .emptyCell)
        }
        return AccountUserDataCellViewModel(title: title, value: value, action: action, cellType: .defaultCell)
    }
}

@objc
extension AccountUserDataInteractor {
    func didReceiveUpdates(_ notification: Notification) {
        guard let action = notification.userInfo?["action"] as? AccountUserDataAction else {
            return 
        }

        switch action {
        case .admin:
            presenter.feedback(Localizable.updatedAdmin)
        case .email:
            presenter.feedback(Localizable.updatedEmail)
        case .phone:
            presenter.feedback(Localizable.updatedPhone)
        case .address:
            presenter.feedback(Localizable.updatedAddress)
        case .companyAddress:
            presenter.feedback(Localizable.updatedCompanyAddress)
        }
    }
}
