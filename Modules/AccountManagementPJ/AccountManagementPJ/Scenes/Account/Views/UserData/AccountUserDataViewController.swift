import SnapKit
import UI
import UIKit

protocol AccountUserDataDisplaying: AnyObject {
    func display(userData: [AccountUserDataCellViewModel])
    func feedback(_ message: String)
}

private extension AccountUserDataViewController.Layout {
    enum TableView {
        static let estimatedHeight: CGFloat = 140
    }
}

final class AccountUserDataViewController: ViewController<AccountUserDataInteracting, UIView> {
    fileprivate enum Layout { }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.Account.UserData.title
        label.labelStyle(BodyPrimaryLabelStyle())
        return label
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .singleLine
        tableView.separatorInset = .zero
        tableView.isScrollEnabled = false
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Layout.TableView.estimatedHeight
        tableView.delegate = self
        tableView.register(cellType: AccountUserDataCell.self)
        tableView.register(cellType: AccountUserDataEmptyCell.self)
        return tableView
    }()
    
    private lazy var dataSource: TableViewDataSource<Int, AccountUserDataCellViewModel> = {
        let dataSource = TableViewDataSource<Int, AccountUserDataCellViewModel>(view: tableView)
        dataSource.add(section: 0)
        dataSource.itemProvider = { tableView, indexPath, cellViewModel -> AccountUserDataCellProtocol? in
            let cellIdentifier = cellViewModel.cellType.rawValue
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? AccountUserDataCellProtocol
            cell?.setup(viewModel: cellViewModel)
            return cell
        }
        return dataSource
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.loadUserData()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().priority(.low)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        tableView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.bottom.equalToSuperview()
            $0.height.equalTo(0)
        }
    }

    override func configureViews() {
        tableView.dataSource = dataSource
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let height = tableView.contentSize.height
        tableView.snp.updateConstraints {
            $0.height.equalTo(height)
        }
    }
}

// MARK: UITableViewDelegate

extension AccountUserDataViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        interactor.didSelectCell(indexPath: indexPath)
    }
}

// MARK: - AccountUserDataDisplaying

extension AccountUserDataViewController: AccountUserDataDisplaying {
    func display(userData: [AccountUserDataCellViewModel]) {
        dataSource.remove(section: 0)
        dataSource.add(items: userData, to: 0)
    }

    func feedback(_ message: String) {
        let snackbar = ApolloSnackbar(text: message, iconType: .success, emphasis: .high)
        showSnackbar(snackbar, onTopOf: navigationController?.view)
    }
}
