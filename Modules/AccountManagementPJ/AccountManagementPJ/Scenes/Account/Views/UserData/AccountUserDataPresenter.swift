import Foundation

protocol AccountUserDataPresenting: AnyObject {
    var viewController: AccountUserDataDisplaying? { get set }
    func present(userData: [AccountUserDataCellViewModel])
    func didNextStep(action: AccountUserDataAction)
    func feedback(_ message: String)
}

final class AccountUserDataPresenter {
    private let coordinator: AccountUserDataCoordinating
    weak var viewController: AccountUserDataDisplaying?

    init(coordinator: AccountUserDataCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - AccountUserDataPresenting
extension AccountUserDataPresenter: AccountUserDataPresenting {
    func present(userData: [AccountUserDataCellViewModel]) {
        viewController?.display(userData: userData)
    }
    
    func didNextStep(action: AccountUserDataAction) {
        coordinator.perform(action: action)
    }

    func feedback(_ message: String) {
        viewController?.feedback(message)
    }
}
