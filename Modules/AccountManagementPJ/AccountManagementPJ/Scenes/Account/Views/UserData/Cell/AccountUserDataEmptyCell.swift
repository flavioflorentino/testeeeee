import SnapKit
import UI
import UIKit

final class AccountUserDataEmptyCell: UITableViewCell {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
        label.textColor = Colors.grayscale750.color
        return label
    }()
    
    private lazy var warningIcon: UILabel = {
        let label = UILabel()
        label.text = Iconography.exclamationTriangle.rawValue
        label.labelStyle(IconLabelStyle(type: .small))
        label.textColor = Colors.warning600.color
        label.size = CGSize(width: Sizing.base02, height: Sizing.base02)
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle())
        return label
    }()
    
    private lazy var customAccessoryView: UIView = {
        let label = UILabel()
        label.text = Iconography.angleRight.rawValue
        label.labelStyle(IconLabelStyle(type: .medium))
        label.textColor = Colors.branding600.color
        label.size = CGSize(width: Sizing.base02, height: Sizing.base02)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) { nil }
}

// MARK: - View Configuration

extension AccountUserDataEmptyCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(warningIcon)
        contentView.addSubview(valueLabel)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        warningIcon.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalTo(titleLabel)
            $0.bottom.equalToSuperview().inset(Spacing.base02)
        }
        
        valueLabel.snp.makeConstraints {
            $0.centerY.equalTo(warningIcon)
            $0.leading.equalTo(warningIcon.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalTo(titleLabel)
        }
    }
    
    func configureViews() {
        accessoryView = customAccessoryView
        accessoryView?.isUserInteractionEnabled = false
    }
}

// MARK: - AccountUserDataCellProtocol

extension AccountUserDataEmptyCell: AccountUserDataCellProtocol {
    func setup(viewModel: AccountUserDataCellViewModel) {
        titleLabel.text = viewModel.title
        valueLabel.text = viewModel.value
    }
}
