import SnapKit
import UI
import UIKit

struct AccountUserDataCellViewModel: Equatable {
    enum CellType: String {
        case defaultCell
        case emptyCell
        
        var rawValue: String {
            switch self {
            case .defaultCell:
                return AccountUserDataCell.identifier
            case .emptyCell:
                return AccountUserDataEmptyCell.identifier
            }
        }
    }
    let title: String
    let value: String
    let action: AccountUserDataAction
    let cellType: CellType
}

protocol AccountUserDataCellProtocol: UITableViewCell {
    func setup(viewModel: AccountUserDataCellViewModel)
}

final class AccountUserDataCell: UITableViewCell {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
        label.textColor = Colors.grayscale750.color
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        return label
    }()
    
    private lazy var customAccessoryView: UIView = {
        let label = UILabel()
        label.text = Iconography.angleRight.rawValue
        label.labelStyle(IconLabelStyle(type: .medium))
        label.textColor = Colors.branding600.color
        label.size = CGSize(width: Sizing.base02, height: Sizing.base02)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) { nil }
}

// MARK: - View Configuration

extension AccountUserDataCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(valueLabel)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        valueLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom)
            $0.leading.trailing.equalTo(titleLabel)
            $0.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        accessoryView = customAccessoryView
        accessoryView?.isUserInteractionEnabled = false
    }
}

// MARK: - AccountUserDataCellProtocol

extension AccountUserDataCell: AccountUserDataCellProtocol {
    func setup(viewModel: AccountUserDataCellViewModel) {
        titleLabel.text = viewModel.title
        valueLabel.text = viewModel.value
    }
}
