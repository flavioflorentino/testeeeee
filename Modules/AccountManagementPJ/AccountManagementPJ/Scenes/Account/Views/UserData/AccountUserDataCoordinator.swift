import UIKit

public protocol AccountUserDataCoordinatorWrapping {
    func navigateToName()
    func navigateToEmail()
    func navigateToPhone()
    func navigateToAddress()
    func navigateToCompanyAddress()
}

public enum AccountUserDataAction: Equatable {
    case admin
    case email
    case phone
    case address
    case companyAddress
}

protocol AccountUserDataCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: AccountUserDataAction)
}

final class AccountUserDataCoordinator {
    weak var viewController: UIViewController?
    
    private let wrapper: AccountUserDataCoordinatorWrapping
    
    init(wrapper: AccountUserDataCoordinatorWrapping) {
        self.wrapper = wrapper
    }
}

// MARK: - AccountUserDataCoordinating
extension AccountUserDataCoordinator: AccountUserDataCoordinating {
    func perform(action: AccountUserDataAction) {
        switch action {
        case .admin:
            wrapper.navigateToName()
        case .email:
            wrapper.navigateToEmail()
        case .phone:
            wrapper.navigateToPhone()
        case .address:
            wrapper.navigateToAddress()
        case .companyAddress:
            wrapper.navigateToCompanyAddress()
        }
    }
}
