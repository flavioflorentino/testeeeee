import UIKit

enum AccountUserDataFactory {
    static func make(
        viewModel: AccountUserDataViewModel,
        wrapper: AccountUserDataCoordinatorWrapping
    ) -> AccountUserDataViewController {
        let dependencies = DependencyContainer()
        let coordinator: AccountUserDataCoordinating = AccountUserDataCoordinator(wrapper: wrapper)
        let presenter: AccountUserDataPresenting = AccountUserDataPresenter(coordinator: coordinator)
        let interactor = AccountUserDataInteractor(presenter: presenter, viewModel: viewModel, dependencies: dependencies)
        let viewController = AccountUserDataViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
