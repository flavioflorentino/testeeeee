import SnapKit
import UI
import UIKit

protocol AccountCompanyDataViewDelegate: AnyObject {
    func chatLinkTapped()
}

public struct AccountCompanyDataViewModel: Equatable {
    let name: String
    let cnpj: String
    
    init(account: Account) {
        name = account.companyName
        cnpj = account.companyDocument
    }
}

final class AccountCompanyDataView: UIView {
    private typealias Localizable = Strings.Account.CompanyData
    
    private lazy var companyIconLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, Colors.grayscale950.color)
        label.text = Iconography.building.rawValue
        return label
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.title
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, Colors.grayscale950.color)
        return label
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.name
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale750.color)
        return label
    }()
    
    private lazy var nameValueLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale750.color)
        return label
    }()
        
    private lazy var companyNameStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [nameLabel, nameValueLabel])
        stack.axis = .vertical

        return stack
    }()
    
    private lazy var cnpjLabel: UILabel = {
        let label = UILabel()
        label.text = Localizable.cnpj
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale750.color)
        return label
    }()
    
    private lazy var cnpjValueLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale750.color)
        return label
    }()
        
    private lazy var cnpjStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [cnpjLabel, cnpjValueLabel])
        stack.axis = .vertical

        return stack
    }()
    
    private lazy var attrText: NSAttributedString = {
        let text = NSMutableAttributedString()
        
        let attributes: [NSAttributedString.Key: Any] = [
            .font: Typography.bodySecondary().font(),
            .foregroundColor: Colors.grayscale800.color
        ]
        
        var linkAttributes: [NSAttributedString.Key: Any] = [
            .link: "",
            .font: Typography.linkSecondary().font(),
            .underlineStyle: NSUnderlineStyle.single.rawValue,
            .foregroundColor: Colors.branding600.color
        ]
                
        text.append(NSAttributedString(string: Localizable.dataText, attributes: attributes))
        text.append(NSAttributedString(string: Localizable.dataLinkText, attributes: linkAttributes))
        return text
    }()
    
    private lazy var textView: UITextView = {
        let textView = UITextView()
        textView.textContainerInset = UIEdgeInsets.zero
        textView.textContainer.lineFragmentPadding = 0
        textView.isUserInteractionEnabled = true
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.attributedText = attrText
        textView.tintColor = Colors.branding600.color
        textView.delegate = self
        return textView
    }()
    
    weak var delegate: AccountCompanyDataViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration

extension AccountCompanyDataView: ViewConfiguration {
    func configureViews() {
        layer.cornerRadius = CornerRadius.medium
        layer.borderWidth = 1
        layer.borderColor = Colors.grayscale100.color.cgColor
        clipsToBounds = true
    }
    
    func buildViewHierarchy() {
        addSubviews(companyIconLabel, titleLabel, companyNameStackView, cnpjStackView, textView)
    }
    
    func setupConstraints() {
        companyIconLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }
        
        titleLabel.snp.makeConstraints {
            $0.leading.equalTo(companyIconLabel.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.centerY.equalTo(companyIconLabel)
        }
        
        companyNameStackView.snp.makeConstraints {
            $0.top.equalTo(companyIconLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        cnpjStackView.snp.makeConstraints {
            $0.top.equalTo(companyNameStackView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        textView.snp.makeConstraints {
            $0.top.equalTo(cnpjStackView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base03)
        }
    }
}

// MARK: - Internal Methods

extension AccountCompanyDataView {
    func setup(viewModel: AccountCompanyDataViewModel) {
        nameValueLabel.text = viewModel.name
        cnpjValueLabel.text = viewModel.cnpj
    }
}

extension AccountCompanyDataView: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        delegate?.chatLinkTapped()
        return false
    }
}
