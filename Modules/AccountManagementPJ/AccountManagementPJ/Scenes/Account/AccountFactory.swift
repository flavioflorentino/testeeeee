import UIKit

enum AccountFactory {
    static func make(userDataCoordinatorWrapper: AccountUserDataCoordinatorWrapping) -> AccountViewController {
        let dependencies = DependencyContainer()
        let service: AccountServicing = AccountService(dependencies: dependencies)
        let coordinator: AccountCoordinating = AccountCoordinator(dependencies: dependencies)
        let presenter: AccountPresenting = AccountPresenter(coordinator: coordinator)
        let interactor = AccountInteractor(service: service, presenter: presenter, dependencies: dependencies)
        let viewController = AccountViewController(
            interactor: interactor,
            userDataCoordinatorWrapper: userDataCoordinatorWrapper
        )

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
