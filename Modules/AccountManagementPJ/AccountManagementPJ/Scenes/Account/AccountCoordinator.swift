import CustomerSupport
import UIKit

enum AccountAction: Equatable {
    case faq(id: String)
    case chat
}

protocol AccountCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: AccountAction)
}

final class AccountCoordinator {
    typealias Dependencies = HasZendesk
    
    weak var viewController: UIViewController?
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - AccountCoordinating
extension AccountCoordinator: AccountCoordinating {
    func perform(action: AccountAction) {
        switch action {
        case .chat:
            guard let chatController = dependencies.zendeskContainer?.buildTicketListController() else { return }
            let navigationController = UINavigationController(rootViewController: chatController)
            viewController?.present(navigationController, animated: true)
        case .faq(let id):
            guard let faqController = dependencies.zendeskContainer?.buildFAQController(option: .section(id: id)) else { return }
            let navigationController = UINavigationController(rootViewController: faqController)
            viewController?.present(navigationController, animated: true)
        }
    }
}
