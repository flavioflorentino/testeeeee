import UI
import UIKit

protocol AccountDisplaying: AnyObject {
    func displayUserData(viewModel: AccountUserDataViewModel)
    func displayCompanyData(viewModel: AccountCompanyDataViewModel)
}

final class AccountViewController: ViewController<AccountInteracting, UIView> {
    private typealias Localizable = Strings.Account
    
    private lazy var contentView = UIView()
    private lazy var scrollView = UIScrollView()
    private lazy var userContentView = UIView()
    private lazy var companyContentView = UIView()
    private let userDataCoordinatorWrapper: AccountUserDataCoordinatorWrapping
    
    private lazy var faqButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.CompanyData.helpCenterLink, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(faqButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private var userDataViewController: AccountUserDataViewController?
    
    init(interactor: AccountInteracting, userDataCoordinatorWrapper: AccountUserDataCoordinatorWrapping) {
        self.userDataCoordinatorWrapper = userDataCoordinatorWrapper
        super.init(interactor: interactor)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadUserData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        userDataViewController?.viewDidLayoutSubviews()
    }

    override func buildViewHierarchy() {
        contentView.addSubviews(userContentView, companyContentView, faqButton)
        scrollView.addSubview(contentView)
        view.addSubview(scrollView)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.width.equalTo(view)
        }
        
        userContentView.snp.makeConstraints {
            $0.top.equalTo(contentView.compatibleSafeArea.top).inset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
        }
        
        companyContentView.snp.makeConstraints {
            $0.top.equalTo(userContentView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        faqButton.snp.makeConstraints {
            $0.top.equalTo(companyContentView.snp.bottom).offset(Spacing.base04)
            $0.centerX.equalToSuperview()
            $0.bottom.equalToSuperview().inset(Spacing.base03)
        }
    }

    override func configureViews() {
        title = Localizable.title
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - AccountDisplaying
extension AccountViewController: AccountDisplaying {
    func displayUserData(viewModel: AccountUserDataViewModel) {
        let userDataViewController = AccountUserDataFactory.make(
            viewModel: viewModel,
            wrapper: userDataCoordinatorWrapper
        )
        
        addChild(userDataViewController)
        userContentView.addSubview(userDataViewController.view)
        userDataViewController.didMove(toParent: self)
        
        userDataViewController.view.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        self.userDataViewController = userDataViewController
    }
    
    func displayCompanyData(viewModel: AccountCompanyDataViewModel) {
        let accountCompanyDataView = AccountCompanyDataView()
        accountCompanyDataView.setup(viewModel: viewModel)
        companyContentView.addSubview(accountCompanyDataView)
        
        accountCompanyDataView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        accountCompanyDataView.delegate = self
    }
}

extension AccountViewController: AccountCompanyDataViewDelegate {
    func chatLinkTapped() {
        interactor.openChat()
    }
}

private extension AccountViewController {
    @objc
    func faqButtonTapped() {
        interactor.openFaq()
    }
}

extension AccountViewController: StyledNavigationDisplayable {
    var navigationBarStyle: DefaultNavigationStyle {
        LargeNavigationStyle()
    }
}
