import FeatureFlag
import Foundation

protocol AccountInteracting: AnyObject {
    func loadUserData()
    func openFaq()
    func openChat()
}

final class AccountInteractor {
    private typealias Localizable = Strings.Account.UserData
    typealias Dependencies = HasFeatureManager
    
    private let service: AccountServicing
    private let presenter: AccountPresenting
    private let dependencies: Dependencies
    
    init(service: AccountServicing, presenter: AccountPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - AccountInteracting
extension AccountInteractor: AccountInteracting {
    func loadUserData() {
        service.fetchAccount { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let account):
                let userViewModel = AccountUserDataViewModel(account: account)
                let companyViewModel = AccountCompanyDataViewModel(account: account)
                self.presenter.presentUserData(viewModel: userViewModel)
                self.presenter.presentCompanyData(viewModel: companyViewModel)
                
            case .failure(let error):
                // Present Error
                print(error)
            }
        }
    }
    
    func openFaq() {
        let id = dependencies.featureManager.text(.faqSectionAccountManagement)
        presenter.didNextStep(action: .faq(id: id))
    }
    
    func openChat() {
        presenter.didNextStep(action: .chat)
    }
}
