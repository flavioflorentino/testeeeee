import Core
import Foundation
import NetworkPJ

protocol AccountServicing {
    func fetchAccount(completion: @escaping(Result<Account, ApiError>) -> Void)
}

struct AccountService {
    typealias Dependencies = HasMainQueue
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - AccountServicing
extension AccountService: AccountServicing {
    func fetchAccount(completion: @escaping(Result<Account, ApiError>) -> Void) {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        BizApi<Account>(endpoint: AccountEndpoint.fetchAccount).execute(jsonDecoder: decoder) { result in
            self.dependencies.mainQueue.async {
                BizApiHelper.unwrapBizResult(result: result, completion: completion)
            }
        }
    }
}
