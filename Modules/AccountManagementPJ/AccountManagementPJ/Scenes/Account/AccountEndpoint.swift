import Core
import Foundation

enum AccountEndpoint {
    case fetchAccount
}

extension AccountEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .fetchAccount:
            return "/v2/seller/registration-info"
        }
    }
}
