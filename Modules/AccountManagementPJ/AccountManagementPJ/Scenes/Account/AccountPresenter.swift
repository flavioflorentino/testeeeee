import Foundation

protocol AccountPresenting: AnyObject {
    var viewController: AccountDisplaying? { get set }
    func presentUserData(viewModel: AccountUserDataViewModel)
    func presentCompanyData(viewModel: AccountCompanyDataViewModel)
    func didNextStep(action: AccountAction)
}

final class AccountPresenter {
    private let coordinator: AccountCoordinating
    weak var viewController: AccountDisplaying?

    init(coordinator: AccountCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - AccountPresenting
extension AccountPresenter: AccountPresenting {
    func presentUserData(viewModel: AccountUserDataViewModel) {
        viewController?.displayUserData(viewModel: viewModel)
    }
    
    func presentCompanyData(viewModel: AccountCompanyDataViewModel) {
        viewController?.displayCompanyData(viewModel: viewModel)
    }
    
    func didNextStep(action: AccountAction) {
        coordinator.perform(action: action)
    }
}
