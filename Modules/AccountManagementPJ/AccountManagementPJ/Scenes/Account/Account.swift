import Foundation

struct Account: Decodable {
    let renovationHistory: RenovationHistory
    let adminName: String
    let email: String
    let partnerAddress: Address?
    let companyAddress: Address?
    let companyName: String
    let companyDocument: String
    let phones: Phones
}

struct RenovationHistory: Decodable {
    let needUpdate: Bool
}

struct Address: Decodable {
    let address: String
    let city: String
    let district: String
    let uf: String
    let postalCode: String
    let number: String
    let addressFormatted: String
    let complement: String?
}

struct Phones: Decodable {
    let companyPhone: String?
    let mobilePhone: String?
}
