import UI
import UIKit

public enum FlowCoordinator {
    public static func start(
        navigationController: UINavigationController,
        userDataCoordinatorWrapper: AccountUserDataCoordinatorWrapping
    ) {
        let accountViewController = AccountFactory.make(userDataCoordinatorWrapper: userDataCoordinatorWrapper)
        accountViewController.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(accountViewController, animated: true)
    }
}

public extension FlowCoordinator {
    static func popToUserData(_ navigationController: UINavigationController?) {
        guard let navigation = navigationController else { return }
        
        if let vc = navigation.viewControllers.first(where: { $0 is AccountViewController }) {
            navigation.popToViewController(vc, animated: true)
        }
    }
}
