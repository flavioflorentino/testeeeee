import CustomerSupport
import FeatureFlag
import XCTest

@testable import AccountManagementPJ

final class DependencyContainerMock: Dependencies {
    lazy var featureManager: FeatureManagerContract = resolve()
    lazy var zendeskContainer: ThirdPartySupportSDKContract? = resolve()
    lazy var mainQueue: DispatchQueue = resolve()
    
    private let dependencies: [Any]
    
    init(_ dependencies: Any...) {
        self.dependencies = dependencies
    }
}

extension DependencyContainerMock {
    func resolve<T>() -> T {
        let resolved = dependencies.compactMap { $0 as? T }
                
        switch resolved.first {
        case .none:
            fatalError("DependencyContainerMock could not resolve dependency: \(T.self)\n")
        case .some where resolved.count > 1:
            fatalError("DependencyContainerMock resolved multiple dependencies for: \(T.self)\n")
        case .some(let mock):
            return mock
        }
    }
    
    func resolve() -> FeatureManagerContract {
        let resolved = dependencies.compactMap { $0 as? FeatureManagerContract }.first
        return resolved ?? FeatureManagerMock()
    }
    
    func resolve() -> DispatchQueue {
        let resolved = dependencies.compactMap { $0 as? DispatchQueue }.first
        return resolved ?? DispatchQueue(label: "AccountManagementPJTests")
    }
}
