import UIKit

final class ViewControllerMock: UIViewController {
    private(set) var presentCallsCount = 0
    private(set) var viewControllerPresented = UIViewController()
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        presentCallsCount += 1
        viewControllerPresented = viewControllerToPresent
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
}

final class NavigationControllerMock: UINavigationController { }
