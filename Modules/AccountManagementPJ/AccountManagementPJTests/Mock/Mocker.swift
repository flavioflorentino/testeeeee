import Foundation

// swiftlint:disable convenience_type
final class AccountManagementPJTestsResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: AccountManagementPJTestsResources.self).url(forResource: "AccountManagementPJTestsResources", withExtension: "bundle") else {
            return Bundle(for: AccountManagementPJTestsResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: AccountManagementPJTestsResources.self)
    }()
}

struct LocalMock<T: Decodable> {
    func pureLocalMock(_ fileName: String, decoder: JSONDecoder = JSONDecoder()) -> T? {
        guard let data = Mocker().data(fileName) else { return nil }
        
        do {
            return try decoder.decode(T.self, from: data)
        } catch {
            return nil
        }
    }
}

struct Mocker {
    func data(_ fileName: String) -> Data? {
        guard
            let file = AccountManagementPJTestsResources.resourcesBundle.path(forResource: fileName, ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: file))
            else {
                return nil
        }
        return data
    }
}
