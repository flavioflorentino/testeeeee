import XCTest
@testable import AccountManagementPJ

final class AccountMock {
    func account() -> Account? {
        LocalMock<Account>().pureLocalMock("account", decoder: .init(.convertFromSnakeCase))
    }
}
