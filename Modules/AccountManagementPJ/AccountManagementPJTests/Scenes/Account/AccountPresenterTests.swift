import XCTest
@testable import AccountManagementPJ

private class AccountCoordinatingSpy: AccountCoordinating {
    var viewController: UIViewController?

    // MARK: - Perform
    private(set) var performActionCallsCount = 0
    private(set) var action: AccountAction?
    
    func perform(action: AccountAction) {
        performActionCallsCount += 1
        self.action = action
    }
}

private class AccountDisplayingSpy: AccountDisplaying {
    // MARK: - DisplayUserData
    private(set) var displayUserDataViewModelCallsCount = 0
    private(set) var displayUserDataViewModel: AccountUserDataViewModel?

    func displayUserData(viewModel: AccountUserDataViewModel) {
        displayUserDataViewModelCallsCount += 1
        displayUserDataViewModel = viewModel
    }

    // MARK: - DisplayCompanyData
    private(set) var displayCompanyDataViewModelCallsCount = 0
    private(set) var displayCompanyDataViewModel: AccountCompanyDataViewModel?

    func displayCompanyData(viewModel: AccountCompanyDataViewModel) {
        displayCompanyDataViewModelCallsCount += 1
        displayCompanyDataViewModel = viewModel
    }
}

final class AccountPresenterTests: XCTestCase {
    private let viewController = AccountDisplayingSpy()
    private let coordinator = AccountCoordinatingSpy()
    
    private lazy var sut: AccountPresenter = {
        let sut = AccountPresenter(coordinator: coordinator)
        sut.viewController = viewController
        return sut
    }()
    
    func testPresentUserData_ShouldDisplayUserData() throws {
        let account = try XCTUnwrap(AccountMock().account())
        let userDataViewModel = AccountUserDataViewModel(account: account)
        
        sut.presentUserData(viewModel: userDataViewModel)
        
        XCTAssertEqual(viewController.displayUserDataViewModelCallsCount, 1)
        XCTAssertEqual(viewController.displayUserDataViewModel, userDataViewModel)
    }
    
    func testPresentCompanyData_ShouldDisplayCompanyData() throws {
        let account = try XCTUnwrap(AccountMock().account())
        let companyDataViewModel = AccountCompanyDataViewModel(account: account)
        
        sut.presentCompanyData(viewModel: companyDataViewModel)
        
        XCTAssertEqual(viewController.displayCompanyDataViewModelCallsCount, 1)
        XCTAssertEqual(viewController.displayCompanyDataViewModel, companyDataViewModel)
    }
    
    func testDidNextStep_ShouldPerformAction() {
        sut.didNextStep(action: .chat)
        XCTAssertEqual(coordinator.performActionCallsCount, 1)
        XCTAssertEqual(coordinator.action, .chat)
    }
}
