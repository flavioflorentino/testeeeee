import Core
import XCTest
import FeatureFlag
@testable import AccountManagementPJ

private class AccountPresentingSpy: AccountPresenting {
    var viewController: AccountDisplaying?

    // MARK: - PresentUserData
    private(set) var presentUserDataViewModelCallsCount = 0
    private(set) var presentUserDataViewModelReceived: AccountUserDataViewModel?

    func presentUserData(viewModel: AccountUserDataViewModel) {
        presentUserDataViewModelCallsCount += 1
        presentUserDataViewModelReceived = viewModel
    }

    // MARK: - PresentCompanyData
    private(set) var presentCompanyDataViewModelCallsCount = 0
    private(set) var presentCompanyDataViewModelReceived: AccountCompanyDataViewModel?

    func presentCompanyData(viewModel: AccountCompanyDataViewModel) {
        presentCompanyDataViewModelCallsCount += 1
        presentCompanyDataViewModelReceived = viewModel
    }

    // MARK: - DidNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var action: AccountAction?

    func didNextStep(action: AccountAction) {
        didNextStepActionCallsCount += 1
        self.action = action
    }
}

private class AccountServicingMock: AccountServicing {
    var fetchAccountResult: Result<Account, ApiError> = .failure(ApiError.bodyNotFound)
    func fetchAccount(completion: @escaping (Result<Account, ApiError>) -> Void) {
        completion(fetchAccountResult)
    }
}

final class AccountInteractorTests: XCTestCase {
    private let presenter = AccountPresentingSpy()
    private let service = AccountServicingMock()
    
    private let featureManager: FeatureManagerMock = {
        let featureManager = FeatureManagerMock()
        featureManager.override(key: .faqSectionAccountManagement, with: "360011682912")
        return featureManager
    }()
    
    private lazy var dependencies = DependencyContainerMock(featureManager)
    
    private lazy var sut = AccountInteractor(service: service, presenter: presenter, dependencies: dependencies)
    
    func testLoadUserData_WhenSuccess_ShouldPresentUserData() throws {
        let account = try XCTUnwrap(AccountMock().account())
        let userDataVM = AccountUserDataViewModel(account: account)
        service.fetchAccountResult = .success(account)
        
        sut.loadUserData()
        
        XCTAssertEqual(presenter.presentUserDataViewModelCallsCount, 1)
        XCTAssertEqual(presenter.presentUserDataViewModelReceived, userDataVM)
    }
    
    func testLoadCompanyData_WhenSuccess_ShouldPresentCompanyData() throws {
        let account = try XCTUnwrap(AccountMock().account())
        let companyVM = AccountCompanyDataViewModel(account: account)
        service.fetchAccountResult = .success(account)
        
        sut.loadUserData()
        
        XCTAssertEqual(presenter.presentCompanyDataViewModelCallsCount, 1)
        XCTAssertEqual(presenter.presentCompanyDataViewModelReceived, companyVM)
    }
    
    func testFaqAction_ShouldPresentFaq() {
        sut.openFaq()
        XCTAssertEqual(presenter.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenter.action, .faq(id: featureManager.text(.faqSectionAccountManagement)))
    }
    
    func testChatAction_ShouldPresentChat() {
        sut.openChat()
        XCTAssertEqual(presenter.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenter.action, .chat)
    }
}
