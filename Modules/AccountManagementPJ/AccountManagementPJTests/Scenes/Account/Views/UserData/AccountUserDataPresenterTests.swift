import XCTest
@testable import AccountManagementPJ

private class AccountUserDataCoordinatingSpy: AccountUserDataCoordinating {
    var viewController: UIViewController?

    // MARK: - Perform
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocations: [AccountUserDataAction] = []

    func perform(action: AccountUserDataAction) {
        performActionCallsCount += 1
        performActionReceivedInvocations.append(action)
    }
}

private class AccountUserDataDisplayingSpy: AccountUserDataDisplaying {
    // MARK: - Display
    private(set) var displayUserDataCallsCount = 0
    private(set) var displayUserDataReceivedInvocations: [[AccountUserDataCellViewModel]] = []

    func display(userData: [AccountUserDataCellViewModel]) {
        displayUserDataCallsCount += 1
        displayUserDataReceivedInvocations.append(userData)
    }

    // MARK: - Feedback
    private(set) var feedbackCallsCount = 0
    private(set) var feedbackReceivedInvocations: [String] = []

    func feedback(_ message: String) {
        feedbackCallsCount += 1
        feedbackReceivedInvocations.append(message)
    }
}

final class AccountUserDataPresenterTests: XCTestCase {
    private let coordinator = AccountUserDataCoordinatingSpy()
    private let viewController = AccountUserDataDisplayingSpy()
    private lazy var sut: AccountUserDataPresenter = {
        let sut = AccountUserDataPresenter(coordinator: coordinator)
        sut.viewController = viewController
        return sut
    }()
    
    func testPresentUserData_ShouldDisplayUserData() throws {
        let userData = [
            AccountUserDataCellViewModel(title: "Log Horizon", value: "Barcelona", action: .admin, cellType: .defaultCell),
            AccountUserDataCellViewModel(title: "Shingeki no Kyojin", value: "Bayern", action: .phone, cellType: .defaultCell)
        ]
        sut.present(userData: userData)
        
        let invocation = try XCTUnwrap(viewController.displayUserDataReceivedInvocations.first)
        XCTAssertEqual(invocation, userData)
    }
    
    func testDidNextStep_ShouldCallPerformActionWithCorrectStep() throws {
        let action: AccountUserDataAction = .companyAddress
        sut.didNextStep(action: action)
        
        let invocation = try XCTUnwrap(coordinator.performActionReceivedInvocations.first)
        XCTAssertEqual(invocation, action)
    }
}
