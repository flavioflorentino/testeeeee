import FeatureFlag
import XCTest
@testable import AccountManagementPJ

private class AccountUserDataPresentingSpy: AccountUserDataPresenting {
    var viewController: AccountUserDataDisplaying?

    // MARK: - Present
    private(set) var presentUserDataCallsCount = 0
    private(set) var presentUserDataReceivedInvocation: [AccountUserDataCellViewModel] = []

    func present(userData: [AccountUserDataCellViewModel]) {
        presentUserDataCallsCount += 1
        presentUserDataReceivedInvocation = userData
    }

    // MARK: - DidNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [AccountUserDataAction] = []

    func didNextStep(action: AccountUserDataAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }

    // MARK: - Feedback
    private(set) var feedbackCallsCount = 0
    private(set) var feedbackReceivedInvocation: String?

    func feedback(_ message: String) {
        print(message)
        feedbackCallsCount += 1
        feedbackReceivedInvocation = message
    }
}

final class AccountUserDataInteractorTests: XCTestCase {
    private typealias Localizable = Strings.Account.UserData
    
    private let viewModel: AccountUserDataViewModel? = {
        guard let account = AccountMock().account() else { return nil }
        return AccountUserDataViewModel(account: account)
    }()
    
    private var featureManager: FeatureManagerMock = {
        let featureManager = FeatureManagerMock()
        featureManager.override(key: .isAccountInfoRenewalAvailable, with: true)
        return featureManager
    }()
    
    private let presenter = AccountUserDataPresentingSpy()
    private lazy var dependencyContainer = DependencyContainerMock(featureManager)
    
    private lazy var sut: AccountUserDataInteractor? = {
        guard let viewModel = viewModel else { return nil }
        return AccountUserDataInteractor(
            presenter: presenter,
            viewModel: viewModel,
            dependencies: dependencyContainer
        )
    }()
    
    func testLoadUserData_ShouldPresentUserDataWithCorrectValues() {
        sut?.loadUserData()
        
        let invocation = presenter.presentUserDataReceivedInvocation
        
        XCTAssertEqual(viewModel?.admin, invocation[0].value)
        XCTAssertEqual(viewModel?.email, invocation[1].value)
        XCTAssertEqual(viewModel?.phone, invocation[2].value)
        XCTAssertEqual(viewModel?.address, invocation[3].value)
        XCTAssertEqual(viewModel?.companyAddress, invocation[4].value)
    }
    
    func testLoadUserData_ShouldPresentUserDataWithCorrectTitles() {
        sut?.loadUserData()
        
        let invocation = presenter.presentUserDataReceivedInvocation
        
        XCTAssertEqual(Localizable.admin, invocation[0].title)
        XCTAssertEqual(Localizable.email, invocation[1].title)
        XCTAssertEqual(Localizable.phone, invocation[2].title)
        XCTAssertEqual(Localizable.address, invocation[3].title)
        XCTAssertEqual(Localizable.companyAddress, invocation[4].title)
    }
    
    func testDidSelectCell_ShouldCallDidNextStepWithCorrectAction() throws {
        sut?.loadUserData()
        
        try testDidSelectCell(indexPath: IndexPath(row: 0, section: 0), action: .admin)
        try testDidSelectCell(indexPath: IndexPath(row: 1, section: 0), action: .email)
        try testDidSelectCell(indexPath: IndexPath(row: 2, section: 0), action: .phone)
        try testDidSelectCell(indexPath: IndexPath(row: 3, section: 0), action: .address)
        try testDidSelectCell(indexPath: IndexPath(row: 4, section: 0), action: .companyAddress)
    }
    
    func testLoadUserData_WhenFlagDisabled_ShouldPresentUserDataWithCorrectValues() {
        featureManager.override(key: .isAccountInfoRenewalAvailable, with: false)
        sut?.loadUserData()
        
        let invocation = presenter.presentUserDataReceivedInvocation
        
        XCTAssertEqual(viewModel?.admin, invocation[0].value)
        XCTAssertEqual(viewModel?.email, invocation[1].value)
        XCTAssertEqual(viewModel?.phone, invocation[2].value)
    }
    
    func testLoadUserData_WhenFlagDisabled_ShouldPresentUserDataWithCorrectTitles() {
        featureManager.override(key: .isAccountInfoRenewalAvailable, with: false)
        sut?.loadUserData()
        
        let invocation = presenter.presentUserDataReceivedInvocation
        
        XCTAssertEqual(Localizable.admin, invocation[0].title)
        XCTAssertEqual(Localizable.email, invocation[1].title)
        XCTAssertEqual(Localizable.phone, invocation[2].title)
    }
    
    func testDidSelectCell_WhenFlagDisabled_ShouldCallDidNextStepWithCorrectAction() throws {
        featureManager.override(key: .isAccountInfoRenewalAvailable, with: false)
        sut?.loadUserData()
        
        try testDidSelectCell(indexPath: IndexPath(row: 0, section: 0), action: .admin)
        try testDidSelectCell(indexPath: IndexPath(row: 1, section: 0), action: .email)
        try testDidSelectCell(indexPath: IndexPath(row: 2, section: 0), action: .phone)
    }

    func testDidReceiveNotification_WhenNotificationCorrect_ShouldCallPresenter() throws {
        let userInfo: [String: AccountUserDataAction] = ["action": .admin]
        let name = Notification.Name(rawValue: "UserDataHasUpdated")
        let notification = Notification(name: name, object: nil, userInfo: userInfo)
        
        sut?.didReceiveUpdates(notification)
        
        let message = presenter.feedbackReceivedInvocation
        XCTAssertEqual(Localizable.updatedAdmin, message)
    }
}

private extension AccountUserDataInteractorTests {
    func testDidSelectCell(indexPath: IndexPath, action: AccountUserDataAction) throws {
        sut?.didSelectCell(indexPath: indexPath)
        
        let invocation = try XCTUnwrap(presenter.didNextStepActionReceivedInvocations.last)
        XCTAssertEqual(invocation, action)
    }
}
