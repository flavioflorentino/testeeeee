import CustomerSupport
import FeatureFlag
import XCTest
@testable import AccountManagementPJ

final class AccountCoordinatorTests: XCTestCase {
    private let viewControllerMock = ViewControllerMock()
    private lazy var navControllerMock = NavigationControllerMock(rootViewController: viewControllerMock)
    private let zendeskWrapper = ZendeskWrapperMock()
    
    private let featureManager: FeatureManagerMock = {
        let featureManager = FeatureManagerMock()
        featureManager.override(key: .faqSectionAccountManagement, with: "360011682912")
        return featureManager
    }()
    
    private lazy var dependencies = DependencyContainerMock(zendeskWrapper)
    
    private lazy var sut: AccountCoordinator = {
        let coordinator = AccountCoordinator(dependencies: dependencies)
        coordinator.viewController = viewControllerMock
        return coordinator
    }()
    
    func testPerform_WhenActionIsChat_ShouldCallBuildTicketViewController() {
        sut.perform(action: .chat)
        
        XCTAssertEqual(zendeskWrapper.didCallBuildTicketListController, 1)
        XCTAssertEqual(viewControllerMock.presentCallsCount, 1)
    }
    
    func testPerform_WhenActionIsFaq_ShouldCallBuildFAQController() {
        sut.perform(action: .faq(id: featureManager.text(.faqSectionAccountManagement)))
        
        XCTAssertEqual(zendeskWrapper.didCallBuildFAQController, 1)
        XCTAssertEqual(viewControllerMock.presentCallsCount, 1)
    }
}
