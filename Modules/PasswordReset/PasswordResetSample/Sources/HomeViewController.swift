import PasswordReset
import UI
import UIKit

final class HomeViewController: UIViewController {
    private var resetCoordinator: PasswordResetCoordinatorProtocol?
    private var deeplinkCoordinator: Coordinating?
    
    private lazy var button: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("Iniciar", for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTouchButton), for: .touchUpInside)
        return button
    }()

    private lazy var linkButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("Deeplink", for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTouchLinkButton), for: .touchUpInside)
        return button
    }()
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        view.addSubview(button)
        view.addSubview(linkButton)
        
        button.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base04)
            $0.trailing.equalToSuperview().offset(-Spacing.base04)
        }

        linkButton.snp.makeConstraints {
            $0.top.equalTo(button.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalTo(button)
        }

        title = "Password Reset"
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    func didTouchButton() {
        guard let navigationController = navigationController else {
            return
        }
        
        resetCoordinator = PasswordResetCoordinator(presenterController: navigationController)
        resetCoordinator?.start()
    }

    @objc
    func didTouchLinkButton() {
        deeplinkCoordinator = PasswordResetDeeplinkCoordinator(originController: self, code: "Code")
        deeplinkCoordinator?.start()
    }
}
