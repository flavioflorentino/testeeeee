@testable import PasswordReset
import XCTest

class DocumentVerificatorTests: XCTestCase {
    let sut = DocumentVerificator()
    
    func testIsCompleteDocument_WhenDocumentIsCompleteCPF_ShouldReturnTrue() {
        let document = "517.188.214-00"
        
        XCTAssertTrue(sut.isComplete(document, documentType: .cpf))
    }
    
    func testIsCompleteDocument_WhenDocumentIsIncompleteCPF_ShouldReturnFalse() {
        let document = "517.188.214"
        
        XCTAssertFalse(sut.isComplete(document, documentType: .cpf))
    }
    
    func testValidateDocument_WhenDocumentIsInvalidCpf_ShouldReturnFalse() {
        let document = "517.188.214-23"
        
        XCTAssertFalse(sut.validateDocument(document, documentType: .cpf))
    }
    
    func testValidateDocument_WhenDocumentIsIncompletCpf_ShouldReturnFalse() {
        let document = "517.188.214"
        
        XCTAssertFalse(sut.validateDocument(document, documentType: .cpf))
    }
    
    func testValidateDocument_WhenDocumentIsValidCpf_ShouldReturnTrue() {
        let document = "517.188.214-25"
        
        XCTAssertTrue(sut.validateDocument(document, documentType: .cpf))
    }
    
    func testValidateDocument_WhenDocumentIsCpfWithRepeatedNumbers_ShouldReturnFalse() {
        let document = "111.111.111-11"
        
        XCTAssertFalse(sut.validateDocument(document, documentType: .cpf))
    }
    
    func testValidateDocument_WhenDocumentIsEmptyCpf_ShouldReturnFalse() {
        let document = ""
        
        XCTAssertFalse(sut.validateDocument(document, documentType: .cpf))
    }
}
