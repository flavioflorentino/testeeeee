@testable import PasswordReset
import XCTest

private class PasswordResetTimerDelegateSpy: PasswordResetTimerDelegate {
    private(set) var callTimerTicCount = 0

    func timerTic() {
        callTimerTicCount += 1
    }
}

final class PasswordResetTimerTests: XCTestCase {
    private let delegateSpy = PasswordResetTimerDelegateSpy()

    private lazy var sut: PasswordResetTimer = {
        let timer = PasswordResetTimer()
        timer.delegate = delegateSpy
        return timer
    }()

    func testStart_ShouldFireTimer() throws {
        sut.start()

        let timer = try XCTUnwrap(sut.timer)
        XCTAssertTrue(timer.isValid)
        XCTAssertEqual(delegateSpy.callTimerTicCount, 1)
    }

    func testStop_ShouldInvalidateTimer() throws {
        sut.start()
        sut.stop()

        let timer = try XCTUnwrap(sut.timer)
        XCTAssertFalse(timer.isValid)
    }
}
