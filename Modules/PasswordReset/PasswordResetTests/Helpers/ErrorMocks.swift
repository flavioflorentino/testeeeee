import Core
import Foundation

enum ErrorMocks {
    static func generateRequestErrorWith(title: String, message: String, code: String) -> RequestError {
        let data: [String: Any] = [
            "short_message" : title,
            "data" : [
                "retry_time" : 10
            ],
            "code" : code,
            "message" : message
        ]
        let jsonData = try? JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
        return RequestError.initialize(with: jsonData)
    }
}

