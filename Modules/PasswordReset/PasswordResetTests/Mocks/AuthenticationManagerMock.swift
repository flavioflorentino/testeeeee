import Core
import Foundation

final class AuthenticationManagerMock: AuthenticationContract {
    var authenticated = false
    var isAuthenticated: Bool {
        authenticated
    }

    func authenticate(_ completion: @escaping AuthenticationResult) {
        completion(.success("123"))
    }

    private(set) var disableBiometricAuthenticationCallCount = 0
    func disableBiometricAuthentication() {
        disableBiometricAuthenticationCallCount += 1
    }
}
