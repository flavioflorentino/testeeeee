@testable import PasswordReset
import UI
import XCTest

private class PasswordResetFallbackCoordinatorDelegateSpy: PasswordResetFallbackCoordinatorDelegate {
    private(set) var resetFallbackFlowCallCount = 0

    func resetFallbackFlow() {
        resetFallbackFlowCallCount += 1
    }
}

final class PasswordResetFallbackCoordinatorTests: XCTestCase {
    private let navigationSpy = NavigationControllerSpy()
    private let delegateSpy = PasswordResetFallbackCoordinatorDelegateSpy()
    
    private lazy var sut = PasswordResetFallbackCoordinator(
        navigationController: navigationSpy,
        currentStep: PasswordResetStepData(passwordResetId: "abc", nextStep: .newEmail),
        channelType: .biometric,
        delegate: delegateSpy
    )
    
    func testStart_ShouldShowBiometricIntroViewController() {
        sut.start()
        
        XCTAssertEqual(navigationSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is BiometricIntroViewController)
    }
    
    func testBiometricIntroGoBack_ShouldPopToPreviousViewController() {
        sut.biometricIntroGoBack()
        
        XCTAssertEqual(navigationSpy.popViewControllerCallCount, 1)
    }
    
    func testBiometricIntroGoNextStep_ShouldShowEmailConfirmationViewController() {
        sut.biometricIntroGoNextStep()
        
        XCTAssertEqual(navigationSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is EmailConfirmationViewController)
    }
    
    func testEmailConfirmationNextStep_WhenStepIsCode_ShouldShowAuthorizationCodeViewController() {
        sut.emailConfirmationNextStep(PasswordResetStepData(passwordResetId: "abc", nextStep: .code), userEmail: "email")
        
        XCTAssertEqual(navigationSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is AuthorizationCodeViewController)
    }
    
    func testAuthorizationCodeNextStep_WhenStepIsNotApproved_shouldShowValidationStatusViewController() {
        sut.authorizationCodeNextStep(PasswordResetStepData(passwordResetId: "abc", nextStep: .notApproved))
        
        XCTAssertEqual(navigationSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is ValidationStatusViewController)
    }
    
    func testDidTapCloseButton_ShouldDismissResetFlow() {
        sut.didTapCloseButton()
        
        XCTAssertEqual(navigationSpy.dismissViewControllerCallCount, 1)
    }
    
    func testDidTapActionButton_ShouldDismissResetFlow() {
        sut.didTapActionButton()
        
        XCTAssertEqual(navigationSpy.dismissViewControllerCallCount, 1)
    }

    func testAuthorizationCodeNextStep_WhenStepIsSelfie_PushBiometricViewController() {
        sut.authorizationCodeNextStep(PasswordResetStepData(passwordResetId: "ResetID", nextStep: .selfie))

        XCTAssertEqual(navigationSpy.presentViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.viewControllerPresented is BiometricViewController)
    }

    func testBiometricDelegateShowIdentity_StartIdentityValidationModule() throws {
        sut.biometricController(nil, showIdentity: IdentityFlowData(token: "", flow: ""))

        XCTAssertEqual(navigationSpy.presentViewControllerCallCount, 1)
        let presentedNavigation = try XCTUnwrap(navigationSpy.viewControllerPresented)
        XCTAssertTrue(presentedNavigation is UINavigationController)
    }

    func testBiometricDelegateBack_DismissController() {
        let testController = NavigationControllerSpy()
        navigationSpy.present(testController, animated: false)
        XCTAssertEqual(navigationSpy.presentViewControllerCallCount, 1)

        sut.back(from: testController)

        XCTAssertEqual(testController.dismissViewControllerCallCount, 1)
    }

    func testValidationStatusdDidClose_ShouldDismissResetFlow() {
        sut.validationStatusDidClose()
        
        XCTAssertEqual(navigationSpy.dismissViewControllerCallCount, 1)
    }

    func testIdentityValidationCompletion_WhenStatusIsNil_DoNothing() {
        sut.identityValidationCompletion(with: nil)

        XCTAssertEqual(navigationSpy.pushViewControllerCallCount, 0)
    }

    func testIdentityValidationCompletion_WhenStatusIsNotNil_PushStepLoader() {
        sut.identityValidationCompletion(with: .approved)

        XCTAssertEqual(navigationSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is StepLoaderViewController)
    }

    func testBackFromStepLoader_PresentWarningPopup() throws {
        sut.backFromStepLoader()

        XCTAssertEqual(navigationSpy.presentViewControllerCallCount, 1)
        let presentedPopup = try XCTUnwrap(navigationSpy.viewControllerPresented)
        XCTAssertTrue(presentedPopup is PopupViewController)
    }

    func testStepLoaderNextStep_WhenStepIsNewPassword_PushNewPasswordController() {
        sut.stepLoaderNextStep(PasswordResetStepData(passwordResetId: "", nextStep: .newPassword))

        XCTAssertEqual(navigationSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is NewPasswordViewController)
    }

    func testNewPasswordNextStep_WhenStepIsDone_PushValidationSuccessViewController() {
        sut.newPasswordNextStep(PasswordResetStepData(passwordResetId: "", nextStep: .done))

        XCTAssertEqual(navigationSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is ResetSuccessViewController)
    }

    func testBackFromNewPassword_WhenShouldShowWarningIsTrue_PresentWarningPopup() throws {
        sut.backFromNewPassword(shouldShowWarning: true)

        XCTAssertEqual(navigationSpy.presentViewControllerCallCount, 1)
        let presentedPopup = try XCTUnwrap(navigationSpy.viewControllerPresented)
        XCTAssertTrue(presentedPopup is PopupViewController)
    }

    func testBackFromNewPassword_WhenShouldShowWarningIsFalse_PopViewController() {
        sut.backFromNewPassword(shouldShowWarning: false)

        XCTAssertEqual(navigationSpy.popViewControllerCallCount, 1)
    }

    func testStepLoaderNextStep_WhenStepIsPersonalData_PushUserFormController() {
        sut.stepLoaderNextStep(PasswordResetStepData(passwordResetId: "", nextStep: .personalData))

        XCTAssertEqual(navigationSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is UserFormViewController)
    }

    func testUserFormNextStep_WhenNextStepIsManualReview_PushValidationStatusController() {
        sut.userFormNextStep(PasswordResetStepData(passwordResetId: "", nextStep: .manualReview))

        XCTAssertEqual(navigationSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is ValidationStatusViewController)
    }

    func testBackFromUserForm_PresentWarningPopup() throws {
        sut.backFromUserForm()

        XCTAssertEqual(navigationSpy.presentViewControllerCallCount, 1)
        let presentedPopup = try XCTUnwrap(navigationSpy.viewControllerPresented)
        XCTAssertTrue(presentedPopup is PopupViewController)
    }
}
