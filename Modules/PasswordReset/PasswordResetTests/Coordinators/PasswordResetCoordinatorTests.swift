@testable import PasswordReset
import XCTest

final class PasswordResetCoordinatorTests: XCTestCase {
    let navigationSpy = NavigationControllerSpy()
    let presenterController = NavigationControllerSpy()
    let smsChannel = Channel(channelType: .sms, title: "SMS", icon: "", data: "****-9999")
    let emailChannel = Channel(channelType: .email, title: "Email", icon: "", data: "****@mail.com")
    
    private lazy var sut = PasswordResetCoordinator(presenterController: presenterController, navigationController: navigationSpy)

    func testStart_WhenPresentedControllerIsNil_ShouldShowUserVerificationViewController() {
        sut.start()

        XCTAssertEqual(presenterController.presentViewControllerCallCount, 1)
        XCTAssertTrue(presenterController.viewControllerPresented is NavigationControllerSpy)
        XCTAssertEqual(navigationSpy.setViewControllersCallCount, 1)
        XCTAssertTrue(navigationSpy.viewControllers.first is UserIdentificationViewController)
    }

    func testStart_WhenPresentedControllerIsNotNil_ShouldShowUserVerificationViewController() {
        presenterController.present(UIViewController(), animated: false)

        sut.start()

        XCTAssertEqual(presenterController.dismissViewControllerCallCount, 1)
        XCTAssertEqual(presenterController.presentViewControllerCallCount, 2)
        XCTAssertTrue(presenterController.viewControllerPresented is NavigationControllerSpy)
        XCTAssertEqual(navigationSpy.setViewControllersCallCount, 1)
        XCTAssertTrue(navigationSpy.viewControllers.first is UserIdentificationViewController)
    }
    
    func testDidReceivedResetChannels_ShouldShowResetChannelsViewController() {
        let channelsData = PasswordResetChannels(channels: [smsChannel, emailChannel], fallback: nil)

        sut.didReceivedResetChannels(channelsData, for: "")
        
        XCTAssertEqual(navigationSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is ResetChannelsViewController)
    }
    
    func testDidReceiveChannelNextStep_WhenChannelTypeIsBiometric_ShouldStartPasswordResetFallbackCoordinator() {
        let channel = Channel(channelType: .biometric, title: "", icon: "", data: "")

        sut.didReceiveChannelNextStep(PasswordResetStepData(passwordResetId: "abc", nextStep: .code), channel: channel)
        
        XCTAssertNotNil(sut.childCoordinator)
        XCTAssertTrue(sut.childCoordinator is PasswordResetFallbackCoordinator)
    }
    
    func testDidReceiveChannelNextStep_WhenChannelTypeIsNotBiometric_ShouldStartPasswordResetChannelsCoordinator() {
        let channel = Channel(channelType: .sms, title: "", icon: "", data: "")

        sut.didReceiveChannelNextStep(PasswordResetStepData(passwordResetId: "abc", nextStep: .code), channel: channel)
        
        XCTAssertNotNil(sut.childCoordinator)
        XCTAssertTrue(sut.childCoordinator is PasswordResetChannelsCoordinator)
    }
    
    func testDidClose_ShouldDismissPasswordResetFlow() {
        sut.didClose()
        
        XCTAssertEqual(navigationSpy.dismissViewControllerCallCount, 1)
    }

    func testResetFallbackFlow_WhenThereIsResetChannels_PopToResetChannelsViewController() {
        sut.didReceivedResetChannels(PasswordResetChannels(channels: [], fallback: nil), for: "")
        navigationSpy.pushViewController(UIViewController(), animated: false)

        sut.resetFallbackFlow()

        XCTAssertNil(sut.childCoordinator)
        XCTAssertTrue(navigationSpy.topViewController is ResetChannelsViewController)
    }

    func testResetFallbackFlow_PopToRootViewController() {
        sut.resetFallbackFlow()

        XCTAssertEqual(navigationSpy.popToRootViewControllerCallCount, 1)
    }
}
