@testable import PasswordReset
import UI
import XCTest

class PasswordResetChannelsCoordinatorTests: XCTestCase {
    let navigationSpy = NavigationControllerSpy()
    
    private lazy var sut = createSut()
    
    func testStart_ShouldShowAuthorizationCodeViewController() {
        sut.start()
        
        XCTAssertEqual(navigationSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is AuthorizationCodeViewController)
    }
    
    func testAuthorizationCodeNextStep_WhenStepIsNewPassword_ShouldShowNewPasswordViewController() {
        sut.authorizationCodeNextStep(PasswordResetStepData(passwordResetId: "abc", nextStep: .newPassword))
        
        XCTAssertEqual(navigationSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is NewPasswordViewController)
    }
    
    func testNewPasswordNextStep_WhenStepIsDone_ShouldShowResetSuccessViewController() {
        sut.newPasswordNextStep(PasswordResetStepData(passwordResetId: "abc", nextStep: .done))
        
        XCTAssertEqual(navigationSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is ResetSuccessViewController)
    }
    
    func testDidTapCloseButton_ShouldDismissResetFlow() {
        sut.didTapCloseButton()
        
        XCTAssertEqual(navigationSpy.dismissViewControllerCallCount, 1)
    }
    
    func testDidTapActionButton_ShouldDismissResetFlow() {
        sut.didTapActionButton()
        
        XCTAssertEqual(navigationSpy.dismissViewControllerCallCount, 1)
    }

    func testBackFromNewPassword_WhenShouldShowWarningIsFalse_ShouldPopViewController() {
        sut.backFromNewPassword(shouldShowWarning: false)

        XCTAssertEqual(navigationSpy.popViewControllerCallCount, 1)
    }

    func testBackFromNewPassword_WhenShouldShowWarningIsTrue_ShouldPresentWarningPopup() {
        sut.backFromNewPassword(shouldShowWarning: true)

        XCTAssertEqual(navigationSpy.presentViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.viewControllerPresented is PopupViewController)
    }

    func testAuthorizationCodeNextStep_WhenStepIsNotApproved_shouldShowValidationStatusViewController() {
        sut.authorizationCodeNextStep(PasswordResetStepData(passwordResetId: "", nextStep: .notApproved))

        XCTAssertEqual(navigationSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is ValidationStatusViewController)
    }

    func testAuthorizationCodeNextStep_WhenNextStepIsManualReview_shouldShowValidationStatusViewController() {
        sut.authorizationCodeNextStep(PasswordResetStepData(passwordResetId: "", nextStep: .manualReview))

        XCTAssertEqual(navigationSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is ValidationStatusViewController)
    }

    func testValidationStatusdDidClose_ShouldDismissResetFlow() {
        sut.validationStatusDidClose()

        XCTAssertEqual(navigationSpy.dismissViewControllerCallCount, 1)
    }

    func testStart_WhenNextStepIsLink_ShouldShowLinkConfirmationViewController() {
        sut = createSut(nextStep: .link)

        sut.start()

        XCTAssertEqual(navigationSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is LinkConfirmationViewController)
    }

    func testBackFromLinkConfirmation_ShouldPopViewController() {
        sut.backFromLinkConfirmation()

        XCTAssertEqual(navigationSpy.popViewControllerCallCount, 1)
    }

    func testLinkConfirmationDone_ShouldDismissResetFlow() {
        sut.linkConfirmationDone()

        XCTAssertEqual(navigationSpy.dismissViewControllerCallCount, 1)
    }
}

private extension PasswordResetChannelsCoordinatorTests {
    func createSut(nextStep: PasswordResetStep = .code, isFromDeeplink: Bool = false) -> PasswordResetChannelsCoordinator {
        PasswordResetChannelsCoordinator(
            navigationController: navigationSpy,
            currentStep: PasswordResetStepData(passwordResetId: "res_et_ID", nextStep: nextStep),
            channel: Channel(channelType: .email, title: "", icon: "", data: ""),
            isFromDeeplink: isFromDeeplink
        )
    }
}
