@testable import PasswordReset
import XCTest

final class PasswordResetDeeplinkCoordinatorTests: XCTestCase {
    private let originControllerSpy = NavigationControllerSpy()
    private let navigationSpy = NavigationControllerSpy()

    private lazy var sut = PasswordResetDeeplinkCoordinator(originController: originControllerSpy,
                                                            code: "CODE",
                                                            navigationController: navigationSpy)

    func testStart_WhenOriginPresentedControllerIsNil_ShouldPresentLinkValidationController() {
        sut.start()

        XCTAssertEqual(navigationSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is LinkValidationViewController)
        XCTAssertEqual(originControllerSpy.presentViewControllerCallCount, 1)
        XCTAssertTrue(originControllerSpy.viewControllerPresented is NavigationControllerSpy)
    }

    func testStart_WhenOriginPresentedControllerIsNotNil_ShouldPresentLinkValidationController() {
        originControllerSpy.present(UIViewController(), animated: false)

        sut.start()

        XCTAssertEqual(navigationSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is LinkValidationViewController)
        XCTAssertEqual(originControllerSpy.dismissViewControllerCallCount, 1)
        XCTAssertEqual(originControllerSpy.presentViewControllerCallCount, 2)
        XCTAssertTrue(originControllerSpy.viewControllerPresented is NavigationControllerSpy)
    }

    func testLinkValidationNextStep_WhenStepIsNewPassword_ShouldPushNewPasswordController() {
        let stepData = PasswordResetStepData(passwordResetId: "Id", nextStep: .newPassword)

        sut.linkValidationNextStep(stepData)

        XCTAssertEqual(navigationSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is NewPasswordViewController)
    }

    func testLinkValidationError_ShouldPushLinkErrorController() {
        sut.linkValidationError(code: .unkown, title: nil, message: nil)

        XCTAssertEqual(navigationSpy.pushViewControllerCallCount, 1)
        XCTAssertTrue(navigationSpy.pushedViewController is LinkErrorViewController)
    }

    func testLinkValidationClose_ShouldDismissNavigationController() {
        sut.linkValidationClose()

        XCTAssertEqual(navigationSpy.dismissViewControllerCallCount, 1)
    }

    func testCloseLinkError_ShouldDismissNavigationController() {
        sut.closeLinkError()

        XCTAssertEqual(navigationSpy.dismissViewControllerCallCount, 1)
    }
}
