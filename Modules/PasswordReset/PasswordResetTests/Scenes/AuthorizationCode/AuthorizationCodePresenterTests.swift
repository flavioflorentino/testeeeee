@testable import PasswordReset
import UI
import XCTest

private class AuthorizationCodeCoordinatingSpy: AuthorizationCodeCoordinating {
    weak var delegate: AuthorizationCodeCoordinatorDelegate?
    
    private(set) var backActionCount = 0
    private(set) var nextStepActionCount = 0

    func perform(action: AuthorizationCodeAction) {
        switch action {
        case .back:
            backActionCount += 1
        case .nextStep:
            nextStepActionCount += 1
        }
    }
}

private class AuthorizationCodeDisplaySpy: AuthorizationCodeDisplay {
    var loadingView = LoadingView()
    private(set) var callSetDescriptionTextCount = 0
    private(set) var callSetDescriptionAttributedStringCount = 0
    private(set) var callSetRetryButtonAttributedStringCount = 0
    private(set) var callTranslateViewYCount = 0
    private(set) var callSetTextfieldTextCount = 0
    private(set) var callShowPopupControllerCount = 0
    private(set) var callStartLoadingViewCount = 0
    private(set) var callStopLoadingViewCount = 0
    private(set) var callDisableConfirmButtonCount = 0
    private(set) var callEnableConfirmButtonCount = 0

    private(set) var descriptionText: String?
    private(set) var isButtonEnabled: Bool = false
    private(set) var buttonState: UIControl.State?
    private(set) var popupProperties: PopupProperties?

    func setDescriptionText(_ text: String) {
        callSetDescriptionTextCount += 1
        descriptionText = text
    }

    func setDescriptionAttributedString(_ string: NSAttributedString) {
        callSetDescriptionAttributedStringCount += 1
        descriptionText = string.string
    }

    func setRetryButtonAttributedString(_ string: NSAttributedString, isEnabled: Bool, state: UIControl.State) {
        callSetRetryButtonAttributedStringCount += 1
        isButtonEnabled = isEnabled
        buttonState = state
    }

    func translateViewY(by offset: CGFloat, with properties: KeyboardProperties) {
        callTranslateViewYCount += 1
    }

    func setTextfieldText(_ text: String) {
        callSetTextfieldTextCount += 1
    }

    func showPopupController(with properties: PopupProperties) {
        callShowPopupControllerCount += 1
        popupProperties = properties
    }

    func disableConfirmButton() {
        callDisableConfirmButtonCount += 1
    }

    func enableConfirmButton() {
        callEnableConfirmButtonCount += 1
    }

    func startLoadingView() {
        callStartLoadingViewCount += 1
    }

    func stopLoadingView() {
        callStopLoadingViewCount += 1
    }
}

final class AuthorizationCodePresenterTests: XCTestCase {
    private let coordinatorSpy = AuthorizationCodeCoordinatingSpy()
    private let controllerSpy = AuthorizationCodeDisplaySpy()
    private let mailMock = "mail@test.com"

    private lazy var sut: AuthorizationCodePresenter = {
        let presenter = AuthorizationCodePresenter(coordinator: coordinatorSpy, emailString: mailMock)
        presenter.viewController = controllerSpy
        return presenter
    }()

    func testSetupDescription_WhenTypeIsEmail_ShouldSetEmailDescription() {
        sut.setupDescription(for: .email)

        XCTAssertEqual(controllerSpy.callSetDescriptionTextCount, 1)
        XCTAssertEqual(controllerSpy.descriptionText, Strings.Authorization.Description.Email.label)
    }

    func testSetupDescription_WhenTypeIsSms_ShouldSetSmsDescription() {
        sut.setupDescription(for: .sms)

        XCTAssertEqual(controllerSpy.callSetDescriptionTextCount, 1)
        XCTAssertEqual(controllerSpy.descriptionText, Strings.Authorization.Description.Sms.label)
    }

    func testSetupDescription_WhenTypeIsBiometric_ShouldSetAttributedBiometricDescription() {
        sut.setupDescription(for: .biometric)

        XCTAssertEqual(controllerSpy.callSetDescriptionAttributedStringCount, 1)
        XCTAssertEqual(controllerSpy.descriptionText, Strings.Authorization.Description.Biometric.label(mailMock))
    }

    func testSetupRetryButtonTitle_WhenIsEnabledIsTrue_ShouldCreateAttributedStringWithStateNormal() {
        sut.setupRetryButtonTitle(isEnabled: true, remainingSeconds: 0)

        XCTAssertEqual(controllerSpy.callSetRetryButtonAttributedStringCount, 1)
        XCTAssertTrue(controllerSpy.isButtonEnabled)
        XCTAssertEqual(controllerSpy.buttonState, .normal)
    }

    func testSetupRetryButtonTitle_WhenIsEnabledIsFalse_ShouldCreateAttributedStringWithStateDisabled() {
        sut.setupRetryButtonTitle(isEnabled: false, remainingSeconds: 300)

        XCTAssertEqual(controllerSpy.callSetRetryButtonAttributedStringCount, 1)
        XCTAssertFalse(controllerSpy.isButtonEnabled)
        XCTAssertEqual(controllerSpy.buttonState, .disabled)
    }

    func testTranslateViewY_ShouldCallControllerTranslateViewY() {
        sut.translateViewY(by: 1, with: KeyboardProperties(height: 0, animationCurve: 0, animationDuration: 0))

        XCTAssertEqual(controllerSpy.callTranslateViewYCount, 1)
    }

    func testSetTextfieldText_ShouldCallControllerSetTextField() {
        sut.setTextfieldText("")

        XCTAssertEqual(controllerSpy.callSetTextfieldTextCount, 1)
    }

    func testPresentRetryError_ShouldCallShowPopupController() {
        sut.presentRetryError(nil)

        XCTAssertEqual(controllerSpy.callShowPopupControllerCount, 1)
        XCTAssertEqual(controllerSpy.popupProperties?.title, Strings.Error.title)
        XCTAssertEqual(controllerSpy.popupProperties?.message, Strings.Error.message)
        XCTAssertEqual(controllerSpy.popupProperties?.mainButtonTitle, Strings.General.tryAgain)
        XCTAssertEqual(controllerSpy.popupProperties?.secondaryButtonTitle, Strings.General.back)
    }

    func testPresentValidationError_ShouldCallShowPopupController() {
        sut.presentValidationError(nil)

        XCTAssertEqual(controllerSpy.callShowPopupControllerCount, 1)
        XCTAssertEqual(controllerSpy.popupProperties?.title, Strings.Error.title)
        XCTAssertEqual(controllerSpy.popupProperties?.message, Strings.Error.message)
        XCTAssertEqual(controllerSpy.popupProperties?.mainButtonTitle, Strings.General.understood)
        XCTAssertNil(controllerSpy.popupProperties?.secondaryButtonTitle)
    }

    func testStartLoading_ShouldCallStartLoadingView() {
        sut.startLoading()

        XCTAssertEqual(controllerSpy.callStartLoadingViewCount, 1)
    }

    func testStopLoading_ShouldCallStopLoadingView() {
        sut.stopLoading()

        XCTAssertEqual(controllerSpy.callStopLoadingViewCount, 1)
    }

    func testDisableConfirmButton_ShouldCallDisableConfirmButton() {
        sut.disableConfirmButton()

        XCTAssertEqual(controllerSpy.callDisableConfirmButtonCount, 1)
    }

    func testEnableConfirmButton_ShouldCallEnableConfirmButton() {
        sut.enableConfirmButton()

        XCTAssertEqual(controllerSpy.callEnableConfirmButtonCount, 1)
    }

    func testGoToNextStep_ShouldCallCoordinatorNextStepAction() {
        sut.goToNextStep(PasswordResetStepData(passwordResetId: "", nextStep: .newPassword))

        XCTAssertEqual(coordinatorSpy.nextStepActionCount, 1)
    }

    func testGoBack_ShouldCallCoordinatorBackAction() {
        sut.goBack()

        XCTAssertEqual(coordinatorSpy.backActionCount, 1)
    }
}
