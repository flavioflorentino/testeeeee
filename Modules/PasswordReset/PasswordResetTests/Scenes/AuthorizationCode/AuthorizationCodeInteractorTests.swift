import AnalyticsModule
import Core
@testable import PasswordReset
import XCTest

private class AuthorizationCodeServicingMock: AuthorizationCodeServicing {
    private(set) var callRequestCodeCount = 0
    private(set) var callValidateCodeCount = 0
    var requestCodeResult: Result<CodeConfirmation, ApiError> = .failure(.serverError)
    var validateCodeResult: Result<PasswordResetStepData, ApiError> = .failure(.serverError)

    func requestCode(resetId: String, completion: @escaping (Result<CodeConfirmation, ApiError>) -> Void) {
        callRequestCodeCount += 1
        completion(requestCodeResult)
    }
    
    func validateCode(_ code: String, resetId: String, completion: @escaping (Result<PasswordResetStepData, ApiError>) -> Void) {
        callValidateCodeCount += 1
        completion(validateCodeResult)
    }
}

private class AuthorizationCodePresentingSpy: AuthorizationCodePresenting {
    var viewController: AuthorizationCodeDisplay?
    private(set) var callSetupDescriptionCount = 0
    private(set) var callDisableConfirmButtonCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callPresentRetryErrorCount = 0
    private(set) var callPresentValidationErrorCount = 0
    private(set) var callTranslateViewYCount = 0
    private(set) var callEnableConfirmButtonCount = 0
    private(set) var callSetupRetryButtonTitleCount = 0
    private(set) var callSetTextfieldTextCount = 0
    private(set) var callGoToNextStepCount = 0
    private(set) var callGoBackCount = 0

    private(set) var isRetryButtonEnabled: Bool = false
    private(set) var trimmedText: String?

    func setupDescription(for type: ChannelType) {
        callSetupDescriptionCount += 1
    }

    func setupRetryButtonTitle(isEnabled: Bool, remainingSeconds: Int) {
        callSetupRetryButtonTitleCount += 1
        isRetryButtonEnabled = isEnabled
    }

    func translateViewY(by offset: CGFloat, with properties: KeyboardProperties) {
        callTranslateViewYCount += 1
    }

    func setTextfieldText(_ text: String) {
        callSetTextfieldTextCount += 1
        trimmedText = text
    }

    func presentRetryError(_ requestError: RequestError?) {
        callPresentRetryErrorCount += 1
    }

    func presentValidationError(_ requestError: RequestError?) {
        callPresentValidationErrorCount += 1
    }

    func startLoading() {
        callStartLoadingCount += 1
    }

    func stopLoading() {
        callStopLoadingCount += 1
    }

    func disableConfirmButton() {
        callDisableConfirmButtonCount += 1
    }

    func enableConfirmButton() {
        callEnableConfirmButtonCount += 1
    }
    
    func goToNextStep(_ step: PasswordResetStepData) {
        callGoToNextStepCount += 1
    }

    func goBack() {
        callGoBackCount += 1
    }
}

private class PasswordResetTimerProtocolSpy: PasswordResetTimerProtocol {
    weak var delegate: PasswordResetTimerDelegate?
    private(set) var callStartCount = 0
    private(set) var callStopCount = 0

    func start() {
        callStartCount += 1
    }

    func stop() {
        callStopCount += 1
    }
}

final class AuthorizationCodeInteractorTests: XCTestCase {
    private let serviceMock = AuthorizationCodeServicingMock()
    private let presenterSpy = AuthorizationCodePresentingSpy()
    private let timerSpy = PasswordResetTimerProtocolSpy()
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)

    private lazy var sut = AuthorizationCodeInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        channelType: .sms,
        resetId: "EYE_D",
        dependencies: dependencies,
        timer: timerSpy
    )

    func testSetupView_ShouldCallSetupDescription() {
        sut.setupView()

        XCTAssertEqual(presenterSpy.callSetupDescriptionCount, 1)
    }

    func testSetupView_ShouldDisableConfirmButton() {
        sut.setupView()

        XCTAssertEqual(presenterSpy.callDisableConfirmButtonCount, 1)
    }

    func testSetupView_ShouldStartAndStopLoading() {
        sut.setupView()

        XCTAssertEqual(presenterSpy.callStartLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callStopLoadingCount, 1)
    }

    func testSetupView_WhenRequestIsSuccess_ShouldStartTimer() {
        serviceMock.requestCodeResult = .success(CodeConfirmation(retryTime: 300))

        sut.setupView()

        XCTAssertEqual(timerSpy.callStartCount, 1)
    }

    func testSetupView_WhenRequestIsFailure_ShouldStopTimer() {
        sut.setupView()

        XCTAssertEqual(timerSpy.callStopCount, 1)
    }

    func testSetupView_WhenRequestIsFailure_ShouldCallPresentError() {
        sut.setupView()

        XCTAssertEqual(presenterSpy.callPresentRetryErrorCount, 1)
    }

    func testSetupView_WhenRequestIsFailure_ShouldTrackAppearAndErrorEvent() {
        sut.setupView()

        let appearEvent = PasswordResetTracker(method: .sms, eventType: AuthorizationCodeEvent.appear).event()
        let errorEvent = PasswordResetTracker(method: .sms, eventType: AuthorizationCodeEvent.channelError).event()
        XCTAssertTrue(analytics.equals(to: appearEvent, errorEvent))
    }

    func testSetupView_WhenRequestIsFailure_WhenErrorIsBlocked_ShouldTrackBlockedErrorEvent() {
        var requestError = RequestError()
        requestError.code = "password_reset_block"
        serviceMock.requestCodeResult = .failure(.unauthorized(body: requestError))

        sut.setupView()

        let errorEvent = PasswordResetTracker(method: .sms, eventType: ErrorEvent.blocked).event()
        XCTAssertTrue(analytics.equals(to: errorEvent))
    }

    func testKeyboardWillShow_WhenKeyboardStateIsShownig_ShouldDoNothing() {
        let properties = KeyboardProperties(height: 0, animationCurve: 0, animationDuration: 0)
        sut.keyboardWillShow(currentSpacing: 0, minSpacing: 0, properties: properties)

        sut.keyboardWillShow(currentSpacing: 0, minSpacing: 0, properties: properties)

        XCTAssertEqual(presenterSpy.callTranslateViewYCount, 0)
    }

    func testKeyboardWillShow_WhenKeyboardStateIsHiding_WhenCurrentSpacingIsEqualToMinSpacing_ShouldDoNothing() {
        let properties = KeyboardProperties(height: 0, animationCurve: 0, animationDuration: 0)
        sut.keyboardWillShow(currentSpacing: 0, minSpacing: 0, properties: properties)

        XCTAssertEqual(presenterSpy.callTranslateViewYCount, 0)
    }

    func testKeyboardWillShow_WhenKeyboardStateIsHiding_WhenCurrentSpacingIsGreaterThanMinSpacing_ShouldDoNothing() {
        let properties = KeyboardProperties(height: 0, animationCurve: 0, animationDuration: 0)
        sut.keyboardWillShow(currentSpacing: 1, minSpacing: 0, properties: properties)

        XCTAssertEqual(presenterSpy.callTranslateViewYCount, 0)
    }

    func testKeyboardWillShow_WhenKeyboardStateIsHiding_WhenCurrentSpacingIsLessThanMinSpacing_ShouldCallTranslateView() {
        let properties = KeyboardProperties(height: 0, animationCurve: 0, animationDuration: 0)
        sut.keyboardWillShow(currentSpacing: 0, minSpacing: 1, properties: properties)

        XCTAssertEqual(presenterSpy.callTranslateViewYCount, 1)
    }

    func testKeyboardWillHide_WhenKeyboardStateIsHiding_ShouldDoNothing() {
        let properties = KeyboardProperties(height: 0, animationCurve: 0, animationDuration: 0)
        sut.keyboardWillHide(properties: properties)

        XCTAssertEqual(presenterSpy.callTranslateViewYCount, 0)
    }

    func testKeyboardWillHide_WhenKeyboardStateIsShowing_ShouldCallTranslateView() {
        let properties = KeyboardProperties(height: 0, animationCurve: 0, animationDuration: 0)
        sut.keyboardWillShow(currentSpacing: 0, minSpacing: 0, properties: properties)

        sut.keyboardWillHide(properties: properties)

        XCTAssertEqual(presenterSpy.callTranslateViewYCount, 1)
    }

    func testRetryRequestCode_ShouldCallRequestCodeService() {
        sut.retryRequestCode()

        XCTAssertEqual(serviceMock.callRequestCodeCount, 1)
    }

    func testRetryRequestCode_ShouldTrackEvent() {
        serviceMock.requestCodeResult = .success(CodeConfirmation(retryTime: 300))
        sut.retryRequestCode()

        let expectedEvent = PasswordResetTracker(method: .sms, eventType: AuthorizationCodeEvent.resend).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testBackAction_ShouldCallPresenterGoBack() {
        sut.backAction()

        XCTAssertEqual(presenterSpy.callGoBackCount, 1)
    }

    func testTextfieldEditingChange_WhenTextIsNil_ShouldCallDisableConfirmButton() {
        sut.textfieldEditingChange(nil)

        XCTAssertEqual(presenterSpy.callDisableConfirmButtonCount, 1)
    }

    func testTextfieldEditingChange_WhenCharacterCountIsLessThanFour_ShouldCallDisableConfirmButton() {
        sut.textfieldEditingChange("123")

        XCTAssertEqual(presenterSpy.callDisableConfirmButtonCount, 1)
        XCTAssertEqual(presenterSpy.trimmedText, "123")
    }

    func testTextfieldEditingChange_WhenCharacterCountIsGreaterThanFour_ShouldTrimCharacters() {
        sut.textfieldEditingChange("12345")

        XCTAssertEqual(presenterSpy.callEnableConfirmButtonCount, 1)
        XCTAssertEqual(presenterSpy.trimmedText, "1234")
    }

    func testTextfieldEditingChange_WhenCharacterCountIsEqualToFour_ShouldCallEnableConfirmButton() {
        sut.textfieldEditingChange("1234")

        XCTAssertEqual(presenterSpy.callEnableConfirmButtonCount, 1)
        XCTAssertEqual(presenterSpy.trimmedText, "1234")
    }

    func testConfirmCode_WhenCharacterCountIsLessThanFour_ShouldDoNothing() {
        sut.confirmCode("123")

        XCTAssertEqual(serviceMock.callValidateCodeCount, 0)
    }

    func testConfirmCode_WhenCharacterCountIsGreaterThanFour_ShouldDoNothing() {
        sut.confirmCode("12345")

        XCTAssertEqual(serviceMock.callValidateCodeCount, 0)
    }

    func testConfirmCode_WhenCharacterCountIsEqualToFour_ShouldStartAndStopLoading() {
        sut.confirmCode("1234")

        XCTAssertEqual(presenterSpy.callStartLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callStopLoadingCount, 1)
    }

    func testConfirmCode_WhenCharacterCountIsEqualToFour_WhenValidateIsSuccess_ShouldCallGoToNextStep() {
        serviceMock.validateCodeResult = .success(PasswordResetStepData(passwordResetId: "", nextStep: .newPassword))

        sut.confirmCode("1234")

        XCTAssertEqual(presenterSpy.callGoToNextStepCount, 1)
    }

    func testConfirmCode_WhenCharacterCountIsEqualToFour_WhenValidateIsFailure_ShouldCallPresentError() {
        sut.confirmCode("1234")

        XCTAssertEqual(presenterSpy.callPresentValidationErrorCount, 1)
    }

    func testConfirmCode_WhenValidateIsSuccess_ShouldTrackSuccessEvent() {
        serviceMock.validateCodeResult = .success(PasswordResetStepData(passwordResetId: "", nextStep: .newPassword))

        sut.confirmCode("1234")

        let expectedEvent = PasswordResetTracker(method: .sms, eventType: AuthorizationCodeEvent.success).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testConfirmCode_WhenValidateIsFailure_WhenErrorCodeIsInvalid_ShouldTrackErrorEvent() {
        var requestError = RequestError()
        requestError.code = "invalid_code"
        serviceMock.validateCodeResult = .failure(.unauthorized(body: requestError))
        sut.confirmCode("1234")

        let expectedEvent = PasswordResetTracker(method: .sms, eventType: AuthorizationCodeEvent.invalidCode).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testConfirmCode_WhenValidateIsFailure_WhenErrorCodeIsBlock_ShouldTrackErrorEvent() {
        var requestError = RequestError()
        requestError.code = "password_reset_block"
        serviceMock.validateCodeResult = .failure(.unauthorized(body: requestError))
        sut.confirmCode("1234")

        let expectedEvent = PasswordResetTracker(method: .sms, eventType: AuthorizationCodeEvent.attemptLimit).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testConfirmCode_WhenValidateIsFailure_ShouldTrackErrorEvent() {
        sut.confirmCode("1234")

        let expectedEvent = PasswordResetTracker(method: .sms, eventType: ErrorEvent.generic).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testTimerTic_WhenCountDownIsEqualOrLessThanZero_ShouldCallTimerStop() {
        serviceMock.requestCodeResult = .success(CodeConfirmation(retryTime: -1))
        sut.retryRequestCode()

        sut.timerTic()

        XCTAssertEqual(timerSpy.callStopCount, 1)
    }

    func testTimerTic_WhenCountDownIsEqualOrLessThanZero_ShouldCallSetupRetryButtonTitleEnabled() {
        serviceMock.requestCodeResult = .success(CodeConfirmation(retryTime: -1))
        sut.retryRequestCode()

        sut.timerTic()

        XCTAssertEqual(presenterSpy.callSetupRetryButtonTitleCount, 1)
        XCTAssertTrue(presenterSpy.isRetryButtonEnabled)
    }

    func testTimerTic_WhenCountDownIsGreaterThanZero_ShouldCallSetupRetryButtonTitleDisabled() {
        serviceMock.requestCodeResult = .success(CodeConfirmation(retryTime: 1))
        sut.retryRequestCode()

        sut.timerTic()

        XCTAssertEqual(presenterSpy.callSetupRetryButtonTitleCount, 1)
        XCTAssertFalse(presenterSpy.isRetryButtonEnabled)
    }
}
