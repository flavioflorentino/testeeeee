import Core
@testable import PasswordReset
import UI
import XCTest

private final class UserIdentificationDisplaySpy: UserIdentificationDisplay {
    private(set) var displayButtonEnableCallCount = 0
    private(set) var isButtonEnabled = false
    private(set) var errorMessageDisplayed: String?
    private(set) var displayLoadingCallCount = 0
    private(set) var displayPopupCallCount = 0
    private(set) var popupPropertiesDisplayed: PopupProperties?
    
    func displayButtonEnabled(_ enabled: Bool, errorMessage: String?) {
        displayButtonEnableCallCount += 1
        isButtonEnabled = enabled
        errorMessageDisplayed = errorMessage
    }
    
    func displayLoading(_ loading: Bool) {
        displayLoadingCallCount += 1
    }
    
    func displayPopup(with popupProperties: PopupProperties) {
        displayPopupCallCount += 1
        popupPropertiesDisplayed = popupProperties
    }
}

private final class UserIdentificationCoordinatingSpy: UserIdentificationCoordinating {
    weak var delegate: UserIdentificationCoordinatorDelegate?
    var viewController: UIViewController?
    
    private(set) var performActionCallCount = 0
    private(set) var actionPerformed: UserIdentificationAction?
    
    func perform(action: UserIdentificationAction) {
        performActionCallCount += 1
        actionPerformed = action
    }
}

final class UserIdentificationPresenterTests: XCTestCase {
    private let coordinatorSpy = UserIdentificationCoordinatingSpy()
    private let controllerSpy = UserIdentificationDisplaySpy()
    
    private lazy var sut: UserIdentificationPresenter = {
        let presenter = UserIdentificationPresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()
    
    func testShouldEnableButton_WhenEnableIsTrueAndDocumentIsComplete_ShouldDisplayButtonEnable() {
        sut.shouldEnableButton(true, isCompleteDocument: true)
        
        XCTAssertEqual(controllerSpy.displayButtonEnableCallCount, 1)
        XCTAssertNil(controllerSpy.errorMessageDisplayed)
        XCTAssertTrue(controllerSpy.isButtonEnabled)
    }
    
    func testShouldEnableButton_WhenEnableIsFalseAndIsDocumentIsComplete_ShouldDisplayButtonDisabled() {
        sut.shouldEnableButton(false, isCompleteDocument: true)
        
        XCTAssertEqual(controllerSpy.displayButtonEnableCallCount, 1)
        XCTAssertEqual(controllerSpy.errorMessageDisplayed, Strings.Identification.cpfDocumentError)
        XCTAssertFalse(controllerSpy.isButtonEnabled)
    }
    
    func testShowLoading_WhenCalledFromViewModel_ShouldDisplayLoadingOnViewController() {
        sut.showLoading(true)
        
        XCTAssertEqual(controllerSpy.displayLoadingCallCount, 1)
    }
    
    func testPresentError_WhenCalledFromViewModel_ShouldDisplayErrorPopupOnViewController() {
        sut.presentError(RequestError())
        
        XCTAssertEqual(controllerSpy.displayPopupCallCount, 1)
    }
    
    func testPresentError_WhenErrorIsNil_ShouldDisplayErrorPopupWithDefaultTitleAndMessage() {
        sut.presentError(nil)
        
        XCTAssertEqual(controllerSpy.displayPopupCallCount, 1)
        XCTAssertEqual(controllerSpy.popupPropertiesDisplayed?.title, Strings.Error.title)
        XCTAssertEqual(controllerSpy.popupPropertiesDisplayed?.message, Strings.Error.message)
    }
    
    func testPresentResetChannels_WhenCalledFromViewModel_ShouldPerformActionOnCoordinator() {
        let channels = PasswordResetChannels(
            channels: [
                Channel(channelType: .sms, title: "", icon: "", data: "")
            ],
            fallback: nil
        )
        
        sut.presentResetChannels(channels, for: "")
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
    }
    
    func testClose_ShouldCallCoordinatorWithCloseAction() {
        sut.close()
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.actionPerformed, .close)
    }
}
