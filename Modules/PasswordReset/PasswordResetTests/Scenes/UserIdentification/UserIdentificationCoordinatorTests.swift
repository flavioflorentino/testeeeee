@testable import PasswordReset
import XCTest

private final class UserIdentificationCoordinatorDelegateSpy: UserIdentificationCoordinatorDelegate {
    private(set) var didReceivedResetChannelsCallCount = 0
    private(set) var didCloseCallCount = 0
    
    func didReceivedResetChannels(_ channelsData: PasswordResetChannels, for document: String) {
        didReceivedResetChannelsCallCount += 1
    }
    
    func didClose() {
        didCloseCallCount += 1
    }
}

final class UserIdentificationCoordinatorTests: XCTestCase {
    private lazy var delegateSpy = UserIdentificationCoordinatorDelegateSpy()
    
    let smsChannel = Channel(channelType: .sms, title: "SMS", icon: "", data: "****-9999")
    let biometricChannel = Channel(channelType: .biometric, title: "Fallback", icon: "", data: "")
    private lazy var channelsData = PasswordResetChannels(channels: [smsChannel, biometricChannel], fallback: nil)
    
    private lazy var sut: UserIdentificationCoordinator = {
        let coordinator = UserIdentificationCoordinator()
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerform_WhenActionIsChannels_ShouldCallDelegateDidReceivedResetChannels() {
        sut.perform(action: .channels(channelsData, document: ""))
        
        XCTAssertEqual(delegateSpy.didReceivedResetChannelsCallCount, 1)
    }
    
    func testPerform_WhenActionIsClose_ShouldCallDelegateDidClose() {
        sut.perform(action: .close)
        
        XCTAssertEqual(delegateSpy.didCloseCallCount, 1)
    }
}
