import AnalyticsModule
import Core
@testable import PasswordReset
import XCTest

private final class UserIdentificationServicingMock: UserIdentificationServicing {
    var channelsExpectedResult: Result<PasswordResetChannels, ApiError>?
    
    func channels(document: String, completion: @escaping ((Result<PasswordResetChannels, ApiError>) -> Void)) {
        guard let expectedResult = channelsExpectedResult else {
            return
        }
        completion(expectedResult)
    }
}

private final class UserIdentificationPresentingSpy: UserIdentificationPresenting {
    var viewController: UserIdentificationDisplay?
    private(set) var shouldEnableButtonCallCount = 0
    private(set) var isEnabled = false
    private(set) var completeDocument = false
    private(set) var showLoadingCallCount = 0
    private(set) var presentErrorCallCount = 0
    private(set) var presentedRequestError: RequestError?
    private(set) var presentResetChannelsCallCount = 0
    private(set) var receivedResetChannels: [Channel] = []
    private(set) var closeCallCount = 0
    
    func shouldEnableButton(_ enable: Bool, isCompleteDocument: Bool) {
        shouldEnableButtonCallCount += 1
        isEnabled = enable
        completeDocument = isCompleteDocument
    }
    
    func showLoading(_ loading: Bool) {
        showLoadingCallCount += 1
    }
    
    func presentResetChannels(_ channels: PasswordResetChannels, for document: String) {
        presentResetChannelsCallCount += 1
        receivedResetChannels = channels.channels
    }
    
    func presentError(_ error: RequestError?) {
        presentErrorCallCount += 1
        presentedRequestError = error
    }
    
    func close() {
        closeCallCount += 1
    }
}

final class UserIdentificationInteractorTests: XCTestCase {
    private let serviceMock = UserIdentificationServicingMock()
    private let presenterSpy = UserIdentificationPresentingSpy()
    
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)
    
    private lazy var sut = UserIdentificationInteractor(service: serviceMock, presenter: presenterSpy, dependencies: dependencies)
    
    func testCheckDocument_WhenIsValidDocument_ShouldEnableButton() {
        let document = "517.188.214-25"
        
        sut.checkDocument(document)
        
        XCTAssertTrue(presenterSpy.isEnabled)
        XCTAssertTrue(presenterSpy.completeDocument)
        XCTAssertEqual(presenterSpy.shouldEnableButtonCallCount, 1)
    }
    
    func testConfirm_WhenCalledFromViewController_ShouldPresentLoading() {
        sut.confirm("111.111.111-11")
        
        XCTAssertEqual(presenterSpy.showLoadingCallCount, 1)
    }
    
    func testConfirm_WhenServerHasError_ShouldPresentError() {
        serviceMock.channelsExpectedResult = .failure(.serverError)
        sut.confirm("xxxx")
        
        XCTAssertEqual(presenterSpy.presentErrorCallCount, 1)
    }

    func testConfirm_WhenServerHasError_ShouldTrackErrorEvent() {
        serviceMock.channelsExpectedResult = .failure(.serverError)
        sut.confirm("xxxx")

        let expectedEvent = PasswordResetTracker(eventType: UserIdentificationEvent.error).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testConfirm_WhenServerHasErrorWithRequestError_ShouldPresentErrorWithRequestError() {
        var error = RequestError()
        error.title = "my request error"
        
        serviceMock.channelsExpectedResult = .failure(.badRequest(body: error))
        sut.confirm("xxxx")

        XCTAssertEqual(presenterSpy.presentErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.presentedRequestError?.title, "my request error")
    }

    func testConfirm_WhenServerHasErrorWithBlockedCodeError_ShouldTrackBlockedEvent() {
        var error = RequestError()
        error.code = "password_reset_block"

        serviceMock.channelsExpectedResult = .failure(.unauthorized(body: error))
        sut.confirm("xxxx")

        let expectedEvent = PasswordResetTracker(eventType: ErrorEvent.blocked).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testConfirm_WhenServerHasSuccess_ShouldTrackSuccessEvent() {
        let channels = PasswordResetChannels(channels: [], fallback: nil)
        serviceMock.channelsExpectedResult = .success(channels)
        sut.confirm("xxxx")

        let expectedEvent = PasswordResetTracker(eventType: UserIdentificationEvent.success).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testConfirm_WhenServerHasSuccess_ShouldPresentResetChannels() {
        let channels = PasswordResetChannels(
            channels: [
                Channel(channelType: .sms, title: "por sms", icon: "", data: ""),
                Channel(channelType: .email, title: "por email", icon: "", data: "")
            ],
            fallback: nil
        )
        
        serviceMock.channelsExpectedResult = .success(channels)
        sut.confirm("xxxx")
        
        XCTAssertEqual(presenterSpy.presentResetChannelsCallCount, 1)
        XCTAssertEqual(presenterSpy.receivedResetChannels.count, 2)
        XCTAssertEqual(presenterSpy.receivedResetChannels[0].channelType, .sms)
        XCTAssertEqual(presenterSpy.receivedResetChannels[1].channelType, .email)
    }
    
    func testCheckDocument_WhenDocumentIsCompleteAndIsInvalid_ShouldSendAnalyticsEvent() {
        let document = "111.111.111-11"
        sut.checkDocument(document)
        
        XCTAssertFalse(presenterSpy.isEnabled)
        XCTAssertTrue(presenterSpy.completeDocument)
        XCTAssertEqual(presenterSpy.shouldEnableButtonCallCount, 1)
        
        let expectedEvent = PasswordResetTracker(eventType: UserIdentificationEvent.invalidCpf).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testSendAnalyticsEvent_WhenCalledFromViewController_ShouldSendEvent() {
        sut.sendAnalyticsEvent()
        
        let expectedEvent = PasswordResetTracker(eventType: UserIdentificationEvent.accessed).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testClose_WhenCalledFromViewController_ShouldCallPresenterWithClose() {
        sut.close()
        
        XCTAssertEqual(presenterSpy.closeCallCount, 1)
    }
}
