@testable import PasswordReset
import UI
import XCTest

private final class ResetChannelsCoordinatorDelegateSpy: ResetChannelsCoordinatorDelegate {
    private(set) var didReceiveChannelNextStepCallCount = 0
    private(set) var didReceiveFallbackNextStepChannelCallCount = 0
    
    func didReceiveChannelNextStep(_ stepData: PasswordResetStepData, channel: Channel) {
        didReceiveChannelNextStepCallCount += 1
    }
}

final class ResetChannelsCoordinatorTests: XCTestCase {
    private lazy var delegateSpy = ResetChannelsCoordinatorDelegateSpy()
    
    let smsChannel = Channel(channelType: .sms, title: "SMS", icon: "", data: "****-9999")
    let biometricChannel = Channel(channelType: .biometric, title: "Fallback", icon: "", data: "")
    
    private lazy var sut: ResetChannelsCoordinator = {
        let coordinator = ResetChannelsCoordinator()
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerform_WhenActionIsChannelNextStep_ShouldCallDelegatedidReceiveChannelNextStep() {
        sut.perform(
            action: .channelNextStep(stepData: PasswordResetStepData(passwordResetId: "abc", nextStep: .code),
            channel: smsChannel)
        )
        
        XCTAssertEqual(delegateSpy.didReceiveChannelNextStepCallCount, 1)
    }
}
