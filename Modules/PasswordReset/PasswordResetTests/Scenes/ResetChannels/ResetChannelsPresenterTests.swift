import Core
@testable import PasswordReset
import UI
import XCTest

private final class ResetChannelsDisplaySpy: ResetChannelsDisplay {
    private(set) var displayChannelViewsCallCount = 0
    private(set) var channelsDisplayed: [Channel] = []
    private(set) var displayFallbackCallCount = 0
    private(set) var displayLoadingCallCount = 0
    private(set) var displayPopupCallCount = 0
    private(set) var popupPropertiesDisplayed: PopupProperties?
    
    func displayChannelViews(with channels: [Channel]) {
        displayChannelViewsCallCount += 1
        channelsDisplayed = channels
    }
    
    func displayFallback() {
        displayFallbackCallCount += 1
    }
    
    func displayLoading(_ loading: Bool) {
        displayLoadingCallCount += 1
    }
    
    
    func displayPopup(with popupProperties: PopupProperties) {
        displayPopupCallCount += 1
        popupPropertiesDisplayed = popupProperties
    }
}

private final class ResetChannelsCoordinatingSpy: ResetChannelsCoordinating {
    weak var delegate: ResetChannelsCoordinatorDelegate?
    var viewController: UIViewController?
    
    private(set) var perfomActionCallCount = 0
    private(set) var actionPerformed: ResetChannelsAction?
    
    func perform(action: ResetChannelsAction) {
        perfomActionCallCount += 1
        actionPerformed = action
    }
}

final class ResetChannelsPresenterTests: XCTestCase {
    private let coordinatorSpy = ResetChannelsCoordinatingSpy()
    private let controllerSpy = ResetChannelsDisplaySpy()
    
    let smsChannel = Channel(channelType: .sms, title: "SMS", icon: "", data: "****-9999")
    let biometricChannel = Channel(channelType: .biometric, title: "Fallback", icon: "", data: "")
    
    private lazy var sut: ResetChannelsPresenter = {
        let presenter = ResetChannelsPresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()
    
    func testPresentChannelsData_WhenCalledFromViewModel_ShouldDisplayChannelsOnViewController() {
        let channelsData = PasswordResetChannels(channels: [smsChannel], fallback: nil)
        sut.presentChannelsData(channelsData)
        
        XCTAssertEqual(controllerSpy.displayChannelViewsCallCount, 1)
        XCTAssertEqual(controllerSpy.channelsDisplayed.count, 1)
    }
    
    func testPresentNextStep_ShouldPerformChannelNextStepActionOnCoordinator() {
        let stepData = PasswordResetStepData(passwordResetId: "abc", nextStep: .code)
        sut.presentNextStep(stepData, channel: smsChannel)
        
        XCTAssertEqual(coordinatorSpy.perfomActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.actionPerformed, .channelNextStep(stepData: stepData, channel: smsChannel))
    }
    
    func testPresentFallback_WhenCalledFromViewModel_ShouldDisplayFallbackOnViewController() {
        sut.presentFallback()
        
        XCTAssertEqual(controllerSpy.displayFallbackCallCount, 1)
    }
    
    func testShowLoading_ShouldDisplayLoadingViewOnViewController() {
        sut.showLoading(true)
        
        XCTAssertEqual(controllerSpy.displayLoadingCallCount, 1)
    }
    
    func testPresentError_WhenCalledFromViewModel_ShouldDisplayErrorPopupOnViewController() {
        sut.presentError(RequestError())
        
        XCTAssertEqual(controllerSpy.displayPopupCallCount, 1)
    }
    
    func testPresentError_WhenErrorIsNil_ShouldDisplayErrorPopupWithDefaultTitleAndMessage() {
        sut.presentError(nil)
        
        XCTAssertEqual(controllerSpy.displayPopupCallCount, 1)
        XCTAssertEqual(controllerSpy.popupPropertiesDisplayed?.title, Strings.Error.title)
        XCTAssertEqual(controllerSpy.popupPropertiesDisplayed?.message, Strings.Error.message)
    }
}
