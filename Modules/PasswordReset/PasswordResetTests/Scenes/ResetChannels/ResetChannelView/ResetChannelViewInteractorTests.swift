@testable import PasswordReset
import XCTest

private final class ResetChannelViewPresenterSpy: ResetChannelViewPresenting {
    var viewController: ResetChannelViewDisplay?
    private(set) var configureChannel: Channel?
    private(set) var configureChannelDataCallCount = 0
    private(set) var selectedChannel: Channel?
    private(set) var presentSelectedChannelCallCount = 0
    
    func configureChannelData(_ channel: Channel) {
        configureChannelDataCallCount += 1
        configureChannel = channel
    }
    
    func presentSelectedChannel(_ channel: Channel) {
        presentSelectedChannelCallCount += 1
        selectedChannel = channel
    }
}

final class ResetChannelViewInteractorTests: XCTestCase {
    private let presenterSpy = ResetChannelViewPresenterSpy()
    
    private lazy var sut: ResetChannelViewInteractor = {
        let channel = Channel(channelType: .email, title: "Email", icon: "", data: "***@g*.com")
        let interactor = createInteractorForChannel(channel)
        return interactor
    }()
    
    func testConfigureView_WhenChannelIsEmail_ShouldConfigureEmailChannelOnPresenter() {
        sut.configureView()
        
        XCTAssertEqual(presenterSpy.configureChannelDataCallCount, 1)
        XCTAssertEqual(presenterSpy.configureChannel?.channelType, .email)
        XCTAssertEqual(presenterSpy.configureChannel?.title, "Email")
    }
    
    func testConfigureView_WhenChannelIsSMS_ShouldConfigureSMSChannelOnPresenter() {
        let channel = Channel(channelType: .sms, title: "SMS", icon: "", data: "(**)9****1231")
        let sut = createInteractorForChannel(channel)
        
        sut.configureView()
        
        XCTAssertEqual(presenterSpy.configureChannelDataCallCount, 1)
        XCTAssertEqual(presenterSpy.configureChannel?.channelType, .sms)
        XCTAssertEqual(presenterSpy.configureChannel?.title, "SMS")
    }
    
    func testDidSelect_WhenChannelIsEmail_ShouldPresentEmailChannelOnPresenter() {
        sut.didSelect()
        
        XCTAssertEqual(presenterSpy.presentSelectedChannelCallCount, 1)
        XCTAssertEqual(presenterSpy.selectedChannel?.channelType, .email)
        XCTAssertEqual(presenterSpy.selectedChannel?.title, "Email")
    }
    
    func testDidSelect_WhenChannelIsSMS_ShouldPresentSMSChannelOnPresenter() {
        let channel = Channel(channelType: .sms, title: "SMS", icon: "", data: "(**)9****1231")
        let sut = createInteractorForChannel(channel)
        
        sut.didSelect()
        
        XCTAssertEqual(presenterSpy.presentSelectedChannelCallCount, 1)
        XCTAssertEqual(presenterSpy.selectedChannel?.channelType, .sms)
        XCTAssertEqual(presenterSpy.selectedChannel?.title, "SMS")
    }
}

private extension ResetChannelViewInteractorTests {
    func createInteractorForChannel(_ channel: Channel) -> ResetChannelViewInteractor {
        ResetChannelViewInteractor(presenter: presenterSpy, channel: channel)
    }
}
