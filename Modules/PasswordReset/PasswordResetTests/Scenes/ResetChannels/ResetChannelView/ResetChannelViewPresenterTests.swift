@testable import PasswordReset
import XCTest

private final class ResetChannelViewDisplaySpy: ResetChannelViewDisplay {
    private(set) var configureViewCallCount = 0
    private(set) var configuredChannelTitle: String = ""
    private(set) var displaySelectedChannelCallCount = 0
    private(set) var displayedChannel: Channel?
    
    func configureView(with channelImageURL: URL?, title: String, info: String, placeholderImage: UIImage) {
        configureViewCallCount += 1
        configuredChannelTitle = title
    }
    
    func displaySelectedChannel(_ channel: Channel) {
        displaySelectedChannelCallCount += 1
        displayedChannel = channel
    }
}

final class ResetChannelViewPresenterTests: XCTestCase {
    private let controllerSpy = ResetChannelViewDisplaySpy()
    
    private lazy var sut: ResetChannelViewPresenter = {
        let presenter = ResetChannelViewPresenter()
        presenter.viewController = controllerSpy
        return presenter
    }()
    
    func testConfigureChannelData_WhenChannelIsEmail_ShouldDisplayEmailChannelData() {
        let channel = Channel(channelType: .email, title: "Email", icon: "", data: "***@mail.com")
        sut.configureChannelData(channel)
        
        XCTAssertEqual(controllerSpy.configureViewCallCount, 1)
        XCTAssertEqual(controllerSpy.configuredChannelTitle, "Email")
    }
    
    func testConfigureChannelData_WhenChannelIsSMS_ShouldDisplaySMSChannelData() {
        let channel = Channel(channelType: .sms, title: "SMS", icon: "", data: "****-8859")
        sut.configureChannelData(channel)
        
        XCTAssertEqual(controllerSpy.configureViewCallCount, 1)
        XCTAssertEqual(controllerSpy.configuredChannelTitle, "SMS")
    }
    
    func testConfigureChannelData_WhenChannelIsBiometric_ShouldDisplayBiometricChannelData() {
        let channel = Channel(channelType: .biometric, title: "Biometria", icon: "", data: "")
        sut.configureChannelData(channel)
        
        XCTAssertEqual(controllerSpy.configureViewCallCount, 1)
        XCTAssertEqual(controllerSpy.configuredChannelTitle, "Biometria")
    }
    
    func testPresentSelectedChannel_WhenChannelIsEmail_ShouldDisplayEmailChannel() {
        let channel = Channel(channelType: .email, title: "Email", icon: "", data: "***@mail.com")
        sut.presentSelectedChannel(channel)
        
        XCTAssertEqual(controllerSpy.displaySelectedChannelCallCount, 1)
        XCTAssertEqual(controllerSpy.displayedChannel?.title, "Email")
    }
}
