import AnalyticsModule
import Core
import FeatureFlag
@testable import PasswordReset
import XCTest

private final class ResetChannelsServiceMock: ResetChannelsServicing {
    var nextStepExpectedResult: Result<PasswordResetStepData, ApiError>?
    
    func startResetFlow(
        with channel: ChannelType,
        cpf: String,
        completion: @escaping ((Result<PasswordResetStepData, ApiError>) -> Void)
    ) {
        guard let expectedResult = nextStepExpectedResult else {
            return
        }
        completion(expectedResult)
    }
}

private final class ResetChannelsPresenterSpy: ResetChannelsPresenting {
    var viewController: ResetChannelsDisplay?
    private(set) var presentChannelsCallCount = 0
    private(set) var presentedChannels: [Channel] = []
    private(set) var presentFallbackCallCount = 0
    private(set) var presentNextStepCallCount = 0
    private(set) var receivedStepData: PasswordResetStepData?
    private(set) var showLoadingCallCount = 0
    private(set) var presentErrorCallCount = 0
    
    func presentChannelsData(_ channelsData: PasswordResetChannels) {
        presentChannelsCallCount += 1
        presentedChannels = channelsData.channels
    }
    
    func presentFallback() {
        presentFallbackCallCount += 1
    }
    
    func showLoading(_ loading: Bool) {
        showLoadingCallCount += 1
    }
    
    func presentNextStep(_ stepData: PasswordResetStepData, channel: Channel) {
        presentNextStepCallCount += 1
        receivedStepData = stepData
    }
    
    func presentError(_ error: RequestError?) {
        presentErrorCallCount += 1
    }
}

final class ResetChannelsInteractorTests: XCTestCase {
    private let serviceMock = ResetChannelsServiceMock()
    private let presenterSpy = ResetChannelsPresenterSpy()
    private let analytics = AnalyticsSpy()
    private let featureManagerMock = FeatureManagerMock()
    private let authManagerMock = AuthenticationManagerMock()
    private lazy var dependencies = DependencyContainerMock(analytics, featureManagerMock, authManagerMock)
    
    private lazy var smsChannel = Channel(channelType: .sms, title: "SMS", icon: "", data: "****-9999")
    private lazy var emailChannel = Channel(channelType: .email, title: "Email", icon: "", data: "****@mail.com")
    private lazy var biometricChannel = Channel(channelType: .biometric, title: "Tente de outra maneira", icon: "", data: "")
    
    private lazy var sut = initializeInteractor(channels: [smsChannel, emailChannel, biometricChannel], fallback: nil)

    override func setUp() {
        super.setUp()
        featureManagerMock.override(key: .isPasswordResetEmailAvailable, with: false)
    }
    
    func testConfigureChannels_WhenEmailFlagIsTrue_ShouldPresentAllChannels() {
        featureManagerMock.override(key: .isPasswordResetEmailAvailable, with: true)

        sut.configureChannels()
        
        XCTAssertEqual(presenterSpy.presentChannelsCallCount, 1)
        XCTAssertEqual(presenterSpy.presentedChannels.count, 3)
    }

    func testConfigureChannels_WhenEmailFlagIsFalse_ShouldPresentFilteredChannels() {
        sut.configureChannels()

        XCTAssertEqual(presenterSpy.presentChannelsCallCount, 1)
        XCTAssertEqual(presenterSpy.presentedChannels, [smsChannel, biometricChannel])
    }

    func testConfigureChannels_WhenUserIsNotAuthenticated_ShouldPresentAllChannels() {
        sut.configureChannels()

        XCTAssertEqual(presenterSpy.presentChannelsCallCount, 1)
        XCTAssertEqual(presenterSpy.presentedChannels.count, 2)
    }

    func testConfigureChannels_WhenUserIsNotAuthenticatedAndFallbackIsNil_ShouldNotCallPresentFallback() {
        sut.configureChannels()

        XCTAssertEqual(presenterSpy.presentFallbackCallCount, 0)
    }

    func testConfigureChannels_WhenUserIsNotAuthenticatedAndFallbackIsNotNil_ShouldCallPresentFallback() {
        sut = initializeInteractor(channels: [smsChannel], fallback: biometricChannel)

        sut.configureChannels()

        XCTAssertEqual(presenterSpy.presentFallbackCallCount, 1)
    }

    func testConfigureChannels_WhenUserIsAuthenticated_ShouldPresentFilteredChannels() {
        authManagerMock.authenticated = true
        sut = initializeInteractor(channels: [smsChannel, biometricChannel], fallback: nil)

        sut.configureChannels()

        XCTAssertEqual(presenterSpy.presentChannelsCallCount, 1)
        XCTAssertEqual(presenterSpy.presentedChannels, [smsChannel])
    }

    func testConfigureChannels_WhenUserIsAuthenticated_ShouldNotCallPresentFallback() {
        authManagerMock.authenticated = true
        sut = initializeInteractor(channels: [smsChannel, biometricChannel], fallback: biometricChannel)

        sut.configureChannels()

        XCTAssertEqual(presenterSpy.presentFallbackCallCount, 0)
    }
    
    func testSelectChannel_WhenServiceHasSucceeded_ShouldCallPresenterWithStepReceived() {
        serviceMock.nextStepExpectedResult = .success(PasswordResetStepData(passwordResetId: "abc", nextStep: .code))
        sut.selectChannel(smsChannel)
        
        XCTAssertEqual(presenterSpy.presentNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.receivedStepData?.passwordResetId, "abc")
        XCTAssertEqual(presenterSpy.receivedStepData?.nextStep, .code)
        XCTAssertEqual(presenterSpy.showLoadingCallCount, 2)
    }
    
    func testSelectChannel_WhenServiceReceivesGenericError_ShouldCallPresentError() {
        serviceMock.nextStepExpectedResult = .failure(.serverError)
        sut.selectChannel(smsChannel)
        
        XCTAssertEqual(presenterSpy.presentErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.showLoadingCallCount, 2)
    }
    
    func testSelectFallback_WhenFallbackIsNotNil_ShouldCallPresenterWithNextStepReceived() {
        let channelsData = PasswordResetChannels(
            channels: [smsChannel, emailChannel],
            fallback: Channel(channelType: .biometric, title: "", icon: "", data: "")
        )
        
        let interactor = ResetChannelsInteractor(
            service: serviceMock,
            presenter: presenterSpy,
            channelsData: channelsData,
            document: "",
            dependencies: dependencies
        )
        
        serviceMock.nextStepExpectedResult = .success(PasswordResetStepData(passwordResetId: "def", nextStep: .code))
        interactor.selectFallback()
        
        XCTAssertEqual(presenterSpy.receivedStepData?.passwordResetId, "def")
        XCTAssertEqual(presenterSpy.receivedStepData?.nextStep, .code)
    }
    
    func testSelectFallback_WhenFallbackIsNil_ShouldCallPresenterPresentError() {
        sut.selectFallback()
        
        XCTAssertEqual(presenterSpy.presentNextStepCallCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorCallCount, 1)
    }
    
    func testSelectChannel_WhenServiceReceivesGenericError_ShouldSendAnalyticsEvent() {
        serviceMock.nextStepExpectedResult = .failure(.serverError)

        sut.selectChannel(smsChannel)

        let appearEvent = PasswordResetTracker(method: smsChannel.channelType,
                                               eventType: ResetChannelsEvent.selectChannel).event()
        let errorEvent = PasswordResetTracker(method: smsChannel.channelType,
                                              eventType: ErrorEvent.generic).event()
        XCTAssertTrue(analytics.equals(to: appearEvent, errorEvent))
    }

    func testSelectChannel_WhenServiceReceivesBlockedError_ShouldSendAnalyticsEvent() {
        var error = RequestError()
        error.code = "password_reset_block"
        serviceMock.nextStepExpectedResult = .failure(.unauthorized(body: error))

        sut.selectChannel(smsChannel)

        let appearEvent = PasswordResetTracker(method: smsChannel.channelType, eventType: ResetChannelsEvent.selectChannel).event()
        let errorEvent = PasswordResetTracker(method: smsChannel.channelType, eventType: ErrorEvent.blocked).event()
        XCTAssertTrue(analytics.equals(to: appearEvent, errorEvent))
    }

    func testSelectFallback_WhenFallIsNotNil_ShoulSendAnalyticsEvent() {
        let channelsData = PasswordResetChannels(
            channels: [smsChannel, emailChannel],
            fallback: Channel(channelType: .biometric, title: "", icon: "", data: "")
        )
        
        let interactor = ResetChannelsInteractor(
            service: serviceMock,
            presenter: presenterSpy,
            channelsData: channelsData,
            document: "",
            dependencies: dependencies
        )
        
        interactor.selectFallback()

        let expectedEvent = PasswordResetTracker(method: .biometric,eventType: ResetChannelsEvent.selectChannel).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    private func initializeInteractor(channels: [Channel], fallback: Channel?) -> ResetChannelsInteractor {
        let channelsData = PasswordResetChannels(channels: channels, fallback: fallback)
        return ResetChannelsInteractor(service: serviceMock,
                                       presenter: presenterSpy,
                                       channelsData: channelsData,
                                       document: "",
                                       dependencies: dependencies)
    }
}
