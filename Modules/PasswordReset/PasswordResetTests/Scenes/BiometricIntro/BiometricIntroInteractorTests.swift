import AnalyticsModule
@testable import PasswordReset
import XCTest

private final class BiometricIntroPresenterSpy: BiometricIntroPresenting {
    var viewController: BiometricIntroDisplay?
    private(set) var configureViewCallCount = 0
    private(set) var presentEmailVerificationStepCallCount = 0
    private(set) var presentPreviousStepCallCount = 0
    
    func configureView() {
        configureViewCallCount += 1
    }
    
    func presentEmailVerificationStep() {
        presentEmailVerificationStepCallCount += 1
    }
    
    func presentPreviousStep() {
        presentPreviousStepCallCount += 1
    }
}

final class BiometricIntroInteractorTests: XCTestCase {
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)
    
    private lazy var presenterSpy = BiometricIntroPresenterSpy()
    private lazy var sut = BiometricIntroInteractor(presenter: presenterSpy, dependencies: dependencies)
    
    func testSetupView_WhenCalledFromViewController_ShouldCallPresenterToConfigureView() {
        sut.setupView()
        
        XCTAssertEqual(presenterSpy.configureViewCallCount, 1)
    }
    
    func testSetupView_WhenCalledFromViewController_ShouldSendAnalyticsEvent() {
        sut.setupView()
        
        let expectedEvent = PasswordResetTracker(eventType: FallbackEvent.intro).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testStart_WhenCalledFromViewController_ShouldPresentEmailVerificationStep() {
        sut.start()
        
        XCTAssertEqual(presenterSpy.presentEmailVerificationStepCallCount, 1)
    }
    
    func testGoBack_WhenCalledFromViewController_ShouldCallPresenterToShowPreviousStep() {
        sut.goBack()
        
        XCTAssertEqual(presenterSpy.presentPreviousStepCallCount, 1)
    }
}
