@testable import PasswordReset
import XCTest

private final class BiometricIntroDisplaySpy: BiometricIntroDisplay {
    private(set) var displayStepInfoCallCount = 0
    private(set) var stepImage = UIImage()
    private(set) var stepTitle = String()
    private(set) var stepDescription = String()
    private(set) var stepButtonTitle = String()
    
    func displayStepInfo(with image: UIImage, title: String, description: String, buttonTitle: String) {
        displayStepInfoCallCount += 1
        stepImage = image
        stepTitle = title
        stepDescription = description
        stepButtonTitle = buttonTitle
    }
}

private final class BiometricIntroCoordinatorSpy: BiometricIntroCoordinating {
    var viewController: UIViewController?
    var delegate: BiometricIntroCoordinatorDelegate?
    
    private(set) var performActionCallCount = 0
    private(set) var actionCalled: BiometricIntroAction?
    
    func perform(action: BiometricIntroAction) {
        performActionCallCount += 1
        actionCalled = action
    }
}

final class BiometricIntroPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = BiometricIntroCoordinatorSpy()
    private lazy var viewControllerSpy = BiometricIntroDisplaySpy()
    
    private lazy var sut: BiometricIntroPresenter = {
        let presenter = BiometricIntroPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testConfigureView_WhenStepIsBiometricIntro_ShouldDisplayStepInfoProperties() {
        sut.configureView()
        
        XCTAssertEqual(viewControllerSpy.displayStepInfoCallCount, 1)
        XCTAssertEqual(viewControllerSpy.stepImage, Assets.resetSuccess.image)
        XCTAssertEqual(viewControllerSpy.stepTitle, Strings.BiometricIntro.title)
        XCTAssertEqual(viewControllerSpy.stepDescription, Strings.BiometricIntro.description)
        XCTAssertEqual(viewControllerSpy.stepButtonTitle, Strings.BiometricIntro.buttonTitle)
    }
    
    func testPresentEmailVerificationStep_ShouldCallCoordinatorWithEmailVerificationAction() {
        sut.presentEmailVerificationStep()
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.actionCalled, .emailVerification)
    }
    
    func testPresentPreviousStep_ShouldCallCoordinatorWithPreviousStepAction() {
        sut.presentPreviousStep()
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.actionCalled, .previousStep)
    }
}

