@testable import PasswordReset
import XCTest

private final class BiometricIntroCoordinatorDelegateSpy: BiometricIntroCoordinatorDelegate {
    private(set) var goBackCallCount = 0
    private(set) var goNextStepCallCount = 0
    
    func biometricIntroGoBack() {
        goBackCallCount += 1
    }
    
    func biometricIntroGoNextStep() {
        goNextStepCallCount += 1
    }
}

final class BiometricIntroCoordinatorTests: XCTestCase {
    private let delegateSpy = BiometricIntroCoordinatorDelegateSpy()

    private lazy var sut: BiometricIntroCoordinator = {
        let coordinator = BiometricIntroCoordinator()
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerform_WhenActionIsPreviousStep_ShouldCallDelegateToGoBack() {
        sut.perform(action: .previousStep)
        
        XCTAssertEqual(delegateSpy.goBackCallCount, 1)
    }
    
    func testPerform_WhenActionIsEmailVerification_ShouldCallDelegateToGoNextStep() {
        sut.perform(action: .emailVerification)
        
        XCTAssertEqual(delegateSpy.goNextStepCallCount, 1)
    }
}
