@testable import PasswordReset
import XCTest

private final class ValidationStatusCoordinatorDelegateSpy: ValidationStatusCoordinatorDelegate {
    private(set) var didCloseCallCount = 0
    
    func validationStatusDidClose() {
        didCloseCallCount += 1
    }
}

final class ValidationStatusCoordinatorTests: XCTestCase {
    private let delegateSpy = ValidationStatusCoordinatorDelegateSpy()

    private lazy var sut: ValidationStatusCoordinator = {
        let coordinator = ValidationStatusCoordinator()
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerform_WhenActionIsClose_ShouldCallDelegateDidClose() {
        sut.perform(action: .close)
        
        XCTAssertEqual(delegateSpy.didCloseCallCount, 1)
    }
}
