import AssetsKit
@testable import PasswordReset
import XCTest

private final class ValidationStatusDisplaySpy: ValidationStatusDisplaying {
    private(set) var displayStepInfoCallCount = 0
    private(set) var stepImage = UIImage()
    private(set) var stepTitle = String()
    private(set) var stepDescription = String()
    private(set) var stepButtonTitle = String()
    
    func displayStepInfo(with image: UIImage, title: String, description: String, buttonTitle: String) {
        displayStepInfoCallCount += 1
        stepImage = image
        stepTitle = title
        stepDescription = description
        stepButtonTitle = buttonTitle
    }
}

private final class ValidationStatusCoordinatorSpy: ValidationStatusCoordinating {
    var viewController: UIViewController?
    var delegate: ValidationStatusCoordinatorDelegate?
    private(set) var performActionCallCount = 0
    private(set) var actionCalled: ValidationStatusAction?
    
    func perform(action: ValidationStatusAction) {
        performActionCallCount += 1
        actionCalled = action
    }
}

final class ValidationStatusPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = ValidationStatusCoordinatorSpy()
    private lazy var viewControllerSpy = ValidationStatusDisplaySpy()
    
    private lazy var sut: ValidationStatusPresenter = {
        let presenter = ValidationStatusPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testConfigureView_WhenStepIsNotApproved_ShouldDisplayRejectedInfoProperties() {
        sut.configureView(for: .notApproved)
        
        XCTAssertEqual(viewControllerSpy.displayStepInfoCallCount, 1)
        XCTAssertEqual(viewControllerSpy.stepImage, Resources.Illustrations.iluRegisterDenied.image)
        XCTAssertEqual(viewControllerSpy.stepTitle, Strings.ValidationRejected.title)
        XCTAssertEqual(viewControllerSpy.stepDescription, Strings.ValidationRejected.description)
        XCTAssertEqual(viewControllerSpy.stepButtonTitle, Strings.General.understood)
    }
    
    func testConfigureView_WhenStepIsManualReview_ShouldDisplayWaitingAnalysisInfoProperties() {
        sut.configureView(for: .manualReview)
        
        XCTAssertEqual(viewControllerSpy.displayStepInfoCallCount, 1)
        XCTAssertEqual(viewControllerSpy.stepImage, Resources.Illustrations.iluClock.image)
        XCTAssertEqual(viewControllerSpy.stepTitle, Strings.WaitingAnalysis.title)
        XCTAssertEqual(viewControllerSpy.stepDescription, Strings.WaitingAnalysis.description)
        XCTAssertEqual(viewControllerSpy.stepButtonTitle, Strings.General.understood)
    }
    
    func testClose_WhenCalledFromInteractor_ShouldCallCoordinatorWithCloseAction() {
        sut.close()
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.actionCalled, .close)
    }
}
