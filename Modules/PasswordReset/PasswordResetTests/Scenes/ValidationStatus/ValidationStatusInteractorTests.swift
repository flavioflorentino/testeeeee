import AnalyticsModule
@testable import PasswordReset
import XCTest

private final class ValidationStatusPresenterSpy: ValidationStatusPresenting {
    var viewController: ValidationStatusDisplaying?
    private(set) var configureViewCallCount = 0
    private(set) var closeCallCount = 0
    
    func configureView(for step: PasswordResetStep) {
        configureViewCallCount += 1
    }
    
    func close() {
        closeCallCount += 1
    }
}

final class ValidationStatusInteractorTests: XCTestCase {
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)
    
    private lazy var presenterSpy = ValidationStatusPresenterSpy()
    private lazy var sut = ValidationStatusInteractor(presenter: presenterSpy, dependencies: dependencies, step: .notApproved)
    
    func testSetupView_WhenCalledFromViewController_ShouldCallPresenterConfigureView() {
        sut.setupView()
        
        XCTAssertEqual(presenterSpy.configureViewCallCount, 1)
    }
    
    func testSetupView_WhenStepIsNotApproved_ShouldSendValidationRejectedEvent() {
        sut.setupView()
        
        let expectedEvent = PasswordResetTracker(eventType: BiometricEvent.rejected).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testSetupView_WhenStepIsManualReview_ShouldSendWaitingAnalysisEvent() {
        sut = ValidationStatusInteractor(presenter: presenterSpy, dependencies: dependencies, step: .manualReview)
        sut.setupView()
        
        let expectedEvent = PasswordResetTracker(eventType: WaitingAnalysisEvent.formSuccess).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testUnderstood_WhenCalledFromViewController_ShouldCallPresenterClose() {
        sut.understood()
        
        XCTAssertEqual(presenterSpy.closeCallCount, 1)
    }
}
