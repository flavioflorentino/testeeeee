@testable import PasswordReset
import XCTest

private class LinkValidationCoordinatorDelegateSpy: LinkValidationCoordinatorDelegate {
    private(set) var linkValidationNextStepCallCount = 0
    private(set) var linkValidationErrorCallCount = 0
    private(set) var linkValidationCloseCallCount = 0

    func linkValidationNextStep(_ stepData: PasswordResetStepData) {
        linkValidationNextStepCallCount += 1
    }

    func linkValidationError(code: PasswordResetErrorCode, title: String?, message: String?) {
        linkValidationErrorCallCount += 1
    }

    func linkValidationClose() {
        linkValidationCloseCallCount += 1
    }
}

final class LinkValidationCoordinatorTests: XCTestCase {
    private let delegateSpy = LinkValidationCoordinatorDelegateSpy()

    private lazy var sut: LinkValidationCoordinator = {
        let coordinator = LinkValidationCoordinator()
        coordinator.delegate = delegateSpy
        return coordinator
    }()

    func testPerform_WhenActionIsNextStep_ShouldCallDelegateNextStep() {
        sut.perform(action: .nextStep(PasswordResetStepData(passwordResetId: "abc", nextStep: .newPassword)))

        XCTAssertEqual(delegateSpy.linkValidationNextStepCallCount, 1)
    }

    func testPerform_WhenActionIsError_ShouldCallDelegateError() {
        sut.perform(action: .error(code: .unkown, title: nil, message: nil))

        XCTAssertEqual(delegateSpy.linkValidationErrorCallCount, 1)
    }

    func testPerform_WhenActionIsClose_ShouldCallDelegateClose() {
        sut.perform(action: .close)

        XCTAssertEqual(delegateSpy.linkValidationCloseCallCount, 1)
    }
}
