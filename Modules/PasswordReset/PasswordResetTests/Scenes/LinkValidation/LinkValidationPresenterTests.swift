@testable import PasswordReset
import UI
import XCTest

private class LinkValidationCoordinatingSpy: LinkValidationCoordinating {
    weak var delegate: LinkValidationCoordinatorDelegate?
    private(set) var performCallCount = 0

    func perform(action: LinkValidationAction) {
        performCallCount += 1
    }
}

private class LinkValidationDisplayingSpy: LinkValidationDisplaying {
    var loadingView = LoadingView()
    private(set) var startLoadingViewCallCount = 0
    private(set) var stopLoadingViewCallCount = 0
    private(set) var showErrorPopupControllerCallCount = 0
    private(set) var popupProperties: PopupProperties?

    func startLoadingView() {
        startLoadingViewCallCount += 1
    }

    func stopLoadingView() {
        stopLoadingViewCallCount += 1
    }
}

final class LinkValidationPresenterTests: XCTestCase {
    private let coordinatorSpy = LinkValidationCoordinatingSpy()
    private let controllerSpy = LinkValidationDisplayingSpy()

    private lazy var sut: LinkValidationPresenter = {
        let presenter = LinkValidationPresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()

    func testStartLoading_ShouldStartLoadingView() {
        sut.startLoading()

        XCTAssertEqual(controllerSpy.startLoadingViewCallCount, 1)
    }

    func testStopLoading_ShouldStopLoadingView() {
        sut.stopLoading()

        XCTAssertEqual(controllerSpy.stopLoadingViewCallCount, 1)
    }

    func testDidNextStep_ShouldCallCoordinatorPerform() {
        sut.didNextStep(action: .close)

        XCTAssertEqual(coordinatorSpy.performCallCount, 1)
    }
}
