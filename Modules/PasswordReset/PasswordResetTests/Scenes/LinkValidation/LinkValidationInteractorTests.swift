import Core
@testable import PasswordReset
import XCTest

private class LinkValidationServicingMock: LinkValidationServicing {
    var result: Result<PasswordResetStepData, ApiError> = .failure(ApiError.connectionFailure)

    func validateCode(code: String, completion: @escaping (Result<PasswordResetStepData, ApiError>) -> Void) {
        completion(result)
    }
}

private class LinkValidationPresentingSpy: LinkValidationPresenting {
    weak var viewController: LinkValidationDisplaying?
    private(set) var startLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0
    private(set) var didNextStepCallCount = 0
    private(set) var action: LinkValidationAction?

    func startLoading() {
        startLoadingCallCount += 1
    }

    func stopLoading() {
        stopLoadingCallCount += 1
    }

    func didNextStep(action: LinkValidationAction) {
        didNextStepCallCount += 1
        self.action = action
    }
}

final class LinkValidationInteractorTests: XCTestCase {
    private let serviceMock = LinkValidationServicingMock()
    private let presenterSpy = LinkValidationPresentingSpy()

    private lazy var sut = LinkValidationInteractor(code: "code", service: serviceMock, presenter: presenterSpy)

    func testValidateCode_ShouldStartAndStopLoading() {
        sut.validateCode()

        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallCount, 1)
    }

    func testValidateCode_WhenResultIsSuccess_ShouldCallPresenterNextStepAction() {
        let stepData = PasswordResetStepData(passwordResetId: "Id", nextStep: .newPassword)
        serviceMock.result = .success(stepData)

        sut.validateCode()

        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.action, .nextStep(stepData))
    }

    func testValidateCode_WhenResultIsFailure_ShouldCallPresentNextStepWithErrorAction() throws {
        sut.validateCode()

        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        let action = try XCTUnwrap(presenterSpy.action)
        guard case LinkValidationAction.error(code: _, title: _, message: _) = action else {
            XCTFail()
            return
        }
    }

    func testCloseAction_ShouldCallPresenterNextStepWithCloseAction() {
        sut.closeAction()

        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.action, .close)
    }
}
