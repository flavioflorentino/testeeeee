@testable import PasswordReset
import XCTest

private class NewPasswordCoordinatorDelegateSpy: NewPasswordCoordinatorDelegate {
    private(set) var newPasswordNextStepCallCount = 0
    private(set) var backFromNewPasswordCallCount = 0

    func newPasswordNextStep(_ stepData: PasswordResetStepData) {
        newPasswordNextStepCallCount += 1
    }

    func backFromNewPassword(shouldShowWarning: Bool) {
        backFromNewPasswordCallCount += 1
    }
}

final class NewPasswordCoordinatorTests: XCTestCase {
    private let delegateSpy = NewPasswordCoordinatorDelegateSpy()

    private lazy var sut: NewPasswordCoordinator = {
        let coordinator = NewPasswordCoordinator(shouldShowWarning: false)
        coordinator.delegate = delegateSpy
        return coordinator
    }()

    func testPerform_WhenActionIsNextStep_ShouldCallDelegateNewPasswordNextStep() {
        sut.perform(action: .nextStep(PasswordResetStepData(passwordResetId: "", nextStep: .done)))

        XCTAssertEqual(delegateSpy.newPasswordNextStepCallCount, 1)
    }

    func testPerform_WhenActionIsBack_CallDelegateBackFromNewPassword() {
        sut.perform(action: .back)

        XCTAssertEqual(delegateSpy.backFromNewPasswordCallCount, 1)
    }
}
