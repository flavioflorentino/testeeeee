import AnalyticsModule
import Core
@testable import PasswordReset
import XCTest

private class NewPasswordServicingMock: NewPasswordServicing {
    var result: Result<PasswordResetStepData, ApiError> = .failure(.serverError)

    func resetPassword(
        resetId: String,
        password: String,
        passwordConfirmation: String,
        completion: @escaping (Result<PasswordResetStepData, ApiError>) -> Void
    ) {
        completion(result)
    }
}

private class NewPasswordPresentingSpy: NewPasswordPresenting {
    var viewController: NewPasswordDisplay?
    private(set) var callEnableCreateButtonCount = 0
    private(set) var callTranslateViewYCount = 0
    private(set) var callShowFirstPasswordErrorCount = 0
    private(set) var callShowSecondPasswordErrorCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callGoToNextStepCount = 0
    private(set) var callShowPopupControllerCount = 0
    private(set) var callGoBackCount = 0

    private(set) var enableCreateButton = false
    private(set) var errorMessage: String?
    private(set) var isFirstPasswordError = false
    private(set) var isSecondPasswordError = false

    func enableCreateButton(_ enable: Bool) {
        callEnableCreateButtonCount += 1
        enableCreateButton = enable
    }

    func translateViewY(by offset: CGFloat, with properties: KeyboardProperties) {
        callTranslateViewYCount += 1
    }

    func showFirstPasswordError(_ message: String?, isError: Bool) {
        callShowFirstPasswordErrorCount += 1
        errorMessage = message
        isFirstPasswordError = isError
    }

    func showSecondPasswordError(_ message: String?, isError: Bool) {
        callShowSecondPasswordErrorCount += 1
        errorMessage = message
        isSecondPasswordError = isError
    }

    func startLoading() {
        callStartLoadingCount += 1
    }

    func stopLoading() {
        callStopLoadingCount += 1
    }
    
    func goToNextStep(_ step: PasswordResetStepData) {
        callGoToNextStepCount += 1
    }

    func showPopupController(with requestError: RequestError?) {
        callShowPopupControllerCount += 1
    }

    func goBack() {
        callGoBackCount += 1
    }
}

final class NewPasswordInteractorTests: XCTestCase {
    private let serviceMock = NewPasswordServicingMock()
    private let presenterSpy = NewPasswordPresentingSpy()
    private let analytics = AnalyticsSpy()
    private let authManagerMock = AuthenticationManagerMock()
    private lazy var dependencies = DependencyContainerMock(analytics, authManagerMock)

    private lazy var sut = NewPasswordInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        channelType: .sms,
        resetId: "👁 D",
        dependencies: dependencies
    )

    func testSetupView_ShouldDisableCreateButton() {
        sut.setupView()

        XCTAssertEqual(presenterSpy.callEnableCreateButtonCount, 1)
        XCTAssertFalse(presenterSpy.enableCreateButton)
    }

    func testSetupView_ShouldTrackEvent() {
        sut.setupView()

        let expectedEvent = PasswordResetTracker(method: .sms, eventType: NewPasswordEvent.appear).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testKeyboardWillShow_WhenKeyboardStateIsShownig_ShouldDoNothing() {
        let properties = KeyboardProperties(height: 0, animationCurve: 0, animationDuration: 0)
        sut.keyboardWillShow(currentSpacing: 0, minSpacing: 0, properties: properties)

        sut.keyboardWillShow(currentSpacing: 0, minSpacing: 0, properties: properties)

        XCTAssertEqual(presenterSpy.callTranslateViewYCount, 0)
    }

    func testKeyboardWillShow_WhenKeyboardStateIsHiding_WhenCurrentSpacingIsEqualToMinSpacing_ShouldDoNothing() {
        let properties = KeyboardProperties(height: 0, animationCurve: 0, animationDuration: 0)
        sut.keyboardWillShow(currentSpacing: 0, minSpacing: 0, properties: properties)

        XCTAssertEqual(presenterSpy.callTranslateViewYCount, 0)
    }

    func testKeyboardWillShow_WhenKeyboardStateIsHiding_WhenCurrentSpacingIsGreaterThanMinSpacing_ShouldDoNothing() {
        let properties = KeyboardProperties(height: 0, animationCurve: 0, animationDuration: 0)
        sut.keyboardWillShow(currentSpacing: 1, minSpacing: 0, properties: properties)

        XCTAssertEqual(presenterSpy.callTranslateViewYCount, 0)
    }

    func testKeyboardWillShow_WhenKeyboardStateIsHiding_WhenCurrentSpacingIsLessThanMinSpacing_ShouldCallTranslateView() {
        let properties = KeyboardProperties(height: 0, animationCurve: 0, animationDuration: 0)
        sut.keyboardWillShow(currentSpacing: 0, minSpacing: 1, properties: properties)

        XCTAssertEqual(presenterSpy.callTranslateViewYCount, 1)
    }

    func testKeyboardWillHide_WhenKeyboardStateIsHiding_ShouldDoNothing() {
        let properties = KeyboardProperties(height: 0, animationCurve: 0, animationDuration: 0)
        sut.keyboardWillHide(properties: properties)

        XCTAssertEqual(presenterSpy.callTranslateViewYCount, 0)
    }

    func testKeyboardWillHide_WhenKeyboardStateIsShowing_ShouldCallTranslateView() {
        let properties = KeyboardProperties(height: 0, animationCurve: 0, animationDuration: 0)
        sut.keyboardWillShow(currentSpacing: 0, minSpacing: 0, properties: properties)

        sut.keyboardWillHide(properties: properties)

        XCTAssertEqual(presenterSpy.callTranslateViewYCount, 1)
    }

    func testFirstPasswordEditingChange_WhenTextIsNil_ShouldDoNothing() {
        sut.firstPasswordEditingChange(nil)

        XCTAssertEqual(presenterSpy.callEnableCreateButtonCount, 0)
    }

    func testFirstPasswordEditingChange_WhenTextContainsLetters_ShouldPresentError() {
        sut.firstPasswordEditingChange("A1B2c3")

        XCTAssertEqual(presenterSpy.callEnableCreateButtonCount, 1)
        XCTAssertFalse(presenterSpy.enableCreateButton)
        XCTAssertEqual(presenterSpy.callShowFirstPasswordErrorCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, Strings.NewPassword.Error.numbersOnly)
        XCTAssertTrue(presenterSpy.isFirstPasswordError)
    }

    func testFirstPasswordEditingChange_WhenTextLengthIsLessThanFour_ShouldPresentError() {
        sut.firstPasswordEditingChange("123")

        XCTAssertEqual(presenterSpy.callEnableCreateButtonCount, 1)
        XCTAssertFalse(presenterSpy.enableCreateButton)
        XCTAssertEqual(presenterSpy.callShowFirstPasswordErrorCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, Strings.NewPassword.Error.length(4))
        XCTAssertTrue(presenterSpy.isFirstPasswordError)
    }

    func testFirstPasswordEditingChange_WhenTextLengthIsFour_ShouldHideError() {
        sut.firstPasswordEditingChange("1234")

        XCTAssertEqual(presenterSpy.callEnableCreateButtonCount, 1)
        XCTAssertFalse(presenterSpy.enableCreateButton)
        XCTAssertEqual(presenterSpy.callShowFirstPasswordErrorCount, 1)
        XCTAssertFalse(presenterSpy.isFirstPasswordError)
    }

    func testSecondPasswordEditingChange_WhenTextIsNil_ShouldDoNothing() {
        sut.secondPasswordEditingChange(nil)

        XCTAssertEqual(presenterSpy.callEnableCreateButtonCount, 0)
    }

    func testSecondPasswordEditingChange_WhenFirstPasswordIsDifferentFromSecondPassword_ShouldPresentError() {
        sut.firstPasswordEditingChange("12345")
        sut.secondPasswordEditingChange("1234")

        XCTAssertEqual(presenterSpy.callEnableCreateButtonCount, 2)
        XCTAssertFalse(presenterSpy.enableCreateButton)
        XCTAssertEqual(presenterSpy.callShowSecondPasswordErrorCount, 2)
        XCTAssertEqual(presenterSpy.errorMessage, Strings.NewPassword.Error.differentPasswords)
        XCTAssertTrue(presenterSpy.isSecondPasswordError)
    }

    func testSecondPasswordEditingChange_WhenFirstPasswordIsEqualToSecondPassword_ShouldHideError() {
        sut.firstPasswordEditingChange("12345")
        sut.secondPasswordEditingChange("12345")

        XCTAssertEqual(presenterSpy.callEnableCreateButtonCount, 2)
        XCTAssertTrue(presenterSpy.enableCreateButton)
        XCTAssertEqual(presenterSpy.callShowSecondPasswordErrorCount, 2)
        XCTAssertNil(presenterSpy.errorMessage)
        XCTAssertFalse(presenterSpy.isSecondPasswordError)
    }

    func testResetPassword_ShouldStartAndStopLoading() {
        sut.resetPassword()

        XCTAssertEqual(presenterSpy.callStartLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callStopLoadingCount, 1)
    }

    func testResetPassword_WhenResultIsSuccess_CallGoToNextStep() {
        serviceMock.result = .success(PasswordResetStepData(passwordResetId: "", nextStep: .done))

        sut.resetPassword()

        XCTAssertEqual(presenterSpy.callGoToNextStepCount, 1)
    }

    func testResetPassword_WhenResultIsSuccess_ShouldDisableBiometricAuthentication() {
        serviceMock.result = .success(PasswordResetStepData(passwordResetId: "", nextStep: .done))

        sut.resetPassword()

        XCTAssertEqual(authManagerMock.disableBiometricAuthenticationCallCount, 1)
    }

    func testResetPassword_WhenResultIsFailure_CallShowPopupController() {
        sut.resetPassword()

        XCTAssertEqual(presenterSpy.callShowPopupControllerCount, 1)
    }

    func testResetPassword_WhenResultIsFailure_ShouldTrackEvent() {
        sut.resetPassword()

        let expectedEvent = PasswordResetTracker(method: .sms, eventType: ErrorEvent.generic).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testResetPassword_WhenResultIsFailure_WhenErrorIsDifferentPasswords_ShouldTrackEvent() {
        var requestError = RequestError()
        requestError.code = "password_different"
        serviceMock.result = .failure(.unauthorized(body: requestError))
        sut.resetPassword()

        let expectedEvent = PasswordResetTracker(method: .sms, eventType: NewPasswordEvent.matchError).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testResetPassword_WhenResultIsFailure_WhenErrorIsBlocked_ShouldTrackEvent() {
        var requestError = RequestError()
        requestError.code = "password_reset_block"
        serviceMock.result = .failure(.unauthorized(body: requestError))
        sut.resetPassword()

        let expectedEvent = PasswordResetTracker(method: .sms, eventType: ErrorEvent.blocked).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }

    func testBackButtonAction_CallPresenterGoBack() {
        sut.backButtonAction()

        XCTAssertEqual(presenterSpy.callGoBackCount, 1)
    }
}
