@testable import PasswordReset
import UI
import XCTest

private class NewPasswordCoordinatingSpy: NewPasswordCoordinating {
    weak var delegate: NewPasswordCoordinatorDelegate?
    private(set) var nextStepActionCount = 0
    private(set) var backActionCount = 0

    func perform(action: NewPasswordAction) {
        switch action {
        case .nextStep:
            nextStepActionCount += 1
        case .back:
            backActionCount += 1
        }
    }
}

private class NewPasswordDisplaySpy: NewPasswordDisplay {
    var loadingView = LoadingView()
    private(set) var callEnableCreateButtonCount = 0
    private(set) var callTranslateViewYCount = 0
    private(set) var callFirstPasswordErrorCount = 0
    private(set) var callSecondPasswordErrorCount = 0
    private(set) var callStartLoadingViewCount = 0
    private(set) var callStopLoadingViewCount = 0
    private(set) var callShowPopupControllerCount = 0

    private(set) var popupProperties: PopupProperties?

    func enableCreateButton(_ enable: Bool) {
        callEnableCreateButtonCount += 1
    }

    func translateViewY(by offset: CGFloat, with properties: KeyboardProperties) {
        callTranslateViewYCount += 1
    }

    func firstPasswordError(_ message: String?, isError: Bool) {
        callFirstPasswordErrorCount += 1
    }

    func secondPasswordError(_ message: String?, isError: Bool) {
        callSecondPasswordErrorCount += 1
    }

    func showPopupController(with properties: PopupProperties) {
        callShowPopupControllerCount += 1
        popupProperties = properties
    }

    func startLoadingView() {
        callStartLoadingViewCount += 1
    }

    func stopLoadingView() {
        callStopLoadingViewCount += 1
    }
}

final class NewPasswordPresenterTests: XCTestCase {
    private let coordinatorSpy = NewPasswordCoordinatingSpy()
    private let controllerSpy = NewPasswordDisplaySpy()

    private lazy var sut: NewPasswordPresenter = {
        let presenter = NewPasswordPresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()

    func testEnableCreateButton_ShouldCallEnableCreateButton() {
        sut.enableCreateButton(true)

        XCTAssertEqual(controllerSpy.callEnableCreateButtonCount, 1)
    }

    func testTranslateViewY_ShouldCallTranslateViewY() {
        let properties = KeyboardProperties(height: 0, animationCurve: 0, animationDuration: 0)
        sut.translateViewY(by: 0, with: properties)

        XCTAssertEqual(controllerSpy.callTranslateViewYCount, 1)
    }

    func testShowFirstPasswordError_ShouldCallFirstPasswordError() {
        sut.showFirstPasswordError(nil, isError: false)

        XCTAssertEqual(controllerSpy.callFirstPasswordErrorCount, 1)
    }

    func testShowSecondPasswordError_ShouldCallSecondPasswordError() {
        sut.showSecondPasswordError(nil, isError: false)

        XCTAssertEqual(controllerSpy.callSecondPasswordErrorCount, 1)
    }

    func testStartLoading_ShouldCallstartLoading() {
        sut.startLoading()

        XCTAssertEqual(controllerSpy.callStartLoadingViewCount, 1)
    }

    func testStopLoading_ShouldCallstartLoading() {
        sut.stopLoading()

        XCTAssertEqual(controllerSpy.callStopLoadingViewCount, 1)
    }

    func testShowPopupController_ShouldCallShowPopupController() {
        sut.showPopupController(with: nil)

        XCTAssertEqual(controllerSpy.callShowPopupControllerCount, 1)
        XCTAssertEqual(controllerSpy.popupProperties?.title, Strings.Error.title)
        XCTAssertEqual(controllerSpy.popupProperties?.message, Strings.Error.message)
        XCTAssertEqual(controllerSpy.popupProperties?.mainButtonTitle, Strings.General.tryAgain)
        XCTAssertEqual(controllerSpy.popupProperties?.secondaryButtonTitle, Strings.General.back)
    }

    func testGoToNextStep_ShouldPerformNextStepAction() {
        sut.goToNextStep(PasswordResetStepData(passwordResetId: "", nextStep: .done))

        XCTAssertEqual(coordinatorSpy.nextStepActionCount, 1)
    }

    func testGoBack_CallCoordinatorPerformBackAction() {
        sut.goBack()

        XCTAssertEqual(coordinatorSpy.backActionCount, 1)
    }
}
