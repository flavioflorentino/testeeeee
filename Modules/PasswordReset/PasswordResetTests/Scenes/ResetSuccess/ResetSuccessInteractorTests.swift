import AnalyticsModule
@testable import PasswordReset
import XCTest

private final class ResetSuccessPresenterSpy: ResetSuccessPresenting {
    private(set) var didNextStepCallCount = 0
    private(set) var actionCalled: ResetSuccessAction?
    
    func didNextStep(action: ResetSuccessAction) {
        didNextStepCallCount += 1
        actionCalled = action
    }
}

final class ResetSuccessInteractorTests: XCTestCase {
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)
    
    private let presenterSpy = ResetSuccessPresenterSpy()
    
    private lazy var sut = ResetSuccessInteractor(method: .sms, presenter: presenterSpy, dependencies: dependencies)
    
    func testEnter_ShouldCallPresentNextStepWithEnter() {
        sut.enter()
        
        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.actionCalled, .enter)
    }
    
    func testClose_ShouldCallPresentNextStepWithClose() {
        sut.close()
        
        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.actionCalled, .close)
    }
    
    func testSendAnalytics_ShoulSendAnalyticsEvent() {
        sut.sendAnalytics()
        
        let expectedEvent = PasswordResetTracker(method: .sms, eventType: ResetSuccessEvent.appear).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
}
