@testable import PasswordReset
import XCTest

private final class ResetSuccessCoordinatorDelegateSpy: ResetSuccessCoordinatorDelegate {
    private(set) var didTapActionButtonCallCount = 0
    private(set) var didTapCloseButtonCallCount = 0
    
    func didTapActionButton() {
        didTapActionButtonCallCount += 1
    }
    
    func didTapCloseButton() {
        didTapCloseButtonCallCount += 1
    }
}

final class ResetSuccessCoordinatorTests: XCTestCase {
    private lazy var delegateSpy = ResetSuccessCoordinatorDelegateSpy()
    
    let smsChannel = Channel(channelType: .sms, title: "SMS", icon: "", data: "****-9999")
    let biometricChannel = Channel(channelType: .biometric, title: "Fallback", icon: "", data: "")
    
    private lazy var sut: ResetSuccessCoordinator = {
        let coordinator = ResetSuccessCoordinator()
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerform_WhenActionIsEnter_ShouldCallDelegateDidTapActionButton() {
        sut.perform(action: .enter)
        
        XCTAssertEqual(delegateSpy.didTapActionButtonCallCount, 1)
    }
    
    func testPerform_WhenActionIsClose_ShouldCallDelegateDidTapCloseButton() {
        sut.perform(action: .close)
        
        XCTAssertEqual(delegateSpy.didTapCloseButtonCallCount, 1)
    }
}
