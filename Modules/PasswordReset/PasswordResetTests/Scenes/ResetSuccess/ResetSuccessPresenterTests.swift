@testable import PasswordReset
import XCTest

private final class ResetSuccessCoordinatingSpy: ResetSuccessCoordinating {
    weak var delegate: ResetSuccessCoordinatorDelegate?
    var viewController: UIViewController?
    private(set) var performActionCallCount = 0
    private(set) var actionCalled: ResetSuccessAction?
    
    func perform(action: ResetSuccessAction) {
        performActionCallCount += 1
        actionCalled = action
    }
}

final class ResetSuccessPresenterTests: XCTestCase {
    private let coordinatorSpy = ResetSuccessCoordinatingSpy()
    private lazy var sut = ResetSuccessPresenter(coordinator: coordinatorSpy)
    
    func testDidNextStep_WhenActionIsEnter_ShouldPerformEnterActionOnCoordinator() {
        sut.didNextStep(action: .enter)
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.actionCalled, .enter)
    }

    func testDidNextStep_WhenActionIsClose_ShouldPerformCloseActionOnCoordinator() {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.actionCalled, .close)
    }
}
