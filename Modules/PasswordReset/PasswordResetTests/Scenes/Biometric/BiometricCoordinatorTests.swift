@testable import PasswordReset
import XCTest

private class BiometricCoordinatorDelegateSpy: BiometricCoordinatorDelegate {
    private(set) var callIdentityValidationCount = 0
    private(set) var callBackFromBiometricCount = 0

    func biometricController(_ controller: UIViewController?, showIdentity flowData: IdentityFlowData) {
        callIdentityValidationCount += 1
    }

    func back(from controller: UIViewController?) {
        callBackFromBiometricCount += 1
    }
}

final class BiometricCoordinatorTests: XCTestCase {
    private let delegateSpy = BiometricCoordinatorDelegateSpy()

    private lazy var sut: BiometricCoordinator = {
        let coordinator = BiometricCoordinator()
        coordinator.delegate = delegateSpy
        return coordinator
    }()

    func testPerform_WhenActionIsIdentityValidation_CallDelegateIdentityValidation() {
        sut.perform(action: .identityValidation(IdentityFlowData(token: "", flow: "")))

        XCTAssertEqual(delegateSpy.callIdentityValidationCount, 1)
    }

    func testPerform_WhenActionIsBack_CallDelegateBackFromBiometric() {
        sut.perform(action: .back)

        XCTAssertEqual(delegateSpy.callBackFromBiometricCount, 1)
    }
}
