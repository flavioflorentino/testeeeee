import Core
@testable import PasswordReset
import UI
import XCTest

private class BiometricCoordinatingSpy: BiometricCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: BiometricCoordinatorDelegate?
    private(set) var identityValidationActionCount = 0
    private(set) var backActionCount = 0

    func perform(action: BiometricAction) {
        switch action {
        case .identityValidation:
            identityValidationActionCount += 1
        case .back:
            backActionCount += 1
        }
    }
}

private class BiometricDisplaySpy: BiometricDisplay {
    var loadingView = LoadingView()
    private(set) var callStartLoadingViewCount = 0
    private(set) var callStopLoadingViewCount = 0
    private(set) var callShowPopupControllerCount = 0

    private(set) var popupProperties: PopupProperties?

    func startLoadingView() {
        callStartLoadingViewCount += 1
    }

    func stopLoadingView() {
        callStopLoadingViewCount += 1
    }

    func showPopupController(with properties: PopupProperties) {
        callShowPopupControllerCount += 1
        popupProperties = properties
    }
}

final class BiometricPresenterTests: XCTestCase {
    private let coordinatorSpy = BiometricCoordinatingSpy()
    private let controllerSpy = BiometricDisplaySpy()

    private lazy var sut: BiometricPresenter = {
        let presenter = BiometricPresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()

    func testStartLoading_CallViewControllerStartLoading() {
        sut.startLoading()

        XCTAssertEqual(controllerSpy.callStartLoadingViewCount, 1)
    }

    func testStopLoading_CallViewControllerStopLoading() {
        sut.stopLoading()

        XCTAssertEqual(controllerSpy.callStopLoadingViewCount, 1)
    }

    func testShowIdentityValidation_CallNavigateToIdentityValidation() {
        sut.showIdentityValidation(IdentityFlowData(token: "", flow: ""))

        XCTAssertEqual(coordinatorSpy.identityValidationActionCount, 1)
    }

    func testShowError_CallViewControllerShowPopupController() {
        sut.showError(nil)

        XCTAssertEqual(controllerSpy.callShowPopupControllerCount, 1)
    }

    func testShowError_WhenRequestErrorMessageIsNil_PassTheCorrectPopupPropertiesObjetc() {
        sut.showError(nil)

        XCTAssertEqual(controllerSpy.popupProperties?.title, Strings.Error.title)
        XCTAssertEqual(controllerSpy.popupProperties?.message, Strings.Error.message)
        XCTAssertEqual(controllerSpy.popupProperties?.mainButtonTitle, Strings.General.tryAgain)
        XCTAssertEqual(controllerSpy.popupProperties?.secondaryButtonTitle, Strings.General.back)
    }

    func testShowError_WhenRequestErrorMessageIsNotNil_PassTheCorrectPopupPropertiesObjetc() {
        var requestError = RequestError()
        requestError.message = "Error Message"
        sut.showError(requestError)

        XCTAssertEqual(controllerSpy.popupProperties?.title, requestError.title)
        XCTAssertEqual(controllerSpy.popupProperties?.message,requestError.message)
        XCTAssertEqual(controllerSpy.popupProperties?.mainButtonTitle, Strings.General.tryAgain)
        XCTAssertEqual(controllerSpy.popupProperties?.secondaryButtonTitle, Strings.General.back)
    }

    func testGoBack_CallCoordinatorBackAction() {
        sut.goBack()

        XCTAssertEqual(coordinatorSpy.backActionCount, 1)
    }
}
