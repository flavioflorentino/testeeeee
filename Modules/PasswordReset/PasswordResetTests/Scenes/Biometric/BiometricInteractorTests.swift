import AnalyticsModule
import Core
@testable import PasswordReset
import XCTest

private class BiometricServicingMock: BiometricServicing {
    var result: Result<IdentityFlowData, ApiError> = .failure(.serverError)

    func selfieFlow(resetId: String, completion: @escaping (Result<IdentityFlowData, ApiError>) -> Void) {
        completion(result)
    }
}

private class BiometricPresentingSpy: BiometricPresenting {
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callShowIdentityValidationCount = 0
    private(set) var callShowErrorCount = 0
    private(set) var callGoBackCount = 0

    func startLoading() {
        callStartLoadingCount += 1
    }

    func stopLoading() {
        callStopLoadingCount += 1
    }

    func showIdentityValidation(_ data: IdentityFlowData) {
        callShowIdentityValidationCount += 1
    }

    func showError(_ requestError: RequestError?) {
        callShowErrorCount += 1
    }

    func goBack() {
        callGoBackCount += 1
    }
}

final class BiometricInteractorTests: XCTestCase {
    private let serviceMock = BiometricServicingMock()
    private let presenterSpy = BiometricPresentingSpy()
    private let analyticsSpy = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(analyticsSpy)

    private lazy var sut = BiometricInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        resetId: "I_Dee",
        dependencies: dependenciesMock
    )

    func testGetIdentityFlow_ShouldStartAndStopLoading() {
        sut.getIdentityFlow()

        XCTAssertEqual(presenterSpy.callStartLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callStopLoadingCount, 1)
    }

    func testGetIdentityFlow_WhenResultIsSuccess_CallPresentIdentityValidation() {
        serviceMock.result = .success(IdentityFlowData(token: "", flow: ""))

        sut.getIdentityFlow()

        XCTAssertEqual(presenterSpy.callShowIdentityValidationCount, 1)
    }

    func testGetIdentityFlow_WhenResultIsSuccess_SendSuccessEvent() {
        serviceMock.result = .success(IdentityFlowData(token: "", flow: ""))

        sut.getIdentityFlow()

        let expectedEvent = PasswordResetTracker(eventType: BiometricEvent.success).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testGetIdentityFlow_WhenResultIsFailure_CallPresentError() {
        sut.getIdentityFlow()

        XCTAssertEqual(presenterSpy.callShowErrorCount, 1)
    }

    func testBackAction_CallPresenterGoBack() {
        sut.backAction()

        XCTAssertEqual(presenterSpy.callGoBackCount, 1)
    }
}
