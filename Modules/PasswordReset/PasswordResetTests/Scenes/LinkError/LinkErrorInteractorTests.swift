import AnalyticsModule
@testable import PasswordReset
import XCTest

private class LinkErrorPresentingSpy: LinkErrorPresenting {
    var viewController: LinkErrorDisplaying?
    private(set) var displayPropertiesCallCount = 0
    private(set) var didNextStepCallCount = 0
    private(set) var action: LinkErrorAction?

    func displayProperties(title: String?, message: String?) {
        displayPropertiesCallCount += 1
    }

    func didNextStep(action: LinkErrorAction) {
        didNextStepCallCount += 1
        self.action = action
    }
}

final class LinkErrorInteractorTests: XCTestCase {
    private let presenterSpy = LinkErrorPresentingSpy()
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)

    private lazy var sut = LinkErrorInteractor(presenter: presenterSpy,
                                               errorCode: .unkown,
                                               channelType: .email,
                                               title: nil,
                                               message: nil,
                                               dependencies: dependencies)

    func testLoadView_ShouldCallPresenterDisplayProperties() {
        sut.loadView()

        XCTAssertEqual(presenterSpy.displayPropertiesCallCount, 1)
    }

    func testLoadView_ShouldSendAppearEvent() {
        sut.loadView()

        let appearEvent = PasswordResetTracker(method: .email, eventType: LinkErrorEvent.appear, errorCode: .unkown).event()
        XCTAssertTrue(analytics.equals(to: appearEvent))
    }
    
    func testUnderstoodAction_ShouldCallPresenterNextStepWithDoneAction() {
        sut.didConfirm()

        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.action, .done)
    }
}
