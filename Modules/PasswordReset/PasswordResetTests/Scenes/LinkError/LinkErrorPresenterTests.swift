@testable import PasswordReset
import XCTest

private class LinkErrorCoordinatingSpy: LinkErrorCoordinating {
    weak var delegate: LinkErrorCoordinatorDelegate?
    private(set) var performCallCount = 0

    func perform(action: LinkErrorAction) {
        performCallCount += 1
    }
}

private class LinkErrorDisplayingSpy: LinkErrorDisplaying {
    private(set) var displayInfoCallCount = 0
    private(set) var title: String?
    private(set) var message: String?

    func displayInfo(image: UIImage, title: String, description: String, buttonTitle: String) {
        displayInfoCallCount += 1
        self.title = title
        self.message = description
    }
}

final class LinkErrorPresenterTests: XCTestCase {
    private let coordinatorSpy = LinkErrorCoordinatingSpy()
    private let controllerSpy = LinkErrorDisplayingSpy()

    private lazy var sut: LinkErrorPresenter = {
        let presenter = LinkErrorPresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()

    func testDisplayProperties_WhenMessageAndTitleIsNil_ShouldCallControllerDisplayInfo() {
        sut.displayProperties(title: nil, message: nil)

        XCTAssertEqual(controllerSpy.displayInfoCallCount, 1)
        XCTAssertEqual(controllerSpy.title, Strings.LinkError.title)
        XCTAssertEqual(controllerSpy.message, Strings.LinkError.message)
    }

    func testDisplayProperties_ShouldCallControllerDisplayInfo() {
        sut.displayProperties(title: "Title", message: "Message")

        XCTAssertEqual(controllerSpy.displayInfoCallCount, 1)
        XCTAssertEqual(controllerSpy.title, "Title")
        XCTAssertEqual(controllerSpy.message, "Message")
    }

    func testDidNextStep_ShouldCallCoordinatorPerform() {
        sut.didNextStep(action: .done)

        XCTAssertEqual(coordinatorSpy.performCallCount, 1)
    }
}
