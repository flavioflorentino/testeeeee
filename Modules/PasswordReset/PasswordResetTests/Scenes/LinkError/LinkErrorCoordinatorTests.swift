@testable import PasswordReset
import XCTest

private class LinkErrorCoordinatorDelegateSpy: LinkErrorCoordinatorDelegate {
    private(set) var closeLinkErrorCallCount = 0

    func closeLinkError() {
        closeLinkErrorCallCount += 1
    }
}

final class LinkErrorCoordinatorTests: XCTestCase {
    private let delegateSpy = LinkErrorCoordinatorDelegateSpy()

    private lazy var sut: LinkErrorCoordinator = {
        let coordinator = LinkErrorCoordinator()
        coordinator.delegate = delegateSpy
        return coordinator
    }()

    func testPerform_WhenActionIsDone_ShouldCallDelegateClose() {
        sut.perform(action: .done)

        XCTAssertEqual(delegateSpy.closeLinkErrorCallCount, 1)
    }
}
