@testable import PasswordReset
import XCTest

private class LinkConfirmationCoordinatorDelegateSpy: LinkConfirmationCoordinatorDelegate {
    private(set) var backFromLinkConfirmationCallCount = 0
    private(set) var linkConfirmationDoneCallCount = 0

    func backFromLinkConfirmation() {
        backFromLinkConfirmationCallCount += 1
    }

    func linkConfirmationDone() {
        linkConfirmationDoneCallCount += 1
    }
}

final class LinkConfirmationCoordinatorTests: XCTestCase {
    private let delegateSpy = LinkConfirmationCoordinatorDelegateSpy()

    private lazy var sut: LinkConfirmationCoordinator = {
        let coordinator = LinkConfirmationCoordinator()
        coordinator.delegate = delegateSpy
        return coordinator
    }()

    func testPerform_WhenActionIsBack_ShouldCallDelegateBackFromLinkConfirmation() {
        sut.perform(action: .back)

        XCTAssertEqual(delegateSpy.backFromLinkConfirmationCallCount, 1)
    }

    func testPerform_WhenActionIsDone_ShouldCallDelegateLinkConfirmationDone() {
        sut.perform(action: .done)

        XCTAssertEqual(delegateSpy.linkConfirmationDoneCallCount, 1)
    }
}
