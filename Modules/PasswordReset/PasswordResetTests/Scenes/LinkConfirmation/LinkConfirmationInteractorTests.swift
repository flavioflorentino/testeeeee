import AnalyticsModule
import Core
@testable import PasswordReset
import XCTest

private class LinkConfirmationServicingMock: LinkConfirmationServicing {
    private(set) var requestLinkCallCount = 0
    var requestCompletion: Result<NoContent, ApiError> = .success(NoContent())

    func requestLink(resetId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        requestLinkCallCount += 1
        completion(requestCompletion)
    }
}

private class LinkConfirmationPresentingSpy: LinkConfirmationPresenting {
    var viewController: LinkConfirmationDisplaying?
    private(set) var configureViewCallCount = 0
    private(set) var startLoadingCallCount = 0
    private(set) var stopLoadingCallCount = 0
    private(set) var presentErrorWithCallCount = 0
    private(set) var goBackCallCount = 0
    private(set) var endResetFlowCallCount = 0
    private(set) var retryButton = false

    func configureView(with email: String) {
        configureViewCallCount += 1
    }

    func startLoading() {
        startLoadingCallCount += 1
    }

    func stopLoading() {
        stopLoadingCallCount += 1
    }

    func presentErrorWith(retryButton: Bool, title: String?, message: String?) {
        presentErrorWithCallCount += 1
        self.retryButton = retryButton
    }

    func goBack() {
        goBackCallCount += 1
    }

    func endResetFlow() {
        endResetFlowCallCount += 1
    }
}

final class LinkConfirmationInteractorTests: XCTestCase {
    private let serviceMock = LinkConfirmationServicingMock()
    private let presenterSpy = LinkConfirmationPresentingSpy()
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)

    private lazy var sut = LinkConfirmationInteractor(service: serviceMock, presenter: presenterSpy, channelType: .email, resetId: "", emailString: "", dependencies: dependencies)

    func testLoadView_ShouldCallPresenterConfigureView() {
        sut.loadView()

        XCTAssertEqual(presenterSpy.configureViewCallCount, 1)
    }

    func testLoadView_ShouldSendAppearEvent() {
        sut.loadView()

        let appearEvent = PasswordResetTracker(method: .email, eventType: LinkConfirmationEvent.appear).event()
        XCTAssertTrue(analytics.equals(to: appearEvent))
    }
    
    func testRequestLink_ShouldStartAndStopLoading() {
        sut.requestLink()

        XCTAssertEqual(presenterSpy.startLoadingCallCount, 1)
        XCTAssertEqual(presenterSpy.stopLoadingCallCount, 1)
    }

    func testRequestLink_WhenResultIsFailureAndErrorIsRequestBlock_ShouldCallPresentErrorWithRetryButtonFalse() {
        var requestError = RequestError()
        requestError.code = "password_send_link_many_request"
        serviceMock.requestCompletion = .failure(ApiError.unauthorized(body: requestError))

        sut.requestLink()

        XCTAssertEqual(presenterSpy.presentErrorWithCallCount, 1)
        XCTAssertFalse(presenterSpy.retryButton)
    }

    func testRequestLink_WhenResultIsFailureAndErrorIsRequestBlock_ShouldSendBlockErrorEvent() {
        var requestError = RequestError()
        requestError.code = "password_send_link_many_request"
        serviceMock.requestCompletion = .failure(ApiError.unauthorized(body: requestError))

        sut.requestLink()

        let blockEvent = PasswordResetTracker(method: .email, eventType: LinkConfirmationEvent.blockedError).event()
        XCTAssertTrue(analytics.equals(to: blockEvent))
    }

    func testRequestLink_WhenResultIsFailure_ShouldCallPresentErrorWithRetryButtonTrue() {
        serviceMock.requestCompletion = .failure(ApiError.bodyNotFound)

        sut.requestLink()

        XCTAssertEqual(presenterSpy.presentErrorWithCallCount, 1)
        XCTAssertTrue(presenterSpy.retryButton)
    }

    func testRequestLink_WhenResultIsFailure_ShouldSendGenericErrorEvent() {
        serviceMock.requestCompletion = .failure(ApiError.bodyNotFound)

        sut.requestLink()

        let genericEvent = PasswordResetTracker(method: .email, eventType: LinkConfirmationEvent.genericError).event()
        XCTAssertTrue(analytics.equals(to: genericEvent))
    }

    func testGoBackAction_ShouldCallPresenterGoBack() {
        sut.goBackAction()

        XCTAssertEqual(presenterSpy.goBackCallCount, 1)
    }

    func testDoneAction_ShouldCallPresenterEndResetFlow() {
        sut.doneAction()

        XCTAssertEqual(presenterSpy.endResetFlowCallCount, 1)
    }
}
