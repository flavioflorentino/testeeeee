@testable import PasswordReset
import UI
import XCTest

private class LinkConfirmationCoordinatingSpy: LinkConfirmationCoordinating {
    weak var delegate: LinkConfirmationCoordinatorDelegate?
    private(set) var backActionCallCount = 0
    private(set) var doneActionCallCount = 0

    func perform(action: LinkConfirmationAction) {
        switch action {
        case .back:
            backActionCallCount += 1
        case .done:
            doneActionCallCount += 1
        }
    }
}

private class LinkConfirmationDisplayingSpy: LinkConfirmationDisplaying {
    var loadingView = LoadingView()
    private(set) var displayInfoCallCount = 0
    private(set) var startLoadingViewCallCount = 0
    private(set) var stopLoadingViewCallCount = 0
    private(set) var showErrorPopupControllerCallCount = 0
    private(set) var showRetryPopupControllerCallCount = 0

    func displayInfo(image: UIImage, title: String, description: String, buttonTitle: String) {
        displayInfoCallCount += 1
    }

    func startLoadingView() {
        startLoadingViewCallCount += 1
    }

    func stopLoadingView() {
        stopLoadingViewCallCount += 1
    }

    func showErrorPopupController(with properties: PopupProperties) {
        showErrorPopupControllerCallCount += 1
    }

    func showRetryPopupController(with properties: PopupProperties) {
        showRetryPopupControllerCallCount += 1
    }
}

final class LinkConfirmationPresenterTests: XCTestCase {
    private let controllerSpy = LinkConfirmationDisplayingSpy()
    private let coordinatorSpy = LinkConfirmationCoordinatingSpy()

    private lazy var sut: LinkConfirmationPresenter = {
        let presenter = LinkConfirmationPresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()

    func testConfigureView_ShouldCallControllerDisplayInfo() {
        sut.configureView(with: "email")

        XCTAssertEqual(controllerSpy.displayInfoCallCount, 1)
    }

    func testStartLoading_ShouldCallControllerStartLoading() {
        sut.startLoading()

        XCTAssertEqual(controllerSpy.startLoadingViewCallCount, 1)
    }

    func testStopLoading_ShouldCallControllerStopLoading() {
        sut.stopLoading()

        XCTAssertEqual(controllerSpy.stopLoadingViewCallCount, 1)
    }

    func testPresentErrorWith_WhenRetryButtonIsTrue_ShouldCallShowRetryPopupController() {
        sut.presentErrorWith(retryButton: true, title: nil, message: nil)

        XCTAssertEqual(controllerSpy.showRetryPopupControllerCallCount, 1)
    }

    func testPresentErrorWith_WhenRetryButtonIsFalse_ShouldCallShowErrorPopupController() {
        sut.presentErrorWith(retryButton: false, title: "", message: "")

        XCTAssertEqual(controllerSpy.showErrorPopupControllerCallCount, 1)
    }

    func testGoBack_ShouldPerformCoordinatorBackAction() {
        sut.goBack()

        XCTAssertEqual(coordinatorSpy.backActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.doneActionCallCount, 0)
    }

    func testEndResetFlow_ShouldPerformCoordinatorDoneAction() {
        sut.endResetFlow()

        XCTAssertEqual(coordinatorSpy.doneActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.backActionCallCount, 0)
    }
}
