@testable import PasswordReset
import XCTest

private class UserFormCoordinatorDelegateSpy: UserFormCoordinatorDelegate {
    private(set) var userFormNextStepCallCount = 0
    private(set) var backFromUserFormCallCount = 0

    func userFormNextStep(_ stepData: PasswordResetStepData) {
        userFormNextStepCallCount += 1
    }

    func backFromUserForm() {
        backFromUserFormCallCount += 1
    }
}

final class UserFormCoordinatorTests: XCTestCase {
    private let coordinatorDelegateSpy = UserFormCoordinatorDelegateSpy()

    private lazy var sut: UserFormCoordinator = {
        let coordinator = UserFormCoordinator()
        coordinator.delegate = coordinatorDelegateSpy
        return coordinator
    }()

    func testPerform_WhenActionIsNextStep_CallDelegateNextStep() {
        sut.perform(action: .nextStep(PasswordResetStepData(passwordResetId: "", nextStep: .done)))

        XCTAssertEqual(coordinatorDelegateSpy.userFormNextStepCallCount, 1)
    }

    func testPerform_WhenActionIsBack_CallDelegateBackFromStepLoader() {
        sut.perform(action: .back)

        XCTAssertEqual(coordinatorDelegateSpy.backFromUserFormCallCount, 1)
    }
}
