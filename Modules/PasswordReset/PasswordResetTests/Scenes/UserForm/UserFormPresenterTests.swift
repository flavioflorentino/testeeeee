import Core
@testable import PasswordReset
import UI
import XCTest

private class UserFormCoordinatingSpy: UserFormCoordinating {
    weak var delegate: UserFormCoordinatorDelegate?
    private(set) var nextStepActionCount = 0
    private(set) var backActionCount = 0

    func perform(action: UserFormAction) {
        switch action {
        case .nextStep:
            nextStepActionCount += 1
        case .back:
            backActionCount += 1
        }
    }
}

private class UserFormDisplayingSpy: UserFormDisplaying {
    var loadingView = LoadingView()
    private(set) var startLoadingViewCallCount = 0
    private(set) var stopLoadingViewCallCount = 0
    private(set) var showPopupControllerCallCount = 0
    private(set) var nameFieldErrorCallCount = 0
    private(set) var dateFieldErrorCallCount = 0
    private(set) var phoneFieldErrorCallCount = 0
    private(set) var modelFieldErrorCallCount = 0

    private(set) var popupProperties: PopupProperties?
    private(set) var showNameFieldError = false
    private(set) var showDateFieldError = false
    private(set) var showPhoneFieldError = false
    private(set) var showModelFieldError = false

    func startLoadingView() {
        startLoadingViewCallCount += 1
    }

    func stopLoadingView() {
        stopLoadingViewCallCount += 1
    }

    func showPopupController(with properties: PopupProperties) {
        showPopupControllerCallCount += 1
        popupProperties = properties
    }

    func shouldShowNameFieldError(_ shouldShow: Bool, with message: String?) {
        nameFieldErrorCallCount += 1
        showNameFieldError = shouldShow
    }

    func shouldShowDateFieldError(_ shouldShow: Bool, with message: String?) {
        dateFieldErrorCallCount += 1
        showDateFieldError = shouldShow
    }

    func shouldShowPhoneFieldError(_ shouldShow: Bool, with message: String?) {
        phoneFieldErrorCallCount += 1
        showPhoneFieldError = shouldShow
    }

    func shouldShowModelFieldError(_ shouldShow: Bool, with message: String?) {
        modelFieldErrorCallCount += 1
        showModelFieldError = shouldShow
    }
}

final class UserFormPresenterTests: XCTestCase {
    private let coordinatorSpy = UserFormCoordinatingSpy()
    private let controllerSpy = UserFormDisplayingSpy()

    private lazy var sut: UserFormPresenter = {
        let presenter = UserFormPresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()

    func testStartLoading_CallControllerStartLoadingView() {
        sut.startLoading()

        XCTAssertEqual(controllerSpy.startLoadingViewCallCount, 1)
    }

    func testStopLoading_CallControllerStopLoadingView() {
        sut.stopLoading()

        XCTAssertEqual(controllerSpy.stopLoadingViewCallCount, 1)
    }

    func testPresentInformationPopup_CallControllerShowPopup() {
        sut.presentInformationPopup()

        XCTAssertEqual(controllerSpy.showPopupControllerCallCount, 1)
    }

    func testPresentEmptyBirthDateError_CallControllerDateFieldError() {
        sut.presentEmptyBirthDateError()

        XCTAssertEqual(controllerSpy.dateFieldErrorCallCount, 1)
        XCTAssertTrue(controllerSpy.showDateFieldError)
    }

    func testPresentInvalidBirthDateError_CallControllerDateFieldError() {
        sut.presentInvalidBirthDateError()

        XCTAssertEqual(controllerSpy.dateFieldErrorCallCount, 1)
        XCTAssertTrue(controllerSpy.showDateFieldError)
    }

    func testHideBirthDateError_CallControllerDateFieldError() {
        sut.hideBirthDateError()

        XCTAssertEqual(controllerSpy.dateFieldErrorCallCount, 1)
        XCTAssertFalse(controllerSpy.showDateFieldError)
    }

    func testPresentEmptyPhoneNumberError_CallControllerPhoneFieldError() {
        sut.presentEmptyPhoneNumberError()

        XCTAssertEqual(controllerSpy.phoneFieldErrorCallCount, 1)
        XCTAssertTrue(controllerSpy.showPhoneFieldError)
    }

    func testPresentInvalidPhoneNumberError_CallControllerPhoneFieldError() {
        sut.presentInvalidPhoneNumberError()

        XCTAssertEqual(controllerSpy.phoneFieldErrorCallCount, 1)
        XCTAssertTrue(controllerSpy.showPhoneFieldError)
    }

    func testHidePhoneNumberError_CallControllerPhoneFieldError() {
        sut.hidePhoneNumberError()

        XCTAssertEqual(controllerSpy.phoneFieldErrorCallCount, 1)
        XCTAssertFalse(controllerSpy.showPhoneFieldError)
    }

    func testShouldPresentNameError_WhenErrorIsTrue_CallControllerNameFieldError() {
        sut.shouldPresentNameError(true)

        XCTAssertEqual(controllerSpy.nameFieldErrorCallCount, 1)
        XCTAssertTrue(controllerSpy.showNameFieldError)
    }

    func testShouldPresentNameError_WhenErrorIsFalse_CallControllerNameFieldError() {
        sut.shouldPresentNameError(false)

        XCTAssertEqual(controllerSpy.nameFieldErrorCallCount, 1)
        XCTAssertFalse(controllerSpy.showNameFieldError)
    }

    func testShouldPresentPhoneModelError_WhenErrorIsTrue_CallControllerModelFieldError() {
        sut.shouldPresentPhoneModelError(true)

        XCTAssertEqual(controllerSpy.modelFieldErrorCallCount, 1)
        XCTAssertTrue(controllerSpy.showModelFieldError)
    }

    func testShouldPresentPhoneModelError_WhenErrorIsFalse_CallControllerModelFieldError() {
        sut.shouldPresentPhoneModelError(false)

        XCTAssertEqual(controllerSpy.modelFieldErrorCallCount, 1)
        XCTAssertFalse(controllerSpy.showNameFieldError)
    }

    func testShowFieldsPopupError_CallControllerShowPopup() {
        sut.showFieldsPopupError()

        XCTAssertEqual(controllerSpy.showPopupControllerCallCount, 1)
    }

    func testGoToNextStep_CallCoordinatorPerformNextStep() {
        sut.goToNextStep(PasswordResetStepData(passwordResetId: "", nextStep: .manualReview))

        XCTAssertEqual(coordinatorSpy.nextStepActionCount, 1)
    }

    func testGoBack_CallCoordinatorPerformBack() {
        sut.goBack()

        XCTAssertEqual(coordinatorSpy.backActionCount, 1)
    }

    func testShowErrorPopup_CallControllerShowPopup() {
        var requestError = RequestError()
        requestError.title = "Test Title"
        requestError.message = "Test Message"
        sut.showErrorPopup(with: requestError)

        XCTAssertEqual(controllerSpy.showPopupControllerCallCount, 1)
        XCTAssertEqual(controllerSpy.popupProperties?.title, "Test Title")
        XCTAssertEqual(controllerSpy.popupProperties?.message, "Test Message")
    }

    func testShowErrorPopup_WhenRequestErrorIsNil_CallControllerShowPopup() {
        sut.showErrorPopup(with: nil)

        XCTAssertEqual(controllerSpy.showPopupControllerCallCount, 1)
        XCTAssertEqual(controllerSpy.popupProperties?.title, Strings.Error.title)
        XCTAssertEqual(controllerSpy.popupProperties?.message, Strings.Error.message)
    }
}
