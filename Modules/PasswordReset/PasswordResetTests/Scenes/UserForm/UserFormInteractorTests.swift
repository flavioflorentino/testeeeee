import AnalyticsModule
import Core
@testable import PasswordReset
import XCTest

private class UserFormServicingMock: UserFormServicing {
    var result: Result<PasswordResetStepData, ApiError> = .failure(.serverError)

    func sendFormData(personalData: PersonalData, completion: @escaping (Result<PasswordResetStepData, ApiError>) -> Void) {
        completion(result)
    }
}

private class UserFormPresentingSpy: UserFormPresenting {
    var viewController: UserFormDisplaying?
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callPresentInformationPopupCount = 0
    private(set) var callPresentEmptyBirthDateErrorCount = 0
    private(set) var callPresentInvalidBirthDateErrorCount = 0
    private(set) var callHideBirthDateErrorCount = 0
    private(set) var callPresentEmptyPhoneNumberErrorCount = 0
    private(set) var callPresentInvalidPhoneNumberErrorCount = 0
    private(set) var callHidePhoneNumberErrorCount = 0
    private(set) var callShouldPresentNameErrorCount = 0
    private(set) var callShouldPresentPhoneModelErrorCount = 0
    private(set) var callShowFieldsPopupErrorCount = 0
    private(set) var callGoToNextStepCount = 0
    private(set) var callGoBackCount = 0
    private(set) var callShowErrorPopupCount = 0

    private(set) var presentNameError = false
    private(set) var presentModelError = false

    func startLoading() {
        callStartLoadingCount += 1
    }

    func stopLoading() {
        callStopLoadingCount += 1
    }

    func presentInformationPopup() {
        callPresentInformationPopupCount += 1
    }

    func presentEmptyBirthDateError() {
        callPresentEmptyBirthDateErrorCount += 1
    }

    func presentInvalidBirthDateError() {
        callPresentInvalidBirthDateErrorCount += 1
    }

    func hideBirthDateError() {
        callHideBirthDateErrorCount += 1
    }

    func presentEmptyPhoneNumberError() {
        callPresentEmptyPhoneNumberErrorCount += 1
    }

    func presentInvalidPhoneNumberError() {
        callPresentInvalidPhoneNumberErrorCount += 1
    }

    func hidePhoneNumberError() {
        callHidePhoneNumberErrorCount += 1
    }

    func shouldPresentNameError(_ shouldPresent: Bool) {
        callShouldPresentNameErrorCount += 1
        presentNameError = shouldPresent
    }

    func shouldPresentPhoneModelError(_ shouldPresent: Bool) {
        callShouldPresentPhoneModelErrorCount += 1
        presentModelError = shouldPresent
    }

    func showFieldsPopupError() {
        callShowFieldsPopupErrorCount += 1
    }

    func goToNextStep(_ step: PasswordResetStepData) {
        callGoToNextStepCount += 1
    }

    func goBack() {
        callGoBackCount += 1
    }

    func showErrorPopup(with requestError: RequestError?) {
        callShowErrorPopupCount += 1
    }
}

final class UserFormInteractorTests: XCTestCase {
    private let serviceMock = UserFormServicingMock()
    private let presenterSpy = UserFormPresentingSpy()
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)

    private lazy var sut = UserFormInteractor(
        service: serviceMock,
        presenter: presenterSpy,
        dependencies: dependencies,
        resetId: "rEsEtId"
    )

    func testBackButtonAction_CallPresenterGoBack() {
        sut.backButtonAction()

        XCTAssertEqual(presenterSpy.callGoBackCount, 1)
    }

    func testInformationButtonAction_CallPresentInformation() {
        sut.informationButtonAction()

        XCTAssertEqual(presenterSpy.callPresentInformationPopupCount, 1)
    }

    func testNameFieldBecomesFirstResponder_CallShouldPresentNameError() {
        sut.nameFieldBecomesFirstResponder()

        XCTAssertEqual(presenterSpy.callShouldPresentNameErrorCount, 1)
        XCTAssertFalse(presenterSpy.presentNameError)
    }

    func testBirthDateFieldBecomesFirstResponder_CallHideBirthDateError() {
        sut.birthDateFieldBecomesFirstResponder()

        XCTAssertEqual(presenterSpy.callHideBirthDateErrorCount, 1)
    }

    func testPhoneNumberFieldBecomesFirstResponder_CallHidePhoneNumberError() {
        sut.phoneNumberFieldBecomesFirstResponder()

        XCTAssertEqual(presenterSpy.callHidePhoneNumberErrorCount, 1)
    }

    func testPhoneModelFieldBecomesFirstResponder_CallShouldPresentPhoneModelErrorCount() {
        sut.phoneModelFieldBecomesFirstResponder()

        XCTAssertEqual(presenterSpy.callShouldPresentPhoneModelErrorCount, 1)
        XCTAssertFalse(presenterSpy.presentModelError)
    }

    func testValidateName_WhenTextIsNil_CallShouldPresentNameError() {
        sut.validateName(nil)

        XCTAssertEqual(presenterSpy.callShouldPresentNameErrorCount, 1)
        XCTAssertTrue(presenterSpy.presentNameError)
    }

    func testValidateName_WhenTextIsEmpty_CallShouldPresentNameError() {
        sut.validateName("")

        XCTAssertEqual(presenterSpy.callShouldPresentNameErrorCount, 1)
        XCTAssertTrue(presenterSpy.presentNameError)
    }

    func testValidateName_WhenTextIsNotNilOrEmpty_CallShouldPresentNameError() {
        sut.validateName("Name")

        XCTAssertEqual(presenterSpy.callShouldPresentNameErrorCount, 1)
        XCTAssertFalse(presenterSpy.presentNameError)
    }

    func testValidateDateOfBirth_WhenTextIsNil_CallPresentEmptyBirthDateError() {
        sut.validateDateOfBirth(nil)

        XCTAssertEqual(presenterSpy.callPresentEmptyBirthDateErrorCount, 1)
    }

    func testValidateDateOfBirth_WhenTextIsEmpty_CallPresentEmptyBirthDateError() {
        sut.validateDateOfBirth("")

        XCTAssertEqual(presenterSpy.callPresentEmptyBirthDateErrorCount, 1)
    }

    func testValidateDateOfBirth_WhenTextIsNotAValidDate_CallPresentInvalidBirthDateError() {
        sut.validateDateOfBirth("01/13/2020")

        XCTAssertEqual(presenterSpy.callPresentInvalidBirthDateErrorCount, 1)
    }

    func testValidateDateOfBirth_WhenDateIsBeforeCurrentDate_CallHideBirthDateError() {
        sut.validateDateOfBirth("01/10/2000")

        XCTAssertEqual(presenterSpy.callHideBirthDateErrorCount, 1)
    }

    func testValidateDateOfBirth_WhenDateIsAfterCurrentDate_CallPresentInvalidBirthDateError() {
        sut.validateDateOfBirth(generateTomorrowDateString())

        XCTAssertEqual(presenterSpy.callPresentInvalidBirthDateErrorCount, 1)
    }

    func testValidateDateOfBirth_WhenTextCountIsNotDateLength_CallPresentInvalidBirthDateError() {
        sut.validateDateOfBirth("01/13/20")

        XCTAssertEqual(presenterSpy.callPresentInvalidBirthDateErrorCount, 1)
    }

    func testValidatePhoneNumber_WhenTextIsNil_CallPresentEmptyPhoneNumberError() {
        sut.validatePhoneNumber(nil)

        XCTAssertEqual(presenterSpy.callPresentEmptyPhoneNumberErrorCount, 1)
    }

    func testValidatePhoneNumber_WhenTextIsEmpty_CallPresentEmptyPhoneNumberError() {
        sut.validatePhoneNumber("")

        XCTAssertEqual(presenterSpy.callPresentEmptyPhoneNumberErrorCount, 1)
    }

    func testValidatePhoneNumber_WhenTextCountIsCorrectLength_CallPresentEmptyPhoneNumberError() {
        sut.validatePhoneNumber("(12) 34567-8900")

        XCTAssertEqual(presenterSpy.callHidePhoneNumberErrorCount, 1)
    }

    func testValidatePhoneNumber_WhenTextCountIsWrongLength_CallPresentEmptyPhoneNumberError() {
        sut.validatePhoneNumber("12345-6789")

        XCTAssertEqual(presenterSpy.callPresentInvalidPhoneNumberErrorCount, 1)
    }

    func testValidatePhoneModel_WhenTextIsNil_CallShouldPresentPhoneModelError() {
        sut.validatePhoneModel(nil)

        XCTAssertEqual(presenterSpy.callShouldPresentPhoneModelErrorCount, 1)
        XCTAssertTrue(presenterSpy.presentModelError)
    }

    func testValidatePhoneModel_WhenTextIsEmpty_CallShouldPresentPhoneModelError() {
        sut.validatePhoneModel("")

        XCTAssertEqual(presenterSpy.callShouldPresentPhoneModelErrorCount, 1)
        XCTAssertTrue(presenterSpy.presentModelError)
    }

    func testValidatePhoneModel_WhenTextIsNotNilOrEmpty_CallShouldPresentPhoneModelError() {
        sut.validatePhoneModel("iPhone 12")

        XCTAssertEqual(presenterSpy.callShouldPresentPhoneModelErrorCount, 1)
        XCTAssertFalse(presenterSpy.presentModelError)
    }

    func testValidateForm_WhenAllFieldsInvalid_CallPresentEmptyNameErrorAndShowFieldsPopupError() {
        sut.validateForm(nameText: nil, dateText: "01/01/2001", numberText: "(12) 34567-8900", phoneModelText: "iPhone 12")

        XCTAssertTrue(presenterSpy.presentNameError)
        XCTAssertEqual(presenterSpy.callHideBirthDateErrorCount, 1)
        XCTAssertEqual(presenterSpy.callHidePhoneNumberErrorCount, 1)
        XCTAssertFalse(presenterSpy.presentModelError)
        XCTAssertEqual(presenterSpy.callShowFieldsPopupErrorCount, 1)
    }

    func testValidateForm_WhenPhoneModelIsInvalid_CallPresentEmptyPhoneModelErrorAndShowFieldsPopupError() {
        sut.validateForm(nameText: "Test Name", dateText: "01/01/2001", numberText: "(12) 34567-8900", phoneModelText: nil)

        XCTAssertFalse(presenterSpy.presentNameError)
        XCTAssertEqual(presenterSpy.callHideBirthDateErrorCount, 1)
        XCTAssertEqual(presenterSpy.callHidePhoneNumberErrorCount, 1)
        XCTAssertTrue(presenterSpy.presentModelError)
        XCTAssertEqual(presenterSpy.callShowFieldsPopupErrorCount, 1)
    }

    func testValidateForm_WhenAllFieldsAreValid_CallHideAllErrors() {
        sut.validateForm(nameText: "Test Name", dateText: "01/01/2001", numberText: "(12) 34567-8900", phoneModelText: "iPhone 12")

        XCTAssertFalse(presenterSpy.presentNameError)
        XCTAssertEqual(presenterSpy.callHideBirthDateErrorCount, 1)
        XCTAssertEqual(presenterSpy.callHidePhoneNumberErrorCount, 1)
        XCTAssertFalse(presenterSpy.presentModelError)
    }

    func testValidateForm_WhenAllFieldsAreValid_CallPresenterStartAndStopLoading() {
        sut.validateForm(nameText: "Test Name", dateText: "01/01/2001", numberText: "(12) 34567-8900", phoneModelText: "iPhone 12")

        XCTAssertEqual(presenterSpy.callStartLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callStopLoadingCount, 1)
    }

    func testValidateForm_WhenAllFieldsAreValid_WhenResultIsSuccess_() {
        serviceMock.result = .success(PasswordResetStepData(passwordResetId: "", nextStep: .manualReview))

        sut.validateForm(nameText: "Test Name", dateText: "01/01/2001", numberText: "(12) 34567-8900", phoneModelText: "iPhone 12")

        XCTAssertEqual(presenterSpy.callGoToNextStepCount, 1)
    }

    func testValidateForm_WhenAllFieldsAreValid_WhenResultIsFailure_() {
        sut.validateForm(nameText: "Test Name", dateText: "01/01/2001", numberText: "(12) 34567-8900", phoneModelText: "iPhone 12")

        XCTAssertEqual(presenterSpy.callShowErrorPopupCount, 1)
    }
}

private extension UserFormInteractorTests {
    func generateTomorrowDateString() -> String {
        let oneDayInSeconds: TimeInterval = 86400
        let currentDate = Date().addingTimeInterval(oneDayInSeconds)
        return DateFormatter(style: .notLenient).string(from: currentDate)
    }
}
