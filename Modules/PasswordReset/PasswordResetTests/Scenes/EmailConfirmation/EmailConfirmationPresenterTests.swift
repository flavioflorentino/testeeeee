import Core
@testable import PasswordReset
import XCTest

private final class EmailConfirmationDisplaySpy: EmailConfirmationDisplaying {
    private(set) var displayContinueButtonEnabledCallCount = 0
    private(set) var continueButtonEnabled = false
    private(set) var displayErrorCallCount = 0
    private(set) var errorTitle = ""
    private(set) var errorMessage = ""
    private(set) var displayLoadingCallCount = 0
    
    func displayContinueButtonEnabled(_ enabled: Bool) {
        displayContinueButtonEnabledCallCount += 1
        continueButtonEnabled = enabled
    }
    
    func displayError(title: String, message: String) {
        displayErrorCallCount += 1
        errorTitle = title
        errorMessage = message
    }
    
    func displayLoading(_ loading: Bool) {
        displayLoadingCallCount += 1
    }
}

private final class EmailConfirmationCoordinatorSpy: EmailConfirmationCoordinating {
    var viewController: UIViewController?
    var delegate: EmailConfirmationCoordinatorDelegate?
    private(set) var performActionCallCount = 0
    private(set) var actionPerformed: EmailConfirmationAction?
    
    func perform(action: EmailConfirmationAction) {
        performActionCallCount += 1
        actionPerformed = action
    }
}

final class EmailConfirmationPresenterTests: XCTestCase {
    private lazy var displaySpy = EmailConfirmationDisplaySpy()
    private lazy var coordinatorSpy = EmailConfirmationCoordinatorSpy()
    
    private lazy var sut: EmailConfirmationPresenter = {
        let presenter = EmailConfirmationPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()
    
    func testShouldEnableContinue_WhenEnableIsTrue_ShouldDisplayContinueButtonEnabled()  {
        sut.shouldEnableContinue(true)
        
        XCTAssertEqual(displaySpy.displayContinueButtonEnabledCallCount, 1)
        XCTAssertTrue(displaySpy.continueButtonEnabled)
    }
    
    func testShouldEnableContinue_WhenEnableIsFalse_ShouldDisplayContinueButtonDisabled()  {
        sut.shouldEnableContinue(false)
        
        XCTAssertEqual(displaySpy.displayContinueButtonEnabledCallCount, 1)
        XCTAssertFalse(displaySpy.continueButtonEnabled)
    }
    
    func testPresentError_WhenRequestErrorExists_ShouldDisplayRequestErrorInfoOnViewController() {
        let requestError = ErrorMocks.generateRequestErrorWith(title: "Title", message: "Message", code: "some code")
        sut.presentError(ApiError.unauthorized(body: requestError))
        
        XCTAssertEqual(displaySpy.displayErrorCallCount, 1)
        XCTAssertEqual(displaySpy.errorTitle, "Title")
        XCTAssertEqual(displaySpy.errorMessage, "Message")
    }
    
    func testPresentError_WhenRequestErrorInfoNotPresent_ShouldDisplayGenericErrorOnViewController() {
        sut.presentError(ApiError.serverError)
        
        XCTAssertEqual(displaySpy.displayErrorCallCount, 1)
        XCTAssertEqual(displaySpy.errorTitle, Strings.Error.title)
        XCTAssertEqual(displaySpy.errorMessage, Strings.Error.message)
    }
    
    func testPresentNextStep_WhenCalledFromViewModel_ShouldPerformActionOnCoordinator() {
        let stepData = PasswordResetStepData(passwordResetId: "abc", nextStep: .code)
        sut.presentNextStep(stepData, userEmail: "email")
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.actionPerformed, .next(stepData: stepData, email: "email"))
    }
    
    func testPresentLoading_WhenCalledFromViewModel_ShouldDisplayLoadingOnViewController() {
        sut.presentLoading(true)
        
        XCTAssertEqual(displaySpy.displayLoadingCallCount, 1)
    }
}
