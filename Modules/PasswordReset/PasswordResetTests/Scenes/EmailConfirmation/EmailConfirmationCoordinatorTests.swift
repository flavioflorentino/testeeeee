@testable import PasswordReset
import XCTest

private final class EmailConfirmationCoordinatorDelegateSpy: EmailConfirmationCoordinatorDelegate {
    private(set) var emailConfirmationNextStepCallCount = 0
    
    func emailConfirmationNextStep(_ stepData: PasswordResetStepData, userEmail: String) {
        emailConfirmationNextStepCallCount += 1
    }
}

final class EmailConfirmationCoordinatorTests: XCTestCase {
    private lazy var delegateSpy = EmailConfirmationCoordinatorDelegateSpy()
    
    private lazy var sut: EmailConfirmationCoordinator = {
        let coordinator = EmailConfirmationCoordinator()
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerform_WhenActionIsNext_ShouldCallDelegateEmailConfirmationNextStep() {
        sut.perform(action: .next(stepData: PasswordResetStepData(passwordResetId: "abc", nextStep: .code), email: "email"))
        
        XCTAssertEqual(delegateSpy.emailConfirmationNextStepCallCount, 1)
    }
}
