import AnalyticsModule
import Core
@testable import PasswordReset
import XCTest

private final class EmailConfirmationServiceMock: EmailConfirmationServicing {
    var nextStepExpectedResult: Result<PasswordResetStepData, ApiError>?
    
    func validateEmail(_ email: String, resetId: String, completion: @escaping ((Result<PasswordResetStepData, ApiError>) -> Void)) {
        guard let expectedResult = nextStepExpectedResult else {
            return
        }
        completion(expectedResult)
    }
}

private final class EmailConfirmationPresenterSpy: EmailConfirmationPresenting {
    var viewController: EmailConfirmationDisplaying?
    private(set) var shouldEnableContinueCallCount = 0
    private(set) var continueButtonEnabled = false
    private(set) var presentErrorCallCount = 0
    private(set) var presentNextStepCallCount = 0
    private(set) var presentLoadingCallCount = 0
    
    func shouldEnableContinue(_ enable: Bool) {
        shouldEnableContinueCallCount += 1
        continueButtonEnabled = enable
    }
    
    func presentError(_ error: ApiError) {
        presentErrorCallCount += 1
    }
    
    func presentNextStep(_ stepData: PasswordResetStepData, userEmail: String) {
        presentNextStepCallCount += 1
    }
    
    func presentLoading(_ loading: Bool) {
        presentLoadingCallCount += 1
    }
}

final class EmailConfirmationInteractorTests: XCTestCase {
    private lazy var serviceMock = EmailConfirmationServiceMock()
    private lazy var presenterSpy = EmailConfirmationPresenterSpy()
    
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)
    
    private lazy var sut: EmailConfirmationInteractor = {
        let interactor = EmailConfirmationInteractor(
            service: serviceMock,
            presenter: presenterSpy,
            dependencies: dependencies,
            resetId: ""
        )
        return interactor
    }()
    
    func testSendAnalytics_WhenCalledFromViewController_ShouldSendAnalyticsEvent() {
        sut.sendAnalytics()
         
        let expectedEvent = PasswordResetTracker(eventType: FallbackEvent.newEmail).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testCheckEmail_WhenEmailIsValid_ShouldEnableContinue() {
        sut.checkEmail("douglas.santos@picpay.com")
        
        XCTAssertEqual(presenterSpy.shouldEnableContinueCallCount, 1)
        XCTAssertTrue(presenterSpy.continueButtonEnabled)
    }
    
    func testCheckEmail_WhenEmailIsInvalid_ShouldNotEnableContinue() {
        sut.checkEmail("")
        
        XCTAssertEqual(presenterSpy.shouldEnableContinueCallCount, 1)
        XCTAssertFalse(presenterSpy.continueButtonEnabled)
    }
    
    func testValidateEmail_WhenServiceHasFailed_ShouldPresentError() {
        serviceMock.nextStepExpectedResult = .failure(.serverError)
        sut.validateEmail("")
        
        XCTAssertEqual(presenterSpy.presentErrorCallCount, 1)
    }
    
    func testValidateEmail_WhenCalledFromViewController_ShouldPresentLoading() {
        serviceMock.nextStepExpectedResult = .failure(.serverError)
        sut.validateEmail("")
        
        XCTAssertEqual(presenterSpy.presentLoadingCallCount, 2)
    }
    
    func testValidateEmail_WhenServiceHasFailed_ShouldSendAnalyticsEvent() {
        serviceMock.nextStepExpectedResult = .failure(.serverError)
        sut.validateEmail("")
        
        let expectedEvent = PasswordResetTracker(eventType: FallbackEvent.newEmailError).event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testValidateEmail_WhenServiceHasSucceded_ShouldPresentNextStep() {
        serviceMock.nextStepExpectedResult = .success(PasswordResetStepData(passwordResetId: "abc", nextStep: .code))
        sut.validateEmail("douglas.santos@picpay.com")
        
        XCTAssertEqual(presenterSpy.presentNextStepCallCount, 1)
    }
}
