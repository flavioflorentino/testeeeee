@testable import PasswordReset
import XCTest

private class StepLoaderCoordinatorDelegateSpy: StepLoaderCoordinatorDelegate {
    private(set) var callStepLoaderNextStepCount = 0
    private(set) var callBackFromStepLoaderCount = 0

    func stepLoaderNextStep(_ stepData: PasswordResetStepData) {
        callStepLoaderNextStepCount += 1
    }

    func backFromStepLoader() {
        callBackFromStepLoaderCount += 1
    }
}

final class StepLoaderCoordinatorTests: XCTestCase {
    private let coordinatorDelegateSpy = StepLoaderCoordinatorDelegateSpy()

    private lazy var sut: StepLoaderCoordinator = {
        let coordinator = StepLoaderCoordinator()
        coordinator.delegate = coordinatorDelegateSpy
        return coordinator
    }()

    func testPerform_WhenActionIsNextStep_CallDelegateNextStep() {
        sut.perform(action: .nextStep(PasswordResetStepData(passwordResetId: "", nextStep: .done)))

        XCTAssertEqual(coordinatorDelegateSpy.callStepLoaderNextStepCount, 1)
    }

    func testPerform_WhenActionIsBack_CallDelegateBackFromStepLoader() {
        sut.perform(action: .back)

        XCTAssertEqual(coordinatorDelegateSpy.callBackFromStepLoaderCount, 1)
    }
}
