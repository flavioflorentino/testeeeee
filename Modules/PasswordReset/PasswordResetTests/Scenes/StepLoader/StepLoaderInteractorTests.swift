import AnalyticsModule
import Core
@testable import PasswordReset
import XCTest

private class StepLoaderServicingMock: StepLoaderServicing {
    var result: Result<PasswordResetStepData, ApiError> = .failure(.serverError)

    func getNextStep(resetId: String, completion: @escaping (Result<PasswordResetStepData, ApiError>) -> Void) {
        completion(result)
    }
}

private class StepLoaderPresentingSpy: StepLoaderPresenting {
    var viewController: StepLoaderDisplay?
    private(set) var callSetupViewCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callGoToNextStepCount = 0
    private(set) var callGoBackCount = 0
    private(set) var callShowErrorCount = 0

    func setupView() {
        callSetupViewCount += 1
    }

    func startLoading() {
        callStartLoadingCount += 1
    }

    func stopLoading() {
        callStopLoadingCount += 1
    }

    func goToNextStep(_ stepData: PasswordResetStepData) {
        callGoToNextStepCount += 1
    }

    func goBack() {
        callGoBackCount += 1
    }

    func showError(_ error: RequestError?) {
        callShowErrorCount += 1
    }
}

final class StepLoaderInteractorTests: XCTestCase {
    private let presenterSpy = StepLoaderPresentingSpy()
    private let serviceMock = StepLoaderServicingMock()
    private let analyticsSpy = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(analyticsSpy)

    private lazy var sut = StepLoaderInteractor(
        presenter: presenterSpy,
        service: serviceMock,
        resetId: "ID",
        dependencies: dependenciesMock
    )

    func testSetupView_CallPresenterSetupView() {
        sut.setupView()

        XCTAssertEqual(presenterSpy.callSetupViewCount, 1)
    }

    func testSetupView_SendAppearEvent() {
        sut.setupView()

        let expectedEvent = PasswordResetTracker(method: .biometric, eventType: StepLoaderEvent.appear).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testContinueButtonAction_CallPresenterStartAndStopLoading() {
        sut.continueButtonAction()

        XCTAssertEqual(presenterSpy.callStartLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callStopLoadingCount, 1)
    }

    func testContinueButtonAction_WhenResultIsSuccess_CallPresenterGoToNextStep() {
        serviceMock.result = .success(PasswordResetStepData(passwordResetId: "", nextStep: .done))

        sut.continueButtonAction()

        XCTAssertEqual(presenterSpy.callGoToNextStepCount, 1)
    }

    func testContinueButtonAction_WhenResultIsFailure_CallPresenterShowError() {
        sut.continueButtonAction()

        XCTAssertEqual(presenterSpy.callShowErrorCount, 1)
    }

    func testBackButtonAction_CallPresenterGoBack() {
        sut.backButtonAction()

        XCTAssertEqual(presenterSpy.callGoBackCount, 1)
    }
}
