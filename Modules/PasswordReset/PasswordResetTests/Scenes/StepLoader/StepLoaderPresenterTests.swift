import Core
@testable import PasswordReset
import UI
import XCTest

private class StepLoaderCoordinatingSpy: StepLoaderCoordinating {
    weak var delegate: StepLoaderCoordinatorDelegate?
    private(set) var nextStepActionCount = 0
    private(set) var backActionCount = 0

    func perform(action: StepLoaderAction) {
        switch action {
        case .nextStep:
            nextStepActionCount += 1
        case .back:
            backActionCount += 1
        }
    }
}

private class StepLoaderDisplaySpy: StepLoaderDisplay {
    var loadingView = LoadingView()
    private(set) var callDisplayInfoCount = 0
    private(set) var callStartLoadingViewCount = 0
    private(set) var callStopLoadingViewCount = 0
    private(set) var callShowPopupControllerCount = 0

    private(set) var popupProperties: PopupProperties?

    func displayInfo(with image: UIImage, title: String, description: String, buttonTitle: String) {
        callDisplayInfoCount += 1
    }

    func startLoadingView() {
        callStartLoadingViewCount += 1
    }

    func stopLoadingView() {
        callStopLoadingViewCount += 1
    }

    func showPopupController(with properties: PopupProperties) {
        callShowPopupControllerCount += 1
        popupProperties = properties
    }
}

final class StepLoaderPresenterTests: XCTestCase {
    private let controllerSpy = StepLoaderDisplaySpy()
    private let coordinatorSpy = StepLoaderCoordinatingSpy()

    private lazy var sut: StepLoaderPresenter = {
        let presenter = StepLoaderPresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()

    func testSetupView_CallControllerDisplayInfo() {
        sut.setupView()

        XCTAssertEqual(controllerSpy.callDisplayInfoCount, 1)
    }

    func testStartLoading_CallControllerStartLoading() {
        sut.startLoading()

        XCTAssertEqual(controllerSpy.callStartLoadingViewCount, 1)
    }

    func testStopLoading_CallControllerStopLoading() {
        sut.stopLoading()

        XCTAssertEqual(controllerSpy.callStopLoadingViewCount, 1)
    }

    func testGoToNextStep_CallCoordinatorActionGoToNextStep() {
        sut.goToNextStep(PasswordResetStepData(passwordResetId: "", nextStep: .done))

        XCTAssertEqual(coordinatorSpy.nextStepActionCount, 1)
    }

    func testGoBack_CallCoordinatorBackAction() {
        sut.goBack()

        XCTAssertEqual(coordinatorSpy.backActionCount, 1)
    }

    func testShowError_CallControllerShowPopup() {
        var requestError = RequestError()
        requestError.title = "Test title"
        requestError.message = "Test message"

        sut.showError(requestError)

        XCTAssertEqual(controllerSpy.callShowPopupControllerCount, 1)
        XCTAssertEqual(controllerSpy.popupProperties?.title, "Test title")
        XCTAssertEqual(controllerSpy.popupProperties?.message, "Test message")
        XCTAssertEqual(controllerSpy.popupProperties?.mainButtonTitle, Strings.General.understood)
        XCTAssertNil(controllerSpy.popupProperties?.secondaryButtonTitle)
    }

    func testShowError_WhenRequestErrorIsNil_CallControllerShowPopup() {
        sut.showError(nil)

        XCTAssertEqual(controllerSpy.callShowPopupControllerCount, 1)
        XCTAssertEqual(controllerSpy.popupProperties?.title, Strings.Error.title)
        XCTAssertEqual(controllerSpy.popupProperties?.message, Strings.Error.message)
        XCTAssertEqual(controllerSpy.popupProperties?.mainButtonTitle, Strings.General.understood)
        XCTAssertNil(controllerSpy.popupProperties?.secondaryButtonTitle)
    }
}
