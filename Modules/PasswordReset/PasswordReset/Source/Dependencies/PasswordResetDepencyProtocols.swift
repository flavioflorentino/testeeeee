import Core

protocol HasAuthManager {
    var authenticationManager: AuthenticationContract? { get }
}
