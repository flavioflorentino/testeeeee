import AnalyticsModule
import Core
import FeatureFlag

typealias PasswordResetDependencies =
    HasMainQueue &
    HasAnalytics &
    HasFeatureManager &
    HasAuthManager

final class PasswordResetDependencyContainer: PasswordResetDependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var authenticationManager: AuthenticationContract? = PasswordResetSetup.authInstance
}
