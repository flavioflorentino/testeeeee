import Core

public enum PasswordResetSetup {
    public static var authInstance: AuthenticationContract?
    
    public static func inject(instance: AuthenticationContract?) {
        authInstance = instance
    }
}
