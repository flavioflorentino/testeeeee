import Core
import Foundation

enum PasswordResetEndpoint {
    case channels(document: String)
    case startResetFlow(cpf: String, channelType: ChannelType)
    case requestCode(resetId: String)
    case validateCode(resetId: String, code: String)
    case resetPassword(resetId: String, password: String, passwordConfirmation: String)
    case validateEmail(email: String, resetId: String)
    case selfieData(resetId: String)
    case loadStep(resetId: String)
    case personalData(data: PersonalData)
    case requestLink(resetId: String)
    case validateLink(code: String)
}

extension PasswordResetEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .channels:
            return "v2/users/password/channels"
        case .startResetFlow:
            return "v2/users/password"
        case .requestCode, .validateCode:
            return "v2/users/password/code"
        case .resetPassword:
            return "v2/users/password/"
        case .validateEmail:
            return "v2/users/password/email"
        case .selfieData:
            return "v2/users/password/selfie"
        case .loadStep:
            return "v2/users/password/step"
        case .personalData:
            return "v2/users/password/personal"
        case .requestLink, .validateLink:
            return "v2/users/password/link"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .validateCode, .resetPassword, .validateEmail, .personalData, .validateLink:
            return .put
        default:
            return .post
        }
    }
    
    var body: Data? {
        var bodyData: [String: Any]
        switch self {
        case let .channels(document):
            bodyData = ["cpf": document]
        case let .startResetFlow(cpf, channelType):
            bodyData = ["cpf": cpf, "channel": channelType.rawValue]
        case let .requestCode(resetId),
             let .selfieData(resetId),
             let .loadStep(resetId),
             let .requestLink(resetId):
            bodyData = ["password_reset_id": resetId]
        case let .validateCode(resetId, code):
            bodyData = ["password_reset_id": resetId, "code": code]
        case let .resetPassword(resetId, password, passwordConfirmation):
            bodyData = ["password_reset_id": resetId, "password": password, "password_confirmation": passwordConfirmation]
        case let .validateEmail(email, resetId):
            bodyData = ["password_reset_id": resetId, "email": email.lowercased()]
        case let .personalData(data):
            bodyData = data.bodyDictionary
        case let .validateLink(code):
            bodyData = ["code": code]
        }
        return bodyData.toData()
    }
}
