import UI

enum WarningPopupFactory {
    static func make(backCompletion: (() -> Void)?) -> PopupViewController {
        let popupController = PopupViewController(
            title: Strings.WarningPopup.title,
            description: Strings.WarningPopup.description,
            preferredType: .text
        )

        let continueAction = PopupAction(title: Strings.General.continue, style: .fill)
        popupController.addAction(continueAction)

        let backAction = PopupAction(title: Strings.General.back, style: .link, completion: backCompletion)
        popupController.addAction(backAction)

        return popupController
    }
}
