import Foundation

enum AuthorizationCodeAction {
    case back
    case nextStep(PasswordResetStepData)
}

protocol AuthorizationCodeCoordinatorDelegate: AnyObject {
    func authorizationCodeNextStep(_ stepData: PasswordResetStepData)
}

protocol AuthorizationCodeCoordinating: AnyObject {
    var delegate: AuthorizationCodeCoordinatorDelegate? { get set }
    func perform(action: AuthorizationCodeAction)
}

final class AuthorizationCodeCoordinator: AuthorizationCodeCoordinating {
    weak var delegate: AuthorizationCodeCoordinatorDelegate?
    
    func perform(action: AuthorizationCodeAction) {
        switch action {
        case .back:
            // todo
            break
        case let .nextStep(step):
            delegate?.authorizationCodeNextStep(step)
        }
    }
}
