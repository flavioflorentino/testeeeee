import Core
import Foundation

protocol AuthorizationCodeServicing {
    func requestCode(resetId: String, completion: @escaping (Result<CodeConfirmation, ApiError>) -> Void)
    func validateCode(_ code: String, resetId: String, completion: @escaping (Result<PasswordResetStepData, ApiError>) -> Void)
}

final class AuthorizationCodeService: AuthorizationCodeServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    // MARK: - AuthorizationCodeServicing

    func requestCode(resetId: String, completion: @escaping (Result<CodeConfirmation, ApiError>) -> Void) {
        let api = Api<CodeConfirmation>(endpoint: PasswordResetEndpoint.requestCode(resetId: resetId))

        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }

    func validateCode(_ code: String, resetId: String, completion: @escaping (Result<PasswordResetStepData, ApiError>) -> Void) {
        let api = Api<PasswordResetStepData>(endpoint: PasswordResetEndpoint.validateCode(resetId: resetId, code: code))

        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
