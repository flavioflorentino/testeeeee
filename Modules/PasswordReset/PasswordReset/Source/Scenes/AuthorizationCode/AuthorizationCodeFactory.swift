import Foundation

enum AuthorizationCodeFactory {
    static func make(
        channelType: ChannelType,
        resetId: String,
        coordinatorDelegate: AuthorizationCodeCoordinatorDelegate,
        emailString: String? = nil
    ) -> AuthorizationCodeViewController {
        let container = PasswordResetDependencyContainer()
        let service = AuthorizationCodeService(dependencies: container)
        let coordinator = AuthorizationCodeCoordinator()
        let presenter = AuthorizationCodePresenter(coordinator: coordinator, emailString: emailString)
        let interactor = AuthorizationCodeInteractor(
            service: service,
            presenter: presenter,
            channelType: channelType,
            resetId: resetId,
            dependencies: container
        )
        let viewController = AuthorizationCodeViewController(interactor: interactor)

        presenter.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        return viewController
    }
}
