import AnalyticsModule
import CoreGraphics
import Foundation

private enum KeyboardState {
    case showing
    case hiding
}

protocol AuthorizationCodeInteracting: PasswordResetTimerDelegate {
    func setupView()
    func keyboardWillShow(currentSpacing: CGFloat, minSpacing: CGFloat, properties: KeyboardProperties)
    func keyboardWillHide(properties: KeyboardProperties)
    func retryRequestCode()
    func backAction()
    func textfieldEditingChange(_ text: String?)
    func confirmCode(_ text: String?)
}

final class AuthorizationCodeInteractor: AuthorizationCodeInteracting {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: AuthorizationCodeServicing
    private let presenter: AuthorizationCodePresenting
    private let channelType: ChannelType
    private let resetId: String

    private let codeLenght = 4
    private var keyboardState: KeyboardState = .hiding
    private var offset: CGFloat = 0

    private var timer: PasswordResetTimerProtocol
    private var countdownSeconds = 300

    init(
        service: AuthorizationCodeServicing,
        presenter: AuthorizationCodePresenting,
        channelType: ChannelType,
        resetId: String,
        dependencies: Dependencies,
        timer: PasswordResetTimerProtocol = PasswordResetTimer()
    ) {
        self.service = service
        self.presenter = presenter
        self.channelType = channelType
        self.resetId = resetId
        self.dependencies = dependencies
        self.timer = timer
        self.timer.delegate = self
    }

    func setupView() {
        trackEvent(AuthorizationCodeEvent.appear)
        presenter.setupDescription(for: channelType)
        presenter.disableConfirmButton()
        requestCodeForChannel()
    }

    func keyboardWillShow(currentSpacing: CGFloat, minSpacing: CGFloat, properties: KeyboardProperties) {
        guard keyboardState != .showing else {
            return
        }
        keyboardState = .showing

        if currentSpacing < minSpacing {
            offset = minSpacing - currentSpacing
            presenter.translateViewY(by: offset * -1, with: properties)
        }
    }

    func keyboardWillHide(properties: KeyboardProperties) {
        guard keyboardState != .hiding else {
            return
        }
        keyboardState = .hiding
        presenter.translateViewY(by: offset, with: properties)
    }

    func retryRequestCode() {
        trackEvent(AuthorizationCodeEvent.resend)
        requestCodeForChannel()
    }

    func backAction() {
        presenter.goBack()
    }

    func textfieldEditingChange(_ text: String?) {
        guard let code = text else {
            presenter.disableConfirmButton()
            return
        }

        let trimmedCode = String(code.prefix(codeLenght))
        presenter.setTextfieldText(trimmedCode)

        if trimmedCode.count == codeLenght {
            presenter.enableConfirmButton()
        } else {
            presenter.disableConfirmButton()
        }
    }

    func confirmCode(_ text: String?) {
        guard let code = text, code.count == codeLenght else {
            return
        }
        validateCode(code)
    }

    // MARK: - PasswordResetTimerDelegate

    func timerTic() {
        if countdownSeconds <= 0 {
            timer.stop()
            presenter.setupRetryButtonTitle(isEnabled: true, remainingSeconds: countdownSeconds)
        } else {
            presenter.setupRetryButtonTitle(isEnabled: false, remainingSeconds: countdownSeconds)
            countdownSeconds -= 1
        }
    }
}

private extension AuthorizationCodeInteractor {
    func requestCodeForChannel() {
        presenter.startLoading()

        service.requestCode(resetId: resetId) { [weak self] result in
            self?.presenter.stopLoading()

            switch result {
            case .success(let confirmation):
                self?.startTimer(countdown: confirmation.retryTime)

            case .failure(let apiError):
                self?.handleRequestCodeErrorTracking(apiError.requestError?.code)
                self?.timer.stop()
                self?.presenter.presentRetryError(apiError.requestError)
            }
        }
    }

    func startTimer(countdown: Int) {
        countdownSeconds = countdown
        timer.start()
    }

    func validateCode(_ code: String) {
        presenter.startLoading()

        service.validateCode(code, resetId: resetId) { [weak self] result in
            self?.presenter.stopLoading()

            switch result {
            case .success(let validation):
                self?.trackEvent(AuthorizationCodeEvent.success)
                self?.presenter.goToNextStep(validation)

            case .failure(let apiError):
                self?.handleValidateCodeErrorTracking(apiError.requestError?.code)
                self?.presenter.presentValidationError(apiError.requestError)
            }
        }
    }

    func trackEvent(_ event: PasswordResetEvent) {
        let eventTracker = PasswordResetTracker(method: channelType, eventType: event)
        dependencies.analytics.log(eventTracker)
    }

    func handleRequestCodeErrorTracking(_ errorCode: String?) {
        let errorCode = PasswordResetErrorCode(errorCode)
        switch errorCode {
        case .passwordResetBlock:
            trackEvent(ErrorEvent.blocked)
        default:
            trackEvent(AuthorizationCodeEvent.channelError)
        }
    }

    func handleValidateCodeErrorTracking(_ errorCode: String?) {
        let errorCode = PasswordResetErrorCode(errorCode)
        switch errorCode {
        case .passwordResetBlock:
            trackEvent(AuthorizationCodeEvent.attemptLimit)
        case .invalidCode:
            trackEvent(AuthorizationCodeEvent.invalidCode)
        default:
            trackEvent(ErrorEvent.generic)
        }
    }
}
