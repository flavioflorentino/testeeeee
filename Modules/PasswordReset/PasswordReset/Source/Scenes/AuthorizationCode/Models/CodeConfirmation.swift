import Foundation

struct CodeConfirmation: Decodable {
    let retryTime: Int
}
