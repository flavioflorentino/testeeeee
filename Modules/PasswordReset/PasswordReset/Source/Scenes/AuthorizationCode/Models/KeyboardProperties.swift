import Foundation

struct KeyboardProperties {
    let height: CGFloat
    let animationCurve: UInt
    let animationDuration: TimeInterval
}
