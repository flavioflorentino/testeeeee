import Core
import CoreGraphics
import Foundation
import UI

protocol AuthorizationCodePresenting: AnyObject {
    var viewController: AuthorizationCodeDisplay? { get set }
    func setupDescription(for type: ChannelType)
    func setupRetryButtonTitle(isEnabled: Bool, remainingSeconds: Int)
    func translateViewY(by offset: CGFloat, with properties: KeyboardProperties)
    func setTextfieldText(_ text: String)
    func presentRetryError(_ requestError: RequestError?)
    func presentValidationError(_ requestError: RequestError?)
    func startLoading()
    func stopLoading()
    func disableConfirmButton()
    func enableConfirmButton()
    func goToNextStep(_ step: PasswordResetStepData)
    func goBack()
}

final class AuthorizationCodePresenter: AuthorizationCodePresenting {
    private let coordinator: AuthorizationCodeCoordinating
    private let emailString: String?
    private let timeFormatter: TimeFormatterProtocol

    private let primaryAttributes: [NSAttributedString.Key: Any] = [
        .font: Typography.bodyPrimary().font(),
        .foregroundColor: Colors.grayscale750.color
    ]

    private let secondaryAttributes: [NSAttributedString.Key: Any] = [
        .font: Typography.bodySecondary().font(),
        .foregroundColor: Colors.grayscale500.color
    ]

    private let primaryBoldAttributes: [NSAttributedString.Key: Any] = [
        .font: Typography.bodyPrimary(.highlight).font(),
        .foregroundColor: Colors.grayscale750.color
    ]

    private let secondaryBoldAttributes: [NSAttributedString.Key: Any] = [
        .font: Typography.bodySecondary(.highlight).font(),
        .foregroundColor: Colors.grayscale500.color
    ]

    private let underlineAttributes: [NSAttributedString.Key: Any] = [
        .foregroundColor: Colors.branding600.color,
        .underlineStyle: NSUnderlineStyle.single.rawValue
    ]

    weak var viewController: AuthorizationCodeDisplay?

    init(
        coordinator: AuthorizationCodeCoordinating,
        emailString: String?,
        timeFormatter: TimeFormatterProtocol = TimeFormatter()
    ) {
        self.coordinator = coordinator
        self.emailString = emailString
        self.timeFormatter = timeFormatter
    }

    // MARK: - AuthorizationCodePresenting

    func setupDescription(for type: ChannelType) {
        switch type {
        case .email:
            viewController?.setDescriptionText(Strings.Authorization.Description.Email.label)
        case .sms:
            viewController?.setDescriptionText(Strings.Authorization.Description.Sms.label)
        case .biometric:
            guard let mail = emailString else { break }

            let attributedString = createAttributedString(
                string: Strings.Authorization.Description.Biometric.label(mail),
                attributes: primaryAttributes,
                substring: mail,
                substringAttributes: primaryBoldAttributes
            )
            viewController?.setDescriptionAttributedString(attributedString)
        }
    }

    func setupRetryButtonTitle(isEnabled: Bool, remainingSeconds: Int) {
        let attributedString: NSAttributedString
        let state: UIControl.State

        if isEnabled {
            attributedString = createEnabledButtonAttributedString()
            state = .normal
        } else {
            attributedString = createDisabledButtonAttributedString(remainingSeconds: remainingSeconds)
            state = .disabled
        }

        viewController?.setRetryButtonAttributedString(attributedString, isEnabled: isEnabled, state: state)
    }

    func translateViewY(by offset: CGFloat, with properties: KeyboardProperties) {
        viewController?.translateViewY(by: offset, with: properties)
    }

    func setTextfieldText(_ text: String) {
        viewController?.setTextfieldText(text)
    }

    func presentRetryError(_ requestError: RequestError?) {
        viewController?.showPopupController(with: createPopupProperties(from: requestError, isRetry: true))
    }

    func presentValidationError(_ requestError: RequestError?) {
        viewController?.showPopupController(with: createPopupProperties(from: requestError, isRetry: false))
    }

    func startLoading() {
        viewController?.startLoadingView()
    }

    func stopLoading() {
        viewController?.stopLoadingView()
    }

    func disableConfirmButton() {
        viewController?.disableConfirmButton()
    }

    func enableConfirmButton() {
        viewController?.enableConfirmButton()
    }

    func goToNextStep(_ step: PasswordResetStepData) {
        coordinator.perform(action: .nextStep(step))
    }

    func goBack() {
        coordinator.perform(action: .back)
    }
}

private extension AuthorizationCodePresenter {
    func createDisabledButtonAttributedString(remainingSeconds: Int) -> NSAttributedString {
        let remainingTimeString = timeFormatter.formatString(from: TimeInterval(remainingSeconds))
        let boldString = Strings.Authorization.wait(remainingTimeString)

        return createAttributedString(
            string: Strings.Authorization.Button.retry(boldString),
            attributes: secondaryAttributes,
            substring: boldString,
            substringAttributes: secondaryBoldAttributes
        )
    }

    func createEnabledButtonAttributedString() -> NSAttributedString {
        let underlineString = Strings.Authorization.resendCode

        return createAttributedString(
            string: Strings.Authorization.Button.retry(underlineString),
            attributes: secondaryAttributes,
            substring: underlineString,
            substringAttributes: underlineAttributes
        )
    }

    func createAttributedString(
        string: String,
        attributes: [NSAttributedString.Key: Any],
        substring: String,
        substringAttributes: [NSAttributedString.Key: Any]
    ) -> NSAttributedString {
        let mutableAttributedString = NSMutableAttributedString(string: string, attributes: attributes)
        let attributedRange = (mutableAttributedString.string as NSString).range(of: substring)
        mutableAttributedString.addAttributes(substringAttributes, range: attributedRange)
        return mutableAttributedString
    }

    func createPopupProperties(from error: RequestError?, isRetry: Bool) -> PopupProperties {
        let mainButtonTitle = isRetry ? Strings.General.tryAgain : Strings.General.understood
        let secondaryButtonTitle = isRetry ? Strings.General.back : nil

        return PopupProperties(
            title: error?.title ?? Strings.Error.title,
            message: error?.message ?? Strings.Error.message,
            mainButtonTitle: mainButtonTitle,
            secondaryButtonTitle: secondaryButtonTitle
        )
    }
}
