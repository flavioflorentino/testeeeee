import UI
import UIKit

protocol AuthorizationCodeDisplay: AnyObject, LoadingViewProtocol {
    func setDescriptionText(_ text: String)
    func setDescriptionAttributedString(_ string: NSAttributedString)
    func setRetryButtonAttributedString(_ string: NSAttributedString, isEnabled: Bool, state: UIControl.State)
    func translateViewY(by offset: CGFloat, with properties: KeyboardProperties)
    func setTextfieldText(_ text: String)
    func showPopupController(with properties: PopupProperties)
    func disableConfirmButton()
    func enableConfirmButton()
}

private extension AuthorizationCodeViewController.Layout {
    enum Spacing {
        static let minBetweenRetryButtonAndKeyboard = UI.Spacing.base04
    }
}

final class AuthorizationCodeViewController: ViewController<AuthorizationCodeInteracting, UIView>, AuthorizationCodeDisplay {
    fileprivate enum Layout { }

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .grayscale750())
            .with(\.text, Strings.Authorization.Title.label)
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale750())
        return label
    }()

    private lazy var codeTextField: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.defaultForm()
        textfield.keyboardType = .numberPad
        textfield.placeholder = Strings.Authorization.TextField.placeholder
        textfield.placeholderFont = Typography.bodyPrimary().font()
        textfield.selectedTitleColor = Colors.grayscale400.color
        textfield.font = Typography.bodyPrimary().font()
        textfield.textColor = Colors.grayscale700.color
        textfield.addTarget(self, action: #selector(codeTextFieldEditingChanged), for: .editingChanged)
        return textfield
    }()

    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.Authorization.confirmCode, for: .normal)
        button.addTarget(self, action: #selector(didTapConfirm), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()

    private lazy var retryButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTapRetryButton), for: .touchUpInside)
        return button
    }()

    var loadingView = LoadingView()

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.setupView()

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(notification:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(notification:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }

    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(codeTextField)
        view.addSubview(confirmButton)
        view.addSubview(retryButton)
    }

    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(view.snp.topMargin).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalTo(titleLabel)
        }

        codeTextField.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalTo(descriptionLabel)
        }

        confirmButton.snp.makeConstraints {
            $0.top.equalTo(codeTextField.snp.bottom).offset(Spacing.base05)
            $0.leading.trailing.equalTo(codeTextField)
        }

        retryButton.snp.makeConstraints {
            $0.top.equalTo(confirmButton.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalTo(confirmButton)
        }
    }

    override func configureViews() {
        let backButton = UIBarButtonItem()
        backButton.title = String()
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        view.backgroundColor = Colors.backgroundPrimary.color
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        codeTextField.resignFirstResponder()
    }

    // MARK: - AuthorizationCodeDisplay

    func setDescriptionText(_ text: String) {
        descriptionLabel.text = text
    }

    func setDescriptionAttributedString(_ string: NSAttributedString) {
        descriptionLabel.attributedText = string
    }

    func setRetryButtonAttributedString(_ string: NSAttributedString, isEnabled: Bool, state: UIControl.State) {
        retryButton.isEnabled = isEnabled
        retryButton.setAttributedTitle(string, for: state)
    }

    func translateViewY(by offset: CGFloat, with properties: KeyboardProperties) {
        UIView.animate(withDuration: properties.animationDuration,
                       delay: 0,
                       options: [.init(rawValue: properties.animationCurve)],
                       animations: {
                            self.view.frame.origin.y += offset
        })
    }

    func setTextfieldText(_ text: String) {
        codeTextField.text = text
    }

    func showPopupController(with properties: PopupProperties) {
        let popupController = PopupViewController(
            title: properties.title,
            description: properties.message,
            preferredType: .text
        )
        addPopupButtons(with: properties, controller: popupController)
        showPopup(popupController)
    }

    func disableConfirmButton() {
        confirmButton.isEnabled = false
    }

    func enableConfirmButton() {
        confirmButton.isEnabled = true
    }
}

@objc
private extension AuthorizationCodeViewController {
    // MARK: - Keyboard

    func keyboardWillShow(notification: NSNotification) {
        guard let keyboardProperties = getKeyboardProperties(from: notification) else {
            return
        }

        let keyboardYInView = view.frame.height - keyboardProperties.height
        let retryButtonMaxY = retryButton.frame.maxY
        let currentSpacing = keyboardYInView - retryButtonMaxY

        interactor.keyboardWillShow(currentSpacing: currentSpacing,
                                    minSpacing: Layout.Spacing.minBetweenRetryButtonAndKeyboard,
                                    properties: keyboardProperties)
    }

    func keyboardWillHide(notification: NSNotification) {
        guard let keyboardProperties = getKeyboardProperties(from: notification) else {
            return
        }
        interactor.keyboardWillHide(properties: keyboardProperties)
    }

    // MARK: - Textfield editing

    func codeTextFieldEditingChanged() {
        interactor.textfieldEditingChange(codeTextField.text)
    }

    // MARK: - Buttons

    func didTapConfirm() {
        codeTextField.resignFirstResponder()
        interactor.confirmCode(codeTextField.text)
    }

    func didTapRetryButton() {
        codeTextField.resignFirstResponder()
        interactor.retryRequestCode()
    }
}

private extension AuthorizationCodeViewController {
    func getKeyboardProperties(from notification: NSNotification) -> KeyboardProperties? {
        guard
            let frame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect,
            let curve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt,
            let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double
            else {
                return nil
        }
        return KeyboardProperties(height: frame.height, animationCurve: curve, animationDuration: duration)
    }

    func addPopupButtons(with properties: PopupProperties, controller: PopupViewController) {
        guard let secondaryTitle = properties.secondaryButtonTitle else {
            controller.addAction(PopupAction(title: properties.mainButtonTitle, style: .fill))
            return
        }

        let tryAgainAction = PopupAction(title: properties.mainButtonTitle, style: .fill) { [weak self, weak controller] in
            controller?.dismiss(animated: true) {
                self?.interactor.retryRequestCode()
            }
        }
        controller.addAction(tryAgainAction)

        let backAction = PopupAction(title: secondaryTitle, style: .link) { [weak self, weak controller] in
            controller?.dismiss(animated: true) {
                self?.interactor.backAction()
            }
        }
        controller.addAction(backAction)
    }
}
