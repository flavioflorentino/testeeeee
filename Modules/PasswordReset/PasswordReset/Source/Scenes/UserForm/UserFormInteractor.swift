import AnalyticsModule
import Core
import Foundation
import UI

protocol UserFormInteracting {
    func backButtonAction()
    func informationButtonAction()
    func nameFieldBecomesFirstResponder()
    func birthDateFieldBecomesFirstResponder()
    func phoneNumberFieldBecomesFirstResponder()
    func phoneModelFieldBecomesFirstResponder()
    func validateName(_ text: String?)
    func validateDateOfBirth(_ text: String?)
    func validatePhoneNumber(_ text: String?)
    func validatePhoneModel(_ text: String?)
    func validateForm(nameText: String?, dateText: String?, numberText: String?, phoneModelText: String?)
}

final class UserFormInteractor: UserFormInteracting {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: UserFormServicing
    private let presenter: UserFormPresenting
    private let resetId: String

    private let phoneLength = PersonalDocumentMaskDescriptor.cellPhoneWithDDD.format.count
    private let dateLentgth = 10
    private var isNameValid = false
    private var isDateOfBirthValid = false
    private var isPhoneNumberValid = false
    private var isPhoneModelValid = false

    init(service: UserFormServicing, presenter: UserFormPresenting, dependencies: Dependencies, resetId: String) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.resetId = resetId
    }

    // MARK: - UserFormInteracting

    func backButtonAction() {
        presenter.goBack()
    }

    func informationButtonAction() {
        presenter.presentInformationPopup()
    }

    func nameFieldBecomesFirstResponder() {
        presenter.shouldPresentNameError(false)
    }

    func birthDateFieldBecomesFirstResponder() {
        presenter.hideBirthDateError()
    }

    func phoneNumberFieldBecomesFirstResponder() {
        presenter.hidePhoneNumberError()
    }

    func phoneModelFieldBecomesFirstResponder() {
        presenter.shouldPresentPhoneModelError(false)
    }

    func validateName(_ text: String?) {
        if let nameString = text, !nameString.isEmpty {
            isNameValid = true
        } else {
            isNameValid = false
        }
        presenter.shouldPresentNameError(!isNameValid)
    }

    func validateDateOfBirth(_ text: String?) {
        guard let birthDateString = text, !birthDateString.isEmpty else {
            isDateOfBirthValid = false
            presenter.presentEmptyBirthDateError()
            return
        }

        guard let date = DateFormatter(style: .notLenient).date(from: birthDateString) else {
            isDateOfBirthValid = false
            presenter.presentInvalidBirthDateError()
            return
        }

        let currentDate = Date()
        if date < currentDate && birthDateString.count == dateLentgth {
            isDateOfBirthValid = true
            presenter.hideBirthDateError()
        } else {
            isDateOfBirthValid = false
            presenter.presentInvalidBirthDateError()
        }
    }

    func validatePhoneNumber(_ text: String?) {
        guard let phoneString = text, !phoneString.isEmpty else {
            isPhoneNumberValid = false
            presenter.presentEmptyPhoneNumberError()
            return
        }

        if phoneString.count == phoneLength {
            isPhoneNumberValid = true
            presenter.hidePhoneNumberError()
        } else {
            isPhoneNumberValid = false
            presenter.presentInvalidPhoneNumberError()
        }
    }

    func validatePhoneModel(_ text: String?) {
        if let phoneModelString = text, !phoneModelString.isEmpty {
            isPhoneModelValid = true
        } else {
            isPhoneModelValid = false
        }
        presenter.shouldPresentPhoneModelError(!isPhoneModelValid)
    }

    func validateForm(nameText: String?, dateText: String?, numberText: String?, phoneModelText: String?) {
        validateName(nameText)
        validateDateOfBirth(dateText)
        validatePhoneNumber(numberText)
        validatePhoneModel(phoneModelText)

        guard areFieldsValid() else {
            presenter.showFieldsPopupError()
            return
        }

        let personalData = PersonalData(
            resetId: resetId,
            name: nameText ?? "",
            birthDate: dateText ?? "",
            phoneNumber: numberText ?? "",
            deviceModel: phoneModelText ?? ""
        )

        sendFormData(personalData)
    }
}

private extension UserFormInteractor {
    func areFieldsValid() -> Bool {
        isNameValid && isDateOfBirthValid && isPhoneNumberValid && isPhoneModelValid
    }

    func sendFormData(_ data: PersonalData) {
        presenter.startLoading()

        service.sendFormData(personalData: data) { [weak self] result in
            self?.presenter.stopLoading()

            switch result {
            case .success(let stepData):
                self?.presenter.goToNextStep(stepData)
            case .failure(let apiError):
                self?.presenter.showErrorPopup(with: apiError.requestError)
            }
        }
    }
}
