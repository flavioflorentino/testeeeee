import Core
import Foundation

protocol UserFormServicing {
    func sendFormData(personalData: PersonalData, completion: @escaping (Result<PasswordResetStepData, ApiError>) -> Void)
}

final class UserFormService: UserFormServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    // MARK: - UserFormServicing

    func sendFormData(personalData: PersonalData, completion: @escaping (Result<PasswordResetStepData, ApiError>) -> Void) {
        let endpoint = PasswordResetEndpoint.personalData(data: personalData)
        let api = Api<PasswordResetStepData>(endpoint: endpoint)

        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
