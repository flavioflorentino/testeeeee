import Foundation

enum UserFormAction {
    case nextStep(PasswordResetStepData)
    case back
}

protocol UserFormCoordinating {
    var delegate: UserFormCoordinatorDelegate? { get set }
    func perform(action: UserFormAction)
}

protocol UserFormCoordinatorDelegate: AnyObject {
    func userFormNextStep(_ stepData: PasswordResetStepData)
    func backFromUserForm()
}

final class UserFormCoordinator: UserFormCoordinating {
    weak var delegate: UserFormCoordinatorDelegate?

    // MARK: - UserFormCoordinating

    func perform(action: UserFormAction) {
        switch action {
        case .nextStep(let data):
            delegate?.userFormNextStep(data)
        case .back:
            delegate?.backFromUserForm()
        }
    }
}
