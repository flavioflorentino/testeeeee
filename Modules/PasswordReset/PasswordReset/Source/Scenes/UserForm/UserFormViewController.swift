import UI
import UIKit

protocol UserFormDisplaying: AnyObject, LoadingViewProtocol {
    func showPopupController(with properties: PopupProperties)
    func shouldShowNameFieldError(_ shouldShow: Bool, with message: String?)
    func shouldShowDateFieldError(_ shouldShow: Bool, with message: String?)
    func shouldShowPhoneFieldError(_ shouldShow: Bool, with message: String?)
    func shouldShowModelFieldError(_ shouldShow: Bool, with message: String?)
}

private extension UserFormViewController.Layout {
    enum Insets {
        static let stackView = UIEdgeInsets(top: 0, left: Spacing.base02, bottom: Spacing.base04, right: Spacing.base02)
    }

    enum Size {
        static let backButton: CGFloat = 28
        static let informationButton: CGFloat = 28
    }
}

final class UserFormViewController: ViewController<UserFormInteracting, UIView>, UserFormDisplaying {
    fileprivate enum Layout { }

    var loadingView = LoadingView()

    private lazy var headerView = UIView()

    private lazy var backButton: UIButton = {
        let backButton = UIButton()
        backButton.setImage(Assets.backGreenArrow.image, for: .normal)
        backButton.addTarget(self, action: #selector(tapBackButton), for: .touchUpInside)
        return backButton
    }()

    private lazy var informationButton: UIButton = {
        let informationButton = UIButton()
        informationButton.setImage(Assets.informationIcon.image, for: .normal)
        informationButton.addTarget(self, action: #selector(tapInformationButton), for: .touchUpInside)
        return informationButton
    }()

    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.keyboardDismissMode = .onDrag
        return scrollView
    }()

    private lazy var keyboardScrollViewHandler = KeyboardScrollViewHandler(scrollView: scrollView)

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = Layout.Insets.stackView
        return stackView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .grayscale700())
            .with(\.text, Strings.UserForm.Label.title)
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale700())
            .with(\.text, Strings.UserForm.Label.description)
        return label
    }()

    private lazy var nameTextField: ErrorHintFloatingTextField = {
        let textField = ErrorHintFloatingTextField()
        textField.setPlaceholder(Strings.UserForm.Placeholder.name)
        textField.editingDidBegin = { [weak self] in self?.nameFieldEditingDidBegin() }
        textField.editingDidEnd = { [weak self] in self?.nameFieldEditingDidEnd() }
        textField.returnKeyType = .next
        return textField
    }()

    private lazy var birthDateTextField: ErrorHintFloatingTextField = {
        let textField = ErrorHintFloatingTextField()
        textField.setPlaceholder(Strings.UserForm.Placeholder.date)
        textField.editingDidBegin = { [weak self] in self?.dateFieldEditingDidBegin() }
        textField.editingDidEnd = { [weak self] in self?.dateFieldEditingDidEnd() }
        textField.returnKeyType = .next
        textField.keyboardType = .numberPad
        textField.setTextFieldMask(with: PersonalDocumentMaskDescriptor.date)
        return textField
    }()

    private lazy var phoneTextField: ErrorHintFloatingTextField = {
        let textField = ErrorHintFloatingTextField()
        textField.setPlaceholder(Strings.UserForm.Placeholder.phone)
        textField.editingDidBegin = { [weak self] in self?.phoneFieldEditingDidBegin() }
        textField.editingDidEnd = { [weak self] in self?.phoneFieldEditingDidEnd() }
        textField.returnKeyType = .next
        textField.keyboardType = .numberPad
        textField.setTextFieldMask(with: PersonalDocumentMaskDescriptor.cellPhoneWithDDD)
        return textField
    }()

    private lazy var deviceTextField: ErrorHintFloatingTextField = {
        let textField = ErrorHintFloatingTextField()
        textField.setPlaceholder(Strings.UserForm.Placeholder.device)
        textField.editingDidBegin = { [weak self] in self?.deviceFieldEditingDidBegin() }
        textField.editingDidEnd = { [weak self] in self?.deviceFieldEditingDidEnd() }
        textField.returnKeyType = .done
        return textField
    }()

    private lazy var sendButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.General.continue, for: .normal)
        button.addTarget(self, action: #selector(didTapSend), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func buildViewHierarchy() {
        view.addSubview(headerView)
        headerView.addSubview(backButton)
        headerView.addSubview(informationButton)
        view.addSubview(scrollView)
        scrollView.addSubview(stackView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(descriptionLabel)
        stackView.addArrangedSubview(nameTextField)
        stackView.addArrangedSubview(birthDateTextField)
        stackView.addArrangedSubview(phoneTextField)
        stackView.addArrangedSubview(deviceTextField)
        stackView.addArrangedSubview(sendButton)
    }

    override func setupConstraints() {
        headerView.snp.makeConstraints {
            $0.leading.top.trailing.equalToSuperview()
        }

        backButton.snp.makeConstraints {
            $0.leading.bottom.equalToSuperview().inset(Spacing.base00)
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.size.equalTo(Layout.Size.backButton)
        }

        informationButton.snp.makeConstraints {
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalToSuperview().inset(Spacing.base01)
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.size.equalTo(Layout.Size.informationButton)
        }

        scrollView.snp.makeConstraints {
            $0.top.equalTo(headerView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }

        stackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.width.equalTo(scrollView.snp.width)
        }
    }

    override func configureViews() {
        keyboardScrollViewHandler.registerForKeyboardNotifications()
        view.backgroundColor = Colors.backgroundPrimary.color
    }

    // MARK: - UserFormDisplaying

    func showPopupController(with properties: PopupProperties) {
        let popupController = PopupViewController(
            title: properties.title,
            description: properties.message,
            preferredType: .text
        )
        popupController.addAction(PopupAction(title: properties.mainButtonTitle, style: .fill))
        showPopup(popupController)
    }

    func shouldShowNameFieldError(_ shouldShow: Bool, with message: String?) {
        nameTextField.shouldShowError(shouldShow, with: message)
    }

    func shouldShowDateFieldError(_ shouldShow: Bool, with message: String?) {
        birthDateTextField.shouldShowError(shouldShow, with: message)
    }

    func shouldShowPhoneFieldError(_ shouldShow: Bool, with message: String?) {
        phoneTextField.shouldShowError(shouldShow, with: message)
    }

    func shouldShowModelFieldError(_ shouldShow: Bool, with message: String?) {
        deviceTextField.shouldShowError(shouldShow, with: message)
    }
}

@objc
private extension UserFormViewController {
    func tapBackButton() {
        interactor.backButtonAction()
    }

    func tapInformationButton() {
        interactor.informationButtonAction()
    }

    func didTapSend() {
        interactor.validateForm(nameText: nameTextField.text,
                                dateText: birthDateTextField.text,
                                numberText: phoneTextField.text,
                                phoneModelText: deviceTextField.text)
    }
}

private extension UserFormViewController {
    func nameFieldEditingDidBegin() {
        interactor.nameFieldBecomesFirstResponder()
    }

    func nameFieldEditingDidEnd() {
        interactor.validateName(nameTextField.text)
    }

    func dateFieldEditingDidBegin() {
        interactor.birthDateFieldBecomesFirstResponder()
    }

    func dateFieldEditingDidEnd() {
        interactor.validateDateOfBirth(birthDateTextField.text)
    }

    func phoneFieldEditingDidBegin() {
        interactor.phoneNumberFieldBecomesFirstResponder()
    }

    func phoneFieldEditingDidEnd() {
        interactor.validatePhoneNumber(phoneTextField.text)
    }

    func deviceFieldEditingDidBegin() {
        interactor.phoneModelFieldBecomesFirstResponder()
    }

    func deviceFieldEditingDidEnd() {
        interactor.validatePhoneModel(deviceTextField.text)
    }
}
