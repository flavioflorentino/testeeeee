import UIKit

enum UserFormFactory {
    static func make(resetId: String, coordinatorDelegate: UserFormCoordinatorDelegate?) -> UserFormViewController {
        let container = PasswordResetDependencyContainer()
        let service = UserFormService(dependencies: container)
        let coordinator = UserFormCoordinator()
        let presenter = UserFormPresenter(coordinator: coordinator)
        let interactor = UserFormInteractor(service: service, presenter: presenter, dependencies: container, resetId: resetId)
        let viewController = UserFormViewController(interactor: interactor)

        presenter.viewController = viewController
        coordinator.delegate = coordinatorDelegate

        return viewController
    }
}
