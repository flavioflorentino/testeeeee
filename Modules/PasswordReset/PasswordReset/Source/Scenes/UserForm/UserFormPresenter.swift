import Core
import Foundation

protocol UserFormPresenting {
    var viewController: UserFormDisplaying? { get set }
    func startLoading()
    func stopLoading()
    func presentInformationPopup()
    func presentEmptyBirthDateError()
    func presentInvalidBirthDateError()
    func hideBirthDateError()
    func presentEmptyPhoneNumberError()
    func presentInvalidPhoneNumberError()
    func hidePhoneNumberError()
    func shouldPresentNameError(_ shouldPresent: Bool)
    func shouldPresentPhoneModelError(_ shouldPresent: Bool)
    func showFieldsPopupError()
    func goToNextStep(_ step: PasswordResetStepData)
    func goBack()
    func showErrorPopup(with requestError: RequestError?)
}

final class UserFormPresenter: UserFormPresenting {
    private let coordinator: UserFormCoordinating
    weak var viewController: UserFormDisplaying?

    init(coordinator: UserFormCoordinating) {
        self.coordinator = coordinator
    }

    // MARK: - UserFormPresenting

    func startLoading() {
        viewController?.startLoadingView()
    }

    func stopLoading() {
        viewController?.stopLoadingView()
    }

    func presentInformationPopup() {
        let properties = PopupProperties(
            title: Strings.UserForm.InfoPopup.title,
            message: Strings.UserForm.InfoPopup.message,
            mainButtonTitle: Strings.General.understood,
            secondaryButtonTitle: nil
        )
        viewController?.showPopupController(with: properties)
    }

    func presentEmptyBirthDateError() {
        viewController?.shouldShowDateFieldError(true, with: Strings.UserForm.Error.blankField)
    }

    func presentInvalidBirthDateError() {
        viewController?.shouldShowDateFieldError(true, with: Strings.UserForm.Error.invalidField)
    }

    func hideBirthDateError() {
        viewController?.shouldShowDateFieldError(false, with: nil)
    }

    func presentEmptyPhoneNumberError() {
        viewController?.shouldShowPhoneFieldError(true, with: Strings.UserForm.Error.blankField)
    }

    func presentInvalidPhoneNumberError() {
        viewController?.shouldShowPhoneFieldError(true, with: Strings.UserForm.Error.invalidField)
    }

    func hidePhoneNumberError() {
        viewController?.shouldShowPhoneFieldError(false, with: nil)
    }

    func shouldPresentNameError(_ shouldPresent: Bool) {
        viewController?.shouldShowNameFieldError(shouldPresent, with: Strings.UserForm.Error.blankField)
    }

    func shouldPresentPhoneModelError(_ shouldPresent: Bool) {
        viewController?.shouldShowModelFieldError(shouldPresent, with: Strings.UserForm.Error.blankField)
    }

    func showFieldsPopupError() {
        let properties = PopupProperties(
            title: Strings.Error.title,
            message: Strings.UserForm.Error.message,
            mainButtonTitle: Strings.General.understood,
            secondaryButtonTitle: nil
        )
        viewController?.showPopupController(with: properties)
    }

    func goToNextStep(_ step: PasswordResetStepData) {
        coordinator.perform(action: .nextStep(step))
    }

    func goBack() {
        coordinator.perform(action: .back)
    }

    func showErrorPopup(with requestError: RequestError?) {
        let properties = PopupProperties(
            title: requestError?.title ?? Strings.Error.title,
            message: requestError?.message ?? Strings.Error.message,
            mainButtonTitle: Strings.General.understood,
            secondaryButtonTitle: nil
        )
        viewController?.showPopupController(with: properties)
    }
}
