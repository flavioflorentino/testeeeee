import Foundation
import UI
import UIKit

protocol BiometricIntroDisplay: AnyObject {
    func displayStepInfo(with image: UIImage, title: String, description: String, buttonTitle: String)
}

final class BiometricIntroViewController: ViewController<BiometricIntroInteracting, UIView> {
    private lazy var infoView: PasswordResetInfoView = {
        let view = PasswordResetInfoView()
        view.delegate = self
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.setupView()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(infoView)
    }
    
    override func setupConstraints() {
        infoView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        let backButton = UIBarButtonItem()
        backButton.title = String()
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
}

extension BiometricIntroViewController: BiometricIntroDisplay {
    func displayStepInfo(with image: UIImage, title: String, description: String, buttonTitle: String) {
        infoView.configureView(with: image, title: title, description: description, buttonTitle: buttonTitle)
    }
}

extension BiometricIntroViewController: PasswordResetInfoViewDelegate {
    func actionButtonTapped() {
        interactor.start()
    }
}
