import Foundation

enum BiometricIntroAction {
    case emailVerification
    case previousStep
}

protocol BiometricIntroCoordinating: AnyObject {
    func perform(action: BiometricIntroAction)
    var delegate: BiometricIntroCoordinatorDelegate? { get set }
}

protocol BiometricIntroCoordinatorDelegate: AnyObject {
    func biometricIntroGoBack()
    func biometricIntroGoNextStep()
}

final class BiometricIntroCoordinator {
    weak var delegate: BiometricIntroCoordinatorDelegate?
}

extension BiometricIntroCoordinator: BiometricIntroCoordinating {
    func perform(action: BiometricIntroAction) {
        switch action {
        case .emailVerification:
            delegate?.biometricIntroGoNextStep()
        case .previousStep:
            delegate?.biometricIntroGoBack()
        }
    }
}
