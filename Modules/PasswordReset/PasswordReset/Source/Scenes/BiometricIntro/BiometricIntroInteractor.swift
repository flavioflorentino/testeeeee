import AnalyticsModule
import Foundation

protocol BiometricIntroInteracting: AnyObject {
    func setupView()
    func start()
    func goBack()
}

final class BiometricIntroInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    let presenter: BiometricIntroPresenting
    
    init(presenter: BiometricIntroPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

extension BiometricIntroInteractor: BiometricIntroInteracting {
    func setupView() {
        let eventTracker = PasswordResetTracker(eventType: FallbackEvent.intro)
        dependencies.analytics.log(eventTracker)
        presenter.configureView()
    }
    
    func start() {
        presenter.presentEmailVerificationStep()
    }
    
    func goBack() {
        presenter.presentPreviousStep()
    }
}
