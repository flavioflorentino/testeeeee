import Foundation
import UI
import UIKit

enum BiometricIntroFactory {
    static func make(coordinatorDelegate: BiometricIntroCoordinatorDelegate) -> BiometricIntroViewController {
        let container = PasswordResetDependencyContainer()
        let coordinator: BiometricIntroCoordinating = BiometricIntroCoordinator()
        let presenter: BiometricIntroPresenting = BiometricIntroPresenter(coordinator: coordinator)
        let interactor: BiometricIntroInteracting = BiometricIntroInteractor(presenter: presenter, dependencies: container)
        let viewController = BiometricIntroViewController(interactor: interactor)
        
        coordinator.delegate = coordinatorDelegate
        presenter.viewController = viewController
        
        return viewController
    }
}
