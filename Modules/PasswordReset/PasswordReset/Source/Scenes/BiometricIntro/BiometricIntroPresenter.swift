import Foundation

protocol BiometricIntroPresenting: AnyObject {
    var viewController: BiometricIntroDisplay? { get set }
    func configureView()
    func presentEmailVerificationStep()
    func presentPreviousStep()
}

final class BiometricIntroPresenter {
    weak var viewController: BiometricIntroDisplay?
    private let coordinator: BiometricIntroCoordinating
    
    init(coordinator: BiometricIntroCoordinating) {
        self.coordinator = coordinator
    }
}

extension BiometricIntroPresenter: BiometricIntroPresenting {
    func configureView() {
        viewController?.displayStepInfo(
            with: Assets.resetSuccess.image,
            title: Strings.BiometricIntro.title,
            description: Strings.BiometricIntro.description,
            buttonTitle: Strings.BiometricIntro.buttonTitle
        )
    }
    
    func presentEmailVerificationStep() {
        coordinator.perform(action: .emailVerification)
    }
    
    func presentPreviousStep() {
        coordinator.perform(action: .previousStep)
    }
}
