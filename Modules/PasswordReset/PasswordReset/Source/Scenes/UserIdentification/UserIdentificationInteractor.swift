import AnalyticsModule
import Core
import Foundation

protocol UserIdentificationInteracting: AnyObject {
    func checkDocument(_ document: String)
    func confirm(_ document: String)
    func close()
    func sendAnalyticsEvent()
}

final class UserIdentificationInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: UserIdentificationServicing
    private let presenter: UserIdentificationPresenting
    
    init(service: UserIdentificationServicing, presenter: UserIdentificationPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - UserIdentificationInteracting
extension UserIdentificationInteractor: UserIdentificationInteracting {
    func checkDocument(_ document: String) {
        let verificator = DocumentVerificator()
        let isCompleteDocument = verificator.isComplete(document, documentType: .cpf)
        let isValid = verificator.validateDocument(document, documentType: .cpf)
        shouldSendInvalidCpfEvent(isCompleteDocument: isCompleteDocument, isValid: isValid)
        presenter.shouldEnableButton(isValid, isCompleteDocument: isCompleteDocument)
    }
    
    func confirm(_ document: String) {
        let documentOnlyNumbers = document.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        presenter.showLoading(true)
        
        service.channels(document: documentOnlyNumbers) { [weak self] response in
            self?.presenter.showLoading(false)
            switch response {
            case let .success(channels):
                self?.trackEvent(UserIdentificationEvent.success)
                self?.receivedResetChannels(channels, for: document)

            case let .failure(error):
                self?.handleErrorTracking(error.requestError?.code)
                self?.handleServiceError(error)
            }
        }
    }
    
    func close() {
        presenter.close()
    }
    
    func sendAnalyticsEvent() {
        trackEvent(UserIdentificationEvent.accessed)
    }
}

private extension UserIdentificationInteractor {
    func receivedResetChannels(_ channels: PasswordResetChannels, for document: String) {
        presenter.presentResetChannels(channels, for: document)
    }
    
    func handleServiceError(_ error: ApiError) {
        presenter.presentError(error.requestError)
    }
    
    func shouldSendInvalidCpfEvent(isCompleteDocument: Bool, isValid: Bool) {
        if isCompleteDocument && !isValid {
            trackEvent(UserIdentificationEvent.invalidCpf)
        }
    }

    func trackEvent(_ event: PasswordResetEvent) {
        let eventTracker = PasswordResetTracker(eventType: event)
        dependencies.analytics.log(eventTracker)
    }

    func handleErrorTracking(_ errorCode: String?) {
        let errorCode = PasswordResetErrorCode(errorCode)
        switch errorCode {
        case .passwordResetBlock:
            trackEvent(ErrorEvent.blocked)
        default:
            trackEvent(UserIdentificationEvent.error)
        }
    }
}
