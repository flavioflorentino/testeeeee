import Core
import Foundation
import UI

protocol UserIdentificationPresenting: AnyObject {
    var viewController: UserIdentificationDisplay? { get set }
    func shouldEnableButton(_ enable: Bool, isCompleteDocument: Bool)
    func showLoading(_ loading: Bool)
    func presentResetChannels(_ channels: PasswordResetChannels, for document: String)
    func presentError(_ error: RequestError?)
    func close()
}

final class UserIdentificationPresenter {
    private let coordinator: UserIdentificationCoordinating
    weak var viewController: UserIdentificationDisplay?
    
    init(coordinator: UserIdentificationCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - UserIdentificationPresenting
extension UserIdentificationPresenter: UserIdentificationPresenting {
    func shouldEnableButton(_ enable: Bool, isCompleteDocument: Bool) {
        let shouldShowError = !enable && isCompleteDocument
        let errorMessage = Strings.Identification.cpfDocumentError
        
        viewController?.displayButtonEnabled(enable, errorMessage: shouldShowError ? errorMessage : nil)
    }
    
    func showLoading(_ loading: Bool) {
        viewController?.displayLoading(loading)
    }
    
    func presentResetChannels(_ channels: PasswordResetChannels, for document: String) {
        coordinator.perform(action: .channels(channels, document: document))
    }
    
    func presentError(_ error: RequestError?) {
        let properties = PopupProperties(
            title: error?.title ?? Strings.Error.title,
            message: error?.message ?? Strings.Error.message,
            mainButtonTitle: Strings.General.understood,
            secondaryButtonTitle: nil
        )
        viewController?.displayPopup(with: properties)
    }
    
    func close() {
        coordinator.perform(action: .close)
    }
}
