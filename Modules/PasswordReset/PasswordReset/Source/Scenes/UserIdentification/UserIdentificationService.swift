import Core
import Foundation

protocol UserIdentificationServicing {
    func channels(document: String, completion: @escaping ((Result<PasswordResetChannels, ApiError>) -> Void))
}

final class UserIdentificationService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - UserIdentificationServicing
extension UserIdentificationService: UserIdentificationServicing {
    func channels(document: String, completion: @escaping ((Result<PasswordResetChannels, ApiError>) -> Void)) {
        let api = Api<PasswordResetChannels>(endpoint: PasswordResetEndpoint.channels(document: document))
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
