import Foundation

enum UserIdentificationFactory {
    static func make(with delegate: UserIdentificationCoordinatorDelegate) -> UserIdentificationViewController {
        let container = PasswordResetDependencyContainer()
        let service: UserIdentificationServicing = UserIdentificationService(dependencies: container)
        let coordinator: UserIdentificationCoordinating = UserIdentificationCoordinator()
        let presenter: UserIdentificationPresenting = UserIdentificationPresenter(coordinator: coordinator)
        let interactor = UserIdentificationInteractor(service: service, presenter: presenter, dependencies: container)
        let viewController = UserIdentificationViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
