import UIKit

enum UserIdentificationAction: Equatable {
    case channels(PasswordResetChannels, document: String)
    case close
}

protocol UserIdentificationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: UserIdentificationAction)
    var delegate: UserIdentificationCoordinatorDelegate? { get set }
}

protocol UserIdentificationCoordinatorDelegate: AnyObject {
    func didReceivedResetChannels(_ channelsData: PasswordResetChannels, for document: String)
    func didClose()
}

final class UserIdentificationCoordinator {
    weak var viewController: UIViewController?
    weak var delegate: UserIdentificationCoordinatorDelegate?
}

// MARK: - UserIdenticationCoordinating
extension UserIdentificationCoordinator: UserIdentificationCoordinating {
    func perform(action: UserIdentificationAction) {
        switch action {
        case .close:
            delegate?.didClose()
        case let .channels(channelsData, document):
            delegate?.didReceivedResetChannels(channelsData, for: document)
        }
    }
}
