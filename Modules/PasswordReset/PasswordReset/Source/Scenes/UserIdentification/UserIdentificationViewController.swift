import AssetsKit
import Core
import UI
import UIKit

protocol UserIdentificationDisplay: AnyObject {
    func displayButtonEnabled(_ enabled: Bool, errorMessage: String?)
    func displayLoading(_ loading: Bool)
    func displayPopup(with popupProperties: PopupProperties)
}

final class UserIdentificationViewController: ViewController<UserIdentificationInteracting, UIView>, LoadingViewProtocol {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
        label.text = Strings.Identification.header
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
        label.text = Strings.Identification.body
        return label
    }()
    
    private lazy var documentTextfield: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.defaultForm()
        textfield.keyboardType = .numberPad
        textfield.placeholder = Strings.Identification.fieldPlaceholder
        textfield.addTarget(self, action: #selector(didTypeNumber(_:)), for: .editingChanged)
        textfield.delegate = self
        return textfield
    }()
    
    private lazy var masker: TextFieldMasker = {
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf)
        let masker = TextFieldMasker(textMask: mask)
        return masker
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.General.confirm, for: .normal)
        button.addTarget(self, action: #selector(didTapConfirm), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        button.isEnabled = false
        return button
    }()
    
    lazy var loadingView = LoadingView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.sendAnalyticsEvent()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(documentTextfield)
        view.addSubview(confirmButton)
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(view.snp.topMargin).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(descriptionLabel.snp.top).offset(-Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(documentTextfield.snp.top).offset(-Spacing.base03)
        }
        
        documentTextfield.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(confirmButton.snp.top).offset(-Spacing.base05)
        }
        
        confirmButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    override func configureViews() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: Resources.Icons.icoClose.image, style: .plain, target: self, action: #selector(didTapClose))
        view.backgroundColor = Colors.backgroundPrimary.color
        masker.bind(to: documentTextfield)
    }
}

@objc
private extension UserIdentificationViewController {
    func didTapConfirm() {
        viewModel.confirm(documentTextfield.text ?? "")
    }
    
    func didTapClose() {
        interactor.close()
    }
    
    func didTypeNumber(_ sender: UITextField) {
        viewModel.checkDocument(documentTextfield.text ?? "")
    }
}

// MARK: UserIdentificationDisplay
extension UserIdentificationViewController: UserIdentificationDisplay {
    func displayButtonEnabled(_ enabled: Bool, errorMessage: String?) {
        confirmButton.isEnabled = enabled
        documentTextfield.errorMessage = errorMessage
    }
    
    func displayLoading(_ loading: Bool) {
        loading ? startLoadingView() : stopLoadingView()
    }
    
    func displayPopup(with popupProperties: PopupProperties) {
        let popupController = PopupViewController(
            title: popupProperties.title,
            description: popupProperties.message,
            preferredType: .text
        )
                
        let okAction = PopupAction(title: popupProperties.mainButtonTitle, style: .fill)
        popupController.addAction(okAction)
        showPopup(popupController)
    }
}

extension UserIdentificationViewController: UITextFieldDelegate {
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isEmpty {
            return true
        }
        
        guard let text = textField.text else {
            return false
        }
        
        return text.count < DocumentVerificator.maskedCpfDocumentLength
    }
}
