import AnalyticsModule
import Foundation

protocol LinkErrorInteracting: AnyObject {
    func loadView()
    func didConfirm()
}

final class LinkErrorInteractor: LinkErrorInteracting {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let presenter: LinkErrorPresenting
    private let errorCode: PasswordResetErrorCode
    private let channelType: ChannelType
    private let title: String?
    private let message: String?

    init(presenter: LinkErrorPresenting,
         errorCode: PasswordResetErrorCode,
         channelType: ChannelType,
         title: String?,
         message: String?,
         dependencies: Dependencies) {
        self.presenter = presenter
        self.errorCode = errorCode
        self.channelType = channelType
        self.title = title
        self.message = message
        self.dependencies = dependencies
    }

    // MARK: - LinkErrorInteracting

    func loadView() {
        trackAppearEvent()
        presenter.displayProperties(title: title, message: message)
    }

    func didConfirm() {
        presenter.didNextStep(action: .done)
    }
}

private extension LinkErrorInteractor {
    func trackAppearEvent() {
        let eventTracker = PasswordResetTracker(method: channelType, eventType: LinkErrorEvent.appear, errorCode: errorCode)
        dependencies.analytics.log(eventTracker)
    }
}
