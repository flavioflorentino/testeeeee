import Foundation

enum LinkErrorFactory {
    static func make(code: PasswordResetErrorCode,
                     channelType: ChannelType,
                     title: String?,
                     message: String?,
                     delegate: LinkErrorCoordinatorDelegate?) -> LinkErrorViewController {
        let container = PasswordResetDependencyContainer()
        let coordinator = LinkErrorCoordinator()
        let presenter = LinkErrorPresenter(coordinator: coordinator)
        let interactor = LinkErrorInteractor(presenter: presenter,
                                             errorCode: code,
                                             channelType: channelType,
                                             title: title,
                                             message: message,
                                             dependencies: container)
        let viewController = LinkErrorViewController(interactor: interactor)

        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
