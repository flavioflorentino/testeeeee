import UI
import UIKit

protocol LinkErrorDisplaying: AnyObject {
    func displayInfo(image: UIImage, title: String, description: String, buttonTitle: String)
}

final class LinkErrorViewController: ViewController<LinkErrorInteracting, UIView>, LinkErrorDisplaying, PasswordResetInfoViewDelegate {
    private lazy var infoView: PasswordResetInfoView = {
        let view = PasswordResetInfoView()
        view.delegate = self
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadView()
    }

    override func buildViewHierarchy() {
        view.addSubview(infoView)
    }

    override func setupConstraints() {
        infoView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }

    // MARK: - LinkErrorDisplaying

    func displayInfo(image: UIImage, title: String, description: String, buttonTitle: String) {
        infoView.configureView(with: image, title: title, description: description, buttonTitle: buttonTitle)
    }

    // MARK: - PasswordResetInfoViewDelegate

    func actionButtonTapped() {
        interactor.didConfirm()
    }
}
