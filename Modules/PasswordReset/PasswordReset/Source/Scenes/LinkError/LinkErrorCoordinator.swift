import Foundation

enum LinkErrorAction {
    case done
}

protocol LinkErrorCoordinatorDelegate: AnyObject {
    func closeLinkError()
}

protocol LinkErrorCoordinating: AnyObject {
    var delegate: LinkErrorCoordinatorDelegate? { get set }
    func perform(action: LinkErrorAction)
}

final class LinkErrorCoordinator: LinkErrorCoordinating {
    weak var delegate: LinkErrorCoordinatorDelegate?

    func perform(action: LinkErrorAction) {
        if case LinkErrorAction.done = action {
            delegate?.closeLinkError()
        }
    }
}
