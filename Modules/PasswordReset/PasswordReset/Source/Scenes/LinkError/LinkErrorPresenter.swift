import AssetsKit
import Foundation

protocol LinkErrorPresenting: AnyObject {
    var viewController: LinkErrorDisplaying? { get set }
    func displayProperties(title: String?, message: String?)
    func didNextStep(action: LinkErrorAction)
}

final class LinkErrorPresenter: LinkErrorPresenting {
    private let coordinator: LinkErrorCoordinating
    weak var viewController: LinkErrorDisplaying?

    init(coordinator: LinkErrorCoordinating) {
        self.coordinator = coordinator
    }

    // MARK: - LinkErrorPresenting

    func displayProperties(title: String?, message: String?) {
        viewController?.displayInfo(image: Resources.Illustrations.iluError.image,
                                    title: title ?? Strings.LinkError.title,
                                    description: message ?? Strings.LinkError.message,
                                    buttonTitle: Strings.General.understood)
    }

    func didNextStep(action: LinkErrorAction) {
        coordinator.perform(action: action)
    }
}
