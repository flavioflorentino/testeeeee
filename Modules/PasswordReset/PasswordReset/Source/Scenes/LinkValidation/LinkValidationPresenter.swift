import Foundation

protocol LinkValidationPresenting: AnyObject {
    var viewController: LinkValidationDisplaying? { get set }
    func startLoading()
    func stopLoading()
    func didNextStep(action: LinkValidationAction)
}

final class LinkValidationPresenter: LinkValidationPresenting {
    private let coordinator: LinkValidationCoordinating
    weak var viewController: LinkValidationDisplaying?

    init(coordinator: LinkValidationCoordinating) {
        self.coordinator = coordinator
    }

    // MARK: - LinkValidationPresenting

    func startLoading() {
        viewController?.startLoadingView()
    }

    func stopLoading() {
        viewController?.stopLoadingView()
    }

    func didNextStep(action: LinkValidationAction) {
        coordinator.perform(action: action)
    }
}
