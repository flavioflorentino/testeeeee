import UIKit

enum LinkValidationFactory {
    static func make(code: String, delegate: LinkValidationCoordinatorDelegate?) -> LinkValidationViewController {
        let container = PasswordResetDependencyContainer()
        let service = LinkValidationService(dependencies: container)
        let coordinator = LinkValidationCoordinator()
        let presenter = LinkValidationPresenter(coordinator: coordinator)
        let interactor = LinkValidationInteractor(code: code, service: service, presenter: presenter)
        let viewController = LinkValidationViewController(interactor: interactor)

        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
