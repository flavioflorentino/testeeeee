import Core
import Foundation

protocol LinkValidationInteracting: AnyObject {
    func validateCode()
    func closeAction()
}

final class LinkValidationInteractor: LinkValidationInteracting {
    private let service: LinkValidationServicing
    private let presenter: LinkValidationPresenting
    private let code: String

    init(code: String, service: LinkValidationServicing, presenter: LinkValidationPresenting) {
        self.code = code
        self.service = service
        self.presenter = presenter
    }

    // MARK: - LinkValidationInteracting

    func validateCode() {
        presenter.startLoading()

        service.validateCode(code: code) { [weak self] result in
            self?.presenter.stopLoading()

            switch result {
            case let .success(stepData):
                self?.presenter.didNextStep(action: .nextStep(stepData))

            case let .failure(apiError):
                let errorCode = PasswordResetErrorCode(apiError.requestError?.code)
                let errorTitle = apiError.requestError?.title
                let errorMessage = apiError.requestError?.message
                self?.presenter.didNextStep(action: .error(code: errorCode, title: errorTitle, message: errorMessage))
            }
        }
    }

    func closeAction() {
        presenter.didNextStep(action: .close)
    }
}
