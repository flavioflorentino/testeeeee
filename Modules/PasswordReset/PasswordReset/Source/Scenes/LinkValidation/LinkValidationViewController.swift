import UI
import UIKit

protocol LinkValidationDisplaying: AnyObject, LoadingViewProtocol {}

final class LinkValidationViewController: ViewController<LinkValidationInteracting, UIView>, LinkValidationDisplaying {
    var loadingView = LoadingView()

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.validateCode()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}
