import Core
import Foundation

protocol LinkValidationServicing {
    func validateCode(code: String, completion: @escaping (Result<PasswordResetStepData, ApiError>) -> Void)
}

final class LinkValidationService: LinkValidationServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    // MARK: - LinkValidationServicing

    func validateCode(code: String, completion: @escaping (Result<PasswordResetStepData, ApiError>) -> Void) {
        let endpoint = PasswordResetEndpoint.validateLink(code: code)
        let api = Api<PasswordResetStepData>(endpoint: endpoint)

        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
