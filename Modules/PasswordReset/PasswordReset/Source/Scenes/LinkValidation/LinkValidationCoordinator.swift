import UIKit

enum LinkValidationAction: Equatable {
    case nextStep(PasswordResetStepData)
    case error(code: PasswordResetErrorCode, title: String?, message: String?)
    case close
}

protocol LinkValidationCoordinatorDelegate: AnyObject {
    func linkValidationNextStep(_ stepData: PasswordResetStepData)
    func linkValidationError(code: PasswordResetErrorCode, title: String?, message: String?)
    func linkValidationClose()
}

protocol LinkValidationCoordinating: AnyObject {
    var delegate: LinkValidationCoordinatorDelegate? { get set }
    func perform(action: LinkValidationAction)
}

final class LinkValidationCoordinator: LinkValidationCoordinating {
    weak var delegate: LinkValidationCoordinatorDelegate?

    // MARK: - LinkValidationCoordinating

    func perform(action: LinkValidationAction) {
        switch action {
        case let .nextStep(stepData):
            delegate?.linkValidationNextStep(stepData)

        case let .error(code, title, message):
            delegate?.linkValidationError(code: code, title: title, message: message)

        case .close:
            delegate?.linkValidationClose()
        }
    }
}
