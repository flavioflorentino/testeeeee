import Foundation

enum StepLoaderAction {
    case nextStep(PasswordResetStepData)
    case back
}

protocol StepLoaderCoordinatorDelegate: AnyObject {
    func stepLoaderNextStep(_ stepData: PasswordResetStepData)
    func backFromStepLoader()
}

protocol StepLoaderCoordinating {
    var delegate: StepLoaderCoordinatorDelegate? { get set }
    func perform(action: StepLoaderAction)
}

final class StepLoaderCoordinator: StepLoaderCoordinating {
    weak var delegate: StepLoaderCoordinatorDelegate?

    func perform(action: StepLoaderAction) {
        switch action {
        case .nextStep(let data):
            delegate?.stepLoaderNextStep(data)
        case .back:
            delegate?.backFromStepLoader()
        }
    }
}
