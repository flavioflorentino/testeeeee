import Core
import Foundation

protocol StepLoaderServicing {
    func getNextStep(resetId: String, completion: @escaping (Result<PasswordResetStepData, ApiError>) -> Void)
}

final class StepLoaderService: StepLoaderServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    // MARK: - StepLoaderServicing

    func getNextStep(resetId: String, completion: @escaping (Result<PasswordResetStepData, ApiError>) -> Void) {
        let api = Api<PasswordResetStepData>(endpoint: PasswordResetEndpoint.loadStep(resetId: resetId))

        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
