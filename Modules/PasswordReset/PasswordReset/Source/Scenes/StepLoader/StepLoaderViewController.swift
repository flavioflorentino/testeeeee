import UIKit
import UI

protocol StepLoaderDisplay: AnyObject, LoadingViewProtocol {
    func displayInfo(with image: UIImage, title: String, description: String, buttonTitle: String)
    func showPopupController(with properties: PopupProperties)
}

private extension StepLoaderViewController.Layout {
    enum Size {
        static let backButton: CGFloat = 28
    }
}

final class StepLoaderViewController: ViewController<StepLoaderInteracting, PasswordResetInfoView>, StepLoaderDisplay {
    fileprivate enum Layout { }

    var loadingView = LoadingView()

    private lazy var backButton: UIButton = {
        let backButton = UIButton()
        backButton.setImage(Assets.backGreenArrow.image, for: .normal)
        backButton.addTarget(self, action: #selector(tapBackButton), for: .touchUpInside)
        return backButton
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.setupView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func buildViewHierarchy() {
        view.addSubview(backButton)
    }

    override func setupConstraints() {
        backButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base00)
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.size.equalTo(Layout.Size.backButton)
        }
    }

    override func configureViews() {
        rootView.delegate = self
    }

    // MARK: - StepLoaderDisplay

    func displayInfo(with image: UIImage, title: String, description: String, buttonTitle: String) {
        rootView.configureView(with: image, title: title, description: description, buttonTitle: buttonTitle)
    }

    func showPopupController(with properties: PopupProperties) {
        let popupController = PopupViewController(
            title: properties.title,
            description: properties.message,
            preferredType: .text
        )

        let okAction = PopupAction(title: properties.mainButtonTitle, style: .fill)
        popupController.addAction(okAction)

        showPopup(popupController)
    }
}

extension StepLoaderViewController: PasswordResetInfoViewDelegate {
    func actionButtonTapped() {
        interactor.continueButtonAction()
    }
}

private extension StepLoaderViewController {
    @objc
    func tapBackButton() {
        interactor.backButtonAction()
    }
}
