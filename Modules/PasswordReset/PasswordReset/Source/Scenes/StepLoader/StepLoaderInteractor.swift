import AnalyticsModule
import Foundation

protocol StepLoaderInteracting {
    func setupView()
    func continueButtonAction()
    func backButtonAction()
}

final class StepLoaderInteractor: StepLoaderInteracting {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let presenter: StepLoaderPresenting
    private let service: StepLoaderServicing
    private let resetId: String

    init(presenter: StepLoaderPresenting, service: StepLoaderServicing, resetId: String, dependencies: Dependencies) {
        self.presenter = presenter
        self.service = service
        self.resetId = resetId
        self.dependencies = dependencies
    }

    // MARK: - StepLoaderInteracting

    func setupView() {
        let tracker = PasswordResetTracker(method: .biometric, eventType: StepLoaderEvent.appear)
        dependencies.analytics.log(tracker)
        presenter.setupView()
    }

    func continueButtonAction() {
        presenter.startLoading()

        service.getNextStep(resetId: resetId) { [weak self] result in
            self?.presenter.stopLoading()

            switch result {
            case .success(let data):
                self?.presenter.goToNextStep(data)
            case .failure(let apiError):
                self?.presenter.showError(apiError.requestError)
            }
        }
    }

    func backButtonAction() {
        presenter.goBack()
    }
}
