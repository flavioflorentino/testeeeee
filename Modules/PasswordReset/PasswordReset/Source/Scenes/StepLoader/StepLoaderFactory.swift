import Foundation

enum StepLoaderFactory {
    static func make(resetId: String, coordinatorDelegate: StepLoaderCoordinatorDelegate?) -> StepLoaderViewController {
        let container = PasswordResetDependencyContainer()
        let service = StepLoaderService(dependencies: container)
        let coordinator = StepLoaderCoordinator()
        let presenter = StepLoaderPresenter(coordinator: coordinator)
        let interactor = StepLoaderInteractor(presenter: presenter, service: service, resetId: resetId, dependencies: container)
        let viewController = StepLoaderViewController(interactor: interactor)

        presenter.viewController = viewController
        coordinator.delegate = coordinatorDelegate

        return viewController
    }
}
