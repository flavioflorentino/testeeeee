import AssetsKit
import Core
import Foundation

protocol StepLoaderPresenting {
    var viewController: StepLoaderDisplay? { get set }
    func setupView()
    func startLoading()
    func stopLoading()
    func goToNextStep(_ stepData: PasswordResetStepData)
    func goBack()
    func showError(_ error: RequestError?)
}

final class StepLoaderPresenter: StepLoaderPresenting {
    private let coordinator: StepLoaderCoordinating

    weak var viewController: StepLoaderDisplay?

    init(coordinator: StepLoaderCoordinating) {
        self.coordinator = coordinator
    }

    // MARK: - StepLoaderPresenting

    func setupView() {
        viewController?.displayInfo(
            with: Resources.Illustrations.iluVictory.image,
            title: Strings.StepLoader.title,
            description: Strings.StepLoader.description,
            buttonTitle: Strings.General.continue)
    }

    func startLoading() {
        viewController?.startLoadingView()
    }

    func stopLoading() {
        viewController?.stopLoadingView()
    }

    func goToNextStep(_ stepData: PasswordResetStepData) {
        coordinator.perform(action: .nextStep(stepData))
    }

    func goBack() {
        coordinator.perform(action: .back)
    }

    func showError(_ error: RequestError?) {
        let properties = PopupProperties(
            title: error?.title ?? Strings.Error.title,
            message: error?.message ?? Strings.Error.message,
            mainButtonTitle: Strings.General.understood,
            secondaryButtonTitle: nil
        )
        viewController?.showPopupController(with: properties)
    }
}
