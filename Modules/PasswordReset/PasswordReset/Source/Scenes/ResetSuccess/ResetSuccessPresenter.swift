import Foundation

protocol ResetSuccessPresenting: AnyObject {
    func didNextStep(action: ResetSuccessAction)
}

final class ResetSuccessPresenter {
    private let coordinator: ResetSuccessCoordinating

    init(coordinator: ResetSuccessCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ResetPasswordSuccessPresenting
extension ResetSuccessPresenter: ResetSuccessPresenting {
    func didNextStep(action: ResetSuccessAction) {
        coordinator.perform(action: action)
    }
}
