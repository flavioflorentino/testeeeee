import UIKit

enum ResetSuccessAction {
    case enter
    case close
}

protocol ResetSuccessCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: ResetSuccessCoordinatorDelegate? { get set }
    func perform(action: ResetSuccessAction)
}

protocol ResetSuccessCoordinatorDelegate: AnyObject {
    func didTapActionButton()
    func didTapCloseButton()
}

final class ResetSuccessCoordinator {
    weak var viewController: UIViewController?
    weak var delegate: ResetSuccessCoordinatorDelegate?
}

// MARK: - ResetPasswordSuccessCoordinating
extension ResetSuccessCoordinator: ResetSuccessCoordinating {
    func perform(action: ResetSuccessAction) {
        viewController?.navigationController?.setNavigationBarHidden(false, animated: false)
        switch action {
        case .enter:
            delegate?.didTapActionButton()
        case .close:
            delegate?.didTapCloseButton()
        }
    }
}
