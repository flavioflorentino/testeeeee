import Foundation

enum ResetSuccessFactory {
    static func make(method: ChannelType, coordinatorDelegate: ResetSuccessCoordinatorDelegate) -> ResetSuccessViewController {
        let container = PasswordResetDependencyContainer()
        let coordinator: ResetSuccessCoordinating = ResetSuccessCoordinator()
        let presenter: ResetSuccessPresenting = ResetSuccessPresenter(coordinator: coordinator)
        let interactor = ResetSuccessInteractor(method: method, presenter: presenter, dependencies: container)
        let viewController = ResetSuccessViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        
        return viewController
    }
}
