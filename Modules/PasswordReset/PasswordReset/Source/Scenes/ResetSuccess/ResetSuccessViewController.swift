import AssetsKit
import UI
import UIKit

final class ResetSuccessViewController: ViewController<ResetSuccessInteracting, UIView> {
    private lazy var closeButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(Resources.Icons.icoClose.image, for: .normal)
        button.addTarget(self, action: #selector(didTapActionButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var centerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = Spacing.base03
        return stackView
    }()
    
    private lazy var successImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = Assets.resetSuccess.image
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, .black())
            .with(\.textAlignment, .center)
        label.text = Strings.ResetPasswordSuccess.title
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        label.text = Strings.ResetPasswordSuccess.description
        return label
    }()
    
    private lazy var actionButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Strings.ResetPasswordSuccess.enter, for: .normal)
        button.addTarget(self, action: #selector(didTapActionButton), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.sendAnalytics()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func buildViewHierarchy() {
        view.addSubview(closeButton)
        view.addSubview(centerStackView)
        centerStackView.addArrangedSubview(successImageView)
        centerStackView.addArrangedSubview(titleLabel)
        centerStackView.addArrangedSubview((descriptionLabel))
        centerStackView.addArrangedSubview(actionButton)
    }
    
    override func setupConstraints() {
        closeButton.snp.makeConstraints {
            $0.top.equalTo(view.snp.topMargin).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        centerStackView.snp.makeConstraints {
            $0.centerX.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.change(.dark, to: Colors.backgroundSecondary.darkColor).color
    }
}

@objc
private extension ResetSuccessViewController {
    func didTapActionButton() {
        interactor.enter()
    }
    
    func didTapCloseButton() {
        interactor.close()
    }
}
