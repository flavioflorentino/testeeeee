import AnalyticsModule
import Foundation

protocol ResetSuccessInteracting: AnyObject {
    func sendAnalytics()
    func enter()
    func close()
}

final class ResetSuccessInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let presenter: ResetSuccessPresenting
    private let method: ChannelType

    init(method: ChannelType, presenter: ResetSuccessPresenting, dependencies: Dependencies) {
        self.method = method
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - ResetPasswordSuccessInteracting
extension ResetSuccessInteractor: ResetSuccessInteracting {
    func sendAnalytics() {
        let tracker = PasswordResetTracker(method: method, eventType: ResetSuccessEvent.appear)
        dependencies.analytics.log(tracker)
    }
    
    func enter() {
        presenter.didNextStep(action: .enter)
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
}
