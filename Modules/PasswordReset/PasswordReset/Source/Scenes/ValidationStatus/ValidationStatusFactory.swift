import UIKit

enum ValidationStatusFactory {
    static func make(
        with step: PasswordResetStep,
        coordinatorDelegate: ValidationStatusCoordinatorDelegate
    ) -> ValidationStatusViewController {
        let container = PasswordResetDependencyContainer()
        let coordinator: ValidationStatusCoordinating = ValidationStatusCoordinator()
        let presenter: ValidationStatusPresenting = ValidationStatusPresenter(coordinator: coordinator)
        let interactor = ValidationStatusInteractor(presenter: presenter, dependencies: container, step: step)
        let viewController = ValidationStatusViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        presenter.viewController = viewController

        return viewController
    }
}
