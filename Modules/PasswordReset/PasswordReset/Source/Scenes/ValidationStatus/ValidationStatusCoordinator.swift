import UIKit

enum ValidationStatusAction {
    case close
}

protocol ValidationStatusCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ValidationStatusAction)
    var delegate: ValidationStatusCoordinatorDelegate? { get set }
}

protocol ValidationStatusCoordinatorDelegate: AnyObject {
    func validationStatusDidClose()
}

final class ValidationStatusCoordinator {
    weak var viewController: UIViewController?
    weak var delegate: ValidationStatusCoordinatorDelegate?
}

// MARK: - ValidationStatusCoordinating
extension ValidationStatusCoordinator: ValidationStatusCoordinating {
    func perform(action: ValidationStatusAction) {
        if case .close = action {
            delegate?.validationStatusDidClose()
        }
    }
}
