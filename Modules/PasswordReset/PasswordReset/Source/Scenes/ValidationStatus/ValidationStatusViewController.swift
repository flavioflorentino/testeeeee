import UI
import UIKit

protocol ValidationStatusDisplaying: AnyObject {
    func displayStepInfo(with image: UIImage, title: String, description: String, buttonTitle: String)
}

final class ValidationStatusViewController: ViewController<ValidationStatusInteracting, UIView> {
    private lazy var infoView: PasswordResetInfoView = {
        let view = PasswordResetInfoView()
        view.delegate = self
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(infoView)
    }
    
    override func setupConstraints() {
        infoView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - ValidationStatusDisplaying
extension ValidationStatusViewController: ValidationStatusDisplaying {
    func displayStepInfo(with image: UIImage, title: String, description: String, buttonTitle: String) {
        infoView.configureView(with: image, title: title, description: description, buttonTitle: buttonTitle)
    }
}

extension ValidationStatusViewController: PasswordResetInfoViewDelegate {
    func actionButtonTapped() {
        interactor.understood()
    }
}
