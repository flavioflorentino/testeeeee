import AnalyticsModule
import Foundation

protocol ValidationStatusInteracting: AnyObject {
    func setupView()
    func understood()
}

final class ValidationStatusInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let presenter: ValidationStatusPresenting
    private let step: PasswordResetStep

    init(presenter: ValidationStatusPresenting, dependencies: Dependencies, step: PasswordResetStep) {
        self.presenter = presenter
        self.dependencies = dependencies
        self.step = step
    }
}

// MARK: - ValidationStatusInteracting
extension ValidationStatusInteractor: ValidationStatusInteracting {
    func setupView() {
        sendAnalytics()
        presenter.configureView(for: step)
    }
    
    func understood() {
        presenter.close()
    }
    
    private func sendAnalytics() {
        switch step {
        case .notApproved:
            let tracker = PasswordResetTracker(eventType: BiometricEvent.rejected)
            dependencies.analytics.log(tracker)
        case .manualReview:
            let tracker = PasswordResetTracker(eventType: WaitingAnalysisEvent.formSuccess)
            dependencies.analytics.log(tracker)
        default:
            break
        }
    }
}
