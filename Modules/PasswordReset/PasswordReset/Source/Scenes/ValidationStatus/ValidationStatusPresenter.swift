import AssetsKit
import Foundation

protocol ValidationStatusPresenting: AnyObject {
    var viewController: ValidationStatusDisplaying? { get set }
    func configureView(for step: PasswordResetStep)
    func close()
}

final class ValidationStatusPresenter {
    private let coordinator: ValidationStatusCoordinating
    weak var viewController: ValidationStatusDisplaying?

    init(coordinator: ValidationStatusCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ValidationStatusPresenting
extension ValidationStatusPresenter: ValidationStatusPresenting {
    func configureView(for step: PasswordResetStep) {
        switch step {
        case .notApproved:
            configureRejectedView()
        case .manualReview:
            configureWaitingAnalysisView()
        default:
            break
        }
    }
    
    func close() {
        coordinator.perform(action: .close)
    }
    
    private func configureRejectedView() {
        viewController?.displayStepInfo(
            with: Resources.Illustrations.iluRegisterDenied.image,
            title: Strings.ValidationRejected.title,
            description: Strings.ValidationRejected.description,
            buttonTitle: Strings.General.understood
        )
    }
    
    private func configureWaitingAnalysisView() {
        viewController?.displayStepInfo(
          with: Resources.Illustrations.iluClock.image,
          title: Strings.WaitingAnalysis.title,
          description: Strings.WaitingAnalysis.description,
          buttonTitle: Strings.General.understood
        )
    }
}
