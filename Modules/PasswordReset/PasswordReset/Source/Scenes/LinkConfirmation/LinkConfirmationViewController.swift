import UI
import UIKit

protocol LinkConfirmationDisplaying: AnyObject, LoadingViewProtocol {
    func displayInfo(image: UIImage, title: String, description: String, buttonTitle: String)
    func showErrorPopupController(with properties: PopupProperties)
    func showRetryPopupController(with properties: PopupProperties)
}

final class LinkConfirmationViewController: ViewController<LinkConfirmationInteracting, UIView>, LinkConfirmationDisplaying, PasswordResetInfoViewDelegate {
    var loadingView = LoadingView()

    private lazy var infoView: PasswordResetInfoView = {
        let view = PasswordResetInfoView()
        view.delegate = self
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadView()
        interactor.requestLink()
    }

    override func buildViewHierarchy() {
        view.addSubview(infoView)
    }

    override func setupConstraints() {
        infoView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        let backButton = UIBarButtonItem()
        backButton.title = String()
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }

    // MARK: - LinkConfirmationDisplaying

    func displayInfo(image: UIImage, title: String, description: String, buttonTitle: String) {
        infoView.configureView(with: image, title: title, description: description, buttonTitle: buttonTitle)
    }

    func showErrorPopupController(with properties: PopupProperties) {
        let popupController = createPopupController(properties: properties)

        let okAction = PopupAction(title: properties.mainButtonTitle, style: .fill) { [weak self, weak popupController] in
            popupController?.dismiss(animated: true) {
                self?.interactor.goBackAction()
            }
        }
        popupController.addAction(okAction)
        showPopup(popupController)
    }

    func showRetryPopupController(with properties: PopupProperties) {
        let popupController = createPopupController(properties: properties)

        let tryAgainAction = PopupAction(title: properties.mainButtonTitle, style: .fill) { [weak self, weak popupController] in
            popupController?.dismiss(animated: true) {
                self?.interactor.requestLink()
            }
        }
        popupController.addAction(tryAgainAction)

        let secondaryTitle = properties.secondaryButtonTitle ?? Strings.General.back
        let backAction = PopupAction(title: secondaryTitle, style: .link) { [weak self, weak popupController] in
            popupController?.dismiss(animated: true) {
                self?.interactor.goBackAction()
            }
        }
        popupController.addAction(backAction)

        showPopup(popupController)
    }

    // MARK: - PasswordResetInfoViewDelegate

    func actionButtonTapped() {
        interactor.doneAction()
    }
}

private extension LinkConfirmationViewController {
    func createPopupController(properties: PopupProperties) -> PopupViewController {
        let popupController = PopupViewController(
            title: properties.title,
            description: properties.message,
            preferredType: .text
        )

        return popupController
    }
}
