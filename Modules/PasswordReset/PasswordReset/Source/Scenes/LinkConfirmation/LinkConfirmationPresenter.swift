import AssetsKit
import Foundation

protocol LinkConfirmationPresenting: AnyObject {
    var viewController: LinkConfirmationDisplaying? { get set }
    func configureView(with email: String)
    func startLoading()
    func stopLoading()
    func presentErrorWith(retryButton: Bool, title: String?, message: String?)
    func goBack()
    func endResetFlow()
}

final class LinkConfirmationPresenter: LinkConfirmationPresenting {
    private let coordinator: LinkConfirmationCoordinating
    weak var viewController: LinkConfirmationDisplaying?

    init(coordinator: LinkConfirmationCoordinating) {
        self.coordinator = coordinator
    }

    // MARK: - LinkConfirmationPresenting

    func configureView(with email: String) {
        viewController?.displayInfo(image: Resources.Illustrations.iluEmail.image,
                                    title: Strings.LinkConfirmation.title,
                                    description: Strings.LinkConfirmation.description(email),
                                    buttonTitle: Strings.General.understood)
    }

    func startLoading() {
        viewController?.startLoadingView()
    }

    func stopLoading() {
        viewController?.stopLoadingView()
    }

    func presentErrorWith(retryButton: Bool, title: String?, message: String?) {
        let properties = createPopupProperties(title: title, message: message, isRetry: retryButton)

        if retryButton {
            viewController?.showRetryPopupController(with: properties)
        } else {
            viewController?.showErrorPopupController(with: properties)
        }
    }

    func goBack() {
        coordinator.perform(action: .back)
    }

    func endResetFlow() {
        coordinator.perform(action: .done)
    }
}

private extension LinkConfirmationPresenter {
    func createPopupProperties(title: String?, message: String?, isRetry: Bool) -> PopupProperties {
        let mainButtonTitle = isRetry ? Strings.General.tryAgain : Strings.General.understood
        let secondaryButtonTitle = isRetry ? Strings.General.back : nil

        return PopupProperties(
            title: title ?? Strings.Error.title,
            message: message ?? Strings.Error.message,
            mainButtonTitle: mainButtonTitle,
            secondaryButtonTitle: secondaryButtonTitle
        )
    }
}
