import Core
import Foundation

protocol LinkConfirmationServicing {
    func requestLink(resetId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void)
}

final class LinkConfirmationService: LinkConfirmationServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    // MARK: - LinkConfirmationServicing

    func requestLink(resetId: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let api = Api<NoContent>(endpoint: PasswordResetEndpoint.requestLink(resetId: resetId))

        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
