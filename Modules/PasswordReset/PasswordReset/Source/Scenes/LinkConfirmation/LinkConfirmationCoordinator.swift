import Foundation

enum LinkConfirmationAction {
    case back
    case done
}

protocol LinkConfirmationCoordinatorDelegate: AnyObject {
    func backFromLinkConfirmation()
    func linkConfirmationDone()
}

protocol LinkConfirmationCoordinating: AnyObject {
    var delegate: LinkConfirmationCoordinatorDelegate? { get set }
    func perform(action: LinkConfirmationAction)
}

final class LinkConfirmationCoordinator: LinkConfirmationCoordinating {
    weak var delegate: LinkConfirmationCoordinatorDelegate?

    func perform(action: LinkConfirmationAction) {
        switch action {
        case .back:
            delegate?.backFromLinkConfirmation()
        case .done:
            delegate?.linkConfirmationDone()
        }
    }
}
