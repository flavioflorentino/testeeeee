import AnalyticsModule
import Core
import Foundation

protocol LinkConfirmationInteracting: AnyObject {
    func loadView()
    func requestLink()
    func goBackAction()
    func doneAction()
}

final class LinkConfirmationInteractor: LinkConfirmationInteracting {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: LinkConfirmationServicing
    private let presenter: LinkConfirmationPresenting
    private let channelType: ChannelType
    private let resetId: String
    private let emailString: String

    init(service: LinkConfirmationServicing,
         presenter: LinkConfirmationPresenting,
         channelType: ChannelType,
         resetId: String,
         emailString: String,
         dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.channelType = channelType
        self.resetId = resetId
        self.emailString = emailString
        self.dependencies = dependencies
    }

    // MARK: - LinkConfirmationInteracting

    func loadView() {
        trackEvent(.appear)
        presenter.configureView(with: emailString)
    }

    func requestLink() {
        presenter.startLoading()

        service.requestLink(resetId: resetId) { [weak self] result in
            self?.presenter.stopLoading()

            if case let Result.failure(apiError) = result {
                self?.handleError(apiError.requestError)
            }
        }
    }

    func goBackAction() {
        presenter.goBack()
    }

    func doneAction() {
        presenter.endResetFlow()
    }
}

private extension LinkConfirmationInteractor {
    func handleError(_ requestError: RequestError?) {
        let errorCode = PasswordResetErrorCode(requestError?.code)
        let shouldHaveRetryButton = errorCode != .passwordLinkRequestBlock

        trackErrorEvent(for: errorCode)
        presenter.presentErrorWith(retryButton: shouldHaveRetryButton,
                                   title: requestError?.title,
                                   message: requestError?.message)
    }

    func trackEvent(_ event: LinkConfirmationEvent) {
        let eventTracker = PasswordResetTracker(method: channelType, eventType: event)
        dependencies.analytics.log(eventTracker)
    }

    func trackErrorEvent(for code: PasswordResetErrorCode) {
        if code == .passwordLinkRequestBlock {
            trackEvent(.blockedError)
        } else {
            trackEvent(.genericError)
        }
    }
}
