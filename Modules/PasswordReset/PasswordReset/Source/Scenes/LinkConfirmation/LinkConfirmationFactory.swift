import Foundation

enum LinkConfirmationFactory {
    static func make(channelType: ChannelType,
                     resetId: String,
                     email: String,
                     coordinatorDelegate: LinkConfirmationCoordinatorDelegate?) -> LinkConfirmationViewController {
        let container = PasswordResetDependencyContainer()
        let service = LinkConfirmationService(dependencies: container)
        let coordinator = LinkConfirmationCoordinator()
        let presenter = LinkConfirmationPresenter(coordinator: coordinator)
        let interactor = LinkConfirmationInteractor(service: service,
                                                    presenter: presenter,
                                                    channelType: channelType,
                                                    resetId: resetId,
                                                    emailString: email,
                                                    dependencies: container)
        let viewController = LinkConfirmationViewController(interactor: interactor)

        coordinator.delegate = coordinatorDelegate
        presenter.viewController = viewController

        return viewController
    }
}
