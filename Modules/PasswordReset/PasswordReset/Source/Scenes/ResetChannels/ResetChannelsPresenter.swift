import Core
import Foundation
import UI

protocol ResetChannelsPresenting: AnyObject {
    var viewController: ResetChannelsDisplay? { get set }
    func presentChannelsData(_ channelsData: PasswordResetChannels)
    func presentFallback()
    func showLoading(_ loading: Bool)
    func presentNextStep(_ stepData: PasswordResetStepData, channel: Channel)
    func presentError(_ error: RequestError?)
}

final class ResetChannelsPresenter {
    private let coordinator: ResetChannelsCoordinating
    weak var viewController: ResetChannelsDisplay?
    
    init(coordinator: ResetChannelsCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ResetChannelsPresenting
extension ResetChannelsPresenter: ResetChannelsPresenting {
    func presentChannelsData(_ channelsData: PasswordResetChannels) {
        viewController?.displayChannelViews(with: channelsData.channels)
    }
    
    func presentFallback() {
        viewController?.displayFallback()
    }
    
    func showLoading(_ loading: Bool) {
        viewController?.displayLoading(loading)
    }
    
    func presentNextStep(_ stepData: PasswordResetStepData, channel: Channel) {
        coordinator.perform(action: .channelNextStep(stepData: stepData, channel: channel))
    }
    
    func presentError(_ error: RequestError?) {        
        let properties = PopupProperties(
            title: error?.title ?? Strings.Error.title,
            message: error?.message ?? Strings.Error.message,
            mainButtonTitle: Strings.General.understood,
            secondaryButtonTitle: nil
        )
        viewController?.displayPopup(with: properties)
    }
}
