import AnalyticsModule
import Core
import FeatureFlag
import Foundation

protocol ResetChannelsInteracting: AnyObject {
    func configureChannels()
    func selectChannel(_ channel: Channel)
    func selectFallback()
}

final class ResetChannelsInteractor {
    typealias Dependencies = HasAnalytics & HasFeatureManager & HasAuthManager
    private let dependencies: Dependencies
    
    private let service: ResetChannelsServicing
    private let presenter: ResetChannelsPresenting
    private let channelsData: PasswordResetChannels
    private let document: String

    private var shouldHideFallback: Bool {
        dependencies.authenticationManager?.isAuthenticated ?? false
    }

    init(
        service: ResetChannelsServicing,
        presenter: ResetChannelsPresenting,
        channelsData: PasswordResetChannels,
        document: String,
        dependencies: Dependencies
    ) {
        self.service = service
        self.presenter = presenter
        self.channelsData = channelsData
        self.document = document
        self.dependencies = dependencies
    }
}

// MARK: - ResetChannelsInteracting
extension ResetChannelsInteractor: ResetChannelsInteracting {
    func configureChannels() {
        presenter.presentChannelsData(channelsToPresent())
        shouldPresentFallbackOption()
    }
    
    func selectChannel(_ channel: Channel) {
        trackEvent(channelType: channel.channelType, event: ResetChannelsEvent.selectChannel)
        startResetFlow(with: channel, document: document)
    }
    
    func selectFallback() {
        guard let fallback = channelsData.fallback else {
            presenter.presentError(nil)
            return
        }
        
        trackEvent(channelType: fallback.channelType, event: ResetChannelsEvent.selectChannel)
        startResetFlow(with: fallback, document: document)
    }
}

private extension ResetChannelsInteractor {
    func channelsToPresent() -> PasswordResetChannels {
        PasswordResetChannels(channels: channelsList(),
                              fallback: channelsData.fallback)
    }

    func channelsList() -> [Channel] {
        var channels = channelsData.channels

        if !dependencies.featureManager.isActive(.isPasswordResetEmailAvailable) {
            channels = channels.filter {
                $0.channelType != .email
            }
        }

        if shouldHideFallback {
            channels = channels.filter {
                $0.channelType != .biometric
            }
        }

        return channels
    }

    func shouldPresentFallbackOption() {
        if channelsData.fallback != nil && !shouldHideFallback {
            presenter.presentFallback()
        }
    }

    func startResetFlow(with channel: Channel, document: String) {
        let documentOnlyNumbers = document.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        presenter.showLoading(true)
        
        service.startResetFlow(with: channel.channelType, cpf: documentOnlyNumbers) { [weak self] result in
            self?.presenter.showLoading(false)
            
            switch result {
            case let .success(stepData):
                self?.presenter.presentNextStep(stepData, channel: channel)

            case let .failure(error):
                self?.handleErrorTracking(error.requestError?.code, channelType: channel.channelType)
                self?.handleServiceError(error)
            }
        }
    }
    
    func handleServiceError(_ error: ApiError) {
        presenter.presentError(error.requestError)
    }

    func trackEvent(channelType: ChannelType, event: PasswordResetEvent) {
        let eventTracker = PasswordResetTracker(method: channelType, eventType: event)
        dependencies.analytics.log(eventTracker)
    }

    func handleErrorTracking(_ errorCode: String?, channelType: ChannelType) {
        let errorCode = PasswordResetErrorCode(errorCode)
        switch errorCode {
        case .passwordResetBlock:
            trackEvent(channelType: channelType, event: ErrorEvent.blocked)
        default:
            trackEvent(channelType: channelType, event: ErrorEvent.generic)
        }
    }
}
