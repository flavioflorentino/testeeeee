import UIKit

enum ResetChannelsAction: Equatable {
    case channelNextStep(stepData: PasswordResetStepData, channel: Channel)
}

protocol ResetChannelsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ResetChannelsAction)
    var delegate: ResetChannelsCoordinatorDelegate? { get set }
}

protocol ResetChannelsCoordinatorDelegate: AnyObject {
    func didReceiveChannelNextStep(_ stepData: PasswordResetStepData, channel: Channel)
}

final class ResetChannelsCoordinator {
    weak var viewController: UIViewController?
    weak var delegate: ResetChannelsCoordinatorDelegate?
}

// MARK: - ResetChannelsCoordinating
extension ResetChannelsCoordinator: ResetChannelsCoordinating {
    func perform(action: ResetChannelsAction) {
        switch action {
        case let .channelNextStep(stepData, channel):
            delegate?.didReceiveChannelNextStep(stepData, channel: channel)
        }
    }
}
