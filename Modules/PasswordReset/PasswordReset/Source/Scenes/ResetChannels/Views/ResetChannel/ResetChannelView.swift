import Foundation
import UI
import UIKit

protocol ResetChannelViewDisplay: AnyObject {
    func configureView(with channelImageURL: URL?, title: String, info: String, placeholderImage: UIImage)
    func displaySelectedChannel(_ channel: Channel)
}

protocol ResetChannelViewDelegate: AnyObject {
    func didSelectChannel(_ channel: Channel)
}

private extension ResetChannelView.Layout {
    enum Size {
        static let imageSize: CGFloat = 48
        static let separatorViewHeight: CGFloat = 1
    }
}

final class ResetChannelView: UIView {
    fileprivate enum Layout { }
    
    private let interactor: ResetChannelViewInteracting
    weak var delegate: ResetChannelViewDelegate?
    
    private lazy var channelImageView = UIImageView()
    
    private lazy var channelTitleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
        return label
    }()
    
    private lazy var channelInfoLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
        return label
    }()
    
    private lazy var channelDisclosureIndicatorView: UIImageView = {
        let imageView = UIImageView(image: Assets.disclosure.image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var bottomSeparatorView: UIView = {
        let view = UIView()
        view.viewStyle(BackgroundViewStyle(color: .grayscale200(.default)))
        return view
    }()
    
    private lazy var actionGesture = UITapGestureRecognizer(target: self, action: #selector(didTapView(_:)))
    
    init(interactor: ResetChannelViewInteracting) {
        self.interactor = interactor
        super.init(frame: .zero)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        addSubview(channelImageView)
        addSubview(channelTitleLabel)
        addSubview(channelInfoLabel)
        addSubview(channelDisclosureIndicatorView)
        addSubview(bottomSeparatorView)
    }
    
    private func layoutComponents() {
        channelImageView.snp.makeConstraints {
            $0.width.height.equalTo(Layout.Size.imageSize)
            $0.leading.equalToSuperview()
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalTo(channelTitleLabel.snp.leading).offset(-Spacing.base01)
            $0.bottom.equalTo(bottomSeparatorView.snp.top).offset(-Spacing.base02)
        }
        
        channelTitleLabel.snp.makeConstraints {
            $0.top.equalTo(channelImageView.snp.top).offset(Spacing.base00)
            $0.trailing.equalTo(channelDisclosureIndicatorView).offset(-Spacing.base00)
            $0.bottom.equalTo(channelInfoLabel.snp.top)
        }
    
        channelInfoLabel.snp.makeConstraints {
            $0.leading.equalTo(channelTitleLabel)
            $0.trailing.equalTo(channelDisclosureIndicatorView).offset(-Spacing.base00)
            $0.bottom.equalToSuperview().offset(-Spacing.base03)
        }
        
        channelDisclosureIndicatorView.snp.makeConstraints {
            $0.trailing.centerY.equalToSuperview()
        }
        
        bottomSeparatorView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.separatorViewHeight)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    private func setup() {
        backgroundColor = Colors.backgroundPrimary.change(.dark, to: Colors.backgroundSecondary.darkColor).color
        addComponents()
        layoutComponents()
        addGestureRecognizer(actionGesture)
    }
    
    @objc
    private func didTapView(_ gesture: UIGestureRecognizer) {
        interactor.didSelect()
    }
}

extension ResetChannelView: ResetChannelViewDisplay {
    func configureView(with channelImageURL: URL?, title: String, info: String, placeholderImage: UIImage) {
        channelTitleLabel.text = title
        channelInfoLabel.text = info
        channelImageView.setImage(url: channelImageURL, placeholder: placeholderImage)
    }
    
    func displaySelectedChannel(_ channel: Channel) {
        delegate?.didSelectChannel(channel)
    }
}
