import Foundation
import UI
import UIKit

protocol ResetChannelViewPresenting: AnyObject {
    var viewController: ResetChannelViewDisplay? { get set }
    func configureChannelData(_ channel: Channel)
    func presentSelectedChannel(_ channel: Channel)
}

final class ResetChannelViewPresenter: ResetChannelViewPresenting {
    weak var viewController: ResetChannelViewDisplay?
    
    func configureChannelData(_ channel: Channel) {
        viewController?.configureView(
            with: URL(string: channel.icon),
            title: channel.title,
            info: channel.data,
            placeholderImage: placholderImage(for: channel.channelType)
        )
    }
    
    func presentSelectedChannel(_ channel: Channel) {
        viewController?.displaySelectedChannel(channel)
    }
}

private extension ResetChannelViewPresenter {
    func placholderImage(for channelType: ChannelType) -> UIImage {
        switch channelType {
        case .sms:
            return Assets.smsIcon.image
        case .email:
            return Assets.emailIcon.image
        case .biometric:
            return Assets.smartphoneIcon.image
        }
    }
}
