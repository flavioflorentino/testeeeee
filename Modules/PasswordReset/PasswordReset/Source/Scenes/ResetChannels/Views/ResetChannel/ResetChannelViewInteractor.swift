import Foundation

public protocol ResetChannelViewInteracting: AnyObject {
    func configureView()
    func didSelect()
}

final class ResetChannelViewInteractor {
    private let channel: Channel
    private let presenter: ResetChannelViewPresenting
    
    init(presenter: ResetChannelViewPresenting, channel: Channel) {
        self.channel = channel
        self.presenter = presenter
    }
}

extension ResetChannelViewInteractor: ResetChannelViewInteracting {
    func configureView() {
        presenter.configureChannelData(channel)
    }
    
    func didSelect() {
        presenter.presentSelectedChannel(channel)
    }
}
