import Foundation

enum ResetChannelsFactory {
    static func make(
        resetChannelsData: PasswordResetChannels,
        document: String,
        delegate: ResetChannelsCoordinatorDelegate?
    ) -> ResetChannelsViewController {
        let container = PasswordResetDependencyContainer()
        let coordinator: ResetChannelsCoordinating = ResetChannelsCoordinator()
        let service = ResetChannelsService(dependencies: container)
        let presenter: ResetChannelsPresenting = ResetChannelsPresenter(coordinator: coordinator)
        
        let interactor = ResetChannelsInteractor(
            service: service,
            presenter: presenter,
            channelsData: resetChannelsData,
            document: document,
            dependencies: container
        )
        
        let viewController = ResetChannelsViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
