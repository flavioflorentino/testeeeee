import Core
import Foundation

protocol ResetChannelsServicing {
    func startResetFlow(
        with channel: ChannelType,
        cpf: String,
        completion: @escaping ((Result<PasswordResetStepData, ApiError>) -> Void)
    )
}

final class ResetChannelsService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ResetChannelsService
extension ResetChannelsService: ResetChannelsServicing {
    func startResetFlow(
        with channel: ChannelType,
        cpf: String,
        completion: @escaping ((Result<PasswordResetStepData, ApiError>) -> Void)
    ) {
        let api = Api<PasswordResetStepData>(endpoint: PasswordResetEndpoint.startResetFlow(cpf: cpf, channelType: channel))
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
