import UI
import UIKit

protocol ResetChannelsDisplay: AnyObject {
    func displayChannelViews(with channels: [Channel])
    func displayLoading(_ loading: Bool)
    func displayFallback()
    func displayPopup(with popupProperties: PopupProperties)
}

private extension ResetChannelsViewController.Layout {
    enum Size {
        static let separatorViewHeight: CGFloat = 1
    }
}

final class ResetChannelsViewController: ViewController<ResetChannelsInteracting, UIView>, LoadingViewProtocol {
    fileprivate enum Layout { }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
        label.text = Strings.ResetChannels.header
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
        label.text = Strings.ResetChannels.body
        return label
    }()
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale200.color
        return view
    }()
    
    private lazy var channelsStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .fill
        stack.distribution = .fill
        return stack
    }()
    
    private lazy var fallbackView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.isHidden = true
        return view
    }()
    
    private lazy var fallbackMessageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .center)
        label.text = Strings.ResetChannels.fallbackLabel
        return label
    }()
    
    private lazy var fallbackActionButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.ResetChannels.fallbackButton, for: .normal)
        button.addTarget(self, action: #selector(didTapFallbackButton), for: .touchUpInside)
        button.buttonStyle(LinkButtonStyle())
        return button
    }()
    
    lazy var loadingView = LoadingView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.configureChannels()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(separatorView)
        view.addSubview(channelsStackView)
        view.addSubview(fallbackView)
        fallbackView.addSubview(fallbackMessageLabel)
        fallbackView.addSubview(fallbackActionButton)
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(view.snp.topMargin).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalTo(descriptionLabel.snp.top).offset(-Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalTo(separatorView.snp.top).offset(-Spacing.base03)
        }
        
        separatorView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.separatorViewHeight)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalTo(channelsStackView.snp.top)
        }
        
        channelsStackView.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        fallbackView.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base08)
        }
        
        fallbackMessageLabel.snp.makeConstraints {
            $0.leading.trailing.top.equalToSuperview()
            $0.bottom.equalTo(fallbackActionButton.snp_topMargin).offset(Spacing.base00)
        }
        
        fallbackActionButton.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    override func configureViews() {
        let backButton = UIBarButtonItem()
        backButton.title = String()
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        view.backgroundColor = Colors.backgroundPrimary.change(.dark, to: Colors.backgroundSecondary.darkColor).color
    }
}

@objc
private extension ResetChannelsViewController {
    func didTapFallbackButton() {
        interactor.selectFallback()
    }
}

// MARK: ResetChannelsDisplay
extension ResetChannelsViewController: ResetChannelsDisplay {
    func displayChannelViews(with channels: [Channel]) {
        for channel in channels {
            let channelPresenter = ResetChannelViewPresenter()
            let resetChannelInteractor = ResetChannelViewInteractor(presenter: channelPresenter, channel: channel)
            let resetChannelView = ResetChannelView(interactor: resetChannelInteractor)
            resetChannelView.delegate = self
            channelPresenter.viewController = resetChannelView
            resetChannelInteractor.configureView()
            channelsStackView.addArrangedSubview(resetChannelView)
        }
    }
    
    func displayFallback() {
        fallbackView.isHidden = false
    }
    
    func displayLoading(_ loading: Bool) {
        loading ? startLoadingView() : stopLoadingView()
    }
    
    func displayPopup(with popupProperties: PopupProperties) {
        let popupController = PopupViewController(
            title: popupProperties.title,
            description: popupProperties.message,
            preferredType: .text
        )
                
        let okAction = PopupAction(title: popupProperties.mainButtonTitle, style: .fill)
        popupController.addAction(okAction)
        showPopup(popupController)
    }
}

extension ResetChannelsViewController: ResetChannelViewDelegate {
    func didSelectChannel(_ channel: Channel) {
        interactor.selectChannel(channel)
    }
}
