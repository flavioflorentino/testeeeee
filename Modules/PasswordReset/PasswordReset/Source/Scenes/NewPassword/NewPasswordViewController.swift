import UI
import UIKit

protocol NewPasswordDisplay: AnyObject, LoadingViewProtocol {
    func enableCreateButton(_ enable: Bool)
    func translateViewY(by offset: CGFloat, with properties: KeyboardProperties)
    func firstPasswordError(_ message: String?, isError: Bool)
    func secondPasswordError(_ message: String?, isError: Bool)
    func showPopupController(with properties: PopupProperties)
}

private extension NewPasswordViewController.Layout {
    enum Size {
        static let backButton: CGFloat = 28
    }

    enum Spacing {
        static let minBetweenCreateButtonAndKeyboard = UI.Spacing.base03
    }
}

final class NewPasswordViewController: ViewController<NewPasswordInteracting, UIView>, NewPasswordDisplay {
    fileprivate enum Layout { }

    private lazy var backButton: UIButton = {
        let backButton = UIButton()
        backButton.setImage(Assets.backGreenArrow.image, for: .normal)
        backButton.addTarget(self, action: #selector(tapBackButton), for: .touchUpInside)
        return backButton
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .grayscale750())
            .with(\.text, Strings.NewPassword.Title.label)
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale750())
            .with(\.text, Strings.NewPassword.Description.label)
        return label
    }()

    private lazy var firstPasswordTextField: PasswordTextField = {
        let textField = PasswordTextField()
        textField.setPlaceholder(Strings.NewPassword.Placeholder.text)
        textField.textDidChange = firstPasswordTextChanged(_:)
        return textField
    }()

    private lazy var secondPasswordTextField: PasswordTextField = {
        let textField = PasswordTextField()
        textField.setPlaceholder(Strings.NewPassword.Placeholder.confirm)
        textField.textDidChange = secondPasswordTextChanged(_:)
        return textField
    }()

    private lazy var createButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.NewPassword.Button.createPassword, for: .normal)
        button.addTarget(self, action: #selector(didTapCreate), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()

    var loadingView = LoadingView()

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.setupView()

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(notification:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(notification:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func buildViewHierarchy() {
        view.addSubview(backButton)
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(firstPasswordTextField)
        view.addSubview(secondPasswordTextField)
        view.addSubview(createButton)
    }

    override func setupConstraints() {
        backButton.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base00)
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.size.equalTo(Layout.Size.backButton)
        }

        titleLabel.snp.makeConstraints {
            $0.top.equalTo(backButton.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalTo(titleLabel)
        }

        firstPasswordTextField.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalTo(descriptionLabel)
        }

        secondPasswordTextField.snp.makeConstraints {
            $0.top.equalTo(firstPasswordTextField.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalTo(firstPasswordTextField)
        }

        createButton.snp.makeConstraints {
            $0.top.equalTo(secondPasswordTextField.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalTo(secondPasswordTextField)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }

    // MARK: - NewPasswordDisplay

    func enableCreateButton(_ enable: Bool) {
        createButton.isEnabled = enable
    }

    func translateViewY(by offset: CGFloat, with properties: KeyboardProperties) {
        UIView.animate(withDuration: properties.animationDuration,
                       delay: 0,
                       options: [.init(rawValue: properties.animationCurve)],
                       animations: {
                            self.view.frame.origin.y += offset
        })
    }

    func firstPasswordError(_ message: String?, isError: Bool) {
        firstPasswordTextField.shouldShowError(isError, with: message)
    }

    func secondPasswordError(_ message: String?, isError: Bool) {
        secondPasswordTextField.shouldShowError(isError, with: message)
    }

    func showPopupController(with properties: PopupProperties) {
        let popupController = PopupViewController(
            title: properties.title,
            description: properties.message,
            preferredType: .text
        )

        let tryAgainAction = PopupAction(title: properties.mainButtonTitle, style: .fill) { [weak self, weak popupController] in
            popupController?.dismiss(animated: true) {
                self?.interactor.resetPassword()
            }
        }
        popupController.addAction(tryAgainAction)

        if let secondaryTitle = properties.secondaryButtonTitle {
            popupController.addAction(PopupAction(title: secondaryTitle, style: .link))
        }

        showPopup(popupController)
    }
}

@objc
private extension NewPasswordViewController {
    // MARK: - Keyboard
    
    func keyboardWillShow(notification: NSNotification) {
        guard let keyboardProperties = getKeyboardProperties(from: notification) else {
            return
        }

        let keyboardYInView = view.frame.height - keyboardProperties.height
        let createButtonMaxY = createButton.frame.maxY
        let currentSpacing = keyboardYInView - createButtonMaxY

        interactor.keyboardWillShow(currentSpacing: currentSpacing,
                                    minSpacing: Layout.Spacing.minBetweenCreateButtonAndKeyboard,
                                    properties: keyboardProperties)
    }

    func keyboardWillHide(notification: NSNotification) {
        guard let keyboardProperties = getKeyboardProperties(from: notification) else {
            return
        }
        interactor.keyboardWillHide(properties: keyboardProperties)
    }

    // MARK: - Button

    func didTapCreate() {
        view.endEditing(true)
        interactor.resetPassword()
    }

    func tapBackButton() {
        interactor.backButtonAction()
    }
}

private extension NewPasswordViewController {
    func getKeyboardProperties(from notification: NSNotification) -> KeyboardProperties? {
        guard
            let frame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect,
            let curve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt,
            let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double
            else {
                return nil
        }
        return KeyboardProperties(height: frame.height, animationCurve: curve, animationDuration: duration)
    }

    func firstPasswordTextChanged(_ text: String?) {
        interactor.firstPasswordEditingChange(text)
    }

    func secondPasswordTextChanged(_ text: String?) {
        interactor.secondPasswordEditingChange(text)
    }
}
