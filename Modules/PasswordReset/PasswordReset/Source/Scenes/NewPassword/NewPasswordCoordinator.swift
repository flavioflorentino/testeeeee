import Foundation

enum NewPasswordAction {
    case nextStep(PasswordResetStepData)
    case back
}

protocol NewPasswordCoordinatorDelegate: AnyObject {
    func newPasswordNextStep(_ stepData: PasswordResetStepData)
    func backFromNewPassword(shouldShowWarning: Bool)
}

protocol NewPasswordCoordinating: AnyObject {
    var delegate: NewPasswordCoordinatorDelegate? { get set }
    func perform(action: NewPasswordAction)
}

final class NewPasswordCoordinator: NewPasswordCoordinating {
    weak var delegate: NewPasswordCoordinatorDelegate?
    private let shouldShowWarning: Bool

    init(shouldShowWarning: Bool) {
        self.shouldShowWarning = shouldShowWarning
    }
    
    func perform(action: NewPasswordAction) {
        switch action {
        case .nextStep(let data):
            delegate?.newPasswordNextStep(data)
        case .back:
            delegate?.backFromNewPassword(shouldShowWarning: shouldShowWarning)
        }
    }
}
