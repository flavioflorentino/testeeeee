import AnalyticsModule
import CoreGraphics
import Foundation

private enum KeyboardState {
    case showing
    case hiding
}

protocol NewPasswordInteracting: AnyObject {
    func setupView()
    func keyboardWillShow(currentSpacing: CGFloat, minSpacing: CGFloat, properties: KeyboardProperties)
    func keyboardWillHide(properties: KeyboardProperties)
    func firstPasswordEditingChange(_ text: String?)
    func secondPasswordEditingChange(_ text: String?)
    func resetPassword()
    func backButtonAction()
}

final class NewPasswordInteractor: NewPasswordInteracting {
    typealias Dependencies = HasAnalytics & HasAuthManager
    private let dependencies: Dependencies

    private let service: NewPasswordServicing
    private let presenter: NewPasswordPresenting
    private let channelType: ChannelType
    private let resetId: String

    private var keyboardState: KeyboardState = .hiding
    private var offset: CGFloat = 0

    private let passwordMinLength = 4
    private var firstPassword: String = ""
    private var secondPassword: String = ""

    init(
        service: NewPasswordServicing,
        presenter: NewPasswordPresenting,
        channelType: ChannelType,
        resetId: String,
        dependencies: Dependencies
    ) {
        self.service = service
        self.presenter = presenter
        self.channelType = channelType
        self.resetId = resetId
        self.dependencies = dependencies
    }

    // MARK: - NewPasswordInteracting

    func setupView() {
        trackEvent(NewPasswordEvent.appear)
        presenter.enableCreateButton(false)
    }

    func keyboardWillShow(currentSpacing: CGFloat, minSpacing: CGFloat, properties: KeyboardProperties) {
        guard keyboardState != .showing else {
            return
        }
        keyboardState = .showing

        if currentSpacing < minSpacing {
            offset = minSpacing - currentSpacing
            presenter.translateViewY(by: offset * -1, with: properties)
        }
    }

    func keyboardWillHide(properties: KeyboardProperties) {
        guard keyboardState != .hiding else {
            return
        }
        keyboardState = .hiding
        presenter.translateViewY(by: offset, with: properties)
    }

    func firstPasswordEditingChange(_ text: String?) {
        guard let password = text else { return }
        firstPassword = password
        validatePasswords()
    }

    func secondPasswordEditingChange(_ text: String?) {
        guard let password = text else { return }
        secondPassword = password
        validatePasswords()
    }

    func resetPassword() {
        presenter.startLoading()
        service.resetPassword(
            resetId: resetId,
            password: firstPassword,
            passwordConfirmation: secondPassword
        ) { [weak self] result in
            self?.presenter.stopLoading()

            switch result {
            case .success(let response):
                self?.dependencies.authenticationManager?.disableBiometricAuthentication()
                self?.presenter.goToNextStep(response)
            case .failure(let apiError):
                self?.handleErrorTracking(apiError.requestError?.code)
                self?.presenter.showPopupController(with: apiError.requestError)
            }
        }
    }

    func backButtonAction() {
        presenter.goBack()
    }
}

private extension NewPasswordInteractor {
    func validatePasswords() {
        guard validateFirstPassword(), validateSecondPassword() else {
            presenter.enableCreateButton(false)
            return
        }
        presenter.enableCreateButton(true)
    }

    func validateFirstPassword() -> Bool {
        guard CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: firstPassword)) else {
            presenter.showFirstPasswordError(Strings.NewPassword.Error.numbersOnly, isError: true)
            return false
        }

        guard firstPassword.count >= passwordMinLength else {
            presenter.showFirstPasswordError(Strings.NewPassword.Error.length(passwordMinLength), isError: true)
            return false
        }

        presenter.showFirstPasswordError(nil, isError: false)
        return true
    }

    func validateSecondPassword() -> Bool {
        guard secondPassword.count >= passwordMinLength else {
            presenter.showSecondPasswordError(nil, isError: false)
            return false
        }
        
        guard secondPassword == firstPassword else {
            presenter.showSecondPasswordError(Strings.NewPassword.Error.differentPasswords, isError: true)
            return false
        }

        presenter.showSecondPasswordError(nil, isError: false)
        return true
    }

    func trackEvent(_ event: PasswordResetEvent) {
        let eventTracker = PasswordResetTracker(method: channelType, eventType: event)
        dependencies.analytics.log(eventTracker)
    }

    func handleErrorTracking(_ errorCode: String?) {
        let errorCode = PasswordResetErrorCode(errorCode)
        switch errorCode {
        case .passwordDifferent:
            trackEvent(NewPasswordEvent.matchError)
        case .passwordResetBlock:
            trackEvent(ErrorEvent.blocked)
        default:
            trackEvent(ErrorEvent.generic)
        }
    }
}
