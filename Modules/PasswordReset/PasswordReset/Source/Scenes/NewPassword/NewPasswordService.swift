import Core
import Foundation

protocol NewPasswordServicing {
    func resetPassword(
        resetId: String,
        password: String,
        passwordConfirmation: String,
        completion: @escaping (Result<PasswordResetStepData, ApiError>) -> Void
    )
}

final class NewPasswordService: NewPasswordServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    // MARK: - NewPasswordServicing

    func resetPassword(
        resetId: String,
        password: String,
        passwordConfirmation: String,
        completion: @escaping (Result<PasswordResetStepData, ApiError>) -> Void
    ) {
        let endpoint = PasswordResetEndpoint.resetPassword(
            resetId: resetId,
            password: password,
            passwordConfirmation: passwordConfirmation
        )
        let api = Api<PasswordResetStepData>(endpoint: endpoint)

        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
