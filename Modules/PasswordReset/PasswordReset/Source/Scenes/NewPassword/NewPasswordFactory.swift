import Foundation

enum NewPasswordFactory {
    static func make(
        channelType: ChannelType,
        resetId: String,
        coordinatorDelegate: NewPasswordCoordinatorDelegate?,
        shouldShowPopupWarning: Bool = false
    ) -> NewPasswordViewController {
        let container = PasswordResetDependencyContainer()
        let service = NewPasswordService(dependencies: container)
        let coordinator = NewPasswordCoordinator(shouldShowWarning: shouldShowPopupWarning)
        let presenter = NewPasswordPresenter(coordinator: coordinator)
        let interactor = NewPasswordInteractor(
            service: service,
            presenter: presenter,
            channelType: channelType,
            resetId: resetId,
            dependencies: container
        )
        let viewController = NewPasswordViewController(interactor: interactor)

        presenter.viewController = viewController
        coordinator.delegate = coordinatorDelegate

        return viewController
    }
}
