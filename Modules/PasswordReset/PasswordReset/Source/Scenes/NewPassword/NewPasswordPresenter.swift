import Core
import Foundation

protocol NewPasswordPresenting: AnyObject {
    var viewController: NewPasswordDisplay? { get set }
    func enableCreateButton(_ enable: Bool)
    func translateViewY(by offset: CGFloat, with properties: KeyboardProperties)
    func showFirstPasswordError(_ message: String?, isError: Bool)
    func showSecondPasswordError(_ message: String?, isError: Bool)
    func startLoading()
    func stopLoading()
    func goToNextStep(_ step: PasswordResetStepData)
    func goBack()
    func showPopupController(with requestError: RequestError?)
}

final class NewPasswordPresenter: NewPasswordPresenting {
    private let coordinator: NewPasswordCoordinating
    weak var viewController: NewPasswordDisplay?

    init(coordinator: NewPasswordCoordinating) {
        self.coordinator = coordinator
    }

    // MARK: - NewPasswordPresenting

    func enableCreateButton(_ enable: Bool) {
        viewController?.enableCreateButton(enable)
    }

    func translateViewY(by offset: CGFloat, with properties: KeyboardProperties) {
        viewController?.translateViewY(by: offset, with: properties)
    }

    func showFirstPasswordError(_ message: String?, isError: Bool) {
        viewController?.firstPasswordError(message, isError: isError)
    }

    func showSecondPasswordError(_ message: String?, isError: Bool) {
        viewController?.secondPasswordError(message, isError: isError)
    }

    func startLoading() {
        viewController?.startLoadingView()
    }

    func stopLoading() {
        viewController?.stopLoadingView()
    }

    func showPopupController(with requestError: RequestError?) {
        let properties = PopupProperties(
            title: requestError?.title ?? Strings.Error.title,
            message: requestError?.message ?? Strings.Error.message,
            mainButtonTitle: Strings.General.tryAgain,
            secondaryButtonTitle: Strings.General.back
        )
        viewController?.showPopupController(with: properties)
    }

    func goToNextStep(_ step: PasswordResetStepData) {
        coordinator.perform(action: .nextStep(step))
    }

    func goBack() {
        coordinator.perform(action: .back)
    }
}
