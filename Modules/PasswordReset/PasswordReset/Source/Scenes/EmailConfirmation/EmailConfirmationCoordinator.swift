import UIKit

enum EmailConfirmationAction: Equatable {
    case next(stepData: PasswordResetStepData, email: String)
}

protocol EmailConfirmationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: EmailConfirmationAction)
    var delegate: EmailConfirmationCoordinatorDelegate? { get set }
}

protocol EmailConfirmationCoordinatorDelegate: AnyObject {
    func emailConfirmationNextStep(_ stepData: PasswordResetStepData, userEmail: String)
}

final class EmailConfirmationCoordinator {
    weak var viewController: UIViewController?
    weak var delegate: EmailConfirmationCoordinatorDelegate?
}

// MARK: - EmailConfirmationCoordinating
extension EmailConfirmationCoordinator: EmailConfirmationCoordinating {
    func perform(action: EmailConfirmationAction) {
        if case let .next(stepData, email) = action {
            delegate?.emailConfirmationNextStep(stepData, userEmail: email)
        }
    }
}
