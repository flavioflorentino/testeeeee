import AnalyticsModule
import Core
import Foundation
import Validations

protocol EmailConfirmationInteracting: AnyObject {
    func sendAnalytics()
    func checkEmail(_ email: String)
    func validateEmail(_ email: String)
}

final class EmailConfirmationInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: EmailConfirmationServicing
    private let presenter: EmailConfirmationPresenting
    private let resetId: String
    
    init(service: EmailConfirmationServicing, presenter: EmailConfirmationPresenting, dependencies: Dependencies, resetId: String) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.resetId = resetId
    }
}

// MARK: - EmailConfirmationInteracting
extension EmailConfirmationInteractor: EmailConfirmationInteracting {
    func sendAnalytics() {
        let eventTracker = PasswordResetTracker(eventType: FallbackEvent.newEmail)
        dependencies.analytics.log(eventTracker)
    }
    
    func checkEmail(_ email: String) {
        let validator = RegexValidator(descriptor: AccountRegexDescriptor.email)
        do {
            try validator.validate(email)
            presenter.shouldEnableContinue(true)
        } catch {
            presenter.shouldEnableContinue(false)
        }
    }
    
    func validateEmail(_ email: String) {
        presenter.presentLoading(true)
        service.validateEmail(email, resetId: resetId) { [weak self] result in
            self?.presenter.presentLoading(false)
            switch result {
            case let .success(stepData):
                self?.presenter.presentNextStep(stepData, userEmail: email)
            case let .failure(error):
                let eventTracker = PasswordResetTracker(eventType: FallbackEvent.newEmailError)
                self?.dependencies.analytics.log(eventTracker)
                self?.presenter.presentError(error)
            }
        }
    }
}
