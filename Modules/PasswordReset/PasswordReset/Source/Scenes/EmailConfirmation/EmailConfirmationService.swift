import Core
import Foundation

protocol EmailConfirmationServicing {
    func validateEmail(_ email: String, resetId: String, completion: @escaping ((Result<PasswordResetStepData, ApiError>) -> Void))
}

final class EmailConfirmationService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - EmailConfirmationServicing
extension EmailConfirmationService: EmailConfirmationServicing {
    func validateEmail(_ email: String, resetId: String, completion: @escaping ((Result<PasswordResetStepData, ApiError>) -> Void)) {
        let endpoint = PasswordResetEndpoint.validateEmail(email: email, resetId: resetId)
        let api = Api<PasswordResetStepData>(endpoint: endpoint)
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
