import Core
import Foundation

protocol EmailConfirmationPresenting: AnyObject {
    var viewController: EmailConfirmationDisplaying? { get set }
    func shouldEnableContinue(_ enable: Bool)
    func presentError(_ error: ApiError)
    func presentNextStep(_ stepData: PasswordResetStepData, userEmail: String)
    func presentLoading(_ loading: Bool)
}

final class EmailConfirmationPresenter {
    private let coordinator: EmailConfirmationCoordinating
    weak var viewController: EmailConfirmationDisplaying?

    init(coordinator: EmailConfirmationCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - EmailConfirmationPresenting
extension EmailConfirmationPresenter: EmailConfirmationPresenting {
    func shouldEnableContinue(_ enable: Bool) {
        viewController?.displayContinueButtonEnabled(enable)
    }
    
    func presentError(_ error: ApiError) {
        guard let requestError = error.requestError else {
            viewController?.displayError(title: Strings.Error.title, message: Strings.Error.message)
            return
        }
        
        viewController?.displayError(title: requestError.title, message: requestError.message)
    }
    
    func presentNextStep(_ stepData: PasswordResetStepData, userEmail: String) {
        coordinator.perform(action: .next(stepData: stepData, email: userEmail))
    }
    
    func presentLoading(_ loading: Bool) {
        viewController?.displayLoading(loading)
    }
}
