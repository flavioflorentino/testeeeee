import AssetsKit
import UI
import UIKit

protocol EmailConfirmationDisplaying: AnyObject {
    func displayContinueButtonEnabled(_ enabled: Bool)
    func displayError(title: String, message: String)
    func displayLoading(_ loading: Bool)
}

final class EmailConfirmationViewController: ViewController<EmailConfirmationInteracting, UIView>, LoadingViewProtocol {
    private lazy var scrollView: UIScrollView = {
           let scroll = UIScrollView()
           scroll.backgroundColor = .clear
           return scroll
       }()
       
       private lazy var contentView: UIView = {
           let view = UIView()
           view.backgroundColor = .clear
           return view
       }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
        label.text = Strings.NewEmail.title
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
        label.text = Strings.NewEmail.description
        return label
    }()
    
    private lazy var emailTextfield: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.defaultForm()
        textfield.keyboardType = .emailAddress
        textfield.autocorrectionType = .no
        textfield.placeholder = Strings.NewEmail.fieldPlaceholder
        textfield.addTarget(self, action: #selector(didTypeEmail), for: .editingChanged)
        textfield.delegate = self
        return textfield
    }()
    
    private lazy var continueButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.General.continue, for: .normal)
        button.addTarget(self, action: #selector(didTapContinue), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        button.isEnabled = false
        return button
    }()
    
    lazy var loadingView = LoadingView()
    private lazy var keyboardScrollViewHandler = KeyboardScrollViewHandler(scrollView: scrollView)
        
    override func viewDidLoad() {
        super.viewDidLoad()
        keyboardScrollViewHandler.registerForKeyboardNotifications()
        interactor.sendAnalytics()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(emailTextfield)
        contentView.addSubview(continueButton)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.top.bottom.leading.trailing.equalTo(view)
        }
        
        contentView.snp.makeConstraints {
            $0.top.bottom.leading.trailing.equalTo(scrollView)
            $0.width.equalTo(view.snp.width)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(contentView.snp.topMargin).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(descriptionLabel.snp.top).offset(-Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(emailTextfield.snp.top).offset(-Spacing.base03)
        }
        
        emailTextfield.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(continueButton.snp.top).offset(-Spacing.base05)
        }
        
        continueButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base04)
        }
    }
    
    override func configureViews() {        
        view.backgroundColor = Colors.backgroundPrimary.color
        let backButton = UIBarButtonItem()
        backButton.title = String()
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
}

@objc
private extension EmailConfirmationViewController {
    func didTapContinue() {
        viewModel.validateEmail(emailTextfield.text ?? "")
    }
    
    func didTypeEmail() {
        viewModel.checkEmail(emailTextfield.text ?? "")
    }
}

// MARK: - EmailConfirmationDisplaying
extension EmailConfirmationViewController: EmailConfirmationDisplaying {
    func displayContinueButtonEnabled(_ enabled: Bool) {
        continueButton.isEnabled = enabled
    }
    
    func displayError(title: String, message: String) {
        let popup = PopupViewController(title: title, description: message)
        let okAction = PopupAction(title: Strings.General.understood, style: .fill)
        popup.addAction(okAction)
        showPopup(popup)
    }
    
    func displayLoading(_ loading: Bool) {
        loading ? startLoadingView() : stopLoadingView()
    }
}

extension EmailConfirmationViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
