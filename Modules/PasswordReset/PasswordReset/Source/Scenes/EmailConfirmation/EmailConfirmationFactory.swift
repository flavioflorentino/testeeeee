import UIKit

enum EmailConfirmationFactory {
    static func make(resetId: String, coordinatorDelegate: EmailConfirmationCoordinatorDelegate) -> EmailConfirmationViewController {
        let container = PasswordResetDependencyContainer()
        let service: EmailConfirmationServicing = EmailConfirmationService(dependencies: container)
        let coordinator: EmailConfirmationCoordinating = EmailConfirmationCoordinator()
        let presenter: EmailConfirmationPresenting = EmailConfirmationPresenter(coordinator: coordinator)
        let interactor = EmailConfirmationInteractor(service: service, presenter: presenter, dependencies: container, resetId: resetId)
        let viewController = EmailConfirmationViewController(interactor: interactor)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        presenter.viewController = viewController

        return viewController
    }
}
