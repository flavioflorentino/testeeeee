import Foundation

struct IdentityFlowData: Decodable {
    let token: String
    let flow: String
}
