import UI
import UIKit

protocol BiometricDisplay: AnyObject, LoadingViewProtocol {
    func showPopupController(with properties: PopupProperties)
}

final class BiometricViewController: ViewController<BiometricInteracting, UIView>, BiometricDisplay {
    lazy var loadingView = LoadingView()

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.getIdentityFlow()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func configureViews() {
        view.backgroundColor = .clear
    }

    // MARK: - BiometricDisplay

    func showPopupController(with properties: PopupProperties) {
        let popupController = PopupViewController(
            title: properties.title,
            description: properties.message,
            preferredType: .text
        )

        let tryAgainAction = PopupAction(title: properties.mainButtonTitle, style: .fill) { [weak self, weak popupController] in
            popupController?.dismiss(animated: true) {
                self?.interactor.getIdentityFlow()
            }
        }
        popupController.addAction(tryAgainAction)

        let backActionTitle = properties.secondaryButtonTitle ?? Strings.General.back
        let backAction = PopupAction(title: backActionTitle, style: .link) { [weak self, weak popupController] in
            popupController?.dismiss(animated: true) {
                self?.interactor.backAction()
            }
        }
        popupController.addAction(backAction)

        showPopup(popupController)
    }
}
