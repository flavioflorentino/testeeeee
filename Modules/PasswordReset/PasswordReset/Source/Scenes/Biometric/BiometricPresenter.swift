import Core
import Foundation

protocol BiometricPresenting {
    func startLoading()
    func stopLoading()
    func showIdentityValidation(_ data: IdentityFlowData)
    func showError(_ requestError: RequestError?)
    func goBack()
}

final class BiometricPresenter: BiometricPresenting {
    private let coordinator: BiometricCoordinating
    weak var viewController: BiometricDisplay?

    init(coordinator: BiometricCoordinating) {
        self.coordinator = coordinator
    }

    // MARK: - BiometricPresenting

    func startLoading() {
        viewController?.startLoadingView()
    }

    func stopLoading() {
        viewController?.stopLoadingView()
    }

    func showIdentityValidation(_ data: IdentityFlowData) {
        coordinator.perform(action: .identityValidation(data))
    }

    func showError(_ requestError: RequestError?) {
        let properties = PopupProperties(
            title: requestError?.title ?? Strings.Error.title,
            message: requestError?.message ?? Strings.Error.message,
            mainButtonTitle: Strings.General.tryAgain,
            secondaryButtonTitle: Strings.General.back
        )
        viewController?.showPopupController(with: properties)
    }

    func goBack() {
        coordinator.perform(action: .back)
    }
}
