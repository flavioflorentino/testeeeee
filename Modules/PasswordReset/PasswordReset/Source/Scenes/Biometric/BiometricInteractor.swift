import AnalyticsModule
import Foundation

protocol BiometricInteracting {
    func getIdentityFlow()
    func backAction()
}

final class BiometricInteractor: BiometricInteracting {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: BiometricServicing
    private let presenter: BiometricPresenting
    private let resetId: String

    init(service: BiometricServicing, presenter: BiometricPresenting, resetId: String, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.resetId = resetId
        self.dependencies = dependencies
    }

    // MARK: - BiometricInteracting

    func getIdentityFlow() {
        presenter.startLoading()

        service.selfieFlow(resetId: resetId) { [weak self] result in
            self?.presenter.stopLoading()

            switch result {
            case .success(let selfieData):
                self?.dependencies.analytics.log(PasswordResetTracker(eventType: BiometricEvent.success))
                self?.presenter.showIdentityValidation(selfieData)
            case .failure(let apiError):
                self?.presenter.showError(apiError.requestError)
            }
        }
    }

    func backAction() {
        presenter.goBack()
    }
}
