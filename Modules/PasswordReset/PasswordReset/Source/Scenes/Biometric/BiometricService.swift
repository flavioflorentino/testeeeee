import Core
import Foundation

protocol BiometricServicing {
    func selfieFlow(resetId: String, completion: @escaping (Result<IdentityFlowData, ApiError>) -> Void)
}

final class BiometricService: BiometricServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    // MARK: - BiometricServicing

    func selfieFlow(resetId: String, completion: @escaping (Result<IdentityFlowData, ApiError>) -> Void) {
        let api = Api<IdentityFlowData>(endpoint: PasswordResetEndpoint.selfieData(resetId: resetId))

        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
