import Foundation
import UIKit

enum BiometricAction {
    case identityValidation(IdentityFlowData)
    case back
}

protocol BiometricCoordinatorDelegate: AnyObject {
    func biometricController(_ controller: UIViewController?, showIdentity flowData: IdentityFlowData)
    func back(from controller: UIViewController?)
}

protocol BiometricCoordinating {
    var viewController: UIViewController? { get set }
    var delegate: BiometricCoordinatorDelegate? { get set }
    func perform(action: BiometricAction)
}

final class BiometricCoordinator: BiometricCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: BiometricCoordinatorDelegate?

    func perform(action: BiometricAction) {
        switch action {
        case .identityValidation(let identityData):
            delegate?.biometricController(viewController, showIdentity: identityData)
        case .back:
            delegate?.back(from: viewController)
        }
    }
}
