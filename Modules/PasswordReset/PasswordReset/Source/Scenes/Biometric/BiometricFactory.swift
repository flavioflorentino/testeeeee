import Foundation

enum BiometricFactory {
    static func make(resetId: String, coordinatorDelegate: BiometricCoordinatorDelegate?) -> BiometricViewController {
        let container = PasswordResetDependencyContainer()
        let service = BiometricService(dependencies: container)
        let coordinator = BiometricCoordinator()
        let presenter = BiometricPresenter(coordinator: coordinator)
        let interactor = BiometricInteractor(service: service, presenter: presenter, resetId: resetId, dependencies: container)
        let viewController = BiometricViewController(interactor: interactor)

        presenter.viewController = viewController
        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        return viewController
    }
}
