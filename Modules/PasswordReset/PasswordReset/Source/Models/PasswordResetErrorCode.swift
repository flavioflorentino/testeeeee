import Foundation

enum PasswordResetErrorCode: String {
    case invalidCode = "invalid_code"
    case passwordDifferent = "password_different"
    case passwordResetBlock = "password_reset_block"
    case passwordLinkRequestBlock = "password_send_link_many_request"
    case passwordReceivedDevice = "password_received_device"
    case passwordReceivedExpiredCode = "password_received_expired_code"
    case passwordReceivedValidatedCode = "password_received_validated_code"
    case passwordReceivedLink = "password_received_link"
    case unkown

    init(_ string: String?) {
        self = PasswordResetErrorCode(rawValue: string ?? "") ?? .unkown
    }

    var trackableCode: String {
        switch self {
        case .passwordReceivedDevice:
            return "WRONG_DEVICE"
        case .passwordReceivedExpiredCode:
            return "EXPIRED_CODE"
        case .passwordReceivedValidatedCode:
            return "VALIDATED_CODE"
        default:
            return "GENERIC"
        }
    }
}
