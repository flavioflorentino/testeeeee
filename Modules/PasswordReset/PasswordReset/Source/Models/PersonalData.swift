import Foundation

struct PersonalData {
    let resetId: String
    let name: String
    let birthDate: String
    let phoneNumber: String
    let deviceModel: String

    var bodyDictionary: [String: Any] {
        [
            "password_reset_id": resetId,
            "name": name,
            "actual_phone_number": phoneNumber,
            "device_model": deviceModel,
            "birth_date": birthDate
        ]
    }
}
