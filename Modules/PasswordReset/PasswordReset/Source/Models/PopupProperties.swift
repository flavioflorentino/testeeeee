import Foundation

struct PopupProperties {
    let title: String
    let message: String
    let mainButtonTitle: String
    let secondaryButtonTitle: String?
}
