import Foundation

struct PasswordResetChannels: Decodable, Equatable {
    let channels: [Channel]
    let fallback: Channel?
}

struct Channel: Decodable, Equatable {
    let channelType: ChannelType
    let title: String
    let icon: String
    let data: String
    
    enum CodingKeys: String, CodingKey {
        case channelType = "channel"
        case title
        case icon
        case data
    }
}

enum ChannelType: String, Decodable, Equatable {
    case sms = "SMS"
    case email = "EMAIL"
    case biometric = "BIOMETRIC"
}
