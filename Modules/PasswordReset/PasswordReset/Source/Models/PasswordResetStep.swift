import Foundation

struct PasswordResetStepData: Decodable, Equatable {
    let passwordResetId: String
    let nextStep: PasswordResetStep
}

enum PasswordResetStep: String, Decodable, Equatable {
    case code = "CODE"
    case newPassword = "NEW_PASSWORD"
    case newEmail = "NEW_EMAIL"
    case selfie = "SELFIE"
    case notApproved = "NOT_APPROVED"
    case personalData = "PERSONAL_DATA"
    case manualReview = "MANUAL_REVIEW"
    case done = "DONE"
    case link = "LINK"
}
