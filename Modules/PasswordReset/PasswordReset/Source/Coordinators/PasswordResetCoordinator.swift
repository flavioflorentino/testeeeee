import Foundation
import UI
import UIKit

public protocol PasswordResetCoordinatorProtocol: Coordinating {
    var childCoordinator: PasswordResetCoordinatorProtocol? { get set }
}

public final class PasswordResetCoordinator: PasswordResetCoordinatorProtocol {
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    public var childCoordinator: PasswordResetCoordinatorProtocol?

    private let navigationController: UINavigationController
    private let presenterController: UIViewController
    
    public init(
        presenterController: UIViewController,
        navigationController: UINavigationController = UINavigationController()
    ) {
        self.navigationController = navigationController
        self.presenterController = presenterController
        configureNavigationBar()
        setupInitalController()
    }
    
    public func start() {
        if presenterController.presentedViewController == nil {
            presenterController.present(navigationController, animated: true)
        } else {
            presenterController.dismiss(animated: true) {
                self.presenterController.present(self.navigationController, animated: true)
            }
        }
    }
}

// MARK: - Private methods
private extension PasswordResetCoordinator {
    func configureNavigationBar() {
        if #available(iOS 13.0, *) {
            navigationController.isModalInPresentation = true
        }
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.tintColor = Colors.brandingBase.color
        navigationController.navigationBar.barTintColor = Colors.backgroundPrimary.color
        navigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController.navigationBar.shadowImage = UIImage()
    }
    
    func setupInitalController() {
        let userIdentificationVC = UserIdentificationFactory.make(with: self)
        navigationController.viewControllers = [userIdentificationVC]
    }
    
    func presentChannels(_ channelsData: PasswordResetChannels, for document: String) {
        let resetChannelsVC = ResetChannelsFactory.make(resetChannelsData: channelsData, document: document, delegate: self)
        navigationController.pushViewController(resetChannelsVC, animated: true)
    }
    
    func presentChannelsCoordinator(with stepData: PasswordResetStepData, channel: Channel) {
        childCoordinator = PasswordResetChannelsCoordinator(
            navigationController: navigationController,
            currentStep: stepData,
            channel: channel
        )
        childCoordinator?.start()
    }
    
    func presenteFallbackCoordinator(with stepData: PasswordResetStepData, channelType: ChannelType) {
        childCoordinator = PasswordResetFallbackCoordinator(
            navigationController: navigationController,
            currentStep: stepData,
            channelType: channelType,
            delegate: self
        )
        childCoordinator?.start()
    }
}

// MARK: - Delegate methods
extension PasswordResetCoordinator: UserIdentificationCoordinatorDelegate {
    func didReceivedResetChannels(_ channelsData: PasswordResetChannels, for document: String) {
        presentChannels(channelsData, for: document)
    }
    
    func didClose() {
        navigationController.dismiss(animated: true)
    }
}

extension PasswordResetCoordinator: ResetChannelsCoordinatorDelegate {
    func didReceiveChannelNextStep(_ stepData: PasswordResetStepData, channel: Channel) {
        guard channel.channelType == .biometric else {
            presentChannelsCoordinator(with: stepData, channel: channel)
            return
        }
        presenteFallbackCoordinator(with: stepData, channelType: channel.channelType)
    }
}

extension PasswordResetCoordinator: PasswordResetFallbackCoordinatorDelegate {
    func resetFallbackFlow() {
        childCoordinator = nil

        let channelsViewController = navigationController.viewControllers.first { $0 is ResetChannelsViewController }
        guard let channelsVC = channelsViewController else {
            navigationController.popToRootViewController(animated: true)
            return
        }
        navigationController.popToViewController(channelsVC, animated: true)
    }
}
