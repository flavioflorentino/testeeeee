import UI
import UIKit

public final class PasswordResetDeeplinkCoordinator: PasswordResetCoordinatorProtocol {
    public var childViewController = [UIViewController]()
    public var viewController: UIViewController?
    public var childCoordinator: PasswordResetCoordinatorProtocol?

    private weak var originController: UIViewController?
    private let code: String
    private let navigationController: UINavigationController
    private let channel = Channel(channelType: .email, title: "", icon: "", data: "")

    public init(originController: UIViewController?,
                code: String,
                navigationController: UINavigationController = UINavigationController()) {
        self.originController = originController
        self.code = code
        self.navigationController = navigationController
    }

    public func start() {
        pushValidationControllerToNavigation()
        presentNavigationController()
    }

    private func pushValidationControllerToNavigation() {
        let validationController = LinkValidationFactory.make(code: code, delegate: self)
        navigationController.pushViewController(validationController, animated: false)
        if #available(iOS 13.0, *) {
            navigationController.isModalInPresentation = true
        }
    }

    private func presentNavigationController() {
        if originController?.presentedViewController == nil {
            originController?.present(navigationController, animated: true)
        } else {
            originController?.dismiss(animated: true) {
                self.originController?.present(self.navigationController, animated: true)
            }
        }
    }
}

extension PasswordResetDeeplinkCoordinator: LinkValidationCoordinatorDelegate {
    func linkValidationNextStep(_ stepData: PasswordResetStepData) {
        childCoordinator = PasswordResetChannelsCoordinator(
            navigationController: navigationController,
            currentStep: stepData,
            channel: channel,
            isFromDeeplink: true
        )
        childCoordinator?.start()
    }

    func linkValidationError(code: PasswordResetErrorCode, title: String?, message: String?) {
        let errorController = LinkErrorFactory.make(code: code,
                                                    channelType: channel.channelType,
                                                    title: title,
                                                    message: message,
                                                    delegate: self)
        navigationController.pushViewController(errorController, animated: true)
    }

    func linkValidationClose() {
        navigationController.dismiss(animated: true)
    }
}

extension PasswordResetDeeplinkCoordinator: LinkErrorCoordinatorDelegate {
    func closeLinkError() {
        navigationController.dismiss(animated: true)
    }
}
