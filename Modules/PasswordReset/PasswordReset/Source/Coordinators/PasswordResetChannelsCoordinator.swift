import Foundation
import UIKit

final class PasswordResetChannelsCoordinator: PasswordResetCoordinatorProtocol {
    var childViewController: [UIViewController] = []
    var viewController: UIViewController?
    var childCoordinator: PasswordResetCoordinatorProtocol?
    
    private let navigationController: UINavigationController
    private let channel: Channel
    private let isFromDeeplink: Bool
    private var currentStep: PasswordResetStepData
    
    init(navigationController: UINavigationController,
         currentStep: PasswordResetStepData,
         channel: Channel,
         isFromDeeplink: Bool = false) {
        self.navigationController = navigationController
        self.currentStep = currentStep
        self.channel = channel
        self.isFromDeeplink = isFromDeeplink
    }
 
    func start() {
        presentNextStep(currentStep)
    }
}

// MARK: - Private methods
private extension PasswordResetChannelsCoordinator {
    func presentAuthorizationCode() {
        let authorizationVC = AuthorizationCodeFactory.make(
            channelType: channel.channelType,
            resetId: currentStep.passwordResetId,
            coordinatorDelegate: self
        )
        navigationController.pushViewController(authorizationVC, animated: true)
    }
    
    func presentNewPassword() {
        let newPasswordVC = NewPasswordFactory.make(
            channelType: channel.channelType,
            resetId: currentStep.passwordResetId,
            coordinatorDelegate: self,
            shouldShowPopupWarning: isFromDeeplink
        )
        navigationController.pushViewController(newPasswordVC, animated: true)
    }
    
    func presentSuccess() {
        let vc = ResetSuccessFactory.make(method: channel.channelType, coordinatorDelegate: self)
        navigationController.pushViewController(vc, animated: true)
    }

    func presentValidationStatus() {
        let infoVC = ValidationStatusFactory.make(with: currentStep.nextStep, coordinatorDelegate: self)
        navigationController.pushViewController(infoVC, animated: true)
    }

    func presentLinkConfirmation() {
        let confirmationController = LinkConfirmationFactory.make(channelType: channel.channelType,
                                                                  resetId: currentStep.passwordResetId,
                                                                  email: channel.data,
                                                                  coordinatorDelegate: self)
        navigationController.pushViewController(confirmationController, animated: true)
    }

    func presentWarningPopup() {
        let warningController = WarningPopupFactory.make(backCompletion: { [weak self] in
            self?.endResetFlow()
        })
        navigationController.showPopup(warningController)
    }

    func endResetFlow() {
        navigationController.dismiss(animated: true)
    }
    
    func presentNextStep(_ step: PasswordResetStepData) {
        switch step.nextStep {
        case .code:
            presentAuthorizationCode()
        case .newPassword:
            presentNewPassword()
        case .done:
            presentSuccess()
        case .notApproved, .manualReview:
            presentValidationStatus()
        case .link:
            presentLinkConfirmation()
        default:
            break
        }
    }
}

// MARK: - Delegates
extension PasswordResetChannelsCoordinator: AuthorizationCodeCoordinatorDelegate {
    func authorizationCodeNextStep(_ stepData: PasswordResetStepData) {
        currentStep = stepData
        presentNextStep(stepData)
    }
}

extension PasswordResetChannelsCoordinator: ResetSuccessCoordinatorDelegate {
    func didTapActionButton() {
        navigationController.dismiss(animated: true)
    }
    
    func didTapCloseButton() {
        navigationController.dismiss(animated: true)
    }
}

extension PasswordResetChannelsCoordinator: NewPasswordCoordinatorDelegate {
    func newPasswordNextStep(_ stepData: PasswordResetStepData) {
        currentStep = stepData
        presentNextStep(stepData)
    }

    func backFromNewPassword(shouldShowWarning: Bool) {
        if shouldShowWarning {
            presentWarningPopup()
        } else {
            navigationController.popViewController(animated: true)
        }
    }
}

extension PasswordResetChannelsCoordinator: ValidationStatusCoordinatorDelegate {
    func validationStatusDidClose() {
        navigationController.dismiss(animated: true)
    }
}

extension PasswordResetChannelsCoordinator: LinkConfirmationCoordinatorDelegate {
    func backFromLinkConfirmation() {
        navigationController.popViewController(animated: true)
    }

    func linkConfirmationDone() {
        navigationController.dismiss(animated: true)
    }
}
