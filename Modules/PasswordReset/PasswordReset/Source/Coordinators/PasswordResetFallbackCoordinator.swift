import Foundation
import IdentityValidation
import UIKit

protocol PasswordResetFallbackCoordinatorDelegate: AnyObject {
    func resetFallbackFlow()
}

final class PasswordResetFallbackCoordinator: PasswordResetCoordinatorProtocol {
    var childViewController: [UIViewController] = []
    var viewController: UIViewController?
    var childCoordinator: PasswordResetCoordinatorProtocol?
    private var identityValidationCoordinator: IDValidationCoordinatorProtocol?
    private weak var delegate: PasswordResetFallbackCoordinatorDelegate?

    let navigationController: UINavigationController
    let channelType: ChannelType
    var currentStep: PasswordResetStepData
    var userEmail: String?

    init(
        navigationController: UINavigationController,
        currentStep: PasswordResetStepData,
        channelType: ChannelType,
        delegate: PasswordResetFallbackCoordinatorDelegate?
    ) {
        self.navigationController = navigationController
        self.currentStep = currentStep
        self.channelType = channelType
        self.delegate = delegate
    }
    
    func start() {
        presentIntro()
    }
}

// MARK: - Private methods
private extension PasswordResetFallbackCoordinator {
    func presentIntro() {
        let infoVC = BiometricIntroFactory.make(coordinatorDelegate: self)
        navigationController.pushViewController(infoVC, animated: true)
    }

    func presentBiometric() {
        let biometricVC = BiometricFactory.make(resetId: currentStep.passwordResetId, coordinatorDelegate: self)
        biometricVC.modalPresentationStyle = .overCurrentContext
        navigationController.present(biometricVC, animated: false)
    }
        
    func presentEmailConfirmation() {
        let emailConfirmationVC = EmailConfirmationFactory.make(resetId: currentStep.passwordResetId, coordinatorDelegate: self)
        navigationController.pushViewController(emailConfirmationVC, animated: true)
    }
    
    func presentAuthorizationCode() {
        let authorizationVC = AuthorizationCodeFactory.make(
            channelType: channelType,
            resetId: currentStep.passwordResetId,
            coordinatorDelegate: self,
            emailString: userEmail)
        navigationController.pushViewController(authorizationVC, animated: true)
    }
    
    func presentValidationStatus() {
        let infoVC = ValidationStatusFactory.make(with: currentStep.nextStep, coordinatorDelegate: self)
        navigationController.pushViewController(infoVC, animated: true)
    }

    func resetFallbackFlow() {
        delegate?.resetFallbackFlow()
    }
    
    func presentSuccess() {
        let successVC = ResetSuccessFactory.make(method: .biometric, coordinatorDelegate: self)
        navigationController.pushViewController(successVC, animated: true)
    }

    func presentWarningPopup() {
        let warningController = WarningPopupFactory.make(backCompletion: { [weak self] in
            self?.resetFallbackFlow()
        })
        navigationController.showPopup(warningController)
    }

    func presentNewPassword() {
        let newPasswordVC = NewPasswordFactory.make(
            channelType: channelType,
            resetId: currentStep.passwordResetId,
            coordinatorDelegate: self,
            shouldShowPopupWarning: true
        )
        navigationController.pushViewController(newPasswordVC, animated: true)
    }
    
    func presentUserForm() {
        let formVC = UserFormFactory.make(resetId: currentStep.passwordResetId, coordinatorDelegate: self)
        navigationController.pushViewController(formVC, animated: true)
    }
    
    func presentNextStep(step: PasswordResetStep) {
        switch step {
        case .newEmail:
            presentEmailConfirmation()
        case .code:
            presentAuthorizationCode()
        case .notApproved, .manualReview:
            presentValidationStatus()
        case .selfie:
            presentBiometric()
        case .done:
            presentSuccess()
        case .newPassword:
            presentNewPassword()
        case .personalData:
            presentUserForm()
        default:
            break
        }
    }
}

// MARK: - Delegate methods

extension PasswordResetFallbackCoordinator: BiometricIntroCoordinatorDelegate {
    func biometricIntroGoBack() {
        navigationController.popViewController(animated: true)
    }
    
    func biometricIntroGoNextStep() {
        presentNextStep(step: currentStep.nextStep)
    }
}

extension PasswordResetFallbackCoordinator: EmailConfirmationCoordinatorDelegate {
    func emailConfirmationNextStep(_ stepData: PasswordResetStepData, userEmail: String) {
        currentStep = stepData
        self.userEmail = userEmail.lowercased()
        presentNextStep(step: stepData.nextStep)
    }
}

extension PasswordResetFallbackCoordinator: AuthorizationCodeCoordinatorDelegate {
    func authorizationCodeNextStep(_ stepData: PasswordResetStepData) {
        currentStep = stepData
        presentNextStep(step: stepData.nextStep)
    }
}

extension PasswordResetFallbackCoordinator: ResetSuccessCoordinatorDelegate {
    func didTapActionButton() {
        navigationController.dismiss(animated: true)
    }
    
    func didTapCloseButton() {
        navigationController.dismiss(animated: true)
    }
}

extension PasswordResetFallbackCoordinator: BiometricCoordinatorDelegate {
    func biometricController(_ controller: UIViewController?, showIdentity flowData: IdentityFlowData) {
        controller?.dismiss(animated: false)
        identityValidationCoordinator = IDValidationFlowCoordinator(
            from: navigationController,
            token: flowData.token,
            flow: flowData.flow
        ) { [weak self] status in
            self?.identityValidationCompletion(with: status)
        }
        identityValidationCoordinator?.start()
    }

    func identityValidationCompletion(with status: IDValidationStatus?) {
        guard status != nil else { return }
        let stepLoaderVC = StepLoaderFactory.make(resetId: currentStep.passwordResetId, coordinatorDelegate: self)
        navigationController.pushViewController(stepLoaderVC, animated: true)
    }

    func back(from controller: UIViewController?) {
        controller?.dismiss(animated: false)
    }
}

extension PasswordResetFallbackCoordinator: StepLoaderCoordinatorDelegate {
    func stepLoaderNextStep(_ stepData: PasswordResetStepData) {
        currentStep = stepData
        presentNextStep(step: stepData.nextStep)
    }

    func backFromStepLoader() {
        presentWarningPopup()
    }
}

extension PasswordResetFallbackCoordinator: ValidationStatusCoordinatorDelegate {
    func validationStatusDidClose() {
        navigationController.dismiss(animated: true)
    }
}

extension PasswordResetFallbackCoordinator: NewPasswordCoordinatorDelegate {
    func newPasswordNextStep(_ stepData: PasswordResetStepData) {
        currentStep = stepData
        presentNextStep(step: stepData.nextStep)
    }

    func backFromNewPassword(shouldShowWarning: Bool) {
        if shouldShowWarning {
            presentWarningPopup()
        } else {
            navigationController.popViewController(animated: true)
        }
    }
}

extension PasswordResetFallbackCoordinator: UserFormCoordinatorDelegate {
    func userFormNextStep(_ stepData: PasswordResetStepData) {
        currentStep = stepData
        presentNextStep(step: stepData.nextStep)
    }

    func backFromUserForm() {
        presentWarningPopup()
    }
}
