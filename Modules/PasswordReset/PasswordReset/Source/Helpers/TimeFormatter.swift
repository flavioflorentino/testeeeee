import Foundation

protocol TimeFormatterProtocol {
    func formatString(from interval: TimeInterval) -> String
}

struct TimeFormatter: TimeFormatterProtocol {
    func formatString(from interval: TimeInterval) -> String {
        let dateFormatter = DateComponentsFormatter()
        dateFormatter.allowedUnits = [.minute, .second]
        dateFormatter.zeroFormattingBehavior = .pad

        return dateFormatter.string(from: interval) ?? ""
    }
}
