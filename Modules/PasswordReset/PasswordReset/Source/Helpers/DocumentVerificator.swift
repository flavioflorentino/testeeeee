import Foundation

enum DocumentType {
    case cpf
}

struct DocumentVerificator {
    static let maskedCpfDocumentLength = 14
    
    func isComplete(_ document: String, documentType: DocumentType) -> Bool {
        switch documentType {
        case .cpf:
            return isCompleteCpf(document)
        }
    }
    
    func validateDocument(_ document: String, documentType: DocumentType) -> Bool {
        let onlyNumbers = document.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        
        switch documentType {
        case .cpf:
            return validateCpf(onlyNumbers)
        }
    }
    
    private func isCompleteCpf(_ cpf: String) -> Bool {
        let onlyNumbers = cpf.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        return onlyNumbers.count == 11
    }
    
    /// Verifies if the cpf document has only repeatedNumbers
    private func isRepeatedNumbersCpf(_ cpf: String) -> Bool {
        let onlyNumbers = cpf.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let characterSet = Set<Character>(onlyNumbers)
        return characterSet.count <= 1
    }
    
    private func validateCpf(_ cpf: String) -> Bool {
        guard isCompleteCpf(cpf), !isRepeatedNumbersCpf(cpf) else {
            return false
        }
        
        let index1 = cpf.index(cpf.startIndex, offsetBy: 9)
        let index2 = cpf.index(cpf.startIndex, offsetBy: 10)
        let index3 = cpf.index(cpf.startIndex, offsetBy: 11)
        let originalInitialDac = Int(cpf[index1..<index2])
        let originalFinalDac = Int(cpf[index2..<index3])
        
        var calcInitialDac = 0, calcFinalDac = 0
        
        for i in 0...8 {
            let start = cpf.index(cpf.startIndex, offsetBy: i)
            let end = cpf.index(cpf.startIndex, offsetBy: i + 1)
            if let char = Int(cpf[start..<end]) {
                calcInitialDac += char * (10 - i)
                calcFinalDac += char * (11 - i)
            }
        }
        
        calcInitialDac %= 11
        calcInitialDac = calcInitialDac < 2 ? 0 : 11 - calcInitialDac
        
        calcFinalDac += calcInitialDac * 2
        calcFinalDac %= 11
        calcFinalDac = calcFinalDac < 2 ? 0 : 11 - calcFinalDac
        
        return calcInitialDac == originalInitialDac && calcFinalDac == originalFinalDac
    }
}
