import Foundation

protocol PasswordResetTimerDelegate: AnyObject {
    func timerTic()
}

protocol PasswordResetTimerProtocol {
    var delegate: PasswordResetTimerDelegate? { get set }
    func start()
    func stop()
}

final class PasswordResetTimer: PasswordResetTimerProtocol {
    private let timeInterval: TimeInterval = 1
    private let repeats = true

    private(set) var timer: Timer?
    weak var delegate: PasswordResetTimerDelegate?

    func start() {
        configureTimer()
        timer?.fire()
    }

    func stop() {
        timer?.invalidate()
    }
}

private extension PasswordResetTimer {
    func configureTimer() {
        timer = Timer.scheduledTimer(
            timeInterval: timeInterval,
            target: self,
            selector: #selector(runTimedCode),
            userInfo: nil,
            repeats: repeats
        )
    }

    @objc
    func runTimedCode() {
        delegate?.timerTic()
    }
}
