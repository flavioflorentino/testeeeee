import Foundation
import UI
import UIKit

protocol PasswordResetInfoViewDelegate: AnyObject {
    func actionButtonTapped()
}

final class PasswordResetInfoView: UIView {
    weak var delegate: PasswordResetInfoViewDelegate?
    
    private lazy var infoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var actionButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.BiometricIntro.buttonTitle, for: .normal)
        button.addTarget(self, action: #selector(didTapAction), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()
    
    private lazy var centerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = Spacing.base03
        return stackView
    }()
    
    init() {
        super.init(frame: .zero)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureView(with image: UIImage, title: String, description: String, buttonTitle: String) {
        infoImageView.image = image
        titleLabel.text = title
        descriptionLabel.text = description
        actionButton.setTitle(buttonTitle, for: .normal)
    }
    
    private func addComponents() {
        addSubview(centerStackView)
        centerStackView.addArrangedSubview(infoImageView)
        centerStackView.addArrangedSubview(titleLabel)
        centerStackView.addArrangedSubview(descriptionLabel)
        centerStackView.addArrangedSubview(actionButton)
    }
    
    private func layoutComponents() {
        centerStackView.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base04)
            $0.trailing.equalToSuperview().offset(-Spacing.base04)
            $0.centerY.equalToSuperview()
        }
        
        actionButton.snp.makeConstraints {
            $0.leading.equalTo(centerStackView.snp.leading)
            $0.trailing.equalTo(centerStackView.snp.trailing)
        }
    }
    
    private func setup() {
        backgroundColor = Colors.backgroundPrimary.change(.dark, to: Colors.backgroundSecondary.darkColor).color
        addComponents()
        layoutComponents()
    }
    
    @objc
    private func didTapAction() {
        delegate?.actionButtonTapped()
    }
}
