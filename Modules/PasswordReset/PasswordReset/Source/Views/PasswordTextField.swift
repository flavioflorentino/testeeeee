import UI
import UIKit

private enum State {
    case secure
    case unsecure

    var buttonImage: UIImage {
        switch self {
        case .secure:
            return Assets.eyeSlash.image
        case .unsecure:
            return Assets.eye.image
        }
    }

    var isSecureTextEntry: Bool {
        self == .secure
    }
}

final class PasswordTextField: ErrorHintFloatingTextField {
    private var state: State = .secure

    private lazy var eyeButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.eyeSlash.image, for: .normal)
        button.addTarget(self, action: #selector(didTapEyeButton), for: .touchUpInside)
        return button
    }()

    override func configureViews() {
        super.configureViews()
        configureTextFieldForSecureEntry(with: eyeButton)
    }
}

@objc
private extension PasswordTextField {
    func didTapEyeButton() {
        state = state == .secure ? .unsecure : .secure

        eyeButton.setImage(state.buttonImage, for: .normal)
        isSecureTextEntry = state.isSecureTextEntry
    }
}
