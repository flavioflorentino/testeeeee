import UI
import UIKit

private extension ErrorHintFloatingTextField.Layout {
    enum Size {
        static let errorLabel: CGFloat = 15
    }
}

class ErrorHintFloatingTextField: UIView, ViewConfiguration {
    fileprivate enum Layout { }

    var textDidChange: ((_ text: String?) -> Void)?
    var editingDidBegin: (() -> Void)?
    var editingDidEnd: (() -> Void)?

    var text: String? {
        floatingTextField.text
    }

    var isSecureTextEntry: Bool = false {
        didSet {
            floatingTextField.isSecureTextEntry = isSecureTextEntry
        }
    }

    var keyboardType: UIKeyboardType = .default {
        didSet {
            floatingTextField.keyboardType = keyboardType
        }
    }

    var returnKeyType: UIReturnKeyType = .default {
        didSet {
            floatingTextField.returnKeyType = returnKeyType
        }
    }

    private var textFieldMasker: TextFieldMasker?

    private lazy var floatingTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.defaultForm()
        textField.autocorrectionType = .no
        textField.placeholderFont = Typography.bodyPrimary().font()
        textField.selectedTitleColor = Colors.grayscale400.color
        textField.font = Typography.bodyPrimary().font()
        textField.textColor = Colors.grayscale700.color
        textField.addTarget(self, action: #selector(textfieldEditingChanged), for: .editingChanged)
        textField.addTarget(self, action: #selector(textfieldEditingDidBegin), for: .editingDidBegin)
        textField.addTarget(self, action: #selector(textfieldEditingDidEnd), for: .editingDidEnd)
        textField.addTarget(self, action: #selector(textfieldEditingDidEndOnExit), for: .editingDidEndOnExit)
        return textField
    }()

    private lazy var errorLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle(type: .default))
            .with(\.textColor, .critical600())
        return label
    }()

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func buildViewHierarchy() {
        addSubview(floatingTextField)
        addSubview(errorLabel)
    }

    func setupConstraints() {
        floatingTextField.snp.makeConstraints {
            $0.leading.top.trailing.equalToSuperview()
        }

        errorLabel.snp.makeConstraints {
            $0.top.equalTo(floatingTextField.snp.bottom).offset(Spacing.base00)
            $0.leading.trailing.bottom.equalToSuperview()
            $0.height.greaterThanOrEqualTo(Layout.Size.errorLabel)
        }
    }

    func configureViews() {
        shouldShowError(false)
    }

    func configureTextFieldForSecureEntry(with rightView: UIView) {
        floatingTextField.rightViewMode = .always
        floatingTextField.rightView = rightView
        floatingTextField.keyboardType = .numberPad
        isSecureTextEntry = true
    }

    func setPlaceholder(_ placeholder: String) {
        floatingTextField.placeholder = placeholder
    }

    func setTextFieldMask(with descriptor: MaskDescriptor) {
        let mask = CustomStringMask(descriptor: descriptor)
        textFieldMasker = TextFieldMasker(textMask: mask)
        textFieldMasker?.bind(to: floatingTextField)
    }

    func shouldShowError(_ shouldShow: Bool, with message: String? = nil) {
        errorLabel.text = message
        errorLabel.isHidden = !shouldShow

        floatingTextField.lineColor = shouldShow ? Colors.critical400.color : Colors.grayscale400.color
        floatingTextField.selectedLineColor = shouldShow ? Colors.critical600.color : Colors.branding300.color
    }
}

@objc
private extension ErrorHintFloatingTextField {
    func textfieldEditingChanged() {
        textDidChange?(floatingTextField.text)
    }

    func textfieldEditingDidBegin() {
        editingDidBegin?()
    }

    func textfieldEditingDidEnd() {
        editingDidEnd?()
    }

    func textfieldEditingDidEndOnExit() {
        floatingTextField.resignFirstResponder()
    }
}
