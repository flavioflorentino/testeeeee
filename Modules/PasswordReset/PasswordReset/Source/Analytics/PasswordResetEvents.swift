import Foundation

enum ErrorEvent: String, PasswordResetEvent {
    case generic = "Generic Error"
    case blocked = "Blocked"
}

enum UserIdentificationEvent: String, PasswordResetEvent {
    case accessed = "Accessed"
    case invalidCpf = "Invalid CPF"
    case error = "Error CPF"
    case success = "Identification Success"
}

enum ResetChannelsEvent: String, PasswordResetEvent {
    case selectChannel = "Chosen Method"
}

enum AuthorizationCodeEvent: String, PasswordResetEvent {
    case appear = "Authorization Code"
    case success = "Authorization Code Success"
    case channelError = "Error Method"
    case invalidCode = "Invalid Code"
    case attemptLimit = "Attempt Limit"
    case resend = "Resent Code"
}

enum NewPasswordEvent: String, PasswordResetEvent {
    case appear = "New Password"
    case matchError = "Invalid Password"
}

enum BiometricEvent: String, PasswordResetEvent {
    case success = "Identity Validation"
    case rejected = "Rejected"
}

enum ResetSuccessEvent: String, PasswordResetEvent {
    case appear = "New Password Success"
}

enum FallbackEvent: String, PasswordResetEvent {
    case intro = "Fallback Intro"
    case newEmail = "New Email"
    case newEmailError = "New Email Error"
}

enum WaitingAnalysisEvent: String, PasswordResetEvent {
    case formSuccess = "Form Success"
}

enum StepLoaderEvent: String, PasswordResetEvent {
    case appear = "Identity Validation Success"
}

enum LinkConfirmationEvent: String, PasswordResetEvent {
    case appear = "Email Sent"
    case genericError = "Email Sent Generic Error"
    case blockedError = "Email Sent Blocked"
}

enum LinkErrorEvent: String, PasswordResetEvent {
    case appear = "Email Error"
}
