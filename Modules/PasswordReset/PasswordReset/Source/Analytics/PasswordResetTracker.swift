import AnalyticsModule
import Foundation

protocol PasswordResetEvent {
    var name: String { get }
}

extension PasswordResetEvent where Self: RawRepresentable, Self.RawValue == String {
    var name: String { rawValue }
}

struct PasswordResetTracker: AnalyticsKeyProtocol {
    var method: ChannelType?
    var eventType: PasswordResetEvent
    var errorCode: PasswordResetErrorCode?

    private var properties: [String: Any] {
        var propertiesDictionary = [String: Any]()

        if let method = self.method {
            propertiesDictionary["method"] = method == .biometric ? "FALLBACK" : method.rawValue
        }

        if let error = errorCode {
            propertiesDictionary["error_type"] = error.trackableCode
        }
        
        propertiesDictionary["authenticated"] = PasswordResetSetup.authInstance?.isAuthenticated ?? false

        return propertiesDictionary
    }

    private var name: String { eventType.name }

    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase, .appsFlyer]
    }

    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("Password Recovery - " + name, properties: properties, providers: providers)
    }
}
