import Foundation

// swiftlint:disable convenience_type
final class PasswordResetResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: PasswordResetResources.self).url(forResource: "PasswordResetResources", withExtension: "bundle") else {
            return Bundle(for: PasswordResetResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: PasswordResetResources.self)
    }()
}
