import Foundation
import FeatureFlag

public final class EventTrackerManager {
    static let instace = EventTrackerManager()

    private var trackerInstance: EventTracking?

    private init() {}

    public static func initialize(apiToken: ApiToken) {
        instace.trackerInstance = EventTracker(apiToken: apiToken)
    }

    public static var tracker: EventTracking {
        guard let tracker = instace.trackerInstance else {
            fatalError("Tracker instance has not been propertly initialized. Please call initialize(:) with a proper token.")
        }
        return tracker
    }
}
