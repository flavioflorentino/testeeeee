import Foundation
import FeatureFlag

typealias EventName = String
typealias IntegrationName = String

struct EventsController {
    typealias Dependencies = HasFeatureManager

    private let eventsFilter: EventsFilter
    private let integrations: [EventName: [IntegrationName: Bool]]
    private let screenEventNames = ["User Navigated", "Screen Viewed"]
    private let dependencies: Dependencies

    init(controller: EventsFilter = .init(type: .denyList, events: []),
         configurations: [EventConfiguration] = [],
         dependencies: Dependencies = EventTrackerDependencyContainer()) {
        self.dependencies = dependencies
        var integrationsMap: [EventName: [IntegrationName: Bool]] = [:]
        if !dependencies.featureManager.isActive(.isEventTrackerProxyEnabled) {
            for event in configurations {
                // Integration names come in snake_case and uppercased.
                // for the collect API to accept it we need to convert to camelCase.
                let list = event.integrations.map { ($0.camelCased(), true) }
                integrationsMap[event.name] = .init(uniqueKeysWithValues: list)
            }
        }

        self.integrations = integrationsMap
        self.eventsFilter = controller
    }

    func shouldTrackEvent(named eventName: EventName) -> Bool {
        let containsEvent = eventsFilter.events.contains(eventName)
        guard case .allowList = eventsFilter.type else {
            return !containsEvent
        }

        return containsEvent
    }

    func integrationsForEvent(named eventName: EventName) -> [IntegrationName: Bool] {
        guard !dependencies.featureManager.isActive(.isEventTrackerProxyEnabled) else {
            return [:]
        }

        guard let integrations = self.integrations[eventName] else {
            return ["all": true]
        }

        return integrations
    }

    func typeForEvent(named eventName: EventName) -> Event.EventType {
        screenEventNames.contains(eventName) ? .screen : .track
    }
}

private extension String {
    func camelCased() -> String {
        let replaced = capitalized.replacingOccurrences(
            of: "(\\w{0,1})_",
            with: "$1",
            options: .regularExpression,
            range: nil
        )
        return replaced.prefix(1).lowercased() + replaced.dropFirst()
    }
}
