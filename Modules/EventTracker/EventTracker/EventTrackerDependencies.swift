import Foundation
import Core
import FeatureFlag

protocol HasEventTrackingServicing {
    var service: EventTrackingServicing { get }
}

protocol HasLogger {
    var logger: Logging { get }
}

protocol HasTrackingQueue {
    var trackingQueue: Dispatching { get }
}

protocol HasMainQueue {
    var mainQueue: Dispatching { get }
}

protocol HasTiming {
    var timer: RepeatingTiming { get }
}

protocol HasCoreDataStore {
    var dataStore: CoreDataStoring { get }
}

typealias EventTrackerDependencies = HasKVStore
    & HasEventTrackingServicing
    & HasLogger
    & HasMainQueue
    & HasTrackingQueue
    & HasTiming
    & HasKeychainManager
    & HasFeatureManager
    & HasCoreDataStore

final class EventTrackerDependencyContainer: EventTrackerDependencies {
    lazy var service: EventTrackingServicing = {
        if featureManager.isActive(.isEventTrackerProxyEnabled) {
            return ProxiedEventTrackerService()
        } else {
            return EventTrackerService()
        }
    }()
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var keychain: KeychainManagerContract = KeychainManager()
    lazy var kvStore: KVStoreContract = KVStore()
    lazy var logger: Logging = Logger()
    lazy var trackingQueue: Dispatching = DispatchQueue(label: "com.picpay.eventtracker.queue", qos: .background)
    lazy var mainQueue: Dispatching = DispatchQueue.main
    lazy var timer: RepeatingTiming = RepeatingTimer()
    lazy var dataStore: CoreDataStoring = EventTrackerStore.shared
}
