import Foundation
import Core

enum KeychainKey: String, KeychainKeyable {
    case apiToken = "event_tracker_api_token"
}
