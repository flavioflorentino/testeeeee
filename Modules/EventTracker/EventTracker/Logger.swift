import Foundation

protocol Logging {
    func info(_ message: String)
    func error(_ message: String)
}

struct Logger: Logging {
    func info(_ message: String) {
        #if DEBUG
        print("\(Date()) EventTracker [INFO] - \(message)")
        #endif
    }
    func error(_ message: String) {
        #if DEBUG
        print("\(Date()) EventTracker [ERROR] - \(message)")
        #endif
    }
}
