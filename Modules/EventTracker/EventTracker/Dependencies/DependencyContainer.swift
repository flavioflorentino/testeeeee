import Core

typealias Dependencies = HasKeychainManager

final class DependencyContainer: Dependencies {
    lazy var keychain: KeychainManagerContract = KeychainManager()
}
