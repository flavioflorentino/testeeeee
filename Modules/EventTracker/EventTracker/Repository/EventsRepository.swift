import Foundation
import CoreData
import Core

typealias CompletionBlock<T> = (Result<T, Error>) -> Void

protocol EventRepositoryContract {
    func add(_ event: Event, completion: CompletionBlock<NoContent>?)
    func add(_ events: [Event], completion: CompletionBlock<NoContent>?)
    func getAll(completion: CompletionBlock<[Event]>)
    func removeAll(completion: CompletionBlock<NoContent>?)
}

final class EventsRepository: EventRepositoryContract {
    typealias Dependencies = HasCoreDataStore

    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    func add(_ events: [Event], completion: CompletionBlock<NoContent>? = nil) {
        let context = dependencies.dataStore.newBackgroundContext()
        context.performAndWait {
            do {
                for event in events {
                    try EventEntity.create(from: event, in: context)
                }
                try context.save()
                completion?(.success)
            } catch {
                completion?(.failure(error))
            }
        }
    }

    func add(_ event: Event, completion: CompletionBlock<NoContent>? = nil) {
        let context = dependencies.dataStore.newBackgroundContext()
        context.performAndWait {
            do {
                try EventEntity.create(from: event, in: context)
                try context.save()
                completion?(.success)
            } catch {
                completion?(.failure(error))
            }
        }
    }

    func getAll(completion: CompletionBlock<[Event]>) {
        let context = dependencies.dataStore.newBackgroundContext()
        context.performAndWait {
            let fetchAllRequest: NSFetchRequest<EventEntity> = EventEntity.fetchRequest()
            do {
                let entities = try context.fetch(fetchAllRequest)
                let events = try entities.map { (entity) throws in
                    try JSONDecoder().decode(Event.self, from: entity.eventData)
                }
                completion(.success(events))
            } catch {
                completion(.failure(error))
            }
        }
    }

    func removeAll(completion: CompletionBlock<NoContent>? = nil) {
        let context = dependencies.dataStore.newBackgroundContext()
        context.performAndWait {
            let batchDelete = NSBatchDeleteRequest(fetchRequest: EventEntity.fetchRequest())
            do {
                try context.execute(batchDelete)
                try context.save()

                completion?(.success)
            } catch {
                completion?(.failure(error))
            }
        }
    }
}
