import Foundation
import CoreData
import Core

protocol CoreDataStoring {
    var container: NSPersistentContainer { get }
    func newBackgroundContext() -> NSManagedObjectContext
}

extension CoreDataStoring {
    func newBackgroundContext() -> NSManagedObjectContext {
        container.newBackgroundContext()
    }
}

final class EventTrackerStore: CoreDataStoring {
    static let shared = EventTrackerStore()

    lazy var container: NSPersistentContainer = {
        let container = EventCoreDataPersistenceContainer()
        container.loadPersistentStores { _, error in
            if let error = error {
                print("Error loading EventTracker database store: \(error)")
            }
        }
        return container
    }()

    private init() { 
        // Intentionally left blank. Singleton Implementation
    }
}

final class EventCoreDataPersistenceContainer: NSPersistentContainer {
    private static let modelName = "EventTrackerDataModel"
    private static let databaseDirectoryName = "ET"

    convenience init() {
        self.init(name: EventCoreDataPersistenceContainer.modelName)
    }

    override class func defaultDirectoryURL() -> URL {
        super.defaultDirectoryURL().appendingPathComponent(databaseDirectoryName)
    }
}

@objc(EventEntity)
public class EventEntity: NSManagedObject {
    @NSManaged var createdAt: Date
    @NSManaged var eventData: Data
    
    @nonobjc
    public class func fetchRequest() -> NSFetchRequest<EventEntity> {
        NSFetchRequest<EventEntity>(entityName: String(describing: self))
    }
}

extension EventEntity {
    @discardableResult
    static func create(from event: Event, in context: NSManagedObjectContext) throws -> EventEntity {
        let entity = EventEntity(context: context)
        entity.createdAt = Date()
        entity.eventData = try JSONEncoder().encode(event)
        return entity
    }
}
