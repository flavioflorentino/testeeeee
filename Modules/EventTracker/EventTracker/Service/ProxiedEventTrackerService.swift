import Foundation
import Core

final class ProxiedEventTrackerService: EventTrackingServicing {
    func getConfiguration(completion: @escaping (Result<[EventConfiguration], ApiError>) -> Void) {
        let api = Api<[EventConfiguration]>(endpoint: EventTrackerEndpoint.configurations)
        execute(api, andThen: completion)
    }

    func getFilters(completion: @escaping (Result<EventsFilter, ApiError>) -> Void) {
        let api = Api<EventsFilter>(endpoint: EventTrackerEndpoint.controller)
        execute(api, andThen: completion)
    }

    func track(_ events: BatchEnvelope, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let endpoint = EventTrackerEndpoint.batch(events)
        let api = Api<NoContent>(endpoint: endpoint)
        execute(api, andThen: completion)
    }

    func track(_ event: TrackEvent, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let endpoint = EventTrackerEndpoint.track(event)
        let api = Api<NoContent>(endpoint: endpoint)
        execute(api, andThen: completion)
    }

    func trackAnonymously(_ event: TrackEvent, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        track(event, completion: completion)
    }

    func screen(_ event: ScreenEvent, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let endpoint = EventTrackerEndpoint.screen(event)
        let api = Api<NoContent>(endpoint: endpoint)
        execute(api, andThen: completion)
    }

    func screenAnonymously(_ event: ScreenEvent, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        screen(event, completion: completion)
    }

    private func execute<T>(_ api: Api<T>, andThen completion: @escaping (Result<T, ApiError>) -> Void) {
        api.execute { result in completion(result.map(\.model)) }
    }
}
