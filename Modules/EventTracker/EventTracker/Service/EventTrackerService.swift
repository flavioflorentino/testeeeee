import Foundation
import Core

protocol EventTrackingServicing {
    func getConfiguration(completion: @escaping (Result<[EventConfiguration], ApiError>) -> Void)
    func getFilters(completion: @escaping (Result<EventsFilter, ApiError>) -> Void)
    func track(_ events: BatchEnvelope, completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func track(_ event: TrackEvent, completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func trackAnonymously(_ event: TrackEvent, completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func screen(_ event: ScreenEvent, completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func screenAnonymously(_ event: ScreenEvent, completion: @escaping (Result<NoContent, ApiError>) -> Void)
}

final class EventTrackerService: EventTrackingServicing {
    func getConfiguration(completion: @escaping (Result<[EventConfiguration], ApiError>) -> Void) {
        let api = Api<[EventConfiguration]>(endpoint: EventControllerEndpoint.configurations)
        api.execute { result in
            completion(result.map(\.model))
        }
    }

    func getFilters(completion: @escaping (Result<EventsFilter, ApiError>) -> Void) {
        let api = Api<EventsFilter>(endpoint: EventControllerEndpoint.controller)
        api.execute { result in
            completion(result.map(\.model))
        }
    }
    
    func track(_ events: BatchEnvelope, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let endpoint = EventCollectorEndpoint.batch(events)
        let api = Api<NoContent>(endpoint: endpoint)
        api.execute { result in
            completion(result.map(\.model))
        }
    }

    func track(_ event: TrackEvent, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let endpoint = EventCollectorEndpoint.track(event)
        let api = Api<NoContent>(endpoint: endpoint)
        api.execute { result in
            completion(result.map(\.model))
        }
    }

    func trackAnonymously(_ event: TrackEvent, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let endpoint = EventCollectorEndpoint.trackAnonymously(event)
        let api = Api<NoContent>(endpoint: endpoint)
        api.execute { result in
            completion(result.map(\.model))
        }
    }

    func screen(_ event: ScreenEvent, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let endpoint = EventCollectorEndpoint.screen(event)
        let api = Api<NoContent>(endpoint: endpoint)
        api.execute { result in
            completion(result.map(\.model))
        }
    }

    func screenAnonymously(_ event: ScreenEvent, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let endpoint = EventCollectorEndpoint.screenAnonymously(event)
        let api = Api<NoContent>(endpoint: endpoint)
        api.execute { result in
            completion(result.map(\.model))
        }
    }
}
