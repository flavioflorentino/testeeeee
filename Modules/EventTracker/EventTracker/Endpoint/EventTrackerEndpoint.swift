import Foundation
import Core

enum EventTrackerEndpoint {
    case configurations
    case controller
    case screen(ScreenEvent)
    case track(TrackEvent)
    case batch(BatchEnvelope)
}

extension EventTrackerEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .configurations:
            return "event-controller/configurations"
        case .controller:
            return "event-controller/controller"
        case .track:
            return "event-collector/track"
        case .screen:
            return "event-collector/screen"
        case .batch:
            return "event-collector/anonymous/batch"
        }
    }

    var baseURL: URL {
        guard let apiGateway: String = Environment.get(EnvironmentKeys.eventTrackerApiProxyBaseURL),
              let apiUrl = URL(string: apiGateway) else {
            fatalError("Could not parse Event Tracker API Gateway url")
        }

        return apiUrl
    }

    var method: HTTPMethod {
        switch self {
        case .configurations, .controller:
            return .get
        default:
            return .post
        }
    }

    var customHeaders: [String: String] {
        let dependencies: HasKeychainManager = DependencyContainer()
        return ["x-event-key": dependencies.keychain.getData(key: KeychainKey.apiToken) ?? ""]
    }

    var body: Data? {
        switch self {
        case .screen(let event):
            return prepareBody(with: event)

        case .track(let event):
            return prepareBody(with: event)

        case .batch(let events):
            return prepareBody(with: events)

        default:
            return nil
        }
    }

    private func prepareBody<T: Encodable>(with payload: T) -> Data? {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .formatted(formatter)

        #if DEBUG
        encoder.outputFormatting = .prettyPrinted
        #endif

        return try? encoder.encode(payload)
    }
}
