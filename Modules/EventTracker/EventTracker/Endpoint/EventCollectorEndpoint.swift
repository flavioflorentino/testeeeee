import Foundation
import Core

enum EventCollectorEndpoint {
    case screen(ScreenEvent)
    case screenAnonymously(ScreenEvent)
    case track(TrackEvent)
    case trackAnonymously(TrackEvent)
    case batch(BatchEnvelope)
}

extension EventCollectorEndpoint: ApiEndpointExposable {
    typealias Dependencies = HasKeychainManager
    var path: String {
        switch self {
        case .track:
            return "event-collector/track"
        case .trackAnonymously:
            return "event-collector/anonymous/track"
        case .screen:
            return "event-collector/screen"
        case .screenAnonymously:
            return "event-collector/anonymous/screen"
        case .batch:
            return "event-collector/anonymous/batch"
        }
    }

    var method: HTTPMethod { .post }

    var isTokenNeeded: Bool {
        switch self {
        case .screenAnonymously, .trackAnonymously:
            return false
        default:
            return true
        }
    }

    var customHeaders: [String: String] {
        let dependencies: Dependencies = EventTrackerDependencyContainer()
        return [
            "x-event-key": dependencies.keychain.getData(key: KeychainKey.apiToken) ?? ""
        ]
    }

    var baseURL: URL {
        guard let apiGateway: String = Environment.get(EnvironmentKeys.eventTrackerApiBaseURL),
              let apiUrl = URL(string: apiGateway) else {
            fatalError("Could not parse Event Tracker API url")
        }
        return apiUrl
    }

    var body: Data? {
        switch self {
        case .screen(let event), .screenAnonymously(let event):
            return prepareBody(with: event)

        case .track(let event), .trackAnonymously(let event):
            return prepareBody(with: event)

        case .batch(let events):
            return prepareBody(with: events)
        }
    }

    private func prepareBody<T: Encodable>(with payload: T) -> Data? {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .formatted(formatter)

        #if DEBUG
        encoder.outputFormatting = .prettyPrinted
        #endif

        return try? encoder.encode(payload)
    }
}
