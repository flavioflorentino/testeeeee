import Foundation
import Core

enum EventControllerEndpoint {
    case configurations
    case controller
}

extension EventControllerEndpoint: ApiEndpointExposable {
    typealias Dependencies = HasKeychainManager
    var path: String {
        switch self {
        case .configurations:
            return "event-controller/configurations"
        case .controller:
            return "event-controller/controller"
        }
    }

    var customHeaders: [String: String] {
        let dependencies: Dependencies = EventTrackerDependencyContainer()
        return [
            "x-event-key": dependencies.keychain.getData(key: KeychainKey.apiToken) ?? ""
        ]
    }

    var baseURL: URL {
        guard let apiGateway: String = Environment.get(EnvironmentKeys.eventTrackerApiBaseURL),
              let apiUrl = URL(string: apiGateway) else {
            fatalError("Could not parse Event Tracker API url")
        }
        return apiUrl
    }
}
