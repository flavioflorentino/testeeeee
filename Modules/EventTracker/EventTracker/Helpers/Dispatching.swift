import Foundation

protocol Dispatching {
    func async(execute work: @escaping () -> Void)
}

extension DispatchQueue: Dispatching {
    func async(execute work: @escaping () -> Void) {
        async(group: nil, execute: work)
    }
}
