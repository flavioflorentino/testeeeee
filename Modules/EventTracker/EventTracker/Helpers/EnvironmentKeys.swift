import Foundation

enum EnvironmentKeys {
    static let eventTrackerApiBaseURL = "EVENT_TRACKER_API_URL"
    static let eventTrackerApiProxyBaseURL = "EVENT_TRACKER_API_PROXY_URL"
}
