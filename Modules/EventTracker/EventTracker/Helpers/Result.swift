import Foundation
import Core

extension Result where Success == NoContent {
    static var success: Self { .success(.init()) }
}
