protocol RepeatingTiming {
    func schedule(withInterval interval: TimeInterval, tickBlock block: @escaping () -> Void)
    func fire()
    func invalidate()
}

final class RepeatingTimer: RepeatingTiming {
    var timer: Timer?

    func schedule(withInterval interval: TimeInterval, tickBlock block: @escaping () -> Void) {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block: { _ in block() })
    }

    func fire() {
        timer?.fire()
    }

    func invalidate() {
        timer?.invalidate()
        timer = nil
    }
}
