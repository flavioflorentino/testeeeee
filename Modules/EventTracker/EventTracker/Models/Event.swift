import Foundation
import Core

extension AnyCodable: Equatable {
    public static func == (lhs: AnyCodable, rhs: AnyCodable) -> Bool {
        guard let lhsHashable = lhs.value as? AnyHashable,
              let rhsHashable = rhs.value as? AnyHashable else {
            return false
        }

        return lhsHashable == rhsHashable
    }
}

struct Event: Codable, Equatable {
    let keyType: String
    let distinctId: String
    let userId: String?
    let name: String
    let context: [String: AnyCodable]
    let properties: [String: AnyCodable]
    let integrations: [String: Bool]
    let createdAt: Date
    let eventType: EventType
}

extension Event {
    enum EventType: String, Codable {
        case screen
        case track
    }

    enum CodingKeys: String, CodingKey {
        case distinctId = "anonymousId"
        case name = "event"
        case keyType, userId, context, properties, integrations, createdAt, eventType
    }
}

extension Event: CustomDebugStringConvertible {
    var debugDescription: String {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted

        if let data = try? encoder.encode(self),
           let description = String(data: data, encoding: .utf8) {
            return description
        } else {
            return "{\n\tname: \(name),\n\tcontext: \(context),\n\tproperties: \(properties)\n}"
        }
    }
}
