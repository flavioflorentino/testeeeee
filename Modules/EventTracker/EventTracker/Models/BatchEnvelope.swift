import Foundation

struct BatchEnvelope: Encodable {
    let sentAt: Date
    let batch: [BatchEventEnvelope]
}
