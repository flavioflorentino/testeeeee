import Foundation

struct EventsFilter: Codable {
    let type: FilterType
    let events: Set<String>
}

extension EventsFilter {
    enum FilterType: String, Codable {
        case denyList = "BLACKLIST"
        case allowList = "WHITELIST"
    }

    enum CodingKeys: String, CodingKey {
        case type = "event_list_type"
        case events = "event_list"
    }
}
