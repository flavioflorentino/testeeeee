import Foundation

struct EventConfiguration: Decodable {
    let id: String
    let name: String
    let type: String
    let integrations: [String]
}

extension EventConfiguration {
    private enum CodingKeys: String, CodingKey {
        case id, name, integrations
        case type = "event_type"
    }
}
