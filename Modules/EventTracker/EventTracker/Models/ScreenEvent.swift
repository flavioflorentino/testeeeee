import Foundation
import Core

struct ScreenEvent: Codable {
    let keyType: String
    let distinctId: String
    let userId: String?
    let name: String
    let context: [String: AnyCodable]
    let properties: [String: AnyCodable]
    let integrations: [String: Bool]
    let createdAt: Date
}

extension ScreenEvent {
    private enum CodingKeys: String, CodingKey {
        case distinctId = "anonymousId"
        case keyType, userId, name, context, properties, integrations, createdAt
    }
}

extension ScreenEvent: CustomDebugStringConvertible {
    var debugDescription: String {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted

        if let data = try? encoder.encode(self),
           let description = String(data: data, encoding: .utf8) {
            return description
        } else {
            return "{ name: \(name),context: \(context), properties: \(properties) }"
        }
    }
}
