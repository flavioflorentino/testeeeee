import Core
import Foundation
import UIKit

public enum ApiToken {
    case consumer(String)
    case seller(String)

    var token: String {
        switch self {
        case .consumer(let token),
             .seller(let token):
            return token
        }
    }

    var keyType: String {
        switch self {
        case .consumer:
            return "CONSUMER"
        case .seller:
            return "SELLER"
        }
    }
}

public enum UserId {
    case consumerId(String)
    case sellerId(String)

    func getId() -> String {
        switch self {
        case .consumerId(let id),
             .sellerId(let id):
            return id
        }
    }
}

public enum EventFlushInterval {
    case immediate
    case interval(TimeInterval)
}

public protocol EventTracking {
    var distinctId: String { get }
    var userId: UserId? { get }
    var commonProperties: [String: AnyCodable] { get set }
    var contextPropertyProviders: [ContextPropertyProviding] { get set }
    var flushInterval: EventFlushInterval { get set }
    func track(eventName: String, properties: [String: AnyCodable])
    func identify(with id: UserId)
    func flush()
    func reset()
}

public final class EventTracker: EventTracking {
    private let defaultFlushInterval: TimeInterval = 30
    private let dependencies: EventTrackerDependencies
    private let keyType: String
    private var eventRepository: EventsRepository
    private var controller: EventsController

    public private(set) var userId: UserId?
    public private(set) var distinctId: String = UUID().uuidString

    public var contextPropertyProviders: [ContextPropertyProviding]
    public var commonProperties: [String: AnyCodable] = [:]
    public var flushInterval: EventFlushInterval = .immediate {
        didSet {
            switch (oldValue, flushInterval) {
            case (_, .immediate):
                stopFlushTimer()
            case let (.interval(oldInterval), .interval(newInterval)) where oldInterval != newInterval:
                startFlushTimer(interval: newInterval)
            case let (.immediate, .interval(newInterval)):
                startFlushTimer(interval: newInterval)
            default:
                break
            }
        }
    }

    init(apiToken: ApiToken, dependencies: EventTrackerDependencies = EventTrackerDependencyContainer()) {
        self.keyType = apiToken.keyType
        self.dependencies = dependencies
        self.contextPropertyProviders = [DefaultContextPropertiesProvider()]
        self.controller = EventsController()
        self.eventRepository = EventsRepository(dependencies: dependencies)
        self.distinctId = recoverDistinctId()

        dependencies.keychain.set(key: KeychainKey.apiToken, value: apiToken.token)
        dependencies.trackingQueue.async(execute: loadController)
    }

    public func track(eventName name: String, properties: [String: AnyCodable]) {
        // userId and distinctId are capture on the same moment of the event creation and then dispatched.
        // This is needed because a call to `reset` is sync and will immediatly reset those values.
        // This will garantee an event has no wrong owner when its dispatched because the values are copied.
        dependencies.trackingQueue.async { [weak self, userId, distinctId] in
            self?.dispatchEvent(
                userId: userId,
                anonymousId: distinctId,
                eventName: name,
                properties: properties
            )
        }
    }

    public func identify(with id: UserId) {
        userId = id
        dependencies.trackingQueue.async { [weak self] in
            self?.loadController()
        }
    }

    public func flush() {
        dependencies.trackingQueue.async { [weak self] in
            self?.flushEventBuffer()
        }
    }

    public func reset() {
        userId = nil
        resetDistinctId()
        dependencies.trackingQueue.async { [weak self] in
            self?.loadController()
        }
    }
}

// MARK: - Tracking methods.
private extension EventTracker {
    private func dispatchEvent(userId: UserId?,
                               anonymousId: String,
                               eventName name: String,
                               properties: [String: AnyCodable]) {
        guard controller.shouldTrackEvent(named: name) else {
            dependencies.logger.info("Event named \"\(name)\" is set to NOT be tracked")
            return
        }

        let mergedProperties = properties.merging(commonProperties, uniquingKeysWith: { left, _ in left })
        let contextProperties = getContextProperties()

        let event = Event(
            keyType: keyType,
            distinctId: anonymousId,
            userId: userId?.getId(),
            name: name,
            context: contextProperties,
            properties: mergedProperties,
            integrations: controller.integrationsForEvent(named: name),
            createdAt: Date(),
            eventType: controller.typeForEvent(named: name)
        )

        switch (flushInterval, event.eventType) {
        case (.immediate, .screen):
            immediateTrackScreen(event)
        case (.immediate, .track):
            immediateTrackEvent(event)
        case (.interval, _):
            eventRepository.add(event)
        }
    }

    private func immediateTrackEvent(_ event: Event) {
        let event = TrackEvent(
            keyType: event.keyType,
            distinctId: event.distinctId,
            userId: event.userId,
            name: event.name,
            context: event.context,
            properties: event.properties,
            integrations: event.integrations,
            createdAt: event.createdAt
        )

        let trackingService = event.userId != nil
            ? dependencies.service.track
            : dependencies.service.trackAnonymously

        trackingService(event) { [weak self] result in
            switch result {
            case .success:
                self?.dependencies.logger.info("Sent event: \(event)")
            case .failure(let error):
                self?.dependencies.logger.error("Failed tracking event: \(event) with error \(error)")
            }
        }
    }

    private func immediateTrackScreen(_ event: Event) {
        let event = ScreenEvent(
            keyType: event.keyType,
            distinctId: event.distinctId,
            userId: event.userId,
            name: event.name,
            context: event.context,
            properties: event.properties,
            integrations: event.integrations,
            createdAt: event.createdAt
        )

        let trackingService = event.userId != nil
            ? dependencies.service.screen
            : dependencies.service.screenAnonymously

        trackingService(event) { [weak self] result in
            switch result {
            case .success:
                self?.dependencies.logger.info("Sent event: \(event)")
            case .failure(let error):
                self?.dependencies.logger.error("Failed tracking event: \(event) with error \(error)")
            }
        }
    }

    private func flushEventBuffer() {
        eventRepository.getAll { result in
            guard case let .success(events) = result,
                  events.isNotEmpty else { return }

            let wrappedEvents = events.map(BatchEventEnvelope.init(custom:))
            let batch = BatchEnvelope(sentAt: Date(), batch: wrappedEvents)

            let synchronizer = DispatchGroup()
            synchronizer.enter()
            dependencies.service.track(batch) { [weak self, events] result in
                defer { synchronizer.leave() }

                switch result {
                case .success:
                    self?.dependencies.logger.info("Sent events: \(events)")

                case .failure(let error):
                    self?.dependencies.logger.error("Failed tracking events: \(events) with error \(error)")
                }
            }

            eventRepository.removeAll()
            synchronizer.wait()
        }
    }
}

// MARK: - Loading & Initialization
private extension EventTracker {
    private func getContextProperties() -> [String: AnyCodable] {
        var properties: [String: AnyCodable] = [:]
        for provider in contextPropertyProviders {
            properties.merge(provider.properties, uniquingKeysWith: { left, _ in left })
        }
        return properties
    }

    private func resetDistinctId() {
        let key = UUID().uuidString
        self.distinctId = key
        dependencies.kvStore.set(value: key, with: KVKeys.distinctId)
    }

    private func recoverDistinctId() -> String {
        guard let key = dependencies.kvStore.stringFor(KVKeys.distinctId) else {
            let key = UUID().uuidString
            dependencies.kvStore.set(value: key, with: KVKeys.distinctId)
            return key
        }
        return key
    }

    private func loadController() {
        let group = DispatchGroup()
        let logger = dependencies.logger
        var eventConfigs: [EventConfiguration] = []
        var filter: EventsFilter = .init(type: .denyList, events: [])

        group.enter()
        dependencies.service.getConfiguration { result in
            defer { group.leave() }

            switch result {
            case .success(let configs):
                eventConfigs = configs
            case .failure(let error):
                logger.info("Failed to load configurations with error \(error)")
            }
        }

        group.enter()
        dependencies.service.getFilters { result in
            defer { group.leave() }

            switch result {
            case .success(let controller):
                filter = controller
            case .failure(let error):
                logger.info("Failed to load filters with error \(error)")
            }
        }

        group.wait()
        controller = .init(controller: filter, configurations: eventConfigs)
    }
}

// MARK: - Flush timer & time control
private extension EventTracker {
    private func stopFlushTimer() {
        dependencies.mainQueue.async { [weak self] in
            self?.dependencies.timer.invalidate()
        }
    }

    private func startFlushTimer(interval: TimeInterval) {
        dependencies.mainQueue.async { [weak self] in
            guard let self = self else { return }
            self.dependencies.timer.schedule(
                withInterval: interval,
                tickBlock: self.timerDidTick
            )
        }
    }

    private func timerDidTick() {
        flush()
    }
}

extension EventTracker {
    enum KVKeys: KVKeyContract {
        case distinctId

        var description: String {
            switch self {
            case .distinctId:
                return "event_tracker_distinct_id"
            }
        }
    }
}
