import Foundation
import Core

public protocol ContextPropertyProviding {
    var properties: [String: AnyCodable] { get }
}

struct DefaultContextPropertiesProvider: ContextPropertyProviding {
    private let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") ?? ""
    private let deviceModel = UIDevice.current.deviceModel
    private let installId = UIDevice.current.installationId ?? ""
    private let os = UIDevice.current.systemName
    private let osVersion = UIDevice.current.systemVersion

    var properties: [String: AnyCodable] {
        [
            "app_version": .init(value: versionNumber),
            "device_model": .init(value: deviceModel),
            "installation_id": .init(value: installId),
            "os": .init(value: os),
            "os_version": .init(value: osVersion),
            "library": .init(value: "ios"),
            "libraryVersion": .init(value: versionNumber)
        ]
    }
}
