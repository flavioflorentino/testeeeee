import Foundation
import XCTest
@testable import EventTracker

final class EventsRespositoryTests: XCTestCase {
    private let dataStoreMock = EventTrackerCoreDataStoreMock()
    private let event = Event(
        keyType: "keyType",
        distinctId: "distictId",
        userId: nil,
        name: "name",
        context: ["aKey": .init(value: "aValue")],
        properties: ["aKey": .init(value: "aValue")],
        integrations: ["aIntegration": true],
        createdAt: Date(),
        eventType: .track
    )

    private lazy var dependencies: EventTrackerDependencyContainer = {
        let dependencies = EventTrackerDependencyContainer()
        dependencies.dataStore = dataStoreMock
        return dependencies
    }()

    private lazy var sut: EventsRepository = {
        EventsRepository(dependencies: dependencies)
    }()

    func testRepository_WhenAddedSingleEvent_EventMustBeStored() {
        sut.add(event)

        sut.getAll { result in
            guard case let .success(events) = result else {
                XCTFail("Não foi possível ler registros do banco.")
                return
            }

            let aEvent = events[0]
            print(aEvent == event)
            XCTAssertEqual(1, events.count)
            XCTAssertEqual(event, events[0])
        }
    }
    
    func testRepository_WhenAddedMultipleEvents_EventsMustBeStored() {
        sut.add([event, event])

        sut.getAll { result in
            guard case let .success(events) = result else {
                XCTFail("Não foi possível ler registros do banco.")
                return
            }

            XCTAssertEqual(2, events.count)
            XCTAssertEqual(event, events[0])
            XCTAssertEqual(event, events[1])
        }
    }

    func testRepository_WhenRemovingEvents_EventsMustBeRemoved() {
        sut.add([event, event])

        sut.removeAll()

        sut.getAll { result in
            guard case let .success(events) = result else {
                XCTFail("Não foi possível ler registros do banco.")
                return
            }

            XCTAssertEqual(0, events.count)
        }
    }
}
