import Foundation
import CoreData
@testable import EventTracker

final class EventTrackerCoreDataStoreMock: CoreDataStoring {
    private let modelName = "EventTrackerDataModel"
    lazy var container: NSPersistentContainer = {
        let container = EventCoreDataPersistenceContainer()

        // Why we don't use NSInMemoryStoreType you say...
        // That's because of the recommandation given at this talk from WWDC https://developer.apple.com/videos/play/wwdc2018/224/
        // which states that this is the recommended way to build an inMemory coredata database.
        let storeDescription = NSPersistentStoreDescription()
        storeDescription.url = URL(fileURLWithPath: "/dev/null")
        container.persistentStoreDescriptions = [storeDescription]

        container.loadPersistentStores { _, error in
            if let error = error {
                print("Error loading EventTracker database store: \(error)")
            }
        }
        return container
    }()

    init() {}
}
