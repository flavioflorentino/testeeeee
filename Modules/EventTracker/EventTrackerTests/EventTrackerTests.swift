import XCTest
import Core
@testable import EventTracker

final class DispatchQueueMock: Dispatching {
    func async(execute work: @escaping () -> Void) {
        work()
    }
}

final class RepeatingTimerMock: RepeatingTiming {
    private(set) var scheduleCount = 0
    private(set) var invalidateCount = 0
    private(set) var lastSetInterval: TimeInterval?
    private(set) var lastSetBlock: (() -> Void)?

    func schedule(withInterval interval: TimeInterval, tickBlock block: @escaping () -> Void) {
        scheduleCount += 1
        lastSetInterval = interval
        lastSetBlock = block
    }

    func fire() {
        lastSetBlock?()
    }

    func invalidate() {
        invalidateCount += 1
    }
}

final class EventTrackerServiceMock: EventTrackingServicing {
    var getConfigurationResult: Result<[EventConfiguration], ApiError>?
    var getFilersResult: Result<EventsFilter, ApiError>?
    var trackResult: Result<NoContent, ApiError>?

    private(set) var lastTrackedEvent: TrackEvent?
    private(set) var lastScreenEvent: ScreenEvent?
    private(set) var lastTrackedEventBatch: BatchEnvelope?
    private(set) var getConfigurationCallCount = 0
    private(set) var getFilterCallCount = 0
    private(set) var trackCallCount = 0
    private(set) var trackAnonymoulsyCallCount = 0
    private(set) var screenCallCount = 0
    private(set) var screenAnonymoulsyCallCount = 0
    private(set) var trackBatchCallCount = 0

    func getConfiguration(completion: @escaping (Result<[EventConfiguration], ApiError>) -> Void) {
        getConfigurationCallCount += 1
        completion(getConfigurationResult ?? .failure(.cancelled))
    }

    func getFilters(completion: @escaping (Result<EventsFilter, ApiError>) -> Void) {
        getFilterCallCount += 1
        completion(getFilersResult ?? .failure(.cancelled))
    }

    func track(_ event: BatchEnvelope, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        trackBatchCallCount += 1
        lastTrackedEventBatch = event
        completion(trackResult ?? .success(NoContent()))
    }

    func track(_ event: TrackEvent, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        trackCallCount += 1
        lastTrackedEvent = event
        completion(trackResult ?? .success(NoContent()))
    }

    func trackAnonymously(_ event: TrackEvent, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        trackAnonymoulsyCallCount += 1
        lastTrackedEvent = event
        completion(trackResult ?? .success(NoContent()))
    }

    func screen(_ event: ScreenEvent, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        screenCallCount += 1
        lastScreenEvent = event
        completion(trackResult ?? .success(NoContent()))
    }

    func screenAnonymously(_ event: ScreenEvent, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        screenAnonymoulsyCallCount += 1
        lastScreenEvent = event
        completion(trackResult ?? .success(NoContent()))
    }
}

final class PropertyProviderMock: ContextPropertyProviding {
    private var injectedProperties: [String: AnyCodable]
    private(set) var propertiesCallCount: Int = 0

    init(properties: [String: AnyCodable]) {
        injectedProperties = properties
    }

    var properties: [String: AnyCodable] {
        propertiesCallCount += 1
        return injectedProperties
    }
}

final class EventTrackerTests: XCTestCase {
    private let serviceMock = EventTrackerServiceMock()
    private let queueMock = DispatchQueueMock()
    private let kvStoreMock = KVStoreMock()
    private let timerMock = RepeatingTimerMock()

    private lazy var dependencies: EventTrackerDependencyContainer = {
        let dependencies = EventTrackerDependencyContainer()
        dependencies.kvStore = kvStoreMock
        dependencies.trackingQueue = queueMock
        dependencies.mainQueue = queueMock
        dependencies.service = serviceMock
        dependencies.timer = timerMock
        dependencies.dataStore = EventTrackerCoreDataStoreMock()
        return dependencies
    }()

    private lazy var sut: EventTracking = {
        EventTracker(apiToken: .consumer("token"), dependencies: dependencies)
    }()

    func testTrack_WhenSendingCommonEvent_ShoudTrackEvent() {
        sut.track(eventName: "Common Event", properties: [:])

        XCTAssertEqual(serviceMock.trackAnonymoulsyCallCount, 1)
    }

    func testTrack_WhenSendingScreenEventsAnom_ShoudTrackScreenEvent() {
        let screenEvents = ["User Navigated", "Screen Viewed"]

        for name in screenEvents {
            sut.track(eventName: name, properties: [:])
        }

        XCTAssertEqual(serviceMock.screenAnonymoulsyCallCount, screenEvents.count)
    }

    func testTrack_WhenSendingEventOnDenyList_ShoudNotTrackEvent() {
        serviceMock.getFilersResult = .success(
            .init(type: .denyList, events: ["Filtered Event"])
        )

        sut.track(eventName: "Filtered Event", properties: [:])

        XCTAssertEqual(serviceMock.trackCallCount, 0)
    }

    func testTrack_WhenSendingEventNOTOnDenyList_ShoudTrackEvent() {
        serviceMock.getFilersResult = .success(
            .init(type: .denyList, events: ["Denyied Event"])
        )

        sut.track(eventName: "Non Denyied Event", properties: [:])

        XCTAssertEqual(serviceMock.trackAnonymoulsyCallCount, 1)
    }

    func testTrack_WhenSendindEventOnAllowList_ShoudTrackEvent() {
        serviceMock.getFilersResult = .success(
            .init(type: .allowList, events: ["Allowed Event"])
        )

        sut.track(eventName: "Allowed Event", properties: [:])

        XCTAssertEqual(serviceMock.trackAnonymoulsyCallCount, 1)
    }

    func testTrack_WhenSendindEventNOTOnAllowList_ShoudNotTrackEvent() {
        serviceMock.getFilersResult = .success(
            .init(type: .allowList, events: ["Allowed Event"])
        )

        sut.track(eventName: "Not Allowed Event", properties: [:])

        XCTAssertEqual(serviceMock.trackCallCount, 0)
    }

    func testTrack_WhenSendingEventAuthenticated_ShoudTrackEvent() {
        sut.identify(with: .consumerId("consumerId"))
        sut.track(eventName: "Some Event", properties: [:])
        sut.flush()

        XCTAssertEqual(serviceMock.trackCallCount, 1)
        XCTAssertEqual(serviceMock.lastTrackedEvent?.userId, "consumerId")
    }

    func testTrack_WhenSendingScreenEventsAuthenticated_ShoudTrackScreenEvent() {
        sut.identify(with: .consumerId("consumerId"))
        let screenEvents = ["User Navigated", "Screen Viewed"]

        for name in screenEvents {
            sut.track(eventName: name, properties: [:])
        }

        XCTAssertEqual(serviceMock.screenCallCount, screenEvents.count)
        XCTAssertEqual(serviceMock.lastScreenEvent?.userId, "consumerId")
    }

    func testTrack_WhenUsingCommonProperties_ShoudMergePropertiesToEvent() {
        sut.commonProperties["mergedPropertie"] = .init(value: "mergedValue")
        sut.track(eventName: "event", properties: ["nonMergedPropertie": .init(value: "nonMergedValue")])

        if let event = serviceMock.lastTrackedEvent {
            XCTAssertTrue(event.properties.keys.contains("mergedPropertie"))
            XCTAssertTrue(event.properties.keys.contains("nonMergedPropertie"))
        }
    }

    func testEventTracker_WhenCreated_ShouldReuseUniqueId() {
        var tracker = EventTracker(apiToken: .consumer(""), dependencies: dependencies)
        let firstId = tracker.distinctId

        tracker = EventTracker(apiToken: .consumer(""), dependencies: dependencies)
        let secondId = tracker.distinctId

        XCTAssertEqual(firstId, secondId)
    }

    func testEventTracker_WhenCreatedFirstTime_ShouldGenerateUniqueId() {
        let currentId = sut.distinctId

        kvStoreMock.removeObjectFor(EventTracker.KVKeys.distinctId)
        let newInstance = EventTracker(apiToken: .consumer(""), dependencies: dependencies)
        let newId = newInstance.distinctId

        XCTAssertNotEqual(currentId, newId)
    }

    func testReset_WhenReseted_ShouldGenerateNewUniqueId() {
        let tracker = EventTracker(apiToken: .consumer(""), dependencies: dependencies)
        let firstId = tracker.distinctId

        tracker.reset()
        let secondId = tracker.distinctId

        XCTAssertNotEqual(firstId, secondId)
    }

    func testIdentify_WhenCalled_ShouldIdentifyUser() {
        XCTAssertNil(sut.userId)

        sut.identify(with: .consumerId("consumerId"))

        XCTAssertEqual(sut.userId?.getId(), "consumerId")
    }

    func testEventTracker_WhenReseted_ShouldGenerateDistinctId() {
        sut.identify(with: .consumerId("cunsumerId"))
        let currentId = sut.distinctId

        sut.reset()

        XCTAssertNotEqual(sut.distinctId, currentId)
        XCTAssertNil(sut.userId)
    }

    func testEventTracker_WithCustomContextPropertyProvider_ShouldAppendCustomContextProperties() throws {
        let customProperties: [String: AnyCodable] = ["propertyA": .init(value: "valueA")]
        let provider = PropertyProviderMock(properties: customProperties)

        sut.contextPropertyProviders.append(provider)
        sut.track(eventName: "event", properties: [:])

        XCTAssertEqual(provider.propertiesCallCount, 1)

        let lastEvent = try XCTUnwrap(serviceMock.lastTrackedEvent)
        XCTAssertTrue(lastEvent.context.keys.contains("propertyA"))
    }

    func testEventTracker_WhenFlushed_ShouldSendAllEventsInBuffer() throws {
        let eventNames = ["Event1", "Event2", "Event3"]

        sut.flushInterval = .interval(60)
        eventNames.forEach { sut.track(eventName: $0, properties: [:]) }
        sut.flush()

        let batch = try XCTUnwrap(serviceMock.lastTrackedEventBatch?.batch)
        XCTAssertEqual(serviceMock.trackBatchCallCount, 1)
        XCTAssertEqual(batch.count, 3)
        XCTAssertEqual(batch.map { $0.custom.name }, eventNames)
    }

    func testEventTracker_WhenChangedFlushIntervalToImmediate_ShouldStopTimer() {
        sut.flushInterval = .interval(60)

        sut.flushInterval = .immediate

        XCTAssertEqual(timerMock.invalidateCount, 1)
    }

    func testEventTracker_WhenChangedFlushInterval_ShouldReeschedulleTimer() {
        sut.flushInterval = .interval(60)

        sut.flushInterval = .interval(30)

        XCTAssertEqual(timerMock.scheduleCount, 2)
        XCTAssertEqual(timerMock.lastSetInterval, 30)
    }

    func testEventTracker_WhenTimerTick_ShouldFlushEvents() throws {
        sut.flushInterval = .interval(60)

        sut.track(eventName: "Event1", properties: [:])
        timerMock.fire()

        let batch = try XCTUnwrap(serviceMock.lastTrackedEventBatch?.batch)
        XCTAssertEqual(batch.count, 1)
        XCTAssertEqual(batch.map { $0.custom.name }, ["Event1"])
    }
}
