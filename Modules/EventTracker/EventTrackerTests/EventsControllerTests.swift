import XCTest
@testable import EventTracker

final class EventControllerTests: XCTestCase {
    func testShouldTrackEvent_WhenAllowDenyListEmpty_ShouldAllowEvent() {
        let sut = EventsController()

        XCTAssertTrue(sut.shouldTrackEvent(named: "UserNavigated"))
    }

    func testShoudTrackEvent_WhenInAllowList_ShouldAllowEvent() {
        let eventName = "User Navigated"
        let controller = EventsFilter(type: .allowList, events: [eventName])
        let sut = EventsController(controller: controller, configurations: [])

        XCTAssertTrue(sut.shouldTrackEvent(named: eventName))
    }

    func testShoudTrackEvent_WhenInDenyList_ShouldNotAllowEvent() {
        let eventName = "User Navigated"
        let controller = EventsFilter(type: .denyList, events: [eventName])
        let sut = EventsController(controller: controller, configurations: [])

        XCTAssertFalse(sut.shouldTrackEvent(named: eventName))
    }

    func testIntegrationsForEvent_WhenEventNotInList_ShouldIntegrateWithAll() {
        let eventName = "User Navigated"
        let configuration = EventConfiguration(id: "", name: eventName, type: "SCREEN", integrations: ["mixpanel", "firebase", "appsFlyer"])
        let controller = EventsFilter(type: .denyList, events: [])
        let sut = EventsController(controller: controller, configurations: [configuration])

        let integrations = sut.integrationsForEvent(named: "EventNotInList")

        XCTAssertEqual(integrations.count, 1)
        XCTAssertTrue(integrations.keys.contains("all"))
    }
    
    func testIntegrationsForEvent_WheneEventInList_ShouldReturnConfiguredIntegrations() {
        let eventName = "User Navigated"
        let configuration = EventConfiguration(id: "", name: eventName, type: "SCREEN", integrations: ["mixpanel", "firebase", "apps_flyer"])
        let controller = EventsFilter(type: .denyList, events: [])
        let sut = EventsController(controller: controller, configurations: [configuration])

        let integrations = sut.integrationsForEvent(named: eventName)

        XCTAssertEqual(integrations.count, 3)
        XCTAssertTrue(integrations.keys.contains("mixpanel"))
        XCTAssertTrue(integrations.keys.contains("firebase"))
        XCTAssertTrue(integrations.keys.contains("appsFlyer"))
    }

    func testIntegrationsForEvent_WheneEventInList_ShouldCamelCaseIntegrationNames() {
        let eventName = "User Navigated"
        let configuration = EventConfiguration(
            id: "",
            name: eventName,
            type: "SCREEN",
            integrations: ["UPPERCASED", "Capitalized", "camel_cased", "big_camel_cased", "_prefix_underlined", "sufix_underlined_"]
        )
        let controller = EventsFilter(type: .denyList, events: [])
        let sut = EventsController(controller: controller, configurations: [configuration])

        let integrations = sut.integrationsForEvent(named: eventName)

        XCTAssertEqual(integrations.count, 6)
        XCTAssertTrue(integrations.keys.contains("uppercased"))
        XCTAssertTrue(integrations.keys.contains("capitalized"))
        XCTAssertTrue(integrations.keys.contains("camelCased"))
        XCTAssertTrue(integrations.keys.contains("bigCamelCased"))
        XCTAssertTrue(integrations.keys.contains("prefixUnderlined"))
        XCTAssertTrue(integrations.keys.contains("sufixUnderlined"))
    }

    func testGetTypeForEvent_WhenEventIsScreen_ShoudReturnScreen() {
        let eventNames = ["User Navigated", "Screen Viewed"]
        let sut = EventsController()

        for event in eventNames {
            XCTAssertEqual(sut.typeForEvent(named: event), .screen)
        }
    }
}
