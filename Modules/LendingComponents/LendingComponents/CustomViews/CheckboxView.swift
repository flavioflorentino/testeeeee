import UI
import UIKit

extension CheckboxView.Layout {
    enum Insets {
        static let layoutMargins: EdgeInsets = .zero
    }
}

public final class CheckboxView: UIControl {
    fileprivate enum Layout { }
    
    public private(set) lazy var checkboxButton: CheckboxButton = {
        let button = CheckboxButton()
        button.setContentHuggingPriority(.required, for: .horizontal)
        button.addTarget(self, action: #selector(checkboxButtonValueChanged), for: .valueChanged)
        return button
    }()
    
    public private(set) lazy var textView: UITextView = {
        let textView = TextView()
        textView.dataDetectorTypes = .link
        textView.isScrollEnabled = false
        textView.setContentHuggingPriority(.defaultLow, for: .horizontal)
        textView.isEditable = false
        textView.backgroundColor = .clear
        textView.linkTextAttributes = [
            .foregroundColor: Colors.branding600.color,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        return textView
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [checkboxButton, textView])
        stackView.spacing = Spacing.base02
        stackView.alignment = .center
        return stackView
    }()
    
    override public var isSelected: Bool {
        get {
            checkboxButton.isSelected
        }
        set {
            checkboxButton.isSelected = newValue
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CheckboxView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(rootStackView)
    }
    
    public func setupConstraints() {
        rootStackView.snp.makeConstraints {
            $0.directionalEdges.equalTo(layoutMarginsGuide)
        }
    }
    
    public func configureViews() {
        compatibleLayoutMargins = Layout.Insets.layoutMargins
    }
}

@objc
extension CheckboxView {
    private func checkboxButtonValueChanged() {
        sendActions(for: .valueChanged)
    }
}
