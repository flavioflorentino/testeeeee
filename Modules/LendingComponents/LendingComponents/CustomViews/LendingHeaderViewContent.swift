import Foundation

public struct LendingHeaderViewContent: Equatable {
    public let image: UIImage?
    public let title: String
    public let description: NSAttributedString

    public init(image: UIImage?, title: String, description: NSAttributedString) {
        self.image = image
        self.title = title
        self.description = description
    }

    public init(image: UIImage?, title: String, description: String) {
        self.image = image
        self.title = title
        self.description = NSAttributedString(string: description)
    }
}
