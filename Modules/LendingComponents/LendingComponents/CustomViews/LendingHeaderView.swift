import UI
import UIKit
import AssetsKit

public class LendingHeaderView: UITableViewHeaderFooterView {
    private lazy var illustrationImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .center
        return imageView
    }()

    private lazy var headlineLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
        label.textAlignment = .center
        return label
    }()

    private lazy var subheadLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        label.textColor = Colors.grayscale600.color
        label.textAlignment = .center
        return label
    }()

    private lazy var textStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [headlineLabel, subheadLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()

    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [illustrationImageView, textStackView])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        stackView.compatibleLayoutMargins = .rootView
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        buildLayout()
    }
}

extension LendingHeaderView: ViewConfiguration {
    public func configureViews(content: LendingHeaderViewContent) {
        headlineLabel.text = content.title
        subheadLabel.attributedText = content.description
        illustrationImageView.image = content.image
    }

    public func buildViewHierarchy() {
        contentView.addSubview(rootStackView)
    }

    public func setupConstraints() {
        rootStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}
