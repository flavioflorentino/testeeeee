import UI
import UIKit
import AssetsKit

public final class InstallmentCell: UITableViewCell {
    private lazy var orderLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        return label
    }()
    
    private lazy var dueDateTitleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle(type: .default))
        label.text = Strings.Installments.dueDate
        return label
    }()
    
    private lazy var dueDateLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var dueDateStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [dueDateTitleLabel, dueDateLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        stackView.alignment = .leading
        return stackView
    }()
    
    private lazy var statusLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle(type: .default))
        return label
    }()
    
    private lazy var originalValueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(CaptionLabelStyle(type: .default))
            .with(\.textAlignment, .right)
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        return label
    }()
    
    private lazy var valueStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [originalValueLabel, valueLabel])
        stackView.axis = .horizontal
        stackView.spacing = Spacing.base01
        stackView.alignment = .trailing
        return stackView
    }()
    
    private lazy var statusStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [statusLabel, valueStackView])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        stackView.alignment = .trailing
        return stackView
    }()
    
    private lazy var chevronImageView: UIImageView = {
        let imageView = UIImageView(image: Resources.Icons.icoGreenRightArrow.image)
        imageView.setContentHuggingPriority(.required, for: .horizontal)
        imageView.setContentHuggingPriority(.required, for: .vertical)
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

public extension InstallmentCell {
    func setup(with installment: InstallmentCellViewModeling) {
        orderLabel.text = installment.order
        dueDateLabel.text = installment.dueDate
        statusLabel.attributedText = installment.statusDescription
        originalValueLabel.attributedText = installment.originalValueDescription
        valueLabel.attributedText = installment.valueDescription
        accessoryView = installment.hideAccessoryView ? nil : chevronImageView
    }
}

extension InstallmentCell: ViewConfiguration {
    public func buildViewHierarchy() {
        contentView.addSubview(orderLabel)
        contentView.addSubview(dueDateStackView)
        contentView.addSubview(statusStackView)
    }
    
    public func setupConstraints() {
        orderLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }
        
        dueDateStackView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(orderLabel.snp.trailing).offset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
        }
        
        statusStackView.snp.makeConstraints {
            $0.top.trailing.bottom.equalToSuperview().inset(Spacing.base02)
            $0.centerY.equalToSuperview()
        }
        
        originalValueLabel.snp.makeConstraints {
            $0.centerY.equalTo(valueLabel)
        }
    }
    
    public func configureViews() {
        selectionStyle = .none
    }
}
