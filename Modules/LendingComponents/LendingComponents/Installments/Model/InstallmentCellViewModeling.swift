public protocol InstallmentCellViewModeling {
    var order: String { get }
    var dueDate: String { get }
    var statusDescription: NSAttributedString { get }
    var originalValueDescription: NSAttributedString? { get }
    var valueDescription: NSAttributedString? { get }
    var hideAccessoryView: Bool { get }
}
