import Foundation

// swiftlint:disable convenience_type
final class LendingComponentsResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: LendingComponentsResources.self).url(forResource: "LendingComponentsResources", withExtension: "bundle") else {
            return Bundle(for: LendingComponentsResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: LendingComponentsResources.self)
    }()
}
