import UI
import UIKit
import AssetsKit

private extension LendingProductTableViewCell.Layout {
    enum Size {
        static let imageView = CGSize(width: Sizing.base06, height: Sizing.base06)
    }

    enum Length {
        static let imageViewCornerRadius: CGFloat = Sizing.base06 / 2
        static let imageViewBorderWidth: CGFloat = 1
    }

    enum Margins {
        static let contentView: EdgeInsets = {
            var insets: EdgeInsets = .rootView
            insets.top = Spacing.base03
            insets.bottom = Spacing.base03
            return insets
        }()
    }
}

public class LendingProductTableViewCell: UITableViewCell {
    fileprivate enum Layout {}

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        label.textColor = Colors.grayscale700.color
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
        label.textColor = Colors.grayscale500.color
        return label
    }()

    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .center
        imageView.layer.cornerRadius = Layout.Length.imageViewCornerRadius
        imageView.layer.borderWidth = Layout.Length.imageViewBorderWidth
        imageView.layer.borderColor = Colors.grayscale100.color.cgColor
        return imageView
    }()

    private lazy var chevronImageView: UIImageView = {
        let imageView = UIImageView(image: Resources.Icons.icoGreenRightArrow.image)
        imageView.setContentHuggingPriority(.required, for: .horizontal)
        return imageView
    }()

    private lazy var textStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, descriptionLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        return stackView
    }()

    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [iconImageView, textStackView, chevronImageView])
        stackView.spacing = Spacing.base02
        stackView.alignment = .center
        stackView.compatibleLayoutMargins = Layout.Margins.contentView
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        buildLayout()
    }

    public func setup(for item: LendingProductCellViewModeling) {
        titleLabel.text = item.title
        descriptionLabel.text = item.description
        iconImageView.image = item.image
    }
}

extension LendingProductTableViewCell: ViewConfiguration {
    public func buildViewHierarchy() {
        contentView.addSubview(rootStackView)
    }

    public func setupConstraints() {
        rootStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        iconImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.imageView)
        }
    }

    public func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
        separatorInset = .zero
        selectionStyle = .none
    }
}
