public protocol LendingProductCellViewModeling {
    var image: UIImage { get }
    var title: String { get }
    var description: String { get }
}
