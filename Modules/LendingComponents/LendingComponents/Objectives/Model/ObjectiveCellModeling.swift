import UIKit
import UI

public protocol ObjectiveCellModeling {
    var icon: Iconography? { get }
    var imgUrl: URL? { get }
    var title: String? { get }
    var isDisclosureIndicatorHidden: Bool { get }
}
