import AssetsKit
import UI
import UIKit

extension ObjectiveCell.Layout {
    enum Size {
        static var iconContainer = CGSize(width: 44, height: 44)
        static var icon = CGSize(width: 20, height: 20)
    }
}

public final class ObjectiveCell: UITableViewCell {
    fileprivate enum Layout {}

    private lazy var iconImageView = UIImageView()

    private lazy var iconContainer: UIView = {
        let view = UIView()
        view.layer.cornerRadius = Layout.Size.iconContainer.width / 2
        view.border = .light(color: .grayscale050())
        return view
    }()

    private lazy var iconLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, .branding600())
            .with(\.textAlignment, .center)
        label.setContentCompressionResistancePriority(.required, for: .horizontal)
        return label
    }()
    
    private lazy var chevronImageView: UIImageView = {
        let imageView = UIImageView(image: Resources.Icons.icoGreenRightArrow.image)
        imageView.setContentHuggingPriority(.required, for: .horizontal)
        return imageView
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [iconContainer, nameLabel, chevronImageView])
        stackView.spacing = Spacing.base02
        stackView.alignment = .center
        return stackView
    }()
    
    private func setImage(for objective: ObjectiveCellModeling) {
        guard objective.imgUrl != nil else {
            iconLabel.text = objective.icon?.rawValue
            return
        }
        iconImageView.setImage(url: objective.imgUrl)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setup(for objective: ObjectiveCellModeling) {
        nameLabel.text = objective.title
        setImage(for: objective)
        chevronImageView.isHidden = objective.isDisclosureIndicatorHidden
    }
    
    override public func prepareForReuse() {
        super.prepareForReuse()
        iconImageView.cancelRequest()
    }
    
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard #available(iOS 13, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) else {
            return
        }
    }
}

extension ObjectiveCell: ViewConfiguration {
    public func buildViewHierarchy() {
        iconContainer.addSubview(iconImageView)
        iconContainer.addSubview(iconLabel)
        contentView.addSubview(stackView)
    }
    
    public func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.directionalEdges.equalTo(contentView.layoutMarginsGuide)
        }

        iconContainer.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.iconContainer)
        }

        iconLabel.snp.makeConstraints {
            $0.center.equalToSuperview()
        }

        iconImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.icon)
            $0.center.equalToSuperview()
        }
    }
    
    public func configureViews() {
        backgroundColor = .clear
    }
}
