import Foundation

public enum SearchableType: String, Decodable {
    case consumer
    case places
    case subscription
    case store
}
