import Foundation

public struct SearchSection {
    public var sectionTitle: String?
    public var actionType: SectionActionType?
    public var rows: SectionRow
}

extension SearchSection: Decodable {
    private enum CodingKeys: String, CodingKey {
        case sectionTitle
        case component = "sectionDisplay"
        case type
        case rows
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        sectionTitle = try container.decodeIfPresent(String.self, forKey: .sectionTitle)
        actionType = try container.decode(SectionActionType.self, forKey: .type)

        let component = try container.decode(SectionComponent.self, forKey: .component)

        switch component {
        case .carousel:
            let carousel = try container.decode([SearchResultCarouselRow].self, forKey: .rows)
            rows = .carousel(carousel)
        case .list:
            let list = try container.decode([SearchResultListRow].self, forKey: .rows)
            rows = .list(list)
        case .card:
            let card = try container.decode([SearchResultInfoRow].self, forKey: .rows)
            rows = .info(card)
        case .secondaryCarousel:
            let services = try container.decode([SearchSecondaryCarouselRow].self, forKey: .rows)
            rows = .secondaryCarousel(services)
        }
    }
}

public enum SectionActionType: String, Decodable {
    case consumer
    case places
    case store
    case subscription
}
