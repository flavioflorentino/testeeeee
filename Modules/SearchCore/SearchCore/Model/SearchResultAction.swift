import Foundation

public struct SearchResultAction: Decodable {
    var type: SearchResultActionType
    var value: String
}

public enum SearchResultActionType: String, Decodable {
    case webview
    case deeplink
}
