import Foundation

public enum SectionRow {
    case recents([SearchResultListRow])
    case list([SearchResultListRow])
    case carousel([SearchResultCarouselRow])
    case secondaryCarousel([SearchSecondaryCarouselRow])
    case info([SearchResultInfoRow])
}

public struct SearchResultListRow: Decodable {
    public let type: SearchableType
    public let avatarImageUrl: String?
    public let title: String
    public let body: String
    public let footer: String?
    public let isVerified: Bool
    public let isStudent: Bool
    public let isPro: Bool
    public let itemAction: SearchResultAction
    public let paymentItemAction: SearchResultAction?
}

public struct SearchResultCarouselRow: Decodable {
    public let avatarImageUrl: String?
    public let title: String
    public let footer: String?
    public let itemAction: SearchResultAction
    public let paymentItemAction: SearchResultAction?
}

public struct SearchResultInfoRow: Decodable {
    public let title: String
    public let itemAction: SearchResultAction
}

public struct SearchSecondaryCarouselRow: Decodable {
    public let title: String?
    public let body: String
    public let type: SearchResultServicesType
    public let itemAction: SearchResultAction
}

public enum SearchResultServicesType: String, Codable {
    case p2m
    case pix
    case boleto
}
