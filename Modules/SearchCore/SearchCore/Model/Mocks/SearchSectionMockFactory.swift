import Foundation

public enum SearchSectionMockFactory {
    public static func make() -> [SearchSection] {
        let section1 = SearchSection(sectionTitle: "Resultados",
                                     actionType: nil,
                                     rows: buildPeopleRows())
        return [section1]
    }

    static func buildPeopleRows() -> SectionRow {
        SectionRow.list(buildPeopleList())
    }

    // swiftlint:disable function_body_length
    static func buildPeopleList() -> [SearchResultListRow] {
        let row1 = SearchResultListRow(type: .consumer,
                                       avatarImageUrl: "https://i.ibb.co/xgz9CgD/bbb1.png",
                                       title: "arretada_incompreendida",
                                       body: "Juliette Freire",
                                       footer: "Contato na sua agenda",
                                       isVerified: true,
                                       isStudent: false,
                                       isPro: false,
                                       itemAction: .init(type: .deeplink, value: ""),
                                       paymentItemAction: .init(type: .deeplink, value: ""))
        let row2 = SearchResultListRow(type: .consumer,
                                       avatarImageUrl: "https://i.ibb.co/QbNpPtV/bbb2.png",
                                       title: "cachorrada",
                                       body: "Gilberto do Vigor",
                                       footer: "",
                                       isVerified: true,
                                       isStudent: true,
                                       isPro: true,
                                       itemAction: .init(type: .deeplink, value: ""),
                                       paymentItemAction: .init(type: .deeplink, value: ""))
        let row3 = SearchResultListRow(type: .consumer,
                                       avatarImageUrl: "https://i.ibb.co/267nzQq/bbb3.png",
                                       title: "cobrinha_negacionista",
                                       body: "Sarah Andrade",
                                       footer: nil,
                                       isVerified: true,
                                       isStudent: false,
                                       isPro: false,
                                       itemAction: .init(type: .deeplink, value: ""),
                                       paymentItemAction: .init(type: .deeplink, value: ""))
        let row4 = SearchResultListRow(type: .consumer,
                                       avatarImageUrl: "https://i.ibb.co/5FMq8Tw/bbb4.png",
                                       title: "filho_gloria_pires",
                                       body: "Filipe Kartalian Ayrosa Galvão",
                                       footer: nil,
                                       isVerified: false,
                                       isStudent: false,
                                       isPro: true,
                                       itemAction: .init(type: .deeplink, value: ""),
                                       paymentItemAction: .init(type: .deeplink, value: ""))
        let row5 = SearchResultListRow(type: .consumer,
                                       avatarImageUrl: "https://i.ibb.co/sPfM3jv/bbb5.png",
                                       title: "jornada_itinerario",
                                       body: "Lumena Aleluia",
                                       footer: nil,
                                       isVerified: false,
                                       isStudent: false,
                                       isPro: true,
                                       itemAction: .init(type: .deeplink, value: ""),
                                       paymentItemAction: .init(type: .deeplink, value: ""))
        let row6 = SearchResultListRow(type: .consumer,
                                       avatarImageUrl: "https://i.ibb.co/M7CpX12/bbb6.png",
                                       title: "carioca_sensitiva",
                                       body: "Camilla de Lucas",
                                       footer: nil,
                                       isVerified: false,
                                       isStudent: false,
                                       isPro: true,
                                       itemAction: .init(type: .deeplink, value: ""),
                                       paymentItemAction: .init(type: .deeplink, value: ""))
        let row7 = SearchResultListRow(type: .consumer,
                                       avatarImageUrl: "https://i.ibb.co/v32sn8p/bbb7.png",
                                       title: "professor_didatico",
                                       body: "João Luiz",
                                       footer: nil,
                                       isVerified: false,
                                       isStudent: false,
                                       isPro: true,
                                       itemAction: .init(type: .deeplink, value: ""),
                                       paymentItemAction: .init(type: .deeplink, value: ""))
        return [row1, row2, row3, row4, row5, row6, row7]
    }
}
