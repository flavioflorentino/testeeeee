import Foundation

public enum SectionComponent: String, Decodable {
    case carousel
    case list
    case card
    case secondaryCarousel
}
