import Core
import Foundation

public enum SearchType: String {
    case main
    case consumers
}

public protocol SearchServicing {
    @discardableResult
    func search(term: String,
                type: SearchType,
                _ completion: @escaping (Result<[SearchSection], ApiError>) -> Void) -> URLSessionTask?
}
