import Foundation
import Core

public class SearchService {
    public typealias Dependencies = HasMainQueue

    private let dependencies: Dependencies

    public init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension SearchService: SearchServicing {
    @discardableResult
    public func search(term: String,
                       type: SearchType,
                       _ completion: @escaping (Result<[SearchSection], ApiError>) -> Void) -> URLSessionTask? {
        let endpoint = getEndpoint(term: term, type: type)
        
        return Api<[SearchSection]>(endpoint: endpoint).execute(jsonDecoder: .init(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map { $0.model })
            }
        }
    }

    private func getEndpoint(term: String, type: SearchType) -> SearchServiceEndpoint {
        switch type {
        case .main:
            return .main(term: term)
        case .consumers:
            return .consumers(term: term)
        }
    }
}
