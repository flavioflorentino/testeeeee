import Core

enum SearchServiceEndpoint {
    case main(term: String)
    case consumers(term: String)
}

private extension SearchServiceEndpoint {
    enum Parameter: String {
        case term
    }
}

// MARK: - ApiEndpointExposable
extension SearchServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .consumers:
            return "searchable/consumers"
        case .main:
            return "searchable"
        }
    }

    var parameters: [String: Any] {
        var parameters: [String: Any] = [:]

        switch self {
        case .main(let term):
            parameters[Parameter.term.rawValue] = term
        case .consumers(let term):
            parameters[Parameter.term.rawValue] = term
        }

        return parameters
    }
}
