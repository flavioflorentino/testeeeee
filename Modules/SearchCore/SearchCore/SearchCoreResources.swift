import Foundation

// swiftlint:disable convenience_type
final class SearchCoreResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: SearchCoreResources.self).url(forResource: "SearchCoreResources", withExtension: "bundle") else {
            return Bundle(for: SearchCoreResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: SearchCoreResources.self)
    }()
}
