# This file contains ALL PicPay PJ dependencies sorted alphabetically!
# Please mantain this file organized and sorted!
# Core should be placed before UI to be included on XCHammer project
pjDeps = [
    "@//Modules/AccountManagementPJ",
    "@//Modules/AccountShutDownBiz",
    "@//Modules/AnalyticsModule", 
    "@//Modules/AuthenticationPJ", 
    "@//Modules/Core", 
    "@//Modules/CoreSellerAccount",
    "@//Modules/CustomerSupport", 
    "@//Modules/EventTracker", 
    "@//Modules/FactorAuthentication", 
    "@//Modules/FeatureFlag",
    "@//Modules/InAppReview",
    "@//Modules/LegacyPJ", 
    "@//Modules/NetworkPJ",
    "@//Modules/PIX", 
    "@//Modules/SettingsBiz", 
    "@//Modules/UI", 
    "@//Modules/Validations"
]