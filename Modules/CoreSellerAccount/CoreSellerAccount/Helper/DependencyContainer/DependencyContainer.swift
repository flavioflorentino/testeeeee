import Core
import Foundation

typealias AppDependencies = HasMainQueue

final class DependencyContainer: AppDependencies {
    lazy var mainQueue = DispatchQueue.main
}
