import Core
import Foundation
import NetworkPJ

protocol SellerInfoManagerServicing {
    func fetchAccountDetails(completion: @escaping(AccountDetailsResult) -> Void)
}

struct SellerInfoManagerService {
    typealias Dependencies = HasMainQueue
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
}

// MARK: - ReasonServicing
extension SellerInfoManagerService: SellerInfoManagerServicing {
    func fetchAccountDetails(completion: @escaping(AccountDetailsResult) -> Void) {
        let endpoint = SellerInfoManagerEndpoint.checkDigitalAccount
        BizApi<AccountDetails>(endpoint: endpoint).execute { result in
            self.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model.data)
                completion(mappedResult)
            }
        }
    }
}
