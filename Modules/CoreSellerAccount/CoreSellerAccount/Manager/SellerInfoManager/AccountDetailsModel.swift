import Foundation

public enum CompanyType: String, Decodable, Equatable {
    case individual, multiPartners
}

public struct AccountDetails: Decodable, Equatable {
    public enum BizAccountType: String, Decodable {
        case receivementAccount = "receivement_account"
        case paymentAccount = "payment_account"
        case unknown
        
        public init(from decoder: Decoder) throws {
            let containter = try decoder.singleValueContainer()
            let rawValue = try containter.decode(RawValue.self)
            self = BizAccountType(rawValue: rawValue) ?? .unknown
        }
    }
    
    public enum CompanyNature: String, Decodable {
        case ei, eireli, ltda, other
        
        public init(from decoder: Decoder) throws {
            let container = try decoder.singleValueContainer()
            let rawValue = try container.decode(RawValue.self)
            self = CompanyNature(rawValue: rawValue) ?? .other
        }
    }
    
    public let bizAccountType: BizAccountType
    public let companyType: CompanyType
    public let companyNature: CompanyNature
    public let hasPartners: Bool
    
    private enum CodingKeys: String, CodingKey {
        case bizAccountType = "biz_account_type"
        case companyType = "company_type"
        case companyNature = "company_nature"
        case hasPartners = "partners_info"
    }
    
    public init(bizAccountType: BizAccountType, companyType: CompanyType, companyNature: CompanyNature, hasPartners: Bool) {
        self.bizAccountType = bizAccountType
        self.companyType = companyType
        self.companyNature = companyNature
        self.hasPartners = hasPartners
    }
}
