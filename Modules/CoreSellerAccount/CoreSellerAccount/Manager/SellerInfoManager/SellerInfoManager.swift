import Core
import Foundation

public typealias AccountDetailsResult = Result<AccountDetails, ApiError>

public protocol HasSellerInfo {
    var sellerInfo: SellerInfoManagerProtocol { get }
}

public protocol SellerInfoManagerProtocol {
    func accountDetails(completion: @escaping(AccountDetailsResult) -> Void)
    func resetAccountDetails()
}

public final class SellerInfoManager: SellerInfoManagerProtocol {
    public static let shared: SellerInfoManagerProtocol = SellerInfoManager()
    
    private let service: SellerInfoManagerServicing
    
    private var accountDetails: AccountDetails?
    
    init(service: SellerInfoManagerServicing = SellerInfoManagerService()) {
        self.service = service
    }
}

// MARK: - Public Methods

public extension SellerInfoManager {
    func accountDetails(completion: @escaping(AccountDetailsResult) -> Void) {
        guard let accountDetails = accountDetails else {
            fetchAccountDetailsAndSaveSuccess(completion: completion)
            return
        }
        completion(.success(accountDetails))
    }
    
    func resetAccountDetails() {
        accountDetails = nil
    }
}

// MARK: - Private Methods

private extension SellerInfoManager {
    func fetchAccountDetailsAndSaveSuccess(completion: @escaping(AccountDetailsResult) -> Void) {
        service.fetchAccountDetails { [weak self] result in
            guard let self = self else { return }
            // It should always perform the request again if it fails
            if case .success(let accountDetails) = result {
                self.accountDetails = accountDetails
            }
            completion(result)
        }
    }
}
