import Core
import Foundation

enum SellerInfoManagerEndpoint {
    case checkDigitalAccount
}

extension SellerInfoManagerEndpoint: ApiEndpointExposable {
    var path: String { "/v2/seller/check-digital-account" }
}
