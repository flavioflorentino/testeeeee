#if DEBUG
import Core

public final class SellerInfoManagerMock: SellerInfoManagerProtocol {
    public var accountDetailsResult: AccountDetailsResult = .failure(ApiError.timeout)
    public func accountDetails(completion: @escaping (AccountDetailsResult) -> Void) {
        completion(accountDetailsResult)
    }
    
    public init() {}
    
    public func resetAccountDetails() { }
}
#endif
