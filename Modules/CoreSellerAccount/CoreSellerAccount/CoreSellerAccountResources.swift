import Foundation

// swiftlint:disable convenience_type
final class CoreSellerAccountResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: CoreSellerAccountResources.self).url(forResource: "CoreSellerAccountResources", withExtension: "bundle") else {
            return Bundle(for: CoreSellerAccountResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: CoreSellerAccountResources.self)
    }()
}
