import Core
import Foundation
import XCTest
@testable import CoreSellerAccount

private class SellerInfoManagerServicingFake: SellerInfoManagerServicing {
    // MARK: - FetchAccountDetails
    private(set) var fetchAccountDetailsCompletionCallsCount = 0
    
    var fetchAccountDetailsResult: AccountDetailsResult = .failure(ApiError.bodyNotFound)
    func fetchAccountDetails(completion: @escaping(AccountDetailsResult) -> Void) {
        fetchAccountDetailsCompletionCallsCount += 1
        completion(fetchAccountDetailsResult)
    }
}

final class SellerInfoManagerTests: XCTestCase {
    private let timeout: TimeInterval = 1
    private let service = SellerInfoManagerServicingFake()
    
    private lazy var sut = SellerInfoManager(service: service)
    
    func testAccountDetails_WhenServiceFails_ShouldPerformTheRequestAgain() {
        service.fetchAccountDetailsResult = .failure(.bodyNotFound)
        
        sut.accountDetails { _ in }
        XCTAssertEqual(service.fetchAccountDetailsCompletionCallsCount, 1)
        
        sut.accountDetails { _ in }
        XCTAssertEqual(service.fetchAccountDetailsCompletionCallsCount, 2)
    }
    
    func testAccountDetails_WhenAccountDetailsStored_ShouldNotPerformRequestAgain() {
        let accountDetails = AccountDetails(
            bizAccountType: .paymentAccount,
            companyType: .individual,
            companyNature: .ei,
            hasPartners: true
        )
        service.fetchAccountDetailsResult = .success(accountDetails)
        
        sut.accountDetails { _ in }
        XCTAssertEqual(service.fetchAccountDetailsCompletionCallsCount, 1)
        
        sut.accountDetails { _ in }
        XCTAssertEqual(service.fetchAccountDetailsCompletionCallsCount, 1)
    }
    
    func testResetAccountDetails_ShouldResetAccountDetails() {
        let accountDetails = AccountDetails(
            bizAccountType: .paymentAccount,
            companyType: .individual,
            companyNature: .ei,
            hasPartners: true
        )
        service.fetchAccountDetailsResult = .success(accountDetails)
        
        // We should call the service for the first time to store the accountDetails
        sut.accountDetails { _ in }
        XCTAssertEqual(service.fetchAccountDetailsCompletionCallsCount, 1)
        
        // Since we did not reset the accountDetails, we should not call the service again
        sut.accountDetails { _ in }
        XCTAssertEqual(service.fetchAccountDetailsCompletionCallsCount, 1)
        
        // After we reset the AccountDetails, we should call the service again
        sut.resetAccountDetails()
        sut.accountDetails { _ in }
        XCTAssertEqual(service.fetchAccountDetailsCompletionCallsCount, 2)
    }
}
