protocol PermissionsInteracting {
    var canRequestLocationAndContactsPermissions: Bool { get }
    var canRequestLocationPermission: Bool { get }
    var canRequestContactsPermission: Bool { get }

    func requestLocationAndContactsPermissions(_ handler: ((Bool) -> Void)?)
    func requestLocationPermission(_ handler: ((Bool) -> Void)?)
    func requestContactsPermission(_ handler: ((Bool) -> Void)?)

    func didAttemptToRequestPermissions()
}
