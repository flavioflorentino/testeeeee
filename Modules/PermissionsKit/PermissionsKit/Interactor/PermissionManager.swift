protocol PermissionManager {
    var status: PermissionStatus { get }

    func request(handler: ((PermissionStatus) -> Void)?)
}

extension Permission: PermissionManager {
    func request(handler: ((PermissionStatus) -> Void)?) {
        request { handler?($0) }
    }
}
