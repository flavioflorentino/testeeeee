import Core
import Foundation

struct PermissionsInteractor {
    typealias Dependency = HasKVStore

    private let maxAttempts: Int = 2

    private let dependency: Dependency
    private let locationPermissionManager: PermissionManager
    private let contactsPermissionManager: PermissionManager

    init(dependency: Dependency, locationPermissionManager: PermissionManager, contactsPermissionManager: PermissionManager) {
        self.dependency = dependency
        self.locationPermissionManager = locationPermissionManager
        self.contactsPermissionManager = contactsPermissionManager
    }
}

extension PermissionsInteractor: PermissionsInteracting {
    var canRequestLocationAndContactsPermissions: Bool {
        canRequestPermissions
            && locationPermissionManager.status != .authorized
            && contactsPermissionManager.status != .authorized
    }

    var canRequestContactsPermission: Bool {
        canRequestPermissions && contactsPermissionManager.status != .authorized
    }

    var canRequestLocationPermission: Bool {
        canRequestPermissions && locationPermissionManager.status != .authorized
    }

    func didAttemptToRequestPermissions() {
        updateAttempts(to: attempts + 1)
    }

    func requestLocationAndContactsPermissions(_ handler: ((Bool) -> Void)?) {
        locationPermissionManager.request { locationStatus in
            self.contactsPermissionManager.request { contactsStatus in
                self.postDidChangeNotification()
                handler?(locationStatus == .authorized && contactsStatus == .authorized)
            }
        }
    }

    func requestLocationPermission(_ handler: ((Bool) -> Void)?) {
        locationPermissionManager.request {
            self.postDidChangeNotification()
            handler?($0 == .authorized)
        }
    }

    func requestContactsPermission(_ handler: ((Bool) -> Void)?) {
        contactsPermissionManager.request {
            self.postDidChangeNotification()
            handler?($0 == .authorized)
        }
    }
}

// MARK: - Private
private extension PermissionsInteractor {
    var canRequestPermissions: Bool {
        if locationPermissionManager.status == .authorized && contactsPermissionManager.status == .authorized {
            updateAttempts(to: maxAttempts)
        }

        return attempts < maxAttempts
    }

    var attempts: Int {
        dependency.kvStore.intFor(PermissionsKey.attempsToRequestPermissions)
    }

    func updateAttempts(to value: Int) {
        dependency.kvStore.setInt(min(maxAttempts, value), with: PermissionsKey.attempsToRequestPermissions)
    }

    func postDidChangeNotification() {
        NotificationCenter.default.post(Notification.Permissions.didChange)
    }
}

// MARK: - KVStore Key
private enum PermissionsKey: String, KVKeyContract {
    case attempsToRequestPermissions

    var description: String {
        rawValue
    }
}
