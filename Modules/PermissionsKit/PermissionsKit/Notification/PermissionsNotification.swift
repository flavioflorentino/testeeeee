import Foundation

public extension Notification.Name {
    enum Permissions {
        public static let didChange = Notification.Name("Permissions.DidChange")
    }
}

public extension Notification {
    enum Permissions {
        public static let didChange = Notification(name: Notification.Name.Permissions.didChange)
    }
}
