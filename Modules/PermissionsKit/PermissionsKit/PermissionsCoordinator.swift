import AnalyticsModule
import Core
import FeatureFlag
import UI
import UIKit

public protocol PermissionsCoordinating: Coordinating {
    var didFinishFlow: (() -> Void)? { get set }
}

final class PermissionsCoordinator {
    typealias Dependency = HasFeatureManager & HasAnalytics
    typealias PopupFactory = ContactPermissionPopupFactory
        & LocationPermissionPopupFactory
        & LocationAndContactPermissionsPopupFactory

    // MARK: - Properties
    private let dependency: Dependency
    private let mainController: UIViewController
    private var childCoordinators: [Coordinating] = []

    private let interactor: PermissionsInteracting
    private let popupFactory: PopupFactory

    var childViewController: [UIViewController] = []
    var viewController: UIViewController?

    var didFinishFlow: (() -> Void)?

    // MARK: - Initialization
    init(
        dependency: Dependency,
        mainController: UIViewController,
        interactor: PermissionsInteracting,
        popupFactory: PopupFactory
    ) {
        self.dependency = dependency
        self.mainController = mainController
        self.interactor = interactor
        self.popupFactory = popupFactory
    }
}

// MARK: - PermissionsCoordinating
extension PermissionsCoordinator: PermissionsCoordinating {
    func start() {
        guard dependency.featureManager.isActive(.featureInitialPermissionsAfterLogin) else {
            return
        }

        if interactor.canRequestLocationAndContactsPermissions {
            showPermissionContactAndLocationPopup()
        } else if interactor.canRequestContactsPermission {
            showPermissionContactPopup()
        } else if interactor.canRequestLocationPermission {
            showPermissionLocationPopup()
        }
    }
}

// MARK: - Private
private extension PermissionsCoordinator {
    // MARK: - Show methods
    func showPermissionContactAndLocationPopup() {
        let authorizeAction: () -> Void = { [weak self] in
            self?.dependency.analytics.log(PermissionsEvent.popUpContactsAndLocationPermission(action: .authorize))

            self?.interactor.requestLocationAndContactsPermissions { [weak self] _ in
                self?.didFinishFlow?()
            }
        }

        let closeAction: () -> Void = { [weak self] in
            self?.dependency.analytics.log(PermissionsEvent.popUpContactsAndLocationPermission(action: .close))
            self?.didFinishFlow?()
        }

        let popup = popupFactory.makeLocationAndContactPermissionsPopup(
            authorizeAction: authorizeAction,
            closeAction: closeAction
        )

        show(popup)
    }

    func showPermissionContactPopup() {
        let authorizeAction: () -> Void = { [weak self] in
            self?.dependency.analytics.log(PermissionsEvent.popUpContactsPermission(action: .authorize))
            self?.interactor.requestContactsPermission { [weak self] _ in
                self?.didFinishFlow?()
            }
        }

        let closeAction: () -> Void = { [weak self] in
            self?.dependency.analytics.log(PermissionsEvent.popUpContactsPermission(action: .close))
            self?.didFinishFlow?()
        }

        let popup = popupFactory.makeContactPermissionPopup(
            authorizeAction: authorizeAction,
            closeAction: closeAction
        )

        show(popup)
    }

    func showPermissionLocationPopup() {
        let authorizeAction: () -> Void = { [weak self] in
            self?.dependency.analytics.log(PermissionsEvent.popUpLocationPermission(action: .authorize))
            self?.interactor.requestLocationPermission { [weak self] _ in
                self?.didFinishFlow?()
            }
        }

        let closeAction: () -> Void = { [weak self] in
            self?.dependency.analytics.log(PermissionsEvent.popUpLocationPermission(action: .close))
            self?.didFinishFlow?()
        }

        let popup = popupFactory.makeLocationPermissionPopup(
            authorizeAction: authorizeAction,
            closeAction: closeAction
        )

        show(popup)
    }

    func show(_ popup: UIViewController) {
        interactor.didAttemptToRequestPermissions()
        mainController.showPopup(popup)
    }
}
