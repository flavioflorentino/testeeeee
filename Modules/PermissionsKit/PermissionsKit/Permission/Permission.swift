import Contacts

public enum PermissionStatus: String {
    case authorized = "Authorized"
    case denied = "Denied"
    case disabled = "Disabled"
    case notDetermined = "Not Determined"
    
    internal init?(string: String?) {
        guard let string = string else {
            return nil
        }
        self.init(rawValue: string)
    }
}

public enum PermissionType: CustomStringConvertible {
    case contacts
    case addressBook
    case locationAlways
    case locationWhenInUse
    case camera
    
    public var description: String {
        switch self {
        case .contacts, .addressBook:
            return "Contatos"
        case .locationAlways,
             .locationWhenInUse:
            return "Localização"
        case .camera:
            return "Câmera"
        }
    }
}

public class Permission: NSObject {
    static let locationWhenInUseUsageDescription = "NSLocationWhenInUseUsageDescription"
    static let locationAlwaysUsageDescription = "NSLocationAlwaysUsageDescription"
    static let cameraUsageDescription = "NSCameraUsageDescription"
    
    public typealias Callback = (PermissionStatus) -> Void
    
    public static let contacts = Permission(type: .contacts)
    public static let locationAlways = Permission(type: .locationAlways)
    public static let locationWhenInUse = Permission(type: .locationWhenInUse)
    public static let camera = Permission(type: .camera)
    
    public let type: PermissionType
    
    public var status: PermissionStatus {
        switch type {
        case .contacts:
            return statusContacts
        case .locationAlways:
            return statusLocationAlways
        case .locationWhenInUse:
            return statusLocationWhenInUse
        case .camera:
            return statusCamera
        default:
            return .notDetermined
        }
    }
    
    /// Determines whether to present the denied alert.
    public var presentDeniedAlert = true
    
    /// The alert when the permission was denied.
    public lazy var deniedAlert: PermissionAlert = {
        DeniedAlert(permission: self)
    }()
    
    /// Determines whether to present the disabled alert.
    public var presentDisabledAlert = true
    
    /// The alert when the permission is disabled.
    public lazy var disabledAlert: PermissionAlert = {
        DisabledAlert(permission: self)
    }()
    
    var callback: Callback?
    
    /**
     Creates and return a new permission for the specified domain.
     - parameter domain: The permission domain.
     - returns: A newly created permission.
     */
    fileprivate init(type: PermissionType) {
        self.type = type
    }
    
    /**
     Requests the permission.
     - parameter callback: The function to be triggered after the user responded to the request.
     */
    public func request(_ callback: @escaping Callback) {
        self.callback = callback
        
        let status = self.status
        
        switch status {
        case .authorized:
            callbacks(status)
        case .notDetermined:
            requestAuthorization(callbacks)
        case .denied:
            presentDeniedAlert ? deniedAlert.present() : callbacks(status)
        case .disabled:
            presentDisabledAlert ? disabledAlert.present() : callbacks(status)
        }
    }
    
    func requestAuthorization(_ callback: @escaping Callback) {
        switch type {
        case .contacts:
            requestContacts(callback)
        case .locationAlways:
            requestLocationAlways(callback)
        case .locationWhenInUse:
            requestLocationWhenInUse(callback)
        case .camera:
            requestCamera(callback)
        default:
            callback(.notDetermined)
        }
    }
    
    func callbacks(_ with: PermissionStatus) {
        DispatchQueue.main.async {
            self.callback?(self.status)
        }
    }
    
    override public var description: String {
        type.description
    }
}
