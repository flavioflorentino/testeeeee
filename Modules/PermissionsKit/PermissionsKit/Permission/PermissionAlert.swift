open class PermissionAlert {
    fileprivate let permission: Permission
    fileprivate var status: PermissionStatus { permission.status }
    fileprivate var type: PermissionType { permission.type }
    fileprivate var callbacks: Permission.Callback { permission.callbacks }
    
    /// The title of the alert.
    open var title: String?
    
    /// Descriptive text that provides more details about the reason for the alert.
    open var message: String?
    
    /// The title of the cancel action.
    open var cancel: String? {
        get { cancelActionTitle }
        set { cancelActionTitle = newValue }
    }
    
    /// The title of the settings action.
    open var settings: String? {
        get { defaultActionTitle }
        set { defaultActionTitle = newValue }
    }
    
    /// The title of the confirm action.
    open var confirm: String? {
        get { defaultActionTitle }
        set { defaultActionTitle = newValue }
    }
    
    fileprivate var cancelActionTitle: String?
    fileprivate var defaultActionTitle: String?
    
    var controller: UIAlertController {
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: cancelActionTitle, style: .cancel, handler: cancelHandler)
        controller.addAction(action)
        
        return controller
    }
    
    fileprivate var topViewController: UIViewController? {
        if var topController = UIApplication.shared.delegate?.window??.rootViewController {
            while let presentedVC = topController.presentedViewController {
                topController = presentedVC
            }
            return topController
        }
        return nil
    }
    
    internal init(permission: Permission) {
        self.permission = permission
    }
    
    internal func present() {
        DispatchQueue.main.async {
            self.topViewController?.present(self.controller, animated: true, completion: nil)
        }
    }

    fileprivate func cancelHandler(_ action: UIAlertAction) {
        callbacks(status)
    }
}

internal final class DisabledAlert: PermissionAlert {
    override init(permission: Permission) {
        super.init(permission: permission)
        
        /// Default text
        title   = "\(permission) está desabilitado(a)"
        message = "Favor fornecer acesso à \(permission) nas configurações."
        cancel  = "OK"
    }
}

internal final class DeniedAlert: PermissionAlert {
    override var controller: UIAlertController {
        let controller = super.controller
        
        let action = UIAlertAction(title: defaultActionTitle, style: .default, handler: settingsHandler)
        controller.addAction(action)
        controller.preferredAction = action
        
        return controller
    }
    
    override init(permission: Permission) {
        super.init(permission: permission)
        
        /// Default text
        title    = "Permissão para \(permission) foi negada"
        message  = "Favor habilitar acesso à \(permission) nas configurações."
        cancel   = "Cancelar"
        settings = "Configurações"
    }
    
    @objc
    func settingsHandlerBackToApp() {
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
        callbacks(status)
    }
    
    private func settingsHandler(_ action: UIAlertAction) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.settingsHandlerBackToApp), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        if let URL = URL(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.open(URL)
        }
    }
}
