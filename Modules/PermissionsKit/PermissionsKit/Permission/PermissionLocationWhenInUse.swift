import CoreLocation
import Foundation

extension Permission {
    var statusLocationWhenInUse: PermissionStatus {
        guard CLLocationManager.locationServicesEnabled() else { return .disabled }
        
        let status = CLLocationManager.authorizationStatus()
        
        switch status {
        case .authorizedWhenInUse, .authorizedAlways:
            return .authorized
        case .restricted, .denied:
            return .denied
        case .notDetermined:
            return .notDetermined
        @unknown default:
            return .notDetermined
        }
    }
    
    func requestLocationWhenInUse(_ callback: Callback) {
        guard Bundle.main.object(forInfoDictionaryKey: Permission.locationWhenInUseUsageDescription) != nil else {
            debugPrint("WARNING: \(Permission.locationWhenInUseUsageDescription) not found in Info.plist")
            return
        }
        
        PermissionLocationManager.request(self)
    }
}
