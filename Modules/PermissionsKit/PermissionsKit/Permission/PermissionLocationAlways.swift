import Core
import CoreLocation
import Foundation

extension Permission {
    var statusLocationAlways: PermissionStatus {
        guard CLLocationManager.locationServicesEnabled() else { return .disabled }
        
        let status = CLLocationManager.authorizationStatus()
        
        switch status {
        case .authorizedAlways:
            return .authorized
        case .authorizedWhenInUse:
            return KVStore().boolFor(.locationAlwaysWithWhenInUse) ? .denied : .notDetermined
        case .notDetermined:
            return .notDetermined
        case .restricted, .denied:
            return .denied
        @unknown default:
            return .notDetermined
        }
    }
    
    func requestLocationAlways(_ callback: Callback) {
        guard Bundle.main.object(forInfoDictionaryKey: Permission.locationAlwaysUsageDescription) != nil else {
            debugPrint("WARNING: \(Permission.locationAlwaysUsageDescription) not found in Info.plist")
            return
        }
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            KVStore().setBool(true, with: .locationAlwaysWithWhenInUse)
        }
        
        PermissionLocationManager.request(self)
    }
}
