import AVFoundation

extension Permission {
    var statusCamera: PermissionStatus {
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        
        switch status {
        case .authorized:
            return .authorized
        case .restricted, .denied:
            return .denied
        case .notDetermined:
            return .notDetermined
        @unknown default:
            return .notDetermined
        }
    }
    
    func requestCamera(_ callback: @escaping Callback) {
        guard Bundle.main.object(forInfoDictionaryKey: Permission.cameraUsageDescription) != nil else {
            debugPrint("WARNING: \(Permission.cameraUsageDescription) not found in Info.plist")
            return
        }
        
        guard !ProcessInfo.processInfo.environment.keys.contains("XCTestConfigurationFilePath") else {
            return
        }
        
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { _ in
            callback(self.statusCamera)
        }
    }
}
