import CoreLocation

internal let PermissionLocationManager = CLLocationManager()

private var requestedLocation = false
private var triggerCallbacks = false

extension Permission: CLLocationManagerDelegate {
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch (requestedLocation, triggerCallbacks) {
        case (true, false):
            triggerCallbacks = true
            callbacks(self.status)
        case (true, true):
            requestedLocation = false
            triggerCallbacks = false
            callbacks(self.status)
        default:
            break
        }
    }
}

extension CLLocationManager {
    func request(_ permission: Permission) {
        delegate = permission
        requestedLocation = true

        if case .locationAlways = permission.type {
            requestAlwaysAuthorization()
            return
        }
        
        if case .locationWhenInUse = permission.type {
            requestWhenInUseAuthorization()
            return
        }
    }
}
