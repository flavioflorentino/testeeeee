import AssetsKit
import Foundation
import UI
import UIKit

public struct PermissionRequestViewSetup {
    private let requestType: RequestType
    private let style: ViewStyle

    private (set) var authorizeHandler: () -> Void
    private (set) var notNowHandler: (() -> Void)?

    var canShowNotNowButton: Bool {
        notNowHandler != nil
    }

    var iconSize: CGSize {
        switch style {
        case .full:
            return CGSize(width: 168.0, height: 160.0)
        case .listItem:
            return CGSize(width: 124.0, height: 80.0)
        }
    }

    var autorizeButtonWidth: CGFloat {
        switch style {
        case .full:
            return 272.0
        case .listItem:
            return 261.0
        }
    }

    private init(
        requestType: RequestType,
        style: ViewStyle,
        authorizeHandler: @escaping () -> Void,
        notNowHandler: (() -> Void)?
    ) {
        self.requestType = requestType
        self.style = style
        self.authorizeHandler = authorizeHandler
        self.notNowHandler = notNowHandler
    }

    func setup(iconImageView: UIImageView) {
        iconImageView.backgroundColor = Colors.backgroundPrimary.color
        iconImageView.isOpaque = true
        iconImageView.clipsToBounds = true
        iconImageView.contentMode = .scaleAspectFit
        iconImageView.image = icon
    }

    func setup(titleLabel: UILabel) {
        titleLabel
            .labelStyle(TitleLabelStyle(type: style.titleTypography))
            .with(\.backgroundColor, .backgroundPrimary())
            .with(\.textAlignment, .center)

        titleLabel.setContentCompressionResistancePriority(.required, for: .vertical)
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.text = title
    }

    func setup(descriptionLabel: UILabel) {
        descriptionLabel
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.backgroundColor, .backgroundPrimary())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)

        descriptionLabel.lineBreakMode = .byWordWrapping
        descriptionLabel.clipsToBounds = true
        descriptionLabel.text = description
    }

    func setup(authorizeButton: UIButton) {
        switch style {
        case let .full(style) where style == .clear:
            authorizeButton
                .buttonStyle(SecondaryButtonStyle())
                .with(\.typography, .bodyPrimary())
        default:
            authorizeButton
                .buttonStyle(PrimaryButtonStyle())
                .with(\.typography, .bodySecondary())
        }
           
        authorizeButton.setTitle(authorizeButtonText, for: .normal)
    }

    func setup(notNowButton: UIButton) {
        notNowButton.backgroundColor = (color: .backgroundPrimary(), state: .normal)
        notNowButton.textColor = (color: .grayscale400(), state: .normal)
        notNowButton.typography = .bodyPrimary()
        notNowButton.setAttributedTitle(notNowButtonText, for: .normal)
    }
}

public extension PermissionRequestViewSetup {
    enum RequestType {
        case location
        case contacts(usesOldImage: Bool = false)
        case contactsAndLocation
        case contactsPix
    }
    
    enum ButtonStyle {
        case `default`
        case clear
    }

    static func full(
        requestType: RequestType,
        buttonStyle: ButtonStyle = .default,
        authorizeHandler: @escaping () -> Void,
        notNowHandler: (() -> Void)? = nil
    ) -> PermissionRequestViewSetup {
        .init(
            requestType: requestType,
            style: .full(buttonStyle),
            authorizeHandler: authorizeHandler,
            notNowHandler: notNowHandler
        )
    }

    static func listItem(
        requestType: RequestType,
        authorizeHandler: @escaping () -> Void,
        notNowHandler: (() -> Void)? = nil
    ) -> PermissionRequestViewSetup {
        .init(
            requestType: requestType,
            style: .listItem,
            authorizeHandler: authorizeHandler,
            notNowHandler: notNowHandler
        )
    }
}

// MARK: - Private
private extension PermissionRequestViewSetup {
    var icon: UIImage? {
        switch requestType {
        case let .contacts(usesOldImage):
            if usesOldImage {
                return Resources.Illustrations.iluSocialOld.image
            } else {
                return Resources.Illustrations.iluSocial.image
            }
        case .contactsAndLocation:
            return Resources.Illustrations.iluLocationSocial.image
        case .contactsPix:
            return Resources.Illustrations.iluAgendaCircledBackground.image
        case .location:
            return Resources.Illustrations.iluLocation.image
        }
    }

    var title: String? {
        switch requestType {
        case .contacts:
            return Strings.contactsTitle
        case .contactsAndLocation:
            return Strings.locationAndContactsTitle
        case .contactsPix:
            return Strings.contactsPixTitle
        case .location:
            return Strings.locationTitle
        }
    }

    var description: String? {
        switch requestType {
        case .contacts:
            return Strings.contactsDescription
        case .contactsAndLocation:
            return Strings.locationAndContactsPopUpDescription
        case .contactsPix:
            return Strings.contactsPixDescription
        case .location:
            return Strings.locationDescription
        }
    }

    var authorizeButtonText: String? {
        switch requestType {
        case .contacts:
            return Strings.contactsButton
        case .contactsAndLocation:
            return Strings.locationAndContactsButton
        case .contactsPix:
            return Strings.contactsButton
        case .location:
            return Strings.locationButton
        }
    }

    var notNowButtonText: NSAttributedString? {
        guard canShowNotNowButton else {
            return nil
        }

        return NSAttributedString(
            string: Strings.notNowButton,
            attributes: [
                .underlineStyle: NSUnderlineStyle.single.rawValue,
                .foregroundColor: Colors.grayscale400.color
            ]
        )
    }
}

// MARK: - ViewType
private extension PermissionRequestViewSetup {
    enum ViewStyle {
        case full(ButtonStyle)
        case listItem

        var titleTypography: Typography.Style.Title {
            switch self {
            case .full:
                return .large
            case .listItem:
                return .small
            }
        }
    }
}
