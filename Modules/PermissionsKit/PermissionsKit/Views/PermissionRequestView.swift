import SnapKit
import UI
import UIKit

// MARK: - Layout
private extension PermissionRequestView.Layout {
    enum Margin {
        static let buttonBottom: CGFloat = 40
    }
}

public final class PermissionRequestView: UIView {
    // MARK: - Properties
    fileprivate enum Layout { }

    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        stackView.alignment = .center
        return stackView
    }()
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        setup.setup(iconImageView: imageView)

        return imageView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        setup.setup(titleLabel: label)

        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        setup.setup(descriptionLabel: label)

        return label
    }()

    private lazy var authorizeButton: UIButton = {
        let button = UIButton(type: .system)
        button.addTarget(self, action: #selector(didTapAtAuthorizeButton), for: .touchUpInside)
        setup.setup(authorizeButton: button)

        return button
    }()

    private lazy var notNowButton: UIButton = {
        let button = UIButton(type: .system)
        button.addTarget(self, action: #selector(didTapAtNotNowButton), for: .touchUpInside)
        setup.setup(notNowButton: button)

        return button
    }()

    private let setup: PermissionRequestViewSetup

    // MARK: - Initialization
    public init(setup: PermissionRequestViewSetup) {
        self.setup = setup

        super.init(frame: .zero)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration
extension PermissionRequestView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(contentStackView)
        contentStackView.addArrangedSubview(iconImageView)
        contentStackView.addArrangedSubview(titleLabel)
        contentStackView.addArrangedSubview(descriptionLabel)
        addSubview(authorizeButton)

        if setup.canShowNotNowButton {
            addSubview(notNowButton)
        }
    }

    public func setupConstraints() {
        contentStackView.snp.makeConstraints {
            $0.top.greaterThanOrEqualToSuperview().inset(Spacing.base05)
            $0.centerX.equalToSuperview()
            $0.leading.greaterThanOrEqualToSuperview().offset(Spacing.base03)
            $0.trailing.lessThanOrEqualToSuperview().offset(-Spacing.base03)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.centerY.equalTo(self).priority(.medium)
        }
        
        iconImageView.snp.makeConstraints {
            $0.height.equalTo(setup.iconSize.height)
        }

        authorizeButton.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(contentStackView.snp.bottom).offset(Spacing.base02)
            $0.centerX.equalToSuperview()
            $0.leading.greaterThanOrEqualToSuperview().offset(Spacing.base03)
            $0.trailing.lessThanOrEqualToSuperview().offset(-Spacing.base03)
            $0.width.greaterThanOrEqualTo(setup.autorizeButtonWidth)

            guard setup.canShowNotNowButton == false else {
                return
            }

            $0.bottom.equalToSuperview().offset(-Layout.Margin.buttonBottom)
        }

        guard setup.canShowNotNowButton else {
            return
        }

        notNowButton.snp.makeConstraints {
            $0.top.equalTo(authorizeButton.snp.bottom).offset(Spacing.base02)
            $0.leading.greaterThanOrEqualToSuperview().offset(Spacing.base03)
            $0.trailing.lessThanOrEqualToSuperview().offset(-Spacing.base03)
            $0.centerX.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-Layout.Margin.buttonBottom)
        }
    }

    public func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}

private protocol PermissionRequestLayoutSetup {
    func style(titleLabel: UILabel)
}

// MARK: - Private
private extension PermissionRequestView {
    @objc
    func didTapAtAuthorizeButton() {
        setup.authorizeHandler()
    }

    @objc
    func didTapAtNotNowButton() {
        setup.notNowHandler?()
    }
}
