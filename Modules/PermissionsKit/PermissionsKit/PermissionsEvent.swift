import AnalyticsModule

public enum PermissionsEvent {
    case popUpContactsAndLocationPermission(action: Action)
    case popUpContactsPermission(action: Action)
    case popUpLocationPermission(action: Action)
    case cardContactsPermission(origin: Origin)
}

// MARK: - Nested types
public extension PermissionsEvent {
    enum Action: String {
        case close
        case authorize
    }

    enum Origin: String {
        case pay = "Pagar Principais"
        case payPeople = "Pagar pessoas"
    }
}

// MARK: - Private
private extension PermissionsEvent {
    var name: String {
        switch self {
        case .popUpContactsAndLocationPermission:
            return "Pop Up Contacts and Location Permission"
        case .popUpContactsPermission:
            return "Pop Up Contacts Permission"
        case .popUpLocationPermission:
            return "Pop Up Location Permission"
        case .cardContactsPermission:
            return "Card Contacts Permission"
        }
    }

    private var properties: [String: Any] {
        let actionKey = "action"

        switch self {
        case let .popUpContactsAndLocationPermission(action),
             let .popUpContactsPermission(action),
             let .popUpLocationPermission(action):
            return [actionKey: action.rawValue]
        case let .cardContactsPermission(origin):
            return ["origin": origin.rawValue]
        }
    }

    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
}

// MARK: - AnalyticsKeyProtocol
extension PermissionsEvent: AnalyticsKeyProtocol {
    public func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
