import UIKit

protocol LocationAndContactPermissionsPopupFactory {
    func makeLocationAndContactPermissionsPopup(
        authorizeAction: @escaping () -> Void,
        closeAction: @escaping () -> Void
    ) -> UIViewController
}
