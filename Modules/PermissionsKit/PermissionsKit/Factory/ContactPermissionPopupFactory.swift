import UIKit

protocol ContactPermissionPopupFactory {
    func makeContactPermissionPopup(authorizeAction: @escaping () -> Void, closeAction: @escaping () -> Void) -> UIViewController
}
