import AnalyticsModule
import AssetsKit
import Core
import FeatureFlag
import UI
import UIKit

public final class PermissionsFactory {
    public typealias Dependencies = HasFeatureManager & HasAnalytics & HasKVStore

    private let dependencies: Dependencies

    public init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - PermissionsCoordinatorFactory
public extension PermissionsFactory {
    func makePermissionsCoordinator(mainController: UIViewController) -> PermissionsCoordinating {
        let interactor = PermissionsInteractor(
            dependency: dependencies,
            locationPermissionManager: Permission.locationWhenInUse,
            contactsPermissionManager: Permission.contacts
        )

        return PermissionsCoordinator(
            dependency: dependencies,
            mainController: mainController,
            interactor: interactor,
            popupFactory: self
        )
    }
}

// MARK: - LocationAndContactPermissionsPopupFactory
extension PermissionsFactory: LocationAndContactPermissionsPopupFactory {
    func makeLocationAndContactPermissionsPopup(
        authorizeAction: @escaping () -> Void,
        closeAction: @escaping () -> Void
    ) -> UIViewController {
        makePopupViewController(
            data: Data(
                image: Resources.Illustrations.iluLocationSocial.image,
                title: Strings.locationAndContactsTitle,
                description: Strings.locationAndContactsPopUpDescription
            ),
            authorizeButtonTitle: Strings.locationAndContactsButton,
            authorizeAction: authorizeAction,
            closeAction: closeAction
        )
    }
}

// MARK: - ContactPermissionPopupFactory
extension PermissionsFactory: ContactPermissionPopupFactory {
    func makeContactPermissionPopup(
        authorizeAction: @escaping () -> Void,
        closeAction: @escaping () -> Void
    ) -> UIViewController {
        makePopupViewController(
            data: Data(
                image: Resources.Illustrations.iluSocialOld.image,
                title: Strings.contactsTitle,
                description: Strings.contactsPopUpDescription
            ),
            authorizeButtonTitle: Strings.contactsButton,
            authorizeAction: authorizeAction,
            closeAction: closeAction
        )
    }
}

// MARK: - LocationPermissionPopupFactory
extension PermissionsFactory: LocationPermissionPopupFactory {
    func makeLocationPermissionPopup(
        authorizeAction: @escaping () -> Void,
        closeAction: @escaping () -> Void
    ) -> UIViewController {
        makePopupViewController(
            data: Data(
                image: Resources.Illustrations.iluLocation.image,
                title: Strings.locationTitle,
                description: Strings.locationDescription
            ),
            authorizeButtonTitle: Strings.locationButton,
            authorizeAction: authorizeAction,
            closeAction: closeAction
        )
    }
}

// MARK: - Private
private extension PermissionsFactory {
    struct Data {
        let image: UIImage
        let title: String
        let description: String
    }

    func makePopupViewController(
        data: Data,
        authorizeButtonTitle: String,
        authorizeAction: @escaping () -> Void,
        closeAction: @escaping () -> Void
    ) -> PopupViewController {
        let controller = PopupViewController(title: data.title, description: data.description, preferredType: .image(data.image))
        controller.didCloseDismiss = closeAction
        controller.addAction(.init(title: authorizeButtonTitle, style: .fill, completion: authorizeAction))

        return controller
    }
}
