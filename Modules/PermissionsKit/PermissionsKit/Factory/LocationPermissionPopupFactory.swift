import UIKit

protocol LocationPermissionPopupFactory {
    func makeLocationPermissionPopup(authorizeAction: @escaping () -> Void, closeAction: @escaping () -> Void) -> UIViewController
}
