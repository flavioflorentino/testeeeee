import Foundation

// swiftlint:disable convenience_type
final class PermissionsKitResources {
    static let resourcesBundle: Bundle = {
        guard let url = Bundle(for: PermissionsKitResources.self).url(forResource: "PermissionsKitResources", withExtension: "bundle") else {
            return Bundle(for: PermissionsKitResources.self)
        }
        
        return Bundle(url: url) ?? Bundle(for: PermissionsKitResources.self)
    }()
}
