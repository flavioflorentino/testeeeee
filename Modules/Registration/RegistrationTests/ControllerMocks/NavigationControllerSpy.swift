import UIKit

final class NavigationControllerSpy: UINavigationController {
    private(set) var pushViewControllerCallsCount = 0
    private(set) var popViewControllerCallsCount = 0
    private(set) var presentViewControllerCallsCount = 0
    private(set) var dismissViewControllerCallsCount = 0
    private(set) var setViewControllersCallsCount = 0
    
    private(set) var lastPushedViewController: UIViewController?
    private(set) var lastPopedViewController: UIViewController?
    private(set) var lastPresentedViewController: UIViewController?
    private(set) var lastSetViewControllers: [UIViewController]?
    
    public override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        lastPushedViewController = viewController
        pushViewControllerCallsCount += 1
        super.pushViewController(viewController, animated: false)
    }
    
    public override func popViewController(animated: Bool) -> UIViewController? {
        let viewController = super.popViewController(animated: false)
        lastPopedViewController = viewController
        popViewControllerCallsCount += 1
        return viewController
    }
    
    public override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        lastPresentedViewController = viewControllerToPresent
        presentViewControllerCallsCount += 1
        super.present(viewControllerToPresent, animated: false)
        completion?()
    }
    
    public override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        dismissViewControllerCallsCount += 1
        super.dismiss(animated: false)
        completion?()
    }
    
    public override func setViewControllers(_ viewControllers: [UIViewController], animated: Bool) {
        setViewControllersCallsCount += 1
        lastSetViewControllers = viewControllers
        super.setViewControllers(viewControllers, animated: true)
    }
}
