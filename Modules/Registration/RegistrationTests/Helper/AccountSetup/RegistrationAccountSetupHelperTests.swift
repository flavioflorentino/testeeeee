@testable import Registration
import Core
import XCTest

private final class RegistrationAccountSetupServiceMock: RegistrationAccountSetupServicing {
    private(set) var callSendSetupAccountCount = 0
     private(set) var model: RegistrationAccountSetupModel?
    var result: Result<NoContent, ApiError> = .success(NoContent())

    func sendSetupAccount(model: RegistrationAccountSetupModel, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        self.callSendSetupAccountCount += 1
        self.model = model
        
        completion(result)
    }
}

final class RegistrationAccountSetupTests: XCTestCase {
    private lazy var service = RegistrationAccountSetupServiceMock()
    private lazy var sut = RegistrationAccountSetupHelper(service: service)
    
    func testSetupAccount_WhenSetupAccountReturnSuccess_ShouldExecuteCompletionsWithSuccess() {
        let model = RegistrationAccountSetupModel(
            document: "588.176.241-01",
            birthdate: "23/11/1999",
            phone: "989772277",
            areaCode: "11",
            firstName: "Joao",
            lastName: "Silva"
        )
        
        service.result = .success(NoContent())
        sut.setupAccount(model: model)
        
        XCTAssertEqual(service.callSendSetupAccountCount, 1)
        XCTAssertNotNil(service.model)
    }
}
