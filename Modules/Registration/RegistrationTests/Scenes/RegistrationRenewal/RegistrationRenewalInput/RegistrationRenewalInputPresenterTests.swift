@testable import Registration
import UI
import XCTest

private class RegistrationRenewalInputCoordinatingSpy: RegistrationRenewalInputCoordinating {
    var viewController: UIViewController?
    
    // MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var action: RegistrationRenewalInputAction?

    func perform(action: RegistrationRenewalInputAction) {
        performActionCallsCount += 1
        self.action = action
    }
}

private class RegistrationRenewalInputDisplaySpy: RegistrationRenewalInputDisplay {
    // MARK: - display
    private(set) var displayCallsCount = 0
    private(set) var displayModel: RegistrationRenewalInputDisplayModel?

    func display(_ displayModel: RegistrationRenewalInputDisplayModel) {
        displayCallsCount += 1
        self.displayModel = displayModel
    }

    // MARK: - setupPhoneNumberInputTextField
    private(set) var setupPhoneNumberInputTextFieldCallsCount = 0

    func setupPhoneNumberInputTextField() {
        setupPhoneNumberInputTextFieldCallsCount += 1
    }

    // MARK: - setupEmailInputTextField
    private(set) var setupEmailInputTextFieldCallsCount = 0

    func setupEmailInputTextField() {
        setupEmailInputTextFieldCallsCount += 1
    }

    // MARK: - updateButtonsState
    private(set) var updateButtonsStateCallsCount = 0
    private(set) var buttonsAreEnabled: Bool?

    func updateButtonsState(areEnabled: Bool) {
        updateButtonsStateCallsCount += 1
        buttonsAreEnabled = areEnabled
    }

    // MARK: - displayConfirmationPopup
    private(set) var displayConfirmationPopupCallsCount = 0
    private(set) var title: String?
    private(set) var message: String?

    func displayConfirmationPopup(withTitle title: String, message: String) {
        displayConfirmationPopupCallsCount += 1
        self.title = title
        self.message = message
    }

    // MARK: - showLoading
    private(set) var showLoadingCallsCount = 0

    func showLoading() {
        showLoadingCallsCount += 1
    }

    // MARK: - hideLoading
    private(set) var hideLoadingCallsCount = 0

    func hideLoading() {
        hideLoadingCallsCount += 1
    }

    // MARK: - displayErrorPopup
    private(set) var displayErrorPopupCallsCount = 0
    private(set) var errorTitle: String?
    private(set) var errorMessage: String?
    private(set) var errorButtonTitle: String?

    func displayErrorPopup(title: String?, message: String, buttonTitle: String) {
        displayErrorPopupCallsCount += 1
        errorTitle = title
        errorMessage = message
        errorButtonTitle = buttonTitle
    }
    
    // MARK: - displayConfirmBackActionPopup
    private(set) var displayConfirmBackActionPopupCount = 0
    private(set) var popupTitle: String?
    private(set) var popupMessage: String?
    private(set) var popupBackTitle: String?
    private(set) var popupCloseTitle: String?
    
    func displayConfirmBackActionPopup(title: String, message: String, backTitle: String, closeTitle: String) {
        displayConfirmBackActionPopupCount += 1
        popupTitle = title
        popupMessage = message
        popupBackTitle = backTitle
        popupCloseTitle = closeTitle
    }
}

final class RegistrationRenewalInputPresenterTests: XCTestCase {
    private let coordinatorSpy = RegistrationRenewalInputCoordinatingSpy()
    private let viewControllerSpy = RegistrationRenewalInputDisplaySpy()
    private lazy var sut: RegistrationRenewalInputPresenting = {
        let presenter = RegistrationRenewalInputPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPopulateView_WhenPhoneInput_ShouldPopulateView() {
        let phoneValueMock = "(27) 98765-4321"
        let expectedDisplayModel = RegistrationRenewalInputDisplayModel(inputType: .phone, value: phoneValueMock)
        
        sut.populateView(for: .phone, value: phoneValueMock)
        
        XCTAssertEqual(viewControllerSpy.displayCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayModel, expectedDisplayModel)
        XCTAssertEqual(viewControllerSpy.setupPhoneNumberInputTextFieldCallsCount, 1)
    }
    
    func testPopulateView_WhenEmailInput_ShouldPopulateView() {
        let emailValueMock = "fulano@picpay.com"
        let expectedDisplayModel = RegistrationRenewalInputDisplayModel(inputType: .email, value: emailValueMock)
        
        sut.populateView(for: .email, value: emailValueMock)
        
        XCTAssertEqual(viewControllerSpy.displayCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.displayModel, expectedDisplayModel)
        XCTAssertEqual(viewControllerSpy.setupEmailInputTextFieldCallsCount, 1)
    }
    
    func testDidNextStep_WhenInputValueIsPhone_ShoulPerformAction() {
        let delay = Int.random(in: 0...60)
        let action = RegistrationRenewalInputAction.validation(
            inputType: .phone,
            inputedValue: "(27) 98765-4321",
            codeResendDelay: delay,
            userProfile: .unlimitedAccount
        )

        sut.didNextStep(action: action)

        XCTAssertEqual(coordinatorSpy.action, action)
    }
    
    func testDidNextStep_WhenInputValueIsEmail_ShoulPerformAction() {
        let delay = Int.random(in: 0...60)
        let action = RegistrationRenewalInputAction.validation(
            inputType: .email,
            inputedValue: "fulano@picpay.com",
            codeResendDelay: delay,
            userProfile: .picpayCard
        )

        sut.didNextStep(action: action)

        XCTAssertEqual(coordinatorSpy.action, action)
    }
    
    func testUpdateButtonsState_WhenAreEnabled_ShouldUpdateButtonsStateToEnabled() {
        sut.updateButtonsState(areEnabled: true)
        
        XCTAssertEqual(viewControllerSpy.updateButtonsStateCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.buttonsAreEnabled, true)
    }
    
    func testUpdateButtonsState_WhenAreNotEnabled_ShouldUpdateButtonsStateToNotEnabled() {
        sut.updateButtonsState(areEnabled: false)
        
        XCTAssertEqual(viewControllerSpy.updateButtonsStateCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.buttonsAreEnabled, false)
    }

    func testPresentConfirmation_WhenInputIsPhone_ShouldDisplayConfirmationPopup() {
        let phoneValueMock = "(27) 98765-4321"
        
        sut.presentConfirmation(for: .phone, value: phoneValueMock)
        
        XCTAssertEqual(viewControllerSpy.displayConfirmationPopupCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.title, Strings.Input.Phone.confirmationTitle)
        XCTAssertEqual(viewControllerSpy.message, phoneValueMock)
    }
    
    func testPresentConfirmation_WhenInputIsEmail_ShouldDisplayConfirmationPopup() {
        let emailValueMock = "fulano@picpay.com"
        
        sut.presentConfirmation(for: .email, value: emailValueMock)
        
        XCTAssertEqual(viewControllerSpy.displayConfirmationPopupCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.title, Strings.Input.Email.confirmationTitle)
        XCTAssertEqual(viewControllerSpy.message, emailValueMock)
    }

    func testPresentLoading_ShouldShowLoading() {
        sut.presentLoading()

        XCTAssertEqual(viewControllerSpy.showLoadingCallsCount, 1)
    }

    func testHideLoading_ShouldHideLoading() {
        sut.hideLoading()

        XCTAssertEqual(viewControllerSpy.hideLoadingCallsCount, 1)
    }
    
    func testPresentErrorMessage_ShouldDisplayErrorMessage() {
        let errorTitle = "A generic title message"
        let errorMessage = "A generic error message"
        let errorButtonTitle = "ok"

        sut.presentErrorPopup(title: errorTitle, message: errorMessage, buttonTitle: errorButtonTitle)

        XCTAssertEqual(viewControllerSpy.displayErrorPopupCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.errorTitle, errorTitle)
        XCTAssertEqual(viewControllerSpy.errorMessage, errorMessage)
        XCTAssertEqual(viewControllerSpy.errorButtonTitle, errorButtonTitle)
    }
    
    func testPresentConfirmBackActionPopup_ShouldDisplayConfirmBackPopup() {
        sut.presentConfirmBackActionPopup()
        
        XCTAssertEqual(viewControllerSpy.displayConfirmBackActionPopupCount, 1)
        XCTAssertEqual(viewControllerSpy.popupTitle, Strings.Validation.Alert.title)
        XCTAssertEqual(viewControllerSpy.popupMessage, Strings.Validation.Alert.message)
        XCTAssertEqual(viewControllerSpy.popupBackTitle, Strings.Button.discard)
        XCTAssertEqual(viewControllerSpy.popupCloseTitle, Strings.Button.continueFilling)
    }
}
