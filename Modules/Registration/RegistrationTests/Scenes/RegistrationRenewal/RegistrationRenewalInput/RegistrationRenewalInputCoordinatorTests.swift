@testable import Registration
import XCTest

final class RegistrationRenewalInputDelegateSpy: RegistrationRenewalInputDelegate {
    private(set) var didChangeItemCallCount = 0
    private(set) var backToFormCallCount = 0
    private(set) var rawValue: String?
    private(set) var formatedValue: String?
    
    func didChangeItem(toValue rawValue: String, formatedValue: String) {
        self.rawValue = rawValue
        self.formatedValue = formatedValue
        didChangeItemCallCount += 1
    }
    
    func backToForm() {
        backToFormCallCount += 1
    }
}

final class RegistrationRenewalInputCoordinatorTests: XCTestCase {
    private let navControllerSpy = NavigationControllerSpy(rootViewController: UIViewController())
    private let delegateMock = RegistrationRenewalInputDelegateSpy()
    
    private lazy var sut: RegistrationRenewalInputCoordinating = {
        let coordinator = RegistrationRenewalInputCoordinator(delegate: delegateMock)
        coordinator.viewController = navControllerSpy.topViewController
        return coordinator
    }()
    
    func testPerformAction_WhenActionIsValidation_ShouldOpenValidationScreen() throws {
        let action = RegistrationRenewalInputAction.validation(
            inputType: .phone,
            inputedValue: "",
            codeResendDelay: 20,
            userProfile: .picpayCard
        )
        
        sut.perform(action: action)
        
        XCTAssertEqual(navControllerSpy.pushViewControllerCallsCount, 2)
        XCTAssert(navControllerSpy.lastPushedViewController is RegistrationRenewalCodeViewController)
    }
}
