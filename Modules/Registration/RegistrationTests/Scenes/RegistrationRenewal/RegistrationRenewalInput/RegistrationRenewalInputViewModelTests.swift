import AnalyticsModule
import Core
@testable import Registration
import XCTest

final private class RegistrationRenewalInputServicingMock: RegistrationRenewalInputServicing {
    // MARK: - changeRegistrationValue
    private(set) var changeRegistrationValueCallsCount = 0
    var result: Result<RegistrationRenewalInputResponse, ApiError>?

    func changeRegistrationValue(
        to value: String,
        completion: @escaping (Result<RegistrationRenewalInputResponse, ApiError>) -> Void
    ) {
        changeRegistrationValueCallsCount += 1
        guard let result = result else {
            XCTFail("Mocked result is nil")
            return
        }
        completion(result)
    }
}

final private class RegistrationRenewalInputPresentingSpy: RegistrationRenewalInputPresenting {
    var viewController: RegistrationRenewalInputDisplay?

    // MARK: - populateView
    private(set) var populateViewCallsCount = 0
    private(set) var viewInputType: RegistrationRenewalInputType?
    private(set) var viewValue: String?

    func populateView(for inputType: RegistrationRenewalInputType, value: String) {
        populateViewCallsCount += 1
        viewInputType = inputType
        viewValue = value
    }

    // MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var action: RegistrationRenewalInputAction?

    func didNextStep(action: RegistrationRenewalInputAction) {
        didNextStepActionCallsCount += 1
        self.action = action
    }

    // MARK: - updateButtonsState
    private(set) var updateButtonsStateCallsCount = 0
    private(set) var buttonsAreEnabled: Bool?

    func updateButtonsState(areEnabled: Bool) {
        updateButtonsStateCallsCount += 1
        buttonsAreEnabled = areEnabled
    }

    // MARK: - presentConfirmation
    private(set) var presentConfirmationCallsCount = 0
    private(set) var confirmationInputType: RegistrationRenewalInputType?
    private(set) var confirmationValue: String?

    func presentConfirmation(for inputType: RegistrationRenewalInputType, value: String) {
        presentConfirmationCallsCount += 1
        confirmationInputType = inputType
        confirmationValue = value
    }

    // MARK: - presentLoading
    private(set) var presentLoadingCallsCount = 0

    func presentLoading() {
        presentLoadingCallsCount += 1
    }

    // MARK: - hideLoading
    private(set) var hideLoadingCallsCount = 0

    func hideLoading() {
        hideLoadingCallsCount += 1
    }
    
    // MARK: - presentErrorPopup
    private(set) var presentErrorPopupCallsCount = 0
    private(set) var errorTitle: String?
    private(set) var errorMessage: String?
    private(set) var errorButtonTitle: String?
    
    func presentErrorPopup(title: String?, message: String, buttonTitle: String) {
        presentErrorPopupCallsCount += 1
        errorTitle = title
        errorMessage = message
        errorButtonTitle = buttonTitle
    }
    
    // MARK: - presentConfirmBackActionPopup
    private(set) var presentConfirmBackActionPopupCount = 0
    
    func presentConfirmBackActionPopup() {
        presentConfirmBackActionPopupCount += 1
    }
}

final class RegistrationRenewalInputViewModelTests: XCTestCase {
    private let serviceMock = RegistrationRenewalInputServicingMock()
    private let presenterSpy = RegistrationRenewalInputPresentingSpy()
    private let analyticsMock = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(analyticsMock)
    
    private let phoneValueMock = "(27) 98765-4321"
    private let emailValueMock = "fulano@picpay.com"
    
    private func createViewModel(for inputType: RegistrationRenewalInputType = .phone) -> RegistrationRenewalInputViewModel {
        RegistrationRenewalInputViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            dependencies: dependenciesMock,
            inputType: inputType,
            inputValue: inputType == .phone ? phoneValueMock : emailValueMock,
            userProfile: .limitedAccount
        )
    }
    
    func testPopulateView_WhenInputIsPhone_ShouldPopulateView() {
        let sut = createViewModel(for: .phone)

        sut.populateView()
        
        XCTAssertEqual(presenterSpy.populateViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewInputType, .phone)
        XCTAssertEqual(presenterSpy.viewValue, phoneValueMock)
    }
    
    func testPopulateView_WhenInputIsEmail_ShouldPopulateView() {
        let sut = createViewModel(for: .email)

        sut.populateView()
        
        XCTAssertEqual(presenterSpy.populateViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.viewInputType, .email)
        XCTAssertEqual(presenterSpy.viewValue, emailValueMock)
    }
    
    func testDidConfirm_WhenInputIsPhone_ShouldPresentConfirmation() {
        let sut = createViewModel(for: .phone)
        
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.presentConfirmationCallsCount, 1)
        XCTAssertEqual(presenterSpy.confirmationInputType, .phone)
        XCTAssertEqual(presenterSpy.confirmationValue, phoneValueMock)
    }
    
    func testDidConfirm_WhenInputIsEmail_ShouldPresentConfirmation() {
        let sut = createViewModel(for: .email)
        
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.presentConfirmationCallsCount, 1)
        XCTAssertEqual(presenterSpy.confirmationInputType, .email)
        XCTAssertEqual(presenterSpy.confirmationValue, emailValueMock)
    }
    
    func testDidSecondConfirmation_WhenResponseIsSuccess_ShouldPresentValidationScreen() {
        let sut = createViewModel(for: .phone)
        let delay = Int.random(in: 0...60)
        serviceMock.result = .success(RegistrationRenewalInputResponse(delay: delay))
        
        sut.didSecondConfirmation()
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(serviceMock.changeRegistrationValueCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        let expectedAction = RegistrationRenewalInputAction.validation(
            inputType: .phone,
            inputedValue: phoneValueMock,
            codeResendDelay: delay,
            userProfile: .limitedAccount
        )
        XCTAssertEqual(presenterSpy.action, expectedAction)
    }
    
    func testDidSecondConfirmation_WhenResponseIsFailure_ShouldPresentError() {
        let sut = createViewModel(for: .phone)
        var requestError = RequestError()
        requestError.message = "An error message"
        serviceMock.result = .failure(ApiError.badRequest(body: requestError))
        
        sut.didSecondConfirmation()
        
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(serviceMock.changeRegistrationValueCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 0)
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 1)
        XCTAssertEqual(presenterSpy.errorTitle, requestError.title)
        XCTAssertEqual(presenterSpy.errorMessage, requestError.message)
        XCTAssertEqual(presenterSpy.errorButtonTitle, Strings.Button.ok)
    }
    
    func testDidHaveCode_ShouldPresentValidationScreen() {
        let sut = createViewModel(for: .phone)
        
        sut.didHaveCode()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        let expectedAction = RegistrationRenewalInputAction.validation(
            inputType: .phone,
            inputedValue: phoneValueMock,
            codeResendDelay: nil,
            userProfile: .limitedAccount
        )
        XCTAssertEqual(presenterSpy.action, expectedAction)
    }
    
    func testUpdateInputValue_WhenEmailInputValueIsValid_ShouldEnableButtons() throws {
        let sut = createViewModel(for: .email)
        let value = "azAZ.09_@abc.com"
        
        sut.updateInputValue(value)
        
        XCTAssertEqual(presenterSpy.updateButtonsStateCallsCount, 1)
        let buttonsAreEnabled = try XCTUnwrap(presenterSpy.buttonsAreEnabled)
        XCTAssert(buttonsAreEnabled)
    }
    
    func testUpdateInputValue_WhenPhoneInputValueIsValid_ShouldEnableButtons() throws {
        let sut = createViewModel(for: .phone)
        let value = "(93) 45321-6833"
        
        sut.updateInputValue(value)
        
        XCTAssertEqual(presenterSpy.updateButtonsStateCallsCount, 1)
        let buttonsAreEnabled = try XCTUnwrap(presenterSpy.buttonsAreEnabled)
        XCTAssert(buttonsAreEnabled)
    }
    
    func testUpdateInputValue_WhenEmailInputValueHasWrongFormat_ShouldDisableButtons() throws {
        let sut = createViewModel(for: .email)
        let value = "azAZ.09_abc.com"
        
        sut.updateInputValue(value)
        
        XCTAssertEqual(presenterSpy.updateButtonsStateCallsCount, 1)
        let buttonsAreEnabled = try XCTUnwrap(presenterSpy.buttonsAreEnabled)
        XCTAssertFalse(buttonsAreEnabled)
    }
    
    func testUpdateInputValue_WhenPhoneInputValueHasWrongFormat_ShouldDisableButtons() throws {
        let sut = createViewModel(for: .phone)
        let value = "(93) 453216833"
        
        sut.updateInputValue(value)
        
        XCTAssertEqual(presenterSpy.updateButtonsStateCallsCount, 1)
        let buttonsAreEnabled = try XCTUnwrap(presenterSpy.buttonsAreEnabled)
        XCTAssertFalse(buttonsAreEnabled)
    }
    
    func testUpdateInputValue_WhenInputValueDidNotChange_ShouldDisableButtons() throws {
        let sut = createViewModel(for: .phone)
        let value = "(27) 98765-4321"
        
        sut.updateInputValue(value)
        
        XCTAssertEqual(presenterSpy.updateButtonsStateCallsCount, 1)
        let buttonsAreEnabled = try XCTUnwrap(presenterSpy.buttonsAreEnabled)
        XCTAssertFalse(buttonsAreEnabled)
    }
    
    func testUpdateInputValue_WhenPhoneInputValueIsNil_ShouldNotDoAnything() throws {
        let sut = createViewModel(for: .phone)
        
        sut.updateInputValue(nil)
        
        XCTAssertEqual(presenterSpy.updateButtonsStateCallsCount, 0)
    }
    
    func testDidConfirmBackAction_ShouldBackToFirstView() {
        let sut = createViewModel(for: .phone)

        sut.didConfirmBackAction()

        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .backToForm)
    }
    
    func testDidBack_ShouldPresentConfirmBackPopup() {
        let sut = createViewModel(for: .phone)

        sut.didBack()

        XCTAssertEqual(presenterSpy.presentConfirmBackActionPopupCount, 1)
    }
    
    // Tracking of events
    
    func testDidConfirmBackAction_ShouldSendDiscardChangesEvent() {
        let sut = createViewModel(for: .email)

        sut.didConfirmBackAction()

        let expectedEvent = RegistrationRenewalEvent.didDiscardChangedData(userProfile: .limitedAccount, discartedData: .email)
            .event()
        XCTAssert(analyticsMock.equals(to: expectedEvent))
    }
}
