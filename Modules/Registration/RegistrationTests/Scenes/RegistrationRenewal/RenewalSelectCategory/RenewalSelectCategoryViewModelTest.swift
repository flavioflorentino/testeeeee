import AnalyticsModule
import Core
@testable import Registration
import XCTest

private final class RenewalSelectCategoryServiceMock: RenewalSelectCategoryServicing {
    var getSelectableCategorySuccess: Bool = true
    var saveSelectableCategorySuccess: Bool = true
    
    func getSelectableCategory(completion: @escaping (Result<RenewalSelectCategory, ApiError>) -> Void) {
        guard getSelectableCategorySuccess else {
            completion(.failure(ApiError.bodyNotFound))
            return
        }
        
        do {
            let model = try MockDecodable<RenewalSelectCategory>().loadCodableObject(resource: "selectCategory")
            completion(.success(model))
        } catch {
            completion(.failure(ApiError.connectionFailure))
        }
    }
    
    func saveSelectableCategory(value: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        guard saveSelectableCategorySuccess else {
            completion(.failure(ApiError.bodyNotFound))
            return
        }
        let model = NoContent()
        completion(.success(model))
    }
}

private final class RenewalSelectCategoryPresenterSpy: RenewalSelectCategoryPresenting {
    private(set) var callPresentView = 0
    private(set) var callUpdateItems = 0
    private(set) var callPresentLoading = 0
    private(set) var callDidNextStep = 0
    private(set) var callPresentErrorPopupCount = 0
    
    private(set) var titleError: String?
    private(set) var messageError: String?
    private(set) var buttonTitleError: String?
    private(set) var model: RenewalSelectCategory?
    private(set) var items: [SelectCategory]?
    private(set) var action: RenewalSelectCategoryAction?
    
    var viewController: RenewalSelectCategoryDisplay?
    
    func presentView(with model: RenewalSelectCategory) {
        callPresentView += 1
        self.model = model
    }
    
    func updateItems(_ items: [SelectCategory]) {
        callUpdateItems += 1
        self.items = items
    }
    
    func presentErrorPopup(title: String?, message: String, buttonTitle: String) {
        callPresentErrorPopupCount += 1
        titleError = title
        messageError = message
        buttonTitleError = buttonTitle
    }
    
    func presentLoading() {
        callPresentLoading += 1
    }
    
    func didNextStep(action: RenewalSelectCategoryAction) {
        callDidNextStep += 1
        self.action = action
    }
}

final class RenewalSelectCategoryViewModelTest: XCTestCase {
    private let serviceMock = RenewalSelectCategoryServiceMock()
    private let presenterSpy = RenewalSelectCategoryPresenterSpy()
    private let analyticsMock = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(analyticsMock)
    
    private lazy var sut = RenewalSelectCategoryViewModel(
        service: serviceMock,
        presenter: presenterSpy,
        dependencies: dependenciesMock,
        itemType: .wealthRange,
        userProfile: .limitedAccount
    )
    
    func testUpdateView_WhenCalled_ShouldCallStartLoadCategory() {
        sut.updateView()
        
        XCTAssertEqual(presenterSpy.callPresentLoading, 1)
    }
    
    func testUpdateView_WhenCallServiceSuccess_ShouldCallPresenterView() {
        serviceMock.getSelectableCategorySuccess = true
        sut.updateView()
        
        XCTAssertEqual(presenterSpy.callPresentView, 1)
        XCTAssertEqual(presenterSpy.model?.items.count, 7)
    }
    
    func testUpdateView_WhenCallServiceFailure_ShouldCallPresentErrorMessage() {
        serviceMock.getSelectableCategorySuccess = false
        sut.updateView()
        
        XCTAssertEqual(presenterSpy.callPresentErrorPopupCount, 1)
        XCTAssertNotNil(presenterSpy.titleError)
        XCTAssertNotNil(presenterSpy.messageError)
        XCTAssertNotNil(presenterSpy.buttonTitleError)
    }
    
    func testDidTapRow_WhenValidIndexPath_ShouldCalldidNextStep() {
        serviceMock.getSelectableCategorySuccess = true
        serviceMock.saveSelectableCategorySuccess = true
        sut.updateView()
        
        sut.didTapRow(index: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.callDidNextStep, 1)
        switch presenterSpy.action {
        case let .didSelectItem(id, _):
            XCTAssertEqual(id, "1")
        default:
            XCTFail("Not Call DidSelectItem")
        }
    }
    
    func testDidTapRow_WhenInvalidIndexPath_ShouldNotCalldidNextStep() {
        serviceMock.getSelectableCategorySuccess = true
        sut.updateView()
        
        sut.didTapRow(index: IndexPath(row: 7, section: 0))
        
        XCTAssertEqual(presenterSpy.callDidNextStep, 0)
    }
    
    func testDidTapRow_WhenSaveResponseIsFalse_ShouldPresentError() {
        serviceMock.getSelectableCategorySuccess = true
        serviceMock.saveSelectableCategorySuccess = false
        sut.updateView()
        
        sut.didTapRow(index: IndexPath(row: 0, section: 0))

        XCTAssertEqual(presenterSpy.callPresentErrorPopupCount, 1)
    }
    
    func testDidBack_ShouldNotCallDidNextStep() {
        sut.didGetBack()
        
        XCTAssertEqual(presenterSpy.callDidNextStep, 1)
        XCTAssertEqual(presenterSpy.action, .close)
    }
    
    func testDidTapHeader_WhenCalled_ShouldCallDidNextStep() {
        serviceMock.getSelectableCategorySuccess = true
        sut.updateView()
        
        sut.didTapHeader()
        
        XCTAssertEqual(presenterSpy.callDidNextStep, 1)
        switch presenterSpy.action {
        case .openHelp(let url):
            XCTAssertNotNil(url)
        default:
            XCTFail("Not Call OpenHelp")
        }
    }
    
    func testDidTapHeader_WhenCalled_ShouldNotCallDidNextStep() {
        sut.didTapHeader()
        
        XCTAssertEqual(presenterSpy.callDidNextStep, 0)
    }
    
    func testFilter_WhenEmpty_ShouldCallUpdateItemsWithAllItems() {
        serviceMock.getSelectableCategorySuccess = true
        sut.updateView()
        
        sut.filter(searchText: "")
        
        XCTAssertEqual(presenterSpy.callUpdateItems, 1)
        XCTAssertEqual(presenterSpy.items?.count, 7)
    }
    
    func testFilter_WhenNotEmpty_ShouldCallUpdateWithFourItems() {
        serviceMock.getSelectableCategorySuccess = true
        sut.updateView()
        
        sut.filter(searchText: "Maior de")
        
        XCTAssertEqual(presenterSpy.callUpdateItems, 1)
        XCTAssertEqual(presenterSpy.items?.count, 4)
    }
    
    // Tracking of events
    
    func testDidTapHeader_ShouldSendWealthInfoOpenedEvent() {
        serviceMock.getSelectableCategorySuccess = true
        sut.updateView()
        
        sut.didTapHeader()
        
        let expectedEvent = RegistrationRenewalEvent.didTapWealthLink(userProfile: .limitedAccount).event()
        XCTAssert(analyticsMock.equals(to: expectedEvent))
    }
    
    func testDidTapRow_WhenSaveResponseIsFalse_ShouldSendErrorEvent() {
        serviceMock.getSelectableCategorySuccess = true
        serviceMock.saveSelectableCategorySuccess = false
        sut.updateView()
        
        sut.didTapRow(index: IndexPath(row: 0, section: 0))

        let expectedEvent = RegistrationRenewalEvent.didReceiveFieldUpdateError(
            userProfile: .limitedAccount,
            itemType: .wealthRange,
            errorType: "Ocorreu um erro ao carregar as informações, tente novamente."
        )
        .event()
        XCTAssert(analyticsMock.equals(to: expectedEvent))
    }
}
