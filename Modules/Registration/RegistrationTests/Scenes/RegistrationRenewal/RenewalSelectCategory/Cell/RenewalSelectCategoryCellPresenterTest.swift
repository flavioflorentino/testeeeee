@testable import Registration
import XCTest

private final class RenewalSelectCategoryCellDisplaySpy: RenewalSelectCategoryCellDisplay {
    private(set) var callDisplayDescription = 0
    private(set) var descriptionText: String?
    
    func displayDescription(with text: String) {
        callDisplayDescription += 1
        descriptionText = text
    }
}

class RenewalSelectCategoryCellPresenterTest: XCTestCase {
    private let viewSpy = RenewalSelectCategoryCellDisplaySpy()
    private let model = RenewalSelectCategoryCell(description: "description")
    
    private lazy var sut: RenewalSelectCategoryCellPresenting = {
        let sut = RenewalSelectCategoryCellPresenter()
        sut.view = viewSpy
        
        return sut
    }()
    
    func testSetup_WhenCalledModel_ShouldCallDisplayDescriptionWithValue() {
        sut.setup(model: model)
        
        XCTAssertEqual(viewSpy.callDisplayDescription, 1)
        XCTAssertEqual(viewSpy.descriptionText, model.description)
    }
}
