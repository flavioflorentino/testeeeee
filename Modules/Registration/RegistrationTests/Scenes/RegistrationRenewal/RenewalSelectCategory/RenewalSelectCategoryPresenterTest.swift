@testable import Registration
import UI
import XCTest

private final class RenewalSelectCategoryCoordinatorSpy: RenewalSelectCategoryCoordinating {
    private(set) var callPerform = 0
    private(set) var action: RenewalSelectCategoryAction?
    
    var viewController: UIViewController?
    
    func perform(action: RenewalSelectCategoryAction) {
        callPerform += 1
        self.action = action
    }
}

private final class RenewalSelectCategoryDisplaySpy: RenewalSelectCategoryDisplay {
    private(set) var callDisplayTitle = 0
    private(set) var callDisplayErrorPopup = 0
    private(set) var callDisplayHeaderView = 0
    private(set) var callDisplaySearchBar = 0
    private(set) var callDisplayItens = 0
    private(set) var callShowLoading = 0
    private(set) var callHideLoading = 0
    
    private(set) var title: String?
    private(set) var errorTitle: String?
    private(set) var errorMessage: String?
    private(set) var errorButtonTitle: String?
    private(set) var attributedText: NSAttributedString?
    private(set) var searchPlaceholder: String?
    private(set) var data: [Section<String, RenewalSelectCategoryCell>]?
    
    func displayTitle(_ title: String) {
        callDisplayTitle += 1
        self.title = title
    }
    
    func displayErrorPopup(title: String?, message: String, buttonTitle: String) {
        callDisplayErrorPopup += 1
        errorTitle = title
        errorMessage = message
        errorButtonTitle = buttonTitle
    }
    
    func displayHeaderView(attributedText: NSAttributedString) {
        callDisplayHeaderView += 1
        self.attributedText = attributedText
    }
    
    func displaySearchBar(searchPlaceholder: String?) {
        callDisplaySearchBar += 1
        self.searchPlaceholder = searchPlaceholder
    }
    
    func displayItens(data: [Section<String, RenewalSelectCategoryCell>]) {
        callDisplayItens += 1
        self.data = data
    }
    
    func showLoading() {
        callShowLoading += 1
    }
    
    func hideLoading() {
        callHideLoading += 1
    }
}

class RenewalSelectCategoryPresenterTest: XCTestCase {
    private let displaySpy = RenewalSelectCategoryDisplaySpy()
    private let coordinatorSpy = RenewalSelectCategoryCoordinatorSpy()
    
    private lazy var sut: RenewalSelectCategoryPresenting = {
        let sut = RenewalSelectCategoryPresenter(coordinator: coordinatorSpy)
        sut.viewController = displaySpy
        
        return sut
    }()
    
    private func createModel(isSearchable: Bool = true) -> RenewalSelectCategory {
        let items = try? MockDecodable<[SelectCategory]>().loadCodableObject(resource: "categoryInvoice")
        return RenewalSelectCategory(
            title: "Title",
            description: "Parte normal [a]parte selecionada[a] fim",
            items: items ?? [],
            isSearchable: isSearchable,
            searchPlaceholder: "Search Placeholder",
            deeplinkFaq: "picpay://picpay/helpcenter?query=auxilio-merenda-dos-estudantes-parceria")
    }
    
    func testPresenterView_WhenCalledWithModel_ShouldCallDisplayTitle() {
        let model = createModel()
        sut.presentView(with: model)
        XCTAssertEqual(displaySpy.callDisplayTitle, 1)
        XCTAssertEqual(displaySpy.title, model.title)
    }
    
    func testPresentErrorMessage_ShouldCallDisplayError() {
        let errorTitle = "Error title"
        let errorMessage = "Error message"
        let errorButtonTitle = "Error button title"
        
        sut.presentErrorPopup(title: errorTitle, message: errorMessage, buttonTitle: errorButtonTitle)
        
        XCTAssertEqual(displaySpy.callDisplayErrorPopup, 1)
        XCTAssertEqual(displaySpy.errorTitle, errorTitle)
        XCTAssertEqual(displaySpy.errorMessage, errorMessage)
        XCTAssertEqual(displaySpy.errorButtonTitle, errorButtonTitle)
    }
    
    func testPresentErrorMessage_ShouldCallHideLoading() {
        let errorTitle = "Error title"
        let errorMessage = "Error message"
        let errorButtonTitle = "Error button title"
        
        sut.presentErrorPopup(title: errorTitle, message: errorMessage, buttonTitle: errorButtonTitle)
        
        XCTAssertEqual(displaySpy.callHideLoading, 1)
    }
    
    func testPresenterView_WhenCalledWithModel_ShouldCallHideLoading() {
        let model = createModel()
        sut.presentView(with: model)
        
        XCTAssertEqual(displaySpy.callHideLoading, 1)
    }
    
    func testPresenterView_WhenCalledWithModel_ShouldCallDisplayHeaderViewWithValue() {
        let model = createModel()
        let result = model.description.replacingOccurrences(of: "[a]", with: "")
        sut.presentView(with: model)
        
        XCTAssertEqual(displaySpy.callDisplayHeaderView, 1)
        XCTAssertEqual(displaySpy.attributedText?.string, result)
    }
    
    func testPresenterView_WhenIsSearchableTrue_ShouldCallDisplaySearchBar() {
        let model = createModel(isSearchable: true)
        sut.presentView(with: model)
        
        XCTAssertEqual(displaySpy.callDisplaySearchBar, 1)
        XCTAssertEqual(displaySpy.searchPlaceholder, model.searchPlaceholder)
    }
    
    func testPresenterView_WhenIsSearchableFalse_ShouldCallDisplaySearchBar() {
        let model = createModel(isSearchable: false)
        sut.presentView(with: model)
        
        XCTAssertEqual(displaySpy.callDisplaySearchBar, 0)
        XCTAssertNil(displaySpy.searchPlaceholder)
    }
    
    func testPresenterView_WhenCalledWithModel_ShouldCallDisplayItens() {
        let model = createModel()
        sut.presentView(with: model)
        
        XCTAssertEqual(displaySpy.callDisplayItens, 1)
        XCTAssertEqual(displaySpy.data?[0].items.count, 7)
    }
    
    func testUpdateItems_WhenCalled_ShouldCallDisplayItens() {
        let model = createModel()
        sut.updateItems(model.items)
        
        XCTAssertEqual(displaySpy.callDisplayItens, 1)
        XCTAssertEqual(displaySpy.data?[0].items.count, 7)
    }
    
    func testStartLoadCategory_WhenCalled_ShouldCallShowLoading() {
        sut.presentLoading()
        
        XCTAssertEqual(displaySpy.callShowLoading, 1)
    }
    
    func testDidNextStep_WhenActionDidSelectItem_ShouldCallPerformAction() {
        sut.didNextStep(action: .didSelectItem(id: "1", displayValue: ""))
        
        XCTAssertEqual(coordinatorSpy.callPerform, 1)
        switch coordinatorSpy.action {
        case let .didSelectItem(id, _):
            XCTAssertEqual(id, "1")
        default:
            XCTFail("Not Call DidSelectItem")
        }
    }
      
    func testDidNextStep_WhenActionOpenHelp_ShouldCallPerformAction() {
        let url = URL(string: "https://www.picpay.com/site")!
        sut.didNextStep(action: .openHelp(url: url))
        
        XCTAssertEqual(coordinatorSpy.callPerform, 1)
        switch coordinatorSpy.action {
        case .openHelp(let url):
            XCTAssertNotNil(url)
        default:
            XCTFail("Not Call OpenHelp")
        }
    }
    
    func testDidNextStep_WhenActionClose_ShouldCallPerformAction() {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.callPerform, 1)
        XCTAssertEqual(coordinatorSpy.action, .close)
    }
}
