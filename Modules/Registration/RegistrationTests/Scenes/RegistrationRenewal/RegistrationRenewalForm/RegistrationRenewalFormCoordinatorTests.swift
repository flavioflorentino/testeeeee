@testable import Registration
import XCTest

final class RenewalFormCoordinatorDelegateSpy: RegistrationRenewalFormCoordinatorDelegate {
    private(set) var dismissViewControllerCallsCount = 0
    private(set) var openAddressRenewalCallsCount = 0
    private(set) var successCallsCount = 0
    
    private(set) var model: RegistrationRenewalSuccessModel?
    
    func openAddressRenewal(idAddress: String?, completion: @escaping (String, String) -> Void) {
        dismissViewControllerCallsCount += 1
    }
    
    func success(model: RegistrationRenewalSuccessModel) {
        successCallsCount += 1
        self.model = model
    }
    
    func close() {
        dismissViewControllerCallsCount += 1
    }
}

final class RegistrationRenewalFormCoordinatorTests: XCTestCase {
    private let navControllerSpy = NavigationControllerSpy(rootViewController: UIViewController())
    private let delegateSpy = RenewalFormCoordinatorDelegateSpy()
    private lazy var sut: RegistrationRenewalFormCoordinating = {
        let coordinator = RegistrationRenewalFormCoordinator(delegate: delegateSpy)
        coordinator.viewController = navControllerSpy.topViewController
        return coordinator
    }()
    
    private func editValueAction(of type: RegistrationRenewalItemType) -> RegistrationRenewalFormAction {
        RegistrationRenewalFormAction.editValue(
            of: type,
            fromValue: "",
            fromDisplayValue: "",
            userProfile: .picpayCard,
            completion: { _, _ in }
        )
    }
    
    func testPerformAction_WhenActionIsClose_ShouldDismiss() {
        sut.perform(action: .close)
        
        XCTAssertEqual(delegateSpy.dismissViewControllerCallsCount, 1)
    }
    
    func testPerformAction_WhenActionIsSuccess_ShouldDismiss() {
        let model = RegistrationRenewalSuccessModel(title: "title", description: "description")
        sut.perform(action: .success(model))
        
        XCTAssertEqual(delegateSpy.successCallsCount, 1)
        XCTAssertEqual(delegateSpy.model, model)
    }
    
    func testPerformAction_WhenActionIsChangePhoneValue_ShouldPushChangePhoneScreen() throws {
        sut.perform(action: editValueAction(of: .phone))
        
        XCTAssertEqual(navControllerSpy.pushViewControllerCallsCount, 2)
        let pushedViewController = try XCTUnwrap(navControllerSpy.lastPushedViewController)
        XCTAssert(pushedViewController is RegistrationRenewalInputViewController)
    }
    
    func testPerformAction_WhenActionIsChangeEmailValue_ShouldPushChangeEmailScreen() throws {
        sut.perform(action: editValueAction(of: .email))
        
        XCTAssertEqual(navControllerSpy.pushViewControllerCallsCount, 2)
        let pushedViewController = try XCTUnwrap(navControllerSpy.lastPushedViewController)
        XCTAssert(pushedViewController is RegistrationRenewalInputViewController)
    }
    
    func testPerformAction_WhenActionIsChangeIncomeRangeValue_ShouldPushChangeIncomeRangeScreen() throws {
        sut.perform(action: editValueAction(of: .incomeRange))
        
        XCTAssertEqual(navControllerSpy.pushViewControllerCallsCount, 2)
        let pushedViewController = try XCTUnwrap(navControllerSpy.lastPushedViewController)
        XCTAssert(pushedViewController is RenewalSelectCategoryViewController)
    }
    
    func testPerformAction_WhenActionIsChangeWeathRangeValue_ShouldPushChangeWealthRangeScreen() throws {
        sut.perform(action: editValueAction(of: .wealthRange))
        
        XCTAssertEqual(navControllerSpy.pushViewControllerCallsCount, 2)
         let pushedViewController = try XCTUnwrap(navControllerSpy.lastPushedViewController)
        XCTAssert(pushedViewController is RenewalSelectCategoryViewController)
    }
    
    func testPerformAction_WhenActionIsProfessionValue_ShouldPushChangeProfessionScreen() throws {
        sut.perform(action: editValueAction(of: .profession))
        
        XCTAssertEqual(navControllerSpy.pushViewControllerCallsCount, 2)
        let pushedViewController = try XCTUnwrap(navControllerSpy.lastPushedViewController)
        XCTAssert(pushedViewController is RenewalSelectCategoryViewController)
    }
    
    func testPerformAction_WhenActionIsChangeAddressValue_ShouldPushChangeAddressScreen() throws {
        sut.perform(action: editValueAction(of: .address))
        
        XCTAssertEqual(navControllerSpy.pushViewControllerCallsCount, 1)
    }
}
