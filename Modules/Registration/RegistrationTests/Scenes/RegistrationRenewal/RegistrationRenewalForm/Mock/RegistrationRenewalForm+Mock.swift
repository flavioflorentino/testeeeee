@testable import Registration

extension RegistrationRenewalForm {
    static var mock = RegistrationRenewalForm(
        title: "Atualize seu cadastro",
        subtitle: "Confirme se as informações sobre você estão atualizadas. Caso algo tenho mudado, faça as alterações necessárias.",
        items: [
            RegistrationRenewalFormItem(title: "Telefone", value: "11998871234", displayValue: "11998871234", alertText: "Telefone já cadastrado em outra conta PicPay", type: .phone),
            RegistrationRenewalFormItem(title: "E-mail", value: "fulano@email.com", displayValue: "fulano@email.com", alertText: nil, type: .email),
            RegistrationRenewalFormItem(title: "Renda mensal", value: "21212", displayValue: "De R$10.000,00 a R$20.000,00", alertText: nil, type: .incomeRange),
            RegistrationRenewalFormItem(title: "Endereço", value: "93483", displayValue: "Av. Jerônimo Monteiro, 1000, A2 apto 101 - Vitória, ES - 29100100", alertText: "Error Endereço", type: .address),
            RegistrationRenewalFormItem(title: "Patrimônio", value: "75363", displayValue: "De R$1.000 a 2.000", alertText: nil, type: .wealthRange),
            RegistrationRenewalFormItem(title: "Profissão", value: "03484", displayValue: "Economista", alertText: nil, type: .profession)
        ]
    )
    
    static var cellModel: [RenewalFormCell] = [
        RenewalFormCell(title: "Telefone", displayValue: "11998871234", alert: "Telefone já cadastrado em outra conta PicPay", alertIsHidden: false),
        RenewalFormCell(title: "E-mail", displayValue: "fulano@email.com", alert: nil, alertIsHidden: true),
        RenewalFormCell(title: "Renda mensal", displayValue: "De R$10.000,00 a R$20.000,00", alert: nil, alertIsHidden: true),
        RenewalFormCell(title: "Endereço", displayValue: "Av. Jerônimo Monteiro, 1000, A2 apto 101 - Vitória, ES - 29100100", alert: "Error Endereço", alertIsHidden: false),
        RenewalFormCell(title: "Patrimônio", displayValue: "De R$1.000 a 2.000", alert: nil, alertIsHidden: true),
        RenewalFormCell(title: "Profissão", displayValue: "Economista", alert: nil, alertIsHidden: true)
    ]
    
    static var mockObfuscate: [RenewalFormCell] = [
        RenewalFormCell(title: "Telefone", displayValue: "(**) *****-1234", alert: "Telefone já cadastrado em outra conta PicPay", alertIsHidden: false),
        RenewalFormCell(title: "E-mail", displayValue: "f****o@e***l.com", alert: nil, alertIsHidden: true),
        RenewalFormCell(title: "Renda mensal", displayValue: "De R$10.000,00 a R$20.000,00", alert: nil, alertIsHidden: true),
        RenewalFormCell(title: "Endereço", displayValue: "Av. Jerônimo Monteiro, 1000, A2 apto 101 - Vitória, ES - 29100100", alert: "Error Endereço", alertIsHidden: false),
        RenewalFormCell(title: "Patrimônio", displayValue: "De R$1.000 a 2.000", alert: nil, alertIsHidden: true),
        RenewalFormCell(title: "Profissão", displayValue: "Economista", alert: nil, alertIsHidden: true)
    ]
}
