import FeatureFlag
@testable import Registration
import UI
import XCTest

private final class RegistrationRenewalFormCoordinatingSpy: RegistrationRenewalFormCoordinating {
    var viewController: UIViewController?

    // MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var action: RegistrationRenewalFormAction?

    func perform(action: RegistrationRenewalFormAction) {
        performActionCallsCount += 1
        self.action = action
    }
}

private final class RegistrationRenewalFormDisplaySpy: RegistrationRenewalFormDisplay {
    // MARK: - setupTableViewHandler
    private(set) var setupTableViewHandlerCallsCount = 0
    private(set) var cellModels: [Section<String, RenewalFormCell>]?

    func setupTableViewHandler(cellModels: [Section<String, RenewalFormCell>]) {
        setupTableViewHandlerCallsCount += 1
        self.cellModels = cellModels
    }

    // MARK: - displayTitle
    private(set) var displayTitleCallsCount = 0
    private(set) var title: String?

    func displayTitle(_ title: String) {
        displayTitleCallsCount += 1
        self.title = title
    }

    // MARK: - displayErrorPopup
    private(set) var displayErrorPopupCallsCount = 0
    private(set) var errorTitle: String?
    private(set) var errorMessage: String?
    private(set) var errorButtonTitle: String?

    func displayErrorPopup(title: String?, message: String, buttonTitle: String) {
        displayErrorPopupCallsCount += 1
        errorTitle = title
        errorMessage = message
        errorButtonTitle = buttonTitle
    }

    // MARK: - showLoading
    private(set) var showLoadingCallsCount = 0

    func showLoading() {
        showLoadingCallsCount += 1
    }

    // MARK: - hideLoading
    private(set) var hideLoadingCallsCount = 0

    func hideLoading() {
        hideLoadingCallsCount += 1
    }
    
    private(set) var displayPopupAuthCallsCount = 0
    
    func displayPopupAuth() {
        displayPopupAuthCallsCount += 1
    }
}

final class RegistrationRenewalFormPresenterTests: XCTestCase {
    private let coordinatorSpy = RegistrationRenewalFormCoordinatingSpy()
    private let viewControllerSpy = RegistrationRenewalFormDisplaySpy()
    private let featureManagerMock = FeatureManagerMock()
    private lazy var sut: RegistrationRenewalFormPresenting = {
        let presenter = RegistrationRenewalFormPresenter(coordinator: coordinatorSpy, dependencies: DependencyContainerMock(featureManagerMock))
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPresentForm_WhenOfuscationIsEnabled_ShouldDisplayTitleAndSetupTableView() throws {
        featureManagerMock.override(key: .featureAppSecObfuscationMask, with: true)
        
        let formModel = RegistrationRenewalForm.mock
        let obfuscateFormModel = RegistrationRenewalForm.mockObfuscate
        
        sut.presentForm(formModel)
         
        XCTAssertEqual(viewControllerSpy.displayTitleCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.title, formModel.title)
        XCTAssertEqual(viewControllerSpy.setupTableViewHandlerCallsCount, 1)
        let sectionModel = try XCTUnwrap(viewControllerSpy.cellModels?.first)
        XCTAssertEqual(sectionModel.header, formModel.subtitle)
        XCTAssertEqual(sectionModel.items, obfuscateFormModel)
    }
    
    func testPresentForm_WhenOfuscationIsDisabled_ShouldDisplayTitleAndSetupTableView() throws {
        featureManagerMock.override(key: .featureAppSecObfuscationMask, with: false)
        
        let formModel = RegistrationRenewalForm.mock
        let cellModel = RegistrationRenewalForm.cellModel
        
        sut.presentForm(formModel)
         
        XCTAssertEqual(viewControllerSpy.displayTitleCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.title, formModel.title)
        XCTAssertEqual(viewControllerSpy.setupTableViewHandlerCallsCount, 1)
        let sectionModel = try XCTUnwrap(viewControllerSpy.cellModels?.first)
        XCTAssertEqual(sectionModel.header, formModel.subtitle)
        XCTAssertEqual(sectionModel.items, cellModel)
    }
    
    func testPresentForm_WhenAlertIsNotNil_ShouldSetupTableViewWithAlertHiddenFalse() throws {
        featureManagerMock.override(key: .featureAppSecObfuscationMask, with: true)
        let formModel = RegistrationRenewalForm.mock
        
        sut.presentForm(formModel)
        
        let sectionModel = try XCTUnwrap(viewControllerSpy.cellModels?.first)
        XCTAssertNotNil(sectionModel.items[0].alert)
        XCTAssertEqual(sectionModel.items[0].alertIsHidden, false)
        XCTAssertEqual(viewControllerSpy.setupTableViewHandlerCallsCount, 1)
    }
    
    func testPresentForm_WhenAlertIsNil_ShouldSetupTableViewWithAlertHiddenTrue() throws {
        featureManagerMock.override(key: .featureAppSecObfuscationMask, with: true)
        let formModel = RegistrationRenewalForm.mock
        
        sut.presentForm(formModel)
        
        let sectionModel = try XCTUnwrap(viewControllerSpy.cellModels?.first)
        XCTAssertNil(sectionModel.items[1].alert)
        XCTAssertEqual(sectionModel.items[1].alertIsHidden, true)
        XCTAssertEqual(viewControllerSpy.setupTableViewHandlerCallsCount, 1)
    }
    
    func testDidNextStep_ShoulPerformAction() {
        let action = RegistrationRenewalFormAction.close
        
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinatorSpy.action, action)
    }
    
    func testPresentErrorMessage_ShouldDisplayErrorMessage() {
        let errorTitle = "A generic title message"
        let errorMessage = "A generic error message"
        let errorButtonTitle = "ok"
        
        sut.presentErrorPopup(title: errorTitle, message: errorMessage, buttonTitle: errorButtonTitle)
        
        XCTAssertEqual(viewControllerSpy.displayErrorPopupCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.errorTitle, errorTitle)
        XCTAssertEqual(viewControllerSpy.errorMessage, errorMessage)
        XCTAssertEqual(viewControllerSpy.errorButtonTitle, errorButtonTitle)
    }
    
    func testPresentLoading_ShouldShowLoading() {
        sut.presentLoading()
        
        XCTAssertEqual(viewControllerSpy.showLoadingCallsCount, 1)
    }
    
    func testHideLoading_ShouldHideLoading() {
        sut.hideLoading()
        
        XCTAssertEqual(viewControllerSpy.hideLoadingCallsCount, 1)
    }
    
    func testPresentAuth_ShouldCallDisplayPopupAuth() {
        sut.presentAuth()
        
        XCTAssertEqual(viewControllerSpy.displayPopupAuthCallsCount, 1)
    }
}
