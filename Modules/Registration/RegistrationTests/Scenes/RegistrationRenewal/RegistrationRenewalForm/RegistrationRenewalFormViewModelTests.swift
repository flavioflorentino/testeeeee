import AnalyticsModule
import Core
@testable import Registration
import XCTest

extension RegistrationRenewalFormAction: Equatable {
    public static func == (lhs: RegistrationRenewalFormAction, rhs: RegistrationRenewalFormAction) -> Bool {
        switch (rhs, lhs) {
        case (.close, .close):
            return true
        case let (.editValue(lType, lValue, lDisplayValue, lUserProfile, _),
                  .editValue(rType, rValue, rDisplayValue, rUserProfile, _)):
            return lType == rType && lValue == rValue && lDisplayValue == rDisplayValue && lUserProfile == rUserProfile
        case let (.success(lModel), .success(rModel)):
            return lModel == rModel
        default:
            return false
        }
    }
}

private class RegistrationRenewalFormServicingMock: RegistrationRenewalFormServicing {
    // MARK: - sendConfirmation
    private(set) var sendConfirmationCallsCount = 0
    
    var result: Result<RegistrationRenewalSuccessModel, ApiError>?

    func sendConfirmation(password: String, completion: @escaping ((Result<RegistrationRenewalSuccessModel, ApiError>) -> Void)) {
        sendConfirmationCallsCount += 1
        if let result = result {
            completion(result)
        }
    }
}

private class RegistrationRenewalFormPresentingSpy: RegistrationRenewalFormPresenting {
    var viewController: RegistrationRenewalFormDisplay?

    // MARK: - presentForm
    private(set) var presentFormCallsCount = 0
    private(set) var formModel: RegistrationRenewalForm?

    func presentForm(_ formModel: RegistrationRenewalForm) {
        presentFormCallsCount += 1
        self.formModel = formModel
    }

    // MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var action: RegistrationRenewalFormAction?
    private(set) var editValueCompletionAction: ((String, String) -> Void)?

    func didNextStep(action: RegistrationRenewalFormAction) {
        if case let .editValue(_, _, _, _, completion) = action {
            editValueCompletionAction = completion
        }
        
        didNextStepActionCallsCount += 1
        self.action = action
    }
    
    // MARK: - presentErrorMessage
    private(set) var presentErrorPopupCallsCount = 0
    private(set) var errorTitle: String?
    private(set) var errorMessage: String?
    private(set) var errorButtonTitle: String?
    
    func presentErrorPopup(title: String?, message: String, buttonTitle: String) {
        presentErrorPopupCallsCount += 1
        errorTitle = title
        errorMessage = message
        errorButtonTitle = buttonTitle
    }

    // MARK: - presentLoading
    private(set) var presentLoadingCallsCount = 0

    func presentLoading() {
        presentLoadingCallsCount += 1
    }

    // MARK: - hideLoading
    private(set) var hideLoadingCallsCount = 0

    func hideLoading() {
        hideLoadingCallsCount += 1
    }
    
    private(set) var presentAuthCallsCount = 0
    
    func presentAuth() {
        presentAuthCallsCount += 1
    }
}

final class RegistrationRenewalFormViewModelTests: XCTestCase {
    private let serviceMock = RegistrationRenewalFormServicingMock()
    private let presenterSpy = RegistrationRenewalFormPresentingSpy()
    private let analyticsMock = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(analyticsMock)
    
    private lazy var sut = RegistrationRenewalFormViewModel(
        service: serviceMock,
        presenter: presenterSpy,
        dependencies: dependenciesMock,
        formModel: RegistrationRenewalForm.mock,
        userProfile: .unlimitedAccount
    )
    
    func testFetchFormData_ShouldPresentForm() {
        sut.fetchFormData()
        
        XCTAssertEqual(presenterSpy.presentFormCallsCount, 1)
        XCTAssertEqual(presenterSpy.formModel, RegistrationRenewalForm.mock)
    }
    
    func testDidSelectItem_WhenPassingAInvalidIndexPath_ShouldNotPresentAnyNewScreen() {
        sut.didSelectItem(at: IndexPath(row: 6, section: 0))
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 0)
    }
    
    func testDidSelectItem_WhenSelectedPhoneItem_ShouldPresentChangePhoneScreen() {
        sut.didSelectItem(at: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .editValue(
            of: .phone,
            fromValue: "11998871234",
            fromDisplayValue: "11998871234",
            userProfile: .unlimitedAccount,
            completion: { _, _ in })
        )
    }
    
    func testDidSelectItem_WhenSelectedPhoneItem_ShouldCallPresentForm() throws {
        sut.didSelectItem(at: IndexPath(row: 0, section: 0))
        
        let value = "279234244324"
        let displayValue = "(27) 92342-44324"
        presenterSpy.editValueCompletionAction?(value, displayValue)
        
        XCTAssertEqual(presenterSpy.presentFormCallsCount, 1)
        let updatedItem = try XCTUnwrap(presenterSpy.formModel?.items.first)
        XCTAssertEqual(updatedItem.value, value)
        XCTAssertEqual(updatedItem.displayValue, displayValue)
    }
    
    func testDidSelectItem_WhenSelectedTelefoneItemComplete_ShouldCallPresentFormWhitAlertNil() throws {
        sut.fetchFormData()
        let fetchItem = try XCTUnwrap(presenterSpy.formModel?.items[0])
        
        sut.didSelectItem(at: IndexPath(row: 0, section: 0))
        
        let value = "fulano@email.com"
        let displayValue = "fulano@email.com"
        presenterSpy.editValueCompletionAction?(value, displayValue)
        
        let updatedItem = try XCTUnwrap(presenterSpy.formModel?.items[0])
        
        XCTAssertNil(updatedItem.alertText)
        XCTAssertNotNil(fetchItem.alertText)
    }
    
    func testDidSelectItem_WhenSelectedEmailItem_ShouldPresentChangeEmailScreen() {
        sut.didSelectItem(at: IndexPath(row: 1, section: 0))
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .editValue(
            of: .email,
            fromValue: "fulano@email.com",
            fromDisplayValue: "fulano@email.com",
            userProfile: .unlimitedAccount,
            completion: { _, _ in })
        )
    }
    
    func testDidSelectItem_WhenSelectedEmailItem_ShouldCallPresentForm() throws {
        sut.didSelectItem(at: IndexPath(row: 1, section: 0))
        
        let value = "3"
        let displayValue = "fulano.borges@gmail.com"
        presenterSpy.editValueCompletionAction?(value, displayValue)
        
        XCTAssertEqual(presenterSpy.presentFormCallsCount, 1)
        let updatedItem = try XCTUnwrap(presenterSpy.formModel?.items[1])
        XCTAssertEqual(updatedItem.value, value)
        XCTAssertEqual(updatedItem.displayValue, displayValue)
    }
    
    func testDidSelectItem_WhenSelectedIncomeRangeItem_ShouldPresentChangeIncomeRangeScreen() {
        sut.didSelectItem(at: IndexPath(row: 2, section: 0))
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .editValue(
            of: .incomeRange,
            fromValue: "21212",
            fromDisplayValue: "De R$10.000,00 a R$20.000,00",
            userProfile: .unlimitedAccount,
            completion: { _, _ in })
        )
    }
    
    func testDidSelectItem_WhenSelectedIncomeRangeItem_ShouldCallPresentForm() throws {
        sut.didSelectItem(at: IndexPath(row: 2, section: 0))
        
        let value = "21212"
        let displayValue = "De R$1.000 a 2.000"
        presenterSpy.editValueCompletionAction?(value, displayValue)
        
        XCTAssertEqual(presenterSpy.presentFormCallsCount, 1)
        let updatedItem = try XCTUnwrap(presenterSpy.formModel?.items[2])
        XCTAssertEqual(updatedItem.value, value)
        XCTAssertEqual(updatedItem.displayValue, displayValue)
    }
    
    func testDidSelectItem_WhenSelectedAddressItem_ShouldPresentChangeAddressScreen() {
        sut.didSelectItem(at: IndexPath(row: 3, section: 0))
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .editValue(
            of: .address,
            fromValue: "93483",
            fromDisplayValue: "Av. Jerônimo Monteiro, 1000, A2 apto 101 - Vitória, ES - 29100100",
            userProfile: .unlimitedAccount,
            completion: { _, _ in })
        )
    }
    
    func testDidSelectItem_WhenSelectedAddressItem_ShouldCallPresentForm() throws {
        sut.didSelectItem(at: IndexPath(row: 3, section: 0))
        
        let value = "53564"
        let displayValue = "Av. Jerônimo Monteiro, 1000, A2 apto 101 - Vitória, ES - 29100100"
        presenterSpy.editValueCompletionAction?(value, displayValue)
        
        XCTAssertEqual(presenterSpy.presentFormCallsCount, 1)
        let updatedItem = try XCTUnwrap(presenterSpy.formModel?.items[3])
        XCTAssertEqual(updatedItem.value, value)
        XCTAssertEqual(updatedItem.displayValue, displayValue)
    }
    
    func testDidSelectItem_WhenSelectedWealthRangeItem_ShouldPresentChangeWealthRangeScreen() {
        sut.didSelectItem(at: IndexPath(row: 4, section: 0))
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .editValue(
            of: .wealthRange,
            fromValue: "75363",
            fromDisplayValue: "De R$1.000 a 2.000",
            userProfile: .unlimitedAccount,
            completion: { _, _ in })
        )
    }
    
    func testDidSelectItem_WhenSelectedWealthRangeItem_ShouldCallPresentForm() throws {
        sut.didSelectItem(at: IndexPath(row: 4, section: 0))
        
        let value = "45345"
        let displayValue = "De R$1.000 a 2.000"
        presenterSpy.editValueCompletionAction?(value, displayValue)
        
        XCTAssertEqual(presenterSpy.presentFormCallsCount, 1)
        let updatedItem = try XCTUnwrap(presenterSpy.formModel?.items[4])
        XCTAssertEqual(updatedItem.value, value)
        XCTAssertEqual(updatedItem.displayValue, displayValue)
    }
    
    func testDidSelectItem_WhenSelectedProfessionItem_ShouldPresentChangeProfessionScreen() {
        sut.didSelectItem(at: IndexPath(row: 5, section: 0))
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .editValue(
            of: .profession,
            fromValue: "03484",
            fromDisplayValue: "Economista",
            userProfile: .unlimitedAccount,
            completion: { _, _ in })
        )
    }
    
    func testDidSelectItem_WhenSelectedProfessionItem_ShouldCallPresentForm() throws {
        sut.didSelectItem(at: IndexPath(row: 5, section: 0))
        
        let value = "45435"
        let displayValue = "Economista"
        presenterSpy.editValueCompletionAction?(value, displayValue)
        
        XCTAssertEqual(presenterSpy.presentFormCallsCount, 1)
        let updatedItem = try XCTUnwrap(presenterSpy.formModel?.items[5])
        XCTAssertEqual(updatedItem.value, value)
        XCTAssertEqual(updatedItem.displayValue, displayValue)
    }
    
    func testDidClose_ShouldPerformCloseAction() {
        sut.didClose()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .close)
    }
    
    func testSendConfirmation_WhenResponseIsSuccess_ShouldPresentSuccess() {
        let responseModel = RegistrationRenewalSuccessModel(
            title: "Cadastro atualizado",
            description: "Obrigado por manter seu cadastro sempre atualizado!"
        )
        serviceMock.result = .success(responseModel)
        
        sut.sendConfirmation(password: "1234")
        
        XCTAssertEqual(serviceMock.sendConfirmationCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .success(responseModel))
    }
    
    func testSendConfirmation_WhenResponseIsFailure_ShouldPresentError() {
        serviceMock.result = .failure(ApiError.connectionFailure)
        
        sut.sendConfirmation(password: "1234")
        
        XCTAssertEqual(serviceMock.sendConfirmationCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 1)
        XCTAssertNotNil(presenterSpy.errorTitle)
        XCTAssertNotNil(presenterSpy.errorMessage)
        XCTAssertNotNil(presenterSpy.errorButtonTitle)
    }
    
    func testSendConfirmation_WhenCalled_ShouldCallPresentAuth() {
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.presentAuthCallsCount, 1)
    }
    
    // Tracking of events
    
    func testDidSelectItem_WhenSelectedItem_ShouldSendPhoneEditingEvent() {
        sut.didSelectItem(at: IndexPath(row: 0, section: 0))
        
        let expectedEvent = RegistrationRenewalEvent.didTapEditField(userProfile: .unlimitedAccount, selectedField: .phone)
            .event()
        XCTAssert(analyticsMock.equals(to: expectedEvent))
    }
    
    func testDidSelectItem_WhenSelectedItemAndCompletionActionCalled_ShouldSendUpdateItemEvent() {
        sut.didSelectItem(at: IndexPath(row: 0, section: 0))
        presenterSpy.editValueCompletionAction?("5345", "Any value")
        
        let expectedEvent = RegistrationRenewalEvent.didUpdateField(userProfile: .unlimitedAccount, itemType: .phone).event()
        XCTAssert(analyticsMock.equals(to: expectedEvent))
    }
    
    func testSendConfirmation_WhenResponseFailureNotUpdateAlertText_ShouldPresentError() {
        serviceMock.result = .failure(ApiError.connectionFailure)
        
        sut.sendConfirmation(password: "1234")
        
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 1)
        XCTAssertNotNil(presenterSpy.errorTitle)
        XCTAssertNotNil(presenterSpy.errorMessage)
    }
    
    func testSendConfirmation_WhenResponseFailureUpdateAlertText_ShouldPresentError() throws {
        let requestError = try MockDecodable<RequestError>().loadCodableObject(resource: "confirmationError")
        serviceMock.result = .failure(ApiError.otherErrors(body: requestError))
        
        sut.sendConfirmation(password: "1234")
        let fetchItem = try XCTUnwrap(presenterSpy.formModel?.items)
        
        XCTAssertEqual(presenterSpy.presentFormCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 1)
        XCTAssertEqual(fetchItem[0].alertText, "Este phone ja pertence a outro usuário")
        XCTAssertEqual(fetchItem[1].alertText, "Este e-mail ja pertence a outro usuário")
    }
}
