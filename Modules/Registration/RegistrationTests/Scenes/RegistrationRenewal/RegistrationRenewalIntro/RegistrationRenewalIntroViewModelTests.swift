import AnalyticsModule
import Core
@testable import Registration
import XCTest

extension RegistrationRenewalIntroAction: Equatable {
    public static func == (lhs: RegistrationRenewalIntroAction, rhs: RegistrationRenewalIntroAction) -> Bool {
        switch (rhs, lhs) {
        case (.close, .close):
            return true
        case let (.openForm(lModel, lUserProfile), .openForm(rModel, rUserProfile)):
            return lModel == rModel && lUserProfile == rUserProfile
        default:
            return false
        }
    }
}

private final class RegistrationRenewalIntroServicingMock: RegistrationRenewalIntroServicing {
    // MARK: - getRegistrationRenewalData
    private(set) var getRegistrationRenewalDataCallsCount = 0
    var result: Result<RegistrationRenewalResponse, ApiError>?

    func getRegistrationRenewalData(completion: @escaping (Result<RegistrationRenewalResponse, ApiError>) -> Void) {
        getRegistrationRenewalDataCallsCount += 1
        if let result = result {
            completion(result)
        }
    }
}

private final class RegistrationRenewalIntroPresentingSpy: RegistrationRenewalIntroPresenting {
    var viewController: RegistrationRenewalIntroDisplay?

    // MARK: - showLoading
    private(set) var showLoadingCallsCount = 0

    func showLoading() {
        showLoadingCallsCount += 1
    }

    // MARK: - hideLoading
    private(set) var hideLoadingCallsCount = 0

    func hideLoading() {
        hideLoadingCallsCount += 1
    }

    // MARK: - presentUpdatedInformation
    private(set) var presentUpdatedInformationCallsCount = 0
    private(set) var title: String?
    private(set) var description: String?

    func presentUpdatedInformation(title: String, description: String) {
        presentUpdatedInformationCallsCount += 1
        self.title = title
        self.description = description
    }
    
    // MARK: - presentErrorMessage
    private(set) var presentErrorPopupCallsCount = 0
    private(set) var errorTitle: String?
    private(set) var errorMessage: String?
    private(set) var errorButtonTitle: String?
    private(set) var erroCloseCompletion: (() -> Void)?
    
    func presentErrorPopup(title: String?, message: String, buttonTitle: String, closeCompletion: (() -> Void)?) {
        presentErrorPopupCallsCount += 1
        errorTitle = title
        errorMessage = message
        errorButtonTitle = buttonTitle
        erroCloseCompletion = closeCompletion
    }

    // MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var action: RegistrationRenewalIntroAction?

    func didNextStep(action: RegistrationRenewalIntroAction) {
        didNextStepActionCallsCount += 1
        self.action = action
    }
}

final class RegistrationRenewalIntroViewModelTests: XCTestCase {
    private let serviceMock = RegistrationRenewalIntroServicingMock()
    private let presenterSpy = RegistrationRenewalIntroPresentingSpy()
    private let analyticsMock = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(analyticsMock)
    
    private lazy var sut = RegistrationRenewalIntroViewModel(
        service: serviceMock,
        presenter: presenterSpy,
        dependencies: dependenciesMock,
        origin: .settings
    )
    
    func testFetchChecklistFormData_WhenIsSuccessWithWelcomeScreen_ShouldUpdateInformation() throws {
        let decoder = JSONFileDecoder<RegistrationRenewalResponse>()
        let successResponse = try decoder.load(
            resource: "registrationRenewalWelcomeScreenMock",
            typeDecoder: .convertFromSnakeCase
        )
        
        serviceMock.result = .success(successResponse)
        
        sut.fetchChecklistFormData()
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(serviceMock.getRegistrationRenewalDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentUpdatedInformationCallsCount, 1)
        XCTAssertEqual(presenterSpy.title, successResponse.welcomeScreen?.title)
        XCTAssertEqual(presenterSpy.description, successResponse.welcomeScreen?.description)
    }
    
    func testFetchChecklistFormData_WhenIsSuccessWithNoWelcomeScreen_ShouldOpenFormScreen() throws {
        let decoder = JSONFileDecoder<RegistrationRenewalResponse>()
        let successResponse = try decoder.load(
            resource: "registrationRenewalNoWelcomeScreenMock",
            typeDecoder: .convertFromSnakeCase
        )
        
        serviceMock.result = .success(successResponse)
        
        sut.fetchChecklistFormData()
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 0)
        XCTAssertEqual(serviceMock.getRegistrationRenewalDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentUpdatedInformationCallsCount, 0)
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .openForm(model: successResponse.formScreen, userProfile: .unlimitedAccount))
    }
    
    func testFetchChecklistFormData_WhenIsFailure_ShouldPresentErrorMessage() throws {
        serviceMock.result = .failure(.connectionFailure)
  
        sut.fetchChecklistFormData()
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(serviceMock.getRegistrationRenewalDataCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 1)
        XCTAssertNotNil(presenterSpy.errorTitle)
        XCTAssertNotNil(presenterSpy.errorMessage)
        XCTAssertNotNil(presenterSpy.errorButtonTitle)
        XCTAssertNotNil(presenterSpy.erroCloseCompletion)
    }
    
    func testDidConfirm_WhenFormModelIsLoaded_ShoulOpenForm() throws {
        let decoder = JSONFileDecoder<RegistrationRenewalResponse>()
        let successResponse = try decoder.load(
            resource: "registrationRenewalWelcomeScreenMock",
            typeDecoder: .convertFromSnakeCase
        )
        serviceMock.result = .success(successResponse)
        sut.fetchChecklistFormData()
        
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .openForm(model: successResponse.formScreen, userProfile: .limitedAccount))
    }
    
    func testDidConfirm_WhenFormModelIsNotLoaded_ShoulDoNothing() throws {
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 0)
    }
    
    func testDidClose_ShouldPerformClose() {
        sut.didClose()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .close)
    }
    
    // Tracking of events
    
    func testFetchChecklistFormData_WhenIsSuccessWithWelcomeScreen_ShouldSendScreenViewedEvent() throws {
        let decoder = JSONFileDecoder<RegistrationRenewalResponse>()
        let successResponse = try decoder.load(
            resource: "registrationRenewalWelcomeScreenMock",
            typeDecoder: .convertFromSnakeCase
        )
        
        serviceMock.result = .success(successResponse)
        
        sut.fetchChecklistFormData()
        
        let expectedEvent = RegistrationRenewalEvent.didViewIntro(userProfile: .limitedAccount, origin: .settings).event()
        XCTAssert(analyticsMock.equals(to: expectedEvent))
    }
    
    func testDidConfirm_WhenFormModelIsLoaded_ShoulSendAnualDataUpdateStarted() throws {
        let decoder = JSONFileDecoder<RegistrationRenewalResponse>()
        let successResponse = try decoder.load(
            resource: "registrationRenewalWelcomeScreenMock",
            typeDecoder: .convertFromSnakeCase
        )
        serviceMock.result = .success(successResponse)
        sut.fetchChecklistFormData()
        
        sut.didConfirm()
        
        let expectedEvent = RegistrationRenewalEvent.didStarted(userProfile: .limitedAccount).event()
        XCTAssert(analyticsMock.equals(to: expectedEvent))
    }
}
