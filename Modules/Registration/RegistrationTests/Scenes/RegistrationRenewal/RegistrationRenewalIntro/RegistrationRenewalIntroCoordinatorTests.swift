@testable import Registration
import XCTest

final class RenewalIntroCoordinatorDelegateSpy: RegistrationRenewalIntroCoordinatorDelegate {
    private(set) var openFormCallsCount = 0
    private(set) var closeCallsCount = 0
    
    private(set) var formModel: RegistrationRenewalForm?
    private(set) var userProfile: RegistrationRenewalUserProfile?
    
    func close() {
        closeCallsCount += 1
    }
    
    func openForm(model: RegistrationRenewalForm, userProfile: RegistrationRenewalUserProfile ) {
        openFormCallsCount += 1
        formModel = model
        self.userProfile = userProfile
    }
}

final class RegistrationRenewalIntroCoordinatorTests: XCTestCase {
    private let navControllerSpy = NavigationControllerSpy(rootViewController: UIViewController())
    private let delegateSpy = RenewalIntroCoordinatorDelegateSpy()
    
    private lazy var sut: RegistrationRenewalIntroCoordinating = {
        let coordinator = RegistrationRenewalIntroCoordinator(delegate: delegateSpy)
        coordinator.viewController = navControllerSpy.topViewController
        return coordinator
    }()
    
    func testPerformAction_WhenActionIsClose_ShouldDismiss() {
        sut.perform(action: .close)
        
        XCTAssertEqual(delegateSpy.closeCallsCount, 1)
    }
    
    func testPerformAction_WhenActionIsOpenForm_ShouldPushFormScreen() throws {
        let formModel = RegistrationRenewalForm(title: "A Title", subtitle: "A Subtitle", items: [])
        
        sut.perform(action: .openForm(model: formModel, userProfile: .limitedAccount))
        
        XCTAssertEqual(delegateSpy.openFormCallsCount, 1)
        XCTAssertEqual(delegateSpy.formModel, formModel)
        XCTAssertEqual(delegateSpy.userProfile, .limitedAccount)
        XCTAssertEqual(navControllerSpy.pushViewControllerCallsCount, 1)
    }
}
