@testable import Registration
import XCTest

final private class RegistrationRenewalIntroCoordinatingSpy: RegistrationRenewalIntroCoordinating {
    var viewController: UIViewController?

    // MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var action: RegistrationRenewalIntroAction?

    func perform(action: RegistrationRenewalIntroAction) {
        performActionCallsCount += 1
        self.action = action
    }
}

final private class RegistrationRenewalIntroDisplaySpy: RegistrationRenewalIntroDisplay {
    // MARK: - showLoading
    private(set) var showLoadingCallsCount = 0

    func showLoading() {
        showLoadingCallsCount += 1
    }

    // MARK: - hideLoading
    private(set) var hideLoadingCallsCount = 0

    func hideLoading() {
        hideLoadingCallsCount += 1
    }

    // MARK: - updateLabels
    private(set) var updateLabelsCallsCount = 0
    private(set) var title: String?
    private(set) var description: String?

    func updateLabels(title: String, description: String) {
        updateLabelsCallsCount += 1
        self.title = title
        self.description = description
    }

    // MARK: - displayErrorPopup
    private(set) var displayErrorPopupCallsCount = 0
    private(set) var errorTitle: String?
    private(set) var errorMessage: String?
    private(set) var errorButtonTitle: String?
    private(set) var errorCloseCompletion: (() -> Void)?

    func displayErrorPopup(title: String?, message: String, buttonTitle: String, closeCompletion: (() -> Void)?) {
        displayErrorPopupCallsCount += 1
        errorTitle = title
        errorMessage = message
        errorButtonTitle = buttonTitle
        errorCloseCompletion = closeCompletion
    }
}

final class RegistrationRenewalIntroPresenterTests: XCTestCase {
    private let coordinatorSpy = RegistrationRenewalIntroCoordinatingSpy()
    private let viewControllerSpy = RegistrationRenewalIntroDisplaySpy()
    private lazy var sut: RegistrationRenewalIntroPresenting = {
        let presenter = RegistrationRenewalIntroPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPresentUpdatedInformation_ShouldUpdateLabels() {
        let title = "Abcd title"
        let description = "A description for the abcd title"
        
        sut.presentUpdatedInformation(title: title, description: description)
        
        XCTAssertEqual(viewControllerSpy.updateLabelsCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.title, title)
        XCTAssertEqual(viewControllerSpy.description, description)
    }
    
    func testPresentErrorMessage_ShouldDisplayErrorMessage() {
        let errorTitle = "A generic title message"
        let errorMessage = "A generic error message"
        let errorButtonTitle = "ok"
        
        sut.presentErrorPopup(title: errorTitle, message: errorMessage, buttonTitle: errorButtonTitle, closeCompletion: nil)
        
        XCTAssertEqual(viewControllerSpy.displayErrorPopupCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.errorTitle, errorTitle)
        XCTAssertEqual(viewControllerSpy.errorMessage, errorMessage)
        XCTAssertEqual(viewControllerSpy.errorButtonTitle, errorButtonTitle)
        XCTAssertNil(viewControllerSpy.errorCloseCompletion)
    }
    
    func testShowLoading_ShouldShowLoading() {
        sut.showLoading()
        
        XCTAssertEqual(viewControllerSpy.showLoadingCallsCount, 1)
    }
    
    func testhideLoading_ShouldHidLoading() {
        sut.hideLoading()
        
        XCTAssertEqual(viewControllerSpy.hideLoadingCallsCount, 1)
    }
    
    func testDidNextStep_WhenActionIsClose_ShouldPerformCloseAction() {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.action, .close)
    }
    
    func testDidNextStep_WhenActionIsOpenForm_ShouldPerformOpenFormAction() {
        let formModel = RegistrationRenewalForm(
            title: "Form Title",
            subtitle: "Form Subtitle",
            items: [
                RegistrationRenewalFormItem(title: "TitleA", value: "98346", displayValue: "abcd", type: .address),
                RegistrationRenewalFormItem(title: "TitleB", value: "84352", displayValue: "cdef", type: .profession)
            ]
        )
        
        sut.didNextStep(action: .openForm(model: formModel, userProfile: .picpayCard))
        
        XCTAssertEqual(coordinatorSpy.action, .openForm(model: formModel, userProfile: .picpayCard))
    }
}
