import FeatureFlag
@testable import Registration
import UI
import XCTest

private class RegistrationRenewalCodeCoordinatingSpy: RegistrationRenewalCodeCoordinating {
    var viewController: UIViewController?
    
    // MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var action: RegistrationRenewalCodeAction?

    func perform(action: RegistrationRenewalCodeAction) {
        performActionCallsCount += 1
        self.action = action
    }
}

private class RegistrationRenewalCodeDisplaySpy: RegistrationRenewalCodeDisplay {
    // MARK: - display
    private(set) var displayCallsCount = 0
    private(set) var displayTitle: String?
    private(set) var displayMessage: NSAttributedString?
    private(set) var displayPlaceholder: String?
    
    func display(title: String, message: NSAttributedString, placeholder: String) {
        displayCallsCount += 1
        displayTitle = title
        displayMessage = message
        displayPlaceholder = placeholder
    }
    // MARK: - updateResendCodeButton
    private(set) var updateResendCodeButtonCallsCount = 0
    private(set) var isResendCodeButtonHidden: Bool?
    private(set) var isResendCodeButtonEnabled: Bool = false
    private(set) var resendCodeButtonTitle: String?

    func updateResendCodeButton(isHidden: Bool, isEnabled: Bool, title: String?) {
        updateResendCodeButtonCallsCount += 1
        isResendCodeButtonHidden = isHidden
        isResendCodeButtonEnabled = isEnabled
        resendCodeButtonTitle = title
    }

    // MARK: - showLoading
    private(set) var showLoadingCallsCount = 0

    func showLoading() {
        showLoadingCallsCount += 1
    }

    // MARK: - hideLoading
    private(set) var hideLoadingCallsCount = 0

    func hideLoading() {
        hideLoadingCallsCount += 1
    }

    // MARK: - displayErrorPopup
    private(set) var displayErrorPopupCallsCount = 0
    private(set) var popupErrorTitle: String?
    private(set) var popupErrorMessage: String?
    private(set) var popupButtonTitle: String?
    private(set) var popupErrorCloseCompletion: (() -> Void)?

    func displayErrorPopup(title: String?, message: String, buttonTitle: String, closeCompletion: (() -> Void)?) {
        displayErrorPopupCallsCount += 1
        popupErrorTitle = title
        popupErrorMessage = message
        popupButtonTitle = buttonTitle
        popupErrorCloseCompletion = closeCompletion
    }
    
    // MARK: - cleanCodeTextField
    private(set) var cleanCodeTextFieldCallsCount = 0
    
    func cleanCodeTextField() {
        cleanCodeTextFieldCallsCount += 1
    }
}

final class RegistrationRenewalCodePresenterTests: XCTestCase {
    private let coordinatorSpy = RegistrationRenewalCodeCoordinatingSpy()
    private let viewControllerSpy = RegistrationRenewalCodeDisplaySpy()
    private let featureManagerMock = FeatureManagerMock()
    private lazy var sut: RegistrationRenewalCodePresenting = {
        let presenter = RegistrationRenewalCodePresenter(coordinator: coordinatorSpy, dependencies: DependencyContainerMock(featureManagerMock))
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPopulateView_WhenInputTypeIsPhoneAndOfuscationIsEnabled_ShouldPopulateViews() throws {
        featureManagerMock.override(key: .featureAppSecObfuscationMask, with: true)
        
        let mockedPhoneNumber = "(29) 94534-7453"
        let obfuscatePhoneNumber = try XCTUnwrap(StringObfuscationMasker.mask(phone: "29945347453"))
            
        sut.populateView(for: .phone, inputedValue: mockedPhoneNumber)
        
        XCTAssertEqual(viewControllerSpy.displayTitle, Strings.Validation.Phone.title)
        XCTAssertEqual(viewControllerSpy.displayMessage?.string, Strings.Validation.Phone.message(obfuscatePhoneNumber))
        XCTAssertEqual(viewControllerSpy.displayPlaceholder, Strings.Validation.Phone.placeholder)
    }
    
    func testPopulateView_WhenInputTypeIsPhoneAndOfuscationIsDisabled_ShouldPopulateViews() throws {
        featureManagerMock.override(key: .featureAppSecObfuscationMask, with: false)
        
        let mockedPhoneNumber = "(29) 94534-7453"
            
        sut.populateView(for: .phone, inputedValue: mockedPhoneNumber)
        
        XCTAssertEqual(viewControllerSpy.displayTitle, Strings.Validation.Phone.title)
        XCTAssertEqual(viewControllerSpy.displayMessage?.string, Strings.Validation.Phone.message(mockedPhoneNumber))
        XCTAssertEqual(viewControllerSpy.displayPlaceholder, Strings.Validation.Phone.placeholder)
    }
    
    func testPopulateView_WhenInputTypeIsEmailAndOfuscationIsEnabled_ShouldPopulateViews() throws {
        featureManagerMock.override(key: .featureAppSecObfuscationMask, with: true)
        
        let mockedEmail = "joao.almeida@gmail.com"
        let obfuscateEmail = try XCTUnwrap(StringObfuscationMasker.mask(email: mockedEmail))
        
        sut.populateView(for: .email, inputedValue: mockedEmail)
        
        XCTAssertEqual(viewControllerSpy.displayTitle, Strings.Validation.Email.title)
        XCTAssertEqual(viewControllerSpy.displayMessage?.string, Strings.Validation.Email.message(obfuscateEmail))
        XCTAssertEqual(viewControllerSpy.displayPlaceholder, Strings.Validation.Email.placeholder)
    }
    
    func testPopulateView_WhenInputTypeIsEmailAndOfuscationIsDisabled_ShouldPopulateViews() throws {
        featureManagerMock.override(key: .featureAppSecObfuscationMask, with: false)
        
        let mockedEmail = "joao.almeida@gmail.com"
        
        sut.populateView(for: .email, inputedValue: mockedEmail)
        
        XCTAssertEqual(viewControllerSpy.displayTitle, Strings.Validation.Email.title)
        XCTAssertEqual(viewControllerSpy.displayMessage?.string, Strings.Validation.Email.message(mockedEmail))
        XCTAssertEqual(viewControllerSpy.displayPlaceholder, Strings.Validation.Email.placeholder)
    }
    
    func testUpdateResendCodeButton_WhenStateIsEnabled_ShouldEnableResendButton() {
        sut.updateResendCodeButton(state: .enabled)
        
        XCTAssertEqual(viewControllerSpy.isResendCodeButtonHidden, false)
        XCTAssertEqual(viewControllerSpy.isResendCodeButtonEnabled, true)
        XCTAssertEqual(viewControllerSpy.resendCodeButtonTitle, Strings.Validation.resendCode)
    }
    
    func testUpdateResendCodeButton_WhenStateIsDisable_ShouldDisableResendButton() {
        sut.updateResendCodeButton(state: .disabled(counterValue: 60))
        
        XCTAssertEqual(viewControllerSpy.isResendCodeButtonHidden, false)
        XCTAssertEqual(viewControllerSpy.isResendCodeButtonEnabled, false)
        XCTAssertEqual(viewControllerSpy.resendCodeButtonTitle, Strings.Validation.counterMessage("60"))
    }
    
    func testUpdateResendCodeButton_WhenStateIsHidden_ShouldHideResendButton() {
        sut.updateResendCodeButton(state: .hidden)
        
        XCTAssertEqual(viewControllerSpy.isResendCodeButtonHidden, true)
        XCTAssertTrue(viewControllerSpy.isResendCodeButtonEnabled)
        XCTAssertNil(viewControllerSpy.resendCodeButtonTitle)
    }
    
    func testDidNextStep_WhenActionIsBackToForm_ShouldPerformBackToFormAction() {
        sut.didNextStep(action: .backToForm)
        
        XCTAssertEqual(coordinatorSpy.action, .backToForm)
    }
    
    func testDidNextStep_WhenActionIsSuccess_ShouldPerformSuccessAction() {
        let action = RegistrationRenewalCodeAction.showSuccessAndBackToForm(
            inputType: .phone,
            rawValue: "27943453453",
            formatedValue: "(27) 94345-3453"
        )
        
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinatorSpy.action, action)
    }
    
    func testPresentErrorPopup_ShouldPresentGenericErrorPopup() {
        let title = "Ops!"
        let message = "Email inválido"
        let buttonTitle = "Generic action"
        let completion = {}
        
        sut.presentErrorPopup(title: title, message: message, buttonTitle: buttonTitle, closeCompletion: completion)
        
        XCTAssertEqual(viewControllerSpy.popupErrorTitle, title)
        XCTAssertEqual(viewControllerSpy.popupErrorMessage, message)
        XCTAssertEqual(viewControllerSpy.popupButtonTitle, buttonTitle)
        XCTAssertNotNil(viewControllerSpy.popupErrorCloseCompletion)
    }
    
    func testShowLoading_ShouldShowLoading() {
        sut.showLoading()

        XCTAssertEqual(viewControllerSpy.showLoadingCallsCount, 1)
    }

    func testHideLoading_ShouldHideLoading() {
        sut.hideLoading()

        XCTAssertEqual(viewControllerSpy.hideLoadingCallsCount, 1)
    }
    
    func testCleanCodeTextField_ShouldCleanCodeTextField() {
        sut.cleanCodeTextField()
        
        XCTAssertEqual(viewControllerSpy.cleanCodeTextFieldCallsCount, 1)
    }
}
