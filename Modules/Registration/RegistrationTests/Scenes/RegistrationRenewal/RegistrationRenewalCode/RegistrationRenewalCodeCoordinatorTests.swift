@testable import Registration
import XCTest

private final class SuccessAnimatorMock: RegistrationRenewalCodeSuccessAnimating {
    private(set) var showSuccessAnimationCallsCount = 0
    private(set) var inputType: RegistrationRenewalInputType?
    
    func showSuccessAnimation(
        overView superview: UIView,
        for inputType: RegistrationRenewalInputType,
        completion: @escaping () -> Void
    ) {
        showSuccessAnimationCallsCount += 1
        self.inputType = inputType
        completion()
    }
}

final class RegistrationRenewalCodeCoordinatorTests: XCTestCase {
    private let navControllerSpy = NavigationControllerSpy(rootViewController: UIViewController())
    private let delegateMock = RegistrationRenewalInputDelegateSpy()
    private let successAnimatorMock = SuccessAnimatorMock()
    
    private lazy var sut: RegistrationRenewalCodeCoordinating = {
        let coordinator = RegistrationRenewalCodeCoordinator(
            delegate: delegateMock,
            successAnimator: successAnimatorMock
        )
        coordinator.viewController = navControllerSpy.topViewController
        return coordinator
    }()
    
    func testPerformAction_WhenActionIsSuccess_ShouldUpdateItemAndAnimateAndBackToForm() {
        let rawValue = "27938373938"
        let formatedValue = "(27) 93837-3938"
        let action = RegistrationRenewalCodeAction.showSuccessAndBackToForm(
            inputType: .phone,
            rawValue: rawValue,
            formatedValue: formatedValue
        )
        
        sut.perform(action: action)
        
        XCTAssertEqual(delegateMock.didChangeItemCallCount, 1)
        XCTAssertEqual(delegateMock.rawValue, rawValue)
        XCTAssertEqual(delegateMock.formatedValue, formatedValue)
        XCTAssertEqual(delegateMock.backToFormCallCount, 1)
        XCTAssertEqual(successAnimatorMock.showSuccessAnimationCallsCount, 1)
        XCTAssertEqual(successAnimatorMock.inputType, .phone)
    }
    
    func testPerformAction_WhenActionIsBackToForm_ShouldBackToForm() {
        sut.perform(action: .backToForm)
        
        XCTAssertEqual(delegateMock.backToFormCallCount, 1)
    }
}
