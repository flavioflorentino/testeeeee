import AnalyticsModule
import Core
@testable import Registration
import XCTest

final private class RegistrationRenewalCodeServicingMock: RegistrationRenewalCodeServicing {
    private(set) var sendConfirmationCodeCallsCount = 0
    private(set) var requestNewCodeCallsCount = 0

    var sendConfirmationCodeResult: Result<NoContent, ApiError>?
    var requestNewCodeResult: Result<RegistrationRenewalInputResponse, ApiError>?
    
    func sendConfirmationCode(codeValue: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        sendConfirmationCodeCallsCount += 1
        guard let result = sendConfirmationCodeResult else {
            XCTFail("Mocked retult for sendConfirmationCode method is nil")
            return
        }
        completion(result)
    }
    
    func requestNewCode(completion: @escaping (Result<RegistrationRenewalInputResponse, ApiError>) -> Void) {
        requestNewCodeCallsCount += 1
        guard let result = requestNewCodeResult else {
            XCTFail("Mocked retult for requestNewCode method is nil")
            return
        }
        completion(result)
    }
}

private final class RegistrationRenewalCodePresentingSpy: RegistrationRenewalCodePresenting {
    var viewController: RegistrationRenewalCodeDisplay?

    // MARK: - populateView
    private(set) var populateViewCallsCount = 0
    private(set) var inputType: RegistrationRenewalInputType?
    private(set) var inputedValue: String?

    func populateView(for inputType: RegistrationRenewalInputType, inputedValue: String) {
        populateViewCallsCount += 1
        self.inputType = inputType
        self.inputedValue = inputedValue
    }

    // MARK: - updateResendCodeButton
    private(set) var updateResendCodeButtonCallsCount = 0
    private(set) var resendCodeButtonState: ResendCodeButtonState?

    func updateResendCodeButton(state: ResendCodeButtonState) {
        updateResendCodeButtonCallsCount += 1
        resendCodeButtonState = state
    }

    // MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var action: RegistrationRenewalCodeAction?

    func didNextStep(action: RegistrationRenewalCodeAction) {
        didNextStepActionCallsCount += 1
        self.action = action
    }

    // MARK: - presentErrorPopup
    private(set) var presentErrorPopupCallsCount = 0
    private(set) var popupErrorTitle: String?
    private(set) var popupErrorMessage: String?
    private(set) var popupbuttonTitle: String?
    private(set) var popupErrorCloseCompletion: (() -> Void)?

    func presentErrorPopup(title: String?, message: String, buttonTitle: String, closeCompletion: (() -> Void)?) {
        presentErrorPopupCallsCount += 1
        popupErrorTitle = title
        popupErrorMessage = message
        popupbuttonTitle = buttonTitle
        popupErrorCloseCompletion = closeCompletion
    }

    // MARK: - showLoading
    private(set) var showLoadingCallsCount = 0

    func showLoading() {
        showLoadingCallsCount += 1
    }

    // MARK: - hideLoading
    private(set) var hideLoadingCallsCount = 0

    func hideLoading() {
        hideLoadingCallsCount += 1
    }
    
    // MARK: - cleanCodeTextField
    private(set) var cleanCodeTextFieldCallsCount = 0
    
    func cleanCodeTextField() {
        cleanCodeTextFieldCallsCount += 1
    }
}

final class RegistrationRenewalCodeViewModelTests: XCTestCase {
    private let serviceMock = RegistrationRenewalCodeServicingMock()
    private let presenterSpy = RegistrationRenewalCodePresentingSpy()
    private let analyticsMock = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(analyticsMock)
    
    private let mockedPhoneNumber = "(27) 94534-0938"
    private let mockedEmail = "augusto.pereira@gmail.com"
    private let mockedResendDelay = Int.random(in: 0...60)
    
    private func createViewModel(inputType: RegistrationRenewalInputType) -> RegistrationRenewalCodeViewModelInputs {
        RegistrationRenewalCodeViewModel(
            dependencies: dependenciesMock,
            service: serviceMock,
            presenter: presenterSpy,
            inputType: inputType,
            inputedValue: inputType == .phone ? mockedPhoneNumber : mockedEmail,
            codeResendDelay: mockedResendDelay,
            userProfile: .picpayCard
        )
    }
    
    func testPopulateView_WhenInputTypeIsPhone_ShouldPopulateView() {
        let sut = createViewModel(inputType: .phone)
        
        sut.populateView()

        XCTAssertEqual(presenterSpy.populateViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.inputType, .phone)
        XCTAssertEqual(presenterSpy.inputedValue, mockedPhoneNumber)
    }
    
    func testPopulateView_WhenInputTypeIsEmail_ShouldPopulateView() {
        let sut = createViewModel(inputType: .email)
        
        sut.populateView()

        XCTAssertEqual(presenterSpy.populateViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.inputType, .email)
        XCTAssertEqual(presenterSpy.inputedValue, mockedEmail)
    }
    
    func testDidAskToResendCode_WhenSuccessResponse_ShouldStartCounter() {
        let newDelay = Int.random(in: 0...60)
        serviceMock.requestNewCodeResult = .success(RegistrationRenewalInputResponse(delay: newDelay))
        let sut = createViewModel(inputType: .phone)
        
        sut.didAskToResendCode()
        
        XCTAssertEqual(serviceMock.requestNewCodeCallsCount, 1)
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
    }
    
    func testDidAskToResendCode_WhenResponseIsErrorOfExpiredRequest_ShouldPresentError() {
        let requestError: RequestError = {
            var requestError = RequestError()
            requestError.title = "Ops!"
            requestError.message = "Código inválido"
            requestError.code = "1001"
            return requestError
        }()
        serviceMock.requestNewCodeResult = .failure(ApiError.badRequest(body: requestError))
        let sut = createViewModel(inputType: .phone)
        
        sut.didAskToResendCode()
        
        XCTAssertEqual(serviceMock.requestNewCodeCallsCount, 1)
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 1)
        XCTAssertEqual(presenterSpy.popupErrorTitle, requestError.title)
        XCTAssertEqual(presenterSpy.popupErrorMessage, requestError.message)
        XCTAssertEqual(presenterSpy.popupbuttonTitle, Strings.Button.back)
        XCTAssertNotNil(presenterSpy.popupErrorCloseCompletion)
    }
    
    func testDidAskToResendCode_WhenResponseIsGenericError_ShouldPresentError() {
        let requestError = RequestError()
        serviceMock.requestNewCodeResult = .failure(ApiError.badRequest(body: requestError))
        let sut = createViewModel(inputType: .phone)
        
        sut.didAskToResendCode()
        
        XCTAssertEqual(serviceMock.requestNewCodeCallsCount, 1)
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 1)
        XCTAssertEqual(presenterSpy.popupErrorTitle, requestError.title)
        XCTAssertEqual(presenterSpy.popupErrorMessage, requestError.message)
        XCTAssertEqual(presenterSpy.popupbuttonTitle, Strings.Button.ok)
        XCTAssertNil(presenterSpy.popupErrorCloseCompletion)
    }
    
    func testUpdateCodeValue_WhenCodeHasLessThanFourCharacters_ShouldNotCallApi() {
        let sut = createViewModel(inputType: .phone)
        
        sut.updateCodeValue("843")
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 0)
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 0)
        XCTAssertEqual(serviceMock.sendConfirmationCodeCallsCount, 0)
    }
    
    func testUpdateCodeValue_WhenCodeIsNil_ShouldNotCallApi() {
        let sut = createViewModel(inputType: .phone)
        
        sut.updateCodeValue(nil)
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 0)
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 0)
        XCTAssertEqual(serviceMock.sendConfirmationCodeCallsCount, 0)
    }
    
    func testUpdateCodeValue_WhenCodeHasFourCharactersAndApiRespondsSuccess_ShouldPerformSuccess() {
        serviceMock.sendConfirmationCodeResult = .success(NoContent())
        let sut = createViewModel(inputType: .phone)
        
        sut.updateCodeValue("6534")
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(serviceMock.sendConfirmationCodeCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        let expectedAction: RegistrationRenewalCodeAction = .showSuccessAndBackToForm(
            inputType: .phone,
            rawValue: "27945340938",
            formatedValue: mockedPhoneNumber
        )
        XCTAssertEqual(presenterSpy.action, expectedAction)
    }
    
    func testUpdateCodeValue_WhenCodeHasFourCharactersAndApiRespondsStandardError_ShouldPresentError() {
        let requestError: RequestError = {
            var requestError = RequestError()
            requestError.title = "Ops!"
            requestError.message = "Código inválido"
            requestError.code = "1000"
            return requestError
        }()
        serviceMock.sendConfirmationCodeResult = .failure(ApiError.badRequest(body: requestError))
        let sut = createViewModel(inputType: .phone)
        
        sut.updateCodeValue("6534")
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 1)
        XCTAssertEqual(presenterSpy.popupErrorTitle, requestError.title)
        XCTAssertEqual(presenterSpy.popupErrorMessage, requestError.message)
        XCTAssertEqual(presenterSpy.popupbuttonTitle, Strings.Button.ok)
        XCTAssertEqual(presenterSpy.updateResendCodeButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.resendCodeButtonState, .hidden)
    }
    
    func testUpdateCodeValue_WhenCodeHasFourCharactersAndApiRespondsExpiredConfirmationRequest_ShouldPresentError() {
        let requestError: RequestError = {
            var requestError = RequestError()
            requestError.title = "Ops!"
            requestError.message = "Código inválido"
            requestError.code = "1001"
            return requestError
        }()
        serviceMock.sendConfirmationCodeResult = .failure(ApiError.badRequest(body: requestError))
        let sut = createViewModel(inputType: .phone)
        
        sut.updateCodeValue("6534")
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 1)
        XCTAssertEqual(presenterSpy.popupErrorTitle, requestError.title)
        XCTAssertEqual(presenterSpy.popupErrorMessage, requestError.message)
        XCTAssertEqual(presenterSpy.popupbuttonTitle, Strings.Button.back)
        XCTAssertNotNil(presenterSpy.popupErrorCloseCompletion)
        XCTAssertEqual(presenterSpy.updateResendCodeButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.resendCodeButtonState, .hidden)
    }
    
    func testUpdateCodeValue_WhenCodeHasFourCharactersAndApiRespondsGenericError_ShouldPresentError() {
        let requestError = RequestError()
        serviceMock.sendConfirmationCodeResult = .failure(ApiError.badRequest(body: requestError))
        let sut = createViewModel(inputType: .phone)
        
        sut.updateCodeValue("6534")
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideLoadingCallsCount, 1)
        XCTAssertEqual(presenterSpy.presentErrorPopupCallsCount, 1)
        XCTAssertEqual(presenterSpy.popupErrorTitle, requestError.title)
        XCTAssertEqual(presenterSpy.popupErrorMessage, requestError.message)
        XCTAssertEqual(presenterSpy.popupbuttonTitle, Strings.Button.ok)
        XCTAssertNil(presenterSpy.popupErrorCloseCompletion)
        XCTAssertEqual(presenterSpy.updateResendCodeButtonCallsCount, 1)
        XCTAssertEqual(presenterSpy.resendCodeButtonState, .hidden)
    }
    
    func testDidBack_ShouldPresentConfirmationBackPopup() {
        let sut = createViewModel(inputType: .phone)
        
        sut.didBack()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .backToPreviousView)
    }
    
    // Tracking of events
    
    func testPopulateView_ShouldSendCodeScreenViewedEvent() {
        let sut = createViewModel(inputType: .email)
        
        sut.populateView()

        let expectedEvent = RegistrationRenewalEvent.didViewCodeValidation(userProfile: .picpayCard, inputType: .email).event()
        XCTAssert(analyticsMock.equals(to: expectedEvent))
    }
    
    func testDidAskToResendCode_ShouldSendAskToResendCodeEvent() {
        serviceMock.requestNewCodeResult = .success(RegistrationRenewalInputResponse(delay: 10))
        let sut = createViewModel(inputType: .email)
        
        sut.didAskToResendCode()
        
        let expectedEvent = RegistrationRenewalEvent.didAskToResentCode(userProfile: .picpayCard, inputType: .email).event()
        XCTAssertTrue(analyticsMock.equals(to: expectedEvent))
    }
    
    func testUpdateCodeValue_WhenValidSizeCodeAndApiRespondsInvalidCodeError_ShouldSendInvalidCodeErrorEvent() {
        let requestError: RequestError = {
            var requestError = RequestError()
            requestError.code = "1004"
            return requestError
        }()
        serviceMock.sendConfirmationCodeResult = .failure(ApiError.badRequest(body: requestError))
        let sut = createViewModel(inputType: .phone)
        
        sut.updateCodeValue("6534")
        
        let expectedEvent = RegistrationRenewalEvent.didReceiveInvalidCodeError(userProfile: .picpayCard, inputType: .phone)
            .event()
        XCTAssertTrue(analyticsMock.equals(to: expectedEvent))
    }
    
    func testUpdateCodeValue_WhenValidSizeCodeAndApiRespondsStandardError_ShouldSendInvalidCodeErrorEvent() {
        let requestError: RequestError = {
            var requestError = RequestError()
            requestError.message = "An standard error message"
            requestError.code = "1000"
            return requestError
        }()
        serviceMock.sendConfirmationCodeResult = .failure(ApiError.badRequest(body: requestError))
        let sut = createViewModel(inputType: .phone)
        
        sut.updateCodeValue("6534")
        
        let expectedEvent = RegistrationRenewalEvent.didReceiveFieldUpdateError(
            userProfile: .picpayCard,
            itemType: .phone,
            errorType: requestError.message)
        .event()
        XCTAssertTrue(analyticsMock.equals(to: expectedEvent))
    }
    
    func testUpdateCodeValue_WhenValidSizeCodeAndApiRespondsReachedMaxNumberOfAtteptsError_ShouldSendCodeLimitErrorEvent() {
        let requestError: RequestError = {
            var requestError = RequestError()
            requestError.code = "1003"
            return requestError
        }()
        serviceMock.sendConfirmationCodeResult = .failure(ApiError.badRequest(body: requestError))
        let sut = createViewModel(inputType: .phone)
        
        sut.updateCodeValue("6534")
        
        let expectedEvent = RegistrationRenewalEvent.didReceiveCodeLimitExceedError(userProfile: .picpayCard, inputType: .phone)
            .event()
        XCTAssertTrue(analyticsMock.equals(to: expectedEvent))
    }
    
    func testUpdateCodeValue_WhenValidSizeCodeAndApiRespondsGenericError_ShouldSendCodeLimitErrorEvent() {
        let requestError = RequestError()
        serviceMock.sendConfirmationCodeResult = .failure(ApiError.badRequest(body: requestError))
        let sut = createViewModel(inputType: .email)
        
        sut.updateCodeValue("6534")
        
        let expectedEvent = RegistrationRenewalEvent.didReceiveFieldUpdateError(
            userProfile: .picpayCard,
            itemType: .email,
            errorType: "Ocorreu um erro ao carregar as informações, tente novamente."
        )
        .event()
        XCTAssertTrue(analyticsMock.equals(to: expectedEvent))
    }
}
