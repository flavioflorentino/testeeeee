@testable import Registration
import XCTest

private final class RenewalSuccessCoordinatorSpy: RenewalSuccessCoordinating {
    private(set) var callPerform = 0
    
    private(set) var action: RenewalSuccessAction?
    
    var viewController: UIViewController?
    
    func perform(action: RenewalSuccessAction) {
        callPerform += 1
        self.action = action
    }
}

private final class RenewalSuccessDisplaySpy: RenewalSuccessDisplay {
    private(set) var callDisplay = 0
    
    private(set) var title: String?
    private(set) var subtitle: String?
    
    func display(title: String, subtitle: String) {
        callDisplay += 1
        self.title = title
        self.subtitle = subtitle
    }
}

final class RenewalSuccessPresenterTest: XCTestCase {
    private let coordinatorSpy = RenewalSuccessCoordinatorSpy()
    private let displaySpy = RenewalSuccessDisplaySpy()
    
    private lazy var sut: RenewalSuccessPresenting = {
        let sut = RenewalSuccessPresenter(coordinator: coordinatorSpy)
        sut.viewController = displaySpy
        
        return sut
    }()
    
    func testPresenterView_WhenCalled_ShouldCallDisplayTitle() {
        sut.presenterView(title: "title", subtitle: "subtitle")
        
        XCTAssertEqual(displaySpy.callDisplay, 1)
        XCTAssertEqual(displaySpy.title, "title")
    }
    
    func testPresenterView_WhenCalled_ShouldCallDisplaySubtitle() {
        sut.presenterView(title: "title", subtitle: "subtitle")
        
        XCTAssertEqual(displaySpy.callDisplay, 1)
        XCTAssertEqual(displaySpy.subtitle, "subtitle")
    }
    
    func testDidNextStep_WhenBack_ShouldCallPerform() {
        sut.didNextStep(action: .back)
        
        XCTAssertEqual(coordinatorSpy.callPerform, 1)
        XCTAssertEqual(coordinatorSpy.action, .back)
    }
}
