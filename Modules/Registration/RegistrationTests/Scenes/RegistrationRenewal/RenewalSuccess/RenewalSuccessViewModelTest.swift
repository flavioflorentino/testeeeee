@testable import Registration
import XCTest

private final class RenewalSuccessPresenterSpy: RenewalSuccessPresenting {
    private(set) var callPresentView = 0
    private(set) var callDidNextStep = 0
    
    private(set) var title: String?
    private(set) var subtitle: String?
    private(set) var action: RenewalSuccessAction?
    
    var viewController: RenewalSuccessDisplay?
    
    func presenterView(title: String, subtitle: String) {
        callPresentView += 1
        self.title = title
        self.subtitle = subtitle
    }
    
    func didNextStep(action: RenewalSuccessAction) {
        callDidNextStep += 1
        self.action = action
    }
}

final class RenewalSuccessViewModelTest: XCTestCase {
    private let presenterSpy = RenewalSuccessPresenterSpy()
    private var model = RegistrationRenewalSuccessModel(title: "title", description: "description")
    
    private lazy var sut: RenewalSuccessViewModelInputs = {
        let sut = RenewalSuccessViewModel(
            model: model,
            presenter: presenterSpy
        )
        
        return sut
    }()
    
    func testUpdateView_WhenCalled_ShouldCallPresenterView() {
        sut.updateView()

        XCTAssertEqual(presenterSpy.callPresentView, 1)
        XCTAssertEqual(presenterSpy.title, model.title)
        XCTAssertEqual(presenterSpy.subtitle, model.description)
    }
    
    func testDidBack_WhenCalled_ShouldCallDidNextStepActionBack() {
        sut.didBack()

        XCTAssertEqual(presenterSpy.callDidNextStep, 1)
        XCTAssertEqual(presenterSpy.action, .back)
    }
}
