@testable import Registration
@testable import UI
import XCTest

private final class RegistrationComplianceReviewCoordinatorSpy: RegistrationComplianceReviewCoordinating {
    var viewController: UIViewController?
    private(set) var callPerformCount = 0
    private(set) var action: RegistrationComplianceReviewAction?
    
    func perform(action: RegistrationComplianceReviewAction) {
        self.callPerformCount += 1
        self.action = action
    }
}

private final class RegistrationCompReviewViewControllerSpy: RegistrationComplianceReviewDisplay {
    private(set) var callUpdateBirthdateErrorStateCount = 0
    private(set) var callUpdateButtonStateCount = 0
    private(set) var callUpdateNameErrorStateCount = 0
    private(set) var callDisplayContentCount = 0
    private(set) var callDisplayErrorCount = 0
    private(set) var callDismissLoadingCount = 0
    private(set) var callDisplayDescriptionCount = 0
    private(set) var callDisplayDocumentCount = 0
    private(set) var documentText: String?
    private(set) var updateBirthdateErrorMessage: String?
    private(set) var updateNameErrorStateErrorMessage: String?
    private(set) var enabled: Bool?
    
    func updateBirthdateErrorState(errorMessage: String?) {
        callUpdateBirthdateErrorStateCount += 1
        updateBirthdateErrorMessage = errorMessage
    }
    
    func updateNameErrorState(errorMessage: String?) {
        callUpdateNameErrorStateCount += 1
        updateNameErrorStateErrorMessage = errorMessage
    }
    
    func updateButtonState(enabled: Bool) {
        callUpdateButtonStateCount += 1
        self.enabled = enabled
    }
    
    func displayError(error: StatefulErrorViewModel) {
        callDisplayErrorCount += 1
    }
    
    func dismissLoading() {
        callDismissLoadingCount += 1
    }
    
    func displayDescription(description: NSAttributedString) {
        callDisplayDescriptionCount += 1
    }
    
    func displayDocument(documentText: String) {
        callDisplayDocumentCount += 1
        self.documentText = documentText
    }
}

final class RegistrationComplianceReviewPresenterTests: XCTestCase {
    private let viewController = RegistrationCompReviewViewControllerSpy()
    private let coordinator = RegistrationComplianceReviewCoordinatorSpy()
    private lazy var sut: RegistrationComplianceReviewPresenting = {
        let sut = RegistrationComplianceReviewPresenter(coordinator: coordinator)
        sut.viewController = viewController
        
        return sut
    }()
    
    func testHasNameError_WhenSet_ShouldCallUpdateNameErrorState() {
        sut.hasNameError = true
        
        XCTAssertEqual(viewController.callUpdateNameErrorStateCount, 1)
        XCTAssertNotNil(viewController.updateNameErrorStateErrorMessage)
    }
    
    func testHasBirthdateError_WhenSet_ShouldCallUpdateBirthdateErrorState() {
        sut.hasBirthdateError = true
        
        XCTAssertEqual(viewController.callUpdateBirthdateErrorStateCount, 1)
        XCTAssertNotNil(viewController.updateBirthdateErrorMessage)
    }
    
    func testPresentDocument_WhenCalled_ShouldCallDisplayDocument() {
        let someDocument = "000.000.000-00"
        sut.presentDocument(document: someDocument)
        
        XCTAssertEqual(viewController.callDisplayDocumentCount, 1)
    }
    
    func testDidNextStep_WhenCalledWithCloseAction_ShouldCallPerformClose() {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinator.callPerformCount, 1)
        XCTAssertEqual(coordinator.action, .close)
    }
    
    func testDidNextStep_WhenCalledWithIdentityValidationAction_ShouldCallPerformIdentityValidation() {
        let action = RegistrationComplianceReviewAction.identityValidation(token: "someToken", flow: "TFA", completion: { _ in })
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinator.callPerformCount, 1)
        XCTAssertEqual(coordinator.action, action)
    }
    
    func testDidNextStep_WhenCalledWithComplianceStatusAction_ShouldCallPerformComplianceStatus() {
        let action = RegistrationComplianceReviewAction.complianceStatus(status: .analysis)
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinator.callPerformCount, 1)
        XCTAssertEqual(coordinator.action, action)
    }
}
