import FeatureFlag
@testable import Registration
import XCTest
import Core
import AnalyticsModule

private final class RegistrationComplianceReviewPresenterSpy: RegistrationComplianceReviewPresenting {
    private(set) var callDidNextStepCount = 0
    private(set) var callPresentContentCount = 0
    private(set) var callPresentErrorCount = 0
    private(set) var callPresentDescriptionCount = 0
    private(set) var callDismissLoadingCount = 0
    private(set) var callPresentDocumentCount = 0
    private(set) var document: String?
    private(set) var action: RegistrationComplianceReviewAction?
    
    var hasBirthdateError: Bool = false
    var hasNameError: Bool = false
    var viewController: RegistrationComplianceReviewDisplay?
    
    func didNextStep(action: RegistrationComplianceReviewAction) {
        callDidNextStepCount += 1
        self.action = action
    }
    
    func presentError() {
        callPresentErrorCount += 1
    }
    
    func dismissLoading() {
        callDismissLoadingCount += 1
    }
    
    func presentDocument(document: String) {
        callPresentDocumentCount += 1
        self.document = document
    }
    
    func presentDescription() {
        callPresentDescriptionCount += 1
    }
}

private final class RegistrationComplianceReviewServiceSpy: RegistrationComplianceReviewServicing {
    private(set) var callValidateReviewCount = 0
    private(set) var callSendReviewCount = 0
    private(set) var callRequestStatusUpdateCount = 0
    private(set) var sendReviewName: String?
    private(set) var sendReviewBirthdate: String?
    
    var sendReviewResult: Result<RegistrationCompReviewSuccessModel, ApiError> = .success(
        RegistrationCompReviewSuccessModel(step: .success, flow: "TFA", token: RegistrationToken(code: "SomeToken", type: .permanent))
    )
    
    var requestStatusUpdateResult: Result<RegistrationCompReviewResultModel, ApiError> = .success(
        RegistrationCompReviewResultModel(step: .success, token: RegistrationToken(code: "SomeToken", type: .permanent))
    )
    
    func sendReview(name: String, birthdate: String, completion: @escaping ((Result<RegistrationCompReviewSuccessModel, ApiError>) -> Void)) {
        callSendReviewCount += 1
        sendReviewName = name
        sendReviewBirthdate = birthdate
        
        completion(sendReviewResult)
    }
    
    func requestStatusUpdate(completion: @escaping ((Result<RegistrationCompReviewResultModel, ApiError>) -> Void)) {
        callRequestStatusUpdateCount += 1
        
        completion(requestStatusUpdateResult)
    }
}

final class RegistrationComplianceReviewInteractorTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private let featureManagerMock = FeatureManagerMock()
    private lazy var dependenciesMock = DependencyContainerMock(RegistrationSetupMock(.tokenManager), analyticsSpy,featureManagerMock)
    private let model = RegistrationCompReviewModel(name: "someName", birthdate: "01/01/1999", cpf: "123.456.780-10")
    private let presenter = RegistrationComplianceReviewPresenterSpy()
    private let service = RegistrationComplianceReviewServiceSpy()
    private lazy var sut = RegistrationComplianceReviewInteractor(
        dependencies: dependenciesMock,
        model: model,
        service: service,
        presenter: presenter
    )
    
    func testUpdateBirthdateValue_WhenBirthdateIsValid_ShouldHasBirthdateErrorFalse() {
        let birthdate = "01/01/1999"
        sut.updateBirthdateValue(birthdate: birthdate)
        
        XCTAssertFalse(presenter.hasBirthdateError)
    }
    
    func testUpdateBirthdateValue_WhenBirthdateIsInvalid_ShouldHasBirthdateErrorTrue() {
        let birthdate = "31/02/1999"
        sut.updateBirthdateValue(birthdate: birthdate)
        
        XCTAssertTrue(presenter.hasBirthdateError)
    }
    
    func testUpdateNameValue_WhenNameIsValid_ShouldHasNameErrorFalse() {
        let name = "Joao da Silva"
        sut.updateNameValue(name: name)
        
        XCTAssertFalse(presenter.hasNameError)
    }
    
    func testUpdateNameValue_WhenNameIsInvalid_ShouldHasNameErrorTrue() {
        let name = String()
        sut.updateNameValue(name: name)
        
        XCTAssertTrue(presenter.hasNameError)
    }
}
