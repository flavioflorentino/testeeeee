@testable import Registration
import XCTest
import Core
import AnalyticsModule
import FeatureFlag

private final class RegistrationCompStatusPresentingSpy: RegistrationComplianceStatusPresenting {
    var viewController: RegistrationComplianceStatusDisplay?
    
    private(set) var presentWithStatusCallsCount = 0
    private(set) var didNextStepCallsCount = 0
    private(set) var presentDidNextStepCount = 0
    private(set) var presentInProgressAlertCallsCount = 0
    private(set) var presentErrorCallsCount = 0
    private(set) var status: RegistrationComplianceStatus?
    private(set) var action: RegistrationComplianceStatusAction?
    
    func present(with status: RegistrationComplianceStatus) {
        presentWithStatusCallsCount += 1
        self.status = status
    }
    func didNextStep(action: RegistrationComplianceStatusAction) {
        didNextStepCallsCount += 1
        self.action = action
    }
    
    func presentInProgressAlert() {
        presentInProgressAlertCallsCount += 1
    }
    
    func presentError() {
        presentErrorCallsCount += 1
    }
}

private final class RegistrationCompStatusServiceMock: RegistrationComplianceStatusServicing {
    private(set) var requestStatusUpdateCallsCount = 0
    var result: Result<RegistrationComplianceStatusModel, ApiError>?
    
    func requestStatusUpdate(completion: @escaping (Result<RegistrationComplianceStatusModel, ApiError>) -> Void) {
        requestStatusUpdateCallsCount += 1
        if let result = result {
            completion(result)
        }
    }
}

final class RegistrationCompStatusInteractorTests: XCTestCase {
    private let presenterSpy = RegistrationCompStatusPresentingSpy()
    private let serviceMock = RegistrationCompStatusServiceMock()
    private let analyticsSpy = AnalyticsSpy()
    private let registrationSetupMock = RegistrationSetupMock(.authManager)
    private let featureManagerMock = FeatureManagerMock()
    private lazy var dependenciesMock = DependencyContainerMock(registrationSetupMock, analyticsSpy, featureManagerMock)
    private lazy var sut = RegistrationComplianceStatusInteractor(
        presenter: presenterSpy,
        status: .approved,
        dependencies: dependenciesMock,
        service: serviceMock
    )
    
    func testgetContent_WhenCalledFromDisplay_ShouldPopulateView() {
        sut.getContent()
        XCTAssertEqual(presenterSpy.presentWithStatusCallsCount, 1)
    }
    
    func testPrimaryAction_WhenCalledFromDisplayWithApprovedStatus_ShouldCallCorrectAction() {
        sut = RegistrationComplianceStatusInteractor(
            presenter: presenterSpy,
            status: .approved,
            dependencies: dependenciesMock,
            service: serviceMock
        )
        
        let mockController = UIViewController()
        sut.primaryAction(from: mockController)
        
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
    }
    
    func testPrimaryAction_WhenCalledFromDisplayWithAnalysisStatus_ShouldCallRequestStatusUpdate() {
        sut = RegistrationComplianceStatusInteractor(
            presenter: presenterSpy,
            status: .analysis,
            dependencies: dependenciesMock,
            service: serviceMock
        )
        
        let mockController = UIViewController()
        sut.primaryAction(from: mockController)
        
        XCTAssertEqual(serviceMock.requestStatusUpdateCallsCount, 1)
    }
    
    func testPrimaryAction_WhenCalledFromDisplayWithAnalysisStatusAndReviewResult_ShouldCallReview() {
        let name = "Some Name"
        let birthdate = "23/11/1999"
        let cpf = "123.456.789-10"
        
        serviceMock.result = .success(
            RegistrationComplianceStatusModel(
                step: .inconsistency,
                consumer: RegistrationComplianceStatusConsumer(
                    name: name,
                    birthdate: birthdate,
                    cpf: cpf
                )
            )
        )
        
        sut = RegistrationComplianceStatusInteractor(
            presenter: presenterSpy,
            status: .analysis,
            dependencies: dependenciesMock,
            service: serviceMock
        )
        
        let mockController = UIViewController()
        sut.primaryAction(from: mockController)
        
        XCTAssertEqual(serviceMock.requestStatusUpdateCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepCallsCount, 1)
        XCTAssertEqual(presenterSpy.action, .review(name: name, birthdate: birthdate, cpf: cpf))
    }
}
