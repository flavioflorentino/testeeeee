@testable import Registration
@testable import UI
@testable import AssetsKit
import XCTest

private final class RegistrationComplianceStatusCoordinatorSpy: RegistrationComplianceStatusCoordinating {
    var viewController: UIViewController?
    private(set) var callPerformCount = 0
    private(set) var action: RegistrationComplianceStatusAction?
    
    func perform(action: RegistrationComplianceStatusAction) {
        self.callPerformCount += 1
        self.action = action
    }
}

private final class RegistrationComplianceStatusDisplaySpy: RegistrationComplianceStatusDisplay {
    private(set) var callDisplayCount = 0
    private(set) var callDisplayInProgressAlertCount = 0
    private(set) var callDisplayErrorAlertCount = 0
    private(set) var image: UIImage?
    private(set) var model: RegistrationComplianceStatusViewModel?
    
    func display(image: UIImage, model: RegistrationComplianceStatusViewModel) {
        callDisplayCount += 1
        self.image = image
        self.model = model
    }
    
    func displayInProgressAlert() {
        callDisplayInProgressAlertCount += 1
    }
    
    func displayErrorAlert() {
        callDisplayErrorAlertCount += 1
    }
}

final class RegistrationComplianceStatusPresenterTests: XCTestCase {
    private let viewController = RegistrationComplianceStatusDisplaySpy()
    private let coordinator = RegistrationComplianceStatusCoordinatorSpy()
    private lazy var sut: RegistrationComplianceStatusPresenting = {
        let sut = RegistrationComplianceStatusPresenter(coordinator: coordinator)
        sut.viewController = viewController
        
        return sut
    }()
    
    func testPresent_WhenStatusIsMinor_ShouldCallDisplayWithMinorModel() {
        let minorImage = Resources.Illustrations.iluWarning.image
        let minorModel = RegistrationComplianceStatusViewModel(
            title:  Strings.Status.Minor.title,
            description:  Strings.Status.Minor.message,
            buttonTitle:  Strings.Status.Minor.Button.title,
            isHelpCenterEnabled: true
        )
        
        sut.present(with: .minor)
        
        XCTAssertEqual(viewController.callDisplayCount, 1)
        XCTAssertEqual(viewController.model, minorModel)
        XCTAssertEqual(viewController.image, minorImage)
    }
}
