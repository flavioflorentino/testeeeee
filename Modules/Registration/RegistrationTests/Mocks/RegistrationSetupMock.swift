import Core
@testable import Registration

enum LegacyDependencies {
    case deeplink
    case authManager
    case registration
    case tokenManager
}

final class RegistrationSetupMock: RegistrationSetupContract {
    var deeplinkInstance: DeeplinkContract?
    var authManagerInstance: AuthenticationManagerContract?
    var registrationInstance: RegistrationComplianceCompleteContract?
    var tokenManager: TokenManagerContract?
    
    init(_ dependencies: LegacyDependencies...) {
        dependencies.forEach {
            switch $0 {
            case .deeplink:
                deeplinkInstance = DeeplinkWrapperMock()
            case .authManager:
                authManagerInstance = AuthManagerWrapperMock()
            case .registration:
                registrationInstance = RegistrationLegacyWrapperMock()
            case .tokenManager:
                tokenManager = TokenManagerMock()
            }
        }
    }
}
