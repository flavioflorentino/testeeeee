import Core
@testable import Registration

final class TokenManagerMock: TokenManagerContract {
    var token: String = ""
    
    private(set) var callUpdateTokenCount = 0
    private(set) var updatedToken: String?
    
    func updateToken(_ token: String) {
        callUpdateTokenCount += 1
        updatedToken = token
    } 
}
