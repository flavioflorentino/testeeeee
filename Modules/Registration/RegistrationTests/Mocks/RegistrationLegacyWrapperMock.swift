import Core
@testable import Registration

final class RegistrationLegacyWrapperMock: RegistrationComplianceCompleteContract {
    private(set) var callStartCount = 0
    private(set) var promoCode: String?
    
    func start(promoCode: String?) -> UIViewController {
        callStartCount += 1
        self.promoCode = promoCode
        return UIViewController()
    }
}
