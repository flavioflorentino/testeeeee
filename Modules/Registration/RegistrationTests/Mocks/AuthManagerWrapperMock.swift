import Core
@testable import Registration

final class AuthManagerWrapperMock: AuthenticationManagerContract {
    private(set) var callLogoutCount = 0
    private(set) var context: UIViewController?
    private(set) var completion: ((Error?) -> Void)?
    
    func logout(fromContext context: UIViewController?, completion: ((Error?) -> Void)?) {
        callLogoutCount += 1
        self.context = context
        self.completion = completion
    }
}
