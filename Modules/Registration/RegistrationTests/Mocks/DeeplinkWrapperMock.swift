import Core
@testable import Registration

final class DeeplinkWrapperMock: DeeplinkContract {
    private(set) var callOpenDeeplinkCount = 0
    private(set) var url: URL?
    
    func open(url: URL) -> Bool {
        self.url = url
        callOpenDeeplinkCount += 1
        return true
    }
}
