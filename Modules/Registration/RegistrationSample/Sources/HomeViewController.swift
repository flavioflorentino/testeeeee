import Registration
import SnapKit
import UI
import UIKit

// Sample screen to launch the flows created under this module

final class RegistrationSampleMock: RegistrationRenewalAddressInput, RegistrationRenewalAuthDelegate {
    func openAddressList(idAddress: String?, navigationController: UINavigationController?, completion: @escaping (String, String) -> Void) {
    }
    
    func authenticate(
        sucessHandler: @escaping (_ password: String, _ isBiometric: Bool) -> Void,
        cancelHandler: @escaping () -> Void
    ) {
    }
}

final class HomeViewController: UIViewController {
    private var coordinator: Coordinating?
    private let sampleMock = RegistrationSampleMock()
    
    private lazy var registrationRenewalButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle("Renovação cadastral", for: .normal)
        button.addTarget(self, action: #selector(tapRegistrationRenewalButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var registrationComplianceStatusButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle("Conformidade cadastral Status", for: .normal)
        button.addTarget(self, action: #selector(tapRegistrationComplianceStatusButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var registrationComplianceReviewButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle("Conformidade cadastral Revisão", for: .normal)
        button.addTarget(self, action: #selector(tapRegistrationComplianceReviewButton), for: .touchUpInside)
        return button
    }()
    
    init() {
        super.init(nibName: nil, bundle: nil)
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    private func tapRegistrationRenewalButton() {
        guard let navController = navigationController else {
            return
        }
        let dependencies = RegistrationRenewalDependencies(addressSelector: sampleMock, authManager: sampleMock, origin: .settings)
        let coordinator = RegistrationRenewalFlowCoordinator(with: navController, dependencies: dependencies)
        coordinator.start()
        self.coordinator = coordinator
    }
    
    @objc
    private func tapRegistrationComplianceStatusButton() {
        guard let navController = navigationController else {
            return
        }

        let coordinator = RegistrationComplianceFlowCoordinator(with: navController)
        coordinator.startCompliance(status: .minor)
        
        self.coordinator = coordinator
    }
    
    @objc
    private func tapRegistrationComplianceReviewButton() {
        guard let navController = navigationController else {
            return
        }
        
        let coordinator = RegistrationComplianceFlowCoordinator(with: navController)
        coordinator.startReview(model: RegistrationCompReviewModel(firstName: "SomeName", lastName: "SomeLastName", birthdate: "01/01/1999", cpf: "123.456.789.11"))

        self.coordinator = coordinator
    }
}

extension HomeViewController: ViewConfiguration {
    func buildViewHierarchy() {
        view.addSubview(registrationRenewalButton)
        view.addSubview(registrationComplianceReviewButton)
        view.addSubview(registrationComplianceStatusButton)
    }
    
    func setupConstraints() {
        registrationRenewalButton.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        registrationComplianceReviewButton.snp.makeConstraints {
            $0.top.equalTo(registrationRenewalButton.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        registrationComplianceStatusButton.snp.makeConstraints {
            $0.top.equalTo(registrationComplianceReviewButton.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
    }
    
    func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}
