import Foundation

struct RenewalFormCell: Equatable {
    let title: String
    let displayValue: String?
    let alert: String?
    let alertIsHidden: Bool
}
