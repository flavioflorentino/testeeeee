import Foundation

struct RenewalFormError: Decodable {
    var items: [RenewalFormItem]
}

struct RenewalFormItem: Decodable {
    var alertText: String
    let type: RegistrationRenewalItemType
}
