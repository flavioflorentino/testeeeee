import AnalyticsModule
import Core
import Foundation

enum RegistrationComplianceEvent: AnalyticsKeyProtocol {
    case didViewCompliance(status: RegistrationComplianceStatus)
    case didViewReview
    case didReviewOptionSelected(value: String)
    case didReviewConfirm
    case didCloseReview
    case didUpdateStatus
    case didContinueApprove
    case didTapDeniedAgree
    case didOpenDeniedFaq
    
    private var name: String {
        switch self {
        case .didViewCompliance, .didViewReview:
            return "Status"
        case .didUpdateStatus,
             .didContinueApprove,
             .didReviewOptionSelected,
             .didReviewConfirm,
             .didCloseReview,
             .didTapDeniedAgree,
             .didOpenDeniedFaq:
            return "Option Selected"
        }
    }

    private var providers: [AnalyticsProvider] {
        guard case .didContinueApprove = self else {
            return  [.firebase, .mixPanel]
        }
        
        return [.firebase, .mixPanel, .appsFlyer]
    }
    
    private var properties: [String: Any] {
        switch self {
        case .didViewCompliance(let status):
            return ["status": status.rawValue]
        case .didViewReview:
            return ["status": "review"]
        case .didUpdateStatus:
            return ["status": "analysis"]
        case .didContinueApprove:
            return  ["status": "approved"]
        case let .didReviewOptionSelected(value):
            return ["option_selected": value]
        case .didReviewConfirm:
            return ["option_selected": "confirm_infos"]
        case .didCloseReview:
            return ["option_selected": "exit"]
        case .didTapDeniedAgree:
            return ["option_selected": "agreed"]
        case .didOpenDeniedFaq:
            return ["option_selected": "faq"]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("Compliance - " + name, properties: properties, providers: providers)
    }
}
