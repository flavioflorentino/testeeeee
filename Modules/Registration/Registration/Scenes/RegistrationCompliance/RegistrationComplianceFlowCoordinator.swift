import Foundation
import UI
import UIKit

public final class RegistrationComplianceFlowCoordinator: Coordinating {
    private let rootNavigationController: UINavigationController
    private lazy var navigationController: UINavigationController = {
        let navigationController = UINavigationController()
        navigationController.view.backgroundColor = Colors.backgroundPrimary.color
        
        return navigationController
    }()
    
    private var childCoordinators: [Coordinating] = []
    
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    
    public func startCompliance(status: RegistrationComplianceStatus) {
        openComplianceStatus(status: status)
    }
    
    public func startReview(model: RegistrationCompReviewModel) {
        openComplianceReview(model: model)
    }
    
    public init(with rootNavigationController: UINavigationController) {
        self.rootNavigationController = rootNavigationController
    }
}

extension RegistrationComplianceFlowCoordinator {
    private func openComplianceStatus(status: RegistrationComplianceStatus) {
        let controller = RegistrationComplianceStatusFactory.make(status: status)
        navigationController.modalPresentationStyle = .fullScreen
        navigationController.viewControllers = [controller]
        rootNavigationController.present(navigationController, animated: false)
    }
    
    private func openComplianceReview(model: RegistrationCompReviewModel) {
        let controller = RegistrationComplianceReviewFactory.make(model: model)
        navigationController.modalPresentationStyle = .fullScreen
        navigationController.viewControllers = [controller]
        rootNavigationController.present(navigationController, animated: false)
    }
}
