import Core

enum RegistrationCompReviewEndpoint {
    case review(name: String, birthdate: String)
    case requestStatusUpdate
}

extension RegistrationCompReviewEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .review:
            return "registration-validation/review"
        case .requestStatusUpdate:
            return "registration-validation/find-biometry-step"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .review:
            return .post
        case .requestStatusUpdate:
            return .get
        }
    }

    var body: Data? {
        switch self {
        case let .review(name, birthdate):
            return ["name": name, "birth": birthdate].toData()
        default:
            return nil
        }
    }
    
    var contentType: ContentType {
        switch self {
        case .requestStatusUpdate:
            return .textPlain
        default:
            return .applicationJson
        }
    }
}
