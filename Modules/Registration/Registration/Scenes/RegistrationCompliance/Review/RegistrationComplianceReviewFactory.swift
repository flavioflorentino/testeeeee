import Foundation

enum RegistrationComplianceReviewFactory {
    static func make(model: RegistrationCompReviewModel) -> RegistrationCompReviewViewController {
        let dependencyContainer = DependencyContainer()
        let service: RegistrationComplianceReviewServicing = RegistrationComplianceReviewService(dependencies: dependencyContainer)
        let coordinator: RegistrationComplianceReviewCoordinating = RegistrationComplianceReviewCoordinator()
        let presenter: RegistrationComplianceReviewPresenting = RegistrationComplianceReviewPresenter(coordinator: coordinator)
        let interactor = RegistrationComplianceReviewInteractor(
            dependencies: dependencyContainer,
            model: model,
            service: service,
            presenter: presenter
        )
        
        let viewController = RegistrationCompReviewViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
