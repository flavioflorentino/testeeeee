import AnalyticsModule
import Core
import Foundation
import IdentityValidation
import FeatureFlag

protocol RegistrationComplianceReviewInteracting: AnyObject {
    func getContent()
    func updateBirthdateValue(birthdate: String)
    func updateNameValue(name: String)
    func review()
    func close(viewController: UIViewController)
}

final class RegistrationComplianceReviewInteractor {
    typealias Dependencies = HasLegacy & HasAnalytics & HasFeatureManager
    private let service: RegistrationComplianceReviewServicing
    private let presenter: RegistrationComplianceReviewPresenting
    private let cpf: String
    private let dependencies: Dependencies
    private var birthdate: String {
        didSet {
            validateBirthdate(birthDate: birthdate)
        }
    }
    
    private var name: String {
        didSet {
            validateName(name: name)
        }
    }
    
    init(
        dependencies: Dependencies,
        model: RegistrationCompReviewModel,
        service: RegistrationComplianceReviewServicing,
        presenter: RegistrationComplianceReviewPresenting
    ) {
        self.dependencies = dependencies
        self.cpf = model.cpf
        self.name = model.name
        self.birthdate = model.birthdate
        self.service = service
        self.presenter = presenter
    }
}

// MARK: - RegistrationComplianceReviewInteracting
extension RegistrationComplianceReviewInteractor: RegistrationComplianceReviewInteracting {
    func getContent() {
        dependencies.analytics.log(RegistrationComplianceEvent.didViewReview)
        presenter.presentDocument(document: cpf)
        presenter.presentDescription()
    }
    
    func updateBirthdateValue(birthdate: String) {
        dependencies.analytics.log(RegistrationComplianceEvent.didReviewOptionSelected(value: birthdate))
        self.birthdate = birthdate
    }
    
    func updateNameValue(name: String) {
        dependencies.analytics.log(RegistrationComplianceEvent.didReviewOptionSelected(value: name))
        self.name = name
    }
    
    func close(viewController: UIViewController) {
        dependencies.analytics.log(RegistrationComplianceEvent.didCloseReview)
        dependencies.legacy.authManagerInstance?.logout(fromContext: viewController, completion: nil)
    }
    
    func review() {
        dependencies.analytics.log(RegistrationComplianceEvent.didReviewConfirm)
        service.sendReview(name: name, birthdate: birthdate) { [weak self] result in
            guard let self = self else {
                return
            }
            
            switch result {
            case .success(let model):
                switch model.step {
                case .biometry:
                    self.presenter.didNextStep(
                        action: .identityValidation(
                            token: model.token.code,
                            flow: model.flow ?? "TFA",
                            completion: self.didFinishIdentityValidation
                        )
                    )
                case .success:
                    self.dependencies.legacy.tokenManager?.updateToken(model.token.code)
                    self.presenter.didNextStep(action: .complianceStatus(status: .approved))
                case .minor:
                    self.dependencies.legacy.tokenManager?.updateToken(model.token.code)
                    self.presenter.didNextStep(action: .complianceStatus(status: .minor))
                }
            case .failure:
                self.presenter.presentError()
            }
        }
    }
}

extension RegistrationComplianceReviewInteractor {
    private func validateBirthdate(birthDate: String) {
        let birthDateLenght = 10
        let currentDate = Date()
        
        guard let date = Date.forthFormatter.date(from: birthDate) else {
            presenter.hasBirthdateError = true
            return
        }
        
        let isValidDate = birthDate.count == birthDateLenght && date < currentDate
        
        presenter.hasBirthdateError = !isValidDate
    }
    
    private func validateName(name: String) {
        presenter.hasNameError = name.isEmpty
    }
    
    private func didFinishIdentityValidation(status: IDValidationStatus?) {
        guard status != nil else {
            presenter.dismissLoading()
            return
        }
        
        service.requestStatusUpdate { [weak self] result in
            switch result {
            case .success(let model):
                self?.presenter.didNextStep(action: .complianceStatus(status: model.step.complianceStatus()))
            case .failure:
                self?.presenter.presentError()
            }
        }
    }
}
