import SkyFloatingLabelTextField
import UI
import UIKit

protocol RegistrationComplianceReviewDisplay: AnyObject {
    func displayDocument(documentText: String)
    func updateButtonState(enabled: Bool)
    func updateBirthdateErrorState(errorMessage: String?)
    func displayError(error: StatefulErrorViewModel)
    func dismissLoading()
    func updateNameErrorState(errorMessage: String?)
    func displayDescription(description: NSAttributedString)
}

private extension RegistrationCompReviewViewController.Layout {
    enum TextField {
        static let lineHeight: CGFloat = 1
        static let height: CGFloat = 40.0
    }
}

final class RegistrationCompReviewViewController: ViewController<RegistrationComplianceReviewInteracting, UIView> {
    fileprivate enum Layout { }
    
    private lazy var scrollView = UIScrollView()
    private lazy var contentView = UIView()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            documentView,
            SpacerView(size: Spacing.base05),
            nameTextField,
            SpacerView(size: Spacing.base05),
            birthDateTextField
        ])
        stackView.axis = .vertical
        stackView.alignment = .center
        
        return stackView
    }()
    
    private lazy var documentView: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle(cornerRadius: .medium))
            .with(\.border, .light(color: .grayscale200()))
        
        return view
    }()
    
    private lazy var documentLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale600.color)
        
        return label
    }()
    
    private lazy var nameTextField: SkyFloatingLabelTextField = {
        let textField = SkyFloatingLabelTextField()
        textField.lineHeight = Layout.TextField.lineHeight
        textField.lineColor = Colors.grayscale300.color
        textField.titleColor = Colors.grayscale300.color
        textField.selectedTitleColor = Colors.grayscale300.color
        textField.selectedLineColor = Colors.branding400.color
        textField.textColor = Colors.grayscale700.color
        textField.titleFormatter = { $0 }
        textField.placeholder = Strings.Input.Name.title
        textField.addTarget(self, action: #selector(updateNameValue(_:)), for: .editingChanged)
        
        return textField
    }()
    
    private lazy var birthDateTextField: SkyFloatingLabelTextField = {
        let textField = SkyFloatingLabelTextField()
        textField.lineHeight = Layout.TextField.lineHeight
        textField.lineColor = Colors.grayscale300.color
        textField.titleColor = Colors.grayscale300.color
        textField.selectedTitleColor = Colors.grayscale300.color
        textField.selectedLineColor = Colors.branding400.color
        textField.textColor = Colors.grayscale700.color
        textField.titleFormatter = { $0 }
        textField.placeholder = Strings.Input.Birthdate.title
        textField.keyboardType = .numberPad
        birthdayNumberMaks.bind(to: textField)
        textField.addTarget(self, action: #selector(updateBirthdateValue(_:)), for: .editingChanged)
        
        return textField
    }()
    
    private lazy var confirmationButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Strings.Button.confirmData, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.isEnabled = false
        button.addTarget(self, action: #selector(tapConfirmationButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var birthdayNumberMaks: TextFieldMasker = {
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.date)
        return TextFieldMasker(textMask: mask)
    }()
    
    private lazy var keyboardScrollViewHandler = KeyboardScrollViewHandler(scrollView: scrollView)
    
    private func setupNavigationBar() {
        navigationController?.navigationBar.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: Assets.icoRoundCloseGreen.image,
            style: .done,
            target: self,
            action: #selector(tapClose)
        )
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationItem.largeTitleDisplayMode = .always
            navigationController?.navigationBar.prefersLargeTitles = true
        }
    }

    override func buildViewHierarchy() {
        documentView.addSubview(documentLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(stackView)
        contentView.addSubview(confirmationButton)
        scrollView.addSubview(contentView)
        
        view.addSubview(scrollView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.getContent()
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints {
            $0.edges.width.equalToSuperview()
            $0.height.equalTo(view.compatibleSafeArea.height).priority(.low)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.leading.trailing.top.equalTo(contentView).inset(Spacing.base02)
        }
        
        documentView.snp.makeConstraints {
            $0.height.equalTo(Spacing.base07)
            $0.width.equalToSuperview()
        }
        
        stackView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        documentLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.centerY.equalToSuperview()
        }
        
        nameTextField.snp.makeConstraints {
            $0.height.equalTo(Layout.TextField.height)
            $0.width.equalToSuperview()
        }
        
        birthDateTextField.snp.makeConstraints {
            $0.height.equalTo(Layout.TextField.height)
            $0.width.equalToSuperview()
        }
        
        confirmationButton.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    override func configureViews() {
        title = Strings.Review.title
        view.backgroundColor = Colors.backgroundPrimary.color
        keyboardScrollViewHandler.registerForKeyboardNotifications()
        setupNavigationBar()
        hideKeyboardWhenTappedAround()
    }
    
    private func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
}

@objc
extension RegistrationCompReviewViewController {
    private func updateBirthdateValue(_ textField: UITextField) {
        interactor.updateBirthdateValue(birthdate: textField.text ?? "")
    }
    
    private func updateNameValue(_ textField: UITextField) {
        interactor.updateNameValue(name: textField.text ?? "")
    }
    
    private func dismissKeyboard() {
        view.endEditing(true)
    }
}

@objc
extension RegistrationCompReviewViewController {
    private func tapClose() {
        interactor.close(viewController: self)
    }
    
    private func tapConfirmationButton() {
        beginState()
        interactor.review()
    }
}

// MARK: RegistrationComplianceReviewDisplay
extension RegistrationCompReviewViewController: RegistrationComplianceReviewDisplay {
    func displayDocument(documentText: String) {
        documentLabel.text = documentText
        title = Strings.Review.title
    }
    
    func displayDescription(description: NSAttributedString) {
        descriptionLabel.attributedText = description
    }
    
    func updateNameErrorState(errorMessage: String?) {
        nameTextField.errorMessage = errorMessage
    }
    
    func updateBirthdateErrorState(errorMessage: String?) {
        birthDateTextField.errorMessage = errorMessage
    }
    
    func updateButtonState(enabled: Bool) {
        let isNameEmpty = nameTextField.text?.isEmpty ?? true
        let isBirthdateEmpty = birthDateTextField.text?.isEmpty ?? true
        
        let isEnabled = enabled && !(isNameEmpty || isBirthdateEmpty)
        confirmationButton.isEnabled = isEnabled
    }
    
    func displayError(error: StatefulErrorViewModel) {
        title = String()
        endState(animated: true, model: error)
    }
    
    func dismissLoading() {
        endState()
    }
}

extension RegistrationCompReviewViewController: StatefulTransitionViewing {
    func didTryAgain() {
        beginState()
        interactor.review()
    }
}
