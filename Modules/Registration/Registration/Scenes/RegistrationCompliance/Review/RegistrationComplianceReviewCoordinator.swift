import UIKit
import IdentityValidation
import UI

enum RegistrationComplianceReviewAction: Equatable {
    case identityValidation(token: String, flow: String, completion: (IDValidationStatus?) -> Void)
    case complianceStatus(status: RegistrationComplianceStatus)
    case close
    
    static func == (lhs: RegistrationComplianceReviewAction, rhs: RegistrationComplianceReviewAction) -> Bool {
        switch (lhs, rhs) {
        case (.identityValidation, identityValidation):
            return true
        case (.complianceStatus, .complianceStatus):
            return true
        case (.close, .close):
            return true
        default:
            return false
        }
    }
}

protocol RegistrationComplianceReviewCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RegistrationComplianceReviewAction)
}

final class RegistrationComplianceReviewCoordinator {
    weak var viewController: UIViewController?
    private var identityValidantionCoordinator: IDValidationFlowCoordinator?
}

// MARK: - RegistrationComplianceReviewCoordinating
extension RegistrationComplianceReviewCoordinator: RegistrationComplianceReviewCoordinating {
    func perform(action: RegistrationComplianceReviewAction) {
        switch action {
        case let .identityValidation(token, flow, completion):
            openIdentityValidation(token: token, flow: flow, completion: completion)
        case let .complianceStatus(status):
            openComplianceStatus(status: status)
        case .close:
            close()
        }
    }
    
    private func openIdentityValidation(token: String, flow: String, completion: @escaping (IDValidationStatus?) -> Void) {
        guard let viewController = viewController else {
            return
        }
        
        identityValidantionCoordinator = IDValidationFlowCoordinator(
            from: viewController,
            token: token,
            flow: flow,
            completion: completion
        )
        
        identityValidantionCoordinator?.start()
    }
    
    private func openComplianceStatus(status: RegistrationComplianceStatus) {
        let controller = RegistrationComplianceStatusFactory.make(status: status)
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func close() {
        viewController?.dismiss(animated: false)
    }
}
