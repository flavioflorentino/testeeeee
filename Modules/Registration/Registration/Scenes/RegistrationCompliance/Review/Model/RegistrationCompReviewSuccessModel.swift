import Foundation

enum RegistrationCompReviewStep: String, Decodable {
    case success = "validation-success"
    case biometry = "validation-biometry"
    case minor = "validation-denied-under-age-16"
}

struct RegistrationCompReviewSuccessModel: Decodable {
    let step: RegistrationCompReviewStep
    let flow: String?
    let token: RegistrationToken
}
