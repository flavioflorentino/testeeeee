import Foundation

struct RegistrationReviewValidResponseModel: Decodable {
    let data: RegistrationCompReviewValidationModel
}

struct RegistrationCompReviewValidationModel: Decodable {
    let isValidCpf: Bool
    let cpfExists: Bool
    let isValidBirth: Bool
}
