import Foundation

public struct RegistrationCompReviewModel {
    var name: String
    let birthdate: String
    let cpf: String

    public init(
        name: String,
        birthdate: String,
        cpf: String
    ) {
        self.name = name
        self.birthdate = birthdate
        self.cpf = cpf
    }
    
    public init(
        firstName: String,
        lastName: String,
        birthdate: String,
        cpf: String
    ) {
        self.name = "\(firstName) \(lastName)"
        self.birthdate = birthdate
        self.cpf = cpf
    }
}
