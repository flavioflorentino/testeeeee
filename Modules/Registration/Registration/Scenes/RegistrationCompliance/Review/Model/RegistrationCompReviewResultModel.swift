import Foundation

enum RegistrationCompReviewStatus: String, Decodable {
    case success = "validation-success"
    case failure = "validation-denied"
    case progress = "validation-in-progress"
    
    func complianceStatus() -> RegistrationComplianceStatus {
        switch self {
        case .success:
            return .approved
        case .failure:
            return .denied
        case .progress:
            return .analysis
        }
    }
}

struct RegistrationCompReviewResultModel: Decodable {
    let step: RegistrationCompReviewStatus
    let token: RegistrationToken
}
