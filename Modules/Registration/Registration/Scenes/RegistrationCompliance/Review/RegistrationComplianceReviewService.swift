import Core

protocol RegistrationComplianceReviewServicing {
    func sendReview(
        name: String,
        birthdate: String,
        completion: @escaping ((Result<RegistrationCompReviewSuccessModel, ApiError>) -> Void)
    )
    
    func requestStatusUpdate(completion: @escaping ((Result<RegistrationCompReviewResultModel, ApiError>) -> Void))
}

final class RegistrationComplianceReviewService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - RegistrationComplianceReviewServicing
extension RegistrationComplianceReviewService: RegistrationComplianceReviewServicing {
    func requestStatusUpdate(completion: @escaping ((Result<RegistrationCompReviewResultModel, ApiError>) -> Void)) {
        let api = Api<RegistrationCompReviewResultModel>(endpoint: RegistrationCompReviewEndpoint.requestStatusUpdate)
        
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result
                    .map(\.model)
                completion(mappedResult)
            }
        }
    }
    
    func sendReview(
        name: String,
        birthdate: String,
        completion: @escaping ((Result<RegistrationCompReviewSuccessModel, ApiError>) -> Void)
    ) {
        let api = Api<RegistrationCompReviewSuccessModel>(
            endpoint: RegistrationCompReviewEndpoint.review(name: name, birthdate: birthdate)
        )
        
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result
                    .map(\.model)
                completion(mappedResult)
            }
        }
    }
}
