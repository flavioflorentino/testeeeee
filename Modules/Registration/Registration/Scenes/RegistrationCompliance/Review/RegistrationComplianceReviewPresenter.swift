import AssetsKit
import Foundation
import UI

protocol RegistrationComplianceReviewPresenting: AnyObject {
    var viewController: RegistrationComplianceReviewDisplay? { get set }
    var hasBirthdateError: Bool { get set }
    var hasNameError: Bool { get set }
    func presentDocument(document: String)
    func presentError()
    func dismissLoading()
    func presentDescription()
    func didNextStep(action: RegistrationComplianceReviewAction)
}

final class RegistrationComplianceReviewPresenter {
    private let coordinator: RegistrationComplianceReviewCoordinating
    weak var viewController: RegistrationComplianceReviewDisplay?
    var hasNameError: Bool = false {
        didSet {
            viewController?.updateNameErrorState(
                errorMessage: hasNameError ? Strings.Review.Validation.name : nil
            )
            updateButtonState()
        }
    }
    
    var hasBirthdateError: Bool = false {
        didSet {
            viewController?.updateBirthdateErrorState(
                errorMessage: hasBirthdateError ? Strings.Review.Validation.birthdate : nil
            )
            updateButtonState()
        }
    }

    init(coordinator: RegistrationComplianceReviewCoordinating) {
        self.coordinator = coordinator
    }
    
    private func buildErrorStateful() -> StatefulErrorViewModel {
        StatefulErrorViewModel(
            image: Resources.Illustrations.iluPinkCellphone.image,
            content: (
                title: Strings.Review.Error.title,
                description: Strings.Review.Error.message
            ),
            button: (image: Resources.Icons.icoRefresh.image, title: Strings.Review.Error.Button.title)
        )
    }
    
    private func updateButtonState() {
        viewController?.updateButtonState(enabled: !hasNameError && !hasBirthdateError)
    }
}

// MARK: - RegistrationComplianceReviewPresenting
extension RegistrationComplianceReviewPresenter: RegistrationComplianceReviewPresenting {
    func presentDescription() {
        let highlightedText = "\(Strings.Review.Message.cpf)"
        
        let attributedMessage = NSMutableAttributedString(
            string: Strings.Review.message(highlightedText),
            attributes: [.font: Typography.bodyPrimary().font()]
        )
        
        attributedMessage.font(
            text: highlightedText,
            font: Typography.bodyPrimary(.highlight).font()
        )
        
        viewController?.displayDescription(description: attributedMessage)
    }
    
    func dismissLoading() {
        viewController?.dismissLoading()
    }
    
    func presentError() {
        viewController?.displayError(error: buildErrorStateful())
    }
    
    func presentDocument(document: String) {
        viewController?.displayDocument(documentText: Strings.Review.Message.cpfdescription(document))
    }
    
    func didNextStep(action: RegistrationComplianceReviewAction) {
        coordinator.perform(action: action)
    }
}
