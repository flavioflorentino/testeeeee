import Foundation
import UI
import UIKit

public final class RegistrationCompReviewFlowCoordinator: Coordinating {
    private let rootNavigationController: UINavigationController
    private lazy var navigationController: UINavigationController = {
        let viewController = RegistrationComplianceReviewFactory.make(model: model)
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.view.backgroundColor = Colors.white.color
        
        return navigationController
    }()
    
    private var childCoordinators: [Coordinating] = []
    
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    private let model: RegistrationCompReviewModel
    
    public init(with rootNavigationController: UINavigationController, model: RegistrationCompReviewModel) {
        self.rootNavigationController = rootNavigationController
        self.model = model
    }
    
    public func start() {
        navigationController.modalPresentationStyle = .fullScreen
        rootNavigationController.present(navigationController, animated: true)
    }
}
