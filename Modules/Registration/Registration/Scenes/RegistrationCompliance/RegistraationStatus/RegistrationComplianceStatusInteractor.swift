import AnalyticsModule
import Foundation

protocol RegistrationComplianceStatusInteracting: AnyObject {
    func getContent()
    func primaryAction(from viewController: UIViewController)
    func openHelpCenter()
}

final class RegistrationComplianceStatusInteractor {
    typealias Dependencies = HasLegacy & HasAnalytics
    private let presenter: RegistrationComplianceStatusPresenting
    private let service: RegistrationComplianceStatusServicing
    private var status: RegistrationComplianceStatus
    private let dependencies: Dependencies
    
    init(
        presenter: RegistrationComplianceStatusPresenting,
        status: RegistrationComplianceStatus,
        dependencies: Dependencies,
        service: RegistrationComplianceStatusServicing
    ) {
        self.presenter = presenter
        self.status = status
        self.dependencies = dependencies
        self.service = service
    }
}

// MARK: - RegistrationComplianceStatusInteracting
extension RegistrationComplianceStatusInteractor: RegistrationComplianceStatusInteracting {
    func getContent() {
        dependencies.analytics.log(RegistrationComplianceEvent.didViewCompliance(status: status))
        presenter.present(with: status)
    }
    
    func primaryAction(from viewController: UIViewController) {
        switch status {
        case .approved:
            dependencies.analytics.log(RegistrationComplianceEvent.didContinueApprove)
            continueRegistration()
        case .denied:
            dependencies.analytics.log(RegistrationComplianceEvent.didTapDeniedAgree)
            logout(from: viewController)
        case .minor, .pending:
            logout(from: viewController)
        case .analysis:
            dependencies.analytics.log(RegistrationComplianceEvent.didUpdateStatus)
            requestStatusUpdate()
        }
    }
    
    func openHelpCenter() {
        dependencies.analytics.log(RegistrationComplianceEvent.didOpenDeniedFaq)
        presenter.didNextStep(action: .helpCenter)
    }
    
    private func continueRegistration() {
        presenter.didNextStep(action: .continueRegistration)
    }
    
    private func logout(from viewController: UIViewController) {
        dependencies.legacy.authManagerInstance?.logout(fromContext: viewController, completion: nil)
    }
    
    private func requestStatusUpdate() {
        service.requestStatusUpdate { [weak self] result in
            switch result {
            case .success(let result):
                guard result.step == .inconsistency,
                    let name = result.consumer?.name,
                    let birthdate = result.consumer?.birthdate,
                    let cpf = result.consumer?.cpf else {
                        self?.setupContent(step: result.step)
                        return
                }
                
                self?.presenter.didNextStep(action: .review(name: name, birthdate: birthdate, cpf: cpf))
            case .failure:
                self?.presenter.presentError()
            }
        }
    }
    
    private func setupContent(step: RegistrationComplianceResult) {
        if (status == .analysis || status == .pending) && step == .inProgress {
            presenter.presentInProgressAlert()
        } else {
            status = RegistrationComplianceStatus.status(with: step)
            getContent()
        }
    }
}
