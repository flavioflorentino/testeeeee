import CustomerSupport
import UIKit

enum RegistrationComplianceStatusAction: Equatable {
    case helpCenter
    case continueRegistration
    case review(name: String, birthdate: String, cpf: String)
}

protocol RegistrationComplianceStatusCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RegistrationComplianceStatusAction)
}

final class RegistrationComplianceStatusCoordinator {
    typealias Dependencies = HasLegacy
    weak var viewController: UIViewController?
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - RegistrationComplianceStatusCoordinating
extension RegistrationComplianceStatusCoordinator: RegistrationComplianceStatusCoordinating {
    func perform(action: RegistrationComplianceStatusAction) {
        switch action {
        case .helpCenter:
            openHelpCenter()
        case .continueRegistration:
            openNextRegistrationScreen()
        case let .review(name, birthdate, cpf):
            openComplianceReview(name: name, birthdate: birthdate, cpf: cpf)
        }
    }
    
    private func openHelpCenter() {
        let option = RegistrationDeniedCustomer().option()
        let faqController = FAQFactory.make(option: option)
        viewController?.navigationController?.pushViewController(faqController, animated: true)
    }
    
    private func openNextRegistrationScreen() {
        guard let controller = dependencies.legacy.registrationInstance?.start() else {
            return
        }
        
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func openComplianceReview(name: String, birthdate: String, cpf: String) {
        guard let navigation = viewController?.navigationController else {
            return
        }
        
        let coordinator = RegistrationCompReviewFlowCoordinator(
            with: navigation,
            model: RegistrationCompReviewModel(name: name, birthdate: birthdate, cpf: cpf)
        )
        coordinator.start()
    }
}
