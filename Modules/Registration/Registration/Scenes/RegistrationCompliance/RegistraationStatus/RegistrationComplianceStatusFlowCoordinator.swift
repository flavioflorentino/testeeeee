import Core
import Foundation
import UI
import UIKit

public final class RegistrationCompStatusFlowCoordinator: Coordinating {
    private let rootNavigationController: UINavigationController
    private lazy var navigationController: UINavigationController = {
        let viewController = RegistrationComplianceStatusFactory.make(status: status)
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.view.backgroundColor = Colors.white.color
        
        return navigationController
    }()
    
    private var childCoordinators: [Coordinating] = []
    
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    private let status: RegistrationComplianceStatus
    
    public init(
        with rootNavigationController: UINavigationController,
        status: RegistrationComplianceStatus
    ) {
        self.rootNavigationController = rootNavigationController
        self.status = status
    }
    
    public func start() {
        rootNavigationController.present(navigationController, animated: true)
    }
}
