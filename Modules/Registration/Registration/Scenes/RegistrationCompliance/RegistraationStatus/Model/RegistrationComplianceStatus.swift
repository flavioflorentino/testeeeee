import Foundation

public enum RegistrationComplianceStatus: String {
    case approved
    case analysis
    case denied
    case pending
    case minor
    
    public static func status(with resultStatus: RegistrationComplianceResult) -> RegistrationComplianceStatus {
        switch resultStatus {
        case .success:
            return .approved
        case .inProgress:
            return .analysis
        case .denied:
            return .denied
        case .minor:
            return .minor
        default:
            return .analysis
        }
    }
}
