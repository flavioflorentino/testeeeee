import Core
import Foundation

public enum RegistrationComplianceResult: String, Decodable {
    case success = "validation-success"
    case inProgress = "validation-in-progress"
    case denied = "validation-denied"
    case minor = "validation-denied-under-age-16"
    case inconsistency = "validation-inconsistency"
    case biometry = "validation-biometry"
}
