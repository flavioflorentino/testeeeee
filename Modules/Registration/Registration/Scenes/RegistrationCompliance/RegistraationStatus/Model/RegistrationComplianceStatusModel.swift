import Core
import Foundation

struct RegistrationComplianceStatusModel: Decodable {
    let step: RegistrationComplianceResult
    let consumer: RegistrationComplianceStatusConsumer?
}

struct RegistrationComplianceStatusConsumer: Decodable {
    let name: String
    let birthdate: String
    let cpf: String
}
