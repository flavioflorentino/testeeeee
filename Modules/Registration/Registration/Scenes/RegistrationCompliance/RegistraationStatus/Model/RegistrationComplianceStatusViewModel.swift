import Foundation

struct RegistrationComplianceStatusViewModel: Equatable {
    let title: String
    let description: String
    let buttonTitle: String
    let isHelpCenterEnabled: Bool
}
