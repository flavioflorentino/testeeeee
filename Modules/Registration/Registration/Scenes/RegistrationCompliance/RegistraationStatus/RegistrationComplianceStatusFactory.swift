import Foundation

enum RegistrationComplianceStatusFactory {
    static func make(status: RegistrationComplianceStatus) -> RegistrationCompStatusViewController {
        let dependencyContainer = DependencyContainer()
        let coordinator: RegistrationComplianceStatusCoordinating = RegistrationComplianceStatusCoordinator(dependencies: dependencyContainer)
        let presenter: RegistrationComplianceStatusPresenting = RegistrationComplianceStatusPresenter(coordinator: coordinator)
        let service: RegistrationComplianceStatusServicing = RegistrationComplianceStatusService(dependencies: dependencyContainer)
        let interactor = RegistrationComplianceStatusInteractor(
            presenter: presenter,
            status: status,
            dependencies: dependencyContainer,
            service: service
        )
        let viewController = RegistrationCompStatusViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
