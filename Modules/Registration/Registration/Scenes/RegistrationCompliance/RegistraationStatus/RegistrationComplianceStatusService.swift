import Core
import Foundation

protocol RegistrationComplianceStatusServicing {
    func requestStatusUpdate(completion: @escaping (Result<RegistrationComplianceStatusModel, ApiError>) -> Void)
}

final class RegistrationComplianceStatusService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - RegistrationComplianceStatusServicing
extension RegistrationComplianceStatusService: RegistrationComplianceStatusServicing {
    func requestStatusUpdate(completion: @escaping (Result<RegistrationComplianceStatusModel, ApiError>) -> Void) {
        let endpoint = RegistrationComplianceStatusEnpoint.requestStatusUpdate
        Api<RegistrationComplianceStatusModel>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
