import Core

enum RegistrationComplianceStatusEnpoint {
    case requestStatusUpdate
}

extension RegistrationComplianceStatusEnpoint: ApiEndpointExposable {
    var path: String {
        "registration-validation/find-step"
    }
}
