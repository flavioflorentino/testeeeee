import Core

enum RegistrationComplianceStatusEndpoint {
    case requestStatusUpdate
}

extension RegistrationComplianceStatusEndpoint: ApiEndpointExposable {
    var path: String {
        "registration-validation/find-step"
    }
}
