import AssetsKit
import AnalyticsModule
import Foundation
import UIKit
import UI

protocol RegistrationComplianceStatusPresenting: AnyObject {
    var viewController: RegistrationComplianceStatusDisplay? { get set }
    func present(with status: RegistrationComplianceStatus)
    func didNextStep(action: RegistrationComplianceStatusAction)
    func presentInProgressAlert()
    func presentError()
}

final class RegistrationComplianceStatusPresenter {
    typealias Localizable = Strings.Status
    
    private let coordinator: RegistrationComplianceStatusCoordinating
    weak var viewController: RegistrationComplianceStatusDisplay?
    
    init(coordinator: RegistrationComplianceStatusCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - RegistrationComplianceStatusPresenting
extension RegistrationComplianceStatusPresenter: RegistrationComplianceStatusPresenting {
    func didNextStep(action: RegistrationComplianceStatusAction) {
        coordinator.perform(action: action)
    }
    
    func present(with status: RegistrationComplianceStatus) {
        let image: UIImage
        let model: RegistrationComplianceStatusViewModel
        
        switch status {
        case .approved:
            image = Resources.Illustrations.iluRegisterApproved.image
            model = RegistrationComplianceStatusViewModel(
                title: Localizable.Approved.title,
                description: Localizable.Approved.message,
                buttonTitle: Localizable.Approved.Button.title,
                isHelpCenterEnabled: false
            )
        case .denied:
            image = Resources.Illustrations.iluRegisterDenied.image
            model = RegistrationComplianceStatusViewModel(
                title: Localizable.Denied.title,
                description: Localizable.Denied.message,
                buttonTitle: Localizable.Denied.Button.title,
                isHelpCenterEnabled: true
            )
        case .pending:
            image = Resources.Illustrations.iluClock.image
            model = RegistrationComplianceStatusViewModel(
                title: Localizable.Pending.title,
                description: Localizable.Pending.message,
                buttonTitle: Localizable.Pending.Button.title,
                isHelpCenterEnabled: false
            )
        case .analysis:
            image = Resources.Illustrations.iluClock.image
            model = RegistrationComplianceStatusViewModel(
                title: Localizable.Analysis.title,
                description: Localizable.Analysis.message,
                buttonTitle: Localizable.Analysis.Button.title,
                isHelpCenterEnabled: false
            )
        case .minor:
            image = Resources.Illustrations.iluWarning.image
            model = RegistrationComplianceStatusViewModel(
                title: Localizable.Minor.title,
                description: Localizable.Minor.message,
                buttonTitle: Localizable.Minor.Button.title,
                isHelpCenterEnabled: true
            )
        }
        
        viewController?.display(image: image, model: model)
    }
    
    func presentInProgressAlert() {
        viewController?.displayInProgressAlert()
    }
    
    func presentError() {
        viewController?.displayErrorAlert()
    }
}
