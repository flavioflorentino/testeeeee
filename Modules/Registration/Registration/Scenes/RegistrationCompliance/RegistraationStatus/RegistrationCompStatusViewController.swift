import AssetsKit
import UI
import UIKit

protocol RegistrationComplianceStatusDisplay: AnyObject {
    func display(image: UIImage, model: RegistrationComplianceStatusViewModel)
    func displayInProgressAlert()
    func displayErrorAlert()
}

private extension RegistrationCompStatusViewController.Layout {
    enum Image {
        static let size = CGSize(width: 168, height: 182)
    }
}

final class RegistrationCompStatusViewController: ViewController<RegistrationComplianceStatusInteracting, UIView> {
    typealias Localizable = Strings.Status
    
    fileprivate enum Layout { }
    
    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = Spacing.base00
        
        return stackView
    }()
    
    private lazy var imageView = UIImageView(image: Resources.Illustrations.iluRegisterDenied.image)
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .center)
        
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale600())
            .with(\.textAlignment, .center)
        
        return label
    }()
    
    private lazy var primaryButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapPrimaryButton), for: .touchUpInside)
        
        return button
    }()
    
    private lazy var helpCenterButton: UnderlinedButton = {
        let button = UnderlinedButton()
        button.updateColor(
            Colors.branding600.color,
            highlightedColor: Colors.branding500.color,
            font: Typography.linkSecondary().font()
        )
        button.setTitle(Localizable.HelpCenter.Button.title, for: .normal)
        button.addTarget(self, action: #selector(didTapHelpCenterButton), for: .touchUpInside)
        
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.getContent()
    }
    
    override func buildViewHierarchy() {
        containerStackView.addArrangedSubview(imageView)
        containerStackView.addArrangedSubview(SpacerView(size: Spacing.base03))
        containerStackView.addArrangedSubview(titleLabel)
        containerStackView.addArrangedSubview(SpacerView(size: Spacing.base02))
        containerStackView.addArrangedSubview(descriptionLabel)
        containerStackView.addArrangedSubview(SpacerView(size: Spacing.base04))
        containerStackView.addArrangedSubview(primaryButton)
        containerStackView.addArrangedSubview(SpacerView(size: Spacing.base03))
        containerStackView.addArrangedSubview(helpCenterButton)
        
        view.addSubview(containerStackView)
    }
    
    override func setupConstraints() {
        containerStackView.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        imageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Image.size)
            $0.centerX.equalToSuperview()
        }
        
        primaryButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        updateNavigationBarAppearance()
    }

    private func updateNavigationBarAppearance() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.tintColor = Colors.backgroundPrimary.color
        navigationItem.hidesBackButton = true
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
            navigationController?.navigationItem.largeTitleDisplayMode = .never
        }
    }
}

@objc
extension RegistrationCompStatusViewController {
    private func didTapPrimaryButton() {
        beginState()
        interactor.primaryAction(from: self)
    }
    
    private func didTapHelpCenterButton() {
        interactor.openHelpCenter()
    }
}

// MARK: RegistrationComplianceStatusDisplay
extension RegistrationCompStatusViewController: RegistrationComplianceStatusDisplay {
    func display(image: UIImage, model: RegistrationComplianceStatusViewModel) {
        imageView.image = image
        titleLabel.text = model.title
        descriptionLabel.text = model.description
        primaryButton.setTitle(model.buttonTitle, for: .normal)
        helpCenterButton.isHidden = !model.isHelpCenterEnabled
        endState()
    }
    
    func displayInProgressAlert() {
        endState()
        let popupViewController = PopupViewController(
            title: Strings.Status.Popup.title,
            description: Strings.Status.Popup.message,
            image: Resources.Illustrations.iluPersonInfo.image
        )
        
        popupViewController.addAction(PopupAction(title: Strings.Status.Popup.Button.title, style: .fill))
        showPopup(popupViewController)
    }
    
    func displayErrorAlert() {
        endState()
        let popupViewController = PopupViewController(
            title: Strings.Review.Error.title,
            description: Strings.Review.Error.message,
            image: Resources.Illustrations.iluPeople.image
        )
        
        popupViewController.addAction(PopupAction(title: Strings.Status.Popup.Button.title, style: .fill))
        showPopup(popupViewController)
    }
}

extension RegistrationCompStatusViewController: StatefulTransitionViewing {
    func didTryAgain() {
        interactor.primaryAction(from: self)
    }
}
