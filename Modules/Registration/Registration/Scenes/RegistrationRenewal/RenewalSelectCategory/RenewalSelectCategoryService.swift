import Core
import Foundation

protocol RenewalSelectCategoryServicing {
    func getSelectableCategory(completion: @escaping (Result<RenewalSelectCategory, ApiError>) -> Void)
    func saveSelectableCategory(value: String, completion: @escaping (Result<NoContent, ApiError>) -> Void)
}

class RenewalSelectCategoryService: RenewalSelectCategoryServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    private let type: RegistrationRenewalItemType
    
    init(type: RegistrationRenewalItemType, dependencies: Dependencies) {
        self.type = type
        self.dependencies = dependencies
    }
    
    func getSelectableCategory(completion: @escaping (Result<RenewalSelectCategory, ApiError>) -> Void) {
        let api = Api<RenewalSelectCategory>(endpoint: RegistrationRenewalEndpoint.registrationRenewalCategory(type: type))
        
        api.execute { [weak self] result in
            let mappedResult = result
                .map(\.model)
            
            self?.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
    
    func saveSelectableCategory(value: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let model = RegistrationRenewalSaveModel(type: type, value: value)
        let api = Api<NoContent>(endpoint: RegistrationRenewalEndpoint.saveRegistrationRenewalCategory(model))
        
        api.execute { [weak self] result in
            let mappedResult = result
                .map(\.model)
            
            self?.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
}
