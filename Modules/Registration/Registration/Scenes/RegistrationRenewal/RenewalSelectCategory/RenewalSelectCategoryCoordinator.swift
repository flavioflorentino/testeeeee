import UIKit

protocol RenewalSelectCategoryCoordinatorDelegate: AnyObject {
    func didSelectItem(id: String, displayValue: String)
}

enum RenewalSelectCategoryAction: Equatable {
    case close
    case openHelp(url: URL)
    case didSelectItem(id: String, displayValue: String)
}

protocol RenewalSelectCategoryCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RenewalSelectCategoryAction)
}

final class RenewalSelectCategoryCoordinator {
    typealias Dependencies = HasLegacy
    private weak var delegate: RenewalSelectCategoryCoordinatorDelegate?
    private let dependencies: Dependencies
    weak var viewController: UIViewController?
    
    init(delegate: RenewalSelectCategoryCoordinatorDelegate?, dependencies: Dependencies) {
        self.delegate = delegate
        self.dependencies = dependencies
    }
}

// MARK: - RenewalSelectCategoryCoordinating
extension RenewalSelectCategoryCoordinator: RenewalSelectCategoryCoordinating {
    func perform(action: RenewalSelectCategoryAction) {
        switch action {
        case .openHelp(let url):
            dependencies.legacy.deeplinkInstance?.open(url: url)
        case let .didSelectItem(id, displayValue):
            delegate?.didSelectItem(id: id, displayValue: displayValue)
            viewController?.navigationController?.popViewController(animated: true)
        case .close:
            viewController?.navigationController?.popViewController(animated: true)
        }
    }
}
