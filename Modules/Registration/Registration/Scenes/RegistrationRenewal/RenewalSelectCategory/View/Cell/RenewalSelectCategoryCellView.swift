import UIKit
import SnapKit
import UI

final class RenewalSelectCategoryCellView: UITableViewCell, ViewConfiguration {
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale850())
        
        return label
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale300.color
        
        return view
    }()
    
    private lazy var presenter: RenewalSelectCategoryCellPresenting = {
        let presenter = RenewalSelectCategoryCellPresenter()
        presenter.view = self
        
        return presenter
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
        accessoryView = UIImageView(image: Assets.icoChevron.image)
    }
    
    func buildViewHierarchy() {
        contentView.addSubview(descriptionLabel)
    }
    
    func setupConstraints() {
        descriptionLabel.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
        }
    }
    
    func setupView(model: RenewalSelectCategoryCell) {
        presenter.setup(model: model)
    }
}

extension RenewalSelectCategoryCellView: RenewalSelectCategoryCellDisplay {
    func displayDescription(with text: String) {
        descriptionLabel.text = text
    }
}
