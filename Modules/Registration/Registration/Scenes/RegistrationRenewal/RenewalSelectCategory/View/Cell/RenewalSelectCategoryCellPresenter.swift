import Foundation
import UI

protocol RenewalSelectCategoryCellPresenting {
    var view: RenewalSelectCategoryCellDisplay? { get set }
    func setup(model: RenewalSelectCategoryCell)
}

protocol RenewalSelectCategoryCellDisplay: AnyObject {
    func displayDescription(with text: String)
}

final class RenewalSelectCategoryCellPresenter: RenewalSelectCategoryCellPresenting {
    weak var view: RenewalSelectCategoryCellDisplay?
    
    func setup(model: RenewalSelectCategoryCell) {
        view?.displayDescription(with: model.description)
    }
}
