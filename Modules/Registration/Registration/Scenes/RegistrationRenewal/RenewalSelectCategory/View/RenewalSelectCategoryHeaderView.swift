import UIKit
import SnapKit
import UI

protocol RenewalSelectCategoryHeaderViewDelegate: AnyObject {
    func searchBarTextDidChange(_ searchText: String)
    func didTapHeader()
}

final class RenewalSelectCategoryHeaderView: UIView {
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = Spacing.base03
        
        return stackView
    }()
    
    private lazy var headerLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.isUserInteractionEnabled = true
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale850())
        
        return label
    }()
    
    private lazy var searchBar: UISearchBar = {
        let search = UISearchBar()
        search.searchBarStyle = .minimal
        search.delegate = self
        
        return search
    }()
    
    weak var delegate: RenewalSelectCategoryHeaderViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView(attributedText: NSAttributedString) {
        headerLabel.attributedText = attributedText
    }
    
    func addSearchBar(searchPlaceholder: String?) {
        stackView.addArrangedSubview(searchBar)
        searchBar.placeholder = searchPlaceholder
    }
}

extension RenewalSelectCategoryHeaderView: ViewConfiguration {
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
        let tapLabel = UITapGestureRecognizer(target: self, action: #selector(didTapLabel))
        headerLabel.addGestureRecognizer(tapLabel)
    }
    
    func buildViewHierarchy() {
        stackView.addArrangedSubview(headerLabel)
        addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
        }
    }
}

@objc
private extension RenewalSelectCategoryHeaderView {
    func didTapLabel() {
        delegate?.didTapHeader()
    }
}

extension RenewalSelectCategoryHeaderView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        delegate?.searchBarTextDidChange(searchText)
    }
        
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        endEditing(true)
    }
}
