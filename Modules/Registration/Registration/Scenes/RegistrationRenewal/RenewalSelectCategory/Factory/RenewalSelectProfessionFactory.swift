import UIKit
import Foundation

enum RenewalSelectProfessionFactory {
    static func make(
        delegate: RenewalSelectCategoryCoordinatorDelegate?,
        userProfile: RegistrationRenewalUserProfile
    ) -> UIViewController {
        let container = DependencyContainer()
        let service: RenewalSelectCategoryServicing = RenewalSelectCategoryService(type: .profession, dependencies: container)
        let coordinator: RenewalSelectCategoryCoordinating = RenewalSelectCategoryCoordinator(delegate: delegate, dependencies: container)
        let presenter: RenewalSelectCategoryPresenting = RenewalSelectCategoryPresenter(coordinator: coordinator)
        let viewModel = RenewalSelectCategoryViewModel(
            service: service,
            presenter: presenter,
            dependencies: container,
            itemType: .profession,
            userProfile: userProfile
        )
        let viewController = RenewalSelectCategoryViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
