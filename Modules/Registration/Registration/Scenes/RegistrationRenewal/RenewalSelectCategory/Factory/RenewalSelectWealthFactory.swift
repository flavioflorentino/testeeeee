import UIKit
import Foundation

enum RenewalSelectWealthFactory {
    static func make(
        delegate: RenewalSelectCategoryCoordinatorDelegate?,
        userProfile: RegistrationRenewalUserProfile
    ) -> UIViewController {
        let container = DependencyContainer()
        let service: RenewalSelectCategoryServicing = RenewalSelectCategoryService(type: .wealthRange, dependencies: container)
        let coordinator: RenewalSelectCategoryCoordinating = RenewalSelectCategoryCoordinator(delegate: delegate, dependencies: container)
        let presenter: RenewalSelectCategoryPresenting = RenewalSelectCategoryPresenter(coordinator: coordinator)
        let viewModel = RenewalSelectCategoryViewModel(
            service: service,
            presenter: presenter,
            dependencies: container,
            itemType: .wealthRange,
            userProfile: userProfile
        )
        
        let viewController = RenewalSelectCategoryViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
