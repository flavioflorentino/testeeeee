import UIKit
import Foundation

enum RenewalSelectIncomeFactory {
    static func make(
        delegate: RenewalSelectCategoryCoordinatorDelegate?,
        userProfile: RegistrationRenewalUserProfile
    ) -> UIViewController {
        let container = DependencyContainer()
        let service: RenewalSelectCategoryServicing = RenewalSelectCategoryService(type: .incomeRange, dependencies: container)
        let coordinator: RenewalSelectCategoryCoordinating = RenewalSelectCategoryCoordinator(delegate: delegate, dependencies: container)
        let presenter: RenewalSelectCategoryPresenting = RenewalSelectCategoryPresenter(coordinator: coordinator)
        let viewModel = RenewalSelectCategoryViewModel(
            service: service,
            presenter: presenter,
            dependencies: container,
            itemType: .incomeRange,
            userProfile: userProfile
        )
        let viewController = RenewalSelectCategoryViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
