import AnalyticsModule
import Core
import Foundation

protocol RenewalSelectCategoryViewModelInputs: AnyObject {
    func updateView()
    func didGetBack()
    func filter(searchText: String)
    func didTapHeader()
    func didTapRow(index: IndexPath)
}

final class RenewalSelectCategoryViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let service: RenewalSelectCategoryServicing
    private let presenter: RenewalSelectCategoryPresenting
    private let itemType: RegistrationRenewalItemType
    private let userProfile: RegistrationRenewalUserProfile
    private var model: RenewalSelectCategory?
    private var allItems: [SelectCategory]
    private var filterItems: [SelectCategory]
    
    init(
        service: RenewalSelectCategoryServicing,
        presenter: RenewalSelectCategoryPresenting,
        dependencies: Dependencies,
        itemType: RegistrationRenewalItemType,
        userProfile: RegistrationRenewalUserProfile
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.itemType = itemType
        self.userProfile = userProfile
        allItems = []
        filterItems = []
    }
    
    private func trackWealthInfoOpened() {
        let event = RegistrationRenewalEvent.didTapWealthLink(userProfile: userProfile)
        dependencies.analytics.log(event)
    }
    
    private func trackError(_ error: RequestError) {
        let event = RegistrationRenewalEvent.didReceiveFieldUpdateError(
            userProfile: userProfile,
            itemType: itemType,
            errorType: error.message
        )
        dependencies.analytics.log(event)
    }
}

// MARK: - RenewalSelectCategoryViewModelInputs
extension RenewalSelectCategoryViewModel: RenewalSelectCategoryViewModelInputs {
    func updateView() {
        presenter.presentLoading()
        service.getSelectableCategory { [weak self] result in
            switch result {
            case .success(let model):
                self?.loadSelectableCategorySuccess(model: model)
            case .failure(let error):
                let requestError = error.requestError ?? RequestError()
                self?.presenter.presentErrorPopup(
                    title: requestError.title,
                    message: requestError.message,
                    buttonTitle: Strings.Button.ok
                )
            }
        }
    }
    
    func didTapHeader() {
        guard
            let faq = model?.deeplinkFaq,
            let url = URL(string: faq)
            else {
                return
        }
        
        trackWealthInfoOpened()
        presenter.didNextStep(action: .openHelp(url: url))
    }
    
    func didTapRow(index: IndexPath) {
        let row = index.row
        guard filterItems.indices.contains(row) else {
            return
        }
        
        let item = filterItems[row]
        presenter.presentLoading()
        service.saveSelectableCategory(value: item.id) { [weak self] result in
            switch result {
            case .success:
                self?.presenter.didNextStep(action: .didSelectItem(id: item.id, displayValue: item.value))
            case .failure(let error):
                let requestError = error.requestError ?? RequestError()
                self?.trackError(requestError)
                self?.presenter.presentErrorPopup(
                    title: requestError.title,
                    message: requestError.message,
                    buttonTitle: Strings.Button.ok
                )
            }
        }
    }
    
    func filter(searchText: String) {
        if searchText.isEmpty {
            filterItems = allItems
        } else {
            filterItems = allItems.filter { $0.value.contains(searchText) }
        }
        
        presenter.updateItems(filterItems)
    }
    
    func didGetBack() {
        presenter.didNextStep(action: .close)
    }
    
    private func loadSelectableCategorySuccess(model: RenewalSelectCategory) {
        self.model = model
        allItems = model.items
        filterItems = model.items
        presenter.presentView(with: model)
    }
}
