import Foundation
import UI
import UIKit

protocol RenewalSelectCategoryPresenting: AnyObject {
    var viewController: RenewalSelectCategoryDisplay? { get set }
    func presentView(with model: RenewalSelectCategory)
    func updateItems(_ items: [SelectCategory])
    func presentLoading()
    func presentErrorPopup(title: String?, message: String, buttonTitle: String)
    func didNextStep(action: RenewalSelectCategoryAction)
}

final class RenewalSelectCategoryPresenter: RenewalSelectCategoryPresenting {
    private let coordinator: RenewalSelectCategoryCoordinating
    weak var viewController: RenewalSelectCategoryDisplay?
    
    private var underlineAttributed: [NSAttributedString.Key: Any] = {
        let font = Typography.bodyPrimary().font()
        let color = Colors.branding300.color
        return [
            .font: font,
            .foregroundColor: color,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
    }()
    
    init(coordinator: RenewalSelectCategoryCoordinating) {
        self.coordinator = coordinator
    }
    
    func presentView(with model: RenewalSelectCategory) {
        viewController?.hideLoading()
        
        presentTitle(model.title)
        updateItems(model.items)
        presentHeaderView(
            description: model.description,
            isSearchable: model.isSearchable,
            searchPlaceholder: model.searchPlaceholder
        )
    }
    
    func presentErrorPopup(title: String?, message: String, buttonTitle: String) {
        viewController?.hideLoading()
        viewController?.displayErrorPopup(title: title, message: message, buttonTitle: buttonTitle)
    }
    
    func presentLoading() {
        viewController?.showLoading()
    }
    
    func didNextStep(action: RenewalSelectCategoryAction) {
        coordinator.perform(action: action)
    }
    
    func updateItems(_ items: [SelectCategory]) {
        let cell = items.map { RenewalSelectCategoryCell(description: $0.value) }
        let section = Section<String, RenewalSelectCategoryCell>(header: nil, items: cell)
        
        viewController?.displayItens(data: [section])
    }
    
    private func presentTitle(_ title: String) {
        viewController?.displayTitle(title)
    }
    
    private func presentHeaderView(description: String, isSearchable: Bool, searchPlaceholder: String?) {
        let attributedText = description.attributedString(separated: "[a]", attrs: underlineAttributed)
        
        viewController?.displayHeaderView(attributedText: attributedText)
        if isSearchable {
            viewController?.displaySearchBar(searchPlaceholder: searchPlaceholder)
        }
    }
}
