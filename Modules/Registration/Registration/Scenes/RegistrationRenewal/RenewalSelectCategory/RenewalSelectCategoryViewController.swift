import UIKit
import SnapKit
import UI

protocol RenewalSelectCategoryDisplay: AnyObject {
    func displayTitle(_ title: String)
    func displayHeaderView(attributedText: NSAttributedString)
    func displayErrorPopup(title: String?, message: String, buttonTitle: String)
    func displaySearchBar(searchPlaceholder: String?)
    func displayItens(data: [Section<String, RenewalSelectCategoryCell>])
    func showLoading()
    func hideLoading()
}

private extension RenewalSelectCategoryViewController.Layout {
    enum Size {
        static let imageHeight: CGFloat = 90.0
        static let headerHeight: CGFloat = 70.0
    }
}

final class RenewalSelectCategoryViewController: ViewController<RenewalSelectCategoryViewModelInputs, UIView>, LoadingViewProtocol {
    fileprivate enum Layout { }
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorInset = .zero
        tableView.separatorStyle = .singleLine
        tableView.backgroundColor = .clear
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(
            RenewalSelectCategoryCellView.self,
            forCellReuseIdentifier: String(describing: RenewalSelectCategoryCellView.self)
        )
        
        return tableView
    }()
    
    private lazy var headerView: RenewalSelectCategoryHeaderView = {
        let headerView = RenewalSelectCategoryHeaderView()
        headerView.delegate = self
        
        return headerView
    }()
    
    private var dataSource: TableViewHandler<String, RenewalSelectCategoryCell, RenewalSelectCategoryCellView>?
    
    lazy var loadingView = LoadingView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.updateView()
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        configureNavigationBar()
        addGesture()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(tableView)
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = UIView()
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        headerView.snp.makeConstraints {
            $0.top.equalTo(tableView)
            $0.leading.trailing.equalTo(tableView).inset(Spacing.base02)
            $0.centerX.equalTo(tableView)
            $0.height.greaterThanOrEqualTo(Layout.Size.headerHeight)
        }
    }
    
    private func configureNavigationBar() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: Assets.icoChevronLeft.image,
            style: .plain,
            target: self,
            action: #selector(tapBackButton)
        )
    }
    
    private func addGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        tapGesture.delegate = self
        view.addGestureRecognizer(tapGesture)
    }
    
    private func updateView() {
        view.layoutIfNeeded()
        tableView.reloadData()
    }
}

@objc
private extension RenewalSelectCategoryViewController {
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func tapBackButton() {
        viewModel.didGetBack()
    }
}

extension RenewalSelectCategoryViewController: RenewalSelectCategoryHeaderViewDelegate {
    func didTapHeader() {
        viewModel.didTapHeader()
    }
    
    func searchBarTextDidChange(_ searchText: String) {
        viewModel.filter(searchText: searchText)
    }
}

// MARK: RenewalSelectCategoryDisplay
extension RenewalSelectCategoryViewController: RenewalSelectCategoryDisplay {
    func displayTitle(_ title: String) {
        self.title = title
    }
    
    func displayHeaderView(attributedText: NSAttributedString) {
        headerView.setupView(attributedText: attributedText)
        updateView()
    }
    
    func displaySearchBar(searchPlaceholder: String?) {
        headerView.addSearchBar(searchPlaceholder: searchPlaceholder)
        updateView()
    }
    
    func displayItens(data: [Section<String, RenewalSelectCategoryCell>]) {
        dataSource = TableViewHandler(
            data: data,
            cellType: RenewalSelectCategoryCellView.self,
            configureCell: { _, item, cellView in
                cellView.setupView(model: item)
            },
            configureDidSelectRow: { [weak self] index, _ in
                self?.viewModel.didTapRow(index: index)
            }
        )
        
        tableView.delegate = dataSource
        tableView.dataSource = dataSource
        tableView.reloadData()
    }
    
    func showLoading() {
        startLoadingView()
    }
    
    func hideLoading() {
        stopLoadingView()
    }
    
    func displayErrorPopup(title: String?, message: String, buttonTitle: String) {
        showErrorPopup(message: message, buttonTitle: buttonTitle, title: title) { [weak self] in
            self?.viewModel.didGetBack()
        }
    }
}

extension RenewalSelectCategoryViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        true
    }
}
