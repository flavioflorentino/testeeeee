import Foundation

struct RegistrationRenewalSuccessModel: Decodable, Equatable {
    let title: String
    let description: String
}
