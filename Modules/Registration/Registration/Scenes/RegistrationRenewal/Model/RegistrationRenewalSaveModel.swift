import Foundation

struct RegistrationRenewalSaveModel: Encodable {
    let type: RegistrationRenewalItemType
    let value: String
}
