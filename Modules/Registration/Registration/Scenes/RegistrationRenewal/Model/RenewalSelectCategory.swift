import Foundation

struct RenewalSelectCategory: Decodable {
    let title: String
    let description: String
    let items: [SelectCategory]
    let isSearchable: Bool
    let searchPlaceholder: String?
    let deeplinkFaq: String?
}

struct SelectCategory: Decodable {
    let id: String
    let value: String
}

private extension RenewalSelectCategory {
    private enum CodingKeys: String, CodingKey {
        case title, description, items
        case isSearchable = "searchable"
        case searchPlaceholder = "search_placeholder"
        case deeplinkFaq = "faq"
    }
}
