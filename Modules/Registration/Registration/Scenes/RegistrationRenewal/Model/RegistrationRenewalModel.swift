import Foundation

enum RegistrationRenewalUserProfile: String, Decodable, Equatable {
    case limitedAccount = "LIMITED_ACCOUNT"
    case unlimitedAccount = "UNLIMITED_ACCOUNT"
    case picpayCard = "PICPAY_CARD"
}

enum RegistrationRenewalItemType: String, Codable {
    case email
    case phone
    case address
    case incomeRange = "monthly_income_range"
    case wealthRange = "wealth_range"
    case profession
}

struct RegistrationRenewalResponse: Decodable {
    let userProfile: RegistrationRenewalUserProfile
    let welcomeScreen: RegistrationRenewalWelcomeScreen?
    let formScreen: RegistrationRenewalForm
}

struct RegistrationRenewalForm: Decodable, Equatable {
    let title: String
    let subtitle: String
    var items: [RegistrationRenewalFormItem]
}

struct RegistrationRenewalFormItem: Decodable, Equatable {
    let title: String
    var value: String?
    var displayValue: String?
    var alertText: String?
    let type: RegistrationRenewalItemType
}

struct RegistrationRenewalWelcomeScreen: Decodable {
    let title: String
    let description: String
}
