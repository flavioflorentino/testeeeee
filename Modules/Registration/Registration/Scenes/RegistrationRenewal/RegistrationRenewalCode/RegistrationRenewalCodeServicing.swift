import Core

protocol RegistrationRenewalCodeServicing {
    func sendConfirmationCode(codeValue: String, completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func requestNewCode(completion: @escaping (Result<RegistrationRenewalInputResponse, ApiError>) -> Void)
}
