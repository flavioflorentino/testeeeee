import SnapKit
import UI
import UIKit

private extension RegistrationRenewalCodeSuccessView.Layout {
    enum Size {
        static let imageSize = CGSize(width: 90, height: 90)
    }
}

final class RegistrationRenewalCodeSuccessView: UIView {
    fileprivate enum Layout { }
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.backgroundPrimary.color
        return view
    }()
    
    private lazy var successImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.icoSuccess.image
        imageView.contentMode = .scaleAspectFit
        
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.typography, .title(.xLarge))
            .with(\.textAlignment, .center)
        return label
    }()
    
    init(frame: CGRect, message: String) {
        super.init(frame: frame)
        titleLabel.text = message
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension RegistrationRenewalCodeSuccessView: ViewConfiguration {
    func buildViewHierarchy() {
        containerView.addSubview(successImage)
        containerView.addSubview(titleLabel)
        addSubview(containerView)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.top.greaterThanOrEqualToSuperview()
            $0.bottom.lessThanOrEqualToSuperview()
        }
        
        successImage.snp.makeConstraints {
            $0.top.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.imageSize)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(successImage.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
        isUserInteractionEnabled = true
    }
}
