import Foundation
import FeatureFlag
import UI

enum ResendCodeButtonState: Equatable {
    case enabled
    case disabled(counterValue: Int)
    case hidden
}

protocol RegistrationRenewalCodePresenting: AnyObject {
    var viewController: RegistrationRenewalCodeDisplay? { get set }
    func populateView(for inputType: RegistrationRenewalInputType, inputedValue: String)
    func updateResendCodeButton(state: ResendCodeButtonState)
    func didNextStep(action: RegistrationRenewalCodeAction)
    func presentErrorPopup(title: String?, message: String, buttonTitle: String, closeCompletion: (() -> Void)?)
    func showLoading()
    func hideLoading()
    func cleanCodeTextField()
}

final class RegistrationRenewalCodePresenter {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    private let coordinator: RegistrationRenewalCodeCoordinating
    weak var viewController: RegistrationRenewalCodeDisplay?
    
    init(coordinator: RegistrationRenewalCodeCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    private func obfuscateDataIfNeeded(for inputType: RegistrationRenewalInputType, inputedValue: String) -> String {
        guard dependencies.featureManager.isActive(.featureAppSecObfuscationMask) else {
            return inputedValue
        }
        
        switch inputType {
        case .email:
            return StringObfuscationMasker.mask(email: inputedValue) ?? inputedValue
        case .phone:
            return StringObfuscationMasker.mask(phone: removeCellPhoneMask(inputedValue)) ?? inputedValue
        }
    }
    
    private func removeCellPhoneMask(_ value: String) -> String {
        value.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    }
}

// MARK: - RegistrationRenewalCodePresenting
extension RegistrationRenewalCodePresenter: RegistrationRenewalCodePresenting {
    func populateView(for inputType: RegistrationRenewalInputType, inputedValue: String) {
        let title: String
        let message: String
        let placeholder: String
        let inputedValue = obfuscateDataIfNeeded(for: inputType, inputedValue: inputedValue)
        
        switch inputType {
        case .phone:
            title = Strings.Validation.Phone.title
            message = Strings.Validation.Phone.message(inputedValue)
            placeholder = Strings.Validation.Phone.placeholder
        case .email:
            title = Strings.Validation.Email.title
            message = Strings.Validation.Email.message(inputedValue)
            placeholder = Strings.Validation.Email.placeholder
        }
        
        let attributedMessage = NSMutableAttributedString(
            string: message,
            attributes: [.font: Typography.bodyPrimary().font()]
        )
        attributedMessage.font(
            text: inputedValue,
            font: Typography.bodyPrimary(.highlight).font()
        )
        
        viewController?.display(title: title, message: attributedMessage, placeholder: placeholder)
    }
    
    func updateResendCodeButton(state: ResendCodeButtonState) {
        switch state {
        case .enabled:
            viewController?.updateResendCodeButton(isHidden: false, isEnabled: true, title: Strings.Validation.resendCode)
        case .disabled(let counterValue):
            let disabledWithCounterTitle = Strings.Validation.counterMessage(String(counterValue))
            viewController?.updateResendCodeButton(isHidden: false, isEnabled: false, title: disabledWithCounterTitle)
        case .hidden:
            viewController?.updateResendCodeButton(isHidden: true, isEnabled: true, title: nil)
        }
    }
    
    func didNextStep(action: RegistrationRenewalCodeAction) {
        coordinator.perform(action: action)
    }
    
    func showLoading() {
        viewController?.showLoading()
    }
    
    func hideLoading() {
        viewController?.hideLoading()
    }
    
    func presentErrorPopup(title: String?, message: String, buttonTitle: String, closeCompletion: (() -> Void)?) {
        viewController?.displayErrorPopup(
            title: title,
            message: message,
            buttonTitle: buttonTitle,
            closeCompletion: closeCompletion
        )
    }
    
    func cleanCodeTextField() {
        viewController?.cleanCodeTextField()
    }
}
