import Foundation
import AnalyticsModule
import Core

private enum ValidationError {
    static let standard = "1000"
    static let expiredConfirmationRequest = "1001"
    static let expiredConfirmationCode = "1002"
    static let reachedMaxNumberOfAttempts = "1003"
    static let invalidCode = "1004"
}

private enum ValidationDelayKey: String {
    case smsDelay
    case emailDelay
}

protocol RegistrationRenewalCodeViewModelInputs: AnyObject {
    func populateView()
    func didAskToResendCode()
    func didBack()
    func updateCodeValue(_ inputValue: String?)
}

final class RegistrationRenewalCodeViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let service: RegistrationRenewalCodeServicing
    private let presenter: RegistrationRenewalCodePresenting
    
    private let userProfile: RegistrationRenewalUserProfile
    private let inputType: RegistrationRenewalInputType
    private let inputedValue: String
    
    private var rawInputedValue: String {
        switch inputType {
        case .email:
            return inputedValue
        case .phone:
            return inputedValue.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        }
    }
    
    private let validationCodeFullLength = 4
    
    private let initialCodeResendDelay: Int
    
    private(set) var timer: Timer?
    
    init(
        dependencies: Dependencies,
        service: RegistrationRenewalCodeServicing,
        presenter: RegistrationRenewalCodePresenting,
        inputType: RegistrationRenewalInputType,
        inputedValue: String,
        codeResendDelay: Int?,
        userProfile: RegistrationRenewalUserProfile
    ) {
        self.dependencies = dependencies
        self.service = service
        self.presenter = presenter
        self.inputType = inputType
        self.inputedValue = inputedValue
        self.userProfile = userProfile
        initialCodeResendDelay = codeResendDelay ?? 0
    }
    
    private func startDecreasingCounter(from initialValue: Int) {
        timer?.invalidate()
        let initialTime = Date()
        
        guard initialValue > .zero else {
            return
        }
        
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { [weak self] _ in
            guard let self = self else {
                return
            }
            
            self.scheduledTimer(
                initialTime: initialTime,
                initialValue: initialValue,
                update: { interval in
                     self.presenter.updateResendCodeButton(state: .disabled(counterValue: interval))
                },
                completion: {
                    self.timer?.invalidate()
                    self.presenter.updateResendCodeButton(state: .enabled)
                }
            )
        })
        
        timer?.fire()
    }
    
    private func scheduledTimer(initialTime: Date, initialValue: Int, update: (Int) -> Void, completion: () -> Void) {
        let timeFromStartUntilNow = -Int(initialTime.timeIntervalSinceNow)
        let timeLeft = initialValue - timeFromStartUntilNow
        timeLeft <= .zero ? completion() : update(timeLeft)
    }
    
    private func validateCode(_ code: String) {
        presenter.showLoading()
        service.sendConfirmationCode(codeValue: code) { [weak self] result in
            guard let self = self else {
                return
            }
            self.presenter.hideLoading()
            
            switch result {
            case .success:
                let action = RegistrationRenewalCodeAction.showSuccessAndBackToForm(
                    inputType: self.inputType,
                    rawValue: self.rawInputedValue,
                    formatedValue: self.inputedValue
                )
                self.presenter.didNextStep(action: action)
            case .failure(let error):
                self.presenter.updateResendCodeButton(state: .hidden)
                self.presenter.cleanCodeTextField()
                self.presentErrorMessage(error)
                self.trackError(error)
                self.startCounterIfResendDelay(in: error)
            }
        }
    }
    
    private func startCounterIfResendDelay(in error: ApiError) {
        let delayKey: ValidationDelayKey = inputType == .phone ? .smsDelay : .emailDelay
        guard
            let data = error.requestError?.data,
            let delay = data[delayKey.rawValue] as? Int
            else {
                return
        }
        startDecreasingCounter(from: delay)
    }
    
    private func presentErrorMessage(_ error: ApiError) {
        let requestError = error.requestError ?? RequestError()
        switch requestError.code {
        case ValidationError.expiredConfirmationRequest:
            presenter.presentErrorPopup(
                title: requestError.title,
                message: requestError.message,
                buttonTitle: Strings.Button.back
            ) { [weak self] in
                self?.presenter.didNextStep(action: .backToForm)
            }
        default:
            presenter.presentErrorPopup(
                title: requestError.title,
                message: requestError.message,
                buttonTitle: Strings.Button.ok,
                closeCompletion: nil
            )
        }
    }
    
    private func trackCodeScreenViewed() {
        let viewedEvent = RegistrationRenewalEvent.didViewCodeValidation(userProfile: userProfile, inputType: inputType)
        dependencies.analytics.log(viewedEvent)
    }
    
    private func trackResendCodeAction() {
        let resendEvent = RegistrationRenewalEvent.didAskToResentCode(userProfile: userProfile, inputType: inputType)
        dependencies.analytics.log(resendEvent)
    }
    
    private func trackError(_ error: ApiError) {
        let requestError = error.requestError ?? RequestError()
        
        let event: RegistrationRenewalEvent
        switch requestError.code {
        case ValidationError.invalidCode:
            event = .didReceiveInvalidCodeError(userProfile: userProfile, inputType: inputType)
        case ValidationError.reachedMaxNumberOfAttempts:
            event = .didReceiveCodeLimitExceedError(userProfile: userProfile, inputType: inputType)
        default:
            let itemType: RegistrationRenewalItemType = inputType == .phone ? .phone : .email
            event = .didReceiveFieldUpdateError(userProfile: userProfile, itemType: itemType, errorType: requestError.message)
        }
        dependencies.analytics.log(event)
    }
}

// MARK: - RegistrationRenewalCodeViewModelInputs
extension RegistrationRenewalCodeViewModel: RegistrationRenewalCodeViewModelInputs {
    func populateView() {
        trackCodeScreenViewed()
        presenter.populateView(for: inputType, inputedValue: inputedValue)
        startDecreasingCounter(from: initialCodeResendDelay)
    }
    
    func didAskToResendCode() {
        trackResendCodeAction()
        
        presenter.showLoading()
        presenter.cleanCodeTextField()
        service.requestNewCode { [weak self] result in
            self?.presenter.hideLoading()
            
            switch result {
            case .success (let response):
                self?.startDecreasingCounter(from: response.delay)
            case .failure(let error):
                self?.presentErrorMessage(error)
            }
        }
    }
    
    func didBack() {
        presenter.didNextStep(action: .backToPreviousView)
    }
    
    func updateCodeValue(_ code: String?) {
        guard let code = code, code.count == validationCodeFullLength else {
            return
        }
        
        validateCode(code)
    }
}
