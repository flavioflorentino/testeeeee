import UI
import UIKit

protocol RegistrationRenewalCodeSuccessAnimating {
    func showSuccessAnimation(
        overView superview: UIView,
        for inputType: RegistrationRenewalInputType,
        completion: @escaping () -> Void
    )
}

final class RegistrationRenewalCodeSuccessAnimator: RegistrationRenewalCodeSuccessAnimating {
    func showSuccessAnimation(
        overView superview: UIView,
        for inputType: RegistrationRenewalInputType,
        completion: @escaping () -> Void
    ) {
        let message = inputType == .phone ? Strings.Validation.Phone.success : Strings.Validation.Email.success
        let successView = RegistrationRenewalCodeSuccessView(frame: superview.frame, message: message)
        successView.alpha = 0
        
        superview.addSubview(successView)
        
        UIView.animate(
            withDuration: 0.25,
            animations: {
                successView.alpha = 1
            },
            completion: { _ in
                completion()
                UIView.animate(
                    withDuration: 0.25,
                    delay: 1.5,
                    animations: {
                        successView.alpha = 0
                    },
                    completion: { _ in
                        successView.removeFromSuperview()
                    }
                )
            }
        )
    }
}
