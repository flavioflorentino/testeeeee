import Foundation

enum RegistrationRenewalEmailCodeFactory {
    static func make(
        delegate: RegistrationRenewalInputDelegate?,
        inputedValue: String,
        codeResendDelay: Int?,
        userProfile: RegistrationRenewalUserProfile
    ) -> RegistrationRenewalCodeViewController {
        let container = DependencyContainer()
        let service: RegistrationRenewalCodeServicing = RegistrationRenewalEmailCodeService(dependencies: container)
        let coordinator: RegistrationRenewalCodeCoordinating = RegistrationRenewalCodeCoordinator(
            delegate: delegate,
            successAnimator: RegistrationRenewalCodeSuccessAnimator()
        )
        
        let presenter: RegistrationRenewalCodePresenting = RegistrationRenewalCodePresenter(
            coordinator: coordinator,
            dependencies: container
        )
        
        let viewModel = RegistrationRenewalCodeViewModel(
            dependencies: container,
            service: service,
            presenter: presenter,
            inputType: .email,
            inputedValue: inputedValue,
            codeResendDelay: codeResendDelay,
            userProfile: userProfile
        )
        let viewController = RegistrationRenewalCodeViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
