import Foundation
import Core

final class RegistrationRenewalEmailCodeService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - RegistrationRenewalEmailCodeServicing
extension RegistrationRenewalEmailCodeService: RegistrationRenewalCodeServicing {
    func sendConfirmationCode(codeValue: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let codeModel = RegistrationRenewalCodeModel(type: .email, code: codeValue)
        let endpoint = RegistrationRenewalEndpoint.validateCode(codeModel)
        
        Api<NoContent>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func requestNewCode(completion: @escaping (Result<RegistrationRenewalInputResponse, ApiError>) -> Void) {
        let typeModel = RegistrationRenewalItemTypeModel(type: .email)
        let endpoint = RegistrationRenewalEndpoint.resendCode(typeModel)
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        Api<RegistrationRenewalEmailInputResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            let mappedResult = result.map { RegistrationRenewalInputResponse(delay: $0.model.emailDelay) }
            
            self?.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
}
