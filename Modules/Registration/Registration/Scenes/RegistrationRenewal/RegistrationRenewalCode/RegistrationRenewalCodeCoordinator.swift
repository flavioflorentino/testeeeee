import UIKit

enum RegistrationRenewalCodeAction: Equatable {
    case showSuccessAndBackToForm(inputType: RegistrationRenewalInputType, rawValue: String, formatedValue: String)
    case backToForm
    case backToPreviousView
}

protocol RegistrationRenewalCodeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RegistrationRenewalCodeAction)
}

final class RegistrationRenewalCodeCoordinator {
    weak var viewController: UIViewController?
    private weak var delegate: RegistrationRenewalInputDelegate?
    private let successAnimator: RegistrationRenewalCodeSuccessAnimating
    
    init(delegate: RegistrationRenewalInputDelegate?, successAnimator: RegistrationRenewalCodeSuccessAnimating) {
        self.delegate = delegate
        self.successAnimator = successAnimator
    }
}

// MARK: - RegistrationRenewalCodeCoordinating
extension RegistrationRenewalCodeCoordinator: RegistrationRenewalCodeCoordinating {
    func perform(action: RegistrationRenewalCodeAction) {
        switch action {
        case let .showSuccessAndBackToForm(inputType, rawValue, formatedValue):
            delegate?.didChangeItem(toValue: rawValue, formatedValue: formatedValue)
            
            guard let animationSuperview = viewController?.navigationController?.view else {
                delegate?.backToForm()
                return
            }
            successAnimator.showSuccessAnimation(overView: animationSuperview, for: inputType) { [weak self] in
                self?.delegate?.backToForm()
            }
            
        case .backToForm:
            delegate?.backToForm()
        case .backToPreviousView:
            viewController?.navigationController?.popViewController(animated: true)
        }
    }
}
