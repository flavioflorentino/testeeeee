import SecurityModule
import SkyFloatingLabelTextField
import SnapKit
import UI
import UIKit

protocol RegistrationRenewalCodeDisplay: AnyObject {
    func display(title: String, message: NSAttributedString, placeholder: String)
    func updateResendCodeButton(isHidden: Bool, isEnabled: Bool, title: String?)
    func showLoading()
    func hideLoading()
    func displayErrorPopup(title: String?, message: String, buttonTitle: String, closeCompletion: (() -> Void)?)
    func cleanCodeTextField()
}

private extension RegistrationRenewalCodeViewController.Layout {
    enum Size {
        static let textFieldHeight: CGFloat = 48.0
    }
}

final class RegistrationRenewalCodeViewController: ViewController<RegistrationRenewalCodeViewModelInputs, UIView>, LoadingViewProtocol, Obfuscable {
    fileprivate enum Layout { }
    
    var loadingView = LoadingView()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        return label
    }()
    
    private lazy var codeTextField: SkyFloatingLabelTextField = {
        let textField = SkyFloatingLabelTextField()
        textField.lineHeight = 1
        textField.lineColor = Colors.grayscale300.color
        textField.titleColor = Colors.grayscale300.color
        textField.selectedTitleColor = Colors.grayscale300.color
        textField.selectedLineColor = Colors.branding400.color
        textField.textColor = Colors.grayscale700.color
        textField.titleFormatter = { $0 }
        
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        textField.keyboardType = .numberPad
        textField.addTarget(self, action: #selector(textFieldEditingChanged(_:)), for: .editingChanged)
        
        return textField
    }()
    
    private lazy var resendCodeButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(tapResendCodeButton), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    private let codeMask = CustomStringMask(mask: "0000", maskTokenSet: [.decimalDigits])
    
    // MARK: - Screen obfuscation
    var appSwitcherObfuscator: AppSwitcherObfuscating?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.populateView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupObfuscationConfiguration()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        codeTextField.becomeFirstResponder()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        deinitObfuscator()
    }

    override func buildViewHierarchy() {
        view.addSubview(messageLabel)
        view.addSubview(codeTextField)
        view.addSubview(resendCodeButton)
    }
    
    override func setupConstraints() {
        messageLabel.snp.makeConstraints {
            $0.leading.trailing.top.equalToSuperview().inset(Spacing.base02)
        }
        
        codeTextField.snp.makeConstraints {
            $0.top.equalTo(messageLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.Size.textFieldHeight)
        }
        
        resendCodeButton.snp.makeConstraints {
            $0.top.equalTo(codeTextField.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: Assets.icoChevronLeft.image,
            style: .plain,
            target: self,
            action: #selector(tapBackButton)
        )
        setupNavigationBar()
    }
    
    private func setupNavigationBar() {
        navigationController?.navigationBar.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationItem.largeTitleDisplayMode = .always
            navigationController?.navigationBar.prefersLargeTitles = true
        }
    }
}

@objc
extension RegistrationRenewalCodeViewController {
    private func tapResendCodeButton() {
        viewModel.didAskToResendCode()
    }
    
    private func textFieldEditingChanged(_ textField: UITextField) {
        codeTextField.text = codeMask.maskedText(from: textField.text)
        viewModel.updateCodeValue(textField.text)
    }
    
    private func tapBackButton() {
        viewModel.didBack()
    }
}

// MARK: RegistrationRenewalCodeDisplay
extension RegistrationRenewalCodeViewController: RegistrationRenewalCodeDisplay {
    func display(title: String, message: NSAttributedString, placeholder: String) {
        self.title = title
        messageLabel.attributedText = message
        codeTextField.placeholder = placeholder
    }
    
    func updateResendCodeButton(isHidden: Bool, isEnabled: Bool, title: String?) {
        resendCodeButton.setTitle(title, for: .normal)
        resendCodeButton.buttonStyle(LinkButtonStyle())
        resendCodeButton.isEnabled = isEnabled
        resendCodeButton.isHidden = isHidden
    }
    
    func showLoading() {
        codeTextField.isEnabled = false
        startLoadingView()
    }
    
    func hideLoading() {
        codeTextField.isEnabled = true
        stopLoadingView()
    }
    
    func displayErrorPopup(title: String?, message: String, buttonTitle: String, closeCompletion: (() -> Void)?) {
        showErrorPopup(message: message, buttonTitle: buttonTitle, title: title, closeCompletion: closeCompletion)
    }
    
    func cleanCodeTextField() {
        codeTextField.text = nil
    }
}
