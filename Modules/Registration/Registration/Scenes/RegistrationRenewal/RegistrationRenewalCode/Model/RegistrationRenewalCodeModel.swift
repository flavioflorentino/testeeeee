import Foundation

struct RegistrationRenewalCodeModel: Encodable {
    let type: RegistrationRenewalItemType
    let code: String
}

struct RegistrationRenewalItemTypeModel: Encodable {
    let type: RegistrationRenewalItemType
}
