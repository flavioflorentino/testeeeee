import Foundation

protocol RegistrationRenewalIntroPresenting: AnyObject {
    var viewController: RegistrationRenewalIntroDisplay? { get set }
    func showLoading()
    func hideLoading()
    func presentUpdatedInformation(title: String, description: String)
    func presentErrorPopup(title: String?, message: String, buttonTitle: String, closeCompletion: (() -> Void)?)
    func didNextStep(action: RegistrationRenewalIntroAction)
}

final class RegistrationRenewalIntroPresenter {
    private let coordinator: RegistrationRenewalIntroCoordinating
    weak var viewController: RegistrationRenewalIntroDisplay?

    init(coordinator: RegistrationRenewalIntroCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - RegistrationRenewalIntroPresenting
extension RegistrationRenewalIntroPresenter: RegistrationRenewalIntroPresenting {
    func presentUpdatedInformation(title: String, description: String) {
        viewController?.updateLabels(title: title, description: description)
    }
    
    func presentErrorPopup(title: String?, message: String, buttonTitle: String, closeCompletion: (() -> Void)?) {
        viewController?.displayErrorPopup(
            title: title,
            message: message,
            buttonTitle: buttonTitle,
            closeCompletion: closeCompletion
        )
    }
    
    func showLoading() {
        viewController?.showLoading()
    }
    
    func hideLoading() {
        viewController?.hideLoading()
    }
    
    func didNextStep(action: RegistrationRenewalIntroAction) {
        coordinator.perform(action: action)
    }
}
