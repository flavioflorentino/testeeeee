import AnalyticsModule
import Core
import Foundation

public protocol RegistrationRenewalIntroViewModelInputs: AnyObject {
    func fetchChecklistFormData()
    func didConfirm()
    func didClose()
}

final class RegistrationRenewalIntroViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: RegistrationRenewalIntroServicing
    private let presenter: RegistrationRenewalIntroPresenting
    private let origin: RegistrationRenewalOrigin
    private var formModel: RegistrationRenewalForm?
    private var userProfile: RegistrationRenewalUserProfile?

    init(
        service: RegistrationRenewalIntroServicing,
        presenter: RegistrationRenewalIntroPresenting,
        dependencies: Dependencies,
        origin: RegistrationRenewalOrigin
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.origin = origin
    }
    
    private func processFetchSuccessResponse(_ response: RegistrationRenewalResponse) {
        formModel = response.formScreen
        userProfile = response.userProfile
        
        guard let welcomeScreen = response.welcomeScreen else {
            presenter.didNextStep(action: .openForm(model: response.formScreen, userProfile: response.userProfile))
            return
        }
        presenter.hideLoading()
        trackViewed(userProfile: response.userProfile)
        
        presenter.presentUpdatedInformation(title: welcomeScreen.title, description: welcomeScreen.description)
    }
    
    private func trackViewed(userProfile: RegistrationRenewalUserProfile) {
        let viewedEvent = RegistrationRenewalEvent.didViewIntro(userProfile: userProfile, origin: origin)
        dependencies.analytics.log(viewedEvent)
    }
}

// MARK: - RegistrationRenewalIntroViewModelInputs
extension RegistrationRenewalIntroViewModel: RegistrationRenewalIntroViewModelInputs {
    func fetchChecklistFormData() {
        presenter.showLoading()
        service.getRegistrationRenewalData { [weak self] result in
            switch result {
            case .success(let response):
                self?.processFetchSuccessResponse(response)
            case .failure(let error):
                self?.presenter.hideLoading()
                let requestError = error.requestError ?? RequestError()
                self?.presenter.presentErrorPopup(
                    title: requestError.title,
                    message: requestError.message,
                    buttonTitle: Strings.Button.ok,
                    closeCompletion: { [weak self] in
                        self?.didClose()
                    }
                )
            }
        }
    }
    
    func didConfirm() {
        guard let model = formModel, let userProfile = userProfile else {
            return
        }
        
        let startedEvent = RegistrationRenewalEvent.didStarted(userProfile: userProfile)
        dependencies.analytics.log(startedEvent)
        
        presenter.didNextStep(action: .openForm(model: model, userProfile: userProfile))
    }
    
    func didClose() {
        presenter.didNextStep(action: .close)
    }
}
