import Foundation

enum RegistrationRenewalIntroFactory {
    static func make(delegate: RegistrationRenewalIntroCoordinatorDelegate, origin: RegistrationRenewalOrigin) -> RegistrationRenewalIntroViewController {
        let container = DependencyContainer()
        let service: RegistrationRenewalIntroServicing = RegistrationRenewalIntroService(dependencies: container)
        let coordinator: RegistrationRenewalIntroCoordinating = RegistrationRenewalIntroCoordinator(delegate: delegate)
        let presenter: RegistrationRenewalIntroPresenting = RegistrationRenewalIntroPresenter(coordinator: coordinator)
        let viewModel = RegistrationRenewalIntroViewModel(
            service: service,
            presenter: presenter,
            dependencies: container,
            origin: origin
        )
        let viewController = RegistrationRenewalIntroViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
