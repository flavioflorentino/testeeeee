import Core
import Foundation

protocol RegistrationRenewalIntroServicing {
     func getRegistrationRenewalData(completion: @escaping (Result<RegistrationRenewalResponse, ApiError>) -> Void)
}

final class RegistrationRenewalIntroService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - RegistrationRenewalIntroServicing
extension RegistrationRenewalIntroService: RegistrationRenewalIntroServicing {
    func getRegistrationRenewalData(completion: @escaping (Result<RegistrationRenewalResponse, ApiError>) -> Void) {
        let endpoint = RegistrationRenewalEndpoint.registrationRenewalScreens
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        Api<RegistrationRenewalResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            let mappedResult = result.map(\.model)
            
            self?.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
}
