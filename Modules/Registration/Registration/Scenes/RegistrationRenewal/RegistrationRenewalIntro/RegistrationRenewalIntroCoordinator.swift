import UIKit

enum RegistrationRenewalIntroAction {
    case openForm(model: RegistrationRenewalForm, userProfile: RegistrationRenewalUserProfile)
    case close
}

protocol RegistrationRenewalIntroCoordinatorDelegate: AnyObject {
    func close()
    func openForm(model: RegistrationRenewalForm, userProfile: RegistrationRenewalUserProfile)
}

protocol RegistrationRenewalIntroCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RegistrationRenewalIntroAction)
}

final class RegistrationRenewalIntroCoordinator {
    private weak var delegate: RegistrationRenewalIntroCoordinatorDelegate?
    weak var viewController: UIViewController?
    
    init(delegate: RegistrationRenewalIntroCoordinatorDelegate) {
        self.delegate = delegate
    }
}

// MARK: - RegistrationRenewalIntroCoordinating
extension RegistrationRenewalIntroCoordinator: RegistrationRenewalIntroCoordinating {
    func perform(action: RegistrationRenewalIntroAction) {
        switch action {
        case let .openForm(formModel, userProfile):
            delegate?.openForm(model: formModel, userProfile: userProfile)
        case .close:
            delegate?.close()
        }
    }
}
