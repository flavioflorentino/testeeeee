import SnapKit
import UI
import UIKit

protocol RegistrationRenewalIntroDisplay: AnyObject {
    func showLoading()
    func hideLoading()
    func updateLabels(title: String, description: String)
    func displayErrorPopup(title: String?, message: String, buttonTitle: String, closeCompletion: (() -> Void)?)
}

private extension RegistrationRenewalIntroViewController.Layout {
    enum Size {
        static let imageHeight: CGFloat = 200
    }
}

public final class RegistrationRenewalIntroViewController: ViewController<RegistrationRenewalIntroViewModelInputs, UIView>, LoadingViewProtocol {
    fileprivate enum Layout { }
    
    public lazy var loadingView = LoadingView()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView(image: Assets.iluFacialBiometry.image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.labelStyle(TitleLabelStyle(type: .xLarge))
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.labelStyle(BodyPrimaryLabelStyle())
        return label
    }()
    
    private lazy var confirmationButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Strings.Button.update, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(tapConfirmationButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var containerView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [imageView, titleLabel, descriptionLabel, confirmationButton])
        view.spacing = Spacing.base03
        view.axis = .vertical
        return view
    }()
    
    private func setupNavigationBar() {
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: Assets.icoRoundCloseGreen.image,
            style: .done,
            target: self,
            action: #selector(tapClose)
        )
    }

    override public func buildViewHierarchy() {
        view.addSubview(containerView)
    }
    
    override public func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.centerY.equalToSuperview()
            $0.bottom.lessThanOrEqualToSuperview().inset(Spacing.base03)
        }
        
        imageView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.imageHeight)
        }
    }

    override public func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        setupNavigationBar()
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchChecklistFormData()
    }
}

@objc
extension RegistrationRenewalIntroViewController {
    private func tapConfirmationButton() {
        viewModel.didConfirm()
    }
    
    private func tapClose() {
        viewModel.didClose()
    }
}

// MARK: RegistrationRenewalIntroDisplay
extension RegistrationRenewalIntroViewController: RegistrationRenewalIntroDisplay {
    func updateLabels(title: String, description: String) {
        titleLabel.text = title
        descriptionLabel.text = description
    }
    
    func displayErrorPopup(title: String?, message: String, buttonTitle: String, closeCompletion: (() -> Void)?) {
        showErrorPopup(message: message, buttonTitle: buttonTitle, title: title, closeCompletion: closeCompletion)
    }
    
    func showLoading() {
        startLoadingView()
    }
    
    func hideLoading() {
        stopLoadingView()
    }
}
