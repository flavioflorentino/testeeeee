import Foundation

public enum RegistrationRenewalOrigin: String {
    case notification = "Notification"
    case settings = "Settings"
}

public struct RegistrationRenewalDependencies {
    let addressSelector: RegistrationRenewalAddressInput
    let authManager: RegistrationRenewalAuthDelegate
    let origin: RegistrationRenewalOrigin
    
    public init(
        addressSelector: RegistrationRenewalAddressInput,
        authManager: RegistrationRenewalAuthDelegate,
        origin: RegistrationRenewalOrigin
    ) {
        self.addressSelector = addressSelector
        self.authManager = authManager
        self.origin = origin
    }
}
