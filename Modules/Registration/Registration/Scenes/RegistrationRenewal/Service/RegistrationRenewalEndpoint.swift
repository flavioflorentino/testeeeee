import Foundation
import Core

enum RegistrationRenewalEndpoint {
    case registrationRenewalScreens
    case saveRegistrationUpdate(String)
    case registrationRenewalCategory(type: RegistrationRenewalItemType)
    case saveRegistrationRenewalCategory(RegistrationRenewalSaveModel)
    case updateRegistrationField(RegistrationRenewalSaveModel)
    case validateCode(RegistrationRenewalCodeModel)
    case resendCode(RegistrationRenewalItemTypeModel)
}

extension RegistrationRenewalEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .registrationRenewalScreens:
            return "registration-manager/register/update/screen/main"
        case .saveRegistrationUpdate:
            return "registration-manager/register/update/save"
        case .saveRegistrationRenewalCategory,
             .updateRegistrationField:
            return "registration-manager/register/update/field/save"
        case .registrationRenewalCategory:
            return "registration-manager/register/update/screen/list"
        case .validateCode:
            return "registration-manager/register/update/field/validate"
        case .resendCode:
            return "registration-manager/register/update/field/validate/resend"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .registrationRenewalScreens,
             .registrationRenewalCategory:
            return .get
        default:
            return .post
        }
    }
    
    var customHeaders: [String: String] {
        switch self {
        case let .saveRegistrationUpdate(password):
            return ["Password": password]
        default:
            return [:]
        }
    }
    
    var body: Data? {
        switch self {
        case let .updateRegistrationField(fieldModel):
            return prepareBody(with: fieldModel)
        case let .saveRegistrationRenewalCategory(fieldModel):
            return prepareBody(with: fieldModel)
        case let .validateCode(codeModel):
            return prepareBody(with: codeModel)
        case let .resendCode(itemTypeModel):
            return prepareBody(with: itemTypeModel)
        default:
            return nil
        }
    }
    
    var parameters: [String: Any] {
        switch self {
        case .registrationRenewalCategory(let type):
            return [Parameters.type: type.rawValue]
        default:
            return [:]
        }
    }
}

extension RegistrationRenewalEndpoint {
    private enum Parameters {
        static let type = "type"
    }
}

private extension Array {
    func toData() -> Data? {
        try? JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
    }
}
