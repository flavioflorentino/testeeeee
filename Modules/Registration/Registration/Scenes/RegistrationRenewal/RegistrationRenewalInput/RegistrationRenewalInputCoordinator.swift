import UIKit

protocol RegistrationRenewalInputDelegate: AnyObject {
    func didChangeItem(toValue rawValue: String, formatedValue: String)
    func backToForm()
}

enum RegistrationRenewalInputAction: Equatable {
    case validation(
        inputType: RegistrationRenewalInputType,
        inputedValue: String,
        codeResendDelay: Int?,
        userProfile: RegistrationRenewalUserProfile
    )
    case backToForm
}

protocol RegistrationRenewalInputCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RegistrationRenewalInputAction)
}

final class RegistrationRenewalInputCoordinator {
    weak var viewController: UIViewController?
    private weak var delegate: RegistrationRenewalInputDelegate?
    
    init(delegate: RegistrationRenewalInputDelegate) {
        self.delegate = delegate
    }
}

// MARK: - RegistrationRenewalInputCoordinating
extension RegistrationRenewalInputCoordinator: RegistrationRenewalInputCoordinating {
    func perform(action: RegistrationRenewalInputAction) {
        switch action {
        case .backToForm:
            viewController?.navigationController?.popToRootViewController(animated: true)
        case let .validation(inputType, inputedValue, codeResendDelay, userProfile):
            let controller: UIViewController
                
            switch inputType {
            case .phone:
                controller = RegistrationRenewalPhoneCodeFactory.make(
                    delegate: delegate,
                    inputedValue: inputedValue,
                    codeResendDelay: codeResendDelay,
                    userProfile: userProfile
                )
            case .email:
                controller = RegistrationRenewalEmailCodeFactory.make(
                    delegate: delegate,
                    inputedValue: inputedValue,
                    codeResendDelay: codeResendDelay,
                    userProfile: userProfile
                )
            }
            viewController?.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
