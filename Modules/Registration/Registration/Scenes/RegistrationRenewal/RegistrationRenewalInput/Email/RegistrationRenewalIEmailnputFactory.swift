import UIKit
import Foundation

enum RegistrationRenewalEmailInputFactory {
    static func make(
        delegate: RegistrationRenewalInputDelegate,
        currentValue: String?,
        userProfile: RegistrationRenewalUserProfile
    ) -> UIViewController {
        let container = DependencyContainer()
        let service: RegistrationRenewalInputServicing = RegistrationRenewalEmailInputService(dependencies: container)
        let coordinator: RegistrationRenewalInputCoordinating = RegistrationRenewalInputCoordinator(delegate: delegate)
        let presenter: RegistrationRenewalInputPresenting = RegistrationRenewalInputPresenter(coordinator: coordinator)
        let viewModel = RegistrationRenewalInputViewModel(
            service: service,
            presenter: presenter,
            dependencies: container,
            inputType: .email,
            inputValue: currentValue,
            userProfile: userProfile
        )
        let viewController = RegistrationRenewalInputViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
