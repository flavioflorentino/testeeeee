import Foundation
import Core

final class RegistrationRenewalEmailInputService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - RegistrationRenewalPhoneInputServicing
extension RegistrationRenewalEmailInputService: RegistrationRenewalInputServicing {
    func changeRegistrationValue(
        to value: String,
        completion: @escaping (Result<RegistrationRenewalInputResponse, ApiError>) -> Void
    ) {
        let fieldModel = RegistrationRenewalSaveModel(type: .email, value: value)
        let endpoint = RegistrationRenewalEndpoint.updateRegistrationField(fieldModel)
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        Api<RegistrationRenewalEmailInputResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            let mappedResult = result.map { RegistrationRenewalInputResponse(delay: $0.model.emailDelay) }
            
            self?.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
}
