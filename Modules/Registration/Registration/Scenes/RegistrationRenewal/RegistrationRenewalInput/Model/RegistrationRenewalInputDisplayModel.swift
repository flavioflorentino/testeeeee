import Foundation

struct RegistrationRenewalInputDisplayModel: Equatable {
    let title: String
    let message: String
    let placeholder: String
    let value: String
    
    init(inputType: RegistrationRenewalInputType, value: String) {
        switch inputType {
        case .phone:
            title = Strings.Input.Phone.title
            message = Strings.Input.Phone.message
            placeholder = Strings.Input.Phone.placeholder
        case .email:
            title = Strings.Input.Email.title
            message = Strings.Input.Email.message
            placeholder = Strings.Input.Email.placeholder
        }
        
        self.value = value
    }
}
