import Foundation

enum RegistrationRenewalInputType: String {
    case email
    case phone
}
