import Foundation

struct RegistrationRenewalInputResponse {
    let delay: Int
}

struct RegistrationRenewalPhoneInputResponse: Decodable {
    let smsDelay: Int
}

struct RegistrationRenewalEmailInputResponse: Decodable {
    let emailDelay: Int
}
