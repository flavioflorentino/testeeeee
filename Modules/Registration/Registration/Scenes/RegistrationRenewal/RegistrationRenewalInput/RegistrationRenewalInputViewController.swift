import UIKit
import Foundation
import SecurityModule
import SkyFloatingLabelTextField
import SnapKit
import UI

protocol RegistrationRenewalInputDisplay: AnyObject {
    func display(_ displayModel: RegistrationRenewalInputDisplayModel)
    func setupPhoneNumberInputTextField()
    func setupEmailInputTextField()
    func updateButtonsState(areEnabled: Bool)
    func displayConfirmationPopup(withTitle title: String, message: String)
    func showLoading()
    func hideLoading()
    func displayErrorPopup(title: String?, message: String, buttonTitle: String)
    func displayConfirmBackActionPopup(title: String, message: String, backTitle: String, closeTitle: String)
}

private extension RegistrationRenewalInputViewController.Layout {
    enum Size {
        static let textFieldHeight: CGFloat = 48.0
    }
}

final class RegistrationRenewalInputViewController: ViewController<RegistrationRenewalInputViewModelInputs, UIView>, Obfuscable {
    fileprivate enum Layout { }

    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        return label
    }()
    
    private lazy var inputValueTextField: UITextField = {
        let textField = SkyFloatingLabelTextField()
        textField.lineHeight = 1
        textField.titleColor = Colors.grayscale300.color
        textField.selectedTitleColor = Colors.grayscale300.color
        textField.selectedLineColor = Colors.branding400.color
        textField.textColor = Colors.grayscale700.color
        textField.titleFormatter = { $0 }
        
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        textField.addTarget(self, action: #selector(textFieldEditingChanged(_:)), for: .editingChanged)
        
        return textField
    }()
    
    private lazy var confirmationButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Strings.Button.confirm, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(tapConfirmationButton), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    private lazy var alreadyHaveCodeButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Strings.Button.alreadyHaveCode, for: .normal)
        button
            .buttonStyle(LinkButtonStyle())
            .with(\.textAttributedColor, (color: .grayscale600(), state: .normal))
            .with(\.textAttributedColor, (color: .grayscale600(), state: .highlighted))
        button.addTarget(self, action: #selector(tapAlreadyHaveCodeButton), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    private var inputMask: TextMask?
    
    // MARK: - Screen obfuscation
    var appSwitcherObfuscator: AppSwitcherObfuscating?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.populateView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupObfuscationConfiguration()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        inputValueTextField.becomeFirstResponder()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        deinitObfuscator()
    }

    override func buildViewHierarchy() {
        view.addSubview(messageLabel)
        view.addSubview(inputValueTextField)
        view.addSubview(confirmationButton)
        view.addSubview(alreadyHaveCodeButton)
    }

    override func setupConstraints() {
        messageLabel.snp.makeConstraints {
            $0.leading.trailing.top.equalToSuperview().inset(Spacing.base02)
        }
        
        inputValueTextField.snp.makeConstraints {
            $0.top.equalTo(messageLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.Size.textFieldHeight)
        }
        
        confirmationButton.snp.makeConstraints {
            $0.top.equalTo(inputValueTextField.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        alreadyHaveCodeButton.snp.makeConstraints {
            $0.top.equalTo(confirmationButton.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: Assets.icoChevronLeft.image,
            style: .plain,
            target: self,
            action: #selector(tapBackButton)
        )
        setupNavigationBar()
    }
    
    private func setupNavigationBar() {
        navigationController?.navigationBar.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationItem.largeTitleDisplayMode = .always
            navigationController?.navigationBar.prefersLargeTitles = true
        }
    }
    
    @objc
    private func tapConfirmationButton() {
        viewModel.didConfirm()
    }
    
    @objc
    private func tapAlreadyHaveCodeButton() {
        viewModel.didHaveCode()
    }
    
    @objc
    private func textFieldEditingChanged(_ textField: UITextField) {
        if let inputMask = inputMask {
            textField.text = inputMask.maskedText(from: textField.text)
        }
        
        viewModel.updateInputValue(textField.text)
    }
    
    @objc
    private func tapBackButton() {
        viewModel.didBack()
    }
}

// MARK: RegistrationRenewalInputDisplay
extension RegistrationRenewalInputViewController: RegistrationRenewalInputDisplay {
    func display(_ displayModel: RegistrationRenewalInputDisplayModel) {
        title = displayModel.title
        messageLabel.text = displayModel.message
        inputValueTextField.placeholder = displayModel.placeholder
        inputValueTextField.text = displayModel.value
        textFieldEditingChanged(inputValueTextField)
    }
    
    func setupPhoneNumberInputTextField() {
        inputMask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cellPhoneWithDDD)
        inputValueTextField.keyboardType = .numberPad
    }
    
    func setupEmailInputTextField() {
        inputValueTextField.keyboardType = .emailAddress
    }
    
    func updateButtonsState(areEnabled: Bool) {
        confirmationButton.isEnabled = areEnabled
        alreadyHaveCodeButton.isEnabled = areEnabled
    }
    
    func displayConfirmationPopup(withTitle title: String, message: String) {
        let popupController = PopupViewController(title: title, description: message, preferredType: .text)
        let yesAction = PopupAction(title: Strings.Button.yes, style: .fill) { [weak self] in
            self?.viewModel.didSecondConfirmation()
        }
        let noAction = PopupAction(title: Strings.Button.no, style: .default)
        popupController.addAction(yesAction)
        popupController.addAction(noAction)
        
        showPopup(popupController)
    }
    
    func showLoading() {
        confirmationButton.startLoading()
    }
    
    func hideLoading() {
        confirmationButton.stopLoading()
    }
    
    func displayErrorPopup(title: String?, message: String, buttonTitle: String) {
        showErrorPopup(message: message, buttonTitle: buttonTitle, title: title)
    }
    
    func displayConfirmBackActionPopup(title: String, message: String, backTitle: String, closeTitle: String) {
        let popupViewController = PopupViewController(title: title, description: message, preferredType: .text)

        popupViewController.addAction(PopupAction(title: backTitle, style: .destructive, completion: { [weak self] in
           self?.viewModel.didConfirmBackAction()
        }))

        popupViewController.addAction(PopupAction(title: closeTitle, style: .default))

        showPopup(popupViewController)
    }
}
