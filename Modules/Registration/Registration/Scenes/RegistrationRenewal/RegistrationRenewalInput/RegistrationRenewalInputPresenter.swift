import Foundation

protocol RegistrationRenewalInputPresenting: AnyObject {
    var viewController: RegistrationRenewalInputDisplay? { get set }
    func populateView(for inputType: RegistrationRenewalInputType, value: String)
    func didNextStep(action: RegistrationRenewalInputAction)
    func updateButtonsState(areEnabled: Bool)
    func presentConfirmation(for inputType: RegistrationRenewalInputType, value: String)
    func presentLoading()
    func hideLoading()
    func presentErrorPopup(title: String?, message: String, buttonTitle: String)
    func presentConfirmBackActionPopup()
}

final class RegistrationRenewalInputPresenter {
    private let coordinator: RegistrationRenewalInputCoordinating
    weak var viewController: RegistrationRenewalInputDisplay?

    init(coordinator: RegistrationRenewalInputCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - RegistrationRenewalInputPresenting
extension RegistrationRenewalInputPresenter: RegistrationRenewalInputPresenting {
    func populateView(for inputType: RegistrationRenewalInputType, value: String) {
        if inputType  == .phone {
            viewController?.setupPhoneNumberInputTextField()
        } else {
            viewController?.setupEmailInputTextField()
        }
        
        let displayModel = RegistrationRenewalInputDisplayModel(inputType: inputType, value: value)
        viewController?.display(displayModel)
    }
    
    func didNextStep(action: RegistrationRenewalInputAction) {
        coordinator.perform(action: action)
    }
    
    func updateButtonsState(areEnabled: Bool) {
        viewController?.updateButtonsState(areEnabled: areEnabled)
    }
    
    func presentConfirmation(for inputType: RegistrationRenewalInputType, value: String) {
        let title = inputType == .phone ? Strings.Input.Phone.confirmationTitle : Strings.Input.Email.confirmationTitle
        viewController?.displayConfirmationPopup(withTitle: title, message: value)
    }
    
    func presentLoading() {
        viewController?.showLoading()
    }
    
    func hideLoading() {
        viewController?.hideLoading()
    }
    
    func presentErrorPopup(title: String?, message: String, buttonTitle: String) {
        viewController?.displayErrorPopup(title: title, message: message, buttonTitle: buttonTitle)
    }
    
    func presentConfirmBackActionPopup() {
        viewController?.displayConfirmBackActionPopup(
            title: Strings.Validation.Alert.title,
            message: Strings.Validation.Alert.message,
            backTitle: Strings.Button.discard,
            closeTitle: Strings.Button.continueFilling
        )
    }
}
