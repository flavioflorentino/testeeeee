import Foundation
import AnalyticsModule
import Core
import UI

protocol RegistrationRenewalInputViewModelInputs: AnyObject {
    func populateView()
    func didConfirm()
    func didSecondConfirmation()
    func didHaveCode()
    func updateInputValue(_ inputValue: String?)
    func didConfirmBackAction()
    func didBack()
}

final class RegistrationRenewalInputViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let service: RegistrationRenewalInputServicing
    private let presenter: RegistrationRenewalInputPresenting
    private let userProfile: RegistrationRenewalUserProfile
    
    private let inputType: RegistrationRenewalInputType
    private let oldValue: String
    private var inputValue: String
    private let cellPhoneWithDDDLength = PersonalDocumentMaskDescriptor.cellPhoneWithDDD.format.count
    
    private var rawInputValue: String {
        switch inputType {
        case .email:
            return inputValue
        case .phone:
            return inputValue.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        }
    }
    
    private lazy var emailValidationRegex: NSPredicate = {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegEx)
    }()
    
    private var isValidInputValue: Bool {
        guard inputValue != oldValue else {
            return false
        }
        
        switch inputType {
        case .email:
            return emailValidationRegex.evaluate(with: inputValue)
        case .phone:
            return inputValue.count == cellPhoneWithDDDLength
        }
    }

    init(
        service: RegistrationRenewalInputServicing,
        presenter: RegistrationRenewalInputPresenting,
        dependencies: Dependencies,
        inputType: RegistrationRenewalInputType,
        inputValue: String?,
        userProfile: RegistrationRenewalUserProfile
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.inputType = inputType
        self.inputValue = inputValue ?? ""
        self.userProfile = userProfile
        oldValue = self.inputValue
    }
    
    private func trackDiscartChangesAction() {
        let event = RegistrationRenewalEvent.didDiscardChangedData(userProfile: userProfile, discartedData: inputType)
        dependencies.analytics.log(event)
    }
}

// MARK: - RegistrationRenewalInputViewModelInputs
extension RegistrationRenewalInputViewModel: RegistrationRenewalInputViewModelInputs {
    func populateView() {
        presenter.populateView(for: inputType, value: inputValue)
        presenter.updateButtonsState(areEnabled: isValidInputValue)
    }
    
    func didConfirm() {
        presenter.presentConfirmation(for: inputType, value: inputValue)
    }
    
    func didSecondConfirmation() {
        presenter.presentLoading()
        
        service.changeRegistrationValue(to: rawInputValue) { [weak self] result in
            guard let self = self else {
                return
            }
            self.presenter.hideLoading()
            
            switch result {
            case .success (let response):
                let action = RegistrationRenewalInputAction.validation(
                    inputType: self.inputType,
                    inputedValue: self.inputValue,
                    codeResendDelay: response.delay,
                    userProfile: self.userProfile
                )
                self.presenter.didNextStep(action: action)
            case .failure(let error):
                let requestError = error.requestError ?? RequestError()
                self.presenter.presentErrorPopup(
                    title: requestError.title,
                    message: requestError.message,
                    buttonTitle: Strings.Button.ok
                )
            }
        }
    }
    
    func didHaveCode() {
        let action = RegistrationRenewalInputAction.validation(
            inputType: inputType,
            inputedValue: inputValue,
            codeResendDelay: nil,
            userProfile: userProfile
        )
        presenter.didNextStep(action: action)
    }
    
    func updateInputValue(_ inputValue: String?) {
        guard let value = inputValue else {
            return
        }
        self.inputValue = value
        presenter.updateButtonsState(areEnabled: isValidInputValue)
    }
    
    func didConfirmBackAction() {
        trackDiscartChangesAction()
        presenter.didNextStep(action: .backToForm)
    }
    
    func didBack() {
        presenter.presentConfirmBackActionPopup()
    }
}
