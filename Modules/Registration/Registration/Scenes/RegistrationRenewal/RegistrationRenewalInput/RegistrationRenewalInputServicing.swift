import Core

protocol RegistrationRenewalInputServicing {
    func changeRegistrationValue(
        to value: String,
        completion: @escaping (Result<RegistrationRenewalInputResponse, ApiError>) -> Void
    )
}
