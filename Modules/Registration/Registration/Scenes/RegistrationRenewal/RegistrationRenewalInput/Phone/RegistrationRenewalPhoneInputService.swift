import Foundation
import Core

final class RegistrationRenewalPhoneInputService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - RegistrationRenewalPhoneInputServicing
extension RegistrationRenewalPhoneInputService: RegistrationRenewalInputServicing {
    func changeRegistrationValue(
        to value: String,
        completion: @escaping (Result<RegistrationRenewalInputResponse, ApiError>) -> Void
    ) {
        let fieldModel = RegistrationRenewalSaveModel(type: .phone, value: value)
        let endpoint = RegistrationRenewalEndpoint.updateRegistrationField(fieldModel)
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        Api<RegistrationRenewalPhoneInputResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            let mappedResult = result.map { RegistrationRenewalInputResponse(delay: $0.model.smsDelay) }
            
            self?.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
}
