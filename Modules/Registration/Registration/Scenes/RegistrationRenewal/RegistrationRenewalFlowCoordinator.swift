import UIKit
import Foundation
import UI

public final class RegistrationRenewalFlowCoordinator: Coordinating {
    private let rootNavigationController: UINavigationController
    private lazy var navigationController: UINavigationController = {
        let viewController = RegistrationRenewalIntroFactory.make(delegate: self, origin: dependencies.origin)
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.view.backgroundColor = Colors.white.color
        
        return navigationController
    }()
    
    private let dependencies: RegistrationRenewalDependencies
    private var childCoordinators: [Coordinating] = []
    
    public var childViewController: [UIViewController] = []
    public var viewController: UIViewController?
    
    public init(with rootNavigationController: UINavigationController, dependencies: RegistrationRenewalDependencies) {
        self.rootNavigationController = rootNavigationController
        self.dependencies = dependencies
    }
    
    public func start() {
        rootNavigationController.present(navigationController, animated: true)
    }
}

extension RegistrationRenewalFlowCoordinator: RegistrationRenewalIntroCoordinatorDelegate {
    func openForm(model: RegistrationRenewalForm, userProfile: RegistrationRenewalUserProfile) {
        let viewController = RegistrationRenewalFormFactory.make(
            with: model,
            delegate: self,
            auth: dependencies.authManager,
            userProfile: userProfile
        )
        navigationController.setViewControllers([viewController], animated: true)
    }
    
    func close() {
        navigationController.dismiss(animated: true)
    }
}

extension RegistrationRenewalFlowCoordinator: RegistrationRenewalFormCoordinatorDelegate, RenewalSuccessCoordinatorDelegate {
    func success(model: RegistrationRenewalSuccessModel) {
        let viewController = RenewalSuccessFactory.make(model: model, delegate: self)
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = .fade
        navigationController.view.layer.add(transition, forKey: nil)

        navigationController.pushViewController(viewController, animated: false)
    }
    
    func openAddressRenewal(idAddress: String?, completion: @escaping (String, String) -> Void) {
        dependencies.addressSelector.openAddressList(
            idAddress: idAddress,
            navigationController: navigationController,
            completion: completion
        )
    }
}
