import Core
import Foundation

public protocol RenewalAddressServiceServicing {
    func saveAddress(value: String, completion: @escaping (Result<NoContent, ApiError>) -> Void)
}

public final class RenewalAddressServiceService: RenewalAddressServiceServicing {
    public init() { }
    
    public func saveAddress(value: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let model = RegistrationRenewalSaveModel(type: .address, value: value)
        let api = Api<NoContent>(endpoint: RegistrationRenewalEndpoint.saveRegistrationRenewalCategory(model))
        
        api.execute { result in
            let mappedResult = result
                .map(\.model)
            
            DispatchQueue.main.async {
                completion(mappedResult)
            }
        }
    }
}
