import UIKit
import Foundation

public protocol RegistrationRenewalAddressInput: AnyObject {
    func openAddressList(
        idAddress: String?,
        navigationController: UINavigationController?,
        completion: @escaping (_ id: String, _ displayValue: String) -> Void
    )
}
