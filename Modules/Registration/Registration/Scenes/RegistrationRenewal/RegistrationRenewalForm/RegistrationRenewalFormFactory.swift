import Foundation

enum RegistrationRenewalFormFactory {
    static func make(
        with model: RegistrationRenewalForm,
        delegate: RegistrationRenewalFormCoordinatorDelegate,
        auth: RegistrationRenewalAuthDelegate,
        userProfile: RegistrationRenewalUserProfile
    ) -> RegistrationRenewalFormViewController {
        let container = DependencyContainer()
        let service: RegistrationRenewalFormServicing = RegistrationRenewalFormService(dependencies: container)
        let coordinator: RegistrationRenewalFormCoordinating = RegistrationRenewalFormCoordinator(delegate: delegate)
        
        let presenter: RegistrationRenewalFormPresenting = RegistrationRenewalFormPresenter(
            coordinator: coordinator,
            dependencies: container
        )
        
        let viewModel = RegistrationRenewalFormViewModel(
            service: service,
            presenter: presenter,
            dependencies: container,
            formModel: model,
            userProfile: userProfile
        )
        
        let viewController = RegistrationRenewalFormViewController(viewModel: viewModel, auth: auth)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
