import SecurityModule
import SnapKit
import UI
import UIKit

protocol RegistrationRenewalFormDisplay: AnyObject {
    func setupTableViewHandler(cellModels: [Section<String, RenewalFormCell>])
    func displayTitle(_ title: String)
    func displayErrorPopup(title: String?, message: String, buttonTitle: String)
    func displayPopupAuth()
    func showLoading()
    func hideLoading()
}

private extension RegistrationRenewalFormViewController.Layout {
    enum Size {
        static let estimatedRowHeight: CGFloat = 70
        static let estimatedSectionHeaderHeight: CGFloat = 90
        static let buttonHeight: CGFloat = 48
        static let tableViewFooterHeight: CGFloat = buttonHeight + 2 * Spacing.base02
    }
}

final class RegistrationRenewalFormViewController: ViewController<RegistrationRenewalFormViewModelInputs, UIView>, Obfuscable {
    fileprivate enum Layout {}
    private var auth: RegistrationRenewalAuthDelegate?
    
    init(viewModel: RegistrationRenewalFormViewModelInputs, auth: RegistrationRenewalAuthDelegate) {
        super.init(viewModel: viewModel)
        self.auth = auth
    }
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.register(
            RegistrationRenewalFormCell.self,
            forCellReuseIdentifier: String(describing: RegistrationRenewalFormCell.self)
        )
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Layout.Size.estimatedRowHeight
        tableView.estimatedSectionHeaderHeight = Layout.Size.estimatedSectionHeaderHeight
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.showsVerticalScrollIndicator = false
        
        let emptyFooterView = UIView()
        emptyFooterView.size.height = Layout.Size.tableViewFooterHeight
        tableView.tableFooterView = emptyFooterView
        
        return tableView
    }()
    
    private lazy var confirmationButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Strings.Button.confirmData, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(tapConfirmationButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var confirmationButtonContainer: UIView = {
        let blurEffect = UIBlurEffect(style: .regular)
        return UIVisualEffectView(effect: blurEffect)
    }()
    
    private var tableViewHandler: TableViewHandler<String, RenewalFormCell, RegistrationRenewalFormCell>?

    // MARK: - Screen obfuscation
    var appSwitcherObfuscator: AppSwitcherObfuscating?

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchFormData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupObfuscationConfiguration()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        deinitObfuscator()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(tableView)
        view.addSubview(confirmationButtonContainer)
        view.addSubview(confirmationButton)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        confirmationButton.snp.makeConstraints {
            $0.leading.trailing.top.equalTo(confirmationButtonContainer).inset(Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).inset(Spacing.base02)
        }
        
        confirmationButtonContainer.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        setupNavigationBar()
    }
    
    private func setupNavigationBar() {
        setupBackButton()
        navigationController?.navigationBar.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: Assets.icoRoundCloseGreen.image,
            style: .done,
            target: self,
            action: #selector(tapClose)
        )
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationItem.largeTitleDisplayMode = .always
            navigationController?.navigationBar.prefersLargeTitles = true
        }
    }

    private func setupBackButton() {
        let backButton = UIBarButtonItem()
        backButton.title = String()
        navigationItem.backBarButtonItem = backButton
    }
}

@objc
extension RegistrationRenewalFormViewController {
    private func tapClose() {
        viewModel.didClose()
    }

    private func tapConfirmationButton() {
        viewModel.didConfirm()
    }
}

// MARK: RegistrationRenewalFormDisplay
extension RegistrationRenewalFormViewController: RegistrationRenewalFormDisplay {    
    func setupTableViewHandler(cellModels: [Section<String, RenewalFormCell>]) {
        tableViewHandler = TableViewHandler(
            data: cellModels,
            configureCell: { _, model, cell in
                cell.setupCell(with: model)
            },
            configureSection: { _, section in
                guard let message = section.header else {
                    return nil
                }
                return .view(RegistrationRenewalFormHeaderView(message: message))
            },
            configureDidSelectRow: { indexPath, _ in
                self.viewModel.didSelectItem(at: indexPath)
            }
        )
        tableView.dataSource = tableViewHandler
        tableView.delegate = tableViewHandler
        tableView.reloadData()
    }
    
    func displayTitle(_ title: String) {
        self.title = title
    }
    
    func displayPopupAuth() {
        auth?.authenticate(sucessHandler: { [weak self] password, _ in
            self?.viewModel.sendConfirmation(password: password)
        }, cancelHandler: {})
    }
    
    func displayErrorPopup(title: String?, message: String, buttonTitle: String) {
        showErrorPopup(message: message, buttonTitle: buttonTitle, title: title)
    }
    
    func showLoading() {
        tableView.isUserInteractionEnabled = false
        confirmationButton.startLoading()
    }
    
    func hideLoading() {
        tableView.isUserInteractionEnabled = true
        confirmationButton.stopLoading()
    }
}
