import FeatureFlag
import UI
import UIKit

protocol RegistrationRenewalFormPresenting: AnyObject {
    var viewController: RegistrationRenewalFormDisplay? { get set }
    func presentForm(_ formModel: RegistrationRenewalForm)
    func didNextStep(action: RegistrationRenewalFormAction)
    func presentErrorPopup(title: String?, message: String, buttonTitle: String)
    func presentLoading()
    func presentAuth()
    func hideLoading()
}

final class RegistrationRenewalFormPresenter {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    private let coordinator: RegistrationRenewalFormCoordinating
    weak var viewController: RegistrationRenewalFormDisplay?
    
    init(coordinator: RegistrationRenewalFormCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    private func createRenewalFormCell(items: [RegistrationRenewalFormItem]) -> [RenewalFormCell] {
        items.map { RenewalFormCell(title: $0.title,
                                    displayValue: obfuscateDataIfNeeded(item: $0),
                                    alert: $0.alertText,
                                    alertIsHidden: $0.alertText == nil)
        }
    }
    
    private func obfuscateDataIfNeeded(item: RegistrationRenewalFormItem) -> String? {
        guard dependencies.featureManager.isActive(.featureAppSecObfuscationMask) else {
            return item.displayValue
        }
        
        let displayValue: String?
        switch item.type {
        case .email:
            displayValue = StringObfuscationMasker.mask(email: item.displayValue)
        case .phone:
            displayValue = StringObfuscationMasker.mask(phone: item.value)
        default:
            displayValue = item.displayValue
        }
        return displayValue
    }
}

// MARK: - RegistrationRenewalFormPresenting
extension RegistrationRenewalFormPresenter: RegistrationRenewalFormPresenting {
    func presentForm(_ formModel: RegistrationRenewalForm) {
        viewController?.displayTitle(formModel.title)
        let formModelItems = createRenewalFormCell(items: formModel.items)
        viewController?.setupTableViewHandler(cellModels: [Section(header: formModel.subtitle, items: formModelItems)])
    }
    
    func didNextStep(action: RegistrationRenewalFormAction) {
        coordinator.perform(action: action)
    }
    
    func presentErrorPopup(title: String?, message: String, buttonTitle: String) {
        viewController?.displayErrorPopup(title: title, message: message, buttonTitle: buttonTitle)
    }
    
    func presentLoading() {
        viewController?.showLoading()
    }
    
    func presentAuth() {
        viewController?.displayPopupAuth()
    }
    
    func hideLoading() {
        viewController?.hideLoading()
    }
}
