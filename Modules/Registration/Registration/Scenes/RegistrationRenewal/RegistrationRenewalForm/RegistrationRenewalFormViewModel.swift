import AnalyticsModule
import Core
import Foundation

protocol RegistrationRenewalFormViewModelInputs: AnyObject {
    func fetchFormData()
    func didSelectItem(at indexPath: IndexPath)
    func didClose()
    func didConfirm()
    func sendConfirmation(password: String)
}

final class RegistrationRenewalFormViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let service: RegistrationRenewalFormServicing
    private let presenter: RegistrationRenewalFormPresenting
    private var formModel: RegistrationRenewalForm
    private let userProfile: RegistrationRenewalUserProfile
    
    init(
        service: RegistrationRenewalFormServicing,
        presenter: RegistrationRenewalFormPresenting,
        dependencies: Dependencies,
        formModel: RegistrationRenewalForm,
        userProfile: RegistrationRenewalUserProfile
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.formModel = formModel
        self.userProfile = userProfile
    }
    
    private func updateFormData(withNewValue newValue: String, newDisplayValue: String, at row: Int) {
        trackFieldUpdated(itemType: formModel.items[row].type)
        
        formModel.items[row].value = newValue
        formModel.items[row].displayValue = newDisplayValue
        formModel.items[row].alertText = nil
        presenter.presentForm(formModel)
    }
    
    private func trackEditFieldAction(itemType: RegistrationRenewalItemType) {
        let editEvent = RegistrationRenewalEvent.didTapEditField(userProfile: userProfile, selectedField: itemType)
        dependencies.analytics.log(editEvent)
    }
    
    private func trackFieldUpdated(itemType: RegistrationRenewalItemType) {
        let event = RegistrationRenewalEvent.didUpdateField(userProfile: userProfile, itemType: itemType)
        dependencies.analytics.log(event)
    }
}

// MARK: - RegistrationRenewalFormViewModelInputs
extension RegistrationRenewalFormViewModel: RegistrationRenewalFormViewModelInputs {
    func fetchFormData() {
        presenter.presentForm(formModel)
    }
    
    func didSelectItem(at indexPath: IndexPath) {
        guard indexPath.row < formModel.items.count else {
            return
        }
        
        let item = formModel.items[indexPath.row]
        trackEditFieldAction(itemType: item.type)
        
        let action: RegistrationRenewalFormAction = .editValue(
            of: item.type,
            fromValue: item.value,
            fromDisplayValue: item.displayValue,
            userProfile: userProfile,
            completion: { [weak self] newValue, newDisplayValue in
                self?.updateFormData(withNewValue: newValue, newDisplayValue: newDisplayValue, at: indexPath.row)
            }
        )
        presenter.didNextStep(action: action)
    }
    
    func didClose() {
        presenter.didNextStep(action: .close)
    }
    
    func didConfirm() {
        presenter.presentAuth()
    }
    
    func sendConfirmation(password: String) {
        presenter.presentLoading()
        
        service.sendConfirmation(password: password) { [weak self] result in
            self?.presenter.hideLoading()
            switch result {
            case let .success(successModel):
                self?.presenter.didNextStep(action: .success(successModel))
            case .failure(let error):
                let requestError = error.requestError ?? RequestError()
                self?.sendConfirmationError(requestError: requestError)
            }
        }
    }
    
    private func sendConfirmationError(requestError: RequestError) {
        guard errorContainsAlert(requestError: requestError),
            let formError = decodeErrorToFormError(requestError: requestError) else {
            return presentError(requestError: requestError)
        }
        
        updateFormWithError(formError: formError)
        presentError(requestError: requestError)
    }
    
    private func updateFormWithError(formError: RenewalFormError) {
        for (index, element) in formModel.items.enumerated() {
            if let error = formError.items.first(where: { $0.type == element.type }) {
                formModel.items[index].alertText = error.alertText
            }
        }
        
        presenter.presentForm(formModel)
    }
    
    private func decodeErrorToFormError(requestError: RequestError) -> RenewalFormError? {
        guard
            !requestError.data.isEmpty,
            let data = try? JSONSerialization.data(withJSONObject: requestError.data, options: .prettyPrinted) else {
            return nil
        }
        
        let decode = JSONDecoder(.convertFromSnakeCase)
        return try? decode.decode(RenewalFormError.self, from: data)
    }
    
    private func presentError(requestError: RequestError) {
        presenter.presentErrorPopup(title: requestError.title, message: requestError.message, buttonTitle: Strings.Button.ok)
    }
    
    private func errorContainsAlert(requestError: RequestError) -> Bool {
        requestError.code == "1005"
    }
}
