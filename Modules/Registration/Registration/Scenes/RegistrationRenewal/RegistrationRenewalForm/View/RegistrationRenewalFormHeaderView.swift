import SnapKit
import UI
import UIKit

final class RegistrationRenewalFormHeaderView: UIView {
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        return label
    }()
    
    init(message: String) {
        super.init(frame: .zero)
        messageLabel.text = message
        
        buildViewHierarchy()
        setupConstraints()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension RegistrationRenewalFormHeaderView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(messageLabel)
    }
    
    func setupConstraints() {
        messageLabel.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base02)
        }
    }
}
