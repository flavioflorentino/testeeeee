import SnapKit
import UI
import UIKit

extension RegistrationRenewalFormCell.Layout {
    enum Size {
        static let separatorLineHeight: CGFloat = 1.0
    }
}

final class RegistrationRenewalFormCell: UITableViewCell {
    fileprivate enum Layout {}
    static let identifier = String(describing: RegistrationRenewalFormCell.self)
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        return stackView
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale400())
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .grayscale850())
        return label
    }()
    
    private lazy var alertLabel: UILabel = {
        let label = UILabel()
        label.isHidden = true
        label.labelStyle(CaptionLabelStyle())
            .with(\.textColor, .critical600())
        return label
    }()
    
    private lazy var separatorInsetView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        return view
    }()
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }

    func setupCell(with model: RenewalFormCell) {
        nameLabel.text = model.title
        valueLabel.text = model.displayValue
        alertLabel.text = model.alert
        alertLabel.isHidden = model.alertIsHidden
    }
}

extension RegistrationRenewalFormCell: ViewConfiguration {
    func buildViewHierarchy() {
        stackView.addArrangedSubview(nameLabel)
        stackView.addArrangedSubview(valueLabel)
        stackView.addArrangedSubview(alertLabel)
        addSubview(stackView)
        addSubview(separatorInsetView)
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
        tintColor = Colors.branding600.color
        accessoryView = UIImageView(image: Assets.icoChevron.image)
        selectionStyle = .none
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.leading.trailing.top.bottom.equalToSuperview().inset(Spacing.base02)
        }
        
        separatorInsetView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.separatorLineHeight)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    private func setupView() {
        buildViewHierarchy()
        configureViews()
        setupConstraints()
    }
}
