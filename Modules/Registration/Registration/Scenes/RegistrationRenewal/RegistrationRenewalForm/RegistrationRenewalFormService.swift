import Core
import Foundation

protocol RegistrationRenewalFormServicing {
    func sendConfirmation(
        password: String,
        completion: @escaping ((Result<RegistrationRenewalSuccessModel, ApiError>) -> Void)
    )
}

final class RegistrationRenewalFormService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - RegistrationRenewalFormServicing
extension RegistrationRenewalFormService: RegistrationRenewalFormServicing {
    func sendConfirmation(
        password: String,
        completion: @escaping ((Result<RegistrationRenewalSuccessModel, ApiError>) -> Void)
    ) {
        let api = Api<RegistrationRenewalSuccessModel>(endpoint: RegistrationRenewalEndpoint.saveRegistrationUpdate(password))
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result
                    .map(\.model)
                completion(mappedResult)
            }
        }
    }
}
