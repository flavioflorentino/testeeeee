import UIKit

protocol RegistrationRenewalFormCoordinatorDelegate: AnyObject {
    func openAddressRenewal(idAddress: String?, completion: @escaping(_ id: String, _ displayValue: String) -> Void)
    func success(model: RegistrationRenewalSuccessModel)
    func close()
}

enum RegistrationRenewalFormAction {
    case editValue(
        of: RegistrationRenewalItemType,
        fromValue: String?,
        fromDisplayValue: String?,
        userProfile: RegistrationRenewalUserProfile,
        completion: ((String, String) -> Void)
    )
    case close
    case success(RegistrationRenewalSuccessModel)
}

protocol RegistrationRenewalFormCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RegistrationRenewalFormAction)
}

final class RegistrationRenewalFormCoordinator {
    private weak var delegate: RegistrationRenewalFormCoordinatorDelegate?
    private var updateValue: ((String, String) -> Void)?
    weak var viewController: UIViewController?
    
    init(delegate: RegistrationRenewalFormCoordinatorDelegate) {
        self.delegate = delegate
    }
}

// MARK: - RegistrationRenewalFormCoordinating
extension RegistrationRenewalFormCoordinator: RegistrationRenewalFormCoordinating {
    func perform(action: RegistrationRenewalFormAction) {
        switch action {
        case .close:
            delegate?.close()
        case let .editValue(type, value, displayValue, userProfile, completion):
            self.updateValue = completion
            openChangeValue(type: type, value: value, displayValue: displayValue, userProfile: userProfile)
        case let .success(model):
            delegate?.success(model: model)
        }
    }
    
    private func openChangeValue(
        type: RegistrationRenewalItemType,
        value: String?,
        displayValue: String?,
        userProfile: RegistrationRenewalUserProfile
    ) {
        guard let updateValue = updateValue else {
            return
        }
        
        let controller: UIViewController
        switch type {
        case .email:
            controller = RegistrationRenewalEmailInputFactory.make(
                delegate: self,
                currentValue: displayValue,
                userProfile: userProfile
            )
        case .phone:
            controller = RegistrationRenewalPhoneInputFactory.make(
                delegate: self,
                currentValue: displayValue,
                userProfile: userProfile
            )
        case .incomeRange:
            controller = RenewalSelectIncomeFactory.make(delegate: self, userProfile: userProfile)
        case .profession:
            controller = RenewalSelectProfessionFactory.make(delegate: self, userProfile: userProfile)
        case .wealthRange:
            controller = RenewalSelectWealthFactory.make(delegate: self, userProfile: userProfile)
        case .address:
            delegate?.openAddressRenewal(idAddress: value, completion: updateValue)
            return
        }
        
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}

extension RegistrationRenewalFormCoordinator: RenewalSelectCategoryCoordinatorDelegate {
    func didSelectItem(id: String, displayValue: String) {
        updateValue?(id, displayValue)
    }
}

extension RegistrationRenewalFormCoordinator: RegistrationRenewalInputDelegate {
    func didChangeItem(toValue newValue: String, formatedValue displayValue: String) {
        updateValue?(newValue, displayValue)
    }
    
    func backToForm() {
        viewController?.navigationController?.popToRootViewController(animated: true)
    }
}
