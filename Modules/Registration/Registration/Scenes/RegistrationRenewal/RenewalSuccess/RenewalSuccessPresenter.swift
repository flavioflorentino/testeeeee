import Foundation

protocol RenewalSuccessPresenting: AnyObject {
    var viewController: RenewalSuccessDisplay? { get set }
    func presenterView(title: String, subtitle: String)
    func didNextStep(action: RenewalSuccessAction)
}

final class RenewalSuccessPresenter {
    private let coordinator: RenewalSuccessCoordinating
    weak var viewController: RenewalSuccessDisplay?

    init(coordinator: RenewalSuccessCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - RenewalSuccessPresenting
extension RenewalSuccessPresenter: RenewalSuccessPresenting {
    func presenterView(title: String, subtitle: String) {
        viewController?.display(title: title, subtitle: subtitle)
    }
    
    func didNextStep(action: RenewalSuccessAction) {
        coordinator.perform(action: action)
    }
}
