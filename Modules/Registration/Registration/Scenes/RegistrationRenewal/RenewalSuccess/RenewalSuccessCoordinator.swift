import UIKit

protocol RenewalSuccessCoordinatorDelegate: AnyObject {
    func close()
}

enum RenewalSuccessAction {
    case back
}

protocol RenewalSuccessCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RenewalSuccessAction)
}

final class RenewalSuccessCoordinator {
    weak var delegate: RenewalSuccessCoordinatorDelegate?
    weak var viewController: UIViewController?
    
    init(delegate: RenewalSuccessCoordinatorDelegate) {
        self.delegate = delegate
    }
}

// MARK: - RenewalSuccessCoordinating
extension RenewalSuccessCoordinator: RenewalSuccessCoordinating {
    func perform(action: RenewalSuccessAction) {
        delegate?.close()
    }
}
