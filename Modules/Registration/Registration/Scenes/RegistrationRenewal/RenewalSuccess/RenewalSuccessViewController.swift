import UIKit
import SnapKit
import UI

protocol RenewalSuccessDisplay: AnyObject {
    func display(title: String, subtitle: String)
}

private extension RenewalSuccessViewController.Layout {
    enum Size {
        static let imageSize = CGSize(width: 90, height: 90)
    }
}

final class RenewalSuccessViewController: ViewController<RenewalSuccessViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.backgroundPrimary.color
        return view
    }()
    
    private lazy var successImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.icoSuccess.image
        imageView.contentMode = .scaleAspectFit
        
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.typography, .title(.xLarge))
            .with(\.textAlignment, .center)
        label.numberOfLines = 0
        
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
        
        return label
    }()
    
    private lazy var backButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Strings.Button.back, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(tapBackButton), for: .touchUpInside)
        
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.updateView()
    }
        
    override func buildViewHierarchy() {
        containerView.addSubview(successImage)
        containerView.addSubview(titleLabel)
        containerView.addSubview(subtitleLabel)
        containerView.addSubview(backButton)
        
        view.addSubview(containerView)
    }
    
    override func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
            $0.top.greaterThanOrEqualToSuperview()
            $0.bottom.lessThanOrEqualToSuperview()
        }
        
        successImage.snp.makeConstraints {
            $0.top.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.imageSize)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(successImage.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview()
        }
        
        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
        }
        
        backButton.snp.makeConstraints {
            $0.top.equalTo(subtitleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.isNavigationBarHidden = true
    }
}

@objc
private extension RenewalSuccessViewController {
    func tapBackButton() {
        viewModel.didBack()
    }
}

// MARK: RenewalSuccessDisplay
extension RenewalSuccessViewController: RenewalSuccessDisplay {
    func display(title: String, subtitle: String) {
        titleLabel.text = title
        subtitleLabel.text = subtitle
    }
}
