import Foundation

protocol RenewalSuccessViewModelInputs: AnyObject {
    func updateView()
    func didBack()
}

final class RenewalSuccessViewModel {
    private let presenter: RenewalSuccessPresenting
    private let model: RegistrationRenewalSuccessModel
    
    init(model: RegistrationRenewalSuccessModel, presenter: RenewalSuccessPresenting) {
        self.model = model
        self.presenter = presenter
    }
}

// MARK: - RenewalSuccessViewModelInputs
extension RenewalSuccessViewModel: RenewalSuccessViewModelInputs {
    func updateView() {
        presenter.presenterView(title: model.title, subtitle: model.description)
    }
    
    func didBack() {
        presenter.didNextStep(action: .back)
    }
}
