import Foundation

enum RenewalSuccessFactory {
    static func make(
        model: RegistrationRenewalSuccessModel,
        delegate: RenewalSuccessCoordinatorDelegate
    ) -> RenewalSuccessViewController {
        let coordinator: RenewalSuccessCoordinating = RenewalSuccessCoordinator(delegate: delegate)
        let presenter: RenewalSuccessPresenting = RenewalSuccessPresenter(coordinator: coordinator)
        let viewModel = RenewalSuccessViewModel(model: model, presenter: presenter)
        let viewController = RenewalSuccessViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
