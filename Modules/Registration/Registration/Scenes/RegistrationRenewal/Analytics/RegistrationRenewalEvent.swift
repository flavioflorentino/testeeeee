import AnalyticsModule
import Core

enum RegistrationRenewalEvent: AnalyticsKeyProtocol {
    case didViewIntro(userProfile: RegistrationRenewalUserProfile, origin: RegistrationRenewalOrigin)
    case didStarted(userProfile: RegistrationRenewalUserProfile)
    case didTapEditField(userProfile: RegistrationRenewalUserProfile, selectedField: RegistrationRenewalItemType)
    case didViewCodeValidation(userProfile: RegistrationRenewalUserProfile, inputType: RegistrationRenewalInputType)
    case didUpdateField(userProfile: RegistrationRenewalUserProfile, itemType: RegistrationRenewalItemType)
    case didTapWealthLink(userProfile: RegistrationRenewalUserProfile)
    case didAskToResentCode(userProfile: RegistrationRenewalUserProfile, inputType: RegistrationRenewalInputType)
    case didDiscardChangedData(userProfile: RegistrationRenewalUserProfile, discartedData: RegistrationRenewalInputType)
    case didReceiveInvalidCodeError(userProfile: RegistrationRenewalUserProfile, inputType: RegistrationRenewalInputType)
    case didReceiveCodeLimitExceedError(userProfile: RegistrationRenewalUserProfile, inputType: RegistrationRenewalInputType)
    case didReceiveFieldUpdateError(
        userProfile: RegistrationRenewalUserProfile,
        itemType: RegistrationRenewalItemType,
        errorType: String
    )
    
    private var name: String {
        switch self {
        case .didViewIntro:
            return "Viewed"
        case .didStarted:
            return "Started"
        case .didTapEditField:
            return "Editing"
        case .didViewCodeValidation:
            return "code validation started"
        case .didUpdateField:
            return "Data Updated"
        case .didTapWealthLink:
            return "Wealth Info"
        case .didAskToResentCode:
            return "Code Resent"
        case .didDiscardChangedData:
            return "Discarted Data"
        case .didReceiveInvalidCodeError:
            return "Code Invalid"
        case .didReceiveFieldUpdateError:
            return "Updated Error"
        case .didReceiveCodeLimitExceedError:
            return "Code Limit Exceeded"
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.firebase, .mixPanel]
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .didViewIntro(userProfile, origin):
            return ["user_profile": userProfile.rawValue, "origin": origin.rawValue]
        case let .didStarted(userProfile):
            return ["user_profile": userProfile.rawValue]
        case let .didTapEditField(userProfile, selectedField):
            return ["user_profile": userProfile.rawValue, "option_selected": selectedField.description]
        case let .didViewCodeValidation(userProfile, inputType):
            return ["user_profile": userProfile.rawValue, "option_selected": inputType.description]
        case let .didUpdateField(userProfile, itemType):
            return ["user_profile": userProfile.rawValue, "data_updated": itemType.description]
        case let .didTapWealthLink(userProfile):
            return ["user_profile": userProfile.rawValue]
        case let .didAskToResentCode(userProfile, inputType):
            return ["user_profile": userProfile.rawValue, "option_selected": inputType.description]
        case let .didDiscardChangedData(userProfile, discartedData):
            return ["user_profile": userProfile.rawValue, "discarted_data": discartedData.description]
        case let .didReceiveInvalidCodeError(userProfile, inputType):
            return ["user_profile": userProfile.rawValue, "option_selected": inputType.description]
        case let .didReceiveCodeLimitExceedError(userProfile, inputType):
            return ["user_profile": userProfile.rawValue, "option_selected": inputType.description]
        case let .didReceiveFieldUpdateError(userProfile, itemType, errorType):
            return ["user_profile": userProfile.rawValue, "data_updated": itemType.description, "error_type": errorType]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("Annual Data Update - " + name, properties: properties, providers: providers)
    }
}

private extension RegistrationRenewalInputType {
    var description: String {
        switch self {
        case .phone:
            return "PHONE NUMBER"
        case .email:
            return "EMAIL"
        }
    }
}

private extension RegistrationRenewalItemType {
    var description: String {
        switch self {
        case .phone:
            return "PHONE NUMBER"
        case .email:
            return "EMAIL"
        case .incomeRange:
            return "MONTHLY INCOME"
        case .address:
            return "ADDRESS"
        case .wealthRange:
            return "WEALTH"
        case .profession:
            return "PROFESSION"
        }
    }
}
