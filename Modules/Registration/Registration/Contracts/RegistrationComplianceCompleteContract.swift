import UIKit

public protocol RegistrationComplianceCompleteContract {
     func start(promoCode: String?) -> UIViewController
}

extension RegistrationComplianceCompleteContract {
    func start() -> UIViewController {
        start(promoCode: nil)
    }
}
