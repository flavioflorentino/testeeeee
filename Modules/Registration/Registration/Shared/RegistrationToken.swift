import Foundation

enum RegistrationTokenType: String, Codable {
    case permanent
    case temporary
}

struct RegistrationToken: Codable {
    let code: String
    let type: RegistrationTokenType
}
