import Foundation

public enum RegistrationComplianceTokenType: String, Decodable {
    case temporary
    case permanent
}
