import Foundation

public enum RegistrationAccountSetupFactory {
    public static func make() -> RegistrationAccountSetupContract {
        let container = DependencyContainer()
        let service: RegistrationAccountSetupServicing = RegistrationAccountSetupService(dependencies: container)
        let helper: RegistrationAccountSetupContract = RegistrationAccountSetupHelper(service: service)
 
        return helper
    }
}
