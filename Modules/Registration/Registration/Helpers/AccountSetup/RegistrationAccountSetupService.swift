import Core
import Foundation

protocol RegistrationAccountSetupServicing {
    func sendSetupAccount(
        model: RegistrationAccountSetupModel,
        completion: @escaping (Result<NoContent, ApiError>) -> Void
    )
}

final class RegistrationAccountSetupService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - RegistrationAccountSetupServicing
extension RegistrationAccountSetupService: RegistrationAccountSetupServicing {
    func sendSetupAccount(
        model: RegistrationAccountSetupModel,
        completion: @escaping (Result<NoContent, ApiError>) -> Void
    ) {
        let api = Api<NoContent>(endpoint: RegistrationAccountSetupEndPoint.sendSetupAccount(model: model))
        
        api.execute { result in
            let mappedResult = result
                .map(\.model)
            
            self.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
}
