import Foundation

public struct RegistrationAccountSetupModel: Encodable {
    let document: String
    let birthdate: String
    let phone: String
    let areaCode: String
    let firstName: String
    let lastName: String
    var fullName: String {
        "\(firstName) \(lastName)"
    }
    
    public init(document: String, birthdate: String, phone: String, areaCode: String, firstName: String, lastName: String) {
        self.document = document
        self.birthdate = birthdate
        self.areaCode = areaCode
        self.phone = phone
        self.firstName = firstName
        self.lastName = lastName
    }
}
