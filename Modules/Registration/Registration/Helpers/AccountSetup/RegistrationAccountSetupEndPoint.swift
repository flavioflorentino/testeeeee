import Core

enum RegistrationAccountSetupEndPoint {
    case sendSetupAccount(model: RegistrationAccountSetupModel)
}

extension RegistrationAccountSetupEndPoint: ApiEndpointExposable {
    var path: String {
        "registration-validation/create"
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var body: Data? {
        guard case let .sendSetupAccount(model) = self else {
            return nil
        }
        return [
            "cpf": model.document,
            "birthdate": model.birthdate,
            "name": model.fullName,
            "phone": model.areaCode + model.phone
        ].toData()
    }
}
