import Foundation

public protocol RegistrationAccountSetupContract: AnyObject {
    func setupAccount(model: RegistrationAccountSetupModel)
}

final class RegistrationAccountSetupHelper: RegistrationAccountSetupContract {
    private let service: RegistrationAccountSetupServicing
    
    init(service: RegistrationAccountSetupServicing) {
        self.service = service
    }
    
    func setupAccount(model: RegistrationAccountSetupModel) {
        service.sendSetupAccount(model: model) { _ in
            // TODO: -  Adicionar Analitycs
        }
    }
}
