import CustomerSupport
import Foundation

struct RegistrationDeniedCustomer {
    let development: FAQOptions = .home
    let production: FAQOptions = .article(id: "360046963132")
}

extension RegistrationDeniedCustomer {
    func option() -> FAQOptions {
        #if DEBUG
        return development
        #else
        return production
        #endif
    }
}
