import Core
import Foundation

public protocol RegistrationComplianceLoginHelperContract {
    func loginUserWithRestriction(
        login: String,
        password: String,
        completion: @escaping (Result<RegistrationComplianceLoginModel, Error>) -> Void
    )
}

final class RegistrationComplianceLoginHelper {
    let service: RegistrationComplianceLoginServicing
    
    init(service: RegistrationComplianceLoginServicing) {
        self.service = service
    }
}

extension RegistrationComplianceLoginHelper: RegistrationComplianceLoginHelperContract {
    func loginUserWithRestriction(
        login: String,
        password: String,
        completion: @escaping (Result<RegistrationComplianceLoginModel, Error>) -> Void
    ) {
        service.loginUserWithRestriction(
            login: login,
            password: password
        ) {  result in
            switch result {
            case .success(let model):
                completion(.success(model))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
