import Foundation

public protocol RegistrationRenewalAuthDelegate: AnyObject {    
    func authenticate(
        sucessHandler: @escaping (_ password: String, _ isBiometric: Bool) -> Void,
        cancelHandler: @escaping () -> Void
    )
}
