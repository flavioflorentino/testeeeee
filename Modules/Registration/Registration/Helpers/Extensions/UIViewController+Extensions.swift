import Foundation
import UI
import UIKit

extension UIViewController {
    func showErrorPopup(
        message: String,
        buttonTitle: String = Strings.Button.ok,
        title: String? = nil,
        closeCompletion: (() -> Void)? = nil
    ) {
        let popupViewController = PopupViewController(
            title: title,
            description: message,
            preferredType: .image(Assets.icoSad.image)
        )
        
        let confirmAction = PopupAction(title: buttonTitle, style: .fill, completion: closeCompletion)
        popupViewController.didCloseDismiss = closeCompletion
        popupViewController.addAction(confirmAction)
        
        showPopup(popupViewController)
    }
}
