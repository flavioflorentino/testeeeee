import Core

public protocol RegistrationSetupContract {
    var tokenManager: TokenManagerContract? { get set }
    var deeplinkInstance: DeeplinkContract? { get set }
    var authManagerInstance: AuthenticationManagerContract? { get set }
    var registrationInstance: RegistrationComplianceCompleteContract? { get set }
}

public final class RegistrationSetup: RegistrationSetupContract {
    public static let shared = RegistrationSetup()
    
    public var deeplinkInstance: DeeplinkContract?
    public var authManagerInstance: AuthenticationManagerContract?
    public var registrationInstance: RegistrationComplianceCompleteContract?
    public var tokenManager: TokenManagerContract?
    
    public func inject(instance: DeeplinkContract) {
        deeplinkInstance = instance
    }
    
    public func inject(instance: AuthenticationManagerContract) {
        authManagerInstance = instance
    }
    
    public func inject(instance: RegistrationComplianceCompleteContract) {
        registrationInstance = instance
    }
    
    public func inject(instance: TokenManagerContract) {
        tokenManager = instance
    }
}
