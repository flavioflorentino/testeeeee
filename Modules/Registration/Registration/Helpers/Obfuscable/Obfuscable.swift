import UIKit
import Core
import FeatureFlag
import SecurityModule

protocol Obfuscable: UIViewController {
    var appSwitcherObfuscator: AppSwitcherObfuscating? { get set }
    func deinitObfuscator()
}

extension Obfuscable where Self: UIViewController {
    func setupObfuscationConfiguration(window: UIWindow? = UIApplication.shared.keyWindow) {
        guard FeatureManager.isActive(.featureAppSwitcherObfuscation), let window = window else {
            return
        }
        appSwitcherObfuscator = AppSwitcherObfuscator(window: window)
        appSwitcherObfuscator?.setupObfuscationView(image: Assets.whiteLogo.image, appType: .personal)
    }
    
    func deinitObfuscator() {
        // Make sure to not show obfuscator view in screens that don't implement Obfuscable
        appSwitcherObfuscator = nil
    }
}
