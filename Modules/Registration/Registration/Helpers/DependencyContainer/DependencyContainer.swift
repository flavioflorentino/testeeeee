import Foundation
import AnalyticsModule
import Core
import FeatureFlag

typealias RegistrationDependencies = HasMainQueue & HasAnalytics & HasFeatureManager & HasLegacy

final class DependencyContainer: RegistrationDependencies {
    lazy var mainQueue = DispatchQueue.main
    lazy var analytics: AnalyticsProtocol = Analytics.shared
    lazy var featureManager: FeatureManagerContract = FeatureManager.shared
    lazy var legacy: RegistrationSetupContract = RegistrationSetup.shared
}
