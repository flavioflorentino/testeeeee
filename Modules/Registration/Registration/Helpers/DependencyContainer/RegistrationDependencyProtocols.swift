protocol HasLegacy {
    var legacy: RegistrationSetupContract { get }
}
