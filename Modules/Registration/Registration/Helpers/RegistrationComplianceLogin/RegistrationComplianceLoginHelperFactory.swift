import Foundation

public enum RegistrationComplianceLoginHelperFactory {
    public static func make() -> RegistrationComplianceLoginHelperContract {
        let container = DependencyContainer()
        let service: RegistrationComplianceLoginServicing = RegistrationComplianceLoginService(dependencies: container)
        let helper: RegistrationComplianceLoginHelperContract = RegistrationComplianceLoginHelper(service: service)
        return helper
    }
}
