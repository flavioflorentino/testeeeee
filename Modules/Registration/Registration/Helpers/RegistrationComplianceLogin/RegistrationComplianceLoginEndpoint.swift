import Core
import Foundation

enum RegistrationComplianceLoginEndpoint {
    case loginUserWithRestriction(login: String, password: String)
}

extension RegistrationComplianceLoginEndpoint: ApiEndpointExposable {
    var path: String {
        "registration-validation/login"
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var body: Data? {
        guard case let .loginUserWithRestriction(login, password) = self else {
            return nil
        }
        
        return ["login": login, "password": password].toData()
    }
}
