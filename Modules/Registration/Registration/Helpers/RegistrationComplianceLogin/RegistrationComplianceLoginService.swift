import Core
import Foundation

protocol RegistrationComplianceLoginServicing {
    func loginUserWithRestriction(
        login: String,
        password: String,
        completion: @escaping(Result<RegistrationComplianceLoginModel, ApiError>) -> Void
    )
}
final class RegistrationComplianceLoginService {
    typealias Dependencies = HasMainQueue
    let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension RegistrationComplianceLoginService: RegistrationComplianceLoginServicing {
    func loginUserWithRestriction(
        login: String,
        password: String,
        completion: @escaping (Result<RegistrationComplianceLoginModel, ApiError>) -> Void
    ) {
        let endpoint = RegistrationComplianceLoginEndpoint.loginUserWithRestriction(login: login, password: password)
        Api<RegistrationComplianceLoginModel>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
