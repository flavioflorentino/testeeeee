import Foundation

public struct RegistrationComplianceLoginModel: Decodable {
    public let step: RegistrationComplianceResult
    public let token: RegistrationComplianceToken
    public let consumer: RegistrationComplianceConsumer?
    
    public init(step: RegistrationComplianceResult, token: RegistrationComplianceToken, consumer: RegistrationComplianceConsumer?) {
        self.step = step
        self.token = token
        self.consumer = consumer
    }
}

public struct RegistrationComplianceToken: Decodable {
    public let code: String
    public let type: RegistrationComplianceTokenType
    
    public init(code: String, type: RegistrationComplianceTokenType) {
        self.code = code
        self.type = type
    }
}

public struct RegistrationComplianceConsumer: Decodable {
    public let name: String
    public let cpf: String
    public let birthdate: String
    
    public init(name: String, cpf: String, birthdate: String) {
        self.name = name
        self.cpf = cpf
        self.birthdate = birthdate
    }
}
