import Foundation

class MockDecodable<T: Decodable> {
    private lazy var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
    
        return decoder
    }()

    func loadCodableObject(resource: String, typeDecoder: JSONDecoder.KeyDecodingStrategy = .convertFromSnakeCase) throws -> T {
        decoder.keyDecodingStrategy = typeDecoder
        let bundle = Bundle(for: type(of: self))
        let path = bundle.path(forResource: resource, ofType: "json")!
        let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
    
        return try decoder.decode(T.self, from: data)
    }
}
