import Foundation
import OHHTTPStubs
@testable import PCI

class StubResponse {
    private let headers = ["Content-Type": "application/json"]
    
    func response(at path: String, status: Code) -> OHHTTPStubsResponse {
        let file = OHPathForFile(path, type(of: self))!
        return OHHTTPStubsResponse(fileAtPath: file, statusCode: status.rawValue, headers: headers)
    }
}

extension StubResponse {
    enum Code: Int32 {
        case success = 200
        case error = 400
    }
}
