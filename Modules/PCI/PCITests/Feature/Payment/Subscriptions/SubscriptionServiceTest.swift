import XCTest
import OHHTTPStubs
@testable import PCI

class SubscriptionServiceTest: XCTestCase {
    private var service: SubscriptionServiceProtocol?
    
    private func createSubscriptionPayload() -> PaymentPayload<SubscribePayload> {
        let model = try! MockDecodable<SubscribePayload>().loadCodableObject(resource: "SubscribePayload", typeDecoder: .useDefaultKeys)
        return PaymentPayload(cvv: nil, generic: model)
    }
    
    private func createSubscriptionPaymentPayload() -> PaymentPayload<SubscriptionPaymentPayload> {
        let model = try! MockDecodable<SubscriptionPaymentPayload>().loadCodableObject(resource: "SubscriptionPaymentPayload", typeDecoder: .useDefaultKeys)
        return PaymentPayload(cvv: nil, generic: model)
    }
    
    override func setUp() {
        super.setUp()
        service = SubscriptionService()
    }
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testSubscribeSuccess() {
        let expectation = XCTestExpectation(description: "Test Subscribe Success")
        let payload = createSubscriptionPayload()
        
        stub(condition: isPath("/payment/membership/subscription/9a2b12")) { _ in
            return StubResponse().response(at: "SubscribeSuccess.json", status: .success)
        }
        
        service?.subscribe(password: "1234", planId: "9a2b12", payload: payload, isNewArchitecture: true) { (result) in
            switch result {
            case .success(let value):
                XCTAssertNotNil(value)
                expectation.fulfill()
                
            case .failure:
                XCTFail()
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testSubscribeFailure() {
        let expectation = XCTestExpectation(description: "Test Subscribe Failure")
        let payload = createSubscriptionPayload()
        
        stub(condition: isPath("/payment/membership/subscription/9a2b12")) { _ in
            return StubResponse().response(at: "apiNewError.json", status: .error)
        }
        
        service?.subscribe(password: "1234", planId: "9a2b12", payload: payload, isNewArchitecture: true) { (result) in
            switch result {
            case .success:
                XCTFail()
                
            case .failure(let error):
                XCTAssertNotNil(error)
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testSubscriptionPaySuccess() {
        let expectation = XCTestExpectation(description: "Test Subscribe Payment Success")
        let payload = createSubscriptionPaymentPayload()
        
        stub(condition: isPath("/payment/membership/payment/9a2b12")) { _ in
            return StubResponse().response(at: "SubscribeSuccess.json", status: .success)
        }
        
        service?.subscriptionPay(password: "1234", subscriptionId: "9a2b12", payload: payload, isNewArchitecture: false) { result in
            switch result {
            case .success(let value):
                XCTAssertNotNil(value)
                expectation.fulfill()
                
            case .failure:
                XCTFail()
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testSubscriptionPayFailure() {
        let expectation = XCTestExpectation(description: "Test Subscribe Payment Failure")
        let payload = createSubscriptionPaymentPayload()
        
        stub(condition: isPath("/payment/membership/payment/9a2b12")) { _ in
            return StubResponse().response(at: "apiNewError.json", status: .error)
        }
        
        service?.subscriptionPay(password: "1234", subscriptionId: "9a2b12", payload: payload, isNewArchitecture: false) { result in
            switch result {
            case .success:
                XCTFail()
                
            case .failure(let error):
                XCTAssertNotNil(error)
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
}
