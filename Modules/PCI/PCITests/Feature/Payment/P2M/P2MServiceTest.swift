import XCTest
import OHHTTPStubs
@testable import PCI

class P2MServiceTest: XCTestCase {
    private var service: P2MServiceProtocol?
    
    private func createP2PPayload() -> PaymentPayload<P2MPayload> {
        let model = try! MockDecodable<P2MPayload>().loadCodableObject(resource: "P2MPayload", typeDecoder: .useDefaultKeys)
        return PaymentPayload(cvv: nil, generic: model)
    }
    
    override func setUp() {
        super.setUp()
        service = P2MService()
    }
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func testCreateTransactionSuccess() {
        let expectation = XCTestExpectation(description: "Test Payment P2M Success")
        let payload = createP2PPayload()
        
        stub(condition: isPath("/payment/p2m")) { _ in
            return StubResponse().response(at: "P2MSuccess.json", status: .success)
        }
        
        service?.createTransaction(password: "1234", payload: payload, isNewArchitecture: true) { result in
            switch result {
             case .success(let value):
                XCTAssertNotNil(value)
                expectation.fulfill()
                
             case .failure:
                XCTFail()
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testCreateTransactionFailure() {
        let expectation = XCTestExpectation(description: "Test Payment P2M Failure")
        let payload = createP2PPayload()
           
        stub(condition: isPath("/payment/p2m")) { _ in
           return StubResponse().response(at: "apiNewError.json", status: .error)
        }
        
        service?.createTransaction(password: "1234", payload: payload, isNewArchitecture: true) { result in
            switch result {
             case .success:
                 XCTFail()

             case .failure(let error):
                 XCTAssertNotNil(error)
                 expectation.fulfill()
            }
        }
           
        wait(for: [expectation], timeout: 5.0)
    }
}
