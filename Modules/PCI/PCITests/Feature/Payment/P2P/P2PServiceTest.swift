import XCTest
import OHHTTPStubs
@testable import PCI

final class P2PServiceTest: XCTestCase {
    private lazy var service: P2PServiceProtocol = P2PService()
    
    private lazy var payload: PaymentPayload<P2PPayload>? = {
        let mock = MockDecodable<P2PPayload>()
        guard let model = try? mock.loadCodableObject(resource: "P2PPayload", typeDecoder: .useDefaultKeys) else {
            return nil
        }
        return PaymentPayload(cvv: nil, generic: model)
    }()
    
    private let timeout: Double = 0.5
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testCreateTransactionSuccess() throws {
        let expectation = XCTestExpectation(description: "Test Payment P2P Success")
        let payload = try XCTUnwrap(self.payload)
        
        stub(condition: isPath("/payment/p2p")) { _ in
            return StubResponse().response(at: "P2PSuccess.json", status: .success)
        }
        
        service.createTransaction(password: "1234", isFeedzai: false, payload: payload, isNewArchitecture: true) { result in
            switch result {
             case .success(let value):
                XCTAssertNotNil(value)
                expectation.fulfill()
                
             case .failure:
                XCTFail()
            }
        }
        
        wait(for: [expectation], timeout: timeout)
    }
    
    func testCreateTransactionFailure() throws {
        let expectation = XCTestExpectation(description: "Test Payment P2P Failure")
        let payload = try XCTUnwrap(self.payload)
           
        stub(condition: isPath("/payment/p2p")) { _ in
           return StubResponse().response(at: "apiNewError.json", status: .error)
        }
        
        service.createTransaction(password: "1234", isFeedzai: false, payload: payload, isNewArchitecture: true) { result in
            switch result {
             case .success:
                 XCTFail()

             case .failure(let error):
                 XCTAssertNotNil(error)
                 expectation.fulfill()
            }
        }
           
        wait(for: [expectation], timeout: timeout)
    }
    
    func testCreateTransaction_WhenFlagTransactionP2PV2FeedzaiIsTrue_ShouldAllowTransaction() throws {
        let expectation = XCTestExpectation(description: "Test Payment P2P Success")
        let payload = try XCTUnwrap(self.payload)
        
        stub(condition: hasHeaderNamed("risk_engine_v2")) { _ in
            return StubResponse().response(at: "P2PSuccess.json", status: .success)
        }

        service.createTransaction(password: "1234", isFeedzai: true, payload: payload, isNewArchitecture: false) { result in
            switch result {
                case let .success(value):
                    XCTAssertNotNil(value)
                    expectation.fulfill()
                case .failure:
                    XCTFail()
            }
        }
        
        wait(for: [expectation], timeout: timeout)
    }
}
