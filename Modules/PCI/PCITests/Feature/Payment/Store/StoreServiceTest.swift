import XCTest
import OHHTTPStubs
@testable import PCI

class StoreServiceTest: XCTestCase {
    private var service: StoreServiceProtocol?
    
    override func setUp() {
        super.setUp()
        service = StoreService()
    }
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testCreateUserBalanceTransactionSuccess() {
        let payload = createStorePayload(cvv: nil, json: "StoreUserBalancePayload")
        createTransactionSuccess(for: payload)
    }
    
    func testCreateCreditCardTransactionSuccess() {
        let payload = createStorePayload(cvv: "123", json: "StoreCreditCardPayload")
        createTransactionSuccess(for: payload)
    }
    
    func testCreateTransactionFailure() {
        let expectation = XCTestExpectation(description: "Test Payment Store Failure")
        let payload = createStorePayload(cvv: nil, json: "StoreUserBalancePayload")
           
        stub(condition: isPath("/payment/store")) { _ in
           return StubResponse().response(at: "apiNewError.json", status: .error)
        }
        
        service?.createTransaction(password: "1234", payload: payload, isNewArchitecture: true) { result in
            switch result {
            case .success:
                XCTFail()
            case .failure(let error):
                XCTAssertNotNil(error)
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    private func createStorePayload(cvv: String?, json: String) -> PaymentPayload<StoreRequestPayload> {
        let cvvPayload = CVVPayload(value: cvv)
        let model = try! MockDecodable<StoreRequestPayload>().loadCodableObject(resource: json, typeDecoder: .convertFromSnakeCase)
        return PaymentPayload<StoreRequestPayload>(cvv: cvvPayload, generic: model)
    }
    
    private func createTransactionSuccess(for payload: PaymentPayload<StoreRequestPayload>) {
        let expectation = XCTestExpectation(description: "Test Payment Store Success")
        stub(condition: isPath("/payment/store")) { _ in
            return StubResponse().response(at: "StoreSuccess.json", status: .success)
        }
        service?.createTransaction(password: "1234", payload: payload, isNewArchitecture: false) { result in
            switch result {
            case .success(let value):
                XCTAssertNotNil(value)
                expectation.fulfill()
            case .failure:
                XCTFail()
            }
        }
        wait(for: [expectation], timeout: 5.0)
    }
}
