import XCTest
import OHHTTPStubs
@testable import PCI

class EcommerceServiceTest: XCTestCase {
    private var service: EcommercePciServicing?
    
    private func createEcommercePayload() -> PaymentPayload<EcommercePayload> {
        let model = try! MockDecodable<EcommercePayload>().loadCodableObject(resource: "EcommercePayload", typeDecoder: .useDefaultKeys)
        return PaymentPayload(cvv: nil, generic: model)
    }
    
    override func setUp() {
        super.setUp()
        service = EcommercePciService()
    }
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testCreateTransactionSuccess() {
        let expectation = XCTestExpectation(description: "Test Ecommerce Payment Success")
        let payload = createEcommercePayload()
        
        stub(condition: isPath("/payment/ecommerce/200")) { _ in
            return StubResponse().response(at: "EcommerceSuccess.json", status: .success)
        }
        
        service?.createTransaction(password: "1234", orderId: "200", payload: payload, isNewArchitecture: false) { result in
            switch result {
            case .success(let value):
                XCTAssertNotNil(value)
                expectation.fulfill()
                
            case .failure:
                XCTFail()
            }
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testCreateTransactionFailure() {
        let expectation = XCTestExpectation(description: "Test Ecommerce Payment Failure")
        let payload = createEcommercePayload()
           
        stub(condition: isPath("/payment/ecommerce/200")) { _ in
           return StubResponse().response(at: "apiNewError.json", status: .error)
        }
        
        service?.createTransaction(password: "1234", orderId: "200", payload: payload, isNewArchitecture: false) { result in
            switch result {
            case .success:
                XCTFail()

            case .failure(let error):
                XCTAssertNotNil(error)
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 5.0)
    }
}
