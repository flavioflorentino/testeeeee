import XCTest
import OHHTTPStubs
@testable import PCI

class TransitPassServiceTest: XCTestCase {
    private var service: TransitPassServiceProtocol?
    
    private func createTransitPassPayload() -> PaymentPayload<DigitalGoodsPayload<TransitPass>> {
        let model = try! MockDecodable<DigitalGoodsPayload<TransitPass>>().loadCodableObject(resource: "TransitPassPayload", typeDecoder: .useDefaultKeys)
        return PaymentPayload<DigitalGoodsPayload<TransitPass>>(cvv: nil, generic: model)
    }
    
    override func setUp() {
        super.setUp()
        service = TransitPassService()
    }
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testCreateTransactionSuccess() {
        let expectation = XCTestExpectation(description: "Test Payment Transport Success")
        let payload = createTransitPassPayload()
        
        stub(condition: isPath("/payment/transport")) { _ in
            return StubResponse().response(at: "TransitPassSuccess.json", status: .success)
        }
        
        service?.createTransaction(password: "1234", payload: payload, isNewArchitecture: false) { result in
            switch result {
             case .success(let value):
                XCTAssertNotNil(value)
                expectation.fulfill()
                
             case .failure:
                XCTFail()
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testCreateTransactionFailure() {
        let expectation = XCTestExpectation(description: "Test Payment Transport Failure")
        let payload = createTransitPassPayload()
           
        stub(condition: isPath("/payment/transport")) { _ in
           return StubResponse().response(at: "apiNewError.json", status: .error)
        }
        
        service?.createTransaction(password: "1234", payload: payload, isNewArchitecture: true) { result in
            switch result {
             case .success:
                 XCTFail()

             case .failure(let error):
                 XCTAssertNotNil(error)
                 expectation.fulfill()
            }
        }
           
        wait(for: [expectation], timeout: 5.0)
    }
}
