import XCTest
import OHHTTPStubs
@testable import PCI

class BillServiceTest: XCTestCase {
    private var service: PaymentBillServiceProtocol?
    
    private func createBillPayload() -> PaymentPayload<BillPayload> {
        let model = try! MockDecodable<BillPayload>().loadCodableObject(resource: "BillPayload", typeDecoder: .useDefaultKeys)
        return PaymentPayload<BillPayload>(cvv: nil, generic: model)
    }
    
    override func setUp() {
        super.setUp()
        service = PaymentBillService()
    }
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testPaySuccess() {
        let expectation = XCTestExpectation(description: "Test Payment Bill Succes")
        let payload = createBillPayload()
        
        stub(condition: isPath("/payment/bills")) { _ in
            return StubResponse().response(at: "BillSuccess.json", status: .success)
        }
        
        service?.pay(password: "1234", payload: payload, isNewArchitecture: false) { result in
            switch result {
            case .success(let value):
                XCTAssertNotNil(value)
                expectation.fulfill()
                
            case .failure:
                XCTFail()
            }
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testPayFailure() {
        let expectation = XCTestExpectation(description: "Test Payment Bill Failure")
        let payload = createBillPayload()
           
        stub(condition: isPath("/payment/bills")) { _ in
           return StubResponse().response(at: "apiOldError.json", status: .error)
        }
        
        service?.pay(password: "1234", payload: payload, isNewArchitecture: true) { result in
            switch result {
            case .success:
                XCTFail()

            case .failure(let error):
                XCTAssertNotNil(error)
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
}
