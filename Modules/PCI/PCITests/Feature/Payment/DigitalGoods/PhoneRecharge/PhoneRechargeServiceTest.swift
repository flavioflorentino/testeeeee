import XCTest
import OHHTTPStubs
@testable import PCI

class PhoneRechargeServiceTest: XCTestCase {
    private var service: PhoneRechargeServiceProtocol?
    
    private func createPhoneRechargePayload() -> PaymentPayload<DigitalGoodsPayload<PhoneRecharge>> {
        let model = try! MockDecodable<DigitalGoodsPayload<PhoneRecharge>>().loadCodableObject(resource: "PhoneRechargePayload", typeDecoder: .useDefaultKeys)
        return PaymentPayload<DigitalGoodsPayload<PhoneRecharge>>(cvv: nil, generic: model)
    }
    
    override func setUp() {
        super.setUp()
        service = PhoneRechargeService()
    }
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testCreateTransactionSuccess() {
        let expectation = XCTestExpectation(description: "Test Payment PAV Success")
        let payload = createPhoneRechargePayload()
        
        stub(condition: isPath("/payment/pav")) { _ in
            return StubResponse().response(at: "PhoneRechargeSuccess.json", status: .success)
        }
        
        service?.createTransaction(password: "1234", payload: payload, isNewArchitecture: false) { result in
            switch result {
            case .success(let value):
                XCTAssertNotNil(value)
                expectation.fulfill()
                
            case .failure:
                XCTFail()
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testCreateTransactionFailure() {
        let expectation = XCTestExpectation(description: "Test Payment PAV Failure")
        let payload = createPhoneRechargePayload()
           
        stub(condition: isPath("/payment/pav")) { _ in
           return StubResponse().response(at: "apiNewError.json", status: .error)
        }
        
        service?.createTransaction(password: "1234", payload: payload, isNewArchitecture: false) { result in
            switch result {
            case .success:
                XCTFail()

            case .failure(let error):
                XCTAssertNotNil(error)
                expectation.fulfill()
            }
        }
           
        wait(for: [expectation], timeout: 5.0)
    }
}
