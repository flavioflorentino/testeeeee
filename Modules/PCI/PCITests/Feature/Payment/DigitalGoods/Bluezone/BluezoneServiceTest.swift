import XCTest
import OHHTTPStubs
@testable import PCI

class BluezoneServiceTest: XCTestCase {
    private var service: BluezoneServiceProtocol?
    
    private func createBluezonePayload() -> PaymentPayload<DigitalGoodsPayload<Bluezone>> {
        let model = try! MockDecodable<DigitalGoodsPayload<Bluezone>>().loadCodableObject(resource: "BluezonePayload", typeDecoder: .useDefaultKeys)
        return PaymentPayload<DigitalGoodsPayload<Bluezone>>(cvv: nil, generic: model)
    }
    
    override func setUp() {
        super.setUp()
        service = BluezoneService()
    }
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testMakeTransactionSuccess() {
        let expectation = XCTestExpectation(description: "Test Payment Bluezone Success")
        let payload = createBluezonePayload()
        
        stub(condition: isPath("/payment/bluezone")) { _ in
            return StubResponse().response(at: "BluezoneSuccess.json", status: .success)
        }
        
        service?.makeTransaction(password: "1234", payload: payload, isNewArchitecture: false) { result in
            switch result {
             case .success(let value):
                XCTAssertNotNil(value)
                expectation.fulfill()
                
             case .failure:
                XCTFail()
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testMakeTransactionFailure() {
        let expectation = XCTestExpectation(description: "Test Payment Bluezone Failure")
        let payload = createBluezonePayload()
           
        stub(condition: isPath("/payment/bluezone")) { _ in
           return StubResponse().response(at: "apiNewError.json", status: .error)
        }
        
        service?.makeTransaction(password: "1234", payload: payload, isNewArchitecture: true) { result in
            switch result {
             case .success:
                 XCTFail()

             case .failure(let error):
                 XCTAssertNotNil(error)
                 expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
}
