import XCTest
import OHHTTPStubs
@testable import PCI

class ParkingServiceTest: XCTestCase {
    private var service: ParkingServiceProtocol?
    
    private func createParkingPayload() -> PaymentPayload<DigitalGoodsPayload<Parking>> {
        let model = try! MockDecodable<DigitalGoodsPayload<Parking>>().loadCodableObject(resource: "ParkingPayload", typeDecoder: .useDefaultKeys)
        return PaymentPayload<DigitalGoodsPayload<Parking>>(cvv: nil, generic: model)
    }
    
    override func setUp() {
        super.setUp()
        service = ParkingService()
    }
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testCreateTransactionSuccess() {
        let expectation = XCTestExpectation(description: "Test Payment Parking Success")
        let payload = createParkingPayload()
        
        stub(condition: isPath("/payment/parking")) { _ in
            return StubResponse().response(at: "ParkingSuccess.json", status: .success)
        }
        
        service?.createTransaction(password: "1234", payload: payload, isNewArchitecture: true) { result in
            switch result {
            case .success(let value):
                XCTAssertNotNil(value)
                expectation.fulfill()
                
            case .failure:
                XCTFail()
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testCreateTransactionFailure() {
        let expectation = XCTestExpectation(description: "Test Payment Parking Failure")
        let payload = createParkingPayload()
           
        stub(condition: isPath("/payment/parking")) { _ in
            return StubResponse().response(at: "apiNewError.json", status: .error)
        }
        
        service?.createTransaction(password: "1234", payload: payload, isNewArchitecture: true) { result in
            switch result {
            case .success:
                XCTFail()

            case .failure(let error):
                XCTAssertNotNil(error)
                expectation.fulfill()
            }
        }
           
        wait(for: [expectation], timeout: 5.0)
    }
}
