import XCTest
import OHHTTPStubs
@testable import PCI

class TvRechargeServiceTest: XCTestCase {
    private var service: TvRechargeServiceProtocol?
    
    private func createTvRechargePayload() -> PaymentPayload<DigitalGoodsPayload<TvRecharge>> {
        let model = try! MockDecodable<DigitalGoodsPayload<TvRecharge>>().loadCodableObject(resource: "TvRechargePayload", typeDecoder: .useDefaultKeys)
        return PaymentPayload<DigitalGoodsPayload<TvRecharge>>(cvv: nil, generic: model)
    }
    
    override func setUp() {
        super.setUp()
        service = TvRechargeService()
    }
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testCreateTransactionSuccess() {
        let expectation = XCTestExpectation(description: "Test Payment PAV Success")
        let payload = createTvRechargePayload()
        
        stub(condition: isPath("/payment/pav")) { _ in
            return StubResponse().response(at: "TvRechargeSuccess.json", status: .success)
        }
        
        service?.createTransaction(password: "1234", payload: payload, isNewArchitecture: true) { result in
            switch result {
             case .success(let value):
                XCTAssertNotNil(value)
                expectation.fulfill()
                
             case .failure:
                XCTFail()
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testCreateTransactionFailure() {
        let expectation = XCTestExpectation(description: "Test Payment PAV Failure")
        let payload = createTvRechargePayload()
           
        stub(condition: isPath("/payment/pav")) { _ in
           return StubResponse().response(at: "apiNewError.json", status: .error)
        }
        
        service?.createTransaction(password: "1234", payload: payload, isNewArchitecture: true) { result in
            switch result {
             case .success:
                 XCTFail()

             case .failure(let error):
                 XCTAssertNotNil(error)
                 expectation.fulfill()
            }
        }
           
        wait(for: [expectation], timeout: 5.0)
    }
}
