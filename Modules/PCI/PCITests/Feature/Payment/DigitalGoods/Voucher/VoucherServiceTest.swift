import XCTest
import OHHTTPStubs
@testable import PCI

class VoucherServiceTest: XCTestCase {
    private var service: VoucherServiceProtocol?
    
    private func createVoucherPayload() -> PaymentPayload<DigitalGoodsPayload<Voucher>> {
        let model = try! MockDecodable<DigitalGoodsPayload<Voucher>>().loadCodableObject(resource: "VoucherPayload", typeDecoder: .useDefaultKeys)
        return PaymentPayload<DigitalGoodsPayload<Voucher>>(cvv: nil, generic: model)
    }
    
    override func setUp() {
        super.setUp()
        service = VoucherService()
    }
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testCreateTransactionSuccess() {
        let expectation = XCTestExpectation(description: "Test Payment PAV Success")
        let payload = createVoucherPayload()
        
        stub(condition: isPath("/payment/pav")) { _ in
            return StubResponse().response(at: "VoucherSuccess.json", status: .success)
        }
        
        service?.createTransaction(password: "1234", payload: payload, isNewArchitecture: false) { result in
            switch result {
            case .success(let value):
                XCTAssertNotNil(value)
                expectation.fulfill()
                
            case .failure:
                XCTFail()
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testCreateTransactionFailure() {
        let expectation = XCTestExpectation(description: "Test Payment PAV Failure")
        let payload = createVoucherPayload()
           
        stub(condition: isPath("/payment/pav")) { _ in
           return StubResponse().response(at: "apiNewError.json", status: .error)
        }
        
        service?.createTransaction(password: "1234", payload: payload, isNewArchitecture: true) { result in
            switch result {
            case .success:
                XCTFail()

            case .failure(let error):
                XCTAssertNotNil(error)
                expectation.fulfill()
            }
        }
           
        wait(for: [expectation], timeout: 5.0)
    }
}
