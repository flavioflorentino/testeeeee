import XCTest
import OHHTTPStubs
@testable import PCI

final class PAVServiceTest: XCTestCase {
    private var service: PAVServiceProtocol = PAVService()
    
    private lazy var payload: PaymentPayload<PAVPayload>? = {
        let mock = MockDecodable<PAVPayload>()
        guard let model = try? mock.loadCodableObject(resource: "PAVPayload", typeDecoder: .useDefaultKeys) else {
            return nil
        }
        return PaymentPayload(cvv: nil, generic: model)
    }()
    
    private let timeout: Double = 0.5
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testCreateTransactionSuccess() throws {
        let expectation = XCTestExpectation(description: "Test Payment Pav Success")
        let payload = try XCTUnwrap(self.payload)
        
        stub(condition: isPath("/payment/pav")) { _ in
            return StubResponse().response(at: "PAVSuccess.json", status: .success)
        }
        
        service.createTransaction(password: "1234", isFeedzai: false, payload: payload, isNewArchitecture: false) { result in
            switch result {
             case .success(let value):
                XCTAssertNotNil(value)
                expectation.fulfill()
                
             case .failure:
                XCTFail()
            }
        }
        
        wait(for: [expectation], timeout: timeout)
    }
    
    func testCreateTransactionFailure() throws {
        let expectation = XCTestExpectation(description: "Test Payment Pav Failure")
        let payload = try XCTUnwrap(self.payload)
           
        stub(condition: isPath("/payment/pav")) { _ in
           return StubResponse().response(at: "apiOldError.json", status: .error)
        }
        
        service.createTransaction(password: "1234", isFeedzai: false, payload: payload, isNewArchitecture: true) { result in
            switch result {
             case .success:
                 XCTFail()

             case .failure(let error):
                 XCTAssertNotNil(error)
                 expectation.fulfill()
            }
        }
           
        wait(for: [expectation], timeout: timeout)
    }
    
    func testCreateTransaction_WhenFlagTransactionPAVV2FeedzaiIsTrue_ShouldBlockTransaction() throws {
        let expectation = XCTestExpectation(description: "Test Payment PAV Success")
        let payload = try XCTUnwrap(self.payload)
        
        stub(condition: hasHeaderNamed("risk_engine_v2")) { _ in
            return StubResponse().response(at: "P2PSuccess.json", status: .success)
        }
        
        service.createTransaction(password: "1234", isFeedzai: true, payload: payload, isNewArchitecture: false) { result in
            switch result {
            case let .success(value):
                XCTAssertNotNil(value)
                expectation.fulfill()
            case .failure:
                XCTFail()
            }
            
        }
        wait(for: [expectation], timeout: timeout)
    }
}
