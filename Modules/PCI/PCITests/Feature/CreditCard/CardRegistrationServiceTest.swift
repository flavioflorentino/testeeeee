import XCTest
import OHHTTPStubs
@testable import PCI

class CardRegistrationServiceTest: XCTestCase {
    private var service: CardRegistrationServicing?
    
    private func createCardPayload(withAddress: Bool) -> CardPayload {
        let resource = withAddress ? "cardWithoutAddress" : "cardWithAddress"
        return try! MockDecodable<CardPayload>().loadCodableObject(resource: resource, typeDecoder: .useDefaultKeys)
    }
    
    override func setUp() {
        super.setUp()
        service = CardRegistrationService()
    }
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testValidateCreditCardSuccess() {
        let expectation = XCTestExpectation(description: "Test Validate Success")
        let card = createCardPayload(withAddress: false)
        
        stub(condition: isPath("/cards/validation")) { _ in
            return StubResponse().response(at: "validateCreditCard.json", status: .success)
        }

        service?.validate(card: card) { result in
            switch result {
            case .success(let value):
                XCTAssertNotNil(value)
                expectation.fulfill()

            case .failure:
                XCTFail()
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testValidateCreditCardFailure() {
        let expectation = XCTestExpectation(description: "Test Validate Failure")
        let card = createCardPayload(withAddress: false)
        
        stub(condition: isPath("/cards/validation")) { _ in
            return StubResponse().response(at: "apiOldError.json", status: .error)
        }

        service?.validate(card: card) { result in
            switch result {
            case .success:
                XCTFail()

            case .failure(let error):
                XCTAssertNotNil(error)
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testSaveCreditCardSuccess() {
        let expectation = XCTestExpectation(description: "Test Insert Success")
        let card = createCardPayload(withAddress: true)
        
        stub(condition: isPath("/cards")) { _ in
            return StubResponse().response(at: "insertCreditCard.json", status: .success)
        }

        service?.save(card: card) { result in
            switch result {
                case .success(let value):
                   XCTAssertNotNil(value)
                   expectation.fulfill()

                case .failure:
                   XCTFail()
                }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testSaveCreditCardFailure() {
        let expectation = XCTestExpectation(description: "Test Insert Failure")
        let card = createCardPayload(withAddress: true)
        
        stub(condition: isPath("/cards")) { _ in
            return StubResponse().response(at: "apiOldError.json", status: .error)
        }

        service?.save(card: card) { result in
            switch result {
            case .success:
                XCTFail()

            case .failure(let error):
                XCTAssertNotNil(error)
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
}
