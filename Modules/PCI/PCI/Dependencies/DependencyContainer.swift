import Core

protocol HasNoDependency {}

typealias AppDependencies =
    HasNoDependency &
    HasKeychainManager

final class DependencyContainer: AppDependencies {
    lazy var keychain: KeychainManagerContract = KeychainManager()
}
