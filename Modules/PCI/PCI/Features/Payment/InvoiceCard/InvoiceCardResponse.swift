import Core
import Foundation

public struct InvoiceCardResponse: Decodable {
    public let statusName: String
    public let receipt: [[String: Any]]
    public let transaction: [String: Any]

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        statusName = try container.decode(String.self, forKey: .statusName)
        transaction = try container.decode([String: Any].self, forKey: .transaction)
        
        guard let widgets = try? container.decode([Any].self, forKey: .receipt) as? [[String: Any]] else {
            throw NSError(domain: "picpay.com.api", code: 1)
        }
        
        receipt = widgets
    }

    private enum CodingKeys: String, CodingKey {
        case statusName = "status_name"
        case receipt
        case transaction = "Transaction"
    }
}
