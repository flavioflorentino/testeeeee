import Core
import Foundation

public protocol InvoiceCardServiceProtocol {
    func createTransaction(
        password: String,
        invoiceId: String,
        payload: PaymentPayload<InvoiceCardPayload>,
        isNewArchitecture: Bool,
        completion: @escaping(Result<InvoiceCardResponse, ApiError>) -> Void
    )
}

public class InvoiceCardService: InvoiceCardServiceProtocol {
    public init() { }

    public func createTransaction(
        password: String,
        invoiceId: String,
        payload: PaymentPayload<InvoiceCardPayload>,
        isNewArchitecture: Bool,
        completion: @escaping(Result<InvoiceCardResponse, ApiError>) -> Void
    ) {
        let api = Api<InvoiceCardResponse>(
            endpoint: PaymentEndpoint.invoiceCard(payload,
                                                  invoiceId: invoiceId,
                                                  password: password,
                                                  isNewArchitecture: isNewArchitecture)
        )
        api.execute { result in
            completion(result.map(\.model))
        }
    }
}
