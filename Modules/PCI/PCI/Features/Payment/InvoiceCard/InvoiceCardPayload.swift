import Foundation

public struct InvoiceCardPayload: Codable {
    let origin: String
    let biometry: Bool
    let credit: String
    let value: String
    let feedVisibility: String
    let cardId: Int?
    let installments: Int
    
    public init(
        origin: String,
        biometry: Bool,
        credit: String,
        value: String,
        feedVisibility: String,
        cardId: Int,
        installments: Int
    ) {
        self.origin = origin
        self.biometry = biometry
        self.credit = credit
        self.value = value
        self.feedVisibility = feedVisibility
        self.cardId = cardId
        self.installments = installments
    }
}

extension InvoiceCardPayload {
    private enum CodingKeys: String, CodingKey {
        case origin
        case biometry
        case credit
        case value
        case feedVisibility = "feed_visibility"
        case cardId = "consumer_credit_card_id"
        case installments = "nparcelas"
    }
}
