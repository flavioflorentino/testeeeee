import Foundation

public struct PAVResponse: Decodable {
    public let json: [String: Any]

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        json = try container.decode([String: Any].self, forKey: .data)
    }

    private enum CodingKeys: String, CodingKey {
        case data
    }
}
