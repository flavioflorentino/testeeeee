import Core
import Foundation

public protocol PAVServiceProtocol {
    typealias PAVTransactionCompletion = (Result<PAVResponse, ApiError>) -> Void
    func createTransaction(
        password: String,
        isFeedzai: Bool,
        payload: PaymentPayload<PAVPayload>,
        isNewArchitecture: Bool,
        completion: @escaping PAVTransactionCompletion
    )
}

public class PAVService: PAVServiceProtocol {
    public init() { }

    public func createTransaction(
        password: String,
        isFeedzai: Bool,
        payload: PaymentPayload<PAVPayload>,
        isNewArchitecture: Bool,
        completion: @escaping PAVTransactionCompletion
    ) {
        let api = Api<PAVResponse>(endpoint: PaymentEndpoint.pav(payload,
                                                                 password: password,
                                                                 isFeedzai: isFeedzai,
                                                                 isNewArchitecture: isNewArchitecture))
        api.execute { result in
            let mappedResult = result
                .map { $0.model }
            completion(mappedResult)
        }
    }
}
