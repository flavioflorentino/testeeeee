import Core
import Foundation

public struct PAVPayload: Codable {
    let origin: String
    let password: String
    let biometry: String
    let sellerId: String
    let planType: String
    let ignoreBalance: Bool
    let value: String
    let credit: String
    let shippingId: String
    let shipping: String
    let cardId: String?
    let addressId: String
    let installments: String
    let items: [PAVItems]
    let surcharge: String
    let fromExplore: String
    let someErrorOccurred: String
    let latitude: String
    let longitude: String
    let gpsAcc: String
    let licensePlate: String
    let privacyConfig: String
    let additionalInfo: AnyCodable?

    public init(
        origin: String = "",
        password: String,
        biometry: String,
        sellerId: String,
        planType: String,
        ignoreBalance: Bool,
        value: String,
        credit: String,
        shippingId: String = "",
        shipping: String = "0",
        cardId: String,
        addressId: String = "",
        installments: String,
        items: [PAVItems],
        surcharge: String = "",
        fromExplore: String = "0",
        someErrorOccurred: String,
        latitude: String,
        longitude: String,
        gpsAcc: String,
        licensePlate: String = "",
        privacyConfig: String,
        additionalInfo: AnyCodable
    ) {
        self.origin = origin
        self.password = password
        self.biometry = biometry
        self.sellerId = sellerId
        self.planType = planType
        self.ignoreBalance = ignoreBalance
        self.value = value
        self.credit = credit
        self.shippingId = shippingId
        self.shipping = shipping
        self.cardId = cardId
        self.addressId = addressId
        self.installments = installments
        self.items = items
        self.surcharge = surcharge
        self.fromExplore = fromExplore
        self.someErrorOccurred = someErrorOccurred
        self.latitude = latitude
        self.longitude = longitude
        self.gpsAcc = gpsAcc
        self.licensePlate = licensePlate
        self.privacyConfig = privacyConfig
        self.additionalInfo = additionalInfo
    }
    
    private enum CodingKeys: String, CodingKey {
        case origin
        case password = "pin"
        case biometry
        case sellerId = "seller_id"
        case planType = "plan_type"
        case ignoreBalance = "ignore_balance"
        case value = "total"
        case credit
        case shippingId = "shipping_id"
        case shipping
        case cardId = "credit_card_id"
        case addressId = "address_id"
        case installments = "parcelas"
        case items = "itens"
        case surcharge
        case fromExplore = "from_explore"
        case someErrorOccurred = "duplicated"
        case latitude = "lat"
        case longitude = "lng"
        case gpsAcc = "gps_acc"
        case licensePlate = "license_plate"
        case privacyConfig = "feed_visibility"
        case additionalInfo = "additional_info"
    }
}

public struct PAVItems: Codable {
    let pavId: String
    let amount: String

    public init(pavId: String, amount: String) {
        self.pavId = pavId
        self.amount = amount
    }

    private enum CodingKeys: String, CodingKey {
        case pavId = "id"
        case amount
    }
}
