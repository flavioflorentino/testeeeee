import Core
import Foundation

public protocol EcommercePciServicing {
    typealias EcommerceCompletion = (Result<EcommerceResponse, ApiError>) -> Void
    func createTransaction(
        password: String,
        orderId: String,
        payload: PaymentPayload<EcommercePayload>,
        isNewArchitecture: Bool,
        completion: @escaping EcommerceCompletion
    )
}

public class EcommercePciService: EcommercePciServicing {
    public init() { }

    public func createTransaction(
        password: String,
        orderId: String,
        payload: PaymentPayload<EcommercePayload>,
        isNewArchitecture: Bool,
        completion: @escaping EcommerceCompletion
    ) {
        let api = Api<EcommerceResponse>(endpoint: PaymentEndpoint.ecommerce(payload,
                                                                             orderId: orderId,
                                                                             password: password,
                                                                             isNewArchitecture: isNewArchitecture))
        api.execute { result in
            let mappedResult = result
                .map { $0.model }
            completion(mappedResult)
        }
    }
}
