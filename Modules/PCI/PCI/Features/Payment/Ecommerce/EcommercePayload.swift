import Foundation

public struct EcommercePayload: Codable {
    let orderId: String
    let biometry: Bool
    let sellerId: String
    let cardId: String?
    let value: String
    let credit: String
    let installments: String
    let someErrorOccurred: Bool
    let privacyConfig: String
    let origin: String
    
    public init(
        orderId: String,
        biometry: Bool,
        sellerId: String,
        cardId: String,
        value: String,
        credit: String,
        installments: String,
        someErrorOccurred: Bool,
        privacyConfig: String,
        origin: String
    ) {
        self.orderId = orderId
        self.biometry = biometry
        self.sellerId = sellerId
        self.cardId = cardId
        self.value = value
        self.credit = credit
        self.installments = installments
        self.someErrorOccurred = someErrorOccurred
        self.privacyConfig = privacyConfig
        self.origin = origin
    }
}

extension EcommercePayload {
    private enum CodingKeys: String, CodingKey {
        case orderId
        case biometry
        case sellerId = "seller_id"
        case cardId = "consumer_credit_card_id"
        case value
        case credit
        case installments = "parcelas"
        case someErrorOccurred = "duplicated"
        case privacyConfig = "feed_visibility"
        case origin
    }
}
