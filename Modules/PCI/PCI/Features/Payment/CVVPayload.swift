import Foundation

public struct CVVPayload: Codable {
    public let value: String

    public init?(value: String?) {
        if let cvv = value {
            self.value = cvv
        } else {
            return nil
        }
    }
}
