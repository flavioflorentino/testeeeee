import Foundation

public struct RechargeResponse: Decodable {
    public let receiptWidgets: [[String: Any]]

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let dataContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)

        guard let widgets = try? dataContainer.decode([Any].self, forKey: .receiptWidgets) as? [[String: Any]] else {
            throw NSError(domain: "picpay.com.api", code: 1)
        }

        receiptWidgets = widgets
    }
}

extension RechargeResponse {
    private enum CodingKeys: String, CodingKey {
        case data
        case receiptWidgets = "receipt"
    }
}
