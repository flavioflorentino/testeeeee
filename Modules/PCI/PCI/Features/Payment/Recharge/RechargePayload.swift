import Foundation

public struct RechargePayload: Codable {
    let rechargeTypeId: Int
    let value: String
    let cardSelected: String
    let biometry: Bool
    let chargedValue: String?
    let serviceFee: String?
}

extension RechargePayload {
    private enum CodingKeys: String, CodingKey {
        case rechargeTypeId = "recharge_type_id"
        case value
        case cardSelected = "debit_card_id"
        case biometry
        case chargedValue = "value_charged"
        case serviceFee = "service_charge"
    }
}
