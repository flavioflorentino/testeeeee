import Core
import Foundation

public protocol RechargeServiceProtocol {
    func createTransaction(
        password: String,
        payload: PaymentPayload<RechargePayload>,
        isNewArchitecture: Bool,
        completion: @escaping(Result<RechargeResponse, ApiError>) -> Void
    )
}

public class RechargeService: RechargeServiceProtocol {
    public init() { }

    public func createTransaction(
        password: String,
        payload: PaymentPayload<RechargePayload>,
        isNewArchitecture: Bool,
        completion: @escaping(Result<RechargeResponse, ApiError>) -> Void
    ) {
        let api = Api<RechargeResponse>(endpoint: PaymentEndpoint.recharge(payload,
                                                                           password: password,
                                                                           isNewArchitecture: isNewArchitecture))
        api.execute { result in
            let mappedResult = result
                .map { $0.model }
            
            completion(mappedResult)
        }
    }
}
