import Foundation

public struct DigitalGoodsPayload<T: Codable>: Codable {
    let digitalGoodId: String
    let digitalGoods: T
    let sellerId: String?
    let origin: String
    let password: String
    let biometry: Bool
    let credit: String
    let value: String
    let installments: Int
    let planType: String
    let addressId: String
    let cardId: String?
    let someErrorOccurred: Int
    let privacyConfig: String

    public init(
        digitalGoodId: String,
        digitalGoods: T,
        sellerId: String?,
        origin: String,
        password: String,
        biometry: Bool,
        credit: String,
        value: String,
        installments: Int,
        planType: String,
        addressId: String,
        cardId: String,
        someErrorOccurred: Int,
        privacyConfig: String
    ) {
        self.digitalGoodId = digitalGoodId
        self.digitalGoods = digitalGoods
        self.sellerId = sellerId
        self.origin = origin
        self.password = password
        self.biometry = biometry
        self.credit = credit
        self.value = value
        self.installments = installments
        self.planType = planType
        self.addressId = addressId
        self.cardId = cardId
        self.someErrorOccurred = someErrorOccurred
        self.privacyConfig = privacyConfig
    }
    
    private enum CodingKeys: String, CodingKey {
        case digitalGoodId = "id_digitalgoods"
        case digitalGoods = "digitalgoods"
        case sellerId = "seller_id"
        case origin
        case password = "pin"
        case biometry
        case credit
        case value = "total"
        case installments = "parcelas"
        case planType = "plan_type"
        case addressId = "address_id"
        case cardId = "credit_card_id"
        case someErrorOccurred = "duplicated"
        case privacyConfig = "feed_visibility"
    }
}
