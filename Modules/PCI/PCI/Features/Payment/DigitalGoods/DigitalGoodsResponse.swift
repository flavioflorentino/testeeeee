import Foundation

public struct DigitalGoodsResponse: Decodable {
    public let json: [String: Any]

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        json = try container.decode([String: Any].self)
    }

    private enum CodingKeys: String, CodingKey {
        case data
    }
}
