import Foundation

public struct TransitPass: Codable {
    let productId: Int
    let productType: String
    let value: Double
    let quantity: Int?
    let supplier: String?
    let cardId: String?

    init(
        productId: Int,
        productType: String,
        value: Double,
        quantity: Int?,
        supplier: String?,
        cardId: String?
    ) {
        self.productId = productId
        self.productType = productType
        self.value = value
        self.quantity = quantity
        self.supplier = supplier
        self.cardId = cardId
    }
}

private extension TransitPass {
    enum CodingKeys: String, CodingKey {
        case productId = "product_id"
        case productType = "product_type"
        case value
        case quantity
        case supplier
        case cardId = "user_card_id"
    }
}
