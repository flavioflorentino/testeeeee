import Core
import Foundation

public protocol TransitPassServiceProtocol {
    typealias TransitPassCompletion = (Result<DigitalGoodsResponse, ApiError>) -> Void
    func createTransaction(
        password: String,
        payload: PaymentPayload<DigitalGoodsPayload<TransitPass>>,
        isNewArchitecture: Bool,
        completion: @escaping TransitPassCompletion
    )
}

public class TransitPassService: TransitPassServiceProtocol {
    public init() { }

    public func createTransaction(
        password: String,
        payload: PaymentPayload<DigitalGoodsPayload<TransitPass>>,
        isNewArchitecture: Bool,
        completion: @escaping TransitPassCompletion
    ) {
        let api = Api<DigitalGoodsResponse>(endpoint: PaymentEndpoint.transitPass(payload,
                                                                                  password: password,
                                                                                  isNewArchitecture: isNewArchitecture))
        api.execute { result in
            let mappedResult = result
                .mapError { $0 }
                .map { $0.model }
            completion(mappedResult)
        }
    }
}
