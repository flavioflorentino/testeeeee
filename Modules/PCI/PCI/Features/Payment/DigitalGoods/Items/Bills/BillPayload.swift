import Core
import Foundation

public struct BillPayload: Codable {
    let code: String
    let description: String
    let origin: String
    let password: String
    let biometry: Bool
    let credit: String
    let value: String
    let installments: Int
    let planType: String
    let addressId: String
    let cardId: String?
    let someErrorOccurred: Int
    let privacyConfig: String
    let paymentType: Int
    let schedulePaymentDate: String?
    let cip: AnyCodable?

    private enum CodingKeys: String, CodingKey {
        case code
        case description
        case origin
        case password = "pin"
        case biometry
        case credit
        case value = "total"
        case installments = "parcelas"
        case planType = "plan_type"
        case addressId = "address_id"
        case cardId = "credit_card_id"
        case someErrorOccurred = "duplicated"
        case privacyConfig = "feed_visibility"
        case paymentType = "payment_type"
        case schedulePaymentDate = "schedule_payment_date"
        case cip
    }
    
    public init(
        code: String,
        description: String,
        origin: String,
        password: String,
        biometry: Bool,
        credit: String,
        value: String,
        installments: Int,
        planType: String,
        addressId: String,
        someErrorOccurred: Int,
        privacyConfig: String,
        cardId: String? = nil,
        paymentType: Int = 1,
        schedulePaymentDate: String? = nil,
        cip: AnyCodable? = nil
    ) {
        self.code = code
        self.description = description
        self.origin = origin
        self.password = password
        self.biometry = biometry
        self.credit = credit
        self.value = value
        self.installments = installments
        self.planType = planType
        self.addressId = addressId
        self.someErrorOccurred = someErrorOccurred
        self.privacyConfig = privacyConfig
        self.cardId = cardId
        self.paymentType = paymentType
        self.schedulePaymentDate = schedulePaymentDate
        self.cip = cip
    }
}
