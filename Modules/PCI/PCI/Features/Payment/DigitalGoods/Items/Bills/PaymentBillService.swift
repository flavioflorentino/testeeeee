import Core
import Foundation

public protocol PaymentBillServiceProtocol {
    typealias BillCompletion = (Result<DigitalGoodsResponse, ApiError>) -> Void

    func pay(
        password: String,
        payload: PaymentPayload<BillPayload>,
        isNewArchitecture: Bool,
        completion: @escaping BillCompletion)
}

public class PaymentBillService: PaymentBillServiceProtocol {
    public init() { }

    public func pay(
        password: String,
        payload: PaymentPayload<BillPayload>,
        isNewArchitecture: Bool,
        completion: @escaping BillCompletion
    ) {
        let api = Api<DigitalGoodsResponse>(endpoint: PaymentEndpoint.bill(payload,
                                                                           password: password,
                                                                           isNewArchitecture: isNewArchitecture))
        api.execute { result in
            let mappedResult = result
                .map { $0.model }

            completion(mappedResult)
        }
    }
}
