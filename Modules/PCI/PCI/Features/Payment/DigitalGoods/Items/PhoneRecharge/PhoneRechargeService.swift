import Core
import Foundation

public protocol PhoneRechargeServiceProtocol {
    typealias PhoneRechargeCompletion = (Result<DigitalGoodsResponse, ApiError>) -> Void
    func createTransaction(
        password: String,
        payload: PaymentPayload<DigitalGoodsPayload<PhoneRecharge>>,
        isNewArchitecture: Bool,
        completion: @escaping PhoneRechargeCompletion
    )
}

public class PhoneRechargeService: PhoneRechargeServiceProtocol {
    public init() { }

    public func createTransaction(
        password: String,
        payload: PaymentPayload<DigitalGoodsPayload<PhoneRecharge>>,
        isNewArchitecture: Bool,
        completion: @escaping PhoneRechargeCompletion
    ) {
        let api = Api<DigitalGoodsResponse>(endpoint: PaymentEndpoint.phoneRecharge(payload,
                                                                                    password: password,
                                                                                    isNewArchitecture: isNewArchitecture))
        api.execute { result in
            let mappedResult = result
                .mapError { $0 }
                .map { $0.model }
            completion(mappedResult)
        }
    }
}
