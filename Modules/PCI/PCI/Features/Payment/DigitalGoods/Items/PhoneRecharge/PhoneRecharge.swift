import Foundation

public struct PhoneRecharge: Codable {
    let type: String
    let productId: String
    let ddd: String
    let phone: String
    let carrier: String
    
    public init(
        type: String,
        productId: String,
        ddd: String,
        phone: String,
        carrier: String
    ) {
        self.type = type
        self.productId = productId
        self.ddd = ddd
        self.phone = phone
        self.carrier = carrier
    }
    
    private enum CodingKeys: String, CodingKey {
        case type
        case productId = "product_id"
        case ddd
        case phone
        case carrier = "operator"
    }
}
