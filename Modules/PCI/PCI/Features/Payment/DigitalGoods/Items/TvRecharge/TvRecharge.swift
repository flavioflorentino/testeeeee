import Foundation

public struct TvRecharge: Codable {
    let type: String
    let subscriberCode: String
    let productId: String

    public init(type: String, subscriberCode: String, productId: String) {
        self.type = type
        self.subscriberCode = subscriberCode
        self.productId = productId
    }
    
    private enum CodingKeys: String, CodingKey {
        case type
        case subscriberCode = "subscriber_code"
        case productId = "product_id"
    }
}
