import Core
import Foundation

public protocol TvRechargeServiceProtocol {
    typealias TvRechargeCompletion = (Result<DigitalGoodsResponse, ApiError>) -> Void
    func createTransaction(
        password: String,
        payload: PaymentPayload<DigitalGoodsPayload<TvRecharge>>,
        isNewArchitecture: Bool,
        completion: @escaping TvRechargeCompletion
    )
}

public class TvRechargeService: TvRechargeServiceProtocol {
    public init() { }

    public func createTransaction(
        password: String,
        payload: PaymentPayload<DigitalGoodsPayload<TvRecharge>>,
        isNewArchitecture: Bool,
        completion: @escaping TvRechargeCompletion
    ) {
        let api = Api<DigitalGoodsResponse>(endpoint: PaymentEndpoint.tvRecharge(payload,
                                                                                 password: password,
                                                                                 isNewArchitecture: isNewArchitecture))
        api.execute { result in
            let mappedResult = result
                .mapError { $0 }
                .map { $0.model }
            completion(mappedResult)
        }
    }
}
