import Foundation

public struct Parking: Codable {
    let type: String
    let ticket: String
    let price: String

    public init(type: String, ticket: String, price: String) {
        self.type = type
        self.ticket = ticket
        self.price = price
    }
    
    private enum CodingKeys: String, CodingKey {
        case type
        case ticket = "ticket_number"
        case price = "value"
    }
}
