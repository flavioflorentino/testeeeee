import Core
import Foundation

public protocol ParkingServiceProtocol {
    typealias ParkingCompletion = (Result<DigitalGoodsResponse, ApiError>) -> Void
    func createTransaction(
        password: String,
        payload: PaymentPayload<DigitalGoodsPayload<Parking>>,
        isNewArchitecture: Bool,
        completion: @escaping ParkingCompletion
    )
}

public class ParkingService: ParkingServiceProtocol {
    public init() { }

    public func createTransaction(
        password: String,
        payload: PaymentPayload<DigitalGoodsPayload<Parking>>,
        isNewArchitecture: Bool,
        completion: @escaping ParkingCompletion
    ) {
        let api = Api<DigitalGoodsResponse>(endpoint: PaymentEndpoint.parking(payload,
                                                                              password: password,
                                                                              isNewArchitecture: isNewArchitecture))
        api.execute { result in
            let mappedResult = result
                .mapError { $0 }
                .map { $0.model }
            completion(mappedResult)
        }
    }
}
