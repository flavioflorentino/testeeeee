import Foundation

public struct BluezoneResponse: Decodable {
    public let receiptWidgets: [[String: Any]]

    public init(from decoder: Decoder) throws {
        var container = try decoder.unkeyedContainer()
        guard let widgets = try? container.decode(Array<Any>.self) as? [[String: Any]] else {
            throw NSError(domain: "picpay.com.api", code: 1)
        }
        receiptWidgets = widgets
    }
}
