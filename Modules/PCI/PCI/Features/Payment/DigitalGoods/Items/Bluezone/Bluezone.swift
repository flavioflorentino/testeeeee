import Foundation

public struct Bluezone: Codable {
    let storeId: String
    let type: String
    let areaId: String
    let minutes: String
    let vehicleType: String
    let vehiclePlate: String

    private enum CodingKeys: String, CodingKey {
        case storeId = "store_id"
        case type
        case areaId = "area_id"
        case minutes
        case vehicleType = "vehicle_type"
        case vehiclePlate = "vehicle_plate"
    }
}
