import Core
import Foundation

public protocol BluezoneServiceProtocol {
    typealias BluezonCompletion = (Result<BluezoneResponse, ApiError>) -> Void
    func makeTransaction(
        password: String,
        payload: PaymentPayload<DigitalGoodsPayload<Bluezone>>,
        isNewArchitecture: Bool,
        completion: @escaping BluezonCompletion
    )
}

public class BluezoneService: BluezoneServiceProtocol {
    public init() { }

    public func makeTransaction(
        password: String,
        payload: PaymentPayload<DigitalGoodsPayload<Bluezone>>,
        isNewArchitecture: Bool,
        completion: @escaping BluezonCompletion
    ) {
        let api = Api<BluezoneResponse>(endpoint: PaymentEndpoint.bluezone(payload,
                                                                           password: password,
                                                                           isNewArchitecture: isNewArchitecture))
        api.execute { result in
            let mappedResult = result
                .mapError { $0 }
                .map { $0.model }
            completion(mappedResult)
        }
    }
}
