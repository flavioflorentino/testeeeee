import Core
import Foundation

public protocol VoucherServiceProtocol {
    typealias VoucherCompletion = (Result<DigitalGoodsResponse, ApiError>) -> Void
    func createTransaction(
        password: String,
        payload: PaymentPayload<DigitalGoodsPayload<Voucher>>,
        isNewArchitecture: Bool,
        completion: @escaping VoucherCompletion
    )
}

public class VoucherService: VoucherServiceProtocol {
    public init() { }

    public func createTransaction(
        password: String,
        payload: PaymentPayload<DigitalGoodsPayload<Voucher>>,
        isNewArchitecture: Bool,
        completion: @escaping VoucherCompletion
    ) {
        let api = Api<DigitalGoodsResponse>(endpoint: PaymentEndpoint.voucher(payload,
                                                                              password: password,
                                                                              isNewArchitecture: isNewArchitecture))
        api.execute { result in
            let mappedResult = result
                .mapError { $0 }
                .map { $0.model }
            completion(mappedResult)
        }
    }
}
