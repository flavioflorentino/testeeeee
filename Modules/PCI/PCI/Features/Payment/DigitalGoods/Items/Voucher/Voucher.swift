import Foundation

public struct Voucher: Codable {
    let type: String
    let productId: String

    public init(type: String, productId: String) {
        self.type = type
        self.productId = productId
    }
    
    private enum CodingKeys: String, CodingKey {
        case type
        case productId = "product_id"
    }
}
