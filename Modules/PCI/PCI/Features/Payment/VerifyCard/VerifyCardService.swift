import Core
import Foundation

public protocol VerifyCardServing {
    typealias VerifyCardCompletion = (Result<VerifyCardResponse, ApiError>) -> Void
    func startVerifyCard(payload: PaymentPayload<VerifyCardPayload>, completion: @escaping VerifyCardCompletion)
}

public final class VerifyCardService: VerifyCardServing {
    public init() { }
    
    public func startVerifyCard(payload: PaymentPayload<VerifyCardPayload>, completion: @escaping VerifyCardCompletion) {
        let api = Api<VerifyCardResponse>(endpoint: PaymentEndpoint.verifyCard(payload))
        api.execute { result in
            let mappedResult = result
                .map { $0.model }
            completion(mappedResult)
        }
    }
}
