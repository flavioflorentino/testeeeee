import Foundation

public struct VerifyCardPayload: Codable {
    let id: String
    
    public init(id: String) {
        self.id = id
    }
}
