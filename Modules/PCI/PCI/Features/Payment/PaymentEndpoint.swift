import Core
import Foundation

enum PaymentEndpoint {
    case p2p(_ payment: PaymentPayload<P2PPayload>, password: String, isFeedzai: Bool, isNewArchitecture: Bool)
    case pav(_ payment: PaymentPayload<PAVPayload>, password: String, isFeedzai: Bool, isNewArchitecture: Bool)
    case bill(_ payment: PaymentPayload<BillPayload>, password: String, isNewArchitecture: Bool)
    case bluezone(_ payment: PaymentPayload<DigitalGoodsPayload<Bluezone>>, password: String, isNewArchitecture: Bool)
    case parking(_ payment: PaymentPayload<DigitalGoodsPayload<Parking>>, password: String, isNewArchitecture: Bool)
    case phoneRecharge(_ payment: PaymentPayload<DigitalGoodsPayload<PhoneRecharge>>, password: String, isNewArchitecture: Bool)
    case voucher(_ payment: PaymentPayload<DigitalGoodsPayload<Voucher>>, password: String, isNewArchitecture: Bool)
    case tvRecharge(_ payment: PaymentPayload<DigitalGoodsPayload<TvRecharge>>, password: String, isNewArchitecture: Bool)
    case transitPass(_ payment: PaymentPayload<DigitalGoodsPayload<TransitPass>>, password: String, isNewArchitecture: Bool)
    case linx(_ payment: PaymentPayload<LinxPayload>, password: String, isNewArchitecture: Bool)
    case subscribe(_ planId: String, payment: PaymentPayload<SubscribePayload>, password: String, isNewArchitecture: Bool)
    case subscriptionPay(_ subscriptionId: String, payment: PaymentPayload<SubscriptionPaymentPayload>, password: String, isNewArchitecture: Bool)
    case p2mPayload(_ payment: PaymentPayload<P2MPayload>, password: String, isNewArchitecture: Bool)
    case ecommerce(_ payment: PaymentPayload<EcommercePayload>, orderId: String, password: String, isNewArchitecture: Bool)
    case invoiceCard(_ payment: PaymentPayload<InvoiceCardPayload>, invoiceId: String, password: String, isNewArchitecture: Bool)
    case recharge(_ payment: PaymentPayload<RechargePayload>, password: String, isNewArchitecture: Bool)
    case verifyCard(_ payment: PaymentPayload<VerifyCardPayload>)
    case store(_ payment: PaymentPayload<StoreRequestPayload>, password: String, isNewArchitecture: Bool)
}

class BundleToken { }

extension PaymentEndpoint: ApiEndpointExposable {
    typealias Dependencies = HasKeychainManager
    
    var baseURL: URL {
        let awsApiGateway: String = Environment.get("CARD_ZONE_API_URL", bundle: Bundle(for: BundleToken.self)) ?? "https://qa.card-zone.picpay.com/"
        guard let apiUrl = URL(string: awsApiGateway) else {
            fatalError("Could not parse AWS API Gateway url")
        }
        
        return apiUrl
    }
    
    var path: String {
        switch self {
        case .p2p:
            return "payment/p2p"
        case .recharge:
            return "payment/deposit"
        case .bill:
            return "payment/bills"
        case .ecommerce(_, let orderId, _, _):
            return "payment/ecommerce/\(orderId)"
        case .parking:
            return "payment/parking"
        case .bluezone:
            return "payment/bluezone"
        case .subscribe(let planId, _, _, _):
            return "payment/membership/subscription/\(planId)"
        case .subscriptionPay(let subscriptionId, _, _, _):
            return "payment/membership/payment/\(subscriptionId)"
        case .invoiceCard(_, let invoiceId, _, _):
            return "payment/invoice/\(invoiceId)"
        case .transitPass:
            return "payment/transport"
        case .p2mPayload:
            return "payment/p2m"
        case .pav,
             .voucher,
             .tvRecharge,
             .phoneRecharge,
             .linx:
            return "payment/pav"
        case .verifyCard:
            return "cards/verification"
        case .store:
            return "payment/store"
        }
    }
    
    var isTokenNeeded: Bool {
        false
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var body: Data? {
        switch self {
        case .p2p(let payment, _, _, _):
            return prepareBody(with: payment)
        case .pav(let payment, _, _, _):
            return prepareBody(with: payment)
        case .bill(let payment, _, _):
            return prepareBody(with: payment)
        case .ecommerce(let payment, _, _, _):
            return prepareBody(with: payment)
        case .parking(let payment, _, _):
            return prepareBody(with: payment)
        case .bluezone(let payment, _, _):
            return prepareBody(with: payment)
        case .phoneRecharge(let payment, _, _):
            return prepareBody(with: payment)
        case .subscribe(_, let payment, _, _):
            return prepareBody(with: payment)
        case .subscriptionPay(_, let payment, _, _):
            return prepareBody(with: payment)
        case .invoiceCard(let payment, _, _, _):
            return prepareBody(with: payment)
        case .voucher(let payment, _, _):
            return prepareBody(with: payment)
        case .tvRecharge(let payment, _, _):
            return prepareBody(with: payment)
        case .transitPass(let payment, _, _):
            return prepareBody(with: payment)
        case .p2mPayload(let payment, _, _):
            return prepareBody(with: payment)
        case .linx(let payment, _, _):
            return prepareBody(with: payment)
        case .recharge(let payment, _, _):
            return prepareBody(with: payment)
        case .verifyCard(let payment):
            return prepareBody(with: payment)
        case .store(let payment, _, _):
            return prepareBody(with: payment, strategy: .convertToSnakeCase)
        }
    }
    
    var customHeaders: [String: String] {
        let dependencies: Dependencies = DependencyContainer()
        let token = dependencies.keychain.getData(key: KeychainKey.token) ?? ""
        switch self {
        case .verifyCard:
            return ["X-Authentication": "token=\(token)", "Token": token]
            
        case let .recharge(_, password, isNewArchitecture),
             let .ecommerce(_, _, password, isNewArchitecture),
             let .invoiceCard(_, _, password, isNewArchitecture),
             let .subscribe(_, _, password, isNewArchitecture),
             let .subscriptionPay(_, _, password, isNewArchitecture),
             let .p2mPayload(_, password, isNewArchitecture),
             let .parking(_, password, isNewArchitecture),
             let .bluezone(_, password, isNewArchitecture),
             let .phoneRecharge(_, password, isNewArchitecture),
             let .voucher(_, password, isNewArchitecture),
             let .tvRecharge(_, password, isNewArchitecture),
             let .transitPass(_, password, isNewArchitecture),
             let .linx(_, password, isNewArchitecture),
             let .bill(_, password, isNewArchitecture),
             let .store(_, password, isNewArchitecture):
            return [
                "X-Authentication": "token=\(token), password=\(password)",
                "password": "\(password)",
                "X-New-Payment": "\(isNewArchitecture)"
            ]
            
        case let .p2p(_, password, isFeedzai, isNewArchitecture),
             let .pav(_, password, isFeedzai, isNewArchitecture):
            return [
                "X-Authentication": "token=\(token), password=\(password)",
                "password": "\(password)",
                "risk_engine_v2": "\(isFeedzai)",
                "X-New-Payment": "\(isNewArchitecture)"
            ]
        }
    }
}
