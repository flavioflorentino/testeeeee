import Foundation
import Core

public struct LinxPayload: Codable {
    let pin: String
    let gpsAcc: Int
    let ignoreBalance: Bool
    let fromExplore: Int
    let biometry: Bool
    let credit: String
    let installment: String
    let sellerTransactionId: String
    let sellerId: String
    let planType: String
    let total: String
    let creditCardId: String?
    let feedVisibility: Int
    let itens: [Item]
    let additionalInfo: AnyCodable
    let shippingId: String
    let shipping: String
    let addressId: String
    
    public init(
        pin: String,
        gpsAcc: Int,
        ignoreBalance: Bool,
        fromExplore: Int,
        biometry: Bool,
        credit: String,
        installment: String,
        sellerTransactionId: String,
        sellerId: String,
        planType: String,
        total: String,
        creditCardId: String,
        feedVisibility: Int,
        itens: [Item],
        additionalInfo: AnyCodable,
        shippingId: String,
        shipping: String,
        addressId: String
    ) {
        self.pin = pin
        self.gpsAcc = gpsAcc
        self.ignoreBalance = ignoreBalance
        self.fromExplore = fromExplore
        self.biometry = biometry
        self.credit = credit
        self.installment = installment
        self.sellerTransactionId = sellerTransactionId
        self.sellerId = sellerId
        self.planType = planType
        self.total = total
        self.creditCardId = creditCardId
        self.feedVisibility = feedVisibility
        self.itens = itens
        self.additionalInfo = additionalInfo
        self.shippingId = shippingId
        self.shipping = shipping
        self.addressId = addressId
    }
}

extension LinxPayload {
    public struct Item: Codable {
        let id: String
        let amount: String
        
        public init(id: String, amount: String) {
            self.id = id
            self.amount = amount
        }
    }
}

extension LinxPayload {
    private enum CodingKeys: String, CodingKey {
        case pin
        case gpsAcc = "gps_acc"
        case ignoreBalance = "ignore_balance"
        case fromExplore = "from_explore"
        case biometry, credit
        case installment = "parcelas"
        case sellerTransactionId = "id_transacao_lojista"
        case sellerId = "seller_id"
        case planType = "plan_type"
        case total
        case creditCardId = "credit_card_id"
        case feedVisibility = "feed_visibility"
        case itens
        case additionalInfo = "additional_info"
        case shippingId = "shipping_id"
        case shipping
        case addressId = "address_id"
    }
}
