import Core
import Foundation

public protocol LinxServicing {
    typealias LinxTransactionCompletion = (Result<LinxResponse, ApiError>) -> Void
    func createTransaction(
        password: String,
        payload: PaymentPayload<LinxPayload>,
        isNewArchitecture: Bool,
        completion: @escaping LinxTransactionCompletion
    )
}

public class LinxService: LinxServicing {
    public init() { }

    public func createTransaction(
        password: String,
        payload: PaymentPayload<LinxPayload>,
        isNewArchitecture: Bool,
        completion: @escaping LinxTransactionCompletion
    ) {
        let api = Api<LinxResponse>(endpoint: PaymentEndpoint.linx(payload,
                                                                   password: password,
                                                                   isNewArchitecture: isNewArchitecture))
        api.execute { result in
            let mappedResult = result
                .map { $0.model }
            completion(mappedResult)
        }
    }
}
