import Foundation

public struct LinxResponse: Decodable {
    public let transactionId: String?
    public let receiptWidgets: [[String: Any]]

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let dataContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        let transactionContainer = try dataContainer.nestedContainer(keyedBy: CodingKeys.self, forKey: .transaction)
        
        transactionId = try? transactionContainer.decode(String.self, forKey: .transactionId)
        
        guard let widgets = try? dataContainer.decode([Any].self, forKey: .receiptWidgets) as? [[String: Any]] else {
            throw NSError(domain: "picpay.com.api", code: 1)
        }

        receiptWidgets = widgets
    }

    private enum CodingKeys: String, CodingKey {
        case data
        case transactionId = "id"
        case transaction = "Transaction"
        case receiptWidgets = "receipt"
    }
}
