import Foundation

public struct SubscriptionPaymentPayload: Codable {
    public let biometry: Bool
    public let duplicated: Int
    public let total: String
    public let credit: String
    public let feedVisibility: String
    public let creditCardId: String
    enum CodingKeys: String, CodingKey {
        case biometry
        case duplicated
        case total = "total_value"
        case credit
        case feedVisibility = "feed_visibility"
        case creditCardId = "consumer_credit_card_id"
    }
    public init(
        biometry: Bool,
        duplicated: Int,
        total: String,
        credit: String,
        feedVisibility: String,
        creditCardId: String
    ) {
        self.biometry = biometry
        self.duplicated = duplicated
        self.total = total
        self.credit = credit
        self.feedVisibility = feedVisibility
        self.creditCardId = creditCardId
    }
}
