import Core
import Foundation

public protocol SubscriptionServiceProtocol {
    typealias SubscriptionCompletion = (Result<Data?, ApiError>) -> Void
    func subscribe(
        password: String,
        planId: String,
        payload: PaymentPayload<SubscribePayload>,
        isNewArchitecture: Bool,
        completion: @escaping SubscriptionCompletion
    )
    func subscriptionPay(
        password: String,
        subscriptionId: String,
        payload: PaymentPayload<SubscriptionPaymentPayload>,
        isNewArchitecture: Bool,
        completion: @escaping SubscriptionCompletion
    )
}

public class SubscriptionService: SubscriptionServiceProtocol {
    public init() { }
    public func subscribe(
        password: String,
        planId: String,
        payload: PaymentPayload<SubscribePayload>,
        isNewArchitecture: Bool,
        completion: @escaping SubscriptionCompletion
    ) {
        let api = Api<Empty>(endpoint: PaymentEndpoint.subscribe(planId,
                                                                 payment: payload,
                                                                 password: password,
                                                                 isNewArchitecture: isNewArchitecture))
        api.execute { result in
            let mappedResult = result
                .map { $0.data }

            completion(mappedResult)
        }
    }

    public func subscriptionPay(
        password: String,
        subscriptionId: String,
        payload: PaymentPayload<SubscriptionPaymentPayload>,
        isNewArchitecture: Bool,
        completion: @escaping SubscriptionCompletion
    ) {
        let api = Api<Empty>(endpoint: PaymentEndpoint.subscriptionPay(subscriptionId,
                                                                       payment: payload,
                                                                       password: password,
                                                                       isNewArchitecture: isNewArchitecture))
        api.execute { result in
            let mappedResult = result
                .map { $0.data }
            completion(mappedResult)
        }
    }
}
