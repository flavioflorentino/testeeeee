import Foundation

public struct SubscribePayload: Codable {
    public let addressId: Int
    public let biometry: Bool
    public let creditCardId: String?
    public let credit: String
    public let duplicated: Int
    public let feedVisibility: String
    public let total: String
    enum CodingKeys: String, CodingKey {
        case addressId = "address_id"
        case biometry
        case creditCardId = "consumer_credit_card_id"
        case credit
        case duplicated
        case feedVisibility = "feed_visibility"
        case total = "total_value"
    }
    public init(
        addressId: Int,
        biometry: Bool,
        creditCardId: String?,
        credit: String,
        duplicated: Int,
        feedVisibility: String,
        total: String
    ) {
        self.addressId = addressId
        self.biometry = biometry
        self.creditCardId = creditCardId
        self.credit = credit
        self.duplicated = duplicated
        self.feedVisibility = feedVisibility
        self.total = total
    }
}
