import Foundation

public struct PaymentPayload<T: Codable>: Codable {
    public let cvv: CVVPayload?
    public let generic: T
    public init(cvv: CVVPayload?, generic: T) {
        self.generic = generic
        self.cvv = cvv
    }
}
