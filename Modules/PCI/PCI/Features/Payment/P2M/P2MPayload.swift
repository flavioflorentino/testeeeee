import Foundation
import Core

public struct P2MPayload: Codable {
    let origin: String
    let biometry: Bool
    let cardId: String?
    let value: String
    let credit: String
    let installments: String
    let someErrorOccurred: Bool
    let privacyConfig: String
    let session: AnyCodable

    public init(
        origin: String,
        biometry: Bool,
        cardId: String,
        value: String,
        credit: String,
        installments: String,
        someErrorOccurred: Bool,
        privacyConfig: String,
        session: AnyCodable
    ) {
        self.origin = origin
        self.biometry = biometry
        self.cardId = cardId
        self.value = value
        self.credit = credit
        self.installments = installments
        self.someErrorOccurred = someErrorOccurred
        self.privacyConfig = privacyConfig
        self.session = session
    }
    
    private enum CodingKeys: String, CodingKey {
        case origin
        case biometry
        case cardId = "consumer_credit_card_id"
        case value
        case credit
        case installments = "parcelas"
        case someErrorOccurred = "duplicated"
        case privacyConfig = "feed_visibility"
        case session
    }
}
