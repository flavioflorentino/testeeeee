import Core
import Foundation

public protocol P2MServiceProtocol {
    func createTransaction(
        password: String,
        payload: PaymentPayload<P2MPayload>,
        isNewArchitecture: Bool,
        completion: @escaping(Result<P2MResponse, ApiError>) -> Void)
}

public class P2MService: P2MServiceProtocol {
    public init() { }

    public func createTransaction(
        password: String,
        payload: PaymentPayload<P2MPayload>,
        isNewArchitecture: Bool,
        completion: @escaping(Result<P2MResponse, ApiError>) -> Void
    ) {
        let api = Api<P2MResponse>(endpoint: PaymentEndpoint.p2mPayload(payload,
                                                                        password: password,
                                                                        isNewArchitecture: isNewArchitecture))
        api.execute { result in
            let mappedResult = result
                .map { $0.model }
            completion(mappedResult)
        }
    }
}
