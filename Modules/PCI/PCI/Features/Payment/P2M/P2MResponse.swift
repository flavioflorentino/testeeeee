import Foundation

public struct P2MResponse: Decodable {
    public let transactionId: String?
    public let receiptWidgets: [[String: Any]]

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        transactionId = try? container.decode(String.self, forKey: .transactionId)
        guard let widgets = try? container.decode(Array<Any>.self, forKey: .receiptWidgets) as? [[String: Any]] else {
            throw NSError(domain: "picpay.com.api", code: 1)
        }

        receiptWidgets = widgets
    }

    private enum CodingKeys: String, CodingKey {
        case transactionId = "transaction_id"
        case receiptWidgets = "receipt"
    }
}
