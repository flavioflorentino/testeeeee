import Core
import Foundation

public protocol P2PServiceProtocol {
    typealias P2PTransactionCompletion = (Result<P2PResponse, ApiError>) -> Void
    func createTransaction(
        password: String,
        isFeedzai: Bool,
        payload: PaymentPayload<P2PPayload>,
        isNewArchitecture: Bool,
        completion: @escaping P2PTransactionCompletion
    )
}

public class P2PService: P2PServiceProtocol {
    public init() { }

    public func createTransaction(
        password: String,
        isFeedzai: Bool,
        payload: PaymentPayload<P2PPayload>,
        isNewArchitecture: Bool,
        completion: @escaping P2PTransactionCompletion
    ) {
        let api = Api<P2PResponse>(endpoint: PaymentEndpoint.p2p(payload,
                                                                 password: password,
                                                                 isFeedzai: isFeedzai,
                                                                 isNewArchitecture: isNewArchitecture))
        api.execute { result in
            let mappedResult = result
                .map { $0.model }
            completion(mappedResult)
        }
    }
}
