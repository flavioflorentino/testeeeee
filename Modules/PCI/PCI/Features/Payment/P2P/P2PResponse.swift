import Foundation

public struct P2PResponse: Decodable {
    public let p2pId: String
    public let value: String
    public let receiptWidgets: [[String: Any]]

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let dataContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        let p2pContainer = try dataContainer.nestedContainer(keyedBy: CodingKeys.self, forKey: .p2p)

        p2pId = try p2pContainer.decode(String.self, forKey: .p2pId)
        value = try p2pContainer.decode(String.self, forKey: .value)

        guard let widgets = try? dataContainer.decode([Any].self, forKey: .receiptWidgets) as? [[String: Any]] else {
            throw NSError(domain: "picpay.com.api", code: 1)
        }

        receiptWidgets = widgets
    }

    private enum CodingKeys: String, CodingKey {
        case p2p = "P2PTransaction"
        case data
        case p2pId = "id"
        case value = "consumer_value"
        case receiptWidgets = "receipt"
    }
}
