import Foundation

public struct P2PPayload: Codable {
    let password: String
    let biometry: Bool
    let payeeId: String
    let value: Double
    let credit: Double
    let installments: Int
    let cardId: String?
    let someErrorOccurred: Bool
    let privacyConfig: String
    let message: String?
    let ignoreBalance: Bool?
    let extra: String?
    let surcharge: Double?
    let payeePhone: String?

    public init(
        password: String,
        biometry: Bool,
        payeeId: String,
        value: Double,
        credit: Double,
        installments: Int,
        cardId: String,
        someErrorOccurred: Bool,
        privacyConfig: String,
        message: String?,
        ignoreBalance: Bool? = nil,
        extra: String? = nil,
        surcharge: Double?,
        payeePhone: String? = nil
    ) {
        self.password = password
        self.biometry = biometry
        self.payeeId = payeeId
        self.value = value
        self.credit = credit
        self.installments = installments
        self.cardId = cardId
        self.someErrorOccurred = someErrorOccurred
        self.privacyConfig = privacyConfig
        self.message = message
        self.ignoreBalance = ignoreBalance
        self.extra = extra
        self.surcharge = surcharge
        self.payeePhone = payeePhone
    }
    
    private enum CodingKeys: String, CodingKey {
        case password = "pin"
        case biometry
        case payeeId = "payee_id"
        case value = "consumer_value"
        case credit
        case ignoreBalance = "ignore_balance"
        case installments = "parcelas"
        case cardId = "consumer_credit_card_id"
        case someErrorOccurred = "duplicated"
        case privacyConfig = "feed_visibility"
        case message
        case extra
        case surcharge
        case payeePhone = "payee_phone"
    }
}
