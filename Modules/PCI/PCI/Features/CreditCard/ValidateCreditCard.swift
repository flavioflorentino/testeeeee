import Foundation

public struct ValidateCreditCard: Decodable {
    public let askCpf: Bool

    enum RootKeys: String, CodingKey {
        case data
    }

    enum DataKeys: String, CodingKey {
        case askCpf
    }

    public init(from decoder: Decoder) throws {
        let rootContainer = try decoder.container(keyedBy: RootKeys.self)

        let dataContainer = try rootContainer.nestedContainer(keyedBy: DataKeys.self, forKey: .data)

        let askCpfInt = try dataContainer.decode(Int.self, forKey: .askCpf)

        askCpf = askCpfInt == 1
    }
}
