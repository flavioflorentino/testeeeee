import Core
import Foundation

enum CreditCardEndpoint {
    case save(card: CardPayload)
}

extension CreditCardEndpoint: ApiEndpointExposable {
    typealias Dependencies = HasKeychainManager
    var path: String {
        "api/getCardCode.json"
    }

    var isTokenNeeded: Bool {
        false
    }

    var method: HTTPMethod {
        .post
    }

    var body: Data? {
        switch self {
        case let .save(card):
            return prepareBody(with: card)
        }
    }

    var customHeaders: [String: String] {
        let dependencies: Dependencies = DependencyContainer()
        return ["Token": dependencies.keychain.getData(key: KeychainKey.token) ?? ""]
    }

    private func prepareBody(with card: CardPayload) -> Data? {
        let jsonEncoder = JSONEncoder()
        jsonEncoder.outputFormatting = .prettyPrinted
        do {
            return try jsonEncoder.encode(card)
        } catch {
            debugPrint("Failure to prepare card payload. \(error)")
            return nil
        }
    }
}
