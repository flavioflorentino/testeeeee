import Core
import Foundation

public protocol CardRegistrationServicing {
    typealias CardValidationCompletion = (Result<ValidateCreditCard, ApiError>) -> Void
    typealias CardRegistrationCompletion = (Result<CreditCard, ApiError>) -> Void

    func validate(card: CardPayload, completion: @escaping CardValidationCompletion)
    func save(card: CardPayload, completion: @escaping CardRegistrationCompletion)
}

public class CardRegistrationService: CardRegistrationServicing {
    public init() { }

    public func validate(card: CardPayload, completion: @escaping CardValidationCompletion) {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase

        let api = Api<ValidateCreditCard>(endpoint: CardEndpoint.validate(card))
        api.execute(jsonDecoder: decoder) { result in
            let mappedResult = result
                .map { $0.model }

            completion(mappedResult)
        }
    }
    public func save(card: CardPayload, completion: @escaping CardRegistrationCompletion) {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase

        let api = Api<CreditCard>(endpoint: CardEndpoint.save(card))
        api.execute(jsonDecoder: decoder) { result in
            let mappedResult = result
                .map { $0.model }

            completion(mappedResult)
        }
    }
}
