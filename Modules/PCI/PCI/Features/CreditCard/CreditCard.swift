import Foundation

public struct CreditCard: Decodable {
    public let data: [String: Any]
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        data = try container.decode([String: Any].self, forKey: .data)
    }
}

extension CreditCard {
    private enum CodingKeys: String, CodingKey {
        case data
    }
}
