import Core
import Foundation

enum CardEndpoint {
    case save(_ card: CardPayload)
    case validate(_ card: CardPayload)
}

extension CardEndpoint: ApiEndpointExposable {
    typealias Dependencies = HasKeychainManager
    var baseURL: URL {
        let awsApiGateway: String = Environment.get("CARD_ZONE_API_URL", bundle: Bundle(for: BundleToken.self)) ?? "https://qa.card-zone.picpay.com/"
        guard let apiUrl = URL(string: awsApiGateway) else {
            fatalError("Could not parse AWS API Gateway url")
        }

        return apiUrl
    }

    var path: String {
        switch self {
        case .save:
            return "cards"
        case .validate:
            return "cards/validation"
        }
    }

    var isTokenNeeded: Bool {
        false
    }

    var method: HTTPMethod {
        .post
    }

    var body: Data? {
        switch self {
        case let .save(card), let .validate(card):
            return prepareBody(with: card, strategy: .convertToSnakeCase)
        }
    }
    
    var customHeaders: [String: String] {
        let dependencies: Dependencies = DependencyContainer()
        let token = dependencies.keychain.getData(key: KeychainKey.token) ?? ""
        return ["X-Authentication": "token=\(token)"]
    }
}
