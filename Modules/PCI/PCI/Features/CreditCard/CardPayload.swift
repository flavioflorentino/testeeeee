import Foundation

// swiftlint:disable identifier_name
public struct CardPayload: Codable {
    public let cardHolder: String
    public let cardNumber: String
    public let expiryDate: String
    public let cvv: String
    public let cardFlagId: Int
    public let scanned: Bool
    public let origin: String
    public let alias: String?
    public let cpf: String?
    public let phone: String?
    public let zipcode: String?
    public let city: String?
    public let uf: String?
    public let number: String?
    public let complement: String?
    public let street: String?
    public let scannedCardHolder: String?
    public let isDebitCard: Bool

    private enum CodingKeys: String, CodingKey {
        case cardHolder = "card_holder"
        case cardNumber = "card_number"
        case expiryDate = "expiry_date"
        case cvv
        case alias
        case cardFlagId = "credit_card_bandeira_id"
        case cpf = "holder_cpf"
        case phone = "holder_phone"
        case scanned
        case zipcode = "zip_code"
        case city = "cidade"
        case uf
        case number
        case complement
        case street
        case origin
        case scannedCardHolder = "scanned_card_holder"
        case isDebitCard = "insert_as_debit_card"
    }

    public init(
        cardHolder: String,
        cardNumber: String,
        expiryDate: String,
        cvv: String,
        cardFlagId: Int,
        scanned: Bool,
        origin: String,
        alias: String? = nil,
        cpf: String? = nil,
        phone: String? = nil,
        zipcode: String? = nil,
        city: String? = nil,
        uf: String? = nil,
        number: String? = nil,
        complement: String? = nil,
        street: String? = nil,
        scannedCardHolder: String? = nil,
        isDebitCard: Bool
    ) {
        self.cardHolder = cardHolder
        self.cardNumber = cardNumber
        self.expiryDate = expiryDate
        self.cvv = cvv
        self.alias = alias
        self.cardFlagId = cardFlagId
        self.cpf = cpf
        self.phone = phone
        self.scanned = scanned
        self.zipcode = zipcode
        self.city = city
        self.uf = uf
        self.number = number
        self.complement = complement
        self.street = street
        self.origin = origin
        self.scannedCardHolder = scannedCardHolder
        self.isDebitCard = isDebitCard
    }
}
