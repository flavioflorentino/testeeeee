import Foundation

public struct StoreRequestPayload: Codable {
    private var data: StoreData?
    private var storeId: String
    private var feedVisibility: String
    private var paymentMethods: PaymentMethod?
    private var consumerCreditCardId: String
    
    public init(storeId: String, feedVisibility: String, paymentMethod: PaymentMethod, data: StoreData?) {
        self.data = data
        self.storeId = storeId
        self.feedVisibility = feedVisibility
        self.paymentMethods = paymentMethod
        self.consumerCreditCardId = paymentMethod.creditCard.id
    }
}

public struct PaymentMethod: Codable {
    private let userBalance: UserBalance
    public let creditCard: UserCreditCard
    public init(userBalance: UserBalance, creditCard: UserCreditCard) {
        self.creditCard = creditCard
        self.userBalance = userBalance
    }
    
    public struct UserBalance: Codable {
        private let amount: String
        public init(amount: String) {
            self.amount = amount
        }
    }
    
    public struct UserCreditCard: Codable {
        public let id: String
        private let amount: String
        private let installments: Int
        public init(id: String, amount: String, installments: Int) {
            self.id = id
            self.amount = amount
            self.installments = installments
        }
    }
}

public struct StoreData: Codable {
    private let inputData: [Model]
    private let externalData: [Model]
    public init(inputData: [Model], externalData: [Model]) {
        self.inputData = inputData
        self.externalData = externalData
    }
    public struct Model: Codable {
        let key: String
        let value: String
        public init(key: String, value: String) {
            self.key = key
            self.value = value
        }
    }
}
