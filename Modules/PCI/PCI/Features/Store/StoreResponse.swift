import Foundation

public struct StoreResponse: Decodable {
    private enum CodingKeys: String, CodingKey {
        case data
    }
    
    public let json: [String: Any]
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.json = try container.decode([String: Any].self, forKey: .data)
    }
}
