import Core
import Foundation

public protocol StoreServiceProtocol {
    typealias StoreCompletion = (Result<StoreResponse, ApiError>) -> Void
    func createTransaction(
        password: String,
        payload: PaymentPayload<StoreRequestPayload>,
        isNewArchitecture: Bool,
        completion: @escaping StoreCompletion
    )
}

public class StoreService: StoreServiceProtocol {
    public init() { }
    
    public func createTransaction(
        password: String,
        payload: PaymentPayload<StoreRequestPayload>,
        isNewArchitecture: Bool,
        completion: @escaping StoreCompletion
    ) {
        let api = Api<StoreResponse>(endpoint: PaymentEndpoint.store(payload,
                                                                     password: password,
                                                                     isNewArchitecture: isNewArchitecture))
        api.execute { result in
            let mappedResult = result
                .map(\.model)
            completion(mappedResult)
        }
    }
}
