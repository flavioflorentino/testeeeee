import Foundation

struct RSA {
    static func decrypt(cvv: String, privateKey: String) -> String? {
        guard let key = secKey(with: privateKey) else {
            return nil
        }

        guard let data = Data(base64Encoded: cvv) else {
            return nil
        }

        guard let decryptedCvvData = rsaDecrypt(data, withKey: key) else {
            return nil
        }

        return String(data: decryptedCvvData, encoding: .utf8)
    }

    private static func rsaDecrypt(_ inputData: Data, withKey key: SecKey) -> Data? {
        guard inputData.count == SecKeyGetBlockSize(key) else {
            return nil
        }

        let keySize = SecKeyGetBlockSize(key)
        var decryptBytes = [UInt8](repeating: 0, count: keySize)
        var outputSize: Int = keySize

        guard SecKeyDecrypt(key, SecPadding.PKCS1, [UInt8](inputData), inputData.count, &decryptBytes, &outputSize) == errSecSuccess else {
            return nil
        }

        return Data(bytes: UnsafePointer<UInt8>(decryptBytes), count: outputSize)
    }

    private static func secKey(with key: String) -> SecKey? {
        let keyString = key
            .replacingOccurrences(of: "-----BEGIN RSA PRIVATE KEY-----\n", with: "")
            .replacingOccurrences(of: "\n-----END RSA PRIVATE KEY-----", with: "")

        guard let keyData = Data(base64Encoded: keyString) else {
            return nil
        }
        
        var attributes: CFDictionary {
            [
                kSecAttrKeyType: kSecAttrKeyTypeRSA,
                kSecAttrKeyClass: kSecAttrKeyClassPrivate,
                kSecAttrKeySizeInBits: 1024,
                kSecReturnPersistentRef: kCFBooleanTrue!
            ] as CFDictionary
        }
        
        var error: Unmanaged<CFError>?
        guard let secKey = SecKeyCreateWithData(keyData as CFData, attributes, &error) else {
            return nil
        }

        return secKey
    }
}
