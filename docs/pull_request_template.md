<!---XX - Type(Squad): [JIRA-001] Descrição da mudança em português -->

<!--- Por favor, inclua os valores PF para o app verdinho, PJ para o app do empresas ou ANY quando for uma PR que impacta os dois, substituindo o `XX` no título do PR, seguindo o formato acima. -->
<!--- Por favor, inclua o `Type` da mudança e opcionalmente a `(Squad)` no título do PR, seguindo o formato acima. -->
<!--- Valores válidos para `Type` são: <Added|Changed|Deprecated|Removed|Fixed|Security> (http://keepachangelog.com/en/1.0.0/) -->

# Descrição

<!--- Favor incluir um resumo das alterações e erros que foram corrigidos, assim como motivação e contexto relevantes. Liste qualquer dependência necessária para essa alteração -->

## Qual sua feature flag para essa PR? 
<!--- 
Descreva aqui a(s) feature flag(s) relacionada(s) à sua funcionalidade ou correção.
Caso não haja uma feature flag, lembre-se dos riscos que isso pode acarretar para todo o projeto.
Nos casos que o fluxo já esta contemplado com uma ou mais flags, descreva aqui todas as necessárias para testar o fluxo corretamente
-->
N/A

## Tipo de mudança

<!--- Por favor, remova as opções irrelevantes. -->

- Bug fix (Correção de um problema sem efeito colateral)
- New feature (Adiciona uma nova funcionalidade sem efeito colateral no código)
- Breaking change (Correção ou recurso que faria com que alguma funcionalidade existente não funcionasse como esperado)
- Code refactor (Melhorias de código) 

## O CI deve ser executado?
Lembre de alterar essa sessão quando for atualizar o PR.

<!--- Por favor, remova a opção que não condiz com seu PR. -->

- **Sim!**
- **Não, [ci skip]**

## Como deve ser testado?

<!--- Por favor, descreva os testes que devem ser executados para verificar suas alterações. -->

N/A

## Prints da tela ou vídeo:

<!--- 
Prints da tela feita ou modificada em light e dark mode, caso se aplique 
Light Mode | Dark Mode
---------  | ---------
Link foto  | Link foto
-->

<!--- Video -->
<!--- <img width=250 src=link_video.mov> -->
N/A

## Outras observações:

<!--- Outras observações relevantes que não se encaixam nos campos acima -->

N/A
