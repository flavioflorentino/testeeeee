SCRIPT=`/usr/bin/find "${SRCROOT}/.." -name newrelic_postbuild.sh | head -n 1`

if [[ $CONFIGURATION == 'Release' ]]; then
    echo "PicPay Release"
    /bin/sh "${SCRIPT}" "AA2892828b28001aa811582902ec152fe4908e8a40-NRMA"
else
    echo "PicPay Debug | Homolog"
    /bin/sh "${SCRIPT}" "AA44dc9bbf2ff7b4285958bd1d98c056401e738733-NRMA"
fi
