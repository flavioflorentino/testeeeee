PATH_TO_GOOGLE_PLISTS="${PROJECT_DIR}/PicPay/Supporting Files"

case "${CONFIGURATION}" in
   "Debug" | "Homolog" )
        cp -v "$PATH_TO_GOOGLE_PLISTS/GoogleService-Info-Dev.plist" "${BUILT_PRODUCTS_DIR}/${PRODUCT_NAME}.app/GoogleService-Info.plist" ;;

   "Release")
        cp -v "$PATH_TO_GOOGLE_PLISTS/GoogleService-Info.plist" "${BUILT_PRODUCTS_DIR}/${PRODUCT_NAME}.app/GoogleService-Info.plist" ;;
    *)
        ;;
esac
