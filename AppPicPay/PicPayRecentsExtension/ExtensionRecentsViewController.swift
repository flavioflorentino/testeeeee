//
//  ExtensionRecentsViewController.swift
//  PicPay
//
//  Created by Lorenzo Piccoli on 5/3/17.
//
//

import Foundation
import UIKit
import NotificationCenter


class ExtensionRecentsViewController: UIViewController {
    @IBOutlet weak var collectionView: ExtensionCollectionView!
    fileprivate var recents: [Recent] = [] {
        didSet {
            setupCollectionView()
            configureShowMoreButton()
        }
    }
    fileprivate var isLoggedIn = false
    fileprivate var isEmpty = false
    fileprivate var isLoading = false

    @IBOutlet weak var loginButton: UIPPButton!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        automaticallyAdjustsScrollViewInsets = false

        collectionView.delegate = self
        loginButton.normalBackgrounColor = .clear
        loginButton.borderColor = .clear
        loginButton.normalTitleColor = .darkGray
        
        isLoggedIn = APIManager.shared.token != ""
        loadingView.isHidden = true
        
        configureShowMoreButton()
        
        if isLoggedIn {
            CacheManager.loadFromCache(completion: { [weak self] (recents) in
                DispatchQueue.main.async {
                    self?.recents = recents
                    self?.configureUI()
                }
            })
        }else{
            CacheManager.clear()
        }
    }
    
    func configureShowMoreButton() {
        // Show more button if the user is logged and there are more than 4 recents transactions
        self.extensionContext?.widgetLargestAvailableDisplayMode = isLoggedIn && recents.count > 4 ? .expanded : .compact
    }
    
    func configureUI(){
        // Do not show the login button if the user is logged and the result is not empty
        loginButton.isHidden = isLoggedIn && !recents.isEmpty || isLoading
        // Do not show the collectionView if the user is not logged in or the result is empty
        collectionView.isHidden = !isLoggedIn || recents.isEmpty
        
        if recents.isEmpty && isLoggedIn {
            if CacheManager.hasLoadedData() {
                loginButton.setTitle("Você não tem pagamentos recentes", for: .normal)
            }else{
                loginButton.setTitle("", for: .normal)
            }
        }

    }
    
    @IBAction private func loginTapped(_ sender: Any) {
        // Just open the app
        guard let url = URL(string: "picpay://") else {
            return
        }
        self.extensionContext?.open(url, completionHandler: nil)
    }
    
    func setupCollectionView() {
        if  let context = self.extensionContext {
            let width = context.widgetMaximumSize(for: context.widgetActiveDisplayMode).width
            self.collectionView.setup(results: recents, screenWidth: width)
        }
    }
    
}

extension ExtensionRecentsViewController: NCWidgetProviding {
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        // Max size if compact or 200 height if expanded
        switch activeDisplayMode {
        case .compact:
            preferredContentSize = maxSize
        case .expanded:
            preferredContentSize = CGSize(width: maxSize.width, height: 200)
        @unknown default:
            preferredContentSize = maxSize
        }
    }
    
    // Called on startup, after viewDidLoad
    func widgetPerformUpdate(completionHandler: @escaping (NCUpdateResult) -> Void) {
        guard isLoggedIn else { completionHandler(.noData); return }
        
        if recents.isEmpty {
            loadingView.isHidden = false
        }
        
        isLoading = true
        APIManager.shared.search(limit: 8) { [weak self] (recent, error) in
            DispatchQueue.main.async {
                self?.loadingView.isHidden = true
                self?.isLoading = false
                
                if let error = error, error.code != -1009 {
                    self?.isLoggedIn = false
                    self?.configureUI()
                    completionHandler(.failed)
                    return
                }
            
                guard let recent = recent else {
            return
        }
                self?.recents = recent

                if recent.isEmpty {
                    self?.isEmpty = true
                    self?.configureUI()
                    completionHandler(.noData)
                    return
                }
                self?.setupCollectionView()
                self?.configureUI()
                completionHandler(.newData)
                return
            }
        }
    }
}

extension ExtensionRecentsViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let recent = recents[indexPath.row]
        guard let type = recent.type,
            let id = recent.id else {
            return
        }
        
        // Send the type and id (for both user and store)
        var urlString = "picpay://picpay/payment?type=\(type.rawValue)&id=\(id)"
        
        // If it's a store, add the seller ID
        if type == .store{
            guard let sellerId = recent.sellerId else {
            return
        }
            urlString = urlString.appending("&sellerId=\(sellerId)")
        }
        
        if type == .digitalgoods {
            guard let service = recent.service else {
            return
        }
            urlString = urlString.appending("&service=\(service)")
        }
        
        guard let url = URL(string: urlString) else {
            print("No URL")
            return
        }
        
        self.extensionContext?.open(url, completionHandler: nil)
    }
}
