//
//  Recent.swift
//  PicPay
//
//  Created by Lorenzo Piccoli on 5/5/17.
//
//

import Foundation
import UIKit

enum RecentType: String {
    case store = "store"
    case user = "user"
    case digitalgoods = "digital_good"
}

class Recent: NSObject, NSCoding
{
    var id: String?
    var sellerId: String?
    var image: String?
    var name: String?
    var type: RecentType?
    var service: String?
    
    init(json: Dictionary<String, AnyObject>, seller: Dictionary<String, AnyObject>? = nil, type: RecentType) {
        super.init()
        
        self.type = type
        
        if let id = json["id"] as? String {
            self.id = id
        }
        
        if let id = json["_id"] as? String {
            self.id = id
        }
        
        if let sellerId = json["seller_id"] as? String {
            self.sellerId = sellerId
        }
        
        if let service = json["service"] as? String {
            self.service = service
        }
        
        if let name = json["name"] as? String {
            self.name = name
        }
        // If it's a user we actually want the username
        if let username = json["username"] as? String {
            self.name = "@\(username)"
        }
        /*if let img = json["img_url"] as? String {
            self.image = img
        }*/
        // If it's a place we actually want the seller image
        if let sellerData = seller,
           let image = sellerData["img_url"] as? String {
            self.image = image
        }
        
        if let image = json["image_url"] as? String {
            self.image = image
        } else if let image = json["image_url_small"] as? String {
            self.image = image
        }
        
        if let processedImagesJson = json["processed_images"] as? String,
           let processedImages = convertToDictionary(processedImagesJson),
           let thumb = processedImages["200"] as? String{
               self.image = thumb
        }
    }
    
    // Encode / Decode to save on userDefaults
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(sellerId, forKey: "sellerId")
        aCoder.encode(image, forKey: "image")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(type?.rawValue, forKey: "type")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? String
        self.sellerId = aDecoder.decodeObject(forKey: "sellerId") as? String
        self.image = aDecoder.decodeObject(forKey: "image") as? String
        self.name = aDecoder.decodeObject(forKey: "name") as? String
        self.type = RecentType.init(rawValue: aDecoder.decodeObject(forKey: "type") as? String ?? "user")
    }
    
    private func convertToDictionary(_ text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
