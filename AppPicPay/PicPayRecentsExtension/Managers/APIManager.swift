import Foundation
import UIKit
import Core

class APIManager: NSObject {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies = DependencyContainer()
    
    static let shared = APIManager()
    var token: String {
        let groupIdentifier = Bundle.main.bundleIdentifier.map {
            $0.components(separatedBy: ".").dropLast().joined(separator: ".")
        }
        
        if let token = dependencies.keychain.getData(key: KeychainKey.token) {
            return token
        } else if let defaults = UserDefaults(suiteName: Constants.userDefaultSuite), let token = defaults.object(forKey: "userToken") as? String {
            dependencies.keychain.set(key: KeychainKey.token, value: token)
            defaults.removeObject(forKey: "userToken")
            return token
        }
        
        return ""
    }
    
    func search(limit: Int,  _ completion: @escaping ([Recent]?, NSError?) -> ()) {
        guard let appVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String else {
            return
        }

        AlamofireWrapper.get(route: "search", headers: ["token": token, "device_os": "ios", "app_version": appVersion], params: ["group": "MAIN", "widget": true]) { (result, error) in
            if let e = error{
                completion(nil, e)
                return
            }
            
            if let e = result?["Error"] as? Dictionary<String, AnyObject>,
               let stringId = e["id"] as? String,
               let id = Int(stringId) {
                completion(nil, NSError(domain: "PicPay", code: id, userInfo: nil))
                return
            }
            
            guard let sections = result as? Array<NSDictionary> else {
            return
        }
            
            var recents:[Recent] = []
            var count = 0
            for section in sections {
                guard let type = section["type"] as? String,
                    let rows = section["rows"] as? Array<NSObject>,
                    type == "recents" else { continue }
                
                for row in rows {
                    guard let recentObj = row as? Dictionary<String, AnyObject>,
                        let type = recentObj["type"] as? String,
                        let recentData = recentObj["data"] as? Dictionary<String, AnyObject>,
                        count < limit else { continue }
                    
                    if (type != "store" && type != "consumer" && type != "digital_good") {
                        continue
                    }
                    
                    if type == "digital_good" {
                        //guard let service = recentData["service"] as? String, service == "" else { continue }
                        
                    }
                    
                    var finalData = recentData
                    
                    if type == "store" {
                        guard let store = recentData["Store"] as? Dictionary<String, AnyObject> else { continue }
                        
                        finalData = store
                    }
                    
                    // This won't be nil if the result is a store
                    let seller = recentData["Seller"] as? Dictionary<String, AnyObject>
                    
                    
                    var typeEnum: RecentType = .user
                    if type == "store"{
                        typeEnum = .store
                    }else if type == "digital_good" {
                        typeEnum = .digitalgoods
                    }
                    
                    
                    recents.append(Recent(json: finalData, seller: seller, type: typeEnum))
                    count += 1
                }
                
                // Save to cache
                CacheManager.saveToCache(data: recents)
                
                completion(recents, nil)
                return
            }
            // No recents section = empty
            completion([], nil)
        }

    }
}
