//
//  CacheManager.swift
//  PicPay
//
//  Created by Lorenzo Piccoli on 5/10/17.
//
//

import Foundation
import UIKit

class CacheManager: NSObject {
    class func saveToCache(data: [Recent]) {
        let defaults = UserDefaults(suiteName: Constants.userDefaultSuite)
        let archivedData = NSKeyedArchiver.archivedData(withRootObject: data)
        defaults?.set(archivedData, forKey: Constants.userDefaultKey)
        
        if data.count > 0 {
            defaults?.set(true, forKey: Constants.hasLoadedData)
        }
        
        defaults?.synchronize()
    }
    
    class func loadFromCache(completion: @escaping ((_ recents:[Recent]) -> Void) ) {
        DispatchQueue.global().async {
            guard let defaults = UserDefaults(suiteName: Constants.userDefaultSuite) else { completion([]); return }
            
            // Clear the cache if needed
            if let shouldClear = defaults.object(forKey: Constants.clearCacheKey) as? Bool, shouldClear {
                CacheManager.clear()
                defaults.set(false, forKey: Constants.clearCacheKey)
                defaults.set(false, forKey: Constants.hasLoadedData)
                defaults.synchronize()
                completion([]); return
            }
            
            guard let cacheData = defaults.object(forKey: Constants.userDefaultKey) as? Data,
                let recents = NSKeyedUnarchiver.unarchiveObject(with: cacheData) as? [Recent] else { completion([]); return }
            
            completion(recents); return
        }
        
    }
    
    class func clear() {
        let defaults = UserDefaults(suiteName: Constants.userDefaultSuite)
        defaults?.removeObject(forKey: Constants.userDefaultKey)
        defaults?.synchronize()
    }
    
    class func hasLoadedData() -> Bool{
        let defaults = UserDefaults(suiteName: Constants.userDefaultSuite)
        return defaults?.object(forKey: Constants.hasLoadedData) as? Bool ?? false
    }
}
