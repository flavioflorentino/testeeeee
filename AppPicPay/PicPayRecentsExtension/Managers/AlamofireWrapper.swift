//
//  AlamofireWrapper.swift
//  PicPay
//
//  Created by Lorenzo Piccoli on 5/5/17.
//
//

import Foundation
import Alamofire
import Core

// Custom error typealias
typealias NetworkResponseHandler = (AnyObject?, NSError?) -> Void

/*
 * This class is a wrapper around Alamofire's (get, post, put, ...) methods
 * The idea is to provide a place to handle the response, handle errors in a custom way
 * and log the requests for easier debugging
 */
class AlamofireWrapper: NSObject{
    
    //MARK: Response handler
    class func handleResponse(response: DataResponse<Any>, completion: NetworkResponseHandler){
        
        if let request = response.request{
            if let url = request.url{
                print("Requesting Server for: ", url.absoluteString)
            }
        }
        
        if let response = response.response{
            print("Got response: ", response.statusCode)
            
            if response.statusCode < 200 || response.statusCode > 299 {
                completion(nil, NSError(domain: "picpay", code: response.statusCode, userInfo: nil))
                return
            }
        }
        
        switch response.result {
        case .success(let resultData):
            print("Validation Successful")
            print("Result: ")
            
            print(resultData)
            
            completion(resultData as AnyObject, nil)
            
        case .failure(let error):
            print("Failed to request")
            print(error.localizedDescription)
            completion(nil, error as NSError)
            return
        }
    }
    
    //MARK: Method wrappers
    
    class func post(route:String,
                    headers:[String : String] = [:],
                    params:[String : Any] = [:],
                    completion: @escaping NetworkResponseHandler){
        
        Alamofire.request(AlamofireWrapper.getFormatterdUrl(route: route),
                          method: .post,
                          parameters: params,
                          encoding: JSONEncoding.default,
                          headers: headers).responseJSON(queue: DispatchQueue.global() )            { response in
                
                AlamofireWrapper.handleResponse(response: response, completion: completion)
        }
    }
    
    class func get(route:String,
                   headers:[String : String] = [:],
                   params:[String : Any] = [:],
                   completion: @escaping NetworkResponseHandler){
        
        Alamofire.request(AlamofireWrapper.getFormatterdUrl(route: route),
                          method: .get,
                          parameters: params,
                          encoding: URLEncoding.default,
                          headers: headers).responseJSON(queue: DispatchQueue.global())
            { response in
                
                AlamofireWrapper.handleResponse(response: response, completion: completion)
        }
    }
    
    class func put(route:String,
                   headers:[String : String] = [:],
                   params:[String : Any] = [:],
                   completion: @escaping NetworkResponseHandler){
        
        Alamofire.request(AlamofireWrapper.getFormatterdUrl(route: route),
                          method: .put,
                          parameters: params,
                          encoding: JSONEncoding.default,
                          headers: headers).responseJSON(queue: DispatchQueue.global())
            { response in
                
                AlamofireWrapper.handleResponse(response: response, completion: completion)
        }
    }
    
    class func delete(route:String,
                      headers:[String : String] = [:],
                      params:[String : Any] = [:],
                      completion: @escaping NetworkResponseHandler){
        
        Alamofire.request(AlamofireWrapper.getFormatterdUrl(route: route),
                          method: .delete,
                          parameters: params,
                          encoding: JSONEncoding.default,
                          headers: headers).responseJSON(queue: DispatchQueue.global())
            { response in
                
                AlamofireWrapper.handleResponse(response: response, completion: completion)
        }
    }
    
    //MARK: Helpers
    class func getFormatterdUrl(route: String) -> String {
        var bundle = Bundle.main
        if bundle.bundleURL.pathExtension == "appex" {
            // Peel off two directory levels - MY_APP.app/PlugIns/MY_APP_EXTENSION.appex
            let url = bundle.bundleURL.deletingLastPathComponent().deletingLastPathComponent()
            if let otherBundle = Bundle(url: url) {
                bundle = otherBundle
            }
        }
        guard let host = Environment.apiUrl else {
            return ""
        }
        
        return "\(host)\(route)"
    }
}
