import UI
import UIKit

@IBDesignable
class UIPPButton: UIButton {

    @IBInspectable var type:NSInteger = 0

    @IBInspectable var borderColor: UIColor = Palette.ppColorBranding300.color
    @IBInspectable var borderWidth: CGFloat = AppStyles.buttonBorderWidth()
    @IBInspectable var cornerRadius: CGFloat = AppStyles.buttonCornerRadius()

    @IBInspectable var leftIconPadding: CGFloat = 20.0

    @IBInspectable var tintImage: Bool = false

    @IBInspectable var normalTitleColor: UIColor = Palette.ppColorBranding300.color
    @IBInspectable var normalBackgrounColor: UIColor = Palette.ppColorGrayscale000.color

    @IBInspectable var disabledTitleColor: UIColor = Palette.ppColorBranding300.color
    @IBInspectable var disabledBackgrounColor: UIColor = Palette.ppColorGrayscale000.color
    @IBInspectable var disabledBorderColor: UIColor = Palette.ppColorGrayscale000.color

    @IBInspectable var highlightedImage: String = ""
    @IBInspectable var highlightedTitleColor: UIColor = Palette.ppColorGrayscale000.color
    @IBInspectable var highlightedBackgrounColor: UIColor = Palette.ppColorBranding300.color

    // MARK: - Private Properties

    private var activituIndicator: UIActivityIndicatorView?
    private var tempTitle: String?
    private var tempAttributedTitle: NSAttributedString?

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }

    func commonInit(){
        // to enable tintImage
        /*if tintImage {
         self.adjustsImageWhenHighlighted = false
         if let image = self.imageView?.image {
         self.setImage(image.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate), forState: UIControlState.Normal)
         }
         }*/
    }

    // MARK: Public Methods

    func startLoadingAnimating(style: UIActivityIndicatorView.Style = .white){
        activituIndicator?.removeFromSuperview()
        activituIndicator = UIActivityIndicatorView(style: style)
        activituIndicator?.center = CGPoint(x: frame.width / 2 , y: frame.height / 2 )
        addSubview(activituIndicator!)
        activituIndicator?.startAnimating()
        tempTitle = title(for: state)
        tempAttributedTitle = attributedTitle(for: state)

        setTitle("", for: state)
        setAttributedTitle(nil, for: state)
    }

    func stopLoadingAnimating() {
        activituIndicator?.removeFromSuperview()
        activituIndicator = nil
        setTitle(tempTitle, for: state)
        setAttributedTitle(tempAttributedTitle, for: state)
    }

    // MARK: Layout Methods
    func setupTintImage() {
        if tintImage {
            self.adjustsImageWhenHighlighted = false
            if let image = self.imageView?.image {
                self.setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
            }
        }
    }

    // MARK: - Layout Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        setupTintImage()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.cornerRadius = cornerRadius

        var frame = self.imageView?.frame
        frame?.origin.x = leftIconPadding
        self.imageView?.frame = frame!

        if type == 0 {
            if(self.state == .normal){
                self.titleLabel?.textColor = normalTitleColor
                self.backgroundColor = normalBackgrounColor
                if tintImage {
                    self.imageView?.tintColor = normalTitleColor
                }
            }
            if self.state == .highlighted {
                if highlightedImage != "" {
                    self.setImage(UIImage(named: highlightedImage), for: .highlighted)
                }
                self.titleLabel?.textColor = highlightedTitleColor
                self.backgroundColor = highlightedBackgrounColor
                if tintImage {
                    self.imageView?.tintColor = highlightedTitleColor
                }
            }
            if(self.state == .disabled){
                self.titleLabel?.textColor = disabledTitleColor
                self.layer.borderColor = disabledBorderColor.cgColor
                self.backgroundColor = disabledBackgrounColor
                if tintImage {
                    self.imageView?.tintColor = disabledTitleColor
                }
            }
        }

        if type == 1 {
            if self.state == .highlighted {
                self.titleLabel?.textColor = normalTitleColor
                self.backgroundColor = normalBackgrounColor
                if tintImage {
                    self.imageView?.tintColor = normalTitleColor
                }
            }
            if self.state == .normal {
                if highlightedImage != "" {
                    self.setImage(UIImage(named: highlightedImage), for: .highlighted)
                }
                self.titleLabel?.textColor = highlightedTitleColor
                self.backgroundColor = highlightedBackgrounColor
                if tintImage {
                    self.imageView?.tintColor = highlightedTitleColor
                }
            }
            if self.state == .disabled {
                self.titleLabel?.textColor = disabledTitleColor
                self.layer.borderColor = disabledBorderColor.cgColor
                self.backgroundColor = disabledBackgrounColor
                if tintImage {
                    self.imageView?.tintColor = disabledTitleColor
                }
            }
        }
    }
}
