//
//  ExtensionCollectionViewCell.swift
//  PicPay
//
//  Created by Lorenzo Piccoli on 5/3/17.
//
//

import Foundation
import UIKit

class ExtensionCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!

    override func prepareForReuse() {
        super.prepareForReuse()
        // Set the image to nil in case this cell is being reutilized
        profileImage.image = nil
        profileImage.cancelRequest()
        profileImage.image = UIImage(named: "avatar_person")
    }

    func configure(name: String?, imageUrl: String?, isStore: Bool?){
        // User label configuration
        nameLabel.numberOfLines = 1
        nameLabel.lineBreakMode = .byTruncatingTail
        nameLabel.text = name ?? ""

        profileImage.layer.cornerRadius = 24
        
        if let _ = isStore {
            nameLabel.numberOfLines = 0
            nameLabel.lineBreakMode = .byWordWrapping
        }
        profileImage.showActivityIndicator = true
        let url = URL(string: imageUrl ?? "")
        profileImage.setImage(url: url)
        
        profileImage.backgroundColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.00)
    }
    
}
