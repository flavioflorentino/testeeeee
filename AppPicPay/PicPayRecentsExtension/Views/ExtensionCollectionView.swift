//
//  ExtensionRecentsCollectionView.swift
//  PicPay
//
//  Created by Lorenzo Piccoli on 5/3/17.
//
//
import Foundation
import UIKit

class ExtensionCollectionView: UICollectionView {
    
    var results:[Recent] = []
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        commmonInit()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commmonInit()
    }
    
    func commmonInit(){
        self.dataSource = self

        let nibRecentCollectionCell = UINib(nibName: "ExtensionCollectionViewCell", bundle: Bundle.main)
        self.register(nibRecentCollectionCell, forCellWithReuseIdentifier: "cell")
        
        self.backgroundColor = UIColor.clear
        
        guard let layout = self.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        
        // ----- Default values: these are changed on setup() -----
        
        // Left and right padding
        layout.headerReferenceSize = CGSize(width: 5, height:0)
        layout.footerReferenceSize = CGSize(width: 5, height:0)
        
        layout.itemSize = CGSize(width: 75, height: 80)
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 10
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)

        // ----------
        
    }
    
    func setup(results: [Recent], screenWidth: CGFloat? = nil) {
        self.results = results
        
        if let layout = self.collectionViewLayout as? UICollectionViewFlowLayout, let screenWidth = screenWidth {
            // We want to evenly distribute the cells
            let numberOfItems:CGFloat = CGFloat(results.count > 4 ? 4 : results.count)
            let numberOfSpaces:CGFloat = 1 + numberOfItems
            let distanceBetweenItems = (screenWidth - (layout.itemSize.width * numberOfItems)) / numberOfSpaces
            
            let itemWidth = (screenWidth / 4) - 5
            layout.itemSize = CGSize(width: itemWidth, height: 80)
            layout.minimumInteritemSpacing = distanceBetweenItems
            layout.minimumLineSpacing = 20
            layout.sectionInset = UIEdgeInsets(top: 10, left: distanceBetweenItems, bottom: 20, right: distanceBetweenItems)
        }
        reloadData()
    }
}

// MARK: Data Source
extension ExtensionCollectionView: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return results.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = results[indexPath.row]

        guard
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? ExtensionCollectionViewCell else {
                return UICollectionViewCell()
        }
        
        cell.configure(name: data.name, imageUrl: data.image, isStore: data.type == .store)

        return cell
    }
    
    
}
