import Foundation
import UIKit

class Constants: NSObject {
    static let userDefaultSuite = "group.com.picpay.PicPay"
    static let userDefaultKey = "group.com.picpay.PicPay"
    static let clearCacheKey = "clearCache"
    static let hasLoadedData = "hasLoadedData"
}
