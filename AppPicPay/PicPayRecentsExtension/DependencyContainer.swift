import Foundation
import Core

typealias RecentsExtensionDependencies = HasNoDependency & HasMainQueue & HasKeychainManager

final class DependencyContainer: RecentsExtensionDependencies {
    lazy var mainQueue: DispatchQueue = .main
    lazy var keychain: KeychainManagerContract = KeychainManager()
}
