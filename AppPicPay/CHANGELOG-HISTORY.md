<!--- This is a history changelog prior to git generated changelog. -->
<!--- !!! DO NOT USE THIS FILE !!! -->

### Added
- **Bills:** [BILL-XXX] Adiciona os arquivos .profraw ao .gitignore ([#3089](https://github.com/PicPay/picpay-ios/issues/3089))
- **CARD:** [PCO-270] Criação da regra para mostrar o banner de tracking do Card ([#3094](https://github.com/PicPay/picpay-ios/issues/3094))
- **CARD:** [PCO-281] Criado tela de loading do início do fluxo ([#3113](https://github.com/PicPay/picpay-ios/issues/3113))
- **CRED:** [CRED-817] Adicionar o fluxo de cadastro do card no novo fluxo de crédito pessoal ([#3119](https://github.com/PicPay/picpay-ios/issues/3119))
- **CRED:** Criação da arquitetura inicial da tela de erro de horario ([#3115](https://github.com/PicPay/picpay-ios/issues/3115))
- **Card XP:** [PCX-461] Tela 'cartão bloqueado' - Regra para exibir opção de 'solicitar nova via' ([#3103](https://github.com/PicPay/picpay-ios/issues/3103))
- **Card-Offer:** [PCO-286] Criação do fluxo do Tracking do Card ([#3145](https://github.com/PicPay/picpay-ios/issues/3145))
- **Card-Offer:** [PCO-273] Tela "Seu Picpay Card chegou!" ([#3098](https://github.com/PicPay/picpay-ios/issues/3098))
- **Card-Offer:** [PCO-272] Adiciona componente de card tracking ([#3100](https://github.com/PicPay/picpay-ios/issues/3100))
- **ContaPF:** [CPF-104] Deeplink para a Renovação Cadastral ([#3120](https://github.com/PicPay/picpay-ios/issues/3120))
- **ContaPF:** [CPF-109] Corrige parse e formato dos dados enviados no fluxo de renovação cadastral ([#3130](https://github.com/PicPay/picpay-ios/issues/3130))
- **ContaPF:** [CPF-87] Exibir popup de alerta ao sair do fluxo de trocar telefone/email da renovação cadastral ([#3092](https://github.com/PicPay/picpay-ios/issues/3092))
- **ContaPF:** [CPF-102] Ofuscar view com as informações iniciais do fluxo de renovação cadastral ([#3104](https://github.com/PicPay/picpay-ios/issues/3104))
- **ContaPF:** [CPF-99] Animação de sucesso ao alterar telefone/email na renovação cadastral ([#3087](https://github.com/PicPay/picpay-ios/issues/3087))
- **Engajamento:** [EN-253] Tracking para Dark Mode ([#3105](https://github.com/PicPay/picpay-ios/issues/3105))
- **Engajamento:** [EN-270] Mostra o item de Pagar pessoas ([#3096](https://github.com/PicPay/picpay-ios/issues/3096))
- **P2PLending:** [P2PLEN-209] Adicionando testes funcionais da tela de oportunidade ([#3072](https://github.com/PicPay/picpay-ios/issues/3072))
- **P2PLending:** [P2PLEN-228] Adicionar deep link da notificação de oferta ([#3112](https://github.com/PicPay/picpay-ios/issues/3112))
- **P2PLending:** [P2PLEN-252] Adiciona "aura" a ilustração na tela de proposta enviada ([#3127](https://github.com/PicPay/picpay-ios/issues/3127))
- **pagamento:** [P2P-595] Implementa trackings da busca de usuário e pequenos ajustes ([#3082](https://github.com/PicPay/picpay-ios/issues/3082))
- **plataforma:** [PLAIOS-171] Monorepo - Importação do repositorio business-ios  ([#3128](https://github.com/PicPay/picpay-ios/issues/3128))
- **universitarios:** [UNI-310] Oferta de ContaUni no cadastro ([#2998](https://github.com/PicPay/picpay-ios/issues/2998))
- **user-security:** [US-504] Adiciona cena de loading no modulo de validação de identidade ([#3081](https://github.com/PicPay/picpay-ios/issues/3081))

### Changed
- **APPSEC:** [APPSEC-XXXX] Migrando os métodos de máscara de ofuscação para o Módulo de UI ([#3111](https://github.com/PicPay/picpay-ios/issues/3111))
- **Bills:** [BILL-XXX] Move extension de print do Módulo do App para o Core ([#3080](https://github.com/PicPay/picpay-ios/issues/3080))
- **CRED:** Abrindo FAQ com deeplink em vez de uma web view  ([#3108](https://github.com/PicPay/picpay-ios/issues/3108))
- **CRED:** Correções de layout e melhorias de código ([#3083](https://github.com/PicPay/picpay-ios/issues/3083))
- **Card-XP:** [PCX-443][PCX-450] Fluxo de bloqueio - alterações para adicionar fluxo de 'nova via' ([#3079](https://github.com/PicPay/picpay-ios/issues/3079))
- **ContaPF:** [CPF-91] Corrige parse e popups de erro no fluxo de renovação cadastral ([#3097](https://github.com/PicPay/picpay-ios/issues/3097))
- **ContaPF:** [CPF-58] Alterar o botão voltar do fluxo de cadastro ([#3042](https://github.com/PicPay/picpay-ios/issues/3042))
- **MOB:** [JIRA-XXX] Ajuste no arquivo de Localizables ([#3091](https://github.com/PicPay/picpay-ios/issues/3091))
- **P2PLending:** [P2PLEN-229] Ajustes no cenário de erro para seleção de amigo ([#3085](https://github.com/PicPay/picpay-ios/issues/3085))
- **beneficios:** [JIRA-XXX] Atualiza decorator do Style Rounded ([#3095](https://github.com/PicPay/picpay-ios/issues/3095))
- **user-security:** [US-513] Ajustes na cena de introdução da biometria ([#3102](https://github.com/PicPay/picpay-ios/issues/3102))

### Fixed
- **APPSEC:** [APPSEC-323] Tratamento para a ofuscação quando se abre o deeplink ([#3101](https://github.com/PicPay/picpay-ios/issues/3101))
- **Bills:** [BILL-412] Corrige bug de layout na tela de transação de boleto ([#3106](https://github.com/PicPay/picpay-ios/issues/3106))
- **ExpAtendimento:** [XPAT-XXX] Arruma o estilo do body da tela e adiciona a função para baixar o teclado no toque  ([#3114](https://github.com/PicPay/picpay-ios/issues/3114))
- **MGM:** [MGM-346] Fix Imagem de Indicação. ([#3109](https://github.com/PicPay/picpay-ios/issues/3109))
- **Squad:** [PCIDEV-746] Fix Back Button Renovação Cadastral. ([#3099](https://github.com/PicPay/picpay-ios/issues/3099))
- **pagamento:** [P2P-755] Corrige texto no recibo de p2p ([#3136](https://github.com/PicPay/picpay-ios/issues/3136))
- **plataforma:** ajustes no testes que estavam falhando

### Removed
- **plataforma:** [PLAIOS-192] remove firebase performance ([#3132](https://github.com/PicPay/picpay-ios/issues/3132))


<a name="v10.19.9"></a>
## [v10.19.9] - 2020-07-01
### Added
- **CRED:** [CRED-891] Adicionando FAQ na tela de cadastro negado ([#3044](https://github.com/PicPay/picpay-ios/issues/3044))
- **Card-Offer:** [PCO-259] Flow Coordinator do Tracking ([#3036](https://github.com/PicPay/picpay-ios/issues/3036))
- **Card-Offer:** [PCO-247] Refactor da tela de Dados Adicionais ([#2997](https://github.com/PicPay/picpay-ios/issues/2997))
- **ContaPF:** [CPF-75] Implementar deeplink para a carteira ([#3065](https://github.com/PicPay/picpay-ios/issues/3065))
- **ContaPF:** [CPF-56] Lógica da tela de validar código do fluxo de renovação cadastral ([#3066](https://github.com/PicPay/picpay-ios/issues/3066))
- **Engajamento:** [EN-199] Tracking para popup ([#3024](https://github.com/PicPay/picpay-ios/issues/3024))
- **Engajamento:** [EN-267] Tracking para novas cores da aba pagar ([#3043](https://github.com/PicPay/picpay-ios/issues/3043))
- **ExpAtendimento:** [XPAT-XXX] Adiciona substituições que faltavam do HelpCenter ([#3060](https://github.com/PicPay/picpay-ios/issues/3060))
- **ExpAtendimento:** [XPAT-243]  Ajustar apresentação das telas do CustomerSupport  ([#3051](https://github.com/PicPay/picpay-ios/issues/3051))
- **ExpAtendimento:** [XPAT-193] Substitui o HelpShift e HelpCenter pelo CustomerSupportModule ([#3027](https://github.com/PicPay/picpay-ios/issues/3027))
- **Feature Management:** Adiciona suporte ao MQTT ([#3012](https://github.com/PicPay/picpay-ios/issues/3012))
- **MGM:** [MGM-279] Final da tela de Indicar Contato ([#3013](https://github.com/PicPay/picpay-ios/issues/3013))
- **MGM:** [MGM-279] Tela de Indicações Regras de Negócio  ([#3046](https://github.com/PicPay/picpay-ios/issues/3046))
- **MGM:** [MGM-279]  Integração da Features do MGM na Paginação ([#3059](https://github.com/PicPay/picpay-ios/issues/3059))
- **MGM:** [MGM-279] Tela de Indicações ([#3014](https://github.com/PicPay/picpay-ios/issues/3014))
- **MOB:** [MOB-115] - Integração do fluxo de recarga Metro Rio ([#3070](https://github.com/PicPay/picpay-ios/issues/3070))
- **P2PLending:** [P2PLEN-230] Adicionando estado de loading na tela de valor da proposta ([#3057](https://github.com/PicPay/picpay-ios/issues/3057))
- **P2PLending:** [P2PLEN-208] Camada de lógica da tela de resumo da oportunidade ([#2995](https://github.com/PicPay/picpay-ios/issues/2995))
- **P2PLending:** [P2PLEN-231] Adicionando labels com informações sobre parcelas e juros na tela de condições ([#3058](https://github.com/PicPay/picpay-ios/issues/3058))
- **P2PLending:** [P2PLEN-232] Adicionar modal de ajuda na tela de condições da proposta ([#3061](https://github.com/PicPay/picpay-ios/issues/3061))
- **PCI:** [PCIDEV-735] Permitir mais entrada de status Code para Error ([#3075](https://github.com/PicPay/picpay-ios/issues/3075))
- **PLAIOS:** [PLAIOS-186] Criação de flag no PCI nas transações do PAV ([#3063](https://github.com/PicPay/picpay-ios/issues/3063))
- **Plataforma:** [PLAIOS-184]  Criar feature flag no PCI ([#3026](https://github.com/PicPay/picpay-ios/issues/3026))
- **VMPay:** [SAPI-385] Adiciona endpoint do websocket de produção e faz ajustes finais ao projeto ([#3062](https://github.com/PicPay/picpay-ios/issues/3062))
- **engajamento_pj:** [ENPJ-73] Trata o hash de deeplink de pagamento ([#2881](https://github.com/PicPay/picpay-ios/issues/2881))
- **pagamento:** [P2P-674] Adiciona view de permissão de contatos na busca de usuários da cobrança ([#3015](https://github.com/PicPay/picpay-ios/issues/3015))
- **pagamento:** [P2P-731] Lógica detalhamento feed cobrança ([#3049](https://github.com/PicPay/picpay-ios/issues/3049))
- **plataforma:** [PLAIOS-157] Melhorias na SearchTab e FeatureManager com JSONs ([#2972](https://github.com/PicPay/picpay-ios/issues/2972))
- **user-security:** [US-483] Adiciona Alerta de Permissão de câmera. ([#3030](https://github.com/PicPay/picpay-ios/issues/3030))

### Changed
- **BACEN:** [BD-1332] Pedir senha para exibir número do processo, Bloqueio Judicial ([#3038](https://github.com/PicPay/picpay-ios/issues/3038))
- **Card-XP:** [PCX-389] Atualiza tela de configurar cartão ao sair de um fluxo ([#3055](https://github.com/PicPay/picpay-ios/issues/3055))
- **Engajamento:** [EN-268] Ajuste do tracking quando excede limite (R$800,00) de pagamento. ([#3054](https://github.com/PicPay/picpay-ios/issues/3054))
- **Engajamento:** [EN-251] Muda lógica dos cards de Oportunidades no feed quando o usuário não tem atividade ([#3041](https://github.com/PicPay/picpay-ios/issues/3041))
- **Engajamento:** [EN-269] Tracking duplicado ([#3053](https://github.com/PicPay/picpay-ios/issues/3053))
- **ExpAtendimento:** [XPAT-189] Substitui pelo 'ChatBot' a chamada do HelpShift/HelpCenter da tela de login ([#3071](https://github.com/PicPay/picpay-ios/issues/3071))
- **ExpAtendimento:** [XPAT-223] Melhora o comportamento da tela de ChatBot ([#3064](https://github.com/PicPay/picpay-ios/issues/3064))
- **P2PLending:** [P2PLEN-225] Ajustes na tela de apresentação do fluxo de pedir dinheiro emprestado ao amigo ([#3040](https://github.com/PicPay/picpay-ios/issues/3040))
- **pagamento:** [P2P-674] Pequenas correções da busca de usuários ([#3050](https://github.com/PicPay/picpay-ios/issues/3050))
- **plataforma:** [PLAIOS-XXX] update de pods e também de gems e generates ([#3039](https://github.com/PicPay/picpay-ios/issues/3039))

### Fixed
- **DesignSystem:** Corrige suporte dos estilos para o Swift 5.3 ([#3067](https://github.com/PicPay/picpay-ios/issues/3067))
- **Feature Management:** Correção dos valores padrões do .plist ([#3076](https://github.com/PicPay/picpay-ios/issues/3076))
- **pagamento:** [P2P] Corrige crash em cálculo da altura da célula no TableViewHandler ([#3069](https://github.com/PicPay/picpay-ios/issues/3069))
- **plataforma:** [PLAIOS-189] adicionado propriedades de usuário no envio padrão do evento do modulo de Analytics ([#3084](https://github.com/PicPay/picpay-ios/issues/3084))
- **plataforma:** [PLAIOS-187] ajusta evento do dynamic deeplink do firebase ([#3052](https://github.com/PicPay/picpay-ios/issues/3052))
- **plataforma:** Adiciona dependencia do CustomerSupport ao FeatureFlag no SPM ([#3056](https://github.com/PicPay/picpay-ios/issues/3056))
- **plataforma:** Adiciona arquivos `default.profraw` ao .gitignore [#3045](https://github.com/PicPay/picpay-ios/issues/3045)

### Removed
- **Engajamento:** [EN-247] Remoção de tracking ([#3048](https://github.com/PicPay/picpay-ios/issues/3048))
- **plataforma:** [PLAIOS-146, PLAIOS-188] remove fluxo antigo de cadastro e edição de conta bancária e ajuste de bug ao selecionar conta ([#3073](https://github.com/PicPay/picpay-ios/issues/3073))


<a name="v10.19.8"></a>
## [v10.19.8] - 2020-06-23
### Added
- **BACEN:** [BD-1356] Suporte para challange PAV, Boleto, P2P ([#3025](https://github.com/PicPay/picpay-ios/issues/3025))
- **Bacen:** [BD-1302] Adicionar Request ao final de cada passo ([#2966](https://github.com/PicPay/picpay-ios/issues/2966))
- **Bacen:** [BD-1303] Fazer integração com tela de endereço e tela de sucesso. ([#3000](https://github.com/PicPay/picpay-ios/issues/3000))
- **Bacen:** [BD-1333] Escrita de testes telas a antigas bacen ([#3010](https://github.com/PicPay/picpay-ios/issues/3010))
- **CRED:** [CRED-879] Adicionando testes na tela de onboarding do Cadastro ([#3004](https://github.com/PicPay/picpay-ios/issues/3004))
- **Card:** [PCO-206] Criação de teste a/b para oferta do Card ([#2999](https://github.com/PicPay/picpay-ios/issues/2999))
- **Card-XP:** [PCX-356] Tela configuração do cartão - feature flag da função 'nova via do cartão' ([#3007](https://github.com/PicPay/picpay-ios/issues/3007))
- **CardXP:** [PCX-385] Tela configuração do cartão - implementa ação do botão "nova via solicitada" ([#2979](https://github.com/PicPay/picpay-ios/issues/2979))
- **ContaPF:** [CPF-53] Layout da tela de confirmar código de telefone/email do fluxo de renovação cadastral ([#3006](https://github.com/PicPay/picpay-ios/issues/3006))
- **ContaPF:** [CPF-50] Lógica da tela de alterar telefone/email no fluxo de renovação cadastral ([#2989](https://github.com/PicPay/picpay-ios/issues/2989))
- **ContaPJ:** [COPJ-479] Adiciona completion de willDisplayCell no TableViewHandler ([#2983](https://github.com/PicPay/picpay-ios/issues/2983))
- **DesignSystem:** [DSGN-701] Adiciona suporte a estilos de avatares ([#3008](https://github.com/PicPay/picpay-ios/issues/3008))
- **DesignSystem:** [DSGN-759] Adiciona suporte de registro de font  ([#3031](https://github.com/PicPay/picpay-ios/issues/3031))
- **DesignSystem:** [DSGN-758] Ajustes de cores de background e personalização ([#3022](https://github.com/PicPay/picpay-ios/issues/3022))
- **Engajamento:** [EN-207] (Experimento) Novas cores no Search ([#3001](https://github.com/PicPay/picpay-ios/issues/3001))
- **ExpAtendimento:** [XPAT-191] Registra a chave de acesso para notificação do Zendesk ([#3019](https://github.com/PicPay/picpay-ios/issues/3019))
- **ExpAtendimento:** [XPAT-141] Cria a tela de requisição de ticket ([#3018](https://github.com/PicPay/picpay-ios/issues/3018))
- **ExpAtendimento:** [XPAT-137] Adiciona a tela da lista de assuntos ([#3017](https://github.com/PicPay/picpay-ios/issues/3017))
- **MGM:** [MGM-279] Componente de Paginação ([#2967](https://github.com/PicPay/picpay-ios/issues/2967))
- **MGM:** [MGM-279] Regras de Negocio Tela de Indicar Contatos ([#2981](https://github.com/PicPay/picpay-ios/issues/2981))
- **P2PLending:** [P2PLEN-177] Testes funcionais da tela de resumo da proposta ([#2987](https://github.com/PicPay/picpay-ios/issues/2987))
- **P2PLending:** [P2PLEN-207] Camada de view da tela de oportunidade ([#2957](https://github.com/PicPay/picpay-ios/issues/2957))
- **P2PLending:** [P2PLEN-104] Testes funcionais da tela de proposta enviada ([#2986](https://github.com/PicPay/picpay-ios/issues/2986))
- **StoreDesign:** [VSD-4] Adicionado eventos de analytics ao fluxo de recarga ([#2994](https://github.com/PicPay/picpay-ios/issues/2994))
- **pagamento:** [P2P-730] Estrutura inicial da tela de detalhamento do feed do cobrador ([#3011](https://github.com/PicPay/picpay-ios/issues/3011))
- **plataforma:** [PLAIOS-166] implementação do modulo CoreSentinel junto com api_error para o New Relic.  ([#2971](https://github.com/PicPay/picpay-ios/issues/2971))
- **user-security:** [US-493] Adiciona uma flag única para o fluxo de validação de identidade ([#3005](https://github.com/PicPay/picpay-ios/issues/3005))
- **user-security:** [US-494]: Adiciona termos de uso na cena inicial da validação de identidade ([#3023](https://github.com/PicPay/picpay-ios/issues/3023))

### Changed
- **Bills:** [BILL-369] Troca o alerta de sistema para o popup novo para erro de verificação de boleto ([#2985](https://github.com/PicPay/picpay-ios/issues/2985))
- **CRED:** Adicionando estados de cadastro completo ou incompleto na tela de onboarding ([#2978](https://github.com/PicPay/picpay-ios/issues/2978))
- **Card-XP:** [PCX-380] Altera regra de visualização da opção 'Bloquear cartão' e se o cartão bloqueado ([#2984](https://github.com/PicPay/picpay-ios/issues/2984))
- **ContaPF:** [CPF-49] Fazer a chamada dos favoritos somente quando a aba for ativada ([#2975](https://github.com/PicPay/picpay-ios/issues/2975))
- **Engajamento:** [EN-223] Limita items no carrossel de oportunidades ([#2988](https://github.com/PicPay/picpay-ios/issues/2988))
- **Engajamento:** [EN-222] Muda layout dos cards de Oportunidade no Feed ([#2969](https://github.com/PicPay/picpay-ios/issues/2969))
- **ExpAtendimento:** [XPAT-112] Refatora o CustomerSupport. ([#2961](https://github.com/PicPay/picpay-ios/issues/2961))
- **ExpAtendimento:** [XPAT-135] Mudar comportamento do ação de criação de ticket ([#2970](https://github.com/PicPay/picpay-ios/issues/2970))
- **pagamento:** [P2P] Correções na cobrança V2
- **user-security:** [US-482] Altera ícones e textos no fluxo de validação de identidade ([#2996](https://github.com/PicPay/picpay-ios/issues/2996))

### Fixed
- **Bills:** [BILL-400] Corrige eventos da timeline de boleto ([#3016](https://github.com/PicPay/picpay-ios/issues/3016))
- **ExpAtendimento:** [XPAT_XXX] Arruma o problema da perda de instancia do wrapper no CustomerSupportModule ([#3033](https://github.com/PicPay/picpay-ios/issues/3033))
- **PLAIOS:** Ajuste para voltar .generated do CustomerSupport para o git ([#3034](https://github.com/PicPay/picpay-ios/issues/3034))
- **expansao:** [PLAIOS-XXX] ajusta a propriedade de usuário que pega do data ([#3009](https://github.com/PicPay/picpay-ios/issues/3009))
- **user-security:** [US-438] Ajusta mensagens de instruções para a validação do TFA ([#2976](https://github.com/PicPay/picpay-ios/issues/2976))
- **user-security:** [US-476] Corrige alerta de erro exibido na validação de TFA. ([#2977](https://github.com/PicPay/picpay-ios/issues/2977))

### Removed
- **Engajamento:** [EN-237] (Experimento) Remoção da label do botão principal da tabbar ("Pagar") ([#3003](https://github.com/PicPay/picpay-ios/issues/3003))
- **Engajamento:** [EN-250] (Experimento) Remoção da seção "sugestões para você" ([#3002](https://github.com/PicPay/picpay-ios/issues/3002))


<a name="v10.19.7"></a>
## [v10.19.7] - 2020-06-16
### Added
- **AtivaçãoPJ:** [B2B-669] Adiciona mascaras de DDD, telefone e celular no componente de UI ([#2941](https://github.com/PicPay/picpay-ios/issues/2941))
- **Bacen:** [BD-1290] Criar entrada do fluxo de renovação cadastral no Settings ([#2948](https://github.com/PicPay/picpay-ios/issues/2948))
- **CRED:** Criação do fluxo completo da tela de status pendente do cadastro ([#2959](https://github.com/PicPay/picpay-ios/issues/2959))
- **CRED:** Criando arquitetura das duas novas de tela de status pendente e onboarding ([#2958](https://github.com/PicPay/picpay-ios/issues/2958))
- **CRED:** Criação da tela completa de onboarding do cadastro ([#2960](https://github.com/PicPay/picpay-ios/issues/2960))
- **CRED:** [CRED-817] Injeção do fluxo de cadastro de crédito no módulo Loan ([#2955](https://github.com/PicPay/picpay-ios/issues/2955))
- **Card-Offer:** [PCO-247] Refactor da tela de Dados Adicionais (View) ([#2964](https://github.com/PicPay/picpay-ios/issues/2964))
- **Card-XP:** [PCX-355] Tela nova via solicitada - Estado de erro e carregando ([#2952](https://github.com/PicPay/picpay-ios/issues/2952))
- **ContaPF:** [CPF-46] Layout da tela de trocar o número de telefone/email no fluxo de renovação cadastral ([#2956](https://github.com/PicPay/picpay-ios/issues/2956))
- **Engajamento:** [EN-226] Novos ícones tabbar e navigationbar ([#2947](https://github.com/PicPay/picpay-ios/issues/2947))
- **MGM:** [MGM-279] Segunda Parte da tela de Convidar ([#2962](https://github.com/PicPay/picpay-ios/issues/2962))
- **MGM:** [MGM-279] Tela de Indicar Contatos (View) ([#2922](https://github.com/PicPay/picpay-ios/issues/2922))
- **MGM:** [MGM-342] Eventos da tela de Indique e ganhe ([#2965](https://github.com/PicPay/picpay-ios/issues/2965))
- **P2PLending:** [P2PLEN-176] Camada de lógica e testes unitários da tela de resumo da proposta ([#2946](https://github.com/PicPay/picpay-ios/issues/2946))
- **pagamento:** [P2P-676] Altera regras de paginação e implementa empty state na busca de usuários ([#2953](https://github.com/PicPay/picpay-ios/issues/2953))

### Changed
- **Bills:** [BILL-XXX] Refaz o BilletSetup e move os contratos de injeção para o Core ([#2944](https://github.com/PicPay/picpay-ios/issues/2944))
- **CRED:** Adicionando estados na tela de termos ([#2963](https://github.com/PicPay/picpay-ios/issues/2963))
- **CRED:** Alterando link do resumo contratual que é estatico e aparece na tela de termos ([#2954](https://github.com/PicPay/picpay-ios/issues/2954))
- **Card-Offer:** [PCO-227] Modifica o texto de aceite dos termos incluindo os links de contrato do multiplo e débito ([#2936](https://github.com/PicPay/picpay-ios/issues/2936))
- **Card-XP:** [PCX-248] Refatora tela de bloqueio do cartão ([#2882](https://github.com/PicPay/picpay-ios/issues/2882))
- **Engajamento:** [EN-190] Muda dados enviados ao Analytics do Bottom Sheet ([#2939](https://github.com/PicPay/picpay-ios/issues/2939))
- **Housekeeping:** Alterações de design system, correções e melhorias ([#2924](https://github.com/PicPay/picpay-ios/issues/2924))

### Fixed
- **CARD:** [PCO-222] Unificado fluxo de desbloqueio do cartão ([#2945](https://github.com/PicPay/picpay-ios/issues/2945))
- **ContaPF:** [CPF-26]  Corrigir a aparência de erro dos campos de texto em algumas telas do cadastro ([#2937](https://github.com/PicPay/picpay-ios/issues/2937))
- **card-offer:** [PCO-207] Mudado nome do evento que estava errado ([#2951](https://github.com/PicPay/picpay-ios/issues/2951))
- **plataforma:** [PLAIOS-XXX] ajusta a propriedade de usuário que pega do consumer ([#2968](https://github.com/PicPay/picpay-ios/issues/2968))
- **user-security:** [US-467] Corrige popup de error no carregamento das opções de TFA ([#2931](https://github.com/PicPay/picpay-ios/issues/2931))


<a name="v10.19.6"></a>
## [v10.19.6] - 2020-06-10
### Added
- **Bacen:** [BD-1262] Tela Generica Recadastro parte III ([#2910](https://github.com/PicPay/picpay-ios/issues/2910))
- **Bacen:** [BD-1263] Fazer Integração com tela de endereço. ([#2938](https://github.com/PicPay/picpay-ios/issues/2938))
- **Bacen:** [BD-1262]  Criar tela de Profissão, Renda e Patrimônio - Parte II ([#2884](https://github.com/PicPay/picpay-ios/issues/2884))
- **CRED:** [CRED-737] Biometria Facial: Envio da imagem ([#2904](https://github.com/PicPay/picpay-ios/issues/2904))
- **Card-Offer:** [PCO-193] Adiciona analytics do fluxo de solicitação do PicPay Card ([#2912](https://github.com/PicPay/picpay-ios/issues/2912))
- **Card-Offer:** [PCO-244] Mudanças visuais no Onboarding do Crédito ([#2923](https://github.com/PicPay/picpay-ios/issues/2923))
- **Card-XP:** [PCX-371] Tela Nova via solicitada - Implementação da função FAQ ([#2940](https://github.com/PicPay/picpay-ios/issues/2940))
- **Card-XP:** [PCX-268] Tela nova via solicitada ([#2893](https://github.com/PicPay/picpay-ios/issues/2893))
- **Card-XP:** [PCX-255] Tela configuração do cartão - adiciona botão de nova via ([#2890](https://github.com/PicPay/picpay-ios/issues/2890))
- **ContaPF:** [CPF-35] Implementa layout para o formulário de renovação cadastral ([#2898](https://github.com/PicPay/picpay-ios/issues/2898))
- **ContaPF:** [CPF-39] Implementa a lógica do formulário de renovação cadastral ([#2921](https://github.com/PicPay/picpay-ios/issues/2921))
- **DesignSystem:** [DSGN-489] Adiciona suporte a icons nos estilos ([#2901](https://github.com/PicPay/picpay-ios/issues/2901))
- **DesignSystem:** [DSGN-526] Adiciona Icons e Buttons no UISample ([#2895](https://github.com/PicPay/picpay-ios/issues/2895))
- **Engajamento:** [EN-208] Cores da tela inicial - Sugestão + feed ([#2927](https://github.com/PicPay/picpay-ios/issues/2927))
- **Engajamento:** [EN-230] Deeplink genérico para sub abas do search ([#2915](https://github.com/PicPay/picpay-ios/issues/2915))
- **Engajamento:** [EN-56] Melhora o pedido de permissão para Contatos ([#2918](https://github.com/PicPay/picpay-ios/issues/2918))
- **P2PLending:** [P2PLEN-175] Tela de resumo da proposta (camada de view apenas) ([#2905](https://github.com/PicPay/picpay-ios/issues/2905))
- **P2PLending:** [P2PLEN-93] Tela de proposta enviada com sucesso ([#2932](https://github.com/PicPay/picpay-ios/issues/2932))
- **PCI:** [PCIDEV-683] Implementar evento na tela de abrir cvv ([#2920](https://github.com/PicPay/picpay-ios/issues/2920))
- **PCI:** [PCIDEV-XX1] Atualizar Header para ficar conforme Android ([#2909](https://github.com/PicPay/picpay-ios/issues/2909))
- **pagamento:** [P2P-688] Lógica de negócio na nova tela de valor da cobrança ([#2885](https://github.com/PicPay/picpay-ios/issues/2885))
- **pagamento:** [P2P-672] Implementa paginação na busca de usuários ([#2903](https://github.com/PicPay/picpay-ios/issues/2903))
- **user-security:** [US-463] Adiciona analytics para tela de preview de foto de documento ([#2919](https://github.com/PicPay/picpay-ios/issues/2919))

### Changed
- **APPSEC:** [HouseKeeping] Atualizando o comando do make setup na doc ([#2914](https://github.com/PicPay/picpay-ios/issues/2914))
- **Bills:** [BILL-XXX] Renomeia módulo Bills para Billet ([#2908](https://github.com/PicPay/picpay-ios/issues/2908))
- **CRED:** Alterando aceite para ter dois links de contratos  ([#2913](https://github.com/PicPay/picpay-ios/issues/2913))
- **DesignSystem:** [DSGN-697] Altera forma de analise do background em modal ([#2900](https://github.com/PicPay/picpay-ios/issues/2900))
- **user-security:** [US-416] Remodela o coordinator de validação de identidade ([#2916](https://github.com/PicPay/picpay-ios/issues/2916))
- **user-security:** [US-449] Modifica os textos da cena de introdução de captura de documentos para estáticos ([#2934](https://github.com/PicPay/picpay-ios/issues/2934))

### Fixed
- **Bills:** [BILL-319] Atualiza instruções baseado no json de boleto de concessionária ([#2896](https://github.com/PicPay/picpay-ios/issues/2896))
- **CRED:** [CRED-790] Revisando analytics e corrigindo testes de UI ([#2897](https://github.com/PicPay/picpay-ios/issues/2897))
- **ContaPF:** [CPF-45] Corrige o texto da tela de cadastro de username ([#2933](https://github.com/PicPay/picpay-ios/issues/2933))
- **ContaPF:**  [CPF-44] Corrige teste unitário quebrando quando a feature flag de passos do cadastro é alterada ([#2929](https://github.com/PicPay/picpay-ios/issues/2929))
- **MGM:** [MGM-288] Alterados alguns campos para opcionais no modelo ([#2943](https://github.com/PicPay/picpay-ios/issues/2943))
- **card-offer:** [PCO-249] Remover analytics do fluxo de desbloqueio ([#2928](https://github.com/PicPay/picpay-ios/issues/2928))
- **plataforma:** [PLAIOS-164] Corrige a geração do changelog ([#2899](https://github.com/PicPay/picpay-ios/issues/2899))

### Removed
- **ContaPF:** [CPF-36] Remover feature flag do cashin no onboarding ([#2930](https://github.com/PicPay/picpay-ios/issues/2930))
- **ContaPF:** [CPF-40] Remover feature flag que desabilita o evento de scroll no feed ([#2917](https://github.com/PicPay/picpay-ios/issues/2917))
- **ContaPF:** [CPF-42] Remover feature flag que mostra pins da Cielo no mapa ([#2925](https://github.com/PicPay/picpay-ios/issues/2925))
- **user-security:** [US-456] Remove fluxo antigo da validação de TFA ([#2911](https://github.com/PicPay/picpay-ios/issues/2911))


<a name="v10.19.5"></a>
## [v10.19.5] - 2020-06-05
### Fixed
- **Bills:** [BILL-XXX] Hotfix para concertar mudança na notificação p2pComments


<a name="v10.19.4"></a>
## [v10.19.4] - 2020-06-03
### Added
- **Bacen:** [BD-XX1] Tela de Renovação para Profissão, Renda e Patrimônio -> Parte 1 ([#2868](https://github.com/PicPay/picpay-ios/issues/2868))
- **Bills:** [BILL-17] Implementa os testes unitários para novo detalhe do feed ([#2892](https://github.com/PicPay/picpay-ios/issues/2892))
- **Bills:** [BILL-17] Implementa últimos passos para a feature de transação ([#2861](https://github.com/PicPay/picpay-ios/issues/2861))
- **CARD-OFFER:** [PCO-173] Eventos do múltiplo ([#2867](https://github.com/PicPay/picpay-ios/issues/2867))
- **CRED:** [CRED-737] Biometria Facial: Envio da Foto ([#2865](https://github.com/PicPay/picpay-ios/issues/2865))
- **CRED:** [CRED-765] Dupla confirmação na hora da contratação ([#2856](https://github.com/PicPay/picpay-ios/issues/2856))
- **Card-Offer:** [PCO-192] Eventos do múltiplo pt. 2 ([#2872](https://github.com/PicPay/picpay-ios/issues/2872))
- **Card-XP:** [PCX-268] Tela Nova Via Solicitada - Parte2  ([#2849](https://github.com/PicPay/picpay-ios/issues/2849))
- **ContaPF:** [CPF-19] Cria a tela inicial de apresentação da renovação cadastral ([#2863](https://github.com/PicPay/picpay-ios/issues/2863))
- **ContaPF:** [CPF-28] Cria testes unitários para a tela inicial de atualização cadastral ([#2878](https://github.com/PicPay/picpay-ios/issues/2878))
- **Engajamento:** [EN-191] Ajustes no Tour do App ([#2875](https://github.com/PicPay/picpay-ios/issues/2875))
- **Engajamento:** [EN-73] Tracking permissão de localização das promoções locais ([#2866](https://github.com/PicPay/picpay-ios/issues/2866))
- **MGM:** [MGM-288] Tela de Indique e ganhe pt. 2 ([#2874](https://github.com/PicPay/picpay-ios/issues/2874))
- **MGM:** [MGM-288] Tela de Indique e ganhe Regulamento e Toggle ([#2886](https://github.com/PicPay/picpay-ios/issues/2886))
- **MGM:** [MGM-288] Indique e ganhe ([#2847](https://github.com/PicPay/picpay-ios/issues/2847))
- **P2PLending:** [P2PLEN-99] - Camadas de lógica e testes unitários da tela de descrição do objetivo do fluxo de pedir dinheiro emprestado ([#2855](https://github.com/PicPay/picpay-ios/issues/2855))
- **P2PLending:** [P2PLEN-105] Testes funcionais da tela de descrição do objetivo ([#2876](https://github.com/PicPay/picpay-ios/issues/2876))
- **UI:** SpacerView para espaçamentos diferentes em StackView ([#2880](https://github.com/PicPay/picpay-ios/issues/2880))
- **pagamento:** [P2P-673] Add tratamento de erro na busca de usuários (cobrança) ([#2859](https://github.com/PicPay/picpay-ios/issues/2859))
- **pagamento:** [P2P-681] Integra componente de contagem de usuarios a tela de detalhamento ([#2860](https://github.com/PicPay/picpay-ios/issues/2860))
- **pagamento:** [P2P-649] Tratamento dos contatos na busca ([#2891](https://github.com/PicPay/picpay-ios/issues/2891))
- **user-security:** [US-433] Adiciona status de rejeitado ao fim da verificação de identidade ([#2854](https://github.com/PicPay/picpay-ios/issues/2854))
- **user-security:** [US-450] Adiciona a tela de dicas antes da etapa de selfie ([#2887](https://github.com/PicPay/picpay-ios/issues/2887))
- **user-security:** [US-434] Adiciona lógica e testes da cena de preview de captura de documento ([#2871](https://github.com/PicPay/picpay-ios/issues/2871))
- **user-security:** [US-418] Testes captura da foto de documento ([#2850](https://github.com/PicPay/picpay-ios/issues/2850))

### Changed
- **DesignSystem:** [DSGN-663] Altera core do sistema de estilos ([#2848](https://github.com/PicPay/picpay-ios/issues/2848))
- **ExpAtendimento:** [XPAT-86] Altera a lógica de credenciais ([#2864](https://github.com/PicPay/picpay-ios/issues/2864))
- **beneficios:** [PDB-073] Tracking de eventos para deeplink de lista de promoções ([#2889](https://github.com/PicPay/picpay-ios/issues/2889))
- **beneficios:** [PDB-011] Adiciona compatibilidade para botão no feed ([#2877](https://github.com/PicPay/picpay-ios/issues/2877))

### Fixed
- **CRED:** [CRED-731] Abrindo FAQ dentro do app ([#2862](https://github.com/PicPay/picpay-ios/issues/2862))
- **Card-Offer:** [PCO-190] Corrige visibilidade do botão fechar, quando o usuário volta para a ValidateCardViewController ([#2858](https://github.com/PicPay/picpay-ios/issues/2858))
- **Card-XP:** [PCX-56] Bug - Tela ajuste de limite - Não refletia para tela de configuração do app e do cartão ([#2803](https://github.com/PicPay/picpay-ios/issues/2803))
- **ExpAtendimento:** [XPAT-102] Adiciona a classe de Assets do UI novamente ao Git ([#2869](https://github.com/PicPay/picpay-ios/issues/2869))

### Removed
- **ContaPF:** [CPF-13] Remover feature flag da tela inicial ([#2870](https://github.com/PicPay/picpay-ios/issues/2870))
- **ContaPF:** [CPF-12] Remover feature flag cobrar usuário nas sugestões ([#2873](https://github.com/PicPay/picpay-ios/issues/2873))
- **ContaPF:** [CPF-17] Remover feature flag de exibição do telefone na tela de confirmação do código SMS ([#2851](https://github.com/PicPay/picpay-ios/issues/2851))
- **housekeeping:** [JIRA-XXX] Remove asset gerado automaticamente ([#2888](https://github.com/PicPay/picpay-ios/issues/2888))


<a name="v10.19.3"></a>
## [v10.19.3] - 2020-05-27
### Added
- **Bills:** [BILL-17] Cria design da tela de mensagens para detalhe de boleto ([#2754](https://github.com/PicPay/picpay-ios/issues/2754))
- **Bills:** [BILL-17] Cria ações genéricas de eventos. ([#2828](https://github.com/PicPay/picpay-ios/issues/2828))
- **CARD:** [PCO-118] Eventos do card ([#2767](https://github.com/PicPay/picpay-ios/issues/2767))
- **CARD-OFFER:** [PCO-118] Analytics fluxo de desbloqueio do cartão ([#2787](https://github.com/PicPay/picpay-ios/issues/2787))
- **CARD-OFFER:** [PCO-156] Analytics do fluxo de request do cartão de débito ([#2829](https://github.com/PicPay/picpay-ios/issues/2829))
- **CARD-OFFER:** [PCO-156] Melhorias nos analytics destes fluxos ([#2841](https://github.com/PicPay/picpay-ios/issues/2841))
- **CRED:** [CRED-736] Biometria Facial: Preview ([#2842](https://github.com/PicPay/picpay-ios/issues/2842))
- **CRED:** [CRED-733] Biometria Facial: Testes da Tela de Intro  ([#2776](https://github.com/PicPay/picpay-ios/issues/2776))
- **CRED:** [CRED-719] Adicionando validação de saldo insuficiente na hora de pagar com saldo picpay ([#2752](https://github.com/PicPay/picpay-ios/issues/2752))
- **CRED:** [CRED-745] Adicionando injeção do saldo em carteira no modulo de loan ([#2798](https://github.com/PicPay/picpay-ios/issues/2798))
- **CRED:** [CRED-740] Biometria Facial: Configuração da Câmera ([#2824](https://github.com/PicPay/picpay-ios/issues/2824))
- **CRED:** [CRED-733] Biometria Facial: Tela de Intro ([#2762](https://github.com/PicPay/picpay-ios/issues/2762))
- **CRED:** [CRED-734] Biometria Facial: Tela de Instruções ([#2797](https://github.com/PicPay/picpay-ios/issues/2797))
- **Card-XP:** [PCX-268] Tela Nova Via Solicitada - Parte1 ([#2836](https://github.com/PicPay/picpay-ios/issues/2836))
- **ContaPF:** [CPF-18] Cria módulo "Registration" ([#2831](https://github.com/PicPay/picpay-ios/issues/2831))
- **DesignSystem:** [DSGN-662] Adiciona suporte a ícones nos botões ([#2819](https://github.com/PicPay/picpay-ios/issues/2819))
- **Engajamento:** [EN-54] Pop Up para pedido de permissão de Contatos e Localização ([#2774](https://github.com/PicPay/picpay-ios/issues/2774))
- **Engajamento:** [EN-125] Recomendação de amigos para seguir ([#2843](https://github.com/PicPay/picpay-ios/issues/2843))
- **Engajamento:** [EN-143] Obter cards de oportunidades pelo servidor ([#2799](https://github.com/PicPay/picpay-ios/issues/2799))
- **Engajamento:** [EN-186] DeepLink para tela de rendimentos ([#2821](https://github.com/PicPay/picpay-ios/issues/2821))
- **ExpAtendimento:**  [XPAT-26] Montar o Modulo de suporte ao consumidor (CustomerSupport) ([#2796](https://github.com/PicPay/picpay-ios/issues/2796))
- **Expansão:** [EXP-401] Adição de testes unitários ([#2816](https://github.com/PicPay/picpay-ios/issues/2816))
- **MGM:** [MGM-297] Deep Link Indicações ([#2769](https://github.com/PicPay/picpay-ios/issues/2769))
- **P2PLending:** [P2PLEN-100] Camadas de view model, service e presenter da tela de seleção de amigo ([#2779](https://github.com/PicPay/picpay-ios/issues/2779))
- **P2PLending:** [P2PLEN-94] Tela de seleção de amigo ([#2745](https://github.com/PicPay/picpay-ios/issues/2745))
- **P2PLending:** [P2PLEN-92] Tela de descrição do objetivo ([#2827](https://github.com/PicPay/picpay-ios/issues/2827))
- **P2PLending:** [P2PLEN-112] Testes unitários da tela de seleção de amigo ([#2806](https://github.com/PicPay/picpay-ios/issues/2806))
- **P2PLending:** [P2PLEN-106] Testes funcionais da tela de escolher amigo ([#2809](https://github.com/PicPay/picpay-ios/issues/2809))
- **P2PLending:** [P2PLEN-113] Testes unitários da tela de seleção de objetivo ([#2742](https://github.com/PicPay/picpay-ios/issues/2742))
- **PCI:** [PCIDEV-646] Refatorar Invoice Card - Parte 2 ([#2840](https://github.com/PicPay/picpay-ios/issues/2840))
- **PCI:** [PCIDEV-XX1] Adiciona Logica de Cvv Container ([#2810](https://github.com/PicPay/picpay-ios/issues/2810))
- **PCI:** [PCIDEV-636] Refatorar Invoice Card nova arquitetura - Parte 1 ([#2771](https://github.com/PicPay/picpay-ios/issues/2771))
- **Plataforma:** [PLAIOS-153] Reseta user no Mixpanel ([#2846](https://github.com/PicPay/picpay-ios/issues/2846))
- **Store:** [SDPA-12] Adicionado evento de acesso a ver mais ([#2807](https://github.com/PicPay/picpay-ios/issues/2807))
- **ativação:** [PPAT-646] Ajustes de layout e comportamento da tela de presentear novo amigo ([#2794](https://github.com/PicPay/picpay-ios/issues/2794))
- **ativação:** [PPAT-643] Lança a tela de presentear novo amigo a partir de uma notificação e adiciona eventos de tracking ([#2732](https://github.com/PicPay/picpay-ios/issues/2732))
- **beneficios:** [PDB-003] Compartilhamento de Estabelecimentos ([#2755](https://github.com/PicPay/picpay-ios/issues/2755))
- **pagamento:** [P2P] Adiciona componente de contagem de usuários ([#2832](https://github.com/PicPay/picpay-ios/issues/2832))
- **pagamento:** [P2P-642] Integra componentes de seleção de usuário ([#2773](https://github.com/PicPay/picpay-ios/issues/2773))
- **pagamento:** [P2P-650] Lógica botão avançar ([#2791](https://github.com/PicPay/picpay-ios/issues/2791))
- **pagamento:** [P2P-644] Implementa loading na tela de busca do fluxo de cobrança ([#2763](https://github.com/PicPay/picpay-ios/issues/2763))
- **pagamento:** [P2P-649] Refatora tratamento dos estados da busca ([#2815](https://github.com/PicPay/picpay-ios/issues/2815))
- **plataforma:** [PLAIOS-155] adiciona propriedade de usuário is_picpay_lover ([#2785](https://github.com/PicPay/picpay-ios/issues/2785))
- **plataforma:** [PLAIO-121] Adiciona ApiCompletion para tracking nos erros de Api usando o Core ([#2628](https://github.com/PicPay/picpay-ios/issues/2628))
- **plataforma:** [PLAIOS-158] adiciona SDK do NewRelic no aplicativo  ([#2790](https://github.com/PicPay/picpay-ios/issues/2790))
- **user-security:** [US-381] Tela de abertura de camera para documentos ([#2814](https://github.com/PicPay/picpay-ios/issues/2814))
- **user-security:** [US-429] Tela de preview de foto de documento ([#2839](https://github.com/PicPay/picpay-ios/issues/2839))
- **user-security:** [US-372] Adiciona feature flag para o fluxo de validação de identidade ([#2789](https://github.com/PicPay/picpay-ios/issues/2789))
- **user-security:** [US-388] Testes da tela de introdução da foto do documento ([#2751](https://github.com/PicPay/picpay-ios/issues/2751))
- **user-security:** [US-415]  Tela de Status da Validação de identidade ([#2823](https://github.com/PicPay/picpay-ios/issues/2823))
- **user-security:** [US-411] Adiciona eventos do Analytics e melhorias ([#2792](https://github.com/PicPay/picpay-ios/issues/2792))

### Changed
- **CRED:** Renomeando tela de sucesso ao final do fluxo ([#2844](https://github.com/PicPay/picpay-ios/issues/2844))
- **Card:** Corrige os espaçamentos do tamanho da imagem ([#2845](https://github.com/PicPay/picpay-ios/issues/2845))
- **Card-Offer:** [PCO-155] Criado o protocolo DeeplinkResolver e implementado no módulo do Card ([#2795](https://github.com/PicPay/picpay-ios/issues/2795))
- **Card-Offer:** [PCO-62] Remover tela de onboarding do fluxo de pedir o cartão físico do múltiplo ([#2783](https://github.com/PicPay/picpay-ios/issues/2783))
- **Card-Offer:** [PCO-142] Mudança do titulo de desbloqueio ([#2766](https://github.com/PicPay/picpay-ios/issues/2766))
- **ExpAtendimento:** [XPAT-86] Refatora o Modulo CustomerSupport para a dependência do Zendesk vir via injeção ([#2825](https://github.com/PicPay/picpay-ios/issues/2825))
- **HouseKeeping:** [HK] Remove warnings simples ([#2837](https://github.com/PicPay/picpay-ios/issues/2837))
- **Housekeeping:** [HK] Adiciona correções no template ([#2784](https://github.com/PicPay/picpay-ios/issues/2784))
- **P2PLending:** [P2PLEN-133] Cenários de erro "Ajustando condições de júros e parcelas" ([#2768](https://github.com/PicPay/picpay-ios/issues/2768))
- **P2PLending:** [P2PLEN-132] Alterar forma de como é apresentado erro na tela de apresentação ([#2811](https://github.com/PicPay/picpay-ios/issues/2811))
- **P2PLending:** [P2PLEN-134] Atualizando a maneira como é reportado o erro de valor invalido ([#2764](https://github.com/PicPay/picpay-ios/issues/2764))
- **PCI:** [PCIDEV-XX1] Debito tecnico PCI ([#2834](https://github.com/PicPay/picpay-ios/issues/2834))
- **Plataforma:** [PLAIOS-161] corrige link original ([#2833](https://github.com/PicPay/picpay-ios/issues/2833))
- **ativação:** [PPAT-662] Mover o evento "Register - Screen Viewed" para o momento de abertura do fluxo de registro ([#2788](https://github.com/PicPay/picpay-ios/issues/2788))
- **plataforma:** [PLAIOS-144] Move os módulos internos para uma pasta chamada 'Modules' ([#2778](https://github.com/PicPay/picpay-ios/issues/2778))
- **plataforma:** Atualização de pods ([#2835](https://github.com/PicPay/picpay-ios/issues/2835))
- **user-security:** [US-414] Altera a qualidade da imagem da selfie para 100% ([#2800](https://github.com/PicPay/picpay-ios/issues/2800))

### Fixed
- **APPSEC:** [ajustes-core] Alteração do acesso para public do typealias da API ([#2772](https://github.com/PicPay/picpay-ios/issues/2772))
- **Bills:** [BILL-287] Reverte update na lib SkeletonView ([#2853](https://github.com/PicPay/picpay-ios/issues/2853))
- **Card-Offer:** [PCO-163] Adiciona feature flag para carregar pinned banners na home ([#2820](https://github.com/PicPay/picpay-ios/issues/2820))
- **Card-XP:** [PCX-287] Adiciona feature flag para o badge na wallet ([#2817](https://github.com/PicPay/picpay-ios/issues/2817))
- **Expansão:** [EXP-401] Ajuste no fluxo de envio do credit card id ([#2808](https://github.com/PicPay/picpay-ios/issues/2808))
- **PCI:** [PCIDEV-XX1] Ajuste dark mode tela de CVV
- **PCI:** [PCIDE-XX1] Ajuste de Payload para id do cartão de credito ([#2805](https://github.com/PicPay/picpay-ios/issues/2805))
- **Plataforma:** [PLAIOS-160] Correção de Strings não identificadas ([#2813](https://github.com/PicPay/picpay-ios/issues/2813))
- **ativação:** [PPAT-666] Ajustar animação da NavigationBar nas views do Onboarding ([#2818](https://github.com/PicPay/picpay-ios/issues/2818))
- **ativação:** [PPAT-664] Alterar nome da variável de response do cadastro e validação de telefone. ([#2802](https://github.com/PicPay/picpay-ios/issues/2802))
- **ativação:** [PPAT-661] Remove assincronia na troca de views para corrigir a abertura de deeplinks no onboarding de primeiras ações ([#2775](https://github.com/PicPay/picpay-ios/issues/2775))
- **beneficios:** [PDB-056] Arruma ação no mapa de detalhe de promoção ([#2804](https://github.com/PicPay/picpay-ios/issues/2804))
- **housekeeping:** [JIRA-XXX] Corrige crash ao instanciar ASDimension com valores negativos ([#2748](https://github.com/PicPay/picpay-ios/issues/2748))

### Removed
- **Card-Offer:** [PCO-161] Remove o programa de cashback do Card ([#2830](https://github.com/PicPay/picpay-ios/issues/2830))
- **P2PLending:** [P2PLEN-157] Remove tela de data de pagamento do fluxo ([#2786](https://github.com/PicPay/picpay-ios/issues/2786))
- **contas:** [CPF-16] [iOS] Remover feature flag de comunicação amigável no cadastro do CPF ([#2838](https://github.com/PicPay/picpay-ios/issues/2838))


<a name="v10.19.2"></a>
## [v10.19.2] - 2020-05-16
### Added
- **Bills:** [BILL-17] - Cria a parte social do detalhe de feed (Parte 1) ([#2670](https://github.com/PicPay/picpay-ios/issues/2670))
- **Bills:** [BILL-17] Cria tela de bills para passo de timeline ([#2716](https://github.com/PicPay/picpay-ios/issues/2716))
- **CRED:** [CRED-641] Criação da interface da tela de pagamento da fatura do empréstimo ([#2697](https://github.com/PicPay/picpay-ios/issues/2697))
- **CRED:** [CRED-732] Criar novo módulo para a validação da biometria facial ([#2746](https://github.com/PicPay/picpay-ios/issues/2746))
- **CRED:** [CRED-718] Integrando fluxo de pagamento de boleto ([#2735](https://github.com/PicPay/picpay-ios/issues/2735))
- **CRED:** [CRED-705] Adicionando lógica de view ([#2718](https://github.com/PicPay/picpay-ios/issues/2718))
- **Card:** [PCO-107] Adiciona toolbar na tela de Informações pessoais no cadastro do débito ([#2717](https://github.com/PicPay/picpay-ios/issues/2717))
- **Card-Offer:** [PCO-120] Criado nova tela de sucesso do desbloqueio do cartão de débito  ([#2747](https://github.com/PicPay/picpay-ios/issues/2747))
- **Card-Offer:** [PCO-112] Dimensionamento do botão da oferta ([#2721](https://github.com/PicPay/picpay-ios/issues/2721))
- **DesignSystem:** [DSGN-612] Adicionar suporte a cores temporárias ([#2744](https://github.com/PicPay/picpay-ios/issues/2744))
- **DesignSystem:** [DSGN-536] Adiciona suporte a calculo de contraste ([#2733](https://github.com/PicPay/picpay-ios/issues/2733))
- **DesignSystem:** [DSGN-610] Adiciona suporte de underline no botão link ([#2713](https://github.com/PicPay/picpay-ios/issues/2713))
- **DesignSystem:** [DSGN-487] Adiciona estilos de botões  ([#2688](https://github.com/PicPay/picpay-ios/issues/2688))
- **DesignSystem:** [DSGN-535] Adiciona suporte background dinamico para Push e Modal ([#2712](https://github.com/PicPay/picpay-ios/issues/2712))
- **MGM:** [MGM-259] Eventos de Analytics ([#2698](https://github.com/PicPay/picpay-ios/issues/2698))
- **P2PLending:** [P2PLEN-114] Testes unitários da tela de data pagamento do fluxo de pedir dinheiro emprestado ([#2687](https://github.com/PicPay/picpay-ios/issues/2687))
- **P2PLending:** [P2PLEN-108] Testes funcionais da tela de data pagamento do fluxo de pedir dinheiro emprestado ([#2686](https://github.com/PicPay/picpay-ios/issues/2686))
- **P2PLending:** [P2PLEN-107] Testes funcionais da tela de seleção de objetivo ([#2743](https://github.com/PicPay/picpay-ios/issues/2743))
- **P2PLending:** [P2PLEN-101] Implementação da camada de viewModel e presenter da tela de seleção de objetivo ([#2701](https://github.com/PicPay/picpay-ios/issues/2701))
- **PCI:** [PCIDEV-597] Normalizar campos P2P e Aniversario ([#2699](https://github.com/PicPay/picpay-ios/issues/2699))
- **PCI:** [PCIDEV-626] Refatorar TransportPass para nova arquitetura de pagamento ([#2719](https://github.com/PicPay/picpay-ios/issues/2719))
- **PLATAFORMA:** [PLAIOS-92] Cria MasterSwitch ([#2624](https://github.com/PicPay/picpay-ios/issues/2624))
- **ativação:** [PPAT-636] Cria testes unitários para o componente PaymentToolbarLarge na tela de presentear amigo ([#2690](https://github.com/PicPay/picpay-ios/issues/2690))
- **ativação:** [PPAT-637] Cria testes unitários e ajustes para o componente PaymentGiftAccessory da tela de pagamento ([#2681](https://github.com/PicPay/picpay-ios/issues/2681))
- **pagamento:** [P2P] Adiciona função de remoção de usuários no agrupador de usuários ([#2727](https://github.com/PicPay/picpay-ios/issues/2727))
- **pagamento:** [P2P-632] Adiciona dados do receptor da transferência no recibo ([#2700](https://github.com/PicPay/picpay-ios/issues/2700))
- **pagamento:** [P2P-643] Cria empty state na busca de usuários ([#2749](https://github.com/PicPay/picpay-ios/issues/2749))
- **pagamento:** [P2P-592] Cria componente de lista de usuários ([#2694](https://github.com/PicPay/picpay-ios/issues/2694))
- **pagamento:** [P2P-593] Integra a lista da busca de usuários com o serviço ([#2726](https://github.com/PicPay/picpay-ios/issues/2726))
- **user-security:** [US-382] Testes da tela de dicas da foto do documento ([#2736](https://github.com/PicPay/picpay-ios/issues/2736))
- **user-security:** [US-375] Adiciona tela de dicas para captura de documentos ([#2702](https://github.com/PicPay/picpay-ios/issues/2702))
- **user-security:** [US-379] Tela de introdução de validação de documentos ([#2725](https://github.com/PicPay/picpay-ios/issues/2725))

### Changed
- **APPSEC:** [APPSEC-305] Separando as chaves do keychain por módulo ([#2730](https://github.com/PicPay/picpay-ios/issues/2730))
- **Card:** [CPO-66] Adiciona o suporte ao darkmode na tela de Esperando Aprovação do Crédito  ([#2711](https://github.com/PicPay/picpay-ios/issues/2711))
- **Card-Offer:** [PCO-138] Atualiza o estilo do botão na tela de Personal Info ([#2729](https://github.com/PicPay/picpay-ios/issues/2729))
- **MGM:** [MGM-246] Ajustes Indicação de Estabelecimento ([#2685](https://github.com/PicPay/picpay-ios/issues/2685))
- **PCI:** [PCIDEV-XX1] Não permitir cadastrar cartão com data de 2 dígitos ([#2738](https://github.com/PicPay/picpay-ios/issues/2738))
- **ativação:** [PPAT-659] Refatorar view de código promocional ([#2756](https://github.com/PicPay/picpay-ios/issues/2756))
- **ativação:** [PPAT-657]  Integração com a tela de username ([#2692](https://github.com/PicPay/picpay-ios/issues/2692))
- **pagamento:** [P2P] Modifica o agrupador de usuários pra aceitar urls ao invés de imagens ([#2710](https://github.com/PicPay/picpay-ios/issues/2710))
- **pagamento:** [P2P-589] Refatora serviço de permissão de cobrança ([#2693](https://github.com/PicPay/picpay-ios/issues/2693))

### Fixed
- **CRED:** Retirando Alerta de FAQ e Adicionando link Externo ([#2707](https://github.com/PicPay/picpay-ios/issues/2707))
- **Card:** [PCO-148] Correção de algumas chaves do localizable ([#2753](https://github.com/PicPay/picpay-ios/issues/2753))
- **Card-Offer:** [PCO-131] Arrumado espaçamento do onboarding do cartão ([#2724](https://github.com/PicPay/picpay-ios/issues/2724))
- **Card-Offer:** [PCO-135] Alterado os espaçamento da tela de confirmação ([#2728](https://github.com/PicPay/picpay-ios/issues/2728))
- **Card-XP:** [PCX-60] Bug - Tela troca de senha - ErrorMessage ([#2740](https://github.com/PicPay/picpay-ios/issues/2740))
- **Engajamento:** [EN-179] Falha no detalhe de Estabelecimento ([#2731](https://github.com/PicPay/picpay-ios/issues/2731))
- **Engajamento_PJ:** [ENPJ-5] Corrigi redirecionamento para a loja do app de Business ([#2691](https://github.com/PicPay/picpay-ios/issues/2691))
- **MGM:** [MGM-246] Corrigidos alguns campos ([#2737](https://github.com/PicPay/picpay-ios/issues/2737))
- **PCI:** [PCIDEV-XX1] Adicionar campo cip que estava faltando. ([#2758](https://github.com/PicPay/picpay-ios/issues/2758))
- **Platafora:** [PLAIOS-XX] Corrige bug de compilação pra prod com MasterSwitch ([#2750](https://github.com/PicPay/picpay-ios/issues/2750))
- **Plataforma:** [PLAIOS-149] Alinhamento de strings ([#2741](https://github.com/PicPay/picpay-ios/issues/2741))
- **plataforma:** Renomeia o target 'setup' para 'setuptools' no Makefile ([#2739](https://github.com/PicPay/picpay-ios/issues/2739))
- **plataforma:** Remove UIMakedTextField porque o mesmo estava crashando o aplicativo [#2723](https://github.com/PicPay/picpay-ios/issues/2723)


<a name="v10.19.1"></a>
## [v10.19.1] - 2020-05-06
### Added
- **APPSEC:** [APPSEC-300]Criando módulo novo de segurança ([#2680](https://github.com/PicPay/picpay-ios/issues/2680))
- **Bills:** [BILL-17]- Cria serviços principais e serviços customizados de social ([#2639](https://github.com/PicPay/picpay-ios/issues/2639))
- **CARD:** [CCBO-] Implementação da regra do textfield  ([#2668](https://github.com/PicPay/picpay-ios/issues/2668))
- **CRED:** [CRED-466] Criando Layout da tela de Fatura Crédito Pessoal ([#2683](https://github.com/PicPay/picpay-ios/issues/2683))
- **DesignSystem:** [DSGN-584] Adiciona cores de background agrupado ([#2663](https://github.com/PicPay/picpay-ios/issues/2663))
- **DesignSystem:** [DSGN-534] Adiciona nova sistema de estilos  ([#2664](https://github.com/PicPay/picpay-ios/issues/2664))
- **Engajamento:** [PPEN-XXX] Tracking Bottom Sheet ([#2661](https://github.com/PicPay/picpay-ios/issues/2661))
- **Engajamento:** [PPEN-XX] Tracking permissão para usar localização em pagar > locais ([#2666](https://github.com/PicPay/picpay-ios/issues/2666))
- **Engajamento:** [PPEN-58] Alterando texto permissão de contatos ([#2679](https://github.com/PicPay/picpay-ios/issues/2679))
- **MGM:** [MGM-226] Modal de incentivo na Home. ([#2659](https://github.com/PicPay/picpay-ios/issues/2659))
- **P2PLending:** [P2PLEN-95] Tela de seleção de objetivos ([#2689](https://github.com/PicPay/picpay-ios/issues/2689))
- **P2PLending:** [P2PLEN-102] Camadas de viewModel e presenter da tela de data de pagamento ([#2669](https://github.com/PicPay/picpay-ios/issues/2669))
- **ativação:** [PPAT-658] Remove o componente UIMaskedTextField da tela de cadastrar documento ([#2695](https://github.com/PicPay/picpay-ios/issues/2695))
- **ativação:** [PPAT-656] Criar testes do coordinator e criar os eventos. ([#2682](https://github.com/PicPay/picpay-ios/issues/2682))
- **ativação:** [PPAT-645] Cria testes unitários para o componente ProfileHeader da tela de pagamento ([#2671](https://github.com/PicPay/picpay-ios/issues/2671))
- **pagamento:** [P2P-590] Cria componente de pesquisa ([#2651](https://github.com/PicPay/picpay-ios/issues/2651))
- **pagamento:** [P2P-591] Cria componente de agrupamento de usuários ([#2655](https://github.com/PicPay/picpay-ios/issues/2655))
- **pagamento:** [P2P-594] Adiciona estrutura incial da nova tela de busca de cobrança ([#2676](https://github.com/PicPay/picpay-ios/issues/2676))
- **user-security:** [US-359] Tracking da validação de Selfie ([#2667](https://github.com/PicPay/picpay-ios/issues/2667))

### Changed
- **CRED:** [CRED-476] Adicionando chamada à nova API de limites de simulação na tela de onboarding ([#2629](https://github.com/PicPay/picpay-ios/issues/2629))
- **Plataforma:** [PLAIOS-63] Alteração de Strings para seus respectivos Localizable ([#2531](https://github.com/PicPay/picpay-ios/issues/2531))
- **ativação:** [PPAT-652] Refatorar presenter da tela de definição de username ([#2654](https://github.com/PicPay/picpay-ios/issues/2654))
- **user-security:** [US-377]  Organização de arquivos e nomenclatura ([#2696](https://github.com/PicPay/picpay-ios/issues/2696))

### Fixed
- **Engajamento:** [PPEN-160] Mostra botão de fechar na WebView de Novidades ([#2705](https://github.com/PicPay/picpay-ios/issues/2705))
- **PCI:** [PCIDEV-XX1] Hotfix cadastro cartão de debito fluxo PCI [#2677](https://github.com/PicPay/picpay-ios/issues/2677) [#2678](https://github.com/PicPay/picpay-ios/issues/2678)
- **ativação:** [PPAT-655] Incluir placeholder na chamada do método ([#2674](https://github.com/PicPay/picpay-ios/issues/2674))


<a name="v10.19.0"></a>
## [v10.19.0] - 2020-04-29
### Added
- **BACEN:** [BD-1071] Atualizar tela de Bloqueio Judicial e seus entry points ([#2635](https://github.com/PicPay/picpay-ios/issues/2635))
- **BACEN:** [BD-1107] Criar Cards do wallet para o PEP ([#2630](https://github.com/PicPay/picpay-ios/issues/2630))
- **BACEN:** [BD-1098] Adicionar tracking de eventos para as telas de PEP ([#2653](https://github.com/PicPay/picpay-ios/issues/2653))
- **Bills:** [BILL-19] Adiciona tracking de eventos em Bills e troca paleta de cores ([#2601](https://github.com/PicPay/picpay-ios/issues/2601))
- **Bills:** [BILL-17] Adiciona a arquitetura base da nova tela de detalhe do feed para boleto ([#2621](https://github.com/PicPay/picpay-ios/issues/2621))
- **Bills:** [BILL-184] Correções de design nas telas de erro de boleto ([#2662](https://github.com/PicPay/picpay-ios/issues/2662))
- **CARD:** [CCBO-*] Trocado os textos do débito  ([#2658](https://github.com/PicPay/picpay-ios/issues/2658))
- **CRED:** [CRED-466] Preparação para a tela de Acompanhamento de Contratação de Crédito ([#2607](https://github.com/PicPay/picpay-ios/issues/2607))
- **CRED:** [CRED-639] Criação da arquitetura inicial da nova tela de pagamento do emprestimo ([#2632](https://github.com/PicPay/picpay-ios/issues/2632))
- **Card:** [CCBO-2946] Habilita o debito com Feature Flag ([#2618](https://github.com/PicPay/picpay-ios/issues/2618))
- **DesignSystem:** [DSGN-595] Adiciona tokens de cores adicionais e marcas externas  ([#2600](https://github.com/PicPay/picpay-ios/issues/2600))
- **DesignSystem:** [DSGN-537] Adiciona documentação interna na tipografia ([#2602](https://github.com/PicPay/picpay-ios/issues/2602))
- **Engajamento:** [PPEN-463] Integração backend bottomSheet ([#2613](https://github.com/PicPay/picpay-ios/issues/2613))
- **Engajamento:** [EN-28] Capa e número de telefone no detalhe de promoção ([#2660](https://github.com/PicPay/picpay-ios/issues/2660))
- **Housekeeping:** Usando CDN do cocoapods ao invés do repo ([#2646](https://github.com/PicPay/picpay-ios/issues/2646))
- **MGM:** [MGM-225] Incentivo de recomendação. ([#2596](https://github.com/PicPay/picpay-ios/issues/2596))
- **MGM:** [MGM-233] Alterações na tela de recomendação de estabelecimento. ([#2627](https://github.com/PicPay/picpay-ios/issues/2627))
- **MGM:** [MGM-217] Tela Inicial contextualizada. ([#2536](https://github.com/PicPay/picpay-ios/issues/2536))
- **P2PLending:** [P2PLEN-109] Testes unitários da tela de condições da proposta ([#2616](https://github.com/PicPay/picpay-ios/issues/2616))
- **P2PLending:** [P2PLEN-115] Testes funcionais da tela de condições da proposta ([#2625](https://github.com/PicPay/picpay-ios/issues/2625))
- **P2PLending:** [P2PLEN-103] Camadas de viewModel, Service e Presenter da tela de condições da proposta ([#2604](https://github.com/PicPay/picpay-ios/issues/2604))
- **P2PLending:** [P2PLEN-96] Tela de data de pagamento ([#2657](https://github.com/PicPay/picpay-ios/issues/2657))
- **P2PLending:** [P2PLEN-53] Componente para seleção de dia de pagamento ([#2641](https://github.com/PicPay/picpay-ios/issues/2641))
- **PCI:** [PCIDEV-588] Colocar password no X-Authentication e Mudar chamada CVV ([#2642](https://github.com/PicPay/picpay-ios/issues/2642))
- **ativação:** [PPAT-638] Cria a tela de presentear novo amigo ([#2587](https://github.com/PicPay/picpay-ios/issues/2587))
- **ativação:** [PPAT-644] Adiciona novas ações para a tela de presentear novo amigo ([#2637](https://github.com/PicPay/picpay-ios/issues/2637))
- **pagamento:** [P2P-588] Atualiza o layout da hub de cobrança ([#2645](https://github.com/PicPay/picpay-ios/issues/2645))
- **pagamento:** [P2P-495] Adiciona card clicável de usuário cobrado ([#2606](https://github.com/PicPay/picpay-ios/issues/2606))
- **pagamento:** [P2P-202] Altera fluxo de cobrança por Link e QrCode ([#2610](https://github.com/PicPay/picpay-ios/issues/2610))
- **pagamento:** [P2P-96] Adiciona opções de cobrança nos ajustes de notificação e privacidade ([#2598](https://github.com/PicPay/picpay-ios/issues/2598))
- **universitarios:** [UNI-354] Entrega parcial da oferta de contaUni no cadastro ([#2633](https://github.com/PicPay/picpay-ios/issues/2633))
- **universitários:** [UNI-364] Adiciona tracking de eventos do Perfil ([#2612](https://github.com/PicPay/picpay-ios/issues/2612))
- **user-security:** [US-334] Tela de Preview da Biometria Facial ([#2603](https://github.com/PicPay/picpay-ios/issues/2603))
- **user-security:** [US-344] Biometria facial Carregamento e validação ([#2647](https://github.com/PicPay/picpay-ios/issues/2647))

### Changed
- **APPSEC:** [APPSEC-250] Alteração de como pegamos os certificados do pinning ([#2626](https://github.com/PicPay/picpay-ios/issues/2626))
- **CRED:** [CRED-646] Modificando Coordinator da Fatura para enum de actions ([#2638](https://github.com/PicPay/picpay-ios/issues/2638))
- **CRED:** [CRED-635] Adicionando telas de faq e mudando coordinators para usarem actions ([#2644](https://github.com/PicPay/picpay-ios/issues/2644))
- **Card:** [CCBO-2947] Adição de novos Status ([#2623](https://github.com/PicPay/picpay-ios/issues/2623))
- **Card:** [CCBO-2960] Parametros opcionais para o CreditAccount ([#2652](https://github.com/PicPay/picpay-ios/issues/2652))
- **Engajamento:** [PPEN-66] Alterando a view de autorização de localização na aba pagar > locais ([#2640](https://github.com/PicPay/picpay-ios/issues/2640))
- **Expansão:** [EXP-385] LINX AnyCodable Refactoring ([#2615](https://github.com/PicPay/picpay-ios/issues/2615))
- **HouseKeeping:** [JIRA-XXX] Corrigindo teste funcional do RegistrationStepsTests que estava falhando ([#2614](https://github.com/PicPay/picpay-ios/issues/2614))
- **HouseKeeping:** [JIRA-XXX] Ajustes no Template de MVVM-C para que o ViewController seja final e alteração de struct do Layout para enum. ([#2643](https://github.com/PicPay/picpay-ios/issues/2643))
- **Plataforma:** [PLAIOS-104] Substituição do algoritmo de mascara de texto ([#2359](https://github.com/PicPay/picpay-ios/issues/2359))
- **ativação:** [PPAT-649] Refatorar viewmodel da tela de definição de username ([#2631](https://github.com/PicPay/picpay-ios/issues/2631))
- **user-security:** [US-345] Alterar campo do nome da mãe no TFA e Dados Pessoais ([#2622](https://github.com/PicPay/picpay-ios/issues/2622))
- **user-security:** [US-353] Alterar valor default feature flag do TFA para true ([#2636](https://github.com/PicPay/picpay-ios/issues/2636))

### Fixed
- **DesignSystem:** [DSGN-538] Atualiza calculo de contraste de background ([#2608](https://github.com/PicPay/picpay-ios/issues/2608))
- **PCI:** [PCIDEV-XX1] Hotfix cadastro cartão de debito fluxo PCI [#2677](https://github.com/PicPay/picpay-ios/issues/2677)
- **ativação:** [PPAT-651] Corrige o bug na apresentação das células de Contas Vinculadas ([#2617](https://github.com/PicPay/picpay-ios/issues/2617))
- **card:** CCBO-2958 Corrige layout da tela de confirmação de cadastro no SE
- **plataforma:** [PLAIOS-131] correção tela verde quando acessava deeplink pagamento ([#2649](https://github.com/PicPay/picpay-ios/issues/2649))


<a name="v10.18.24"></a>
## [v10.18.24] - 2020-04-20
### Added
- **Bacen:** [BD-xx1] Tela Conta Limitada PEP ([#2568](https://github.com/PicPay/picpay-ios/issues/2568))
- **Bills:** [BILL-19] Cria injeção de dependências para Bills e adiciona testes unitários ([#2595](https://github.com/PicPay/picpay-ios/issues/2595))
- **Bills:** [BILL-19] Ajusta layout da timeline e cria lógica para actions vindas da API ([#2570](https://github.com/PicPay/picpay-ios/issues/2570))
- **CARD:** [CCBO-2900] Lógica para sumir com o banner da home após o fluxo ([#2585](https://github.com/PicPay/picpay-ios/issues/2585))
- **CARD:** [CCBO-2818] Trocar componente do cartão no fluxo de esqueci minha senha ([#2580](https://github.com/PicPay/picpay-ios/issues/2580))
- **CASH:** [CASH-XX1] Ajuste de Contrato - Ultima Parte ([#2611](https://github.com/PicPay/picpay-ios/issues/2611))
- **Card:** [CCBO-2922] Adiciona Dialogs de Erro ([#2592](https://github.com/PicPay/picpay-ios/issues/2592))
- **Card:** [CCBO-2438] Banner da home (Testes) ([#2581](https://github.com/PicPay/picpay-ios/issues/2581))
- **Card:** [CCBO-2907] Home - Banner - Tracking dinâmico ([#2583](https://github.com/PicPay/picpay-ios/issues/2583))
- **Card:** [CCBO-2387] Tela de verificando os seus dados (fluxo do débito) ([#2594](https://github.com/PicPay/picpay-ios/issues/2594))
- **Card:** [CCBO-2507]:  Exibir o menu de acordo com as opções Debito | Múltiplo ([#2543](https://github.com/PicPay/picpay-ios/issues/2543))
- **Card:** [CCBO-2877] Cria fluxo do desbloqueio do cartão de débito ([#2578](https://github.com/PicPay/picpay-ios/issues/2578))
- **Cash:** [CASH-611] Tela de passo a passo para criar cartão virtual - Parte 2 ([#2609](https://github.com/PicPay/picpay-ios/issues/2609))
- **Engajamento:** [PPEN-454] Bottom Sheet aba pagar ([#2566](https://github.com/PicPay/picpay-ios/issues/2566))
- **Engajamento:** [PPEN-463] Favoritos e Deeplink no BottomSheet ([#2591](https://github.com/PicPay/picpay-ios/issues/2591))
- **P2PLending:** [P2PLEN-52] Criação do componente de stepper customizado ([#2576](https://github.com/PicPay/picpay-ios/issues/2576))
- **P2PLending:** [P2PLEN-97] Layout da tela de condições da proposta ([#2586](https://github.com/PicPay/picpay-ios/issues/2586))
- **P2PLending:** [P2PLEN-42] Tests unitários da tela de valor da proposta ([#2551](https://github.com/PicPay/picpay-ios/issues/2551))
- **P2PLending:** [P2PLEN-43] Testes funcionais da tela de valor da proposta ([#2552](https://github.com/PicPay/picpay-ios/issues/2552))
- **ativação:** [PPAT-650] Cria evento para monitorar o fluxo dinâmico de cadastro ([#2597](https://github.com/PicPay/picpay-ios/issues/2597))
- **card:** CCBO-2451 Adiciona serviço e chamada para o badge da carteira ([#2589](https://github.com/PicPay/picpay-ios/issues/2589))
- **pagamento:** [P2P-98] Botão de cobrar no perfil ([#2573](https://github.com/PicPay/picpay-ios/issues/2573))
- **universitários:** [UNI-365] Adiciona testes unitários da camada Service ([#2588](https://github.com/PicPay/picpay-ios/issues/2588))
- **user-security:** [US-333] Tela de captura da biometria testes unitários ([#2582](https://github.com/PicPay/picpay-ios/issues/2582))
- **user-security:** [US-333] Tela de captura da biometria ([#2572](https://github.com/PicPay/picpay-ios/issues/2572))

### Changed
- **APPSEC:** [APPSEC-288] Alteração do acesso a flag do pinning para facilitar a utilização e alteração no Modo Debug ([#2593](https://github.com/PicPay/picpay-ios/issues/2593))
- **CRED:** [CRED-535] Mantendo numero de parcelas quando cliente faz nova simulação ([#2575](https://github.com/PicPay/picpay-ios/issues/2575))
- **Card:** [CCBO-2803] Mudança dos texto do Fluxo de Desbloqueio ([#2558](https://github.com/PicPay/picpay-ios/issues/2558))
- **HouseKeeping:** [JIRA-XXX] Corrigindo testes unitários de ofuscação que estavam falhando ([#2599](https://github.com/PicPay/picpay-ios/issues/2599))
- **ativação:** [PPAT-633] Refatorar layout da tela de definição de username ([#2567](https://github.com/PicPay/picpay-ios/issues/2567))

### Removed
- **Plataforma:** [PLAIOS-43] Remover a dependencia MBProgressBar ([#2522](https://github.com/PicPay/picpay-ios/issues/2522))


<a name="v10.18.23"></a>
## [v10.18.23] - 2020-04-15
### Added
- **Advertising:** [CCBO-2438] Fix dependencies on project.yml ([#2560](https://github.com/PicPay/picpay-ios/issues/2560))
- **CARD:** [CCBO-2438] Criação da InlineBannerView ([#2569](https://github.com/PicPay/picpay-ios/issues/2569))
- **CARD:** [CCBO-2801] Criação da view do cartão de crédito ([#2564](https://github.com/PicPay/picpay-ios/issues/2564))
- **Card:** [CCBO-2793] Atualizar o link da central de ajuda ([#2548](https://github.com/PicPay/picpay-ios/issues/2548))
- **Card:** [CCBO-2383] Banner oferta da home - ActionSheet + Tracking ([#2555](https://github.com/PicPay/picpay-ios/issues/2555))
- **Engajamento:** [PPEN-361] Mostra detalhe de promoções para os Estabelecimentos ([#2557](https://github.com/PicPay/picpay-ios/issues/2557))
- **MGM:** [MGM-171] Testes do ViewModel e Presenter de Indicações. ([#2492](https://github.com/PicPay/picpay-ios/issues/2492))
- **P2PLending:** [P2PLEN-41] Camada de viewModel e Presenter da tela de valor da proposta ([#2547](https://github.com/PicPay/picpay-ios/issues/2547))
- **P2PLending:** [P2PLEN-40] Layout da tela do valor da proposta ([#2533](https://github.com/PicPay/picpay-ios/issues/2533))
- **PCI:** [PCIDEV-569] Pagamento de fatura do PPCard ([#2544](https://github.com/PicPay/picpay-ios/issues/2544))
- **card:** CCBO-2898 Cria o banner de desbloqueio do cartão ([#2577](https://github.com/PicPay/picpay-ios/issues/2577))
- **card:**  CCBO -2832 Exibe link de oferta de solicitação do débito na carteira ([#2535](https://github.com/PicPay/picpay-ios/issues/2535))
- **credito:** Modulo de crédito pessoal ([#2541](https://github.com/PicPay/picpay-ios/issues/2541))
- **user-security:** [US-305] Adiciona FeatureFlag para controlar novo fluxo de validação de Selfie ([#2549](https://github.com/PicPay/picpay-ios/issues/2549))
- **user-security:** [US-306] Tela de introdução para validação de Biometria facial ([#2565](https://github.com/PicPay/picpay-ios/issues/2565))

### Changed
- **B2B:** Adição de meio para remover application/json se não for necessário na chamada ([#2571](https://github.com/PicPay/picpay-ios/issues/2571))
- **CASH:** [CASH-427] Voltar para o feed após recarga via débito ([#2561](https://github.com/PicPay/picpay-ios/issues/2561))
- **PCI:** [PCIDEV-571] Fix Pedindo CVV duas vezes ([#2559](https://github.com/PicPay/picpay-ios/issues/2559))
- **Plataforma:** [PLAIOS-132] Move extension de download de imagens para o módulo de UI ([#2550](https://github.com/PicPay/picpay-ios/issues/2550))
- **ativação:** [PPAT-630] Posterga a exibição popups e abertura de deeplinks para depois da exibição do tutorial  ([#2539](https://github.com/PicPay/picpay-ios/issues/2539))

### Fixed
- **ativação:** [PPAT-632] Limpa dados temporários locais de cartões de crédito no logout ([#2545](https://github.com/PicPay/picpay-ios/issues/2545))
- **plataforma:** Ajuste no script de instalação do template de arquitetura ([#2554](https://github.com/PicPay/picpay-ios/issues/2554))
- **plataforma:** [PLAIOS-111] Adiciona os testes de modulos criados a partir do template ([#2518](https://github.com/PicPay/picpay-ios/issues/2518))

### Security
- **APPSEC:** [APPSEC-268] Adicionando máscara de ofuscação na tela de settings ([#2556](https://github.com/PicPay/picpay-ios/issues/2556))


<a name="v10.18.22"></a>
## [v10.18.22] - 2020-04-09
### Fixed
- **pagamento:** [P2P] Problema com compartilhamento da imagem do recibo [#2562](https://github.com/PicPay/picpay-ios/issues/2562)


<a name="v10.18.21"></a>
## [v10.18.21] - 2020-04-08
### Added
- **APPSEC:** [APPSEC-238] Adição de testes unitários e funcionais para ofuscação ([#2508](https://github.com/PicPay/picpay-ios/issues/2508))
- **Bills:** [BILL-19] Adiciona a lógica de negócios da timeline de boleto ([#2527](https://github.com/PicPay/picpay-ios/issues/2527))
- **Bills:** [BILL-69] Implementa tela de erro de taxa alterada ao pagar boleto ([#2517](https://github.com/PicPay/picpay-ios/issues/2517))
- **Bills:** [BILL-19] Cria primeira versão da tela de timeline de boleto ([#2512](https://github.com/PicPay/picpay-ios/issues/2512))
- **CARD:** [CCBO-2710] Criado método para atualizar a view de trás ao encerrar o fluxo de débito ([#2526](https://github.com/PicPay/picpay-ios/issues/2526))
- **CARD:** [CCBO-2387] Criação do fluxo in-progress para o débito ([#2515](https://github.com/PicPay/picpay-ios/issues/2515))
- **Card:** [CCBO-2438] Criar o módulo de Advertising ([#2487](https://github.com/PicPay/picpay-ios/issues/2487))
- **Card:** [CCBO-2503] Fluxo do Débito  ([#2470](https://github.com/PicPay/picpay-ios/issues/2470))
- **Engajamento:** [PPEN-444] Favoritar estabelecimentos pelo detalhe de promoção ([#2507](https://github.com/PicPay/picpay-ios/issues/2507))
- **Engajamento:** [PPEN-421] Adicionando BottomSheet à aba de pagamentos. ([#2479](https://github.com/PicPay/picpay-ios/issues/2479))
- **MGM:** [MGM-197] Alterado icone de indicações. ([#2496](https://github.com/PicPay/picpay-ios/issues/2496))
- **P2PLending:** [P2PLEN-34] Implementação da camada de viewModel, service e presenter da tela de apresentação ([#2506](https://github.com/PicPay/picpay-ios/issues/2506))
- **P2PLending:** [P2PLEN-35] Testes unitários da tela de apresentação ([#2511](https://github.com/PicPay/picpay-ios/issues/2511))
- **P2PLending:** [P2PLEN-29] Layout da tela de apresentação ([#2489](https://github.com/PicPay/picpay-ios/issues/2489))
- **P2PLending:** [P2PLEN-27] Model da configuração da proposta ([#2493](https://github.com/PicPay/picpay-ios/issues/2493))
- **P2PLending:** [P2PLEN-37] Testes funcionais da tela de apresentação ([#2519](https://github.com/PicPay/picpay-ios/issues/2519))
- **PCI:** [PCIDEV-556] Refatorar Aniversario Parte II - Accessory e Integração ([#2498](https://github.com/PicPay/picpay-ios/issues/2498))
- **ativação:** [PPAT-610] Cria as views para cada passo do tutorial ([#2520](https://github.com/PicPay/picpay-ios/issues/2520))
- **ativação:** [PPAT-629] Implementa eventos para as telas de tutorial ([#2525](https://github.com/PicPay/picpay-ios/issues/2525))
- **ativação:** [PPAT-609] Implementar lógica de passos do tutorial ([#2500](https://github.com/PicPay/picpay-ios/issues/2500))
- **pagamento:** [P2P-505] Adiciona alguns analytics da transferência externa ([#2524](https://github.com/PicPay/picpay-ios/issues/2524))
- **pagamento:** [P2P-530] Adiciona opção de voltar no alerta de saldo mínimo ([#2540](https://github.com/PicPay/picpay-ios/issues/2540))
- **pagamento:** [P2P-520] Atualiza feed e saldo depois da transferência externa ([#2523](https://github.com/PicPay/picpay-ios/issues/2523))
- **user-security:** [US-275] Adiciona campo dígito da agência  ([#2495](https://github.com/PicPay/picpay-ios/issues/2495))

### Changed
- Update version [#2510](https://github.com/PicPay/picpay-ios/issues/2510)
- Update version [#2509](https://github.com/PicPay/picpay-ios/issues/2509)
- **HouseKeeping:** [JIRA-XXX] Alterando o teste funcional testRecoverPassword para utilizar o mock ([#2538](https://github.com/PicPay/picpay-ios/issues/2538))
- **PCI:** [PCIDEV-562] Salvar CVV apenas transações com sucesso ([#2516](https://github.com/PicPay/picpay-ios/issues/2516))
- **PCI:** [PCIDEV-561] Feature flag cadastro de cartão ([#2514](https://github.com/PicPay/picpay-ios/issues/2514))
- **Plataforma:** PLAIOS-91 Adiciona mocks de flags no target certo ([#2412](https://github.com/PicPay/picpay-ios/issues/2412))
- **Plataforma:** [PLAIOS-120] Refatora a extensão de UIImageView para download de imagens. ([#2513](https://github.com/PicPay/picpay-ios/issues/2513))
- **pagamento:** [P2P-534] Corrige textos de alertas de privacidade e de forma de pagamento ([#2542](https://github.com/PicPay/picpay-ios/issues/2542))
- **pagamento:** [P2P-512] Refatora construção de layout do ImageLabelValueButtonReceiptWidgetView ([#2529](https://github.com/PicPay/picpay-ios/issues/2529))
- **plataforma:** [PLAIOS-33] limpa atributos que não era utilizado no consumer ([#2505](https://github.com/PicPay/picpay-ios/issues/2505))

### Fixed
- **Expansão:** [EXP-296] Correção do Bug que deixava a tela preta no cancelamento de P2M ([#2480](https://github.com/PicPay/picpay-ios/issues/2480))
- **MGM:** [MGM-171] Solução da tela em Dark Mode.  ([#2530](https://github.com/PicPay/picpay-ios/issues/2530))
- **ativação:** [PPAT-631] Remover warnings do swiftlint do Cadastro novo ([#2537](https://github.com/PicPay/picpay-ios/issues/2537))
- **ativação:** [PPAT-628] Ajustes nas constraints na aba de Pagar ([#2521](https://github.com/PicPay/picpay-ios/issues/2521))
- **ativação:** [PPAT-608] Corrige testes unitários relacionados à tela de meu código e à popup de inserir código promocional ([#2484](https://github.com/PicPay/picpay-ios/issues/2484))
- **universitários:** [UNI-373] Arruma bug de header no perfil ([#2546](https://github.com/PicPay/picpay-ios/issues/2546))


<a name="v10.18.20"></a>
## [v10.18.20] - 2020-04-01
### Added
- **API:** [SAPI-244] Adiciona suporte a QR code de outros serviços de Vending Machine ([#2438](https://github.com/PicPay/picpay-ios/issues/2438))
- **Bills:** Cria o módulo de pagamentos ([#2481](https://github.com/PicPay/picpay-ios/issues/2481))
- **CARD:** [CCBO-2656] Tracking da tela de confirmação de endereço ([#2461](https://github.com/PicPay/picpay-ios/issues/2461))
- **CARD:** [CCBO-2654] Tracking da tela de address  ([#2449](https://github.com/PicPay/picpay-ios/issues/2449))
- **CARD:** [CCBO-2387] Criação da tela de loading do fluxo do débito ([#2446](https://github.com/PicPay/picpay-ios/issues/2446))
- **Card:** [CCBO-2511] Criar fluxo do débito ([#2452](https://github.com/PicPay/picpay-ios/issues/2452))
- **Card:** [CCBO-2516] Debit Onboarding - PDF de contrato ([#2457](https://github.com/PicPay/picpay-ios/issues/2457))
- **Card:** [CCBO-2532] Tela de confirmação de endereço (Testes) ([#2462](https://github.com/PicPay/picpay-ios/issues/2462))
- **Card:** [CCBO-2385] DebitOnboarding - Tests ([#2447](https://github.com/PicPay/picpay-ios/issues/2447))
- **DesignSystem:** [DSGN-345] Adiciona tipografia no UISample ([#2419](https://github.com/PicPay/picpay-ios/issues/2419))
- **Engajamento:** [PPEN-329] Favoritar Digital Good ([#2423](https://github.com/PicPay/picpay-ios/issues/2423))
- **Engajamento:** [PPEN-435] Deep link para detalhes de promoções ([#2459](https://github.com/PicPay/picpay-ios/issues/2459))
- **MGM:** [MGM-171] Item de indicação no feed. ([#2460](https://github.com/PicPay/picpay-ios/issues/2460))
- **MGM:** [MGM-171] Serviço de indicação ([#2435](https://github.com/PicPay/picpay-ios/issues/2435))
- **PCI:** [PCIDEV-526] Adequar PCI a vending machine ([#2472](https://github.com/PicPay/picpay-ios/issues/2472))
- **PCI:** [PCIDEV-553] Refatorar Aniversario Parte I - Header e Footer ([#2467](https://github.com/PicPay/picpay-ios/issues/2467))
- **Plataforma:** [PLAIOS-122] Adiciona configuração de PR Authors e Ready To Merge ao Kodiak ([#2488](https://github.com/PicPay/picpay-ios/issues/2488))
- **ativação:** [PPAT-596] Integração da tela de continuar cadastro com a restauração do cadastro salvo ([#2441](https://github.com/PicPay/picpay-ios/issues/2441))
- **ativação:** [PPAT-604] Adiciona a feature flag para a feature de continuar cadastro ([#2471](https://github.com/PicPay/picpay-ios/issues/2471))
- **ativação:** [PPAT-607] Implementa tela base do tutorial inicial do app ([#2474](https://github.com/PicPay/picpay-ios/issues/2474))
- **ativação:** [PPAT-603] Criar barra de progresso para o tutorial após o cadastro ([#2469](https://github.com/PicPay/picpay-ios/issues/2469))
- **pagamento:** [P2P-297] Configura actions no feed e recibo da transferencia externa ([#2478](https://github.com/PicPay/picpay-ios/issues/2478))
- **pagamento:** [P2P-295] Adiciona botão de reenviar link de transferência  externa no feed ([#2444](https://github.com/PicPay/picpay-ios/issues/2444))
- **pagamento:** [P2P-425] Adiciona etapa de senha na transferência externa ([#2491](https://github.com/PicPay/picpay-ios/issues/2491))
- **pagamento:** [P2P-474] Atualizando trackings transferência externa ([#2499](https://github.com/PicPay/picpay-ios/issues/2499))
- **plataforma:** [PLAIOS-111] Adiciona target de sample ao template de módulos [#2477](https://github.com/PicPay/picpay-ios/issues/2477)
- **universitários:** [UNI-151] Identificação de ContaUni no feed e perfil ([#2494](https://github.com/PicPay/picpay-ios/issues/2494))
- **user-security:** [US-181] Adicionad suporte para force update na nova Api ([#2455](https://github.com/PicPay/picpay-ios/issues/2455))
- **user-security:** [US-233] Tela de validação do TFA por cartão parte 3 - Testes do viewModel e Coordinator ([#2439](https://github.com/PicPay/picpay-ios/issues/2439))
- **user-security:** [US-233] Validação TFA com cartão parte 4 Testes do presenter e ajustes ([#2453](https://github.com/PicPay/picpay-ios/issues/2453))

### Changed
- **Engajamento:** [PPEN-429] Refatora modelo de favoritos apresentados na Home ([#2421](https://github.com/PicPay/picpay-ios/issues/2421))
- **HouseKeeping:** [JIRA-XXX] Alteração da camada de Analytics ([#2485](https://github.com/PicPay/picpay-ios/issues/2485))
- **Plataforma:** [PLAIOS-87] Remover Reachability do projeto ([#2463](https://github.com/PicPay/picpay-ios/issues/2463))
- **Plataforma:** [PLAIOS-112] Substitui novo fluxo de conta bancária em entradas alternativas do fluxo ([#2381](https://github.com/PicPay/picpay-ios/issues/2381))
- **UI:** Adiciona suporte a múltiplos tipos de celula ao TableViewHandler ([#2468](https://github.com/PicPay/picpay-ios/issues/2468))
- **card:** CCBO-2534 Adiciona bagde para a wallet ([#2465](https://github.com/PicPay/picpay-ios/issues/2465))
- **pagamento:** [P2P] Cria um novo tipo de item de pesquisa ([#2450](https://github.com/PicPay/picpay-ios/issues/2450))

### Fixed
- **Plataforma:** [JIRA-XXX] Corrige o BankAccountTests.testBankAccountDisplay() ([#2482](https://github.com/PicPay/picpay-ios/issues/2482))
- **Plataforma:** [PLAIOS-123] Corrige carregamento de imagem da Payment Toolbar. ([#2490](https://github.com/PicPay/picpay-ios/issues/2490))
- **pagamento:** [P2P] Corrige teste unitários da PaymentRequestCheckoutViewModel ([#2475](https://github.com/PicPay/picpay-ios/issues/2475))
- **plataforma:** Update da gem Octokit (dependencia do Danger)
- **plataforma:** Reimplementa script de upload de changelog ([#2458](https://github.com/PicPay/picpay-ios/issues/2458))
- **universitários:** [JIRA-XXX] Housekeeping - Arruma testes com simulador em dark mode ([#2476](https://github.com/PicPay/picpay-ios/issues/2476))

### Security
- **APPSEC:** [APPSEC-228] Adição de ofuscação nas telas ([#2466](https://github.com/PicPay/picpay-ios/issues/2466))


<a name="v10.18.19"></a>
## [v10.18.19] - 2020-03-25
### Added
- **API:** [SAPI-242] Adiciona o suporte ao pagamento a Vending Machine ([#2393](https://github.com/PicPay/picpay-ios/issues/2393))
- **BIlls:** [BILL-79] Implementa testes unitários para a tela genérica de erro [#2413](https://github.com/PicPay/picpay-ios/issues/2413)
- **Bills:** [BILL-68] Cria tela genérica de erro e troca alerta por ela no fluxo de boleto ([#2388](https://github.com/PicPay/picpay-ios/issues/2388))
- **Card:** [CCBO-2519] Tela Dados pessoais (Testes) ([#2396](https://github.com/PicPay/picpay-ios/issues/2396))
- **Card:** [CCBO-2682] Atualiza o modelo do Débito ([#2407](https://github.com/PicPay/picpay-ios/issues/2407))
- **Card:** [CCBO-2531] Criado service da tela de endereço do débito ([#2420](https://github.com/PicPay/picpay-ios/issues/2420))
- **Card:** [CCBO-2655] Tela Dados pessoais (tracking) ([#2414](https://github.com/PicPay/picpay-ios/issues/2414))
- **Card:** [CCBO-2385] DebitOnboarding - Tracking ([#2428](https://github.com/PicPay/picpay-ios/issues/2428))
- **Card:** [CCBO-2682] Tela de confirmação de endereço no fluxo do cadastro do débito ([#2415](https://github.com/PicPay/picpay-ios/issues/2415))
- **Card:** [CCBO-2519] Tela Dados pessoais (Service) ([#2386](https://github.com/PicPay/picpay-ios/issues/2386))
- **Design System:** [DSGN-450] Adiciona Shadow Radius e Shadow Offset no UISample ([#2426](https://github.com/PicPay/picpay-ios/issues/2426))
- **Engajamento:** [PPEN-416] Tratamento para novo item "Novidades" no sugestões ([#2392](https://github.com/PicPay/picpay-ios/issues/2392))
- **Engajamento:** [PPEN-427] Adiciona botão de favoritar na recarga de celular ([#2416](https://github.com/PicPay/picpay-ios/issues/2416))
- **MGM:** [MGM-171] Formulário de indicação ([#2394](https://github.com/PicPay/picpay-ios/issues/2394))
- **PCI:** [PCIDEV-536] Refatorar Parking DG ([#2425](https://github.com/PicPay/picpay-ios/issues/2425))
- **ativação:** [PPAT-590] Armazena e recupera o fluxo de cadastro interrompido ([#2408](https://github.com/PicPay/picpay-ios/issues/2408))
- **ativação:** [PPAT-587] Desenvolver tela de continuar cadastro ([#2384](https://github.com/PicPay/picpay-ios/issues/2384))
- **pagamento:** [P2P-378] Tracking tela checkout transferência externa ([#2397](https://github.com/PicPay/picpay-ios/issues/2397))
- **pagamento:** [P2P] Refatora testes que estavam quebrando de vez em quando ([#2402](https://github.com/PicPay/picpay-ios/issues/2402))
- **pagamento:** [P2P-375] Adiciona regra de negócio na tela de valor de transferência externa ([#2379](https://github.com/PicPay/picpay-ios/issues/2379))
- **user-security:** [US-233] Tela de validação de cartão parte2 ([#2406](https://github.com/PicPay/picpay-ios/issues/2406))

### Changed
- **API:** [SAPI-243] Move o payment id para o model de order ([#2437](https://github.com/PicPay/picpay-ios/issues/2437))
- **DesignSystem:** [DSGN-444] Ajustes finos para disponibilidade dos átomos ([#2411](https://github.com/PicPay/picpay-ios/issues/2411))
- **DesignSystem:** [DSGN-477] Adicionar suporte a cores na borda ([#2371](https://github.com/PicPay/picpay-ios/issues/2371))
- **Engajamento:** [PPEN-428] Unifica serviços de Favoritos ([#2418](https://github.com/PicPay/picpay-ios/issues/2418))
- **Expansão:** [EXP-252] Desvinculando P2M Scanner da Cielo e abrindo pra novos adiquirentes ([#2427](https://github.com/PicPay/picpay-ios/issues/2427))
- **Expansão:** [EXP-252] Alterações P2M ([#2445](https://github.com/PicPay/picpay-ios/issues/2445))
- **Expansão:** [exp-254] Inclusão de recusa explícita na tela de cancelamento ([#2400](https://github.com/PicPay/picpay-ios/issues/2400))
- **PCI:** [PCIDEV-546] Remover envio de senha no Header X-Authenticated ([#2409](https://github.com/PicPay/picpay-ios/issues/2409))
- **PCI:** [PCIDEV-537] Alterar Leitura da resposta de erro do backend ([#2401](https://github.com/PicPay/picpay-ios/issues/2401))
- **Plataforma:** [PLAIOS-XXX] Aumentando o timeout dos testes da RegistrationPhoneCodeViewModelTests pra reduzir falhas ([#2432](https://github.com/PicPay/picpay-ios/issues/2432))
- **Plataforma:** [PLAIOS-XXX] Aumentando o timeout dos testes da RegistrationPhoneCodeViewModelTests pra reduzir falhas [#2431](https://github.com/PicPay/picpay-ios/issues/2431)
- **Plataforma:** [PLAIOS-XXX] Corrige e padroniza teste da extensão de UIImageView ([#2436](https://github.com/PicPay/picpay-ios/issues/2436))
- **Segurança ofensiva:** [APPSEC-227] Alterando implementação base para a ofuscação de tela ([#2443](https://github.com/PicPay/picpay-ios/issues/2443))
- **cashin-cashout:** [CASH-450] Altera maneira como os dados bancários eram copiados na stackview ([#2410](https://github.com/PicPay/picpay-ios/issues/2410))
- **plataforma:** [PLAIOS-119] Ajusta imports do FeatureFlags que falha nos testes unitários ([#2424](https://github.com/PicPay/picpay-ios/issues/2424))

### Fixed
- **Plataforma:** [PLAIOS-105] Corrige bug no carregamento de imagens. ([#2399](https://github.com/PicPay/picpay-ios/issues/2399))
- **ativação:** [PPAT-597] Correções de bugs no novo fluxo de cadastro ([#2440](https://github.com/PicPay/picpay-ios/issues/2440))
- **ativação:** [PPAT-588] Corrige implementação da feature flag da nova tela inicial ([#2377](https://github.com/PicPay/picpay-ios/issues/2377))
- **plataforma:** [PLAIOS-96] Ajusta o comportamento do botão cancelar na barra de busca da tela "Pagar" ([#2405](https://github.com/PicPay/picpay-ios/issues/2405))
- **plataforma:** [PLAIOS-116] hotfix no adicionar dinheiro pelo banco original [#2398](https://github.com/PicPay/picpay-ios/issues/2398)
- **plataforma:** Ajustes para o CI enviar o changelog apenas a partir da dev ([#2389](https://github.com/PicPay/picpay-ios/issues/2389))
- **user-security:** [US] Ajuste em teste do LoginCoordinator [#2433](https://github.com/PicPay/picpay-ios/issues/2433)

### Security
- **Segurança ofensiva:** [APPSEC-227] Adicionando implementação base para a ofuscação de tela ([#2383](https://github.com/PicPay/picpay-ios/issues/2383))


<a name="v10.18.18"></a>
## [v10.18.18] - 2020-03-18
### Added
- **BACEN:** [BD-XXX] Testes da tela de bloqueio judicial ([#2336](https://github.com/PicPay/picpay-ios/issues/2336))
- **Bills:** [BILL-45] Adiciona tagueamento de eventos no pagamento de boleto ([#2362](https://github.com/PicPay/picpay-ios/issues/2362))
- **Bills:** [BILL-61] Adiciona o objeto de cip no request quando o boleto irá ser pagado ([#2353](https://github.com/PicPay/picpay-ios/issues/2353))
- **BlackOps:** [BOIO-194] Inclusão de data da geolocalização e do aparelho ([#2186](https://github.com/PicPay/picpay-ios/issues/2186))
- **Card:** [CCBO-2519] Tela Dados pessoais (Validações) ([#2340](https://github.com/PicPay/picpay-ios/issues/2340))
- **Card:** [CCBO-2385] Débito - Tela de Onboarding - ActionSheet ([#2365](https://github.com/PicPay/picpay-ios/issues/2365))
- **Card:** [CCBO-2496] DebitOnboarding - Advantages ScrollView ([#2337](https://github.com/PicPay/picpay-ios/issues/2337))
- **DesignSystem:** [DSGN-443] Adiciona suporte a ObjC para a nova palette de cores ([#2332](https://github.com/PicPay/picpay-ios/issues/2332))
- **DesignSystem:** [DSGN-341] Adiciona Tipografia ([#2370](https://github.com/PicPay/picpay-ios/issues/2370))
- **DesignSystem:** [DSGN-422] Adicionar cores no UISample ([#2360](https://github.com/PicPay/picpay-ios/issues/2360))
- **DesignSystem:** [DSGN-446] Adiciona suporte a cores HEX na palette ([#2341](https://github.com/PicPay/picpay-ios/issues/2341))
- **Engajamento:** [PPEN-308] Nova home - Aba Pagar ([#2357](https://github.com/PicPay/picpay-ios/issues/2357))
- **MGM:** [MGM-171] Indicar estabelecimento (Tela de o que é) ([#2372](https://github.com/PicPay/picpay-ios/issues/2372))
- **PCI:** [PCIDEV-510] Refatorar recarga de celular ([#2367](https://github.com/PicPay/picpay-ios/issues/2367))
- **PCI:** [PCIDEV-469] Refatorar pagamento Voucher  ([#2329](https://github.com/PicPay/picpay-ios/issues/2329))
- **PCI:** [PCIDEV-511] Refatorar recarga de TV ([#2382](https://github.com/PicPay/picpay-ios/issues/2382))
- **Plataforma:** [PLAIOS-100] Formulário de conta bancária - testes funcionais ([#2338](https://github.com/PicPay/picpay-ios/issues/2338))
- **ativação:** [PPAT-567] Implementa testes unitários para o novo fluxo de cadastro de usuários ([#2368](https://github.com/PicPay/picpay-ios/issues/2368))
- **ativação:** [PPAT-566] Implementar modal de cpf já utilizado - Eventos [#2355](https://github.com/PicPay/picpay-ios/issues/2355)
- **ativação:** [PPAT-577] Desenvolver tela de continuar cadastro - Layout ([#2363](https://github.com/PicPay/picpay-ios/issues/2363))
- **ativação:** [PPAT-564] Implementar modal de CPF já utilizado ([#2335](https://github.com/PicPay/picpay-ios/issues/2335))
- **ativação:** [PPAT-505] Implementa a chamada addConsumer para finalizar o fluxo de cadastro refatorado ([#2327](https://github.com/PicPay/picpay-ios/issues/2327))
- **card:** CCBO-2501 Exibir banner para continuar cadastro (in-progress) ([#2286](https://github.com/PicPay/picpay-ios/issues/2286))
- **pagamento:** [P2P-326] Altera texto da introdução da transferência externa [#2354](https://github.com/PicPay/picpay-ios/issues/2354)
- **pagamento:** [P2P-429] Adiciona tracking de pagamento de cobrança [#2346](https://github.com/PicPay/picpay-ios/issues/2346)
- **pagamento:** [P2P-382] Adiciona tracking da transferência disponível ([#2328](https://github.com/PicPay/picpay-ios/issues/2328))
- **pagamento:** [P2P-374] Criação layout checkout de transferência externa ([#2348](https://github.com/PicPay/picpay-ios/issues/2348))
- **plataforma:** [PLAIOS-79] Utilitario para geração de Fake/Spy/Mock's a partir de protocolos ([#2339](https://github.com/PicPay/picpay-ios/issues/2339))
- **user-security:** [US-233] Tela de validação do cartão parte 1 ([#2378](https://github.com/PicPay/picpay-ios/issues/2378))
- **user-security:** [US-227] Tela de escolha do cartão parte 2 ([#2352](https://github.com/PicPay/picpay-ios/issues/2352))

### Changed
- **DesignSystem:** [DSGN-418] Adiciona tela de detalhe no UISample em SwiftUI ([#2315](https://github.com/PicPay/picpay-ios/issues/2315))
- **Plataforma:** [PLAIOS-115] Melhoria na velocidade dos testes funcionais ([#2387](https://github.com/PicPay/picpay-ios/issues/2387))
- **Plataforma:** [JIRA-XXX] House Keeping - Removendo warnings de nullables e variáveis não utilizadas ([#2374](https://github.com/PicPay/picpay-ios/issues/2374))
- **card:** CCBO-2576 - Renomeia módulo Credit para Card ([#2325](https://github.com/PicPay/picpay-ios/issues/2325))
- **expansão:** [exp-241] Desvinculando Cielo e abrindo para novos adquirentes ([#2320](https://github.com/PicPay/picpay-ios/issues/2320))

### Fixed
- **Bills:** Corrige bug de layout no scanner de boleto que fazia o ícone de flash não aparecer ([#2331](https://github.com/PicPay/picpay-ios/issues/2331))
- **Features Management:** Exclusão do semáforo para as flags do Big Brother Brasil ([#2349](https://github.com/PicPay/picpay-ios/issues/2349))
- **card:** CCBO-2576 Adiciona linha no makefile para remover o antigo módulo Credit ([#2366](https://github.com/PicPay/picpay-ios/issues/2366))
- **plataforma:** [PLAIOS-114] ajusta testes unitários ([#2373](https://github.com/PicPay/picpay-ios/issues/2373))
- **plataforma:** [PLAIOS-113] ajustes no testes funcionais do cadastro. ([#2369](https://github.com/PicPay/picpay-ios/issues/2369))
- **plataforma:** [PLAIOS-116] hotfix no adicionar dinheiro pelo banco original [#2395](https://github.com/PicPay/picpay-ios/issues/2395)
- **plataforma:** Ajustes para o CI enviar o changelog apenas a partir da dev ([#2389](https://github.com/PicPay/picpay-ios/issues/2389))
- **universitarios:** [UNI-300] Arruma fluxo de cadastro de ContaUni  ([#2356](https://github.com/PicPay/picpay-ios/issues/2356))


<a name="v10.18.17"></a>
## [v10.18.17] - 2020-03-11
### Added
- **Bills:** [BILL-49] Adiciona label de juros e desconto ao resumo do pagamento de boleto ([#2323](https://github.com/PicPay/picpay-ios/issues/2323))
- **CARD:** [CCBO-2387] Criado tela de sucesso do envio do cartão de débito ([#2282](https://github.com/PicPay/picpay-ios/issues/2282))
- **Card:** [CCBO-2496] Strings e novos arquivos ([#2301](https://github.com/PicPay/picpay-ios/issues/2301))
- **Card:** [CCBO-2496] Onboarding Débito - ContentVIew  ([#2330](https://github.com/PicPay/picpay-ios/issues/2330))
- **Card:** [CCBO-2519] Tela Dados pessoais (Layout) ([#2312](https://github.com/PicPay/picpay-ios/issues/2312))
- **Card:** [CCBO-2559] Adiciona o Floating Label no modulo de Credit ([#2292](https://github.com/PicPay/picpay-ios/issues/2292))
- **Card:** [CCBO-2520] Criação da view da tela de adição de endereço do débito ([#2313](https://github.com/PicPay/picpay-ios/issues/2313))
- **Card:** [CCBO-2496] Debit Onboarding - Controller ([#2302](https://github.com/PicPay/picpay-ios/issues/2302))
- **Design System:** [DSGN-223] - Abstracao de Shadows ([#2252](https://github.com/PicPay/picpay-ios/issues/2252))
- **DesignSystem:** [DSGN-445] Adicionar cores de background refinando cores no Dark Mode [#2316](https://github.com/PicPay/picpay-ios/issues/2316)
- **Engajamento:** [PPEN-286-Analytics] Adicionando analytics para o novo botão de parcelamento ([#2287](https://github.com/PicPay/picpay-ios/issues/2287))
- **Engajamento:** [PPEN-310] Mapa do Estabelecimento ([#2308](https://github.com/PicPay/picpay-ios/issues/2308))
- **Plataforma:** [PLAIOS-101] Substitui arquitetura de mascara de texto ([#2297](https://github.com/PicPay/picpay-ios/issues/2297))
- **Plataforma:** [PLAIOS-99] Formulário de conta bancária - testes unitários ([#2321](https://github.com/PicPay/picpay-ios/issues/2321))
- **ativação:** [PPAT-480] Implementar novas features flags para view de CPF [#2318](https://github.com/PicPay/picpay-ios/issues/2318)
- **ativação:** [PPAT-540] Integra as telas refatoradas de cadastro ao flow coordinator de cadastro ([#2298](https://github.com/PicPay/picpay-ios/issues/2298))
- **bacen:** [BD-949] Criar tela de bloqueio Judicial ([#2194](https://github.com/PicPay/picpay-ios/issues/2194))
- **pagamento:** [P2P-379] Cria o layout da tela de transferência externa disponível ([#2307](https://github.com/PicPay/picpay-ios/issues/2307))
- **pagamento:** [P2P-380] Implementa a lógica no view model da transferência disponível
- **pagamento:** [P2P-381] Finaliza a lógica da transferência disponível ([#2319](https://github.com/PicPay/picpay-ios/issues/2319))
- **plataforma:** [PLAIOS-90] Inserção do fluxo de verificação de telefone que tinha ficado de fora. ([#2255](https://github.com/PicPay/picpay-ios/issues/2255))
- **user-security:** [US-227] Tela de validação do TFA com cartão parte 1 ([#2322](https://github.com/PicPay/picpay-ios/issues/2322))
- **user-security:** [US-205] Adiciona testes do Coordinator ([#2276](https://github.com/PicPay/picpay-ios/issues/2276))
- **user-security:** [US-221] Nova tela de sucesso do TFA ([#2291](https://github.com/PicPay/picpay-ios/issues/2291))

### Changed
- **CARD:** [CCBO-2538] Move UIPPFloatingTextField para o modulo de UI ([#2277](https://github.com/PicPay/picpay-ios/issues/2277))
- **DesignSystem:** [DSGN-324] Altera UISample para SwiftUI ([#2285](https://github.com/PicPay/picpay-ios/issues/2285))
- **Engajamento:** [PPEN-402] Fixa botão de pagar estabelecimento ([#2343](https://github.com/PicPay/picpay-ios/issues/2343))
- **Engajamento:** [PPEN-286-P2P] Alteração layout novo botão de parcelamento (p2p) ([#2278](https://github.com/PicPay/picpay-ios/issues/2278))
- **Engajamento:** [PPEN-403] Muda endpoint da API de Promoções ([#2344](https://github.com/PicPay/picpay-ios/issues/2344))
- **Engajamento:** [PPEN-286-P2B] Alteração layout novo botão de parcelamento (p2b) ([#2280](https://github.com/PicPay/picpay-ios/issues/2280))
- **Pagamento:** [P2P-340] Refatora fluxo de cobrança por QR Code ([#2244](https://github.com/PicPay/picpay-ios/issues/2244))
- **Plataforma:** [PLAIOS-98] Implementação das camadas de ViewModel, Presenter, Service e Coordinator do formulário de conta bancária ([#2304](https://github.com/PicPay/picpay-ios/issues/2304))
- **ativação:** [PPAT-552] Implementado alerta de confirmação de telefone ([#2300](https://github.com/PicPay/picpay-ios/issues/2300))
- **ativação:** [PPAT-504] Revisar os eventos no fluxo de cadastro ([#2310](https://github.com/PicPay/picpay-ios/issues/2310))
- **pagamento:** [P2P-338] Refatora a scene PaymentRequestValue ([#2246](https://github.com/PicPay/picpay-ios/issues/2246))
- **pagamento:** [P2P-379] Abstrai a modal de transferência externa ([#2296](https://github.com/PicPay/picpay-ios/issues/2296))
- **pagamento:** [P2P] Corrige de loader, url e testes unitários ([#2279](https://github.com/PicPay/picpay-ios/issues/2279))
- **universitarios:** [UNI-232] Dynamic Link para MGM e cadastro de estudante ([#2324](https://github.com/PicPay/picpay-ios/issues/2324))
- **user-security:** [US-229] Organizar arquivos do fluxo de TFA ([#2305](https://github.com/PicPay/picpay-ios/issues/2305))

### Fixed
- **Plataforma:** [PLAIOS-XXX] Exibir taxa de pagamento na tela de aniversario ([#2342](https://github.com/PicPay/picpay-ios/issues/2342))
- **plataforma:** Melhorias na resposta do comando `make buildspm` ([#2294](https://github.com/PicPay/picpay-ios/issues/2294))
- **plataforma:** [PLAIOS-84] Ajustes em testes assíncronos ([#2261](https://github.com/PicPay/picpay-ios/issues/2261))


<a name="v10.18.16"></a>
## [v10.18.16] - 2020-03-04
### Added
- **Card:** [CCBO-24961] Feature Débito - Tela Onboarding - Arquivos criados ([#2272](https://github.com/PicPay/picpay-ios/issues/2272))
- **DesignSystem:** [DSGN-323] Adiciona calculo de contraste de cores ([#2236](https://github.com/PicPay/picpay-ios/issues/2236))
- **Engajamento:** [PPEN-286-Card] Novo botão de parcelamento para telas de pagamento (PicPay Card) ([#2264](https://github.com/PicPay/picpay-ios/issues/2264))
- **PAY:** [PAY-411] - Loading bloqueando ações do usuário quando Débito. ([#2251](https://github.com/PicPay/picpay-ios/issues/2251))
- **PAY:** [PAY-412] - Casos de erro do BB para Tela especifica. ([#2208](https://github.com/PicPay/picpay-ios/issues/2208))
- **Pci:** [PCIDEV-495] Adequação PCI para o pagamento Linx ([#2250](https://github.com/PicPay/picpay-ios/issues/2250))
- **ativação:** [PPAT-511] Implementa a lógica da tela refatorada de cadastrar CPF e data de nascimento ([#2270](https://github.com/PicPay/picpay-ios/issues/2270))
- **ativação:** [PPAT-527] Navegação entre a tela de telefone e validação de telefone [#2266](https://github.com/PicPay/picpay-ios/issues/2266)
- **ativação:** [PPAT-539] Implementa a Coordinator e Presenter da tela refatorada de cadastrar CPF e data de nascimento ([#2271](https://github.com/PicPay/picpay-ios/issues/2271))
- **ativação:** [PPAT-503] Refatora a view da tela de documentos no cadastro de novo usuário ([#2199](https://github.com/PicPay/picpay-ios/issues/2199))
- **bills:** [QI-39]) Adiciona tratativa para o fluxo de pmv ([#2181](https://github.com/PicPay/picpay-ios/issues/2181))
- **card:** CCBO-2513 Cria feature flag para fluxo de oferta do cartão de débito ([#2275](https://github.com/PicPay/picpay-ios/issues/2275))
- **plataforma:** [PLAIOS-89] adicionado firebase performance ([#2260](https://github.com/PicPay/picpay-ios/issues/2260))
- **user-security:** [US-205] Nova tela de validação de código TFA testes do viewModel ([#2257](https://github.com/PicPay/picpay-ios/issues/2257))
- **user-security:** [US-205] Nova tela de validação de código TFA testes do Presenter ([#2268](https://github.com/PicPay/picpay-ios/issues/2268))
- **user-security:** [US-205] Adicionando testes do TFAFlowCoordinator. ([#2243](https://github.com/PicPay/picpay-ios/issues/2243))

### Changed
- **Plataforma:** [PLAIOS-81] Refatoração do fluxo de conta bancária (Formulário de conta bancária - Tela apenas)  ([#2263](https://github.com/PicPay/picpay-ios/issues/2263))
- **Plataforma:** [PLAIOS-80] Refatoração do fluxo de conta bancária (Testes unitários Lista de bancos) ([#2233](https://github.com/PicPay/picpay-ios/issues/2233))
- **ativação:** [PPAT-XXX] Implementado itens faltantes na view de validação de telefone [#2262](https://github.com/PicPay/picpay-ios/issues/2262)
- **ativação:** [PPAT-522] Implementado presenter na tela de validação de telefone ([#2254](https://github.com/PicPay/picpay-ios/issues/2254))
- **ativação:** [PPAT-521] Requests da tela de validação de telefone ([#2248](https://github.com/PicPay/picpay-ios/issues/2248))
- **plataforma:** [PLAIOS-67] adicionado Firebase Crashlytics

### Fixed
- **Card:** [CCBO-2138] Fix update view appeareance problem ([#2242](https://github.com/PicPay/picpay-ios/issues/2242))
- **ativação:** [PPAT-477] Bug onde o escaner principal para de escanear após fechar uma tela no novo modal sobre ele ([#2253](https://github.com/PicPay/picpay-ios/issues/2253))
- **pagamento:** [P2P] Corrige origin da seção de contatos na tela de busca ([#2256](https://github.com/PicPay/picpay-ios/issues/2256))
- **plataforma:** [PLAIOS-86] correção testes funcionais com remote config ([#2245](https://github.com/PicPay/picpay-ios/issues/2245))


<a name="v10.18.15"></a>
## [v10.18.15] - 2020-02-26
### Added
- **CARD:** [CCBO-2393] Serviço do onboarding do cashback ([#2175](https://github.com/PicPay/picpay-ios/issues/2175))
- **DesignSystem:** [DSGN-295] Adicionar nova Palette (Colors) ([#2221](https://github.com/PicPay/picpay-ios/issues/2221))
- **Engajamento:** [PPEN-322] Promoções já utilizadas ([#2224](https://github.com/PicPay/picpay-ios/issues/2224))
- **Engajamento:** [PPEN-286-DG] Novo botão de parcelamento para telas de pagamento (Digital Goods) ([#2215](https://github.com/PicPay/picpay-ios/issues/2215))
- **PAY:** Adicionados dados do 3DS no Analytics para erros de transação ([#2165](https://github.com/PicPay/picpay-ios/issues/2165))
- **PCI:** [PCIDEV-336] Refatorar rota verify card para PCI ([#2238](https://github.com/PicPay/picpay-ios/issues/2238))
- **ativação:** [PPAT-377] Cria flow coordinator para o fluxo de cadastro de usuário ([#2166](https://github.com/PicPay/picpay-ios/issues/2166))
- **pagamento:** [P2P-269]Adiciona tracking no fluxo de transferência externa ([#2219](https://github.com/PicPay/picpay-ios/issues/2219))
- **pagamento:** [P2P-268] Adiciona integracao backend real ([#2229](https://github.com/PicPay/picpay-ios/issues/2229))
- **user-security:** [US-205] Nova tela validação de código TFA parte 2 ([#2230](https://github.com/PicPay/picpay-ios/issues/2230))
- **user-security:** [US-205] Nova tela de validação de código TFA parte 1 ([#2220](https://github.com/PicPay/picpay-ios/issues/2220))
- **user-security:** [US-201] Ajustar os fluxos de validação para usarem os novos endpoints. ([#2193](https://github.com/PicPay/picpay-ios/issues/2193))

### Changed
- **CARD:** [CCBO-2457] Tela Onboarding do cartão com cashback com feature flag ([#2200](https://github.com/PicPay/picpay-ios/issues/2200))
- **Card:** [CCBO-2264] Cashback Flow - Parte 1 - Onboarding ([#2209](https://github.com/PicPay/picpay-ios/issues/2209))
- **Card:** [CCBO-2264] Cashback Flow - Parte 4 - Implementation ([#2213](https://github.com/PicPay/picpay-ios/issues/2213))
- **Plataforma:** [PLAIOS-82] Refatoração do fluxo de conta bancária (Testes funcionais Lista de bancos) ([#2237](https://github.com/PicPay/picpay-ios/issues/2237))
- **Plataforma:** [PLAIOS-42] Remove SDWebImage e substitui por extension do UIImageView  ([#2072](https://github.com/PicPay/picpay-ios/issues/2072))
- **Plataforma:** [PLAIOS-64] Refatoração do fluxo de conta bancária (Lista de bancos) ([#2212](https://github.com/PicPay/picpay-ios/issues/2212))
- **Plataforma:** [PLAIOS-76] Melhoria sintaxe Feature Flags ([#2142](https://github.com/PicPay/picpay-ios/issues/2142))
- **ativação:** [PPAT-512] Refatoração da tela de validação de telefone - ViewModel ([#2227](https://github.com/PicPay/picpay-ios/issues/2227))
- **empresas:** [B2B-434] Adiciona requests de body contendo Data ([#2143](https://github.com/PicPay/picpay-ios/issues/2143))

### Fixed
- **Engajamento:** [PPEN-XXX] Ajusta parser do detalhe da promoção ([#2232](https://github.com/PicPay/picpay-ios/issues/2232))
- **card:** CCBO-2462 Refatora deeplink/Delegate do fluxo de solicitar o cartão ([#2225](https://github.com/PicPay/picpay-ios/issues/2225))
- **card:** CCBO-2461 Refatora deeplink/delegate do fluxo de ativar débito ([#2218](https://github.com/PicPay/picpay-ios/issues/2218))
- **card:** CCBO-2460 Refatora deeplink/delegate do fluxo de ativar cartão ([#2217](https://github.com/PicPay/picpay-ios/issues/2217))
- **pagamento:** [P2P] Corrige testes do fluxo de cobrança ([#2222](https://github.com/PicPay/picpay-ios/issues/2222))

### Removed
- **universitarios:** [UNI-235] Remove tela de onboarding do cadastro da conta de estudante ([#2202](https://github.com/PicPay/picpay-ios/issues/2202))


<a name="v10.18.14"></a>
## [v10.18.14] - 2020-02-24
### Fixed
- **plataforma:** Identificação do usuario no AppsFlyer após o login ([#2231](https://github.com/PicPay/picpay-ios/issues/2231))


<a name="v10.18.12"></a>
## [v10.18.12] - 2020-02-19
### Added
- **Card:** [CCBO-2396] Analytics do fluxo de Ativação do Card ([#2196](https://github.com/PicPay/picpay-ios/issues/2196))
- **Card:** [CCBO-2395] Analytics do fluxo de solicitar o Picpay Card ([#2172](https://github.com/PicPay/picpay-ios/issues/2172))
- **Engajamento:** [PPEN-286] Refatorando botão de parcelamento - pagamento p2p ([#2132](https://github.com/PicPay/picpay-ios/issues/2132))
- **Engajamento:** [PPEN-226] Promoções por localização ([#2174](https://github.com/PicPay/picpay-ios/issues/2174))
- **Engajamento:** [PPEN-294] Aviso sobre localização ([#2133](https://github.com/PicPay/picpay-ios/issues/2133))
- **Pagamento:** [PPEN-267] Implementa lógica da tela intro do fluxo de transferência externa ([#2189](https://github.com/PicPay/picpay-ios/issues/2189))
- **ativação:** [PPAT-502] Refatora a tela de criar senha no cadastro do usuário ([#2185](https://github.com/PicPay/picpay-ios/issues/2185))
- **card:** [CCBO-2246/2394] Adiciona oferta de inscrição do programa de cashback na carteira com feature flag ([#2184](https://github.com/PicPay/picpay-ios/issues/2184))
- **expansão:** [exp-105] Criação da tela de confirmação de cancelamento ([#2190](https://github.com/PicPay/picpay-ios/issues/2190))
- **expansão:** [exp-105] Criação da tela de confirmação de cancelamento ([#2167](https://github.com/PicPay/picpay-ios/issues/2167))
- **pagamento:** [P2P 239] Adiciona tela de intro do fluxo de transferência externa ([#2176](https://github.com/PicPay/picpay-ios/issues/2176))
- **user-security:** [US- 161] Nova arquitetura para a tela de opções do TFA. ([#2173](https://github.com/PicPay/picpay-ios/issues/2173))

### Changed
- **Engajamento:** [PPEN-317] Muda texto e imagens do onboarding de favoritos ([#2187](https://github.com/PicPay/picpay-ios/issues/2187))
- **Plataforma:** [PLAIOS-64] Refatoração do fluxo de conta bancária (Tela de instruções e Dados Bancários) ([#2140](https://github.com/PicPay/picpay-ios/issues/2140))
- **Squad:** [CCBO-2401] Onboarding Card - Alteração de texto  ([#2182](https://github.com/PicPay/picpay-ios/issues/2182))
- **ativação:** [PPAT-458] Refatorada tela de telefone ([#2177](https://github.com/PicPay/picpay-ios/issues/2177))
- **expansão:** [EXP-105] Scene de confirmação de cancelamento ([#2195](https://github.com/PicPay/picpay-ios/issues/2195))

### Fixed
- **Card:** [CCBO-2416] Mudança no envio de parametros do cardPin ([#2188](https://github.com/PicPay/picpay-ios/issues/2188))
- **ativação:** [PPAT-] Corrige dark mode na tela de editar apelido do cartão ([#2169](https://github.com/PicPay/picpay-ios/issues/2169))
- **expansão:** [exp-105] Ajuste pra tela funcionar bem com o DarkMode ([#2197](https://github.com/PicPay/picpay-ios/issues/2197))
- **pci:** [PCI-XXX] Ajuste Currency Field ([#2192](https://github.com/PicPay/picpay-ios/issues/2192))

### Security
- **Segurança ofensiva:** [OS-57] Adição de Pinning nas requests para nossos endpoints ([#2109](https://github.com/PicPay/picpay-ios/issues/2109))


<a name="v10.18.11"></a>
## [v10.18.11] - 2020-02-13
### Added
- **CARD:** [CCBO-2248] Tela de boas vindas do fluxo de cashback ([#2080](https://github.com/PicPay/picpay-ios/issues/2080))
- **Card:** [CCBO-2247] Programa de Cashback - Tela de Onboarding ([#2091](https://github.com/PicPay/picpay-ios/issues/2091))
- **DesignSystem:** [DSGN-151] Adicionar Corner Radius no UISample ([#2161](https://github.com/PicPay/picpay-ios/issues/2161))
- **DesignSystem:** [DSGN-160] UI Sample adicionado ([#2110](https://github.com/PicPay/picpay-ios/issues/2110))
- **DesignSystem:** [DSGN-152] Adiciona Spacing no UISample ([#2164](https://github.com/PicPay/picpay-ios/issues/2164))
- **Engajamento:** [PPEN-181] Onboarding de promoções ([#2019](https://github.com/PicPay/picpay-ios/issues/2019))
- **Engajamento:** [PPEN-200] Integração com o serviço de promoções ([#2087](https://github.com/PicPay/picpay-ios/issues/2087))
- **Engajamento:** [PPEN-237] Deep linking de Promoções ([#2098](https://github.com/PicPay/picpay-ios/issues/2098))
- **Engajamento:** [PPEN-48] Implementação de Testes ([#2093](https://github.com/PicPay/picpay-ios/issues/2093))
- **Engajamento:** [PPEN-256] Adicionando deeplink para acessar onboarding do Favoritos ([#2096](https://github.com/PicPay/picpay-ios/issues/2096))
- **PCI:** [PAY] Merge da feature PCI com a DEV ([#2038](https://github.com/PicPay/picpay-ios/issues/2038))
- **ativação:** [PPAT-261] Implementar tela de sucesso do código conta uni ([#2016](https://github.com/PicPay/picpay-ios/issues/2016))
- **ativação:** [PPAT-456] Refatora a tela de cadastro de email ([#2152](https://github.com/PicPay/picpay-ios/issues/2152))
- **card:** [CCBO-2342] Tela de bem-vindo do cashback ([#2115](https://github.com/PicPay/picpay-ios/issues/2115))
- **expansão:** [EXP-105] Recriar tela de autorização de cancelamento ([#2090](https://github.com/PicPay/picpay-ios/issues/2090))
- **iptu:** [QI-37] Adiciona header de descrição na lista de boletos ([#2149](https://github.com/PicPay/picpay-ios/issues/2149))
- **iptu:** [QI-36] Adiciona placeholder com o título do boleto na hora do pagamento ([#2117](https://github.com/PicPay/picpay-ios/issues/2117))
- **iptu:** [QI-35] Ajusta detalhes no layout e adiciona botão para help center ([#2079](https://github.com/PicPay/picpay-ios/issues/2079))
- **pagamento:** [P2P-239] Adiciona opção de transferência externa nas sugestões ([#2168](https://github.com/PicPay/picpay-ios/issues/2168))
- **pagamento:** [P2P] Feature de cobrança ([#1916](https://github.com/PicPay/picpay-ios/issues/1916))
- **plataforma:** [PLAIOS-17] Módulo de Feature Flags ([#2054](https://github.com/PicPay/picpay-ios/issues/2054))
- **plataforma:** FeatureManagerMock para ser usado com o DependencyContainerMock ([#2089](https://github.com/PicPay/picpay-ios/issues/2089))
- **plataforma:** [PLAIOS-72] Target de testes do módulo Core [#2123](https://github.com/PicPay/picpay-ios/issues/2123)
- **user-security:** [US-160] Nova arquitetura para as telas informativas ([#2088](https://github.com/PicPay/picpay-ios/issues/2088))

### Changed
- **3ds:** [PAY-405] Alterar error do 3DS pelo error code ([#2083](https://github.com/PicPay/picpay-ios/issues/2083))
- **DesignSystem:**  [DSGN-220] Atualização do README do átomos de design ([#2112](https://github.com/PicPay/picpay-ios/issues/2112))
- **Engajamento:** [PPEN-48] Ajuste na implementação das constraints e Layout da feature seguindo novo padrão. ([#2073](https://github.com/PicPay/picpay-ios/issues/2073))
- **Plataforma:** [PLAIOS-60] Refatorar para nova arquitetura o fluxo de trocar senha ([#2078](https://github.com/PicPay/picpay-ios/issues/2078))
- **ativação:** [PPAT-483-484] Remover o evento "Lading Screen - Viewed" e enviar outros somente para o firebase ([#2138](https://github.com/PicPay/picpay-ios/issues/2138))
- **ativação:** [PPAT-455] Refatorada tela de nome e sobrenome ([#2129](https://github.com/PicPay/picpay-ios/issues/2129))
- **card:** Corrige link do faq na tela de seleção de endereço ([#2157](https://github.com/PicPay/picpay-ios/issues/2157))

### Fixed
- **Engajamento:** Ajuste das constraints dos itens do carrossel do Favoritos. ([#2139](https://github.com/PicPay/picpay-ios/issues/2139))
- **Engajamento:** [PPEN-236] Ajustar leitura do QRCode 24horas ([#2094](https://github.com/PicPay/picpay-ios/issues/2094))
- **Expansão:** [EXP-165] Ajuste erro linx ([#2127](https://github.com/PicPay/picpay-ios/issues/2127))
- **Expansão:** [EXP-165] Ajuste no Pagamento Linx ([#2106](https://github.com/PicPay/picpay-ios/issues/2106))
- **blackops:** Corrige bug de loading quando o app acionava o PPAuth ([#2095](https://github.com/PicPay/picpay-ios/issues/2095))
- **card:** [CCBO-2375] - Corrige erro ao exibir oferta de solicitar o cartão [#2128](https://github.com/PicPay/picpay-ios/issues/2128)
- **card:** [CCBO-2373] Corrige testes unitários que estão quebrando ([#2111](https://github.com/PicPay/picpay-ios/issues/2111))
- **plataforma:** [PLAIOS-69] Remoção de arquivos gerados desnecessários [#2114](https://github.com/PicPay/picpay-ios/issues/2114)
- **plataforma:** [PLAIOS-69] Habilita todas as suite de testes do app e módulos ([#2085](https://github.com/PicPay/picpay-ios/issues/2085))
- **plataforma:** Fix do build do módulo UI usando o SPM  ([#2105](https://github.com/PicPay/picpay-ios/issues/2105))
- **user-security:** [US-194] Ajustar fluxo de validação TFA para usuário sem telefone cadastrado. ([#2141](https://github.com/PicPay/picpay-ios/issues/2141))

### Removed
- **universitarios:** [UNI-228] Remove FeatureFlag para que opção de Conta Universitária apareça em Ajustes ([#2162](https://github.com/PicPay/picpay-ios/issues/2162))


<a name="v10.18.9"></a>
## [v10.18.9] - 2020-02-06

<a name="v10.18.8"></a>
## [v10.18.8] - 2020-02-06

<a name="v10.18.10"></a>
## [v10.18.10] - 2020-02-05
### Added
- **CARD:** [CCBO-2248] Tela de boas vindas do fluxo de cashback ([#2080](https://github.com/PicPay/picpay-ios/issues/2080))
- **Engajamento:** [PPEN-256] Adicionando deeplink para acessar onboarding do Favoritos ([#2096](https://github.com/PicPay/picpay-ios/issues/2096))
- **Engajamento:** [PPEN-237] Deep linking de Promoções ([#2098](https://github.com/PicPay/picpay-ios/issues/2098))
- **Engajamento:** [PPEN-181] Onboarding de promoções ([#2019](https://github.com/PicPay/picpay-ios/issues/2019))
- **Engajamento:** [PPEN-200] Integração com o serviço de promoções ([#2087](https://github.com/PicPay/picpay-ios/issues/2087))
- **PCI:** [PAY] Merge da feature PCI com a DEV ([#2038](https://github.com/PicPay/picpay-ios/issues/2038))
- **ativação:** [PPAT-261] Implementar tela de sucesso do código conta uni ([#2016](https://github.com/PicPay/picpay-ios/issues/2016))
- **expansão:** [EXP-105] Recriar tela de autorização de cancelamento ([#2090](https://github.com/PicPay/picpay-ios/issues/2090))
- **iptu:** [QI-35] Ajusta detalhes no layout e adiciona botão para help center ([#2079](https://github.com/PicPay/picpay-ios/issues/2079))
- **iptu:** [QI-36] Adiciona placeholder com o título do boleto na hora do pagamento ([#2117](https://github.com/PicPay/picpay-ios/issues/2117))
- **plataforma:** [PLAIOS-72] Target de testes do módulo Core [#2123](https://github.com/PicPay/picpay-ios/issues/2123)
- **user-security:** [US-160] Nova arquitetura para as telas informativas ([#2088](https://github.com/PicPay/picpay-ios/issues/2088))

### Changed
- **Engajamento:** [PPEN-48] Ajuste na implementação das constraints e Layout da feature seguindo novo padrão. ([#2073](https://github.com/PicPay/picpay-ios/issues/2073))

### Fixed
- **Engajamento:** [PPEN-236] Ajustar leitura do QRCode 24horas ([#2094](https://github.com/PicPay/picpay-ios/issues/2094))
- **Expansão:** [EXP-165] Ajuste erro linx ([#2127](https://github.com/PicPay/picpay-ios/issues/2127))
- **Expansão:** [EXP-165] Ajuste no Pagamento Linx ([#2106](https://github.com/PicPay/picpay-ios/issues/2106))
- **blackops:** Corrige bug de loading quando o app acionava o PPAuth ([#2095](https://github.com/PicPay/picpay-ios/issues/2095))
- **card:** [CCBO-2375] - Corrige erro ao exibir oferta de solicitar o cartão [#2128](https://github.com/PicPay/picpay-ios/issues/2128)
- **card:** [CCBO-2373] Corrige testes unitários que estão quebrando ([#2111](https://github.com/PicPay/picpay-ios/issues/2111))
- **plataforma:** [PLAIOS-69] Remoção de arquivos gerados desnecessários [#2114](https://github.com/PicPay/picpay-ios/issues/2114)
- **plataforma:** [PLAIOS-69] Habilita todas as suite de testes do app e módulos ([#2085](https://github.com/PicPay/picpay-ios/issues/2085))
- **plataforma:** Fix do build do módulo UI usando o SPM  ([#2105](https://github.com/PicPay/picpay-ios/issues/2105))


<a name="v10.18.7"></a>
## [v10.18.7] - 2020-01-29
### Added
- **3DS:** [PAY-396] Excluir Cartão no Fluxo do 3DS ([#2037](https://github.com/PicPay/picpay-ios/issues/2037))
- **Card:** [CCBO-2114] Migração das regras para leitura dos novos atributos ([#2024](https://github.com/PicPay/picpay-ios/issues/2024))
- **DesignSystem:** [DSGN-139] Adiciona token de Spacing (margin padrão) ([#2077](https://github.com/PicPay/picpay-ios/issues/2077))
- **DesignSystem:** [DSGN-143] Adiciona token de arredondamento das bordas do elemento ([#2067](https://github.com/PicPay/picpay-ios/issues/2067))
- **DesignSystem:** Adiciona token de espessura de Borda ([#2039](https://github.com/PicPay/picpay-ios/issues/2039))
- **Engajamento:** [PPEN-48] Lista de contatos para recarga de celular. ([#1964](https://github.com/PicPay/picpay-ios/issues/1964))
- **Engajamento:** [PPEN-161] Listagem de promoções ([#2015](https://github.com/PicPay/picpay-ios/issues/2015))
- **ativação:** [PPAT-271] Implementa validação de código promocional antes do cadastro ([#2025](https://github.com/PicPay/picpay-ios/issues/2025))
- **ativação:** [PPAT-298] Carregar feature flags antes de lançar a tela inicial ([#1982](https://github.com/PicPay/picpay-ios/issues/1982))
- **ativação:** [PPAT-272] Envia o código promocional inserido na tela inicial para ser ativado no final do fluxo de cadastro ([#2056](https://github.com/PicPay/picpay-ios/issues/2056))
- **bacen:** [PAY-XXX] Adicionar suporte a badge de verify no PAV ([#2034](https://github.com/PicPay/picpay-ios/issues/2034))
- **iptu:** [QI-28] Cria lógica de roteamento para a DGBoletoViewController ([#2022](https://github.com/PicPay/picpay-ios/issues/2022))
- **plataforma:** [PLAIOS-53] Módulo de Analytics  ([#2032](https://github.com/PicPay/picpay-ios/issues/2032))
- **universitarios:** [UNI-20] Adiciona ícone de cashback para contaUni ([#2050](https://github.com/PicPay/picpay-ios/issues/2050))
- **universitarios:** [UNI-140] Tracking de evento aba Ajustes item Conta Universitaria ([#2053](https://github.com/PicPay/picpay-ios/issues/2053))
- **universitários:** [UNI-115] Conta Universitária em Ajustes redirecionando para webview de ativação ([#2003](https://github.com/PicPay/picpay-ios/issues/2003))
- **user-security:** [US-121] atualizar fluxo do iOS para validação de dados no TFA com fallback ([#2063](https://github.com/PicPay/picpay-ios/issues/2063))

### Changed
- **CARD:** [CCBO-2329] Alteração do nome dos parâmetros  ([#2081](https://github.com/PicPay/picpay-ios/issues/2081))
- **Card:** [CCBO-2263] Alterado tela de onboarding do cartão ([#2064](https://github.com/PicPay/picpay-ios/issues/2064))
- **Card:** [CCBO-2297] Alteração das regras para o CardSettings ([#2044](https://github.com/PicPay/picpay-ios/issues/2044))

### Fixed
- **Engajamento:** [PPEN-XXX] Carregamento das imagens dos itens no carrossel do Favoritos ([#2051](https://github.com/PicPay/picpay-ios/issues/2051))
- **card:** [CCBO-2298] Aplica darkmode na tela de resumo do contato ([#2071](https://github.com/PicPay/picpay-ios/issues/2071))
- **card:** [CCBO-2295] Corrige ciclo de vida do iOS13 na PaymentViewControler para o fluxo de registro do card ([#2041](https://github.com/PicPay/picpay-ios/issues/2041))
- **card:** [CCBO-2293] Corrige ciclo de vida do iOS13 na PaymentViewControler para os fluxos do módulo de credito
- **plataforma:** [PLAIO-59] Crash no PPAuth ao exibir erro usando AlertMessage [#2070](https://github.com/PicPay/picpay-ios/issues/2070)
- **plataforma:** Adiciona dependencia ao Package.swift [#2029](https://github.com/PicPay/picpay-ios/issues/2029)


<a name="v10.18.6"></a>
## [v10.18.6] - 2020-01-27

<a name="v10.18.5"></a>
## [v10.18.5] - 2020-01-24

<a name="v10.18.4"></a>
## [v10.18.4] - 2020-01-21

<a name="v10.18.3"></a>
## [v10.18.3] - 2020-01-20
### Added
- **Card:** [CCBO-2146] Analytics Tracking ([#1993](https://github.com/PicPay/picpay-ios/issues/1993))
- **Engajamento:** [PPEN-137] Empty state da tela de promoções ([#1971](https://github.com/PicPay/picpay-ios/issues/1971))
- **Engajamento:** [PPEN-XXX] Acessar onboarding dos Favoritos pela lista de Favoritos em Ajustes ([#1979](https://github.com/PicPay/picpay-ios/issues/1979))
- **Expansão:** [EXP-62] Implementa Fluxo de P2M para terminais Linx ([#2008](https://github.com/PicPay/picpay-ios/issues/2008))
- **Feature Management:** Features Flags para o Big Brother Brasil ([#2005](https://github.com/PicPay/picpay-ios/issues/2005))
- **ativação:** [PPAT-296] Cria nova tela de signUp ([#1959](https://github.com/PicPay/picpay-ios/issues/1959))
- **ativação:** [PPAT-301] Envio de código pelo WhastApp ([#1975](https://github.com/PicPay/picpay-ios/issues/1975))
- **ativação:** [PPAT-326] Implementação da tela de sucesso - MGM ([#1998](https://github.com/PicPay/picpay-ios/issues/1998))
- **card:** [CCBO-1799] Exibe popup informativo na tela de pagamento ([#1966](https://github.com/PicPay/picpay-ios/issues/1966))
- **iptu:** [QI-31] Adiciona Feature Toggle e ajusta Scanner para o IPTU ([#1995](https://github.com/PicPay/picpay-ios/issues/1995))
- **iptu:** [QI-20] Cria a base da arquitetura da feature de IPTU ([#1986](https://github.com/PicPay/picpay-ios/issues/1986))
- **iptu:** [QI-29] Cria lógica de apresentação do QRCodeBills ([#2001](https://github.com/PicPay/picpay-ios/issues/2001))
- **iptu:** [QI-30] Cria layout da listagem de boletos ([#2014](https://github.com/PicPay/picpay-ios/issues/2014))
- **plataforma:** [PLAIOS-55] Container de injeção de dependencia ([#1977](https://github.com/PicPay/picpay-ios/issues/1977))

### Changed
- **3DS:** [PAY-382] Travar a tela após o usuário clicar no botão de criar recarga

### Fixed
- **Card:** [CCBO-2231] Adicionado darkmode na tela de Análise de Dados
- **Engajamento:** Ajustes de teste. ([#1988](https://github.com/PicPay/picpay-ios/issues/1988))
- **blackops:** [BOIO-279] Corrige erro no build de SignUpService em prod ([#2011](https://github.com/PicPay/picpay-ios/issues/2011))
- **blackops:** [BOIO-233] Corrige cor dos textos das notificações quando estão no estado highlighted ([#1957](https://github.com/PicPay/picpay-ios/issues/1957))
- **card:** [CCBO-2233] - Adiciona darkmode na tela de PicPay Card Ativado
- **card:** [CCBO-2206] Corrige darkmode na tela de data de vencimento ([#1987](https://github.com/PicPay/picpay-ios/issues/1987))
- **universitários:** [UNI-17] Arruma fluxo de navegação de webview de beneficios ([#1976](https://github.com/PicPay/picpay-ios/issues/1976))
- **user-security:** [US-131] Feature flag para esconder a opção de TFA por SMS. ([#1978](https://github.com/PicPay/picpay-ios/issues/1978))


<a name="v10.18.2"></a>
## [v10.18.2] - 2020-01-08
### Added
- **Engajamento:** [PPEN-140] Tracking para feature Favoritos ([#1939](https://github.com/PicPay/picpay-ios/issues/1939))
- **ativação:** [PPAT-370] Viu e não viu a nova tela de comunicação de SMS ([#1942](https://github.com/PicPay/picpay-ios/issues/1942))
- **plataforma:** [PLAIOS-50] Adicionar item de beta na sessão de geral em ajustes ([#1960](https://github.com/PicPay/picpay-ios/issues/1960))
- **plataforma:** [PLAIOS-45] Template MVVM-C ([#1961](https://github.com/PicPay/picpay-ios/issues/1961))

### Changed
- **blackops:** [BOIO-241] Refatora EcommercePayment para o novo padrão de arquitetura ([#1897](https://github.com/PicPay/picpay-ios/issues/1897))
- **plataforma:** [PLAIOS-46] Move códigos legados para pasta PicPay/Legacy ([#1958](https://github.com/PicPay/picpay-ios/issues/1958))

### Fixed
- **backops:** [BOIO-274] Corrige as cores da tela de login ([#1943](https://github.com/PicPay/picpay-ios/issues/1943))
- **blackops:** [BOIO-273] Crash ao desativar conta após refatoração da funcionalidade ([#1930](https://github.com/PicPay/picpay-ios/issues/1930))
- **plataforma:** [PLAIOS-XXX] Ajuste de Thread EcommercePayment
- **plataforma:** [PLAIOS-49] bug que dava crash no aplicativo ao selecionar um banco quando a pesquisa tivesse sido acionada ([#1956](https://github.com/PicPay/picpay-ios/issues/1956))


<a name="V10.18.1"></a>
## [V10.18.1] - 2020-01-04

<a name="v10.18.0"></a>
## [v10.18.0] - 2020-01-02
### Added
- **Engajamento:** [PPEN-132] Adiciona botão de promoções na home ([#1924](https://github.com/PicPay/picpay-ios/issues/1924))
- **Engajamento:** Adiciona tracking de erro no 24 horas ([#1935](https://github.com/PicPay/picpay-ios/issues/1935))
- **Engajamento:** Adiciona Deeplink para Feed ([#1927](https://github.com/PicPay/picpay-ios/issues/1927))
- **Engajamento:** Feature Favoritos na Home e fluxo Onboarding ([#1895](https://github.com/PicPay/picpay-ios/issues/1895))
- **Engajamento:** [PPEN-97] Enviar dados pela WebView para tracking ([#1913](https://github.com/PicPay/picpay-ios/issues/1913))
- **ativação:** [PPAT-261] Melhorar comunicação da tela de CPF ([#1917](https://github.com/PicPay/picpay-ios/issues/1917))
- **ativação:** [PPAT-243] Cria a feature de apresentação e sugestões de cash-in no onboarding ([#1906](https://github.com/PicPay/picpay-ios/issues/1906))
- **plataforma:** Adiciona compatibilidade com sections no TableViewHandler ([#1918](https://github.com/PicPay/picpay-ios/issues/1918))
- **plataforma:** [PLAIOS-34] Suporte ao SPM nos módulos Core e UI ([#1922](https://github.com/PicPay/picpay-ios/issues/1922))

### Changed
- **Engajamento:** Alteração de pastas da feature Favoritos e rename das classes do Onboarding do Favoritos ([#1936](https://github.com/PicPay/picpay-ios/issues/1936))
- **Engajamento:** Atualiza exibição de mensagem de erro na saque 24 horas ([#1910](https://github.com/PicPay/picpay-ios/issues/1910))
- **blackops:** dark mode e ajustes para iOS13 com Xcode 11 ([#1527](https://github.com/PicPay/picpay-ios/issues/1527))
- **plataforma:** [PLAIOS-32] adicionar o los do firebase para todos os eventos existentes hoje ([#1908](https://github.com/PicPay/picpay-ios/issues/1908))
- **plataforma:** [PLAIOS-18] Suporte a Pods nos testes dos modulos gerados  ([#1909](https://github.com/PicPay/picpay-ios/issues/1909))

### Fixed
- **blackops:** [BOIO-266] Corrige de forma paliativa o crash no load de imagem em células que utilizam a extension UIImageView+Extension ([#1929](https://github.com/PicPay/picpay-ios/issues/1929))
- **blackops:** [BOIO-260] Corrige testes de validação de identidade falhando por causa dos dispatches ([#1920](https://github.com/PicPay/picpay-ios/issues/1920))


<a name="v10.17.5"></a>
## [v10.17.5] - 2019-12-20
### Added
- **3DS:** [PAY-304] Fazer tracking de eventos adicionar dinheiro por débito ([#1885](https://github.com/PicPay/picpay-ios/issues/1885))
- **3DS:** [PAY-353] Mostrar um aviso no cadastro de cartão de débito, quando o cartão for de crédito ([#1893](https://github.com/PicPay/picpay-ios/issues/1893))
- **ativação:** [PPAT-322] Adicionar evento para logar a exibição do feed com ou sem as células de oportunidades ([#1911](https://github.com/PicPay/picpay-ios/issues/1911))
- **ativação:** [PPAT-300] Informar o telefone que o SMS foi disparado. ([#1898](https://github.com/PicPay/picpay-ios/issues/1898))
- **engajamento:** [PPEN-56] Lista de Favoritos ([#1849](https://github.com/PicPay/picpay-ios/issues/1849))
- **plataforma:** [PLAIOS-18] Módulos com Xcodegen ([#1888](https://github.com/PicPay/picpay-ios/issues/1888))
- **universitario:** [UNI-86] Diferenciar itens com benefício para Conta Uni na Store ([#1905](https://github.com/PicPay/picpay-ios/issues/1905))
- **user-security:** [US-93] Sim/Swap causando fraude em contas utilizando o TFA por SMS ([#1899](https://github.com/PicPay/picpay-ios/issues/1899))

### Changed
- Atualiza modelo de pr para português ([#1904](https://github.com/PicPay/picpay-ios/issues/1904))
- **3DS:** [K3DS-24] Pequenos ajustes 3DS  ([#1790](https://github.com/PicPay/picpay-ios/issues/1790))
- **Engajamento:** [PPEN-95] Alteração na abertura de links externos ([#1890](https://github.com/PicPay/picpay-ios/issues/1890))
- **Engajamento:** Atualiza transição entre estados das telas (loading e error) ([#1894](https://github.com/PicPay/picpay-ios/issues/1894))
- **blackops:** [BOIO-106] Reescrever na nova arquitetura o fluxo de Desativar Conta ([#1863](https://github.com/PicPay/picpay-ios/issues/1863))
- **card:** Exibir fluxos relacionados ao card em fullscreen ([#1901](https://github.com/PicPay/picpay-ios/issues/1901))
- **card:** [CCBO-1982] Ajusta darkmode no fluxo de bloquear cartão ([#1896](https://github.com/PicPay/picpay-ios/issues/1896))

### Fixed
- **ativação:** [PPAT-308] Corrige crash ao definir remotamente um json vazio para as células de oportunidade do feed ([#1880](https://github.com/PicPay/picpay-ios/issues/1880))
- **plataforma:** [PLAIOS-18] Imports implicitos do modulo UI ([#1882](https://github.com/PicPay/picpay-ios/issues/1882))


<a name="v10.17.4"></a>
## [v10.17.4] - 2019-12-11
### Added
- **3DS:** [PAY-310] Alterar célula de recarga por débito, quando não tiver cartão de debito. ([#1871](https://github.com/PicPay/picpay-ios/issues/1871))
- **3DS:** [PAY-300] Guardar informações de Adquirente e Bandeira no cartão ([#1866](https://github.com/PicPay/picpay-ios/issues/1866))

### Fixed
- **ativacao:** [PPAT-278] Bug não permite fechar o scanner da Cielo aberto ao tocar em loja, na lista da tela de Locais ([#1868](https://github.com/PicPay/picpay-ios/issues/1868))
- **plataforma:** Alteração no regex de changelog para suportar caracteres especiais ([#1878](https://github.com/PicPay/picpay-ios/issues/1878))


<a name="v10.17.3"></a>
## [v10.17.3] - 2019-12-06
### Added
- **3DS:** [PAY-317] Tela de Sucesso e Erro, fluxo adicionar dinheiro Debito ([#1856](https://github.com/PicPay/picpay-ios/issues/1856))
- **Card:** [CCBO-1921] Adicionar Onboarding para quem já tem limite aprovado ([#1870](https://github.com/PicPay/picpay-ios/issues/1870))
- **card:** [CCBO-1743] Tela de Onboarding do Card ([#1855](https://github.com/PicPay/picpay-ios/issues/1855))

### Changed
- **card:** [CCBO-1833] Atualiza tela de Resumo ([#1869](https://github.com/PicPay/picpay-ios/issues/1869))
- **card:** [CCBO-1831] Altera layout e textos da tela de Data de Vencimento ([#1867](https://github.com/PicPay/picpay-ios/issues/1867))
- **card:** [CCBO-1823] Atualiza textos na tela de analise do crédito ([#1857](https://github.com/PicPay/picpay-ios/issues/1857))
- **card:** [CCBO-1911] Altera regra para exibir as ofertas do PicPay Card na carteira ([#1854](https://github.com/PicPay/picpay-ios/issues/1854))
- **engajamento:** [PPEN-49][PPEN-60] Tracking detalhado Aba Pagar e busca ([#1766](https://github.com/PicPay/picpay-ios/issues/1766))
- **engajamento:** [PPEN-61] Tracking Detalhado da Home (Sugestões e Feed) ([#1752](https://github.com/PicPay/picpay-ios/issues/1752))

### Fixed
- Correção do bug de logout ao iniciar o app sem internet ([#1865](https://github.com/PicPay/picpay-ios/issues/1865))
- **blackops:** Atualizando Readme link Changelog ([#1853](https://github.com/PicPay/picpay-ios/issues/1853))


<a name="v10.17.2"></a>
## [v10.17.2] - 2019-12-04
### Added
- **ativação:** [PPAT-168] Células de oportunidades no feed (banner e carrossel) ([#1858](https://github.com/PicPay/picpay-ios/issues/1858))
- **blackops:** [BOIO-254] Adiciona suporte ao Wiremock ([#1831](https://github.com/PicPay/picpay-ios/issues/1831))
- **blackops:** Adicionado arquivo de configuração do Code Cov ([#1834](https://github.com/PicPay/picpay-ios/issues/1834))
- **card:** Fluxo de ativação da função de débito do cartão ([#1840](https://github.com/PicPay/picpay-ios/issues/1840))
- **card:** fluxo de esqueci minha senha ([#1813](https://github.com/PicPay/picpay-ios/issues/1813))
- **plataforma:** Adiciona checagem do titulo dos PRs no Danger ([#1846](https://github.com/PicPay/picpay-ios/issues/1846))
- **universitario:** [UNI-8] Nova tela de conta universitária ativada ao ter cadastro aprovado ([#1782](https://github.com/PicPay/picpay-ios/issues/1782))

### Changed
- **ativação:** [PPAT-159] Mostra as lojas que usam as máquinas de pagamento da Cielo no mapa ([#1827](https://github.com/PicPay/picpay-ios/issues/1827))
- **blackops:** Atualização do README  ([#1825](https://github.com/PicPay/picpay-ios/issues/1825))
- **blackops:** [BOIO-186] Refatora feature de validar identidade para o novo padrão de arquitetura [#1821](https://github.com/PicPay/picpay-ios/issues/1821)
- **blackops:** Atualizações no script de setup do projeto
- **card:** [CCBO-1912] Corrigi regra para exibir menu de configurar cartão na home do PicPay Card ([#1852](https://github.com/PicPay/picpay-ios/issues/1852))
- **card:** [CCBO-1908] Altera regra para exibir menus referente ao PicPay Card nos settings ([#1850](https://github.com/PicPay/picpay-ios/issues/1850))
- **card:** [CCBO-1843] Atualiza as imagens do card de branco para chumbo ([#1837](https://github.com/PicPay/picpay-ios/issues/1837))

### Fixed
- **plataforma:** Conserta o script setup.sh  ([#1842](https://github.com/PicPay/picpay-ios/issues/1842))


<a name="v10.17.1"></a>
## [v10.17.1] - 2019-11-29

<a name="v10.17.0"></a>
## [v10.17.0] - 2019-11-27
### Added
- **blackops:** Implementa git-chglog para gerar changelogs ([#1819](https://github.com/PicPay/picpay-ios/issues/1819))
- **engajamento:** [PPEN-68] Abrir os links da web view externamente ([#1795](https://github.com/PicPay/picpay-ios/issues/1795))

### Fixed
- **blackops:** Conserta a lane beta_dev do Fastlane e o histórico do changelog ([#1832](https://github.com/PicPay/picpay-ios/issues/1832))
- **blackops:** [BOIO-248] Conserta o acesso ao group keychain na RecentsExtension ([#1824](https://github.com/PicPay/picpay-ios/issues/1824))

### PCI
- PCIDEV-69 Criar rota de Ecommerce para o AWS API Gateway
- PCIDEV-71 Criar rota de Digital Goods(Bluezone Transaction) para o AWS API Gateway
- PCIDEV-72 Criar rota de Digital Goods(Parking) para o AWS API Gateway
- PCIDEV-247 Criar rota de PhoneRecharges para o AWS API Gateway
- PCIDEV-36 Criar rota de Subscribe para o AWS API Gateway
- PCIDEV-67 Criar rota de Subscription Payment para o AWS API Gateway
- PCIDEV-246 Criar rota de Digitalcodes para o AWS API Gateway
- PCIDEV-248 Criar rota de Transportpass para o AWS API Gateway
- PCIDEV-249 Migra o `PicPayError` para o módulo Core como `RequestError`

### Added
- PPAT-144 Adiciona status do boleto no card do feed
- BOIO-121 Adicionando som da moeda na tela de sucesso
- BOIO-242 Ajusta eventos de analytics do AppsFlyer para funcionarem no Firebase
- PPAT-144 Adiciona status do boleto no card do feed
- PPAT-110 Tela com detalhes sobre o saldo disponível para saque por meio de transferência bancária
- PPEN-68 Externalizando abertura de links da webView.
- PicPay Card
  - CCBO-1695 Cria tela de motivo de bloqueio do cartão
  - CCBO-1697 Alterar serviço de bloqueio do cartão, passando o motivo como parâmetro
  - CCBO-1751 Atualiza layout e textos da tela de Bloquear Cartão
  - CCBO-1726 Tela Endereço e confirmação - Alterar texto
  - CCBO-1728 Tela Endereço e confirmação - Alterar imagem
  - CCBO-1730 Tela Endereço e confirmação - Incluir link de ajuda
  - CCBO-1736 Tela Card solicitado - Alterar textos e remover endereço
  - CCBO-1757 Tela Relembrando/Contrato - Alterar texto do link
  - CCBO-1842 Tela Relembrando/Contrato - Ajuste de cores no darkmode
  - CCBO-1736 Tela Card solicitado - Alterar textos e remover endereço
  - CCBO-1840 Corrige gap entre navigation e view na CreditLimitController
  - CCBO-1788 Remove erro na tela e exibe erros somente em Popup
  - Fluxo esqueci minha senha
    - CCBO-1671 Esqueci minha senha, validação do cartão
    - CCBO-1633 Inclui link "esqueci minha senha" na tela de alterar a senha
    - CCBO-1672 Cria tela de "Nova Senha" no fluxo de esqueci minha senha
    - CCBO-1673 Cria tela de "Senha alterada" no fluxo de esqueci minha senha
    - CCBO-1882 Bug ao exibir loading, esconder botões
- PPAT-110 Tela com detalhes sobre o saldo disponível para saque por meio de transferência bancária
- PPEN-68 Externalizando abertura de links da webView.

### Changed
- Mudar a ViewController para não aceitar Coordinator
- BOIO-122 Refatoração da ProfilePromoCodeViewController
- Dark Mode
    - BOIO-226 Alterando tela de notificação interna para dark mode
    - BOIO-255 Tela de Pagamento e-commerce adaptada ao dark mode
    - Alterado cores da tabbar e também do header da home
    - BOIO-207 Ajustes na tela de pagamento que estava branca quando vinha do scanner de qrcode
    - BOIO-78 ajusta tela de pagamentos P2P para darkmode
    - BOIO-94 Ajusta as tabs Pagar para o dark mode
    - BOIO-98 Ajusta as tela de adicionar cartão ao dark mode
    - BOIO-94 Ajusta as tabs Pagar para o dark mode
    - BOIO-134 Ajusta as telas de configuração de privacidade e notificações para usar as cores da Palette
    - BOIO-132 Adjusta a tela de trocar senha para o dark mode
    - BOIO-137 Tela de Desativar minha conta com Dark Mode
    - BOIO-174 Dark Mode tela de popUp de permissão da camera
    - BOIO-163 Ajustes da tela de Recibo com Dark Mode
    - BOIO-164 Ajustes do fluxo de verificação de cartão de crédito com dark mode
    - BOIO-138 Ajusta a tela de perguntas da validação de identidade ao dark mode
    - BOIO-91 Telas referêntes a assinaturas
    - BOIO-193 Card de Upgrade de conta com DarkMode
    - BOIO-192 Dark mode de recibo de assinatura
    - BOIO-179 Cor do header em carteira > crédito
    - BOIO-202 cores para o opção de saque e saque 24 horas
    - BOIO-93 - Ajusta fluxo de registro para Dark Mode
    - BOIO-204 Ajusta tela de outro valor do digital goods para o dark mode
    - BOIO-205 Ajustes para dark mode na tela de contas vinculadas em ajustes e na tela de detalhes da conta vinculada
    - BOIO-181 Corrige a tela de detalhes do feed que exibia uma faixa preta no bottom
    - BOIO-235 Ajusta tela de detalhamento de estabelecimento no feed para Dark Mode
    - BOIO-223 Ajustes para dark mode no fluxo de autorização de novo device (TFA)

### Fixed
- BOIO-229 - Corrige o tracking de transação para boletos
- S24H-212 Corrigir os fluxos antigos para a nova arquitetura do App.
- BOIO-229 - Corrige o tracking de transação para boletos
- CCBO-1754 - Adiciona SwiftLint ao módulo UI e refatora código para tirar novos warnings
- S24H-212 Corrigir os fluxos antigos para a nova arquitetura do App.
- BOIO-215 - Removendo o popup de cashback popup ao inicializar o app
- BOIO-244 - Corrige o campo de valor do pagamento de bolete 
- BOIO-210 Ajusta tela de seguir contatos após cadastro para o dark mode
- CCBO-1754 - Adiciona SwiftLint ao módulo UI e refatora código para tirar novos warning
- BOIO-244 - Corrige o campo de valor do pagamento de bolete
- BOIO-231 Remove arquivos legados referentes ao PhoneVerificationViewController e SMSCodeVerificationViewController.
- BOIO-249 Ajusta o topo da tela de detalhamento de feed.

### Added
- UNI-8 Nova tela de Conta Uni ativada ao ter o cadastro aprovado
    - UNI-49 Exibe popup de conta universitária ativada e ao abrir o app depois de ser aprovado (apenas uma vez)
    - UNI-51 Tracking de eventos do popup de conta ativada
    - UNI-58 Webview de tela de beneficios e regras de conta universitária com URL no Remote Config

## [v10.16.5] - 2019-11-1

### Fixed
 - BOIO-203 Corrige envio de parâmetro incorreto na tela de settings


## [v10.16.4] - 2019-10-23
### Added

- PicPay Card
  - Fluxo de solicitar cartão
  - CCBO-1450 Banner para solicitar cartão físico
  - CCBO-1452 Tela de confirmação de endereço de entrega
  - CCBO-1454 Tela de cartão solicitado com sucesso
  - CCBO-1565 Acrescentar olho mágico para visualizar senha e alterar texto na tela de Criar Senha
  - CCBO-1563 Acrescentar olho mágico para visualizar senha e alterar texto na tela de Alterar Senha
- PPAT-118 Criar telas do fluxo de cobrar usuário
- PPAT-119 Implementa parse do item qua lança o fluxo de cobrar e configura sua ativação por remote config
- PPEN-36 Adiciona tracking na Aba Pagar
- PPEN-43 Adiciona tracking na Aba Inicio
- PPEN-24 Adiciona Popup de recorrencia para bilhete unica e recarga de celular
- S24H-213 Ajustar a marca do banco 24 horas que aparece no Popup
- BOIO-172 XcodeGen
- PPEN-37 Adiciona tracking na Aba Notificações
- PPEN-44 Adiciona tracking na Aba Carteira

### Fixed
- BOIO-203 Corrige envio de parâmetro incorreto na tela de settings
- BOIO-184 Corrige layout dos botões na tela de aniversariante do dia
- BOIO-196 Corrige falta de envio de parâmetro ao final de qualquer transação
- BOIO-209 Corrige crash ao cancelar etapa de seguir contatos.
- BOIO-121 Refactor BusinessTypeViewController
- PicPay Card
  - CCBO-1678 - Bug ao navegar rapidamente dentro da home do antigo pós
  - CCBO-1682 - Inconsistência no feed do card
  - CCBO-1526 - Tela Relembrando - Alterar botão de aceite
  - CCBO-1552 - Tela Relembrando - Alterar Link do contrato PDF
  - CCBO-1552 - Feed Card - Bug de load infinito
- Corrige posição e crash de constraint no popup com imagem
- BOIO-215 - Removendo o popup de cashback popup ao inicializar o app

### Changed
- PPAT-121 Relayout do scanner principal e da tela de meu código

## [v10.16.4] - 2019-10-23
### Fixed
- BOIO-195 Ajuste texto de erro de validação de cartão de crédito

### Added
- S24H - Saque no banco24horas
    - S24H-146 Saque Banco 24 horas
    - S24H-145 Tela de opcoes de saque
    - S24H-147 Tela de valores para saque
    - S24H-148 Tela de scanner banco 24horas
    - S24H-168 Tela de scanner banco 24horas parte2 - bottom sheet
    - S24H-149 Tela de confirmacao do saque
    - S24H-170 Tela de outros valores de saque
    - S24H-171 Falta de limite devido ao limite de saque diário
    - S24H-172 Tela de Erro de saldo insuficiente
    - S24H-169 Tela de informação sobre limites de saque 24 horas
    - S24H-186 Popup de oferta
    - S24H-203 Ajustar link do helpcenter na tela de limite de saque no banco24horas
    - S24H-204 Ajustar nome da marca Banco24Horas na tela de limite de saque no Banco24Horas
    - S24H-208 Ajustes e melhorias no código e correção do parâmetros de requisição.
    - S24H-209 Correção de fluxo do original quando existir saque em andamento.

- PCI
    - PCIDEV-69 Criar rota de Ecommerce para o AWS API Gateway
    - PCIDEV-71 Criar rota de Digital Goods(Bluezone Transaction) para o AWS API Gateway
    - PCIDEV-72 Criar rota de Digital Goods(Parking) para o AWS API Gateway
    - PCIDEV-247 Criar rota de PhoneRecharges para o AWS API Gateway
    - PCIDEV-36 Criar rota de Subscribe para o AWS API Gateway
    - PCIDEV-67 Criar rota de Subscription Payment para o AWS API Gateway
    - PCIDEV-246 Criar rota de Digitalcodes para o AWS API Gateway
    - PCIDEV-248 Criar rota de Transportpass para o AWS API Gateway
    - PCIDEV-249 Migra o `PicPayError` para o módulo Core como `RequestError`
    - PCIDEV-65 Criar rota de P2P para o AWS API Gateway
    - PCIDEV-66 Criar rota de PAV para o AWS API Gateway
    - PCIDEV-70 Criar rota de P2M para o AWS API Gateway
    - PCIDEV-285 Criar tela de inserção de CVV para o caso de não ter o CVV na keychain
    - PCIDEV-310 Unificar Payload Digital Goods

- PCI
    - PCIDEV-69 Criar rota de Ecommerce para o AWS API Gateway
    - PCIDEV-71 Criar rota de Digital Goods(Bluezone Transaction) para o AWS API Gateway
    - PCIDEV-72 Criar rota de Digital Goods(Parking) para o AWS API Gateway
    - PCIDEV-247 Criar rota de PhoneRecharges para o AWS API Gateway
    - PCIDEV-36 Criar rota de Subscribe para o AWS API Gateway
    - PCIDEV-67 Criar rota de Subscription Payment para o AWS API Gateway
    - PCIDEV-246 Criar rota de Digitalcodes para o AWS API Gateway
    - PCIDEV-248 Criar rota de Transportpass para o AWS API Gateway
    - PCIDEV-249 Migra o `PicPayError` para o módulo Core como `RequestError`
    - PCIDEV-65 Criar rota de P2P para o AWS API Gateway
    - PCIDEV-66 Criar rota de PAV para o AWS API Gateway
    - PCIDEV-70 Criar rota de P2M para o AWS API Gateway
    - PCIDEV-285 Criar tela de inserção de CVV para o caso de não ter o CVV na keychain
    - PCIDEV-310 Unificar Payload Digital Goods

### Fixed
- BOIO-203 Corrige envio de parâmetro incorreto na tela de settings
- BOIO-184 Corrige layout dos botões na tela de aniversariante do dia
- BOIO-196 Corrige falta de envio de parâmetro ao final de qualquer transação
- BOIO-209 Corrige crash ao cancelar etapa de seguir contatos.
- BOIO-121 Refactor BusinessTypeViewController

- PicPay Card
  - CCBO-1678 - Bug ao navegar rapidamente dentro da home do antigo pós
  - CCBO-1682 - Inconsistência no feed do card
  - CCBO-1571 - Criar tela de Onboarding antes da criação da senha
  - CCBO-1526 - Tela Relembrando - Alterar botão de aceite
  - CCBO-1552 - Tela Relembrando - Alterar Link do contrato PDF
  - CCBO-1552 - Feed Card - Bug de load infinito
  - CCBO-1703 - Exibir menu de configurar cartão mesmo quando o cartão estiver bloqueado
- Corrige posição e crash de constraint no popup com imagem

### Changed
- PPAT-121 Relayout do scanner principal e da tela de meu código

## [v10.16.4] - 2019-10-23
### Fixed
- BOIO-195 Ajuste texto de erro de validação de cartão de crédito
- BOIO-203 Corrige envio de parâmetro incorreto na tela de settings
- BOIO-184 Corrige layout dos botões na tela de aniversariante do dia
- BOIO-196 Corrige falta de envio de parâmetro ao final de qualquer transação
- BOIO-209 Corrige crash ao cancelar etapa de seguir contatos.
- BOIO-121 Refactor BusinessTypeViewController
- PicPay Card
  - CCBO-1678 Bug ao navegar rapidamente dentro da home do antigo pós
  - CCBO-1682 Inconsistência no feed do card
  - CCBO-1571 Criar tela de Onboarding antes da criação da senha
  - CCBO-1526 Tela Relembrando - Alterar botão de aceite
  - CCBO-1552 Tela Relembrando - Alterar Link do contrato PDF
  - CCBO-1552 Feed Card - Bug de load infinito
  - CCBO-1522 Ajustar tela de configuração de cartão físico
- Corrige posição e crash de constraint no popup com imagem

### Changed
- Altera o valor do deeplink para ficar sincronizado com o Android
- CCBO-1314 - Renomear Picpay pos para Picpay card
- Dark Mode
    - BOIO-78 ajusta tela de pagamentos P2P para darkmode
    - BOIO-94 Ajusta as tabs Pagar para o dark mode
    - BOIO-98 Ajusta as tela de adicionar cartão ao dark mode
    - BOIO-94 Ajusta as tabs Pagar para o dark mode
    - BOIO-134 Ajusta as telas de configuração de privacidade e notificações para usar as cores da Palette
- PPAT-121 Relayout do scanner principal e da tela de meu código
    - BOIO-137 Tela de Desativar minha conta com Dark Mode
    - BOIO-174 Dark Mode tela de popUp de permissão da camera
    - BOIO-163 Ajustes da tela de Recibo com Dark Mode
    - BOIO-187 Ajustes cores do rendimento em carteira para dark mode
    - BOIO-164 Ajustes do fluxo de verificação de cartão de crédito com dark mode
    - BOIO-138 Ajusta a tela de perguntas da validação de identidade ao dark mode
    - BOIO-91 Telas referêntes a assinaturas
    - BOIO-193 Card de Upgrade de conta com DarkMode
    - BOIO-192 Dark mode de recibo de assinatura
    - BOIO-179 Cor do header em carteira > crédito
    - BOIO-178 Ajusta o fluxo de cadastro do PicPay Pós para dark mode
    - BOIO-211 Ajusta célula de adicionar cartão de crédito para dark mode
- PicPay Card
  - CCBO-1314 Renomear Picpay pos para Picpay card
  - CCBO-1550 Ativar cartão - Alterar botão de ajuda
  - CCBO-1557 Criar senha - Alterar botão de ajuda
  - CCBO-1612 Nova senha - alterar botão de ajuda
  - CCBO-1561 Criar senha - TouchID - Alterar texto do alert
- BOIO-175 Bug ao tirar outra foto na tela de Validar Identidade
- BOIO-189 UIButtonBizType rewrite - Corrige crash no PRO
  - CCBO-1569 - Criar senha - Erro de input
- Corrige posição e crash de constraint no popup com imagem

### Changed
- PPAT-121 Relayout do scanner principal e da tela de meu código
- PicPay Card
  - CCBO-1314 - Renomear Picpay pos para Picpay card
  - CCBO-1550 - Ativar cartão - Alterar botão de ajuda
  - CCBO-1557 - Criar senha - Alterar botão de ajuda
  - CCBO-1612 - Nova senha - alterar botão de ajuda
  - CCBO-1561 - Criar senha - TouchID - Alterar texto do alert
- BOIO-175 - Bug ao tirar outra foto na tela de Validar Identidade
- BOIO-189 - UIButtonBizType rewrite - Corrige crash no PRO
- PPAT-115 Muda a apresentação da mensagem ao copiar código MGM
- PPAT-116 Melhora a ação de compartilhar código MGM por email
- PPAT 177 Ajuste na exibição câmera e nas cores do scanner principal para o dark mode
- BOIO-165 - Deeplink do HelpCenter com usuário deslogado
- BOIO-175 - Bug ao tirar outra foto na tela de Validar Identidade

## [v10.16.4] - 2019-10-17
### Added
- PPAT-53 Adiciona deeplink de cadastrar cartão e de qrcode do usuário
- PPAT-48 e 50 Implementar tela de onboarding de cartão
- PPAT-47 e 49 Integrar telas ao fluxo de onboarding e adicionar controle por feature flags
- PPAT-44 e PPAT-46 Implementar as novas telas de onboarding de contatos
- PPAT-51 Implementar tela de primeira ação no novo onboarding
- PPEN-13 Criação de visualização de eventos no App de Dev
- PPEN-24 Criação de lembrete de recorrencia para bilhete unico
- PPEN-27 Criação de lembrete de recorrencia para recarga de celular
- BOIO-110 Extensão da UIImageView para download e cache de imagens
- BOIO-157 Swiftlint ordem do conteúdo das classes do projeto
- BOIO-109 Remove UIWebView da WebViewPopUpViewController e renomeia para HTMLPopUpViewController
- BOIO-109 Refatoração da CreditAboutViewController para a MVVM-C
- PR-XXX Componente CurrencyField
- PR-XXX Transforma o enum PicPayResult em um typealias do Result nativo do Swift 5
- BOIO-153 Arquivos .xcconfig para configurações de build
- PR-3DS Ajustar Warnings do Projeto

### Fixed
- BOIO-69 Ajuste ao copiar código e mensagem de compartilhamento no MGM
- BOIO-97 UniqueId adicionado nos requests no Keychain
- BOIO-154 Corrige Device ID
- BOIO-167 Bug no fluxo de verificação de documento no envio de comprovante de residência (impossibilitando o envio da foto após sua captura)
- BOIO-152 Corrigido crash ao acessar o app pelo quick acess da Home sem estar loggado
- PPAT-95 Link quebrado no compartilhamento de recibo
- BOIO-168 Bug na tela de nova transação quando abre a primeira vez o aplicativo.

## [v10.16.3] - 2019-10-06
- Hotfix bug de popup de cashback estar ficando em cima da tabbar, fazendo com que consiga trocar de tabs, e bugando quando volta para a home

## [v10.16.2] - 2019-10-05
### Fixed
- Hotfix bug contrato 3DS
- Hotfix problema ao logar no aplicativo, estava ficando com a tela travada

## [v10.16.1] - 2019-10-03
## 3DS
- PAY-XX1 Refatorar RechargeViewController
- PAY-XX2 Fazer tela troca de cartões
- PAY-XX3 Correção Bug SDK
- PAY-167 Adicionar função de debito na tela de recarga.
- PAY-187 Refatorar PPRechargeMethod
- PAY-165 Implementar fluxo de depósito com débito
- PAY-139 Ajustar fluxo de adicionar cartão para o débito.
- PAY-201 Refatorar RechargeBoletoViewController
- PAY-198 Refatorar PPRecharge
- PAY-202 Refatorar RechargeOriginalCTAViewController
- PAY-220 Refatorar RechargeBankTransferViewController
- PAY-227 Melhorar CardRechargeViewModel e CardViewModel
- PAY-231 Ajustar a mensagem de erro que é exibida com o desafio é cancelado.
- PAY-180 Integração com serviços do deposit e backend no cash in de débito
- PAY-217 Eventos analytics
- PAY-283 Corrigir textos e Adicionar Helpcenter no deposito
- PAY-285 Adicionar 3DS Birthday
- PAY-XXX4 Rollback Para Payload Antigo de Adicionar Cartão.

### Changed
- BOIO 123 - Troca de alerta nativo para customizado
- BOIO-97 UniqueId adicionado nos requests no Keychain
- BOIO-97 UniqueId adicionado nos requests no Keychain
- BOIO-15 Reescrever classe PersonalData usando novo modelo de arquitetura
- BOIO-141 Remove Launch images e cria Launch Screen storyboard
- BOIO-140 Remover SwiftLint do Pod e colocar no Gem

### Fixed
- BOIO-144 ajustado popup de cashback que estava aparecendo antes do recibo e fazendo com que o recibo não aparecesse

## [v10.16.0] - 2019-09-25
### Added
- PicPay Card
  - CCBO-1248 Card Banner de ativação cartão físico
  - CCBO-1252 Tela de dicas de segurança
  - CCBO-1251 Tela de ajuda na ativação do cartão fisíco
  - CCBO-1254 Tela de sucesso de ativação
  - CCBO-1250 Tela de ativar cartão físico
  - CCBO-1253 Cria tela de criar senha do cartão
  - CCBO-1256 Tela de configurar cartão fisico
  - CCBO-1334 Fluxo de ativar cartão
  - CCBO-1264 Tela bloquear cartão
  - CCBO-1375 Validar senha do cartão
  - CCBO-1257 Tela de alterar senha do cartão
  - CCBO-1376 Cria fluxo de trocar a senha do cartão
  - CCBO-1380 Adiciona Fluxo de Settings do Cartão dentro do Settings do APP e do Meu Picpay Pós
  - CCBO-1470 Melhoria no retorno de erro na API
  - CCBO-1459 Tela de confirmação de dados do contrato de crédito
  - CCBO-1450 Banner para solicitar cartão físico
  - CCBO-1452 Tela de confirmar endereço de entrega
  - CCBO-1454 Tela de cartão solicitado com sucesso
- PM-1290 adiciona novos eventos no fluxo de cadastro e pagamento;
- PM-1290 cria nova camada de Analytics
- Volta com o Firebase para os eventos na nova classe de Analytics


### Changed
- Adiciona ViewController Generic dentro o Core
- Adiciona Wrapper para SafeArea
- Atualiza controle de acesso dos metodos e variaveis da ViewController
- XXX- Adicionar Adapter
- BOIO-7 Reescrever classe MyNumberViewController usando novo modelo de arquitetura
- CCBO-1332 Move LoadingView para UI
- BOIO-72 ajusta parse do Token para notificações
- Adiciona Coordinating Protocol para ajudar na navegação entre os módulos
- Adiciona extension para Currency(moeda) no Core
- BOIO-78 ajusta tela de pagamentos P2P para darkmode
- PCIDEV - 185 Parte 1: Refatorar BirthdayGiftPaymentViewController.
- BOIO-16 Refactor da tela Mail View Controller para nova arquitetura
- Atualiza configuração do Credit para Homolog
- Atualiza versão do App para 10.16.0
- PCIDEV-186 Adptar Birthday PCI
- Correção visual na tela de sucesso ao solicitar cartão
- Altera o valor do deeplink para ficar sincronizado com o Android

### Fixed
- PM-1324 Texto escrito errado na tela de cadastro em análise do PicPay Pós
- PM-1308 Bug de cores no Bacen e tela de Ajustes
- PM-1294 Corrige e refatora a popup de avaliação do aplicativo
- BOIO-69 Ajuste ao copiar código e mensagem de compartilhamento no MGM
- BOIO-71 ajustado os textos do feed para o iOS 13
- BOIO-73 Ajusta erros visuais na tela de buscas recentes, nova transação P2P e recibo
- BOIO-69 Ajuste ao copiar código e mensagem de compartilhamento no MGM
- BOIO-69 Ajuste ao copiar código e mensagem de compartilhamento no MGM
- BOIO-2 Permite somente numeros nos campos de DDD e Telefone no cadastro
- BOIO-72 "Corrige" crash na tela Pagar no iOS 13
- BOIO-81 Corrige problema ao voltar da tela de adicionar cartão de crédito

## [v10.15.3] - 2019-09-17
### Fixed
- ajustado o envio de documentos que estava sem o popup para envio

## [v10.15.2] - 2019-08-28
### Added
- SA-90 Cria teste unitários para conta universitária
- PM-1276 adicionado teste unitários na classe de seleção de limite do PicPay Pós
- SA-135 Exibe cupom utilizado de conta universitária no cadastro
- PM-1314 Adicionado domínios como exceção para permitir http
- Separa o tombamento do CVV pra um módulo a parte
- PM-1307 Integrar projeto com o Danger

### Changed
- PM-1059 Refatorar as ações customizadas de Alert (Button.Action.custom usa string para definir fluxo)
- PM-609 Remover dependência KLCPopup e passar a utilizar outra ou fazer nativamente
- PM-647 Remover arquivos que não estão no gerenciador de dependências
- PM-1031 Remover o padrão singleton de SearchRecentsManager
- PM-1331 Mover paleta de cores para o módulo UI
- PM-1332 Adiciona outros cases na API e um decode vazio para os casos de NoContent
- PM-1341 Corrigir Api quando fizer um request sem body de retorno (noContent)

### Fixed
- PM-1293 Loading eterno ao iniciar validação de identidade
- Projeto não estava fazendo deploy no target de homolog
- PM-1311 Configuração de cores do Helpshift tornando textos e ícones ilegíveis
- PM-1315 Bug nas ações da popup de realizar assinatura
- PM-1318 ajustado tela de seleção de limite do PicPay Pós que não estava fechando

## [v10.15.1] - 2019-08-08
### Added
- PM-1122 Deeplink ao logar deve ser executado o deep link
- PM-1061 Cria deeplink para webview
- SA-90 Cria teste unitários para conta universitária
- PM-1200 Adiciona alert de imagem na area de transferencia na tela de anexo comprovante envio de dinheiro
- PCIDEV-41 Tombamento do CVV para a migração dos cartões para o escopo PCI

### Changed
- Remove o target PicPayUITest
- Adiciona o KIF para realizar testes funcionais
- PM-1270 Color Palette
- PM-1282 Remove SDK Facebook e Google Analytics
- PM-1288 Textos trocados e ausência de efeitos de blur em botões de envio de foto de documento do PicPay Pós
- PM-1270 Cores, refatoração 2

### Fixed
- PM-1269 Corrige Acessibilidade do Fluxo de Registro de Usuário e Login
- SA-128 Corrige título do botão na tela de Status
- PM-1263 Corrige o bug na altura da scroll view do recibo na reapresentação dessa tela
- PM-1280 Esconde tab bar ao apresentar a tela de indicação
- PM-1275 Bug de quando abre um deeplink para a home do PicPay Pós
- PM-1274 Corrige bug scroll timeline PicPay Pós
- PM-1286 Bug no cadastro do PicPay Pós: a lista de opções de órgão emissor do documento pode aparecer vazia
- PM-1277 Remover Framework Adyen do projeto
- PM-1287 Bug no cadastro do PicPay Pós: o campo número do endereço pode ser editado mesmo que a opção "sem número" esteja marcada
- SA-144 Corrige evento analytics
- PM-1298 Adiciona máscara na data do documento, no cadastro do PicPay Pós
- PM-1279 Corrige texto na tela de privacidade

## [v10.15.0] - 2019-07-26
### Changed
- PM-1244 Migra as dependências do carthage para o Cocoapods, além de adicionar a compatibilidade com o Xcode 11 ;)
- PM-1075 Criar um LoginViewModel de verdade
- Arruma o provision profile do scheme de Homologação
- Refatora um pedaço da classe `AppManager.swift` para tentar identificar o crash top 1 no crashlytics
- Ajusta a configuração do `Swift Lint` para melhorar o tempo de build
- PM-1261 Refatoração da funcionalidade de validação de identidade

### Fixed
- PM-1260 Alteração de texto no fluxo de troca de email
- PM-1265 Correção do crash no feed após volta das dependências para o CocoaPods
- PM-950 Reestruturar Api de contatos para Swift
- PM-1254 Validação de formulário do PicPay Pós - Parte 1
- PM-1254 Validação de formulário do PicPay Pós - Parte 2
- PM-1253 Não deixar inserir caracteres especiais
- Melhorias de padrão de codigo seguindo as regras do Swift Lint
- Corrige PromoCode para WebView e Referral

### Added
- Cria o `ViewController` de base para a injeção do `ViewModel` e do `Coordinator`
- CCBO-1061 adicionado ajuste de limite no conta universitária
- Conta Universitária
    - SA-4 cria coordinator e statusViewController
    - SA-43 adiciona deeplink a StudentAccount
    - SA-6 cria tela para exibir o status da StudentAccount
    - SA-5 cria tela de onboarding
    - SA-55 criar tela de erro pra StudentAccount
    - SA-7 cria tratamento para o cupom do tipo StudentAccount
    - SA-40 cria tela para envio de documentos
    - SA-64 cria tela de benefícios e regras
    - SA-66 cria tela de conta aprovada
    - SA-42 cria seção de benefícios nos ajustes
    - SA-93 adiciona o cupom utilizado no cadastro
    - SA-92 melhorias de código, linter e organização
    - SA-82 cria eventos pro analytics e feature toggle
    - Adiciona eventos analytics que ficaram pendentes

## [v10.14.0] - 2019-07-09
### Changed
- MEPA1-21 Ajustes visuais no item de análise do recibo
- MEPA1-40 Corrigir valores esperados no json do recibo
- Adicionado o flag para que o fastalne espere o tempo de processamento para versões em homolog (teste)
- PM-1252 Corrigir altura das células do feed após merge da feature/analise-pagamento

### Added
- MEPA1-4 Adicionar PopUp no recibo sempre que uma transação voltar com status em análise
- MEPA1-23 Implementar deeplink de acelerar análise
- MEPA1-2 Adicionar saiba mais e alterar layout do item do feed para transações em análise
- MEPA1-29 Gerar o json p/ os eventos de deeplink
- MEPA1-39 Challenge de validação de identidade no P2P
- MEPA1-41 Challenge de validação de cartão no P2P
- MEPA1-33 Implementar track de eventos nas challenges
- Adicionado grupos de testes em homologação e também produção, além de habilitar a notificação para novos builds
    - Ajustes em cima do groups que faltou o array
- Adicionado SDK para o 3ds para testes de review da Apple

### Fixed
- MEPA1-54 Correções e melhorias no item do feed, widget e popup de recibo para transações em análise
- MEPA1-55 Correções e melhorias nos Challenges de pagamento
- PM-1112 Usar novo endpoint de logout

## [v10.13.7] - 2019-07-04
### Fixed
- Ajustes no objecto de taxas e limites que estava como opcional
- CCBO-960 prevenção de duplo toque no botão de finalizar cadastro do PicPay Pós
- PM-481 Correção de warnings / Primeira etapa
- PM-821 Exibe warnings especificos de self retain quando compilar o scheme PicPay - Dev
- PM-821 Ajusta o Fastlane para automatizar o build de prod e homolog no CI

### Added
- PM-821 Ajusta o Fastlane para automatizar o build de prod e homolog no CI
- PM-1236 Adicionar webview da ouvidoria no menu de ajustes
- BACEN
    - BD-183 Adicionado tela de UpgradeChecklist
    - BD-200 Adicionado tela de IncomeRange
    - BD-205 Adicionado opção de selecionar endereço na UpgradeChecklist
    - BD-208 Adicionado tela de UpgradeStatus
    - BD-195 Adicionado lógica para aparição de card de Upgrade de Conta na tela da carteira
    - BD-187 Novos limites na tela de limites
    - BD-218 Adicionado lógica para montar a tela de UpgradeChecklist
    - BD-211 Adicionando DeepLink pra tela de UpgradeChecklist
    - BD-283 Adicionado lógica para confirmar upgrade de conta
    - BD-231 Tela de limites - Limites de conta
    - BD-215 Links para o Helpcenter
    - BD-216 Adicionar eventos que serão disparados pelo Mobile
    - BD-319 Adaptar Transação p2p para utilizar dialogs _ui
    - BD-317 Implementar tracking de eventos na tela de Taxas e limites
    - BD-318 Adaptar telas de depósito para utilizar dialogs _ui
    - BD-294 Ajuste Large Title e Padrão de Código
    - BD-316 Deeplink track de evento
    - BD-220 Adicionar opção de Validar Identidade na UpgradeChecklist
    - BD-353 Teste funcionalidade básicas Bacen
    - BD-389 Alterar Tela de Renda
    - BD-403 Tratamento do _ui para devolver dinheiro
    - BD-1228 Adicionar regra banner na carteira
    - BD-1243 Alterar contrato botão

### Fixed
- PM-1230 Bug na exibição do botão da tela de atualizar o app
- PM-481 Correção de warnings / Primeira etapa
- CCBO-960 prevenção de duplo toque no botão de finalizar cadastro do PicPay Pós

## [v10.13.6] - 2019-07-01
### Fixed
- PM-1235 ajustes na labels de mostrar se existe acrescimo na parcela ou não
- PM-1230 Bug na exibição do botão da tela de atualizar o app
- CCBO-1133 Correção da data que não estava sendo reconhecido do formato válido, assim alguns usuário não conseguiam avançar no cadastro do PicPay Pós

## [v10.13.5] - 2019-06-24
### Added
- PM-1222 Criar deeplink para abrir tela de boleto
- PM-1221 Criar deeplink para perfil de usuário
- PM-1086 cria coordinator inicial do app (logado, deslogado, precisa de username)
- SA-4 cria coordinator e statusViewController
- SA-43 adiciona deeplink a StudentAccount
- SA-6 cria tela para exibir o status da StudentAccount
- PM-1217 Adiciona telefone da ouvidoria ao Recibo
- CCBO-1069 Track de eventos no fluxo de Pagamento de Fatura

### Changed
- PM-1133 Remover o botão de usar código promocional da tela de formas de pagamento
- CCBO-1120 Mudar forma de quando apresentar oferta

### Fixed
- Corrige typo do método Authenticated
- PM-1187 Bug reenvio da foto de comprovante
- CCBO-1039 removido o alerta de taxas do pagamento de fatura para quando vai se pagar com o saldo apenas

## [v10.13.4] - 2019-06-12
### Changed
- PM-1161 Correção da animação de load ao mudar switch na tela de configurações de notificação
- PM-1154 Usar novo endpoint de configurações de privacidade de conta
- CCBO-1108 Esconder Pagamento mínimo quando o valor for zero

### Added
- PM-1144 Inserir o link de Regulamento na tela “Usar código promocional”
- PM-xxxx Adiciona testes para a função toDictionary da extension de Encodable

### Fixed
- CCBO-1094 ajustes ao entrar na segunda parte do cadastro do crédito. Estava com bug em um loading
- PM-1231 Verificar deeplinks do confluence

## [v10.13.3] - 2019-05-29

### Added
- PM-1137 Função de esconder o saldo da carteira

### Fixed
- PM-1149 Corrigir mensagem de troca de endereço do cartão de crédito
- Removido qualquer referencia do texto de "sem juros" trocando para "sem acréscimo"
- PM-1188 deeplink de notificação
- PM-1175 Animação de carregamento do UIPPButton não exibida
- Corrige teste InvoicePayment, a data limite para pagamento já tinha passado
- CCBO-961  Ajustado a tela de documento negado que não estava funcionando
- PM-1160 Ajustar loading das notificações e carregamentos de webviews

### Changed
- PM-1169 Remove algumas funções não utilizadas
- CCBO-1003 ajustes no fluxo mandar documentos, agora vem de uma api

### Changed
- PM-996 Transformar CardBank e InsertCardBank para funcionar com Codable
    - * Simplificação de lógica do model CardBank e InsertCardBank;
    - * PaymentMethodsViewModel adicionado o controle da lógica de deleção ao ViewModel;
    - * Removido dependência de Obj-c do CardBank;
    - * Atualiza AddNewCreditCardDelegate removendo necessidade de passar o objeto CardBank pra frente;
    - * uso de Stubs nos testes do ViewModel e remoção de classes de Mock
    - * Cria helper no JSONDecoder, decodedNestedData para lidar com respostas que só precisa andar dentro de um nível "data"."

## [v10.13.1] - 2019-05-17

### Added
- CCBO-1009 adiciona OHTTPStubs e cria testes pra CreditPaymentViewModel e Coordinator
- PM-1156 Enviar informações adicionais no cadastro

### Changed
- Swift 5
- CCBO-XXX atualiza parâmetros de pagamento
- CCBO-XXX atualiza data do CreditInvoice
- CCBO-1020 ajustes no enpoint de pagamento

### Fixed
- CCBO-972 ajustes de bugs:
    * quando entra na tela de detalhes e da um erro, o usuário esta permanecendo na atual tela
    * quando não existe transações nem futuras e processadas esta aparecendo um linha em branco
    * quando da algum erro no carregamento da lista de faturas (meses) esta aparecendo um mensagem de erro, porém o loadding continua.
    * Data da lista de transações esta errado, usar a data em fomato de string que vem também na API
- PM-1153 Bug na verificação de identidade - Crash no aplicativo

### Added
- PM-1142 Integrar com AppsFlyer e adicionar novos eventos

## [v10.13.0] - 2019-05-11
### Added
- CCBO-929 Adicionado dropdown para esconder os lançamentos futuros na tela de detalhamento de fatura
- CCBO-930 mudança na tela de selecao tipo de pagamento da fatura
- CCBO-940 tela de seleção de tipo de pagamento da fatura
- CCBO-944 digitar outros valores para pagamento de fatura
- CCBO-940 tela de pagamento
- CCBO-932 pagamento via boleto

### Removed
- remoção dos arquivos do trustkit que estava no projeto

### Changed
- PM-1102 Melhoria no compartilhamento de QR code
- PM-1100 Usar novo endpoint de configurações de notificações
- CCBO-973 nova tela de seleção de método de pagamento

## [v10.12.38] - 2019-04-29
### Changed
- PM-1116 Passa AppDelegate para Swift;
    - Remove a pasta do carthage do projeto;
    - Ajuste nas cores para Swift acessar PicPayStyle ao invés de AppStyle;
    - AppParameter: remove o código de Reachability que não fazia nada.

### Fixed
- PM-1124 Não estava sendo mostrando o codigo de barras no recibo do pagamento de boleto
- PM-1106 Bug - Perdendo a barra de navegação na lista de solicitações de seguidores
- BD-213 Implementar entrada da tela de UpgradeChecklist pela tela de Settings

## [v10.12.37] - 2019-04-17
### Added
- PM-1044 Implementar os deep links
- PM-1042 adiciona testes nos models dos widgets no recibo
- PM-1087 adiciona log para erro 1 e erro 666
- CCBO-929 Adicionado dropdown para esconder os lançamentos futuros na tela de detalhamento de fatura
- CCBO-930 mudança na tela de selecao tipo de pagamento da fatura

### Changed
- PM-977 Fazer as chamadas das classes de AntiFraud para a nova classe de requisição
- CCBO-913 Mudança na tela de detalhes de faturas que tem comportamento e informações a mais

### Fixed
- PM-1108 Botão de acelerar análise no recibo não funciona
- PM-1030 Texto "cortado" na tela de QRCode do usuário para iPhones de tela pequena
- PM-1080 Scanner do Estacionamento Shopping Vitória não lendo os códigos de barras
- CCBO-950 ajustado enum de regras do wallet da carteira que estava diferente do android, tendo que criar regra especifica para o iOS no remote config
- CCBO-951 Data de vencimento na tela de pagamento estava aparecendo com um dia a menos sempre
- CCBO-958 correção no tap em cima do dropdown de faturas recentes
- CCBO-corrigido o problema que estava fazendo com que não aparecia a data por inteiro no detalhes da fatura
- Scanner nativo não conseguindo ler o código do estacionamento Shopping Vitória

## [v10.12.36] - 2019-04-11
### Added
- CCBO-905 evento para o registro do PicPay Pós

### Fixed
- PM-1060 Tela antiga do rotativo sendo aberta ao tocar no ícone do rotativo e então em pagar

### Changed
- PM-1077 Transformar ConsumerAddressItem em Codable

## [v10.12.35] - 2019-04-05
### Added
- CCBO-903 adicionado evento nos cards de oferta da carteira, evento fica no remote config
- CCBO-901 alerta de pagamento ao pagar um fatura com saldo, indicando quantos dias leva para processamento
- CCBO-752 modificado o jeito em que o boleto é mostrado, agora ele aparece com visual do aplicativo PicPay e não PicPay Pós

### Removed
- Vários trechos de códigos comentados e não utilizados

### Fixed
- CCBO-910 bug que não deixava tocar no botão de pagar fatura
- CCBO-923 bug que não etava fazendo a validação do valor que entrava na tela.
- PM-1041 ativa linter para as views do recibo

### Changed
- PM-1039 Remover deprecated do iOS 10.3 para baixo


## [v10.12.32] - 2019-04-02
### Fixed
- Prevenção para que o usuário atual não seja delogado

## [v10.12.33] - 2019-03-27
### Fixed
- Usuário sendo jogado para a tela de login

### Changed
- PM-975 Crop da imagem de perfil ficando diferente da pré visualização

## [v10.12.32] - 2019-04-02
### Fixed
- Prevenção para que o usuário atual não seja delogado

## [v10.12.31] - 2019-03-27
### Fixed
- PM-1000 Ajustar a bolinha do gráfico do rendimento

### Added
- CCBO-837 adicionado remote config para uma parte do texto de cadastro em análise do PicPay Pós
- Adicionado função para começar a padronizar os arquivos de strings no projeto
- CCBO-778 adicionado possiblidade de pegar documentos do PicPay Pós via galeria de fotos

### Changed
- PM-822 Remove o código duplicado que faz parse dos widgets
- PM-1028 Removido as anotações de available até o iOS 10 e fix do Widget

## [v10.12.29] - 2019-03-21
### Changed
- PM-994 Nos Ajustes, mudar o botão de contas vinculadas da seção "Minha conta" para “Configurações"
- CCBO-780 Mudado comportamento do popup de quando um usuário esta com a fatura atrasada, antes estava indo para o pagamento da fatura, agora ele vai para o detalhe da fatura

### Fixed
- PM-986 Ajustes no botão de Pagar outra conta, no recibo de pagamento de boleto
- PM-972 Sugestões e histórico de pesquisa permanecendo em cache ao alternar entre contas de usuário

## [v10.12.28] - 2019-03-20
### Added
- PAY-66 Adicionar SDK Adyen 3ds;
- PAY-67 Cria tratamento de erro para transação 3ds no P2P;
- PAY-68 Realiza challenge da transação com o retorno do erro 412;
- Cria testes para integração PP e Adyen;
- PAY-100 Permite responder o challenge da Adyen
- PAY-55 Exibe alerta no primeiro uso do 3ds
- PAY-128 adiciona 3ds pra birthday gift
- CCBO-767 adicionado valor já preenchido no pagamento da fatura, para que o usuário não precise ficar digitando
- CCBO-730 adicionado pull-refresh na home do picpay pós
- PM-963 Implementação nativa do leitor de QRCode / DataMatrix junto com ZXing através do remote config

### Changed
- PM-973 Ajustar tamanho da logo sobre o QR code para não prejudicar sua leitura
- PM-918 Mudando as chamadas de cartão de crédito para nova camada de serviço
- PM-992 Ajustar tamanho da célula de cartão de crédito quando é PicPay Pós
- CCBO-879 adicionado sessão de transações em processamento
- CCBO-878 mudança no texto de crédito ativado

### Removed
- PM-921 Deletando classes nao utilizados do PicPay/Controllers

### Fixed
- CCBO-845 ajustado erro de português no popup de trocar a data de vencimento do PicPay Pós
- PM-1025 Verfiificar identidade estava ficando com loading infinito.

## [v10.12.27] - 2019-03-14
### Changed
- PM-944 Mudancas no layout do savings

### Fixed
- PM-966 Fix Constraints quebrando nas células de cartão da carteira

## [v10.12.26] - 2019-03-12
### Removed
- PM-752 Remover classes com prefixo MB que não são utilizadas
- PM-976 Remover chamadas de serviços que não são mais usados

### Added
- PM-910 Mandar localização em todo header de requisição (caso exista localização recente persistida)
- PM-737 iOS continua recebendo push após deslogar usuário

### Changed
- CCBO-825 mudança de nome, tudo que era Credito PicPay virou PicPay Pós

## [v10.12.25] - 2019-03-07
### Added
- CCBO-821 adicionado tela de informações sobre o parcelamento de um faturo do PicPay Pós
- PM-962 Login automatico após concluir o processo do 2FA com sucesso

### Changed
- PM-649 Centralizar verticalmente label de cartão principal em métodos de pagamento
- PM-955 Ajustar a cor da safe area no pagamento do parking
- PM-959 Alguns textos do 2FA

### Fixed
- CCBO-842 ajustado comportamento e bug que não mostrava os erros que existe no envio de documentos.
- CCBO-841 corrido problema que estava tendo de o usuário não conseguir fazer scroll assim que o teclado do formulário estava presente

## [v10.12.23] - 2019-03-01
### Changed
- PM-820 Bibliotecas para o Carthage
- PM-536 PicPayDeviceId e InstallationId movido do AppParameter para o UIDevice
- PM-802 Ajuste na posição do botão de parcelamento
- PM-914 Mudar a webview do original para mandar o developer key como header
- PM-938 Alteração dos nomes dos eventos do 2FA
- PM-796 Habilitar bitcode e atualizar Helpshift
- PM-956 Remover código do Google Analytics que está diretamente no projeto e adicionar no gerenciador de dependências

### Added
- CCBO-835 adicionado eventos para o lançamento do crédito (não vai ser mais esse o nome)
- CCBO-832 Adicionado opção de ao tocar em cima de um parcela do crédito já ir para a tela de pagamento com os valor preenchido

### Removed
- PM-930 Retirando codigo comentado do TrustKit
- PM-919 Remove MBConsumerAddress

### Fixed
- PM-941 Fix da privacidade do pagamento do novo rotativo novo rotativo.
- PM-623 Corrigir layout do Card.io no cadastro de cartão iPhones com telas maiores
- PM-641 Pagamento enviando id de cartão de crédito excluído

## [v10.12.22] - 2019-02-26
### Changed
- PM-917 Ajustar cor dos campos desabilitados na tela de Dados Pessoais e Endereço
- PM-936 ajustado teste que estava quebrando e atualização de pods
- PM-920 Trocando as referencias de AppParameters para CreditCardManager para validar carta
- PM-936 ajustado teste que estava quebrando e atualização de pods
- PM-923 Desabilitar a verificação de identidade quando o status vier DISABLED do backEnd
- PM-891 Ajustar pergunta sobre a biometria para toda vez que o usuário entrar no aplicativo
- PM-901 Titulo do botão de reenvio de código para cada canal de validação do 2FA

### Fixed
- CCBO-832 no cadastro do crédito a data de nascimento sempre estava indo com a data de emissão do documento selecionado
- PM-765 Verificar comportamento de quando tem muitos cartões e entra e sai da carteira, o botão de adicionar fica abaixo das abas

## [v10.12.20] - 2019-02-22
### Added
- PM-877 Teste do 2FA usando snapshot
- PM-928 Incluir botão de inserir código promocional na carteira

### Removed
- CCBO-757 remoção da etapa de assinatura do usuário, não precisando mais de assinar para concluir o cadastro do crédito
- CCBO-755 remoção da tela de seleção de limite de crédito na hora da contratação docrédito
- CCBO-759 remoção de campos do registro do crédito

### Changed
- CCBO-715 adicionado link no texo em que esta sublinhado na tela de resumo ao contratar credito
- PM-815 Refactor do SettingsUnsernameViewController
- CCBO-763 alteração no fluxo e view de setar e alterar data de vencimento
- PM-676 Remoção de várias classes/Métodos objc que estavam na pasta MBClasses ou estavam relacionadas com essas classes e não eram utilizadas.
- PM-906 Alinhando o marcador do grafico do rendimento
- PM-905 Ajustes na tela de rendimento para setar o inset quando voltar a info de rendimento
- PM-889 Quando codigo de error vier vazio, entao colocar Empty Code
- PM-871 Atualizar o feed quando o mesmo for visível
- CCBO-799 mudança alogica de pagamento de fatura do crédito e também Codable para os models.
- CCBO-799 mudança alogica de pagamento de fatura do crédito e também Codable para os models.
- PM-523 Mudando as chamadas de allConsumerData para ConsumerManager
- CCBO-801 mudança no comportamento do alerta de oferta do crédito e ajustes para controlar o credito do usuário criando um helper para isso
- CCBO-772 adicionado url de saiba mais na apresentação de contratação de crédito

### Fixed
- PM-893 Ajustes na exibição da tela de verificação de cartão
- PM-926 BugFix para o problema de deslogar o usuário automaticamente
- PM-875 Inconsistência no load das sugestões

## [v10.12.19] - 2019-02-15
### Changed
- PM-895 Mandar id na query help center
- CCBO-761 trocado label do header do crédito na carteira para Limite de Crédito PicPay para quando o usuário tem crédito
- PM-908 ajustes no request manager para que o SwiftyJSON não precisa do try para interpretar o JSON do parser
- PM-909 Alterar nome do parâmetro Help Center
- PM-908 ajustes no request manager para que o SwiftyJSON não precisa do try para interpretar o JSON do parser
- PM-909 Alterar nome do parâmetro Help Center

## [v10.12.17] - 2019-02-14
### Added
- PM-851 Track dos eventos de 2FA
- Eventos de quando user token esta nulo ou quando seta o user token como nulo
- PM-853 Password Autofill no login

### Changed
- PM-603 Refatorar Settings
- PM-884 Desabilitar zoom no grafico do rendimento
- PM-899 Garantindo que o endereço não tenha nenhum valor nil

### Removed
- PM-863 Remover PicPayPlasticCard

### Fixed
- PM-859 Chamadas de View que estão acontecendo em Background Thread

### Fixed
-  PM-887  Bug DeepLink PAV

## [v10.12.16] - 2019-02-08
### Added
- PM-836 Disparar eventos associado ao hint de parcelamento
- PM-830 Fallback para enviar código do 2FA por email
- CCBO-692 adicionado gradient na header do crédito na carteira
- CCBO regras de dismiss e apresentação no OfferCards da carteira
- CCBO-721 adicionado ação no valor de limite do crédito, na header que aparece na carteira

### Changed
- PM-862 Remover WalletViewController do projeto
- PM-873 Evento para enviar erro mesmo sem mensagem nas chamadas do legado
- CCBO-737 modificado texto dos cartões para formas de pagamento
- PM-345 Convertendo swift 4 para 4.2
- CCBO-710 mudança no nome da tela de Origem para Local de nascimento, junto com modificações na classe que estava no storyboard e foi para xib, também ajustado a chamada e realizado injeção de dependencia via construtor.
- [PM-659] Adaptação de algumas classes do 2FA para facilitar os testes unitários

### Fixed
- CCBO-720 ajustes nas datas de vencimento e fechamento do invoice de crédito
- PM-854 Bug na header do feed
- CCBO-699 tela de informações pendentes do usuário não estava pegando as informações do modelo correto
- PM-856 Resolvendo Index out of range na tela de parking nova
- PM-866 Bug na tela de digitar valor de retirada no iOS 10
- CCBO regras de dismiss e apresentação no OfferCards da carteira

## [v10.12.15] - 2019-01-30

### Changed
- PM-813 Alteração nos textos do savings

### Added
- PM-807 Exibir captcha no login caso backend retorne erro com cod. 2674

### Fixed
- PM-826  Fix Banner MGM
- PM-823  Bug na Biometria para iOS 11.0.0
- PM-829 Fix do Recibo que não era exibido quando o pagamento efeito a partir de um perfil com outro perfil aberto
- PM-834 Texto quebrado na tela de formas de pagamento
- PM-857 Bug cache do consumer data

### Added
- PM-817 Adicionado o saque instantâneo quando for Original

### Added
- PM-817 Adicionado o saque instantâneo quando for Original

## [v10.12.14] - 2019-01-23

### Added
- PM-757 Adiciona tooltips e melhora as mensagens de erro no parking
- PM-755 Adiciona máscara para as placas antigas e novas no parking
- PM-683 Adicionado Saque em Lotérica
- CCBO-657 Card de oferta do Crédito na Carteira assim como eventos
- PM-818 Exibição do hint de parcelamento no PAV e DG

### Changed
- CCBO-684 mudança no endpoint de enviar boleto da fatura do crédito picpay via email
- PM-660 Refatorar a classe de Keychain para fazer tudo nativo e retirar a biblioteca do Locksmith
- PM-624 Movido todas as imagens que estvam soltas pelo projeto para dento do .xcassets

### Fixed
- PM-730 Fix Bug PersonalDataViewModel
- PM-759 Corrige espaçamento do slider no Parking

### Changed
- PM-764 Permitir apenas letras minúsculas na edição e cadastro username.

## [v10.12.13] - 2019-01-16

### Added
- PM-725 adiciona testes pra ParkingRootViewModel, ParkintVehicleEditViewModel e ParkingModel
- CCBO-666 adicionado atributos no remote config para teste ab na tela de webview de oferta de crédito para quando o usuário entra um primera vez

### Changed
- PM-602 centraliza chaves do UserDefault na KVKey e KVStore
- PM-720 Load da lista de cartões de crédito quando recebe qualquer push foi alterado para carregar apenas o saldo (Para evitar popup recorrente de erro na carteira)
- PM-804 Refatora a classe UIPPButton

### Fixed
- Fix do label de e-mail da notificação do 2FA
- PM-753 Correção do bug no PaymentViewController, linha 234, exibido no Crashlytics
- PM-701 Fix da animação na view superior de quando trocava o pagamento do cartão pela pagamendo com saldo

## [v10.12.12] - 2019-01-11
### Changed
- PM-706 Cria constante para IsNewParking e IsOldParking

### Removed
- PM-629 Remove Core Data do projeto

### Added
- PM-615 Novo tipo de notificação para exibir acesso a novo dispositivo
- PM-686 Adicionando Dynamic link no login
- CCBO-641 adicionado scroll infinito para listagem de transação na home do crédito
- PM-704 Nova regra para hint de parcelamento

### Fixed
- PM-673 Fix bug no parcelamento do e-commerce
- PM-691 Fix bug no parcelamento que não estava limitando o valor de parcela mínima
- PM-701 Bug de popup de cartão não cadastrado quando já tem um cartão cadastrado
- CCBO-653 bug fundo transparente e layout e célula de transação que estava quebrando
- PM-744 Fix seleção de veículo sem tipo no novo Parking
- PM-650 Layout de células de métodos de pagamento "quebrando" no iOS 9 e 10
- PM-729 Fix warnings de constraints da ParkingRootView

### Changed
- PM-612 Retirada da chamada do getGlobalBalance, e chamar ela na tela de paymentMethods apenas se a chamada de paymentMethod nao view com o balance
- PM-736 Exibir a célula "usar saldo para pagar" sempre acima dos meios de pagamento
- PM-639 Aplicar MVVM para a lista de endereços
- PM-671 Ajustes na tela de error do Savings
- PM-742 Ajustar layout da célula de endereço em assinatura

## [v10.12.4] - 2019-01-08

### Changed
- Mudando o valor default do remote config pra false

## [v10.12.3] - 2019-01-04

### Fixed
- PM-739 Hotfix no Parking pra aderir a opção do usuário de usar ou não o saldo da carteira

## [v10.12.2] - 2019-01-03

### Added
- PM-631 Novo tipo de código promocional que abre webview e sem relação com convite de usuário (Só funciona no backend na versão 10.11.1 do app)
- PM-635 nova tela de pagamento da VixPark

### Changed
- PM-658 Refatorar Chamada WebView

### Fixed
- PM-669 Bug ao abrir deepLink pelo helpCenter
- PM-692 Bug no top header da carteira ao fazer scroll para o crédito
- PM-762 ajustado o scrollview da tela de sacar e adicionar dinheito
- PM-693 bug de upload de foto de recarga
- PM-699 bug que dava crash no aplicativo quando aparecia o popup de senha e o usuário cancelava, principalmente quando o usuário ia retirar dinheiro do picpay
- PM-695 Bug TaxsAndLimits Limites de Recebimento Trocados

## [v10.9.24] - 2018-12-13

### Added
- CCBO-615 Adicionado remote config para habilitar a possibilidade de ativar a opção de ajustar o limite pelas configurações
- CCBO-611 Adicionado deeplinks para o crédito
- PM-595 Novo fluxo de autorização de Dispositivo

### Changed
- PM-627 Remover limite do formulário de saque
- PM-633 Ajustes na lista de formas de pagamento
- PM-628 Refatorar TaxsAndLimits em Settings
- CCBO-609 mudanças no detalhe da fatura, adicionando teste unitários para as viewModels
- PM-655 Refazer tela de alterar senha
- ajustes para o projeto rodar no CI
- PM-662 atualizado versão do Fabric e do Firebase para suportar ultimas funcionalidades do crashlytics do Firebase
- CCBO-607 mudanças de comportamento na home do credito picpay
- PM-653 trocado todas as cores do PicPayStyles de UIColor para ColorLiteral e também adequado a novas cores que estão no Figma
- PM-640 Refatorar  tela de dados pessoais
- PM-688 possibilidade de parcelar em transações P2P

### Fixed
- PM-579 Corrige bug da mensagem de erro na aba da carteira
- PM-564 Bug no envio de fotos do documento de identificação, na Verificação de Documentos
- PM-630 Ajustado a altura minima da carteira, antes etava ficando quebrado em iphones menores
- PM-646 Trocar a variavel da suggestionView com force unwrap para lazy
- PM-642 Fix no campo de adicionar dinheiro
- PM-645 Ajuste para evitar o problema que abre o chat do helpshif sem identificador do usuário.
- PM-630 Ajustado a altura minima da carteira, antes etava ficando quebrado em iphones menores
- PM-598 ajustado o bug que estava ocorrendo ao fazer o pull-refresh na view
- PM-663 Fix crash DGImportantDetailCell

### Removed
- PM-629 Remove CoreData

### Added
- PM-585 Adicionado funcionalidade de rendimento

## [v10.9.21] - 2018-11-30

### Changed
- PM-591 Ajustes iPhone X’s na tela de sucesso de inserção de código promocional de novo usuário
- PM-593 adicionado cardIO diretamento no projeto, assim o projeto não mais envia os pods para o git, necessitando dar um pod install na primeira vez que se inicia o projeto
- CCBO-598 Mudança nas opções de boleto de fatura do crédito PicPay
- PM-574 Primeiras telas do fluxo de 2FA (Em andamento)
- PM-520 Remove referência do PaymentConfig da AppParameters
- PM-527 Remove referência do UseBalance da AppParameters
- PM-597 Adiciona validação para as novas placas MercoSul do Brasil
- CCBO-589 Ajustes visuais na carteira e também modificações de comportamento e visuais para o crédito

## [v10.9.18] - 2018-11-26

### Fixed
- Hotfix porcentagem da taxa de pagamento/recebido excedente
- Hotfix labels dos serviços de contas vinculadas
- PM-597 Adiciona validação para as novas placas MercoSul do Brasil

### Changed

### Fixed
- PM-578 Bug no posicionamento da view de "cadastrar novo endereço"

## [v10.9.17] - 2018-11-20

### Added
- PM-562 Teste para função sanitizedCardName em AddNewCreditCardViewModel
- PM-547 Implementado entrada no app com dynamic link do firebase pra entrar diretamente com algum deep link quando baixar o app e fizer o cadastro, e com referral code para, no fim do cadastro, ja acionar o código automaticamente.
- CCBO-592 Adicionado skeleton view para os loadings de lista do crédito
- PM-453 Colocar para que o recibo apareça como modal e fechar recibo com swipe down após pagamento.

### Changed
- PM-522 Remove hideKeyboard do AppParameters
- PM-550 Remover do projeto o MoviePlayerViewController
- PM-535 Movendo tudo relacionado ao PrivacyConfig do AppParameters para o ConsumerManager
- PM-517 Separado cache de pesquisa do cache principal do app
- PM-529 Funções de logout movidos do AppParameters para o AuthManager
- PM-531 Movendo unreadNotifications para o AppManager
- PM-532 Removendo funçōes de location do AppParameters
- PM-577 Correção de cores de algumas telas

### Removed
- PM-559 Remover dependencia do que tenha o AIAdditions e o AIAdditions
- PM-526 Removido referencias de flags do AppParameters
- PM-537 Removido referencias de EvaluatedPolicyDomainState do AppParameters
- PM-533 Removido referencias de contatos do AppParameters e adicionado ao PPContactList
- PM-621 Remoção da verficação de versão do iOS do AppParameters

### Fixed
- PM-566 corrigo problema do teclado ficar em cima do alerta de erro na tela de pagamento
- PM-575 bug da carteira que estava fazendo com que desse crash na carteira assim que entrasse no aplicativo e na carteira rápido
- PM-549 Remover todos os warnings referentes a String e deletado MGMHomeViewController
- PM-611 Bug atualização nickname nos ajustes

## [v10.9.15] - 2018-11-13

### Changed
- PM-493 Manter pilha de view controllers ao abrir deep link para o HelpCenter. Permitir retorno para a view controller anterior.
- PM-499  Remover verificar cartão no fluxo de pagamento
- PM-510 Ajustar posição dos itens da seção promoções na tela de ajustes
- PM-519 Melhorar layout do scanner do Estacionamento Shopping Vitória
- PM-538 Adaptar TabBar da tela de assinaturas para iPhone X
- PM-542 Adicionar tipo "deeplink" de ação em Button e melhorar código da PM-496
- PM-519 Melhorar layout do scanner do Estacionamento Shopping Vitória
- PM-538 Adaptar TabBar da tela de assinaturas para iPhone X
- PM-525 remoção do bank list do app parameters porque não estava sendo usado
- Remove a dependência SRRefreshView que não era utilizada no projeto
- Organiza arquivo de extension para Double e Float
- Remove LoadingPresenter protocol que não é utilizado
- Melhoria de Sintaxe no BiometricAuth
- Trocada flag do remote config
- PM-544 Readicionar opção de verificar cartão na action sheet, quando no fluxo de pagamento
- PM-524 Movendo os métodos loadConsumerBalance, setConsumerBalance e setConsumerWithdrawBalance

### Fixed
- PM-540 Botão demora a ser exibido(chamada em background) após reativação.
- Fix da ativação via remote config para contas vinculadas (feature_tps_active).
- PM-541 Resolvido crash quando uma view da tela de pagamento de digital good virava nil
- PM-518 Resolvido bug na scrollView do perfil

## [v10.9.11] - 2018-10-31

### Added
- PM-461 Quando o usuário sair no meio do questionário de verificação de identidade, enviar para o verifications a seleção parcial.
- PM-488 Mudança na cor placeholder do cartao de roxo para preto
- PM-305 Adicionar eventos na popup de visibilidade do pagamento

### Fixed
- Hotfix Popup de senha fluando para usuários com senha alfanumérico que deixam a senha salva no sistema
- PM-444 Apenas adicionar o loading quando ele nao tiver sido adicionado a View ainda.
- PM-446 Ajustar na transição da tela de validação de cartão de crédito.
- PM-441 Atualizado os pods do Firebase e do Mixpanel para evitar as chamadas internas que aconteciam erroniamente em background thread
- PM-480 Ajuste na scrollView pra voltar a imagem corretamente quando o perfil tiver pouca atividade
- PM-482 Esconder o infoButton e so mostrar ele caso ele encontre um infoUrl
- PM-491 Fix crashlytcs 3487 (Crash no abertura do cadastro de cartão no fluxo de pagamento de boleto)
- PM-497 Tirar o hidden da navigationBar quando entrar na tela de contactList
- PM-493 Recarregar as sugestoes do feed quando voltar de background
- PM-483 Tratamento do erro de conta bloqueada no login (Habilita opção de reativação de conta)
- PM-509 Mudar os UIActionSheet para UIAlertController

### Changed
- PM-463  Não deixar salvar caracter especial nem numero no nome do cartão
- PM-420 Inserir novo popup de senha em ativar biometria

## [v10.9.10] - 2018-10-24

### Added
- PM-305 Adicionar eventos na popup de visibilidade do pagamento
- PM-451 Exibir view de acesso da camera quando permissão negada (Alteração de foto do perfil)

### Fixed
- Hotfix Popup de senha fluando para usuários com senha alfanumérico que deixam a senha salva no sistema

## [v10.9.9] - 2018-10-17

### Added
- PM-327 Adicionado nova funcionalidade de vinculação de contas de terceiros com a conta do PicPay
- PM-489 Link para webView na tela do scanner CIELO
- PM-407 RemoteConfig no texto transferir para banco.
- PM-399 Deep link para scanner Cielo (picpay://picpay/pos)
- Log do breadcrumb e do id do usuário no crashlytcs

### Fixed
- PM-371 Bug - Deep link para o helpCenter enviando contexto “não logado” Com isso não vai para o artigo que deveria
- PM-459  Bug limite de quatro dígitos no pagamento do boleto

### Changed
- PM-410  Migrar os AlertView para AlertViewController, excluir UIAlertView+Blocks.h, modularizar AlertMessageObjc
- PM-364 Substituir a lib de permissões
- PM-422 Ajustar footer da tela de scanner da Cielo para iPhone X
- PM-354 Trocar alerta de mensagem de bloqueio no login (usa novo endpoint de login)
- PM-447 Ajustar opção de editar cartão de crédito do action sheet
- PM-452 Ajustar background das webviews para branco
- PM-463  Não deixar salvar caracter especial nem numero no nome do cartão

## [v10.9.6] - 2018-10-08

### Added
- PM-327 Adicionado nova funcionalidade de vinculação de contas de terceiros com a conta do PicPay

### Changed
- PM-245 Nova tela de senha para pagamento
- PM-332 Remover a biblioteca pay.cards e voltar a usar o card.io
- PM-356 Campo de bairro no cadastro de endereço

### Fixed
- PM-361 Ajustes na tela de Cartão de Transporte, em digital goods
- PM-395 Alterar descrição na label de cadastro de cartão.
- PM-427 Possível resolução do bug de quando entrava na tela de pagamento do digital goods (issue #3365 crashlytics)
- PM-369 Fix Load infinito ao cancelar, o cancelamento na Cielo
- PM-363 Fix Titulo ao voltar para tela de recarga

## [v10.9.3] - 2018-09-28

### Added
- PM-322 Na edição de cartão de crédito faz check no endereço selecionado
- PM-318 Abrir boleto em pdf
- PM-334 Eventos no fluxo de identidade
- PM-396 Disparar Evento Transação na Cielo.

### Fixed
- Ajustado o endereço de editar cartão que em determinado momentos não estava aparecendo
- PM-376 Fix Load  na célula de cadastro cartão retirada.
- Ajustado o endereço de editar cartão que em determinado momentos não estava aparecendo
- Fix not_avaliable para not_available na validação de cartão de crédito
- PM-362 Fix lowercase Tab
- PM-348 Ajustes na validação de cartão de crédito
- PM-379 Alterara textos no fluxo de validação de cartão
- PM-381 Fix cor no background das imagens da lista de sugestões

### Changed
- PM-324 Abrir vendas por assinatura dentro do safari
- CCBO-308 modificado a logica para exibição de cartão para quando for cartão de crédito da PicPay
- PM-335 Ajusta as configurações de exposição e foco da câmera no fluxo de identidade
- PM-346 Alteração de texto no scanner da Cielo
- Removido o scroll horizontal na home, não tem como trocar de feed de todos para o minha dando o scroll horizontal, apenas tocando nos botões para trocar
- PM-359 Integrar xib de adicionar cartão de crédito na storyboard do payment methods
- PM-317 Removido a responsabilidade de armazenamento de cartão primário do AppParametres criando assim um manager para os cartões de crédito, onde ele fica com a responsabilidade que estava no AppParameteres. Agora não se pega a lista de cartões do Consumer data, apenas da requisição de listagem de cartões

## [v10.9.2] - 2018-09-13
### Added
- Adicionado shortcut para abrir o scanner de pagamento
- PM-307 Eventos de permissão de acesso de camera, localização, contatos e notificação
- CIEL-17 Validação de cartão de crédito
- PM-378 Adicionar referências ao FaceID.

### Fixed
- PM-300 Bug: Cadastro de endereço não remove view de lista vazia
- PM-292 Habilita a abertura automática do teclado ao entrar na tela de adicionar novo endereço
- PM-260 Scroll para o topo quando apertar em inicio.
- PM-287 pull refresh do feed da home não estava recarregando as sigestões e também não atualizava o saldo.
- PM-293 Botão de "saiba mais" exibido no pagamento de bilhete único, mesmo quando url não é recebida.
- PM-302 Bug: Adicionar Remote Config em report profile
- PM-315 Bug na altura da barra de atividades da tab início
- PM-319 Corrige exibição de texto aparecendo truncado na tela de edição de username
- PM-320 Na tela de edição do username o texto embaixo do campo não está sendo exibido corretamente
- PM-313 Descrição melhorada para endereço

### Changed
- PM-273 Adicionado o swipe para voltar os campos no cadastro de cartão
- PM-290 Acertos na análise de identidade
- PM-208 iPhoneStoryboard deletado
- PM-310 Atualizar saldo sempre que a barra de pagamentos for exibida
- CIEL-82 Alterado texto do cabeçalho da tela de formas de pagamento via CIELO.
- Editar cartão de crédito exibe endereço completo

## [v10.9.1] - 2018-09-03
### Added
- Novo método de pagamento P2M Cielo

## [v10.8.14] - 2018-08-31
### Fixed
- Corrigindo título do popup de taxa de limite de recebimento

## [v10.8.13] - 2018-08-30
### Added
- PM-243 Adiciona tela com feedback de cartão de crédito cadastrado com sucesso
- PM-252 Log de primeiro acesso em determinadas telas.
- PM-121 Implementa telas de edição de cartão de crédito e habilita apelido no cadastro
- PM-284 Enviar MetaData para o chat do helpShift atravéd de um json e também um versionamento para que a pagina do help center consiga identificar quando for uma chamada de um aplicativo novo ou um legado

### Changed
- PM-242 Melhorias na apresentação do cadastro de endereço
- PM-249 Popup para poder cancelar pagamento de boleto em análise
- PM-274 Alterar texto e estilo do botão, em Popup de descartar cadastro e mudar endereço.
- PM-270 Alterar texto no recibo de "Total Pago" -> "Valor"
- PM-281 Alteração do código de erro 1339->1340
- PM-141 Novo comportamento das permissões na tela pagar (acesso à localização e contatos)
- PM-166 Finalizar animação de pagamento ao clicar na tela

### Fixed
- PM-289 Fix pagamento duplicado em todas as telas de pagamento

## [v10.8.12] - 2018-08-17
### Changed
- PM-206 Alteração das cores #21C25E para #11C76F e #ED1846 para #FF5075
- PM-135 Melhorias no onboarding social
- Mudado a cor rosa (que estava errada) para a cor correta
- PM-259 Mudançar para o funcionamento do projeto no xcode 10

### Added
- PM-168 Esconder a barra de seleção do tipo do feed na home, quando ainda não tenha sido exibido algum feed
- PM-249 Popup para poder cancelar pagamento de boleto em análise

### Fixed
- PM-247 Fix Feed "Minha" não está sendo atualizada após pagamento
- PM-268 Fix pagamento de boleto com valor aberto

## [v10.8.11] - 2018-08-14
### Added
- PM-139 Banner no topo do feed na Home, para usuários que acabaram de se cadastrar sem código promocional
- PM-201 Swipe para voltar na tela de detalhes da assinatura
- PM-182 Exibe a mensagem de erro quando o servidor retornar erro = 1 (usuário não autenticado)
- PM-202 popup com opção do usuário pagar taxa adicional (erro: 1339) ou então notificar o recebedor para virar PicPay PRO

### Changed
- PM-201 Descrição dos detalhes da assinatura com altura dinâmica
- PM-235 Ao cadastrar um novo endereço no fluxo de adicionar cartão, retorna para tela de adicionar cartão.
- Adicionado @objc nos atributos de classes e também em funções para dar suporte ao Swift 4

### Fixed
- PM-201 Layout para iPhoneX nos detalhes da assinatura
- PM-221 Fix bug pagamento de boleto com mesmo valor do saldo

### Changed
- PM-219 Alteração tamanho botão de acelerar análise, no feed e no recibo

## [v10.8.10] - 2018-08-13
### Fixed
- PM-237 corrigido problema de quando ia pagar um boleto e o aplicatvo dava crash (Quando o usuário pagava no cartão de crédito)

## [v10.8.9] - 2018-08-10
### Added
-  Retira qualquer elemento visual ou de interação com o crédito do PicPay caso o status do crédito esteja como DENIED [CCBO-305]
 - Edição da imagem do endereço.
 - Disparar evento ao exibir popup de cashback [PM-163]
 - Enviar notification token após o login [PM-173]

### Changed
- Ajuste do título da tela de MGB (Convidar Estabelecimentos)
- PM-133 Corrige layout da popup de alerta
- Swift 4
- PM-178 Ativa a abertura do HelpCenter somente a partir do iOS10. Nas outras versões abre diretamente o chat do helpshift.
- PM-176 Desativa pesquisa de sugestões de FAQ antes de abrir chat no helpshift
- PM-174 Não chamar addNotificationToken quando o usuário não está logado
- PR-137 Redesign da view de feed vazio (com Lottie)
- CCBO-345 Continuação dos ajustes do crédito
- CCBO-340 Mudanças realizadas na carteira para atender ao créditoCCBO-340
- PM-209 Adicionado biblioteca pay.card para escanear o cartão de crédito
- Digital Goods de Parking com scanner que ler QRCode e DataMatrix

### Fixed
- PM-183 Ajuste da cor da barra de pesquisa na tela de cadastro de conta bancária
- Fix do crash da notificação `search_people` (Ação da notificação foi removida)
- Fix na exibição do botão de opções da tela de assinaturas (não era mostrado)
- PM-198 Pedido de documentos com células de altura dinâmica (remove problemas de "estouro" do espaço com o uso de acessibilidade para aumentar os textos)
- PM-179 Ajustes nas chamadas javascript do helpcenter, elas não estavam sendo chamadas corretamente, e alguns métodos não existiam mais, o por isso que não estava funcionando
- PM-192 Nome do portador do cartão de crédito estava com a última letra minuscula

## [v10.8.8] - 2018-08-06
### Fixed
- Hotfix botão de cancelar assinatura não estava sendo exibido

## [v10.8.7] - 2018-07-27
### Fixed
- Corrigindo eventos do FB com acentos
- Ajustes feitos em algumas telas do crédito: Carteira, Home do crédito, Lista de faturas, Detalhamento da fatura, Alteração de data de vencimento da fatura
- Exibir botão de `Avançar` no Digital Goods com valor em aberto no iOS12

## [v10.8.6] - 2018-07-27
### Added
- Item 'Venda por assinaturas' na seção 'Para meus negócios' em Ajustes
- Adicionado endpoint e configurações da tela de enviar convite do crédito

### Changed
- Ajuste do título da tela de MGB (Convidar Estabelecimentos)

## [v10.8.7] - 2018-07-27
### Fixed
- Corrigindo eventos do FB com acentos
- Ajustes feitos em algumas telas do crédito: Carteira, Home do crédito, Lista de faturas, Detalhamento da fatura, Alteração de data de vencimento da fatura
- Exibir botão de `Avançar` no Digital Goods com valor em aberto no iOS12

## [v10.8.6] - 2018-07-27
### Added
- Item 'Venda por assinaturas' na seção 'Para meus negócios' em Ajustes
- Adicionado endpoint e configurações da tela de enviar convite do crédito

### Changed
- Redesign da tela de inserir código para acelerar análise
- A seção 'Para meus negócios' continua mesmo após o consumer se tornar PRO
- Redesign da tela de inserir código para acelerar análise
- A seção 'Para meus negócios' continua mesmo após o consumer se tornar PRO
- Redesign da tela de inserir código para acelerar análise

### Fixed
- Ajustes no no timer do TEF que quando voltava do background voltava para o momento que entrou em backgound ficando um timer com mais tempo que o normal
- Ajustes na tela de seleção de planos para iPhoneX
- Swipe para voltar para tela anterior através do novo perfil
- NavigationBar da tela de perfil estava piscando ao sair para outro perfil
- Fix Crashlytics 3183 - Crash ao enviar token na header do logout
- Ajustado o refeshcontrol que ficava na posição errada quando não havia sugestões

## [v10.8.4] - 2018-07-24
### Fixed
- Bugfix webview streamlabs que bugava vinculo do consumer com webhook

### Changed
- removendo toggle de apagar endereço. Permanentemete habilitado
- Aplica novo layout à tela de código promocional

## [v10.8.4] - 2018-07-24
### Fixed
- Ajustes no reload do feed na nova home
- Ajustes no Consumer Profile
- Evento de `Cashback Recebido` movido para o momento em que transação é concluida

### Changed
- Mudança da cor da statusbar de algumas telas

## [v10.8.2] - 2018-07-20
### Added
- Dispara evento de "Logout" para o Mixpanel
- eventos fb para o growth

### Changed
- Texto para expandir a célula com a descrição do plano para "Ver mais"
- Reorganização de itens e sections na tela de ajustes
- Sempre que da um erro no cadastro de cartão vai mostrar o teclado, antes o usuário estava tendo que selecionar o campo depois que dava um erro
- Adicionado versionamento de cache de requisição
- Tamanho de fonte e layout do detalhe do feed
- Refactor do consumer profile

### Fixed
- Validação da data de válidade no cadastro de cartão
- Ajustes na header da tela de notificações que em alguns casos sempre ficava visivel que não se tinha notificações ou que não tinha conexão com a internet
- Corrige crash ao abrir deeplink /payment?type=store sem o parâmetro "id". Parâmetro "id" é definido com o valor "0" caso ausente.
- Envio da token do usuário logado no request de Logout

### Removed
- Remoção do parametro de cep na validação do cartão de crédito porque não é mais preciso na API

## [v10.8.1] - 2018-07-17
### Added
- Nova busca
- Nova Home

### Changed
- Ajustado o tamanho máximo do campo de descrição do boleto
- Substitui a animação de carregamento na WebViewController pela mesma utilizada na tela de notificações

### Fixed
- Ajustado saldo ao se pagar um boleto, quando se estava pagando boleto e outro sequencialmente, fazia com que o saldo da carteira não atualizasse.
- Corrigido o posicionamento da view do card.io no iPhoneX, na tela de adicionar cartão de crédito
- Label do popup de permissão de localização (Estava falando de contatos)
- Fazendo url encode dos consumer labels para corrigir helpcenter que carregava infinitamente
- Corrigido problmema de quando o usuário era deslogado e ao logar ia para a ultima tela.

### Removed
- Remoção do framework AppsFlyer e suas chamadas de eventos.
- Remoção do lixo que tinha dicado do AppsFlier

## [v10.7.14] - 2018-07-11
### Added
- Adicionado botão de sabia mais e ícone de status no widget do recibo, onde é aberto o help center
- Novo Log interno (NSlogger)

### Changed
- Ajustado o aplicativo para sempre ficar em protrait e também o scanner do boleto, não rotacionando mais o aplicativo
- Ignora o erro 404 quando se ignora a solicição de um novo seguidor

### Fixed
- Correção ao cadastrar um cartão pela tela de pagamento
- Correção de recarregar o recibo assim que concluía a aceleração de análise de uma transação.

## [v10.7.10] - 2018-07-04
### Added
- Novo cadastro de cartão de crédito

### Fixed
- Duplicação de comentários no detalhe de uma transação
- Ao cadastrar o cartão nãoe estava fazendo com que o cartão não ficasse como ativo
- Safe area na tela de selecionar cartão pelo fluxo de pagamento
- Ajustado um bug ao carregar os comentários, as vezes ocorria crash

## [v10.7.9] - 2018-06-29
### Fixed
- Animação de esconder barra de navegação da carteira no iPhone X
- Posição do banner de notificação no iPhone X
- UISwitch de biometria, nos ajustes, estava com bug ao ativar (com erro da senha). Não estava mudando estado.
- Abertura de push de aniversariante com o aplicativo morto

### Changed
- Receipt Widget do type "label_value_centralized_item" com texto justificado

## [v10.7.8] - 2018-06-27
### Added
- Popup de opt in na escolha do plano na tela de assinatura

### Changed
- Cor do botão do pagamento da assinatura em atraso na tela de assinaturas
- Ao apresentar os cards não apresenta o `warning_text` nos detalhes da assinatura

### Fixed
- Opção sobre o producer no option sheet nos detalhes da assinatura
- Fix crahs 3055 crashlytics (Tela de MGM quando aberta a partir do recibo)

## [v10.7.7] - 2018-06-22
### Added
- Adicionado opção de abrir o deeplink pelo scanner
- Adicicionado opção de mandar parâmetros extras pelo deeplink para a tela de pagamento
- Possibilidade de fixar um valor de um pagamento atravês do deeplink
- Implementado Card para apresentar perfil já assinado na tela de planos

### Changed
- Não permite enviar campos vazios no login

## [v10.7.7] - 2018-06-22
### Added
- Adicionado opção de abrir o deeplink pelo scanner
- Adicicionado opção de mandar parâmetros extras pelo deeplink para a tela de pagamento
- Possibilidade de fixar um valor de um pagamento atravês do deeplink
- Implementado Card para apresentar perfil já assinado na tela de planos

### Changed
- Alterando forma de esconder navigation bar da carteira
- Alteração de segurança: parando de enviar token de auth na url do helpcenter
- Persistindo Installation ID no primeiro uso, não pegando sempre o vendorID do AdSupport
- Ajustado o KeyChen e UserDefaults para se comportar diferentemente para quando rodar em Development, ajustando o login de produção e desenvolvimento.
- Possibilidade de controlar a status bar pela view controller
- Agora na tela de compartilhamento social é feito uma requisição para carregar os dados que vão ser visualizador

### Fixed
- Adicionando "device_model", "device_id" e "installation_id" às chamadas da camada de WS nova
- Câmera do boleto ficava travada ao sair e entrar no aplicativo quando vinha da câmera de código.
- Reload nas seções da tela de Ajustes e na imagem de perfil ao retorno do getProfile
- Crash 2962 do Crashlytics (Orientation Error na tela de MGM a partir do recibo)
- Adicionado o token no queryString do HelpCenter

## [v10.7.3] - 2018-05-24
### Added
- Enviando consumer_id para oauth do Streamlabs

### Fixed
- Adaptacao da header da tela de cadastro de endereço para iPhone X
- Ajustado a altura da statusbar para o iPhone X no help center
- Corrigido layout na tela de trocar username
- Descrição do Digital Goods estava ficando cortada quando se tinha um link no final
- iPhone X, ajustes no background color da camera de scaneamento de codigo
- A chegagem de transação do TEF não parava de exacutar pq a camada de serviço não sabia tratar erro com id int

### Changed
- Foi trocado UITextFiled de username para UITextView na tela de alterar username
- Ao remover e for adicionar um novo banco, ao concluir ele vai para tela com banco do usuário cadastrado, antes ficava na mesma tela de cadastro

## [v10.7.2] - 2018-05-23
### Added
- Remote Config para o crédito PicPay
- Widget de MGM no Recibo com evento Mixpanel
- Possibilidade do servidor enviar atributo de ordenação dos widgets do recibo
- Adicionado alerta de descartar alterações ao sair da tela de editar endereço, (apenas se tiver mudado alguma informação)

### Fixed
- Câmera/Scanner funcionando em background (quando a tela de scanner não estava sendo exibida)
- Crash 2920 do crashlytics (editar perfil do usuário, deve abrir nova tela de ajustes)
- Crash 2809 do crashlytics (atualizar PaymentToolbar em background)
- Crash 2932 crashlytcs (Máscara de telefone)
- Removendo toque da célula de biometria
- Crash 2955 crashlytcs (Editar meu número com numero não verificado)
- Crash 2954 crashlytcs (Abrir já possou código sem numero salvo localmente)
- Ajustado a navigation bar que estava sendo ocultada na metade ao rolar a descrição no Digital Goods

### Deprecated
- Retirado o showAlert(withMessage:) todos os warnings referente a chamada

## [v10.7.1] - 2018-05-17
### Added
- Funcionalidade acelerar análise hold para transações pav/biz - Recibo
- Campo de Telefone no cadastro de cartão de crédito de terceiros
- Swipe Back para a tela de perfil
- Verificação de Identidade
- Editar lista de cartões de transporte
- Nova notificação que abre deeplink
- Card na tela de assinatura do usuário (remote config: feature_subscription_card)
- Deeplink picpay:// funcionando dentro helpcenter
- Pop up de esqueci minha senha ao errar a senha no pagamento
- Enviando token para o helpcenter por javascript
- Deeplink para abrir helpcenter com query http://app.picpay.com/helpcenter?query=lorem

### Fixed
- Aceitando códigos de erro int e string
- Campo de texto agora fica visível no iPhone4S ao editar algum campo na tela de Adicionar endereço
- Corrigido inset bottom na tela de login (iPhone X)
- Corrigido problema de autolayout no  popup de adcidicionar cartão
- removido tarja branca que ficava no no topo das notificações
- Remover linha de inset enquanto está carregando a lista de endereços
- Placeholder dos campos de adicionar novo endereço
- Navegacao do pagamento do picpay.me
- Cor de fundo da tela de lista de endereços
- Disclaimer não estava sendo exibido no pagamento de Partking
- Notificação que abre o digital goods

### Changed
- Exibe uma view em caso de erro de carregar os endereços
- Novo layout da SearchBar que fica na NavigationBar
- Tela de alteração de endereço para novo layout

## [v10.6.2] - 2018-05-04
### Fixed
- Erros de português na tela de ajuste e cadastro de endereço

### Added
- Pop up de esqueci minha senha, ao errar a senha no pagamento

## [v10.6.1] - 2018-05-03
### Added
- Deeplink de Producer com querystring para seleção do plano
- Deeplink P2P com valor de pagamento na url
- Gerenciamento de endereço do consumer (remote config: feature_address_active)
- Solicitação de endereço na seleção de um nova assinatura
- Alteração de endereço de uma assinatura ativa
- Notificações do tipo open_profile agora abrem o perfil do usuário passado no param1 do push
- Renderizar markdown na tela de `Saiba mais` do producer
- Novo layout do in-app push notification com suporte opcional a título
- Tratamento de erros na webview do


### Changed
- Pagamento do bilhete único direto na lista de items (pular passo de seleção de quantidade)
- Adicionado zero nas casas decimais na seleção de planos da assinatura
- Tela de ajustes refeita em swift
- Background da Status Bar para verde do HelpCenter

### Fixed
- Executa a busca ao clicar na notificação do tipo `search_text` e `filtered_places`
- CRASHFIX NSFaceIDUsageDescription
- Bugfix  de constraints para iOS < 11 na webview do HelpCenter
- Texto ao compartilhar boleto de recarga (removido `Optional(#)`)

## [v10.5.2] - 2018-04-25
### Fixed
- Mostrar recibo após pagamento do TEF

### Changed
- Tipo de digital goods de `parking` para `parkings`
- Ajustes de UI na tela de alteração de Email

## [v10.5.1] - 2018-04-24
### Added
- Funcionalidade para acelerar análise hold para transações PAV
- Pagamento de estacionamento

### Changed
- [ZXing adicionado de volta](https://vignette.wikia.nocookie.net/elysiumrp/images/5/5d/Phoenix-624x490.jpg/revision/latest?cb=20170719035637)

### Fixed
- Error que deslogava usuário funcionava apenas uma vez no ciclo de vida do app (para funcionar novamente era necessário reiniciar o app).

## [v10.4.6] - 2018-04-20
### Added
- Abertura do digital goods a partir do widget
- Alerta na validação do boleto

### Changed
- Loading de notificações para animação de progress linear
- Nome do widget de "PicPay Recentes" para "PicPay Sugestões"

### Fixed
- Layout do botão de saiba mais e dos texto da notificação de saque
- Suporte ao Xcode 9.3
- Alerta de mensagem de erro ao rejeitar solicitação de follow e voltar para tela de notificação
- Crashlytics 2851 (Notificação PaymentMethodChange no setUseBalance em background)
- Navegação de pagamento para abertura a partir do widget
- Load dos recentes/sugestões no widget

## [v10.4.1] - 2018-04-13
### Added
- Bilhete Único

## [v10.3.7] - 2018-04-06
### Changed
- Alert mensagem em swift com método para apresentar alerta no padrão de popup

## [v10.3.7] - 2018-04-06
### Changed
- Header da carteira com suporte a carrossel (será utilizado para exibir banner do crédito picpay)
- mascara no campo de CEP do cadastro de cartão de crédito

### Fixed
- Tela principal do app espremida. Bug que voltar para o app alterava a orientação do app ao voltar do escaner do boleto
- Camera do boleto invertida
- Bug na navegação quando contato é aberto pelo Spotlight
- Bug na barra de navegação do perfil do usuário no iphone X
- Workaround para problema de precisao na conversao de valor para formato monetário

## [v10.3.6] - 2018-04-03
### Fixed
- fix do crash 2745 do Crashlytics (Erro na main thread na tela de pagamento de boleto)
- fix do crash 2749 do Crashlytics (Erro PaymentToolbar)

## [v10.3.5] - 2018-03-28
### Added
- Ação genérica do feed, novo behavior para realizar reload da tela
- Adicionado ícones no título dos ítens na Busca

## Changed
- Loading view sempre aparece ao receber notificações
- Adicionando, no evento API Error, propriedades "status_code" e "endpoint".
- Enviando "timezone" no header de todas as requisições.

## [v10.3.4] - 2018-03-21
### Fixed
- Placeholder dinâmico para campo de busca

### Added
- Uso de código promocional pode abrir webview

### Changed
- Texto na tela de pagamento de boleto
- Text do botão de escanear boleto
- Envia bar code quando o boleto for escaneado
- Atualizado calculo do surcharge
- Corrige bug de orientação na abertura do app
- Corrige bug do valor do surcharge no boleto

## [v10.3.3] - 2018-03-15
### Added
- Recarga de TV
- Campo Estado (UF) e cidade no formulário de cartão de crédito
- Widget de recibo do tipo `html_document`
- Alertas na tela de pagamento do ecommerce

### Changed
- Envio de  `origin` no pagamento de ecommerce
- Trata erro em chamadas relacionadas a seguidores

### Fixed
- Exibir notificação não lidas de forma diferenciada
- Banner gigante no ecommerce

## [v10.2.6] - 2018-03-14
### Added
- Deeplink dentro do webview
- Banner de destaque na tela de pagamento
- Tipo de banner que abre web_view
- Tipo de banner que abre um grupo
- Pagamento de boleto
- Widget de recibo do tipo `open_url`
- Tela de animação ao finalizar o pagamento

### Changed
- Tela `UnloggedViewController`, ao clicar em ajuda vai direto para o FAQ
- Método de renderização mais simples na tela de Notificações
- Desativa o botão de parcelamento quando não disponível (pagamento Ecommerce)
- Adicionado a query string `labels=consumer_labels` na url do HelpCenter
- Desabilita possibilidade de voltar enquanto uma trasação está sendo processada
- Troca do separador do Breadcrumb de seta ( > ) para vírgula ( , )
- Alterar propriedades do breadcrumb de Tela N para T-n
- Melhorias no boleto
- Formatação do campo `total` no envio de pagamento digital goods

### Fixed
- Logo PicPay na tela de recibo (iPhone X)
- Barra cinza na tela de detalhes do feed
- Barra pagar nas telas de pagamento (iPhone X)
- Removido tabbar no fluxo de nova assinatura, quando redirecionado pelo deeplink
- Deeplink de notificações para picpay:// com parametros adicionais
- Corrige casos que o breadcrumb enviaria popup como tipo de pagamento
- Barra de informação do plano na tela de seleção de plano de assinatura (iPhone X)

## [v10.2.5] - 2018-02-22
### Added
- Action do Feed `subscription_details`
- Evento `Transaction` para fazer rastrear origem do pagamento
- Nova tela de saiba mais sobre o producer
- Evento `scanned_text` para leitura de QR Code no scanner
- Scanner de usuário para o tipo producer membership (Assinatura)

### Changed
- Placeholder do campo de busca da tela pesquisar pessoas
- Adicionado a query string `token=consumer_token` na url do HelpCenter
- Endereço do app do original na tela de recarga (url do AppsFlyer)
- Removido botão para cancelar assinatura ativa, nos detalhes da assinatura
- Pop up do limite de pagamento via cartão de crédito

### Fixed
- Cor do título `Atividades` na tela de perfil
- Resolvido problema do reuso que exibi o switch errado na tela de Ajustes

## [v10.2.3] - 2018-02-16
### Added
- Indicação que está carregando itens do feed no servidor
- Opção de copiar CNPJ na tela de recargar TED/DOC

### Fixed
- Tratamento do erro 526 para 426
- Abrir perfil ao tocar na notificação do tipo "follow_accept"
- Fix caractere indiano que crash o app

## [v10.2.1] - 2018-02-15
### Added
- Nova opção de adicionar dinheiro através de TED/Doc outras contas
- Pagamento `TEF` (Tratamento para pagamento do tipo TEF)
- HelpCenter

### Changed
- Change label do campo `Nome da Mãe` no formulário de dados pessoais
- Seleção do plano da assinatura pelos pelo valor/título
- Opção `Desativar minha conta` sempre ativa no app

### Fixed
- Notificação de comentário/curtida de pagamento de assinatura não estava funcionando
- Adicionado `@` no perfil do producer
- Texto de recorrência de pagamento removido para status `cancelled` e `activeCancelled`
- Corrige bug que não desabilitava a tela de QR code quando abria o app por push notification

### Added
- Opção de `Deixar de ser PRO`

## [v10.1.11] - 2018-01-29
### Fixed
- Crash do load no top na tela de notificações

## [v10.1.7] - 2018-01-26
### Changed
- Abrir todas telas internas de ajustes sem a tabbar
- Altera o teclado da busca

### Fixed
- Corrige crash na tela de pesquisa de pessoas
- Corrige bug no debounce da busca
- Corrige bug de permissão na nova busca

## [v10.1.6] - 2018-01-25
### Added
- Abrindo perfil de assinatura pelo feed e pela tela de comentários

## [v10.1.2] - 2018-01-24
### Added
- Ecommerce
- Adiciona possibilidade de parcelamento no Digital Goods
- Notificação para abrir detalhes de uma assinatura

### Changed
- Fala conosco do helpshift alterado para HsEnableContactUsAfterMarkingAnswerUnhelpful

### Fixed
- Texto da tela de saque
- Bug na tela de pesquisa que não limpava memória
- Corrige bug na busca depois de efetuar pagamento
- Corrige retain cycle no novo perfil
- Corrige problema no scroll do cadastro de cartão de crédito
- Corrige bug de teclado na busca

## [v10.1.1] - 2018-01-19
### Added
- Assinatura (pagamento recorrente)
- Apresentação de carousel com onboard messages do remote config na tela de login

### Fixed
- Glitch com usuário deslogado (usuário que autorizou notificação e entrava na tela de login retornava a tela de deslogado)
- Limpar resultado da busca após pagamento (estava limpando apenas o textfield o resultado)
- Fix do icone de alert no feed (exibir apenas se for warning)

## [v10.0.29] - 2018-01-18
### Fixed
- Compatibilidade iPhone X
- Corrige crash na tela de busca de pessoas
- Corrige bug na tela de busca de pessoas

### Changed
- Barra de pesquisa na tela de busca de pessoas atualizada
- Nova UI pra busca sem resultados e erro de internet
- Mostra a distancia dos locais na busca

## [v10.0.27] - 2018-01-16
### Fixed
- Reaload do feed não atualizava saldo

## [v10.0.26] - 2018-01-16
### Changed
- Correções e melhorias na mapa
- Correções e melhorias na navegação da busca

## [v10.0.24] - 2018-01-11
### Added
- Mapa na aba Locais

### Changed
- Efeito de seleção das tabs da busca

## [v10.0.23] - 2018-01-10
### Changed
- Layout da busca

## [v10.0.22] - 2018-01-09
### Added
- Nova busca

### Changed
- Helpshift SDK update to 6.4.1

## [v10.0.15] - 2018-01-08
### Added
- Setando customer id no AppsFlyer para integrar AppsFlyer e Mixpanel
- Disparando evento de criação de conta para o AppsFlyer

### Fixed
- Header da carteria se movendo ao editar(remover) cartão de crédito
- QR scale para remover blur do QR

### Changed
- API de contatos (Utilizando novo framework de contacts do iOS 9)

## [v10.0.14] - 2017-12-29
### Fixed
- Removendo imagem do qrcode para telas com larguras menores que 320. Não estava escaneando

## [v10.0.13] - 2017-12-29
### Fixed
- Bug do field de comentário (estava dando foco no botão de pagar da tabbar)
- Bug: O scanner estava abrindo com slide quando abria o app a partir de um push
- Alerta de error ao abrir deeplink (picpay.me) com usuário inválido
- Glitch da header (convidar) na tela de buscar pessoas quando está buscando e quanto não encontra resultado
- Bug scanner ao tocar no icone do scaner não abre camera

## [v10.0.12] - 2017-12-23

### Fixed
- Renderizando HTML na descrição da tela de Taxas

## [v10.0.8] - 2017-12-20
## Added
- Tratamento do usuário que não tem contatos com conta no picpay
- Nova taxa/limite para pagamento com cartão de crédito na página de Taxas e Limites
- Evento `Virou PRO`

### Removed
- Remoção do evento `Abriu App`

### Fixed
- Popup de permissão de acesso a contatos/localização em baixo da tabbar
- Ícone do QRcode na barra de navegação

## [v10.0.7] - 2017-12-19
### Added
- Tratamento para o erro de limite excedido (1338)
- Firebase-Performance pod
- Scroll pro topo do feed se clicar duas vezes na tabbar
- Opção de copiar código identificador na tela de adicionar saldo do Itaú

### Fixed
- Bug na altura da célula de onboarding de cartão na carteira
- Bug da busca travada depois do pagamento

## [v10.0.6] - 2017-12-15
### Added
- Redesign da barra de pagamentos
- Tratamento para o erro de limite excedido (1338)
- Processamento do syncdata na nova camada de serviço
- Firebase-Performance pod
- Evento de analytics na abertura do app
- Scroll pro topo do feed se clicar duas vezes na tabbar

### Fixed
- Voltar para o feed (PopToRoot) da carteira quando um pagamento é feito
 @@ -18,6 +21,7 @@ O arquivo está formatado com base no [Keep a Changelog](http://keepachangelog.c
- Scanner abrindo câmera quando app entrava em foreground
- Bug de scroll no formulário de cadastro de conta bancária
- ajuste da posição do on board do feed

### Changed
- Layout da previsão de conclusão do saque
- Atualização do feed após a conclusão do on boarding social

## [v10.0.5] - 2017-12-07
### Fixed
- Fixing deploy

## [v10.0.4] - 2017-12-07
- Nova tela de Pagamentos
- Nova navegação por abas
- Tela de formas de pagamento
- Novo scanner com slide
- QRCode para receber pagamentos

## [v9.5.28] - 2017-11-13
### Fixed
- Corrigindo configuração de notificação para novidades e promoções

## [v9.5.27] - 2017-11-10
### Added
- Configuração de notificação para novidades e promoções
- UI do digital codes refeita

## [v9.5.25] - 2017-11-07
### Added
- Opção de adicionar conta bancárias de terceiros

## [v9.5.24] - 2017-11-06
### Fixed
- Não exibia o voucher receipt widget, ao visualizar o recibo
- O endereço de email no alerta, quando email já verificado

## [v9.5.23] - 2017-11-02
### Added
- Agora usuário pode editar o próprio email, sujeito a verificações diversas dependendo do estato de verificação do email antigo. Textos em remote config

### Changed
- Tela para solicitar permissão a camera no pagamento com scanner para melhor usabilidade
- Universal link agora também pode predefinir o valor do pagamento

## [v9.5.22] - 2017-10-30
### Added
- Digital codes
- Novo fluxo helpshift indo direto para o FAQ (com feature toogle para o botão de Fale Conosco)
- Adiciona tratamento para parametro "value" nos deeplinks

### Fixed
- Botão para alternar tipo de teclado de senha no login
- Saque Picpay, ao selecionar um novo banco o formulário de conta bancária não atualizava o formulário
- Possíveis problemas de UI na tela de selecionar valor de recarga no 7 plus

## [v9.5.20] - 2017-10-28
### Added
- Mixpanel user token notification

### Changed
- Colocando label "Transação" no id da transação do recibo

### Fixed
- Crashlytics issue 1754 e 628 (Pesquisar pessoas e onboarding social)
- Crashlytics issue 1439 (Tela de pagamento sem nenhum dado)
- Crashlytics issue 1743 (Tela de pagamento estado não confirmado)
- Crashlytics issue 17 (Erro intermitente nas linhas de withdrawal na lista de notificações )
- Notificação filtered_places
- Aceleração de analise para telas pequenas

## [v9.5.15] - 2017-10-25
### Fixed
- Descrição da permissão de salvar foto na biblioteca (erro ao salvar recibo)

## [v9.5.13] - 2017-10-25
### Fixed
- Fix do tamanho do campo message na tela de pagamento P2P

## [v9.5.12] - 2017-10-24
### Fixed
- Texto do botão de adidionar dinheiro
- Fix do download do pdf do boleto
- Feature Toggle para a section Promoções nos Ajustes

## [v9.5.11] - 2017-10-23
## Added
- Remote config para MGB (feature_mgb com default false)

### Changed
- Novo ícone
- Barra de pagamento unificada (Layout antigo)

### Fixed
- Execução de ignorar a solicitação de seguir ao sair da tela de notificações
- Push para tela de pagamento (iOS 11)

## [v9.5.5] - 2017-10-20
### Added
- Privacidade de aniversário
- Nova configuração de push de aniversário de terceiros

## [v9.5.3] - 2017-10-18
### Added
- Confirmação da ação genérica no detalhe da transação
- Autenticação para ação genérica no detalhe da transação

## [v9.1.83] - 2017-10-13
### Changed
- Mudando atalhos do helpshift para usuários PRO

### Fixed
- Crash de saque que tentava ir para tela antiga
- Texto na tela de saque

## [v9.1.81] - 2017-10-11
### Added
- Tab de serviços na tela pagar
- Fluxo de recarga de celular
- Tela de recibo com Widgets

### Changed
- Apresentação de recibo com a nova tela com widgets
- Removido campo de CPF do cadastro de conta bancária

## [v9.1.80] - 2017-10-09
### Fixed
- Texto referentes ao saques para conta do Original

## [v9.1.79] - 2017-10-06
### Fixed
- Telas / fluxo de saque

## [v9.1.78] - 2017-10-05
### Added
- Novas telas / fluxo de saque para banco original

### Changed
- Telas / fluxo de saque

## [v9.1.77] - 2017-10-02
### Added
- Opção de reativação de conta no login

### Changed
- Placeholder na tela configurações para usuário sem foto

### Fixed
- Ajuste para prevenir crash causado por dados inválidos no PPContact

## [v9.1.76] - 2017-09-25
### Fixed
- Ajustes de layout nas telas de recarga e link do app do original (Recarga picpay)

## [v9.1.75] - 2017-09-23
### Added
- Novas telas de adicionar dinheiro (Recarga picpay)

## [v9.1.68] - 2017-09-15
### Added
- Remote Config para compartilhamento de código do MGM (feature_mgm_code)

## [v9.1.67] - 2017-09-07
### Added
- Agrupamento das solicitações de seguir
- Opção de desfazer ao ignorar uma solicitação de seguir

### Fixed
- Estado da solicitação de seguir, quando ignorada

## [v9.1.66] - 2017-09-05
### Added
- AppsFlyer Track AppLaunch
- Remote Config para Report de profile (feature_report_profile)

### Fixed
- Crash do image_url_large na tela de aniversariante

## [v9.1.65] - 2017-08-28
### Added
- Tipo genérico de ação do feed, para permitir execução de feed behaviors
- Feed item behavior reload

### Changed
- IDs e Sections do FAQ

## [v9.1.64] - 2017-08-22
### Fixed
- link do regulamento do MGB

## [v9.1.63] - 2017-08-21
### Added
- Push `mgb` para abrir tela de compartilhamento do código (MGB)

### Changed
- Push `mgm` para abrir tela de compartilhamento do código (MGM)

## [v9.1.62] - 2017-08-16
### Added
- Novo botão de compartilhamento do código de indicação do Business (MGB)

### Changed
- Novo perfil altarado para definitivo
- Stroke na imagem do perfil para evitar fraude do badge verifield

### Removed
- Feature toggle do novo perfil

## [v9.1.61] - 2017-08-09
### Added
- Popup de compartilhamento do agosto verde no recibo

### Fixed
- Remoção do texto optional(username) na ação de remove/Cancelar seguidor
- Reusable do botão de follow na tela de pesquisar pessoas
- Conta privada / Sem transações na tela do novo perfil estava cortado

## [v9.1.60] - 2017-08-05
### Added
- Opção de cancelar conta

## [v9.1.58] - 2017-08-05
### Added
- 3DTouch para itens do feed
- Widget de Recentes

## [v9.1.54] - 2017-07-28
### Added
- Seção de Promoções na tela de ajuste do usuário
- Report de Comentário, Transação e Perfil (Desabilitado via Feature Toogle / Não implementado no server)

### Fixed
- Load do feed na tela de perfil (Texto para feed fazio)
- Botão de editar no novo perfil

## [v9.1.53] - 2017-07-07
### Fixed
- Seleção de placa no pagamento da vixpark

## [v9.1.50] - 2017-07-07
### Added
- Novas opção de ajuda (diferenciado para usuario PRO e normal)
- Parcelamento para PAV

### Fixed
- Load do novo perfil
- Cor do helpshift

## [v9.1.45] - 2017-07-07
### Added
- Novo cadastro de conta bancária
- Novo fluxo de saque
- Novo perfil de usuário
- Formulrio para alteração do nome da mãe
- Username no recibo P2P
- Opção de selecionar placa do carro (Vixpark)
- Opção de remover placa do carro (Vixpark)
- Fluxo de filtro para usuario PRO
- Tela de Taxas e Limites

### Changed
- Subistituição do ZXing por um Scanner nativo

### Removed
- Remoção do ZXing

### Fixed
- Removida opção de voltar para o cadastro a partir da tela de username
- Texto do Botão cancelar do convidar amigos


[Unreleased]: https://github.com/PicPay/picpay-ios/compare/v10.19.9...HEAD
[pf@v10.19.9]: https://github.com/PicPay/picpay-ios/compare/v10.19.8...v10.19.9
[v10.19.9]: https://github.com/PicPay/picpay-ios/compare/v10.19.8...v10.19.9
[v10.19.8]: https://github.com/PicPay/picpay-ios/compare/v10.19.7...v10.19.8
[v10.19.7]: https://github.com/PicPay/picpay-ios/compare/v10.19.6...v10.19.7
[v10.19.6]: https://github.com/PicPay/picpay-ios/compare/v10.19.5...v10.19.6
[v10.19.5]: https://github.com/PicPay/picpay-ios/compare/v10.19.4...v10.19.5
[v10.19.4]: https://github.com/PicPay/picpay-ios/compare/v10.19.3...v10.19.4
[v10.19.3]: https://github.com/PicPay/picpay-ios/compare/v10.19.2...v10.19.3
[v10.19.2]: https://github.com/PicPay/picpay-ios/compare/v10.19.1...v10.19.2
[v10.19.1]: https://github.com/PicPay/picpay-ios/compare/v10.19.0...v10.19.1
[v10.19.0]: https://github.com/PicPay/picpay-ios/compare/v10.18.24...v10.19.0
[v10.18.24]: https://github.com/PicPay/picpay-ios/compare/v10.18.23...v10.18.24
[v10.18.23]: https://github.com/PicPay/picpay-ios/compare/v10.18.22...v10.18.23
[v10.18.22]: https://github.com/PicPay/picpay-ios/compare/v10.18.21...v10.18.22
[v10.18.21]: https://github.com/PicPay/picpay-ios/compare/v10.18.20...v10.18.21
[v10.18.20]: https://github.com/PicPay/picpay-ios/compare/v10.18.19...v10.18.20
[v10.18.19]: https://github.com/PicPay/picpay-ios/compare/v10.18.18...v10.18.19
[v10.18.18]: https://github.com/PicPay/picpay-ios/compare/v10.18.17...v10.18.18
[v10.18.17]: https://github.com/PicPay/picpay-ios/compare/v10.18.16...v10.18.17
[v10.18.16]: https://github.com/PicPay/picpay-ios/compare/v10.18.15...v10.18.16
[v10.18.15]: https://github.com/PicPay/picpay-ios/compare/v10.18.14...v10.18.15
[v10.18.14]: https://github.com/PicPay/picpay-ios/compare/v10.18.12...v10.18.14
[v10.18.12]: https://github.com/PicPay/picpay-ios/compare/v10.18.11...v10.18.12
[v10.18.11]: https://github.com/PicPay/picpay-ios/compare/v10.18.9...v10.18.11
[v10.18.9]: https://github.com/PicPay/picpay-ios/compare/v10.18.8...v10.18.9
[v10.18.8]: https://github.com/PicPay/picpay-ios/compare/v10.18.10...v10.18.8
[v10.18.10]: https://github.com/PicPay/picpay-ios/compare/v10.18.7...v10.18.10
[v10.18.7]: https://github.com/PicPay/picpay-ios/compare/v10.18.6...v10.18.7
[v10.18.6]: https://github.com/PicPay/picpay-ios/compare/v10.18.5...v10.18.6
[v10.18.5]: https://github.com/PicPay/picpay-ios/compare/v10.18.4...v10.18.5
[v10.18.4]: https://github.com/PicPay/picpay-ios/compare/v10.18.3...v10.18.4
[v10.18.3]: https://github.com/PicPay/picpay-ios/compare/v10.18.2...v10.18.3
[v10.18.2]: https://github.com/PicPay/picpay-ios/compare/V10.18.1...v10.18.2
[V10.18.1]: https://github.com/PicPay/picpay-ios/compare/v10.18.0...V10.18.1
[v10.18.0]: https://github.com/PicPay/picpay-ios/compare/v10.17.5...v10.18.0
[v10.17.5]: https://github.com/PicPay/picpay-ios/compare/v10.17.4...v10.17.5
[v10.17.4]: https://github.com/PicPay/picpay-ios/compare/v10.17.3...v10.17.4
[v10.17.3]: https://github.com/PicPay/picpay-ios/compare/v10.17.2...v10.17.3
[v10.17.2]: https://github.com/PicPay/picpay-ios/compare/v10.17.1...v10.17.2
[v10.17.1]: https://github.com/PicPay/picpay-ios/compare/v10.17.0...v10.17.1
[v10.17.0]: https://github.com/PicPay/picpay-ios/compare/v10.16.5...v10.17.0

[v9.1.61]: https://github.com/PicPay/picpay-ios/compare/v9.1.60...v9.1.16
[v9.1.60]: https://github.com/PicPay/picpay-ios/compare/v9.1.58...v9.1.60
[v9.1.58]: https://github.com/PicPay/picpay-ios/compare/v9.1.54...v9.1.58
[v9.1.54]: https://github.com/PicPay/picpay-ios/compare/v9.1.53...v9.1.54
[v9.1.53]: https://github.com/PicPay/picpay-ios/compare/v9.1.50...v9.1.53
[v9.1.50]: https://github.com/PicPay/picpay-ios/compare/v9.1.45...v9.1.50
[v9.1.45]: https://github.com/PicPay/picpay-ios/compare/v9.1.44...v9.1.44
[v9.1.62]: https://github.com/PicPay/picpay-ios/compare/v9.1.62...v9.1.44
[v9.1.44]: https://github.com/PicPay/picpay-ios/compare/v9.1.63...v9.1.63
[v9.1.64]: https://github.com/PicPay/picpay-ios/compare/v9.1.64...v9.1.63
[v9.1.63]: https://github.com/PicPay/picpay-ios/compare/v9.1.65...v9.1.65
[v9.1.66]: https://github.com/PicPay/picpay-ios/compare/v9.1.66...v9.1.65
[v9.1.65]: https://github.com/PicPay/picpay-ios/compare/v9.1.67...v9.1.67
[v9.1.68]: https://github.com/PicPay/picpay-ios/compare/v9.1.68...v9.1.67
[v9.1.67]: https://github.com/PicPay/picpay-ios/compare/v9.1.75...v9.1.75
[v9.1.76]: https://github.com/PicPay/picpay-ios/compare/v9.1.76...v9.1.75
[v9.1.75]: https://github.com/PicPay/picpay-ios/compare/v9.1.77...v9.1.77
[v9.1.78]: https://github.com/PicPay/picpay-ios/compare/v9.1.78...v9.1.77
[v9.1.77]: https://github.com/PicPay/picpay-ios/compare/v9.1.79...v9.1.79
[v9.1.80]: https://github.com/PicPay/picpay-ios/compare/v9.1.80...v9.1.79
[v9.1.79]: https://github.com/PicPay/picpay-ios/compare/v9.1.81...v9.1.81
[v9.1.83]: https://github.com/PicPay/picpay-ios/compare/v9.1.83...v9.1.81
[v9.1.81]: https://github.com/PicPay/picpay-ios/compare/v9.5.3...v9.5.3
[v9.5.5]: https://github.com/PicPay/picpay-ios/compare/v9.5.5...v9.5.3
[v9.5.11]: https://github.com/PicPay/picpay-ios/compare/v9.5.11...v9.5.5
[v9.5.3]: https://github.com/PicPay/picpay-ios/compare/v9.5.12...v9.5.12
[v9.5.13]: https://github.com/PicPay/picpay-ios/compare/v9.5.13...v9.5.12
[v9.5.12]: https://github.com/PicPay/picpay-ios/compare/v9.5.15...v9.5.15
[v9.5.20]: https://github.com/PicPay/picpay-ios/compare/v9.5.20...v9.5.15
[v9.5.15]: https://github.com/PicPay/picpay-ios/compare/v9.5.22...v9.5.22
[v9.5.23]: https://github.com/PicPay/picpay-ios/compare/v9.5.23...v9.5.22
[v9.5.22]: https://github.com/PicPay/picpay-ios/compare/v9.5.24...v9.5.24
[v9.5.25]: https://github.com/PicPay/picpay-ios/compare/v9.5.25...v9.5.24
[v9.5.24]: https://github.com/PicPay/picpay-ios/compare/v9.5.27...v9.5.27
[v9.5.28]: https://github.com/PicPay/picpay-ios/compare/v9.5.28...v9.5.27
[v9.5.27]: https://github.com/PicPay/picpay-ios/compare/v10.0.4...v10.0.4
[v10.0.5]: https://github.com/PicPay/picpay-ios/compare/v10.0.5...v10.0.4
[v10.0.4]: https://github.com/PicPay/picpay-ios/compare/v10.0.6...v10.0.6
[v10.0.7]: https://github.com/PicPay/picpay-ios/compare/v10.0.7...v10.0.6
[v10.0.6]: https://github.com/PicPay/picpay-ios/compare/v10.0.8...v10.0.8
[v10.0.12]: https://github.com/PicPay/picpay-ios/compare/v10.0.12...v10.0.8
[v10.0.8]: https://github.com/PicPay/picpay-ios/compare/v10.0.13...v10.0.13
[v10.0.14]: https://github.com/PicPay/picpay-ios/compare/v10.0.14...v10.0.13
[v10.0.13]: https://github.com/PicPay/picpay-ios/compare/v10.0.15...v10.0.15
[v10.0.22]: https://github.com/PicPay/picpay-ios/compare/v10.0.22...v10.0.15
[v10.0.15]: https://github.com/PicPay/picpay-ios/compare/v10.0.23...v10.0.23
[v10.0.24]: https://github.com/PicPay/picpay-ios/compare/v10.0.24...v10.0.23
[v10.0.23]: https://github.com/PicPay/picpay-ios/compare/v10.0.26...v10.0.26
[v10.0.27]: https://github.com/PicPay/picpay-ios/compare/v10.0.27...v10.0.26
[v10.0.26]: https://github.com/PicPay/picpay-ios/compare/v10.0.29...v10.0.29
[v10.1.1]: https://github.com/PicPay/picpay-ios/compare/v10.1.1...v10.0.29
[v10.0.29]: https://github.com/PicPay/picpay-ios/compare/v10.1.2...v10.1.2
[v10.1.6]: https://github.com/PicPay/picpay-ios/compare/v10.1.6...v10.1.2
[v10.1.2]: https://github.com/PicPay/picpay-ios/compare/v10.1.7...v10.1.7
[v10.1.11]: https://github.com/PicPay/picpay-ios/compare/v10.1.11...v10.1.7
[v10.1.7]: https://github.com/PicPay/picpay-ios/compare/v10.2.1...v10.2.1
[v10.2.3]: https://github.com/PicPay/picpay-ios/compare/v10.2.3...v10.2.1
[v10.2.1]: https://github.com/PicPay/picpay-ios/compare/v10.2.5...v10.2.5
[v10.2.6]: https://github.com/PicPay/picpay-ios/compare/v10.2.6...v10.2.5
[v10.2.5]: https://github.com/PicPay/picpay-ios/compare/v10.3.3...v10.3.3
[v10.3.4]: https://github.com/PicPay/picpay-ios/compare/v10.3.4...v10.3.3
[v10.3.3]: https://github.com/PicPay/picpay-ios/compare/v10.3.5...v10.3.5
[v10.3.6]: https://github.com/PicPay/picpay-ios/compare/v10.3.6...v10.3.5
[v10.3.5]: https://github.com/PicPay/picpay-ios/compare/v10.3.7...v10.3.7
[v10.4.1]: https://github.com/PicPay/picpay-ios/compare/v10.4.1...v10.3.7
[v10.3.7]: https://github.com/PicPay/picpay-ios/compare/v10.4.6...v10.4.6
[v10.5.1]: https://github.com/PicPay/picpay-ios/compare/v10.5.1...v10.4.6
[v10.4.6]: https://github.com/PicPay/picpay-ios/compare/v10.5.2...v10.5.2
[v10.6.1]: https://github.com/PicPay/picpay-ios/compare/v10.6.1...v10.5.2
[v10.5.2]: https://github.com/PicPay/picpay-ios/compare/v10.6.2...v10.6.2
[v10.7.1]: https://github.com/PicPay/picpay-ios/compare/v10.7.1...v10.6.2
[v10.6.2]: https://github.com/PicPay/picpay-ios/compare/v10.7.2...v10.7.2
[v10.7.3]: https://github.com/PicPay/picpay-ios/compare/v10.7.3...v10.7.2
[v10.7.2]: https://github.com/PicPay/picpay-ios/compare/v10.7.7...v10.7.7
[v10.7.8]: https://github.com/PicPay/picpay-ios/compare/v10.7.8...v10.7.7
[v10.7.7]: https://github.com/PicPay/picpay-ios/compare/v10.7.9...v10.7.9
[v10.7.10]: https://github.com/PicPay/picpay-ios/compare/v10.7.10...v10.7.9
[v10.7.9]: https://github.com/PicPay/picpay-ios/compare/v10.7.14...v10.7.14
[v10.8.1]: https://github.com/PicPay/picpay-ios/compare/v10.8.1...v10.7.14
[v10.7.14]: https://github.com/PicPay/picpay-ios/compare/v10.8.2...v10.8.2
[v10.8.4]: https://github.com/PicPay/picpay-ios/compare/v10.8.4...v10.8.2
[v10.8.2]: https://github.com/PicPay/picpay-ios/compare/v10.8.6...v10.8.6
[v10.8.7]: https://github.com/PicPay/picpay-ios/compare/v10.8.7...v10.8.6
[v10.8.6]: https://github.com/PicPay/picpay-ios/compare/v10.8.9...v10.8.9
[v10.8.10]: https://github.com/PicPay/picpay-ios/compare/v10.8.10...v10.8.9
[v10.8.9]: https://github.com/PicPay/picpay-ios/compare/v10.8.11...v10.8.11
[v10.8.12]: https://github.com/PicPay/picpay-ios/compare/v10.8.12...v10.8.11
[v10.8.11]: https://github.com/PicPay/picpay-ios/compare/v10.8.13...v10.8.13
[v10.8.14]: https://github.com/PicPay/picpay-ios/compare/v10.8.14...v10.8.13
[v10.8.13]: https://github.com/PicPay/picpay-ios/compare/v10.9.1...v10.9.1
[v10.9.2]: https://github.com/PicPay/picpay-ios/compare/v10.9.2...v10.9.1
[v10.9.1]: https://github.com/PicPay/picpay-ios/compare/v10.9.3...v10.9.3
[v10.9.6]: https://github.com/PicPay/picpay-ios/compare/v10.9.6...v10.9.3
[v10.9.3]: https://github.com/PicPay/picpay-ios/compare/v10.9.9...v10.9.9
[v10.9.10]: https://github.com/PicPay/picpay-ios/compare/v10.9.10...v10.9.9
[v10.9.9]: https://github.com/PicPay/picpay-ios/compare/v10.9.11...v10.9.11
[v10.9.15]: https://github.com/PicPay/picpay-ios/compare/v10.9.15...v10.9.11
[v10.9.11]: https://github.com/PicPay/picpay-ios/compare/v10.9.17...v10.9.17
[v10.9.21]: https://github.com/PicPay/picpay-ios/compare/v10.9.21...v10.9.17
[v10.9.17]: https://github.com/PicPay/picpay-ios/compare/v10.9.21...v10.11.2
[v10.9.17]: https://github.com/PicPay/picpay-ios/compare/v10.11.2...v10.12.2
[v10.12.3]: https://github.com/PicPay/picpay-ios/compare/v10.12.2...v10.12.3
[v10.12.4]: https://github.com/PicPay/picpay-ios/compare/v10.12.3...v10.12.4
[v10.12.4]: https://github.com/PicPay/picpay-ios/compare/v10.12.4...v10.12.12
[v10.12.12]: https://github.com/PicPay/picpay-ios/compare/v10.12.4...v10.12.13
[v10.12.12]: https://github.com/PicPay/picpay-ios/compare/v10.12.13...v10.12.14
[v10.12.12]: https://github.com/PicPay/picpay-ios/compare/v10.12.14...v10.12.15
[v10.12.12]: https://github.com/PicPay/picpay-ios/compare/v10.12.15...v10.12.16
[v10.12.12]: https://github.com/PicPay/picpay-ios/compare/v10.12.16...v10.12.17
[v10.12.12]: https://github.com/PicPay/picpay-ios/compare/v10.12.17...v10.12.19
[v10.12.12]: https://github.com/PicPay/picpay-ios/compare/v10.12.19...v10.12.20
[v10.12.12]: https://github.com/PicPay/picpay-ios/compare/v10.12.20...v10.12.22
[v10.12.12]: https://github.com/PicPay/picpay-ios/compare/v10.12.22...v10.12.23
[v10.12.12]: https://github.com/PicPay/picpay-ios/compare/v10.12.23...v10.12.25
[v10.12.12]: https://github.com/PicPay/picpay-ios/compare/v10.12.25...v10.12.26
[v10.12.12]: https://github.com/PicPay/picpay-ios/compare/v10.12.26...v10.12.27
[v10.12.28]: https://github.com/PicPay/picpay-ios/compare/v10.12.27...v10.12.28
[v10.12.29]: https://github.com/PicPay/picpay-ios/compare/v10.12.28...v10.12.29
[v10.12.30]: https://github.com/PicPay/picpay-ios/compare/v10.12.29...v10.12.30
[v10.12.35]: https://github.com/PicPay/picpay-ios/compare/v10.12.30...v10.12.35
[v10.12.35]: https://github.com/PicPay/picpay-ios/compare/v10.12.35...v10.12.35
[v10.12.35]: https://github.com/PicPay/picpay-ios/compare/v10.12.35...v10.12.35
[v10.12.36]: https://github.com/PicPay/picpay-ios/compare/v10.12.35...v10.12.36
[v10.12.37]: https://github.com/PicPay/picpay-ios/compare/v10.12.36...v10.12.37
[v10.12.38]: https://github.com/PicPay/picpay-ios/compare/v10.12.37...v10.12.38
[v10.13.1]: https://github.com/PicPay/picpay-ios/compare/v10.12.38...v10.13.1
[v10.13.3]: https://github.com/PicPay/picpay-ios/compare/v10.13.1...v10.13.3
[v10.13.4]: https://github.com/PicPay/picpay-ios/compare/v10.13.3...v10.13.4
[v10.13.5]: https://github.com/PicPay/picpay-ios/compare/v10.13.4...v10.13.5
