import UIKit
import XCTest

func AssertImageEqual(
    _ expression1: @autoclosure () throws -> UIImage,
    _ expression2: @autoclosure () throws -> UIImage,
    file: StaticString = #file,
    line: UInt = #line
) {
    do {
        let image1 = try expression1()
        let image2 = try expression2()

        let imageData1 = try XCTUnwrap(image1.pngData())
        let imageData2 = try XCTUnwrap(image2.pngData())

        let imageFromData1 = try XCTUnwrap(UIImage(data: imageData1))
        let imageFromData2 = try XCTUnwrap(UIImage(data: imageData2))

        XCTAssertEqual(
            imageFromData1.pngData(),
            imageFromData2.pngData(),
            failMessage(image1, image2),
            file: file,
            line: line
        )
    } catch {
        XCTFail(error.localizedDescription, file: file, line: line)
    }
}

func AssertImageEqual(
    _ expression1: @autoclosure () throws -> UIImage?,
    _ expression2: @autoclosure () throws -> UIImage?,
    file: StaticString = #file,
    line: UInt = #line
)  {
    do {
        guard let image1 = try expression1(), let image2 = try expression2() else {
            return fail(try expression1(), try expression2(), file: file, line: line)
        }

        AssertImageEqual(image1, image2, file: file, line: line)
    }
    catch {
        XCTFail(error.localizedDescription, file: file, line: line)
    }
}

func AssertImageEqual(
    _ expression1: @autoclosure () throws -> UIImage?,
    _ expression2: @autoclosure () throws -> UIImage,
    file: StaticString = #file,
    line: UInt = #line
) {
    do {
        guard let image1 = try expression1() else {
            return fail(try expression1(), try expression2(), file: file, line: line)
        }

        AssertImageEqual(image1, try expression2(), file: file, line: line)
    }
    catch {
        XCTFail(error.localizedDescription, file: file, line: line)
    }
}

func AssertImageEqual(
    _ expression1: @autoclosure () throws -> UIImage,
    _ expression2: @autoclosure () throws -> UIImage?,
    file: StaticString = #file,
    line: UInt = #line
) {
    do {
        guard let image2 = try expression2() else {
            return fail(try expression1(), try expression2(), file: file, line: line)
        }

        AssertImageEqual(try expression1(), image2, file: file, line: line)
    }
    catch {
        XCTFail(error.localizedDescription, file: file, line: line)
    }
}

private func failMessage<T, U>(_ image1: T, _ image2: U) -> String {
    "Image \(String(describing: image1)) is not equals to \(String(describing: image2))"
}

private func fail<T, U>(
    _ image1: T,
    _ image2: U,
    file: StaticString = #file,
    line: UInt = #line
) {
    XCTFail(failMessage(image1, image2), file: file, line: line)
}
