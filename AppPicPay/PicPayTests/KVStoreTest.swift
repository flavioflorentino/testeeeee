import XCTest
import Core
@testable import PicPay

class KVStoreTest: XCTestCase {
    var storage = KVStore()
    
    let keys: [KVKey] = [.use_touch_id,
                         .touch_id_asked,
                         .isTouchIdKeychainBug,
                         .needUpdateBiometricAuthPin,
                         .identifyContactsCache,
                         .localContactsHash,
                         .defaultCreditCardId,
                         .userToken,
                         .clearCache,
                         .isPhoneVerified,
                         .authInfoUpdated,
                         .getAllConsumerData,
                         .onboardSocial,
                         .showCreditPaymentMethodIntro,
                         .lastBoleto,
                         .DGRecents,
                         .tvRecharges,
                         .cardsStatus,
                         .cashbackPopup,
                         .userName,
                         .email,
                         .email_confirmed,
                         .email_pending,
                         .verifiedNumber,
                         .notFirstTimeOnApp,
                         .hasAtLeastOneFeedOnce,
                         .phoneVerificationCallDelay,
                         .isEnabledPhoneVerificationCall,
                         .phoneVerificationSMSDelay,
                         .phoneVerificationSentDate,
                         .phoneVerificationDDD,
                         .phoneVerificationNumber,
                         .isRegisterUsernameStep,
                         .askedForRegistrationRemoteNotification,
                         .installation_id,
                         .privacyCconfig,
                         .privacyConfigSuggestionPromptCount,
                         .lastChosenPrivacyConfig,
                         .useBalance,
                         .useBalanceAlreadySetted,
                         .installmentsPopupShow,
                         .developmentApiHost,
                         .installmentPopupShowedPAV,
                         .firebase_dynamic_link_deep_link,
                         .firebase_dynamic_link_referral_code,
                         .locationAlwaysWithWhenInUse,
                         .hasPresentedCreditPicpayOfferAlert,
                         .listOfferCardsWalletDismissed,
                         .locationCoordinates,
                         .hasPresentedBetaAlert]
    
    override func setUp() {
        storage = KVStore(storage: UserDefaults.init(suiteName: "TestSuite")!)
    }
    
    override func tearDown() {
        UserDefaults.standard.removePersistentDomain(forName: "TestSuite")
    }
    
    func testGetAndSaveBool() {
        keys.forEach { key in
            storage.setBool(true, with: key)
        }
        keys.forEach { key in
            XCTAssertTrue(storage.boolFor(key))
        }
    }
    
    func testGetAndSaveString() {
        keys.forEach { key in
            storage.setString("StringMonstra", with: key)
        }
        keys.forEach { key in
            XCTAssertEqual(storage.stringFor(key), "StringMonstra")
        }
    }
    
    func testGetAndSaveInt() {
        keys.forEach { key in
            storage.setInt(1, with: key)
        }
        keys.forEach { key in
            XCTAssertEqual(storage.intFor(key), 1)
        }
    }
    
    func testGetAndSaveArray() {
        let array = ["hi", "hello"]
        keys.forEach { key in
            storage.set(value: array, with: key)
        }
        keys.forEach { key in
            let result = storage.arrayFor(key)
            let stringArray = result as! [String]
            XCTAssertNotNil(result)
            XCTAssertEqual(result!.count, 2)
            XCTAssertEqual(stringArray[0], "hi")
            XCTAssertEqual(stringArray[1], "hello")
        }
    }
    
    func testGetAndSaveDate() {
        let date = Date()
        keys.forEach { key in
            storage.setDate(date, with: key)
        }
        keys.forEach { key in
            guard let result = storage.objectForKey(key) as? Date else {
                XCTFail()
                return
            }
            XCTAssertEqual(result, date)
        }
    }
    
    func testGetAndSaveDict() {
        let dict: [String: Any] = ["number": 1, "string": "Steve Jobs"]
        keys.forEach { key in
            storage.set(value: dict, with: key)
        }
        keys.forEach { key in
            let result = storage.dictionaryFor(key)
            XCTAssertNotNil(result)
            XCTAssertEqual(result!.count, 2)
            XCTAssertEqual(result!["number"] as! Int, 1)
            XCTAssertEqual(result!["string"] as! String, "Steve Jobs")
        }
    }
    
    func testRemoveObject() {
        let array = ["hi", "hello"]
        keys.forEach { key in
            storage.set(value: array, with: key)
        }
        keys.forEach { key in
            storage.removeObjectFor(key)
            XCTAssertNil(storage.objectForKey(key))
        }
    }
    
    func testWhenTheKeyWasNotSavedAndTheDefaultIsFalse() {
        keys.forEach { key in
            XCTAssertFalse(storage.boolFor(key))
        }
    }
    
    func testWhenTheKeyWasNotSavedAndTheDefaultIsZero() {
        keys.forEach { key in
            XCTAssertEqual(0, storage.intFor(key))
        }
    }
    
    func testWhenFirstTimeKeyWasNotSavedAndTheDefaultIsFalse() {
        let event = "Event"
        XCTAssertFalse(storage.getFirstTimeOnlyEvent(event))
    }
    
    func testWhenFirstTimeKeyWasIsTrue() {
        let event = "Event"
        storage.setFirstTimeOnlyEvent(event)
        XCTAssertTrue(storage.getFirstTimeOnlyEvent(event))
    }
    
    func testSavingNSKeydArchivedObject() {
        let recharges = [DGRechargeRecent(ddd: "21", phoneNumber: "123456789", carrier: "Apple"), DGRechargeRecent(ddd: "11", phoneNumber: "987654321", carrier: "Google")]
        
        let encodedData = try! NSKeyedArchiver.archivedData(withRootObject: recharges, requiringSecureCoding: false)
        
        XCTAssertNil(storage.objectForKey(.DGRecents))
        storage.setData(encodedData, with: .DGRecents)
        
        let result = storage.objectForKey(.DGRecents)
        XCTAssertNotNil(result)
        
        guard let data = result as? Data else {
            XCTFail("Was expecting Data")
            return
        }
        guard let rechargesRecent = NSKeyedUnarchiver.unarchiveObject(with: data) as? [DGRechargeRecent] else {
            XCTFail("Was expecting [DGRechargeRec")
            return
        }
        
        XCTAssertNotNil(rechargesRecent.first(where: { $0.phoneNumber == "987654321" }))
        XCTAssertNotNil(rechargesRecent.first(where: { $0.phoneNumber == "123456789" }))
    }
}
