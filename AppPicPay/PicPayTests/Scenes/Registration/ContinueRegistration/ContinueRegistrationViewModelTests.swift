@testable import PicPay
import XCTest

final class ContinueRegistrationViewModelTests: XCTestCase {
    private let presenterSpy = ContinueRegistrationPresenterSpy()
    
    private lazy var sut: ContinueRegistrationViewModel = {
        let viewModel = ContinueRegistrationViewModel(presenter: presenterSpy)
        return viewModel
    }()
    
    func testSetupViewContent_WhenNameIsEmpty_ShouldDisplayTitle() {
        sut.setupViewContent()
        
        XCTAssertEqual(presenterSpy.callDisplayTitleCount, 1)
    }
    
    func testSetupViewContent_WhenNameIsNotEmpty_ShouldDisplayTitleWithName() {
        sut = ContinueRegistrationViewModel(presenter: presenterSpy, name: "Name")
        
        sut.setupViewContent()
        
        XCTAssertEqual(presenterSpy.callDisplayTitleWithNameCount, 1)
        XCTAssertEqual(presenterSpy.callDisplayTitleWithNamePassed, "Name")
    }
    
    func testDidContinueRegistration_ShouldPresenterContinueRegistration() {
        sut.didContinueRegistration()
        
        XCTAssertEqual(presenterSpy.callContinueRegistrationCount, 1)
    }
    
    func testDidStartNewRegistration_ShouldPresenterStartNewRegistration() {
        sut.didStartNewRegistration()
        
        XCTAssertEqual(presenterSpy.callStartNewRegistrationCount, 1)
    }
}

private final class ContinueRegistrationPresenterSpy: ContinueRegistrationPresenting {
    var viewController: ContinueRegistrationDisplay?
    
    private(set) var callDisplayTitleCount = 0
    private(set) var callDisplayTitleWithNameCount = 0
    private(set) var callDisplayTitleWithNamePassed: String?
    private(set) var callContinueRegistrationCount = 0
    private(set) var callStartNewRegistrationCount = 0
    
    func displayTitle() {
        callDisplayTitleCount += 1
    }
    
    func displayTitle(withName name: String) {
        callDisplayTitleWithNameCount += 1
        callDisplayTitleWithNamePassed = name
    }
    
    func didNextStep(action: ContinueRegistrationAction) {
        switch action {
        case .continueRegistration:
            callContinueRegistrationCount += 1
        case .startNewRegistration:
            callStartNewRegistrationCount += 1
        }
    }
}
