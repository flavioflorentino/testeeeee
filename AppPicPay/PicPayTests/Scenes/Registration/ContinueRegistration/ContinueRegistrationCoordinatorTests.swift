@testable import PicPay
import XCTest

final class ContinueRegistrationCoordinatorTests: XCTestCase {
    private let navController = CustomPopNavigationControllerMock(rootViewController: UIViewController())
    private let stepsMock: [RegistrationStep] = [.name, .phone, .email, .password, .document]
    private lazy var cachedRegistrationMock = RegistrationCache(
        model: RegistrationModel(),
        statistics: RegistrationStatisticsModel(),
        stepSequence: stepsMock,
        interruptedStep: .email
    )
    private lazy var sut: ContinueRegistrationCoordinator = {
        let coordinator = ContinueRegistrationCoordinator(cache: cachedRegistrationMock)
        coordinator.viewController = navController.topViewController
        return coordinator
    }()
    
    func testPerform_WhenActionStartNewRegistration_ShouldPresentNewRegistration() {
        sut.perform(action: .startNewRegistration)
        
        XCTAssert(navController.isSetViewControllersCalled)
        XCTAssertEqual(navController.viewControllers.count, 2)
        XCTAssert(navController.viewControllers.first is SignUpViewController)
        let topViewController = navController.topViewController
        XCTAssert(topViewController is RegistrationNameViewController
            || topViewController is RegistrationPhoneNumberViewController
            || topViewController is RegistrationEmailViewController
            || topViewController is RegistrationPasswordViewController
            || topViewController is RegistrationDocumentViewController
        )
    }
    
    func testPerform_WhenActionStartContinueRegistation_ShouldContinueSavedRegistration() {
        sut.perform(action: .continueRegistration)
        
        XCTAssert(navController.isSetViewControllersCalled)
        XCTAssertEqual(navController.viewControllers.count, 4)
        XCTAssert(navController.viewControllers.first is SignUpViewController)
        XCTAssert(navController.viewControllers[1] is RegistrationNameViewController)
        XCTAssert(navController.viewControllers[2] is RegistrationPhoneNumberViewController)
        XCTAssert(navController.topViewController is RegistrationEmailViewController)
    }
}
