@testable import PicPay
import XCTest

final class ContinueRegistrationPresenterTests: XCTestCase {
    private let coordinatorSpy = ContinueRegistrationCoordinatorSpy()
    private let viewControllerSpy = ContinueRegistrationViewControllerSpy()
    
    private lazy var sut: ContinueRegistrationPresenter = {
        let presenter = ContinueRegistrationPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDisplayTitle_ShouldDisplayDefaultTitle() {
        sut.displayTitle()
        
        XCTAssertEqual(viewControllerSpy.callDisplayTitleCount, 1)
        XCTAssertEqual(viewControllerSpy.callDisplayTitlePassed, RegisterLocalizable.continueRegistrationTitle.text)
    }
    
    func testDisplayNameWithName_ShouldDisplayTitleWithName() {
        sut.displayTitle(withName: "Name")
        
        XCTAssertEqual(viewControllerSpy.callDisplayTitleCount, 1)
        XCTAssertEqual(viewControllerSpy.callDisplayTitlePassed, String(format: RegisterLocalizable.continueRegistrationTitleWithName.text, "Name"))
    }
    
    func testDidNextStep_WhenActionIsContinueRegistration_ShouldPresenterContinheRegistration() {
        sut.didNextStep(action: .continueRegistration)
        
        XCTAssertEqual(coordinatorSpy.callContinueRegistrationCount, 1)
    }
    
    func testDidNextStep_WhenActionIsStartNewRegistration_ShouldPresenterStartNewRegistration() {
        sut.didNextStep(action: .startNewRegistration)
        
        XCTAssertEqual(coordinatorSpy.callStartNewRegistrationCount, 1)
    }
}

private final class ContinueRegistrationViewControllerSpy: ContinueRegistrationDisplay {
    private(set) var callDisplayTitleCount = 0
    private(set) var callDisplayTitlePassed: String?
    
    func display(title: String) {
        callDisplayTitleCount += 1
        callDisplayTitlePassed = title
    }
}

private final class ContinueRegistrationCoordinatorSpy: ContinueRegistrationCoordinating {
    var viewController: UIViewController?
    private(set) var callContinueRegistrationCount = 0
    private(set) var callStartNewRegistrationCount = 0
    
    func perform(action: ContinueRegistrationAction) {
        switch action {
        case .continueRegistration:
            callContinueRegistrationCount += 1
        case .startNewRegistration:
            callStartNewRegistrationCount += 1
        }
    }
}
