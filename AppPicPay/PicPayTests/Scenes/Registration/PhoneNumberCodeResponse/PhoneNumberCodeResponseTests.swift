import XCTest
@testable import PicPay

final class PhoneNumberCodeResponseTests: XCTestCase {
    
    func testModelParse_WhenTypeIsSMS_ShouldParseSuccess() throws {
        let json = MockJSON().load(resource: "PhoneNumberCodeResponse_SMS")
        let jsonDecoder = JSONDecoder(.convertFromSnakeCase)
        let response = try jsonDecoder.decode(RegisterResponse<PhoneNumberCodeResponse>.self, from: json.rawData())
        let phoneNumberCodeResponse = response.data
        
        XCTAssertEqual(phoneNumberCodeResponse?.callDelay, 15)
        XCTAssertEqual(phoneNumberCodeResponse?.smsDelay, 15)
        XCTAssertEqual(phoneNumberCodeResponse?.callEnabled, true)
        XCTAssertNil(phoneNumberCodeResponse?.whatsAppDelay)
    }
    
    func testModelParse_WhenTypeIsWhatsApp_ShouldParseSuccess() throws {
        let json = MockJSON().load(resource: "PhoneNumberCodeResponse_WhatsApp")
        let jsonDecoder = JSONDecoder(.convertFromSnakeCase)
        let response = try jsonDecoder.decode(RegisterResponse<PhoneNumberCodeResponse>.self, from: json.rawData())
        let phoneNumberCodeResponse = response.data
        
        XCTAssertEqual(phoneNumberCodeResponse?.callDelay, 15)
        XCTAssertEqual(phoneNumberCodeResponse?.whatsAppDelay, 15)
        XCTAssertEqual(phoneNumberCodeResponse?.callEnabled, true)
        XCTAssertNil(phoneNumberCodeResponse?.smsDelay)
    }
}
