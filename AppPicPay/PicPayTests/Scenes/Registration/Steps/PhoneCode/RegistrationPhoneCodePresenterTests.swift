@testable import PicPay
import XCTest

final class RegistrationPhoneCodePresenterTests: XCTestCase {
    private let viewControllerSpy = RegistrationPhoneCodeDisplayable()
    private let coordinatorSpy = RegistrationPhoneCodeCoordinatorSpy()
    
    lazy var sut: RegistrationPhoneCodePresenter = {
        let presenter = RegistrationPhoneCodePresenter(coordinator: coordinatorSpy, dependencies: DependencyContainerMock())
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDisplayDescription_WhenCalledFromViewModel_ShouldDisplayDescription() {
        sut.displayDescription()
        
        XCTAssertEqual(viewControllerSpy.displayDescriptionCount, 1)
        XCTAssertEqual(viewControllerSpy.displayDescriptionPassed, NSAttributedString(string: RegisterLocalizable.codeStepDefaultTextLabel.text))
    }
    
    func testDisplayDescriptionWithDDDAndNumber_WhenCalledFromViewModel_ShouldDisplayDescription() {
        let description = String(format: RegisterLocalizable.codeStepPhoneTextLabel.text, "<b>(11) 12345-1234</b>")
        let attributedText = NSAttributedString(string: description).boldfyWithSystemFont(ofSize: 14)
        sut.displayDescription(ddd: "11", number: "12345-1234")
        
        XCTAssertEqual(viewControllerSpy.displayDescriptionCount, 1)
        XCTAssertEqual(viewControllerSpy.displayDescriptionPassed, attributedText)
    }
    
    func testDisplayCallButton_WhenCalledFromViewModel_ShouldDisplayCallButton() {
        sut.displayCallButton()
        
        XCTAssertEqual(viewControllerSpy.displayCallButtonCount, 1)
    }

    func testDisplayConfirmResendCodeAlert_ShouldDisplayConfirmResendCodeAlert() {
        sut.displayConfirmResendCodeAlert(ddd: "11", number: "12345-1234")
        
        XCTAssertEqual(viewControllerSpy.displayConfirmResendCodeAlertCount, 1)
        XCTAssertEqual(
            viewControllerSpy.displayConfirmResendCodeAlertPassed?.title,
            NSAttributedString(string: RegisterLocalizable.alertPhoneConfirmTitle.text)
        )
        XCTAssertEqual(
            viewControllerSpy.displayConfirmResendCodeAlertPassed?.text,
            NSAttributedString(string: String(format: RegisterLocalizable.alertPhoneConfirmMessage.text, "11", "12345-1234"))
        )
        XCTAssertEqual(viewControllerSpy.displayConfirmResendCodeAlertPassed?.buttons.count, 2)
        XCTAssertEqual(viewControllerSpy.displayConfirmResendCodeAlertPassed?.buttons.first?.title, DefaultLocalizable.btOk.text)
        XCTAssertEqual(viewControllerSpy.displayConfirmResendCodeAlertPassed?.buttons.last?.title, DefaultLocalizable.btEdit.text)
    }
    
    func testDisplayConfirmReceiveCallAlert_ShouldDisplayConfirmReceiveCallAlert() {
        sut.displayConfirmReceiveCallAlert(ddd: "11", number: "12345-1234")
        
        XCTAssertEqual(viewControllerSpy.displayConfirmReceiveCallAlertCount, 1)
        XCTAssertEqual(
            viewControllerSpy.displayConfirmReceiveCallAlertPassed?.title,
            NSAttributedString(string: RegisterLocalizable.alertPhoneConfirmTitle.text)
        )
        XCTAssertEqual(
            viewControllerSpy.displayConfirmReceiveCallAlertPassed?.text,
            NSAttributedString(string: String(format: RegisterLocalizable.alertPhoneConfirmMessage.text, "11", "12345-1234"))
        )
        XCTAssertEqual(viewControllerSpy.displayConfirmReceiveCallAlertPassed?.buttons.count, 2)
        XCTAssertEqual(viewControllerSpy.displayConfirmReceiveCallAlertPassed?.buttons.first?.title, DefaultLocalizable.btOk.text)
        XCTAssertEqual(viewControllerSpy.displayConfirmReceiveCallAlertPassed?.buttons.last?.title, DefaultLocalizable.btEdit.text)
    }
    
    func testDisplayApertError_WhenCalledFromViewModel_ShouldDisplayAlertWithError() {
        sut.displayAlertError()
        
        XCTAssertEqual(viewControllerSpy.displayAlertErrorCount, 1)
        XCTAssertEqual(viewControllerSpy.displayAlertErrorPassed?.title, NSAttributedString(string: DefaultLocalizable.attention.text))
        XCTAssertEqual(viewControllerSpy.displayAlertErrorPassed?.text, NSAttributedString(string: RegisterLocalizable.wrongInformations.text))
        XCTAssertEqual(viewControllerSpy.displayAlertErrorPassed?.buttons.count, 1)
        XCTAssertEqual(viewControllerSpy.displayAlertErrorPassed?.buttons.first?.title, DefaultLocalizable.btOk.text)
    }
    
    func testCodeButtonIsActive_WhenParametersIsTrue_ShouldEnableCodeButton() {
        sut.codeButtonIsActive(true)
        
        XCTAssertEqual(viewControllerSpy.codeButtonIsActiveCount, 1)
        XCTAssert(viewControllerSpy.codeButtonIsActivePassed == true)
    }
    
    func testCodeButtonIsActive_WhenParamerersIsFalse_ShouldDisableCodeButton() {
        sut.codeButtonIsActive(false)
        
        XCTAssertEqual(viewControllerSpy.codeButtonIsActiveCount, 1)
        XCTAssert(viewControllerSpy.codeButtonIsActivePassed == false)
    }
    
    func testCallButtonIsActive_WhenParameterIsTrue_ShouldEnableCallButton() {
        sut.callButtonIsActive(true)
        
        XCTAssertEqual(viewControllerSpy.callButtonIsActiveCount, 1)
        XCTAssert(viewControllerSpy.callButtonIsActivePassed == true)
    }
    
    func testCallButtonIsActive_WhenParametersISTrue_ShouldDisableCallButton() {
        sut.callButtonIsActive(false)
        
        XCTAssertEqual(viewControllerSpy.callButtonIsActiveCount, 1)
        XCTAssert(viewControllerSpy.callButtonIsActivePassed == false)
    }
    
    func testUpdateCodeButtonTitle_WhenCalledFromViewModel_ShouldUpdateCodeButtonTitle() {
        sut.updateCodeButtonTitle()
        
        XCTAssertEqual(viewControllerSpy.updateCodeButtonTitleCount, 1)
        XCTAssertEqual(viewControllerSpy.updateCodeButtonTitlePassed, RegisterLocalizable.resendCode.text)
    }
    
    func testUpdateCodeButtonTitle_WhenIntervalGreaterThanZero_ShouldUpdateCodeButtonTitleWithInterval() {
        sut.updateCodeButtonTitle(interval: 10)
        
        XCTAssertEqual(viewControllerSpy.updateCodeButtonTitleCount, 1)
        XCTAssertEqual(viewControllerSpy.updateCodeButtonTitlePassed, String(format: RegisterLocalizable.resendCodedelay.text, "00:10"))
    }
    
    func testUpdateCodeButtonTitle_WhenIntervalIsEqualZero_ShouldUpdateCodeButtonTitleWithDefaultTitle() {
        sut.updateCodeButtonTitle(interval: 0)
        
        XCTAssertEqual(viewControllerSpy.updateCodeButtonTitleCount, 1)
        XCTAssertEqual(viewControllerSpy.updateCodeButtonTitlePassed, RegisterLocalizable.resendCode.text)
    }
    
    func testUpdateCallButtonTitle_WhenCalledFromViewModel_ShouldUpdateCodeButtonTitle() {
        sut.updateCallButtonTitle()
        
        XCTAssertEqual(viewControllerSpy.updateCallButtonTitleCount, 1)
        XCTAssertEqual(viewControllerSpy.updateCallButtonTitlePassed, RegisterLocalizable.callMe.text)
    }
    
    func testUpdateCallButtonTitle_WhenIntervalGreaterThanZero_ShouldUpdateCallButtonTitleWithInterval() {
        sut.updateCallButtonTitle(interval: 10)
        
        XCTAssertEqual(viewControllerSpy.updateCallButtonTitleCount, 1)
        XCTAssertEqual(viewControllerSpy.updateCallButtonTitlePassed,  String(format: RegisterLocalizable.callMeDelay.text, "00:10"))
    }
    
    func testUpdateCallButtonTitle_WhenIntervalIsEqualZero_ShouldUpdateCallButtonTitleWithDefaultTitle() {
        sut.updateCallButtonTitle(interval: 0)
        
        XCTAssertEqual(viewControllerSpy.updateCallButtonTitleCount, 1)
        XCTAssertEqual(viewControllerSpy.updateCallButtonTitlePassed,  RegisterLocalizable.callMe.text)
    }
    
    func testDisplayError_WhenCalledFromViewModel_ShouldDisplayErrorWithMessage() {
        sut.displayError(message: "Error")
        
        XCTAssertEqual(viewControllerSpy.displayErrorCount, 1)
        XCTAssertEqual(viewControllerSpy.displayErrorPassed, "Error")
    }
    
    func testHideError_WhenCalledFromViewModel_ShouldHideError() {
        sut.hideError()
        
        XCTAssertEqual(viewControllerSpy.hideErrorCount, 1)
    }
    
    func testShowLoading_WhenCalledFromViewModel_ShouldShowLoading() {
        sut.showLoading()
        
        XCTAssertEqual(viewControllerSpy.showLoadingCount, 1)
    }
    
    func testHideLoading_WhenCalledFromViewModel_ShouldHideLoading() {
        sut.hideLoading()
        
        XCTAssertEqual(viewControllerSpy.hideLoadingCount, 1)
    }
    
    func testEnableControls_WhenCalledFromViewModel_ShouldEnableControls() {
        sut.enableControls()
        
        XCTAssertEqual(viewControllerSpy.enableControlsCount, 1)
    }
    
    func testDisableControls_WhenCalledFromViewModel_ShouldDisableControls() {
        sut.disableControls()
        
        XCTAssertEqual(viewControllerSpy.disableControlsCount, 1)
    }
    
    func testDidNextStep_WhenActionIsAlreadyRegistered_ShouldPassAlreadyRegisteredActionToCoordinator() {
        sut.didNextStep(action: .alreadyRegistered)
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertTrue(coordinatorSpy.alreadyRegisteredCalled)
    }
    
    func testDidNextStep_WhenActionIsOpenHelp_ShouldPassOpenHelpActionToCoordinator() {
        sut.didNextStep(action: .openHelp(with: "help"))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertTrue(coordinatorSpy.openHelpCalled)
        XCTAssertEqual(coordinatorSpy.openHelpPassed, "help")
    }
    
    func testDidNextStep_WhenActionIsMoveToNextStep_ShouldPassMoveToNextStepActionToCoordinator() {
        sut.didNextStep(action: .moveToNextStep(with: "11", number: "12345-1234"))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertTrue(coordinatorSpy.moveToNextStepCalled)
        XCTAssertEqual(coordinatorSpy.moveToNextStepPassed?.ddd, "11")
        XCTAssertEqual(coordinatorSpy.moveToNextStepPassed?.number, "12345-1234")
    }
    
    func testDidNextStep_WhenActionIsEditPhoneNumber_ShouldPassEditPhoneNumberActionToCoordinator() {
        sut.didNextStep(action: .editPhoneNumber)
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertTrue(coordinatorSpy.editPhoneNumberCalled)
    }
    
    func testDidNextStep_WhenActionisBack_ShouldBackView() {
        sut.didNextStep(action: .back)
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertTrue(coordinatorSpy.backCalled)
    }
}

private final class RegistrationPhoneCodeDisplayable: RegistrationPhoneCodeDisplay {
    private(set) var displayDescriptionCount = 0
    private(set) var displayDescriptionPassed: NSAttributedString?
    private(set) var displayCallButtonCount = 0
    private(set) var displayConfirmResendCodeAlertCount = 0
    private(set) var displayConfirmResendCodeAlertPassed: Alert?
    private(set) var displayConfirmReceiveCallAlertCount = 0
    private(set) var displayConfirmReceiveCallAlertPassed: Alert?
    private(set) var displayAlertErrorCount = 0
    private(set) var displayAlertErrorPassed: Alert?
    private(set) var codeButtonIsActiveCount = 0
    private(set) var codeButtonIsActivePassed: Bool?
    private(set) var callButtonIsActiveCount = 0
    private(set) var callButtonIsActivePassed: Bool?
    private(set) var updateCodeButtonTitleCount = 0
    private(set) var updateCodeButtonTitlePassed: String?
    private(set) var updateCallButtonTitleCount = 0
    private(set) var updateCallButtonTitlePassed: String?
    private(set) var displayErrorCount = 0
    private(set) var displayErrorPassed: String?
    private(set) var hideErrorCount = 0
    private(set) var showLoadingCount = 0
    private(set) var hideLoadingCount = 0
    private(set) var enableControlsCount = 0
    private(set) var disableControlsCount = 0
    
    func displayDescription(_ description: NSAttributedString) {
        displayDescriptionCount += 1
        displayDescriptionPassed = description
    }
    
    func displayCallButton() {
        displayCallButtonCount += 1
    }
    
    func displayConfirmResendCodeAlert(_ alert: Alert) {
        displayConfirmResendCodeAlertCount += 1
        displayConfirmResendCodeAlertPassed = alert
    }
    
    func displayConfirmReceiveCallAlert(_ alert: Alert) {
        displayConfirmReceiveCallAlertCount += 1
        displayConfirmReceiveCallAlertPassed = alert
    }
    
    func displayAlertError(_ alert: Alert) {
        displayAlertErrorCount += 1
        displayAlertErrorPassed = alert
    }
    
    func codeButtonIsActive(_ active: Bool) {
        codeButtonIsActiveCount += 1
        codeButtonIsActivePassed = active
    }
    
    func callButtonIsActive(_ active: Bool) {
        callButtonIsActiveCount += 1
        callButtonIsActivePassed = active
    }
    
    func updateCodeButtonTitle(_ title: String) {
        updateCodeButtonTitleCount += 1
        updateCodeButtonTitlePassed = title
    }
    
    func updateCallButtonTitle(_ title: String) {
        updateCallButtonTitleCount += 1
        updateCallButtonTitlePassed = title
    }
    
    func displayError(message: String) {
        displayErrorCount += 1
        displayErrorPassed = message
    }
    
    func hideError() {
        hideErrorCount += 1
    }
    
    func showLoading() {
        showLoadingCount += 1
    }
    
    func hideLoading() {
        hideLoadingCount += 1
    }
    
    func enableControls() {
        enableControlsCount += 1
    }
    
    func disableControls() {
        disableControlsCount += 1
    }
}

private final class RegistrationPhoneCodeCoordinatorSpy: RegistrationPhoneCodeCoordinating {
    var delegate: RegistrationPhoneCodeCoordinatorDelegate?
    var viewController: UIViewController?
    private(set) var callPerformActionCount = 0
    private(set) var alreadyRegisteredCalled = false
    private(set) var openHelpCalled = false
    private(set) var openHelpPassed: String?
    private(set) var moveToNextStepCalled = false
    private(set) var moveToNextStepPassed: (ddd: String, number: String)?
    private(set) var editPhoneNumberCalled = false
    private(set) var backCalled = false
    
    func perform(action: RegistrationPhoneCodeAction) {
        callPerformActionCount += 1
        
        switch action {
        case .back:
            backCalled = true
        case .alreadyRegistered:
            alreadyRegisteredCalled = true
        case let .openHelp(path):
            openHelpCalled = true
            openHelpPassed = path
        case let .moveToNextStep(ddd, number):
            moveToNextStepCalled = true
            moveToNextStepPassed = (ddd: ddd, number: number)
        case .editPhoneNumber:
            editPhoneNumberCalled = true
        }
        
    }
}
