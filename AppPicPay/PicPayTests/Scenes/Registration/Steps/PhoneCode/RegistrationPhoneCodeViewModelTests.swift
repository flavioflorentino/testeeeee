@testable import PicPay
import Core
import XCTest

final class RegistrationPhoneCodeViewModelTests: XCTestCase {
    private let serviceMock = RegistrationPhoneCodeServiceMock()
    private let presenterSpy = RegistrationPhoneCodePresenterMock()
    private lazy var sut = RegistrationPhoneCodeViewModel(
        service: serviceMock,
        presenter: presenterSpy,
        ddd: "11",
        number: "12345-1234",
        codeType: .sms
    )
    
    func testLoadDescription_WhenDDDAndNumberIsValid_ShouldDisplayDescriptioWithPhoneNumber() {
        sut.loadDescription()
        
        XCTAssertEqual(presenterSpy.displayDescriptionWithParametersCount, 1)
        XCTAssertEqual(presenterSpy.displayDescriptionDDDPassed, "11")
        XCTAssertEqual(presenterSpy.displayDescriptionNumberPassed, "12345-1234")
    }
    
    func testLoadDescription_WhenDDDorNumberIsInvalid_ShouldDisplayDescriptio() {
        sut = RegistrationPhoneCodeViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            ddd: "1",
            number: "12345-123",
            codeType: .sms
        )
        
        sut.loadDescription()
        
        XCTAssertEqual(presenterSpy.displayDescriptionCount, 1)
        XCTAssertNil(presenterSpy.displayDescriptionDDDPassed)
        XCTAssertNil(presenterSpy.displayDescriptionNumberPassed)
    }
    
    func testLoadDescription_WhenVerifyCallButtonIsVisible_ShouldDisplayCallButton() {
        serviceMock.isPhoneVerificationCallEnabledAtived = true
        
        sut.loadDescription()
        
        XCTAssertEqual(presenterSpy.displayCallButtonCount, 1)
    }
    
    func testLoadDescription_WhenCalledFromViewController_ShouldupdateCodeButtonTitle() {
        sut.loadDescription()
        
        XCTAssertEqual(presenterSpy.updateCodeButtonTitleCount, 1)
    }
    
    func testLoadDescription_WhenCalledFromViewController_ShouldupdateCallButtonTitleIfIsVerifySMSPhoneNumberIsVisibleIsTrue() {
        serviceMock.isPhoneVerificationCallEnabledAtived = true
        
        sut.loadDescription()
        
        XCTAssertEqual(presenterSpy.updateCallButtonTitleCount, 1)
    }
    
    func testConfigureTimers_WhenPhoneVerificationCallIsEnabledAndDelayIsPositive_ShouldUpdateCallButtonTitle() {
        let expectedCallCount = 3
        let delay = 2
        
        serviceMock.isPhoneVerificationCallEnabledAtived = true
        serviceMock.phoneVerificationCallDelayPassed = delay
        serviceMock.phoneVerificationSentDatePassed = Date()
        
        sut.configureTimers()
        
        expectation(waitForCondition: self.presenterSpy.updateCallButtonTitleCount == expectedCallCount)
        waitForExpectations(timeout: TimeInterval(expectedCallCount * 3))
    }
    
    func testConfigureTimers_WhenPhoneCodeTyleIsSMSAndDelayIsPositive_ShouldUpdateCodeButtonTitle() {
       let expectedCallCount = 4
       let delay = 3
       
       serviceMock.phoneVerificationSMSDelayPassed = delay
       serviceMock.phoneVerificationSentDatePassed = Date()
       
       sut.configureTimers()
       
       expectation(waitForCondition: self.presenterSpy.updateCodeButtonTitleCount == expectedCallCount)
       waitForExpectations(timeout: TimeInterval(expectedCallCount * 3))
    }
    
    func testConfigureTimers_WhenPhoneCodeTyleIsWhatsAppAndDelayIsPositive_ShouldUpdateCodeButtonTitle() {
        sut = RegistrationPhoneCodeViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            ddd: "11",
            number: "12345-1234",
            codeType: .whatsApp
        )
        
        let expectedCallCount = 2
        let delay = 1
       
        serviceMock.phoneVerificationWhatsAppDelayPassed = delay
        serviceMock.phoneVerificationSentDatePassed = Date()

        sut.configureTimers()

        expectation(waitForCondition: self.presenterSpy.updateCodeButtonTitleCount == expectedCallCount)
        waitForExpectations(timeout: TimeInterval(expectedCallCount * 3))
    }
    
    func testVerifyPhoneNumber_WhenCodeIsNil_ShouldDisplayInvalidCodeError() {
        sut.verifyPhoneNumber(with: nil)
        
        XCTAssertEqual(presenterSpy.displayErrorCount, 1)
        XCTAssertEqual(presenterSpy.displayErrorPassed, RegisterLocalizable.invalidCode.text)
    }
    
    func testVerifyPhoneNumber_WhenCodeIsInvalid_ShouldDisplayInvalidCodeError() {
        sut.verifyPhoneNumber(with: "123")
        
        XCTAssertEqual(presenterSpy.displayErrorCount, 1)
        XCTAssertEqual(presenterSpy.displayErrorPassed, RegisterLocalizable.invalidCode.text)
    }
    
    func testVerifyPhoneNumber_WhenCodeIsFormattedAndValid_ShouldPresentNextStep() {
        serviceMock.result = Result.success(PhoneNumberValidationResponse(ddd: "11", number: "12345-1234"))
        
        sut.verifyPhoneNumber(with: "1234")
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.dddToNextStep, "11")
        XCTAssertEqual(presenterSpy.numberToNextStep, "12345-1234")
        XCTAssertTrue(presenterSpy.moveToNextStepAction)
    }
    
    func testVerifyPhoneNumber_WhenCodeIsFormattedAndNotValid_ShouldDisplayErrorFromBackend() {
        let someErrorMessage = "Error message"
        var requestError = RequestError()
        requestError.message = someErrorMessage
        serviceMock.result = Result.failure(ApiError.badRequest(body: requestError))
        
        sut.verifyPhoneNumber(with: "1234")
        
        XCTAssertEqual(presenterSpy.displayErrorCount, 1)
        XCTAssertEqual(presenterSpy.displayErrorPassed, someErrorMessage)
        XCTAssertEqual(serviceMock.callValidatePhoneNumberCount, 1)
    }
    
    func testVerifyPhoneNumber_WhenCodeIsFormatedButApiCallFailWithConnectionError_ShouldDisplayError() {
        serviceMock.result = Result.failure(ApiError.connectionFailure)
        
        sut.verifyPhoneNumber(with: "1234")
        
        XCTAssertEqual(presenterSpy.displayErrorCount, 1)
        XCTAssertEqual(presenterSpy.displayErrorPassed, DefaultLocalizable.errorConnectionViewSubtitle.text)
        XCTAssertEqual(serviceMock.callValidatePhoneNumberCount, 1)
    }
    
    func testVerifyPhoneNumber_WhenCodeIsFormatedButApiCallFailWithGenericError_ShouldDisplayError() {
        serviceMock.result = Result.failure(ApiError.timeout)
        
        sut.verifyPhoneNumber(with: "1234")

        XCTAssertEqual(presenterSpy.displayErrorCount, 1)
        XCTAssertEqual(presenterSpy.displayErrorPassed, DefaultLocalizable.requestError.text)
        XCTAssertEqual(serviceMock.callValidatePhoneNumberCount, 1)
    }
    
    func testConfirmPhoneWithResendCode_WhenPhoneNumberIsValid_ShoudDisplayConfirmPhoneWithResendCode() {
        sut.confirmPhoneWithResendCode()
        
        XCTAssertEqual(presenterSpy.displayConfirmResendCodeAlertCount, 1)
    }
    
    func testConfirmPhoneWithResendCode_WhenPhoneNumberIsInvalid_ShoudDisplayAlertPhoneError() {
        sut = RegistrationPhoneCodeViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            ddd: "1",
            number: "12345-34",
            codeType: .whatsApp
        )
        
        sut.confirmPhoneWithResendCode()
        
        XCTAssertEqual(presenterSpy.displayConfirmResendCodeAlertCount, 0)
        XCTAssertEqual(presenterSpy.displayAlertErrorCount, 1)
    }
    
    func testConfirmPhoneWithReceiveCall__WhenPhoneNumberIsValid_ShoudDisplayConfirmPhoneWithReceiveCall() {
        sut.confirmPhoneWithReceiveCall()
        
        XCTAssertEqual(presenterSpy.displayConfirmReceiveCallAlertCount, 1)
    }
    
    func testConfirmPhoneWithReceiveCall_WhenPhoneNumberIsInvalid_ShoudDisplayAlertPhoneError() {
        sut = RegistrationPhoneCodeViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            ddd: "1",
            number: "12345-34",
            codeType: .whatsApp
        )
        
        sut.confirmPhoneWithReceiveCall()
        
        XCTAssertEqual(presenterSpy.displayConfirmReceiveCallAlertCount, 0)
        XCTAssertEqual(presenterSpy.displayAlertErrorCount, 1)
    }
    
    func testRequestCode_WhenReceiveSuccessFromService_ShouldUpdateVerificationData() {
        serviceMock.resultCode = Result.success(PhoneNumberCodeResponse(smsDelay: 0, whatsAppDelay: 0, callEnabled: true, callDelay: 0))
        
        sut.requestCode()
        
        XCTAssertEqual(serviceMock.callRequestValidationCodeCount, 1)
        XCTAssertEqual(serviceMock.callUpdateVerificationData, 1)
    }
    
    func testRequestCode_WhenReceiveErrorFromBackEnd_ShouldDisplayErrorFromBackend() {
        let someErrorMessage = "Error message"
        var requestError = RequestError()
        requestError.message = someErrorMessage
        serviceMock.resultCode = Result.failure(ApiError.badRequest(body: requestError))
        
        sut.requestCode()
        
        XCTAssertEqual(presenterSpy.displayErrorCount, 1)
        XCTAssertEqual(presenterSpy.displayErrorPassed, someErrorMessage)
        XCTAssertEqual(serviceMock.callRequestValidationCodeCount, 1)
    }
    
    func testRequestCode_WhenApiCallFailWithConnectionError_ShouldDisplayErrorFromBackend() {
        serviceMock.resultCode = Result.failure(ApiError.connectionFailure)
        
        sut.requestCode()
        
        XCTAssertEqual(presenterSpy.displayErrorCount, 1)
        XCTAssertEqual(presenterSpy.displayErrorPassed, DefaultLocalizable.errorConnectionViewSubtitle.text)
        XCTAssertEqual(serviceMock.callRequestValidationCodeCount, 1)
    }
    
    func testRequestCode_WhenApiCallFailWithGenericError_ShouldDisplayErrorFromBackend() {
        serviceMock.resultCode = Result.failure(ApiError.timeout)
        
        sut.requestCode()
        
        XCTAssertEqual(presenterSpy.displayErrorCount, 1)
        XCTAssertEqual(presenterSpy.displayErrorPassed, DefaultLocalizable.requestError.text)
        XCTAssertEqual(serviceMock.callRequestValidationCodeCount, 1)
    }
    
    func testRequestCode_WhenDDDIsInvalid_ShouldDisplayAlertError() {
        sut = RegistrationPhoneCodeViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            ddd: "1",
            number: "12345-1234",
            codeType: .whatsApp
        )
        
        sut.requestCode()
        
        XCTAssertEqual(presenterSpy.displayAlertErrorCount, 1)
    }
    
    func testRequestCode_WhenNumberIsInvalid_ShouldDisplayAlertError() {
       sut = RegistrationPhoneCodeViewModel(
           service: serviceMock,
           presenter: presenterSpy,
           ddd: "11",
           number: "12345-12",
           codeType: .whatsApp
       )
       
       sut.requestCode()
       
       XCTAssertEqual(presenterSpy.displayAlertErrorCount, 1)
   }
    
    func testRequestCode_WhenDDDAndNumberAreInvalid_ShouldDisplayAlertError() {
        sut = RegistrationPhoneCodeViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            ddd: "1",
            number: "12345-12",
            codeType: .whatsApp
        )
        
        sut.requestCode()
        
        XCTAssertEqual(presenterSpy.displayAlertErrorCount, 1)
    }
    
    func testRequestCall_WhenReceiveSuccessFromService_ShouldUpdateVerificationData() {
        serviceMock.resultCode = Result.success(PhoneNumberCodeResponse(smsDelay: 0, whatsAppDelay: 0, callEnabled: true, callDelay: 0))
        
        sut.requestCall()
        
        XCTAssertEqual(serviceMock.callRequestCallVerification, 1)
        XCTAssertEqual(serviceMock.callUpdateVerificationData, 1)
    }
    
    func testRequestCall_WhenReceiveErrorFromBackEnd_ShouldDisplayErrorFromBackend() {
        let someErrorMessage = "Error message"
        var requestError = RequestError()
        requestError.message = someErrorMessage
        serviceMock.resultCode = Result.failure(ApiError.badRequest(body: requestError))
        
        sut.requestCall()
        
        XCTAssertEqual(presenterSpy.displayErrorCount, 1)
        XCTAssertEqual(presenterSpy.displayErrorPassed, someErrorMessage)
        XCTAssertEqual(serviceMock.callRequestCallVerification, 1)
    }
    
    func testRequestCall_WhenApiCallFailWithConnectionError_ShouldDisplayErrorFromBackend() {
        serviceMock.resultCode = Result.failure(ApiError.connectionFailure)
        
        sut.requestCall()
        
        XCTAssertEqual(presenterSpy.displayErrorCount, 1)
        XCTAssertEqual(presenterSpy.displayErrorPassed, DefaultLocalizable.errorConnectionViewSubtitle.text)
        XCTAssertEqual(serviceMock.callRequestCallVerification, 1)
    }
    
    func testRequestCall_WhenApiCallFailWithGenericError_ShouldDisplayErrorFromBackend() {
        serviceMock.resultCode = Result.failure(ApiError.timeout)
        
        sut.requestCall()
        
        XCTAssertEqual(presenterSpy.displayErrorCount, 1)
        XCTAssertEqual(presenterSpy.displayErrorPassed, DefaultLocalizable.requestError.text)
        XCTAssertEqual(serviceMock.callRequestCallVerification, 1)
    }
    
    func testRequestCall_WhenDDDIsInvalid_ShouldDisplayAlertError() {
         sut = RegistrationPhoneCodeViewModel(
             service: serviceMock,
             presenter: presenterSpy,
             ddd: "1",
             number: "12345-1234",
             codeType: .whatsApp
         )
         
         sut.requestCall()
         
         XCTAssertEqual(presenterSpy.displayAlertErrorCount, 1)
     }
     
     func testRequestCall_WhenNumberIsInvalid_ShouldDisplayAlertError() {
        sut = RegistrationPhoneCodeViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            ddd: "11",
            number: "12345-12",
            codeType: .whatsApp
        )
        
        sut.requestCall()
        
        XCTAssertEqual(presenterSpy.displayAlertErrorCount, 1)
    }
     
     func testRequestCall_WhenDDDAndNumberAreInvalid_ShouldDisplayAlertError() {
         sut = RegistrationPhoneCodeViewModel(
             service: serviceMock,
             presenter: presenterSpy,
             ddd: "1",
             number: "12345-12",
             codeType: .whatsApp
         )
         
         sut.requestCall()
         
         XCTAssertEqual(presenterSpy.displayAlertErrorCount, 1)
     }
    
    func testVerifyPhoneNumber_WhenCodeIsValidAndRequestWasStarted_ShouldDisableControlsHideErrosAndShowLoading() {
        sut.verifyPhoneNumber(with: "1234")
        
        XCTAssertEqual(presenterSpy.disableControlsCount, 1)
        XCTAssertEqual(presenterSpy.hideErrorCount, 1)
        XCTAssertEqual(presenterSpy.showLoadingCount, 1)
    }
    
    func testVerifyPhoneNumber_WhenCodeIsValidAndRequestWasFinished_ShouldEnableControlsHideLoading() {
        serviceMock.result = Result.success(PhoneNumberValidationResponse(ddd: "11", number: "12345-1234"))
        
        sut.verifyPhoneNumber(with: "1234")
        
        XCTAssertEqual(presenterSpy.hideErrorCount, 1)
        XCTAssertEqual(presenterSpy.enableControlsCount, 1)
    }
    
    func testDidAlreadyRegister_WhenCalledFromViewModel_ShouldCallAlreadyRegisteredAction() {
        sut.didAlreadyRegister()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertTrue(presenterSpy.alreadyRegisteredAction)
    }
    
    func testDidHelp_WhenCalledFromViewModel_ShouldCallOpenHelp() {
        serviceMock.currentHelpPath = "help-path"
        
        sut.didHelp()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.openHelpPassed, "help-path")
        XCTAssertTrue(presenterSpy.openHelpAction)
    }
    
    func testDidEditPhoneNumber_WhenCalledFromViewModel_ShouldBackToEditPhoneNumber() {
        sut.didEditPhoneNumber()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertTrue(presenterSpy.editPhoneNumberAction)
    }
    
    func testDidTapBack_WhenCalledFromViewModel_ShouldBackView() {
        sut.didTapBack()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertTrue(presenterSpy.backAction)
    }
}

private final class RegistrationPhoneCodePresenterMock: RegistrationPhoneCodePresenting {
    var viewController: RegistrationPhoneCodeDisplay?
    private(set) var displayDescriptionCount = 0
    private(set) var displayDescriptionWithParametersCount = 0
    private(set) var displayDescriptionDDDPassed: String?
    private(set) var displayDescriptionNumberPassed: String?
    private(set) var displayCallButtonCount = 0
    private(set) var displayAlertErrorCount = 0
    private(set) var codeButtonIsActiveCount = 0
    private(set) var callButtonIsActiveCount = 0
    private(set) var updateCallButtonTitleCount = 0
    private(set) var updateCodeButtonTitleCount = 0
    private(set) var displayErrorCount = 0
    private(set) var displayErrorPassed: String?
    private(set) var displayConfirmResendCodeAlertCount = 0
    private(set) var displayConfirmReceiveCallAlertCount = 0
    private(set) var hideErrorCount = 0
    private(set) var showLoadingCount = 0
    private(set) var hideLoadingCount = 0
    private(set) var enableControlsCount = 0
    private(set) var disableControlsCount = 0
    private(set) var callDidNextStepCount = 0
    private(set) var moveToNextStepAction = false
    private(set) var dddToNextStep: String?
    private(set) var numberToNextStep: String?
    private(set) var alreadyRegisteredAction = false
    private(set) var openHelpAction = false
    private(set) var openHelpPassed: String?
    private(set) var editPhoneNumberAction = false
    private(set) var backAction = false
    
    func displayDescription() {
        displayDescriptionCount += 1
    }
    
    func displayDescription(ddd: String, number: String) {
        displayDescriptionWithParametersCount += 1
        displayDescriptionDDDPassed = ddd
        displayDescriptionNumberPassed = number
    }
    
    func displayCallButton() {
        displayCallButtonCount += 1
    }
    
    func displayAlertError() {
        displayAlertErrorCount += 1
    }
    
    func codeButtonIsActive(_ active: Bool) {
        codeButtonIsActiveCount += 1
    }
    
    func callButtonIsActive(_ active: Bool) {
        callButtonIsActiveCount += 1
    }
    
    func updateCodeButtonTitle() {
        updateCodeButtonTitleCount += 1
    }
    
    func updateCallButtonTitle() {
        updateCallButtonTitleCount += 1
    }
    
    func updateCallButtonTitle(interval: Int) {
        updateCallButtonTitleCount += 1
    }
    
    func updateCodeButtonTitle(interval: Int) {
        updateCodeButtonTitleCount += 1
    }
    
    func displayError(message: String) {
        displayErrorCount += 1
        displayErrorPassed = message
    }
    
    func displayConfirmResendCodeAlert(ddd: String, number: String) {
        displayConfirmResendCodeAlertCount += 1
    }
    
    func displayConfirmReceiveCallAlert(ddd: String, number: String) {
        displayConfirmReceiveCallAlertCount += 1
    }
    
    func hideError() {
        hideErrorCount += 1
    }
    
    func showLoading() {
        showLoadingCount += 1
    }
    
    func hideLoading() {
        hideLoadingCount += 1
    }
    
    func enableControls() {
        enableControlsCount += 1
    }
    
    func disableControls() {
        disableControlsCount += 1
    }
    
    func didNextStep(action: RegistrationPhoneCodeAction) {
        callDidNextStepCount += 1
        switch action {
        case .back:
            backAction = true
        case let .moveToNextStep(ddd, number):
            moveToNextStepAction = true
            dddToNextStep = ddd
            numberToNextStep = number
        case .alreadyRegistered:
            alreadyRegisteredAction = true
        case let .openHelp(path):
            openHelpAction = true
            openHelpPassed = path
        case .editPhoneNumber:
            editPhoneNumberAction = true
        }
    }
}

private final class RegistrationPhoneCodeServiceMock: RegistrationPhoneCodeServicing {
    var isPhoneVerificationCallEnabledAtived = false
    var currentHelpPath: String = ""
    var phoneVerificationSentDatePassed: Date?
    var phoneVerificationCallDelayPassed = 0
    var phoneVerificationSMSDelayPassed = 0
    var phoneVerificationWhatsAppDelayPassed = 0
    var result: Result<PhoneNumberValidationResponse, ApiError>?
    private(set) var callValidatePhoneNumberCount = 0
    var resultCode: Result<PhoneNumberCodeResponse, ApiError>?
    private(set) var callRequestValidationCodeCount = 0
    private(set) var callRequestCallVerification = 0
    private(set) var callUpdateVerificationData = 0
    
    var isPhoneVerificationCallEnabled: Bool {
        isPhoneVerificationCallEnabledAtived
    }
    
    var helpPath: String {
        currentHelpPath
    }
    
    lazy var phoneVerificationSentDate: Date = {
        phoneVerificationSentDatePassed ?? Date()
    }()
    
    var phoneVerificationCallDelay: Int {
        phoneVerificationCallDelayPassed
    }
    
    var phoneVerificationSMSDelay: Int {
        phoneVerificationSMSDelayPassed
    }
    
    var phoneVerificationWhatsAppDelay: Int {
        phoneVerificationWhatsAppDelayPassed
    }
    
    func verifyPhoneNumber(with code: String, completion: @escaping (Result<PhoneNumberValidationResponse, ApiError>) -> Void) {
        guard let result = result else {
            return
        }
        
        callValidatePhoneNumberCount += 1
        completion(result)
    }
    
    func requestValidationCode(ddd: String, number: String, codeType: RegistrationPhoneCodeType, completion: @escaping (Result<PhoneNumberCodeResponse, ApiError>) -> Void) {
        guard let resultCode = resultCode else {
            return
        }
        
        callRequestValidationCodeCount += 1
        completion(resultCode)
    }
    
    func requestCallVerification(ddd: String, number: String, completion: @escaping (Result<PhoneNumberCodeResponse, ApiError>) -> Void) {
        guard let resultCode = resultCode else {
            return
        }
        
        callRequestCallVerification += 1
        completion(resultCode)
    }
    
    func updateVerificationData(date: Date, smsDelay: Int, whatsAppDelay: Int, callDelay: Int, callEnabled: Bool) {
        callUpdateVerificationData += 1
    }
}
