@testable import PicPay

extension RegistrationPhoneNumberAction: Equatable {
    public static func ==(lhs: RegistrationPhoneNumberAction, rhs: RegistrationPhoneNumberAction) -> Bool {
        switch (lhs, rhs) {
        case (.alreadyCode, .alreadyCode), (.alreadyRegistered, .alreadyRegistered), (.back, .back):
            return true
        case let (.moveToNextStep(dddLhs, numberLhs, codeTypeLhs), .moveToNextStep(dddRhs, numberRhs, codeTypeRhs)):
            return dddLhs == dddRhs && numberLhs == numberRhs && codeTypeLhs == codeTypeRhs
        case let (.webview(urlLhs), .webview(urlRhs)):
            return urlLhs == urlRhs
        default:
            return false
        }
    }
}
