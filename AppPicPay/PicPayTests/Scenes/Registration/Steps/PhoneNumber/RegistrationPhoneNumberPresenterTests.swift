@testable import PicPay
import XCTest

final class RegistrationPhoneNumberPresenterTests: XCTestCase {
    
    private let viewControllerSpy = RegistrationPhoneNumberDisplayable()
    private let coordinatorSpy = RegistrationPhoneNumberCoordinatorSpy()
    
    private lazy var sut: RegistrationPhoneNumberPresenter = {
        let sut = RegistrationPhoneNumberPresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    func testDisplayTermsOfService_WhenCalledFromViewModel_ShouldShowTermsOfService() {
        sut.displayTermsOfService()
        
        XCTAssertEqual(viewControllerSpy.displayTermsOfServiceCount, 1)
    }
    
    func testFillDDDTextField_WhenCalledFromViewModel_ShouldFillDDDInTextField() {
        sut.fillDDDTextField(with: "11")
        
        XCTAssertEqual(viewControllerSpy.fillDDDTextFieldCount, 1)
        XCTAssertEqual(viewControllerSpy.fillDDDTextFieldPassed, "11")
    }
    
    func testFillPhoneTextField_WhenCalledFromViewModel_ShouldFillPhoneInTextField() {
        sut.fillPhoneTextField(with: "12345-1234")
        
        XCTAssertEqual(viewControllerSpy.fillPhoneTextFieldCount, 1)
        XCTAssertEqual(viewControllerSpy.fillPhoneTextFieldPassed, "12345-1234")
    }
    
    func testShowPhoneNumberError_WhenCalledFromViewModel_ShouldShowMessageErrorToPhoneNumber() {
        sut.showPhoneNumberError()
        
        XCTAssertEqual(viewControllerSpy.displayErrorMessageCount, 1)
        XCTAssertEqual(viewControllerSpy.displayErrorMessagePassed, RegisterLocalizable.invalidDDDAndPhone.text)
        XCTAssertTrue(try XCTUnwrap(viewControllerSpy.isDDDErrorPassed))
        XCTAssertTrue(try XCTUnwrap(viewControllerSpy.isNumberErrorPassed))
    }
    
    func testShowDDDError_WhenCalledFromViewModel_ShouldShowMessageErrorToDDD() throws {
        sut.showDDDError()
        
        XCTAssertEqual(viewControllerSpy.displayErrorMessageCount, 1)
        XCTAssertEqual(viewControllerSpy.displayErrorMessagePassed, RegisterLocalizable.invalidDDD.text)
        XCTAssertTrue(try XCTUnwrap(viewControllerSpy.isDDDErrorPassed))
    }
    
    func testShowNumberError_WhenCalledFromViewModel_ShouldShowMessageErrorToNumber() {
        sut.showNumberError()
        
        XCTAssertEqual(viewControllerSpy.displayErrorMessageCount, 1)
        XCTAssertEqual(viewControllerSpy.displayErrorMessagePassed, RegisterLocalizable.invalidPhone.text)
        XCTAssertTrue(try XCTUnwrap(viewControllerSpy.isNumberErrorPassed))
    }
    
    func testPresentError_WhenCalledFromViewModel_ShouldDisplayErrorMessage() {
        sut.presentError("Error")
        
        XCTAssertEqual(viewControllerSpy.displayErrorMessageCount, 1)
        XCTAssertEqual(viewControllerSpy.displayErrorMessagePassed, "Error")
    }
    
    func testHideErrorsMessages_WhenCalledFromViewModel_ShouldHideErrorsMessages() {
        sut.hideErrorsMessages()
        
        XCTAssertEqual(viewControllerSpy.hideErrorsMessagesCount, 1)
    }
    
    func testShowLoadingAnimationWithSMSCodeType_WhenCalledFromViewModel_ShouldShowLoadingInSMSButton() {
        sut.showLoadingAnimation(with: .sms)
        
        XCTAssertEqual(viewControllerSpy.showLoadingAnimationCount, 1)
        XCTAssertEqual(viewControllerSpy.showLoadingAnimationPassed, RegistrationPhoneCodeType.sms)
    }
    
    func testShowLoadingAnimationWithWhatsAppCodeType_WhenCalledFromViewModel_ShouldShowLoadingInWhatsAppButton() {
        sut.showLoadingAnimation(with: .whatsApp)
        
        XCTAssertEqual(viewControllerSpy.showLoadingAnimationCount, 1)
        XCTAssertEqual(viewControllerSpy.showLoadingAnimationPassed, RegistrationPhoneCodeType.whatsApp)
    }
    
    func testHideLoadingAnimationWithSMSCodeType_WhenCalledFromViewModel_ShouldHideLoadingInSMSButton() {
        sut.hideLoadingAnimation(with: .sms)
        
        XCTAssertEqual(viewControllerSpy.hideLoadingAnimationCount, 1)
        XCTAssertEqual(viewControllerSpy.hideLoadingAnimationPassed, RegistrationPhoneCodeType.sms)
    }
    
    func testHideLoadingAnimationWithWhatsAppType_WhenCalledFromViewModel_ShouldHideLoadingInWhatsApp() {
        sut.hideLoadingAnimation(with: .whatsApp)
        
        XCTAssertEqual(viewControllerSpy.hideLoadingAnimationCount, 1)
        XCTAssertEqual(viewControllerSpy.hideLoadingAnimationPassed, RegistrationPhoneCodeType.whatsApp)
    }
    
    func testDisableControls_WhenCalledFromViewModel_ShouldDisableControlsInViewController() {
        sut.disableControls()
        
        XCTAssertEqual(viewControllerSpy.disableControlsCount, 1)
    }
    
    func testEnableControls_WhenCalledFromViewModel_ShouldEnableControlsInViewController() {
        sut.enableControls()
        
        XCTAssertEqual(viewControllerSpy.enableControlsCount, 1)
    }
    
    func testDisplayAlert_WhenCalledFromViewModel_ShouldPassCorrectAlertParameters() {
        sut.displayAlert(ddd: "11", number: "12345-1234", codeType: .whatsApp)
        
        XCTAssertEqual(viewControllerSpy.displayAlertCount, 1)
        XCTAssertEqual(viewControllerSpy.alertTitle, RegisterLocalizable.alertPhoneConfirmTitle.text)
        XCTAssertEqual(viewControllerSpy.alertMessage, String(format: RegisterLocalizable.alertPhoneConfirmMessage.text, "11", "12345-1234"))
        XCTAssertEqual(viewControllerSpy.displayAlertDDDPassed, "11")
        XCTAssertEqual(viewControllerSpy.displayAlertNumberPassed, "12345-1234")
        XCTAssertEqual(viewControllerSpy.displayAlertPhoneCodeTypePassed, .whatsApp)
    }
    
    func testDisplayAlert_WhenCalledFromViewModelAndDDDorNumberIsNil_ShouldNotShowAlert() {
        sut.displayAlert(ddd: nil, number: nil, codeType: .whatsApp)
        
        XCTAssertEqual(viewControllerSpy.displayAlertCount, 0)
        XCTAssertNil(viewControllerSpy.displayAlertDDDPassed)
        XCTAssertNil(viewControllerSpy.displayAlertNumberPassed)
        XCTAssertNil(viewControllerSpy.displayAlertPhoneCodeTypePassed)
    }
    
    func testDidNextStepWithActionAlreadyCode_WhenCalledFromViewModel_ShouldPassAnActionToCoordinator() {
        sut.didNextStep(action: .alreadyCode(withDDD: "11", number: "12345-1234"))
        
        XCTAssertEqual(coordinatorSpy.actionCount, 1)
        XCTAssertEqual(coordinatorSpy.actionPassed, RegistrationPhoneNumberAction.alreadyCode(withDDD: "11", number: "12345-1234"))
    }
    
    func testDidNextStepWithActionMoveToNextStep_WhenCalledFromViewModel_ShouldPassAnActionToCoordinator() {
        sut.didNextStep(action: .moveToNextStep(withDDD: "11", number: "12345-1234", codeType: .sms))
        
        XCTAssertEqual(coordinatorSpy.actionCount, 1)
        XCTAssertEqual(coordinatorSpy.actionPassed, RegistrationPhoneNumberAction.moveToNextStep(withDDD: "11", number: "12345-1234", codeType: .sms))
    }
    
    func testDidNextStepWithActionAlreadyRegistered_WhenCalledFromViewModel_ShouldPassAnActionToCoordinator() {
        sut.didNextStep(action: .alreadyRegistered)
        
        XCTAssertEqual(coordinatorSpy.actionCount, 1)
        XCTAssertEqual(coordinatorSpy.actionPassed, RegistrationPhoneNumberAction.alreadyRegistered)
    }
    
    func testDidNextStepWithActionBack_WhenCalledFromViewModel_ShouldPassAnActionToCoordinator() {
        sut.didNextStep(action: .back)
        
        XCTAssertEqual(coordinatorSpy.actionCount, 1)
        XCTAssertEqual(coordinatorSpy.actionPassed, RegistrationPhoneNumberAction.back)
    }
}

private final class RegistrationPhoneNumberDisplayable: RegistrationPhoneNumberDisplay {
    private(set) var displayTermsOfServiceCount = 0
    private(set) var fillDDDTextFieldCount = 0
    private(set) var fillDDDTextFieldPassed: String?
    private(set) var fillPhoneTextFieldCount = 0
    private(set) var fillPhoneTextFieldPassed: String?
    private(set) var showWhatsAppButtonCount = 0
    private(set) var displayErrorMessageCount = 0
    private(set) var displayErrorMessagePassed: String?
    private(set) var isDDDErrorPassed: Bool?
    private(set) var isNumberErrorPassed: Bool?
    private(set) var hideErrorsMessagesCount = 0
    private(set) var showLoadingAnimationCount = 0
    private(set) var showLoadingAnimationPassed: RegistrationPhoneCodeType?
    private(set) var hideLoadingAnimationCount = 0
    private(set) var hideLoadingAnimationPassed: RegistrationPhoneCodeType?
    private(set) var disableControlsCount = 0
    private(set) var enableControlsCount = 0
    private(set) var displayAlertCount = 0
    private(set) var alertTitle: String?
    private(set) var alertMessage: String?
    private(set) var displayAlertDDDPassed: String?
    private(set) var displayAlertNumberPassed: String?
    private(set) var displayAlertPhoneCodeTypePassed: RegistrationPhoneCodeType?
    
    func displayTermsOfService() {
        displayTermsOfServiceCount += 1
    }
    
    func fillDDDTextField(with ddd: String) {
        fillDDDTextFieldCount += 1
        fillDDDTextFieldPassed = ddd
    }
    
    func fillPhoneTextField(with number: String) {
        fillPhoneTextFieldCount += 1
        fillPhoneTextFieldPassed = number
    }
    
    func showWhatsAppButton() {
        showWhatsAppButtonCount += 1
    }
    
    func displayError(_ message: String, isDDDError: Bool, isNumberError: Bool) {
        displayErrorMessageCount += 1
        displayErrorMessagePassed = message
        isDDDErrorPassed = isDDDError
        isNumberErrorPassed = isNumberError
    }
    
    func hideErrorsMessages() {
        hideErrorsMessagesCount += 1
    }
    
    func showLoadingAnimation(with codeType: RegistrationPhoneCodeType) {
        showLoadingAnimationCount += 1
        showLoadingAnimationPassed = codeType
    }
    
    func hideLoadingAnimation(with codeType: RegistrationPhoneCodeType) {
        hideLoadingAnimationCount += 1
        hideLoadingAnimationPassed = codeType
    }
    
    func disableControls() {
        disableControlsCount += 1
    }
    
    func enableControls() {
        enableControlsCount += 1
    }
    
    func displayAlert(title: String, message: String, ddd: String, number: String, codeType: RegistrationPhoneCodeType) {
        displayAlertCount += 1
        alertTitle = title
        alertMessage = message
        displayAlertDDDPassed = ddd
        displayAlertNumberPassed = number
        displayAlertPhoneCodeTypePassed = codeType
    }
}

private final class RegistrationPhoneNumberCoordinatorSpy: RegistrationPhoneNumberCoordinating {
    var delegate: RegistrationPhoneDelegate?
    var viewController: UIViewController?
    private(set) var actionCount = 0
    private(set) var actionPassed: RegistrationPhoneNumberAction?
    
    func perform(action: RegistrationPhoneNumberAction) {
        actionCount += 1
        actionPassed = action
    }
}
