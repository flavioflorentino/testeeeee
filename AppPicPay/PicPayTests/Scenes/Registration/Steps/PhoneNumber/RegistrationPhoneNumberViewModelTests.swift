@testable import PicPay
import Core
import XCTest

final class RegistrationPhoneNumberViewModelTests: XCTestCase {
    private let serviceMock = RegistrationPhoneNumberServiceMock()
    private let presenterSpy = RegistrationPhoneNumberPresenterSpy()
    private let phoneParametersProviderMock = PhoneParametersProviderMock()
    private lazy var sut =  RegistrationPhoneNumberViewModel(
        service: serviceMock,
        presenter: presenterSpy,
        phoneParametersProvider: phoneParametersProviderMock,
        showTermsOfService: false
    )
    
    func testSetupViewContent_WhenShowTermsOfServiceIsTrue_ShouldShowTermsOfService() {
        sut = RegistrationPhoneNumberViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            phoneParametersProvider: phoneParametersProviderMock,
            showTermsOfService: true
        )
        
        sut.setupViewContent()
        
        XCTAssertEqual(presenterSpy.displayTermsOfServiceCount, 1)
    }
    
    func testSetupViewContent_WhenFeatureFlagIsNotActive_ShouldShowWhatsAppButtonIsNotCalled() {
        sut.setupViewContent()
        
        XCTAssertEqual(presenterSpy.showWhatsAppButtonCount, 0)
    }
    
    func testFillPhoneNumberIfNeeded_WhenDDDAndPhoneIsNotEmpty_ShouldfillDDDAndPhone() {
        sut = RegistrationPhoneNumberViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            phoneParametersProvider: phoneParametersProviderMock,
            ddd: "11",
            number: "12345-1234",
            showTermsOfService: false
        )
        
        sut.fillPhoneNumberIfNeeded()
        
        XCTAssertEqual(presenterSpy.fillDDDTextFieldCount, 1)
        XCTAssertEqual(presenterSpy.fillDDDTextFieldPassed, "11")
        XCTAssertEqual(presenterSpy.fillPhoneTextFieldCount, 1)
        XCTAssertEqual(presenterSpy.fillPhoneTextFieldPassed, "12345-1234")
    }
    
    func testValidatePhone_WhenDddAndNumberAreNil_ShouldReturnFalseAndShowPhoneNumberError() {
        let isValid = sut.validatePhone(ddd: nil, number: nil)
        
        XCTAssertFalse(isValid)
        XCTAssertEqual(presenterSpy.showPhoneNumberErrorCount, 1)
    }
    
    func testValidatePhone_WhenDddAndNumberAreInvalid_ShouldReturnFalseAndShowPhoneNumberError() {
        let isValid = sut.validatePhone(ddd: "", number: "")
        
        XCTAssertFalse(isValid)
        XCTAssertEqual(presenterSpy.showPhoneNumberErrorCount, 1)
    }
    
    func testValidatePhone_WhenDddIsInvalidValidAndNumberIsValid_ShouldReturnFalseAndShowDDDError() {
        let isValid = sut.validatePhone(ddd: "1", number: "1234-1234")
        
        XCTAssertFalse(isValid)
        XCTAssertEqual(presenterSpy.showDDDErrorCount, 1)
    }
    
    func testValidatePhone_WhenDddIsValidAndNumberInvalid_ShouldReturnFalseAndShowPhoneError() {
        let isValid = sut.validatePhone(ddd: "11", number: "1234-")
        
        XCTAssertFalse(isValid)
        XCTAssertEqual(presenterSpy.showNumberErrorCount, 1)
    }
    
    func testValidatePhone_WhenDDDAndNumberAreValid_ShouldReturnTrue() {
        let isValid = sut.validatePhone(ddd: "11", number: "12345-1234")
        
        XCTAssertTrue(isValid)
    }
    
    func testConfirmPhone_WhenDDDAndNumberIsValid_ShouldPresenterAlertViewAndCodeTypeIsSMS() {
        sut.confirmPhoneNumber(with: .sms, ddd: "11", number: "12345-1234")
        
        XCTAssertEqual(presenterSpy.displayAlertCount, 1)
        XCTAssertEqual(presenterSpy.displayAlertDDDPassed, "11")
        XCTAssertEqual(presenterSpy.displayAlertNumberPassed, "12345-1234")
        XCTAssertEqual(presenterSpy.displayAlertCodeTypePassed, RegistrationPhoneCodeType.sms)
    }
    
    func testConfirmPhone_WhenDDDAndNumberIsValid_ShouldPresenterAlertViewAndCodeTypeIsWhatsApp() {
        sut.confirmPhoneNumber(with: .whatsApp, ddd: "41", number: "1234-1234")
        
        XCTAssertEqual(presenterSpy.displayAlertCount, 1)
        XCTAssertEqual(presenterSpy.displayAlertDDDPassed, "41")
        XCTAssertEqual(presenterSpy.displayAlertNumberPassed, "1234-1234")
        XCTAssertEqual(presenterSpy.displayAlertCodeTypePassed, RegistrationPhoneCodeType.whatsApp)
    }
    
    func testConfirmPhone_WhenDDDOrdNumberIsInvalid_ShouldNotPresentAlertViewController() {
        sut.confirmPhoneNumber(with: .whatsApp, ddd: "4", number: "1234-124")
        
        XCTAssertEqual(presenterSpy.displayAlertCount, 0)
        XCTAssertNil(presenterSpy.displayAlertDDDPassed)
        XCTAssertNil(presenterSpy.displayAlertNumberPassed)
        XCTAssertNil(presenterSpy.displayAlertCodeTypePassed)
    }
    
    func testConfirmPhone_WhenDDDAndNumberIsValid_ShouldHideAllErros() {
        sut.confirmPhoneNumber(with: .sms, ddd: "11", number: "12345-1234")
        
        XCTAssertEqual(presenterSpy.hideErrorsMessagesCount, 1)
    }
    
    func testRequestCode_WhenDDDAndNumberIsInvalid_ShouldShowNumerPhoneError() {
        sut.requestCode(with: .sms, ddd: "1", number: "1234-123")
        
        XCTAssertEqual(presenterSpy.showPhoneNumberErrorCount, 1)
    }
    
    func testRequestCode_WhenDDDIsValidAndNumberIsInvalid_ShouldShowNumberError() {
        sut.requestCode(with: .sms, ddd: "11", number: "1234-123")
        
        XCTAssertEqual(presenterSpy.showNumberErrorCount, 1)
    }
    
    func testRequestCode_WhenDDDIsInvalidAndNumberIsValid_ShouldShowDDDError() {
        sut.requestCode(with: .sms, ddd: "1", number: "1234-1234")
        
        XCTAssertEqual(presenterSpy.showDDDErrorCount, 1)
    }
    
    func testRequestCode_WhenCodeTypeIsWhatsAppAndDDDAndNumberIsValid_ShouldReturnSuccess() {
        serviceMock.isSuccess = true
        
        sut.requestCode(with: .whatsApp, ddd: "11", number: "12345-1234")
        
        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepPassed, RegistrationPhoneNumberAction.moveToNextStep(withDDD: "11", number: "12345-1234", codeType: .whatsApp))
    }
    
    func testRequestCode_WhenCodeTypeIsWhatsAppAndDDDAndNumberIsValid_ShouldReturnFailure() {
        serviceMock.isSuccess = false
        
        sut.requestCode(with: .whatsApp, ddd: "11", number: "12345-1234")
        
        XCTAssertEqual(presenterSpy.displayErrorMessageCount, 1)
        XCTAssertEqual(presenterSpy.displayErrorMessagePassed, "Error")
    }
    
    func testRequestCode_WhenCodeTypeIsSmsAndDDDAndNumberIsValid_ShouldReturnSuccess() {
        serviceMock.isSuccess = true
        
        sut.requestCode(with: .sms, ddd: "11", number: "12345-1234")
        
        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepPassed, RegistrationPhoneNumberAction.moveToNextStep(withDDD: "11", number: "12345-1234", codeType: .sms))
    }
    
    func testRequestCode_WhenCodeTypeIsSmSAndDDDAndNumberIsValid_ShouldReturnFailure() {
        serviceMock.isSuccess = false
        
        sut.requestCode(with: .sms, ddd: "11", number: "12345-1234")
        
        XCTAssertEqual(presenterSpy.displayErrorMessageCount, 1)
        XCTAssertEqual(presenterSpy.displayErrorMessagePassed, "Error")
    }
    
    func testRequestCode_WhenAnyCodeTypeAndDDDAndNumberIsValid_ShouldReturnSuccessOrFailureAndShowLoadingAnimation() {
        serviceMock.isSuccess = true
        
        sut.requestCode(with: .whatsApp, ddd: "11", number: "12345-1234")
        
        XCTAssertEqual(presenterSpy.showLoadingAnimationCount, 1)
    }
    
    func testRequestCode_WhenAnyCodeTypeAndDDDAndNumberIsValid_ShouldReturnSuccessOrFailureAndHideLoadingAnimation() {
        serviceMock.isSuccess = true
        
        sut.requestCode(with: .whatsApp, ddd: "11", number: "12345-1234")
        
        XCTAssertEqual(presenterSpy.hideLoadingAnimationCount, 1)
    }
    
    func testRequestCode_WhenAnyCodeTypeAndDDDAndNumberIsValid_ShouldDisableControls() {
        serviceMock.isSuccess = true
        
        sut.requestCode(with: .whatsApp, ddd: "11", number: "12345-1234")
        
        XCTAssertEqual(presenterSpy.disableControlsCount, 1)
    }
    
    func testRequestCode_WhenAnyCodeTypeAndDDDAndNumberIsValid_ShouldEnableControls() {
        serviceMock.isSuccess = true
        
        sut.requestCode(with: .whatsApp, ddd: "11", number: "12345-1234")
        
        XCTAssertEqual(presenterSpy.enableControlsCount, 1)
    }
    
    func testDidAlreadyCode_WhenCalledFromViewController_ShouldDidNextStepCalled() {
        sut.didAlreadyCode(ddd: "11", number: "12345-1234")
        
        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepPassed, RegistrationPhoneNumberAction.alreadyCode(withDDD: "11", number: "12345-1234"))
    }
    
    func testDidAlreadyRegistered_WhenCalledFromViewController_ShouldDidNextStepCalled() {
        sut.didAlreadyRegistered()
        
        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepPassed, RegistrationPhoneNumberAction.alreadyRegistered)
    }
    
    func testDidInteractWithUrl_WhenURLISValid_ShouldOpenWebview() throws {
        let url = try URL(string: "api/terms").safe()
        let expectedURL = try URL(string: WebServiceInterface.apiEndpoint(url.absoluteString)).safe()
        
        sut.didInteractWithUrl(url)
        
        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepPassed, RegistrationPhoneNumberAction.webview(url: expectedURL))
    }
    
    func testDidTapBack_WhenCalledFromViewController_ShouldDidNextStepCalled() {
        sut.didTapBack()
        
        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepPassed, RegistrationPhoneNumberAction.back)
    }
}

private final class RegistrationPhoneNumberPresenterSpy: RegistrationPhoneNumberPresenting {
    var viewController: RegistrationPhoneNumberDisplay?
    private(set) var displayTermsOfServiceCount = 0
    private(set) var fillDDDTextFieldCount = 0
    private(set) var fillDDDTextFieldPassed: String?
    private(set) var fillPhoneTextFieldCount = 0
    private(set) var fillPhoneTextFieldPassed: String?
    private(set) var showWhatsAppButtonCount = 0
    private(set) var showPhoneNumberErrorCount = 0
    private(set) var showDDDErrorCount = 0
    private(set) var showNumberErrorCount = 0
    private(set) var displayErrorMessageCount = 0
    private(set) var displayErrorMessagePassed: String?
    private(set) var hideErrorsMessagesCount = 0
    private(set) var displayAlertCount = 0
    private(set) var displayAlertDDDPassed: String?
    private(set) var displayAlertNumberPassed: String?
    private(set) var displayAlertCodeTypePassed: RegistrationPhoneCodeType?
    private(set) var didNextStepCount = 0
    private(set) var didNextStepPassed: RegistrationPhoneNumberAction?
    private(set) var showLoadingAnimationCount = 0
    private(set) var hideLoadingAnimationCount = 0
    private(set) var disableControlsCount = 0
    private(set) var enableControlsCount = 0
    
    func displayTermsOfService() {
        displayTermsOfServiceCount += 1
    }
    
    func fillDDDTextField(with ddd: String) {
        fillDDDTextFieldCount += 1
        fillDDDTextFieldPassed = ddd
    }
    
    func fillPhoneTextField(with number: String) {
        fillPhoneTextFieldCount += 1
        fillPhoneTextFieldPassed = number
    }
    
    func showWhatsAppButton() {
        showWhatsAppButtonCount += 1
    }
    
    func showPhoneNumberError() {
        showPhoneNumberErrorCount += 1
    }
    
    func showDDDError() {
        showDDDErrorCount += 1
    }
    
    func showNumberError() {
        showNumberErrorCount += 1
    }
    
    func presentError(_ message: String) {
        displayErrorMessageCount += 1
        displayErrorMessagePassed = message
    }
    
    func hideErrorsMessages() {
        hideErrorsMessagesCount += 1
    }
    
    func showLoadingAnimation(with codeType: RegistrationPhoneCodeType) {
        showLoadingAnimationCount += 1
    }
    
    func hideLoadingAnimation(with codeType: RegistrationPhoneCodeType) {
        hideLoadingAnimationCount += 1
    }
    
    func disableControls() {
        disableControlsCount += 1
    }
    
    func enableControls() {
        enableControlsCount += 1
    }
    
    func displayAlert(ddd: String?, number: String?, codeType: RegistrationPhoneCodeType) {
        displayAlertCount += 1
        displayAlertDDDPassed = ddd
        displayAlertNumberPassed = number
        displayAlertCodeTypePassed = codeType
    }
    
    func didNextStep(action: RegistrationPhoneNumberAction) {
        didNextStepPassed = action
        didNextStepCount += 1
    }
}

private final class RegistrationPhoneNumberServiceMock: RegistrationPhoneNumberServicing {
    var isWhatsAppIsEbabled = false
    var isSuccess = false
    
    var whatsAppIsEbabled: Bool {
        return isWhatsAppIsEbabled
    }
    
    func requestValidationCode(ddd: String, number: String, codeType: RegistrationPhoneCodeType, completion: @escaping (Result<PhoneNumberCodeResponse, ApiError>) -> Void) {
        guard isSuccess else {
           var requestError = RequestError()
           requestError.message = "Error"
            completion(.failure(ApiError.badRequest(body: requestError)))
           return
        }
        
        completion(.success(PhoneNumberCodeResponse(smsDelay: 0, whatsAppDelay: 0, callEnabled: true, callDelay: 0)))
    }
}

private final class PhoneParametersProviderMock: PhoneVerificationParametersProvider {
    func getPhoneVerificationCallDelay() -> Int {
        1
    }
    
    func setPhoneVerificationCallDelay(_ callDelay: Int) {
        
    }
    
    func isEnabledPhoneVerificationCall() -> Bool {
        true
    }
    
    func setEnabledPhoneVerificationCall(_ enabled: Bool) {
        
    }
    
    func getPhoneVerificationSMSDelay() -> Int {
        1
    }
    
    func setPhoneVerificationSMSDelay(_ callDelay: Int) {
        
    }
    
    func getPhoneVerificationWhatsAppDelay() -> Int {
        1
    }
    
    func setPhoneVerificationWhatsAppDelay(_ whatsAppDelay: Int) {
        
    }
    
    func getPhoneVerificationSentDate() -> Date? {
        Date()
    }
    
    func setPhoneVerificationSentDate(_ date: Date?) {
        
    }
    
    func setPhoneVerificationDDD(_ ddd: String?) {
        
    }
    
    func getPhoneVerificationDDD() -> String? {
        nil
    }
    
    func setPhoneVerificationNumber(_ number: String?) {
        
    }
    
    func getPhoneVerificationNumber() -> String? {
        nil
    }
}
