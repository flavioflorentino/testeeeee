@testable import PicPay
import XCTest

private final class RegistrationEmailCoordinatorSpy: RegistrationEmailCoordinating {
    var delegate: RegistrationEmailDelegate?
    
     weak var viewController: UIViewController?
    
    private(set) var callPerformCount = 0
    private(set) var moveToNextStepAction = false
    private(set) var emailToNextStep: String?
    private(set) var invalidEmailCounter = 0
    private(set) var alreadyRegisteredAction = false
    private(set) var webviewAction = false
    private(set) var webviewUrl: URL?
    private(set) var backAction = false
    
    func perform(action: RegistrationEmailAction) {
        callPerformCount += 1
        
        switch action {
        case .back:
            backAction = true
        case let .moveToNextStep(email, invalidEmailCounter):
            moveToNextStepAction = true
            emailToNextStep = email
            self.invalidEmailCounter = invalidEmailCounter
        case .alreadyRegistered:
            alreadyRegisteredAction = true
        case .webview(let url):
            webviewAction = true
            webviewUrl = url
        }
    }
}

private final class RegistrationEmailViewControllerSpy: RegistrationEmailDisplay {
    private(set) var callFillEmailTextFieldCount = 0
    private(set) var email: String?
    private(set) var callDisplayTermsOfServiceCount = 0
    private(set) var callDisplayErrorCount = 0
    private(set) var errorMessage: String?
    private(set) var callHideErrorsCount = 0
    private(set) var callShowLoadingCount = 0
    private(set) var callHideLoadingCount = 0
    private(set) var callDismissKeyboardCount = 0
    private(set) var callEnableControlsCount = 0
    
    func fillEmailTextField(with email: String) {
        self.email = email
        callFillEmailTextFieldCount += 1
    }
    
    func displayTermsOfService() {
        callDisplayTermsOfServiceCount += 1
    }
    
    func displayError(message: String) {
        errorMessage = message
        callDisplayErrorCount += 1
    }
    
    func hideErrors() {
        callHideErrorsCount += 1
    }
    
    func showLoading() {
        callShowLoadingCount += 1
    }
    
    func hideLoading() {
        callHideLoadingCount += 1
    }
    
    func dismissKeyboard() {
        callDismissKeyboardCount += 1
    }
    
    func enableControls() {
        callEnableControlsCount += 1
    }
}

class RegistrationEmailPresenterTests: XCTestCase {
    private let viewControllerSpy = RegistrationEmailViewControllerSpy()
    private let coordinatorSpy = RegistrationEmailCoordinatorSpy()
    private lazy var sut: RegistrationEmailPresenting = {
        let presenter = RegistrationEmailPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testFillEmailTextField_ShouldFillEmailTextFieldWithString() {
        sut.fillEmailTextField(with: "some_string")
        
        XCTAssertEqual(viewControllerSpy.callFillEmailTextFieldCount, 1)
        XCTAssertEqual(viewControllerSpy.email, "some_string")
    }
    
    func testDisplayTermsOfService_ShouldDisplayTermsOfServiceView() {
        sut.displayTermsOfService()
        
        XCTAssertEqual(viewControllerSpy.callDisplayTermsOfServiceCount, 1)
    }
    
    func testDidNextStep_WhenActionIsMoveToNextStep_ShouldMoveToNextStep() {
        let email = "myemail@picpay.com"
        let invalidEmailCounter = Int.random(in: 0...100)
        
        sut.didNextStep(action: .moveToNextStep(withEmail: email, invalidEmailCounter: invalidEmailCounter))
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssert(coordinatorSpy.moveToNextStepAction)
        XCTAssertEqual(coordinatorSpy.emailToNextStep, email)
        XCTAssertEqual(coordinatorSpy.invalidEmailCounter, invalidEmailCounter)
    }
    
    func testDidNextStep_WhenActionIsOpenWebview_ShouldOpenWebview() throws {
        let url = try XCTUnwrap(URL(string: "www.picpay.com"))
        
        sut.didNextStep(action: .webview(url: url))
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssert(coordinatorSpy.webviewAction)
        XCTAssertEqual(coordinatorSpy.webviewUrl, url)
    }
    
    func testDidNextStep_WhenActionIsAlreadyRegistered_ShouldMaketheAlreadyRegisteredAction() throws {
        sut.didNextStep(action: .alreadyRegistered)
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssert(coordinatorSpy.alreadyRegisteredAction)
    }
    
    func testDidNextStep_WhenActionIsBack_ShouldMakeBackAction() {
        sut.didNextStep(action: .back)
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertTrue(coordinatorSpy.backAction)
    }
    
    func testDisplayError_ShouldDisplayErrorMessage() {
        let errorMessage = "Some error"
        
        sut.displayError(message: errorMessage)
        
        XCTAssertEqual(viewControllerSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(viewControllerSpy.errorMessage, errorMessage)
    }
    
    func testHideErrors_ShouldHideErrorMessages() {
        sut.hideErrors()
        
        XCTAssertEqual(viewControllerSpy.callHideErrorsCount, 1)
    }
    
    func testShowLoading_ShouldShowLoadingAnimation() {
        sut.showLoading()
        
        XCTAssertEqual(viewControllerSpy.callShowLoadingCount, 1)
    }
    
    func testHideLoading_ShouldHideLoadingAnimation() {
        sut.hideLoading()
        
        XCTAssertEqual(viewControllerSpy.callHideLoadingCount, 1)
    }
    
    func testDismissKeyboard_ShouldDismissKeyboard() {
        sut.dismissKeyboard()
        
        XCTAssertEqual(viewControllerSpy.callDismissKeyboardCount, 1)
    }
}
