@testable import PicPay
import XCTest

private final class RegistrationFlowCoordinatorSpy: RegistrationEmailDelegate {
    private(set) var callMoveToNextStepCount = 0
    private(set) var emailToNextStep: String?
    private(set) var invalidEmailCounter: Int?
    private(set) var callMoveToLoginCount = 0
    private(set) var callMoveToPreviousRegistrationStep = 0
    
    func moveToNextStep(withEmail email: String, invalidEmailCounter: Int) {
        emailToNextStep = email
        callMoveToNextStepCount += 1
        self.invalidEmailCounter = invalidEmailCounter
    }
    
    func moveToLogin() {
        callMoveToLoginCount += 1
    }
    
    func moveToPreviousRegistrationStep() {
        callMoveToPreviousRegistrationStep += 1
    }
}

final class RegistrationEmailCoordinatorTests: XCTestCase {
    private let flowCoordinator = RegistrationFlowCoordinatorSpy()
    private let navController = NavigationControllerMock(rootViewController: UIViewController())
    private lazy var sut: RegistrationEmailCoordinating = {
        let coordinator = RegistrationEmailCoordinator()
        coordinator.viewController = navController.topViewController
        coordinator.delegate = flowCoordinator
        return coordinator
    }()
    
    func testPerform_WhenActionIsMoveToNextStep_ShouldMoveToNextStep() {
        let email = "email string"
        let invalidEmailCounter = Int.random(in: 0...100)
        
        sut.perform(action: .moveToNextStep(withEmail: email, invalidEmailCounter: invalidEmailCounter))
        
        XCTAssertEqual(flowCoordinator.callMoveToNextStepCount, 1)
        XCTAssertEqual(flowCoordinator.emailToNextStep, email)
        XCTAssertEqual(flowCoordinator.invalidEmailCounter, invalidEmailCounter)
    }
    
    func testPerform_WhenActionIsAlreadyRegistered_ShouldMoveBackToStartScreen() {
        sut.perform(action: .alreadyRegistered)
        
        XCTAssertEqual(flowCoordinator.callMoveToLoginCount, 1)
    }
    
    func testPerform_WhenActionIsWebview_ShouldOpenWebview() throws {
        let url = try XCTUnwrap(URL(string: "www.picpay.com"))
        
        sut.perform(action: .webview(url: url))
        
        XCTAssertTrue(navController.isPresentViewControllerCalled)
        let viewControllerPresented = try XCTUnwrap((navController.viewControllerPresented as? UINavigationController)?.topViewController)
        XCTAssertTrue(viewControllerPresented.isKind(of: WebViewController.self))
    }
    
    func testPerform_WhenActionIsBack_ShouldBackView() {
       sut.perform(action: .back)
       
       XCTAssertEqual(flowCoordinator.callMoveToPreviousRegistrationStep, 1)
    }
}
