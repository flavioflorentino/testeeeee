@testable import PicPay
import Core
import XCTest

private final class RegistrationEmailPresenterSpy: RegistrationEmailPresenting {
    var viewController: RegistrationEmailDisplay?
    
    private(set) var callFillEmailTextFieldCount = 0
    private(set) var emailInTextField: String?
    private(set) var callDisplayTermsOfServiceCount = 0
    private(set) var callDisplayInputErrorCount = 0
    private(set) var callDisplayErrorCount = 0
    private(set) var errorMessage: String?
    private(set) var callHideErrorsCount = 0
    private(set) var callShowLoadingCount = 0
    private(set) var callHideLoadingCount = 0
    private(set) var callDismissKeyboardCount = 0
    private(set) var callEnableControlsCount = 0
    
    private(set) var callDidNextStepCount = 0
    private(set) var moveToNextStepAction = false
    private(set) var emailToNextStep: String?
    private(set) var invalidEmailCounter = 0
    private(set) var alreadyRegisteredAction = false
    private(set) var webviewAction = false
    private(set) var webviewUrl: URL?
    private(set) var backAction = false
    
    func fillEmailTextField(with email: String) {
        emailInTextField = email
        callFillEmailTextFieldCount += 1
    }
    
    func displayTermsOfService() {
        callDisplayTermsOfServiceCount += 1
    }
    
    func didNextStep(action: RegistrationEmailAction) {
        callDidNextStepCount += 1
        switch action {
        case .back:
            backAction = true
        case let .moveToNextStep(email, invalidEmailCounter):
            moveToNextStepAction = true
            emailToNextStep = email
            self.invalidEmailCounter = invalidEmailCounter
        case .alreadyRegistered:
            alreadyRegisteredAction = true
        case .webview(let url):
            webviewAction = true
            webviewUrl = url
        }
    }
    
    func displayInputError() {
        callDisplayInputErrorCount += 1
    }
    
    func displayError(message: String) {
        errorMessage = message
        callDisplayErrorCount += 1
    }
    
    func hideErrors() {
        callHideErrorsCount += 1
    }
    
    func showLoading() {
        callShowLoadingCount += 1
    }
    
    func hideLoading() {
        callHideLoadingCount += 1
    }
    
    func dismissKeyboard() {
        callDismissKeyboardCount += 1
    }
    
    func enableControls() {
        callEnableControlsCount += 1
    }
}

private final class RegistrationEmailServiceMock: RegistrationEmailServicing {
    private(set) var callValidateEmailCount = 0
    
    var result: Result<Void, ApiError> = Result.success
    
    func validateEmail(email: String, completion: @escaping (Result<Void, ApiError>) -> Void) {
        callValidateEmailCount += 1
        completion(result)
    }
}

final class RegistrationEmailViewModelTests: XCTestCase {
    private let serviceMock = RegistrationEmailServiceMock()
    private let presenterSpy = RegistrationEmailPresenterSpy()
    private lazy var sut = RegistrationEmailViewModel(
        service: serviceMock,
        presenter: presenterSpy,
        email: nil,
        showTermsOfService: false
    )
    
    func testSetupViewContent_WhenTermsOfServiceIsActive_ShouldDisplayTermsOfService() {
        sut = RegistrationEmailViewModel(service: serviceMock, presenter: presenterSpy, email: nil, showTermsOfService: true)
        
        sut.setupViewContent()
        
        XCTAssertEqual(presenterSpy.callDisplayTermsOfServiceCount, 1)
    }
    
    func testSetupViewContent_WhenTermsOfServiceIsNotActive_ShouldNotDisplayTermsOfService() {
        sut = RegistrationEmailViewModel(service: serviceMock, presenter: presenterSpy, email: nil, showTermsOfService: false)
        
        sut.setupViewContent()
        
        XCTAssertEqual(presenterSpy.callDisplayTermsOfServiceCount, 0)
    }
    
    func testSetupViewContent_WhenInitiatedWithEmail_ShouldFillTextFieldWithTheEmail() {
        sut = RegistrationEmailViewModel(service: serviceMock, presenter: presenterSpy, email: "email@abc.com", showTermsOfService: false)
        
        sut.setupViewContent()
        
        XCTAssertEqual(presenterSpy.callFillEmailTextFieldCount, 1)
        XCTAssertEqual(presenterSpy.emailInTextField, "email@abc.com")
    }
    
    func testSetupViewContent_WhenInitiatedWithoutEmail_ShouldNotChangeTextInTextField() {
        sut = RegistrationEmailViewModel(service: serviceMock, presenter: presenterSpy, email: nil, showTermsOfService: false)
        
        sut.setupViewContent()
        
        XCTAssertEqual(presenterSpy.callFillEmailTextFieldCount, 0)
        XCTAssertNil(presenterSpy.emailInTextField)
    }
    
    func testDidTApConfirmButton_WhenEmailIsNil_ShouldDisplayInputError() {
        sut = RegistrationEmailViewModel(service: serviceMock, presenter: presenterSpy, email: nil, showTermsOfService: false)
        
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.callDisplayInputErrorCount, 1)
        XCTAssertEqual(serviceMock.callValidateEmailCount, 0)
    }
    
    func testDidTApConfirmButton_WhenEmailIsNotFormatedWithADot_ShouldDisplayInputError() {
        sut.updateEmail(with: "email@google")
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.callDisplayInputErrorCount, 1)
        XCTAssertEqual(serviceMock.callValidateEmailCount, 0)
    }
    
    func testDidTApConfirmButton_WhenEmailIsNotFormated_ShouldDisplayInputError() {
        sut.updateEmail(with: "abc")
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.callDisplayInputErrorCount, 1)
        XCTAssertEqual(serviceMock.callValidateEmailCount, 0)
    }
    
    func testDidTApConfirmButton_WhenEmailIsFormatedAndValid_ShouldPresentNextStep() {
        serviceMock.result = Result.success
        
        sut.updateEmail(with: "email@google.com")
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.callDisplayInputErrorCount, 0)
        XCTAssertEqual(serviceMock.callValidateEmailCount, 1)
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssert(presenterSpy.moveToNextStepAction)
        XCTAssertEqual(presenterSpy.emailToNextStep, "email@google.com")
        XCTAssertEqual(presenterSpy.invalidEmailCounter, 0)
    }
    
    func testDidTApConfirmButton_WhenEmailIsFormatedAndValidAfterInvalidEmailError_ShouldPresentNextStep() {
        var requestError = RequestError()
        requestError.code = "5004"
        serviceMock.result = Result.failure(ApiError.badRequest(body: requestError))
        
        sut.updateEmail(with: "email@google.com")
        sut.didConfirm()
        
        serviceMock.result = Result.success
        
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.callDisplayInputErrorCount, 0)
        XCTAssertEqual(serviceMock.callValidateEmailCount, 2)
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssert(presenterSpy.moveToNextStepAction)
        XCTAssertEqual(presenterSpy.emailToNextStep, "email@google.com")
        XCTAssertEqual(presenterSpy.invalidEmailCounter, 1)
    }
    
    func testDidTApConfirmButton_WhenEmailIsFormatedAndValidAfterAlreadyRegisteredEmailError_ShouldPresentNextStep() {
        var requestError = RequestError()
        requestError.code = "5005"
        serviceMock.result = Result.failure(ApiError.badRequest(body: requestError))
        
        sut.updateEmail(with: "email@google.com")
        sut.didConfirm()
        
        serviceMock.result = Result.success
        
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.callDisplayInputErrorCount, 0)
        XCTAssertEqual(serviceMock.callValidateEmailCount, 2)
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssert(presenterSpy.moveToNextStepAction)
        XCTAssertEqual(presenterSpy.emailToNextStep, "email@google.com")
        XCTAssertEqual(presenterSpy.invalidEmailCounter, 1)
    }
    
    func testDidTApConfirmButton_WhenEmailIsFormatedButNotValid_ShouldDisplayErrorFromBackend() {
        let someErrorMessage = "Error message"
        var requestError = RequestError()
        requestError.message = someErrorMessage
        serviceMock.result = Result.failure(ApiError.badRequest(body: requestError))
        
        sut.updateEmail(with: "email@google.com")
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.callDisplayInputErrorCount, 0)
        XCTAssertEqual(serviceMock.callValidateEmailCount, 1)
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 0)
        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, someErrorMessage)
    }
    
    func testDidTApConfirmButton_WhenEmailIsFormatedButApiCallFailWithConnectionError_ShouldDisplayError() {
        serviceMock.result = Result.failure(ApiError.connectionFailure)
        
        sut.updateEmail(with: "email@google.com")
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.callDisplayInputErrorCount, 0)
        XCTAssertEqual(serviceMock.callValidateEmailCount, 1)
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 0)
        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, DefaultLocalizable.errorConnectionViewSubtitle.text)
    }
    
    func testDidTApConfirmButton_WhenEmailIsFormatedButApiCallFailWithGenericError_ShouldDisplayError() {
        serviceMock.result = Result.failure(ApiError.timeout)
        
        sut.updateEmail(with: "email@google.com")
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.callDisplayInputErrorCount, 0)
        XCTAssertEqual(serviceMock.callValidateEmailCount, 1)
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 0)
        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, DefaultLocalizable.requestError.text)
    }
    
    func testDidTapAlreadyRegisteredButton_ShouldPerformTheAlreadyRegisteredAction() {
        sut.didAlreadyRegistered()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssert(presenterSpy.alreadyRegisteredAction)
    }
    
    func testDidInteractWithValidUrl_ShouldOpenWebview() throws {
        let url = try XCTUnwrap(URL(string: "api/terms"))
        
        sut.didInteractWithUrl(url)
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssert(presenterSpy.webviewAction)
        let formatedUrlString = try XCTUnwrap(WebServiceInterface.apiEndpoint("api/terms"))
        
        XCTAssertEqual(presenterSpy.webviewUrl?.absoluteString, formatedUrlString)
    }
    
    func testDidTapBack_ShouldPerformBackAction() {
        sut.didTapBack()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertTrue(presenterSpy.backAction)
    }
}
