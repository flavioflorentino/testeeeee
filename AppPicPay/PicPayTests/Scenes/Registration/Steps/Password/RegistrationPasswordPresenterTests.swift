@testable import PicPay
import XCTest

private final class RegistrationPasswordCoordinatorSpy: RegistrationPasswordCoordinating {
    var delegate: RegistrationPasswordDelegate?
    weak var viewController: UIViewController?
    
    private(set) var callPerformCount = 0
    private(set) var moveToNextStepAction = false
    private(set) var passwordToNextStep: String?
    private(set) var alreadyRegisteredAction = false
    private(set) var webviewAction = false
    private(set) var webviewUrl: URL?
    private(set) var backAction = false
    
    func perform(action: RegistrationPasswordAction) {
        callPerformCount += 1
        
        switch action {
        case .back:
            backAction = true
        case .moveToNextStep(let  password):
            moveToNextStepAction = true
            passwordToNextStep = password
        case .alreadyRegistered:
            alreadyRegisteredAction = true
        case .webview(let url):
            webviewAction = true
            webviewUrl = url
        }
    }
}

private final class RegistrationPasswordViewControllerSpy: RegistrationPasswordDisplay {
    private(set) var callFillPasswordTextFieldCount = 0
    private(set) var password: String?
    private(set) var callDisplayTermsOfServiceCount = 0
    private(set) var callUpdatePasswordVisibilityCount = 0
    private(set) var isPasswordVisible: Bool?
    private(set) var callDisplayErrorCount = 0
    private(set) var errorMessage: String?
    private(set) var callHideErrorsCount = 0
    private(set) var callShowLoadingCount = 0
    private(set) var callHideLoadingCount = 0
    private(set) var callDismissKeyboardCount = 0
    
    func fillPasswordTextField(with password: String) {
        self.password = password
        callFillPasswordTextFieldCount += 1
    }
    
    func displayTermsOfService() {
        callDisplayTermsOfServiceCount += 1
    }
    
    func updatePasswordVisibility(isVisible: Bool) {
        callUpdatePasswordVisibilityCount += 1
        isPasswordVisible = isVisible
    }
    
    func displayError(message: String) {
        errorMessage = message
        callDisplayErrorCount += 1
    }
    
    func hideErrors() {
        callHideErrorsCount += 1
    }
    
    func showLoading() {
        callShowLoadingCount += 1
    }
    
    func hideLoading() {
        callHideLoadingCount += 1
    }
    
    func dismissKeyboard() {
        callDismissKeyboardCount += 1
    }
}

class RegistrationPasswordPresenterTests: XCTestCase {
    private let viewControllerSpy = RegistrationPasswordViewControllerSpy()
    private let coordinatorSpy = RegistrationPasswordCoordinatorSpy()
    private lazy var sut: RegistrationPasswordPresenting = {
        let presenter = RegistrationPasswordPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testFillPasswordTextField_ShouldFillPasswordTextFieldWithString() {
        sut.fillPasswordTextField(with: "623464")
        
        XCTAssertEqual(viewControllerSpy.callFillPasswordTextFieldCount, 1)
        XCTAssertEqual(viewControllerSpy.password, "623464")
    }
    
    func testDisplayTermsOfService_ShouldDisplayTermsOfServiceView() {
        sut.displayTermsOfService()
        
        XCTAssertEqual(viewControllerSpy.callDisplayTermsOfServiceCount, 1)
    }
    
    func testUpdatePasswordVisibility_WhenParameterIsFalse_ShouldSetItNotVisible() throws {
        sut.updatePasswordVisibility(isVisible: false)
        
        XCTAssertEqual(viewControllerSpy.callUpdatePasswordVisibilityCount, 1)
        XCTAssertFalse(try XCTUnwrap(viewControllerSpy.isPasswordVisible))
    }
    
    func testUpdatePasswordVisibility_WhenParameterIsTrue_ShouldSetItVisible() throws {
        sut.updatePasswordVisibility(isVisible: true)
        
        XCTAssertEqual(viewControllerSpy.callUpdatePasswordVisibilityCount, 1)
        XCTAssert(try XCTUnwrap(viewControllerSpy.isPasswordVisible))
    }
    
    func testDidNextStep_WhenActionIsMoveToNextStep_ShouldMoveToNextStep() {
        let password = "56745685"
        
        sut.didNextStep(action: .moveToNextStep(withPassword: password))
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssert(coordinatorSpy.moveToNextStepAction)
        XCTAssertEqual(coordinatorSpy.passwordToNextStep, password)
    }
    
    func testDidNextStep_WhenActionIsOpenWebview_ShouldOpenWebview() throws {
        let url = try XCTUnwrap(URL(string: "www.picpay.com"))
        
        sut.didNextStep(action: .webview(url: url))
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssert(coordinatorSpy.webviewAction)
        XCTAssertEqual(coordinatorSpy.webviewUrl, url)
    }
    
    func testDidNextStep_WhenActionIsAlreadyRegistered_ShouldMaketheAlreadyRegisteredAction() throws {
        sut.didNextStep(action: .alreadyRegistered)
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssert(coordinatorSpy.alreadyRegisteredAction)
    }
    
    func testDidNextStep_WhenActionIsBack_ShouldMakeBackAction() {
        sut.didNextStep(action: .back)
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertTrue(coordinatorSpy.backAction)
    }
    
    func testDisplayError_ShouldDisplayErrorMessage() {
        let errorMessage = "Some error"
        
        sut.displayError(message: errorMessage)
        
        XCTAssertEqual(viewControllerSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(viewControllerSpy.errorMessage, errorMessage)
    }
    
    func testHideErrors_ShouldHideErrorMessages() {
        sut.hideErrors()
        
        XCTAssertEqual(viewControllerSpy.callHideErrorsCount, 1)
    }
    
    func testShowLoading_ShouldShowLoadingAnimation() {
        sut.showLoading()
        
        XCTAssertEqual(viewControllerSpy.callShowLoadingCount, 1)
    }
    
    func testHideLoading_ShouldHideLoadingAnimation() {
        sut.hideLoading()
        
        XCTAssertEqual(viewControllerSpy.callHideLoadingCount, 1)
    }
    
    func testDismissKeyboard_ShouldDismissKeyboard() {
        sut.dismissKeyboard()
        
        XCTAssertEqual(viewControllerSpy.callDismissKeyboardCount, 1)
    }
}

