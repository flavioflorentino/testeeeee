@testable import PicPay
import Core
import XCTest

private final class RegistrationPasswordPresenterSpy: RegistrationPasswordPresenting {
    var viewController: RegistrationPasswordDisplay?
    
    private(set) var callFillPasswordTextFieldCount = 0
    private(set) var passwordInTextField: String?
    private(set) var callDisplayTermsOfServiceCount = 0
    private(set) var callUpdatePasswordVisibilityCount = 0
    private(set) var isPasswordVisible: Bool?
    private(set) var callDisplayInputErrorCount = 0
    private(set) var callDisplayErrorCount = 0
    private(set) var errorMessage: String?
    private(set) var callHideErrorsCount = 0
    private(set) var callShowLoadingCount = 0
    private(set) var callHideLoadingCount = 0
    private(set) var callDismissKeyboardCount = 0
    private(set) var callDidNextStepCount = 0
    private(set) var moveToNextStepAction = false
    private(set) var passwordToNextStep: String?
    private(set) var alreadyRegisteredAction = false
    private(set) var webviewAction = false
    private(set) var webviewUrl: URL?
    private(set) var backAction = false
    
    func fillPasswordTextField(with password: String) {
        passwordInTextField = password
        callFillPasswordTextFieldCount += 1
    }
    
    func displayTermsOfService() {
        callDisplayTermsOfServiceCount += 1
    }
    
    func updatePasswordVisibility(isVisible: Bool) {
        callUpdatePasswordVisibilityCount += 1
        isPasswordVisible = isVisible
    }
    
    func didNextStep(action: RegistrationPasswordAction) {
        callDidNextStepCount += 1
        switch action {
        case .back:
            backAction = true
        case .moveToNextStep(let password):
            moveToNextStepAction = true
            passwordToNextStep = password
        case .alreadyRegistered:
            alreadyRegisteredAction = true
        case .webview(let url):
            webviewAction = true
            webviewUrl = url
        }
    }
    
    func displayInputError() {
        callDisplayInputErrorCount += 1
    }
    
    func displayError(message: String) {
        errorMessage = message
        callDisplayErrorCount += 1
    }
    
    func hideErrors() {
        callHideErrorsCount += 1
    }
    
    func showLoading() {
        callShowLoadingCount += 1
    }
    
    func hideLoading() {
        callHideLoadingCount += 1
    }
    
    func dismissKeyboard() {
        callDismissKeyboardCount += 1
    }
}

private final class RegistrationPasswordServiceMock: RegistrationPasswordServicing {
    private(set) var callValidatePasswordCount = 0
    
    var result: Result<Void, ApiError> = Result.success
    
    func validatePassword(_ password: String, completion: @escaping (Result<Void, ApiError>) -> Void) {
        callValidatePasswordCount += 1
        completion(result)
    }
}

final class RegistrationPasswordViewModelTests: XCTestCase {
    private let serviceMock = RegistrationPasswordServiceMock()
    private let presenterSpy = RegistrationPasswordPresenterSpy()
    private lazy var sut = RegistrationPasswordViewModel(
        service: serviceMock,
        presenter: presenterSpy,
        password: nil,
        showTermsOfService: false
    )
    
    func testDisplayTermsOfServiceIfNeeded_WhenTermsOfServiceIsActive_ShouldDisplayTermsOfService() {
        sut = RegistrationPasswordViewModel(service: serviceMock, presenter: presenterSpy, password: nil, showTermsOfService: true)
        
        sut.displayTermsOfServiceIfNeeded()
        
        XCTAssertEqual(presenterSpy.callDisplayTermsOfServiceCount, 1)
    }
    
    func testDisplayTermsOfServiceIfNeeded_WhenTermsOfServiceIsNotActive_ShouldNotDisplayTermsOfService() {
        sut = RegistrationPasswordViewModel(service: serviceMock, presenter: presenterSpy, password: nil, showTermsOfService: false)
        
        sut.displayTermsOfServiceIfNeeded()
        
        XCTAssertEqual(presenterSpy.callDisplayTermsOfServiceCount, 0)
    }
    
    func testFillPasswordTextFieldIfNeeded_WhenInitiatedWithPassword_ShouldFillTextFieldWithThePassword() {
        sut = RegistrationPasswordViewModel(service: serviceMock, presenter: presenterSpy, password: "52313235", showTermsOfService: false)
        
        sut.fillPasswordTextFieldIfNeeded()
        
        XCTAssertEqual(presenterSpy.callFillPasswordTextFieldCount, 1)
        XCTAssertEqual(presenterSpy.passwordInTextField, "52313235")
    }
    
    func testFillPasswordTextFieldIfNeeded_WhenInitiatedWithoutPassword_ShouldNotChangeTextInTextField() {
        sut = RegistrationPasswordViewModel(service: serviceMock, presenter: presenterSpy, password: nil, showTermsOfService: false)
        
        sut.fillPasswordTextFieldIfNeeded()
        
        XCTAssertEqual(presenterSpy.callFillPasswordTextFieldCount, 0)
        XCTAssertNil(presenterSpy.passwordInTextField)
    }
    
    func testDidChangePasswordVisibility_WhenItWasVisible_ShouldSetItNotVisible() throws {
        sut.didChangePasswordVisibility(fromVisible: true)
        
        XCTAssertEqual(presenterSpy.callUpdatePasswordVisibilityCount, 1)
        XCTAssertFalse(try XCTUnwrap(presenterSpy.isPasswordVisible))
    }
    
    func testDidChangePasswordVisibility_WhenItWasNotVisible_ShouldSetItVisible() throws {
        sut.didChangePasswordVisibility(fromVisible: false)
        
        XCTAssertEqual(presenterSpy.callUpdatePasswordVisibilityCount, 1)
        XCTAssert(try XCTUnwrap(presenterSpy.isPasswordVisible))
    }
    
    func testDidTApConfirmButton_WhenPasswordIsNil_ShouldDisplayInputError() {
        sut.updatePassword(with: nil)
        
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.callDisplayInputErrorCount, 1)
        XCTAssertEqual(serviceMock.callValidatePasswordCount, 0)
    }
    
    func testDidTApConfirmButton_WhenPasswordIsNotFormated_ShouldDisplayInputError() {
        sut.updatePassword(with: "325")
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.callDisplayInputErrorCount, 1)
        XCTAssertEqual(serviceMock.callValidatePasswordCount, 0)
    }
    
    func testDidTApConfirmButton_WhenPasswordIsFormatedAndValid_ShouldPresentNextStep() {
        serviceMock.result = Result.success
        
        sut.updatePassword(with: "1235")
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.callDisplayInputErrorCount, 0)
        XCTAssertEqual(serviceMock.callValidatePasswordCount, 1)
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssert(presenterSpy.moveToNextStepAction)
        XCTAssertEqual(presenterSpy.passwordToNextStep, "1235")
    }
    
    func testDidTApConfirmButton_WhenPasswordIsFormatedButNotValid_ShouldDisplayErrorFromBackend() {
        let someErrorMessage = "Error message"
        var requestError = RequestError()
        requestError.message = someErrorMessage
        serviceMock.result = Result.failure(ApiError.badRequest(body: requestError))
        
        sut.updatePassword(with: "743646")
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.callDisplayInputErrorCount, 0)
        XCTAssertEqual(serviceMock.callValidatePasswordCount, 1)
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 0)
        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, someErrorMessage)
    }
    
    func testDidTApConfirmButton_WhenPasswordIsFormatedButApiCallFailWithConnectionError_ShouldDisplayError() {
        serviceMock.result = Result.failure(ApiError.connectionFailure)
        
        sut.updatePassword(with: "756885")
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.callDisplayInputErrorCount, 0)
        XCTAssertEqual(serviceMock.callValidatePasswordCount, 1)
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 0)
        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, DefaultLocalizable.errorConnectionViewSubtitle.text)
    }
    
    func testDidTApConfirmButton_WhenPasswordIsFormatedButApiCallFailWithGenericError_ShouldDisplayError() {
        serviceMock.result = Result.failure(ApiError.timeout)
        
        sut.updatePassword(with: "890678")
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.callDisplayInputErrorCount, 0)
        XCTAssertEqual(serviceMock.callValidatePasswordCount, 1)
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 0)
        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, DefaultLocalizable.requestError.text)
    }
    
    func testDidTapAlreadyRegisteredButton_ShouldPerformTheAlreadyRegisteredAction() {
        sut.didAlreadyRegistered()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssert(presenterSpy.alreadyRegisteredAction)
    }
    
    func testDidInteractWithValidUrl_ShouldOpenWebview() throws {
        let url = try XCTUnwrap(URL(string: "api/terms"))
        
        sut.didInteractWithUrl(url)
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssert(presenterSpy.webviewAction)
        let formatedUrlString = try XCTUnwrap(WebServiceInterface.apiEndpoint("api/terms"))
        
        XCTAssertEqual(presenterSpy.webviewUrl?.absoluteString, formatedUrlString)
    }
    
    func testDidTapBack_ShouldPerformBackAction() {
        sut.didTapBack()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertTrue(presenterSpy.backAction)
    }
}


