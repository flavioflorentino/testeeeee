@testable import PicPay
import XCTest

private final class RegistrationFlowCoordinatorSpy: RegistrationPasswordDelegate {
    private(set) var callMoveToNextStepCount = 0
    private(set) var passwordToNextStep: String?
    private(set) var callMoveToLoginCount = 0
    private(set) var callMoveToPreviousRegistrationStep = 0
    
    func moveToNextStep(withPassword password: String) {
        passwordToNextStep = password
        callMoveToNextStepCount += 1
    }
    
    func moveToLogin() {
        callMoveToLoginCount += 1
    }
    
    func moveToPreviousRegistrationStep() {
        callMoveToPreviousRegistrationStep += 1
    }
}

final class RegistrationPasswordCoordinatorTests: XCTestCase {
    private let flowCoordinator = RegistrationFlowCoordinatorSpy()
    private let navController = NavigationControllerMock(rootViewController: UIViewController())
    private lazy var sut: RegistrationPasswordCoordinating = {
        let coordinator = RegistrationPasswordCoordinator()
        coordinator.viewController = navController.topViewController
        coordinator.delegate = flowCoordinator
        return coordinator
    }()
    
    func testPerform_WhenActionIsMoveToNextStep_ShouldMoveToNextStep() {
        let password = "3452446"
        
        sut.perform(action: .moveToNextStep(withPassword: password))
        
        XCTAssertEqual(flowCoordinator.callMoveToNextStepCount, 1)
        XCTAssertEqual(flowCoordinator.passwordToNextStep, password)
    }
    
    func testPerform_WhenActionIsAlreadyRegistered_ShouldMoveBackToStartScreen() {
        sut.perform(action: .alreadyRegistered)
        
        XCTAssertEqual(flowCoordinator.callMoveToLoginCount, 1)
    }
    
    func testPerform_WhenActionIsWebview_ShouldOpenWebview() throws {
        let url = try XCTUnwrap(URL(string: "www.picpay.com"))
        
        sut.perform(action: .webview(url: url))
        
        XCTAssertTrue(navController.isPresentViewControllerCalled)
        let viewControllerPresented = try XCTUnwrap((navController.viewControllerPresented as? UINavigationController)?.topViewController)
        XCTAssertTrue(viewControllerPresented.isKind(of: WebViewController.self))
    }
    
    func testPerform_WhenActionIsBack_ShouldBackView() {
        sut.perform(action: .back)
        
        XCTAssertEqual(flowCoordinator.callMoveToPreviousRegistrationStep, 1)
    }
}
