@testable import PicPay
import UI
import XCTest

final class RegistrationNamePresenterTests: XCTestCase {
    private let coordinatorSpy = SpyRegistrationNameCoordinator()
    private let viewControllerSpy = SpyRegistrationNameViewController()
    
    private lazy var sut: RegistrationNamePresenter = {
        let presenter = RegistrationNamePresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPresentTermsOfService_WhenCalledFromViewModel_ShouldShowTermsOfService() {
        sut.presentTermsOfService()
        
        XCTAssertEqual(viewControllerSpy.displayTermsOfServiceCount, 1)
    }
    
    func testFillTextFields_WhenCalledFromViewModel_ShouldFillFirstNameInTextFieldAndLastNameInTextField() {
        sut.fillTextFields(with: "FirstName", lastName: "LastName")
        
        XCTAssertEqual(viewControllerSpy.fillTextFieldsCount, 1)
        XCTAssertEqual(viewControllerSpy.fillTextFieldsPassed, "FirstName LastName")
    }
    
    func testShowDefaultTitle_WhenViewDidLoadIsCalled_ShouldReturnDefaultTitle() {
        sut.showDefaultTitle()
        
        XCTAssertEqual(viewControllerSpy.setTitleCount, 1)
        XCTAssertEqual(viewControllerSpy.setTitlePassed, NSAttributedString(string: RegisterLocalizable.nameStepTitle.text))
    }
    
    func testBuildTitle_WhenStringIsNotEmpty_ShouldReturnCustomTitle() {
        let name = "FirstName LastName"
        let title = NSMutableAttributedString(string: "Oi, \(name)!")
        let titleColor = Palette.ppColorBranding300.color
        let titleColorRange = (title.string as NSString).range(of: name)
        var titleColorAssert = false
        title.textColor(text: name, color: titleColor)
        
        sut.buildTitle(name)
        
        XCTAssertEqual(viewControllerSpy.setTitleCount, 1)
        XCTAssertEqual(viewControllerSpy.setTitlePassed?.string, title.string)
        
        viewControllerSpy.setTitlePassed?.enumerateAttribute(NSAttributedString.Key.foregroundColor, in: titleColorRange) { value, range, stop in
            if let foregroundColor = value as? UIColor {
                titleColorAssert = titleColor.cgColor == foregroundColor.cgColor
            }
        }
        
        XCTAssertTrue(titleColorAssert)
    }
    
    func testShowLoadingAnimation_WhenTheFunctionIsCalled_ShouldShowLoader() {
        sut.showLoadingAnimation()
        
        XCTAssertEqual(viewControllerSpy.showLoadingAnimationCount, 1)
    }
    
    func testHideLoadingAnimation_WhenTheFunctionIsCalled_ShouldHideLoader() {
        sut.hideLoadingAnimation()
        
        XCTAssertEqual(viewControllerSpy.hideLoadingAnimationCount, 1)
    }
    
    func testPresentErrorMessage_WhenFirstNameAndLastNameIsInvalid_ShouldPresentError() throws {
        sut.presentError("Error", isFirstNameError: true, isLastNameError: true)
        
        XCTAssertEqual(viewControllerSpy.displayErrorMessageCount, 1)
        XCTAssertEqual(viewControllerSpy.displayErrorMessagePassed, "Error")
        XCTAssertTrue(try XCTUnwrap(viewControllerSpy.isFirstNameErrorPassed))
        XCTAssertTrue(try XCTUnwrap(viewControllerSpy.isLastNameErrorPassed))
    }
    
    func testPresentErrorMessage_WhenFirstNameIsInvalid_ShouldPresentError() throws {
        sut.presentError("Error", isFirstNameError: true, isLastNameError: false)
        
        XCTAssertEqual(viewControllerSpy.displayErrorMessageCount, 1)
        XCTAssertEqual(viewControllerSpy.displayErrorMessagePassed, "Error")
        XCTAssertTrue(try XCTUnwrap(viewControllerSpy.isFirstNameErrorPassed))
        XCTAssertFalse(try XCTUnwrap(viewControllerSpy.isLastNameErrorPassed))
    }
    
    func testPresentErrorMessage_WhenLastNameIsInvalid_ShouldPresentError() throws {
        sut.presentError("Error", isFirstNameError: false, isLastNameError: true)
        
        XCTAssertEqual(viewControllerSpy.displayErrorMessageCount, 1)
        XCTAssertEqual(viewControllerSpy.displayErrorMessagePassed, "Error")
        XCTAssertFalse(try XCTUnwrap(viewControllerSpy.isFirstNameErrorPassed))
        XCTAssertTrue(try XCTUnwrap(viewControllerSpy.isLastNameErrorPassed))
    }
    
    func testHideErrorMessage_WhenTheFunctionIsCalled_ShouldHideError() {
        sut.hideErrorMessage()
        
        XCTAssertEqual(viewControllerSpy.hideErrorMessageCount, 1)
    }
    
    func testDidNextStepWithActionMoveToNextStep_WhenCalledFromViewModel_ShouldPassAnActionToCoordinator() {
        sut.didNextStep(action: .moveToNextStep(withFirstName: "FirstName", lastName: "LastName"))
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.actionPassed, RegistrationNameAction.moveToNextStep(withFirstName: "FirstName", lastName: "LastName"))
    }
    
    func testDidNextStep_WhendCallNextStepAction_ShouldMoveToNextStepActionIsPassed() {
        let firstName = "João"
        let lastName = "Silva"
        sut.didNextStep(action: .moveToNextStep(withFirstName: firstName, lastName: lastName))
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.firstName, firstName)
        XCTAssertEqual(coordinatorSpy.lastName, lastName)
    }
    
    func testDidNextStep_WhenMoveToLogin_ShouldMoveToLoginActionIsPassed() {
        sut.didNextStep(action: .moveToLogin)
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertTrue(coordinatorSpy.moveToLoginCalled)
    }
    
    func testDidNextStep_WhendOpenWebview_ShouldMoveToLoginActionIsPassed() throws {
        let url = try XCTUnwrap(URL(string: "www.picpay.com"))
        sut.didNextStep(action: .webview(url: url))
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.webviewUrl, url)
    }
    
    func testDidNextStepWithActionMoveToLogin_WhenCalledFromViewModel_ShouldPassAnActionToCoordinator() {
        sut.didNextStep(action: .moveToLogin)
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.actionPassed, RegistrationNameAction.moveToLogin)
    }
    
    func testDidNextStepWithActionWebview_WhenCalledFromViewModel_ShouldPassAnActionToCoordinator() throws {
        let url = try URL(string: "https://picpay.com").safe()
        sut.didNextStep(action: .webview(url: url))
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.actionPassed, RegistrationNameAction.webview(url: url))
    }
    
    func testDidNextStepWithActionBack_WhenCalledFromViewModel_ShouldPassAnActionToCoordinator() {
        sut.didNextStep(action: .back)
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertTrue(coordinatorSpy.backCalled)
    }
}

final class SpyRegistrationNameCoordinator: RegistrationNameCoordinating {
    var delegate: RegistrationNameDelegate?
    
    weak var viewController: UIViewController?
    private(set) var callPerformCount = 0
    private(set) var moveToLoginCalled = false
    private(set) var firstName: String?
    private(set) var lastName: String?
    private(set) var webviewUrl: URL?
    private(set) var actionPassed: RegistrationNameAction?
    private(set) var backCalled = false
    
    
    func perform(action: RegistrationNameAction) {
        callPerformCount += 1
        actionPassed = action
        
        switch action {
        case .back:
            backCalled = true
        case .moveToLogin:
            moveToLoginCalled = true
        case let .moveToNextStep(firstName, lastName):
            self.firstName = firstName
            self.lastName = lastName
        case let .webview(url):
            webviewUrl = url
        }
    }
}

private final class SpyRegistrationNameViewController: RegistrationNameDisplay {
    private(set) var fillTextFieldsCount = 0
    private(set) var fillTextFieldsPassed: String?
    private(set) var displayTermsOfServiceCount = 0
    private(set) var setTitleCount = 0
    private(set) var setTitlePassed: NSAttributedString?
    private(set) var showLoadingAnimationCount = 0
    private(set) var hideLoadingAnimationCount = 0
    private(set) var displayErrorMessagePassed: String?
    private(set) var displayErrorMessageCount = 0
    private(set) var isFirstNameErrorPassed: Bool?
    private(set) var isLastNameErrorPassed: Bool?
    private(set) var hideErrorMessageCount = 0
    
    func displayTermsOfService() {
        displayTermsOfServiceCount += 1
    }
    
    func fillTextFields(with firstName: String, lastName: String) {
        fillTextFieldsCount += 1
        fillTextFieldsPassed = "\(firstName) \(lastName)"
    }
    
    func setTitle(_ title: NSAttributedString) {
        setTitleCount += 1
        setTitlePassed = title
    }
    
    func showLoading() {
        showLoadingAnimationCount += 1
    }
    
    func hideLoading() {
        hideLoadingAnimationCount += 1
    }
    
    func displayErrorMessage(_ message: String) {
        displayErrorMessageCount += 1
        displayErrorMessagePassed = message
    }
    
    func displayError(_ message: String, isFirstNameError: Bool, isLastNameError: Bool) {
        displayErrorMessageCount += 1
        displayErrorMessagePassed = message
        isFirstNameErrorPassed = isFirstNameError
        isLastNameErrorPassed = isLastNameError
    }
    
    func hideErrorMessage() {
        hideErrorMessageCount += 1
    }
}
