@testable import PicPay

extension RegistrationNameAction: Equatable {
    public static func ==(lhs: RegistrationNameAction, rhs: RegistrationNameAction) -> Bool {
        switch (lhs, rhs) {
        case (.moveToLogin, .moveToLogin):
            return true
        case let (.moveToNextStep(firstNameLhs, lastNameLhs), .moveToNextStep(firstNameRhs, lastNameRhs)):
            return firstNameLhs == firstNameRhs && lastNameLhs == lastNameRhs
        case let (.webview(urlLhs), .webview(urlRhs)):
            return urlLhs == urlRhs
        default:
            return false
        }
    }
}
