@testable import PicPay
import XCTest
import Core

final class RegistrationNameViewModelTests: XCTestCase {
    private let serviceMock = RegistrationNameServiceMock()
    private let presenterSpy = RegistrationNamePresenterSpy()
    private lazy var sut = RegistrationNameViewModel(service: serviceMock, presenter: presenterSpy, showTermsOfService: false)
    
    func testLoadInformations_WhenShowTermsOfServiceIsTrue_ShouldDisplayTermsOfService() {
        sut = RegistrationNameViewModel(service: serviceMock, presenter: presenterSpy, showTermsOfService: true)
        
        sut.loadInformations()
        
        XCTAssertEqual(presenterSpy.presentTermsOfServiceCount, 1)
    }
    
    func testFillTextFieldsIfNeeded_WhenFirstNameAndLastNameIsNotEmpty_ShouldfillName() {
        sut = RegistrationNameViewModel(service: serviceMock, presenter: presenterSpy, firstName: "FirstName", lastName: "LastName", showTermsOfService: false)
        
        sut.fillTextFieldsIfNeeded()
        
        XCTAssertEqual(presenterSpy.fillTextFieldsCount, 1)
        XCTAssertEqual(presenterSpy.fillTextFieldsPassed, "FirstName LastName")
    }
    
    func testLoadInformations_WhenBuildTitle_ShouldReturnDefaultTitle() {
        sut.loadInformations()
        
        XCTAssertEqual(presenterSpy.defaulTitleCount, 1)
    }
    
    func testUpdateFirstName_WhenStringIsNotEmpty_ShouldReturnFirstName() {
        sut.updateFirstName("FirstName")
        
        XCTAssertEqual(presenterSpy.buildTitleCount, 1)
        XCTAssertEqual(presenterSpy.buildTitlePassed, "FirstName")
    }
    
    func testUpdateLastName_WhenStringIsNotEmpty_ShouldReturnLastName() {
        sut.updateLastName("LastName")
        
        XCTAssertEqual(presenterSpy.buildTitleCount, 1)
        XCTAssertEqual(presenterSpy.buildTitlePassed, "LastName")
    }
    
    func testUpdateFirstNameAndUpdateLastName_WhenStringIsNotEmpty_ShouldReturnFirstNameAndLastName() {
        sut.updateFirstName("FirstName")
        sut.updateLastName("LastName")
        
        XCTAssertEqual(presenterSpy.buildTitleCount, 2)
        XCTAssertEqual(presenterSpy.buildTitlePassed, "FirstName LastName")
    }
    
    func testUpdateFirstNameAndUpdateLastName_WhenStringIsEmpty_ShouldReturnDefaultTime() {
        sut.updateFirstName("")
        sut.updateLastName("")
        
        XCTAssertEqual(presenterSpy.buildTitleCount, 0)
        XCTAssertEqual(presenterSpy.defaulTitleCount, 2)
    }
    
    func testVerifyName_WhenFirstNameAndLastNameIsInvalid_ShouldReturnAnError() throws {
        sut.updateFirstName("")
        sut.updateLastName("")
        sut.verifyName()
        
        XCTAssertEqual(presenterSpy.presentErrorMessageCount, 1)
        XCTAssertEqual(presenterSpy.messagePassed, RegisterLocalizable.nameStepError.text)
        XCTAssertTrue(try XCTUnwrap(presenterSpy.isFirstNameErrorPassed))
        XCTAssertTrue(try XCTUnwrap(presenterSpy.isLastNameErrorPassed))
    }
    
    func testVerifyName_WhenFirstNameIsInvalid_ShouldReturnAnError() throws {
        sut.updateFirstName("")
        sut.updateLastName("LastName")
        sut.verifyName()
        
        XCTAssertEqual(presenterSpy.presentErrorMessageCount, 1)
        XCTAssertEqual(presenterSpy.messagePassed, RegisterLocalizable.nameStepMustContainAtLeast.text)
        XCTAssertTrue(try XCTUnwrap(presenterSpy.isFirstNameErrorPassed))
        XCTAssertFalse(try XCTUnwrap(presenterSpy.isLastNameErrorPassed))
    }
    
    func testVerifyName_WhenLastNameIsInvalid_ShouldReturnAnError() throws {
        sut.updateFirstName("FirstName")
        sut.updateLastName("")
        sut.verifyName()
        
        XCTAssertEqual(presenterSpy.presentErrorMessageCount, 1)
        XCTAssertEqual(presenterSpy.messagePassed, RegisterLocalizable.lastNameStepMustContainAtLeast.text)
        XCTAssertFalse(try XCTUnwrap(presenterSpy.isFirstNameErrorPassed))
        XCTAssertTrue(try XCTUnwrap(presenterSpy.isLastNameErrorPassed))
    }
    
    func testVerifyName_WhenServiceResponseItWasNotASuccess_ShouldReturnAnError() {
        serviceMock.isSuccess = false
        
        sut.updateFirstName("FirstName")
        sut.updateLastName("LastName")
        sut.verifyName()
        
        XCTAssertEqual(presenterSpy.presentErrorMessageCount, 1)
        XCTAssertEqual(presenterSpy.messagePassed, "Error")
    }
    
    func testVerifyName_WhenServiceResponseItWasSuccess_ShouldReturnCallNextStep() {
        serviceMock.isSuccess = true
        
        sut.updateFirstName("FirstName")
        sut.updateLastName("LastName")
        sut.verifyName()
        
        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
    }
    
    func testVerifyName_WhenThereIsNoError_ShouldHideErrorMessage() {
        serviceMock.isSuccess = true
        
        sut.updateFirstName("FirstName")
        sut.updateLastName("LastName")
        sut.verifyName()
        
        XCTAssertEqual(presenterSpy.hideErrorMessageCount, 1)
    }
    
    func testVerifyName_WhenRequestInProgress_ShouldShowLoadingAnimation() {
        sut.updateFirstName("FirstName")
        sut.updateLastName("LastName")
        sut.verifyName()
        
        XCTAssertEqual(presenterSpy.showLoadingAnimationCount, 1)
    }
    
    func testVerifyName_WhenRequestIsFinished_ShouldShowLoadingAnimation() {
        sut.updateFirstName("FirstName")
        sut.updateLastName("LastName")
        sut.verifyName()
        
        XCTAssertEqual(presenterSpy.hideLoadingAnimationCount, 1)
    }
    
    func testDidInteractWithUrl_WhenURLISValid_ShouldOpenWebview() throws {
        let url = try URL(string: "api/terms").safe()
        let expectedURL = try URL(string: WebServiceInterface.apiEndpoint(url.absoluteString)).safe()
        
        sut.didInteractWithUrl(url)
        
        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepPassed, RegistrationNameAction.webview(url: expectedURL))
    }
}

private final class RegistrationNameServiceMock: RegistrationNameServicing {
    var isSuccess = false
    
    func verifyName(_ name: String, completion: @escaping (ApiError?) -> Void) {
        guard isSuccess else {
            var requestError = RequestError()
            requestError.message = "Error"
            completion(ApiError.badRequest(body: requestError))
            return
        }
        
        completion(nil)
    }
}

private final class RegistrationNamePresenterSpy: RegistrationNamePresenting {
    var viewController: RegistrationNameDisplay?
    private(set) var fillTextFieldsCount = 0
    private(set) var fillTextFieldsPassed: String?
    private(set) var presentTermsOfServiceCount = 0
    private(set) var defaulTitleCount = 0
    private(set) var buildTitleCount = 0
    private(set) var buildTitlePassed: String?
    private(set) var showLoadingAnimationCount = 0
    private(set) var hideLoadingAnimationCount = 0
    private(set) var presentErrorMessageCount = 0
    private(set) var isFirstNameErrorPassed: Bool?
    private(set) var isLastNameErrorPassed: Bool?
    private(set) var hideErrorMessageCount = 0
    private(set) var messagePassed: String?
    private(set) var didNextStepCount = 0
    private(set) var didNextStepPassed: RegistrationNameAction?
    
    func fillTextFields(with firstName: String, lastName: String) {
        fillTextFieldsCount += 1
        fillTextFieldsPassed = "\(firstName) \(lastName)"
    }
    
    func presentTermsOfService() {
       presentTermsOfServiceCount += 1
    }
    
    func showDefaultTitle() {
        defaulTitleCount += 1
    }
    
    func buildTitle(_ message: String) {
        buildTitleCount += 1
        buildTitlePassed = message
    }
    
    func showLoadingAnimation() {
        showLoadingAnimationCount += 1
    }
    
    func hideLoadingAnimation() {
        hideLoadingAnimationCount += 1
    }
    
    func presentError(_ message: String, isFirstNameError: Bool, isLastNameError: Bool) {
        presentErrorMessageCount += 1
        messagePassed = message
        isFirstNameErrorPassed = isFirstNameError
        isLastNameErrorPassed = isLastNameError
    }
    
    func hideErrorMessage() {
        hideErrorMessageCount += 1
    }
    
    func didNextStep(action: RegistrationNameAction) {
        didNextStepCount += 1
        didNextStepPassed = action
    }
}
