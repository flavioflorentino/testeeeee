@testable import PicPay
import Core
import XCTest

private final class RegistrationDocumentPresenterSpy: RegistrationDocumentPresenting {
   
    var viewController: RegistrationDocumentDisplay?
    
    private(set) var callDisplayTermsOfServiceCount = 0
    private(set) var callUpdateTextsCount = 0
    private(set) var textsModel: DocumentFeature?
    
    private(set) var callFillTextFieldsCount = 0
    private(set) var cpfInForm: String?
    private(set) var dateOfBirthInForm: String?
    
    private(set) var callDisplayErrorCount = 0
    private(set) var errorMessage: String?
    private(set) var isCpfError = false
    private(set) var isDateOfBirthError = false
    
    private(set) var callDisplayDocumentAlreadyRegisteredAlertCount = 0
    
    private(set) var callHideErrorsCount = 0
    private(set) var callShowLoadingCount = 0
    private(set) var callHideLoadingCount = 0
    private(set) var callDismissKeyboardCount = 0
    private(set) var showAlertMinimumAgeCallsCount = 0
    
    private(set) var callShowDocumentHelpCount = 0
    private(set) var helpTitle: String?
    private(set) var helpMessage: String?
    private(set) var helpButtonTitle: String?

    private(set) var callDidNextStepCount = 0
    private(set) var moveToNextStepAction = false
    private(set) var cpfToNextStep: String?
    private(set) var dateOfBirthToNextStep: String?
    private(set) var alreadyRegisteredAction = false
    private(set) var invalidCpfCounter = 0
    private(set) var copyPasteCpf = false
    private(set) var webviewAction = false
    private(set) var webviewUrl: URL?
    private(set) var backAction = false

    func displayTermsOfService() {
        callDisplayTermsOfServiceCount += 1
    }
    
    func updateTexts(_ textsModel: DocumentFeature) {
        callUpdateTextsCount += 1
        self.textsModel = textsModel
    }
    
    func fillTextFields(withCpf cpf: String, dateOfBirth: String) {
        callFillTextFieldsCount += 1
        cpfInForm = cpf
        dateOfBirthInForm = dateOfBirth
    }
    
    func displayError(message: String, isCpfError: Bool, isDateOfBirthError: Bool) {
        errorMessage = message
        self.isCpfError = isCpfError
        self.isDateOfBirthError = isDateOfBirthError
        callDisplayErrorCount += 1
    }
    
    func displayDocumentAlreadyRegisteredAlert() {
        callDisplayDocumentAlreadyRegisteredAlertCount += 1
    }
    
    func showDocumentHelp(title: String, message: String, buttonTitle: String) {
        callShowDocumentHelpCount += 1
        helpTitle = title
        helpMessage = message
        helpButtonTitle = buttonTitle
    }
    
    func hideErrors() {
        callHideErrorsCount += 1
    }

    func showLoading() {
        callShowLoadingCount += 1
    }

    func hideLoading() {
        callHideLoadingCount += 1
    }

    func dismissKeyboard() {
        callDismissKeyboardCount += 1
    }
    
    func showAlertMinimumAge() {
        showAlertMinimumAgeCallsCount += 1
    }
    
    func didNextStep(action: RegistrationDocumentAction) {
        callDidNextStepCount += 1
        switch action {
        case .back:
            backAction = true
        case let .moveToNextStep(cpf, dateOfBirth, invalidCpfCounter, copyPasteCpf):
            moveToNextStepAction = true
            cpfToNextStep = cpf
            dateOfBirthToNextStep = dateOfBirth
            self.invalidCpfCounter = invalidCpfCounter
            self.copyPasteCpf = copyPasteCpf
        case .alreadyRegistered:
            alreadyRegisteredAction = true
        case .webview(let url):
            webviewAction = true
            webviewUrl = url
        }
    }
}

private final class RegistrationDocumentServiceMock: RegistrationDocumentServicing {
    private(set) var callValidateDocumentCount = 0
    
    var result: Result<Void, ApiError> = Result.success
    
    var documentInUseDialog: Bool = false
    
    var documentInUseFaqURL: String  = ""
    
    func validateDocument(cpf: String, dateOfBirth: String, completion: @escaping (Result<Void, ApiError>) -> Void) {
        callValidateDocumentCount += 1
        completion(result)
    }
}

final class RegistrationDocumentViewModelTests: XCTestCase {
    private let serviceMock = RegistrationDocumentServiceMock()
    private let presenterSpy = RegistrationDocumentPresenterSpy()
    private lazy var sut: RegistrationDocumentViewModel = {
        let model = RegistrationDocumentModel(cpf: nil, dateOfBirth: nil, name: nil, showTermsOfService: false)
        return RegistrationDocumentViewModel(service: serviceMock, presenter: presenterSpy, model: model)
    }()
    
    func testDisplayTermsOfServiceIfNeeded_WhenTermsOfServiceIsActive_ShouldDisplayTermsOfService() {
        let model = RegistrationDocumentModel(cpf: nil, dateOfBirth: nil, name: nil, showTermsOfService: true)
        sut = RegistrationDocumentViewModel(service: serviceMock, presenter: presenterSpy, model: model)

        sut.displayTermsOfServiceIfNeeded()

        XCTAssertEqual(presenterSpy.callDisplayTermsOfServiceCount, 1)
    }

    func testDisplayTermsOfServiceIfNeeded_WhenTermsOfServiceIsNotActive_ShouldNotDisplayTermsOfService() {
        let model = RegistrationDocumentModel(cpf: nil, dateOfBirth: nil, name: nil, showTermsOfService: false)
        sut = RegistrationDocumentViewModel(service: serviceMock, presenter: presenterSpy, model: model)

        sut.displayTermsOfServiceIfNeeded()

        XCTAssertEqual(presenterSpy.callDisplayTermsOfServiceCount, 0)
    }

    func testSetupViewContent_WhenInitiatedWithCpfAndBirth_ShouldFillTextFieldsWithCpfAndBirth() {
        let model = RegistrationDocumentModel(cpf: "134.352.635-12", dateOfBirth: "10/10/1999", name: nil, showTermsOfService: false)
        sut = RegistrationDocumentViewModel(service: serviceMock, presenter: presenterSpy, model: model)
      
        sut.setupViewContent()

        XCTAssertEqual(presenterSpy.callFillTextFieldsCount, 1)
        XCTAssertEqual(presenterSpy.cpfInForm, "134.352.635-12")
        XCTAssertEqual(presenterSpy.dateOfBirthInForm, "10/10/1999")
    }

    func testSetupViewContent_WhenInitiatedWithoudCpfAndBirth_ShouldNotChangeTextInTextFields() {
        sut.setupViewContent()

        XCTAssertEqual(presenterSpy.callFillTextFieldsCount, 0)
        XCTAssertEqual(presenterSpy.cpfInForm, nil)
        XCTAssertEqual(presenterSpy.dateOfBirthInForm, nil)
    }
    
    func testSetupViewContent_ShouldUpdateScreen() {
        let model = RegistrationDocumentModel(cpf: nil, dateOfBirth: nil, name: "João", showTermsOfService: false)
        let sut = RegistrationDocumentViewModel(service: serviceMock, presenter: presenterSpy, model: model)

        sut.setupViewContent()

        XCTAssertEqual(presenterSpy.callUpdateTextsCount, 1)
        XCTAssertEqual(presenterSpy.textsModel?.title, RegisterLocalizable.documentStepTitle.text)
        XCTAssertEqual(presenterSpy.textsModel?.text, "Não se preocupe, João! Seus dados estão seguros conosco e são necessários para confirmar sua identidade.")
        XCTAssertEqual(presenterSpy.textsModel?.buttonTitle, DefaultLocalizable.learnMore.text)
    }

    func testDidConfirmButton_WhenCpfAndBirthAreEmpty_ShouldDisplayCpfAndBirthError() {
        sut.didConfirm()

        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, RegisterLocalizable.invalidCpfAndDateOfBirth.text)
        XCTAssert(presenterSpy.isCpfError)
        XCTAssert(presenterSpy.isDateOfBirthError)
        XCTAssertEqual(serviceMock.callValidateDocumentCount, 0)
    }
    
    func testDidConfirmButton_WhenCpfAndBirthHaveInvalidLength_ShouldDisplayCpfAndBirthError() {
        sut.updateDocument(withCpf: "110.356.87", dateOfBirth: "10/10/10")
        
        sut.didConfirm()

        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, RegisterLocalizable.invalidCpfAndDateOfBirth.text)
        XCTAssert(presenterSpy.isCpfError)
        XCTAssert(presenterSpy.isDateOfBirthError)
        XCTAssertEqual(serviceMock.callValidateDocumentCount, 0)
    }
    
    func testDidConfirmButton_WhenCpfHasInvalidLength_ShouldDisplayCpfError() {
        sut.updateDocument(withCpf: "110.356.87", dateOfBirth: "10/10/1988")
        
        sut.didConfirm()

        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, RegisterLocalizable.invalidCpf.text)
        XCTAssert(presenterSpy.isCpfError)
        XCTAssertFalse(presenterSpy.isDateOfBirthError)
        XCTAssertEqual(serviceMock.callValidateDocumentCount, 0)
    }
    
    func testDidConfirmButton_WhenDateOfBirthHasInvalidLength_ShouldDisplayDateOfBirthError() {
        sut.updateDocument(withCpf: "110.356.897-14", dateOfBirth: "10/10/98")
        
        sut.didConfirm()

        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, RegisterLocalizable.invalidDateOfBirth.text)
        XCTAssertFalse(presenterSpy.isCpfError)
        XCTAssert(presenterSpy.isDateOfBirthError)
        XCTAssertEqual(serviceMock.callValidateDocumentCount, 0)
    }
    
    func testDidConfirmButton_WhenBadApiRequest_ShouldDisplayApiError() {
        sut.updateDocument(withCpf: "110.356.897-14", dateOfBirth: "10/10/1998")
        var requestError = RequestError()
        requestError.message = "A random error message"
        serviceMock.result = Result.failure(ApiError.badRequest(body: requestError))
        
        sut.didConfirm()

        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, "A random error message")
        XCTAssert(presenterSpy.isCpfError)
        XCTAssert(presenterSpy.isDateOfBirthError)
        XCTAssertEqual(serviceMock.callValidateDocumentCount, 1)
    }
    
    func testDidConfirmButton_WhenConnectionFailure_ShouldDisplayApiError() {
        sut.updateDocument(withCpf: "110.356.897-14", dateOfBirth: "10/10/1998")
        serviceMock.result = Result.failure(ApiError.connectionFailure)
        
        sut.didConfirm()

        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, DefaultLocalizable.errorConnectionViewSubtitle.text)
        XCTAssert(presenterSpy.isCpfError)
        XCTAssert(presenterSpy.isDateOfBirthError)
        XCTAssertEqual(serviceMock.callValidateDocumentCount, 1)
    }
    
    func testDidConfirmButton_WhenApiUnknownError_ShouldDisplayApiError() {
        sut.updateDocument(withCpf: "110.356.897-14", dateOfBirth: "10/10/1998")
        serviceMock.result = Result.failure(ApiError.unknown(nil))
        
        sut.didConfirm()

        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(presenterSpy.errorMessage, DefaultLocalizable.requestError.text)
        XCTAssert(presenterSpy.isCpfError)
        XCTAssert(presenterSpy.isDateOfBirthError)
        XCTAssertEqual(serviceMock.callValidateDocumentCount, 1)
    }
    
    func testDidConfirmButton_WhenCpfAndBithAreValidLength_ShouldPresentNextStep() {
        sut.updateDocument(withCpf: "110.356.897-14", dateOfBirth: "10/10/1998")
        serviceMock.result = Result.success
        
        sut.didConfirm()

        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 0)
        XCTAssertEqual(presenterSpy.invalidCpfCounter, 0)
        XCTAssertFalse(presenterSpy.copyPasteCpf)
        XCTAssertEqual(serviceMock.callValidateDocumentCount, 1)
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssert(presenterSpy.moveToNextStepAction)
    }
    
    func testDidConfirmButton_WhenApiErrorWasDocumentAlreadyRegistredAndFeatureFlagWasActived_ShouldDisplayDocumentAlreadyRegisteredAlert() {
        sut.updateDocument(withCpf: "110.356.897-14", dateOfBirth: "10/10/1998")
        
        var requestError = RequestError()
        requestError.code = "5061"
        requestError.message = "O CPF informado já está cadastrado no PicPay."
        
        serviceMock.result = Result.failure(ApiError.badRequest(body: requestError))
        serviceMock.documentInUseDialog = true
        
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.callDisplayDocumentAlreadyRegisteredAlertCount, 1)
    }
    
    func testDidConfirmButton_WhenCpfAndBithAreValidLengthAndCpfWasCopyPasted_ShouldPresentNextStepWithCopyPastedTrue() {
        sut.updateDocument(withCpf: "110.356.897-14", dateOfBirth: "10/10/1998")
        serviceMock.result = Result.success
        
        sut.didCopyPasteCpf()
        sut.didConfirm()

        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 0)
        XCTAssertEqual(presenterSpy.invalidCpfCounter, 0)
        XCTAssert(presenterSpy.copyPasteCpf)
        XCTAssertEqual(serviceMock.callValidateDocumentCount, 1)
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssert(presenterSpy.moveToNextStepAction)
    }
    
    func testDidConfirmButton_WhenCpfAndBithAreValidLengthAndCpfWasInvalidated_ShouldPresentNextStepWithInvalidCpfCount() {
        sut.updateDocument(withCpf: "110.356.897-14", dateOfBirth: "10/10/1998")
        var requestError = RequestError()
        requestError.code = "5060"
        serviceMock.result = Result.failure(ApiError.badRequest(body: requestError))
        sut.didConfirm()
        serviceMock.result = Result.success

        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(presenterSpy.invalidCpfCounter,1)
        XCTAssertFalse(presenterSpy.copyPasteCpf)
        XCTAssertEqual(serviceMock.callValidateDocumentCount, 2)
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssert(presenterSpy.moveToNextStepAction)
    }
    
    func testDidTapAlreadyRegisteredButton_ShouldPerformTheAlreadyRegisteredAction() {
        sut.didAlreadyRegistered()

        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssert(presenterSpy.alreadyRegisteredAction)
    }

    func testDidInteractWithValidUrl_ShouldOpenWebview() throws {
        let url = try XCTUnwrap(URL(string: "api/terms"))

        sut.didInteractWithUrl(url)

        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssert(presenterSpy.webviewAction)
        let formatedUrlString = try XCTUnwrap(WebServiceInterface.apiEndpoint("api/terms"))

        XCTAssertEqual(presenterSpy.webviewUrl?.absoluteString, formatedUrlString)
    }
    
    func testDidLearnMore_ShouldPresentLearnMoreHelp() {
        sut.didLearnMore()
        
        XCTAssertEqual(presenterSpy.callShowDocumentHelpCount, 1)
        XCTAssertEqual(presenterSpy.helpTitle, RegisterLocalizable.alertDocumentHelpTitle.text)
        XCTAssertEqual(presenterSpy.helpMessage, RegisterLocalizable.alertDocumentHelpText.text)
        XCTAssertEqual(presenterSpy.helpButtonTitle, DefaultLocalizable.btOkUnderstood.text)
    }
    
    func testDidLogin_WhenCalledFromModal_ShouldPerformTheAlreadyRegisteredAction() {
        sut.didLogin()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssert(presenterSpy.alreadyRegisteredAction)
    }
    
    func testDidForgotPassword_WhenCalledFromModal_ShouldOpenWebview() {
        serviceMock.documentInUseFaqURL = "https://www.picpay.com"
        
        sut.didForgotPassword()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssert(presenterSpy.webviewAction)
        XCTAssertEqual(presenterSpy.webviewUrl?.absoluteString, "https://www.picpay.com")
    }
    
    func testDidTapBack_WhenCalledFromModal_ShouldPerformBackAction() {
        sut.didTapBack()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertTrue(presenterSpy.backAction)
    }
    
    
    func testDidConfirm_WhenUserAgeIsUnder16_ShouldShowMinimumAgeAlert() {
        let date = Calendar.current.date(byAdding: .year, value: -15, to: Date())
        let dateString = date?.stringFormatted() ?? "02/09/2020"
        sut.updateDocument(withCpf: "562.785.120-17", dateOfBirth: dateString)
        
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.showAlertMinimumAgeCallsCount, 1)
    }
    
    func testDidConfirm_WhenUserAgeIsOver16_ShouldNotShowMinimumAgeAlert() {
        let date = Calendar.current.date(byAdding: .year, value: -17, to: Date())
        let dateString = date?.stringFormatted() ?? "02/09/2004"
        sut.updateDocument(withCpf: "562.785.120-17", dateOfBirth: dateString)
        
        sut.didConfirm()
        
        XCTAssertEqual(presenterSpy.showAlertMinimumAgeCallsCount, 0)
    }
}
