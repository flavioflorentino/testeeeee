@testable import PicPay
import XCTest

private final class RegistrationFlowCoordinatorSpy: RegistrationDocumentDelegate {
    private(set) var callMoveToNextStepCount = 0
    private(set) var cpfToNextStep: String?
    private(set) var dateOfBirthToNextStep: String?
    private(set) var invalidCpfCounter = 0
    private(set) var copyPasteCpf = false
    private(set) var callMoveToLoginCount = 0
    private(set) var callMoveToPreviousRegistrationStep = 0
    
    
    func moveToNextStep(withCpf cpf: String, dateOfBirth: String, invalidCpfCounter: Int, copyPasteCpf: Bool) {
        cpfToNextStep = cpf
        dateOfBirthToNextStep = dateOfBirth
        self.invalidCpfCounter = invalidCpfCounter
        self.copyPasteCpf = copyPasteCpf
        callMoveToNextStepCount += 1
    }
    
    func moveToLogin() {
        callMoveToLoginCount += 1
    }
    
    func moveToPreviousRegistrationStep() {
        callMoveToPreviousRegistrationStep += 1
    }
}

final class RegistrationDocumentCoordinatorTests: XCTestCase {
    private let flowCoordinator = RegistrationFlowCoordinatorSpy()
    private let navController = NavigationControllerMock(rootViewController: UIViewController())
    private lazy var sut: RegistrationDocumentCoordinating = {
        let coordinator = RegistrationDocumentCoordinator()
        coordinator.viewController = navController.topViewController
        coordinator.delegate = flowCoordinator
        return coordinator
    }()
    
    func testPerform_WhenActionIsMoveToNextStep_ShouldMoveToNextStep() {
        let cpf = "123.634.675-56"
        let dateOfBirth = "04/06/1978"
        let invalidCpfCounter = Int.random(in: 0...100)
        let copyPasteCpf = true
        
        sut.perform(action: .moveToNextStep(withCpf: cpf, dateOfBirth: dateOfBirth, invalidCpfCounter: invalidCpfCounter, copyPasteCpf: copyPasteCpf))
        
        XCTAssertEqual(flowCoordinator.callMoveToNextStepCount, 1)
        XCTAssertEqual(flowCoordinator.cpfToNextStep, cpf)
        XCTAssertEqual(flowCoordinator.invalidCpfCounter, invalidCpfCounter)
        XCTAssertEqual(flowCoordinator.copyPasteCpf, copyPasteCpf)
        XCTAssertEqual(flowCoordinator.dateOfBirthToNextStep, dateOfBirth)
    }
    
    func testPerform_WhenActionIsAlreadyRegistered_ShouldMoveBackToStartScreen() {
        sut.perform(action: .alreadyRegistered)
        
        XCTAssertEqual(flowCoordinator.callMoveToLoginCount, 1)
    }
    
    func testPerform_WhenActionIsWebview_ShouldOpenWebview() throws {
        let url = try XCTUnwrap(URL(string: "www.picpay.com"))
        
        sut.perform(action: .webview(url: url))
        
        XCTAssertTrue(navController.isPresentViewControllerCalled)
        let viewControllerPresented = try XCTUnwrap((navController.viewControllerPresented as? UINavigationController)?.topViewController)
        XCTAssertTrue(viewControllerPresented.isKind(of: WebViewController.self))
    }
    
    func testPerform_WhenActionIsBack_ShouldBackView() {
        sut.perform(action: .back)
        
        XCTAssertEqual(flowCoordinator.callMoveToPreviousRegistrationStep, 1)
    }
}
