@testable import PicPay
import XCTest

private final class RegistrationDocumentCoordinatorSpy: RegistrationDocumentCoordinating {
    var delegate: RegistrationDocumentDelegate?
    
    weak var viewController: UIViewController?
    
    private(set) var callPerformCount = 0
    private(set) var moveToNextStepAction = false
    private(set) var cpfToNextStep: String?
    private(set) var dateOfBirthToNextStep: String?
    private(set) var invalidCpfCounter = 0
    private(set) var copyPasteCpf = false
    private(set) var alreadyRegisteredAction = false
    private(set) var webviewAction = false
    private(set) var webviewUrl: URL?
    private(set) var backAction = false
    
    func perform(action: RegistrationDocumentAction) {
        callPerformCount += 1
        
        switch action {
        case .back:
            backAction = true
        case let .moveToNextStep(cpf, dateOfBirth, invalidCpfCounter, copyPasteCpf):
            moveToNextStepAction = true
            cpfToNextStep = cpf
            self.invalidCpfCounter = invalidCpfCounter
            self.copyPasteCpf = copyPasteCpf
            dateOfBirthToNextStep = dateOfBirth
        case .alreadyRegistered:
            alreadyRegisteredAction = true
        case .webview(let url):
            webviewAction = true
            webviewUrl = url
        }
    }
}

private final class RegistrationDocumentViewControllerSpy: RegistrationDocumentDisplay {
    private(set) var callFillDocumentTextFieldCount = 0
    private(set) var cpfInForm: String?
    private(set) var dateOfBirthInForm: String?
    
    private(set) var callDisplayTermsOfServiceCount = 0
    
    private(set) var callUpdateTextsCount = 0
    private(set) var title: String?
    private(set) var description: String?
    private(set) var buttonTitle: NSAttributedString?
    
    private(set) var callDisplayErrorCount = 0
    private(set) var errorMessage: String?
    private(set) var isCpfError = false
    private(set) var isDateOfBirthError = false
    
    private(set) var calldisplayDocumentAlreadyRegisteredAlertCount = 0
    private(set) var calldisplayDocumentAlreadyRegisteredAlertPassed: Alert?
    
    private(set) var callHideErrorsCount = 0
    private(set) var callShowLoadingCount = 0
    private(set) var callHideLoadingCount = 0
    private(set) var callDismissKeyboardCount = 0
    private(set) var callDisplayPopupHelpCount = 0
    private(set) var helpAlert: Alert?
    
    
    func fillDocumentTextFields(cpf: String, dateOfBirth: String) {
        callFillDocumentTextFieldCount += 1
        cpfInForm = cpf
        dateOfBirthInForm = dateOfBirth
    }
    
    func displayTermsOfService() {
        callDisplayTermsOfServiceCount += 1
    }
    
    func updateTexts(title: String, description: String, buttonTitle: NSAttributedString) {
        callUpdateTextsCount += 1
        self.title = title
        self.description = description
        self.buttonTitle = buttonTitle
    }
    
    func displayError(message: String, isCpfError: Bool, isDateOfBirthError: Bool) {
        errorMessage = message
        callDisplayErrorCount += 1
        self.isCpfError = isCpfError
        self.isDateOfBirthError = isDateOfBirthError
    }
    
    func displayDocumentAlreadyRegisteredAlert(_ alert: Alert) {
        calldisplayDocumentAlreadyRegisteredAlertCount += 1
        calldisplayDocumentAlreadyRegisteredAlertPassed = alert
    }
    
    func hideErrors() {
        callHideErrorsCount += 1
    }
    
    func showLoading() {
        callShowLoadingCount += 1
    }
    
    func hideLoading() {
        callHideLoadingCount += 1
    }
    
    func dismissKeyboard() {
        callDismissKeyboardCount += 1
    }
    
    func displayPopupHelp(_ popup: Alert) {
        callDisplayPopupHelpCount += 1
        helpAlert = popup
    }
}

final class RegistrationDocumentPresenterTests: XCTestCase {
    private let viewControllerSpy = RegistrationDocumentViewControllerSpy()
    private let coordinatorSpy = RegistrationDocumentCoordinatorSpy()
    private lazy var sut: RegistrationDocumentPresenting = {
        let presenter = RegistrationDocumentPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testUpdateTexts_ShouldUpdateTextsOnScreen() {
        let textModel = DocumentFeature(title: "A cool title", text: "Description of the step", buttonTitle: "Click here")
        
        sut.updateTexts(textModel)
        
        XCTAssertEqual(viewControllerSpy.title, textModel.title)
        XCTAssertEqual(viewControllerSpy.description, textModel.text)
        XCTAssertEqual(viewControllerSpy.buttonTitle?.string, textModel.buttonTitle)
    }
    
    func testFillDocumentTextFields_ShouldFillCpfAndDateOfBirth() {
        let cpf = "123.634.675-56"
        let dateOfBirth = "04/06/1978"
        
        sut.fillTextFields(withCpf: cpf, dateOfBirth: dateOfBirth)
        
        XCTAssertEqual(viewControllerSpy.cpfInForm, cpf)
        XCTAssertEqual(viewControllerSpy.dateOfBirthInForm, dateOfBirth)
    }
    
    func testDisplayTermsOfService_ShouldDisplayTermsOfServiceView() {
        sut.displayTermsOfService()
        
        XCTAssertEqual(viewControllerSpy.callDisplayTermsOfServiceCount, 1)
    }
    
    func testDidNextStep_WhenActionIsMoveToNextStep_ShouldMoveToNextStep() {
        let cpf = "123.634.675-56"
        let dateOfBirth = "04/06/1978"
        let invalidCpfCounter = Int.random(in: 0...100)
        let copyPasteCpf = true
        
        sut.didNextStep(action: .moveToNextStep(withCpf: cpf, dateOfBirth: dateOfBirth, invalidCpfCounter: invalidCpfCounter, copyPasteCpf: copyPasteCpf))
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssert(coordinatorSpy.moveToNextStepAction)
        XCTAssertEqual(coordinatorSpy.cpfToNextStep, cpf)
        XCTAssertEqual(coordinatorSpy.dateOfBirthToNextStep, dateOfBirth)
        XCTAssertEqual(coordinatorSpy.invalidCpfCounter, invalidCpfCounter)
        XCTAssertEqual(coordinatorSpy.copyPasteCpf, copyPasteCpf)
    }
    
    func testDidNextStep_WhenActionIsOpenWebview_ShouldOpenWebview() throws {
        let url = try XCTUnwrap(URL(string: "www.picpay.com"))
        
        sut.didNextStep(action: .webview(url: url))
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssert(coordinatorSpy.webviewAction)
        XCTAssertEqual(coordinatorSpy.webviewUrl, url)
    }
    
    func testDidNextStep_WhenActionIsAlreadyRegistered_ShouldMaketheAlreadyRegisteredAction() throws {
        sut.didNextStep(action: .alreadyRegistered)
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssert(coordinatorSpy.alreadyRegisteredAction)
    }
    
    func testDidNextStep_WhenActionIsBack_ShouldBackView() throws {
        sut.didNextStep(action: .back)
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertTrue(coordinatorSpy.backAction)
    }
    
    func testDisplayError_WhenCpfIsInvalid_ShouldDisplayErrorMessageForCpf() {
        let errorMessage = "Some error"
        
        sut.displayError(message: errorMessage, isCpfError: true, isDateOfBirthError: false)
        
        XCTAssertEqual(viewControllerSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(viewControllerSpy.errorMessage, errorMessage)
        XCTAssert(viewControllerSpy.isCpfError)
        XCTAssertFalse(viewControllerSpy.isDateOfBirthError)
    }
    
    func testDisplayError_WhenDateOfBirthIsInvalid_ShouldDisplayErrorMessageForDateOfBirth() {
        let errorMessage = "Another error"
        
        sut.displayError(message: errorMessage, isCpfError: false, isDateOfBirthError: true)
        
        XCTAssertEqual(viewControllerSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(viewControllerSpy.errorMessage, errorMessage)
        XCTAssertFalse(viewControllerSpy.isCpfError)
        XCTAssertTrue(viewControllerSpy.isDateOfBirthError)
    }
    
    func testDisplayError_WhenCpfAndDateOfBirthAreInvalid_ShouldDisplayErrorMessageForCpfAndDateOfBirth() {
        let errorMessage = "Random error"
        
        sut.displayError(message: errorMessage, isCpfError: true, isDateOfBirthError: true)
        
        XCTAssertEqual(viewControllerSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(viewControllerSpy.errorMessage, errorMessage)
        XCTAssert(viewControllerSpy.isCpfError)
        XCTAssert(viewControllerSpy.isDateOfBirthError)
    }
    
    func testDisplayDocumentAlreadyRegisteredAlert_ShouldDisplayDocumentAlreadyRegisteredAlertInController() {
        sut.displayDocumentAlreadyRegisteredAlert()
        
        let alert = viewControllerSpy.calldisplayDocumentAlreadyRegisteredAlertPassed
        XCTAssertEqual(viewControllerSpy.calldisplayDocumentAlreadyRegisteredAlertCount, 1)
        XCTAssertEqual(alert?.title?.string, RegisterLocalizable.alertDocumentAlreadyRegisteredTitle.text)
        XCTAssertEqual(alert?.text?.string, RegisterLocalizable.alertDocumentAlreadyRegisteredText.text)
        XCTAssertEqual(alert?.buttons.first?.title, RegisterLocalizable.alertDocumentAlreadyRegisteredLoginButton.text)
        XCTAssertEqual(alert?.buttons.last?.title, RegisterLocalizable.alertDocumentAlreadyRegisteredForgotPasswordButton.text)
    }
    
    func testHideErrors_ShouldHideErrorMessages() {
        sut.hideErrors()
        
        XCTAssertEqual(viewControllerSpy.callHideErrorsCount, 1)
    }
    
    func testShowLoading_ShouldShowLoadingAnimation() {
        sut.showLoading()
        
        XCTAssertEqual(viewControllerSpy.callShowLoadingCount, 1)
    }
    
    func testHideLoading_ShouldHideLoadingAnimation() {
        sut.hideLoading()
        
        XCTAssertEqual(viewControllerSpy.callHideLoadingCount, 1)
    }
    
    func testDismissKeyboard_ShouldDismissKeyboard() {
        sut.dismissKeyboard()
        
        XCTAssertEqual(viewControllerSpy.callDismissKeyboardCount, 1)
    }
    
    func testShowDocumentHelp_ShouldDisplayHelpPopup() {
        let title = "Popup title"
        let message = "An explanation"
        let buttonTitle = "Confirmation title"
        
        sut.showDocumentHelp(title: title, message: message, buttonTitle: buttonTitle)
        
        XCTAssertEqual(viewControllerSpy.callDisplayPopupHelpCount, 1)
        
        let alert = viewControllerSpy.helpAlert
        XCTAssertEqual(alert?.title?.string, title)
        XCTAssertEqual(alert?.text?.string, message)
        XCTAssertEqual(alert?.buttons.first?.title, buttonTitle)
    }
}
