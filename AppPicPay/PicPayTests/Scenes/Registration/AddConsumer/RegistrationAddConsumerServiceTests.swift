@testable import PicPay
import OHHTTPStubs
import XCTest

final class RegistrationAddConsumerServiceTests: XCTestCase {
    private let sut = RegistrationAddConsumerService(dependencies: DependencyContainerMock())
    
    override func setUp() {
        super.setUp()
        OHHTTPStubs.removeAllStubs()
    }
    
    func testAddConsumer_WhenCallReturnsFailure_ShouldReturnError() {
        let expectation = XCTestExpectation(description: "Expected to receive error")

        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.fail()
        }
        
        sut.addConsumer(with: RegistrationModel(), statistics: RegistrationStatisticsModel()) { result in
            switch result {
            case .success:
                XCTFail("Expected to fail")
            case .failure:
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testAddConsumer_WhenCallReturnsErrorOnSuccessResponse_ShouldReturnError() {
        let errorMessage = "Este CPF já está sendo utilizado em outra conta cadastrada no PicPay"
        let errorPayloadMock: [String: Any] = ["Error": ["id": 5018, "description_pt": errorMessage]]
        
        let expectation = XCTestExpectation(description: "Expected to receive success payload with error")

        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(with: errorPayloadMock)
        }
        
        sut.addConsumer(with: RegistrationModel(), statistics: RegistrationStatisticsModel()) { result in
            switch result {
            case .success:
                XCTFail("Expected to fail")
            case .failure(let error):
                if case .badRequest(let body) = error {
                    XCTAssertEqual(body.message, errorMessage)
                    expectation.fulfill()
                } else {
                    XCTFail("Expected to fail with badRequest")
                }
                
            }
        }
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testAddConsumer_WhenCallReturnsErrorOnNoBodySuccessResponse_ShouldReturnError() {
        let expectation = XCTestExpectation(description: "Expected to receive success payload with error")
        
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(with: [:])
        }
        
        sut.addConsumer(with: RegistrationModel(), statistics: RegistrationStatisticsModel()) { result in
            switch result {
            case .success:
                XCTFail("Expected to fail")
            case .failure(let error):
                if case .bodyNotFound = error {
                    expectation.fulfill()
                } else {
                    XCTFail("Expected to fail with bodyNotFound")
                }
            }
        }
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testAddConsumer_WhenCallReturnsSuccessResponse_ShouldReturnSuccess() {
        let successResponse: [String: Any] = ["data": ["token": "96121aac8d7648e581d720296574d424", "mgm": "5643"]]
        let expectation = XCTestExpectation(description: "Expected to receive success")
        
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(with: successResponse)
        }
        
        sut.addConsumer(with: RegistrationModel(), statistics: RegistrationStatisticsModel()) { result in
            switch result {
            case .success:
                expectation.fulfill()
            case .failure:
                XCTFail("Expected success")
            }
        }
        
        wait(for: [expectation], timeout: 1.0)
    }
}
