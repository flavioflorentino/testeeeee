@testable import PicPay
import Core
import XCTest

private final class RegistrationAddConsumerServiceMock: RegistrationAddConsumerServicing {
    var addConsumerResult: Result<AddConsumerResponse, ApiError> = .success(AddConsumerResponse(token: "", mgm: nil))
    var addComplianceConsumerResult: Result<RegistrationComplianceModel, ApiError> = .success(RegistrationComplianceModel(
        token: "someToken",
        mgm: nil,
        tokenType: .permanent,
        step: .success)
    )
    
    func addConsumer(with consumerModel: RegistrationModel, statistics: RegistrationStatisticsModel, completion: @escaping (Result<AddConsumerResponse, ApiError>) -> Void) {
        completion(addConsumerResult)
    }
    
    func addComplianceConsumer(
        with consumerModel: RegistrationModel,
        statistics: RegistrationStatisticsModel,
        completion: @escaping (Result<RegistrationComplianceModel, ApiError>) -> Void
    ) {
        completion(addComplianceConsumerResult)
    }
}

final class RegistrationAddConsumerHelperTests: XCTestCase {
    private let consumerManagerMock = ConsumerManagerMock()
    private let userManagerMock = UserManagerMock()
    private lazy var dependencies = DependencyContainerMock(consumerManagerMock, userManagerMock)
    private lazy var service = RegistrationAddConsumerServiceMock()
    private lazy var sut = RegistrationAddConsumerHelper(dependencies: dependencies, service: service)
    
    func testAddConsumer_WhenAddConsumerAndLoadConsumerDataReturnSuccess_ShouldExecuteCompletionsWithSuccess() {
        let token = "46324708970721"
        let mgm = "876954"
        service.addConsumerResult = .success(AddConsumerResponse(token: token, mgm: mgm))
        consumerManagerMock.loadConsumerDataCachedResponse = (success: true, error: nil)
        
        sut.addConsumer(with: RegistrationModel(), statistics: RegistrationStatisticsModel()) { result in
            switch result {
            case .success:
                XCTAssertEqual(self.userManagerMock.token, token)
            case .failure:
                XCTFail("addConsumer call should return success, not failure")
            }
        }
    }
    
    func testAddConsumer_WhenAddConsumerReturnsBadRequestFailure_ShouldExecuteCompletionWithFailure() {
        let errorMessage = "AddConsumer error"
        var requestError = RequestError()
        requestError.message = errorMessage
        service.addConsumerResult = .failure(ApiError.badRequest(body: requestError))
        
        sut.addConsumer(with: RegistrationModel(), statistics: RegistrationStatisticsModel()) { result in
            switch result {
            case .success:
                XCTFail("addConsumer call should return failure, not success")
            case .failure(let error):
                XCTAssertEqual(error.localizedDescription, errorMessage)
            }
        }
    }
    
    func testAddConsumer_WhenAddConsumerReturnsConnectionFailure_ShouldExecuteCompletionWithFailure() {
        service.addConsumerResult = .failure(ApiError.connectionFailure)
        
        sut.addConsumer(with: RegistrationModel(), statistics: RegistrationStatisticsModel()) { result in
            switch result {
            case .success:
                XCTFail("addConsumer call should return failure, not success")
            case .failure(let error):
                XCTAssertEqual(error.localizedDescription, DefaultLocalizable.errorConnectionViewSubtitle.text)
            }
        }
    }
    
    func testAddConsumer_WhenAddConsumerReturnsGenericFailure_ShouldExecuteCompletionWithFailure() {
        service.addConsumerResult = .failure(ApiError.unknown(nil))
        
        sut.addConsumer(with: RegistrationModel(), statistics: RegistrationStatisticsModel()) { result in
            switch result {
            case .success:
                XCTFail("addConsumer call should return failure, not success")
            case .failure(let error):
                XCTAssertEqual(error.localizedDescription, DefaultLocalizable.requestError.text)
            }
        }
    }
}
