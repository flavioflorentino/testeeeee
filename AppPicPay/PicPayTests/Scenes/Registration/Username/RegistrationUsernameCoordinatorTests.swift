@testable import PicPay
import XCTest
import FeatureFlag

final class RegistrationUsernameCoordinatorTests: XCTestCase {
    private let featureManagerMock = FeatureManagerMock()
    private let viewControllerMock = NavigationControllerMock(rootViewController: UIViewController())
    private lazy var dependencyContainerMock = DependencyContainerMock(featureManagerMock, ConsumerManagerMock())
    private lazy var sut: RegistrationUsernameCoordinator = {
        let coordinator = RegistrationUsernameCoordinator(dependencies: dependencyContainerMock)
        coordinator.viewController = viewControllerMock
        return coordinator
    }()
    
    func testPerform_WhenActionOnboarding_ShouldPresentOnboarding() throws {
        featureManagerMock.override(with: [.newOnboarding: true, .onboardingContacts: true])
        
        sut = RegistrationUsernameCoordinator(dependencies: dependencyContainerMock)
        sut.viewController = viewControllerMock.topViewController
        
        sut.perform(action: .onboarding)
        
        XCTAssertTrue(viewControllerMock.isPushViewControllerCalled)
        XCTAssertTrue(viewControllerMock.topViewController is FindFriendsViewController)
    }
    
    func testPerform_WhenActionStartStudentAccount_ShouldPresentStartStudentAccount() throws {
        sut.perform(action: .startStudentAccount)
        
        let currentNavigation = try XCTUnwrap(viewControllerMock.viewControllerPresented) as? UINavigationController
        XCTAssertTrue(viewControllerMock.isPresentViewControllerCalled)
        XCTAssertTrue(currentNavigation?.topViewController is StudentStatusViewController)
    }
    
    func testPerform_WhenActionPresentWebView_ShouldPresentWebView() {
        sut.perform(action: .presentWebView("https://picpay.com"))
        
        XCTAssertTrue(viewControllerMock.isPresentViewControllerCalled)
        XCTAssertTrue(viewControllerMock.viewControllerPresented is WebViewController)
    }
    
    func testPerform_WhenActionPresentInviter_ShouldPresentInviter() throws {
        let json = MockJSON().load(resource: "validPromoCodeReward")
        let dictionary = try XCTUnwrap(json.dictionaryObject?["data"] as? [String: Any])
        let rewardPromoCode = try XCTUnwrap(PPRegisterRewardPromoCode(dictionary: dictionary))
        let contact = try XCTUnwrap(rewardPromoCode.inviter)
        
        sut.perform(action: .presentInviter(contact))
        
        XCTAssertTrue(viewControllerMock.isPresentViewControllerCalled)
        XCTAssertTrue(viewControllerMock.viewControllerPresented is ProfilePromoCodeViewController)
    }
    
    func testPerform_WhenPresentStudentAccountOffer_ShouldPresentUniversityOfferRegister() {
        sut.perform(action: .presentStudentAccountOffer(withValidatedApiId: "1"))
        
        XCTAssertTrue(viewControllerMock.isPresentViewControllerCalled)
        XCTAssertTrue(viewControllerMock.viewControllerPresented is UniversityOfferRegisterViewController)
    }
}
