@testable import PicPay
import UI
import XCTest

private final class RegistrationUsernameCoordinatorSpy: RegistrationUsernameCoordinating {
    private(set) var callOnboardingCount = 0
    private(set) var callStartStudentAccountCount = 0
    private(set) var callPresentWebViewCount = 0
    private(set) var callPresentInviterCount = 0
    private(set) var callpresentStudentAccountOfferCount = 0
    
    private(set) var webViewUrlPassed: String?
    private(set) var contactPassed: PPContact?
    private(set) var validatedApiIdPassed: String?
    
    var viewController: UIViewController?
    
    func perform(action: RegistrationUsernameAction) {
        switch action {
        case .onboarding:
            callOnboardingCount += 1
        case .startStudentAccount:
            callStartStudentAccountCount += 1
        case .presentWebView(let url):
            callPresentWebViewCount += 1
            webViewUrlPassed = url
        case .presentInviter(let contact):
            callPresentInviterCount += 1
            contactPassed = contact
        case let .presentStudentAccountOffer(withValidatedApiId: apiId):
            callpresentStudentAccountOfferCount += 1
            validatedApiIdPassed = apiId
        }
    }
}

private final class RegistrationUsernameViewControllerSpy: RegistrationUsernameDisplay {
    private(set) var callDisplayUsernameCount = 0
    private(set) var displayUsernamePassed: String?
    private(set) var callDisplayPromoCodeAlertCount = 0
    private(set) var displayPromoCodeAlertPassed: PromoCodeAlertData?
    private(set) var callDisplayPromoCodeViewCount = 0
    private(set) var titleDisplayPromoCodePassed: String?
    private(set) var descriptionDisplayPromoCodePassed: String?
    private(set) var imageDisplayPromoCodePassed: UIImage?
    private(set) var valueDisplayPromoCodePassed: String?
    private(set) var imageURLDisplayPromoCodePassed: String?
    private(set) var placeholderDisplayPromoCodePassed: UIImage?
    private(set) var callDisplayAlertErrorCount = 0
    private(set) var alertErrorPassed: Alert?
    private(set) var callDisplayInvalidPromoCodeAlertCount = 0
    private(set) var displayInvalidPromoCodeAlertPassed: Alert?
    private(set) var callDisplayErrorCount = 0
    private(set) var displayErrorPassed: String?
    private(set) var callHideErrorCount = 0
    private(set) var callShowLoadingCount = 0
    private(set) var callHideLoadingCount = 0
    
    func displayUsername(_ username: String) {
        callDisplayUsernameCount += 1
        displayUsernamePassed = username
    }
    
    func displayPromoCodeAlert(_ data: PromoCodeAlertData) {
        callDisplayPromoCodeAlertCount += 1
        displayPromoCodeAlertPassed = data
    }
    
    func displayPromoCodeView(title: String, description: String, image: UIImage) {
        callDisplayPromoCodeViewCount += 1
        titleDisplayPromoCodePassed = title
        descriptionDisplayPromoCodePassed = description
        imageDisplayPromoCodePassed = image
    }
    
    func displayPromoCodeView(title: String, description: String, value: String?, imageURL: String?, placeholder: UIImage?) {
        callDisplayPromoCodeViewCount += 1
        titleDisplayPromoCodePassed = title
        descriptionDisplayPromoCodePassed = description
        valueDisplayPromoCodePassed = value
        imageURLDisplayPromoCodePassed = imageURL
        placeholderDisplayPromoCodePassed = placeholder
    }
    
    func displayInvalidPromoCodeAlert(_ alert: Alert) {
        callDisplayInvalidPromoCodeAlertCount += 1
        displayInvalidPromoCodeAlertPassed = alert
    }
    
    func displayError(message: String) {
        callDisplayErrorCount += 1
        displayErrorPassed = message
    }
    
    func hideError() {
        callHideErrorCount += 1
    }
    
    func showLoading() {
        callShowLoadingCount += 1
    }
    
    func hideLoading() {
        callHideLoadingCount += 1
    }
}

final class RegistrationUsernamePresenterTests: XCTestCase {
    private lazy var coordinatorSpy = RegistrationUsernameCoordinatorSpy()
    private lazy var viewControllerSpy = RegistrationUsernameViewControllerSpy()
    
    private lazy var sut: RegistrationUsernamePresenter = {
        let presenter = RegistrationUsernamePresenter(coordinator: coordinatorSpy, dependencies: DependencyContainerMock())
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPresentUsernameSuggestion_ShouldDisplayUsername() {
        sut.presentUsernameSuggestion("@username")
        
        XCTAssertEqual(viewControllerSpy.callDisplayUsernameCount, 1)
        XCTAssertEqual(viewControllerSpy.displayUsernamePassed, "@username")
    }
    
    func testPresentPromoCode_WhenPromoCodeRewardType_ShouldDisplayPromoCodeView() throws {
        let json = MockJSON().load(resource: "validPromoCodeReward")
        let dictionary = try XCTUnwrap(json.dictionaryObject?["data"] as? [String: Any])
        let rewardPromoCode = try XCTUnwrap(PPRegisterRewardPromoCode(dictionary: dictionary))
        
        sut.presentPromoCode(reward: rewardPromoCode, placeholder: nil)
        
        XCTAssertEqual(viewControllerSpy.callDisplayPromoCodeViewCount, 1)
        XCTAssertEqual(viewControllerSpy.titleDisplayPromoCodePassed, "1234")
        XCTAssertEqual(viewControllerSpy.descriptionDisplayPromoCodePassed, "name")
        XCTAssertEqual(viewControllerSpy.valueDisplayPromoCodePassed, "10")
        XCTAssertEqual(viewControllerSpy.imageURLDisplayPromoCodePassed, "https://picpay.com/image")
        XCTAssertNotNil(viewControllerSpy.placeholderDisplayPromoCodePassed)
        XCTAssertNil(viewControllerSpy.imageDisplayPromoCodePassed)
    }
    
    func testPresentPromoCode_WhenPromoCodeRewardTypeWithURL_ShouldDisplayPromoCodeView() throws {
        let json = MockJSON().load(resource: "validPromoCodeReward_URL")
        let dictionary = try XCTUnwrap(json.dictionaryObject?["data"] as? [String: Any])
        let rewardPromoCode = try XCTUnwrap(PPRegisterRewardPromoCode(dictionary: dictionary))
        
        sut.presentPromoCode(reward: rewardPromoCode, placeholder: nil)
        
        XCTAssertEqual(viewControllerSpy.callDisplayPromoCodeViewCount, 1)
        XCTAssertEqual(viewControllerSpy.titleDisplayPromoCodePassed, "1234")
        XCTAssertEqual(viewControllerSpy.descriptionDisplayPromoCodePassed, "name")
        XCTAssertEqual(viewControllerSpy.valueDisplayPromoCodePassed, "10")
        XCTAssertEqual(viewControllerSpy.imageURLDisplayPromoCodePassed, "https://picpay.com/image")
        XCTAssertNotNil(viewControllerSpy.placeholderDisplayPromoCodePassed)
        XCTAssertNil(viewControllerSpy.imageDisplayPromoCodePassed)
    }
    
    func testPresentStudentPromoCode_ShouldDisplayStudentPromoCodeView() {
        sut.presentStudentPromoCode("1234")
        
        XCTAssertEqual(viewControllerSpy.callDisplayPromoCodeViewCount, 1)
        XCTAssertEqual(viewControllerSpy.titleDisplayPromoCodePassed, "1234")
        XCTAssertEqual(viewControllerSpy.descriptionDisplayPromoCodePassed, StudentLocalizable.studentAccount.text)
        XCTAssertEqual(viewControllerSpy.imageDisplayPromoCodePassed, Assets.Icons.iconCapBlack.image)
        XCTAssertNil(viewControllerSpy.valueDisplayPromoCodePassed)
        XCTAssertNil(viewControllerSpy.imageURLDisplayPromoCodePassed)
        XCTAssertNil(viewControllerSpy.placeholderDisplayPromoCodePassed)
    }
    
    func testPresentPromoCodeAlert_ShouldDisplayPromoCodeAlert() {
        sut.presentPromoCodeAlert()
        
        XCTAssertEqual(viewControllerSpy.callDisplayPromoCodeAlertCount, 1)
        XCTAssertEqual(viewControllerSpy.displayPromoCodeAlertPassed?.title, RegisterLocalizable.enterYourCode.text)
        XCTAssertEqual(viewControllerSpy.displayPromoCodeAlertPassed?.message, RegisterLocalizable.yourPromotionalCode.text)
        XCTAssertEqual(viewControllerSpy.displayPromoCodeAlertPassed?.confirmText, SignUpLocalizable.activate.text)
        XCTAssertEqual(viewControllerSpy.displayPromoCodeAlertPassed?.cancelText, DefaultLocalizable.btCancel.text)
        XCTAssertEqual(viewControllerSpy.displayPromoCodeAlertPassed?.placeholder, RegisterLocalizable.promotionalCode.text)
    }
    
    func testPresenterError_ShouldDisplayError() {
        sut.presentError(message: "Error")
        
        XCTAssertEqual(viewControllerSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(viewControllerSpy.displayErrorPassed, "Error")
    }
    
    func testPresentInvalidPromoCodeAlert_ShouldDisplayInvalidPromoCodeAlert() {
        sut.presentInvalidPromoCodeAlert("Error")
        
        XCTAssertEqual(viewControllerSpy.callDisplayInvalidPromoCodeAlertCount, 1)
        XCTAssertEqual(viewControllerSpy.displayInvalidPromoCodeAlertPassed?.title?.string, DefaultLocalizable.oops.text)
        XCTAssertEqual(viewControllerSpy.displayInvalidPromoCodeAlertPassed?.text?.string, "Error")
        XCTAssertEqual(viewControllerSpy.displayInvalidPromoCodeAlertPassed?.buttons.first?.title, RegisterLocalizable.useAnotherCode.text)
        XCTAssertEqual(viewControllerSpy.displayInvalidPromoCodeAlertPassed?.buttons.last?.title, DefaultLocalizable.btCancel.text)
    }
    
    func testHideError_ShouldHideError() {
        sut.hideError()
        
        XCTAssertEqual(viewControllerSpy.callHideErrorCount, 1)
    }
    
    func testShowLoading_ShouldDisplayLoadingAnimation() {
        sut.showLoading()
        
        XCTAssertEqual(viewControllerSpy.callShowLoadingCount, 1)
    }
    
    func testHideLoading_ShouldStopLoadingAnimation() {
        sut.hideLoading()
        
        XCTAssertEqual(viewControllerSpy.callHideLoadingCount, 1)
    }
    
    func testDidNextStep_WhenActionIsOnboarding_ShouldPresenterOnboarding() {
        sut.didNextStep(action: .onboarding)
        
        XCTAssertEqual(coordinatorSpy.callOnboardingCount, 1)
    }
    
    func testDidNextStep_WhenActionIsStartStudentAccount_ShouldPresenterStartStudentAccount() {
        sut.didNextStep(action: .startStudentAccount)
        
        XCTAssertEqual(coordinatorSpy.callStartStudentAccountCount, 1)
    }
    
    func testDidNextStep_WhenActionIsPresentWebView_ShouldPresenterWebView() {
        sut.didNextStep(action: .presentWebView("https://picpay.com"))
        
        XCTAssertEqual(coordinatorSpy.callPresentWebViewCount, 1)
        XCTAssertEqual(coordinatorSpy.webViewUrlPassed, "https://picpay.com")
    }
    
    func testDidNextStep_WhenActionIsPresentInviter_ShouldPresenterInviter() throws {
        let json = MockJSON().load(resource: "validPromoCodeReward")
        let dictionary = try XCTUnwrap(json.dictionaryObject?["data"] as? [String: Any])
        let rewardPromoCode = try XCTUnwrap(PPRegisterRewardPromoCode(dictionary: dictionary))
        let contact = try XCTUnwrap(rewardPromoCode.inviter)
        
        sut.didNextStep(action: .presentInviter(contact))
        
        XCTAssertEqual(coordinatorSpy.callPresentInviterCount, 1)
        XCTAssertEqual(coordinatorSpy.contactPassed, contact)
    }
    
    func testDidNextStep_WhenActionIspresentStudentAccountOffer_ShouldPresenterStudentAccountOffer() {
        let validatedApiId = "1"
        
        sut.didNextStep(action: .presentStudentAccountOffer(withValidatedApiId: validatedApiId))
        
        XCTAssertEqual(coordinatorSpy.callpresentStudentAccountOfferCount, 1)
        XCTAssertEqual(coordinatorSpy.validatedApiIdPassed, validatedApiId)
    }
}
