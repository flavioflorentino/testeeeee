@testable import PicPay
import Core
import XCTest
import AnalyticsModule

private final class RegistrationUsernamePresenterSpy: RegistrationUsernamePresenting {
    private(set) var callPresentUsernameSuggestionCount = 0
    private(set) var callPresentPromoCodeCount = 0
    private(set) var callpresentStudentPromoCodeCount = 0
    private(set) var callPresentPromoCodeAlertCount = 0
    private(set) var callPresentErrorCount = 0
    private(set) var callPresentInvalidPromoCodeAlertCount = 0
    private(set) var callHideErrorCount = 0
    private(set) var callOnboardingActionCount = 0
    private(set) var callStartStudentAccountCount = 0
    private(set) var callPresentWebViewCount = 0
    private(set) var callPresentInviterCount = 0
    private(set) var callPresentStudentAccountOfferCount = 0
    
    private(set) var usernameSuggestionPassed: String?
    private(set) var promoCodePassed: PPRegisterRewardPromoCode?
    private(set) var studentPromoCodePassed: String?
    private(set) var presentInvalidPromoCodeAlertPassed: String?
    private(set) var errorMessagePassed: String?
    private(set) var presentWebViewPassed: String?
    private(set) var presentInviterPassed: PPContact?
    private(set) var presentStudentAccountOfferValidatedApiId: String?
    
    var viewController: RegistrationUsernameDisplay?
    
    func presentUsernameSuggestion(_ username: String) {
        callPresentUsernameSuggestionCount += 1
        usernameSuggestionPassed = username
    }
    
    func presentPromoCode(reward: PPRegisterRewardPromoCode, placeholder: UIImage?) {
        callPresentPromoCodeCount += 1
        promoCodePassed = reward
    }
    
    func presentStudentPromoCode(_ code: String) {
        callpresentStudentPromoCodeCount += 1
        studentPromoCodePassed = code
    }
    
    func showLoading() {
        
    }
    
    func hideLoading() {
        
    }
    
    func presentPromoCodeAlert() {
        callPresentPromoCodeAlertCount += 1
    }
    
    func presentError(message: String) {
        callPresentErrorCount += 1
        errorMessagePassed = message
    }
    
    func hideError() {
        callHideErrorCount += 1
    }
    
    func presentInvalidPromoCodeAlert(_ error: String) {
        callPresentInvalidPromoCodeAlertCount += 1
        presentInvalidPromoCodeAlertPassed = error
    }
    
    func didNextStep(action: RegistrationUsernameAction) {
        switch action {
        case .onboarding:
            callOnboardingActionCount += 1
        case .startStudentAccount:
            callStartStudentAccountCount += 1
        case .presentWebView(let url):
            callPresentWebViewCount += 1
            presentWebViewPassed = url
        case .presentInviter(let contact):
            callPresentInviterCount += 1
            presentInviterPassed = contact
        case let .presentStudentAccountOffer(withValidatedApiId: apiId):
            callPresentStudentAccountOfferCount += 1
            presentStudentAccountOfferValidatedApiId = apiId
        }
    }
}

private final class RegistrationUsernameServiceMock: RegistrationUsernameServicing {
    var usernameCache: String?  = nil
    var hasStudentSignUpFlow: Bool = false
    var studentValidatedApiId: String? = nil
    var loadUserNameResult: Result<String, ApiError>?
    var createUsernameResult: Result<Void, ApiError>?
    var validatePromoCodeResult: Result<PPPromoCode, ApiError>?
    private(set) var callRemoveReferralCodePersistedCount = 0
    private(set) var callStoreUsernameStepIsFinishedCount = 0
    private(set) var callClearReferalCodeBannerDismissDateCount = 0
    
    var usernameFromCache: String? {
        usernameCache
    }
    
    var consumerStudentValidatedApiId: String? {
        studentValidatedApiId
    }
    
    func removeReferralCodePersisted() {
        callRemoveReferralCodePersistedCount += 1
    }
    
    func storeUsernameStepIsFinished() {
        callStoreUsernameStepIsFinishedCount += 1
    }
    
    func clearReferalCodeBannerDismissDate() {
        callClearReferalCodeBannerDismissDateCount += 1
    }
    
    func loadUsername(completion: @escaping (Result<String, ApiError>) -> Void) {
        guard let loadUserNameResult = loadUserNameResult else {
            return
        }
        
        completion(loadUserNameResult)
    }
    
    func createUsername(_ username: String, completion: @escaping (Result<Void, ApiError>) -> Void) {
        guard let createUsernameResult = createUsernameResult else {
            return
        }
        
        completion(createUsernameResult)
    }
    
    func validadePromoCode(_ promoCode: String, completion: @escaping (Result<PPPromoCode, ApiError>) -> Void) {
        guard let validatePromoCodeResult = validatePromoCodeResult else {
            return
        }
        
        completion(validatePromoCodeResult)
    }
}

final class RegistrationUsernameViewModelTests: XCTestCase {
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var serviceMock = RegistrationUsernameServiceMock()
    private lazy var presenterSpy = RegistrationUsernamePresenterSpy()
    private lazy var logDetectionServiceSpy = LogDetectionServiceSpy()
    
    private lazy var sut: RegistrationUsernameViewModel = {
        let viewModel = RegistrationUsernameViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            dependencies: DependencyContainerMock(analyticsSpy),
            promoCode: nil,
            logDetectionService: logDetectionServiceSpy
        )
        return viewModel
    }()
    
    func testLoadUsernameSuggestion_WhenHasUsernameInCache_ShouldPresentUsernameCached() {
        serviceMock.usernameCache = "@usernameCached"
        
        sut.loadUsernameSuggestion()
        
        XCTAssertEqual(presenterSpy.callPresentUsernameSuggestionCount, 1)
        XCTAssertEqual(presenterSpy.usernameSuggestionPassed, "@usernameCached")
    }
    
    func testLoadUsernameSuggestion_WhenUsernameInCacheIsEmptyAndResponseIsSuccess_ShouldPresentUsernameFromServer() {
        serviceMock.loadUserNameResult = Result.success("@usernameFromServer")
        
        sut.loadUsernameSuggestion()
        
        XCTAssertEqual(presenterSpy.callHideErrorCount, 1)
        XCTAssertEqual(presenterSpy.callPresentUsernameSuggestionCount, 1)
        XCTAssertEqual(presenterSpy.usernameSuggestionPassed, "@usernameFromServer")
    }
    
    func testLoadUsernameSuggestion_WhenUsernameInCacheIsEmptyAndResponseIsFailureBadRequest_ShouldPresentError() {
        var requestError = RequestError()
        requestError.message = "Error Bad Request"
        serviceMock.loadUserNameResult = Result.failure(ApiError.badRequest(body: requestError))
        
        sut.loadUsernameSuggestion()
        
        XCTAssertEqual(presenterSpy.callHideErrorCount, 1)
        XCTAssertEqual(presenterSpy.callPresentErrorCount, 1)
        XCTAssertEqual(presenterSpy.errorMessagePassed, "Error Bad Request")
    }
    
    func testLoadUsernameSuggestion_WhenUsernameInCacheIsEmptyAndResponseIsFailureConnectionFailure_ShouldPresentError() {
        serviceMock.loadUserNameResult = Result.failure(ApiError.connectionFailure)
        
        sut.loadUsernameSuggestion()
        
        XCTAssertEqual(presenterSpy.callHideErrorCount, 1)
        XCTAssertEqual(presenterSpy.callPresentErrorCount, 1)
        XCTAssertEqual(presenterSpy.errorMessagePassed, DefaultLocalizable.errorConnectionViewSubtitle.text)
    }
    
    func testLoadUsernameSuggestion_WhenUsernameInCacheIsEmptyAndResponseIsFailureGenericError_ShouldPresentError() {
        serviceMock.loadUserNameResult = Result.failure(ApiError.timeout)
        
        sut.loadUsernameSuggestion()
        
        XCTAssertEqual(presenterSpy.callHideErrorCount, 1)
        XCTAssertEqual(presenterSpy.callPresentErrorCount, 1)
        XCTAssertEqual(presenterSpy.errorMessagePassed, DefaultLocalizable.requestError.text)
    }
    
    func testValidatePromoCodeIfNeeded_WhenPromoCodeIsEmpty_ShouldNotPresentPromoCodeView() {
        sut.validatePromoCodeIfNeeded()
        
        XCTAssertEqual(presenterSpy.callPresentPromoCodeCount, 0)
        XCTAssertNil(presenterSpy.promoCodePassed)
    }
    
    func testValidatePromoCodeIfNeeded_WhenPromoCodeIsNotEmptyAndValidAndPromoCodeStudentType_ShoulPresentStartStudentAccount() throws {
        sut = RegistrationUsernameViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            dependencies: DependencyContainerMock(analyticsSpy),
            promoCode: "1234"
        )
        let promoCode = try XCTUnwrap(PPPromoCode(json: MockJSON().load(resource: "validPromoCodeStudent")))
        serviceMock.validatePromoCodeResult = Result.success(promoCode)

        sut.validatePromoCodeIfNeeded()

        XCTAssertEqual(presenterSpy.callHideErrorCount, 1)
        XCTAssertEqual(presenterSpy.callStartStudentAccountCount, 1)
        XCTAssertEqual(serviceMock.callClearReferalCodeBannerDismissDateCount, 1)
    }
    
    func testValidatePromoCodeIfNeeded_WhenPromoCodeIsNotEmptyAndValidAndPromoCodeWebType_ShoulPresentWebView() throws {
        sut = RegistrationUsernameViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            dependencies: DependencyContainerMock(analyticsSpy),
            promoCode: "1234"
        )
        let promoCode = try XCTUnwrap(PPPromoCode(json: MockJSON().load(resource: "validPromoCodeWeb")))
        serviceMock.validatePromoCodeResult = Result.success(promoCode)

        sut.validatePromoCodeIfNeeded()

        XCTAssertEqual(presenterSpy.callHideErrorCount, 1)
        XCTAssertEqual(presenterSpy.callPresentWebViewCount, 1)
        XCTAssertEqual(presenterSpy.presentWebViewPassed, "https://picpay.com")
        XCTAssertEqual(serviceMock.callClearReferalCodeBannerDismissDateCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: RegistrationEvent.validatePromoCode("1234").event()))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testValidatePromoCodeIfNeeded_WhenPromoCodeIsNotEmptyAndValidAndPromoCodeRewardType_ShoulPresentInviter() throws {
        sut = RegistrationUsernameViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            dependencies: DependencyContainerMock(analyticsSpy),
            promoCode: "1234"
        )
        let promoCode = try XCTUnwrap(PPPromoCode(json: MockJSON().load(resource: "validPromoCodeReward")))
        serviceMock.validatePromoCodeResult = Result.success(promoCode)

        sut.validatePromoCodeIfNeeded()

        XCTAssertEqual(presenterSpy.callHideErrorCount, 1)
        XCTAssertEqual(presenterSpy.callPresentInviterCount, 1)
        XCTAssertNotNil(presenterSpy.presentInviterPassed)
        XCTAssertEqual(presenterSpy.callPresentPromoCodeCount, 1)
        XCTAssertNotNil(presenterSpy.promoCodePassed)
        XCTAssertEqual(serviceMock.callClearReferalCodeBannerDismissDateCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: RegistrationEvent.validatePromoCode("1234").event()))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testValidatePromoCodeIfNeeded_WhenPromoCodeIsNotEmptyAndValidAndPromoCodeRewardTypeWithURL_ShoulPresentWebView() throws {
        sut = RegistrationUsernameViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            dependencies: DependencyContainerMock(analyticsSpy),
            promoCode: "1234"
        )
        let promoCode = try XCTUnwrap(PPPromoCode(json: MockJSON().load(resource: "validPromoCodeReward_URL")))
        serviceMock.validatePromoCodeResult = Result.success(promoCode)

        sut.validatePromoCodeIfNeeded()

        XCTAssertEqual(presenterSpy.callHideErrorCount, 1)
        XCTAssertEqual(presenterSpy.callPresentWebViewCount, 1)
        XCTAssertEqual(presenterSpy.callPresentPromoCodeCount, 1)
        XCTAssertNotNil(presenterSpy.promoCodePassed)
        XCTAssertEqual(presenterSpy.presentWebViewPassed, "https://picpay.com")
        XCTAssertEqual(serviceMock.callClearReferalCodeBannerDismissDateCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: RegistrationEvent.validatePromoCode("1234").event()))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testValidatePromoCodeIfNeeded_WhenPromoCodeIsNotEmptyAndInvalid_ShouldPresentError() {
        sut = RegistrationUsernameViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            dependencies: DependencyContainerMock(analyticsSpy),
            promoCode: "1234"
        )
        
        var requestError = RequestError()
        requestError.message = "Invalid code"
        
        let validatePromoCodeEvent = RegistrationEvent.validatePromoCode("1234").event()
        let promoCodeErrorEvent = RegistrationEvent.promoCodeError("Invalid code").event()
        
        serviceMock.validatePromoCodeResult = Result.failure(ApiError.badRequest(body: requestError))
        
        sut.validatePromoCodeIfNeeded()
        
        XCTAssertEqual(presenterSpy.callHideErrorCount, 1)
        XCTAssertEqual(presenterSpy.callPresentInvalidPromoCodeAlertCount, 1)
        XCTAssertEqual(presenterSpy.presentInvalidPromoCodeAlertPassed, "Invalid code")
        XCTAssertEqual(serviceMock.callRemoveReferralCodePersistedCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: validatePromoCodeEvent, promoCodeErrorEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 2)
    }
    
    func testValidadePromoCode_WhenPromoCodeIsNil_ShouldNotPresentPromoCodeView() {
        sut.validadePromoCode(nil)
        
        XCTAssertEqual(presenterSpy.callPresentPromoCodeCount, 0)
        XCTAssertNil(presenterSpy.promoCodePassed)
    }
    
    func testValidadePromoCode_WhenPromoCodeIsValidAndPromoCodeStudentType_ShouldPresentStartStudentAccount() throws {
        let promoCode = try XCTUnwrap(PPPromoCode(json: MockJSON().load(resource: "validPromoCodeStudent")))
        serviceMock.validatePromoCodeResult = Result.success(promoCode)

        sut.validadePromoCode("1234")

        XCTAssertEqual(presenterSpy.callHideErrorCount, 1)
        XCTAssertEqual(presenterSpy.callStartStudentAccountCount, 1)
        XCTAssertEqual(serviceMock.callClearReferalCodeBannerDismissDateCount, 1)
    }
    
    func testValidadePromoCode_WhenPromoCodeIsValidAndPromoCodeWebType_ShouldPresentWebView() throws {
        let promoCode = try XCTUnwrap(PPPromoCode(json: MockJSON().load(resource: "validPromoCodeWeb")))
        serviceMock.validatePromoCodeResult = Result.success(promoCode)

        sut.validadePromoCode("1234")

        XCTAssertEqual(presenterSpy.callHideErrorCount, 1)
        XCTAssertEqual(presenterSpy.callPresentWebViewCount, 1)
        XCTAssertEqual(presenterSpy.presentWebViewPassed, "https://picpay.com")
        XCTAssertEqual(serviceMock.callClearReferalCodeBannerDismissDateCount, 1)
    }
    
    func testValidadePromoCode_WhenPromoCodeIsValidAndPromoCodeRewardType_ShoulPresentInviter() throws {
        let promoCode = try XCTUnwrap(PPPromoCode(json: MockJSON().load(resource: "validPromoCodeReward")))
        serviceMock.validatePromoCodeResult = Result.success(promoCode)

        sut.validadePromoCode("1234")

        XCTAssertEqual(presenterSpy.callHideErrorCount, 1)
        XCTAssertEqual(presenterSpy.callPresentInviterCount, 1)
        XCTAssertNotNil(presenterSpy.presentInviterPassed)
        XCTAssertEqual(presenterSpy.callPresentPromoCodeCount, 1)
        XCTAssertNotNil(presenterSpy.promoCodePassed)
        XCTAssertEqual(serviceMock.callClearReferalCodeBannerDismissDateCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: RegistrationEvent.validatePromoCode("1234").event()))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testValidadePromoCode_WhenPromoCodeIsValidAndPromoCodeRewardTypeWithURL_ShoulPresentWebView() throws {
        let promoCode = try XCTUnwrap(PPPromoCode(json: MockJSON().load(resource: "validPromoCodeReward_URL")))
        serviceMock.validatePromoCodeResult = Result.success(promoCode)

        sut.validadePromoCode("1234")

        XCTAssertEqual(presenterSpy.callHideErrorCount, 1)
        XCTAssertEqual(presenterSpy.callPresentWebViewCount, 1)
        XCTAssertEqual(presenterSpy.callPresentPromoCodeCount, 1)
        XCTAssertNotNil(presenterSpy.promoCodePassed)
        XCTAssertEqual(presenterSpy.presentWebViewPassed, "https://picpay.com")
        XCTAssertEqual(serviceMock.callClearReferalCodeBannerDismissDateCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: RegistrationEvent.validatePromoCode("1234").event()))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testValidadePromoCode_WhenPromoCodeIsInvalid_ShouldPresentError() {
        var requestError = RequestError()
        requestError.message = "Invalid code"
        
        let validatePromoCodeEvent = RegistrationEvent.validatePromoCode("1234").event()
        let promoCodeErrorEvent = RegistrationEvent.promoCodeError("Invalid code").event()
        
        serviceMock.validatePromoCodeResult = Result.failure(ApiError.badRequest(body: requestError))

        sut.validadePromoCode("1234")

        XCTAssertEqual(presenterSpy.callHideErrorCount, 1)
        XCTAssertEqual(presenterSpy.callPresentInvalidPromoCodeAlertCount, 1)
        XCTAssertEqual(presenterSpy.presentInvalidPromoCodeAlertPassed, "Invalid code")
        XCTAssertEqual(serviceMock.callRemoveReferralCodePersistedCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: validatePromoCodeEvent, promoCodeErrorEvent))
        XCTAssertEqual(analyticsSpy.logCalledCount, 2)
    }
    
    func testPresentStudentPromoCode_WhenStudentAccountCreationCompleted_ShouldPresentStudentPromoCode() {
        sut = RegistrationUsernameViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            dependencies: DependencyContainerMock(analyticsSpy),
            promoCode: "1234"
        )
        
        sut.presentStudentPromoCode()
        
        XCTAssertEqual(presenterSpy.callpresentStudentPromoCodeCount, 1)
        XCTAssertEqual(presenterSpy.studentPromoCodePassed, "1234")
    }
    
    func testCreateUsername_WhenUsernameIsValid_ShouldPresentOnBoarding() {
        serviceMock.createUsernameResult = Result.success
        
        sut.createUsername("username")
        
        XCTAssertEqual(presenterSpy.callHideErrorCount, 1)
        XCTAssertEqual(presenterSpy.callOnboardingActionCount, 1)
        XCTAssertEqual(serviceMock.callStoreUsernameStepIsFinishedCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: RegistrationEvent.registerUsername(attempts: 1, withSuggestion: false).event()))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testCreateUsername_WhenUsernameCountIsInvalid_ShouldPresentError() {
        sut.createUsername("ab")
        
        XCTAssertEqual(presenterSpy.callPresentErrorCount, 1)
        XCTAssertEqual(presenterSpy.errorMessagePassed, RegisterLocalizable.usernameInvalid.text)
    }
    
    func testCreateUsername_WhenUsernameResponseIsInvalid_ShouldPresentError() {
        var requestError = RequestError()
        requestError.message = "Invalid user"
        serviceMock.createUsernameResult = Result.failure(ApiError.badRequest(body: requestError))
        
        sut.createUsername("username")
        
        XCTAssertEqual(presenterSpy.callHideErrorCount, 1)
        XCTAssertEqual(presenterSpy.callPresentErrorCount, 1)
        XCTAssertEqual(presenterSpy.errorMessagePassed, "Invalid user")
        XCTAssertTrue(analyticsSpy.equals(to: RegistrationEvent.registerUsername(attempts: 1, withSuggestion: false).event()))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testCreateUsername_WhenUseUsernameSuggestion_ShouldPresentOnBoarding() {
        serviceMock.usernameCache = "username"
        serviceMock.createUsernameResult = Result.success
        
        sut.loadUsernameSuggestion()
        sut.createUsername("username")
        
        XCTAssertEqual(presenterSpy.callHideErrorCount, 1)
        XCTAssertEqual(presenterSpy.callOnboardingActionCount, 1)
        XCTAssertEqual(serviceMock.callStoreUsernameStepIsFinishedCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: RegistrationEvent.registerUsername(attempts: 1, withSuggestion: true).event()))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testDidUsePromoCode_ShouldPresentUserPromoCode() throws {
        sut.didUsePromoCode()
        
        XCTAssertEqual(presenterSpy.callPresentPromoCodeAlertCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: RegistrationEvent.touchPromoCode.event()))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testDidSkip_ShouldPresentOnboarding() {
        sut.didSkip()
        
        XCTAssertEqual(presenterSpy.callOnboardingActionCount, 1)
        XCTAssertEqual(serviceMock.callStoreUsernameStepIsFinishedCount, 1)
    }
    
    func testCheckStudentOffer_WhenFeatureFlagTrueNotPresentedStudentRegisterAndHasStudentValidatedApiId_ShouldPresentStudentAccountOffer() {
        let validApiId = "1"
        serviceMock.hasStudentSignUpFlow = true
        serviceMock.studentValidatedApiId = validApiId
        
        sut.checkStudentOffer()
        
        XCTAssertEqual(presenterSpy.callPresentStudentAccountOfferCount, 1)
        XCTAssertEqual(presenterSpy.presentStudentAccountOfferValidatedApiId, validApiId)
    }
    
    func testCheckStudentOffer_WhenFeatureFlagTrueAlreadyPresentedStudentRegister_ShouldNotPresentStudentAccountOffer() throws {
        serviceMock.hasStudentSignUpFlow = true
        sut = RegistrationUsernameViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            dependencies: DependencyContainerMock(analyticsSpy),
            promoCode: "1234"
        )
        let promoCode = try XCTUnwrap(PPPromoCode(json: MockJSON().load(resource: "validPromoCodeStudent")))
        serviceMock.validatePromoCodeResult = Result.success(promoCode)
        sut.validatePromoCodeIfNeeded()
        
        sut.checkStudentOffer()

        XCTAssertEqual(presenterSpy.callPresentStudentAccountOfferCount, 0)
    }
    
    func testCheckStudentOffer_WhenFeatureFlagTrueNotPresentedStudentRegisterWithouStudentValidatedApiId_ShouldNotPresentStudentAccountOffer() {
        serviceMock.hasStudentSignUpFlow = true
        
        sut.checkStudentOffer()
        
        XCTAssertEqual(presenterSpy.callPresentStudentAccountOfferCount, 0)
    }
    
    func testCheckStudentOffer_WhenFeatureFlagFalse_ShouldNotPresentStudentAccountOffer() {
        serviceMock.hasStudentSignUpFlow = false
        
        sut.checkStudentOffer()
        
        XCTAssertEqual(presenterSpy.callPresentStudentAccountOfferCount, 0)
    }
    
    func testSendLogDetection_WhenCalled_ShouldSendLog() {
        sut.sendLogDetection()
        XCTAssertEqual(logDetectionServiceSpy.sendLogCallCount, 1)
    }
}
