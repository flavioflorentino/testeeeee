@testable import PicPay
import XCTest

final class RegisterPromoCodeSuccessViewModelTests: XCTestCase {
    private var viewModel: RegisterPromoCodeViewModelInputs?
    private let presenter = SpyRegisterPromoCodePresenter()
    
    private func setupViewModel(validURL: Bool = true) {
        let validPromoCode = ValidPromoCode.mock(validUrl: validURL)
        viewModel = RegisterPromoCodeViewModel(presenter: presenter, validPromoCode: validPromoCode)
    }
    
    override func setUp() {
        setupViewModel()
        super.setUp()
    }
    
    override func tearDown() {
        viewModel = nil
        super.tearDown()
    }
    
    func testBuildViewModelCalled() {
        setupViewModel()
        viewModel?.viewDidLoad()
        XCTAssertTrue(presenter.isBuildModelCalled)
    }
    
    func testUnderstandAction() {
        setupViewModel()
        viewModel?.understandAction()
        XCTAssertTrue(presenter.isDidNextStepCalled)
        XCTAssertNotNil(presenter.actionCalled)
        
        let validPromoCode = ValidPromoCode.mock()
        XCTAssertEqual(presenter.actionCalled, RegisterPromoCodeAction.createStudentAccount(validPromoCode: validPromoCode))
    }
    
    func testOpenRegulationAction() throws {
        setupViewModel()
        let expectURL = try URL(string: "https://picpay.com").safe()
        viewModel?.openRegulationAction()
        XCTAssertTrue(presenter.isDidNextStepCalled)
        XCTAssertNotNil(presenter.actionCalled)
        XCTAssertEqual(presenter.actionCalled, RegisterPromoCodeAction.openRegulation(expectURL))
    }
    
    func testFailedOpenRegulationAction() {
        setupViewModel(validURL: false)
        viewModel?.openRegulationAction()
        XCTAssertFalse(presenter.isDidNextStepCalled)
        XCTAssertNil(presenter.actionCalled)
    }
}

extension RegisterPromoCodeAction: Equatable {
    public static func ==(lhs: RegisterPromoCodeAction, rhs: RegisterPromoCodeAction) -> Bool {
        switch (lhs, rhs) {
        case (.createStudentAccount, .createStudentAccount):
            return true
        case let (.openRegulation(lhsURL), .openRegulation(rhsURL)):
            return lhsURL == rhsURL
        default:
            return false
        }
    }
}

private final class SpyRegisterPromoCodePresenter: RegisterPromoCodePresenting {
    fileprivate(set) var viewController: RegisterPromoCodeDisplay?
    private(set) var isBuildModelCalled: Bool = false
    private(set) var isDidNextStepCalled: Bool = false
    private(set) var actionCalled: RegisterPromoCodeAction?
    
    func buildModel(_ response: PromoCodeValidationData) {
        isBuildModelCalled = true
    }
    
    func didNextStep(action: RegisterPromoCodeAction) {
        isDidNextStepCalled = true
        actionCalled = action
    }
}
