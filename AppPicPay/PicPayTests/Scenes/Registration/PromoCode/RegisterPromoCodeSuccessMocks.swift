@testable import PicPay

extension PromoCodeValidationResponse {
    static func mock(type: PromoCodeType = .promo, validUrl: Bool = true) -> PromoCodeValidationResponse {
        PromoCodeValidationResponse(
            type: type,
            data: PromoCodeValidationData.mock(validUrl: validUrl)
        )
    }
}

extension PromoCodeValidationData {
    static func mock(type: PromoCodeValidationResponse.PromoCodeType = .promo, validUrl: Bool = true) -> PromoCodeValidationData {
        let title = type == .studentAccount ?  "Oi, estudante!" : "Muito bem! Agora é só finalizar o cadastro para validar o seu código e ganhar R$10 de volta quando fizer seu primeiro pagamento."
        
        let description = type == .studentAccount ?  "Com a ContaUni você ganha 10% de dinheiro de volta quando:" : "Isabelle Bezerra convidou você para o PicPay com o código H3F11Q. Você tem até 7 dias para pagar com cartão de crédito e validar seu cashback."
        
        return PromoCodeValidationData(
            referralName: "Isabelle Bezerra",
            referralCode: "H3F11Q",
            faqLink: validUrl ? "https://picpay.com" : nil,
            promoAmount: type == .studentAccount ? "10%" : "R$10",
            title: title,
            description: description,
            benefits: [
                "Recarregar o celular",
                "Usar o Uber pré-pago",
                "Comprar online",
                "Pagar com a maquininha Cielo",
                "Recargar o cartão de transporte"
            ]
        )
    }
}

extension ValidPromoCode {
    static func mock(type: PromoCodeValidType = .promo, validUrl: Bool = true) -> ValidPromoCode {
        ValidPromoCode(
            type: type,
            data: PromoCodeValidationData.mock(validUrl: validUrl)
        )
    }
}
