@testable import PicPay
import XCTest

private final class MockRegisterPromoCodeCoordinatorDelegate: RegisterPromoCodeCoordinatorDelegate {
    private(set) var callPresentUserRegistrationCount = 0
    private(set) var promoCode: String?
    
    func presentUserRegistration(with promoCode: ValidPromoCode) {
        callPresentUserRegistrationCount += 1
        self.promoCode = promoCode.data.referralCode
    }
}

final class RegisterPromoCodeCoordinatorTests: XCTestCase {
    private lazy var viewController: UIViewController = {
        let viewController = UIViewController()
        return viewController
    }()
    
    private lazy var navigationController: NavigationControllerMock = {
        let navigationController = NavigationControllerMock(rootViewController: viewController)
        return navigationController
    }()
    
    private lazy var coordinatorDelegate = MockRegisterPromoCodeCoordinatorDelegate()
    
    private lazy var coordinator: RegisterPromoCodeCoordinating = {
        let coordinator = RegisterPromoCodeCoordinator()
        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        return coordinator
    }()
    
    override func setUp() {
        navigationController.loadViewIfNeeded()
        super.setUp()
    }
    
    func testPerform_WhenPassedCreateUserAccountAction_ShouldPresentUserRegistration() {
        let validPromoCode = ValidPromoCode.mock()
        
        coordinator.perform(action: .createStudentAccount(validPromoCode: validPromoCode))
        
        XCTAssertTrue(navigationController.isPushViewControllerCalled)
        XCTAssertEqual(coordinatorDelegate.callPresentUserRegistrationCount, 1)
        XCTAssertEqual(coordinatorDelegate.promoCode, validPromoCode.data.referralCode)
    }
    
    func testPerformOpenRegulationAction() throws {
        let url = try URL(string: "https://picpay.com").safe()
        coordinator.perform(action: .openRegulation(url))
        XCTAssertTrue(navigationController.isPresentViewControllerCalled)
    }
}
