@testable import PicPay
import XCTest
import UI

final class RegisterPromoCodeSuccessPresenterTests: XCTestCase {
    private lazy var view: SpyRegisterPromoCodeView = {
        let view = SpyRegisterPromoCodeView()
        return view
    }()
    
    private lazy var coordinator: MockRegisterPromoCodeCoordinating = {
        let coordinator = MockRegisterPromoCodeCoordinating()
        coordinator.viewController = view
        return coordinator
    }()
    
    private lazy var presenter: RegisterPromoCodePresenting = {
       let presenter = RegisterPromoCodePresenter(coordinator: coordinator)
        presenter.viewController = view
        return presenter
    }()
    
    func testBuildModelShouldReturnAModelForTypeMGM() throws {
        let title = NSMutableAttributedString(string: "Muito bem! Agora é só finalizar o cadastro para validar o seu código e ganhar R$10 de volta quando fizer seu primeiro pagamento.")
        let titleColor = Palette.ppColorBranding300.color
        let titleColorRange = (title.string as NSString).range(of: "R$10")
        var titleColorAssert = false
        title.textColor(text: "R$10", color: titleColor)

        let description = NSMutableAttributedString(string: "Isabelle Bezerra convidou você para o PicPay com o código H3F11Q. Você tem até 7 dias para pagar com cartão de crédito e validar seu cashback.")
        description.font(text: "Isabelle Bezerra", font: .boldSystemFont(ofSize: 14))
        description.font(text: "H3F11Q", font: .boldSystemFont(ofSize: 14))
        description.paragraph(aligment: .center, lineSpace: 5)
        
        presenter.buildModel(PromoCodeValidationData.mock())
        
        XCTAssertEqual(try view.model.safe().image, Assets.SocialOnboarding.onboardingSocialPopup.image)
        XCTAssertEqual(try view.model.safe().title.string, title.string)
        XCTAssertEqual(try view.model.safe().description, description)
        XCTAssertEqual(view.model?.link, "https://picpay.com")
        
        view.model?.title.enumerateAttribute(NSAttributedString.Key.foregroundColor, in: titleColorRange) { value, range, stop in
            if let foregroundColor = value as? UIColor {
                titleColorAssert = titleColor.cgColor == foregroundColor.cgColor
            }
        }
        
        XCTAssertTrue(titleColorAssert)
    }
    
    func testBuildModelShouldReturnAModelForTypePromo() throws {
         let title = NSMutableAttributedString(string: "Muito bem! Agora é só finalizar o cadastro para validar o seu código e ganhar R$10 de volta quando fizer seu primeiro pagamento.")
        let titleColor = Palette.ppColorBranding300.color
        let titleColorRange = (title.string as NSString).range(of: "R$10")
        var titleColorAssert = false
        title.textColor(text: "R$10", color: titleColor)

        let description = NSMutableAttributedString(string: "Isabelle Bezerra convidou você para o PicPay com o código H3F11Q. Você tem até 7 dias para pagar com cartão de crédito e validar seu cashback.")
        description.font(text: "Isabelle Bezerra", font: .boldSystemFont(ofSize: 14))
        description.font(text: "H3F11Q", font: .boldSystemFont(ofSize: 14))
        description.paragraph(aligment: .center, lineSpace: 5)
        
        presenter.buildModel(PromoCodeValidationData.mock(validUrl: false))
        
        XCTAssertEqual(try view.model.safe().image, Assets.SocialOnboarding.onboardingSocialPopup.image)
        XCTAssertEqual(try view.model.safe().title.string, title.string)
        XCTAssertEqual(try view.model.safe().description, description)
        XCTAssertNil(view.model?.link)
        
        view.model?.title.enumerateAttribute(NSAttributedString.Key.foregroundColor, in: titleColorRange) { value, range, stop in
            if let foregroundColor = value as? UIColor {
                titleColorAssert = titleColor.cgColor == foregroundColor.cgColor
            }
        }
        
        XCTAssertTrue(titleColorAssert)
    }
    
    func testDidNextStepShouldExecuteUnderstandAction() {
        let validPromoCode = ValidPromoCode.mock()
        presenter.didNextStep(action: .createStudentAccount(validPromoCode: validPromoCode))
        XCTAssertTrue(coordinator.understandActionCalled)
        XCTAssertFalse(coordinator.openRegulationCalled)
        XCTAssertNil(coordinator.receivedOpenRegulationURL)
    }
    
    func testDidNextStepShouldExecuteOpenRegulationAction() throws {
        let url = try URL(string: "https://picpay.com").safe()
        presenter.didNextStep(action: .openRegulation(url))
        XCTAssertTrue(coordinator.openRegulationCalled)
        XCTAssertFalse(coordinator.understandActionCalled)
        XCTAssertNotNil(coordinator.receivedOpenRegulationURL)
        XCTAssertEqual(url, coordinator.receivedOpenRegulationURL)
    }
}

private final class SpyRegisterPromoCodeView: UIViewController, RegisterPromoCodeDisplay {
    private(set) var model: RegisterPromoCodeModel?
    
    func displayModel(_ model: RegisterPromoCodeModel) {
        self.model = model
    }
}

private final class MockRegisterPromoCodeCoordinating: RegisterPromoCodeCoordinating {
    var delegate: RegisterPromoCodeCoordinatorDelegate?
    
    fileprivate(set) var viewController: UIViewController?
    private(set) var receivedOpenRegulationURL: URL?
    private(set) var understandActionCalled = false
    private(set) var openRegulationCalled = false
    
    func perform(action: RegisterPromoCodeAction) {
        switch action {
        case .createStudentAccount:
            understandActionCalled = true
        case let .openRegulation(url):
            openRegulationCalled = true
            receivedOpenRegulationURL = url
        }
    }
}
