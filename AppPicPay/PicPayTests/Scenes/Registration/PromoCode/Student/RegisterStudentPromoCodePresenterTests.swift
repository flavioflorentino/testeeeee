@testable import PicPay
import UI
import XCTest

final class RegisterStudentPromoCodePresenterTests: XCTestCase {
    private lazy var view: SpyRegisterStudentPromoCode = {
        let view = SpyRegisterStudentPromoCode()
        return view
    }()
    
    private lazy var coordinator: MockRegisterStudentPromoCodeCoordinating = {
        let coordinator = MockRegisterStudentPromoCodeCoordinating()
        coordinator.viewController = view
        return coordinator
    }()
    
    private lazy var presenter: RegisterStudentPromoCodePresenting = {
        let presenter = RegisterStudentPromoCodePresenter(coordinator: coordinator)
        presenter.viewController = view
        return presenter
    }()
    
    func testBuildModelShouldReturnAModelForCorrectType() throws {
        let title = NSMutableAttributedString(string: "Oi, estudante!")
        let titleColor = Palette.ppColorBranding300.color
        let titleColorRange = (title.string as NSString).range(of: "estudante")
        var titleColorAssert = false
        title.textColor(text: "estudante", color: titleColor)

        let lightModeTraitCollection = UITraitCollection(userInterfaceStyle: .light)
        let darkModeTraitCollection = UITraitCollection(userInterfaceStyle: .dark)
        let description = NSMutableAttributedString(string: "Com a ContaUni você ganha 10% de dinheiro de volta quando:")
        let descriptionBlueColor = Palette.ppColorNeutral500.color.withCustomDarkColor(Palette.ppColorNeutral300.color)
        let descriptionBlueColorRange = (description.string as NSString).range(of: "ContaUni")
        var descriptionBlueColorAssert = false
        let descriptionWhiteColor = Palette.white.color
        let descriptionBlackColor = Palette.black.color
        let descriptionWhiteColorRange = (description.string as NSString).range(of: "10%")
        let descriptionGreenColor = Palette.ppColorBranding300.color
        let descriptionGreenColorRange = (description.string as NSString).range(of: "10%")
        var descriptionGreenColorAssert = false
        
        let benefits = ["Recarregar o celular", "Usar o Uber pré-pago", "Comprar online", "Pagar com a maquininha Cielo", "Recargar o cartão de transporte"]
        
        presenter.buildModel(PromoCodeValidationData.mock(type: .studentAccount))
        
        XCTAssertEqual(try view.model.safe().image, Assets.Icons.iconUniversitario.image)
        XCTAssertEqual(try view.model.safe().title.string, title.string)
        XCTAssertEqual(try view.model.safe().description.string, description.string)
        XCTAssertEqual(view.model?.benefits, benefits)
        
        view.model?.title.enumerateAttribute(NSAttributedString.Key.foregroundColor, in: titleColorRange) { value, range, stop in
            if let foregroundColor = value as? UIColor {
                titleColorAssert = titleColor.cgColor == foregroundColor.cgColor
            }
        }
        
        XCTAssertTrue(titleColorAssert)
        
        view.model?.description.enumerateAttribute(NSAttributedString.Key.foregroundColor, in: descriptionBlueColorRange) { value, range, stop in
            if let foregroundColor = value as? UIColor {
                descriptionBlueColorAssert = descriptionBlueColor.cgColor == foregroundColor.cgColor
            }
        }
        
        XCTAssertTrue(descriptionBlueColorAssert)
        
        view.model?.description.enumerateAttribute(NSAttributedString.Key.foregroundColor, in: descriptionWhiteColorRange) { value, range, stop in
            if let foregroundColor = value as? UIColor {
                if #available(iOS 13.0, *) {
                    XCTAssertEqual(descriptionWhiteColor.resolvedColor(with: lightModeTraitCollection).cgColor, foregroundColor.resolvedColor(with: lightModeTraitCollection).cgColor)
                    XCTAssertEqual(descriptionBlackColor.resolvedColor(with: darkModeTraitCollection).cgColor, foregroundColor.resolvedColor(with: darkModeTraitCollection).cgColor)
                } else {
                    XCTAssertEqual(descriptionWhiteColor.cgColor, foregroundColor.cgColor)
                }
            }
        }
        
        view.model?.description.enumerateAttribute(NSAttributedString.Key.backgroundColor, in: descriptionGreenColorRange) { value, range, stop in
            if let backgroundColor = value as? UIColor {
                descriptionGreenColorAssert = descriptionGreenColor.cgColor == backgroundColor.cgColor
            }
        }
        
        XCTAssertTrue(descriptionGreenColorAssert)
    }
    
    func testDidNextStepShouldExecuteCreateStudentAccount() {
        let validPromoCode =  ValidPromoCode.mock(type: .studentAccount)
        presenter.didNextStep(action: .createStudentAccount(validPromoCode: validPromoCode))
        XCTAssertTrue(coordinator.createStudentAcctountActionCalled)
    }
}

private final class SpyRegisterStudentPromoCode: UIViewController, RegisterStudentPromoCodeDisplay {
    private(set) var model: RegisterStudentPromoCodeModel?
    
    func displayModel(_ model: RegisterStudentPromoCodeModel) {
        self.model = model
    }
}


private final class MockRegisterStudentPromoCodeCoordinating: RegisterStudentPromoCodeCoordinating {
    var delegate: RegisterStudentPromoCodeCoordinatorDelegate?
    
    fileprivate(set) var viewController: UIViewController?
    private(set) var createStudentAcctountActionCalled = false
    
    func perform(action: RegisterStudentPromoCodeAction) {
        createStudentAcctountActionCalled = true
    }
}
