@testable import PicPay
import XCTest

final class RegisterStudentPromoCodeViewModelTests: XCTestCase {
    private lazy var viewModel: RegisterStudentPromoCodeViewModelInputs = {
        let viewModel = RegisterStudentPromoCodeViewModel(
            presenter: presenter,
            validPromoCode: validPromoCode
        )
        return viewModel
    }()
    
    private let validPromoCode = ValidPromoCode.mock(type: .studentAccount, validUrl: false)
    private let presenter = SpyRegisterStutendPromoCodePresenter()

    func testBuildViewModelCalled() {
        viewModel.viewDidLoad()
        XCTAssertTrue(presenter.isBuildModelCalled)
    }
    
    func testCreateStudentAccountAction() {
        viewModel.createStudentAccountAction()
        XCTAssertTrue(presenter.isDidNextStepCalled)
        XCTAssertNotNil(presenter.actionCalled)
        XCTAssertTrue(presenter.actionCalled == RegisterStudentPromoCodeAction.createStudentAccount(validPromoCode: validPromoCode))
    }
}

extension RegisterStudentPromoCodeAction: Equatable {
    public static func ==(lhs: RegisterStudentPromoCodeAction, rhs: RegisterStudentPromoCodeAction) -> Bool {
        switch (lhs, rhs) {
        case let (.createStudentAccount(lhsValidPromoCode), .createStudentAccount(rhsValidPromoCode)):
            return lhsValidPromoCode.type == rhsValidPromoCode.type && lhsValidPromoCode.data.referralCode == rhsValidPromoCode.data.referralCode
        }
    }
}

private final class SpyRegisterStutendPromoCodePresenter: RegisterStudentPromoCodePresenting {
    fileprivate(set) var viewController: RegisterStudentPromoCodeDisplay?
    private(set) var isBuildModelCalled: Bool = false
    private(set) var isDidNextStepCalled: Bool = false
    private(set) var actionCalled: RegisterStudentPromoCodeAction?
    
    func buildModel(_ response: PromoCodeValidationData) {
        isBuildModelCalled = true
    }
    
    func didNextStep(action: RegisterStudentPromoCodeAction) {
        isDidNextStepCalled = true
        actionCalled = action
    }
}
