@testable import PicPay
import XCTest

private final class MockRegisterStudentPromoCodeCoordinatorDelegate: RegisterStudentPromoCodeCoordinatorDelegate {
    private(set) var callPresentUserRegistrationCount = 0
    private(set) var promoCode: String?
    
    func presentUserRegistration(with promoCode: ValidPromoCode) {
        callPresentUserRegistrationCount += 1
        self.promoCode = promoCode.data.referralCode
    }
}

final class RegisterStudentPromoCodeCoordinatorTests: XCTestCase {
    private lazy var viewController: UIViewController = {
        let viewController = UIViewController()
        return viewController
    }()
    
    private lazy var navigationController: NavigationControllerMock = {
        let navigationController = NavigationControllerMock(rootViewController: viewController)
        return navigationController
    }()
    
    private lazy var coordinatorDelegate = MockRegisterStudentPromoCodeCoordinatorDelegate()
    
    private lazy var coordinator: RegisterStudentPromoCodeCoordinating = {
        let coordinator = RegisterStudentPromoCodeCoordinator()
        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        return coordinator
    }()
    
    override func setUp() {
        navigationController.loadViewIfNeeded()
        super.setUp()
    }
    
    func testPerform_WhenPassedUnderstandAction_ShouldPresentUserRegistration() {
        let validPromoCode = ValidPromoCode.mock()
        
        coordinator.perform(action: .createStudentAccount(validPromoCode: ValidPromoCode.mock()))
        
        XCTAssertTrue(navigationController.isPushViewControllerCalled)
        XCTAssertEqual(coordinatorDelegate.callPresentUserRegistrationCount, 1)
        XCTAssertEqual(coordinatorDelegate.promoCode, validPromoCode.data.referralCode)
    }
}
