import Core
import FeatureFlag
import OHHTTPStubs
import XCTest

@testable import PicPay

final class StudentAvailabilityServiceTests: XCTestCase {
    private var featureManagerMock = FeatureManagerMock()
    private let kvStoreMock = KVStoreMock()
    
    private lazy var sut: StudentAvailabilityService = {
        let container = DependencyContainerMock(featureManagerMock, kvStoreMock)
        return StudentAvailabilityService(dependencies: container)
    }()
    
    override func setUp() {
        super.setUp()
        OHHTTPStubs.removeAllStubs()
    }
    
    func testHasStudentSignUpFlow_WhenFeatureFlagIsTrue_ShouldReturnTrue() {
        featureManagerMock.override(key: .featureStudentSignUpFlow, with: true)
        
        XCTAssertTrue(sut.hasStudentSignUpFlow)
    }
    
    func testHasStudentSignUpFlow_WhenFeatureFlagIsFalse_ShouldReturnFalse() {
        featureManagerMock.override(key: .featureStudentSignUpFlow, with: false)
        
        XCTAssertFalse(sut.hasStudentSignUpFlow)
    }
    
    func testSaveStudentValidatedApiId_WhenPassingString_ShouldSaveOnKvStore() {
        let expectedValidatedApiId = "1"
        
        sut.saveStudentValidatedApiId(expectedValidatedApiId)
        
        XCTAssertEqual(kvStoreMock.stringFor(UniversityAccountKey.consumerStudentValidatedApiId), expectedValidatedApiId)
    }
    
    func testCheckConsumerStudentAvailability_WhenCallReturnsFailure_ShouldReturnError() {
        let expectation = XCTestExpectation(description: "Expected to receive error")

        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.fail()
        }
        
        sut.checkConsumerStudentAvailability(cpf: "", dateOfBirth: "") { result in
            switch result {
            case .success:
                XCTFail("Expected to fail")
            case .failure:
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testCheckConsumerStudentAvailability_WhenCallReturnsSuccessResponse_ShouldReturnSuccess() {
        let successResponse: [String: Any] = ["validate_student": true, "validate_api_id": "1"]
        let expectation = XCTestExpectation(description: "Expected to receive success")

        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(with: successResponse)
        }
        
        sut.checkConsumerStudentAvailability(cpf: "", dateOfBirth: "") { result in
            switch result {
            case .success:
                expectation.fulfill()
            case .failure:
                XCTFail("Expected success")
            }
        }
        
        wait(for: [expectation], timeout: 1.0)
    }
}
