@testable import PicPay
import Core
import XCTest

private final class StudentAvailabilityServicingMock: StudentAvailabilityServicing {
    var checkConsumerStudentAvailabilityResult: Result<StudentAvailabilityResponse, ApiError>?
    
    private(set) var saveStudentValidatedApiIdCount = 0
    private(set) var checkConsumerStudentAvailabilityCount = 0
    
    private(set) var validatedApiIdStored: String?
    private(set) var cpfStored: String?
    private(set) var dateOfBirthStored: String?
    
    var hasStudentSignUpFlow: Bool = false

    func saveStudentValidatedApiId(_ validatedApiId: String) {
        saveStudentValidatedApiIdCount += 1
        validatedApiIdStored = validatedApiId
    }
    
    func checkConsumerStudentAvailability(
        cpf: String,
        dateOfBirth: String,
        completion: @escaping (Result<StudentAvailabilityResponse, ApiError>) -> Void
    ) {
        guard let checkConsumerStudentAvailabilityResult = checkConsumerStudentAvailabilityResult else {
            return
        }
        
        checkConsumerStudentAvailabilityCount += 1
        cpfStored = cpf
        dateOfBirthStored = dateOfBirth
        completion(checkConsumerStudentAvailabilityResult)
    }
}


final class StudentAvailabilityInteractorTests: XCTestCase {
    private lazy var serviceMock = StudentAvailabilityServicingMock()
    
    private lazy var sut = StudentAvailabilityInteractor(service: serviceMock)
    
    func testCheckConsumerStudentAvailability_WhenStudentSignUpFlowIsOff_ShouldDoNothing() {
        serviceMock.hasStudentSignUpFlow = false
        
        sut.checkConsumerStudentAvailability(cpf: "289.255.357-18", dateOfBirth: "14/06/1965")
        
        XCTAssertEqual(serviceMock.checkConsumerStudentAvailabilityCount, 0)
        XCTAssertNil(serviceMock.cpfStored)
        XCTAssertNil(serviceMock.dateOfBirthStored)
        XCTAssertEqual(serviceMock.saveStudentValidatedApiIdCount, 0)
        XCTAssertNil(serviceMock.validatedApiIdStored)
    }
    
    func testCheckConsumerStudentAvailability_WhenHasStudentSignUpFlowAndConsumerIsNotStudent_ShouldNotSaveStudentValidatedApiId() {
        let expectedCpf = "289.255.357-18"
        let expectedDateOfBirth = "14/06/1965"
        let expectedResponse = StudentAvailabilityResponse(isStudent: false, validatedApiId: nil)
        
        serviceMock.hasStudentSignUpFlow = true
        serviceMock.checkConsumerStudentAvailabilityResult = .success(expectedResponse)
        
        sut.checkConsumerStudentAvailability(cpf: expectedCpf, dateOfBirth: expectedDateOfBirth)
        
        XCTAssertEqual(serviceMock.checkConsumerStudentAvailabilityCount, 1)
        XCTAssertEqual(serviceMock.cpfStored, expectedCpf)
        XCTAssertEqual(serviceMock.dateOfBirthStored, expectedDateOfBirth)
        XCTAssertEqual(serviceMock.saveStudentValidatedApiIdCount, 0)
        XCTAssertNil(serviceMock.validatedApiIdStored)
    }
    func testCheckConsumerStudentAvailability_WhenHasStudentSignUpFlowAndResponseFailed_ShouldNotSaveStudentValidatedApiId() {
        let expectedCpf = "289.255.357-18"
        let expectedDateOfBirth = "14/06/1965"
        
        serviceMock.hasStudentSignUpFlow = true
        serviceMock.checkConsumerStudentAvailabilityResult = .failure(ApiError.serverError)
        
        sut.checkConsumerStudentAvailability(cpf: expectedCpf, dateOfBirth: expectedDateOfBirth)
        
        XCTAssertEqual(serviceMock.checkConsumerStudentAvailabilityCount, 1)
        XCTAssertEqual(serviceMock.cpfStored, expectedCpf)
        XCTAssertEqual(serviceMock.dateOfBirthStored, expectedDateOfBirth)
        XCTAssertEqual(serviceMock.saveStudentValidatedApiIdCount, 0)
        XCTAssertNil(serviceMock.validatedApiIdStored)
    }
    
    func testCheckConsumerStudentAvailability_WhenHasStudentSignUpFlowAndConsumerIsStudent_ShouldSaveStudentValidatedApiId() {
        let expectedCpf = "289.255.357-18"
        let expectedDateOfBirth = "14/06/1965"
        let expectedValidatedApiId = "1"
        let expectedResponse = StudentAvailabilityResponse(isStudent: true, validatedApiId: expectedValidatedApiId)
        
        serviceMock.hasStudentSignUpFlow = true
        serviceMock.checkConsumerStudentAvailabilityResult = .success(expectedResponse)
        
        sut.checkConsumerStudentAvailability(cpf: expectedCpf, dateOfBirth: expectedDateOfBirth)
        
        XCTAssertEqual(serviceMock.checkConsumerStudentAvailabilityCount, 1)
        XCTAssertEqual(serviceMock.cpfStored, expectedCpf)
        XCTAssertEqual(serviceMock.dateOfBirthStored, expectedDateOfBirth)
        XCTAssertEqual(serviceMock.saveStudentValidatedApiIdCount, 1)
        XCTAssertEqual(serviceMock.validatedApiIdStored, expectedValidatedApiId)
    }
}
