@testable import PicPay
@testable import Registration
import XCTest
import FeatureFlag

private final class RegistrationAddConsumerHelperMock: RegistrationAddConsumerHelperContract {
    private(set) var callAddConsumerCount = 0
    private(set) var callAddComplianceConsumerCount = 0
    private(set) var consumerModel: RegistrationModel?
    private(set) var statistics: RegistrationStatisticsModel?
    
    var result: Result<Void, Error> = .success
    var complianceResult: Result<RegistrationComplianceModel, Error> = .success(RegistrationComplianceModel(
        token: "someToken",
        mgm: nil,
        tokenType: .temporary,
        step: .success)
    )

    func addConsumer(with consumerModel: RegistrationModel, statistics: RegistrationStatisticsModel, completion: @escaping (Result<Void, Error>) -> Void) {
        callAddConsumerCount += 1
        self.consumerModel = consumerModel
        self.statistics = statistics
        completion(result)
    }
    
    func addComplianceConsumer(
        with consumerModel: RegistrationModel,
        statistics: RegistrationStatisticsModel,
        completion: @escaping (Result<RegistrationComplianceModel, Error>) -> Void
    ) {
        callAddComplianceConsumerCount += 1
        self.consumerModel = consumerModel
        self.statistics = statistics
        
        completion(complianceResult)
    }
}

private final class RegistrationAccountSetupMock: RegistrationAccountSetupContract {
    private(set) var callSetupAccountCount = 0
    private(set) var model: RegistrationAccountSetupModel?
    
    func setupAccount(model: RegistrationAccountSetupModel){
        callSetupAccountCount += 1
        self.model = model
    }
}

final class RegistrationCacheHelperMock: RegistrationCacheHelperContract {
    private(set) var callStoreFinishedStepsCount = 0
    private(set) var callCleanCacheDataCount = 0
    
    private(set) var storedRegistrationModel: RegistrationModel?
    private(set) var storedStatistics: RegistrationStatisticsModel?
    private(set) var storedStepSequence: [RegistrationStep]?
    private(set) var storedInterruptedStep: RegistrationStep?
    
    var cache: RegistrationCache?
    
    func storeFinishedSteps(model: RegistrationModel, statistics: RegistrationStatisticsModel, stepSequence: [RegistrationStep], interruptedStep: RegistrationStep) {
        callStoreFinishedStepsCount += 1
        storedRegistrationModel = model
        storedStatistics = statistics
        storedStepSequence = stepSequence
        storedInterruptedStep = interruptedStep
    }
    
    func cleanCachedData() {
        callCleanCacheDataCount += 1
    }
}

final class StudentAvailabilityInteractorMock: StudentAvailabilityInteractorContract {
    private(set) var callCheckConsumerStudentAvailabilityCount = 0
    
    private(set) var storedCpf: String?
    private(set) var storedDateOfBirth: String?
    
    func checkConsumerStudentAvailability(cpf: String, dateOfBirth: String) {
        callCheckConsumerStudentAvailabilityCount += 1
        storedCpf = cpf
        storedDateOfBirth = dateOfBirth
    }
}

final class RegistrationFlowCoordinatorTests: XCTestCase {
    private let featureManagerMock = FeatureManagerMock()
    private lazy var dependencies = DependencyContainerMock(featureManagerMock)
    private let addConsumerHelper = RegistrationAddConsumerHelperMock()
    private let navController = NavigationControllerMock()
    private let stepsMock: [RegistrationStep] = [.name, .phone, .email, .password, .document]
    private let registrationCacheHelperMock = RegistrationCacheHelperMock()
    private let studentAvailabilityInteractorMock = StudentAvailabilityInteractorMock()
    private let registrationSetupMock = RegistrationAccountSetupMock()
    private lazy var cachedRegistrationMock = RegistrationCache(
        model: RegistrationModel(),
        statistics: RegistrationStatisticsModel(),
        stepSequence: stepsMock,
        interruptedStep: .name
    )
    
    func createRegistrationFlowCoordinator(steps: [RegistrationStep], continueRegistration: Bool = false) -> RegistrationFlowCoordinator {
        featureManagerMock.override(
            with: [
                .continueRegistration: continueRegistration,
                .dynamicRegistrationStepsSequence: steps,
                .registrationCompliance: false
        ])
        
        return RegistrationFlowCoordinator(
            dependencies: dependencies,
            addConsumerHelper: addConsumerHelper,
            registrationCacheHelper: registrationCacheHelperMock,
            studentAvailabilityInteractor: studentAvailabilityInteractorMock,
            navigationController: navController,
            registrationAccountSetupContract: registrationSetupMock
        )
    }
    
    func createRegistrationFlowCoordinatorForCachedRegistration() -> RegistrationFlowCoordinator {
        featureManagerMock.override(key: .continueRegistration, with: true)
        return RegistrationFlowCoordinator(
            dependencies: dependencies,
            addConsumerHelper: addConsumerHelper,
            navigationController: navController,
            cachedRegistration: cachedRegistrationMock,
            registrationAccountSetupContract: registrationSetupMock
        )
    }
    
    func testStart_WhenRemoteConfigStepsAreNil_ShouldPresentDefaultNameStep() throws {
        featureManagerMock.override(key: .dynamicRegistrationStepsSequence, with: [RegistrationStep]())
        
        let sut = RegistrationFlowCoordinator(
            dependencies: dependencies,
            addConsumerHelper: addConsumerHelper,
            registrationCacheHelper: registrationCacheHelperMock,
            navigationController: navController,
            registrationAccountSetupContract: registrationSetupMock
        )
        sut.start()
        
        XCTAssert(navController.isPushViewControllerCalled)
        let pushedViewController = try XCTUnwrap(navController.pushedViewController)
        XCTAssert(pushedViewController is RegistrationNameViewController)
    }
    
    func testStart_WhenNameIsFirstStep_ShouldPresentNameStep() throws {
        let sut = createRegistrationFlowCoordinator(steps: [.name, .phone, .email, .password, .document])
        
        sut.start()
        
        XCTAssert(navController.isPushViewControllerCalled)
        let pushedViewController = try XCTUnwrap(navController.pushedViewController)
        XCTAssert(pushedViewController is RegistrationNameViewController)
    }
    
    func testStart_WhenPhoneIsFirstStep_ShouldPresentPhoneStep() throws {
        let sut = createRegistrationFlowCoordinator(steps: [.phone, .name, .email, .password, .document])
        
        sut.start()
        
        XCTAssert(navController.isPushViewControllerCalled)
        let pushedViewController = try XCTUnwrap(navController.pushedViewController)
        XCTAssert(pushedViewController is RegistrationPhoneNumberViewController)
    }
    
    func testStart_WhenEmailIsFirstStep_ShouldPresentEmailStep() throws {
        let sut = createRegistrationFlowCoordinator(steps: [.email, .phone, .name, .password, .document])
        
        sut.start()
        
        XCTAssert(navController.isPushViewControllerCalled)
        let pushedViewController = try XCTUnwrap(navController.pushedViewController)
        XCTAssert(pushedViewController is RegistrationEmailViewController)
    }
    
    func testStart_WhenPasswordIsFirstStep_ShouldPresentPasswordStep() throws {
        let sut = createRegistrationFlowCoordinator(steps: [.password, .phone, .name, .email, .document])
        
        sut.start()
        
        XCTAssert(navController.isPushViewControllerCalled)
        let pushedViewController = try XCTUnwrap(navController.pushedViewController)
        XCTAssert(pushedViewController is RegistrationPasswordViewController)
    }
    
    func testStart_WhenDocumentIsFirstStep_ShouldPresentDocumentStep() throws {
        let sut = createRegistrationFlowCoordinator(steps: [.document, .phone, .name, .email, .password])
        
        sut.start()
        
        XCTAssert(navController.isPushViewControllerCalled)
        let pushedViewController = try XCTUnwrap(navController.pushedViewController)
        XCTAssert(pushedViewController is RegistrationDocumentViewController)
    }
    
    func testStart_WhenTheListOfStepsIsEmpty_ShouldPresentDefaultFirstRegistrationSteps() throws {
        let sut = createRegistrationFlowCoordinator(steps: [])
        
        sut.start()
        
        let pushedViewController = try XCTUnwrap(navController.pushedViewController)
        XCTAssert(pushedViewController is RegistrationNameViewController)
    }
    
    func testMoveToNextStep_WhenOnLastStepAndPassingFirstAndLastName_ShouldFinishRegistration() {
        let sut = createRegistrationFlowCoordinator(steps: [.name])
        let firstName = "Fulano"
        let lastName = "Silva de Oliveira"
        
        sut.start()
        sut.moveToNextStep(withFirstName: firstName, lastName: lastName)
        
        XCTAssertEqual(addConsumerHelper.callAddConsumerCount, 1)
        XCTAssertEqual(addConsumerHelper.consumerModel?.firstName, firstName)
        XCTAssertEqual(addConsumerHelper.consumerModel?.lastName, lastName)
    }
    
    func testMoveToNextStep_WhenOnLastStepAndPassingDDDAndPhoneNumber_ShouldFinishRegistration() {
        let sut = createRegistrationFlowCoordinator(steps: [.phone])
        let phoneNumber = "95864-9383"
        let ddd = "27"
        
        sut.start()
        sut.moveToNextStep(withDdd: ddd, phoneNumber: phoneNumber)
        
        XCTAssertEqual(addConsumerHelper.callAddConsumerCount, 1)
        XCTAssertEqual(addConsumerHelper.consumerModel?.phoneNumber, phoneNumber)
        XCTAssertEqual(addConsumerHelper.consumerModel?.ddd, ddd)
    }
    
    func testMoveToNextStep_WhenOnLastStepAndPassingPassword_ShouldFinishRegistration() {
        let sut = createRegistrationFlowCoordinator(steps: [.password])
        let password = String(Int.random(in: 1000...9999))
        
        sut.start()
        sut.moveToNextStep(withPassword: password)
        
        XCTAssertEqual(addConsumerHelper.callAddConsumerCount, 1)
        XCTAssertEqual(addConsumerHelper.consumerModel?.password, password)
    }
    
    func testMoveToNextStep_WhenOnLastStepAndPassingEmail_ShouldFinishRegistration() {
        let sut = createRegistrationFlowCoordinator(steps: [.email])
        let email = "junior.silva@picpay.com"
        let invalidEmailCounter = Int.random(in: 0...100)
        
        sut.start()
        sut.moveToNextStep(withEmail: email, invalidEmailCounter: invalidEmailCounter)
        
        XCTAssertEqual(addConsumerHelper.callAddConsumerCount, 1)
        XCTAssertEqual(addConsumerHelper.consumerModel?.email, email)
        XCTAssertEqual(addConsumerHelper.statistics?.invalidEmailCounter, invalidEmailCounter)
    }
    
    func testMoveToNextStep_WhenOnLastStepAndPassingCpfAndDateOfBirth_ShouldFinishRegistrationAndCheckConsumerStudentAvailability() {
        let sut = createRegistrationFlowCoordinator(steps: [.document])
        let cpf = "123.578.642-46"
        let dateOfBirth = "10/05/1986"
        let invalidCpfCounter = Int.random(in: 0...100)
        let copyPasteCpf = true
        
        sut.start()
        sut.moveToNextStep(withCpf: cpf, dateOfBirth: dateOfBirth, invalidCpfCounter: invalidCpfCounter, copyPasteCpf: copyPasteCpf)
        
        XCTAssertEqual(addConsumerHelper.callAddConsumerCount, 1)
        XCTAssertEqual(addConsumerHelper.consumerModel?.cpf, cpf)
        XCTAssertEqual(addConsumerHelper.consumerModel?.dateOfBirth, dateOfBirth)
        XCTAssertEqual(addConsumerHelper.statistics?.invalidCpfCounter, invalidCpfCounter)
        XCTAssertEqual(addConsumerHelper.statistics?.copyPasteCpf, copyPasteCpf)
        
        XCTAssertEqual(studentAvailabilityInteractorMock.callCheckConsumerStudentAvailabilityCount, 1)
        XCTAssertEqual(studentAvailabilityInteractorMock.storedCpf, cpf)
        XCTAssertEqual(studentAvailabilityInteractorMock.storedDateOfBirth, dateOfBirth)
    }
    
    func testMoveToNextStep_WhenNextStepIsName_ShouldPresentNameStep() {
        let sut = createRegistrationFlowCoordinator(steps: [.document, .name])
        
        sut.start()
        sut.moveToNextStep(withCpf: "", dateOfBirth: "", invalidCpfCounter: 0, copyPasteCpf: false)
        
        XCTAssert(navController.isPushViewControllerCalled)
        XCTAssert(navController.pushedViewController is RegistrationNameViewController)
    }
    
    func testMoveToNextStep_WhenNextStepIsPhone_ShouldPresentPhoneStep() {
        let sut = createRegistrationFlowCoordinator(steps: [.name, .phone])
        
        sut.start()
        sut.moveToNextStep(withFirstName: "", lastName: "")
        
        XCTAssert(navController.isPushViewControllerCalled)
        XCTAssert(navController.pushedViewController is RegistrationPhoneNumberViewController)
    }
    
    func testMoveToNextStep_WhenNextStepIsEmail_ShouldPresentEmailStep() {
        let sut = createRegistrationFlowCoordinator(steps: [.phone, .email])
        
        sut.start()
        sut.moveToNextStep(withDdd: "", phoneNumber: "")
        
        XCTAssert(navController.isPushViewControllerCalled)
        XCTAssert(navController.pushedViewController is RegistrationEmailViewController)
    }
    
    func testMoveToNextStep_WhenNextStepIsPassword_ShouldPresentPasswordStep() {
        let sut = createRegistrationFlowCoordinator(steps: [.email, .password])
        
        sut.start()
        sut.moveToNextStep(withEmail: "", invalidEmailCounter: 0)
        
        XCTAssert(navController.isPushViewControllerCalled)
        XCTAssert(navController.pushedViewController is RegistrationPasswordViewController)
    }
    
    func testMoveToNextStep_WhenNextStepIsDocument_ShouldPresentDocumentStep() {
        let sut = createRegistrationFlowCoordinator(steps: [.password, .document])
        
        sut.start()
        sut.moveToNextStep(withPassword: "")
        
        XCTAssert(navController.isPushViewControllerCalled)
        XCTAssert(navController.pushedViewController is RegistrationDocumentViewController)
    }
    
    func testMoveToNextStep_WhenOnLastStep_ShouldPresentUsernameStep() {
        let sut = createRegistrationFlowCoordinator(steps: [.password])
        addConsumerHelper.result = .success
        
        sut.start()
        sut.moveToNextStep(withPassword: "")
        
        XCTAssert(navController.isSetViewControllersCalled)
        XCTAssert(navController.topViewController is RegistrationUsernameViewController)
    }
    
    func testMoveToNextStep_WhenOnLastStepOnErrorAddingUser_ShouldPresentError() {
        let sut = createRegistrationFlowCoordinator(steps: [.password])
        addConsumerHelper.result = .failure(MockError.genericError)
        
        sut.start()
        sut.moveToNextStep(withPassword: "")
        
        XCTAssert(navController.isPresentViewControllerCalled)
        XCTAssert(navController.viewControllerPresented is AlertPopupViewController)
    }
    
    func testMoveToNextStep_WhenNextStepIsDocument_ShouldStoreFinishedSteps() throws {
        let mockedSteps: [RegistrationStep] = [.name, .password, .phone, .document]
        let sut = createRegistrationFlowCoordinator(steps: mockedSteps, continueRegistration: true)
        
        sut.start()
        sut.moveToNextStep(withFirstName: "João Antônio", lastName: "Ribeiro")
        sut.moveToNextStep(withPassword: "15770")
        sut.moveToNextStep(withDdd: "29", phoneNumber: "95956-6997")
        
        let model = try XCTUnwrap(registrationCacheHelperMock.storedRegistrationModel)
        let statistics = try XCTUnwrap(registrationCacheHelperMock.storedStatistics)
        let steps = try XCTUnwrap(registrationCacheHelperMock.storedStepSequence)
        let interruptedStep = registrationCacheHelperMock.storedInterruptedStep
        
        XCTAssertEqual(registrationCacheHelperMock.callStoreFinishedStepsCount, 3)
        XCTAssert(mockedSteps.elementsEqual(steps))
        XCTAssertEqual(interruptedStep, .document)
        XCTAssertEqual(model.firstName, "João Antônio")
        XCTAssertEqual(model.lastName, "Ribeiro")
        XCTAssertEqual(model.password, "15770")
        XCTAssertEqual(model.ddd, "29")
        XCTAssertEqual(model.phoneNumber, "95956-6997")
        XCTAssertNil(model.email)
        XCTAssertNil(model.cpf)
        XCTAssertNil(model.dateOfBirth)
        XCTAssertFalse(statistics.copyPasteCpf)
        XCTAssertEqual(statistics.invalidCpfCounter, 0)
        XCTAssertEqual(statistics.invalidEmailCounter, 0)
    }
    
    func testMoveToNextStep_WhenNextStepIsPhone_ShouldStoreFinishedSteps() throws {
        let mockedSteps: [RegistrationStep] = [.email, .document, .phone]
        let sut = createRegistrationFlowCoordinator(steps: mockedSteps, continueRegistration: true)
        
        sut.start()
        sut.moveToNextStep(withEmail: "joao.antonio@picpay.com", invalidEmailCounter: 6)
        sut.moveToNextStep(withCpf: "119.433.466-53", dateOfBirth: "01/10/1967", invalidCpfCounter: 3, copyPasteCpf: true)
        
        let model = try XCTUnwrap(registrationCacheHelperMock.storedRegistrationModel)
        let statistics = try XCTUnwrap(registrationCacheHelperMock.storedStatistics)
        let steps = try XCTUnwrap(registrationCacheHelperMock.storedStepSequence)
        let interruptedStep = registrationCacheHelperMock.storedInterruptedStep
        
        XCTAssertEqual(registrationCacheHelperMock.callStoreFinishedStepsCount, 2)
        XCTAssert(mockedSteps.elementsEqual(steps))
        XCTAssertEqual(interruptedStep, .phone)
        XCTAssertEqual(model.email, "joao.antonio@picpay.com")
        XCTAssertEqual(model.cpf, "119.433.466-53")
        XCTAssertEqual(model.dateOfBirth, "01/10/1967")
        XCTAssertNil(model.firstName)
        XCTAssertNil(model.lastName)
        XCTAssertNil(model.password)
        XCTAssertNil(model.ddd)
        XCTAssertNil(model.phoneNumber)
        XCTAssert(statistics.copyPasteCpf)
        XCTAssertEqual(statistics.invalidCpfCounter, 3)
        XCTAssertEqual(statistics.invalidEmailCounter, 6)
    }
    
    func testMoveToNextStep_WhenNextContinueStepFlagIsOff_ShouldNotStoreFinishedSteps() throws {
        let mockedSteps: [RegistrationStep] = [.name, .password, .phone, .document]
        let sut = createRegistrationFlowCoordinator(steps: mockedSteps, continueRegistration: false)
        
        sut.start()
        sut.moveToNextStep(withFirstName: "João Antônio", lastName: "Ribeiro")
        
        XCTAssertNil(registrationCacheHelperMock.storedRegistrationModel)
    }
    
    func testMoveToLogin_ShouldPresentLoginScreen() {
        let initialController = SignUpFactory.make()
        navController.viewControllers = [initialController]
        let sut = createRegistrationFlowCoordinator(steps: [.password, .document])
        
        sut.start()
        sut.moveToNextStep(withPassword: "")
        sut.moveToLogin()
        
        XCTAssert(navController.isPushViewControllerCalled)
        XCTAssert(navController.isPopToRootViewControllerCalled)
        XCTAssert(navController.pushedViewController is LoginViewController)
    }
    
    func testShouldPop_WhenPreviewsStepIsPhone_ShoudPresentAlertPopup() {
        let sut = createRegistrationFlowCoordinator(steps: [.phone, .name])
        sut.start()
        sut.moveToNextStep(withDdd: "", phoneNumber: "")
        sut.moveToPreviousRegistrationStep()
        
        XCTAssertTrue(navController.isPresentViewControllerCalled)
        XCTAssertTrue(navController.viewControllerPresented is AlertPopupViewController)
    }
    
    func testShouldPop_WhenPreviewsStepIsNotPhone_ShoutNotPresentAlertPopup() {
        let sut = createRegistrationFlowCoordinator(steps: [.password, .email])
        sut.start()
        sut.moveToNextStep(withPassword: "")
        sut.moveToPreviousRegistrationStep()
        
        XCTAssertFalse(navController.viewControllerPresented is AlertPopupViewController)
    }
    
    func testShouldPop_WhenCurrentStepIsPhone_ShoutNotPresentAlertPopup() {
        let sut = createRegistrationFlowCoordinator(steps: [.email, .phone])
        sut.start()
        sut.moveToNextStep(withPassword: "")
        sut.moveToPreviousRegistrationStep()
        
        XCTAssertFalse(navController.viewControllerPresented is AlertPopupViewController)
    }
    
    // Continue registration feature
    
    func testStartOnTopOf_WhenRegistrationInterruptedOnFirstStep_ShouldPresentFirstStep() {
        cachedRegistrationMock.stepSequence = [.name]
        cachedRegistrationMock.interruptedStep = .name
        let sut = createRegistrationFlowCoordinatorForCachedRegistration()
        let initialViewController = UIViewController()
        sut.start(onTopOf: initialViewController)
        
        XCTAssert(navController.isSetViewControllersCalled)
        XCTAssertEqual(navController.viewControllers.count, 2)
        XCTAssertEqual(navController.viewControllers.first, initialViewController)
        XCTAssert(navController.topViewController is RegistrationNameViewController)
    }
    
    func testStartOnTopOf_WhenRegistrationInterruptedOnSecondStep_ShouldPresentSecondStep() {
        cachedRegistrationMock.stepSequence = [.document, .password]
        cachedRegistrationMock.interruptedStep = .password
        let sut = createRegistrationFlowCoordinatorForCachedRegistration()
        let initialViewController = UIViewController()
        sut.start(onTopOf: initialViewController)
        
        XCTAssert(navController.isSetViewControllersCalled)
        XCTAssertEqual(navController.viewControllers.count, 3)
        XCTAssertEqual(navController.viewControllers.first, initialViewController)
        XCTAssert(navController.viewControllers[1] is RegistrationDocumentViewController)
        XCTAssert(navController.topViewController is RegistrationPasswordViewController)
    }
    
    func testStartOnTopOf_WhenRegistrationInterruptedOnNameStepAsThrirdStep_ShouldPresentThirdStep() {
        cachedRegistrationMock.stepSequence = [.password, .email, .document, .name]
        cachedRegistrationMock.interruptedStep = .document
        let sut = createRegistrationFlowCoordinatorForCachedRegistration()
        let initialViewController = UIViewController()
        sut.start(onTopOf: initialViewController)
        
        XCTAssert(navController.isSetViewControllersCalled)
        XCTAssertEqual(navController.viewControllers.count, 4)
        XCTAssertEqual(navController.viewControllers.first, initialViewController)
        XCTAssert(navController.viewControllers[1] is RegistrationPasswordViewController)
        XCTAssert(navController.viewControllers[2] is RegistrationEmailViewController)
        XCTAssert(navController.topViewController is RegistrationDocumentViewController)
    }
    
    func testStartOnTopOf_WhenIsNewRegistration_ShouldPresentFirstStep() {
        let stepSequence: [RegistrationStep] = [.password, .email, .document, .name]
        featureManagerMock.override(with: [.dynamicRegistrationStepsSequence: stepSequence])
        
        let sut = RegistrationFlowCoordinator(
            dependencies: dependencies,
            addConsumerHelper: addConsumerHelper,
            registrationCacheHelper: registrationCacheHelperMock,
            navigationController: navController
        )
        let initialViewController = UIViewController()
        sut.start(onTopOf: initialViewController)
        
        XCTAssert(navController.isSetViewControllersCalled)
        XCTAssertEqual(navController.viewControllers.count, 2)
        XCTAssertEqual(navController.viewControllers.first, initialViewController)
        XCTAssert(navController.viewControllers[1] is RegistrationPasswordViewController)
    }
    
    func testAccountSetup_WhenIsFlagActivated_ShouldCallAccountSetup() {
        let mockedSteps: [RegistrationStep] = [.name, .document, .phone]
        let invalidCpfCounter = Int.random(in: 0...100)
        
        let sut = createRegistrationFlowCoordinator(steps: mockedSteps)
        featureManagerMock.override(key: .registrationCompliance, with: true)

        sut.start()
        sut.moveToNextStep(withFirstName: "João Antônio", lastName: "Ribeiro")
        sut.moveToNextStep(
            withCpf: "588.176.241-01",
            dateOfBirth: "23/11/1999",
            invalidCpfCounter: invalidCpfCounter,
            copyPasteCpf: false
        )
        sut.moveToNextStep(withDdd: "11", phoneNumber:  "989616066")
        
        XCTAssertEqual(registrationSetupMock.callSetupAccountCount, 1)
        XCTAssertNotNil(registrationSetupMock.model)
    }
    
    func testAccountSetup_WhenIsFlagDeactivated_ShouldNotCallAccountSetup() {
        let mockedSteps: [RegistrationStep] = [.name, .document, .phone]
        let invalidCpfCounter = Int.random(in: 0...100)
        
        let sut = createRegistrationFlowCoordinator(steps: mockedSteps)
        featureManagerMock.override(key: .registrationCompliance, with: false)
        sut.start()
        sut.moveToNextStep(withFirstName: "João Antônio", lastName: "Ribeiro")
        sut.moveToNextStep(
            withCpf: "588.176.241-01",
            dateOfBirth: "23/11/1999",
            invalidCpfCounter: invalidCpfCounter,
            copyPasteCpf: false
        )
        sut.moveToNextStep(withDdd: "11", phoneNumber:  "989616066")
        
        XCTAssertEqual(registrationSetupMock.callSetupAccountCount, 0)
    }
    
    func testMoveToNextStep_WhenOnLastStepAndComplianceFlagActivated_ShouldFinishRegistratioWithCompliance() {
        let sut = createRegistrationFlowCoordinator(steps: [.password])
        let password = String(Int.random(in: 1000...9999))
        featureManagerMock.override(key: .registrationCompliance, with: true)
        
        sut.start()
        sut.moveToNextStep(withPassword: password)
        
        XCTAssertEqual(addConsumerHelper.callAddComplianceConsumerCount, 1)
        XCTAssertEqual(addConsumerHelper.callAddConsumerCount, 0)
        XCTAssertEqual(addConsumerHelper.consumerModel?.password, password)
    }
}
