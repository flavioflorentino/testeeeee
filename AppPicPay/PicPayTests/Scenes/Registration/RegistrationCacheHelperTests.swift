@testable import PicPay
import Core
import FeatureFlag
import XCTest

extension RegistrationModel: Equatable {
    public static func == (lhs: RegistrationModel, rhs: RegistrationModel) -> Bool {
        return lhs.firstName == rhs.firstName &&
            lhs.lastName == rhs.lastName &&
            lhs.email == rhs.email &&
            lhs.ddd == rhs.ddd &&
            lhs.phoneNumber == rhs.phoneNumber &&
            lhs.cpf == rhs.cpf &&
            lhs.dateOfBirth == rhs.dateOfBirth &&
            lhs.password == rhs.password &&
            lhs.username == rhs.username &&
            lhs.promoCode == rhs.promoCode
    }
}

extension RegistrationStatisticsModel: Equatable {
    public static func == (lhs: RegistrationStatisticsModel, rhs: RegistrationStatisticsModel) -> Bool {
        return lhs.invalidEmailCounter == rhs.invalidEmailCounter &&
            lhs.invalidCpfCounter == rhs.invalidCpfCounter &&
            lhs.copyPasteCpf == rhs.copyPasteCpf
    }
}

final class RegistrationCacheHelperTests: XCTestCase {
    private let featureManagerMock = FeatureManagerMock()
    private lazy var mockDependencies = DependencyContainerMock(featureManagerMock)
    private lazy var sut = RegistrationCacheHelper(dependencies: mockDependencies)
    private let mockedModel = RegistrationModel(
        firstName: "Maurício",
        lastName: "de Souza Braga",
        email: "mausouza@picpay.com",
        ddd: "29",
        phoneNumber: "95754-9765",
        phoneCode: "2345",
        cpf: "590.487.597-64",
        dateOfBirth: "08/07/1980",
        password: "0373643",
        username: "mauro.souza",
        promoCode: "PRGNES"
    )
    private let mockedStatistics = RegistrationStatisticsModel(
        invalidEmailCounter: 6,
        invalidCpfCounter: 9,
        copyPasteCpf: true
    )
    
    func testStoreFinishedSteps_WhenStepSequenceDoesNotChange_ShouldStorePassedParameters() throws {
        let stepSequence: [RegistrationStep] = [.name, .email, .password, .phone, .document]
        let interruptedStep: RegistrationStep = .document
        featureManagerMock.override(key: .dynamicRegistrationStepsSequence, with: stepSequence)
        
        sut.storeFinishedSteps(
            model: mockedModel,
            statistics: mockedStatistics,
            stepSequence: stepSequence,
            interruptedStep: interruptedStep
        )
        
        let cache = try XCTUnwrap(sut.cache)
        XCTAssertEqual(cache.model, mockedModel)
        XCTAssertEqual(cache.statistics, mockedStatistics)
        XCTAssert(cache.stepSequence.elementsEqual(stepSequence))
        XCTAssertEqual(cache.interruptedStep, interruptedStep)
    }
    
    func testStoreFinishedSteps_WhenStepSequenceDoesChange_ShouldStorePassedParameters() throws {
        let stepSequence: [RegistrationStep] = [.name, .email, .document, .password, .phone, .document]
        let interruptedStep: RegistrationStep = .document
        let currentStepSequence: [RegistrationStep] = [.name, .email, .document, .password, .phone]
        featureManagerMock.override(key: .dynamicRegistrationStepsSequence, with: currentStepSequence)
        
        sut.storeFinishedSteps(
            model: mockedModel,
            statistics: mockedStatistics,
            stepSequence: stepSequence,
            interruptedStep: interruptedStep
        )
        
        XCTAssertNil(sut.cache)
    }
    
    func testCleanCachedData_ShouldEraseAllRegistrationCacheData() {
        let stepSequence: [RegistrationStep] = [.name, .email, .password, .phone, .document]
        let interruptedStep: RegistrationStep = .document        
        featureManagerMock.override(key: .dynamicRegistrationStepsSequence, with: stepSequence)
        
        sut.storeFinishedSteps(
            model: mockedModel,
            statistics: mockedStatistics,
            stepSequence: stepSequence,
            interruptedStep: interruptedStep
        )
        sut.cleanCachedData()
        
        XCTAssertNil(sut.cache)
    }
}
