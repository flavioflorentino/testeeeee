import CorePayment
@testable import PicPay

final class PaymentActionsDelegateSpy: PaymentActionsDelegate {
    var paymentMethod: PaymentMethod?
    var installment: Int?
    var extraParams: Decodable?
    
    private(set) var callViewWillAppearCount = 0
    private(set) var callDidTapPayCount = 0
    private(set) var callDidTapPaymentCount = 0
    private(set) var callPaymentSuccessCount = 0
    private(set) var callPaymentFailuresCount = 0
    private(set) var callPaymentCancelCount = 0

    private(set) var wentToCards: Bool?
    private(set) var wentToInstallments: Bool?
    private(set) var transactionId: String?
    private(set) var value: Double?
    private(set) var paymentActionErrorViewModel: PaymentActionErrorViewModel?
    
    func viewWillAppear() {
        callViewWillAppearCount += 1
    }
    
    func didTapPay(privacyType: PaymentPrivacy, hasMessage: Bool, requestValue: String) {
        callDidTapPayCount += 1
    }
    
    func didTapPayment(wentToCards: Bool, wentToInstallments: Bool) {
        self.wentToCards = wentToCards
        self.wentToInstallments = wentToInstallments
        callDidTapPaymentCount += 1
    }
    
    func paymentSuccess(transactionId: String, value: Double) {
        self.transactionId  = transactionId
        self.value = value
        callPaymentSuccessCount += 1
    }
    
    func paymentFailure(error: PaymentActionErrorViewModel) {
        self.paymentActionErrorViewModel = error
        callPaymentFailuresCount += 1
    }
    
    func paymentCancel() {
        callPaymentCancelCount += 1
    }
}
