import CorePayment
import XCTest
@testable import PicPay

private final class PaymentToolbarLargeViewControllerSpy: PaymentToolbarLargeDisplay {
    private(set) var callCompletionPaymentMethod = 0
    private(set) var callDisplayPayTitleCount = 0
    private(set) var callDisplayPaymentMethodCount = 0
    private(set) var callDisplayPrivacyCount = 0
    private(set) var callDisplayAskDefaultPrivacyCount = 0
    private(set) var callDisplayAskPrivacyCount = 0
    
    private(set) var payTitle: String?

    private(set) var paymentMethodImage: UIImage?
    
    private(set) var privacyTitle: String?
    private(set) var privacyimage: UIImage?
    private(set) var privacy: PaymentPrivacy?
    
    private(set) var askDefaultPrivacyTitle: String?
    private(set) var askDefaultPrivacyMessage: String?
    private(set) var askDefaultPrivacyAcceptAction: String?
    private(set) var askDefaultPrivacyCancelAction: String?
    
    private(set) var askPrivacyTitle: String?
    private(set) var askPrivacyMessage: String?
    private(set) var cancelAction: String?
    private(set) var askPrivacyPublicButton: String?
    private(set) var askPrivacyPrivateButton: String?
    
    func completionPaymentMethod() {
        callCompletionPaymentMethod += 1
    }
    
    func displayPayTitle(_ title: String) {
        self.payTitle = title
        callDisplayPayTitleCount += 1
    }
    
    func displayPaymentMethod(_ image: UIImage) {
        self.paymentMethodImage = image
        callDisplayPaymentMethodCount += 1
    }
    
    func displayPrivacy(title: String, image: UIImage, privacy: PaymentPrivacy) {
        self.privacyTitle = title
        self.privacyimage = image
        self.privacy = privacy
        callDisplayPrivacyCount += 1
    }
    
    func displayAskPrivacy(title: String, message: String, publicButton: String, privateButton: String, cancelAction: String) {
        self.askPrivacyTitle = title
        self.askPrivacyMessage = message
        self.askPrivacyPublicButton = publicButton
        self.askPrivacyPrivateButton = privateButton
        callDisplayAskPrivacyCount += 1
    }
}

extension PaymentToolbarLargeAction: Equatable {
    public static func == (lhs: PaymentToolbarLargeAction, rhs: PaymentToolbarLargeAction) -> Bool {
        switch (lhs, rhs) {
        case (.openPaymentMethods, .openPaymentMethods):
            return true
        }
    }
}

private final class PaymentToolbarLargeCoordinatorSpy: PaymentToolbarLargeCoordinating {
    private(set) var callDidNextStepCount = 0
    private(set) var action: PaymentToolbarLargeAction?
    
    var viewController: UIViewController?
    
    func perform(action: PaymentToolbarLargeAction) {
        self.action = action
        callDidNextStepCount += 1
    }
}

final class PaymentToolbarLargePresenterTests: XCTestCase {
    private let viewController = PaymentToolbarLargeViewControllerSpy()
    private let model = PaymentToolbarLarge(title: "Toolbar Example")
    private lazy var coordinator = PaymentToolbarLargeCoordinatorSpy()
    private lazy var sut: PaymentToolbarLargePresenting = {
        let presenter = PaymentToolbarLargePresenter(model: model, coordinator: coordinator)
        presenter.viewController = viewController
        return presenter
    }()
    
    func testUpdateView_WhenPaymentMethodIsCardAndPivacyIsPublic_ShouldUpdateView() {
        sut.updateView(paymentMethod: .card, privacy: .public)
        
        XCTAssertEqual(viewController.callDisplayPaymentMethodCount, 1)
        XCTAssertEqual(viewController.paymentMethodImage, Assets.NewGeneration.paymentOriginIconCard.image)
        
        XCTAssertEqual(viewController.callDisplayPrivacyCount, 1)
        XCTAssertEqual(viewController.privacyTitle, PaymentLocalizable.privacyPublic.text)
        XCTAssertEqual(viewController.privacyimage, Assets.NewGeneration.icoPrivAmigos.image)
        XCTAssertEqual(viewController.privacy, .public)
        
        XCTAssertEqual(viewController.callDisplayPayTitleCount, 1)
        XCTAssertEqual(viewController.payTitle, "Toolbar Example")
    }
    
    func testUpdateView_WhenPaymentMethodIsBalanceAndCardAndPivacyIsPrivate_ShouldUpdateView() {
        sut.updateView(paymentMethod: .balanceAndCard, privacy: .private)
        
        XCTAssertEqual(viewController.callDisplayPaymentMethodCount, 1)
        XCTAssertEqual(viewController.paymentMethodImage, Assets.NewGeneration.paymentOriginIconCardAndBalance.image)
        
        XCTAssertEqual(viewController.callDisplayPrivacyCount, 1)
        XCTAssertEqual(viewController.privacyTitle, PaymentLocalizable.privacyPrivate.text)
        XCTAssertEqual(viewController.privacyimage, Assets.NewGeneration.icoPrivPrivado.image)
        XCTAssertEqual(viewController.privacy, .private)
        
        XCTAssertEqual(viewController.callDisplayPayTitleCount, 1)
        XCTAssertEqual(viewController.payTitle, "Toolbar Example")
    }
    
    func testUpdateView_WhenPaymentMethodIsBalanceAndPivacyIsPrivate_ShouldUpdateView() {
        sut.updateView(paymentMethod: .accountBalance, privacy: .private)
        
        XCTAssertEqual(viewController.callDisplayPaymentMethodCount, 1)
        XCTAssertEqual(viewController.paymentMethodImage, Assets.NewGeneration.paymentOriginIconBalance.image)
        
        XCTAssertEqual(viewController.callDisplayPrivacyCount, 1)
        XCTAssertEqual(viewController.privacyTitle, PaymentLocalizable.privacyPrivate.text)
        XCTAssertEqual(viewController.privacyimage, Assets.NewGeneration.icoPrivPrivado.image)
        XCTAssertEqual(viewController.privacy, .private)
        
        XCTAssertEqual(viewController.callDisplayPayTitleCount, 1)
        XCTAssertEqual(viewController.payTitle, "Toolbar Example")
    }
    
    func testUpdatePaymentMethod_WhenPaymentMethodIsBalance_ShouldUpdatePaymentMethod() {
        sut.updatePaymentMethod(.accountBalance)
        
        XCTAssertEqual(viewController.callDisplayPaymentMethodCount, 1)
        XCTAssertEqual(viewController.paymentMethodImage, Assets.NewGeneration.paymentOriginIconBalance.image)
    }
    
    func testUpdatePaymentMethod_WhenPaymentMethodIsBalanceAndCard_ShouldUpdatePaymentMethod() {
        sut.updatePaymentMethod(.balanceAndCard)
        
        XCTAssertEqual(viewController.callDisplayPaymentMethodCount, 1)
        XCTAssertEqual(viewController.paymentMethodImage, Assets.NewGeneration.paymentOriginIconCardAndBalance.image)
    }
    
    func testUpdatePaymentMethod_WhenPaymentMethodIsCard_ShouldUpdatePaymentMethod() {
        sut.updatePaymentMethod(.card)
        
        XCTAssertEqual(viewController.callDisplayPaymentMethodCount, 1)
        XCTAssertEqual(viewController.paymentMethodImage, Assets.NewGeneration.paymentOriginIconCard.image)
    }
    
    func testUpdatePrivacy_WhenPaymentMethodIsCard_ShouldUpdatePaymentMethod() {
        sut.updatePaymentMethod(.card)
        
        XCTAssertEqual(viewController.callDisplayPaymentMethodCount, 1)
        XCTAssertEqual(viewController.paymentMethodImage, Assets.NewGeneration.paymentOriginIconCard.image)
    }
    
    func testUpdatePrivacy_WhenPrivacyIsPublic_ShouldUpdatePrivacy() {
        sut.updatePrivacy(.public)
        
        XCTAssertEqual(viewController.callDisplayPrivacyCount, 1)
        XCTAssertEqual(viewController.privacyTitle, PaymentLocalizable.privacyPublic.text)
        XCTAssertEqual(viewController.privacyimage, Assets.NewGeneration.icoPrivAmigos.image)
        XCTAssertEqual(viewController.privacy, .public)
    }
    
    func testUpdatePrivacy_WhenPrivacyIsPrivate_ShouldUpdatePrivacy() {
        sut.updatePrivacy(.private)
        
        XCTAssertEqual(viewController.callDisplayPrivacyCount, 1)
        XCTAssertEqual(viewController.privacyTitle, PaymentLocalizable.privacyPrivate.text)
        XCTAssertEqual(viewController.privacyimage, Assets.NewGeneration.icoPrivPrivado.image)
        XCTAssertEqual(viewController.privacy, .private)
    }
    
    func testPresentAskPrivacy_ShouldAskPrivacy() {
        sut.presentAskPrivacy()
        
        XCTAssertEqual(viewController.callDisplayAskPrivacyCount, 1)
        XCTAssertEqual(viewController.askPrivacyTitle, PaymentLocalizable.selectPrivacyTitle.text)
        XCTAssertEqual(viewController.askPrivacyMessage, PaymentLocalizable.selectPrivacyDesc.text)
        XCTAssertEqual(viewController.askPrivacyPublicButton, PaymentLocalizable.privacyPublic.text)
        XCTAssertEqual(viewController.askPrivacyPrivateButton, PaymentLocalizable.privacyPrivate.text)
    }
    
    func testCompletionPaymentMethod_ShouldCallCompletionPaymentMethod() {
        sut.completionPaymentMethod()
        
        XCTAssertEqual(viewController.callCompletionPaymentMethod, 1)
    }
    
    func testDidNextStep_WhenOpenPaymentMethodsActionisPassed_ShouldOpenPaymentMethods() {
        sut.didNextStep(action: .openPaymentMethods(completion: {}))
        
        XCTAssertEqual(coordinator.callDidNextStepCount, 1)
        XCTAssertEqual(coordinator.action, .openPaymentMethods(completion: {}))
    }
}
