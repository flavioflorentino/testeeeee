@testable import PicPay
import XCTest

final class PaymentToolbarLargeCoordinatorTest: XCTestCase {
    private let navController = CustomPopNavigationControllerMock(rootViewController: UIViewController())
    private lazy var sut: PaymentToolbarLargeCoordinator = {
        let coordinator = PaymentToolbarLargeCoordinator()
        coordinator.viewController = navController.topViewController
        return coordinator
    }()
    
    func testPerform_WhenActionPassedIsOpenPaymentPethods_ShouldPushPaymentMethodsScreen() {
        let completion: () -> Void = {}
        
        sut.perform(action: .openPaymentMethods(completion: completion))
        
        XCTAssertNotNil(navController.pushedViewController is PaymentMethodsViewController)
    }
    
    func testPaymentMethodsCompletion_ShouldCallCompletionMethod() throws {
        let expectation = XCTestExpectation(description: "Completion called")
        let completion: () -> Void = {
            expectation.fulfill()
        }
        
        sut.perform(action: .openPaymentMethods(completion: completion))
        
        let pushedViewController = try XCTUnwrap(navController.pushedViewController as? PaymentMethodsViewController)
        
        pushedViewController.delegate?.paymentMethodsCompletion()
        wait(for: [expectation], timeout: 1.0)
    }
}
