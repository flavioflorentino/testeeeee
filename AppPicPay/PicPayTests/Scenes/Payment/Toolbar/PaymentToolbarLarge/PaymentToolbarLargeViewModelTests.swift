import CorePayment
import XCTest
@testable import PicPay

private final class PaymentToolbarLargeServiceMock: PaymentToolbarLargeServicing {
    var privacyConfig: PaymentPrivacy = .public
}

private final class PaymentToolbarLargePresenterSpy: PaymentToolbarLargePresenting {
    private(set) var callUpdatePaymentMethodCount = 0
    private(set) var callUpdatePrivacyCount = 0
    private(set) var callUpdateViewCount = 0
    private(set) var callPresentAskPrivacyCount = 0
    private(set) var callCompletionPaymentMethodCount = 0
    private(set) var callDidNextStepCount = 0
    
    private(set) var paymentMethod: PaymentMethod?
    private(set) var privacy: PaymentPrivacy?
    private(set) var action: PaymentToolbarLargeAction?
    
    var viewController: PaymentToolbarLargeDisplay?
    
    func updatePaymentMethod(_ paymentMethod: PaymentMethod) {
        self.paymentMethod = paymentMethod
        callUpdatePaymentMethodCount += 1
    }
    
    func updatePrivacy(_ privacy: PaymentPrivacy) {
        self.privacy = privacy
        callUpdatePrivacyCount += 1
    }
    
    func updateView(paymentMethod: PaymentMethod, privacy: PaymentPrivacy) {
        self.paymentMethod = paymentMethod
        self.privacy = privacy
        callUpdateViewCount += 1
    }
    
    func presentAskPrivacy() {
        callPresentAskPrivacyCount += 1
    }
    
    func completionPaymentMethod() {
        callCompletionPaymentMethodCount += 1
    }
    
    func didNextStep(action: PaymentToolbarLargeAction) {
        self.action = action
        callDidNextStepCount += 1
    }
}

final class PaymentToolbarLargeViewModelTest: XCTestCase {
    private let service = PaymentToolbarLargeServiceMock()
    private let presenter = PaymentToolbarLargePresenterSpy()
    private lazy var sut = PaymentToolbarLargeViewModel(service: service, presenter: presenter)
    
    func testSetupView_WhenPrivacyIsPublic_ShouldPUpdatePaymentMethodAndPrivacy() {
        service.privacyConfig = .public
        
        sut.setupView()
        
        XCTAssertEqual(presenter.callUpdateViewCount, 1)
        XCTAssertEqual(presenter.privacy, .public)
        XCTAssertEqual(presenter.paymentMethod, .accountBalance)
    }
    
    func testSetupView_WhenPrivacyIsPrivate_ShouldPUpdatePaymentMethodAndPrivacy() {
        service.privacyConfig = .private
        
        sut.setupView()
        
        XCTAssertEqual(presenter.callUpdateViewCount, 1)
        XCTAssertEqual(presenter.privacy, .private)
        XCTAssertEqual(presenter.paymentMethod, .accountBalance)
    }
    
    func testDidTapPrivacy_ShouldPresentAskPrivacy() {
        sut.didTapPrivacy()
        
        XCTAssertEqual(presenter.callPresentAskPrivacyCount, 1)
    }
    
    func testDidTapPaymenMethods_ShouldOpenPaymentMethods() {
        sut.didTapPaymentMethods()
        
        XCTAssertEqual(presenter.callDidNextStepCount, 1)
        XCTAssertEqual(presenter.action, .openPaymentMethods(completion: {}))
    }
    
    func testChangePaymentMethod_WhenPassedCardMethod_ShouldUpdatePaymentMethod() {
        sut.changePaymentMethod(.card)
        
        XCTAssertEqual(presenter.callUpdatePaymentMethodCount, 1)
        XCTAssertEqual(presenter.paymentMethod, .card)
    }
    
    func testChangePaymentMethod_WhenPassedBalanceAndCardMethod_ShouldUpdatePaymentMethod() {
        sut.changePaymentMethod(.balanceAndCard)
        
        XCTAssertEqual(presenter.callUpdatePaymentMethodCount, 1)
        XCTAssertEqual(presenter.paymentMethod, .balanceAndCard)
    }
    
    func testChangePaymentPrivacy_WhenPassedPublicPrivacyOverPrivate_ShouldUpdatePaymentPrivacy() {
        service.privacyConfig = .private
        
        sut.changePaymentPrivacy(.public)
        
        XCTAssertEqual(presenter.callUpdatePrivacyCount, 1)
        XCTAssertEqual(presenter.privacy, .public)
    }
    
    func testChangePaymentPrivacy_WhenPassedPrivatePrivacyOverPublic_ShouldKeepPaymentPrivacy() {
        service.privacyConfig = .public
        
        sut.changePaymentPrivacy(.private)
        
        XCTAssertEqual(presenter.callUpdatePrivacyCount, 1)
        XCTAssertEqual(presenter.privacy, .private)
    }
    
    func testChangePaymentPrivacy_WhenPassedPrivatePrivacyOverPrivate_ShouldKeepPaymentPrivacy() {
        service.privacyConfig = .private
        
        sut.changePaymentPrivacy(.private)
        
        XCTAssertEqual(presenter.callUpdatePrivacyCount, 1)
        XCTAssertEqual(presenter.privacy, .private)
    }
}
