import XCTest
@testable import UI
@testable import PicPay

private final class PaymentProfileHeaderCoordinatorSpy: PaymentProfileHeaderCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformOpenProfileCount = 0
    private(set) var profileId: String?
    
    func perform(action: PaymentProfileHeaderAction) {
        guard case let .openProfile(profileId) = action else {
            return
        }
        
        callPerformOpenProfileCount += 1
        self.profileId = profileId
    }
}

private final class PaymentProfileHeaderViewControllerSpy: PaymentProfileHeaderDisplay {
    private(set) var callDisplayNameCount = 0
    private(set) var callDisplayImageCount = 0
    private(set) var callDisplayDescriptionCount = 0
    private(set) var callDisplayFollowButtonCount = 0
    private(set) var callDisplayFollowConfirmationPopupCount = 0
    private(set) var callDisplayErrorCount = 0
    
    private(set) var name: String?
    private(set) var imageUrl: URL?
    private(set) var placeholder: UIImage?
    private(set) var type: HeaderPayment.PayeeType?
    private(set) var description: String?
    private(set) var status: FollowerStatus?
    private(set) var alert: Alert?
    private(set) var action: FollowerButtonAction?
    private(set) var error: PicPayError?
    
    
    func displayName(_ name: String) {
        callDisplayNameCount += 1
        self.name = name
    }
    
    func displayImage(image: URL?, placeholder: UIImage, type: HeaderPayment.PayeeType) {
        callDisplayImageCount += 1
        imageUrl = image
        self.placeholder = placeholder
        self.type = type
    }
    
    func displayDescription(_ text: String?) {
        callDisplayDescriptionCount += 1
        self.description = text
    }
    
    func displayFollowButton(withStatus status: FollowerStatus) {
        callDisplayFollowButtonCount += 1
        self.status = status
    }
    
    func displayFollowConfirmationPopup(_ alert: Alert, action: FollowerButtonAction) {
        callDisplayFollowConfirmationPopupCount += 1
        self.alert = alert
        self.action = action
    }
    
    func displayError(_ error: PicPayError) {
        callDisplayErrorCount += 1
        self.error = error
    }
}

final class PaymentProfileHeaderPresenterTest: XCTestCase {
    private let viewController = PaymentProfileHeaderViewControllerSpy()
    private let coordinator = PaymentProfileHeaderCoordinatorSpy()
    
    private func presenter(model: HeaderProfile) -> PaymentProfileHeaderPresenter {
        let presenter = PaymentProfileHeaderPresenter(model: model, coordinator: coordinator)
        presenter.viewController = viewController
        return presenter
    }
    
    func testUpdateView_ShouldUpdateNameImageAndDescription() {
        let model = HeaderProfile(
            username: "jose.santos.silva",
            image: "https://s3.amazonaws.com/cdn.picpay.com/apps/icons/gift_beer.png",
            description: "Eba! Um super mimo pela sua chegada!!!",
            showFollowButton: false
        )
        let sut = presenter(model: model)
        
        sut.updateView(payeeType: .pro)
        
        XCTAssertEqual(viewController.callDisplayNameCount, 1)
        XCTAssertEqual(viewController.name, "@jose.santos.silva")
        XCTAssertEqual(viewController.callDisplayImageCount, 1)
        XCTAssertEqual(viewController.imageUrl?.absoluteString, model.image)
        XCTAssertEqual(viewController.placeholder, Assets.NewGeneration.avatarPlaceholder.image)
        XCTAssertEqual(viewController.type, .pro)
        XCTAssertEqual(viewController.callDisplayDescriptionCount, 1)
        XCTAssertEqual(viewController.description, model.description)
    }
    
    func testPresentFollowConfirmationPopup_WhenActionIsUnfollow_ShowDisplayConfirmationToUnfollow() {
        let username = "Maria de Oliveira"
        let model = HeaderProfile(username: username, image: "", description: "", showFollowButton: true)
        let sut = presenter(model: model)
        
        sut.presentFollowConfirmationPopup(for: .unfollow)
        
        XCTAssertEqual(viewController.callDisplayFollowConfirmationPopupCount, 1)
        XCTAssertEqual(viewController.action, .unfollow)
        XCTAssertEqual(
            viewController.alert?.title?.string,
            String(format: PaymentLocalizable.unfollowConfirmationTitle.text, username)
        )
    }
    
    func testPresentFollowConfirmationPopup_WhenActionIsCancelReques_ShowDisplayConfirmationToCancelRequest() {
        let username = "Joana Santos"
        let model = HeaderProfile(username: username, image: "", description: "", showFollowButton: true)
        let sut = presenter(model: model)
        
        sut.presentFollowConfirmationPopup(for: .cancelRequest)
        
        XCTAssertEqual(viewController.callDisplayFollowConfirmationPopupCount, 1)
        XCTAssertEqual(viewController.action, .cancelRequest)
        XCTAssertEqual(
            viewController.alert?.title?.string,
            String(format: PaymentLocalizable.cancelRequestConfirmationTitle.text, username)
        )
    }
    
    func testUpdateFollowButton_WhenFollowButtonIsEnabled_ShouldUpdateStatus() {
        let model = HeaderProfile(username: "", image: "", description: "", showFollowButton: true)
        let sut = presenter(model: model)
        
        sut.updateFollowButton(with: .loading)
        
        XCTAssertEqual(viewController.callDisplayFollowButtonCount, 1)
        XCTAssertEqual(viewController.status, .loading)
    }
    
    func testUpdateFollowButton_WhenFollowButtonIsDisabled_ShouldNotUpdateStatus() {
        let model = HeaderProfile(username: "", image: "", description: "", showFollowButton: false)
        let sut = presenter(model: model)
        
        sut.updateFollowButton(with: .loading)
        
        XCTAssertEqual(viewController.callDisplayFollowButtonCount, 0)
        XCTAssertNil(viewController.status)
    }
    
    func testPresentError_ShouldDisplayError() {
        let model = HeaderProfile(username: "", image: "", description: "", showFollowButton: false)
        let sut = presenter(model: model)
        let error = MockError.connectionError
        
        sut.presentError(error)
        
        XCTAssertEqual(viewController.callDisplayErrorCount, 1)
        XCTAssertEqual(viewController.error, error)
    }
    
    func testDidNextStep_WhenActionIsOpenProfileWithId_ShouldCallOpenProfileAction() {
        let model = HeaderProfile(username: "", image: "", description: "", showFollowButton: false)
        let sut = presenter(model: model)
        let profileId = "956076386"
        
        sut.didNextStep(action: .openProfile(profileId: profileId))
        
        XCTAssertEqual(coordinator.callPerformOpenProfileCount, 1)
        XCTAssertEqual(coordinator.profileId, profileId)
    }
}
