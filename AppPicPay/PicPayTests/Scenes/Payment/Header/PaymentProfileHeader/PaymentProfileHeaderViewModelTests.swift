import XCTest
@testable import PicPay

private final class PaymentProfileHeaderServiceMock: PaymentProfileHeaderServicing {
    var result: Result<FollowerStatus, PicPayError> = .success(.undefined)
    
    func followFriend(action: FollowerButtonAction, followerId: String, completion: @escaping (Result<FollowerStatus, PicPayError>) -> Void) {
        completion(result)
    }
}

private final class PaymentProfileHeaderPresenterSpy: PaymentProfileHeaderPresenting {
    var viewController: PaymentProfileHeaderDisplay?
    
    private(set) var callUpdateViewCount = 0
    private(set) var callUpdateFollowButtonCount = 0
    private(set) var callPresentFollowConfirmationPopupCount = 0
    private(set) var callPresentErrorCount = 0
    private(set) var callDidNextStepCount = 0
    
    private(set) var payeeType: HeaderPayment.PayeeType?
    private(set) var followerStatus: FollowerStatus?
    private(set) var action: FollowerButtonAction?
    private(set) var error: PicPayError?
    private(set) var didSelectOpenProfileAction = 0
    
    func updateView(payeeType: HeaderPayment.PayeeType) {
        callUpdateViewCount += 1
        self.payeeType = payeeType
    }
    
    func updateFollowButton(with status: FollowerStatus) {
        callUpdateFollowButtonCount += 1
        followerStatus = status
    }
    
    func presentFollowConfirmationPopup(for action: FollowerButtonAction) {
        callPresentFollowConfirmationPopupCount += 1
        self.action = action
    }
    
    func presentError(_ error: PicPayError) {
        callPresentErrorCount += 1
        self.error = error
    }
    
    func didNextStep(action: PaymentProfileHeaderAction) {
        callDidNextStepCount += 1
        if case .openProfile = action {
            didSelectOpenProfileAction += 1
        }
    }
}

final class PaymentProfileHeaderViewModelTest: XCTestCase {
    private let genericModel = HeaderPayment(
        sellerId: nil,
        payeeId: "3626",
            value: 5.0,
            installments: 2,
            payeeType: .none,
            paymentType: .newFriendGift
    )
    private let presenter = PaymentProfileHeaderPresenterSpy()
    private let service = PaymentProfileHeaderServiceMock()
    private let analyticsSpy = PaymentProfileHeaderAnalyticsSpy()
    
    private func viewModel(for model: HeaderPayment, followingStatus: FollowerStatus?) -> PaymentProfileHeaderViewModelInputs {
        return PaymentProfileHeaderViewModel(
            model: model,
            followingStatus: followingStatus,
            presenter: presenter,
            service: service,
            analytics: analyticsSpy
        )
    }
    
    func testSetupPayeeType_WhenPayeeTypeIsNewFriendGiftPayment_ShouldSetupNewFiendGiftPaymentType() {
        let model = HeaderPayment(sellerId: nil, payeeId: "123", value: 10.0, installments: 2, payeeType: .none, paymentType: .newFriendGift)
        let sut = viewModel(for: model, followingStatus: .notFollowing)
        
        sut.setupPayeeType()
        
        XCTAssertEqual(presenter.callUpdateViewCount, 1)
        XCTAssertEqual(presenter.payeeType, model.payeeType)
    }
    
    func testSetupPayeeType_WhenPayeeTypeIsBirthdayPayment_ShouldSetupBirthdayPaymentType() {
        let model = HeaderPayment(sellerId: nil, payeeId: "123", value: 10.0, installments: 2, payeeType: .none, paymentType: .birthday)
        let sut = viewModel(for: model, followingStatus: .notFollowing)
        
        sut.setupPayeeType()
        
        XCTAssertEqual(presenter.callUpdateViewCount, 1)
        XCTAssertEqual(presenter.payeeType, model.payeeType)
    }
    
    
    func testUpdateFollowStatus_WhenStatusIsNotFollowing_ShouldUpdateFollowButtowWithNotFollowingStatus() {
        let followingStatus = FollowerStatus.notFollowing
        let sut = viewModel(for: genericModel, followingStatus: followingStatus)
        
        sut.updateFollowStatus()
        
        XCTAssertEqual(presenter.callUpdateFollowButtonCount, 1)
        XCTAssertEqual(presenter.followerStatus, followingStatus)
    }
    
    func testUpdateFollowStatus_WhenStatusIsFollowing_ShouldUpdateFollowButtowWithFollowingStatus() {
        let followingStatus = FollowerStatus.following
        let sut = viewModel(for: genericModel, followingStatus: followingStatus)
        
        sut.updateFollowStatus()
        
        XCTAssertEqual(presenter.callUpdateFollowButtonCount, 1)
        XCTAssertEqual(presenter.followerStatus, followingStatus)
    }
    
    func testCheckIfCanFollow_WhenActionIsUnfollow_ShouldPresentConfirmationPopup() {
        let sut = viewModel(for: genericModel, followingStatus: .following)
        
        sut.checkIfCanFollow(.unfollow)
        
        XCTAssertEqual(presenter.callPresentFollowConfirmationPopupCount, 1)
        XCTAssertEqual(presenter.action, .unfollow)
    }
    
    func testCheckIfCanFollow_WhenActionIsCancelRequest_ShouldPresentConfirmationPopup() {
        let sut = viewModel(for: genericModel, followingStatus: .waitingAnswer)
        
        sut.checkIfCanFollow(.cancelRequest)
        
        XCTAssertEqual(presenter.callPresentFollowConfirmationPopupCount, 1)
        XCTAssertEqual(presenter.action, .cancelRequest)
    }
    
    func testCheckIfCanFollow_WhenActionIsFollow_ShouldCallFollowingAction() {
        service.result = .success(.following)
        let sut = viewModel(for: genericModel, followingStatus: .waitingAnswer)
        
        sut.checkIfCanFollow(.follow)
        
        XCTAssertEqual(analyticsSpy.callWillFollowCount, 1)
        XCTAssertEqual(presenter.callUpdateFollowButtonCount, 2)
        XCTAssertEqual(presenter.followerStatus, .following)
    }
    
    func testFollowFriend_WhenNotFollowingFriendAndNeedsConfirmation_ShouldUpdateStatusToWaitingAnswer() {
        service.result = .success(.waitingAnswer)
        let sut = viewModel(for: genericModel, followingStatus: .notFollowing)
        
        sut.followAction(.follow)
        
        XCTAssertEqual(presenter.callUpdateFollowButtonCount, 2)
        XCTAssertEqual(presenter.followerStatus, .waitingAnswer)
        XCTAssertEqual(analyticsSpy.callWillFollowCount, 1)
    }
    
    func testFollowFriend_WhenNotFollowingFriendAndNeedsConfirmation_ShouldUpdateStatusToFollowing() {
        service.result = .success(.following)
        let sut = viewModel(for: genericModel, followingStatus: .notFollowing)
        
        sut.followAction(.follow)
        
        XCTAssertEqual(presenter.callUpdateFollowButtonCount, 2)
        XCTAssertEqual(presenter.followerStatus, .following)
        XCTAssertEqual(analyticsSpy.callWillFollowCount, 1)
    }
    
    func testFollowFriend_WhenFollowingFriendAndNeedsConfirmation_ShouldUpdateStatusToNotFollowing() {
        service.result = .success(.notFollowing)
        let sut = viewModel(for: genericModel, followingStatus: .following)
        
        sut.followAction(.follow)
        
        XCTAssertEqual(presenter.callUpdateFollowButtonCount, 2)
        XCTAssertEqual(presenter.followerStatus, .notFollowing)
        XCTAssertEqual(analyticsSpy.callWillFollowCount, 1)
    }
    
    func testFollowFriend_WhenReturningFailure_ShouldPresentError() {
        let error = MockError.genericError
        service.result = .failure(error)
        let sut = viewModel(for: genericModel, followingStatus: .following)
        
        sut.followAction(.follow)
        
        XCTAssertEqual(presenter.callUpdateFollowButtonCount, 2)
        XCTAssertEqual(presenter.callPresentErrorCount, 1)
        XCTAssertEqual(presenter.error, error)
        XCTAssertEqual(analyticsSpy.callWillFollowCount, 1)
    }
    
    func testOpenProfile() {
        let sut = viewModel(for: genericModel, followingStatus: .none)
        
        sut.openProfile()
        
        XCTAssertEqual(presenter.callDidNextStepCount, 1)
        XCTAssertEqual(analyticsSpy.callWillOpenProfileCount, 1)
    }
}
