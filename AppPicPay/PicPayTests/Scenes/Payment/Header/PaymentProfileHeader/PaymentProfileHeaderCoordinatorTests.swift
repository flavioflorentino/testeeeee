@testable import PicPay
import XCTest

final class PaymentProfileHeaderCoordinatorTest: XCTestCase {
    private let navController = CustomPopNavigationControllerMock(rootViewController: UIViewController())
    private lazy var sut: PaymentProfileHeaderCoordinating = {
        let coordinator = PaymentProfileHeaderCoordinator()
        coordinator.viewController = navController.topViewController
        return coordinator
    }()
    
    func testPerform_WhenActionIsOpenProfile_ShouldPushUserProfile() throws {
        sut.perform(action: .openProfile(profileId: ""))
        
        XCTAssert(navController.isPushViewControllerCalled)
        XCTAssertTrue(navController.pushedViewController is ConsumerProfileViewController)
    }
}
