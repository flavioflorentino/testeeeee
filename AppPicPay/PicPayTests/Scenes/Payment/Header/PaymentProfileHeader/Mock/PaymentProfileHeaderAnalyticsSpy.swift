@testable import PicPay

final class PaymentProfileHeaderAnalyticsSpy: PaymentProfileHeaderAnalytics {
    private(set) var callWillOpenProfileCount = 0
    private(set) var callWillFollowCount = 0
    
    func willOpenProfile() {
        callWillOpenProfileCount += 1
    }
    
    func willFollow() {
        callWillFollowCount += 1
    }
}
