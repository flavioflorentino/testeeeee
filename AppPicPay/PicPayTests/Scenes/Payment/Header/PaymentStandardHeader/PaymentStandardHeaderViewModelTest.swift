import XCTest
import AnalyticsModule
@testable import PicPay

final class MockPaymentStandardHeaderService: PaymentStandardHeaderServicing {
    private let installmentEnable: Bool
    private let installmentAvailable: Int
    private let cardValue: Double
    private let usePicPayBalanceValue: Bool
    
    init(installmentAvailable: Int, installmentEnable: Bool, cardValue: Double, usePicPayBalanceValue: Bool) {
        self.installmentAvailable = installmentAvailable
        self.installmentEnable = installmentEnable
        self.cardValue = cardValue
        self.usePicPayBalanceValue = usePicPayBalanceValue
    }
    
    func usePicPayBalance() -> Bool {
        return usePicPayBalanceValue
    }
    
    func installmentActivated(type: HeaderPayment.PaymentType) -> Bool {
        return installmentEnable
    }
    
    func numberInstallmentAvailable(total: Double, installment: Int) -> Int {
        return installmentAvailable
    }
    
    func cardValue(total: Double, installment: Int) -> Double {
        return cardValue
    }
}
 
final class MockPaymentStandardHeaderPresenter: PaymentStandardHeaderPresenting {
    var viewController: PaymentStandardHeaderDisplay?
    
    private(set) var calledDisplayHeader = false
    private(set) var calledDisplayInstallment = false
    private(set) var calledUpdateCardValue = false
    private(set) var errorInstallmentCallsCount = 0
    private(set) var errorType: PaymentStandardHeaderPresenter.InstallmentError?
    private(set) var presentErrorCallsCount = 0
    private(set) var errorMessage: String?
    private(set) var presentInstallmentsDisabledAlertCallsCount = 0
    private(set) var description: String?
    private(set) var calledDidNextStep = false
    private(set) var calledUpdateInput = false
    private(set) var calledDidTapInfoButton = 0
    
    private(set) var headerValue: Double?
    private(set) var headerPayeeType: HeaderPayment.PayeeType?
    private(set) var headerPaymentType: HeaderPayment.PaymentType?
    
    private(set) var installmentisEnable: Bool?
    private(set) var installmentValue: Int?
    
    private(set) var inputValue: Double?
    
    private(set) var cardValue: Double?
    private(set) var stepAction: PaymentStandardHeaderAction?
    
    func viewDidLoad(value: Double, payeeType: HeaderPayment.PayeeType, paymentType: HeaderPayment.PaymentType) {
        calledDisplayHeader = true
        headerValue = value
        headerPayeeType = payeeType
        headerPaymentType = paymentType
    }
    
    func updateInput(value: Double) {
        inputValue = value
        calledUpdateInput = true
        
    }
    
    func displayInstallment(isEnable: Bool, installment: Int) {
        calledDisplayInstallment = true
        installmentisEnable = isEnable
        installmentValue = installment
    }
    
    func updateCardValue(cardValue: Double) {
        calledUpdateCardValue = true
        self.cardValue = cardValue
    }
    
    func errorInstallment(type: PaymentStandardHeaderPresenter.InstallmentError) {
        errorInstallmentCallsCount += 1
        errorType = type
    }

    func presentInstallmentsDisabledAlert(withDescription description: String) {
        presentInstallmentsDisabledAlertCallsCount += 1
        self.description = description
    }
    
    func presentError(message: String) {
        presentErrorCallsCount += 1
        errorMessage = message
    }
    
    func didNextStep(action: PaymentStandardHeaderAction) {
        calledDidNextStep = true
        stepAction = action
    }
    
    func didTapInfoButton() {
        calledDidTapInfoButton += 1
    }
}

final class PaymentStandardHeaderViewModelTest: XCTestCase {
    private lazy var installmentAvailable = 1
    private lazy var installmentEnable = true
    private lazy var cardValue: Double = 0.0
    private lazy var usePicPayBalanceValue = false
    private lazy var mockService = MockPaymentStandardHeaderService(
        installmentAvailable: installmentAvailable,
        installmentEnable: installmentEnable,
        cardValue: cardValue,
        usePicPayBalanceValue: usePicPayBalanceValue
    )
    private let mockPresenter = MockPaymentStandardHeaderPresenter()
    private lazy var model = createModel()
    private let analytics = AnalyticsSpy()
    private let consumerManager: ConsumerManagerContract = {
        let consumerManager = ConsumerManagerMock()
        consumerManager.consumer = MBConsumer()
        consumerManager.consumer?.wsId = 1
        return consumerManager
    }()
    private lazy var viewModel = PaymentStandardHeaderViewModel(model: model, service: mockService, presenter: mockPresenter, dependencies: DependencyContainerMock(analytics, consumerManager))
    
    private func createModel(
        paymentType: HeaderPayment.PaymentType = .p2p,
        installments: Int = 1,
        payeeType: HeaderPayment.PayeeType = .pro
    ) -> HeaderPayment {
        return HeaderPayment(
            sellerId: nil,
            payeeId: "12345",
            value: 0.0,
            installments: installments,
            payeeType: payeeType,
            paymentType: paymentType)
    }
    
    func testViewDidLoadShouldCallDisplayHeader() {
        viewModel.viewDidLoad()
        
        XCTAssertTrue(mockPresenter.calledDisplayHeader)
        XCTAssertEqual(mockPresenter.headerValue, model.value)
        XCTAssertEqual(mockPresenter.headerPayeeType, model.payeeType)
        XCTAssertEqual(mockPresenter.headerPaymentType, model.paymentType)
    }
    
    func testViewDidLoadShouldCallDisplayInstallmentEnable() {
        viewModel.viewDidLoad()
        
        XCTAssertTrue(mockPresenter.calledDisplayInstallment)
        XCTAssertEqual(mockPresenter.installmentisEnable, false)
        XCTAssertEqual(mockPresenter.installmentValue, model.installments)
    }
    
    func testViewDidLoadShouldCallDisplayInstallmentDisable() {
        cardValue = 20.0
        
        viewModel.viewDidLoad()
        
        XCTAssertTrue(mockPresenter.calledDisplayInstallment)
        XCTAssertEqual(mockPresenter.installmentisEnable, true)
        XCTAssertEqual(mockPresenter.installmentValue, model.installments)
    }
    
    func testUpdateValues_ShouldCallUpdateCardValue() {
        cardValue = 20.0

        viewModel.updateValues()
        
        XCTAssertTrue(mockPresenter.calledUpdateCardValue)
        XCTAssertEqual(mockPresenter.cardValue, 20.0)
    }
    
    func testUpdateValues_ShouldCallDisplayInstallment() {
        cardValue = 20.0
        
        viewModel.updateValues()
        
        XCTAssertTrue(mockPresenter.calledDisplayInstallment)
        XCTAssertEqual(mockPresenter.installmentisEnable, true)
    }
    
    func testDidTapImageP2PShouldCallOpenP2PProfile() {
        model = createModel(paymentType: .p2p)

        viewModel.didTapImage()
        
        XCTAssertTrue(mockPresenter.calledDidNextStep)
        switch mockPresenter.stepAction! {
        case .openP2PProfile(let payeeId):
            XCTAssertNotNil(payeeId)
        default:
            XCTFail()
        }
    }
    
    func testChangeValueShouldCallUpdateCardValue() {
        cardValue = 20.0

        viewModel.changeValue(value: 50.0)
        
        XCTAssertTrue(mockPresenter.calledUpdateCardValue)
        XCTAssertEqual(mockPresenter.cardValue, 20.0)
    }
    
    func testChangeValueShouldReturnEqualInstallment() {
        installmentAvailable = 10
        model = createModel(installments: 8)

        viewModel.changeValue(value: 50.0)
        
        XCTAssertTrue(mockPresenter.calledDisplayInstallment)
        XCTAssertEqual(mockPresenter.installmentValue, 8)
    }
    
    func testChangeValueShouldReturnOneInstallment() {
        installmentAvailable = -10
        model = createModel(installments: 1)

        viewModel.changeValue(value: 50.0)
        
        XCTAssertTrue(mockPresenter.calledDisplayInstallment)
        XCTAssertEqual(mockPresenter.installmentValue, 1)
    }
    
    func testChangeValueShouldReturnSmallestInstallment() {
        installmentAvailable = 10
        model = createModel(installments: 12)

        viewModel.changeValue(value: 50.0)
        viewModel.didTapInstallment()
        
        XCTAssertTrue(mockPresenter.calledDisplayInstallment)
        XCTAssertEqual(mockPresenter.installmentValue, 10)
    }
    
    func testDidTapInstallment_WhenPaymentTypeIsP2pAndValueIsZeroAndCardValueIsZeroAndUsePicPayBalanceValueIsTrue_ShouldCallDidNextStepAndErrorInstallmentWithEmptyValueAndWrongPaymentMethod() throws {
        usePicPayBalanceValue = true
        viewModel.changeValue(value: 0.0)
        viewModel.didTapInstallment()
        
        XCTAssertFalse(mockPresenter.calledDidNextStep)
        XCTAssertEqual(mockPresenter.errorInstallmentCallsCount, 1)
        XCTAssertEqual(mockPresenter.errorType, .emptyValueAndWrongPaymentMethod)
        let firstEvent = try XCTUnwrap(analytics.events.first)
        let properties = try XCTUnwrap(firstEvent.properties as? [String: String])
        XCTAssertEqual(analytics.logCalledCount, 1)
        XCTAssertEqual(properties["from"], "button installment")
        XCTAssertEqual(properties["to"], "modal installment balance")
        XCTAssertFalse(properties["user_id"].isEmpty)
        XCTAssertFalse(properties["timestamp"].isEmpty)
    }
    
    func testDidTapInstallment_WhenPaymentTypeIsP2pAndValueIsGreaterThanZeroAndCardValueIsZeroAndUsePicPayBalanceValueIsTrue_ShouldCallDidNextStepAndErrorInstallmentWithWrongPaymentMethod() throws {
        usePicPayBalanceValue = true
        viewModel.changeValue(value: 10.0)
        viewModel.didTapInstallment()
        
        XCTAssertFalse(mockPresenter.calledDidNextStep)
        XCTAssertEqual(mockPresenter.errorInstallmentCallsCount, 1)
        XCTAssertEqual(mockPresenter.errorType, .wrongPaymentMethod)
        let firstEvent = try XCTUnwrap(analytics.events.first)
        let properties = try XCTUnwrap(firstEvent.properties as? [String: String])
        XCTAssertEqual(analytics.logCalledCount, 1)
        XCTAssertEqual(properties["from"], "button installment")
        XCTAssertEqual(properties["to"], "modal filled balance")
        XCTAssertFalse(properties["user_id"].isEmpty)
        XCTAssertFalse(properties["timestamp"].isEmpty)
    }
    
    func testDidTapInstallment_WhenPaymentTypeIsP2pAndCardValueIsZeroAndUsePicPayBalanceValueIsFalse_ShouldCallDidNextStepAndErrorInstallmentWithEmptyValue() throws {
        usePicPayBalanceValue = false
        viewModel.changeValue(value: 0.0)
        viewModel.didTapInstallment()
        
        XCTAssertFalse(mockPresenter.calledDidNextStep)
        XCTAssertEqual(mockPresenter.errorInstallmentCallsCount, 1)
        XCTAssertEqual(mockPresenter.errorType, .emptyValue)
        let firstEvent = try XCTUnwrap(analytics.events.first)
        let properties = try XCTUnwrap(firstEvent.properties as? [String: String])
        XCTAssertEqual(analytics.logCalledCount, 1)
        XCTAssertEqual(properties["from"], "button installment")
        XCTAssertEqual(properties["to"], "modal installment creditcard")
        XCTAssertFalse(properties["user_id"].isEmpty)
        XCTAssertFalse(properties["timestamp"].isEmpty)
    }
    
    func testDidTapInstallment_WhenPaymentTypeIsP2pAndValueIsZeroAndCardValueIsNotZeroAndUsePicPayBalanceValueIsFalse_ShouldCallDidNextStepAndPresentErrorWithUnexpectedError() {
        cardValue = 10.0

        viewModel.changeValue(value: 0.0)
        viewModel.didTapInstallment()
        
        XCTAssertFalse(mockPresenter.calledDidNextStep)
        XCTAssertEqual(mockPresenter.presentErrorCallsCount, 1)
        XCTAssertEqual(mockPresenter.errorMessage, DefaultLocalizable.unexpectedError.text)
    }
    
    func testDidTapInstallmentReturnP2pInstallmentRulesP2PDisable() {
        installmentEnable = false
        cardValue = 10.0
        model = createModel(payeeType: .none)

        viewModel.changeValue(value: 10.0)
        viewModel.didTapInstallment()
        
        XCTAssertFalse(mockPresenter.calledDidNextStep)
        XCTAssertEqual(mockPresenter.presentErrorCallsCount, 1)
    }
    
    func testDidTapInstallmentReturnP2pInstallmentRulesUserNoProInstalmentEnable() {
        cardValue = 10.0
        model = createModel(payeeType: .none)

        viewModel.changeValue(value: 10.0)
        viewModel.didTapInstallment()
        
        XCTAssertTrue(mockPresenter.calledDidNextStep)
    }
    
    func testDidTapInstallmentReturnP2pInstallmentRulesUserProInstalmentEnable() {
        cardValue = 10.0
        model = createModel(payeeType: .pro)

        viewModel.changeValue(value: 10.0)
        viewModel.didTapInstallment()
        
        XCTAssertTrue(mockPresenter.calledDidNextStep)
    }

    func testDidTapInstallment_WhenPaymentTypeIsPIX_ShouldCallPresentInstallmentsDisabledAlert() throws {
        let pixViewModel = PaymentStandardHeaderViewModel(
            model: createModel(paymentType: .pix),
            service: mockService,
            presenter: mockPresenter,
            dependencies: DependencyContainerMock(analytics, consumerManager)
        )

        pixViewModel.didTapInstallment()

        let fakeDescription = try XCTUnwrap(mockPresenter.description)

        XCTAssertEqual(mockPresenter.presentInstallmentsDisabledAlertCallsCount, 1)
        XCTAssertEqual(fakeDescription, "Por enquanto, não é possível parcelar via PIX.")
    }
    
    func testDidTapInfoButton_WhenCall_ShouldCallDidTapInfoButton() {
        viewModel.didTapInfoButton()
        
        XCTAssertEqual(mockPresenter.calledDidTapInfoButton, 1)
    }
    
    func testDidTapInstallmentAlert_WhenActionChangePaymentMethod_ShouldCallEventUserNavigatedWithChangePaymentMethodData() throws {
        viewModel.didTapInstallmentAlert(action: .changePaymentMethod)
        let firstEvent = try XCTUnwrap(analytics.events.first)
        let properties = try XCTUnwrap(firstEvent.properties as? [String: String])
        XCTAssertEqual(analytics.logCalledCount, 1)
        XCTAssertEqual(properties["from"], "button change payment method")
        XCTAssertEqual(properties["to"], "screen payment method")
        XCTAssertFalse(properties["user_id"].isEmpty)
        XCTAssertFalse(properties["timestamp"].isEmpty)
    }
    
    func testDidTapInstallmentAlert_WhenActionNotNow_ShouldCallEventUserNavigatedWithNotNowData() throws {
        viewModel.didTapInstallmentAlert(action: .notNow)
        let firstEvent = try XCTUnwrap(analytics.events.first)
        let properties = try XCTUnwrap(firstEvent.properties as? [String: String])
        XCTAssertEqual(analytics.logCalledCount, 1)
        XCTAssertEqual(properties["from"], "button not now")
        XCTAssertEqual(properties["to"], "screen transaction")
        XCTAssertFalse(properties["user_id"].isEmpty)
        XCTAssertFalse(properties["timestamp"].isEmpty)
    }
    
    func testDidTapInstallmentAlert_WhenActionChangeOkGotIt_ShouldCallEventUserNavigatedWithOkGotItData() throws {
        viewModel.didTapInstallmentAlert(action: .okGotIt)
        let firstEvent = try XCTUnwrap(analytics.events.first)
        let properties = try XCTUnwrap(firstEvent.properties as? [String: String])
        XCTAssertEqual(analytics.logCalledCount, 1)
        XCTAssertEqual(properties["from"], "button ok")
        XCTAssertEqual(properties["to"], "screen transaction")
        XCTAssertFalse(properties["user_id"].isEmpty)
        XCTAssertFalse(properties["timestamp"].isEmpty)
    }
}
