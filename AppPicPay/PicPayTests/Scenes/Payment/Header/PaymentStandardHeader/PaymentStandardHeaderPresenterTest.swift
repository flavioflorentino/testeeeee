import XCTest
import FeatureFlag
@testable import UI
@testable import PicPay

final class MockPaymentStandardHeaderCoordinator: PaymentStandardHeaderCoordinating {
    var viewController: UIViewController?
    private(set) var calledPerform = false
    private(set) var calledAction: PaymentStandardHeaderAction?
    func perform(action: PaymentStandardHeaderAction) {
        calledPerform = true
        calledAction = action
    }
}

final class MockPaymentStandardHeaderViewController: PaymentStandardHeaderDisplay {
    private(set) var calledDisplayTooltip = false
    private(set) var calledDescriptionIsHidden = false
    private(set) var calledSetupInput = false
    private(set) var calledUpdateValue = false
    private(set) var calledSetupTitle = false
    private(set) var calledSetupDescription = false
    private(set) var calledDisplayInstallmentButton = false
    private(set) var calledSetupImage = false
    private(set) var showAlertCallCount = 0
    private(set) var alert: Alert?
    private(set) var calledDisplayInfoButton = 0
    private(set) var calledDisplayTooltipNewInstallment = 0
    private(set) var calledOldInstallmentButton = 0
    private(set) var calledNewInstallmentButton = 0
    
    private(set) var inputValue: Double?
    private(set) var inputIsActive: Bool?
    
    private(set) var updateValue: Double?
    
    private(set) var titleText: String?
    private(set) var titleColor: UIColor?
    private(set) var titleSize: CGFloat?
    
    private(set) var descriptionText: String?
    private(set) var descriptionColor: UIColor?
    private(set) var descriptionSize: CGFloat?
    
    private(set) var imagePlacehoplder: UIImage?
    private(set) var imageType: HeaderPayment.PayeeType?
    
    private(set) var descriptionIsHidden: Bool?
    
    private(set) var tooltipValue: Double?
    
    private(set) var forceCreditCard: Bool?
    private(set) var updateInstallment: Int?
    
    private(set) var titleButton: String?
    private(set) var colorButton: UIColor?
    
    private(set) var oldIsHidden: Bool?
    private(set) var newIsHidden: Bool?
    
    private(set) var didReceiveAnInstallmentErrorCallsCount = 0
    
    func displayTooltip(cardValue: Double) {
        calledDisplayTooltip = true
        tooltipValue = cardValue
    }
    
    func description(isHidden: Bool) {
        calledDescriptionIsHidden = true
        descriptionIsHidden = isHidden
    }
        
    func setupInput(value: Double, isActive: Bool) {
        calledSetupInput = true
        inputValue = value
        inputIsActive = isActive
    }
    
    func setupTitle(title: String, color: UIColor, ofSize: CGFloat) {
        calledSetupTitle = true
        titleText = title
        titleColor = color
        titleSize = ofSize
    }
    
    func setupDescription(description: String?, color: UIColor, ofSize: CGFloat) {
        calledSetupDescription = true
        descriptionText = description
        descriptionColor = color
        descriptionSize = ofSize
    }
    
    func updateValue(_ value: Double) {
        calledUpdateValue = true
        updateValue = value
    }
    
    func displayInstallmentButton(title: String?, image: UIImage?, color: UIColor, borderColor: UIColor) {
        calledDisplayInstallmentButton = true
        titleButton = title
        colorButton = color
    }
    
    func updateInstallment(installment: Int, forceCreditCard: Bool) {
        calledDisplayInstallmentButton = true
        self.forceCreditCard = forceCreditCard
        self.updateInstallment = installment
    }
    
    func setupImage(imageUrl image: URL?, placeholder: UIImage, type: HeaderPayment.PayeeType) {
        calledSetupImage = true
        imagePlacehoplder = placeholder
        imageType = type
    }

    func showAlert(_ alert: Alert) {
        showAlertCallCount += 1
        self.alert = alert
    }
    
    func didReceiveAnInstallmentError(title: String, description: NSMutableAttributedString, image: UIImage?, buttons: [PaymentStandardHeaderViewController.ButtonAction]) {
        didReceiveAnInstallmentErrorCallsCount += 1
    }
    
    func displayInfoButton() {
        calledDisplayInfoButton += 1
    }
    
    func displayTooltipNewInstallment(cardValue: Double) {
        calledDisplayTooltipNewInstallment += 1
        tooltipValue = cardValue
    }
    
    func oldInstallmentButton(isHidden: Bool) {
        calledOldInstallmentButton += 1
        oldIsHidden = isHidden
    }
    
    func newInstallmentButton(isHidden: Bool) {
        calledNewInstallmentButton += 1
        newIsHidden = isHidden
    }
}

final class PaymentStandardHeaderPresenterTest: XCTestCase {
    private var featureManagerMock = FeatureManagerMock()
    private lazy var mockDependencies = DependencyContainerMock(featureManagerMock)
    private let mockViewController = MockPaymentStandardHeaderViewController()
    private let mockCoordinator = MockPaymentStandardHeaderCoordinator()
    private lazy var model: HeaderStandard = createModel(isEditable: false)
    private lazy var presenter: PaymentStandardHeaderPresenter = {
        let presenter = PaymentStandardHeaderPresenter(model: model, coordinator: mockCoordinator)
        presenter.viewController = mockViewController
        return presenter
    }()
    
    private func createModel(isEditable: Bool = true, link: String? = nil, newInstallmentEnable: Bool = false) -> HeaderStandard {        
        featureManagerMock.override(keys: .featureInstallmentNewButton, with: false)
        return HeaderStandard(
            title: "title",
            description: "description",
            image: .url("image"),
            isEditable: isEditable,
            link: link,
            newInstallmentEnable: newInstallmentEnable,
            isInstallmentsEnabled: true
        )
    }
    
    func testViewDidLoadShouldCallUpdateValueWithValue() {
        model = createModel(isEditable: false, newInstallmentEnable: false)
        
        presenter.viewDidLoad(value: 50.0, payeeType: .pro, paymentType: .p2p)
        
        XCTAssertTrue(mockViewController.calledUpdateValue)
        XCTAssertEqual(mockViewController.updateValue, 50.0)
    }
    
    func testViewDidLoadTypeP2PShouldCallSetupTitleWithValue() {
        presenter.viewDidLoad(value: 50.0, payeeType: .pro, paymentType: .p2p)
        
        XCTAssertTrue(mockViewController.calledSetupTitle)
        XCTAssertEqual(mockViewController.titleText, "@\(model.title)")
        XCTAssertEqual(mockViewController.titleColor?.cgColor, Palette.ppColorGrayscale600.cgColor)
        XCTAssertEqual(mockViewController.titleSize, 11)
    }
    
    func testViewDidLoadTypeP2PShouldCallSetupDescriptionWithValue() {
        presenter.viewDidLoad(value: 50.0, payeeType: .pro, paymentType: .p2p)
        
        XCTAssertTrue(mockViewController.calledSetupDescription)
        XCTAssertEqual(mockViewController.descriptionText, model.description)
        XCTAssertEqual(mockViewController.descriptionColor?.cgColor, Palette.ppColorGrayscale400.cgColor)
        XCTAssertEqual(mockViewController.descriptionSize, 12)
    }
    
    func testViewDidLoadTypeP2PShouldCallSetupImageWithValue() {
        presenter.viewDidLoad(value: 50.0, payeeType: .pro, paymentType: .p2p)
        
        XCTAssertTrue(mockViewController.calledSetupImage)
        XCTAssertEqual(mockViewController.imagePlacehoplder, #imageLiteral(resourceName: "avatar_person"))
        XCTAssertEqual(mockViewController.imageType, .pro)
    }
    
    func testViewDidLoadTypeP2PShouldCallDescriptionIsHiddenWithValue() {
        presenter.viewDidLoad(value: 50.0, payeeType: .pro, paymentType: .p2p)
        
        XCTAssertTrue(mockViewController.calledDescriptionIsHidden)
        XCTAssertEqual(mockViewController.descriptionIsHidden, false)
    }
    
    func testUpdateCardValueShouldCallDisplayTooltipWhenOldInstallmentValue() {
        model = createModel(isEditable: false, newInstallmentEnable: false)

        presenter.updateCardValue(cardValue: 100)
        
        XCTAssertTrue(mockViewController.calledDisplayTooltip)
        
    }

    func testShowAlertDidCallShowAlert() throws {
        let description = "Description"
        let alert = Alert(title: "Parcelamento indisponível", text: description)

        presenter.presentInstallmentsDisabledAlert(withDescription: description)
        let fakeAlert = try XCTUnwrap(mockViewController.alert)

        XCTAssertEqual(fakeAlert.title, alert.title)
        XCTAssertEqual(fakeAlert.text, alert.text)
    }
    
    func testPresentError_ShouldCallShowAlert() throws {
        let description = NSAttributedString(string: "Error")
        presenter.presentError(message: description.string)
        
        let fakeAlert = try XCTUnwrap(mockViewController.alert)

        XCTAssertEqual(fakeAlert.title, NSAttributedString())
        XCTAssertEqual(fakeAlert.text, description)
    }
    
    func testErrorInstallment_WhenTypeIsEmptyValueAndWrongPaymentMethod_ShouldCallDidReceiveAnInstallmentError() {
        presenter.errorInstallment(type: .emptyValueAndWrongPaymentMethod)
        
        XCTAssertEqual(mockViewController.didReceiveAnInstallmentErrorCallsCount, 1)
    }
    
    func testErrorInstallment_WhenTypeIsWrongPaymentMethod_ShouldCallDidReceiveAnInstallmentError() {
        presenter.errorInstallment(type: .wrongPaymentMethod)
        
        XCTAssertEqual(mockViewController.didReceiveAnInstallmentErrorCallsCount, 1)
    }
    
    func testErrorInstallment_WhenTypeIsEmptyValue_ShouldCallDidReceiveAnInstallmentError() {
        presenter.errorInstallment(type: .emptyValue)
        
        XCTAssertEqual(mockViewController.didReceiveAnInstallmentErrorCallsCount, 1)
    }
    
    func testDisplayInstallmentShouldCallDisplayInstallmentButtonn() {
        presenter.displayInstallment(isEnable: true, installment: 2)
        
        XCTAssertTrue(mockViewController.calledDisplayInstallmentButton)
        XCTAssertEqual(mockViewController.updateInstallment, 2)
        XCTAssertEqual(mockViewController.forceCreditCard, false)
        XCTAssertEqual(mockViewController.titleButton, "2x")
    }
    
    func testDisplayInstallmentReturnDisabledButton() {
        presenter.displayInstallment(isEnable: false, installment: 2)
        
        XCTAssertEqual(mockViewController.colorButton?.cgColor, Palette.ppColorGrayscale300.cgColor)
    }
    
    func testDisplayInstallmentReturnEnabledButton() {
        presenter.displayInstallment(isEnable: true, installment: 2)
        
        XCTAssertEqual(mockViewController.colorButton?.cgColor, Palette.ppColorBranding300.cgColor)
    }
    
    func testDidNextStepShouldCallCoordinator() {
        presenter.didNextStep(action: .openP2PProfile(payeeId: "2"))
        
        XCTAssertTrue(mockCoordinator.calledPerform)
    }
    
    func testViewDidLoad_WhenCallLinkNotNil_ShouldCallDisplayInfoButton() {
        model = createModel(link: "1234")
    
        presenter.viewDidLoad(value: 50.0, payeeType: .pro, paymentType: .p2p)
        
        XCTAssertEqual(mockViewController.calledDisplayInfoButton, 1)
    }
    
    func testViewDidLoad_WhenCallLinkNil_ShouldNotCallDisplayInfoButton() {
        model = createModel(link: nil)
        
        presenter.viewDidLoad(value: 50.0, payeeType: .pro, paymentType: .p2p)
        
        XCTAssertEqual(mockViewController.calledDisplayInfoButton, 0)
    }
    
    func testDisplayInstallment_WhenNewInstallmentEnable_ShouldCallNewInstallmentButton() {
        model = createModel(newInstallmentEnable: true)

        presenter.displayInstallment(isEnable: true, installment: 1)
        
        XCTAssertEqual(mockViewController.calledNewInstallmentButton, 1)
        XCTAssertEqual(mockViewController.newIsHidden, false)
    }
    
    func testDisplayInstallment_WhenNewInstallmentDisable_ShouldCallNewInstallmentButton() {
        model = createModel(newInstallmentEnable: false)
        
        presenter.displayInstallment(isEnable: true, installment: 1)
        
        XCTAssertEqual(mockViewController.calledNewInstallmentButton, 1)
        XCTAssertEqual(mockViewController.newIsHidden, true)
    }
    
    func testDisplayInstallment_WhenNewInstallmentEnable_ShouldCallOldInstallmentButton() {
        model = createModel(newInstallmentEnable: true)
        
        presenter.displayInstallment(isEnable: true, installment: 1)
        
        XCTAssertEqual(mockViewController.calledOldInstallmentButton, 1)
        XCTAssertEqual(mockViewController.oldIsHidden, true)
    }
    
    func testDisplayInstallment_WhenNewInstallmentDisable_ShouldCallOldInstallmentButton() {
        model = createModel(newInstallmentEnable: false)
        
        presenter.displayInstallment(isEnable: true, installment: 1)
        
        XCTAssertEqual(mockViewController.calledOldInstallmentButton, 1)
        XCTAssertEqual(mockViewController.oldIsHidden, false)
    }
    
    func testUpdateCardValue_WhenNewInstallmentEnable_ShouldCallDisplayTooltipNewInstallment() {
        model = createModel(isEditable: false, newInstallmentEnable: true)
        
        presenter.updateCardValue(cardValue: 100)
        
        XCTAssertEqual(mockViewController.calledDisplayTooltipNewInstallment, 1)
    }
    
    func testDidTapInfoButton_WhenUrlIsHttp_ShouldCallOpenWebview() throws {
        //Given
        let urlString = "https://somelink.com"
        model = createModel(link: urlString)
        
        //When
        presenter.didTapInfoButton()
        
        //Then
        XCTAssert(mockCoordinator.calledPerform)
        
        let expectedUrl = try XCTUnwrap(URL(string: urlString))
        let calledAction: PaymentStandardHeaderAction = try XCTUnwrap(mockCoordinator.calledAction)
    
        guard case let .openWebView(url) = calledAction else {
            XCTFail("Expected openWebView action to be called")
            return
        }
        
        XCTAssertEqual(expectedUrl, url)
    }
    
    func testDidTapInfoButton_WhenUrlIsNotHttp_ShouldCallOpenUrl() throws {
        //Given
        let urlString = "picpay://somelink.com"
        model = createModel(link: urlString)
        
        //When
        presenter.didTapInfoButton()
        
        //Then
        XCTAssert(mockCoordinator.calledPerform)
        
        let expectedUrl = try XCTUnwrap(URL(string: urlString))
        let calledAction: PaymentStandardHeaderAction = try XCTUnwrap(mockCoordinator.calledAction)
    
        guard case let .openUrl(url) = calledAction else {
            XCTFail("Expected openUrl action to be called")
            return
        }
        
        XCTAssertEqual(expectedUrl, url)
    }
}
