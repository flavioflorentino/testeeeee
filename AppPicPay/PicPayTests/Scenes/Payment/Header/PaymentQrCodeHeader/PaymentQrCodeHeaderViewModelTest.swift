import XCTest
@testable import PicPay

final class MockPaymentQrCodeHeaderService: PaymentQrCodeHeaderServicing {
    private let installmentAvailable: Int
    private let valueCard: Double
    private let isBalance: Bool
    
    init(installmentAvailable: Int = 1, valueCard: Double = 0, isBalance: Bool = true) {
        self.installmentAvailable = installmentAvailable
        self.valueCard = valueCard
        self.isBalance = isBalance
    }
    
    func numberInstallmentAvailable(total: Double, installment: Int) -> Int {
        return installmentAvailable
    }
    
    func cardValue(total: Double, installment: Int) -> Double {
        return valueCard
    }
    
    func paymentMethodIsBalance() -> Bool {
        return isBalance
    }
}

final class MockPaymentQrCodeHeaderPresenter: PaymentQrCodeHeaderPresenting {
    var viewController: PaymentQrCodeHeaderDisplay?
    
    private(set) var calledViewDidLoad = 0
    private(set) var calledDisplayInstallment = 0
    private(set) var calledUpdateInstallment = 0
    private(set) var calledUpdateInput = 0
    private(set) var calledDidTapFinish = 0
    private(set) var calledDidNextStep = 0
    
    private(set) var didLoadValue: Double?
    private(set) var payeeType: HeaderPayment.PayeeType?
    private(set) var paymentType: HeaderPayment.PaymentType?
    
    private(set) var displayIsEnable: Bool?
    private(set) var displayInstallment: Int?
    
    private(set) var updateInstallment: Int?
    private(set) var forceCreditCard: Bool?
    
    private(set) var inputValue: Double?
    
    private(set) var nextStepAction: PaymentQrCodeHeaderAction?
    
    func viewDidLoad(value: Double, payeeType: HeaderPayment.PayeeType, paymentType: HeaderPayment.PaymentType) {
        calledViewDidLoad += 1
        didLoadValue = value
        self.payeeType = payeeType
        self.paymentType = paymentType
    }
    
    func displayInstallment(isEnable: Bool, installment: Int) {
        calledDisplayInstallment += 1
        displayIsEnable = isEnable
        displayInstallment = installment
    }
    
    func updateInstallment(installment: Int, forceCreditCard: Bool) {
        calledUpdateInstallment += 1
        updateInstallment = installment
        self.forceCreditCard = forceCreditCard
    }
    
    func updateInput(value: Double) {
        calledUpdateInput += 1
        inputValue = value
    }
    
    func didTapFinish() {
        calledDidTapFinish += 1
    }
    
    func didNextStep(action: PaymentQrCodeHeaderAction) {
        calledDidNextStep += 1
        nextStepAction = action
    }
}

final class PaymentQrCodeHeaderViewModelTest: XCTestCase {
    func createModel(installment: Int = 1, paymentType: HeaderPayment.PaymentType = .p2m) -> HeaderPayment {
        return HeaderPayment(
            sellerId: nil,
            payeeId: "1234",
            value: 10.00,
            installments: installment,
            payeeType: .none,
            paymentType: paymentType
        )
    }
    
    func testViewDidLoadShouldCallViewDidLoadWithValue() {
        let mockService = MockPaymentQrCodeHeaderService()
        let mockPresenter = MockPaymentQrCodeHeaderPresenter()
        let model = createModel()
        let viewModel = PaymentQrCodeHeaderViewModel(model: model, service: mockService, presenter: mockPresenter)
        
        viewModel.viewDidLoad()
        XCTAssertEqual(mockPresenter.calledViewDidLoad, 1)
        XCTAssertEqual(mockPresenter.didLoadValue, 10)
        XCTAssertEqual(mockPresenter.payeeType, HeaderPayment.PayeeType.none)
        XCTAssertEqual(mockPresenter.paymentType, .p2m)
    }
    
    func testViewWillAppearShouldCallUpdateInputWithValue() {
        let mockService = MockPaymentQrCodeHeaderService()
        let mockPresenter = MockPaymentQrCodeHeaderPresenter()
        let model = createModel()
        let viewModel = PaymentQrCodeHeaderViewModel(model: model, service: mockService, presenter: mockPresenter)
        
        viewModel.viewWillAppear()
        XCTAssertEqual(mockPresenter.calledUpdateInput, 1)
        XCTAssertEqual(mockPresenter.inputValue, 10)
    }
    
    func testViewWillAppearShouldCallupdateInstallmentWithValue() {
        let mockService = MockPaymentQrCodeHeaderService()
        let mockPresenter = MockPaymentQrCodeHeaderPresenter()
        let model = createModel()
        let viewModel = PaymentQrCodeHeaderViewModel(model: model, service: mockService, presenter: mockPresenter)
        
        viewModel.viewWillAppear()
        XCTAssertEqual(mockPresenter.calledUpdateInstallment, 1)
        XCTAssertEqual(mockPresenter.updateInstallment, 1)
    }
    
    func testViewWillAppearShouldCallupdateInstallmentWithForceCreditCardFalseWhenPaymentTypeNotP2M() {
        let mockService = MockPaymentQrCodeHeaderService()
        let mockPresenter = MockPaymentQrCodeHeaderPresenter()
        let model = createModel(installment: 3, paymentType: .p2p)
        let viewModel = PaymentQrCodeHeaderViewModel(model: model, service: mockService, presenter: mockPresenter)
        
        viewModel.viewWillAppear()
        XCTAssertEqual(mockPresenter.calledUpdateInstallment, 1)
        XCTAssertEqual(mockPresenter.forceCreditCard, false)
    }
    
    func testViewWillAppearShouldCallupdateInstallmentWithForceCreditCardFalseWhenInstallmentOne() {
        let mockService = MockPaymentQrCodeHeaderService()
        let mockPresenter = MockPaymentQrCodeHeaderPresenter()
        let model = createModel(installment: 1, paymentType: .p2m)
        let viewModel = PaymentQrCodeHeaderViewModel(model: model, service: mockService, presenter: mockPresenter)
        
        viewModel.viewWillAppear()
        XCTAssertEqual(mockPresenter.calledUpdateInstallment, 1)
        XCTAssertEqual(mockPresenter.forceCreditCard, false)
    }
    
    func testViewWillAppearShouldCallupdateInstallmentWithForceCreditCardFalseWhenIsBalanceFalse() {
        let mockService = MockPaymentQrCodeHeaderService(isBalance: false)
        let mockPresenter = MockPaymentQrCodeHeaderPresenter()
        let model = createModel(installment: 3, paymentType: .p2m)
        let viewModel = PaymentQrCodeHeaderViewModel(model: model, service: mockService, presenter: mockPresenter)
        
        viewModel.viewWillAppear()
        XCTAssertEqual(mockPresenter.calledUpdateInstallment, 1)
        XCTAssertEqual(mockPresenter.forceCreditCard, false)
    }

    func testViewWillAppearShouldCallupdateInstallmentWithForceCreditCardTrue() {
        let mockService = MockPaymentQrCodeHeaderService(isBalance: true)
        let mockPresenter = MockPaymentQrCodeHeaderPresenter()
        let model = createModel(installment: 3, paymentType: .p2m)
        let viewModel = PaymentQrCodeHeaderViewModel(model: model, service: mockService, presenter: mockPresenter)
        
        viewModel.viewWillAppear()
        XCTAssertEqual(mockPresenter.calledUpdateInstallment, 1)
        XCTAssertEqual(mockPresenter.forceCreditCard, true)
    }
    
    func testChangeValueShouldCallUpdateInputWithValue() {
        let mockService = MockPaymentQrCodeHeaderService()
        let mockPresenter = MockPaymentQrCodeHeaderPresenter()
        let model = createModel()
        let viewModel = PaymentQrCodeHeaderViewModel(model: model, service: mockService, presenter: mockPresenter)
        
        viewModel.changeValue(value: 20)
        XCTAssertEqual(mockPresenter.calledUpdateInput, 1)
        XCTAssertEqual(mockPresenter.inputValue, 20)
    }
    
    func testChangeValueShouldCallupdateInstallmentWithValue() {
        let mockService = MockPaymentQrCodeHeaderService()
        let mockPresenter = MockPaymentQrCodeHeaderPresenter()
        let model = createModel()
        let viewModel = PaymentQrCodeHeaderViewModel(model: model, service: mockService, presenter: mockPresenter)
        
        viewModel.changeValue(value: 20)
        XCTAssertEqual(mockPresenter.calledUpdateInstallment, 1)
        XCTAssertEqual(mockPresenter.updateInstallment, 1)
    }
    
    func testChangeValueShouldCallupdateInstallmentWithForceCreditCardFalseWhenPaymentTypeNotP2M() {
        let mockService = MockPaymentQrCodeHeaderService()
        let mockPresenter = MockPaymentQrCodeHeaderPresenter()
        let model = createModel(installment: 3, paymentType: .p2p)
        let viewModel = PaymentQrCodeHeaderViewModel(model: model, service: mockService, presenter: mockPresenter)
        
        viewModel.changeValue(value: 20)
        XCTAssertEqual(mockPresenter.calledUpdateInstallment, 1)
        XCTAssertEqual(mockPresenter.forceCreditCard, false)
    }
    
    func testChangeValueShouldCallupdateInstallmentWithForceCreditCardFalseWhenInstallmentOne() {
        let mockService = MockPaymentQrCodeHeaderService()
        let mockPresenter = MockPaymentQrCodeHeaderPresenter()
        let model = createModel(installment: 1, paymentType: .p2m)
        let viewModel = PaymentQrCodeHeaderViewModel(model: model, service: mockService, presenter: mockPresenter)
        
        viewModel.changeValue(value: 20)
        XCTAssertEqual(mockPresenter.calledUpdateInstallment, 1)
        XCTAssertEqual(mockPresenter.forceCreditCard, false)
    }
    
    func testChangeValueShouldCallupdateInstallmentWithForceCreditCardFalseWhenIsBalanceFalse() {
        let mockService = MockPaymentQrCodeHeaderService(isBalance: false)
        let mockPresenter = MockPaymentQrCodeHeaderPresenter()
        let model = createModel(installment: 3, paymentType: .p2m)
        let viewModel = PaymentQrCodeHeaderViewModel(model: model, service: mockService, presenter: mockPresenter)
        
        viewModel.changeValue(value: 20)
        XCTAssertEqual(mockPresenter.calledUpdateInstallment, 1)
        XCTAssertEqual(mockPresenter.forceCreditCard, false)
    }

    func testChangeValueShouldCallupdateInstallmentWithForceCreditCardTrue() {
        let mockService = MockPaymentQrCodeHeaderService(isBalance: true)
        let mockPresenter = MockPaymentQrCodeHeaderPresenter()
        let model = createModel(installment: 3, paymentType: .p2m)
        let viewModel = PaymentQrCodeHeaderViewModel(model: model, service: mockService, presenter: mockPresenter)
        
        viewModel.changeValue(value: 20)
        XCTAssertEqual(mockPresenter.calledUpdateInstallment, 1)
        XCTAssertEqual(mockPresenter.forceCreditCard, true)
    }
    
    func testDidTapInstallmentNotShouldCallDidNextStepWhenP2M() {
        let mockService = MockPaymentQrCodeHeaderService()
        let mockPresenter = MockPaymentQrCodeHeaderPresenter()
        let model = createModel(paymentType: .p2m)
        let viewModel = PaymentQrCodeHeaderViewModel(model: model, service: mockService, presenter: mockPresenter)
        
        viewModel.didTapInstallment()
        XCTAssertEqual(mockPresenter.calledDidNextStep, 0)
    }
    
    func testDidTapInstallmentShouldCallDidNextStepWhenNotP2MAndCardBiggestZero() throws {
        let mockService = MockPaymentQrCodeHeaderService(valueCard: 10.0)
        let mockPresenter = MockPaymentQrCodeHeaderPresenter()
        let model = createModel(paymentType: .p2p)
        let viewModel = PaymentQrCodeHeaderViewModel(model: model, service: mockService, presenter: mockPresenter)
        
        viewModel.didTapInstallment()
        XCTAssertEqual(mockPresenter.calledDidNextStep, 1)
        switch try mockPresenter.nextStepAction.safe() {
        case .openInstallmentOptions:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testDidTapInstallmentShouldNotCallDidNextStepWhenNotP2MAndCardIsZero() throws {
        let mockService = MockPaymentQrCodeHeaderService(valueCard: 0.0)
        let mockPresenter = MockPaymentQrCodeHeaderPresenter()
        let model = createModel(paymentType: .p2p)
        let viewModel = PaymentQrCodeHeaderViewModel(model: model, service: mockService, presenter: mockPresenter)
        
        viewModel.didTapInstallment()
        XCTAssertEqual(mockPresenter.calledDidNextStep, 0)
    }
    
    
    func testDidTapInstallmentShouldNotCallDidNextStepWhenP2MAndCardBiggestZero() throws {
        let mockService = MockPaymentQrCodeHeaderService(valueCard: 10.0)
        let mockPresenter = MockPaymentQrCodeHeaderPresenter()
        let model = createModel(paymentType: .p2m)
        let viewModel = PaymentQrCodeHeaderViewModel(model: model, service: mockService, presenter: mockPresenter)
        
        viewModel.didTapInstallment()
        XCTAssertEqual(mockPresenter.calledDidNextStep, 0)
    }
    
    func testDidTapFinishShouldCallDidTapFinish() {
        let mockService = MockPaymentQrCodeHeaderService()
        let mockPresenter = MockPaymentQrCodeHeaderPresenter()
        let model = createModel()
        let viewModel = PaymentQrCodeHeaderViewModel(model: model, service: mockService, presenter: mockPresenter)
        
        viewModel.didTapFinish()
        XCTAssertEqual(mockPresenter.calledDidTapFinish, 1)
    }
}
