import XCTest
@testable import UI
@testable import PicPay

final class MockPaymentQrCodeHeaderCoordinator: PaymentQrCodeHeaderCoordinating {
    var viewController: UIViewController?
    private(set) var calledPerformAction = 0
    private(set) var action: PaymentQrCodeHeaderAction?
    
    func perform(action: PaymentQrCodeHeaderAction) {
        calledPerformAction += 1
        self.action = action
    }
}

final class MockPaymentQrCodeHeaderViewController: PaymentQrCodeHeaderDisplay {
    private(set) var calledSetupInput = 0
    private(set) var calledUpdateValue = 0
    private(set) var calledDiplayProfileName = 0
    private(set) var calledDiplayBackgroundImage = 0
    private(set) var calledDiplayProfileImage = 0
    private(set) var calledDiplayFinishButton = 0
    private(set) var calledDisplayInstallmentButton = 0
    private(set) var calledUpdateInstallment = 0
    private(set) var calledDiplayOrder = 0
    
    private(set) var inputValue: Double?
    private(set) var updateValue: Double?
    
    private(set) var profileName: String?
    
    private(set) var backgroundImage: URL?
    
    private(set) var profileImage: URL?
    private(set) var profilePlaceholder: UIImage?
    private(set) var profileType: HeaderPayment.PayeeType?
    
    private(set) var finishTitle: String?
    private(set) var finishImage: UIImage?
    
    private(set) var installmentTitle: String?
    private(set) var installmentColor: UIColor?
    private(set) var installmentBorderColor: UIColor?
    
    private(set) var installmentValue: Int?
    private(set) var installmentForceCreditCard: Bool?
    
    private(set) var order: String?
    private(set) var orderIsDisabled: Bool?
    
    func setupInput(value: Double) {
        calledSetupInput += 1
        inputValue = value
    }
    
    func updateValue(value: Double) {
        calledUpdateValue += 1
        updateValue = value
    }
    
    func diplayProfileName(_ profileName: String) {
        calledDiplayProfileName += 1
        self.profileName = profileName
    }
    
    func diplayBackgroundImage(_ image: URL?) {
        calledDiplayBackgroundImage += 1
        backgroundImage = image
    }
    
    func diplayProfileImage(_ image: URL?, placeholder: UIImage, type: HeaderPayment.PayeeType) {
        calledDiplayProfileImage += 1
        profileImage = image
        profilePlaceholder = placeholder
        profileType = type
    }
    
    func diplayFinishButton(title: String, image: UIImage?) {
        calledDiplayFinishButton += 1
        finishTitle = title
        finishImage = image
    }
    
    func displayInstallmentButton(title: String?, color: UIColor, borderColor: UIColor) {
        calledDisplayInstallmentButton += 1
        installmentTitle = title
        installmentColor = color
        installmentBorderColor = borderColor
    }
    
    func updateInstallment(installment: Int, forceCreditCard: Bool) {
        calledUpdateInstallment += 1
        installmentValue = installment
        installmentForceCreditCard = forceCreditCard
    }
    
    func diplayOrder(_ order: String?, isDisabled: Bool) {
        calledDiplayOrder += 1
        self.order = order
        self.orderIsDisabled = isDisabled
    }
}

final class PaymentQrCodeHeaderPresenterTest: XCTestCase {
    private func createModel(isModal: Bool = true, orderNumber: String? = nil) -> HeaderQrCode {
        return HeaderQrCode(
            profileName: "Gustavo",
            backgroundImage: nil,
            profileImage: nil,
            orderNumber: orderNumber,
            isModal: isModal
        )
    }
    
    func testViewDidLoadShouldCallUpdateValue() {
        let mockCoordinator = MockPaymentQrCodeHeaderCoordinator()
        let mockViewController = MockPaymentQrCodeHeaderViewController()
        let model = createModel()
        let presenter = PaymentQrCodeHeaderPresenter(model: model, coordinator: mockCoordinator)
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(value: 100, payeeType: .none, paymentType: .p2m)
        
        XCTAssertEqual(mockViewController.calledUpdateValue, 1)
        XCTAssertEqual(mockViewController.updateValue, 100)
    }
    
    func testViewDidLoadShouldCallDiplayOrder() {
        let mockCoordinator = MockPaymentQrCodeHeaderCoordinator()
        let mockViewController = MockPaymentQrCodeHeaderViewController()
        let model = createModel()
        let presenter = PaymentQrCodeHeaderPresenter(model: model, coordinator: mockCoordinator)
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(value: 100, payeeType: .none, paymentType: .p2m)
        
        XCTAssertEqual(mockViewController.calledDiplayOrder, 1)
    }
    
    func testViewDidLoadShouldCallDiplayOrderWithIsDisabledTrueWhenOrderNumberNil() {
        let mockCoordinator = MockPaymentQrCodeHeaderCoordinator()
        let mockViewController = MockPaymentQrCodeHeaderViewController()
        let model = createModel(orderNumber: nil)
        let presenter = PaymentQrCodeHeaderPresenter(model: model, coordinator: mockCoordinator)
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(value: 100, payeeType: .none, paymentType: .p2m)
        
        XCTAssertEqual(mockViewController.order, nil)
        XCTAssertEqual(mockViewController.orderIsDisabled, true)
    }
    
    func testViewDidLoadShouldCallDiplayOrderWithIsDisabledFalseWhenOrderNumberNotNil() {
        let mockCoordinator = MockPaymentQrCodeHeaderCoordinator()
        let mockViewController = MockPaymentQrCodeHeaderViewController()
        let model = createModel(orderNumber: "1234")
        let presenter = PaymentQrCodeHeaderPresenter(model: model, coordinator: mockCoordinator)
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(value: 100, payeeType: .none, paymentType: .p2m)
        
        XCTAssertEqual(mockViewController.order, "\(PaymentLocalizable.order.text) 1234")
        XCTAssertEqual(mockViewController.orderIsDisabled, false)
    }
    
    func testViewDidLoadShouldCallDiplayFinishButtonAsModal() {
        let mockCoordinator = MockPaymentQrCodeHeaderCoordinator()
        let mockViewController = MockPaymentQrCodeHeaderViewController()
        let model = createModel(isModal: true)
        let presenter = PaymentQrCodeHeaderPresenter(model: model, coordinator: mockCoordinator)
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(value: 100, payeeType: .none, paymentType: .p2m)
        
        XCTAssertEqual(mockViewController.calledDiplayFinishButton, 1)
        XCTAssertEqual(mockViewController.finishTitle, DefaultLocalizable.btCancel.text)
        XCTAssertEqual(mockViewController.finishImage, nil)
    }
    
    func testViewDidLoadShouldCallDiplayFinishButtonAsPush() {
        let mockCoordinator = MockPaymentQrCodeHeaderCoordinator()
        let mockViewController = MockPaymentQrCodeHeaderViewController()
        let model = createModel(isModal: false)
        let presenter = PaymentQrCodeHeaderPresenter(model: model, coordinator: mockCoordinator)
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(value: 100, payeeType: .none, paymentType: .p2m)
        
        XCTAssertEqual(mockViewController.calledDiplayFinishButton, 1)
        XCTAssertEqual(mockViewController.finishTitle, String())
        XCTAssertEqual(mockViewController.finishImage, Assets.back.image)
    }
    
    func testViewDidLoadShouldCallDiplayBackgroundImage() {
        let mockCoordinator = MockPaymentQrCodeHeaderCoordinator()
        let mockViewController = MockPaymentQrCodeHeaderViewController()
        let model = createModel()
        let presenter = PaymentQrCodeHeaderPresenter(model: model, coordinator: mockCoordinator)
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(value: 100, payeeType: .none, paymentType: .p2m)
        
        XCTAssertEqual(mockViewController.calledDiplayBackgroundImage, 1)
    }
    
    func testViewDidLoadShouldCallDiplayProfileName() {
        let mockCoordinator = MockPaymentQrCodeHeaderCoordinator()
        let mockViewController = MockPaymentQrCodeHeaderViewController()
        let model = createModel()
        let presenter = PaymentQrCodeHeaderPresenter(model: model, coordinator: mockCoordinator)
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(value: 100, payeeType: .none, paymentType: .p2m)
        
        XCTAssertEqual(mockViewController.calledDiplayProfileName, 1)
        XCTAssertEqual(mockViewController.profileName, model.profileName)
    }
    
    func testViewDidLoadShouldCallDiplayDiplayProfileImageWithQrWhenP2M() {
        let mockCoordinator = MockPaymentQrCodeHeaderCoordinator()
        let mockViewController = MockPaymentQrCodeHeaderViewController()
        let model = createModel()
        let presenter = PaymentQrCodeHeaderPresenter(model: model, coordinator: mockCoordinator)
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(value: 100, payeeType: .none, paymentType: .p2m)
        
        XCTAssertEqual(mockViewController.calledDiplayProfileImage, 1)
        XCTAssertEqual(mockViewController.profilePlaceholder, Assets.Ilustration.iluP2mPlaceholder.image)
    }
    
    func testViewDidLoadShouldCallDiplayDiplayProfileImageWithQrWhenNotP2M() {
        let mockCoordinator = MockPaymentQrCodeHeaderCoordinator()
        let mockViewController = MockPaymentQrCodeHeaderViewController()
        let model = createModel()
        let presenter = PaymentQrCodeHeaderPresenter(model: model, coordinator: mockCoordinator)
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(value: 100, payeeType: .none, paymentType: .p2p)
        
        XCTAssertEqual(mockViewController.calledDiplayProfileImage, 1)
        XCTAssertEqual(mockViewController.profilePlaceholder, Assets.NewGeneration.avatarPlace.image)
    }
    
    func testUpdateInputShouldCallSetupInput() {
        let mockCoordinator = MockPaymentQrCodeHeaderCoordinator()
        let mockViewController = MockPaymentQrCodeHeaderViewController()
        let model = createModel()
        let presenter = PaymentQrCodeHeaderPresenter(model: model, coordinator: mockCoordinator)
        
        presenter.viewController = mockViewController
        presenter.updateInput(value: 50)
        
        XCTAssertEqual(mockViewController.calledSetupInput, 1)
        XCTAssertEqual(mockViewController.inputValue, 50)
    }
    
    func testUpdateInstallmentShouldCallUpdateInstallment() {
        let mockCoordinator = MockPaymentQrCodeHeaderCoordinator()
        let mockViewController = MockPaymentQrCodeHeaderViewController()
        let model = createModel()
        let presenter = PaymentQrCodeHeaderPresenter(model: model, coordinator: mockCoordinator)
        
        presenter.viewController = mockViewController
        presenter.updateInstallment(installment: 3, forceCreditCard: true)
        
        XCTAssertEqual(mockViewController.calledUpdateInstallment, 1)
        XCTAssertEqual(mockViewController.installmentValue, 3)
        XCTAssertEqual(mockViewController.installmentForceCreditCard, true)
    }
    
    func testDidTapFinishShouldCallDidNextStepFinishModal() throws {
        let mockCoordinator = MockPaymentQrCodeHeaderCoordinator()
        let mockViewController = MockPaymentQrCodeHeaderViewController()
        let model = createModel(isModal: true)
        let presenter = PaymentQrCodeHeaderPresenter(model: model, coordinator: mockCoordinator)
        
        presenter.viewController = mockViewController
        presenter.didTapFinish()
        
        XCTAssertEqual(mockCoordinator.calledPerformAction, 1)
        switch try mockCoordinator.action.safe() {
        case .finishModal:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testDidTapFinishShouldCallDidNextStepFinishPush() throws {
        let mockCoordinator = MockPaymentQrCodeHeaderCoordinator()
        let mockViewController = MockPaymentQrCodeHeaderViewController()
        let model = createModel(isModal: false)
        let presenter = PaymentQrCodeHeaderPresenter(model: model, coordinator: mockCoordinator)
        
        presenter.viewController = mockViewController
        presenter.didTapFinish()
        
        XCTAssertEqual(mockCoordinator.calledPerformAction, 1)
        switch try mockCoordinator.action.safe() {
        case .finishPush:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testDisplayInstallmentCallDisplayInstallmentButtonDisable() throws {
        let mockCoordinator = MockPaymentQrCodeHeaderCoordinator()
        let mockViewController = MockPaymentQrCodeHeaderViewController()
        let model = createModel()
        let presenter = PaymentQrCodeHeaderPresenter(model: model, coordinator: mockCoordinator)
        
        presenter.viewController = mockViewController
        presenter.displayInstallment(isEnable: false, installment: 2)
        
        XCTAssertEqual(mockViewController.calledDisplayInstallmentButton, 1)
        XCTAssertEqual(mockViewController.installmentTitle, "\(PaymentLocalizable.installment.text) 2x")
        XCTAssertEqual(mockViewController.installmentColor?.cgColor, Palette.ppColorGrayscale300.color.cgColor)
    }
    
    func testDisplayInstallmentCallDisplayInstallmentButtonEnable() throws {
        let mockCoordinator = MockPaymentQrCodeHeaderCoordinator()
        let mockViewController = MockPaymentQrCodeHeaderViewController()
        let model = createModel()
        let presenter = PaymentQrCodeHeaderPresenter(model: model, coordinator: mockCoordinator)
        
        presenter.viewController = mockViewController
        presenter.displayInstallment(isEnable: true, installment: 2)
        
        XCTAssertEqual(mockViewController.calledDisplayInstallmentButton, 1)
        XCTAssertEqual(mockViewController.installmentTitle, "\(PaymentLocalizable.installment.text) 2x")
        XCTAssertEqual(mockViewController.installmentColor?.cgColor, Palette.ppColorBranding300.color.cgColor)
    }
}
