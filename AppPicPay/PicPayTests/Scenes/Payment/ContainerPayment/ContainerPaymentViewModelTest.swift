import CorePayment
import XCTest
@testable import PicPay

final class MockContainerPaymentService: ContainerPaymentServicing {
    private var cvv: String?
    private var flag: CreditCardFlagTypeId
    private let first3DS: Bool
    private let isBalance: Bool
    private let defaultCard: String
    private let balance: Double
    private let cardMoney: Double
    private let success3DS: Bool
    var calledSendNotificationPicPayPro = false
    
    init(
        cvv: String? = nil,
        flag: CreditCardFlagTypeId = .visa,
        first3DS: Bool = false,
        isBalance: Bool = false,
        defaultCard: String = "3121",
        balance: Double = 0.0,
        cardMoney: Double = 0.0,
        success3DS: Bool = true
    ) {
        self.cvv = cvv
        self.flag = flag
        self.first3DS = first3DS
        self.isBalance = isBalance
        self.defaultCard = defaultCard
        self.balance = balance
        self.cardMoney = cardMoney
        self.success3DS = success3DS
    }
    var firstAccess3DS: Bool {
        return first3DS
    }
        
    var defaultCardId: String {
        return defaultCard
    }
    
    var flagCard: CreditCardFlagTypeId {
        return flag
    }
    
    var useBalance: Bool {
        return isBalance
    }

    func saveCvvCard(id: String, value: String?) {
        cvv = value
    }
    
    func cvvCard(id: String) -> String? {
        return cvv
    }
    
    func errorIs3ds(error: PicPayError) -> Bool {
        return error.code == AdyenConstant.code
    }
    
    func value(for paymentValue: PaymentValue, with payload: PaymentManagerPayload) -> Double {
        switch paymentValue {
        case .balance:
            return balance
        case .card:
            return cardMoney
        default:
            return 10
        }
    }
    
    func sendNotificationPicPayPro(payload: [String : Any]) {
        calledSendNotificationPicPayPro = true
    }
    
    func createPayload3DS(error: PicPayError) -> [String : String]? {
        return ["Sim": "Sim"]
    }
    
    func continueOn3DSTransaction(adyenPayload: [String : String], completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        guard success3DS else {
            let picpayError = PicPayError(message: DefaultLocalizable.unexpectedError.text)
            completion(.failure(picpayError))
            return
        }
        
        let receipt = ReceiptWidgetViewModel(transactionId: "1234", type: .P2P)
        completion(.success(receipt))
    }
}

final class MockPaymentService: LegacyPaymentServicing {
    typealias Success = ReceiptWidgetViewModel
    private let paymentSuccess: Bool
    private let errorType: ErrorType
    
    private(set) var calledCreateTransaction = false
    private(set) var transactionPayment: Payment?
    
    init(paymentSuccess: Bool = true, errorType: ErrorType = .none) {
        self.paymentSuccess = paymentSuccess
        self.errorType = errorType
    }
    
    func createTransaction(payment: Payment, completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        calledCreateTransaction = true
        transactionPayment = payment
        
        guard !paymentSuccess else {
            let receipt = ReceiptWidgetViewModel(transactionId: "1234", type: .P2P)
            completion(.success(receipt))
            return
        }
        
        switch errorType {
        case .error3DS:
            createError3DS(completion: completion)
        case .identityChallenge:
            createIdentityChallenge(completion: completion)
        case .cardChalleng:
            createCardChalleng(completion: completion)
        case .incorrectPassword:
            createIncorrectPassword(completion: completion)
        case .typeLimitExceeded:
            createTypeLimitExceeded(completion: completion)
        case .defaultError:
            createDefaultError(completion: completion)
        default:
            break
        }
    }
    
    private func createError3DS(completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let dictionary = MockJSON().load(resource: "3dsError")
        let picpayError = PicPayError(errorJSON: dictionary.dictionaryObject! as NSDictionary)
        completion(.failure(picpayError))
    }
    
    private func createIdentityChallenge(completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let dictionary = MockJSON().load(resource: "identityChallengeError")
        let picpayError = PicPayError(apiJSON: dictionary.dictionaryObject! as NSDictionary)
        completion(.failure(picpayError))
    }
    
    private func createCardChalleng(completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let dictionary = MockJSON().load(resource: "cardChallengeError")
        let picpayError = PicPayError(apiJSON: dictionary.dictionaryObject! as NSDictionary)
        completion(.failure(picpayError))
    }
    
    private func createIncorrectPassword(completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let dictionary = MockJSON().load(resource: "incorrectPasswordError")
        let picpayError = PicPayError(apiJSON: dictionary.dictionaryObject! as NSDictionary)
        completion(.failure(picpayError))
    }
    
    private func createTypeLimitExceeded(completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let dictionary = MockJSON().load(resource: "limitExceededError")
        let picpayError = PicPayError(apiJSON: dictionary.dictionaryObject! as NSDictionary)
        completion(.failure(picpayError))
    }
    
    private func createDefaultError(completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let picpayError = PicPayError(message: DefaultLocalizable.unexpectedError.text)
        completion(.failure(picpayError))
    }
}

extension MockPaymentService {
    enum ErrorType {
        case error3DS
        case identityChallenge
        case cardChalleng
        case incorrectPassword
        case typeLimitExceeded
        case defaultError
        case none
    }
}

final class MockContainerPaymentPresenter: ContainerPaymentPresenting {
    typealias Success = ReceiptWidgetViewModel
    var viewController: ContainerPaymentDisplay?
    
    private(set) var callPayment3DSSuccessCount = 0
    private(set) var callPaymentSuccessCount = 0
    private(set) var callHeaderHasUpdatedCount = 0
    private(set) var callAccessoryHasUpdatedCount = 0
    private(set) var callIncorrectPasswordCount = 0
    private(set) var callPresentTryAgainCount = 0
    private(set) var callDidReceiveAnErrorCount = 0
    private(set) var callLimitExceededCount = 0
    private(set) var callLimitExceededCloseCount = 0
    private(set) var callDidNextStepCount = 0
    private(set) var callUpdatePaymentMethodCount = 0

    private(set) var callPresentAccessoryValuesCount = 0

    private(set) var callPresentPasswordConfirmationCount = 0
    
    private(set) var headerValue: Double?
    private(set) var model: ReceiptWidgetViewModel?
    private(set) var headerInstallment: Int?
    
    private(set) var accessoryValue: Double?
    private(set) var headerForceCreditCard: Bool?
    
    private(set) var paymentSuccessValue: Double?
    
    private(set) var stepAction: ContainerPaymentAction?
    private(set) var picpayError: PicPayError?
    
    func paymentSuccess(model: ReceiptWidgetViewModel, paymentType: PaymentType) {
        callPaymentSuccessCount += 1
        self.model = model
    }
    
    func payment3DSSuccess(model: ReceiptWidgetViewModel) {
        callPayment3DSSuccessCount += 1
        self.model = model
    }
    
    func headerHasUpdated(value: Double, installment: Int, forceCreditCard: Bool) {
        callHeaderHasUpdatedCount += 1
        headerValue = value
        headerInstallment = installment
        headerForceCreditCard = forceCreditCard
    }
    
    func accessoryHasUpdated(value: Double) {
        callAccessoryHasUpdatedCount += 1
        accessoryValue = value
    }
    
    func incorrectPassword() {
        callIncorrectPasswordCount += 1
    }
    
    func presentTryAgain(withTitle title: String, andMessage message: String) {
        callPresentTryAgainCount += 1
    }
    
    func didReceiveAnError(error: PicPayError) {
        callDidReceiveAnErrorCount += 1
        picpayError = error
    }
    
    func limitExceeded(error: PicPayError) {
        callLimitExceededCount += 1
    }
    
    func limitExceededClose() {
        callLimitExceededCloseCount += 1
    }
    
    func didNextStep(action: ContainerPaymentAction) {
        callDidNextStepCount += 1
        stepAction = action
    }
    
    func updatePaymentMethod(_ paymentMethod: PaymentMethod) {
        callUpdatePaymentMethodCount += 1
    }
    
    func presentAccessoryValues(_ values: AccessoryValue) {
        callPresentAccessoryValuesCount += 1
    }
    
    func presentPasswordConfirmation() {
        callPresentPasswordConfirmationCount += 1
    }
}

final class ContainerPaymentViewModelTest: XCTestCase {
    private let actionsSpy = PaymentActionsDelegateSpy()
    private let mockPresenter = MockContainerPaymentPresenter()
    
    private func viewModel(
        service: ContainerPaymentServicing = MockContainerPaymentService(),
        payment: MockPaymentService = MockPaymentService()
    ) -> ContainerPaymentViewModelInputs {
        ContainerPaymentViewModel(
            service: service,
            payment: payment,
            presenter: mockPresenter,
            actionsDelegate: actionsSpy
        )
    }
    
    func testViewWillAppear_ShouldLogCallPresenterWillAppear() {
        let sut = viewModel()
        
        sut.viewWillAppear()
        
        XCTAssertEqual(actionsSpy.callViewWillAppearCount, 1)
    }
    
    func testDidTapPay_ShouldAskForPasswordAndLogDidTapPay() {
        let sut = viewModel()
        
        sut.didTapPay()
        
        XCTAssertEqual(actionsSpy.callDidTapPayCount, 1)
        XCTAssertEqual(mockPresenter.callPresentPasswordConfirmationCount, 1)
    }
    
    func testMakePayment_WhenDidNotTapInstallMents_ShouldLogDidTapPaymentAndWentToInstallmentsAsFalse() {
        let sut = viewModel()
        
        sut.makePayment(password: "123", biometry: true)
        
        XCTAssertEqual(actionsSpy.callDidTapPaymentCount, 1)
        XCTAssertEqual(actionsSpy.wentToInstallments, false)
    }
    
    func testMakePayment_WhenDidTapInstallMents_ShouldLogLogDidTapPaymentAndWentToInstallmentsAsTrue() {
        let sut = viewModel()
        sut.didTapInstallments()
        
        sut.makePayment(password: "123", biometry: true)
        
        XCTAssertEqual(actionsSpy.callDidTapPaymentCount, 1)
        XCTAssertEqual(actionsSpy.wentToInstallments, true)
    }
    
    func testMakePayment_WhenDidNotCompletionPaymentMethods_ShouldLogDidTapPaymentAndWentToInstallmentsAsFalse() {
        let sut = viewModel()
        
        sut.makePayment(password: "123", biometry: true)
        
        XCTAssertEqual(actionsSpy.callDidTapPaymentCount, 1)
        XCTAssertEqual(actionsSpy.wentToCards, false)
    }
    
    func testMakePayment_WhenDidCompletionPaymentMethods_ShouldLogDidTapPaymentAndWentToInstallmentsAsTrue() {
        let sut = viewModel()
        
        sut.completionPaymentMethods()
        sut.makePayment(password: "123", biometry: true)
        
        XCTAssertEqual(actionsSpy.callDidTapPaymentCount, 1)
        XCTAssertEqual(actionsSpy.wentToCards, true)
    }
    
    func testChangeHeaderValueShouldCallHeaderHasUpdated() {
        let sut = viewModel()
        
        sut.changeHeaderValue(100)
        
        XCTAssertEqual(mockPresenter.callHeaderHasUpdatedCount, 1)
        XCTAssertEqual(mockPresenter.headerValue, 100)
        XCTAssertEqual(mockPresenter.headerInstallment, 1)
    }
    
    func testChangeHeaderInstallmentShouldCallHeaderHasUpdated() {
        let sut = viewModel()

        sut.changeHeaderInstallment(3, forceCreditCard: false)
        
        XCTAssertEqual(mockPresenter.callHeaderHasUpdatedCount, 1)
        XCTAssertEqual(mockPresenter.headerInstallment, 3)
    }
    
    func testChangeMessageShouldUpdateMessage() {
        let mockPaymentService = MockPaymentService()
        let sut = viewModel(payment: mockPaymentService)
        
        sut.changeMessage("Te paguei :)")
        sut.changeHeaderValue(100)
        sut.makePayment(password: "123", biometry: true)
        
        XCTAssertEqual(mockPaymentService.transactionPayment?.message, "Te paguei :)")
    }
    
    func testChangeAccessoryValueShouldCallAccessoryHasUpdated() {
        let sut = viewModel()

        sut.changeAccessoryValue(200)
        
        XCTAssertEqual(mockPresenter.callAccessoryHasUpdatedCount, 1)
        XCTAssertEqual(mockPresenter.accessoryValue, 200)
    }
    
    func testChangePrivacyShouldUpdatePrivacy() {
        let mockPaymentService = MockPaymentService()
        let sut = viewModel(payment: mockPaymentService)

        sut.changePrivacy(.private)
        sut.changeHeaderValue(100)
        sut.makePayment(password: "123", biometry: true)
        
        XCTAssertEqual(mockPaymentService.transactionPayment?.privacyConfig, .private)
    }
    
    func testDidTapClose_ShouldCallDidNextStepClose() throws {
        let sut = viewModel()

        sut.didTapClose()
        
        XCTAssertEqual(mockPresenter.callDidNextStepCount, 1)
        XCTAssertEqual(actionsSpy.callPaymentCancelCount, 1)
        
        let stepAction = try XCTUnwrap(mockPresenter.stepAction)
        switch stepAction {
        case .close:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testLimitExceededCloseShouldPresenterCallLimitExceeded() {
        let sut = viewModel()

        sut.limitExceededClose()
        
        XCTAssertEqual(mockPresenter.callLimitExceededCloseCount, 1)
    }
    
    func testLimitExceededPayShouldPresenterCallLimitExceeded() {
        let mockPaymentService = MockPaymentService()
        let sut = viewModel(payment: mockPaymentService)

        sut.limitExceededPay(surcharge: 20)
        
        XCTAssertEqual(mockPaymentService.transactionPayment?.surcharge, 20)
    }
    
    func testLimitExceededSendNotificationShouldCallSerivcePicPayPro() throws {
        let mockService = MockContainerPaymentService()
        let sut = viewModel(service: mockService)
        let dictionary = MockJSON().load(resource: "limitExceeded")
        let picpayError = PicPayError(apiJSON: dictionary.dictionaryObject! as NSDictionary)
        let limitError = try XCTUnwrap(LimitExceededError(error: picpayError))
        
        sut.limitExceededSendNotification(error: limitError)
        
        XCTAssertTrue(mockService.calledSendNotificationPicPayPro)
    }
    
    func testMakePayment_ShouldCallDidTapPayment() {
        let mockPaymentService = MockPaymentService()
        let sut = viewModel(payment: mockPaymentService)

        sut.makePayment(password: "1234", biometry: false)
        
        XCTAssertEqual(actionsSpy.callDidTapPaymentCount, 1)
        XCTAssertEqual(actionsSpy.callPaymentSuccessCount, 0)
        XCTAssertEqual(actionsSpy.callPaymentFailuresCount, 0)
    }
    
    func testMakePaymentShouldCallDidReceiveAnErrorErrorUnfilledValue() {
        let mockPaymentService = MockPaymentService()
        let sut = viewModel(payment: mockPaymentService)

        sut.changeHeaderValue(0.0)
        sut.makePayment(password: "1234", biometry: false)
        
        XCTAssertEqual(mockPresenter.callDidReceiveAnErrorCount, 1)
        XCTAssertEqual(mockPresenter.picpayError?.message, PaymentLocalizable.errorValue.text)
        XCTAssertEqual(actionsSpy.callPaymentSuccessCount, 0)
        XCTAssertEqual(actionsSpy.callPaymentFailuresCount, 0)
    }
    
    func testMakePayment_WhenCardPicPay_NotCallDidNextStepOpenInsertCvv() throws {
        let mockService = MockContainerPaymentService(cvv: nil, flag: .picpay, defaultCard: "123", cardMoney: 100)
        let mockPaymentService = MockPaymentService()
        let sut = viewModel(service: mockService, payment: mockPaymentService)

        sut.changeHeaderValue(100.0)
        sut.makePayment(password: "1234", biometry: false)
        
        XCTAssertEqual(mockPresenter.callDidNextStepCount, 0)
        XCTAssertEqual(actionsSpy.callPaymentSuccessCount, 1)
    }
    
    func testMakePayment_ShouldCallDidNextStepOpenInsertCvv() throws {
        let mockService = MockContainerPaymentService(cvv: nil, defaultCard: "123", cardMoney: 100)
        let mockPaymentService = MockPaymentService()
        let sut = viewModel(service: mockService, payment: mockPaymentService)

        sut.changeHeaderValue(100.0)
        sut.makePayment(password: "1234", biometry: false)
        
        XCTAssertEqual(mockPresenter.callDidNextStepCount, 1)
        XCTAssertEqual(actionsSpy.callPaymentSuccessCount, 0)
        XCTAssertEqual(actionsSpy.callPaymentFailuresCount, 0)
        switch try XCTUnwrap(mockPresenter.stepAction) {
        case .openInsertCvv:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testMakePaymentShouldNotCallOpenInsertCvvNotNil() {
        let mockService = MockContainerPaymentService(cvv: "1234", defaultCard: "123", cardMoney: 100)
        let mockPaymentService = MockPaymentService()
        let sut = viewModel(service: mockService, payment: mockPaymentService)

        sut.changeHeaderValue(100.0)
        sut.makePayment(password: "1234", biometry: false)
        
        XCTAssertTrue(mockPaymentService.calledCreateTransaction)
        XCTAssertEqual(mockPresenter.callDidNextStepCount, 0)
        XCTAssertEqual(actionsSpy.callPaymentSuccessCount, 1)
        XCTAssertEqual(actionsSpy.callPaymentFailuresCount, 0)
    }
    
    func testMakePaymentShouldNotCallOpenInsertCvvCardZero() {
        let mockService = MockContainerPaymentService(cvv: nil, defaultCard: "123", cardMoney: 0)
        let mockPaymentService = MockPaymentService()
        let sut = viewModel(service: mockService, payment: mockPaymentService)

        sut.changeHeaderValue(100.0)
        sut.makePayment(password: "1234", biometry: false)
        
        XCTAssertTrue(mockPaymentService.calledCreateTransaction)
        XCTAssertEqual(mockPresenter.callDidNextStepCount, 0)
        XCTAssertEqual(actionsSpy.callPaymentSuccessCount, 1)
        XCTAssertEqual(actionsSpy.callPaymentFailuresCount, 0)
    }
    
    func testMakePaymentShouldCallPaymentSuccess() {
        let mockService = MockContainerPaymentService(cvv: "1234", defaultCard: "123", cardMoney: 10)
        let mockPaymentService = MockPaymentService(paymentSuccess: true)
        let sut = viewModel(service: mockService, payment: mockPaymentService)

        sut.changeHeaderValue(100.0)
        sut.makePayment(password: "1234", biometry: false)
        
        XCTAssertEqual(mockPresenter.callPaymentSuccessCount, 1)
        XCTAssertEqual(actionsSpy.callPaymentSuccessCount, 1)
        XCTAssertEqual(actionsSpy.callPaymentFailuresCount, 0)
    }
    
    func testMakePaymentShouldCallErrorDidNextStepOpenIdentityChallenge() throws {
        let mockService = MockContainerPaymentService(cvv: "1234", defaultCard: "123", cardMoney: 10)
        let mockPaymentService = MockPaymentService(paymentSuccess: false, errorType: .identityChallenge)
        let sut = viewModel(service: mockService, payment: mockPaymentService)

        sut.changeHeaderValue(100.0)
        sut.makePayment(password: "1234", biometry: false)
        
        XCTAssertEqual(actionsSpy.callPaymentSuccessCount, 0)
        XCTAssertEqual(actionsSpy.callPaymentFailuresCount, 1)
        XCTAssertEqual(mockPresenter.callDidNextStepCount, 1)
        
        let stepAction = try XCTUnwrap(mockPresenter.stepAction)
        switch stepAction {
        case .openIdentityChallenge:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testMakePaymentShouldCallErrorOpenCardChallenge() throws {
        let mockService = MockContainerPaymentService(cvv: "1234", defaultCard: "123", cardMoney: 10)
        let mockPaymentService = MockPaymentService(paymentSuccess: false, errorType: .cardChalleng)
        let sut = viewModel(service: mockService, payment: mockPaymentService)

        sut.changeHeaderValue(100.0)
        sut.makePayment(password: "1234", biometry: false)
        
        XCTAssertEqual(actionsSpy.callPaymentSuccessCount, 0)
        XCTAssertEqual(actionsSpy.callPaymentFailuresCount, 1)
        XCTAssertEqual(mockPresenter.callDidNextStepCount, 1)
        
        let stepAction = try XCTUnwrap(mockPresenter.stepAction)
        switch stepAction {
        case .openCardChallenge(let cardId):
            XCTAssertNotNil(cardId)
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testMakePaymentShouldCallErrorIncorrectPassword() {
        let mockService = MockContainerPaymentService(cvv: "1234", defaultCard: "123", cardMoney: 10)
        let mockPaymentService = MockPaymentService(paymentSuccess: false, errorType: .incorrectPassword)
        let sut = viewModel(service: mockService, payment: mockPaymentService)

        sut.changeHeaderValue(100.0)
        sut.makePayment(password: "1234", biometry: false)
        
        XCTAssertEqual(mockPresenter.callIncorrectPasswordCount, 1)
        XCTAssertEqual(actionsSpy.callPaymentSuccessCount, 0)
        XCTAssertEqual(actionsSpy.callPaymentFailuresCount, 1)
    }
    
    func testMakePaymentShouldCallErrorLimitExceeded() {
        let mockService = MockContainerPaymentService(cvv: "1234", defaultCard: "123", cardMoney: 10)
        let mockPaymentService = MockPaymentService(paymentSuccess: false, errorType: .typeLimitExceeded)
        let sut = viewModel(service: mockService, payment: mockPaymentService)

        sut.changeHeaderValue(100.0)
        sut.makePayment(password: "1234", biometry: false)
        
        XCTAssertEqual(mockPresenter.callLimitExceededCount, 1)
        XCTAssertEqual(actionsSpy.callPaymentSuccessCount, 0)
        XCTAssertEqual(actionsSpy.callPaymentFailuresCount, 1)
    }
    
    func testMakePaymentShouldCallDidReceiveAnError() {
        let mockService = MockContainerPaymentService(cvv: "1234", defaultCard: "123", cardMoney: 10)
        let mockPaymentService = MockPaymentService(paymentSuccess: false, errorType: .defaultError)
        let sut = viewModel(service: mockService, payment: mockPaymentService)

        sut.changeHeaderValue(100.0)
        sut.makePayment(password: "1234", biometry: false)
        
        XCTAssertEqual(mockPresenter.callDidReceiveAnErrorCount, 1)
        XCTAssertEqual(actionsSpy.callPaymentSuccessCount, 0)
        XCTAssertEqual(actionsSpy.callPaymentFailuresCount, 1)
    }
    
    func testMakePaymentShouldCallDidNextStepShow3DSWarning() throws {
        let mockService = MockContainerPaymentService(cvv: "1234", first3DS: true, defaultCard: "123", cardMoney: 10)
        let mockPaymentService = MockPaymentService(paymentSuccess: false, errorType: .error3DS)
        let sut = viewModel(service: mockService, payment: mockPaymentService)

        sut.changeHeaderValue(100.0)
        sut.makePayment(password: "1234", biometry: false)
        
        XCTAssertEqual(mockPresenter.callDidNextStepCount, 1)
        XCTAssertEqual(actionsSpy.callPaymentSuccessCount, 0)
        XCTAssertEqual(actionsSpy.callPaymentFailuresCount, 1)
        let stepAction = try XCTUnwrap(mockPresenter.stepAction)
        switch stepAction {
        case .show3DSWarning:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testMakePaymentShouldCall3DSPaymentSuccess() {
        let mockService = MockContainerPaymentService(cvv: "1234", first3DS: false, defaultCard: "123", cardMoney: 10, success3DS: true)
        let mockPaymentService = MockPaymentService(paymentSuccess: false, errorType: .error3DS)
        let sut = viewModel(service: mockService, payment: mockPaymentService)

        sut.changeHeaderValue(100.0)
        sut.makePayment(password: "1234", biometry: false)
        
        XCTAssertEqual(mockPresenter.callPayment3DSSuccessCount, 1)
        XCTAssertEqual(actionsSpy.callPaymentFailuresCount, 1)
        XCTAssertEqual(actionsSpy.callPaymentSuccessCount, 1)
    }
    
    func testMakePaymentShouldCall3DSDidReceiveAnError() {
        let mockService = MockContainerPaymentService(cvv: "1234", first3DS: false, defaultCard: "123", cardMoney: 10, success3DS: false)
        let mockPaymentService = MockPaymentService(paymentSuccess: false, errorType: .error3DS)
        let sut = viewModel(service: mockService, payment: mockPaymentService)

        sut.changeHeaderValue(100.0)
        sut.makePayment(password: "1234", biometry: false)
        
        XCTAssertEqual(mockPresenter.callDidReceiveAnErrorCount, 1)
        XCTAssertEqual(actionsSpy.callPaymentSuccessCount, 0)
        XCTAssertEqual(actionsSpy.callPaymentFailuresCount, 2)
    }
}
