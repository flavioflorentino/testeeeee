import CorePayment
import XCTest
@testable import PicPay

final class MockContainerPaymentViewController: ContainerPaymentDisplay {
    private(set) var callDisplayRecoverPasswordCount = 0
    private(set) var callBecomeResponderCount = 0
    private(set) var callDisplayLimitExceededCount = 0
    private(set) var callDisplayTryAgainCount = 0
    private(set) var callUpdateHeaderCount = 0
    private(set) var callUpdateToolbarCount = 0
    private(set) var callDidReceiveAnErrorCount = 0
    private(set) var callStopLoadCount = 0
    private(set) var callUpdatePayToolbarCount = 0
    private(set) var callUpdateAccessoryCount = 0
    private(set) var callDisplayPasswordConfirmationCount = 0
    
    private(set)var toolbarValue: Double?
    private(set)var toolbarInstallment: Int?
    private(set)var toolbarForceCreditCard: Bool?
    
    private(set) var headerValue: Double?
    
    private(set) var picpayError: PicPayError?
    private(set) var paymentMethod: PaymentMethod?
    
    func displayRecoverPassword() {
        callDisplayRecoverPasswordCount += 1
    }
    
    func becomeResponder() {
        callBecomeResponderCount += 1
    }
    
    func displayLimitExceeded(model: LimitExceededError) {
        callDisplayLimitExceededCount += 1
    }
    
    func displayTryAgain(withTitle title: String, andMessage message: String) {
        callDisplayTryAgainCount += 1
    }
    
    func updateHeader(value: Double) {
        callUpdateHeaderCount += 1
        headerValue = value
    }
    
    func updateToolbar(value: Double, installment: Int, forceCreditCard: Bool) {
        callUpdateToolbarCount += 1
        toolbarValue = value
        toolbarInstallment = installment
        toolbarForceCreditCard = forceCreditCard
    }
    
    func didReceiveAnError(error: PicPayError) {
        callDidReceiveAnErrorCount += 1
        picpayError = error
    }
    
    func stopLoad() {
        callStopLoadCount += 1
    }
    
    func updateToolbar(paymentMethod: PaymentMethod) {
        callUpdatePayToolbarCount += 1
        self.paymentMethod = paymentMethod
    }
    
    func updateAccessory(paymentMethod: PaymentMethod) {
        callUpdateAccessoryCount += 1
        self.paymentMethod = paymentMethod
    }
    
    func updateAccessory(with values: AccessoryValue) {
        callUpdateAccessoryCount += 1
    }
    
    func displayPasswordConfirmation() {
        callDisplayPasswordConfirmationCount += 1
    }
}

final class MockContainerPaymentCoordinator: ContainerPaymentCoordinating {
    typealias Success = ReceiptWidgetViewModel
    var viewController: UIViewController?
    
    private(set) var calledPaymentSuccess = 0
    private(set) var calledPerform = 0
    
    private(set) var performAction: ContainerPaymentAction?
    private(set) var model: ReceiptWidgetViewModel?
    
    func paymentSuccess(model: ReceiptWidgetViewModel, paymentType: PaymentType) {
        calledPaymentSuccess += 1
        self.model = model
    }
    
    func perform(action: ContainerPaymentAction) {
        calledPerform += 1
        performAction = action
    }
}

final class ContainerPaymentPresenterTest: XCTestCase {
    private let mockViewController = MockContainerPaymentViewController()
    private let mockCoordinator = MockContainerPaymentCoordinator()
    private lazy var presenter: ContainerPaymentPresenter<ReceiptWidgetViewModel, MockContainerPaymentCoordinator> = {
        let presenter = ContainerPaymentPresenter(coordinator: mockCoordinator)
        presenter.viewController = mockViewController
        return presenter
    }()
    
    func testHeaderHasUpdated_ShouldCallUpdateToolbar() {
        presenter.headerHasUpdated(value: 200, installment: 2, forceCreditCard: true)
        
        XCTAssertEqual(mockViewController.callUpdateToolbarCount, 1)
        XCTAssertEqual(mockViewController.toolbarValue, 200)
        XCTAssertEqual(mockViewController.toolbarInstallment, 2)
        XCTAssertEqual(mockViewController.toolbarForceCreditCard, true)
    }
    
    func testAccessoryHasUpdatedShouldCallUpdateHeader() {
        presenter.accessoryHasUpdated(value: 100)
        
        XCTAssertEqual(mockViewController.callUpdateHeaderCount, 1)
        XCTAssertEqual(mockViewController.headerValue, 100)
    }
    
    func testDidReceiveAnError_ShouldCallDidReceiveAnError() {
        presenter.didReceiveAnError(error: PicPayError(message: "BlaBla"))
       
        XCTAssertEqual(mockViewController.callDidReceiveAnErrorCount, 1)
        XCTAssertNotNil(mockViewController.picpayError)
    }
    
    func testIncorrectPassword_ShouldCallDisplayRecoverPassword() {
        presenter.incorrectPassword()
       
        XCTAssertEqual(mockViewController.callDisplayRecoverPasswordCount, 1)
    }
    
    func testPresentTryAgain_ShouldCallViewControllerWithTheRightValues() {
        presenter.presentTryAgain(withTitle: "random title", andMessage: "random message")
        
        XCTAssertEqual(mockViewController.callDisplayTryAgainCount, 1)
    }
    
    func testLimitExceeded_ShouldCallDidReceiveAnError() {
        presenter.limitExceeded(error: PicPayError(message: "BlaBla"))
       
        XCTAssertEqual(mockViewController.callDidReceiveAnErrorCount, 1)
    }
    
    func testLimitExceeded_ShouldCallDisplayLimitExceeded() {
        let dictionary = MockJSON().load(resource: "limitExceeded")
        let picpayError = PicPayError(apiJSON: dictionary.dictionaryObject! as NSDictionary)
        
        presenter.limitExceeded(error: picpayError)
        
        XCTAssertEqual(mockViewController.callDisplayLimitExceededCount, 1)
    }
    
    func testPaymentSuccess_ShouldCallDidNextStepPaymentSuccess() {
        let receipt = ReceiptWidgetViewModel(transactionId: "2", type: .P2P)
        
        presenter.paymentSuccess(model: receipt)
       
        XCTAssertEqual(mockCoordinator.calledPaymentSuccess, 1)
        XCTAssertEqual(mockCoordinator.model?.transactionId, "2")
    }
    
    func testDidNextStep_ShouldCallDidNextStep() {
        presenter.didNextStep(action: .close)
       
        XCTAssertEqual(mockCoordinator.calledPerform, 1)
    }
    
    func testLimitExceededClose_ShouldCallBecomeResponder() {
        presenter.limitExceededClose()
       
        XCTAssertEqual(mockViewController.callBecomeResponderCount, 1)
    }
    
    func testOpenCardChallenge_ShouldCallStopLoad() {
        presenter.didNextStep(action: .openCardChallenge(cardId: "123"))
       
        XCTAssertEqual(mockViewController.callStopLoadCount, 1)
    }
    
    func testOpenIdentityChallenge_ShouldCallStopLoad() {
        presenter.didNextStep(action: .openIdentityChallenge)
       
        XCTAssertEqual(mockViewController.callStopLoadCount, 1)
    }
    
    func testPresentPasswordConfirmation_ShouldCallDisplayPasswordConfirmation() {
        presenter.presentPasswordConfirmation()
       
        XCTAssertEqual(mockViewController.callDisplayPasswordConfirmationCount, 1)
    }
    
    func testPayment3DSSuccess_ShouldCallActionPayment3DSSuccess() {
        let receipt = ReceiptWidgetViewModel(transactionId: "2", type: .P2P)
        
        presenter.payment3DSSuccess(model: receipt)
       
        XCTAssertEqual(mockCoordinator.calledPerform, 1)
        switch mockCoordinator.performAction {
        case let .payment3DSSuccess(model):
            XCTAssertEqual(model.transactionId, "2")
        default:
            XCTFail("Not Call payment3DSSuccess")
        }
    }
    
    func testUpdatePaymentMethod_WithPaymentMethodBalanceAndCard_ShouldCallUpdateToolbar() {
        presenter.updatePaymentMethod(.balanceAndCard)
       
        XCTAssertEqual(mockViewController.callUpdatePayToolbarCount, 1)
        XCTAssertEqual(mockViewController.paymentMethod, .balanceAndCard)
    }
    
    func testUpdatePaymentMethod_WithPaymentMethodBalanceAndCard_ShouldCallUpdateAccessory() {
        presenter.updatePaymentMethod(.balanceAndCard)
       
        XCTAssertEqual(mockViewController.callUpdateAccessoryCount, 1)
        XCTAssertEqual(mockViewController.paymentMethod, .balanceAndCard)
    }
}
