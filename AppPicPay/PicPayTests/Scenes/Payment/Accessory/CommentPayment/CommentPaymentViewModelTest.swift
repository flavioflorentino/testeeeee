import XCTest
@testable import PicPay

class MockCommentPaymentPresenter: CommentPaymentPresenting {
    var viewController: CommentPaymentDisplay?
    private(set) var calledDisplayComment = false
    private(set) var calledInputValue = false
    private(set) var calledDidBeginEditing = false
    private(set) var calledDidEndEditing = false
    
    private(set) var messageInput: String?
    
    func displayComment() {
        calledDisplayComment = true
    }
    
    func inputValue(message: String) {
        calledInputValue = true
        messageInput = message
    }
    
    func didBeginEditing() {
        calledDidBeginEditing = true
    }
    
    func didEndEditing() {
        calledDidEndEditing = true
    }
}

class CommentPaymentViewModelTest: XCTestCase {
    func testViewDidLoadShouldCallDisplayComment() {
        let mockPresenter = MockCommentPaymentPresenter()
        let viewModel = CommentPaymentViewModel(presenter: mockPresenter)
        
        viewModel.viewDidLoad()
        
        XCTAssertTrue(mockPresenter.calledDisplayComment)
    }
    
    func testInputValueShouldCallInputValueWithoutChangingMessage() {
        let mockPresenter = MockCommentPaymentPresenter()
        let viewModel = CommentPaymentViewModel(presenter: mockPresenter)
        
        viewModel.inputValue(message: "Test")
        
        XCTAssertTrue(mockPresenter.calledInputValue)
        XCTAssertEqual(mockPresenter.messageInput, "Test")
    }
    
    func testDidBeginEditingShouldCallDidBeginEditing() {
        let mockPresenter = MockCommentPaymentPresenter()
        let viewModel = CommentPaymentViewModel(presenter: mockPresenter)
        
        viewModel.didBeginEditing()
        
        XCTAssertTrue(mockPresenter.calledDidBeginEditing)
    }
    
    func testDidEndEditingShouldCallDidEndEditing() {
        let mockPresenter = MockCommentPaymentPresenter()
        let viewModel = CommentPaymentViewModel(presenter: mockPresenter)
        
        viewModel.didEndEditing()
        
        XCTAssertTrue(mockPresenter.calledDidEndEditing)
    }
}
