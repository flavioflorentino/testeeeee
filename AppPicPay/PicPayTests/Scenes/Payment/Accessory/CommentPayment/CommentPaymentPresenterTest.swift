import XCTest
@testable import UI
@testable import PicPay

class MockCommentPaymentViewController: CommentPaymentDisplay {
    private(set) var calledUpdateTextView = false
    private(set) var calledUpdatePaceholder = false
    private(set) var calledUpdateColor = false
    private(set) var calledDeleteBackward = false

    private(set) var textViewMessage: String?
    private(set) var placeholderMessage: String?
    private(set) var color: UIColor?
    
    func updateTextView(message: String) {
        calledUpdateTextView = true
        textViewMessage = message
    }
    
    func updatePaceholder(message: String) {
        calledUpdatePaceholder =  true
        placeholderMessage = message
    }
    
    func updateColor(color: UIColor) {
        calledUpdateColor = true
        self.color = color
    }
    
    func deleteBackward() {
        calledDeleteBackward = true
    }
}

final class CommentPaymentPresenterTest: XCTestCase {

    private let mockViewController = MockCommentPaymentViewController()

    private let fakePlaceholder = "Placeholder"

    private lazy var presenter: CommentPaymentPresenter = {
        let pres = CommentPaymentPresenter(commentPlaceholder: fakePlaceholder)
        pres.viewController = mockViewController
        return pres
    }()

    func testDisplayCommentShouldCallUpdateTextView() {
        presenter.displayComment()
        
        XCTAssertTrue(mockViewController.calledUpdateTextView)
        XCTAssertEqual(mockViewController.textViewMessage, String())
    }
    
    func testDisplayCommentShouldCallUpdatePaceholder() {
        presenter.displayComment()
        
        XCTAssertTrue(mockViewController.calledUpdatePaceholder)
        XCTAssertEqual(mockViewController.placeholderMessage, fakePlaceholder)
    }
    
    func testDisplayCommentShouldCallUpdateColor() {
        presenter.displayComment()
        
        XCTAssertTrue(mockViewController.calledUpdateColor)
        XCTAssertEqual(mockViewController.color?.cgColor, Palette.ppColorGrayscale300.cgColor)
    }
    
    func testInputValueShouldCallDeleteLastCharacter() {
        presenter.inputValue(message: "Texto com 100 caracteres Lorem ipsum praesent id porta nulla primis sollicitudin, adipiscing posueres")
        
        XCTAssertTrue(mockViewController.calledDeleteBackward)
    }
    
    func testInputValueShouldCallUpdateTextView() {
        presenter.inputValue(message: "Texto de pagamento")
        
        XCTAssertTrue(mockViewController.calledUpdateTextView)
        XCTAssertEqual(mockViewController.textViewMessage, "Texto de pagamento")
    }
    
    func testDidBeginEditingShouldCallUpdatePaceholder() {
        presenter.inputValue(message: "")
        
        presenter.didBeginEditing()
        
        XCTAssertTrue(mockViewController.calledUpdatePaceholder)
        XCTAssertEqual(mockViewController.placeholderMessage, String())
    }
    
    func testDidBeginEditingShouldCallUpdateColor() {
        presenter.inputValue(message: "")
        
        presenter.didBeginEditing()
        
        XCTAssertTrue(mockViewController.calledUpdateColor)
        XCTAssertEqual(mockViewController.color?.cgColor, Palette.ppColorGrayscale500.cgColor)
    }
    
    func testDidBeginEditingNotShouldCallUpdatePaceholderAndUpdateColor() {
        presenter.inputValue(message: "Tem texto")
        
        presenter.didBeginEditing()
        
        XCTAssertFalse(mockViewController.calledUpdatePaceholder)
        XCTAssertFalse(mockViewController.calledUpdateColor)
    }
    
    func testDidEndEditingShouldCallUpdateTextView() {
        presenter.inputValue(message: "")
        
        presenter.didEndEditing()
        
        XCTAssertTrue(mockViewController.calledUpdateTextView)
        XCTAssertEqual(mockViewController.textViewMessage, String())
    }
    
    func testDidEndEditingShouldCallUpdatePaceholder() {
        presenter.inputValue(message: "")
        
        presenter.didEndEditing()
        
        XCTAssertTrue(mockViewController.calledUpdatePaceholder)
        XCTAssertEqual(mockViewController.placeholderMessage, fakePlaceholder)
    }
    
    func testDidEndEditingShouldCallUpdateColor() {
        presenter.inputValue(message: "")
        
        presenter.didEndEditing()
        
        XCTAssertTrue(mockViewController.calledUpdateColor)
        XCTAssertEqual(mockViewController.color?.cgColor, Palette.ppColorGrayscale300.cgColor)
    }
    
    func testDidEndEditingNotShouldCallUpdateColorAndUpdatePaceholder() {
        presenter.inputValue(message: "Tem Texto")
        
        presenter.didEndEditing()
        
        XCTAssertFalse(mockViewController.calledUpdatePaceholder)
        XCTAssertFalse(mockViewController.calledUpdateColor)
    }
}
