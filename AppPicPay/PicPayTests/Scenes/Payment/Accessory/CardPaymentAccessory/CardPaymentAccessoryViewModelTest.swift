import XCTest
@testable import PicPay

private final class CardPaymentAccessoryPresentingSpy: CardPaymentAccessoryPresenting {
    var viewController: CardPaymentAccessoryDisplay?
    
    private(set) var calledPresentView = 0
    private(set) var calledPresentAlertTax = 0
    private(set) var calledShowTax = 0
    private(set) var calledHiddenTax = 0
    
    private(set) var total: Double?
    private(set) var tax: Double?
    private(set) var disclaimer: String?
    
    func presentView(total: Double, tax: Double, disclaimer: String?) {
        calledPresentView += 1
        self.total = total
        self.tax = tax
        self.disclaimer = disclaimer
    }
    
    func presentAlertTax(_ tax: Double) {
        calledPresentAlertTax += 1
        self.tax = tax
    }
    
    func showTax() {
        calledShowTax += 1
    }
    
    func hiddenTax() {
        calledHiddenTax += 1
    }
}

private final class CardPaymentAccessoryViewModelTest: XCTestCase {
    private let presenterSpy = CardPaymentAccessoryPresentingSpy()
    private let model = CardPaymentAccessory(total: 100, tax: 2.99, disclaimer: "disclaimer")
    private lazy var sut = CardPaymentAccessoryViewModel(model: model, presenter: presenterSpy)
    
    func testUpdateView_WhenCalled_ShouldCallPresentViewWithModelValue() {
        sut.updateView()
        
        XCTAssertEqual(presenterSpy.calledPresentView, 1)
        XCTAssertEqual(presenterSpy.total, model.total)
        XCTAssertEqual(presenterSpy.tax, model.tax)
        XCTAssertEqual(presenterSpy.disclaimer, model.disclaimer)
    }
    
    func testChangePaymentMethod_WhenMethodCard_ShouldCallShowTax() {
        sut.changePaymentMethod(method: .card)
        
        XCTAssertEqual(presenterSpy.calledShowTax, 1)
    }
    
    func testChangePaymentMethod_WhenMethodBalanceAndCard_ShouldCallShowTax() {
        sut.changePaymentMethod(method: .balanceAndCard)
        
        XCTAssertEqual(presenterSpy.calledShowTax, 1)
    }
    
    func testChangePaymentMethod_WhenMethodAccountBalance_ShouldCallShowTax() {
        sut.changePaymentMethod(method: .accountBalance)
        
        XCTAssertEqual(presenterSpy.calledHiddenTax, 1)
    }
    
    func testTapTax_WhenCalled_ShouldCallPresentAlertTaxWithValue() {
        sut.tapTax()
        
        XCTAssertEqual(presenterSpy.calledPresentAlertTax, 1)
        XCTAssertEqual(presenterSpy.tax, model.tax)
    }
}
