import XCTest
@testable import PicPay

private final class CardPaymentAccessoryDisplaySpy: CardPaymentAccessoryDisplay {
    var viewController: CardPaymentAccessoryDisplay?
    
    private(set) var calledDisplayTotalValue = 0
    private(set) var calledDisplayTaxValue = 0
    private(set) var calledDisplayDisclaimer = 0
    private(set) var calledTaxValueIsHidden = 0
    private(set) var calledDisclaimerViewIsHidden = 0
    private(set) var calledDisplayAlert = 0
    
    private(set) var isHiddenTax: Bool?
    private(set) var isHiddenDisclaimer: Bool?
    private(set) var textDisclaimer: String?
    private(set) var totalLeftText: String?
    private(set) var totalRightText: String?
    private(set) var taxLeftText: String?
    private(set) var taxRightText: String?
    private(set) var alert: Alert?
    
    func displayTotalValue(leftText: String, rightText: String){
        calledDisplayTotalValue += 1
        totalLeftText = leftText
        totalRightText = rightText
    }
    
    func displayTaxValue(leftText: String, rightText: String) {
        calledDisplayTaxValue += 1
        taxLeftText = leftText
        taxRightText = rightText
    }
    
    func displayDisclaimer(text: String) {
        calledDisplayDisclaimer += 1
        textDisclaimer = text
    }
    
    func taxValueIsHidden(_ value: Bool) {
        calledTaxValueIsHidden += 1
        isHiddenTax = value
    }
    
    func disclaimerViewIsHidden(_ value: Bool) {
        calledDisclaimerViewIsHidden += 1
        isHiddenDisclaimer = value
    }
    
    func displayAlert(_ alert: Alert) {
        calledDisplayAlert += 1
        self.alert = alert
    }
}

final class CardPaymentAccessoryPresenterTest: XCTestCase {
    private let viewControllerSpy = CardPaymentAccessoryDisplaySpy()
    
    private lazy var sut: CardPaymentAccessoryPresenter = {
        let presenter = CardPaymentAccessoryPresenter()
        presenter.viewController = viewControllerSpy
        
        return presenter
    }()
    
    func testPresentView_WhenCalled_ShouldCallDisplayTotalValueWithValueFalse() {
        let total = 100.0
        sut.presentView(total: total, tax: 2.99, disclaimer: "disclaimer")
        
        XCTAssertEqual(viewControllerSpy.calledDisplayTotalValue, 1)
        XCTAssertEqual(viewControllerSpy.totalLeftText, PaymentLocalizable.billTotal.text)
        XCTAssertEqual(viewControllerSpy.totalRightText, total.stringAmount)
    }
    
    func testPresentView_WhenCalled_ShouldCallDisplayTaxValueValueWithValueFalse() {
        let tax = 2.99
        sut.presentView(total: 100.0, tax: tax, disclaimer: "disclaimer")
        
        XCTAssertEqual(viewControllerSpy.calledDisplayTaxValue, 1)
        XCTAssertEqual(viewControllerSpy.taxLeftText, PaymentLocalizable.inconvenienceTax.text)
        XCTAssertEqual(viewControllerSpy.taxRightText, String(tax))
    }
    
    func testPresentView_WhenDisclaimerNil_NotShouldCallDisplayDisclaimer() {
        sut.presentView(total: 100.0, tax: 2.99, disclaimer: nil)
        
        XCTAssertEqual(viewControllerSpy.calledDisplayDisclaimer, 0)
        XCTAssertEqual(viewControllerSpy.calledDisclaimerViewIsHidden, 1)
        XCTAssertEqual(viewControllerSpy.isHiddenDisclaimer, true)
    }
    
    func testPresentView_WhenDisclaimerNotNil_ShouldCallDisplayDisclaimer() {
        sut.presentView(total: 100.0, tax: 2.99, disclaimer: "disclaimer")
        
        XCTAssertEqual(viewControllerSpy.calledDisplayDisclaimer, 1)
        XCTAssertEqual(viewControllerSpy.calledDisclaimerViewIsHidden, 1)
        XCTAssertEqual(viewControllerSpy.isHiddenDisclaimer, false)
        XCTAssertEqual(viewControllerSpy.textDisclaimer, "disclaimer")
    }
    
    func testShowTax_WhenCalled_ShouldCallTaxValueIsHiddenWithValueFalse() {
        sut.showTax()
        
        XCTAssertEqual(viewControllerSpy.calledTaxValueIsHidden, 1)
        XCTAssertEqual(viewControllerSpy.isHiddenTax, false)
    }
    
    func testHiddenTax_WhenCalled_ShouldCallTaxValueIsHiddenWithValueTrue() {
        sut.hiddenTax()
        
        XCTAssertEqual(viewControllerSpy.calledTaxValueIsHidden, 1)
        XCTAssertEqual(viewControllerSpy.isHiddenTax, true)
    }
    
    func testPresentAlertTax_WhenCalled_ShouldCallDisplayAlertWithValueTax() {
        let tax = 2.99
        let text = PaymentLocalizable.alertConvenienceTaxSubtitle.text.replacingOccurrences(of: "$tax", with: String(tax))
        sut.presentAlertTax(tax)
        
        XCTAssertEqual(viewControllerSpy.calledDisplayAlert, 1)
        XCTAssertEqual(viewControllerSpy.alert?.text?.string, text)
    }
}
