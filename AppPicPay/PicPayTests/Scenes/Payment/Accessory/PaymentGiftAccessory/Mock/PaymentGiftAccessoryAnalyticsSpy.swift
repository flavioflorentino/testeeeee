@testable import PicPay

final class PaymentGiftAccessoryAnalyticsSpy: PaymentGiftAcessoryAnalytics {
    private(set) var callDidSelectGiftCount = 0
    private(set) var gift: PaymentGift?
    
    func didSelectGift(_ gift: PaymentGift) {
        callDidSelectGiftCount += 1
        self.gift = gift
    }
}
