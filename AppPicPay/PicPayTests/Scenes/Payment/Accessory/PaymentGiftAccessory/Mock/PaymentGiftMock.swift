import XCTest
@testable import PicPay

extension PaymentGift {
    static var birthdayNoSelectionMock: [PaymentGift] {
        [
            PaymentGift(id: "1", imageGift: .emoji("🍺"), description: "descr2", value: 5.0, isDefaultSelection: false),
            PaymentGift(id: "2", imageGift: .emoji("🍦"), description: "descr1", value: 2.0, isDefaultSelection: false),
            PaymentGift(id: "3", imageGift: .emoji("🍰"), description: "descr3", value: 15.0, isDefaultSelection: false)
        ]
    }
    
    static var birthdayWithSelectionMock: [PaymentGift] {
        [
            PaymentGift(id: "1", imageGift: .emoji("🍺"), description: "descr2", value: 5.0, isDefaultSelection: false),
            PaymentGift(id: "2", imageGift: .emoji("🍦"), description: "descr1", value: 2.0, isDefaultSelection: false),
            PaymentGift(id: "3", imageGift: .emoji("🍰"), description: "descr3", value: 15.0, isDefaultSelection: true)
        ]
    }
    
    static func newFriendWithSelectionMock() throws -> [PaymentGift] {
        let url1 = try XCTUnwrap(URL(string: "https://s3.amazonaws.com/cdn.picpay.com/apps/icons/gift_ice_cream.png"))
        let url2 = try XCTUnwrap(URL(string: "https://s3.amazonaws.com/cdn.picpay.com/apps/icons/gift_beer.png"))
        let url3 = try XCTUnwrap(URL(string: "https://s3.amazonaws.com/cdn.picpay.com/apps/icons/gift_shopping_bags.png"))
        
        return [
            PaymentGift(id: "1", imageGift: .url(url1), description: "descr1", value: 2.0, isDefaultSelection: false),
            PaymentGift(id: "2", imageGift: .url(url2), description: "descr2", value: 5.0, isDefaultSelection: true),
            PaymentGift(id: "3", imageGift: .url(url3), description: "descr3", value: 15.0, isDefaultSelection: false)
        ]
    }
}

extension PaymentGift: Equatable {
    public static func == (lhs: PaymentGift, rhs: PaymentGift) -> Bool {
        switch (lhs.imageGift, rhs.imageGift) {
        case (let .emoji(lhsString), let .emoji(rhsString)):
            if lhsString != rhsString {
                return false
            }
        case (let .url(lhsUrl), let .url(rhsUrl)):
            if lhsUrl != rhsUrl {
                return false
            }
        default:
            return false
        }
        
        return lhs.id == rhs.id &&
            lhs.description == rhs.description &&
            lhs.isDefaultSelection == rhs.isDefaultSelection &&
            lhs.value == rhs.value
    }
}

extension PaymentGiftCell {
    static var birthdayWithSelectionMock: [PaymentGiftCell] {
        [
            PaymentGiftCell(imageGift: .emoji("🍺"), value: 5.0, isActive: false),
            PaymentGiftCell(imageGift: .emoji("🍦"), value: 2.0, isActive: true),
            PaymentGiftCell(imageGift: .emoji("🍰"), value: 15.0, isActive: false)
        ]
    }
    
    static var birthdayNoSelectionMock: [PaymentGiftCell] {
        [
            PaymentGiftCell(imageGift: .emoji("🍺"), value: 5.0, isActive: false),
            PaymentGiftCell(imageGift: .emoji("🍦"), value: 2.0, isActive: false),
            PaymentGiftCell(imageGift: .emoji("🍰"), value: 15.0, isActive: false)
        ]
    }
}

extension PaymentGiftCell: Equatable {
    public static func == (lhs: PaymentGiftCell, rhs: PaymentGiftCell) -> Bool {
        switch (lhs.imageGift, rhs.imageGift) {
        case (let .emoji(lhsString), let .emoji(rhsString)):
            if lhsString != rhsString {
                return false
            }
        case (let .url(lhsUrl), let .url(rhsUrl)):
            if lhsUrl != rhsUrl {
                return false
            }
        default:
            return false
        }
        
        return lhs.isActive == rhs.isActive && lhs.value == rhs.value
    }
}
