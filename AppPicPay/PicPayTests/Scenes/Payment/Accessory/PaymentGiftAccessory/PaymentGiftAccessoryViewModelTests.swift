import XCTest
@testable import PicPay

private final class PaymentGiftAccessoryPresenterSpy: PaymentGiftAccessoryPresenting {
    private(set) var callUpdateListCount = 0
    private(set) var data: [PaymentGift]?
    private(set) var selectedGift: PaymentGift?
    
    weak var viewController: PaymentGiftAccessoryDisplay?
    
    func updateList(data: [PaymentGift], selectedGift: PaymentGift?) {
        self.data = data
        self.selectedGift = selectedGift
        callUpdateListCount += 1
    }
}

final class PaymentGiftAcessoryViewModelTest: XCTestCase {
    private let presenter = PaymentGiftAccessoryPresenterSpy()
    private let analyticsSpy = PaymentGiftAccessoryAnalyticsSpy()
    
    func testUpdateViewWithDefaultSelection_WhenDefaultOptionIsExplicity_ShouldUpdateViewWithDefaultSelection() {
        let model = PaymentGift.birthdayWithSelectionMock
        let sut = PaymentGiftAccessoryViewModel(model: model, presenter: presenter, analytics: analyticsSpy)
        
        sut.updateViewWithDefaultSelection()
        
        XCTAssertEqual(presenter.callUpdateListCount, 1)
        XCTAssertEqual(presenter.data, model)
        let defaultGift = model[2]
        XCTAssertEqual(presenter.selectedGift, defaultGift)
    }
    
    func testUpdateViewWithDefaultSelection_WhenDefaultOptionIsNotInformed_ShouldUpdateViewWithCheapestItemSelection() {
        let model = PaymentGift.birthdayNoSelectionMock
        let sut = PaymentGiftAccessoryViewModel(model: model, presenter: presenter, analytics: analyticsSpy)
        
        sut.updateViewWithDefaultSelection()
        
        XCTAssertEqual(presenter.callUpdateListCount, 1)
        XCTAssertEqual(presenter.data, model)
        let cheapestGift = model[1]
        XCTAssertEqual(presenter.selectedGift, cheapestGift)
        XCTAssertEqual(analyticsSpy.callDidSelectGiftCount, 0)
    }
    
    func testUpdateViewWithDefaultSelection_WhenImageGiftIsUrl_ShouldUpdateViewWithSameValues() throws {
        let model = try PaymentGift.newFriendWithSelectionMock()
        let sut = PaymentGiftAccessoryViewModel(model: model, presenter: presenter, analytics: analyticsSpy)
        
        sut.updateViewWithDefaultSelection()
        
        XCTAssertEqual(presenter.callUpdateListCount, 1)
        XCTAssertEqual(presenter.data, model)
        let cheapestGift = model[1]
        XCTAssertEqual(presenter.selectedGift, cheapestGift)
        XCTAssertEqual(analyticsSpy.callDidSelectGiftCount, 0)
    }
    
    func testDidTapCell_WhenIndexIsValid_ShouldUpdateViewWithSelection() {
        let model = PaymentGift.birthdayNoSelectionMock
        let sut = PaymentGiftAccessoryViewModel(model: model, presenter: presenter, analytics: analyticsSpy)
        
        sut.didTapCell(at: IndexPath(item: 0, section: 0))
        
        XCTAssertEqual(presenter.callUpdateListCount, 1)
        XCTAssertEqual(presenter.data, model)
        let selectedGift = model[0]
        XCTAssertEqual(presenter.selectedGift, selectedGift)
        XCTAssertEqual(analyticsSpy.callDidSelectGiftCount, 1)
        XCTAssertEqual(analyticsSpy.gift, selectedGift)
    }
    
    func testDidTapCell_WhenIndexIsInvalid_ShouldNotUpdateView() {
        let model = PaymentGift.birthdayNoSelectionMock
        let sut = PaymentGiftAccessoryViewModel(model: model, presenter: presenter, analytics: analyticsSpy)
        
        sut.didTapCell(at: IndexPath(item: 3, section: 0))
        
        XCTAssertEqual(presenter.callUpdateListCount, 0)
        XCTAssertEqual(analyticsSpy.callDidSelectGiftCount, 0)
    }
}
