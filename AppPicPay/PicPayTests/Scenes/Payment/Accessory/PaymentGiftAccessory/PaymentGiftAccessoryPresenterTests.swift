import XCTest
@testable import UI
@testable import PicPay

private final class PaymentGiftAccessoryViewControllerSpy: PaymentGiftAccessoryDisplay {
    private(set) var callReloadListCount = 0
    private(set) var model: [PaymentGiftCell]?
    
    private(set) var callUpdateValueCount = 0
    private(set) var value: Double?
    
    private(set) var callUpdateMessageCount = 0
    private(set) var message: String?
    
    
    func reloadList(model: [PaymentGiftCell]) {
        callReloadListCount += 1
        self.model = model
    }
    
    func updateValue(value: Double) {
        callUpdateValueCount += 1
        self.value = value
    }
    
    func updateMessage(message: String) {
        self.message = message
        callUpdateMessageCount += 1
    }
}

final class PaymentGiftAccessoryPresenterTests: XCTestCase {
    private let viewController = PaymentGiftAccessoryViewControllerSpy()
    private lazy var sut: PaymentGiftAccessoryPresenting = {
        let presenter = PaymentGiftAccessoryPresenter()
        presenter.viewController = viewController
        return presenter
    }()
    
    func testUpdateList_WithNilSelectedGift_ShouldOnlyReloadList() {
        sut.updateList(data: PaymentGift.birthdayNoSelectionMock, selectedGift: nil)
        
        XCTAssertEqual(viewController.callReloadListCount, 1)
        XCTAssertEqual(viewController.model, PaymentGiftCell.birthdayNoSelectionMock)
    }
    
    func testUpdateList_WithSelectedGift_ShouldReloadListAndUpdateValueAndMessage() {
        let model = PaymentGift.birthdayNoSelectionMock
        let selectedGift = model[1]
        
        sut.updateList(data: model, selectedGift: selectedGift)
        
        XCTAssertEqual(viewController.callReloadListCount, 1)
        XCTAssertEqual(viewController.model, PaymentGiftCell.birthdayWithSelectionMock)
        XCTAssertEqual(viewController.callUpdateValueCount, 1)
        XCTAssertEqual(viewController.value, selectedGift.value)
        XCTAssertEqual(viewController.callUpdateMessageCount, 1)
        XCTAssertEqual(viewController.message, selectedGift.description)
    }
}
