import XCTest
@testable import PicPay

private final class DGTableViewPaymentPresenterSpy: DGTableViewPaymentPresenting {
    private(set) var didCallUpdateItensPayment = 0
    private(set) var paymentData: [ItensPayment]?
    
    private(set) var didCallDidNextStep = 0
    private(set) var performAction: DGTableViewPaymentAction?
    
    var viewController: DGTableViewPaymentDisplay?
    
    func updateItensPayment(_ data: [ItensPayment]) {
        didCallUpdateItensPayment += 1
        paymentData = data
    }
    
    func didNextStep(action: DGTableViewPaymentAction) {
        didCallDidNextStep += 1
        performAction = action
    }
}

private final class DGTableViewPaymentViewModelTest: XCTestCase {
    private let presenterSpy = DGTableViewPaymentPresenterSpy()
    
    private lazy var stu: DGTableViewPaymentViewModel = {
        let surcharge = SurchargePayment(title: "title", description: "description", surcharge: 10.0, surchargeAmount: 5.0)
        let data: [ItensPayment] = [
            PaymentDetailCell(detail: "detail"),
            PaymentMarkdownCell(markdown: "markdown"),
            PaymentTwoLabelsCell(left: "left", right: "right", buttonIsEnable: true, popup: surcharge)
        ]
        
        return DGTableViewPaymentViewModel(model: data, presenter: presenterSpy)
    }()
    
    func testViewDidLoad_WhenCall_ShouldCallUpdateItensPayment() {
        stu.viewDidLoad()
        
        XCTAssertEqual(presenterSpy.didCallUpdateItensPayment, 1)
        XCTAssertFalse(presenterSpy.paymentData.isEmpty)
    }
    
    func testOpenMarkdownUrl_WhenCall_ShouldCallDidNextStep() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        stu.openMarkdownUrl(url)
        
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 1)
        
        switch try XCTUnwrap(presenterSpy.performAction) {
        case .openWebView(let url):
            XCTAssertNotNil(url)
        default:
            XCTFail("Action Not openWebView")
        }
    }
    
    func testOpenMarkdownUrl_WhenCellPaymentTwoLabelsCell_ShouldCallDidNextStep() throws {
        stu.openInformationButton(index: IndexPath(row: 2, section: 1))
        
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 1)
        
        switch try XCTUnwrap(presenterSpy.performAction) {
        case .openSurchargeConfirmation(let surcharge):
            XCTAssertNotNil(surcharge)
        default:
            XCTFail("Action Not openSurchargeConfirmation")
        }
    }
    
    func testOpenMarkdownUrl_WhenCellPaymentDetailCell_NotShouldCallDidNextStep() {
        stu.openInformationButton(index: IndexPath(row: 0, section: 1))
        
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 0)
        XCTAssertNil(presenterSpy.performAction)
    }
    
    func testOpenMarkdownUrl_WhenCellPaymentMarkdownCell_NotShouldCallDidNextStep() {
        stu.openInformationButton(index: IndexPath(row: 1, section: 1))
        
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 0)
        XCTAssertNil(presenterSpy.performAction)
    }
    
    func testCellChangedHeight_WhenCellPaymentMarkdownCell_ShouldCallUpdateItensPayment() throws {
        stu.cellChangedHeight(index: IndexPath(row: 1, section: 1))
        
        let cell = try XCTUnwrap(presenterSpy.paymentData?[1] as? PaymentMarkdownCell)
        XCTAssertEqual(presenterSpy.didCallUpdateItensPayment, 1)
        XCTAssertFalse(cell.needLoad)
    }
    
    func testCellChangedHeight_WhenCellPaymentDetailCell_NotShouldCallUpdateItensPayment() {
        stu.cellChangedHeight(index: IndexPath(row: 0, section: 1))
        
        XCTAssertEqual(presenterSpy.didCallUpdateItensPayment, 0)
    }
    
    func testCellChangedHeight_WhenCellPaymentTwoLabelsCell_NotShouldCallUpdateItensPayment() {
        stu.cellChangedHeight(index: IndexPath(row: 2, section: 1))
        
        XCTAssertEqual(presenterSpy.didCallUpdateItensPayment, 0)
    }
}
