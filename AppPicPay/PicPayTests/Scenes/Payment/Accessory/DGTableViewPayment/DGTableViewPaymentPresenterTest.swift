import XCTest
@testable import PicPay

private final class DGTableViewPaymentViewControllerSpy: DGTableViewPaymentDisplay {
    private(set) var didCallDisplayItensPayment = 0
    private(set) var displayData: [ItensPayment]?
    
    func displayItensPayment(_ data: [ItensPayment]) {
        didCallDisplayItensPayment += 1
        displayData = data
    }
}

private final class DGTableViewPaymentCoordinatorSpy: DGTableViewPaymentCoordinating {
    var viewController: UIViewController? = nil
    
    private(set) var didCallPerformAction = 0
    private(set) var performAction: DGTableViewPaymentAction?
    
    func perform(action: DGTableViewPaymentAction) {
        didCallPerformAction += 1
        performAction = action
    }
}

private final class DGTableViewPaymentPresenterTest: XCTestCase {
    private let coordinatorSpy = DGTableViewPaymentCoordinatorSpy()
    private let viewControllerSpy = DGTableViewPaymentViewControllerSpy()
    
    private lazy var stu: DGTableViewPaymentPresenter = {
        let stu = DGTableViewPaymentPresenter(coordinator: coordinatorSpy)
        stu.viewController = viewControllerSpy
        
        return stu
    }()
    
    
    func testUpdateItensPayment_WhenDataFull_ShouldCallUpdateItensPayment() {
        let data: [ItensPayment] = [
            PaymentDetailCell(detail: "detail"),
            PaymentMarkdownCell(markdown: "markdown"),
            PaymentTwoLabelsCell(left: "left", right: "right", buttonIsEnable: false, popup: nil)
        ]
        
        stu.updateItensPayment(data)
        
        XCTAssertEqual(viewControllerSpy.didCallDisplayItensPayment, 1)
        XCTAssertEqual(viewControllerSpy.displayData?[0].cellType, .detail)
        XCTAssertEqual(viewControllerSpy.displayData?[1].cellType, .markdown)
        XCTAssertEqual(viewControllerSpy.displayData?[2].cellType, .information)
    }
    
    func testUpdateItensPayment_WhenDataEmpty_ShouldCallUpdateItensPayment() {
        stu.updateItensPayment([])
        
        XCTAssertEqual(viewControllerSpy.didCallDisplayItensPayment, 1)
        XCTAssertTrue(viewControllerSpy.displayData?.isEmpty ?? false)
    }
    
    func testDidNextStep_WhenCallOpenWebView_ShouldCallPerformAction() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        stu.didNextStep(action: .openWebView(url))
        
        XCTAssertEqual(coordinatorSpy.didCallPerformAction, 1)
        switch try XCTUnwrap(coordinatorSpy.performAction) {
        case .openWebView(let url):
            XCTAssertNotNil(url)
        default:
            XCTFail("Action Not openWebView")
        }
    }
    
    func testDidNextStep_WhenCallOpenSurchargeConfirmation_ShouldCallPerformAction() throws {
        let surchargePayment = SurchargePayment(
            title: "title",
            description: "description",
            surcharge: 10.0,
            surchargeAmount: 5.0
        )
        
        stu.didNextStep(action: .openSurchargeConfirmation(surchargePayment))
        
        XCTAssertEqual(coordinatorSpy.didCallPerformAction, 1)
        switch try XCTUnwrap(coordinatorSpy.performAction) {
        case .openSurchargeConfirmation(let surcharge):
            XCTAssertNotNil(surcharge)
        default:
            XCTFail("Action Not openSurchargeConfirmation")
        }
    }
}
