import UI
import UIKit
import XCTest
@testable import PicPay

class FakeBannerFeedItemView: UIView, BannerFeedItemDisplay {
    var title: String?
    var titleColor: UIColor?
    var message: String?
    var messageColor: UIColor?
    var buttonTitle: NSAttributedString?
    var buttonTitleColor: UIColor?
    var imageUrl: URL?

    func display(_ model: DisplayModel) {
        title = model.title
        titleColor = model.titleColor
        message = model.message
        messageColor = model.messageColor
        imageUrl = model.image
        buttonTitle = model.action
        buttonTitleColor = model.actionColor
        backgroundColor = model.backgroundColor
    }
}

class FakeBannerFeedItemCoordinator: BannerFeedItemCoordinating {
    var view: UIView?
    var receivedDeeplinkUrlToPerform: URL?
    
    func perform(action: BannerFeedItemAction) {
        switch action {
        case .deeplink(let url):
          receivedDeeplinkUrlToPerform = url
        }
    }
}

class BannerFeedCellPresenterTest: XCTestCase {
    let view = FakeBannerFeedItemView()
    
    lazy var coordinator: FakeBannerFeedItemCoordinator = {
        let coordinator = FakeBannerFeedItemCoordinator()
        coordinator.view = view
        return coordinator
    }()
    
    lazy var presenter: BannerFeedItemPresenter = {
         let presenter = BannerFeedItemPresenter(coordinator: coordinator)
        presenter.view = view
        return presenter
    }()
    
    func testSetupItemToDisplay() throws {
        let item = OpportunityItem.makeDummy(id: 1)
        presenter.setupItemToDisplay(item)

        XCTAssertEqual(view.title, item.bannerTitle)
        XCTAssertEqual(view.titleColor?.cgColor, item.textColor.color.cgColor)
        XCTAssertEqual(view.message, item.description)
        XCTAssertEqual(view.messageColor?.cgColor, item.messageColor.color.cgColor)
        XCTAssertEqual(view.buttonTitle?.string, item.action)
        XCTAssertEqual(view.buttonTitleColor?.cgColor, item.actionTextColor.color.cgColor)
        XCTAssertEqual(view.imageUrl?.absoluteString, item.image)
    }
    
    func test_setupItemToDisplay_V2() throws {
        let item = OpportunityItemV2.makeDummy(id: 1)
        presenter.setupItemToDisplay(item)

        XCTAssertEqual(view.title, item.bannerTitle)
        XCTAssertEqual(view.titleColor?.cgColor, item.textColor.color.cgColor)
        XCTAssertEqual(view.message, item.description)
        XCTAssertEqual(view.messageColor?.cgColor, item.messageColor.color.cgColor)
        XCTAssertEqual(view.buttonTitle?.string, item.action)
        XCTAssertEqual(view.buttonTitleColor?.cgColor, item.actionTextColor.color.cgColor)
        XCTAssertEqual(view.imageUrl?.absoluteString, item.image)
    }
    
    func testPerform() {
        let url = URL(string: "picpay://picpay/cardregistration")!

        presenter.perform(action: .deeplink(url: url))
        coordinator.receivedDeeplinkUrlToPerform = url
    }

    private func color(from colorString: String?, defaultColor: UIColor) -> UIColor {
        guard let colorString = colorString else {
            return defaultColor
        }

        return Colors.hexColor(colorString)?.lightColor ?? defaultColor
    }
}
