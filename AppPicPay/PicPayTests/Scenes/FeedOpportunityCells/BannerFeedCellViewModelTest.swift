@testable import PicPay
import XCTest

class FakeBannerFeedItemPresenter: BannerFeedItemPresenting {
    var view: BannerFeedItemDisplay?
    var opportunityItemSetToDisplay: Opportunity?
    var gradientStartColor: String?
    var gradientEndColor: String?
    var deeplinkUrl: URL?
    
    func setupItemToDisplay(_ item: Opportunity) {
        opportunityItemSetToDisplay = item
    }
    
    func applyGradientBackground(startColor: String, endColor: String) {
        gradientStartColor = startColor
        gradientEndColor = endColor
    }
    
    func perform(action: BannerFeedItemAction) {
        switch action {
        case .deeplink(let url):
            deeplinkUrl = url
        }
    }
}

class BannerFeedItemViewModelTest: XCTestCase {
    let presenter = FakeBannerFeedItemPresenter()
    var viewModel: BannerFeedItemViewModelInputs?
    
    let opportunityItem = OpportunityItem.makeDummy(id: 1)
    
    func setupViewModel() {
        viewModel = BannerFeedItemViewModel(presenter: presenter, item: opportunityItem)
    }
    
    override func setUp() {
        super.setUp()
        setupViewModel()
    }
    
    func testFetchData() {
        viewModel?.fetchData()
        XCTAssertNotNil(presenter.opportunityItemSetToDisplay)
        XCTAssertEqual(presenter.opportunityItemSetToDisplay?.bannerTitle, opportunityItem.bannerTitle)
    }
    
    func testDidTapButton() {
        viewModel?.didTapButton()
        XCTAssertNotNil(presenter.deeplinkUrl)
        XCTAssertEqual(presenter.deeplinkUrl?.absoluteString, opportunityItem.deeplink)
    }
    
    func testDidTapInsideCard() {
        viewModel?.didTapInsideCard()
        XCTAssertNotNil(presenter.deeplinkUrl)
        XCTAssertEqual(presenter.deeplinkUrl?.absoluteString, opportunityItem.deeplink)
    }
}
