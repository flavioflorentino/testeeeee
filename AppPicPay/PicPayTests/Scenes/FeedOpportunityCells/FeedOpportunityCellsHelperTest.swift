import XCTest

@testable import PicPay

final class FeedOpportunityCellsHelperTest: XCTestCase {
    func testMergeOpportunities_WhenHasNoFeedItems_ShouldReturnOnlyFistCarouselAndBanner() {
        let sut = FeedOpportunityCellsHelper(opportunities: OpportunityItem.makeDummies())
        let mergedItems = sut.mergeOpportunities(withFeedItems: [])

        XCTAssertEqual(mergedItems.count, 2)
        XCTAssertEqual(mergedItems.first?.component, .carousel)
        XCTAssertEqual(mergedItems.last?.component, .banner)
    }

    func testMergeOpportunities_FirstCarousel_ShouldHaveUpTo4Items() throws {
        try (2...6).forEach { totalOfOpportunities in
            let sut = FeedOpportunityCellsHelper(opportunities: OpportunityItem.makeDummies(count: totalOfOpportunities))
            let mergedItems = sut.mergeOpportunities(withFeedItems: [])

            let carousel = try XCTUnwrap(mergedItems.first(where: { $0.component == .carousel }) as? OpportunityFeedItem)

            XCTAssertEqual(carousel.carouselPosition, .first)
            XCTAssertLessThanOrEqual(carousel.opportunities.count, 4)
        }
    }

    func testMergeOpportunities_WhenThereIsOnlyOneOpportunity_ShouldReturnOnlyABanner() {
        let sut = FeedOpportunityCellsHelper(opportunities: OpportunityItem.makeDummies(count: 1))
        let mergedItems = sut.mergeOpportunities(withFeedItems: [])

        XCTAssertEqual(mergedItems.count, 1)
        XCTAssertEqual(mergedItems.first?.component, .banner)
    }

    func testMergeOpportunities_WhenThereAre5Opportunities_ShouldHaveACarouselAndABanner() {
        let feedItems = makeFeedItems(count: 1)

        let sut = FeedOpportunityCellsHelper(opportunities: OpportunityItem.makeDummies(count: 5))
        let mergedItems = sut.mergeOpportunities(withFeedItems: feedItems)

        XCTAssertEqual(mergedItems.count, feedItems.count + 2)
        XCTAssertNotNil(mergedItems.first(where: { $0.component == .carousel }))
        XCTAssertNotNil(mergedItems.first(where: { $0.component == .banner }))
    }

    func testMergeOpportunities_WhenThereAreSevenOrMoreOpportunities_ShouldHaveACarouselBannerAndAnotherCarousel() {
        let feedItems = makeFeedItems()

        let sut = FeedOpportunityCellsHelper(opportunities: OpportunityItem.makeDummies(count: 7))
        let mergedItems = sut.mergeOpportunities(withFeedItems: feedItems)

        XCTAssertEqual(mergedItems.count, feedItems.count + 3)

        let firstCarouselIndex = index(of: .first, on: mergedItems)
        let bannerIndex = index(of: nil, on: mergedItems)
        let secondCarouselIndex = index(of: .second, on: mergedItems)

        XCTAssertTrue(firstCarouselIndex < bannerIndex && bannerIndex < secondCarouselIndex)
    }

    func testMergeOpportunities_ShouldReturnFirstCarouselAfterTheFirstFeedItem() {
        let feedItems = makeFeedItems()

        let sut = FeedOpportunityCellsHelper(opportunities: OpportunityItem.makeDummies(count: 7))
        let mergedItems = sut.mergeOpportunities(withFeedItems: feedItems)

        XCTAssertEqual(mergedItems.count, feedItems.count + 3)

        let firstCarouselIndex = index(of: .first, on: mergedItems)

        XCTAssertEqual(firstCarouselIndex, 1)
    }

    func testMergeOpportunities_WhenPossible_ShouldReturnOpportunitiesCollectionsWithADistanceOf4Items() throws {
        try (1...10).forEach { totalOfFeedItems in
            let feedItems = makeFeedItems(count: totalOfFeedItems)

            let sut = FeedOpportunityCellsHelper(opportunities: OpportunityItem.makeDummies())
            let mergedItems = sut.mergeOpportunities(withFeedItems: feedItems)

            XCTAssertEqual(mergedItems.count, feedItems.count + 3)

            let firstCarouselIndex = try XCTUnwrap(index(of: .first, on: mergedItems))
            let bannerIndex = try XCTUnwrap(index(of: nil, on: mergedItems))
            let secondCarouselIndex = try XCTUnwrap(index(of: .second, on: mergedItems))

            XCTAssertTrue(firstCarouselIndex < bannerIndex && bannerIndex < secondCarouselIndex)
            XCTAssertLessThanOrEqual(bannerIndex, firstCarouselIndex + 5)
            XCTAssertLessThanOrEqual(secondCarouselIndex, secondCarouselIndex + 5)
        }
    }
}

private extension FeedOpportunityCellsHelperTest {
    func makeFeedItems(count: Int = 10) -> [FeedItem] {
        (0 ..< count).map { _ in
            FeedItem()
        }
    }

    func index(of opportunityCarouselPosition: OpportunityFeedItem.CarouselPosition?, on items: [FeedItem]) -> Int? {
        let opportunitiesFeedItems = items.compactMap({ $0 as? OpportunityFeedItem })

        return items.firstIndex(where: {
            $0 === opportunitiesFeedItems.first(where: { $0.carouselPosition == opportunityCarouselPosition })
        })
    }
}
