@testable import PicPay
import XCTest

class FakeCarouselFeedItemPresenter: CarouselFeedItemPresenting {
    var view: CarouselFeedItemDisplay?
    var receivedData: [Opportunity]?
    var receivedDeeplinkUrlToPerform: URL?
    
    func setupDataSourceHandler(withData data: [Opportunity]) {
        receivedData = data
    }
    
    func perform(action: CarouselFeedItemAction) {
        switch action {
        case .deeplink(let url):
            receivedDeeplinkUrlToPerform = url
        }
    }
}

class CarouselFeedItemViewModelTest: XCTestCase {
    let presenter = FakeCarouselFeedItemPresenter()
    
    func testSetupDataSourceHandler() {
        let opportunityItem = OpportunityFeedItem.makeDummies()[0]
        let viewModel = CarouselFeedItemViewModel(presenter: presenter, opportunityItem: opportunityItem)
        
        viewModel.fetchData()
        XCTAssertNotNil(presenter.receivedData)
        XCTAssertEqual(presenter.receivedData!.count, opportunityItem.opportunities.count)
    }
    
    func testSetupDataSourceHandlerEmptyData() {
        let opportunityItem = OpportunityFeedItem(carouselPosition: .first, opportunities: [])
        let viewModel = CarouselFeedItemViewModel(presenter: presenter, opportunityItem: opportunityItem)
        viewModel.fetchData()
        XCTAssertNotNil(presenter.receivedData)
        XCTAssert(presenter.receivedData.isEmpty)
    }
    
    func testDidTapCollectionItem() throws {
        let opportunityItem = OpportunityFeedItem.makeDummies()[0]
        let viewModel = CarouselFeedItemViewModel(presenter: presenter, opportunityItem: opportunityItem)
        viewModel.fetchData()
        
        let url = try URL(string: opportunityItem.opportunities[2].deeplink).safe()
        viewModel.didTapCollectionItem(at: IndexPath(item: 2, section: 0))
        XCTAssertEqual(presenter.receivedDeeplinkUrlToPerform, url)
    }
    
    func testDidTapCollectionItemWrongIndex() throws {
        let opportunityItem = try OpportunityFeedItem.makeDummies(totalOfOpportunities: 1).first.safe()
        let viewModel = CarouselFeedItemViewModel(presenter: presenter, opportunityItem: opportunityItem)
        viewModel.fetchData()
        
        viewModel.didTapCollectionItem(at: IndexPath(item: 3, section: 0))
        XCTAssertNil(presenter.receivedDeeplinkUrlToPerform)
    }
}
