import XCTest

@testable import PicPay

final class OpportunityFeedItemTests: XCTestCase {
    func testInitialization_WhenThereIsOnlyOneOpportunity_ShouldReturnABannerComponentWithoutCarouselPosition() {
        let sut = OpportunityFeedItem(carouselPosition: .first, opportunities: [OpportunityItem.makeDummy(id: 1)])

        XCTAssertNil(sut.carouselPosition)
        XCTAssertEqual(sut.component, .banner)
    }

    func testInitialization_WhenThereAreMoreThanOneOpportunity_ShouldReturnACarouselAtPassedPosition() {
        let firstCarousel = OpportunityFeedItem(carouselPosition: .first, opportunities: OpportunityItem.makeDummies(count: 2))
        let secondCarousel = OpportunityFeedItem(carouselPosition: .second, opportunities: OpportunityItem.makeDummies(count: 2))

        XCTAssertEqual(firstCarousel.component, .carousel)
        XCTAssertEqual(firstCarousel.carouselPosition, .first)

        XCTAssertEqual(secondCarousel.component, .carousel)
        XCTAssertEqual(secondCarousel.carouselPosition, .second)
    }
}
