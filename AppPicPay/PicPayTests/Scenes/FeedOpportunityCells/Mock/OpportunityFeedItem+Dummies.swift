@testable import PicPay

extension OpportunityFeedItem {
    static func makeDummies(totalOfOpportunities: Int = 10) -> [OpportunityFeedItem] {
        let opportunities = (0..<totalOfOpportunities).map(OpportunityItem.makeDummy(id:))

        guard opportunities.count > 4 else {
            return [OpportunityFeedItem(carouselPosition: .first, opportunities: Array(opportunities.prefix(4)))]
        }

        guard opportunities.count > 5 else {
            return [
                OpportunityFeedItem(carouselPosition: .first, opportunities: Array(opportunities.prefix(4))),
                OpportunityFeedItem(opportunities: [opportunities[4]])
            ]
        }

        return [
            OpportunityFeedItem(carouselPosition: .first, opportunities: Array(opportunities.prefix(4))),
            OpportunityFeedItem(opportunities: [opportunities[4]]),
            OpportunityFeedItem(carouselPosition: .second, opportunities: Array(opportunities[5...]))
        ]
    }
}
