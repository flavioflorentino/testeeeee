@testable import PicPay

extension OpportunityItem {
    static func makeDummies(count: Int = 10) -> [OpportunityItem] {
        (0..<count).map(makeDummy(id:))
    }

    static func makeDummy(id: Int) -> OpportunityItem {
        OpportunityItem(
            id: "\(id)",
            bannerTitle: "Pague um amigo",
            carouselTitle: "Pague seus amigos que estão no PicPay",
            description: "Pague todos os seus contatos que estão no PicPay.",
            action: "Buscar uma pessoa",
            image: "https://s3.amazonaws.com/cdn.picpay.com/imgs/pay_to_person.png",
            deeplink: "picpay://picpay/search",
            titleColor: "#172126",
            descriptionColor: "Pague todos os seus contatos que estão no PicPay.",
            actionColor: "#3D4451",
            cardBackgroundColor: ["#F2FBFF", "#F2FBFF"]
        )
    }
}

extension OpportunityItemV2 {
    static func makeDummies(count: Int = 10) -> [OpportunityItemV2] {
        (0..<count).map(makeDummy(id:))
    }

    static func makeDummy(id: Int) -> OpportunityItemV2 {
        OpportunityItemV2(
            id: "\(id)",
            bannerTitle: "Pague um amigo v2",
            carouselTitle: "Pague seus amigos que estão no PicPay",
            description: "Pague todos os seus contatos que estão no PicPay.",
            action: "Buscar uma pessoa",
            image: "https://s3.amazonaws.com/cdn.picpay.com/imgs/pay_to_person.png",
            deeplink: "picpay://picpay/search",
            titleColor: Color(light: "#172126", dark: "#172126"),
            descriptionColor: Color(light: "#172126", dark: "#172126"),
            actionColor: Color(light: "#172126", dark: "#172126"),
            cardBackgroundColor: Color(light: "#F7F6FF", dark: "#9A8FF1")
        )
    }
}

