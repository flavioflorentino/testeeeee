@testable import PicPay
import XCTest

class FakeCarouselFeedItemView: CarouselFeedItemDisplay {
    var receivedData: [Opportunity]?
    
    func setupDataSourceHandler(withData data: [Opportunity]) {
        receivedData = data
    }
}

class FakeCarouselFeedItemCoordinator: CarouselFeedItemCoordinating {
    var receivedDeeplinkUrlToPerform: URL?
    
    func perform(action: CarouselFeedItemAction) {
        switch action {
        case .deeplink(let url):
            receivedDeeplinkUrlToPerform = url
        }
    }
}

class CarouselFeedCellPresenterTest: XCTestCase {
    func testSetupDataSourceHandler() {
        let items = OpportunityItem.makeDummies()
        let presenter = CarouselFeedItemPresenter(coordinator: CarouselFeedItemCoordinator())
        let view = FakeCarouselFeedItemView()
        presenter.view = view
        presenter.setupDataSourceHandler(withData: items)
        XCTAssertNotNil(view.receivedData)
        XCTAssertEqual(view.receivedData!.count, items.count)
    }
    
    func testSetupDataSourceHandlerEmptyData() {
        let presenter = CarouselFeedItemPresenter(coordinator: CarouselFeedItemCoordinator())
        let view = FakeCarouselFeedItemView()
        presenter.view = view
        presenter.setupDataSourceHandler(withData: [])
        XCTAssertNotNil(view.receivedData)
        XCTAssert(view.receivedData.isEmpty)
    }
    
    func testPerformDeeplink() {
        
        let coordinator = FakeCarouselFeedItemCoordinator()
        let presenter = CarouselFeedItemPresenter(coordinator: coordinator)
        let view = FakeCarouselFeedItemView()
        presenter.view = view
        
        let url = URL(string: OpportunityItem.makeDummies()[2].deeplink)!
        presenter.perform(action: .deeplink(url: url))
        XCTAssertEqual(coordinator.receivedDeeplinkUrlToPerform, url)
    }
}
