import Foundation
@testable import PicPay

final class DeactivateAccountPresenterMock: DeactivateAccountPresenting {
    var viewController: DeactivateAccountDisplay?
    
    var isDeleteAccountCalled: Bool = false
    var isDeactivateWithSuccessCalled: Bool = false
    var isShowLoadingCalled: Bool = false
    var isShowLoading: Bool = false
    var isShowAlertWithErrorCalled: Bool = false
    var isDeactivateAlertCalled: Bool = false
    var isLogoutCalled: Bool = false
    var isSetTextsCalled: Bool = false
    
    func deactivateAccountButtonWasTapped() {
        isDeleteAccountCalled = true
    }
    
    func deactivatedWithSuccess() {
        isDeactivateWithSuccessCalled = true
        isLogoutCalled = true
        showLoading(false)
        showAccountWasDeactivatedAlert()
    }
    
    func showLoading(_ show: Bool) {
        isShowLoadingCalled = true
        isShowLoading = show
    }
    
    func showDeactivateAlert(with: Error) {
        isShowAlertWithErrorCalled = true
        showLoading(false)
    }
    
    func showAccountWasDeactivatedAlert() {
        isDeactivateAlertCalled = true
    }
    
    func logout(fromContext context: UIViewController?) {
        isLogoutCalled = true
    }
    
    func setTexts() {
        isSetTextsCalled = true
    }
}
