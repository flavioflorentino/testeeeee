import Foundation

@testable import PicPay

final class DeactivateAccountViewControllerMock: DeactivateAccountDisplay {
    var isDeleteAccountCalled: Bool = false
    var isDeactivateWithSuccessCalled: Bool = false
    var isShowLoadingCalled: Bool = false
    var isShowLoading: Bool = false
    var isLogoutCalled: Bool = false
    var title: String?
    var description: NSAttributedString?
    
    func showLoading(_ show: Bool) {
        isShowLoadingCalled = true
        isShowLoading = show
    }
    
    func logout() {
        isLogoutCalled = true
    }
    
    func showDeactivateAccountConfirmation() {
        isDeleteAccountCalled = true
    }
    
    func setTitle(_ title: String) {
        self.title = title
    }
    
    func setDescription(_ description: NSAttributedString) {
        self.description = description
    }
}
