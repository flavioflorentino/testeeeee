import Foundation
@testable import PicPay

class DeactivateAccountCoordinatorMock: DeactivateAccountCoordinating {
    var viewController: UIViewController?
    var actionType: DeactivateAccountAction?
    
    var showAlertWithError: Error?
    var deactivateWithSuccessfullAlert: Alert?
    var deactivateConfirmationAlert: Alert?
    
    func perform(action: DeactivateAccountAction) {
        switch action {
        case .showDeactivateAlert(let error):
            showDeactivateAlert(with: error)
        case .showAccountWasDeactivated(let alert):
            showAccountWasDeactivated(with: alert)
        case .showDeactivateAccountConfirmation(let alert):
            showDeactivateAccountConfirmation(with: alert)
        }
    }
    
    func showDeactivateAlert(with error: Error) {
        showAlertWithError = error
    }
    
    func showAccountWasDeactivated(with alert: Alert) {
        deactivateWithSuccessfullAlert = alert
    }
    
    func showDeactivateAccountConfirmation(with alert: Alert) {
        deactivateConfirmationAlert = alert
    }
}
