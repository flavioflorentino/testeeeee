import Foundation
@testable import PicPay

class DeactivateAccountViewModelMock: DeactivateAccountViewModelInputs {
    var isDeleteAccountCalled: Bool = false
    var isDeactivatedWithSuccessCalled: Bool = false
    var isShowAlertCalled: Bool = false
    var isLogoutCalled: Bool = false
    
    func viewDidLoad() {}
    
    func deactivateAccountButtonWasTapped() {
        isDeleteAccountCalled = true
    }
    
    func showDeactivateAccountConfirmation() {
        isDeactivatedWithSuccessCalled = true
    }
    
    func logout(fromContext context: UIViewController?) {
        isLogoutCalled = true
    }
}

class DeactivateAccountViewModelFailureMock: DeactivateAccountViewModelMock {
    override func showDeactivateAccountConfirmation() {
        isShowAlertCalled = true
    }
}
