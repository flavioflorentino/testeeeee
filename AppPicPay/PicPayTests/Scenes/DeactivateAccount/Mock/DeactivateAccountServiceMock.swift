import Foundation
@testable import PicPay

public class DeactivateAccountServiceMock: DeactivateAccountServicing {
    var isDeactivateAccountCalled: Bool = false
    var isAuthenticateCalled: Bool = false
    
    public func authenticate(_ completion: @escaping (Result<AuthResult<String>, Error>) -> Void) {
        isAuthenticateCalled = true
        completion(.success(AuthResult.success("password")))
    }
    
    public func deactivateAccount(password: String, _ completion: @escaping (Result<Void, Error>) -> Void) {
        isDeactivateAccountCalled = true
        completion(.success)
    }
}

public class DeactivateAccountServiceFailureMock: DeactivateAccountServiceMock {
    override public func deactivateAccount(password: String, _ completion: @escaping (Result<Void, Error>) -> Void) {
        isDeactivateAccountCalled = true
        completion(.failure(PicPayError(message: "Ocorreu um erro")))
    }
}
