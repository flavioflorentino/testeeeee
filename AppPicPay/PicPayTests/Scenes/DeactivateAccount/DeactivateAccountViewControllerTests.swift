import XCTest
@testable import PicPay

private enum MocksTests {
    case success
    case failure
}

class DeactivateAccountViewControllerTests: XCTestCase {
    var sut: DeactivateAccountViewController!
    
    var serviceMock: DeactivateAccountServiceMock!
    var presenterMock: DeactivateAccountPresenterMock!
    var coordinatorMock: DeactivateAccountCoordinatorMock!
    var mockDependencies = DependencyContainerMock(AuthManagerMock())

    override func setUp() {
        super.setUp()
        setUpMocks()
    }
    
    private func setUpMocks(_ mocks: MocksTests = .success) {
        presenterMock = DeactivateAccountPresenterMock()
        coordinatorMock = DeactivateAccountCoordinatorMock()
        
        switch mocks {
        case .success:
            serviceMock = DeactivateAccountServiceMock()
        case .failure:
            serviceMock = DeactivateAccountServiceFailureMock()
        }
        let viewModel = DeactivateAccountViewModel(service: serviceMock,
                                                   presenter: presenterMock,
                                                   dependencies: mockDependencies)
        
        sut = DeactivateAccountViewController(viewModel: viewModel)
    }
    
    func testSetTexts() {
        setUpMocks()
        
        guard sut.view != nil else {
            XCTFail()
            return
        }
        XCTAssertTrue(presenterMock.isSetTextsCalled)
    }
    
    func testLoadingShouldStart() {
        setUpMocks()
        sut.showDeactivateAccountConfirmation()
        sut.showLoading(true)
        XCTAssertTrue(presenterMock.isShowLoadingCalled)
    }
    
    func testLoadingShouldStop() {
        setUpMocks()
        sut.showDeactivateAccountConfirmation()
        sut.showLoading(false)
        XCTAssertTrue(presenterMock.isShowLoadingCalled)
        XCTAssertFalse(presenterMock.isShowLoading)
    }
    
    func testTapDeleteAccountShouldShowAlert() {
        setUpMocks()
        sut.tapDeactivateAccountButton()
        XCTAssertTrue(presenterMock.isDeleteAccountCalled)
    }
    
    func testDeleteAccountConfirmationShouldConfirmDelete() {
        setUpMocks()
        sut.showDeactivateAccountConfirmation()
        XCTAssertTrue(serviceMock.isAuthenticateCalled)
        XCTAssertTrue(serviceMock.isDeactivateAccountCalled)
        XCTAssertTrue(presenterMock.isDeactivateWithSuccessCalled)
    }
    
    func testLogout() {
        setUpMocks()
        sut.logout()
    }
}
