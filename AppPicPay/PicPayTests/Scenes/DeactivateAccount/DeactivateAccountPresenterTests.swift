import FeatureFlag
import XCTest

@testable import PicPay

private enum MocksTests {
    case success
    case failure
}

class DeactivateAccountPresenterTests: XCTestCase {
    var sut: DeactivateAccountPresenting!
    var viewControllerMock: DeactivateAccountViewControllerMock!
    var coordinatorMock: DeactivateAccountCoordinatorMock!
    var featureManagerMock = FeatureManagerMock()
    lazy var mockDependencies = DependencyContainerMock(featureManagerMock)
    
    override func setUp() {
        super.setUp()
        coordinatorMock = DeactivateAccountCoordinatorMock()
        sut = DeactivateAccountPresenter(coordinator: coordinatorMock, dependencies: mockDependencies)
        viewControllerMock = DeactivateAccountViewControllerMock()
        sut.viewController = viewControllerMock
    }
    
    func testSetTexts() {
        featureManagerMock.override(with: [.deactivateAccountScreenTitle: "Mocked Title",
                                           .deactivateAccountScreenDesc: "Mocked Description"])
        sut.setTexts()
        XCTAssertEqual(viewControllerMock.title, "Mocked Title")
        XCTAssertEqual(viewControllerMock.description?.string, "Mocked Description")
    }
    
    func testLoadingShouldStart() {
        sut.showLoading(true)
        XCTAssertTrue(viewControllerMock.isShowLoadingCalled)
    }
    
    func testLoadingShouldStop() {
        sut.showLoading(false)
        XCTAssertTrue(viewControllerMock.isShowLoadingCalled)
        XCTAssertFalse(viewControllerMock.isShowLoading)
    }
    
    func testDeactivateConfirmationShouldShowAlert() {
        featureManagerMock.override(key: .deactivateAccountConfirmTitle, with: "dialogConfirmationTitleMock")
        
        mockDependencies.mainQueue.async {
            self.sut.deactivateAccountButtonWasTapped()
            
            let alert = self.coordinatorMock.deactivateConfirmationAlert
            XCTAssertEqual(alert?.title?.string, "dialogConfirmationTitleMock")
            
            let buttonTitles = alert?.buttons.map { $0.title }
            XCTAssertEqual(buttonTitles, ["Desativar conta", "Cancelar"])
            
            let buttonActions = alert?.buttons.map { $0.action }
            XCTAssertEqual(buttonActions, [Button.Action.handler, Button.Action.close])
        }
    }
    
    func testDeactivateWithErrorShouldShowAlert() {
        mockDependencies.mainQueue.async {
            self.sut.showDeactivateAlert(with: PicPayError(message: "Ocorreu um erro"))
            let error = self.coordinatorMock.showAlertWithError as? PicPayError
            XCTAssertEqual(error?.message, "Ocorreu um erro")
        }
    }

    func testDeactivateWithSuccessShouldShowAlert() {
        featureManagerMock.override(with: [.deactivateAccountSuccessTitle: "alertSuccessTitleMock",
                                           .deactivateAccountSuccessDesc: "alertSuccessDescriptionMock"])

        mockDependencies.mainQueue.async {
            self.sut.showAccountWasDeactivatedAlert()
            
            let alert = self.coordinatorMock.deactivateWithSuccessfullAlert
            XCTAssertEqual(alert?.title?.string, "alertSuccessTitleMock")
            XCTAssertEqual(alert?.text?.string, "alertSuccessDescriptionMock")
        }
    }
    
    func testDeactivateWithSuccess() {
        sut.deactivatedWithSuccess()
        XCTAssertTrue(viewControllerMock.isLogoutCalled)
    }
}
