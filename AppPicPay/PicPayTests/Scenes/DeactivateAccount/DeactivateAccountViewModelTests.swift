import XCTest
@testable import PicPay

class DeactivateAccountViewModelTests: XCTestCase {

    var sut: DeactivateAccountViewModel!
    var serviceMock: DeactivateAccountServiceMock!
    var presenterMock: DeactivateAccountPresenterMock!

    func setUpMocks(isSuccess: Bool = true) {
        serviceMock = isSuccess ? DeactivateAccountServiceMock() : DeactivateAccountServiceFailureMock()
        presenterMock = DeactivateAccountPresenterMock()
        sut = DeactivateAccountViewModel(service: serviceMock, presenter: presenterMock, dependencies: DependencyContainerMock())
    }
    
    func testSetTexts() {
        setUpMocks()
        sut.viewDidLoad()
        XCTAssertTrue(presenterMock.isSetTextsCalled)
    }
    
    func testDeactivationSuccess() {
        setUpMocks()
        sut.showDeactivateAccountConfirmation()
        XCTAssertTrue(presenterMock.isShowLoadingCalled)
        XCTAssertTrue(serviceMock.isAuthenticateCalled)
        XCTAssertTrue(serviceMock.isDeactivateAccountCalled)
        XCTAssertFalse(presenterMock.isShowAlertWithErrorCalled)
        XCTAssertTrue(presenterMock.isDeactivateAlertCalled)
    }
    
    func testDeactivationFailure() {
        setUpMocks(isSuccess: false)
        sut.showDeactivateAccountConfirmation()
        XCTAssertTrue(presenterMock.isShowLoadingCalled)
        XCTAssertTrue(serviceMock.isAuthenticateCalled)
        XCTAssertTrue(serviceMock.isDeactivateAccountCalled)
        XCTAssertTrue(presenterMock.isShowAlertWithErrorCalled)
    }
    
    func testDeleteAccountWhenCancelAction() {
        setUpMocks()
        sut.deactivateAccountButtonWasTapped()
        XCTAssertTrue(presenterMock.isDeleteAccountCalled)
    }
    
    func testAuthenticationCancel() {
        setUpMocks()
        sut.showDeactivateAccountConfirmation()
        XCTAssertTrue(presenterMock.isShowLoadingCalled)
        XCTAssertTrue(serviceMock.isAuthenticateCalled)
    }
}
