@testable import PicPay
@testable import AnalyticsModule
import XCTest

final class CvvRegisterServiceMock: CvvRegisterServicing {
    var hasDynamicCvv = false
    var isAmex: Bool = false
    var flagCardExists: Bool = true
    
    var cards: [CardBank] {
        let cardsBank = try? MockDecodable<[CardBank]>().loadCodableObject(resource: "registerCards")
        return cardsBank ?? []
    }
    
    var creditCard: CardBank? {
        let card = try? MockDecodable<[CardBank]>().loadCodableObject(resource: "registerCards")[4]
        if isAmex {
            card?.creditCardBandeiraId = "\(CreditCardFlagTypeId.amex.rawValue)"
        }
        if !flagCardExists {
            card?.creditCardBandeiraId = "10"
        }
        return card
    }
    
    var debitCard: CardBank? {
        try? MockDecodable<[CardBank]>().loadCodableObject(resource: "registerCards")[2]
    }
}

final class CvvRegisterPresenterSpy: CvvRegisterPresenting {
    var viewController: CvvRegisterDisplay?
    
    private(set) var calledDisplayCard = 0
    private(set) var calledDisplayButtonIsEnable = 0
    private(set) var calledDidNextStep = 0
    private(set) var calledDisplayNotFoundCard = 0
    private(set) var calledDiplayNumMaxCvv = 0
    
    private(set) var model: CvvRegister?
    private(set) var value: Bool?
    private(set) var action: CvvRegisterAction?
    private(set) var cvvLimit: Int?
    
    func displayCard(model: CvvRegister, isEmergencyAid: Bool) {
        calledDisplayCard += 1
        self.model = model
    }
    
    func displayNotFoundCard() {
        calledDisplayNotFoundCard += 1
    }
    
    func displayButtonIsEnable(value: Bool) {
        calledDisplayButtonIsEnable += 1
        self.value = value
    }
    
    func didNextStep(action: CvvRegisterAction) {
        calledDidNextStep += 1
        self.action = action
    }
    
    func diplayNumMaxCvv(limit: Int) {
        calledDiplayNumMaxCvv += 1
        self.cvvLimit = limit
    }
}

final class CvvRegisterViewModelTest: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    let serviceMock = CvvRegisterServiceMock()
    let presenterSpy = CvvRegisterPresenterSpy()
    private var cardType: CvvRegisterViewModel.CardType = .credit
    
    private lazy var sut: CvvRegisterViewModelInputs = {
        CvvRegisterViewModel(
            origin: "Origin",
            isEmergencyAid: false,
            cardType: cardType,
            service: serviceMock,
            presenter: presenterSpy,
            dependencies: DependencyContainerMock(analyticsSpy)
        )
    }()

    func testViewDidLoad_WhenCardTypeCredit_ShouldCallDisplayCard() {
        cardType = .credit
        
        sut.viewDidLoad()
        
        XCTAssertEqual(presenterSpy.calledDisplayCard, 1)
        XCTAssertEqual(presenterSpy.model?.alias, "Teste Storck QA")
        XCTAssertEqual(presenterSpy.model?.image, "https://cdn.picpay.com/apps/picpay/imgs/visa.png")
    }
    
    func testViewDidLoad_WhenCardTypeCreditNotAmex_ShouldCallDiplayNumMaxCvvWith3() {
        cardType = .credit
        serviceMock.isAmex = false
        
        sut.viewDidLoad()
        
        XCTAssertEqual(presenterSpy.calledDiplayNumMaxCvv, 1)
        XCTAssertEqual(presenterSpy.cvvLimit, 3)
    }
    
    func testViewDidLoad_WhenCardTypeCreditAmex_ShouldCallDiplayNumMaxCvvWith4() {
        cardType = .credit
        serviceMock.isAmex = true
        
        sut.viewDidLoad()
        
        XCTAssertEqual(presenterSpy.calledDiplayNumMaxCvv, 1)
        XCTAssertEqual(presenterSpy.cvvLimit, 4)
    }
    
    func testViewDidLoad_WhenCardTypeCreditGlagNotExist_ShouldCallDiplayNumMaxCvvWith4() {
        cardType = .credit
        serviceMock.flagCardExists = false
        
        sut.viewDidLoad()
        
        XCTAssertEqual(presenterSpy.calledDiplayNumMaxCvv, 1)
        XCTAssertEqual(presenterSpy.cvvLimit, 4)
    }
    
    func testViewDidLoad_WhenCardTypeIdExist_ShouldCallDisplayCard() {
        cardType = .id("42722799999")
        
        sut.viewDidLoad()
        
        XCTAssertEqual(presenterSpy.calledDisplayCard, 1)
        XCTAssertEqual(presenterSpy.model?.alias, "Teste Storck Credito e Debito 2 Esse")
        XCTAssertEqual(presenterSpy.model?.image, "https://cdn.picpay.com/apps/picpay/imgs/visa.png")
    }
    
    func testViewDidLoad_WhenCardTypeIdNotExist_ShouldCallDisplayCard() {
        cardType = .id("12")
        
        sut.viewDidLoad()
        
        XCTAssertEqual(presenterSpy.calledDisplayNotFoundCard, 1)
    }
    
    func testViewDidLoad_WhenCardTypeDebit_ShouldCallDisplayCard() {
        cardType = .debit
        
        sut.viewDidLoad()
        
        XCTAssertEqual(presenterSpy.calledDisplayCard, 1)
        XCTAssertEqual(presenterSpy.model?.alias, "Teste Storck Debito 1")
        XCTAssertEqual(presenterSpy.model?.image, "https://cdn.picpay.com/apps/picpay/imgs/visa.png")
    }
    
    func testUpdateCvv_WhenValueLess3_ShouldCallDisplayButtonIsEnable() {
        sut.updateCvv(value: "12")
        
        XCTAssertEqual(presenterSpy.calledDisplayButtonIsEnable, 1)
        XCTAssertEqual(presenterSpy.value, false)
    }
    
    func testUpdateCvv_WhenValueEqual3_ShouldCallDisplayButtonIsEnable() {
        sut.updateCvv(value: "123")
        
        XCTAssertEqual(presenterSpy.calledDisplayButtonIsEnable, 1)
        XCTAssertEqual(presenterSpy.value, true)
    }
    
    
    func testDidTapContinue_WhenCall_ShouldCallDidNextStep() {
        sut.didTapContinue(value: "123")
        
        XCTAssertEqual(presenterSpy.calledDidNextStep, 1)
        switch presenterSpy.action {
        case .success(let cvv):
            XCTAssertEqual(cvv, "123")
        default:
            XCTFail("Not Call Success")
        }
    }
    
    func testDidTapInformation_WhenCall_ShouldCallDidNextStep() {
        sut.didTapInformation()
        
        XCTAssertEqual(presenterSpy.calledDidNextStep, 1)
        switch presenterSpy.action {
        case .openInformation:
            XCTAssertTrue(true)
        default:
            XCTFail("Not Call OpenInformation")
        }
    }
    
    func testDidTapClose_WhenCall_ShouldCallDidNextStep() {
        sut.didTapClose()
        
        XCTAssertEqual(presenterSpy.calledDidNextStep, 1)
        switch presenterSpy.action {
        case .close:
            XCTAssertTrue(true)
        default:
            XCTFail("Not Call Close")
        }
    }
    
    func testDidTapClose_WhenCardTypeCredit_ShouldCallAnalytics() throws {
        serviceMock.hasDynamicCvv = false
        cardType = .credit
        
        sut.didTapClose()
        
        let cardId = try XCTUnwrap(serviceMock.creditCard?.id)
        let properties = CvvRegisterEvent.CvvEventProperties(
            isDynamicCvv: serviceMock.hasDynamicCvv,
            origin: "Origin",
            cardId: cardId
        )
        let expectedEvent = CvvRegisterEvent.notEnterCvv(properties: properties).event()
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testDidTapClose_WhenCardTypeDebit_ShouldCallAnalytics() throws {
        serviceMock.hasDynamicCvv = false
        cardType = .debit
        
        sut.didTapClose()
        
        let cardId = try XCTUnwrap(serviceMock.debitCard?.id)
        let properties = CvvRegisterEvent.CvvEventProperties(
            isDynamicCvv: serviceMock.hasDynamicCvv,
            origin: "Origin",
            cardId: cardId
        )
        let expectedEvent = CvvRegisterEvent.notEnterCvv(properties: properties).event()
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testDidTapContinue_WhenCardTypeCredit_ShouldCallAnalytics() throws {
        serviceMock.hasDynamicCvv = false
        cardType = .credit
        
        sut.didTapContinue(value: "1234")
        
        let cardId = try XCTUnwrap(serviceMock.creditCard?.id )
        let properties = CvvRegisterEvent.CvvEventProperties(
            isDynamicCvv: serviceMock.hasDynamicCvv,
            origin: "Origin",
            cardId: cardId
        )
        let expectedEvent = CvvRegisterEvent.insertedCvv(properties: properties).event()
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testDidTapContinue_WhenCardTypeDebit_ShouldCallAnalytics() throws {
        serviceMock.hasDynamicCvv = false
        cardType = .debit
        
        sut.didTapContinue(value: "1234")
        
        let cardId = try XCTUnwrap(serviceMock.debitCard?.id)
        let properties = CvvRegisterEvent.CvvEventProperties(
            isDynamicCvv: serviceMock.hasDynamicCvv,
            origin: "Origin",
            cardId: cardId
        )
        let expectedEvent = CvvRegisterEvent.insertedCvv(properties: properties).event()
        
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
}
