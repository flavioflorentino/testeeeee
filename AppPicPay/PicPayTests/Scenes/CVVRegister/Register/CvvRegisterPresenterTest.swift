@testable import PicPay
import XCTest

final class MockCvvRegisterViewController: CvvRegisterDisplay {
    private(set)var calledUpdateCard = false
    private(set)var calledButtonIsEnable = false
    private(set)var calledDidNextStep = false
    private(set)var calledDisplayViewState = false
    private(set)var calledHideRightBarButton = false

    private(set) var calledDisplayAlert = 0
    private(set) var calledConfigureInputField = 0
    
    private(set) var numLimit: Int?
    
    private(set) var message: String?
    var model: CvvRegister?
    var value: Bool?
    var action: CvvRegisterAction?
    
    func displayAlert(message: String) {
        calledDisplayAlert += 1
        self.message = message
    }
    
    func updateCard(model: CvvRegister) {
        calledUpdateCard = true
        self.model = model
    }
    
    func buttonIsEnable(value: Bool) {
        calledButtonIsEnable = true
        self.value = value
    }
    
    func didNextStep(action: CvvRegisterAction) {
        calledDidNextStep = true
        self.action = action
    }

    func displayViewState(viewState: CvvRegisterViewState) {
        calledDisplayViewState = true
    }

    func hideRightBarButton() {
        calledHideRightBarButton = true
    }
    
    func configureInputField(limit: Int) {
        calledConfigureInputField += 1
        self.numLimit = limit
    }
}

final class CvvRegisterPresenterTest: XCTestCase {
    func testDisplayCardShouldCallUpdateCardWithModel() {
        let mockViewController = MockCvvRegisterViewController()
        let presenter = CvvRegisterPresenter()
        let model = CvvRegister(alias: "Alias", lastDigits: "1234", image: "Image")
        
        presenter.viewController = mockViewController
        presenter.displayCard(model: model)
        
        XCTAssertTrue(mockViewController.calledUpdateCard)
        XCTAssertEqual(mockViewController.model?.alias, "Alias")
        XCTAssertEqual(mockViewController.model?.lastDigits, "1234")
        XCTAssertEqual(mockViewController.model?.image, "Image")
    }
    
    func testDisplayNotFoundCard_ShouldCallDisplayAlert() {
        let mockViewController = MockCvvRegisterViewController()
        let presenter = CvvRegisterPresenter()
        presenter.viewController = mockViewController
        
        presenter.displayNotFoundCard()
        XCTAssertEqual(mockViewController.calledDisplayAlert, 1)
        XCTAssertEqual(mockViewController.message, CvvLocalizable.notFoundCard.text)
    }
    
    func testDisplayCardShouldCallButtonIsEnableWithFalse() {
        let mockViewController = MockCvvRegisterViewController()
        let presenter = CvvRegisterPresenter()
        let model = CvvRegister(alias: "Alias", lastDigits: "1234", image: "Image")
        
        presenter.viewController = mockViewController
        presenter.displayCard(model: model)
        
        XCTAssertTrue(mockViewController.calledButtonIsEnable)
        XCTAssertEqual(mockViewController.value, false)
    }
    
    func testDisplayButtonIsEnableShouldCallButtonIsEnableWithTrue() {
        let mockViewController = MockCvvRegisterViewController()
        let presenter = CvvRegisterPresenter()
        
        presenter.viewController = mockViewController
        presenter.displayButtonIsEnable(value: true)
        
        XCTAssertTrue(mockViewController.calledButtonIsEnable)
        XCTAssertEqual(mockViewController.value, true)
    }
    
    func testDidNextStepShouldCallDidNextStep() {
        let mockViewController = MockCvvRegisterViewController()
        let presenter = CvvRegisterPresenter()
        
        presenter.viewController = mockViewController
        presenter.didNextStep(action: .close)
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
    }

    func testDisplayCard_WhenIsEmergencyAid_ShouldCallDisplayViewState() {
        let mockViewController = MockCvvRegisterViewController()
        let presenter = CvvRegisterPresenter()
        let model = CvvRegister(alias: "Alias", lastDigits: "1234", image: "Image")

        presenter.viewController = mockViewController
        presenter.displayCard(model: model, isEmergencyAid: true)

        XCTAssertTrue(mockViewController.calledDisplayViewState)
        XCTAssertFalse(mockViewController.calledHideRightBarButton)
    }
    
    func testdiplayNumMaxCvv_WhenLimit10_ShouldCallConfigureInputField() {
        let mockViewController = MockCvvRegisterViewController()
        let presenter = CvvRegisterPresenter()

        presenter.viewController = mockViewController
        presenter.diplayNumMaxCvv(limit: 10)

        XCTAssertEqual(mockViewController.calledConfigureInputField, 1)
        XCTAssertEqual(mockViewController.numLimit, 10)
    }
}
