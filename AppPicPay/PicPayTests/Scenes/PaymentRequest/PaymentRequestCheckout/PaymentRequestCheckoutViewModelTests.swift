import Core
import XCTest
@testable import PicPay

private final class FakePaymentRequestCheckoutService: PaymentRequestCheckoutServicing {
    
    var isSuccess: Bool = false
    
    func sendPaymentRequest(idPayer: Int, message: String, value: Double, completion: @escaping (Result<(model: PaymentRequestCheckoutResponse, data: Data?), ApiError>) -> Void) {
        if isSuccess {
            let checkoutResponse = PaymentRequestCheckoutResponse(success: true)
            let response: (model: PaymentRequestCheckoutResponse, data: Data?) = (model: checkoutResponse, data: nil)
            completion(.success(response))
        } else {
            completion(.failure(ApiError.connectionFailure))
        }
    }
}

private final class FakePaymentRequestCheckoutPresenter: PaymentRequestCheckoutPresenting {
    var viewController: PaymentRequestCheckoutDisplay?
    
    private(set) var didDisplayProfileCount = 0
    private(set) var contact: PPContact?
    private(set) var didDisplayPrivacyAlertCount = 0
    private(set) var didUpdateButtonStateCount = 0
    private(set) var isButtonEnable: Bool?
    private(set) var didPopViewControllerCount = 0
    private(set) var didShowPlaceholderMessageCount = 0
    private(set) var isPlaceholderMessageVisible: Bool?
    private(set) var didPresentProfileCount = 0
    private(set) var profileImage: UIPPProfileImage?
    private(set) var didPresentPaymentRequestSuccessCount = 0
    private(set) var didPresentPaymentRequestErrorCount = 0
    private(set) var didDismissViewControllerCount = 0
    private(set) var didPresentShouldFillValueAlertCount = 0
    private(set) var didStartLoadingCount = 0
    
    func displayProfile(with contact: PPContact) {
        didDisplayProfileCount += 1
        self.contact = contact
    }
    
    func presentPrivacyAlert() {
        didDisplayPrivacyAlertCount += 1
    }
    
    func updateSendButtonState(isEnable: Bool) {
        didUpdateButtonStateCount += 1
        self.isButtonEnable = isEnable
    }
    
    func popViewController() {
        didPopViewControllerCount += 1
    }
    
    func showPlaceholderMessage(isVisible: Bool) {
        didShowPlaceholderMessageCount += 1
        isPlaceholderMessageVisible = isVisible
    }
    
    func presentProfile(profileImage: UIPPProfileImage, contact: PPContact) {
        didPresentProfileCount += 1
        self.profileImage = profileImage
        self.contact = contact
    }
    
    func presentPaymentRequestSuccess() {
        didPresentPaymentRequestSuccessCount += 1
    }
    
    func dismissViewController() {
        didDismissViewControllerCount += 1
    }
    
    func presentPaymentRequestError() {
        didPresentPaymentRequestErrorCount += 1
    }
    
    func presentShouldFillValueAlert() {
        didPresentShouldFillValueAlertCount += 1
    }
    
    func startLoading() {
        didStartLoadingCount += 1
    }
}

final class PaymentRequestCheckoutViewModelTests: XCTestCase {
    // MARK: - Variables
    private lazy var service = FakePaymentRequestCheckoutService()
    private lazy var presenter = FakePaymentRequestCheckoutPresenter()
    private let contact = PPContact(profileDictionary: ["username": "userTest"])
    private var backButtonClose = false
    
    private lazy var viewModel = PaymentRequestCheckoutViewModel(service: service, presenter: presenter, contact: contact, backButtonClose: backButtonClose)
    
    // MARK: - loadProfile
    func testLoadProfile_ShouldCallDisplayProfile() throws {
        viewModel.loadProfile()
        let unwrappedContact = try XCTUnwrap(presenter.contact)
        XCTAssertEqual(presenter.didDisplayProfileCount, 1)
        XCTAssertEqual(unwrappedContact, contact)
    }
    
    // MARK: - sendPaymentRequest
    func testSendPaymentRequest_WhenValueEqualOrLessThanZero_ShouldCallPresentShouldFillValueAlert() {
        viewModel.sendPaymentRequest(message: "", value: 0)
        XCTAssertEqual(presenter.didPresentShouldFillValueAlertCount, 1)
    }
    
    func testSendPaymentRequest_WhenValueIsGreaterThanZeroAndIsSuccess_ShouldCallPresentPaymentRequestSuccess() {
        service.isSuccess = true
        viewModel.sendPaymentRequest(message: "", value: 1)
        XCTAssertEqual(presenter.didStartLoadingCount, 1)
        XCTAssertEqual(presenter.didPresentPaymentRequestSuccessCount, 1)
    }
    
    func testSendPaymentRequest_WhenValueIsGreaterThanZeroAndIsFailure_ShouldCallPresentPaymentRequestError() {
        service.isSuccess = false
        viewModel.sendPaymentRequest(message: "", value: 1)
        XCTAssertEqual(presenter.didStartLoadingCount, 1)
        XCTAssertEqual(presenter.didPresentPaymentRequestErrorCount, 1)
    }
    
    // MARK: - beginEditingTextView and changeTextView
    func testBeginEditingTextView_WhenHasOnlyPlaceholderOnTextView_ShouldCallShowPlaceholderMessageAndPlaceholderNotBeVisible() throws {
        viewModel.beginEditingTextView()
        let unwrappedIsPlaceholderMessageVisible = try XCTUnwrap(presenter.isPlaceholderMessageVisible)
        XCTAssertEqual(presenter.didShowPlaceholderMessageCount, 1)
        XCTAssertFalse(unwrappedIsPlaceholderMessageVisible)
    }
    
    func testBeginEditingTextView_WhenHasTextOnTextView_ShouldNotCallShowPlaceholderMessage() {
        viewModel.changeTextView(text: "teste")
        viewModel.beginEditingTextView()
        XCTAssertEqual(presenter.didShowPlaceholderMessageCount, 0)
    }
    
    // MARK: - endEditingTextView and changeTextView
    func testEndEditingTextView_WhenHasOnlyPlaceholderOnTextView_ShouldCallShowPlaceholderMessageAndPlaceholderBeVisible() throws {
        viewModel.endEditingTextView()
        let unwrappedIsPlaceholderMessageVisible = try XCTUnwrap(presenter.isPlaceholderMessageVisible)
        XCTAssertEqual(presenter.didShowPlaceholderMessageCount, 1)
        XCTAssert(unwrappedIsPlaceholderMessageVisible)
    }
    
    func testEndEditingTextView_WhenHasTextOnTextView_ShouldNotCallShowPlaceholderMessage() {
        viewModel.changeTextView(text: "teste")
        viewModel.endEditingTextView()
        XCTAssertEqual(presenter.didShowPlaceholderMessageCount, 0)
    }
    
    // MARK: - touchOnPrivacyButton
    func testTouchOnPrivateButton_ShouldPresentPrivacyAlert() {
        viewModel.touchOnPrivacyButton()
        XCTAssertEqual(presenter.didDisplayPrivacyAlertCount, 1)
    }
    
    // MARK: - touchOnCloseButton
    func testTapCloseButton_WhenBackButtonCloseIsTrue_ShouldCallPopViewController() {
        viewModel.didTapCloseButton()
        XCTAssertEqual(presenter.didPopViewControllerCount, 1)
    }
    
    func testTapCloseButton_WhenBackButtonCloseIsFalse_ShouldCallDismissViewController() {
        backButtonClose = true
        viewModel.didTapCloseButton()
        XCTAssertEqual(presenter.didDismissViewControllerCount, 1)
    }
    
    // MARK: - shouldChangeText
    func testShouldChangeText_WhenTextCountLessOrEqualThan100_ShouldReturnTrue() {
        backButtonClose = false
        let returnedValue = viewModel.shouldChangeText("")
        XCTAssert(returnedValue)
    }
    
    func testShouldChangeText_WhenTextCountGreaterThan100_ShouldReturnFalse() {
        let text = "Teste teste teste teste teste teste teste teste teste teste teste teste teste teste teste teste teste"
        let returnedValue = viewModel.shouldChangeText(text)
        XCTAssertFalse(returnedValue)
    }
    
    // MARK: - showProfile
    func testShowProfile_ShouldCallPresentProfileAndHasTheGivenData() throws {
        let profileImage = UIPPProfileImage(frame: .zero)
        viewModel.showProfile(profileImage: profileImage, contact: contact)
        let unwrappedContact = try XCTUnwrap(presenter.contact)
        let unwrappedProfileImage = try XCTUnwrap(presenter.profileImage)
        XCTAssertEqual(presenter.didPresentProfileCount, 1)
        XCTAssertEqual(unwrappedContact, contact)
        XCTAssertEqual(unwrappedProfileImage, profileImage)
    }
    
    // MARK: - dismissViewController
    func testDismissViewController_ShouldCallDismiss() {
        viewModel.dismissViewController()
        XCTAssertEqual(presenter.didDismissViewControllerCount, 1)
    }
}
