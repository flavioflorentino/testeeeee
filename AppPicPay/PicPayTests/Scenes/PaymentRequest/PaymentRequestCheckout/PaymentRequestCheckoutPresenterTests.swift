import XCTest
@testable import PicPay
@testable import UI

private final class PaymentRequestCheckoutCoordinatorMock: PaymentRequestCheckoutCoordinating {
    var viewController: UIViewController?
    
    private(set) var didPerformBackCount = 0
    private(set) var didShowProfileCount = 0
    private(set) var contact: PPContact?
    private(set) var profileImage: UIPPProfileImage?
    private(set) var action: PaymentRequestCheckoutAction?
    private(set) var didDisplayPaymentRequestSuccessCount = 0
    private(set) var didPresentAlertCount = 0
    
    func perform(action: PaymentRequestCheckoutAction) {
        didPerformBackCount += 1
        self.action = action
    }
    
    func performShowProfile(profileImage: UIPPProfileImage, contact: PPContact) {
        didShowProfileCount += 1
        self.profileImage = profileImage
        self.contact = contact
    }
    
    func presentSuccessDialog() {
        didDisplayPaymentRequestSuccessCount += 1
    }
    
    func present(alertController: UIAlertController) {
        didPresentAlertCount += 1
    }
}

private final class PaymentRequestCheckoutControllerMock: PaymentRequestCheckoutDisplay {
    private(set) var didDisplayProfileCount = 0
    private(set) var username: String?
    private(set) var contact: PPContact?
    private(set) var didShowTextMessageCount = 0
    private(set) var textMessage: String?
    private(set) var didDisplayPrivacyAlertCount = 0
    private(set) var didUpdateButtonStateCount = 0
    private(set) var isEnable: Bool?
    private(set) var didDisplayPaymentRequestErrorCount = 0
    private(set) var didStartLoadingCount = 0
    
    func displayProfile(with contact: PPContact, username: String) {
        didDisplayProfileCount += 1
        self.username = username
        self.contact = contact
    }
    
    func displayPrivacyAlert() {
        didDisplayPrivacyAlertCount += 1
    }
    
    func updateSendButtonState(isEnable: Bool) {
        didUpdateButtonStateCount += 1
        self.isEnable = isEnable
    }
    
    func showTextMessage(_ text: String) {
        didShowTextMessageCount += 1
        textMessage = text
    }
    
    func showError(with model: StatefulViewModeling) {
        didDisplayPaymentRequestErrorCount += 1
    }
    
    func startLoading() {
        didStartLoadingCount += 1
    }
}

final class PaymentRequestCheckoutPresenterTests: XCTestCase {
    // MARK: - Variables
    private lazy var coordinator = PaymentRequestCheckoutCoordinatorMock()
    private lazy var controller = PaymentRequestCheckoutControllerMock()
    private lazy var presenter: PaymentRequestCheckoutPresenter = {
        let paymentPresenter = PaymentRequestCheckoutPresenter(coordinator: coordinator)
        paymentPresenter.viewController = controller
        return paymentPresenter
    }()

    // MARK: - displayProfile
    func testDisplayProfile_ShouldCallDisplayProfileWithGivenData() throws {
        let username = "usertest"
        let contact = PPContact(profileDictionary: ["username": username])
        presenter.displayProfile(with: contact)
        let unwrappedContact = try XCTUnwrap(controller.contact)
        let unwrappedUsername = try XCTUnwrap(controller.username)
        XCTAssertEqual(controller.didDisplayProfileCount, 1)
        XCTAssertEqual(unwrappedContact, contact)
        XCTAssertEqual(unwrappedUsername, "@\(username)")
    }
    
    // MARK: - presentPrivacyAlert
    func testPresentPrivarcyAlert_ShouldCallDisplayPrivacyAlert() {
        presenter.presentPrivacyAlert()
        XCTAssertEqual(controller.didDisplayPrivacyAlertCount, 1)
    }
    
    // MARK: - showPlaceholderMessage
    func testShowTextMessage_WhenIsNotVisible_ShouldCallShowTextMessageAndTextMessageBeEmpty() throws {
        presenter.showPlaceholderMessage(isVisible: false)
        let unwrappedTextMessage = try XCTUnwrap(controller.textMessage)
        XCTAssertEqual(controller.didShowTextMessageCount, 1)
        XCTAssert(unwrappedTextMessage.isEmpty)
    }
    
    func testShowTextMessage_WhenIsVisible_ShouldCallShowTextMessageAndTextMessageBePlaceholder() throws {
        presenter.showPlaceholderMessage(isVisible: true)
        let unwrappedMessage = try XCTUnwrap(controller.textMessage)
        XCTAssertEqual(controller.didShowTextMessageCount, 1)
        XCTAssertEqual(unwrappedMessage, PaymentRequestLocalizable.messagePlaceholder.text)
    }
    
    // MARK: - popViewController
    func testPopViewController_ShouldCallPerformBack() throws {
        presenter.popViewController()
        let unwrappedAction = try XCTUnwrap(coordinator.action)
        XCTAssertEqual(coordinator.didPerformBackCount, 1)
        XCTAssertEqual(unwrappedAction, .back)
    }
    
    // MARK: - presentProfile
    func testPresentProfile_ShouldCallPerformShowProfileWithTheGivenData() throws {
        let username = "usertest"
        let contact = PPContact(profileDictionary: ["username": username])
        let profileImage = UIPPProfileImage(frame: .zero)
        presenter.presentProfile(profileImage: profileImage, contact: contact)
        let unwrappedContact = try XCTUnwrap(coordinator.contact)
        let unwrappedProfileImage = try XCTUnwrap(coordinator.profileImage)
        XCTAssertEqual(coordinator.didShowProfileCount, 1)
        XCTAssertEqual(unwrappedContact, contact)
        XCTAssertEqual(unwrappedProfileImage, profileImage)
    }
    
    // MARK: - presentPaymentRequestSuccess
    func testPaymentRequestSuccess_ShouldCallDisplayPaymentRequestSuccess() {
        presenter.presentPaymentRequestSuccess()
        XCTAssertEqual(coordinator.didDisplayPaymentRequestSuccessCount, 1)
    }
    
    // MARK: - dismissViewController
    func testDismissViewController_ShouldCallPerformActionClose() throws {
        presenter.dismissViewController()
        let unwrappedAction = try XCTUnwrap(coordinator.action)
        XCTAssertEqual(coordinator.didPerformBackCount, 1)
        XCTAssertEqual(unwrappedAction, .close)
    }
    
    // MARK: - startLoading
    func testStartLoading_ShouldCallLoading() {
        presenter.startLoading()
        XCTAssertEqual(controller.didStartLoadingCount, 1)
    }
    
    // MARK: - presentShouldFillValueAlert
    func testPresentShouldFillAlert_ShouldPresentAlert() {
        presenter.presentShouldFillValueAlert()
        XCTAssertEqual(coordinator.didPresentAlertCount, 1)
    }
    
    // MARK: - presentPaymentRequestError
    func testPresentPaymentRequestError_ShouldDisplayError() {
        presenter.presentPaymentRequestError()
        XCTAssertEqual(controller.didDisplayPaymentRequestErrorCount, 1)
    }
}
