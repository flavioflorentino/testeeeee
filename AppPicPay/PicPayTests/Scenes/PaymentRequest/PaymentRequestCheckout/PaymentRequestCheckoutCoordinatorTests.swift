import XCTest
@testable import UI
@testable import PicPay

final class PaymentRequestCheckoutCoordinatorTests: XCTestCase {
    // MARK: - Variables
    private let controller = ViewControllerMock()
    private lazy var navigationController = NavigationControllerMock(rootViewController: controller)
    private lazy var coordinator: PaymentRequestCheckoutCoordinator = {
        let coordinator = PaymentRequestCheckoutCoordinator()
        coordinator.viewController = navigationController.topViewController
        return coordinator
    }()
    
    // MARK: - perform
    func testPerformShouldCallPop() {
        coordinator.perform(action: .back)
        XCTAssert(navigationController.isPopViewControllerCalled)
    }
    
     // MARK: - performShowProfile
    func testPerformShowProfileShouldPushProfileController() {
        let username = "usertest"
        let contact = PPContact(profileDictionary: ["username": username])
        let profileImage = UIPPProfileImage(frame: .zero)
        coordinator.performShowProfile(profileImage: profileImage, contact: contact)
        XCTAssert(navigationController.pushedViewController is ConsumerProfileViewController)
    }
    
    func testPresentSuccessDialogShouldShowPresentSuccessAlertController() {
        coordinator.presentSuccessDialog()
        XCTAssertEqual(controller.didPresentControllerCount, 1)
        XCTAssert(controller.viewControllerPresented is StatusAlertViewController)
    }
    
    func testPresentAnAlertControllerShouldPresentAnAlertController() {
        let alertController = UIAlertController(title: "", message: "", preferredStyle: .alert)
        coordinator.present(alertController: alertController)
        XCTAssertEqual(controller.didPresentControllerCount, 1)
        XCTAssert(controller.viewControllerPresented is UIAlertController)
    }
}



