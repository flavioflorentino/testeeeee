import XCTest
import UI
@testable import PicPay

private final class PaymentRequestFeedDetailCoordinatorSpy: PaymentRequestFeedDetailCoordinating {
    var viewController: UIViewController?
    private(set) var performCallsCount = 0
    private(set) var action: PaymentRequestFeedDetailAction?
    
    func perform(action: PaymentRequestFeedDetailAction) {
        performCallsCount += 1
        self.action = action
    }
}

private final class PaymentRequestFeedDetailViewControllerSpy: PaymentRequestFeedDetailDisplay {
    private(set) var displayFeedImageCallsCount = 0
    private(set) var url: URL?
    private(set) var privacyImage: UIImage?
    private(set) var displayFeedTextCallsCount = 0
    private(set) var title: NSAttributedString?
    private(set) var message: NSAttributedString?
    private(set) var value: NSAttributedString?
    private(set) var date: NSAttributedString?
    private(set) var displayTableViewContentCallsCount = 0
    private(set) var sections: [Section<String, PaymentRequestFeedDetailViewController.CellType>]?
    
    func displayFeedImage(url: URL?, privacyImage: UIImage) {
        displayFeedImageCallsCount += 1
        self.url = url
        self.privacyImage = privacyImage
    }
    
    func displayFeedText(title: NSAttributedString, message: NSAttributedString, value: NSAttributedString, date: NSAttributedString) {
        displayFeedTextCallsCount += 1
        self.title = title
        self.message = message
        self.value = value
        self.date = date
    }
    
    func displayTableViewContent(_ sections: [Section<String, PaymentRequestFeedDetailViewController.CellType>]) {
        displayTableViewContentCallsCount += 1
        self.sections = sections
    }
}

final class PaymentRequestFeedDetailPresenterTests: XCTestCase {

    private let coordinator = PaymentRequestFeedDetailCoordinatorSpy()
    private let viewController = PaymentRequestFeedDetailViewControllerSpy()
    private lazy var presenter: PaymentRequestFeedDetailPresenter = {
        let presenter = PaymentRequestFeedDetailPresenter(
            coordinator: coordinator
        )
        presenter.viewController = viewController
        return presenter
    }()
    
    private func createDefaultFeedItem(title: String, message: String, money: String, timeAgo: String, isPrivate: Bool) throws -> DefaultFeedItem {
        let feedItem = DefaultFeedItem()
        feedItem.image = ImageFeedWidget(jsonDict: ["Url" : "https://s3.amazonaws.com/cdn.picpay.com/apps/picpay/imgs/p2p-charge-icon.png"])
        feedItem.title = try XCTUnwrap(TextLinkedFeedWidget(jsonDict: ["Value" : title]))
        feedItem.text = TextFeedWidget(jsonDict: ["Value" : message])
        feedItem.money = TextFeedWidget(jsonDict: ["Value" : money])
        feedItem.timeAgo = TextFeedWidget(jsonDict: ["Value" : timeAgo])
        feedItem.timeAgoIcon = IconFeedWidget(jsonDict: ["Type" : isPrivate ? "private" : "public"])
        return feedItem
    }
    
    private func createTransactions(paymentStatus: [Bool]) -> [PaymentRequestTransaction] {
        var transactions: [PaymentRequestTransaction] = []
        (0..<paymentStatus.count).forEach { index in
            transactions.append(PaymentRequestTransaction(
                p2pTransactionId: paymentStatus[index] ? index : nil,
                consumerPayerUsername: "username \(index)",
                consumerPayerSmallImageUrl: "https://s3.amazonaws.com/cdn.picpay.com/apps/picpay/imgs/p2p-charge-icon.png"
            ))
        }
        return transactions
    }
    
    // MARK: - presentFeedInfo
    func testPresentFeedInfo_WhenFeedItemIsPrivate_ShouldCallDisplayFeedImageAndDisplayFeedTextWithPresentableDataAndPrivateImage() throws {
        let feedItem = try createDefaultFeedItem(
            title: "<b>Você</b> cobrou a <b><lnk href='{\"action\": \"openProfile\", \"id\": 2}'>@jose</lnk></b> e mais uma pessoa",
            message: "🥺",
            money: "<font color='#6e6e6e'><b>R$ 1,00</b></font>",
            timeAgo: "22 horas atrás",
            isPrivate: true
        )
        presenter.presentFeedInfo(feedItem: feedItem)
        XCTAssertEqual(viewController.displayFeedImageCallsCount, 1)
        XCTAssertEqual(viewController.displayFeedTextCallsCount, 1)
        XCTAssertNotNil(viewController.url)
        XCTAssertEqual(viewController.privacyImage, Assets.Icons.icoPrivate.image)
        XCTAssertEqual(viewController.title?.string, "Você cobrou a @jose e mais uma pessoa")
        XCTAssertEqual(viewController.message?.string, "🥺")
        XCTAssertEqual(viewController.value?.string, "R$ 1,00")
        XCTAssertEqual(viewController.date?.string, "22 horas atrás")
    }
    
    func testPresentFeedInfo_WhenFeedItemIsPublic_ShouldCallDisplayFeedImageAndDisplayFeedTextWithPresentableDataAndPublicImage() throws {
        let feedItem = try createDefaultFeedItem(
            title: "<b>Você</b> cobrou a <b><lnk href='{\"action\": \"openProfile\", \"id\": 2}'>@jose</lnk></b> e mais uma pessoa",
            message: "🥺",
            money: "<font color='#6e6e6e'><b>R$ 1,00</b></font>",
            timeAgo: "22 horas atrás",
            isPrivate: false
        )
        presenter.presentFeedInfo(feedItem: feedItem)
        XCTAssertEqual(viewController.displayFeedImageCallsCount, 1)
        XCTAssertEqual(viewController.displayFeedTextCallsCount, 1)
        XCTAssertNotNil(viewController.url)
        XCTAssertEqual(viewController.privacyImage, Assets.Icons.icoPublic.image)
        XCTAssertEqual(viewController.title?.string, "Você cobrou a @jose e mais uma pessoa")
        XCTAssertEqual(viewController.message?.string, "🥺")
        XCTAssertEqual(viewController.value?.string, "R$ 1,00")
        XCTAssertEqual(viewController.date?.string, "22 horas atrás")
    }
    
    // MARK: - presentTransactions
    func testPresentTransactions_ShouldCallDisplayTableViewContent() throws {
        let transactions = createTransactions(paymentStatus: [true, false])
        presenter.presentTransactions(transactions)
        let firstTransaction = try XCTUnwrap(viewController.sections?.first?.items[0])
        let secondTransaction = try XCTUnwrap(viewController.sections?.first?.items[1])
        XCTAssertEqual(viewController.displayTableViewContentCallsCount, 1)
        
        if case let PaymentRequestFeedDetailViewController.CellType.default(transaction) = firstTransaction {
            XCTAssertEqual(transaction.username, "@username 0")
            XCTAssertNotNil(transaction.imageUrl)
            XCTAssertEqual(transaction.statusText, PaymentRequestLocalizable.paymentMade.text)
            XCTAssertFalse(transaction.isArrowHidden)
        } else {
            XCTFail("Wrong Cell Type")
        }
        
        if case let PaymentRequestFeedDetailViewController.CellType.default(transaction) = secondTransaction {
            XCTAssertEqual(transaction.username, "@username 1")
            XCTAssertNotNil(transaction.imageUrl)
            XCTAssertEqual(transaction.statusText, PaymentRequestLocalizable.pendingPayment.text)
            XCTAssertTrue(transaction.isArrowHidden)
        } else {
            XCTFail("Wrong Cell Type")
        }
    }
    
    // MARK: - presentLoader
    func testPresentLoader_ShouldCallDisplayTableViewContent() throws {
        presenter.presentLoader()
        let firstTransaction = try XCTUnwrap(viewController.sections?.first?.items[0])
        XCTAssertEqual(viewController.displayTableViewContentCallsCount, 1)
        guard case PaymentRequestFeedDetailViewController.CellType.loading = firstTransaction else {
            XCTFail("Wrong Cell Type")
            return
        }
    }
    
    // MARK: - presentGenericError
    func testPresentGenericError_ShouldCallDisplayTableViewContent() throws {
        presenter.presentGenericError()
        let firstTransaction = try XCTUnwrap(viewController.sections?.first?.items[0])
        XCTAssertEqual(viewController.displayTableViewContentCallsCount, 1)
        guard case PaymentRequestFeedDetailViewController.CellType.error(_, _) = firstTransaction else {
            XCTFail("Wrong Cell Type")
            return
        }
    }
}
