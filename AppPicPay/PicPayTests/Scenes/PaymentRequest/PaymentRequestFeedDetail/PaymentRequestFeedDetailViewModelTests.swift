import AnalyticsModule
import XCTest
@testable import PicPay

private final class PaymentRequestFeedDetailServiceSpy: PaymentRequestFeedDetailServicing {
    private(set) var paymentRequestStatusCallsCount = 0
    private(set) var paymentRequestFeedTransactionCallsCount = 0
    var result: Result<(model: [PaymentRequestTransaction], data: Data?), DialogError>?
    var resultFeed: Result<(model: DefaultFeedItem, data: Data?), DialogError>?
    
    func paymentRequestStatus(transactionId: String, completion: @escaping (Result<(model: [PaymentRequestTransaction], data: Data?), DialogError>) -> Void) {
        paymentRequestStatusCallsCount += 1
        guard let result = result else {
            return
        }
        completion(result)
    }
    
    func paymentRequestFeedTransaction(transactionId: String, completion: @escaping (Result<(model: DefaultFeedItem, data: Data?), DialogError>) -> Void) {
        paymentRequestFeedTransactionCallsCount += 1
        guard let result = resultFeed else {
            return
        }
        completion(result)
    }
}

private final class PaymentRequestFeedDetailPresenterSpy: PaymentRequestFeedDetailPresenting {
    var viewController: PaymentRequestFeedDetailDisplay?
    private(set) var didNextStepCallsCount = 0
    private(set) var action: PaymentRequestFeedDetailAction?
    private(set) var presentFeedInfoCallsCount = 0
    private(set) var feedItem: DefaultFeedItem?
    private(set) var presentTransactionsCallsCount = 0
    private(set) var transactions: [PaymentRequestTransaction]?
    private(set) var presentLoaderCallsCount = 0
    private(set) var presentGenericErrorCallsCount = 0
    
    func didNextStep(action: PaymentRequestFeedDetailAction) {
        didNextStepCallsCount += 1
        self.action = action
    }
    
    func presentFeedInfo(feedItem: DefaultFeedItem) {
        presentFeedInfoCallsCount += 1
        self.feedItem = feedItem
    }
    
    func presentTransactions(_ transactions: [PaymentRequestTransaction]) {
        presentTransactionsCallsCount += 1
        self.transactions = transactions
    }
    
    func presentLoader() {
        presentLoaderCallsCount += 1
    }
    
    func presentGenericError() {
        presentGenericErrorCallsCount += 1
    }
}

final class PaymentRequestFeedDetailViewModelTests: XCTestCase {
    // MARK: - Variables
    private let service = PaymentRequestFeedDetailServiceSpy()
    private let presenter = PaymentRequestFeedDetailPresenterSpy()
    private var feedItem = DefaultFeedItem()
    private let analytics = AnalyticsSpy()
    private lazy var viewModel: PaymentRequestFeedDetailViewModel = {
        return PaymentRequestFeedDetailViewModel(
            service: service,
            presenter: presenter,
            feedItem: feedItem,
            dependencies: DependencyContainerMock(analytics)
        )
    }()
    
    // MARK: - func callScreenAnalytics()
    func testCallScreenAnalytics_ShouldCallFeedItemViewedAnalytics() {
        viewModel.callScreenAnalytics()
        assertEvent(PaymentRequestUserEvent.feedItemViewed)
    }
    
    // MARK: - loadFeedData
    func testLoadFeedData_ShouldCallPresentFeedInfo() {
        viewModel.loadFeedData()
        XCTAssertEqual(presenter.presentFeedInfoCallsCount, 1)
        XCTAssertNotNil(presenter.feedItem)
    }
    
    // MARK: - loadTransactionStatus
    func testLoadTransactionStatus_WhenResultIsSuccess_ShouldCallPresentLoaderAndPresentTransactions() throws {
        let transactions = [PaymentRequestTransaction(p2pTransactionId: 0, consumerPayerUsername: "username", consumerPayerSmallImageUrl: "url")]
        service.result = .success((model: transactions, data: nil))
        viewModel.loadTransactionStatus()
        let finalTransactions = try XCTUnwrap(presenter.transactions)
        XCTAssertEqual(presenter.presentLoaderCallsCount, 1)
        XCTAssertEqual(service.paymentRequestStatusCallsCount, 1)
        XCTAssertEqual(presenter.presentTransactionsCallsCount, 1)
        XCTAssertEqual(finalTransactions.count, transactions.count)
        assertEvent(PaymentRequestUserEvent.paidDetail)
    }
    
    func testLoadTransactionStatus_WhenResultIsSuccess_ShouldCallPaidDetailAnalytics() {
        let transactions = [PaymentRequestTransaction(p2pTransactionId: 0, consumerPayerUsername: "username", consumerPayerSmallImageUrl: "url")]
        service.result = .success((model: transactions, data: nil))
        viewModel.loadTransactionStatus()
        assertEvent(PaymentRequestUserEvent.paidDetail)
    }
    
    func testLoadTransactionStatus_WhenResultIsFailure_ShouldCallPresentLoaderAndPresentGenricError() {
        service.result = .failure(.default(.unknown(nil)))
        viewModel.loadTransactionStatus()
        XCTAssertEqual(presenter.presentLoaderCallsCount, 1)
        XCTAssertEqual(service.paymentRequestStatusCallsCount, 1)
        XCTAssertEqual(presenter.presentGenericErrorCallsCount, 1)
    }
    
    // MARK: - loadTransactionDetail
    func testLoadTransactionDetail_WhenRowIsGreaterThanOrEqualToTransactionCount_ShouldDoNothing() {
        viewModel.loadTransactionDetail(at: IndexPath(row: 1, section: 0))
        XCTAssertEqual(presenter.presentLoaderCallsCount, 0)
        XCTAssertEqual(service.paymentRequestFeedTransactionCallsCount, 0)
    }
    
    func testLoadTransactionDetail_WhenRowIsLessThanTransactionCountAndTrasactionIsNotDetailable_ShouldDoNothing() {
        let transactions = [
            PaymentRequestTransaction(p2pTransactionId: nil, consumerPayerUsername: "username", consumerPayerSmallImageUrl: "url")
        ]
        service.result = .success((model: transactions, data: nil))
        viewModel.loadTransactionStatus()
        viewModel.loadTransactionDetail(at: IndexPath(row: 0, section: 0))
        XCTAssertEqual(presenter.presentLoaderCallsCount, 1)
        XCTAssertEqual(service.paymentRequestFeedTransactionCallsCount, 0)
    }
    
    func testLoadTransactionDetail_WhenRowIsLessThanTransactionCountAndTrasactionIsDetailableAndResultIsSuccess_ShouldCallLoadingAndServiceFeedDetailAndPresentTrasanctionsAndNextStep() throws {
        let transactions = [
            PaymentRequestTransaction(p2pTransactionId: 1, consumerPayerUsername: "username", consumerPayerSmallImageUrl: "url")
        ]
        let feedItem = try XCTUnwrap(DefaultFeedItem(jsonDict: [
            "Config": ["Title": ["Value": "TestTitle"]],
            "Type": "TestType",
            "UIComponent": "TestUIComponent"
        ]))
        service.result = .success((model: transactions, data: nil))
        service.resultFeed = .success((model: feedItem, data: nil))
        viewModel.loadTransactionStatus()
        viewModel.loadTransactionDetail(at: IndexPath(row: 0, section: 0))
        XCTAssertEqual(presenter.presentLoaderCallsCount, 2)
        XCTAssertEqual(service.paymentRequestFeedTransactionCallsCount, 1)
        XCTAssertEqual(presenter.didNextStepCallsCount, 1)
        XCTAssertEqual(presenter.presentTransactionsCallsCount, 2)
        XCTAssertNotNil(presenter.action)
    }
    
    func testLoadTransactionDetail_WhenRowIsLessThanTransactionCountAndTrasactionIsDetailableAndResultIsFailure_ShouldCallLoadingAndServiceFeedDetailAndGenericError() {
        let transactions = [
            PaymentRequestTransaction(p2pTransactionId: 1, consumerPayerUsername: "username", consumerPayerSmallImageUrl: "url")
        ]
        service.result = .success((model: transactions, data: nil))
        service.resultFeed = .failure(DialogError.default(.unknown(nil)))
        viewModel.loadTransactionStatus()
        viewModel.loadTransactionDetail(at: IndexPath(row: 0, section: 0))
        XCTAssertEqual(presenter.presentLoaderCallsCount, 2)
        XCTAssertEqual(service.paymentRequestFeedTransactionCallsCount, 1)
        XCTAssertEqual(presenter.presentGenericErrorCallsCount, 1)
    }
    
    private func assertEvent(_ event: AnalyticsKeyProtocol) {
        guard let loggedEvent = analytics.event else {
            return XCTFail("event logged is nil and it is not equal to \(event.event())")
        }
        XCTAssertTrue(analytics.equals(to: event.event()), "\(loggedEvent) it is not equal to \(event.event())")
    }
}
