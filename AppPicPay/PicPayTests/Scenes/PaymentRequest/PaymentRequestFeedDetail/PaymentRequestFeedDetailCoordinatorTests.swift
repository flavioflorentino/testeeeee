import XCTest
@testable import PicPay

final class PaymentRequestFeedDetailCoordinatorTests: XCTestCase {
    private let controller = UIViewController()
    private lazy var navigationController = NavigationControllerMock(rootViewController: controller)
    private lazy var coordinator: PaymentRequestFeedDetailCoordinator = {
        let coordinator = PaymentRequestFeedDetailCoordinator()
        coordinator.viewController = navigationController.topViewController
        return coordinator
    }()
    
    // MARK: - performShowDetail
    func testPerformShowDetail_ShouldPushFeedDetailViewController() throws {
        let feedItem = try XCTUnwrap(DefaultFeedItem(jsonDict: [
            "Config": ["Title": ["Value": "TestTitle"]],
            "Type": "TestType",
            "UIComponent": "TestUIComponent"
        ]))
        coordinator.perform(action: .showDetail(feedItem: feedItem))
        XCTAssertTrue(navigationController.isPushViewControllerCalled)
        XCTAssertTrue(navigationController.pushedViewController is FeedDetailViewController)
    }
}
