import XCTest
@testable import PicPay

private final class FakePaymentRequestQrCodeCoordinator: PaymentRequestQrCodeCoordinating {
    var viewController: UIViewController?
    
    private(set) var didCallPerformWithBackAction = 0
    private(set) var didCallPerformWithPresentAction = 0
    
    func perform(action: PaymentRequestQrCodeAction) {
        switch action {
        case .back:
            didCallPerformWithBackAction += 1
        case .present:
            didCallPerformWithPresentAction += 1
        }
    }
}

private final class FakePaymentRequestQrCodeViewController: PaymentRequestQrCodeDisplay {
    private(set) var didCallShowInformValueButtonCount = 0
    private(set) var didCallShowPaymentValueCount = 0
    private(set) var setUpPaymentCodeCallsCount = 0
    private(set) var displayLoaderCallsCount = 0
    private(set) var displayQrCodeSuccessCallsCount = 0
    private(set) var displayErrorCallsCount = 0
    private(set) var value: Double?
    
    func showInformValueButton() {
        didCallShowInformValueButtonCount += 1
    }
    
    func showPaymentValue(_ value: Double) {
        self.value = value
        didCallShowPaymentValueCount += 1
    }
    
    func setUpPaymentCode(paymentValue: Double?) {
        setUpPaymentCodeCallsCount += 1
    }
    
    func displayLoader() {
        displayLoaderCallsCount += 1
    }
    
    func displayQrCodeSuccess() {
        displayQrCodeSuccessCallsCount += 1
    }
    
    func displayError(icon: UIImage?, title: String, description: NSAttributedString, buttonTitle: String) {
        displayErrorCallsCount += 1
    }
}

final class PaymentRequestQrCodePresenterTests: XCTestCase {
    // MARK: - Variables
    private lazy var coordinator = FakePaymentRequestQrCodeCoordinator()
    private lazy var controller = FakePaymentRequestQrCodeViewController()
    private lazy var presenter: PaymentRequestQrCodePresenter = {
        let paymentPresenter = PaymentRequestQrCodePresenter(coordinator: coordinator)
        paymentPresenter.viewController = controller
        return paymentPresenter
    }()

    // MARK: - setUpPaymentCode
    func testSetUpPaymentCode_ShouldCallSetUpPaymentCode() {
        presenter.setUpPaymentCode(paymentValue: nil)
        XCTAssertEqual(controller.setUpPaymentCodeCallsCount, 1)
    }
    
    // MARK: - showInformValueButton
    func testShowInformValueButton_ShouldCallInformValueButton_WhenCalledByPresenter() {
        presenter.showInformValueButton()
        XCTAssertEqual(controller.didCallShowInformValueButtonCount, 1)
    }
    
    // MARK: - showPaymentValue
    func testShowPaymentValue_ShouldCallShowPaymentValueWithValue2_WhenValueIs2() {
        presenter.showPaymentValue(2.0)
        XCTAssertEqual(controller.didCallShowPaymentValueCount, 1)
        XCTAssertEqual(controller.value, 2.0)
    }
    
    // MARK: - perform
    func testPerform_ShouldCallPerformWithActionBack_WhenActionIsBack() {
        presenter.perform(action: .back)
        XCTAssertEqual(coordinator.didCallPerformWithBackAction, 1)
    }
    
    // MARK: - shareQrCode
    func testShareQrCode_ShouldCallPerformWithActionPresent_WhenActionIsPresent() {
        presenter.shareQrCode(withUsername: "", paymentValue: 1.0, qrcode: UIImage(), userImage: UIImage())
        XCTAssertEqual(coordinator.didCallPerformWithPresentAction, 1)
    }
    
    // MARK: - presentLoader
    func testPresentLoader_ShouldCalldisplayLoader() {
        presenter.presentLoader()
        XCTAssertEqual(controller.displayLoaderCallsCount, 1)
    }
    
    // MARK: - presentQrCodeSuccess
    func testPresentQrCodeSuccess_ShouldCallDisplayQrCodeSuccess() {
        presenter.presentQrCodeSuccess()
        XCTAssertEqual(controller.displayQrCodeSuccessCallsCount, 1)
    }
    
    // MARK: - presentError
    func testPresentError_ShouldCallDisplayError() {
        presenter.presentError()
        XCTAssertEqual(controller.displayErrorCallsCount, 1)
    }
}
