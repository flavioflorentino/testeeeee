@testable import PicPay
import XCTest

private final class FakePaymentRequestQrCodeService: PaymentRequestQrCodeServicing {
    var username: String?
}

private final class FakePaymentRequestQrCodePresenter: PaymentRequestQrCodePresenting {
    var viewController: PaymentRequestQrCodeDisplay?
    private(set) var didCallSetUpPaymentCodeCount = 0
    private(set) var didCallShowInformValueButtonCount = 0
    private(set) var didCallShowPaymentValueCount = 0
    private(set) var didCallPerformBackCount = 0
    private(set) var didCallShareQrCodeCount = 0
    private(set) var presentLoaderCallsCount = 0
    private(set) var presentQrCodeSuccessCallsCount = 0
    private(set) var presentErrorCallsCount = 0
    private(set) var username: String?
    private(set) var paymentValue: Double?
    private(set) var qrCodeImage: UIImage?
    private(set) var userImage: UIImage?
    
    func setUpPaymentCode(paymentValue: Double?) {
        didCallSetUpPaymentCodeCount += 1
        self.paymentValue = paymentValue
    }
    
    func showInformValueButton() {
        didCallShowInformValueButtonCount += 1
    }
    
    func showPaymentValue(_ value: Double) {
        didCallShowPaymentValueCount += 1
    }
    
    func perform(action: PaymentRequestQrCodeAction) {
        if case .back = action {
            didCallPerformBackCount += 1
        }
    }
    
    func shareQrCode(withUsername username: String, paymentValue: Double, qrcode: UIImage, userImage: UIImage) {
        didCallShareQrCodeCount += 1
        self.username = username
        self.paymentValue = paymentValue
        self.qrCodeImage = qrcode
        self.userImage = userImage
    }
    
    func presentLoader() {
        presentLoaderCallsCount += 1
    }
    
    func presentQrCodeSuccess() {
        presentQrCodeSuccessCallsCount += 1
    }
    
    func presentError() {
        presentErrorCallsCount += 1
    }
}

final class PaymentRequestQrCodeInteractorTests: XCTestCase {
    private var presenter = FakePaymentRequestQrCodePresenter()
    private var service = FakePaymentRequestQrCodeService()
    private lazy var interactor = PaymentRequestQrCodeInteractor(service: service, presenter: presenter, paymentValue: paymentValue)
    private var paymentValue: Double = .zero
    
    // MARK: - generatePaymentCode
    func testGeneratePaymentCode_WhenPaymentValueIsZero_ShouldCallSetUpPaymentCodeWithPaymentValueNil() {
        interactor.generatePaymentCode()
        XCTAssertEqual(presenter.didCallSetUpPaymentCodeCount, 1)
        XCTAssertNil(presenter.paymentValue)
    }
    
    func testGeneratePaymentCode_WhenPaymentValueIsNotZero_ShouldCallSetUpPaymentCodeWithPaymentValue() {
        paymentValue = 1.0
        interactor.generatePaymentCode()
        XCTAssertEqual(presenter.didCallSetUpPaymentCodeCount, 1)
        XCTAssertNotNil(presenter.paymentValue)
    }
    
    // MARK: - showPaymentView
    func testShowPaymentView_ShouldCallShowInformValueButton_WhenPaymentValueIs0() {
        interactor.showPaymentView()
        XCTAssertEqual(presenter.didCallShowInformValueButtonCount, 1)
    }
    
    func testShowPaymentView_ShouldCallShowPaymentValue_WhenPaymentValueIs1() {
        paymentValue = 1.0
        interactor.showPaymentView()
        XCTAssertEqual(presenter.didCallShowPaymentValueCount, 1)
    }
    
    // MARK: - back
    func testBack_ShouldCallPerformWithActionBack_WhenCalledByViewModel() {
        interactor.back()
        XCTAssertEqual(presenter.didCallPerformBackCount, 1)
    }
    
    // MARK: - sendCharge
    func testSendCharge_ShouldCallShowShareActionSheetWithUsernameFulanoAndPaymentValue1_WhenPassingTheSameParameters() {
        service.username = "fulano"
        paymentValue = 1.0
        let qrCodeImage = UIImage()
        let userImage = UIImage()
        interactor.sendCharge(withQrCode: qrCodeImage, userImage: userImage)
        XCTAssertEqual(presenter.didCallShareQrCodeCount, 1)
        XCTAssertEqual(try presenter.username.safe(), "fulano")
        XCTAssertEqual(try presenter.paymentValue.safe(), 1.0)
        XCTAssertEqual(try presenter.qrCodeImage.safe(), qrCodeImage)
        XCTAssertEqual(try presenter.userImage.safe(), userImage)
    }
    
    // MARK: - startRequest
    func testStartRequest_ShouldCallPresentLoader() {
        interactor.startRequest()
        XCTAssertEqual(presenter.presentLoaderCallsCount, 1)
    }
    
    // MARK: - finishRequest
    func testFinishRequest_WhenIsSuccessAndPaymentValueIsZero_ShouldCallShowInformValueButtonAndPresentQrCodeSuccess() {
        interactor.finishRequest(isSuccess: true)
        XCTAssertEqual(presenter.didCallShowInformValueButtonCount, 1)
        XCTAssertEqual(presenter.presentQrCodeSuccessCallsCount, 1)
        XCTAssertNil(presenter.paymentValue)
    }
    
    func testFinishRequest_WhenIsSuccessAndPaymentValueIsNotZero_ShouldCallShowPaymentValueAndPresentQrCodeSuccess() {
        paymentValue = 1.0
        interactor.finishRequest(isSuccess: true)
        XCTAssertEqual(presenter.didCallShowPaymentValueCount, 1)
        XCTAssertEqual(presenter.presentQrCodeSuccessCallsCount, 1)
    }
    
    func testFinishRequest_WhenIsNotSuccess_ShouldCallPresentError() {
        interactor.finishRequest(isSuccess: false)
        XCTAssertEqual(presenter.presentErrorCallsCount, 1)
    }
}
