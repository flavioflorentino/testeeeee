@testable import PicPay
import XCTest

final class PaymentRequestQrCodeCoordinatorTests: XCTestCase {
    private lazy var controller = ViewControllerMock()
    private lazy var navigationController = NavigationControllerMock(rootViewController: controller)
    private lazy var coordinator: PaymentRequestQrCodeCoordinator = {
        let coordinator = PaymentRequestQrCodeCoordinator()
        coordinator.viewController = navigationController.topViewController
        return coordinator
    }()
    
    func test_Perform_ShouldPopViewController_WhenActionIsBack() {
        coordinator.perform(action: .back)
        XCTAssert(navigationController.isPopViewControllerCalled)
    }
    
    func testPerform_ShouldPresentAController_WhenActionIsPresent() {
        let presentController = UIViewController()
        coordinator.perform(action: .present(presentController))
        XCTAssertEqual(controller.didPresentControllerCount, 1)
        XCTAssertEqual(controller.viewControllerPresented, presentController)
    }
}
