import XCTest
@testable import PicPay

final class PaymentRequestCheckoutEndpointTests: XCTestCase {
    // MARK: - checkout
    func testCheckout_ShouldBeCorrect() throws {
        let id = 1
        let value = 1.0
        let message = "message"
        let endpoint = PaymentRequestServiceEndpoint.checkout(idPayer: id, totalValue: value, message: message)
        let body = try XCTUnwrap(endpoint.body)
        let serializedObject = try JSONSerialization.jsonObject(with: body, options: .allowFragments)
        let dict = try XCTUnwrap(serializedObject as? [String: Any])
        let unwrappedId = try XCTUnwrap(dict["consumer_id_payer"] as? Int)
        let unwrappedTotalValue = try XCTUnwrap(dict["total_value"] as? Double)
        let unwrappedMessage = try XCTUnwrap(dict["message"] as? String)
        XCTAssertEqual(endpoint.path, "api/createP2PChargeTransaction.json")
        XCTAssertEqual(endpoint.method, .post)
        XCTAssertEqual(unwrappedId, id)
        XCTAssertEqual(unwrappedTotalValue, value)
        XCTAssertEqual(unwrappedMessage, message)
    }
}
