import FeatureFlag
import XCTest
@testable import PicPay

final class PaymentRequestSearchFactoryTests: XCTestCase {
    // MARK: - Variables
    private let featureManager = FeatureManagerMock()
    private lazy var factory = PaymentRequestSearchFactory(dependencies: DependencyContainerMock(featureManager))
    
    // MARK: - make
    func testMake_WhenPaymentRequestV2IsTrue_ShouldCreatePaymentRequestSearchViewController() {
        featureManager.override(key: .paymentRequestV2, with: true)
        let controller = factory.make(maxUsers: 5)
        XCTAssertTrue(controller is PaymentRequestSearchViewController)
    }
    
    func testMake_WhenPaymentRequestV2IsFalse_ShouldCreateSearchWrapper() {
        featureManager.override(key: .paymentRequestV2, with: false)
        let controller = factory.make()
        XCTAssertTrue(controller is SearchWrapper)
    }
}
