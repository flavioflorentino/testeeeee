import XCTest
import UI
@testable import PicPay

private final class PaymentRequestSearchCoordinatorSpy: PaymentRequestSearchCoordinating {
    var viewController: UIViewController?
    private(set) var performCallsCount = 0
    private(set) var action: PaymentRequestSearchAction?
    
    func perform(action: PaymentRequestSearchAction) {
        performCallsCount += 1
        self.action = action
    }
}

private final class PaymentRequestSearchViewControllerSpy: PaymentRequestSearchDisplay {
    private(set) var users: [UserListComponentModel]?
    private(set) var title: String?
    private(set) var displayUsersCallsCount = 0
    private(set) var displayEmptyStateWithMessageTitleCallsCount = 0
    private(set) var emptyStateMessage: String?
    private(set) var emptyStateTitle: String?
    private(set) var user: GroupedUser?
    private(set) var displaySelectionGrouperCallscount = 0
    private(set) var grouperUserId: String?
    private(set) var displayDeselectionGrouperCallsCount = 0
    private(set) var listUserId: String?
    private(set) var displayDeselectionListCallsCount = 0
    private(set) var blockedTitle: String?
    private(set) var blockedMessage: String?
    private(set) var blockedButtonTitle: String?
    private(set) var displayBlockedToSelectANewUserCallsCount = 0
    private(set) var displayLoadingCallsCount = 0
    private(set) var displayListLoadingCallsCount = 0
    private(set) var hideListLoadingCallsCount = 0
    private(set) var displayEnableForwardButtonCallsCount = 0
    private(set) var displayDisableForwardButtonCallsCount = 0
    private(set) var displayErrorWithCallsCount = 0
    private(set) var message: String?
    private(set) var buttonText: String?
    private(set) var displayRequestContactPermissionCallsCount = 0
    
    func displayUsers(_ users: [UserListComponentModel], title: String?) {
        displayUsersCallsCount += 1
        self.users = users
        self.title = title
    }
    
    func displayEmptyStateWithMessage(_ message: String, title: String?) {
        displayEmptyStateWithMessageTitleCallsCount += 1
        emptyStateMessage = message
        emptyStateTitle = title
    }
    
    func displaySelectionGrouper(user: GroupedUser) {
        displaySelectionGrouperCallscount += 1
        self.user = user
    }
    
    func displayDeselectionGrouper(userId: String) {
        displayDeselectionGrouperCallsCount += 1
        grouperUserId = userId
    }
    
    func displayDeselectionList(userId: String) {
        displayDeselectionListCallsCount += 1
        listUserId = userId
    }
    
    func displayBlockedToSelectANewUser(title: String, message: String, buttonTitle: String) {
        displayBlockedToSelectANewUserCallsCount += 1
        blockedTitle = title
        blockedMessage = message
        blockedButtonTitle = buttonTitle
    }
    
    func displayLoading() {
        displayLoadingCallsCount += 1
    }
    
    func displayListLoading() {
        displayListLoadingCallsCount += 1
    }
    
    func hideListLoading() {
        hideListLoadingCallsCount += 1
    }
    
    func displayEnableForwardButton() {
        displayEnableForwardButtonCallsCount += 1
    }
    
    func displayDisableForwardButton() {
        displayDisableForwardButtonCallsCount += 1
    }
    
    func displayErrorWith(message: String, buttonText: String) {
        displayErrorWithCallsCount += 1
        self.message = message
        self.buttonText = buttonText
    }
    
    func displayRequestContactPermission() {
        displayRequestContactPermissionCallsCount += 1
    }
}

final class PaymentRequestSearchPresenterTests: XCTestCase {
    // MARK: - Variables
    private let viewController = PaymentRequestSearchViewControllerSpy()
    private let coordinator = PaymentRequestSearchCoordinatorSpy()
    private lazy var presenter: PaymentRequestSearchPresenter = {
        let presenter = PaymentRequestSearchPresenter(coordinator: coordinator)
        presenter.viewController = viewController
        return presenter
    }()
    
    // MARK: - Mock
    private func createSearchUser(id: String, imageURL: URL?, paymentRequest: Bool) -> PaymentRequestSearchUser {
        let userConfig = PaymentRequestSearchUserConfig(paymentRequest: paymentRequest)
        let userData = PaymentRequestSearchUserData(
            username: "Username \(id)",
            name: "Name \(id)",
            id: id,
            config: userConfig,
            imageUrlSmall: imageURL
        )
        return PaymentRequestSearchUser(data: userData)
    }
    
    // MARK: - didNextStep
    func testDidNextStep_ShouldCallPerform() {
        presenter.didNextStep(action: .forward(users: []))
        XCTAssertEqual(coordinator.performCallsCount, 1)
        XCTAssertNotNil(coordinator.action)
    }
    
    // MARK: - presentUsers
    func testPresentUsers_ShouldCallDisplayUsersAndFormatUsername() throws {
        let user1 = createSearchUser(id: "1", imageURL: nil, paymentRequest: true)
        let user2 = createSearchUser(id: "2", imageURL: nil, paymentRequest: true)
        let users = [
            (user: user1, isSelected: true),
            (user: user2, isSelected: false)
        ]
        let title = "test"
        presenter.presentUsers(users, title: title)
        let finalUsers = try XCTUnwrap(viewController.users)
        let finalTitle = try XCTUnwrap(viewController.title)
        let firstFinalUser = try XCTUnwrap(finalUsers.first)
        XCTAssertEqual(viewController.displayUsersCallsCount, 1)
        XCTAssertEqual(users.count, finalUsers.count)
        XCTAssertEqual(firstFinalUser.username, "@Username 1")
        XCTAssertEqual(title, finalTitle)
    }
    
    // MARK: - presentEmptyStateWithMessageTitle
    func testPresentEmptyStateWithMessageTitle_ShouldCallDisplayEmptyStateWithMessageTitle() throws {
        let message = "Message"
        let title = "Title"
        presenter.presentEmptyStateWithMessage(message, title: title)
        
        let fakeMessage = try XCTUnwrap(viewController.emptyStateMessage)
        let fakeTitle = try XCTUnwrap(viewController.emptyStateTitle)
        
        XCTAssertEqual(viewController.displayEmptyStateWithMessageTitleCallsCount, 1)
        XCTAssertEqual(fakeMessage, message)
        XCTAssertEqual(fakeTitle, title)
    }
    
    // MARK: - presentSelectionGrouper
    func testPresentSelectionGrouper_ShouldCallDisplaySelectionGrouper() {
        let user = UserListComponentModel(
            id: "1",
            username: "Username",
            fullName: "Fullname",
            urlImage: nil,
            paymentRequestAllowed: true,
            isSelected: true
        )
        presenter.presentSelectionGrouper(user: user)
        XCTAssertEqual(viewController.displaySelectionGrouperCallscount, 1)
        XCTAssertNotNil(viewController.user)
    }
    
    // MARK: - presentDeselectionGrouper
    func testPresentDeselectionGrouper_ShouldCallDisplayDeselectionGrouper() throws {
        let userId = "1"
        presenter.presentDeselectionGrouper(userId: userId)
        let finalUserId = try XCTUnwrap(viewController.grouperUserId)
        XCTAssertEqual(viewController.displayDeselectionGrouperCallsCount, 1)
        XCTAssertEqual(finalUserId, userId)
    }
    
    // MARK: - presentDeselectionList
    func testPresentDeselectionList_ShouldCallDisplayDeselectionList() throws {
        let userId = "1"
        presenter.presentDeselectionList(userId: userId)
        let finalUserId = try XCTUnwrap(viewController.listUserId)
        XCTAssertEqual(viewController.displayDeselectionListCallsCount, 1)
        XCTAssertEqual(finalUserId, userId)
    }
    
    // MARK: - presentBlockedToSelectANewUser
    func testPresentBlockedToSelectANewUser_ShouldCallDisplayBlockedToSelectANewUser() {
        presenter.presentBlockedToSelectANewUser(maxUsers: 5)
        XCTAssertEqual(viewController.displayBlockedToSelectANewUserCallsCount, 1)
        XCTAssertNotNil(viewController.blockedTitle)
        XCTAssertNotNil(viewController.blockedMessage)
        XCTAssertNotNil(viewController.blockedButtonTitle)
    }
    
    // MARK: - presentLoading
    func testPresentLoading_ShouldCallDisplayLoading() {
        presenter.presentLoading()
        XCTAssertEqual(viewController.displayLoadingCallsCount, 1)
    }
    
    // MARK: - presentListLoading
    func testPresentListLoading_ShouldCallDisplayListLoading() {
        presenter.presentListLoading()
        XCTAssertEqual(viewController.displayListLoadingCallsCount, 1)
    }
    
    // MARK: - hideListLoading
    func testHideListLoading_ShouldCallHideListLoading() {
        presenter.hideListLoading()
        XCTAssertEqual(viewController.hideListLoadingCallsCount, 1)
    }
    
    // MARK: - presentEnableForwardButton
    func testPresentEnableForwardButton_ShouldCallDisplayEnableForwardButton() {
        presenter.presentEnableForwardButton()
        XCTAssertEqual(viewController.displayEnableForwardButtonCallsCount, 1)
    }
    
    // MARK: - presentDisableForwardButton
    func testPresentDisableForwardButton_ShouldCallDisplayDisableForwardButton() {
        presenter.presentDisableForwardButton()
        XCTAssertEqual(viewController.displayDisableForwardButtonCallsCount, 1)
    }
    
    // MARK: - presentError
    func testPresentError_ShouldCallDisplayErrorWithMessageAndButtonText() throws {
        presenter.presentError()
        let message = "Não foi possível realizar a busca"
        let buttonText = "Tentar Novamente"
        
        let fakeMessage = try XCTUnwrap(viewController.message)
        let fakeButtonText = try XCTUnwrap(viewController.buttonText)
        
        XCTAssertEqual(viewController.displayErrorWithCallsCount, 1)
        XCTAssertEqual(fakeMessage, message)
        XCTAssertEqual(fakeButtonText, buttonText)
    }
    
    // MARK: - presentRequestContactPermission
    func testPresentRequestContactPermission_ShouldCallDisplayRequestContactPermission() throws {
        presenter.presentRequestContactPermission()
        XCTAssertEqual(viewController.displayRequestContactPermissionCallsCount, 1)
    }
    
}
