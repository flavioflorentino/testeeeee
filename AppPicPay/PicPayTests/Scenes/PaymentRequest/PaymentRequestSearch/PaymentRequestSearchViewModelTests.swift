import XCTest
import Core
import UI
import AnalyticsModule
import PermissionsKit
@testable import PicPay

private class PaymentRequestSearchServiceMock: PaymentRequestSearchServicing {
    private(set) var searchUserNamedPageCompletionCallsCount = 0
    var searchUserResult: Result<(model: [PaymentRequestSearchResponse], data: Data?), ApiError>?
    
    private(set) var contactsPageCompletionCallsCount = 0
    var contactsResult: Result<(model: [PaymentRequestSearchResponse], data: Data?), ApiError>?

    func searchUser(
        named name: String,
        completion: @escaping (Result<(model: [PaymentRequestSearchResponse], data: Data?), ApiError>) -> Void
    ) -> URLSessionTask? {
        searchUserNamedPageCompletionCallsCount += 1
        guard let result = searchUserResult else {
            return nil
        }
        completion(result)
        return URLSessionTask()
    }
    
    func contacts(
        page: Int,
        completion: @escaping (Result<(model: [PaymentRequestSearchResponse], data: Data?), ApiError>) -> Void
    ) {
        contactsPageCompletionCallsCount += 1
        guard let result = contactsResult else {
            return
        }
        completion(result)
    }
}

private class PaymentRequestContactsPermissionMock: PaymentRequestContactsPermission {
    var permissionStatus: PermissionStatus?
    
    var status = PermissionStatus.authorized
    func requestContacts(completion: @escaping Permission.Callback) {
        guard let status = permissionStatus else {
            return
        }
        completion(status)
    }
}

private class ContactsManagerMock: ContactsProtocol {
    var status: IdentifyContactsStatus?
    
    func identifyPicpayContacts(completion: IdentifyContactsCompletion?) {
        guard let status = status else {
            return
        }
        completion?(status)
    }
    
    func userContacts() -> UserContacts? {
        return nil
    }
}

private class PaymentRequestSearchPresenterSpy: PaymentRequestSearchPresenting {
    var viewController: PaymentRequestSearchDisplay?

    private(set) var didNextStepActionCallsCount = 0
    private(set) var action: PaymentRequestSearchAction?
    private(set) var presentUsersTitleCallsCount = 0
    private(set) var users: [PaymentRequestSearchViewModel.BruteUser]?
    private(set) var title: String?
    private(set) var presentEmptyStateWithMessageTitleCallsCount = 0
    private(set) var emptyStateMessage: String?
    private(set) var emptyStateTitle: String?
    private(set) var presentSelectionGrouperUserCallsCount = 0
    private(set) var user: UserListComponentModel?
    private(set) var presentDeselectionGrouperUserIdCallsCount = 0
    private(set) var grouperUserId: String?
    private(set) var presentDeselectionListUserIdCallsCount = 0
    private(set) var listUserId: String?
    private(set) var presentBlockedToSelectANewUserCallsCount = 0
    private(set) var maxUsers: Int?
    private(set) var presentLoadingCallsCount = 0
    private(set) var presentListLoadingCallsCount = 0
    private(set) var hideListLoadingCallsCount = 0
    private(set) var presentEnableForwardButtonCallsCount = 0
    private(set) var presentDisableForwardButtonCallsCount = 0
    private(set) var presentErrorCallsCount = 0
    private(set) var presentRequestContactPermissionCallsCount = 0

    func didNextStep(action: PaymentRequestSearchAction) {
        didNextStepActionCallsCount += 1
        self.action = action
    }

    func presentUsers(_ users: [PaymentRequestSearchViewModel.BruteUser], title: String?) {
        presentUsersTitleCallsCount += 1
        self.users = users
        self.title = title
    }
    
    func presentEmptyStateWithMessage(_ message: String, title: String?) {
        presentEmptyStateWithMessageTitleCallsCount += 1
        self.emptyStateMessage = message
        self.emptyStateTitle = title
    }

    func presentSelectionGrouper(user: UserListComponentModel) {
        presentSelectionGrouperUserCallsCount += 1
        self.user = user
    }

    func presentDeselectionGrouper(userId: String) {
        presentDeselectionGrouperUserIdCallsCount += 1
        grouperUserId = userId
    }

    func presentDeselectionList(userId: String) {
        presentDeselectionListUserIdCallsCount += 1
        listUserId = userId
    }
    
    func presentBlockedToSelectANewUser(maxUsers: Int) {
        presentBlockedToSelectANewUserCallsCount += 1
        self.maxUsers = maxUsers
    }
    
    func presentLoading() {
        presentLoadingCallsCount += 1
    }
    
    func presentListLoading() {
        presentListLoadingCallsCount += 1
    }
    
    func hideListLoading() {
        hideListLoadingCallsCount += 1
    }
    
    func presentEnableForwardButton() {
        presentEnableForwardButtonCallsCount += 1
    }
    
    func presentDisableForwardButton() {
        presentDisableForwardButtonCallsCount += 1
    }
    
    func presentError() {
        presentErrorCallsCount += 1
    }
    
    func presentRequestContactPermission() {
        presentRequestContactPermissionCallsCount += 1
    }
}

final class PaymentRequestSearchViewModelTests: XCTestCase {
    typealias User = (id: String, imageURL: URL?, paymentRequest: Bool)
    // MARK: - Variables
    private let service = PaymentRequestSearchServiceMock()
    private let presenter = PaymentRequestSearchPresenterSpy()
    private let contactsManager = ContactsManagerMock()
    private let contactsPermission = PaymentRequestContactsPermissionMock()
    private let queue = DispatchQueue(label: "paymentRequestSearchViewModelTests")
    private let analytics = AnalyticsSpy()
    private lazy var viewModel = PaymentRequestSearchViewModel(service: service, presenter: presenter, timerType: TimerMock.self, contactsManager: contactsManager, contactsPermission: contactsPermission, dependencies: DependencyContainerMock(queue, analytics))
    
    // MARK: - Mock
    private func createSearchUser(id: String, imageURL: URL?, paymentRequest: Bool) -> PaymentRequestSearchUser {
        let userConfig = PaymentRequestSearchUserConfig(paymentRequest: paymentRequest)
        let userData = PaymentRequestSearchUserData(
            username: "Username \(id)",
            name: "Name \(id)",
            id: id,
            config: userConfig,
            imageUrlSmall: imageURL
        )
        return PaymentRequestSearchUser(data: userData)
    }
    
    private func createSearchResponse(users: [User]) -> PaymentRequestSearchResponse {
        var rows: [PaymentRequestSearchUser] = []
        for user in users {
            rows.append(createSearchUser(id: user.id, imageURL: user.imageURL, paymentRequest: user.paymentRequest))
        }
        return PaymentRequestSearchResponse(rows: rows)
    }
    
    private func createUserListModel(id: String, urlImage: URL? = nil, paymentRequest: Bool = true, isSelected: Bool) -> UserListComponentModel {
        UserListComponentModel(
            id: id,
            username: "username \(id)",
            fullName: "fullname \(id)",
            urlImage: urlImage,
            paymentRequestAllowed: paymentRequest,
            isSelected: isSelected
        )
    }
    
    private func createGroupedUser(id: String, urlImage: URL? = nil) -> GroupedUser {
        GroupedUser(id: id, imageUrl: urlImage, name: "name \(id)")
    }
    
    // MARK: - requestContactsPermission
    
    func testRequestContactsPermission_WhenContactsPermissionIsDenied_ShouldNotCallPresentUsersOrPresentError() throws {
        contactsPermission.permissionStatus = .denied
        contactsManager.status = .usedContactsFromCache
        
        viewModel.requestContactsPermission()
        
        XCTAssertEqual(presenter.presentUsersTitleCallsCount, 0)
        XCTAssertEqual(presenter.presentErrorCallsCount, 0)
    }
    
    func testRequestContactsPermission_WhenContactsPermissionIsAuthorizedAndContactsManagerUseContactsFromCacheAndServiceReturnUsers_ShouldCallPresentUsersTitle() throws {
        contactsPermission.permissionStatus = .authorized
        contactsManager.status = .usedContactsFromCache
        let users: [User] = [
            (id: "1", imageURL: nil, paymentRequest: true),
            (id: "2", imageURL: nil, paymentRequest: true)
        ]
        service.contactsResult = .success((model: [createSearchResponse(users: users)], data: nil))
        
        viewModel.requestContactsPermission()
        
        let expectation = XCTestExpectation(description: "PaymentRequestSearchViewModelExpectation")
        queue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
        
        XCTAssertEqual(presenter.presentUsersTitleCallsCount, 1)
    }
    
    func testRequestContactsPermission_WhenContactsPermissionIsAuthorizedAndContactsManagerReturnError_ShouldCallPresentError() throws {
        contactsPermission.permissionStatus = .authorized
        contactsManager.status = .serviceError
        
        viewModel.requestContactsPermission()
        
        let expectation = XCTestExpectation(description: "PaymentRequestSearchViewModelExpectation")
        queue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
       
        XCTAssertEqual(presenter.presentErrorCallsCount, 1)
    }
    
    // MARK: - searchUser
    func testSearchUser_WhenNameCountIs2AndContactsPermissionIsAuthorized_ShouldCallServiceContactsAndPresentUsersAndUsersWithSelectionBeSelected() throws {
        contactsPermission.permissionStatus = .authorized
        contactsManager.status = .usedContactsFromCache
        let users: [User] = [
            (id: "1", imageURL: nil, paymentRequest: true),
            (id: "2", imageURL: nil, paymentRequest: true)
        ]
        let selectedUser = createUserListModel(id: "1", isSelected: true)
        service.contactsResult = .success((model: [createSearchResponse(users: users)], data: nil))
        viewModel.addSelectionGrouper(user: selectedUser)
        viewModel.searchUser(named: "Te", useDelayIfNeeded: true)
        
        let expectation = XCTestExpectation(description: "PaymentRequestSearchViewModelExpectation")
        queue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
        
        let fakeUsers = try XCTUnwrap(presenter.users)
        let fakeTitle = try XCTUnwrap(presenter.title)
        XCTAssertEqual(presenter.presentUsersTitleCallsCount, 1)
        XCTAssertEqual(fakeTitle, "CONTATOS")
        XCTAssertEqual(fakeUsers.count, users.count)
        XCTAssertTrue(fakeUsers[0].isSelected)
        XCTAssertFalse(fakeUsers[1].isSelected)
    }
    
    func testSearchUser_WhenNameCountIs2AndCallingTwiceAndContactsPermissionIsAuthorized_ShouldCallServiceContactsOnlyOnceAndPresentUsersTwiceAndUsersWithSelectionBeSelected() throws {
        contactsPermission.permissionStatus = .authorized
        contactsManager.status = .usedContactsFromCache
        let users: [User] = [
            (id: "1", imageURL: nil, paymentRequest: true),
            (id: "2", imageURL: nil, paymentRequest: true)
        ]
        let selectedUser = createUserListModel(id: "1", isSelected: true)
        service.contactsResult = .success((model: [createSearchResponse(users: users)], data: nil))
        viewModel.addSelectionGrouper(user: selectedUser)
        viewModel.searchUser(named: "Te", useDelayIfNeeded: true)
        
        let expectation = XCTestExpectation(description: "PaymentRequestSearchViewModelExpectation")
        queue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
        
        viewModel.searchUser(named: "T", useDelayIfNeeded: true)
        
        let fakeUsers = try XCTUnwrap(presenter.users)
        
        XCTAssertEqual(presenter.presentUsersTitleCallsCount, 2)
        XCTAssertEqual(fakeUsers.count, users.count)
        XCTAssertTrue(fakeUsers[0].isSelected)
        XCTAssertFalse(fakeUsers[1].isSelected)
        XCTAssertEqual(service.contactsPageCompletionCallsCount, 1)
    }
    
    func testSearchUser_WhenNameCountGreaterThan2_ShouldCallServiceSearchUserAndPresentUsersAndUsersWithSelectionBeSelected() throws {
        let users: [User] = [
            (id: "1", imageURL: nil, paymentRequest: true),
            (id: "2", imageURL: nil, paymentRequest: true)
        ]
        let selectedUser = createUserListModel(id: "1", isSelected: true)
        service.searchUserResult = .success((model: [createSearchResponse(users: users)], data: nil))
        viewModel.addSelectionGrouper(user: selectedUser)
        viewModel.searchUser(named: "Teste", useDelayIfNeeded: true)
        TimerMock.currentTimer?.fire()
        
        let finalUsers = try XCTUnwrap(presenter.users)
        let finalTitle = try XCTUnwrap(presenter.title)
        
        XCTAssertEqual(service.searchUserNamedPageCompletionCallsCount, 1)
        XCTAssertEqual(presenter.presentLoadingCallsCount, 1)
        XCTAssertEqual(presenter.presentUsersTitleCallsCount, 1)
        XCTAssertEqual(finalTitle, "RESULTADOS")
        XCTAssertEqual(finalUsers.count, users.count)
        XCTAssertTrue(finalUsers[0].isSelected)
        XCTAssertFalse(finalUsers[1].isSelected)
    }
    
    func testSearchUser_WhenNameCountGreaterThan2AndServerReturnFailure_ShouldCallPresentError() {
        service.searchUserResult = .failure(.serverError)
        viewModel.searchUser(named: "Title", useDelayIfNeeded: true)
        TimerMock.currentTimer?.fire()
        XCTAssertEqual(presenter.presentErrorCallsCount, 1)
    }
    
    func testSearchUser_WhenNameCountIs2AndServerReturnFailureAndContactsPermissionIsAuthorized_ShouldCallPresentError() {
        contactsPermission.permissionStatus = .authorized
        contactsManager.status = .usedContactsFromCache
        service.contactsResult = .failure(.serverError)
        viewModel.searchUser(named: "Ti", useDelayIfNeeded: true)
        
        let expectation = XCTestExpectation(description: "PaymentRequestSearchViewModelExpectation")
        queue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
        
        XCTAssertEqual(self.presenter.presentErrorCallsCount, 1)
    }
    
    func testSearchUser_WhenNameCountIs2AndContactsPermissionIsAuthorized_ShouldCallHideListLoading() {
        contactsPermission.permissionStatus = .authorized
        contactsManager.status = .usedContactsFromCache
        service.contactsResult = .failure(.serverError)
        viewModel.searchUser(named: "Ti", useDelayIfNeeded: true)
        
        let expectation = XCTestExpectation(description: "PaymentRequestSearchViewModelExpectation")
        queue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
        
        XCTAssertEqual(self.presenter.hideListLoadingCallsCount, 1)
    }
    
    func testSearchUserNamed_WhenNameCountIs2AndServiceReturnNoContactsAndContactsPermissionIsAuthorized_ShouldCallPresentEmptyStateWithMessage() throws {
        contactsPermission.permissionStatus = .authorized
        contactsManager.status = .usedContactsFromCache
        service.contactsResult = .success((model: [createSearchResponse(users: [])], data: nil))
        viewModel.searchUser(named: "Te", useDelayIfNeeded: true)
        
        let expectation = XCTestExpectation(description: "PaymentRequestSearchViewModelExpectation")
        queue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
        
        let fakeMessage = try XCTUnwrap(presenter.emptyStateMessage)
        let fakeTitle = try XCTUnwrap(presenter.emptyStateTitle)
        
        XCTAssertEqual(presenter.presentEmptyStateWithMessageTitleCallsCount, 1)
        XCTAssertEqual(fakeTitle, "CONTATOS")
        XCTAssertEqual(fakeMessage, "Nenhum contato encontrado")
    }
    
    func testSearchUserNamed_WhenNameCountIs2AndServiceReturnNoContactsAndCallingTwiceAndContactsPermissionIsAuthorized_ShouldCallPresentEmptyStateWithMessageService() throws {
        contactsPermission.permissionStatus = .authorized
        contactsManager.status = .usedContactsFromCache
        service.contactsResult = .success((model: [createSearchResponse(users: [])], data: nil))
        viewModel.searchUser(named: "T", useDelayIfNeeded: true)
        viewModel.searchUser(named: "Te", useDelayIfNeeded: true)
        
        let expectation = XCTestExpectation(description: "PaymentRequestSearchViewModelExpectation")
        queue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
    
        XCTAssertEqual(presenter.presentEmptyStateWithMessageTitleCallsCount, 1)
        XCTAssertEqual(service.contactsPageCompletionCallsCount, 1)
    }
    
    func testSearchUserNamed_WhenSearchForNamesWithThreeAndTwoLettersAndServiceReturnEmpty_ShouldCallInvalidate() throws {
        contactsPermission.permissionStatus = .authorized
        contactsManager.status = .usedContactsFromCache
        service.contactsResult = .success((model: [createSearchResponse(users: [])], data: nil))
        viewModel.searchUser(named: "Tes", useDelayIfNeeded: true)
        TimerMock.currentTimer?.fire()
        viewModel.searchUser(named: "Te", useDelayIfNeeded: true)
        
        let expectation = XCTestExpectation(description: "PaymentRequestSearchViewModelExpectation")
        queue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
    
        let timer = try XCTUnwrap(TimerMock.currentTimer)
        XCTAssertEqual(timer.invalidateCallCount, 1)
    }
    
    func testSearchUserNamed_WhenNameCountGreaterThan2AndServiceReturnNoContacts_ShouldCallPresentEmptyStateWithMessage() throws {
        service.searchUserResult = .success((model: [createSearchResponse(users: [])], data: nil))
        viewModel.searchUser(named: "Teste", useDelayIfNeeded: true)
        TimerMock.currentTimer?.fire()
        
        let fakeMessage = try XCTUnwrap(presenter.emptyStateMessage)
        
        XCTAssertEqual(presenter.presentEmptyStateWithMessageTitleCallsCount, 1)
        XCTAssertNil(presenter.emptyStateTitle)
        XCTAssertEqual(fakeMessage, "Nenhum resultado encontrado")
    }
    
    func testSearchUser_WhenNameCountIs2AndContactsPermissionIsDenied_ShouldCallPresentRequestContactPermission() throws {
        contactsPermission.status = .denied
        
        viewModel.searchUser(named: "Te", useDelayIfNeeded: true)
        
        XCTAssertEqual(presenter.presentRequestContactPermissionCallsCount, 1)
        
    }
    
    // MARK: - searchMore
    func testSearchMore_WhenLastSearchWasWithCountEqual2AndContactsPermissionIsAuthorized_ShouldCallPresentListLoading() {
        contactsPermission.permissionStatus = .authorized
        contactsManager.status = .usedContactsFromCache
        let users: [User] = [
            (id: "1", imageURL: nil, paymentRequest: true),
            (id: "2", imageURL: nil, paymentRequest: true)
        ]
        service.contactsResult = .success((model: [createSearchResponse(users: users)], data: nil))
        viewModel.searchUser(named: "Ab", useDelayIfNeeded: true)
        
        let expectation = XCTestExpectation(description: "PaymentRequestSearchViewModelExpectation")
        queue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
        
        viewModel.searchMore()
        XCTAssertEqual(presenter.presentListLoadingCallsCount, 1)
    }
    
    func testSearchMore_WhenLastSearchWasWithCountEqual3_ShouldNotCallPresentListLoading() {
        let users: [User] = [
            (id: "1", imageURL: nil, paymentRequest: true),
            (id: "2", imageURL: nil, paymentRequest: true)
        ]
        service.contactsResult = .success((model: [createSearchResponse(users: users)], data: nil))
        viewModel.searchUser(named: "Abc", useDelayIfNeeded: true)
        
        viewModel.searchMore()
        
        XCTAssertEqual(presenter.presentListLoadingCallsCount, 0)
    }
    
    func testSearchMore_WhenNameCountGreaterThan2AndServiceReturnedFirstOneContactAndLaterNoContact_ShouldNotCallEmptyState() throws {
        let user: [User] = [
            (id: "1", imageURL: nil, paymentRequest: true)
        ]
        service.contactsResult = .success((model: [createSearchResponse(users: user)], data: nil))
        viewModel.searchUser(named: "Teste", useDelayIfNeeded: true)
        
        service.contactsResult = .success((model: [createSearchResponse(users: [])], data: nil))
        viewModel.searchMore()
        
        XCTAssertEqual(presenter.presentEmptyStateWithMessageTitleCallsCount, 0)
    }
    
    func testSearchMore_WhenNameCountIs2AndServiceReturnsEmptyAndContactsPermissionIsAuthorized_ShouldCallServiceOnlyOnce() throws {
        contactsPermission.permissionStatus = .authorized
        contactsManager.status = .usedContactsFromCache
        service.contactsResult = .success((model: [createSearchResponse(users: [])], data: nil))
        viewModel.searchUser(named: "Te", useDelayIfNeeded: true)
        
        let expectation = XCTestExpectation(description: "PaymentRequestSearchViewModelExpectation")
        queue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
        
        viewModel.searchMore()
        
        XCTAssertEqual(self.service.contactsPageCompletionCallsCount, 1)
    }
    
    func testSearchMore_WhenNameCountIs2AndServerReturnFailureAndFirstRequestWasSuccess_ShouldNotCallPresentError() {
        contactsPermission.permissionStatus = .authorized
        contactsManager.status = .usedContactsFromCache
        let user: [User] = [
            (id: "1", imageURL: nil, paymentRequest: true)
        ]
        service.contactsResult = .success((model: [createSearchResponse(users: user)], data: nil))
        viewModel.searchUser(named: "Ti", useDelayIfNeeded: true)
        
        let expectation = XCTestExpectation(description: "PaymentRequestSearchViewModelExpectation")
        queue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
        
        service.contactsResult = .failure(.serverError)
        viewModel.searchMore()
        
        XCTAssertEqual(presenter.presentErrorCallsCount, 0)
    }
    
    func testSearchMore_WhenNameCountIs2AndServerReturnNoContactsAndFirstRequestWasSuccess_ShouldNotCallPresentError() {
        contactsPermission.permissionStatus = .authorized
        contactsManager.status = .usedContactsFromCache
        let user: [User] = [
            (id: "1", imageURL: nil, paymentRequest: true)
        ]
        service.contactsResult = .success((model: [createSearchResponse(users: user)], data: nil))
        viewModel.searchUser(named: "Ti", useDelayIfNeeded: true)
        
        let expectation = XCTestExpectation(description: "PaymentRequestSearchViewModelExpectation")
        queue.async {
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
        
        service.contactsResult = .success((model: [createSearchResponse(users: [])], data: nil))
        viewModel.searchMore()
        
        XCTAssertEqual(presenter.presentEmptyStateWithMessageTitleCallsCount, 0)
    }
    
    // MARK: - addSelectionGrouper
    func testAddSelectionGrouper_WhenHasNoUserSelected_ShouldCallPresentSelectionGrouperAndPresentEnableForwardButton() {
        let selectedUser = createUserListModel(id: "1", isSelected: true)
        viewModel.addSelectionGrouper(user: selectedUser)
        XCTAssertEqual(presenter.presentSelectionGrouperUserCallsCount, 1)
        XCTAssertEqual(presenter.presentEnableForwardButtonCallsCount, 1)
        XCTAssertNotNil(presenter.user)
    }
    
    func testAddSelectionGrouper_WhenHasAtLeastOneUserSelected_ShouldCallPresentSelectionGrouperTwoTimesAndPresentEnableForwardButtonOneTime() {
        let preSelectedUser = createUserListModel(id: "1", isSelected: true)
        let selectedUser = createUserListModel(id: "2", isSelected: true)
        viewModel.addSelectionGrouper(user: preSelectedUser)
        viewModel.addSelectionGrouper(user: selectedUser)
        XCTAssertEqual(presenter.presentSelectionGrouperUserCallsCount, 2)
        XCTAssertEqual(presenter.presentEnableForwardButtonCallsCount, 1)
        XCTAssertNotNil(presenter.user)
    }
    
    // MARK: - removeSelectionGrouper
    func testRemoveSelectionGrouper_WhenAfterRemovingHasNoUserSelected_ShouldCallPresentDeselectionGrouperAndPresentDisableForwardButton() throws {
        let id = "1"
        let selectedUser = createUserListModel(id: id, isSelected: true)
        viewModel.removeSelectionGrouper(user: selectedUser)
        let finalUserID = try XCTUnwrap(presenter.grouperUserId)
        XCTAssertEqual(presenter.presentDeselectionGrouperUserIdCallsCount, 1)
        XCTAssertEqual(presenter.presentDisableForwardButtonCallsCount, 1)
        XCTAssertEqual(finalUserID, id)
    }
    
    func testRemoveSelectionGrouper_WhenAfterRemovingHasAtLeastOneUserSelected_ShouldCallPresentDeselectionGrouperTwoTimesAndPresentDisableForwardButtonOneTime() throws {
        let id = "2"
        let preSelectedUser = createUserListModel(id: "1", isSelected: true)
        let selectedUser = createUserListModel(id: id, isSelected: true)
        viewModel.addSelectionGrouper(user: preSelectedUser)
        viewModel.addSelectionGrouper(user: selectedUser)
        viewModel.removeSelectionGrouper(user: preSelectedUser)
        viewModel.removeSelectionGrouper(user: selectedUser)
        let finalUserID = try XCTUnwrap(presenter.grouperUserId)
        XCTAssertEqual(presenter.presentDeselectionGrouperUserIdCallsCount, 2)
        XCTAssertEqual(presenter.presentDisableForwardButtonCallsCount, 1)
        XCTAssertEqual(finalUserID, id)
    }
    
    // MARK: - removeSelectionList
    func testRemoveSelectionList_WhenAfterRemovingHasNoUserSelected_ShouldCallPresentDeselectionListAndPresentDisableForwardButton() throws {
        let id = "1"
        let user = createGroupedUser(id: id)
        viewModel.removeSelectionList(user: user)
        let finalUserID = try XCTUnwrap(presenter.listUserId)
        XCTAssertEqual(presenter.presentDeselectionListUserIdCallsCount, 1)
        XCTAssertEqual(presenter.presentDisableForwardButtonCallsCount, 1)
        XCTAssertEqual(finalUserID, id)
    }
    
    func testRemoveSelectionList_WhenAfterRemovingHasAtLeastOneUserSelected_ShouldCallPresentDeselectionListTwoTimesAndPresentDisableForwardButtonOneTime() throws {
        let id = "2"
        let preSelectedUser = createUserListModel(id: "1", isSelected: true)
        let selectedUser = createUserListModel(id: id, isSelected: true)
        let preSelectedGroupedUser = createGroupedUser(id: "1")
        let user = createGroupedUser(id: id)
        viewModel.addSelectionGrouper(user: preSelectedUser)
        viewModel.addSelectionGrouper(user: selectedUser)
        viewModel.removeSelectionList(user: preSelectedGroupedUser)
        viewModel.removeSelectionList(user: user)
        let finalUserID = try XCTUnwrap(presenter.listUserId)
        XCTAssertEqual(presenter.presentDeselectionListUserIdCallsCount, 2)
        XCTAssertEqual(presenter.presentDisableForwardButtonCallsCount, 1)
        XCTAssertEqual(finalUserID, id)
    }
    
    // MARK: - blockedToSelectANewUser
    func testBlockedToSelectANewUser_ShouldCallBlockedToSelectANewUser() {
        let maxUsers = 5
        viewModel.blockedToSelectANewUser(maxUsers: maxUsers)
        XCTAssertEqual(presenter.presentBlockedToSelectANewUserCallsCount, 1)
        XCTAssertEqual(presenter.maxUsers, maxUsers)
    }
    
    // MARK: - forward
    func testForward_WhenHasNoUserSelected_ShouldNotCallDidNextStep() {
        viewModel.forward()
        XCTAssertEqual(presenter.didNextStepActionCallsCount, 0)
    }
    
    func testForward_WhenHasAtLeastOneUserSelected_ShouldCallDidNextStep() {
        let id = "1"
        let user = createUserListModel(id: id, isSelected: true)
        viewModel.addSelectionGrouper(user: user)
        viewModel.forward()
        XCTAssertEqual(presenter.didNextStepActionCallsCount, 1)
        
        assertEvent(PaymentRequestUserEvent.selectedUsers(ids: [id]))
    }
    
    // MARK: - registerDidOpenSearchScreenAnalytics
    func testRegisterDidOpenSearchScreenAnalytics_ShouldCallAnalytics() {
        viewModel.registerDidOpenSearchScreenAnalytics()
        assertEvent(PaymentRequestUserEvent.didOpenSearchScreen)
    }
    
    // MARK: - registerDidTapSearchField
    func testRegisterDidTapSearchField_ShouldCallAnalytics() {
        viewModel.registerDidTapSearchField()
        assertEvent(PaymentRequestUserEvent.didTapSearchField)
    }
    
    private func assertEvent(_ event: AnalyticsKeyProtocol) {
        guard let loggedEvent = analytics.event else {
            return XCTFail("event logged is nil and it is not equal to \(event.event())")
        }
        XCTAssertTrue(analytics.equals(to: event.event()), "\(loggedEvent) it is not equal to \(event.event())")
    }
}
