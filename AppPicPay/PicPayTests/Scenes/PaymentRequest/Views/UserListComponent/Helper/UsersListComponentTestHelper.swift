import XCTest
@testable import PicPay

final class UsersListComponentTestHelper {
    func createUser(
        id: String,
        paymentRequestAllowed: Bool = true,
        isSelected: Bool = false
    ) -> UserListComponentModel {
        return UserListComponentModel(
            id: id,
            username: "teste\(id)",
            fullName: "full name\(id)",
            urlImage: URL(string: "teste\(id)"),
            paymentRequestAllowed: paymentRequestAllowed,
            isSelected: isSelected
        )
    }
}
