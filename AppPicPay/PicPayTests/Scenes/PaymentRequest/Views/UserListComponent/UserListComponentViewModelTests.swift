
import XCTest
@testable import PicPay

private final class UserListComponentPresenterSpy: UsersListComponentPresenting {
    var view: UsersListComponentDisplay?
    
    private(set) var displayUsersCallCount = 0
    private(set) var users: [UserListComponentModel]?
    private(set) var presentEmptyStateCallCount = 0
    private(set) var emptyStateMessage: String?
    private(set) var emptyStateTitle: String?
    private(set) var title: String?
    private(set) var checkSelectedCallCount = 0
    private(set) var rows: [Int]?
    private(set) var deselectUserCallCount = 0
    private(set) var row: Int?
    
    func displayUsers(_ users: [UserListComponentModel], title: String?) {
        displayUsersCallCount += 1
        self.users = users
        self.title = title
    }
    
    func presentEmptyStateWithMessage(_ message: String, title: String?) {
        presentEmptyStateCallCount += 1
        emptyStateMessage = message
        emptyStateTitle = title
    }
    
    func checkSelected(rows: [Int]) {
        checkSelectedCallCount += 1
        self.rows = rows
    }
    
    func deselectUser(at row: Int) {
        deselectUserCallCount += 1
        self.row = row
    }
}

private final class UserListDelegateSpy: UsersListComponentViewModelDelegate {
    private(set) var didRemoveUserCallCount = 0
    private(set) var removedUser: UserListComponentModel?
    private(set) var didAddUserCallCount = 0
    private(set) var addedUser: UserListComponentModel?
    private(set) var blockedToSelectANewUserCallCount = 0
    private(set) var didTryAgainOnErrorCallCount = 0
    private(set) var didScrollReachBottomListCallCount = 0
    private(set) var didTouchOnRequestPermissionCallCount = 0
    
    func didRemoveUser(_ user: UserListComponentModel) {
        didRemoveUserCallCount += 1
        self.removedUser = user
    }
    
    func didAddUser(_ user: UserListComponentModel) {
        didAddUserCallCount += 1
        self.addedUser = user
    }
    
    func blockedToSelectANewUser() {
        blockedToSelectANewUserCallCount += 1
    }
    
    func didTryAgainOnError() {
        didTryAgainOnErrorCallCount += 1
    }
    
    func didScrollReachBottomList() {
        didScrollReachBottomListCallCount += 1
    }
    
    func didTouchOnRequestPermission() {
        didTouchOnRequestPermissionCallCount += 1
    }
}

final class UserListComponentViewModelTests: XCTestCase {
    // MARK: - Variables
    private let presenter = UserListComponentPresenterSpy()
    private let delegate = UserListDelegateSpy()
    private let helper = UsersListComponentTestHelper()
    private lazy var user0 = helper.createUser(id: "0")
    private lazy var user1 = helper.createUser(id: "1", isSelected: true)
    private lazy var user2 = helper.createUser(id: "2", paymentRequestAllowed: false)
    
    private lazy var viewModel = UsersListComponentViewModel(
        presenter: presenter,
        delegate: delegate,
        maxUsersSelected: 5,
        users: [user0, user1, user2]
    )
    
    // MARK: - displayUsers
    func testDisplayUsers_WhenPassingUsersArray_ShouldCallDisplayUsersWithThatArrayAndTitle() throws {
        let users = [user0, user1]
        let title = "Title test"
        viewModel.displayUsers(users, title: title)
        let fakeUser0 = try XCTUnwrap(presenter.users?.first)
        let fakeUser1 = try XCTUnwrap(presenter.users?.last)
        let fakeTitle = try XCTUnwrap(presenter.title)
        
        XCTAssertEqual(presenter.displayUsersCallCount, 1)
        XCTAssertEqual(fakeUser0, user0)
        XCTAssertEqual(fakeUser1, user1)
        XCTAssertEqual(title, fakeTitle)
    }
    
    func testDisplayUsers_WhenSelectedUsersAreLessThanMaxUsersSelected_ShouldCallCheckSelectedWithArrayContaingSelectedUsersIndex() throws {
        let users = [user0, user1, user2]
        viewModel.displayUsers(users, title: "")
        let fakeRows = try XCTUnwrap(presenter.rows)
        
        XCTAssertEqual(presenter.checkSelectedCallCount, 1)
        XCTAssertEqual(fakeRows, [1])
    }
    
    // MARK: - displayEmptyStateWithMessageTitle
    func testDisplayEmptyStateWithMessageTitle_ShouldCallPresentEmptyState() throws {
        let message = "Message"
        let title = "Title"
        viewModel.displayEmptyStateWithMessage(message, title: title)
        let fakeMessage = try XCTUnwrap(presenter.emptyStateMessage)
        let fakeTitle = try XCTUnwrap(presenter.emptyStateTitle)
        XCTAssertEqual(presenter.presentEmptyStateCallCount, 1)
        XCTAssertEqual(fakeMessage, message)
        XCTAssertEqual(fakeTitle, title)
    }

    // MARK: - updateUserListSelection
    func testUpdateUserListSelection_WhenPassingIndex0ThatIsNotSelected_ShouldReturnTrueAndCallDelegateDidAddUserWithUser() throws {
        let index = 0
        let didUpdate = viewModel.updateUserListSelection(at: index)
        let fakeUser = try XCTUnwrap(delegate.addedUser)
        XCTAssertEqual(delegate.didAddUserCallCount, 1)
        XCTAssertEqual(user0, fakeUser)
        XCTAssertTrue(didUpdate)
    }
    
    func testUpdateUserListSelection_WhenPassingIndex1ThatIsSelected_ShouldReturnTrueAndCallDelegateDidRemoveUserWithUser() throws {
        let index = 1
        let didUpdate = viewModel.updateUserListSelection(at: index)
        let fakeUser = try XCTUnwrap(delegate.removedUser)
        XCTAssertEqual(delegate.didRemoveUserCallCount, 1)
        XCTAssertEqual(user1, fakeUser)
        XCTAssertTrue(didUpdate)
    }
    
    func testUpdateUserListSelection_WhenPassingIndex2ThatNotAllowsSelection_ShouldReturnFalseAndNotCallAnything() throws {
        let index = 2
        let didUpdate = viewModel.updateUserListSelection(at: index)
        XCTAssertEqual(delegate.didRemoveUserCallCount, 0)
        XCTAssertEqual(delegate.didAddUserCallCount, 0)
        XCTAssertFalse(didUpdate)
    }
    
    func testUpdateUserListSelection_WhenPassingIndex3ThatNotExists_ShouldReturnFalseAndNotCallAnything() throws {
        let index = 3
        let didUpdate = viewModel.updateUserListSelection(at: index)
        XCTAssertEqual(delegate.didRemoveUserCallCount, 0)
        XCTAssertEqual(delegate.didAddUserCallCount, 0)
        XCTAssertFalse(didUpdate)
    }
    
    func testUpdateUserListSelection_WhenSelectANewUserWhenMaxUsersSelectedAreReached_ShouldReturnFalseAndCallBlockedToSelectANewUser() throws {
        let selectedUser0 = helper.createUser(id: "0", isSelected: true)
        let selectedUser1 = helper.createUser(id: "1", isSelected: true)
        let selectedUser2 = helper.createUser(id: "2", isSelected: false)
        let maxUsers2ViewModel = UsersListComponentViewModel(
           presenter: presenter,
           delegate: delegate,
           maxUsersSelected: 2,
           users: [selectedUser0, selectedUser1, selectedUser2],
           selectedUsersIDs: ["0": true, "1": true]
        )
        let index = 2
        let didUpdate = maxUsers2ViewModel.updateUserListSelection(at: index)
        XCTAssertEqual(delegate.didRemoveUserCallCount, 0)
        XCTAssertEqual(delegate.didAddUserCallCount, 0)
        XCTAssertEqual(delegate.blockedToSelectANewUserCallCount, 1)
        XCTAssertFalse(didUpdate)
    }
    
    // MARK: - deselectUserWithId
    func testDeselectUserWithId_WhenIdIs1ThatIsSelected_ShouldCallDeselectUserWithRow1() throws {
        viewModel.deselectUserWithId("1")
        let fakeRow = try XCTUnwrap(presenter.row)
        XCTAssertEqual(presenter.deselectUserCallCount, 1)
        XCTAssertEqual(fakeRow, 1)
    }
    
    func testDeselectUserWithId_WhenIdIs1_ShouldRemoveUserWithUser1() throws {
        viewModel.deselectUserWithId("1")
        let fakeUser = try XCTUnwrap(delegate.removedUser)
        XCTAssertEqual(delegate.didRemoveUserCallCount, 1)
        XCTAssertEqual(fakeUser, user1)
    }
    
    func testDeselectUserWithId_WhenIdIs0ThatIsDeselected_ShouldNotCallAnything() throws {
        viewModel.deselectUserWithId("0")
        XCTAssertEqual(delegate.didRemoveUserCallCount, 0)
    }
    
    func testDeselectUserWithId_WhenIdIs4ThatNoExistis_ShouldNotCallAnything() throws {
        viewModel.deselectUserWithId("4")
        XCTAssertEqual(delegate.didRemoveUserCallCount, 0)
    }
    
    // MARK: - didTryAgainOnError
    func testDidTryAgainOnError_ShouldCallDelegateDidTryAgainOnError() {
        viewModel.didTryAgainOnError()
        XCTAssertEqual(delegate.didTryAgainOnErrorCallCount, 1)
    }
    
    // MARK: - didScrollReachBottomWithType
    func testDidScrollReachBottomWithType_WhenTypeIsList_ShouldCallDelegateDidScrollReachBottomList() {
        viewModel.didScrollReachBottomWithType(.list)
        XCTAssertEqual(delegate.didScrollReachBottomListCallCount, 1)
    }
    
    func testDidScrollReachBottomWithType_WhenTypeIsSingle_ShouldNotCallAnything() {
        viewModel.didScrollReachBottomWithType(.single)
        XCTAssertEqual(delegate.didScrollReachBottomListCallCount, 0)
    }
    
    // MARK: - didTouchOnRequestPermission
    func testDidTouchOnRequestPermission_ShouldCallDidTouchOnRequestPermission() {
        viewModel.didTouchOnRequestPermission()
        XCTAssertEqual(delegate.didTouchOnRequestPermissionCallCount, 1)
    }
}
