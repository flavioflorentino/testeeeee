import XCTest
import UI
@testable import PicPay

extension UserListComponentModel: Equatable {
    public static func == (lhs: UserListComponentModel, rhs: UserListComponentModel) -> Bool {
        return
            lhs.id == rhs.id &&
            lhs.username == rhs.username &&
            lhs.fullName == rhs.fullName &&
            lhs.urlImage == rhs.urlImage &&
            lhs.paymentRequestAllowed == rhs.paymentRequestAllowed
    }
}

extension UserListCellType: Equatable {
    public static func == (lhs: UserListCellType, rhs: UserListCellType) -> Bool {
        switch (lhs, rhs) {
        case
            (.empty, .empty),
            (.loading, .loading):
            return true
        case (let .default(lhsUsers), let .default(rhsUsers)):
            return lhsUsers == rhsUsers
        default:
            return false
        }
    }
}

private final class UsersListComponentViewSpy: UsersListComponentDisplay {
    private(set) var displayDataCallCount = 0
    private(set) var section: Section<String, UserListCellType>?
    private(set) var dataType: UsersListComponentDataType?
    private(set) var checkSelectedCallCount = 0
    private(set) var rows: [Int]?
    private(set) var deselectUserCallCount = 0
    private(set) var row: Int?
    
    func displayData(type: UsersListComponentDataType, section userSection: Section<String, UserListCellType>) {
        displayDataCallCount += 1
        self.dataType = type
        self.section = userSection
    }
    
    func checkSelected(at rows: [Int]) {
        checkSelectedCallCount += 1
        self.rows = rows
    }
    
    func deselectUser(at row: Int) {
        deselectUserCallCount += 1
        self.row = row
    }
}

final class UsersListComponentPresenterTests: XCTestCase {
    // MARK: - Variables
    private let view = UsersListComponentViewSpy()
    private lazy var presenter: UsersListComponentPresenter = {
        let presenter = UsersListComponentPresenter()
        presenter.view = view
        return presenter
    }()
    
    // MARK: - displayUsers
    func testDisplayUsers_WhenPassingANotEmptyUsersArray_ShouldCallDisplayDataWithListDataTypeAndSectionsContainingThatUsersArrayAndTheTitle() throws {
        let testerHelper = UsersListComponentTestHelper()
        let user1 = testerHelper.createUser(id: "1")
        let user2 = testerHelper.createUser(id: "2")
        let users = [user1, user2]
        let mappedUsers = users.map(UserListCellType.default)
        let title = "TitleTest"
        
        presenter.displayUsers(users, title: title)
        
        let fakeSection = try XCTUnwrap(view.section)
        let fakeUsers = fakeSection.items
        let fakeTitle = try XCTUnwrap(fakeSection.header)
        let fakeDataType = try XCTUnwrap(view.dataType)
        
        XCTAssertEqual(view.displayDataCallCount, 1)
        XCTAssertEqual(fakeTitle, title)
        XCTAssertEqual(fakeDataType, UsersListComponentDataType.list)
        XCTAssertEqual(fakeUsers, mappedUsers)
    }
    
    // MARK: - presentEmptyStateWithMessageTitle
    func testPresentEmptyStateWithMessageTitle_ShouldCallDisplayDataWithSingleDataTypeAndSectionContainingEmptyWithMessage() throws {
        let message = "Message"
        let title = "Title"
        presenter.presentEmptyStateWithMessage(message, title: title)
        let fakeDataType = try XCTUnwrap(view.dataType)
        let fakeSection = try XCTUnwrap(view.section)
        let fakeLoading = try XCTUnwrap(fakeSection.items.first)
        
        XCTAssertEqual(fakeDataType, .single)
        XCTAssertEqual(fakeLoading, UserListCellType.empty(message: message))
    }
    
    // MARK: - checkSelected
    func testCheckSelected_ShouldCallCheckSelectedWithIndexArray() throws {
        let rows = [1, 2, 3]
        presenter.checkSelected(rows: rows)
        let fakeRows = try XCTUnwrap(view.rows)
        
        XCTAssertEqual(view.checkSelectedCallCount, 1)
        XCTAssertEqual(rows, fakeRows)
    }
    
    // MARK: - deselectUser
    func testDeselectUser_ShouldCallDeselectUserWithRow() throws {
        let row = 1
        presenter.deselectUser(at: row)
        let fakeRow = try XCTUnwrap(view.row)
        
        XCTAssertEqual(view.deselectUserCallCount, 1)
        XCTAssertEqual(row, fakeRow)
    }
}
