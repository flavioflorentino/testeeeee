import AnalyticsModule
import Core
import XCTest
@testable import PicPay

private final class PaymentRequestDetailPresenterSpy: PaymentRequestDetailPresenting {
    private(set) var performCallsCount = 0
    private(set) var action: PaymentRequestDetailAction?
    private(set) var presentInitialDataCallsCount = 0
    private(set) var users: [PaymentRequestSearchViewModel.SelectedUser]?
    private(set) var presentPrivacyAlertCallsCount = 0
    private(set) var presentPlaceholderMessageCallsCount = 0
    private(set) var isVisible: Bool?
    private(set) var presentPaymentRequestSuccessCallsCount = 0
    private(set) var presentPaymentRequestErrorCallsCount = 0
    private(set) var presentPaymentRequestAlertErrorCallsCount = 0
    private(set) var alertErrorTitle: String?
    private(set) var alertErrorMessage: String?
    private(set) var alertErrorButtonTitle: String?
    private(set) var presentShouldFillValueAlertCallsCount = 0
    private(set) var startLoadingCallsCount = 0
    
    func perform(action: PaymentRequestDetailAction) {
        performCallsCount += 1
        self.action = action
    }
    
    func presentInitialData(users: [PaymentRequestSearchViewModel.SelectedUser]) {
        presentInitialDataCallsCount += 1
        self.users = users
    }
    
    func presentPrivacyAlert() {
        presentPrivacyAlertCallsCount += 1
    }
    
    func presentPlaceholderMessage(isVisible: Bool) {
        presentPlaceholderMessageCallsCount += 1
        self.isVisible = isVisible
    }
    
    func presentPaymentRequestSuccess() {
        presentPaymentRequestSuccessCallsCount += 1
    }
    
    func presentPaymentRequestError() {
        presentPaymentRequestErrorCallsCount += 1
    }
    
    func presentPaymentRequestAlertError(title: String, message: String, buttonTitle: String) {
        presentPaymentRequestAlertErrorCallsCount += 1
        alertErrorTitle = title
        alertErrorMessage = message
        alertErrorButtonTitle = buttonTitle
    }
    
    func presentShouldFillValueAlert() {
        presentShouldFillValueAlertCallsCount += 1
    }
    
    func startLoading() {
        startLoadingCallsCount += 1
    }
}

private final class PaymentRequestDetailServiceSpy: PaymentRequestDetailServicing {
    private(set) var sendPaymentRequestCallsCount = 0
    private(set) var users: [(value: Double, id: Int)]?
    private(set) var message: String?
    var result: Result<(model: NoContent, data: Data?), DialogError>?
    
    func sendPaymentRequest(users: [(Double, Int)], message: String, completion: @escaping (Result<(model: NoContent, data: Data?), DialogError>) -> Void) {
        self.users = users
        self.message = message
        sendPaymentRequestCallsCount += 1
        guard let result = result else {
            return
        }
        completion(result)
    }
}

final class PaymentRequestDetailViewModelTests: XCTestCase {
    // MARK: - Variables
    private let dependencies = DependencyContainerMock(Analytics.shared)
    private var users: [PaymentRequestSearchViewModel.SelectedUser] = []
    private lazy var viewModel = PaymentRequestDetailViewModel(
        users: users,
        dependencies: dependencies,
        presenter: presenter,
        service: service
    )
    private let presenter = PaymentRequestDetailPresenterSpy()
    private let service = PaymentRequestDetailServiceSpy()
    
    // MARK: - Mocks
    private func createUsers(qty: Int) -> [PaymentRequestSearchViewModel.SelectedUser] {
        guard qty >= 1 else {
            return []
        }
        return (1...qty).map { (id: "\($0)", name: "Name \($0)", url: nil) }
    }
    
    // MARK: - loadInitialData
    func testLoadInitialData_ShouldCallPresentInitialData() throws {
        users = createUsers(qty: 5)
        viewModel.loadInitialData()
        let finalUsers = try XCTUnwrap(presenter.users)
        XCTAssertEqual(presenter.presentInitialDataCallsCount, 1)
        XCTAssertEqual(finalUsers.count, users.count)
    }
    
    // MARK: - sendPaymentRequest
    func testSendPaymentRequest_WhenValueIsLessThanOrEqualToZero_ShouldCallPresentShouldFillValueAlert() {
        viewModel.sendPaymentRequest(message: "Text", value: 0.0)
        XCTAssertEqual(presenter.presentShouldFillValueAlertCallsCount, 1)
    }
    
    func testSendPaymentRequest_WhenValueIsGreaterThanZeroAndResultIsSuccess_ShouldCallStartLoadingAndServiceSendPaymentRequestAndPresentPaymentRequestSuccessAndNotPresentShouldFillValueAlert() throws {
        let message = "Text"
        let value = 10.0
        users = createUsers(qty: 1)
        service.result = .success((model: NoContent(), data: nil))
        viewModel.changeTextView(text: message)
        viewModel.sendPaymentRequest(message: message, value: value)
        let finalMessage = try XCTUnwrap(service.message)
        let serviceFirstUser = try XCTUnwrap(service.users?.first)
        XCTAssertEqual(message, finalMessage)
        XCTAssertEqual(users[0].id, "\(serviceFirstUser.id)")
        XCTAssertEqual(value, serviceFirstUser.value)
        XCTAssertEqual(presenter.startLoadingCallsCount, 1)
        XCTAssertEqual(service.sendPaymentRequestCallsCount, 1)
        XCTAssertEqual(presenter.presentPaymentRequestSuccessCallsCount, 1)
        XCTAssertEqual(presenter.presentShouldFillValueAlertCallsCount, 0)
    }
    
    func testSendPaymentRequest_WhenValueIsGreaterThanZeroAndResultIsFailureWithUnknownError_ShouldCallStartLoadingAndServiceSendPaymentRequestAndPresentPaymentRequestErrorAndNotPresentShouldFillValueAlert() throws {
        let message = "Text"
        let value = 10.0
        users = createUsers(qty: 1)
        service.result = .failure(.default(ApiError.unknown(nil)))
        viewModel.changeTextView(text: message)
        viewModel.sendPaymentRequest(message: message, value: value)
        let finalMessage = try XCTUnwrap(service.message)
        let serviceFirstUser = try XCTUnwrap(service.users?.first)
        XCTAssertEqual(message, finalMessage)
        XCTAssertEqual(users[0].id, "\(serviceFirstUser.id)")
        XCTAssertEqual(value, serviceFirstUser.value)
        XCTAssertEqual(presenter.startLoadingCallsCount, 1)
        XCTAssertEqual(service.sendPaymentRequestCallsCount, 1)
        XCTAssertEqual(presenter.presentPaymentRequestErrorCallsCount, 1)
        XCTAssertEqual(presenter.presentShouldFillValueAlertCallsCount, 0)
    }
    
    func testSendPaymentRequest_WhenValueIsGreaterThanZeroAndResultIsFailureWithMinimumValueError_ShouldCallStartLoadingAndServiceSendPaymentRequestAndPresentPaymentRequestAlertErrorAndNotPresentShouldFillValueAlert() throws {
        let message = "Text"
        let value = 10.0
        let errorTitle = "title"
        let errorMessage = "message"
        let errorButtonText = "buttonText"
        let errorModel = DialogErrorModel(title: errorTitle, message: errorMessage, buttonText: errorButtonText)
        let responseError = DialogResponseError(
            code: PaymentRequestDetailViewModel.ErrorCode.minimumValue.rawValue,
            model: errorModel
        )
        users = createUsers(qty: 1)
        service.result = .failure(.dialog(responseError))
        viewModel.changeTextView(text: message)
        viewModel.sendPaymentRequest(message: message, value: value)
        let finalMessage = try XCTUnwrap(service.message)
        let serviceFirstUser = try XCTUnwrap(service.users?.first)
        let finalErrorTitle = try XCTUnwrap(presenter.alertErrorTitle)
        let finalErrorMessage = try XCTUnwrap(presenter.alertErrorMessage)
        let finalErrorButtonText = try XCTUnwrap(presenter.alertErrorButtonTitle)
        XCTAssertEqual(message, finalMessage)
        XCTAssertEqual(users[0].id, "\(serviceFirstUser.id)")
        XCTAssertEqual(value, serviceFirstUser.value)
        XCTAssertEqual(presenter.startLoadingCallsCount, 1)
        XCTAssertEqual(service.sendPaymentRequestCallsCount, 1)
        XCTAssertEqual(presenter.presentPaymentRequestAlertErrorCallsCount, 1)
        XCTAssertEqual(errorTitle, finalErrorTitle)
        XCTAssertEqual(errorMessage, finalErrorMessage)
        XCTAssertEqual(errorButtonText, finalErrorButtonText)
        XCTAssertEqual(presenter.presentShouldFillValueAlertCallsCount, 0)
    }
    
    // MARK: - beginEditingTextView n changeTextView
    func testBeginEditingTextViewAndChangeTextView_WhenHasOnlyPlaceholderOnTextView_ShouldCallPresentPlaceholderMessage() {
        viewModel.beginEditingTextView()
        XCTAssertEqual(presenter.presentPlaceholderMessageCallsCount, 1)
    }
    
    func testBeginEditingTextViewAndChangeTextView_WhenHasNotOnlyPlaceholderOnTextView_ShouldNotCallPresentPlaceholderMessageCallsCount() {
        viewModel.changeTextView(text: "text")
        viewModel.beginEditingTextView()
        XCTAssertEqual(presenter.presentPlaceholderMessageCallsCount, 0)
    }
    
    // MARK: - shouldChangeText
    func testShouldChangeText_WhenMessageCountIsGreaterThan100_ShouldReturnFalse() {
        let text = "Teste teste teste teste teste teste teste teste teste teste teste teste teste teste teste teste teste"
        let result = viewModel.shouldChangeText(text)
        XCTAssertFalse(result)
    }
    
    func testShouldChangeText_WhenMessageCountIsLessThanOrEqualTo100_ShouldReturnTrue() {
        let text = "123456789"
        let result = viewModel.shouldChangeText(text)
        XCTAssertTrue(result)
    }
    
    // MARK: - endEditingTextView n changeTextView
    func testEndEditingTextViewAndChangeTextView_WhenHasOnlyPlaceholderOnTextView_ShouldCallPresentPlaceholderMessage() {
        viewModel.endEditingTextView()
        XCTAssertEqual(presenter.presentPlaceholderMessageCallsCount, 1)
    }
    
    func testEndEditingTextViewAndChangeTextView_WhenHasNotOnlyPlaceholderOnTextView_ShouldNotCallPresentPlaceholderMessage() {
        viewModel.changeTextView(text: "text")
        viewModel.endEditingTextView()
        XCTAssertEqual(presenter.presentPlaceholderMessageCallsCount, 0)
    }
    
    // MARK: - touchOnPrivacyButton
    func testTouchOnPrivacyButton_ShouldCallPresentPrivacyAlert() {
        viewModel.touchOnPrivacyButton()
        XCTAssertEqual(presenter.presentPrivacyAlertCallsCount, 1)
    }
    
    // MARK: - endflow
    func testEndFlow_ShouldCallPerform() throws {
        viewModel.endflow()
        let action = try XCTUnwrap(presenter.action)
        XCTAssertEqual(presenter.performCallsCount, 1)
        XCTAssertEqual(action, .endFlow)
    }
}
