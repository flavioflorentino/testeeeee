import XCTest
@testable import PicPay

final class PaymentRequestDetailCoordinatorTests: XCTestCase {
    // MARK: - Variables
    private lazy var coordinator: PaymentRequestDetailCoordinator = {
        let coordinator = PaymentRequestDetailCoordinator()
        coordinator.viewController = viewController
        return coordinator
    }()
    private let viewController = UIViewController()
    private var navigationController: NavigationControllerMock?
    
    // MARK: - Setup
    override func setUpWithError() throws {
        try super.setUpWithError()
        navigationController = NavigationControllerMock(rootViewController: viewController)
    }
    
    // MARK: - perform
    func testPerform_WhenActionIsEndFlow_ShouldDismissNavigation() throws {
        coordinator.perform(action: .endFlow)
        let navigationController = try XCTUnwrap(self.navigationController)
        XCTAssertTrue(navigationController.isDismissViewControllerCalled)
    }
}
