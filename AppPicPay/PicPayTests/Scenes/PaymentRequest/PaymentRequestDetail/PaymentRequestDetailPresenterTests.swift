import UI
import XCTest
@testable import PicPay

private final class PaymentRequestCoordinatorSpy: PaymentRequestDetailCoordinating {
    private(set) var performCallsCount = 0
    private(set) var action: PaymentRequestDetailAction?
    var viewController: UIViewController?
    
    func perform(action: PaymentRequestDetailAction) {
        performCallsCount += 1
        self.action = action
    }
}

private final class PaymentRequestViewControllerSpy: PaymentRequestDetailDisplay {
    private(set) var displayUsersCallsCount = 0
    private(set) var users: [UserCounterModel]?
    private(set) var displayTitleCallsCount = 0
    private(set) var title: String?
    private(set) var displayPrivacyAlertCallsCount = 0
    private(set) var message: String?
    private(set) var displayTextMessageCallsCount = 0
    private(set) var model: StatefulErrorViewModel?
    private(set) var displayErrorCallsCount = 0
    private(set) var displayAlertCallsCount = 0
    private(set) var displayAlertTitle: String?
    private(set) var displayAlertMessage: String?
    private(set) var displayAlertButtonTitle: String?
    private(set) var displayAlertControllerCallsCount = 0
    private(set) var displayAlertControllerTitle: String?
    private(set) var displayAlertControllerMessage: String?
    private(set) var displayAlertControllerButtonTitle: String?
    private(set) var displaySuccessCallsCount = 0
    private(set) var alertData: StatusAlertData?
    private(set) var startLoadingCallsCount = 0
    
    func display(users: [UserCounterModel]) {
        displayUsersCallsCount += 1
        self.users = users
    }
    
    func display(title: String) {
        displayTitleCallsCount += 1
        self.title = title
    }
    
    func displayPrivacyAlert() {
        displayPrivacyAlertCallsCount += 1
    }
    
    func displayTextMessage(_ message: String) {
        displayTextMessageCallsCount += 1
        self.message = message
    }
    
    func displayError(_ model: StatefulErrorViewModel) {
        displayErrorCallsCount += 1
        self.model = model
    }
    
    func displayAlert(title: String, message: String, buttonTitle: String) {
        displayAlertCallsCount += 1
        displayAlertTitle = title
        displayAlertMessage = message
        displayAlertButtonTitle = buttonTitle
    }
    
    func displayAlertController(title: String, message: String, buttonTitle: String) {
        displayAlertControllerCallsCount += 1
        displayAlertControllerTitle = title
        displayAlertControllerMessage = message
        displayAlertControllerButtonTitle = buttonTitle
    }
    
    func displaySuccess(alertData: StatusAlertData) {
        displaySuccessCallsCount += 1
        self.alertData = alertData
    }
    
    func startLoading() {
        startLoadingCallsCount += 1
    }
}

final class PaymentRequestDetailPresenterTests: XCTestCase {
    // MARK: - Variables
    private let coordinator = PaymentRequestCoordinatorSpy()
    private let viewController = PaymentRequestViewControllerSpy()
    private lazy var presenter: PaymentRequestDetailPresenting = {
        let presenter = PaymentRequestDetailPresenter(coordinator: coordinator)
        presenter.viewController = viewController
        return presenter
    }()
    
    // MARK: - Mocks
    private func createUsers(qty: Int) -> [PaymentRequestSearchViewModel.SelectedUser] {
        guard qty >= 1 else {
            return []
        }
        return (1...qty).map { (id: "\($0)", name: "Name \($0)", url: nil) }
    }
    
    // MARK: - perform
    func testPerform_ShouldCallCoordinatorPerform() {
        presenter.perform(action: .endFlow)
        XCTAssertEqual(coordinator.performCallsCount, 1)
        XCTAssertNotNil(coordinator.action)
    }
    
    // MARK: - presentInitialData
    func testPresentInitialData_WhenUserCountEqualTo1_ShouldCallDisplayUsersAndDisplayTitleWithUsername() throws {
        let users = createUsers(qty: 1)
        presenter.presentInitialData(users: users)
        let finalTitle = try XCTUnwrap(viewController.title)
        XCTAssertEqual(viewController.displayUsersCallsCount, 1)
        XCTAssertEqual(viewController.displayTitleCallsCount, 1)
        XCTAssertEqual(finalTitle, users[0].name)
    }
    
    func testPresentInitialData_WhenUserCountIsNotEqualTo1_ShouldCallDisplayUsersAndDisplayTitleWithDefaultTitle() throws {
        let users = createUsers(qty: 3)
        presenter.presentInitialData(users: users)
        let finalTitle = try XCTUnwrap(viewController.title)
        XCTAssertEqual(viewController.displayUsersCallsCount, 1)
        XCTAssertEqual(viewController.displayTitleCallsCount, 1)
        XCTAssertEqual(finalTitle, PaymentRequestLocalizable.valuePerPerson.text)
    }
    
    // MARK: - presentPrivacyAlert
    func testPresentPrivacyAlert_ShouldCallDisplayPrivacyAlert() {
        presenter.presentPrivacyAlert()
        XCTAssertEqual(viewController.displayPrivacyAlertCallsCount, 1)
    }
    
    // MARK: - presentPlaceholderMessage
    func testPresentPlaceholderMessage_WhenIsVisible_ShouldCallDisplayTextMessageWithPlaceholder() throws {
        presenter.presentPlaceholderMessage(isVisible: true)
        let message = try XCTUnwrap(viewController.message)
        XCTAssertEqual(viewController.displayTextMessageCallsCount, 1)
        XCTAssertEqual(message, PaymentRequestLocalizable.messagePlaceholder.text)
    }
    
    func testPresentPlaceholderMessage_WhenIsNotVisible_ShouldCallDisplayTextMessageIsEmptyText() throws {
        presenter.presentPlaceholderMessage(isVisible: false)
        let message = try XCTUnwrap(viewController.message)
        XCTAssertEqual(viewController.displayTextMessageCallsCount, 1)
        XCTAssertTrue(message.isEmpty)
    }
    
    // MARK: - presentPaymentRequestSuccess
    func testPresentPaymentRequestSuccess_ShouldCallDisplaySuccess() {
        presenter.presentPaymentRequestSuccess()
        XCTAssertEqual(viewController.displaySuccessCallsCount, 1)
        XCTAssertNotNil(viewController.alertData)
    }
    
    // MARK: - presentPaymentRequestError
    func testPresentPaymentRequestError_ShouldCallDisplayError() {
        presenter.presentPaymentRequestError()
        XCTAssertEqual(viewController.displayErrorCallsCount, 1)
    }
    
    // MARK: - presentPaymentRequestAlertError
    func testPresentPaymentRequestAlertError_ShouldCallDisplayAlert() throws {
        let title = "title"
        let message = "message"
        let buttonTitle = "buttonTitle"
        presenter.presentPaymentRequestAlertError(title: title, message: message, buttonTitle: buttonTitle)
        let finalTitle = try XCTUnwrap(viewController.displayAlertTitle)
        let finalMessage = try XCTUnwrap(viewController.displayAlertMessage)
        let finalButtonTitle = try XCTUnwrap(viewController.displayAlertButtonTitle)
        XCTAssertEqual(viewController.displayAlertCallsCount, 1)
        XCTAssertEqual(title, finalTitle)
        XCTAssertEqual(message, finalMessage)
        XCTAssertEqual(buttonTitle, finalButtonTitle)
    }
    
    // MARK: - presentShouldFillValueAlert
    func testPresentShouldFillValueAlert_ShouldCallDisplayAlertController() throws {
        presenter.presentShouldFillValueAlert()
        let finalTitle = try XCTUnwrap(viewController.displayAlertControllerTitle)
        let finalMessage = try XCTUnwrap(viewController.displayAlertControllerMessage)
        let finalButtonTitle = try XCTUnwrap(viewController.displayAlertControllerButtonTitle)
        XCTAssertEqual(viewController.displayAlertControllerCallsCount, 1)
        XCTAssertEqual(PaymentRequestLocalizable.paymentRequestShouldFillValueAlertTitle.text, finalTitle)
        XCTAssertEqual("", finalMessage)
        XCTAssertEqual(DefaultLocalizable.btOk.text, finalButtonTitle)
    }
    
    // MARK: - startLoading
    func testStartLoading_ShouldCallStartLoading() {
        presenter.startLoading()
        XCTAssertEqual(viewController.startLoadingCallsCount, 1)
    }
}
