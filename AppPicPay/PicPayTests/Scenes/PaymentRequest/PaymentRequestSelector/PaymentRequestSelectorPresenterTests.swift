@testable import PicPay
@testable import UI
import XCTest

private final class FakePaymentRequestSelectorViewController: PaymentRequestSelectorDisplay {
    private(set) var setTableViewCallCount = 0
    private(set) var displayOptionsCallCount = 0
    private(set) var displayRetryLimitExceededViewCallCount = 0
    private(set) var displayPicPaySelectionFailureCallCount = 0
    private(set) var updateLoaderCallCount = 0
    private(set) var isScrollEnabled: Bool?
    private(set) var options: [Section<String, PaymentRequestOption>]?
    private(set) var alertAttributedData: StatusAlertAttributedData?
    private(set) var alertData: StatusAlertData?
    private(set) var isLoaderVisible: Bool?
    
    func setTableView(isScrollEnabled: Bool) {
        self.isScrollEnabled = isScrollEnabled
        setTableViewCallCount += 1
    }
    
    func displayOptions(_ options: [Section<String, PaymentRequestOption>]) {
        self.options = options
        displayOptionsCallCount += 1
    }
    
    func displayRetryLimitExceeded(with alertData: StatusAlertAttributedData) {
        displayRetryLimitExceededViewCallCount += 1
        alertAttributedData = alertData
    }
    
    func displayPicPaySelectionFailure(with alertData: StatusAlertData) {
        displayPicPaySelectionFailureCallCount += 1
        self.alertData = alertData
    }
    
    func updateLoader(isVisible: Bool) {
        updateLoaderCallCount += 1
        isLoaderVisible = isVisible
    }
}

private final class FakePaymentRequestSelectorCoordinator: PaymentRequestSelectorCoordinating {
    private(set) var consumersSearchCallCount = 0
    private(set) var consumersSearchV2CallCount = 0
    private(set) var chargeUserValueCallCount = 0
    private(set) var closeCallCount = 0
    private(set) var knowMoreCallCount = 0
    private(set) var pixReceivementCallsCount = 0
    private(set) var origin: DGHelpers.Origin?
    private(set) var type: PaymentRequestExternalType?
    private(set) var maxUsers: Int?
    var viewController: UIViewController?
    
    func perform(action: PaymentRequestSelectorAction) {
        switch action {
        case .consumersSearch:
            consumersSearchCallCount += 1
        case let .consumersSearchV2(maxUsers):
            consumersSearchV2CallCount += 1
            self.maxUsers = maxUsers
        case let .chargeUserValue(origin, type):
            self.origin = origin
            self.type = type
            chargeUserValueCallCount += 1
        case .close:
            closeCallCount += 1
        case .knowMore:
            knowMoreCallCount += 1
        case .pixReceivement:
            pixReceivementCallsCount += 1
        }
    }
}

final class PaymentRequestSelectorPresenterTests: XCTestCase {
    // MARK: - Variables
    private lazy var fakeViewController = FakePaymentRequestSelectorViewController()
    private lazy var presenter = PaymentRequestSelectorPresenter(coordinator: fakeCoordinator)
    private lazy var fakeCoordinator = FakePaymentRequestSelectorCoordinator()
    
    // MARK: - Setup
    override func setUp() {
        presenter.viewController = fakeViewController
    }
    
    // MARK: - setTableView
    func testSetTableViewShouldBeScrollDisabledAndCallSetTableView() throws {
        presenter.setTableView(isScrollEnabled: false)
        let isScrollEnabled = try XCTUnwrap(fakeViewController.isScrollEnabled)
        XCTAssertFalse(isScrollEnabled)
        XCTAssertEqual(fakeViewController.setTableViewCallCount, 1)
    }
    
    // MARK: - presentOptions
    func testPresentOptionsShouldHaveTheGivenOptionsAndCallDisplayOptions() throws {
        let givenOptions: [PaymentRequestOption] = PaymentRequestOption.allCases
        presenter.presentOptions(givenOptions)
        let fakeOptions = try XCTUnwrap(fakeViewController.options)
        XCTAssertEqual(fakeOptions[0].items.count, givenOptions.count)
        XCTAssertEqual(fakeViewController.displayOptionsCallCount, 1)
        if fakeOptions.count == givenOptions.count {
            for index in 0..<fakeOptions.count {
                XCTAssertEqual(fakeOptions[0].items[index], givenOptions[index])
            }
        }
    }
    
    // MARK: - presentConsumersSearch
    func testPresentConsumersSearchHaveTheGivenConfigurations() throws {
        presenter.presentConsumersSearch()
        XCTAssertEqual(fakeCoordinator.consumersSearchCallCount, 1)
    }
    
    // MARK: - presentConsumersSearchV2
    func testPresentConsumersSearchV2_ShouldCallPerformConsumerSearchV2WithGivenData() throws {
        let maxUsers = 2
        presenter.presentConsumersSearchV2(maxUsers: maxUsers)
        let finalMaxUsers = try XCTUnwrap(fakeCoordinator.maxUsers)
        XCTAssertEqual(fakeCoordinator.consumersSearchV2CallCount, 1)
        XCTAssertEqual(finalMaxUsers, maxUsers)
    }
    
    // MARK: - presentChargeUserValueSelection
    func testPresentChargeUserValueSelection_WhenOriginIsDeeplinkAndTypeIsLink_ShouldHaveTheGivenOriginAndTypeAndCallChargeUserValue() throws {
        let origin: DGHelpers.Origin = .deeplink
        let type = PaymentRequestExternalType.link
        presenter.presentChargeUserValueSelection(origin: origin, type: type)
        let fakeOrigin = try XCTUnwrap(fakeCoordinator.origin)
        let fakeType = try XCTUnwrap(fakeCoordinator.type)
        XCTAssertEqual(fakeCoordinator.chargeUserValueCallCount, 1)
        XCTAssertEqual(fakeOrigin, origin)
        XCTAssertEqual(fakeType, type)
    }
    
    // MARK: - dismissViewController
    func testDismissViewControllerShouldCallDismiss() {
        presenter.dismissViewController()
        XCTAssertEqual(fakeCoordinator.closeCallCount, 1)
    }
    
    // MARK: - presentRetryLimitExceeded
    func testPresentRetryLimitExceededShouldCallDisplayRetryLimit() {
        presenter.presentRetryLimitExceeded()
        XCTAssertEqual(fakeViewController.displayRetryLimitExceededViewCallCount, 1)
    }
    
    // MARK: - presentRetryLimitExceededV2
    func testPresentRetryLimitExceededV2_ShouldCallDisplayRetryLimit() {
        presenter.presentRetryLimitExceededV2(title: "title", message: "message", buttonTitle: "btnTitle")
        XCTAssertEqual(fakeViewController.displayRetryLimitExceededViewCallCount, 1)
    }
    
    // MARK: - presentKnowMore
    func testPresentKnowMoreShouldCallPerformActionKnowMore() {
        presenter.presentKnowMore()
        XCTAssertEqual(fakeCoordinator.knowMoreCallCount, 1)
    }
    
    // MARK: - presentPicPaySelectionFailure
    func testPresentPicPaySelectionFailureShouldCallDisplayPicPaySelectionFailureWithAlertData() throws {
        presenter.presentPicPaySelectionFailure()
        let alertData = try XCTUnwrap(fakeViewController.alertData)
        XCTAssertEqual(fakeViewController.displayPicPaySelectionFailureCallCount, 1)
        XCTAssertEqual(alertData.icon, Assets.Emotions.iluSadFace.image)
        XCTAssertEqual(alertData.title, PaymentRequestLocalizable.somethingWentWrong.text)
        XCTAssertEqual(alertData.text, PaymentRequestLocalizable.requestNotCompleted.text)
        XCTAssertEqual(alertData.buttonTitle, PaymentRequestLocalizable.tryAgain.text)
    }
    
    // MARK: - updateLoader
    func testUpdateLoaderShouldCallUpdateLoaderAndLoaderBeVisible() throws {
        presenter.updateLoader(isVisible: true)
        let isLoaderVisible = try XCTUnwrap(fakeViewController.isLoaderVisible)
        XCTAssertEqual(fakeViewController.updateLoaderCallCount, 1)
        XCTAssertTrue(isLoaderVisible)
    }
    
    func testUpdateLoaderShouldCallUpdateLoaderAndLoaderNotBeVisible() throws {
        presenter.updateLoader(isVisible: false)
        let isLoaderVisible = try XCTUnwrap(fakeViewController.isLoaderVisible)
        XCTAssertEqual(fakeViewController.updateLoaderCallCount, 1)
        XCTAssertFalse(isLoaderVisible)
    }
    
    // MARK: - presentPixReceivementScene
    func testpresentPixReceivementScene_ShouldCallPixReceivement() {
        presenter.presentPixReceivementScene()
        XCTAssertEqual(fakeCoordinator.pixReceivementCallsCount, 1)
    }
}
