@testable import PicPay
import XCTest
import Core
import FeatureFlag

// MARK: - FakePaymentRequestSelectorService
private final class FakePaymentRequestSelectorService: PaymentRequestSelectorServicing {
    var paymentRequestPermission: Result<(model: PaymentRequestPermissionData, data: Data?), ApiError>?
    var paymentRequestPermissionV2: Result<(model: PaymentRequestPermissionV2, data: Data?), DialogError>?

    private(set) var getPaymentRequestPermissionCallCount = 0
    private(set) var getPaymentRequestPermissionV2CallCount = 0
    
    func getPaymentRequestPermission(completion: @escaping (Result<(model: PaymentRequestPermissionData, data: Data?), ApiError>) -> Void) {
        getPaymentRequestPermissionCallCount += 1
        guard let permission = paymentRequestPermission else {
            return
        }
        completion(permission)
    }
    
    func getPaymentRequestPermissionV2(completion: @escaping (Result<(model: PaymentRequestPermissionV2, data: Data?), DialogError>) -> Void) {
        getPaymentRequestPermissionV2CallCount += 1
        guard let permission = paymentRequestPermissionV2 else {
            return
        }
        completion(permission)
    }
}

// MARK: - FakePaymentRequestSelectorPresenter
private final class FakePaymentRequestSelectorPresenter: PaymentRequestSelectorPresenting {
    private(set) var setTableViewCallCount = 0
    private(set) var presentOptionsCallCount = 0
    private(set) var presentConsumersSearchCallCount = 0
    private(set) var presentConsumersSearchV2CallCount = 0
    private(set) var presentChargeUserValueCallCount = 0
    private(set) var dismissViewControllerCallCount = 0
    private(set) var presentRetryLimitExceededCallCount = 0
    private(set) var presentRetryLimitExceededV2CallCount = 0
    private(set) var isScrollEnabled: Bool?
    private(set) var options: [PaymentRequestOption]?
    private(set) var origin: DGHelpers.Origin?
    private(set) var type: PaymentRequestExternalType?
    private(set) var presentKnowMoreCallCount = 0
    private(set) var presentPicPaySelectionFailureCallCount = 0
    private(set) var updateLoaderCallCount = 0
    private(set) var isLoaderVisible: Bool?
    private(set) var maxUsers: Int?
    var viewController: PaymentRequestSelectorDisplay?
    private(set) var presentPixReceivementSceneCallsCount = 0
    
    func setTableView(isScrollEnabled: Bool) {
        self.isScrollEnabled = isScrollEnabled
        setTableViewCallCount += 1
    }
    
    func presentOptions(_ options: [PaymentRequestOption]) {
        self.options = options
        presentOptionsCallCount += 1
    }
    
    func presentConsumersSearch() {
        presentConsumersSearchCallCount += 1
    }
    
    func presentConsumersSearchV2(maxUsers: Int) {
        presentConsumersSearchV2CallCount += 1
        self.maxUsers = maxUsers
    }
    
    func presentChargeUserValueSelection(origin: DGHelpers.Origin, type: PaymentRequestExternalType) {
        self.origin = origin
        self.type = type
        presentChargeUserValueCallCount += 1
    }
    
    func dismissViewController() {
        dismissViewControllerCallCount += 1
    }
    
    func presentRetryLimitExceeded() {
        presentRetryLimitExceededCallCount += 1
    }
    
    func presentRetryLimitExceededV2(title: String, message: String, buttonTitle: String) {
        presentRetryLimitExceededV2CallCount += 1
    }
    
    func presentKnowMore() {
        presentKnowMoreCallCount += 1
    }
    
    func presentPicPaySelectionFailure() {
        presentPicPaySelectionFailureCallCount += 1
    }
    
    func updateLoader(isVisible: Bool) {
        updateLoaderCallCount += 1
        isLoaderVisible = isVisible
    }
    
    func presentPixReceivementScene() {
        presentPixReceivementSceneCallsCount += 1
    }
}

// MARK: - PaymentRequestSelectorViewModelTests
final class PaymentRequestSelectorViewModelTests: XCTestCase {
    // MARK: - Variables
    private let featureManager = FeatureManagerMock()
    private var viewModel: PaymentRequestSelectorViewModel!
    private var fakePresenter: FakePaymentRequestSelectorPresenter!
    private var fakeService: FakePaymentRequestSelectorService!
    private let origin: DGHelpers.Origin = .deeplink
    
    // MARK: - Setup
    override func setUp() {
        fakePresenter = FakePaymentRequestSelectorPresenter()
        fakeService = FakePaymentRequestSelectorService()
        viewModel = PaymentRequestSelectorViewModel(
            dependencies: DependencyContainerMock(featureManager),
            service: fakeService,
            presenter: fakePresenter,
            origin: origin
        )
    }
    
    // MARK: - setTableViewScroll
    func testSetTableViewScrollShouldHaveScrollDisabledAndCallSetTableView() {
        let screenSize = CGSize(width: 200, height: 400)
        viewModel.setTableViewScroll(
            rowHeight: 85,
            screenSize: screenSize,
            tableViewOrigin: .zero,
            bottomMargin: 0
        )
        guard let isScrollEnabled = fakePresenter.isScrollEnabled else {
            XCTFail("isScrollEnabled isn't defined")
            return
        }
        XCTAssertFalse(isScrollEnabled)
        XCTAssertEqual(fakePresenter.setTableViewCallCount, 1)
    }
    
    func testSetTableViewScrollShouldHaveScrollEnabledAndCallSetTableView() {
        let screenSize = CGSize(width: 200, height: 400)
        let tableViewOrigin = CGPoint(x: 0, y: 150)
        featureManager.override(key: .featurePixHubReceiving, with: false)
        viewModel.presentOptions()
        viewModel.setTableViewScroll(
            rowHeight: 85,
            screenSize: screenSize,
            tableViewOrigin: tableViewOrigin,
            bottomMargin: 0
        )
        guard let isScrollEnabled = fakePresenter.isScrollEnabled else {
            XCTFail("isScrollEnabled isn't defined")
            return
        }
        XCTAssertTrue(isScrollEnabled)
        XCTAssertEqual(fakePresenter.setTableViewCallCount, 1)
    }
    
    // MARK: - presentOptions
    func testPresentOptions_WhenReceiveHubPixFlagIsDisabled_ShouldCallPresentOptionsWithOptionsPicPayAndQRCodeAndLink() {
        featureManager.override(key: .featurePixHubReceiving, with: false)
        viewModel.presentOptions()
        XCTAssertEqual(fakePresenter.presentOptionsCallCount, 1)
        XCTAssertEqual(fakePresenter.options, [.picpay, .qrcode, .link])
    }
    
    func testPresentOptions_WhenReceiveHubPixFlagIsEnabled_ShouldCallPresentOptionsWithAllOptions() {
        featureManager.override(key: .featurePixHubReceiving, with: true)
        viewModel.presentOptions()
        XCTAssertEqual(fakePresenter.presentOptionsCallCount, 1)
        XCTAssertEqual(fakePresenter.options, PaymentRequestOption.allCases)
    }
    
    // MARK: - selectOption
    func testSelectOption_WhenSelectPicPayAndPaymentRequestV2IsNotActive_ShouldCallPresentConsumersSearch() throws {
        let permission = PaymentRequestPermission(isP2PBlockDayChargesEnabled: false)
        let data = PaymentRequestPermissionData(data: permission)
        let response: (model: PaymentRequestPermissionData, data: Data?) = (model: data, data: nil)
        fakeService.paymentRequestPermission = .success(response)
        featureManager.override(key: .paymentRequestV2, with: false)
        featureManager.override(key: .featurePixHubReceiving, with: true)
        viewModel.presentOptions()
        viewModel.selectOption(at: PaymentRequestOption.picpay.rawValue)
        let isLoaderVisible = try XCTUnwrap(fakePresenter.isLoaderVisible)
        XCTAssertEqual(fakeService.getPaymentRequestPermissionCallCount, 1)
        XCTAssertEqual(fakePresenter.updateLoaderCallCount, 2)
        XCTAssertEqual(fakePresenter.presentConsumersSearchCallCount, 1)
        XCTAssertFalse(isLoaderVisible)
    }
    
    func testSelectOption_WhenSelectPicPayAndPaymentRequestV2IsActive_ShouldCallPresentConsumersSearchV2() throws {
        let maxUsers = 2
        let data = PaymentRequestPermissionV2(maxUsers: maxUsers)
        let response: (model: PaymentRequestPermissionV2, data: Data?) = (model: data, data: nil)
        fakeService.paymentRequestPermissionV2 = .success(response)
        featureManager.override(key: .paymentRequestV2, with: true)
        featureManager.override(key: .featurePixHubReceiving, with: true)
        viewModel.presentOptions()
        viewModel.selectOption(at: PaymentRequestOption.picpay.rawValue)
        let isLoaderVisible = try XCTUnwrap(fakePresenter.isLoaderVisible)
        XCTAssertEqual(fakeService.getPaymentRequestPermissionV2CallCount, 1)
        XCTAssertEqual(fakePresenter.updateLoaderCallCount, 2)
        XCTAssertEqual(fakePresenter.presentConsumersSearchV2CallCount, 1)
        XCTAssertFalse(isLoaderVisible)
    }
    
    func testSelectOption_WhenSelectPicPayAndRetryLimitExceededAndPaymentRequestV2IsNotActive_ShouldCallRetryLimitExceededFlow() throws {
        let permission = PaymentRequestPermission(isP2PBlockDayChargesEnabled: true)
        let data = PaymentRequestPermissionData(data: permission)
        let response: (model: PaymentRequestPermissionData, data: Data?) = (model: data, data: nil)
        fakeService.paymentRequestPermission = .success(response)
        featureManager.override(key: .paymentRequestV2, with: false)
        featureManager.override(key: .featurePixHubReceiving, with: true)
        viewModel.presentOptions()
        viewModel.selectOption(at: PaymentRequestOption.picpay.rawValue)
        let isLoaderVisible = try XCTUnwrap(fakePresenter.isLoaderVisible)
        XCTAssertEqual(fakeService.getPaymentRequestPermissionCallCount, 1)
        XCTAssertEqual(fakePresenter.updateLoaderCallCount, 1)
        XCTAssertEqual(fakePresenter.presentRetryLimitExceededCallCount, 1)
        XCTAssertTrue(isLoaderVisible)
    }
    
    func testSelectOption_WhenSelectPicPayAndRetryLimitExceededAndPaymentRequestV2IsActive_ShouldCallRetryLimitExceededV2Flow() throws {
        let dialogModel = DialogErrorModel(title: "", message: "", buttonText: "")
        let code = PaymentRequestSelectorViewModel.ErrorCode.limitExceeded.rawValue
        let dialogResponse = DialogResponseError(code: code, model: dialogModel)
        let dialogError = DialogError.dialog(dialogResponse)
        fakeService.paymentRequestPermissionV2 = .failure(dialogError)
        featureManager.override(key: .paymentRequestV2, with: true)
        featureManager.override(key: .featurePixHubReceiving, with: true)
        viewModel.presentOptions()
        viewModel.selectOption(at: PaymentRequestOption.picpay.rawValue)
        let isLoaderVisible = try XCTUnwrap(fakePresenter.isLoaderVisible)
        XCTAssertEqual(fakeService.getPaymentRequestPermissionV2CallCount, 1)
        XCTAssertEqual(fakePresenter.updateLoaderCallCount, 1)
        XCTAssertEqual(fakePresenter.presentRetryLimitExceededV2CallCount, 1)
        XCTAssertTrue(isLoaderVisible)
    }
    
    func testSelectOption_WhenSelectPicPayAndReceiveUnmappedErrorAndPaymentRequestV2IsNotActive_ShouldCallPresentPicPaySelectionFailure() throws {
        let error = ApiError.unknown(nil)
        fakeService.paymentRequestPermission = .failure(error)
        featureManager.override(key: .paymentRequestV2, with: false)
        featureManager.override(key: .featurePixHubReceiving, with: true)
        viewModel.presentOptions()
        viewModel.selectOption(at: PaymentRequestOption.picpay.rawValue)
        let isLoaderVisible = try XCTUnwrap(fakePresenter.isLoaderVisible)
        XCTAssertEqual(fakeService.getPaymentRequestPermissionCallCount, 1)
        XCTAssertEqual(fakePresenter.updateLoaderCallCount, 1)
        XCTAssertEqual(fakePresenter.presentPicPaySelectionFailureCallCount, 1)
        XCTAssertTrue(isLoaderVisible)
    }
    
    func testSelectOption_WhenSelectPicPayAndReceiveUnmappedErrorAndPaymentRequestV2IsActive_ShouldCallPresentPicPaySelectionFailure() throws {
        let error = ApiError.unknown(nil)
        let dialogError = DialogError.default(error)
        fakeService.paymentRequestPermissionV2 = .failure(dialogError)
        featureManager.override(key: .paymentRequestV2, with: true)
        featureManager.override(key: .featurePixHubReceiving, with: true)
        viewModel.presentOptions()
        viewModel.selectOption(at: PaymentRequestOption.picpay.rawValue)
        let isLoaderVisible = try XCTUnwrap(fakePresenter.isLoaderVisible)
        XCTAssertEqual(fakeService.getPaymentRequestPermissionV2CallCount, 1)
        XCTAssertEqual(fakePresenter.updateLoaderCallCount, 1)
        XCTAssertEqual(fakePresenter.presentPicPaySelectionFailureCallCount, 1)
        XCTAssertTrue(isLoaderVisible)
    }
    
    func testSelectOption_WhenOriginIsDeeplinkAndTypeIsQrCode_ShouldCallPresentChargeUserValueWithTheGivenOriginAndType() throws {
        let type = PaymentRequestExternalType.qrCode
        featureManager.override(key: .featurePixHubReceiving, with: true)
        viewModel.presentOptions()
        viewModel.selectOption(at: PaymentRequestOption.qrcode.rawValue)
        let fakeOrigin = try XCTUnwrap(fakePresenter.origin)
        let fakeType = try XCTUnwrap(fakePresenter.type)
        XCTAssertEqual(fakePresenter.presentChargeUserValueCallCount, 1)
        XCTAssertEqual(fakeOrigin, origin)
        XCTAssertEqual(fakeType, type)
    }
    
    func testSelectOption_WhenOriginIsDeeplinkAndTypeIsLink_ShouldCallPresentChargeUserValueWithTheGivenOriginAndType() throws {
        let type = PaymentRequestExternalType.link
        featureManager.override(key: .featurePixHubReceiving, with: true)
        viewModel.presentOptions()
        viewModel.selectOption(at: PaymentRequestOption.link.rawValue)
        let fakeOrigin = try XCTUnwrap(fakePresenter.origin)
        let fakeType = try XCTUnwrap(fakePresenter.type)
        XCTAssertEqual(fakePresenter.presentChargeUserValueCallCount, 1)
        XCTAssertEqual(fakeOrigin, origin)
        XCTAssertEqual(fakeType, type)
    }
    
    func testSelectOption_WhenTypeIsPix_ShouldCallPresentPixReceivementScene() throws {
        featureManager.override(key: .featurePixHubReceiving, with: true)
        viewModel.presentOptions()
        viewModel.selectOption(at: PaymentRequestOption.pix
                                .rawValue)
        XCTAssertEqual(fakePresenter.presentPixReceivementSceneCallsCount, 1)
    }
    
    // MARK: - dismissViewController
    func testDismissViewControllerShouldCallDismissViewController() {
        viewModel.dismissViewController()
        XCTAssertEqual(fakePresenter.dismissViewControllerCallCount, 1)
    }
    
    // MARK: - didTouchOnLink
    func testDidTouchOnLinkShouldCallPresentKnowMore() {
        let knowMoreLink = "/\(PaymentRequestLocalizable.paymentRequestExceededAlertKnowMoreButton.text)"
        let url = URL(fileURLWithPath: knowMoreLink)
        viewModel.didTouchOnLink(URL: url)
        XCTAssertEqual(fakePresenter.presentKnowMoreCallCount, 1)
    }
    
    func testDidTouchOnLinkShouldNotCallPresentKnowMore() {
        let knowMoreLink = "/teste"
        let url = URL(fileURLWithPath: knowMoreLink)
        viewModel.didTouchOnLink(URL: url)
        XCTAssertEqual(fakePresenter.presentKnowMoreCallCount, 0)
    }
}
