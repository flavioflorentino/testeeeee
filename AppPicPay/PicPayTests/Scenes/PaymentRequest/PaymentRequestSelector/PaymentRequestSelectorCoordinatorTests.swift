@testable import PicPay
import PIX
import XCTest

final class PaymentRequestSelectorCoordinatorTests: XCTestCase {
    // MARK: - Variables
    private var coordinator: PaymentRequestSelectorCoordinating!
    private var navigationController: NavigationControllerMock!
    private var viewController: UIViewController!
    private let origin: DGHelpers.Origin = .deeplink
    
    // MARK: - Setup
    override func setUp() {
        viewController = UIViewController()
        navigationController = NavigationControllerMock(rootViewController: viewController)
        coordinator = PaymentRequestSelectorCoordinator()
        coordinator.viewController = viewController
    }
    
    // MARK: - Perform
    func testPerformShouldCallDismiss() {
        coordinator.perform(action: .close)
        XCTAssertTrue(navigationController.isDismissViewControllerCalled)
    }
    
    func testPerformSelectionShouldPushNewViewController() {
        coordinator.perform(action: .consumersSearch)
        XCTAssertNotNil(navigationController.pushedViewController)
    }
    
    func testPerformSelection_WhenActionIsConsumersSearchV2_ShouldPushNewViewController() {
        coordinator.perform(action: .consumersSearchV2(maxUsers: 1))
        XCTAssertNotNil(navigationController.pushedViewController)
    }
    
    func testPerform_WhenActionIsChargeUserValueAndTypeIsLink_ShouldPushPaymentRequestValueViewController() {
        coordinator.perform(action: .chargeUserValue(origin: origin, type: .link))
        XCTAssertTrue(navigationController.pushedViewController is PaymentRequestValueViewController)
    }
    
    func testPerform_WhenActionIsChargeUserValueAndTypeIsQrCode_ShouldPushPaymentRequestValueViewController() {
        coordinator.perform(action: .chargeUserValue(origin: origin, type: .qrCode))
        XCTAssertTrue(navigationController.pushedViewController is PaymentRequestValueViewController)
    }
    
    func testPerform_WhenActionIsPixReceivement_ShouldPushViewController() {
        coordinator.perform(action: .chargeUserValue(origin: origin, type: .qrCode))
        XCTAssertNotNil(navigationController.pushedViewController)
    }
}
