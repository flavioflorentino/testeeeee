import XCTest
@testable import PicPay

final class PaymentRequestValueCoordinatorTests: XCTestCase {
    // MARK: - Variables
    private lazy var controller = ViewControllerMock()
    private lazy var navigationController = NavigationControllerMock(rootViewController: controller)
    private lazy var coordinator = createCoordinatorWithType(.link)
    
    // MARK: - perform
    func testPerform_ShouldCallDismiss_WhenActionIsClose() {
        coordinator.perform(action: .close)
        XCTAssertTrue(navigationController.isDismissViewControllerCalled)
    }
    
    func testPerform_ShouldCallPop_WhenActionIsBack() {
        coordinator.perform(action: .back)
        XCTAssertTrue(navigationController.isPopViewControllerCalled)
    }
    
    func testPerform_WhenActionIsConfirmValueAndTypeIsQrCode_ShouldCallPushWithPaymentRequestQrCodeViewController() {
        let qrCodeCoordinator = createCoordinatorWithType(.qrCode)
        qrCodeCoordinator.perform(action: .confirmValue(1.00))
        XCTAssertTrue(navigationController.isPushViewControllerCalled)
        XCTAssertTrue(navigationController.pushedViewController is PaymentRequestQrCodeViewController)
    }
    
    func testPerform_WhenActionIsConfirmValueAndTypeIsLink_ShouldCallPresentWithUIActivityViewController() {
        let linkCoordinator = createCoordinatorWithType(.link)
        linkCoordinator.perform(action: .confirmValue(1.00))
        XCTAssertEqual(controller.didPresentControllerCount, 1)
        XCTAssertTrue(controller.viewControllerPresented is UIActivityViewController)
    }
    
    func testPerform_WhenActionIsDoNotSetValueAndTypeIsQrCode_ShouldCallPushWithPaymentRequestQrCodeViewController() {
        let qrCodeCoordinator = createCoordinatorWithType(.qrCode)
        qrCodeCoordinator.perform(action: .doNotSetValue)
        XCTAssert(navigationController.isPushViewControllerCalled)
        XCTAssertTrue(navigationController.pushedViewController is PaymentRequestQrCodeViewController)
    }
    
    func testPerform_WhenActionIsDoNotSetValueAndTypeIsLink_ShouldCallPresentWithUIActivityViewController() {
        let linkCoordinator = createCoordinatorWithType(.link)
        linkCoordinator.perform(action: .doNotSetValue)
        XCTAssertEqual(controller.didPresentControllerCount, 1)
        XCTAssertTrue(controller.viewControllerPresented is UIActivityViewController)
    }
}

private extension PaymentRequestValueCoordinatorTests {
    func createCoordinatorWithType(_ type: PaymentRequestExternalType) -> PaymentRequestValueCoordinator {
        let consumerManagerMock = ConsumerManagerMock()
        consumerManagerMock.consumer = MBConsumer()
        consumerManagerMock.consumer?.username = "Test"
        let mockDependencies = DependencyContainerMock(consumerManagerMock)
        let coordinator = PaymentRequestValueCoordinator(type: type, dependencies: mockDependencies)
        coordinator.viewController = navigationController.topViewController
        return coordinator
    }
}
