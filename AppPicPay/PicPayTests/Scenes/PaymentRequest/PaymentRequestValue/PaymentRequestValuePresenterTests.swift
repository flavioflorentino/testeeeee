import XCTest
@testable import PicPay

private final class PaymentRequestValueCoordinatorMock: PaymentRequestValueCoordinating {
    var viewController: UIViewController?
    private(set) var closeCallCount = 0
    private(set) var backCallCount = 0
    private(set) var confirmValueCallCount = 0
    private(set) var doNotSetValueCallCount = 0
    private(set) var value: Double?
    
    func perform(action: PaymentRequestValueAction) {
        switch action {
        case .close:
            closeCallCount += 1
        case .back:
            backCallCount += 1
        case let .confirmValue(value):
            confirmValueCallCount += 1
            self.value = value
        case .doNotSetValue:
            doNotSetValueCallCount += 1
        }
    }
}

private final class PaymentRequestValueViewControllerMock: PaymentRequestValueDisplay {
    private(set) var displayConfirmStateCallCount = 0
    private(set) var isConfirmEnabled: Bool?
    
    func displayConfirmState(isEnabled: Bool) {
        displayConfirmStateCallCount += 1
        isConfirmEnabled = isEnabled
    }
}

final class PaymentRequestValuePresenterTests: XCTestCase {
    // MARK: - Variables
    private let viewController = PaymentRequestValueViewControllerMock()
    private let coordinator = PaymentRequestValueCoordinatorMock()
    private lazy var presenter: PaymentRequestValuePresenter = {
        let presenter = PaymentRequestValuePresenter(coordinator: coordinator)
        presenter.viewController = viewController
        return presenter
    }()
    
    // MARK: - perform
    func testPerform_ShouldCallClose_WhenActionClose() {
        presenter.perform(action: .close)
        XCTAssertEqual(coordinator.closeCallCount, 1)
    }
    
    func testPerform_ShouldCallBack_WhenActionBack() {
        presenter.perform(action: .back)
        XCTAssertEqual(coordinator.backCallCount, 1)
    }
    
    func testPerform_ShouldCallConfirmValueWithTheGivenValue_WhenActionConfirmValue() {
        let value: Double = 10.0
        presenter.perform(action: .confirmValue(value))
        XCTAssertEqual(coordinator.confirmValueCallCount, 1)
        XCTAssertEqual(coordinator.value, value)
    }
    
    func testPerform_ShouldCallDoNotSetValue_WhenActionDoNotSetValue() {
        presenter.perform(action: .doNotSetValue)
        XCTAssertEqual(coordinator.doNotSetValueCallCount, 1)
    }
    
    // MARK: - presentConfirmState
    func testPresentConfirmState_ShouldCallDisplayConfirmStateWithTheGivenIsEnabled() {
        let value = true
        presenter.presentConfirmState(isEnabled: value)
        XCTAssertEqual(viewController.displayConfirmStateCallCount, 1)
        XCTAssertEqual(viewController.isConfirmEnabled, value)
    }
}
