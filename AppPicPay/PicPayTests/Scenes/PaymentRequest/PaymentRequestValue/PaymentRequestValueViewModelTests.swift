@testable import PicPay
import XCTest

private final class PaymentRequestValuePresenterMock: PaymentRequestValuePresenting {
    private(set) var closeCallCount = 0
    private(set) var backCallCount = 0
    private(set) var confirmValueCallCount = 0
    private(set) var doNotSetValueCallCount = 0
    private(set) var presentConfirmStateCallCount = 0
    private(set) var value: Double?
    private(set) var isConfirmButtonEnabled: Bool?
        
    func perform(action: PaymentRequestValueAction) {
        switch action {
        case .close:
            closeCallCount += 1
        case .back:
            backCallCount += 1
        case let .confirmValue(value):
            confirmValueCallCount += 1
            self.value = value
        case .doNotSetValue:
            doNotSetValueCallCount += 1
        }
    }

    func presentConfirmState(isEnabled: Bool) {
        isConfirmButtonEnabled = isEnabled
        presentConfirmStateCallCount += 1
    }
}

final class PaymentRequestValueViewModelTests: XCTestCase {
    // MARK: - Variables
    private let presenter = PaymentRequestValuePresenterMock()
    private lazy var viewModel = PaymentRequestValueViewModel(presenter: presenter, isBackActionEnabled: isBackActionEnabled)
    private var isBackActionEnabled = true
    
    // MARK: - dismiss
    func testDismiss_ShouldCallClose_WhenBackActionEnabledIsFalse() {
        isBackActionEnabled = false
        viewModel.dismiss()
        XCTAssertEqual(presenter.closeCallCount, 1)
    }
    
    func testDismiss_ShouldCallBack_WhenBackActionEnabledIsTrue() {
        isBackActionEnabled = true
        viewModel.dismiss()
        XCTAssertEqual(presenter.backCallCount, 1)
    }
    
    // MARK: - next
    func testNext_ShouldCallConfirmValueAndHasTheGivenValue() {
        let value: Double = 10.0
        viewModel.newValue(value)
        viewModel.next()
        XCTAssertEqual(presenter.presentConfirmStateCallCount, 1)
        XCTAssertEqual(presenter.confirmValueCallCount, 1)
        XCTAssertEqual(presenter.value, value)
    }
    
    // MARK: - nextWithoutValue
    func testNextWithoutValue_ShouldCallDoNotSetValue() {
        viewModel.nextWithoutValue()
        XCTAssertEqual(presenter.doNotSetValueCallCount, 1)
    }
    
    // MARK: - newValue
    func testNewValue_ShouldEnableConfirmButton_WhenValueGreaterThanZero() {
        let value: Double = 10.0
        viewModel.newValue(value)
        XCTAssertEqual(presenter.presentConfirmStateCallCount, 1)
        XCTAssertEqual(presenter.isConfirmButtonEnabled, true)
    }
    
    func testNewValue_ShouldDisableConfirmButton_WhenValueEqualZero() {
        let value: Double = .zero
        viewModel.newValue(value)
        XCTAssertEqual(presenter.presentConfirmStateCallCount, 1)
        XCTAssertEqual(presenter.isConfirmButtonEnabled, false)
    }
}
