import XCTest
@testable import PicPay

final class PaymentRequestValueFactoryTests: XCTestCase {
    // MARK: - Make
    func testMake_ShouldCreateAPaymentRequestValueViewController_WhenHasNavigationControllerIsFalse() {
        let viewController = PaymentRequestValueFactory.make(origin: .deeplink, type: .link, hasNavigationController: false)
        XCTAssertTrue(viewController is PaymentRequestValueViewController)
    }
    
    func testMake_ShouldCreateANavigationController_WhenHasNavigationControllerIsTrue() {
        let viewController = PaymentRequestValueFactory.make(origin: .deeplink, type: .link, hasNavigationController: true)
        XCTAssertTrue(viewController is UINavigationController)
    }
}
