import Core
import PIX
import XCTest

@testable import PicPay

class PixPasteboardInteractorTests: XCTestCase {
    // MARK: - Properties
    private lazy var kvStoreMock = KVStoreMock()
    
    private let featureManagerMock: FeatureManagerMock = {
        let mock = FeatureManagerMock()
        mock.override(key: .isPixCopiedKeyTransactionsPopUpAvailable, with: true)
        mock.override(keys: .featurePixkeyPhoneWithDDI, with: false)
        return mock
    }()
    
    private lazy var dependenciesMock = DependencyContainerMock(kvStoreMock, featureManagerMock)
    
    private lazy var sut = PixPasteboardInteractor(dependencies: dependenciesMock)
    
    // MARK: - Tests
    func testVerifyIfHasCopiedKey_WhenHasNotACopiedKey_ShouldNotCallShowCopiedKeyAlertAction() {
        var copiedKey: String?
        var copiedKeyType: KeyType?
        var isShowCopiedKeyAlertActionCalled = false
        
        UIPasteboard.general.string = ""
        
        sut.verifyIfHasCopiedKey { (keyType, key) in
            isShowCopiedKeyAlertActionCalled = true
            copiedKey = key
            copiedKeyType = keyType
        }
        
        XCTAssertFalse(isShowCopiedKeyAlertActionCalled)
        XCTAssertNil(copiedKey)
        XCTAssertNil(copiedKeyType)
    }
    
    func testVerifyIfHasCopiedKey_WhenHasCopiedACPFKey_ShouldNotCallshowCopiedKeyAlertAction() {
        let pixKey = "950.839.650-46"
        let pixKeyType: KeyType = .cpf
        UIPasteboard.general.string = pixKey
        
        var copiedKey: String?
        var copiedKeyType: KeyType?
        var isShowCopiedKeyAlertActionCalled = false
        
        dependenciesMock.kvStore.set(value: "", with: KVKey.lastPix)

        sut.verifyIfHasCopiedKey { keyType, key in
            isShowCopiedKeyAlertActionCalled = true
            copiedKey = key
            copiedKeyType = keyType
        }
        
        XCTAssertTrue(isShowCopiedKeyAlertActionCalled)
        XCTAssertEqual(copiedKey, pixKey)
        XCTAssertEqual(copiedKeyType, pixKeyType)
    }
    
    func testVerifyIfHasCopiedKey_WhenHasCopiedACNPJKey_ShouldNotCallshowCopiedKeyAlertAction() {
        let pixKey = "67.530.111/0001-80"
        let pixKeyType: KeyType = .cnpj
        UIPasteboard.general.string = pixKey
        
        var copiedKey: String?
        var copiedKeyType: KeyType?
        var isShowCopiedKeyAlertActionCalled = false
        
        dependenciesMock.kvStore.set(value: "", with: KVKey.lastPix)

        sut.verifyIfHasCopiedKey { keyType, key in
            isShowCopiedKeyAlertActionCalled = true
            copiedKey = key
            copiedKeyType = keyType
        }
        
        XCTAssertTrue(isShowCopiedKeyAlertActionCalled)
        XCTAssertEqual(copiedKey, pixKey)
        XCTAssertEqual(copiedKeyType, pixKeyType)
    }
    
    func testVerifyIfHasCopiedKey_WhenHasCopiedAnEmailKey_ShouldNotCallshowCopiedKeyAlertAction() {
        let pixKey = "teste@test.com"
        let pixKeyType: KeyType = .email
        UIPasteboard.general.string = pixKey
        
        var copiedKey: String?
        var copiedKeyType: KeyType?
        var isShowCopiedKeyAlertActionCalled = false
        
        dependenciesMock.kvStore.set(value: "", with: KVKey.lastPix)

        sut.verifyIfHasCopiedKey { keyType, key in
            isShowCopiedKeyAlertActionCalled = true
            copiedKey = key
            copiedKeyType = keyType
        }
        
        XCTAssertTrue(isShowCopiedKeyAlertActionCalled)
        XCTAssertEqual(copiedKey, pixKey)
        XCTAssertEqual(copiedKeyType, pixKeyType)
    }
    
    func testVerifyIfHasCopiedKey_WhenHasCopiedAnPhoneKeyAndDDDFeatureFlagIsOff_ShouldNotCallshowCopiedKeyAlertAction() {
        let pixKey = "2134567891"
        let pixKeyType: KeyType = .phone
        UIPasteboard.general.string = pixKey
        
        var copiedKey: String?
        var copiedKeyType: KeyType?
        var isShowCopiedKeyAlertActionCalled = false
        
        dependenciesMock.kvStore.set(value: "", with: KVKey.lastPix)

        sut.verifyIfHasCopiedKey { keyType, key in
            isShowCopiedKeyAlertActionCalled = true
            copiedKey = key
            copiedKeyType = keyType
        }
        
        XCTAssertTrue(isShowCopiedKeyAlertActionCalled)
        XCTAssertEqual(copiedKey, pixKey)
        XCTAssertEqual(copiedKeyType, pixKeyType)
    }
    
    func testVerifyIfHasCopiedKey_WhenHasCopiedAnPhoneKeyAndDDDFeatureFlagIsOn_ShouldNotCallshowCopiedKeyAlertAction() {
        let pixKey = "+5512345678"
        let pixKeyType: KeyType = .phone
        UIPasteboard.general.string = pixKey
        
        var copiedKey: String?
        var copiedKeyType: KeyType?
        var isShowCopiedKeyAlertActionCalled = false
        
        dependenciesMock.kvStore.set(value: "", with: KVKey.lastPix)
        featureManagerMock.override(key: .featurePixkeyPhoneWithDDI, with: true)

        sut.verifyIfHasCopiedKey { keyType, key in
            isShowCopiedKeyAlertActionCalled = true
            copiedKey = key
            copiedKeyType = keyType
        }
        
        XCTAssertTrue(isShowCopiedKeyAlertActionCalled)
        XCTAssertEqual(copiedKey, pixKey)
        XCTAssertEqual(copiedKeyType, pixKeyType)
    }
    
    func testVerifyIfHasCopiedKey_WhenHasCopiedACPFKeyAndLastPixHasTheSameKey_ShouldNotCallshowCopiedKeyAlertAction() {
        let pixKey = "950.839.650-46"
        UIPasteboard.general.string = pixKey
        
        var copiedKey: String?
        var copiedKeyType: KeyType?
        var isShowCopiedKeyAlertActionCalled = false
        
        dependenciesMock.kvStore.set(value: "950.839.650-46", with: KVKey.lastPix)

        sut.verifyIfHasCopiedKey { keyType, key in
            isShowCopiedKeyAlertActionCalled = true
            copiedKey = key
            copiedKeyType = keyType
        }
        
        XCTAssertFalse(isShowCopiedKeyAlertActionCalled)
        XCTAssertNil(copiedKey)
        XCTAssertNil(copiedKeyType)
    }
    
    func testVerifyIfHasCopiedKey_WhenHasACopiedKeyAndFeatureFlagIsOff_ShouldNotCallshowCopiedKeyAlertAction() {
        let pixKey = "950.839.650-46"
        UIPasteboard.general.string = pixKey
        
        var copiedKey: String?
        var copiedKeyType: KeyType?
        var isShowCopiedKeyAlertActionCalled = false
        
        dependenciesMock.kvStore.set(value: "", with: KVKey.lastPix)
        featureManagerMock.override(keys: .isPixCopiedKeyTransactionsPopUpAvailable, with: false)
        
        sut.verifyIfHasCopiedKey { (keyType, key) in
            isShowCopiedKeyAlertActionCalled = true
            copiedKey = key
            copiedKeyType = keyType
        }
        
        XCTAssertFalse(isShowCopiedKeyAlertActionCalled)
        XCTAssertNil(copiedKey)
        XCTAssertNil(copiedKeyType)
    }
}
