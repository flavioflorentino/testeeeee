import Core
import UIKit
import XCTest

@testable import PicPay

final class BilletPasteboardWrapperTests: XCTestCase {
    // MARK: - Properties
    private lazy var pasteboardName = UIPasteboard.Name(String(describing: Self.self))

    private var pasteboard: UIPasteboard!
    private var validatorStub: BilletCodeValidatorStub!
    private var sut: BilletPasteboardWrapper!

    // MARK: - Setup
    override func setUpWithError() throws {
        try super.setUpWithError()

        pasteboard = try XCTUnwrap(UIPasteboard(name: pasteboardName, create: true))

        validatorStub = BilletCodeValidatorStub()

        sut = BilletPasteboardWrapper(pasteboard: pasteboard, validator: validatorStub, dependencies: DispatchQueueFake())
    }

    // MARK: - Teardown
    override func tearDownWithError() throws {
        UIPasteboard.remove(withName: pasteboardName)

        try super.tearDownWithError()
    }

    func testCopiedBillet_WhenHasAValidBilletCode_ShouldCallCompletionWithSuccess() {
        validatorStub.isValid = true

        let billetCode = "00000000000-0 00000000000-0 00000000000-0 00000000000-0"
        pasteboard.string = billetCode

        var result: Result<String, BilletPasteboardError>?

        let expectation = XCTestExpectation()
        sut.copiedBillet {
            result = $0
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1)

        XCTAssertNotNil(result)
        XCTAssertEqual(result, .success(billetCode.onlyNumbers))
    }

    func testCopiedBillet_WhenHasAnInvalidBilletCode_ShouldCallCompletionWithFailure() {
        let billetCodesAndValidatorReturns: [String: Bool] = [
            "00000000000-0 00000000000-0 00000000000-0 00000000000-": false,
            "test": true
        ]

        for (billetCode, isValid) in billetCodesAndValidatorReturns {
            validatorStub.isValid = isValid
            pasteboard.string = billetCode

            var result: Result<String, BilletPasteboardError>?

            let expectation = XCTestExpectation()
            sut.copiedBillet {
                result = $0
                expectation.fulfill()
            }

            wait(for: [expectation], timeout: 1)

            XCTAssertNotNil(result)
            XCTAssertEqual(result, .failure(.notABilletCode))
        }
    }
}

private final class DispatchQueueFake: HasMainQueue {
    private (set) lazy var mainQueue = DispatchQueue(label: String(describing: Self.self))
}
