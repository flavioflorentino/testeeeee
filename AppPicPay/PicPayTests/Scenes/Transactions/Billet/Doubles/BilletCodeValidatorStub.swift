import Core

@testable import PicPay

final class BilletCodeValidatorStub {
    // MARK: - Properties
    var isValid: Bool = false
}

// MARK: - BilletCodeValidating
extension BilletCodeValidatorStub: BilletCodeValidating {
    func validate(code: String?) -> Bool {
        isValid
    }
}
