import Core

@testable import PicPay

final class BilletPasteboardWrapperStub {
    // MARK: - Properties
    var copiedBilletResult: Result<String, BilletPasteboardError> = .failure(.notABilletCode)
}

// MARK: - BilletPasteboardWrapping
extension BilletPasteboardWrapperStub: BilletPasteboardWrapping {
    func copiedBillet(_ completion: @escaping (Result<String, BilletPasteboardError>) -> Void) {
        completion(copiedBilletResult)
    }
}
