import Core
import XCTest

@testable import PicPay

final class BilletPasteboardInteractorTests: XCTestCase {
    // MARK: - Properties
    private lazy var pasteboardStub = BilletPasteboardWrapperStub()
    private lazy var fakeDependencies = FakeDependencies()

    private lazy var sut = BilletPasteboardInteractor(
        pasteboard: pasteboardStub,
        dependencies: fakeDependencies
    )

    // MARK: - Tests
    func testVerifyIfHasCopiedBillet_WhenHasABilletCodeCopied_ShouldCallShowCopiedBilletAlertAction() {
        let billetCode = "00000.00000 00000.000000 00000.000000 0 00000000000000"
        pasteboardStub.copiedBilletResult = .success(billetCode.onlyNumbers)

        var isShowCopiedBilletAlertActionCalled = false
        var codeCopied: String?

        sut.verifyIfHasCopiedBillet { code in
            isShowCopiedBilletAlertActionCalled = true
            codeCopied = code
        }

        XCTAssertTrue(isShowCopiedBilletAlertActionCalled)
        XCTAssertEqual(codeCopied, billetCode)
    }

    func testVerifyIfHasCopiedBillet_WhenHasAnInvalidBilletCodeCopied_ShouldNotCallShowCopiedBilletAlertAction() {
        pasteboardStub.copiedBilletResult = .failure(.notABilletCode)

        var isShowCopiedBilletAlertActionCalled = false
        var codeCopied: String?

        sut.verifyIfHasCopiedBillet { code in
            isShowCopiedBilletAlertActionCalled = true
            codeCopied = code
        }

        XCTAssertFalse(isShowCopiedBilletAlertActionCalled)
        XCTAssertNil(codeCopied)
    }

    func testVerifyIfHasCopiedBillet_WhenHasABilletCodeCopiedAndDifferentFromTheLastOneSaved_ShouldCallShowCopiedBilletAlertAction() {
        fakeDependencies.kvStore.set(value: "", with: KVKey.lastBoleto)

        let billetCode = "00000.00000 00000.000000 00000.000000 0 00000000000000"
        pasteboardStub.copiedBilletResult = .success(billetCode.onlyNumbers)

        var isShowCopiedBilletAlertActionCalled = false
        var codeCopied: String?

        sut.verifyIfHasCopiedBillet { code in
            isShowCopiedBilletAlertActionCalled = true
            codeCopied = code
        }

        XCTAssertTrue(isShowCopiedBilletAlertActionCalled)
        XCTAssertEqual(codeCopied, billetCode)
    }

    func testVerifyIfHasCopiedBillet_WhenHasABilletCodeCopiedAndDifferentFromTheLastOneSaved_ShouldSaveCopiedCode() {
        fakeDependencies.kvStore.set(value: "", with: KVKey.lastBoleto)

        let billetCode = "00000.00000 00000.000000 00000.000000 0 00000000000000"
        pasteboardStub.copiedBilletResult = .success(billetCode.onlyNumbers)

        sut.verifyIfHasCopiedBillet { _ in }

        let savedCode = fakeDependencies.kvStore.stringFor(KVKey.lastBoleto)
        XCTAssertEqual(savedCode, billetCode.onlyNumbers)
    }

    func testVerifyIfHasCopiedBillet_WhenHasABilletCodeCopiedAndEqualsTheLastOneSaved_ShouldNotCallShowCopiedBilletAlertAction() {
        let billetCode = "00000.00000 00000.000000 00000.000000 0 00000000000000"
        fakeDependencies.kvStore.set(value: billetCode.onlyNumbers, with: KVKey.lastBoleto)

        pasteboardStub.copiedBilletResult = .success(billetCode.onlyNumbers)

        var isShowCopiedBilletAlertActionCalled = false
        var codeCopied: String?

        sut.verifyIfHasCopiedBillet { code in
            isShowCopiedBilletAlertActionCalled = true
            codeCopied = code
        }

        XCTAssertFalse(isShowCopiedBilletAlertActionCalled)
        XCTAssertNil(codeCopied)
    }
}

private final class FakeDependencies: HasKVStore {
    private (set) var kvStore: KVStoreContract

    init() {
        self.kvStore = KVStoreMock()
    }
}
