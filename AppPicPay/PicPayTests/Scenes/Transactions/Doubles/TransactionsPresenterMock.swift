import Core
import XCTest

@testable import PicPay

final class TransactionsPresenterMock: MockVerifier {
    // MARK: - Properties
    private var presentTransactionsCount: Int = 0
    private var transactions: [TransactionSection]?

    private var openItemCount: Int = 0
    private var item: TransactionItem?

    private var presentLoadingCount: Int = 0
    private var hideLoadingCount: Int = 0

    private var presentErrorCount: Int = 0
    private var presentedError: ApiError?
    private var hideErrorCount: Int = 0

    private var openSearchCount: Int = 0

    // MARK: - Verifiers
    func verifyPresent(transactions expectedTransactions: [TransactionSection], file: StaticString = #file, line: UInt = #line) {
        guard verifyMethodCalledOnce(
                methodName: "present",
                callCount: presentTransactionsCount,
                describeArguments: "transactions: \(String(describing: transactions))",
                file: file,
                line: line) else {
            return
        }

        XCTAssertEqual(transactions, expectedTransactions, file: file, line: line)
    }

    func verifyOpen(item expectedItem: TransactionItem, file: StaticString = #file, line: UInt = #line) {
        guard verifyMethodCalledOnce(
                methodName: "open",
                callCount: openItemCount,
                describeArguments: "item: \(String(describing: item))",
                file: file,
                line: line) else {
            return
        }

        XCTAssertEqual(item, expectedItem, file: file, line: line)
    }

    func verifyPresentLoading(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "presentLoading", callCount: presentLoadingCount, file: file, line: line)
    }

    func verifyHideLoading(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "hideLoading", callCount: hideLoadingCount, file: file, line: line)
    }

    func verifyPresent(error expectedError: ApiError, file: StaticString = #file, line: UInt = #line) {
        guard verifyMethodCalledOnce(
                methodName: "present",
                callCount: presentErrorCount,
                describeArguments: "error: \(String(describing: presentedError))",
                file: file,
                line: line) else {
            return
        }

        assertEqual(error: presentedError, expected: expectedError, file: file, line: line)
    }

    func verifyHideError(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "hideError", callCount: hideErrorCount, file: file, line: line)
    }

    func verifyOpenSearch(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "openSearch", callCount: openSearchCount, file: file, line: line)
    }
}

// MARK: - TransactionsPresenting
extension TransactionsPresenterMock: TransactionsPresenting {
    func present(transactions: [TransactionSection]) {
        presentTransactionsCount += 1
        self.transactions = transactions
    }

    func open(item: TransactionItem) {
        openItemCount += 1
        self.item = item
    }

    func openSearch() {
        openSearchCount += 1
    }

    func presentLoading() {
        presentLoadingCount += 1
    }

    func hideLoading() {
        hideLoadingCount += 1
    }

    func present(error: ApiError) {
        presentErrorCount += 1
        presentedError = error
    }

    func hideError() {
        hideErrorCount += 1
    }
}

// MARK: - Private
private extension TransactionsPresenterMock {
    func assertEqual(error: ApiError?, expected: ApiError?, file: StaticString = #file, line: UInt = #line) {
        guard !isEqual(lhs: error, rhs: expected) else { return }
        XCTFail("\(String(describing: error)) is not equal to expected \(String(describing: expected))", file: file, line: line)
    }

    func isEqual(lhs: ApiError?, rhs: ApiError?) -> Bool {
        switch (lhs, rhs) {
        case (nil, nil),
             (.notFound, .notFound),
             (.unauthorized, .unauthorized),
             (.badRequest, .badRequest),
             (.tooManyRequests, .tooManyRequests),
             (.otherErrors, .otherErrors),
             (.connectionFailure, .connectionFailure),
             (.cancelled, .cancelled),
             (.serverError, .serverError),
             (.timeout, .timeout),
             (.bodyNotFound, .bodyNotFound),
             (.upgradeRequired, .upgradeRequired),
             (.decodeError, .decodeError),
             (.unknown, .unknown):
            return true
        case let (.malformedRequest(lhs), .malformedRequest(rhs)):
            return lhs == rhs
        default:
            return false
        }
    }
}
