import Core

@testable import PicPay

final class TransactionsServiceStub {
    var transactionsResult: Result<[TransactionSection], ApiError>?
}

// MARK: - TransactionsServicing
extension TransactionsServiceStub: TransactionsServicing {
    func sections(_ completion: @escaping (Result<[TransactionSection], ApiError>) -> Void) {
        guard let result = transactionsResult else { return completion(.failure(.unknown(nil))) }
        completion(result)
    }
}
