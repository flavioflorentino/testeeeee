import XCTest

@testable import PicPay

final class TransactionsDisplayMock: MockVerifier {
    // MARK: - Properties
    private var showSectionsCount: Int = 0
    private var sections: [TransactionSectionViewModel]?

    private var showLoadingCount: Int = 0
    private var hideLoadingCount: Int = 0

    private var showNoConnectionErrorCount: Int = 0
    private var showUnknownErrorCount: Int = 0
    private var hideErrorCount: Int = 0

    // MARK: - Verifiers
    func verifyShow(sections expectedSections: [TransactionSectionViewModel], file: StaticString = #file, line: UInt = #line) {
        guard verifyMethodCalledOnce(
                methodName: "show",
                callCount: showSectionsCount,
                describeArguments: "sections: \(String(describing: sections))",
                file: file,
                line: line) else {
            return
        }

        XCTAssertEqual(sections, expectedSections, file: file, line: line)
    }

    func verifyShowLoading(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "showLoading", callCount: showLoadingCount, file: file, line: line)
    }

    func verifyHideLoading(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "hideLoading", callCount: hideLoadingCount, file: file, line: line)
    }

    func verifyShowNoConnectionError(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "showNoConnectionError", callCount: showNoConnectionErrorCount, file: file, line: line)
    }

    func verifyShowUnknownError(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "showUnknownError", callCount: showUnknownErrorCount, file: file, line: line)
    }

    func verifyHideError(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "hideError", callCount: hideErrorCount, file: file, line: line)
    }
}

// MARK: - TransactionsDisplay
extension TransactionsDisplayMock: TransactionsDisplay {
    func show(sections: [TransactionSectionViewModel]) {
        showSectionsCount += 1
        self.sections = sections
    }

    func showLoading() {
        showLoadingCount += 1
    }

    func hideLoading() {
        hideLoadingCount += 1
    }

    func showNoConnectionError() {
        showNoConnectionErrorCount += 1
    }

    func showUnknownError() {
        showUnknownErrorCount += 1
    }

    func hideError() {
        hideErrorCount += 1
    }
}
