import AnalyticsModule
import Core
import XCTest

@testable import PicPay

final class TransactionsInteractorTests: XCTestCase {
    // MARK: - Properties
    private lazy var serviceStub = TransactionsServiceStub()
    private lazy var presenterMock = TransactionsPresenterMock()
    private lazy var analyticsSpy = AnalyticsSpy()

    private lazy var sut = TransactionsInteractor(
        service: serviceStub,
        presenter: presenterMock,
        dependencies: DependencyContainerMock(analyticsSpy)
    )

    // MARK: - Tests
    func testFetchTransactions_WhenServiceReturnsSuccess_ShouldCallPresentTransactionsOnPresenter() {
        let transactions = makeTransactions()
        serviceStub.transactionsResult = .success(transactions)

        sut.fetchTransactions()

        presenterMock.verifyPresent(transactions: transactions)
    }

    func testFetchTransactions_WhenCalled_ShouldCallPresentLoadingOnPresenter() {
        serviceStub.transactionsResult = .success([])

        sut.fetchTransactions()

        presenterMock.verifyPresentLoading()
    }

    func testFetchTransactions_WhenServiceReturnsSuccess_ShouldCallHideLoadingOnPresenter() {
        serviceStub.transactionsResult = .success([])
        sut.fetchTransactions()

        presenterMock.verifyHideLoading()
    }

    func testFetchTransactions_WhenServiceReturnsFailure_ShouldCallHideLoadingOnPresenter() {
        serviceStub.transactionsResult = .failure(.unknown(nil))
        sut.fetchTransactions()

        presenterMock.verifyHideLoading()
    }

    func testFetchTransactions_WhenServiceReturnsFailure_ShouldCallPresentErrorOnPresenter() {
        let error = ApiError.unknown(nil)

        serviceStub.transactionsResult = .failure(error)
        sut.fetchTransactions()

        presenterMock.verifyPresent(error: error)
    }

    func testFetchTransactions_WhenCalled_ShouldCallHideErrorOnPresenter() {
        serviceStub.transactionsResult = .success([])
        sut.fetchTransactions()

        presenterMock.hideError()
    }

    func testDidSetectItem_WhenCalled_ShouldCallOpenItemOnPresenter() {
        let item = makeTransactionItem()
        let expectedEvent = PaymentEvent.itemAccessed(
            type: "",
            id: nil,
            name: item.trackingData.type,
            section: (name: item.trackingData.section, position: item.trackingData.order)
        ).event()

        sut.didSelect(item: item)

        presenterMock.verifyOpen(item: item)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent), "Tracked wrong event")
    }

    func testTapAtSearch_WhenCalled_ShouldCallOpenSearchOnPresenter() {
        sut.tapAtSearch()

        presenterMock.verifyOpenSearch()
    }
}

// MARK: - Private
private extension TransactionsInteractorTests {
    func makeTransactions() -> [TransactionSection] {
        [
            .main(groups: makeTransactionGroups()),
            .services(groups: makeTransactionGroups())
        ]
    }

    func makeTransactionGroups() -> [TransactionGroup] {
        [
            .carousel(items: makeTransactionItems(count: 5)),
            .column(items: makeTransactionItems())
        ]
    }

    func makeTransactionItems(count: Int = 10) -> [TransactionItem] {
        (0 ... count).map { _ in makeTransactionItem() }
    }

    func makeTransactionItem() -> TransactionItem {
        TransactionItem(
            type: .local,
            title: "Test",
            description: "Test",
            image: nil,
            imageDark: nil,
            icon: nil,
            deeplink: nil,
            trackingData: .init(type: "Test", section: "Test", order: 0)
        )
    }
}
