import Core
import XCTest

@testable import PicPay

final class TransactionsLocalAndRemoteServiceTests: XCTestCase {
    private typealias ServiceResult = Result<[TransactionSection], ApiError>

    // MARK: - Properties
    private lazy var localServiceStub = TransactionsServiceStub()
    private lazy var remoteServiceStub = TransactionsServiceStub()

    private lazy var sut = TransactionsLocalAndRemoteService(localService: localServiceStub, remoteService: remoteServiceStub)

    // MARK: - Tests
    func testSections_WhenRemoteServiceReturnsSuccess_ShouldCallCompletion() {
        let expectedTransactions = makeTransactions()
        remoteServiceStub.transactionsResult = .success(expectedTransactions)


        var completionCount: Int = 0
        var result: ServiceResult?
        
        sut.sections {
            completionCount += 1
            result = $0
        }

        XCTAssertEqual(completionCount, 1)
        XCTAssertNotNil(result)

        switch result {
        case let .success(transactions):
            XCTAssertEqual(transactions, expectedTransactions)
        default:
            XCTFail("Remote returns failure")
        }
    }

    func testSections_WhenRemoteServiceResturnsSuccessButEmpty_ShouldUseLocalService() {
        remoteServiceStub.transactionsResult = .success([])

        let expectedTransactions = makeTransactions()
        localServiceStub.transactionsResult = .success(expectedTransactions)

        var completionCount: Int = 0
        var result: ServiceResult?

        sut.sections {
            completionCount += 1
            result = $0
        }

        XCTAssertEqual(completionCount, 1)
        XCTAssertNotNil(result)

        switch result {
        case let .success(transactions):
            XCTAssertEqual(transactions, expectedTransactions)
        default:
            XCTFail("Remote returns failure")
        }
    }

    func testSections_WhenRemoteServiceReturnsFailure_ShouldUseLocalService() {
        remoteServiceStub.transactionsResult = .failure(.unknown(nil))

        let expectedTransactions = makeTransactions()
        localServiceStub.transactionsResult = .success(expectedTransactions)

        var completionCount: Int = 0
        var result: ServiceResult?

        sut.sections {
            completionCount += 1
            result = $0
        }

        XCTAssertEqual(completionCount, 1)
        XCTAssertNotNil(result)

        switch result {
        case let .success(transactions):
            XCTAssertEqual(transactions, expectedTransactions)
        default:
            XCTFail("Did not use local service")
        }
    }

    func testSections_WhenRemoteAndLocalServiceReturnsFailure_ShouldCallCompletionWithFailure() {
        remoteServiceStub.transactionsResult = .failure(.unknown(nil))
        localServiceStub.transactionsResult = .failure(.unknown(nil))

        var completionCount: Int = 0
        var result: ServiceResult?

        sut.sections {
            completionCount += 1
            result = $0
        }

        XCTAssertEqual(completionCount, 1)
        XCTAssertNotNil(result)

        switch result {
        case let .failure(error):
            if case ApiError.unknown = error {
            } else {
                XCTFail("Wrong error returned")
            }
        default:
            XCTFail("Did not returns failure")
        }
    }
}

// MARK: - Private
private extension TransactionsLocalAndRemoteServiceTests {
    func makeTransactions() -> [TransactionSection] {
        [
            .main(groups: makeTransactionGroups()),
            .services(groups: makeTransactionGroups())
        ]
    }

    func makeTransactionGroups() -> [TransactionGroup] {
        [
            .carousel(items: makeTransactionItems(count: 5)),
            .column(items: makeTransactionItems())
        ]
    }

    func makeTransactionItems(count: Int = 10) -> [TransactionItem] {
        (0 ... count).map { _ in makeTransactionItem() }
    }

    func makeTransactionItem() -> TransactionItem {
        TransactionItem(
            type: .local,
            title: "Test \(Int.random(in: 0...10))",
            description: "Test \(Int.random(in: 0...10))",
            image: nil,
            imageDark: nil,
            icon: nil,
            deeplink: nil,
            trackingData: .init(type: "Test", section: "Test", order: 0)
        )
    }
}
