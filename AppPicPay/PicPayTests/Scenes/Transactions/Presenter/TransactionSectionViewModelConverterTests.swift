import FeatureFlag
import XCTest

@testable import PicPay

final class TransactionSectionViewModelConverterTests: XCTestCase {
    // MARK: - Properties
    private lazy var featureManagerMock = FeatureManagerMock()
    private lazy var sut = TransactionSectionViewModelConverter(dependencies: DependencyContainerMock(featureManagerMock))

    // MARK: - Tests
    func testConvertToViewModels_ShouldIgnoreSectionsWithUnknowTypes() {
        let sections: [TransactionSection] = [.unknown, .unknown]
        let viewModels = sut.convertToViewModels(sections: sections)

        XCTAssertTrue(viewModels.isEmpty)
    }

    func testConvertToViewModel_WhenConvertingMainSection_ShouldReturnViewModelWithoutTile() {
        let section = TransactionSection.main(groups: [])
        let viewModel = sut.convertToViewModel(section: section)

        XCTAssertNil(viewModel.title)
    }

    func testConvertToViewModel_WhenConvertingGroupOfUnknownType_ShouldBeIgnored() {
        let group: TransactionGroup = .unknown
        let viewModel = sut.convertToViewModel(section: .main(groups: [group]))

        XCTAssertTrue(viewModel.items.isEmpty)
    }

    func testConvertToViewModel_WhenConvertingItemOfUnknownType_ShouldBeIgnored() {
        let item = makeTransactionItem(type: .unknown)
        let viewModel = sut.convertToViewModel(section: .main(groups: [.carousel(items: [item])]))

        XCTAssertTrue(viewModel.items.isEmpty)
    }

    func testConvertToViewModel_WhenConvertingItemOfQRCodeTypeAndTheFlagIsDisabled_ShouldBeIgnored() {
        featureManagerMock.override(key: .experimentShowScannerOnTransactionsMenu, with: false)

        let item = makeTransactionItem(type: .qrcode)
        let viewModel = sut.convertToViewModel(section: .main(groups: [.carousel(items: [item])]))

        XCTAssertTrue(viewModel.items.isEmpty)
    }

    func testConvertToViewModel_WhenConvertingItemOfQRCodeTypeAndTheFlagIsEnabled_ShouldNotBeIgnored() {
        featureManagerMock.override(key: .experimentShowScannerOnTransactionsMenu, with: true)

        let item = makeTransactionItem(type: .qrcode)
        let viewModel = sut.convertToViewModel(section: .main(groups: [.carousel(items: [item])]))

        XCTAssertFalse(viewModel.items.isEmpty)
    }
}

// MARK: - Private
private extension TransactionSectionViewModelConverterTests {
    func makeTransactions() -> [TransactionSection] {
        [
            .main(groups: makeTransactionGroups()),
            .services(groups: makeTransactionGroups()),
        ]
    }

    func makeTransactionGroups() -> [TransactionGroup] {
        [
            .carousel(items: makeTransactionItems(count: 5)),
            .column(items: makeTransactionItems())
        ]
    }

    func makeTransactionItems(count: Int = 10) -> [TransactionItem] {
        (0 ... count).map { _ in makeTransactionItem() }
    }

    func makeTransactionItem(type: TransactionItem.`Type` = .local) -> TransactionItem {
        TransactionItem(
            type: type,
            title: "Test",
            description: "Test",
            image: nil,
            imageDark: nil,
            icon: nil,
            deeplink: nil,
            trackingData: .init(type: "Test", section: "Test", order: 0)
        )
    }
}
