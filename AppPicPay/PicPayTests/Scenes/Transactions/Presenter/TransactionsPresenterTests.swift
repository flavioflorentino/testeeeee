import Core
import FeatureFlag
import XCTest

@testable import PicPay

final class TransactionsPresenterTests: XCTestCase {
    // MARK: - Properties
    private lazy var displayMock = TransactionsDisplayMock()
    private lazy var converter = TransactionSectionViewModelConverter(dependencies: DependencyContainerMock(FeatureManagerMock()))

    private lazy var sut: TransactionsPresenter = {
        let sut = TransactionsPresenter(converter: converter)
        sut.display = displayMock

        return sut
    }()

    // MARK: - Tests
    func testPresentTransactions_WhenCalled_ShouldCallShowSectionsOnDisplay() {
        let transactions = makeTransactions()
        sut.present(transactions: transactions)

        let expectedSections = converter.convertToViewModels(sections: transactions)
        displayMock.verifyShow(sections: expectedSections)
    }

    func testOpenItem_WhenCalled_ShouldInvokeOpenItemAction() {
        let expectedItem = makeTransactionItem()

        var item: TransactionItem?
        var openItemActionCount: Int = 0

        sut.openItemAction = {
            openItemActionCount += 1
            item = $0
        }

        sut.open(item: expectedItem)

        XCTAssertEqual(openItemActionCount, 1)
        XCTAssertEqual(item, expectedItem)
    }

    func testOpenSearch_WhenCalled_ShouldExecuteOpenSearchAction() {
        var openSearchActionCount: Int = 0

        sut.openSearchAction = {
            openSearchActionCount += 1
        }

        sut.openSearch()

        XCTAssertEqual(openSearchActionCount, 1)
    }

    func testPresentLoading_WhenCalled_ShouldCallShowLoadingOnDisplay() {
        sut.presentLoading()
        displayMock.verifyShowLoading()
    }

    func testHideLoading_WhenCalled_ShouldCallHideLoadingOnDisplay() {
        sut.hideLoading()
        displayMock.verifyHideLoading()
    }

    func testPresentError_WhenReceiveConnectionFailureError_ShouldCallShowNoConnectionOnDisplay() {
        sut.present(error: .connectionFailure)
        displayMock.verifyShowNoConnectionError()
    }

    func testPresentError_WhenReceiveError_ShouldCallShowNoConnectionOnDisplay() {
        sut.present(error: .unknown(nil))
        displayMock.verifyShowUnknownError()
    }

    func testHideError_WhenCalled_ShouldCallHideErrorOnDisplay() {
        sut.hideError()
        displayMock.verifyHideError()
    }
}

// MARK: - Private
private extension TransactionsPresenterTests {
    func makeTransactions() -> [TransactionSection] {
        [
            .main(groups: makeTransactionGroups()),
            .services(groups: makeTransactionGroups()),
        ]
    }

    func makeTransactionGroups() -> [TransactionGroup] {
        [
            .carousel(items: makeTransactionItems(count: 5)),
            .column(items: makeTransactionItems())
        ]
    }

    func makeTransactionItems(count: Int = 10) -> [TransactionItem] {
        (0 ... count).map { _ in makeTransactionItem() }
    }

    func makeTransactionItem(type: TransactionItem.`Type` = .local) -> TransactionItem {
        TransactionItem(
            type: .local,
            title: "Test",
            description: "Test",
            image: nil,
            imageDark: nil,
            icon: nil,
            deeplink: nil,
            trackingData: .init(type: "Test", section: "Test", order: 0)
        )
    }
}
