import XCTest
@testable import PicPay

final class PlaceIndicationPresenterTests: XCTestCase {
    private let viewControllerSpy = PlaceIndicationViewControllerSpy()
    private let coordinatorSpy = PlaceIndicationCoordinatorSpy()
    
    private lazy var sut: PlaceIndicationPresenter = {
        let presenter = PlaceIndicationPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDidNextStep_WhenIndicationSuccess_CallSuccessAlert() {
        let action: PlaceIndicationAction = .indicationSuccess
        sut.didNextStep(action: action)
        XCTAssertEqual(viewControllerSpy.callDisplaySuccessAlertCount, 1)
        XCTAssertNotNil(viewControllerSpy.displaySuccessAlertAction)
    }
    
    func testPresentError_WhenHasError_CallDisplayFormError() {
        let error = PlaceIndicationError.placeNameRequired
        sut.present(error: error)
        XCTAssertEqual(viewControllerSpy.callDisplayFormError, error)
    }
    
    func testStartIndicatingLocation_WhenStart_CallClearFormErrosAndShowLoading() {
        sut.startIndicatingLocation()
        XCTAssertEqual(viewControllerSpy.callClearFormErrosCount, 1)
        XCTAssertEqual(viewControllerSpy.callShowLoadingCount, 1)
    }
    
    func testHideLoading_WhenCalled_CallHideLoading() {
        sut.hideLoading()
        XCTAssertEqual(viewControllerSpy.callHideLoadingCount, 1)
    }
    
    func testPresentErrorView_WhenCalled_CallDisplayErrorView() {
        sut.presentErrorView()
        XCTAssertEqual(viewControllerSpy.callDisplayErrorViewCount, 1)
    }
}

private final class PlaceIndicationViewControllerSpy: PlaceIndicationDisplay {
    // MARK: - Variables
    private(set) var callDisplayFormError: PlaceIndicationError?
    private(set) var callClearFormErrosCount = 0
    private(set) var callShowLoadingCount = 0
    private(set) var callHideLoadingCount = 0
    private(set) var callDisplayErrorViewCount = 0
    private(set) var callDisplaySuccessAlertCount = 0
    private(set) var displaySuccessAlertAction: (() -> Void)?
    
    // MARK: - Tests
    func displayFormError(error: PlaceIndicationError) {
        callDisplayFormError = error
    }
    
    func clearFormErros() {
        callClearFormErrosCount += 1
    }
    
    func showLoading() {
        callShowLoadingCount += 1
    }
    
    func hideLoading() {
        callHideLoadingCount += 1
    }
    
    func displayErrorView() {
        callDisplayErrorViewCount += 1
    }
    
    func displaySuccessAlert(dismissAction: @escaping () -> Void) {
        callDisplaySuccessAlertCount += 1
        displaySuccessAlertAction = dismissAction
    }
}

private final class PlaceIndicationCoordinatorSpy: PlaceIndicationCoordinating {
    // MARK: - Variables
    var viewController: UIViewController?
    private(set) var callPerformActionCount = 0
    private(set) var actionCalled: PlaceIndicationAction?
    
    // MARK: - Tests
    func perform(action: PlaceIndicationAction) {
        callPerformActionCount += 1
        actionCalled = action
    }
}
