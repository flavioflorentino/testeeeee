import Core
import XCTest
import UI

@testable import PicPay

private final class PlaceIndicationViewModelTests: XCTestCase {
    private lazy var serviceMock = PlaceIndicationServiceMock()
    private lazy var presenterSpy = PlaceIndicationPresenterSpy()
    
    private lazy var sut: PlaceIndicationViewModel = {
        return PlaceIndicationViewModel(
            service: serviceMock,
            presenter: presenterSpy
        )
    }()
    
    func testIndicatePlace_WhenValidData_CallIndicationSuccess() {
        sut.indicatePlace(model: PlaceInfoModel(
            name: "Madero",
            city: "São Paulo",
            state: "@madero",
            phone: "SP",
            socialMedia: "1112345678")
        )
        
        XCTAssertEqual(presenterSpy.startIndicatingLocationCalledCount, 1)
        XCTAssertEqual(serviceMock.indicatePlaceCalledCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepCalledCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepAction, .indicationSuccess)
    }
    
    func testIndicatePlace_WhenInvalidData_CallPresentError() {
        sut.indicatePlace(model: PlaceInfoModel(
            name: "",
            city: "São Paulo",
            state: "SP",
            phone: "1112345678",
            socialMedia: "@madero")
        )
        
        XCTAssertEqual(presenterSpy.startIndicatingLocationCalledCount, 1)
        XCTAssertEqual(presenterSpy.presentCalledCount, 1)
        XCTAssertEqual(serviceMock.indicatePlaceCalledCount, .zero)
    }
    
    func testOpenInfo_WhenCalled_CallDidNextStep() {
        sut.openInfo()
        
        XCTAssertEqual(presenterSpy.didNextStepCalledCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepAction, .openInfo)
    }
}

private final class PlaceIndicationServiceMock: PlaceIndicationServicing {
    private(set) var indicatePlaceCalledCount = 0
    
    func indicatePlace(placeInfo: PlaceInfoModel, completion: @escaping (Result<Void, Error>) -> Void) {
        indicatePlaceCalledCount += 1
        completion(.success)
    }
}

private final class PlaceIndicationPresenterSpy: PlaceIndicationPresenting {
    var viewController: PlaceIndicationDisplay?
    
    private(set) var presentCalledCount = 0
    private(set) var startIndicatingLocationCalledCount = 0
    private(set) var hideLoadingCalledCount = 0
    private(set) var presentErrorViewCalledCount = 0
    private(set) var didNextStepCalledCount = 0
    private(set) var presentError: PlaceIndicationError?
    private(set) var didNextStepAction: PlaceIndicationAction?
    
    
    func present(error: PlaceIndicationError) {
        presentCalledCount += 1
        presentError = error
    }
    
    func startIndicatingLocation() {
        startIndicatingLocationCalledCount += 1
    }
    
    func hideLoading() {
        hideLoadingCalledCount += 1
    }
    
    func presentErrorView() {
        presentErrorViewCalledCount += 1
    }
    
    func didNextStep(action: PlaceIndicationAction) {
        didNextStepCalledCount += 1
        didNextStepAction = action
    }
}
