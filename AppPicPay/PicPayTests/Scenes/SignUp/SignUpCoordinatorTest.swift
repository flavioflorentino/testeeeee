import AnalyticsModule
import FeatureFlag
import XCTest

@testable import PicPay

final class SignUpCoordinatorTest: XCTestCase {
    private var featureManagerMock = FeatureManagerMock()
    private var dynamicLinkHelperMock = DynamicLinkHelperMock()
    private var analyticsSpy = AnalyticsSpy()
    private lazy var mockDependencies = DependencyContainerMock(featureManagerMock, dynamicLinkHelperMock, analyticsSpy)
    private let navController = CustomPopNavigationControllerMock(rootViewController: UIViewController())
    private let registrationCacheHelperMock = RegistrationCacheHelperMock()
    private lazy var coordinator: SignUpCoordinator = {
        let coordinator = SignUpCoordinator(dependencies: mockDependencies, registrationCacheHelper: registrationCacheHelperMock)
        coordinator.viewController = navController.topViewController
        return coordinator
    }()
    
    func testPerformShouldPresentLoginScreen() throws {
        coordinator.perform(action: .login)
        
        XCTAssert(navController.isPushViewControllerCalled)
        let pushedViewController = try XCTUnwrap(navController.pushedViewController)
        XCTAssert(pushedViewController is LoginViewController)
    }
    
    func testPerformShouldPresentOldRegisterScreenNotUseDynamicRegistrationSteps() throws {
        featureManagerMock.override(with: [
            .dynamicRegistrationSteps: false,
            .helpCenter: true
        ])

        coordinator.viewController = navController.topViewController
        
        coordinator.perform(action: .register)
        
        XCTAssert(navController.isPushViewControllerCalled)
        let pushedViewController = try XCTUnwrap(navController.pushedViewController)
        XCTAssert(pushedViewController is RegistrationStepViewController)
        
        let expectedEvent = RegistrationEvent.startScreenViewed(
            dynamicRegistrations: false,
            registrationSteps: [.name, .phone, .email, .password, .document]
        ).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testPerform_WhenEnabledTheNewRegistrationFlow_ShouldPresentSomeOfTheNewRegistrationScreens() throws {
        let registrationSteps: [RegistrationStep] = [.password, .email, .name, .document, .phone]
        featureManagerMock.override(with: [
            .dynamicRegistrationSteps: true,
            .dynamicRegistrationStepsSequence: registrationSteps,
            .helpCenter: true
        ])
        
        coordinator.viewController = navController.topViewController
        
        coordinator.perform(action: .register)
        
        XCTAssert(navController.isPushViewControllerCalled)
        let pushedViewController = try XCTUnwrap(navController.pushedViewController)
        
        XCTAssert(
            pushedViewController is RegistrationNameViewController ||
            pushedViewController is RegistrationEmailViewController ||
            pushedViewController is RegistrationPasswordViewController ||
            pushedViewController is RegistrationPhoneNumberViewController ||
            pushedViewController is RegistrationDocumentViewController
        )
        let expectedEvent = RegistrationEvent.startScreenViewed(
            dynamicRegistrations: true,
            registrationSteps: registrationSteps
        ).event()
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }

    func testPerformShouldPushPromoCodeScreenWhenPromoCodeActionWithAMgmPromoCodeIsPassed() throws {
        let mgmPromoCode = ValidPromoCode(
            type: .mgm,
            data: PromoCodeValidationData.mock(type: .mgm, validUrl: true)
        )
            
        coordinator.perform(action: .promoCodeActivated(validPromoCode: mgmPromoCode))
        
        XCTAssert(navController.isPushViewControllerCalled)
        let pushedViewController = try XCTUnwrap(navController.pushedViewController)
        XCTAssert(pushedViewController is RegisterPromoCodeViewController)
    }
    
    func testPerformShouldPushPromoCodeScreenWhenPromoCodeActionWithAGeneralPromoCodeIsPassed() throws {
        let promoCode = ValidPromoCode(
            type: .promo,
            data: PromoCodeValidationData.mock(type: .promo, validUrl: true)
        )
            
        coordinator.perform(action: .promoCodeActivated(validPromoCode: promoCode))
        
        XCTAssert(navController.isPushViewControllerCalled)
        let pushedViewController = try XCTUnwrap(navController.pushedViewController)
        XCTAssert(pushedViewController is RegisterPromoCodeViewController)
    }
    
    func testPerformShouldPushStudentPromoCodeScreenWhenPromoCodeActionWithAStudentPromoCodeIsPassed() throws {
        let studentPromoCode = ValidPromoCode(
            type: .studentAccount,
            data: PromoCodeValidationData.mock(type: .studentAccount, validUrl: true)
        )
            
        coordinator.perform(action: .promoCodeActivated(validPromoCode: studentPromoCode))
        
        XCTAssert(navController.isPushViewControllerCalled)
        let pushedViewController = try XCTUnwrap(navController.pushedViewController)
        XCTAssert(pushedViewController is RegisterStudentPromoCodeViewController)
    }
}
