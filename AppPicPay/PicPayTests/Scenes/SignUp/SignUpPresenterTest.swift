@testable import PicPay
import XCTest

private final class SpySignUpViewController: SignUpDisplay {
    private(set) var updatedHostText: String?
    private(set) var updatedMainText: String?
    private(set) var updatedBackgroundImage: UIImage?
    private(set) var isHelpButtonBadgeVisible = false
    private(set) var alertPresented: Alert?
    private(set) var isCodePopupPresented = false
    private(set) var isPromoCodeButtonEnabled: Bool?
    
    func updateHostTextField(text: String?) {
        updatedHostText = text
    }
    
    func updateMainText(text: String) {
        updatedMainText = text
    }
    
    func updateBackgroundImage(image: UIImage) {
        updatedBackgroundImage = image
    }
    
    func updateHelpButton(isBadgeVisible: Bool) {
        isHelpButtonBadgeVisible = isBadgeVisible
    }
    
    func alertUserToEnableNotifications(alert: Alert) {
        alertPresented = alert
    }
    
    func presentPromoCodePopup() {
        isCodePopupPresented = true
    }
    
    func updatePromoCodeButton(isEnabled: Bool) {
        isPromoCodeButtonEnabled = isEnabled
    }
}


private final class SpySignUpCoordinator: SignUpCoordinating {
    private(set) var didPresentLogin = false
    private(set) var didPresentHelpCenter = false
    private(set) var didPresentRegisterStep = false
    private(set) var didPresentPromoCodeActivated = false
    private(set) var validPromoCode: ValidPromoCode?
    
    var viewController: UIViewController?
    
    func perform(action: SignUpAction) {
        switch action {
        case .login :
            didPresentLogin = true
        case .helpCenter:
            didPresentHelpCenter = true
        case .register:
            didPresentRegisterStep = true
        case .promoCodeActivated(let validPromoCode):
            didPresentPromoCodeActivated = true
            self.validPromoCode = validPromoCode
        }
    }
}

final class SignUpPresenterTest: XCTestCase {
    private let viewController = SpySignUpViewController()
    private let coordinator = SpySignUpCoordinator()
    private lazy var presenter: SignUpPresenter = {
        let presenter = SignUpPresenter(coordinator: coordinator)
        presenter.viewController = viewController
        return presenter
    }()
    
    func testUpdateHostTextField() {
        presenter.updateHostTextField(text: "https://gateway.service.ppay.me/")
        
        XCTAssertEqual(viewController.updatedHostText, "https://gateway.service.ppay.me/")
    }
    
    func testUpdateMainText() {
        presenter.updateMainText(text: "Com o PicPay seus\\npagamentos são mais\\nsimples e seguros.")
        
        XCTAssertEqual(viewController.updatedMainText, "Com o PicPay seus\npagamentos são mais\nsimples e seguros.")
    }
    
    func testUpdateBackgroundImage() {
        let image = UIImage()
        
        presenter.updateBackgroundImage(image)
        
        XCTAssertEqual(viewController.updatedBackgroundImage, image)
    }
    
    func testDidNextStepShouldDisplayLoginScreen() {
        presenter.didNextStep(action: .login)
        
        XCTAssert(coordinator.didPresentLogin)
    }
    
    func testDidNextStepShouldDisplayRegisterScreen() {
        presenter.didNextStep(action: .register)
        
        XCTAssert(coordinator.didPresentRegisterStep)
    }
    
    func testDidNextStepShouldDisplayHelpScreen() {
        presenter.didNextStep(action: .helpCenter)
        
        XCTAssert(coordinator.didPresentHelpCenter)
    }
    
    func testDidNextStepShouldDisplayPromoCodeActvatedScreen() throws {
        let validPromoCode = ValidPromoCode.mock()
        presenter.didNextStep(action: .promoCodeActivated(validPromoCode: validPromoCode))
        
        XCTAssert(coordinator.didPresentPromoCodeActivated)
        XCTAssertEqual(coordinator.validPromoCode?.type, validPromoCode.type)
    }
    
    func testUpdateHelpButtonShouldDisplayBadge() {
        presenter.updateHelpButton(isBadgeVisible: true)
        
        XCTAssert(viewController.isHelpButtonBadgeVisible)
    }
    
    func testUpdateHelpButtonShouldNotDisplayBadge() {
        presenter.updateHelpButton(isBadgeVisible: false)
        
        XCTAssertFalse(viewController.isHelpButtonBadgeVisible)
    }
    
    func testAlertUserToEnableNotificationsShouldDisplayAlert() {
        let alert = Alert(title: "title", text: nil)
        
        presenter.alertUserToEnableNotifications(alert: alert)
        
        XCTAssertNotNil(viewController.alertPresented)
    }
    
    func testDidPresentPromoCodePopup() {
        presenter.presentPromoCodePopup()
        
        XCTAssert(viewController.isCodePopupPresented)
    }
    
    func testUpdatePromoCodeButtonShouldEnablePromoCodeButtonWhenParameterIsTrue() {
        presenter.updatePromoCodeButton(isEnabled: true)
        
        XCTAssert(viewController.isPromoCodeButtonEnabled ?? false)
    }
    
    func testUpdatePromoCodeButtonShouldDisablePromoCodeButtonWhenParameterIsFalse() {
        presenter.updatePromoCodeButton(isEnabled: false)
        
        XCTAssertFalse(viewController.isPromoCodeButtonEnabled ?? true)
    }
}
