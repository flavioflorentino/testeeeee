import AnalyticsModule
import FeatureFlag
import XCTest

@testable import PicPay

private final class SpySignUpPresenter: SignUpPresenting {
    private(set) var updatedHostText: String?
    private(set) var updatedMainText: String?
    private(set) var updatedBackgroundImage: UIImage?
    
    private(set) var didPresentLogin = false
    private(set) var didPresentHelpCenter = false
    private(set) var didPresentRegisterStep = false
    private(set) var didPresentPromoCodeActivated = false
    
    private(set) var isHelpButtonBadgeVisible: Bool?
    private(set) var alertPresented: Alert?
    private(set) var validPromoCode: ValidPromoCode?
    private(set) var isCodePopupPresented = false
    private(set) var isPromoCodeButtonEnabled: Bool?
    
    weak var viewController: SignUpDisplay?
    
    func updateHostTextField(text: String?) {
        updatedHostText = text
    }
    
    func updateMainText(text: String) {
        updatedMainText = text
    }
    
    func updateBackgroundImage(_ image: UIImage) {
        updatedBackgroundImage = image
    }
    
    func didNextStep(action: SignUpAction) {
        switch action {
        case .login :
            didPresentLogin = true
        case .helpCenter:
            didPresentHelpCenter = true
        case .register:
            didPresentRegisterStep = true
        case .promoCodeActivated(let validPromoCode):
            didPresentPromoCodeActivated = true
            self.validPromoCode = validPromoCode
        }
    }
    
    func updateHelpButton(isBadgeVisible: Bool) {
        isHelpButtonBadgeVisible = isBadgeVisible
    }
    
    func alertUserToEnableNotifications(alert: Alert) {
        alertPresented = alert
    }
    
    func presentPromoCodePopup() {
        isCodePopupPresented = true
    }
    
    func updatePromoCodeButton(isEnabled: Bool) {
        isPromoCodeButtonEnabled = isEnabled
    }
}

private final class SpySignUpService: SignUpServicing {
    var successLoadingConsumerData = true
    var notificationStatus: UNAuthorizationStatus = .authorized
    var didSetNotFirstTimeOnApp = false
    
    var isUserAuthenticated: Bool = false
    
    var devApiHost: String? = ""
    
    var mainText: String = ""
    
    var enablePromoCodeButton: Bool = false
    
    var askedForRegistrationRemoteNotification: Bool = false
    
    var isFirstTimeOnApp: Bool = false
    
    func setNotFirstTimeOnApp() {
        didSetNotFirstTimeOnApp = true
    }
    
    func fetchConsumerData(_ completion: @escaping (Bool) -> Void) {
        completion(successLoadingConsumerData)
    }
    
    func getNotificationAuthorizationStatus(_ completion: @escaping (UNAuthorizationStatus) -> Void) {
        completion(notificationStatus)
    }
}

final class SignUpViewModelTest: XCTestCase {
    private let featureManagerMock = FeatureManagerMock()
    private let analyticsMock = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(featureManagerMock, analyticsMock)
    private let service = SpySignUpService()
    private let presenter = SpySignUpPresenter()
    private var viewModel: SignUpViewModelInputs?
    private let referralWorker = ReferralRecommendationWorkerMock()
    
    override func setUp() {
        super.setUp()
        viewModel = SignUpViewModel(service: service, presenter: presenter, referralWorker: referralWorker, dependencies: dependencies)
    }

    func testViewDidLoadShouldUpdateTexts() {
        service.devApiHost = "api-host"
        service.mainText = "main-text"

        viewModel?.viewDidLoad()
        
        XCTAssertEqual(presenter.updatedHostText, "api-host")
        XCTAssertEqual(presenter.updatedMainText, "main-text")
        XCTAssertNotNil(presenter.updatedBackgroundImage)
    }
    
    func testViewDidLoadShouldEnablePromoCodeButtonWhenFeatureIsActive() {
        service.enablePromoCodeButton = true
        
        viewModel?.viewDidLoad()
        
        XCTAssert(presenter.isPromoCodeButtonEnabled ?? false)
    }
    
    func testViewDidLoadShouldDisablePromoCodeButtonWhenFeatureIsNotActive() {
        service.enablePromoCodeButton = false
        
        viewModel?.viewDidLoad()
        
        XCTAssertFalse(presenter.isPromoCodeButtonEnabled ?? true)
    }
    
    func testDidTapRegisterButtonShoulPresentRegisterScreen() {
        viewModel?.didTapRegisterButton()
        
        XCTAssert(presenter.didPresentRegisterStep)
    }
    
    func testDidTapLoginButtonShouldPresentLoginScreen() {
        viewModel?.didTapLoginButton()
        
        XCTAssert(presenter.didPresentLogin)
    }
    
    func testDidTapHelpButtonShouldPresentHelpScreen() {
        viewModel?.didTapHelpButton()
        
        XCTAssert(presenter.didPresentHelpCenter)
    }
    
    func testNewConversationStartedShouldPresentAlertWhenNoficationsAreNotEnabledAndWereNeverAsked() throws {
        service.notificationStatus = .denied
        service.askedForRegistrationRemoteNotification = false
        
        viewModel?.newConversationStarted()
        
        XCTAssertNotNil(presenter.alertPresented)
        let alert = try presenter.alertPresented.safe()
        XCTAssertEqual(alert.title?.string, SignUpLocalizable.enableNotificationAlertTitle.text)
        XCTAssertEqual(alert.text?.string, SignUpLocalizable.enableNotificationAlertMessage.text)
        XCTAssertEqual(alert.buttons.count, 2)
        XCTAssertEqual(alert.buttons.first?.title, SignUpLocalizable.authorize.text)
        XCTAssertEqual(alert.buttons.last?.title, DefaultLocalizable.notNow.text)
    }
    
    func testNewConversationStartedShouldPresentAlertWhenNoficationsAreNotEnabledAndWereAskedOnce() throws {
        service.notificationStatus = .denied
        service.askedForRegistrationRemoteNotification = true
        
        viewModel?.newConversationStarted()
        
        XCTAssertNotNil(presenter.alertPresented)
        let alert = try presenter.alertPresented.safe()
        XCTAssertEqual(alert.title?.string, SignUpLocalizable.enableNotificationAlertTitle.text)
        XCTAssertEqual(alert.text?.string, SignUpLocalizable.enableNotificationAlertMessage.text)
        XCTAssertEqual(alert.buttons.count, 2)
        XCTAssertEqual(alert.buttons.first?.title, SignUpLocalizable.configurations.text)
        XCTAssertEqual(alert.buttons.last?.title, DefaultLocalizable.btCancel.text)
    }
    
    func testNewConversationStartedShouldNotPresentAlertWhenNoficationsAreEnabled() {
        service.notificationStatus = .authorized
        service.askedForRegistrationRemoteNotification = true
        
        viewModel?.newConversationStarted()
        
        XCTAssertNil(presenter.alertPresented)
    }
    
    func testDidActivatePromoCode() throws {
        let validPromoCode = ValidPromoCode.mock()
        viewModel?.didActivatePromoCode(validPromoCode: validPromoCode)
        
        XCTAssert(presenter.didPresentPromoCodeActivated)
        XCTAssertEqual(presenter.validPromoCode?.type, validPromoCode.type)
    }
    
    func testDidTapUsePromoCodeButtonShouldPresentAPromoCodePopup() {
        viewModel?.didTapUsePromoCodeButton()
        
        XCTAssert(presenter.isCodePopupPresented)
    }
}
