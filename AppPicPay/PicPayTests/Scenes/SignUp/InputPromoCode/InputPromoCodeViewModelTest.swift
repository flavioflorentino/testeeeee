@testable import PicPay
import Core
import XCTest

private final class SpyInputPromoCodePresenter: InputPromoCodePresenting {
    private(set) var didShowLoadingAnimation = false
    private(set) var didHideLoadingAnimation = false
    private(set) var didDeactivateConfirmButton = false
    private(set) var didActivateConfirmButton = false
    private(set) var didDisplayErrorMessage = false
    private(set) var didHideErrorMessage = false
    private(set) var didNextStepSuccess = false
    private(set) var didNextStepClose = false
    private(set) var errorMessage: String?
    private(set) var validPromoCode: ValidPromoCode?
    
    var viewController: InputPromoCodeDisplay?
    
    func showLoadingAnimation() {
        didShowLoadingAnimation = true
    }
    
    func hideLoadingAnimation() {
        didHideLoadingAnimation = true
    }
    
    func deactivateConfirmButton() {
        didDeactivateConfirmButton = true
    }
    
    func activateConfirmButton() {
        didActivateConfirmButton = true
    }
    
    func displayErrorMessage(_ message: String) {
        didDisplayErrorMessage = true
        errorMessage = message
    }
    
    func hideErrorMessage() {
        didHideErrorMessage = true
    }
    
    func didNextStep(action: InputPromoCodeAction) {
        switch action {
        case .success(let validPromoCode):
            didNextStepSuccess = true
            self.validPromoCode = validPromoCode
        case .close:
            didNextStepClose = true
        }
    }
}

private final class SpyInputPromoCodeService: InputPromoCodeServicing {
    private(set) var promoCode: String?
    private(set) var didVerifyPromoCode = false
    var verifyPromoCodeResult: Result<PromoCodeValidationResponse, ApiError>?
    
    func verifyPromoCode(_ promoCode: String, completion: @escaping (Result<PromoCodeValidationResponse, ApiError>) -> Void) {
        didVerifyPromoCode = true
        self.promoCode = promoCode
        
        guard let result = verifyPromoCodeResult else {
            return
        }
        completion(result)
    }
}

final class InputPromoCodeViewModelTest: XCTestCase {
    private let service = SpyInputPromoCodeService()
    private let presenter = SpyInputPromoCodePresenter()
    private lazy var viewModel = InputPromoCodeViewModel(service: service, presenter: presenter)
    
    func testDidTapCloseButtonShouldClosePopup() {
        viewModel.didTapCloseButton()
        
        XCTAssert(presenter.didNextStepClose)
    }
    
    func testDidTapConfirmButtonShouldDoNothingPromoCodeIsNil() {
        viewModel.didTapConfirmButton(promoCode: nil)
        
        XCTAssertFalse(service.didVerifyPromoCode)
        XCTAssertFalse(presenter.didNextStepSuccess)
        XCTAssertFalse(presenter.didNextStepClose)
    }
    
    func testDidTapConfirmButtonShouldShowSuccessWhenVerificationReturnsTypeMGM() {
        let promoCodeValidationResponse = PromoCodeValidationResponse.mock(type: .mgm)
        service.verifyPromoCodeResult = Result.success(promoCodeValidationResponse)
        
        viewModel.didTapConfirmButton(promoCode: "GTEAS32")
        
        XCTAssert(service.didVerifyPromoCode)
        XCTAssertEqual(service.promoCode, "GTEAS32")
        XCTAssert(presenter.didNextStepSuccess)
        XCTAssertEqual(presenter.validPromoCode?.type, .mgm)
    }
    
    func testDidTapConfirmButtonShouldShowSuccessWhenVerificationReturnsTypeUNI() {
        let promoCodeValidationResponse = PromoCodeValidationResponse.mock(type: .studentAccount)
        service.verifyPromoCodeResult = Result.success(promoCodeValidationResponse)
        
        viewModel.didTapConfirmButton(promoCode: "94EIRFSV")
        
        XCTAssert(service.didVerifyPromoCode)
        XCTAssertEqual(service.promoCode, "94EIRFSV")
        XCTAssert(presenter.didNextStepSuccess)
        XCTAssertEqual(presenter.validPromoCode?.type, .studentAccount)
    }
    
    func testDidTapConfirmButtonShouldShowSuccessWhenVerificationReturnsTypePromo() {
        let promoCodeValidationResponse = PromoCodeValidationResponse.mock(type: .promo)
        service.verifyPromoCodeResult = Result.success(promoCodeValidationResponse)
        
        viewModel.didTapConfirmButton(promoCode: "437HSS4")
        
        XCTAssert(service.didVerifyPromoCode)
        XCTAssertEqual(service.promoCode, "437HSS4")
        XCTAssert(presenter.didNextStepSuccess)
        XCTAssertEqual(presenter.validPromoCode?.type, .promo)
    }
    
    func testDidTapConfirmButtonShouldShowErrorWhenVerificationReturnsTypeNotFound() {
        let promoCodeValidationResponse = PromoCodeValidationResponse.mock(type: .notFound)
        service.verifyPromoCodeResult = Result.success(promoCodeValidationResponse)
        
        viewModel.didTapConfirmButton(promoCode: "ORNG9RD")
        
        XCTAssert(service.didVerifyPromoCode)
        XCTAssertEqual(service.promoCode, "ORNG9RD")
        XCTAssertFalse(presenter.didNextStepSuccess)
        XCTAssert(presenter.didDisplayErrorMessage)
        XCTAssertEqual(presenter.errorMessage, SignUpLocalizable.codeIsNotValid.text)
    }
    
    func testDidTapConfirmButtonShouldShowErrorWhenVerificationReturnsTypeInvalid() {
        let promoCodeValidationResponse = PromoCodeValidationResponse.mock(type: .invalid)
        service.verifyPromoCodeResult = Result.success(promoCodeValidationResponse)
        
        viewModel.didTapConfirmButton(promoCode: "EROGH93")
        
        XCTAssert(service.didVerifyPromoCode)
        XCTAssertEqual(service.promoCode, "EROGH93")
        XCTAssertFalse(presenter.didNextStepSuccess)
        XCTAssert(presenter.didDisplayErrorMessage)
        XCTAssertEqual(presenter.errorMessage, SignUpLocalizable.codeIsNotValid.text)
    }
    
    func testDidTapConfirmButtonShouldShowErrorWhenVerificationReturnsNotFoundFailure() {
        var error = RequestError()
        error.message = "Código Promocional não encontrado!"
        
        service.verifyPromoCodeResult = Result.failure(ApiError.notFound(body: error))
        
        viewModel.didTapConfirmButton(promoCode: "GNE55E")
        
        XCTAssert(service.didVerifyPromoCode)
        XCTAssertEqual(service.promoCode, "GNE55E")
        XCTAssertFalse(presenter.didNextStepSuccess)
        XCTAssert(presenter.didDisplayErrorMessage)
        XCTAssertEqual(presenter.errorMessage, "Código Promocional não encontrado!")
    }
    
    func testDidTapConfirmButtonShouldShowErrorWhenVerificationReturnsConnectionFailure() {
        service.verifyPromoCodeResult = Result.failure(ApiError.connectionFailure)
        
        viewModel.didTapConfirmButton(promoCode: "ORNG9RD")
        
        XCTAssert(service.didVerifyPromoCode)
        XCTAssertEqual(service.promoCode, "ORNG9RD")
        XCTAssertFalse(presenter.didNextStepSuccess)
        XCTAssert(presenter.didDisplayErrorMessage)
        XCTAssertEqual(presenter.errorMessage, DefaultLocalizable.errorConnectionViewSubtitle.text)
    }
    
    func testDidTapConfirmButtonShouldShowErrorWhenVerificationReturnsUnknownFailure() {
        service.verifyPromoCodeResult = Result.failure(ApiError.unknown(nil))
        
        viewModel.didTapConfirmButton(promoCode: "GNE55E")
        
        XCTAssert(service.didVerifyPromoCode)
        XCTAssertEqual(service.promoCode, "GNE55E")
        XCTAssertFalse(presenter.didNextStepSuccess)
        XCTAssert(presenter.didDisplayErrorMessage)
        XCTAssertEqual(presenter.errorMessage, DefaultLocalizable.requestError.text)
    }
    
    func testDidTapCancelButtonShouldDismissPromoCodePopup() {
        viewModel.didTapCancelButton()
        
        XCTAssert(presenter.didNextStepClose)
    }
    
    func testDidUpdateCodeTextFieldShouldDeactivateConfirmButtonWhenCodeIsEmpty() {
        viewModel.didUpdateCodeTextField(with: "")
        
        XCTAssert(presenter.didDeactivateConfirmButton)
        XCTAssertFalse(presenter.didActivateConfirmButton)
    }
    
    func testDidUpdateCodeTextFieldShouldDeactivateConfirmButtonWhenCodeIsNil() {
        viewModel.didUpdateCodeTextField(with: nil)
        
        XCTAssert(presenter.didDeactivateConfirmButton)
        XCTAssertFalse(presenter.didActivateConfirmButton)
    }
    
    func testDidUpdateCodeTextFieldShouldActivateConfirmButtonWhenCodeIsNotEmpty() {
        viewModel.didUpdateCodeTextField(with: "HL43HE")
        
        XCTAssertFalse(presenter.didDeactivateConfirmButton)
        XCTAssert(presenter.didActivateConfirmButton)
    }
}
