@testable import PicPay
import XCTest

private final class SpyInputPromoCodeViewController: InputPromoCodeDisplay {
    private(set) var didShowLoadingAnimation = false
    private(set) var didHideLoadingAnimation = false
    private(set) var didDeactivateConfirmButton = false
    private(set) var didActivateConfirmButton = false
    private(set) var didDisplayErrorMessage = false
    private(set) var didHideErrorMessage = false
    private(set) var errorMessage: String?

    
    func showLoadingAnimation() {
        didShowLoadingAnimation = true
    }
    
    func hideLoadingAnimation() {
        didHideLoadingAnimation = true
    }
    
    func deactivateConfirmButton() {
        didDeactivateConfirmButton = true
    }
    
    func activateConfirmButton() {
        didActivateConfirmButton = true
    }
    
    func displayErrorMessage(_ message: String) {
        didDisplayErrorMessage = true
        errorMessage = message
    }
    
    func hideErrorMessage() {
        didHideErrorMessage = true
    }
}


private final class SpyInputPromoCodeCoordinator: InputPromoCodeCoordinating {
    var viewController: UIViewController?
    var delegate: InputPromoCodeDelegate?
    
    private(set) var didNextStepSuccess = false
    private(set) var didNextStepClose = false
    private(set) var validPromoCode: ValidPromoCode?
    
    func perform(action: InputPromoCodeAction) {
        switch action {
        case .close:
            didNextStepClose = true
        case .success(let validPromoCode):
            didNextStepSuccess = true
            self.validPromoCode = validPromoCode
        }
    }
}

final class InputPromoCodePresenterTest: XCTestCase {
    private let viewController = SpyInputPromoCodeViewController()
    private let coordinator = SpyInputPromoCodeCoordinator()
    private lazy var presenter: InputPromoCodePresenter = {
        let presenter = InputPromoCodePresenter(coordinator: coordinator)
        presenter.viewController = viewController
        return presenter
    }()
    
    func testDidNextStepShouldClosePopup() {
        presenter.didNextStep(action: .close)
        
        XCTAssert(coordinator.didNextStepClose)
    }
    
    func testDidNextStepShouldPresentSuccess() {
        let validPromoCode = ValidPromoCode.mock()
        
        presenter.didNextStep(action: .success(validPromoCode))
        
        XCTAssert(coordinator.didNextStepSuccess)
        XCTAssertNotNil(coordinator.validPromoCode)
        XCTAssertEqual(coordinator.validPromoCode?.type, validPromoCode.type)
    }
    
    func testShowLoadingAnimation() {
        presenter.showLoadingAnimation()
        
        XCTAssert(viewController.didShowLoadingAnimation)
    }
    
    func testHideLoadingAnimation() {
        presenter.hideLoadingAnimation()
        
        XCTAssert(viewController.didHideLoadingAnimation)
    }
    
    func testDeactivateConfirmButton() {
        presenter.deactivateConfirmButton()
        
        XCTAssert(viewController.didDeactivateConfirmButton)
    }
    
    func testActivateConfirmButton() {
        presenter.activateConfirmButton()
        
        XCTAssert(viewController.didActivateConfirmButton)
    }
    
    func testDisplayErrorMessage() {
        presenter.displayErrorMessage("Error message")
        
        XCTAssert(viewController.didDisplayErrorMessage)
        XCTAssertEqual(viewController.errorMessage, "Error message")
    }
    
    func testHideErrorMessage() {
        presenter.hideErrorMessage()
        
        XCTAssert(viewController.didHideErrorMessage)
    }
}
