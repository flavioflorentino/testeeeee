@testable import PicPay
import XCTest

final class SpyInputPromoCodeDelegate: InputPromoCodeDelegate {
    var validPromoCode: ValidPromoCode?
    
    func didActivatePromoCode(validPromoCode: ValidPromoCode) {
        self.validPromoCode = validPromoCode
    }
}

final class InputPromoCodeCoordinatorTest: XCTestCase {
    private let viewController = ViewControllerMock()
    private let coordinatorDelegate = SpyInputPromoCodeDelegate()
    private lazy var navigationController = NavigationControllerMock(rootViewController: viewController)
    private lazy var coordinator: InputPromoCodeCoordinator = {
        let coordinator = InputPromoCodeCoordinator()
        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        return coordinator
    }()
    
    func testPerformShouldCloseViewController() {
        coordinator.perform(action: .close)
        
        XCTAssertEqual(viewController.didDismissControllerCount, 1)
    }
    
    func testPerformShouldPresentRegisterScreen() throws {
        let validPromoCode = ValidPromoCode.mock()

        coordinator.perform(action: .success(validPromoCode))
        
        XCTAssertEqual(viewController.didDismissControllerCount, 1)
        XCTAssertNotNil(coordinatorDelegate.validPromoCode)
        XCTAssertEqual(validPromoCode.type, coordinatorDelegate.validPromoCode?.type)
    }
}
