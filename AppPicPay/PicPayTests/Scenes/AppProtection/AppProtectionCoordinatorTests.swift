import XCTest
@testable import PicPay

final class AppProtectionCoordinatorTests: XCTestCase {
    private let viewControllerMock = NavigationControllerMock()

    private lazy var sut: AppProtectionCoordinator = {
        let coordinator = AppProtectionCoordinator()
        coordinator.viewController = viewControllerMock
        return coordinator
    }()

    func testPerform_WhenActionIsDismiss_ShouldDismissViewController() {
        sut.perform(action: .dismiss)

        XCTAssertTrue(viewControllerMock.isDismissViewControllerCalled)
    }
}
