import XCTest
@testable import PicPay

private class AppProtectionCoordinatingSpy: AppProtectionCoordinating {
    weak var viewController: UIViewController?
    private(set) var performCallCount = 0

    func perform(action: AppProtectionAction) {
        performCallCount += 1
    }
}

private class AppProtectionDisplayingSpy: AppProtectionDisplaying {
    private(set) var setBiometricTypeCallCount = 0
    private(set) var hideCloseButtonCallCount = 0
    private(set) var setSwitchCallCount = 0
    private(set) var showDescriptionLabelCallCount = 0
    private(set) var showPopupControllerCallCount = 0

    private(set) var biometricTypeString: String?

    func setBiometricType(text: String) {
        setBiometricTypeCallCount += 1
        biometricTypeString = text
    }

    func hideCloseButton() {
        hideCloseButtonCallCount += 1
    }

    func setSwitch(value: Bool) {
        setSwitchCallCount += 1
    }

    func showDescriptionLabel() {
        showDescriptionLabelCallCount += 1
    }

    func showPopupController(title: String, message: String, buttonTitle: String) {
        showPopupControllerCallCount += 1
    }
}

final class AppProtectionPresenterTests: XCTestCase {
    private let controllerSpy = AppProtectionDisplayingSpy()
    private let coordinatorSpy = AppProtectionCoordinatingSpy()

    private lazy var sut: AppProtectionPresenter = {
        let presenter = AppProtectionPresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()

    func testSetBiometricText_WhenTypeIsTouchId_ShouldCallControllerSetText() {
        sut.setBiometricText(for: .touchID)

        XCTAssertEqual(controllerSpy.setBiometricTypeCallCount, 1)
        XCTAssertEqual(controllerSpy.biometricTypeString, AppProtectionLocalizable.biometricTouchType.text)
        XCTAssertEqual(controllerSpy.showDescriptionLabelCallCount, 0)
    }

    func testSetBiometricText_WhenTypeIsFaceId_ShouldCallControllerSetText() {
        sut.setBiometricText(for: .faceID)

        XCTAssertEqual(controllerSpy.setBiometricTypeCallCount, 1)
        XCTAssertEqual(controllerSpy.biometricTypeString, AppProtectionLocalizable.biometricFaceType.text)
        XCTAssertEqual(controllerSpy.showDescriptionLabelCallCount, 0)
    }

    func testSetBiometricText_WhenTypeIsNone_ShouldDoNothing() {
        sut.setBiometricText(for: .none)

        XCTAssertEqual(controllerSpy.setBiometricTypeCallCount, 1)
        XCTAssertEqual(controllerSpy.biometricTypeString, AppProtectionLocalizable.pinProtectionType.text)
        XCTAssertEqual(controllerSpy.showDescriptionLabelCallCount, 1)
    }

    func testHideCloseButton_ShouldCallControllerHideCloseButton() {
        sut.hideCloseButton()

        XCTAssertEqual(controllerSpy.hideCloseButtonCallCount, 1)
    }

    func testSetSwitch_ShouldCallControllerSetSwitch() {
        sut.setSwitch(value: true)

        XCTAssertEqual(controllerSpy.setSwitchCallCount, 1)
    }

    func testShowPasscodeNotSetError_ShouldCallControllerShowPopup() {
        sut.showPasscodeNotSetError()

        XCTAssertEqual(controllerSpy.showPopupControllerCallCount, 1)
    }

    func testDidNextStep_ShouldCallCoordinatorPerformAction() {
        sut.didNextStep(action: .dismiss)

        XCTAssertEqual(coordinatorSpy.performCallCount, 1)
    }
}
