import AnalyticsModule
import XCTest
@testable import PicPay
import SecurityModule

private class AppProtectionServicingMock: AppProtectionServicing {
    private(set) var getBiometricTypeCallCount = 0
    private(set) var getValueForAppProtectionCallCount = 0
    private(set) var changeAppProtectionCallCount = 0
    private(set) var evaluateBiometryCallCount = 0

    private(set) var appProtectionValue = false
    var authenticated = true
    var evaluationError: LAError? = nil

    var biometricType: DeviceBiometricType {
        getBiometricTypeCallCount += 1
        return .none
    }

    var valueForAppProtection: Bool {
        getValueForAppProtectionCallCount += 1
        return true
    }

    func changeAppProtection(_ enable: Bool) {
        changeAppProtectionCallCount += 1
        appProtectionValue = enable
    }

    func evaluateBiometry(reason: String, completion: @escaping (Bool, LAError?) -> Void) {
        evaluateBiometryCallCount += 1
        completion(authenticated, evaluationError)
    }
}

private class AppProtectionPresentingSpy: AppProtectionPresenting {
    weak var viewController: AppProtectionDisplaying?
    private(set) var setBiometricTextCallCount = 0
    private(set) var setSwitchCallCount = 0
    private(set) var showPasscodeNotSetErrorCallCount = 0
    private(set) var hideCloseButtonCallCount = 0
    private(set) var didNextStepCallCount = 0

    private(set) var switchValue = false
    private(set) var action: AppProtectionAction?

    func setBiometricText(for type: DeviceBiometricType) {
        setBiometricTextCallCount += 1
    }

    func setSwitch(value: Bool) {
        setSwitchCallCount += 1
        switchValue = value
    }

    func showPasscodeNotSetError() {
        showPasscodeNotSetErrorCallCount += 1
    }

    func hideCloseButton() {
        hideCloseButtonCallCount += 1
    }

    func didNextStep(action: AppProtectionAction) {
        didNextStepCallCount += 1
        self.action = action
    }
}

final class AppProtectionInteractorTests: XCTestCase {
    private let serviceMock = AppProtectionServicingMock()
    private let presenterSpy = AppProtectionPresentingSpy()
    private let analyticsSpy = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analyticsSpy)

    private lazy var sut = createSut()

    func testSetupView_WhenShouldHideCloseButtonIsFalse_ShouldPassTheValueFromServiceToPresenter() {
        sut.setupView()

        XCTAssertEqual(serviceMock.getBiometricTypeCallCount, 1)
        XCTAssertEqual(presenterSpy.setBiometricTextCallCount, 1)
        XCTAssertEqual(presenterSpy.hideCloseButtonCallCount, 0)
    }

    func testSetupView_WhenShouldHideCloseButtonIsTrue_ShouldCallPresenterHideCloseButton() {
        sut = createSut(shouldHideCloseButton: true)

        sut.setupView()

        XCTAssertEqual(serviceMock.getBiometricTypeCallCount, 1)
        XCTAssertEqual(presenterSpy.setBiometricTextCallCount, 1)
        XCTAssertEqual(presenterSpy.hideCloseButtonCallCount, 1)
    }

    func testGetProtectionSwitchValue_ShouldPassTheValueFromServiceToPresenter() {
        sut.getProtectionSwitchValue()

        XCTAssertEqual(serviceMock.getValueForAppProtectionCallCount, 1)
        XCTAssertEqual(presenterSpy.setSwitchCallCount, 1)
    }

    func testGetProtectionSwitchValue_ShouldSendAppearEvent() {
        sut.getProtectionSwitchValue()

        let expectedEvent = AppProtectionEvent.appear(switchValue: true).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testSwitchDidChange_ShouldSendSwitchTouchedEventAndSwitchChangedEvent() {
        sut.switchDidChange(value: true)

        let touchedEvent = AppProtectionEvent.switchTouched(switchValue: false).event()
        let changedEvent = AppProtectionEvent.switchChanged(switchValue: true).event()
        XCTAssertTrue(analyticsSpy.equals(to: touchedEvent, changedEvent))
    }

    func testSwitchDidChange_WhenValueIsTrue_ShouldCallServiceAndPresenter() {
        sut.switchDidChange(value: true)

        XCTAssertEqual(serviceMock.changeAppProtectionCallCount, 1)
        XCTAssertTrue(serviceMock.appProtectionValue)
        XCTAssertEqual(presenterSpy.setSwitchCallCount, 1)
        XCTAssertTrue(presenterSpy.switchValue)
    }

    func testSwitchDidChange_WhenValueIsFalseAndAuthenticationPJErrorIsNil_ShouldCallServiceAndPresenter() {
        sut.switchDidChange(value: false)

        XCTAssertEqual(serviceMock.evaluateBiometryCallCount, 1)
        XCTAssertEqual(presenterSpy.showPasscodeNotSetErrorCallCount, 0)
        XCTAssertEqual(serviceMock.changeAppProtectionCallCount, 1)
        XCTAssertFalse(serviceMock.appProtectionValue)
        XCTAssertEqual(presenterSpy.setSwitchCallCount, 1)
        XCTAssertFalse(presenterSpy.switchValue)
    }

    func testSwitchDidChange_WhenValueIsFalseAndAuthenticationPJErrorIsNotNil_ShouldCallServiceAndPresenter() {
        serviceMock.authenticated = false
        serviceMock.evaluationError = LAError(.authenticationFailed)

        sut.switchDidChange(value: false)

        XCTAssertEqual(serviceMock.evaluateBiometryCallCount, 1)
        XCTAssertEqual(presenterSpy.showPasscodeNotSetErrorCallCount, 0)
        XCTAssertEqual(serviceMock.changeAppProtectionCallCount, 1)
        XCTAssertTrue(serviceMock.appProtectionValue)
        XCTAssertEqual(presenterSpy.setSwitchCallCount, 1)
        XCTAssertTrue(presenterSpy.switchValue)
    }

    func testSwitchDidChange_WhenValueIsFalseAndAuthenticationPJErrorIsPasscodeNotSet_ShouldCallServiceAndPresenter() {
        serviceMock.authenticated = false
        serviceMock.evaluationError = LAError(.passcodeNotSet)

        sut.switchDidChange(value: false)

        XCTAssertEqual(serviceMock.evaluateBiometryCallCount, 1)
        XCTAssertEqual(presenterSpy.showPasscodeNotSetErrorCallCount, 1)
        XCTAssertEqual(serviceMock.changeAppProtectionCallCount, 1)
        XCTAssertTrue(serviceMock.appProtectionValue)
        XCTAssertEqual(presenterSpy.setSwitchCallCount, 1)
        XCTAssertTrue(presenterSpy.switchValue)
    }

    func testClose_ShouldCallPresenterNextStepWithDismissAction() {
        sut.close()

        XCTAssertEqual(presenterSpy.didNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.action, .dismiss)
    }

    private func createSut(shouldHideCloseButton: Bool = false) -> AppProtectionInteractor {
        AppProtectionInteractor(service: serviceMock,
                                presenter: presenterSpy,
                                shouldHideCloseButton: shouldHideCloseButton,
                                dependencies: dependencies)
    }
}
