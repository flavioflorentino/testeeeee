import UI
import XCTest
@testable import PicPay

private final class ReferralInviteContactsDisplaySpy: ReferralInviteContactsDisplay {
    private(set) var callDisplayRewardCount = 0
    private(set) var callDisplayContactsCount = 0
    private(set) var callDisplayContactsCountCount = 0
    private(set) var callDisplayRequestContactsAccessCount = 0
    private(set) var callDisplayEmptyContactsViewCount = 0
    private(set) var callDisplayErrorViewCount = 0
    private(set) var cellPresenters: [ReferralInviteContactsCellPresenting]?
    private(set) var contactsCount: String?
    private(set) var reward: String?
    
    func displayReward(reward: String?) {
        callDisplayRewardCount += 1
        self.reward = reward
    }
    
    func displayContacts(_ cells: [ReferralInviteContactsCellPresenting]) {
        callDisplayContactsCount += 1
        self.cellPresenters = cells
    }

    func displayContactsCount(contactsCount: String) {
        callDisplayContactsCountCount += 1
        self.contactsCount = contactsCount
    }
    
    func displayRequestContactsAccess() {
        callDisplayRequestContactsAccessCount += 1
    }
    
    func displayErrorView() {
        callDisplayErrorViewCount += 1
    }
    
    func displayEmptyContactsView() {
        callDisplayEmptyContactsViewCount += 1
    }
}

private final class ReferralInviteContactsCoordinatorSpy: ReferralInviteContactsCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformCount = 0
    private(set) var action: ReferralInviteContactsAction?
    
    func perform(action: ReferralInviteContactsAction) {
        callPerformCount += 1
        self.action = action
    }
}

final class ReferralInviteContactsPresenterTests: XCTestCase {
    private let viewControllerSpy = ReferralInviteContactsDisplaySpy()
    private let coordinatorSpy = ReferralInviteContactsCoordinatorSpy()
    private lazy var sut: ReferralInviteContactsPresenter = {
        let sut = ReferralInviteContactsPresenter(
            coordinator: coordinatorSpy
        )
        sut.viewController = viewControllerSpy
        
        return sut
    }()
    
    func testPresentContacts_WhenCalled_ShouldDisplayTheRightValues() {
        let contacts = [
            ReferralInviteContact(
                name: "Gabriela Lacerda Rodrigues",
                imageUrl: URL(string: "https://img.freepik.com/free-vector/cartoon-monster-face-avatar-halloween-monster_6996-1154.jpg?size=338&ext=jpg"),
                invitationText: "Some invitation text",
                phoneNumber: "1198972727",
                imageData: nil
            ),
            ReferralInviteContact(
                name: "Camila Fernandes Silva",
                imageUrl: URL(string: "https://img.freepik.com/free-vector/cartoon-monster-face-avatar-halloween-monster_6996-1154.jpg?size=338&ext=jpg"),
                invitationText: "Some invitation text",
                phoneNumber: "11989727282",
                imageData: nil
            )
        ]
        
        sut.presentContacts(contacts: contacts)
        
        XCTAssertEqual(viewControllerSpy.callDisplayContactsCount, 1)
        XCTAssertNotNil(viewControllerSpy.cellPresenters)
    }
    
    func testPresentRequestAuthorization_WhenCalledFromPresenter_ShouldCallDisplayRequest() {
        sut.presentRequestAuthorization()
        
        XCTAssertEqual(viewControllerSpy.callDisplayRequestContactsAccessCount, 1)
    }
    
    func testPresentContactsCountText_WhenCalledFromPresenter_ShouldCallDisplaycontactsCount() {
        let someContactsCount = "10"
        sut.presentContactsCountText(contactsCount: someContactsCount)
        
        XCTAssertEqual(viewControllerSpy.callDisplayContactsCountCount, 1)
        XCTAssertEqual(viewControllerSpy.contactsCount, someContactsCount)
    }
    
    func testPresentReward_WhenCalledFromPresenter_ShouldCallDisplayReward() {
        let someReward = 10.0
        sut.presentReward(reward: someReward)
        
        XCTAssertEqual(viewControllerSpy.callDisplayRewardCount, 1)
        XCTAssertEqual(viewControllerSpy.reward, "R$ 10")
    }
}
