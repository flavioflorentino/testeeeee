import AnalyticsModule
import Core
import FeatureFlag
import XCTest

@testable import PicPay

private final class ReferralInviteContactsPresenterSpy: ReferralInviteContactsPresenting {
    var viewController: ReferralInviteContactsDisplay?
    private(set) var callPresentRequestAuthorizationCount = 0
    private(set) var callPresentContactsCount = 0
    private(set) var callPresentErrorCount = 0
    private(set) var calldidNextStepCountCount = 0
    private(set) var callPresentRewardCount = 0
    private(set) var callPresentEmptyIndicationsViewCount = 0
    private(set) var callPresentContactsCountTextCount = 0
    private(set) var contacts: [ReferralInviteContact]?
    private(set) var action: ReferralInviteContactsAction?
    private(set) var reward: Double?
    private(set) var contactsCount: String?
    
    func presentRequestAuthorization() {
        callPresentRequestAuthorizationCount += 1
    }
    
    func presentError() {
        callPresentErrorCount += 1
    }
    
    func presentContacts(contacts: [ReferralInviteContact]) {
        callPresentContactsCount += 1
        self.contacts = contacts
    }
    
    func didNextStep(action: ReferralInviteContactsAction) {
        calldidNextStepCountCount += 1
        self.action = action
    }
    
    func presentContactsCountText(contactsCount: String) {
        callPresentContactsCountTextCount += 1
        self.contactsCount = contactsCount
        
    }
    
    func presentReward(reward: Double) {
        callPresentRewardCount += 1
        self.reward = reward
    }
    
    func presentEmptyIndicationsView() {
        callPresentEmptyIndicationsViewCount += 1
    }
}

private final class ReferralInviteContactsServiceSpy: ReferralInviteContactsServicing {
    private(set) var callFetchUnregisteredContactsCount = 0
    let referralInviteContactsMock = [
        ReferralInviteContact(
            name: "Gabriela Lacerda Rodrigues",
            imageUrl: URL(string: "https://img.freepik.com/free-vector/cartoon-monster-face-avatar-halloween-monster_6996-1154.jpg?size=338&ext=jpg"),
            invitationText: "Some Invitation Text",
            phoneNumber: "11989616761",
            imageData: nil
        ),
        ReferralInviteContact(
            name: "Camila Fernandes Silva",
            imageUrl: URL(string: "https://img.freepik.com/free-vector/cartoon-monster-face-avatar-halloween-monster_6996-1154.jpg?size=338&ext=jpg"),
            invitationText: "Some Invitation Text",
            phoneNumber: "11989616761",
            imageData: nil
        ),
        ReferralInviteContact(
            name: "Victor Oliveira",
            imageUrl: URL(string: "https://img.freepik.com/free-vector/cartoon-monster-face-avatar-halloween-monster_6996-1154.jpg?size=338&ext=jpg"),
            invitationText: "Some Invitation Text",
            phoneNumber: "11989616761",
            imageData: nil
        ),
    ]
    
    func fetchUnregisteredContacts(
        userAgendaContacts: [UserAgendaContact],
        completion: @escaping (Result<[ReferralInviteContact], ApiError>) -> Void
    ) {
        callFetchUnregisteredContactsCount += 1
        completion(.success(referralInviteContactsMock))
    }
}

final class ReferralInviteContactsViewModelTests: XCTestCase {
    typealias Dependencies = HasFeatureManager & HasAnalytics & HasReferralManager
    private let presenterSpy = ReferralInviteContactsPresenterSpy()
    private let serviceSpy = ReferralInviteContactsServiceSpy()
    private let featureManager = FeatureManagerMock()
    private let analyticsSpy = AnalyticsSpy()
    private let referralManagerMock = ReferralManagerMock()
    private lazy var dependencies: Dependencies = DependencyContainerMock(
        featureManager,
        analyticsSpy,
        referralManagerMock
    )
    
    private let contactsServiceMock = ContactsListServiceMock(
        status: .success,
        permissionStatus: .authorized
    )
    
    private lazy var sut = ReferralInviteContactsViewModel(
        service: serviceSpy,
        presenter: presenterSpy,
        dependencies: dependencies,
        contactsService: contactsServiceMock
    )
    
    func testFetchUnregisterContacts_WhenCalled_ShouldHaveContacts() {
        sut.fetchUnregisterContacts()
        
        XCTAssertEqual(contactsServiceMock.isGetContactsListCalled, true)
        XCTAssertEqual(serviceSpy.callFetchUnregisteredContactsCount, 1)
        XCTAssertEqual(presenterSpy.callPresentContactsCount, 1)
        XCTAssertNotNil(presenterSpy.contacts)
    }
    
    func testRequestContactsAccess_WhenCalled_ShouldCallRequestPremission() {
        sut.requestContactsAccess()
        
        XCTAssert(contactsServiceMock.isRequestPermissionToAccessContactsCalled)
    }
}
