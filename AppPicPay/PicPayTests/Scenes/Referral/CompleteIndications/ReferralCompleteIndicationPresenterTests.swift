import UI
import XCTest
@testable import PicPay

private final class ReferralCompleteIndicationDisplaySpy: ReferralCompleteIndicationDisplay {
    private(set) var callDisplayCompleteIndicationsCount = 0
    private(set) var callDisplayEmptyIndicationsViewCount = 0
    private(set) var callDisplayErrorViewCount = 0
    private(set) var callDisplayIndicationsDescriptionAtributtedCount = 0
    private(set) var callDisplayRewardCount = 0
    private(set) var cellPresenters: [ReferralCompleteIndicationCellPresenting]?
    private(set) var description: NSAttributedString?
    private(set) var reward: String?
    
    func displayCompleteIndications(_ cells: [ReferralCompleteIndicationCellPresenting]) {
        callDisplayCompleteIndicationsCount += 1
        self.cellPresenters = cells
    }
    
    func displayEmptyIndicationsView() {
        callDisplayEmptyIndicationsViewCount += 1
    }
    
    func displayErrorView() {
        callDisplayErrorViewCount += 1
    }
    
    func displayIndicationsDescriptionAtributted(atributted: NSAttributedString) {
        callDisplayIndicationsDescriptionAtributtedCount += 1
        self.description = atributted
    }
    
    func displayReward(reward: String?) {
        callDisplayRewardCount += 1
        self.reward = reward
    }
}

final class ReferralCompleteIndicationPresenterTests: XCTestCase {
    private let viewControllerSpy = ReferralCompleteIndicationDisplaySpy()
    private lazy var sut: ReferralCompleteIndicationPresenter = {
        let sut = ReferralCompleteIndicationPresenter()
        sut.viewController = viewControllerSpy
        
        return sut
    }()
    
    func testPresentCompleteIndications_WhenCalledFromPresenter_ShouldDisplayTheRightValues() {
        sut.presentCompleteIndications(indications: ReferralIndicationMock.completeIndications.completeInvitations)
        
        XCTAssertEqual(viewControllerSpy.callDisplayCompleteIndicationsCount, 1)
        XCTAssertNotNil(viewControllerSpy.cellPresenters)
    }
    
    func testPresentIndicationsCount_WhenCalledFromPresenter_ShouldCallDisplayDescription() {
        sut.presentIndicationsCount(indicationsCount: 10)

        XCTAssertEqual(viewControllerSpy.callDisplayIndicationsDescriptionAtributtedCount, 1)
    }

    func testUpdateDescription_WhenCalledFromPresenter_ShouldCallDisplayDescription() {
        let someIndicationCount = 10
        sut.presentIndicationsCount(indicationsCount: someIndicationCount)
        
        sut.updateDescription()

        XCTAssertEqual(viewControllerSpy.callDisplayIndicationsDescriptionAtributtedCount, 2)
        XCTAssertNotNil(viewControllerSpy.description)
    }
}
