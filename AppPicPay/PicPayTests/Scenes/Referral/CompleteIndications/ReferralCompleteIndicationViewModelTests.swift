import XCTest
import Core
import FeatureFlag
@testable import PicPay

private final class ReferralCompleteIndicationPresenterSpy: ReferralCompleteIndicationPresenting {
    var viewController: ReferralCompleteIndicationDisplay?
    private(set) var callPresentCompleteIndicationsCount = 0
    private(set) var callPresentEmptyIndicationsCount = 0
    private(set) var callPresentIndicationsCountCount = 0
    private(set) var callUpdateDescriptionCount = 0
    private(set) var callPresentErrorAlertCount = 0
    private(set) var callPresentRewardCount = 0
    private(set) var indications: [ReferralCompleteInvitation]?
    private(set) var rewardValue: Double?
    private(set) var indicationsCount: Int?
    private(set) var reward: Double?
    
    func presentCompleteIndications(indications: [ReferralCompleteInvitation]) {
        callPresentCompleteIndicationsCount += 1
        self.indications = indications
    }
    
    func presentErrorAlert() {
        callPresentErrorAlertCount += 1
    }
    
    func presentEmptyIndications() {
        callPresentEmptyIndicationsCount += 1
    }
    
    func presentIndicationsCount(indicationsCount: Int) {
        callPresentIndicationsCountCount += 1
        self.indicationsCount = indicationsCount
    }
    
    func presentReward(reward: Double) {
        callPresentRewardCount += 1
        self.reward = reward
    }
    
    func updateDescription() {
        callUpdateDescriptionCount += 1
    }
}

private final class ReferralCompleteIndicationServiceSpy: ReferralCompleteIndicationServicing {
    private(set) var callFetchCompleteIndicationsCount = 0
    var fetchCompleteIndicationsExpectedResult: Result<ReferralCompleteIndication, Error>?

    func fetchCompleteIndications(
        lastIndicationId: String?,
        completion: @escaping (Result<ReferralCompleteIndication, Error>) -> Void
    ) {
        callFetchCompleteIndicationsCount += 1
        guard let expectedResult = fetchCompleteIndicationsExpectedResult else {
            XCTFail("Expected result not defined")
            return
        }
        
        completion(expectedResult)
    }
}

final class ReferralCompleteIndicationViewModelTests: XCTestCase {
    typealias Dependencies = HasFeatureManager
    private let presenterSpy = ReferralCompleteIndicationPresenterSpy()
    private let serviceSpy = ReferralCompleteIndicationServiceSpy()
    private let featureManager = FeatureManagerMock()
    private lazy var dependencies: Dependencies = DependencyContainerMock(featureManager)
    
    private lazy var sut = ReferralCompleteIndicationViewModel(
        service: serviceSpy,
        presenter: presenterSpy,
        dependencies: dependencies
    )
    
    
    func testRefreshIndications_WhenCalledWithViewModel_ShouldHaveIndications() {
        serviceSpy.fetchCompleteIndicationsExpectedResult = .success(ReferralIndicationMock.completeIndications)

        sut.refreshIndications()
        
        XCTAssertEqual(serviceSpy.callFetchCompleteIndicationsCount, 1)
        XCTAssertEqual(presenterSpy.callPresentCompleteIndicationsCount, 1)
        XCTAssertEqual(presenterSpy.callPresentIndicationsCountCount, 1)
        XCTAssertEqual(presenterSpy.callPresentRewardCount, 1)
        XCTAssertNotNil(presenterSpy.reward)
    }
    
    
    
    func testUpdateIndicationsIfNeeded_WhenCalledWithViewModel_ShouldHaveIndications() {
        serviceSpy.fetchCompleteIndicationsExpectedResult = .success(ReferralIndicationMock.completeIndications)

        sut.fetchCompleteIndications()
        sut.updateIndicationsIfNeeded(index: 0)
        
        XCTAssertEqual(serviceSpy.callFetchCompleteIndicationsCount, 2)
        XCTAssertEqual(presenterSpy.callPresentCompleteIndicationsCount, 2)
        XCTAssertEqual(presenterSpy.callPresentIndicationsCountCount, 2)
        XCTAssertEqual(presenterSpy.callPresentRewardCount, 2)
        XCTAssertNotNil(presenterSpy.reward)
    }
    
    func testFetchCompleteIndications_WhenCalledWithViewModel_ShouldHaveIndications() {
        serviceSpy.fetchCompleteIndicationsExpectedResult = .success(ReferralIndicationMock.completeIndications)

        sut.fetchCompleteIndications()
        
        XCTAssertEqual(serviceSpy.callFetchCompleteIndicationsCount, 1)
        XCTAssertEqual(presenterSpy.callPresentCompleteIndicationsCount, 1)
        XCTAssertEqual(presenterSpy.callPresentIndicationsCountCount, 1)
        XCTAssertEqual(presenterSpy.callPresentRewardCount, 1)
        XCTAssertNotNil(presenterSpy.reward)
    }
}
