import Foundation
@testable import PicPay

struct ReferralIndicationMock {
    static let indications = [
        ReferralIndication(
            campaign: "Some  Campaign",
            invitee: Invitee(
                invitationId: "SOMECONSUMERID",
                isReminded: true,
                consumerId: 5050,
                userName: "Some User Name",
                name: "Some Name",
                email: "someemail@email.com",
                avatarUrl: URL(string: "https://img.freepik.com/free-vector/cartoon-monster-face-avatar-halloween-monster_6996-1154.jpg?size=338&ext=jpg"),
                mobileAreaCode: 55,
                mobilePhone: 989616062
            ),
            expiresAt: ExpiresAt(days: 5)
        ),
        ReferralIndication(
            campaign: "Some  Campaign",
            invitee: Invitee(
                invitationId: "SOMECONSUMERID",
                isReminded: false,
                consumerId: 5050,
                userName: "Some User Name",
                name: "Some Name",
                email: "someemail@email.com",
                avatarUrl: URL(string: "https://img.freepik.com/free-vector/cartoon-monster-face-avatar-halloween-monster_6996-1154.jpg?size=338&ext=jpg"),
                mobileAreaCode: 55,
                mobilePhone: 989616062
            ),
            expiresAt: ExpiresAt(days: 2)
        )
    ]
    
    static let completeIndications = ReferralCompleteIndication(
        completeInvitations: [
            ReferralCompleteInvitation(
                campaign: "somecampaign",
                invitee: CompleteInvitationInvitee(
                    userName: "someusername",
                    name: "Some Indication",
                    email: "someemail@gmail.com",
                    avatarUrl: URL(
                        string: "https://img.freepik.com/free-vector/cartoon-monster-face-avatar-halloween-monster_6996-1154.jpg?size=338&ext=jpg"
                    )
                ),
                rewardAmount: 10.0
            ),
            ReferralCompleteInvitation(
                campaign: "othercampaign",
                invitee: CompleteInvitationInvitee(
                    userName: "otherusername",
                    name: "Other Indication",
                    email: "otheremail@gmail.com",
                    avatarUrl: URL(
                        string: "https://img.freepik.com/free-vector/cartoon-monster-face-avatar-halloween-monster_6996-1154.jpg?size=338&ext=jpg"
                    )
                ),
                rewardAmount: 20.0
            )],
        lastItemId: "somelastId",
        total: 3
    )
}
