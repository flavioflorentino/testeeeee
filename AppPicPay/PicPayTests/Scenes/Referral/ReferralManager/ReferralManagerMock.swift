import Foundation
@testable import PicPay

final class ReferralManagerMock: ReferralManageable {
    private(set) var callLoadReferralCount = 0
    var referral: ReferralCodeModel?
    var loadError: Error?
    
    func loadReferral() {
        callLoadReferralCount += 1
    }
}
