import UI
import XCTest
@testable import PicPay

private final class ReferralCodeDisplaySpy: ReferralCodeDisplay {
    private(set) var callDisplayDataCount = 0
    private(set) var callDisplayAlertError = 0
    private(set) var referralCodeData: (imageUrl: URL?, title: String, description: String, code: String)?
    private(set) var error: Error?

    func displayData(imageUrl: URL?, title: String, description: String, code: String) {
        callDisplayDataCount += 1
        referralCodeData = (imageUrl: imageUrl, title: title, description: description, code: code)
    }

    func displayAlertError(with error: Error) {
        callDisplayAlertError += 1
        self.error = error
    }
}

private final class ReferralCodeCoordinatorSpy: ReferralCodeCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformCount = 0
    private(set) var action: ReferralCodeAction?
    private(set) var shareController: UIViewController?
    
    func perform(action: ReferralCodeAction) {
        callPerformCount += 1
        guard case let .presentShare(controller) = action else {
            return
        }
        shareController = controller
        self.action = action
    }
}

final class ReferralCodePresenterTests: XCTestCase {
    private let viewControllerSpy = ReferralCodeDisplaySpy()
    private let coordinatorSpy = ReferralCodeCoordinatorSpy()
    private lazy var sut: ReferralCodePresenter = {
        let sut = ReferralCodePresenter(coordinator: coordinatorSpy)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    func testPresentData_ShouldDisplayTheRightValues() {
        let imageURL = URL(string: "http://cdn.picpay.com/imgs/campaign-global-mgm-home.png")
        let title = "some title"
        let description = "some description"
        let code = "some code"
        
        sut.presentData(imageUrl: imageURL, title: title, description: description, code: code)
        
        XCTAssertEqual(viewControllerSpy.callDisplayDataCount, 1)
        XCTAssertEqual(viewControllerSpy.referralCodeData?.imageUrl, imageURL)
        XCTAssertEqual(viewControllerSpy.referralCodeData?.title, title)
        XCTAssertEqual(viewControllerSpy.referralCodeData?.description, description)
        XCTAssertEqual(viewControllerSpy.referralCodeData?.code, code)
    }
    
    func testDidNextStep_WhenReceivesAnAction_ShouldRedirectToTheCoordinator() {
        let controller = UIViewController()
        sut.didNextStep(action: .presentShare(shareController: controller))
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.shareController, controller)
        XCTAssertNotNil(coordinatorSpy.action)
    }
}
