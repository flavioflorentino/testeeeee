import XCTest
import Core
import AnalyticsModule
@testable import PicPay

private final class ReferralCodePresenterSpy: ReferralCodePresenting {
    var viewController: ReferralCodeDisplay?
    private(set) var callPresentDataCount = 0
    private(set) var callPresentAlertError = 0
    private(set) var referralCodeData: (imageUrl: URL?, title: String, description: String, code: String)?
    private(set) var error: Error?
    private(set) var calldidNextStepCount = 0
    
    func presentData(imageUrl: URL?, title: String, description: String, code: String) {
        callPresentDataCount += 1
        referralCodeData = (imageUrl: imageUrl, title: title, description: description, code: code)
    }
    
    func didNextStep(action: ReferralCodeAction) {
        calldidNextStepCount += 1
    }
    
    func presentAlertError(with error: Error) {
        callPresentAlertError += 1
        self.error = error
    }
}

private final class SocialShareWorkerSpy: SocialShareable {
    private(set) var callShareCount = 0
    private(set) var type: SocialType?
    private(set) var completion: ((UIViewController?) -> Void)?
    private(set) var shareText: String?
    
    func share(type: SocialType) {
        callShareCount += 1
        self.type = type
        
        switch type {
        case let .facebook(text, completion):
            self.completion = completion
            self.shareText = text
        case let .twitter(text, completion):
            self.completion = completion
            self.shareText = text
        case let .native(text, completion):
            self.completion = completion
            self.shareText = text
        case let .whatsapp(text):
            self.shareText = text
        }
    }
}

private final class ReferralCodeServiceSpy: ReferralCodeServicing {
    private(set) var callGetReferralCodeDataCount = 0
    private(set) var successModelMock = ReferralCodeModel(
        campaign: Campaign(
            imageURL: URL(string: "http://cdn.picpay.com/imgs/campaign-global-mgm-home.png"),
            title: "Campanha global MGM",
            amountInviter: 10.0,
            amountInvitee: 10.0,
            description: "Campanha global MGM",
            terms: "https://cdn.picpay.com/webview/terms/mgm-pf.html",
            rules: []
        ),
        userInfo: UserInfo(
            name: "Henrique",
            mgmCode: "UZSSZW"),
        share: Share(
            twitter: ContentShare(
                link: "http://www.picpay.com/convite?02-UZSSZW",
                text: "Compartilhe pelo Email! http://www.picpay.com/convite?02-UZSSZW"
            ),
            whatsApp: ContentShare(
                link: "http://www.picpay.com/convite?02-UZSSZW",
                text: "Compartilhe pelo Email! http://www.picpay.com/convite?02-UZSSZW"
            ),
            facebook: ContentShare(
                link: "http://www.picpay.com/convite?02-UZSSZW",
                text: "Compartilhe pelo Email! http://www.picpay.com/convite?02-UZSSZW"
            ),
            systemOption: ContentShare(
                link: "http://www.picpay.com/convite?02-UZSSZW",
                text: "Compartilhe pelo Email! http://www.picpay.com/convite?02-UZSSZW"
            )
        )
    )
    
    func getReferralCodeData(completion: @escaping (Result<ReferralCodeModel, ApiError>) -> Void) {
        callGetReferralCodeDataCount += 1
        completion(.success(successModelMock))
    }
}

private final class ReferralCodeFailureServiceSpy: ReferralCodeServicing {
    func getReferralCodeData(completion: @escaping (Result<ReferralCodeModel, ApiError>) -> Void) {
        let error = ApiError.connectionFailure
        completion(.failure(error))
    }
}

final class ReferralCodeViewModelTests: XCTestCase {
    private let presenterSpy = ReferralCodePresenterSpy()
    private let socialShareSpy = SocialShareWorkerSpy()
    private let analytics = AnalyticsSpy()
    private lazy var dependencies = DependencyContainerMock(analytics)
    private let serviceSpy = ReferralCodeServiceSpy()
    
    private lazy var sut = ReferralCodeViewModel(
        service: serviceSpy,
        presenter: presenterSpy,
        socialShareWorker: socialShareSpy,
        dependencies: dependencies
    )
    
    func testcopyCode_WhenCalled_ShouldHaveTheCorrectCode() {
        let code = "SOMECODE"
        sut.copyCode(code: code)
        
        XCTAssertEqual(UIPasteboard.general.string, code)
    }
    
    func testShareCode_WhenCalled_ShouldCallWorkerWithCorrectType() {
        sut.shareCode(type: .facebook)
        
        XCTAssertEqual(socialShareSpy.callShareCount, 1)
        XCTAssertNotNil(socialShareSpy.type)
        XCTAssertNotNil(socialShareSpy.completion)
    }
    
    func testFetchData_WhenCalled_ShouldCallPresentDataWithCorrecData() {
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.callPresentDataCount, 1)
        XCTAssertEqual(presenterSpy.referralCodeData?.imageUrl, serviceSpy.successModelMock.campaign.imageURL)
        XCTAssertEqual(presenterSpy.referralCodeData?.title, serviceSpy.successModelMock.campaign.title)
        XCTAssertEqual(presenterSpy.referralCodeData?.description, serviceSpy.successModelMock.campaign.description)
        XCTAssertEqual(presenterSpy.referralCodeData?.code, serviceSpy.successModelMock.userInfo.mgmCode)
    }
    
    func testFetchData_WhenFailed_ShouldCallPresentAlertError() {
        sut = ReferralCodeViewModel(
            service: ReferralCodeFailureServiceSpy(),
            presenter: presenterSpy,
            socialShareWorker: socialShareSpy,
            dependencies: dependencies
        )
        
        sut.fetchData()
        
        XCTAssertEqual(presenterSpy.callPresentAlertError, 1)
        XCTAssertNotNil(presenterSpy.error)
    }
}

