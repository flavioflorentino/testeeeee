@testable import PicPay
import XCTest

final class ReferralCodeCoordinatorTests: XCTestCase {
    private let viewControllerMock = ViewControllerMock()
    
    private lazy var sut: ReferralCodeCoordinator = {
        let coordinator = ReferralCodeCoordinator()
        coordinator.viewController = viewControllerMock
        return coordinator
    }()
    
    func testPerform_WhenActionPresentShare_ShouldPresentShareController() throws {
        let shareController = UIViewController()
        sut.perform(action: .presentShare(shareController: shareController))
        
        XCTAssertEqual(viewControllerMock.didPresentControllerCount, 1)
        XCTAssertEqual(viewControllerMock.viewControllerPresented, shareController)
    }
}
