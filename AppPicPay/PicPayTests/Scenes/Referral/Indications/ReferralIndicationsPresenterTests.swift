import UI
import XCTest
@testable import PicPay

private final class ReferralIndicationsDisplaySpy: ReferralIndicationsDisplay {
    private(set) var callDisplayIndicationsCount = 0
    private(set) var callDisplayIndicationsDescriptionAtributtedCount = 0
    private(set) var callDisplayEmptyIndicationsDescriptionCount = 0
    private(set) var callDisplayEmptyIndicationsViewCount = 0
    private(set) var callDisplayErrorViewCount = 0
    private(set) var callDisplayRewardCount = 0
    private(set) var callDisplayErrorAlertCount = 0
    private(set) var cellPresenters: [ReferralIndicationCellPresenting]?
    private(set) var description: NSAttributedString?
    private(set) var reward: String?
    
    
    func displayIndications(_ cells: [ReferralIndicationCellPresenting]) {
        callDisplayIndicationsCount += 1
        self.cellPresenters = cells
    }
    
    func displayIndicationsDescriptionAtributted(description: NSAttributedString) {
        callDisplayIndicationsDescriptionAtributtedCount += 1
        self.description = description
    }
    
    func displayEmptyIndicationsDescription() {
        callDisplayEmptyIndicationsDescriptionCount += 1
    }
    
    func displayEmptyIndicationsView() {
        callDisplayEmptyIndicationsViewCount += 1
    }
    
    func displayReward(reward: String?) {
        callDisplayRewardCount += 1
        self.reward = reward
    }
    
    func displayErrorView() {
        callDisplayErrorViewCount += 1
    }
    
    func displayErrorAlert() {
        callDisplayErrorAlertCount += 1
    }
}

final class ReferralIndicationsPresenterTests: XCTestCase {
    private let viewControllerSpy = ReferralIndicationsDisplaySpy()
    private lazy var sut: ReferralIndicationsPresenter = {
        let sut = ReferralIndicationsPresenter()
        sut.viewController = viewControllerSpy
        
        return sut
    }()
    
    func testPresentIndications_WhenCalledFromPresenter_ShouldDisplayTheRightValues() {
        let referralIndicationsMock = [
            ReferralIndication(
                campaign: "Some  Campaign",
                invitee: Invitee(
                    invitationId: "SOMECONSUMERID",
                    isReminded: true,
                    consumerId: 5050,
                    userName: "Some User Name",
                    name: "Some Name",
                    email: "someemail@email.com",
                    avatarUrl: URL(string: "https://img.freepik.com/free-vector/cartoon-monster-face-avatar-halloween-monster_6996-1154.jpg?size=338&ext=jpg"),
                    mobileAreaCode: 55,
                    mobilePhone: 989616062
                ),
                expiresAt: ExpiresAt(days: 5)
            ),
            ReferralIndication(
                campaign: "Some  Campaign",
                invitee: Invitee(
                    invitationId: "SOMECONSUMERID",
                    isReminded: false,
                    consumerId: 5050,
                    userName: "Some User Name",
                    name: "Some Name",
                    email: "someemail@email.com",
                    avatarUrl: URL(string: "https://img.freepik.com/free-vector/cartoon-monster-face-avatar-halloween-monster_6996-1154.jpg?size=338&ext=jpg"),
                    mobileAreaCode: 55,
                    mobilePhone: 989616062
                ),
                expiresAt: ExpiresAt(days: 2)
            )
        ]
        
        sut.presentIndications(indications: referralIndicationsMock, rewardValue: 10)
        
        XCTAssertEqual(viewControllerSpy.callDisplayIndicationsCount, 1)
        XCTAssertNotNil(viewControllerSpy.cellPresenters)
    }
    
    func testPresentEmptyIndicationsDescription_WhenCalledFromPresenter_ShouldCallDisplayDescription() {
        sut.presentEmptyIndicationsDescription()
        
        XCTAssertEqual(viewControllerSpy.callDisplayEmptyIndicationsDescriptionCount, 1)
    }
    
    func testPresentIndicationsCount_WhenCalledFromPresenter_ShouldCallDisplayIndications() {
        let someIndicationCount = 10
        sut.presentIndicationsCount(indicationsCount: someIndicationCount)
        
        XCTAssertEqual(viewControllerSpy.callDisplayIndicationsDescriptionAtributtedCount, 1)
        XCTAssertNotNil(viewControllerSpy.description)
    }
    
    func testPresntReward_WhenCalledFromPresenter_ShouldCallDisplayReward() {
        let someReward = 10.0
        sut.presentReward(reward: someReward)
        
        XCTAssertEqual(viewControllerSpy.callDisplayRewardCount, 1)
        XCTAssertNotNil(viewControllerSpy.reward)
    }
    
    func testPresentErrorView_WhenCalledFromPresenter_ShouldCallDisplayErrorView() {
        sut.presentErrorView()
        
        XCTAssertEqual(viewControllerSpy.callDisplayErrorViewCount, 1)
    }
    
    func testPresentAlertError_WhenCalledFromPresenter_ShouldCallDisplayErrorAlert() {
         sut.presentAlertError()
         
         XCTAssertEqual(viewControllerSpy.callDisplayErrorAlertCount, 1)
     }
}
