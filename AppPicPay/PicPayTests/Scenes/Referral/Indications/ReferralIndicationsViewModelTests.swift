import XCTest
import Core
import FeatureFlag
@testable import PicPay

private final class ReferralIndicationsPresenterSpy: ReferralIndicationsPresenting {
    var viewController: ReferralIndicationsDisplay?
    private(set) var callPresentIndicationsCount = 0
    private(set) var callPresentEmptyIndicationsCount = 0
    private(set) var callPresentIndicationsCountCount = 0
    private(set) var callUpdateDescriptionCount = 0
    private(set) var callPresentEmptyIndicationsDescriptionCount = 0
    private(set) var callPresentErrorViewCount = 0
    private(set) var callPresentRewardCount = 0
    private(set) var callPresentAlertErrorCount = 0
    private(set) var indications: [ReferralIndication]?
    private(set) var rewardValue: Double?
    private(set) var indicationsCount: Int?
    private(set) var reward: Double?
    
    func presentIndications(indications: [ReferralIndication], rewardValue: Double) {
        callPresentIndicationsCount += 1
        self.indications = indications
        self.rewardValue = rewardValue
    }
    
    func presentEmptyIndications() {
        callPresentEmptyIndicationsCount += 1
    }
    
    func presentIndicationsCount(indicationsCount: Int) {
        callPresentIndicationsCountCount += 1
        self.indicationsCount = indicationsCount
    }
    
    func presentEmptyIndicationsDescription() {
        callPresentEmptyIndicationsDescriptionCount += 1
    }
    
    func presentErrorView() {
        callPresentErrorViewCount += 1
    }
    
    func presentReward(reward: Double) {
        callPresentRewardCount += 1
        self.reward = reward
    }
    
    func updateDescription() {
        callUpdateDescriptionCount += 1
    }

    func presentAlertError() {
        callPresentAlertErrorCount += 1
    }
}

private final class ReferralIndicationsServiceSpy: ReferralIndicationsServicing {
    private(set) var callFetchIndicationsCount = 0
    private(set) var callSendRememberCount = 0
    var fetchIndicationsExpectedResult: Result<[ReferralIndication], Error>?
    var sendRememberExpectedResult: Result<Void, ApiError>?
    
    func fetchIndications(completion: @escaping (Result<[ReferralIndication], Error>) -> Void) {
        callFetchIndicationsCount += 1
        guard let expectedResult = fetchIndicationsExpectedResult else {
            XCTFail("Expected result not defined")
            return
        }
        completion(expectedResult)
    }
    
    func sendRemember(invitationId: String, completion: @escaping (Result<Void, ApiError>) -> Void) {
        callSendRememberCount += 1
        guard let expectedResult = sendRememberExpectedResult else {
            XCTFail("Expected result not defined")
            return
        }
        completion(expectedResult)
    }
}

final class ReferralIndicationsViewModelTests: XCTestCase {
    typealias Dependencies = HasFeatureManager
    private let presenterSpy = ReferralIndicationsPresenterSpy()
    private let serviceSpy = ReferralIndicationsServiceSpy()
    private let featureManager = FeatureManagerMock()
    private lazy var dependencies: Dependencies = DependencyContainerMock(featureManager)
    
    private lazy var sut = ReferralIndicationsViewModel(
        service: serviceSpy,
        presenter: presenterSpy,
        dependencies: dependencies
    )
    
    
    func testFetchIndications_WhenCalledWithIndications_ShouldHaveIndications() {
        serviceSpy.fetchIndicationsExpectedResult = .success(ReferralIndicationMock.indications)
        sut.fetchIndications()
        
        XCTAssertEqual(serviceSpy.callFetchIndicationsCount, 1)
        XCTAssertEqual(presenterSpy.callPresentIndicationsCount, 1)
        XCTAssertEqual(presenterSpy.callPresentIndicationsCountCount, 1)
    }
    
    func testFetchIndications_WhenCalledWithEmptyIndications_ShouldDisplayEmpty() {
        serviceSpy.fetchIndicationsExpectedResult = .success([])
        sut.fetchIndications()
        
        XCTAssertEqual(serviceSpy.callFetchIndicationsCount, 1)
        XCTAssertEqual(presenterSpy.callPresentEmptyIndicationsCount, 1)
        XCTAssertEqual(presenterSpy.callPresentEmptyIndicationsDescriptionCount, 1)
    }
    
    func testFetchIndications_WhenFailed_ShouldPresentError() {
        serviceSpy.fetchIndicationsExpectedResult = .failure(ApiError.serverError)
        sut.fetchIndications()
        
        XCTAssertEqual(serviceSpy.callFetchIndicationsCount, 1)
        XCTAssertEqual(presenterSpy.callPresentErrorViewCount, 1)
    }
    
    func testInviteRemember_WhenCalledWithViewModel_ShouldUpdateIndications() {
        serviceSpy.sendRememberExpectedResult = .success
        serviceSpy.fetchIndicationsExpectedResult = .success(ReferralIndicationMock.indications)
        sut.fetchIndications()
        
        sut.inviteRemember(index: 0)
        
        XCTAssertEqual(serviceSpy.callSendRememberCount, 1)
        XCTAssertEqual(serviceSpy.callFetchIndicationsCount, 2)
        XCTAssertNotNil(presenterSpy.indications)
    }
}
