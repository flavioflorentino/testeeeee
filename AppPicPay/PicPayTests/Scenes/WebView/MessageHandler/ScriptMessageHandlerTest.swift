import WebKit
import XCTest

@testable import PicPay

final class ScriptMessageHandlerTest: XCTestCase {
    func testShouldRegisterAtAnUserContentController() {
        let expectedMessageName = "test"

        let userContentController = FakeUserContentController()
        let handler = ScriptMessageHandler(messageName: expectedMessageName)
        handler.register(at: userContentController)

        XCTAssertEqual(userContentController.registeredMessageName, expectedMessageName)
    }

    func testShouldReturnCorrectValueWhenCallJavascriptFunction() throws {
        let expectedMessageCalled = "setTest('value')"

        let fakeWebView = FakeWebView()
        let userContentController = FakeUserContentController()

        let handler = ScriptMessageHandler(messageName: "getTest", returnMessageName: "setTest") { return "value" }
        handler.register(at: userContentController)

        let fakeMessage = FakeScriptMessage(name: "getTest", url: try URL(string: "https://picpay.com").safe())
        fakeMessage.webViewToReturn = { return fakeWebView }

        let registerdHandler = userContentController.registeredHandler
        registerdHandler?.userContentController(userContentController, didReceive: fakeMessage)

        XCTAssertEqual(fakeWebView.receivedMessageToEvaluate, expectedMessageCalled)
    }

    func testShouldNotCallJavascriptFunctionForNilReturnNameOrValue() throws {
        let fakeWebView = FakeWebView()
        let userContentController = FakeUserContentController()

        let handler = ScriptMessageHandler(messageName: "getTest")
        handler.register(at: userContentController)

        let fakeMessage = FakeScriptMessage(name: "getTest", url: try URL(string: "https://picpay.com").safe())
        fakeMessage.webViewToReturn = { return fakeWebView }

        let registerdHandler = userContentController.registeredHandler
        registerdHandler?.userContentController(userContentController, didReceive: fakeMessage)

        XCTAssertNil(fakeWebView.receivedMessageToEvaluate)
    }
}

final class FakeUserContentController: WKUserContentController {
    var registeredMessageName: String?
    var registeredHandler: WKScriptMessageHandler?

    override func add(_ scriptMessageHandler: WKScriptMessageHandler, name: String) {
        registeredHandler = scriptMessageHandler
        registeredMessageName = name
    }
}

final class FakeWebView: WKWebView {
    var receivedMessageToEvaluate: String?

    override func evaluateJavaScript(_ javaScriptString: String, completionHandler: ((Any?, Error?) -> Void)? = nil) {
        receivedMessageToEvaluate = javaScriptString
    }
}
