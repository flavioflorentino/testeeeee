import WebKit
import XCTest

@testable import PicPay

final class ScriptMessageWrapperTest: XCTestCase {
    func testShouldNotReceiveMessageOutsidePicPayHost() throws {
        var didReceiveMessage = false

        let wrapper = ScriptMessageWrapper(messageName: "")
        wrapper.didReceiveMessage = { _ in didReceiveMessage = true }

        var message = FakeScriptMessage(name: "", url: try URL(string: "https://www.google.com").safe())
        wrapper.userContentController(WKUserContentController(), didReceive: message)

        XCTAssertFalse(didReceiveMessage)

        message = FakeScriptMessage(name: "", url: try URL(string: "https://www.picpay.com").safe())
        wrapper.userContentController(WKUserContentController(), didReceive: message)

        XCTAssertTrue(didReceiveMessage)

        didReceiveMessage = false
        message = FakeScriptMessage(name: "", url: try URL(string: "https://www.picpay.me").safe())
        wrapper.userContentController(WKUserContentController(), didReceive: message)

        XCTAssertTrue(didReceiveMessage)

        didReceiveMessage = false
        message = FakeScriptMessage(name: "", url: try URL(string: "https://www.ppay.me").safe())
        wrapper.userContentController(WKUserContentController(), didReceive: message)

        XCTAssertTrue(didReceiveMessage)
    }

    func testShouldNotReceiveMessageWithDifferentName() throws {
        var didReceiveMessage = false

        let wrapper = ScriptMessageWrapper(messageName: "test")
        wrapper.didReceiveMessage = { _ in didReceiveMessage = true }

        var message = FakeScriptMessage(name: "", url: try URL(string: "https://www.picpay.com").safe())
        wrapper.userContentController(WKUserContentController(), didReceive: message)

        XCTAssertFalse(didReceiveMessage)

        didReceiveMessage = false
        message = FakeScriptMessage(name: "test", url: try URL(string: "https://www.picpay.com").safe())
        wrapper.userContentController(WKUserContentController(), didReceive: message)

        XCTAssertTrue(didReceiveMessage)
    }
}
