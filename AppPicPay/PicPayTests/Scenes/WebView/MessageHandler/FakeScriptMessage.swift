import WebKit

final class FakeScriptMessage: WKScriptMessage {
    // MARK: - Properties
    private let _name: String
    let url: URL

    var webViewToReturn: (() -> WKWebView)?

    override var body: Any { return "" }

    override weak var webView: WKWebView? {
        var webView = webViewToReturn?()
        if webView == nil {
            webView = WKWebView(frame: .zero)
        }

        webView?.load(URLRequest(url: url))
        return webView
    }


    override var frameInfo: WKFrameInfo {
        get { return WKFrameInfo() }
    }

    override var name: String { return _name }

    // MARK: - Initialization
    init(name: String, url: URL) {
        self._name = name
        self.url = url
    }
}
