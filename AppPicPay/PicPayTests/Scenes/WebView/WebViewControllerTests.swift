import XCTest
import Core
@testable import PicPay

final class WebViewControllerTests: XCTestCase {
    private let handlerSpy = PinningHandlerSpy()
    private lazy var sut = URLSessionPinning(pinningHandler: handlerSpy)
    private let sessionDelegate = SessionDelegateDummy()
    
    func testUrlSessionWhenReceiveChallengeShouldHaveTheSameChallengeAsPinningHandlerProvider() {
        let session = URLSession()
        let space = URLProtectionSpace(host: "mock", port: 443, protocol: "", realm: "", authenticationMethod: NSURLAuthenticationMethodHTTPBasic)
        let challenge = URLAuthenticationChallenge(protectionSpace: space, proposedCredential: nil, previousFailureCount: 0, failureResponse: nil, error: nil, sender: sessionDelegate)
        
        let expectation = self.expectation(description: "calledHandler")
        let completionHandler: ChallengeHandler = { _,_ in
            expectation.fulfill()
        }
        
        sut.urlSession(session, didReceive: challenge, completionHandler: completionHandler)
        XCTAssertTrue(handlerSpy.challenge === challenge)
        wait(for: [expectation], timeout: 1)
    }
}
