import XCTest
import WebKit
import Core
@testable import PicPay

fileprivate struct LocationManagerMock: LocationManagerContract {
    var authorizedLocation: CLLocation?
    var lastAvailableLocation: CLLocation?
    
    init(latitude: Double, longitude: Double, accuracy: Double, timestamp: Date) {
        authorizedLocation = CLLocation(
            coordinate: CLLocationCoordinate2D(
                latitude: latitude,
                longitude: longitude
            ),
            altitude: .zero,
            horizontalAccuracy: accuracy,
            verticalAccuracy: accuracy,
            timestamp: timestamp)
    }
}

fileprivate struct WebViewServiceMock: WebViewServiceProtocol {
    var consumerWsId: Int?
    
    var userToken: String?
    
    var appVersion: String?
    
    var machineName: String?
    
    var carrierName: String?
    
    var installationId: String?
    
    var location: LocationDescriptor?
    
    init() {
        consumerWsId = 314159265
        userToken = "4u7h-70k3n-d0-u5u4r10"
        appVersion = "42.0"
        machineName = "iPoney 1"
        carrierName = "Mock Carrier"
        installationId = "4P3N4sS-uM-Uu1D-F4k3-b0N1740"
        
        let locationManagerMock = LocationManagerMock.init(
                                        latitude: 12.345,
                                        longitude: 54.321,
                                        accuracy: 1.43,
                                        timestamp: Date(timeIntervalSince1970: 1584021499.983))
        location = LocationDescriptor.init(locationManagerMock.authorizedLocation, currentTimestamp: Date(timeIntervalSince1970: 1584021499.984))
    }
}

class WebViewModelTest: XCTestCase {
    var viewModel: WebViewModel!
    
    lazy private var service = WebViewServiceMock()
    
    func testCreateRequestWithCustomAndPicPayHeaders() {
        viewModel = WebViewModel(withURL: URL(string: "http://mock.url/test/headers/custom&system")!,
                                 sendHeaders: true,
                                 service: service,
                                 customHeaders: ["mock": "header"])

        let output = WebViewModelOutput()
        viewModel.outputs = output
        
        let expectation = XCTestExpectation(description: "request")
        
        output.loadCalled = { (request) in
            XCTAssertEqual(request.url?.absoluteString, "http://mock.url/test/headers/custom&system")
            
            let referenceHeaders = ["mock": "header", "token": "4u7h-70k3n-d0-u5u4r10", "app_version": "42.0", "device_os": "ios", "device_model": "iPoney 1", "carrier": "Mock Carrier", "installation_id": "4P3N4sS-uM-Uu1D-F4k3-b0N1740", "latitude": "12.345000", "longitude": "54.321000", "gps_acc": "1.43", "current_timestamp": "1584021499984", "location_timestamp": "1584021499983"]
            XCTAssertEqual(request.allHTTPHeaderFields, referenceHeaders)
            expectation.fulfill()
        }
        
        viewModel.inputs.viewDidLoad()
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testCreateRequestWithCustomHeaders() {
        viewModel = WebViewModel(withURL: URL(string: "http://mock.url/test/headers/custom")!,
                                 sendHeaders: false,
                                 service: service,
                                 customHeaders: ["mock": "header"])

        let output = WebViewModelOutput()
        viewModel.outputs = output
        
        let expectation = XCTestExpectation(description: "request")
        
        output.loadCalled = { (request) in
            XCTAssertEqual(request.url?.absoluteString, "http://mock.url/test/headers/custom")
            
            let referenceHeaders = ["mock": "header"]
            XCTAssertEqual(request.allHTTPHeaderFields, referenceHeaders)
            expectation.fulfill()
        }
        
        viewModel.inputs.viewDidLoad()
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testCompletedWithSuccess() {
        let url = URL(string: "http://mock.url/test/api/closeWebview")!
        viewModel = WebViewModel(withURL: url, sendHeaders: false, service: service, customHeaders: [:])
        
        let output = WebViewModelOutput()
        viewModel.outputs = output
        
        let expectation = XCTestExpectation(description: "request")
        
        output.didNextActionCalled = { (action) in
            switch action {
            case .completed(let error):
                XCTAssertNil(error)
            default:
                XCTFail("expected completed action")
            }
            expectation.fulfill()
        }
        
        viewModel.inputs.didFinishLoad(url: url)
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testCompletedWithError() {
        let url = URL(string: "http://mock.url/test/api/errorWebview")!
        viewModel = WebViewModel(withURL: url, sendHeaders: false, service: service, customHeaders: [:])

        let output = WebViewModelOutput()
        viewModel.outputs = output
        
        let expectation = XCTestExpectation(description: "request")
        
        output.didNextActionCalled = { (action) in
            switch action {
            case .completed(let error):
                XCTAssertEqual(error?.localizedDescription, "Ocorreu algum erro ao vincular a sua conta. Tente novamente.")
            default:
                XCTFail("expected completed action")
            }
            expectation.fulfill()
        }
        
        viewModel.inputs.didFinishLoad(url: url)
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testRedirectToPicpayMeLink() {
        let url = URL(string: "http://picpay.me/test/link")!
        let viewModel = WebViewModel(withURL: url, sendHeaders: false, service: service, customHeaders: [:])

        let output = WebViewModelOutput()
        viewModel.outputs = output
        
        let expectation = XCTestExpectation(description: "request")
        
        output.loadCalled = {(request) in
            viewModel.inputs.shouldStartWebLoad(with: request, decisionHandler: { _ in })
        }
        
        output.didNextActionCalled = { (action) in
            switch action {
            case .open(let url?):
                XCTAssertEqual(url.absoluteString, "picpay://picpay.me/test/link")
            default:
                XCTFail("expected completed action")
            }
            expectation.fulfill()
        }
        
        viewModel.inputs.viewDidLoad()
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testRedirectToDeepLink() {
        let url = URL(string: "picpay://picpay.com/test/deeplink")!
        let viewModel = WebViewModel(withURL: url, sendHeaders: false, service: service, customHeaders: [:])

        let output = WebViewModelOutput()
        viewModel.outputs = output
        
        let expectation = XCTestExpectation(description: "request")
        
        output.loadCalled = {(request) in
            viewModel.inputs.shouldStartWebLoad(with: request, decisionHandler: { _ in })
        }
        
        output.didNextActionCalled = { (action) in
            switch action {
            case .open(let url?):
                XCTAssertEqual(url.absoluteString, "picpay://picpay.com/test/deeplink")
            default:
                XCTFail("expected completed action")
            }
            expectation.fulfill()
        }
        
        viewModel.inputs.viewDidLoad()
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testHeaderToStreamlabsToken() {
        let url = URL(string: "https://iw9hbq7q6h.execute-api.us-east-1.amazonaws.com/prod/streamlabs/token/")!
        let viewModel = WebViewModel(withURL: url, sendHeaders: true, service: service, customHeaders: [:])

        let output = WebViewModelOutput()
        viewModel.outputs = output
        
        let expectation = XCTestExpectation(description: "request")
        
        output.loadCalled = {(request) in
            guard let consumerId = request.allHTTPHeaderFields?["consumer_id"] else {
                viewModel.inputs.shouldStartWebLoad(with: request) { shouldLoad in
                    XCTAssertFalse(shouldLoad)
                }
                return
            }
            
            XCTAssertEqual(consumerId, "314159265")
            viewModel.inputs.shouldStartWebLoad(with: request) { shouldLoad in
                XCTAssertTrue(shouldLoad)
                expectation.fulfill()
            }
        }
        
        viewModel.inputs.viewDidLoad()
        wait(for: [expectation], timeout: 50.0)
    }

    func testExternalLinksShouldBeOpenedAtSafari() {
        let validExternalURLs = [URL(string: "https://www.picpay.com/?ppexternal=true")!,
                                 URL(string: "https://www.picpay.com/?ppexternal=1")!,
                                 URL(string: "https://www.picpay.me/?ppexternal=true")!,
                                 URL(string: "https://www.ppay.me/?ppexternal=true")!]

        validExternalURLs.forEach(testExternalLinkShouldBeOpenedAtSafari)
    }

    func testExternalLinkWithInvalidParameterDataShouldntBeOpennedAtSafari() {
        let url = URL(string: "https://www.picpay.com/?ppexternal=")!
        var action: WebViewAction?

        let outputs = WebViewModelOutput()
        outputs.didNextActionCalled = { action = $0 }

        viewModel = WebViewModel(withURL: URL(string: "https://www.picpay.com")!,
                                 sendHeaders: true,
                                 service: service,
                                 customHeaders: [:])
        viewModel.outputs = outputs

        viewModel.shouldStartWebLoad(with: URLRequest(url: url)) {
            XCTAssertFalse($0)
        }

        XCTAssertNil(action)
    }

    func testShouldAddScriptHandlerWhenViewDidLoad() {
        let outputs = WebViewModelOutput()

        viewModel = WebViewModel(withURL: URL(string: "https://www.picpay.com")!,
                                 sendHeaders: true,
                                 service: service,
                                 customHeaders: [:],
                                 scriptHandlers: [ScriptMessageHandler(messageName: "")])
        viewModel.outputs = outputs
        viewModel.viewDidLoad()

        XCTAssertTrue(outputs.didCallAddScriptHanlder)
    }
    
    // Obfuscation
    private let urlString = "https://gateway.service.ppay.me/password"
    private var serviceMock = WebViewServiceMock()
    private var webViewControllerSpy: WebViewControllerSpy?
    private var webViewSut: WebViewModel?
    
    func testOutputSetupObfuscation_WhenWebViewShouldObfuscate_ShouldCallSetupObfuscation() throws {
        let url = try XCTUnwrap(URL(string: urlString))
        webViewSut = WebViewModel(
            withURL: url,
            sendHeaders: true,
            service: serviceMock,
            shouldObfuscate: true,
            customHeaders: [:],
            scriptHandlers: []
        )
        webViewControllerSpy = WebViewControllerSpy()
        webViewSut?.outputs = webViewControllerSpy
        webViewSut?.viewWillAppear()
        
        XCTAssertEqual(self.webViewControllerSpy?.didCallSetupObfuscation, 1)
    }
    
    func testOutputDeinitObfuscation_WhenWebViewShouldObfuscate_ShouldCallDeinitObfuscation() throws {
        let url = try XCTUnwrap(URL(string: urlString))
        webViewSut = WebViewModel(
            withURL: url,
            sendHeaders: true,
            service: serviceMock,
            shouldObfuscate: true,
            customHeaders: [:],
            scriptHandlers: []
        )
        webViewControllerSpy = WebViewControllerSpy()
        webViewSut?.outputs = webViewControllerSpy
        webViewSut?.viewWillAppear()
        XCTAssertEqual(self.webViewControllerSpy?.didCallSetupObfuscation, 1)
        webViewSut?.viewDidDisappear()
        XCTAssertEqual(self.webViewControllerSpy?.didCallDeinitObfuscation, 1)
    }
    
    func testOutputSetupObfuscation_WhenWebViewShouldNotObfuscate_ShouldNotCallSetupObfuscation() throws {
        let url = try XCTUnwrap(URL(string: urlString))
        webViewSut = WebViewModel(
            withURL: url,
            sendHeaders: true,
            service: serviceMock,
            shouldObfuscate: false,
            customHeaders: [:],
            scriptHandlers: []
        )
        
        webViewControllerSpy = WebViewControllerSpy()
        webViewSut?.outputs = webViewControllerSpy
        webViewSut?.viewDidLoad()
        XCTAssertEqual(self.webViewControllerSpy?.didCallSetupObfuscation, 0)
    }
    
    func testOutputDeinitObfuscation_WhenWebViewShouldNotObfuscate_ShouldNotCallDeinitObfuscation() throws {
        let url = try XCTUnwrap(URL(string: urlString))
        webViewSut = WebViewModel(
            withURL: url,
            sendHeaders: true,
            service: serviceMock,
            shouldObfuscate: false,
            customHeaders: [:],
            scriptHandlers: []
        )
        
        webViewControllerSpy = WebViewControllerSpy()
        webViewSut?.outputs = webViewControllerSpy
        webViewSut?.viewDidDisappear()
        XCTAssertEqual(self.webViewControllerSpy?.didCallDeinitObfuscation, 0)
    }
    
    func testOutputSetupUIDelegate_WhenWebViewHasUIDelegate_ShouldCallSetupUIDelegate() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com/password"))
        webViewSut = WebViewModel(
            withURL: url,
            sendHeaders: true,
            service: serviceMock,
            shouldObfuscate: true,
            customHeaders: [:],
            scriptHandlers: []
        )
        webViewControllerSpy = WebViewControllerSpy()
        webViewSut?.outputs = webViewControllerSpy
        webViewSut?.viewDidLoad()
        
        XCTAssertEqual(self.webViewControllerSpy?.didCallSetupUIdelegate, 1)
    }
    
    func testOutputSetupUIDelegate_WhenWebViewHasUIDelegate_ShouldNoCallSetupUIDelegate() throws {
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        webViewSut = WebViewModel(
            withURL: url,
            sendHeaders: true,
            service: serviceMock,
            shouldObfuscate: true,
            customHeaders: [:],
            scriptHandlers: []
        )
        webViewControllerSpy = WebViewControllerSpy()
        webViewSut?.outputs = webViewControllerSpy
        webViewSut?.viewDidLoad()
        
        XCTAssertEqual(self.webViewControllerSpy?.didCallSetupUIdelegate, 0)
    }
}

private extension WebViewModelTest {
    private func testExternalLinkShouldBeOpenedAtSafari(url: URL) {
        var action: WebViewAction?

        let outputs = WebViewModelOutput()
        outputs.didNextActionCalled = { action = $0 }

        viewModel = WebViewModel(withURL: URL(string: "https://www.picpay.com")!,
                                 sendHeaders: true,
                                 service: service,
                                 customHeaders: [:])
        viewModel.outputs = outputs

        viewModel.shouldStartWebLoad(with: URLRequest(url: url)) {
            XCTAssertFalse($0, "Invalid decision for url: \(url.absoluteString)")
        }

        guard let receivedAction = action, case let .open(urlToOpen) = receivedAction else {
            XCTFail("Invalid WebView action for url: \(url.absoluteString)")
            return
        }

        XCTAssertTrue(urlToOpen == url, "Invalid url: \(url.absoluteString)")
    }
}

extension WebViewModelTest {
    class WebViewModelOutput: WebViewModelOutputs {
        func setupObfuscation() {
            //stub
        }
        
        func deinitObfuscation() {
            //stub
        }
        
        func setupUIDelegate() {
            //
        }
        
        var loadCalled: (URLRequest) -> Void = { _ in }
        var didNextActionCalled: (WebViewAction) -> Void = { _ in }
        var didCallAddScriptHanlder: Bool = false
        
        func load(request: URLRequest) {
            loadCalled(request)
        }
        
        func didNextAction(_ action: WebViewAction) {
            didNextActionCalled(action)
        }

        func addScriptHandler(scriptHandler: ScriptMessageHandler) {
            didCallAddScriptHanlder = true
        }
    }
}

// Obfuscation

private final class WebViewControllerSpy: UIViewController, WebViewModelOutputs {
    var didCallSetupObfuscation = 0
    var didCallDeinitObfuscation = 0
    var didCallSetupUIdelegate = 0
    func load(request: URLRequest) { }
    func didNextAction(_ action: WebViewAction) { }
    func addScriptHandler(scriptHandler: ScriptMessageHandler) { }
    
    func setupObfuscation() {
        didCallSetupObfuscation += 1
    }
    
    func deinitObfuscation() {
        didCallDeinitObfuscation += 1
    }
    
    func setupUIDelegate() {
        didCallSetupUIdelegate += 1
    }
}

