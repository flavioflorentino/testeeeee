@testable import PicPay
import UIKit

class MockNavigationController: UINavigationController {
    
    var pushedViewController: UIViewController?
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        pushedViewController = viewController
        super.pushViewController(viewController, animated: true)
    }
}

// Helper Functions
func defaultCashout(withAvailableWithdrawValue value: Double, availableFreeWithdrawal: Int = 2, withdrawalFee: Double = 10.90) -> CashoutValues {
    let availableOptions = [20.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0, 0.0]
    
    let defaultCashoutValues = CashoutValues(availableWithdrawalValue: value,
                                             withdrawalBalance: 1000.0,
                                             dailyWithdrawalLimit: 200.0,
                                             maxWithdrawalValue: 1000.0,
                                             minWithdrawalValue: 20.0,
                                             maxFreeWithdrawalsMonthly: 2,
                                             withdrawalFee: withdrawalFee,
                                             availableOptions: availableOptions,
                                             availableFreeWithdrawal: availableFreeWithdrawal,
                                             withdrawalExhibitionFee: 6.9)
    return defaultCashoutValues
}
