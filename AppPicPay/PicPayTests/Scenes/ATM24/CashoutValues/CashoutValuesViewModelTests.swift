import AnalyticsModule
import Core
import FeatureFlag
import XCTest

@testable import PicPay

final class CashoutValuesServiceMock: CashoutValuesServicing {
    var isSuccess = false
    var loadValuesWasCalled = false
    var balance: Double = 200.0
    var availableCashoutValue: Double = 100.0
    
    var availableFreeWithdrawal: Int = 2
    
    func loadCashoutValues(success: @escaping (CashoutValues) -> Void, failure: @escaping (Error) -> Void) {
        loadValuesWasCalled = true
        if isSuccess {
            success(CashoutValues(availableWithdrawalValue: availableCashoutValue, withdrawalBalance: balance, dailyWithdrawalLimit: 50.0, maxWithdrawalValue: 200.0, minWithdrawalValue: 20.0, maxFreeWithdrawalsMonthly: 2, withdrawalFee: 10.90, availableOptions: [20.0, 40.0, 50.0], availableFreeWithdrawal: availableFreeWithdrawal, withdrawalExhibitionFee: 6.9))
        } else {
            failure(NSError(domain: "ATM24", code: 999, userInfo: [NSLocalizedDescriptionKey: "Error loading cashout values"]))
        }
    }
}

class CashoutValuesViewModelOutputSpy: CashoutValuesViewModelOutputs {
    var didUpdateView = false
    var cashoutData = CashoutValuesPresenter(cashoutValues: [], availableBalance: 0.0, availableFreeWithdrawal: 2, withdrawalExhibitionFee: 6.9)
    var didShowError = false
    var cashoutError: Error?
    var didShowNextStep = false
    var didShowLoading = false
    var didShowScanner = false
    var selectedValue = 0.0
    var didShowOtherValue = false
    var limitInfoStatus: LimitErrorStatus?
    var didShowHelp = false
    var didShowAlert = false
    var didCallshowWithdrawTextInfo = false
    var showWithdrawAlert = false
    var didTrackDidAcceptFeeLimit = ATM24Events.popupFeeLimit
    var didTrackDidTapFooterFeeLimit = ATM24Events.footerFeeLimit
    var nextStepOtherValuesData: CashoutValues?
    var status: LimitErrorStatus?
    
    var didOpenFirstNotificationLimit = ATM24Events.cashoutFirstLimitNotification
    
    func showLoading(_ loading: Bool) {
        didShowLoading = true
    }
    
    func showErrorView(_ error: Error?) {
        didShowError = true
        cashoutError = error
    }
    
    func showNextStep(step: CashoutValuesAction) {
        didShowNextStep = true
        switch step {
        case .scanner(let selected, _):
            didShowScanner = true
            selectedValue = selected
        case .otherValues(let data):
            nextStepOtherValuesData = data
            didShowOtherValue = true
        default:
            break
        }
    }
    
    func updateView(with status: LimitErrorStatus?, data: CashoutValuesPresenter) {
        didUpdateView = true
        cashoutData = data
        limitInfoStatus = status
    }
    
    func showHelpInfo(withData data: CashoutValues) {
        didShowHelp = true
    }
    
    func showWithdrawAlert(withLimit limit: String) {
        didShowAlert = true
    }
    
    func showWithdrawTextInfo() {
        didCallshowWithdrawTextInfo = true
    }
    
    func showWithdrawAlert(withLimit limit: Int) {
        showWithdrawAlert = true
    }
}

private final class CashoutValuesCoordinatorSpy : CashoutValuesCoordinating {
    var viewController: UIViewController?
    private(set) var action: CashoutValuesAction?
    
    func perform(action: CashoutValuesAction) {
        self.action = action
    }
}

final class CashoutValuesViewModelTests: XCTestCase {
    private let analyticsMock = AnalyticsSpy()
    private let kvStoreMock = KVStoreMock()
    private let featureManagerMock = FeatureManagerMock()
    
    var sut: CashoutValuesViewModel!
    var mock: CashoutValuesServiceMock!
    var spy: CashoutValuesViewModelOutputSpy!
    fileprivate var coordinator: CashoutValuesCoordinatorSpy!
    
    override func setUp() {
        super.setUp()
        let dependencies = DependencyContainerMock(analyticsMock, kvStoreMock, featureManagerMock)
        coordinator = CashoutValuesCoordinatorSpy()
        
        mock = CashoutValuesServiceMock()
        spy = CashoutValuesViewModelOutputSpy()
        sut = CashoutValuesViewModel(service: mock, dependencies: dependencies, coordinator: coordinator)
        sut.outputs = spy
    }
    
    override func tearDown() {
        spy = nil
        mock = nil
        sut = nil
        super.tearDown()
    }
    
    private func loadValues(_ success: Bool) {
        mock.isSuccess = success
        sut.loadCashoutValues()
        XCTAssertTrue(mock.loadValuesWasCalled)
    }
    
    func testLoadingCashoutValuesWithSuccessOutputsTheResult() {
        loadValues(true)
        
        XCTAssertTrue(spy.didUpdateView)
        XCTAssertEqual(spy.cashoutData.cashoutValues, [20.0, 40.0, 50.0])
        XCTAssertEqual(spy.cashoutData.availableBalance, 100.0)
    }
    
    func testLoadingCashoutValuesWithFailureOutputsError() {
        loadValues(false)
        
        XCTAssertTrue(spy.didShowError)
        XCTAssertEqual(spy.cashoutError?.localizedDescription, "Error loading cashout values")
    }
    
    func testLoadingCashoutValuesOutputsLoadingState() {
        loadValues(true)
        
        XCTAssertTrue(spy.didShowLoading)
    }
    
    func testSelectCashoutValueBiggerThanZeroOutputsScanner() {
        loadValues(true)
        
        sut.selectCashoutValue(spy.cashoutData.cashoutValues[0])
        
        XCTAssertTrue(spy.didShowNextStep)
        XCTAssertTrue(spy.didShowScanner)
        XCTAssertEqual(spy.selectedValue, spy.cashoutData.cashoutValues[0])
    }
    
    func testSelectCashoutValueEqualToZeroOutputsOtherValueStep() {
        loadValues(true)
        
        sut.selectCashoutValue(0.0)
        XCTAssertTrue(spy.didShowNextStep)
        XCTAssertTrue(spy.didShowOtherValue)
    }
    
    func testShouldLoadCashoutValuesWhenNeeded() {
        sut.loadCashoutValuesIfNeeded()
        
        XCTAssertTrue(mock.loadValuesWasCalled)
        XCTAssertTrue(spy.didShowLoading)
    }
    
    func testShouldShowDailyLimitStatusWhenReachDailyCashoutLimit() {
        mock.availableCashoutValue = 10.0
        mock.isSuccess = true
        sut.loadCashoutValues()
        
        XCTAssertNotNil(spy.limitInfoStatus)
    }
    
    func testShouldShowNoLimitStatusWhenReachBalanceLimit() {
        mock.availableCashoutValue = 10.0
        mock.balance = 10.0
        mock.isSuccess = true
        sut.loadCashoutValues()
        
        XCTAssertNotNil(spy.limitInfoStatus)
    }
    
    func testDidAskForHelpShouldShowHelp() {
        mock.isSuccess = true
        sut.loadCashoutValues()
        sut.loadHelpInfo()
        
        XCTAssertTrue(spy.didShowHelp)
    }
    
    
    func testShouldShowWithdrawTextInfo_WhenCalledAndFeatureFlagFalse_ShouldCallShowWithdrawTextInfoInOutput() {
        featureManagerMock.override(key: .featureShowWithdrawFeeEnabled, with: false)
        sut.shouldShowWithdrawTextInfo()
        XCTAssertTrue(spy.didCallshowWithdrawTextInfo)
    }
    
    func testShouldShowWithdrawTextInfo_WhenCalledAndFeatureFlagTrue_ShouldNotCallShowWithdrawTextInfoInOutput() {
        featureManagerMock.override(key: .featureShowWithdrawFeeEnabled, with: true)
        sut.shouldShowWithdrawTextInfo()
        XCTAssertFalse(spy.didCallshowWithdrawTextInfo)
    }
    
    func testShouldTrackDidAcceptFeeLimit_WhenAnalyticsIsTriggered_ShouldTriggeredAnalytics() {
        sut.trackDidAcceptFeeLimit()
        XCTAssertTrue(analyticsMock.equals(to: ATM24Events.popupFeeLimit.event()))
    }
    
    func testShouldTrackDidTapFooterFeeLimit_WhenAnalyticsisTriggered_ShouldTriggeredAnalytics() {
        sut.trackDidTapFooterFeeLimit()
        XCTAssertTrue(analyticsMock.equals(to: ATM24Events.footerFeeLimit.event()))
    }
    
    func testShouldShowWithdrawalLimitExceeded_WhenAvaialableWuthdrawalFeeIsZero_AndShowWithdrawalFeeIsEnabled() {
        spy.didUpdateView = false
        
        featureManagerMock.override(key: .featureShowWithdrawFeeEnabled, with: true)
        mock.availableFreeWithdrawal = 0
        mock.balance = 100
        loadValues(true)
        sut.selectCashoutValue(0.0)
        
        XCTAssertTrue(spy.didUpdateView)
        XCTAssertEqual(spy.cashoutData.availableFreeWithdrawal, 0)
        XCTAssertEqual(spy.cashoutData.availableBalance, 100)
        XCTAssertEqual(spy.cashoutData.withdrawalExhibitionFee, 6.9)
        
        switch spy.limitInfoStatus {
        case .exceededFreeFeeLimit(let maxFreeWithdrawalsMonthly, let withdrawalExhibitionFee):
            XCTAssertEqual(maxFreeWithdrawalsMonthly, "2")
            XCTAssertEqual(withdrawalExhibitionFee, 6.9.stringAmount)
        default:
            XCTFail("Expected other type of status")
        }
        
    }
    
    func testLimitInfoStatusShouldBeNil_WhenShowWithdrawalFeeIsDisabled() {
        spy.limitInfoStatus = nil
        
        featureManagerMock.override(key: .featureShowWithdrawFeeEnabled, with: false)
        mock.availableFreeWithdrawal = 0
        loadValues(true)
        sut.selectCashoutValue(20.0)
        
        XCTAssertNil(spy.limitInfoStatus)
    }
    
    func setupSelectCashoutTestWith(value: Double) {
        spy.didUpdateView = false
        spy.didShowScanner = false
        spy.didShowNextStep = false
        
        mock.availableFreeWithdrawal = 1
        loadValues(true)
        sut.selectCashoutValue(value)
    }
    
    func testShouldGoToNextStepOthervalues_WhenAvaialableWuthdrawalFeeIsNotZero_AndSelectedValueIsZero() {
        setupSelectCashoutTestWith(value: 0.0)
        
        XCTAssertTrue(spy.didUpdateView)
        XCTAssertTrue(spy.didShowNextStep)
        XCTAssertTrue(spy.didShowOtherValue)
    }
    
    func testShouldGoToNextStepOthervalues_WhenAvaialableWuthdrawalFeeIsNotZero_AndSelectedValueIsNotZero() {
        setupSelectCashoutTestWith(value: 20.0)
        
        XCTAssertTrue(spy.didUpdateView)
        XCTAssertTrue(spy.didShowNextStep)
        XCTAssertTrue(spy.didShowScanner)
        XCTAssertEqual(spy.selectedValue, 20.0)
    }
    
    func prepareViewModelToTestNextFunc(selectedValue: Double) {
        // Since value is a private var, we will have to call selectCashoutValue() so it can fill the value.
        featureManagerMock.override(key: .featureShowWithdrawFeeEnabled, with: true)
        mock.availableFreeWithdrawal = 0
        loadValues(true)
        let cashoutData = spy.cashoutData
        sut.selectCashoutValue(selectedValue)
        
        // Change back to the original values
        spy.didUpdateView = false
        spy.cashoutData = cashoutData
        spy.limitInfoStatus = nil
    }
    
    func testShouldCallNextStepOtherValues_WhenSelectedValueIsZero_AndNextIsCalled() {
        prepareViewModelToTestNextFunc(selectedValue: 0.0)
        spy.didShowOtherValue = false
        
        sut.next()
        
        XCTAssertTrue(spy.didShowOtherValue)
    }
    
    func testShouldCallCoordinatorScanner_WhenSelectedValueIsZero_AndNextIsCalled() {
        prepareViewModelToTestNextFunc(selectedValue: 20.0)
        
        sut.next()
        
        guard let action = try? XCTUnwrap(coordinator.action) else {
            XCTFail("Should have an action here")
            return
        }
        
        switch action {
        case .scanner(let selectedValue, _):
            XCTAssertEqual(selectedValue, 20.0)
        default:
            XCTFail("Should have returned a Scanner")
        }
    }
    
    func testShouldTrackDidTapProceedButton_WhenFreeFeeIsExceeded_ShouldTriggeredAnalytics() {
        sut.trackTapContinueButtonWhenLimiteExceeded()
        XCTAssertTrue(analyticsMock.equals(to: ATM24Events.tapContinueLimiteExceeded.event()))
    }
    
    func testShouldTrackDidTapBackToHome_WhenFreeFeeIsExceeded_ShouldTriggeredAnalytics() {
        sut.trackBackToHomeWhenLimiteExceeded()
        XCTAssertTrue(analyticsMock.equals(to: ATM24Events.tapBackToHomeLimiteExceeded.event()))
    }
    
    func testShouldTrackDidTapBackButton_WhenFreeFeeIsExceeded_ShouldTriggeredAnalytics() {
        sut.trackTapBackButtonWhenLimiteExceeded()
        if case .exceededFreeFeeLimit = spy.status {
            XCTAssertTrue(analyticsMock.equals(to: ATM24Events.tapBackLimiteExceeded.event()))
        }
    }
    
    func testShouldTrackOpenFirstNotificationLimit_WhenAnalyticsisTriggered_ShouldTriggeredAnalytics() {
        sut.trackCashoutFirstLimitNotification()
        XCTAssertTrue(analyticsMock.equals(to: spy.didOpenFirstNotificationLimit.event()))
    }
}
