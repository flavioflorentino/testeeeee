import XCTest
@testable import PicPay

private class CashoutConfirmationCoordinatingSpy: CashoutConfirmationCoordinating {
    var viewController: UIViewController?

    // MARK: - Perform
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocation: CashoutConfirmationAction?

    func perform(action: CashoutConfirmationAction) {
        performActionCallsCount += 1
        performActionReceivedInvocation = action
    }
}

private class CashoutConfirmationDisplaySpy: CashoutConfirmationDisplay {

    // MARK: - UpdateView
    private(set) var updateViewWithDataCallsCount = 0
    private(set) var updateViewWithDataReceivedInvocation: CashoutDataInteractor?

    func updateView(withData data: CashoutDataInteractor) {
        updateViewWithDataCallsCount += 1
        updateViewWithDataReceivedInvocation = data
    }

    // MARK: - ShowLoading
    private(set) var showLoadingCallsCount = 0
    private(set) var showLoadingReceivedInvocation: Bool?

    func showLoading(_ loading: Bool) {
        showLoadingCallsCount += 1
        showLoadingReceivedInvocation = loading
    }

    // MARK: - ShowError
    private(set) var showErrorTitleMessageCallsCount = 0
    private(set) var showErrorTitleMessageReceivedInvocation: (title: String, message: String)?

    func showError(title: String, message: String) {
        showErrorTitleMessageCallsCount += 1
        showErrorTitleMessageReceivedInvocation = (title: title, message: message)
    }

    // MARK: - ShowWithdrawSuccess
    private(set) var showWithdrawSuccessCallsCount = 0

    func showWithdrawSuccess() {
        showWithdrawSuccessCallsCount += 1
    }

    // MARK: - ShowWithdrawSuccessWithAlert
    private(set) var showWithdrawSuccessWithAlertSubtitleIconCallsCount = 0
    private(set) var showWithdrawSuccessWithAlertSubtitleIconReceivedInvocation: (subtitle: String, icon: UIImage)?

    func showWithdrawSuccessWithAlert(subtitle: String, icon: UIImage) {
        showWithdrawSuccessWithAlertSubtitleIconCallsCount += 1
        showWithdrawSuccessWithAlertSubtitleIconReceivedInvocation = (subtitle: subtitle, icon: icon)
    }

    // MARK: - ShowDialogError
    private(set) var showDialogErrorErrorCallsCount = 0
    private(set) var showDialogErrorErrorReceivedInvocation: ATM24CodeError?

    func showDialogError(error: ATM24CodeError) {
        showDialogErrorErrorCallsCount += 1
        showDialogErrorErrorReceivedInvocation = error
    }

    // MARK: - ShowScreenError
    private(set) var showScreenErrorErrorCallsCount = 0
    private(set) var showScreenErrorErrorReceivedInvocation: ATM24CodeError?

    func showScreenError(error: ATM24CodeError) {
        showScreenErrorErrorCallsCount += 1
        showScreenErrorErrorReceivedInvocation = error
    }
}

final class ATM24ErrorStub: ATM24CodeError {
    var id: String
    var title: String
    var message: String
    var presentationType: ATM24ErrorPresentationType
    var icon: UIImage
    var primaryButtonTitle: String
    var secondaryButtonTitle: String?

    init(id: String,
         title: String,
         message: String,
         presentationType: ATM24ErrorPresentationType,
         icon: UIImage,
         primaryButtonTitle: String,
         secondaryButtonTitle: String? = nil,
         primaryActionReturn: CashoutConfirmationAction,
         secondaryActionReturn: CashoutConfirmationAction?) {
        self.id = id
        self.title = title
        self.message = message
        self.presentationType = presentationType
        self.icon = icon
        self.primaryButtonTitle = primaryButtonTitle
        self.secondaryButtonTitle = secondaryButtonTitle
        self.primaryActionReturn = primaryActionReturn
        self.secondaryActionReturn = secondaryActionReturn
    }

    var primaryActionReturn: CashoutConfirmationAction
    func primaryAction() -> CashoutConfirmationAction {
        primaryActionReturn
    }

    var secondaryActionReturn: CashoutConfirmationAction?
    func secondaryAction() -> CashoutConfirmationAction? {
        secondaryActionReturn
    }
}

final class CashoutConfirmationPresenterTests: XCTestCase {
    private let coordinatorSpy = CashoutConfirmationCoordinatingSpy()
    private let viewControllerSpy = CashoutConfirmationDisplaySpy()
    
    private lazy var sut: CashoutConfirmationPresenting = {
        let presenter = CashoutConfirmationPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()

    func testShowError_WhenPresentationTypeIsDialog_ShouldCallShowDialogError() {
        let dialogError = ATM24ErrorStub(
            id: "teste_id",
            title: "teste_title",
            message: "teste_message",
            presentationType: .dialog,
            icon: UIImage(),
            primaryButtonTitle: "teste_primaryButtonTitle",
            secondaryButtonTitle: "teste_secondaryButtonTitle",
            primaryActionReturn: .cancel,
            secondaryActionReturn: nil
        )

        sut.show(error: dialogError)

        XCTAssertEqual(viewControllerSpy.showScreenErrorErrorCallsCount, 0)
        XCTAssertEqual(viewControllerSpy.showDialogErrorErrorCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.showDialogErrorErrorReceivedInvocation?.id ?? "",  dialogError.id)
    }

    func testShowError_WhenPresentationTypeIsScreen_ShouldCallShowDialogError() {
        let dialogError = ATM24ErrorStub(
            id: "teste_id",
            title: "teste_title",
            message: "teste_message",
            presentationType: .screen,
            icon: UIImage(),
            primaryButtonTitle: "teste_primaryButtonTitle",
            secondaryButtonTitle: "teste_secondaryButtonTitle",
            primaryActionReturn: .cancel,
            secondaryActionReturn: nil
        )

        sut.show(error: dialogError)

        XCTAssertEqual(viewControllerSpy.showDialogErrorErrorCallsCount, 0)
        XCTAssertEqual(viewControllerSpy.showScreenErrorErrorCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.showScreenErrorErrorReceivedInvocation?.id ?? "",  dialogError.id)
    }
    
    func testUpdateView_WhenCashoutDataIsCorrect_ShouldUpdateView() throws {
        let data = CashoutDataInteractor(
            selectedValue: 100.0,
            withdrawalFee: 10.0,
            selectedValueAttributedText: configureCashoutLabelAttributes(withValue: 100.0)
        )
        
        sut.updateView(selectedValue: data.selectedValue, withdrawFee: data.withdrawalFee)
        
        XCTAssertEqual(viewControllerSpy.updateViewWithDataCallsCount, 1)
        XCTAssertEqual(try XCTUnwrap(viewControllerSpy.updateViewWithDataReceivedInvocation), data)
    }
    
    func testShowLoading_WhenLoadingIsTrue_ShouldShowLoading() {
        sut.showLoading(true)
        
        XCTAssertEqual(viewControllerSpy.showLoadingCallsCount, 1)
        XCTAssertTrue(viewControllerSpy.showLoadingReceivedInvocation ?? false)
    }
    
    func testShowLoading_WhenLoadingIsFalse_ShouldHideLoading() {
        sut.showLoading(false)
        
        XCTAssertEqual(viewControllerSpy.showLoadingCallsCount, 1)
        XCTAssertFalse(viewControllerSpy.showLoadingReceivedInvocation ?? true)
    }
    
    func testeShowWithdrawSuccess_ShouldShowWithdrawSuccess() {
        sut.showWithdrawSuccess()
        
        XCTAssertEqual(viewControllerSpy.showWithdrawSuccessCallsCount, 1)
    }
    
    func testShowLastFeeWithdrawSuccessWithAlert_WhenUserHasOneRemainWithdraw_ShoulShowLastWithdrawAlert() throws {
        sut.showLastFeeWithdrawSuccessWithAlert()
        
        XCTAssertEqual(viewControllerSpy.showWithdrawSuccessWithAlertSubtitleIconCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.showWithdrawSuccessWithAlertSubtitleIconReceivedInvocation?.subtitle ?? "", Strings.Atm24.lastWithdrawl)
        XCTAssertEqual(try XCTUnwrap(viewControllerSpy.showWithdrawSuccessWithAlertSubtitleIconReceivedInvocation?.icon), Assets.WithdrawIcons.iconWithdrawSadMoney.image)
    }
    
    func testShowLastFeeWithdrawSuccessWithAlert_WhenUserHasTwoRemainWithdraw_ShoulShowSecondToLastWithdrawAlert() throws {
        sut.showSecondToLastFeeWithdrawSuccessWithAlert()
        
        XCTAssertEqual(viewControllerSpy.showWithdrawSuccessWithAlertSubtitleIconCallsCount, 1)
        XCTAssertEqual(viewControllerSpy.showWithdrawSuccessWithAlertSubtitleIconReceivedInvocation?.subtitle ?? "", Strings.Atm24.secondToLast)
        XCTAssertEqual(try XCTUnwrap(viewControllerSpy.showWithdrawSuccessWithAlertSubtitleIconReceivedInvocation?.icon), Assets.WithdrawIcons.iconWithdrawSmile.image)
    }
    
    func testPerformAction_ShouldShowHelpAction() throws {
        let cashout = defaultCashout(withAvailableWithdrawValue: 1000.0)

        sut.perform(action: .help(cashout))
        
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(try XCTUnwrap(coordinatorSpy.performActionReceivedInvocation), .help(cashout))
    }
    
    func testPerformAction_ShouldShowCancelAction() throws {
        sut.perform(action: .cancel)
        
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(try XCTUnwrap(coordinatorSpy.performActionReceivedInvocation), .cancel)
    }
    
    func testPerformAction_ShouldShowFinishAction() throws {
        sut.perform(action: .finish)
        
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(try XCTUnwrap(coordinatorSpy.performActionReceivedInvocation), .finish)
    }
    
    private func configureCashoutLabelAttributes(withValue value: Double) -> NSAttributedString {
        let symbol = Strings.Atm24.currencySymbol
        let completeText = "\(value.stringAmount)"
        let valueString = value.stringAmount.replacingOccurrences(of: symbol, with: "").replacingOccurrences(of: " ", with: "")
        
        let rangeOfValue = (completeText as NSString).range(of: valueString)
        let rangeOfSymbol = (completeText as NSString).range(of: symbol)
        
        let attributedString = NSMutableAttributedString(string: completeText)
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 52), range: rangeOfValue)
        attributedString.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: 22), range: rangeOfSymbol)
        attributedString.addAttribute(.baselineOffset, value: NSNumber(20), range: rangeOfSymbol)
        return attributedString
    }
}
