@testable import PicPay
import AnalyticsModule
import FeatureFlag
import XCTest

enum CashoutConfirmationError: Error {
    case generic
}

private final class CashoutConfirmationServicingMock: CashoutConfirmationServicing {
    var authenticateUserCallsCount = 0
    var verifyTransactionCallsCount = 0
    var authorizeWithdrawCallsCount = 0
    var password: String?
    var authorizeWithdrawResult: Result<WithdrawAuthorization, Error> = .failure(CashoutConfirmationError.generic)
    var verifyTransactionStatusResult: Result<WithdrawTransactionStatus, Error> = .failure(CashoutConfirmationError.generic)
    
    func authenticateUser(completion: @escaping (String?) -> Void) {
        authenticateUserCallsCount += 1
        completion(password)
    }
    
    func authorizeWithdraw(
        withData data: AuthorizationInfoRequestData,
        completion: @escaping (Result<WithdrawAuthorization, Error>) -> Void
    ){
        authorizeWithdrawCallsCount += 1
        completion(authorizeWithdrawResult)
    }
    
    func verifyTransactionStatus(forTransactionId id: String, completion: @escaping (
        Result<WithdrawTransactionStatus,
               Error>
    ) -> Void) {
        verifyTransactionCallsCount += 1
        completion(verifyTransactionStatusResult)
    }
}

private class CashoutConfirmationPresentingSpy: CashoutConfirmationPresenting {
    var viewController: CashoutConfirmationDisplay?

    // MARK: - UpdateView
    private(set) var updateViewCallsCount = 0
    private(set) var updateViewSelectedValueWithdrawFeeReceivedInvocation: (selectedValue: Double, withdrawFee: Double)?

    func updateView(selectedValue: Double, withdrawFee: Double) {
        updateViewCallsCount += 1
        updateViewSelectedValueWithdrawFeeReceivedInvocation = (selectedValue: selectedValue, withdrawFee: withdrawFee)
    }

    // MARK: - ShowLoading
    private(set) var showLoadingCallsCount = 0
    private(set) var showLoadingReceivedInvocation: Bool?

    func showLoading(_ loading: Bool) {
        showLoadingCallsCount += 1
        showLoadingReceivedInvocation = loading
    }

    // MARK: - ShowPasswordError
    private(set) var showPasswordErrorCallsCount = 0
    private(set) var showPasswordErrorReceivedInvocation: AuthorizationError?

    func showPasswordError(_ errorInfo: AuthorizationError) {
        showPasswordErrorCallsCount += 1
        showPasswordErrorReceivedInvocation = errorInfo
    }

    // MARK: - ShowWithdrawSuccess
    private(set) var showWithdrawSuccessCallsCount = 0

    func showWithdrawSuccess() {
        showWithdrawSuccessCallsCount += 1
    }

    // MARK: - ShowLastFeeWithdrawSuccessWithAlert
    private(set) var showLastFeeWithdrawSuccessWithAlertCallsCount = 0

    func showLastFeeWithdrawSuccessWithAlert() {
        showLastFeeWithdrawSuccessWithAlertCallsCount += 1
    }

    // MARK: - ShowSecondToLastFeeWithdrawSuccessWithAlert
    private(set) var showSecondToLastFeeWithdrawSuccessWithAlertCallsCount = 0

    func showSecondToLastFeeWithdrawSuccessWithAlert() {
        showSecondToLastFeeWithdrawSuccessWithAlertCallsCount += 1
    }

    // MARK: - Perform
    private(set) var performActionCallsCount = 0
    private(set) var performActionReceivedInvocation: CashoutConfirmationAction?

    func perform(action: CashoutConfirmationAction) {
        performActionCallsCount += 1
        performActionReceivedInvocation = action
    }

    // MARK: - Show
    private(set) var showErrorCallsCount = 0
    private(set) var showErrorReceivedInvocation: ATM24CodeError?

    func show(error: ATM24CodeError) {
        showErrorCallsCount += 1
        showErrorReceivedInvocation = error
    }
}


final class CashoutConfirmationInteractorTests: XCTestCase {
    private let analyticsMock = AnalyticsSpy()
    private let featureManagerMock = FeatureManagerMock()
    private lazy var dependencies = DependencyContainerMock(analyticsMock, featureManagerMock)
    private let serviceMock = CashoutConfirmationServicingMock()
    private let presenterSpy = CashoutConfirmationPresentingSpy()
    
    private lazy var sut = CashoutConfirmationInteractor(
        dependencies: dependencies,
        service: serviceMock,
        presenter: presenterSpy,
        qrCode: "abc",
        selectedValue: 20.0,
        cashoutData: defaultCashout(withAvailableWithdrawValue: 1000.0)
    )
    
    func testViewDidLoad_WhenDataIsCorrect_ShouldUpdatesViewWithCashoutData() {
        sut.showDataToView()
        
        XCTAssertEqual(presenterSpy.updateViewCallsCount, 1)
        XCTAssertEqual(presenterSpy.updateViewSelectedValueWithdrawFeeReceivedInvocation?.selectedValue ?? 0, 20)
        XCTAssertEqual(presenterSpy.updateViewSelectedValueWithdrawFeeReceivedInvocation?.withdrawFee ?? 0, 10.90)
        XCTAssertTrue(analyticsMock.equals(to: ATM24Events.openConfirmationWithdrawScreen.event()))
    }
    
    func testConfirmWithdraw_WhenPasswordIsNil_ShouldNotAuthorizeWithdraw() {
        serviceMock.password = nil
        
        sut.confirmWithdraw()
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 0)
        XCTAssertEqual(serviceMock.authorizeWithdrawCallsCount, 0)
        XCTAssertTrue(analyticsMock.equals(to: ATM24Events.confirmWithdraw.event()))
    }
    
    func testConfirmWithdraw_WhenPasswordIsCorrectAndUserHasTwoRemainWithdraw_ShouldShowSecondToLastAlert() {
        let authorizationDumb = WithdrawAuthorization(transactionId: "id", withdrawValue: 10.0, withdrawalFee: 1.0)
        let transactionStatusDumb = WithdrawTransactionStatus(
            transactionId: "id",
            status: .confirmed,
            withdrawalValue: 10.0,
            withdrawalFee: 1.0
        )
        serviceMock.password = "1234"
        serviceMock.authorizeWithdrawResult = .success(authorizationDumb)
        serviceMock.verifyTransactionStatusResult = .success(transactionStatusDumb)
        featureManagerMock.override(key: .featureShowWithdrawFeeEnabled, with: true)
        
        sut.confirmWithdraw()
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.showSecondToLastFeeWithdrawSuccessWithAlertCallsCount, 1)
        XCTAssertTrue(
            analyticsMock.equals(
                to: ATM24Events.confirmWithdraw.event(),
                ATM24Events.openSecondToLastWithdrawTransactionAlert.event()
            )
        )
    }
    
    func testConfirmWithdraw_WhenPasswordIsCorrectAndUserHasThreeRemainWithdraw_ShouldNotShowSAlert() {
        let sut = CashoutConfirmationInteractor(
            dependencies: dependencies,
            service: serviceMock,
            presenter: presenterSpy,
            qrCode: "abc",
            selectedValue: 20.0,
            cashoutData: defaultCashout(withAvailableWithdrawValue: 1000.0, availableFreeWithdrawal: 3)
        )
        let authorizationDumb = WithdrawAuthorization(transactionId: "id", withdrawValue: 10.0, withdrawalFee: 1.0)
        let transactionStatusDumb = WithdrawTransactionStatus(
            transactionId: "id",
            status: .confirmed,
            withdrawalValue: 10.0,
            withdrawalFee: 1.0
        )
        serviceMock.password = "1234"
        serviceMock.authorizeWithdrawResult = .success(authorizationDumb)
        serviceMock.verifyTransactionStatusResult = .success(transactionStatusDumb)
        featureManagerMock.override(key: .featureShowWithdrawFeeEnabled, with: true)
        
        sut.confirmWithdraw()
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.showWithdrawSuccessCallsCount, 1)
        XCTAssertTrue(analyticsMock.equals(to: ATM24Events.confirmWithdraw.event()))
    }
    
    func testConfirmWithdraw_WhenPasswordIsCorrectAndUserHasOneRemainWithdraw_ShouldShowLastAlert() {
        let sut = CashoutConfirmationInteractor(
            dependencies: dependencies,
            service: serviceMock,
            presenter: presenterSpy,
            qrCode: "abc",
            selectedValue: 20.0,
            cashoutData: defaultCashout(withAvailableWithdrawValue: 1000.0, availableFreeWithdrawal: 1)
        )
        let authorizationDumb = WithdrawAuthorization(transactionId: "id", withdrawValue: 10.0, withdrawalFee: 1.0)
        let transactionStatusDumb = WithdrawTransactionStatus(
            transactionId: "id",
            status: .confirmed,
            withdrawalValue: 10.0,
            withdrawalFee: 1.0
        )
        serviceMock.password = "1234"
        serviceMock.authorizeWithdrawResult = .success(authorizationDumb)
        serviceMock.verifyTransactionStatusResult = .success(transactionStatusDumb)
        featureManagerMock.override(key: .featureShowWithdrawFeeEnabled, with: true)
        
        sut.confirmWithdraw()
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.showLastFeeWithdrawSuccessWithAlertCallsCount, 1)
        XCTAssertTrue(
            analyticsMock.equals(
                to: ATM24Events.confirmWithdraw.event(),
                ATM24Events.openLastWithdrawTransactionAlert.event()
            )
        )
    }
    
    func testConfirmWithDraw_WhenFeatureFlagIsFalse_ShouldNotShowAlert() {
        let authorizationDumb = WithdrawAuthorization(transactionId: "id", withdrawValue: 10.0, withdrawalFee: 1.0)
        let transactionStatusDumb = WithdrawTransactionStatus(
            transactionId: "id",
            status: .confirmed,
            withdrawalValue: 10.0,
            withdrawalFee: 1.0
        )
        serviceMock.password = "1234"
        serviceMock.authorizeWithdrawResult = .success(authorizationDumb)
        serviceMock.verifyTransactionStatusResult = .success(transactionStatusDumb)
        featureManagerMock.override(key: .featureShowWithdrawFeeEnabled, with: false)
        
        sut.confirmWithdraw()
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.showWithdrawSuccessCallsCount, 1)
        XCTAssertTrue(analyticsMock.equals(to: ATM24Events.confirmWithdraw.event()))
    }
    
    func testConfirmWithdraw_WhenStatusIsCanceled_ShouldNotAuthorizeWithdraw() {
        let authorizationDumb = WithdrawAuthorization(transactionId: "id", withdrawValue: 10.0, withdrawalFee: 1.0)
        let transactionStatusDumb = WithdrawTransactionStatus(
            transactionId: "id",
            status: .canceled,
            withdrawalValue: 10.0,
            withdrawalFee: 1.0
        )
        serviceMock.password = "1234"
        serviceMock.authorizeWithdrawResult = .success(authorizationDumb)
        serviceMock.verifyTransactionStatusResult = .success(transactionStatusDumb)
        featureManagerMock.override(key: .featureShowWithdrawFeeEnabled, with: false)
        
        sut.confirmWithdraw()
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.showErrorCallsCount, 1)
        XCTAssertTrue(presenterSpy.showErrorReceivedInvocation?.title.isNotEmpty ?? false)
        XCTAssertTrue(presenterSpy.showErrorReceivedInvocation?.message.isNotEmpty ?? false)
        XCTAssertTrue(analyticsMock.equals(to: ATM24Events.confirmWithdraw.event()))
    }
    
    func testConfirmWithdraw_WhenStatusIsPending_ShouldAuthorizeWithdraw() {
        let authorizationDumb = WithdrawAuthorization(transactionId: "id", withdrawValue: 10.0, withdrawalFee: 1.0)
        let transactionStatusDumb = WithdrawTransactionStatus(
            transactionId: "id",
            status: .pending,
            withdrawalValue: 10.0,
            withdrawalFee: 1.0
        )
        serviceMock.password = "1234"
        serviceMock.authorizeWithdrawResult = .success(authorizationDumb)
        serviceMock.verifyTransactionStatusResult = .success(transactionStatusDumb)
        featureManagerMock.override(key: .featureShowWithdrawFeeEnabled, with: false)
        
        sut.confirmWithdraw()
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 1)
        XCTAssertEqual(serviceMock.verifyTransactionCallsCount, 1)
        XCTAssertTrue(analyticsMock.equals(to: ATM24Events.confirmWithdraw.event()))
    }
    
    func testConfirmWithdraw_WhenTransactionStatusFails_ShouldShowError() {
        let authorizationDumb = WithdrawAuthorization(transactionId: "id", withdrawValue: 10.0, withdrawalFee: 1.0)
        serviceMock.password = "1234"
        serviceMock.authorizeWithdrawResult = .success(authorizationDumb)
        serviceMock.verifyTransactionStatusResult = .failure(CashoutConfirmationError.generic)
        featureManagerMock.override(key: .featureShowWithdrawFeeEnabled, with: true)
        
        sut.confirmWithdraw()
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.showErrorCallsCount, 1)

        XCTAssertTrue(presenterSpy.showErrorReceivedInvocation?.title.isNotEmpty ?? false)
        XCTAssertTrue(presenterSpy.showErrorReceivedInvocation?.message.isNotEmpty ?? false)
        XCTAssertTrue(analyticsMock.equals(to: ATM24Events.confirmWithdraw.event()))
    }
    
    func testConfirmWithdraw_WhenTransactionStatusFails_ShouldShowAuthorizationError() {
        let authorizationDumb = WithdrawAuthorization(transactionId: "id", withdrawValue: 10.0, withdrawalFee: 1.0)
        let authorizationError = AuthorizationError(code: "S24H-10014", title: "errorTitle", message: "errorMessage")
        serviceMock.password = "1234"
        serviceMock.authorizeWithdrawResult = .success(authorizationDumb)
        serviceMock.verifyTransactionStatusResult = .failure(authorizationError)
        featureManagerMock.override(key: .featureShowWithdrawFeeEnabled, with: true)
        
        sut.confirmWithdraw()
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.showErrorCallsCount, 1)

        XCTAssertTrue(presenterSpy.showErrorReceivedInvocation?.title.isNotEmpty ?? false)
        XCTAssertTrue(presenterSpy.showErrorReceivedInvocation?.message.isNotEmpty ?? false)
        XCTAssertTrue(
            analyticsMock.equals(
                to: ATM24Events.confirmWithdraw.event(),
                ATM24Events.authorizationError(message: "errorMessage", code: "S24H-10014").event()
            )
        )
    }
    
    func testConfirmWithdraw_WhenTransactionStatusFails_ShouldShowPasswordError() {
        let authorizationDumb = WithdrawAuthorization(transactionId: "id", withdrawValue: 10.0, withdrawalFee: 1.0)
        let authorizationError = AuthorizationError(code: "6003", title: "errorTitle", message: "errorMessage")
        serviceMock.password = "1234"
        serviceMock.authorizeWithdrawResult = .success(authorizationDumb)
        serviceMock.verifyTransactionStatusResult = .failure(authorizationError)
        featureManagerMock.override(key: .featureShowWithdrawFeeEnabled, with: true)
        
        sut.confirmWithdraw()
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.showErrorCallsCount, 1)
        XCTAssertTrue(try XCTUnwrap(presenterSpy.showErrorReceivedInvocation) is ATM24ResetPasswordError)
        XCTAssertTrue(
            analyticsMock.equals(
                to: ATM24Events.confirmWithdraw.event(),
                ATM24Events.authorizationError(message: "errorMessage", code: "6003").event()
            )
        )
    }
    
    func testConfirmWithdraw_WhenAuthorizationFails_ShouldShowError() {
        serviceMock.password = "1234"
        serviceMock.authorizeWithdrawResult = .failure(CashoutConfirmationError.generic)
        
        sut.confirmWithdraw()
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.showErrorCallsCount, 1)

        XCTAssertTrue(presenterSpy.showErrorReceivedInvocation?.title.isNotEmpty ?? false)
        XCTAssertTrue(presenterSpy.showErrorReceivedInvocation?.message.isNotEmpty ?? false)
        XCTAssertTrue(analyticsMock.equals(to: ATM24Events.confirmWithdraw.event()))
    }
    
    func testConfirmWithdraw_WhenAuthorizationFails_ShouldShowAuthorizationError() {
        let authorizationError = AuthorizationError(code: "S24H-10015", title: "errorTitle", message: "errorMessage")
        serviceMock.password = "1234"
        serviceMock.authorizeWithdrawResult = .failure(authorizationError)
        
        sut.confirmWithdraw()
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.showErrorCallsCount, 1)

        XCTAssertTrue(presenterSpy.showErrorReceivedInvocation?.title.isNotEmpty ?? false)
        XCTAssertTrue(presenterSpy.showErrorReceivedInvocation?.message.isNotEmpty ?? false)
        XCTAssertTrue(
            analyticsMock.equals(
                to: ATM24Events.confirmWithdraw.event(),
                ATM24Events.authorizationError(message: "errorMessage", code: "S24H-10015").event()
            )
        )
    }
    
    func testConfirmWithdraw_WhenAuthorizationFails_ShouldShowPasswordError() throws {
        let authorizationError = AuthorizationError(code: "6003", title: "errorTitle", message: "errorMessage")
        serviceMock.password = "1234"
        serviceMock.authorizeWithdrawResult = .failure(authorizationError)
        
        sut.confirmWithdraw()
        
        XCTAssertEqual(presenterSpy.showLoadingCallsCount, 2)
        XCTAssertEqual(presenterSpy.showErrorCallsCount, 1)
        XCTAssertTrue(try XCTUnwrap(presenterSpy.showErrorReceivedInvocation) is ATM24ResetPasswordError)
        XCTAssertTrue(
            analyticsMock.equals(
                to: ATM24Events.confirmWithdraw.event(),
                ATM24Events.authorizationError(message: "errorMessage", code: "6003").event()
            )
        )
    }
    
    func testLoadHelp_ShouldCallHelpAction() {
        let cashout = defaultCashout(withAvailableWithdrawValue: 1000.0)
        
        sut.loadHelp()
        
        XCTAssertEqual(presenterSpy.performActionReceivedInvocation, .help(cashout))
        XCTAssertEqual(presenterSpy.performActionCallsCount, 1)
    }
    
    func testAcceptedTransaction_WhenWithdrawalFeeIsGreaterThanZero_ShouldTrackEvent() {
        sut.acceptedTransaction()
        
        XCTAssertTrue(
            analyticsMock.equals(
                to: ATM24Events.confirmWithdrawTransaction(true).event()))
    }
    
    func testAcceptedTransaction_WhenWithdrawalFeeIsLessThanZero_ShouldTrackEvent() {
        let sut = CashoutConfirmationInteractor(
            dependencies: dependencies,
            service: serviceMock,
            presenter: presenterSpy,
            qrCode: "abc",
            selectedValue: 20.0,
            cashoutData: defaultCashout(withAvailableWithdrawValue: 1000.0, withdrawalFee: 0.0)
        )
        
        sut.acceptedTransaction()
        
        XCTAssertTrue(
            analyticsMock.equals(
                to: ATM24Events.confirmWithdrawTransaction(false).event()
            )
        )
    }
    
    func testCancelTransaction_WhenWithdrawalFeeIsGreaterThanZero_ShouldTrackEventAndPerformCancelAction() {
        sut.cancelTransaction()
        
        XCTAssertEqual(presenterSpy.performActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.performActionReceivedInvocation, .cancel)
        XCTAssertTrue(
            analyticsMock.equals(
                to: ATM24Events.cancelWithdrawTransaction(true).event()
            )
        )
    }
    
    func testCancelTransaction_WhenWithdrawalFeeIsLessThanZero_ShouldTrackEventAndPerformCancelAction() {
        let sut = CashoutConfirmationInteractor(
            dependencies: dependencies,
            service: serviceMock,
            presenter: presenterSpy,
            qrCode: "abc",
            selectedValue: 20.0,
            cashoutData: defaultCashout(withAvailableWithdrawValue: 1000.0, withdrawalFee: 0.0)
        )
        
        sut.cancelTransaction()
        
        XCTAssertEqual(presenterSpy.performActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.performActionReceivedInvocation, .cancel)
        XCTAssertTrue(
            analyticsMock.equals(
                to: ATM24Events.cancelWithdrawTransaction(false).event()
            )
        )
    }
    
    func testCancel_ShouldPerformCancelAction() {
        sut.cancel()
        
        XCTAssertEqual(presenterSpy.performActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.performActionReceivedInvocation, .cancel)
    }
    
    func testFinish_ShouldPerformFinishAction() {
        sut.finish()
        
        XCTAssertEqual(presenterSpy.performActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.performActionReceivedInvocation, .finish)
    }
    
    func testTrackAlert_WhenUserHasTwoRemainWithdraw_ShouldTrackEvent() {
        sut.trackAlert()
        
        XCTAssertTrue(
            analyticsMock.equals(
                to: ATM24Events.confirmSecondToLastWithdrawTransactionAlert.event()
            )
        )
    }
    
    func testTrackAlert_WhenUserHasOneRemainWithdraw_ShouldTrackEvent() {
        let sut = CashoutConfirmationInteractor(
            dependencies: dependencies,
            service: serviceMock,
            presenter: presenterSpy,
            qrCode: "abc",
            selectedValue: 20.0,
            cashoutData: defaultCashout(withAvailableWithdrawValue: 1000.0, availableFreeWithdrawal: 1)
        )
        
        sut.trackAlert()
        
        XCTAssertTrue(
            analyticsMock.equals(
                to: ATM24Events.confirmLastWithdrawTransactionAlert.event()
            )
        )
    }
    
    func testTrackAlert_WhenUserHasThreeRemainWithdraw_ShouldNotTrackEvent() {
        let sut = CashoutConfirmationInteractor(
            dependencies: dependencies,
            service: serviceMock,
            presenter: presenterSpy,
            qrCode: "abc",
            selectedValue: 20.0,
            cashoutData: defaultCashout(withAvailableWithdrawValue: 1000.0, availableFreeWithdrawal: 3)
        )
        
        sut.trackAlert()
        
        XCTAssertEqual(analyticsMock.events.count, 0)
    }
    
    func testVerifyTransactionStatus_WhenTransactionStatusFails_ShouldShowError() {
        sut.verifyTransactionStatus()
        
        XCTAssertEqual(presenterSpy.showErrorCallsCount, 1)
        XCTAssertTrue(presenterSpy.showErrorReceivedInvocation?.title.isNotEmpty ?? false)
        XCTAssertTrue(presenterSpy.showErrorReceivedInvocation?.message.isNotEmpty ?? false)
    }
}
