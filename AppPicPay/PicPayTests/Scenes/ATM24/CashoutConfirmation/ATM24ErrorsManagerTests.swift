import XCTest
@testable import PicPay

final class ATM24ErrorsManagerTests: XCTestCase {
    private lazy var testErrors: [ATM24CodeError] = []
    private lazy var  sut = ATM24ErrorsManager(errors: testErrors)


    func testAction_WhenErrorsExists_shouldGetTheRightError() {
        let aError = ATM24LegacyCreateCashoutMovementFailError()
        testErrors = [
            ATM24ResetPasswordError(),
            aError,
            ATM24OriginalCommunicationFailError()
        ]

        let error = sut.error(byId: aError.id)

        XCTAssertEqual(error.id, aError.id)
        XCTAssertEqual(error.title, aError.title)
        XCTAssertEqual(error.primaryAction(), aError.primaryAction())
    }

    func testAction_WhenErrorsDontExistsOrWrongID_shouldGetTheRightError() {
        testErrors = [
            ATM24ResetPasswordError(),
            ATM24OriginalCommunicationFailError(),
            ATM24OriginalCommunicationFailError()
        ]
        let genericError = ATM24GenericError()

        let error = sut.error(byId: "Dont exist")

        XCTAssertEqual(error.id, genericError.id)
        XCTAssertEqual(error.title, genericError.title)
        XCTAssertEqual(error.primaryAction(), genericError.primaryAction())
    }
}
