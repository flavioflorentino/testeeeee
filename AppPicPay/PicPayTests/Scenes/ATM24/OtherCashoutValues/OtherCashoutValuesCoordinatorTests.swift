@testable import PicPay
import XCTest

class OtherCashoutValuesCoordinatorTests: XCTestCase {
    var sut: OtherCashoutValuesCoordinator!

    override func setUp() {
        super.setUp()
        let testNavViewController = MockNavigationController(rootViewController: UIViewController())

        let window = UIWindow(frame: UIScreen.main.bounds)
        window.makeKeyAndVisible()
        window.rootViewController = testNavViewController
        _ = testNavViewController.view

        sut = OtherCashoutValuesCoordinator()
        sut.viewController = testNavViewController.topViewController
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func testPerformScannerActionShouldGoToScanner() {
        sut.perform(action: .scanner(selectedValue: 100.0, cashoutData: defaultCashout(withAvailableWithdrawValue: 1000.0)))

        let presentedVC = sut.viewController?.presentedViewController
        XCTAssertNotNil(presentedVC)
        XCTAssertTrue(presentedVC?.isKind(of: UINavigationController.self) ?? false)
        let navigation = presentedVC as? UINavigationController
        XCTAssertNotNil(navigation?.viewControllers.first)
        let topController = navigation?.viewControllers.first
        XCTAssertTrue(topController?.isKind(of: QRCodeScannerViewController.self) ?? false)
    }
}
