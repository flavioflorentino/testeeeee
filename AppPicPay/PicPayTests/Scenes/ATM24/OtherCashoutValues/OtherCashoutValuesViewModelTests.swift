@testable import PicPay
import XCTest

class OtherCashoutValuesViewModelTests: XCTestCase {
    var sut: OtherCashoutValuesViewModel!
    var mock: OtherCashoutValuesServiceMock!
    var spy: OtherCashoutValuesViewModelOutputSpy!

    override func setUp() {
        super.setUp()
        mock = OtherCashoutValuesServiceMock()
        spy = OtherCashoutValuesViewModelOutputSpy()
        sut = OtherCashoutValuesViewModel(service: mock, data: defaultCashout(withAvailableWithdrawValue: 100.0))
        sut.outputs = spy
    }

    override func tearDown() {
        spy = nil
        mock = nil
        sut = nil
        super.tearDown()
    }

    func testViewDidLoadShouldUpdateView() {
        let availableValue = 100.0
        sut.viewDidLoad()

        XCTAssertTrue(spy.didUpdateView)
        XCTAssertEqual(spy.availableValue, availableValue)
    }

    func testdidContinueShouldGoToNextStepWithSelectedValue() {
        let selectedValue = 250.0
        sut.didContinue(withSelectedValue: selectedValue)

        XCTAssertTrue(spy.didShowNextStep)
        XCTAssertEqual(spy.valueSelected, selectedValue)
    }

    func testDidAskForHelpShouldShowHelp() {
        sut.loadHelpInfo()

        XCTAssertTrue(spy.didShowHelp)
    }
}

class OtherCashoutValuesServiceMock: OtherCashoutValuesServicing {
}

class OtherCashoutValuesViewModelOutputSpy: OtherCashoutValuesViewModelOutputs {
    var didUpdateView = false
    var didShowNextStep = false
    var availableValue = 0.0
    var valueSelected = 0.0
    var didShowHelp = false

    func updateView(withData data: OtherCashoutValuesPresenter) {
        didUpdateView = true
        availableValue = data.availableWithdrawValue
    }

    func showNextStep(withCashoutData: CashoutValues, selectedValue: Double) {
        didShowNextStep = true
        valueSelected = selectedValue
    }

    func showHelpInfo(with data: CashoutValues) {
        didShowHelp = true
    }
}
