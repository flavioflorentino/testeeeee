@testable import PicPay
import XCTest

class OtherValueConfigViewViewModelTests: XCTestCase {
    var sut: OtherValueConfigViewViewModel!
    var spy: OtherValueConfigViewViewModelOutputSpy!

    override func setUp() {
        super.setUp()
        spy = OtherValueConfigViewViewModelOutputSpy()
        sut = OtherValueConfigViewViewModel()
        sut.outputs = spy
    }

    override func tearDown() {
        spy = nil
        sut = nil
        super.tearDown()
    }

    func testAdding10ToCashoutValueEnablesMinusButtonForCurrentValueOf100() {
        sut.configureValues(withAvailable: 1000)
        XCTAssertFalse(spy.viewStatus.minusButtonEnable)

        sut.configureViewState(addingValue: 10)

        XCTAssertTrue(spy.viewStatus.minusButtonEnable)
        XCTAssertTrue(spy.viewStatus.plusButtonEnabled)
        XCTAssertTrue(spy.viewStatus.plus50ButtonEnabled)
        XCTAssertTrue(spy.viewStatus.plus100ButtonEnabled)
        XCTAssertEqual(spy.viewStatus.currentCashoutValueAttributedString.string, "R$110")
    }

    func testAddingMinus10ToCashoutValueDisablesMinusButtonForCurrentValueOf110() {
        sut.configureValues(withAvailable: 1000)
        sut.configureViewState(addingValue: 10)
        XCTAssertTrue(spy.viewStatus.minusButtonEnable)

        sut.configureViewState(addingValue: -10)

        XCTAssertFalse(spy.viewStatus.minusButtonEnable)
        XCTAssertTrue(spy.viewStatus.plusButtonEnabled)
        XCTAssertTrue(spy.viewStatus.plus50ButtonEnabled)
        XCTAssertTrue(spy.viewStatus.plus100ButtonEnabled)
        XCTAssertEqual(spy.viewStatus.currentCashoutValueAttributedString.string, "R$100")
    }

    func testPlus50ButtonIsDisabledWhenAvailableValuePlus50BecomesBiggerThanAvailableValue() {
        sut.configureValues(withAvailable: 200)
        XCTAssertTrue(spy.viewStatus.plus50ButtonEnabled)
        sut.configureViewState(addingValue: 60)

        XCTAssertFalse(spy.viewStatus.plus50ButtonEnabled)
        XCTAssertFalse(spy.viewStatus.plus100ButtonEnabled)
        XCTAssertEqual(spy.viewStatus.currentCashoutValueAttributedString.string, "R$160")
    }

    func testPlus100ButtonIsDisabledWhenAvailableValuePlus100BecomesBiggerThanAvailableValue() {
        sut.configureValues(withAvailable: 300)
        XCTAssertTrue(spy.viewStatus.plus100ButtonEnabled)
        sut.configureViewState(addingValue: 110)

        XCTAssertTrue(spy.viewStatus.plus50ButtonEnabled)
        XCTAssertFalse(spy.viewStatus.plus100ButtonEnabled)
        XCTAssertEqual(spy.viewStatus.currentCashoutValueAttributedString.string, "R$210")
    }

    func testPlusButtonIsDisabledWhenAvailableValuePlus10BecomesBiggerThanAvailableValue() {
        sut.configureValues(withAvailable: 110)
        XCTAssertTrue(spy.viewStatus.plusButtonEnabled)

        sut.configureViewState(addingValue: 10)
        XCTAssertFalse(spy.viewStatus.plusButtonEnabled)
        XCTAssertEqual(spy.viewStatus.currentCashoutValueAttributedString.string, "R$110")
    }
}

class OtherValueConfigViewViewModelOutputSpy: OtherValueConfigViewViewModelOutputs {

    var viewStatus: OtherValueData!

    func updateViewState(withData data: OtherValueData) {
        viewStatus = data
    }
}
