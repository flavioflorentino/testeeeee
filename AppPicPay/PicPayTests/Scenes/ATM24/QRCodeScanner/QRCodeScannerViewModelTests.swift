@testable import PicPay
import PermissionsKit
import XCTest

class QRCodeScannerViewModelTests: XCTestCase {
    var sut: QRCodeScannerViewModel!
    var mock: QRCodeScannerServiceMock!
    var spy: QRCodeScannerViewModelOutputSpy!

    override func setUp() {
        super.setUp()
        mock = QRCodeScannerServiceMock()
        spy = QRCodeScannerViewModelOutputSpy()
        sut = QRCodeScannerViewModel(service: mock, data: defaultCashout(withAvailableWithdrawValue: 100.0), selectedValue: 40.0)
        sut.outputs = spy
    }

    override func tearDown() {
        mock = nil
        spy = nil
        sut = nil
        super.tearDown()
    }

    func testTryStartScannerInitsScannerIfPermissionIsGranted() {
        mock.hasPermission = true
        sut.tryStartScanner()

        XCTAssertTrue(spy.scannerDidStart)
    }

    func testTryStartScannerDontInitsScannerIfPermissionIsDenied() {
        mock.hasPermission = false
        sut.tryStartScanner()

        XCTAssertFalse(spy.scannerDidStart)
    }

    func testShouldProccedToConfirmationWhenReadQRCode() {
        let code = "Tecban QRCode"
        sut.onReadQRCode(scannedText: code)

        XCTAssertTrue(spy.didProccedToConfirmation)
        XCTAssertEqual(spy.qrCodeData, code)
    }
}

class QRCodeScannerServiceMock: QRCodeScannerServicing {
    var hasPermission = false
    func requestCameraPermission(completion: @escaping (PermissionStatus) -> Void) {
        completion(hasPermission ? .authorized : .denied)
    }
}

class QRCodeScannerViewModelOutputSpy: QRCodeScannerViewModelOutputs {
    var scannerDidStart = false
    var didProccedToConfirmation = false
    var qrCodeData = ""
    func didStartScanner() {
        scannerDidStart = true
    }

    func proccedToConfirmationStep(withQRCode qrCode: String, selectedValue: Double, cashoutData: CashoutValues) {
        didProccedToConfirmation = true
        qrCodeData = qrCode
    }
    
    func close() {
    }
}
