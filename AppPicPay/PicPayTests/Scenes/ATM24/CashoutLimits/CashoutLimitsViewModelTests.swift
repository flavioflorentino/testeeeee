@testable import PicPay
import XCTest

class CashoutLimitsViewModelTests: XCTestCase {
    var sut: CashoutLimitsViewModel!
    var spy: CashoutLimitsViewModelOutputSpy!
    
    override func setUp() {
        super.setUp()
        spy = CashoutLimitsViewModelOutputSpy()
        sut = CashoutLimitsViewModel(service: CashoutLimitsServiceMock(), cashoutData: defaultCashout(withAvailableWithdrawValue: 100.0))
        sut.outputs = spy
    }
    
    override func tearDown() {
        spy = nil
        sut = nil
        super.tearDown()
    }
    
    func testViewDidLoadUpdatesViewWithData() {
        sut.viewDidLoad()
        
        XCTAssertTrue(spy.didUpdateView)
        XCTAssertFalse(spy.didCallFeatureFlag)
    }
}

class CashoutLimitsServiceMock: CashoutLimitsServicing {
}

class CashoutLimitsViewModelOutputSpy: CashoutLimitsViewModelOutputs {
    var didUpdateView = false
    var didCallFeatureFlag = false
    
    func showWithdrawTextInfo() {
        didCallFeatureFlag = true
    }
    
    
    func updateView(withData data: CashoutLimitsDataPresenter) {
        didUpdateView = true
    }
}
