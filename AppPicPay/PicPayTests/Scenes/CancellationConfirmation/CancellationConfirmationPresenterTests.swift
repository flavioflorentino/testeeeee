import XCTest
@testable import PicPay

private final class ViewControllerSpy: CancellationConfirmationDisplay {
    var displayAskCancelationViewCount = 0
    var displayConfirmationCancelationCount = 0
    var displayErrorCount = 0
    var displayLoadStatusCount = 0
    var displayConfigureViewCount = 0
    
    func displayAskCancelationView(viewElements: CancellationConfirmationBaseView.ViewElements) {
        displayAskCancelationViewCount += 1
    }
    
    func displayConfirmationCancelation(viewElements: CancellationConfirmationBaseView.ViewElements) {
        displayConfirmationCancelationCount += 1
    }
    
    func displayError(error: CancellationConfirmationViewController.CancellationConfirmationError) {
        displayErrorCount += 1
    }
    
    func displayLoadStatus() {
        displayLoadStatusCount += 1
    }
    
    func displayConfigureView(viewSetup: CancellationConfirmationViewController.ViewSetup) {
          displayConfigureViewCount += 1
    }
}

private final class CancellationConfirmationCoordinatorSpy: CancellationConfirmationCoordinating{
    var viewController: UIViewController?
    var performCount = 0
    var action : CancellationConfirmationAction?
    
    func perform(action: CancellationConfirmationAction) {
        performCount += 1
        self.action = action
    }
}

private struct Seeds {
    static let dummyAskCancellationResponse = CancellationConfirmationPresenter.AskCancellationResponse(
        storeImage: nil,
        storeName: "Pic Pay",
        value: 100.00,
        date: nil
    )
    
    static let dummyConfirmationResponse = CancellationConfirmationPresenter.ConfirmationResponse(storeImage: nil)
}

final class CancellationConfirmationPresenterTests: XCTestCase {
    
    private var sut: CancellationConfirmationPresenter?
    private var coordinatorSpy: CancellationConfirmationCoordinatorSpy?
    private var viewControllerSpy: ViewControllerSpy?
    
    private func setupStubs(){
        coordinatorSpy = CancellationConfirmationCoordinatorSpy()
        viewControllerSpy = ViewControllerSpy()
        
        guard let viewControllerSpy = viewControllerSpy, let coordinatorSpy = coordinatorSpy else {
            XCTFail("Nil objects on setup")
            return
        }
        
        sut = CancellationConfirmationPresenter(coordinator: coordinatorSpy)
        sut?.viewController = viewControllerSpy
    }
    
    func testPresentAskCancelationViewWhenCalledShouldAskViewControllerToDisplayAskCancelationView(){
        setupStubs()
        
        sut?.presentAskCancelationView(response: Seeds.dummyAskCancellationResponse)
        
        XCTAssertEqual(viewControllerSpy?.displayAskCancelationViewCount, 1)
    }
    
    func testPresentConfirmationCancelationWhenCalledShouldAskViewControllerToDisplayConfirmationCancelation(){
        setupStubs()
        
        sut?.presentConfirmationCancelation(response: Seeds.dummyConfirmationResponse)
        
        XCTAssertEqual(viewControllerSpy?.displayConfirmationCancelationCount, 1)
    }
    
    func testPresentErrorWhenCalledShouldAskViewControllerToDisplayError(){
        setupStubs()
        
        sut?.presentError(message: "Some error message")
        
        XCTAssertEqual(viewControllerSpy?.displayErrorCount, 1)
    }
    
    func testPresentLoadStatusWhenCalledShouldAskViewControllerToDisplayLoadStatus(){
        setupStubs()
        
        sut?.presentLoadStatus()
        
        XCTAssertEqual(viewControllerSpy?.displayLoadStatusCount, 1)
    }
    
    func testPresentyConfigureViewWhenCalledShouldAskViewControllerToDisplayConfigureView(){
        setupStubs()
        
        sut?.presentConfigureView()
        
        XCTAssertEqual(viewControllerSpy?.displayConfigureViewCount, 1)
    }
    
    func testDidNextStepWhenCalledWithDismissShouldAskCoordinatorToActionDismiss(){
        setupStubs()
        let actionExpectation = CancellationConfirmationAction.dismiss
    
        sut?.didNextStep(action: .dismiss)
        
        XCTAssertEqual(coordinatorSpy?.performCount, 1)
        XCTAssertEqual(coordinatorSpy?.action, actionExpectation)
    }
}
