import XCTest
@testable import PicPay

private enum Seeds {
    static let rightUrl = URL(string: "picpay://picpay/paymentcancellation?uuid=SOMEUUID&sellerCode=SOMESELLERCODE&storeName=Nome%20do%20Estabelecimento&type=linx&date=2020-02-07T20:00:00.000Z&value=880.0&store_image=https://via.placeholder.com/150")
    
    static let urlWithNoUUID = URL(string: "picpay://picpay/paymentcancellation?sellerCode=SOMESELLERCODE&store_name=Nome%20do%20Estabelecimento&type=linx&date=2020-02-07T20:00:00.000Z&value=880.0&store_image=https://via.placeholder.com/150")
    
    static let urlWithNoSellerCode = URL(string: "picpay://picpay/paymentcancellation?uuid=SOMEUUID&store_name=Nome%20do%20Estabelecimento&type=linx&date=2020-02-07T20:00:00.000Z&value=880.0&store_image=https://via.placeholder.com/150")
    
    static let urlWithWrongValue = URL(string: "picpay://picpay/paymentcancellation?uuid=SOMEUUID&sellerCode=SOMESELLERCODE&store_name=Nome%20do%20Estabelecimento&type=linx&date=2020-02-07T20:00:00.000Z&value=FOO&store_image=https://via.placeholder.com/150")
}

final class CancellationConfirmationDeepLinkTests: XCTestCase {
    private var sut : CancellationConfirmationDeepLink?
    
    func testInitWhenPassCorrectURLShouldCreateAnInstace() {
        guard let url = Seeds.rightUrl else {
            XCTFail()
            return
        }
        
        sut = CancellationConfirmationDeepLink(url: url)
        
        XCTAssertNotNil(sut)
    }
    
    func testInitWhenPassNoUUIDInURLShouldNotCreateAnInstace() {
        guard let url = Seeds.urlWithNoUUID else {
            XCTFail()
            return
        }
        
        sut = CancellationConfirmationDeepLink(url: url)
        
        XCTAssertNil(sut)
    }
    
    func testInitWhenPassNoSellerCodeInURLShouldNotCreateAnInstace() {
        guard let url = Seeds.urlWithNoSellerCode else {
            XCTFail()
            return
        }
        
        sut = CancellationConfirmationDeepLink(url: url)
        
        XCTAssertNil(sut)
    }
    
    func testInitWhenPassNoNumericValueInURLShouldNotCreateAnInstace() {
        guard let url = Seeds.urlWithWrongValue else {
            XCTFail()
            return
        }
        
        sut = CancellationConfirmationDeepLink(url: url)
        
        XCTAssertNil(sut)
    }
}
