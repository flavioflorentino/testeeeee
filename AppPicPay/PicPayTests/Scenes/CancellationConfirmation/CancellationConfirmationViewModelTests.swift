import XCTest
import Core
@testable import PicPay


private final class CancellationConfirmationPresenterSpy: CancellationConfirmationPresenting {
    var viewController: CancellationConfirmationDisplay?
    
    private(set) var presentAskCancelationViewCount = 0
    private(set) var presentConfirmationCancelationCount = 0
    private(set) var presentErrorCount = 0
    private(set) var presentLoadStatusCount = 0
    private(set) var didNextStepCount = 0
    private(set) var presentConfigureViewCount = 0
    
    private(set) var action: CancellationConfirmationAction?
    
    func didNextStep(action: CancellationConfirmationAction) {
        didNextStepCount += 1
        self.action = action
    }
    
    func presentAskCancelationView(response: CancellationConfirmationPresenter.AskCancellationResponse) {
        presentAskCancelationViewCount += 1
    }
    
    func presentConfirmationCancelation(response: CancellationConfirmationPresenter.ConfirmationResponse) {
        presentConfirmationCancelationCount += 1
    }
    
    func presentError(message: String) {
        presentErrorCount += 1
    }
    
    func presentLoadStatus() {
        presentLoadStatusCount += 1
    }
    
    func presentConfigureView() {
        presentConfigureViewCount += 1
    }
}

private final class CancellationConfirmationServiceStub: CancellationConfirmationServicing{
    
    let result : Result<Void, ApiError>
    
    var responseToCancellationCount = 0
    var cancellation: CancellationConfirmationService.Cancellation?
    
    init(result: Result<Void, ApiError>){
        self.result = result
    }
    
    func responseToCancellation(cancellation: CancellationConfirmationService.Cancellation, completionHandler: ((Result<Void, ApiError>) -> Void)?) {
        
        self.cancellation = cancellation
        responseToCancellationCount += 1
        switch result {
        case .success:
            completionHandler?(.success)
        case .failure(let error):
            completionHandler?(.failure(error))
        }
    }
}


private struct Seeds {
    static let dummyOrder: CancellationOrder = CancellationOrder(
        uuid: "SOMEUUID",
        sellerCode: "somesellercode",
        storeName: "PicPayStore",
        value: 100.00,
        date: "2019-02-06T00:35:01.746Z",
        storeImage: nil,
        type: "someType"
    )
    
    static var dummyError = ApiError.connectionFailure
}

final class CancellationConfirmationViewModelTests: XCTestCase {

    private var sut: CancellationConfirmationViewModel?
    private var serviceStub: CancellationConfirmationServiceStub?
    private var presenterSpy: CancellationConfirmationPresenterSpy?
    
    private func setUpStubs(serviceStatus: Result<Void, ApiError>, order: CancellationOrder) {
        serviceStub = CancellationConfirmationServiceStub(result: serviceStatus)
        presenterSpy = CancellationConfirmationPresenterSpy()
        
        guard let serviceStub = serviceStub, let presenterSpy = presenterSpy else {
            XCTFail("Nil objects on setup")
            return
        }
        
        sut = CancellationConfirmationViewModel(service: serviceStub, presenter: presenterSpy, cancellationOrder: order)
    }
    
     func testStartFlow_WhenCalled_ShouldAskPresenterToPresentAskCancelationView(){

        setUpStubs(serviceStatus: .success, order: Seeds.dummyOrder)
        
        sut?.startFlow()
        
        XCTAssertEqual(presenterSpy?.presentAskCancelationViewCount, 1)
    }
    
    func testAuthorizeCancel_WhenCalled_ShouldAskPresenterToPresentLoadStatus(){
        setUpStubs(serviceStatus: .success, order: Seeds.dummyOrder)
        
        sut?.authorizeCancel()
        
        XCTAssertEqual(presenterSpy?.presentLoadStatusCount, 1)
    }
    
    func testAuthorizeCancel_WhenCalled_ShouldAskServiceToConfirmCancellation(){
        setUpStubs(serviceStatus: .success, order: Seeds.dummyOrder)
        
        sut?.authorizeCancel()
        
        XCTAssertEqual(serviceStub?.responseToCancellationCount, 1)
    }
    
    func testAuthorizeCancel_WhenCalled_ShouldAskServiceToConfirmCancellationWithAuthorizationTrue() throws {
        setUpStubs(serviceStatus: .success, order: Seeds.dummyOrder)
        
        sut?.authorizeCancel()
        
        let userAuthorization = try XCTUnwrap(serviceStub?.cancellation?.userAuthorization)
        XCTAssertTrue(userAuthorization)
    }
    
    func testAuthorizeCancel_WhenServicesFinishWithSucess_ShouldAskPresenterToPresentConfirmationCancelation(){
        setUpStubs(serviceStatus: .success, order: Seeds.dummyOrder)
        
        sut?.authorizeCancel()
        
        XCTAssertEqual(presenterSpy?.presentConfirmationCancelationCount, 1)
    }
    
    func testAuthorizeCancel_WhenServicesFinishWithError_ShouldAskPresenterToPresentError(){
        setUpStubs(serviceStatus: .failure(Seeds.dummyError), order: Seeds.dummyOrder)
        
        sut?.authorizeCancel()
        
        XCTAssertEqual(presenterSpy?.presentErrorCount, 1)
    }
    
    func testConfirmationFeedback_WhenCalled_ShouldAskPresenterNextStep(){
        setUpStubs(serviceStatus: .success, order: Seeds.dummyOrder)
        let expectedAction = CancellationConfirmationAction.dismiss
        
        sut?.confirmationFeedback()
        
        XCTAssertEqual(presenterSpy?.didNextStepCount, 1)
        XCTAssertEqual(presenterSpy?.action, expectedAction)
    }
    
    func testExit_WhenCalled_ShouldAskPresenterNextStep(){
        setUpStubs(serviceStatus: .success, order: Seeds.dummyOrder)
        let expectedAction = CancellationConfirmationAction.dismiss
        
        sut?.exit()
        
        XCTAssertEqual(presenterSpy?.didNextStepCount, 1)
        XCTAssertEqual(presenterSpy?.action, expectedAction)
    }
    
    func testExit_WhenCalledInAskCancellationState_ShouldCallServiceToConfirmUnauthorizeCancelWithAuthorizationFalse() throws {
        setUpStubs(serviceStatus: .success, order: Seeds.dummyOrder)
        
        sut?.exit()
        
        XCTAssertEqual(serviceStub?.responseToCancellationCount, 1)
        let userAuthorization = try XCTUnwrap(serviceStub?.cancellation?.userAuthorization)
        XCTAssertFalse(userAuthorization)
    }
    
    func testExit_WhenCalledAfterSucessFromService_ShouldNotCallServiceToConfirmUnauthorizeCancelAgain() throws {
        setUpStubs(serviceStatus: .success, order: Seeds.dummyOrder)
        
        sut?.authorizeCancel()
        sut?.exit()
        
        let userAuthorization = try XCTUnwrap(serviceStub?.cancellation?.userAuthorization)
        XCTAssertTrue(userAuthorization)
        XCTAssertEqual(serviceStub?.responseToCancellationCount, 1)
        
    }
    
    func testExit_WhenCalledAfterErrorFromService_ShouldCallServiceToConfirmUnauthorizeCancel() throws {
        setUpStubs(serviceStatus: .failure(Seeds.dummyError), order: Seeds.dummyOrder)
        
        sut?.authorizeCancel()
        sut?.exit()
        
        XCTAssertEqual(serviceStub?.responseToCancellationCount, 2)
        let userAuthorization = try XCTUnwrap(serviceStub?.cancellation?.userAuthorization)
        XCTAssertFalse(userAuthorization)
        
    }
    
    func testUnauthorizeCancel_WhenCalled_ShouldAskServiceToConfirmUnauthorizeCancel() {
        setUpStubs(serviceStatus: .success, order: Seeds.dummyOrder)
        
        sut?.unauthorizeCancel()
        
        XCTAssertEqual(serviceStub?.responseToCancellationCount, 1)
    }
    
    func testUnauthorizeCancel_WhenCalled_ShouldAskServiceToConfirmUnauthorizeCancelWithAuthorizationFalse() throws {
        setUpStubs(serviceStatus: .success, order: Seeds.dummyOrder)
        
        sut?.unauthorizeCancel()
        
        let userAuthorization = try XCTUnwrap(serviceStub?.cancellation?.userAuthorization)
        XCTAssertFalse(userAuthorization)
    }
}
