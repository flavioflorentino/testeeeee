import XCTest
@testable import PicPay

private final class NavigationControllerSpy: UINavigationController{
    
    var popViewControllerCalled = false
    
    override func popViewController(animated: Bool) -> UIViewController? {
        super.popViewController(animated: animated)
        popViewControllerCalled = true
        return nil
    }
}

private final class ViewControllerSpy: UIViewController{
    
    var dismissCalled = false
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        super.dismiss(animated: flag, completion: completion)
        dismissCalled = true
    }
}

final class CancellationConfirmationCoordinatorTests: XCTestCase {

   private var sut: CancellationConfirmationCoordinator?
   private var viewController = ViewControllerSpy()
   private var navigationControllerSpy = NavigationControllerSpy()
    
    private func setupStubs() {
        sut = CancellationConfirmationCoordinator()
        navigationControllerSpy = NavigationControllerSpy()
        navigationControllerSpy.pushViewController(viewController, animated: false)
        
        sut?.viewController = viewController
    }
    
    func testPerformWhenCalledWithDismissActionShouldDismissOrPopCurrentController(){
        setupStubs()
        
        sut?.perform(action: .dismiss)
        
        XCTAssertTrue(navigationControllerSpy.popViewControllerCalled || viewController.dismissCalled)
    }
}
