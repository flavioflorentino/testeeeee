@testable import PicPay
import XCTest

private final class BusinessTypeCoordinatingSpy: BusinessTypeCoordinating {
    var viewController: UIViewController?

    //MARK: - perform
    private(set) var performActionCallsCount = 0
    private(set) var openWebviewBusinessTypeProAction = 0
    private(set) var openWebviewBusinessTypeBusinessAction = 0

    func perform(action: BusinessTypeAction) {
        performActionCallsCount += 1
        
        switch action {
        case .openWebView(.pro):
            openWebviewBusinessTypeProAction += 1
        case .openWebView(.business):
            openWebviewBusinessTypeBusinessAction += 1
        }
    }
}

final class BusinessTypePresenterTests: XCTestCase {
    private let coordinatorSpy = BusinessTypeCoordinatingSpy()
    private lazy var sut = BusinessTypePresenter(coordinator: coordinatorSpy)
    
    func testOpenWebview_WhenBusinessTypeIsPro_ShouldPerformOpenWebviewPro()  {
        sut.openWebview(forBusinessType: .pro)
        
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.openWebviewBusinessTypeProAction, 1)
    }
    
    func testOpenWebview_WhenBusinessTypeIsBusiness_ShouldPerformOpenWebviewBusiness() {
        sut.openWebview(forBusinessType: .business)
        
        XCTAssertEqual(coordinatorSpy.performActionCallsCount, 1)
        XCTAssertEqual(coordinatorSpy.openWebviewBusinessTypeBusinessAction, 1)
    }
}
