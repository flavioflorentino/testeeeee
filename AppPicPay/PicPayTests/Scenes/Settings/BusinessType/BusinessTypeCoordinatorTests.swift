@testable import PicPay
import XCTest

final class BusinessTypeCoordinatorTests: XCTestCase {
    private let viewControllerMock = ViewControllerMock()
    private lazy var navControllerMock = NavigationControllerMock(rootViewController: viewControllerMock)
    
    private lazy var sut: BusinessTypeCoordinator = {
        let coordinator = BusinessTypeCoordinator()
        coordinator.viewController = navControllerMock.topViewController
        return coordinator
    }()
    
    func testPerform_WhenActionIsOpenWebview_ShouldStartWebViewController() throws {
        sut.perform(action: .openWebView(.pro))
        
        XCTAssertTrue(navControllerMock.isPushViewControllerCalled)
        XCTAssertTrue(navControllerMock.pushedViewController is WebViewController)
    }
}
