@testable import PicPay
import XCTest
import AnalyticsModule

private final class BusinessTypePresentingSpy: BusinessTypePresenting {
    //MARK: - openWebview
    private(set) var didOpenWebviewForBusinessTypeCallsCount = 0
    private(set) var didBusinessTypeAccountWebViewPro = 0
    private(set) var didBusinessTypeAccountWebViewBusiness = 0

    func openWebview(forBusinessType account: WebViewPro.Account) {
        didOpenWebviewForBusinessTypeCallsCount += 1
        
        switch account {
        case .business:
            didBusinessTypeAccountWebViewBusiness += 1
        case .pro:
            didBusinessTypeAccountWebViewPro += 1
        }
    }
}

final class BusinessTypeInteractorTests: XCTestCase {
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var presenterSpy = BusinessTypePresentingSpy()
    
    private lazy var sut = BusinessTypeInteractor(presenter: presenterSpy,
                                                  dependencies: DependencyContainerMock(analyticsSpy))

    func testTrackOpenView_WhenCalled_ShouldTrackProOfferViewed() {
        sut.trackOpenView()
        
        XCTAssertTrue(analyticsSpy.equals(to: ProEvent.offerViewed.event()))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testOpenProWebview_WhenCalled_ShouldTrackOfferInteractedWithLiberalAndPresentProWebview() {
        sut.openProWebview()
        
        XCTAssertTrue(analyticsSpy.equals(to: ProEvent.offerInteracted(option: .liberalProfessional).event()))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(presenterSpy.didOpenWebviewForBusinessTypeCallsCount, 1)
        XCTAssertEqual(presenterSpy.didBusinessTypeAccountWebViewPro, 1)
    }
    
    func testOpenBusinessWebview_WhenCalled_ShouldTrackOfferInteractedWithBusinessAndPresentBusinessWebview() {
        sut.openBusinessWebview()
        
        XCTAssertTrue(analyticsSpy.equals(to: ProEvent.offerInteracted(option: .business).event()))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(presenterSpy.didOpenWebviewForBusinessTypeCallsCount, 1)
        XCTAssertEqual(presenterSpy.didBusinessTypeAccountWebViewBusiness, 1)
    }
}
