import XCTest
@testable import PicPay

final class ResetPasswordCoordinatorTests: XCTestCase {
    private lazy var sut: ResetPasswordCoordinator = {
        let sut = ResetPasswordCoordinator()
        sut.viewController = spy
        return sut
    }()
    private let spy = ResetPasswordViewControllerMock()
    
    func testDismissWithoutNavigation() {
        sut.perform(action: .dismiss)
        XCTAssertTrue(spy.dismissViewControllerCalled)
    }
    
    func testDismissWithNavigation() {
        let navigation = ResetPasswordNavigationControllerMock(rootViewController: spy)
        sut.perform(action: .dismiss)
        XCTAssertTrue(navigation.popViewControllerCalled)
    }
}
