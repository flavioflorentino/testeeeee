import XCTest
@testable import PicPay

final class ResetPasswordViewControllerTests: XCTestCase {
    private lazy var sut = ResetPasswordViewController(viewModel: spy)
    private let spy = ResetPasswordViewModelMock()
    private let window = UIWindow(frame: CGRect(origin: .zero, size: CGSize(width: 375, height: 812)))
    
    override func setUp() {
        super.setUp()
        loadView()
    }
    
    func loadView() {
        window.addSubview(sut.view)
        RunLoop.main.run(until: Date())
    }

    func testFetchConsumerConfiguration() {
        XCTAssertTrue(spy.fetchConsumerConfigurationCalled)
    }
    
    func testCurrentPasswordBecomeFirstResponder() {
        XCTAssertTrue(sut.currentPasswordTextField.isFirstResponder)
    }
    
    func testChangePassword() {
        let currentPassword = "1234"
        let newPassword = "12345"
        let newPasswordConfirmation = "123456"
        
        sut.currentPasswordTextField.text = currentPassword
        sut.newPasswordTextField.text = newPassword
        sut.newPasswordConfirmationTextField.text = newPasswordConfirmation
        
        sut.saveButtonTapped(sut.saveButton)
        
        XCTAssertTrue(spy.changePasswordCalled)
        XCTAssertEqual(spy.currentPassword, currentPassword)
        XCTAssertEqual(spy.newPassword, newPassword)
        XCTAssertEqual(spy.newPasswordConfirmation, newPasswordConfirmation)
    }
    
    func testDisplayCurrentPasswordErrorMessage() {
        let errorMessage = "Test Message"
        sut.displayCurrentPassword(errorMessage: errorMessage)
        XCTAssertEqual(sut.currentPasswordTextField.errorMessage, errorMessage)
    }
    
    func testDisplayNewPasswordErrorMessage() {
        let errorMessage = "Test Message"
        sut.displayNewPassword(errorMessage: errorMessage)
        XCTAssertEqual(sut.newPasswordTextField.errorMessage, errorMessage)
    }
    
    func testDisplayNewPasswordConfirmationErrorMessage() {
        let errorMessage = "Test Message"
        sut.displayNewPasswordConfirmation(errorMessage: errorMessage)
        XCTAssertEqual(sut.newPasswordConfirmationTextField.errorMessage, errorMessage)
    }
    
    func testSetNumericTextFieldForPasswordTextFields() {
        sut.setNumericTextFieldForPasswordTextFields()
        XCTAssertEqual(sut.currentPasswordTextField.keyboardType, .numberPad)
        XCTAssertEqual(sut.newPasswordTextField.keyboardType, .numberPad)
        XCTAssertEqual(sut.newPasswordConfirmationTextField.keyboardType, .numberPad)
    }

    func testDisplayLoading() {
        sut.displayLoading()
        XCTAssertNotNil(sut.navigationItem.leftBarButtonItem)
        XCTAssertFalse(sut.currentPasswordTextField.isEnabled)
        XCTAssertFalse(sut.newPasswordTextField.isEnabled)
        XCTAssertFalse(sut.newPasswordConfirmationTextField.isEnabled)
        XCTAssertFalse(sut.saveButton.isEnabled)
        XCTAssertTrue(sut.saveButton.isLoading)
    }
    
    func testHideLoading() {
        sut.hideLoading()
        XCTAssertNil(sut.navigationItem.leftBarButtonItem)
        XCTAssertTrue(sut.currentPasswordTextField.isEnabled)
        XCTAssertTrue(sut.newPasswordTextField.isEnabled)
        XCTAssertTrue(sut.newPasswordConfirmationTextField.isEnabled)
        XCTAssertTrue(sut.saveButton.isEnabled)
        XCTAssertFalse(sut.saveButton.isLoading)
    }
}
