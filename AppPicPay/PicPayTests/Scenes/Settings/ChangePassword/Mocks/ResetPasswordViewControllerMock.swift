@testable import PicPay

final class ResetPasswordNavigationControllerMock: UINavigationController {
    var popViewControllerCalled = false
    
    override func popViewController(animated: Bool) -> UIViewController? {
        self.popViewControllerCalled = true
        return nil
    }
}

final class ResetPasswordViewControllerMock: UIViewController, ResetPasswordDisplay {
    var displayErrorCalled = false
    var displayAlertCalled = false
    var displaySuccessfullyChangePasswordAlertMessageCalled = false
    var displayCurrentPasswordErrorMessageCalled = false
    var displayNewPasswordErrorMessageCalled = false
    var displayNewPasswordConfirmationErrorMessageCalled = false
    var setNumericTextFieldForPasswordTextFieldsCalled = false
    var displayLoadingCalled = false
    var hideLoadingCalled = false
    var dismissViewControllerCalled = false
    
    var error: Error?
    var message: String?
    var errorMessage: String?
    
    func display(error: Error) {
        self.displayErrorCalled = true
        self.error = error
    }
    
    func displayAlert(message: String) {
        self.displayAlertCalled = true
        self.message = message
    }
    
    func displaySuccessfullyChangePasswordAlert(message: String) {
        self.displaySuccessfullyChangePasswordAlertMessageCalled = true
        self.message = message
    }
    
    func displayCurrentPassword(errorMessage: String?) {
        self.displayCurrentPasswordErrorMessageCalled = true
        self.errorMessage = errorMessage
    }
    
    func displayNewPassword(errorMessage: String?) {
        self.displayNewPasswordErrorMessageCalled = true
        self.errorMessage = errorMessage
    }
    
    func displayNewPasswordConfirmation(errorMessage: String?) {
        self.displayNewPasswordConfirmationErrorMessageCalled = true
        self.errorMessage = errorMessage
    }
    
    func setNumericTextFieldForPasswordTextFields() {
        self.setNumericTextFieldForPasswordTextFieldsCalled = true
    }
    
    func displayLoading() {
        self.displayLoadingCalled = true
    }
    
    func hideLoading() {
        self.hideLoadingCalled = true
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        self.dismissViewControllerCalled = true
    }
}
