@testable import PicPay

final class ResetPasswordCoordinatorMock: ResetPasswordCoordinating {
    var viewController: UIViewController?
    
    var performActionCalled = false
    
    var action: ResetPasswordAction? = nil
    
    func perform(action: ResetPasswordAction) {
        self.performActionCalled = true
        self.action = action
    }
}
