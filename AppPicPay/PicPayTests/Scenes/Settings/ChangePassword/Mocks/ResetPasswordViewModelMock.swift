@testable import PicPay

final class ResetPasswordViewModelMock: ResetPasswordViewModelInputs {
    var fetchConsumerConfigurationCalled = false
    var changePasswordCalled = false
    var successAlertOkButtonTappedCalled = false
    
    var currentPassword: String? = nil
    var newPassword: String? = nil
    var newPasswordConfirmation: String? = nil
    
    func fetchConsumerConfiguration() {
        fetchConsumerConfigurationCalled = true
    }
    
    func changePassword(_ currentPassword: String?, newPassword: String?, newPasswordConfirmation: String?) {
        changePasswordCalled = true
        self.currentPassword = currentPassword
        self.newPassword = newPassword
        self.newPasswordConfirmation = newPasswordConfirmation
    }
    
    func successAlertOkButtonTapped() {
        successAlertOkButtonTappedCalled = true
    }
}
