@testable import PicPay

final class ResetPasswordServiceMock: ResetPasswordServicing {
    var expectedResult: Result<Bool, Error> = .success(true)
    
    func change(currentPassword: String, with newPassword: String, completion: @escaping (Result<Bool, Error>) -> Void) {
        completion(expectedResult)
    }
}
