@testable import PicPay

final class ResetPasswordPresenterMock: ResetPasswordPresenting {
    var setNumericTextFieldForPasswordTextFieldsCalled = false
    var displayErrorCalled = false
    var displayResetPasswordErrorCalled = false
    var clearFormErrorsCalled = false
    var displayLoadingCalled = false
    var hideLoadingCalled = false
    var displaySuccessfullyChangedPasswordAlertCalled = false
    var didNextStepActionCalled = false
    
    var error: Error? = nil
    var resetPasswordError: ResetPasswordError? = nil
    var action: ResetPasswordAction? = nil
    
    weak var viewController: ResetPasswordDisplay?
    
    func setNumericTextFieldForPasswordTextFields() {
        self.setNumericTextFieldForPasswordTextFieldsCalled = true
    }
    
    func display(error: Error) {
        self.displayErrorCalled = true
        self.error = error
    }
    
    func display(error: ResetPasswordError) {
        self.displayResetPasswordErrorCalled = true
        self.resetPasswordError = error
    }
    
    func clearFormErrors() {
        self.clearFormErrorsCalled = true
    }
    
    func displayLoading() {
        self.displayLoadingCalled = true
    }
    
    func hideLoading() {
        self.hideLoadingCalled = true
    }
    
    func displaySuccessfullyChangedPasswordAlert() {
        self.displaySuccessfullyChangedPasswordAlertCalled = true
    }
    
    func didNextStep(action: ResetPasswordAction) {
        self.didNextStepActionCalled = true
        self.action = action
    }
}
