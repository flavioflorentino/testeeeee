import XCTest
@testable import PicPay

final class ResetPasswordPresenterTests: XCTestCase {
    private lazy var sut: ResetPasswordPresenter = {
        let sut = ResetPasswordPresenter(coordinator: coordinator)
        sut.viewController = spy
        return sut
    }()
    private let spy = ResetPasswordViewControllerMock()
    private let coordinator = ResetPasswordCoordinatorMock()
    
    func testSetNumericTextFieldForPasswordTextFields() {
        sut.setNumericTextFieldForPasswordTextFields()
        XCTAssertTrue(spy.setNumericTextFieldForPasswordTextFieldsCalled)
    }
    
    func testDisplayError() {
        let error: Error = ResetPasswordError.newPasswordDoesNotMatch
        sut.display(error: error)
        XCTAssertTrue(spy.displayErrorCalled)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testDisplayResetPasswordPasswordDoesNotMatchError() {
        let error = ResetPasswordError.newPasswordDoesNotMatch
        sut.display(error: error)
        XCTAssertTrue(spy.displayAlertCalled)
        XCTAssertEqual(spy.message, error.localizedDescription)
    }
    
    func testDisplayResetPasswordMissingCurrentPasswordError() {
        let error = ResetPasswordError.missingCurrentPassword
        sut.display(error: error)
        XCTAssertTrue(spy.displayCurrentPasswordErrorMessageCalled)
        XCTAssertEqual(spy.errorMessage, error.localizedDescription)
    }
    
    func testDisplayResetPasswordMissingNewPasswordError() {
        let error = ResetPasswordError.missingNewPassword
        sut.display(error: error)
        XCTAssertTrue(spy.displayNewPasswordErrorMessageCalled)
        XCTAssertEqual(spy.errorMessage, error.localizedDescription)
    }
    
    func testDisplayResetPasswordMissingNewPasswordConfirmationError() {
        let error = ResetPasswordError.missingNewPasswordConfirmation
        sut.display(error: error)
        XCTAssertTrue(spy.displayNewPasswordConfirmationErrorMessageCalled)
        XCTAssertEqual(spy.errorMessage, error.localizedDescription)
    }
    
    func testClearFormErrors() {
        sut.clearFormErrors()
        XCTAssertTrue(spy.displayCurrentPasswordErrorMessageCalled)
        XCTAssertTrue(spy.displayNewPasswordErrorMessageCalled)
        XCTAssertTrue(spy.displayNewPasswordConfirmationErrorMessageCalled)
        XCTAssertNil(spy.errorMessage)
    }
    
    func testDisplaySuccessfullyChangedPasswordAlert() {
        sut.displaySuccessfullyChangedPasswordAlert()
        XCTAssertTrue(spy.displaySuccessfullyChangePasswordAlertMessageCalled)
        XCTAssertEqual(spy.message, "Senha alterada com sucesso!")
    }
    
    func testDisplayLoading() {
        sut.displayLoading()
        XCTAssertTrue(spy.displayLoadingCalled)
    }
    
    func testHideLoading() {
        sut.hideLoading()
        XCTAssertTrue(spy.hideLoadingCalled)
    }
    
    func testDidNextStep() {
        let action: ResetPasswordAction = .dismiss
        sut.didNextStep(action: action)
        XCTAssertTrue(coordinator.performActionCalled)
        XCTAssertEqual(coordinator.action, action)
    }
}

