import XCTest
@testable import PicPay

final class ResetPasswordViewModelTests: XCTestCase {
    private let spy = ResetPasswordPresenterMock()
    private let service = ResetPasswordServiceMock()
    private lazy var sut = ResetPasswordViewModel(service: service, presenter: spy)

    func testFetchConsumerConfigurationNumericTextField() {
        let consumer = MBConsumer()
        consumer.numericKeyboardForPassword = true
        ConsumerManager.shared.consumer = consumer
        sut.fetchConsumerConfiguration()
        XCTAssertTrue(spy.setNumericTextFieldForPasswordTextFieldsCalled)
    }
    
    func testFetchConsumerConfigurationNone() {
        let consumer = MBConsumer()
        consumer.numericKeyboardForPassword = false
        ConsumerManager.shared.consumer = consumer
        sut.fetchConsumerConfiguration()
        XCTAssertFalse(spy.setNumericTextFieldForPasswordTextFieldsCalled)
    }
    
    func testClearFormErrors() {
        sut.changePassword("1234", newPassword: "1234", newPasswordConfirmation: "1234")
        XCTAssertTrue(spy.clearFormErrorsCalled)
    }
    
    func testChangePasswordAllMissingCurrentPassword() {
        sut.changePassword(nil, newPassword: "1234", newPasswordConfirmation: "1234")
        XCTAssertTrue(spy.clearFormErrorsCalled)
        XCTAssertTrue(spy.displayResetPasswordErrorCalled)
        XCTAssertEqual(spy.resetPasswordError, ResetPasswordError.missingCurrentPassword)
    }
    
    func testChangePasswordAllMissingNewPassword() {
        sut.changePassword("1234", newPassword: nil, newPasswordConfirmation: "1234")
        XCTAssertTrue(spy.clearFormErrorsCalled)
        XCTAssertTrue(spy.displayResetPasswordErrorCalled)
        XCTAssertEqual(spy.resetPasswordError, ResetPasswordError.missingNewPassword)
    }
    
    func testChangePasswordAllMissingNewPasswordConfirmation() {
        sut.changePassword("1234", newPassword: "1234", newPasswordConfirmation: nil)
        XCTAssertTrue(spy.clearFormErrorsCalled)
        XCTAssertTrue(spy.displayResetPasswordErrorCalled)
        XCTAssertEqual(spy.resetPasswordError, ResetPasswordError.missingNewPasswordConfirmation)
    }
    
    func testChangePasswordAllMissingNewPasswordDoesNotMatch() {
        sut.changePassword("1234", newPassword: "1234", newPasswordConfirmation: "123456")
        XCTAssertTrue(spy.clearFormErrorsCalled)
        XCTAssertTrue(spy.displayResetPasswordErrorCalled)
        XCTAssertEqual(spy.resetPasswordError, ResetPasswordError.newPasswordDoesNotMatch)
    }
    
    func testChangePasswordSuccessfully() {
        service.expectedResult = .success(true)
        sut.changePassword("1234", newPassword: "1234", newPasswordConfirmation: "1234")
        XCTAssertTrue(spy.clearFormErrorsCalled)
        XCTAssertTrue(spy.displayLoadingCalled)
        XCTAssertTrue(spy.hideLoadingCalled)
        XCTAssertTrue(spy.displaySuccessfullyChangedPasswordAlertCalled)
        XCTAssertFalse(spy.displayErrorCalled)
        XCTAssertFalse(spy.displayResetPasswordErrorCalled)
    }
    
    func testChangePasswordRequestError() {
        service.expectedResult = .failure(ResetPasswordError.newPasswordDoesNotMatch)
        sut.changePassword("1234", newPassword: "1234", newPasswordConfirmation: "1234")
        XCTAssertTrue(spy.clearFormErrorsCalled)
        XCTAssertTrue(spy.displayLoadingCalled)
        XCTAssertTrue(spy.hideLoadingCalled)
        XCTAssertFalse(spy.displaySuccessfullyChangedPasswordAlertCalled)
        XCTAssertTrue(spy.displayErrorCalled)
        XCTAssertFalse(spy.displayResetPasswordErrorCalled)
    }
    
    func testSuccessAlertOkButtonTapped() {
        sut.successAlertOkButtonTapped()
        XCTAssertTrue(spy.didNextStepActionCalled)
        XCTAssertEqual(spy.action, .dismiss)
    }
}
