@testable import PicPay
import XCTest

final class NewBankAccountFormCoordinatorTests: XCTestCase {
    private lazy var viewControllerSpy = NewBankAccountFormViewControllerMock()
    private lazy var navigationSpy = NavigationControllerMock(rootViewController: viewControllerSpy)
    private lazy var sut: NewBankAccountFormCoordinator = {
        let coordinator = NewBankAccountFormCoordinator()
        coordinator.viewController = viewControllerSpy
        return coordinator
    }()
    
    func testPerformActionSaveCompletion_WhenCalledFromPresenter_ShouldExecuteSaveCompletion() {
        let expectation = XCTestExpectation(description: "save completion")
        sut.onSaveCompletion = { _ in
            expectation.fulfill()
        }
        
        sut.perform(action: .saveCompletion)
        
        wait(for: [expectation], timeout: 0.1)
    }
    
    func testPerformActionPopToBankList_WhenCalledFromPresenter_ShouldPopViewController() {
        let window = UIWindow(frame: CGRect(origin: .zero, size: CGSize(width: 400, height: 400)))
        window.addSubview(navigationSpy.view)
        RunLoop.main.run(until: Date())
        
        sut.perform(action: .popToBankList)
        
        XCTAssertTrue(navigationSpy.isPopViewControllerCalled)
    }
}
