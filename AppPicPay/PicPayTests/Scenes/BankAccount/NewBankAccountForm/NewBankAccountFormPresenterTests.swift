@testable import PicPay
import UI
import Core
import XCTest

final class NewBankAccountFormPresenterTests: XCTestCase {
    private lazy var spy = NewBankAccountFormViewControllerMock()
    private lazy var coordinatorSpy = NewBankAccountFormCoordinatorMock()
    private lazy var sut: NewBankAccountFormPresenter = {
        let presenter = NewBankAccountFormPresenter(coordinator: coordinatorSpy)
        presenter.viewController = spy
        return presenter
    }()
    
    func testDisplayBank_WhenReceivePPBank_ShouldConvertToBankItem() {
        let bank = PPBank()
        bank.wsId = "123"
        bank.name = "Bank"
        let bankAccount = PPBankAccount()
        bankAccount.bank = bank
        let expectedBank = BankItem(bank: bank)
        
        sut.display(bankAccount: bankAccount)
        
        XCTAssertEqual(spy.callDisplayBankCount, 1)
        XCTAssertEqual(spy.bank, expectedBank)
    }
    
    func testConfigureForm_WhenReceivePPBankAccountForm_ShouldConfigureFormWithBankAccountFormConfiguration() {
        let bankAccountForm = PPBankAccountForm()
        
        sut.configureForm(using: bankAccountForm)
        
        XCTAssertEqual(spy.callConfigureFormUsingConfigurationCount, 1)
        XCTAssertEqual(spy.callConfigureAccountTypeOptionCount, 1)
        
        XCTAssertEqual(spy.configuration?.branchFieldConfiguration, bankAccountForm.branchFieldConfiguration)
        XCTAssertEqual(spy.configuration?.branchDigitFieldConfiguration, bankAccountForm.branchDigitFieldConfiguration)
        XCTAssertEqual(spy.configuration?.accountFieldConfiguration, bankAccountForm.accountFieldConfiguration)
        XCTAssertEqual(spy.configuration?.accountDigitFieldConfiguration, bankAccountForm.accountDigitFieldConfiguration)
        XCTAssertEqual(spy.configuration?.accountTypeFieldConfiguration, bankAccountForm.accountTypeFieldConfiguration)
        XCTAssertEqual(spy.configuration?.documentFieldConfiguration, bankAccountForm.documentFieldConfiguration)
        XCTAssertEqual(spy.configuration?.recipientNameFieldConfiguration, bankAccountForm.recipientNameFieldConfiguration)
        
        XCTAssertEqual(spy.option?.map { $0.rowTitle }, bankAccountForm.accountTypes.map { $0.rowTitle })
        XCTAssertEqual(spy.option?.map { $0.rowValue }, bankAccountForm.accountTypes.map { $0.rowValue })
    }
    
    func testStartLoading_WhenCalledFromPresenter_ShouldCallStartLoading() {
        sut.startLoading()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callChangeTextFieldEnabledStateCount, 1)
        XCTAssertEqual(spy.textFieldEnabledState, false)
    }
    
    func testStopLoading_WhenCalledFromPresenter_ShouldCallStopLoading() {
        sut.stopLoading()
        
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callChangeTextFieldEnabledStateCount, 1)
        XCTAssertEqual(spy.textFieldEnabledState, true)
    }
    
    func testPresentPopTipForBranchDigit_WhenCalledFromPresenter_ShouldCallDisplayPopTipForBranchDigit() {
        let text = "Some text"
        
        sut.presentPopTipForBranchDigit(text: text)
        
        XCTAssertEqual(spy.callDisplayPopTipForBranchDigitCount, 1)
        XCTAssertEqual(spy.popTipText, text)
    }
    
    func testPresentError_whenCalledFromPresenter_shouldDisplayError() {
        let error = ApiError.bodyNotFound
        
        sut.present(error: error)
        
        XCTAssertEqual(spy.callDisplayErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testPresentValidationErrors_WhenCalledFromPresenterWithAllValidationErrors_ShouldDisplayValidationErrorForEachField() {
        let validationErrors = NewBankAccountFormValidationError.allCases
        
        sut.present(validationErrors: validationErrors)
        
        XCTAssertEqual(spy.callDisplayBranchNumberErrorMessageCount, 1)
        XCTAssertEqual(spy.callDisplayBranchDigitErrorMessageCount, 1)
        XCTAssertEqual(spy.callDisplayAccountNumberErrorMessageCount, 1)
        XCTAssertEqual(spy.callDisplayAccountDigitErrorMessageCount, 1)
        XCTAssertEqual(spy.callDisplayAccountTypeErrorMessageCount, 1)
        XCTAssertEqual(spy.callDisplayDocumentErrorMessageCount, 1)
        XCTAssertEqual(spy.callDisplayRecipientNameErrorMessageCount, 1)
        
        XCTAssertEqual(spy.branchNumberErrorMessage, NewBankAccountFormValidationError.invalidBranchNumber.message)
        XCTAssertEqual(spy.branchDigitErrorMessage, NewBankAccountFormValidationError.invalidBranchDigit.message)
        XCTAssertEqual(spy.accountNumberErrorMessage, NewBankAccountFormValidationError.invalidAccountNumber.message)
        XCTAssertEqual(spy.accountDigitErrorMessage, NewBankAccountFormValidationError.invalidAccountDigit.message)
        XCTAssertEqual(spy.accountTypeErrorMessage, NewBankAccountFormValidationError.invalidAccountType.message)
        XCTAssertEqual(spy.documentErrorMessage, NewBankAccountFormValidationError.invalidDocumentNumber.message)
        XCTAssertEqual(spy.recipientNameErrorMessage, NewBankAccountFormValidationError.invalidRecipientName.message)
    }
    
    func testPresentValidationErrors_WhenCalledFromPresenterWithSomeValidationErrors_ShouldDisplayValidationErrorForSomeErrors() {
        let validationErrors: [NewBankAccountFormValidationError] = [.invalidAccountType, .invalidBranchNumber]
        
        sut.present(validationErrors: validationErrors)
        
        XCTAssertEqual(spy.callDisplayBranchNumberErrorMessageCount, 1)
        XCTAssertEqual(spy.callDisplayBranchDigitErrorMessageCount, 1)
        XCTAssertEqual(spy.callDisplayAccountNumberErrorMessageCount, 1)
        XCTAssertEqual(spy.callDisplayAccountDigitErrorMessageCount, 1)
        XCTAssertEqual(spy.callDisplayAccountTypeErrorMessageCount, 1)
        XCTAssertEqual(spy.callDisplayDocumentErrorMessageCount, 1)
        
        XCTAssertEqual(spy.branchNumberErrorMessage, NewBankAccountFormValidationError.invalidBranchNumber.message)
        XCTAssertNil(spy.branchDigitErrorMessage)
        XCTAssertNil(spy.accountNumberErrorMessage)
        XCTAssertNil(spy.accountDigitErrorMessage)
        XCTAssertEqual(spy.accountTypeErrorMessage, NewBankAccountFormValidationError.invalidAccountType.message)
        XCTAssertNil(spy.documentErrorMessage)
    }
    
    func testDidNextStep_WhenReceiveSaveCompletionAction_ShouldCallPerformActionSaveCompletion() {
        let action: NewBankAccountFormAction = .saveCompletion
        
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.action, action)
    }
    
    func testDidNextStep_WhenReceivePopToBankListAction_ShouldCallPopToBankList() {
        let action: NewBankAccountFormAction = .popToBankList
        
        sut.didNextStep(action: action)
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertEqual(coordinatorSpy.action, action)
    }
}

extension FormFieldConfiguration: Equatable {
    public static func == (lhs: FormFieldConfiguration, rhs: FormFieldConfiguration) -> Bool {
        return
            lhs.isHidden == rhs.isHidden &&
            lhs.keyboardType == rhs.keyboardType &&
            lhs.maxTextLength == rhs.maxTextLength
    }
}
