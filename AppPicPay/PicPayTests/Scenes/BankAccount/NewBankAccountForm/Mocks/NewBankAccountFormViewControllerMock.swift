@testable import PicPay
import UI
import UIKit

final class NewBankAccountFormViewControllerMock: UIViewController, NewBankAccountFormDisplay {
    private(set) var callDisplayBankCount = 0
    private(set) var callPopulateFormCount = 0
    private(set) var callConfigureFormUsingConfigurationCount = 0
    private(set) var callConfigureAccountTypeOptionCount = 0
    private(set) var callDisplayErrorCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callDisplayPopTipForBranchDigitCount = 0
    private(set) var callChangeTextFieldEnabledStateCount = 0
    private(set) var callDisplayBranchNumberErrorMessageCount = 0
    private(set) var callDisplayBranchDigitErrorMessageCount = 0
    private(set) var callDisplayAccountNumberErrorMessageCount = 0
    private(set) var callDisplayAccountDigitErrorMessageCount = 0
    private(set) var callDisplayAccountTypeErrorMessageCount = 0
    private(set) var callDisplayDocumentErrorMessageCount = 0
    private(set) var callDisplayRecipientNameErrorMessageCount = 0

    private(set) var bank: BankItem?
    private(set) var form: BankAccountFormInput?
    private(set) var configuration: BankAccountFormConfiguration?
    private(set) var option: [PickerOption]?
    private(set) var error: Error?
    private(set) var popTipText: String?
    private(set) var textFieldEnabledState: Bool?
    private(set) var branchNumberErrorMessage: String?
    private(set) var branchDigitErrorMessage: String?
    private(set) var accountNumberErrorMessage: String?
    private(set) var accountDigitErrorMessage: String?
    private(set) var accountTypeErrorMessage: String?
    private(set) var documentErrorMessage: String?
    private(set) var recipientNameErrorMessage: String?

    func display(bank: BankItem) {
        callDisplayBankCount += 1
        self.bank = bank
    }
    
    func populateForm(_ form: BankAccountFormInput) {
        callPopulateFormCount += 1
        self.form = form
    }
    
    func configureForm(using configuration: BankAccountFormConfiguration) {
        callConfigureFormUsingConfigurationCount += 1
        self.configuration = configuration
    }
    
    func configureAccountType(option: [PickerOption]) {
        callConfigureAccountTypeOptionCount += 1
        self.option = option
    }
    
    func display(error: Error) {
        callDisplayErrorCount += 1
        self.error = error
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
    
    func displayPopTipForBranchDigit(text: String) {
        callDisplayPopTipForBranchDigitCount += 1
        popTipText = text
    }
    
    func changeTextFieldEnableState(to enabled: Bool) {
        callChangeTextFieldEnabledStateCount += 1
        textFieldEnabledState = enabled
    }
    
    func displayBranchNumber(errorMessage: String?) {
        callDisplayBranchNumberErrorMessageCount += 1
        branchNumberErrorMessage = errorMessage
    }
    
    func displayBranchDigit(errorMessage: String?) {
        callDisplayBranchDigitErrorMessageCount += 1
        branchDigitErrorMessage = errorMessage
    }
    
    func displayAccountNumber(errorMessage: String?) {
        callDisplayAccountNumberErrorMessageCount += 1
        accountNumberErrorMessage = errorMessage
    }
    
    func displayAccountDigit(errorMessage: String?) {
        callDisplayAccountDigitErrorMessageCount += 1
        accountDigitErrorMessage = errorMessage
    }
    
    func displayAccountType(errorMessage: String?) {
        callDisplayAccountTypeErrorMessageCount += 1
        accountTypeErrorMessage = errorMessage
    }
    
    func displayDocument(errorMessage: String?) {
        callDisplayDocumentErrorMessageCount += 1
        documentErrorMessage = errorMessage
    }
    
    func displayRecipientName(errorMessage: String?) {
        callDisplayRecipientNameErrorMessageCount += 1
        recipientNameErrorMessage = errorMessage
    }
}
