@testable import PicPay

final class NewBankAccountFormViewModelMock: NewBankAccountFormViewModelInputs {
    private(set) var callFetchBankAccountInformationCount = 0
    private(set) var callBranchDigitTextDidChangeCount = 0
    private(set) var callSaveBankAccountCount = 0
    private(set) var callChangeBankCount = 0
    
    private(set) var branchDigitText: String?
    private(set) var form: BankAccountFormInput?
    
    func fetchBankAccountInformation() {
        callFetchBankAccountInformationCount += 1
    }
    
    func branchDigitTextDidChange(_ branchDigitText: String?) {
        callBranchDigitTextDidChangeCount += 1
        self.branchDigitText = branchDigitText
    }
    
    func save(bankAccount form: BankAccountFormInput) {
        callSaveBankAccountCount += 1
        self.form = form
    }
    
    func changeBank() {
        callChangeBankCount += 1
    }
}
