@testable import PicPay

extension BankAccountFormInput: Equatable {
    static var mock: BankAccountFormInput {
        BankAccountFormInput(
            branchNumber: "1234",
            branchDigit: "",
            accountNumber: "12345",
            accountDigit: "2",
            accountType: PPBankAccountType(label: "Conta Corrente", value: "C"),
            differentAccountHolder: false,
            accountHolderDocument: ""
        )
    }
    
    public static func == (lhs: BankAccountFormInput, rhs: BankAccountFormInput) -> Bool {
        return lhs.branchNumber == rhs.branchNumber
            && lhs.branchDigit == rhs.branchDigit
            && lhs.accountNumber == rhs.accountNumber
            && lhs.accountDigit == rhs.accountDigit
            && lhs.accountType?.rowValue == rhs.accountType?.rowValue
            && lhs.differentAccountHolder == rhs.differentAccountHolder
            && lhs.accountHolderDocument == rhs.accountHolderDocument
    }
}
