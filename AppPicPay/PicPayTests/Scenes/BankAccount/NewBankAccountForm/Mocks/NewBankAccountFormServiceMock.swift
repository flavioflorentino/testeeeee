@testable import PicPay

final class NewBankAccountFormServiceMock: NewBankAccountFormServicing {
    
    lazy var saveBankAccountExpectedResult: Result<Void, Error> = .success
    
    func save(bankAccount: PPBankAccount, completion: @escaping (Result<Void, Error>) -> Void) {
        completion(saveBankAccountExpectedResult)
    }
}
