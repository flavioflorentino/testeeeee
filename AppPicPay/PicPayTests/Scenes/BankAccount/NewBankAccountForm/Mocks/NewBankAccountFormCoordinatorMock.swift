@testable import PicPay

final class NewBankAccountFormCoordinatorMock: NewBankAccountFormCoordinating {
    
    var viewController: UIViewController?
    var onSaveCompletion: ((Bool) -> Void)?
    var isUpdate: Bool = false
    
    private(set) var callPerformActionCount = 0
    
    private(set) var action: NewBankAccountFormAction?
    
    func perform(action: NewBankAccountFormAction) {
        callPerformActionCount += 1
        self.action = action
    }
}
