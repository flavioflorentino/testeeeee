@testable import PicPay
import Validator

final class NewBankAccountFormPresenterMock: NewBankAccountFormPresenting {
    
    var viewController: NewBankAccountFormDisplay?
    
    private(set) var callDisplayBankAccountCount = 0
    private(set) var callConfigureFormUsingConfigurationCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callPresentPopTipForBranchDigitCount = 0
    private(set) var callPresentErrorCount = 0
    private(set) var callPresentValidationErrorsCount = 0
    private(set) var callPresentInvalidDocumentNumberMessageCount = 0
    private(set) var callPresentInvalidRecipientNameMessageCount = 0
    private(set) var callDidNextStepCount = 0
    
    private(set) var bankAccount: PPBankAccount?
    private(set) var configuration: PPBankAccountForm?
    private(set) var popTipText: String?
    private(set) var error: Error?
    private(set) var validationErrors: [ValidationError]?
    private(set) var invalidDocumentMessage: String?
    private(set) var invalidRecipientNameMessage: String?
    private(set) var action: NewBankAccountFormAction?
    
    func display(bankAccount: PPBankAccount) {
        callDisplayBankAccountCount += 1
        self.bankAccount = bankAccount
    }
    
    func configureForm(using configuration: PPBankAccountForm) {
        callConfigureFormUsingConfigurationCount += 1
        self.configuration = configuration
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
    
    func presentPopTipForBranchDigit(text: String) {
        callPresentPopTipForBranchDigitCount += 1
        popTipText = text
    }
    
    func present(error: Error) {
        callPresentErrorCount += 1
        self.error = error
    }
    
    func present(validationErrors: [ValidationError]) {
        callPresentValidationErrorsCount += 1
        self.validationErrors = validationErrors
    }
    
    func presentInvalidDocumentNumber(message: String) {
        callPresentInvalidDocumentNumberMessageCount += 1
        invalidDocumentMessage = message
    }
    func presentInvalidRecipientName(message: String){
        callPresentInvalidRecipientNameMessageCount += 1
        invalidRecipientNameMessage = message
    }
    func didNextStep(action: NewBankAccountFormAction) {
        callDidNextStepCount += 1
        self.action = action
    }
}
