@testable import PicPay
import XCTest

final class NewBankAccountFormViewControllerTests: XCTestCase {
    private lazy var spy = NewBankAccountFormViewModelMock()
    private lazy var sut = NewBankAccountFormViewController(viewModel: spy)
    private lazy var window = UIWindow(frame: UIScreen.main.bounds)
    
    override func setUp() {
        super.setUp()
        window.addSubview(sut.view)
        RunLoop.main.run(until: Date())
    }
    
    func testViewDidLoad_WhenCalledFromViewControllerLifeCycle_ShouldCallFetchBankInformation() {
        XCTAssertEqual(spy.callFetchBankAccountInformationCount, 1)
    }
    
    func testSaveButtonTapped_WhenFormHasValues_ShouldCallSaveBankWithFormValues() {
        let form = BankAccountFormInput.mock
        
        sut.branchNumberTextField.text = form.branchNumber
        sut.branchDigitTextField.text = form.branchDigit
        sut.accountNumberTextField.text = form.accountNumber
        sut.accountDigitTextField.text = form.accountDigit
        sut.accountTypePickerInputView.selectedOption = form.accountType
        sut.accountHolderContainerView.documentNumberTextField.text = form.accountHolderDocument
        sut.accountHolderContainerView.recipientNameTextField.text = form.accountRecipientName
        
        sut.saveButtonTapped()
        
        XCTAssertEqual(spy.callSaveBankAccountCount, 1)
        XCTAssertEqual(spy.form, form)
    }
    
    func testChangeBankTapped_WhenCalledFromController_ShouldCallChangeBank() {
        sut.changeBankTapped()
        
        XCTAssertEqual(spy.callChangeBankCount, 1)
    }
    
    func testBranchDigitTextFieldEditingChangedEvent_WhenReceiveEditingChangedControlEvent_ShouldCallBranchDigitTextDidChange() {
        let branchDigitText = "0"
        sut.branchDigitTextField.text = branchDigitText
        
        sut.textFieldEditingChanged(sut.branchDigitTextField)
        
        XCTAssertEqual(spy.callBranchDigitTextDidChangeCount, 1)
        XCTAssertEqual(spy.branchDigitText, branchDigitText)
    }
}
