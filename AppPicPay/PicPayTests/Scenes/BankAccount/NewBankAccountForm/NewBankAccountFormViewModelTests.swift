@testable import PicPay
import XCTest

final class NewBankAccountFormViewModelTests: XCTestCase {
    private lazy var serviceMock = NewBankAccountFormServiceMock()
    private lazy var spy = NewBankAccountFormPresenterMock()
    private lazy var sut = NewBankAccountFormViewModel(service: serviceMock, presenter: spy, bankAccount: bankAccountMock)
    
    private lazy var bankMock: PPBank = {
        let bank = PPBank()
        bank.wsId = "123"
        bank.name = "Bank name"
        return bank
    }()
    
    private lazy var bankAccountMock: PPBankAccount = {
        let bankAccount = PPBankAccount()
        bankAccount.bank = bankMock
        return bankAccount
    }()
    
    func testFetchBankInformation_whenBankHasNoBankAccountFormConfiguration_shouldDisplayBankInformationAndConfigureBankAccountFormWithDefaultBankAccountFormConfiguration() {
        let configuration = PPBankAccountForm()
        
        sut.fetchBankAccountInformation()
        
        XCTAssertEqual(spy.callDisplayBankAccountCount, 1)
        XCTAssertEqual(spy.callConfigureFormUsingConfigurationCount, 1)
        XCTAssertEqual(spy.bankAccount, bankAccountMock)
        XCTAssertEqual(spy.configuration?.branchFieldConfiguration, configuration.branchFieldConfiguration)
        XCTAssertEqual(spy.configuration?.branchDigitFieldConfiguration, configuration.branchDigitFieldConfiguration)
        XCTAssertEqual(spy.configuration?.accountFieldConfiguration, configuration.accountFieldConfiguration)
        XCTAssertEqual(spy.configuration?.accountDigitFieldConfiguration, configuration.accountDigitFieldConfiguration)
        XCTAssertEqual(spy.configuration?.accountTypeFieldConfiguration, configuration.accountTypeFieldConfiguration)
        XCTAssertEqual(spy.configuration?.documentFieldConfiguration, configuration.documentFieldConfiguration)
    }
    
    func testFetchBankInformation_WhenBankHasCustomBankAccountFormConfiguration_ShouldDisplayBankInformationAndConfigureBankAccountFormCustomBankAccountFormConfiguration() {
        let configuration = PPBankAccountForm()
        configuration.isBranchDigitEnabled = true
        bankMock.form = configuration
        
        sut.fetchBankAccountInformation()
        
        XCTAssertEqual(spy.callDisplayBankAccountCount, 1)
        XCTAssertEqual(spy.callConfigureFormUsingConfigurationCount, 1)
        XCTAssertEqual(spy.bankAccount, bankAccountMock)
        XCTAssertEqual(spy.configuration?.branchFieldConfiguration, configuration.branchFieldConfiguration)
        XCTAssertEqual(spy.configuration?.branchDigitFieldConfiguration, configuration.branchDigitFieldConfiguration)
        XCTAssertEqual(spy.configuration?.accountFieldConfiguration, configuration.accountFieldConfiguration)
        XCTAssertEqual(spy.configuration?.accountDigitFieldConfiguration, configuration.accountDigitFieldConfiguration)
        XCTAssertEqual(spy.configuration?.accountTypeFieldConfiguration, configuration.accountTypeFieldConfiguration)
        XCTAssertEqual(spy.configuration?.documentFieldConfiguration, configuration.documentFieldConfiguration)
    }
    
    func testBranchDigitTextDidChange_WhenFormTypeIsBancoDoBrasilAndTextIs0_ShouldDisplayBranchDigitDisclaimer() {
        let configuration = PPBankAccountForm()
        configuration.type = .bancoDoBrasil
        bankMock.form = configuration
        
        let text = "0"
        
        sut.branchDigitTextDidChange(text)
        
        XCTAssertEqual(spy.callPresentPopTipForBranchDigitCount, 1)
        XCTAssertEqual(spy.popTipText, BankAccountLocalizable.branchDigitDisclaimer.text)
    }
    
    func testBranchDigitTextDidChange_WhenFormTypeIsBancoDoBrasilAndTextIsNot0_ShouldNotDisplayBranchDigitDisclaimer() {
        let configuration = PPBankAccountForm()
        configuration.type = .bancoDoBrasil
        bankMock.form = configuration
        
        let text = "1"
        
        sut.branchDigitTextDidChange(text)
        
        XCTAssertEqual(spy.callPresentPopTipForBranchDigitCount, 0)
        XCTAssertNil(spy.popTipText)
    }
    
    func testBranchDigitTextDidChange_WhenFormTypeIsNotBancoDoBrasilAndTextIs0_ShouldNotDisplayBranchDigitDisclaimer() {
        let configuration = PPBankAccountForm()
        configuration.type = .normal
        bankMock.form = configuration
        
        let text = "0"
        
        sut.branchDigitTextDidChange(text)
        
        XCTAssertEqual(spy.callPresentPopTipForBranchDigitCount, 0)
        XCTAssertNil(spy.popTipText)
    }
    
    func testBranchDigitTextDidChange_WhenFormTypeIsNotBancoDoBrasilAndTextIsNot0_ShouldNotDisplayBranchDigitDisclaimer() {
        let configuration = PPBankAccountForm()
        configuration.type = .normal
        bankMock.form = configuration
        
        let text = "1"
        
        sut.branchDigitTextDidChange(text)
        
        XCTAssertEqual(spy.callPresentPopTipForBranchDigitCount, 0)
        XCTAssertNil(spy.popTipText)
    }
    
    func testSaveBankAccountForm_WhenFormValuesAreValidAndReceiveSuccessFromService_ShouldDidNextStepSaveCompletion() {
        let form = BankAccountFormInput.mock
        serviceMock.saveBankAccountExpectedResult = .success
        
        sut.save(bankAccount: form)
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(spy.action, .saveCompletion)
    }
    
    func testSaveBankAccountForm_WhenFormValuesAreValidAndReceiveFailureFromService_ShouldPresentError() {
        let form = BankAccountFormInput.mock
        let error = PicPayError(message: "Some error")
        serviceMock.saveBankAccountExpectedResult = .failure(error)
        
        sut.save(bankAccount: form)
        
        XCTAssertEqual(spy.callPresentErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testSaveBankAccountForm_WhenFormValuesAreInvalid_ShouldPresentValidationErrors() throws {
        var form = BankAccountFormInput()
        form.differentAccountHolder = true
        let configuration = PPBankAccountForm()
        configuration.isBranchDigitEnabled = true
        let expectedValidationErrors = NewBankAccountFormValidationError.allCases
        
        bankMock.form = configuration
        
        sut.save(bankAccount: form)
        
        XCTAssertEqual(spy.callPresentValidationErrorsCount, 1)
        let validationErrors = try XCTUnwrap(spy.validationErrors as? [NewBankAccountFormValidationError])
        XCTAssertEqual(validationErrors, expectedValidationErrors)
    }
    
    func testChangeBank_WhenCalledFromController_ShouldDidNextStepPopToBankList() {
        sut.changeBank()
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        XCTAssertEqual(spy.action, .popToBankList)
    }
}
