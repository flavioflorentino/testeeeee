import XCTest
@testable import PicPay

final class BanksViewControllerTests: XCTestCase {
    private lazy var spy = BanksViewModelMock()
    private lazy var sut = BanksViewController(viewModel: spy, definedTitle: nil, searchPlaceholder: nil)
    private let window = UIWindow(frame: CGRect(origin: .zero, size: CGSize(width: 375, height: 812)))
    
    override func setUp() {
        super.setUp()
        loadView()
    }
    
    func loadView() {
        window.addSubview(sut.view)
        RunLoop.main.run(until: Date())
    }
    
    func testFetchBanksWhenViewDidLoad() {
        XCTAssertEqual(spy.callFetchBanksCount, 1)
    }
    
    func testShouldCallFilterWhenUpdateSearchResultsIsCalled() {
        let query = "Frequent"
        
        sut.searchController.searchBar.text = query
        
        XCTAssertEqual(spy.callFilterBanksQueryCount, 1)
        XCTAssertEqual(spy.query, query)
    }
    
    func testStartLoadingShouldStartAnimatingActivityIndicator() {
        sut.startLoading()
        
        XCTAssertTrue(sut.activityIndicatorView.isAnimating)
    }
    
    func testStopLoadingShouldStopAnimatingActivityIndicator() {
        sut.stopLoading()
        
        XCTAssertFalse(sut.activityIndicatorView.isAnimating)
    }
}
