import XCTest
import Core
@testable import PicPay

final class BanksViewModelTests: XCTestCase {
    private lazy var spy = BanksPresenterMock()
    private lazy var serviceMock = BanksServiceMock()
    private lazy var sut = BanksViewModel(service: serviceMock, presenter: spy)
    
    private lazy var bankListSectionMock: [BankListSection] = {
        return [frequentBanksSection, otherBanksSection]
    }()
    
    private lazy var frequentBanksSection: BankListSection = {
        let firstBank = PPBank()
        firstBank.wsId = "123"
        firstBank.name = "Frequent 1"
        
        let secondBank = PPBank()
        secondBank.wsId = "124"
        secondBank.name = "Frequent 2"
        
        return BankListSection(title: "Frequent Banks", rows: [firstBank, secondBank])
    }()
    
    private lazy var otherBanksSection: BankListSection = {
        let firstBank = PPBank()
        firstBank.wsId = "321"
        firstBank.name = "Other 1"
        
        let secondBank = PPBank()
        secondBank.wsId = "213"
        secondBank.name = "Other 2"
        
        return BankListSection(title: "Frequent Banks", rows: [firstBank, secondBank])
    }()
    
    func testFetchBanksAndWhenReceiveSuccessShouldPresentBanks() {
        serviceMock.fetchBanksExpectedResult = .success(bankListSectionMock)
        
        sut.fetchBanks()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callPresentBanksSectionsCount, 1)
        XCTAssertEqual(spy.sections, bankListSectionMock)
    }
    
    func testFetchBanksAndWhenReceiveFailureShouldPresentError() {
        let error: ApiError = .serverError
        serviceMock.fetchBanksExpectedResult = .failure(error)
        
        sut.fetchBanks()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
        XCTAssertEqual(spy.callStopLoadingCount, 1)
        XCTAssertEqual(spy.callPresentErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testInternalError_WhenReceiveFailure_ShouldPresentInternalerror() {
        let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
        serviceMock.fetchBanksExpectedResult = .success(bankListSectionMock)
        sut.fetchBanks()
        sut.selectBank(at: IndexPath(row: 100, section: 100))
        
        XCTAssertEqual(spy.callPresentInternalErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testFetchBanksSuccessfullyAndWhenFilterBanksWithQueryShouldPresentFilteredBanks() {
        serviceMock.fetchBanksExpectedResult = .success(bankListSectionMock)
        
        sut.fetchBanks()
        sut.filterBanks(query: "Frequent")
        
        XCTAssertEqual(spy.callPresentBanksSectionsCount, 2)
        XCTAssertEqual(spy.sections, [frequentBanksSection])
    }
    
    func testFetchBanksSucessfullyAndFilterBanksWithoutQueryShouldPresentAllFetchedBanks() {
        serviceMock.fetchBanksExpectedResult = .success(bankListSectionMock)
        
        sut.fetchBanks()
        sut.filterBanks(query: "")
        
        XCTAssertEqual(spy.callPresentBanksSectionsCount, 2)
        XCTAssertEqual(spy.sections, bankListSectionMock)
    }
    
    func testFetchBanksSucessfullyWhenFilterBanksWithQueryWithoutResultsShouldPresentEmptyResults() {
        serviceMock.fetchBanksExpectedResult = .success(bankListSectionMock)
        
        sut.fetchBanks()
        sut.filterBanks(query: "No results query")
        
        XCTAssertEqual(spy.callPresentBanksSectionsCount, 2)
        XCTAssertEqual(spy.callPresentEmptySearchResultsCount, 1)
        XCTAssertEqual(spy.sections, [])
    }
    
    func testSelectBankWhenCalledFromViewControllerShouldDidNextStepNewBankAccount() {
        let indexPath = IndexPath(row: 0, section: 0)
        serviceMock.fetchBanksExpectedResult = .success(bankListSectionMock)
        
        sut.fetchBanks()
        sut.selectBank(at: indexPath)
        
        XCTAssertEqual(spy.callDidNextStepCount, 1)
        if case let .selectBank(bank) = spy.action {
            XCTAssertEqual(bank, bankListSectionMock.first?.rows.first)
        } else {
            XCTFail("Wrong action")
        }
    }
}
