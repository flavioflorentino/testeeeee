@testable import PicPay

extension BankItem: Equatable {
    public static func ==(lhs: BankItem, rhs: BankItem) -> Bool {
        return lhs.name == rhs.name && lhs.logoURL == rhs.logoURL
    }
}
