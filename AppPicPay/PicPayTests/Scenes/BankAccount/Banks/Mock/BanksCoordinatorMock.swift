@testable import PicPay

final class BanksCoordinatorMock: BanksCoordinating {
    private(set) var callPerformActionCount = 0
    
    var viewController: UIViewController?
    var onSaveCompletion: ((Bool) -> Void)?
    var onBankSelectionCompletion: ((_ wsId: String, _ name: String) -> Void)?
    private(set) var action: BanksAction?
    
    func perform(action: BanksAction) {
        callPerformActionCount += 1
        self.action = action
    }
}
