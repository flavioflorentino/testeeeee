import UI
import UIKit
@testable import PicPay

final class BanksViewControllerMock: UIViewController, BanksDisplay {
    private(set) var callDisplayBanksCount = 0
    private(set) var callDisplayEmptySearchResultsCount = 0
    private(set) var callDisplayErrorCount = 0
    private(set) var callDisplayInternalErrorCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callShowViewControllerAnimatedCount = 0
    
    private(set) var banks: [Section<String, BankItem>]?
    private(set) var error: Error?
    private(set) var viewController: UIViewController?
    
    func display(banks: [Section<String, BankItem>]) {
        callDisplayBanksCount += 1
        self.banks = banks
    }
    
    func displayEmptySearchResults() {
        callDisplayEmptySearchResultsCount += 1
    }
    
    func display(error: Error) {
        callDisplayErrorCount += 1
        self.error = error
    }
    
    func displayInternalError(error: Error) {
        callDisplayInternalErrorCount += 1
        self.error = error
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
    
    override func show(_ vc: UIViewController, sender: Any?) {
        callShowViewControllerAnimatedCount += 1
        viewController = vc
    }
}
