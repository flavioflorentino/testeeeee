@testable import PicPay

final class BanksViewModelMock: BanksViewModelInputs {
    private(set) var callFetchBanksCount = 0
    private(set) var callFilterBanksQueryCount = 0
    private(set) var callSelectBankAtIndexPathCount = 0
    
    private(set) var query: String?
    private(set) var indexPath: IndexPath?
    
    func fetchBanks() {
        callFetchBanksCount += 1
    }
    
    func filterBanks(query: String?) {
        callFilterBanksQueryCount += 1
        self.query = query
    }
    
    func selectBank(at indexPath: IndexPath) {
        callSelectBankAtIndexPathCount += 1
        self.indexPath = indexPath
    }
}
