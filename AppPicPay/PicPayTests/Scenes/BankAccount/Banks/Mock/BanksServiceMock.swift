import Core
@testable import PicPay

final class BanksServiceMock: BanksServicing {
    var fetchBanksExpectedResult: Result<[BankListSection], ApiError> = .success([])
    
    func fetchBanks(completion: @escaping (Result<[BankListSection], ApiError>) -> Void) {
        completion(fetchBanksExpectedResult)
    }
}
