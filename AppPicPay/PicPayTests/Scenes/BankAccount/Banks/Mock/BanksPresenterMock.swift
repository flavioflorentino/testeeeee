@testable import PicPay

final class BanksPresenterMock: BanksPresenting {
    private(set) var callPresentBanksSectionsCount = 0
    private(set) var callPresentEmptySearchResultsCount = 0
    private(set) var callPresentErrorCount = 0
    private(set) var callPresentInternalErrorCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callDidNextStepCount = 0
    
    var viewController: BanksDisplay?
    private(set) var sections: [BankListSection]?
    private(set) var error: Error?
    private(set) var action: BanksAction?
    
    func presentBanks(sections: [BankListSection]) {
        callPresentBanksSectionsCount += 1
        self.sections = sections
    }
    
    func presentEmptySearchResults() {
        callPresentEmptySearchResultsCount += 1
    }
    
    func present(error: Error) {
        callPresentErrorCount += 1
        self.error = error
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func stopLoading() {
        callStopLoadingCount += 1
    }
    
    func didNextStep(action: BanksAction) {
        callDidNextStepCount += 1
        self.action = action
    }
    
    func presentInternalError(error: Error) {
        callPresentInternalErrorCount += 1
        self.error = error
    }
}
