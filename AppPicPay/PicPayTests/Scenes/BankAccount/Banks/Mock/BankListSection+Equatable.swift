@testable import PicPay

extension BankListSection: Equatable {
    public static func ==(lhs: BankListSection, rhs: BankListSection) -> Bool {
        return lhs.title == rhs.title && lhs.rows == rhs.rows
    }
}
