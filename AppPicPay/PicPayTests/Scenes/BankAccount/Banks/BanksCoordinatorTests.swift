import XCTest
@testable import PicPay

final class BanksCoordinatorTests: XCTestCase {
    private lazy var spy = BanksViewControllerMock()
    private lazy var sut: BanksCoordinator = {
        let coordinator = BanksCoordinator()
        coordinator.viewController = spy
        return coordinator
    }()
    
    func testPerformSelectBank_ShouldPresentBankAccountFormViewController() throws {        
        sut.perform(action: .selectBank(PPBank()))
        XCTAssertEqual(spy.callShowViewControllerAnimatedCount, 1)
        let viewController = try XCTUnwrap(spy.viewController)
        XCTAssertTrue(viewController is NewBankAccountFormViewController)
    }
}
