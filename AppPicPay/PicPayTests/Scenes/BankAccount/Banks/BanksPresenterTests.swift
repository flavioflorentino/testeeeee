import XCTest
import Core
@testable import PicPay

final class BanksPresenterTests: XCTestCase {
    private lazy var coordinatorMock = BanksCoordinatorMock()
    private lazy var spy = BanksViewControllerMock()
    private lazy var sut: BanksPresenter = {
        let presenter = BanksPresenter(coordinator: coordinatorMock)
        presenter.viewController = spy
        return presenter
    }()
    
    func testPresentBanksWhenCalledFromViewModelShouldFormatBanksAndDisplayBankAccounts() {
        let logoURL = "https://www.someimage.com/resource.jpg"
        let bank = PPBank()
        bank.wsId = "12"
        bank.name = "Some bank name"
        bank.imageUrl = logoURL
        
        let bankSection = BankListSection(title: "Some banks", rows: [bank])
        let formattedBank = BankItem(name: "12 - Some bank name", logoURL: URL(string: logoURL))
        
        
        sut.presentBanks(sections: [bankSection])
        
        XCTAssertEqual(spy.callDisplayBanksCount, 1)
        XCTAssertEqual(spy.banks?.first?.items.first, formattedBank)
    }
    
    func testPresentEmptySearchResultsWhenCalledFromViewModelShouldDisplayEmptySearchResults() {
        sut.presentEmptySearchResults()
        
        XCTAssertEqual(spy.callDisplayEmptySearchResultsCount, 1)
    }
    
    func testPresentErrorWhenCalledFromViewModelShouldDisplayError() {
        let error: ApiError = .serverError
        
        sut.present(error: error)
        
        XCTAssertEqual(spy.callDisplayErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testPresentInternalError_WhenCalledFromViewModel_ShouldDiplayUnexpectedError() {
        let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
        
        sut.presentInternalError(error: error)
        
        XCTAssertEqual(spy.callDisplayInternalErrorCount, 1)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testStartLoadingWhenCalledFromViewModelShouldStartLoading() {
        sut.startLoading()
        
        XCTAssertEqual(spy.callStartLoadingCount, 1)
    }
    
    func testStopLoadingWhenCalledFromViewModelShouldStopLoading() {
        sut.stopLoading()
        
        XCTAssertEqual(spy.callStopLoadingCount, 1)
    }
    
    func testDidNextStepNewBankAccountWhenCalledFromViewModelShouldPerformNewBankAccountAction() {
        let selectedBank = PPBank()
        selectedBank.wsId = "12"
        selectedBank.name = "Some"
        
        sut.didNextStep(action: .selectBank(selectedBank))
        
        XCTAssertEqual(coordinatorMock.callPerformActionCount, 1)
        if case let .selectBank(bank) = coordinatorMock.action {
            XCTAssertEqual(bank, selectedBank)
        } else {
            XCTFail("wrong action")
        }
    }
}
