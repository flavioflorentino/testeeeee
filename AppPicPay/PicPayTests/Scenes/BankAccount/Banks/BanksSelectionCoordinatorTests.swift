import XCTest
@testable import PicPay

final class BanksSelectionCoordinatorTests: XCTestCase {
    private let navigationController = NavigationControllerMock(rootViewController: UIViewController())
    private let controller = BanksViewControllerMock()
    private lazy var coordinator: BanksSelectionCoordinator = {
        let coordinator = BanksSelectionCoordinator()
        coordinator.viewController = controller
        return coordinator
    }()
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        navigationController.pushViewController(controller, animated: false)
    }
    
    func testPerformSelectBank_ShouldPopViewController() throws {
        coordinator.perform(action: .selectBank(PPBank(dictionary: ["code": "1", "name": "teste"])))
        XCTAssertTrue(navigationController.isPopViewControllerCalled)
    }
}
