import XCTest
@testable import PicPay

final class RegisterBankAccountSplashViewModelTests: XCTestCase {
    private let spy = RegisterBankAccountSplashPresenterMock()
    private lazy var sut = RegisterBankAccountSplashViewModel(presenter: spy)
    
    func testFetchInstructionsShouldDisplayInstructions() {
        sut.fetchInstructions()
        
        XCTAssertTrue(spy.displayInstructionsCalled)
    }
    
    func testRegisterBankAccountCallShouldCallDidNextStepActionWithBankListAction() {
        sut.registerBankAccount()
        
        XCTAssertTrue(spy.didNextStepActionCalled)
        XCTAssertEqual(spy.action, .bankList)
    }
}
