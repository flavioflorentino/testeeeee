@testable import PicPay

final class RegisterBankAccountSplashPresenterMock: RegisterBankAccountSplashPresenting {
    var displayInstructionsCalled = false
    var didNextStepActionCalled = false
    
    var action: RegisterBankAccountSplashAction?
    
    var viewController: RegisterBankAccountSplashDisplay?
    
    func displayInstructions() {
        displayInstructionsCalled = true
    }
    
    func didNextStep(action: RegisterBankAccountSplashAction) {
        self.didNextStepActionCalled = true
        self.action = action
    }
}
