@testable import PicPay

final class RegisterBankAccountSplashViewModelMock: RegisterBankAccountSplashViewModelInputs {
    var fetchInstructionsCalled = false
    var registerBankAccountCalled = false
    
    func fetchInstructions() {
        self.fetchInstructionsCalled = true
    }
    
    func registerBankAccount() {
        registerBankAccountCalled = true
    }
}
