@testable import PicPay

final class RegisterBankAccountSplashCoordinatorMock: RegisterBankAccountSplashCoordinating {
    var performActionCalled = false
    
    var action: RegisterBankAccountSplashAction?
    
    var viewController: UIViewController?
    
    func perform(action: RegisterBankAccountSplashAction) {
        self.performActionCalled = true
        self.action = action
    }
}
