import UIKit
@testable import PicPay

final class RegisterBankAccountSplashViewControllerMock: UIViewController, RegisterBankAccountSplashDisplay {
    var displayInstructionsCalled = false
    
    var headlineText: String?
    var captionText: String?
    var illustrativeImage: ImageAsset.Image?
    
    func displayInstructions(headlineText: String?, captionText: String, illustrativeImage: ImageAsset.Image) {
        self.displayInstructionsCalled = true
        self.headlineText = headlineText
        self.captionText = captionText
        self.illustrativeImage = illustrativeImage
    }
}
