import XCTest
@testable import PicPay

final class RegisterBankAccountSplashViewControllerTests: XCTestCase {
    private lazy var sut = RegisterBankAccountSplashViewController(viewModel: spy)
    private let spy = RegisterBankAccountSplashViewModelMock()
    private let window = UIWindow(frame: CGRect(origin: .zero, size: CGSize(width: 375, height: 812)))
    
    override func setUp() {
        super.setUp()
        loadView()
    }

    func loadView() {
        window.addSubview(sut.view)
        RunLoop.main.run(until: Date())
    }
    
    func testFetchInstructionsCalledWhenViewDidLoad() {
        XCTAssertTrue(spy.fetchInstructionsCalled)
    }
    
    func testRegisterBankAccountCalledWhenRegisterBankAccountButtonIsTapped() {
        sut.registerBankAccountButtonTapped()
        
        XCTAssertTrue(spy.registerBankAccountCalled)
    }
}
