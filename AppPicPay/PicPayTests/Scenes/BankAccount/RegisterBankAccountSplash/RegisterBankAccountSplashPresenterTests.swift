import XCTest
@testable import PicPay

final class RegisterBankAccountSplashPresenterTests: XCTestCase {
    private let coordinatorSpy = RegisterBankAccountSplashCoordinatorMock()
    private let spy = RegisterBankAccountSplashViewControllerMock()
    private lazy var sut: RegisterBankAccountSplashPresenter = {
        let presenter = RegisterBankAccountSplashPresenter(coordinator: coordinatorSpy)
        presenter.viewController = spy
        return presenter
    }()

    func testLoadRegisterInstructionsWhenReceiveSuccessFromServiceDisplayInstructions() {
        sut.displayInstructions()
        
        XCTAssertTrue(spy.displayInstructionsCalled)
        XCTAssertEqual(spy.headlineText, BankAccountLocalizable.registerBankAccountHeadline.text)
        XCTAssertEqual(spy.captionText, BankAccountLocalizable.registerBankAccountCaption.text)
        XCTAssertEqual(spy.illustrativeImage, Assets.Ilustration.iluBankFt.image)
    }
    
    func testDidNextStepWithBankListActionCallShouldPerformBankListCoordinatorAction() {
        let action: RegisterBankAccountSplashAction = .bankList
        
        sut.didNextStep(action: action)
        
        XCTAssertTrue(coordinatorSpy.performActionCalled)
        XCTAssertEqual(coordinatorSpy.action, action)
    }
}
