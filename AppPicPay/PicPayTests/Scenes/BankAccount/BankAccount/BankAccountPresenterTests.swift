import XCTest
@testable import PicPay

final class BankAccountPresenterTests: XCTestCase {
    private let coordinator = BankAccountCoordinatorMock()
    private let spy = BankAccountDisplayMock()
    private lazy var sut: BankAccountPresenter = {
        let sut = BankAccountPresenter(coordinator: coordinator)
        sut.viewController = spy
        return sut
    }()
    
    // MARK: - Mocks
    
    private lazy var bankAccountMock: PPBankAccount = {
        let bank = PPBank()
        bank.name = "itaú"
        bank.imageUrl = "https://www.host.com/someImage.png"
        let bankAccount = PPBankAccount()
        bankAccount.agencyNumber = "1234"
        bankAccount.agencyDigit = "1"
        bankAccount.accountNumber = "12345"
        bankAccount.accountDigit = "1"
        bankAccount.bank = bank
        return bankAccount
    }()
    lazy var bankAccountDescription: String = {
        let agencyNumber = bankAccountMock.agencyNumber ?? ""
        let agencyDigit = bankAccountMock.agencyDigit ?? ""
        let accountNumber = bankAccountMock.accountNumber ?? ""
        let accountDigit = bankAccountMock.accountDigit ?? ""
        
        let bankAccountDescriptionFormat = BankAccountLocalizable.bankCellText.text
        let formattedBankAccount = String(format: bankAccountDescriptionFormat, agencyNumber, agencyDigit, accountNumber, accountDigit)
        return formattedBankAccount
    }()
    
    // MARK: - Tests
    
    func testDisplayBankAccountsShouldDisplayBankAccountItemWithFormattedValues() {
        let bankAccounts = [bankAccountMock]
        
        sut.display(bankAccounts: bankAccounts)
        
        XCTAssertTrue(spy.displayBankAccountsCalled)
        XCTAssertEqual(spy.bankAccountsSection?.header, BankAccountLocalizable.bankMyAccount.text)
        XCTAssertEqual(spy.bankAccountsSection?.items[0].bankAccount, bankAccountDescription)
        XCTAssertEqual(spy.bankAccountsSection?.items[0].bankLogoURL, bankAccountMock.bank?.imageUrl)
        XCTAssertEqual(spy.bankAccountsSection?.items[0].bankName, bankAccountMock.bank?.name)
    }
    
    func testDisplayRemovalConfirmationSheetForBankAtIndexShouldDisplayBankAccountItemWithFormattedValues() {
        let index = 0
        let bankAccount = bankAccountMock
        
        sut.displayRemovalConfirmationSheet(for: bankAccount, at: index)
        
        XCTAssertTrue(spy.displayRemovalConfirmationSheetForBankAccountAtIndexCalled)
        XCTAssertEqual(spy.bankAccount?.bankAccount, bankAccountDescription)
        XCTAssertEqual(spy.bankAccount?.bankLogoURL, bankAccountMock.bank?.imageUrl)
        XCTAssertEqual(spy.bankAccount?.bankName, bankAccountMock.bank?.name)
        XCTAssertEqual(spy.bankAccountIndex, index)
    }
    
    func testDisplayErrorShouldDisplayError() {
        let error = PicPayError(message: "Some error")
        
        sut.display(error: error)
        
        XCTAssertTrue(spy.displayErrorCalled)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
    }
    
    func testStartLoadingShouldStartLoadingView() {
        sut.startLoading()
        
        XCTAssertTrue(spy.startLoadingViewCalled)
    }
    
    func testStopLoadingShouldStopLoadingView() {
        sut.stopLoading()
        
        XCTAssertTrue(spy.stopLoadingViewCalled)
    }
    
    func testDidNextStepRegisterBankAccountSplashActionShouldPerformRegisterBankAccountSplashAction() {
        let action: BankAccountAction = .registerBankAccountSplash
        
        sut.didNextStep(action: action)
        
        XCTAssertTrue(coordinator.performActionCalled)
        XCTAssertEqual(coordinator.action, action)
    }
}
