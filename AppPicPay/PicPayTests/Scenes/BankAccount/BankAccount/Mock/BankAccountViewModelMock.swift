@testable import PicPay

final class BankAccountViewModelMock: BankAccountViewModelInputs {
    var fetchBankAccountCalled = false
    var requestRemovalConfirmationForBankAccountAtIndexCalled = false
    var removeBankAccountAtIndexCalled = false
    var changeBankAccountCalled = false
    
    var bankAccountIndex: Int?
    
    func fetchBankAccount() {
        self.fetchBankAccountCalled = true
    }
    
    func requestRemovalConfirmation(forBankAccountAt index: Int) {
        self.requestRemovalConfirmationForBankAccountAtIndexCalled = true
        self.bankAccountIndex = index
    }
    
    func removeBankAccount(at index: Int) {
        self.removeBankAccountAtIndexCalled = true
        self.bankAccountIndex = index
    }
    
    func changeBankAccount() {
        self.changeBankAccountCalled = true
    }
}
