@testable import PicPay

final class BankAccountPresenterMock: BankAccountPresenting {
    var displayBankAccountsCalled = false
    var displayRemovalConfirmationSheetForBankAccountAtIndexCalled = false
    var displayErrorCalled = false
    var didNextStepActionCalled = false
    var startLoadingCalled = false
    var stopLoadingCalled = false
    
    var bankAccounts: [PPBankAccount]?
    var bankAccount: PPBankAccount?
    var bankAccountIndex: Int?
    var error: Error?
    var action: BankAccountAction?
    
    var viewController: BankAccountDisplay?
    
    func display(bankAccounts: [PPBankAccount]) {
        self.displayBankAccountsCalled = true
        self.bankAccounts = bankAccounts
    }
    
    func displayRemovalConfirmationSheet(for bankAccount: PPBankAccount, at index: Int) {
        self.displayRemovalConfirmationSheetForBankAccountAtIndexCalled = true
        self.bankAccount = bankAccount
        self.bankAccountIndex = index
    }
    
    func display(error: Error) {
        self.displayErrorCalled = true
        self.error = error
    }
    
    func didNextStep(action: BankAccountAction) {
        self.didNextStepActionCalled = true
        self.action = action
    }
    
    func startLoading() {
        self.startLoadingCalled = true
    }
    
    func stopLoading() {
        self.stopLoadingCalled = true
    }
}
