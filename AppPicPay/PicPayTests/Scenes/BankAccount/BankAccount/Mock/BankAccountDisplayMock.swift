import UI
@testable import PicPay

final class BankAccountDisplayMock: BankAccountDisplay {
    var displayBankAccountsCalled = false
    var displayRemovalConfirmationSheetForBankAccountAtIndexCalled = false
    var displayErrorCalled = false
    var startLoadingViewCalled = false
    var stopLoadingViewCalled = false
    
    lazy var loadingView: LoadingView = LoadingView()
    
    var bankAccountsSection: Section<String, BankAccountItem>?
    var bankAccount: BankAccountItem?
    var bankAccountIndex: Int?
    var error: Error?

    
    func display(bankAccountsSection: Section<String, BankAccountItem>) {
        self.displayBankAccountsCalled = true
        self.bankAccountsSection = bankAccountsSection
    }
    
    func displayRemovalConfirmationSheet(for bankAccount: BankAccountItem, at index: Int) {
        self.displayRemovalConfirmationSheetForBankAccountAtIndexCalled = true
        self.bankAccount = bankAccount
        self.bankAccountIndex = index
    }
    
    func display(error: Error) {
        self.displayErrorCalled = true
        self.error = error
    }
    
    func startLoadingView() {
        self.startLoadingViewCalled = true
    }
    
    func stopLoadingView() {
        self.stopLoadingViewCalled = true
    }
}
