@testable import PicPay

final class BankAccountCoordinatorMock: BankAccountCoordinating {
    var performActionCalled = false
    
    var viewController: UIViewController?
    var action: BankAccountAction?
    
    func perform(action: BankAccountAction) {
        self.performActionCalled = true
        self.action = action
    }
}
