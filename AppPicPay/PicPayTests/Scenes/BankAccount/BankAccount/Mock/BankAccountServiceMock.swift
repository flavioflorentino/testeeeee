@testable import PicPay

final class BankAccountServiceMock: BankAccountServicing {
    var expectedResult: Result<Void, Error> = .success
    
    func remove(bankAccount: PPBankAccount, completion: ((Result<Void, Error>) -> Void)?) {
        completion?(expectedResult)
    }
}
