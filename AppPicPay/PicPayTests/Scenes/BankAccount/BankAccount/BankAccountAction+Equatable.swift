@testable import PicPay

extension BankAccountAction: Equatable {
    public static func == (lhs: BankAccountAction, rhs: BankAccountAction) -> Bool {
        switch (lhs, rhs) {
        case (.registerBankAccountSplash, .registerBankAccountSplash):
            return true
        case (.changeBankAccount, .changeBankAccount):
            return true
        default:
            return false
        }
    }
}
