import XCTest
@testable import PicPay

final class BankAccountViewModelTests: XCTestCase {
    private let spy = BankAccountPresenterMock()
    private let serviceMock = BankAccountServiceMock()
    private lazy var sut = BankAccountViewModel(service: serviceMock, presenter: spy, bankAccounts: [bankAccountMock])
    private lazy var bankAccountMock: PPBankAccount = {
        let bank = PPBank()
        bank.name = "itaú"
        let bankAccount = PPBankAccount()
        bankAccount.cpf = "123.456.789-01"
        bankAccount.accountDigit = "1"
        bankAccount.accountNumber = "12345"
        bankAccount.accountType = "c"
        bankAccount.agencyNumber = "1234"
        bankAccount.agencyDigit = "1"
        bankAccount.bank = bank
        return bankAccount
    }()
    
    func testFetchBankAccountsShouldDisplayBankAccounts() {
        sut.fetchBankAccount()
        
        XCTAssertTrue(spy.displayBankAccountsCalled)
        XCTAssertNotNil(spy.bankAccounts)
        XCTAssertEqual(spy.bankAccounts?.contains(bankAccountMock), true)
    }
    
    func testRequestRemovalConfirmationForBankAtIndexShouldDisplayRemovalConfirmationForBankAtIndex() {
        let index = 0
        
        sut.requestRemovalConfirmation(forBankAccountAt: index)
        
        XCTAssertTrue(spy.displayRemovalConfirmationSheetForBankAccountAtIndexCalled)
        XCTAssertEqual(spy.bankAccountIndex, index)
        XCTAssertEqual(spy.bankAccount?.cpf, bankAccountMock.cpf)
    }
    
    func testRemoveBankAccountAtIndexWhenReceiveSuccessFromServiceDidNextStepRegisterBankAccount() {
        let index = 0
        
        sut.removeBankAccount(at: index)
        
        XCTAssertTrue(spy.startLoadingCalled)
        XCTAssertTrue(spy.stopLoadingCalled)
        XCTAssertTrue(spy.didNextStepActionCalled)
        XCTAssertEqual(spy.action, .registerBankAccountSplash)
        XCTAssertFalse(spy.displayErrorCalled)
    }
    
    func testRemoveBankAccountAtIndexWhenReceiveFailureFromServiceDisplayReceivedError() {
        let index = 0
        let error = PicPayError(message: "Some error")
        serviceMock.expectedResult = .failure(error)
        
        sut.removeBankAccount(at: index)
        
        XCTAssertTrue(spy.startLoadingCalled)
        XCTAssertTrue(spy.stopLoadingCalled)
        XCTAssertTrue(spy.displayErrorCalled)
        XCTAssertEqual(spy.error?.localizedDescription, error.localizedDescription)
        XCTAssertFalse(spy.didNextStepActionCalled)
    }
    
    func testChangeBankAccountShouldCallDidNextStepChangeBankAccountAction() {
        sut.changeBankAccount()
        
        XCTAssertTrue(spy.didNextStepActionCalled)
        XCTAssertEqual(spy.action, .changeBankAccount(completion: nil))
    }
}
