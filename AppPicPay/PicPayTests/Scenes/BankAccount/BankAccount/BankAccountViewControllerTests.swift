import XCTest
@testable import PicPay

final class BankAccountViewControllerTests: XCTestCase {
    private let spy = BankAccountViewModelMock()
    private lazy var sut = BankAccountViewController(viewModel: spy)
    private let window = UIWindow(frame: CGRect(origin: .zero, size: CGSize(width: 375, height: 812)))
    
    override func setUp() {
        super.setUp()
        loadView()
    }

    func loadView() {
        window.addSubview(sut.view)
        RunLoop.main.run(until: Date())
    }
    
    func testFetchBankAccountWhenViewDidLoad() {
        XCTAssertTrue(spy.fetchBankAccountCalled)
    }
    
    func testRemoveBankAccountButtonTappedShouldRequestRemovalConfirmationForBankAccount() {
        sut.removeBankAccountButtonTapped()
        
        XCTAssertTrue(spy.requestRemovalConfirmationForBankAccountAtIndexCalled)
    }
    
    func testChangeBankAccountButtonTappedShouldCallChangeBankAccount() {
        sut.changeBankAccountButtonTapped()
        
        XCTAssertTrue(spy.changeBankAccountCalled)
    }
}
