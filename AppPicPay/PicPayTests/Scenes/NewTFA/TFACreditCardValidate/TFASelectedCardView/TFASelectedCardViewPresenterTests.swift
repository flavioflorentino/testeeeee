@testable import PicPay
import XCTest

final class TFASelectedCardViewPresenterTests: XCTestCase {
    private lazy var viewControllerSpy = TFASelectedCardViewDisplaySpy()

    private lazy var sut: TFASelectedCardViewPresenter = {
        let presenter = TFASelectedCardViewPresenter()
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testConfigureView_ShouldCallConfigureViewOnDisplay() {
        let card = TFACreditCardData(id: 1, flagName: "visa", lastFour: "1234", urlFlagLogoImage: "")
        sut.configureView(with: card)
        
        XCTAssertEqual(viewControllerSpy.configureViewCallCount, 1)
    }
}

private final class TFASelectedCardViewDisplaySpy: TFASelectedCardViewDisplay {
    private(set) var configureViewCallCount = 0
    
    func configureView(with logoUrl: URL?, cardTitle: String, placeholderImage: UIImage) {
        configureViewCallCount += 1
    }
}
