@testable import PicPay
import XCTest

final class TFASelectedCardViewViewModelTests: XCTestCase {
    private lazy var spy = TFASelectedCardViewPresenterSpy()
    
    private lazy var sut: TFASelectedCardViewViewModel = {
        let viewModel = TFASelectedCardViewViewModel(
            presenter: spy,
            card: TFACreditCardData(id: 1, flagName: "visa", lastFour: "1234", urlFlagLogoImage: "")
        )
        return viewModel
    }()
    
    func testConfigure_ShouldCallConfigureViewOnPresenter() {
        sut.configure()
        
        XCTAssertEqual(spy.configureViewCallCount, 1)
    }
}

private final class TFASelectedCardViewPresenterSpy: TFASelectedCardViewPresenting {
    var viewController: TFASelectedCardViewDisplay?
    private(set) var configureViewCallCount = 0
    
    func configureView(with card: TFACreditCardData) {
        configureViewCallCount += 1
    }
}
