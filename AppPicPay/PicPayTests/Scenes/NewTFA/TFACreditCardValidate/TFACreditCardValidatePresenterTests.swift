@testable import PicPay
import XCTest

final class TFACreditCardValidatePresenterTests: XCTestCase {
    private lazy var coordinatorSpy = TFACreditCardValidateCoordinatorSpy()
    private lazy var viewControllerSpy = TFACreditCardValidateDisplaySpy()
    
    private lazy var sut: TFACreditCardValidatePresenter = {
        let presenter = TFACreditCardValidatePresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDidNextStep_WhenActionIsValidateCard_ShouldPerformActionOnCoordinator() throws {
        sut.didNextStep(action: .validateCard(nextStep: .final))
        
        XCTAssertEqual(coordinatorSpy.didPerformActionCallCount, 1)
        XCTAssertNotNil(coordinatorSpy.actionPerformed)
    }
    
    func testPresentCardInfo_WhenCardFlagIsVisa_ShouldCallDisplayCardInfoWithVisaFlag(){
        let card = TFACreditCardData(id: 1, flagName: "visa", lastFour: "1234", urlFlagLogoImage: "")
        
        sut.presentCardInfo(card)
        
        XCTAssertEqual(viewControllerSpy.displayCardInfoCallCount, 1)
        XCTAssertEqual(viewControllerSpy.displayCardInfoFlag, "visa")
    }
    
    func testPresentTextFieldSettings_ShouldCallDisplayTextFieldSettings() {
        let cardNumberSetting = TextfieldSetting(placeholder: "", text: "", rule: nil, mask: nil)
        let dueDateSetting = TextfieldSetting(placeholder: "", text: "", rule: nil, mask: nil)
        let settings = TFACreditCardValidateTextFieldSettings(cardNumber: cardNumberSetting, dueDate: dueDateSetting)
        
        sut.presentTextFieldSettings(settings)
        
        XCTAssertEqual(viewControllerSpy.displayTextFieldSettingsCallCount, 1)
    }
    
    func testPresentCardNumberFieldMaskedText_WhenMaskIsValid_ShouldDisplayCardNumberFieldMaskedTextWithoutError() {
        let mask = "0000 0000 0000 0000 0000"
        let cardText = "1234567812345678"
        let expectedMaskedText = "1234 5678 1234 5678"
        sut.presentCardNumberFieldMaskedText(withMask: mask, text: cardText, errorMessage: nil)
        
        XCTAssertEqual(viewControllerSpy.displayCardNumberFieldMaskedTextCallCount, 1)
        XCTAssertEqual(viewControllerSpy.cardNumberMaskedText, expectedMaskedText)
        XCTAssertNil(viewControllerSpy.cardNumberMaskTextError)
    }
    
    func testPresentCardNumberFieldMaskedText_WhenMaskIsInvalid_ShouldDisplayCardNumberFieldWithSameText() {
        let mask = "AAAA"
        let cardText = "1234567812345678"
        let expecteText = ""
        sut.presentCardNumberFieldMaskedText(withMask: mask, text: cardText, errorMessage: nil)
        
        XCTAssertEqual(viewControllerSpy.displayCardNumberFieldMaskedTextCallCount, 1)
        XCTAssertEqual(viewControllerSpy.cardNumberMaskedText, expecteText)
        XCTAssertNil(viewControllerSpy.cardNumberMaskTextError)
    }
    
    func testPresentCardNumberFieldMaskedText_WhenMaskIsNil_ShouldDisplayCardNumberFieldWithSameText() {
        let cardText = "123456"
        let expecteText = "123456"
        sut.presentCardNumberFieldMaskedText(withMask: nil, text: cardText, errorMessage: nil)
        
        XCTAssertEqual(viewControllerSpy.displayCardNumberFieldMaskedTextCallCount, 1)
        XCTAssertEqual(viewControllerSpy.cardNumberMaskedText, expecteText)
        XCTAssertNil(viewControllerSpy.cardNumberMaskTextError)
    }
    
    func testPresentCardNumberFieldMaskedText_WhenErrorMessageIsNotNil_ShouldDisplayCardNumberFieldWithErrorMessage() {
        let cardText = "123456"
        let expecteText = "123456"
        let expectedErrorMessage = "Campo obrigátorio"
        sut.presentCardNumberFieldMaskedText(withMask: nil, text: cardText, errorMessage: expectedErrorMessage)
        
        XCTAssertEqual(viewControllerSpy.displayCardNumberFieldMaskedTextCallCount, 1)
        XCTAssertEqual(viewControllerSpy.cardNumberMaskedText, expecteText)
        XCTAssertEqual(viewControllerSpy.cardNumberMaskTextError, expectedErrorMessage)
    }
    
    func testPresentDueDateFieldMaskedText_WhenMaskIsValid_ShouldDisplayDueDateFieldMaskedTextWithoutError() {
        let mask = "00/00"
        let dueDateText = "1122"
        let expectedMaskedText = "11/22"
        sut.presentDueDateFieldMaskedText(withMask: mask, text: dueDateText, errorMessage: nil)
        
        XCTAssertEqual(viewControllerSpy.displayDueDateFieldMaskedTextCallCount, 1)
        XCTAssertEqual(viewControllerSpy.dueDateMaskedText, expectedMaskedText)
        XCTAssertNil(viewControllerSpy.dueDateMaskTextError)
    }
    
    func testShouldEnableContinue_WhenEnableIsTrue_ShouldDisplayContinueButtonEnabled() {
        sut.shouldEnableContinue(true)
        
        XCTAssertEqual(viewControllerSpy.displayContinueButtonEnabledCallCount, 1)
        XCTAssertTrue(viewControllerSpy.continueButtonEnabled)
    }
    
    func testPresentLoading_WhenLoadingIsTrue_ShouldDisplayLoading() {
        sut.presentLoading(true)
        
        XCTAssertEqual(viewControllerSpy.displayLoadingCallCount, 1)
        XCTAssertTrue(viewControllerSpy.isLoading)
    }
    
    func testUpdateContinueButtonRemainingTime_ShouldDisplayContinueButtonWithCorrectRemainingTimeText() {
        sut.updateContinueButtonRemainingTime(10)
        let expectedContinueButtonText = "Aguarde 00:10"
        
        XCTAssertEqual(viewControllerSpy.displayContinueButtonWithRemainingTimeCallCount, 1)
        XCTAssertEqual(viewControllerSpy.continueButtonRemainingTimeText, expectedContinueButtonText)
    }
    
    func testPresentValidationError_WhenErrorIsServerError_ShouldDisplayGenericErrorAlert() {
        sut.presentGenericErrorAlert(Alert(title: "Ops!", text: "Ocorreu um erro inesperado"))
        XCTAssertEqual(viewControllerSpy.displayGenericErrorAlertCallCount, 1)
    }
    
    func testPresentValidationError_WhenErrorHasRetryTime_ShouldDisplayAttemptLimitErrorAlert() {
        sut.presentAttemptLimitErrorAlert(
            Alert(title: "Limite excedido", text: "Você atingiu o limite de tentativas"), retryTime: 10
        )
        
        XCTAssertEqual(viewControllerSpy.displayAttemptLimitErrorAlertCallCount, 1)
        XCTAssertEqual(viewControllerSpy.attemptLimitErrorRetryTime, 10)
    }
    
    func testPresentValidationError_WhenErrorIsInvalidData_ShoulDisplayValidationErrorAlert() {
        sut.presentValidationErrorAlert(Alert(title: "Ops!", text: "Dados inválidos"))
        XCTAssertEqual(viewControllerSpy.displayValidationErrorCallCount, 1)
    }
}

private final class TFACreditCardValidateCoordinatorSpy: TFACreditCardValidateCoordinating {
    var viewController: UIViewController?
    var delegate: TFACreditCardValidateCoordinatorDelegate?
    private(set) var didPerformActionCallCount = 0
    private(set) var actionPerformed: TFACreditCardValidateAction?
    
    func perform(action: TFACreditCardValidateAction) {
        didPerformActionCallCount += 1
        actionPerformed = action
    }
}

private final class TFACreditCardValidateDisplaySpy: TFACreditCardValidateDisplay {
    private(set) var displayCardInfoCallCount = 0
    private(set) var displayCardInfoFlag = ""
    private(set) var displayTextFieldSettingsCallCount = 0
    private(set) var displayCardNumberFieldMaskedTextCallCount = 0
    private(set) var cardNumberMaskedText = ""
    private(set) var cardNumberMaskTextError: String?
    private(set) var displayDueDateFieldMaskedTextCallCount = 0
    private(set) var dueDateMaskedText = ""
    private(set) var dueDateMaskTextError: String?
    private(set) var displayContinueButtonEnabledCallCount = 0
    private(set) var continueButtonEnabled = false
    private(set) var displayLoadingCallCount = 0
    private(set) var isLoading = false
    private(set) var displayContinueButtonWithRemainingTimeCallCount = 0
    private(set) var continueButtonRemainingTimeText = ""
    private(set) var displayGenericErrorAlertCallCount = 0
    private(set) var displayAttemptLimitErrorAlertCallCount = 0
    private(set) var attemptLimitErrorRetryTime = 0
    private(set) var displayValidationErrorCallCount = 0
    
    func displayCardInfo(with card: TFACreditCardData) {
        displayCardInfoCallCount += 1
        displayCardInfoFlag = card.flagName
    }
    
    func displayCardNumberFieldMaskedTextWith(_ text: String, errorMessage: String?) {
        displayCardNumberFieldMaskedTextCallCount += 1
        cardNumberMaskedText = text
        cardNumberMaskTextError = errorMessage
    }
    
    func displayDueDateFieldMaskedText(_ text: String, errorMessage: String?) {
        displayDueDateFieldMaskedTextCallCount += 1
        dueDateMaskedText = text
        dueDateMaskTextError = errorMessage
    }
    
    func displayContinueButtonEnabled(_ enabled: Bool) {
        displayContinueButtonEnabledCallCount += 1
        continueButtonEnabled = enabled
    }
    
    func displayTextFieldSettings(_ settings: TFACreditCardValidateTextFieldSettings) {
        displayTextFieldSettingsCallCount += 1
    }
    
    func displayLoading(_ loading: Bool) {
        displayLoadingCallCount += 1
        isLoading = loading
    }
    
    func displayGenericErrorAlert(_ alert: Alert) {
        displayGenericErrorAlertCallCount += 1
    }
    
    func displayValidationErrorAlert(_ alert: Alert) {
        displayValidationErrorCallCount += 1
    }
    
    func displayAttemptLimitErrorAlert(_ alert: Alert, retryTime: Int) {
        displayAttemptLimitErrorAlertCallCount += 1
        attemptLimitErrorRetryTime = retryTime
    }
    
    func displayContinueButton(with currentRemainingTime: String) {
        displayContinueButtonWithRemainingTimeCallCount += 1
        continueButtonRemainingTimeText = currentRemainingTime
    }
}
