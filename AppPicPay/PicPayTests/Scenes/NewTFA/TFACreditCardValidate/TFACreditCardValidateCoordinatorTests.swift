@testable import PicPay
import XCTest

final class TFACreditCardValidateCoordinatorTests: XCTestCase {
    private let navController = NavigationControllerMock(rootViewController: UIViewController())
    private lazy var delegateSpy = TFACreditCardValidateCoordinatorDelegateSpy()
    
    private lazy var sut: TFACreditCardValidateCoordinator = {
        let coordinator = TFACreditCardValidateCoordinator()
        coordinator.viewController = navController.topViewController
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerformAction_WhenActionIsValidateCard_ShouldCallDelegateDidValidateCard() {
        sut.perform(action: .validateCard(nextStep: .final))
        
        XCTAssertEqual(delegateSpy.didValidateCardCallCount, 1)
        XCTAssertEqual(delegateSpy.validationNextStep, .final)
    }
}

private final class TFACreditCardValidateCoordinatorDelegateSpy: TFACreditCardValidateCoordinatorDelegate {
    private(set) var didValidateCardCallCount = 0
    private(set) var validationNextStep: TFAStep?
    
    func didValidateCard(nextStep: TFAStep) {
        didValidateCardCallCount += 1
        validationNextStep = nextStep
    }
}
