import Core
@testable import PicPay
import Validator
import XCTest

final class TFACreditCardValidateViewModelTests: XCTestCase {
    private lazy var mock = TFACreditCardValidateServiceMock()
    private lazy var spy = TFACreditCardValidatePresenterSpy()
    private lazy var mockDependencies = DependencyContainerMock()
    
    private lazy var sut: TFACreditCardValidateViewModel = {
        let tfaRequestData = TFARequestData(flow: .creditCard, step: .creditCard, id: "1")
        let creditCard = TFACreditCardData(id: 1, flagName: "visa", lastFour: "1234", urlFlagLogoImage: "")
        let viewModel = TFACreditCardValidateViewModel(
            service: mock,
            presenter: spy,
            selectedCard: creditCard,
            tfaRequestData: tfaRequestData,
            dependencies: mockDependencies
        )
        
        return viewModel
    }()
    
    func testConfigureView_ShouldPresentCardInfoAndConfigureSettingsForFields() {
        sut.configureView()
        
        XCTAssertEqual(spy.presentCardInfoCallCount, 1)
        XCTAssertEqual(spy.presentTextFieldSettingsCallCount, 1)
    }
    
    func testCheckCardNumberField_WhenValidationIsValid_ShouldPresentNoErrorMessage() {
        sut.checkCardNumberFieldWith(mask: "", text: "123456", validationResult: .valid)
        
        XCTAssertEqual(spy.presentCardNumberFieldMaskedTextCallCount, 1)
        XCTAssertNil(spy.cardNumberFieldErrorMessage)
    }
    
    func testCheckDueDateField_WhenValidationIsValid_ShouldPresentNoErrorMessage() {
        sut.checkDueDateFieldWith(mask: "", text: "123456", validationResult: .valid)
        
        XCTAssertEqual(spy.presentDueDateFieldMaskedTextCallCount, 1)
        XCTAssertNil(spy.dueDateFieldErrorMessage)
    }
    
    func testCheckDueDateField_WhenValidationIsInValid_ShouldPresentErrorMessage() {
        sut.checkDueDateFieldWith(
            mask: "",
            text: "123456",
            validationResult: .invalid([TFACreditCardFieldCheckValidationError.invalidDuedate])
        )
        
        XCTAssertEqual(spy.presentDueDateFieldMaskedTextCallCount, 1)
        XCTAssertNotNil(spy.dueDateFieldErrorMessage)
        XCTAssertEqual(spy.dueDateFieldErrorMessage, TFACreditCardFieldCheckValidationError.invalidDuedate.message)
    }
    
    func testCheckFieldsAndEnableContinue_WhenValidationIsValid_ShouldEnableContinue() {
        let cardNumberValid = ValidationResult.valid
        let dueDateValid = ValidationResult.valid
        
        sut.checkFieldsAndEnableContinue(cardNumberValidationResult: cardNumberValid, dueDateValidationResult: dueDateValid)
        
        XCTAssertEqual(spy.shouldEnableContinueButtonCallCount, 1)
        XCTAssertTrue(spy.continueEnabled)
    }
    
    func testValidateCreditCard_ShouldPresentLoadingTwice() {
        sut.validateCreditCardWith(cardNumber: "", dueDate: "")
        
        XCTAssertEqual(spy.presentLoadingCallCount, 2)
    }
    
    func testValidateCreditCard_WhenServiceIsSuccess_ShouldPerformDidNextStep() {
        mock.isSuccess = true
        
        sut.validateCreditCardWith(cardNumber: "123456789", dueDate: "10/22")
        
        XCTAssertEqual(spy.didNextStepCallCount, 1)
    }
    
    func testValidateCreditCard_WhenServiceIsFailure_ShouldPresentGenericError() {
        mock.isSuccess = false
        
        sut.validateCreditCardWith(cardNumber: "123456789", dueDate: "10/22")
        
        XCTAssertEqual(spy.presentGenericErrorCallCount, 1)
    }
    
    func testValidateCreditCard_WhenServiceHasValidationError_ShouldPresentValidationError() {
        mock.isSuccess = false
        mock.isValidationError = true
        
        sut.validateCreditCardWith(cardNumber: "123456789", dueDate: "10/22")
        
        XCTAssertEqual(spy.presentValidationErrorCallCount, 1)
    }
    
    func testValidateCreditCard_WhenServiceHasAttempLimitError_ShouldPresentAttempLimitError() {
        mock.isSuccess = false
        mock.isAttemptLimitError = true
        
        sut.validateCreditCardWith(cardNumber: "123456789", dueDate: "10/22")
        
        XCTAssertEqual(spy.presentAttemptLimitErrorCallCount, 1)
    }
    
    func testStartTimerForNewValidation_WhenTimerFinish_ShouldEnableContinueButton() {
        sut.startTimerForNewValidation(retryTime: 3)
        
        expectation(waitForCondition: self.spy.remainingTimeForNewRequest <= 0)
        expectation(waitForCondition: self.spy.continueEnabled)
        
        waitForExpectations(timeout: 5.0)
    }
    
    func testStartTimerForNewValidation_WhenTimerIsNil_ShouldEnableContinueButton() {
        let tfaRequestData = TFARequestData(flow: .creditCard, step: .creditCard, id: "1")
        let creditCard = TFACreditCardData(id: 1, flagName: "visa", lastFour: "1234", urlFlagLogoImage: "")
        let viewModel = TFACreditCardValidateViewModel(
            service: mock,
            presenter: spy,
            selectedCard: creditCard,
            tfaRequestData: tfaRequestData,
            dependencies: mockDependencies,
            timer: nil
        )
        
        viewModel.startTimerForNewValidation(retryTime: 3)
        expectation(waitForCondition: self.spy.remainingTimeForNewRequest <= 0)
        expectation(waitForCondition: self.spy.continueEnabled)
        
        waitForExpectations(timeout: 5.0)
    }
}

private final class TFACreditCardValidateServiceMock: TFACreditCardValidateServicing {
    var isSuccess = false
    var isValidationError = false
    var isAttemptLimitError = false
    
    func validatePersonalData(data: TFACreditCardValidateData, tfaRequestData: TFARequestData, completion: @escaping (Result<TFAValidationResponse, ApiError>) -> Void) {
        if isSuccess {
            completion(.success(TFAValidationResponse(nextStep: .final)))
        } else if isValidationError {
            completion(.failure(.unauthorized(body: TFAModelsHelper().invalidDataErrorJson())))
        } else if isAttemptLimitError {
            completion(.failure(.tooManyRequests(body: TFAModelsHelper().attempLimitErrorJson())))
        } else {
            completion(.failure(.serverError))
        }
    }
}

private final class TFACreditCardValidatePresenterSpy: TFACreditCardValidatePresenting {
    var viewController: TFACreditCardValidateDisplay?
    private(set) var presentCardInfoCallCount = 0
    private(set) var presentTextFieldSettingsCallCount = 0
    private(set) var presentCardNumberFieldMaskedTextCallCount = 0
    private(set) var cardNumberFieldErrorMessage: String?
    private(set) var presentDueDateFieldMaskedTextCallCount = 0
    private(set) var dueDateFieldErrorMessage: String?
    private(set) var shouldEnableContinueButtonCallCount = 0
    private(set) var continueEnabled = false
    private(set) var presentLoadingCallCount = 0
    private(set) var didNextStepCallCount = 0
    private(set) var presentGenericErrorCallCount = 0
    private(set) var presentValidationErrorCallCount = 0
    private(set) var presentAttemptLimitErrorCallCount = 0
    private(set) var remainingTimeForNewRequest = 180
    private(set) var presentedAlert: PicPay.Alert?
    
    func didNextStep(action: TFACreditCardValidateAction) {
        didNextStepCallCount += 1
    }
    
    func presentCardInfo(_ card: TFACreditCardData) {
        presentCardInfoCallCount += 1
    }
    
    func presentCardNumberFieldMaskedText(withMask mask: String?, text: String, errorMessage: String?) {
        presentCardNumberFieldMaskedTextCallCount += 1
        cardNumberFieldErrorMessage = errorMessage
    }
    
    func presentDueDateFieldMaskedText(withMask mask: String?, text: String, errorMessage: String?) {
        presentDueDateFieldMaskedTextCallCount += 1
        dueDateFieldErrorMessage = errorMessage
    }
    
    func presentTextFieldSettings(_ settings: TFACreditCardValidateTextFieldSettings) {
        presentTextFieldSettingsCallCount += 1
    }
    
    func shouldEnableContinue(_ enable: Bool) {
        shouldEnableContinueButtonCallCount += 1
        continueEnabled = enable
    }
    
    func presentLoading(_ loading: Bool) {
        presentLoadingCallCount += 1
    }
    
    func presentGenericErrorAlert(_ alert: PicPay.Alert) {
        presentGenericErrorCallCount += 1
    }
    
    func presentValidationErrorAlert(_ alert: PicPay.Alert) {
        presentValidationErrorCallCount += 1
        presentedAlert = alert
    }
    
    func presentAttemptLimitErrorAlert(_ alert: PicPay.Alert, retryTime: Int) {
        presentAttemptLimitErrorCallCount += 1
    }
    
    func updateContinueButtonRemainingTime(_ remainingTime: TimeInterval) {
        remainingTimeForNewRequest = Int(remainingTime)
    }
}
