import AnalyticsModule
import Core
@testable import PicPay
import XCTest
import UI

private final class TFAPersonalDataCheckServiceMock: TFAPersonalDataCheckServicing {
    var personalDataExpectedResult: Result<TFAPersonalDataInfo, ApiError>?
    var validatePersonalDataExpectedResult: Result<TFAValidationResponse, ApiError>?
    
    func getPersonalData(tfaRequestData: TFARequestData, completion: @escaping (Result<TFAPersonalDataInfo, ApiError>) -> Void) {
        guard let expectedResult = personalDataExpectedResult else {
            return
        }
        completion(expectedResult)
    }
    
    func validatePersonalData(
        data: TFAPersonalDataCheckInfo,
        tfaRequestData: TFARequestData,
        completion: @escaping (Result<TFAValidationResponse, ApiError>) -> Void
    ) {
        guard let expectedResult = validatePersonalDataExpectedResult else {
            return
        }
        completion(expectedResult)
    }
}

private final class TFAPersonalDataCheckPresenterSpy: TFAPersonalDataCheckPresenting {
    var viewController: TFAPersonalDataCheckDisplay?
    
    private(set) var presentLoadingCallCount = 0
    private(set) var isLoading = false
    private(set) var shouldEnableContinueButtonCallCount = 0
    private(set) var continueButtonEnabled = false
    private(set) var didValidatePersonalDataCallCount = 0
    private(set) var personalDataValidationType: TFAType?
    private(set) var personalDataValidationNextStep: TFAStep?
    private(set) var presentContinueButtonUpdatedCallCount = 0
    private(set) var continueButtonRemainingTime = 0
    private(set) var presentValidationInfoErrorCallCount = 0
    private(set) var validationInfoError: RequestError?
    private(set) var presentAttemptLimitErrorCallCount = 0
    private(set) var attemptLimitError: RequestError?
    private(set) var attemptLimitErrorRetryTime = 0
    private(set) var attemptLimitErrorFlow: TFAType?
    private(set) var presentGenericErrorCallCount = 0
    private(set) var validationErrorMessage: String?
    private(set) var updateEmailFieldErrorCallCount = 0
    private(set) var updateDocumentFieldErrorCallCount = 0
    private(set) var updateMotherNameFieldErrorCallCount = 0
    private(set) var updateBirthdateFieldErrorCallCount = 0
    private(set) var updateEmailHintCallCount = 0
    private(set) var emailHint: String?
    
    func presentLoading(_ loading: Bool) {
        presentLoadingCallCount += 1
        isLoading = loading
    }
    
    func shouldEnableContinueButton(enable: Bool) {
        shouldEnableContinueButtonCallCount += 1
        continueButtonEnabled = enable
    }
    
    func didValidatePersonalData(with type: TFAType, nextStep: TFAStep) {
        didValidatePersonalDataCallCount += 1
        personalDataValidationType = type
        personalDataValidationNextStep = nextStep
    }
    
    func presentContinueButtonUpdated(with remainingTime: Int) {
        presentContinueButtonUpdatedCallCount += 1
        continueButtonRemainingTime = remainingTime
        continueButtonEnabled = remainingTime <= 0
    }
    
    func presentValidationInfoError(tfaError: RequestError) {
        presentValidationInfoErrorCallCount += 1
        validationInfoError = tfaError
    }
    
    func presentAttempLimitError(tfaError: RequestError, retryTime: Int, tfaType: TFAType) {
        presentAttemptLimitErrorCallCount += 1
        attemptLimitError = tfaError
        attemptLimitErrorRetryTime = retryTime
        attemptLimitErrorFlow = tfaType
    }
    
    func presentGenericError() {
        presentGenericErrorCallCount += 1
    }
    
    func updateEmailHint(with hint: String) {
        updateEmailHintCallCount += 1
        emailHint = hint
    }
    
    func updateEmailField(withError message: String?) {
        updateEmailFieldErrorCallCount += 1
        validationErrorMessage = message
    }
    
    func updateDocumentField(withError message: String?) {
        updateDocumentFieldErrorCallCount += 1
        validationErrorMessage = message
    }
    
    func updateBirthdateField(withError message: String?) {
        updateBirthdateFieldErrorCallCount += 1
        validationErrorMessage = message
    }
    
    func updateMotherNameField(withError message: String?) {
        updateMotherNameFieldErrorCallCount += 1
        validationErrorMessage = message
    }
}

private class TFATimerProtocolSpy: TFATimerProtocol {
    weak var delegate: TFATimerDelegate?
    private(set) var callStartCount = 0
    private(set) var callStopCount = 0

    func start() {
        callStartCount += 1
    }

    func stop() {
        callStopCount += 1
    }
}

final class TFAPersonalDataCheckViewModelTests: XCTestCase {
    private let analytics = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(analytics)
    private lazy var serviceMock = TFAPersonalDataCheckServiceMock()
    private lazy var presenterSpy = TFAPersonalDataCheckPresenterSpy()
    private let timerSpy = TFATimerProtocolSpy()
    
    private lazy var sut: TFAPersonalDataCheckViewModel = {
        let viewModel = tfaPersonalDataCheckViewModel(withID: "123", tfaType: .email)
        return viewModel
    }()
    
    func testDidLoadView_ShouldLogPersonalDataEvent() {
        sut.didLoadView()

        let expectedEvent = TFAEvents.validatePersonalData.event()
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testEmailBeginEditing_WhenPersonalDataIsReceived_ShouldPresentEmailHint() {
        serviceMock.personalDataExpectedResult = .success(
            TFAPersonalDataInfo(
                consumerData: ConsumerData(
                    email: "jos*****@gmail.com"
                )
            )
        )
        sut.getPersonalDataInfo()
        sut.emailBeginEditing()
        
        XCTAssertEqual(presenterSpy.updateEmailHintCallCount, 1)
        XCTAssertEqual(presenterSpy.emailHint, "jos*****@gmail.com")
    }
    
    func testEmailBeginEditing_WhenEmailHintIsNil_ShouldNotPresentEmailHint() {
        sut.emailBeginEditing()
        
        XCTAssertEqual(presenterSpy.updateEmailHintCallCount, 0)
        XCTAssertNil(presenterSpy.emailHint)
    }
    
    func testCheckMotherName_WhenNameIsInvalid_ShouldPresentErrorMessage() {
        sut.checkMotherName(with: "")
        
        XCTAssertEqual(presenterSpy.updateMotherNameFieldErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.validationErrorMessage, TFAPersonalDataFieldCheckValidationError.motherNameRequired.message)
    }
    
    func testCheckMotherName_WhenNameIsValid_ShouldNotPresentErrorMessage() {
        sut.checkMotherName(with: "Maria")
        
        XCTAssertEqual(presenterSpy.updateMotherNameFieldErrorCallCount, 1)
        XCTAssertNil(presenterSpy.validationErrorMessage)
    }
    
    func testCheckEmail_WhenEmailIsInvalid_ShoulPresentErrorMessage() {
        sut.checkEmail(with: "jose@")
        
        XCTAssertEqual(presenterSpy.updateEmailFieldErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.validationErrorMessage, TFAPersonalDataFieldCheckValidationError.invalidEmail.message)
    }
    
    func testCheckEmail_WhenEmailIsValid_ShouldNotPresentErrorMessage() {
        sut.checkEmail(with: "jose@mail.com")
        
        XCTAssertEqual(presenterSpy.updateEmailFieldErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.validationErrorMessage, nil)
    }
    
    func testCheckDocument_WhenDocumentIsInvalid_ShoulPresentErrorMessage() {
        sut.checkDocument(with: "11111111111")
        
        XCTAssertEqual(presenterSpy.updateDocumentFieldErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.validationErrorMessage, TFAPersonalDataFieldCheckValidationError.invalidCPFDocument.message)
    }
    
    func testCheckDocument_WhenDocumentIsValid_ShouldNotPresentErrorMessage() {
        sut.checkDocument(with: "199.213.820-63")
        
        XCTAssertEqual(presenterSpy.updateDocumentFieldErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.validationErrorMessage, nil)
    }
    
    func testCheckBirthday_WhenBirthdayIsEmpty_ShoulPresentRequiredErrorMessage() {
        sut.checkBirthday(with: "")
        
        XCTAssertEqual(presenterSpy.updateBirthdateFieldErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.validationErrorMessage, TFAPersonalDataFieldCheckValidationError.birthdateRequired.message)
    }
    
    func testCheckBirthday_WhenBirthdayLenghtIsInvalid_ShoulPresentInvalidErrorMessage() {
        sut.checkBirthday(with: "11/")
        
        XCTAssertEqual(presenterSpy.updateBirthdateFieldErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.validationErrorMessage, TFAPersonalDataFieldCheckValidationError.invalidBirthdate.message)
    }
    
    func testCheckBirthday_WhenBirthdayLenghtAndDateAreValid_ShoulNotPresentErrorMessage() {
        sut.checkBirthday(with: "11/11/2000")
        
        XCTAssertEqual(presenterSpy.updateBirthdateFieldErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.validationErrorMessage, nil)
    }
    
    func testCheckBirthday_WhenBirthdayLenghtIsValidButDateIsInvalid_ShoulPresentErrorMessage() {
        sut.checkBirthday(with: "33/15/2000")
        
        XCTAssertEqual(presenterSpy.updateBirthdateFieldErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.validationErrorMessage, TFAPersonalDataFieldCheckValidationError.invalidBirthdate.message)
    }
    
    func testCheckEmail_WhenAllFieldsAreValid_ShouldEnableContinue() {
        sut.checkDocument(with: "199.213.820-63")
        sut.checkBirthday(with: "11/11/2000")
        sut.checkMotherName(with: "Maria")
        sut.checkEmail(with: "jose@mail.com")
        
        XCTAssertEqual(presenterSpy.shouldEnableContinueButtonCallCount, 4)
        XCTAssertTrue(presenterSpy.continueButtonEnabled)
    }
    
    func testValidateForm_WhenServerHasError_ShouldPresentsGenericError() {
        serviceMock.validatePersonalDataExpectedResult = .failure(.serverError)
        sut.validateForm(with: "123.456.789.01", birthDate: "10/10/2000", motherName: "mae2", email: "email@mail.com")
        
        XCTAssertEqual(presenterSpy.presentGenericErrorCallCount, 1)
    }

    func testValidateForm_WhenInputIsInvalid_ShouldPresentNotAuthorizedError() {
        var notAuthorizedError = RequestError()
        notAuthorizedError.title = "Dados inválidos"
        notAuthorizedError.message = "Verifique os dados informados."
        
        serviceMock.validatePersonalDataExpectedResult = .failure(.unauthorized(body: notAuthorizedError))
        sut.validateForm(with: "123.456.789.01", birthDate: "10/10/2000", motherName: "mae2", email: "email@mail.com")

        XCTAssertNotNil(presenterSpy.validationInfoError)
        XCTAssertEqual(presenterSpy.presentValidationInfoErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.validationInfoError?.title, "Dados inválidos")
        XCTAssertEqual(presenterSpy.validationInfoError?.message, "Verifique os dados informados.")
    }

    func testValidateForm_WhenAttempLimitReached_ShouldPresentAttemptLimitError() {
        let attempLimitError = TFAModelsHelper().attempLimitErrorJson()
        
        serviceMock.validatePersonalDataExpectedResult = .failure(.tooManyRequests(body: attempLimitError))
        sut.validateForm(with: "123.456.789.01", birthDate: "10/10/2000", motherName: "mae2", email: "email@mail.com")

        XCTAssertNotNil(presenterSpy.attemptLimitError)
        XCTAssertEqual(presenterSpy.presentAttemptLimitErrorCallCount, 1)
        XCTAssertEqual(presenterSpy.attemptLimitError?.title, "Limite atingido")
        XCTAssertEqual(
            presenterSpy.attemptLimitError?.message,
            "Você excedeu o limite de tentativas. Aguarde 02:52 minutos para tentar novamente."
        )
        XCTAssertEqual(presenterSpy.attemptLimitError?.code, "form_limit")
        XCTAssertEqual(presenterSpy.attemptLimitErrorRetryTime, 10)
    }
    
    func testValidateForm_WhenAttempLimitReached_ShouldLogExceededValidationEvent() {
        let attempLimitError = TFAModelsHelper().attempLimitErrorJson()
        serviceMock.validatePersonalDataExpectedResult = .failure(.tooManyRequests(body: attempLimitError))
        
        sut.validateForm(with: "123.456.789.01", birthDate: "10/10/2000", motherName: "mae2", email: "email@mail.com")
        
        let expectedEvent = TFAEvents.didReceivedExceededDataValidationLimitError(tfaType: .email, currentStep: .userData).event()
        XCTAssertEqual(presenterSpy.presentAttemptLimitErrorCallCount, 1)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
        
    }
    
    func testValidateForm_WhenInputIsValid_ShouldCallValidateDataForNextStep() {
        serviceMock.validatePersonalDataExpectedResult = .success(TFAValidationResponse(nextStep: .phoneNumber))
        
        sut.validateForm(with: "123.456.789-01", birthDate: "01/01/2001", motherName: "mae", email: "email")
        
        XCTAssertEqual(presenterSpy.didValidatePersonalDataCallCount, 1)
        XCTAssertNotNil(presenterSpy.personalDataValidationNextStep)
        XCTAssertEqual(presenterSpy.personalDataValidationNextStep, .phoneNumber)
        XCTAssertEqual(presenterSpy.personalDataValidationType, .email)
    }
    
    func testValidateForm_WhenInputIsValid_ShouldLogDidValidateDataEvent() {
        serviceMock.validatePersonalDataExpectedResult = .success(TFAValidationResponse(nextStep: .phoneNumber))
        
        sut.validateForm(with: "123.456.789-01", birthDate: "01/01/2001", motherName: "mae", email: "email")
        
        let expectedEvent = TFAEvents.didValidateData(.email).event()
        
        XCTAssertEqual(presenterSpy.didValidatePersonalDataCallCount, 1)
        XCTAssertTrue(analytics.equals(to: expectedEvent))
    }
    
    func testTimerTime_WhenCountdownTimeIsEqualOrLessThanZero_ShouldCallTimerStop() {
        sut.startTimerForNewValidation(retryTime: -1)
        
        sut.timerTic()
        
        XCTAssertEqual(timerSpy.callStopCount, 1)
    }

    func testTimerTic_WhenCountDownIsEqualOrLessThanZero_ShouldPresentContinueButtonEnabled() {
        sut.startTimerForNewValidation(retryTime: -1)
        
        sut.timerTic()

        XCTAssertEqual(presenterSpy.presentContinueButtonUpdatedCallCount, 1)
        XCTAssertTrue(presenterSpy.continueButtonEnabled)
    }

    func testTimerTic_WhenCountDownIsGreaterThanZero_ShouldPresentContinueButtonDisabled() {
        sut.startTimerForNewValidation(retryTime: 5)
        
        sut.timerTic()

        XCTAssertEqual(presenterSpy.presentContinueButtonUpdatedCallCount, 1)
        XCTAssertFalse(presenterSpy.continueButtonEnabled)
    }
}

private extension TFAPersonalDataCheckViewModelTests {
    // initialization
    private func tfaPersonalDataCheckViewModel(withID tfaId: String, tfaType: TFAType) -> TFAPersonalDataCheckViewModel {
        return TFAPersonalDataCheckViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            tfaRequestData: TFARequestData(flow: tfaType, step: .userData, id: tfaId),
            dependencies: dependenciesMock,
            timer: timerSpy
        )
    }
}
