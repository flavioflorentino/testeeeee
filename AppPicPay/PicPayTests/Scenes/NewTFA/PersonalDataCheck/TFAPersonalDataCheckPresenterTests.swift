@testable import PicPay
import XCTest
import UI

private final class TFAPersonalDataCheckCoordinatorSpy: TFAPersonalDataCheckCoordinating {
    var viewController: UIViewController?
    var delegate: TFAPersonalDataCheckCoordinatorDelegate?
    private(set) var performActionCallCount = 0
    private(set) var actionPerformed: TFAPersonalDataCheckAction?
    
    func perform(action: TFAPersonalDataCheckAction) {
        performActionCallCount += 1
        actionPerformed = action
    }
}

private final class TFAPersonalDataCheckDisplaySpy: TFAPersonalDataCheckDisplay {
    private(set) var continueButtonEnabled = false
    private(set) var enableContinueButtonCallCount = 0
    private(set) var displayLoadingCallCount = 0
    private(set) var isLoading = false
    private(set) var displayedAlert: Alert?
    private(set) var displayValidationInfoErrorCallCount = 0
    private(set) var displayAttempLimitErrorCallCount = 0
    private(set) var attemptLimitRetryTime = 0
    private(set) var displayGenericErrorCallCount = 0
    private(set) var displayContinueButtonCallCount = 0
    private(set) var continueButtonText: String?
    private(set) var emailHint: String?
    private(set) var displayEmailHintCallCount = 0
    private(set) var fieldValidationErrorMessage: String?
    private(set) var updateEmailFieldErrorMessageCallCount = 0
    private(set) var updateDocumentFieldErrorMessageCallCount = 0
    private(set) var updateBirthdateFieldErrorMessageCallCount = 0
    private(set) var updateMotherNameFieldErrorMessageCallCount = 0
    
    func enableContinueButton(enable: Bool) {
        enableContinueButtonCallCount += 1
        continueButtonEnabled = enable
    }
    
    func displayLoading(loading: Bool) {
        displayLoadingCallCount += 1
        isLoading = loading
    }
    
    func displayValidationInfoError(alert: Alert) {
        displayValidationInfoErrorCallCount += 1
        displayedAlert = alert
    }
    
    func displayAttempLimitError(alert: Alert, retryTime: Int) {
        displayAttempLimitErrorCallCount += 1
        displayedAlert = alert
        attemptLimitRetryTime = retryTime
    }
    
    func displayGenericError(alert: Alert) {
        displayGenericErrorCallCount += 1
        displayedAlert = alert
    }
    
    func displayContinueButton(with currentRemainingTime: String, enabled: Bool) {
        displayContinueButtonCallCount += 1
        continueButtonText = currentRemainingTime
        continueButtonEnabled = enabled
    }
    
    func displayEmailHint(_ hint: String) {
        displayEmailHintCallCount += 1
        emailHint = hint
    }
    
    func updateEmailFieldErrorMessage(_ message: String?) {
        updateEmailFieldErrorMessageCallCount += 1
        fieldValidationErrorMessage = message
    }
    
    func updateDocumentFieldErrorMessage(_ message: String?) {
        updateDocumentFieldErrorMessageCallCount += 1
        fieldValidationErrorMessage = message
    }
    
    func updateBirthdateFieldErrorMessage(_ message: String?) {
        updateBirthdateFieldErrorMessageCallCount += 1
        fieldValidationErrorMessage = message
    }
    
    func updateMotherNameFieldErrorMessage(_ message: String?) {
        updateMotherNameFieldErrorMessageCallCount += 1
        fieldValidationErrorMessage = message
    }
}

final class TFAPersonalDataCheckPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = TFAPersonalDataCheckCoordinatorSpy()
    private lazy var displaySpy = TFAPersonalDataCheckDisplaySpy()
    
    private lazy var sut: TFAPersonalDataCheckPresenter = {
        let presenter = TFAPersonalDataCheckPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()
    
    func testPresentLoading_WhenLoadingIsTrue_ShouldDisplayLoadingActive() {
        sut.presentLoading(true)
        
        XCTAssertEqual(displaySpy.displayLoadingCallCount, 1)
        XCTAssertTrue(displaySpy.isLoading)
    }
    
    func testPresentLoading_WhenLoadingIsFalse_ShouldDisplayLoadingDeactive() {
        sut.presentLoading(false)
        
        XCTAssertEqual(displaySpy.displayLoadingCallCount, 1)
        XCTAssertFalse(displaySpy.isLoading)
    }
    
    func testDidValidatePersonalData_WhenCalledFromViewModel_ShouldPerformStartNextStepActionOnCoordinator() {
        sut.didValidatePersonalData(with: .email, nextStep: .phoneNumber)
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.actionPerformed, .validateDataAndStartNextStep(tfaType: .email, nextStep: .phoneNumber))
    }
    
    func testShouldEnableContinueButton_WhenEnableIsTrue_ShouldDisplayButtonEnabled() {
        sut.shouldEnableContinueButton(enable: true)
     
        XCTAssertEqual(displaySpy.enableContinueButtonCallCount, 1)
        XCTAssertTrue(displaySpy.continueButtonEnabled)
    }

    func testPresentGenericError_WhenCalledFromViewModel_ShouldDisplayGenericAlert() {
        sut.presentGenericError()
        
        XCTAssertEqual(displaySpy.displayGenericErrorCallCount, 1)
        XCTAssertNotNil(displaySpy.displayedAlert)
        XCTAssertEqual(displaySpy.displayedAlert?.text?.string, "Ocorreu um erro, tente novamente")
    }
    
    func testPresentValidationInfoError_WhenCalledFromViewModel_ShouldDisplayValidationInfoAlert() {
        let error = TFAModelsHelper().invalidDataErrorJson()
        sut.presentValidationInfoError(tfaError: error)
        
        XCTAssertEqual(displaySpy.displayValidationInfoErrorCallCount, 1)
        XCTAssertNotNil(displaySpy.displayedAlert)
        XCTAssertEqual(displaySpy.displayedAlert?.title?.string, "Ops!")
        XCTAssertEqual(displaySpy.displayedAlert?.text?.string, "Dados inválidos")
    }
    
    func testPresentAttemptLimitError_WhenCalledFromViewModel_ShouldDisplayAttemptLimitAlert() {
        let error = TFAModelsHelper().attempLimitErrorJson()
        sut.presentAttempLimitError(tfaError: error, retryTime: 180, tfaType: .sms)
        
        XCTAssertEqual(displaySpy.displayAttempLimitErrorCallCount, 1)
        XCTAssertNotNil(displaySpy.displayedAlert)
        XCTAssertEqual(displaySpy.displayedAlert?.title?.string, "Limite atingido")
        XCTAssertEqual(displaySpy.attemptLimitRetryTime, 180)
    }
    
    func testPresentContinueButtonUpdated_WhenCalledFromViewModel_ShouldDisplayButtonWithCorrectText() {
        sut.presentContinueButtonUpdated(with: 100)
        
        XCTAssertEqual(displaySpy.displayContinueButtonCallCount, 1)
        XCTAssertNotNil(displaySpy.continueButtonText)
        XCTAssertEqual(displaySpy.continueButtonText, "Aguarde 01:40")
        XCTAssertFalse(displaySpy.continueButtonEnabled)
    }

    func testUpdateEmailHint_WhenCalledFromViewModel_ShouldDisplayHintOnViewController() {
        sut.updateEmailHint(with: "jos****@gmail.com")
        
        XCTAssertEqual(displaySpy.displayEmailHintCallCount, 1)
        XCTAssertEqual(displaySpy.emailHint, "jos****@gmail.com")
    }
    
    func testUpdateEmailField_WhenErrorIsNil_ShouldNotDisplayErrorOnViewController() {
        sut.updateEmailField(withError: nil)
        
        XCTAssertEqual(displaySpy.updateEmailFieldErrorMessageCallCount, 1)
        XCTAssertNil(displaySpy.fieldValidationErrorMessage)
    }
    
    func testUpdateDocumentField_WhenErrorIsNil_ShouldNotDisplayErrorOnViewController() {
        sut.updateDocumentField(withError: nil)
        
        XCTAssertEqual(displaySpy.updateDocumentFieldErrorMessageCallCount, 1)
        XCTAssertNil(displaySpy.fieldValidationErrorMessage)
    }
    
    func testUpdateBirthdateField_WhenErrorIsNil_ShouldNotDisplayErrorOnViewController() {
        sut.updateBirthdateField(withError: nil)
        
        XCTAssertEqual(displaySpy.updateBirthdateFieldErrorMessageCallCount, 1)
        XCTAssertNil(displaySpy.fieldValidationErrorMessage)
    }
    
    func testUpdateMotherNameField_WhenErrorIsNil_ShouldNotDisplayErrorOnViewController() {
        sut.updateMotherNameField(withError: nil)
        
        XCTAssertEqual(displaySpy.updateMotherNameFieldErrorMessageCallCount, 1)
        XCTAssertNil(displaySpy.fieldValidationErrorMessage)
    }
    
    func testUpdateEmailField_WhenErrorIsNotNil_ShouldDisplayErrorOnViewController() {
        sut.updateEmailField(withError: TFAPersonalDataFieldCheckValidationError.invalidEmail.message)
        
        XCTAssertEqual(displaySpy.updateEmailFieldErrorMessageCallCount, 1)
        XCTAssertEqual(displaySpy.fieldValidationErrorMessage, TFAPersonalDataFieldCheckValidationError.invalidEmail.message)
    }
    
    func testUpdateDocumentField_WhenErrorIsNotNil_ShouldDisplayErrorOnViewController() {
        sut.updateDocumentField(withError: TFAPersonalDataFieldCheckValidationError.invalidCPFDocument.message)
        
        XCTAssertEqual(displaySpy.updateDocumentFieldErrorMessageCallCount, 1)
        XCTAssertEqual(displaySpy.fieldValidationErrorMessage, TFAPersonalDataFieldCheckValidationError.invalidCPFDocument.message)
    }
    
    func testUpdateBirthdateField_WhenErrorIsNotNil_ShouldDisplayErrorOnViewController() {
        sut.updateBirthdateField(withError: TFAPersonalDataFieldCheckValidationError.birthdateRequired.message)
        
        XCTAssertEqual(displaySpy.updateBirthdateFieldErrorMessageCallCount, 1)
        XCTAssertEqual(displaySpy.fieldValidationErrorMessage, TFAPersonalDataFieldCheckValidationError.birthdateRequired.message)
    }
    
    func testUpdateMotherNameField_WhenErrorIsNotNil_ShouldDisplayErrorOnViewController() {
        sut.updateMotherNameField(withError: TFAPersonalDataFieldCheckValidationError.motherNameRequired.message)
        
        XCTAssertEqual(displaySpy.updateMotherNameFieldErrorMessageCallCount, 1)
        XCTAssertEqual(displaySpy.fieldValidationErrorMessage, TFAPersonalDataFieldCheckValidationError.motherNameRequired.message)
    }
}
