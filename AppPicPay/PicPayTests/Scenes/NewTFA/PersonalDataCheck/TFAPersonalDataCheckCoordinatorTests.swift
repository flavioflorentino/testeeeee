@testable import PicPay
import XCTest

final class TFAPersonalDataCheckCoordinatorTests: XCTestCase {
    private lazy var spy: TFAPersonalDataCheckCoordinatorDelegateSpy = {
        let coordinatorDelegate = TFAPersonalDataCheckCoordinatorDelegateSpy()
        return coordinatorDelegate
    }()
    
    private lazy var sut: TFAPersonalDataCheckCoordinator = {
        let coordinator = TFAPersonalDataCheckCoordinator(dependencies: DependencyContainerMock())
        coordinator.delegate = spy
        return coordinator
    }()

    func testValidateDataAndStartNextStepAction_WhenCalledFromPresenter_ShouldCallDidValidatePersonalDataOnDelegate() {
        sut.perform(action: .validateDataAndStartNextStep(tfaType: .email, nextStep: .bankData))

        XCTAssertEqual(spy.didValidatePersonalDataCallCount, 1)
        XCTAssertEqual(spy.nextStepReceived, .bankData)
    }
}

private final class TFAPersonalDataCheckCoordinatorDelegateSpy: TFAPersonalDataCheckCoordinatorDelegate {
    private(set) var didValidatePersonalDataCallCount = 0
    private(set) var nextStepReceived = TFAStep.initial
    
    func didValidatePersonalData(with tfaType: TFAType, nextStep: TFAStep) {
        didValidatePersonalDataCallCount += 1
        nextStepReceived = nextStep
    }
}
