@testable import PicPay
import UI
import Validator

final class BankAccountTestMocks {
    func minimumLengthField() -> UIPPFloatingTextField {
        var field = UIPPFloatingTextField()
        var rule = ValidationRuleSet<String>()
        rule.add(rule: ValidationRuleLength(min: 1, error: BankAccountCheckValidationError.agencyRequired))
        field.validationRules = rule
        return field
    }
}

