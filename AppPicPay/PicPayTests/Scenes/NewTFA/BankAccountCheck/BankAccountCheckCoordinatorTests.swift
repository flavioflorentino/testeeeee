@testable import PicPay
import XCTest

private final class BankAccountCheckCoordinatorDelegateSpy: BankAccountCheckCoordinatorDelegate {
    private(set) var didValidateAccountDataCallCount = 0
    private(set) var nextStepReceived = TFAStep.initial
    
    func didValidateAccountData(with Type: TFAType, nextStep: TFAStep) {
        didValidateAccountDataCallCount += 1
        nextStepReceived = nextStep
    }
}

final class BankAccountCheckCoordinatorTests: XCTestCase {
    private let navController = NavigationControllerMock(rootViewController: UIViewController())
    private lazy var delegateSpy = BankAccountCheckCoordinatorDelegateSpy()
    
    private lazy var sut: BankAccountCheckCoordinator = {
        let coordinator = BankAccountCheckCoordinator()
        coordinator.delegate = delegateSpy
        return coordinator
    }()

    func testPerform_WhenActionIsAccountDataValidateAndStartNextStep_ShouldCallDelegateWithNextStep() {
        sut.perform(action: .accountDataValidateAndStartNextStep(tfaType: .email, nextStep: .code))

        XCTAssertEqual(delegateSpy.didValidateAccountDataCallCount, 1)
        XCTAssertEqual(delegateSpy.nextStepReceived, .code)
    }
}
