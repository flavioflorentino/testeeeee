import Core
@testable import PicPay
import Validator
import XCTest
import UI

final class BankAccountCheckViewModelTests: XCTestCase {
    private lazy var mock = BankAccountCheckServiceMock()
    private lazy var spy = BankAccountCheckPresenterSpy()
    
    lazy var mockDependencies = DependencyContainerMock()
    
    private lazy var sut: BankAccountCheckViewModel = {
        let viewModel = BankAccountCheckViewModel(
            service: mock,
            presenter: spy,
            bankInfo: TFABankAccount(bankName: "Caixa", imageUrl: "imageURL", agencyDv: false),
            tfaRequestData: TFARequestData(flow: .email, step: .bankData, id: "123"),
            dependencies: mockDependencies
        )
        
        return viewModel
    }()
    
    func testLoadInfo_ShouldConfigTextFields() {
        sut.loadInfo()
        
        XCTAssertEqual(spy.setFieldsRulesCallCount, 1)
    }
    
    func testLoadinInfo_ShouldConfigureBankInfo() {
        sut.loadInfo()
        
        XCTAssertNotNil(spy.configuredBank)
        XCTAssertEqual(spy.configuredBank?.bankName, "Caixa")
    }
    
    func testcheckFieldsAndEnableContinue_WhenInputIsValid_ShouldEnableContinueButton() {
        let field = BankAccountTestMocks().minimumLengthField()
        field.text = "1234"
        
        sut.checkFieldsAndEnableContinue(
            accountNumberValidationResult: field.validate(),
            accountDigitValidationResult: field.validate(),
            agencyValidationResult: field.validate(),
            agencyDigitValidationResult: field.validate()
        )
        
        XCTAssertTrue(spy.continueButtonEnabled)
    }
    
    func testcheckFieldsAndEnableContinue_WhenInputIsInValid_ShouldNotEnableContinueButton() {
        let field = BankAccountTestMocks().minimumLengthField()
        field.text = ""
        
        sut.checkFieldsAndEnableContinue(
            accountNumberValidationResult: field.validate(),
            accountDigitValidationResult: field.validate(),
            agencyValidationResult: field.validate(),
            agencyDigitValidationResult: field.validate()
        )
        
        XCTAssertFalse(spy.continueButtonEnabled)
    }
    
    func testCheckAccountNumberField_WhenInputIsValid_ShouldNotPresentErrorMessage() {
        let field = BankAccountTestMocks().minimumLengthField()
        field.text = "1234"
        
        sut.checkAccountNumberFieldWith(validationResult: field.validate())
        XCTAssertEqual(spy.presentAccountNumberFieldErrorMessageCallCount, 1)
        XCTAssertNil(spy.presentedErrorMessage)
    }
    
    func testCheckAccountNumberField_WhenInputIsInValid_ShouldPresentErrorMessage() {
        let field = BankAccountTestMocks().minimumLengthField()
        field.text = ""
        
        sut.checkAccountNumberFieldWith(validationResult: field.validate())
        XCTAssertEqual(spy.presentAccountNumberFieldErrorMessageCallCount, 1)
        XCTAssertNotNil(spy.presentedErrorMessage)
    }
    
    func testCheckAccountDigitField_WhenInputIsValid_ShouldNotPresentErrorMessage() {
        let field = BankAccountTestMocks().minimumLengthField()
        field.text = "1"
        
        sut.checkAccountDigitFieldWith(validationResult: field.validate())
        XCTAssertEqual(spy.presentAccountDigitFieldErrorMessageCallCount, 1)
        XCTAssertNil(spy.presentedErrorMessage)
    }
    
    func testCheckAgencyNumberField_WhenInputIsValid_ShouldNotPresentErrorMessage() {
        let field = BankAccountTestMocks().minimumLengthField()
        field.text = "1234"
        
        sut.checkAgencyNumberFieldWith(validationResult: field.validate())
        XCTAssertEqual(spy.presentAgencyNumberFieldErrorMessageCallCount, 1)
        XCTAssertNil(spy.presentedErrorMessage)
    }
    
    func testCheckAgencyDigitField_WhenInputIsValid_ShouldNotPresentErrorMessage() {
        let field = BankAccountTestMocks().minimumLengthField()
        field.text = "1"
        
        sut.checkAgencyDigitFieldWith(validationResult: field.validate())
        XCTAssertEqual(spy.presentAgencyDigitFieldErrorMessageCallCount, 1)
        XCTAssertNil(spy.presentedErrorMessage)
    }
    
    func testValidateForm_WhenInputIsValid_ShouldCallValidateDataForNextStep() {
        mock.isSuccess = true
        sut.validateForm(agency: "123", agencyDigit: nil, account: "123456", accountDigit: "1")

        XCTAssertEqual(spy.validateDataForNextStepCallCount, 1)
        XCTAssertEqual(spy.nextStepCalled, .code)
    }

    func testValidateForm_WhenServerHasError_ShouldPresentsGenericError() {
        sut.validateForm(agency: "123", agencyDigit: nil, account: "123456", accountDigit: "1")

        XCTAssertEqual(spy.presentGenericErrorCallCount, 1)
    }

    func testValidateForm_WhenInputIsInvalid_ShouldPresentNotAuthorizedError() {
        mock.isInvalidInput = true
        sut.validateForm(agency: "123", agencyDigit: nil, account: "123456", accountDigit: "1")

        XCTAssertNotNil(spy.validationError)
        XCTAssertEqual(spy.presentValidationInfoErrorCallCount, 1)
        XCTAssertEqual(spy.validationError?.message, "Dados inválidos")
    }

    func testValidateForm_WhenReachedAttemptLimit_ShouldPresentAttemptLimitReachedError() {
        mock.hasReachLimit = true
        sut.validateForm(agency: "123", agencyDigit: nil, account: "123456", accountDigit: "1")

        XCTAssertNotNil(spy.validationError)
        XCTAssertEqual(spy.presentAttemptLimitErrorCallCount, 1)
        XCTAssertEqual(spy.validationError?.title, "Limite atingido")
    }
    
    func testHelpInfoCalled() {
        sut.bankAccountHelpInfo()
        
        XCTAssertEqual(spy.didShowHelpInfoCallCount, 1)
    }
    
    func testStartTimerForNewValidation_WhenTimerIsNil_ShouldEnableContinueButton() {
        let viewModel = BankAccountCheckViewModel(
            service: mock,
            presenter: spy,
            bankInfo: TFABankAccount(bankName: "Caixa", imageUrl: "imageURL", agencyDv: false),
            tfaRequestData: TFARequestData(flow: .email, step: .bankData, id: "123"),
            dependencies: mockDependencies,
            timer: nil
        )
        
        viewModel.startTimerForNewValidation(retryTime: 3)
        expectation(waitForCondition: self.spy.continueButtonRemainingTimeForNewAttempt <= 0)
        expectation(waitForCondition: self.spy.continueButtonEnabled)
        
        waitForExpectations(timeout: 5.0)
    }
    
    func testStartTimerForNewValidation_WhenTimerFinish_ShouldEnableContinueButton() {
         sut.startTimerForNewValidation(retryTime: 3)
         
         expectation(waitForCondition: self.spy.continueButtonRemainingTimeForNewAttempt <= 0)
         expectation(waitForCondition: self.spy.continueButtonEnabled)
         
         waitForExpectations(timeout: 5.0)
     }
    
    func testLoadInfo_WhenBankInfoIsNil_ShouldLoadBankInfo() {
        let viewModel = BankAccountCheckViewModel(
            service: mock,
            presenter: spy,
            bankInfo: nil,
            tfaRequestData: TFARequestData(flow: .email, step: .bankData, id: "123"),
            dependencies: mockDependencies
        )
        
        mock.isSuccess = true
        viewModel.loadInfo()
        
        XCTAssertEqual(spy.presentLoadingScreenCallCount, 2)
        XCTAssertNotNil(spy.configuredBank)
        XCTAssertEqual(spy.configuredBank?.bankName, "BB")
    }
    
    func testLoadInfo_WhenBankInfoIsNilAndServiceHasError_ShouldPresentGenericError() {
        let viewModel = BankAccountCheckViewModel(
            service: mock,
            presenter: spy,
            bankInfo: nil,
            tfaRequestData: TFARequestData(flow: .email, step: .bankData, id: "123"),
            dependencies: mockDependencies
        )
        
        mock.isSuccess = false
        viewModel.loadInfo()
        
        XCTAssertEqual(spy.presentLoadingScreenCallCount, 2)
        XCTAssertEqual(spy.presentGenericErrorCallCount, 1)
    }
    
    func test_ValidateForm_WhenPerformingValidation_ShouldPresentLoading() {
        sut.validateForm(agency: "2123", agencyDigit: nil, account: "298", accountDigit: "3")

        XCTAssertEqual(spy.presentLoadingCallCount, 2)
    }

    func testValidateForm_WhenServiceHasError_ShouldPresentGenericError() {
        sut.validateForm(agency: "2123", agencyDigit: nil, account: "298", accountDigit: "3")

        XCTAssertNil(spy.nextStepCalled)
        XCTAssertEqual(spy.presentGenericErrorCallCount, 1)
    }
}

private final class BankAccountCheckServiceMock: BankAccountCheckServicing {
    var isSuccess = false
    var hasReachLimit = false
    var isInvalidInput = false
    
    func validateBankAccountData(bankAccount: BankAccountData, tfaRequestData: TFARequestData, completion: @escaping (Result<TFAValidationResponse, ApiError>) -> Void) {
        if isSuccess {
            completion(.success(TFAValidationResponse(nextStep: .code)))
        } else {
            if hasReachLimit {
                completion(.failure(ApiError.badRequest(body: TFAModelsHelper().attempLimitErrorJson())))
            } else if isInvalidInput {
                completion(.failure(ApiError.unauthorized(body: TFAModelsHelper().invalidDataErrorJson())))
            } else {
                completion(.failure(ApiError.serverError))
            }
        }
    }
    
    func getBankAccountData(tfaRequestData: TFARequestData, completion: @escaping (Result<TFABankAccountInfo, ApiError>) -> Void) {
        if isSuccess {
            completion(.success(TFABankAccountInfo(bank: TFABankAccount(bankName: "BB", imageUrl: "", agencyDv: false))))
        } else {
            completion(.failure(.serverError))
        }
    }
}

private final class BankAccountCheckPresenterSpy: BankAccountCheckPresenting {
    var viewController: BankAccountCheckDisplay?
    
    private(set) var setFieldsRulesCallCount = 0
    private(set) var configuredBank: TFABankAccount?
    private(set) var continueButtonEnabled = false
    private(set) var presentedErrorMessage: String?
    private(set) var presentGenericErrorCallCount = 0
    private(set) var presentValidationInfoErrorCallCount = 0
    private(set) var presentAttemptLimitErrorCallCount = 0
    private(set) var validationError: RequestError?
    private(set) var didShowHelpInfoCallCount = 0
    private(set) var continueButtonRemainingTimeForNewAttempt: TimeInterval = 0
    private(set) var presentLoadingScreenCallCount = 0
    private(set) var presentLoadingCallCount = 0
    private(set) var nextStepCalled: TFAStep?
    private(set) var validateDataForNextStepCallCount = 0
    private(set) var presentAccountNumberFieldErrorMessageCallCount = 0
    private(set) var presentAccountDigitFieldErrorMessageCallCount = 0
    private(set) var presentAgencyNumberFieldErrorMessageCallCount = 0
    private(set) var presentAgencyDigitFieldErrorMessageCallCount = 0
    
    func setFieldsRules(settings: BankAccountCheckTextFieldSettings) {
        setFieldsRulesCallCount += 1
    }
    
    func presentBankInfo(bank: TFABankAccount?) {
        configuredBank = bank
    }
    
    func validateData(with tfaType: TFAType, nextStep: TFAStep) {
        validateDataForNextStepCallCount += 1
        nextStepCalled = nextStep
    }
    
    func presentLoadingScreen(_ loading: Bool) {
        presentLoadingScreenCallCount += 1
    }
    
    func presentBankAccountHelpInfo() {
        didShowHelpInfoCallCount += 1
    }
    
    func setLoading(loading: Bool) {
        presentLoadingCallCount += 1
    }
    
    func presentContinueButtonUpdated(with remainingTime: TimeInterval) {
        continueButtonRemainingTimeForNewAttempt = remainingTime
    }
    
    func presentValidationInfoError(tfaError: RequestError) {
        presentValidationInfoErrorCallCount += 1
        validationError = tfaError
    }
    
    func presentAttemptLimitError(tfaError: RequestError, retryTime: Int, tfaType: TFAType) {
        presentAttemptLimitErrorCallCount += 1
        validationError = tfaError
    }
    
    func presentGenericError() {
        presentGenericErrorCallCount += 1
    }
    
    func presentAccountNumberFieldWith(errorMessage: String?) {
        presentedErrorMessage = errorMessage
        presentAccountNumberFieldErrorMessageCallCount += 1
    }
    
    func presentAccountDigitFieldWith(errorMessage: String?) {
        presentedErrorMessage = errorMessage
        presentAccountDigitFieldErrorMessageCallCount += 1
    }
    
    func presentAgencyNumberFieldWith(errorMessage: String?) {
        presentedErrorMessage = errorMessage
        presentAgencyNumberFieldErrorMessageCallCount += 1
    }
    
    func presentAgencyDigitFieldWith(errorMessage: String?) {
        presentedErrorMessage = errorMessage
        presentAgencyDigitFieldErrorMessageCallCount += 1
    }
    
    func shouldEnableContinue(_ enable: Bool) {
        continueButtonEnabled = enable
    }
}
