@testable import PicPay
import XCTest
import UI

final class BankAccountCheckPresenterTest: XCTestCase {
    private lazy var coordinatorMock: BankAccountCheckCoordinatorMock = BankAccountCheckCoordinatorMock()
    private lazy var spy = BankAccountCheckDisplaySpy()
    private lazy var mockDependencies = DependencyContainerMock()
    
    private lazy var sut: BankAccountCheckPresenter = {
        let presenter = BankAccountCheckPresenter(coordinator: coordinatorMock, dependencies: mockDependencies)
        presenter.viewController = spy
        return presenter
    }()
    
    func testValidateDataForNextStep_WhenFlowIsEmail_ShoudPerformAccountDataValidateOnCoordinator() {
        sut.validateData(with: .email, nextStep: .code)
        
        XCTAssertNotNil(coordinatorMock.actionCalled)
        if case let .accountDataValidateAndStartNextStep(tfaType, _) = coordinatorMock.actionCalled {
            XCTAssertEqual(tfaType, .email)
        }
    }
    
    func testShoulEnableContinue_ShouldCalDisplaylEnableContinueButton() {
        sut.shouldEnableContinue(true)
        
        XCTAssertEqual(spy.enableContinueButtonCallCount, 1)
    }
    
    func testPresentGenericError_ShouldDisplayGenericAlert() {
        sut.presentGenericError()
        
        XCTAssertNotNil(spy.displayedAlert)
        XCTAssertEqual(spy.displayedAlert?.text?.string, "Ocorreu um erro, tente novamente")
    }
    
    func testPresentValidationInfoError_WhenErrorIsInvalidData_ShouldDisplayValidationInfoAlert() {
        let error = TFAModelsHelper().invalidDataErrorJson()
        
        sut.presentValidationInfoError(tfaError: error)
        
        XCTAssertNotNil(spy.displayedAlert)
        XCTAssertEqual(spy.displayValidationInfoErrorCallCount, 1)
        XCTAssertEqual(spy.displayedAlert?.title?.string, "Ops!")
    }
    
    func testPresentAttemptLimitError_WhenErrorIsLimitReached_ShouldDisplayAttemptLimitAlert() {
        let error = TFAModelsHelper().attempLimitErrorJson()
        sut.presentAttemptLimitError(tfaError: error, retryTime: 180, tfaType: .sms)
        
        XCTAssertNotNil(spy.displayedAlert)
        XCTAssertEqual(spy.displayAttemptLimitErrorCallCount, 1)
        XCTAssertEqual(spy.displayedAlert?.title?.string, "Limite atingido")
        XCTAssertNotNil(spy.currentRetryTime)
        XCTAssertEqual(spy.currentRetryTime, 180)
    }
    
    func testPresentContinueButtonUpdated_ShouldPresentContinueButtonWithCorrectText() {
        sut.presentContinueButtonUpdated(with: 100)
        
        XCTAssertNotNil(spy.continueButtonText)
        XCTAssertEqual(spy.continueButtonText, "Aguarde 01:40")
    }
    
    func testSetFieldsRules_ShouldConfigureTextFieldSettings() {
        let setting = TextfieldSetting(placeholder: "place", text: "field", rule: nil, mask: nil)
        sut.setFieldsRules(settings: BankAccountCheckTextFieldSettings(
            bankAgency: setting,
            bankAccount: setting,
            accountDigit: setting,
            agencyDigit: setting)
        )
        
        XCTAssertNotNil(spy.fieldsRules)
        XCTAssertEqual(spy.setFieldsRulesCallCount, 1)
        XCTAssertEqual(spy.fieldsRules?.bankAgency.placeholder, setting.placeholder)
    }
    
    func testPresentBankInfo_WhenBankIsOriginal_ShouldDisplayBankWithCorrectName() {
        let bankInfo = TFABankAccount(bankName: "Original", imageUrl: "", agencyDv: false)
        sut.presentBankInfo(bank: bankInfo)
        
        XCTAssertNotNil(spy.bankInfo)
        XCTAssertEqual(spy.bankInfo?.bankName, bankInfo.bankName)
    }
    
    func testPresentBankAccountHelpInfo_ShouldDisplayAlertWithCorrectText() {
        sut.presentBankAccountHelpInfo()
        
        XCTAssertNotNil(spy.displayedAlert)
        XCTAssertEqual(spy.displayedAlert?.text?.string, BankAccountCheckLocalizable.bankAccountAlertMessage.text)
    }
    
    func testSetLoading_ShouldDisplayLoadingOnView() {
        sut.setLoading(loading: true)
        XCTAssertEqual(spy.displayLoadingCallCount, 1)
    }
    
    func testPresentLoadingScreen_ShouldDisplayLoadingScreenOnView() {
        sut.presentLoadingScreen(true)
        
        XCTAssertEqual(spy.displayLoadingScreenCallCount, 1)
    }
    
    func testPresentAccountNumberField_WhenErrorMessageIsNil_ShouldDisplayAccountNumberField_WithoutError() {
        sut.presentAccountNumberFieldWith(errorMessage: nil)
        
        XCTAssertEqual(spy.displayAccountNumberFieldErrorMessageCallCount, 1)
        XCTAssertNil(spy.errorMessage)
    }
    
    func testPresentAccountDigitFieldWith_WhenErrorMessageIsNil_ShouldDisplayAccountNumberField_WithoutError() {
        sut.presentAccountDigitFieldWith(errorMessage: nil)
        
        XCTAssertEqual(spy.displayAccountDigitFieldErrorMessageCallCount, 1)
        XCTAssertNil(spy.errorMessage)
    }
    
    func testPresentAgencyNumberFieldWith_WhenErrorMessageIsNil_ShouldDisplayAccountNumberField_WithoutError() {
        sut.presentAgencyNumberFieldWith(errorMessage: nil)
        
        XCTAssertEqual(spy.displayAgencyNumberFieldErrorMessageCallCount, 1)
        XCTAssertNil(spy.errorMessage)
    }
    
    func testPresentAgencyDigitFieldWith_WhenErrorMessageIsNil_ShouldDisplayAccountNumberField_WithoutError() {
        sut.presentAgencyDigitFieldWith(errorMessage: nil)
        
        XCTAssertEqual(spy.displayAgencyDigitFieldErrorMessageCallCount, 1)
        XCTAssertNil(spy.errorMessage)
    }
    func testPresentAccountNumberField_WhenErrorMessageIsNotNil_ShouldDisplayAccountNumberField_WitError() {
        sut.presentAccountNumberFieldWith(errorMessage: "Campo obrigátorio")
        
        XCTAssertEqual(spy.displayAccountNumberFieldErrorMessageCallCount, 1)
        XCTAssertNotNil(spy.errorMessage)
        XCTAssertEqual(spy.errorMessage, "Campo obrigátorio")
    }
    
}

private final class BankAccountCheckCoordinatorMock: BankAccountCheckCoordinating {
    var delegate: BankAccountCheckCoordinatorDelegate?
    var viewController: UIViewController?
    private(set) var actionCalled: BankAccountCheckAction?
    
    func perform(action: BankAccountCheckAction) {
        actionCalled = action
    }
}

private final class BankAccountCheckDisplaySpy: BankAccountCheckDisplay {
    private(set) var errorMessage: String?
    private(set) var enableContinueButtonCallCount = 0
    private(set) var displayedAlert: Alert?
    private(set) var displayValidationInfoErrorCallCount = 0
    private(set) var displayAttemptLimitErrorCallCount = 0
    private(set) var displayGenericErrorCallCount = 0
    private(set) var currentRetryTime: TimeInterval?
    private(set) var continueButtonText: String?
    private(set) var fieldsRules: BankAccountCheckTextFieldSettings?
    private(set) var setFieldsRulesCallCount = 0
    private(set) var bankInfo: TFABankAccount?
    private(set) var displayLoadingCallCount = 0
    private(set) var displayLoadingScreenCallCount = 0
    private(set) var displayAccountNumberFieldErrorMessageCallCount = 0
    private(set) var displayAccountDigitFieldErrorMessageCallCount = 0
    private(set) var displayAgencyNumberFieldErrorMessageCallCount = 0
    private(set) var displayAgencyDigitFieldErrorMessageCallCount = 0
    
    func displayValidationInfoError(alert: Alert) {
        displayedAlert = alert
        displayValidationInfoErrorCallCount += 1
    }
    
    func displayAttempLimitError(alert: Alert, retryTime: TimeInterval) {
        displayedAlert = alert
        currentRetryTime = retryTime
        displayAttemptLimitErrorCallCount += 1
    }
    
    func displayGenericError(alert: Alert) {
        displayedAlert = alert
        displayGenericErrorCallCount += 1
    }
    
    func displayContinueButton(with currentRemainingTime: String, enabled: Bool) {
        continueButtonText = currentRemainingTime
    }
    
    func setFieldRules(settings: BankAccountCheckTextFieldSettings) {
        fieldsRules = settings
        setFieldsRulesCallCount += 1
    }
    
    func displayBankAccountHelpInfo(alert: Alert) {
        displayedAlert = alert
    }
    
    func displayLoading(loading: Bool) {
        displayLoadingCallCount += 1
    }
    
    func displayLoadingScreen(loading: Bool) {
        displayLoadingScreenCallCount += 1
    }
    
    func displayBankInfo(bank: TFABankAccount?, hasAgencyField: Bool) {
        bankInfo = bank
    }
    
    func displayAccountNumberFieldErrorMessage(_ message: String?) {
        displayAccountNumberFieldErrorMessageCallCount += 1
        errorMessage = message
    }
    
    func displayAccountDigitFieldErrorMessage(_ message: String?) {
        displayAccountDigitFieldErrorMessageCallCount += 1
        errorMessage = message
    }
    
    func displayAgencyNumberFieldErrorMessage(_ message: String?) {
        displayAgencyNumberFieldErrorMessageCallCount += 1
        errorMessage = message
    }
    
    func displayAgencyDigitFieldErrorMessage(_ message: String?) {
        displayAgencyDigitFieldErrorMessageCallCount += 1
        errorMessage = message
    }
    
    func displayContinueButtonEnabled(_ enabled: Bool) {
        enableContinueButtonCallCount += 1
    }
}
