@testable import PicPay
import UI
import XCTest
import Core

final class NewTFAOptionsPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = NewTFAOptionsCoordinatorSpy()
    private lazy var controllerSpy = NewTFAOptionsViewControllerSpy()
    
    private lazy var sut: NewTFAOptionsPresenter = {
        let presenter = NewTFAOptionsPresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()
    
    func testPresentOptionsCallsDisplayOptionsOnView() {
        sut.presentOptions(
            [
                NewTFAOption(flow: .authorizedDevice, firstStep: .code, iconUrl: "", title: "Receber código pelo aparelho antigo"),
                NewTFAOption(flow: .sms, firstStep: .userData, iconUrl: "", title: "Receber código via SMS"),
                NewTFAOption(flow: .email, firstStep: .userData, iconUrl: "", title: "Receber código via e-mail")
            ]
        )
        
        XCTAssertEqual(controllerSpy.displayOptionsCallCount, 1)
    }

    func testPresentFallBack_ShouldCallControllerDisplayFallback() {
        sut.presentFallback("")

        XCTAssertEqual(controllerSpy.displayFallbackCallCount, 1)
    }
    
    func testPresentLoadingDisplaysLoadingOnView() {
        sut.presentLoading(true)
        
        XCTAssertEqual(controllerSpy.displayLoadingCallCount, 1)
    }
    
    func testPresentOptionsLoadError_WhenReceiveRequestError_ShouldDisplayOptionsLoadErrorAlert() {
        sut.presentError(error: RequestError())
        
        XCTAssertEqual(controllerSpy.displayErrorCallCount, 1)
        XCTAssertNotNil(controllerSpy.displayedAlert)
        XCTAssertEqual(controllerSpy.displayedAlert?.title?.string, "Não conseguimos carregar as opções")
    }
    
    func testTryAgainButtonHandler_WhenLoadOptionsErrorOccur_ShouldDisplayRetryLoadOptionsList() throws {
        sut.presentError(error: RequestError())
        
        XCTAssertEqual(controllerSpy.displayErrorCallCount, 1)
        let alert = try XCTUnwrap(controllerSpy.displayedAlert)
        
        let tryAgainButton = alert.buttons.first(where: { $0.type == .cta })
        XCTAssertNotNil(tryAgainButton)
        tryAgainButton?.handler?(AlertPopupViewController(alert: alert, controller: nil), UIPPButton())
        
        XCTAssertEqual(controllerSpy.displayRetryLoadOptionsCallCount, 1)
    }
    
    func testBackButtonHandler_WhenLoadOptionsErrorOccur_ShouldPerformCloseActionOnCoordinator() throws {
        sut.presentError(error: RequestError())
        
        XCTAssertEqual(controllerSpy.displayErrorCallCount, 1)
        let alert = try XCTUnwrap(controllerSpy.displayedAlert)
        
        let backButton = alert.buttons.first(where: { $0.type == .cleanUnderlined })
        XCTAssertNotNil(backButton)
        backButton?.handler?(AlertPopupViewController(alert: alert, controller: nil), UIPPButton())
        
        XCTAssertEqual(coordinatorSpy.performCloseActionCallCount, 1)
    }
    
    func testPresentNextStepDisplaysNextStep() {
        sut.presentNextStepFor(NewTFAOption(flow: .authorizedDevice, firstStep: .userData, iconUrl: "", title: "Device"))
        
        XCTAssertNotNil(coordinatorSpy.nextStep)
        XCTAssertEqual(coordinatorSpy.flowSelected, .authorizedDevice)
    }
    
    func testPresentSelfieValidation_WhenOptionIsSelfie_ShouldPerformActionOnCoordinator() {
        sut.presentSelfieValidation(
            with: TFASelfieValidationData(selfie: TFASelfieValidationData.Selfie(token: "token", flow: "TFA")),
            andOption: NewTFAOption(flow: .validationID, firstStep: .selfie, iconUrl: "", title: "")
        )
        
        XCTAssertNotNil(coordinatorSpy.nextStep)
        XCTAssertEqual(coordinatorSpy.flowSelected, .validationID)
        XCTAssertEqual(coordinatorSpy.performValidationIdCallCount, 1)
    }
    
    func testPresentSelfieValidationError_WhenCalledFromViewModel_ShouldPresentErrorOnViewController() {
        sut.presentSelfieValidationError(error: RequestError())
        
        XCTAssertEqual(controllerSpy.displayErrorCallCount, 1)
        XCTAssertNotNil(controllerSpy.displayedAlert)
    }
    
    func testPresentTFABlockedError_WhenCalledFromViewModel_ShouldDisplayErrorOnViewController() {
        sut.presentTFABlockedError(RequestError())
        
        XCTAssertEqual(controllerSpy.displayErrorCallCount, 1)
    }
    
    func testOkButtonHandler_WhenTFABlockedErrorOccur_ShouldPerformBlockedActionOnCoordinator() throws {
        sut.presentTFABlockedError(TFAModelsHelper().tfaBlockedErrorJson())
        
        XCTAssertEqual(controllerSpy.displayErrorCallCount, 1)
        let alert = try XCTUnwrap(controllerSpy.displayedAlert)
        
        let okButton = alert.buttons.first(where: { $0.type == .cta })
        XCTAssertNotNil(okButton)
        okButton?.handler?(AlertPopupViewController(alert: alert, controller: nil), UIPPButton())
        
        XCTAssertEqual(coordinatorSpy.performBlockedActionCallCount, 1)
    }
}

private final class NewTFAOptionsCoordinatorSpy: NewTFAOptionsCoordinating {
    var viewController: UIViewController?
    var delegate: NewTFAOptionsCoordinatorDelegate?
    
    private(set) var nextStep: NewTFAOptionsAction?
    private(set) var flowSelected: TFAType?
    private(set) var performCloseActionCallCount = 0
    private(set) var performValidationIdCallCount = 0
    private(set) var performBlockedActionCallCount = 0
    
    func perform(action: NewTFAOptionsAction) {
        nextStep = action
        switch action {
        case .close:
            performCloseActionCallCount += 1
        case let .selectedOption(optionType):
            flowSelected = optionType.flow
        case let .validationId(_, option):
            performValidationIdCallCount += 1
            flowSelected = option.flow
        case .blocked:
            performBlockedActionCallCount += 1
        }
    }
}

private final class NewTFAOptionsViewControllerSpy: NewTFAOptionsDisplay {
    private(set) var displayOptionsCallCount = 0
    private(set) var displayFallbackCallCount = 0
    private(set) var displayLoadingCallCount = 0
    private(set) var displayErrorCallCount = 0
    private(set) var displayedAlert: PicPay.Alert?
    private(set) var displayRetryLoadOptionsCallCount = 0
    
    func displayOptions(_ options: [Section<String, TFAOptionCellViewModel>]) {
        displayOptionsCallCount += 1
    }

    func displayFallback(message: String) {
        displayFallbackCallCount += 1
    }
    
    func displayLoading(_ loading: Bool) {
        displayLoadingCallCount += 1
    }
    
    func displayError(alert: PicPay.Alert) {
        displayErrorCallCount += 1
        displayedAlert = alert
    }
    
    func displayRetryLoadOptions() {
        displayRetryLoadOptionsCallCount += 1
    }
}
