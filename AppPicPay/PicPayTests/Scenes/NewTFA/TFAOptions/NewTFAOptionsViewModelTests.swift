import Core
import FeatureFlag
import XCTest

@testable import PicPay

final class NewTFAOptionsViewModelTests: XCTestCase {
    private lazy var mock = NewTFAOptionsServiceMock()
    private lazy var spy = NewTFAOptionsPresenterSpy()
    private lazy var logDetectionServiceSpy = LogDetectionServiceSpy()
    
    private lazy var featureManagerMock = FeatureManagerMock()
    private lazy var mockDependencies = DependencyContainerMock(featureManagerMock)

    private let mockedOptionsSuccess: Result<NewTFAOptions, ApiError> = .success(
        NewTFAOptions(options: [
            NewTFAOption(flow: .authorizedDevice, firstStep: .code, iconUrl: "", title: "Aparelho antigo"),
            NewTFAOption( flow: .creditCard, firstStep: .creditCard, iconUrl: "", title: "Cartão de Crédito"),
            NewTFAOption( flow: .sms, firstStep: .userData, iconUrl: "", title: "SMS"),
            NewTFAOption( flow: .email, firstStep: .userData, iconUrl: "", title: "Email")
        ], fallback: nil)
    )

    private let fallbackOptionsSuccess: Result<NewTFAOptions, ApiError> = .success(
        NewTFAOptions(options: [NewTFAOption( flow: .sms, firstStep: .userData, iconUrl: "", title: "SMS")],
                      fallback: NewTFAOption(flow: .validationID, firstStep: .document, iconUrl: "", title: ""))
    )
    
    private lazy var sut: NewTFAOptionsViewModel = {
        let tfaRequestData = TFARequestData(flow: .sms, step: .initial, id: "1")
        let viewModel = NewTFAOptionsViewModel(
            service: mock,
            presenter: spy,
            dependencies: mockDependencies,
            tfaRequestData: tfaRequestData,
            logDetectionService: logDetectionServiceSpy
        )
        return viewModel
    }()
    
    func testLoadOptionsCallsPresentOptionsWhenServiceReturnsSuccess() {
        mock.tfaOptionExpectedResult = mockedOptionsSuccess
        featureManagerMock.override(key: .featureTfaCreditcardFlowActive, with: true)
        
        sut.loadOptions()
        
        XCTAssertNotNil(spy.presentedOptions)
        XCTAssertEqual(spy.presentedOptions?.count, 4)
    }

    func testLoadOptions_WhenResultIsSuccessAndFallbackIsNotNil_ShouldPresentFallback() {
        mock.tfaOptionExpectedResult = fallbackOptionsSuccess

        sut.loadOptions()

        XCTAssertEqual(spy.presentFallbackCallCount, 1)
    }
    
    func testLoadOptions_WhenCreditCardFlagIsInactive_ShouldRemoveCreditCardOption() {
        featureManagerMock.override(key: .featureTfaCreditcardFlowActive, with: false)
        mock.tfaOptionExpectedResult = mockedOptionsSuccess
        
        sut.loadOptions()
        XCTAssertNotNil(spy.presentedOptions)
        XCTAssertFalse(spy.presentedOptions?.contains(where: { $0.flow == .creditCard }) ?? true)
    }
    
    func testLoadOptions_WhenCreditCardFlagIsActive_ShouldShowCreditCardOption() {
        featureManagerMock.override(key: .featureTfaCreditcardFlowActive, with: true)
        mock.tfaOptionExpectedResult = mockedOptionsSuccess
        
        sut.loadOptions()
        XCTAssertNotNil(spy.presentedOptions)
        XCTAssertTrue(spy.presentedOptions?.contains(where: { $0.flow == .creditCard }) ?? false)
    }
    
    func testLoadOptions_WhenServiceFailures_ShouldPresentError() {
        mock.tfaOptionExpectedResult = .failure(.serverError)
        sut.loadOptions()
        
        XCTAssertNil(spy.presentedOptions)
        XCTAssertNotNil(spy.presentedError)
        XCTAssertTrue(spy.presentErrorCalled)
    }
    
    func testLoadOptionsCallsPresentGenericErrorWhenServiceReturnsError() {
        mock.tfaOptionExpectedResult = .failure(.serverError)
        sut.loadOptions()
        
        XCTAssertNil(spy.presentedOptions)
        XCTAssertTrue(spy.presentErrorCalled)
        XCTAssertNotNil(spy.presentedError)
        XCTAssertEqual(spy.presentedError?.title, "Problema na conexão")
        XCTAssertEqual(spy.presentedError?.message, "Ocorreu um erro ao carregar as informações, tente novamente.")
        XCTAssertEqual(spy.presentedError?.code, "Empty code")
    }
 
    func testLoadingState() {
        sut.showLoading(true)
        XCTAssertTrue(spy.isLoading)
        
        sut.showLoading(false)
        XCTAssertFalse(spy.isLoading)
    }
    
    func testSelectItem_WhenCalledFromViewModel_ShouldPresentsNextStep() {
        mock.tfaOptionExpectedResult = mockedOptionsSuccess
        
        sut.loadOptions()
        sut.selectedItem(IndexPath(row: 0, section: 0))
        
        XCTAssertNotNil(spy.flowSelected)
        XCTAssertEqual(spy.flowSelected, .authorizedDevice)
    }

    func testSelectFallback_WhenFallbackOptionIsNil_ShouldDoNothing() {
        sut.selectFallback()

        XCTAssertEqual(mock.startValidationCallCount, 0)
    }

    func testSelectFallback_ShouldStartValidation() {
        mock.tfaOptionExpectedResult = fallbackOptionsSuccess
        sut.loadOptions()

        sut.selectFallback()

        XCTAssertEqual(mock.startValidationCallCount, 1)
    }
    
    func testSelectSelfieValidationItem_WhenServiceIsSuccess_ShouldReceiveToken() {
        mock.tfaOptionExpectedResult = .success(
            NewTFAOptions(options: [
                NewTFAOption(flow: .validationID, firstStep: .selfie, iconUrl: "", title: "Validar Biometria")
            ], fallback: nil)
        )
        mock.startValidationExpectedResult = .success(
            TFASelfieValidationData(selfie: TFASelfieValidationData.Selfie(token: "token", flow: "TFA"))
        )

        sut.loadOptions()
        sut.selectedItem(IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(spy.presentSelfieValidationCallCount, 1)
        XCTAssertNotNil(spy.selfieValidationData)
        XCTAssertEqual(spy.selfieValidationData?.selfie.token, "token")
    }
    
    func testSelectSelfieValidationItem_WhenServiceIsFailure_ShouldPresentError() {
        mock.tfaOptionExpectedResult = .success(
            NewTFAOptions(options: [
                NewTFAOption(flow: .validationID, firstStep: .selfie, iconUrl: "", title: "Validar Biometria")
            ], fallback: nil)
        )
        mock.startValidationExpectedResult = .failure(.serverError)

        sut.loadOptions()
        sut.selectedItem(IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(spy.presentSelfieValidationErrorCallCount, 1)
        XCTAssertNil(spy.selfieValidationData)
        XCTAssertEqual(logDetectionServiceSpy.sendLogCallCount, 1)
    }
    
    func testLoadOptions_WhenTFAIsBlocked_ShouldPresentTFABlockedError() {
        mock.tfaOptionExpectedResult = .failure(.unauthorized(body: TFAModelsHelper().tfaBlockedErrorJson()))
        sut.loadOptions()
        
        XCTAssertEqual(spy.presentBlockedErrorCallCount, 1)
    }
}

private final class NewTFAOptionsServiceMock: NewTFAOptionsServicing {
    private(set) var startValidationCallCount = 0
    var startValidationExpectedResult: Result<TFASelfieValidationData, ApiError>?
    var tfaOptionExpectedResult: Result<NewTFAOptions, ApiError>?

    func tfaOptions(with data: TFARequestData, completion: @escaping (Result<NewTFAOptions, ApiError>) -> Void) {
        guard let expectedResult = tfaOptionExpectedResult else {
            return
        }
        completion(expectedResult)
    }
    
    func startValidation(with data: TFARequestData, completion: @escaping (Result<TFASelfieValidationData, ApiError>) -> Void) {
        startValidationCallCount += 1
        guard let expectedResult = startValidationExpectedResult else {
            return
        }
        completion(expectedResult)
    }
}

private final class NewTFAOptionsPresenterSpy: NewTFAOptionsPresenting {
    var viewController: NewTFAOptionsDisplay?
    
    private(set) var presentedOptions: [NewTFAOption]?
    private(set) var presentFallbackCallCount = 0
    private(set) var presentErrorCalled = false
    private(set) var presentedError: RequestError?
    private(set) var isLoading = false
    private(set) var flowSelected: TFAType?
    private(set) var presentSelfieValidationCallCount = 0
    private(set) var presentSelfieValidationErrorCallCount = 0
    private(set) var selfieValidationData: TFASelfieValidationData?
    private(set) var presentBlockedErrorCallCount = 0
    
    func presentOptions(_ options: [NewTFAOption]) {
        presentedOptions = options
    }

    func presentFallback(_ title: String) {
        presentFallbackCallCount += 1
    }
    
    func presentNextStepFor(_ option: NewTFAOption) {
        flowSelected = option.flow
    }
    
    func presentLoading(_ loading: Bool) {
        isLoading = loading
    }
    
    func presentError(error: RequestError) {
        presentedError = error
        presentErrorCalled = true
    }
    
    func presentSelfieValidation(with validationData: TFASelfieValidationData, andOption option: NewTFAOption) {
        selfieValidationData = validationData
        presentSelfieValidationCallCount += 1
    }
    
    func presentSelfieValidationError(error: RequestError) {
        presentSelfieValidationErrorCallCount += 1
    }
    
    func presentTFABlockedError(_ error: RequestError) {
        presentBlockedErrorCallCount += 1
        presentedError = error
    }
}
