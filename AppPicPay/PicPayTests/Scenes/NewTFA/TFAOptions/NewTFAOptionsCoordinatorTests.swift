@testable import PicPay
import XCTest

final class NewTFAOptionsCoordinatorTests: XCTestCase {
    private lazy var navController = NavigationControllerMock(rootViewController: UIViewController())
    private lazy var delegateSpy: NewTFAOptionsCoordinatorDelegateSpy = {
        let coordinatorDelegate = NewTFAOptionsCoordinatorDelegateSpy()
        return coordinatorDelegate
    }()
    
    private lazy var sut: NewTFAOptionsCoordinator = {
        let coordinator = NewTFAOptionsCoordinator()
        coordinator.delegate = delegateSpy
        coordinator.viewController = navController.topViewController
        return coordinator
    }()
    
    func testPerform_WhenActionIsSelectedOption_ShouldCallDelegateWithSelectedOption() {
        sut.perform(
            action: .selectedOption(
                option: NewTFAOption(flow: .authorizedDevice,
                                     firstStep: .userData,
                                     iconUrl: "",
                                     title: "Device")
            )
        )
        
        XCTAssertEqual(delegateSpy.selectOptionCallCount, 1)
        XCTAssertEqual(delegateSpy.selectedOption, .authorizedDevice)
    }
    
    func testPerform_WhenActionIsSelectedValidationId_ShouldCallDelegateWithValidationIdData() {
        sut.perform(action:
            .validationId(
                validationData: TFASelfieValidationData(selfie: TFASelfieValidationData.Selfie(token: "token", flow: "TFA")),
                option: NewTFAOption.init(
                    flow: .validationID,
                    firstStep: .selfie,
                    iconUrl: "",
                    title: ""
                )
            )
        )
        
        XCTAssertEqual(delegateSpy.selectValidationIDCallCount, 1)
        XCTAssertEqual(delegateSpy.selectedOption, .validationID)
    }
    
    func testPerform_WhenActionIsClose_ShouldPopViewController() {
        sut.perform(action: .close)
        
        XCTAssertTrue(navController.isPopViewControllerCalled)
    }
    
    func testPerform_WhenActionIsBlocked_ShouldCallDelegateWithDidReceivedBlockedError() {
        sut.perform(action: .blocked)
        
        XCTAssertEqual(delegateSpy.receivedBlockedErrorCallCount, 1)
    }
}

private final class NewTFAOptionsCoordinatorDelegateSpy: NewTFAOptionsCoordinatorDelegate {
    private(set) var selectOptionCallCount = 0
    private(set) var selectValidationIDCallCount = 0
    private(set) var selectedOption: TFAType?
    private(set) var receivedBlockedErrorCallCount = 0
    
    func didSelectOption(option: NewTFAOption) {
        selectOptionCallCount += 1
        selectedOption = option.flow
    }
    
    func didSelectValidationId(validationData: TFASelfieValidationData, option: NewTFAOption) {
        selectValidationIDCallCount += 1
        selectedOption = option.flow
    }
    
    func didReceivedBlockedError() {
        receivedBlockedErrorCallCount += 1
    }
}
