@testable import PicPay
import XCTest

private final class PhoneNumberVerificationCoordinatorDelegateSpy: PhoneNumberVerificationCoordinatorDelegate {
    private(set) var didValidatePhoneNumberCallCount = 0
    private(set) var nextStepReceived = TFAStep.initial
    
    func didValidatePhoneNumber(with tfaType: TFAType, nextStep: TFAStep) {
        didValidatePhoneNumberCallCount += 1
        nextStepReceived = nextStep
    }
}

final class PhoneNumberVerificationCoordinatorTests: XCTestCase {
    private let navController = NavigationControllerMock(rootViewController: UIViewController())
    private lazy var delegateSpy = PhoneNumberVerificationCoordinatorDelegateSpy()
    
    private lazy var sut: PhoneNumberVerificationCoordinator = {
        let coordinator = PhoneNumberVerificationCoordinator()
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerform_WhenActionIsPhoneNumberValidateAndStartNextStep_ShouldCallDelegateWithNextStep() {
        sut.perform(action: .phoneNumberValidateAndStartNextStep(tfaType: .email, nextStep: .bankData))
        
        XCTAssertEqual(delegateSpy.didValidatePhoneNumberCallCount, 1)
        XCTAssertEqual(delegateSpy.nextStepReceived, .bankData)
    }
}
