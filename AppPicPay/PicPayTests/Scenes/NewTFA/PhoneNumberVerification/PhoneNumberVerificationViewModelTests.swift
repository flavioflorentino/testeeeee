import Core
@testable import PicPay
import Validator
import XCTest
import UI

final class PhoneNumberVerificationViewModelTests: XCTestCase {
    private lazy var mock: PhoneNumberVerificationServiceMock = {
        let service = PhoneNumberVerificationServiceMock()
        return service
    }()
    
    private lazy var spy: PhoneNumberVerificationPresenterSpy = {
        let presenter = PhoneNumberVerificationPresenterSpy()
        return presenter
    }()
    
    private lazy var sut: PhoneNumberVerificationViewModel = {
        let viewModel = PhoneNumberVerificationViewModel(
            service: mock,
            presenter: spy,
            phoneInfo: "88889999",
            tfaRequestData: TFARequestData(flow: .email, step: .initial, id: "123")
        )
        return viewModel
    }()
    
    func testShouldCallPresentHint() {
        sut.loadInfo()
        
        XCTAssertEqual(spy.presentHintForNumberCallCount, 1)
    }
    
    func testShouldSetupTextFields() {
        sut.buildTextfieldSettings()
        
        XCTAssertEqual(spy.setupFieldsRulesCallCount, 1)
    }
    
    func testShouldPresentErrorMessageForInvalidInput() {
        var field = UIPPFloatingTextField()
        var phoneRules = ValidationRuleSet<String>()
        phoneRules.add(rule: ValidationRuleLength(min: 15, max: 15, lengthType: .characters, error: PhoneNumberVerificationValidationError.phoneNumberRequired))
        field.validationRules = phoneRules
        field.text = "1312"
        
        sut.checkTextfield(textfield: field)
        
        XCTAssertNotNil(spy.fieldErrorMessage)
        XCTAssertEqual(spy.fieldErrorMessage, PhoneNumberVerificationValidationError.phoneNumberRequired.message)
    }
    
    func testShouldNotPresentErrorMessageForValidInput() {
        var field = UIPPFloatingTextField()
        var phoneRules = ValidationRuleSet<String>()
        phoneRules.add(rule: ValidationRuleLength(min: 15, max: 15, lengthType: .characters, error: PhoneNumberVerificationValidationError.phoneNumberRequired))
        field.validationRules = phoneRules
        field.text = "(11) 98888-7777"
        
        sut.checkTextfield(textfield: field)
        
        XCTAssertNil(spy.fieldErrorMessage)
    }
    
    func testShouldEnableContinueButtonWithValidInput() {
        var field = UIPPFloatingTextField()
        var phoneRules = ValidationRuleSet<String>()
        phoneRules.add(rule: ValidationRuleLength(min: 15, max: 15, lengthType: .characters, error: PhoneNumberVerificationValidationError.phoneNumberRequired))
        field.validationRules = phoneRules
        field.text = "(11) 98888-7777"
        
        sut.checkTextfieldsAndEnableContinue(textFields: [field])
        
        XCTAssertTrue(spy.continueButtonEnabled)
    }
    
    func testValidateForm_WhenServerHasError_ShoulPresentsGenericError() {
        sut.validateForm(phoneNumber: "88889999")
        
        XCTAssertEqual(spy.presentGenericErrorCallCount, 1)
    }
    
    func testValidateForm_WhenInputIsInvalid_ShoulPresentNotAuthorizedError() {
        mock.isInvalidInput = true
        sut.validateForm(phoneNumber: "88889999")
        
        XCTAssertNotNil(spy.validationError)
        XCTAssertEqual(spy.presentValidationInfoErrorCallCount, 1)
        XCTAssertEqual(spy.validationError?.message, "Dados inválidos")
    }
    
    func testvalidateForm_WhenAttemptLimitReached_ShouldPresentAttempLimitError() {
        mock.hasReachLimit = true
        sut.validateForm(phoneNumber: "88889999")
        
        XCTAssertNotNil(spy.validationError)
        XCTAssertEqual(spy.presentAttemptLimitErrorCallCount, 1)
        XCTAssertEqual(spy.validationError?.title, "Limite atingido")
    }
    
    func testStartTimerForNewValidation_WhenRemainingTimeBiggerThanZero_ShouldNotStopTimer() {
        sut.startTimerForNewValidation(retryTime: 60)
        
        let expect = expectation(waitForCondition: self.spy.continueButtonRemainingTimeForNewAttempt < 57) {
            XCTAssertFalse(self.spy.timerFinished)
        }
        
        wait(for: [expect], timeout: 5)
    }
    
    func testStartTimerForNewValidation_WhenAttemptLimitReached_ShouldExpectTimeToReachZero() {
        sut.startTimerForNewValidation(retryTime: 4)
        
        let expect = expectation(waitForCondition: self.spy.continueButtonRemainingTimeForNewAttempt <= 0)
        
        wait(for: [expect], timeout: 6)
    }
    
    func testShouldLoadBankInfoWhenBankInfoIsNil() {
        let viewModel = PhoneNumberVerificationViewModel(
            service: mock,
            presenter: spy,
            phoneInfo: nil,
            tfaRequestData: TFARequestData(flow: .email, step: .initial, id: "123")
        )
        sut = viewModel
        
        mock.isSuccess = true
        sut.loadInfo()
        
        XCTAssertEqual(spy.presentLoadingScreenCallCount, 2)
        XCTAssertNotNil(spy.phoneNumberHint)
        XCTAssertEqual(spy.phoneNumberHint, "****-9999")
    }
       
    func testValidateForm_WhenCalledFromViewModel_ShouldPresentLoadingState() {
        sut.validateForm(phoneNumber: "88889999")
        
        XCTAssertEqual(spy.presentLoadingCallCount, 2)
    }

    func testValidateForm_WhenServerHasSucceed_ShouldCallPresenterValidateDataForNextStep() {
        mock.isSuccess = true
        
        sut.validateForm(phoneNumber: "88889999")
        
        XCTAssertEqual(spy.validateDataForNextStepCallCount, 1)
        XCTAssertNotNil(spy.nextStepCalled)
        XCTAssertEqual(spy.nextStepCalled, .code)
    }

    func testValidateForm_WhenServerHasError_ShouldPresentGenericError() {
        sut.validateForm(phoneNumber: "88889999")
        
        XCTAssertEqual(spy.presentGenericErrorCallCount, 1)
    }
}

private final class PhoneNumberVerificationServiceMock: PhoneNumberVerificationServicing {
    var isSuccess = false
    var hasReachLimit = false
    var isInvalidInput = false
    
    func getPhoneNumberData(tfaRequestData: TFARequestData, completion: @escaping (Result<TFAPhoneNumberInfo, ApiError>) -> Void) {
        if isSuccess {
            completion(.success(TFAPhoneNumberInfo(phone: TFAPhoneNumberInfo.TFAPhoneNumber(phoneNumber: "****-9999"))))
        } else {
            completion(.failure(.serverError))
        }
    }
    
    func validatePhoneNumberData(phone: String, tfaRequestData: TFARequestData, completion: @escaping (Result<TFAValidationResponse, ApiError>) -> Void) {
        if isSuccess {
            completion(.success(TFAValidationResponse.init(nextStep: .code)))
        } else {
            if hasReachLimit {
                completion(.failure(ApiError.badRequest(body: TFAModelsHelper().attempLimitErrorJson())))
            } else if isInvalidInput {
                completion(.failure(ApiError.unauthorized(body: TFAModelsHelper().invalidDataErrorJson())))
            } else {
                completion(.failure(ApiError.serverError))
            }
        }
    }
}

private final class PhoneNumberVerificationPresenterSpy: PhoneNumberVerificationPresenting {
    var viewController: PhoneNumberVerificationDisplay?
    
    private(set) var presentHintForNumberCallCount = 0
    private(set) var setupFieldsRulesCallCount = 0
    private(set) var fieldErrorMessage: String?
    private(set) var continueButtonEnabled = false
    private(set) var presentGenericErrorCallCount = 0
    private(set) var presentValidationInfoErrorCallCount = 0
    private(set) var presentAttemptLimitErrorCallCount = 0
    private(set) var continueButtonRemainingTimeForNewAttempt: TimeInterval = 0
    private(set) var timerFinished = false
    private(set) var validationError: RequestError?
    private(set) var phoneNumberHint: String?
    private(set) var presentLoadingScreenCallCount = 0
    private(set) var presentLoadingCallCount = 0
    private(set) var nextStepCalled: TFAStep?
    private(set) var validateDataForNextStepCallCount = 0
    
    func setFieldsRules(settings: PhoneNumberVerificationTextFieldSettings) {
        setupFieldsRulesCallCount += 1
    }
    
    func presentHintForNumber(hint: String?) {
        presentHintForNumberCallCount += 1
        phoneNumberHint = hint
    }
    
    func updateTextFieldErrorMessage(textfield: UIPPFloatingTextField, message: String?) {
        fieldErrorMessage = message
    }
    
    func shouldEnableContinueButton(enable: Bool) {
        continueButtonEnabled = enable
    }
    
    func validateData(with tfaType: TFAType, nextStep: TFAStep) {
        validateDataForNextStepCallCount += 1
        nextStepCalled = nextStep
    }
    
    func presentLoadingScreen(loading: Bool) {
        presentLoadingScreenCallCount += 1
    }
    
    func setLoading(loading: Bool) {
        presentLoadingCallCount += 1
    }
    
    func presentContinueButtonUpdated(with remainingTime: TimeInterval) {
        continueButtonRemainingTimeForNewAttempt = remainingTime
        timerFinished = remainingTime <= 0
    }
    
    func presentValidationInfoError(tfaError: RequestError) {
        presentValidationInfoErrorCallCount += 1
        validationError = tfaError
    }
    
    func presentAttempLimitError(tfaError: RequestError, retryTime: Int, tfaType: TFAType) {
        validationError = tfaError
        presentAttemptLimitErrorCallCount += 1
    }
    
    func presentGenericError() {
        presentGenericErrorCallCount += 1
    }
}
