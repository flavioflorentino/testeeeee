@testable import PicPay
import XCTest
import IdentityValidation

final class TFAFlowCoordinatorTests: XCTestCase {
    private var finishedWithSuccess: Bool?
    private lazy var mockDependencies = DependencyContainerMock()
    private let presenterMock = NavigationControllerMock(rootViewController: UIViewController())
    
    private lazy var sut: TFAFlowCoordinator = {
        return TFAFlowCoordinator(
            id: "123",
            presenter: presenterMock,
            navigation: CustomPopNavigationControllerMock(),
            onFinish: finishBlock(_:)
        )
    }()
    
    override func setUp() {
        super.setUp()
        let window = UIWindow()
        window.rootViewController = presenterMock
        window.makeKeyAndVisible()
        sut.start()
    }
    
    override func tearDown() {
        super.tearDown()
        finishedWithSuccess = nil
    }
    
    func test_Start_ShouldPresentOnboarding() {
        XCTAssertTrue(presenterMock.isPresentViewControllerCalled)
        let navigation = presenterMock.viewControllerPresented as? CustomPopNavigationControllerMock
        XCTAssertNotNil(navigation)
        XCTAssertTrue(navigation?.topViewController?.isKind(of: NewTFAOnboardingViewController.self) ?? false)
    }
    
    func test_OnboardingShowHelp_ShouldPresent_HelpInfoScreen() {
        sut.onboardingShowHelp()
        check(with: NewTFAInfoViewController.self)
    }
    
    func test_OnboardingDidClose_ShouldDismissFlow_WithFailureResult() {
        sut.onboardingClose()
        
        let navigation = presenterMock.viewControllerPresented as? CustomPopNavigationControllerMock
        expectation(waitForCondition: self.finishedWithSuccess != nil, then: {
            XCTAssertTrue(navigation?.isDismissViewControllerCalled ?? false)
            XCTAssertFalse(self.finishedWithSuccess ?? true)
        })
        
        waitForExpectations(timeout: 2.0)
    }
    
    func test_OnboardingNextStep_ShouldPresentVerificationOptions() {
        sut.onboardingNextStep()
        check(with: NewTFAOptionsViewController.self)
    }
    
    func test_DidSelectOption_WhenFirstStepIsUserData_ShouldPresentPersonalDataVerification() {
        let option = NewTFAOption(flow: .email, firstStep: .userData, iconUrl: "", title: "")
        
        sut.didSelectOption(option: option)
        check(with: TFAPersonalDataCheckViewController.self)
    }
    
    func test_DidValidatePersonalData_WhenNextStepIsBankData_ShouldPresentBankAccountDataVerification() {
        sut.didValidatePersonalData(with: .email, nextStep: .bankData)
        check(with: BankAccountCheckViewController.self)
    }
    
    func test_DidValidatePersonalData_WhenNextStepIsPhoneNumber_ShouldPresentPhoneNumberVerification() {
        sut.didValidatePersonalData(with: .email, nextStep: .phoneNumber)
        check(with: PhoneNumberVerificationViewController.self)
    }
    
    func test_DidValidateBankAccountData_WhenNextStepIsCode_ShouldPresentCodeVerification() {
        sut.didValidateAccountData(with: .email, nextStep: .code)
        check(with: NewTFACodeFormViewController.self)
    }
    
    func test_DidValidatePhoneNumber_WhenNextStepIsCode_ShouldPresentCodeVerification() {
        sut.didValidatePhoneNumber(with: .email, nextStep: .code)
        check(with: NewTFACodeFormViewController.self)
    }
    
    func testDidSelectValidationId_ShouldPresentIdentityValidationCoordinator() {
        sut.didSelectValidationId(
            validationData: TFASelfieValidationData(selfie: TFASelfieValidationData.Selfie(token: "token", flow: "TFA")),
            option: NewTFAOption(flow: .validationID, firstStep: .selfie, iconUrl: "", title: "Validar Biometria")
        )
        
        XCTAssertNotNil(sut.identityValidationCoordinator)
    }
    
    func testDidValidateCodeWithTypeAndFinalStep_ShouldPresentSuccessAlert() {
        sut.didValidateCode(with: .email, nextStep: .final)
        
        let navigation = presenterMock.viewControllerPresented as? CustomPopNavigationControllerMock
        XCTAssertNotNil(navigation)
        expectation(waitForCondition: navigation?.isPresentViewControllerCalled == true) {
            XCTAssertTrue(navigation?.viewControllerPresented?.isKind(of: TFASuccessViewController.self) ?? false)
        }
        
        waitForExpectations(timeout: 3.0)
    }
    
    func testDidEnter_ShouldCallFinish() {
        sut.didEnter()
        
        expectation(waitForCondition: self.finishedWithSuccess == true)
        waitForExpectations(timeout: 3.0)
    }
    
    func testDidReceivedApprovedStatus_ShouldCallFinishedWithSuccess() {
        sut.didReceivedApprovedStatus()
        XCTAssertTrue(finishedWithSuccess ?? false)
    }
    
    func testCloseTFAFlow_ShouldCallFinishWithoutSucces() {
        sut.closeTFAFlow()
        XCTAssertFalse(finishedWithSuccess ?? true)
    }
    
    func testPresentTFAOptions_WhenTFAStatusIsInconclusive_ShouldPresentValidationOptions() {
        sut.presentTFAOptions()
        check(with: NewTFAOptionsViewController.self)
    }
    
    func test_PopViewController_WhenCurrentStepIsInitial_ShouldGoBackWithoutPrompt() {
        sut.onboardingNextStep()
        
        let navigation = presenterMock.viewControllerPresented as? CustomPopNavigationControllerMock
        XCTAssertNotNil(navigation)
        XCTAssertTrue(sut.shouldPop())
        
        _ = navigation?.popViewController(animated: false)
        expectation(waitForCondition: navigation?.isPopViewControllerCalled == true) {
            XCTAssertTrue(navigation?.popedViewController?.isKind(of: NewTFAOptionsViewController.self) ?? false)
            XCTAssertTrue(navigation?.topViewController?.isKind(of: NewTFAOnboardingViewController.self) ?? false)
        }
        
        waitForExpectations(timeout: 3.0)
    }
    
    func test_PopViewController_WhenCurrentStepIsNotInitial_ShouldPresentPrompt() {
        sut.onboardingNextStep()
        let option = NewTFAOption(flow: .email, firstStep: .userData, iconUrl: "", title: "")
        
        sut.didSelectOption(option: option)
        
        let navigation = presenterMock.viewControllerPresented as? CustomPopNavigationControllerMock
        XCTAssertNotNil(navigation)
        XCTAssertFalse(sut.shouldPop())
        
        _ = navigation?.popViewController(animated: false)
        
        expectation(waitForCondition: navigation?.isPresentViewControllerCalled == true) {
            XCTAssertTrue(navigation?.viewControllerPresented?.isKind(of: AlertPopupViewController.self) ?? false)
        }
        
        waitForExpectations(timeout: 3.0)
    }
    
    func test_PopViewController_ShouldGobackToOptionsStep_WhenUserConfirmPrompt() {
        sut.onboardingNextStep()
        let option = NewTFAOption(flow: .email, firstStep: .userData, iconUrl: "", title: "")
        
        sut.didSelectOption(option: option)
        
        let navigation = presenterMock.viewControllerPresented as? CustomPopNavigationControllerMock
        XCTAssertNotNil(navigation)
        
        let pop = sut.shouldPop()
        XCTAssertFalse(pop)
        
        guard let popup = navigation?.viewControllerPresented as? AlertPopupViewController else {
            XCTFail("Popup Not loaded")
            return
        }
        
        let backButton = popup.alert.buttons.first(where: {$0.type == .cleanUnderlined })
        backButton?.handler?(popup, UIPPButton())
        
        self.expectation(waitForCondition: navigation?.isSetViewControllersCalled == true) {
            XCTAssertTrue(navigation?.topViewControllerSetted?.isKind(of: NewTFAOptionsViewController.self) ?? false)
        }
        
        waitForExpectations(timeout: 3.0)
    }
    
    func test_PopViewController_ShouldGobackRootController_WhenUserConfirmPrompt_AndOptionsCannotBeFound() {
        let option = NewTFAOption(flow: .email, firstStep: .userData, iconUrl: "", title: "")
        sut.didSelectOption(option: option)
        
        let navigation = presenterMock.viewControllerPresented as? CustomPopNavigationControllerMock
        XCTAssertNotNil(navigation)
        
        let pop = sut.shouldPop()
        XCTAssertFalse(pop)
        
        guard let popup = navigation?.viewControllerPresented as? AlertPopupViewController else {
            XCTFail("Popup Not loaded")
            return
        }
        
        let backButton = popup.alert.buttons.first(where: {$0.type == .cleanUnderlined })
        backButton?.handler?(popup, UIPPButton())
        
        self.expectation(waitForCondition: navigation?.isPopToRootViewControllerCalled == true) {
            XCTAssertTrue(navigation?.popedToRootViewController?.isKind(of: NewTFAOnboardingViewController.self) ?? false)
        }
        
        waitForExpectations(timeout: 3.0)
    }
    
    func testDidReceivedBlockedError_ShouldCallFinishWithoutSucces() throws {
        sut.didReceivedBlockedError()
        
        let isSuccess = try XCTUnwrap(finishedWithSuccess)
        XCTAssertFalse(isSuccess)
    }
}

private extension TFAFlowCoordinatorTests {
    func finishBlock(_ completed: Bool) {
        self.finishedWithSuccess = completed
    }
    
    func check(with expectedType: AnyClass) {
        let navigation = presenterMock.viewControllerPresented as? CustomPopNavigationControllerMock
        XCTAssertNotNil(navigation)
        expectation(waitForCondition: navigation?.isPushViewControllerCalled == true) {
            XCTAssertTrue(navigation?.pushedViewController?.isKind(of: expectedType) ?? false)
        }
        
        waitForExpectations(timeout: 5.0)
    }
}
