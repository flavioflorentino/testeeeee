import Core
@testable import PicPay
import XCTest

final class TFACreditCardViewModelTests: XCTestCase {
    private lazy var mock = TFACreditCardServiceMock()
    private lazy var spy = TFACreditCardPresenterSpy()
    
    private lazy var sut: TFACreditCardViewModel = {
        let tfaRequestData = TFARequestData(flow: .creditCard, step: .creditCard, id: "1")
        let viewModel = TFACreditCardViewModel(
            service: mock,
            presenter: spy,
            tfaRequestData: tfaRequestData
        )
        return viewModel
    }()
    
    func testLoadCards_ShouldCallPresentLoadingTwice() {
        sut.loadCards()
        
        XCTAssertEqual(spy.presentLoadingCallCount, 2)
    }
    
    func testLoadCards_WhenServiceIsSuccessful_ShouldCallPresentCardListWithTwoCards() {
        mock.isSuccess = true
        sut.loadCards()
        
        XCTAssertEqual(spy.presentCardListCallCount, 1)
        XCTAssertEqual(spy.cardListPresented.count, 2)
        XCTAssertEqual(spy.cardListPresented[0].flagName, "master")
        XCTAssertEqual(spy.cardListPresented[1].flagName, "visa")
        XCTAssertEqual(spy.cardListPresented[0].lastFour, "1234")
        XCTAssertEqual(spy.cardListPresented[1].lastFour, "5678")
    }
    
    func testLoadCards_WhenServiceHasFailed_ShouldCallPresentCardLoadError() {
        sut.loadCards()
        
        XCTAssertEqual(spy.presentCardLoadErrorCallCount, 1)
    }
    
    func testSelecteCard_WhenServiceIsSuccessful_ShouldCallPresentDidNextStep() {
        mock.isSuccess = true
        sut.loadCards()
        sut.selectedItem(IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(spy.didNextStepCallCount, 1)
    }
}

private final class TFACreditCardServiceMock: TFACreditCardServicing {
    var isSuccess = false
    
    func loadTFACard(with data: TFARequestData, completion: @escaping (Result<TFACreditCardList, ApiError>) -> Void) {
        if isSuccess {
            let card1 = TFACreditCardData(id: 1, flagName: "master", lastFour: "1234", urlFlagLogoImage: "")
            let card2 = TFACreditCardData(id: 1, flagName: "visa", lastFour: "5678", urlFlagLogoImage: "")
            completion(.success(TFACreditCardList(creditCard: [card1, card2])))
        } else {
            completion(.failure(ApiError.serverError))
        }
    }
}

private final class TFACreditCardPresenterSpy: TFACreditCardPresenting {
    var viewController: TFACreditCardDisplay?
    private(set) var presentLoadingCallCount = 0
    private(set) var presentCardListCallCount = 0
    private(set) var cardListPresented: [TFACreditCardData] = []
    private(set) var presentCardLoadErrorCallCount = 0
    private(set) var didNextStepCallCount = 0
    private(set) var didNextStepAction: TFACreditCardAction?
    
    func presentLoading(_ loading: Bool) {
        presentLoadingCallCount += 1
    }
    
    func didNextStep(action: TFACreditCardAction) {
        didNextStepCallCount += 1
    }
    
    func presentCardList(_ cardList: [TFACreditCardData]) {
        presentCardListCallCount += 1
        cardListPresented = cardList
    }
    
    func presentCardLoadError(_ error: ApiError) {
        presentCardLoadErrorCallCount += 1
    }
}
