import Core
@testable import PicPay
import UI
import XCTest

final class TFACreditCardPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = TFACreditCoordinatorSpy()
    private lazy var viewControllerSpy = TFACreditCardDisplaySpy()
    
    private lazy var sut: TFACreditCardPresenter = {
        let presenter = TFACreditCardPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPresentCardList_ShouldDisplayCards() {
        let cardList = [TFACreditCardData(id: 1, flagName: "master", lastFour: "1234", urlFlagLogoImage: "")]
        sut.presentCardList(cardList)
        
        XCTAssertEqual(viewControllerSpy.displayCardsCallCount, 1)
    }
    
    func testPresentLoading_WhenLoadingIsTrue_ShouldDisplayLoading() {
        sut.presentLoading(true)
        
        XCTAssertEqual(viewControllerSpy.displayLoadingCallCount, 1)
        XCTAssertTrue(viewControllerSpy.isLoading)
    }
    
    func testPresentLoading_WhenLoadingIsFalse_ShouldNotDisplayLoading() {
        sut.presentLoading(false)
        
        XCTAssertEqual(viewControllerSpy.displayLoadingCallCount, 1)
        XCTAssertFalse(viewControllerSpy.isLoading)
    }
    
    func testPresentCardLoadError_ShouldDisplayCardLoadErrorAlert() {
        sut.presentCardLoadError(ApiError.serverError)
        
        XCTAssertEqual(viewControllerSpy.displayCardLoadErrorCallCount, 1)
        XCTAssertNotNil(viewControllerSpy.errorAlert)
        XCTAssertEqual(viewControllerSpy.errorAlert?.title?.string, "Não conseguimos carregar os seus cartões")
    }
    
    func testDidNextStep_WhenActionIsValidateSelectedCard_ShouldPerformActionWithSelectedCard() {
        let card = TFACreditCardData(id: 1, flagName: "visa", lastFour: "1234", urlFlagLogoImage: "")
        sut.didNextStep(action: .validateSelectedCard(tfaCard: card))
        
        XCTAssertEqual(coordinatorSpy.performValidateSelectedCardCallCount, 1)
        XCTAssertNotNil(coordinatorSpy.selectedCardData)
        XCTAssertEqual(coordinatorSpy.selectedCardData?.flagName, "visa")
        XCTAssertEqual(coordinatorSpy.selectedCardData?.lastFour, "1234")
    }
    
    func testTryAgainButtonHandler_WhenLoadCardsErrorOccur_ShouldDisplayRetryLoadCardList() throws {
        sut.presentCardLoadError(ApiError.serverError)
        
        XCTAssertEqual(viewControllerSpy.displayCardLoadErrorCallCount, 1)
        let alert = try XCTUnwrap(viewControllerSpy.errorAlert)
        
        let tryAgainButton = alert.buttons.first(where: { $0.type == .cta })
        XCTAssertNotNil(tryAgainButton)
        tryAgainButton?.handler?(AlertPopupViewController(alert: alert, controller: nil), UIPPButton())
        
        XCTAssertEqual(viewControllerSpy.displayRetryLoadCardListCallCount, 1)
    }
    
    func testAnotherAuthorizationButtonHandler_WhenLoadCardsErrorOccur_ShouldPerformTryAnotherAuthorizationAction() throws {
        sut.presentCardLoadError(ApiError.serverError)
        
        XCTAssertEqual(viewControllerSpy.displayCardLoadErrorCallCount, 1)
        let alert = try XCTUnwrap(viewControllerSpy.errorAlert)
        
        let anotherAuthorizationButton = alert.buttons.first(where: { $0.type == .cleanUnderlined })
        XCTAssertNotNil(anotherAuthorizationButton)
        anotherAuthorizationButton?.handler?(AlertPopupViewController(alert: alert, controller: nil), UIPPButton())
        
        XCTAssertEqual(coordinatorSpy.performAnotherAuthorizationMethodCallCount, 1)
    }
}

private final class TFACreditCoordinatorSpy: TFACreditCardCoordinating {
    var viewController: UIViewController?
    var delegate: TFACreditCardCoordinatorDelegate?
    private(set) var performAnotherAuthorizationMethodCallCount = 0
    private(set) var performValidateSelectedCardCallCount = 0
    private(set) var selectedCardData: TFACreditCardData?
    
    func perform(action: TFACreditCardAction) {
        switch action {
        case .tryAnotherAuthorizationMethod:
            performAnotherAuthorizationMethodCallCount += 1
        case let .validateSelectedCard(tfaCard):
            performValidateSelectedCardCallCount += 1
            selectedCardData = tfaCard
        }
    }
}

private final class TFACreditCardDisplaySpy: TFACreditCardDisplay {
    private(set) var displayCardsCallCount = 0
    private(set) var displayLoadingCallCount = 0
    private(set) var isLoading = false
    private(set) var displayCardLoadErrorCallCount = 0
    private(set) var errorAlert: PicPay.Alert?
    private(set) var displayRetryLoadCardListCallCount = 0
    
    func displayLoading(_ loading: Bool) {
        isLoading = loading
        displayLoadingCallCount += 1
    }
    
    func displayCards(_ cards: [Section<String, TFACardCellViewModel>]) {
        displayCardsCallCount += 1
    }
    
    func displayCardListLoadErrorAlert(_ alert: PicPay.Alert) {
        displayCardLoadErrorCallCount += 1
        errorAlert = alert
    }
    
    func displayRetryLoadCardList() {
        displayRetryLoadCardListCallCount += 1
    }
}

