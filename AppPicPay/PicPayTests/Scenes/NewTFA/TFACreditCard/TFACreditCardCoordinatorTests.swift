@testable import PicPay
import XCTest

private final class TFACreditCardCoordinatorDelegateSpy: TFACreditCardCoordinatorDelegate {
    private(set) var didTryAnotherAuthorizationMethodCallCount = 0
    private(set) var didTryValidateTfaCardCallCount = 0
    private(set) var cardSelected: TFACreditCardData?
    
    func didTryAnotherAuthorizationMethod() {
        didTryAnotherAuthorizationMethodCallCount += 1
    }
    
    func didTryValidateTfaCard(with card: TFACreditCardData) {
        didTryValidateTfaCardCallCount += 1
        cardSelected = card
    }
}

final class TFACreditCardCoordinatorTests: XCTestCase {
    private let navController = NavigationControllerMock(rootViewController: UIViewController())
    private lazy var delegateSpy = TFACreditCardCoordinatorDelegateSpy()
    
    private lazy var sut: TFACreditCardCoordinator = {
        let coordinator = TFACreditCardCoordinator()
        coordinator.viewController = navController.topViewController
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerformAction_WhenActionIsTryAnotherAuthorizationMethod_ShouldCallDelegatedidTryAnotherAuthorizationMethod() {
        sut.perform(action: .tryAnotherAuthorizationMethod)
        
        XCTAssertEqual(delegateSpy.didTryAnotherAuthorizationMethodCallCount, 1)
    }
    
    func testPerformAction_WhenActionIsValidateSelectedCard_ShouldCallDelegateDidTryValidateTfaCard() {
        let card = TFACreditCardData(id: 1, flagName: "master", lastFour: "1234", urlFlagLogoImage: "")
        sut.perform(action: .validateSelectedCard(tfaCard: card))
        
        XCTAssertEqual(delegateSpy.didTryValidateTfaCardCallCount, 1)
        XCTAssertNotNil(delegateSpy.cardSelected)
        XCTAssertEqual(delegateSpy.cardSelected?.flagName, "master")
        XCTAssertEqual(delegateSpy.cardSelected?.lastFour, "1234")
    }
}
