@testable import PicPay
import XCTest

final class NewTFACodeFormCoordinatorTests: XCTestCase {
    private let navController = NavigationControllerMock(rootViewController: UIViewController())
    private lazy var delegateSpy = NewTFACodeFormCoordinatorDelegateSpy()
    
    private lazy var sut: NewTFACodeFormCoordinator = {
        let coordinator = NewTFACodeFormCoordinator()
        coordinator.viewController = navController.topViewController
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerformAction_WhenActionIsCodeValidateWithTypeAndNextStep_ShouldCallDelegateDidValidateCodeWithTypeAndNextStepMethod() {
        let tfaFlow = TFAType.email
        let nextStep = TFAStep.final
        sut.perform(action: .codeValidateAndStartNextStep(tfaType: tfaFlow, nextStep: nextStep))
        
        XCTAssertEqual(delegateSpy.validateCodeWithTypeCallCount, 0)
        XCTAssertEqual(delegateSpy.validateCodeWithTypeAndStepCallCount, 1)
        XCTAssertNotNil(delegateSpy.tfaFlow)
        XCTAssertEqual(delegateSpy.tfaFlow, tfaFlow)
        XCTAssertNotNil(delegateSpy.tfaStep)
        XCTAssertEqual(delegateSpy.tfaStep, nextStep)
    }
}

private final class NewTFACodeFormCoordinatorDelegateSpy: NewTFACodeFormCoordinatorDelegate {
    
    private(set) var validateCodeWithTypeCallCount = 0
    private(set) var validateCodeWithTypeAndStepCallCount = 0
    private(set) var tfaFlow: TFAType?
    private(set) var tfaStep: TFAStep?
    
    func didValidateCode(with tfaType: TFAType) {
        validateCodeWithTypeCallCount += 1
        tfaFlow = tfaType
    }
    
    func didValidateCode(with tfaType: TFAType, nextStep: TFAStep) {
        validateCodeWithTypeAndStepCallCount += 1
        tfaFlow = tfaType
        tfaStep = nextStep
    }
}
