@testable import PicPay
import XCTest

final class NewTFACodeFormPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = NewTFACodeFormCoordinatorSpy()
    private lazy var viewControllerSpy = NewTFACodeFormDisplaySpy()
    
    private lazy var sut: NewTFACodeFormPresenter = {
        let presenter = NewTFACodeFormPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testUpdateCodeText_ShouldDisplayUpdatedCode_WhenCalledFromViewModel() {
        let code = "123"
        sut.updateCodeTextFieldWith(code)
        
        XCTAssertEqual(viewControllerSpy.updateCodeCallCount, 1)
        XCTAssertNotNil(viewControllerSpy.updatedCode)
        XCTAssertEqual(viewControllerSpy.updatedCode, code)
    }
    
    func testPresentCodeValidation_ShouldDisplayCodeValidation_WhenCalledFromViewModel() {
        let code = "1234"
        sut.presentCodeValidationWith(code)
        
        XCTAssertEqual(viewControllerSpy.codeValidationCallCount, 1)
        XCTAssertNotNil(viewControllerSpy.validationCode)
        XCTAssertEqual(viewControllerSpy.validationCode, code)
    }
    
    func testPresentLoading_ShouldDisplayLoading_WhenCalledFromViewModel() {
        let loading = true
        sut.presentLoading(loading)
        
        XCTAssertEqual(viewControllerSpy.displayLoadingCallCount, 1)
    }
    
    func testPresentDescriptionAttributedTextWithSMSFlowAndDestination_ShouldDisplayAttributedTextWithExpectedText_WhenCalledFromViewModel() {
        let destination = "8888-9999"
        let flow = TFAType.sms
        let expectedText = String(format: TFALocalizable.smsNotificationWithMaskPhone.text, destination)
        
        sut.presentDescriptionAttributedText(for: destination, flow: flow)
        
        XCTAssertEqual(viewControllerSpy.displayAttributedDescriptionCallCount, 1)
        XCTAssertNotNil(viewControllerSpy.attributedDescriptionText)
        XCTAssertEqual(viewControllerSpy.attributedDescriptionText, expectedText)
        XCTAssertEqual(viewControllerSpy.flowTitleText, TFALocalizable.tfaCodeSmsTitle.text)
    }
    
    func testPresentDescriptionAttributedTextWithSMSFlowWithoutDestination_ShouldDisplayAttributedTextWithExpectedText_WhenCalledFromViewModel() {
        let flow = TFAType.sms
        let expectedText = TFALocalizable.smsNotificationSentGeneric.text
        
        sut.presentDescriptionAttributedText(for: nil, flow: flow)
        
        XCTAssertEqual(viewControllerSpy.displayAttributedDescriptionCallCount, 1)
        XCTAssertNotNil(viewControllerSpy.attributedDescriptionText)
        XCTAssertEqual(viewControllerSpy.attributedDescriptionText, expectedText)
    }
    
    func testPresentDescriptionAttributedTextWithOldDeviceFlow_ShouldDisplayAttributedTextWithExpectedText_WhenCalledFromViewModel() {
        let flow = TFAType.authorizedDevice
        let expectedText = TFALocalizable.oldDeviceNotificationSent.text
        
        sut.presentDescriptionAttributedText(for: nil, flow: flow)
        
        XCTAssertEqual(viewControllerSpy.displayAttributedDescriptionCallCount, 1)
        XCTAssertNotNil(viewControllerSpy.attributedDescriptionText)
        XCTAssertEqual(viewControllerSpy.attributedDescriptionText, expectedText)
    }
    
    func testPresentDescriptionAttributedTextWithEmailFlowAndDestination_ShouldDisplayAttributedTextWithExpectedText_WhenCalledFromViewModel() {
        let flow = TFAType.email
        let destination = "***erl@gmail.ltd"
        let expectedText = String(format: TFALocalizable.emailNotificationWithMaskEmail.text, destination)
        
        sut.presentDescriptionAttributedText(for: destination, flow: flow)
        
        XCTAssertEqual(viewControllerSpy.displayAttributedDescriptionCallCount, 1)
        XCTAssertNotNil(viewControllerSpy.attributedDescriptionText)
        XCTAssertEqual(viewControllerSpy.attributedDescriptionText, expectedText)
        XCTAssertEqual(viewControllerSpy.flowTitleText, TFALocalizable.tfaCodeTitleTextForEmail.text)
    }
    
    func testPresentDescriptionAttributedTextWithEmailFlowWithoutDestination_ShouldDisplayAttributedTextWithExpectedText_WhenCalledFromViewModel() {
        let flow = TFAType.email
        let expectedText = TFALocalizable.emailNotificationSentGeneric.text
        
        sut.presentDescriptionAttributedText(for: nil, flow: flow)
        
        XCTAssertEqual(viewControllerSpy.displayAttributedDescriptionCallCount, 1)
        XCTAssertNotNil(viewControllerSpy.attributedDescriptionText)
        XCTAssertEqual(viewControllerSpy.attributedDescriptionText, expectedText)
        XCTAssertEqual(viewControllerSpy.flowTitleText, TFALocalizable.tfaCodeTitleTextForEmail.text)
    }
    
    func testPresentDescriptionAttributedTextWithUnexpectedFlow_ShouldDisplayAttributedTextWithExpectedText_WhenCalledFromViewModel() {
        let flow = TFAType.options
        let expectedText = ""
        
        sut.presentDescriptionAttributedText(for: nil, flow: flow)
        
        XCTAssertEqual(viewControllerSpy.displayAttributedDescriptionCallCount, 1)
        XCTAssertNotNil(viewControllerSpy.attributedDescriptionText)
        XCTAssertEqual(viewControllerSpy.attributedDescriptionText, expectedText)
    }
    
    func testPresentRetryButtonStatusEnabledWithNoRemainingTime_ShouldDisplayRetryButtonEnabledWithExpectedText_WhenCalledFromViewModel() {
        let enabled = true
        let remainingTime = 0
        let expectedText = TFALocalizable.resendButtonText.text
        sut.presentRetryButtonStatus(enabled, remainingTime: remainingTime)
        
        XCTAssertEqual(viewControllerSpy.updatedRetryButtonCallCount, 1)
        XCTAssertNotNil(viewControllerSpy.retryButtonText)
        XCTAssertEqual(viewControllerSpy.retryButtonText, expectedText)
    }
    
    func testPresentRetryButtonStatusDisabledWithRemainingTime_ShouldDisplayRetryButtonEnabledWithExpectedText_WhenCalledFromViewModel() {
        let enabled = false
        let remainingTime = 50
        let expectedText = TFALocalizable.didNotReceivedCode.text + "00:50"
        sut.presentRetryButtonStatus(enabled, remainingTime: remainingTime)
        
        XCTAssertEqual(viewControllerSpy.updatedRetryButtonCallCount, 1)
        XCTAssertNotNil(viewControllerSpy.retryButtonText)
        XCTAssertEqual(viewControllerSpy.retryButtonText, expectedText)
    }
    
    func testPresentGenericError_ShouldDisplayAlertWithExpectedTitleAndMessage_WhenCalledFromViewModel() {
        let expectedTitle = TFALocalizable.genericErrorTitle.text
        let expectedMessage = TFALocalizable.genericErrorMessage.text
        sut.presentGenericError()
        
        XCTAssertNotNil(viewControllerSpy.displayedAlert)
        XCTAssertEqual(viewControllerSpy.displayedAlert?.title?.string, expectedTitle)
        XCTAssertEqual(viewControllerSpy.displayedAlert?.text?.string, expectedMessage)
    }
    
    func testPresentRequestError_ShouldDisplayAlertWithExpectedTitle_WhenCalledFromViewModel() {
        let expectedTitle = "Ops!"
        let expectedMessage = "Dados inválidos"
        sut.presentRequestError(tfaError: TFAModelsHelper().invalidDataErrorJson())
        
        XCTAssertNotNil(viewControllerSpy.displayedAlert)
        XCTAssertEqual(viewControllerSpy.displayedAlert?.title?.string, expectedTitle)
        XCTAssertEqual(viewControllerSpy.displayedAlert?.text?.string, expectedMessage)
    }
    
    func testPresentAttemptLimitError_ShouldDisplayAttemptLimitAlertWithExpectedTitleAndMessage_WhenCalledFromViewModel() {
        let expectedTitle = "Limite atingido"
        let expectedMessage = "Você excedeu o limite de tentativas. Aguarde 02:52 minutos para tentar novamente."
        let retryTime = 60
        let flow = TFAType.email
        
        sut.presentAttempLimitError(tfaError: TFAModelsHelper().attempLimitErrorJson(), retryTime: retryTime, tfaType: flow)
        XCTAssertNotNil(viewControllerSpy.displayedAlert)
        XCTAssertEqual(viewControllerSpy.displayedAlert?.title?.string, expectedTitle)
        XCTAssertEqual(viewControllerSpy.displayedAlert?.text?.string, expectedMessage)
        XCTAssertEqual(viewControllerSpy.retryTimeAfterExceededAttempts, retryTime)
    }
    
    func testValidateCodeWithTypeAndNextStep_ShouldPerformActionOnCoordinator_WhenCalledFromViewModel() {
        let flow = TFAType.email
        let step = TFAStep.final
        sut.validateCode(with: flow, nextStep: step)
        
        XCTAssertNotNil(coordinatorSpy.actionCalled)
        XCTAssertNotNil(coordinatorSpy.validationFlow)
        XCTAssertNotNil(coordinatorSpy.validationStep)
        XCTAssertEqual(coordinatorSpy.validationFlow, flow)
        XCTAssertEqual(coordinatorSpy.validationStep, step)
    }
}

private final class NewTFACodeFormCoordinatorSpy: NewTFACodeFormCoordinating {
    var viewController: UIViewController?
    var delegate: NewTFACodeFormCoordinatorDelegate?
    
    private(set) var actionCalled: NewTFACodeFormAction?
    private(set) var validationFlow: TFAType?
    private(set) var validationStep: TFAStep?
    
    func perform(action: NewTFACodeFormAction) {
        actionCalled = action
        if case let .codeValidateAndStartNextStep(tfaType, nextStep) = action {
            validationFlow = tfaType
            validationStep = nextStep
        }
    }
}

private final class NewTFACodeFormDisplaySpy: NewTFACodeFormDisplay {
    private(set) var updateCodeCallCount = 0
    private(set) var updatedCode: String?
    private(set) var codeValidationCallCount = 0
    private(set) var validationCode: String?
    private(set) var displayLoadingCallCount = 0
    private(set) var displayAttributedDescriptionCallCount = 0
    private(set) var attributedDescriptionText: String?
    private(set) var flowTitleText: String?
    private(set) var updatedRetryButtonCallCount = 0
    private(set) var retryButtonText: String?
    private(set) var displayedAlert: Alert?
    private(set) var retryTimeAfterExceededAttempts = 0
    
    func displayUpdatedCode(_ code: String) {
        updateCodeCallCount += 1
        updatedCode = code
    }
    
    func displayCodeValidationWith(_ code: String) {
        codeValidationCallCount += 1
        validationCode = code
    }
    
    func displayLoading(_ loading: Bool) {
        displayLoadingCallCount += 1
    }
    
    func displayAttributedDescription(_ attributedText: NSAttributedString, titleText: String) {
        displayAttributedDescriptionCallCount += 1
        attributedDescriptionText = attributedText.string
        flowTitleText = titleText
    }
    
    func displayUpdatedRetryButton(enabled: Bool, text: NSAttributedString) {
        updatedRetryButtonCallCount += 1
        retryButtonText = text.string
    }
    
    func displayErrorAlert(alert: Alert) {
        displayedAlert = alert
    }
    
    func displayAttempLimitError(alert: Alert, retryTime: Int) {
        displayedAlert = alert
        retryTimeAfterExceededAttempts = retryTime
    }
}
