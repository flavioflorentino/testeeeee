import Core
@testable import PicPay

final class NewTFACodeFormPresenterSpy: NewTFACodeFormPresenting {
    var viewController: NewTFACodeFormDisplay?
    
    private(set) var presentDescriptionCalled = false
    private(set) var presentDescriptionDestination: String?
    private(set) var retryButtonEnabled: Bool?
    private(set) var retryRemainingTime = 0
    private(set) var updatedCode: String?
    private(set) var presentUpdatedCodeCalled = false
    private(set) var presentGenericErrorCalled = false
    private(set) var presentAttemptLimitErrorCalled = false
    private(set) var limitErrorRetryTime = 0
    private(set) var presentRequestErrorCalled = false
    private(set) var validationErrorMessage: String?
    private(set) var presentCodeValidationCalled = false
    private(set) var validationCodeUsed: String?
    private(set) var timerFinished = false
    private(set) var validateCodeFinished = false
    private(set) var validateCodeFinishedWithNextStep = false
    private(set) var nextStepCalled: TFAStep?
    private(set) var presentLoadingCalled = false
    
    func updateCodeTextFieldWith(_ code: String) {
        presentUpdatedCodeCalled = true
        updatedCode = code
    }
    
    func presentCodeValidationWith(_ code: String) {
        presentCodeValidationCalled = true
        validationCodeUsed = code
    }
    
    func presentLoading(_ loading: Bool) {
        presentLoadingCalled = true
    }
    
    func presentDescriptionAttributedText(for destination: String?, flow: TFAType) {
        presentDescriptionCalled = true
        presentDescriptionDestination = destination
    }
    
    func presentRetryButtonStatus(_ enabled: Bool, remainingTime: Int) {
        retryButtonEnabled = enabled
        retryRemainingTime = remainingTime
        timerFinished = remainingTime <= 0
    }
    
    func presentGenericError() {
        presentGenericErrorCalled = true
    }
    
    func presentRequestError(tfaError: RequestError) {
        presentRequestErrorCalled = true
        validationErrorMessage = tfaError.message
    }
    
    func presentAttempLimitError(tfaError: RequestError, retryTime: Int, tfaType: TFAType) {
        presentAttemptLimitErrorCalled = true
        limitErrorRetryTime = retryTime
    }
    
    func validateCode(with type: TFAType) {
        validateCodeFinished = true
    }
    
    func validateCode(with type: TFAType, nextStep: TFAStep) {
        validateCodeFinishedWithNextStep = true
        nextStepCalled = nextStep
    }
}
