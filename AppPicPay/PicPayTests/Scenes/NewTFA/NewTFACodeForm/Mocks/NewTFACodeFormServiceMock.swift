import Core
@testable import PicPay

final class NewTFACodeFormServiceMock: NewTFACodeFormServicing {
    var getCodeDataSuccess = false
    var validateCodeDataForNextStepSuccess = false
    var isServerError = false
    var isAttemptLimitError = false
    var isValidationError = false
    
    private(set) var getCodeDataCalled = false
    private(set) var validateCodeCalled = false
    
    func getNewCodeDataInfo(tfaRequestData: TFARequestData, completion: @escaping (Result<NewTFACode, ApiError>) -> Void) {
        getCodeDataCalled = true
        if getCodeDataSuccess {
            completion(.success(NewTFACode(retryTime: 60, sentTo: "sentTo", channel: "SMS")))
        } else {
            completion(.failure(ApiError.serverError))
        }
    }
    
    func validateCodeForNextStep(code: String, tfaRequestData: TFARequestData, completion: @escaping (Result<TFAValidationResponse, ApiError>) -> Void) {
        validateCodeCalled = true
        if validateCodeDataForNextStepSuccess {
            completion(.success(TFAValidationResponse(nextStep: .final)))
        } else {
            completion(.failure(ApiError.serverError))
        }
    }
}
