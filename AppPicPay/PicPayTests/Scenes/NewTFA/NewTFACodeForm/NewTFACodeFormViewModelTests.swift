import Core
@testable import PicPay
import XCTest

final class NewTFACodeFormViewModelTests: XCTestCase {
    private lazy var mock = NewTFACodeFormServiceMock()
    private lazy var spy = NewTFACodeFormPresenterSpy()
    
    private lazy var sut: NewTFACodeFormViewModel = {
        let viewModel = NewTFACodeFormViewModel(
            service: mock,
            presenter: spy,
            tfaRequestData: TFARequestData(flow: .sms, step: .code, id: "123")
        )
        return viewModel
    }()
    
    func testReloadCode_WhenServiceHasSucceeded_ShouldPresentDescriptionWithDestination() {
        mock.getCodeDataSuccess = true
        sut.reloadCode()
        
        XCTAssertTrue(mock.getCodeDataCalled)
        XCTAssertTrue(spy.presentDescriptionCalled)
        XCTAssertNotNil(spy.presentDescriptionDestination)
        XCTAssertEqual(spy.presentDescriptionDestination, "sentTo")
    }
    
    func testReloadCode_WhenServiceHasFailed_ShouldPresentDescriptionWithoutDestinationAndRetryButtonEnabled() {
        mock.getCodeDataSuccess = false
        sut.reloadCode()
        
        XCTAssertTrue(mock.getCodeDataCalled)
        XCTAssertTrue(spy.presentDescriptionCalled)
        XCTAssertNil(spy.presentDescriptionDestination)
        XCTAssertTrue(spy.retryButtonEnabled ?? false)
        XCTAssertTrue(spy.retryRemainingTime == 0)
    }
    
    func testUpdateCodeText_ShouldPresentUpdatedCode_WhenTextLenghtIsValid() {
        sut.updateCodeTextWith("123")
        
        XCTAssertTrue(spy.presentUpdatedCodeCalled)
        XCTAssertEqual(spy.updatedCode, "123")
    }
    
    func testUpdateCodeText_ShouldPresentUpdatedCodeAndCallValidation_WhenTextReachTheExpectedLenght() {
        sut.updateCodeTextWith("1234")
        
        XCTAssertTrue(spy.presentUpdatedCodeCalled)
        XCTAssertEqual(spy.updatedCode, "1234")
        XCTAssertTrue(spy.presentCodeValidationCalled)
        XCTAssertNotNil(spy.validationCodeUsed)
        XCTAssertEqual(spy.validationCodeUsed, "1234")
    }
    
    func testValidateCode_WhenServiceHasSucceeded_ShouldPresentSuccess() {
        mock.validateCodeDataForNextStepSuccess = true
        sut.validateCode("1234")
    
        XCTAssertTrue(mock.validateCodeCalled)
        XCTAssertTrue(spy.validateCodeFinishedWithNextStep)
    }
    
    func testValidateCode_WhenServiceHasServerError_ShouldPresentGenericError() {
        mock.validateCodeDataForNextStepSuccess = false
        mock.isServerError = true
        sut.validateCode("1234")
        
        XCTAssertFalse(spy.validateCodeFinishedWithNextStep)
        XCTAssertTrue(mock.validateCodeCalled)
        XCTAssertTrue(spy.presentGenericErrorCalled)
    }
    
    func testStartTimerForNewRequest_ShouldPresentRetryButtonDisabled_WhenRemainingTimeHasNotFinished() {
        sut.startTimerForNewRequest(5)
        
        expectation(waitForCondition: self.spy.retryRemainingTime <= 3) {
            XCTAssertFalse(self.spy.timerFinished)
        }
        
        expectation(waitForCondition: self.spy.retryButtonEnabled == false)
        
        waitForExpectations(timeout: 5.0)
    }
    
    func testStartTimerForNewRequestWithTimerNil_ShouldPresentRetryButtonEnabled_WhenRemainingTimeHasFinished() {
        let viewModel = NewTFACodeFormViewModel(
            service: mock,
            presenter: spy,
            tfaRequestData: TFARequestData(flow: .sms, step: .code, id: "123"),
            timer: nil
        )
        viewModel.startTimerForNewRequest(2)
        
        expectation(waitForCondition: self.spy.retryButtonEnabled == true) {
            XCTAssertTrue(self.spy.timerFinished)
        }
        
        waitForExpectations(timeout: 5.0)
    }
}
