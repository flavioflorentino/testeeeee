@testable import PicPay
import XCTest

final class TFASuccessCoordinatorTests: XCTestCase {
    private let navController = NavigationControllerMock(rootViewController: UIViewController())
    private lazy var delegateSpy = TFASuccessCoordinatorDelegateSpy()
    
    private lazy var sut: TFASuccessCoordinator = {
        let coordinator = TFASuccessCoordinator()
        coordinator.viewController = navController.topViewController
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerformEnter_ShouldCallDidEnterOnDelegate() {
        sut.perform(action: .enter)
        
        XCTAssertEqual(delegateSpy.didEnterCallCount, 1)
    }
}

private final class  TFASuccessCoordinatorDelegateSpy: TFASuccessCoordinatorDelegate {
    private(set) var didEnterCallCount = 0
    
    func didEnter() {
        didEnterCallCount += 1
    }
}
