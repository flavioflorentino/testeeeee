@testable import PicPay
import XCTest

final class TFASuccessViewModelTests: XCTestCase {
    private lazy var spy = TFASuccessPresenterSpy()
    private lazy var sut = TFASuccessViewModel(presenter: spy)
    
    func testEnter_ShouldPresentNextStep() {
        sut.enter()
        
        XCTAssertEqual(spy.nextStepCalledCount, 1)
        XCTAssertEqual(spy.tfaAction, .enter)
    }
}

private final class TFASuccessPresenterSpy: TFASuccessPresenting {
    private(set) var nextStepCalledCount = 0
    private(set) var tfaAction: TFASuccessAction?
    
    func didNextStep(action: TFASuccessAction) {
        nextStepCalledCount += 1
        tfaAction = action
    }
}

