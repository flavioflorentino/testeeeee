@testable import PicPay
import XCTest

final class TFASuccessPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = TFASuccessCoordinatorSpy()
    private lazy var sut = TFASuccessPresenter(coordinator: coordinatorSpy)
    
    func testDidNextStep_WhenActionIsEnter_ShouldPerformEnterOnCoordinator() {
        sut.didNextStep(action: .enter)
        
        XCTAssertTrue(coordinatorSpy.enterPerformed)
    }
}

private final class TFASuccessCoordinatorSpy: TFASuccessCoordinating {
    var viewController: UIViewController?
    var delegate: TFASuccessCoordinatorDelegate?
    
    private(set) var enterPerformed = false
    
    func perform(action: TFASuccessAction) {
        enterPerformed = action == .enter
    }
}
