@testable import PicPay
import XCTest

private final class TFAValidationStatusCoordinatorSpy: TFAValidationStatusCoordinating {
    var viewController: UIViewController?
    var delegate: TFAValidationStatusCoordinatorDelegate?
    private(set) var performActionCallCount = 0
    private(set) var performedAction: TFAValidationStatusAction?
    
    func perform(action: TFAValidationStatusAction) {
        performActionCallCount += 1
        performedAction = action
    }
}

private final class TFAValidationStatusDisplaySpy: TFAValidationStatusDisplay {
    private(set) var displayStatusCallCount = 0
    private(set) var displayedStatus: TFAStatus?
    private(set) var isShowingSecondaryButton = false
    
    func displayStatus(_ status: TFAStatus, showSecondaryButton: Bool) {
        displayedStatus = status
        displayStatusCallCount += 1
        isShowingSecondaryButton = showSecondaryButton
    }
}

final class TFAValidationStatusPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = TFAValidationStatusCoordinatorSpy()
    private lazy var displaySpy = TFAValidationStatusDisplaySpy()
    
    private lazy var sut: TFAValidationStatusPresenter = {
        let presenter = TFAValidationStatusPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        return presenter
    }()
    
    func testPresentStatus_WhenCalledFromViewModel_ShouldDisplayExpectedStatusOnViewController() {
        sut.presentStatus(.approved, showSecondaryButton: false)
        
        XCTAssertEqual(displaySpy.displayStatusCallCount, 1)
        XCTAssertEqual(displaySpy.displayedStatus, .approved)
        XCTAssertFalse(displaySpy.isShowingSecondaryButton)
    }
    
    func testHandleActionForStatus_WhenStatusIsApproved_ShouldPerformExpectedActionOnCoordinator() {
        sut.handleActionForStatus(.approved)
        let expectedAction: TFAValidationStatusAction = .enter
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, expectedAction)
    }
    
    func testHandleActionForStatus_WhenStatusIsDenied_ShouldPerformExpectedActionOnCoordinator() {
        sut.handleActionForStatus(.denied)
        let expectedAction: TFAValidationStatusAction = .understood
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, expectedAction)
    }
    
    func testHandleActionForStatus_WhenStatusIsWaitingAnalysis_ShouldPerformExpectedActionOnCoordinator() {
        sut.handleActionForStatus(.waitingAnalysis)
        let expectedAction: TFAValidationStatusAction = .understood
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, expectedAction)
    }
    
    func testHandleActionForStatus_WhenStatusIsInconclusive_ShouldPerformExpectedActionOnCoordinator() {
        sut.handleActionForStatus(.inconclusive)
        let expectedAction: TFAValidationStatusAction = .showOptions
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, expectedAction)
    }
    
    func testDoItLater_WhenCalledFromViewModel_ShouldPerformExpectedActionOnCoordinator() {
        sut.doItLater()
        let expectedAction: TFAValidationStatusAction = .understood
        
        XCTAssertEqual(coordinatorSpy.performActionCallCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, expectedAction)
    }
}
