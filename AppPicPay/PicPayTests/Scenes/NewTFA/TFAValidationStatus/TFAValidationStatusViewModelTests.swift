import AnalyticsModule
@testable import PicPay
import XCTest

private final class TFAValidationStatusPresenterSpy: TFAValidationStatusPresenting {
    var viewController: TFAValidationStatusDisplay?
    private(set) var presentStatusCallCount = 0
    private(set) var presentedStatus: TFAStatus?
    private(set) var handleActionForStatusCallCount = 0
    private(set) var isShowingSecondaryButton = false
    private(set) var handledActionStatus: TFAStatus?
    private(set) var doItLaterCallCount = 0

    func presentStatus(_ status: TFAStatus, showSecondaryButton: Bool) {
        presentStatusCallCount += 1
        presentedStatus = status
        isShowingSecondaryButton = showSecondaryButton
    }
    
    func doItLater() {
        doItLaterCallCount += 1
    }
    
    func handleActionForStatus(_ status: TFAStatus) {
        handleActionForStatusCallCount += 1
        handledActionStatus = status
    }
}

private extension TFAValidationStatusViewModelTests {
    func validationStatusViewModel(withStatus status: TFAStatus) -> TFAValidationStatusViewModel {
        return TFAValidationStatusViewModel(presenter: presenterSpy, status: status, dependencies: mockDependencies)
    }
}

final class TFAValidationStatusViewModelTests: XCTestCase {
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var mockDependencies = DependencyContainerMock(analyticsSpy)
    private lazy var presenterSpy = TFAValidationStatusPresenterSpy()
    
    func testConfigureViews_WhenCalledFromViewController_ShouldPresentExpectedStatus() {
        let sut = validationStatusViewModel(withStatus: .approved)
        
        sut.configureViews()
        XCTAssertEqual(presenterSpy.presentStatusCallCount, 1)
        XCTAssertEqual(presenterSpy.presentedStatus, .approved)
        XCTAssertEqual(presenterSpy.presentedStatus?.actionTitle, TFAStatus.approved.actionTitle)
        XCTAssertFalse(presenterSpy.isShowingSecondaryButton)
        
        let expectedEvent = TFAEvents.didReceivedIdentityStatus(.approved).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testConfigureViews_WhenStatusIsDenied_ShouldLogDeniedEvent() {
        let sut = validationStatusViewModel(withStatus: .denied)
        
        sut.configureViews()
        let expectedEvent = TFAEvents.didReceivedIdentityStatus(.denied).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testConfigureViews_WhenStatusIsInconclusive_ShouldLogInconclusiveEvent() {
        let sut = validationStatusViewModel(withStatus: .inconclusive)
        
        sut.configureViews()
        XCTAssertEqual(presenterSpy.presentStatusCallCount, 1)
        XCTAssertEqual(presenterSpy.presentedStatus, .inconclusive)
        XCTAssertEqual(presenterSpy.presentedStatus?.actionTitle, TFAStatus.inconclusive.actionTitle)
        XCTAssertTrue(presenterSpy.isShowingSecondaryButton)
        
        let expectedEvent = TFAEvents.didReceivedIdentityStatus(.inconclusive).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testConfigureViews_WhenStatusIsWaitingAnalysis_ShouldLogWaitingAnalysisEvent() {
        let sut = validationStatusViewModel(withStatus: .waitingAnalysis)
        
        sut.configureViews()
        let expectedEvent = TFAEvents.didReceivedIdentityStatus(.waitingAnalysis).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testHandelButtonAction_WhenCalledFromViewController_ShouldHandleActionOnPresenter() {
        let sut = validationStatusViewModel(withStatus: .approved)
        
        sut.handleButtonAction()
        XCTAssertEqual(presenterSpy.handleActionForStatusCallCount, 1)
        XCTAssertEqual(presenterSpy.handledActionStatus, .approved)
        XCTAssertEqual(presenterSpy.handledActionStatus?.actionTitle, TFAStatus.approved.actionTitle)
    }
    
    func testDoItLater_WhenCalledFromViewController_ShouldCallDoItLaterOnPresenter() {
        let sut = validationStatusViewModel(withStatus: .inconclusive)
        
        sut.doItLaterAction()
        
        XCTAssertEqual(presenterSpy.doItLaterCallCount, 1)
    }
}
