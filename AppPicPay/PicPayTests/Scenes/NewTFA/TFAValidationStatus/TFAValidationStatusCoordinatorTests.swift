@testable import PicPay
import XCTest

private final class TFAValidationStatusCoordinatorDelegateSpy: TFAValidationStatusCoordinatorDelegate {
    private(set) var receivedApprovedStatusCallCount = 0
    private(set) var closeTFAFlowCallCount = 0
    private(set) var presentTFAOptionsCallCount = 0
    
    func didReceivedApprovedStatus() {
        receivedApprovedStatusCallCount += 1
    }
    
    func closeTFAFlow() {
        closeTFAFlowCallCount += 1
    }
    
    func presentTFAOptions() {
        presentTFAOptionsCallCount += 1
    }
}

final class TFAValidationStatusCoordinatorTests: XCTestCase {
    private let navController = NavigationControllerMock(rootViewController: UIViewController())
    private lazy var delegateSpy = TFAValidationStatusCoordinatorDelegateSpy()
    
    private lazy var sut: TFAValidationStatusCoordinator = {
        let coordinator = TFAValidationStatusCoordinator()
        coordinator.viewController = navController.topViewController
        coordinator.delegate = delegateSpy
        return coordinator
    }()
    
    func testPerform_WhenActionIsEnter_ShouldCallDelegateDidReceivedApprovedStatus() {
        sut.perform(action: .enter)
        
        XCTAssertEqual(delegateSpy.receivedApprovedStatusCallCount, 1)
    }
    
    func testPerform_WhenActionIsShowOptions_ShouldCallDelegatePresentTFAOptions() {
        sut.perform(action: .showOptions)
        
        XCTAssertEqual(delegateSpy.presentTFAOptionsCallCount, 1)
    }
    
    func testPerform_WhenActionIsUnderstood_ShouldCallDelegateCloseTFAFlow() {
        sut.perform(action: .understood)
        
        XCTAssertEqual(delegateSpy.closeTFAFlowCallCount, 1)
    }
}
