import Core
@testable import PicPay
import Foundation

struct TFAModelsHelper {
    func tfaPersonalData(with type: TFAType, tfaId: String = "123", code: String? = nil) -> TFAPersonalDataCheckInfo {
        let tfaData = TFAPersonalDataCheckInfo(
            cpfDocument: "123.456.789-01",
            birthDate: "10/10/2000",
            mothersName: "mae",
            email: "email@mail.com",
            tfaId: tfaId,
            channel: type,
            code: code
        )
        return tfaData
    }
    
    func attempLimitErrorJson() -> RequestError {
        let data: [String: Any] = [
            "short_message" : "Limite atingido",
            "data" : [
                "retry_time" : 10
            ],
            "code" : "form_limit",
            "message" : "Você excedeu o limite de tentativas. Aguarde 02:52 minutos para tentar novamente."
        ]
        let jsonData = try! JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
        return RequestError.initialize(with: jsonData)
    }
    
    func tfaBlockedErrorJson() -> RequestError {
        let data: [String: Any] = [
            "short_message" : "Bloqueio por fraude",
            "data" : [
                "retry_time" : "3 dias"
            ],
            "code" : "tfa_blocked_code",
            "message" : "Aguarde 3 dias para tentar novamente."
        ]
        let jsonData = try! JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
        return RequestError.initialize(with: jsonData)
    }
    
    func invalidDataErrorJson() -> RequestError {
        let data: [String: Any] = [
            "short_message" : "Ops!",
            "message" : "Dados inválidos",
            "code" : "4401"
        ]
        
        let jsonData = try! JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
        return RequestError.initialize(with: jsonData)
    }
    
    func unauthorizedInvalidDataErrorJson() -> Data {
        let data: [String: Any] = [
            "short" : "Ops!",
            "message_desc" : "Dados inválidos",
            "code_error" : "4401"
        ]
        
        return try! JSONSerialization.data(withJSONObject: data, options: .fragmentsAllowed)
    }
    
    func completeConsumerInfoJson() -> Data {
        let data: [String: Any] = [
            "phone_number": "11*****1762",
            "bank": [
                "bank_name": "Santander",
                "image_url": "https://s3-sa-east-1.amazonaws.com/picpay/banks/ico_cadastro_santander.png"
            ]
        ]
        
        return try! JSONSerialization.data(withJSONObject: data, options: .fragmentsAllowed)
    }
    
    func onlyPhoneConsumerInfoJson() -> Data {
        let data: [String: Any] = [
            "phone_number": "11*****1762"
        ]
        
        return try! JSONSerialization.data(withJSONObject: data, options: .fragmentsAllowed)
    }
}
