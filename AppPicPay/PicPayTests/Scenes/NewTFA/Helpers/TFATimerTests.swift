@testable import PicPay
import XCTest

private class TFATimerDelegateSpy: TFATimerDelegate {
    private(set) var callTimerTicCount = 0

    func timerTic() {
        callTimerTicCount += 1
    }
}

final class TFATimerTests: XCTestCase {
    private let delegateSpy = TFATimerDelegateSpy()

    private lazy var sut: TFATimer = {
        let timer = TFATimer()
        timer.delegate = delegateSpy
        return timer
    }()

    func testStart_ShouldFireTimer() throws {
        sut.start()

        let timer = try XCTUnwrap(sut.timer)
        XCTAssertTrue(timer.isValid)
        XCTAssertEqual(delegateSpy.callTimerTicCount, 1)
    }

    func testStop_ShouldInvalidateTimer() throws {
        sut.start()
        sut.stop()

        let timer = try XCTUnwrap(sut.timer)
        XCTAssertFalse(timer.isValid)
    }
}
