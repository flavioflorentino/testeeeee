@testable import PicPay

public class CustomPopNavigationControllerMock: CustomPopNavigationController {
    public var isPushViewControllerCalled: Bool = false
    public var isPopViewControllerCalled: Bool = false
    public var isPopToRootViewControllerCalled: Bool = false
    public var isPresentViewControllerCalled: Bool = false
    public var isDismissViewControllerCalled: Bool = false
    public var isSetViewControllersCalled: Bool = false
    
    public var pushedViewController: UIViewController?
    public var popedViewController: UIViewController?
    public var popedToViewController: UIViewController?
    public var popedToRootViewController: UIViewController?
    public var popedViewControllers: [UIViewController]?
    public var viewControllerPresented: UIViewController?
    public var topViewControllerSetted: UIViewController?
    
    public override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        pushedViewController = viewController
        isPushViewControllerCalled = true
        super.pushViewController(viewController, animated: false)
    }
    
    public override func popViewController(animated: Bool) -> UIViewController? {
        let viewController = super.popViewController(animated: false)
        popedViewController = viewController
        isPopViewControllerCalled = true
        return viewController
    }
    
    public override func popToViewController(_ viewController: UIViewController, animated: Bool) -> [UIViewController]? {
        let viewControllersList = super.popToViewController(viewController, animated: false)
        popedToViewController = viewController
        isPopViewControllerCalled = true
        return viewControllersList
    }
    
    public override func popToRootViewController(animated: Bool) -> [UIViewController]? {
        let viewControllersList = super.popToRootViewController(animated: false)
        isPopToRootViewControllerCalled = true
        popedToRootViewController = viewControllers.first
        popedViewControllers = viewControllersList
        return viewControllersList
    }
    
    public override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        viewControllerPresented = viewControllerToPresent
        isPresentViewControllerCalled = true
        super.present(viewControllerToPresent, animated: false)
        completion?()
    }
    
    public override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        isDismissViewControllerCalled = true
        super.dismiss(animated: false)
        completion?()
    }
    
    public override func setViewControllers(_ viewControllers: [UIViewController], animated: Bool) {
        isSetViewControllersCalled = true
        topViewControllerSetted = viewControllers.last
        super.setViewControllers(viewControllers, animated: false)
    }
}
