@testable import PicPay
import XCTest

final class NewTFAOnboardingViewControllerTests: XCTestCase {
    var sut: NewTFAOnboardingViewController!
    
    private var presenterMock: NewTFAOnboardingPresenterMock!
    private var coordinatorMock: NewTFAOnboardingCoordinatorMock!
    
    override func setUp() {
        super.setUp()
        setupMocks()
    }
    
    func setupMocks() {
        coordinatorMock = NewTFAOnboardingCoordinatorMock()
        presenterMock = NewTFAOnboardingPresenterMock(coordinator: coordinatorMock)
        
        let viewModel = NewTFAOnboardingViewModel(presenter: presenterMock)
        sut = NewTFAOnboardingViewController(viewModel: viewModel)
    }
    
    func testCloseActionCalledWhenTapCloseButton() {
        guard sut.view != nil else {
            XCTFail()
            return
        }
     
        sut.closeButtonTapped(UIButton())
        XCTAssertTrue(presenterMock.didCallClose)
    }
    
    func testNextActionCalledWhenTapContinueButton() {
        guard sut.view != nil else {
            XCTFail()
            return
        }
     
        sut.continueButtonTapped(UIButton())
        XCTAssertTrue(presenterMock.didCallNextStep)
    }
    
    func testShowHelActionCalledWhenTapInformationButton() {
        guard sut.view != nil else {
            XCTFail()
            return
        }
     
        sut.informationButtonTapped(UIButton())
        XCTAssertTrue(presenterMock.didCallShowHelp)
    }
}

fileprivate final class NewTFAOnboardingPresenterMock: NewTFAOnboardingPresenting {
    private let coordinator: NewTFAOnboardingCoordinating
    
    private(set) var didCallClose = false
    private(set) var didCallShowHelp = false
    private(set) var didCallNextStep = false
    
    init(coordinator: NewTFAOnboardingCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: NewTFAOnboardingAction) {
        switch action {
        case .close:
            didCallClose = true
        case .continueValidation:
            didCallNextStep = true
        case .showHelp:
            didCallShowHelp = true
        }
    }
}

fileprivate final class NewTFAOnboardingCoordinatorMock: NewTFAOnboardingCoordinating {
    var viewController: UIViewController?
    var delegate: NewTFAOnboardingCoordinatorDelegate?
    
    func perform(action: NewTFAOnboardingAction) {
        switch action {
        case .close:
            delegate?.onboardingClose()
        case .continueValidation:
            delegate?.onboardingNextStep()
        case .showHelp:
            delegate?.onboardingShowHelp()
        }
    }
}
