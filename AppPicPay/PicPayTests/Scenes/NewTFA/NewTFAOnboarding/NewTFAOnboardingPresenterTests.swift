@testable import PicPay
import XCTest

final class NewTFAOnboardingPresenterTests: XCTestCase {
    private lazy var mock: NewTFAOnboardingCoordinatorMock = {
        let coordinator = NewTFAOnboardingCoordinatorMock()
        return coordinator
    }()
    
    private lazy var sut: NewTFAOnboardingPresenter = {
        let presenter = NewTFAOnboardingPresenter(coordinator: mock)
        return presenter
    }()
    
    func testNextStepCallExpectedAction() {
        sut.didNextStep(action: .continueValidation)
        
        XCTAssertNotNil(mock.calledAction)
        XCTAssertEqual(mock.calledAction, .continueValidation)
    }
}

private final class NewTFAOnboardingCoordinatorMock: NewTFAOnboardingCoordinating {
    var viewController: UIViewController?
    var delegate: NewTFAOnboardingCoordinatorDelegate?
    
    private(set) var calledAction: NewTFAOnboardingAction?
    
    func perform(action: NewTFAOnboardingAction) {
        calledAction = action
    }
}
