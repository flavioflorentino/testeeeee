@testable import PicPay
import XCTest

final class NewTFAOnboardingViewModelTests: XCTestCase {
    private lazy var spy: NewTFAOnboardingPresenterSpy = {
        let presenter = NewTFAOnboardingPresenterSpy()
        return presenter
    }()
    
    private lazy var sut: NewTFAOnboardingViewModel = {
        let viewModel = NewTFAOnboardingViewModel(presenter: spy)
        return viewModel
    }()
    
    func testShowHelpActionPresentsExpectedAction() {
        sut.showHelp()
        
        XCTAssertTrue(spy.showHelpCalled)
    }
    
    func testCloseActionPresentsExpectedAction() {
        sut.closeOnboarding()
        
        XCTAssertTrue(spy.closeCalled)
    }
    
    func testcontinueValidationActionPresentsExpectedAction() {
        sut.continueValidation()
        
        XCTAssertTrue(spy.continueValidationCalled)
    }
}

private final class NewTFAOnboardingPresenterSpy: NewTFAOnboardingPresenting {
    private(set) var showHelpCalled = false
    private(set) var closeCalled = false
    private(set) var continueValidationCalled = false
    
    func didNextStep(action: NewTFAOnboardingAction) {
        switch action {
        case .showHelp:
            showHelpCalled = true
        case .close:
            closeCalled = true
        case .continueValidation:
            continueValidationCalled = true
        }
    }
}
