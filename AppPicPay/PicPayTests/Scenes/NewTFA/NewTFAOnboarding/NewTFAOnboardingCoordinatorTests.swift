@testable import PicPay
import XCTest

final class NewTFAOnboardingCoordinatorTests: XCTestCase {
    private lazy var spy: NewTFAOnboardingCoordinatorDelegateSpy = {
        let coordinatorDelegate = NewTFAOnboardingCoordinatorDelegateSpy()
        return coordinatorDelegate
    }()
    
    private lazy var sut: NewTFAOnboardingCoordinator = {
        let coordinator = NewTFAOnboardingCoordinator()
        coordinator.delegate = spy
        return coordinator
    }()
    
    func testCloseActionCallsDelegateToCloseTFAFlow() {
        sut.perform(action: .close)
        
        XCTAssertTrue(spy.didCallClose)
    }
    
    func testContinueValidationActionCallsDelegateToContinueFlow() {
        sut.perform(action: .continueValidation)
        
        XCTAssertTrue(spy.didCallNextStep)
    }
    
    func testShowHelpActionCallsDelegateToPresentHelpInfo() {
        sut.perform(action: .showHelp)
        
        XCTAssertTrue(spy.didCallShowHelp)
    }
}

private final class NewTFAOnboardingCoordinatorDelegateSpy: NewTFAOnboardingCoordinatorDelegate {
    private(set) var didCallClose = false
    private(set) var didCallShowHelp = false
    private(set) var didCallNextStep = false

    func onboardingClose() {
        didCallClose = true
    }

    func onboardingShowHelp() {
        didCallShowHelp = true
    }

    func onboardingNextStep() {
        didCallNextStep = true
    }
}
