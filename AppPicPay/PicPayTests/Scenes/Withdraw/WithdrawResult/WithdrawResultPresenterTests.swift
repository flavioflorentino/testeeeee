@testable import PicPay
import XCTest

class WithdrawResultPresenterTests: XCTestCase {
    var sut: WithdrawResultPresenter!
    var spy: WithdrawResultDisplaySpy!
    
    override func setUp() {
        super.setUp()
        spy = WithdrawResultDisplaySpy()
        sut = WithdrawResultPresenter()
        sut.viewController = spy
    }

    override func tearDown() {
        spy = nil
        sut = nil
        super.tearDown()
    }

    func testDisplayValuesShowsExpectedValues() {
        let withdraw = WithdrawHelpers().mockedWithdraw(with: 150.0, text: "Withdraw from account")
        sut.displayValues(actionButtonTitle: "Title", buttonAction: .newWithdraw, withdrawData: withdraw)
        
        XCTAssertTrue(spy.didDisplayResultViewData)
        XCTAssertEqual(spy.actionButtonTitleText, "Title")
        XCTAssertEqual(spy.actionButtonAction, ButtonAction.newWithdraw)
    }
    
    func testDisplayChangeAccountShowsExpectedValues() {
        let withdraw = WithdrawHelpers().mockedWithdraw(with: 150.0, text: "Withdraw from account")
        sut.displayChangeAccount(with: withdraw, isSideBarMenu: false)
        
        XCTAssertTrue(spy.didDisplayChangeAccount)
        XCTAssertEqual(spy.changeAccountWithdrawValue, "150.00")
    }
    
    func testDisplayRemakeWithdrawShowsExpectedValues() {
        let withdraw = WithdrawHelpers().mockedWithdraw(with: 250.0, text: "Withdraw from account")
        sut.displayRemakeWithdraw(with: withdraw, isSideBarMenu: false)
        
        XCTAssertTrue(spy.didDisplayRemakeWithdraw)
        XCTAssertEqual(spy.remakeWithdrawValue, "250.00")
    }
    
    func testDisplayMakeNewWithdrawCallsExpectedMethod() {
        sut.displayMakeNewWithdraw(isSideBarMenu: false)
        
        XCTAssertTrue(spy.didDisplayMakeNewWithdraw)
    }
    
    func testDisplayEditAccountShowsExpectedData() {
        let withdraw = WithdrawHelpers().mockedWithdraw(with: 250.0, text: "Withdraw from account")
        sut.displayEditAccount(with: withdraw, bank: nil, isSidebarMenu: false)
        
        XCTAssertTrue(spy.didDisplayEditAccount)
        XCTAssertEqual(spy.editAccountData.isSideBarMenu, false)
        XCTAssertEqual(spy.editAccountData.lastWithdrawValue, "250.00")
        XCTAssertEqual(spy.editAccountData.reasonDescription, "N/A")
    }
}

class WithdrawResultDisplaySpy: WithdrawResultDisplay {
    var didDisplayResultViewData = false
    var actionButtonTitleText = ""
    var actionButtonAction = ButtonAction.newWithdraw
    var didDisplayChangeAccount = false
    var changeAccountWithdrawValue: String? = ""
    var didDisplayEditAccount = false
    var editAccountData: EditAccountData!
    var didDisplayRemakeWithdraw = false
    var remakeWithdrawValue: String? = ""
    var didDisplayMakeNewWithdraw = false
    
    func displayResultViewData(with actionButtontitle: String, buttonAction: ButtonAction, text: String, description: String) {
        didDisplayResultViewData = true
        actionButtonTitleText = actionButtontitle
        actionButtonAction = buttonAction
    }
    
    func displayChangeAccount(with value: String?, isSideBarMenu: Bool) {
        didDisplayChangeAccount = true
        changeAccountWithdrawValue = value
    }
    
    func displayEditAccount(with data: EditAccountData) {
        didDisplayEditAccount = true
        editAccountData = data
    }
    
    func displayRemakeWithdraw(with value: String?, isSideBarMenu: Bool) {
        didDisplayRemakeWithdraw = true
        remakeWithdrawValue = value
    }
    
    func displayMakeNewWithdraw(isSideBarMenu: Bool) {
        didDisplayMakeNewWithdraw = true
    }
}
