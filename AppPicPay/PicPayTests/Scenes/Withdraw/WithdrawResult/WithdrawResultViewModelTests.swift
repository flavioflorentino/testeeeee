@testable import PicPay
import XCTest

class WithdrawResultViewModelTests: XCTestCase {
    var sut: WithdrawResultViewModel!
    var spy: WithdrawResulPresenterSpy!
    
    override func setUp() {
        super.setUp()
        spy = WithdrawResulPresenterSpy()
        sut = WithdrawResultViewModel(presenter: spy,
                                      lastWithdraw: WithdrawHelpers().mockedWithdraw(with: 200.0, text: "Account Withdraw"),
                                      lastWithdrawBank: nil,
                                      isSideBarMenu: false)
    }

    override func tearDown() {
        spy = nil
        sut = nil
        super.tearDown()
    }
    
    func testConfigureValuesCallsPresenterWithExpectedDisplayValuesForBasicWithdraw() {
        sut.configureValues()
        
        XCTAssertTrue(spy.didDisplayValues)
        XCTAssertEqual(spy.actionButtonTitleText, "Solicitar nova transferência")
        XCTAssertEqual(spy.actionForButton, ButtonAction.newWithdraw)
    }
    
    func testConfigureValuesCallsPresenterWithExpectedDisplayValuesForAccountChangeRequired() {
        let withdraw = WithdrawHelpers().mockedWithdraw(with: 200.0, text: "Account Withdraw", statusCode: .completed, account: "1234", requireAccountChange: true)
        sut = WithdrawResultViewModel(presenter: spy,
                                      lastWithdraw: withdraw,
                                      lastWithdrawBank: nil,
                                      isSideBarMenu: false)
        sut.configureValues()
        
        XCTAssertTrue(spy.didDisplayValues)
        XCTAssertEqual(spy.actionButtonTitleText, "Corrigir dados")
        XCTAssertEqual(spy.actionForButton, ButtonAction.editAccount)
    }
    
    func testConfigureValuesCallsPresenterWithExpectedDisplayValuesForBankNotAuthorizedReason() {
        let withdraw = WithdrawHelpers().mockedWithdraw(with: 200.0, text: "Account Withdraw", statusCode: .completed, account: "1234", reasonID: .bankNotAuthorized)
        sut = WithdrawResultViewModel(presenter: spy,
                                      lastWithdraw: withdraw,
                                      lastWithdrawBank: nil,
                                      isSideBarMenu: false)
        sut.configureValues()
        
        XCTAssertTrue(spy.didDisplayValues)
        XCTAssertEqual(spy.actionButtonTitleText, "Solicitar novamente")
        XCTAssertEqual(spy.actionForButton, ButtonAction.remakeWithdraw)
    }
    
    func testChangeAccountCallsPresenterToDisplayAccountChange() {
        sut.changeAccount()
        
        XCTAssertTrue(spy.didDisplayChangeAccount)
        XCTAssertEqual(spy.changeAccountWithdrawValue, NSDecimalNumber(value: 200.0))
    }
    
    func testEditAccountDisplayAccountToEdit() {
        sut.editAccount()
        
        XCTAssertTrue(spy.didDisplayEditAccount)
    }
    
    func testRemakeWithdrawDisplayWithdrawWithLastWithdraw() {
        sut.remakeWithdraw()
        
        XCTAssertTrue(spy.didDisplayRemakeWithdraw)
        XCTAssertEqual(spy.remakeLastWithdrawText, "Account Withdraw")
    }
    
    func testMakeNewWithdrawDisplayNewWithdraw() {
        sut.makeNewWithdraw()
        
        XCTAssertTrue(spy.didDisplayMakeNewWithdraw)
    }
}

class WithdrawResulPresenterSpy: WithdrawResultPresenting {
    var viewController: WithdrawResultDisplay?
    
    var didDisplayValues = false
    var actionButtonTitleText = ""
    var actionForButton = ButtonAction.newWithdraw
    var didDisplayChangeAccount = false
    var changeAccountWithdrawValue: NSDecimalNumber!
    var didDisplayRemakeWithdraw = false
    var remakeLastWithdrawText: String? = ""
    var didDisplayMakeNewWithdraw = false
    var didDisplayEditAccount = false
    
    func displayValues(actionButtonTitle: String, buttonAction: ButtonAction, withdrawData: PPWithdrawal) {
        didDisplayValues = true
        actionButtonTitleText = actionButtonTitle
        actionForButton = buttonAction
    }
    
    func displayChangeAccount(with withdrawData: PPWithdrawal, isSideBarMenu: Bool) {
        didDisplayChangeAccount = true
        changeAccountWithdrawValue = withdrawData.value
    }
    
    func displayRemakeWithdraw(with withdrawData: PPWithdrawal, isSideBarMenu: Bool) {
        didDisplayRemakeWithdraw = true
        remakeLastWithdrawText = withdrawData.text
    }
    
    func displayMakeNewWithdraw(isSideBarMenu: Bool) {
        didDisplayMakeNewWithdraw = true
    }
    
    func displayEditAccount(with lastWithdraw: PPWithdrawal, bank: PPBank?, isSidebarMenu: Bool) {
        didDisplayEditAccount = true
    }
    
    
}
