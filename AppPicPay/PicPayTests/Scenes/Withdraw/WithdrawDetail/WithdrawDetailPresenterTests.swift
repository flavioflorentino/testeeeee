@testable import PicPay
import XCTest
import CoreLegacy

class WithdrawDetailPresenterTests: XCTestCase {
    var sut: WithdrawDetailPresenter!
    var spy: WithdrawDetailDisplaySpy!
    
    override func setUp() {
        super.setUp()
        spy = WithdrawDetailDisplaySpy()
        sut = WithdrawDetailPresenter()
        sut.viewController = spy
    }

    override func tearDown() {
        spy = nil
        sut = nil
        super.tearDown()
    }
    
    func testConfigureViewsWithCompletedStatusDisplaysWithdrawWithCompletedInfoView() {
        let withdraw = WithdrawHelpers().mockedWithdraw(with: 200.0, text: "Account Withdraw", statusCode: .completed)
        sut.configureViews(with: withdraw)
        
        XCTAssertTrue(spy.didDisplayWithdraw)
        XCTAssertEqual(spy.withdrawValueString, CurrencyFormatter.brazillianRealString(from: withdraw.value))
        XCTAssertEqual(spy.withdrawAccount, withdraw.bankAccountDescription)
        XCTAssertTrue(spy.detailInfoView.isKind(of: WithdrawalCompleteView.self))
    }
    
    func testConfigureViewsWithPendingStatusDisplaysWithdrawWithPendingInfoView() {
        let withdraw = WithdrawHelpers().mockedWithdraw(with: 200.0, text: "Account Withdraw", statusCode: .inProgress)
        sut.configureViews(with: withdraw)
        
        XCTAssertTrue(spy.didDisplayWithdraw)
        XCTAssertEqual(spy.withdrawValueString, CurrencyFormatter.brazillianRealString(from: withdraw.value))
        XCTAssertEqual(spy.withdrawAccount, withdraw.bankAccountDescription)
        XCTAssertTrue(spy.detailInfoView.isKind(of: WithdrawalPendingView.self))
    }
    
    func testCloseDetailCallsDisplayClose() {
        sut.closeDetail(with: true)
    }
}

class WithdrawDetailDisplaySpy: WithdrawDetailDisplay {
    var didDisplayWithdraw = false
    var didDisplayClose = false
    var withdrawValueString = ""
    var withdrawAccount = ""
    var detailInfoView: UIView!
    
    func displayWithdraw(with valueString: String, account: String, detailView: UIView) {
        didDisplayWithdraw = true
        withdrawValueString = valueString
        withdrawAccount = account
        detailInfoView = detailView
    }
    
    func displayCloseDetail(with success: Bool) {
        didDisplayClose = true
    }
}
