@testable import PicPay
import XCTest

class WithdrawDetailCoordinatorTests: XCTestCase {
    var sut: WithdrawDetailCoordinator!
    var firstViewController: UIViewController!
    var testNavViewController: NavigationControllerMock!
    
    override func setUp() {
        super.setUp()
        testNavViewController = NavigationControllerMock(rootViewController: UIViewController())
        testNavViewController.title = "TestNavigation"
        
        firstViewController = UIViewController()
        firstViewController.title = "First"

        let window = UIWindow(frame: UIScreen.main.bounds)
        window.makeKeyAndVisible()
        window.rootViewController = firstViewController
        _ = firstViewController.view
        firstViewController.present(testNavViewController, animated: false)
        
        sut = WithdrawDetailCoordinator()
        sut.viewController = firstViewController.presentedViewController
    }

    override func tearDown() {
        testNavViewController = nil
        firstViewController = nil
        sut = nil
        super.tearDown()
    }
    
    func testCloseActionGoesBackToPreviousScreen() {
        XCTAssertEqual(firstViewController.presentedViewController?.title, "TestNavigation")
        sut.perform(action: .close(success: true))
        
        XCTAssertTrue(testNavViewController.isDismissViewControllerCalled)
    }
}
