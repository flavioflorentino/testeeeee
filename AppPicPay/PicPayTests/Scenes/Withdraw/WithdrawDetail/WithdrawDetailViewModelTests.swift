@testable import PicPay
import XCTest

class WithdrawDetailViewModelTests: XCTestCase {
    var sut: WithdrawDetailViewModel!
    var spy: WithdrawDetailPresenterSpy! = WithdrawDetailPresenterSpy()
    
    override func setUp() {
        super.setUp()
        spy = WithdrawDetailPresenterSpy()
        sut = WithdrawDetailViewModel(presenter: spy,
                                      withdraw: WithdrawHelpers().mockedWithdraw(with: 200.0, text: "Withdraw from account"),
                                      isSidebarMenu: false)
    }

    override func tearDown() {
        spy = nil
        sut = nil
        super.tearDown()
    }
    
    func testConfureViewsCallPresenterToConfigure() {
        sut.configureViews()
        
        XCTAssertTrue(spy.didConfigureViews)
        XCTAssertEqual(spy.withdrawDetail.text, "Withdraw from account")
        XCTAssertEqual(spy.withdrawDetail.value, NSDecimalNumber(value: 200.0))
    }
    
    func testCloseDetailViewWithWithdrawCompletedStatusCallsPresenterWithSuccess() {
        sut.closeDetail()
        
        XCTAssertTrue(spy.didCloseDetailView)
        XCTAssertTrue(spy.isSuccessWithdrawStatus)
    }
    
    func testCloseDetailViewWithWithdrawStatusNotCompletedCallsPresenterWithFailure() {
        let withdraw = WithdrawHelpers().mockedWithdraw(with: 200.0, text: "Account Withdraw", statusCode: .canceled)
        sut = WithdrawDetailViewModel(presenter: spy,
                                      withdraw: withdraw,
                                      isSidebarMenu: false)
        sut.closeDetail()
        
        XCTAssertTrue(spy.didCloseDetailView)
        XCTAssertFalse(spy.isSuccessWithdrawStatus)
    }
    
}

class WithdrawDetailPresenterSpy: WithdrawDetailPresenting {
    var viewController: WithdrawDetailDisplay?
    
    var didConfigureViews = false
    var didCloseDetailView = false
    var withdrawDetail: PPWithdrawal!
    var isSuccessWithdrawStatus = false
    
    func configureViews(with withdraw: PPWithdrawal) {
        didConfigureViews = true
        withdrawDetail = withdraw
    }
    
    func closeDetail(with success: Bool) {
        didCloseDetailView = true
        isSuccessWithdrawStatus = success
    }
}
