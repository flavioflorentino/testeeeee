@testable import PicPay
import XCTest

class FakeAvailableWithdrawalViewController: AvailableWithdrawalDisplay {
    var didDisplaytList = false
    var listItems: [AvailableWithdrawalCellItem]?
    
    
    func displayList(_ listItems: [AvailableWithdrawalCellItem]) {
        didDisplaytList = true
        self.listItems = listItems
    }
}

class FakeAvailableWithdrawalCoordinator: AvailableWithdrawalCoordinating {
    var viewController: UIViewController?
    var didPerformUrlAction = false
    var inputUrlToOpenWebview: URL?
    
    func perform(action: AvailableWithdrawalAction) {
        switch action {
        case .openWebview(let url):
            didPerformUrlAction = true
            inputUrlToOpenWebview = url
        }
    }
}

class AvailableWithdrawalPresenterTest: XCTestCase {
    var presenter: AvailableWithdrawalPresenting!
    var controller: FakeAvailableWithdrawalViewController!
    var coordinator: FakeAvailableWithdrawalCoordinator!
    
    static let mockedValues = AvailableWithdrawalModel(
        balance: NSDecimalNumber(floatLiteral: 1234.56),
        promotional: NSDecimalNumber(floatLiteral: 196.08),
        withdrawal: NSDecimalNumber(floatLiteral: 1038.48))!
    
    override func setUp() {
        super.setUp()
        coordinator = FakeAvailableWithdrawalCoordinator()
        presenter = AvailableWithdrawalPresenter(coordinator: coordinator)
        controller = FakeAvailableWithdrawalViewController()
        presenter.viewController = controller
    }
    
    func testBuildList() {
        let balanceOutputItem = AvailableWithdrawalCellItem(description: AvailableWithdrawalLocalizable.totalWalletBalance.text, value: "R$ 1.234,56", highlight: false)
        let cashbackOutputItem = AvailableWithdrawalCellItem(description: AvailableWithdrawalLocalizable.totalPromotionalBalance.text, value: "R$ 196,08", highlight: false)
        let withdrawOutputItem = AvailableWithdrawalCellItem(description: AvailableWithdrawalLocalizable.totalAvaliableWithdrawal.text, value: "R$ 1.038,48", highlight: true)
        
        presenter.buildList(with: AvailableWithdrawalPresenterTest.mockedValues)
        XCTAssert(controller.didDisplaytList)
        XCTAssertNotNil(controller.listItems)
        let items = controller.listItems!
        
        XCTAssertEqual(items.count, 3)
        
        XCTAssertEqual(items[0].description, balanceOutputItem.description)
        XCTAssertEqual(items[0].value, balanceOutputItem.value)
        XCTAssertEqual(items[0].highlight, balanceOutputItem.highlight)
        
        XCTAssertEqual(items[1].description, cashbackOutputItem.description)
        XCTAssertEqual(items[1].value, cashbackOutputItem.value)
        XCTAssertEqual(items[1].highlight, cashbackOutputItem.highlight)
        
        XCTAssertEqual(items[2].description, withdrawOutputItem.description)
        XCTAssertEqual(items[2].value, withdrawOutputItem.value)
        XCTAssertEqual(items[2].highlight, withdrawOutputItem.highlight)
    }
    
    func testOpenWebview() {
        let expectedUrl = URL(string: "https://ajuda.picpay.com/picpay/retirar-dinheiro-do-picpay")!
        presenter.openWebview(url: expectedUrl)
        XCTAssert(coordinator.didPerformUrlAction)
        XCTAssertEqual(expectedUrl, coordinator.inputUrlToOpenWebview)
    }
}
