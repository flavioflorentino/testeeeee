@testable import PicPay
import XCTest

class AvailableWithdrawalModelTest: XCTestCase {
    let balance = NSDecimalNumber(string: "35.00")
    let promotional = NSDecimalNumber(string: "10.00")
    let withdrawal = NSDecimalNumber(string: "25.00")
    
    func testInitWithNonNilValues() {
        let model = AvailableWithdrawalModel(balance: balance, promotional: promotional, withdrawal: withdrawal)
        XCTAssertNotNil(model)
    }
    
    func testInitWithNilValues() {
        var model = AvailableWithdrawalModel(balance: nil, promotional: promotional, withdrawal: withdrawal)
        XCTAssertNil(model)
        model = AvailableWithdrawalModel(balance: balance, promotional: nil, withdrawal: withdrawal)
        XCTAssertNil(model)
        model = AvailableWithdrawalModel(balance: balance, promotional: promotional, withdrawal: nil)
        XCTAssertNil(model)
    }
}
