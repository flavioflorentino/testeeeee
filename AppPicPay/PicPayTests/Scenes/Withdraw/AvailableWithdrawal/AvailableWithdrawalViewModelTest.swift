@testable import PicPay
import XCTest

class FakeAvailableWithdrawalPresenter: AvailableWithdrawalPresenting {
    var viewController: AvailableWithdrawalDisplay?
    var inputModelToBuildList: AvailableWithdrawalModel?
    var inputUrlToOpenWebview: URL?
    
    func buildList(with values: AvailableWithdrawalModel) {
        inputModelToBuildList = values
    }
    
    func openWebview(url: URL) {
        inputUrlToOpenWebview = url
    }
}

class AvailableWithdrawalViewModelTest: XCTestCase {
    var viewModel: AvailableWithdrawalViewModel!
    var presenter: FakeAvailableWithdrawalPresenter!
    let values = AvailableWithdrawalPresenterTest.mockedValues
    
    override func setUp() {
        super.setUp()
        presenter = FakeAvailableWithdrawalPresenter()
        viewModel = AvailableWithdrawalViewModel(withValues: values, presenter: presenter)
    }
    
    func testBuildList() {
        viewModel.fetchData()
        XCTAssertNotNil(presenter.inputModelToBuildList)
        XCTAssertEqual(presenter.inputModelToBuildList?.balance, values.balance)
        XCTAssertEqual(presenter.inputModelToBuildList?.promotionalBalance, values.promotionalBalance)
        XCTAssertEqual(presenter.inputModelToBuildList?.withdrawalBalance, values.withdrawalBalance)
    }
    
    func testDidTapBottomLabel() {
        viewModel.didTapBottomLabel()
        let expectedUrl = URL(string: "https://ajuda.picpay.com/picpay/retirar-dinheiro-do-picpay")!
        XCTAssertNotNil(presenter.inputUrlToOpenWebview)
        XCTAssertEqual(presenter.inputUrlToOpenWebview, expectedUrl)
    }
}
