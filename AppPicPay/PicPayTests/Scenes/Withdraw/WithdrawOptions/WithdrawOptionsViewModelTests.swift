@testable import PicPay
import XCTest

class WithdrawOptionsViewModelTests: XCTestCase {
    var sut: WithdrawOptionsViewModel!
    var mock: WithdrawOptionsServicingMock!
    var spy: WithdrawOptionsViewModelOutputsSpy! = WithdrawOptionsViewModelOutputsSpy()

    override func setUp() {
        super.setUp()
        spy = WithdrawOptionsViewModelOutputsSpy()
        mock = WithdrawOptionsServicingMock()
        sut = WithdrawOptionsViewModel(service: mock, consumer: nil)
        sut.outputs = spy
    }

    override func tearDown() {
        spy = nil
        mock = nil
        sut = nil
        super.tearDown()
    }

    func testLoadingOptionsWithSuccessOutputsTheResult() {
        mock.isSuccess = true
        sut.loadOptions()
        XCTAssertTrue(spy.didShowOptions)
        XCTAssertTrue(spy.withdrawOptions[0].type == .bank24hours)
        XCTAssertTrue(spy.withdrawOptions[0].title == "Saque")
        XCTAssertTrue(spy.withdrawOptions[0].status == .available)
        XCTAssertNil(spy.withdrawOptions[0].statusDescription)
        XCTAssertFalse(spy.didShowLoading)
    }

    func testLoadingOptionsWithErrorOutputsError() {
        mock.isSuccess = false
        sut.loadOptions()
        XCTAssertTrue(spy.didShowError)
        XCTAssertNotNil(spy.withdrawError)
        XCTAssertTrue(spy.withdrawError?.localizedDescription == "Não foi possível carregar as opções de saque")
    }
    
    func testChosingAtm24OptionShowsAtm24Withdraw() {
        sut.loadOptions()
        let opt = WithdrawOption(type: .bank24hours, title: "", description: "", status: .available, statusDescription: "")
        sut.chooseOption(opt, from: [])
        XCTAssertTrue(spy.didShowAtm24)
    }
    
    func testChosingBankTransferOptionShowsBankListIfBankAccountIsNil() {
        sut.loadOptions()
        let opt = WithdrawOption(type: .transfer, title: "", description: "", status: .available, statusDescription: "")
        sut.chooseOption(opt, from: [])
        XCTAssertTrue(spy.didShowBanckList)
    }
    
    func testChosingBankTransferOptionShowswithdrawFormIfBankAccountIsNotNil() {
        let consumer = MBConsumer()
        consumer.bankAccount = PPBankAccount()
        sut = WithdrawOptionsViewModel(service: mock, consumer: consumer)
        sut.outputs = spy
        
        sut.loadOptions()
        let opt = WithdrawOption(type: .transfer, title: "", description: "", status: .available, statusDescription: "")
        sut.chooseOption(opt, from: [])
        XCTAssertTrue(spy.didShowWithdrawForm)
    }
    
    func testChosingOriginalTransferOptionShowsOriginalTransfer() {
        sut.loadOptions()
        let opt = WithdrawOption(type: .original, title: "", description: "", status: .available, statusDescription: "")
        sut.chooseOption(opt, from: [])
        XCTAssertTrue(spy.didShowOriginalNewAccount)
    }
    
    func testChosingOriginalTransferOptionShowsOriginalNewAccount() {
        let consumer = MBConsumer()
        consumer.bankAccount = WithdrawOptionsHelper().originalBankAccount
        sut = WithdrawOptionsViewModel(service: mock, consumer: consumer)
        sut.outputs = spy
        
        sut.loadOptions()
        let opt = WithdrawOption(type: .original, title: "", description: "", status: .available, statusDescription: "")
        sut.chooseOption(opt, from: [])
        XCTAssertTrue(spy.didShowOriginalTransfer)
    }
}

class WithdrawOptionsServicingMock: WithdrawOptionsServicing {
    var isSuccess = true
    func loadWithdrawOptions(_ completion: @escaping WithDrawOptionsCompletionBlock) {
        if isSuccess {
            completion([WithdrawOption(type: .bank24hours, title: "Saque", description: "description", status: .available, statusDescription: nil)], nil)
        } else {
            completion(nil, NSError(domain: "Erro", code: 999, userInfo: [NSLocalizedDescriptionKey: "Não foi possível carregar as opções de saque"]))
        }
    }
}

class WithdrawOptionsViewModelOutputsSpy: WithdrawOptionsViewModelOutputs {
    func showLoading(_ loading: Bool) {
        didShowLoading = loading
    }
    
    func didNextStep(_ action: WithdrawOptionsAction) {
        switch action {
        case .bank24Hours:
            didShowAtm24 = true
        case .bankList:
            didShowBanckList = true
        case .bankTransfer:
            didShowWithdrawForm = true
        case .originalNewAccount:
            didShowOriginalNewAccount = true
        case .originalTransfer:
            didShowOriginalTransfer = true
        case .pix:
            didShowPix = true
        }
    }
    
    var didShowLoading = false
    var didShowError = false
    var didShowOptions = false
    var withdrawOptions: [WithdrawOption] = []
    var withdrawError: Error?
    var didShowAtm24 = false
    var didShowBanckList = false
    var didShowWithdrawForm = false
    var didShowOriginalTransfer = false
    var didShowOriginalNewAccount = false
    var didShowPix = false
    
    func showErrorView(_ error: Error?) {
        didShowError = true
        withdrawError = error
    }

    func showOptions(_ options: [WithdrawOption]) {
        didShowOptions = true
        withdrawOptions = options
    }
}
