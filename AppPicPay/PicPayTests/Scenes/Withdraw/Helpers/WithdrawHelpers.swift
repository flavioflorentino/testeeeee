@testable import PicPay
import Foundation

class WithdrawHelpers {
    
    func mockedWithdraw(with value: Double, text: String, statusCode: WithdrawalStatusCode = .completed, account: String = "1234", requireAccountChange: Bool = false, reasonID: WithdrawalCancelReason = .limitExceeded) -> PPWithdrawal {
        let withdraw = PPWithdrawal()
        withdraw.text = text
        withdraw.value = NSDecimalNumber(value: value)
        withdraw.statusCode = statusCode.rawValue
        withdraw.bankAccountDescription = account
        withdraw.requireAccountChange = requireAccountChange
        withdraw.reasonId = reasonID.rawValue
        withdraw.reasonDescription = "N/A"
        withdraw.date = Date()
        return withdraw
    }
}
