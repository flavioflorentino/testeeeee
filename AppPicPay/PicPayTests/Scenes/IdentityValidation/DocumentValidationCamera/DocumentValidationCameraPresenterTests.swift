@testable import PicPay
import UI
import XCTest

private final class DocumentValidationCameraCoordinatorSpy: DocumentValidationCameraCoordinating {
    private(set) var callCloseActionCount = 0
    private(set) var callPreviewActionCount = 0

    func perform(action: DocumentValidationCameraAction) {
        switch action {
        case .close:
            callCloseActionCount += 1
        case .preview:
            callPreviewActionCount += 1
        }
    }
}

private final class DocumentValidationCameraControllerSpy: DocumentValidationCameraDisplay {
    private(set) var hintText: NSAttributedString?
    private(set) var callDisplayCameraInfoCount = 0
    private(set) var callAuthorizedCameraAccess = 0
    private(set) var callDeniedCameraAccess = 0
    private(set) var callDisplayAlert = 0

    func displayCameraInfo(maskType: IdentityValidationStep.StepType, hintText: NSAttributedString) {
        callDisplayCameraInfoCount += 1
        self.hintText = hintText
    }

    func displayAuthorizedCameraAccess() {
        callAuthorizedCameraAccess += 1
    }

    func displayDeniedCameraAccess() {
        callDeniedCameraAccess += 1
    }

    func displayAlert(with error: Error?) {
        callDisplayAlert += 1
    }
}

final class DocumentValidationCameraPresenterTests: XCTestCase {
    private let coordinatorSpy = DocumentValidationCameraCoordinatorSpy()
    private let viewControllerSpy = DocumentValidationCameraControllerSpy()

    private lazy var sut: DocumentValidationCameraPresenter = {
        let presenter = DocumentValidationCameraPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()

    func testPresentCameraInfo_WhenStepIsInitial_ShouldNotCallDisplayCameraInfo() {
        let step = IdentityValidationStep(type: .initial, title: "", text: "", data: nil)
        sut.presentCameraInfo(forStep: step)

        XCTAssertEqual(viewControllerSpy.callDisplayCameraInfoCount, 0)
    }

    func testPresentCameraInfo_WhenStepIsDocumentFront_ShouldDisplayHintText() {
        let step = IdentityValidationStep(type: .documentFront, title: "", text: "", data: nil)
        sut.presentCameraInfo(forStep: step)

        let expectedAttributedString = highlight(
            string: FacialBiometricLocalizable.documentFrontCameraTitle.text,
            at: FacialBiometricLocalizable.documentFrontCameraHighlight.text
        )
        XCTAssertEqual(viewControllerSpy.hintText, expectedAttributedString)
    }

    func testPresentCameraInfo_WhenStepIsDocumentFront_ShouldCallDisplayInfoOnlyOnce() {
        let step = IdentityValidationStep(type: .documentFront, title: "", text: "", data: nil)
        sut.presentCameraInfo(forStep: step)

        XCTAssertEqual(viewControllerSpy.callDisplayCameraInfoCount, 1)
    }

    func testPresentCameraInfo_WhenStepIsDocumentBack_ShouldDisplayHintText() {
        let step = IdentityValidationStep(type: .documentBack, title: "", text: "", data: nil)
        sut.presentCameraInfo(forStep: step)

        let expectedAttributedString = highlight(
            string: FacialBiometricLocalizable.documentBackCameraTitle.text,
            at: FacialBiometricLocalizable.documentBackCameraHighlight.text
        )
        XCTAssertEqual(viewControllerSpy.hintText, expectedAttributedString)
    }

    func testPresentCameraInfo_WhenStepIsDocumentBack_ShouldCallDisplayInfoOnlyOnce() {
        let step = IdentityValidationStep(type: .documentBack, title: "", text: "", data: nil)
        sut.presentCameraInfo(forStep: step)

        XCTAssertEqual(viewControllerSpy.callDisplayCameraInfoCount, 1)
    }

    func testConfigureCameraAuthorized_ShouldCallDisplayAuthorizedCameraAccess() {
        sut.configureCameraAuthorized()

        XCTAssertEqual(viewControllerSpy.callAuthorizedCameraAccess, 1)
    }

    func testConfigureCameraDenied_ShouldCallDisplayDeniedCameraAccess() {
        sut.configureCameraDenied()

        XCTAssertEqual(viewControllerSpy.callDeniedCameraAccess, 1)
    }

    func testClose_ShouldPerformCloseAction() {
        sut.close()

        XCTAssertEqual(coordinatorSpy.callCloseActionCount, 1)
    }

    func testHandleCaptureError_ShouldCallDisplayAlert() {
        sut.handleCaptureError(error: nil)

        XCTAssertEqual(viewControllerSpy.callDisplayAlert, 1)
    }

    func testShowPreview_ShouldPerformPreviewAction() {
        let step = IdentityValidationStep(type: .documentBack, title: "", text: "", data: nil)
        sut.showPreview(with: UIImage(), currentStep: step)

        XCTAssertEqual(coordinatorSpy.callPreviewActionCount, 1)
    }
}

private extension DocumentValidationCameraPresenterTests {
    func highlight(string: String, at substring: String) -> NSMutableAttributedString {
        let attributedText = NSMutableAttributedString(string: string)
        let attributedRange = (attributedText.string as NSString).range(of: substring)

        attributedText.addAttributes(
            [.font: Typography.bodyPrimary(.highlight).font()],
            range: attributedRange
        )

        return attributedText
    }
}
