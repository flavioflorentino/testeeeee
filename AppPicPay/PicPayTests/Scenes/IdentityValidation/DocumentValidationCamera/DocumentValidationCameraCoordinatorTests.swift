@testable import PicPay
import XCTest

final class DocumentValidationCameraCoordinatorTests: XCTestCase {
    private let navController = NavigationControllerMock(rootViewController: UIViewController())

    private lazy var sut = DocumentValidationCameraCoordinator(navigationController: navController)

    func testPerform_WhenActionIsClose_ShouldDismissNavigation() {
        sut.perform(action: .close)

        XCTAssertTrue(navController.isDismissViewControllerCalled)
    }

    func testPerform_WhenActionIsPreview_ShouldPushPreviewController() throws {
        let step = IdentityValidationStep(type: .documentFront)
        sut.perform(action: .preview(image: UIImage(), currentStep: step))

        let pushedController = try XCTUnwrap(navController.pushedViewController)
        XCTAssertTrue(pushedController is DocumentCapturePreviewViewController)
    }
}
