import AnalyticsModule
import PermissionsKit
@testable import PicPay
import XCTest

private final class DocumentValidationCameraServiceMock: DocumentValidationCameraServicing {
    var permissionStatus: PermissionStatus = .notDetermined

    func checkCameraPermission(completion: @escaping (PermissionStatus) -> Void) {
        completion(permissionStatus)
    }
}

private final class DocumentValidationCameraPresenterSpy: DocumentValidationCameraPresenting {
    var viewController: DocumentValidationCameraDisplay?
    private(set) var callPresentCameraInfo = 0
    private(set) var callConfigureCameraAuthorized = 0
    private(set) var callConfigureCameraDenied = 0
    private(set) var callHandleCaptureError = 0
    private(set) var callShowPreview = 0
    private(set) var callClose = 0

    func presentCameraInfo(forStep step: IdentityValidationStep) {
        callPresentCameraInfo += 1
    }

    func configureCameraAuthorized() {
        callConfigureCameraAuthorized += 1
    }

    func configureCameraDenied() {
        callConfigureCameraDenied += 1
    }

    func close() {
        callClose += 1
    }

    func handleCaptureError(error: Error?) {
        callHandleCaptureError += 1
    }

    func showPreview(with previewImage: UIImage, currentStep: IdentityValidationStep) {
        callShowPreview += 1
    }
}

final class DocumentValidationCameraViewModelTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private lazy var containerMock = DependencyContainerMock(analyticsSpy)
    private let serviceMock = DocumentValidationCameraServiceMock()
    private let presenterSpy = DocumentValidationCameraPresenterSpy()

    private lazy var sut: DocumentValidationCameraViewModel = {
        let step = IdentityValidationStep(type: .documentFront, title: "", text: "", data: nil)
        let viewModel = DocumentValidationCameraViewModel(
            service: serviceMock, 
            presenter: presenterSpy, 
            step: step, 
            dependencies: containerMock
        )
        return viewModel
    }()

    func testLoadCameraInfo_ShouldPresentCameraInfo() {
        sut.loadCameraInfo()

        XCTAssertEqual(presenterSpy.callPresentCameraInfo, 1)
    }

    func testLoadCameraInfo_WhenStepIsDocumentFront_ShouldLogEvent() {
        sut.loadCameraInfo()

        let expectedEvent = IdentityValidationEvent.documentFront(action: .none).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testLoadCameraInfo_WhenStepIsDocumentBack_ShouldLogEvent() {
        initializeSut(with: .documentBack)
        sut.loadCameraInfo()

        let expectedEvent = IdentityValidationEvent.documentBack(action: .none).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testCheckCameraPermission_WhenStatusIsAuthorized_ShouldCallConfigureCameraAuthorized() {
        serviceMock.permissionStatus = .authorized
        sut.checkCameraPermission()

        XCTAssertEqual(presenterSpy.callConfigureCameraAuthorized, 1)
    }

    func testCheckCameraPermission_WhenStatusIsNotAuthorized_ShouldCallConfigureCameraDenied() {
        serviceMock.permissionStatus = .denied
        sut.checkCameraPermission()

        XCTAssertEqual(presenterSpy.callConfigureCameraDenied, 1)
    }

    func testHandleCaptureImageError_ShouldCallHandleCaptureError() {
        sut.handleCaptureImageError(nil)

        XCTAssertEqual(presenterSpy.callHandleCaptureError, 1)
    }

    func testHandleCaptureImageSuccess_ShouldCallShowPreview() {
        sut.handleCaptureImageSuccess(image: UIImage())

        XCTAssertEqual(presenterSpy.callShowPreview, 1)
    }

    func testHandleCaptureImageSuccess_WhenStepIsDocumentFront_ShouldLogEvent() {
        sut.handleCaptureImageSuccess(image: UIImage())

        let expectedEvent = IdentityValidationEvent.documentFront(action: .taken).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testHandleCaptureImageSuccess_WhenStepIsDocumentBack_ShouldLogEvent() {
        initializeSut(with: .documentBack)
        sut.handleCaptureImageSuccess(image: UIImage())

        let expectedEvent = IdentityValidationEvent.documentBack(action: .taken).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testClose_ShouldCallClose() {
        sut.close()

        XCTAssertEqual(presenterSpy.callClose, 1)
    }

    func testClose_WhenStepIsDocumentFront_ShouldLogEvent() {
        sut.close()

        let expectedEvent = IdentityValidationEvent.documentFront(action: .close).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testClose_WhenStepIsDocumentBack_ShouldLogEvent() {
        initializeSut(with: .documentBack)
        sut.close()

        let expectedEvent = IdentityValidationEvent.documentBack(action: .close).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testClose_WhenStepIsDone_ShouldNotLog() {
        initializeSut(with: .done)
        sut.close()

        XCTAssertNil(analyticsSpy.event)
    }
}

private extension DocumentValidationCameraViewModelTests {
    func initializeSut(with type: IdentityValidationStep.StepType) {
        let step = IdentityValidationStep(type: type, title: "", text: "", data: nil)
        sut = DocumentValidationCameraViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            step: step,
            dependencies: containerMock
        )
    }
}
