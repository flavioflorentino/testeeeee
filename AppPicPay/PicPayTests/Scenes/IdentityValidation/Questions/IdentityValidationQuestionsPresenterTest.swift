import XCTest
@testable import PicPay

final class IdentityValidationQuestionsPresenterTest: XCTestCase {
    private let coordinatorSpy = IdentityValidationQuestionsCoordinatorSpy()
    private let controllerSpy = IdentityValidationQuestionsControllerSpy()
    private let queue = DispatchQueue(label: #function)
    
    private lazy var sut: IdentityValidationQuestionsPresenter = {
        let sut = IdentityValidationQuestionsPresenter(coordinator: coordinatorSpy, dependencies: DependencyContainerMock(queue))
        sut.viewController = controllerSpy
        return sut
    }()
    
    func testLoadStepInfoShouldCallViewController() {
        let question = IdentityValidationQuestion(text: "Questão 1", answer: "", options: ["1", "2", "3"])
        let question2 = IdentityValidationQuestion(text: "Questão 2", answer: "", options: ["1", "2", "3"])
        let question3 = IdentityValidationQuestion(text: "Questão 3", answer: "", options: ["1", "2", "3"])
        let manager = IdentityValidationQuestionManager(questions: [question, question2, question3])
        sut.handleLoadInfoValues(with: manager)
    
        XCTAssertTrue(controllerSpy.didCallPresentInfoValues)
        XCTAssertEqual(controllerSpy.questionInfo?.questionNumber, "Pergunta 1 de 3")
        XCTAssertEqual(controllerSpy.questionInfo?.question, "Questão 1")
        XCTAssertEqual(controllerSpy.questionInfo?.questionOptions.count, 3)
    }
    
    func testHandleNextStepShouldCallCoordinator() {
        let step = IdentityValidationStep(type: .questions, title: "a random title", text: "a random text")
        sut.handleNextStep(step)
        
        let expectation = self.expectation(description: "HandleNextStepShouldCallCoordinator")

        queue.async {
            XCTAssertEqual(self.coordinatorSpy.callNextControllerCount, 1)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5)
    }
    
    func testHandleNextQuestionShouldCallCoordinator() {
        let question = IdentityValidationQuestion(text: "Questão 1", answer: "", options: ["1", "2", "3"])
        let question2 = IdentityValidationQuestion(text: "Questão 2", answer: "", options: ["1", "2", "3"])
        let question3 = IdentityValidationQuestion(text: "Questão 3", answer: "", options: ["1", "2", "3"])
        let manager = IdentityValidationQuestionManager(questions: [question, question2, question3])
        sut.handleNextQuestion(with: manager)
    
        XCTAssertEqual(coordinatorSpy.callShowNextQuestionCount, 1)
    }
    
    func testSendAnswersShouldReturnAnError() {
        let error = PicPayError(message: "a random error")
        sut.handleError(error)
        
        let expectation = self.expectation(description: "SendAnswersShouldReturnAnError")

        queue.async {
            XCTAssertTrue(self.controllerSpy.didCallPresentAlert)
            XCTAssertEqual(self.controllerSpy.error, error)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5)
    }
    
    func testHandleCloseFlowShouldCallCoordinator() {
        sut.handleCloseFlow()
        
        XCTAssertEqual(coordinatorSpy.callDismissCount, 1)
    }
}

final private class IdentityValidationQuestionsControllerSpy: IdentityValidationQuestionsDisplayable {
    private(set) var didCallPresentInfoValues = false
    private(set) var questionInfo: (questionNumber: String, question: String, questionOptions: [String])?
    private(set) var didCallPresentAlert = false
    private(set) var error: PicPayError?

    func presentInfoValues(questionNumber: String, question: String, questionOptions: [String]) {
        didCallPresentInfoValues.toggle()
        self.questionInfo = (questionNumber: questionNumber, question: question, questionOptions: questionOptions)
    }
    
    func presentAlert(with error: PicPayError) {
        didCallPresentAlert.toggle()
        self.error = error
    }
}

final private class IdentityValidationQuestionsCoordinatorSpy: IdentityValidationQuestionsCoordinating {    
    private(set) var callShowNextQuestionCount = 0
    private(set) var callNextControllerCount = 0
    private(set) var callDismissCount = 0

    func showNextQuestion(with questionManager: IdentityValidationQuestionManager) {
        callShowNextQuestionCount += 1
    }

    func showNextController(for step: IdentityValidationStep) {
        callNextControllerCount += 1
    }

    func dismiss() {
        callDismissCount += 1
    }
}
