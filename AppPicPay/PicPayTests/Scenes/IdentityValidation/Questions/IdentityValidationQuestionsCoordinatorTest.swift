import XCTest
@testable import PicPay

final class IdentityValidationQuestionsCoordinatorTest: XCTestCase {
    private let coordinatorDelegateSpy = IdentityValidationCoordinatorSpy()
    private let navController = NavigationControllerMock(rootViewController: UIViewController())
    
    private lazy var sut = IdentityValidationQuestionsCoordinator(coordinatorDelegate: coordinatorDelegateSpy, navigationController: navController)

    func testShowNextController_ShouldCallPushNextController() {
        sut.showNextController(for: IdentityValidationStep())

        XCTAssertEqual(coordinatorDelegateSpy.callPushNextControllerCount, 1)
    }

    func testDismiss_ShouldCallDismiss() {
        sut.dismiss()

        XCTAssertEqual(coordinatorDelegateSpy.callDismissCount, 1)
    }

    func testShowNextQuestion_ShouldPushIdentityValidationQuestionsViewController() throws {
        sut.showNextQuestion(with: IdentityValidationQuestionManager(questions: []))

        XCTAssertTrue(navController.isPushViewControllerCalled)
        let pushedViewController = try XCTUnwrap(navController.pushedViewController)
        XCTAssertTrue(pushedViewController is IdentityValidationQuestionsViewController)
    }
}
