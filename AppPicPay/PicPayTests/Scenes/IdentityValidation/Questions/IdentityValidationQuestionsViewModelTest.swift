import XCTest
@testable import PicPay

final class IdentityValidationQuestionsViewModelTest: XCTestCase {
    private let serviceMock = IdentityValidationQuestionsServiceMock()
    private let presenterSpy = IdentityValidationQuestionsPresenterSpy()
    
    private lazy var manager: IdentityValidationQuestionManager = {
        let question = IdentityValidationQuestion(text: "Questão 1", answer: "", options: ["1", "2", "3"])
        let question2 = IdentityValidationQuestion(text: "Questão 2", answer: "", options: ["1", "2", "3"])
        let question3 = IdentityValidationQuestion(text: "Questão 3", answer: "", options: ["1", "2", "3"])
        return IdentityValidationQuestionManager(questions: [question, question2, question3])
    }()
    
    private lazy var sut = IdentityValidationQuestionsViewModel(questionManager: manager, service: serviceMock, presenter: presenterSpy)
    
    func testLoadInfoValuesShouldReturnAQuestionManager() {
        sut.loadInfoValues()
        
        XCTAssertTrue(presenterSpy.didCallHandleLoadInfoValues)
        XCTAssertEqual(presenterSpy.questionManager?.currentIndex, manager.currentIndex)
    }
    
    func testSkipQuestionShouldReturnAManager() {
        sut.skipQuestion()
        
        XCTAssertTrue(presenterSpy.didCallHandleNextQuestion)
        XCTAssertEqual(presenterSpy.questionManager?.currentIndex, manager.currentIndex + 1)
    }
    
    func testNextQuestionShouldReturnAManager() {
        sut.nextQuestion()
        
        XCTAssertTrue(presenterSpy.didCallHandleNextQuestion)
        XCTAssertEqual(presenterSpy.questionManager?.currentIndex, manager.currentIndex + 1)
    }
    
    func testSendAnswersShouldReturnAStep() {
        serviceMock.isSuccess = true
        
        sut.sendAnswers()
        
        XCTAssertTrue(presenterSpy.didCallHandleNextStep)
        XCTAssertEqual(presenterSpy.step?.type, .questions)
        XCTAssertEqual(presenterSpy.step?.title, "random Title")
        XCTAssertEqual(presenterSpy.step?.text, "random text")
    }
    
    func testSendAnswersShouldReturnAnError() {
        serviceMock.isSuccess = false
        
        sut.sendAnswers()
        
        XCTAssertTrue(presenterSpy.didCallHandleError)
        XCTAssertEqual(presenterSpy.error?.message, "random error")
    }
    
    func testCloseFlowShouldCallPresenter() {
        sut.closeFlow()
        
        XCTAssertTrue(presenterSpy.didCallHandleCloseFlow)
    }
}

final private class IdentityValidationQuestionsServiceMock: IdentityValidationQuestionsServicing {
    var isSuccess = false
    
    func sendAnswers(answers: [String], _ completion: @escaping ((PicPayResult<IdentityValidationStep>) -> Void)) {
        if isSuccess {
            let step = IdentityValidationStep(type: .questions, title: "random Title", text: "random text")
            completion(PicPayResult.success(step)); return
        }
        
        let error = PicPayError(message: "random error")
        completion(PicPayResult.failure(error))
    }
}

final private class IdentityValidationQuestionsPresenterSpy: IdentityValidationQuestionsPresenting {
    var coordinator: IdentityValidationQuestionsCoordinating?
    var viewController: IdentityValidationQuestionsDisplayable?
    
    private(set) var didCallHandleLoadInfoValues = false
    private(set) var questionManager: IdentityValidationQuestionManager?
    private(set) var didCallHandleNextQuestion = false
    private(set) var didCallHandleNextStep = false
    private(set) var step: IdentityValidationStep?
    private(set) var didCallHandleError = false
    private(set) var error: PicPayError?
    private(set) var didCallHandleCloseFlow = false
    
    func handleLoadInfoValues(with questionManager: IdentityValidationQuestionManager) {
        didCallHandleLoadInfoValues.toggle()
        self.questionManager = questionManager
    }
    
    func handleNextQuestion(with questionManager: IdentityValidationQuestionManager) {
        didCallHandleNextQuestion.toggle()
        self.questionManager = questionManager
    }
    
    func handleNextStep(_ step: IdentityValidationStep) {
        didCallHandleNextStep.toggle()
        self.step = step
    }
    
    func handleError(_ error: PicPayError) {
        didCallHandleError.toggle()
        self.error = error
    }
    
    func handleCloseFlow() {
        didCallHandleCloseFlow.toggle()
    }
}
