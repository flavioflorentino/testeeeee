import AnalyticsModule
@testable import PicPay
import XCTest

private final class DocumentCapturePreviewServiceMock: DocumentCapturePreviewServicing {
    var isSuccess = false

    func uploadImage(imageData: Data, completion: @escaping (PicPayResult<IdentityValidationStep>) -> Void) {
        if isSuccess {
            completion(.success(IdentityValidationStep(type: .documentBack, title: "", text: "", data: nil)))
        } else {
            completion(.failure(PicPayError(message: "Error sending document")))
        }
    }
}

private final class DocumentCapturePreviewPresenterSpy: DocumentCapturePreviewPresenting {
    var viewController: DocumentCapturePreviewDisplay?
    private(set) var callPreviewImageCount = 0
    private(set) var callHandleCloseCount = 0
    private(set) var callHandleTakeAnotherPictureCount = 0
    private(set) var callLoadingStateCount = 0
    private(set) var callDisplayErrorCount = 0
    private(set) var errorMessage = ""
    private(set) var callNextStepCount = 0

    func previewImage(_ image: UIImage, for step: IdentityValidationStep) {
        callPreviewImageCount += 1
    }

    func handleClose() {
        callHandleCloseCount += 1
    }

    func handleTakeAnotherPicture() {
        callHandleTakeAnotherPictureCount += 1
    }

    func setLoadingState(_ loading: Bool) {
        callLoadingStateCount += 1
    }

    func displayError(message: String) {
        callDisplayErrorCount += 1
        errorMessage = message
    }

    func nextStep(_ step: IdentityValidationStep) {
        callNextStepCount += 1
    }
}

final class DocumentCapturePreviewViewModelTests: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private lazy var containerMock = DependencyContainerMock(analyticsSpy)
    private let serviceMock = DocumentCapturePreviewServiceMock()
    private let presenterSpy = DocumentCapturePreviewPresenterSpy()

    private lazy var sut = loadSut()

    func testConfigurePreview_ShouldCallPresenterPreviewImage() {
        sut.configurePreview()

        XCTAssertEqual(presenterSpy.callPreviewImageCount, 1)
    }

    func testConfigurePreview_WhenStepIsDocumentFront_ShouldLogEvent() {
        sut.configurePreview()

        let expectedEvent = IdentityValidationEvent.sendDocumentFront(action: .none).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testConfigurePreview_WhenStepIsDocumentBack_ShouldLogEvent() {
        sut = loadSut(stepType: .documentBack)
        sut.configurePreview()

        let expectedEvent = IdentityValidationEvent.sendDocumentBack(action: .none).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testHandleClose_ShouldCallPresenterHandleClose() {
        sut.handleClose()

        XCTAssertEqual(presenterSpy.callHandleCloseCount, 1)
    }

    func testHandleTakeAnotherPicture_ShouldCallPresenterHandleTakeAnotherPicture() {
        sut.handleTakeAnotherPicture()

        XCTAssertEqual(presenterSpy.callHandleTakeAnotherPictureCount, 1)
    }

    func testHandleTakeAnotherPicture_WhenStepIsDocumentFront_ShouldLogEvent() {
        sut.handleTakeAnotherPicture()

        let expectedEvent = IdentityValidationEvent.sendDocumentFront(action: .drop).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testHandleTakeAnotherPicture_WhenStepIsDocumentBack_ShouldLogEvent() {
        sut = loadSut(stepType: .documentBack)
        sut.handleTakeAnotherPicture()

        let expectedEvent = IdentityValidationEvent.sendDocumentBack(action: .drop).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testHandleAcceptPicture_WhenImageDataIsNil_ShouldDisplayCorrectErrorMessage() {
        sut.handleAcceptPicture()

        XCTAssertEqual(presenterSpy.errorMessage, FacialBiometricLocalizable.previewUploadError.text)
    }

    func testHandleAcceptPicture_WhenImageDataIsNil_ShouldLogEvent() {
        sut.handleAcceptPicture()

        let expectedEvent = IdentityValidationEvent.documentError.event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testHandleAcceptPicture_WhenImageDataIsNotNil_ShouldSetLoadingStateTwice() {
        sut = loadSut(with: Assets.Registration.backgroundContinueRegistration.image)
        sut.handleAcceptPicture()

        XCTAssertEqual(presenterSpy.callLoadingStateCount, 2)
    }

    func testHandleAcceptPicture_WhenImageDataIsNotNil_WhenUploadSuccess_ShouldPresentNextStep() {
        sut = loadSut(with: Assets.Registration.backgroundContinueRegistration.image)
        serviceMock.isSuccess = true
        sut.handleAcceptPicture()

        XCTAssertEqual(presenterSpy.callNextStepCount, 1)
    }

    func testHandleAcceptPicture_WhenImageDataIsNotNil_WhenUploadSuccess_WhenStepIsDocumentFront_ShouldLogEvent() {
        sut = loadSut(with: Assets.Registration.backgroundContinueRegistration.image)
        serviceMock.isSuccess = true
        sut.handleAcceptPicture()

        let expectedEvent = IdentityValidationEvent.sendDocumentFront(action: .finished).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testHandleAcceptPicture_WhenImageDataIsNotNil_WhenUploadFailure_ShouldDisplayError() {
        sut = loadSut(with: Assets.Registration.backgroundContinueRegistration.image)
        serviceMock.isSuccess = false
        sut.handleAcceptPicture()

        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 1)
    }

    func testHandleAcceptPicture_WhenImageDataIsNotNil_WhenUploadSuccess_WhenStepIsDocumentBack_ShouldLogEvent() {
        sut = loadSut(with: Assets.Registration.backgroundContinueRegistration.image, stepType: .documentBack)
        serviceMock.isSuccess = true
        sut.handleAcceptPicture()

        let expectedEvent = IdentityValidationEvent.sendDocumentBack(action: .finished).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testHandleRetry_ShouldCallHandleAcceptPicture() {
        sut.handleRetry()

        XCTAssertEqual(presenterSpy.callDisplayErrorCount, 1)
    }
}

private extension DocumentCapturePreviewViewModelTests {
    func loadSut(with image: UIImage = UIImage(), stepType: IdentityValidationStep.StepType = .documentFront) -> DocumentCapturePreviewViewModel {
        let step = IdentityValidationStep(type: stepType, title: "", text: "", data: nil)
        return DocumentCapturePreviewViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            step: step,
            previewImage: image,
            dependencies: containerMock
        )
    }
}
