@testable import PicPay
import XCTest

private final class DocumentCapturePreviewCoordinatorSpy: DocumentCapturePreviewCoordinating {
    private(set) var callCloseActionCount = 0
    private(set) var callTakeAnotherPictureActionCount = 0
    private(set) var callNextStepActionCount = 0

    func perform(action: DocumentCapturePreviewAction) {
        switch action {
        case .close:
            callCloseActionCount += 1
        case .takeAnotherPicture:
            callTakeAnotherPictureActionCount += 1
        case .nextStep:
            callNextStepActionCount += 1
        }
    }
}

private final class DocumentCapturePreviewControllerSpy: DocumentCapturePreviewDisplay {
    private(set) var callDisplayImageCount = 0
    private(set) var titleText = ""
    private(set) var denialActionText = ""
    private(set) var confirmActionText = ""
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var errorMessage = ""

    func displayPreviewImage(_ image: UIImage) {
        callDisplayImageCount += 1
    }

    func configureBottomView(titleText: String, denialActionText: String, confirmActionText: String) {
        self.titleText = titleText
        self.denialActionText = denialActionText
        self.confirmActionText = confirmActionText
    }

    func startLoading() {
        callStartLoadingCount += 1
    }

    func stopLoading() {
        callStopLoadingCount += 1
    }

    func displayError(_ message: String) {
        errorMessage = message
    }
}

final class DocumentCapturePreviewPresenterTests: XCTestCase {
    private let coordinatorSpy = DocumentCapturePreviewCoordinatorSpy()
    private let controllerSpy = DocumentCapturePreviewControllerSpy()

    private lazy var sut: DocumentCapturePreviewPresenter = {
        let presenter = DocumentCapturePreviewPresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()

    func testPreviewImage_ShouldDisplayImage() {
        let step = IdentityValidationStep(type: .documentFront, title: "", text: "", data: nil)
        sut.previewImage(UIImage(), for: step)

        XCTAssertEqual(controllerSpy.callDisplayImageCount, 1)
    }

    func testPreviewImage_ShouldConfigureButtonTexts() {
        let step = IdentityValidationStep(type: .documentFront, title: "", text: "", data: nil)
        sut.previewImage(UIImage(), for: step)

        XCTAssertEqual(controllerSpy.confirmActionText, FacialBiometricLocalizable.documentSendPicture.text)
        XCTAssertEqual(controllerSpy.denialActionText, FacialBiometricLocalizable.previewTakeAnother.text)
    }

    func testPreviewImage_WhenStepTypeIsDocumentFront_ShouldConfigureCorrectTitle() {
        let step = IdentityValidationStep(type: .documentFront, title: "", text: "", data: nil)
        sut.previewImage(UIImage(), for: step)

        XCTAssertEqual(controllerSpy.titleText, FacialBiometricLocalizable.documentCaptureFrontTitle.text)
    }

    func testPreviewImage_WhenStepTypeIsDocumentBack_ShouldConfigureCorrectTitle() {
        let step = IdentityValidationStep(type: .documentBack, title: "", text: "", data: nil)
        sut.previewImage(UIImage(), for: step)

        XCTAssertEqual(controllerSpy.titleText, FacialBiometricLocalizable.documentCaptureBackTitle.text)
    }

    func testHandleClose_ShouldPerformCloseAction() {
        sut.handleClose()

        XCTAssertEqual(coordinatorSpy.callCloseActionCount, 1)
    }

    func testHandleTakeAnotherPicture_ShouldPerformTakeAnotherPicture() {
        sut.handleTakeAnotherPicture()

        XCTAssertEqual(coordinatorSpy.callTakeAnotherPictureActionCount, 1)
    }

    func testSetLoadingState_WhenLoadingIsTrue_ShouldCallStartLoading() {
        sut.setLoadingState(true)

        XCTAssertEqual(self.controllerSpy.callStartLoadingCount, 1)
    }

    func testSetLoadingState_WhenLoadingIsFalse_ShouldCallStopLoading() {
        sut.setLoadingState(false)

        XCTAssertEqual(self.controllerSpy.callStopLoadingCount, 1)
    }

    func testDisplayError_ShouldDisplayerrorMessage() {
        let message = "Teste erro"
        sut.displayError(message: message)

        XCTAssertEqual(self.controllerSpy.errorMessage, message)
    }

    func testNextStep_ShouldCallCoordinatorNextStep() {
        let step = IdentityValidationStep(type: .done, title: "", text: "", data: nil)
        sut.nextStep(step)

        XCTAssertEqual(self.coordinatorSpy.callNextStepActionCount, 1)
    }
}
