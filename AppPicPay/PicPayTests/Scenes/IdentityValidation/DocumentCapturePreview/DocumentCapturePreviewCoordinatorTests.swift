@testable import PicPay
import XCTest

final class DocumentCapturePreviewCoordinatorTests: XCTestCase {
    private let navController = NavigationControllerMock(rootViewController: UIViewController())

    private lazy var sut = DocumentCapturePreviewCoordinator(navigationController: navController)

    func testPerform_WhenActionIsClose_ShouldDismissNavigation() {
        sut.perform(action: .close)

        XCTAssertTrue(navController.isDismissViewControllerCalled)
    }

    func testPerform_WhenActionIsTakeAnotherPicture_ShouldPopController() {
        sut.perform(action: .takeAnotherPicture)

        XCTAssertTrue(navController.isPopViewControllerCalled)
    }

    func testPerform_WhenActionIsNextStep_ShouldDismissNavigation() {
        sut.perform(action: .nextStep(step: IdentityValidationStep()))

        XCTAssertTrue(navController.isDismissViewControllerCalled)
    }
}
