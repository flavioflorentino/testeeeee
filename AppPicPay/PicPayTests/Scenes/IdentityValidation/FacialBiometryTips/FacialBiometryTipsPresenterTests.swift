@testable import PicPay
import XCTest

private final class FacialBiometryTipsCoordinatorSpy: FacialBiometryTipsCoordinating {
    var viewController: UIViewController?
    private(set) var actionCalled: FacialBiometryTipsAction?
    private(set) var callPerformActionCount = 0
    
    func perform(action: FacialBiometryTipsAction) {
        callPerformActionCount += 1
        actionCalled = action
    }
}

private final class FacialBiometryTipsDisplaySpy: DocumentCaptureTipsDisplay {
    private(set) var displayTipsCallCount = 0
    
    func displayTips(_ tips: ValidationTips) {
        displayTipsCallCount += 1
    }
}

final class FacialBiometryTipsPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = FacialBiometryTipsCoordinatorSpy()
    private lazy var viewControllerSpy = FacialBiometryTipsDisplaySpy()
    
    private lazy var sut: FacialBiometryTipsPresenter = {
        let presenter = FacialBiometryTipsPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testConfigureViews_WhenCalledFromViewModel_ShouldDisplayTipsOnViewController() {
        sut.configureViews()
        
        XCTAssertEqual(viewControllerSpy.displayTipsCallCount, 1)
    }
    
    func testHandleContinue_WhenCalledFromViewModel_ShouldPassActionToCoordinator() {
        sut.handleContinue()
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertNotNil(coordinatorSpy.actionCalled)
    }
    
    func testHandleClose_WhenCalledFromViewModel_ShouldPassActionToCoordinator() {
        sut.handleClose()
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertNotNil(coordinatorSpy.actionCalled)
    }
    
    func testPresentNextStep_WhenReceivedStepFromViewModel_ShouldPassActionToCoordinator() {
        sut.presentNextStep(IdentityValidationStep(type: .documentFront, title: "", text: "", data: nil, status: nil, icons: nil))
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertNotNil(coordinatorSpy.actionCalled)
    }
}
