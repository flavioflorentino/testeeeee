@testable import PicPay
import XCTest

final class FacialBiometryTipsCoordinatorTests: XCTestCase {
    private let coordinatorDelegateSpy = IdentityValidationCoordinatorSpy()
    private let navController = NavigationControllerMock(rootViewController: UIViewController())
    
    private lazy var sut = FacialBiometryTipsCoordinator(coordinatorDelegate: coordinatorDelegateSpy, navigationController: navController)

    func testPerform_WhenActionIsCamera_ShouldPresentViewController() throws {
        sut.perform(action: .camera)

        let presentedNavigation = try XCTUnwrap(navController.viewControllerPresented as? UINavigationController)
        let pushedController = try XCTUnwrap(presentedNavigation.viewControllers.first)
        XCTAssertTrue(pushedController is FacialBiometryCameraViewController)
    }

    func testPerform_WhenActionIsClose_ShouldDismissNavigation() {
        sut.perform(action: .close)

        XCTAssertEqual(coordinatorDelegateSpy.callDismissCount, 1)
    }
    
    func testPerform_WhenActionIsNextStep_ShouldCallPushNextController() {
        sut.perform(action: .next(step: IdentityValidationStep()))

        XCTAssertEqual(coordinatorDelegateSpy.callPushNextControllerCount, 1)
    }
}
