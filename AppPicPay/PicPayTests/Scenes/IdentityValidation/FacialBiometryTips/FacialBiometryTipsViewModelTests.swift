import AnalyticsModule
@testable import PicPay
import XCTest

private final class FacialBiometryTipsPresenterSpy: FacialBiometryTipsPresenting {
    var viewController: DocumentCaptureTipsDisplay?
    private(set) var configureViewsCallCount = 0
    private(set) var handleContinueCallCount = 0
    private(set) var handleCloseCallCount = 0
    private(set) var presentNextStepCallCount = 0
    private(set) var presentedNextStep: IdentityValidationStep?
    
    func configureViews() {
        configureViewsCallCount += 1
    }
    
    func handleContinue() {
        handleContinueCallCount += 1
    }
    
    func handleClose() {
        handleCloseCallCount += 1
    }
    
    func presentNextStep(_ step: IdentityValidationStep) {
        presentNextStepCallCount += 1
        presentedNextStep = step
    }
}

final class FacialBiometryTipsViewModelTests: XCTestCase {
    private lazy var presenterSpy = FacialBiometryTipsPresenterSpy()
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var mockDependencies = DependencyContainerMock(analyticsSpy)
    
    private lazy var sut: FacialBiometryTipsViewModel = {
        let viewModel = FacialBiometryTipsViewModel(presenter: presenterSpy, dependencies: mockDependencies)
        return viewModel
    }()
    
    func testConfigureViews_WhenViewHasLoaded_ShouldCallConfigureViewsOnPresenter() {
        sut.configureViews()
        
        XCTAssertEqual(presenterSpy.configureViewsCallCount, 1)
        
        let expectedEvent = IdentityValidationEvent.selfieTips(action: .none).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testHandleClose_WhenUserTapOnCloseButton_ShouldCallHandleCloseOnPresenter() {
        sut.handleClose()
        
        XCTAssertEqual(presenterSpy.handleCloseCallCount, 1)
    }
    
    func testHandleContinue_WhenUserTapOnStartButton_ShouldCallHandleContinueOnPresenter() {
        sut.handleContinue()
        
        XCTAssertEqual(presenterSpy.handleContinueCallCount, 1)
    }
    
    func testDidSendSelfie_WhenReceiveNextStep_ShouldPresentNextStep() {
        let notificationObject = IdentityValidationStep(
            type: .documentFront,
            title: "Documento",
            text: "Frente do documento",
            data: nil
        )
        
        let notification = Notification(name: .didSendSelfie, object: notificationObject, userInfo: nil)
        sut.didSendSelfie(notification)
        
        XCTAssertEqual(presenterSpy.presentNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.presentedNextStep?.type, .documentFront)
    }
}
