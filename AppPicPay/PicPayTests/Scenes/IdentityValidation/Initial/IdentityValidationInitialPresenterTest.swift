@testable import PicPay
import UI
import XCTest

final class IdentityValidationInitialPresenterTest: XCTestCase {
    private let mockDependencies = DependencyContainerMock()
    private let controllerSpy = IdentityValidationInitialControllerSpy()
    private let coordinatorSpy = IdentityValidationInitialCoordinatorSpy()
    
    private lazy var sut: IdentityValidationInitialPresenter = {
        let sut = IdentityValidationInitialPresenter(coordinator: coordinatorSpy, dependencies: mockDependencies)
        sut.viewController = controllerSpy
        return sut
    }()
    
    func testLoadStepInfo_ShouldCallViewController() {
        let step = IdentityValidationStep(type: .initial, title: "a random title", text: "a random text")
        sut.loadStepInfo(step)

        XCTAssertTrue(controllerSpy.didCallPresentStepInfo)
        XCTAssertEqual(controllerSpy.stepInfo?.title, step.title)
        XCTAssertEqual(controllerSpy.stepInfo?.description, step.text)
        XCTAssertEqual(controllerSpy.stepInfo?.terms.string, generateTermsString().string)
    }

    func testPresentTermsOfUse_ShouldCallCoordinatorPresentTerms() {
        sut.presentTermsOfUse(URL(fileURLWithPath: ""))

        XCTAssertEqual(coordinatorSpy.callPresentTermsCount, 1)
    }
    
    func testHandleNextStep_ShouldCallViewController() {
        let step = IdentityValidationStep(type: .initial, title: "a random title", text: "a random text")
        sut.handleNextStep(step)

        mockDependencies.mainQueue.async {
            XCTAssertTrue(self.controllerSpy.didCallStopLoading)
        }
    }

    func testHandleNextStep_ShouldCallCoordinatorNextController() {
        let step = IdentityValidationStep()
        sut.handleNextStep(step)

        mockDependencies.mainQueue.async {
            XCTAssertEqual(self.coordinatorSpy.callNextViewControllerCount, 1)
        }
    }
    
    func testLoadStep_ShouldReturnAnError() {
        let error = PicPayError(message: "a random error")
        sut.handleError(error)
        
        let expectation = self.expectation(description: "LoadStepShouldReturnAnError")

        mockDependencies.mainQueue.async {
            XCTAssertTrue(self.controllerSpy.didCallPresentAlert)
            XCTAssertEqual(self.controllerSpy.error, error)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5)
    }

    func testHandleCloseFlow_ShouldCallCoordinatorDismiss() {
        sut.handleCloseFlow()

        XCTAssertEqual(coordinatorSpy.callDismissCount, 1)
    }
}

private extension IdentityValidationInitialPresenterTest {
    func generateTermsString() -> NSMutableAttributedString {
        let attributedText = NSMutableAttributedString(string: IdentityValidationLocalizable.termsOfUseFullMessage.text)
        let attributedRange = (attributedText.string as NSString).range(of: IdentityValidationLocalizable.termsOfUse.text)

        attributedText.addAttributes(
            [
                .foregroundColor: Colors.branding400.color,
                .underlineStyle: NSUnderlineStyle.single.rawValue
            ],
            range: attributedRange
        )
        return attributedText
    }
}

final private class IdentityValidationInitialControllerSpy: IdentityValidationInitialDisplayable {
    private(set) var didCallPresentStepInfo = false
    private(set) var stepInfo: (title: String, description: String, terms: NSAttributedString)?
    private(set) var didCallPresentAlert = false
    private(set) var error: PicPayError?
    private(set) var didCallStopLoading = false

    func presentStepInfo(title: String, description: String, terms: NSAttributedString) {
        didCallPresentStepInfo.toggle()
        self.stepInfo = (title: title, description: description, terms: terms)
    }
    
    func presentAlert(with error: PicPayError) {
        didCallPresentAlert.toggle()
        self.error = error
    }
    
    func stopLoading() {
        didCallStopLoading.toggle()
    }
}

final private class IdentityValidationInitialCoordinatorSpy: IdentityValidationInitialCoordinating {
    private(set) var callPresentTermsCount = 0
    private(set) var callNextViewControllerCount = 0
    private(set) var callDismissCount = 0

    func presentTerms(with url: URL) {
        callPresentTermsCount += 1
    }

    func nextViewController(for step: IdentityValidationStep) {
        callNextViewControllerCount += 1
    }

    func dismiss() {
        callDismissCount += 1
    }
}
