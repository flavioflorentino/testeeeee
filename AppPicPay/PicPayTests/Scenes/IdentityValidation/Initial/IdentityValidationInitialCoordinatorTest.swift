import XCTest
@testable import PicPay

final class IdentityValidationInitialCoordinatorTest: XCTestCase {
    private let coordinatorDelegateSpy = IdentityValidationCoordinatorSpy()
    private let navController = NavigationControllerMock(rootViewController: UIViewController())
    
    private lazy var sut = IdentityValidationInitialCoordinator(coordinatorDelegate: coordinatorDelegateSpy, navigationController: navController)

    func testPresentTerms_ShouldPushViewController() throws {
        let url = try XCTUnwrap(URL(string: "https://google.com"))
        sut.presentTerms(with: url)

        XCTAssertTrue(navController.isPushViewControllerCalled)
    }
    
    func testNextViewController_ShouldCallPushNextController() {
        sut.nextViewController(for: IdentityValidationStep())

        XCTAssertEqual(coordinatorDelegateSpy.callPushNextControllerCount, 1)
    }

    func testDismiss_ShouldCallDismiss() {
        sut.dismiss()

        XCTAssertEqual(coordinatorDelegateSpy.callDismissCount, 1)
    }
}
