import XCTest
@testable import PicPay

final class IdentityValidationInitialViewModelTest: XCTestCase {
    private let serviceMock = IdentityValidationInitialServiceMock()
    private let presenterSpy = IdentityValidationInitialPresenterSpy()
    private let step = IdentityValidationStep(type: .initial, title: "random Title", text: "random text")
    private lazy var sut = IdentityValidationInitialViewModel(.settings,
                                                              step: step,
                                                              service: serviceMock,
                                                              presenter: presenterSpy)
    
    func testLoadStepShouldReturnAStep() {
        sut.loadStepInfo()
        
        XCTAssertTrue(presenterSpy.didCallLoadStepInfo)
        XCTAssertEqual(presenterSpy.stepInfo?.type, .initial)
        XCTAssertEqual(presenterSpy.stepInfo?.title, "random Title")
        XCTAssertEqual(presenterSpy.stepInfo?.text, "random text")
    }

    func testReadTerms_ShouldPresentTermsOfUse() {
        sut.readTerms()

        XCTAssertEqual(presenterSpy.callPresentTermsOfUseCount, 1)
    }
    
    func testStartValidationShouldReturnAStep() {
        serviceMock.isSuccess = true

        sut.startValidation()
    
        XCTAssertTrue(presenterSpy.didCallHandleNextStep)
        XCTAssertEqual(presenterSpy.step?.type, .initial)
        XCTAssertEqual(presenterSpy.step?.title, "random Title")
        XCTAssertEqual(presenterSpy.step?.text, "random text")
    }
    
    func testStartValidationShouldReturnAnError() {
        serviceMock.isSuccess = false

        sut.startValidation()
    
        XCTAssertTrue(presenterSpy.didCallHandleError)
        XCTAssertEqual(presenterSpy.error?.message, "random error")
    }
    
    func testCloseFlowShouldCallPresenter() {
           sut.closeFlow()
       
           XCTAssertTrue(presenterSpy.didCallHandleCloseFlow)
       }
}

final private class IdentityValidationInitialServiceMock: IdentityValidationInitialServicing {
    var isSuccess = false
    
    func startValidation(originFlow: IdentityValidationOriginFlow, _ completion: @escaping ((PicPayResult<IdentityValidationStep>) -> Void)) {
        if isSuccess {
            let step = IdentityValidationStep(type: .initial, title: "random Title", text: "random text")
            completion(PicPayResult.success(step)); return
        }
        
        let error = PicPayError(message: "random error")
        completion(PicPayResult.failure(error))
    }
}

final private class IdentityValidationInitialPresenterSpy: IdentityValidationInitialPresenting {
    var coordinator: IdentityValidationInitialCoordinating?
    var viewController: IdentityValidationInitialDisplayable?
    
    private(set) var didCallLoadStepInfo = false
    private(set) var callPresentTermsOfUseCount = 0
    private(set) var stepInfo: IdentityValidationStep?
    private(set) var didCallHandleNextStep = false
    private(set) var step: IdentityValidationStep?
    private(set) var didCallHandleError = false
    private(set) var error: PicPayError?
    private(set) var didCallHandleCloseFlow = false
    
    func loadStepInfo(_ step: IdentityValidationStep) {
        didCallLoadStepInfo.toggle()
        self.stepInfo = step
    }

    func presentTermsOfUse(_ url: URL) {
        callPresentTermsOfUseCount += 1
    }
    
    func handleNextStep(_ step: IdentityValidationStep) {
        didCallHandleNextStep.toggle()
        self.step = step
    }
    
    func handleError(_ error: PicPayError) {
        didCallHandleError.toggle()
        self.error = error
    }
    
    func handleCloseFlow() {
        didCallHandleCloseFlow.toggle()
    }
}
