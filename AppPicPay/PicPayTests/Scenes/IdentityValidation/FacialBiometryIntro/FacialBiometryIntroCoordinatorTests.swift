@testable import PicPay
import XCTest

final class FacialBiometryIntroCoordinatorTests: XCTestCase {
    private let coordinatorDelegateSpy = IdentityValidationCoordinatorSpy()
    private let navController = NavigationControllerMock(rootViewController: UIViewController())
    
    private lazy var sut = FacialBiometryIntroCoordinator(coordinatorDelegate: coordinatorDelegateSpy, navigationController: navController)
    
    func testPerformCloseAction_ShouldDismissController() {
        sut.perform(action: .close)
       
        XCTAssertEqual(coordinatorDelegateSpy.callDismissCount, 1)
    }

    func testPerformTipsAction_WhenCalledFromPresenter_ShouldPushViewController() {
        sut.perform(action: .tips)
        
        XCTAssertTrue(navController.isPushViewControllerCalled)
    }
}
