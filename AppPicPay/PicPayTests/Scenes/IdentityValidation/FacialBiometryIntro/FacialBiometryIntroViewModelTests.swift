import AnalyticsModule
@testable import PicPay
import XCTest

private final class FacialBiometryIntroPresenterSpy: FacialBiometryIntroPresenting {
    private(set) var presentTipsCallCount = 0
    private(set) var handleCloseCallCount = 0
    
    func handleClose() {
        handleCloseCallCount += 1
    }
    
    func presentTips() {
        presentTipsCallCount += 1
    }
}

final class FacialBiometryIntroViewModelTests: XCTestCase {
    private lazy var spy = FacialBiometryIntroPresenterSpy()
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var mockDependencies = DependencyContainerMock(analyticsSpy)
    
    private lazy var sut = FacialBiometryIntroViewModel(presenter: spy, dependencies: mockDependencies)
    
    func testTakeSelfie_WhenUserTapOnTakeSelfieButton_ShouldPresentTips() {
        sut.takeSelfie()
        
        XCTAssertEqual(spy.presentTipsCallCount, 1)
        
        let expectedEvent = IdentityValidationEvent.selfie(action: .started).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testTakeSelfieLater_WhenUserTapOnTakeSelfieLaterButton_ShouldCloseBiometricValidationFlow() {
        sut.takeSelfieLater()
        
        XCTAssertEqual(spy.handleCloseCallCount, 1)
        
        let expectedEvent = IdentityValidationEvent.selfie(action: .drop).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testTrackSelfieStepPresentation_WhenScreenIsPresented_ShouldLogEvent() {
        sut.trackSelfieStepPresentation()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        
        let expectedEvent = IdentityValidationEvent.selfie(action: .none).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
}
