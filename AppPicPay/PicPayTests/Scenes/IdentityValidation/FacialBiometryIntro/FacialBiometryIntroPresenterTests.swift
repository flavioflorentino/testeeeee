@testable import PicPay
import XCTest

private final class FacialBiometryIntroCoordinatorSpy: FacialBiometryIntroCoordinating {
    var viewController: UIViewController?
    private(set) var actionCalled: FacialBiometryIntroAction?
    private(set) var callPerformActionCount = 0
    func perform(action: FacialBiometryIntroAction) {
        callPerformActionCount += 1
        actionCalled = action
    }
}

final class FacialBiometryIntroPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = FacialBiometryIntroCoordinatorSpy()

    private lazy var sut = FacialBiometryIntroPresenter(coordinator: coordinatorSpy)
    
    func testPresentCamera_WhenCalledFromViewModel_ShouldPassActionToCoordinator() {
        sut.presentTips()
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertNotNil(coordinatorSpy.actionCalled)
    }
    
    func testHandleClose_WhenCalledFromViewModel_ShouldPassActionToCoordinator() {
        sut.handleClose()
        
        XCTAssertEqual(coordinatorSpy.callPerformActionCount, 1)
        XCTAssertNotNil(coordinatorSpy.actionCalled)
    }
}
