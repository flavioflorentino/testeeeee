@testable import PicPay
import XCTest

final class IdentityValidationRouterTests: XCTestCase {
    private lazy var sut = IdentityValidationRouter()

    func testGetController_WhenStepIsInitial_ShouldReturnInitialController() throws {
        let model = createModel(with: IdentityValidationStep(type: .initial))
        let controller = sut.getController(for: model)

        let initialController = try XCTUnwrap(controller)
        XCTAssertTrue(initialController is IdentityValidationInitialViewController)
    }

    func testGetController_WhenStepIsQuestions_WhenDataIsNotIdentityValidationQuestions_ShouldReturnNil() {
        let model = createModel(with: IdentityValidationStep(type: .questions))
        let controller = sut.getController(for: model)

        XCTAssertNil(controller)
    }

    func testGetController_WhenStepIsQuestions_WhenIdentityValidationQuestionsIsEmpty_ShouldReturnNil() {
        let model = createModel(with: IdentityValidationStep(type: .questions, data: [IdentityValidationQuestion]()))
        let controller = sut.getController(for: model)

        XCTAssertNil(controller)
    }

    func testGetController_WhenStepIsQuestions_ShouldReturnquestionsController() throws {
        let questions = [IdentityValidationQuestion(text: "", answer: "", options: [])]
        let model = createModel(with: IdentityValidationStep(type: .questions, data: questions))
        let controller = sut.getController(for: model)

        let questionsController = try XCTUnwrap(controller)
        XCTAssertTrue(questionsController is IdentityValidationQuestionsViewController)
    }

    func testGetController_WhenStepIsSelfie_WhenFlagIsTrue_ShouldReturnSelfieController() throws {
        let model = createModel(with: IdentityValidationStep(type: .selfie), identityFlag: true)
        let controller = sut.getController(for: model)

        let selfieController = try XCTUnwrap(controller)
        XCTAssertTrue(selfieController is FacialBiometryIntroViewController)
    }

    func testGetController_WhenStepIsSelfie_WhenFlagIsFalse_ShouldReturnPhotoController() throws {
        let model = createModel(with: IdentityValidationStep(type: .selfie))
        let controller = sut.getController(for: model)

        let photoController = try XCTUnwrap(controller)
        XCTAssertTrue(photoController is IdentityValidationPhotoViewController)
    }

    func testGetController_WhenStepIsDocumentFront_WhenFlagIsTrue_ShouldReturnDocumentController() throws {
        let model = createModel(with: IdentityValidationStep(type: .documentFront), identityFlag: true)
        let controller = sut.getController(for: model)

        let documentController = try XCTUnwrap(controller)
        XCTAssertTrue(documentController is DocumentCaptureIntroViewController)
    }

    func testGetController_WhenStepIsDocumentFront_WhenFlagIsFalse_ShouldReturnPhotoController() throws {
        let model = createModel(with: IdentityValidationStep(type: .documentFront))
        let controller = sut.getController(for: model)

        let photoController = try XCTUnwrap(controller)
        XCTAssertTrue(photoController is IdentityValidationPhotoViewController)
    }

    func testGetController_WhenStepIsDocumentBack_WhenFlagIsTrue_ShouldReturnDocumentController() throws {
        let model = createModel(with: IdentityValidationStep(type: .documentBack), identityFlag: true)
        let controller = sut.getController(for: model)

        let documentController = try XCTUnwrap(controller)
        XCTAssertTrue(documentController is DocumentCaptureIntroViewController)
    }

    func testGetController_WhenStepIsDocumentBack_WhenFlagIsFalse_ShouldReturnPhotoController() throws {
        let model = createModel(with: IdentityValidationStep(type: .documentBack))
        let controller = sut.getController(for: model)

        let photoController = try XCTUnwrap(controller)
        XCTAssertTrue(photoController is IdentityValidationPhotoViewController)
    }

    func testGetController_WhenStepIsProcessing_WhenFlagIsTrue_ShouldReturnStatusController() throws {
        let model = createModel(with: IdentityValidationStep(type: .processing), identityFlag: true)
        let controller = sut.getController(for: model)

        let statusController = try XCTUnwrap(controller)
        XCTAssertTrue(statusController is ValidationStatusViewController)
    }

    func testGetController_WhenStepIsProcessing_WhenFlagIsFalse_ShouldReturnFinishedController() throws {
        let model = createModel(with: IdentityValidationStep(type: .processing))
        let controller = sut.getController(for: model)

        let finishedController = try XCTUnwrap(controller)
        XCTAssertTrue(finishedController is IdentityValidationFinishedViewController)
    }

    func testGetController_WhenStepIsDone_WhenFlagIsTrue_ShouldReturnStatusController() throws {
        let model = createModel(with: IdentityValidationStep(type: .done), identityFlag: true)
        let controller = sut.getController(for: model)

        let statusController = try XCTUnwrap(controller)
        XCTAssertTrue(statusController is ValidationStatusViewController)
    }

    func testGetController_WhenStepIsDone_WhenFlagIsFalse_ShouldReturnFinishedController() throws {
        let model = createModel(with: IdentityValidationStep(type: .done))
        let controller = sut.getController(for: model)

        let finishedController = try XCTUnwrap(controller)
        XCTAssertTrue(finishedController is IdentityValidationFinishedViewController)
    }
}

private extension IdentityValidationRouterTests {
    func createModel(
        with step: IdentityValidationStep,
        identityFlag: Bool = false
    ) -> IdentityValidationRouterModel {
        return IdentityValidationRouterModel(
            step: step,
            coordinator: nil,
            originFlow: .settings,
            navigation: UINavigationController(),
            identityFlag: identityFlag
        )
    }
}
