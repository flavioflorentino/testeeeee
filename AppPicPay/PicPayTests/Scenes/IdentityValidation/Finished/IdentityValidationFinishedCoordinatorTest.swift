import XCTest
@testable import PicPay

final class IdentityValidationFinishedCoordinatorTest: XCTestCase {
    private let coordinatorDelegateSpy = IdentityValidationCoordinatorSpy()

    private lazy var sut = IdentityValidationFinishedCoordinator(coordinatorDelegate: coordinatorDelegateSpy)
    
    func testFinish_ShouldCallCoordinatorDismiss() {
        sut.finish()

        XCTAssertEqual(coordinatorDelegateSpy.callDismissCount, 1)
    }
}
