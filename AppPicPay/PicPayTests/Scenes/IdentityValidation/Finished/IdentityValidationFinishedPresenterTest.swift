import XCTest
@testable import PicPay

final class IdentityValidationFinishedPresenterTest: XCTestCase {
    private let coordinatorSpy = IdentityValidationFinishedCoordinatorSpy()
    private let controllerSpy = IdentityValidationFinishedControllerSpy()
    private lazy var sut: IdentityValidationFinishedPresenter = {
        let sut = IdentityValidationFinishedPresenter(coordinator: coordinatorSpy)
        sut.viewController = controllerSpy
        return sut
    }()
    
    func testHandleStepInfoShouldCallViewController() {
        let step = IdentityValidationStep(type: .done, title: "a random title", text: "a random text")
        sut.handleStepInfo(step: step)
    
        XCTAssertTrue(controllerSpy.didCallHandleStepInfo)
        XCTAssertEqual(controllerSpy.stepInfo?.title, step.title)
        XCTAssertEqual(controllerSpy.stepInfo?.description, step.text)
    }
    
    func testHandleCloseFlowShouldCallCoordinatorFinish() {
        sut.handleCloseFlow()
        XCTAssertTrue(coordinatorSpy.didCallFinish)
    }
}

final private class IdentityValidationFinishedCoordinatorSpy: IdentityValidationFinishedCoordinating {
    var viewController: UIViewController? = nil
    var navigationController: UINavigationController = UINavigationController()
    
    private(set) var didCallFinish = false
    
    func finish() {
        didCallFinish.toggle()
    }
}

final private class IdentityValidationFinishedControllerSpy: IdentityValidationFinishedDisplayable {
    private(set) var didCallHandleStepInfo = false
    private(set) var stepInfo: (title: String, description: String)?
    private(set) var didCallStopLoading = false

    func presentStepInfo(image: UIImage, title: String, description: String) {
        didCallHandleStepInfo.toggle()
        self.stepInfo = (title: title, description: description)
    }
}
