import XCTest
@testable import PicPay

final class IdentityValidationFinishedViewModelTest: XCTestCase {
    private let presenterSpy = IdentityValidationFinishedPresenterSpy()
    private let step = IdentityValidationStep(type: .done, title: "random Title", text: "random text")
    private lazy var sut = IdentityValidationFinishedViewModel(step: step,
                                                               presenter: presenterSpy)
    
    func testLoadStepInfoShouldReturnAStep() {
        sut.loadStepInfo()
        
        XCTAssertTrue(presenterSpy.didCallHandleStepInfo)
        XCTAssertEqual(presenterSpy.stepInfo?.type, .done)
        XCTAssertEqual(presenterSpy.stepInfo?.title, "random Title")
        XCTAssertEqual(presenterSpy.stepInfo?.text, "random text")
    }
    
    func testCloseFlowShouldCallPresenter() {
           sut.closeFlow()
       
           XCTAssertTrue(presenterSpy.didCallHandleCloseFlow)
       }
}

final private class IdentityValidationFinishedPresenterSpy: IdentityValidationFinishedPresenting {
    var coordinator: IdentityValidationFinishedCoordinating?
    var viewController: IdentityValidationFinishedDisplayable?
    
    private(set) var didCallHandleStepInfo = false
    private(set) var stepInfo: IdentityValidationStep?
    private(set) var didCallHandleCloseFlow = false
    
    func handleStepInfo(step: IdentityValidationStep) {
        didCallHandleStepInfo.toggle()
        self.stepInfo = step
    }
    
    func handleCloseFlow() {
        didCallHandleCloseFlow.toggle()
    }
}
