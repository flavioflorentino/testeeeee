import AnalyticsModule
@testable import PicPay
import XCTest

private final class ValidationStatusPresenterSpy: ValidationStatusPresenting {
    var viewController: ValidationStatusDisplay?
    private(set) var step: IdentityValidationStep?
    private(set) var isDarkMode: Bool?
    private(set) var callConfigureViewsCount = 0
    private(set) var callHandleOkActionCount = 0
    
    func configureViews(for step: IdentityValidationStep, isDarkMode: Bool) {
        self.step = step
        callConfigureViewsCount += 1
        self.isDarkMode = isDarkMode
    }
    
    func handleOkAction() {
        callHandleOkActionCount += 1
    }
}

final class ValidationStatusViewModelTests: XCTestCase {
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var mockDependencies = DependencyContainerMock(analyticsSpy)
    
    private let doneStep = IdentityValidationStep(
        type: .done,
        title: "TITLE",
        text: "TEXT",
        data: nil,
        status: .verified
    )
    
    private let processingStep = IdentityValidationStep(
        type: .processing,
        title: "TITLE",
        text: "TEXT",
        data: nil,
        status: .toVerify
    )
    
    private let presenterSpy = ValidationStatusPresenterSpy()
    private lazy var sut = ValidationStatusViewModel(presenter: presenterSpy, step: doneStep, dependencies: mockDependencies)
    
    func testConfigureViews_WhenCalledFromViewController_ShouldCallPresenterConfigureViewsForStep() {
        sut.configureViews(isDarkMode: false)
        
        XCTAssertEqual(presenterSpy.callConfigureViewsCount, 1)
        XCTAssertEqual(presenterSpy.step, doneStep)
        XCTAssertFalse(presenterSpy.isDarkMode ?? true)
    }
    
    func testConfigureViews_WhenStepIsDoneAndStatusIsVerified_ShouldLogAnalyticsEventValidationSuccess() {
        sut.sendAnalytics()
        
        let expectedEvent = IdentityValidationEvent.validationEnd(status: .success).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testConfigureViews_WhenStepIsDoneAndStatusIsRejected_ShouldLogAnalyticsEventValidationReproved() {
        doneStep.status = .rejected
        sut = ValidationStatusViewModel(presenter: presenterSpy, step: doneStep, dependencies: mockDependencies)
        
        sut.sendAnalytics()
        
        let expectedEvent = IdentityValidationEvent.validationEnd(status: .reproved).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testConfigureViews_WhenStepIsProccessing_ShouldLogAnalyticsEventValidationAnalysis() {
        sut = ValidationStatusViewModel(presenter: presenterSpy, step: processingStep, dependencies: mockDependencies)
        sut.sendAnalytics()
        
        let expectedEvent = IdentityValidationEvent.validationEnd(status: .analysis).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testHandleOkAction_WhenCalledFromViewController_ShouldCallPresenterHandleOkAction() {
        sut.handleOkAction()
        
        XCTAssertEqual(presenterSpy.callHandleOkActionCount, 1)
    }
}
