@testable import PicPay
import XCTest

class ValidationStatusCoordinatorTests: XCTestCase {
    private let coordinatorDelegateSpy = IdentityValidationCoordinatorSpy()
    
    private lazy var sut = ValidationStatusCoordinator(coordinatorDelegate: coordinatorDelegateSpy)

    func testPerformAction_WhenActionIsOk_ShouldDismissNavigationController() {
        sut.perform(action: .ok)
        
        XCTAssertEqual(coordinatorDelegateSpy.callDismissCount, 1)
    }
}
