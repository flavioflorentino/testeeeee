@testable import PicPay
import XCTest

private final class ValidationStatusCoordinatorSpy: ValidationStatusCoordinating {
    var viewController: UIViewController?
    private(set) var callOkActionCount = 0
    
    func perform(action: ValidationStatusAction) {
        if case .ok = action {
            callOkActionCount += 1
        }
    }
}

private final class ValidationStatusControllerSpy: ValidationStatusDisplay {
    private(set) var titleString = ""
    private(set) var descriptionString = ""
    private(set) var imageUrlString = ""
    
    func displayStatusWith(imageUrl: URL?, title: String, description: String) {
        titleString = title
        descriptionString = description
        imageUrlString = imageUrl?.absoluteString ?? ""
    }
}

class ValidationStatusPresenterTests: XCTestCase {
    private let coordinatorSpy = ValidationStatusCoordinatorSpy()
    private let viewControllerSpy = ValidationStatusControllerSpy()

    private let doneStep = IdentityValidationStep(
        type: .done,
        title: "DONE TITLE",
        text: "DONE TEXT",
        data: nil,
        status: .verified,
        icons: IdentityValidationStepIcons(darkMode: "darkModeUrl", lightMode: "dayModeUrl")
    )
    
    private let processingStep = IdentityValidationStep(
        type: .processing,
        title: "PROCESSING TITLE",
        text: "PROCESSING TEXT",
        data: nil,
        status: .toVerify,
        icons: IdentityValidationStepIcons(darkMode: "darkModeUrl", lightMode: "dayModeUrl")
    )
    
    private lazy var sut: ValidationStatusPresenter = {
        let presenter = ValidationStatusPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testConfigureViews_WhenStepIsDone_ShouldDisplayValidationSuccess() {
        sut.configureViews(for: doneStep, isDarkMode: false)
        
        XCTAssertEqual(viewControllerSpy.titleString, doneStep.title)
        XCTAssertEqual(viewControllerSpy.descriptionString, doneStep.text)
        XCTAssertEqual(viewControllerSpy.imageUrlString, "dayModeUrl")
    }
    func testConfigureViews_WhenStepIsProcessing_ShouldDisplayValidationInAnalysis() {
        sut.configureViews(for: processingStep, isDarkMode: false)
        
        XCTAssertEqual(viewControllerSpy.titleString, processingStep.title)
        XCTAssertEqual(viewControllerSpy.descriptionString, processingStep.text)
        XCTAssertEqual(viewControllerSpy.imageUrlString, "dayModeUrl")
    }
    
    func testConfigureViews_WhenUserInterfaceIsDark_ShouldDisplayImageUrlForDarkMode() {
        sut.configureViews(for: doneStep, isDarkMode: true)
        
        XCTAssertEqual(viewControllerSpy.titleString, doneStep.title)
        XCTAssertEqual(viewControllerSpy.descriptionString, doneStep.text)
        XCTAssertEqual(viewControllerSpy.imageUrlString, "darkModeUrl")
    }
    
    func testConfigureViews_WhenLightModeOnAndIconsAreNil_ShouldDisplayEmptyImageUrlString() {
        processingStep.icons = nil
        sut.configureViews(for: processingStep, isDarkMode: false)
        
        XCTAssertEqual(viewControllerSpy.titleString, processingStep.title)
        XCTAssertEqual(viewControllerSpy.descriptionString, processingStep.text)
        XCTAssertEqual(viewControllerSpy.imageUrlString, "")
    }
    
    func testConfigureViews_WhenDarkModeOnAndIconsAreNil_ShouldDisplayEmptyImageUrlString() {
        processingStep.icons = nil
        sut.configureViews(for: processingStep, isDarkMode: true)
        
        XCTAssertEqual(viewControllerSpy.titleString, processingStep.title)
        XCTAssertEqual(viewControllerSpy.descriptionString, processingStep.text)
        XCTAssertEqual(viewControllerSpy.imageUrlString, "")
    }
    
    func testHandleOkAction_WhenCalledFromPresenter_ShouldCallCoordinatorToPerformAction() {
        sut.handleOkAction()
        
        XCTAssertEqual(coordinatorSpy.callOkActionCount, 1)
    }
}
