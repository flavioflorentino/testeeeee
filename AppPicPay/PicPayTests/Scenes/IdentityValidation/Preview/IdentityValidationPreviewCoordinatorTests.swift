import XCTest
@testable import PicPay

final class IdentityValidationPreviewCoordinatorTests: XCTestCase {
    private let navController = NavigationControllerMock(rootViewController: UIViewController())

    private lazy var sut = IdentityValidationPreviewCoordinator(navigationController: navController)

    func testPerform_WhenActionIsUsePhoto_ShouldDismissNavigation() {
        sut.perform(action: .usePhoto(UIImage()))

        XCTAssertTrue(navController.isDismissViewControllerCalled)
    }

    func testPerform_WhenActionIsDiscardPhoto_ShouldPopController() {
        sut.perform(action: .discardPhoto)

        XCTAssertTrue(navController.isPopViewControllerCalled)
    }

    func testPerform_WhenActionIsClose_ShouldPopController() {
        sut.perform(action: .close)

        XCTAssertTrue(navController.isDismissViewControllerCalled)
    }
}
