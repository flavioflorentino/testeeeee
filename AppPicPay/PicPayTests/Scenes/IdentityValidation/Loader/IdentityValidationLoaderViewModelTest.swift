import XCTest
@testable import PicPay

final class IdentityValidationLoaderViewModelTest: XCTestCase {
    private let serviceMock = IdentityValidationLoaderServiceMock()
    private let presenterSpy = IdentityValidationLoaderPresenterSpy()
    private lazy var sut = IdentityValidationLoaderViewModel(service: serviceMock, presenter: presenterSpy)
    
    func testLoadStepShouldReturnAStep() {
        serviceMock.isSuccess = true

        sut.loadStep()
    
        XCTAssertTrue(presenterSpy.didCallHandleNextStep)
        XCTAssertEqual(presenterSpy.step?.type, .initial)
        XCTAssertEqual(presenterSpy.step?.title, "a title")
        XCTAssertEqual(presenterSpy.step?.text, "a random text")
    }
    
    func testLoadStepShouldReturnAnError() {
        serviceMock.isSuccess = false

        sut.loadStep()
    
        XCTAssertTrue(presenterSpy.didCallHandleError)
        XCTAssertEqual(presenterSpy.error?.message, "a random error")
    }
    
    func testCloseFlowShouldCallPresenter() {
        sut.closeFlow()
    
        XCTAssertTrue(presenterSpy.didCallHandleCloseFlow)
    }
}

final private class IdentityValidationLoaderServiceMock: IdentityValidationLoaderServicing {
    var isSuccess = false
    
    func step(_ completion: @escaping ((PicPayResult<IdentityValidationStep>) -> Void)) {
        if isSuccess {
            let step = IdentityValidationStep(type: .initial, title: "a title", text: "a random text")
            completion(PicPayResult.success(step)); return
        }
        
        let error = PicPayError(message: "a random error")
        completion(PicPayResult.failure(error))
    }
}

final private class IdentityValidationLoaderPresenterSpy: IdentityValidationLoaderPresenting {
    var coordinator: IdentityValidationLoaderCoordinating?
    var viewController: IdentityValidationLoaderDisplayable?
    
    private(set) var didCallHandleNextStep = false
    private(set) var step: IdentityValidationStep?
    private(set) var didCallHandleError = false
    private(set) var error: PicPayError?
    private(set) var didCallHandleCloseFlow = false
    
    func handleNextStep(_ step: IdentityValidationStep) {
        didCallHandleNextStep.toggle()
        self.step = step
    }
    
    func handleError(_ error: PicPayError) {
        didCallHandleError.toggle()
        self.error = error
    }
    
    func handleCloseFlow() {
        didCallHandleCloseFlow.toggle()
    }
}
