import XCTest
@testable import PicPay

final class IdentityValidationLoaderPresenterTest: XCTestCase {
    private let mockDependencies = DependencyContainerMock()
    private let controllerSpy = IdentityValidationLoaderControllerSpy()
    private let coordinatorSpy = IdentityValidationLoaderCoordinatorSpy()
    
    private lazy var sut: IdentityValidationLoaderPresenter = {
        let sut = IdentityValidationLoaderPresenter(coordinator: coordinatorSpy, dependencies: mockDependencies)
        sut.viewController = controllerSpy
        return sut
    }()

    func testHandleNextStep_ShouldCallCoordinatorNextController() {
        sut.handleNextStep(IdentityValidationStep())

        mockDependencies.mainQueue.async {
            XCTAssertEqual(self.coordinatorSpy.callNextViewControllerCount, 1)
        }
    }
    
    func testLoadStep_ShouldReturnAnError() {
        let error = PicPayError(message: "a random error")
        sut.handleError(error)
        
        let expectation = self.expectation(description: "LoadStepShouldReturnAnError")

        mockDependencies.mainQueue.async {
            XCTAssertTrue(self.controllerSpy.didCallPresentAlert)
            XCTAssertEqual(self.controllerSpy.error, error)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5)
    }

    func testHandleCloseFlow_ShouldCallCoordinatorDismiss() {
        sut.handleCloseFlow()

        XCTAssertEqual(coordinatorSpy.callDismissCount, 1)
    }
}

final private class IdentityValidationLoaderControllerSpy: IdentityValidationLoaderDisplayable {
    private(set) var didCallPresentAlert = false
    private(set) var error: PicPayError?
    
    func presentAlert(with error: PicPayError) {
        didCallPresentAlert.toggle()
        self.error = error
    }
}

final private class IdentityValidationLoaderCoordinatorSpy: IdentityValidationLoaderCoordinating {
    private(set) var callNextViewControllerCount = 0
    private(set) var callDismissCount = 0

    func nextViewController(for step: IdentityValidationStep) {
        callNextViewControllerCount += 1
    }

    func dismiss() {
        callDismissCount += 1
    }
}
