import XCTest
@testable import PicPay

final class IdentityValidationLoaderCoordinatorTest: XCTestCase {
    private let coordinatorDelegateSpy = IdentityValidationCoordinatorSpy()
    
    private lazy var sut = IdentityValidationLoaderCoordinator(coordinatorDelegate: coordinatorDelegateSpy)

    func testNextViewController_ShouldCallPushNextController() {
        sut.nextViewController(for: IdentityValidationStep())

        XCTAssertEqual(coordinatorDelegateSpy.callPresentLoadedControllerCount, 1)
    }

    func testDismiss_ShouldCallDismiss() {
        sut.dismiss()
    
        XCTAssertEqual(coordinatorDelegateSpy.callDismissCount, 1)
    }
}
