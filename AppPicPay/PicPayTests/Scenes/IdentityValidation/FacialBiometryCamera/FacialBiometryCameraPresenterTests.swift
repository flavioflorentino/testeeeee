@testable import PicPay
import XCTest

private final class FacialBiometryCameraCoordinatorSpy: FacialBiometryCameraCoordinating {
    var viewController: UIViewController?
    private(set) var callCloseActionCount = 0
    private(set) var callPreviewActionCount = 0
    private(set) var callConfigActionCount = 0
    
    func perform(action: FacialBiometryCameraAction) {
        switch action {
        case .close:
            callCloseActionCount += 1
        case .preview:
            callPreviewActionCount += 1
        case .config:
            callConfigActionCount += 1
        }
    }
}

private final class FacialBiometryCameraDisplaySpy: FacialBiometryCameraDisplay {
    private(set) var callDisplayAuthorizedCameraAccessCount = 0
    private(set) var callDisplayErrorCount = 0
    private(set) var errorMessage = ""
    private(set) var callDisplayAlertCount = 0
    private(set) var alertPresented: Alert?
    
    func displayAuthorizedCameraAccess() {
        callDisplayAuthorizedCameraAccessCount += 1
    }
    
    func displayAlert(with error: Error?) {
        errorMessage = error?.localizedDescription ?? ""
        callDisplayErrorCount += 1
    }
    
    func displayAlert(_ alert: Alert) {
        callDisplayAlertCount += 1
        alertPresented = alert
    }
}

final class FacialBiometryCameraPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = FacialBiometryCameraCoordinatorSpy()
    private lazy var viewControllerSpy = FacialBiometryCameraDisplaySpy()
    
    private lazy var sut: FacialBiometryCameraPresenter = {
        let presenter = FacialBiometryCameraPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testClose_WhenCalledFromViewModel_ShouldPassActionToCoordinator() {
        sut.close()
     
        XCTAssertEqual(coordinatorSpy.callCloseActionCount, 1)
    }
    
    func testConfigureCameraAuthorized_WhenCalledFromViewModel_ShouldDisplayAuthorizedCameraAccessOnViewController() {
        sut.configureCameraAuthorized()
        
        XCTAssertEqual(viewControllerSpy.callDisplayAuthorizedCameraAccessCount, 1)
    }
    
    func testHandleCaptureError_WhenCalledFromViewModel_ShouldDisplayErrorAlertOnViewController() {
        sut.handleCaptureError(PicPayError(message: "Some Error"))
        
        XCTAssertEqual(viewControllerSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(viewControllerSpy.errorMessage, "Some Error")
    }
    
    func testShowPreview_WhenCalledFromViewModel_ShouldPassActionToCoordinator() {
        sut.showPreview(with: Assets.Icons.icoCameraWhite.image)
        
        XCTAssertEqual(coordinatorSpy.callPreviewActionCount, 1)
    }
    
    func testPresentPermissionDeniedAlert_WhenCalledFromViewModel_ShouldDisplayAlertOnViewController() {
        sut.presentPermissionDeniedAlert()
        
        XCTAssertEqual(viewControllerSpy.callDisplayAlertCount, 1)
        XCTAssertEqual(viewControllerSpy.alertPresented?.title?.string, FacialBiometricLocalizable.cameraPermissionAlertTitle.text)
        XCTAssertEqual(viewControllerSpy.alertPresented?.text?.string, FacialBiometricLocalizable.cameraPermissionAlertMessage.text)
    }
    
    func testPermissionAlert_WhenActionIsDoItLater_ShouldPassActionToCoordinator() throws {
        sut.presentPermissionDeniedAlert()
               
        XCTAssertEqual(viewControllerSpy.callDisplayAlertCount, 1)
        let alert = try XCTUnwrap(viewControllerSpy.alertPresented)
        
        let doItLater = alert.buttons.first(where: { $0.type == .cleanUnderlined })
        XCTAssertNotNil(doItLater)
        
        doItLater?.handler?(AlertPopupViewController(alert: alert, controller: nil), UIPPButton())
        
        expectation(waitForCondition: self.coordinatorSpy.callCloseActionCount >= 1)
        
        waitForExpectations(timeout: 5.0)
    }
    
    func testPermissionAlert_WhenActionConfiguration_ShouldPassActionToCoordinator() throws {
        sut.presentPermissionDeniedAlert()
               
        XCTAssertEqual(viewControllerSpy.callDisplayAlertCount, 1)
        let alert = try XCTUnwrap(viewControllerSpy.alertPresented)
        
        let configurationButton = alert.buttons.first(where: { $0.type == .cta })
        XCTAssertNotNil(configurationButton)
        
        configurationButton?.handler?(AlertPopupViewController(alert: alert, controller: nil), UIPPButton())
        
        expectation(waitForCondition: self.coordinatorSpy.callConfigActionCount >= 1)
        
        waitForExpectations(timeout: 5.0)
    }

}
