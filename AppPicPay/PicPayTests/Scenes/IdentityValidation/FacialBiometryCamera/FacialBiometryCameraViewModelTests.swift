import AnalyticsModule
import PermissionsKit
@testable import PicPay
import XCTest

private final class FacialBiometryCameraServiceserviceMock: FacialBiometryCameraServicing {
    var permissionStatusForCheck: PermissionStatus = .notDetermined
    private(set) var callRequestCameraPermissionCount = 0
    
    func checkCameraPermission(completion: @escaping (PermissionStatus) -> Void) {
        completion(permissionStatusForCheck)
    }
    
    func requestCameraPermission(completion: @escaping (PermissionStatus) -> Void) {
        callRequestCameraPermissionCount += 1
    }
}

private final class FacialBiometryCameraPresenterSpy: FacialBiometryCameraPresenting {
    var viewController: FacialBiometryCameraDisplay?
    private(set) var callConfigureCameraAuthorizedCount = 0
    private(set) var callCloseCount = 0
    private(set) var callHandleCaptureErrorCount = 0
    private(set) var callShowPreviewCount = 0
    private(set) var callShowPermissionDeniedAlertCount = 0
    
    func configureCameraAuthorized() {
        callConfigureCameraAuthorizedCount += 1
    }
    
    func close() {
        callCloseCount += 1
    }
    
    func showPreview(with previewImage: UIImage) {
        callShowPreviewCount += 1
    }
    
    func handleCaptureError(_ error: Error?) {
        callHandleCaptureErrorCount += 1
    }
    
    func presentPermissionDeniedAlert() {
        callShowPermissionDeniedAlertCount += 1
    }
}

final class FacialBiometryCameraViewModelTests: XCTestCase {
    private lazy var serviceMock = FacialBiometryCameraServiceserviceMock()
    private lazy var presenterSpy = FacialBiometryCameraPresenterSpy()
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var mockDependencies = DependencyContainerMock(analyticsSpy)
    
    private lazy var sut: FacialBiometryCameraViewModel = {
        let viewModel = FacialBiometryCameraViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            dependencies: mockDependencies
        )
        return viewModel
    }()
    
    func testCheckCameraPermission_WhenPermissionIsGranted_ShouldPresentConfigureCameraAuthorized() {
        serviceMock.permissionStatusForCheck = .authorized
        sut.checkCameraPermission()
        
        XCTAssertEqual(presenterSpy.callConfigureCameraAuthorizedCount, 1)
    }
    
    func testCheckCameraPermission_WhenPermissionIsDenied_ShouldPresentConfigureCameraDenied() {
        serviceMock.permissionStatusForCheck = .denied
        sut.checkCameraPermission()
        
        XCTAssertEqual(presenterSpy.callShowPermissionDeniedAlertCount, 1)
    }
    
    func testCheckCameraPermission_WhenPermissionIsNotDetermined_ShouldRequestPermission() {
        serviceMock.permissionStatusForCheck = .notDetermined
        
        sut.checkCameraPermission()
        
        XCTAssertEqual(serviceMock.callRequestCameraPermissionCount, 1)
    }
    
    func testClose_WhenUserTapsCloseButton_ShouldCloseCaptureSelfie() {
        sut.close()
        
        XCTAssertEqual(presenterSpy.callCloseCount, 1)
        
        let expectedEvent = IdentityValidationEvent.takeSelfie(action: .close).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testHandleCaptureImageError_WhenReceivedErrorFromCamera_ShouldHandleErrorOnPresenter() {
        sut.handleCaptureImageError(PicPayError(message: "Some Error"))
        
        XCTAssertEqual(presenterSpy.callHandleCaptureErrorCount, 1)
    }
    
    func testHandleCaptureImageSuccess_WhenReceivedImageFromCamera_ShouldShowImagePreview() {
        sut.handleCaptureImageSuccess(image: Assets.Icons.icoCameraWhite.image)
        
        XCTAssertEqual(presenterSpy.callShowPreviewCount, 1)
        
        let expectedEvent = IdentityValidationEvent.takeSelfie(action: .taken).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testTrackCameraPresentation_WhenScreenIsPresented_ShouldLogEvent() {
        sut.trackCameraPresentation()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        
        let expectedEvent = IdentityValidationEvent.takeSelfie(action: .none).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
}
