@testable import PicPay
import XCTest

final class FacialBiometryCameraCoordinatorTests: XCTestCase {
    private let navController = NavigationControllerMock(rootViewController: UIViewController())
    private let applicationSpy = UIApplicationSpy()
    
    private lazy var sut: FacialBiometryCameraCoordinator = {
        let coordinator = FacialBiometryCameraCoordinator(navigationController: navController, application: applicationSpy)
        coordinator.viewController = navController
        return coordinator
    }()
    
    func testPerformCloseAction_WhenCalledFromPresenter_ShouldDismissController() {
        sut.perform(action: .close)
        
        XCTAssertTrue(navController.isDismissViewControllerCalled)
    }

    func testPerformPreviewAction_WhenCalledFromPresenter_ShouldPushPreviewViewController() {
        sut.perform(action: .preview(image: Assets.Background.purpleRectangle.image))
            
        XCTAssertTrue(navController.isPushViewControllerCalled)
        XCTAssertTrue(navController.pushedViewController?.isKind(of: FacialBiometryPreviewViewController.self) ?? false)
    }
    
    func testPerform_WhenActionIsConfig_ShouldSwitchToAppSettings() throws {
        let validURL = URL(string: UIApplication.openSettingsURLString)?.description
        applicationSpy.canOpenUrl = true

        sut.perform(action: .config)
        
        let spyUrl = try XCTUnwrap(applicationSpy.openUrl)
        XCTAssertEqual(spyUrl.description, validURL)
    }
}
