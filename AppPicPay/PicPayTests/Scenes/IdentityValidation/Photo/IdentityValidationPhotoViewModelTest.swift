import XCTest
@testable import PicPay

final class IdentityValidationPhotoViewModelTest: XCTestCase {
    private let serviceMock = IdentityValidationPhotoServiceMock()
    private let presenterSpy = IdentityValidationPhotoPresenterSpy()
    private let step = IdentityValidationStep(type: .selfie, title: "random Title", text: "random text")
    private lazy var sut = IdentityValidationPhotoViewModel(step: step, service: serviceMock, presenter: presenterSpy)
    
    func testLoadStepInfoShouldCallHesHandler() {
        sut.loadStepInfo()
        
        XCTAssertTrue(presenterSpy.didCallHandleStepInfo)
        XCTAssertEqual(presenterSpy.step?.title, step.title)
        XCTAssertEqual(presenterSpy.step?.text, step.text)
    }
    
    func testOpenCameraShouldCallHesHandler() {
        sut.openCamera()
        
        XCTAssertTrue(presenterSpy.didCallHandleOpenCamera)
        XCTAssertEqual(presenterSpy.step?.title, "random Title")
        XCTAssertEqual(presenterSpy.step?.text, "random text")
    }
    
    func testCloseFlowShouldCallPresenter() {
           sut.closeFlow()
       
           XCTAssertTrue(presenterSpy.didCallHandleCloseFlow)
       }
}

final private class IdentityValidationPhotoServiceMock: IdentityValidationPhotoServicing {
    var isSuccess = false
    
    func uploadImage(imageData: Data, uploadProgress: @escaping ((Double) -> Void), _ completion: @escaping ((PicPayResult<IdentityValidationStep>) -> Void)) {
        if isSuccess {
            let step = IdentityValidationStep(type: .questions, title: "random Title", text: "random text")
            completion(PicPayResult.success(step)); return
        }
        
        let error = PicPayError(message: "random error")
        completion(PicPayResult.failure(error))
    }
}

final private class IdentityValidationPhotoPresenterSpy: IdentityValidationPhotoPresenting {
    var coordinator: IdentityValidationPhotoCoordinating?
    var viewController: IdentityValidationPhotoDisplayable?
    
    private(set) var didCallHandleStepInfo = false
    private(set) var questionManager: IdentityValidationQuestionManager?
    private(set) var didCallHandleOpenCamera = false
    private(set) var didCallHandleStartLoading = false
    private(set) var didCallHandleNextStep = false
    private(set) var step: IdentityValidationStep?
    private(set) var didCallHandleError = false
    private(set) var error: PicPayError?
    private(set) var didCallHandleCloseFlow = false
    
    func handleStepInfo(_ step: IdentityValidationStep) {
        didCallHandleStepInfo.toggle()
        self.step = step
    }
    
    func handleOpenCamera(with step: IdentityValidationStep) {
        didCallHandleOpenCamera.toggle()
        self.step = step
    }
    
    func handleStartLoading() {
        didCallHandleStartLoading.toggle()
    }
    
    func handleNextStep(_ step: IdentityValidationStep) {
        didCallHandleNextStep.toggle()
        self.step = step
    }
    
    func handleError(_ error: PicPayError) {
        didCallHandleError.toggle()
        self.error = error
    }
    
    func handleCloseFlow() {
        didCallHandleCloseFlow.toggle()
    }
}
