import XCTest
@testable import PicPay

final class IdentityValidationPhotoPresenterTest: XCTestCase {
    private let coordinatorSpy = IdentityValidationPhotoCoordinatorSpy()
    private let controllerSpy = IdentityValidationPhotoControllerSpy()
    private let queue = DispatchQueue(label: #function)
    
    private lazy var sut: IdentityValidationPhotoPresenter = {
        let sut = IdentityValidationPhotoPresenter(coordinator: coordinatorSpy, dependencies: DependencyContainerMock(queue))
        sut.viewController = controllerSpy
        return sut
    }()
    
    func testStepInfoShouldCallViewController() {
        let step = IdentityValidationStep(type: .questions, title: "a random title", text: "a random text")
        sut.handleStepInfo(step)
    
        XCTAssertTrue(controllerSpy.didCallPresentStepInfo)
        XCTAssertEqual(controllerSpy.photoInfo?.image, Assets.Ilustration.iluDocumentBack.image)
        XCTAssertEqual(controllerSpy.photoInfo?.title, step.title)
        XCTAssertEqual(controllerSpy.photoInfo?.description, step.text)
    }

    func testStepInfo_WhenStepIsSelfie_ShouldPresentSelfieImage() {
        let step = IdentityValidationStep(type: .selfie, title: "", text: "")
        sut.handleStepInfo(step)

        XCTAssertEqual(controllerSpy.photoInfo?.image, Assets.Ilustration.iluPhotoSelfie.image)
    }

    func testStepInfo_WhenStepIsDocumentFront_ShouldPresentDocumentImage() {
        let step = IdentityValidationStep(type: .documentFront, title: "", text: "")
        sut.handleStepInfo(step)

        XCTAssertEqual(controllerSpy.photoInfo?.image, Assets.Ilustration.iluDocumentFront.image)
    }

    func testHandleOpenCamera_ShouldCallCoordinatorPresentCamera() {
        sut.handleOpenCamera(with: IdentityValidationStep())

        XCTAssertEqual(coordinatorSpy.callPresentCameraCount, 1)
    }
    
    func testHandleNextStepShouldCallCoordinator() {
        let step = IdentityValidationStep(type: .questions, title: "a random title", text: "a random text")
        sut.handleNextStep(step)
        
        let expectation = self.expectation(description: "HandleNextStepShouldCallCoordinator")

        queue.async {
            XCTAssertEqual(self.coordinatorSpy.callNextViewControllerCount, 1)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5)
    }
    
    func testHandleStartLoadingShouldStartLoading() {
        sut.handleStartLoading()
        
        XCTAssertTrue(controllerSpy.didCallStartLoading)
    }
    
    func testSendAnswersShouldReturnAnError() {
        let error = PicPayError(message: "a random error")
        sut.handleError(error)
        
        let expectation = self.expectation(description: "SendAnswersShouldReturnAnError")

        queue.async {
            XCTAssertTrue(self.controllerSpy.didCallPresentAlert)
            XCTAssertEqual(self.controllerSpy.error, error)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5)
    }
    
    func testHandleCloseFlowShouldCallCoordinator() {
        sut.handleCloseFlow()
        
        XCTAssertEqual(coordinatorSpy.callDismissCount, 1)
    }
}

final private class IdentityValidationPhotoControllerSpy: IdentityValidationPhotoDisplayable {
    private(set) var didCallPresentStepInfo = false
    private(set) var photoInfo: (image: UIImage, title: String, description: String)?
    private(set) var didCallStartLoading = false
    private(set) var didCallPresentAlert = false
    private(set) var error: PicPayError?
    
    func presentStepInfo(image: UIImage, title: String, description: String) {
        didCallPresentStepInfo.toggle()
        self.photoInfo = (image: image, title: title, description: description)
    }
    
    func startLoading() {
        didCallStartLoading.toggle()
    }
    
    func stopLoading() { }
    
    func presentAlert(with error: PicPayError) {
        didCallPresentAlert.toggle()
        self.error = error
    }
}

final private class IdentityValidationPhotoCoordinatorSpy: IdentityValidationPhotoCoordinating {
    private(set) var callPresentCameraCount = 0
    private(set) var callNextViewControllerCount = 0
    private(set) var callDismissCount = 0

    func presentCamera(for stepType: IdentityValidationStep.StepType) {
        callPresentCameraCount += 1
    }

    func nextViewController(for step: IdentityValidationStep) {
        callNextViewControllerCount += 1
    }

    func dismiss() {
        callDismissCount += 1
    }
}
