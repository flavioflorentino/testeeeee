import XCTest
@testable import PicPay

final class IdentityValidationPhotoCoordinatorTest: XCTestCase {
    private let coordinatorDelegateSpy = IdentityValidationCoordinatorSpy()
    private let navController = NavigationControllerMock(rootViewController: UIViewController())
    
    private lazy var sut = IdentityValidationPhotoCoordinator(coordinatorDelegate: coordinatorDelegateSpy, navigationController: navController)

    func testNextViewController_ShouldCallPushNextController() {
        sut.nextViewController(for: IdentityValidationStep())

        XCTAssertEqual(coordinatorDelegateSpy.callPushNextControllerCount, 1)
    }

    func testDismiss_ShouldCallDismiss() {
        sut.dismiss()
       
        XCTAssertEqual(coordinatorDelegateSpy.callDismissCount, 1)
    }

    func testPresentCamera_WhenStepIsInitial_ShouldCallDismiss() {
        sut.presentCamera(for: .initial)

        XCTAssertEqual(coordinatorDelegateSpy.callDismissCount, 1)
    }

    func testPresentCamera_WhenStepIsSelfie_ShouldCallDismiss() {
        sut.presentCamera(for: .initial)

        XCTAssertEqual(coordinatorDelegateSpy.callDismissCount, 1)
    }

    func testPresentCamera_WhenStepIsSelfie_ShouldShowPhotoViewController() throws {
        sut.presentCamera(for: .selfie)

        let presentedNavigation = try XCTUnwrap(navController.viewControllerPresented as? UINavigationController)
        let controller = try XCTUnwrap(presentedNavigation.viewControllers.first)
        XCTAssertTrue(controller is IdentityValidationCameraViewController)
    }

    func testPresentCamera_WhenStepIsDocumentFront_ShouldShowPhotoViewController() throws {
        sut.presentCamera(for: .documentFront)

        let presentedNavigation = try XCTUnwrap(navController.viewControllerPresented as? UINavigationController)
        let controller = try XCTUnwrap(presentedNavigation.viewControllers.first)
        XCTAssertTrue(controller is IdentityValidationCameraViewController)
    }

    func testPresentCamera_WhenStepIsDocumentBack_ShouldShowPhotoViewController() throws {
        sut.presentCamera(for: .documentBack)

        let presentedNavigation = try XCTUnwrap(navController.viewControllerPresented as? UINavigationController)
        let controller = try XCTUnwrap(presentedNavigation.viewControllers.first)
        XCTAssertTrue(controller is IdentityValidationCameraViewController)
    }
}
