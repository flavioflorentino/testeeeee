import XCTest
import FeatureFlag

@testable import PicPay

private final class IdentityValidationRouterMock: IdentityValidationRouting {
    var shouldReturnController = true

    func getController(for model: IdentityValidationRouterModel) -> UIViewController? {
        if shouldReturnController {
            let controller = UIViewController()
            controller.accessibilityLabel = "Test Controller"
            return controller
        }
        return nil
    }
}

final class IdentityValidationFlowCoordinatorTests: XCTestCase {
    private let originController = NavigationControllerMock(rootViewController: UIViewController())
    private let navController = NavigationControllerMock(rootViewController: UIViewController())
    private let routerMock = IdentityValidationRouterMock()

    private lazy var sut = IdentityValidationFlowCoordinator(
        from: originController,
        originFlow: .settings,
        navigationController: navController,
        dependencies: DependencyContainerMock(FeatureManagerMock()),
        router: routerMock
    )

    func testStart_ShouldPresentLoaderControllerFromOriginController() throws {
        sut.start()

        XCTAssertTrue(originController.isPresentViewControllerCalled)
        let childController = try XCTUnwrap(navController.children.last)
        XCTAssertTrue(childController is IdentityValidationLoaderViewController)
    }

    func testPresentLoadedController_WhenControllerIsNotNil_ShouldReplaceController() throws {
        sut.presentLoadedController(for: IdentityValidationStep())

        let controller = try XCTUnwrap(navController.viewControllers.last)
        XCTAssertEqual(controller.accessibilityLabel, "Test Controller")
    }

    func testPresentLoadedController_WhenControllerIsNil_ShouldShowAlertError() {}

    func testPushNextController_WhenControllerIsNotNil_ShouldPushController() throws {
        sut.pushNextController(for: IdentityValidationStep())

        let pushedController = try XCTUnwrap(navController.pushedViewController)
        XCTAssertEqual(pushedController.accessibilityLabel, "Test Controller")
    }

    func testPushNextController_WhenControllerIsNil_ShouldShowAlertError() {}

    func testDismiss_ShouldDismissNavigation() {
        sut.dismiss()

        XCTAssertTrue(navController.isDismissViewControllerCalled)
    }
}
