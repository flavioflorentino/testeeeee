@testable import PicPay

final class IdentityValidationCoordinatorSpy: IdentityValidationCoordinating {
    private(set) var callPresentLoadedControllerCount = 0
    private(set) var callPushNextControllerCount = 0
    private(set) var callDismissCount = 0

    func presentLoadedController(for step: IdentityValidationStep) {
        callPresentLoadedControllerCount += 1
    }

    func pushNextController(for step: IdentityValidationStep) {
        callPushNextControllerCount += 1
    }

    func dismiss() {
        callDismissCount += 1
    }
}
