import AnalyticsModule
@testable import PicPay
import XCTest

private final class DocumentCaptureTipsPresenterSpy: DocumentCaptureTipsPresenting {
    var viewController: DocumentCaptureTipsDisplay?
    private(set) var callHandleContinueCount = 0
    private(set) var callHandleNextStepCount = 0
    private(set) var callHandleCloseCount = 0
    private(set) var callConfigureViewsCount = 0

    func configureViews(for step: IdentityValidationStep) {
        callConfigureViewsCount += 1
    }
    
    func handleContinue(step: IdentityValidationStep) {
        callHandleContinueCount += 1
    }

    func handleNextStep(step: IdentityValidationStep) {
        callHandleNextStepCount += 1
    }

    func handleClose() {
        callHandleCloseCount += 1
    }
}

final class DocumentCaptureTipsViewModelTests: XCTestCase {
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var mockDependencies = DependencyContainerMock(analyticsSpy)
    
    private let presenterSpy = DocumentCaptureTipsPresenterSpy()
    private lazy var sut = DocumentCaptureTipsViewModel(
        presenter: presenterSpy,
        step: IdentityValidationStep(type: .documentFront, title: "TITLE", text: "TEXT", data: nil),
        dependencies: mockDependencies
    )

    func testConfigureView_ShouldCallPresenterConfigureViews() {
        sut.configureViews()
        
        XCTAssertEqual(presenterSpy.callConfigureViewsCount, 1)
        
        let expectedEvent = IdentityValidationEvent.documentTips(action: .none).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testHandleContinue_ShouldCallPresenterHandleContinue() {
        sut.handleContinue()

        XCTAssertEqual(presenterSpy.callHandleContinueCount, 1)
    }

    func testHandleClose_ShouldCallPresenterHandleClose() {
        sut.handleClose()

        XCTAssertEqual(presenterSpy.callHandleCloseCount, 1)
    }
}
