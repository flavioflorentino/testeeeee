@testable import PicPay
import XCTest

private final class DocumentCaptureTipsViewModelSpy: DocumentCaptureTipsViewModelInputs {
    private(set) var callAddDocumentObserverCount = 0

    func addDocumentObserver() {
        callAddDocumentObserverCount += 1
    }

    func configureViews() {}
    func handleContinue() {}
    func handleClose() {}
}

final class DocumentCaptureTipsCoordinatorTests: XCTestCase {
    private let viewModelSpy = DocumentCaptureTipsViewModelSpy()
    private let coordinatorDelegateSpy = IdentityValidationCoordinatorSpy()
    private let navController = NavigationControllerMock(rootViewController: UIViewController())

    private lazy var sut: DocumentCaptureTipsCoordinator = {
        let coordinator = DocumentCaptureTipsCoordinator(coordinatorDelegate: coordinatorDelegateSpy, navigationController: navController)
        coordinator.viewModel = viewModelSpy
        return coordinator
    }()

    func testPerform_WhenActionIsClose_ShouldDismissNavigation() {
        sut.perform(action: .close)

        XCTAssertEqual(coordinatorDelegateSpy.callDismissCount, 1)
    }

    func testPerform_WhenActionIsPresentCamera_ShouldPresentViewController() throws {
        let step = IdentityValidationStep(type: .documentFront)
        sut.perform(action: .presentCamera(step: step))

        let presentedController = try XCTUnwrap(navController.viewControllerPresented)
        let presentedNavigation = try XCTUnwrap(presentedController as? UINavigationController)
        let pushedController = try XCTUnwrap(presentedNavigation.viewControllers.first)
        XCTAssertTrue(pushedController is DocumentValidationCameraViewController)
    }

    func testPerform_WhenActionIsNextStep_ShouldCallPushNextController() {
        sut.perform(action: .nextStep(step: IdentityValidationStep()))

        XCTAssertEqual(coordinatorDelegateSpy.callPushNextControllerCount, 1)
    }

    func testPerform_WhenActionIsNextStep_WhenStepTypeIsDocumentBack_CallViewModelAddObserver() {
        sut.perform(action: .nextStep(step: IdentityValidationStep(type: .documentBack)))

        XCTAssertEqual(viewModelSpy.callAddDocumentObserverCount, 1)
    }

    func testPerform_WhenActionIsNextStep_WhenStepTypeIsDocumentBack_ShouldPresentCamera() throws {
        sut.perform(action: .nextStep(step: IdentityValidationStep(type: .documentBack)))

        let presentedController = try XCTUnwrap(navController.viewControllerPresented)
        let presentedNavigation = try XCTUnwrap(presentedController as? UINavigationController)
        let pushedController = try XCTUnwrap(presentedNavigation.viewControllers.first)
        XCTAssertTrue(pushedController is DocumentValidationCameraViewController)
    }
}
