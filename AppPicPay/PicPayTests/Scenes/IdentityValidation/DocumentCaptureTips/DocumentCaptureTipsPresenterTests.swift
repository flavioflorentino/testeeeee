@testable import PicPay
import XCTest

private final class DocumentCaptureTipsCoordinatorSpy: DocumentCaptureTipsCoordinating {
    var viewModel: DocumentCaptureTipsViewModelInputs?
    private(set) var callPresentCameraActionCount = 0
    private(set) var callNextStepActionCount = 0
    private(set) var callCloseActionCount = 0

    func perform(action: DocumentCaptureTipsAction) {
        switch action {
        case .presentCamera:
            callPresentCameraActionCount += 1
        case .nextStep:
            callNextStepActionCount += 1
        case .close:
            callCloseActionCount += 1
        }
    }
}

private final class DocumentCaptureTipsControllerSpy: DocumentCaptureTipsDisplay {
    private(set) var callDisplayTipsCount = 0
    private(set) var displayedTips: ValidationTips?
    
    func displayTips(_ tips: ValidationTips) {
        callDisplayTipsCount += 1
        displayedTips = tips
    }
}

final class DocumentCaptureTipsPresenterTests: XCTestCase {
    private let coordinatorSpy = DocumentCaptureTipsCoordinatorSpy()
    private let viewControllerSpy = DocumentCaptureTipsControllerSpy()

    private lazy var sut:DocumentCaptureTipsPresenter = {
        let presenter = DocumentCaptureTipsPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()

    func testConfigureViews_WhenStepTypeIsDocument_ShouldDisplayDocumentCaptureTips() throws {
        sut.configureViews(for: IdentityValidationStep(type: .documentFront, title: "TITLE", text: "TEXTT", data: nil))
        
        XCTAssertEqual(viewControllerSpy.callDisplayTipsCount, 1)
        let tips = try XCTUnwrap(viewControllerSpy.displayedTips)
        XCTAssertEqual(tips.title, DocumentCaptureLocalizable.documentsTipsTitle.text)
    }
    
    func testConfigureViews_WhenStepTypeIsSelfie_ShouldDisplaySelfieCaptureTips() throws {
        sut.configureViews(for: IdentityValidationStep(type: .selfie, title: "TITLE", text: "TEXTT", data: nil))
        
        XCTAssertEqual(viewControllerSpy.callDisplayTipsCount, 1)
        let tips = try XCTUnwrap(viewControllerSpy.displayedTips)
        XCTAssertEqual(tips.title, DocumentCaptureLocalizable.selfieTipsTitle.text)
    }
    
    func testHandleContinue_ShouldCallCoordinatorWithPresentCameraAction() {
        let step = IdentityValidationStep(type: .documentFront)
        sut.handleContinue(step: step)

        XCTAssertEqual(coordinatorSpy.callPresentCameraActionCount, 1)
    }

    func testHandleNextStep_ShouldCallCoordinatorWithNextStepAction() {
        let step = IdentityValidationStep(type: .documentFront)
        sut.handleNextStep(step: step)

        XCTAssertEqual(coordinatorSpy.callNextStepActionCount, 1)
    }

    func testHandleClose_ShouldCallCoordinatorWithCloseAction() {
        sut.handleClose()

        XCTAssertEqual(coordinatorSpy.callCloseActionCount, 1)
    }
}
