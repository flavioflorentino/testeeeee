@testable import PicPay
import XCTest

private final class FacialBiometryPreviewCoordinatorSpy: FacialBiometryPreviewCoordinating {
    var viewController: UIViewController?
    private(set) var actionCalled: FacialBiometryPreviewAction?
    private(set) var callPerformActionCount = 0
    
    func perform(action: FacialBiometryPreviewAction) {
        actionCalled = action
        callPerformActionCount += 1
    }
}

private final class FacialBiometryPreviewDisplaySpy: FacialBiometryPreviewDisplay {
    private(set) var displayPreviewImageCallCount = 0
    private(set) var displayLoadingCallCount = 0
    private(set) var displayUploadErrorMessageCallCount = 0
    private(set) var displayedError = ""
    private(set) var configureVerifyStatusViewInfoCallCount = 0
    private(set) var configureUploadErrorViewInfoCallCount = 0
    private(set) var statusViewTitleText = ""
    private(set) var statusViewDenialActionText = ""
    private(set) var statusViewConfirmActionText = ""
    private(set) var uploadErrorViewActionText = ""
    
    func displayPreviewImage(_ image: UIImage) {
        displayPreviewImageCallCount += 1
    }
    
    func displayLoading(_ isLoading: Bool) {
        displayLoadingCallCount += 1
    }
    
    func displayUploadErrorMessage(_ message: String) {
        displayUploadErrorMessageCallCount += 1
        displayedError = message
    }
    
    func configureVerifyStatusViewInfo(titleText: String, denialActionText: String, confirmActionText: String) {
        configureVerifyStatusViewInfoCallCount += 1
        statusViewTitleText = titleText
        statusViewDenialActionText = denialActionText
        statusViewConfirmActionText = confirmActionText
    }
    
    func configureUploadErrorViewInfo(actionText: String) {
        configureUploadErrorViewInfoCallCount += 1
        uploadErrorViewActionText = actionText
    }
}

final class FacialBiometryPreviewPresenterTests: XCTestCase {
    private lazy var mockDependencies = DependencyContainerMock()
    private lazy var coordinatorSpy = FacialBiometryPreviewCoordinatorSpy()
    private lazy var displaySpy = FacialBiometryPreviewDisplaySpy()
    
    private lazy var sut: FacialBiometryPreviewPresenter = {
        let presenter = FacialBiometryPreviewPresenter(coordinator: coordinatorSpy, dependencies: mockDependencies)
        presenter.viewController = displaySpy
        return presenter
    }()
    
    func testPresentPreviewImage_WhenCalledFromViewModel_ShouldDisplayImageOnViewController() {
        sut.presentPreviewImage(Assets.Icons.accelerateAnalysis.image)
        
        XCTAssertEqual(displaySpy.displayPreviewImageCallCount, 1)
    }
    
    func testPresentLoading_WhenCalledFromViewModel_ShouldDisplayLoadingOnViewController() {
        sut.presentLoading(true)
        mockDependencies.mainQueue.async {
            XCTAssertEqual(self.displaySpy.displayLoadingCallCount, 1)
        }
    }
    
    func testPresentCamera_WhenCalledFromViewModel_ShouldPerformActionOnCoordinator() {
        sut.presentCamera()
        mockDependencies.mainQueue.async {
            XCTAssertEqual(self.coordinatorSpy.callPerformActionCount, 1)
            XCTAssertNotNil(self.coordinatorSpy.actionCalled)
        }
    }
    
    func testPresentNextStep_WhenCalledFromViewModel_ShouldPerformActionOnCoordinator() {
        sut.presentNextStep(IdentityValidationStep(type: .documentFront, title: "", text: "", data: nil))
        mockDependencies.mainQueue.async {
            XCTAssertEqual(self.coordinatorSpy.callPerformActionCount, 1)
            XCTAssertNotNil(self.coordinatorSpy.actionCalled)
        }
    }
    
    func testPresentUploadError_WhenReceivedFromService_ShouldDisplayErrorOnViewController() {
        sut.presentUploadError(PicPayError(message: "Unknow error"))
        mockDependencies.mainQueue.async {
            XCTAssertEqual(self.displaySpy.displayUploadErrorMessageCallCount, 1)
            XCTAssertEqual(self.displaySpy.displayedError, "Unknow error")
        }
    }
    
    func testConfigureSubviews_WhenCalledFromViewModel_ShouldConfigureSubviewsOnViewController() {
        sut.configureSubviews()
        
        XCTAssertEqual(displaySpy.configureVerifyStatusViewInfoCallCount, 1)
        XCTAssertEqual(displaySpy.configureUploadErrorViewInfoCallCount, 1)
        XCTAssertEqual(displaySpy.statusViewTitleText, "Veja se sua foto está boa para enviá-la")
        XCTAssertEqual(displaySpy.statusViewDenialActionText, "Tirar outra foto")
        XCTAssertEqual(displaySpy.statusViewConfirmActionText, "Enviar foto")
    }
}
