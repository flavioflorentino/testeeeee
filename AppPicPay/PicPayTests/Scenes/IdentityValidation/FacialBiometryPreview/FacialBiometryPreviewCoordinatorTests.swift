@testable import PicPay
import XCTest

final class FacialBiometryPreviewCoordinatorTests: XCTestCase {
    private let navController = NavigationControllerMock(rootViewController: UIViewController())
    
    private lazy var sut: FacialBiometryPreviewCoordinator = {
        let coordinator = FacialBiometryPreviewCoordinator(navigationController: navController)
        coordinator.viewController = navController
        return coordinator
    }()
    
    func testPerformTakePhotoAction_WhenCalledFromPresenter_ShouldPopViewController() {
        sut.perform(action: .takePhoto)
        
        XCTAssertTrue(navController.isPushViewControllerCalled)
    }
    
    func testPerformNextStepAction_WhenCalledFromPresenter_ShoulDismissViewController() {
        sut.perform(action: .nextStep(step: IdentityValidationStep(type: .documentFront, title: "", text: "", data: nil)))
        
        XCTAssertTrue(navController.isDismissViewControllerCalled)
    }
}
