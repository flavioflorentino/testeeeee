import AnalyticsModule
@testable import PicPay
import XCTest

private final class FacialBiometryPreviewServiceMock: FacialBiometryPreviewServicing {
    var isSuccess = false
    func uploadImage(imageData: Data, completion: @escaping (PicPayResult<IdentityValidationStep>) -> Void) {
        if isSuccess {
            completion(.success(IdentityValidationStep(type: .documentFront, title: "", text: "", data: nil)))
        } else {
            completion(.failure(PicPayError(message: "Error sending Selfie")))
        }
    }
}

private final class FacialBiometryPreviewPresenterSpy: FacialBiometryPreviewPresenting {
    var viewController: FacialBiometryPreviewDisplay?
    private(set) var presentPreviewImageCallCount = 0
    private(set) var presentNextStepCallCount = 0
    private(set) var nextStep: IdentityValidationStep.StepType = .initial
    private(set) var presentLoadingCallCount = 0
    private(set) var presentCameraCallCount = 0
    private(set) var presentUploadErrorCallCount = 0
    private(set) var configureSubviewsCallCount = 0
    
    func presentPreviewImage(_ image: UIImage) {
        presentPreviewImageCallCount += 1
    }
    
    func presentCamera() {
        presentCameraCallCount += 1
    }
    
    func presentLoading(_ isLoading: Bool) {
        presentLoadingCallCount += 1
    }
    
    func presentUploadError(_ error: PicPayError) {
        presentUploadErrorCallCount += 1
    }
    
    func presentNextStep(_ step: IdentityValidationStep) {
        presentNextStepCallCount += 1
        nextStep = step.type
    }
    
    func configureSubviews() {
        configureSubviewsCallCount += 1
    }
}

final class FacialBiometryPreviewViewModelTests: XCTestCase {
    private lazy var serviceMock = FacialBiometryPreviewServiceMock()
    private lazy var presenterSpy = FacialBiometryPreviewPresenterSpy()
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var mockDependencies = DependencyContainerMock(analyticsSpy)
    
    private lazy var sut: FacialBiometryPreviewViewModel = {
        let viewModel = FacialBiometryPreviewViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            dependencies: mockDependencies,
            previewImage: Assets.Icons.congratulationsCashback.image
        )
        return viewModel
    }()
    
    func testConfigurePreview_WhenViewHasLoaded_ShouldPresentConfiguredPreviewImage() {
        sut.configurePreview()
        
        XCTAssertEqual(presenterSpy.presentPreviewImageCallCount, 1)
        XCTAssertEqual(presenterSpy.configureSubviewsCallCount, 1)
        
        let expectedEvent = IdentityValidationEvent.sendSelfie(action: .none).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testSendPicture_WhenServiceIsSuccessful_ShouldPresentNextStep() {
        serviceMock.isSuccess = true
        sut.sendPicture()
        
        XCTAssertEqual(presenterSpy.presentNextStepCallCount, 1)
        XCTAssertEqual(presenterSpy.presentLoadingCallCount, 2)
        XCTAssertEqual(presenterSpy.nextStep, .documentFront)
        
        let expectedEvent = IdentityValidationEvent.sendSelfie(action: .finished).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testSendPicture_WhenServiceHasFailed_ShouldPresentUploadError() {
        sut.sendPicture()
        
        XCTAssertEqual(presenterSpy.presentLoadingCallCount, 2)
        XCTAssertEqual(presenterSpy.presentUploadErrorCallCount, 1)
        
        let expectedEvent = IdentityValidationEvent.selfieError.event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testTakeAnotherPicture_WhenCalledFromViewModel_ShouldPresentCamera() {
        sut.takeAnotherPicture()
        
        XCTAssertEqual(presenterSpy.presentCameraCallCount, 1)
        
        let expectedEvent = IdentityValidationEvent.sendSelfie(action: .drop).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
}
