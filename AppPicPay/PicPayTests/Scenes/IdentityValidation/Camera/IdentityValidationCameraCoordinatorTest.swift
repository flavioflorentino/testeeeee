//
//  IdentityValidationCameraCoordinatorTest.swift
//  PicPayTests
//
//  Created by Renato Mendes on 21/11/19.
//

import Foundation
import XCTest
@testable import PicPay

final class IdentityValidationCameraCoordinatorTest: XCTestCase {
    private let navController = NavigationControllerMock(rootViewController: UIViewController())
    private let settings = IdentityValidationCameraViewSettings(
        descriptionText: "random description",
        takePictureButtonTitle: "random button title",
        cameraDevice: .front,
        mask: .selfie,
        confirmButtonTitle: "random confirm button title",
        confirmMessage: "random confirm message"
    )

    private lazy var sut: IdentityValidationCameraCoordinator = {
        let coordinator = IdentityValidationCameraCoordinator(settings: settings, navigationController: navController)
        coordinator.viewController = navController.topViewController
        return coordinator
    }()
    
    func testPerformCloseValidationShouldDismissController() {
        sut.perform(action: .close)
        
        XCTAssertTrue(navController.isDismissViewControllerCalled)
    }
}
