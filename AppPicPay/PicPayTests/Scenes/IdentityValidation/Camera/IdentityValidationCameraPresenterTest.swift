import XCTest
@testable import PicPay

final class IdentityValidationCameraPresenterTest: XCTestCase {
    private let coordinatorSpy = IdentityValidationCameraCoordinatorSpy()
    private let controllerSpy = IdentityValidationCameraControllerSpy()
    private lazy var sut: IdentityValidationCameraPresenter = {
        let sut = IdentityValidationCameraPresenter(coordinator: coordinatorSpy)
        sut.viewController = controllerSpy
        return sut
    }()
    
    func testCameraInfoShouldCallViewController() {
        sut.handleCameraInfo(.selfie, description: "random description", takePhotoButtonTitle: "random button title")
    
        XCTAssertTrue(controllerSpy.didCallPresentCameraInfo)
        XCTAssertEqual(controllerSpy.cameraInfo?.maskType, .selfie)
        XCTAssertEqual(controllerSpy.cameraInfo?.description, "random description")
        XCTAssertEqual(controllerSpy.cameraInfo?.takePhotoButtonTitle, "random button title")
    }
    
    func testHandleImagePreviewShouldCallCoordinator() {
        sut.handleImagePreview(UIImage())
    
        XCTAssertTrue(coordinatorSpy.didCallPerform)
    }
    
    func testHandleImagePreviewShouldReturnAnError() {
        sut.handleErrorPreview(nil)
        
        XCTAssertTrue(controllerSpy.didCallPresentAlert)
        XCTAssertNil(controllerSpy.error)
    }
    
    func testHandleCloseFlowShouldCallCoordinator() {
        sut.handleCloseCamera()
        
        XCTAssertTrue(coordinatorSpy.didCallPerform)
    }
}

final private class IdentityValidationCameraControllerSpy: IdentityValidationCameraDisplayable {
    private(set) var didCallPresentCameraInfo = false
    private(set) var cameraInfo: (maskType: IdentityValidationCameraViewSettings.MaskType, description: String, takePhotoButtonTitle: String)?
    private(set) var didCallStartLoading = false
    private(set) var didCallPresentAlert = false
    private(set) var error: Error?
    
    func presentCameraInfo(_ maskType: IdentityValidationCameraViewSettings.MaskType, description: String, takePhotoButtonTitle: String) {
        didCallPresentCameraInfo.toggle()
        cameraInfo = (maskType: maskType, description: description, takePhotoButtonTitle: takePhotoButtonTitle)
    }
    
    func presentAlert(with error: Error?) {
        didCallPresentAlert.toggle()
        self.error = error
    }
}

final private class IdentityValidationCameraCoordinatorSpy: IdentityValidationCameraCoordinating {
    var viewController: UIViewController? = nil
    var navigationController: UINavigationController = UINavigationController()
    var settings: IdentityValidationCameraViewSettings = IdentityValidationCameraViewSettings(
        descriptionText: "random description",
        takePictureButtonTitle: "random button title",
        cameraDevice: .front,
        mask: .selfie,
        confirmButtonTitle: "random confirm button title",
        confirmMessage: "random confirm message"
    )
    
    private(set) var didCallPerform = false
    private(set) var didCallPresentAlert = false
    private(set) var error: PicPayError?

    func perform(action: IdentityValidationAction) {
        didCallPerform.toggle()
    }
}
