import XCTest
@testable import PicPay

final class IdentityValidationCameraViewModelTest: XCTestCase {
    private let presenterSpy = IdentityValidationCameraPresenterSpy()
    private let settings = IdentityValidationCameraViewSettings(
        descriptionText: "random description",
        takePictureButtonTitle: "random button title",
        cameraDevice: .front,
        mask: .selfie,
        confirmButtonTitle: "random confirm button title",
        confirmMessage: "random confirm message"
    )
    private lazy var sut = IdentityValidationCameraViewModel(settings: settings, presenter: presenterSpy)
    
    func testLoadCameraInfoShouldCallHesHandler() {
        sut.loadCameraInfo()
        
        XCTAssertTrue(presenterSpy.didCallHandleCameraInfo)
        XCTAssertEqual(presenterSpy.cameraInfo?.maskType, .selfie)
        XCTAssertEqual(presenterSpy.cameraInfo?.description, "random description")
        XCTAssertEqual(presenterSpy.cameraInfo?.takePhotoButtonTitle, "random button title")
    }
    
    func testCloseFlowShouldCallPresenter() {
        sut.closeCamera()
        
        XCTAssertTrue(presenterSpy.didCallHandleCloseCamera)
    }
}

final private class IdentityValidationCameraPresenterSpy: IdentityValidationCameraPresenting {
    var coordinator: IdentityValidationCameraCoordinating?
    var viewController: IdentityValidationCameraDisplayable?
    
    private(set) var didCallHandleCameraInfo = false
    private(set) var cameraInfo: (maskType: IdentityValidationCameraViewSettings.MaskType, description: String, takePhotoButtonTitle: String)?
    private(set) var didCallHandleCloseCamera = false
    
    func handleCameraInfo(_ maskType: IdentityValidationCameraViewSettings.MaskType, description: String, takePhotoButtonTitle: String) {
        didCallHandleCameraInfo.toggle()
        cameraInfo = (maskType: maskType, description: description, takePhotoButtonTitle: takePhotoButtonTitle)
    }
    
    func handleImagePreview(_ image: UIImage) { }
    
    func handleErrorPreview(_ error: Error?) { }
    
    func handleCloseCamera() {
        didCallHandleCloseCamera.toggle()
    }
}
