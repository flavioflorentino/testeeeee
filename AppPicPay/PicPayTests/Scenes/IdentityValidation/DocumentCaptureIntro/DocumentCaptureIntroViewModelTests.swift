import AnalyticsModule
@testable import PicPay
import XCTest

final class DocumentCaptureIntroPresenterSpy: DocumentCaptureIntroPresenting {
    var viewController: DocumentCaptureIntroDisplayable?
    private(set) var callHandleStepInfoCount = 0
    private(set) var callHandleContinueCount = 0
    private(set) var callHandleCloseCount = 0

    func handleStepInfo() {
        callHandleStepInfoCount += 1
    }

    func handleContinue(_ step: IdentityValidationStep) {
        callHandleContinueCount += 1
    }

    func handleClose() {
        callHandleCloseCount += 1
    }
}

final class DocumentCaptureIntroViewModelTests: XCTestCase {
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var mockDependencies = DependencyContainerMock(analyticsSpy)
    
    private let presenterSpy = DocumentCaptureIntroPresenterSpy()
    private let step = IdentityValidationStep(type: .done, title: "TITLE", text: "TEXT", data: nil)

    private lazy var sut = DocumentCaptureIntroViewModel(step: step, presenter: presenterSpy, dependencies: mockDependencies)

    func testLoadTexts_ShouldCallPresenterHandleStepInfo() {
        sut.loadTexts()

        XCTAssertEqual(presenterSpy.callHandleStepInfoCount, 1)
        
        let expectedEvent = IdentityValidationEvent.document(action: .none).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testHandleContinue_ShouldCallPresenterHandleContinue() {
        sut.handleContinue()

        XCTAssertEqual(presenterSpy.callHandleContinueCount, 1)
        
        let expectedEvent = IdentityValidationEvent.document(action: .started).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }

    func testHandleClose_ShouldCallPresenterHandleClose() {
        sut.handleClose()

        XCTAssertEqual(presenterSpy.callHandleCloseCount, 1)
        
        let expectedEvent = IdentityValidationEvent.document(action: .drop).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
}
