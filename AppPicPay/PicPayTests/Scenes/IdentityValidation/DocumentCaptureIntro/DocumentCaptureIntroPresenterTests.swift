@testable import PicPay
import XCTest

private final class DocumentCaptureIntroCoordinatorSpy: DocumentCaptureIntroCoordinating {
    private(set) var callNextStepActionCount = 0
    private(set) var callCloseActionCount = 0

    func perform(action: DocumentCaptureIntroAction) {
        switch action {
        case .nextStep:
            callNextStepActionCount += 1
        case .close:
            callCloseActionCount += 1
        }
    }
}

private final class DocumentCaptureIntroControllerSpy: DocumentCaptureIntroDisplayable {
    private(set) var titleString = ""
    private(set) var descriptionString = ""

    func presentStepInfo(title: String, description: String) {
        titleString = title
        descriptionString = description
    }
}

final class DocumentCaptureIntroPresenterTests: XCTestCase {
    private let coordinatorSpy = DocumentCaptureIntroCoordinatorSpy()
    private let viewControllerSpy = DocumentCaptureIntroControllerSpy()

    private lazy var sut: DocumentCaptureIntroPresenter = {
        let presenter = DocumentCaptureIntroPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()

    func testHandleStepInfo_ShouldCallControllersPresentStepInfoWithTitle() {
        sut.handleStepInfo()

        XCTAssertEqual(viewControllerSpy.titleString, FacialBiometricLocalizable.documentIntroTitle.text)
    }

    func testHandleStepInfo_ShouldCallControllersPresentStepInfoWithDescription() {
        sut.handleStepInfo()

        XCTAssertEqual(viewControllerSpy.descriptionString, FacialBiometricLocalizable.documentIntroDescription.text)
    }

    func testHandleContinue_ShouldCallCoordinatorPerformWithNextStep() {
        let step = IdentityValidationStep(type: .documentFront, title: "", text: "", data: nil)
        sut.handleContinue(step)

        XCTAssertEqual(coordinatorSpy.callNextStepActionCount, 1)
    }

    func testHandleClose_ShouldCallCoordinatorPerformWithClose() {
        sut.handleClose()

        XCTAssertEqual(coordinatorSpy.callCloseActionCount, 1)
    }
}
