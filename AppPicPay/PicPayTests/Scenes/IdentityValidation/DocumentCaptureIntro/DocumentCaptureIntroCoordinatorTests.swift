@testable import PicPay
import XCTest

final class DocumentCaptureIntroCoordinatorTests: XCTestCase {
    private let coordinatorDelegateSpy = IdentityValidationCoordinatorSpy()
    private let navController = NavigationControllerMock(rootViewController: UIViewController())

    private lazy var sut = DocumentCaptureIntroCoordinator(coordinatorDelegate: coordinatorDelegateSpy, navigationController: navController)

    func testPerform_WhenActionIsClose_ShouldDismissNavigationController() {
        sut.perform(action: .close)

        XCTAssertEqual(coordinatorDelegateSpy.callDismissCount, 1)
    }

    func testPerform_WhenActionIsNextStep_ShouldPushController() throws {
        sut.perform(action: .nextStep(step: IdentityValidationStep()))

        XCTAssertTrue(navController.isPushViewControllerCalled)
        let pushedViewController = try XCTUnwrap(navController.pushedViewController)
        XCTAssertTrue(pushedViewController is DocumentCaptureTipsViewController)
    }
}
