import XCTest
@testable import PicPay

private final class FakeFactory: InvoicePromoCoordinator.ControllerFactory {
    private(set) lazy var invoiceScannerViewController = InvoiceScannerViewController(interactor: InvoiceScannerInteractorDummy())

    func makeInvoiceScanner() -> InvoiceScannerViewController {
        return invoiceScannerViewController
    }

    final class InvoiceScannerInteractorDummy: InvoiceScannerInteracting {
        func tryStartScanner() { }
        func onReadQRCode(scannerText: String) { }
        func close() { }
    }
}


class InvoicePromoCoordinatorTests: XCTestCase {
    private lazy var viewControllerMock = ViewControllerMock()
    private let factoryMock = FakeFactory()

    private lazy var sut = InvoicePromoCoordinator(viewController: viewControllerMock, controllerFactory: factoryMock)

    func testStart_WhenCalled_ShouldPresentPromotionListScreen() {
        sut.start()

        XCTAssertEqual(viewControllerMock.didPresentControllerCount, 1)
    }
}
