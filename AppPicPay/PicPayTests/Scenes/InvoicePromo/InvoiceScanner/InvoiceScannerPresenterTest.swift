import XCTest
@testable import PicPay

private class InvoiceScannerDisplayingSpy: InvoiceScannerDisplaying {
    // MARK: - startScanner
    private(set) var startScannerCallsCount = 0

    func startScanner() {
        startScannerCallsCount += 1
    }

    // MARK: - displayAlert
    private(set) var displayAlertCallsCount = 0

    func displayAlert() {
        displayAlertCallsCount += 1
    }
}

private class InvoiceScannerCoordinatorSpy: InvoiceScannerCoordinating {
    var viewController: UIViewController?

    //MARK: - perform
    private(set) var action: InvoiceScannerAction?

    func perform(action: InvoiceScannerAction) {
        self.action = action
    }
}

final class InvoiceScannerPresenterTest: XCTestCase {
    private let displaySpy = InvoiceScannerDisplayingSpy()
    private let coordinatorSpy = InvoiceScannerCoordinatorSpy()

    private lazy var sut: InvoiceScannerPresenting = {
        let presenter = InvoiceScannerPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy

        return presenter
    }()

    func testDidStartScanner_WhenCalled_ShouldStartScanner() {
        sut.didStartScanner()

        XCTAssertEqual(displaySpy.startScannerCallsCount, 1)
    }

    func testPresentAlert_WhenCalled_ShouldDisplayAlert() {
        sut.presentAlert()

        XCTAssertEqual(displaySpy.displayAlertCallsCount, 1)
    }

    func testDidNextStep_WhenNextStep_ShouldGoToNextStep() {
        sut.didNextStep(action: .nextStep)

        XCTAssertEqual(coordinatorSpy.action, .nextStep)
    }
}
