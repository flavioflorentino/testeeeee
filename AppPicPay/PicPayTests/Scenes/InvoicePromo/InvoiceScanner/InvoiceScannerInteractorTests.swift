import XCTest
import Core
import PermissionsKit
@testable import PicPay

// MARK: - PresenterSpy
private final class InvoiceScannerPresenterSpy: InvoiceScannerPresenting {
    var viewController: InvoiceScannerDisplaying?

    // MARK: - didStartScanner
    private(set) var didStartScannerCount = 0

    func didStartScanner() {
        didStartScannerCount += 1
    }

    // MARK: - presentAlert
    private(set) var presentAlertCount = 0

    func presentAlert() {
        presentAlertCount += 1
    }

    // MARK: - didNextStep
    private(set) var didNextStepActionCallsCount = 0
    private(set) var didNextStepActionReceivedInvocations: [InvoiceScannerAction] = []

    func didNextStep(action: InvoiceScannerAction) {
        didNextStepActionCallsCount += 1
        didNextStepActionReceivedInvocations.append(action)
    }
}

private final class InvoiceScannerServiceMock: InvoiceScannerServicing {
    var requestCameraPermissionResult: PermissionStatus = .authorized
    var sendQRCodeResult: Result<NoContent, ApiError> = .failure(ApiError.serverError)

    func requestCameraPermission(completion: @escaping (_ permission: PermissionStatus) -> Void) {
        completion(requestCameraPermissionResult)
    }

    func sendQRCode(_ scannerText: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        completion(sendQRCodeResult)
    }
}

final class InvoiceScannerInteractorTests: XCTestCase {
    private let presenterSpy = InvoiceScannerPresenterSpy()
    private let serviceMock = InvoiceScannerServiceMock()

    private lazy var sut = InvoiceScannerInteractor(service: serviceMock, presenter: presenterSpy)

    func testTryStartScanner_WhenAuthorized_ShouldStartScanner() {
        serviceMock.requestCameraPermissionResult = .authorized

        sut.tryStartScanner()

        XCTAssertEqual(presenterSpy.didStartScannerCount, 1)
    }

    func testOnReadQRCode_WhenSuccess_ShouldNextStep() {
        serviceMock.sendQRCodeResult = .success(NoContent())

        sut.onReadQRCode(scannerText: "Teste")

        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.first, InvoiceScannerAction.nextStep)
    }

    func testOnReadQRCode_WhenFailure_ShouldPresentAlert() {
        serviceMock.sendQRCodeResult = .failure(ApiError.serverError)

        sut.onReadQRCode(scannerText: "Teste")

        XCTAssertEqual(presenterSpy.presentAlertCount, 1)
    }

    func testClose_WhenNeedCloseView_ShouldDidNextStep() {
        sut.close()

        XCTAssertEqual(presenterSpy.didNextStepActionCallsCount, 1)
        XCTAssertEqual(presenterSpy.didNextStepActionReceivedInvocations.first, InvoiceScannerAction.close)
    }
}
