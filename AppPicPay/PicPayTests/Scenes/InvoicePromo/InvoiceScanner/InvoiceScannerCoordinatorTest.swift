import XCTest
@testable import PicPay

class InvoiceScannerCoordinatorTest: XCTestCase {
    private lazy var viewControllerMock = ViewControllerMock()
    private lazy var navigationMock = NavigationControllerMock(rootViewController: viewControllerMock)

    private lazy var sut: InvoiceScannerCoordinating = {
        let coordinator = InvoiceScannerCoordinator()
        coordinator.viewController = navigationMock.topViewController
        return coordinator
    }()

    func testPerform_WhenClose_ShouldDismissView() throws {
        sut.perform(action: .close)

        XCTAssertEqual(viewControllerMock.didDismissControllerCount, 1)
    }
}
