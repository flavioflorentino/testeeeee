@testable import PicPay

extension FavoritesAction: Equatable {
    public static func == (lhs: FavoritesAction, rhs: FavoritesAction) -> Bool {
        switch (lhs, rhs) {
        case (.close, .close):
            return true
        case (.payment(let lType), .payment(let rType)):
            return lType == rType
        default:
            return false
        }
    }
}

extension FavoritePaymentType: Equatable {
    public static func == (lhs: FavoritePaymentType, rhs: FavoritePaymentType) -> Bool {
        switch (lhs, rhs) {
        case let (.person(lId), .person(rId)):
            return lId == rId
            
        case let (.store(lDictionary), .store(rDictionary)):
            guard
                let lDic = lDictionary,
                let rDic = rDictionary
                else {
                return false
            }
            
            return NSDictionary(dictionary: lDic).isEqual(to: rDic)

        case let (.digitalGood(lhsItem), .digitalGood(rhsItem)):
            return lhsItem == rhsItem
        default:
            return false
        }
    }
}
