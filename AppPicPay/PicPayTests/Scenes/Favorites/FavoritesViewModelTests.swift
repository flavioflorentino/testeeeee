import XCTest
@testable import PicPay

class FavoritesViewModelTests: XCTestCase {
    var sut: FavoritesViewModel!
    var serviceMock: FavoritesServiceMock!
    var presenterMock: FavoritesPresenterMock!
    
    func testWhenLoadDataShouldConfigureListFavorites() {
        setUpTest(status: .success)
        
        sut.loadData()
        
        XCTAssertEqual(serviceMock.favoritesCalled, 1)
        XCTAssertEqual(presenterMock.listFavoritesCalled, 1)
        XCTAssertEqual(presenterMock.items.count, 1)
    }
    
    func testWhenLoadDataShouldFailureRequest() {
        setUpTest(status: .failure)
        
        sut.loadData()
        
        XCTAssertEqual(serviceMock.favoritesCalled, 1)
        XCTAssertEqual(presenterMock.popupErrorCalled, 1)
        XCTAssertEqual(presenterMock.items.count, 0)
    }
    
    func testWhenDidSelectFavoriteShouldSelectedItem() {
        setUpTest(status: .success)
        
        sut.loadData()
        sut.didSelectFavorite(index: 0)
        
        XCTAssertEqual(presenterMock.didNextStepCalled, 1)
    }
    
    func testWhenCloseShouldCallDidNextStep() {
        setUpTest(status: .success)
        
        sut.close()
        
        XCTAssertEqual(presenterMock.didNextStepCalled, 1)
    }
}

extension FavoritesViewModelTests {
    private enum FavoritesTestsStatus {
        case success
        case failure
    }
    
    private func setUpTest(status: FavoritesTestsStatus) {
        switch status {
        case .success:
            serviceMock = FavoritesServiceMock(status: .success)
        case .failure:
            serviceMock = FavoritesServiceMock(status: .failure)
        }
        
        presenterMock = FavoritesPresenterMock()
        sut = FavoritesViewModel(service: serviceMock, presenter: presenterMock)
    }
}
