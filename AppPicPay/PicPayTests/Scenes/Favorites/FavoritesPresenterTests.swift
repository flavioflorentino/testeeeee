import XCTest
@testable import PicPay

final class FavoritesPresenterTests: XCTestCase {
    var sut: FavoritesPresenter!
    var coordinatorMock: FavoritesCoordinatorMock!
    var fakeDisplay: FakeFavoriteDisplay!
    var queue: DispatchQueue!
    
    override func setUp() {
        super.setUp()
        
        queue = DispatchQueue(label: #function)
        coordinatorMock = FavoritesCoordinatorMock()
        fakeDisplay = FakeFavoriteDisplay()
        sut = FavoritesPresenter(coordinator: coordinatorMock, queue: queue)
        sut.viewController = fakeDisplay
    }
    
    func testWhenDidNextStepShouldSentActionCoordinator() {
        sut.didNextStep(action: .close)
       
        let expectation = self.expectation(description: "action coordinator")
        
        queue.async {
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 1)
        
        XCTAssertTrue(coordinatorMock.action == .close)
    }
    
    func testWhenListFavoritesShouldReturnConfigureCellViewModel() {
        sut.listFavorites(favoriteItems())
        
        let expectation = self.expectation(description: "list favorites")
        
        queue.async {
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 1)
        
        XCTAssertEqual(fakeDisplay.listFavoritesCalled, 1)
        XCTAssertTrue(fakeDisplay.hasFavoritesCellViewModel)
    }
    
    func testeWhenPopupErrorShouldConfigureMessage() {
        sut.popupError()
        
        let expectation = self.expectation(description: "popup error")
        
        queue.async {
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 1)
        
        XCTAssertEqual(fakeDisplay.didFavoriteErrorCalled, 1)
    }
}

extension FavoritesPresenterTests {
    private func favoriteItems() -> [Favorite] {
        let file = Bundle(for: type(of: self)).path(forResource: "favorites", ofType: "json")
        let data = try! Data(contentsOf: URL(fileURLWithPath: file!))
        let items = try! JSONDecoder().decode([Favorite].self, from: data)
        return items
    }
}
