import FeatureFlag
import XCTest

@testable import PicPay

final class FavoriteInteractorTests: XCTestCase {
    private let featureManagerMock = FeatureManagerMock()
    private let serviceMock = FavoritesServiceMock(status: .success)

    private lazy var sut = FavoriteInteractor(service: serviceMock, dependencies: DependencyContainerMock(featureManagerMock))

    override func setUp() {
        super.setUp()
    }

    func testCheckState_WhenServiceReturnsTrue_ShouldCallCompletionPassingFavorited() {
        var state: FavoriteState?
        sut.checkState(id: "1", type: .consumer) { state = $0 }

        XCTAssertEqual(serviceMock.isFavoriteCalled, 1)
        XCTAssertEqual(state, FavoriteState.favorited)
    }

    func testCheckState_WhenServiceReturnsFalse_ShouldCallCompletionPassingUnfavorited() {
        serviceMock.status = .failure

        var state: FavoriteState?
        sut.checkState(id: "1", type: .consumer) { state = $0 }

        XCTAssertEqual(serviceMock.isFavoriteCalled, 1)
        XCTAssertEqual(state, FavoriteState.unfavorited)
    }

    func testChangeState_WhenPassedUnavailable_ShouldCallCompletionPassingUnavailable() {
        var state: FavoriteState?
        sut.changeState(.unavailable, id: "1", type: .consumer) { state = $0 }

        XCTAssertEqual(state, FavoriteState.unavailable)
    }

    func testChangeState_WhenPassedUnfavoritedAndServiceSucceed_ShouldCallCompletionPassingFavorited() {
        var state: FavoriteState?
        sut.changeState(.unfavorited, id: "1", type: .consumer) { state = $0 }

        XCTAssertEqual(state, FavoriteState.favorited)
        XCTAssertEqual(serviceMock.favoriteCalled, 1)
    }

    func testChangeState_WhenPassedUnfavoritedAndServiceFailed_ShouldCallCompletionPassingUnfavorited() {
        serviceMock.status = .failure

        var state: FavoriteState?
        sut.changeState(.unfavorited, id: "1", type: .consumer) { state = $0 }

        XCTAssertEqual(state, FavoriteState.unfavorited)
        XCTAssertEqual(serviceMock.favoriteCalled, 1)
    }

    func testChangeState_WhenPassedFavoritedAndServiceSucceed_ShouldCallCompletionPassingUnfavorited() {
        var state: FavoriteState?
        sut.changeState(.favorited, id: "1", type: .consumer) { state = $0 }

        XCTAssertEqual(state, FavoriteState.unfavorited)
        XCTAssertEqual(serviceMock.unfavoriteCalled, 1)
    }

    func testChangeState_WhenPassedFavoritedAndServiceFailed_ShouldCallCompletionPassingFavorited() {
        serviceMock.status = .failure

        var state: FavoriteState?
        sut.changeState(.favorited, id: "1", type: .consumer) { state = $0 }

        XCTAssertEqual(state, FavoriteState.favorited)
        XCTAssertEqual(serviceMock.unfavoriteCalled, 1)
    }
}
