import XCTest
@testable import PicPay

final class FavoriteCellPresenterTests: XCTestCase {
    var sut: FavoriteCellPresenter!
    var fakeDisplay: FakeFavoriteCellDisplay!
    
    override func setUp() {
        super.setUp()
        sut = FavoriteCellPresenter()
        fakeDisplay = FakeFavoriteCellDisplay()
        sut.viewController = fakeDisplay
    }
    
    func testWhenConfigureViewShouldReturnCellConfigured() {
        sut.configureView(item: favoriteItem())
    
        XCTAssertTrue(fakeDisplay.isConfigureViewCalled)
    }
    
    func testWhenDidFavoriteShouldCallDidFavorite() {
        sut.didFavorite()
        
        XCTAssertTrue(fakeDisplay.isDidFavoriteCalled)
    }
    
    func testWhenDidUnfavoriteShouldCallDidUnfavorite() {
        sut.didUnfavorite()
        
        XCTAssertTrue(fakeDisplay.isDidUnfavoriteCalled)
    }
    
    func testWhenConfigureViewShouldCalldisplayBadgeVerified() {
        sut.configureView(item: favoriteItem())

        XCTAssertTrue(fakeDisplay.isDisplayBadgeVerified)
    }
    
    func testWhenConfigureViewShouldCalldisplayBadgeVerifiedWithValue() {
        sut.configureView(item: favoriteItem())

        XCTAssertTrue(fakeDisplay.isDisplayBadgeVerified)
    }
    
}

private extension FavoriteCellPresenterTests {
    func favoriteItem() -> Favorite {
        let file = Bundle(for: type(of: self)).path(forResource: "favorites", ofType: "json")
        let data = try! Data(contentsOf: URL(fileURLWithPath: file!))
        let items = try! JSONDecoder().decode([Favorite].self, from: data)
        return items.first!
    }
}
