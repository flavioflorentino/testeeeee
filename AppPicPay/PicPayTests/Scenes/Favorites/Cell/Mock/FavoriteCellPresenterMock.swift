@testable import PicPay

final class FavoriteCellPresenterMock: FavoriteCellPresenting {
    public private(set) var configureViewCalled: Int = 0
    public private(set) var didFavoriteCalled: Int = 0
    public private(set) var didUnfavoriteCalled: Int = 0
    
    public var viewController: FavoriteCellDisplay?
    
    func configureView(item: Favorite) {
        configureViewCalled += 1
    }
    
    func didFavorite() {
        didFavoriteCalled += 1
    }
    
    func didUnfavorite() {
        didUnfavoriteCalled += 1
    }
}
