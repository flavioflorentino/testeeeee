@testable import PicPay

final class FakeFavoriteCellDisplay: FavoriteCellDisplay {
    private(set) var isConfigureViewCalled: Bool = false
    private(set) var isDidFavoriteCalled: Bool = false
    private(set) var isDidUnfavoriteCalled: Bool = false
    private(set) var isDisplayBadgeVerified: Bool = false
    
    func configureView(imageUrl: String?, placeholderImage: UIImage?, title: String, description: String?, isAction: Bool) {
        isConfigureViewCalled = true
    }
    
    func didFavorite() {
        isDidFavoriteCalled = true
    }
    
    func didUnfavorite() {
        isDidUnfavoriteCalled = true
    }
    
    func displayBadgeVerified(_ hiddenVerified: Bool) {
        isDisplayBadgeVerified = true
    }
}
