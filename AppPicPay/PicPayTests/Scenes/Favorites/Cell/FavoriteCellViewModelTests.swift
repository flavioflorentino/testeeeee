import XCTest
@testable import PicPay

final class FavoriteCellViewModelTests: XCTestCase {
    var sut: FavoriteCellViewModel!
    var serviceMock: FavoritesServiceMock!
    var presenterMock: FavoriteCellPresenterMock!
    
    override func setUp() {
        super.setUp()
        
        serviceMock = FavoritesServiceMock()
        presenterMock = FavoriteCellPresenterMock()
        
        sut = FavoriteCellViewModel(
            service: serviceMock,
            presenter: presenterMock,
            item: favoriteItem()
        )
    }
    
    func testWhenConfigureShouldCallConfigureView() {
        sut.configure()
        
        XCTAssertEqual(presenterMock.configureViewCalled, 1)
    }
    
    func testWhenUnfavoriteItemShouldCallUnfavoriteMethods() {
        sut.favoriteOrUnfavorite()
        
        XCTAssertEqual(serviceMock.unfavoriteCalled, 1)
        XCTAssertEqual(presenterMock.didUnfavoriteCalled, 1)
    }
    
    func testWhenFavoriteItemShouldCallFavoriteMethods() {
        sut.favoriteOrUnfavorite()
        sut.favoriteOrUnfavorite()
        
        XCTAssertEqual(serviceMock.favoriteCalled, 1)
        XCTAssertEqual(presenterMock.didFavoriteCalled, 1)
    }
}

private extension FavoriteCellViewModelTests {
    func favoriteItem() -> Favorite {
        let file = Bundle(for: type(of: self)).path(forResource: "favorites", ofType: "json")
        let data = try! Data(contentsOf: URL(fileURLWithPath: file!))
        let items = try! JSONDecoder().decode([Favorite].self, from: data)
        return items.first!
    }
}
