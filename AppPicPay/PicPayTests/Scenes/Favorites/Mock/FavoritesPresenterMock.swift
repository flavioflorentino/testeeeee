@testable import PicPay

final class FavoritesPresenterMock: FavoritesPresenting {
    private(set) var items: [Favorite] = []
    
    private(set) var popupErrorCalled: Int = 0
    private(set) var didNextStepCalled: Int = 0
    private(set) var listFavoritesCalled: Int = 0
    
    var viewController: FavoritesDisplay?
    
    func popupError() {
        popupErrorCalled += 1
    }
    
    func didNextStep(action: FavoritesAction) {
        didNextStepCalled += 1
    }
    
    func listFavorites(_ items: [Favorite]) {
        listFavoritesCalled += 1
        self.items = items
    }
}
