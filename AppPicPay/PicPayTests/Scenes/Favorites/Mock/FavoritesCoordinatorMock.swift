@testable import PicPay

final class FavoritesCoordinatorMock: FavoritesCoordinating {
    var viewController: UIViewController?
    weak var delegate: FavoritesDelegate?
    
    private(set) var action: FavoritesAction!
    
    func perform(action: FavoritesAction) {
        self.action = action
    }
}
