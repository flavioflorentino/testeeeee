@testable import PicPay
import UI

public class FakeFavoriteDisplay: FavoritesDisplay {
    public private(set) var listFavoritesCalled: Int = 0
    public private(set) var hasFavoritesCellViewModel: Bool = false
    
    public private(set) var didFavoriteErrorCalled: Int = 0
    
    public func listFavorites(_ favorites: [Section<String, FavoriteCellViewModel>]) {
        listFavoritesCalled += 1
        hasFavoritesCellViewModel = favorites.count > 0
    }
    
    public func didFavoriteError(
        image: UIImage?,
        content: (title: String, description: String),
        button: (image: UIImage?, title: String)
    ) {
        didFavoriteErrorCalled += 1
    }
}
