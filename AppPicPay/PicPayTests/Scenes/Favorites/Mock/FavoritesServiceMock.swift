import Core
import Dispatch

@testable import PicPay

final class FavoritesServiceMock {
    enum FavoritesServiceStatus {
        case success
        case failure
    }

    var status: FavoritesServiceStatus

    private(set) var favoritesCalled: Int = 0
    private(set) var favoriteCalled: Int = 0
    private(set) var unfavoriteCalled: Int = 0
    private(set) var isFavoriteCalled: Int = 0
    
    init(status: FavoritesServiceStatus = .success) {
        self.status = status
    }
}

extension FavoritesServiceMock: FavoritesServicing {
    func favorites(completion: @escaping (Result<[Favorite], ApiError>) -> Void) {
        favoritesCalled += 1

        switch status {
        case .success:
            let file = Bundle(for: type(of: self)).path(forResource: "favorites", ofType: "json")
            let data = try! Data(contentsOf: URL(fileURLWithPath: file!))
            let items = try! JSONDecoder().decode([Favorite].self, from: data)
            
            completion(.success(items))
        case .failure:
            completion(.failure(.connectionFailure))
        }
    }

    func homeFavorites(completion: @escaping (Result<[HomeFavorites], ApiError>) -> Void) {
        completion(.failure(.connectionFailure))
    }

    func favorite(id: String, type: Favorite.`Type`, completion: @escaping (Bool) -> Void) {
        favoriteCalled += 1
        completion(status == .success)
    }

    func unfavorite(id: String, type: Favorite.`Type`, completion: @escaping (Bool) -> Void) {
        unfavoriteCalled += 1
        completion(status == .success)
    }

    func isFavorite(id: String, type: Favorite.`Type`, completion: @escaping (Bool) -> Void) {
        isFavoriteCalled += 1
        completion(status == .success)
    }
}
