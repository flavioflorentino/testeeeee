@testable import PicPay
import XCTest

private final class RechargeValueCoordinatorSpy: RechargeValueCompletionCoordinating {
    var didCompletePaymentCount = 0

    func didCompletePayment(with info: RechargeFeeInfo) {
        didCompletePaymentCount += 1
    }
}

private final class RechargeTransferSummaryCoordinatorSpy: RechargeTransferSummaryCoordinating {
    private weak var rechargeCoordinator: RechargeValueCompletionCoordinating?
    weak var viewController: UIViewController?
    var calledPerformCount = 0
    var calledAction: RechargeTransferSummaryAction?

    init(rechargeCoordinator: RechargeValueCompletionCoordinating) {
        self.rechargeCoordinator = rechargeCoordinator
    }

    func perform(action: RechargeTransferSummaryAction) {
        switch action {
        case .continuePayment:
            let feeInfo = RechargeFeeInfo(chargedValue: "", serviceFee: "")
            rechargeCoordinator?.didCompletePayment(with: feeInfo)
            fallthrough

        default:
            calledPerformCount += 1
            calledAction = action
        }
    }
}

class RechargeTransferSummaryPresenterTests: XCTestCase {
    private let completionCoordinatorSpy = RechargeValueCoordinatorSpy()
    private lazy var coordinatorSpy = RechargeTransferSummaryCoordinatorSpy(rechargeCoordinator: completionCoordinatorSpy)
    private var sut: RechargeTransferSummaryPresenting?

    override func setUp() {
        super.setUp()
        sut = RechargeTransferSummaryPresenter(coordinator: coordinatorSpy)
    }

    func testDidNextStep_WhenCallContinuePayment_shouldCallDidCompletePayment() throws {
        let validInfo = RechargeFeeInfo(chargedValue: "", serviceFee: "")
        let validAction = RechargeTransferSummaryAction.continuePayment(validInfo)
        sut?.didNextStep(action: validAction)

        XCTAssertEqual(coordinatorSpy.calledPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.calledAction, validAction)
        XCTAssertEqual(completionCoordinatorSpy.didCompletePaymentCount, 1)
    }

    func testDidNextStep_WhenCallNotNow_shouldCallNotNow() throws {
        sut?.didNextStep(action: .notNow)

        XCTAssertEqual(coordinatorSpy.calledPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.calledAction, RechargeTransferSummaryAction.notNow)
    }

    func testDidNextStep_WhenCallHelpCenter_shouldCallHelpCenter() throws {
        let validAction: RechargeTransferSummaryAction = .helpCenter(articleId: "123456")
        sut?.didNextStep(action: validAction)

        XCTAssertEqual(coordinatorSpy.calledPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.calledAction, validAction)
    }
}

extension RechargeTransferSummaryAction : Equatable {
    public static func == (lhs: RechargeTransferSummaryAction, rhs: RechargeTransferSummaryAction) -> Bool {
        switch (lhs, rhs) {
        case (.continuePayment, .continuePayment):
            return true
        case (.notNow, .notNow):
            return true
        case (.helpCenter, .helpCenter):
            return true
        default:
            return false
        }
    }
}
