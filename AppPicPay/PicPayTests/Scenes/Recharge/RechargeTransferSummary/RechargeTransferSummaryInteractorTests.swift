@testable import PicPay
import Core
import FeatureFlag
import XCTest

private final class RechargeTransferSummaryPresenterSpy: RechargeTransferSummaryPresenting {
    var viewController: RechargeTransferSummaryDisplaying?
    var displayedSummary: TransferSummary?
    var calledDisplaySummaryCount = 0
    var calledDidNextStep = 0
    var calledPresentErrorCount = 0
    var calledStartLoadingCount = 0
    var calledStopLoadingCount = 0

    func displaySummary(_ summary: TransferSummary) {
        calledDisplaySummaryCount += 1
        displayedSummary = summary
    }

    func didNextStep(action: RechargeTransferSummaryAction) {
        calledDidNextStep  += 1
    }

    func presentError() {
        calledPresentErrorCount += 1
    }

    func startLoading() {
        calledStartLoadingCount += 1
    }

    func stopLoading() {
        calledStopLoadingCount += 1
    }
}

private final class MockRechargeTransferSummaryService: RechargeTransferSummaryServicing {
    var isSuccess = true

    var summaryErrorResult: Result<TransferSummary, ApiError> = .failure(ApiError.serverError)
    var result: Result<TransferSummary, ApiError> = .failure(ApiError.serverError)

    func fetchSummaryInfo(rechargeValue: Double, completion: @escaping (Result<TransferSummary, ApiError>) -> Void) {
        completion(result)
    }

}

final class RechargeTransferSummaryInteractorTests: XCTestCase {
    typealias Dependencies = HasFeatureManager
    private let featureManager = FeatureManagerMock()
    private lazy var dependencies: Dependencies = DependencyContainerMock(featureManager)
    private lazy var presenterSpy = RechargeTransferSummaryPresenterSpy()
    private lazy var serviceMock = MockRechargeTransferSummaryService()
    private let summary = TransferSummary(serviceTax: 1.99, rechargeValue: 20.00, serviceTaxValue: "0.39", rechargeFinalValue: "19.61")

    func testFetchSummaryInfo_ShouldCallFetchSummaryInfo() throws {
        serviceMock.result = .success(summary)
        let sut = RechargeTransferSummaryInteractor(service: serviceMock, presenter: presenterSpy,
                                                    dependencies: dependencies, rechargeValue: 20.00)


        sut.fetchSummaryInfo()

        XCTAssertEqual(presenterSpy.calledStartLoadingCount, 1)
        XCTAssertEqual(presenterSpy.calledDisplaySummaryCount, 1)
        XCTAssertEqual(presenterSpy.displayedSummary, summary)
        XCTAssertEqual(presenterSpy.calledStopLoadingCount, 1)
    }

    func testPresentError_ShouldCallShowPopupError() throws {
        serviceMock.result = .failure(ApiError.serverError)

        let sut = RechargeTransferSummaryInteractor(service: serviceMock, presenter: presenterSpy,
                                                    dependencies: dependencies, rechargeValue: 20.00)

        sut.fetchSummaryInfo()

        XCTAssertEqual(presenterSpy.calledStartLoadingCount, 1)
        XCTAssertEqual(presenterSpy.calledPresentErrorCount, 1)
        XCTAssertEqual(presenterSpy.calledStopLoadingCount, 0)
    }

    func testGoToHelpCenter_ShouldCallGoToHelpCenter() throws {
        featureManager.override(key: .opsHelpRechargeTransactionSummaryString, with: "https://app.picpay.com/helpcenter?query=adicionar-dinheiro-no-picpay")
        let sut = RechargeTransferSummaryInteractor(service: serviceMock, presenter: presenterSpy,
                                                    dependencies: dependencies, rechargeValue: 20.00)

        sut.goToHelpCenter()

        XCTAssertEqual(presenterSpy.calledDidNextStep, 1)
    }

    func testDidTapContinue_WhenDidTapContinue_ShouldCallContinuePayment() throws {
        serviceMock.result = .success(summary)
        let sut = RechargeTransferSummaryInteractor(service: serviceMock, presenter: presenterSpy,
                                                    dependencies: dependencies, rechargeValue: 20.00)

        sut.fetchSummaryInfo()
        sut.didTapContinue()

        XCTAssertEqual(presenterSpy.calledDidNextStep, 1)
    }

    func testDidTapNotNow_WhenDidTapNotNow_ShouldCallNotNow() throws {
        let sut = RechargeTransferSummaryInteractor(service: serviceMock, presenter: presenterSpy,
                                                    dependencies: dependencies, rechargeValue: 20.00)

        sut.didTapNotNow()

        XCTAssertEqual(presenterSpy.calledDidNextStep, 1)
    }
}

extension TransferSummary: Equatable {
    public static func == (lhs: TransferSummary, rhs: TransferSummary) -> Bool {
        return lhs.serviceTax == rhs.serviceTax &&
            lhs.rechargeValue == rhs.rechargeValue &&
            lhs.serviceTaxValue == rhs.serviceTaxValue &&
            lhs.rechargeFinalValue == rhs.rechargeFinalValue
    }
}
