import XCTest
import CashIn
import FeatureFlag
@testable import PicPay

final class RechargeLoadFactoryTest: XCTestCase {
    private lazy var featureManager = FeatureManagerMock()
    private lazy var dependenciesMock = DependencyContainerMock(featureManager)
    
    func testMake_whenRedesignFlagIsOff_ShouldMakeARechargeLoadViewController() {
        featureManager.override(key: .isCashInRedesignAvailable, with: false)
        let controller = RechargeLoadFactory.make(with: dependenciesMock)
        
        XCTAssertTrue(controller is RechargeLoadViewController)
    }
    
    func testMake_whenRedesignFlagIsOn_ShouldMakeACashInMethodsViewController() {
        featureManager.override(key: .isCashInRedesignAvailable, with: true)
        let controller = RechargeLoadFactory.make(with: dependenciesMock)
        
        XCTAssertFalse(controller is RechargeLoadViewController)
    }
}
