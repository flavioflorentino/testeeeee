import XCTest
@testable import PicPay

class FakeRechargeLoadService: RechargeLoadServicing {
    private let response: Response
    
    init(response: Response) {
        self.response = response
    }
    
    func getRecharge(onSuccess: @escaping(Recharge?, [RechargeMethod]) -> Void, onError: @escaping (PicPayErrorDisplayable) -> Void) {
        switch response {
        case .rechargeIsNotEnable:
            onSuccess(nil, [])
            
        case .rechargeBill:
            let recharge = try! MockCodable<Recharge>().loadCodableObject(resource: "RechargeBill", typeDecoder: .useDefaultKeys)
            onSuccess(recharge, [])
            
        case .rechargeDeposit:
            let recharge = try! MockCodable<Recharge>().loadCodableObject(resource: "RechargeDeposit", typeDecoder: .useDefaultKeys)
            onSuccess(recharge, [])
            
        case .error:
            let picpayError = PicPayError(message: "Erro")
            onError(picpayError)
        }
    }
    
    enum Response {
        case rechargeIsNotEnable
        case rechargeBill
        case rechargeDeposit
        case error
    }
}

class FakeRechargeLoadViewController: RechargeLoadViewModelOutputs {
    var calledDidNextStep = false
    var didNextStepAction: RechargeLoadAction?
    var error: PicPayError?
    var calledDidReceivedAnError = false
    
    func didNextStep(action: RechargeLoadAction) {
        calledDidNextStep = true
        didNextStepAction = action
    }
    
    func didReceiveAnError(error: PicPayErrorDisplayable) {
        calledDidReceivedAnError = true
        self.error = error as? PicPayError
    }
}

class RechargeLoadViewModelTest: XCTestCase {
    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testViewDidLoadShouldReturnRechargeIsNotEnabled(){
        let service = FakeRechargeLoadService(response: .rechargeIsNotEnable)
        let viewModel = RechargeLoadViewModel(service: service)
        let mockViewController = FakeRechargeLoadViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        XCTAssert(mockViewController.calledDidNextStep)
        
        switch mockViewController.didNextStepAction! {
        case .rechargeMethods(let methods):
            XCTAssertTrue(true)
            XCTAssertNotNil(methods)
        default:
            XCTFail()
        }
    }
    
    func testViewDidLoadShouldReturnRechargeMethodBill(){
        let service = FakeRechargeLoadService(response: .rechargeBill)
        let viewModel = RechargeLoadViewModel(service: service)
        let mockViewController = FakeRechargeLoadViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        XCTAssert(mockViewController.calledDidNextStep)
        
        switch mockViewController.didNextStepAction! {
        case .rechargeBill(let recharge):
            XCTAssertTrue(true)
            XCTAssertNotNil(recharge)
        default:
            XCTFail()
        }
    }
    
    func testViewDidLoadShouldReturnRechargeMethodDeposit(){
        let service = FakeRechargeLoadService(response: .rechargeDeposit)
        let viewModel = RechargeLoadViewModel(service: service)
        let mockViewController = FakeRechargeLoadViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        XCTAssert(mockViewController.calledDidNextStep)
        
        switch mockViewController.didNextStepAction! {
        case .rechargeDeposit(let recharge):
            XCTAssertTrue(true)
            XCTAssertNotNil(recharge)
        default:
            XCTFail()
        }
    }
    
    func testViewDidLoadShouldReturnError(){
        let service = FakeRechargeLoadService(response: .error)
        let viewModel = RechargeLoadViewModel(service: service)
        let mockViewController = FakeRechargeLoadViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        XCTAssert(mockViewController.calledDidReceivedAnError)
        XCTAssertNotNil(mockViewController.error)
    }
}
