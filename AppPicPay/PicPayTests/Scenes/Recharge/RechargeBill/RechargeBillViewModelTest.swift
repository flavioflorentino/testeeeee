import CoreLegacy
import FeatureFlag
import XCTest
@testable import PicPay

class FakeRechargeBillService: RechargeBillServicing {
    private let successCancel: Bool
    private let successDownload: Bool
    
    init(successCancel: Bool = true, successDownload: Bool = true) {
        self.successCancel = successCancel
        self.successDownload = successDownload
    }
    
    func cancel(id: String, onSuccess: @escaping () -> Void, onError: @escaping (PicPayErrorDisplayable) -> Void) {
        if successCancel {
            onSuccess()
        } else {
            let error = PicPayError(message: "Erro")
            onError(error)
        }
    }
    
    func downloadBill(url: URL, onSuccess: @escaping (Data) -> Void, onError: @escaping (PicPayErrorDisplayable) -> Void) {
        if successDownload {
            let data = Data(base64Encoded: "DATA")!
            onSuccess(data)
        } else {
            let error = PicPayError(message: "Erro")
            onError(error)
        }
    }
}

class FakeRechargeBillViewController: RechargeBillViewModelOutputs {
    var calledDidNextStep = false
    var didNextStepAction: RechargeBillAction?
    var calledDidReceivedAnError = false
    var error: PicPayError?
    var calledDidReceiveAnData = false
    var data: Data?
    
    func didNextStep(action: RechargeBillAction) {
        calledDidNextStep = true
        didNextStepAction = action
    }
    
    func didReceiveAnError(error: PicPayErrorDisplayable) {
        calledDidReceivedAnError = true
        self.error = error as? PicPayError
    }
    
    func didReceiveAnData(data: Data) {
        calledDidReceiveAnData = true
        self.data = data
    }
}

class RechargeBillViewModelTest: XCTestCase {
    private lazy var featureManager = FeatureManagerMock()
    private lazy var dependenciesMock = DependencyContainerMock(featureManager)
    
    private func createViewModel(service: RechargeBillServicing) -> RechargeBillViewModel {
        let recharge = try! MockCodable<Recharge>().loadCodableObject(resource: "RechargeBill", typeDecoder: .useDefaultKeys)
        return RechargeBillViewModel(service: service, recharge: recharge, dependencies: dependenciesMock)
    }
    
    func testBillDateShouldReturnDateMock() {
        let service = FakeRechargeBillService()
        let viewModel = createViewModel(service: service)
        let mockViewController = FakeRechargeBillViewController()
        
        viewModel.outputs = mockViewController
        
        let result = viewModel.inputs.billDate
        XCTAssertEqual(result, "26/07/2019")
    }
    
    func testBillNumberShouldReturnNumberMock() {
        let service = FakeRechargeBillService()
        let viewModel = createViewModel(service: service)
        let mockViewController = FakeRechargeBillViewController()
        
        viewModel.outputs = mockViewController
        
        let result = viewModel.inputs.billNumber
        XCTAssertEqual(result, "21290.00119 21100.012109 04453.620264 1 79620000002000")
    }
    
    func testBillValueShouldReturnValueMock() {
        let service = FakeRechargeBillService()
        let viewModel = createViewModel(service: service)
        let mockViewController = FakeRechargeBillViewController()
        
        viewModel.outputs = mockViewController
        
        let result = viewModel.inputs.billValue
        XCTAssertEqual(result, CurrencyFormatter.brazillianRealString(from: 20.0))
    }
    
    func testInstructions_WheneverCalled_ShouldReturnCorrectText() {
        let service = FakeRechargeBillService()
        let viewModel = createViewModel(service: service)
        
        let validInstructions = "Uma cópia deste boleto foi enviada para o email diogotestando@idiogo.com.br. Este boleto não pode ser usado para fins de venda de produtos ou serviços e não deve ser repassado para terceiros."
        let result = viewModel.inputs.instructions
        XCTAssertEqual(result, validInstructions)
    }
    
    func testCloseShouldReturnActionClose() {
        let service = FakeRechargeBillService()
        let viewModel = createViewModel(service: service)
        let mockViewController = FakeRechargeBillViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.close()
        
        XCTAssert(mockViewController.calledDidNextStep)
        switch mockViewController.didNextStepAction! {
        case .close:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testCancelRechargeShouldCallCancelRecharge() {
        let service = FakeRechargeBillService(successCancel: true)
        let viewModel = createViewModel(service: service)
        let mockViewController = FakeRechargeBillViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.cancelRecharge()
        
        XCTAssert(mockViewController.calledDidNextStep)
        switch mockViewController.didNextStepAction! {
        case .cancelRecharge:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testCancelRechargeShouldCallDidReceivedAnError() {
        let service = FakeRechargeBillService(successCancel: false)
        let viewModel = createViewModel(service: service)
        let mockViewController = FakeRechargeBillViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.cancelRecharge()
        
        XCTAssert(mockViewController.calledDidReceivedAnError)
        XCTAssertNotNil(mockViewController.error)
    }
    
    func testPrintBillShouldCallDidReceiveAnData() {
        let service = FakeRechargeBillService(successDownload: true)
        let viewModel = createViewModel(service: service)
        let mockViewController = FakeRechargeBillViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.printBill()
        
        XCTAssert(mockViewController.calledDidReceiveAnData)
        XCTAssertNotNil(mockViewController.data)
    }
    
    func testPrintBillShouldCallDidReceivedAnError() {
        let service = FakeRechargeBillService(successDownload: false)
        let viewModel = createViewModel(service: service)
        let mockViewController = FakeRechargeBillViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.printBill()
        
        XCTAssert(mockViewController.calledDidReceivedAnError)
        XCTAssertNotNil(mockViewController.error)
    }
    
    func testShareItemsShouldReturnShareItemsMock() {
        let service = FakeRechargeBillService()
        let viewModel = createViewModel(service: service)
        let mockViewController = FakeRechargeBillViewController()
        
        viewModel.outputs = mockViewController
        let result = viewModel.inputs.shareItems()
        
        XCTAssertEqual(result[0] as! String, "Boleto para adicionar \(String(describing: CurrencyFormatter.brazillianRealString(from: 20.0)!)) na minha carteira PicPay.\n\nLinha digitável: 21290.00119 21100.012109 04453.620264 1 79620000002000\n\n")
        XCTAssertEqual((result[1] as! URL).absoluteString, "http://gateway.service.ppay.me/boleto/w3IDTwpe6CCNWBpkDZpWPmFyTUViM2pkS1FXbXFaZTNUUUUzektvaGR6UTRUSmxJc0hMa1dwN2JBWVE9?ext=pdf")
    }
    
    func testOpenUrlBillShouldCallOpenPDF() {
        let service = FakeRechargeBillService()
        let viewModel = createViewModel(service: service)
        let mockViewController = FakeRechargeBillViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.openUrlBill()
        
        XCTAssert(mockViewController.calledDidNextStep)
        switch mockViewController.didNextStepAction! {
        case .openPDF(let url):
            XCTAssertTrue(true)
            XCTAssertNotNil(url)
        default:
            XCTFail()
        }
    }
}
