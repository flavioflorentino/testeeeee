@testable import PicPay
import XCTest
import CoreLegacy

class MockRechargeStatusCoordinator: RechargeStatusCoordinating {
    var viewController: UIViewController?
    var action: RechargeStatusAction?
    func perform(action: RechargeStatusAction) {
        self.action = action
    }
}

class MockRechargeStatusViewController: RechargeStatusDisplay {
    var calledDisplayHeader = false
    var calledDisplayMainButton = false
    var calledDisplayAccessoryButton = false
    var calledDisplayFooter = false
    
    var image: UIImage?
    var title: String?
    var description: String?
    
    var titleMain: String?
    var isHiddenMain: Bool?
    var titleAccessory: String?
    var isHiddenAccessory: Bool?
    
    var value: String?
    var aliasCard: String?
    
    func displayHeader(image: UIImage, title: String, description: String) {
        calledDisplayHeader = true
        self.image = image
        self.title = title
        self.description = description
    }
    
    func displayMainButton(title: String, isHidden: Bool) {
        calledDisplayMainButton = true
        self.titleMain = title
        self.isHiddenMain = isHidden
    }
    
    func displayAccessoryButton(title: String, isHidden: Bool) {
        calledDisplayAccessoryButton = true
        self.titleAccessory = title
        self.isHiddenAccessory = isHidden
    }
    
    func displayFooter(value: String, aliasCard: String) {
        calledDisplayFooter = true
        self.value = value
        self.aliasCard = aliasCard
    }
}

class RechargeStatusPresenterTest: XCTestCase {
    private func createModel(type: RechargeStatus.StatusType = .success) -> RechargeStatus {
        return RechargeStatus(type: type, value: 32.00, aliasCard: "Teste Cartão")
    }
    
    func testViewDidLoadShouldCallDisplayFooter() {
        let model = createModel()
        let mockViewController = MockRechargeStatusViewController()
        let presenter = RechargeStatusPresenter(coordinator: MockRechargeStatusCoordinator())
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(model: model)
        XCTAssertTrue(mockViewController.calledDisplayFooter)
    }
    
    func testViewDidLoadShouldReturnFooterValues() {
        let model = createModel()
        let mockViewController = MockRechargeStatusViewController()
        let presenter = RechargeStatusPresenter(coordinator: MockRechargeStatusCoordinator())
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(model: model)
        
        let currency = CurrencyFormatter.brazillianRealString(from: NSNumber(value: model.value))
        XCTAssertEqual(mockViewController.value, currency)
        XCTAssertEqual(mockViewController.aliasCard, model.aliasCard)
    }
    
    func testViewDidLoadShouldCallDisplayHeader() {
        let model = createModel()
        let mockViewController = MockRechargeStatusViewController()
        let presenter = RechargeStatusPresenter(coordinator: MockRechargeStatusCoordinator())
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(model: model)
        XCTAssertTrue(mockViewController.calledDisplayHeader)
    }
    
    func testViewDidLoadShouldReturnDisplayHeaderValuesWhenSuccess() {
        let model = createModel(type: .success)
        let mockViewController = MockRechargeStatusViewController()
        let presenter = RechargeStatusPresenter(coordinator: MockRechargeStatusCoordinator())
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(model: model)
        
        XCTAssertEqual(mockViewController.image, #imageLiteral(resourceName: "ico_green_checkmark"))
        XCTAssertEqual(mockViewController.title, RechargeLocalizable.statusSuccessTitle.text)
        XCTAssertEqual(mockViewController.description, RechargeLocalizable.statusSuccessDescription.text)
    }
    
    func testViewDidLoadShouldReturnDisplayHeaderValuesWhenFailure() {
        let model = createModel(type: .failure(nil))
        let mockViewController = MockRechargeStatusViewController()
        let presenter = RechargeStatusPresenter(coordinator: MockRechargeStatusCoordinator())
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(model: model)
        
        XCTAssertEqual(mockViewController.image, #imageLiteral(resourceName: "sad-3"))
        XCTAssertEqual(mockViewController.title, RechargeLocalizable.statusFailureTitle.text)
        XCTAssertEqual(mockViewController.description, RechargeLocalizable.statusFailureDescription.text)
    }
    
    func testViewDidLoadShouldReturnDisplayHeaderValuesWhenFaq() {
        let model = createModel(type: .faq("Teste teste"))
        let mockViewController = MockRechargeStatusViewController()
        let presenter = RechargeStatusPresenter(coordinator: MockRechargeStatusCoordinator())
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(model: model)
        
        XCTAssertEqual(mockViewController.image, #imageLiteral(resourceName: "sad-3"))
        XCTAssertEqual(mockViewController.title, RechargeLocalizable.statusFailureTitle.text)
        XCTAssertEqual(mockViewController.description, "Teste teste")
    }
    
    func testViewDidLoadShouldCallDisplayMainButton() {
        let model = createModel()
        let mockViewController = MockRechargeStatusViewController()
        let presenter = RechargeStatusPresenter(coordinator: MockRechargeStatusCoordinator())
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(model: model)
        XCTAssertTrue(mockViewController.calledDisplayMainButton)
    }
    
    func testViewDidLoadShouldReturnDisplayMainButtonValuesWhenSuccess() {
        let model = createModel(type: .success)
        let mockViewController = MockRechargeStatusViewController()
        let presenter = RechargeStatusPresenter(coordinator: MockRechargeStatusCoordinator())
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(model: model)
        
        XCTAssertEqual(mockViewController.titleMain, RechargeLocalizable.backToStart.text)
        XCTAssertEqual(mockViewController.isHiddenMain, false)
    }
    
    func testViewDidLoadShouldReturnDisplayMainButtonValuesWhenFailure() {
        let model = createModel(type: .failure(nil))
        let mockViewController = MockRechargeStatusViewController()
        let presenter = RechargeStatusPresenter(coordinator: MockRechargeStatusCoordinator())
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(model: model)
        
        XCTAssertEqual(mockViewController.titleMain, DefaultLocalizable.tryAgain.text)
        XCTAssertEqual(mockViewController.isHiddenMain, false)
    }
    
    func testViewDidLoadShouldReturnDisplayMainButtonValuesWhenFaq() {
        let model = createModel(type: .faq("Teste Teste"))
        let mockViewController = MockRechargeStatusViewController()
        let presenter = RechargeStatusPresenter(coordinator: MockRechargeStatusCoordinator())
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(model: model)
        
        XCTAssertEqual(mockViewController.titleMain, RechargeLocalizable.knowMore.text)
        XCTAssertEqual(mockViewController.isHiddenMain, false)
    }
    
    func testViewDidLoadShouldCallDisplayAccessoryButton() {
        let model = createModel()
        let mockViewController = MockRechargeStatusViewController()
        let presenter = RechargeStatusPresenter(coordinator: MockRechargeStatusCoordinator())
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(model: model)
        XCTAssertTrue(mockViewController.calledDisplayAccessoryButton)
    }
    
    func testViewDidLoadShouldReturnDisplayAccessoryButtonValuesWhenSuccess() {
        let model = createModel(type: .success)
        let mockViewController = MockRechargeStatusViewController()
        let presenter = RechargeStatusPresenter(coordinator: MockRechargeStatusCoordinator())
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(model: model)
        
        XCTAssertEqual(mockViewController.titleAccessory, String())
        XCTAssertEqual(mockViewController.isHiddenAccessory, true)
    }
    
    func testViewDidLoadShouldReturnDisplayAccessoryButtonValuesWhenFailure() {
        let model = createModel(type: .failure(nil))
        let mockViewController = MockRechargeStatusViewController()
        let presenter = RechargeStatusPresenter(coordinator: MockRechargeStatusCoordinator())
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(model: model)
        
        XCTAssertEqual(mockViewController.titleAccessory, RechargeLocalizable.backToStart.text)
        XCTAssertEqual(mockViewController.isHiddenAccessory, false)
    }
    
    func testViewDidLoadShouldReturnDisplayAccessoryButtonValuesWhenFaq() {
        let model = createModel(type: .faq("Teste Teste"))
        let mockViewController = MockRechargeStatusViewController()
        let presenter = RechargeStatusPresenter(coordinator: MockRechargeStatusCoordinator())
        
        presenter.viewController = mockViewController
        presenter.viewDidLoad(model: model)
        
        XCTAssertEqual(mockViewController.titleAccessory, RechargeLocalizable.backToStart.text)
        XCTAssertEqual(mockViewController.isHiddenAccessory, false)
    }
    
    func testDidNextStepShouldCallClose() throws {
        let mockCoordinator = MockRechargeStatusCoordinator()
        let presenter = RechargeStatusPresenter(coordinator: mockCoordinator)
        
        presenter.didNextStep(action: .close)
        
        switch try mockCoordinator.action.safe() {
        case .close:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testDidNextStepShouldCallBack() throws {
        let mockCoordinator = MockRechargeStatusCoordinator()
        let presenter = RechargeStatusPresenter(coordinator: mockCoordinator)
        
        presenter.didNextStep(action: .back)
        
        switch try mockCoordinator.action.safe() {
        case .back:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testDidNextStepShouldCalloOpenFaq() throws {
        let mockCoordinator = MockRechargeStatusCoordinator()
        let presenter = RechargeStatusPresenter(coordinator: mockCoordinator)
        
        let url = try URL(string: "https://picpay.com/site/").safe()
        presenter.didNextStep(action: .openFaq(url))
        
        switch try mockCoordinator.action.safe() {
        case .openFaq:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
}
