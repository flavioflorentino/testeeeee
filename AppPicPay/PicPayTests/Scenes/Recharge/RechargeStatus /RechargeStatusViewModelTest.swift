@testable import PicPay
import XCTest

private final class MockRechargeStatusService: RechargeStatusServicing {
    var linkFaq: String {
        return "https://picpay.com/site"
    }
}

private final class MockRechargeStatusPresenter: RechargeStatusPresenting {
    var viewController: RechargeStatusDisplay?
    var calledViewDidLoad = false
    var calledDidNextStep = false
    
    var model: RechargeStatus?
    var action: RechargeStatusAction?
    
    func viewDidLoad(model: RechargeStatus) {
        calledViewDidLoad = true
        self.model = model
    }
    
    func didNextStep(action: RechargeStatusAction) {
        calledDidNextStep = true
        self.action = action
    }
}

private final class RechargeStatusViewModelTest: XCTestCase {
    private func createModel(type: RechargeStatus.StatusType = .success) -> RechargeStatus {
        return RechargeStatus(type: type, value: 32.00, aliasCard: "Teste Cartão")
    }
    
    func testViewDidLoadShouldCallPresenter() {
        let model = createModel()
        let mockPresenter = MockRechargeStatusPresenter()
        let service = MockRechargeStatusService()
        let viewModel = RechargeStatusViewModel(model: model, presenter: mockPresenter, service: service)
        
        viewModel.viewDidLoad()
        XCTAssertTrue(mockPresenter.calledViewDidLoad)
    }
    
    func testViewDidLoadShouldCallPresenterWithValues() throws {
        let model = createModel()
        let mockPresenter = MockRechargeStatusPresenter()
        let service = MockRechargeStatusService()
        let viewModel = RechargeStatusViewModel(model: model, presenter: mockPresenter, service: service)
        
        viewModel.viewDidLoad()
        XCTAssertEqual(mockPresenter.model?.value, model.value)
        XCTAssertEqual(mockPresenter.model?.aliasCard, model.aliasCard)
        
        switch try mockPresenter.model.safe().type {
        case .success:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testDidTapCloseShouldCallDidNextStepColse() throws {
        let model = createModel()
        let mockPresenter = MockRechargeStatusPresenter()
        let service = MockRechargeStatusService()
        let viewModel = RechargeStatusViewModel(model: model, presenter: mockPresenter, service: service)
        
        viewModel.didTapClose()
        XCTAssertTrue(mockPresenter.calledDidNextStep)
        switch try mockPresenter.action.safe() {
        case .close:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testDidTapAccessoryButtonShouldCallDidNextStepColse() throws {
        let model = createModel()
        let mockPresenter = MockRechargeStatusPresenter()
        let service = MockRechargeStatusService()
        let viewModel = RechargeStatusViewModel(model: model, presenter: mockPresenter, service: service)
        
        viewModel.didTapAccessoryButton()
        XCTAssertTrue(mockPresenter.calledDidNextStep)
        switch try mockPresenter.action.safe() {
        case .close:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testDidTapMainButtonShouldCallDidNextStepClose() throws {
        let model = createModel(type: .success)
        let mockPresenter = MockRechargeStatusPresenter()
        let service = MockRechargeStatusService()
        let viewModel = RechargeStatusViewModel(model: model, presenter: mockPresenter, service: service)
        
        viewModel.didTapMainButton()
        XCTAssertTrue(mockPresenter.calledDidNextStep)
        switch try mockPresenter.action.safe() {
        case .close:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testDidTapMainButtonShouldCallDidNextStepBack() throws {
        let model = createModel(type: .failure(nil))
        let mockPresenter = MockRechargeStatusPresenter()
        let service = MockRechargeStatusService()
        let viewModel = RechargeStatusViewModel(model: model, presenter: mockPresenter, service: service)
        
        viewModel.didTapMainButton()
        XCTAssertTrue(mockPresenter.calledDidNextStep)
        switch try mockPresenter.action.safe() {
        case .back:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testDidTapMainButtonShouldCallDidNextStepOpenFaq() throws {
        let model = createModel(type: .faq("https://picpay.com/site/"))
        let mockPresenter = MockRechargeStatusPresenter()
        let service = MockRechargeStatusService()
        let viewModel = RechargeStatusViewModel(model: model, presenter: mockPresenter, service: service)
        
        viewModel.didTapMainButton()
        XCTAssertTrue(mockPresenter.calledDidNextStep)
        switch try mockPresenter.action.safe() {
        case .openFaq:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
}
