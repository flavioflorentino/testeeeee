@testable import PicPay
import XCTest
import FeatureFlag

final class RechargeOriginalCoordinatorTest: XCTestCase {
    private var spy = UIApplicationSpy()
    private var featureManagerMock = FeatureManagerMock()
    private lazy var coordinator: RechargeOriginalCoordinator = { RechargeOriginalCoordinator(dependencies:DependencyContainerMock(featureManagerMock), application: spy) }()
    
    func testPerformAction_DownloadOriginal_ShouldOpenUrl() throws {
        // Given
        let validUrl = "www.apple.com"
        featureManagerMock.override(key: .featureOriginalDeeplink, with: validUrl)
        
        // When
        coordinator.perform(action: .downloadOriginal)
        
        // Then
        let spyUrl = try XCTUnwrap(spy.openUrl)
        XCTAssertEqual(spyUrl.description, validUrl)
    }
    
    func testPerformAction_InvalidUrlForDownloadOriginal_ShouldNotify() {
        // Given
        spy.openUrl = nil
        let inValidUrl = "oogle .com"
        featureManagerMock.override(key: .featureOriginalDeeplink, with: inValidUrl)
        
        // When
        coordinator.perform(action: .downloadOriginal)
        
        // Then
        XCTAssertNil(spy.openUrl)
    }
    
    func testPerformAction_Close_ShouldCallDismissOnController() {
        // Given
        let controller = UIViewController()
        coordinator.viewController = controller
        let nav = UINavigationController(rootViewController: UIViewController())
        nav.present(controller, animated: false, completion: nil)
        
        // When
        coordinator.perform(action: .close)
        
        // Then
        XCTAssertFalse(controller.isBeingPresented)
    }
}
