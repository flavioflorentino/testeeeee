@testable import PicPay
import XCTest

class FakeRechargeOriginalService: RechargeOriginalServicing {
    var onboardingTitle: String
    var onboardingDescription: String
    
    init(title: String = "", description: String = "") {
        onboardingTitle = title
        onboardingDescription = description
    }
}

class FakeRechargeOriginalViewController: RechargeOriginalViewModelOutputs {
    var calledSendToAnalytics = false
    var swipesNumber: Int?
    var calledDidNextStep = false
    var didNextStepAction: RechargeOriginalAction?
    var calledSignUp = false
    var calledReloadData = false
    
    func sendToAnalytics(swipes: Int) {
        calledSendToAnalytics = true
        swipesNumber = swipes
    }
    
    func didNextStep(action: RechargeOriginalAction) {
        calledDidNextStep = true
        didNextStepAction = action
    }
    
    func signUp() {
        calledSignUp = true
    }
    
    func reloadData() {
        calledReloadData = true
    }
}

class RechargeOriginalViewModelTest: XCTestCase {
    func testViewDidLoadShouldCallReloadData() {
        let service = FakeRechargeOriginalService()
        let viewModel = RechargeOriginalViewModel(service: service)
        let mockViewController = FakeRechargeOriginalViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        XCTAssertTrue(mockViewController.calledReloadData)
    }
    
    func testNumberOfRowsReturn3() {
        let service = FakeRechargeOriginalService(description: "Teste")
        let viewModel = RechargeOriginalViewModel(service: service)
        let mockViewController = FakeRechargeOriginalViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        let result = viewModel.inputs.numberOfRows()
        
        XCTAssertEqual(result, 3)
    }
    
    func testCellModelReturnModel() {
        let service = FakeRechargeOriginalService(title: "Titulo", description: "Desc")
        let viewModel = RechargeOriginalViewModel(service: service)
        let mockViewController = FakeRechargeOriginalViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        let result = viewModel.inputs.cellModel(index: 0)
        
        XCTAssertEqual(result?.title, "Titulo")
        XCTAssertEqual(result?.description, "Desc")
        XCTAssertEqual(result?.image, PageOriginal.ImageType.onboarding)
    }
    
    func testCellModelReturnNil() {
        let service = FakeRechargeOriginalService(title: "Titulo", description: "Desc")
        let viewModel = RechargeOriginalViewModel(service: service)
        let mockViewController = FakeRechargeOriginalViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        let result = viewModel.inputs.cellModel(index: 10)
        
        XCTAssertNil(result)
    }
    
    func testDidTapDownloadOriginalShouldCallDidNextStep() {
        let service = FakeRechargeOriginalService()
        let viewModel = RechargeOriginalViewModel(service: service)
        let mockViewController = FakeRechargeOriginalViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.didTapDownloadOriginal()
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        XCTAssertEqual(mockViewController.didNextStepAction!, .downloadOriginal)
    }
    
    func testDidTapSignUpOriginalShouldCallSignUp() {
        let service = FakeRechargeOriginalService()
        let viewModel = RechargeOriginalViewModel(service: service)
        let mockViewController = FakeRechargeOriginalViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.didTapSignUpOriginal()
        
        XCTAssertTrue(mockViewController.calledSignUp)
    }
    
    func testDidTapSignUpOriginalShouldCallSendToAnalytics() {
        let service = FakeRechargeOriginalService()
        let viewModel = RechargeOriginalViewModel(service: service)
        let mockViewController = FakeRechargeOriginalViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.didTapSignUpOriginal()
        
        XCTAssertTrue(mockViewController.calledSendToAnalytics)
    }
    
    func testCloseShouldCallCloseAction() {
        let service = FakeRechargeOriginalService()
        let viewModel = RechargeOriginalViewModel(service: service)
        let mockViewController = FakeRechargeOriginalViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.close()
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        XCTAssertEqual(mockViewController.didNextStepAction!, .close)
    }
    
    func testCloseShouldCallSendToAnalytics() {
        let service = FakeRechargeOriginalService()
        let viewModel = RechargeOriginalViewModel(service: service)
        let mockViewController = FakeRechargeOriginalViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.close()
        
        XCTAssertTrue(mockViewController.calledSendToAnalytics)
    }
    
    func testChangePageCallShouldIncrement() {
        let service = FakeRechargeOriginalService()
        let viewModel = RechargeOriginalViewModel(service: service)
        let mockViewController = FakeRechargeOriginalViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.changePage(nextPage: 2, currentPage: 1)
        
        viewModel.inputs.close()
        
        XCTAssertTrue(mockViewController.calledSendToAnalytics)
        XCTAssertEqual(mockViewController.swipesNumber!, 1)
    }
    
    func testChangePageCallShouldNotIncrement() {
        let service = FakeRechargeOriginalService()
        let viewModel = RechargeOriginalViewModel(service: service)
        let mockViewController = FakeRechargeOriginalViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.changePage(nextPage: 1, currentPage: 2)
        
        viewModel.inputs.close()
        
        XCTAssertTrue(mockViewController.calledSendToAnalytics)
        XCTAssertEqual(mockViewController.swipesNumber!, 0)
    }
}
