@testable import PicPay
import XCTest

class FakePageOriginalCollectionViewCell: PageOriginalCellViewModelOutputs {
    var calledReloadData = false
    
    func reloadData() {
        calledReloadData = true
    }
}

class PageOriginalCellViewModelTest: XCTestCase {
    func testTitleReturnMockTitle() {
        let model = PageOriginal(title: "Teste", description: "", image: .onboarding)
        let viewModel = PageOriginalCellViewModel()
        let mockView = FakePageOriginalCollectionViewCell()
       
        viewModel.outputs = mockView
        viewModel.inputs.viewConfigure(model: model)
        
        let result = viewModel.inputs.title
        XCTAssertEqual(result, "Teste")
    }
    
    func testDescriptionReturnMockDescription() {
        let model = PageOriginal(title: "", description: "Teste", image: .onboarding)
        let viewModel = PageOriginalCellViewModel()
        let mockView = FakePageOriginalCollectionViewCell()
        
        viewModel.outputs = mockView
        viewModel.inputs.viewConfigure(model: model)
        
        let result = viewModel.inputs.description
        XCTAssertEqual(result, "Teste")
    }
    
    func testImageReturnMockImagen() {
        let model = PageOriginal(title: "", description: "", image: .onboarding)
        let viewModel = PageOriginalCellViewModel()
        let mockView = FakePageOriginalCollectionViewCell()
        
        viewModel.outputs = mockView
        viewModel.inputs.viewConfigure(model: model)
        
        let result = viewModel.inputs.image
        XCTAssertEqual(result, #imageLiteral(resourceName: "ilu_original_carrosel"))
    }
    
    func testViewConfigureShouldCallReloadData() {
        let model = PageOriginal(title: "", description: "", image: .onboarding)
        let viewModel = PageOriginalCellViewModel()
        let mockView = FakePageOriginalCollectionViewCell()
        
        viewModel.outputs = mockView
        viewModel.inputs.viewConfigure(model: model)
        
        XCTAssertTrue(mockView.calledReloadData)
    }
}
