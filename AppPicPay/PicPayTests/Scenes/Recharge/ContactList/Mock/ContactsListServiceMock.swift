import Core
import Contacts
import PicPay

final public class ContactsListServiceMock: ContactsListServicing {
    public private(set) var isGetContactsListCalled: Bool = false
    public private(set) var isCheckUserGrantContactsPermissionCalled: Bool = false
    public private(set) var isRequestPermissionToAccessContactsCalled: Bool = false
    
    public enum ContactsListServiceStatus {
        case success
        case failure
    }
    
    private let status: ContactsListServiceStatus
    private let permissionStatus: CNAuthorizationStatus
    
    public init(status: ContactsListServiceStatus, permissionStatus: CNAuthorizationStatus) {
        self.status = status
        self.permissionStatus = permissionStatus
    }
    
    public func getContacts(completion: @escaping (Result<[UserAgendaContact], ContactsError>) -> Void) {
        isGetContactsListCalled = true
        
        switch status {
        case .success:
            let file = Bundle(for: type(of: self)).path(forResource: "contacts", ofType: "json")
            let data = try! Data(contentsOf: URL(fileURLWithPath: file!))
            let items = try! JSONDecoder().decode([UserAgendaContact].self, from: data)
            completion(.success(items))
        case .failure:
            completion(.failure(.failedToFetchContacts))
        }
    }

    public func checkUserGrantContactsPermission(completion: @escaping (Bool) -> Void) {
        isCheckUserGrantContactsPermissionCalled = true
        
        switch permissionStatus {
        case .authorized:
            completion(true)
        default:
            completion(false)
        }
    }
    
    public func requestPermissionToAccessContacts(completion: @escaping (Bool) -> Void) {
        isRequestPermissionToAccessContactsCalled = true
        
        switch status {
        case .success:
            completion(true)
        case .failure:
            completion(false)
        }
    }
}
