import PicPay

final public class ContactsListCoordinatorMock: ContactsListCoordinating {
    public var viewController: UIViewController?
    public weak var delegate: ContactsListSelecting?
    
    public private(set) var action: ContactsListAction!
    
    public func perform(action: ContactsListAction) {
        self.action = action
    }
    
    
}
