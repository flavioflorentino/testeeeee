import PicPay

final public class ContactsListPresenterMock: ContactsListPresenting {
    public var viewController: ContactsListDisplay?
    
    public private(set) var isEmptyStateVisible: Bool = false
    public private(set) var isGoingToUpdateContactList: Bool = false
    public private(set) var isContactsRequestAuthPopupVisible: Bool = false
    public private(set) var willReloadContacts: Bool = false
    public private(set) var willReturnWithContactSelected: Bool = false
    public private(set) var willDismissView: Bool = false
    
    public private(set) var contact: UserAgendaContact!
    public private(set) var contactArray: [UserAgendaContact]!
    
    public func updateContactsList(with contacts: [UserAgendaContact]?) {
        isGoingToUpdateContactList = true
        self.contactArray = contacts!
    }
    
    public func presentErrorView() {
        isEmptyStateVisible = true
    }
    
    public func presentRequestAuthorizationView() {
        isContactsRequestAuthPopupVisible = true
    }
    
    public func goBack(withUserContact contact: UserAgendaContact) {
        willReturnWithContactSelected = true
        self.contact = contact
    }
    
    public func dismiss() {
        willDismissView = true
    }
    
    public func retryLoadingContacts() {
        willReloadContacts = true
    }
}
