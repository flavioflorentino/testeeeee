import UI
@testable import PicPay

final public class ContactsListDisplayMock: ContactsListDisplay {
    public private(set) var isUpdateContactsListCalled: Bool = false
    public private(set) var hasContactsListViewModel: Bool = false
    public private(set) var isPresentErrorViewCalled: Bool = false
    public private(set) var isPresentRequestAuthCalled: Bool = false
    public private(set) var isGoingToRetryLoadingContacts: Bool = false
    
    public func updateContactsList(with contacts: [Section<String, ContactsListCellViewModel>]) {
        isUpdateContactsListCalled = true
        hasContactsListViewModel = contacts.count > 0
    }
    
    public func presentErrorView() {
        isPresentErrorViewCalled = true
    }
    
    public func presentRequestAuthorizationView() {
        isPresentRequestAuthCalled = true
    }
    
    public func retryLoadingContacts() {
        isGoingToRetryLoadingContacts = true
    }
}
