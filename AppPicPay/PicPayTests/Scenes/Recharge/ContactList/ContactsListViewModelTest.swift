import AnalyticsModule
import Contacts
import XCTest
@testable import PicPay

final class ContactsListViewModelTest: XCTestCase {
    var sut: ContactsListViewModel!
    var serviceMock: ContactsListServiceMock!
    var presenterMock: ContactsListPresenterMock!
    
    func testGetContactsSuccessShouldUpdateContactsList() {
        // Arrange
        setupTest(status: .success(permissionStatus: .authorized))
        
        // Act
        sut.getUserContacts()
        
        // Assert
        XCTAssertTrue(serviceMock.isGetContactsListCalled)
        XCTAssertTrue(presenterMock.isGoingToUpdateContactList)
    }
    
    func testGetContactsFailureShouldPresentErrorEmptyState() {
        // Arrange
        setupTest(status: .failure(permissionStatus: .authorized))
        
        // Act
        sut.getUserContacts()
        
        // Assert
        XCTAssertTrue(serviceMock.isGetContactsListCalled)
        XCTAssertTrue(presenterMock.isEmptyStateVisible)
    }
    
    func testGetContactsSuccessWithContactsAuthorizationShouldProccedToLoadContacts() {
        /**
         For this test it doesn't matter if the setup is successful or not,
         the test should assert before calling the service method.
         */
        
        // Arrange
        setupTest(status: .success(permissionStatus: .authorized))
        
        // Act
        sut.getUserContacts()
        
        // Assert
        XCTAssertTrue(serviceMock.isCheckUserGrantContactsPermissionCalled)
    }
    
    func testGetContactsSuccessWithoutContactsAuthorizationShouldPresentRequestAuthorizationView() {
        /**
        For this test it doesn't matter if the setup is successful or not,
        the test should assert before calling the service method.
        */
        
        // Arrange
        setupTest(status: .success(permissionStatus: .notDetermined))
        
        // Act
        sut.getUserContacts()
        
        // Assert
        XCTAssertTrue(serviceMock.isCheckUserGrantContactsPermissionCalled)
        XCTAssertTrue(presenterMock.isContactsRequestAuthPopupVisible)
    }
    
    func testSelectedContactFromAllContactsShouldGoBackToParentViewControllerWithContactData() {
        // Arrange
        setupTest(status: .success(permissionStatus: .authorized))
        
        // Act
        sut.getUserContacts()
        sut.goBack(index: 1)
        
        // Assert
        XCTAssertTrue(presenterMock.willReturnWithContactSelected)
        XCTAssertEqual(presenterMock.contact.id, "2")
    }
    
    func testSelectedContactFromFilteredContactsShouldGoBackToParentViewControllerWithFilteredContactData() {
        // Arrange
        setupTest(status: .success(permissionStatus: .authorized))
        
        // Act
        sut.getUserContacts()
        sut.filterContactsList(for: "Steve")
        sut.goBack(index: 0)
        
        // Assert
        XCTAssertTrue(presenterMock.willReturnWithContactSelected)
        XCTAssertEqual(presenterMock.contact.id, "3")
    }
    
    func testFilterContactsNoTextShouldUpdateContactListWithAllContacts() {
        // Arrange
        setupTest(status: .success(permissionStatus: .authorized))
        
        // Act
        sut.getUserContacts()
        sut.filterContactsList(for: "")
        
        // Assert
        XCTAssertTrue(presenterMock.isGoingToUpdateContactList)
        XCTAssertEqual(presenterMock.contactArray.count, 3)
    }
    
    func testFilterContactsWithTextShouldUpdateContactListWithFilteredContacts() {
        // Arrange
        setupTest(status: .success(permissionStatus: .authorized))
        
        // Act
        sut.getUserContacts()
        sut.filterContactsList(for: "John")
        
        // Assert
        XCTAssertTrue(presenterMock.isGoingToUpdateContactList)
        XCTAssertEqual(presenterMock.contactArray.count, 1)
    }
    
    func testRequestPermissionToAccessContactsSuccessShouldRetryLoadingContactsList() {
        /**
         Ignoring the permissionStatus sent, this test is using the .success status to
         test if requestContactsAccess will return true when permission is given
         */
        
        // Arrange
        setupTest(status: .success(permissionStatus: .authorized))
        
        // Act
        sut.requestContactsAccess()
        
        // Assert
        XCTAssertTrue(serviceMock.isRequestPermissionToAccessContactsCalled)
        XCTAssertTrue(presenterMock.willReloadContacts)
    }
    
    func testRequestPermissionToAccessContactsFailureShouldDoNothing() {
        /**
         Ignoring the permissionStatus sent, this test is using the .failure status to
         test if requestContactsAccess will return false when permission is denied
         */
        
        // Arrange
        setupTest(status: .failure(permissionStatus: .denied))
        
        // Act
        sut.requestContactsAccess()
        
        // Assert
        XCTAssertTrue(serviceMock.isRequestPermissionToAccessContactsCalled)
        XCTAssertFalse(presenterMock.willReloadContacts)
    }
}

extension ContactsListViewModelTest {
    private enum ContactsListTestsStatus {
        case success(permissionStatus: CNAuthorizationStatus)
        case failure(permissionStatus: CNAuthorizationStatus)
    }
    
    private func setupTest(status: ContactsListTestsStatus) {
        switch status {
        case let .success(permissionStatus):
            serviceMock = ContactsListServiceMock(status: .success, permissionStatus: permissionStatus)
        case let .failure(permissionStatus):
            serviceMock = ContactsListServiceMock(status: .failure, permissionStatus: permissionStatus)
        }
            
        presenterMock = ContactsListPresenterMock()
        sut = ContactsListViewModel(
            service: serviceMock,
            presenter: presenterMock,
            dependencies: DependencyContainerMock(AnalyticsSpy())
        )
    }
}
