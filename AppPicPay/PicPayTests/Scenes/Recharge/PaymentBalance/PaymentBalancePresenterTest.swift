import XCTest
import CoreLegacy
@testable import PicPay

final class SpyPaymentBalanceViewController: PaymentBalanceDisplay {
    private(set) var calledDisplayBalance = 0
    private(set) var calledIsBalanceOn = 0
    private(set) var calledDidReceiveAnError = 0

    private(set) var valueBalance: String?
    private(set) var isEnable: Bool?
    private(set) var picpayError: PicPayError?

    func displayBalance(value: String) {
        calledDisplayBalance += 1
        valueBalance = value
    }

    func isBalanceOn(enable: Bool) {
        calledIsBalanceOn += 1
        isEnable = enable
    }

    func didReceiveAnError(_ error: PicPayError) {
        calledDidReceiveAnError += 1
        picpayError = error
    }
}

final class PaymentBalancePresenterTest: XCTestCase {
    private let queue = DispatchQueue(label: #function)
    
    func testDisplayBalanceShouldCallIsBalanceOn() {
        let spyViewController = SpyPaymentBalanceViewController()
        let presenter = PaymentBalancePresenter(dependencies: DependencyContainerMock(queue))
        
        presenter.viewController = spyViewController
        presenter.displayBalance(value: 100.0, isEnable: true)
        
        XCTAssertEqual(spyViewController.calledIsBalanceOn, 1)
    }
    
    func testDisplayBalanceShouldCallIsBalanceOnWithTrue() {
        let spyViewController = SpyPaymentBalanceViewController()
        let presenter = PaymentBalancePresenter(dependencies: DependencyContainerMock(queue))
        
        presenter.viewController = spyViewController
        presenter.displayBalance(value: 100.0, isEnable: true)
        
        XCTAssertEqual(spyViewController.isEnable, true)
    }
    
    func testDisplayBalanceShouldCallIsBalanceOnWithFalse() {
        let spyViewController = SpyPaymentBalanceViewController()
        let presenter = PaymentBalancePresenter(dependencies: DependencyContainerMock(queue))
        
        presenter.viewController = spyViewController
        presenter.displayBalance(value: 100.0, isEnable: false)
        
        XCTAssertEqual(spyViewController.isEnable, false)
    }
    
    func testDisplayBalanceShouldCallDisplayBalance() {
        let spyViewController = SpyPaymentBalanceViewController()
        let presenter = PaymentBalancePresenter(dependencies: DependencyContainerMock(queue))
        
        presenter.viewController = spyViewController
        presenter.displayBalance(value: 100.0, isEnable: true)
        
        XCTAssertEqual(spyViewController.calledDisplayBalance, 1)
    }
    
    func testDisplayBalanceShouldCallDisplayBalanceWithValueString() {
        let spyViewController = SpyPaymentBalanceViewController()
        let presenter = PaymentBalancePresenter(dependencies: DependencyContainerMock(queue))
        let result = CurrencyFormatter.brazillianRealString(from: 100.0)
        
        presenter.viewController = spyViewController
        presenter.displayBalance(value: 100.0, isEnable: true)
        
        XCTAssertEqual(spyViewController.valueBalance, result)
    }
}
