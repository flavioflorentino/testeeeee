import XCTest
@testable import PicPay

final class MockPaymentBalanceService: PaymentBalanceServicing {
    private(set) var balanceIsEnable: Bool
    private let value: Double
    
    var balanceValue: Double {
         return value
    }
    
    init(balanceIsEnable: Bool = false, balanceValue: Double = 153.38) {
        self.balanceIsEnable = balanceIsEnable
        self.value = balanceValue
    }
    
    func balanceIsBeingUsed() -> Bool {
        return balanceIsEnable
    }
    
    func setUseBalance(isEnable: Bool) {
        balanceIsEnable = isEnable
    }
}

final class SpyPaymentBalancePresenter: PaymentBalancePresenting {
    var viewController: PaymentBalanceDisplay?
    
    private(set) var calledDisplayBalance = 0
    private(set) var balanceValue: Double?
    private(set) var balanceIsEnable: Bool?
    
    func displayBalance(value: Double, isEnable: Bool) {
        calledDisplayBalance += 1
        balanceValue = value
        balanceIsEnable = isEnable
    }
}

final class PaymentBalanceViewModelTest: XCTestCase {
    private let queue = DispatchQueue(label: #function)
    
    func testViewDidLoadShouldCallDisplayBalance() {
        let mockService = MockPaymentBalanceService()
        let spyPresenter = SpyPaymentBalancePresenter()
        let viewModel = PaymentBalanceViewModel(service: mockService, presenter: spyPresenter, dependencies: DependencyContainerMock(queue))
        
        viewModel.viewDidLoad()
        
        XCTAssertEqual(spyPresenter.calledDisplayBalance, 1)
    }
    
    func testViewDidLoadShouldCallDisplayBalanceWithValue() {
        let mockService = MockPaymentBalanceService(balanceValue: 100.0)
        let spyPresenter = SpyPaymentBalancePresenter()
        let viewModel = PaymentBalanceViewModel(service: mockService, presenter: spyPresenter, dependencies: DependencyContainerMock(queue))
        
        viewModel.viewDidLoad()
        
        XCTAssertEqual(spyPresenter.balanceValue, 100.0)
    }
    
    func testViewDidLoadShouldCallDisplayBalanceWithIsEnable() {
        let mockService = MockPaymentBalanceService(balanceIsEnable: true)
        let spyPresenter = SpyPaymentBalancePresenter()
        let viewModel = PaymentBalanceViewModel(service: mockService, presenter: spyPresenter, dependencies: DependencyContainerMock(queue))
        
        viewModel.viewDidLoad()
        
        XCTAssertEqual(spyPresenter.balanceIsEnable, true)
    }
    
    func testSetUseBalanceShoulEnableBalance() {
        let mockService = MockPaymentBalanceService()
        let spyPresenter = SpyPaymentBalancePresenter()
        let viewModel = PaymentBalanceViewModel(service: mockService, presenter: spyPresenter, dependencies: DependencyContainerMock(queue))
        
        viewModel.setUseBalance(true)
        
        XCTAssertEqual(mockService.balanceIsEnable, true)
    }
    
    func testSetUseBalanceShoulDisableBalance() {
        let mockService = MockPaymentBalanceService()
        let spyPresenter = SpyPaymentBalancePresenter()
        let viewModel = PaymentBalanceViewModel(service: mockService, presenter: spyPresenter, dependencies: DependencyContainerMock(queue))
        
        viewModel.setUseBalance(false)
        
        XCTAssertEqual(mockService.balanceIsEnable, false)
    }
}
