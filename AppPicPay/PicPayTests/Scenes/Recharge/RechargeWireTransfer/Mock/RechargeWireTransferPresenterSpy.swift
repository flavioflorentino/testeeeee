@testable import PicPay

final class RechargeWireTransferPresenterSpy: RechargeWireTransferPresenting {
    var viewController: RechargeWireTransferDisplay?
    
    private(set) var displayedInfo: RechargeWireTransferAccountInfo?
    private(set) var displayCount = 0
    func display(_ accountInfo: RechargeWireTransferAccountInfo) {
        displayedInfo = accountInfo
        displayCount += 1
    }
    
    private(set) var sharedInfo: [Any]?
    private(set) var shareCount = 0
    func share(accountInfo: [Any]) {
        sharedInfo = accountInfo
        shareCount += 1
    }
    
    private(set) var triggeredActions: [RechargeWireTransferAction] = []
    func didNextStep(action: RechargeWireTransferAction) {
        triggeredActions.append(action)
    }
}
