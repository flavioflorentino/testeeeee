@testable import PicPay

final class RechargeWireTransferDelegateSpy: WireTransferCopiableDelegate {
    private(set) var copiedInfo: String?
    private(set) var copiedTitle: String?
    private(set) var didCopyCount: Int = 0
    func didCopy(info: String?, title: String?) {
        copiedInfo = info
        copiedTitle = title
        didCopyCount += 1
    }
}
