@testable import PicPay
import XCTest

final class WireTransferCopiableInfoViewTest: XCTestCase {
    private lazy var delegateSpy = RechargeWireTransferDelegateSpy()
    private lazy var sut: WireTransferCopiableInfoViewProtocol = {
        let view = WireTransferCopiableInfoView(delegate: delegateSpy)
        return view
    }()

    func testCopyInfo_WhenCopyingInfo_ShouldInvokeDelegateDidCopy() {
        let title = "Test Title"
        let info = "Test Info"

        sut.setUp(title: title, info: info)

        sut.copyInfo()

        XCTAssertEqual(delegateSpy.copiedInfo, info)
        XCTAssertEqual(delegateSpy.copiedTitle, title)
        XCTAssertEqual(delegateSpy.didCopyCount, 1)
    }
    
    func testCopyInfo_WhenCopyingInfoWithSpecialCharacters_ShouldCopyInfoWithoutSpecialCharacters() {
        let title = "Test Title"
        let info = "111.111.111-11"
        let validCopiedInfo = "11111111111"

        sut.setUp(title: title, info: info)

        sut.copyInfo()

        XCTAssertEqual(delegateSpy.copiedInfo, validCopiedInfo)
        XCTAssertEqual(delegateSpy.copiedTitle, title)
        XCTAssertEqual(delegateSpy.didCopyCount, 1)
    }
}
