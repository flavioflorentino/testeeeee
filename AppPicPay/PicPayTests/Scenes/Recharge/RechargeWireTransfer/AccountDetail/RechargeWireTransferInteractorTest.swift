@testable import PicPay
import AnalyticsModule
import Core
import Foundation
import XCTest

extension RechargeWireTransferAccountInfo: Equatable {
    public static func == (lhs: RechargeWireTransferAccountInfo, rhs: RechargeWireTransferAccountInfo) -> Bool {
        return lhs.userInfo.avatar == rhs.userInfo.avatar &&
            lhs.userInfo.username == rhs.userInfo.username &&
            lhs.userInfo.fullname == rhs.userInfo.fullname &&
            lhs.bankInfo.bankNumber == rhs.bankInfo.bankNumber &&
            lhs.bankInfo.agencyNumber == rhs.bankInfo.agencyNumber &&
            lhs.bankInfo.accountNumber == rhs.bankInfo.accountNumber &&
            lhs.bankInfo.document == rhs.bankInfo.document
    }
}

final class RechargeWireTransferInteractorTest: XCTestCase {
    private let analytics = AnalyticsSpy()
    private let kvStore = KVStoreMock()
    private lazy var dependenciesMock = DependencyContainerMock(analytics, kvStore)
    private lazy var presenterSpy = RechargeWireTransferPresenterSpy()
    private lazy var sut = RechargeWireTransferInteractor(accountInfo: accountInfoMock,
                                                          presenter: presenterSpy,
                                                          dependencies: dependenciesMock)
    
    private var accountInfoMock: RechargeWireTransferAccountInfo {
        let userInfo = RechargeWireTransferUserInfo(avatar: nil,
                                                username: "magpali",
                                                fullname: "Victor Magpali")
        
        let bankInfo = RechargeWireTransferBankInfo(bankNumber: "111",
                                                    agencyNumber: "11111",
                                                    accountNumber: "1111-1",
                                                    document: "111.111.111-11")
        
        return RechargeWireTransferAccountInfo(userInfo: userInfo, bankInfo: bankInfo)
    }
    
    func testLoadInfo_wheneverCalled_shouldInvokePresenterDisplay() {
        sut.loadInfo()
        
        XCTAssertEqual(presenterSpy.displayedInfo, accountInfoMock)
    }
    
    func testLoadInfo_wheneverCalled_shouldSendAnalyticsEvent() {
        let validEvent = RechargeEvent.wireTransfer(.picpayAccount).event()
        sut.loadInfo()
        
        XCTAssertTrue(analytics.equals(to: validEvent))
    }
    
    func testLoadInfo_onUsersFirstAccess_shouldInvokePresenterOnboarding() {
        kvStore.set(value: false, with: RechargeWireTransferUserDefaultKey.wireTransferOnboardingHasBeenShown.rawValue)
        sut.loadInfo()
        
        XCTAssertEqual(presenterSpy.triggeredActions, [.startOnboarding])
    }
    
    func testLoadInfo_afterUsersFirstAccess_shouldNotInvokePresenterOnboarding() {
        kvStore.set(value: true, with: RechargeWireTransferUserDefaultKey.wireTransferOnboardingHasBeenShown.rawValue)
        sut.loadInfo()
        
        XCTAssertEqual(presenterSpy.triggeredActions, [])
    }
    
    func testClose_wheneverCalled_shouldInvokePresenterBack() {
        sut.close()
        
        XCTAssertEqual(presenterSpy.triggeredActions, [.back])
    }
    
    func testMoreInfo_wheneverCalled_shouldInvokePresenterMoreInfo() {
        sut.moreInfo()
        
        XCTAssertEqual(presenterSpy.triggeredActions, [.moreInfo])
    }
    
    func testShareBankInfo_wheneverCalled_shouldInvokePresenterShareWithFormattedInfo() {
        let validMock = accountInfoMock
        let validAccountString = validMock.bankInfo.accountNumber.byRemovingPunctuation
        let validDocumentString = validMock.bankInfo.document.byRemovingPunctuation
        
        let validInfoString = Strings.RechargeWireTransfer.bankInfoSharable(validMock.userInfo.fullname,
                                                                            validMock.bankInfo.bankNumber,
                                                                            validMock.bankInfo.agencyNumber,
                                                                            validAccountString,
                                                                            validDocumentString)

        sut.shareBankInfo()
        
        XCTAssertEqual(presenterSpy.sharedInfo as? [String], [validInfoString])
        XCTAssertEqual(presenterSpy.shareCount, 1)
    }
}
