import XCTest
import FeatureFlag
import OHHTTPStubs
@testable import PicPay

final class WireTransferReceiptServiceTests: XCTestCase {
    private lazy var featureManagerMock = FeatureManagerMock()
    private lazy var sut = WireTransferReceiptService(dependencies: DependencyContainerMock(featureManagerMock))
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testFetchReceiptInfo_whenReceivingASucessfulResponse_ShouldSuccedInReturningAWireTransferReceiptModel() {
        let expectation = XCTestExpectation(description: "success")
        let rechargeId = "111"
        
        stub(condition: isPath("/api/recharge/\(rechargeId)/receipt")) { _ in
            return StubResponse.success(at: "RechargeWireTransferReceipt.json")
        }
        
        sut.fetchReceiptInfo(receiptId: rechargeId, completion: { result in
            if case .success(let value) = result {
                XCTAssertNotNil(value)
                expectation.fulfill()
            }
        })
            
        wait(for: [expectation], timeout: 1)
    }
    
    func testFetchReceiptInfo_whenReceivingAFailingResponse_ShouldCompleteWithFailure() {
        let expectation = XCTestExpectation(description: "success")
        let rechargeId = "111"
        
        stub(condition: isPath("/api/recharge/\(rechargeId)/receipt")) { _ in
            return StubResponse.fail()
        }
        
        sut.fetchReceiptInfo(receiptId: rechargeId, completion: { result in
            if case .failure = result {
                expectation.fulfill()
            }
        })
            
        wait(for: [expectation], timeout: 1)
    }
    
    func testContantInfo_wheneverCalled_shouldRespondWithAReceiptContactInfo() {
        let contactInfo = WireTransferContactInfo(isContactInformationEnabled: true, sacPhoneNumber: "1212123123", ombudsmanPhoneNumber: "222221111")
        featureManagerMock.override(key: .featureTransferReceiptContactInfo, with: contactInfo)
        
        XCTAssertNotNil(sut.contactInfo)
    }
}


