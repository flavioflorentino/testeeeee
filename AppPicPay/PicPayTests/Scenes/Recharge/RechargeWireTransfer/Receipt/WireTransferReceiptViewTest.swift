@testable import PicPay
import XCTest

final class WireTransferReceiptViewTest: XCTestCase {
    private lazy var sut = WireTransferReceiptView()

    func testBuildLayout_ShouldAddSubviews() {
        sut.buildLayout()
        
        XCTAssertEqual(sut.scrollView.subviews.count, 1)
        XCTAssertTrue(sut.scrollView.subviews.contains(sut.stackView))
        XCTAssertEqual(sut.stackView.arrangedSubviews.count, 7)
        XCTAssertTrue(sut.stackView.arrangedSubviews.contains(sut.headerView))
        XCTAssertTrue(sut.stackView.arrangedSubviews.contains(sut.valueView))
        XCTAssertTrue(sut.stackView.arrangedSubviews.contains(sut.infoView))
        XCTAssertTrue(sut.stackView.arrangedSubviews.contains(sut.footerView))
    }
}

final class WireTransferReceiptHeaderViewTest: XCTestCase {
    private lazy var sut = WireTransferReceiptHeaderView()
    private lazy var mockViewModel = WireTransferReceiptHeaderViewModel(depositId: "DepositId", depositDate: "DepositDate")
    
    func testSetUp_ShouldAddPopulatedSubviews() {
        sut.setUp(with: mockViewModel)
        
        let infoText = Strings.RechargeWireTransfer.receiptHeaderInfo(mockViewModel.depositId, mockViewModel.depositDate)
        
        XCTAssertEqual(sut.subviews.count, 3)
        XCTAssertTrue(sut.subviews.contains(sut.imageView))
        XCTAssertTrue(sut.subviews.contains(sut.titleLabel))
        XCTAssertTrue(sut.subviews.contains(sut.infoLabel))
        XCTAssertEqual(sut.infoLabel.text, infoText)
    }
}

final class WireTransferReceiptInfoComponentTest: XCTestCase {
    func testSetUp_ShouldAddPopulatedSubviews() {
        let title = "Title"
        let info = "Info"
        let sut = WireTransferReceiptInfoComponent(title: title, info: info)
        
        XCTAssertEqual(sut.subviews.count, 2)
        XCTAssertTrue(sut.subviews.contains(sut.titleLabel))
        XCTAssertTrue(sut.subviews.contains(sut.infoLabel))
        XCTAssertEqual(sut.titleLabel.text, title)
        XCTAssertEqual(sut.infoLabel.text, info)
    }
} 

final class WireTransferReceiptInfoViewTest: XCTestCase {
    private lazy var sut = WireTransferReceiptInfoView()
    
    private lazy var mockOriginInfo = ReceiptOriginInfoViewModel(name: "originName", documentType: .cpf, document: "document", bank: "bank", account: "originAccount")
    private lazy var mockDestinationInfo = ReceiptDestinationInfoViewModel(name: "infoName", agency: "agency", account: "destinationAccount")
    private lazy var mockViewModel = WireTransferReceiptInfoViewModel(originInfo: mockOriginInfo, destinationInfo: mockDestinationInfo)
    
    func testSetUp_ShouldAddPopulatedSubviews() throws {
        sut.setUp(with: mockViewModel)
        
        XCTAssertEqual(sut.arrangedSubviews.count, 9)
        
        let originTitleLabel = try XCTUnwrap(sut.arrangedSubviews[0] as? UILabel)
        XCTAssertEqual(originTitleLabel.text, sut.originSectionLabel.text)
        
        let originNameComponent = try XCTUnwrap(sut.arrangedSubviews[1] as? WireTransferReceiptInfoComponent)
        XCTAssertEqual(originNameComponent.infoLabel.text, mockOriginInfo.name)
        
        let originDocumentComponent = try XCTUnwrap(sut.arrangedSubviews[2] as? WireTransferReceiptInfoComponent)
        XCTAssertEqual(originDocumentComponent.titleLabel.text, mockOriginInfo.documentType.rawValue)
        XCTAssertEqual(originDocumentComponent.infoLabel.text, mockOriginInfo.document)
        
        let originBankComponent = try XCTUnwrap(sut.arrangedSubviews[3] as? WireTransferReceiptInfoComponent)
        XCTAssertEqual(originBankComponent.infoLabel.text, mockOriginInfo.bank)
        
        let originAccountComponent = try XCTUnwrap(sut.arrangedSubviews[4] as? WireTransferReceiptInfoComponent)
        XCTAssertEqual(originAccountComponent.infoLabel.text, mockOriginInfo.account)
        
        let destinationTitleLabel = try XCTUnwrap(sut.arrangedSubviews[5] as? UILabel)
        XCTAssertEqual(destinationTitleLabel.text, sut.destinationSectionLabel.text)
        
        let destinationNameComponent = try XCTUnwrap(sut.arrangedSubviews[6] as? WireTransferReceiptInfoComponent)
        XCTAssertEqual(destinationNameComponent.infoLabel.text, mockDestinationInfo.name)
        
        let destinationAgencyComponent = try XCTUnwrap(sut.arrangedSubviews[7] as? WireTransferReceiptInfoComponent)
        XCTAssertEqual(destinationAgencyComponent.infoLabel.text, mockDestinationInfo.agency)
        
        let destinationAccountComponent = try XCTUnwrap(sut.arrangedSubviews[8] as? WireTransferReceiptInfoComponent)
        XCTAssertEqual(destinationAccountComponent.infoLabel.text, mockDestinationInfo.account)
    }
}

final class WireTransferReceiptValueViewTest: XCTestCase {
    private lazy var sut = WireTransferReceiptValueView()
    
    func testSetUp_ShouldAddPopulatedSubviews() {
        let value = "MAIS DE OITO MIL"
        sut.setUp(value: value)
        
        XCTAssertEqual(sut.subviews.count, 2)
        XCTAssertEqual(sut.valueLabel.text, value)
    }
}
