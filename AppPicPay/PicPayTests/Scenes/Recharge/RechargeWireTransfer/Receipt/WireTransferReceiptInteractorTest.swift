@testable import PicPay
import AnalyticsModule
import Core
import UIKit.UIImage
import XCTest

final class WireTransferReceiptServiceMock: WireTransferReceiptServicing {
    var fetchReceiptInfoExpectedResult: Result<WireTransferReceiptModel, ApiError>?
    func fetchReceiptInfo(receiptId: String, completion: @escaping (Result<WireTransferReceiptModel, ApiError>) -> Void) {
        guard let expectedResult = fetchReceiptInfoExpectedResult else {
            XCTFail("Expected result not defined")
            return
        }
        completion(expectedResult)
    }
    
    var contactInfo: WireTransferContactInfo?
}

final class WireTransferReceiptPresenterSpy: WireTransferReceiptPresenting {
    var viewController: WireTransferReceiptDisplaying?
    
    private(set) var populatedViewModel: WireTransferReceiptViewModel?
    private(set) var populateCount = 0
    func populate(with viewModel: WireTransferReceiptViewModel) {
        populatedViewModel = viewModel
        populateCount += 1
    }
    
    private(set) var didNextStepAction: WireTransferReceiptAction?
    private(set) var didNextStepCount = 0
    func didNextStep(action: WireTransferReceiptAction) {
        didNextStepAction = action
        didNextStepCount += 1
    }

    private(set) var sharedImage: UIImage?
    private(set) var shareCount = 0
    func share(_ image: UIImage) {
        shareCount += 1
        sharedImage = image
    }
    
    private(set) var showErrorCount = 0
    func showError() {
        showErrorCount += 1
    }
    
    private(set) var showLoadingCount = 0
    func showLoading() {
        showLoadingCount += 1
    }
    
    private(set) var closeCount = 0
    func close() {
        closeCount += 1
    }
}

final class WireTransferReceiptInteractorTest: XCTestCase {
    typealias Dependencies = HasAnalytics
    private let analyticsSpy = AnalyticsSpy()
    private lazy var dependenciesMock: Dependencies = DependencyContainerMock(analyticsSpy)
    private lazy var presenterSpy = WireTransferReceiptPresenterSpy()
    private lazy var serviceMock = WireTransferReceiptServiceMock()
    
    private lazy var sut = WireTransferReceiptInteractor(receiptId: "",
                                                         service: serviceMock,
                                                         presenter: presenterSpy,
                                                         dependencies: dependenciesMock)
    
    func testLoadInfo_OnCpnjSuccess_ShouldInvokePresenterPopulate() {
        let cnpjMock = WireTransferReceiptModelMock.cnpj.getMock()
        
        let contactInfoMock = WireTransferContactInfo(isContactInformationEnabled: true,
                                                      sacPhoneNumber: "SAC",
                                                      ombudsmanPhoneNumber: "OMBUDSMAN")
        
        serviceMock.fetchReceiptInfoExpectedResult = .success(cnpjMock)
        serviceMock.contactInfo = contactInfoMock
        sut.loadInfo()
        
        XCTAssertEqual(presenterSpy.populatedViewModel?.info.originInfo.documentType, .cnpj)
        XCTAssertNotNil(presenterSpy.populatedViewModel?.contactInfo)
        XCTAssertEqual(presenterSpy.showLoadingCount, 1)
        XCTAssertEqual(presenterSpy.populateCount, 1)
    }
    
    func testLoadInfo_OnCpfSuccess_ShouldInvokePresenterPopulate() {
        let cpjMock = WireTransferReceiptModelMock.cpf.getMock()
        serviceMock.fetchReceiptInfoExpectedResult = .success(cpjMock)
        sut.loadInfo()
        
        XCTAssertEqual(presenterSpy.populatedViewModel?.info.originInfo.documentType, .cpf)
        XCTAssertNil(presenterSpy.populatedViewModel?.contactInfo)
        XCTAssertEqual(presenterSpy.showLoadingCount, 1)
        XCTAssertEqual(presenterSpy.populateCount, 1)
    }
    
    func testLoadInfo_OnFailure_ShouldInvokePresenterShowError() {
        serviceMock.fetchReceiptInfoExpectedResult = .failure(.unknown(nil))
        sut.loadInfo()
        
        XCTAssertNil(presenterSpy.populatedViewModel)
        XCTAssertEqual(presenterSpy.showLoadingCount, 1)
        XCTAssertEqual(presenterSpy.showErrorCount, 1)
        XCTAssertEqual(presenterSpy.populateCount, 0)
    }
    
    func testClose_WheneverCalled_ShouldInvokePresenterClose() {
        sut.close()
        
        XCTAssertEqual(presenterSpy.closeCount, 1)
    }
    
    func testShare_WhenPassingAnImage_ShouldInvokePresenterShareWithTheSameImage() {
        let image = UIImage()
        sut.share(image)

        XCTAssertEqual(presenterSpy.shareCount, 1)
        XCTAssertEqual(presenterSpy.sharedImage, image)
    }
    
    func testShare_WheneverCalled_ShouldSendAnAnalyticsEvent() {
        sut.share(UIImage())
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertEqual(analyticsSpy.event?.name, RechargeEvent.receiptSharing.event().name)
    }
}

enum WireTransferReceiptModelMock: String {
    case cpf = "111.111.111-11"
    case cnpj = "11.111.111/1111-11"
    
    func getMock() -> WireTransferReceiptModel {
        WireTransferReceiptModel(value: "VALOR",
                                 rechargeDate: "DATA",
                                 rechargeId: "ID",
                                 sourceDocument: rawValue,
                                 sourceFullName: "ORIGEM NOME COMPLETO",
                                 sourceBankName: "NOME BANCO",
                                 sourceAccountNumber: "ORIGEM CONTA",
                                 sourceAgency: "ORIGEM AGENCIA",
                                 destinationFullName: "DESTINO NOME COMPLETO",
                                 destinationAccountNumber: "DESTINO CONTA",
                                 destinationAgency: "DESTINO AGENCIA",
                                 destinationBankName: "BANCO")
    }
}
