@testable import PicPay

final class WireTransferReceiptCoordinatorSpy: WireTransferReceiptCoordinating {
    var viewController: UIViewController? = ViewControllerMock()
    
    private(set) var didPerformAction: WireTransferReceiptAction? = nil
    private(set) var performCount: Int = 0
    func perform(action: WireTransferReceiptAction) {
        didPerformAction = action
        performCount += 1
    }
}
