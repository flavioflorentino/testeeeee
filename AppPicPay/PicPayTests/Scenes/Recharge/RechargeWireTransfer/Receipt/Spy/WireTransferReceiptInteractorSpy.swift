@testable import PicPay
import UIKit.UIImage

final class WireTransferReceiptInteractorSpy: WireTransferReceiptInteracting {
    private(set) var loadInfoCount: Int = 0
    func loadInfo() {
        loadInfoCount += 1
    }
    
    private(set) var sharedImage: UIImage?
    private(set) var shareCount = 0
    func share(_ image: UIImage) {
        shareCount += 1
        sharedImage = image
    }
    
    private(set) var closeCount = 0
    func close() {
        closeCount += 1
    }
}
