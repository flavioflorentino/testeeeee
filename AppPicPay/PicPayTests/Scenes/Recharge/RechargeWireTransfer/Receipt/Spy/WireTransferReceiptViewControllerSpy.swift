@testable import PicPay
import UIKit.UIImage

final class WireTransferReceiptViewControllerSpy: WireTransferReceiptDisplaying {
    private(set) var displayedState: WireTransferReceiptState?
    private(set) var displayCount = 0
    func display(state: WireTransferReceiptState) {
        displayedState = state
        displayCount += 1
    }
    
    private(set) var sharedImage: UIImage?
    private(set) var shareCount = 0
    func share(_ image: UIImage) {
        shareCount += 1
        sharedImage = image
    }
    
    private(set) var closeCount = 0
    func close() {
        closeCount += 1
    }
}
