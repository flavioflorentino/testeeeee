@testable import PicPay
import XCTest

final class WireTransferReceiptViewControllerTest: XCTestCase {
    private lazy var interactorSpy = WireTransferReceiptInteractorSpy()
    private lazy var sut = WireTransferReceiptViewController(interactor: interactorSpy)
    
    func testShareScreenSnapshot_WheneverCalled_ShouldInvokeInteractorShareWithTheSameImage() {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.makeKeyAndVisible()
        window.rootViewController = sut
        
        sut.shareScreenSnapshot()
        
        XCTAssertEqual(interactorSpy.shareCount, 1)
        XCTAssertNotNil(interactorSpy.sharedImage)
    }
    
    func testViewDidLoad_WheneverCalled_ShouldInvokeInteractorLoadInfo() {
        sut.viewDidLoad()
        
        XCTAssertEqual(interactorSpy.loadInfoCount, 1)
    }
}
