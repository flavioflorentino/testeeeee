@testable import PicPay
import UI
import XCTest


extension WireTransferReceiptState: Equatable {
    public static func == (lhs: WireTransferReceiptState, rhs: WireTransferReceiptState) -> Bool {
        switch(lhs, rhs) {
        case(.loading, .loading):
            return true
        case(.error, .error):
            return true
        case(.loaded, .loaded):
            return true
        default:
            return false
        }
    }
}

class WireTransferReceiptPresenterTest: UIView {
    typealias Dependencies = HasNoDependency
    private lazy var dependenciesMock: Dependencies = DependencyContainerMock()
    private lazy var coordinatorSpy = WireTransferReceiptCoordinatorSpy()
    private lazy var controllerSpy = WireTransferReceiptViewControllerSpy()
    private lazy var sut: WireTransferReceiptPresenter = {
        let presenter = WireTransferReceiptPresenter(coordinator: coordinatorSpy, dependencies: dependenciesMock)
        presenter.viewController = controllerSpy
        return presenter
    }()
    
    func testShowLoading_wheneverCalled_shouldInvokeViewControllerDisplayWithLoadingState() {
        sut.showLoading()
        
        XCTAssertEqual(controllerSpy.displayCount, 1)
        XCTAssertEqual(controllerSpy.displayedState, .loading)
    }
    
    func testShowError_wheneverCalled_shouldInvokeViewControllerDisplayWithErrorState() throws {
        sut.showError()
        
        let mockViewModel = StatefulErrorViewModel(image: nil,
                                               content: (title: "", description: ""),
                                               button: (image: nil, title: ""))
        let validState: WireTransferReceiptState = .error(viewModel: mockViewModel)
        
        XCTAssertEqual(controllerSpy.displayCount, 1)
        XCTAssertEqual(controllerSpy.displayedState, validState)
    }
    
    func testShare_wheneverCalled_shouldInvokeViewControllerShareWithImage() {
        let image = UIImage()
        sut.share(image)

        XCTAssertEqual(controllerSpy.shareCount, 1)
        XCTAssertEqual(controllerSpy.sharedImage, image)
    }
}
