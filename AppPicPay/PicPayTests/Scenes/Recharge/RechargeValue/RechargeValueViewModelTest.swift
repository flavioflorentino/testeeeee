@testable import PicPay
import FeatureFlag
import XCTest

final class MockRechargeValueService: RechargeValueServicing {
    var isDepositDebitFeeEnabled: Bool
    private let cardSelected: Bool
    private let rechargeSuccess: Bool
    private let errorCode: Int
    private let pciIsEnable: Bool
    private var cvv: String?
    init(
        cardSelected: Bool = true,
        rechargeSuccess: Bool = true,
        errorCode: Int = 400,
        cvvIsEnable: Bool = false,
        cvv: String? = nil,
        isDepositDebitFeeEnabled: Bool = false
    ) {
        self.cardSelected = cardSelected
        self.rechargeSuccess = rechargeSuccess
        self.errorCode = errorCode
        self.pciIsEnable = cvvIsEnable
        self.cvv = cvv
        self.isDepositDebitFeeEnabled = isDepositDebitFeeEnabled
    }

    var defauldDebitCardId: String? {
        let cards =  try! MockCodable<[CardBank]>().loadCodableObject(resource: "CardBank")
        return cards.first?.id
    }

    func pciCvvIsEnable(cardBank: CardBank, cardValue: Double) -> Bool {
        return pciIsEnable
    }

    func saveCvvCard(id: String, value: String?) {
        cvv = value
    }

    func cvvCard(id: String) -> String? {
        return cvv
    }

    func getDebitCardSelected() -> CardBank? {
        guard cardSelected else {
            return nil
        }
        let cards =  try! MockCodable<[CardBank]>().loadCodableObject(resource: "CardBank")
        return cards.first
    }

    func makeRecharge(method: RechargeMethod, value: NSDecimalNumber, security: String, completion: @escaping (Result<Recharge, NSError>) -> Void) {
        guard rechargeSuccess else {
            let error = PicPayError(message: "Teste", code: String(errorCode))
            completion(.failure(error))
            return
        }
        let recharge = try! MockCodable<Recharge>().loadCodableObject(resource: "RechargeDeposit", typeDecoder: .useDefaultKeys)
        completion(.success(recharge))
    }

    func getCardsBank() -> [CardBank] {
        []
    }
}

final class MockRechargePaymentService: RechargePaymentServicing {
    private let authenticateSuccess: Bool
    private let errorCode: Int
    init(authenticateSuccess: Bool = true, errorCode: Int = 400) {
        self.authenticateSuccess = authenticateSuccess
        self.errorCode = errorCode
    }

    func authenticatedRecharge(request: RechargeRequest, completion: @escaping (Result<Void, PicPayError>) -> Void) {
        guard authenticateSuccess else {
            if errorCode == 412 {
                let dict = MockJSON().load(resource: "identifyShopper")
                let error = PicPayError(message: "3DSTransaction", code: String(AdyenConstant.code), dictionary: dict.dictionaryObject! as NSDictionary)
                completion(.failure(error))
            } else {
                let error = PicPayError(message: "Teste", code: String(errorCode))
                completion(.failure(error))
            }
            return
        }

        completion(.success(()))
    }
}

final class MockRechargeValueViewController: NSObject, ADYChallengeDelegate, RechargeValueViewModelOutputs {
    var coordinator: MockRechargeValueCoordenator?
    var calledDidNextStep = false
    var calledDidReceiveAnError = false
    var calledOriginalRequestToken = false
    var calledOriginalRequestPush = false
    var calledOriginalWebSuccess = false
    var calledShowAdyenWarning = false
    var calledChallengeDidFinish = false
    var calledChallengeDidFailWithError = false
    var calledShowLoading = false
    var calledDisplayPasswordPopupCount = 0

    var didNextStepAction: RechargeValueAction?
    var error: Error?

    var titleOriginal: String?
    var messageOriginal: String?

    func didNextStep(action: RechargeValueAction) {
        calledDidNextStep = true
        didNextStepAction = action
        coordinator?.perform(action: action)
    }

    func didReceiveAnError(error: NSError) {
        calledDidReceiveAnError = true
        self.error = error
    }
    
    func didReceiveAnError(error: PicPayError) {
        calledDidReceiveAnError = true
        self.error = error
    }

    func originalRequestToken(error: NSError) {
        calledOriginalRequestToken = true
        self.error = error
    }

    func originalRequestPush(title: String, message: String) {
        calledOriginalRequestPush = true
        titleOriginal = title
        messageOriginal = message
    }

    func originalWebSuccess() {
        calledOriginalWebSuccess = true
    }

    func showAdyenWarning() {
        calledShowAdyenWarning = true
    }

    func challengeDidFinish(with result: ADYChallengeResult) {
        calledChallengeDidFinish = true
    }

    func challengeDidFailWithError(_ error: Error) {
        calledChallengeDidFailWithError = true
        self.error = error
    }

    func showLoading() {
        calledShowLoading = true
    }

    func displayPasswordPopup() {
        calledDisplayPasswordPopupCount += 1
    }
}

final class MockRechargeTransferSummary: UIViewController {
    var rechargeCoordinator: RechargeValueCompletionCoordinating?
    convenience init(rechargeCoordinator: RechargeValueCompletionCoordinating) {
        self.init(nibName: nil, bundle: nil)
        self.rechargeCoordinator = rechargeCoordinator
        let feeInfo = RechargeFeeInfo(chargedValue: "", serviceFee: "")
        rechargeCoordinator.didCompletePayment(with: feeInfo)
        self.navigationController?.popViewController(animated: false)
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

final class MockRechargeValueCoordenator: RechargeValueCoordinating, RechargeValueCompletionCoordinating {
    var viewController: UIViewController?

    var onContinuePayment: ((RechargeFeeInfo) -> Void)?

    func perform(action: RechargeValueAction) {
        switch action {
        case let .showRechargeDebitFee(_, completion):
            self.onContinuePayment = completion
            let controller = MockRechargeTransferSummary(rechargeCoordinator: self)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        default:
            print("")
        }
    }
    func didCompletePayment(with info: RechargeFeeInfo) {
        onContinuePayment?(info)
    }
}

final class RechargeValueViewModelTest: XCTestCase {
    func createModel(type: RechargeMethodType = .bill) -> RechargeMethod {
        let methods = try! MockDecodable<[RechargeMethod]>().loadCodableObject(resource: "RechargeMethods", typeDecoder: .useDefaultKeys)
        switch type {
        case .bill:
            return methods[0]
        case .bank:
            return methods[1].options![0]
        case .original:
            return methods[2]
        case .debit:
            return methods[3]
        case .emergencyAid:
            return methods[5]
        }
    }
    
    func makeSut(
        with modelMock: RechargeMethod? = nil,
        serviceMock: RechargeValueServicing? = nil,
        adyenMock: AdyenServicing? = nil,
        paymentMock: RechargePaymentServicing? = nil,
        controllerMock: MockRechargeValueViewController? = nil
    ) -> RechargeValueViewModel {
        let featureManager = FeatureManagerMock()
        let dependenciesMock = DependencyContainerMock(featureManager)
        
        let adyen = adyenMock ?? MockAdyenService()
        let model = modelMock ?? createModel()
        let service = serviceMock ?? MockRechargeValueService()
        let paymentService = paymentMock ?? MockRechargePaymentService()
        let sut = RechargeValueViewModel(model: model, service: service, paymentService: paymentService, dependencies: dependenciesMock, adyen: adyen)
        
        sut.outputs = controllerMock ?? MockRechargeValueViewController()
        
        return sut
    }

    func testHeaderInfoBankShouldReturnBank() {
        let model = createModel(type: .bank)
        let sut = makeSut(with: model)

        let result = sut.inputs.headerInfo()

        XCTAssertEqual(result.title, RechargeLocalizable.bankTransfer.text)
        XCTAssertEqual(result.desc!, model.optionSelectorTitle)
    }

    func testHeaderInfoOrignalShouldReturnBank() {
        let model = createModel(type: .original)
        let sut = makeSut(with: model)
        
        let result = sut.inputs.headerInfo()

        XCTAssertEqual(result.title, RechargeLocalizable.bankTransfer.text)
        XCTAssertEqual(result.desc!, model.optionSelectorTitle)
    }

    func testHeaderInfoBillShouldReturnBill() {
        let model = createModel(type: .bill)
        let sut = makeSut(with: model)
        
        let result = sut.inputs.headerInfo()

        XCTAssertEqual(result.title, RechargeLocalizable.billTransfer.text)
        XCTAssertNil(result.desc)
    }

    func testHeaderInfoDebitShouldReturnDebit() {
        let model = createModel(type: .debit)
        let sut = makeSut(with: model)
        
        let result = sut.inputs.headerInfo()


        XCTAssertEqual(result.title, RechargeLocalizable.debitTransfer.text)
        XCTAssertNil(result.desc)
    }

    func testTitleButtonBankShouldReturnAddMoney() {
        let model = createModel(type: .bank)
        let sut = makeSut(with: model)
        
        let result = sut.inputs.titleButton()

        XCTAssertEqual(result, RechargeLocalizable.addMoney.text)
    }

    func testTitleButtonOriginalShouldReturnAddMoney() {
        let model = createModel(type: .original)
        let sut = makeSut(with: model)
        
        let result = sut.inputs.titleButton()

        XCTAssertEqual(result, RechargeLocalizable.addMoney.text)
    }

    func testTitleButtonDebitShouldReturnAddMoney() {
        let model = createModel(type: .debit)
        let sut = makeSut(with: model)
        
        let result = sut.inputs.titleButton()

        XCTAssertEqual(result, RechargeLocalizable.addMoney.text)
    }

    func testTitleButtonBillShouldReturnAddBill() {
        let model = createModel(type: .bill)
        let sut = makeSut(with: model)
        
        let result = sut.inputs.titleButton()

        XCTAssertEqual(result, RechargeLocalizable.addBill.text)
    }

    func testDidTapChangeCardShouldCallDidNextStep() {
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(controllerMock: mockViewController)
        
        sut.inputs.didTapChangeCard()

        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch mockViewController.didNextStepAction! {
        case .changeCard:
            XCTAssertTrue(true)

        default:
            XCTFail()
        }
    }

    func testShouldEnableButtonShouldReturnFalse() {
        let sut = makeSut()
        
        let result = sut.inputs.shouldEnableButton(forValue: 0.0)
        XCTAssertFalse(result)
    }

    func testShouldEnableButtonShouldReturnTrue() {
        let sut = makeSut()
        
        let result = sut.inputs.shouldEnableButton(forValue: 1.0)
        XCTAssertTrue(result)
    }

    func testDisplayCardShouldReturnCard() {
        let model = createModel(type: .debit)
        let service = MockRechargeValueService(cardSelected: true)
        let sut = makeSut(with: model, serviceMock: service)
        
        let result = sut.inputs.displayCard()

        XCTAssertEqual(result.title, "Mepa")
        XCTAssertEqual(result.image, "https://cdn.picpay.com/apps/picpay/imgs/visa.png")
    }

    func testDisplayCardShouldCallDidReceiveAnError() {
        let service = MockRechargeValueService(cardSelected: true)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(serviceMock: service, controllerMock: mockViewController)

        let result = sut.inputs.displayCard()

        XCTAssertTrue(mockViewController.calledDidReceiveAnError)
        XCTAssertNotNil(mockViewController.error)
        XCTAssertTrue(result.title.isEmpty)
        XCTAssertTrue(result.image.isEmpty)
    }

    func testShowOriginalWarningShouldCallDidNextStep() {
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(controllerMock: mockViewController)
        
        sut.inputs.showOriginalWarning()

        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch mockViewController.didNextStepAction! {
        case .close:
            XCTAssertTrue(true)

        default:
            XCTFail()
        }
    }

    func testRechargeIsAuthenticatedShouldReturnTrue() {
        let model = createModel(type: .debit)
        let service = MockRechargeValueService(cardSelected: true)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(with: model, serviceMock: service, controllerMock: mockViewController)

        let result = sut.inputs.rechargeIsAuthenticated()

        XCTAssertTrue(result)
    }

    func testRechargeIsAuthenticatedShouldReturnFalse() {
        let model = createModel(type: .bill)
        let service = MockRechargeValueService(cardSelected: true)
        let sut = makeSut(with: model, serviceMock: service)
        
        let result = sut.inputs.rechargeIsAuthenticated()

        XCTAssertFalse(result)
    }

    func testContinueOn3DSTransactionShouldCallDidNextStepRechargeAuthenticatedFailure() throws {
        let adyen = MockAdyenService(transactionSuccess: false, errorCode: 400)
        let model = createModel(type: .bill)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(with: model, adyenMock: adyen, controllerMock: mockViewController)

        sut.inputs.continueOn3DSTransaction()

        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch try mockViewController.didNextStepAction.safe() {
        case .rechargeAuthenticatedFailure:
            XCTAssertTrue(true)

        default:
            XCTFail()
        }
    }

    func testContinueOn3DSTransactionShouldCallDidNextStepRechargeAuthenticatedFaq() throws {
        let adyen = MockAdyenService(transactionSuccess: false, errorCode: AdyenConstant.notAuthorized)
        let model = createModel(type: .bill)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(with: model, adyenMock: adyen, controllerMock: mockViewController)

        sut.inputs.continueOn3DSTransaction()

        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch try mockViewController.didNextStepAction.safe() {
        case .rechargeAuthenticatedFaq:
            XCTAssertTrue(true)

        default:
            XCTFail()
        }
    }

    func testContinueOn3DSTransactionShouldCallDidNextStep() {
        let adyen = MockAdyenService(transactionSuccess: true)
        let model = createModel(type: .bill)
        let service = MockRechargeValueService(cardSelected: true)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(with: model, serviceMock: service, adyenMock: adyen, controllerMock: mockViewController)

        sut.inputs.continueOn3DSTransaction()

        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch mockViewController.didNextStepAction! {
        case .rechargeAuthenticatedSuccess:
            XCTAssertTrue(true)

        default:
            XCTFail()
        }
    }

    func testChallengeFinishSuccessShouldCallDidNextStep() {
        let adyen = MockAdyenService(completeSuccess: true)
        let model = createModel(type: .bill)
        let service = MockRechargeValueService(cardSelected: true)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(with: model, serviceMock: service, adyenMock: adyen, controllerMock: mockViewController)

        sut.inputs.challengeFinishSuccess()

        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch mockViewController.didNextStepAction! {
        case .rechargeAuthenticatedSuccess:
            XCTAssertTrue(true)

        default:
            XCTFail()
        }
    }

    func testChallengeFinishSuccessShouldCallDidNextStepRechargeAuthenticatedFailure() throws {
        let adyen = MockAdyenService(completeSuccess: false, errorCode: 400)
        let model = createModel(type: .bill)
        let service = MockRechargeValueService(cardSelected: true)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(with: model, serviceMock: service, adyenMock: adyen, controllerMock: mockViewController)

        sut.inputs.challengeFinishSuccess()

        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch try mockViewController.didNextStepAction.safe() {
        case .rechargeAuthenticatedFailure:
            XCTAssertTrue(true)

        default:
            XCTFail()
        }
    }

    func testChallengeFinishSuccessShouldCallDidNextStepRechargeAuthenticatedFaq() throws {
        let adyen = MockAdyenService(completeSuccess: false, errorCode: AdyenConstant.notAuthorized)
        let model = createModel(type: .bill)
        let service = MockRechargeValueService(cardSelected: true)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(with: model, serviceMock: service, adyenMock: adyen, controllerMock: mockViewController)

        sut.inputs.challengeFinishSuccess()

        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch try mockViewController.didNextStepAction.safe() {
        case .rechargeAuthenticatedFaq:
            XCTAssertTrue(true)

        default:
            XCTFail()
        }
    }

    func testChallengeFinishErrorNotShouldCallDidReceiveAnError() {
        let adyen = MockAdyenService(completeSuccess: false)
        let model = createModel(type: .bill)
        let service = MockRechargeValueService(cardSelected: true)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(with: model, serviceMock: service, adyenMock: adyen, controllerMock: mockViewController)

        let error = NSError(domain: "3DSTransaction", code: Int(AdyenConstant.errorAdyChallengeCancelled), userInfo: nil)
        sut.inputs.challengeFinishError(error: error)

        XCTAssertFalse(mockViewController.calledDidReceiveAnError)
    }

    func testChallengeFinishErrorShouldCallDidReceiveAnError() {
        let adyen = MockAdyenService(completeSuccess: false)
        let service = MockRechargeValueService(cardSelected: true)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(serviceMock: service, adyenMock: adyen, controllerMock: mockViewController)

        let error = NSError(domain: "3DSTransaction", code: 10, userInfo: nil)
        sut.inputs.challengeFinishError(error: error)

        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch mockViewController.didNextStepAction! {
        case .rechargeAuthenticatedFailure:
            XCTAssertTrue(true)

        default:
            XCTFail()
        }
    }

    func testMakeRechargeShouldCallDidReceiveAnError() {
        let service = MockRechargeValueService(rechargeSuccess: false)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(serviceMock: service, controllerMock: mockViewController)

        sut.makeRecharge(value: 10.0, token: "")

        XCTAssertTrue(mockViewController.calledDidReceiveAnError)
        XCTAssertNotNil(mockViewController.error)
    }

    func testMakeRechargeShouldCallDidNextStep() {
        let service = MockRechargeValueService(rechargeSuccess: true)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(serviceMock: service, controllerMock: mockViewController)

        sut.makeRecharge(value: 10.0, token: "")

        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch mockViewController.didNextStepAction! {
        case .rechargeSuccess:
            XCTAssertTrue(true)

        default:
            XCTFail()
        }
    }

    func testMakeRechargeShouldCallDidNextStepOriginal() {
        let model = createModel(type: .original)
        let service = MockRechargeValueService(rechargeSuccess: true)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(with: model, serviceMock: service, controllerMock: mockViewController)

        sut.makeRecharge(value: 10.0, token: "")

        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch mockViewController.didNextStepAction! {
        case .rechargeOriginalSuccess:
            XCTAssertTrue(true)

        default:
            XCTFail()
        }
    }

    func testMakeRechargeShouldCallOriginalRequestToken() {
        let model = createModel(type: .original)
        let service = MockRechargeValueService(rechargeSuccess: false, errorCode: WSConsumerMakeRechargeOpenBank.token.rawValue)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(with: model, serviceMock: service, controllerMock: mockViewController)

        sut.makeRecharge(value: 10.0, token: "")

        XCTAssertTrue(mockViewController.calledOriginalRequestToken)
    }

    func testMakeRechargeShouldCallOriginalRequestPush() {
        let model = createModel(type: .original)
        let service = MockRechargeValueService(rechargeSuccess: false, errorCode: WSConsumerMakeRechargeOpenBank.push.rawValue)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(with: model, serviceMock: service, controllerMock: mockViewController)

        sut.makeRecharge(value: 10.0, token: "")

        XCTAssertTrue(mockViewController.calledOriginalRequestPush)
    }

    func testMakeRechargeOriginalShouldCallDidReceiveAnError() {
        let model = createModel(type: .original)
        let service = MockRechargeValueService(rechargeSuccess: false)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(with: model, serviceMock: service, controllerMock: mockViewController)

        sut.makeRecharge(value: 10.0, token: "")

        XCTAssertTrue(mockViewController.calledDidReceiveAnError)
        XCTAssertNotNil(mockViewController.error)
    }

    func testMakeRechargeShouldCallDidReceiveAnErrorOpenOriginal() throws {
        let model = createModel(type: .original)
        let service = MockRechargeValueService(rechargeSuccess: false, errorCode: 401)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(with: model, serviceMock: service, controllerMock: mockViewController)

        sut.makeRecharge(value: 10.0, token: "")

        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch try mockViewController.didNextStepAction.safe() {
        case .openOriginal(_, _, _):
            XCTAssertTrue(true)

        default:
            XCTFail()
        }
    }

    func testMakeAuthenticatedRechargeCallDidNextStep() {
        let model = createModel(type: .debit)
        let service = MockRechargeValueService(cardSelected: true)
        let paymentService = MockRechargePaymentService(authenticateSuccess: true)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(with: model, serviceMock: service, paymentMock: paymentService, controllerMock: mockViewController)

        sut.makeAuthenticatedRecharge(value: 20.0, password: "1234", biometry: true)

        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch mockViewController.didNextStepAction! {
        case .rechargeAuthenticatedSuccess:
            XCTAssertTrue(true)

        default:
            XCTFail()
        }
    }

    func testMakeAuthenticatedRechargeCallDidReceiveAnError() {
        let model = createModel(type: .debit)
        let service = MockRechargeValueService(cardSelected: true)
        let paymentService = MockRechargePaymentService(authenticateSuccess: false)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(with: model, serviceMock: service, paymentMock: paymentService, controllerMock: mockViewController)

        sut.makeAuthenticatedRecharge(value: 20.0, password: "1234", biometry: true)

        XCTAssertTrue(mockViewController.calledDidReceiveAnError)
        XCTAssertNotNil(mockViewController.error)
    }

    func testMakeAuthenticatedRechargeCallShowAdyenWarning() {
        let adyen = MockAdyenService(firstAccess: true)
        let model = createModel(type: .debit)
        let service = MockRechargeValueService(cardSelected: true)
        let paymentService = MockRechargePaymentService(authenticateSuccess: false, errorCode: 412)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(with: model, serviceMock: service, adyenMock: adyen, paymentMock: paymentService, controllerMock: mockViewController)

        sut.makeAuthenticatedRecharge(value: 20.0, password: "1234", biometry: true)

        XCTAssertTrue(mockViewController.calledShowAdyenWarning)
    }

    func testMakeAuthenticatedRechargeCallAuthenticatedRechargeSuccess() {
        let adyen = MockAdyenService(firstAccess: false, transactionSuccess: true)
        let model = createModel(type: .debit)
        let service = MockRechargeValueService(cardSelected: true)
        let paymentService = MockRechargePaymentService(authenticateSuccess: false, errorCode: 412)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(with: model, serviceMock: service, adyenMock: adyen, paymentMock: paymentService, controllerMock: mockViewController)

        sut.makeAuthenticatedRecharge(value: 20.0, password: "1234", biometry: true)

        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch mockViewController.didNextStepAction! {
        case .rechargeAuthenticatedSuccess:
            XCTAssertTrue(true)

        default:
            XCTFail()
        }
    }

    func testMakeAuthenticatedShouldCallRechargeOpenFlowInsertCvv() {
        let adyen = MockAdyenService(firstAccess: false, transactionSuccess: true)
        let model = createModel(type: .debit)
        let service = MockRechargeValueService(cvvIsEnable: true)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(with: model, serviceMock: service, adyenMock: adyen, controllerMock: mockViewController)

        sut.makeAuthenticatedRecharge(value: 20.0, password: "1234", biometry: true)

        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch mockViewController.didNextStepAction! {
        case .openCvv:
            XCTAssertTrue(true)

        default:
            XCTFail()
        }
    }

    func testMakeAuthenticatedNotShouldCallRechargeOpenFlowInsertCvvWhenPciDisable() {
        let adyen = MockAdyenService(firstAccess: false, transactionSuccess: true)
        let model = createModel(type: .debit)
        let service = MockRechargeValueService(cvvIsEnable: false)
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(with: model, serviceMock: service, adyenMock: adyen, controllerMock: mockViewController)

        sut.makeAuthenticatedRecharge(value: 20.0, password: "1234", biometry: true)

        switch mockViewController.didNextStepAction! {
        case .openCvv:
            XCTFail()
        default:
            XCTAssertTrue(true)
        }
    }

    func testCheckoutAuthenticatedProcess_ShouldCallDidNextStep() throws {
        let model = createModel(type: .emergencyAid)
        let service = MockRechargeValueService(cvvIsEnable: true)
        let coordinator = MockRechargeValueCoordenator()
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(with: model, serviceMock: service, controllerMock: mockViewController)

        service.isDepositDebitFeeEnabled = true
        mockViewController.coordinator = coordinator

        sut.checkoutAuthenticatedProcess(value: 20.0)
        XCTAssertEqual(mockViewController.calledDisplayPasswordPopupCount, 1)

        switch try XCTUnwrap(mockViewController.didNextStepAction) {
        case .showRechargeDebitFee(value: 20.0, _):
            XCTAssertTrue(true)

        default:
            XCTFail()
        }
    }

    func testCheckoutAuthenticatedProcess_ShouldCallDisplayPasswordPopup() throws {
        let model = createModel(type: .emergencyAid)
        let service = MockRechargeValueService(cvvIsEnable: true)
        let coordinator = MockRechargeValueCoordenator()
        let mockViewController = MockRechargeValueViewController()
        let sut = makeSut(with: model, serviceMock: service, controllerMock: mockViewController)
        
        service.isDepositDebitFeeEnabled = false
        mockViewController.coordinator = coordinator

        sut.checkoutAuthenticatedProcess(value: 20.0)
        XCTAssertEqual(mockViewController.calledDisplayPasswordPopupCount, 1)

        XCTAssertNil(mockViewController.didNextStepAction)
    }
}

extension RechargeValueViewModelTest {
    enum RechargeMethodType {
        case bill
        case bank
        case original
        case debit
        case emergencyAid
    }
}
