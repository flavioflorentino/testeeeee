import Core
import Foundation
import XCTest
@testable import PicPay

final class RechargeValueLoaderServiceMock: RechargeValueLoaderServicing {
    var fetchAddValueDetailsExpectedResult: Result<RechargeDetail, ApiError>?
    
    func fetchAddValueDetails(methodId: Int, completion: @escaping (Result<RechargeDetail, ApiError>) -> Void) {
        guard let expectedResult = fetchAddValueDetailsExpectedResult else {
            XCTFail("Expected result not defined")
            return
        }
        completion(expectedResult)
    }
}

final class RechargeValueLoaderPresenterSpy: RechargeValueLoaderPresenting {
    var viewController: RechargeValueLoaderDisplaying?
    
    private(set) var didNextStepActions = [RechargeValueLoaderAction]()
    func didNextStep(action: RechargeValueLoaderAction) {
        didNextStepActions.append(action)
    }
    
    private(set) var showLoadingCount = 0
    func showLoading() {
        showLoadingCount += 1
    }
    
    private(set) var showErrorCount = 0
    func showError(error: ApiError) {
        showErrorCount += 1
    }
}

final class RechargeValueLoaderInteractorTests: XCTestCase {
    private lazy var serviceMock = RechargeValueLoaderServiceMock()
    private lazy var presenterSpy = RechargeValueLoaderPresenterSpy()
    private lazy var sut = RechargeValueLoaderInteractor(methodId: 1, service: serviceMock, presenter: presenterSpy)
    
    func testLoadDetails_onSuccess_shouldInvokePresenterDidNextStepWithAddValueAction() throws {
        let rechargeType = try XCTUnwrap(RechargeMethod.RechargeType.init(rawValue: 1))
        let validRechargeDetail = try JSONDecoder.decode(Data("{}".utf8), to: RechargeDetail.self)
        let validRechargeMethod = RechargeMethod(rechargeType: rechargeType, name: "", description: "")
        serviceMock.fetchAddValueDetailsExpectedResult = .success(validRechargeDetail)
        
        sut.loadDetails()
        
        XCTAssertEqual(presenterSpy.showLoadingCount, 1)
        XCTAssertEqual(presenterSpy.showErrorCount, 0)
        XCTAssertEqual(presenterSpy.didNextStepActions, [.addValue(method: validRechargeMethod)])
    }
    
    func testLoadDetails_onError_shouldInvokePresenterShowError() throws {
        serviceMock.fetchAddValueDetailsExpectedResult = .failure(.serverError)
        
        sut.loadDetails()
        
        XCTAssertEqual(presenterSpy.showLoadingCount, 1)
        XCTAssertEqual(presenterSpy.showErrorCount, 1)
        XCTAssertTrue(presenterSpy.didNextStepActions.isEmpty)
    }
}
