import Core
import UI
import XCTest
@testable import PicPay

final class RechargeValueLoaderCoordinatorSpy: RechargeValueLoaderCoordinating {
    var viewController: UIViewController?
    
    private(set) var performedActions = [RechargeValueLoaderAction]()
    func perform(action: RechargeValueLoaderAction) {
        performedActions.append(action)
    }
}

final class RechargeValueLoaderViewControllerSpy: RechargeValueLoaderDisplaying {
    private(set) var showLoadingCount = 0
    func showLoading() {
        showLoadingCount += 1
    }
    
    private(set) var shownErrors = [StatefulErrorViewModel]()
    func showError(_ error: StatefulErrorViewModel) {
        shownErrors.append(error)
    }
}

final class RechargeValueLoaderPresenterTests: XCTestCase {
    private lazy var coordinatorSpy = RechargeValueLoaderCoordinatorSpy()
    private lazy var controllerSpy = RechargeValueLoaderViewControllerSpy()
    private lazy var sut: RechargeValueLoaderPresenter = {
        let presenter =  RechargeValueLoaderPresenter(coordinator: coordinatorSpy)
        presenter.viewController = controllerSpy
        return presenter
    }()
    
    func testShowLoading_shouldInvokeControllerShowLoading() {
        sut.showLoading()
        
        XCTAssertEqual(controllerSpy.showLoadingCount, 1)
    }
    
    func testShowError_shouldInvokeControllerShowError() {
        let validError = ApiError.serverError
        let validErrorViewModel = StatefulErrorViewModel(
            image: Assets.Recharge.iluWireTransferReceiptErrorClock.image,
            content: (title: validError.picpayError.title,
                      description: validError.picpayError.errorDescription),
            button: (image: nil, title: Strings.RechargeWireTransfer.receiptLoadingErrorAction)
        )
        
        sut.showError(error: validError)
        
        XCTAssertEqual(controllerSpy.shownErrors, [validErrorViewModel])
    }
    
    func testDidNextStep_shouldInvokeCoordinatorPerformWithCorrectAction() throws {
        let rechargeType = try XCTUnwrap(RechargeMethod.RechargeType.init(rawValue: 1))
        let validRechargeMethod = RechargeMethod(rechargeType: rechargeType,
                                                 name: "Test Name",
                                                 description: "Test Description")
        let validAction = RechargeValueLoaderAction.addValue(method: validRechargeMethod)
        
        sut.didNextStep(action: validAction)
        
        XCTAssertEqual(coordinatorSpy.performedActions, [validAction])
    }
}
