@testable import PicPay
import XCTest

class FakeRechargeBankService: RechargeBankServicing {
    private let uploadPhotoIsSuccess: Bool
    private let cancelIsSuccess: Bool
    
    init(uploadPhotoIsSuccess: Bool = true, cancelIsSuccess: Bool = true) {
        self.uploadPhotoIsSuccess = uploadPhotoIsSuccess
        self.cancelIsSuccess = cancelIsSuccess
    }

    func uploadPhoto(rechargeId: String, image: UIImage, completion: @escaping (Result<Recharge, PicPayError>) -> Void) {
        guard uploadPhotoIsSuccess else {
            let error = PicPayError(message: "Error")
            completion(.failure(error))
            return
        }
        let recharge = try! MockCodable<Recharge>().loadCodableObject(resource: "RechargeDepositTedUpload", typeDecoder: .useDefaultKeys)
        completion(.success(recharge))
    }
    
    func cancel(id: String, completion: @escaping (Result<Void, PicPayError>) -> Void) {
        guard cancelIsSuccess else {
            let error = PicPayError(message: "Error")
            completion(.failure(error))
            return
        }
        completion(.success(()))
    }
}

class FakeRechargeBankViewController: RechargeBankViewModelOutputs {
    var calledReloadData = false
    var calledDidReceiveAnError = false
    var calledDidNextStep = false
    var calledShowWarningUpload = false
    var calledShowOptionsUpload = false
    var calledSuccessSendPhoto = false
    
    var error: PicPayError?
    var didNextStepAction: RechargeBankAction?
    
    func reloadData() {
        calledReloadData = true
    }
    
    func didReceiveAnError(error: PicPayErrorDisplayable) {
        calledDidReceiveAnError = true
        self.error = error as? PicPayError
    }
    
    func didNextStep(action: RechargeBankAction) {
        calledDidNextStep = true
        didNextStepAction = action
    }
    
    func showWarningUpload() {
        calledShowWarningUpload = true
    }
    
    func showOptionsUpload() {
        calledShowOptionsUpload = true
    }
    
    func successSendPhoto() {
        calledSuccessSendPhoto = true
    }
}

class RechargeBankViewModelTest: XCTestCase {
    private func createModel(isTed: Bool = false) -> Recharge {
        guard isTed else {
            return try! MockCodable<Recharge>().loadCodableObject(resource: "RechargeDeposit", typeDecoder: .useDefaultKeys)
        }
        
        return try! MockCodable<Recharge>().loadCodableObject(resource: "RechargeDepositTedUpload", typeDecoder: .useDefaultKeys)
    }
    
    func testViewDidLoadShouldCallReloadData() {
        let service = FakeRechargeBankService()
        let viewModel = RechargeBankViewModel(model: createModel(), service: service)
        let mockViewController = FakeRechargeBankViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        XCTAssert(mockViewController.calledReloadData)
    }
    
    func testHeaderShouldReturnHeaderBank() {
        let service = FakeRechargeBankService()
        let viewModel = RechargeBankViewModel(model: createModel(isTed: false), service: service)
        let mockViewController = FakeRechargeBankViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        let result = viewModel.inputs.header()
        XCTAssertEqual(result.0, RechargeLocalizable.titleHeaderBank.text)
        XCTAssertEqual(result.1, RechargeLocalizable.highlightHeaderBank.text)
    }
    
    func testHeaderShouldReturnHeaderTed() {
        let service = FakeRechargeBankService()
        let viewModel = RechargeBankViewModel(model: createModel(isTed: true), service: service)
        let mockViewController = FakeRechargeBankViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        let result = viewModel.inputs.header()
        XCTAssertEqual(result.0, RechargeLocalizable.headerBank.text)
        XCTAssertEqual(result.1, RechargeLocalizable.headerBankExtra.text)
    }
    
    func testButonTitleShouldReturnUploadText() {
        let service = FakeRechargeBankService()
        let viewModel = RechargeBankViewModel(model: createModel(isTed: true), service: service)
        let mockViewController = FakeRechargeBankViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        let result = viewModel.inputs.buttonTitle()
        XCTAssertEqual(result, RechargeLocalizable.resubmitPhoto.text)
    }
    
    func testButonTitleShouldReturnNoUploadText() {
        let service = FakeRechargeBankService()
        let viewModel = RechargeBankViewModel(model: createModel(isTed: false), service: service)
        let mockViewController = FakeRechargeBankViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        let result = viewModel.inputs.buttonTitle()
        XCTAssertEqual(result, RechargeLocalizable.sendPhoto.text)
    }
    
    func testFooterShouldReturnMockFooter() {
        let service = FakeRechargeBankService()
        let model = createModel()
        let viewModel = RechargeBankViewModel(model: model, service: service)
        let mockViewController = FakeRechargeBankViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        let result = viewModel.inputs.footer
        XCTAssertEqual(result, model.instructions.text)
    }
    
    func testRechargeBankModelShouldReturnMockRechargeBank() {
        let service = FakeRechargeBankService()
        let model = createModel()
        let viewModel = RechargeBankViewModel(model: model, service: service)
        let mockViewController = FakeRechargeBankViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        let result = viewModel.inputs.rechargeBankModel
        XCTAssertNotNil(result)
        XCTAssertEqual(result.bankName, model.bankName)
        XCTAssertEqual(result.bankImage, model.bankImgUrl)
        XCTAssertEqual(result.value, model.value)
    }
    
    func testCancelRechargeShouldCallActionCancelRecharge() {
        let service = FakeRechargeBankService(cancelIsSuccess: true)
        let viewModel = RechargeBankViewModel(model: createModel(), service: service)
        let mockViewController = FakeRechargeBankViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.cancelRecharge()
        let result = mockViewController.didNextStepAction
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        XCTAssertEqual(result, RechargeBankAction.cancelRecharge)
    }
    
    func testCancelRechargeShouldCallDidReceiveAnError() {
        let service = FakeRechargeBankService(cancelIsSuccess: false)
        let viewModel = RechargeBankViewModel(model: createModel(), service: service)
        let mockViewController = FakeRechargeBankViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.cancelRecharge()
        let result = mockViewController.error
        
        XCTAssertTrue(mockViewController.calledDidReceiveAnError)
        XCTAssertNotNil(result)
    }
    
    func testDidTapUploadPhotoShouldCallShowWarningUpload() {
        let service = FakeRechargeBankService()
        let viewModel = RechargeBankViewModel(model: createModel(isTed: true), service: service)
        let mockViewController = FakeRechargeBankViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.didTapUploadPhoto()
        
        XCTAssertTrue(mockViewController.calledShowWarningUpload)
    }
    
    func testDidTapUploadPhotoShouldCallShowOptionsUpload() {
        let service = FakeRechargeBankService()
        let viewModel = RechargeBankViewModel(model: createModel(isTed: false), service: service)
        let mockViewController = FakeRechargeBankViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.didTapUploadPhoto()
        
        XCTAssertTrue(mockViewController.calledShowOptionsUpload)
    }
    
    func testUploadRechargePhotoShouldCallReloadData() {
        let service = FakeRechargeBankService(uploadPhotoIsSuccess: true)
        let viewModel = RechargeBankViewModel(model: createModel(), service: service)
        let mockViewController = FakeRechargeBankViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.uploadRechargePhoto(image: #imageLiteral(resourceName: "upgrade_wait"))
        
        XCTAssertTrue(mockViewController.calledReloadData)
    }
    
    func testUploadRechargePhotoShouldCallSuccessSendPhoto() {
        let service = FakeRechargeBankService(uploadPhotoIsSuccess: true)
        let viewModel = RechargeBankViewModel(model: createModel(), service: service)
        let mockViewController = FakeRechargeBankViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.uploadRechargePhoto(image: #imageLiteral(resourceName: "upgrade_wait"))
        
        XCTAssertTrue(mockViewController.calledSuccessSendPhoto)
    }
    
    func testUploadRechargePhotoShouldCallDidReceiveAnError() {
        let service = FakeRechargeBankService(uploadPhotoIsSuccess: false)
        let viewModel = RechargeBankViewModel(model: createModel(), service: service)
        let mockViewController = FakeRechargeBankViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.uploadRechargePhoto(image: #imageLiteral(resourceName: "upgrade_wait"))
        let result = mockViewController.error
        
        XCTAssertTrue(mockViewController.calledDidReceiveAnError)
        XCTAssertNotNil(result)
    }
    
    func testCloseShouldCallActionClose() {
        let service = FakeRechargeBankService(uploadPhotoIsSuccess: false)
        let viewModel = RechargeBankViewModel(model: createModel(), service: service)
        let mockViewController = FakeRechargeBankViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.close()
        let result = mockViewController.didNextStepAction
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        XCTAssertEqual(result, RechargeBankAction.close)
    }
}
