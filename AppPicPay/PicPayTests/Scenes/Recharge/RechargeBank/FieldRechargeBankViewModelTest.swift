@testable import PicPay
import XCTest

class FakeRechargeBankView: FieldRechargeBankViewModelOutputs {
    var calledReloadData = false
    
    func reloadData() {
        calledReloadData = true
    }
}

class FieldRechargeBankViewModelTest: XCTestCase {
    func testTitleShouldReturnMockTitle() {
        let viewModel = FieldRechargeBankViewModel()
        let view = FakeRechargeBankView()
        
        viewModel.outputs = view
        viewModel.viewConfigure(model: FieldRechargeRow(title: "title", description: "")!)
        
        let result = viewModel.inputs.title
        XCTAssertEqual(result, "title")
    }
    
    func testDescriptionShouldReturnMockDescription() {
        let viewModel = FieldRechargeBankViewModel()
        let view = FakeRechargeBankView()
        
        viewModel.outputs = view
        viewModel.viewConfigure(model: FieldRechargeRow(title: "", description: "description")!)
        
        let result = viewModel.inputs.description
        XCTAssertEqual(result, "description")
    }
    
    func testModelShouldReturnNil() {
        let viewModel = FieldRechargeBankViewModel()
        let view = FakeRechargeBankView()
        let model = FieldRechargeRow(title: "", description: nil)
        
        viewModel.outputs = view
        
        XCTAssertNil(model)
    }
    
    func testHighlightShouldReturnTrue() {
        let viewModel = FieldRechargeBankViewModel()
        let view = FakeRechargeBankView()
        
        viewModel.outputs = view
        viewModel.viewConfigure(model: FieldRechargeRow(title: "", description: "", copyField: false, highlight: true)!)
        
        let result = viewModel.inputs.highlight
        XCTAssertTrue(result)
    }
    
    func testCopyFieldShouldReturnTrue() {
        let viewModel = FieldRechargeBankViewModel()
        let view = FakeRechargeBankView()
        
        viewModel.outputs = view
        viewModel.viewConfigure(model: FieldRechargeRow(title: "", description: "", copyField: true, highlight: false)!)
       
        let result = viewModel.inputs.copyField()
        XCTAssertTrue(result)
    }
    
    func testViewConfigureShouldCallReloadData() {
        let viewModel = FieldRechargeBankViewModel()
        let view = FakeRechargeBankView()
        
        viewModel.outputs = view
        viewModel.viewConfigure(model: FieldRechargeRow(title: "", description: "")!)
        
        let result = view.calledReloadData
        XCTAssertTrue(result)
    }
}
