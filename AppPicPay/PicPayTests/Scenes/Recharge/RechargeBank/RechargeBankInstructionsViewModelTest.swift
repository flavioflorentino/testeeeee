@testable import PicPay
import CoreLegacy
import XCTest

class FakeRechargeBankInstructions: RechargeBankInstructionsViewModelOutputs {
    var calledAddHeader = false
    var calledAddRow = false
    var countCalledAddRow = 0
    
    var title: String?
    var image: String?
    var value: String?
    var model: FieldRechargeRow?
    
    func addHeader(title: String, image: String) {
        calledAddHeader = true
        self.title = title
        self.image = image
    }
    
    func addRow(model: FieldRechargeRow) {
        calledAddRow = true
        countCalledAddRow += 1
        self.model = model
        
        if model.title == RechargeLocalizable.valueField.text {
            value = model.description
        }
    }
}

class RechargeBankInstructionsViewModelTest: XCTestCase {
    private func createModel() -> RechargeBank {
        let instructions = try! MockCodable<RechargeInstructions>().loadCodableObject(resource: "RechargeInstructions", typeDecoder: .useDefaultKeys)
        return RechargeBank(bankName: "Bank Name", bankImage: "Bank Image", value: "20", instructions: instructions)
    }
    
    func testViewConfigureShouldCallAddHeader() {
        let viewModel = RechargeBankInstructionsViewModel()
        let view = FakeRechargeBankInstructions()
        
        viewModel.outputs = view
        viewModel.viewConfigure(model: createModel())
        
        let result = view.calledAddHeader
        XCTAssertTrue(result)
    }
    
    func testViewConfigureNoShouldCallAddHeader() {
        let viewModel = RechargeBankInstructionsViewModel()
        let view = FakeRechargeBankInstructions()
        
        viewModel.outputs = view
        
        let result = view.calledAddHeader
        XCTAssertFalse(result)
    }
    
    func testViewConfigureShouldCallAddHeaderWithTitle() {
        let viewModel = RechargeBankInstructionsViewModel()
        let view = FakeRechargeBankInstructions()
        
        viewModel.outputs = view
        viewModel.viewConfigure(model: createModel())
        
        let result = view.title
        XCTAssertEqual(result, "Bank Name")
    }
    
    func testViewConfigureShouldCallAddHeaderWithImage() {
        let viewModel = RechargeBankInstructionsViewModel()
        let view = FakeRechargeBankInstructions()
        
        viewModel.outputs = view
        viewModel.viewConfigure(model: createModel())
        
        let result = view.image
        XCTAssertEqual(result, "Bank Image")
    }
    
    func testViewConfigureShouldCallAddRow() {
        let viewModel = RechargeBankInstructionsViewModel()
        let view = FakeRechargeBankInstructions()
        
        viewModel.outputs = view
        viewModel.viewConfigure(model: createModel())
        
        let result = view.calledAddRow
        XCTAssertTrue(result)
    }
    
    func testViewConfigureShouldReturnModelNotNil() {
        let viewModel = RechargeBankInstructionsViewModel()
        let view = FakeRechargeBankInstructions()
        
        viewModel.outputs = view
        viewModel.viewConfigure(model: createModel())
        
        let result = view.model
        XCTAssertNotNil(result)
    }
    
    func testViewConfigureNoShouldCallAddRow() {
        let viewModel = RechargeBankInstructionsViewModel()
        let view = FakeRechargeBankInstructions()
        
        viewModel.outputs = view
        
        let result = view.calledAddRow
        XCTAssertFalse(result)
    }
    
    func testViewConfigureShouldCallAddRowFourTimes() {
        let viewModel = RechargeBankInstructionsViewModel()
        let view = FakeRechargeBankInstructions()
        
        viewModel.outputs = view
        viewModel.viewConfigure(model: createModel())
        
        let result = view.countCalledAddRow
        XCTAssertEqual(result, 4)
    }
    
    func testViewConfigureShouldCallAddRowWithValue() {
        let viewModel = RechargeBankInstructionsViewModel()
        let view = FakeRechargeBankInstructions()
        
        viewModel.outputs = view
        viewModel.viewConfigure(model: createModel())
        
        let result = view.value
        XCTAssertEqual(result, CurrencyFormatter.brazillianRealString(from: NSDecimalNumber(string: "20")))
    }
}
