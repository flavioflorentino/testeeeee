@testable import PicPay

extension RechargeMethod {
    public override func isEqual(_ object: Any?) -> Bool {
        guard let comparingObject = object as? RechargeMethod else { return false }
        
        return self.typeId == comparingObject.typeId &&
            self.name == comparingObject.name &&
            self.title == comparingObject.title &&
            self.desc == comparingObject.desc &&
            self.imgUrl == comparingObject.imgUrl &&
            self.bankId == comparingObject.bankId &&
            self.registerDebit == comparingObject.registerDebit &&
            self.options == comparingObject.options &&
            self.authHeaders == comparingObject.authHeaders &&
            self.optionSelectorTitle == comparingObject.optionSelectorTitle &&
            self.authUrl == comparingObject.authUrl &&
            self.linked == comparingObject.linked &&
            self.value == comparingObject.value &&
            self.serviceCharge == comparingObject.serviceCharge &&
            self.accountAgency == comparingObject.accountAgency &&
            self.accountNumber == comparingObject.accountNumber &&
            self.consumerAvatar == comparingObject.consumerAvatar &&
            self.consumerCpf == comparingObject.consumerCpf &&
            self.consumerName == comparingObject.consumerName &&
            self.consumerUsername == comparingObject.consumerUsername
    }
    
    static func == (lhs: RechargeMethod, rhs: RechargeMethod) -> Bool {
        lhs.isEqual(rhs)
    }
}
