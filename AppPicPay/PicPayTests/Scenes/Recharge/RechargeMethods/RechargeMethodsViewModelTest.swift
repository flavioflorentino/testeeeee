import XCTest
import AnalyticsModule
@testable import PicPay

final class FakeRechargeMethodsCoordinator: RechargeMethodsCoordinating {
    private let isDebit: Bool
    var viewController: UIViewController?
    
    init(isDebit: Bool) {
        self.isDebit = isDebit
    }
    
    func perform(action: RechargeMethodsAction) {
        if case .addDebitCard(let completion) = action {
            completion(isDebit ? .debit : .credit)
        }
    }
}

final class FakeRechargeMethodsService: RechargeMethodsServicing {
    private let showDebit: Bool
    private let showAccount: Bool
    private let response: Response
    private let link: String
    private let showLoanMethodMock: Bool
        
    init(
        showDebit: Bool = true,
        showPicpayAccount: Bool = true,
        response: Response = .cardsBankNoDebit,
        link: String = "https://www.google.com",
        showLoanMethodMock: Bool = true
    ) {
        self.showDebit = showDebit
        self.showAccount = showPicpayAccount
        self.response = response
        self.link = link
        self.showLoanMethodMock = showLoanMethodMock
    }

    var showLoanMethod: Bool {
        showLoanMethodMock
    }
    
    var showPicpayAccount: Bool {
        showAccount
    }
    
    var showRechargeDebitMethod: Bool {
        showDebit
    }
    
    func getCardsBank() -> [CardBank] {
        switch response {
        case .cardBankDebit:
            return try! MockCodable<[CardBank]>().loadCodableObject(resource: "CardsBankDebit")
        case .cardBankCreditAndDebit:
            return try! MockCodable<[CardBank]>().loadCodableObject(resource: "CardsBankCreditAndDebit")
        case .cardsBankNoDebit:
            return try! MockCodable<[CardBank]>().loadCodableObject(resource: "CardsBankNoDebit")
        }
    }
    
    var linkHelpCenter: String {
        return link
    }
    
    enum Response {
        case cardBankDebit
        case cardBankCreditAndDebit
        case cardsBankNoDebit
    }
}


final class FakeRechargeMethodsViewController: RechargeMethodsViewModelOutputs {
    var calledReloadData = false
    var calledDidNextStep = false
    var calledShowBanks = false
    var calledDidReceiveAnError = false
    var calledAddRightButton = false
    var calledWarningCard = false
    
    var error: PicPayError?
    var didNextStepAction: RechargeMethodsAction?
    var bankMethod: Int?
    var banks: [String] = []
    
    private let coordinator: RechargeMethodsCoordinating?
    
    init(coordinator: RechargeMethodsCoordinating? = nil) {
        self.coordinator = coordinator
    }
    
    func reloadData() {
        calledReloadData = true
    }
    
    func didNextStep(action: RechargeMethodsAction) {
        calledDidNextStep = true
        didNextStepAction = action
        coordinator?.perform(action: action)
    }
    
    func showBanks(method: Int, banks: [String]) {
        calledShowBanks = true
        self.bankMethod = method
        self.banks = banks
    }
    
    func didReceiveAnError(error: PicPayErrorDisplayable) {
        calledDidReceiveAnError = true
        self.error = error as? PicPayError
    }
    
    func addRightButton() {
        calledAddRightButton = true
    }
    
    func warningCard(title: String, desc: String) {
        calledWarningCard = true
    }
}

final class RechargeMethodsViewModelTest: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    
    private func createViewModel(isEmpty: Bool, service: RechargeMethodsServicing) throws -> RechargeMethodsViewModel {
        let dependenciesMock = DependencyContainerMock(analyticsSpy)
        if isEmpty {
            return RechargeMethodsViewModel(model: [], service: service, dependencies: dependenciesMock)
        } else {
            let array = try XCTUnwrap(MockDecodable<[RechargeMethod]>().loadCodableObject(resource: "RechargeMethods", typeDecoder: .useDefaultKeys))
            return RechargeMethodsViewModel(model: array, service: service, dependencies: dependenciesMock)
        }
    }
    
    private func createPicpayAccountRechargeMethod() throws -> RechargeMethod {
        let mock = MockDecodable<RechargeMethod>()
        let resource = "RechargeMethodPicpayAccount"
        return try XCTUnwrap(mock.loadCodableObject(resource: resource, typeDecoder: .useDefaultKeys))
    }
    
    func testViewDidLoadShouldReturnError() throws {
        let service = FakeRechargeMethodsService()
        let viewModel = try createViewModel(isEmpty: true, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        XCTAssertTrue(mockViewController.calledDidReceiveAnError)
        XCTAssertNotNil(mockViewController.error)
    }
    
    func testViewDidLoadShouldReturnDebitRechargeDisable() throws {
        let service = FakeRechargeMethodsService(showDebit: false, response: .cardsBankNoDebit)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        var arrray: [CardRecharge.TypeCard] = []
        for index in 0..<viewModel.numberOfRows(section: 0) {
            arrray.append(viewModel.type(index: index))
        }
        
        XCTAssertTrue(mockViewController.calledReloadData)
        XCTAssertEqual(viewModel.numberOfRows(section: 0), 7)
        XCTAssertFalse(arrray.contains(.addDebit))
    }
    
    func testViewDidLoadShouldReturnDebitRechargeEnable() throws {
        let service = FakeRechargeMethodsService(showDebit: true, response: .cardsBankNoDebit)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        var arrray: [CardRecharge.TypeCard] = []
        for index in 0..<viewModel.numberOfRows(section: 0) {
            arrray.append(viewModel.type(index: index))
        }
        
        XCTAssertTrue(mockViewController.calledReloadData)
        XCTAssertEqual(viewModel.numberOfRows(section: 0), 8)
        XCTAssertTrue(arrray.contains(.addDebit))
    }
    
    func testNumberOfRowsShouldReturnZeroWhenSectionNoZero() throws {
        let service = FakeRechargeMethodsService(showDebit: true)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        XCTAssertEqual(viewModel.numberOfRows(section: 1), 0)
        XCTAssertEqual(viewModel.numberOfRows(section: 2), 0)
        XCTAssertEqual(viewModel.numberOfRows(section: 3), 0)
    }
    
    func testNumberOfSectionsShouldReturnOne() throws {
        let service = FakeRechargeMethodsService(showDebit: true)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        XCTAssertEqual(viewModel.numberOfSections, 1)
    }
    
    func testDidTapShouldReturnActionBill() throws {
        let service = FakeRechargeMethodsService(showDebit: true)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        viewModel.inputs.didTap(index: 0)
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        let didNextStepAction = try XCTUnwrap(mockViewController.didNextStepAction)
        switch didNextStepAction {
        case .bill(let value):
            XCTAssertNotNil(value)
        default:
            XCTFail()
        }
    }
    
    func testDidTapShouldReturnActionOriginal() throws {
        let service = FakeRechargeMethodsService(showDebit: true)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        viewModel.inputs.didTap(index: 2)
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        let didNextStepAction = try XCTUnwrap(mockViewController.didNextStepAction)
        switch didNextStepAction {
        case .original(let value):
            XCTAssertNotNil(value)
        default:
            XCTFail()
        }
    }
    
    func testDidTapShouldReturnActionDebitWhenHaveCardDebit() throws {
        let service = FakeRechargeMethodsService(showDebit: true, response: .cardBankDebit)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        viewModel.inputs.didTap(index: 3)
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        let didNextStepAction = try XCTUnwrap(mockViewController.didNextStepAction)
        switch didNextStepAction {
        case .debit:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testDidTapShouldReturnActionDebitWhenHaveCardCreditAndDebit() throws {
        let service = FakeRechargeMethodsService(showDebit: true, response: .cardBankCreditAndDebit)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        viewModel.inputs.didTap(index: 3)
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        let didNextStepAction = try XCTUnwrap(mockViewController.didNextStepAction)
        switch didNextStepAction {
        case .debit:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testDidTapShouldReturnActionDebitWhenNoCardDebit() throws {
        let service = FakeRechargeMethodsService(showDebit: true, response: .cardsBankNoDebit)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        viewModel.inputs.didTap(index: 3)
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        let didNextStepAction = try XCTUnwrap(mockViewController.didNextStepAction)
        switch didNextStepAction {
        case .addDebitCard:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testDidTapShouldReturnActionDebitAddDebitCard() throws {
        let service = FakeRechargeMethodsService(showDebit: true, response: .cardsBankNoDebit)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        viewModel.inputs.didTap(index: 3)
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        let didNextStepAction = try XCTUnwrap(mockViewController.didNextStepAction)
        switch didNextStepAction {
        case .addDebitCard:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testDidTap_WhenChoosingPicpayMethod_ShouldInvokeControllerNextStepWithPicpayAccountAction() throws {
        let service = FakeRechargeMethodsService(showDebit: true)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        viewModel.inputs.didTap(index: 6)
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        let didNextStepAction = try XCTUnwrap(mockViewController.didNextStepAction)
        switch didNextStepAction {
        case .picpayAccount(let value):
            XCTAssertNotNil(value)
        default:
            XCTFail()
        }
    }
    
    func testDidTap_WhenChoosingPixMethod_ShouldInvokeControllerNextStepWithDeeplinkAction() throws {
        let service = FakeRechargeMethodsService(showDebit: true)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        viewModel.inputs.didTap(index: 7)

        XCTAssertTrue(analyticsSpy.equals(to: RechargeEvent.deeplink.event()))
        XCTAssertTrue(mockViewController.calledDidNextStep)
        let didNextStepAction = try XCTUnwrap(mockViewController.didNextStepAction)
        switch didNextStepAction {
        case .deeplink(let value):
            XCTAssertEqual(value, URL(string: "picpay://picpay/pix/keyManagement"))
        default:
            XCTFail()
        }
    }
    
    func testGetAccountInfo_WhenPassingARechargeMethod_ShouldReturnAnAccountInfo() throws {
        let service = FakeRechargeMethodsService(showDebit: true)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let method = try createPicpayAccountRechargeMethod()
        let accountInfo = viewModel.getAccountInfo(from: method)
        
        XCTAssertNotNil(accountInfo)
    }
    
    func testDidTapShouldReturnActionClose() throws {
        let service = FakeRechargeMethodsService(showDebit: true, response: .cardsBankNoDebit)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        viewModel.inputs.close()
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        let didNextStepAction = try XCTUnwrap(mockViewController.didNextStepAction)
        switch didNextStepAction {
        case .close:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testDidTapShouldReturnActionBank() throws {
        let service = FakeRechargeMethodsService(showDebit: true, response: .cardsBankNoDebit)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        viewModel.inputs.didTapBank(method: 1, bank: 0)
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        let didNextStepAction = try XCTUnwrap(mockViewController.didNextStepAction)
        switch didNextStepAction {
        case .bank(let value):
            XCTAssertNotNil(value)
        default:
            XCTFail()
        }
    }
    
    func testDidTapShouldReturnShowBanks() throws {
        let service = FakeRechargeMethodsService(showDebit: true, response: .cardsBankNoDebit)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        viewModel.inputs.didTap(index: 1)
        
        XCTAssertTrue(mockViewController.calledShowBanks)
        XCTAssertEqual(mockViewController.bankMethod!, 1)
        XCTAssertEqual(mockViewController.banks.count, 4)
    }
    
    func testCardRechargeReturnAccessoryNotNil() throws {
        let service = FakeRechargeMethodsService(showDebit: true, response: .cardsBankNoDebit)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        let result = viewModel.inputs.cardRecharge(index: 2)
        
        XCTAssertNotNil(result.accessory)
    }
    
    func testCardRechargeReturnRegisterCard() throws {
        let service = FakeRechargeMethodsService(showDebit: true, response: .cardsBankNoDebit)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        let result = viewModel.inputs.cardRecharge(index: 3)
        
        XCTAssertNotNil(result.description, "Cadastre seu cartão de débito")
        XCTAssertTrue(viewModel.type(index: 3) == .addDebit)
    }
    
    func testCardRechargeReturnDebit() throws {
        let service = FakeRechargeMethodsService(showDebit: true, response: .cardBankDebit)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        let result = viewModel.inputs.cardRecharge(index: 3)
        
        XCTAssertEqual(result.description, "Disponível na hora")
        XCTAssertTrue(viewModel.type(index: 3) == .standard)
    }
    
    func testCardRechargeReturnAccessoryNil() throws {
        let service = FakeRechargeMethodsService(showDebit: true, response: .cardsBankNoDebit)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        let result = viewModel.inputs.cardRecharge(index: 0)
        
        XCTAssertNil(result.accessory)
    }
    
    func testRegisterDebitCard() throws {
        let service = FakeRechargeMethodsService(showDebit: true, response: .cardsBankNoDebit)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.didTap(index: 3)
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        let didNextStepAction = try XCTUnwrap(mockViewController.didNextStepAction)
        switch didNextStepAction {
        case .addDebitCard(let method):
            XCTAssertNotNil(method)
        default:
            XCTFail()
        }
    }
    
    func testDidTapHelpCenterShouldCallDidNextStep() throws {
        let service = FakeRechargeMethodsService(link: "https://www.google.com")
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        viewModel.inputs.didTapHelpCenter()
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        let didNextStepAction = try XCTUnwrap(mockViewController.didNextStepAction)
        switch didNextStepAction {
        case .deeplink(let value):
            XCTAssertNotNil(value)
        default:
            XCTFail()
        }
    }
    
    func testDidTapHelpCenterNotShouldCallDidNextStep() throws {
        let service = FakeRechargeMethodsService(link: "")
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        XCTAssertFalse(mockViewController.calledDidNextStep)
    }
    
    func testViewDidLoadShouldCallAddRightButton() throws {
        let service = FakeRechargeMethodsService(showDebit: true)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        viewModel.inputs.didTapHelpCenter()
        
        XCTAssertTrue(mockViewController.calledAddRightButton)
    }
    
    func testViewDidLoadShouldNotCallAddRightButton() throws {
        let service = FakeRechargeMethodsService(showDebit: false)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        XCTAssertFalse(mockViewController.calledAddRightButton)
    }
    
    func testDidTapShouldCallWrongTypeCard() throws {
        let coordinator = FakeRechargeMethodsCoordinator(isDebit: false)
        let service = FakeRechargeMethodsService(showDebit: true, response: .cardsBankNoDebit)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController(coordinator: coordinator)
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        viewModel.inputs.didTap(index: 3)
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        XCTAssertTrue(mockViewController.calledWarningCard)
    }
    
    func testDidTapShouldCallRechargeDebit() throws {
        let coordinator = FakeRechargeMethodsCoordinator(isDebit: true)
        let service = FakeRechargeMethodsService(showDebit: true, response: .cardsBankNoDebit)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController(coordinator: coordinator)
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        viewModel.inputs.didTap(index: 3)
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        if case .debit(let value) = mockViewController.didNextStepAction! {
            XCTAssertNotNil(value)
        }
    }
    
    func testNumberOfRows_WhenSectionZero_ShouldReturnSeven() throws {
        let service = FakeRechargeMethodsService(showDebit: true)
        let viewModel = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        XCTAssertEqual(viewModel.numberOfRows(section: 0), 8)
    }
    
    func testFilterRechargeMethods_wheneverTheViewLoads_ShouldFilterMethodsAccordingToService() throws {
        let service = FakeRechargeMethodsService(showDebit: false, showPicpayAccount: false, showLoanMethodMock: false)
        let sut = try createViewModel(isEmpty: false, service: service)
        
        XCTAssertEqual(sut.numberOfRows(section: 0), 8)
        
        sut.viewDidLoad()

        XCTAssertEqual(sut.numberOfRows(section: 0), 5)
    }
    
    func testFilterRechargeMethods_wheneverTheViewLoads_ShouldFilterDebitMethod() throws {
        let service = FakeRechargeMethodsService(showDebit: false, showPicpayAccount: true, showLoanMethodMock: true)
        let sut = try createViewModel(isEmpty: false, service: service)
        
        XCTAssertEqual(sut.numberOfRows(section: 0), 8)
        sut.viewDidLoad()

        XCTAssertEqual(sut.numberOfRows(section: 0), 7)
    }
    
    func testFilterRechargeMethods_wheneverTheViewLoads_ShouldFilterPicpayAccountMethod() throws {
        let service = FakeRechargeMethodsService(showDebit: true, showPicpayAccount: false, showLoanMethodMock: true)
        let sut = try createViewModel(isEmpty: false, service: service)
        
        XCTAssertEqual(sut.numberOfRows(section: 0), 8)
        
        sut.viewDidLoad()

        XCTAssertEqual(sut.numberOfRows(section: 0), 7)
    }
    
    func testFilterRechargeMethods_wheneverTheViewLoads_ShouldFilterLoanMethod() throws {
        let service = FakeRechargeMethodsService(showDebit: true, showPicpayAccount: true, showLoanMethodMock: false)
        let sut = try createViewModel(isEmpty: false, service: service)
        
        XCTAssertEqual(sut.numberOfRows(section: 0), 8)
        
        sut.viewDidLoad()

        XCTAssertEqual(sut.numberOfRows(section: 0), 7)
    }
    
    func testViewDidLoad_whenFilteringMethods_ShouldInvokeControllerReloadData() throws {
        let service = FakeRechargeMethodsService(showDebit: false, showPicpayAccount: false, showLoanMethodMock: false)
        let sut = try createViewModel(isEmpty: false, service: service)
        let mockViewController = FakeRechargeMethodsViewController()
        
        sut.outputs = mockViewController
        sut.inputs.viewDidLoad()

        XCTAssertEqual(sut.numberOfRows(section: 0), 5)
        XCTAssertTrue(mockViewController.calledReloadData)
    }
}
