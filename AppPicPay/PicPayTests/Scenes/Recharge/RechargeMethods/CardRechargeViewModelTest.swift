@testable import PicPay
import XCTest

class FakeCardRechargeView: CardRechargeViewModelOutputs {
    var calledReloadData = false
    
    func reloadData() {
        calledReloadData = true
    }
}

class CardRechargeViewModelTest: XCTestCase {
    private func createModel() -> CardRecharge {
        return CardRecharge(title: "title", description: "description", image: "https://www.picpay.com/site", accessory: "accessory")
    }
    
    func testViewConfigureShouldCallReloadData() {
        let viewModel = CardRechargeViewModel()
        let model = createModel()
        let mockView = FakeCardRechargeView()
        
        viewModel.outputs = mockView
        viewModel.inputs.viewConfigure(model: model)
        
        XCTAssertTrue(mockView.calledReloadData)
    }
    
    func testTitleShouldReturnTitleMock() {
        let viewModel = CardRechargeViewModel()
        let model = createModel()
        let mockView = FakeCardRechargeView()
        
        viewModel.outputs = mockView
        viewModel.inputs.viewConfigure(model: model)
        
        let result = viewModel.inputs.title
        XCTAssertEqual(result, "title")
    }
    
    func testDescriptionShouldReturnDescriptionMock() {
        let viewModel = CardRechargeViewModel()
        let model = createModel()
        let mockView = FakeCardRechargeView()
        
        viewModel.outputs = mockView
        viewModel.inputs.viewConfigure(model: model)
        
        let result = viewModel.inputs.description
        XCTAssertEqual(result, "description")
    }
    
    func testAccessoryShouldReturnAccessoryMock() {
        let viewModel = CardRechargeViewModel()
        let model = createModel()
        let mockView = FakeCardRechargeView()
        
        viewModel.outputs = mockView
        viewModel.inputs.viewConfigure(model: model)
        
        let result = viewModel.inputs.accessory
        XCTAssertEqual(result, "accessory")
    }
    
    func testImageURLShouldReturnNotNil() {
        let viewModel = CardRechargeViewModel()
        let model = createModel()
        let mockView = FakeCardRechargeView()
        
        viewModel.outputs = mockView
        viewModel.inputs.viewConfigure(model: model)
        
        let result = viewModel.inputs.image
        XCTAssertNotNil(result)
    }
}
