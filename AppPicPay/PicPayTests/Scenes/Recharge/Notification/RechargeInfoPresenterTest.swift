@testable import PicPay
import XCTest

final class RechargeInfoPresenterTest: XCTestCase {
    typealias Dependencies = HasNoDependency
    private lazy var dependenciesMock: Dependencies = DependencyContainerMock()
    private lazy var coordinatorSpy = RechargeInfoCoordinatorSpy()
    private lazy var controllerSpy = RechargeInfoViewControllerSpy()
    private lazy var sut: RechargeInfoPresenter = {
        let presenter = RechargeInfoPresenter(coordinator: coordinatorSpy, dependencies: dependenciesMock)
        presenter.viewController = controllerSpy
        return presenter
    }()
    
    func testSetUp_WheneverCalled_ShouldInvokeControllerSetUpWithSameParameters() {
        let validParameters = (image: UIImage(), title: "ValidTitle", message: "ValidMessage")
        sut.setUp(image: validParameters.image, title: validParameters.title, message: validParameters.message)
        
        XCTAssertEqual(controllerSpy.setUpCount, 1)
        XCTAssertEqual(controllerSpy.setUpParameters?.image, validParameters.image)
        XCTAssertEqual(controllerSpy.setUpParameters?.title, validParameters.title)
        XCTAssertEqual(controllerSpy.setUpParameters?.message, validParameters.message)
    }
    
    func testSetUpAction_WheneverCalled_ShouldInvokeControllerSetUpActionWithSameParameters() {
        let validTitle = "ValidTitle"
        sut.setUpAction(with: validTitle)
        
        XCTAssertEqual(controllerSpy.setUpActionCount, 1)
        XCTAssertEqual(controllerSpy.setUpActionTitle, validTitle)
    }
    
    func testSetUpLink_WheneverCalled_ShouldInvokeControllerSetUpLinkWithSameParameters() {
        let validTitle = "ValidTitle"
        sut.setUpLink(with: validTitle)
        
        XCTAssertEqual(controllerSpy.setUpLinkCount, 1)
        XCTAssertEqual(controllerSpy.setUpLinkTitle, validTitle)
    }
    
    func testDidNextStep_WheneverCalled_ShouldInvokeCoordinatorDidNextStepWithSameAction() throws {
        let validUrl = try XCTUnwrap(URL(string: "www.picpay.com"))
        sut.didNextStep(action: .webview(url: validUrl))
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.actionSpy, [.webview(url: validUrl), .close])
    }

    
    func testShowError_WheneverCalled_ShouldInvokeControllerShowError() {
        let validParameters = (title: "ValidTitle", message: "ValidMessage")
        sut.showError(title: validParameters.title, message: validParameters.message)
        
        XCTAssertEqual(controllerSpy.showErrorCount, 1)
        XCTAssertEqual(controllerSpy.showErrorParameters?.title, validParameters.title)
        XCTAssertEqual(controllerSpy.showErrorParameters?.message, validParameters.message)
    }
}
    
