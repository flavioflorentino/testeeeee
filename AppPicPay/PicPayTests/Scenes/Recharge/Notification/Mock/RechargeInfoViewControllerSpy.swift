@testable import PicPay

final class RechargeInfoViewControllerSpy: RechargeInfoDisplay {
    private(set) var setUpParameters: (image: UIImage, title: String, message: String)?
    private(set) var setUpCount = 0
    func setUp(image: UIImage, title: String, message: String) {
        setUpParameters = (image, title, message)
        setUpCount += 1
    }
    
    private(set) var setUpActionTitle: String?
    private(set) var setUpActionCount = 0
    func setUpAction(with title: String) {
        setUpActionTitle = title
        setUpActionCount += 1
    }
    
    private(set) var setUpLinkTitle: String?
    private(set) var setUpLinkCount = 0
    func setUpLink(with title: String) {
        setUpLinkTitle = title
        setUpLinkCount += 1
    }
    
    private(set) var showErrorParameters: (title: String, message: String)?
    private(set) var showErrorCount = 0
    func showError(title: String, message: String) {
        showErrorParameters = (title, message)
        showErrorCount += 1
    }
}

