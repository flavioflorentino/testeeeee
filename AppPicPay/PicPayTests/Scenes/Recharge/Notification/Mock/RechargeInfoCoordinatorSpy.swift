@testable import PicPay

final class RechargeInfoCoordinatorSpy: RechargeInfoCoordinating {
    var viewController: UIViewController? = ViewControllerMock()
    
    private(set) var actionSpy: [RechargeInfoAction] = []
    
    func perform(action: RechargeInfoAction) {
        actionSpy.append(action)
    }
}
