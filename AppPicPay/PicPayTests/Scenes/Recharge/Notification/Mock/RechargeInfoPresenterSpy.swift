@testable import PicPay

final class RechargeInfoPresenterSpy: RechargeInfoPresenting {
    var viewController: RechargeInfoDisplay?
    
    private(set) var didNextStepActions: [RechargeInfoAction] = []
    
    func didNextStep(action: RechargeInfoAction) {
        didNextStepActions.append(action)
    }
    
    private(set) var showErrorParameters: (title: String, message: String)?
    private(set) var countShowError = 0
    func showError(title: String, message: String) {
        countShowError += 1
        showErrorParameters = (title, message)
    }
    
    private(set) var linkTitleSetUp: String?
    private(set) var linkSetUpCount = 0
    func setUpLink(with title: String) {
        linkSetUpCount += 1
        linkTitleSetUp = title
    }
    
    private(set) var actionSetUpTitle: String?
    private(set) var actionSetUpCount = 0
    func setUpAction(with title: String) {
        actionSetUpCount += 1
        actionSetUpTitle = title
    }
    
    private(set) var parametersSetUp: (image: UIImage, title: String, message: String)?
    private(set) var countSetUp = 0
    func setUp(image: UIImage, title: String, message: String) {
        parametersSetUp = (image, title, message)
        countSetUp += 1
    }

}
