@testable import PicPay
import AnalyticsModule
import FeatureFlag
import Foundation
import XCTest

final class RechargeInfoInteractorTest: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private let featureManager = FeatureManagerMock()
    private lazy var dependenciesMock = DependencyContainerMock(analyticsSpy, featureManager)
    private lazy var presenterSpy = RechargeInfoPresenterSpy()
    private func getSutFor(id: IncentiveId) -> RechargeInfoInteractor {
        return RechargeInfoInteractor(incentiveId: id,
                                      presenter: presenterSpy,
                                      dependencies: dependenciesMock)
    }
    
    func testLoadInfo_WhenIdIsReminder_ShouldInvokeCorrectPresenterMethods() {
        let sut = getSutFor(id: .reminder)
        sut.loadInfo()
        
        XCTAssertEqual(presenterSpy.countSetUp, 1)
        XCTAssertEqual(presenterSpy.parametersSetUp?.image, Assets.Recharge.iluWireTransferIncentive.image)
        XCTAssertEqual(presenterSpy.parametersSetUp?.title, Strings.RechargeWireTransfer.Incentive.Reminder.title)
        XCTAssertEqual(presenterSpy.parametersSetUp?.message, Strings.RechargeWireTransfer.Incentive.Reminder.message)
        
        XCTAssertEqual(presenterSpy.actionSetUpCount, 1)
        XCTAssertEqual(presenterSpy.actionSetUpTitle, Strings.RechargeWireTransfer.Default.action)
        
        XCTAssertEqual(presenterSpy.linkSetUpCount, 0)
    }
    
    func testLoadInfo_WhenIdIsUpgrade_ShouldInvokePresenterWithCorrectInfo() {
        let sut = getSutFor(id: .upgrade)
        sut.loadInfo()
        
        XCTAssertEqual(presenterSpy.countSetUp, 1)
        XCTAssertEqual(presenterSpy.parametersSetUp?.image, Assets.Recharge.iluWireTransferIncentive.image)
        XCTAssertEqual(presenterSpy.parametersSetUp?.title, Strings.RechargeWireTransfer.Incentive.UpgradeAccount.title)
        XCTAssertEqual(presenterSpy.parametersSetUp?.message, Strings.RechargeWireTransfer.Incentive.UpgradeAccount.message)
        
        XCTAssertEqual(presenterSpy.actionSetUpCount, 1)
        XCTAssertEqual(presenterSpy.actionSetUpTitle, Strings.RechargeWireTransfer.Default.continue)
        
        XCTAssertEqual(presenterSpy.linkSetUpCount, 1)
        XCTAssertEqual(presenterSpy.linkTitleSetUp, Strings.RechargeWireTransfer.Default.close)
    }
    
    func testHandleAction_WhenUpgrade_ShouldInvokePresenterDeeplinkWithValidUrl() throws {
        let validUrl = try XCTUnwrap(URL(string: featureManager.text(.opsCashInNotificationUpgradeAccountString)))
        let sut = getSutFor(id: .upgrade)
        sut.handleAction()
        
        XCTAssertEqual(presenterSpy.didNextStepActions.count, 1)
        XCTAssertEqual(presenterSpy.didNextStepActions, [.deeplink(url: validUrl)])
    }
    
    func testHandleAction_WhenReminder_ShouldInvokePresenterWebviewValidUrl() throws {
        let validUrl = try XCTUnwrap(URL(string: featureManager.text(.opsCashInNotificationReminderString)))
        let sut = getSutFor(id: .reminder)
        sut.handleAction()
        
        XCTAssertEqual(presenterSpy.didNextStepActions.count, 1)
        XCTAssertEqual(presenterSpy.didNextStepActions, [.webview(url: validUrl)])
    }
    
    func testHandleAction_WhenUrlIsInvalid_ShouldInvokePresenterShowError() {
        featureManager.override(key: .opsCashInNotificationReminderString, with: "")
        featureManager.update(completion: nil)
        let sut = getSutFor(id: .reminder)

        sut.handleAction()
        
        XCTAssertEqual(presenterSpy.countShowError, 1)
        XCTAssertEqual(presenterSpy.showErrorParameters?.title, DefaultLocalizable.oops.text)
        XCTAssertEqual(presenterSpy.showErrorParameters?.message, Strings.RechargeWireTransfer.Default.Action.Error.message)
    }
    
    func testHandleLink_WhenIdIsReminder_ShouldNotInvokePresenterDidNext() {
        let sut = getSutFor(id: .reminder)
        sut.handleLink()
        
        XCTAssertEqual(presenterSpy.didNextStepActions, [])
    }
    
    func testHandleLink_WhenIdIsUpgrade_ShouldInvokePresenterdidNextStepActionWithCloseAction() {
        let sut = getSutFor(id: .upgrade)
        sut.close()
        
        XCTAssertEqual(presenterSpy.didNextStepActions, [.close])
    }
    
    func testFireDisplayEvent_WhenIdIsReminder_ShouldFireAnAnalyticsEvent() {
        let sut = getSutFor(id: .reminder)
        sut.fireDisplayEvent()
        
        let validEvent = RechargeEvent.notification(.reminder).event()
        let event = analyticsSpy.events.last
        
        XCTAssertEqual(event?.name, validEvent.name)
        XCTAssertTrue(analyticsSpy.equals(to: validEvent))
    }
    
    func testFireDisplayEvent_WhenIdIsUpgrade_ShouldFireAnAnalyticsEvent() {
        let sut = getSutFor(id: .upgrade)
        sut.fireDisplayEvent()
        
        let validEvent = RechargeEvent.notification(.upgradeAccount).event()
        let event = analyticsSpy.events.last
        
        XCTAssertEqual(event?.name, validEvent.name)
        XCTAssertEqual(event?.properties["nome"] as? String, validEvent.properties["nome"] as? String)
    }
    
    func testClose_WheneverCalled_ShouldInvokePresenterdidNextStepActionWithCloseAction() {
        let sut = getSutFor(id: .reminder)
        sut.close()
        
        XCTAssertEqual(presenterSpy.didNextStepActions, [.close])
    }
}


