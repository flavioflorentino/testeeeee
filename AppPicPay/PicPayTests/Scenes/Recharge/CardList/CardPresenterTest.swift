@testable import PicPay
import XCTest

final class SpyCardView: CardPresenterOutputs {
    private(set) var calledDisplayTitle = 0
    private(set) var calledDisplayDescription = 0
    private(set) var calledDisplayImage = 0
    private(set) var calledCardSelected = 0
    private(set) var calledStartLoading = 0
    private(set) var calledStopLoading = 0
    
    private(set) var title: String?
    private(set) var description: String?
    private(set) var image: URL?
    private(set) var isHidden: Bool?
    
    func displayTitle(_ title: String) {
        calledDisplayTitle += 1
        self.title = title
    }
    
    func displayDescription(_ description: String) {
        calledDisplayDescription += 1
        self.description = description
    }
    
    func displayImage(_ image: URL?) {
        calledDisplayImage += 1
        self.image = image
    }
    
    func cardSelected(isHidden: Bool) {
        calledCardSelected += 1
        self.isHidden = isHidden
    }
    
    func startLoading() {
        calledStartLoading += 1
    }
    
    func stopLoading() {
        calledStopLoading += 1
    }
}

final class CardPresenterTest: XCTestCase {
    private func createModel(status: RechargeCard.Status = .none) -> RechargeCard {
        return RechargeCard(
            title: "title",
            description: "description",
            image: "https://www.picpay.com/site",
            status: status
        )
    }
    
    func testViewConfigureShouldCallDisplayTitle() {
        let spyView = SpyCardView()
        let model = createModel()
        let presenter = CardPresenter()
        
        presenter.outputs = spyView
        presenter.viewConfigure(model: model)
        
        XCTAssertEqual(spyView.calledDisplayTitle, 1)
    }
    
    func testViewConfigureShouldCallDisplayTitleWithValue() {
        let spyView = SpyCardView()
        let model = createModel()
        let presenter = CardPresenter()
        
        presenter.outputs = spyView
        presenter.viewConfigure(model: model)
        
        XCTAssertEqual(spyView.title, model.title)
    }
    
    func testViewConfigureShouldCallDisplayDescription() {
        let spyView = SpyCardView()
        let model = createModel()
        let presenter = CardPresenter()
        
        presenter.outputs = spyView
        presenter.viewConfigure(model: model)
        
        XCTAssertEqual(spyView.calledDisplayDescription, 1)
    }
    
    func testViewConfigureShouldCallDisplayDescriptionWithValue() {
        let spyView = SpyCardView()
        let model = createModel()
        let presenter = CardPresenter()
        
        presenter.outputs = spyView
        presenter.viewConfigure(model: model)
        
        XCTAssertEqual(spyView.description, model.description)
    }
    
    func testViewConfigureShouldCallDisplayImage() {
        let spyView = SpyCardView()
        let model = createModel()
        let presenter = CardPresenter()
        
        presenter.outputs = spyView
        presenter.viewConfigure(model: model)
        
        XCTAssertEqual(spyView.calledDisplayImage, 1)
    }
    
    func testViewConfigureShouldCallDisplayImageWithValue() {
        let spyView = SpyCardView()
        let model = createModel()
        let presenter = CardPresenter()
        
        presenter.outputs = spyView
        presenter.viewConfigure(model: model)
        
        let result = URL(string: model.image)
        XCTAssertEqual(spyView.image, result)
    }
    
    func testViewConfigureShouldCallStartLoading() {
        let spyView = SpyCardView()
        let model = createModel(status: .loading)
        let presenter = CardPresenter()
        
        presenter.outputs = spyView
        presenter.viewConfigure(model: model)
        
        XCTAssertEqual(spyView.calledStartLoading, 1)
    }
    
    func testViewConfigureShouldCallStopLoading() {
        let spyView = SpyCardView()
        let model = createModel(status: .none)
        let presenter = CardPresenter()
        
        presenter.outputs = spyView
        presenter.viewConfigure(model: model)
        
        XCTAssertEqual(spyView.calledStopLoading, 1)
    }
    
    func testViewConfigureShouldCallCardSelected() {
        let spyView = SpyCardView()
        let model = createModel(status: .loading)
        let presenter = CardPresenter()
        
        presenter.outputs = spyView
        presenter.viewConfigure(model: model)
        
        XCTAssertEqual(spyView.calledCardSelected, 1)
    }
    
    func testViewConfigureShouldCallCardSelectedWithTrue() {
        let spyView = SpyCardView()
        let model = createModel(status: .loading)
        let presenter = CardPresenter()
        
        presenter.outputs = spyView
        presenter.viewConfigure(model: model)
        
        XCTAssertEqual(spyView.isHidden, true)
    }
    
    func testViewConfigureShouldCallCardSelectedWithFalse() {
        let spyView = SpyCardView()
        let model = createModel(status: .selected)
        let presenter = CardPresenter()
        
        presenter.outputs = spyView
        presenter.viewConfigure(model: model)
        
        XCTAssertEqual(spyView.isHidden, false)
    }
}
