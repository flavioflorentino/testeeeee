import XCTest
@testable import PicPay

private final class FakeCardListCoordinator: CardListCoordinating {
    private let isDebit: Bool
    
    init(isDebit: Bool) {
        self.isDebit = isDebit
    }
    
    var viewController: UIViewController?
    
    func perform(action: CardListAction = .close) {
        if case .addNewCard(let completion) = action {
            completion(isDebit ? .debit : .credit)
        }
    }
}

private final class FakeCardListEvent: CardListEventing {
    var calledChangeDefaultDebitCard = false
    var calledTapAddCard = false
    
    var cardBrand: String?
    var cardIssuer: String?
    
    func changeDefaultDebitCard(cardBrand: String, cardIssuer: String) {
        self.cardBrand = cardBrand
        self.cardIssuer = cardIssuer
        calledChangeDefaultDebitCard = true
    }
    
    func tapAddCard() {
        calledTapAddCard = true
    }
}

private final class FakeCardListService: CardListServicing {
    private var debitSelected: String?
    private var creditSelected: String?
    private var respondeSuccess: Bool
    private var responseDelete: Bool
    
    init(debitSelected: String? = nil, creditSelected: String? = nil, responseSuccess: Bool = true, responseDelete: Bool = true) {
        self.debitSelected = debitSelected
        self.creditSelected = creditSelected
        self.respondeSuccess = responseSuccess
        self.responseDelete = responseDelete
    }
    
    func getCardsList(onCacheLoaded: @escaping ([CardBank]) -> Void, completion: @escaping (PicPayResult<[CardBank]>) -> Void) {
        if respondeSuccess {
            let cards =  try! MockCodable<[CardBank]>().loadCodableObject(resource: "CardBank")
            let success = PicPayResult.success(cards)
            completion(success)
            
        } else {
            let picpayError = PicPayError(message: "Erro")
            let error = PicPayResult<[CardBank]>.failure(picpayError)
            completion(error)
        }
    }
    
    
    func getDebitCardSelected() -> CardBank? {
        guard
            let id = debitSelected,
            let card = try? MockCodable<[CardBank]>().loadCodableObject(resource: "CardsBankDebit")[0]
            else {
                return nil
        }
        
        card.id = id
        return card
    }
    
    func deleteCard(id: String, _ completion: @escaping (Result<Void, PicPayError>) -> Void) {
        let picpayError = PicPayError(message: "Error")
        
        responseDelete ? completion(.success(())) :  completion(.failure(picpayError))
    }
    
    func getCreditCardSelected() -> CardBank? {
        guard
            let id = creditSelected,
            let card = try? MockCodable<[CardBank]>().loadCodableObject(resource: "CardsBankNoDebit")[0]
            else {
                return nil
        }
        card.id = id
        return card
    }
    
    func updateDebitCardSelected(id: String) {
        debitSelected = id
    }
    
    func updateCreditCardSelected(id: String) {
        creditSelected = id
    }
}

private final class FakeCardListViewController: CardListViewModelOutputs {
    private let coordinator: CardListCoordinating?
    var calledReloadData = false
    var calledDidReceiveAnError = false
    var calledDidNextStep = false
    var calledWarningCard = false
    var calledShowWarningDeleteCard = 0
    
    var error: PicPayError?
    var didNextStepAction: CardListAction?
    
    init(coordinator: CardListCoordinating? = nil) {
        self.coordinator = coordinator
    }
    
    func reloadData() {
        calledReloadData = true
    }
    
    func didReceiveAnError(error: PicPayErrorDisplayable) {
        calledDidReceiveAnError = true
        self.error = error as? PicPayError
    }
    
    func didNextStep(action: CardListAction) {
        calledDidNextStep = true
        didNextStepAction = action
        coordinator?.perform(action: action)
    }
    
    func warningCard(title: String, desc: String) {
        calledWarningCard = true
    }
    
    func showWarningDeleteCard(title: String, subtitle: String, cardId: String) {
        calledShowWarningDeleteCard += 1
    }
}

final class CardListViewModelTest: XCTestCase {
    func testViewDidLoadShouldCallReloadData() {
        let service = FakeCardListService(responseSuccess: true)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics)
        let mockViewController = FakeCardListViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        XCTAssert(mockViewController.calledReloadData)
    }
    
    func testViewDidLoadShouldCallDidReceiveAnError() {
        let service = FakeCardListService(responseSuccess: false)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics)
        let mockViewController = FakeCardListViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        XCTAssert(mockViewController.calledDidReceiveAnError)
        XCTAssertNotNil(mockViewController.error)
    }
    
    func testNumberOfRowsShouldReturn2() {
        let service = FakeCardListService(responseSuccess: true)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics, typeFilter: [.debit, .creditDebit])
        let mockViewController = FakeCardListViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        let result = viewModel.inputs.numberOfRows(section: 0)
        XCTAssertEqual(result, 2)
    }
    
    func testNumberOfSectionsShouldAllwaysReturn1() {
        let service = FakeCardListService(responseSuccess: true)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics)
        let mockViewController = FakeCardListViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        let result = viewModel.inputs.numberOfSections()
        XCTAssertEqual(result, 1)
    }
    
    func testBalanceIsEnableShouldReturnTrue() {
        let service = FakeCardListService(responseSuccess: true)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics, balanceEnable: true)
        let mockViewController = FakeCardListViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        let result = viewModel.inputs.balanceIsEnable()
        XCTAssertTrue(result)
    }
    
    func testBalanceIsEnableShouldReturnFalse() {
        let service = FakeCardListService(responseSuccess: true)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics, balanceEnable: false)
        let mockViewController = FakeCardListViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        let result = viewModel.inputs.balanceIsEnable()
        XCTAssertFalse(result)
    }
    
    func testTitleSectionShouldReturnPaymentMethods() {
        let service = FakeCardListService(responseSuccess: true)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics, typeFilter: [.credit])
        let mockViewController = FakeCardListViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        let result = viewModel.inputs.titleSection()
        XCTAssertEqual(result, RechargeLocalizable.paymentMethods.text)
    }
    
    func testTitleSectionShouldReturnDebitRegistered() {
        let service = FakeCardListService(responseSuccess: true)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics, typeFilter: [.debit, .creditDebit])
        let mockViewController = FakeCardListViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        let result = viewModel.inputs.titleSection()
        XCTAssertEqual(result, RechargeLocalizable.debitRegistered.text)
    }
    
    func testCloseShouldReturnActionClose() {
        let service = FakeCardListService(responseSuccess: true)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics)
        let mockViewController = FakeCardListViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.close()
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        
        switch mockViewController.didNextStepAction! {
        case .close:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testDidTapFooterShouldReturnActionAddNewCard() {
        let service = FakeCardListService(responseSuccess: true)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics)
        let mockViewController = FakeCardListViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.didTapFooter()
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        
        switch mockViewController.didNextStepAction! {
        case .addNewCard:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testDidTapCardShouldReturnNewDebitCardSelected() {
        let service = FakeCardListService(debitSelected: nil, responseSuccess: true)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics, typeFilter: [.debit, .creditDebit])
        let mockViewController = FakeCardListViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        let result1 = viewModel.inputs.card(index: 1)
        XCTAssertEqual(result1.status, .none)
        
        viewModel.inputs.didTapCard(index: 1)
        let result2 = viewModel.inputs.card(index: 1)
        XCTAssertEqual(result2.status, .selected)
    }
    
    func testDidTapCardShouldReturnNewCreditCardSelected() {
        let service = FakeCardListService(creditSelected: nil, responseSuccess: true)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics, typeFilter: [.credit, .creditDebit])
        let mockViewController = FakeCardListViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        let result1 = viewModel.inputs.card(index: 1)
        XCTAssertEqual(result1.status, .none)
        
        viewModel.inputs.didTapCard(index: 1)
        let result2 = viewModel.inputs.card(index: 1)
        XCTAssertEqual(result2.status, .selected)
    }
    
    func testNoCardSelectShouldReturnFirstCreditCardSelected() {
        let service = FakeCardListService(creditSelected: "419550", responseSuccess: true)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics, typeFilter: [.credit, .creditDebit])
        let mockViewController = FakeCardListViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        let result1 = viewModel.inputs.card(index: 0)
        XCTAssertEqual(result1.status, .selected)
    }
    
    func testNoCardSelectShouldReturnFirstDebitCardSelected() {
        let service = FakeCardListService(debitSelected: "419604", responseSuccess: true)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics, typeFilter: [.debit, .creditDebit])
        let mockViewController = FakeCardListViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        let result1 = viewModel.inputs.card(index: 0)
        XCTAssertEqual(result1.status, .selected)
    }
    
    func testDidTapDebitCardShouldCallAnalyticsChangeDefaultDebitCard() {
        let service = FakeCardListService(debitSelected: nil, responseSuccess: true)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics, typeFilter: [.debit, .creditDebit])
        let mockViewController = FakeCardListViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.didTapCard(index: 1)
        XCTAssertTrue(analytics.calledChangeDefaultDebitCard)

        XCTAssertEqual(analytics.cardBrand, "VISA")
        XCTAssertEqual(analytics.cardIssuer, "Banco Do Brasil")
    }
    
    func testDidTapFooterShouldCallAnalyticsTapAddCard() {
        let service = FakeCardListService(responseSuccess: true)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics)
        let mockViewController = FakeCardListViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.didTapFooter()
        
        XCTAssertTrue(analytics.calledTapAddCard)
    }
    
    func testDidTapFooterShouldCallWarningCard() {
        let service = FakeCardListService(responseSuccess: true)
        let coordinator = FakeCardListCoordinator(isDebit: false)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics)
        let mockViewController = FakeCardListViewController(coordinator: coordinator)
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.didTapFooter()
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        XCTAssertTrue(mockViewController.calledWarningCard)
    }
    
    func testDidTapFooterNotShouldCallReloadData() {
        let service = FakeCardListService(responseSuccess: true)
        let coordinator = FakeCardListCoordinator(isDebit: false)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics)
        let mockViewController = FakeCardListViewController(coordinator: coordinator)
        
        viewModel.outputs = mockViewController
        
        viewModel.inputs.didTapFooter()
        
        XCTAssertFalse(mockViewController.calledReloadData)
    }
    
    func testDidTapFooterShouldCallReloadData() {
        let service = FakeCardListService(responseSuccess: true)
        let coordinator = FakeCardListCoordinator(isDebit: true)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics)
        let mockViewController = FakeCardListViewController(coordinator: coordinator)
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.didTapFooter()
        
        XCTAssertTrue(mockViewController.calledReloadData)
    }
    
    func testConfirmedCardDeletionSuccessShouldCallReloadData() {
        let service = FakeCardListService(responseDelete: true)
        let coordinator = FakeCardListCoordinator(isDebit: true)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics)
        let mockViewController = FakeCardListViewController(coordinator: coordinator)
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.deleteCard(index: 0)
        viewModel.inputs.confirmedCardDeletion(cardId: "123")
        
        XCTAssert(mockViewController.calledReloadData)
    }
    
    func testConfirmedCardDeletionFailureShouldCallDidReceiveAnError() {
        let service = FakeCardListService(responseDelete: false)
        let coordinator = FakeCardListCoordinator(isDebit: true)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics)
        let mockViewController = FakeCardListViewController(coordinator: coordinator)
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.deleteCard(index: 0)
        viewModel.inputs.confirmedCardDeletion(cardId: "123")
        
        XCTAssert(mockViewController.calledDidReceiveAnError)
    }
    
    func testConfirmedCardDeletionFailureShouldCallReloadData() {
        let service = FakeCardListService(responseDelete: false)
        let coordinator = FakeCardListCoordinator(isDebit: true)
        let analytics = FakeCardListEvent()
        let viewModel = CardListViewModel(service: service, analytics: analytics)
        let mockViewController = FakeCardListViewController(coordinator: coordinator)
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.deleteCard(index: 0)
        viewModel.inputs.confirmedCardDeletion(cardId: "123")
        
        XCTAssert(mockViewController.calledReloadData)
    }
}
