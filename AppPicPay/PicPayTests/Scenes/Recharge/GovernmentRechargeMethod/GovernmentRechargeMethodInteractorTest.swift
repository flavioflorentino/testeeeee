@testable import PicPay
import AnalyticsModule
import Core
import FeatureFlag
import XCTest

final class GovernmentRechargeMethodServiceMock: GovernmentRechargeMethodServicing {
    var fetchMethodsExpectedResult: Result<[GovernmentRechargeOption], ApiError>?
    
    func fetchMethods(completion: @escaping (Result<[GovernmentRechargeOption], ApiError>) -> Void) {
        guard let expectedResult = fetchMethodsExpectedResult else {
            XCTFail("Expected result not defined")
            return
        }
        completion(expectedResult)
    }
    
    func getCards() -> [CardBank] { [] }
}

final class GovernmentRechargeMethodInteractorTest: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private let featureManager = FeatureManagerMock()
    private lazy var dependenciesMock = DependencyContainerMock(analyticsSpy, featureManager)
    private lazy var presenterSpy = GovernmentRechargeMethodPresenterSpy()
    private lazy var serviceMock = GovernmentRechargeMethodServiceMock()

    private lazy var sut: GovernmentRechargeMethodInteractor = {
        let methods = try? GovernmentRechargeMethodMockHelper.mockRechargeMethods()
        return GovernmentRechargeMethodInteractor(methods: methods ?? [],
                                                  presenter: presenterSpy,
                                                  service: serviceMock,
                                                  dependencies: dependenciesMock)
    }()
    
    func testSetUpMethods_wheneverCalled_shouldInvokePresenterDisplay() throws {
        sut.setUpMethods()
        let validMock = try XCTUnwrap(GovernmentRechargeMethodMockHelper.mockRechargeMethods())
        
        XCTAssertEqual(presenterSpy.displayCount, 1)
        XCTAssertEqual(presenterSpy.displayedMethods, validMock)
    }
    
    func testChooseMethod_whenPassingAValidRechargeType_shouldInvokePresenterDidNextWithCorrectAction() {
        let validType: RechargeMethod.RechargeType = .emergencyAid
        sut.choose(method: validType)
        
        var nextMethod: RechargeMethod.RechargeType?
        switch presenterSpy.nextAction {
        case .debit(let method):
            nextMethod = method.typeId
        default:
            break
        }
        
        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
        XCTAssertEqual(nextMethod, validType)
    }
    
    func testChooseMethod_whenPassingAnInvalidRechargeType_shouldNotInvokePresenterDidNextWithCorrectAction() {
        let invalidType: RechargeMethod.RechargeType = .original
        sut.choose(method: invalidType)
        
        var nextMethod: RechargeMethod.RechargeType?
        switch presenterSpy.nextAction {
        case .debit(let method):
            nextMethod = method.typeId
        default:
            break
        }
        
        XCTAssertEqual(presenterSpy.didNextStepCount, 0)
        XCTAssertNotEqual(nextMethod, invalidType)
    }
    
    func testGetHelp_wheneverCalled_shouldInvokePresenterDidNext() {
        sut.getHelp()
        
        XCTAssertEqual(presenterSpy.didNextStepCount, 1)
        XCTAssertNotNil(presenterSpy.nextAction)
    }
    
    func testLoadMethods_onSuccess_shouldInvokePresenterStates() throws {
        let validRechargeOption = GovernmentRechargeOption(rechargeTypeId: "1",
                                                           rechargeTypeDescription: "Test Description",
                                                           rechargeTypeName: "Test Name",
                                                           rechargeTypeImage: "picpay.com",
                                                           serviceCharge: nil)
        let validRechargeMethod = try XCTUnwrap(validRechargeOption.convertToLegacyMethod())
        
        serviceMock.fetchMethodsExpectedResult = .success([validRechargeOption])
        sut.loadMethods()
        
        XCTAssertEqual(presenterSpy.displayedLoading, [true, false])
        XCTAssertEqual(presenterSpy.displayedMethods, [validRechargeMethod])
    }
    
    func testLoadMethods_onError_shouldInvokePresenterStates() {
        serviceMock.fetchMethodsExpectedResult = .failure(.serverError)
        sut.loadMethods()
        
        XCTAssertEqual(presenterSpy.displayedLoading, [true, false])
        XCTAssertEqual(presenterSpy.displayErrorCount, 1)
    }
}
