@testable import PicPay
import AssetsKit
import UI
import XCTest

class GovernmentRechargeMethodPresenterTest: XCTestCase {
    private lazy var dependenciesMock = DependencyContainerMock()
    private lazy var coordinatorSpy = GovernmentRechargeMethodCoordinatorSpy()
    private lazy var viewControllerSpy = GovernmentRechargeMethodViewControllerSpy()

    private lazy var sut: GovernmentRechargeMethodPresenter = {
        let presenter = GovernmentRechargeMethodPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testDisplayMethods_whenPassingValidViewModels_shouldInvokeControllerSetUpMethods() throws {
        let validMock = try XCTUnwrap(GovernmentRechargeMethodMockHelper.mockRechargeMethods())
        
        sut.display(methods: validMock)
        
        XCTAssertEqual(viewControllerSpy.setupCount, 1)
        XCTAssertEqual(viewControllerSpy.emptyStackCount, 1)
        XCTAssertEqual(viewControllerSpy.addCardToStackCount, 2)
        XCTAssertEqual(viewControllerSpy.addLineToStackCount, 1)
        XCTAssertNotNil(viewControllerSpy.addedViewModel)
        XCTAssertNotNil(viewControllerSpy.titleSet)
    }
    
    func testNextStep_wheneverCalled_shouldInvokeCoordinatorPerformWithCorrectAction() throws {
        let validMock = try XCTUnwrap(GovernmentRechargeMethodMockHelper.mockDebitRechargeMethod())
        let validAction = GovernmentRechargeMethodAction.debit(validMock)
        
        sut.didNextStep(action: validAction)
        
        XCTAssertEqual(coordinatorSpy.performCount, 1)
        XCTAssertEqual(coordinatorSpy.performedAction, validAction)
    }
    
    func testDisplayLoading_shouldInvokeControllerDisplayLoading() {
        let validDisplayedLoading = [true, false]
        sut.displayLoading(true)
        sut.displayLoading(false)
        
        XCTAssertEqual(viewControllerSpy.displayedLoading, validDisplayedLoading)
    }
    
    func testDisplayError_shouldInvokeControllerBeginLoading() {
        let validErrorViewModel = StatefulErrorViewModel(image: Resources.Illustrations.iluNotFoundCircledBackground.image,
                                                         content: (title: Strings.RechargeWireTransfer.Default.Error.title,
                                                                   description: Strings.RechargeWireTransfer.Default.Error.message),
                                                         button: (image: nil,
                                                                  title: Strings.RechargeWireTransfer.Default.Error.tryAgain))
        
        sut.displayError()
        
        XCTAssertEqual(viewControllerSpy.displayedErrors, [validErrorViewModel])
    }
}
