import UI
@testable import PicPay

final class GovernmentRechargeMethodViewControllerSpy: GovernmentRechargeMethodDisplaying {
    private(set) var titleSet: String?
    private(set) var setupCount = 0
    func setUp(title: String) {
        titleSet = title
        setupCount += 1
    }
    
    private(set) var emptyStackCount = 0
    func emptyStack() {
        emptyStackCount += 1
    }
    
    private(set) var addCardToStackCount = 0
    private(set) var addedViewModel: GovernmentRechargeMethodCardViewModel?
    func addCardToStack(with viewModel: GovernmentRechargeMethodCardViewModel) -> GovernmentRechargeMethodCard {
        addCardToStackCount += 1
        addedViewModel = viewModel
        return GovernmentRechargeMethodCardComponent()
    }
    
    private(set) var addLineToStackCount = 0
    func addLineToStack(after view: UIView) {
        addLineToStackCount += 1
    }
    
    private(set) var displayedLoading = [Bool]()
    func displayLoading(_ shouldDisplay: Bool) {
        displayedLoading.append(shouldDisplay)
    }
    
    private(set) var displayedErrors = [StatefulErrorViewModel]()
    func displayError(with viewModel: StatefulErrorViewModel) {
        displayedErrors.append(viewModel)
    }
}
