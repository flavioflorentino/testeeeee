@testable import PicPay

final class GovernmentRechargeMethodCoordinatorSpy: GovernmentRechargeMethodCoordinating {
    var viewController: UIViewController?
    
    private(set) var performCount = 0
    private(set) var performedAction: GovernmentRechargeMethodAction?
    func perform(action: GovernmentRechargeMethodAction) {
        performCount += 1
        performedAction = action
    }
}
