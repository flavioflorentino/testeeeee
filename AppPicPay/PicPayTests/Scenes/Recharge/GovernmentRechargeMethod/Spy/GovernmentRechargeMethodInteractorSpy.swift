@testable import PicPay

final class GovernmentRechargeMethodInteractorSpy: GovernmentRechargeMethodInteracting {
    private(set) var setUpMethodsCount = 0
    func setUpMethods() {
        setUpMethodsCount += 1
    }
    
    private(set) var chooseCount = 0
    private(set) var chosenMethod: RechargeMethod.RechargeType?
    func choose(method: RechargeMethod.RechargeType) {
        chooseCount += 1
        chosenMethod = method
    }
    
    private(set) var getHelpCount = 0
    func getHelp() {
        getHelpCount += 1
    }
    
    private(set) var loadMethodsCount = 0
    func loadMethods() {
        loadMethodsCount += 1
    }
}
