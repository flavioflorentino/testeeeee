@testable import PicPay

final class GovernmentRechargeMethodPresenterSpy: GovernmentRechargeMethodPresenting {
    var viewController: GovernmentRechargeMethodDisplaying?
    
    private(set) var displayCount = 0
    private(set) var displayedMethods: [RechargeMethod]?
    func display(methods: [RechargeMethod]) {
        displayCount += 1
        displayedMethods = methods
    }
    
    private(set) var didNextStepCount = 0
    private(set) var nextAction: GovernmentRechargeMethodAction?
    func didNextStep(action: GovernmentRechargeMethodAction) {
        didNextStepCount += 1
        nextAction = action
    }
    
    private(set) var displayedLoading = [Bool]()
    func displayLoading(_ shouldDisplay: Bool) {
        displayedLoading.append(shouldDisplay)
    }
    
    private(set) var displayErrorCount = 0
    func displayError() {
        displayErrorCount += 1
    }
}
