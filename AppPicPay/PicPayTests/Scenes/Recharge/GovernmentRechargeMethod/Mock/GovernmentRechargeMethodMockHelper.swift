@testable import PicPay
import XCTest

final class GovernmentRechargeMethodMockHelper {
    class func mockRechargeMethods() throws -> [RechargeMethod] {
        let mock = MockDecodable<[RechargeMethod]>()
        let resource = "GovernmenRechargeMethods"
        return try XCTUnwrap(mock.loadCodableObject(resource: resource, typeDecoder: .useDefaultKeys))
    }
    
    class func mockDebitRechargeMethod() throws -> RechargeMethod {
        let mock = MockDecodable<RechargeMethod>()
        let resource = "RechargeMethodDebit"
        return try XCTUnwrap(mock.loadCodableObject(resource: resource, typeDecoder: .useDefaultKeys))
    }
    
    class func mockViewModels() -> [GovernmentRechargeMethodCardViewModel] {
        return [GovernmentRechargeMethodCardViewModel(rechargeType: .emergencyAid,
                                                      iconUrl: nil,
                                                      title: "Valid Title",
                                                      info: "Valid Info"),
                GovernmentRechargeMethodCardViewModel(rechargeType: .bill,
                                                      iconUrl: nil,
                                                      title: "Valid Title 2",
                                                      info: "Valid Info 2")]
    }
}
