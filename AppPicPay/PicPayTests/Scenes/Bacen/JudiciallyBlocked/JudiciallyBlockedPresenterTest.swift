import XCTest
import CoreLegacy
@testable import PicPay

private final class JudiciallyBlockedCoordinatorSpy: JudiciallyBlockedCoordinating {
    private(set) var didCallPerform = 0
    
    private(set) var action: JudiciallyBlockedAction?
    
    var viewController: UIViewController?
    func perform(action: JudiciallyBlockedAction) {
        didCallPerform += 1
        self.action = action
    }
}

private final class JudiciallyBlockedViewControllerSpy: JudiciallyBlockedDisplay {
    private(set) var didCallStartLoading = 0
    private(set) var didCallStopLoading = 0
    private(set) var didCallDisplayPopupPassword = 0
    private(set) var didCallDisplayStatus = 0
    private(set) var didCallDisplayProcess = 0
    private(set) var didCallDisplayPopup = 0
    private(set) var didCallAddRowBalance = 0
    private(set) var didCallDisplayBlockedDescription = 0
    private(set) var didCallDisplayBlockedReason = 0
    private(set) var didCallDisplayKnowMore = 0
    private(set) var didCallDidReceiveAnError = 0
    
    private(set) var error: PicPayError?
    
    private(set) var infoBalance: String?
    private(set) var balance: String?
    
    private(set) var status: String?
    private(set) var process: String?
    private(set) var titlePopup: String?
    
    private(set) var reason: String?
    private(set) var blockedDescription: String?
    private(set) var knowMore: String?
    
    func startLoading() {
        didCallStartLoading += 1
    }
    
    func stopLoading() {
        didCallStopLoading += 1
    }
    
    func displayPopupPassword() {
        didCallDisplayPopupPassword += 1
    }
    
    func displayStatus(_ status: String) {
        didCallDisplayStatus += 1
        self.status = status
    }
    
    func displayProcess(_ attributedText: NSAttributedString) {
        didCallDisplayProcess += 1
        self.process = attributedText.string
    }
    
    func displayPopup(title: String) {
        didCallDisplayPopup += 1
        self.titlePopup = title
    }
    
    func addRowBalance(infoBalance: String, balance: String) {
        didCallAddRowBalance += 1
        self.infoBalance = infoBalance
        self.balance = balance
    }
    
    func displayBlockedDescription(_ attributedText: NSAttributedString) {
        blockedDescription = attributedText.string
        didCallDisplayBlockedDescription += 1
    }
    
    func displayBlockedReason(_ reason: String) {
        didCallDisplayBlockedReason += 1
        self.reason = reason
    }
    
    func displayKnowMore(_ attributedText: NSAttributedString) {
        knowMore = attributedText.string
        didCallDisplayKnowMore += 1
    }
    
    func didReceiveAnError(error: PicPayError) {
        didCallDidReceiveAnError += 1
        self.error = error
    }
}

class JudiciallyBlockedPresenterTest: XCTestCase {
    private let coordinatorSpy = JudiciallyBlockedCoordinatorSpy()
    private let viewControllerSpy = JudiciallyBlockedViewControllerSpy()
    
    private func createModel(blockedValue: Double? = nil, transferredValue: Double? = nil) -> BlockedStatus {
        return BlockedStatus(
            description: "description [b]teste[b]",
            blockedValue: blockedValue,
            transferredValue: transferredValue,
            status: "status"
        )
    }
    
    private func createPresenter() -> JudiciallyBlockedPresenter {
        let presenter = JudiciallyBlockedPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        
        return presenter
    }
    
    func testrPesenterLoad_ShouldCallStartLoading() {
        let presenter = createPresenter()
        presenter.presenterLoad()
        
        XCTAssertEqual(viewControllerSpy.didCallStartLoading, 1)
    }
    
    func testDidReceiveAnError_WhenCall_ShouldCallStopLoading() {
        let presenter = createPresenter()
        let error = PicPayError(message: "Error")
        
        presenter.didReceiveAnError(error: error)
        
        XCTAssertEqual(viewControllerSpy.didCallStopLoading, 1)
    }
    
    func testDidReceiveAnError_WhenCall_ShouldCallDidReceiveAnError() {
        let presenter = createPresenter()
        let error = PicPayError(message: "Error")
        
        presenter.didReceiveAnError(error: error)
        
        XCTAssertEqual(viewControllerSpy.didCallDidReceiveAnError, 1)
        XCTAssertNotNil(viewControllerSpy.error)
    }
    
    func testUpdateView_WhenCallWithBlockedValue_ShouldCallAddRowBalance() {
        let presenter = createPresenter()
        let model = createModel(blockedValue: 200)
        let balance = CurrencyFormatter.brazillianRealString(from: 200) ?? ""
        
        presenter.updateView(model: model)
        
        XCTAssertEqual(viewControllerSpy.didCallAddRowBalance, 1)
        XCTAssertEqual(viewControllerSpy.infoBalance, BacenLocalizable.blockedBalance.text)
        XCTAssertEqual(viewControllerSpy.balance, balance)
    }
    
    func testUpdateView_WhenCallWithBalanceTransferred_ShouldCallAddRowBalance() {
        let presenter = createPresenter()
        let model = createModel(transferredValue: 200)
        let balance = CurrencyFormatter.brazillianRealString(from: 200) ?? ""
        
        presenter.updateView(model: model)
        
        XCTAssertEqual(viewControllerSpy.didCallAddRowBalance, 1)
        XCTAssertEqual(viewControllerSpy.infoBalance, BacenLocalizable.balanceTransferred.text)
        XCTAssertEqual(viewControllerSpy.balance, balance)
    }
    
    func testUpdateView_WhenCallWithBalanceTransferredAndBlockedValue_ShouldCallAddRowBalance() {
        let presenter = createPresenter()
        let model = createModel(blockedValue: 100, transferredValue: 200)
        
        presenter.updateView(model: model)
        
        XCTAssertEqual(viewControllerSpy.didCallAddRowBalance, 2)
    }
    
    func testUpdateView_WhenCallWithBlockedTransfer_ShouldCallDisplayBlockedDescription() {
        let presenter = createPresenter()
        let model = createModel()
        let description = model.description.replacingOccurrences(of: "[b]", with: "")
        presenter.updateView(model: model)
        
        XCTAssertEqual(viewControllerSpy.didCallDisplayBlockedDescription, 1)
        XCTAssertEqual(viewControllerSpy.blockedDescription, description)
    }
    
    func testUpdateView_WhenCallWithBlockedTransfer_ShouldCallDisplayBlockedReason() {
        let presenter = createPresenter()
        let model = createModel()
        presenter.updateView(model: model)
        
        XCTAssertEqual(viewControllerSpy.didCallDisplayBlockedReason, 1)
        XCTAssertEqual(viewControllerSpy.reason, BacenLocalizable.blockedReason.text)
    }
    
    func testUpdateView_WhenCallWithBlockedTransfer_ShouldCallDisplayKnowMore() {
        let presenter = createPresenter()
        let model = createModel()
        let description = BacenLocalizable.knowMore.text.replacingOccurrences(of: "[u]", with: "")
        presenter.updateView(model: model)
        
        XCTAssertEqual(viewControllerSpy.didCallDisplayKnowMore, 1)
        XCTAssertEqual(viewControllerSpy.knowMore, description)
    }
    
    func testUpdateView_WhenCallStatus_ShouldCallDisplayStatus() {
        let presenter = createPresenter()
        let model = createModel()
        presenter.updateView(model: model)
        
        XCTAssertEqual(viewControllerSpy.didCallDisplayStatus, 1)
        XCTAssertEqual(viewControllerSpy.status, "\(BacenLocalizable.status.text) \(model.status)")
    }
    
    func testUpdateView_WhenCallProcess_ShouldCallDisplayProcess() {
        let presenter = createPresenter()
        let text = BacenLocalizable.showNumber.text.replacingOccurrences(of: "[u]", with: "")
        let model = createModel()
        presenter.updateView(model: model)
        
        XCTAssertEqual(viewControllerSpy.didCallDisplayProcess, 1)
        XCTAssertEqual(viewControllerSpy.process, "\(BacenLocalizable.process.text) \(text)")
    }
    
    func testCopyProcessNumber_WhenCall_ShouldCallDisplayPopup() {
        let presenter = createPresenter()
        presenter.copyProcessNumber("12345")
        
        XCTAssertEqual(viewControllerSpy.didCallDisplayPopup, 1)
        XCTAssertEqual(UIPasteboard.general.string, "12345")
        XCTAssertEqual(viewControllerSpy.titlePopup, BacenLocalizable.copyCode.text)
    }
    
    func testPresenterPopupPassword_ShouldCallDisplayPopupPassword() {
        let presenter = createPresenter()
        presenter.presenterPopupPassword()
        
        XCTAssertEqual(viewControllerSpy.didCallDisplayPopupPassword, 1)
    }
    
    func testUpdateProcessNumber_ShouldCallDisplayProcess() {
        let process = "123ACS"
        let presenter = createPresenter()
        presenter.updateProcessNumber(process)
        
        XCTAssertEqual(viewControllerSpy.didCallDisplayProcess, 1)
        XCTAssertEqual(viewControllerSpy.process, "\(BacenLocalizable.process.text) \(process)")
    }
    
    func testUpdateProcessNumber_ShouldCallStopLoading() {
        let process = "123ACS"
        let presenter = createPresenter()
        presenter.updateProcessNumber(process)
        
        XCTAssertEqual(viewControllerSpy.didCallStopLoading, 1)
    }
    
    func testDidNextStep_WhenActionClose_ShouldCallPerform() {
        let presenter = createPresenter()
        presenter.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.didCallPerform, 1)
        XCTAssertEqual(coordinatorSpy.action, .close)
    }
    
    func testDidNextStep_WhenActionOpenKnowMore_ShouldCallPerform() throws {
        let presenter = createPresenter()
        let url = try XCTUnwrap(URL(string: "https://www.picpay.com"))
        presenter.didNextStep(action: .openKnowMore(url))
        
        XCTAssertEqual(coordinatorSpy.didCallPerform, 1)
        XCTAssertEqual(coordinatorSpy.action, .openKnowMore(url))
    }
}
