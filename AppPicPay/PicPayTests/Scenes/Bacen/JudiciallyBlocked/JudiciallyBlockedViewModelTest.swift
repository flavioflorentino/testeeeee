@testable import PicPay
@testable import AnalyticsModule
import XCTest

private final class JudiciallyBlockedPresenterSpy: JudiciallyBlockedPresenting {
    private(set) var didCallPresenterLoad = 0
    private(set) var didCallUpdateView = 0
    private(set) var didCallPresenterPopupPassword = 0
    private(set) var didCalluUpdateProcessNumber = 0
    private(set) var didCallCopyProcessNumber = 0
    private(set) var didCallDidReceiveAnError = 0
    private(set) var didCallDidNextStep = 0
    
    private(set) var model: BlockedStatus?
    private(set) var error: PicPayError?
    private(set) var process: String?
    private(set) var action: JudiciallyBlockedAction?
    private(set) var number: String?
    
    var viewController: JudiciallyBlockedDisplay?
    
    func presenterLoad() {
        didCallPresenterLoad += 1
    }
    
    func updateView(model: BlockedStatus) {
        didCallUpdateView += 1
        self.model = model
    }
    
    func copyProcessNumber(_ number: String) {
        didCallCopyProcessNumber += 1
        self.number = number
    }
    
    func presenterPopupPassword() {
        didCallPresenterPopupPassword += 1
    }
    
    func updateProcessNumber(_ process: String) {
        didCalluUpdateProcessNumber += 1
        self.process = process
    }
    
    func didReceiveAnError(error: PicPayError) {
        didCallDidReceiveAnError += 1
        self.error = error
    }
    
    func didNextStep(action: JudiciallyBlockedAction) {
        didCallDidNextStep += 1
        self.action = action
    }
}

private final class JudiciallyBlockedServiceMock: JudiciallyBlockedServicing {
    var link: String?
    var getBlockedStatusSuccess: Bool = true
    var getProcessNumberSuccess: Bool = true
    
    private(set) var endpoint: BacenEndpoint?
    
    var linkFaq: String {
        return link ?? ""
    }
    
    func getBlockedStatus(endpoint: BacenEndpoint, completion: @escaping(Result<BlockedStatus, PicPayError>) -> Void) {
        self.endpoint = endpoint
        guard getBlockedStatusSuccess else {
            let error = PicPayError(message: "Api Error")
            completion(.failure(error))
            return
        }
        
        let model = BlockedStatus(
            description: "description",
            blockedValue: 20.0,
            transferredValue: 10.0,
            status: "status"
        )
        completion(.success(model))
    }
    
    func getProcessNumber(endpoint: BacenEndpoint, completion: @escaping (Result<String, PicPayError>) -> Void) {
        self.endpoint = endpoint
        guard getProcessNumberSuccess else {
            let error = PicPayError(message: "Api Error")
            completion(.failure(error))
            return
        }
        completion(.success("1234ABCD"))
    }
}

final class JudiciallyBlockedViewModelTest: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private let serviceMock = JudiciallyBlockedServiceMock()
    private let presenterSpy = JudiciallyBlockedPresenterSpy()
    
    private func createViewModel(id: String? = nil) ->  JudiciallyBlockedViewModel {
        return JudiciallyBlockedViewModel(
            id: id,
            dependencies: DependencyContainerMock(analyticsSpy),
            origin: .feed,
            service: serviceMock,
            presenter: presenterSpy
        )
    }
    
    func testLoadBlockedStatus_WhenCall_ShouldCallStartLoadBlockedStatus() {
        let viewModel = createViewModel()
        viewModel.loadBlockedStatus()
        
        XCTAssertEqual(presenterSpy.didCallPresenterLoad, 1)
    }
    
    func testeLoadBlockedStatus_WhenCall_ShouldCallEventOpenJudicialAccount() {
        let viewModel = createViewModel()
        viewModel.loadBlockedStatus()
        
        let expectedEvent = BacenEvent.openJudicialAccount(status: "status", origin: "FEED").event()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testeDidKnowMore_WhenCall_ShouldCallEventHelpJudiciallyAccount() {
        let viewModel = createViewModel()
        viewModel.didKnowMore()
        
        let expectedEvent = BacenEvent.helpJudiciallyAccount.event()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testLoadBlockedStatus_WhenCallService_ShouldCallDidReceiveAnError() {
        serviceMock.getBlockedStatusSuccess = false
        let viewModel = createViewModel()
        
        viewModel.loadBlockedStatus()
        
        XCTAssertEqual(presenterSpy.didCallDidReceiveAnError, 1)
        XCTAssertNotNil(presenterSpy.error)
    }
    
    func testLoadBlockedStatus_WhenCallService_ShouldCallUpdateView() {
        serviceMock.getBlockedStatusSuccess = true
        let viewModel = createViewModel()
        
        viewModel.loadBlockedStatus()
        
        XCTAssertEqual(presenterSpy.didCallUpdateView, 1)
        XCTAssertNotNil(presenterSpy.model)
    }
    
    func testDidDismiss_WhenCall_ShouldCallDidNextStep() {
        let viewModel = createViewModel()
        
        viewModel.didDismiss()
        
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 1)
        
        guard case .close = presenterSpy.action else {
            XCTFail()
            return
        }
    }
    
    func testDidTapInfo_WhenCallRequestProcessNumber_ShouldCallCopyProcessNumber() {
        serviceMock.getProcessNumberSuccess = true
        let viewModel = createViewModel()
        viewModel.requestProcessNumber(password: "1234")
        
        
        viewModel.didTapInfo()
        
        XCTAssertEqual(presenterSpy.didCallCopyProcessNumber, 1)
    }
    
    func testDidTapInfo_WhenNotCallRequestProcessNumber_ShouldCallPresenterPopupPassword() {
        let viewModel = createViewModel()
        
        viewModel.didTapInfo()
        
        XCTAssertEqual(presenterSpy.didCallPresenterPopupPassword, 1)
    }
    
    func testDidKnowMore_WhenCall_ShouldCallDidNextStep() {
        serviceMock.link = "https://picpay.com/site"
        let viewModel = createViewModel()
        
        viewModel.didKnowMore()
        
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 1)
        guard case .openKnowMore = presenterSpy.action else {
            XCTFail()
            return
        }
    }
    
    func testDidKnowMore_WhenCall_NotShouldCallDidNextStep() {
        serviceMock.link = ""
        let viewModel = createViewModel()
        
        viewModel.didKnowMore()
        
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 0)
    }
    
    func testLoadBlockedStatus_WhenIdNil_ShouldCallEndpointJudicialBlockWithoutId() {
        let viewModel = createViewModel(id: nil)
        
        viewModel.loadBlockedStatus()
        
        guard case .judicialBlockWithoutId = serviceMock.endpoint else {
            XCTFail("Not endpoint judicialBlockWithoutId")
            return
        }
    }
    
    func testLoadBlockedStatus_WhenIdNotNil_ShouldCallEndpointJudicialBlock() {
        let viewModel = createViewModel(id: "1234")
        
        viewModel.loadBlockedStatus()
        
        guard case .judicialBlock = serviceMock.endpoint else {
            XCTFail("Not Call JudicialBlock")
            return
        }
    }
    
    func testRequestProcessNumber_WhenPasswordNotNil_ShouldCallPresenterLoad() {
        let viewModel = createViewModel(id: "12")
        
        viewModel.requestProcessNumber(password: "1234")
        
        XCTAssertEqual(presenterSpy.didCallPresenterLoad, 1)
    }
    
    func testRequestProcessNumber_WhenPasswordNotNilAndProcessNumberSuccess_ShouldCallPresenterLoad() {
        serviceMock.getProcessNumberSuccess = true
        let viewModel = createViewModel(id: "12")
        
        viewModel.requestProcessNumber(password: "1234")
        
        XCTAssertEqual(presenterSpy.didCalluUpdateProcessNumber, 1)
        XCTAssertEqual(presenterSpy.process, "1234ABCD")
    }
    
    func testRequestProcessNumber_WhenPasswordNotNilAndProcessNumberFailure_ShouldCallDidReceiveAnErro() {
        serviceMock.getProcessNumberSuccess = false
        let viewModel = createViewModel(id: "12")
        
        viewModel.requestProcessNumber(password: "1234")
        
        XCTAssertEqual(presenterSpy.didCallDidReceiveAnError, 1)
        XCTAssertNotNil(presenterSpy.error)
    }
    
    func testRequestProcessNumber_WhenPasswordNil_ShouldNotCallPresenterLoad() {
        let viewModel = createViewModel(id: "12")
        
        viewModel.requestProcessNumber(password: nil)
        
        XCTAssertEqual(presenterSpy.didCallPresenterLoad, 0)
    }
    
    func testRequestProcessNumber_WhenIdNil_ShouldCallEndpointJudicialProcessNumberWithoutId() {
        let viewModel = createViewModel(id: nil)
        
        viewModel.requestProcessNumber(password: "1234")
        
        guard case .judicialProcessNumberWithoutId = serviceMock.endpoint else {
            XCTFail("Not endpoint JudicialProcessNumberWithoutId")
            return
        }
    }
    
    func testRequestProcessNumber_WhenIdNotNil_ShouldCallEndpointJudicialBlock() {
        let viewModel = createViewModel(id: "1234")
        
        viewModel.requestProcessNumber(password: "1234")
        
        guard case .judicialProcessNumber = serviceMock.endpoint else {
            XCTFail("Not Call JudicialProcessNumber")
            return
        }
    }
}
