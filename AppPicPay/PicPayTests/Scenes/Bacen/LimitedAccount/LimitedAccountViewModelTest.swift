import XCTest
@testable import AnalyticsModule
@testable import PicPay

final class LimitedAccountPresenterSpy: LimitedAccountPresenting {
    private(set) var didCallLoadService = 0
    private(set) var didCallUpdateView = 0
    private(set) var didCallDidReceiveAnError = 0
    private(set) var didCallDidNextStep = 0
    
    private(set) var data: LimitedAccountContainer?
    private(set) var error: PicPayError?
    private(set) var action: LimitedAccountAction?
    
    var viewController: LimitedAccountDisplay?
    
    func loadService() {
        didCallLoadService += 1
    }
    
    func updateView(data: LimitedAccountContainer) {
        didCallUpdateView += 1
        self.data = data
    }
    
    func didReceiveAnError(_ error: PicPayError) {
        didCallDidReceiveAnError += 1
        self.error = error
    }
    
    func didNextStep(action: LimitedAccountAction) {
        didCallDidNextStep += 1
        self.action = action
    }
}

final class LimitedAccountServiceMock: LimitedAccountServicing {
    var getLimitedAccountStatusSuccess = true
    
    func getLimitedAccountStatus(completion: @escaping (Result<LimitedAccountContainer, PicPayError>) -> Void) {
        guard getLimitedAccountStatusSuccess else {
            let error = PicPayError(message: "Error")
            completion(.failure(error))
            return
        }
        
        do {
            let model = try MockDecodable<LimitedAccountContainer>().loadCodableObject(resource: "limitedAccount")
            completion(.success(model))
        } catch {
            XCTFail("Error Decodable LimitedAccountContainer")
        }
    }
}

final class LimitedAccountViewModelTest: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private let serviceMock = LimitedAccountServiceMock()
    private let presenterSpy = LimitedAccountPresenterSpy()
    
    private lazy var sut: LimitedAccountViewModelInputs = {
        LimitedAccountViewModel(
            origin: "origin",
            dependencies: DependencyContainerMock(analyticsSpy),
            service: serviceMock,
            presenter: presenterSpy
        )
    }()
    
    func testUpdateView_ShouldCallAnalyticsOpenLimitedAccount() {
        sut.updateView()
        
        let expectedEvent = BacenEvent.openLimitedAccount(origin: "origin").event()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testUpdateView_ShouldCallLoadService() {
        sut.updateView()
        
        XCTAssertEqual(presenterSpy.didCallLoadService, 1)
    }
    
    func testUpdateView_WhenGetLimitedAccountStatusSuccess_ShouldCallUpdateView() {
        serviceMock.getLimitedAccountStatusSuccess = true
        sut.updateView()
        
        XCTAssertEqual(presenterSpy.didCallUpdateView, 1)
        XCTAssertEqual(presenterSpy.data?.limits.count, 2)
    }
    
    func testUpdateView_WhenGetLimitedAccountStatusFailure_ShouldCallDidReceiveAnError() {
        serviceMock.getLimitedAccountStatusSuccess = false
        sut.updateView()
        
        XCTAssertEqual(presenterSpy.didCallDidReceiveAnError, 1)
        XCTAssertNotNil(presenterSpy.error)
    }
    
    func testDidTapRegister_ShouldCallAnalyticsActionsRestrictedAccount() {
        sut.didTapRegister()
        
        let expectedEvent = BacenEvent.actionsRestrictedAccount.event()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testDidTapRegister_ShouldCallDidNextStepOpenUpgradeChecklist() {
        sut.didTapRegister()
        
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 1)
        XCTAssertEqual(presenterSpy.action, .openUpgradeChecklist)
    }
    
    func testDidTapClose_ShouldCallDidNextStepClose() {
        sut.didTapClose()
        
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 1)
        XCTAssertEqual(presenterSpy.action, .close)
    }
    
    func testErrorDisplayed_ShouldCallDidNextStepClose() {
        sut.errorDisplayed()
        
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 1)
        XCTAssertEqual(presenterSpy.action, .close)
    }
}
