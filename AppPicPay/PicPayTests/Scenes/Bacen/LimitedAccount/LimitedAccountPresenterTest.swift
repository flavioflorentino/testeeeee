import XCTest
import CoreLegacy
@testable import PicPay

final class LimitedAccountCoordinatorSpy: LimitedAccountCoordinating {
    private(set) var didCallPerform = 0
    
    private(set) var action: LimitedAccountAction?
    
    var viewController: UIViewController?
    
    func perform(action: LimitedAccountAction) {
        didCallPerform += 1
        self.action = action
    }
}

final class LimitedAccountDisplaySpy: LimitedAccountDisplay {
    private(set) var didCallStartLoad = 0
    private(set) var didCallStopLoad = 0
    private(set) var didCallDisplayDescription = 0
    private(set) var didCallAddLimitedAccountRow = 0
    private(set) var didCallDisplayButton = 0
    private(set) var didCallDidReceiveAnError = 0
    
    private(set) var description: String?
    private(set) var infoBalances: [String] = []
    private(set) var balances: [String] = []
    private(set) var error: PicPayError?
    
    func startLoad() {
        didCallStartLoad += 1
    }
    
    func stopLoad() {
        didCallStopLoad += 1
    }
    
    func displayDescription(_ description: String) {
        didCallDisplayDescription += 1
        self.description = description
    }
    
    func addLimitedAccountRow(infoBalance: String, balance: String) {
        didCallAddLimitedAccountRow += 1
        infoBalances.append(infoBalance)
        balances.append(balance)
    }
    
    func displayButton() {
        didCallDisplayButton += 1
    }
    
    func didReceiveAnError(_ error: PicPayError) {
        didCallDidReceiveAnError += 1
        self.error = error
    }
}

final class LimitedAccountPresenterTest: XCTestCase {
    private let coordinatorSpy = LimitedAccountCoordinatorSpy()
    private let displaySpy = LimitedAccountDisplaySpy()
    
    private lazy var sut: LimitedAccountPresenting = {
        let presenter = LimitedAccountPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        
        return presenter
    }()
    
    private lazy var model: LimitedAccountContainer = {
        let item1 = LimitedAccount(name: "Item1", value: 100)
        let item2 = LimitedAccount(name: "Item2", value: 230.50)
        return LimitedAccountContainer(description: "description", limits: [item1, item2])
    }()
    
    func testLoadService_ShouldCallStartLoad() {
        sut.loadService()
        
        XCTAssertEqual(displaySpy.didCallStartLoad, 1)
    }
    
    func testDidReceiveAnError_ShouldCallStopLoad() {
        let error = PicPayError(message: "Error")
        sut.didReceiveAnError(error)
        
        XCTAssertEqual(displaySpy.didCallStopLoad, 1)
    }
    
    func testDidReceiveAnError_ShouldCallDidReceiveAnError() {
        let error = PicPayError(message: "Error")
        sut.didReceiveAnError(error)
        
        XCTAssertEqual(displaySpy.didCallDidReceiveAnError, 1)
        XCTAssertNotNil(displaySpy.error)
    }
    
    func testDidNextStep_WhenActionClose_ShouldCallPerform() {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.didCallPerform, 1)
        XCTAssertEqual(coordinatorSpy.action, .close)
    }
    
    func testDidNextStep_WhenActionOpenUpgradeChecklist_ShouldCallPerform() {
        sut.didNextStep(action: .openUpgradeChecklist)
        
        XCTAssertEqual(coordinatorSpy.didCallPerform, 1)
        XCTAssertEqual(coordinatorSpy.action, .openUpgradeChecklist)
    }
    
    func testUpdateView_ShouldCallStopLoad() {
        sut.updateView(data: model)
        
        XCTAssertEqual(displaySpy.didCallStopLoad, 1)
    }

    func testUpdateView_ShouldCallDisplayButton() {
        sut.updateView(data: model)
        
        XCTAssertEqual(displaySpy.didCallDisplayButton, 1)
    }
    
    func testUpdateView_ShouldCallDisplayDescription() {
        sut.updateView(data: model)
        
        XCTAssertEqual(displaySpy.didCallDisplayDescription, 1)
        XCTAssertEqual(displaySpy.description, "description")
    }
    
    func testUpdateView_ShouldCallAddLimitedAccountRow() {
        sut.updateView(data: model)
        
        XCTAssertEqual(displaySpy.didCallAddLimitedAccountRow, 2)
        XCTAssertEqual(displaySpy.infoBalances.first, "Item1")
        XCTAssertEqual(displaySpy.balances.first, CurrencyFormatter.brazillianRealString(from: 100))
        XCTAssertEqual(displaySpy.infoBalances.last, "Item2")
        XCTAssertEqual(displaySpy.balances.last, CurrencyFormatter.brazillianRealString(from: 230.50))
    }
}
