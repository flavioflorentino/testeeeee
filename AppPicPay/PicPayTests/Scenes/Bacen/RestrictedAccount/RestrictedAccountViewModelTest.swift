import XCTest
@testable import AnalyticsModule
@testable import PicPay

final class RestrictedAccountPresenterSpy: RestrictedAccountPresenting {
    private(set) var didCallLoadService = 0
    private(set) var didCallUpdateView = 0
    private(set) var didCallDidReceiveAnError = 0
    private(set) var didCallDidNextStep = 0
    
    private(set) var data: RestrictedAccount?
    private(set) var error: PicPayError?
    private(set) var action: RestrictedAccountAction?
    
    var viewController: RestrictedAccountDisplay?
    
    func loadService() {
        didCallLoadService += 1
    }
    
    func updateView(data: RestrictedAccount) {
        didCallUpdateView += 1
        self.data = data
    }
    
    func didReceiveAnError(_ error: PicPayError) {
        didCallDidReceiveAnError += 1
        self.error = error
    }
    
    func didNextStep(action: RestrictedAccountAction) {
        didCallDidNextStep += 1
        self.action = action
    }
}

final class RestrictedAccountServiceMock: RestrictedAccountServicing {
    var getRestrictedAccountStatusSuccess = true
    
    func getRestrictedAccountStatus(completion: @escaping (Result<RestrictedAccount, PicPayError>) -> Void) {
        guard getRestrictedAccountStatusSuccess else {
            let error = PicPayError(message: "Error")
            completion(.failure(error))
            return
        }
        
        do {
            let model = try MockDecodable<RestrictedAccount>().loadCodableObject(resource: "restrictedAccount", typeDecoder: .useDefaultKeys)
            completion(.success(model))
        } catch {
            XCTFail("Error Decodable RestrictedAccount")
        }
    }
}

final class RestrictedAccountViewModelTest: XCTestCase {
    private let analyticsSpy = AnalyticsSpy()
    private let serviceMock = RestrictedAccountServiceMock()
    private let presenterSpy = RestrictedAccountPresenterSpy()
    
    private lazy var sut: RestrictedAccountViewModelInputs = {
        RestrictedAccountViewModel(
            origin: "origin",
            dependencies: DependencyContainerMock(analyticsSpy),
            service: serviceMock,
            presenter: presenterSpy
        )
    }()
    
    func testUpdateView_ShouldCallAnalyticsOpenRestrictedAccount() {
        sut.updateView()
        
        let expectedEvent = BacenEvent.openRestrictedAccount(origin: "origin").event()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testUpdateView_ShouldCallLoadService() {
        sut.updateView()
        
        XCTAssertEqual(presenterSpy.didCallLoadService, 1)
    }
    
    func testUpdateView_WhenGetRestrictedAccountStatusSuccess_ShouldCallUpdateView() {
        serviceMock.getRestrictedAccountStatusSuccess = true
        
        sut.updateView()
        
        XCTAssertEqual(presenterSpy.didCallUpdateView, 1)
        XCTAssertEqual(presenterSpy.data?.unallowed.items.count, 3)
    }
    
    func testUpdateView_WhenGetRestrictedAccountStatusFailure_ShouldCallDidReceiveAnError() {
        serviceMock.getRestrictedAccountStatusSuccess = false
        
        sut.updateView()
        
        XCTAssertEqual(presenterSpy.didCallDidReceiveAnError, 1)
        XCTAssertNotNil(presenterSpy.error)
    }
    
    func testDidTapClose_ShouldCallDidNextStep() {
        sut.didTapClose()
        
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 1)
        XCTAssertEqual(presenterSpy.action, .close)
    }
    
    func testErrorDisplayed_ShouldNotCallDidNextStep() {
        sut.errorDisplayed()
        
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 1)
        XCTAssertEqual(presenterSpy.action, .close)
    }
    
    func testDidTapRow_WhenTapAllowedAccountStatusFailure_ShouldNotCallDidNextStep() {
        serviceMock.getRestrictedAccountStatusSuccess = false
        
        sut.updateView()
        sut.didTapRow(index: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 0)
    }
    
    func testDidTapRow_WhenTapAllowedAndNotAction_ShouldNotCallDidNextStep() {
        serviceMock.getRestrictedAccountStatusSuccess = true
        
        sut.updateView()
        sut.didTapRow(index: IndexPath(row: 1, section: 0))
        
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 0)
    }
    
    func testDidTapRow_WhenTapAllowedAndAction_ShouldCallDidNextStep() {
        serviceMock.getRestrictedAccountStatusSuccess = true
        
        sut.updateView()
        sut.didTapRow(index: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 1)
    }
    
    func testDidTapRow_WhenTapUnallowedAccountStatusFailure_ShouldNotCallDidNextStep() {
        serviceMock.getRestrictedAccountStatusSuccess = false
        
        sut.updateView()
        sut.didTapRow(index: IndexPath(row: 0, section: 1))
        
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 0)
    }
    
    func testDidTapRow_WhenTapUnallowedAndNotAction_ShouldNotCallDidNextStep() {
        serviceMock.getRestrictedAccountStatusSuccess = true
        
        sut.updateView()
        sut.didTapRow(index: IndexPath(row: 1, section: 1))
        
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 0)
    }
    
    func testDidTapRow_WhenTapUnallowedAndAction_ShouldCallDidNextStep() {
        serviceMock.getRestrictedAccountStatusSuccess = true
        
        sut.updateView()
        sut.didTapRow(index: IndexPath(row: 0, section: 1))
        
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 1)
    }
    
    func testDidTapRow_WhenTapNotSection_ShouldNotCallDidNextStep() {
        serviceMock.getRestrictedAccountStatusSuccess = true
        
        sut.updateView()
        sut.didTapRow(index: IndexPath(row: 0, section: 2))
        
        XCTAssertEqual(presenterSpy.didCallDidNextStep, 0)
    }
}
