import UI
import XCTest
@testable import PicPay

final class RestrictedAccountCoordinatorSpy: RestrictedAccountCoordinating {
    private(set) var didCallPerform = 0
    
    private(set) var action: RestrictedAccountAction?
    
    var viewController: UIViewController?
    
    func perform(action: RestrictedAccountAction) {
        didCallPerform += 1
        self.action = action
    }
}

final class RestrictedAccountDisplaySpy: RestrictedAccountDisplay {
    private(set) var didCallStartLoad = 0
    private(set) var didCallStopLoad = 0
    private(set) var didCallDisplayItensPayment = 0
    private(set) var didCallDisplayHeader = 0
    private(set) var didCallDidReceiveAnError = 0
    
    private(set) var data: [Section<RestrictedAccountHeader, RestrictedAccountCell>]?
    private(set) var header: RestrictedAccountHeader?
    private(set) var error: PicPayError?
    
    func startLoad() {
        didCallStartLoad += 1
    }
    
    func stopLoad() {
        didCallStopLoad += 1
    }
    
    func displayItensPayment(data: [Section<RestrictedAccountHeader, RestrictedAccountCell>]) {
        didCallDisplayItensPayment += 1
        self.data = data
    }
    
    func displayHeader(header: RestrictedAccountHeader) {
        didCallDisplayHeader += 1
        self.header = header
    }
    
    func didReceiveAnError(_ error: PicPayError) {
        didCallDidReceiveAnError += 1
        self.error = error
    }
}

class RestrictedAccountPresenterTest: XCTestCase {
    private let coordinatorSpy = RestrictedAccountCoordinatorSpy()
    private let displaySpy = RestrictedAccountDisplaySpy()
    
    private lazy var sut: RestrictedAccountPresenting = {
        let presenter = RestrictedAccountPresenter(coordinator: coordinatorSpy)
        presenter.viewController = displaySpy
        
        return presenter
    }()
    
    private func createModel() throws -> RestrictedAccount {
        try MockDecodable<RestrictedAccount>().loadCodableObject(resource: "restrictedAccount", typeDecoder: .useDefaultKeys)
    }
    
    func testLoadService_ShouldCallStartLoad() {
        sut.loadService()
        
        XCTAssertEqual(displaySpy.didCallStartLoad, 1)
    }
    
    func testUpdateView_ShouldCallStopLoad() throws {
        let model = try XCTUnwrap(createModel())
        sut.updateView(data: model)
        
        XCTAssertEqual(displaySpy.didCallStopLoad, 1)
    }
    
    func testUpdateView_ShouldCallDisplayItensPayment() throws {
        let model = try XCTUnwrap(createModel())
        sut.updateView(data: model)
        
        XCTAssertEqual(displaySpy.didCallDisplayItensPayment, 1)
        XCTAssertEqual(displaySpy.data?.count, 2)
    }
    
    func testUpdateView_ShouldCallDisplayHeader() throws {
        let model = try XCTUnwrap(createModel())
        sut.updateView(data: model)
        
        XCTAssertEqual(displaySpy.didCallDisplayHeader, 1)
        XCTAssertNotNil(displaySpy.header)
    }
    
    func testDidReceiveAnError_ShouldCallStopLoad() {
        let error = PicPayError(message: "Error")
        sut.didReceiveAnError(error)
        
        XCTAssertEqual(displaySpy.didCallStopLoad, 1)
    }
    
    func testDidReceiveAnError_ShouldCallDidReceiveAnError() {
        let error = PicPayError(message: "Error")
        sut.didReceiveAnError(error)
        
        XCTAssertEqual(displaySpy.didCallDidReceiveAnError, 1)
        XCTAssertNotNil(displaySpy.error)
    }
    
    func testDidNextStep_WhenActionClose_ShouldCallPerform() {
        sut.didNextStep(action: .close)
        
        XCTAssertEqual(coordinatorSpy.didCallPerform, 1)
        XCTAssertEqual(coordinatorSpy.action, .close)
    }
    
    func testDidNextStep_WhenActionOpenDeeplink_ShouldCallPerform() throws {
        let url = URL(string: "https://www.picpay.com/site")
        let link = try XCTUnwrap(url)
        sut.didNextStep(action: .openDeeplink(url: link))
        
        XCTAssertEqual(coordinatorSpy.action, .openDeeplink(url: link))
    }
}
