import XCTest
import Core
import AnalyticsModule
import FeatureFlag
@testable import PicPay

private final class UpdatePrivacyServiceSpy: UpdatePrivacyServicing {
    private(set) var callUpdateSettingsCount = 0
    var testingSuccess = false
    
    func updateSettings(configs: [String: Bool], reviewedFrom: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        callUpdateSettingsCount += 1
        
        if testingSuccess {
            completion(.success(NoContent()))
        } else {
            completion(.failure(ApiError.connectionFailure))
        }
    }
    
    func checkRevisionStatus(completion: @escaping (Result<RevisionStatusModel, ApiError>) -> Void) {
        // Não está implementado aqui pois o serviço ainda não está sendo usado
    }
}

private final class UpdatePrivacySettingsPresenterSpy: UpdatePrivacySettingsPresenting {
    var viewController: UpdatePrivacySettingsDisplaying?

    private(set) var callStartLoadingCount = 0
    private(set) var callShowSuccessCount = 0
    private(set) var callShowErrorCount = 0
    private(set) var callConfigurePrivacySeetingsCount = 0
    private(set) var callGoToHomeCount = 0
    private(set) var callConfigureConfirmationPopupCount = 0
    private(set) var callPrivacySettingsPopupCallbackCount = 0
    
    func configurePrivacySettings() {
        callConfigurePrivacySeetingsCount += 1
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func showSuccess() {
        callShowSuccessCount += 1
    }
    
    func showError() {
        callShowErrorCount += 1
    }

    func goToHome() {
        callGoToHomeCount += 1
    }
    
    func configureConfirmationPopup() {
        callConfigureConfirmationPopupCount += 1
    }
    
    func privacySettingsPopupCallback(data: PopupSettingsOption) {
        callPrivacySettingsPopupCallbackCount += 1
    }
}

final class UpdatePrivacySettingsInteractorTests: XCTestCase {
    private let configMock = ["teste": true]
    
    private lazy var kvStoreMock = KVStoreMock()
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var consumerMock = ConsumerManagerMock()
    private lazy var featureManagerMock = FeatureManagerMock()
    private lazy var dependenciesMock = DependencyContainerMock(kvStoreMock, featureManagerMock, analyticsSpy, consumerMock)

    private let serviceSpy = UpdatePrivacyServiceSpy()
    private let presenterSpy = UpdatePrivacySettingsPresenterSpy()
    
    lazy var sut: UpdatePrivacySettingsInteracting = {
        let interactor = UpdatePrivacySettingsInteractor(service: serviceSpy,
                                                         presenter: presenterSpy,
                                                         origin: .transaction,
                                                         dependencies: dependenciesMock)
        return interactor
    }()
    
    func testLoadContent_ShouldCallConfigurePrivacySettings() {
        sut.loadContent()
        
        XCTAssertEqual(presenterSpy.callConfigurePrivacySeetingsCount, 1)
    }
    
    func testUpdateSetting_WhenReturnIsSuccess_ShouldCallPresenterMethods() {
        serviceSpy.testingSuccess = true
        sut.updateConfigs(defaultConfigs: configMock, payInPublic: true)
        
        XCTAssertEqual(serviceSpy.callUpdateSettingsCount, 1)
        XCTAssertEqual(presenterSpy.callStartLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callShowSuccessCount, 1)
        XCTAssertEqual(presenterSpy.callShowErrorCount, 0)
    }
    
    func testUpdateSetting_WhenReturnIsFailure_ShouldCallPresenterMethods() {
        serviceSpy.testingSuccess = false
        sut.updateConfigs(defaultConfigs: configMock, payInPublic: true)
        
        XCTAssertEqual(serviceSpy.callUpdateSettingsCount, 1)
        XCTAssertEqual(presenterSpy.callStartLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callShowSuccessCount, 0)
        XCTAssertEqual(presenterSpy.callShowErrorCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testDismiss_ShouldCallPresenterMethod() {
        sut.dismiss()
        
        XCTAssertEqual(presenterSpy.callGoToHomeCount, 1)
    }
    
    func testSendAnalyticsEventForShowingDialog_WhenItsCalledByController() {
        sut.sendAnalyticsEventForShowingDialog(type: .payment)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
}
