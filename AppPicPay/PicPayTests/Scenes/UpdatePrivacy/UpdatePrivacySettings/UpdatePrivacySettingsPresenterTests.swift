import XCTest
import AssetsKit
import UI
@testable import PicPay

private final class UpdatePrivacySettingsViewControllerSpy: UpdatePrivacySettingsDisplaying {
    private(set) var callStartLoadingCount = 0
    private(set) var callStopLoadingCount = 0
    private(set) var callConfigureProfileSwitchCount = 0
    private(set) var profileSwitchTitle = ""
    private(set) var profileSwitchDescription = ""
    private(set) var callConfigurePaymentSwitchCount = 0
    private(set) var paymentSwitchTitle = ""
    private(set) var paymentSwitchDescription = ""
    private(set) var callConfigureReceiveSwitchCount = 0
    private(set) var receiveSwitchTitle = ""
    private(set) var receiveSwitchDescription = ""
    private(set) var callConfigureWarningCount = 0
    private(set) var warningImagem: UIImage?
    private(set) var warningDescription = ""
    
    private(set) var callDisplaySuccessCount = 0
    private(set) var modelSuccess: StatefulFeedbackViewModel?
    
    private(set) var callDisplayErrorCount = 0
    private(set) var modelError: StatefulFeedbackViewModel?
    
    private(set) var callConfigureDescriptionCount = 0
    private(set) var descriptionText = ""
    
    private(set) var callOpenPopupCount = 0
    private(set) var expectedModel: PrivacySettingsPopupViewModel?
    private(set) var callSetConfirmationPropertiesCount = 0
    private(set) var selectedOption: PopupSettingsOption?
    
    func configureWarning(with image: UIImage, and description: NSAttributedString) {
        callConfigureWarningCount += 1
        warningImagem = image
        warningDescription = description.string
    }
    
    func configureDescription(with text: String) {
        callConfigureDescriptionCount += 1
        descriptionText = text
    }
    
    func configureProfileSwitch(with title: String, and description: NSAttributedString) {
        callConfigureProfileSwitchCount += 1
        profileSwitchTitle = title
        profileSwitchDescription = description.string
    }
    
    func configurePaymentSwitch(with title: String, and description: NSAttributedString) {
        callConfigurePaymentSwitchCount += 1
        paymentSwitchTitle = title
        paymentSwitchDescription = description.string
    }
    
    func configureReceiveSwitch(with title: String, and description: NSAttributedString) {
        callConfigureReceiveSwitchCount += 1
        receiveSwitchTitle = title
        receiveSwitchDescription = description.string
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func displaySuccess(with success: StatefulFeedbackViewModel) {
        callDisplaySuccessCount += 1
        modelSuccess = success
    }
    
    func displayError(with error: StatefulFeedbackViewModel) {
        callDisplayErrorCount += 1
        modelError = error
    }
    
    func openPopup(with model: PrivacySettingsPopupViewModel) {
        callOpenPopupCount += 1
        expectedModel = model
    }
    
    func setConfirmationProperties(with data: PopupSettingsOption) {
        callSetConfirmationPropertiesCount += 1
        selectedOption = data
    }
}

private final class UpdatePrivacySettingsCoordinatorSpy: UpdatePrivacySettingsCoordinating {
    var viewController: UIViewController?
    
    private(set) var callDismissCount = 0
    
    func dismiss() {
        callDismissCount += 1
    }
}

final class UpdatePrivacySettingsPresenterTests: XCTestCase {
    typealias Localizable = Strings.UpdatePrivacy
    
    private let coordinatorSpy = UpdatePrivacySettingsCoordinatorSpy()
    private let viewControllerSpy = UpdatePrivacySettingsViewControllerSpy()
    
    lazy var sut: UpdatePrivacySettingsPresenting = {
        let presenter = UpdatePrivacySettingsPresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testConfigurePrivacySettings_ShouldConfigureView() {
        sut.configurePrivacySettings()
        
        XCTAssertEqual(viewControllerSpy.callConfigureWarningCount, 1)
        XCTAssertEqual(viewControllerSpy.warningImagem, Resources.Icons.icoBlueInfo.image)
        XCTAssertEqual(viewControllerSpy.callConfigureDescriptionCount, 1)
        XCTAssertEqual(viewControllerSpy.descriptionText, Localizable.updatePrivacySettingsDecription)
        // Foi necessário faz o replace na string para pegar apenas o seu valor para a comparação
        XCTAssertEqual(viewControllerSpy.warningDescription, Localizable.updatePrivacySettingsWarning.replacingOccurrences(of: "*", with: ""))
        XCTAssertEqual(viewControllerSpy.callConfigureProfileSwitchCount, 1)
        XCTAssertEqual(viewControllerSpy.profileSwitchTitle, Localizable.updatePrivacySettingPublicProfileTitle)
        // Foi necessário faz o replace na string para pegar apenas o seu valor para a comparação
        XCTAssertEqual(viewControllerSpy.profileSwitchDescription, Localizable.updatePrivacySettingPublicProfileDescription.replacingOccurrences(of: "*", with: ""))
        XCTAssertEqual(viewControllerSpy.callConfigurePaymentSwitchCount, 1)
        XCTAssertEqual(viewControllerSpy.paymentSwitchTitle, Localizable.updatePrivacySettingPublicPaymentsTitle)
        // Foi necessário faz o replace na string para pegar apenas o seu valor para a comparação
        XCTAssertEqual(viewControllerSpy.paymentSwitchDescription, Localizable.updatePrivacySettingPublicPaymentsDescription.replacingOccurrences(of: "*", with: ""))
        XCTAssertEqual(viewControllerSpy.callConfigureReceiveSwitchCount, 1)
        XCTAssertEqual(viewControllerSpy.receiveSwitchTitle, Localizable.updatePrivacySettingPublicReceivesTitle)
        // Foi necessário faz o replace na string para pegar apenas o seu valor para a comparação
        XCTAssertEqual(viewControllerSpy.receiveSwitchDescription, Localizable.updatePrivacySettingPublicReceivesDescription.replacingOccurrences(of: "*", with: ""))
    }

    func testGoToHome_ShouldCallCoordinatorMethod() {
        sut.goToHome()
        
        XCTAssertEqual(coordinatorSpy.callDismissCount, 1)
    }
    
    func testStartLoading_ShouldCallViewControllerMethod() {
        sut.startLoading()
        
        XCTAssertEqual(viewControllerSpy.callStartLoadingCount, 1)
        XCTAssertEqual(viewControllerSpy.callStopLoadingCount, 0)
    }
    
    func testShowSuccess_ShouldCallViewControllerToChangeState() {
        let model = StatefulFeedbackViewModel(
            image: Resources.Illustrations.iluSuccessWithBackground.image,
            content: (
                title: Localizable.updatePrivacyConfirmationTitle,
                description: Localizable.updatePrivacyConfirmationDescription
            ),
            button: (image: nil, title: Localizable.updatePrivacyConfirmationButtonClose),
            secondaryButton: nil
        )
        
        sut.showSuccess()
        
        XCTAssertEqual(viewControllerSpy.callDisplaySuccessCount, 1)
        XCTAssertEqual(viewControllerSpy.callDisplayErrorCount, 0)
        XCTAssertEqual(viewControllerSpy.modelSuccess?.image, model.image)
        XCTAssertEqual(viewControllerSpy.modelSuccess?.content?.title, model.content?.title)
        XCTAssertEqual(viewControllerSpy.modelSuccess?.content?.description, model.content?.description)
        XCTAssertEqual(viewControllerSpy.modelSuccess?.button?.title, model.button?.title)
    }
    
    func testShowError_ShouldCallViewControllerToChangeState() {
        let model = StatefulFeedbackViewModel(
            image: Resources.Icons.icoSad.image,
            content: (
                title: Localizable.updatePrivacyErrorTitle,
                description: Localizable.updatePrivacyErrorDescription
            ),
            button: (image: nil, title: Localizable.updatePrivacyErrorButtonTitle),
            secondaryButton: nil
        )
        sut.showError()
        
        XCTAssertEqual(viewControllerSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(viewControllerSpy.callDisplaySuccessCount, 0)
        XCTAssertEqual(viewControllerSpy.modelError?.image, model.image)
        XCTAssertEqual(viewControllerSpy.modelError?.content?.title, model.content?.title)
        XCTAssertEqual(viewControllerSpy.modelError?.content?.description, model.content?.description)
        XCTAssertEqual(viewControllerSpy.modelError?.button?.title, model.button?.title)
    }
    
    func testConfigureConfirmationPopup_ShouldCallViewControllerToOpenPopup() {
        let options = [
            PopupSettingsOptionData(.hidePastActivities, Localizable.updatePrivacyPopupFirstOptionTitle),
            PopupSettingsOptionData(.hideFromNow, Localizable.updatePrivacyPopupSecondOptionTitle)
        ]
        
        let model = PrivacySettingsPopupViewModel(title: Localizable.updatePrivacyPopupTitle,
                                                  primaryButtonTitle: Localizable.updatePrivacyPopupButtonTitle,
                                                  options: options,
                                                  primaryButtonAction: viewControllerSpy.setConfirmationProperties)
        
        sut.configureConfirmationPopup()
        
        XCTAssertEqual(viewControllerSpy.callOpenPopupCount, 1)
        XCTAssertEqual(viewControllerSpy.expectedModel?.title, model.title)
        XCTAssertEqual(viewControllerSpy.expectedModel?.primaryButtonTitle, model.primaryButtonTitle)
        XCTAssertEqual(viewControllerSpy.expectedModel?.options.count, model.options.count)
    }
    
    func testPrivacySettingsPopupCallback_ShoudlCallViewControllerToSetProperties() {
        let dataMocked = PopupSettingsOption.hidePastActivities

        sut.privacySettingsPopupCallback(data: dataMocked)
        
        XCTAssertEqual(viewControllerSpy.callSetConfirmationPropertiesCount, 1)
        XCTAssertEqual(viewControllerSpy.selectedOption?.rawValue, dataMocked.rawValue)
    }
}
