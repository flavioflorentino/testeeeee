import XCTest
@testable import PicPay

final class UpdatePrivacySettingsCoordinatorTestsTests: XCTestCase {
    private let viewControllerMock = ViewControllerMock()
    private lazy var navControllerMock = NavigationControllerMock(rootViewController: viewControllerMock)
    
    private lazy var sut: UpdatePrivacySettingsCoordinating = {
        let coordinator = UpdatePrivacySettingsCoordinator()
        coordinator.viewController = navControllerMock.topViewController
        return coordinator
    }()
    
    func testDismiss_ShouldPopToRootViewController() {
        sut.dismiss()
        
        XCTAssertTrue(navControllerMock.isPopToRootViewControllerCalled)
    }
}
