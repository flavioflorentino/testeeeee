import XCTest
import Core
import FeatureFlag
import AnalyticsModule
@testable import PicPay

private final class UpdatePrivacyServiceSpy: UpdatePrivacyServicing {
    private(set) var callUpdateSettingsCount = 0
    var testingSuccess = false
    
    func updateSettings(configs: [String: Bool], reviewedFrom: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        callUpdateSettingsCount += 1
        
        if testingSuccess {
            completion(.success(NoContent()))
        } else {
            completion(.failure(ApiError.connectionFailure))
        }
    }
  
    func checkRevisionStatus(completion: @escaping (Result<RevisionStatusModel, ApiError>) -> Void) {
        // Não está implementado aqui pois o serviço ainda não está sendo usado
    }
}

private final class UpdatePrivacyMessagePresenterSpy: UpdatePrivacyMessagePresenting {
    var viewController: UpdatePrivacyMessageDisplaying?
    
    private(set) var callDidNextStepCount = 0
    private(set) var callConfigureViewCount = 0
    private(set) var callConfigureViewWithSecondTextCount = 0
    private(set) var callStartLoadingCount = 0
    private(set) var callShowSuccessCount = 0
    private(set) var callShowErrorCount = 0
    private(set) var action: UpdatePrivacyMessageAction?
    
    func configureView() {
        callConfigureViewCount += 1
    }
    
    func configureViewWithSecondTextOption() {
        callConfigureViewWithSecondTextCount += 1
    }
    
    func didNextStep(action: UpdatePrivacyMessageAction) {
        callDidNextStepCount += 1
        self.action = action
    }
    
    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func showSuccess() {
        callShowSuccessCount += 1
    }
    
    func showError() {
        callShowErrorCount += 1
    }
}

final class UpdatePrivacyMessageInteractorTests: XCTestCase {
    private lazy var kvStoreMock = KVStoreMock()
    private lazy var featureManagerMock = FeatureManagerMock()
    private lazy var consumerMock = ConsumerManagerMock()
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var dependenciesMock = DependencyContainerMock(kvStoreMock, featureManagerMock, consumerMock, analyticsSpy)

    private let presenterSpy = UpdatePrivacyMessagePresenterSpy()
    private let serviceSpy = UpdatePrivacyServiceSpy()
    
    lazy var sut = UpdatePrivacyMessageInteractor(service: serviceSpy,
                                                  presenter: presenterSpy,
                                                  origin: .transaction,
                                                  dependencies: dependenciesMock)
    
    func testLoadContent_WithFirstTextOption_ShouldCallConfigureView() {
        featureManagerMock.override(keys: .experimentReviewPrivacyText, with: true)
        sut.loadContent()
        
        XCTAssertEqual(presenterSpy.callConfigureViewCount, 1)
    }
    
    func testLoadContent_WithSecondTextOption_ShouldCallConfigureViewWithSecondText() {
        featureManagerMock.override(keys: .experimentReviewPrivacyText, with: false)
        sut.loadContent()
        
        XCTAssertEqual(presenterSpy.callConfigureViewWithSecondTextCount, 1)
    }
    
    func testGoToReviewPrivacySettings_ShouldCallPresenterMethod() {
        sut.goToReviewPrivacySettings()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.action, .reviewPrivacySettings)
    }
    
    func testDismiss_ShouldCallPresenterMethod() {
        sut.dismiss()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.action, .dismiss)
    }
    
    func testSaveDefaultConfig_WithSuccess_ShouldDisplaySuccess() {
        serviceSpy.testingSuccess = true
        sut.saveDefaultConfigs()
        
        XCTAssertEqual(serviceSpy.callUpdateSettingsCount, 1)
        XCTAssertEqual(presenterSpy.callStartLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callShowSuccessCount, 1)
        XCTAssertEqual(presenterSpy.callShowErrorCount, 0)
        XCTAssertEqual(analyticsSpy.logCalledCount, 2)
    }
    
    func testSaveDefaultConfig_WithError_ShouldDisplayError() {
        serviceSpy.testingSuccess = false
        sut.saveDefaultConfigs()
        
        XCTAssertEqual(serviceSpy.callUpdateSettingsCount, 1)
        XCTAssertEqual(presenterSpy.callStartLoadingCount, 1)
        XCTAssertEqual(presenterSpy.callShowSuccessCount, 0)
        XCTAssertEqual(presenterSpy.callShowErrorCount, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 2)
    }
    
    func testSendAnalyticsEvents_WhenItsCalledByController() {
        sut.sendAnalyticsForViewingIntroScreen()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
}
