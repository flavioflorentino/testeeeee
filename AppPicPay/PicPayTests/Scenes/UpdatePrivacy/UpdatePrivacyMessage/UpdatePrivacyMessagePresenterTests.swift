import XCTest
import AssetsKit
import UI
@testable import PicPay

private final class UpdatePrivacyMessageViewControllerSpy: UpdatePrivacyMessageDisplaying {
    private(set) var callSetTitleCount = 0
    private(set) var title = ""
    private(set) var callSetDescriptionCount = 0
    private(set) var description = ""
    private(set) var callSetFirstItemDescriptionCount = 0
    private(set) var firstItemDescription = ""
    private(set) var firstImage: UIImage?
    private(set) var callSetSecondItemDescriptionCount = 0
    private(set) var secondItemDescription = ""
    private(set) var secondImage: UIImage?
    private(set) var callSetThirdItemDescriptionCount = 0
    private(set) var thirdItemDescription = ""
    private(set) var thirdImage: UIImage?
    
    private(set) var callStartLoadingCount = 0
    private(set) var callDisplaySuccessCount = 0
    private(set) var modelSuccess: StatefulFeedbackViewModel?
    
    private(set) var callDisplayErrorCount = 0
    private(set) var modelError: StatefulFeedbackViewModel?
    
    private(set) var callSetButtonTitleCount = 0
    private(set) var buttonTittle = ""
    
    func setTitle(with text: String) {
        callSetTitleCount += 1
        title = text
    }

    func setDescription<S>(with text: String, and style: S) where S: LabelStyle {
        callSetDescriptionCount += 1
        description = text
    }
    
    func setFirstItemDescription(with text: String, and image: UIImage) {
        callSetFirstItemDescriptionCount += 1
        firstItemDescription = text
        firstImage = image
    }
    
    func setSecondItemDescription(with text: String, and image: UIImage) {
        callSetSecondItemDescriptionCount += 1
        secondItemDescription = text
        secondImage = image
    }
    
    func setThirdItemDescription(with text: String, and image: UIImage) {
        callSetThirdItemDescriptionCount += 1
        thirdItemDescription = text
        thirdImage = image
    }
    
    func setButtonTitle(with text: String) {
        callSetButtonTitleCount += 1
        buttonTittle = text
    }

    func startLoading() {
        callStartLoadingCount += 1
    }
    
    func displaySuccess(with success: StatefulFeedbackViewModel) {
        callDisplaySuccessCount += 1
        modelSuccess = success
    }
    
    func displayError(with error: StatefulFeedbackViewModel) {
        callDisplayErrorCount += 1
        modelError = error
    }
}

private final class UpdatePrivacyMessageCoordinatorSpy: UpdatePrivacyMessageCoordinating {
    var viewController: UIViewController?
    
    private(set) var callPerformCount = 0
    private(set) var actionSelected: UpdatePrivacyMessageAction?
    
    func perform(action: UpdatePrivacyMessageAction) {
        callPerformCount += 1
        actionSelected = action
    }
}

final class UpdatePrivacyMessagePresenterTests: XCTestCase {
    typealias Localizable = Strings.UpdatePrivacy
    
    private let coordinatorSpy = UpdatePrivacyMessageCoordinatorSpy()
    private let viewControllerSpy = UpdatePrivacyMessageViewControllerSpy()
    
    lazy var sut: UpdatePrivacyMessagePresenting = {
        let presenter = UpdatePrivacyMessagePresenter(coordinator: coordinatorSpy)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testConfigureView_ShouldConfigureViewController() {
        sut.configureView()
        
        XCTAssertEqual(viewControllerSpy.callSetTitleCount, 1)
        XCTAssertEqual(viewControllerSpy.title, Localizable.updatePrivacyMessageTitle)
        XCTAssertEqual(viewControllerSpy.callSetDescriptionCount, 1)
        XCTAssertEqual(viewControllerSpy.description, Localizable.updatePrivacyMessageDescription)
        XCTAssertEqual(viewControllerSpy.callSetFirstItemDescriptionCount, 1)
        XCTAssertEqual(viewControllerSpy.firstItemDescription, Localizable.updatePrivacyMessageDescriptionItem1)
        XCTAssertEqual(viewControllerSpy.firstImage, Resources.Icons.icoValueGray.image)
        XCTAssertEqual(viewControllerSpy.callSetSecondItemDescriptionCount, 1)
        XCTAssertEqual(viewControllerSpy.secondItemDescription, Localizable.updatePrivacyMessageDescriptionItem2)
        XCTAssertEqual(viewControllerSpy.secondImage, Resources.Icons.icoLock.image)
        XCTAssertEqual(viewControllerSpy.callSetThirdItemDescriptionCount, 1)
        XCTAssertEqual(viewControllerSpy.thirdItemDescription, Localizable.updatePrivacyMessageDescriptionItem3)
        XCTAssertEqual(viewControllerSpy.thirdImage, Resources.Icons.icoSafe.image)
        XCTAssertEqual(viewControllerSpy.callSetButtonTitleCount, 1)
        XCTAssertEqual(viewControllerSpy.buttonTittle, Localizable.updatePrivacyMessagePrimaryButtonTitle)
    }
    
    func testConfigureViewWithSecondTextOption_ShouldConfigureViewController() {
        sut.configureViewWithSecondTextOption()
        
        XCTAssertEqual(viewControllerSpy.callSetTitleCount, 1)
        XCTAssertEqual(viewControllerSpy.title, Localizable.updatePrivacyMessageTitleSecondOption)
        XCTAssertEqual(viewControllerSpy.callSetDescriptionCount, 1)
        XCTAssertEqual(viewControllerSpy.description, Localizable.updatePrivacyMessageDescriptionSecondOption)
        XCTAssertEqual(viewControllerSpy.callSetFirstItemDescriptionCount, 1)
        XCTAssertEqual(viewControllerSpy.firstItemDescription, Localizable.updatePrivacyMessageDescriptionItem1SecondOption)
        XCTAssertEqual(viewControllerSpy.firstImage, Resources.Icons.icoValueGray.image)
        XCTAssertEqual(viewControllerSpy.callSetSecondItemDescriptionCount, 1)
        XCTAssertEqual(viewControllerSpy.secondItemDescription, Localizable.updatePrivacyMessageDescriptionItem2SecondOption)
        XCTAssertEqual(viewControllerSpy.secondImage, Resources.Icons.icoLock.image)
        XCTAssertEqual(viewControllerSpy.callSetThirdItemDescriptionCount, 0)
        XCTAssertEqual(viewControllerSpy.callSetButtonTitleCount, 1)
        XCTAssertEqual(viewControllerSpy.buttonTittle, Localizable.updatePrivacyMessagePrimaryButtonTitleSecondOption)
    }
    
    func testDidNextStep_WithReviewPrivacySettingsAction_ShouldCallCoordinatorMethod() {
        sut.didNextStep(action: .reviewPrivacySettings)
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .reviewPrivacySettings)
    }
    
    func testDidNextStep_WithDismissAction_ShouldCallCoordinatorMethod() {
        sut.didNextStep(action: .dismiss)
        
        XCTAssertEqual(coordinatorSpy.callPerformCount, 1)
        XCTAssertEqual(coordinatorSpy.actionSelected, .dismiss)
    }
    
    func testStartLoading_ShouldCallViewControllerMethod() {
        sut.startLoading()
        
        XCTAssertEqual(viewControllerSpy.callStartLoadingCount, 1)
    }
    
    func testShowSuccess_ShouldCallViewControllerToChangeState() {
        let model = StatefulFeedbackViewModel(
            image: Resources.Illustrations.iluSuccessWithBackground.image,
            content: (
                title: Localizable.updatePrivacyConfirmationTitle,
                description: Localizable.updatePrivacyConfirmationDescription
            ),
            button: (image: nil, title: Localizable.updatePrivacyConfirmationButtonClose),
            secondaryButton: nil
        )
        
        sut.showSuccess()
        
        XCTAssertEqual(viewControllerSpy.callDisplaySuccessCount, 1)
        XCTAssertEqual(viewControllerSpy.callDisplayErrorCount, 0)
        XCTAssertEqual(viewControllerSpy.modelSuccess?.image, model.image)
        XCTAssertEqual(viewControllerSpy.modelSuccess?.content?.title, model.content?.title)
        XCTAssertEqual(viewControllerSpy.modelSuccess?.content?.description, model.content?.description)
        XCTAssertEqual(viewControllerSpy.modelSuccess?.button?.title, model.button?.title)
    }
    
    func testShowError_ShouldCallViewControllerToChangeState() {
        let model = StatefulFeedbackViewModel(
            image: Resources.Icons.icoSad.image,
            content: (
                title: Localizable.updatePrivacyErrorTitle,
                description: Localizable.updatePrivacyErrorDescription
            ),
            button: (image: nil, title: Localizable.updatePrivacyErrorButtonTitle),
            secondaryButton: nil
        )
        sut.showError()
        
        XCTAssertEqual(viewControllerSpy.callDisplayErrorCount, 1)
        XCTAssertEqual(viewControllerSpy.callDisplaySuccessCount, 0)
        XCTAssertEqual(viewControllerSpy.modelError?.image, model.image)
        XCTAssertEqual(viewControllerSpy.modelError?.content?.title, model.content?.title)
        XCTAssertEqual(viewControllerSpy.modelError?.content?.description, model.content?.description)
        XCTAssertEqual(viewControllerSpy.modelError?.button?.title, model.button?.title)
    }
}
