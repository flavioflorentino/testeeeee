import XCTest
@testable import PicPay

final class UpdatePrivacyMessageCoordinatorTests: XCTestCase {
    private let viewControllerMock = ViewControllerMock()
    private lazy var navControllerMock = NavigationControllerMock(rootViewController: viewControllerMock)
    
    private lazy var sut: UpdatePrivacyMessageCoordinating = {
        let coordinator = UpdatePrivacyMessageCoordinator(origin: .transaction)
        coordinator.viewController = navControllerMock.topViewController
        return coordinator
    }()
    
    func testPerform_WhenCalledWithDismissAction_ShouldDismissViewController() {
        sut.perform(action: .dismiss)
        
        XCTAssertTrue(navControllerMock.isPopToRootViewControllerCalled)
    }
    
    func testPermorm_WhenCalledWithPrivacySettingsAction_ShouldPresentNewViewController() {
        sut.perform(action: .reviewPrivacySettings)
        
        XCTAssertTrue(navControllerMock.isPushViewControllerCalled)
        XCTAssertTrue(navControllerMock.pushedViewController is UpdatePrivacySettingsViewController)
    }
}
