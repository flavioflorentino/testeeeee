import XCTest
@testable import PicPay

private final class MockEcommerceLoadingService: EcommerceLoadingServicing {
    private let ecommerceIsActive: Bool
    private let resquestSuccess: Bool
    private let withAlert: Bool
    
    init(ecommerceIsActive: Bool = true, resquestSuccess: Bool = true, withAlert: Bool = true) {
        self.ecommerceIsActive = ecommerceIsActive
        self.resquestSuccess = resquestSuccess
        self.withAlert = withAlert
    }
    
    var newEcommerceIsActive: Bool {
        return ecommerceIsActive
    }
    
    func getOrderInfo(_ id: String, _ completion: @escaping (Result<EcommercePaymentInfo, PicPayError>) -> Void) {
        let json = withAlert ? MockJSON().load(resource: "ecommerceInfoAlert") : MockJSON().load(resource: "ecommerceInfo")
        guard resquestSuccess, let result = EcommercePaymentInfo(json: json) else {
            let picpayError = PicPayError(message: "Error")
            completion(.failure(picpayError))
            return
        }
        
        completion(.success(result))
    }
}

private final class SpyEcommerceLoadingPresenter: EcommerceLoadingPresenting {
    var viewController: EcommerceLoadingDisplay?
    
    private(set) var calledDidReceiveAnError = 0
    private(set) var calledShowEcommerceAlert = 0
    private(set) var calledDidNextStep = 0
    
    private(set) var receiveError: PicPayError?
    private(set) var ecommerceAlert: EcommerceAlert?
    private(set) var stepAction: EcommerceLoadingAction?
    
    func didReceiveAnError(_ error: PicPayError) {
        calledDidReceiveAnError += 1
        receiveError = error
    }
    
    func showEcommerceAlert(_ alert: EcommerceAlert) {
        calledShowEcommerceAlert += 1
        ecommerceAlert = alert
    }
    
    func didNextStep(action: EcommerceLoadingAction) {
        calledDidNextStep += 1
        stepAction = action
    }
}
private final class EcommerceLoadingViewModelTest: XCTestCase {
    func testViewDidLoadShouldCallOpenLegacyEcommercePaymentWhenRequestSuccessAndNoAlert() throws {
        let mockService = MockEcommerceLoadingService(ecommerceIsActive: false, resquestSuccess: true, withAlert: false)
        let spyPresenter = SpyEcommerceLoadingPresenter()
        let viewModel = EcommerceLoadingViewModel(orderId: "123", origin: .deeplink, service: mockService, presenter: spyPresenter)
        
        viewModel.viewDidLoad()
        
        XCTAssertEqual(spyPresenter.calledDidNextStep, 1)
        switch try spyPresenter.stepAction.safe() {
        case .openLegacyEcommercePayment:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testViewDidLoadShouldCallOpenNewEcommercePaymentWhenRequestSuccessAndNoAlert() throws {
        let mockService = MockEcommerceLoadingService(ecommerceIsActive: true, resquestSuccess: true, withAlert: false)
        let spyPresenter = SpyEcommerceLoadingPresenter()
        let viewModel = EcommerceLoadingViewModel(orderId: "123", origin: .deeplink, service: mockService, presenter: spyPresenter)
        
        viewModel.viewDidLoad()
        
        XCTAssertEqual(spyPresenter.calledDidNextStep, 1)
        switch try spyPresenter.stepAction.safe() {
        case .openNewEcommercePayment:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
    
    func testViewDidLoadShouldCallShowEcommerceAlertWhenRequestSuccessAndAlert() {
        let mockService = MockEcommerceLoadingService(resquestSuccess: true, withAlert: true)
        let spyPresenter = SpyEcommerceLoadingPresenter()
        let viewModel = EcommerceLoadingViewModel(orderId: "123", origin: .deeplink, service: mockService, presenter: spyPresenter)
        
        viewModel.viewDidLoad()
        
        XCTAssertEqual(spyPresenter.calledShowEcommerceAlert, 1)
    }
    
    func testViewDidLoadShouldCallDidReceiveAnErrorWhenRequestFailure() {
        let mockService = MockEcommerceLoadingService(ecommerceIsActive: false, resquestSuccess: false, withAlert: true)
        let spyPresenter = SpyEcommerceLoadingPresenter()
        let viewModel = EcommerceLoadingViewModel(orderId: "123", origin: .deeplink, service: mockService, presenter: spyPresenter)
        
        viewModel.viewDidLoad()
        
        XCTAssertEqual(spyPresenter.calledDidReceiveAnError, 1)
    }
    
    func testCloseShouldCallDidNextStep() throws {
        let mockService = MockEcommerceLoadingService()
        let spyPresenter = SpyEcommerceLoadingPresenter()
        let viewModel = EcommerceLoadingViewModel(orderId: "123", origin: .deeplink, service: mockService, presenter: spyPresenter)
        
        viewModel.close()
        
        XCTAssertEqual(spyPresenter.calledDidNextStep, 1)
        switch try spyPresenter.stepAction.safe() {
        case .close:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
    }
}
