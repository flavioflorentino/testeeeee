import XCTest
@testable import PicPay

private final class MockEcommerceLoadingCoordinator: EcommerceLoadingCoordinating {
    private(set) var calledPerform = 0
    
    var viewController: UIViewController?
    
    func perform(action: EcommerceLoadingAction) {
        calledPerform += 1
    }
}

private final class SpyEcommerceLoadingViewController: EcommerceLoadingDisplay {
    private(set) var calledDidReceiveAnError = 0
    private(set) var calledDisplayEcommerceAlert = 0
    
    func didReceiveAnError(_ error: PicPayError) {
        calledDidReceiveAnError += 1
    }
    
    func displayEcommerceAlert(_ alert: EcommerceAlert) {
        calledDisplayEcommerceAlert += 1
    }
}

private final class EcommerceLoadingPresenterTest: XCTestCase {
    private let queue = DispatchQueue(label: #function)
    
    func testDidReceiveAnErrorShouldCallDidReceiveAnError() {
        let mockCoordinator = MockEcommerceLoadingCoordinator()
        let spyViewController = SpyEcommerceLoadingViewController()
        let presenter = EcommerceLoadingPresenter(coordinator: mockCoordinator, dependencies: DependencyContainerMock(queue))
        let error = PicPayError(message: "Error")
        
        presenter.viewController = spyViewController
        presenter.didReceiveAnError(error)
        
        XCTAssertEqual(spyViewController.calledDidReceiveAnError, 1)
    }
    
    func testShowEcommerceAlertShouldCallDisplayEcommerceAlert() throws {
        let mockCoordinator = MockEcommerceLoadingCoordinator()
        let spyViewController = SpyEcommerceLoadingViewController()
        let presenter = EcommerceLoadingPresenter(coordinator: mockCoordinator, dependencies: DependencyContainerMock(queue))
        let json = MockJSON().load(resource: "ecommerceInfoAlert")
        let result = try EcommercePaymentInfo(json: json).safe()
        let alert = try result.alert.safe()
        
        presenter.viewController = spyViewController
        presenter.showEcommerceAlert(alert)
        
        XCTAssertEqual(spyViewController.calledDisplayEcommerceAlert, 1)
    }
    
    func testDidNextStepShouldCallPerform() {
        let mockCoordinator = MockEcommerceLoadingCoordinator()
        let spyViewController = SpyEcommerceLoadingViewController()
        let presenter = EcommerceLoadingPresenter(coordinator: mockCoordinator, dependencies: DependencyContainerMock(queue))
        
        presenter.viewController = spyViewController
        presenter.didNextStep(action: .close)
        
        XCTAssertEqual(mockCoordinator.calledPerform, 1)
    }
}
