import XCTest
@testable import PicPay

final class EcommercePaymentViewModelTest: XCTestCase {
    private let serviceMock = EcommerceServiceMock()
    private let servicePaymentMock = EcommercePaymentServiceMock()
    private let presenterSpy = EcommercePaymentPresenterSpy()
    private let paymentInfo = EcommercePaymentInfo(orderNumber: "1234", sellerName: "random name", sellerId: "1234", orderReference: "1234")
    
    private lazy var sut = EcommercePaymentViewModel(paymentInfo: paymentInfo, origin: .notification, paymentService: servicePaymentMock, service: serviceMock, presenter: presenterSpy)
    
    func testUpdateDataShouldCallPresenterWithTheRightValues() {
        sut.updateData()
        
        XCTAssertTrue(presenterSpy.didCallHandleLoadOrUpdateDataInfo)
        XCTAssertEqual(presenterSpy.screenData?.orderReference, paymentInfo.orderReference)
    }
    
    func testLoadOrderInfoShouldCallPresenterWithTheRightValues() {
        serviceMock.isGetOrderInfoSuccess = true
        
        sut.loadOrderInfo()
        
        XCTAssertTrue(presenterSpy.didCallHandleLoadOrUpdateDataInfo)
        XCTAssertEqual(presenterSpy.screenData?.orderReference, paymentInfo.orderReference)
    }
    
    func testLoadOrderInfoWithAnErrorShouldCallPresenterWithTheRightValues() {
        serviceMock.isGetOrderInfoSuccess = false
        
        sut.loadOrderInfo()
        
        XCTAssertTrue(presenterSpy.didCallHandleGetOrderInfoError)
        XCTAssertEqual(presenterSpy.error?.message, "random error")
    }
    
    func testUpdatePaymentInfoShouldCallPresenterWithTheRightValues() {
        sut.updatePaymentInfo()
        
        XCTAssertTrue(presenterSpy.didCallHandleInstallmentsInfo)
        XCTAssertEqual(presenterSpy.installmentsInfo?.selectedQuotaQuantity, 1)
    }
    
    func testCreateTransactionShouldCallPresenterWithTheRightValues() {
        servicePaymentMock.isCreateTransactionSuccess = true
        
        sut.createTransaction(pin: "aabbcc", biometry: false, privacyConfig: nil)
        
        XCTAssertTrue(presenterSpy.didCallHandleSuccessCreateTransaction)
        XCTAssertEqual(presenterSpy.transaction?.transactionId, "1234")
    }
    
    func testCreateTransactionWithAnErrorShouldCallPresenterWithTheRightValues() {
        servicePaymentMock.isCreateTransactionSuccess = false
    
        sut.auth = PPAuth()
        sut.createTransaction(pin: "aabbcc", biometry: false, privacyConfig: nil)
        
        XCTAssertTrue(presenterSpy.didCallHandleFailureCreateTransaction)
    }
    
    func testNavigateToInstallmentsShouldCallPresenterWithTheRightValues() {
        sut.navigateToInstallments()
        
        XCTAssertTrue(presenterSpy.didCallHandleNavigateToInstallments)
        XCTAssertEqual(presenterSpy.sellerId, "1234")
    }
    
    func testCloseWithDismissTrueShouldCallPresenterWithTheRightValues() {
        sut.close(shouldDismiss: true)
        
        XCTAssertTrue(presenterSpy.didCallHandleClose)
        XCTAssertEqual(presenterSpy.isADismiss, true)
    }
    
    func testCloseWithoutDismissTrueShouldCallPresenterWithTheRightValues() {
        sut.close(shouldDismiss: false)
        
        XCTAssertTrue(presenterSpy.didCallHandleClose)
        XCTAssertEqual(presenterSpy.isADismiss, false)
    }
    
    func testCreateTransaction_WhenFailure_ShouldCall() {
        servicePaymentMock.isCreateTransactionSuccess = false
        
        sut.auth = PPAuth()
        sut.createTransaction(pin: "aabbcc", biometry: false, privacyConfig: nil)
        
        XCTAssertEqual(presenterSpy.calledHandleDisableLoading, 1)
    }
}

final private class EcommercePaymentServiceMock: EcommercePaymentServicing {
    var isCreateTransactionSuccess = false
    
    func createTransaction(password: String, cvv: String?, request: EcommercePaymentRequest, _ completion: @escaping ((PicPayResult<CreateTransactionResponse>) -> Void)) {
        if isCreateTransactionSuccess {
            let transactionResponse = CreateTransactionResponse(transactionId: "1234")
            completion(PicPayResult.success(transactionResponse)); return
        }
        
        let error = PicPayError(message: "random error")
        completion(PicPayResult.failure(error))
    }
}

final private class EcommerceServiceMock: EcommerceServicing {
    var isGetOrderInfoSuccess = false
    var cardCvv: String?
    var isCvvEnabled = false
    
    var defauldCardId: String {
        return "12345"
    }
    
    var cardBank: CardBank? {
        guard !isCvvEnabled else {
            return nil
        }
        return try? MockCodable<CardBank>().loadCodableObject(resource: "EcommerceCard")
    }
    
    func saveCvvCard(id: String, value: String?) {
        cardCvv = value
    }
    
    func cvvCard(id: String) -> String? {
        return cardCvv
    }
    
    func pciCvvIsEnable(cardBank: CardBank, cardValue: Double) -> Bool {
        return isCvvEnabled
    }
    
    func getOrderInfo(_ id: String, _ completion: @escaping ((PicPayResult<EcommercePaymentInfo>) -> Void)) {
        if isGetOrderInfoSuccess {
            let paymentInfo = EcommercePaymentInfo(orderNumber: "1234", sellerName: "random name", sellerId: "1234", orderReference: "1234")
            completion(PicPayResult.success(paymentInfo)); return
        }
        
        let error = PicPayError(message: "random error")
        completion(PicPayResult.failure(error))
    }
}

final private class EcommercePaymentPresenterSpy: EcommercePaymentPresenting {
    var coordinator: EcommercePaymentCoordinating?
    weak var viewController: EcommercePaymentDisplayable?
    
    private(set) var didCallHandleSuccessCreateTransaction = false
    private(set) var transaction: (transactionId: String, totalValue: Decimal)?
    private(set) var didCallHandleFailureCreateTransaction = false
    private(set) var error: PicPayError?
    private(set) var didCallHandleLoadOrUpdateDataInfo = false
    private(set) var screenData: (subtotal: NSNumber, sellerName: String, sellerImage: String, orderReference: String)?
    private(set) var didCallHandleGetOrderInfoError = false
    private(set) var didCallHandleInstallmentsInfo = false
    private(set) var installmentsInfo: (selectedQuotaQuantity: Int, hasInstallments: Bool)?
    private(set) var didCallHandleNavigateToInstallments = false
    private(set) var sellerId: String?
    private(set) var didCallHandleClose = false
    private(set) var isADismiss: Bool?
    private(set) var calledDidReceiveAnError: Int = 0
    private(set) var calledHandleInsertCvv = false
    private(set) var calledHandleDisableLoading = 0
    
    func handleEnableLoading() { }
    
    func handleSuccessCreateTransaction(transactionId: String, totalValue: Decimal, receiptWidgets: [ReceiptWidgetItem]) {
        didCallHandleSuccessCreateTransaction.toggle()
        transaction = (transactionId, totalValue)
    }
    
    func handleFailureCreateTransaction(error: Error?) {
        didCallHandleFailureCreateTransaction.toggle()
    }
    
    func handleLoadOrUpdateDataInfo(subtotal: NSNumber, sellerName: String, sellerImage: String, orderReference: String, alert: EcommerceAlert?) {
        didCallHandleLoadOrUpdateDataInfo.toggle()
        screenData = (subtotal, sellerName, sellerImage, orderReference)
    }
    
    func handleGetOrderInfoError(_ error: PicPayError) {
        didCallHandleGetOrderInfoError.toggle()
        self.error = error
    }
    
    func handleInstallmentsInfo(_ selectedQuotaQuantity: Int, hasInstallments: Bool) {
        didCallHandleInstallmentsInfo.toggle()
        installmentsInfo = (selectedQuotaQuantity, hasInstallments)
    }
    
    func handleNavigateToInstallments(_ paymentManager: PPPaymentManager, sellerId: String, installmentsCallback: InstallmentsSelectorViewModelDelegate?) {
        didCallHandleNavigateToInstallments.toggle()
        self.sellerId = sellerId
    }
    
    func handleClose(_ isADismiss: Bool) {
        didCallHandleClose.toggle()
        self.isADismiss = isADismiss
    }
    
    func didReceive(error: PicPayError) {
        calledDidReceiveAnError = calledDidReceiveAnError + 1
    }
    
    func handleInsertCvv(completedCvv: @escaping (String) -> Void, completedWithoutCvv: @escaping () -> Void) {
        calledHandleInsertCvv = true
    }
    
    func handleDisableLoading() {
        calledHandleDisableLoading += 1
    }
}
