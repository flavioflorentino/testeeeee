import XCTest
@testable import PicPay

final class EcommercePaymentCoordinatorTest: XCTestCase {
    private let navController = NavigationControllerMock(rootViewController: UIViewController())
    private lazy var sut = EcommercePaymentCoordinator()
    
    func testPerformCloseValidationShouldDismissController() {
        sut.viewController = navController.topViewController
        
        sut.perform(action: .dismiss)
        
        XCTAssertTrue(self.navController.isDismissViewControllerCalled)
    }
}
