import XCTest
@testable import PicPay

final class EcommercePaymentPresenterTest: XCTestCase {
    private let coordinatorSpy = EcommercePaymentCoordinatorSpy()
    private let controllerSpy = EcommercePaymentControllerSpy()
    private let queue = DispatchQueue(label: #function)
    
    private lazy var sut: EcommercePaymentPresenter = {
        let sut = EcommercePaymentPresenter(coordinator: coordinatorSpy, queue: queue)
        sut.viewController = controllerSpy
        return sut
    }()
    
    func testHandleSuccessCreateTransactionShouldCallCoordinator() {
        sut.handleSuccessCreateTransaction(transactionId: "1234", totalValue: 200, receiptWidgets: [])
        
        let expectation = self.expectation(description: "handleSuccessCreateTransaction")
        
        queue.async {
            XCTAssertTrue(self.coordinatorSpy.didCallPerform)
            XCTAssertEqual(self.coordinatorSpy.receipt?.transactionId, "1234")
            XCTAssertEqual(self.coordinatorSpy.receipt?.widgets.count, 0)
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5)
    }
    
    func testHandleFailureCreateTransactionShouldCallViewController() {
        sut.handleFailureCreateTransaction(error: PicPayError(message: "a generic error"))
        
        let expectation = self.expectation(description: "handleFailureCreateTransaction")
        
        queue.async {
            XCTAssertTrue(self.controllerSpy.didCallPresentCreateTransactionError)
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5)
    }
    
    func testHandleLoadOrUpdateDataInfoShouldCallViewController() {
        sut.handleLoadOrUpdateDataInfo(
            subtotal: 100,
            sellerName: "a generic name",
            sellerImage: "a generic image",
            orderReference: "10000",
            alert: nil
        )
        
        let expectation = self.expectation(description: "handleLoadOrUpdateDataInfo")
              
        queue.async {
            XCTAssertTrue(self.controllerSpy.didUpdateScreenData)
            XCTAssertEqual(self.controllerSpy.screenData?.subtotal, "100,00")
            XCTAssertEqual(self.controllerSpy.screenData?.sellerName, "a generic name")
            XCTAssertEqual(self.controllerSpy.screenData?.orderNumber, "Pedido: 10000")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5)
    }
    
    func testHandleGetOrderInfoErrorShouldCallViewController() {
        sut.handleGetOrderInfoError(PicPayError(message: "a generic error"))
        
        let expectation = self.expectation(description: "handleGetOrderInfoError")
              
        queue.async {
            XCTAssertTrue(self.controllerSpy.didPresentOrderInfoError)
            XCTAssertEqual(self.controllerSpy.error?.message, "a generic error")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5)
    }
    
    func testHandleInstallmentsInfoShouldCallViewController() {
        sut.handleInstallmentsInfo(1, hasInstallments: true)
        
        XCTAssertTrue(controllerSpy.didUpdateInstallmentsInfo)
        XCTAssertEqual(controllerSpy.installmentsInfo?.selectedQuotaQuantity, "Parcelamento 1x")
        XCTAssertEqual(controllerSpy.installmentsInfo?.hasInstallments, true)
    }
    
    func testHandleNavigateToInstallmentsShouldCallCoordinator() {
        sut.handleNavigateToInstallments(PPPaymentManager(), sellerId: "1234", installmentsCallback: nil)
        
        XCTAssertTrue(coordinatorSpy.didCallPerform)
        XCTAssertEqual(coordinatorSpy.installmentsInfo?.sellerId, "1234")
    }
    
    func testHandleCloseWhenDismissIsTrueShouldCallCoordinator() {
        sut.handleClose(true)
        
        XCTAssertTrue(coordinatorSpy.didCallPerform)
        XCTAssertTrue(coordinatorSpy.isADismiss)
    }
    
    func testHandleCloseWhenDismissIsFalseShouldCallCoordinator() {
        sut.handleClose(false)
        
        XCTAssertTrue(coordinatorSpy.didCallPerform)
        XCTAssertFalse(coordinatorSpy.isADismiss)
    }
    
    func testHandleInsertCvvShouldCallCoordinatorOpenInsertCvv() {
        sut.handleInsertCvv(completedCvv: {_ in }, completedWithoutCvv: {})
        
        XCTAssertTrue(coordinatorSpy.didCallPerform)
        XCTAssertTrue(coordinatorSpy.openedInsertCVV)
    }
    
    func testHandleDisableLoading_ShouldCall() {
        sut.handleDisableLoading()
        
        let expectation = self.expectation(description: "didCallDisableLoading")
        queue.async {
            XCTAssertEqual(self.controllerSpy.didCallDisableLoading, 1)
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5)
    }
    
    func testDidReceiveAnErrorShouldCallControllerOpenDidReceiveAnErrorv() {
        let error = PicPayError(message: "Test")
        sut.didReceive(error: error)
        
        let expectation = self.expectation(description: "didReceiveAnError")
        queue.async {
            XCTAssertTrue(self.controllerSpy.calledDidReceiveAnError)
            XCTAssertNotNil(self.controllerSpy.error)
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5)
    }
}

final private class EcommercePaymentCoordinatorSpy: EcommercePaymentCoordinating {
    var viewController: UIViewController? = nil
    var navigationController: UINavigationController = UINavigationController()
    
    private(set) var didCallPerform = false
    private(set) var receipt: (transactionId: String, widgets: [ReceiptWidgetItem])?
    private(set) var installmentsInfo: (paymentManager: PPPaymentManager, sellerId: String, installmentsCallback: InstallmentsSelectorViewModelDelegate?)?
    private(set) var isADismiss = false
    private(set) var openedInsertCVV = false
    
    func perform(action: EcommercePaymentAction) {
        didCallPerform.toggle()
        
        switch action {
        case let .showReceipt(transactionId, widgets):
            receipt = (transactionId, widgets)
        case let .installments(paymentManager, sellerId, installmentsCallback):
            installmentsInfo = (paymentManager, sellerId, installmentsCallback)
        case .openInsertCvv:
            openedInsertCVV = true
        case .close:
            isADismiss = false
        case .dismiss:
            isADismiss = true
        }
    }
}

final private class EcommercePaymentControllerSpy: EcommercePaymentDisplayable {
    private(set) var didCallPresentCreateTransactionError = false
    private(set) var error: PicPayError?
    private(set) var didUpdateScreenData = false
    private(set) var screenData: (subtotal: String, sellerName: String, sellerImageURL: URL?, orderNumber: String, alert: EcommerceAlert?)?
    private(set) var didPresentOrderInfoError = false
    private(set) var didUpdateInstallmentsInfo = false
    private(set) var installmentsInfo: (selectedQuotaQuantity: String, hasInstallments: Bool)?
    private(set) var calledDidReceiveAnError = false
    private(set) var didCallDisableLoading = 0
    
    func enableLoading() { }

    func presentCreateTransactionError(_ error: Error?) {
        didCallPresentCreateTransactionError.toggle()
    }
    
    func updateScreenData(subtotal: String, sellerName: String, sellerImageURL: URL?, orderNumber: String, alert: EcommerceAlert?) {
        didUpdateScreenData.toggle()
        screenData = (subtotal, sellerName, sellerImageURL, orderNumber, alert)
    }
    
    func presentOrderInfoError(_ error: PicPayError) {
        didPresentOrderInfoError.toggle()
        self.error = error
    }
    
    func updateInstallmentsInfo(_ selectedQuotaQuantity: String, hasInstallments: Bool) {
        didUpdateInstallmentsInfo.toggle()
        installmentsInfo = (selectedQuotaQuantity, hasInstallments)
    }
    
    func didReceive(error: PicPayError) {
        calledDidReceiveAnError = true
        self.error = error
    }
    
    func disableLoading() {
        didCallDisableLoading += 1
    }
}
