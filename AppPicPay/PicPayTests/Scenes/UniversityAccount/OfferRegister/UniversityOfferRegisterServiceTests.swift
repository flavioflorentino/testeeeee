@testable import PicPay
import Core
import XCTest

class UniversityOfferRegisterServiceTests: XCTestCase {
    let kvStoreMock = KVStoreMock()
    
    private lazy var sut: UniversityOfferRegisterService = {
        UniversityOfferRegisterService(dependencies: DependencyContainerMock(kvStoreMock))
    }()
    
    func testRemoveConsumerStudentValidatedApiId_WhenCalled_ShouldRemoveValidatedApiIdFromKVStore() {
        let kvKey = UniversityAccountKey.consumerStudentValidatedApiId
        kvStoreMock.setString("1", with: kvKey)
        
        sut.removeConsumerStudentValidatedApiId()
        
        XCTAssertNil(kvStoreMock.stringFor(kvKey))
    }
}
