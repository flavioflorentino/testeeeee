@testable import PicPay
import XCTest

final class UniversityOfferRegisterCoordinatorTests: XCTestCase {
    private let viewControllerMock = NavigationControllerMock(rootViewController: UIViewController())
    
    private lazy var sut: UniversityOfferRegisterCoordinator = {
        let coordinator = UniversityOfferRegisterCoordinator()
        coordinator.viewController = viewControllerMock
        return coordinator
    }()
    
    func testPerform_WhenActionIsRegisterStudentAccount_ShouldStartStudentAccount() throws {
        sut.perform(action: .registerStudentAccount(withValidatedApiId: "1"))

        let currentNavigation = try XCTUnwrap(viewControllerMock.viewControllerPresented) as? UINavigationController
        XCTAssertTrue(viewControllerMock.isPresentViewControllerCalled)
        XCTAssertTrue(currentNavigation?.topViewController is StudentStatusViewController)
    }
    
    func testPerform_WhenActionIsDismiss_Should() throws {
        sut.perform(action: .dismiss)
        
        XCTAssertTrue(viewControllerMock.isDismissViewControllerCalled)
    }
}
