@testable import PicPay
import XCTest

private final class UniversityOfferRegisterCoordinatingSpy: UniversityOfferRegisterCoordinating {
    private(set) var performActionCount = 0
    private(set) var dismissActionCount = 0
    private(set) var registerStudentAccountActionCount = 0
    
    private(set) var validatedApiIdStored: String?
    
    weak var viewController: UIViewController?

    func perform(action: UniversityOfferRegisterAction) {
        performActionCount += 1
        
        switch action {
        case let .registerStudentAccount(withValidatedApiId: apiId):
            registerStudentAccountActionCount += 1
            validatedApiIdStored = apiId
        case .dismiss:
            dismissActionCount += 1
        }
    }
}

final class UniversityOfferRegisterPresenterTests: XCTestCase {
    private let coordinatorSpy = UniversityOfferRegisterCoordinatingSpy()
    private lazy var sut: UniversityOfferRegisterPresenter = {
        let presenter = UniversityOfferRegisterPresenter(coordinator: coordinatorSpy)
        return presenter
    }()
    
    func testDidNextStep_WhenActionIsRegisterStudentAccount_ShouldMoveToNextStepWithValidatedApiId() {
        let expectedApiId = "1"
        
        sut.didNextStep(action: .registerStudentAccount(withValidatedApiId: expectedApiId))
        
        XCTAssertEqual(coordinatorSpy.performActionCount, 1)
        XCTAssertEqual(coordinatorSpy.registerStudentAccountActionCount, 1)
        XCTAssertEqual(coordinatorSpy.validatedApiIdStored, expectedApiId)
    }
    
    func testDidNextStep_WhenActionIsDissmiss_ShouldDismiss() {
        sut.didNextStep(action: .dismiss)
        
        XCTAssertEqual(coordinatorSpy.performActionCount, 1)
        XCTAssertEqual(coordinatorSpy.dismissActionCount, 1)
    }
}
