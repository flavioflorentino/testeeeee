@testable import PicPay
import XCTest
import AnalyticsModule

private final class UniversityOfferRegisterServicingSpy: UniversityOfferRegisterServicing {
    private(set) var removeConsumerStudentValidatedApiIdCount = 0

    func removeConsumerStudentValidatedApiId() {
        removeConsumerStudentValidatedApiIdCount += 1
    }
}

private final class UniversityOfferRegisterPresentingSpy: UniversityOfferRegisterPresenting {
    private(set) var didNextStepActionCount = 0
    private(set) var didRegisterStudentAccountAction = 0
    private(set) var didDismissAction = 0
    
    private(set) var validatedApiIdStored: String?

    func didNextStep(action: UniversityOfferRegisterAction) {
        didNextStepActionCount += 1
        
        switch action {
        case let .registerStudentAccount(withValidatedApiId: apiId):
            didRegisterStudentAccountAction += 1
            validatedApiIdStored = apiId
        case .dismiss:
            didDismissAction += 1
        }
    }
}

final class UniversityOfferRegisterViewModelTests: XCTestCase {
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var serviceSpy = UniversityOfferRegisterServicingSpy()
    private lazy var presenterSpy = UniversityOfferRegisterPresentingSpy()
    
    private lazy var sut: UniversityOfferRegisterViewModel = {
        let viewModel = UniversityOfferRegisterViewModel(
            service: serviceSpy,
            presenter: presenterSpy,
            dependencies: DependencyContainerMock(analyticsSpy),
            validApiId: "1"
        )
        return viewModel
    }()
    
    func testViewDidLoad_WhenCalled_ShouldRemoveConsumerStudentValidatedApiIdAndTrackSignUpEvent() {
        sut.startFlow()
        
        XCTAssertEqual(serviceSpy.removeConsumerStudentValidatedApiIdCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: StudentAccountEvent.signUp.event()))
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
    }
    
    func testDidTapConfirm_WhenCalled_ShouldPresentNextStepWithRegisterStudentAccountAndTrackSignUpConfirmEvent() {
        sut.confirm()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCount, 1)
        XCTAssertEqual(presenterSpy.didRegisterStudentAccountAction, 1)
        XCTAssertEqual(presenterSpy.validatedApiIdStored, "1")
    }
    
    func testDidTapDismiss_WhenCalled_ShouldPresentDismissAndTrackSignUpNotNowEvent() {
        sut.dismiss()
        
        XCTAssertEqual(presenterSpy.didNextStepActionCount, 1)
        XCTAssertEqual(presenterSpy.didDismissAction, 1)
    }
}
