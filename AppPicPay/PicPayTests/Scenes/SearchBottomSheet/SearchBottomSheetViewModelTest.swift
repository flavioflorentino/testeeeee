import AnalyticsModule
import Core
import FeatureFlag
import XCTest

@testable import PicPay

private final class FavoriteInteractorMock: FavoriteInteracting {
    enum FavoriteInteractorStatus {
        case success
    }
    
    var status: FavoriteInteractorStatus
    
    private(set) var checkStateCalled = 0
    private(set) var changeStateCalled = 0
    
    init(status: FavoriteInteractorStatus = .success) {
        self.status = status
    }
    
    func checkState(id: String, type: Favorite.`Type`, _ completion: @escaping (FavoriteState) -> Void) {
        checkStateCalled += 1
        switch status {
        case .success:
            completion(.favorited)
        }
    }
    
    func changeState(_ state: FavoriteState, id: String, type: Favorite.`Type`, _ completion: @escaping (FavoriteState) -> Void) {
        changeStateCalled += 1
        completion(.favorited)
    }
}

private final class SearchBottomSheetServiceMock: SearchBottomSheetServicing {
    enum SearchBottomSheetServiceStatus {
        case success
        case failure
    }
    
    var status: SearchBottomSheetServiceStatus
    
    private(set) var loadPayableOptionsCalled = 0
    
    init(status: SearchBottomSheetServiceStatus = .success) {
        self.status = status
    }
    
    func loadPayableOptions(
        forType type: SearchBottomSheetType,
        andID id: String,
        completion: @escaping (_ response: Result<[SearchBottomSheetItem], ApiError>) -> Void
    ) {
        loadPayableOptionsCalled += 1
        
        switch status {
        case .success:
            let bottomSheetItem = SearchBottomSheetItem(
                type: .activable,
                title: "",
                action: SearchBottomSheetItemAction(
                    type: .favorite,
                    data: ""
                )
            )
            let anotherBottomSheetItem = SearchBottomSheetItem(
                type: .activable,
                title: "",
                action: SearchBottomSheetItemAction(
                    type: .following,
                    data: "waiting"
                )
            )
            completion(.success([bottomSheetItem, anotherBottomSheetItem]))
        case .failure:
            completion(.failure(.connectionFailure))
        }
    }
}

private final class SearchBottomSheetPresenterSpy: SearchBottomSheetPresenting {
    var viewController: SearchBottomSheetDisplay? = nil
    
    private(set) var listPayableOptionsCalled = 0
    private(set) var updateInfoCalled = 0
    private(set) var performActionCalled = 0
    private(set) var presentDeeplinkCalled = 0
    private(set) var options: [SearchBottomSheetItem]?
    
    func updateInfo(withCellObject object: NSObject, andName name: String) {
        updateInfoCalled += 1
    }
    
    func listPayableOptions(options: [SearchBottomSheetItem], isDefault: Bool, cellType: SearchResultType, cellId: String) {
        listPayableOptionsCalled += 1
        self.options = options
    }
    
    func perform(action: SearchBottomSheetAction) {
        performActionCalled += 1
    }
    
    func present(deepLinkAction deeplink: String) {
        presentDeeplinkCalled += 1
    }
}

final private class DummyObject: NSObject { }

final class SearchBottomSheetViewModelTest: XCTestCase {
    private let serviceMock = SearchBottomSheetServiceMock()
    private let presenterSpy = SearchBottomSheetPresenterSpy()
    private let dummySearchResultBasics = SearchResultBasics()
    private var sut: SearchBottomSheetViewModel!
    
    func testLoadPayableOptions_WhenSelectingCell_ShouldCallFunc() {
        setupTest(.success)
        
        sut.loadPayableOptions()
        
        XCTAssertEqual(serviceMock.loadPayableOptionsCalled, 1)
    }
    
    func testLoadPayableOptions_WhenSelectedCellDoesNotHaveId_ShouldCallNotListedFunc() throws {
        setupTest(.noId)
        
        let expectation = self.expectation(description: "request")
        
        sut.loadPayableOptions()
        
        self.expectation(waitForCondition: self.presenterSpy.listPayableOptionsCalled == 1) {
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertEqual(presenterSpy.listPayableOptionsCalled, 1)
        
        let options = try XCTUnwrap(presenterSpy.options)
        XCTAssertEqual(options.count, 1)
        
        let optionTitle = try XCTUnwrap(options.first?.title)
        XCTAssertEqual(optionTitle, "")
    }
    
    func testLoadPayableOptions_WhenSelectedCellDoesNotHaveType_ShouldCallNotListedFunc() throws {
        setupTest(.noType)
        
        let expectation = self.expectation(description: "request")
        
        sut.loadPayableOptions()
        
        self.expectation(waitForCondition: self.presenterSpy.listPayableOptionsCalled == 1) {
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertEqual(presenterSpy.listPayableOptionsCalled, 1)
        
        let options = try XCTUnwrap(presenterSpy.options)
        XCTAssertEqual(options.count, 1)
        
        let optionTitle = try XCTUnwrap(options.first?.title)
        XCTAssertEqual(optionTitle, "")
    }
    
    func testLoadPayableOptions_WhenSelectedCellIsQRCodeType_ShouldCallNotListedFuncForQRCode() throws {
        setupTest(.qrCode)
        
        let expectation = self.expectation(description: "request")
        
        sut.loadPayableOptions()
        
        self.expectation(waitForCondition: self.presenterSpy.listPayableOptionsCalled == 1) {
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertEqual(presenterSpy.listPayableOptionsCalled, 1)
        
        let options = try XCTUnwrap(presenterSpy.options)
        XCTAssertEqual(options.count, 1)
        
        let optionTitle = try XCTUnwrap(options.first?.title)
        XCTAssertEqual(optionTitle, "")
    }
    
    func testCheckIfLastCellIsOnDisplay_WhenCellsAreBeingPresented_ShouldReturnTrueIfItIsLastCell() throws {
        setupTest(.success)
        
        let expectation = self.expectation(description: "request")
        
        sut.loadPayableOptions()
        
        self.expectation(waitForCondition: self.presenterSpy.listPayableOptionsCalled == 1) {
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertEqual(presenterSpy.listPayableOptionsCalled, 1)
        
        let options = try XCTUnwrap(presenterSpy.options)
        XCTAssertEqual(options.count, 2)
        
        let indexPath = IndexPath(row: 1, section: 0)
        let isLastCell = sut.checkIfLastCellIsOnDisplay(indexPath: indexPath)
        XCTAssertTrue(isLastCell)
    }
    
    func testCheckIfLastCellIsOnDisplay_WhenCellsAreBeingPresented_ShouldReturnFalseIfItIsNotTheLastCell() throws {
        setupTest(.success)
        
        let expectation = self.expectation(description: "request")
        
        sut.loadPayableOptions()
        
        self.expectation(waitForCondition: self.presenterSpy.listPayableOptionsCalled == 1) {
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertEqual(presenterSpy.listPayableOptionsCalled, 1)
        
        let options = try XCTUnwrap(presenterSpy.options)
        XCTAssertEqual(options.count, 2)
        
        let indexPath = IndexPath(row: 0, section: 0)
        let isLastCell = sut.checkIfLastCellIsOnDisplay(indexPath: indexPath)
        XCTAssertFalse(isLastCell)
    }
}

extension SearchBottomSheetViewModelTest {
    private enum BSTestStatus {
        case success
        case failure
        case noId
        case noType
        case qrCode
    }
    
    private func setupTest(_ status: BSTestStatus) {
        let interactor = FavoriteInteractorMock(status: .success)

        sut = SearchBottomSheetViewModel(
            service: serviceMock,
            presenter: presenterSpy,
            data: dummySearchResultBasics,
            object: DummyObject(),
            interactor: interactor,
            dependencies: DependencyContainerMock(AnalyticsSpy(), FeatureManagerMock()),
            tabId: ""
        )
        
        switch status {
        case .success:
            dummySearchResultBasics.type = .person
            dummySearchResultBasics.id = "4654"
            serviceMock.status = .success
        case .failure:
            dummySearchResultBasics.type = .person
            dummySearchResultBasics.id = "6650"
            serviceMock.status = .failure
        case .noId:
            dummySearchResultBasics.type = .person
            dummySearchResultBasics.title = ""
        case .noType:
            dummySearchResultBasics.id = "123123"
            dummySearchResultBasics.title = ""
        case .qrCode:
            dummySearchResultBasics.id = "123123"
            dummySearchResultBasics.title = "Pagar nas Maquininhas"
        }
    }
}
