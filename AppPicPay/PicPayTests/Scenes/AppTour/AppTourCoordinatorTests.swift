import XCTest
@testable import PicPay

final private class AppTourCoordinatorDelegateSpy: AppTourCoordinatorDelegate {
    private(set) var callDidFinishTutorialCout = 0
    
    func didFinishTutorial() {
        callDidFinishTutorialCout += 1
    }
}

final class AppTourCoordinatorTests: XCTestCase {
    private let viewControllerSpy = ViewControllerMock()
    private let coordinatorDelegate = AppTourCoordinatorDelegateSpy()
    private lazy var sut: AppTourCoordinator = {
        let coordinator = AppTourCoordinator()
        coordinator.viewController = viewControllerSpy
        coordinator.delegate = coordinatorDelegate
        return coordinator
    }()
    
    func testPerform_WhenActionWasExit_ShouldDismissTutorial() {
        sut.perform(action: .exit)
        
        XCTAssertEqual(viewControllerSpy.didDismissControllerCount, 1)
        XCTAssertEqual(coordinatorDelegate.callDidFinishTutorialCout, 1)
    }
}
