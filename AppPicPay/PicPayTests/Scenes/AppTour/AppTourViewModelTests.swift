import AnalyticsModule
import FeatureFlag
import XCTest

@testable import PicPay

final class AppTourViewModelTests: XCTestCase {
    fileprivate let featureManager = FeatureManagerMock()
    private let analyticsSpy = AnalyticsSpy()
    lazy var mockDependencies = DependencyContainerMock(analyticsSpy)
    private lazy var presenterSpy = AppTourPresenterSpy()
    
    private lazy var sut: AppTourViewModel = {
        let viewModel = AppTourViewModel(dependencies: mockDependencies,presenter: presenterSpy)
        return viewModel
    }()
    
    private func assertEventsEqual(_ eventA: AnalyticsEventProtocol, _ eventB: AnalyticsEventProtocol) throws {
        XCTAssertEqual(eventA.name, eventB.name)
        XCTAssertEqual(eventA.providers, eventB.providers)
        
        let propertiesA = try XCTUnwrap(eventA.properties as? [String: String])
        let propertiesB = try XCTUnwrap(eventB.properties as? [String: String])
        XCTAssertEqual(propertiesA, propertiesB)
    }
    
    func testPresentTutorialHelp_ShouldPresentTutorialHelpView() {
        sut.presentTutorialHelp()
        
        XCTAssertEqual(presenterSpy.callPresentTutorialHelpViewCount, 1)
    }
    
    // didTapOverScreen
    
    func testDidTapOverScreen_WhenPresentTutorialHelpWasTheFirstView_ShouldPresentTutorialProgressViewHideCloseButtonTextAndShowFirstStep() throws {
        sut.didTapOverScreen()
        
        XCTAssertEqual(presenterSpy.callPresentTutorialProgressViewCount, 1)
        XCTAssertEqual(presenterSpy.callHideCloseButtonTextCount, 1)
        XCTAssertEqual(presenterSpy.callDisplayStepCount, 1)
        XCTAssertEqual(presenterSpy.callRemovePreviousStepCount, 1)
        XCTAssertEqual(presenterSpy.stepPassed, .pay)
        XCTAssertEqual(presenterSpy.progressePassed, 0)
        XCTAssertEqual(analyticsSpy.logCalledCount, 2)
        
        let sentEvent = try XCTUnwrap(analyticsSpy.event)
        let expectedEvent = AppTourEvent.tapped(.overScreen, forStep: .intro).event()
        try assertEventsEqual(sentEvent, expectedEvent)
    }
    
    func testDidTapOverScreen_WhenTheCurrentViewWasPayView_ShouldPresentQRCodeView() throws {
        sut.didTapOverScreen()
        sut.didTapOverScreen()
        
        XCTAssertEqual(presenterSpy.callDisplayStepCount, 2)
        XCTAssertEqual(presenterSpy.callRemovePreviousStepCount, 2)
        XCTAssertEqual(presenterSpy.stepPassed, .qrCode)
        XCTAssertEqual(presenterSpy.progressePassed, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 3)
        
        let sentEvent = try XCTUnwrap(analyticsSpy.event)
        let expectedEvent = AppTourEvent.tapped(.overScreen, forStep: .payButton).event()
        try assertEventsEqual(sentEvent, expectedEvent)
    }
    
    func testDidTapOverScreen_WhenTheCurrentViewWasQRCodeView_ShouldPresentWalletView() throws {
        sut.didTapOverScreen()
        sut.didTapOverScreen()
        sut.didTapOverScreen()
        
        XCTAssertEqual(presenterSpy.callDisplayStepCount, 3)
        XCTAssertEqual(presenterSpy.callRemovePreviousStepCount, 3)
        XCTAssertEqual(presenterSpy.stepPassed, .wallet)
        XCTAssertEqual(presenterSpy.progressePassed, 2)
        XCTAssertEqual(analyticsSpy.logCalledCount, 4)
        
        let sentEvent = try XCTUnwrap(analyticsSpy.event)
        let expectedEvent = AppTourEvent.tapped(.overScreen, forStep: .qrCodeButton).event()
        try assertEventsEqual(sentEvent, expectedEvent)
    }
    
    func testDidTapOverScreen_WhenTheCurrentViewWasWalletView_ShouldDismissTutorial() throws {
        sut.didTapOverScreen()
        sut.didTapOverScreen()
        sut.didTapOverScreen()
        sut.didTapOverScreen()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.actionpPassed, .exit)
        XCTAssertEqual(analyticsSpy.logCalledCount, 5)
        
        let sentEvent = try XCTUnwrap(analyticsSpy.event)
        let expectedEvent = AppTourEvent.tapped(.overScreen, forStep: .walletButton).event()
        try assertEventsEqual(sentEvent, expectedEvent)
    }
    
    // didTapHighlightedButton
    
    func testDidTapHighlightedButton_WhenTheCurrentViewWasPayView_ShouldPresentQRCodeView() throws {
        sut.didTapOverScreen()
        sut.didTapHighlightedButton()
        
        XCTAssertEqual(presenterSpy.callDisplayStepCount, 2)
        XCTAssertEqual(presenterSpy.callRemovePreviousStepCount, 2)
        XCTAssertEqual(presenterSpy.stepPassed, .qrCode)
        XCTAssertEqual(presenterSpy.progressePassed, 1)
        XCTAssertEqual(analyticsSpy.logCalledCount, 3)
        
        let sentEvent = try XCTUnwrap(analyticsSpy.event)
        let expectedEvent = AppTourEvent.tapped(.payButton, forStep: .payButton).event()
        try assertEventsEqual(sentEvent, expectedEvent)
    }
    
    func testDidTapHighlightedButton_WhenTheCurrentViewWasQRCodeView_ShouldPresentWalletView() throws {
        sut.didTapOverScreen()
        sut.didTapOverScreen()
        sut.didTapHighlightedButton()
        
        XCTAssertEqual(presenterSpy.callDisplayStepCount, 3)
        XCTAssertEqual(presenterSpy.callRemovePreviousStepCount, 3)
        XCTAssertEqual(presenterSpy.stepPassed, .wallet)
        XCTAssertEqual(presenterSpy.progressePassed, 2)
        XCTAssertEqual(analyticsSpy.logCalledCount, 4)
        
        let sentEvent = try XCTUnwrap(analyticsSpy.event)
        let expectedEvent = AppTourEvent.tapped(.qrCodeButton, forStep: .qrCodeButton).event()
        try assertEventsEqual(sentEvent, expectedEvent)
    }
    
    func testDidTapHighlightedButton_WhenTheCurrentViewWasWalletView_ShouldDismissTutorial() throws {
        sut.didTapOverScreen()
        sut.didTapOverScreen()
        sut.didTapOverScreen()
        sut.didTapHighlightedButton()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.actionpPassed, .exit)
        XCTAssertEqual(analyticsSpy.logCalledCount, 5)
        
        let sentEvent = try XCTUnwrap(analyticsSpy.event)
        let expectedEvent = AppTourEvent.tapped(.walletButton, forStep: .walletButton).event()
        try assertEventsEqual(sentEvent, expectedEvent)
    }
    
    // didTapClose
    
    func testDidTapClose_WhenPresentTutorialHelpWasTheFirstView_ShouldDismissTutorial() throws {
        sut.didTapClose()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.actionpPassed, .exit)
        XCTAssertEqual(analyticsSpy.logCalledCount, 2)
        
        let sentEvent = try XCTUnwrap(analyticsSpy.event)
        let expectedEvent = AppTourEvent.tapped(.closeButton, forStep: .intro).event()
        try assertEventsEqual(sentEvent, expectedEvent)
    }
    
    func testDidTapClose_WhenTheCurrentViewWasPayView_ShouldDismissTutorial() throws {
        sut.didTapOverScreen()
        sut.didTapClose()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.actionpPassed, .exit)
        XCTAssertEqual(analyticsSpy.logCalledCount, 3)
        
        let sentEvent = try XCTUnwrap(analyticsSpy.event)
        let expectedEvent = AppTourEvent.tapped(.closeButton, forStep: .payButton).event()
        try assertEventsEqual(sentEvent, expectedEvent)
    }
    
    func testDidTapClose_WhenTheCurrentViewWasQRCodeView_ShouldDismissTutorial() throws {
        sut.didTapOverScreen()
        sut.didTapOverScreen()
        sut.didTapClose()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.actionpPassed, .exit)
        XCTAssertEqual(analyticsSpy.logCalledCount, 4)
        
        let sentEvent = try XCTUnwrap(analyticsSpy.event)
        let expectedEvent = AppTourEvent.tapped(.closeButton, forStep: .qrCodeButton).event()
        try assertEventsEqual(sentEvent, expectedEvent)
    }
    
    func testDidTapClose_WhenTheCurrentViewWasWalletView_ShouldDismissTutorial() throws {
        sut.didTapOverScreen()
        sut.didTapOverScreen()
        sut.didTapOverScreen()
        sut.didTapClose()
        
        XCTAssertEqual(presenterSpy.callDidNextStepCount, 1)
        XCTAssertEqual(presenterSpy.actionpPassed, .exit)
        XCTAssertEqual(analyticsSpy.logCalledCount, 5)
        
        let sentEvent = try XCTUnwrap(analyticsSpy.event)
        let expectedEvent = AppTourEvent.tapped(.closeButton, forStep: .walletButton).event()
        try assertEventsEqual(sentEvent, expectedEvent)
    }
}

private final class AppTourPresenterSpy: AppTourPresenting {
    var viewController: AppTourDisplay?
    private(set) var callPresentTutorialHelpViewCount = 0
    private(set) var callPresentTutorialProgressViewCount = 0
    private(set) var callHideCloseButtonTextCount = 0
    private(set) var callDisplayStepCount = 0
    private(set) var callRemovePreviousStepCount = 0
    private(set) var stepPassed: AppTourStep?
    private(set) var progressePassed: Int?
    private(set) var callDidNextStepCount = 0
    private(set) var actionpPassed: AppTourAction?
    
    func didNextStep(action: AppTourAction) {
        callDidNextStepCount += 1
        actionpPassed = action
    }
    
    func presentTutorialHelpView() {
        callPresentTutorialHelpViewCount += 1
    }
    
    func removePreviousStep() {
        callRemovePreviousStepCount += 1
    }
    
    func presentTutorialProgressView(steps: Int) {
        callPresentTutorialProgressViewCount += 1
    }
    
    func hideCloseButtonText() {
        callHideCloseButtonTextCount += 1
    }
    
    func presentStep(_ step: AppTourStep, andProgress: Int) {
        callDisplayStepCount += 1
        stepPassed = step
        progressePassed = andProgress
    }
}
