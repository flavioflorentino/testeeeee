import XCTest
import AnalyticsModule
@testable import PicPay

final class AppTourPresenterTests: XCTestCase {
//    typealias Dependencies = HasAnalytics
    private lazy var coordinatorSpy = AppTourCoordinatorSpy()
    private lazy var viewControllerSpy = AppTourViewControllerSpy()
    
    private lazy var sut: AppTourPresenter = {
        let dependencies = DependencyContainerMock(AnalyticsSpy())
        let presenter = AppTourPresenter(coordinator: coordinatorSpy, dependencies: dependencies)
        presenter.viewController = viewControllerSpy
        return presenter
    }()
    
    func testPresentTutorialHelpView_ShoulDisplayTutorialHelpView() throws {
        sut.presentTutorialHelpView()
        
        XCTAssertEqual(viewControllerSpy.callDisplayTutorialHelpCount, 1)
    }
    
    func testPresentStep_WhenStepWasPayAndProgressWaZero_ShouldDisplayPayViewAndSetProgressToZero() {
        sut.presentStep(.pay, andProgress: 0)
        
        XCTAssertEqual(viewControllerSpy.callDisplayStepCount, 1)
        XCTAssertEqual(viewControllerSpy.displayStepPassed, .pay)
        XCTAssertEqual(viewControllerSpy.callSetProgressViewStepPassedCount, 1)
        XCTAssertEqual(viewControllerSpy.setProgressViewStepPassed, 0)
    }
    
    func testPresentStep_WhenStepWasQRCodeProgressWaOne_ShouldDisplayQRCodeViewAndSetProgressToOne() {
        sut.presentStep(.qrCode, andProgress: 1)
        
        XCTAssertEqual(viewControllerSpy.callDisplayStepCount, 1)
        XCTAssertEqual(viewControllerSpy.displayStepPassed, .qrCode)
        XCTAssertEqual(viewControllerSpy.callSetProgressViewStepPassedCount, 1)
        XCTAssertEqual(viewControllerSpy.setProgressViewStepPassed, 1)
    }
    
    func testPresentStep_WhenStepWasWalletProgressWaTwo_ShouldDisplayWalletViewAndSetProgressToTwo() {
        sut.presentStep(.wallet, andProgress: 2)
        
        XCTAssertEqual(viewControllerSpy.callDisplayStepCount, 1)
        XCTAssertEqual(viewControllerSpy.displayStepPassed, .wallet)
        XCTAssertEqual(viewControllerSpy.callSetProgressViewStepPassedCount, 1)
        XCTAssertEqual(viewControllerSpy.setProgressViewStepPassed, 2)
    }
    
    func testPresentTutorialProgressView_ShoulDisplayTutorialProgressView() {
        sut.presentTutorialProgressView(steps: 3)
        
        XCTAssertEqual(viewControllerSpy.callDisplayTutorialProgressCount, 1)
        XCTAssertEqual(viewControllerSpy.displayTutorialProgressPassed, 3)
    }
    
    func testHideCloseButtonText_ShouldHideCloseButtonText() {
        sut.hideCloseButtonText()
        
        XCTAssertEqual(viewControllerSpy.callHideCloseButtonTextCount, 1)
    }
    
    func testRemovePreviousStep_ShouldRemovePreviousStep() {
        sut.removePreviousStep()
        
        XCTAssertEqual(viewControllerSpy.callRemoveStepCount, 1)
    }
    
    func testDidNextStep_WhenActionWasExit_ShouldDismissTutorial() {
        sut.didNextStep(action: .exit)
        
        XCTAssertEqual(coordinatorSpy.callExitCount, 1)
    }
}

private final class AppTourViewControllerSpy: AppTourDisplay {
    private(set) var callDisplayTutorialHelpCount = 0
    private(set) var callDisplayStepCount = 0
    private(set) var displayStepPassed: AppTourStep?
    private(set) var callRemoveStepCount = 0
    private(set) var callDisplayTutorialProgressCount = 0
    private(set) var displayTutorialProgressPassed: Int?
    private(set) var callHideCloseButtonTextCount = 0
    private(set) var callSetProgressViewStepPassedCount = 0
    private(set) var setProgressViewStepPassed: Int?
    
    func displayTutorialHelp() {
        callDisplayTutorialHelpCount += 1
    }
    
    func display(step: AppTourStep) {
        callDisplayStepCount += 1
        displayStepPassed = step
    }
    
    func removeStep() {
        callRemoveStepCount += 1
    }
    
    func displayTutorialProgress(_ steps: Int) {
        callDisplayTutorialProgressCount += 1
        displayTutorialProgressPassed = steps
    }
    
    func setProgressViewStep(_ step: Int) {
        callSetProgressViewStepPassedCount += 1
        setProgressViewStepPassed = step
    }
    
    func hideCloseButtonText() {
        callHideCloseButtonTextCount += 1
    }
}

private final class AppTourCoordinatorSpy: AppTourCoordinating {
    var delegate: AppTourCoordinatorDelegate?
    
    var viewController: UIViewController?
    private(set) var callExitCount = 0
    
    func perform(action: AppTourAction) {
        if case .exit = action {
            callExitCount += 1
        }
    }
}
