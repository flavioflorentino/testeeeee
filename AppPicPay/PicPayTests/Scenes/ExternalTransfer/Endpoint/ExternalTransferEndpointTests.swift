import XCTest
@testable import PicPay

final class ExternalTransferEndpointTests: XCTestCase {
    // MARK: - permission
    func testPermission_ShouldBeCorrect() {
        let endpoint = ExternalTransferServiceEndpoint.permission
        XCTAssertEqual(endpoint.path, "checkPermission/")
        XCTAssertEqual(endpoint.method, .get)
        XCTAssertNil(endpoint.body)
    }
    
    // MARK: - createExternalTransfer
    func testCreateExternalTransfer_ShouldBeCorrect() throws {
        let message = "Message"
        let value = 1.0
        let password = "1234"
        let endpoint = ExternalTransferServiceEndpoint.createExternalTransfer(message: message, value: value, password: password)
        let body = try XCTUnwrap(endpoint.body)
        let headers = try XCTUnwrap(endpoint.customHeaders)
        let serializedObject = try JSONSerialization.jsonObject(with: body, options: .allowFragments)
        let dict = try XCTUnwrap(serializedObject as? [String: Any])
        let unwrappedMessage = try XCTUnwrap(dict["message"] as? String)
        let unwrappedValue = try XCTUnwrap(dict["value"] as? Double)
        let unwrappedPassword = try XCTUnwrap(headers["password"])
        XCTAssertEqual(endpoint.path, "saveWithdrawal")
        XCTAssertEqual(endpoint.method, .post)
        XCTAssertEqual(unwrappedMessage, message)
        XCTAssertEqual(unwrappedValue, value)
        XCTAssertEqual(unwrappedPassword, password)
    }
}
