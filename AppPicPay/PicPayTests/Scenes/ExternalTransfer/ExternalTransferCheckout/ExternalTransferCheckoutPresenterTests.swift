import UI
import XCTest
@testable import PicPay

private final class ExternalTransferCheckoutViewControllerMock: ExternalTransferCheckoutDisplay {
    private(set) var showTextMessageCallCount = 0
    private(set) var textMessage: String?
    private(set) var displayAlertCallCount = 0
    private(set) var displayErrorCallCount = 0
    private(set) var displaySystemAlertCallCount = 0
    private(set) var showPasswordDisplayCallCount = 0
    private(set) var resetDisplayCallCount = 0
    
    func showTextMessage(_ text: String) {
        showTextMessageCallCount += 1
        textMessage = text
    }
    
    func displayAlert(title: String, message: String, buttonTitle: String) {
        displayAlertCallCount += 1
    }
    
    func displayError(with alertData: StatusAlertData) {
        displayErrorCallCount += 1
    }
    
    func displaySystemAlert(title: String, message: String, buttonTitle: String) {
        displaySystemAlertCallCount += 1
    }
    
    func showPasswordDisplay() {
        showPasswordDisplayCallCount += 1
    }
    
    func resetDisplay() {
        resetDisplayCallCount += 1
    }
}

private final class ExternalTransferCheckoutCoordinatorMock: ExternalTransferCheckoutCoordinating {
    private(set) var performCallCount = 0
    private(set) var action: ExternalTransferCheckoutAction?
    var viewController: UIViewController?
    
    func perform(action: ExternalTransferCheckoutAction) {
        performCallCount += 1
        self.action = action
    }
}

final class ExternalTransferCheckoutPresenterTests: XCTestCase {
    // MARK: - Variables
    private let controller = ExternalTransferCheckoutViewControllerMock()
    private let coordinator = ExternalTransferCheckoutCoordinatorMock()
    private lazy var presenter: ExternalTransferCheckoutPresenter = {
        let presenter = ExternalTransferCheckoutPresenter(coordinator: coordinator)
        presenter.viewController = controller
        return presenter
    }()
    
    // MARK: - showPlaceholderMessage
    func testShowPlaceholderMessage_WhenIsVisible_ShouldCallShowTextMessageWithPlaceholderText() throws {
        presenter.showPlaceholderMessage(isVisible: true)
        let textMessage = try XCTUnwrap(controller.textMessage)
        XCTAssertEqual(controller.showTextMessageCallCount, 1)
        XCTAssertEqual(textMessage, PaymentRequestLocalizable.messagePlaceholder.text)
    }
    
    func testShowPlaceholderMessage_WhenIsNotVisible_ShouldCallShowTextMessageWithEmptyText() throws {
        presenter.showPlaceholderMessage(isVisible: false)
        let textMessage = try XCTUnwrap(controller.textMessage)
        XCTAssertEqual(controller.showTextMessageCallCount, 1)
        XCTAssertEqual(textMessage, "")
    }
    
    // MARK: - didNextStep
    func testDidNextStep_WhenActionIsNext_ShouldCallPerform() {
        presenter.didNextStep(action: .next(url: "url"))
        XCTAssertEqual(coordinator.performCallCount, 1)
    }
    
    // MARK: - presentShouldFillValueAlert
    func testPresentShouldFillValueAlert_ShouldCallDisplaySystemAlert() {
        presenter.presentShouldFillValueAlert()
        XCTAssertEqual(controller.displaySystemAlertCallCount, 1)
    }
    
    // MARK: - presentPaymentMethodAlert
    func testPresentPaymentMethodAlert_ShouldCallDisplayAlert() {
        presenter.presentPaymentMethodAlert()
        XCTAssertEqual(controller.displayAlertCallCount, 1)
    }
    
    // MARK: - presentPrivacyAlert
    func testPresentPrivacyAlert_ShouldCallDisplayAlert() {
        presenter.presentPrivacyAlert()
        XCTAssertEqual(controller.displayAlertCallCount, 1)
    }
    
    // MARK: - presentAlert
    func testPresentAlert_ShouldCallDisplayAlert() {
        presenter.presentAlert(title: "", message: "", buttonTitle: "")
        XCTAssertEqual(controller.displayAlertCallCount, 1)
    }
    
    // MARK: - presentError
    func testPresentError_ShouldCallDisplayError() {
        presenter.presentError()
        XCTAssertEqual(controller.displayErrorCallCount, 1)
    }
    
    // MARK: - presentPasswordDisplay
    func testPresentPasswordDisplay_ShouldCallPasswordDisplay() {
        presenter.presentPasswordDisplay()
        XCTAssertEqual(controller.showPasswordDisplayCallCount, 1)
    }
    
    // MARK: - presentWrongPasswordAlert
    func testPresentWrongPasswordAlert_ShouldCallResetDisplay() {
        presenter.presentWrongPasswordAlert()
        XCTAssertEqual(controller.resetDisplayCallCount, 1)
    }
}
