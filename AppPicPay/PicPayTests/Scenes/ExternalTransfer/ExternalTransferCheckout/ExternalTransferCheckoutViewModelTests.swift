import XCTest
import Core
@testable import PicPay

private final class ExternalTransferCheckoutServiceMock: ExternalTransferCheckoutServicing {
    var result: Result<(model: ExternalTransferWithdrawal, data: Data?), DialogError>?
    private(set) var message: String?
    private(set) var value: Double?
    private(set) var createExternalTransferCallCount = 0
    
    func createExternalTransfer(message: String, value: Double, password: String, completion: @escaping (Result<(model: ExternalTransferWithdrawal, data: Data?), DialogError>) -> Void) {
        createExternalTransferCallCount += 1
        self.message = message
        self.value = value
        guard let result = result else {
            return
        }
        completion(result)
    }
}

private final class ExternalTransferCheckoutPresenterMock: ExternalTransferCheckoutPresenting {
    var viewController: ExternalTransferCheckoutDisplay?
    private(set) var didNextStepCallCount = 0
    var action: ExternalTransferCheckoutAction?
    private(set) var showPlaceholderMessageCallCount = 0
    private(set) var isVisible: Bool?
    private(set) var presentShouldFillValueAlertCallCount = 0
    private(set) var presentPaymentMethodAlertCallCount = 0
    private(set) var createExternalTransferCallCount = 0
    private(set) var presentPrivacyAlertCallCount = 0
    private(set) var presentAlertCallCount = 0
    private(set) var presentErrorCallCount = 0
    private(set) var presentSystemAlertCallCount = 0
    private(set) var presentWrongPasswordAlertCallCount = 0
    private(set) var presentPasswordDisplayCount = 0
    
    
    func didNextStep(action: ExternalTransferCheckoutAction) {
        didNextStepCallCount += 1
        self.action = action
    }
    
    func showPlaceholderMessage(isVisible: Bool) {
        showPlaceholderMessageCallCount += 1
        self.isVisible = isVisible
    }
    
    func presentShouldFillValueAlert() {
        presentShouldFillValueAlertCallCount += 1
    }
    
    func presentPaymentMethodAlert() {
        presentPaymentMethodAlertCallCount += 1
    }
    
    func presentPrivacyAlert() {
        presentPrivacyAlertCallCount += 1
    }
    
    func presentAlert(title: String, message: String, buttonTitle: String) {
        presentAlertCallCount += 1
    }
    
    func presentError() {
        presentErrorCallCount += 1
    }
    
    func presentSystemAlert(title: String) {
        presentSystemAlertCallCount += 1
    }
    
    func presentWrongPasswordAlert() {
        presentWrongPasswordAlertCallCount += 1
    }
    
    func presentPasswordDisplay() {
        presentPasswordDisplayCount += 1
    }
}

final class ExternalTransferCheckoutViewModelTests: XCTestCase {
    // MARK: - Variables
    private let dependencies = DependencyContainerMock(ConsumerManagerMock())
    private let service = ExternalTransferCheckoutServiceMock()
    private let presenter = ExternalTransferCheckoutPresenterMock()
    private lazy var viewModel: ExternalTransferCheckoutViewModelInputs = {
        return ExternalTransferCheckoutViewModel(service: service, presenter: presenter, dependencies: dependencies)
    }()
    
    // MARK: - beginEditingTextView and changeTextView
    func testBeginEditingTextView_WhenHasOnlyPlaceholderOnTextViewIsTrue_ShouldCallShowPlaceholderMessageAndIsVisible() throws {
        viewModel.beginEditingTextView()
        let isVisible = try XCTUnwrap(presenter.isVisible)
        XCTAssertEqual(presenter.showPlaceholderMessageCallCount, 1)
        XCTAssertFalse(isVisible)
    }
    
    func testBeginEditingTextView_WhenHasOnlyPlaceholderOnTextViewIsFalse_ShouldNotCallShowPlaceholderMessage() {
        viewModel.changeTextView(text: "Test")
        viewModel.beginEditingTextView()
        XCTAssertEqual(presenter.showPlaceholderMessageCallCount, 0)
    }
    
    // MARK: - endEditingTextView and changeTextView
    func testEndEditingTextView_WhenHasOnlyPlaceholderOnTextViewIsTrue_ShouldCallShowPlaceholderMessageAndIsNotVisible() throws {
        viewModel.endEditingTextView()
        let isVisible = try XCTUnwrap(presenter.isVisible)
        XCTAssertEqual(presenter.showPlaceholderMessageCallCount, 1)
        XCTAssert(isVisible)
    }
    
    func testendEditingTextView_WhenHasOnlyPlaceholderOnTextViewIsFalse_ShouldNotCallShowPlaceholderMessage() {
        viewModel.changeTextView(text: "Test")
        viewModel.endEditingTextView()
        XCTAssertEqual(presenter.showPlaceholderMessageCallCount, 0)
    }
    
    // MARK: - shouldChangeText
    func testShouldChangeText_WhenTextHasLessThan100Characters_ShouldReturnTrue() {
        let text = ""
        XCTAssertTrue(viewModel.shouldChangeText(text))
    }
    
    func testShouldChangeText_WhenTextHasMoreThan100Characters_ShouldReturnFalse() {
        let text = "Teste teste teste teste teste teste teste teste teste teste teste teste teste teste teste teste teste"
        XCTAssertFalse(viewModel.shouldChangeText(text))
    }
    
    // MARK: - startGenerateExternalTransfer
    func testStartGenerateExternalTransfer_WhenValueIsZero_ShouldCallPresentShouldFillValueAlert() throws {
        viewModel.startGenerateExternalTransfer(value: 0.0)
        XCTAssertEqual(presenter.presentShouldFillValueAlertCallCount, 1)
    }
    
    func testStartGenerateExternalTransfer_WhenValueGreaterThanZero_ShouldCallPresentPasswordDisplay() throws {
        viewModel.startGenerateExternalTransfer(value: 10.0)
        XCTAssertEqual(presenter.presentPasswordDisplayCount, 1)
    }
    
    // MARK: - generateExternalTransfer
    func testGenerateExternalTransfer_WhenHasNotOnlyPlaceholderOnTextViewAndValueGreaterThanZeroAndResultIsSuccess_ShouldCallDidNextStep() throws {
        let url = "url"
        let message = "Has Message"
        let value = 10.0
        let model = ExternalTransferWithdrawal(url: url)
        viewModel.changeTextView(text: message)
        service.result = .success((model: model, data: nil))
        viewModel.generateExternalTransfer(message: message, value: value, privacy: .private, password: "")
        let unwrappedMessage = try XCTUnwrap(service.message)
        let unwrappedValue = try XCTUnwrap(service.value)
        let action = try XCTUnwrap(presenter.action)
        XCTAssertEqual(presenter.didNextStepCallCount, 1)
        XCTAssertEqual(unwrappedMessage, message)
        XCTAssertEqual(unwrappedValue, value)
        guard case let ExternalTransferCheckoutAction.next(nextUrl) = action else {
            XCTFail("Next action is not defined")
            return
        }
        XCTAssertEqual(nextUrl, url)
    }
    
    func testGenerateExternalTransfer_WhenHasOnlyPlaceholderOnTextViewAndValueGreaterThanZeroAndResultIsSuccess_ShouldCallDidNextStepWithOutPlaceholderMessage() throws {
        let url = "url"
        let value = 10.0
        let model = ExternalTransferWithdrawal(url: url)
        service.result = .success((model: model, data: nil))
        viewModel.generateExternalTransfer(message: "PlaceHolder", value: value, privacy: .private, password: "")
        let unwrappedMessage = try XCTUnwrap(service.message)
        let unwrappedValue = try XCTUnwrap(service.value)
        let action = try XCTUnwrap(presenter.action)
        XCTAssertEqual(unwrappedMessage, "")
        XCTAssertEqual(unwrappedValue, value)
        XCTAssertEqual(presenter.didNextStepCallCount, 1)
        guard case let ExternalTransferCheckoutAction.next(nextUrl) = action else {
            XCTFail("Next action is not defined")
            return
        }
        XCTAssertEqual(nextUrl, url)
    }
    
    func testGenerateExternalTransfer_WhenValueGreaterThanZeroAndResultIsFailureWithApiError_ShouldCallPresentError() {
        service.result = .failure(DialogError.default(ApiError.unknown(nil)))
        viewModel.generateExternalTransfer(message: "PlaceHolder", value: 10.0, privacy: .private, password: "")
        XCTAssertEqual(presenter.presentErrorCallCount, 1)
    }
    
    func testGenerateExternalTransfer_WhenValueGreaterThanZeroAndResultIsFailureWithDialogResponseErrorWithCode441_ShouldCallPresentError() {
        let errorAlert = DialogErrorModel(title: "", message: "", buttonText: "")
        let dialogResponse = DialogResponseError(code: "441", model: errorAlert)
        service.result = .failure(DialogError.dialog(dialogResponse))
        viewModel.generateExternalTransfer(message: "PlaceHolder", value: 10.0, privacy: .private, password: "")
        XCTAssertEqual(presenter.presentAlertCallCount, 1)
    }
    
    func testGenerateExternalTransfer_WhenValueGreaterThanZeroAndResultIsFailureWithDialogResponseErrorWithCode442_ShouldCallPresentError() {
        let errorAlert = DialogErrorModel(title: "", message: "", buttonText: "")
        let dialogResponse = DialogResponseError(code: "442", model: errorAlert)
        service.result = .failure(DialogError.dialog(dialogResponse))
        viewModel.generateExternalTransfer(message: "PlaceHolder", value: 10.0, privacy: .private, password: "")
        XCTAssertEqual(presenter.presentAlertCallCount, 1)
    }
    
    func testGenerateExternalTransfer_WhenValueGreaterThanZeroAndResultIsFailureWithDialogResponseErrorWithCode443_ShouldCallPresentError() {
        let errorAlert = DialogErrorModel(title: "", message: "", buttonText: "")
        let dialogResponse = DialogResponseError(code: "443", model: errorAlert)
        service.result = .failure(DialogError.dialog(dialogResponse))
        viewModel.generateExternalTransfer(message: "PlaceHolder", value: 10.0, privacy: .private, password: "")
        XCTAssertEqual(presenter.presentAlertCallCount, 1)
    }
    
    func testGenerateExternalTransfer_WhenValueGreaterThanZeroAndResultIsFailureWithDialogResponseErrorWithCode444_ShouldCallPresentError() {
        let errorAlert = DialogErrorModel(title: "", message: "", buttonText: "")
        let dialogResponse = DialogResponseError(code: "444", model: errorAlert)
        service.result = .failure(DialogError.dialog(dialogResponse))
        viewModel.generateExternalTransfer(message: "PlaceHolder", value: 10.0, privacy: .private, password: "")
        XCTAssertEqual(presenter.presentAlertCallCount, 1)
    }
    
    func testGenerateExternalTransfer_WhenValueGreaterThanZeroAndResultIsFailureWithApiErrorWithCode6003_ShouldCallPresentError() {
        var requestError = RequestError()
        requestError.code = "6003"
        service.result = .failure(DialogError.default(ApiError.badRequest(body: requestError)))
        viewModel.generateExternalTransfer(message: "PlaceHolder", value: 10.0, privacy: .private, password: "")
        XCTAssertEqual(presenter.presentWrongPasswordAlertCallCount, 1)
    }
    
    func testGenerateExternalTransfer_WhenValueGreaterThanZeroAndResultIsFailureWithDialogResponseErrorWithUnkownCode_ShouldCallPresentError() {
        let errorAlert = DialogErrorModel(title: "", message: "", buttonText: "")
        let dialogResponse = DialogResponseError(code: "-1", model: errorAlert)
        service.result = .failure(DialogError.dialog(dialogResponse))
        viewModel.generateExternalTransfer(message: "PlaceHolder", value: 10.0, privacy: .private, password: "")
        XCTAssertEqual(presenter.presentErrorCallCount, 1)
    }
    
    func testGenerateExternalTransfer_WhenValueGreaterThanZeroAndResultIsFailureWithApiErrorResponseErrorWithMessage_ShouldCallPresentSystemAlert() {
        let apiError = ApiError.badRequest(body: RequestError())
        service.result = .failure(DialogError.default(apiError))
        viewModel.generateExternalTransfer(message: "PlaceHolder", value: 10.0, privacy: .private, password: "")
        XCTAssertEqual(presenter.presentSystemAlertCallCount, 1)
    }
    
    // MARK: - privacySelection
    func testPrivacySelection_ShouldCallPresentPrivacyAlert() {
        viewModel.privacySelection()
        XCTAssertEqual(presenter.presentPrivacyAlertCallCount, 1)
    }
    
    // MARK: - paymentMethodSelection
    func testPaymentMethodSelection_ShouldCallPresentPaymentMethodAlert() {
        viewModel.paymentMethodSelection()
        XCTAssertEqual(presenter.presentPaymentMethodAlertCallCount, 1)
    }
}
