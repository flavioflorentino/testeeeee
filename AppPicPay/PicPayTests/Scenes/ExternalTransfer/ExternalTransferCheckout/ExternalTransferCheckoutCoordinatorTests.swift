import XCTest
@testable import PicPay

final class ExternalTransferCheckoutCoordinatorTests: XCTestCase {
    // MARK: - Variables
    private let controller = ViewControllerMock()
    private var navigationController: NavigationControllerMock?
    private lazy var coordinator: ExternalTransferCheckoutCoordinator = {
        let coordinator = ExternalTransferCheckoutCoordinator()
        coordinator.viewController = controller
        return coordinator
    }()
    
    // MARK: - Setup
    override func setUp() {
        super.setUp()
        navigationController = NavigationControllerMock(rootViewController: controller)
    }
    
    // MARK: - perform
    func testPerform_WhenCaseIsNext_ShouldPushExternalTransferAvailableViewController() {
        coordinator.perform(action: .next(url: "url"))
        XCTAssert(navigationController?.pushedViewController is ExternalTransferAvailableViewController)
    }
}
