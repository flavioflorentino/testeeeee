import Core
import XCTest
@testable import PicPay

private final class MockExternalTransferAvailablePresenter: ExternalTransferAvailablePresenting {
    var viewController: ExternalTransferAvailableDisplay?
    
    private(set) var didNextStepDismissCount = 0
    private(set) var didCallNextStepSendPaymentCount = 0
    
    func didNextStep(action: ExternalTransferAvailableAction) {
        switch action {
        case .dismiss:
            didNextStepDismissCount += 1
        case .sendPayment:
            didCallNextStepSendPaymentCount += 1
        }
    }
}

final class ExternalTransferAvailableViewModelTests: XCTestCase {
    // MARK: - Variables
    private lazy var presenter = MockExternalTransferAvailablePresenter()
    private lazy var viewModel = ExternalTransferAvailableViewModel(presenter: presenter, transferURLString: "teste")
    
    // MARK: - Dismiss
    func testDismiss_ShouldCallNextStepWithDismiss() {
        viewModel.dismiss()
        XCTAssertEqual(presenter.didNextStepDismissCount, 1)
    }
    
    // MARK: - Send Payment
    func testSendPayment_ShouldCallNextStepWithSendPayment() {
        viewModel.sendPayment()
        XCTAssertEqual(presenter.didCallNextStepSendPaymentCount, 1)
    }
}
