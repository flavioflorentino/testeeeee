import XCTest
@testable import UI
@testable import PicPay

final class ExternalTransferAvailableCoordinatorTests: XCTestCase {
    // MARK: - Variables
    private let controller = ViewControllerMock()
    private lazy var coordinator: ExternalTransferAvailableCoordinator = {
        let coordinator = ExternalTransferAvailableCoordinator()
        coordinator.viewController = controller
        return coordinator
    }()
    
    // MARK: - perform
    func testPerform_WhenActionIsDismiss_ShouldDismissViewController() {
        coordinator.perform(action: .dismiss)
        XCTAssertEqual(controller.didDismissControllerCount, 1)
    }
    
    func testPerform_WhenActionIsSendPayment_ShouldPresentActivityViewController() {
        coordinator.perform(action: .sendPayment(""))
        XCTAssertEqual(controller.didPresentControllerCount, 1)
        XCTAssert(controller.viewControllerPresented is UIActivityViewController)
    }
}
