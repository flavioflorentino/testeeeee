import XCTest
@testable import PicPay
@testable import UI

private final class MockExternalTransferAvailableCoordinator: ExternalTransferAvailableCoordinating {
    var viewController: UIViewController?
    
    private(set) var didCallPerformDismissCount = 0
    private(set) var didCallPerformSendPaymentCount = 0
    private(set) var urlString: String?
    
    func perform(action: ExternalTransferAvailableAction) {
        switch action {
        case .dismiss:
            didCallPerformDismissCount += 1
        case let .sendPayment(url):
            didCallPerformSendPaymentCount += 1
            urlString = url
        }
    }
}

final class ExternalTransferAvailablePresenterTests: XCTestCase {
    // MARK: - Variables
    private lazy var coordinator = MockExternalTransferAvailableCoordinator()
    private lazy var presenter: ExternalTransferAvailablePresenter = ExternalTransferAvailablePresenter(coordinator: coordinator)

    // MARK: - didNextStep
    func testDidNextStep_WhenActionIsDismiss_ShouldCallPerformDismiss() {
        presenter.didNextStep(action: .dismiss)
        XCTAssertEqual(coordinator.didCallPerformDismissCount, 1)
    }
    
    func testDidNextStep_WhenActionIsSendPaymentWithTesteString_ShouldCallPerformSendPaymentWithTesteString() {
        let testString = "Teste"
        presenter.didNextStep(action: .sendPayment(testString))
        XCTAssertEqual(coordinator.didCallPerformSendPaymentCount, 1)
        XCTAssertEqual(coordinator.urlString, testString)
    }
}
