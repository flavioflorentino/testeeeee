import Core
import XCTest
@testable import PicPay

private final class MockExternalTransferIntroService: ExternalTransferIntroServicing {
    
    enum ServiceResult {
        case success
        case insufficientBalance
        case limitReached
        case defaultError
    }
    
    var result = ServiceResult.success
    
    func getExternalTransferPermission(completion: @escaping (Result<(model: NoContent, data: Data?), DialogError>) -> Void) {
        switch result {
        case .success:
            completion(.success((NoContent(), Data())))
        case .limitReached:
            let modelError = DialogErrorModel(title: "", message: "", buttonText: "")
            let responseError = DialogResponseError(code: "441", model: modelError)
            let dialogError = DialogError.dialog(responseError)
            completion(.failure(dialogError))
        case .insufficientBalance:
            let modelError = DialogErrorModel(title: "", message: "", buttonText: "")
            let responseError = DialogResponseError(code: "442", model: modelError)
            let dialogError = DialogError.dialog(responseError)
            completion(.failure(dialogError))
        case .defaultError:
            completion(.failure(.default(ApiError.connectionFailure)))
        }
    }
}

private final class MockExternalTransferIntroPresenter: ExternalTransferIntroPresenting {
    var viewController: ExternalTransferIntroDisplay?
    
    private(set) var didNextStepNextCount = 0
    private(set) var didCallNextStepCloseCount = 0
    private(set) var didCallNextStepAddBalanceCount = 0
    private(set) var presentLimitErrorCount = 0
    private(set) var presentBalanceErrorCount = 0
    private(set) var presentErrorCount = 0
    private(set) var hideLoaderCount = 0
    
    func didNextStep(action: ExternalTransferIntroAction) {
        switch action {
        case .addBalance:
            didCallNextStepAddBalanceCount += 1
        case .close:
            didCallNextStepCloseCount += 1
        case .next:
            didNextStepNextCount += 1
        }
    }
    
    func presentLimitError(titleAlert: String, descriptionAlert: String, buttonTitle: String) {
        presentLimitErrorCount += 1
    }
    
    func presentBalanceError(titleAlert: String, descriptionAlert: String, buttonTitle: String) {
        presentBalanceErrorCount += 1
    }
    
    func presentError() {
        presentErrorCount += 1
    }
    
    func hideLoader() {
        hideLoaderCount += 1
    }
}

final class ExternalTransferIntroViewModelTests: XCTestCase {
    // MARK: - Variables
    private lazy var service = MockExternalTransferIntroService()
    private lazy var presenter = MockExternalTransferIntroPresenter()
    private lazy var viewModel = ExternalTransferIntroViewModel(service: service, presenter: presenter)
    
    // MARK: - dismiss
    func testDismiss_ShouldCallNextStepWithClose() {
        viewModel.dismiss()
        XCTAssertEqual(presenter.didCallNextStepCloseCount, 1)
    }
    
    // MARK: - handleExternalPermission
    func testHandleExternalPermission_WhenServiceReturnsSuccess_ShouldCallNextStepWithNext() {
        service.result = .success
        viewModel.handleExternalPermission()
        XCTAssertEqual(presenter.hideLoaderCount, 1)
        XCTAssertEqual(presenter.didNextStepNextCount, 1)
    }
    
    func testHandleExternalPermission_WhenServiceReturnsLimitReached_ShouldCallLimitError() {
        service.result = .limitReached
        viewModel.handleExternalPermission()
        XCTAssertEqual(presenter.presentLimitErrorCount, 1)
    }
    
    func testHandleExternalPermission_WhenServiceReturnsInsufficientBalance_ShouldCallBalanceError() {
        service.result = .insufficientBalance
        viewModel.handleExternalPermission()
        XCTAssertEqual(presenter.presentBalanceErrorCount, 1)
    }
    
    func testHandleExternalPermission_WhenServiceReturnsDefaultError_ShouldCallPresentError() {
        service.result = .defaultError
        viewModel.handleExternalPermission()
        XCTAssertEqual(presenter.presentErrorCount, 1)
    }
    
    // MARK: - showAddBalance
    func testShowAddBalance_ShouldCallNextStepWithAddBalance() {
        viewModel.showAddBalance()
        XCTAssertEqual(presenter.didCallNextStepAddBalanceCount, 1)
    }
    
    // MARK: - handleBalanceBack
    func testHandleBalanceBack_ShouldCallNextStepWithClose() {
        viewModel.handleBalanceBack()
        XCTAssertEqual(presenter.didCallNextStepCloseCount, 1)
    }
    
    // MARK: - handleLimitError
    func testHandleLimitError_ShouldCallNextStepWithClose() {
        viewModel.handleLimitError()
        XCTAssertEqual(presenter.didCallNextStepCloseCount, 1)
    }
}
