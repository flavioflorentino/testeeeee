import XCTest
@testable import UI
@testable import PicPay

final class ExternalTransferIntroCoordinatorTests: XCTestCase {
    // MARK: - Variables
    private let controller = ViewControllerMock()
    private var navigationController: NavigationControllerMock!
    private lazy var coordinator: ExternalTransferIntroCoordinator = {
        let coordinator = ExternalTransferIntroCoordinator()
        coordinator.viewController = controller
        return coordinator
    }()
    
    // MARK: - Setup
    override func setUp() {
        super.setUp()
        navigationController = NavigationControllerMock(rootViewController: controller)
    }
    
    // MARK: - perform
    func testPerform_WhenActionIsAddBalance_ShouldPresentRechargeLoadViewController() {
        coordinator.perform(action: .addBalance)
        let navController = navigationController.viewControllerPresented as? UINavigationController
        XCTAssert(navController?.topViewController is RechargeLoadViewController)
    }
    
    func testPerform_WhenActionIsClose_ShouldDismissViewController() {
        coordinator.perform(action: .close)
        XCTAssert(navigationController.isDismissViewControllerCalled)
    }
}
