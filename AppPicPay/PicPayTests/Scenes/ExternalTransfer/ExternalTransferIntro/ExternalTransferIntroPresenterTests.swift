import XCTest
@testable import PicPay
@testable import UI

private final class MockExternalTransferIntroCoordinator: ExternalTransferIntroCoordinating {
    var viewController: UIViewController?
    
    private(set) var action: ExternalTransferIntroAction?
    private(set) var didPerformCount = 0
    
    func perform(action: ExternalTransferIntroAction) {
        self.action = action
        didPerformCount += 1
    }
}

private final class MockExternalTransferIntroController: ExternalTransferIntroDisplay {
    
    private(set) var didDisplayErrorCount = 0
    private(set) var didDisplayLimitErrorCount = 0
    private(set) var didDisplayBalanceErrorCount = 0
    private(set) var hideLoaderCount = 0
    
    func displayError(with alertData: StatusAlertData) {
        didDisplayErrorCount += 1
    }
    
    func displayLimitError(titleAlert: NSAttributedString, descriptionAlert: NSAttributedString, buttonTitle: String) {
        didDisplayLimitErrorCount += 1
    }
    
    func displayBalanceError(titleAlert: String, descriptionAlert: String, buttonTitle: String, backButtonTitle: String) {
        didDisplayBalanceErrorCount += 1
    }
    
    func hideLoader() {
        hideLoaderCount += 1
    }
}

final class ExternalTransferIntroPresenterTests: XCTestCase {
    // MARK: - Variables
    private lazy var coordinator = MockExternalTransferIntroCoordinator()
    private lazy var controller = MockExternalTransferIntroController()
    private lazy var presenter: ExternalTransferIntroPresenter = {
        let externalTransferPresenter = ExternalTransferIntroPresenter(coordinator: coordinator)
        externalTransferPresenter.viewController = controller
        return externalTransferPresenter
    }()

    // MARK: - didNextStep
    func testDidNextStep_WhenActionIsAddBalance_ShouldCallPerformAddBalance() {
        presenter.didNextStep(action: .addBalance)
        XCTAssertEqual(coordinator.didPerformCount, 1)
        XCTAssertEqual(coordinator.action, .addBalance)
    }
    
    func testDidNextStep_WhenActionIsClose_ShouldCallPerformClose() {
        presenter.didNextStep(action: .close)
        XCTAssertEqual(coordinator.didPerformCount, 1)
        XCTAssertEqual(coordinator.action, .close)
    }
    
    func testDidNextStep_WhenActionIsNext_ShouldCallPerformNext() {
        presenter.didNextStep(action: .next)
        XCTAssertEqual(coordinator.didPerformCount, 1)
        XCTAssertEqual(coordinator.action, .next)
    }
    
    // MARK: - presentLimitError
    func testPresentLimitError_ShouldCallDisplayLimitError() {
        presenter.presentLimitError(
            titleAlert: "",
            descriptionAlert: "",
            buttonTitle: ""
        )
        XCTAssertEqual(controller.didDisplayLimitErrorCount, 1)
    }
    
    // MARK: - presentBalanceError
    func testPresentBalanceError_ShouldCallDisplayBalanceError() {
        presenter.presentBalanceError(
            titleAlert: "",
            descriptionAlert: "",
            buttonTitle: ""
        )
        XCTAssertEqual(controller.didDisplayBalanceErrorCount, 1)
    }
    
    // MARK: - presentError
    func testPresentError_ShouldCallDisplayError() {
        presenter.presentError()
        XCTAssertEqual(controller.didDisplayErrorCount, 1)
    }
    
    // MARK: - hideLoader
    func testHideLoader_ShouldCallHideLoaderOnViewController() {
        presenter.hideLoader()
        XCTAssertEqual(controller.hideLoaderCount, 1)
    }
}
