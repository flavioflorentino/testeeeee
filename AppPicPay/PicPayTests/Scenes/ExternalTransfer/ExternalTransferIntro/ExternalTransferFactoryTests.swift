import FeatureFlag
import XCTest

@testable import PicPay

final class ExternalTransferIntroFactoryTests: XCTestCase {
    // MARK: - Variables
    private let featureManagerMock = FeatureManagerMock()
    private lazy var factory = ExternalTransferIntroFactory(featureManager: featureManagerMock)
    
    // MARK: - make
    func testMake_WhenFeatureFlagIsFalse_ShouldReturnNil() {
        featureManagerMock.override(key: .externalTransfer, with: false)
        XCTAssertNil(factory.make(origin: .deeplink))
    }
    
    func testMake_WhenFeatureFlagIsTrue_ShouldReturnNotNil() {
        featureManagerMock.override(key: .externalTransfer, with: true)
        XCTAssertNotNil(factory.make(origin: .deeplink))
    }
}
