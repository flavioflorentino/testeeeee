import XCTest
import Core
import AnalyticsModule

@testable import PicPay

private final class PromotionsServiceMock: PromotionsServicing {
    var isTestingSuccess = true
    
    var userName: String?
    var sellerTitle: String?
    var sellerAddres: String?
    var sellerPhone: String?
    var sellerImageUrl: URL?
    var sellerDescription: String?
    var promotions: [PromotionSeller.Item]?
    var sellerMap: PromotionSeller.Map?
    var whatsAppText: String?
    var whatsAppPhone: String?
    private(set) var calledLoadWhatsAppNumber = 0
    
    lazy var paymentInfo: PromotionSeller.PaymentInfo = {
        PromotionSeller.PaymentInfo(sellerId: 0, storeId: 0)
    }()
    
    lazy var sellerTracking: PromotionSeller.Tracking = {
        PromotionSeller.Tracking(
            type: "",
            title: sellerTitle ?? "",
            sellerId: 0,
            storeId: 0,
            section: ""
        )
    }()
    
    var consumerName: String? {
        userName
    }
    
    func promotions(latitude: Double, longitude: Double, _ completion: @escaping (PromotionsResult) -> Void) {
        XCTFail("Method not used in this test")
    }
    
    func promotions(_ completion: @escaping (PromotionsResult) -> Void) {
        XCTFail("Method not used in this test")
    }
    
    func sellerDetails(id: String, _ completion: @escaping (Result<PromotionSeller, ApiError>) -> Void) {
        if isTestingSuccess {
            let seller = PromotionSeller(
                title: sellerTitle ?? "",
                address: sellerAddres ?? "",
                phone: sellerPhone,
                imageUrl: sellerImageUrl,
                description: sellerDescription ?? "",
                items: promotions ?? [],
                paymentInfo: paymentInfo,
                tracking: sellerTracking,
                map: sellerMap
            )
            return completion(.success(seller))
        }
        
        return completion(.failure(.connectionFailure))
    }

    func loadSellerID(from storeId: String, _ completion: @escaping (Result<String, ApiError>) -> Void) {
        if isTestingSuccess {
            return completion(.success("0001"))
        }

        return completion(.failure(.connectionFailure))
    }
    
    func loadWhatsAppNumber(from sellerId: String, _ completion: @escaping (Result<WhatsAppSeller, ApiError>) -> Void) {
        calledLoadWhatsAppNumber += 1
        if isTestingSuccess {
            let whatsapp = WhatsAppSeller(phone: whatsAppPhone ?? "", text: whatsAppText ?? "")
            return completion(.success(whatsapp))
        }
        
        return completion(.failure(.connectionFailure))
    }
}

private final class FavoriteInteractorMock: FavoriteInteracting {
    var currentState: FavoriteState?
    
    func checkState(id: String, type: Favorite.`Type`, _ completion: @escaping (FavoriteState) -> Void) {
        guard let state = currentState else {
            XCTFail("No state to load")
            return
        }
        completion(state)
    }
    
    func changeState(_ state: FavoriteState, id: String, type: Favorite.`Type`, _ completion: @escaping (FavoriteState) -> Void) {
        guard let state = currentState else {
            XCTFail("No state to load")
            return
        }
        completion(state)
    }
}

private final class PromotionDetailsPresenterSpy: PromotionDetailsPresenting {
    private(set) var calledShowDetails = 0
    private(set) var calledShowError = 0
    private(set) var calledUpdateState = 0
    private(set) var calledCallPhone = 0
    private(set) var calledPerformPayment = 0
    private(set) var calledOpenMap = 0
    private(set) var calledOpenWhatsApp = 0
    private(set) var calledPresentShare = 0
    private(set) var calledPresentWhatsAppButton = 0
    
    private(set) var promotionSeller: PromotionSeller?
    private(set) var apiError: ApiError?
    private(set) var favoriteState: FavoriteState?
    private(set) var phoneNumber: String?
    private(set) var mapTitle: String?
    private(set) var latitude: Double?
    private(set) var longitude: Double?
    private(set) var whatsapp: WhatsAppSeller?
    private(set) var shareMessageText: String?
    private(set) var whatsAppButtonIsHidden: Bool?
    
    func show(details promotionSeller: PromotionSeller) {
        calledShowDetails += 1
        self.promotionSeller = promotionSeller
    }
    
    func show(error: ApiError) {
        calledShowError += 1
        apiError = error
    }
    
    func updateState(_ state: FavoriteState) {
        calledUpdateState += 1
        favoriteState = state
    }
    
    func callPhone(_ number: String?) {
        calledCallPhone += 1
        phoneNumber = number
    }
    
    func performPayment(seller: PromotionSeller) {
        calledPerformPayment += 1
        self.promotionSeller = seller
    }
    
    func openMap(title: String, latitude: Double, longitude: Double) {
        calledOpenMap += 1
        mapTitle = title
        self.latitude = latitude
        self.longitude = longitude
    }
    
    func openWhatsApp(whatsapp: WhatsAppSeller) {
        calledOpenWhatsApp += 1
        self.whatsapp = whatsapp
    }
    
    func presentShare(withText text: String) {
        calledPresentShare += 1
        shareMessageText = text
    }
    
    func presentWhatsAppButton(isHidden: Bool) {
        calledPresentWhatsAppButton += 1
        whatsAppButtonIsHidden = isHidden
    }
}

final class PromotionDetailsInteractorTests: XCTestCase {
    private let presenterSpy = PromotionDetailsPresenterSpy()
    private let serviceMock = PromotionsServiceMock()
    private let favoriteMock = FavoriteInteractorMock()
    
    private let analyticsSpy = AnalyticsSpy()
    private lazy var featureMock = FeatureManagerMock()
    
    private lazy var sut: PromotionDetailsInteractor = {
        let dependencyContainerMock = DependencyContainerMock(analyticsSpy, featureMock)
        let viewModel = PromotionDetailsInteractor(
            idType: .seller(id: "0001"),
            service: serviceMock,
            presenter: presenterSpy,
            favoriteInteractor: favoriteMock,
            dependencies: dependencyContainerMock
        )
        return viewModel
    }()
    
    func testLoadDetails_WhenResultIsSuccess_ShouldSaveSellerAndCallPresenterShowDetails() {
        serviceMock.isTestingSuccess = true
        favoriteMock.currentState = .unavailable
        
        sut.loadDetails()
        
        let expectedSeller = PromotionSeller(
            title: "",
            address: "",
            phone: nil,
            imageUrl: nil,
            description: "",
            items: [],
            paymentInfo: serviceMock.paymentInfo,
            tracking: serviceMock.sellerTracking,
            map: nil
        )
        XCTAssertEqual(presenterSpy.calledShowDetails, 1)
        XCTAssertEqual(presenterSpy.promotionSeller, expectedSeller)
    }
    
    func testLoadDetails_WhenResultIsFailure_ShouldCallPresenterShowError() {
        serviceMock.isTestingSuccess = false

        sut.loadDetails()
        
        XCTAssertEqual(presenterSpy.calledShowError, 1)
        XCTAssertEqual(presenterSpy.apiError?.localizedDescription, ApiError.connectionFailure.localizedDescription)
    }
    
    func testCallPhone_WhenCalledWithPreviousLoadDetail_ShouldCallPresenterCallPhone() {
        serviceMock.isTestingSuccess = true
        serviceMock.sellerPhone = "555555"
        favoriteMock.currentState = .unavailable
        sut.loadDetails()
        sut.callPhone()
        
        XCTAssertEqual(presenterSpy.calledCallPhone, 1)
        XCTAssertEqual(presenterSpy.phoneNumber, "555555")
    }
    
    func testPerformPayment_WhenHasASeller_ShouldCallPresenterPerformPayment() {
        serviceMock.isTestingSuccess = true
        favoriteMock.currentState = .unavailable
        sut.loadDetails()
        sut.performPayment()
        
        let expectedSeller = PromotionSeller(
            title: "",
            address: "",
            phone: nil,
            imageUrl: nil,
            description: "",
            items: [],
            paymentInfo: serviceMock.paymentInfo,
            tracking: serviceMock.sellerTracking,
            map: nil
        )
        XCTAssertEqual(presenterSpy.calledPerformPayment, 1)
        XCTAssertEqual(presenterSpy.promotionSeller, expectedSeller)
    }
    
    func testOpenMap_WhenHasSellerAndMap_ShouldCallPresenterOpenMap() {
        serviceMock.isTestingSuccess = true
        favoriteMock.currentState = .unavailable
        let expectedLatitude = -23.541130
        let expectedLongitude = -46.733936
        serviceMock.sellerTitle = "PicPay"
        serviceMock.sellerMap = PromotionSeller.Map(latitude: expectedLatitude, longitude: expectedLongitude, pinUrl: nil)
        sut.loadDetails()
        
        sut.openMap()
        
        XCTAssertEqual(presenterSpy.calledOpenMap, 1)
        XCTAssertEqual(presenterSpy.mapTitle, "PicPay")
        XCTAssertEqual(presenterSpy.latitude, expectedLatitude)
        XCTAssertEqual(presenterSpy.longitude, expectedLongitude)
    }
    
    func testOpenWhatsAppConversation_WhenHasSellerAndFeatureFlagIsTrue_ShouldLoadAndOpenWhatsApp() {
        serviceMock.isTestingSuccess = true
        favoriteMock.currentState = .unavailable
        featureMock.override(key: .isWhatsappSellerAvailable, with: true)
        let expectedText = "Olá! Encontrei vocês pelo app PicPay e gostaria de tirar dúvidas."
        let expectedPhone = "+5511912345678"
        let expectedWhatsApp = WhatsAppSeller(phone: expectedPhone, text: expectedText)
        serviceMock.whatsAppText = expectedText
        serviceMock.whatsAppPhone = expectedPhone
        sut.loadDetails()
        
        sut.openWhatsAppConversation()
        
        XCTAssertEqual(presenterSpy.calledOpenWhatsApp, 1)
        XCTAssertEqual(presenterSpy.whatsapp, expectedWhatsApp)
        XCTAssertEqual(serviceMock.calledLoadWhatsAppNumber, 1)
    }
    
    func testOpenWhatsAppConversation_WhenHasSellerAndFeatureFlagIsFalse_ShouldNotLoadWhatsApp() {
        serviceMock.isTestingSuccess = true
        favoriteMock.currentState = .unavailable
        featureMock.override(key: .isWhatsappSellerAvailable, with: false)
        let expectedText = "Olá! Encontrei vocês pelo app PicPay e gostaria de tirar dúvidas."
        let expectedPhone = "+5511912345678"
        serviceMock.whatsAppText = expectedText
        serviceMock.whatsAppPhone = expectedPhone
        sut.loadDetails()
        
        sut.openWhatsAppConversation()
        
        XCTAssertEqual(presenterSpy.calledOpenWhatsApp, 0)
        XCTAssertNil(presenterSpy.whatsapp)
        XCTAssertEqual(serviceMock.calledLoadWhatsAppNumber, 0)
    }
    
    func testOpenWhatsAppConversation_WhenDoNotHaveSeller_ShouldHideWhatsAppButton() {
        serviceMock.isTestingSuccess = true
        
        sut.openWhatsAppConversation()
        
        XCTAssertEqual(presenterSpy.calledOpenWhatsApp, 0)
        XCTAssertNil(presenterSpy.whatsapp)
        XCTAssertEqual(presenterSpy.calledPresentWhatsAppButton, 1)
        XCTAssertEqual(presenterSpy.whatsAppButtonIsHidden, true)
    }
    
    func testCheckFavoriteState_WhenCalled_ShouldCallPresenterWithCurrentState() {
        favoriteMock.currentState = .unavailable
        
        sut.checkFavoriteState()
        
        XCTAssertEqual(presenterSpy.calledUpdateState, 1)
        XCTAssertEqual(presenterSpy.favoriteState, FavoriteState.unavailable)
    }
    
    func testChangeFavoriteState_WhenCalledWithStateFavorited_ShouldCallPresenterUpdateStateWithUnfavoritedStateAndTrackEvent() {
        favoriteMock.currentState = .unfavorited
        let expectedEvent = FavoritesEvent.statusChanged(false, id: "0001", origin: .profile)
        
        sut.changeFavoriteState(.favorited)
        
        XCTAssertEqual(presenterSpy.calledUpdateState, 1)
        XCTAssertEqual(presenterSpy.favoriteState, FavoriteState.unfavorited)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent.event()))
    }
    
    func testChangeFavoriteState_WhenCalledWithStateUnfavorited_ShouldCallPresenterUpdateStateWithFavoritedStateAndTrackEvent() {
        favoriteMock.currentState = .favorited
        let expectedEvent = FavoritesEvent.statusChanged(true, id: "0001", origin: .profile)
        
        sut.changeFavoriteState(.unfavorited)
        
        XCTAssertEqual(presenterSpy.calledUpdateState, 1)
        XCTAssertEqual(presenterSpy.favoriteState, FavoriteState.favorited)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent.event()))
    }
    
    func testChangeFavoriteState_WhenCalledWithStateUnavailable_ShouldCallPresenterUpdateStateWithUnavailable() {
        favoriteMock.currentState = .unavailable
        sut.changeFavoriteState(.unavailable)
        
        XCTAssertEqual(presenterSpy.calledUpdateState, 1)
        XCTAssertEqual(presenterSpy.favoriteState, FavoriteState.unavailable)
    }
    
    func testShareEstablishment_WhenCalledWithPreviousLoadDetail_ShouldCallPresentShareEstablishmentAndTrackEvent() {
        //Given
        serviceMock.isTestingSuccess = true
        serviceMock.sellerTitle = "Outback"
        serviceMock.userName = "Carol"
        serviceMock.sellerTracking = PromotionSeller.Tracking(
            type: "Biz",
            title: "Outback",
            sellerId: 0001,
            storeId: 232,
            section: "promotion"
        )
        favoriteMock.currentState = .unavailable
        sut.loadDetails()
        
        //When
        sut.shareEstablishment()
        
        //Then
        let expectedMessage = "Carol indicou um local para você visitar! Confira as promoções ao pagar com PicPay no Outback. https://app.picpay.com/promotionsdetails/0001"
        XCTAssertEqual(presenterSpy.shareMessageText, expectedMessage)
        
        let expectedEvent = PromotionsEvent.promotionShare(
            type: serviceMock.sellerTracking.type,
            itemName: serviceMock.sellerTracking.title,
            sellerId: serviceMock.sellerTracking.sellerId
        )
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent.event()))
    }
}
