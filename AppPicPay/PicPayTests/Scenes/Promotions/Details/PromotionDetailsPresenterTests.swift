import XCTest
@testable import PicPay

private class PromotionDetailsDisplaySpy: PromotionDetailsDisplay {

    // MARK: - Show Promotion Data
    private(set) var showDetailsCallsCount = 0
    private(set) var showDetailsReceivedData: PromotionDetailsDisplayData?

    func show(details data: PromotionDetailsDisplayData) {
        showDetailsCallsCount += 1
        showDetailsReceivedData = data
    }

    // MARK: - Show Error Data
    private(set) var showErrorCallsCount = 0
    private(set) var showErrorReceivedData: ErrorData?

    func show(error data: ErrorData) {
        showErrorCallsCount += 1
        showErrorReceivedData = data
    }

    // MARK: - Show Activity View
    private(set) var showShareCallsCount = 0
    private(set) var showShareReceivedActivity: UIActivityViewController?

    func show(share activity: UIActivityViewController) {
        showShareCallsCount += 1
        showShareReceivedActivity = activity
    }

    // MARK: - UpdateState
    private(set) var updateStateCallsCount = 0
    private(set) var updateStateReceived: FavoriteState?

    func updateState(_ state: FavoriteState) {
        updateStateCallsCount += 1
        updateStateReceived = state
    }

    // MARK: - WhatsAppButton
    private(set) var whatsAppButtonIsHiddenCallsCount = 0
    private(set) var whatsAppButtonIsHiddenReceived: Bool?

    func whatsAppButton(isHidden: Bool) {
        whatsAppButtonIsHiddenCallsCount += 1
        whatsAppButtonIsHiddenReceived = isHidden
    }
}

private enum PromotionSellerMock {
    static var map: PromotionSeller.Map?
    
    static func createPromotionSeller() -> PromotionSeller {
        let paymentInfo = PromotionSeller.PaymentInfo(sellerId: 1, storeId: 2)
        
        let sellerTracking = PromotionSeller.Tracking(
            type: "biz",
            title: "Loja",
            sellerId: 1,
            storeId: 2,
            section: ""
        )
        
        let seller = PromotionSeller(
            title: "Loja",
            address: "Rua Serra do Japi",
            phone: "(11) 3524-3057",
            imageUrl: URL(string: "imagem"),
            description: "Loja de Teste",
            items: [],
            paymentInfo: paymentInfo,
            tracking: sellerTracking,
            map: PromotionSellerMock.map ?? nil
        )
        
        return seller
    }
}

final class PromotionDetailsPresenterTests: XCTestCase {
    private lazy var displaySpy = PromotionDetailsDisplaySpy()
    
    private lazy var sut: PromotionDetailsPresenter = {
        let sut = PromotionDetailsPresenter()
        sut.display = displaySpy
        return sut
    }()
    
    func testShowDetails_WhenCalled_ShouldDisplayShowPromotionDetailsDisplayData() {
        let seller = PromotionSellerMock.createPromotionSeller()
        var items = seller.items.map { PromotionDetailsDisplayData.Item.info($0) }
        if let map = seller.map {
            items.append(.map(title: seller.title, map: map))
        }

        let expectedPromotionDetailsDisplayData = PromotionDetailsDisplayData(
            title: seller.title,
            description: seller.description,
            address: seller.address,
            items: items,
            imageUrl: seller.imageUrl,
            phone: seller.phone
        )

        sut.show(details: seller)

        XCTAssertEqual(displaySpy.showDetailsCallsCount, 1)
        XCTAssertEqual(displaySpy.showDetailsReceivedData, expectedPromotionDetailsDisplayData)
    }
    
    func testShowDetails_WhenCalledWithMap_ShouldDisplayShowDataWithMap() {
        let expectedLatitude = -23.5410837
        let expectedLongitude = -46.733894
        let map = PromotionSeller.Map(latitude: expectedLatitude, longitude: expectedLongitude, pinUrl: nil)
        PromotionSellerMock.map = map
        let seller = PromotionSellerMock.createPromotionSeller()
        
        let expectedPromotionDetailsDisplayData = PromotionDetailsDisplayData(
            title: seller.title,
            description: seller.description,
            address: seller.address,
            items: [PromotionDetailsDisplayData.Item.map(title: seller.title, map: map)],
            imageUrl: seller.imageUrl,
            phone: seller.phone
        )
        
        sut.show(details: seller)
        
        XCTAssertEqual(displaySpy.showDetailsCallsCount, 1)
        XCTAssertEqual(displaySpy.showDetailsReceivedData, expectedPromotionDetailsDisplayData)
    }
    
    func testShowError_WhenCalledWithApiErrorConnectionFailure_ShouldDisplayShowErrorWithNoInternetConnectionError() {
        let expectedError = PromotionListDisplay.ErrorData(
            image: Assets.iluSearchErrorCon.image,
            content: (
                title: Strings.Promotions.noInternetConnectionError,
                description: Strings.Promotions.checkYourInternetConnection
            ),
            button: (
                image: Assets.Icons.icoRefresh.image,
                title: DefaultLocalizable.tryAgain.text
            )
        )
        
        sut.show(error: .connectionFailure)
        
        XCTAssertEqual(displaySpy.showErrorCallsCount, 1)
        XCTAssertEqual(displaySpy.showErrorReceivedData?.image, expectedError.image)
        XCTAssertEqual(displaySpy.showErrorReceivedData?.content.title, expectedError.content.title)
        XCTAssertEqual(displaySpy.showErrorReceivedData?.content.description, expectedError.content.description)
        XCTAssertEqual(displaySpy.showErrorReceivedData?.button.image, expectedError.button.image)
        XCTAssertEqual(displaySpy.showErrorReceivedData?.button.title, expectedError.button.title)
    }
    
    func testShowError_WhenCalled_ShouldDisplayShowError() {
        let expectedError = PromotionListDisplay.ErrorData(
            image: Assets.CreditPicPay.Registration.sad3.image,
            content: (
                title: Strings.Promotions.unknownErrorTitle,
                description: Strings.Promotions.weAreDownError
            ),
            button: (
                image: Assets.Icons.icoRefresh.image,
                title: DefaultLocalizable.tryAgain.text
            )
        )
        
        sut.show(error: .bodyNotFound)
        
        XCTAssertEqual(displaySpy.showErrorCallsCount, 1)
        XCTAssertEqual(displaySpy.showErrorReceivedData?.image, expectedError.image)
        XCTAssertEqual(displaySpy.showErrorReceivedData?.content.title, expectedError.content.title)
        XCTAssertEqual(displaySpy.showErrorReceivedData?.content.description, expectedError.content.description)
        XCTAssertEqual(displaySpy.showErrorReceivedData?.button.image, expectedError.button.image)
        XCTAssertEqual(displaySpy.showErrorReceivedData?.button.title, expectedError.button.title)
    }
    
    func testCallPhone_WhenCalled_ShouldCallPhoneAction() {
        let expectation = XCTestExpectation()
        let expectedPhone = "(11) 1212-1212"
        
        sut.callPhoneAction = { phone in
            XCTAssertEqual(phone, expectedPhone)
            expectation.fulfill()
        }
        
        sut.callPhone(expectedPhone)
        
        wait(for: [expectation], timeout: 0.5)
    }
    
    func testPerformPayment_WhenCalled_ShouldPerformPaymentAction() {
        let expectation = XCTestExpectation()
        let expectedSeller = PromotionSellerMock.createPromotionSeller()
        
        sut.performPaymentAction = { promotionSeller in
            XCTAssertEqual(promotionSeller, expectedSeller)
            expectation.fulfill()
        }
        
        sut.performPayment(seller: expectedSeller)
        
        wait(for: [expectation], timeout: 0.5)
    }
    
    func testOpenMap_WhenCalled_ShouldOpenMapAction() {
        let expectation = XCTestExpectation()
        let expectedLatitude = -23.5410837
        let expectedLongitude = -46.733894
        let expectedSellerTitle = "Nome da Loja"
        
        sut.openMapAction = { mapData in
            XCTAssertEqual(mapData.latitude, expectedLatitude)
            XCTAssertEqual(mapData.longitude, expectedLongitude)
            XCTAssertEqual(mapData.title, expectedSellerTitle)
            expectation.fulfill()
        }
        
        sut.openMap(title: expectedSellerTitle, latitude: expectedLatitude, longitude: expectedLongitude)
        
        wait(for: [expectation], timeout: 0.5)
    }

    func testOpenWhatsApp_WhenCalled_ShouldOpenWhatsAppAction() {
        let expectation = XCTestExpectation()
        let expectedPhone = "+5511912345678"
        let expectedText = "Olá, NomeDoSeller! Encontrei vocês pelo app PicPay e gostaria de tirar dúvidas."
        
        sut.openWhatsAppAction = { whatsAppSeller in
            XCTAssertEqual(whatsAppSeller.phone, expectedPhone)
            XCTAssertEqual(whatsAppSeller.text, expectedText)
            expectation.fulfill()
        }
        
        sut.openWhatsApp(whatsapp: WhatsAppSeller(phone: expectedPhone, text: expectedText))
        
        wait(for: [expectation], timeout: 0.5)
    }

    func testUpdateState_WhenCalled_ShouldDisplayUpdateStateFavorite() {
        let expectedFavoriteState = FavoriteState.favorited
        
        sut.updateState(expectedFavoriteState)
        
        XCTAssertEqual(displaySpy.updateStateCallsCount, 1)
        XCTAssertEqual(displaySpy.updateStateReceived, expectedFavoriteState)
    }

    func testPresentWhatsAppButton_WhenCalled_ShouldDisplayWhatsAppButton() {
        let expectedIsHidden = false
        
        sut.presentWhatsAppButton(isHidden: expectedIsHidden)
        
        XCTAssertEqual(displaySpy.whatsAppButtonIsHiddenCallsCount, 1)
        XCTAssertEqual(displaySpy.whatsAppButtonIsHiddenReceived, expectedIsHidden)
    }
}
