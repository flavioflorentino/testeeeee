import UI
import XCTest

@testable import PicPay

final class PromotionListPresenterTests: XCTestCase {
    func testShouldTellToDisplayShowEmptyStateWhenListNoItems() {
        let display = FakeDisplay()

        let presenter = PromotionListPresenter()
        presenter.display = display

        presenter.list([])

        XCTAssertEqual(display.state, .empty)
        XCTAssertEqual(display.showEmptyStateCount, 1)
    }
}

private final class FakeDisplay: PromotionListPresenter.Display {
    func show(error data: ErrorData) {
    }

    func list(_ promotions: [Promotions]) {
    }

    func showLocationWarning() {
    }

    func hideLocationWarning() {
    }

    enum State {
        case normal
        case empty
    }

    private (set) var state: State = .normal
    private (set) var showEmptyStateCount = 0
    private (set) var hideEmptyStateCount = 0
    func showEmptyState() {
        state = .empty
        showEmptyStateCount += 1
    }

    func hideEmptyState() {
        state = .normal
        hideEmptyStateCount += 1
    }
}
