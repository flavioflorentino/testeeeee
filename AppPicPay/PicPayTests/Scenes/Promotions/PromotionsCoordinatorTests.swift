import Core
import UIKit
import XCTest

@testable import PicPay

final class PromotionsCoordinatorTests: XCTestCase {
    private let analyticsMock = AnalyticsLoggerMock()
    private let kvStoreMock = KVStoreMock()
    private let factoryMock = FakeFactory()

    private let navigationControllerMock = NavigationControllerMock(
        rootViewController: UIViewController(nibName: nil, bundle: nil)
    )

    private lazy var sut = PromotionsCoordinator(
        navigationController: navigationControllerMock,
        controllerFactory: factoryMock,
        coordinatorFactory: factoryMock,
        dependencies: DependencyContainerMock(analyticsMock, kvStoreMock)
    )

    func testStart_WhenCalled_ShouldPresentPromotionListScreen() {
        sut.start()

        XCTAssertEqual(navigationControllerMock.pushedViewController, factoryMock.promotionListController)
    }

    func testStart_WhenCalledAndWasNotShownOnboardingBefore_ShouldShowIt() {
        let event = PromotionsUserDefaultKey.alreadyShownOnboading.rawValue
        kvStoreMock.set(value: false, with: event)

        sut.start()

        XCTAssertEqual(navigationControllerMock.viewControllerPresented, factoryMock.onboardingController)
        XCTAssertTrue(kvStoreMock.getFirstTimeOnlyEvent(event))
    }

    func testStart_WhenCalledAndAlreadyShownOnboarding_ShouldNotShowIt() {
        let event = PromotionsUserDefaultKey.alreadyShownOnboading.rawValue
        kvStoreMock.set(value: true, with: event)

        sut.start()

        XCTAssertEqual(navigationControllerMock.viewControllerPresented, nil)
    }
    
    func testStart_WhenCalled_ShouldPresentPromotionNewListScreen() {
        //todo next PR
    }

    func testDidFinishFlow_WhenPopedLasController_ShouldCalledIt() {
        var calledDidFinishFlow = false
        sut.didFinishFlow = {
            calledDidFinishFlow = true
        }

        sut.start()
        navigationControllerMock.tapAtBackBarButton()

        XCTAssertTrue(calledDidFinishFlow)
    }
}

private final class FakeFactory: PromotionsCoordinator.ControllerFactory, PromotionsCoordinator.CoordinatorFactory {
    func makeDetailsCoordinator(
        seller: PromotionDetailsCoordinating.Seller,
        navigationController: UINavigationController
    ) -> PromotionDetailsCoordinating {
        return FakePromotionDetailsCoordinator()
    }

    private(set) lazy var promotionNewListController = PromotionListNewViewController(interactor: FakePromotionListNewInteractor())
    private (set) lazy var promotionListController = PromotionListViewController(viewModel: FakePromotionListViewModel())
    private (set) lazy var onboardingController = PromotionsOnboardingViewController(interactor: FakePromotionsOnboardingInteractor())
    private (set) lazy var webViewController = UIViewController(nibName: nil, bundle: nil)

    func makeNewListController(
        promotion: Promotion?,
        openWebView: @escaping (URL?) -> Void,
        openDeepLink: @escaping (URL?) -> Void,
        openOnboarding: @escaping () -> Void,
        openDetails: @escaping (Promotion, PromotionDetailsIDType) -> Void,
        openPromotions: @escaping (Promotion) -> Void
    ) -> PromotionListNewViewController {
        return promotionNewListController
    }
    
    func makeListController(
        openWebView: @escaping (URL?) -> Void,
        openDeepLink: @escaping (URL?) -> Void,
        openOnboarding: @escaping () -> Void,
        openDetails: @escaping (Promotion, PromotionDetailsIDType) -> Void
    ) -> PromotionListViewController
    {
        return promotionListController
    }

    func makeOnboardingController() -> PromotionsOnboardingViewController {
        onboardingController
    }

    func makeWebController(_ url: URL) -> UIViewController {
        webViewController
    }
    
    final class FakePromotionListNewInteractor: PromotionListNewInteracting {
        func doSomething() {
            
        }
    }
    
    final class FakePromotionsOnboardingInteractor: PromotionsOnboardingInteracting {
        func loadView() {
            
        }
        
        func dismissView(from origin: PromotionsEvent.OnboardingCloseOrigin) {
            
        }
    }

    final class FakePromotionListViewModel: PromotionListViewModelInputs {
        func didSelect(_ promotion: Promotion) {
        }

        func loadData() {
        }

        func requestLocationAuthorization(_ tryAgainAction: (() -> Void)?) {
        }

        func openOnboarding() {
        }
    }

    final class FakePromotionDetailsCoordinator: PromotionDetailsCoordinating {
        var didFinishFlow: (() -> Void)?

        var childViewController: [UIViewController] = []
        var viewController: UIViewController?

        func startWithoutShowing() -> UIViewController {
            UIViewController(nibName: nil, bundle: nil)
        }
    }
}
