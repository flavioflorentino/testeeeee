@testable import PicPay
import Core
import XCTest

private final class QrCodeServiceMock: QrCodeServicing {
    private(set) var generateQrCodeCallsCount = 0
    var consumerImageUrl: String?
    private(set) var amount: String?
    
    var generateQrCodeResult: Result<QrCodeResponse, ApiError> = .failure(.unknown(nil))
    func generateQrCode(amount: String?, completion: @escaping (Result<QrCodeResponse, ApiError>) -> Void) {
        generateQrCodeCallsCount += 1
        completion(generateQrCodeResult)
        self.amount = amount
    }
}

private final class QrCodeDelegateSpy: QrCodeDelegate{
    private(set) var startRequestCallsCount = 0
    private(set) var finishRequestWithSuccessCallsCount = 0
    private(set) var finishRequestWithoutSuccessCallsCount = 0
    func startRequest() {
        startRequestCallsCount += 1
    }
    
    func finishRequest(isSuccess: Bool) {
        guard isSuccess else {
            finishRequestWithoutSuccessCallsCount += 1
            return
        }
        finishRequestWithSuccessCallsCount += 1
    }
}

private final class QrCodePresenterSpy: QrCodePresenting {
    private(set) var presentLogoImageViewInsetsCallsCount = 0
    private(set) var presentConsumerImageCallsCount = 0
    private(set) var presentQrCodeImageCallsCount = 0
    private(set) var inset: CGFloat?
    private(set) var url: URL?
    private(set) var image: UIImage?
    
    func presentLogoImageViewInsets(_ inset: CGFloat) {
        presentLogoImageViewInsetsCallsCount += 1
        self.inset = inset
    }
    
    func presentConsumerImage(url: URL) {
        presentConsumerImageCallsCount += 1
        self.url = url
    }
    
    func presentQrCodeImage(_ image: UIImage) {
        presentQrCodeImageCallsCount += 1
        self.image = image
    }
}

final class QrCodeInteractorTests: XCTestCase {
    // MARK: - Variables
    let qrCodeSize = CGSize(width: UIScreen.main.bounds.width * (3 / 5), height: UIScreen.main.bounds.width * (3 / 5))
    
    private let delegateMock = QrCodeDelegateSpy()
    private var service = QrCodeServiceMock()
    private let presenter = QrCodePresenterSpy()
    private lazy var interector = QrCodeInteractor(presenter: presenter, service: service, size: qrCodeSize, delegate: delegateMock)
    
    //MARK: - GenerateQrCode
    func testGenerateQrCode_WhenAmountIsNotNull_ShouldServiceReciveAmountWithTwoDecimals() {
        let amount = 2.0
        interector.generateQrCode(amount: amount)
        XCTAssertEqual(service.amount, String(format: "%.2f", amount))
    }
    
    func testGenerateQrCode_WhenAmountIsNull_ShouldServiceNotReciveReturnAmount() {
        interector.generateQrCode(amount: nil)
        XCTAssertNil(service.amount)
    }
    
    func testGenerateQrCode_WhenResultIsSuccessAndValidUrlImage_ShouldCallStartRequestAndPresentLogoImageViewInsetsAndPresentConsumerImageAndPresentQrCodeImageAndFinishRequestWithSuccess() {
        service.consumerImageUrl = "Test"
        service.generateQrCodeResult = .success(.init(text: ""))
        interector.generateQrCode(amount: nil)
        XCTAssertEqual(service.generateQrCodeCallsCount, 1)
        XCTAssertEqual(delegateMock.startRequestCallsCount, 1)
        XCTAssertEqual(presenter.presentLogoImageViewInsetsCallsCount, 1)
        XCTAssertEqual(presenter.presentConsumerImageCallsCount, 1)
        XCTAssertEqual(presenter.presentQrCodeImageCallsCount, 1)
        XCTAssertEqual(delegateMock.finishRequestWithSuccessCallsCount, 1)
    }
    
    func testGenerateQrCode_WhenResultIsSuccessAndInvalidUrlImage_ShouldCallStartRequestAndPresentLogoImageViewInsetsAndPresentConsumerImageAndPresentQrCodeImageAndFinishRequestWithSuccess() {
        service.generateQrCodeResult = .success(.init(text: ""))
        interector.generateQrCode(amount: nil)
        XCTAssertEqual(service.generateQrCodeCallsCount, 1)
        XCTAssertEqual(delegateMock.startRequestCallsCount, 1)
        XCTAssertEqual(presenter.presentLogoImageViewInsetsCallsCount, 1)
        XCTAssertEqual(presenter.presentConsumerImageCallsCount, 0)
        XCTAssertEqual(presenter.presentQrCodeImageCallsCount, 1)
        XCTAssertEqual(delegateMock.finishRequestWithSuccessCallsCount, 1)
    }
    
    func testGenerateQrCode_WhenResultIsNotSuccess_ShouldCallStartRequestAndFinishRequestWithFailed() {
        interector.generateQrCode(amount: .zero)
        XCTAssertEqual(delegateMock.startRequestCallsCount, 1)
        XCTAssertEqual(delegateMock.finishRequestWithoutSuccessCallsCount, 1)
    }
}
