@testable import PicPay
import XCTest

private final class QrCodeViewSpy: QrCodeDisplay {
    private(set) var setupLogoImageViewInsetsCallsCount = 0
    private(set) var displayUserImageCallsCount = 0
    private(set) var displayQrCodeImageCallsCount = 0
    
    func setupLogoImageViewInsets(_ inset: CGFloat) {
        setupLogoImageViewInsetsCallsCount += 1
    }
    
    func displayUserImage(url: URL) {
        displayUserImageCallsCount += 1
    }
    
    func displayQrCodeImage(_ image: UIImage) {
        displayQrCodeImageCallsCount += 1
    }
}

final class QrCodePresenterTests: XCTestCase {
    // MARK: - Variables
    private let view = QrCodeViewSpy()
    private lazy var presenter: QrCodePresenting = {
        let presenter = QrCodePresenter()
        presenter.view = view
        return presenter
    }()

    // MARK: - presentLogoImageViewInsets
    func testPresentLogoImageViewInsets_ShouldCallSetupLogoImageViewInsets() {
        presenter.presentLogoImageViewInsets(1)
        XCTAssertEqual(view.setupLogoImageViewInsetsCallsCount, 1)
    }

    // MARK: - presentConsumerImage
    func testPresentConsumerImage_ShouldCallDisplayUserImage() throws {
        let url = try XCTUnwrap(URL(string: "test"))
        presenter.presentConsumerImage(url: url)
        XCTAssertEqual(view.displayUserImageCallsCount, 1)
    }
    //MARK: - presentQrCodeImage
    func testPresentQrCodeImage_ShouldCallDisplayQrCodeImage() {
        presenter.presentQrCodeImage(UIImage())
        XCTAssertEqual(view.displayQrCodeImageCallsCount, 1)
    }
}
