@testable import PicPay
import XCTest

class FakeMyCodeViewController: MyCodeViewModelOutputs {
    var didSetUpQrCode = false
    var paymentLink: String?
    var consumerImageUrl: String?
    
    var didSetUpUsernameLabel = false
    var usernameLabelText: String?
    
    var didShowActionSheet = false
    var imageActivity: UIActivityViewController?
    var linkActivity: UIActivityViewController?
    
    var performedActivity: MyCodeAction?
    private(set) var displayQrCodeSuccessCallsCount = 0
    private(set) var displayQrCodeErrorCallsCount = 0
    
    func setUpQrCode(paymentLink: String, consumerImageUrl: String?) {
        didSetUpQrCode = true
        self.paymentLink = paymentLink
        self.consumerImageUrl = consumerImageUrl
    }
    
    func setUpUsernameLabel(username: String) {
        didSetUpUsernameLabel = true
        usernameLabelText = username
    }
    
    func showShareActionSheet(imageActivity: UIActivityViewController, linkActivity: UIActivityViewController) {
        didShowActionSheet = true
        self.imageActivity = imageActivity
        self.linkActivity = linkActivity
    }
    
    func perform(action: MyCodeAction) {
        performedActivity = action
    }
    
    func displayQrCodeSuccess() {
        displayQrCodeSuccessCallsCount += 1
    }
    
    func displayQrCodeError() {
        displayQrCodeErrorCallsCount += 1
    }
}

class FakeMyCodeService: MyCodeServiceProtocol {
    var username: String?
    var consumerImageUrl: String?
}

class MyCodeViewModelTest: XCTestCase {
    var service: FakeMyCodeService!
    var viewModel: MyCodeViewModel!
    var controller: FakeMyCodeViewController!
    
    override func setUp() {
        super.setUp()
        service = FakeMyCodeService()
        viewModel = MyCodeViewModel(service: service)
        controller = FakeMyCodeViewController()
        viewModel.outputs = controller
    }
    
    func testViewDidLoadSuccess() {
        service.username = "fulano"
        service.consumerImageUrl = "some_url"
        viewModel.viewDidLoad()
        XCTAssert(controller.didSetUpUsernameLabel)
        XCTAssertEqual(controller.usernameLabelText, "@fulano")
    }
    
    func testViewDidLoadNoUsername() {
        service.username = nil
        service.consumerImageUrl = "some_url"
        viewModel.viewDidLoad()
        XCTAssertFalse(controller.didSetUpUsernameLabel)
        XCTAssertFalse(controller.didSetUpQrCode)
    }
    
    func testDidTapChargeUser() {
        viewModel.didTapChargeUser()
        XCTAssertEqual(controller.performedActivity, .chargeUser)
    }
    
    func testDidTapShareCode() {
        service.username = "fulano"
        service.consumerImageUrl = "some_url"
        
        let qrCodeImage = UIImage()
        let userImage = UIImage()
        viewModel.didTapShareCode(withQrCode: qrCodeImage, userImage: userImage)
        XCTAssert(controller.didShowActionSheet)
        XCTAssertNotNil(controller.imageActivity)
        XCTAssertNotNil(controller.linkActivity)
    }
    
    func testDidTapShareCodeNoUserImage() {
        service.username = "fulano"
        service.consumerImageUrl = "some_url"
        
        let qrCodeImage = UIImage()
        viewModel.didTapShareCode(withQrCode: qrCodeImage, userImage: nil)
        XCTAssert(controller.didShowActionSheet)
        XCTAssertNotNil(controller.imageActivity)
        XCTAssertNotNil(controller.linkActivity)
    }
    
    func testDidTapShareCodeNoUsername() {
        service.username = nil
        service.consumerImageUrl = "some_url"
        
        let qrCodeImage = UIImage()
        let userImage = UIImage()
        viewModel.didTapShareCode(withQrCode: qrCodeImage, userImage: userImage)
        XCTAssertFalse(controller.didShowActionSheet)
    }
    
    func testDidTapShareNoQrCode() {
        service.username = "fulano"
        service.consumerImageUrl = "some_url"
        
        let userImage = UIImage()
        viewModel.didTapShareCode(withQrCode: nil, userImage: userImage)
        XCTAssertFalse(controller.didShowActionSheet)
    }
    
    func testHandleQrCodeResponse_WhenIsSuccess_ShouldCallDisplayQrCodeSuccess() {
        viewModel.handleQrCodeResponse(isSuccess: true)
        XCTAssertEqual(controller.displayQrCodeSuccessCallsCount, 1)
    }
    
    func testHandleQrCodeResponse_WhenIsNotSuccess_ShouldCallDisplayQrCodeError() {
        viewModel.handleQrCodeResponse(isSuccess: false)
        XCTAssertEqual(controller.displayQrCodeErrorCallsCount, 1)
    }
}
