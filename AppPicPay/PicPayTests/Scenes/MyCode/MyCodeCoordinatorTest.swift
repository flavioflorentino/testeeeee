@testable import PicPay
import XCTest

final class MyCodeCoordinatorTest: XCTestCase {
    private let viewController = ViewControllerMock()
    private lazy var sut: MyCodeCoordinating = {
        let coordinator = MyCodeCoordinator()
        coordinator.viewController = viewController
        return coordinator
    }()
    
    func testPerform_WhenActionIsChargeUser_ShouldOpenPaymentRequestV2Flow() throws {
        sut.perform(action: .chargeUser)
        
        XCTAssertEqual(viewController.didPresentControllerCount, 1)
        let presentedNavController = try XCTUnwrap(viewController.viewControllerPresented as? UINavigationController)
        XCTAssertTrue(presentedNavController.topViewController is PaymentRequestSelectorDisplay)
    }
}
