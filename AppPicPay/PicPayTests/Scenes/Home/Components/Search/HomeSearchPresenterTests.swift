import XCTest

@testable import PicPay

final class HomeSearchPresenterTests: XCTestCase {
    // MARK: - Properties
    private lazy var sut = HomeSearchPresenter()

    // MARK: - Tests
    func testOpenSearch_WhenCalled_ShouldExecuteOpenSearchAction() {
        var openSearchActionCount: Int = 0

        sut.openSearchAction = {
            openSearchActionCount += 1
        }

        sut.openSearch()

        XCTAssertEqual(openSearchActionCount, 1)
    }
}
