import XCTest

@testable import PicPay

final class HomeSearchPresenterMock: MockVerifier {
    // MARK: - Properties
    private var openSearchCount: Int = 0

    // MARK: - Verifiers
    func verifyOpenSearch(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "openSearch", callCount: openSearchCount, file: file, line: line)
    }
}

// MARK: - HomeFavoritesPresenting
extension HomeSearchPresenterMock: HomeSearchPresenting {
    func openSearch() {
        openSearchCount += 1
    }
}
