import XCTest

@testable import PicPay

final class HomeSearchInteractorTests: XCTestCase {
    private lazy var presenterMock = HomeSearchPresenterMock()

    private lazy var sut = HomeSearchInteractor(presenter: presenterMock)

    func testTapAtSearch_WhenCalled_ShouldCallOpenSearchOnPresenter() {
        sut.tapAtSearch()

        presenterMock.verifyOpenSearch()
    }
}
