import XCTest
import Core

@testable import PicPay

final class HomeSuggestionsPresenterTests: XCTestCase {
    // MARK: - Properties
    private lazy var displayMock = HomeSuggestionsDisplayMock()

    private lazy var sut: HomeSuggestionsPresenter = {
        let sut = HomeSuggestionsPresenter()
        sut.display = displayMock

        return sut
    }()

    // MARK: - Tests
    func testPresentError_WhenCalled_ShouldBeIgnoredAndCallHideLoadingOnDisplay() {
        sut.present(error: .unknown(nil))

        displayMock.verifyHideLoading()
    }

    func testPresenSuggestions_WhenCalled_ShouldCallShowSuggestionsOnDisplay() {
        let suggestions = makeSection()
        sut.present(suggestions: suggestions)

        displayMock.verifyShow(suggestions: suggestions)
    }

    func testOpenPayment_WhenCalled_ShouldInvokeOpenPaymentAction() {
        var openPaymentActionCount: Int = 0
        var paymentData: HomeSuggestionsPresenter.PaymentData?

        sut.openPaymentAction = {
            openPaymentActionCount += 1
            paymentData = $0
        }

        let section = makeSection()
        let row = section.rows[0]

        sut.openPayment(section: section, row: row)

        XCTAssertEqual(openPaymentActionCount, 1)
        XCTAssertEqual(paymentData?.section.id, section.id)
        XCTAssertEqual(paymentData?.row.type, row.type)
    }

    func testShowLoading_WhenCalled_ShouldCallShowLoadingOnDisplay() {
        sut.showLoading()
        displayMock.verifyShowLoading()
    }

    func testHideLoading_WhenCalled_ShouldCallHideLoadingOnDisplay() {
        sut.hideLoading()
        displayMock.verifyHideLoading()
    }
}

// MARK: - Private
private extension HomeSuggestionsPresenterTests {
    func makeSection() -> LegacySearchResultSection {
        let section = LegacySearchResultSection(viewType: .list, rows: makeRows())
        section.id = "\(Int.random(in: 0 ... 100))"
        section.title = "Test"
        section.style = .normal

        return section
    }

    func makeRows(count: Int = 10) -> [LegacySearchResultRow] {
        (0 ... count).map { _ in makeRow() }
    }

    func makeRow() -> LegacySearchResultRow {
        LegacySearchResultRow(type: .person, fullData: NSObject())
    }
}
