import AnalyticsModule
import Core
import Foundation
import XCTest

@testable import PicPay

final class HomeSuggestionsInteractorTests: XCTestCase {
    // MARK: - Properties
    private lazy var analyticsMock = AnalyticsSpy()
    private lazy var serviceStub = SuggestionsServiceStub()
    private lazy var presenterMock = HomeSuggestionsPresenterMock()

    private lazy var sut = HomeSuggestionsInteractor(
        presenter: presenterMock,
        service: serviceStub,
        dependencies: DependencyContainerMock(analyticsMock)
    )

    // MARK: - Tests
    func testFetchSuggestions_WhenServiceReturnsSuccess_ShouldCallPresentSuggestionsOnPresenter() {
        let suggestions = makeSection()
        serviceStub.suggestionsResult = .success(suggestions)

        sut.fetchSuggestions()

        presenterMock.verifyShowLoading()
        presenterMock.verifyHideLoading()
        presenterMock.verifyPresent(suggestions: suggestions)

        XCTAssertEqual(suggestions.style, .home)
    }

    func testFetchSuggestions_WhenServiceReturnsFailure_ShouldCallPresentErrorOnPresenter() {
        let error: ApiError = .bodyNotFound
        serviceStub.suggestionsResult = .failure(error)

        sut.fetchSuggestions()

        presenterMock.verifyShowLoading()
        presenterMock.verifyHideLoading()
        presenterMock.verifyPresent(error: error)
    }

    func testDidSelect_WhenCalled_ShouldCallOpenPaymentOnPresenter() {
        let section = makeSection()
        let row = section.rows[0]

        sut.didSelect(section: section, row: row)

        presenterMock.verifyOpenPayment(section: section, row: row)
    }

    func testDidSelect_WhenRowTypeIsPerson_ShouldLogProfileEvent() {
        let section = makeSection()
        let row = section.rows[0]

        sut.didSelect(section: section, row: row)

        let expectedEvent = ProfileEvent.touchPicture(from: .suggestions).event()
        XCTAssertEqual(analyticsMock.events.contains(where: { equals(analyticsEvent: $0, other: expectedEvent) }), true)
    }

    func testDidSelect_WhenCalled_ShouldLogItemAccessedEvent() {
        let section = makeSection()
        let row = section.rows[0]

        sut.didSelect(section: section, row: row)

        let expectedEvent = PaymentEvent.itemAccessed(
            type: row.type.rawValue,
            id: row.basicData.id ?? "",
            name: row.basicData.title ?? "",
            section: (name: section.title ?? "", position: 0)
        ).event()

        XCTAssertEqual(analyticsMock.events.contains(where: { equals(analyticsEvent: $0, other: expectedEvent) }), true)
    }
}

// MARK: - Private
private extension HomeSuggestionsInteractorTests {
    func makeSection() -> LegacySearchResultSection {
        let section = LegacySearchResultSection(viewType: .list, rows: makeRows())
        section.id = "\(Int.random(in: 0 ... 100))"
        section.title = "Test"
        section.style = .normal

        return section
    }

    func makeRows(count: Int = 10) -> [LegacySearchResultRow] {
        (0 ... count).map { _ in makeRow() }
    }

    func makeRow() -> LegacySearchResultRow {
        LegacySearchResultRow(type: .person, fullData: NSObject())
    }

    func equals(analyticsEvent event: AnalyticsEventProtocol, other: AnalyticsEventProtocol) -> Bool {
        event.name == other.name
            && NSDictionary(dictionary: event.properties) == NSDictionary(dictionary: other.properties)
            && event.providers == other.providers
    }
}
