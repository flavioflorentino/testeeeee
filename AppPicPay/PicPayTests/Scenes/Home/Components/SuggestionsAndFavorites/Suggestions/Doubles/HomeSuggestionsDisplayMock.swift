import XCTest

@testable import PicPay

final class HomeSuggestionsDisplayMock: MockVerifier {
    // MARK: - Properties
    private var showSuggestionsCount: Int = 0
    private var suggestions: LegacySearchResultSection?

    private var showLoadingCount: Int = 0
    private var hideLoadingCount: Int = 0

    // MARK: - Verifiers
    func verifyShow(
        suggestions expectedSuggestions: LegacySearchResultSection,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        guard verifyMethodCalledOnce(
                methodName: "show",
                callCount: showSuggestionsCount,
                describeArguments: "suggestions: \(String(describing: suggestions))",
                file: file,
                line: line) else {
            return
        }

        assert(section: suggestions, expected: expectedSuggestions, file: file, line: line)
    }

    func verifyShowLoading(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "showLoading", callCount: showLoadingCount, file: file, line: line)
    }

    func verifyHideLoading(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "hideLoading", callCount: hideLoadingCount, file: file, line: line)
    }
}

// MARK: - HomeSuggestionsDisplay
extension HomeSuggestionsDisplayMock: HomeSuggestionsDisplay {
    func show(suggestions: LegacySearchResultSection) {
        showSuggestionsCount += 1
        self.suggestions = suggestions
    }

    func showLoading() {
        showLoadingCount += 1
    }

    func hideLoading() {
        hideLoadingCount += 1
    }
}

// MARK: - Private
private extension HomeSuggestionsDisplayMock {
    func assert(
        section: LegacySearchResultSection?,
        expected: LegacySearchResultSection?,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        XCTAssertEqual(section?.id, expected?.id, file: file, line: line)
        XCTAssertEqual(section?.title, expected?.title, file: file, line: line)
        XCTAssertEqual(section?.type?.rawValue, expected?.type?.rawValue, file: file, line: line)
    }
}
