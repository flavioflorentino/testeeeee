import Core

@testable import PicPay

final class SuggestionsServiceStub {
    var suggestionsResult: Result<LegacySearchResultSection, ApiError>?
}

// MARK: - SuggestionsServicing
extension SuggestionsServiceStub: SuggestionsServicing {
    func suggestions(completion: @escaping (Result<LegacySearchResultSection, ApiError>) -> Void) {
        guard let result = suggestionsResult else { return completion(.failure(.unknown(nil))) }
        completion(result)
    }
}
