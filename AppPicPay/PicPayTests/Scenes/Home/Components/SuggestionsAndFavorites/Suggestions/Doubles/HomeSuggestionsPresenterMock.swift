import Core
import XCTest

@testable import PicPay

final class HomeSuggestionsPresenterMock: MockVerifier {
    // MARK: - Properties
    private var presentSuggestionsCount: Int = 0
    private var suggestions: LegacySearchResultSection?

    private var presentErrorCount: Int = 0
    private var error: ApiError?

    private var showLoadingCount: Int = 0
    private var hideLoadingCount: Int = 0

    private var openPaymentCount: Int = 0
    private var section: LegacySearchResultSection?
    private var row: LegacySearchResultRow?

    // MARK: - Verifiers
    func verifyPresent(
        suggestions expectedSuggestions: LegacySearchResultSection,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        guard verifyMethodCalledOnce(
                methodName: "present",
                callCount: presentSuggestionsCount,
                describeArguments: "suggestions: \(String(describing: suggestions))",
                file: file,
                line: line) else {
            return
        }

        assert(section: suggestions, expected: expectedSuggestions, file: file, line: line)
    }

    func verifyPresent(error expectedError: ApiError, file: StaticString = #file, line: UInt = #line) {
        guard verifyMethodCalledOnce(
                methodName: "present",
                callCount: presentErrorCount,
                describeArguments: "error: \(String(describing: error))",
                file: file,
                line: line) else {
            return
        }

        XCTAssertEqual(error?.localizedDescription, expectedError.localizedDescription, file: file, line: line)
    }

    func verifyShowLoading(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "showLoading", callCount: showLoadingCount, file: file, line: line)
    }

    func verifyHideLoading(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "hideLoading", callCount: hideLoadingCount, file: file, line: line)
    }

    func verifyOpenPayment(
        section expectedSection: LegacySearchResultSection,
        row expectedRow: LegacySearchResultRow,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        guard verifyMethodCalledOnce(
                methodName: "openPayment",
                callCount: openPaymentCount,
                describeArguments: "section: \(String(describing: section)), row: \(String(describing: row))",
                file: file,
                line: line) else {
            return
        }

        assert(section: section, expected: expectedSection, file: file, line: line)
        assert(row: row, expected: expectedRow, file: file, line: line)
    }
}

// MARK: - HomeSuggestionsPresenting
extension HomeSuggestionsPresenterMock: HomeSuggestionsPresenting {
    func present(suggestions: LegacySearchResultSection) {
        presentSuggestionsCount += 1
        self.suggestions = suggestions
    }

    func present(error: ApiError) {
        presentErrorCount += 1
        self.error = error
    }

    func showLoading() {
        showLoadingCount += 1
    }

    func hideLoading() {
        hideLoadingCount += 1
    }

    func openPayment(section: LegacySearchResultSection, row: LegacySearchResultRow) {
        openPaymentCount += 1
        self.section = section
        self.row = row
    }
}

// MARK: - Private
private extension HomeSuggestionsPresenterMock {
    func assert(
        section: LegacySearchResultSection?,
        expected: LegacySearchResultSection?,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        XCTAssertEqual(section?.id, expected?.id, file: file, line: line)
        XCTAssertEqual(section?.title, expected?.title, file: file, line: line)
        XCTAssertEqual(section?.type?.rawValue, expected?.type?.rawValue, file: file, line: line)
    }

    func assert(
        row: LegacySearchResultRow?,
        expected: LegacySearchResultRow?,
        file: StaticString = #file,
        line: UInt = #line
    ) {
        XCTAssertEqual(row?.type.rawValue, expected?.type.rawValue, file: file, line: line)
        XCTAssertEqual(row?.fullData, expected?.fullData, file: file, line: line)
    }
}
