import Core
import XCTest

@testable import PicPay

final class HomeFavoritesInteractorTests: XCTestCase {
    // MARK: - Properties
    private lazy var serviceStub = FavoritesServiceStub()
    private lazy var presenterMock = HomeFavoritesPresenterMock()

    private lazy var sut = HomeFavoritesInteractor(presenter: presenterMock, service: serviceStub)

    // MARK: - Tests
    func testFetchFavorites_WhenServiceReturnsFailure_ShouldCallPresentErrorOnPresenter() {
        let error: ApiError = .serverError
        serviceStub.homeFavoritesResult = .failure(error)

        sut.fetchFavorites()

        presenterMock.verifyShowLoading()
        presenterMock.verifyHideLoading()
        presenterMock.verifyPresent(error: error)
    }

    func testFetchFavorites_WhenServiceReturnsSuccess_ShouldCallPresentFavoritesOnPresenter() {
        let favorites = makeHomeFavorites()
        serviceStub.homeFavoritesResult = .success(favorites)

        sut.fetchFavorites()

        presenterMock.verifyShowLoading()
        presenterMock.verifyHideLoading()
        presenterMock.verifyPresent(favorites: favorites)
    }

    func testFetchFavorites_WhenServiceReturnsSuccessButNoFavorites_ShouldCallPresentErrorOnPresenter() {
        let favorites: [HomeFavorites] = [HomeFavorites(id: "", title: "", view: "", items: [.unknown(type: "")])]
        serviceStub.homeFavoritesResult = .success(favorites)

        sut.fetchFavorites()

        presenterMock.verifyShowLoading()
        presenterMock.verifyHideLoading()
        presenterMock.verifyPresent(error: .unknown(nil))
    }

    func testDidSetectItem_WhenCalled_ShouldCallOpenItemOnPresenter() {
        let item = makeHomeFavoriteItem()

        sut.didSetect(item: item)

        presenterMock.verifyOpen(item: item)
    }
}

// MARK: - Private
private extension HomeFavoritesInteractorTests {
    func makeHomeFavorites(count: Int = 10) -> [HomeFavorites] {
        (0 ... count).map { _ in
            HomeFavorites(
                id: "\(Int.random(in: 0 ..< 100))",
                title: "Test",
                view: "",
                items: makeHomeFavoritesItems()
            )
        }
    }

    func makeHomeFavoritesItems(count: Int = 10) -> [HomeFavorites.Item] {
        (0 ... count).map { _ in makeHomeFavoriteItem() }
    }

    func makeHomeFavoriteItem() -> HomeFavorites.Item {
        .consumer(
            item: HomeFavorites.ConsumerItem(
                id: "\(Int.random(in: 0 ..< 100))",
                name: "Consumer",
                username: "consumer",
                imageUrlSmall: nil,
                verified: true,
                pro: false
            )
        )
    }
}
