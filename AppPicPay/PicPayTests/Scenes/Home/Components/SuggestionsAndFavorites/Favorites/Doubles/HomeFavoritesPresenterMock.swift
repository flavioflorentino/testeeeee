import Core
import XCTest

@testable import PicPay

final class HomeFavoritesPresenterMock: MockVerifier {
    // MARK: - Properties
    private var presentErrorCount: Int = 0
    private var error: ApiError?

    private var presentFavoritesCount: Int = 0
    private var favorites: [HomeFavorites.Item]?

    private var openItemCount: Int = 0
    private var item: HomeFavorites.Item?

    private var showLoadingCount: Int = 0
    private var hideLoadingCount: Int = 0

    // MARK: - Verifiers
    func verifyPresent(error expectedError: ApiError, file: StaticString = #file, line: UInt = #line) {
        guard verifyMethodCalledOnce(
                methodName: "present",
                callCount: presentErrorCount,
                describeArguments: "error: \(String(describing: error))",
                file: file,
                line: line) else {
            return
        }

        XCTAssertEqual(error?.localizedDescription, expectedError.localizedDescription, file: file, line: line)
    }

    func verifyPresent(favorites expectedFavorites: [HomeFavorites], file: StaticString = #file, line: UInt = #line) {
        guard verifyMethodCalledOnce(
                methodName: "present",
                callCount: presentFavoritesCount,
                describeArguments: "favorites: \(String(describing: favorites))",
                file: file,
                line: line) else {
            return
        }

        XCTAssertEqual(favorites, expectedFavorites.flatMap { $0.items }, file: file, line: line)
    }

    func verifyOpen(item expectedItem: HomeFavorites.Item, file: StaticString = #file, line: UInt = #line) {
        guard verifyMethodCalledOnce(
                methodName: "open",
                callCount: openItemCount,
                describeArguments: "item: \(String(describing: item))",
                file: file,
                line: line) else {
            return
        }

        XCTAssertEqual(item, expectedItem, file: file, line: line)
    }

    func verifyShowLoading(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "showLoading", callCount: showLoadingCount, file: file, line: line)
    }

    func verifyHideLoading(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "hideLoading", callCount: hideLoadingCount, file: file, line: line)
    }
}

// MARK: - HomeFavoritesPresenting
extension HomeFavoritesPresenterMock: HomeFavoritesPresenting {
    func present(error: ApiError) {
        presentErrorCount += 1
        self.error = error
    }

    func present(favorites: [HomeFavorites.Item]) {
        presentFavoritesCount += 1
        self.favorites = favorites
    }

    func open(item: HomeFavorites.Item) {
        openItemCount += 1
        self.item = item
    }

    func showLoading() {
        showLoadingCount += 1
    }

    func hideLoading() {
        hideLoadingCount += 1
    }
}
