import XCTest

@testable import PicPay

final class HomeFavoritesDisplayMock: MockVerifier {
    // MARK: - Properties
    private var showFavoritesCount: Int = 0
    private var favorites: [HomeFavorites.Item]?

    private var showErrorCount: Int = 0
    private var hideErrorCount: Int = 0

    private var showLoadingCount: Int = 0
    private var hideLoadingCount: Int = 0

    // MARK: - Verifiers
    func verifyShow(favorites expectedFavorites: [HomeFavorites.Item], file: StaticString = #file, line: UInt = #line) {
        guard verifyMethodCalledOnce(
                methodName: "show",
                callCount: showFavoritesCount,
                describeArguments: "favorites: \(String(describing: favorites))",
                file: file,
                line: line) else {
            return
        }

        XCTAssertEqual(favorites, expectedFavorites, file: file, line: line)
    }

    func verifyShowError(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "showError", callCount: showErrorCount, file: file, line: line)
    }

    func verifyHideError(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "hideError", callCount: hideErrorCount, file: file, line: line)
    }

    func verifyShowLoading(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "showLoading", callCount: showLoadingCount, file: file, line: line)
    }

    func verifyHideLoading(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "hideLoading", callCount: hideLoadingCount, file: file, line: line)
    }
}

// MARK: - HomeFavoritesDisplay
extension HomeFavoritesDisplayMock: HomeFavoritesDisplay {
    func show(favorites: [HomeFavorites.Item]) {
        showFavoritesCount += 1
        self.favorites = favorites
    }

    func showError() {
        showErrorCount += 1
    }

    func hideError() {
        hideErrorCount += 1
    }

    func showLoading() {
        showLoadingCount += 1
    }

    func hideLoading() {
        hideLoadingCount += 1
    }
}
