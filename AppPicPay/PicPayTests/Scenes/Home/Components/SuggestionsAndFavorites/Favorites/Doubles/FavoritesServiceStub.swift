import Core

@testable import PicPay

final class FavoritesServiceStub {
    var favoritesResult: Result<[Favorite], ApiError>?
    var homeFavoritesResult: Result<[HomeFavorites], ApiError>?
    var favoriteResult: Bool = false
    var unfavoriteResult: Bool = false
    var isFavoriteResult: Bool = false
}

// MARK: - FavoritesServicing
extension FavoritesServiceStub: FavoritesServicing {
    func favorites(completion: @escaping (Result<[Favorite], ApiError>) -> Void) {
        guard let result = favoritesResult else { return completion(.failure(.unknown(nil))) }
        completion(result)
    }

    func homeFavorites(completion: @escaping (Result<[HomeFavorites], ApiError>) -> Void) {
        guard let result = homeFavoritesResult else { return completion(.failure(.unknown(nil))) }
        completion(result)
    }

    func favorite(id: String, type: Favorite.`Type`, completion: @escaping (Bool) -> Void) {
        completion(favoriteResult)
    }

    func unfavorite(id: String, type: Favorite.`Type`, completion: @escaping (Bool) -> Void) {
        completion(unfavoriteResult)
    }

    func isFavorite(id: String, type: Favorite.`Type`, completion: @escaping (Bool) -> Void) {
        completion(isFavoriteResult)
    }
}
