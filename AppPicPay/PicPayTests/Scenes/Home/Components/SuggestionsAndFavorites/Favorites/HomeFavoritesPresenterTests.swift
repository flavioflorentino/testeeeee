import XCTest
import Core

@testable import PicPay

final class HomeFavoritesPresenterTests: XCTestCase {
    // MARK: - Properties
    private lazy var displayMock = HomeFavoritesDisplayMock()

    private lazy var sut: HomeFavoritesPresenter = {
        let sut = HomeFavoritesPresenter()
        sut.display = displayMock

        return sut
    }()

    // MARK: - Tests
    func testPresentError_WhenCalled_ShouldCallShowErrorOnDisplay() {
        sut.present(error: .unknown(nil))

        displayMock.verifyShowError()
    }

    func testPresentFavorites_WhenCalled_ShouldCallShowFavoritesOnDisplay() {
        let favorites = makeHomeFavoritesItems()
        sut.present(favorites: favorites)

        displayMock.verifyHideError()
        displayMock.verifyShow(favorites: favorites)
    }

    func testOpenItem_WhenCalled_ShouldInvokeOpenItemAction() {
        let expectedItem = makeHomeFavoriteItem()

        var item: HomeFavorites.Item?
        var openItemActionCount: Int = 0

        sut.openItemAction = {
            openItemActionCount += 1
            item = $0
        }

        sut.open(item: expectedItem)

        XCTAssertEqual(openItemActionCount, 1)
        XCTAssertEqual(item, expectedItem)
    }

    func testShowLoading_WhenCalled_ShouldCallShowLoadingOnDisplay() {
        sut.showLoading()
        displayMock.verifyShowLoading()
    }

    func testHideLoading_WhenCalled_ShouldCallHideLoadingOnDisplay() {
        sut.hideLoading()
        displayMock.verifyHideLoading()
    }
}

// MARK: - Private
private extension HomeFavoritesPresenterTests {
    func makeHomeFavoritesItems(count: Int = 10) -> [HomeFavorites.Item] {
        (0 ... count).map { _ in makeHomeFavoriteItem() }
    }

    func makeHomeFavoriteItem() -> HomeFavorites.Item {
        .consumer(
            item: HomeFavorites.ConsumerItem(
                id: "\(Int.random(in: 0 ..< 100))",
                name: "Consumer",
                username: "consumer",
                imageUrlSmall: nil,
                verified: true,
                pro: false
            )
        )
    }
}
