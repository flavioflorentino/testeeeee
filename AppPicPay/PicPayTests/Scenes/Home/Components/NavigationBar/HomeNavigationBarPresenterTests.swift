import CoreLegacy
import DirectMessageSB
import Foundation
import XCTest

@testable import PicPay

final class HomeNavigationBarPresenterTests: XCTestCase {
    // MARK: - Properties
    private lazy var displayMock = HomeNavigationBarDisplayMock()

    private lazy var sut: HomeNavigationBarPresenter = {
        let sut = HomeNavigationBarPresenter()
        sut.display = displayMock

        return sut
    }()

    // MARK: - Tests
    func testChangeBalance_WhenCalled_ShouldCallChangeBalanceOnDisplayWithFormattedValue() {
        let value: Decimal = 2_000
        let expectFormattedValue = CurrencyFormatter.brazillianRealString(from: NSDecimalNumber(decimal: value))

        sut.changeBalance(value: value)

        displayMock.verifyChangeBalance(value: expectFormattedValue)
    }

    func testChangeBalanceVisibility_WhenCalled_ShouldCallChangeBalanceVisibilityOnDisplay() {
        let isVisible = false

        sut.changeBalanceVisibility(isVisible: isVisible)
        displayMock.verifyChangeBalanceVisibility(isVisible: isVisible)
    }

    func testShowPromotionsIndicator_WhenCalled_ShouldCallShowPromotionsIndicatorOnDisplay() {
        sut.showPromotionsIndicator()
        displayMock.verifyShowPromotionsIndicator()
    }

    func testHidePromotionsIndicator_WhenCalled_ShouldCallHidePromotionsIndicatorOnDisplay() {
        sut.hidePromotionsIndicator()
        displayMock.verifyHidePromotionsIndicator()
    }

    func testOpenConsumerWallet_WhenCalled_ShouldExecuteOpenConsumerWalletAction() {
        var openConsumerWalletActionCount: Int = 0

        sut.openConsumerWalletAction = {
            openConsumerWalletActionCount += 1
        }

        sut.openConsumerWallet()

        XCTAssertEqual(openConsumerWalletActionCount, 1)
    }

    func testOpenPromotions_WhenCalled_ShouldExecuteOpenPromotionsAction() {
        var openPromotionsActionCount: Int = 0

        sut.openPromotionsAction = {
            openPromotionsActionCount += 1
        }

        sut.openPromotions()

        XCTAssertEqual(openPromotionsActionCount, 1)
    }

    func testOpenInvite_WhenCalled_ShouldExecuteOpenInviteAction() {
        var openInviteActionCount: Int = 0

        sut.openInviteAction = {
            openInviteActionCount += 1
        }

        sut.openInvite()

        XCTAssertEqual(openInviteActionCount, 1)
    }

    func testOpenScanner_WhenCalled_ShouldExecuteOpenScannerAction() {
        var openScannerActionCount: Int = 0

        sut.openScannerAction = {
            openScannerActionCount += 1
        }

        sut.openScanner()

        XCTAssertEqual(openScannerActionCount, 1)
    }

    func testOpenSettins_WhenCalled_ShouldExecuteOpenSettingsAction() {
        var openSettingsActionCount: Int = 0

        sut.openSettingsAction = {
            openSettingsActionCount += 1
        }

        sut.openSettings()

        XCTAssertEqual(openSettingsActionCount, 1)
    }
    
    func testChangeUnreadMessagesCount_WhenCalled_ShouldChangeUnreadMessagesCount() {
        let expectedUnreadMessages: Int = 2
        sut.changeUnreadMessagesCount(to: expectedUnreadMessages)
        displayMock.verifyChangeUnreadMessagesCount(unreadMessagesCount: expectedUnreadMessages)
    }
}

// DirectMessage Flow
extension HomeNavigationBarPresenterTests {
    func testOpenMessages_WhenCalled_ShouldExecuteOpenMessagesAction() {
        var openMessagesActionCount: Int = 0

        sut.openMessagesAction = {
            openMessagesActionCount += 1
        }

        sut.openMessages()

        XCTAssertEqual(openMessagesActionCount, 1)
    }
}
