import DirectMessageSB
import XCTest

@testable import PicPay

final class HomeNavigationBarPresenterMock: MockVerifier {
    // MARK: - Properties
    private var changeBalanceCount: Int = 0
    private var balance: Decimal = .zero

    private var changeBalanceVisibilityCount: Int = 0
    private var isBalanceVisible: Bool = false
    
    private var changeMessagesVisibilityCount: Int = 0
    private var isMessagesVisible: Bool = false
    private var checkUnreadMessagesCalledCount: Int = 0
    private var unreadMessagesCount: Int = 0
    
    private var showPromotionsIndicatorCount: Int = 0
    private var hidePromotionsIndicatorCount: Int = 0

    private var openConsumerWalletCount: Int = 0
    private var openPromotionsCount: Int = 0
    private var openInviteCount: Int = 0
    private var openScannerCount: Int = 0
    private var openSettingsCount: Int = 0
    private(set) var openMessagesCount: Int = 0

    // MARK: - Verifiers
    func verifyChangeBalanceVisibility(isVisible: Bool, file: StaticString = #file, line: UInt = #line) {
        guard verifyMethodCalledOnce(
                methodName: "changeBalanceVisibility",
                callCount: changeBalanceVisibilityCount,
                describeArguments: "isVisible: \(isVisible)",
                file: file,
                line: line) else {
            return
        }

        XCTAssertEqual(isBalanceVisible, isVisible, file: file, line: line)
    }

    func verifyChangeBalance(file: StaticString = #file, line: UInt = #line) {
        verifyMethodNeverCalled(
            methodName: "changeBalance",
            callCount: changeBalanceCount,
            describeArguments: "value: \(balance)",
            file: file,
            line: line
        )
    }

    func verifyChangeBalance(value: Decimal, file: StaticString = #file, line: UInt = #line) {
        guard verifyMethodCalledOnce(
                methodName: "changeBalance",
                callCount: changeBalanceCount,
                describeArguments: "value: \(value)",
                file: file,
                line: line) else {
            return
        }

        XCTAssertEqual(balance, value, file: file, line: line)
    }
    
    func verifyChangeMessagesVisibility(isVisible: Bool, file: StaticString = #file, line: UInt = #line) {
        guard verifyMethodCalledOnce(
                methodName: "changeMessagesVisibility",
                callCount: changeMessagesVisibilityCount,
                describeArguments: "isVisible: \(isVisible)",
                file: file,
                line: line) else {
            return
        }
        XCTAssertEqual(isMessagesVisible, isVisible, file: file, line: line)
    }
    
    func verifyShowPromotionsIndicator(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(
            methodName: "showPromotionsIndicator",
            callCount: showPromotionsIndicatorCount,
            file: file,
            line: line
        )
    }

    func verifyHidePromotionsIndicator(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(
            methodName: "hidePromotionsIndicator",
            callCount: hidePromotionsIndicatorCount,
            file: file,
            line: line
        )
    }

    func verifyOpenConsumerWallet(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "openConsumerWallet", callCount: openConsumerWalletCount, file: file, line: line)
    }

    func verifyOpenPromotions(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "openPromotions", callCount: openPromotionsCount, file: file, line: line)
    }

    func verifyOpenInvite(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "openInvite", callCount: openInviteCount, file: file, line: line)
    }

    func verifyOpenScanner(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "openScanner", callCount: openScannerCount, file: file, line: line)
    }

    func verifyOpenSettings(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "openSettings", callCount: openSettingsCount, file: file, line: line)
    }
    
    func verifyOpenMessages(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "openMessages", callCount: openMessagesCount, file: file, line: line)
    }
}

// MARK: - HomeNavigationBarPresenting
extension HomeNavigationBarPresenterMock: HomeNavigationBarPresenting {
    func changeBalance(value: Decimal) {
        changeBalanceCount += 1
        balance = value
    }

    func changeBalanceVisibility(isVisible: Bool) {
        changeBalanceVisibilityCount += 1
        isBalanceVisible = isVisible
    }
    
    func changeMessagesVisibility(isVisible: Bool) {
        changeMessagesVisibilityCount += 1
        isMessagesVisible = isVisible
    }
    
    func changeUnreadMessagesCount(to count: Int) {
        checkUnreadMessagesCalledCount += 1
        unreadMessagesCount = count
    }

    func showPromotionsIndicator() {
        showPromotionsIndicatorCount += 1
    }

    func hidePromotionsIndicator() {
        hidePromotionsIndicatorCount += 1
    }

    func openConsumerWallet() {
        openConsumerWalletCount += 1
    }

    func openPromotions() {
        openPromotionsCount += 1
    }

    func openInvite() {
        openInviteCount += 1
    }

    func openScanner() {
        openScannerCount += 1
    }

    func openSettings() {
        openSettingsCount += 1
    }
    
    func openMessages() {
        openMessagesCount += 1
    }
}
