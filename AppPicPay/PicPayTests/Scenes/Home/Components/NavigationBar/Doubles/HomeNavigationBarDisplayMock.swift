import DirectMessageSB
import XCTest

@testable import PicPay

final class HomeNavigationBarDisplayMock: MockVerifier {
    // MARK: - Properties
    private var changeBalanceCount: Int = 0
    private var balance: String?

    private var changeBalanceVisibilityCount: Int = 0
    private var isBalanceVisible: Bool = false

    private var changeSettingsVisibilityCount: Int = 0
    private var isSettingsVisible: Bool = false
    
    private var changeMessagesVisibilityCount: Int = 0
    private var isMessagesVisible: Bool = false
    private var changeUnreadMessagesCalledCount: Int = 0
    private var unreadMessagesCount: Int = 0

    private var showPromotionsIndicatorCount: Int = 0
    private var hidePromotionsIndicatorCount: Int = 0

    // MARK: - Verifiers
    func verifyChangeBalanceVisibility(isVisible: Bool, file: StaticString = #file, line: UInt = #line) {
        guard verifyMethodCalledOnce(
                methodName: "changeBalanceVisibility",
                callCount: changeBalanceVisibilityCount,
                describeArguments: "isVisible: \(isVisible)",
                file: file,
                line: line) else {
            return
        }

        XCTAssertEqual(isBalanceVisible, isVisible, file: file, line: line)
    }

    func verifyChangeBalance(value: String?, file: StaticString = #file, line: UInt = #line) {
        guard verifyMethodCalledOnce(
                methodName: "changeBalance",
                callCount: changeBalanceCount,
                describeArguments: "value: \(value ?? "nil")",
                file: file,
                line: line) else {
            return
        }

        XCTAssertEqual(balance, value, file: file, line: line)
    }
    
    func verifyChangeUnreadMessagesCount(unreadMessagesCount: Int, file: StaticString = #file, line: UInt = #line) {
        guard verifyMethodCalledOnce(
                methodName: "changeUnreadMessagesCount",
                callCount: changeUnreadMessagesCalledCount,
                describeArguments: "unreadMessagesCount: \(unreadMessagesCount)",
                file: file,
                line: line) else {
            return
        }

        XCTAssertEqual(unreadMessagesCount, self.unreadMessagesCount, file: file, line: line)
    }

    func verifyShowPromotionsIndicator(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(
            methodName: "showPromotionsIndicator",
            callCount: showPromotionsIndicatorCount,
            file: file,
            line: line
        )
    }

    func verifyHidePromotionsIndicator(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(
            methodName: "hidePromotionsIndicator",
            callCount: hidePromotionsIndicatorCount,
            file: file,
            line: line
        )
    }
}

// MARK: - HomeNavigationBarDisplay
extension HomeNavigationBarDisplayMock: HomeNavigationBarDisplay {
    func changeBalance(value: String?) {
        changeBalanceCount += 1
        balance = value
    }

    func changeBalanceVisibility(isVisible: Bool) {
        changeBalanceVisibilityCount += 1
        isBalanceVisible = isVisible
    }
    
    func changeMessagesVisibility(isVisible: Bool) {
        changeMessagesVisibilityCount += 1
        isMessagesVisible = isVisible
    }
    
    func changeUnreadMessagesCount(to count: Int) {
        changeUnreadMessagesCalledCount += 1
        unreadMessagesCount = count
    }
    
    func showPromotionsIndicator() {
        showPromotionsIndicatorCount += 1
    }

    func hidePromotionsIndicator() {
        hidePromotionsIndicatorCount += 1
    }
}
