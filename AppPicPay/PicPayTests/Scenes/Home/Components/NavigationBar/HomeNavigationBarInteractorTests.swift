import Core
import DirectMessageSB
import FeatureFlag
import XCTest

@testable import PicPay

final class HomeNavigationBarInteractorTests: XCTestCase {
    private lazy var consumerManagerMock = ConsumerManagerMock()
    private lazy var featureManagerMock = FeatureManagerMock()
    private lazy var kvStoreMock = KVStoreMock()
    private lazy var chatApiManagerMock = ChatApiManagerMock()

    private lazy var homeNavigationBarPresenterMock = HomeNavigationBarPresenterMock()

    private lazy var sut = HomeNavigationBarInteractor(
        presenter: homeNavigationBarPresenterMock,
        dependencies: DependencyContainerMock(consumerManagerMock, featureManagerMock, kvStoreMock, chatApiManagerMock)
    )

    func testLoadConsumerBalance_WhenCalled_ShouldCallLoadConsumerBalanceOnConsumerManager() {
        sut.loadConsumerBalance()

        consumerManagerMock.verifyLoadConsumerBalance()
    }

    func testLoadConsumerBalance_WhenCalled_ShouldChangeConsumerBalanceVisibility() {
        consumerManagerMock.isBalanceHidden = false

        sut.loadConsumerBalance()

        homeNavigationBarPresenterMock.verifyChangeBalanceVisibility(isVisible: true)
    }

    func testLoadConsumerBalance_WhenCalled_ShouldUpdateConsumerBalanceAndHideLoading() {
        let balance: Decimal = 2_000

        consumerManagerMock.isBalanceHidden = false
        consumerManagerMock.consumer = createConsumer(withBalance: balance)

        sut.loadConsumerBalance()

        homeNavigationBarPresenterMock.verifyChangeBalance(value: balance)
    }

    func testLoadConsumerBalance_WhenVisibilityIsHidden_ShouldNotUpdateConsumerBalance() {
        consumerManagerMock.isBalanceHidden = true

        sut.loadConsumerBalance()

        homeNavigationBarPresenterMock.verifyChangeBalanceVisibility(isVisible: false)
        homeNavigationBarPresenterMock.verifyChangeBalance()
    }

    func testCheckPromotionsIndicator_WhenPromotionsHasNeverBeenOpened_ShouldShowPromotionsIndicator() {
        kvStoreMock.set(value: false, with: PromotionsUserDefaultKey.alreadyOpened.rawValue)

        sut.checkPromotionsIndicator()

        homeNavigationBarPresenterMock.verifyShowPromotionsIndicator()
    }

    func testCheckPromotionsIndicator_WhenPromotionsHasAlreadyBeenOpened_ShouldHidePromotionsIndicator() {
        kvStoreMock.set(value: true, with: PromotionsUserDefaultKey.alreadyOpened.rawValue)

        sut.checkPromotionsIndicator()

        homeNavigationBarPresenterMock.verifyHidePromotionsIndicator()
    }
    
    func testCheckIfMessagesAreAvailable_WhenCalled_ShouldChangeMessagesVisibility() {
        let available = true
        featureManagerMock.override(key: .directMessageSBEnabled, with: available)

        sut.checkIfMessagesAreAvailable()

        homeNavigationBarPresenterMock.verifyChangeMessagesVisibility(isVisible: available)
    }

    func testTapAtScanner_WhenCalled_ShouldCallOpenScannerOnPresenter() {
        sut.tapAtScanner()

        homeNavigationBarPresenterMock.verifyOpenScanner()
    }

    func testTapAtSettings_WhenCalled_ShouldCallOpenSettingsOnPresenter() {
        sut.tapAtSettings()

        homeNavigationBarPresenterMock.verifyOpenSettings()
    }

    func testTapAtConsumerBalance_WhenCalled_ShouldCallOpenConsumerWalletOnPresenter() {
        sut.tapAtConsumerBalance()

        homeNavigationBarPresenterMock.verifyOpenConsumerWallet()
    }

    func testTapAtInvite_WhenCalled_ShouldCallOpenInviteOnPresenter() {
        sut.tapAtInvite()

        homeNavigationBarPresenterMock.verifyOpenInvite()
    }

    func testTapAtPromotions_WhenCalled_ShouldCallOpenPromotionsOnPresenter() {
        sut.tapAtPromotions()

        homeNavigationBarPresenterMock.verifyOpenPromotions()
    }

    func testTapAtPromotions_WhenCalled_ShouldRecordThatPromotionsWasOpen() {
        sut.tapAtPromotions()

        XCTAssertEqual(kvStoreMock.getFirstTimeOnlyEvent(PromotionsUserDefaultKey.alreadyOpened.rawValue), true)
    }

    func testTapAtPromotions_WhenCalled_ShouldHidePromotionsIndicator() {
        sut.tapAtPromotions()

        homeNavigationBarPresenterMock.verifyHidePromotionsIndicator()
    }
}

// DirectMessage Flow
extension HomeNavigationBarInteractorTests {
    func testTapAtMessages_WhenFlagIsNotEnabled_ShouldNotCallOpenMessagesOnPresenter() {
        let available = false
        featureManagerMock.override(key: .directMessageSBEnabled, with: available)

        sut.tapAtMessages()
        XCTAssertEqual(homeNavigationBarPresenterMock.openMessagesCount, 0)
    }

    func testTapAtMessages_WhenConnectionStateIsEnabled_ShouldNotCallOpenMessagesOnPresenter() {
        let available = true
        featureManagerMock.override(key: .directMessageSBEnabled, with: available)

        sut.tapAtMessages()

        XCTAssertEqual(homeNavigationBarPresenterMock.openMessagesCount, 1)
    }
}

private extension HomeNavigationBarInteractorTests {
    func createConsumer(withBalance balance: Decimal) -> MBConsumer {
        let consumer = MBConsumer()
        consumer.balance = NSDecimalNumber(decimal: balance)

        return consumer
    }
}
