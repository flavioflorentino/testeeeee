import XCTest

@testable import PicPay

final class HomePromotionsAndMgmPresenterMock: MockVerifier {
    // MARK: - Properties
    private var openPromotionsAndMgmCount: Int = 0

    // MARK: - Verifiers
    func verifyOpenPromotionsAndMgm(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "openPromotionsAndMgm", callCount: openPromotionsAndMgmCount, file: file, line: line)
    }
}

// MARK: - HomePromotionsAndMgmPresenting
extension HomePromotionsAndMgmPresenterMock: HomePromotionsAndMgmPresenting {
    func openPromotionsAndMgm() {
        openPromotionsAndMgmCount += 1
    }
}
