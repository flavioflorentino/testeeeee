import XCTest

@testable import PicPay

final class HomePromotionsAndMgmPresenterTests: XCTestCase {
    // MARK: - Properties
    private lazy var sut = HomePromotionsAndMgmPresenter()

    // MARK: - Tests
    func testOpenPromotionsAndMgm_WhenCalled_ShouldExecuteOpenPromotionsAndMgmAction() {
        var openPromotionsAndMgmActionCount: Int = 0

        sut.openPromotionsAndMgmAction = {
            openPromotionsAndMgmActionCount += 1
        }

        sut.openPromotionsAndMgm()

        XCTAssertEqual(openPromotionsAndMgmActionCount, 1)
    }
}
