import XCTest

@testable import PicPay

final class HomePromotionsAndMgmInteractorTests: XCTestCase {
    private lazy var presenterMock = HomePromotionsAndMgmPresenterMock()

    private lazy var sut = HomePromotionsAndMgmInteractor(presenter: presenterMock)

    func testTapAtPromotionsAndMgm_WhenCalled_ShouldCallOpenPromotionsAndMgmOnPresenter() {
        sut.tapAtPromotionsAndMgm()

        presenterMock.verifyOpenPromotionsAndMgm()
    }
}
