@testable import PicPay
import OHHTTPStubs
import XCTest

class ProfilePromoCodeViewModelTests: XCTestCase {
    let presenterSpy = ProfilePromoCodePresenterSpy()
    lazy var sut: ProfilePromoCodeViewModel? = {
        let service: ProfilePromoCodeServicing = ProfilePromoCodeServiceSpy()
        let contactJson = MockJSON().load(resource: "validateReferralCode")
        let contact = PPContact(profileDictionary: contactJson.dictionaryObject)
        return ProfilePromoCodeViewModel(service: service, presenter: presenterSpy, contact: contact, wsId: 1458632)
    }()
    
    let service: ProfilePromoCodeServicing = ProfilePromoCodeService(dependencies: DependencyContainerMock())
    
    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func testShowInviter() {
        sut?.viewDidLoad()
        
        XCTAssertTrue(presenterSpy.isPro)
        XCTAssertTrue(presenterSpy.showFollow)
        XCTAssertNotNil(presenterSpy.contact?.imgUrlLarge)
    }
    
    func testShowAlertUnfollow() {
        sut?.followAction(action: .unfollow)
        
        XCTAssertEqual(presenterSpy.showAletText, "Deseja cancelar o pedido para seguir @fabio.miciano")
    }
    
    func testShowAlertCancelRequest() {
        sut?.followAction(action: .cancelRequest)
        
        XCTAssertEqual(presenterSpy.showAletText, "Deseja deixar de seguir @fabio.miciano")
    }
    
    func testChangeStatusButton() {
        sut?.actionFollowStatus(action: .follow)
        
        XCTAssertEqual(self.presenterSpy.followerStatus, .loading)
    }
    
    func testErrorMessage() {
        sut?.actionFollowStatus(action: .undefined)
        
        XCTAssertEqual(self.presenterSpy.error?.localizedDescription, "Error crated for unit tests")
    }
    
    func testCloseScreen() {
        sut?.nextStep()
        
        XCTAssertEqual(presenterSpy.action, .finish)
    }
}

final class ProfilePromoCodeServiceSpy: ProfilePromoCodeServicing {
    func executeFollowAction(action: FollowerButtonAction, follower: String, completion: @escaping FollowersActionCompletion) {
        if action == .undefined {
            let error = PicPayError(message: "Error crated for unit tests", code: "500", title: "Test")
            completion(.failure(error))
            return
        }
        
        let response = FollowActionResponse(followerStatus: .following, consumerStatus: .notFollowing)
        completion(.success(response))
    }
}

final class ProfilePromoCodePresenterSpy: ProfilePromoCodePresenting {
    var viewController: ProfilePromoCodeDisplay?
    
    private (set) var isPro: Bool = false
    private (set) var showFollow: Bool = false
    private (set) var contact: PPContact?
    private (set) var showAletText: String?
    private (set) var error: Error?
    private (set) var followerStatus: FollowerStatus?
    private (set) var consumerStatus: FollowerStatus?
    private (set) var action: ProfilePromoCodeAction?
 
    func showProfileInviter(contact: PPContact, isPro: Bool, showFollow: Bool) {
        self.contact = contact
        self.isPro = isPro
        self.showFollow = showFollow
    }
    
    func showAlert(with message: String, userName: String) {
        let newMessage = message+" @\(userName)"
        showAletText = newMessage
    }
    
    func showAlertError(with error: Error) {
        self.error = error
    }
    
    func changeFollowButton(with followerResponse: FollowActionResponse) {
        self.followerStatus = followerResponse.followerStatus
        self.consumerStatus = followerResponse.consumerStatus
    }
    
    func closeScreen(action: ProfilePromoCodeAction) {
        self.action = action
    }
}
