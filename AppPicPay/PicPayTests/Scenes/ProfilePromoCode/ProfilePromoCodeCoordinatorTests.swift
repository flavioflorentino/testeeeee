@testable import PicPay
import XCTest

class ProfilePromoCodeCoordinatorTests: XCTestCase {
    let nav = NavigationControllerMock(rootViewController: UIViewController())
    var sut: ProfilePromoCodeCoordinator? = ProfilePromoCodeCoordinator()
    override func setUp() {
        sut?.viewController = nav
        super.setUp()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func testDimissController() {
        sut?.perform(action: .finish)
        XCTAssertTrue(nav.isDismissViewControllerCalled)
    }
}
