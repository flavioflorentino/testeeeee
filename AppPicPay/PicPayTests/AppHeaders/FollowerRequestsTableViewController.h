//
//  FollowerRequestsTableViewController.h
//  PicPay
//
//  Created by Vagner Orlandi on 13/07/17.
//
//

#import <UIKit/UIKit.h>

@class PPConsumerProfileNotification;

@protocol FollowerRequestDelegate <NSObject>
@optional
- (void)followerRequestWasIgnoredWithId:(NSInteger)wsId;
- (void)followerRequestWasUpdated:(PPConsumerProfileNotification *) notification;
@end

@interface FollowerRequestsTableViewController : UITableViewController

@property (strong, nonatomic) NSMutableArray *notifications;
@property (nonatomic, weak) id <FollowerRequestDelegate> delegate;

@end
