#ifdef __OBJC__
#    import <Foundation/Foundation.h>
#    if __has_include(<UIKit/UIKit.h>)
#        import <UIKit/UIKit.h>
#    endif
#else
#    ifndef FOUNDATION_EXPORT
#        if defined(__cplusplus)
#            define FOUNDATION_EXPORT extern "C"
#        else
#            define FOUNDATION_EXPORT extern
#        endif
#    endif
#endif

#import "MessageComposerView.h"
#import "NSURLRequest+cURL.h"
#import "AccountInstallmentsConfigViewController.h"
#import "NSString+MD5.h"
#import "NSString+OnlyNumbers.h"
#import "NSString+RegularExpressionSearch.h"
#import "PPSlidableTableViewController.h"
#import "PPSlidableViewController.h"
#import "UILabel+ColorInRanges.h"
#import "ViewsManager.h"
#import "CreditCardOnboardingPopUpViewController.h"
#import "FollowerRequestsTableViewController.h"
#import "RewardProgramProgressViewController.h"
#import "HTMLPopUpViewController.h"
#import "NotificationsViewController.h"
#import "NSDecimalNumber+PPDecimalNumber.h"
#import "NewTransactionViewController.h"
#import "PaymentViewController.h"
#import "ProfileViewController.h"
#import "PPAntiFraudChecklistItem.h"
#import "PPPaymentCalculator.h"
#import "PPPaymentManager.h"
#import "PPNotification.h"
#import "PPAnalytics.h"
#import "PPAuth.h"
#import "AdyenProtocol.h"
#import "PPBaseViewControllerObj.h"
#import "PPNavigationController.h"
#import "ChatTableViewCell.h"
#import "SearchNoResultsTableViewCell.h"
#import "ChatLabel.h"
#import "ImageFullScreenView.h"
#import "Loader.h"
#import "ReceiptParkView.h"
#import "UIAlertController+Blocks.h"
#import "UIBorderedButton.h"
#import "UICircularImageView.h"
#import "UILimitedTableViewCellCell.h"
#import "YUMixtureView.h"
#import "YUSegment.h"
#import "ViewUtil.h"
#import "WebServiceInterface.h"
#import "WSAppVersion.h"
#import "WSConnection.h"
#import "WSConsumer.h"
#import "WSConsumerRequest.h"
#import "WSCreditRequest.h"
#import "WSExploreRequest.h"
#import "WSFriends.h"
#import "WSItemRequest.h"
#import "WSParcelamento.h"
#import "WSPasswordChangeRequest.h"
#import "WSTransaction.h"

FOUNDATION_EXPORT double HeadersVersionNumber;
FOUNDATION_EXPORT const unsigned char HeadersVersionString[];
