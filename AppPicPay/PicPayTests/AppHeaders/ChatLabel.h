//
//  ChatLabel.h
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 18/07/16.
//
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface ChatLabel : UILabel

@property IBInspectable BOOL userMessage;
@property IBInspectable CGFloat radius;

@end
