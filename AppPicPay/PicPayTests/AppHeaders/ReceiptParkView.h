//
//  ReceiptParkView.h
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 07/06/16.
//
//

#import <UIKit/UIKit.h>

@interface ReceiptParkView : UIView

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIButton *licensePlateButton;
@property (nonatomic, weak) IBOutlet UIView *view;

@end
