#import <UIKit/UIKit.h>
#import "PPNotification.h"
#import "FollowerRequestsTableViewController.h"
#import "PPBaseViewControllerObj.h"

@class UILoadView;
@class EcommerceLoadingFactory;

@interface NotificationsViewController: PPBaseViewControllerObj <UITableViewDataSource, UITableViewDelegate, FollowerRequestDelegate> {
	BOOL isRequestingNextPage;
	BOOL disappeared;
	BOOL dismissAfterArriveBack;
	BOOL loaded;
	BOOL preventPushAnimation;
}

@property (strong, nonatomic) PPNotification *notificationToOpen;
@property BOOL forceBackButton;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UILoadView *loaderView;

- (void)openNotification:(PPNotification *)notification;
- (void)openNotification:(PPNotification *)notification parentView:(UIViewController *)parentViewController;

@end
