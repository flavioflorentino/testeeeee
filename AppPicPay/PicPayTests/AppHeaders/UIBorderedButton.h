//
//  UIBorderedButton.h
//  PicPay
//
//  Created by Diogo Carneiro on 12/09/13.
//
//

#import <UIKit/UIKit.h>

@interface UIBorderedButton : UIButton

@property (strong, nonatomic) UIColor *mainColor;
@property (strong, nonatomic) UIColor *originalColor; //the first color

- (void)changeColor:(UIColor *)newColor;

@end
