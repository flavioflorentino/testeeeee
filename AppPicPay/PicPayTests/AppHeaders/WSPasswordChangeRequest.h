//
//  WSPasswordChangeRequest.h
//  MoBuy
//
//  Created by Diogo Carneiro on 07/08/12.
//
//

#import <Foundation/Foundation.h>
#import "WebServiceInterface.h"

@import CoreLegacy;

@interface WSPasswordChangeRequest : NSObject

/*!
 * Solicita alteração de senha do usuário
 *
 * Callback:
 *
 *   - @b BOOL => success
 *
 *   - @b NSError => erro
 *
 * @param currentPassword
 * @param newPassword
 * @param callback
 */
+ (void)changePassword: (NSString *)currentPassword newPassword:(NSString *)newPassword completionHandler:(void (^)(BOOL success, NSError *error)) callback;

@end
