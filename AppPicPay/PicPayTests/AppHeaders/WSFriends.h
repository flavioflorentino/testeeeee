//
//  WSFriends.h
//  PicPay
//
//  Created by Diogo Carneiro on 29/08/13.
//
//

#import <Foundation/Foundation.h>
#import "WebServiceInterface.h"

@interface WSFriends : NSObject

/*!
 * Retorna a lista de contatos identificados (Contatos conhecidos no webservice).
 *
 * Callback:
 *
 *   - @b NSArray => Lista de contatos
 *
 *   - @b NSError => erro
 *
 * @param contacts Lista de contatos
 * @param useCache flag para buscar os contatos do cache(caso ele exista). Para força uma requisião ao webservice utilize: NO
 * @param completionHandler callback
 */
+ (void)identifyContacts:(NSArray * _Nonnull)contacts useCache:(BOOL)useCache completionHandler:(void (^ _Nonnull)(NSArray * _Nullable, NSError * _Nullable))callback;

/*!
 * Processa os dados de contatos identificados
 * @return Contatos
 *///duplicated:(NSMutableArray **)duplicated landlines:(NSMutableArray **)landlines
+ (NSArray * _Nullable)proccessIdentifyContacts:(NSArray * _Nonnull)json;

/*!
 * Retorna a lista de emails sugeridos
 *
 * Callback:
 *
 *   - @b NSArray => Lista de emails
 *
 *   - @b NSError => erro
 *
 * @param completionHandler callback
 */
+ (void)emailAddressesSuggestions:(void (^ _Nonnull)(NSArray * _Nullable data, NSError * _Nullable error))callback;

/*!
 * Retorna a lista de sugestão de telefone
 *
 * Callback:
 *
 *   - @b NSArray => Informações de telefones
 *
 *   - @b NSError => erro
 *
 * @param completionHandler callback
 */
+ (void)phoneNumbersSuggestions:(void (^ _Nonnull)(NSArray * _Nullable data, NSError * _Nullable error))callback;

@end
