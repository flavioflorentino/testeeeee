//
//  Loader.h
//  MoBuy
//
//  Created by Diogo Carneiro on 10/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Loader : NSObject

+ (UIView *)getLoadingView:(NSString *)title;
+ (UIView *)getKeyboardLoadingView:(NSString *)title;
+ (UIView *)getMiniLoadingView:(NSString *)title;
+ (UIView *)getNoTabbarLoadingView:(NSString *)title;

@end
