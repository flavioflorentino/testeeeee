//
//  CreditCardOnboardingPopUpViewController.h
//  PicPay
//
//  Created by Diogo Carneiro on 25/09/15.
//
//

#import <UIKit/UIKit.h>
#import "PPBaseViewControllerObj.h"

@class CreditCardOnboardingPopUpViewController;

@protocol CreditCardOnboardingPopUpDelegate <NSObject>

//- (void)creditCardOnboardingPopUp:(CreditCardOnboardingPopUpViewController *)creditCardOnboardingPopUp addCard:(id)sender;
- (void)creditCardOnboardingPopUpAddCreditCard;
- (void)creditCardOnboardingPopUpDidAddCreditCard;

@end

@interface CreditCardOnboardingPopUpViewController : PPBaseViewControllerObj

@property (strong, nonatomic) id<CreditCardOnboardingPopUpDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *textLabel;
@property (strong, nonatomic) IBOutlet UIView *cardView;

+ (CreditCardOnboardingPopUpViewController *)showCreditCardOnboarding:(UIViewController<CreditCardOnboardingPopUpDelegate> *)parent;

- (IBAction)close:(id)sender;
- (IBAction)readMore:(id)sender;
- (IBAction)addCreditCard:(id)sender;
- (IBAction)notNow:(id)sender;

@end
