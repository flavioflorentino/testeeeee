//
//  WSExploreRequest.h
//  PicPay
//
//  Created by Diogo Carneiro on 03/04/13.
//
//

#import <Foundation/Foundation.h>

@interface WSExploreRequest : NSObject

/*!
 * Retorna os creditos globais do usuário (Crédito na carteira)
 *
 * Callback:
 *
 *   - @b NSDecimalNumber => crédito do usuário
 *
 *   - @b NSError => erro
 *
 * @param completionHandler callback
 */
+ (void)globalCredits:(void (^)(NSDecimalNumber *credits, NSError *error)) callback;

@end
