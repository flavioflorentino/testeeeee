//
//  ImageFullScrennView.h
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 27/07/16.
//
//  Class for show a UIImageView in fullscreen

#import <UIKit/UIKit.h>

@interface ImageFullScreenView : UIView

@property (weak, nonatomic) UIImageView *originalImageView;
@property (strong, nonatomic) NSString *imageLargeUrl;

- (instancetype)initWithImageView:(UIImageView *) imageView;
- (instancetype)initWithImageView:(UIImageView *) imageView largeUrl:(NSString *) largeUrl;

@end
