//
//  PaymentViewController.h
//  PicPay
//
//  Created by Diogo Carneiro on 22/02/13.
//
//

@import UIKit;
@import CoreLocation;

#import "Loader.h"
#import "RewardProgramProgressViewController.h"
#import "PPAuth.h"
#import "PPPaymentManager.h"
#import "HTMLPopUpViewController.h"
#import "PPBaseViewControllerObj.h"

@import CoreLegacy;

@class UILoadView;

@interface PaymentViewController : PPBaseViewControllerObj <UITextFieldDelegate, CLLocationManagerDelegate, UIPickerViewDataSource, UIPickerViewDelegate, HTMLPopUpViewDelegate>{
	BOOL itemIsReady;
	
	UIView *loadingView;
	UILoadView *loadingItemView;
	
	RewardProgramProgressViewController *rewardHelper;
	
	PPAuth *auth;
	
	PPPaymentManager *paymentManager;
	
	UIBarButtonItem *paymentMethodButton;
	
	BOOL wentToCards;
	BOOL wentToInstallments;
	
	CLLocationManager *locationManager;
	
	UIPickerView *pickerView;
	
	UIAlertController *carPlateAlert;
	
	NSString *licensePlate;
}

@property (strong, nonatomic, nullable) UINavigationController *previousNavigationController; //var to come back from receipt of places scanner

@property (strong, nonatomic, nullable) NSString *touchOrigin;
@property (strong, nonatomic, nullable) NSString *initialValue;
@property (strong, nonatomic, nullable) NSString *deepLinkExtraParam;
@property (strong, nonatomic, nullable) NSDictionary *additionalInfo;
@property BOOL isFixedValue;

@property (strong, nonatomic, nullable) PPStore *store;
@property (strong, nonatomic, nullable) NSString *transactionHashValue;

@property CGFloat bestGpsAcc;
@property CGFloat bestGpsLat;
@property CGFloat bestGpsLng;

@property (strong, nonatomic, nullable) MBItem *item;
@property int navigationLevel;
@property BOOL from_explore;


//Outlets
@property (weak, nonatomic, nullable) IBOutlet UIView *horizontalLine;
@property (strong, nonatomic, nullable) IBOutlet UIView *inputContainer;
@property (weak, nonatomic, nullable) IBOutlet UILabel *labelStoreName;
@property (weak, nonatomic, nullable) IBOutlet UIImageView *storeImage;
@property (strong, nonatomic, nullable) IBOutlet UIView *viewValueInputContainer;
@property (strong, nonatomic, nullable) IBOutlet UIView *viewLargeGreenButtonContainer;
@property (strong, nonatomic, nullable) IBOutlet UITextField *valueInputTextField;
@property (strong, nonatomic, nullable) IBOutlet UILabel *labelCreditDescription;
@property (weak, nonatomic, nullable) IBOutlet UIView *viewForFidelityInfo;

@property (strong, nonatomic, nullable) IBOutlet UIView *viewForMeasuringPickerViewHeight;
@property (weak, nonatomic, nullable) IBOutlet UILabel *parkingTimeTitleLabel;
@property (strong, nonatomic, nullable) IBOutlet UILabel *parkingTimeLabel;
@property (weak, nonatomic, nullable) IBOutlet UILabel *carPlateTitleLabel;
@property (strong, nonatomic, nullable) IBOutlet UILabel *carPlateLabel;
@property (strong, nonatomic, nullable) IBOutlet UIButton *parkingInfoButton;
@property (strong, nonatomic, nullable) IBOutlet UIView *parkingPaymentFieldsView;
@property (strong, nonatomic, nullable) IBOutlet UIView *viewForMissingCarPlate;
@property (strong, nonatomic, nullable) IBOutlet NSLayoutConstraint *viewForParkingBottomConstraint;

@property (weak, nonatomic, nullable) IBOutlet UIButton *installmentsButton;
@property (weak, nonatomic, nullable) IBOutlet UIView *profile;

@property (strong, nonatomic, nullable) UITextField *carPlateTextField;
@property (copy, nullable) NSString* privacyConfig;
@property BOOL forceModal;
@property BOOL someErrorOccurred;
@property BOOL showCancel;

// New Installment button
@property (weak, nonatomic, nullable) IBOutlet UIButton *enhancedInstallmentButton;
@property (weak, nonatomic, nullable) IBOutlet UIStackView *headerStackView;
@property (weak, nonatomic, nullable) IBOutlet UIView *paymentDetailView;


//IBActions
- (IBAction)resignFirstResponderOnBlur;
- (IBAction)checkout:(id _Nullable)sender;
- (IBAction)editCarPlate;
- (IBAction)showUserPlates;
- (IBAction)parkingInfo;
- (IBAction)changeInstallments:(id _Nullable)sender;

@end
