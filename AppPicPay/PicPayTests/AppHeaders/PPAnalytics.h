#import <Foundation/Foundation.h>

@interface PPAnalytics : NSObject

+ (void)userSignedUpWithId:(NSString * _Nullable)alias;

+ (void)trackScreen:(id _Nonnull)screen;
+ (void)trackScreen:(id _Nonnull)screen withVariation:(NSString * _Nullable)variation;

//Mixpanel
+ (void)trackEvent:(NSString * _Nullable)eventName withLabel:(NSString * _Nullable)label andValue:(NSNumber * _Nullable)value properties:(NSDictionary * _Nullable)properties;
+ (void)trackEvent:(NSString *_Nullable)eventName properties:(NSDictionary * _Nullable)properties;
+ (void)trackFirstTimeOnlyEvent:(NSString * _Nullable)eventName properties:(NSDictionary * _Nullable)properties;
+ (void)trackEvent:(NSString * _Nullable)eventName properties:(NSDictionary * _Nullable)properties includeMixpanel:(BOOL) includeMixpanel;
+ (void)trackFirstTimeOnlyEvent:(NSString * _Nullable)eventName properties:(NSDictionary * _Nullable)properties includeMixpanel:(BOOL) includeMixpanel;

+ (void)setMixpanelUserProperties:(NSDictionary * _Nonnull) properties;

+ (void)timeEvent:(NSString * _Nullable)eventName;

+ (NSString * _Nullable)distinctId;

+ (NSString * _Nullable)screenNameFromViewController:(id _Nonnull)viewController withVariation:(NSString * _Nullable)variation;

@end
