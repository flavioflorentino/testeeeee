#import <UIKit/UIKit.h>
#import <AddressBookUI/AddressBookUI.h>
#import "ViewsManager.h"
#import "Loader.h"
#import "PPAuth.h"
#import "PPBaseViewControllerObj.h"
#import "PPPaymentManager.h"
@import CoreLegacy;
@import Adyen3DS2;

@interface NewTransactionViewController : PPBaseViewControllerObj <UITextFieldDelegate, UITextViewDelegate, ADYChallengeDelegate>{
	BOOL contactAlreadySelected;
    BOOL someErrorOccurred;
    BOOL wentToCards;
    BOOL wentToInstallments;
    int messageMaxLength;
    NSString *editedSelectedPhone;
    UIView *loadingView;
    PPAuth *auth;
    PPContact *selectedContact;
    PPPaymentManager *paymentManager;
    UIBarButtonItem *paymentMethodButton;
}

@property BOOL isFixedValue;
@property BOOL preselectedContactFromSearch;
@property BOOL preselectedContactFromSearchWithUsername;
@property BOOL showCancel;

@property(strong, nonatomic, nullable) ADYTransaction *transaction;
@property(strong, nonatomic, nullable) NSDictionary *adyenParameters;
@property(strong, nonatomic, nullable) NSDecimalNumber *adyenValue;
@property(strong, nonatomic, nullable) NSNumber *chargeTransactionId;

@property (strong, nonatomic, nullable) PPContact *preselectedContact;
@property (strong, nonatomic, nullable) NSString *touchOrigin;
@property (strong, nonatomic, nullable) NSString *preselectedPaymentValue;
@property (strong, nonatomic, nullable) NSString *deepLinkExtraParam;
@property (strong, nonatomic, nullable) UIView *horizontalLine;
@property (weak, nonatomic, nullable) IBOutlet UILabel *contactName;
@property (weak, nonatomic, nullable) IBOutlet UILabel *username;
@property (weak, nonatomic, nullable) IBOutlet UIImageView *contactPicture;
@property (strong, nonatomic, nullable) IBOutlet UIImageView *profileImageBadge;
@property (weak, nonatomic, nullable) IBOutlet UITextField *enteredValue;
@property (weak, nonatomic, nullable) IBOutlet UITextView *messageTextView;
@property (weak, nonatomic, nullable) IBOutlet UILabel *charactersCount;
@property (weak, nonatomic, nullable) IBOutlet UIButton *installmentsButton;
@property (weak, nonatomic, nullable) IBOutlet UIView *profileImage;
@property (copy, nullable) NSString* privacyConfig;
@property (weak, nonatomic, nullable) IBOutlet NSLayoutConstraint *messageBottomConstraint;
@property (weak, nonatomic, nullable) IBOutlet UIView *topView;
@property (weak, nonatomic, nullable) IBOutlet UIButton *enhancedInstallmentButton;
@property (weak, nonatomic, nullable) IBOutlet NSLayoutConstraint *headerViewHeightConstraint;

@property BOOL forceModal;

+ (nullable NewTransactionViewController *) fromStoryboard;

- (IBAction)close:(nullable id)sender;
- (IBAction)changeInstallments:(nullable id)sender;
- (IBAction)checkout:(nullable id)sender;
- (void) loadConsumerInfo: (nullable NSString *) userId;

@end
