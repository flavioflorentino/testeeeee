//
//  ChatTableViewCell.h
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 15/07/16.
//
//

#import <UIKit/UIKit.h>

@interface ChatTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageBackgroundView;

@end
