//
//  UICircularImageView.h
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 23/06/16.
//
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface UICircularImageView : UIImageView

@property (strong, nonatomic) IBInspectable UIColor * borderColor;
@property  IBInspectable CGFloat borderWidth;

@end
