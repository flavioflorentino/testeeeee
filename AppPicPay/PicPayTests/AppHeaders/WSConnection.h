//
//  WSConnection.h
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 26/04/16.
//
//

#import <Foundation/Foundation.h>

/*!
 * Callback da requisição
 * @param responseData responsta do request
 * @param response Objeto de resposta da requisição
 * @param error erro (Caso ocorra)
 */
typedef void (^ConnectionCallback)(NSData *responseData, NSURLResponse *response, NSError *error);

/*!
 * Callback da progresso
 * @param progress porcentagem do progresso
 */
typedef void (^ConnectionProgressCallback)(CGFloat progress);

/*!
 * Class Wrapper para efeutar uma requisiçao com delegate baseada em code block
 */
@interface WSConnection : NSObject <NSURLSessionTaskDelegate>{
    NSMutableData *_data;
}

/*!
 * efetua uma requisição baseado no request.
 * Wrapper para efeutar uma requisiçao com codeblock baseada em delegate
 */
- (void) sendRequest:(NSURLRequest* )request completionHandler:(ConnectionCallback)callback;

/*!
 * efetua uma requisição baseado no request.
 * Wrapper para efeutar uma requisiçao com codeblock baseada em delegate
 */
- (void) sendRequest:(NSURLRequest* )request completionHandler:(ConnectionCallback)callback progressCallback:(ConnectionProgressCallback)progressCallback;

@property (copy) ConnectionCallback callback;
@property (copy) ConnectionProgressCallback progressCallback;
@property (nonatomic, retain) NSMutableData *dataToDownload;
@property (nonatomic) float downloadSize;

@end
