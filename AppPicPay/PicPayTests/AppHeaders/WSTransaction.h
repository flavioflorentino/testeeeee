//
//  WSTransaction.h
//  MoBuy
//
//  Created by Joao victor V lana on 19/08/12.
//
//

#import <Foundation/Foundation.h>
#import "WebServiceInterface.h"
@import CoreLegacy;

@class PPP2PTransaction;
@class P2PTransactionRequest;

@interface WSTransaction : NSObject

# pragma mark Create Methods
/*!
 * Cria uma nova transação
 *
 * Callback:
 *
 *   - @b MBTransaction => Transação
 *
 *   - @b NSError => erro
 *
 * @param args dados da transação
 * @param origin Origem
 * @param completionHandler callback
 */
+ (void)createTransaction:(NSArray * _Nullable)args withOrigin:(NSString * _Nullable)origin withAdditionalInfo:(NSDictionary * _Nullable) completionHandler:(void (^_Nullable)(MBTransaction  * _Nullable transaction, NSError  * _Nullable  error)) callback;

/*!
 * Cria uma nova transação
 * Callback:
 *   - @b MBTransaction => Transação
 *   - @b NSError => erro
 * @param args dados da transação
 * @param origin Origem
 * @param completionHandler callback
 */

+ (void)createTransaction:(NSArray * _Nullable)args withCvv:(NSString * _Nullable)cvv withOrigin:(NSString * _Nullable)origin withAdditionalInfo:(NSDictionary * _Nullable)additionalInfo withExtra:(NSDictionary * _Nullable)extra completionHandler:(void (^_Nullable)(MBTransaction * _Nullable transaction, NSError * _Nullable error)) callback;

/*!
 * Cria uma nova transação P2P
 *
 * Callback:
 *
 *   - @b PPP2PTransaction => Transação
 *
 *   - @b NSError => erro
 *
 * @param args dados da transação
 * @param completionHandler callback
 */
+ (void)createP2PTransaction:(P2PTransactionRequest * _Nonnull)request completionHandler:(void (^_Nullable)(PPP2PTransaction * _Nullable transaction, NSError * _Nullable error)) callback;

# pragma mark Chat Methods

/*
 * Check soft descriptor of card Visa and Master through AnalysisAcceleration screen
 * Callback:
 *   - @b NSError => error
 * @param transaction_id transaction id
 * @param verification_code code
 * @param action_type action type
 * @param completionHandler callback
 */
+ (void)setSoftDescriptorByActionType:(NSString *_Nonnull)transaction_id verification_code:(NSString *_Nonnull)verification_code action_type:(NSString *_Nonnull)action_type completionHandler:(void (^_Nullable)(NSString * _Nullable  data, NSError * _Nullable  error))callback;


@end
