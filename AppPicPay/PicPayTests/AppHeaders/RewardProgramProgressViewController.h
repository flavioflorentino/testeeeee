//
//  RewardProgramProgressViewController.h
//  PicPay
//
//  Created by Diogo Carneiro on 20/03/13.
//
//

#import <UIKit/UIKit.h>
#import "PPBaseViewControllerObj.h"
@import CoreLegacy;

@interface RewardProgramProgressViewController : PPBaseViewControllerObj

@property (strong, nonatomic) PPRewardProgram *rewardProgram;

@property (strong, nonatomic) IBOutlet UIImageView *imageViewProgressBar;
@property (strong, nonatomic) IBOutlet UILabel *labelRewardProgramDescription;
@property (strong, nonatomic) IBOutlet UILabel *labelRewardProgressDescription;
@property (strong, nonatomic) IBOutlet UILabel *labelRewardProgressMin;
@property (strong, nonatomic) IBOutlet UILabel *labelRewardProgressMax;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewProgressBarBackground;
@property (strong, nonatomic) IBOutlet UIView *viewFidelityBottomInfo;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewSeparator;

- (void)animateProgressBarToCurrentProgress;

@end
