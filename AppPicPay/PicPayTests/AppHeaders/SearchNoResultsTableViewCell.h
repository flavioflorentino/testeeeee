//
//  SearchNoResultsTableViewCell.h
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 30/06/16.
//
//

#import <UIKit/UIKit.h>

@interface SearchNoResultsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
