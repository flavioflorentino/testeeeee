#import <Foundation/Foundation.h>
#import <LocalAuthentication/LocalAuthentication.h>

typedef enum PPAuthTempTokenStatus : NSUInteger
{
	PPAuthTempTokenStatusInexistent,
	PPAuthTempTokenStatusLogin,
	PPAuthTempTokenStatusRegister
} PPAuthTempTokenStatus;

@class TouchIdRequestPopUpController;
@class PopupPresenter;

@protocol PPAuthDelegate <NSObject>

@optional

- (void)biometricAuthenticationWillUseNetwork;
- (void)biometricAuthenticationDidUseNetwork;
- (void)biometricAuthenticationStatus:(BOOL)enabled;

@end

@interface PPAuth : NSObject <PPAuthDelegate>{
	LAContext *context;
	NSString *reason;
	NSDictionary *alertMessages;
	TouchIdRequestPopUpController* touchIdRequestView;
	
	UIView *loaderView;
	PPAuth *auxiliarAuthComponent;
	PPAuth *auxiliarErrorTreatmentAuthComponent;
    PopupPresenter* popupPresenter;
}

@property (strong, nonatomic) id<PPAuthDelegate> delegate;
@property (copy)void (^successBlock)(NSString *authToken, BOOL biometry);
@property (copy)void (^canceledByUserBlock)(void);
@property (strong, nonatomic) NSTimer *automaticCancelTimer;
@property NSTimeInterval timeInterval;

- (void)handleError:(NSError *)error callback:(void(^)(NSError *error))callback;
- (BOOL)touchIdAvailable;
+ (PPAuth *)authenticate:(void(^)(NSString *authToken, BOOL biometry))successBlock canceledByUserBlock:(void(^)(void))canceledByUserBlock;
+ (PPAuth *)authenticateAutoCancel:(NSTimeInterval)time success:(void(^)(NSString *authToken, BOOL biometry))successBlock canceledByUserBlock:(void(^)(void))canceledByUserBlock;
- (void)enableBiometricAuthentication;
- (void)biometricAuthenticationWelcomeAlertForViewController:(UIViewController *)viewController withCompletion:(void(^)(void))callback;
- (void)disableBiometricAuthentication;
+ (void)clearAuthenticationData;
+ (BOOL)useTouchId;
+ (void)setTemporaryAuthenticationToken:(NSString *)token forType:(PPAuthTempTokenStatus)status;
+ (void)shouldAskForBiometricAuthentication:(void(^)(void))success
                              noAskBiometry:(void(^)(void))noAskBiometry;
+ (void)setUseTouchId:(BOOL)use;

/*!
 * Update the authentication token for touch id
 */
+ (void)updateBiometricAuthenticationTokenIfNeed:(NSString* )pin;

@end
