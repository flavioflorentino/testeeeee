//
//  PPBaseViewControllerObj.h
//  PicPay
//
//  Created by Marcos Timm on 01/02/2018.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@protocol Breadcrumbable;
#pragma clang diagnostic push
// Se livrar do warning 'Cannot find protocol definition' pois não está certo
#pragma clang diagnostic ignored "-Weverything"
@interface PPBaseViewControllerObj : UIViewController <Breadcrumbable> {
}
#pragma clang diagnostic pop

@property BOOL shouldAddBreadcrumb;

- (BOOL) navigationBarIsTransparent;
- (void) setShouldShowBarWhenDisappear:(BOOL) showBar;
- (BOOL) shouldShowBarWhenDisappear;

@end
