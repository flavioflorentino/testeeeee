//
//  PPPaymentManager.h
//  PicPay
//
//  Created by Diogo Carneiro on 24/10/14.
//
//

#import <Foundation/Foundation.h>

#import "PPPaymentCalculator.h"
@class PPPaymentConfig;

@protocol PaymentManagerContract <NSObject>

@property (nonatomic) NSInteger selectedQuotaQuantity;
@property (strong, nonatomic) NSDecimalNumber *subtotal;

- (void)changePaymentConfig:(PPPaymentConfig *)newConfig;
- (NSDecimalNumber *)total;
- (NSDecimalNumber *)cardTotal;
- (NSDecimalNumber *)cardTotalWithoutInterest;
- (NSDecimalNumber *)balanceTotal;
- (NSDecimalNumber *)balance;
- (NSDecimalNumber *)quotaValue;
- (BOOL)installmentEnabled;
- (BOOL)installmentAvailable;
- (NSArray *)installmentsList;
- (BOOL)usePicPayBalance;
- (PPPaymentConfig *) config;
@end

@interface PPPaymentManager : NSObject <PaymentManagerContract> {
	PPPaymentConfig *paymentConfig;
}

@property (nonatomic) NSInteger selectedQuotaQuantity;
@property (strong, nonatomic) NSDecimalNumber *subtotal;
@property (strong, nonatomic) NSDecimalNumber *sellerLocalDiscount;
@property (strong, nonatomic) NSDecimalNumber *cardFee;
@property (strong, nonatomic) NSDecimalNumber *fixedFee;
@property (strong, nonatomic) PPPaymentCalculator *calculator;
@property BOOL forceCreditCard;

- (id)init;
- (void)changePaymentConfig:(PPPaymentConfig *)newConfig;
- (NSDecimalNumber *)total;
- (NSDecimalNumber *)cardTotal;
- (NSDecimalNumber *)cardTotalWithCardConvenienceFee;
- (NSDecimalNumber *)cardTotalWithoutInterest;
- (NSDecimalNumber *)cardConvenienceFee;
- (NSDecimalNumber *)installmentsTotalFee;
- (NSDecimalNumber *)balanceTotal;
- (NSDecimalNumber *)balance;
- (NSDecimalNumber *)quotaValue;
- (BOOL)installmentEnabled;
- (BOOL)installmentAvailable;
- (NSArray *)installmentsList;
- (BOOL)usePicPayBalance;

- (PPPaymentConfig *) config;

@end
