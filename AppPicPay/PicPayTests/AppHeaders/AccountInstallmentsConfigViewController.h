//
//  AccountInstallmentsConfigViewController.h
//  PicPay
//
//  Created by Diogo Carneiro on 10/11/16.
//
//

#import <UIKit/UIKit.h>
#import "PPBaseViewControllerObj.h"

@class UILoadView;

@interface AccountInstallmentsConfigViewController : PPBaseViewControllerObj <UITableViewDelegate, UITableViewDataSource>{
	UILoadView *loaderView;
	NSArray *installments;
	NSArray *selectedInstallmentToSave;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
