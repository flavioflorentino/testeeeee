import XCTest
@testable import PicPay

private final class SignUpViewModelMock: SignUpViewModelInputs {
    func viewDidLoad() {}
    func didTapRegisterButton() {}
    func didTapLoginButton() {}
    func didTapUsePromoCodeButton() {}
    func didTapHelpButton() {}
    func newConversationStarted() {}
    func didActivatePromoCode(validPromoCode: ValidPromoCode) {}
}

class SessionNotificationHandlerTest: XCTestCase {
    var controllers: [UIViewController]!
    let notificationHandler = SessionNotificationHandler()
    
    // MARK: - User not authenticated handler
    
    func testIsNotPublicVCForUserNotAuthenticatedA() {
        let controller = SignUpViewController(viewModel: SignUpViewModelMock())
        XCTAssertFalse(notificationHandler.isNotPublicVCForUserNotAuthenticated(controller))
    }
    
    func testIsNotPublicVCForUserNotAuthenticatedB() {
        let controller = UIViewController()
        XCTAssertTrue(notificationHandler.isNotPublicVCForUserNotAuthenticated(controller))
    }
    
    func testIsNotPublicVCForUserNotAuthenticatedC() {
        let controller = UIViewController()
        _ = PPPublicNavigationController(rootViewController: controller)
        XCTAssertFalse(notificationHandler.isNotPublicVCForUserNotAuthenticated(controller))
    }
    
    func testIsNotPublicVCForUserNotAuthenticatedD() {
        let controller = UIViewController()
        _ = UINavigationController(rootViewController: controller)
        XCTAssertTrue(notificationHandler.isNotPublicVCForUserNotAuthenticated(controller))
    }
    
    func testIsNotPublicVCForUserNotAuthenticatedE() {
        let controller = SignUpViewController(viewModel: SignUpViewModelMock())
        _ = PPPublicNavigationController(rootViewController: controller)
        XCTAssertFalse(notificationHandler.isNotPublicVCForUserNotAuthenticated(controller))
    }
    
    func testIsNotPublicVCForUserNotAuthenticatedF() {
        let controller = SignUpViewController(viewModel: SignUpViewModelMock())
        _ = UINavigationController(rootViewController: controller)
        XCTAssertFalse(notificationHandler.isNotPublicVCForUserNotAuthenticated(controller))
    }
    
    // MARK: - User blocked handler
    
    func testIsNotPublicVCForUserBlockedA() {
        let controller = SignUpViewController(viewModel: SignUpViewModelMock())
        XCTAssertFalse(notificationHandler.isNotPublicVCForUserBlocked(controller))
    }
    
    func testIsNotPublicVCForUserBlockedB() {
        let controller = UIViewController()
        XCTAssertTrue(notificationHandler.isNotPublicVCForUserBlocked(controller))
    }
    
    func testIsNotPublicVCForUserBlockedC() {
        let controller = UIViewController()
        _ = PPPublicNavigationController(rootViewController: controller)
        XCTAssertFalse(notificationHandler.isNotPublicVCForUserBlocked(controller))
    }
    
    func testIsNotPublicVCForUserBlockedD() {
        let controller = UIViewController()
        _ = UINavigationController(rootViewController: controller)
        XCTAssertTrue(notificationHandler.isNotPublicVCForUserBlocked(controller))
    }
    
    func testIsNotPublicVCForUserBlockedE() {
        let controller = SignUpViewController(viewModel: SignUpViewModelMock())
        _ = PPPublicNavigationController(rootViewController: controller)
        XCTAssertFalse(notificationHandler.isNotPublicVCForUserBlocked(controller))
    }
    
    func testIsNotPublicVCForUserBlockedF() {
        let controller = SignUpViewController(viewModel: SignUpViewModelMock())
        _ = UINavigationController(rootViewController: controller)
        XCTAssertFalse(notificationHandler.isNotPublicVCForUserBlocked(controller))
    }
}
