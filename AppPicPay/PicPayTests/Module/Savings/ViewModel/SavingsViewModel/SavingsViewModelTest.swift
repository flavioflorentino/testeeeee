//
//  SavingsViewModelTest.swift
//  PicPayTests
//
//  Created by PicPay-DEV-PP on 03/12/18.
//

import XCTest
@testable import PicPay

class SavingsViewModelTest: XCTestCase {
    enum SavingsSuccessJSON {
        case oldAccount
        case newAccount
        
        var resource: String {
            switch self {
            case .oldAccount:
                return "OldAccountSavings"
            case .newAccount:
                return "NewAccountSavings"
            }
        }
    }
    
    lazy var testTableView: UITableView = {
        let tableView = UITableView()
        tableView.registerCell(type: SavingsInfoTableViewCell.self)
        tableView.registerCell(type: SavingsButtonsTableViewCell.self)
        tableView.registerCell(type: SavingsExplanationTableViewCell.self)
        tableView.registerCell(type: ProjectedInfoTableViewCell.self)
        return tableView
    }()
    
    func viewModel(for resource: SavingsSuccessJSON) -> SavingsViewModel {
        let json = MockJSON().load(resource: resource.resource)
        let apiSavings = ApiSavingsSuccessMock(json: json)
        return SavingsViewModel(with: apiSavings)
    }
    
    func testOldAccountSavings() -> SavingsViewModel {
        let model = viewModel(for: .oldAccount)
        model.loadSavings { (error) in
            XCTAssert(error == nil)
        }
        
        return model
    }
    
    func testNewAccountSavings() {
        let model = viewModel(for: .newAccount)
        model.loadSavings { (error) in
            XCTAssert(error == nil)
        }
    }
    
    func testErrorSavings() {
        let model = SavingsViewModel(with: ApiSavingsErrorMock())
        model.loadSavings { (error) in
            XCTAssert(error != nil)
        }
    }
    
    func testErrorTableViewHeaderProjected() {
        let model = testOldAccountSavings()
        model.setSelectedValue(index: 5)
        XCTAssert(String(describing: model.getTableViewHeader().self) != String(describing: SavingsInfoHeaderView.self))
    }
    
    
    func testNumberOfRowsToday() {
        let model = testOldAccountSavings()
        XCTAssert(model.numberOfRowsInSection() == 2)
    }
    
    func testNumberOfRowsPast() {
        let model = testOldAccountSavings()
        model.setSelectedValue(index: 0)
        XCTAssert(model.numberOfRowsInSection() == 1)
    }
    
    func testNumberOfRowsProjected() {
        let model = testOldAccountSavings()
        model.setSelectedValue(index: 5)
        XCTAssert(model.numberOfRowsInSection() == 1)
    }
    
    func testErrorNumberOfRowsToday() {
        let model = testOldAccountSavings()
        XCTAssertFalse(model.numberOfRowsInSection() != 2)
    }
    
    func testErrorNumberOfRowsPast() {
        let model = testOldAccountSavings()
        model.setSelectedValue(index: 0)
        XCTAssertFalse(model.numberOfRowsInSection() != 1)
    }
    
    func testErrorNumberOfRowsProjected() {
        let model = testOldAccountSavings()
        model.setSelectedValue(index: 5)
        XCTAssertFalse(model.numberOfRowsInSection() != 1)
    }
    
    func testCellForSavingTodayAndPast() {
        let model = testOldAccountSavings()
        XCTAssert(model.cellForSaving(tableView: testTableView, indexPath: IndexPath(row: 0, section: 0)) is SavingsExplanationTableViewCell)
        XCTAssert(model.cellForSaving(tableView: testTableView, indexPath: IndexPath(row: 1, section: 0)) is SavingsButtonsTableViewCell)
        
        
        model.setSelectedValue(index: 0)
        XCTAssert(model.cellForSaving(tableView: testTableView, indexPath: IndexPath(row: 0, section: 0)) is SavingsExplanationTableViewCell)
        XCTAssertNil(model.cellForSaving(tableView: testTableView, indexPath: IndexPath(row: 1, section: 0)))
    }
    
    func testNotNilCellForSavingTodayAndPast() {
        let model = testOldAccountSavings()
        XCTAssertNotNil(model.cellForSaving(tableView: testTableView, indexPath: IndexPath(row: 0, section: 0)))
        XCTAssertNotNil(model.cellForSaving(tableView: testTableView, indexPath: IndexPath(row: 1, section: 0)))
        
        
        model.setSelectedValue(index: 0)
        XCTAssertNotNil(model.cellForSaving(tableView: testTableView, indexPath: IndexPath(row: 0, section: 0)))
        XCTAssertNil(model.cellForSaving(tableView: testTableView, indexPath: IndexPath(row: 4, section: 0)))
    }
    
    func testErrorCellForSavingTodayAndPast() {
        let model = testOldAccountSavings()
        XCTAssertFalse(model.cellForSaving(tableView: testTableView, indexPath: IndexPath(row: 0, section: 0)) is SavingsButtonsTableViewCell)
        XCTAssertFalse(model.cellForSaving(tableView: testTableView, indexPath: IndexPath(row: 4, section: 0)) is SavingsExplanationTableViewCell)
        
        
        model.setSelectedValue(index: 0)
        XCTAssertFalse(model.cellForSaving(tableView: testTableView, indexPath: IndexPath(row: 0, section: 0)) is SavingsButtonsTableViewCell)
        XCTAssertFalse(model.cellForSaving(tableView: testTableView, indexPath: IndexPath(row: 4, section: 0)) is SavingsExplanationTableViewCell)
    }
    
    func testCellForSavingProjected() {
        let model = testOldAccountSavings()
        model.setSelectedValue(index: 5)
        XCTAssertNotNil(model.cellForSaving(tableView: testTableView, indexPath: IndexPath(row: 0, section: 0)))
    }
    
    func testErrorCellForSavingProjected() {
        let model = testOldAccountSavings()
        model.setSelectedValue(index: 5)
        XCTAssertTrue(model.cellForSaving(tableView: testTableView, indexPath: IndexPath(row: 0, section: 0)) != nil)
    }
    
    func testSelectedValueInvalid() {
        let model = testOldAccountSavings()
        model.setSelectedValue(index: 999999)
        XCTAssertNil(model.selectedSaving)
    }
    
    func testErrorSelectedValueInvalid() {
        let model = testOldAccountSavings()
        model.setSelectedValue(index: 999999)
        XCTAssertFalse(model.selectedSaving != nil)
    }
    
    func testGetMaxValue() {
        let model = testOldAccountSavings()
        XCTAssert(model.savings.getMaxElementValue() == 15.020209)
    }
    
    func testErrorGetMaxValue() {
        let model = testOldAccountSavings()
        XCTAssertFalse(model.savings.getMaxElementValue() == 0.0)
    }
    
    func testGetPresentElement() {
        let model = testOldAccountSavings()
        XCTAssert(model.savings.getPresentElement()!.type == .today)
    }
    
    func testErrorGetPresentElement() {
        let model = testOldAccountSavings()
        XCTAssertFalse(model.savings.getPresentElement()!.type == .past)
    }
}
