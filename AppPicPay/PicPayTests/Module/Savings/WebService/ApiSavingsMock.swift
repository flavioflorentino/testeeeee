//
//  ApiSavingsMock.swift
//  PicPayTests
//
//  Created by PicPay-DEV-PP on 03/12/18.
//
import Foundation
import SwiftyJSON
@testable import PicPay

class ApiSavingsSuccessMock: ApiSavingsProtocol {
    private let json: JSON!
    
    init(json: JSON) {
        self.json = json
    }
    
    func getSavings(completion: @escaping (PicPayResult<BaseApiListResponse<Saving>>) -> Void) {
        let listResult = BaseApiListResponse<Saving>(json: json)!
        let result = PicPayResult.success(listResult)
        completion(result)
    }
}


class ApiSavingsErrorMock: ApiSavingsProtocol {

    func getSavings(completion: @escaping (PicPayResult<BaseApiListResponse<Saving>>) -> Void) {
        let result = PicPayResult<BaseApiListResponse<Saving>>.failure(PicPayError(message: "Error"))
        completion(result)
    }
}
