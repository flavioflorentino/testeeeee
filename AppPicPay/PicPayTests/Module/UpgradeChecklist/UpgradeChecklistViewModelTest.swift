import XCTest
@testable import PicPay

class UpgradeChecklistViewModelTest: XCTestCase {
    func viewModelSuccess(status: [Status] = [], identityStatus: SetupChecklistInfo.IdentityStatus = .verified, resource: String = "checklistInfoAllInfo") -> UpgradeChecklistViewModel {
        let worker = UpgradeAccountWorkerSuccess(resource: .checklistInfoAllInfo)
        let model = try! MockCodable<SetupChecklistInfo>().loadCodableObject(resource: resource, typeDecoder: .useDefaultKeys)
        model.validateIdentity = identityStatus
        if !status.contains(.address) {
            model.addressId = nil
        }
        if !status.contains(.income) {
            model.incomeRange = nil
        }
        if !status.contains(.mother) {
            model.nameMother = nil
        }
        if !status.contains(.identity) {
            model.validateIdentity = .notCreated
        }
        
        return UpgradeChecklistViewModel(model: model, worker: worker)
    }
    
    func createCheckList() -> SetupChecklistInfo {
        return try! MockCodable<SetupChecklistInfo>().loadCodableObject(resource: "checklistInfoAllInfo", typeDecoder: .useDefaultKeys)
    }
    
    func viewModelFailure() -> UpgradeChecklistViewModel {
        let worker = UpgradeAccountWorkerFailure()
        let model = try! MockCodable<SetupChecklistInfo>().loadCodableObject(resource: "checklistInfoAllInfo", typeDecoder: .useDefaultKeys)
        return UpgradeChecklistViewModel(model: model, worker: worker)
    }
    
    func testIsMotherNameChecked() {
        let viewModel = viewModelSuccess(status: [.mother])
        let result = viewModel.motherNameStatus()
        XCTAssertEqual(result, UpgradeChecklistViewModel.OptiontStatus.checked)
    }
    
    func testIsMotherNameCheckedPending() {
        let viewModel = viewModelSuccess()
        let result = viewModel.motherNameStatus()
        XCTAssertEqual(result, UpgradeChecklistViewModel.OptiontStatus.pending)
    }
    
    func testIsAddressChecked() {
        let viewModel = viewModelSuccess(status: [.address])
        let result = viewModel.addressStatus()
        XCTAssertEqual(result, UpgradeChecklistViewModel.OptiontStatus.checked)
    }
    
    func testIsAddressCheckedPending() {
        let viewModel = viewModelSuccess()
        let result = viewModel.addressStatus()
        XCTAssertEqual(result, UpgradeChecklistViewModel.OptiontStatus.pending)
    }
    
    func testIsIncomeRangeChecked() {
        let viewModel = viewModelSuccess(status: [.income])
        let result = viewModel.incomeRangeStatus()
        XCTAssertEqual(result, UpgradeChecklistViewModel.OptiontStatus.checked)
    }
    
    func testIsIncomeRangeCheckedPending() {
        let viewModel = viewModelSuccess()
        let result = viewModel.incomeRangeStatus()
        XCTAssertEqual(result, UpgradeChecklistViewModel.OptiontStatus.pending)
    }
    
    func testIsValidateIdentityChecked() {
        let viewModel = viewModelSuccess(status: [.identity], identityStatus: .verified)
        let result = viewModel.validateIdentityStatus()
        XCTAssertEqual(result, UpgradeChecklistViewModel.OptiontStatus.checked)
    }
    
    func testIsValidateIdentityPending() {
        let viewModel = viewModelSuccess()
        let result = viewModel.validateIdentityStatus()
        XCTAssertEqual(result, UpgradeChecklistViewModel.OptiontStatus.pending)
    }
    
    func testIsValidateIdentityAnalysis() {
        let viewModel = viewModelSuccess(status: [.identity], identityStatus: .toVerify)
        let result = viewModel.validateIdentityStatus()
        XCTAssertEqual(result, UpgradeChecklistViewModel.OptiontStatus.analysis)
    }
    
    func testIsValidateIdentityDisabled() {
        let viewModel = viewModelSuccess(status: [.identity], identityStatus: .disabled)
        let result = viewModel.validateIdentityStatus()
        XCTAssertEqual(result, UpgradeChecklistViewModel.OptiontStatus.disabled)
    }

    func testChecklistProgress1() {
        let viewModel1 = viewModelSuccess(status: [.income])
        let result1 = viewModel1.checklistProgress
        XCTAssertEqual(result1, 1/4)
        
        let viewModel2 = viewModelSuccess(status: [.mother])
        let result2 = viewModel2.checklistProgress
        XCTAssertEqual(result2, 1/4)
        
        let viewModel3 = viewModelSuccess(status: [.address])
        let result3 = viewModel3.checklistProgress
        XCTAssertEqual(result3, 1/4)
        
        let viewModel4 = viewModelSuccess(status: [.identity])
        let result4 = viewModel4.checklistProgress
        XCTAssertEqual(result4, 1/4)
    }
    
    func testChecklistProgress2() {
        let viewModel1 = viewModelSuccess(status: [.income, .mother])
        let result1 = viewModel1.checklistProgress
        XCTAssertEqual(result1, 2/4)
        
        let viewModel2 = viewModelSuccess(status: [.mother, .address])
        let result2 = viewModel2.checklistProgress
        XCTAssertEqual(result2, 2/4)
        
        let viewModel3 = viewModelSuccess(status: [.address, .income])
        let result3 = viewModel3.checklistProgress
        XCTAssertEqual(result3, 2/4)
        
        let viewModel4 = viewModelSuccess(status: [.identity, .income])
        let result4 = viewModel4.checklistProgress
        XCTAssertEqual(result4, 2/4)
        
        let viewModel5 = viewModelSuccess(status: [.identity, .mother])
        let result5 = viewModel5.checklistProgress
        XCTAssertEqual(result5, 2/4)
        
        let viewModel6 = viewModelSuccess(status: [.identity, .address])
        let result6 = viewModel6.checklistProgress
        XCTAssertEqual(result6, 2/4)
    }
    
    func testChecklistProgress3() {
        let viewModel1 = viewModelSuccess(status: [.address, .income, .mother])
        let result1 = viewModel1.checklistProgress
        XCTAssertEqual(result1, 3/4)
        
        let viewModel2 = viewModelSuccess(status: [.identity, .income, .mother])
        let result2 = viewModel2.checklistProgress
        XCTAssertEqual(result2, 3/4)
        
        let viewModel3 = viewModelSuccess(status: [.address, .identity, .mother])
        let result3 = viewModel3.checklistProgress
        XCTAssertEqual(result3, 3/4)
        
        let viewModel4 = viewModelSuccess(status: [.address, .income, .identity])
        let result4 = viewModel4.checklistProgress
        XCTAssertEqual(result4, 3/4)
    }
    
    func testChecklistProgress4() {
        let viewModel = viewModelSuccess(status: [.address, .income, .mother, .identity])
        let result = viewModel.checklistProgress
        XCTAssertEqual(result, 1)
    }
    
    func testTotalItemsAll() {
        let viewModel = viewModelSuccess(status: [.mother, .income, .address, .identity])
        let result = viewModel.totalItems
        XCTAssertEqual(result, 4)
    }
    
    func testTotalItemsWithoutIdentity() {
        let viewModel = viewModelSuccess(status: [.mother, .income, .address, .identity], identityStatus: .disabled)
        let result = viewModel.totalItems
        XCTAssertEqual(result, 3)
    }
    
    func testMotherName() {
        let viewModel = viewModelSuccess(status: [.mother, .income, .address, .identity])
        let result = viewModel.motherName()
        XCTAssertEqual(result, "Mother")
    }
    
    func testIncomeRange() {
        let viewModel = viewModelSuccess(status: [.mother, .income, .address, .identity])
        let result = viewModel.incomeRange()
        XCTAssertEqual(result, "A")
    }
    
    func testAddressId() {
        let viewModel = viewModelSuccess(status: [.mother, .income, .address, .identity])
        let result = viewModel.addressId()
        XCTAssertEqual(result, "2")
    }
    
    func testSetMotherName() {
        let viewModel = viewModelSuccess(status: [.mother, .income, .address, .identity], resource: "checklistInfoAllNull")
        viewModel.setChecklist(upgrade: createCheckList())
        let result = viewModel.motherName()
        XCTAssertEqual(result, "Mother")
    }
    
    func testSetIncomeRange() {
       let viewModel = viewModelSuccess(status: [.mother, .income, .address, .identity], resource: "checklistInfoAllNull")
        viewModel.setChecklist(upgrade: createCheckList())
        let result = viewModel.incomeRange()
        XCTAssertEqual(result, "A")
    }
    
    func testSetAddressId() {
        let viewModel = viewModelSuccess(status: [.mother, .income, .address, .identity], resource: "checklistInfoAllNull")
        viewModel.setChecklist(upgrade: createCheckList())
        let result = viewModel.addressId()
        XCTAssertEqual(result, "2")
    }
    
    func testSetValidateIdentity() {
        let viewModel = viewModelSuccess(status: [.mother, .income, .address, .identity], resource: "checklistInfoAllNull")
        viewModel.setChecklist(upgrade: createCheckList())
        let result = viewModel.validateIdentityStatus()
        XCTAssertEqual(result, UpgradeChecklistViewModel.OptiontStatus.pending)
    }
    
    func testSelectAddressSuccess() {
        let viewModel = viewModelSuccess(status: [.mother, .income, .address])
        let address = viewModel.addressId()
        let expectation = self.expectation(description: "API")
        
        viewModel.selectAddress(with: address, onSuccess: { result in
            XCTAssertNotNil(result)
            expectation.fulfill()
        }, onError: { (_) in
            XCTFail()
        })
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testSelectAddressError() {
        let viewModel = viewModelFailure()
        let address = viewModel.addressId()
        let expectation = self.expectation(description: "API")
        
        viewModel.selectAddress(with: address, onSuccess: {_ in
            XCTFail()
        }, onError: { (error) in
            XCTAssertNotNil(error)
            expectation.fulfill()
        })
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testConfirmUpgradeChecklistSuccess() {
        let viewModel = viewModelSuccess(status: [.mother, .income, .address])
        let expectation = self.expectation(description: "API")
        
        viewModel.confirmUpgradeChecklist(onSuccess: {
            expectation.fulfill()
        }, onError: { (_) in
            XCTFail()
        })
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testConfirmUpgradeChecklistError() {
        let viewModel = viewModelFailure()
        let expectation = self.expectation(description: "API")
        
        viewModel.confirmUpgradeChecklist(onSuccess: {
            XCTFail()
        }, onError: { (error) in
            XCTAssertNotNil(error)
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    
}

extension UpgradeChecklistViewModelTest {
    enum Status {
        case mother
        case address
        case income
        case identity
    }
}
