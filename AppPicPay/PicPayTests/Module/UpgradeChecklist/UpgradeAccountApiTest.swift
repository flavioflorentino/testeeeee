import XCTest
import SwiftyJSON
@testable import PicPay

enum UpgradeAccountResource: String {
    case incomeRangeOptionsSuccess
    case checklistInfoApproved
    case checklistInfoPending
    case checklistInfoRejected
    case checklistInfoAllInfo
    case none
}

class UpgradeAccountWorkerSuccess: UpgradeAccountWorkerProtocol {
    let resource: UpgradeAccountResource
    
    init(resource: UpgradeAccountResource) {
        self.resource = resource
    }
    
    func incomeRangeOptions(completion: @escaping (PicPayResult<[IncomeRangeOption]>) -> Void) {
        let incomeRangeOptions = try! MockCodable<[IncomeRangeOption]>().loadCodableObject(resource: resource.rawValue)
        completion(PicPayResult<[IncomeRangeOption]>.success(incomeRangeOptions))
    }
    
    func setupChecklistInfo(completion: @escaping (PicPayResult<SetupChecklistInfo>) -> Void) {
        let checklistInfo = try! MockCodable<SetupChecklistInfo>().loadCodableObject(resource: resource.rawValue, typeDecoder: .useDefaultKeys)
        completion(PicPayResult<SetupChecklistInfo>.success(checklistInfo))
    }
    
    func confirmUpgradeChecklist(completion: @escaping (PicPayResult<BaseApiGenericResponse>) -> Void) {
        let json = MockJSON().load(resource: "ProfileImageUpload")
        completion(PicPayResult<BaseApiGenericResponse>.success(BaseApiGenericResponse(json: json)!))
    }
    
    func confirmVerificationIdentity(completion: @escaping (PicPayResult<BaseApiGenericResponse>) -> Void) {
        let json = MockJSON().load(resource: "ProfileImageUpload")
        completion(PicPayResult<BaseApiGenericResponse>.success(BaseApiGenericResponse(json: json)!))
    }
    
    func selectRangeOption(_ incomeRange: String, completion: @escaping (PicPayResult<SetupChecklistInfo>) -> Void) {
        let checklistInfo = try! MockCodable<SetupChecklistInfo>().loadCodableObject(resource: resource.rawValue, typeDecoder: .useDefaultKeys)
        completion(PicPayResult<SetupChecklistInfo>.success(checklistInfo))
    }
    
    func selectAddress(with id: String, completion: @escaping (PicPayResult<SetupChecklistInfo>) -> Void) {
        let checklistInfo = try! MockCodable<SetupChecklistInfo>().loadCodableObject(resource: resource.rawValue, typeDecoder: .useDefaultKeys)
        completion(PicPayResult<SetupChecklistInfo>.success(checklistInfo))
    }
    
    func updateMotherName(motherName: String, completion: @escaping (PicPayResult<SetupChecklistInfo>) -> Void) {
        let checklistInfo = try! MockCodable<SetupChecklistInfo>().loadCodableObject(resource: resource.rawValue, typeDecoder: .useDefaultKeys)
        completion(PicPayResult<SetupChecklistInfo>.success(checklistInfo))
    }
    
}

class UpgradeAccountWorkerFailure: UpgradeAccountWorkerProtocol {
    func selectRangeOption(_ incomeRange: String, completion: @escaping (PicPayResult<SetupChecklistInfo>) -> Void) {
        completion(PicPayResult<SetupChecklistInfo>.failure(PicPayError(message: "Error")))
    }
    
    func selectAddress(with id: String, completion: @escaping (PicPayResult<SetupChecklistInfo>) -> Void) {
        completion(PicPayResult<SetupChecklistInfo>.failure(PicPayError(message: "Error")))
    }
    
    func updateMotherName(motherName: String, completion: @escaping (PicPayResult<SetupChecklistInfo>) -> Void) {
        completion(PicPayResult<SetupChecklistInfo>.failure(PicPayError(message: "Error")))
    }
    
    func incomeRangeOptions(completion: @escaping (PicPayResult<[IncomeRangeOption]>) -> Void) {
        completion(PicPayResult<[IncomeRangeOption]>.failure(PicPayError(message: "Error")))
    }
    
    func setupChecklistInfo(completion: @escaping (PicPayResult<SetupChecklistInfo>) -> Void) {
        completion(PicPayResult<SetupChecklistInfo>.failure(PicPayError(message: "Error")))
    }
    
    func confirmUpgradeChecklist(completion: @escaping (PicPayResult<BaseApiGenericResponse>) -> Void) {
        completion(PicPayResult<BaseApiGenericResponse>.failure(PicPayError(message: "Error")))
    }
    
    func confirmVerificationIdentity(completion: @escaping (PicPayResult<BaseApiGenericResponse>) -> Void) {
        completion(PicPayResult<BaseApiGenericResponse>.failure(PicPayError(message: "Error")))
    }
}
