import XCTest
@testable import PicPay

class IncomeRangeViewModelTest: XCTestCase {
    func viewModelFor(success: Bool, post: Bool = false) -> IncomeRangeViewModel {
        if post {
            return IncomeRangeViewModel(worker: success ? UpgradeAccountWorkerSuccess(resource: .checklistInfoAllInfo) : UpgradeAccountWorkerFailure())
        } else {
            return IncomeRangeViewModel(worker: success ? UpgradeAccountWorkerSuccess(resource: .incomeRangeOptionsSuccess) : UpgradeAccountWorkerFailure())
        }
    }
    
    func testIncomeRangeOptionSuccess() -> IncomeRangeViewModel {
        let viewModel = viewModelFor(success: true)
        let expectation = XCTestExpectation(description: "IncomeRangeOptions")
        
        viewModel.incomeRangeOptions(onSucess: {
            XCTAssertEqual(viewModel.numberOfRows(), 5)
            expectation.fulfill()
        }, onError: { (_) in
            XCTFail()
        })
        wait(for: [expectation], timeout: 5)
        return viewModel
    }
    
    func testIncomeRangeOptionFailure() {
        let viewModel = viewModelFor(success: false)
        let expectation = XCTestExpectation(description: "IncomeRangeOptions")
        viewModel.incomeRangeOptions(onSucess: {
             XCTFail()
        }, onError: { (error) in
            XCTAssertNotNil(error)
            XCTAssertEqual(viewModel.numberOfRows(), 0)
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 5)
    }
    
    func testSelectIncomeRangeSuccess() {
        let viewModel = viewModelFor(success: true, post: true)
        let expectation = XCTestExpectation(description: "IncomeRangeOptions")
        
        viewModel.selectIncomeRange(id: "A", onSuccess: { result in
            XCTAssertNotNil(result)
            expectation.fulfill()
        }, onError: { (_) in
            XCTFail()
        })
        
        wait(for: [expectation], timeout: 5)
    }
    
    func testSelectIncomeRangeFailure() {
        let viewModel = viewModelFor(success: false, post: true)
        let expectation = XCTestExpectation(description: "IncomeRangeOptions")
        
        viewModel.selectIncomeRange(id: "A", onSuccess: { _ in
            XCTFail()
        }, onError: { (error) in
            XCTAssertNotNil(error)
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 5)
    }
    
    func testNumberOfRowsSuccess() {
        let viewModel = testIncomeRangeOptionSuccess()
        XCTAssertEqual(viewModel.numberOfRows(), 5)
    }
    
    func testNumberOfRowsFailure() {
        let viewModel = viewModelFor(success: false)
        XCTAssertEqual(viewModel.numberOfRows(), 0)
    }
    
    func testIncomeOptionSelectedSuccess() {
        let viewModel = testIncomeRangeOptionSuccess()
        
        let zero = viewModel.incomeRangeSelected(at: 0)
        XCTAssertEqual(zero.id, "A")
        XCTAssertEqual(zero.text, "De R$0,00 a R$1.000")
        
        let first = viewModel.incomeRangeSelected(at: 1)
        XCTAssertEqual(first.id, "B")
        XCTAssertEqual(first.text, "De R$1.000,00 a R$2.000")
        
        let second = viewModel.incomeRangeSelected(at: 2)
        XCTAssertEqual(second.id, "C")
        XCTAssertEqual(second.text, "De R$2.000,00 a R$5.000")
        
        let third = viewModel.incomeRangeSelected(at: 3)
        XCTAssertEqual(third.id, "D")
        XCTAssertEqual(third.text, "De R$5.000,00 a R$10.000")
        
        let fourth = viewModel.incomeRangeSelected(at: 4)
        XCTAssertEqual(fourth.id, "E")
        XCTAssertEqual(fourth.text, "Mais de R$10.000,00")
    }
    
    func testIncomeOptionSelectedFailure() {
        let viewModel = viewModelFor(success: false)
        let result = viewModel.numberOfRows()
        XCTAssertEqual(result, 0)
    }
}
