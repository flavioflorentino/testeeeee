import XCTest
@testable import PicPay

class EditMotherNameViewModelTest: XCTestCase {
    func viewModelFor(success: Bool, motherName: String = "") -> EditMotherNameViewModel {
        return EditMotherNameViewModel(motherName: motherName, worker: success ? UpgradeAccountWorkerSuccess(resource: .checklistInfoAllInfo) : UpgradeAccountWorkerFailure())
    }
    
    func testMotherNameEmpty() {
        let viewModel = viewModelFor(success: true)
        let result = viewModel.motherName()
        XCTAssertTrue(result.isEmpty)
    }
    
    func testMotherNameNotEmpty() {
        let viewModel = viewModelFor(success: true, motherName: "Gustavo")
        let result = viewModel.motherName()
        XCTAssertEqual(result, "Gustavo")
    }
    
    func testSanitizedMotherNameSuccess() {
        let viewModel = viewModelFor(success: true)
        let result1 = viewModel.sanitizedMotherName(name: "Gústavo Storck")
        let result2 = viewModel.sanitizedMotherName(name: "gustavo stõrck")
        
        XCTAssertEqual(result1, "Gústavo Storck")
        XCTAssertEqual(result2, "gustavo stõrck")
    }
    
    func testSanitizedMotherNameFail() {
        let viewModel = viewModelFor(success: true)
        let result1 = viewModel.sanitizedMotherName(name: "Gustavo1234 Storck1+01")
        let result2 = viewModel.sanitizedMotherName(name: "123gustavo 1storck✅")
        
        XCTAssertEqual(result1, "Gustavo Storck")
        XCTAssertEqual(result2, "gustavo storck")
    }
    
    func testSetMotherNameSuccess() {
        let viewModel = viewModelFor(success: true)
        let expectation = XCTestExpectation(description: "API")
        viewModel.updateMotherName(motherName: "Teste", onSuccess: { result in
            XCTAssertNotNil(result)
            expectation.fulfill()
        }, onError: { (_) in
            XCTFail()
        })
        wait(for: [expectation], timeout: 5)
    }
    
    func testSetMotherNameFail() {
        let viewModel = viewModelFor(success: false)
        let expectation = XCTestExpectation(description: "API")
        viewModel.updateMotherName(motherName: "Teste", onSuccess: { _ in
            XCTFail()
        }, onError: { (error) in
            XCTAssertNotNil(error)
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 5)
    }
}
