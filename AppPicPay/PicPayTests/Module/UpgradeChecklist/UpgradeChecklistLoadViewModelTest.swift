import XCTest
@testable import PicPay

class UpgradeChecklistLoadViewModelTest: XCTestCase {
    func viewModelSuccess(resource: UpgradeAccountResource) -> UpgradeChecklistLoadViewModel {
        let worker = UpgradeAccountWorkerSuccess(resource: resource)
        return UpgradeChecklistLoadViewModel(worker: worker)
    }
    
    func viewModelFailure() -> UpgradeChecklistLoadViewModel {
        let worker = UpgradeAccountWorkerFailure()
        return UpgradeChecklistLoadViewModel(worker: worker)
    }
    
    func testChecklistInfo() {
        let viewModel = viewModelSuccess(resource: .checklistInfoAllInfo)
        let expectation = XCTestExpectation(description: "API")
        viewModel.setupChecklistInfo(onSuccess: {
            let result = viewModel.screenStatus()
            XCTAssertEqual(result, SetupChecklistInfo.ScreenStatus.checklist)
            expectation.fulfill()
        }, onError: { (_) in
            XCTFail()
        })
        wait(for: [expectation], timeout: 5)
    }
    
    func testChecklistInfoApproved() {
        let viewModel = viewModelSuccess(resource: .checklistInfoApproved)
        let expectation = XCTestExpectation(description: "API")
        viewModel.setupChecklistInfo(onSuccess: {
            let result = viewModel.screenStatus()
            XCTAssertEqual(result, SetupChecklistInfo.ScreenStatus.approved)
            expectation.fulfill()
        }, onError: { (_) in
            XCTFail()
        })
        wait(for: [expectation], timeout: 5)
    }

    func testChecklistInfoPending() {
        let viewModel = viewModelSuccess(resource: .checklistInfoPending)
        let expectation = XCTestExpectation(description: "API")
        viewModel.setupChecklistInfo(onSuccess: {
            let result = viewModel.screenStatus()
            XCTAssertEqual(result, SetupChecklistInfo.ScreenStatus.pending)
            expectation.fulfill()
        }, onError: { (_) in
            XCTFail()
        })
        wait(for: [expectation], timeout: 5)
    }

    func testChecklistInfoRejected() {
        let viewModel = viewModelSuccess(resource: .checklistInfoRejected)
        let expectation = XCTestExpectation(description: "API")
        viewModel.setupChecklistInfo(onSuccess: {
            let result = viewModel.screenStatus()
            XCTAssertEqual(result, SetupChecklistInfo.ScreenStatus.rejected)
            expectation.fulfill()
        }, onError: { (_) in
            XCTFail()
        })
        wait(for: [expectation], timeout: 5)
    }

    func testChecklistInfoError() {
        let viewModel = viewModelFailure()
        let expectation = XCTestExpectation(description: "API")
        viewModel.setupChecklistInfo(onSuccess: {
            XCTFail()
        }, onError: { (error) in
            XCTAssertNotNil(error)
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 5)
    }

}
