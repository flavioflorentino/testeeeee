import XCTest
@testable import PicPay

class UpgradeCoordinatorTest: XCTestCase {
    func instantiateCoordinator() -> UpgradeCoordinator {
        let navigation = UINavigationController()
        let coordinator = UpgradeCoordinator(navigationController: navigation)
        coordinator.start()
        
        return coordinator
    }
    
    func testCoordinatorStart() {
        let coordinator = instantiateCoordinator()
        let rootNavigation = coordinator.rootViewController.viewControllers.last
        
        XCTAssertNotNil(rootNavigation as? UpgradeChecklistLoad)
    }
    
    func testOpenScreenChecklist() {
        let coordinator = instantiateCoordinator()
        let model = try! MockCodable<SetupChecklistInfo>().loadCodableObject(resource: "checklistInfoAllInfo", typeDecoder: .useDefaultKeys)
        coordinator.open(screen: .checklist, model: model)
        let rootNavigation = coordinator.rootViewController.viewControllers.last
        
        XCTAssertNotNil(rootNavigation as? UpgradeChecklistViewController)
    }
    
    func testOpenScreenApproved() {
        let coordinator = instantiateCoordinator()
        coordinator.open(screen: .approved,  model: nil)
        let rootNavigation = coordinator.rootViewController.viewControllers.last
        
        XCTAssertNotNil(rootNavigation as? UpgradeStatusViewController)
    }
    
    func testOpenScreenPending() {
        let coordinator = instantiateCoordinator()
        coordinator.open(screen: .pending, model: nil)
        let rootNavigation = coordinator.rootViewController.viewControllers.last
        
        XCTAssertNotNil(rootNavigation as? UpgradeStatusViewController)
    }
    
    func testOpenScreenRejected() {
        let coordinator = instantiateCoordinator()
        coordinator.open(screen: .rejected, model: nil)
        let rootNavigation = coordinator.rootViewController.viewControllers.last
        
        XCTAssertNotNil(rootNavigation as? UpgradeStatusViewController)
    }
    
    func testDidTapEditMother() {
        let coordinator = instantiateCoordinator()
        coordinator.didTapEditMother(name: "Katia Cilene")
        let rootNavigation = coordinator.rootViewController.viewControllers.last
        
        XCTAssertNotNil(rootNavigation as? EditMotherNameViewController)
    }
    
    func testDidTapIncomeRange() {
        let coordinator = instantiateCoordinator()
        coordinator.didTapIncomeRange(id: "A")
        let rootNavigation = coordinator.rootViewController.viewControllers.last
        
        XCTAssertNotNil(rootNavigation as? IncomeRangeViewController)
    }
    
    func testDidTapHelpIncomeRange() {
        let coordinator = instantiateCoordinator()
        coordinator.didTapIncomeRange(id: "A")
        coordinator.didTapHelp()
        let rootNavigation = coordinator.rootViewController.viewControllers.last
        
        XCTAssertNotNil(rootNavigation as? IncomeRangeHelpViewController)
    }
    
    func testCompleteSendUpgrade() {
        let coordinator = instantiateCoordinator()
        coordinator.completeSendUpgrade()
        let rootNavigation = coordinator.rootViewController.viewControllers.last
        
        XCTAssertNotNil(rootNavigation as? UpgradeStatusViewController)
    }
    
    func testCompleteEditMother() {
        let coordinator = instantiateCoordinator()
        let model = try! MockCodable<SetupChecklistInfo>().loadCodableObject(resource: "checklistInfoAllInfo", typeDecoder: .useDefaultKeys)
        coordinator.open(screen: .checklist, model: model)
        coordinator.didTapEditMother(name: "")
        coordinator.completeEditMother(upgrade: model)
        let rootNavigation = coordinator.rootViewController.viewControllers.last
        
        XCTAssertNotNil(rootNavigation as? UpgradeChecklistViewController)
    }
    
    func testCompleteSelectIncomeRange() {
        let coordinator = instantiateCoordinator()
        let model = try! MockCodable<SetupChecklistInfo>().loadCodableObject(resource: "checklistInfoAllInfo", typeDecoder: .useDefaultKeys)
        coordinator.open(screen: .checklist, model: model)
        coordinator.didTapIncomeRange(id: "")
        coordinator.completeSelectIncomeRange(upgrade: model)
        let rootNavigation = coordinator.rootViewController.viewControllers.last
        
        XCTAssertNotNil(rootNavigation as? UpgradeChecklistViewController)
    }
    
    func testCompleteSelectAddressList() {
        let coordinator = instantiateCoordinator()
        let model = try! MockCodable<SetupChecklistInfo>().loadCodableObject(resource: "checklistInfoAllInfo", typeDecoder: .useDefaultKeys)
        coordinator.open(screen: .checklist, model: model)
        coordinator.didTapIncomeRange(id: "")
        coordinator.completeAddressList(upgrade: model)
        let rootNavigation = coordinator.rootViewController.viewControllers.last
        
        XCTAssertNotNil(rootNavigation as? UpgradeChecklistViewController)
    }
}
