import XCTest
@testable import PicPay

class OfferCardsHelperTest: XCTestCase {
    
    var helper: OfferCardsHelper!
    
    override func setUp() {
        super.setUp()
        helper = OfferCardsHelper()
    }
    
    override func tearDown() {
        super.tearDown()
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
    }
    
    func testClosedCreditActive() {
        let worker = OfferCardsMock(with: .walletCardsCreditActive)
        let card = worker.cards[0]
        let expected: Bool = false
        helper.close(card)
        let result = helper.showed(card)
        XCTAssertEqual(expected, result)
    }
    
    func testClosedCreditActiveWithExistClosedCards() {
        let worker1 = OfferCardsMock(with: .walletCardsCreditActive)
        let worker2 = OfferCardsMock(with: .walletCardsCreditRegistration)
        let card1 = worker1.cards[0]
        let card2 = worker2.cards[0]
        let expected: Bool = false
        helper.close(card1)
        helper.close(card2)
        let result = helper.showed(card1)
        XCTAssertEqual(expected, result)
    }
    
    func testShowCardWithEmptyCardsClosed() {
        let worker = OfferCardsMock(with: .walletCardsCreditActive)
        let card = worker.cards[0]
        XCTAssertTrue(helper.showed(card))
    }
}
