import Foundation
@testable import PicPay

class OfferCardsMock: OfferCardsWorkerProtocol {
    private let mock: Mock
    var cards: [OfferCard] {
        guard let walletOfferCards = loadFromMockWith(resource: mock.rawValue) else {
            return []
        }
        
        return walletOfferCards.items
    }
    
    init(with mock: Mock) {
        self.mock = mock
    }
    
    private func loadFromMockWith(resource: String) -> WalletOfferCards? {
        guard let cards = try? MockCodable<WalletOfferCards>().loadCodableObject(resource: resource) else {
            return nil
        }

        return cards
    }
}

extension OfferCardsMock {
    enum Mock: String {
        case walletCardsCreditActive = "FeatureWalletCardsCreditActive"
        case walletCardsCreditRegistration = "FeatureWalletCardsCreditRegistration"
        case walletCardsNoRules = "FeatureWalletCardsNoRules"
    }
}
