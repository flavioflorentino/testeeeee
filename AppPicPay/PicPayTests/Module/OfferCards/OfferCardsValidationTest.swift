import FeatureFlag
import XCTest

@testable import PicPay

class OfferCardsValidationTest: XCTestCase {
    var creditAccount: CPAccount!
    var featureManagerMock = FeatureManagerMock()
    private lazy var mockDependencies = DependencyContainerMock(featureManagerMock)
    
    private func setUpAccount(with type: CreditApiMock.Account) {
        let creditWorker = CreditApiMock()
        creditWorker.accountType = type
        creditWorker.account(complete: true, isLoanFlow: false, onCacheLoaded: { (account) in
        }, completion: { result in
            switch result {
            case .success(let account):
                self.creditAccount = account
            case .failure(_):
                debugPrint("test error")
            }
        })
    }
    
    private func createDigitalAccountInfo(
        offerUpgradeAccount: DigitalAccountInfo.Card = .none,
        isJudiciallyBlocked: Bool = false) -> DigitalAccountInfo {
        return DigitalAccountInfo(offerUpgradeAccount: offerUpgradeAccount, isJudiciallyBlocked: isJudiciallyBlocked)
    }
    
    func testRuleCreditEnable_WhenValidAccount_ShouldBeTrue() {
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .completedTrue)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo)
        let rule = OfferCard.Rule.picpayCreditActive
        XCTAssertTrue(validation.isGranted(for: [rule]))
    }
    
    func testRuleCreditEnable_WheInvalidAccount_ShouldBeFalse() {
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .completedFalse)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo)
        let rule = OfferCard.Rule.picpayCreditActive
        XCTAssertFalse(validation.isGranted(for: [rule]))
    }
    
    func testRuleCreditEnableForRegistration_WhenValidAccount_ShouldBeTrue() {
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .completedTrueOnRegistration)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo)
        let rule = OfferCard.Rule.picpayCreditEnableForRegistration
        XCTAssertTrue(validation.isGranted(for: [rule]))
    }
    
    func testRuleCreditEnableForRegistration_WhenInvalidAccount_ShouldBeFalse() {
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .completedFalseDebitOnRegistration)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo)
        let rule = OfferCard.Rule.picpayCreditEnableForRegistration
        XCTAssertFalse(validation.isGranted(for: [rule]))
    }

    func testRuleDebitEnableForRegistration_WhenValidAccount_ShouldBeTrue() {
        featureManagerMock.override(key: .flagCardDebitOffer, with: true)
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .completedTrueDebitOnRegistration)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo, dependencies: mockDependencies)
        let rule = OfferCard.Rule.picpayDebitEnableForRegistration
        XCTAssertTrue(validation.isGranted(for: [rule]))
    }

    func testRuleDebitEnableForRegistration_WhenInvalidAccount_ShouldBeFalse() {
        featureManagerMock.override(key: .flagCardDebitOffer, with: true)
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .completedFalseOnRegistration)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo, dependencies: mockDependencies)
        let rule = OfferCard.Rule.picpayDebitEnableForRegistration
        XCTAssertFalse(validation.isGranted(for: [rule]))
    }
    
    func testRuleCardEnableForRequest_WhenValidAccount_ShouldBeTrue() {
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .completedTrueCardEnableForRequest)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo)
        let rule = OfferCard.Rule.cardEnableForRequestPhysycalCard
        XCTAssertTrue(validation.isGranted(for: [rule]))
    }
    
    func testRuleCardEnableForRequest_WhenInvalidAccount_ShouldBeFalse() {
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .completedFalseCardEnableForRequest)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo)
        let rule = OfferCard.Rule.cardEnableForRequestPhysycalCard
        XCTAssertFalse(validation.isGranted(for: [rule]))
    }
    
    func testRuleCardEnableForRequestMigration_WhenValidAccount_ShouldBeTrue() {
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .completedTrueCardEnableForRequestMigration)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo)
        let rule = OfferCard.Rule.cardEnableForRequestMigration
        XCTAssertTrue(validation.isGranted(for: [rule]))
    }
    
    func testRuleCardEnableForRequestMigration_WhenInvalidAccount_ShouldBeFalse() {
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .completedFalseCardEnableForRequestMigration)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo)
        let rule = OfferCard.Rule.cardEnableForRequestMigration
        XCTAssertFalse(validation.isGranted(for: [rule]))
    }
    
    func testRuleCardEnableForRegistration_WhenValidAccount_ShouldBeTrue() {
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .completedTrueCardEnableForRegistration)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo)
        let rule = OfferCard.Rule.cardEnableForRegistration
        XCTAssertTrue(validation.isGranted(for: [rule]))
    }
    
    func testRuleCardEnableForRegistration_WhenInvalidAccount_ShouldBeFalse() {
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .completedFalseCardEnableForRegistration)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo)
        let rule = OfferCard.Rule.cardEnableForRegistration
        XCTAssertFalse(validation.isGranted(for: [rule]))
    }

    func testRuleCardDebitEnableForRegistration_WhenValidAccount_ShouldBeTrue() {
        featureManagerMock.override(key: .flagCardDebitOffer, with: true)
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .completedTrueCardDebitEnableForRegistration)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo, dependencies: mockDependencies)
        let rule = OfferCard.Rule.cardDebitEnableForRegistration
        XCTAssertTrue(validation.isGranted(for: [rule]))
    }

    func testRuleCardDebitEnableForRegistration_WhenInvalidAccount_ShouldBeFalse() {
        featureManagerMock.override(key: .flagCardDebitOffer, with: true)
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .completedFalseCardDebitEnableForRegistration)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo, dependencies: mockDependencies)
        let rule = OfferCard.Rule.cardDebitEnableForRegistration
        XCTAssertFalse(validation.isGranted(for: [rule]))
    }
    
    func testRuleCardEnableForActivateDebit_WhenValidAccount_ShouldBeTrue() {
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .completedTrueCardEnableForActivateDebit)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo)
        let rule = OfferCard.Rule.cardEnableForActivateDebit
        XCTAssertTrue(validation.isGranted(for: [rule]))
    }
    
    func testRuleCardEnableForActivateDebit_WhenInvalidAccount_ShouldBeFalse() {
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .completedFalseCardEnableForActivateDebit)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo)
        let rule = OfferCard.Rule.cardEnableForActivateDebit
        XCTAssertFalse(validation.isGranted(for: [rule]))
    }
    
    func testRuleCardEnableForTracking_WhenValidAccount_ShouldBeTrue() {
        featureManagerMock.override(key: .opsCardTracking, with: true)
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .completedTrueCardEnabledForTracking)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo, dependencies: mockDependencies)
        let rule = OfferCard.Rule.cardEnableForTracking
        XCTAssertTrue(validation.isGranted(for: [rule]))
    }
    
    func testRuleCardEnableForTracking_WhenInvalidAccount_ShouldBeFalse() {
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .completedFalseCardEnabledForTracking)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo)
        let rule = OfferCard.Rule.cardEnableForTracking
        XCTAssertFalse(validation.isGranted(for: [rule]))
    }
    
    func testRuleCardEnableForDebitRequestInProgress_WhenValidAccount_ShouldBeTrue() {
        featureManagerMock.override(key: .flagCardDebitOffer, with: true)
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .completedTrueCardEnableForDebitRequestInProgress)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo, dependencies: mockDependencies)
        let rule = OfferCard.Rule.cardEnableForDebitRequestInProgress
        XCTAssertTrue(validation.isGranted(for: [rule]))
    }
    
    func testRuleCardEnableForDebitRequestInProgress_WhenInvalidAccount_ShouldBeFalse() {
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .completedFalseCardEnableForDebitRequestInProgress)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo)
        let rule = OfferCard.Rule.cardEnableForDebitRequestInProgress
        XCTAssertFalse(validation.isGranted(for: [rule]))
    }
    
    func testUpgradeAccountCardLimit() {
        let accountInfo = createDigitalAccountInfo(offerUpgradeAccount: .cardLimit)
        let validation = OfferCardsValidation(creditAccount: nil, accountInfo: accountInfo)
        let rule = OfferCard.Rule.upgradeAccountCardLimit
        XCTAssertTrue(validation.isGranted(for: [rule]))
    }
    
    func testUpgradeAccountCardLess() {
        let accountInfo = createDigitalAccountInfo(offerUpgradeAccount: .cardLess)
        let validation = OfferCardsValidation(creditAccount: nil, accountInfo: accountInfo)
        let rule = OfferCard.Rule.upgradeAccountCardLess
        XCTAssertTrue(validation.isGranted(for: [rule]))
    }
    
    func testUpgradeAccountCardMore() {
        let accountInfo = createDigitalAccountInfo(offerUpgradeAccount: .cardMore)
        let validation = OfferCardsValidation(creditAccount: nil, accountInfo: accountInfo)
        let rule = OfferCard.Rule.upgradeAccountCardMore
        XCTAssertTrue(validation.isGranted(for: [rule]))
    }
    
    func testUpgradeAccountJudiciallyBlockedShouldReturnTrue() {
        let accountInfo = createDigitalAccountInfo(isJudiciallyBlocked: true)
        let validation = OfferCardsValidation(creditAccount: nil, accountInfo: accountInfo)
        let rule = OfferCard.Rule.judiciallyBlocked
        XCTAssertTrue(validation.isGranted(for: [rule]))
    }
    
    func testRuleUpgradeDebitToCredit_WhenValidAccount_ShouldBeTrue() {
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .completedTrueCardEnabledForUpgradeDebitToCredit)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo, dependencies: mockDependencies)
        let rule = OfferCard.Rule.upgradeDebitToCredit
        XCTAssertTrue(validation.isGranted(for: [rule]))
    }
    
    func testRuleDebitRequestSimplified_WhenValidAccount_ShouldBeTrue() {
        featureManagerMock.override(key: .simplifiedDebitRegister, with: true)
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .accountCompletedTrueCardEnableForDebitRequestSimplified)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo, dependencies: mockDependencies)
        let rule = OfferCard.Rule.simplifiedDebitRegistration
        XCTAssertTrue(validation.isGranted(for: [rule]))
    }
    
    func testRuleDebitRequestSimplified_WhenValidAccount_ShouldBeFalse() {
        featureManagerMock.override(key: .simplifiedDebitRegister, with: false)
        let accountInfo = createDigitalAccountInfo()
        setUpAccount(with: .accountCompletedTrueCardEnableForDebitRequestSimplified)
        let validation = OfferCardsValidation(creditAccount: creditAccount, accountInfo: accountInfo, dependencies: mockDependencies)
        let rule = OfferCard.Rule.simplifiedDebitRegistration
        XCTAssertFalse(validation.isGranted(for: [rule]))
    }
}
