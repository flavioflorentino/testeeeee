import XCTest
@testable import PicPay

class AntiFraudViewModelTest: XCTestCase {
    func viewModelMock(for state: AntiFraudApiMock.AntiFraudApiState) -> AntiFraudChecklistViewModel {
        let api = AntiFraudApiMock(state: state)
        return AntiFraudChecklistViewModel(with: api)
    }
    
    func fetchChecklist(viewModel: AntiFraudChecklistViewModel) {
        let expectation = XCTestExpectation(description: "ChecklistLoaded")
        viewModel.loadChecklist(onSuccess: {
            expectation.fulfill()
        }, onError: { _ in
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testNumOfRowsChecklistLoaded() {
        let viewModel = viewModelMock(for: .success)
        fetchChecklist(viewModel: viewModel)
        XCTAssertEqual(viewModel.numberOfRowsChecklist, 7)
    }
    
    func testNumOfRowsChecklistNotLoaded() {
        let viewModel = viewModelMock(for: .success)
        XCTAssertEqual(viewModel.numberOfRowsChecklist, 0)
    }
    
    func testNumOfRowsChecklistFailure() {
        let viewModel = viewModelMock(for: .failure)
        fetchChecklist(viewModel: viewModel)
        XCTAssertEqual(viewModel.numberOfRowsChecklist, 0)
    }
    
    func testChecklistValidItem() {
        let viewModel = viewModelMock(for: .success)
        fetchChecklist(viewModel: viewModel)
        let item = viewModel.checklistItem(at: 3)
        XCTAssertEqual(item?.wsId, "111831")
    }
    
    func testChecklistInvalidItem() {
        let viewModel = viewModelMock(for: .success)
        let item = viewModel.checklistItem(at: 7)
        XCTAssertNil(item)
    }
    
    func testGetDocumentCaptureOption() {
        let viewModel = viewModelMock(for: .success)
        fetchChecklist(viewModel: viewModel)
        let checklistItem = viewModel.checklistItem(at: 2)
        var docCaptureOption: AntiFraudChecklistViewModel.DocumentCaptureOption?
        if let item = checklistItem {
            docCaptureOption = viewModel.getDocumentCaptureOption(item)
        }
        XCTAssertEqual(docCaptureOption, .cameraOnly)
    }
    
    func testGetDocumentCaptureOptionDoubleSided() {
        let viewModel = viewModelMock(for: .success)
        fetchChecklist(viewModel: viewModel)
        let checklistItem = viewModel.checklistItem(at: 1)
        var docCaptureOption: AntiFraudChecklistViewModel.DocumentCaptureOption?
        if let item = checklistItem {
            docCaptureOption = viewModel.getDocumentCaptureOption(item)
        }
        XCTAssertEqual(docCaptureOption, .cameraDoubleSided)
    }
    
    func testGetDocumentCaptureOptionCameraUserChoice() {
        let viewModel = viewModelMock(for: .success)
        fetchChecklist(viewModel: viewModel)
        let checklistItem = viewModel.checklistItem(at: 3)
        var docCaptureOption: AntiFraudChecklistViewModel.DocumentCaptureOption?
        if let item = checklistItem {
            docCaptureOption = viewModel.getDocumentCaptureOption(item)
        }
        XCTAssertEqual(docCaptureOption, .userChoice)
    }
    
    func testSetItemUploaded() {
        let viewModel = viewModelMock(for: .success)
        fetchChecklist(viewModel: viewModel)
        viewModel.setItemUploaded(at: 1)
        let item = viewModel.checklistItem(at: 1)
        XCTAssertEqual(item?.isUploaded, true)
    }
    
    func testIsNotReadyToConclude() {
        let viewModel = viewModelMock(for: .success)
        fetchChecklist(viewModel: viewModel)
        XCTAssertEqual(viewModel.isReadyToConclude, false)
    }
    
    func testIsReadyToConclude() {
        let viewModel = viewModelMock(for: .successAndReadyToConclude)
        fetchChecklist(viewModel: viewModel)
        XCTAssertEqual(viewModel.isReadyToConclude, true)
    }
    
    func testConcludeSuccess() {
        let viewModel = viewModelMock(for: .successAndReadyToConclude)
        let expectation = XCTestExpectation(description: "ConcludeChecklist")
        viewModel.concludeAntiFraudChecklist(onSuccess: {
            expectation.fulfill()
        }, onError: { _ in
        })
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testConcludeFailure() {
        let viewModel = viewModelMock(for: .failure)
        let expectation = XCTestExpectation(description: "ConcludeChecklist")
        viewModel.concludeAntiFraudChecklist(onSuccess: {
        }, onError:  { _ in
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testConcludeAttachmentUploadSuccess() {
        let viewModel = viewModelMock(for: .successAndReadyToConclude)
        let expectation = XCTestExpectation(description: "ConcludeChecklist")
        viewModel.concludeAntiFraudChecklist(onSuccess: {
            expectation.fulfill()
        }, onError: { _ in
        })
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testConcludeAttachmentUploadFailure() {
        let viewModel = viewModelMock(for: .failure)
        let expectation = XCTestExpectation(description: "ConcludeChecklist")
        viewModel.concludeAntiFraudChecklist(onSuccess: {
        }, onError:  { _ in
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testGetRelativeProgressFirstHalf() {
        let viewModel = viewModelMock(for: .success)
        let currentProgress = 0.20
        let firstHalf = true
        let expectedRelativeProgress = 0.10
        
        let relativeProgress = viewModel.relativeProgress(currentProgress, firstHalf: firstHalf)
        XCTAssertEqual(relativeProgress, expectedRelativeProgress)
    }
    
    func testGetRelativeProgressSecondHalf() {
        let viewModel = viewModelMock(for: .success)
        let currentProgress = 0.40
        let firstHalf = false
        let expectedRelativeProgress = 0.70
        
        let relativeProgress = viewModel.relativeProgress(currentProgress, firstHalf: firstHalf)
        XCTAssertEqual(relativeProgress, expectedRelativeProgress)
    }
}
