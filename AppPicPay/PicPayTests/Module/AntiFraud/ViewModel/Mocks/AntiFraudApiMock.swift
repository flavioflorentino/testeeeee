@testable import PicPay

class AntiFraudApiMock: AntiFraudApiProtocol {
    enum AntiFraudApiState {
        case success
        case failure
        case successAndReadyToConclude
        
        var resource: String {
            switch self {
            case .success:
                return "AntiFraudGenericSuccessMock"
            case .failure:
                return "AntiFraudGenericErrorMock"
            case .successAndReadyToConclude:
                return "AntiFraudReadyToConcludeMock"
            }
        }
    }
    
    var state: AntiFraudApiState
    
    init(state: AntiFraudApiState) {
        self.state = state
    }
    
    func getAntiFraudChecklist(_ completion: @escaping (PicPayResult<AntiFraudChecklist>) -> Void) {
        var result: PicPayResult<AntiFraudChecklist>
        if state == .failure {
            let error = PicPayError(json: MockJSON().load(resource: "AntiFraudGenericErrorMock"))
            result = PicPayResult<AntiFraudChecklist>.failure(error)
        } else {
            let resource = (state == .successAndReadyToConclude) ? state.resource : "AntiFraudChecklistLoadedMock"
            let checklistLoaded = try! MockCodable<AntiFraudChecklist>().loadCodableObject(resource: resource)
            result = PicPayResult<AntiFraudChecklist>.success(checklistLoaded)
        }
        completion(result)
    }
    
    func uploadAttachment(imageData: Data, attachmentId: String, lastAttachment: Bool, uploadProgress: @escaping ((Double) -> Void), _ completion: @escaping ((PicPayResult<AntiFraudGenericResponse>) -> Void)) {
        var result: PicPayResult<AntiFraudGenericResponse>
        if state == .failure {
            let error = PicPayError(json: MockJSON().load(resource: "AntiFraudGenericErrorMock"))
            result = PicPayResult<AntiFraudGenericResponse>.failure(error)
        } else {
            let data = try! MockCodable<AntiFraudGenericResponse>().loadCodableObject(resource: "AntiFraudGenericSuccessMock")
            result = PicPayResult<AntiFraudGenericResponse>.success(data)
        }
        completion(result)
    }
    
    func concludeAntiFraudChecklist(_ completion: @escaping ((PicPayResult<AntiFraudGenericResponse>) -> Void)) {
        var result: PicPayResult<AntiFraudGenericResponse>
        if state == .failure {
            let error = PicPayError(json: MockJSON().load(resource: "AntiFraudGenericErrorMock"))
            result = PicPayResult<AntiFraudGenericResponse>.failure(error)
        } else {
            let data = try! MockCodable<AntiFraudGenericResponse>().loadCodableObject(resource: "AntiFraudGenericSuccessMock")
            result = PicPayResult<AntiFraudGenericResponse>.success(data)
        }
        completion(result)
    }
    
    
}
