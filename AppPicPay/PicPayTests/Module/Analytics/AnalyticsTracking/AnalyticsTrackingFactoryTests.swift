import XCTest
@testable import PicPay

class AnalyticsTrackingFactoryTests: XCTestCase {
    func testMakeShouldReturnViewController() {
        let viewController = AnalyticsTrackingFactory.make()
        XCTAssertNotNil(viewController.coordinator.viewController)
        XCTAssertNotNil(viewController.viewModel.outputs)
    }
}
