import XCTest
@testable import PicPay

class AnalyticsTrackingCoordinatorTests: XCTestCase {
    var sut: AnalyticsTrackingCoordinator!
    var fakeViewController: FakeViewController!
    
    override func setUp() {
        super.setUp()
        sut = AnalyticsTrackingCoordinator()
        fakeViewController = FakeViewController()
        sut.viewController = fakeViewController
    }
    
    func testPerformActionCloseShouldCallDismiss() {
        sut.perform(action: .close)
        XCTAssertTrue(fakeViewController.isDismissCalled)
    }
}
