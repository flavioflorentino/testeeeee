import AnalyticsModule
@testable import PicPay

class AnalyticsLoggerMock: AnalyticsLoggerProtocol {
    var isCleanCalled: Bool = false
    var isGetLogsCalled: Bool = false
    var isInfoCalled: Bool = false
    
    var logsMock: [AnalyticsLoggerTrack] = [
        AnalyticsLoggerTrack(provider: "Firebase", event: ["screen": ["HomeViewController": true]]),
        AnalyticsLoggerTrack(provider: "Mixpanel", event: ["action": [:]]),
        AnalyticsLoggerTrack(provider: "AppsFlyer", event: [:])
    ]
    
    func clean() {
        isCleanCalled = true
        logsMock.removeAll()
    }
    
    func getLogs() -> [AnalyticsLoggerTrack] {
        isGetLogsCalled = true
        return logsMock
    }
    
    func info(with eventName: String, properties: [String : Any], from provider: String) {
        isInfoCalled = true
        let eventLog = AnalyticsLoggerTrack(provider: provider, event: [eventName: properties])
        logsMock.append(eventLog)
    }
}
