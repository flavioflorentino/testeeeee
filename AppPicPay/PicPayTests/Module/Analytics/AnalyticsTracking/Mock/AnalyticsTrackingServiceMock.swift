import AnalyticsModule
@testable import PicPay

class AnalyticsTrackingServiceMock: AnalyticsTrackingServiceProtocol {
    var isDeleteLogsCalled: Bool = false
    var isLogsCalled: Bool = false
    
    func deleteLogs(onSuccess: @escaping () -> Void) {
        isDeleteLogsCalled = true
        onSuccess()
    }
    
    func logs(completion: @escaping ([AnalyticsLoggerTrack]) -> Void) {
        isLogsCalled = true
        let logs = [AnalyticsLoggerTrack(provider: "Firebase", event: ["screen": [:]])] as [AnalyticsLoggerTrack]
        completion(logs)
    }
}
