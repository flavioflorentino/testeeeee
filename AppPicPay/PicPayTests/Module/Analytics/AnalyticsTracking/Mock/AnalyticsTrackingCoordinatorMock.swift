@testable import PicPay

class AnalyticsTrackingCoordinatorMock: AnalyticsTrackingCoordinatorProtocol {
    var action: AnalyticsTrackingAction?
    var viewController: UIViewController?
    
    func perform(action: AnalyticsTrackingAction) {
        self.action = action
    }
}
