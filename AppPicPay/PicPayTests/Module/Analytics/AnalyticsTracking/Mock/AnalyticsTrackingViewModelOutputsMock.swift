@testable import PicPay
import UI

class AnalyticsTrackingViewModelOutputsMock: AnalyticsTrackingViewModelOutputs {
    
    var isDidNextActionCalled: Bool = false
    var isAnalyticsLogsCalled: Bool = false
    
    var action: AnalyticsTrackingAction?
    var logs: [Section<String, AnalyticsLogViewPresenterProtocol>]?
    
    func didNextAction(_ action: AnalyticsTrackingAction) {
        isDidNextActionCalled = true
        self.action = action
    }
    
    func analyticsLogs(_ logs: [Section<String, AnalyticsLogViewPresenterProtocol>]) {
        isAnalyticsLogsCalled = true
        self.logs = logs
    }
}
