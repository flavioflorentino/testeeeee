import XCTest
@testable import PicPay

class AnalyticsTrackingViewControllerTests: XCTestCase {
    var sut: AnalyticsTrackingViewController!
    var mockCoordinator: AnalyticsTrackingCoordinatorMock!
    var mockService: AnalyticsTrackingServiceMock!
    
    override func setUp() {
        super.setUp()
        
        mockService = AnalyticsTrackingServiceMock()
        let viewModel = AnalyticsTrackingViewModel(service: mockService)
        mockCoordinator = AnalyticsTrackingCoordinatorMock()

        sut = AnalyticsTrackingViewController(viewModel: viewModel, coordinator: mockCoordinator)
        
        viewModel.outputs = sut
        mockCoordinator.viewController = sut
                
        sut.beginAppearanceTransition(true, animated: false)
        sut.endAppearanceTransition()
    }
    
    func testViewDidLoadShouldCallLogsService() {
        XCTAssertTrue(mockService.isLogsCalled)
    }
    
    func testTapCloseShouldReturnAction() {
        sut.tapClose()
        XCTAssertTrue(mockCoordinator.action == .close)
    }
    
    func testTapTrashShouldReloadTableViewZero() {
        sut.tapTrash()
        XCTAssertTrue(mockService.isDeleteLogsCalled)
        XCTAssertTrue(mockService.isLogsCalled)
    }
}
