import XCTest
@testable import PicPay

class AnalyticsTrackingViewModelTests: XCTestCase {
    var sut: AnalyticsTrackingViewModel!
    var mockService: AnalyticsTrackingServiceMock!
    var mockOutputs: AnalyticsTrackingViewModelOutputsMock!
    
    override func setUp() {
        super.setUp()
        
        mockService = AnalyticsTrackingServiceMock()
        sut = AnalyticsTrackingViewModel(service: mockService)
        mockOutputs = AnalyticsTrackingViewModelOutputsMock()
        sut.outputs = mockOutputs
    }
    
    func testViewDidLoadShouldReturnPresenters() {
        sut.viewDidLoad()
        XCTAssertTrue(mockService.isLogsCalled)
        XCTAssertTrue(mockOutputs.isAnalyticsLogsCalled)
        XCTAssert(mockOutputs.logs?.count > 0)
    }
    
    func testCloseShouldReturnAction() {
        sut.close()
        XCTAssertTrue(mockOutputs.isDidNextActionCalled)
        XCTAssert(mockOutputs.action == .close)
    }
    
    func testCleanShouldCallDeleteLogsAndLogs() {
        sut.clean()
        XCTAssertTrue(mockService.isDeleteLogsCalled)
        XCTAssertTrue(mockService.isLogsCalled)
        XCTAssertTrue(mockOutputs.isAnalyticsLogsCalled)
    }
}
