import XCTest
import AnalyticsModule
@testable import PicPay

class AnalyticsTrackingServiceTests: XCTestCase {
    var sut: AnalyticsTrackingService!
    var loggerMock: AnalyticsLoggerMock!
    
    override func setUp() {
        super.setUp()
        loggerMock = AnalyticsLoggerMock()
        sut = AnalyticsTrackingService(logger: loggerMock)
    }
    
    func testDeleteLogsShouldRemoveAllLogs() {
        var onSuccess: Bool = false
        let expectation = self.expectation(description: "Delete Logs")
        
        sut.deleteLogs {
            onSuccess = true
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5)
        
        XCTAssertTrue(loggerMock.isCleanCalled)
        XCTAssertTrue(loggerMock.logsMock.count == 0)
        XCTAssertTrue(onSuccess)
    }
    
    func testLogsShouldReturnAllLogs() {
        var logs: [AnalyticsLoggerTrack] = []
        let expectation = self.expectation(description: "Logs")
        
        sut.logs { tracks in
            logs = tracks
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5)
        
        XCTAssertTrue(loggerMock.isGetLogsCalled)
        XCTAssertTrue(logs.count == loggerMock.logsMock.count)
        XCTAssertTrue(logs.first?.provider == loggerMock.logsMock.last?.provider)
    }
    
}
