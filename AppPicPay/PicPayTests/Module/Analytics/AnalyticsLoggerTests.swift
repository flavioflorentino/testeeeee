import XCTest
import AnalyticsModule
@testable import PicPay

class AnalyticsLoggerTests: XCTestCase {
    var sut: AnalyticsLogger!
    
    override func setUp() {
        super.setUp()
        sut = AnalyticsLogger()
        sut.info(with: "screen", properties: [:], from: "Firebase")
    }
    
    func testCleanShouldRemovelAllLogs() {
        sut.clean()
        XCTAssert(sut.getLogs().count == 0)
    }
    
    func testGetLogsShouldReturnRegisterLogs() {
        XCTAssert(sut.getLogs().count == 1)
    }
    
    func testInfoEventProtocolShouldAddNewLog() {
        sut.info(with: "action", properties: [:], from: .firebase)
        XCTAssert(sut.getLogs().count == 2)
    }
    
    func testInfoEventPropertiesShouldAddNewLog() {
        sut.info(with: "screen", properties: [:], from: "AppsFlyer")
        XCTAssert(sut.getLogs().count == 2)
    }
}
