import XCTest
@testable import PicPay
import SwiftyJSON

final private class IdentityValidationStatusServiceMock: IdentityValidationStatusServicing {
    var result: PicPayResult<IVIdentityValidationStatus>?
    
    func status(_ completion: @escaping ((PicPayResult<IVIdentityValidationStatus>) -> Void)) {
        guard let result = result else {
            XCTFail("result not initialized in IdentityValidationStatusServiceMock")
            return
        }
        completion(result)
    }
}

final private class IdentityValidationInitialServiceMock: IdentityValidationInitialServicing {
    var result: PicPayResult<IdentityValidationStep>?
    
    func startValidation(originFlow: IdentityValidationOriginFlow, _ completion: @escaping ((PicPayResult<IdentityValidationStep>) -> Void)) {
        guard let result = result else {
            XCTFail("result not initialized in IdentityValidationInitialServiceMock")
            return
        }
        completion(result)
    }
}

final class IdentityChallengeViewModelTest: XCTestCase {
    private let statusServiceMock = IdentityValidationStatusServiceMock()
    private let startValidationServiceMock = IdentityValidationInitialServiceMock()
    private lazy var viewModel = IdentityChallengeViewModel(
        statusService: statusServiceMock,
        startValidationService: startValidationServiceMock
    )
    
    func runGetIdentityValidationStatus(returning status: IVIdentityValidationStatus.Status) {
        let successJson = JSON(["status_label": "_", "status": "\(status.rawValue)"])
        statusServiceMock.result = .success(IVIdentityValidationStatus(json: successJson))

        let expectation = XCTestExpectation(description: "expected to receive status")
        viewModel.getIdentityValidationStatus(onSuccess: {
            expectation.fulfill()
        }, onError: { _ in
            XCTFail()
        })
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testGetIdentityValidationStatus_WhenReturnsNotCreatedStatus_ShouldSetupViewModelProperties() {
        runGetIdentityValidationStatus(returning: .notCreated)
        
        XCTAssertEqual(viewModel.screenState, .startValidation)
        XCTAssertNotNil(viewModel.screenConstants)
        XCTAssertEqual(viewModel.imageAsset, Assets.Ilustration.iluIdentityValidation.image)
    }
    
    func testGetIdentityValidationStatus_WhenReturnsNotVerifiedStatus_ShouldSetupViewModelProperties() {
        runGetIdentityValidationStatus(returning: .notVerified)
        
        XCTAssertEqual(viewModel.screenState, .startValidation)
        XCTAssertNotNil(viewModel.screenConstants)
        XCTAssertEqual(viewModel.imageAsset, Assets.Ilustration.iluIdentityValidation.image)
    }
    
    func testGetIdentityValidationStatus_WhenReturnsUndefinedStatus_ShouldSetupViewModelProperties() {
        runGetIdentityValidationStatus(returning: .undefined)
        
        XCTAssertEqual(viewModel.screenState, .startValidation)
        XCTAssertNotNil(viewModel.screenConstants)
        XCTAssertEqual(viewModel.imageAsset, Assets.Ilustration.iluIdentityValidation.image)
    }
    
    func testGetIdentityValidationStatus_WhenReturnsPendingStatus_ShouldSetupViewModelProperties() {
        runGetIdentityValidationStatus(returning: .pending)
        
        XCTAssertEqual(viewModel.screenState, .continueValidation)
        XCTAssertNotNil(viewModel.screenConstants)
        XCTAssertEqual(viewModel.imageAsset, Assets.Ilustration.iluIdentityValidation.image)
    }
    
    func testGetIdentityValidationStatus_WhenReturnsToVerifyStatus_ShouldSetupViewModelProperties() {
        runGetIdentityValidationStatus(returning: .toVerify)
        
        XCTAssertEqual(viewModel.screenState, .wait)
        XCTAssertNotNil(viewModel.screenConstants)
        XCTAssertEqual(viewModel.imageAsset, Assets.Icons.icoClockGreen.image)
    }
    
    func testGetIdentityValidationStatus_WhenReturnsVerifiedStatus_ShouldSetupViewModelProperties() {
        runGetIdentityValidationStatus(returning: .verified)
        
        XCTAssertEqual(viewModel.screenState, .success)
        XCTAssertNotNil(viewModel.screenConstants)
        XCTAssertEqual(viewModel.imageAsset, Assets.Icons.iconBigSuccess.image)
    }
    
    func testGetIdentityValidationStatus_WhenReturnsRejectedStatus_ShouldSetupViewModelProperties() {
        runGetIdentityValidationStatus(returning: .rejected)
        
        XCTAssertNil(viewModel.screenState)
        XCTAssertNil(viewModel.screenConstants)
        XCTAssertNil(viewModel.imageAsset)
    }
    
    func testGetIdentityValidationStatus_WhenReturnsDisabledStatus_ShouldSetupViewModelProperties() {
        runGetIdentityValidationStatus(returning: .disabled)
        
        XCTAssertNil(viewModel.screenState)
        XCTAssertNil(viewModel.screenConstants)
        XCTAssertNil(viewModel.imageAsset)
    }
    
    func testGetIdentityValidationStatus_WhenReturnsError_ShouldExecuteOnErrorCallback() {
        statusServiceMock.result = .failure(MockError.genericError)
        let expectation = XCTestExpectation(description: "expected to fail")
        viewModel.getIdentityValidationStatus(onSuccess: {
            XCTFail()
        }, onError: { _ in
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testskipFirstStepIdentityValidation_WhenResponseIsSuccess_ShouldExecuteOnSuccessCallback() {
        startValidationServiceMock.result = .success(
            IdentityValidationStep(type: .questions, title: "", text: "",data: nil, status: .none, icons: .none)
        )
        let expectation = XCTestExpectation(description: "expected to return success")
        
        viewModel.skipFirstStepIdentityValidation(onSuccess: {
            expectation.fulfill()
        }, onError: { _ in
            XCTFail()
        })
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testskipFirstStepIdentityValidation_WhenResponseIsFail_ShouldExecuteOnErrorCallback() {
        startValidationServiceMock.result = .failure(MockError.genericError)
        let expectation = XCTestExpectation(description: "expected to fail")
        
        viewModel.skipFirstStepIdentityValidation(onSuccess: {
            XCTFail()
        }, onError: { _ in
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 1.0)
    }
}
