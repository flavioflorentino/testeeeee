import XCTest
import OHHTTPStubs
@testable import PicPay

class CardChallengeViewModelTest: XCTestCase {
    var viewModel: CardChallengeViewModel!
    let cardId = "419504"
    
    override func setUp() {
        super.setUp()
        ConsumerManager.shared.consumer?.balance = 0.00
        viewModel = CardChallengeViewModel(cardId: cardId)
    }
    
    override func tearDown() {
        super.tearDown()
        viewModel = nil
        OHHTTPStubs.removeAllStubs()
    }
    
    func runGetCardValidationStatus(returning status: CardBank.Status) {
        stub(condition: isHost💚.PicPay) { _ in
            return StubResponse.success(with:
                ["data":
                    ["info_text":"",
                     "success":true,
                     "status_text":"",
                     "verify_status":"\(status.rawValue)"
                    ]
                ])
        }
    }
    
    func testGetCardStatusVerifiable() {
        runGetCardValidationStatus(returning: .verifiable)
        
        let expectation = XCTestExpectation(description: "expected to receive status")
        viewModel.getCardValidationStatus(onSuccess: {
            expectation.fulfill()
        }) { (_) in
            XCTFail()
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testGetCardStatusWaitingFillValue() {
        runGetCardValidationStatus(returning: .waitingFillValue)
        
        let expectation = XCTestExpectation(description: "expected to receive status")
        viewModel.getCardValidationStatus(onSuccess: {
            XCTAssertEqual(self.viewModel.screenState, .startValidation)
            XCTAssertNotNil(self.viewModel.screenConstants)
            XCTAssertFalse(self.viewModel.isPrimayButtonDestructive)
            expectation.fulfill()
        }) { (_) in
            XCTFail()
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testGetCardStatusVerified() {
        runGetCardValidationStatus(returning: .verified)
        
        let expectation = XCTestExpectation(description: "expected to receive status")
        viewModel.getCardValidationStatus(onSuccess: {
            XCTAssertEqual(self.viewModel.screenState, .success)
            XCTAssertNotNil(self.viewModel.screenConstants)
            XCTAssertFalse(self.viewModel.isPrimayButtonDestructive)
            expectation.fulfill()
        }) { (_) in
            XCTFail()
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testGetCardStatusNotAvailable() {
        runGetCardValidationStatus(returning: .notAvailable)
        
        let expectation = XCTestExpectation(description: "expected to receive status")
        viewModel.getCardValidationStatus(onSuccess: {
            XCTAssertEqual(self.viewModel.screenState, .failure)
            XCTAssertNotNil(self.viewModel.screenConstants)
            XCTAssertTrue(self.viewModel.isPrimayButtonDestructive)
            expectation.fulfill()
        }) { (_) in
            XCTFail()
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testGetCardStatusUnknown() {
        runGetCardValidationStatus(returning: .unknown)
        XCTAssertNil(viewModel.screenState)
    }
    
    func testGetCardValidationStatusFail() {
        stub(condition: isHost💚.PicPay) { _ in
            return StubResponse.fail()
        }
        let expectation = XCTestExpectation(description: "expected to fail")
        viewModel.getCardValidationStatus(onSuccess: {
            XCTFail()
        }) { (error) in
            XCTAssertNotNil(error)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5.0)
        XCTAssertNil(viewModel.screenState)
        XCTAssertNil(viewModel.screenConstants)
    }
    
    func testGetCardModelSuccess() {
        User.updateToken("abcsef")
        CreditCardManager.shared.cardList = []
        stub(condition: isHost💚.PicPay) { _ in
            return StubResponse.success(at: "cardList.json")
        }
        let expectation = XCTestExpectation(description: "expected to receive card with same id")
        viewModel.getCardModel(onSuccess: { card in
            XCTAssertEqual(card.id, self.cardId)
            expectation.fulfill()
        }) { (_) in
            XCTFail()
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testGetCardModelFail() {
        User.updateToken("abcsef")
        CreditCardManager.shared.cardList = []
        stub(condition: isHost💚.PicPay) { _ in
            return StubResponse.fail()
        }
        let expectation = XCTestExpectation(description: "expected to fail")
        viewModel.getCardModel(onSuccess: { _ in
            XCTFail()
        }) { (error) in
            XCTAssertNotNil(error)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5.0)
    }
}
