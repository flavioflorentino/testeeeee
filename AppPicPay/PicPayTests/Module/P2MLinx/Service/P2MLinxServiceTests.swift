import XCTest
import FeatureFlag

@testable import PicPay
@testable import PCI

private enum RawSeeds {
    static let someCardBankData = """
                                {
                                    "id": "12345",
                                    "type": "credit_card",
                                    "description": "some description for a card",
                                    "image": "https://somecardimage.com/image",
                                    "expired": false,
                                    "alias": "some alias",
                                    "address": {
                                        "name": "some name address"
                                    },
                                    "lastDigits": "456",
                                    "verifyStatus": "verified",
                                    "creditCardBandeiraId": "some bandeira ID",
                                    "cardBrand": "some brand",
                                    "cardIssuer": "someIssuer",
                                    "isOneShotCvv": false
                                }
                                """.data(using: .utf8)
    
    static let someLinxPaymentInfoData = """
                                        {
                                            "additional_info": {
                                                "store": {
                                                    "street": null,
                                                    "trading_name": "PIC PAY",
                                                    "longitude": 0.0,
                                                    "complement": null,
                                                    "postal_code": null,
                                                    "mdr": 2.3500000000000001,
                                                    "latitude": 0.0,
                                                    "city": null,
                                                    "district": null,
                                                    "company_name": "PIC PAY S.A",
                                                    "image_url": "https://www.dummyimage.com/image.png",
                                                    "number": null,
                                                    "state": null,
                                                    "cnpj": "05761098000113"
                                                },
                                                "partner": "linx"
                                            },
                                            "seller_image": "https://www.dummyimage.com/image.png",
                                            "plan_type": "A",
                                            "id_transacao_lojista": "48cf0067-a3c8-449f-833b-f903962a8205",
                                            "total_value": "15.0",
                                            "allows_change_amount_payable": false,
                                            "seller_id": "2",
                                            "seller_name": "PIC PAY",
                                            "allows_installment": true,
                                            "itens": {
                                                "amount": "1",
                                                "id": "1"
                                            }
                                        }
                                        """.data(using: .utf8)
}

private enum Seeds {
    static let someCreditCardBank: CardBank? = {
        guard let cardBankData = RawSeeds.someCardBankData,
            let card = try? JSONDecoder().decode(CardBank.self, from: cardBankData) else{
                return nil
        }
        return card
    }()
    
    static let someLinxPaymentInfo: LinxPaymentInfo? = {
        guard let linxPaymentInfoData = RawSeeds.someLinxPaymentInfoData,
            let linxPaymentInfo = try? JSONDecoder().decode(LinxPaymentInfo.self, from: linxPaymentInfoData) else {
                return nil
        }
        return linxPaymentInfo
    }()
}

private class PaymentManagerUsePicPayBalanceFalse: PPPaymentManager {
    override func usePicPayBalance() -> Bool {
        false
    }
}

private class PaymentManagerUsePicPayBalanceTrue: PPPaymentManager {
    override func usePicPayBalance() -> Bool {
        true
    }
}

private class PaymentManagerGreatherThanZero: PPPaymentManager {
    override func cardTotalWithoutInterest() -> NSDecimalNumber {
        NSDecimalNumber(floatLiteral: 5.00)
    }
}

private class PaymentManagerEqualsZero: PPPaymentManager {
    override func cardTotalWithoutInterest() -> NSDecimalNumber {
        NSDecimalNumber(floatLiteral: 0.0)
    }
}

private final class CreditCardManagerDummy: CreditCardManagerContract{
    var cardList: [CardBank] = []
    var defaultCreditCardId: Int = 1234
    var defaultCreditCard: CardBank? = Seeds.someCreditCardBank
    var isOcurredErrorOnFirstLoad: Bool = false
    
    func defaultDebitCard() -> CardBank? {
        nil
    }
    
    func deleteCard(id: String) {
    }
    
    func saveDefaultDebitCard(_ id: String){
    }
    
    func addCard(_ card: CardBank) {
    }
    
    func loadCardList(_ completion: @escaping (Error?) -> Void) {
    }
    
    func saveDefaultCreditCardId(_ id: Int) {
    }
    
    func updateDefaultCreditCardIfNil() {
    }
    
    func cardWithId(_ id: String) -> CardBank? {
        nil
    }
}

final class P2MLinxServiceTests: XCTestCase {
    lazy var featureManagerMock = FeatureManagerMock()
    lazy var sut = P2MLinxService(dependencies: DependencyContainerMock(CreditCardManagerDummy(), featureManagerMock))
    
    func testCreatePaymentRequest_WhenCalledWithUsePicPayBalanceFalseAndFeatureFlagDisable_ShouldSetCardId() throws {
        featureManagerMock.override(keys: .opsLinxKillswitchBool, with: false)
        let paymentInfoMock = try XCTUnwrap(Seeds.someLinxPaymentInfo)
        
        let request = sut.createPaymentRequest(
            pin: "somePin", biometry: false,
            paymentManager: PaymentManagerUsePicPayBalanceFalse(),
            linxPaymentInfo: paymentInfoMock,
            privacyConfig: "1")
        
        XCTAssertEqual("1234", request.creditCardId)
    }
    
    func testCreatePaymentRequest_WhenCalledWithUsePicPayBalanceTrueAndFeatureFlagDisable_ShouldNotSetCardId() throws {
        featureManagerMock.override(keys: .opsLinxKillswitchBool, with: false)
        let paymentInfoMock = try XCTUnwrap(Seeds.someLinxPaymentInfo)
        
        let request = sut.createPaymentRequest(
            pin: "somePin", biometry: false,
            paymentManager: PaymentManagerUsePicPayBalanceTrue(),
            linxPaymentInfo: paymentInfoMock,
            privacyConfig: "1")
        
        XCTAssertEqual("", request.creditCardId)
    }
    
    func testCreatePaymentRequest_WhenCalledWithTotalCardWithoutInterestEqualsZeroAndFeatureFlagEnabled_ShouldNotSetCardId() throws {
        featureManagerMock.override(keys: .opsLinxKillswitchBool, with: true)
        let paymentInfoMock = try XCTUnwrap(Seeds.someLinxPaymentInfo)
        
        let request = sut.createPaymentRequest(
            pin: "somePin",
            biometry: false,
            paymentManager: PaymentManagerEqualsZero(),
            linxPaymentInfo: paymentInfoMock,
            privacyConfig: "1")
               
        XCTAssertEqual("", request.creditCardId)
        
    }
    
    func testCreatePaymentRequest_WhenCalledWithTotalCardWithoutInterestGreatherThanZeroAndFeatureFlagEnabled_ShouldSetCardId() throws {
        featureManagerMock.override(keys: .opsLinxKillswitchBool, with: true)
        let paymentInfoMock = try XCTUnwrap(Seeds.someLinxPaymentInfo)
        
        let request = sut.createPaymentRequest(
            pin: "somePin",
            biometry: false,
            paymentManager: PaymentManagerGreatherThanZero(),
            linxPaymentInfo: paymentInfoMock,
            privacyConfig: "1")
               
        XCTAssertEqual("1234", request.creditCardId)
    }
}
