import XCTest
import SwiftyJSON
@testable import PicPay

class WSReceiptTests: XCTestCase {
    func testParseBillReceiptWithSuccess() {
        let json = Mock.json(.bill)
        let receipt = WSReceipt.createReceiptWidgetItem(json)
        
        XCTAssertTrue(receipt.count == 5)
        XCTAssertTrue(receipt[0] is TransactionReceiptWidgetItem)
        XCTAssertTrue(receipt[1] is ListReceiptWidgetItem)
        XCTAssertTrue(receipt[2] is LabelValueReceiptWidgetItem)
        XCTAssertTrue(receipt[3] is CashbackReceipWidgetItem)
        XCTAssertTrue(receipt[4] is ImageLabelValueButtonReceiptWidgetItem)
        
        let transaction = receipt[0] as! TransactionReceiptWidgetItem
        XCTAssertEqual(transaction.id, "6211523")
        XCTAssertEqual(transaction.date, "17/10/2018 13:12")
        XCTAssertFalse(transaction.isCanceled)
        XCTAssertEqual(transaction.total, "R$ 50,00")
        XCTAssertEqual(transaction.payee.id, "36748")
        XCTAssertEqual(transaction.payee.name, "Pagamento de contas")
        XCTAssertEqual(transaction.payee.username, "")
        XCTAssertEqual(transaction.payee.imgUrl, "https://picpay.s3.amazonaws.com/sellers/d2f7d0c310e8d6522074bfff51621718.jpg")
        XCTAssertEqual(transaction.list[0].label, "Cartão")
        XCTAssertEqual(transaction.list[0].value, "************5507 Master")
        XCTAssertEqual(transaction.list[1].label, "Pagto. Cartão")
        XCTAssertEqual(transaction.list[1].value, "R$51,50(1x 51,50)")
        XCTAssertEqual(transaction.list[2].label, "Taxa de conveniência do cartão")
        XCTAssertEqual(transaction.list[2].value, "R$1,50")
        XCTAssertEqual(transaction.list[3].label, "Cashback (dinheiro de volta)")
        XCTAssertEqual(transaction.list[3].value, "R$ 20,00")
        XCTAssertEqual(transaction.list[4].label, "Total pago")
        XCTAssertEqual(transaction.list[4].value, "R$ 51,50")
        XCTAssertEqual(transaction.shareUrl, "http://www.picpay.com/locais?")
        XCTAssertEqual(transaction.shareText, "Visitei Pagamento de contas e paguei com PicPay http://www.picpay.com/locais? #PicPay")
        
        let list = receipt[1] as! ListReceiptWidgetItem
        XCTAssertEqual(list.items[0].label, "Descrição")
        XCTAssertEqual(list.items[0].value, "Dívida")
        XCTAssertEqual(list.items[1].label, "Vencimento")
        XCTAssertEqual(list.items[1].value, "18/10/2018")
        XCTAssertEqual(list.items[2].label, "Beneficiário")
        XCTAssertEqual(list.items[2].value, "BANCO BRADESCO")
        
        let label = receipt[2] as! LabelValueReceiptWidgetItem
        XCTAssertEqual(label.label, "Código de barras")
        XCTAssertEqual(label.details, "23793.38128 60001.114101 38000.063305 4 76810000005000")
        
        let cashback = receipt[3] as! CashbackReceipWidgetItem
        XCTAssertEqual(cashback.title, "Seu último pagamento rendeu cashback.")
        XCTAssertEqual(cashback.subtitle, "Você ganhou 20,00 reais para usar no PicPay com o que quiser.")
        XCTAssertEqual(label.details, "23793.38128 60001.114101 38000.063305 4 76810000005000")
    }
    
    func testParseCieloReceiptWithSuccess() {
        let json = Mock.json(.cielo)
        let receipt = WSReceipt.createReceiptWidgetItem(json)
        
        XCTAssertTrue(receipt[0] is TransactionReceiptWidgetItem)
        XCTAssertTrue(receipt[1] is CashbackReceipWidgetItem)
        XCTAssertTrue(receipt[2] is ImageLabelValueButtonReceiptWidgetItem)
        let imageLabel = receipt[2] as! ImageLabelValueButtonReceiptWidgetItem
        XCTAssertEqual(imageLabel.title.text, "Indique e ganhe R$10")
        XCTAssertEqual(imageLabel.text.text, "Gostando do PicPay? Indique seus amigos e ganhe R$10 por cada indicação.")
        XCTAssertEqual(imageLabel.button?.title, "INDICAR AGORA")
        XCTAssertEqual(imageLabel.button?.type.rawValue, "ghost")
    }
    
    func testParseDGPhoneReceiptWithSuccess() {
        let json = Mock.json(.dgPhoneRecharge)
        let receipt = WSReceipt.createReceiptWidgetItem(json)
        
        XCTAssertTrue(receipt[0] is TransactionReceiptWidgetItem)
        XCTAssertTrue(receipt[1] is ListReceiptWidgetItem)
        XCTAssertTrue(receipt[2] is ImageLabelValueButtonReceiptWidgetItem)
    }
    
    func testParseDGRechargeTVWithSuccess() {
        let json = Mock.json(.dgRechargeTV)
        let receipt = WSReceipt.createReceiptWidgetItem(json)
        
        XCTAssertTrue(receipt[0] is TransactionReceiptWidgetItem)
        XCTAssertTrue(receipt[1] is ListReceiptWidgetItem)
        XCTAssertTrue(receipt[2] is ImageLabelValueButtonReceiptWidgetItem)
    }
    
    func testParseDGSteamReceiptWithSuccess() {
        let json = Mock.json(.dgSteam)
        let receipt = WSReceipt.createReceiptWidgetItem(json)
        
        XCTAssertTrue(receipt[0] is TransactionReceiptWidgetItem)
        XCTAssertTrue(receipt[1] is VoucherReceiptWidgetItem)
        let voucher = receipt[1] as! VoucherReceiptWidgetItem
        XCTAssertEqual(voucher.title, "Seu código PIN")
        XCTAssertEqual(voucher.voucher, "40BJA-PNH0V-4TCFX")
        XCTAssertEqual(voucher.button?.title, "Saiba mais")
        XCTAssertEqual(voucher.button?.url.sanitized, """
https://cdn.picpay.com/apps/picpay/digital-goods/steam_info.html
""")
        
        XCTAssertTrue(receipt[2] is OpenUrlReceiptWidgetItem)
        let openUrl = receipt[2] as! OpenUrlReceiptWidgetItem
        XCTAssertEqual(openUrl.title, "Abrir na Steam")
        XCTAssertEqual(openUrl.url, """
https://store.steampowered.com/account/redeemwalletcode?code=40BJA-PNH0V-4TCFX
""")
        XCTAssertTrue(receipt[3] is ImageLabelValueButtonReceiptWidgetItem)
        
    }
    
    func testParseDGUberReceiptWithSuccess() {
        let json = Mock.json(.dgUber)
        let receipt = WSReceipt.createReceiptWidgetItem(json)
        
        XCTAssertTrue(receipt[0] is TransactionReceiptWidgetItem)
        XCTAssertTrue(receipt[1] is VoucherReceiptWidgetItem)
        XCTAssertTrue(receipt[2] is OpenUrlReceiptWidgetItem)
        XCTAssertTrue(receipt[3] is CashbackReceipWidgetItem)
        XCTAssertTrue(receipt[4] is ImageLabelValueButtonReceiptWidgetItem)
    }
    
    func testParseOnlineCancelledReceiptWithSuccess() {
        let json = Mock.json(.onlineCancelled)
        let receipt = WSReceipt.createReceiptWidgetItem(json)
        
        XCTAssertTrue(receipt[0] is TransactionReceiptWidgetItem)
        let transaction = receipt[0] as! TransactionReceiptWidgetItem
        XCTAssertTrue(transaction.isCanceled)
        XCTAssertTrue(receipt[1] is TransactionStatusReceiptWidgetItem)
        XCTAssertTrue(receipt[2] is ListReceiptWidgetItem)
        XCTAssertTrue(receipt[3] is LabelValueCentralizedReceiptWidgetItem)
        let label = receipt[3] as! LabelValueCentralizedReceiptWidgetItem
        XCTAssertEqual(label.label, "Você comprou em Orange Pixel")
        XCTAssertEqual(label.details, "Em breve você receberá um e-mail no endereço gabriel@joaoagbriel.org com todos os detalhes do pagamento.")
        
        XCTAssertTrue(receipt[4] is CashbackReceipWidgetItem)
        XCTAssertTrue(receipt[5] is ImageLabelValueButtonReceiptWidgetItem)
    }
    
    func testParsep2pInAnalysisWithSuccess() {
        let json = Mock.json(.p2pInAnalysisPopup)
        let receipt = WSReceipt.createReceiptWidgetItem(json)
        
        XCTAssertTrue(receipt[0] is TransactionReceiptWidgetItem)
        XCTAssertTrue(receipt[1] is TransactionStatusReceiptWidgetItem)
        let status = receipt[1] as! TransactionStatusReceiptWidgetItem
        XCTAssertEqual(status.title, "Pagamento em análise")
        XCTAssertEqual(status.subtitle, "O pagamento será concluído após análise")
        XCTAssertEqual(status.actionButton?.action.rawValue, "accelerate_analysis")
        XCTAssertEqual(status.readMoreButton?.title, "Saiba mais")
        XCTAssertEqual(status.icon!.type, .asset)
        
        XCTAssertTrue(receipt[2] is PopupReceiptWidgetItem)
        let popup = receipt[2] as! PopupReceiptWidgetItem
        XCTAssertEqual(popup.displayEvent, .onStart)
        XCTAssertEqual(popup.alert!.title, NSAttributedString(string: "Pagamento em análise"))
        XCTAssertEqual(popup.alert!.text, NSAttributedString(string: "Acelere a análise para concluir seu pagamento agora."))
        XCTAssertEqual(popup.alert!.buttons.count, 2)
        XCTAssertEqual(popup.alert!.showCloseButton, true)
        
        XCTAssertTrue(receipt[3] is ImageLabelValueButtonReceiptWidgetItem)
    }
    
    func testParsep2pWithSuccess() {
        let json = Mock.json(.p2pSendMoney)
        let receipt = WSReceipt.createReceiptWidgetItem(json)
        
        XCTAssertTrue(receipt[0] is TransactionReceiptWidgetItem)
        XCTAssertTrue(receipt[1] is ImageLabelValueButtonReceiptWidgetItem)
    }
    
    func testParkingOldWithSuccess() {
        let json = Mock.json(.parkingOld)
        let receipt = WSReceipt.createReceiptWidgetItem(json)
        
        XCTAssertTrue(receipt[0] is TransactionReceiptWidgetItem)
        XCTAssertTrue(receipt[1] is ExpiryAtReceiptWidgetItem)
        let expiry = receipt[1] as! ExpiryAtReceiptWidgetItem
        XCTAssertEqual(expiry.text, "Placa: MZV3216")
        XCTAssertEqual(expiry.title, "Válido até")
        XCTAssertEqual(expiry.datetime, 1553698254)
        XCTAssertEqual(expiry.date, "27/03 às 11:50")
        
        XCTAssertTrue(receipt[2] is CashbackReceipWidgetItem)
        XCTAssertTrue(receipt[3] is ImageLabelValueButtonReceiptWidgetItem)
    }
    
    func testSubscriptionWithSuccess() {
        let json = Mock.json(.subscription)
        let receipt = WSReceipt.createReceiptWidgetItem(json)
        
        XCTAssertTrue(receipt[0] is TransactionReceiptWidgetItem)
        XCTAssertTrue(receipt[1] is SubscriptionReceiptWidgetItem)
        let subscription = receipt[1] as! SubscriptionReceiptWidgetItem
        XCTAssertEqual(subscription.title, "Sua assinatura")
        XCTAssertEqual(subscription.descriptionList[0].label, "Plano")
        XCTAssertEqual(subscription.descriptionList[0].value, "Premium_TESTE")
        XCTAssertEqual(subscription.descriptionList[1].label, "Valor da assinatura")
        XCTAssertEqual(subscription.descriptionList[1].value, "R$ 20,00")
        XCTAssertEqual(subscription.descriptionList[2].label, "Plano mensal")
        XCTAssertEqual(subscription.descriptionList[2].value, "Próxima cobrança 19/04/2019")
        XCTAssertEqual(subscription.seeMoreButton?.subscriptionId, "278")
        XCTAssertEqual(subscription.seeMoreButton?.title, "Ver mais detalhes")
        
        XCTAssertTrue(receipt[2] is ImageLabelValueButtonReceiptWidgetItem)
    }
    
    func testStoreWithSuccess() {
        let json = Mock.json(.store)
        let receipt = WSReceipt.createReceiptWidgetItem(json)
        
        XCTAssertTrue(receipt[0] is TransactionReceiptWidgetItem)
        XCTAssertTrue(receipt[1] is ImageLabelValueButtonReceiptWidgetItem)
    }
}

private enum Mock: String {
    case bill
    case cielo
    case dgPhoneRecharge
    case dgRechargeTV
    case dgSteam
    case dgUber
    case onlineCancelled
    case p2pInAnalysisPopup
    case p2pInAnalysis
    case p2pSendMoney
    case parkingOld
    case subscription
    case store
    
    static func json(_ type: Mock) -> JSON {
        return MockJSON().load(resource: type.rawValue)["data"]["receipt"]
    }
}
