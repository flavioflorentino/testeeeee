import Core
import XCTest
import OHHTTPStubs
import SwiftyJSON
@testable import PicPay

class ReceiptWidgetViewModelTest: XCTestCase {
    var viewModel: ReceiptWidgetViewModel!
    
    override func setUp() {
        super.setUp()
        viewModel = ReceiptWidgetViewModel(type: .P2P)
        KVStore().removeObjectFor(.cashbackPopup)   
    }
    override func tearDown() {
        viewModel = nil
        super.tearDown()
    }
    
    func testGetReceiptInAnalysisSuccess() {
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(at: Mock.filename(.p2pInAnalysis))
        }
        let receiptExpectation = XCTestExpectation(description: "expected to receive valid receipt")
        viewModel.getReceipt(onSuccess: {
            receiptExpectation.fulfill()
        }, onError: { _ in
            XCTFail("Was expecting success")
        })
        wait(for: [receiptExpectation], timeout: 5.0)
        XCTAssertEqual(viewModel.widgets.count, 3)
        
        viewModel.showAlert = { popup in
            XCTFail("Was not expecting have an alert to show")
        }
        viewModel.showAlertIfNecessary()
    }

    func testGetReceiptWithAnalysisPopupSuccess() {
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(at: Mock.filename(.p2pInAnalysisPopup))
        }
        let receiptExpectation = XCTestExpectation(description: "expected to receive valid receipt")
        viewModel.getReceipt(onSuccess: {
            receiptExpectation.fulfill()
        }, onError: { _ in
            XCTFail("Was expecting success")
        })
        wait(for: [receiptExpectation], timeout: 5.0)
        XCTAssertEqual(viewModel.widgets.count, 3)
        
        let alertExpectation = XCTestExpectation(description: "expected to show a valid alert")
        viewModel.showAlert = { _ in
            alertExpectation.fulfill()
        }
        viewModel.showAlertIfNecessary()
        wait(for: [alertExpectation], timeout: 5.0)
    }
    
    func testGetReceiptFailure() {
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.fail()
        }
        let receiptExpectation = XCTestExpectation(description: "expected to not receive a receipt")
        viewModel.getReceipt(onSuccess: {
            XCTFail("Was expecting failure")
        }, onError: { _ in
            receiptExpectation.fulfill()
        })
        wait(for: [receiptExpectation], timeout: 5.0)
        XCTAssertEqual(viewModel.widgets.count, 0)
        
        viewModel.showAlert = { _ in
            XCTFail("Was not expecting have an alert to show")
        }
        viewModel.showAlertIfNecessary()
    }
    
    func testSetReceiptWidgetsWithCashbackWidget() {
        viewModel = ReceiptWidgetViewModel(type: .PAV)
        let json = Mock.json(.cielo)
        let receipt = WSReceipt.createReceiptWidgetItem(json)

        viewModel.setReceiptWidgets(items: receipt)
        XCTAssertEqual(viewModel.widgets.count, 3)
        let cashbackDict = KVStore().dictionaryFor(.cashbackPopup)
        XCTAssertNotNil(cashbackDict)
    }
    
    func testSetReceiptWidgetsWithoutCashbackWidget() {
        viewModel = ReceiptWidgetViewModel(type: .PAV)
        let json = Mock.json(.dgSteam)
        let receipt = WSReceipt.createReceiptWidgetItem(json)
        
        viewModel.setReceiptWidgets(items: receipt)
        XCTAssertEqual(viewModel.widgets.count, 4)
        let cashbackDict = KVStore().dictionaryFor(.cashbackPopup)
        XCTAssertNil(cashbackDict)
    }
    
    func testSetReceiptWidgetsWithPopupWidget() {
        let json = Mock.json(.p2pInAnalysisPopup)
        let receipt = WSReceipt.createReceiptWidgetItem(json)
        
        viewModel.setReceiptWidgets(items: receipt)
        XCTAssertEqual(viewModel.widgets.count, 3)
        let cashbackDict = KVStore().dictionaryFor(.cashbackPopup)
        XCTAssertNil(cashbackDict)
        
        let alertExpectation = XCTestExpectation(description: "expected to show a valid alert")
        viewModel.showAlert = { _ in
            alertExpectation.fulfill()
        }
        viewModel.showAlertIfNecessary()
        wait(for: [alertExpectation], timeout: 5.0)
    }
    
    func testSetReceiptWidgetsWithoutPopupWidget() {
        let json = Mock.json(.p2pInAnalysis)
        let receipt = WSReceipt.createReceiptWidgetItem(json)
        viewModel.setReceiptWidgets(items: receipt)
        XCTAssertEqual(viewModel.widgets.count, 3)
        let cashbackDict = KVStore().dictionaryFor(.cashbackPopup)
        XCTAssertNil(cashbackDict)
        
        viewModel.showAlert = { _ in
            XCTFail("Was not expecting have an alert to show")
        }
        viewModel.showAlertIfNecessary()
    }
    
    func testSetReceiptWidgetsWithoutValidPopupWidget() {
        let json = Mock.json(.p2pInAnalysis)
        let receipt = WSReceipt.createReceiptWidgetItem(json)
        viewModel.setReceiptWidgets(items: receipt)
        XCTAssertEqual(viewModel.widgets.count, 3)
        let cashbackDict = KVStore().dictionaryFor(.cashbackPopup)
        XCTAssertNil(cashbackDict)
        
        viewModel.showAlert = { _ in
            XCTFail("Was not expecting have an alert to show")
        }
        viewModel.showAlertIfNecessary()
    }
}

private enum Mock: String {
    case cielo
    case dgSteam
    case p2pInAnalysisPopup
    case p2pInAnalysis
    
    static func json(_ type: Mock) -> JSON {
        return MockJSON().load(resource: type.rawValue)["data"]["receipt"]
    }
    
    static func filename(_ type: Mock) -> String {
        return "\(type.rawValue).json"
    }
}
