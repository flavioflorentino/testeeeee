import XCTest
@testable import PicPay

class DeeplinkTrackTest: XCTestCase {
    enum Resource {
        case deeplinkTrackDefault
        case deeplinkTrackUnique
        case deeplinkTrackPropertiesEmpty
        case deeplinkTrackPropertiesNil
    }
    
    func instantiate(type: Resource) -> DeeplinkTrack {
        let deeplink = try! MockDecodable<[DeeplinkTrack]>().loadCodableObject(resource: "deeplink_track")
        
        switch type {
        case .deeplinkTrackDefault:
            return deeplink[0]
        case .deeplinkTrackUnique:
            return deeplink[1]
        case .deeplinkTrackPropertiesEmpty:
            return deeplink[2]
        case .deeplinkTrackPropertiesNil:
            return deeplink[3]
        }
    }
    
    func testdeeplinkTrackDefault(){
        let deeplink = instantiate(type: .deeplinkTrackDefault)
        XCTAssertEqual(deeplink.type, .normal)
    }
    
    func testdeeplinkTrackUnique(){
        let deeplink = instantiate(type: .deeplinkTrackUnique)
        XCTAssertEqual(deeplink.type, .firstTimeOnly)
    }
    
    func testdeeplinkTrackProperties(){
        let deeplink = instantiate(type: .deeplinkTrackDefault)
        XCTAssertEqual(deeplink.properties!.count, deeplink.propertiesDictionary()!.count)
        
        let key = deeplink.properties![0].key
        XCTAssertTrue(deeplink.propertiesDictionary()!.keys.contains(key))
        XCTAssertEqual(deeplink.properties![0].value , deeplink.propertiesDictionary()![key])
    }
    
    func testdeeplinkTrackPropertiesEmpty(){
        let deeplink = instantiate(type: .deeplinkTrackPropertiesEmpty)
        XCTAssertEqual(deeplink.type, .firstTimeOnly)
        
        XCTAssertTrue(deeplink.properties.isEmpty)
        XCTAssertTrue(deeplink.propertiesDictionary()!.isEmpty)
    }
    
    func testdeeplinkTrackPropertiesNil(){
        let deeplink = instantiate(type: .deeplinkTrackPropertiesNil)
        XCTAssertNil(deeplink.properties)
        XCTAssertNil(deeplink.propertiesDictionary())
    }
}
