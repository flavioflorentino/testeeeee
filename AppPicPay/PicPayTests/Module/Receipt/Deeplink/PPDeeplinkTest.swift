import XCTest
@testable import PicPay

class DeeplinkWorkerTest: DeeplinkWorkerProtocol {
    func getDeeplinkTrack() -> [DeeplinkTrack]? {
        let deeplink = try! MockDecodable<[DeeplinkTrack]>().loadCodableObject(resource: "deeplink_track")
        return deeplink
    }
}

class PPDeeplinkTest: XCTestCase {
    enum UrlDeeplink: String {
        case analyticsExistIdToo = "picpay://picpay/payment?type=digital_good&id=5a8487870ce368f8f334cf05&analytics=1"
        case analyticsExistIdNo = "picpay://picpay/payment?type=digital_good&id=5a8487870ce368f8f334cf05&analytics=12345111"
        case analyticsNotExist = "picpay://picpay/payment?type=digital_good&id=5a8487870ce368f8f334cf05"
    }
    
    func instantiate(url: UrlDeeplink) -> PPDeeplink {
        return PPDeeplink(url: URL(string: url.rawValue)!, worker: DeeplinkWorkerTest())
    }
    
    func testAnalyticsExistIdToo() {
        let model = instantiate(url: .analyticsExistIdToo)
        XCTAssertNotNil(model.track)
    }
    
    func testAnalyticsExistIdNo() {
        let model = instantiate(url: .analyticsExistIdNo)
        XCTAssertNil(model.track)
    }
    
    func testAnalyticsNotExist() {
        let model = instantiate(url: .analyticsNotExist)
        XCTAssertNil(model.track)
    }
}
