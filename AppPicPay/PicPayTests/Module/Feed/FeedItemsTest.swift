import XCTest
import SwiftyJSON
@testable import PicPay

class FeedViewModelTest: XCTestCase {
    let defaultFeedItems: [NSObject] = {
        let json = MockJSON().load(resource: "DefaultFeedItemAnalysis")
        let dataDict = json["data"].dictionaryObject!
        let feedDict = dataDict["feed"] as! [NSObject]
        return feedDict
    }()
    
    func testParseDefaultFeedItemAnalysis() {
        let firstItemDict = defaultFeedItems[0] as! [AnyHashable: Any]
        var item = DefaultFeedItem(jsonDict: firstItemDict)!
        
        XCTAssertEqual(item.title.value, "<b>Você</b> pagou a <b>[link]@edu.aaa[/link]</b>")
        
        XCTAssertEqual(item.image!.url, "https://picpay-dev.s3.amazonaws.com/profiles-processed-images/8782e17ca76f07137026c0446c591fde.200.jpg")
        XCTAssertEqual(item.image!.action!.type, .OpenConsumerProfile)
        
        XCTAssertEqual(item.text!.value, "")
        XCTAssertEqual(item.text!.style, "comment")
        
        XCTAssertEqual(item.money!.value, "<font color='#ED1846'><b>R$ 2,33</b></font>")
        XCTAssertNil(item.money!.style)
        
        XCTAssertEqual(item.timeAgo!.value, "3 meses atrás")
        XCTAssertNil(item.timeAgo!.style)
        XCTAssertEqual(item.timeAgoIcon!.icon, "friends")
        XCTAssertEqual(item.timeAgoIcon!.color, "#bbbbbb")
        
        XCTAssertEqual(item.comments!.value, "4")
        
        XCTAssertEqual(item.like!.checked, true)
        XCTAssertEqual(item.like!.value, "1")
        
        XCTAssertEqual(item.alert!.type, .warning)
        XCTAssertEqual(item.alert!.text, "<font color='#FFA800'>Este pagamento está em análise.</font>")
        
        XCTAssertEqual(item.buttons.count, 2)
        XCTAssertEqual(item.buttons[.primary]!.actionDataUrl?.absoluteString, "https://app.picpay.com/accelerateAnalysis?transactionId=4506890&transactionType=p2p")
        
        XCTAssertNotNil(item.deprecatedButton)
        XCTAssertEqual(item.deprecatedButton!.action, "openP2PTransactionVerification")
        XCTAssertEqual(item.deprecatedButton!.data, "4506890")
        XCTAssertEqual(item.deprecatedButton!.text, "Acelerar a análise")
        XCTAssertEqual(item.deprecatedButton!.type, "warning")
        
        let secondItemDict = defaultFeedItems[1] as! [AnyHashable: Any]
        item = DefaultFeedItem(jsonDict: secondItemDict)!
        XCTAssertEqual(item.buttons.count, 1)
        XCTAssertNotNil(item.deprecatedButton)
        
        let thirdItemDict = defaultFeedItems[2] as! [AnyHashable: Any]
        item = DefaultFeedItem(jsonDict: thirdItemDict)!
        XCTAssertEqual(item.buttons.count, 0)
        XCTAssertNotNil(item.deprecatedButton)
    }
}
