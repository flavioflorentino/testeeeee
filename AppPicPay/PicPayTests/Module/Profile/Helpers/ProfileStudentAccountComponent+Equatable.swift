@testable import PicPay

extension ProfileStudentAccountComponent: Equatable {
    public static func == (lhs: ProfileStudentAccountComponent, rhs: ProfileStudentAccountComponent) -> Bool {
        return lhs.description == rhs.description &&
            lhs.descriptionColor == rhs.descriptionColor &&
            lhs.descriptionDarkColor == rhs.descriptionDarkColor &&
            lhs.backgroundColor == rhs.backgroundColor &&
            lhs.backgroundDarkColor == rhs.backgroundDarkColor &&
            lhs.buttonLink == rhs.buttonLink &&
            lhs.buttonText == rhs.buttonText &&
            lhs.buttonColor == rhs.buttonColor &&
            lhs.buttonDarkColor == rhs.buttonDarkColor
    }
}
