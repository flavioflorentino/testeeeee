import DirectMessage

enum DirectMessageStub {
    case sender
    case recipient

    var contact: DirectMessage.Contact {
        Contact(id: Int(), imageURL: String(), nickname: String(), name: String())
    }
}
