import UI

@testable import PicPay

final class ConsumerProfileViewControllerSpy: UIViewController, ProfileDisplay {
    private(set) var didCallShowStudentAccountView = 0
    private(set) var didCallSetStudentWebview = 0
    private(set) var displayAlertCallCount = 0
    private(set) var displayPaymentRequestLimitExceededCallCount = 0
    private(set) var displayPaymentRequestSuccessCallCount = 0
    private(set) var displayPaymentRequestErrorCallCount = 0
    private(set) var setMessageButtonVisibilityCallCount = 0

    private(set) var messageButtonVisibility = false
    private(set) var studentAccountViewComponent: ProfileStudentAccountComponent?

    func displayStudentAccountView(with component: ProfileStudentAccountComponent) {
        didCallShowStudentAccountView += 1
        studentAccountViewComponent = component
    }

    func displayAlert(title: String, message: String, buttonTitle: String) {
        displayAlertCallCount += 1
    }

    func displayPaymentRequestLimitExceeded(with alertData: StatusAlertAttributedData) {
        displayPaymentRequestLimitExceededCallCount += 1
    }

    func displayPaymentRequestSuccess() {
        displayPaymentRequestSuccessCallCount += 1
    }

    func displayPaymentRequestError(with alertData: StatusAlertData) {
        displayPaymentRequestErrorCallCount += 1
    }

    func setMessageButtonVisibility(_ visibility: Bool) {
        setMessageButtonVisibilityCallCount += 1
        messageButtonVisibility = visibility
    }
}
