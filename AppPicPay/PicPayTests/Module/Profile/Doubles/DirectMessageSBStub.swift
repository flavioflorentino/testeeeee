@testable import PicPay

enum DirectMessageSBStub: String {
    case regularConsumer = "PPContactRegular"

    var contact: PPContact? {
        let json = MockJSON().load(resource: self.rawValue)
        return PPContact(profileDictionary: json.toDictionary())
    }
}
