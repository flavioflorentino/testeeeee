import AnalyticsModule
import Core
import FeatureFlag
import DirectMessage
import DirectMessageSB
import XCTest

@testable import PicPay

private enum Seeds: String {
    case profileRegular = "PPContactRegular"
    case profilePRO = "PPContactPro"
    case profileVerified = "PPContactVerified"
    case profilePROAndVerified = "PPContactProAndVerified"
    
    var contact: PPContact? {
        let json = MockJSON().load(resource: self.rawValue)
        return PPContact(profileDictionary: json.toDictionary())
    }
}

final private class ProfileServiceMock: ProfileServicing {
    var isSuccess = false
    var hasStudentComponentFeatureActive = false
    var isValidUrl = false

    var expectedResult: Result<Void, Error>?
    var canReachExpectedResult: Result<Bool, MessagingClientError> = .success(true)
    var hasStudentComponentFeature: Bool {
        return hasStudentComponentFeatureActive
    }

    var studentComponent: ProfileStudentAccountComponent? {
        let component = ProfileStudentAccountComponent(
            description: "Seja ContaUni também",
            descriptionColor: "#000000",
            descriptionDarkColor: "#FFFFFF",
            backgroundColor: "#F4F4F6",
            backgroundDarkColor: "#3D4451",
            buttonLink: "https://picpay.com/app_webviews/conta-universitaria/offer",
            buttonText: "SaibaMais",
            buttonColor: "#000000",
            buttonDarkColor: "#FFFFFF"
        )

        return component
    }

    var studentOfferWebview: String {
        if isValidUrl {
            return "https://picpay.com/app_webviews/conta-universitaria/offer"
        }
        return ""
    }

    func loadStudentStatus(consumerId: String, _ completion: @escaping (PicPayResult<StatusPayload>) -> Void) {
        let successPayload = StatusPayload(
            status: .active,
            title: "ContaUni ativada!",
            message: "Agora você pode fazer suas compras e receber 10% de cashback em diversão, recargas e passeios.",
            image: nil
        )

        if isSuccess {
            completion(PicPayResult.success(successPayload))
        } else {
            completion(PicPayResult.failure(PicPayError(message: "some error message")))
        }
    }
    
    func abusesUser(consumerId: String, action: AbusesAction, _ completion: @escaping (Result<Void, Error>) -> Void) {
        guard let expectedResult = expectedResult else {
            XCTFail("Expected result not defined")
            return
        }
        completion(expectedResult)
    }

    func canReach(user: ChatUser, completion: @escaping (Result<Bool, MessagingClientError>) -> Void) {
        completion(canReachExpectedResult)
    }
}

final private class ProfilePresenterSpy: ProfilePresenting {
    private(set) var didCallPresentStudentAccountView = 0
    private(set) var didCallPresentStudentAccountOffer = 0
    private(set) var notAllowedAlertCallCount = 0
    private(set) var limitExceededCallCount = 0
    private(set) var paymentRequestSuccessCallCount = 0
    private(set) var paymentRequestErrorCallCount = 0
    private(set) var presentPaymentRequestLimitExceededV2CallCount = 0
    private(set) var openDirectMessageCallCount = 0
    private(set) var openDirectMessageSBCallCount = 0
    private(set) var setMessageButtonVisibilityCallCount = 0

    private(set) var messageButtonVisibility = false
    private(set) var userForDirectMessageSB: ChatUser?
    private(set) var studentAccountViewComponent: ProfileStudentAccountComponent?
    private(set) var studentAccountOfferWebviewUrl: URL?

    var viewController: ProfileDisplay?
        
    func presentStudentAccountView(with component: ProfileStudentAccountComponent) {
        didCallPresentStudentAccountView += 1
        studentAccountViewComponent = component
    }
    
    func presentStudentAccountOffer(url: URL) {
        didCallPresentStudentAccountOffer += 1
        studentAccountOfferWebviewUrl = url
    }
    
    func presentPaymentRequestNotAllowedAlert(username: String) {
        notAllowedAlertCallCount += 1
    }
    
    func presentPaymentRequestLimitExceeded() {
        limitExceededCallCount += 1
    }
    
    func presentPaymentRequestLimitExceededV2(title: String, message: String, buttonTitle: String) {
        presentPaymentRequestLimitExceededV2CallCount += 1
    }
    
    func presentPaymentRequestSuccess(profile: PPContact) {
        paymentRequestSuccessCallCount += 1
    }
    
    func presentPaymentRequestError() {
        paymentRequestErrorCallCount += 1
    }

    func openDirectMessage(sender: Contact, recipient: Contact) {
        openDirectMessageCallCount += 1
    }
    
    func openDirectMessageSB(with user: ChatUser) {
        openDirectMessageSBCallCount += 1
        userForDirectMessageSB = user
    }

    func setMessageButtonVisibility(_ visibility: Bool) {
        setMessageButtonVisibilityCallCount += 1
        messageButtonVisibility = visibility
    }
}

private final class PaymentRequestSelectorServiceMock: PaymentRequestSelectorServicing {
    var options: [PaymentRequestOption] = []
    var permissionResult: Result<(model: PaymentRequestPermissionData, data: Data?), ApiError>?
    var permissionResultV2: Result<(model: PaymentRequestPermissionV2, data: Data?), DialogError>?
    private(set) var getOptionsCallCount = 0
    private(set) var getPaymentRequestPermissionCallCount = 0
    private(set) var getPaymentRequestPermissionV2CallCount = 0
    
    func getOptions() -> [PaymentRequestOption] {
        getOptionsCallCount += 1
        return options
    }
    
    func getPaymentRequestPermission(completion: @escaping (Result<(model: PaymentRequestPermissionData, data: Data?), ApiError>) -> Void) {
        getPaymentRequestPermissionCallCount += 1
        guard let result = permissionResult else {
            return
        }
        completion(result)
    }
    
    func getPaymentRequestPermissionV2(completion: @escaping (Result<(model: PaymentRequestPermissionV2, data: Data?), DialogError>) -> Void) {
        getPaymentRequestPermissionV2CallCount += 1
        guard let result = permissionResultV2 else {
            return
        }
        completion(result)
    }
}

final class NewProfileViewModelTests: XCTestCase {
    // MARK: - Variables
    private let serviceMock = ProfileServiceMock()
    private let presenterSpy = ProfilePresenterSpy()
    private let paymentRequestService = PaymentRequestSelectorServiceMock()
    private var contact: PPContact?
    private let analyticsSpy = AnalyticsSpy()
    private let featureManager = FeatureManagerMock()
    
    private lazy var sut: NewProfileViewModel = {
        let mockDependencies = DependencyContainerMock(analyticsSpy, featureManager)
        let sut = NewProfileViewModel(
            profileId: "00001",
            placeHolderContact: contact,
            service: serviceMock,
            paymentRequestService: paymentRequestService,
            presenter: presenterSpy,
            dependencies: mockDependencies
        )
        return sut
    }()
    
    func testCheckConsumerStudentStatus_WhenHasStudentComponentFeatureIsFalse_ShouldNotCallPresenterStudentMethodsAndTrackEvents() {
        serviceMock.hasStudentComponentFeatureActive = false
        sut.userProfile = Seeds.profileRegular.contact
        
        sut.checkConsumerStudentStatus()
        
        XCTAssertEqual(presenterSpy.didCallPresentStudentAccountView, 0)
        XCTAssertNil(presenterSpy.studentAccountViewComponent)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        let expectedEvent = ProfileEvent.accessed(type: .regular).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testCheckConsumerStudentStatus_WhenServiceLoadStudentStatusFailureAndHasContactRegular_ShouldNotCallPresenterStudentMethodsAndTrackEvents() {
        serviceMock.hasStudentComponentFeatureActive = true
        serviceMock.isSuccess = false
        sut.userProfile = Seeds.profileRegular.contact
        
        sut.checkConsumerStudentStatus()
        
        XCTAssertEqual(presenterSpy.didCallPresentStudentAccountView, 0)
        XCTAssertNil(presenterSpy.studentAccountViewComponent)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        let expectedEvent = ProfileEvent.accessed(type: .regular).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testCheckConsumerStudentStatus_WhenServiceLoadStudentStatusSuccessAndContactIsPro_ShouldNotCallPresenterStudentMethodsAndTrackEvents() {
        serviceMock.hasStudentComponentFeatureActive = true
        serviceMock.isSuccess = true
        sut.userProfile = Seeds.profilePRO.contact
        
        sut.checkConsumerStudentStatus()
        
        XCTAssertEqual(presenterSpy.didCallPresentStudentAccountView, 0)
        XCTAssertNil(presenterSpy.studentAccountViewComponent)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 2)
        let expectedProEvent = ProfileEvent.accessed(type: .pro).event()
        let expectedStudentEvent = ProfileEvent.accessed(type: .university).event()
        print(analyticsSpy.events)
        XCTAssertTrue(analyticsSpy.equals(to: expectedProEvent, expectedStudentEvent))
        
    }
    
    func testCheckConsumerStudentStatus_WhenServiceLoadStudentStatusSuccessAndContactIsVerified_ShouldNotCallPresenterStudentMethods() {
        serviceMock.hasStudentComponentFeatureActive = true
        serviceMock.isSuccess = true
        sut.userProfile = Seeds.profileVerified.contact
        
        sut.checkConsumerStudentStatus()
        
        XCTAssertEqual(presenterSpy.didCallPresentStudentAccountView, 0)
        XCTAssertNil(presenterSpy.studentAccountViewComponent)
        XCTAssertEqual(analyticsSpy.logCalledCount, 2)
        let expectedVerifiedEvent = ProfileEvent.accessed(type: .verified).event()
        let expectedStudentEvent = ProfileEvent.accessed(type: .university).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedVerifiedEvent, expectedStudentEvent))
    }
    
    func testCheckConsumerStudentStatus_WhenServiceLoadStudentStatusSuccessAndContactIsProAndVerified_ShouldNotCallPresenterStudentMethods() {
        serviceMock.hasStudentComponentFeatureActive = true
        serviceMock.isSuccess = true
        sut.userProfile = Seeds.profilePROAndVerified.contact
        
        sut.checkConsumerStudentStatus()
        
        XCTAssertEqual(presenterSpy.didCallPresentStudentAccountView, 0)
        XCTAssertNil(presenterSpy.studentAccountViewComponent)
        XCTAssertEqual(analyticsSpy.logCalledCount, 3)
        let expectedProEvent = ProfileEvent.accessed(type: .pro).event()
        let expectedVerifiedEvent = ProfileEvent.accessed(type: .verified).event()
        let expectedStudentEvent = ProfileEvent.accessed(type: .university).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedProEvent, expectedVerifiedEvent, expectedStudentEvent))
    }
    
    func testCheckConsumerStudentStatus_WhenServiceLoadStudentStatusSuccessAndContactIsRegular_ShouldShowStudentAccountView() {
        serviceMock.hasStudentComponentFeatureActive = true
        serviceMock.isSuccess = true
        sut.userProfile = Seeds.profileRegular.contact
        
        sut.checkConsumerStudentStatus()
        
        let expectedShowStudentAccountViewComponent = ProfileStudentAccountComponent(
            description: "Seja ContaUni também",
            descriptionColor: "#000000",
            descriptionDarkColor: "#FFFFFF",
            backgroundColor: "#F4F4F6",
            backgroundDarkColor: "#3D4451",
            buttonLink: "https://picpay.com/app_webviews/conta-universitaria/offer",
            buttonText: "SaibaMais",
            buttonColor: "#000000",
            buttonDarkColor: "#FFFFFF"
        )
        
        XCTAssertEqual(presenterSpy.didCallPresentStudentAccountView, 1)
        XCTAssertEqual(presenterSpy.studentAccountViewComponent, expectedShowStudentAccountViewComponent)
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        let expectedEvent = ProfileEvent.accessed(type: .university).event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testOpenStudentAccountOffer_WhenHasAnUrlInvalid_ShouldTrackButtonButNotCallPresentStudentAccountOffer() {
        serviceMock.isValidUrl = false
        
        sut.openStudentAccountOffer()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        let expectedEvent = StudentAccountEvent.profileOffer.event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        
        XCTAssertEqual(presenterSpy.didCallPresentStudentAccountOffer, 0)
        XCTAssertNil(presenterSpy.studentAccountOfferWebviewUrl)
    }
    
    func testOpenStudentAccountOffer_WhenHasAnUrlValid_ShouldTrackButtonAndCallPresentStudentAccountOffer() {
        serviceMock.isValidUrl = true
        
        sut.openStudentAccountOffer()
        
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        let expectedEvent = StudentAccountEvent.profileOffer.event()
        XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        
        XCTAssertEqual(presenterSpy.didCallPresentStudentAccountOffer, 1)
        do {
            let url = try XCTUnwrap(URL(string: "https://picpay.com/app_webviews/conta-universitaria/offer"))
            XCTAssertEqual(presenterSpy.studentAccountOfferWebviewUrl, url)
        } catch {
            XCTFail("Was not able to create URL")
        }
    }
    
    // MARK: - tryPaymentRequest
    func testTryPaymentRequest_WhenCanReceivePaymentRequestIsFalse_ShouldCallPresentPaymentRequestNotAllowedAlertAndNotService() {
        let config = ["payment_request": false]
        contact = PPContact(profileDictionary: ["username": "userTest", "config": config])
        sut.tryPaymentRequest()
        XCTAssertEqual(presenterSpy.notAllowedAlertCallCount, 1)
        XCTAssertEqual(paymentRequestService.getPaymentRequestPermissionCallCount, 0)
    }
    
    func testTryPaymentRequest_WhenCanReceivePaymentRequestIsTrueAndResultFailureAndPaymentRequestV2IsNotActive_ShouldCallPresentPaymentRequestError() {
        let config = ["payment_request": true]
        contact = PPContact(profileDictionary: ["username": "userTest", "config": config])
        paymentRequestService.permissionResult = .failure(ApiError.unknown(nil))
        featureManager.override(with: [.paymentRequestV2: false])
        sut.tryPaymentRequest()
        XCTAssertEqual(presenterSpy.notAllowedAlertCallCount, 0)
        XCTAssertEqual(paymentRequestService.getPaymentRequestPermissionCallCount, 1)
        XCTAssertEqual(presenterSpy.paymentRequestErrorCallCount, 1)
    }
    
    func testTryPaymentRequest_WhenCanReceivePaymentRequestIsTrueAndResultFailureAndPaymentRequestV2IsActive_ShouldCallPresentPaymentRequestError() {
        let config = ["payment_request": true]
        contact = PPContact(profileDictionary: ["username": "userTest", "config": config])
        let dialogError = DialogError.default(ApiError.unknown(nil))
        paymentRequestService.permissionResultV2 = .failure(dialogError)
        featureManager.override(with: [.paymentRequestV2: true])
        sut.tryPaymentRequest()
        XCTAssertEqual(presenterSpy.notAllowedAlertCallCount, 0)
        XCTAssertEqual(paymentRequestService.getPaymentRequestPermissionV2CallCount, 1)
        XCTAssertEqual(presenterSpy.paymentRequestErrorCallCount, 1)
    }
    
    func testTryPaymentRequest_WhenCanReceivePaymentRequestIsTrueAndResultRetryLimitExceededAndPaymentRequestV2IsNotActive_ShouldCallPresentPaymentRequestError() {
        let config = ["payment_request": true]
        let permission = PaymentRequestPermission(isP2PBlockDayChargesEnabled: true)
        let permissionData = PaymentRequestPermissionData(data: permission)
        let model: (model: PaymentRequestPermissionData, data: Data?) = (model: permissionData, data: nil)
        contact = PPContact(profileDictionary: ["username": "userTest", "config": config])
        paymentRequestService.permissionResult = .success(model)
        featureManager.override(with: [.paymentRequestV2: false])
        sut.tryPaymentRequest()
        XCTAssertEqual(presenterSpy.notAllowedAlertCallCount, 0)
        XCTAssertEqual(paymentRequestService.getPaymentRequestPermissionCallCount, 1)
        XCTAssertEqual(presenterSpy.limitExceededCallCount, 1)
    }
    
    func testTryPaymentRequest_WhenCanReceivePaymentRequestIsTrueAndResultRetryLimitExceededAndPaymentRequestV2IsActive_ShouldCallPresentPaymentRequestError() {
        let config = ["payment_request": true]
        contact = PPContact(profileDictionary: ["username": "userTest", "config": config])
        let dialogModel = DialogErrorModel(title: "", message: "", buttonText: "")
        let code = PaymentRequestSelectorViewModel.ErrorCode.limitExceeded.rawValue
        let dialogResponse = DialogResponseError(code: code, model: dialogModel)
        let dialogError = DialogError.dialog(dialogResponse)
        paymentRequestService.permissionResultV2 = .failure(dialogError)
        featureManager.override(with: [.paymentRequestV2: true])
        sut.tryPaymentRequest()
        XCTAssertEqual(presenterSpy.notAllowedAlertCallCount, 0)
        XCTAssertEqual(paymentRequestService.getPaymentRequestPermissionV2CallCount, 1)
        XCTAssertEqual(presenterSpy.presentPaymentRequestLimitExceededV2CallCount, 1)
    }
    
    func testTryPaymentRequest_WhenCanReceivePaymentRequestIsTrueAndResultSuccessAndPaymentRequestV2IsNotActive_ShouldCallPresentPaymentRequestSuccess() {
        let config = ["payment_request": true]
        contact = PPContact(profileDictionary: ["username": "userTest", "config": config])
        let permission = PaymentRequestPermission(isP2PBlockDayChargesEnabled: false)
        let response: (PaymentRequestPermissionData, Data?) = (PaymentRequestPermissionData(data: permission), nil)
        paymentRequestService.permissionResult = .success(response)
        featureManager.override(with: [.paymentRequestV2: false])
        sut.tryPaymentRequest()
        XCTAssertEqual(presenterSpy.notAllowedAlertCallCount, 0)
        XCTAssertEqual(paymentRequestService.getPaymentRequestPermissionCallCount, 1)
        XCTAssertEqual(presenterSpy.paymentRequestSuccessCallCount, 1)
    }
    
    func testTryPaymentRequest_WhenCanReceivePaymentRequestIsTrueAndResultSuccessAndPaymentRequestV2IsActive_ShouldCallPresentPaymentRequestSuccess() {
        let config = ["payment_request": true]
        contact = PPContact(profileDictionary: ["username": "userTest", "config": config])
        let permission = PaymentRequestPermissionV2(maxUsers: 2)
        let response: (PaymentRequestPermissionV2, Data?) = (permission, nil)
        paymentRequestService.permissionResultV2 = .success(response)
        featureManager.override(with: [.paymentRequestV2: true])
        sut.tryPaymentRequest()
        XCTAssertEqual(presenterSpy.notAllowedAlertCallCount, 0)
        XCTAssertEqual(paymentRequestService.getPaymentRequestPermissionV2CallCount, 1)
        XCTAssertEqual(presenterSpy.paymentRequestSuccessCallCount, 1)
    }

    func testOpenDirectMessage_WhenUserProfileIsNil_ShouldNotOpenDirectMessage() {
        ConsumerManager.shared.consumer = MBConsumer()
        sut.userProfile = nil

        sut.openDirectMessage()

        XCTAssertEqual(presenterSpy.openDirectMessageCallCount, 0)
    }

    func testOpenDirectMessage_WhenUserConsumerIsNil_ShouldNotOpenDirectMessage() {
        ConsumerManager.shared.consumer = nil
        sut.userProfile = Seeds.profileRegular.contact

        sut.openDirectMessage()

        XCTAssertEqual(presenterSpy.openDirectMessageCallCount, 0)
    }

    func testOpenDirectMessage_WhenSenderAndRecipientAvailable_ShouldOpenDirectMessage() {
        ConsumerManager.shared.consumer = MBConsumer()
        sut.userProfile = Seeds.profileRegular.contact

        sut.openDirectMessage()

        XCTAssertEqual(presenterSpy.openDirectMessageCallCount, 1)
    }

    func testOpenDirectMessageSB_WhenRecipientIsAvailable_ShouldOpenDirectMessage() {
        sut.userProfile = Seeds.profileRegular.contact

        sut.openDirectMessageSB()

        XCTAssertEqual(presenterSpy.openDirectMessageSBCallCount, 1)
        XCTAssertEqual(sut.userProfile?.asChatUser.clientId, presenterSpy.userForDirectMessageSB?.clientId)
        let expectedEvent = DirectMessageSBAnalytics.button(.clicked, .openChatViaProfile).event()
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssert(analyticsSpy.equals(to: expectedEvent))
    }
    
    func testOpenDirectMessageSB_WhenRecipientIsNotAvailable_ShouldNotOpenDirectMessage() {
        sut.userProfile = nil
        
        sut.openDirectMessageSB()

        XCTAssertEqual(presenterSpy.openDirectMessageSBCallCount, 0)
    }

    func testCheckIfProfileCanOpenDirectMessageSB_WhenProfileIsPlaceholder_ShouldNotSetMessageButtonVisibility() {
        sut.isShowingPlaceholder = true

        sut.checkIfProfileCanOpenDirectMessageSB()

        XCTAssertEqual(presenterSpy.setMessageButtonVisibilityCallCount, 0)
    }

    func testCheckIfProfileCanOpenDirectMessageSB_WhenProfileIsNotAvailable_ShouldNotSetMessageButtonVisibility() {
        sut.isShowingPlaceholder = false
        sut.userProfile = nil

        sut.checkIfProfileCanOpenDirectMessageSB()

        XCTAssertEqual(presenterSpy.setMessageButtonVisibilityCallCount, 0)
    }

    func testCheckIfProfileCanOpenDirectMessageSB_WhenProfileCheckFailed_ShouldNotSetMessageButtonVisibility() {
        sut.isShowingPlaceholder = false
        sut.userProfile = Seeds.profileRegular.contact
        serviceMock.canReachExpectedResult = .failure(.nullUser)

        sut.checkIfProfileCanOpenDirectMessageSB()

        XCTAssertEqual(presenterSpy.setMessageButtonVisibilityCallCount, 0)
    }

    func testCheckIfProfileCanOpenDirectMessageSB_WhenProfileCheckSucceeded_ShouldSetMessageButtonVisibility() {
        sut.isShowingPlaceholder = false
        sut.userProfile = Seeds.profileRegular.contact
        serviceMock.canReachExpectedResult = .success(true)

        sut.checkIfProfileCanOpenDirectMessageSB()

        XCTAssertEqual(presenterSpy.setMessageButtonVisibilityCallCount, 1)
    }
}
