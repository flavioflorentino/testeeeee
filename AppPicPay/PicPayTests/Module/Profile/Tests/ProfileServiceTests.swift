import DirectMessageSB
import FeatureFlag
import XCTest
@testable import PicPay

final class ProfileServiceTests: XCTestCase {
    private let featureManagerMock = FeatureManagerMock()
    private let studentWorkerMock = StudentWorkerMock()
    private let mainQueue = DispatchQueue.main
    private lazy var chatApiManagerMock = ChatApiManagerMock()
    
    lazy var sut: ProfileService = {
        let mockDependencies = DependencyContainerMock(featureManagerMock, studentWorkerMock, mainQueue, chatApiManagerMock)
        return ProfileService(dependencies: mockDependencies)
    }()
    
    func testHasStudentComponentFeature_WhenFeatureFlagIsActive_ShouldReturnTrue() {
        featureManagerMock.override(key: .featureStudentAccountProfileComponentEnabled, with: true)
        
        XCTAssertTrue(sut.hasStudentComponentFeature)
    }
    
    func testHasStudentComponentFeature_WhenFeatureFlagIsInactive_ShouldReturnFalse() {
        featureManagerMock.override(key: .featureStudentAccountProfileComponentEnabled, with: false)
        
        XCTAssertFalse(sut.hasStudentComponentFeature)
    }
    
    func testStudentComponent_WhenFeatureComponentIsSet_ShouldReturnComponent() {
        let component = ProfileStudentAccountComponent(
            description: "Seja ContaUni também",
            descriptionColor: "#000000",
            descriptionDarkColor: "#FFFFFF",
            backgroundColor: "#F4F4F6",
            backgroundDarkColor: "#3D4451",
            buttonLink: "https://picpay.com/app_webviews/conta-universitaria/offer",
            buttonText: "SaibaMais",
            buttonColor: "#000000",
            buttonDarkColor: "#FFFFFF"
        )
        
        featureManagerMock.override(key: .featureStudentAccountProfileComponent, with: component)
        
        XCTAssertEqual(sut.studentComponent, component)
    }
    
    func testStudentComponent_WhenFeatureComponentIsntSet_ShouldReturnNil() {
        XCTAssertNil(sut.studentComponent)
    }
    
    func testStudentOfferWebview_WhenHasButtonLinkOnFeatureComponent_ShouldReturnStringURLFromComponent() {
        let component = ProfileStudentAccountComponent(
            description: "Seja ContaUni também",
            descriptionColor: "#000000",
            descriptionDarkColor: "#FFFFFF",
            backgroundColor: "#F4F4F6",
            backgroundDarkColor: "#3D4451",
            buttonLink: "https://picpay.test",
            buttonText: "SaibaMais",
            buttonColor: "#000000",
            buttonDarkColor: "#FFFFFF"
        )
        
        featureManagerMock.override(key: .featureStudentAccountProfileComponent, with: component)
        
        XCTAssertEqual(sut.studentOfferWebview, component.buttonLink)
    }
    
    func testStudentOfferWebview_WhenFeatureComponentIsntSet_ShouldReturnStringURLFromFeatureUniversityAccountRegisterWebview() {
        let stringUrl = "https://picpay/test/registerWebview"
        
        featureManagerMock.override(key: .universityAccountRegisterWebview, with: stringUrl)
        
        XCTAssertEqual(sut.studentOfferWebview, stringUrl)
    }
    
    func testLoadStudentStatus_WhenProfileIDFromStudent_ShouldReturnStatusActiveOnMainThread() {
        let consumerID = "0001"
        let expectedPayload = StatusPayload(status: .active, title: "Estudante ativo", message: "Bem vindo!", image: nil)
        studentWorkerMock.loadStatusResponse = .success(expectedPayload)
        
        let expectation = XCTestExpectation(description: "Test load student with studentWorkerMock")
        
        sut.loadStudentStatus(consumerId: consumerID) { testResult in
            XCTAssertTrue(Thread.isMainThread)
            switch testResult {
            case .success(let testPayload):
                XCTAssertEqual(testPayload, expectedPayload)
            case .failure(let error):
                XCTFail("Should return sucess message, but returned error \(error)")
            }
            
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 1.0)
    }

    func testCanReachUser_ShouldCompleteWithResult() {
        let reachExpectation = expectation(description: "The method canReach from ChatApiManeger should be called")

        sut.canReach(user: DirectMessageSB.Stubs.otherChatUser) { _ in
            XCTAssert(true)
            reachExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(chatApiManagerMock.didCanReachUserCount, 1)
    }
}
