import AnalyticsModule
import DirectMessage
import UI
import XCTest

@testable import PicPay

private final class ProfileCoordinatorSpy: ProfileCoordinating {
    private(set) var didCallPerforn = 0
    private(set) var paymentRequestCallCount = 0
    private(set) var didCallOpenStudentWebviewAction = 0
    private(set) var openDirectMessageCallCount = 0
    private(set) var openDirectMessageSBCallCount = 0

    private(set) var url: URL?
    
    var viewController: UIViewController?
    
    func perform(action: ProfileAction) {
        didCallPerforn += 1
        switch action {
        case .paymentRequest:
            paymentRequestCallCount += 1
        case let .openStudentWebview(url: urlAction):
            didCallOpenStudentWebviewAction += 1
            url = urlAction
        case .directMessage:
            openDirectMessageCallCount += 1
        case .directMessageSB:
            openDirectMessageSBCallCount += 1
        }
    }
}

final class ProfilePresenterTests: XCTestCase {
    private let viewControllerSpy = ConsumerProfileViewControllerSpy()
    private let coordinatorSpy = ProfileCoordinatorSpy()
    private let analyticsSpy = AnalyticsSpy()
    
    lazy var mockDependencies = DependencyContainerMock(analyticsSpy)
    
    lazy var sut: ProfilePresenter = {
        let sut = ProfilePresenter(coordinator: coordinatorSpy, dependencies: mockDependencies)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    // MARK: - presentStudentAccountView
    func testPresentStudentAccountView_WhenCalled_ShouldCallViewControllerShowStudentAccountView() {
        let component = ProfileStudentAccountComponent(
            description: "Seja ContaUni também",
            descriptionColor: "#000000",
            descriptionDarkColor: "#FFFFFF",
            backgroundColor: "#F4F4F6",
            backgroundDarkColor: "#3D4451",
            buttonLink: "https://picpay.com/app_webviews/conta-universitaria/offer",
            buttonText: "SaibaMais",
            buttonColor: "#000000",
            buttonDarkColor: "#FFFFFF"
        )
        sut.presentStudentAccountView(with: component)
        XCTAssertEqual(viewControllerSpy.didCallShowStudentAccountView, 1)
        XCTAssertEqual(viewControllerSpy.studentAccountViewComponent, component)
    }
    
     func testSetStudentWebview_WhenCalled_ShouldCallViewControllerSetStudentWebview() {
        do {
            let studentWebviewUrl = try XCTUnwrap(URL(string: "https://picpay.com/app_webviews/conta-universitaria/offer"))
        
            sut.presentStudentAccountOffer(url: studentWebviewUrl)
            
            XCTAssertEqual(coordinatorSpy.didCallPerforn, 1)
            XCTAssertEqual(coordinatorSpy.didCallOpenStudentWebviewAction, 1)
            XCTAssertEqual(coordinatorSpy.url, studentWebviewUrl)
            
            XCTAssertEqual(analyticsSpy.logCalledCount, 1)
            
            let expectedEvent = StudentAccountEvent.view(screen: .offerWebview, origin: .profile).event()
            XCTAssertTrue(analyticsSpy.equals(to: expectedEvent))
        } catch {
            XCTFail("Unable to unwrap studentWebviewUrl")
        }
     }
    
    // MARK: - presentPaymentRequestNotAllowedAlert
    func testPresentPaymentRequestNotAllowedAlert_ShouldCallDisplayAlert() {
        sut.presentPaymentRequestNotAllowedAlert(username: "User")
        XCTAssertEqual(viewControllerSpy.displayAlertCallCount, 1)
    }
    
    // MARK: - presentPaymentRequestLimitExceeded
    func testPresentPaymentRequestLimitExceeded_ShouldCallDisplayPaymentRequestLimitExceeded() {
        sut.presentPaymentRequestLimitExceeded()
        XCTAssertEqual(viewControllerSpy.displayPaymentRequestLimitExceededCallCount, 1)
    }
    
    // MARK: - presentPaymentRequestLimitExceededV2
    func testPresentPaymentRequestLimitExceededV2_ShouldCallDisplayPaymentRequestLimitExceeded() {
        sut.presentPaymentRequestLimitExceededV2(title: "", message: "", buttonTitle: "")
        XCTAssertEqual(viewControllerSpy.displayPaymentRequestLimitExceededCallCount, 1)
    }
    
    // MARK: - presentPaymentRequestSuccess
    func testPresentPaymentRequestSuccess_ShouldCallDisplayPaymentRequestSuccess() {
        let profile = PPContact(profileDictionary: ["id": 0])
        sut.presentPaymentRequestSuccess(profile: profile)
        XCTAssertEqual(viewControllerSpy.displayPaymentRequestSuccessCallCount, 1)
    }
    
    // MARK: - presentPaymentRequestError
    func testPresentPaymentRequestError_ShouldCallDisplayPaymentRequestError() {
        sut.presentPaymentRequestError()
        XCTAssertEqual(viewControllerSpy.displayPaymentRequestErrorCallCount, 1)
    }

    func testOpenDirectMessage_ShouldCallCoordinatorPerformAction() {
        sut.openDirectMessage(sender: DirectMessageStub.sender.contact,
                              recipient: DirectMessageStub.recipient.contact)
        XCTAssertEqual(coordinatorSpy.openDirectMessageCallCount, 1)
    }
    
    func testOpenDirectMessageSB_ShouldCallCoordinatorPerformAction() throws {
        let user = try XCTUnwrap(DirectMessageSBStub.regularConsumer.contact?.asChatUser)
        sut.openDirectMessageSB(with: user)
        XCTAssertEqual(coordinatorSpy.openDirectMessageSBCallCount, 1)
    }

    func testSetMessageButtonVisibility_ShouldCallDisplaySetMessageButtonVisibility() {
        sut.setMessageButtonVisibility(true)

        XCTAssertEqual(viewControllerSpy.setMessageButtonVisibilityCallCount, 1)
    }
}
