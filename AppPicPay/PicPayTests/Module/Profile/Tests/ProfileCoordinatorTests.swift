import DirectMessage
import DirectMessageSB
import XCTest
@testable import PicPay

final class ProfileCoordinatorTests: XCTestCase {

    private lazy var spy = ConsumerProfileViewControllerSpy()
    private lazy var navController = NavigationControllerMock(rootViewController: spy)
    private lazy var sut: ProfileCoordinator = {
        let coordinator = ProfileCoordinator()
        coordinator.viewController = navController.viewControllers.first
        return coordinator
    }()

    func testPerformAction_WhenActionIsOpenDirectMessage_ShouldPushDMChatViewController() throws {
        let action: ProfileAction = .directMessage(sender: DirectMessageStub.sender.contact,
                                                   recipient: DirectMessageStub.recipient.contact)

        sut.perform(action: action)

        XCTAssertTrue(navController.isPushViewControllerCalled)
        let pushedViewController = try XCTUnwrap(navController.pushedViewController)
        XCTAssertTrue(pushedViewController is DMChatViewController)
    }
    
    func testPerformAction_WhenActionIsOpenDirectMessageSB_ShouldPushChatViewController() throws {
        let user = try XCTUnwrap(DirectMessageSBStub.regularConsumer.contact?.asChatUser)
        let action: ProfileAction = .directMessageSB(user: user)
        sut.perform(action: action)
        XCTAssertTrue(navController.isPushViewControllerCalled)
        let pushedViewController = try XCTUnwrap(navController.pushedViewController)
        XCTAssertTrue(pushedViewController is ChatSetupViewController)
    }
}

