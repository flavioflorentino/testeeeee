import XCTest
import SwiftyJSON
@testable import PicPay

class CreditPicPayBillPaymentViewModelTest: XCTestCase {
    var account: CPAccount!
    var titleMessageDelegate = ""
    var descriptionMessageDelegate = ""
    
    override func setUp() {
        let mockJSON = MockJSON().load(resource: "accountCompletedTrue")
        account = CPAccount(json: mockJSON)
        titleMessageDelegate = ""
        descriptionMessageDelegate = ""
        ConsumerManager.shared.consumer?.email = nil
    }

    override func tearDown() {
        account = nil
        titleMessageDelegate = ""
        descriptionMessageDelegate = ""
        ConsumerManager.shared.consumer?.email = nil
    }
    
    func testCopyBarCode() {
        let invoicePayment: CreditInvoicePayment = try! MockCodable().loadCodableObject(resource: "invoicePayment")
        let viewModel = CreditPicPayBillPaymentViewModel(with: invoicePayment, coordinatorDelegate: self)
        let expected = "21290001190110002290200200000842500000000000000"
        viewModel.copyBarCode()
        XCTAssertEqual(expected, UIPasteboard.general.string!)
    }
        
    func testSendToEmailUserEmail() {
        let consumer = MBConsumer()
        consumer.email = "lucas.romano@picpay.com"
        ConsumerManager.shared.consumer = consumer
        let invoicePayment: CreditInvoicePayment = try! MockCodable().loadCodableObject(resource: "invoicePayment")
        let viewModel = CreditPicPayBillPaymentViewModel(with: invoicePayment, coordinatorDelegate: self, creditApi: CreditApiMock())
        let title = "Boleto enviado"
        let description = String(format: "O boleto foi enviado para seu e-mail\n%@", "lucas.romano@picpay.com")
        viewModel.sendToEmail {}
        XCTAssertEqual(titleMessageDelegate, title)
        XCTAssertEqual(descriptionMessageDelegate, description)
    }
    
    func testSendToEmailNoUserEmail() {
        let invoicePayment: CreditInvoicePayment = try! MockCodable().loadCodableObject(resource: "invoicePayment")
        let viewModel = CreditPicPayBillPaymentViewModel(with: invoicePayment, coordinatorDelegate: self, creditApi: CreditApiMock())
        let title = "Boleto enviado"
        let description = String(format: "O boleto foi enviado para seu e-mail\n%@", "")
        viewModel.sendToEmail {}
        XCTAssertEqual(titleMessageDelegate, title)
        XCTAssertEqual(descriptionMessageDelegate, description)
    }
    
    func testHasMinimumPaymentForValue() {
        let invoicePayment: CreditInvoicePayment = try! MockCodable().loadCodableObject(resource: "invoicePaymentNoInstallment")
        let viewModel = CreditPicPayBillPaymentViewModel(with: invoicePayment, coordinatorDelegate: self)
        XCTAssertTrue(viewModel.hasMinimumPayment)
    }
    
    func testHasMinimumPaymentForNilValue() {
        let invoicePayment: CreditInvoicePayment = try! MockCodable().loadCodableObject(resource: "invoicePaymentMinimumNil")
        let viewModel = CreditPicPayBillPaymentViewModel(with: invoicePayment, coordinatorDelegate: self)
        XCTAssertFalse(viewModel.hasMinimumPayment)
    }
    
    func testHasMinimumPaymentForZeroValue() {
        let invoicePayment: CreditInvoicePayment = try! MockCodable().loadCodableObject(resource: "invoicePaymentMinimumZero")
        let viewModel = CreditPicPayBillPaymentViewModel(with: invoicePayment, coordinatorDelegate: self)
        XCTAssertFalse(viewModel.hasMinimumPayment)
    }
}

extension CreditPicPayBillPaymentViewModelTest: CreditPicPayBillPaymentCoordinatorDelegate {
    func showAlertError(_ error: Error, completion: (() -> Void)?) {
        descriptionMessageDelegate = error.localizedDescription
    }
    
    func showAlert(_ alert: Alert, completion: ((AlertPopupViewController, Button, UIPPButton) -> Void)?) {
        titleMessageDelegate = alert.title!.string
        descriptionMessageDelegate = alert.text!.string
    }
}
