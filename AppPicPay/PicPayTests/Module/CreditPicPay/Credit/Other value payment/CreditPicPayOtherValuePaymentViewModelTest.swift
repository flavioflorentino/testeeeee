import XCTest
@testable import PicPay

class CreditPicPayOtherValuePaymentViewModelTest: XCTestCase {
    func instantiateViewModel(status: Status = .normal) -> CreditPicPayOtherValuePaymentViewModel {
        let invoicePayment: CreditInvoicePayment = try! MockCodable().loadCodableObject(resource: status.rawValue)
        let viewModel = CreditPicPayOtherValuePaymentViewModel(with: invoicePayment, coordinatorDelegate: nil)
        return viewModel
    }
    
    func testTotalInvoice() {
        let viewModel = instantiateViewModel()
        let result = viewModel.totalInvoice
        XCTAssertEqual(result, 9035.30.stringAmount)
    }
    
    func testReceivedPayment() {
        let viewModel = instantiateViewModel()
        let result = viewModel.receivedPayment
        XCTAssertEqual(result, 600.stringAmount)
    }
    
    func testReceivedPaymentNil() {
        let viewModel = instantiateViewModel(status: .null)
        let result = viewModel.receivedPayment
        XCTAssertNil(result)
    }
    
    func testValueText() {
        let viewModel = instantiateViewModel()
        let result = viewModel.valueText
        XCTAssertEqual(result, 0.stringAmount)
    }
    
    func testHasReceivedPayment() {
        let viewModel = instantiateViewModel()
        let result = viewModel.hasMinimumPayment
        XCTAssertTrue(result)
    }
    
    func testHasReceivedPaymentNil() {
        let viewModel = instantiateViewModel(status: .null)
        let result = viewModel.hasMinimumPayment
        XCTAssertFalse(result)
    }
    
    func testMinimumPayment() {
        let viewModel = instantiateViewModel()
        let result = viewModel.minimumPayment
        XCTAssertEqual(result, 278.16.stringAmount)
    }
    
    func testMinimumPaymentNil() {
        let viewModel = instantiateViewModel(status: .null)
        let result = viewModel.minimumPayment
        XCTAssertNil(result)
    }
    
    func testHasMinimumPayment() {
        let viewModel = instantiateViewModel()
        let result = viewModel.hasMinimumPayment
        XCTAssertTrue(result)
    }
    
    func testHasMinimumPaymentForNil() {
        let viewModel = instantiateViewModel(status: .null)
        let result = viewModel.hasMinimumPayment
        XCTAssertFalse(result)
    }
    
    func testHasMinimumPaymentForZero() {
        let viewModel = instantiateViewModel(status: .zero)
        let result = viewModel.hasMinimumPayment
        XCTAssertFalse(result)
    }
}

extension CreditPicPayOtherValuePaymentViewModelTest {
    enum Status: String {
        case normal = "otherValuePayment"
        case null = "otherValuePaymentNil"
        case zero = "otherValuePaymentZero"
    }
}
