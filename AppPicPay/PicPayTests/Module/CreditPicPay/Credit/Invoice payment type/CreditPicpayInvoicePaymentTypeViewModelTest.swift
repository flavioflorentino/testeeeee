import UI
import XCTest
import SwiftyJSON
@testable import PicPay


class CreditPicpayInvoicePaymentTypeViewModelTest: XCTestCase {
    var account: CPAccount!
    var invoicePayment: CreditInvoicePayment!
    var messageDelegate = ""
    
    override func setUp() {
        let mockJSON = MockJSON().load(resource: "accountCompletedTrue")
        account = CPAccount(json: mockJSON)
        invoicePayment = try! MockCodable().loadCodableObject(resource: "invoicePayment")
        messageDelegate = ""
    }

    override func tearDown() {
        account = nil
        messageDelegate = ""
    }
    
    func testHasIntallmentsOptionsTrueWithInstallment() {
        let viewModel = CreditPicpayInvoicePaymentTypeViewModel(with: invoicePayment, coordinatorDelegate: self)
        XCTAssertTrue(viewModel.hasIntallmentsOptions)
    }
    
    func testHasIntallmentsOptionsTrueNoInstallment() {
        let invoicePayment: CreditInvoicePayment = try! MockCodable().loadCodableObject(resource: "invoicePaymentNoInstallment")
        let viewModel = CreditPicpayInvoicePaymentTypeViewModel(with: invoicePayment, coordinatorDelegate: self)
        XCTAssertFalse(viewModel.hasIntallmentsOptions)
    }
    
    func testHasIntallmentsOptionsFalse() {
        let viewModel = CreditPicpayInvoicePaymentTypeViewModel(with: invoicePayment, coordinatorDelegate: self)
        XCTAssertFalse(!viewModel.hasIntallmentsOptions)
    }
    
    func testRextInstallmentCard() {
        let viewModel = CreditPicpayInvoicePaymentTypeViewModel(with: invoicePayment, coordinatorDelegate: self)
        let expectedText = String(format: "Até %dX de %@", 10, 183.0.stringAmount)
        let result = viewModel.textInstallmentCard!
        XCTAssertEqual(expectedText, result)
    }
    
    func testRextInstallmentCardNil() {
        let invoicePayment: CreditInvoicePayment = try! MockCodable().loadCodableObject(resource: "invoicePaymentNoInstallment")
        let viewModel = CreditPicpayInvoicePaymentTypeViewModel(with: invoicePayment, coordinatorDelegate: self)
        XCTAssertNil(viewModel.textInstallmentCard)
    }
    
    func testRextInstallmentCardNilNoOptionInArray() {
        let invoicePayment: CreditInvoicePayment = try! MockCodable().loadCodableObject(resource: "invoicePaymentNoInstallmentInArray")
        let viewModel = CreditPicpayInvoicePaymentTypeViewModel(with: invoicePayment, coordinatorDelegate: self)
        XCTAssertNil(viewModel.textInstallmentCard)
    }
    
    func testBalanceText() {
        let consumer = MBConsumer()
        consumer.balance = NSDecimalNumber(value: 10000.0)
        ConsumerManager.shared.consumer = consumer
        let viewModel = CreditPicpayInvoicePaymentTypeViewModel(with: invoicePayment, coordinatorDelegate: self)
        
        let balance = 10000.0.stringAmount
        let text = String(format: "Saldo na carteira: %@", balance)
        let range = NSRange(location: 19, length: balance.count)
        let expected = NSMutableAttributedString(string: text)
        expected.addAttributes([.font: UIFont.systemFont(ofSize: 12, weight: .bold)], range: range)
        
        XCTAssertEqual(expected, viewModel.balanceText!)
    }
    
    func testDueDateText() {
        let viewModel = CreditPicpayInvoicePaymentTypeViewModel(with: invoicePayment, coordinatorDelegate: self)
        let text = String(format: "Vencimento %@", "10 de Janeiro")
        let location = 10
        let range = NSRange(location: location, length: text.count - location)
        let expected = NSMutableAttributedString(string: text)
        let color = Palette.ppColorBranding300.color
        expected.addAttributes([NSAttributedString.Key.foregroundColor: color], range: range)
        
        XCTAssertEqual(expected.string, viewModel.dueDateText!.string)
        viewModel.dueDateText!.enumerateAttribute(NSAttributedString.Key.foregroundColor, in: NSRange(0..<expected.length)) { value, range, stop in
            if let foregroundColor = value as? UIColor {
                XCTAssertEqual(color.cgColor, foregroundColor.cgColor)
            }
        }
    }
    
    func testPayTotalDelegate() {
        let viewModel = CreditPicpayInvoicePaymentTypeViewModel(with: invoicePayment, coordinatorDelegate: self)
        viewModel.tapInTotalValue()
        let expected = "test pay"
        XCTAssertEqual(expected, messageDelegate)
    }
    
    func testPayInstallmentDelegate() {
        let viewModel = CreditPicpayInvoicePaymentTypeViewModel(with: invoicePayment, coordinatorDelegate: self)
        viewModel.tapInInvoiceInstall()
        let expected = "test pay installment"
        XCTAssertEqual(expected, messageDelegate)
    }
    
    func testPayOtherValueDelegate() {
        let viewModel = CreditPicpayInvoicePaymentTypeViewModel(with: invoicePayment, coordinatorDelegate: self)
        viewModel.tapInOtherValue()
        let expected = "test pay other value"
        XCTAssertEqual(expected, messageDelegate)
    }
    
    func testTotalValueText() {
        let viewModel = CreditPicpayInvoicePaymentTypeViewModel(with: invoicePayment, coordinatorDelegate: self)
        let expected = 9035.0.stringAmount
        XCTAssertEqual(expected, viewModel.totalValueText)
    }
}

extension CreditPicpayInvoicePaymentTypeViewModelTest: CreditPicPayInvoicePaymentTypeCoordinatorDelegate {
    func pay() {
        messageDelegate = "test pay"
    }
    
    func payWithInstallment() {
        messageDelegate = "test pay installment"
    }
    
    func payWithAnotherValue() {
        messageDelegate = "test pay other value"
    }
}
