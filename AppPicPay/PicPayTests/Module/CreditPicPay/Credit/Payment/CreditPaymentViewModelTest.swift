import XCTest
import OHHTTPStubs
@testable import PicPay

class CreditPaymentViewModelTest: XCTestCase {
    var viewModel: CreditPaymentViewModel!
    
    override func setUp() {
        let invoicePayment: CreditInvoicePayment = try! MockCodable().loadCodableObject(resource: "invoicePayment")
        viewModel = CreditPaymentViewModel(invoice: invoicePayment, servicePayment: CreditPaymentService(), dependencies: DependencyContainerMock())
        viewModel.delegate = self
    }
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testShowAlertInterest() {
        let expectation = XCTestExpectation(description: "expected to showAlertTax")
        viewModel.showAlertTax = { _ in expectation.fulfill() }
        viewModel.tapInfoInterest()
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testShowAlertInterestOnPayment() {
        let expectation = XCTestExpectation(description: "expected to show showAlertTaxOnPayment")
        viewModel.showAlertTaxOnPayment = { _ in expectation.fulfill() }
        viewModel.checkout()
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testAskForAuthentication() {
        let expectation = XCTestExpectation(description: "expected to askForAuthentication")
        viewModel.askForAuthentication = { expectation.fulfill() }
        viewModel.skipAlertTax = true
        viewModel.checkout()
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testUpdateMontlyPayments() {
        let payments: Int = 10
        viewModel.update(montlyPayments: payments, manager: PPPaymentManager())
        XCTAssertEqual(viewModel.montlyPayments, payments)
    }
    
    // MARK: Loading Disclaimer Tests
    private enum Disclaimer: String {
        case success = "payment-msg.json"
        case fail = "payment-msg-fail.json"
        case wrongType = "payment-msg-wrongType.json"
        
        var obj: [String: Any] {
            switch self {
            case .success:
                return ["text": "disclaimerMessage", "payment_disabled": false, "convenience_tax": 2.99]
            case .wrongType:
                return ["text": true, "payment_disabled": false, "convenience_tax": 2.99]
            case .fail:
                return [:]
            }
        }
    }
    
    func testLoadDisclaimerWithSuccess() {
        let expectation = XCTestExpectation(description: "expected to load Disclaimer")
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(with: Disclaimer.success.obj)
        }
        
        viewModel.onDisclaimerLoad = { _ in expectation.fulfill() }
        viewModel.loadDisclaimer()
        wait(for: [expectation], timeout: 5.0)
        
    }
    
    func testLoadDisclaimerWrongType() {
        let expectation = XCTestExpectation(description: "expected to fail Disclaimer")
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(with: Disclaimer.wrongType.obj)
        }
        
        viewModel.onErrorDisclaimer = { _ in expectation.fulfill() }
        viewModel.loadDisclaimer()
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testLoadDisclaimerFailApi() {
        let expectation = XCTestExpectation(description: "expected to fail Disclaimer")
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.fail()
        }
        
        viewModel.onErrorDisclaimer = { _ in expectation.fulfill() }
        viewModel.loadDisclaimer()
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testHasShownLoadingOnDisclaimer() {
        let loadingHasShown = XCTestExpectation(description: "expected loading to appear")
        let loadingHidden = XCTestExpectation(description: "expected loading to disappear")
        
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(with: Disclaimer.success.obj)
        }
        
        viewModel.isLoading = { isLoading in
            guard isLoading else {
                loadingHidden.fulfill()
                return
            }
            
            loadingHasShown.fulfill()
        }
        
        viewModel.loadDisclaimer()
        wait(for: [loadingHasShown, loadingHidden], timeout: 5.0, enforceOrder: true)
    }
    
    // MARK: Payment Tests
    private enum Pay: String {
        case success = "payment_success.json"
    }
    
    func testHasShownLoadingOnPay() {
        let loadingHasShown = XCTestExpectation(description: "expected loading to appear")
        let loadingHidden = XCTestExpectation(description: "expected loading to disappear")
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(at: Pay.success.rawValue)
        }
        
        viewModel.isLoadingPayment = { isLoading in
            guard isLoading else {
                loadingHidden.fulfill()
                return
            }
            loadingHasShown.fulfill()
        }
        
        viewModel.pay("", isBiometry: false, credit: "", total: "")
        
        wait(for: [loadingHasShown, loadingHidden], timeout: 5.0, enforceOrder: true)
    }
    
    func testPaymentFailure() {
        let expectation = XCTestExpectation(description: "expected to fail on payment")
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.fail()
        }
        
        viewModel.onErrorPayment = { _ in
            expectation.fulfill()
        }
        
        viewModel.pay("", isBiometry: false, credit: "", total: "")
        wait(for: [expectation], timeout: 5.0)
    }
    
    var paymentExpectationSuccess = XCTestExpectation(description: "expected paymentWithSuccess")
    func testPaymentSuccess() {
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(at: Pay.success.rawValue)
        }
        
        viewModel.pay("", isBiometry: false, credit: "", total: "")
        wait(for: [paymentExpectationSuccess], timeout: 5.0)
    }
    
    var goBackExpectation = XCTestExpectation(description: "expected goBack to call popViewController")
    func testGoBack() {
        viewModel.goBack()
        wait(for: [goBackExpectation], timeout: 5.0)
    }
}

extension CreditPaymentViewModelTest: CreditPicPayPaymentCoordinatorDelegate {
    func openCvv(completedCvv: @escaping (String) -> Void, completedWithoutCvv: @escaping () -> Void) {
    }
    
    func popViewController() {
        goBackExpectation.fulfill()
    }
    
    func show(receipt: ReceiptWidgetViewModel) {
        paymentExpectationSuccess.fulfill()
    }
    
    func showInstallments(manager: PPPaymentManager) { }
}
