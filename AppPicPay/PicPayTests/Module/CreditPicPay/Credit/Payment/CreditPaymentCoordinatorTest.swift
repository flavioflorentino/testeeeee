import XCTest
import OHHTTPStubs
@testable import PicPay

class CreditPaymentCoordinatorTest: XCTestCase {
    private lazy var navigationController = UINavigationController()
    private var invoicePayment: CreditInvoicePayment {
        let invoicePayment: CreditInvoicePayment = try! MockCodable().loadCodableObject(resource: "invoicePayment")
        return invoicePayment
    }
    private let featureMock = FeatureManagerMock()
    private lazy var creditPaymentCoordinator: CreditPicPayPaymentCoordinator = {
        let dependencieContainer = DependencyContainerMock(featureMock)
        return CreditPicPayPaymentCoordinator(with: navigationController, invoice: invoicePayment, dependencies: dependencieContainer)
    }()

    func testPayWithTotal() {
        creditPaymentCoordinator.start()
        let viewModel = creditPaymentCoordinator.viewModel
        
        XCTAssertEqual(viewModel?.paymentValue, Decimal(floatLiteral: 9035))
        XCTAssertEqual(viewModel?.paymentType, CreditPaymentViewModel.PaymentMethod.total.description)
    }
    
    func testPayWithInstallment() {
        guard let installmentOption = invoicePayment.installment?.installmentOptions.first else {
            return
        }
        creditPaymentCoordinator.start(with: installmentOption)
        let viewModel = creditPaymentCoordinator.viewModel
        
        XCTAssertEqual(viewModel?.paymentValue, Decimal(floatLiteral: 844.25))
        XCTAssertEqual(viewModel?.paymentType, CreditPaymentViewModel.PaymentMethod.installments.description)
    }
    
    func testPayOtherValue() {
        creditPaymentCoordinator.startOtherValue(100.01)
        let viewModel = creditPaymentCoordinator.viewModel
        
        XCTAssertEqual(viewModel?.paymentValue, Decimal(floatLiteral: 100.01))
        XCTAssertEqual(viewModel?.paymentType, CreditPaymentViewModel.PaymentMethod.other.description)
    }
    
    func testLastViewController_WhenFetuatureFlagNewInvoice_IsDisable() {
        featureMock.override(key: .newInvoiceCard, with: false)
        creditPaymentCoordinator.startOtherValue(100.01)
        XCTAssertTrue(navigationController.viewControllers.last is CreditPaymentViewController)
    }
    
    func testLastViewController_WhenFetuatureFlagNewInvoice_IsEnable() {
        featureMock.override(key: .newInvoiceCard, with: true)
        creditPaymentCoordinator.startOtherValue(100.01)
        XCTAssertTrue(navigationController.viewControllers.last is CreditPaymentLoadingViewController)
    }
}
