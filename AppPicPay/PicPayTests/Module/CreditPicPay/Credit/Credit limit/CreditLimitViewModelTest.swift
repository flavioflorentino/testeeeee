import UI
import XCTest
import OHHTTPStubs
@testable import PicPay

class CreditLimitViewModelTest: XCTestCase {
    private var viewModel: CreditLimitViewModel!
    var isClosed = false
    var isLimitUpdated = false
    var alertPresented = false
    var alertTitle = ""
    
    override func setUp() {
        super.setUp()
        viewModel = CreditLimitViewModel()
        viewModel?.coordinatorDelegate = self
    }
    
    override func tearDown() {
        super.tearDown()
        isClosed = false
        alertPresented = false
        alertTitle = ""
    }
    
    func testhavaliableLimitPositive() {
        let expectation = XCTestExpectation(description: "expected success")
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(at: Payload.creditLimitPositive.json)
        }
        let text = NSMutableAttributedString(
            string: "R$ 179,56 Disponível",
            attributes: [.foregroundColor: Palette.ppColorBranding300.color, .font: UIFont.systemFont(ofSize: 13)]
        )
        text.addAttributes([.font: UIFont.boldSystemFont(ofSize: 13)], range: NSRange(location: 0, length: 9))
        let expectedSecondaryColor = Palette.ppColorBranding300.color.withAlphaComponent(0.5)
        let expectedMinValueFormatted = "R$ 200,00"
        let expectedMaxValueFormatted = "R$ 3.000,00"
        viewModel?.onSuccess = {
            XCTAssertEqual(text.string, self.viewModel.avaliableLimit().string)
            XCTAssertEqual(20.44, self.viewModel.secondaryValue)
            XCTAssertEqual(expectedSecondaryColor.cgColor, self.viewModel.secondaryColor.cgColor)
            XCTAssertEqual(expectedMinValueFormatted, self.viewModel.minValueFormatted)
            XCTAssertEqual(200, self.viewModel.minValue)
            XCTAssertEqual(expectedMaxValueFormatted, self.viewModel.maxValueFormatted)
            XCTAssertEqual(3000, self.viewModel.maxValue)
            XCTAssertEqual(200, self.viewModel.currentValue)
            expectation.fulfill()
        }
        viewModel?.getLimit()
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testDelegateCoordinator() {
        XCTAssertNotNil(viewModel.coordinatorDelegate)
    }
    
    func testCalculateCurrentValue_WhenIncrementIs10_ShouldReturnNextMultiple() {
        viewModel.creditLimit = CreditLimit(maxValue: 3000.0, minValue: 200, increment: 10, currentValue: 0.0, balance: 0.0)
        XCTAssertEqual(viewModel.calculateCurrentValue(value: 201.0), 210.0)
        XCTAssertEqual(viewModel.calculateCurrentValue(value: 210.0), 210.0)
        XCTAssertEqual(viewModel.calculateCurrentValue(value: 2999.0), 3000.0)
    }
    
    func testCalculateCurrentValue_WhenIncrementIs2_ShouldReturnNextMultiple() {
        viewModel.creditLimit = CreditLimit(maxValue: 3000.0, minValue: 200, increment: 2, currentValue: 0.0, balance: 0.0)
        XCTAssertEqual(viewModel.calculateCurrentValue(value: 201.0), 202.0)
        XCTAssertEqual(viewModel.calculateCurrentValue(value: 2999.0), 3000.0)
    }
    
    func testCalculateCurrentValue_WhenValueIsMinimum_ShouldReturnMinimumValue() {
        let minValue = 200.0
        viewModel.creditLimit = CreditLimit(maxValue: 3000.0, minValue: minValue, increment: 10, currentValue: 0.0, balance: 0.0)
        XCTAssertEqual(viewModel.calculateCurrentValue(value: Float(minValue)), minValue)
    }
    
    func testCalculateCurrentValue_WhenValueIsMaximum_ShouldReturnMaximumValue() {
        let maxValue = 3000.0
        viewModel.creditLimit = CreditLimit(maxValue: maxValue, minValue: 200, increment: 10, currentValue: 0.0, balance: 0.0)
        XCTAssertEqual(viewModel.calculateCurrentValue(value: Float(maxValue)), maxValue)
    }
    
    func testAvaliableLimitNegative() {
        let expectation = XCTestExpectation(description: "expected success")
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(at: Payload.creditLimitNegative.json)
        }
        let expectedSecondaryColor = Palette.ppColorBranding300.color
        let text = NSMutableAttributedString(
            string: "-R$ 10,44 Disponível",
            attributes: [.foregroundColor: Palette.ppColorBranding300.color, .font: UIFont.systemFont(ofSize: 13)]
        )
        text.addAttributes([.font: UIFont.boldSystemFont(ofSize: 13)], range: NSRange(location: 0, length: 9))
        viewModel?.onSuccess = {
            XCTAssertEqual(text.string, self.viewModel.avaliableLimit().string)
            XCTAssertEqual(expectedSecondaryColor.cgColor, self.viewModel.secondaryColor.cgColor)
            expectation.fulfill()
        }
        viewModel?.getLimit()
        wait(for: [expectation], timeout: 5.0)
        
        viewModel?.valueOnChangeSlider(300)
        XCTAssertEqual(300, viewModel.currentValue)
    }
    
    func testCreditLimitApiError() throws {
        let expectation = XCTestExpectation(description: "expected fail")
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.fail()
        }
        let expectedColor = Palette.ppColorBranding300.color
        viewModel?.onError = {
            XCTAssertTrue(self.viewModel.avaliableLimit().string.isEmpty)
            XCTAssertEqual(0.0, self.viewModel.secondaryValue)
            XCTAssertEqual(expectedColor.cgColor, self.viewModel.secondaryColor.cgColor)
            XCTAssertEqual("???", self.viewModel.minValueFormatted)
            XCTAssertEqual(0.0, self.viewModel.minValue)
            XCTAssertEqual("???", self.viewModel.maxValueFormatted)
            XCTAssertEqual(0.0, self.viewModel.maxValue)
            XCTAssertEqual(0.0, self.viewModel.currentValue)
            expectation.fulfill()
        }
        viewModel?.getLimit()
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testSaveSuccess() {
        let expectation = XCTestExpectation(description: "expected success")
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(at: Payload.creditLimitSave.json)
        }
        viewModel?.onSuccess = {
            expectation.fulfill()
        }
        viewModel?.save()
        wait(for: [expectation], timeout: 5.0)
        XCTAssertTrue(self.alertPresented)
        XCTAssertEqual("Limite ajustado!", self.alertTitle)
        XCTAssertTrue(isLimitUpdated)
    }
    
    func testSaveError() {
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.fail()
        }
        viewModel?.save()
        
        let expectation = self.expectation(waitForCondition: self.alertPresented) {
            XCTAssertEqual("Erro na requisição", self.alertTitle)
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testCloseView() {
        viewModel?.closeView()
        
        let expectation = self.expectation(waitForCondition: self.isClosed)
        wait(for: [expectation], timeout: 5.0)
    }
}

extension CreditLimitViewModelTest: CreditLimitCoordinatorDelegate {
    func close() {
        isClosed = true
    }
    
    func didUpdateLimit() {
        isLimitUpdated = true
    }
    
    func presentAlert(_ alert: Alert, completion: (() -> Void)? = nil) {
        alertPresented = true
        alertTitle = alert.title?.string ?? "false"
    }
}

extension CreditLimitViewModelTest {
    enum Payload: String {
        case creditLimitPositive
        case creditLimitNegative
        case creditLimitSave
        
        var json: String {
            return "\(self.rawValue).json"
        }
    }
}
