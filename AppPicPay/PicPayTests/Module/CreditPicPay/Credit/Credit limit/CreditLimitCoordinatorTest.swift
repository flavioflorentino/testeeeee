import XCTest
@testable import PicPay

class CreditLimitCoordinatorTest: XCTestCase {

    var startViewController: UINavigationController?
    var coordinator: CreditLimitCoordinator?
    var isClosed = false
    var alertTitle = ""
    var alertText = ""
    
    override func setUp() {
        super.setUp()
        isClosed = false
        alertTitle = ""
        alertText = ""
        startViewController = UINavigationController()
        coordinator = CreditLimitCoordinator(from: UINavigationController())
    }

    override func tearDown() {
        super.tearDown()
        coordinator = nil
        alertTitle = ""
        isClosed = false
        alertText = ""
    }
    
    func testStart() {
        coordinator?.start()
        coordinator?.currentCoordinator = coordinator
        XCTAssertNotNil(coordinator?.currentCoordinator)
    }
    
    func testClose() {
        coordinator?.close()
        XCTAssertNil(coordinator?.currentCoordinator)
    }
}
