import XCTest
@testable import PicPay

class CreditAboutViewModelTest: XCTestCase {
    private var viewModel: CreditAboutViewModel!
    private var viewModelOutput: CreditAboutViewModelOutputMock!
    private let service = CreditAboutServiceMock()

    override func setUp() {
        super.setUp()
        viewModel = CreditAboutViewModel(with: service)
        viewModelOutput = CreditAboutViewModelOutputMock()
        viewModel.outputs = viewModelOutput
    }
    
    func testRequest() {
        let expectation = XCTestExpectation(description: "expected request")

        viewModelOutput.didCalledLoadRequest = { (request: URLRequest) in
            XCTAssertEqual("https://mock.picpay.com/picpaypos-webview.html", request.url?.absoluteString)
            expectation.fulfill()
        }
        viewModel.inputs.viewDidLoad()
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testButton() {
        let expectation = XCTestExpectation(description: "expected button")

        viewModelOutput.didCalledSetupButton = { (button: Button) in
            XCTAssertEqual("cta", button.type.rawValue)
            XCTAssertEqual("Mock Button", button.title)
            expectation.fulfill()
        }
        viewModel.inputs.viewDidLoad()
        wait(for: [expectation], timeout: 5.0)
    }
}

extension CreditAboutViewModelTest {
     class CreditAboutViewModelOutputMock: CreditAboutViewModelOutputs {
        var didCalledSetupButton: ((Button) -> Void)?
        var didCalledLoadRequest: ((URLRequest) -> Void)?

        func setup(button: Button) { didCalledSetupButton?(button) }
        func load(request: URLRequest) { didCalledLoadRequest?(request) }
    }
    
    struct CreditAboutServiceMock: CreditAboutServiceProtocol {
        var featureConfigJson: [String : Any]? {
            return ["url": "https://mock.picpay.com/picpaypos-webview.html",
                    "button": ["title": "Mock Button", "type": "cta"]]
        }
    }
}
