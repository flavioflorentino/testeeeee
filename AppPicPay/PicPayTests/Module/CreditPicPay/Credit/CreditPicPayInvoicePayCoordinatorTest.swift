import XCTest
import SwiftyJSON
@testable import PicPay

class CreditPicPayInvoicePayCoordinatorTest: XCTestCase {
    var navigationController: UINavigationController!
    var account: CPAccount!
    var invoicePayment: CreditInvoicePayment {
        let invoicePayment: CreditInvoicePayment = try! MockCodable().loadCodableObject(resource: "invoicePayment")
        return invoicePayment
    }
    
    override func setUp() {
        super.setUp()
        navigationController = UINavigationController()
        let mockJSON = MockJSON().load(resource: "accountCompletedTrue")
        account = CPAccount(json: mockJSON)
    }
    
    override func tearDown() {
        super.tearDown()
        navigationController = nil
    }
    
    func testStart() {
        let coordinator = CreditPicPayInvoicePayCoordinator(with: navigationController, invoiceId: "21231", account: account)
        coordinator.start()
        XCTAssert(coordinator.currentCoordinator is CreditPicPayInvoicePayCoordinator)
    }
    
    func testStartPaymentSelectionType() {
        let coordinator = CreditPicPayInvoicePayCoordinator(with: navigationController, invoiceId: "21231", account: account)
        coordinator.startPaymentSelectionType(invoicePayment)
        XCTAssert(coordinator.currentCoordinator is CreditPicPayInvoicePaymentTypeCoordinator)
    }
    
    func testPayWithInstallment() {
        let coordinator = CreditPicPayInvoicePayCoordinator(with: navigationController, invoiceId: "21231", account: account)
        coordinator.startPaymentSelectionType(invoicePayment)
        coordinator.payWithInstallment()
        XCTAssert(coordinator.currentCoordinator is CreditPicPayInstallmentCoordinator)
    }
    
    func testPayWithAnotherValue() {
        let coordinator = CreditPicPayInvoicePayCoordinator(with: navigationController, invoiceId: "21231", account: account)
        coordinator.startPaymentSelectionType(invoicePayment)
        coordinator.payWithAnotherValue()
        XCTAssert(coordinator.currentCoordinator is CreditPicPayOtherValuePaymentCoordinator)
    }
    
    func testPayWithBill() {
        let coordinator = CreditPicPayInvoicePayCoordinator(with: navigationController, invoiceId: "21231", account: account)
        coordinator.startPaymentSelectionType(invoicePayment)
        coordinator.payWithBill(invoicePayment)
        XCTAssert(coordinator.currentCoordinator is CreditPicPayBillPaymentCoordinator)
    }
}
