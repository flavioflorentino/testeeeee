import SwiftyJSON
@testable import PicPay

class CreditApiMock {
    private var invoiceDetailMock: JSON?
    private var accountMock: JSON?
    lazy var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .customISO8601
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()
    var isSendInvoiceEmailError: Bool = false
    var invoiceStatus: Invoice? {
        didSet {
            guard let invoiceStatus = self.invoiceStatus else {
            return
        }
            let mockJson = MockJSON()
            switch invoiceStatus {
            case .closed:
                invoiceDetailMock = mockJson.load(resource: "invoiceDetailClosed")
            case .delayed:
                invoiceDetailMock = mockJson.load(resource: "invoiceDetailDelayed")
            case .open:
                invoiceDetailMock = mockJson.load(resource: "invoiceDetailOpen")
            case .openPayment:
                invoiceDetailMock = mockJson.load(resource: "invoiceDetailOpenPayment")
            case .error:
                invoiceDetailMock = mockJson.load(resource: "invoiceDetailError")
            case .negativeTotalValue:
                invoiceDetailMock = mockJson.load(resource: "invoiceDetailtOpenNegativeTotalValue")
            case .notExistMinimumPayment:
                invoiceDetailMock = mockJson.load(resource: "invoiceDetailOpenNotExistMinimumPayment")
            case .delayedAndNotPayable:
                invoiceDetailMock = mockJson.load(resource: "invoiceDetailDelayedNotPayable")
            case .openEmptyTransactions:
                invoiceDetailMock = mockJson.load(resource: "invoiceDetailOpenEmptyTransactions")
            case .openPaymentWithProcessedAndUnprocessedTransactions:
                invoiceDetailMock = mockJson.load(resource: "invoiceDetailProcessedAndUnprocessed")
            case .openPaymentWithUnprocessedTransactions:
                invoiceDetailMock = mockJson.load(resource: "invoiceDetailUnprocessed")
            case .invoiceIsNil:
                invoiceDetailMock = mockJson.load(resource: "invoiceDetailOpenAndInvoiceIsNil")
            }
        }
    }
    var accountType: Account? {
        didSet {
            guard let accountType = self.accountType else {
            return
        }
            let mockJson = MockJSON()
            switch accountType {
            case .completedTrue:
                accountMock = mockJson.load(resource: "accountCompletedTrue")
            case .completedFalse:
                accountMock = mockJson.load(resource: "accountCompletedFalse")
            case .completedTrueOnRegistration:
                accountMock = mockJson.load(resource: "accountCompletedTrueOnRegistration")
            case .completedFalseOnRegistration:
                accountMock = mockJson.load(resource: "accountCompletedFalseOnRegistration")
            case .completedTrueDebitOnRegistration:
                accountMock = mockJson.load(resource: "accountCompletedTrueDebitOnRegistration")
            case .completedFalseDebitOnRegistration:
                accountMock = mockJson.load(resource: "accountCompletedFalseDebitOnRegistration")
            case .completedTrueCardEnableForRequest:
                accountMock = mockJson.load(resource: "accountCompletedTrueCardEnableForRequest")
            case .completedFalseCardEnableForRequest:
                accountMock = mockJson.load(resource: "accountCompletedFalseCardEnableForRequest")
            case .completedTrueCardEnableForRequestMigration:
                accountMock = mockJson.load(resource: "accountCompletedTrueCardEnableForRequestMigration")
            case .completedFalseCardEnableForRequestMigration:
                accountMock = mockJson.load(resource: "accountCompletedFalseCardEnableForRequestMigration")
            case .completedTrueCardEnableForRegistration:
                accountMock = mockJson.load(resource: "accountCompletedTrueCardEnableForRegistration")
            case .completedFalseCardEnableForRegistration:
                accountMock = mockJson.load(resource: "accountCompletedFalseCardEnableForRegistration")
            case .completedTrueCardDebitEnableForRegistration:
                accountMock = mockJson.load(resource: "accountCompletedTrueCardDebitEnableForRegistration")
            case .completedFalseCardDebitEnableForRegistration:
                accountMock = mockJson.load(resource: "accountCompletedFalseCardDebitEnableForRegistration")
            case .completedTrueCardEnableForActivateDebit:
                accountMock = mockJson.load(resource: "accountCompletedTrueCardEnableForActivateDebit")
            case .completedFalseCardEnableForActivateDebit:
                accountMock = mockJson.load(resource: "accountCompletedFalseCardEnableForActivateDebit")
            case .completedTrueCardEnableForDebitRequestInProgress:
                accountMock = mockJson.load(resource: "accountCompletedTrueCardEnableForDebitRequestInProgress")
            case .completedFalseCardEnableForDebitRequestInProgress:
                accountMock = mockJson.load(resource: "accountCompletedFalseCardEnableForDebitRequestInProgress")
            case .completedTrueCardEnabledForTracking:
                accountMock = mockJson.load(resource:
                    "accountCompletedTrueCardEnableForTracking")
            case .completedFalseCardEnabledForTracking:
                accountMock = mockJson.load(resource:
                    "accountCompletedFalseCardEnableForTracking")
            case .error:
                accountMock = mockJson.load(resource: "accountError")
            case .completedTrueCardEnabledForUpgradeDebitToCredit:
                accountMock = mockJson.load(resource: "accountCompletedTrueCardEnableForUpgradeDebitToCredit")
            case .accountCompletedTrueCardEnableForDebitRequestSimplified:
                accountMock = mockJson.load(resource: "accountCompletedTrueCardEnableForDebitRequestSimplified")
            }
        }
    }
}

extension CreditApiMock: CreditApiProtocol {
    var defauldCardId: String {
        return "12345"
    }
    
    var defauldCard: CardBank? {
        let cards =  try? MockCodable<[CardBank]>().loadCodableObject(resource: "CardsBankNoDebit")
        return cards?.first
    }
    
    func pciCvvIsEnable(cardBank: CardBank, cardValue: Double) -> Bool {
        return false
    }
    
    func saveCvvCard(id: String, value: String?) {
    }
    
    func cvvCard(id: String) -> String? {
        return nil
    }
    
    func invoiceDetail(invoiceId: String, completion: @escaping (PicPayResult<CreditInvoiceDetail>) -> Void) {
        guard let json = invoiceDetailMock else {
            return
        }
        if invoiceStatus! == .error {
            completion(PicPayResult<CreditInvoiceDetail>.failure(PicPayError(json: json)))
        } else {
            let data = try! json.rawData()
            let invoice = try! decoder.decode(CreditInvoiceDetail.self, from: data)
            let result: PicPayResult = PicPayResult<CreditInvoiceDetail>.success(invoice)
            completion(result)
        }
    }
    
    func account(complete: Bool, isLoanFlow: Bool, onCacheLoaded: @escaping (CPAccount) -> Void, completion: @escaping (PicPayResult<CPAccount>) -> Void) {
        guard let json = accountMock else {
            return
        }
        if accountType! == .error {
            let error = PicPayError(json: json)
            completion(PicPayResult<CPAccount>.failure(error))
        } else {
            let account = CPAccount(json: json)!
            completion(PicPayResult<CPAccount>.success(account))
        }
    }
    
    func sendInvoiceEmail(invoiceId: String, completion: @escaping (PicPayResult<BaseApiEmptyResponse>) -> Void) {
        if isSendInvoiceEmailError {
            let error = PicPayError(message: "Ocorreu um erro")
            let result = PicPayResult<BaseApiEmptyResponse>.failure(error)
            completion(result)
        } else {
            let emptyResult = BaseApiEmptyResponse(json: ["":""])!
            let result = PicPayResult<BaseApiEmptyResponse>.success(emptyResult)
            completion(result)
        }
    }
}

extension CreditApiMock {
    enum Account {
        case
            completedTrue,
            completedFalse,
            completedTrueOnRegistration,
            completedFalseOnRegistration,
            completedTrueDebitOnRegistration,
            completedFalseDebitOnRegistration,
            completedTrueCardEnableForRequest,
            completedFalseCardEnableForRequest,
            completedTrueCardEnableForRequestMigration,
            completedFalseCardEnableForRequestMigration,
            completedTrueCardEnableForRegistration,
            completedFalseCardEnableForRegistration,
            completedTrueCardDebitEnableForRegistration,
            completedFalseCardDebitEnableForRegistration,
            completedTrueCardEnableForActivateDebit,
            completedFalseCardEnableForActivateDebit,
            completedTrueCardEnableForDebitRequestInProgress,
            completedFalseCardEnableForDebitRequestInProgress,
            completedTrueCardEnabledForTracking,
            completedFalseCardEnabledForTracking,
            completedTrueCardEnabledForUpgradeDebitToCredit,
            accountCompletedTrueCardEnableForDebitRequestSimplified,
            error
    }
    
    enum Invoice {
        case delayed, open, closed, openPayment, error, negativeTotalValue, notExistMinimumPayment
        case delayedAndNotPayable, openEmptyTransactions, invoiceIsNil, openPaymentWithProcessedAndUnprocessedTransactions
        case openPaymentWithUnprocessedTransactions
    }
}
