import UI
import XCTest
@testable import PicPay

class InvoiceDetailInformationsViewModelTest: XCTestCase {
    
    func configureObject(status: CreditInvoice.Status, isPayable: Bool, hasInformation: Bool = false) -> CreditInvoice {
        let invoice = CreditInvoice(id: "1",
                                    totalValue: 1000,
                                    paidValue: 100,
                                    closingDate: Date(),
                                    dueDate: Date(),
                                    paymentDate: nil,
                                    minimumPayment: 100,
                                    status: status,
                                    payable: isPayable,
                                    statusText: "nada",
                                    earlyPayment: hasInformation ? 100 : nil,
                                    receivedPayment: hasInformation ? 100 : nil,
                                    totalDebit: hasInformation ? 100 : nil)
        
        
        return invoice
    }
    
    func testHasActionButtonForOpenStatusPayable() {
        let invoice = configureObject(status: .open, isPayable: true)
        let viewModel = InvoiceDetailInformationsViewModel(with: invoice, delegate: self)
        XCTAssertTrue(viewModel.hasActionButton)
    }
    
    func testHasActionButtonForOpenPaymentStatusPayable() {
        let invoice = configureObject(status: .openPayment, isPayable: true)
        let viewModel = InvoiceDetailInformationsViewModel(with: invoice, delegate: self)
        XCTAssertTrue(viewModel.hasActionButton)
    }
    
    func testHasActionButtonForClosedStatusPayable() {
        let invoice = configureObject(status: .closed, isPayable: true)
        let viewModel = InvoiceDetailInformationsViewModel(with: invoice, delegate: self)
        XCTAssertTrue(viewModel.hasActionButton)
    }
    
    func testHasActionButtonForDelayedStatusPayable() {
        let invoice = configureObject(status: .delayed, isPayable: true)
        let viewModel = InvoiceDetailInformationsViewModel(with: invoice, delegate: self)
        XCTAssertTrue(viewModel.hasActionButton)
    }
    
    // MARK: - Action button not payable
    
    func testHasActionButtonForOpenStatusNotPayable() {
        let invoice = configureObject(status: .open, isPayable: false)
        let viewModel = InvoiceDetailInformationsViewModel(with: invoice, delegate: self)
        XCTAssertFalse(viewModel.hasActionButton)
    }
    
    func testHasActionButtonForOpenPaymentStatusNotPayable() {
        let invoice = configureObject(status: .openPayment, isPayable: false)
        let viewModel = InvoiceDetailInformationsViewModel(with: invoice, delegate: self)
        XCTAssertFalse(viewModel.hasActionButton)
    }
    
    func testHasActionButtonForClosedStatusNotPayable() {
        let invoice = configureObject(status: .closed, isPayable: false)
        let viewModel = InvoiceDetailInformationsViewModel(with: invoice, delegate: self)
        XCTAssertTrue(viewModel.hasActionButton)
    }
    
    func testHasActionButtonForDelayedStatusNotPayable() {
        let invoice = configureObject(status: .delayed, isPayable: false)
        let viewModel = InvoiceDetailInformationsViewModel(with: invoice, delegate: self)
        XCTAssertTrue(viewModel.hasActionButton)
    }
    
    // MARK: - Button config
    
    func testHasActionButtonConfigForNotPayable() {
        let title = CreditPicPayLocalizable.btnInvoiceSendToEmail.text
        let titleColor = Palette.ppColorGrayscale400.color
        let backgroundColor: UIColor = .clear
        let borderColor = Palette.ppColorGrayscale400.color
        
        let invoice = configureObject(status: .open, isPayable: false)
        let viewModel = InvoiceDetailInformationsViewModel(with: invoice, delegate: self)
        
        XCTAssertEqual(viewModel.actionButtonConfig.title, title)
        XCTAssertEqual(viewModel.actionButtonConfig.titleColor.cgColor, titleColor.cgColor)
        XCTAssertEqual(viewModel.actionButtonConfig.normalColor.cgColor, backgroundColor.cgColor)
        XCTAssertEqual(viewModel.actionButtonConfig.borderColor.cgColor, borderColor.cgColor)
    }
    
    func testHasActionButtonConfigForOpenStatusPayable() {
        let title = CreditPicPayLocalizable.btInvoiceDetailEarlyPayment.text
        let titleColor = Palette.white.color
        let backgroundColor = Palette.ppColorBranding300.color
        let borderColor:UIColor = .clear
        
        let invoice = configureObject(status: .open, isPayable: true)
        let viewModel = InvoiceDetailInformationsViewModel(with: invoice, delegate: self)
        
        XCTAssertEqual(viewModel.actionButtonConfig.title, title)
        XCTAssertEqual(viewModel.actionButtonConfig.titleColor.cgColor, titleColor.cgColor)
        XCTAssertEqual(viewModel.actionButtonConfig.normalColor.cgColor, backgroundColor.cgColor)
        XCTAssertEqual(viewModel.actionButtonConfig.borderColor.cgColor, borderColor.cgColor)
    }
    
    func testHasActionButtonConfigForOpenPaymentStatusPayable() {
        let title = CreditPicPayLocalizable.payInvoice.text
        let titleColor = Palette.white.color
        let backgroundColor = Palette.ppColorBranding300.color
        let borderColor:UIColor = .clear
        
        let invoice = configureObject(status: .openPayment, isPayable: true)
        let viewModel = InvoiceDetailInformationsViewModel(with: invoice, delegate: self)
        
        XCTAssertEqual(viewModel.actionButtonConfig.title, title)
        XCTAssertEqual(viewModel.actionButtonConfig.titleColor.cgColor, titleColor.cgColor)
        XCTAssertEqual(viewModel.actionButtonConfig.normalColor.cgColor, backgroundColor.cgColor)
        XCTAssertEqual(viewModel.actionButtonConfig.borderColor.cgColor, borderColor.cgColor)
    }
    
    func testHasActionButtonConfigForClosedStatusPayable() {
        let title = CreditPicPayLocalizable.payInvoice.text
        let titleColor = Palette.white.color
        let backgroundColor = Palette.ppColorBranding300.color
        let borderColor: UIColor = .clear
        
        let invoice = configureObject(status: .closed, isPayable: true)
        let viewModel = InvoiceDetailInformationsViewModel(with: invoice, delegate: self)
        
        XCTAssertEqual(viewModel.actionButtonConfig.title, title)
        XCTAssertEqual(viewModel.actionButtonConfig.titleColor.cgColor, titleColor.cgColor)
        XCTAssertEqual(viewModel.actionButtonConfig.normalColor.cgColor, backgroundColor.cgColor)
        XCTAssertEqual(viewModel.actionButtonConfig.borderColor.cgColor, borderColor.cgColor)
    }
    
    func testHasActionButtonConfigForDelayedStatusPayable() {
        let title = CreditPicPayLocalizable.payInvoice.text
        let titleColor = Palette.white.color
        let backgroundColor = Palette.ppColorNegative300.color
        let borderColor: UIColor = .clear
        
        let invoice = configureObject(status: .delayed, isPayable: true)
        let viewModel = InvoiceDetailInformationsViewModel(with: invoice, delegate: self)
        
        XCTAssertEqual(viewModel.actionButtonConfig.title, title)
        XCTAssertEqual(viewModel.actionButtonConfig.titleColor.cgColor, titleColor.cgColor)
        XCTAssertEqual(viewModel.actionButtonConfig.normalColor.cgColor, backgroundColor.cgColor)
        XCTAssertEqual(viewModel.actionButtonConfig.borderColor.cgColor, borderColor.cgColor)
    }
    
    // MARK: Test view model of invoicePaymentInformationViewModel
    
    func testInvoicePaymentInformationViewModelEqualNil() {
        let invoice = configureObject(status: .delayed, isPayable: true)
        let viewModel = InvoiceDetailInformationsViewModel(with: invoice, delegate: self)
        XCTAssertNil(viewModel.invoicePaymentInformationViewModel)
    }
    
    func testInvoicePaymentInformationViewModelNotNil() {
        let invoice = configureObject(status: .delayed, isPayable: true, hasInformation: true)
        let viewModel = InvoiceDetailInformationsViewModel(with: invoice, delegate: self)
        XCTAssertNotNil(viewModel.invoicePaymentInformationViewModel)
    }
    
    func testInvoiceValueDateViewModelNotNil() {
        let invoice = configureObject(status: .delayed, isPayable: true, hasInformation: true)
        let viewModel = InvoiceDetailInformationsViewModel(with: invoice, delegate: self)
        XCTAssertNotNil(viewModel.invoiceValueDateViewModel)
    }
}

extension InvoiceDetailInformationsViewModelTest: InvoiceDetailInformationsDelegate {
    func sendInvoiceEmail() {}
    func pay() {
        
    }
}
