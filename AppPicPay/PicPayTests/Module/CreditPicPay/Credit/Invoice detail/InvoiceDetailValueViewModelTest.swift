import UI
import XCTest
@testable import PicPay

class InvoiceDetailValueViewModelTest: XCTestCase {
    func viewModel(for status: CreditInvoice.Status, minimumPaymentValue: Double? = 200) -> InvoiceDetailValueViewModel {
        let date = DateFormatter.brazillianFormatter(dateFormat: "dd/MM/yyyy").date(from: "23/02/2023")!
        let viewModel = InvoiceDetailValueViewModel(value: 2000,
                                                    dueDate: date,
                                                    closingDate: date,
                                                    minimumPayment: minimumPaymentValue,
                                                    invoiceStatus: status)
        return viewModel
    }
    
    // MARK: Has minimum payment
    
    func testHasMinimumPaymentForOpenStatus() {
        let viewModel = self.viewModel(for: .open)
        XCTAssertFalse(viewModel.hasMinimumPayment)
    }
    
    func testHasMinimumPaymentForOpenPaymentStatus() {
        let viewModel = self.viewModel(for: .openPayment)
        XCTAssertTrue(viewModel.hasMinimumPayment)
    }
    
    func testHasMinimumPaymentForClosedStatus() {
        let viewModel = self.viewModel(for: .closed)
        XCTAssertTrue(viewModel.hasMinimumPayment)
    }
    
    func testHasMinimumPaymentForDelayedStatus() {
        let viewModel = self.viewModel(for: .delayed)
        XCTAssertTrue(viewModel.hasMinimumPayment)
    }
    
    func testHasMinimumPaymentForNilValue() {
        let viewModel = self.viewModel(for: .delayed, minimumPaymentValue: nil)
        XCTAssertFalse(viewModel.hasMinimumPayment)
    }
    
    func testHasMinimumPaymentForZeroValue() {
        let viewModel = self.viewModel(for: .delayed, minimumPaymentValue: 0)
        XCTAssertFalse(viewModel.hasMinimumPayment)
    }
    
    // MARK: has closing date
    
    func testHasClosingDateForOpenStatus() {
        let viewModel = self.viewModel(for: .open)
        XCTAssertTrue(viewModel.hasClosingDate)
    }
    
    func testHasClosingDateForOpenPaymentStatus() {
        let viewModel = self.viewModel(for: .openPayment)
        XCTAssertFalse(viewModel.hasClosingDate)
    }
    
    func testHasClosingDateForClosedStatus() {
        let viewModel = self.viewModel(for: .closed)
        XCTAssertFalse(viewModel.hasClosingDate)
    }
    
    func testHasClosingDateForDelayedStatus() {
        let viewModel = self.viewModel(for: .delayed)
        XCTAssertFalse(viewModel.hasClosingDate)
    }
    
    // MARK: Text's
    
    func testClosingDateFormatted() {
        let viewModel = self.viewModel(for: .open)
        let expected = NSMutableAttributedString(string: "Fechamento em 23 de Fevereiro")
        XCTAssertEqual(expected, viewModel.closingDateFormatted)
    }
    
    func testDueDateFormattedGreen() {
        let viewModel = self.viewModel(for: .open)
        let color = Palette.ppColorBranding300.color
        let text = "Vencimento 23 de Fevereiro"
        let location = 10
        let range = NSRange(location: location, length: text.count - location)
        let expected = NSMutableAttributedString(string: text)
        expected.addAttributes([NSAttributedString.Key.foregroundColor: color], range: range)
        
        XCTAssertEqual(expected.string, viewModel.dueDateFormatted.string)
        viewModel.dueDateFormatted.enumerateAttribute(NSAttributedString.Key.foregroundColor, in: NSRange(0..<expected.length)) { value, range, stop in
            if let foregroundColor = value as? UIColor {
                XCTAssertEqual(color.cgColor, foregroundColor.cgColor)
            }
        }
    }
    
    func testDueDateFormattedRed() {
        let viewModel = self.viewModel(for: .delayed)
        let color = Palette.ppColorNegative300.color
        let text = "Vencimento 23 de Fevereiro"
        let location = 10
        let range = NSRange(location: location, length: text.count - location)
        let expected = NSMutableAttributedString(string: text)
        expected.addAttributes([NSAttributedString.Key.foregroundColor: color], range: range)
        
        XCTAssertEqual(expected.string, viewModel.dueDateFormatted.string)
        viewModel.dueDateFormatted.enumerateAttribute(NSAttributedString.Key.foregroundColor, in: NSRange(0..<expected.length)) { value, range, stop in
            if let foregroundColor = value as? UIColor {
                XCTAssertEqual(color.cgColor, foregroundColor.cgColor)
            }
        }
    }
    
    func testMinimumPaymentFormatted() {
        let viewModel = self.viewModel(for: .delayed)
        let expected = NSAttributedString(string: "Pagamento mínimo de R$ 200,00")
        
        XCTAssertNotNil(viewModel.minimumPaymentFormatted)
        XCTAssertEqual(expected, viewModel.minimumPaymentFormatted!)
    }
    
    func testMinimumPaymentFormattedNil() {
        let viewModel = self.viewModel(for: .open, minimumPaymentValue: nil)
        XCTAssertNil(viewModel.minimumPaymentFormatted)
    }
}
