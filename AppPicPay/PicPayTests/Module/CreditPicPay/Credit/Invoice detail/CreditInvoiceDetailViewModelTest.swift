import XCTest
import SwiftyJSON
@testable import PicPay

class CreditInvoiceDetailViewModelTest: XCTestCase {
    var viewModel: CreditInvoiceDetailViewModel!
    
    override func setUp() {
        super.setUp()
        let json = MockJSON().load(resource: "accountCompletedTrue")
        let account = CPAccount(json: json)!
        let invoiceDetail: CreditInvoiceDetail = try! MockCodable().loadCodableObject(resource: "invoiceDetailOpen")
        
        let mock = CreditApiMock()
        mock.invoiceStatus = .open
        let viewModel = CreditInvoiceDetailViewModel(with: invoiceDetail,
                                                     account: account,
                                                     coordinatorDelegate: nil,
                                                     isPresented: false,
                                                     creditApi: mock)
        viewModel.loadData {}
        self.viewModel = viewModel
    }
    
    func testGetTransactionNil() {
        let transaction = viewModel.getTransaction(in: IndexPath(row: 200, section: 10))
        XCTAssertNil(transaction)
        
        let transaction2 = viewModel.getTransaction(in: IndexPath(row: 200, section: 1))
        XCTAssertNil(transaction2)
    }
    
    func testGettransactionNotNil() {
        let transactionProcessed = viewModel.getTransaction(in: IndexPath(row: 1, section: 1))
        let transactionUnprocessed = viewModel.getTransaction(in: IndexPath(row: 1, section: 2))
        XCTAssertNotNil(transactionProcessed)
        XCTAssertNotNil(transactionUnprocessed)
    }
    
    func testNumberOfRows() {
        let sectionInformation = 0
        let sectionUnprocessedTransaction = 1
        let sectionProcessedtransaction = 2
        
        let information = viewModel.numberOfRows(in: sectionInformation)
        let unprocessed = viewModel.numberOfRows(in: sectionUnprocessedTransaction)
        let processed = viewModel.numberOfRows(in: sectionProcessedtransaction)
        
        XCTAssertEqual(information, 0)
        XCTAssertEqual(unprocessed, 3)
        XCTAssertEqual(processed, 13)
    }
    
    func testNumberOfRowsIsUnprocessedColapsed() {
        viewModel.isCollapsedUnprocessedTransactions = true
        let sectionInformation = 0
        let sectionUnprocessedTransaction = 1
        let sectionProcessedtransaction = 2
        
        let information = viewModel.numberOfRows(in: sectionInformation)
        let unprocessed = viewModel.numberOfRows(in: sectionUnprocessedTransaction)
        let processed = viewModel.numberOfRows(in: sectionProcessedtransaction)
        
        XCTAssertEqual(information, 0)
        XCTAssertEqual(unprocessed, 0)
        XCTAssertEqual(processed, 13)
    }
    
    func testNumberOfRowsIsLoading() {
        viewModel.isListLoading = true
        let sectionInformation = 0
        let sectionUnprocessedTransaction = 1
        let sectionProcessedtransaction = 2
        
        let information = viewModel.numberOfRows(in: sectionInformation)
        let unprocessed = viewModel.numberOfRows(in: sectionUnprocessedTransaction)
        let processed = viewModel.numberOfRows(in: sectionProcessedtransaction)
        
        XCTAssertEqual(information, 0)
        XCTAssertEqual(unprocessed, 0)
        XCTAssertEqual(processed, 5)
    }
    
    func testNumberOfRowsNotExistSection() {
        let section = 3
        let rows = viewModel.numberOfRows(in: section)
        XCTAssert(rows >= 0)
    }
    
    func testTitleForNotExistSection() {
        let title = viewModel.title(for: 3)
        XCTAssertNil(title)
    }
    
    func testTitleForSection() {
        let sectionInformation = 0
        let sectionUnprocessedTransaction = 1
        let sectionProcessedtransaction = 2
        
        let information = viewModel.title(for: sectionInformation)
        let unprocessed = viewModel.title(for: sectionUnprocessedTransaction)
        let processed = viewModel.title(for: sectionProcessedtransaction)
        
        XCTAssertNil(information)
        XCTAssertEqual(unprocessed, "Lançamentos em processamento")
        XCTAssertNil(processed)
    }
    
    func testHeightForHeaderForNotExistSection() {
        let height = viewModel.heightForHeader(in: 3)
         XCTAssertEqual(height, CGFloat.leastNonzeroMagnitude)
    }
    
    func testHeightForHeaderIsLoading() {
        viewModel.isListLoading = true
        let sectionInformation = 0
        let sectionUnprocessedTransaction = 1
        let sectionProcessedtransaction = 2
        
        let information = viewModel.heightForHeader(in: sectionInformation)
        let unprocessed = viewModel.heightForHeader(in: sectionUnprocessedTransaction)
        let processed = viewModel.heightForHeader(in: sectionProcessedtransaction)
        
        XCTAssertEqual(information, UITableView.automaticDimension)
        XCTAssertEqual(unprocessed, CGFloat.leastNonzeroMagnitude)
        XCTAssertEqual(processed, CGFloat.leastNonzeroMagnitude)
    }
    
    func testHeightForHeader() {
        let sectionInformation = 0
        let sectionUnprocessedTransaction = 1
        let sectionProcessedtransaction = 2
        
        let information = viewModel.heightForHeader(in: sectionInformation)
        let unprocessed = viewModel.heightForHeader(in: sectionUnprocessedTransaction)
        let processed = viewModel.heightForHeader(in: sectionProcessedtransaction)
        
        XCTAssertEqual(information, UITableView.automaticDimension)
        XCTAssertEqual(unprocessed, UITableView.automaticDimension)
        XCTAssertEqual(processed, 8.0)
    }
    
    func testHeightForFooterForNotExistSection() {
        let height = viewModel.heightForFooter(in: 3)
        XCTAssertEqual(height, CGFloat.leastNonzeroMagnitude)
    }
    
    func testHeightForFooterIsLoading() {
        viewModel.isListLoading = true
        let sectionInformation = 0
        let sectionUnprocessedTransaction = 1
        let sectionProcessedtransaction = 2
        
        let information = viewModel.heightForFooter(in: sectionInformation)
        let unprocessed = viewModel.heightForFooter(in: sectionUnprocessedTransaction)
        let processed = viewModel.heightForFooter(in: sectionProcessedtransaction)
        
        XCTAssertEqual(information, CGFloat.leastNonzeroMagnitude)
        XCTAssertEqual(unprocessed, CGFloat.leastNonzeroMagnitude)
        XCTAssertEqual(processed, CGFloat.leastNonzeroMagnitude)
    }
    
    func testHeightForFooter() {
        let sectionInformation = 0
        let sectionUnprocessedTransaction = 1
        let sectionProcessedtransaction = 2
        
        let information = viewModel.heightForFooter(in: sectionInformation)
        let unprocessed = viewModel.heightForFooter(in: sectionUnprocessedTransaction)
        let processed = viewModel.heightForFooter(in: sectionProcessedtransaction)
        
        XCTAssertEqual(information, CGFloat.leastNonzeroMagnitude)
        XCTAssertEqual(unprocessed, 1.0)
        XCTAssertEqual(processed, 1.0)
    }
}
