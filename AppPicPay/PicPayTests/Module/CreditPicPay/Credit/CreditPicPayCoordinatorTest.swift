import XCTest
import SwiftyJSON
@testable import PicPay

class CreditPicPayCoordinatorTest: XCTestCase {
    var window: UIWindow?
    var navigationController: UINavigationController!
    var account: CPAccount?
    
    override func setUp() {
        super.setUp()
        navigationController = UINavigationController()
        
        let mockJSON = MockJSON().load(resource: "accountCompletedTrue")
        account = CPAccount(json: mockJSON)
    }

    override func tearDown() {
        navigationController = nil
        super.tearDown()
    }

    func testStartCoordinator() {
        let coordinator = CreditPicPayCoordinator(navigationController: self.navigationController)
        coordinator.start()
        let currentCoordinator = coordinator.currentCoordinator
        XCTAssert(currentCoordinator is CreditPicPayHomeCoordinator)
    }
    
    func testStartCoordinatorWithAccount() {
        let coordinator = CreditPicPayCoordinator(navigationController: navigationController, account: account)
        coordinator.start()
        let currentCoordinator = coordinator.currentCoordinator
        XCTAssert(currentCoordinator is CreditPicPayHomeCoordinator)
    }
    
    func testStartCoordinatorWithAccountInListInvoices() {
        let coordinator = CreditPicPayCoordinator(navigationController: navigationController, account: account)
        coordinator.startInListInvoices()
        let currentCoordinator = coordinator.currentCoordinator
        XCTAssert(currentCoordinator is CreditInvoiceListCoordinator)
    }
    
    func testStartCoordinatorPay() {
        let coordinator = CreditPicPayCoordinator(navigationController: navigationController, account: account)
        coordinator.startPayCoordinator(with: "123")
        let currentCoordinator = coordinator.currentCoordinator
        XCTAssert(currentCoordinator is CreditPicPayInvoicePayCoordinator)
    }
    
    func testStartInInvoiceDetailWithList() {
        let coordinator = CreditPicPayCoordinator(navigationController: navigationController, account: account)
        coordinator.startInInvoiceDetailWithList(with: "111")
        let currentCoordinator = coordinator.currentCoordinator
        XCTAssert(currentCoordinator is CreditInvoiceDetailCoordinator)
    }
    
    func testStartInInvoiceDetail() {
        let coordinator = CreditPicPayCoordinator(navigationController: navigationController, account: account)
        coordinator.startInInvoiceDetail(with: "11232")
        let currentCoordinator = coordinator.currentCoordinator
        XCTAssert(currentCoordinator is CreditInvoiceDetailCoordinator)
    }
    
    func testShowInvoiceInList() {
        let coordinator = CreditPicPayCoordinator(navigationController: navigationController, account: account)
        let mockJSON = MockJSON().load(resource: "accountCompletedTrue")
        guard let account = CPAccount(json: mockJSON) else {
            XCTFail()
            return
        }
        
        coordinator.showInvoiceList(for: account)
        XCTAssert(coordinator.currentCoordinator is CreditInvoiceListCoordinator)
    }
    
    func testShowInvoiceDetailFromHome() {
        let coordinator = CreditPicPayCoordinator(navigationController: navigationController, account: account)
        coordinator.start()
        XCTAssert(coordinator.currentCoordinator is CreditPicPayHomeCoordinator)
        
        let mockJSON = MockJSON().load(resource: "accountCompletedTrue")
        guard let account = CPAccount(json: mockJSON) else {
            XCTFail()
            return
        }
        coordinator.showInvoiceDetailFromHome(with: "12131", and: account)
        XCTAssert(coordinator.currentCoordinator is CreditInvoiceDetailCoordinator)
    }
    
    func testPayInvoice() {
        let coordinator = CreditPicPayCoordinator(navigationController: navigationController, account: account)
        coordinator.payInvoice(with: "", and: account)
        XCTAssert(coordinator.currentCoordinator is CreditPicPayInvoicePayCoordinator)
    }
    
    func testDidSelectedInvoice() {
        let coordinator = CreditPicPayCoordinator(navigationController: navigationController, account: account)
        let invoice = CreditInvoice(id: "1231",
                                    totalValue: 1000,
                                    paidValue: 10,
                                    closingDate: Date(),
                                    dueDate: Date(),
                                    paymentDate: nil,
                                    minimumPayment: 1,
                                    status: .open,
                                    payable: true,
                                    statusText: "",
                                    earlyPayment: nil,
                                    receivedPayment: nil,
                                    totalDebit: nil)
        let creditInvoiceDetail = CreditInvoiceDetail(with: invoice)
        coordinator.didSelectedInvoice(creditInvoiceDetail)
        XCTAssert(coordinator.currentCoordinator is CreditInvoiceDetailCoordinator)
    }
    
    func testPayCoordinator() {
        let coordinator = CreditPicPayCoordinator(navigationController: navigationController, account: account)
        coordinator.pay(with: "1231", and: account)
        XCTAssert(coordinator.currentCoordinator is CreditPicPayInvoicePayCoordinator)
    }
}
