import XCTest
import OHHTTPStubs
@testable import PicPay

class PromoCodeWorkerTest: XCTestCase {
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testPromoCodeValidateWebview() {
        let expectation = XCTestExpectation(description: "Expected to have a valid WebView type PromoCode")

        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(with: Mocks.webview.data)
        }

        PromoCodeWorker.validate("123") { result in
            switch result {
            case .success(_):
                expectation.fulfill()
            case .failure(_):
                XCTFail("expected to succeed")
            }
        }

        wait(for: [expectation], timeout: 5.0)
    }

    func testPromoCodeValidateReferral() {
        let expectation = XCTestExpectation(description: "Expected to have a valid Referreal type PromoCode")

        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(with: Mocks.referral.data)
        }

        PromoCodeWorker.validate("123") { result in
            switch result {
            case .success(_):
                expectation.fulfill()
            case .failure(_):
                XCTFail("expected to succeed")
            }
        }

        wait(for: [expectation], timeout: 5.0)
    }

    func testPromoCodeStudentAccount() {
        let expectation = XCTestExpectation(description: "Expected to have a valid Referreal type PromoCode")

        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(with: Mocks.studentAccount.data)
        }

        PromoCodeWorker.validate("123") { result in
            switch result {
            case .success(_):
                expectation.fulfill()
            case .failure(_):
                XCTFail("expected to succeed")
            }
        }

        wait(for: [expectation], timeout: 5.0)
    }
    
    func testShowError() {
        let expectation = XCTestExpectation(description: "Expected to have an Error")
        
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.fail()
        }
        
        PromoCodeWorker.validate("123") { result in
            switch result {
            case .success(_):
                XCTFail("expected to fail")
            case .failure(_):
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
}

private enum Mocks {
    case webview
    case referral
    case studentAccount

    var data: Data {
        return payload.data(using: .utf8)!
    }

    var payload: String {
        switch self {
        case .webview:
            return """
            {
            "data": {
            "coupon_info": {
            "code": "BBB",
            "campaign_type": "cashback"
            },
            "reward_category": "coupon-webview",
            "webview": "https://picpay.com"
            }
            }

"""
        case .referral:
            return """
            {
            "data":{
            "inviter":{
            "consumer_status_follower":"1",
            "follower_status_consumer":"0",
            "profile":{
            "id":"1199222",
            "name":"PicPay",
            "mobile_phone_verified":"29999999913",
            "img_url":"https://picpay.s3.amazonaws.com/profiles-processed-images/533ecde54a84a86293169e4da870ba54.200.jpg",
            "username":"picpay",
            "verified":"1",
            "business_account":"0",
            "img_url_large":"https://picpay.s3.amazonaws.com/profiles-processed-images/533ecde54a84a86293169e4da870ba54.500.jpg"
            },
            "counts":{
            "following":2,
            "followers":118131
            },
            "can_see":true
            },
            "mgm_info":{
            "referral_code":"MGM123",
            "reward_value":10.0,
            "reward_value_str":"R$ 10,00"
            },
            "reward_category":"default",
            "webview":"https://picpay.com"
            }
            }

"""
        case .studentAccount:
            return """
            {
            "data":{
            "coupon_info":{
            "code":"UBER21"
            },
            "reward_category":"coupon-app-screen",
            "app_screen_path":"student-account"
            }
            }
"""
        }
    }
}
