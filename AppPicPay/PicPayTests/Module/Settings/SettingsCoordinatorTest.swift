import XCTest
@testable import PicPay

class SettingsCoordinatorTest: XCTestCase {
    private var navigationControllerSpy = NavigationControllerMock()
    private var coordinator: SettingsCoordinator?
    private var keysAction: [SettingsRow.TypeAction]!
    let keysScreen: [SettingsCoordinator.TypeScreen] = [.changePassword,
                                                        .aboutPolicies,
                                                        .aboutTerms,
                                                        .ombudsman
                                                        ]
    
    

    override func setUp() {
        super.setUp()
        keysAction = [  .myPicPay,
                        .myNumber,
                        .myNumberCheck,
                        .myEmail,
                        .bankAccount,
                        .bankAccountCheck(PPBankAccount()),
                        .incomeStatements,
                        .myAddress,
                        .linkedAccounts,
                        .promotionalCode,
                        .recommendStores,
                        .mySignatures,
                        .noFreeParcelling,
                        .leavePicPayPro,
                        .picpayPRO,
                        .forEstablishment,
                        .saleSubscriptions,
                        .privacity,
                        .notifications,
                        .beta,
                        .deactivateAccount,
                    ]
       coordinator = SettingsCoordinator(navigationController: navigationControllerSpy)
    }
    
    func testInstantiateViewController() {
        keysAction.forEach { (key) in
            let result = coordinator?.instantiateViewController(transition: key)
            XCTAssertNotNil(result, "Could not instantiate a ViewControler for \(key)")
        }
    }
    
    func testInstantiateDelegateViewController() {
        keysScreen.forEach { (key) in
            let result = coordinator?.instantiateDelegateViewController(transition: key)
            XCTAssertNotNil(result)
        }
    }

    func testDidTapForgotPassword_ShouldStartPasswordResetFlow() {
        coordinator?.didTapForgotPassword()

        XCTAssertTrue(navigationControllerSpy.isPresentViewControllerCalled)
    }
}
