import Core
import CoreLegacy
import Foundation
import XCTest
@testable import PicPay

final class SettingsHeaderViewModelMock: SettingsHeaderViewModelProtocol {
    var username: String { "username_test" }
    var fullname: String { "fullname_test" }
    var contact: PPContact { PPContact() }
    var shouldDisplayBankInfo: Bool { false }
    func fetchBankInfo(completion: @escaping (Result<String, ApiError>) -> Void) {}
    func resizeImage(image: UIImage) -> Data? { return nil }
    func uploadProfileImage(imageData: Data, uploadProgress: @escaping (CGFloat) -> Void, onSuccess: @escaping (String?) -> Void, onError: @escaping (Error) -> Void) {}
}

class SettingsViewControllerTest: XCTestCase {
    func testInstantiateSettingsViewController() {
        let viewModel = SettingsViewModel()
        let vc = SettingsViewController(with: viewModel)

        XCTAssertNotNil(vc)
    }
    
    func testInstantiateSettingsHeaderViewController() {
        let viewModel = SettingsHeaderViewModelMock()
        let vc = SettingsHeaderViewController(with: viewModel)
        
        XCTAssertNotNil(vc)
    }
}
