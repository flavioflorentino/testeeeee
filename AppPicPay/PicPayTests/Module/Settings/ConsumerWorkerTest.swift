import Foundation
@testable import PicPay

final class ConsumerWorkerTest: ConsumerWorkerProtocol {
    var consumer: MBConsumer?
    
    init(consumer: MBConsumer) {
        self.consumer = consumer
    }
}
