import Core
import XCTest
@testable import PicPay

class ConsumerApiTest: ConsumerApiProtocol {
    private let success: Bool
    
    init(success: Bool) {
        self.success = success
    }
    
    func uploadProfileImage(imageData: Data, uploadProgress: @escaping ((Double) -> Void), completion: @escaping ((PicPayResult<ProfileImageUpload>) -> Void)) {
        let image: ProfileImageUpload = try! MockCodable().loadCodableObject(resource: "ProfileImageUpload")
        if success {
           completion(PicPayResult.success(image))
        } else {
            let erro = PicPayError(message: "Teste")
            completion(PicPayResult.failure(erro))
        }        
    }
    
    func validatePromotionalCode(_ code: String, completion: @escaping ((PicPayResult<PPPromoCode>) -> Void)) {
    }
    
    func updateNotificationToken(completion: ((PicPayResult<BaseApiEmptyResponse>) -> Void)?) {
    }
    
    func fetchBankInfo(completion: @escaping (Result<SettingsBankInfo, ApiError>) -> Void) { }
}

class SettingsHeaderViewModelTest: XCTestCase {
    private lazy var featureManagerMock = FeatureManagerMock()
    private lazy var container = DependencyContainerMock(featureManagerMock)
    
    func mockViewModel(success: Bool = true) -> SettingsHeaderViewModel {
        let consumer = MBConsumer()
        consumer.username = "gustavo.storck"
        consumer.name = "Gustavo Storck Andrade de Souza"
        consumer.img_url = "https://cdn-images-1.medium.com/max/1200/1*a8BQ7tLccfU25aNr0ar6ZA.png"
        
        let viewModel = SettingsHeaderViewModel(dependencies: container,
                                                api: ConsumerApiTest(success: success),
                                                worker: ConsumerWorkerTest(consumer: consumer))
        return viewModel
    }
    
    func testUsername() {
        let viewModel = mockViewModel()
        let result = viewModel.username
        XCTAssertEqual(result, "@gustavo.storck")
    }
    
    func testFullname() {
        let viewModel = mockViewModel()
        let result = viewModel.fullname
        XCTAssertEqual(result, "Gustavo Storck Andrade de Souza")
    }
    
    func testContact() {
        let viewModel = mockViewModel()
        let result = viewModel.contact
        XCTAssertEqual(result.imgUrl, "https://cdn-images-1.medium.com/max/1200/1*a8BQ7tLccfU25aNr0ar6ZA.png")
    }
    
    func testResizeImage() throws {
        let viewModel = mockViewModel()
        let image = UIImage(named: "ilu_document_back")!
        let resizeImageData = viewModel.resizeImage(image: image)
        let resizeImage = UIImage(data: try XCTUnwrap(resizeImageData))
        let imageMaxDimension = max(resizeImage?.size.width ?? 0, resizeImage?.size.height ?? 0)
        
        let expectedSize: CGFloat = 1200
        XCTAssertEqual(imageMaxDimension, expectedSize)
    }
    
    func testUploadProfileImageSuccess() {
        let viewModel = mockViewModel()
        let image = #imageLiteral(resourceName: "avatar_person")
        let imageData = image.jpegData(compressionQuality: 0.8)!
        
        let expectation = self.expectation(description: "Image")
        
        viewModel.uploadProfileImage(imageData: imageData, uploadProgress: { (_) in
            XCTFail()
        }, onSuccess: { (_) in
            expectation.fulfill()
            XCTAssert(true)
        }) { (_) in
            XCTFail()
        }
        
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testUploadProfileImageFailure() {
        let viewModel = mockViewModel(success: false)
        let image = #imageLiteral(resourceName: "avatar_person")
        let imageData = image.jpegData(compressionQuality: 0.8)!
        let expectation = self.expectation(description: "Image")
        
        viewModel.uploadProfileImage(imageData: imageData, uploadProgress: { (_) in
            XCTFail()
        }, onSuccess: { (_) in
            XCTFail()
        }) { (_) in
            expectation.fulfill()
            XCTAssert(true)
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }
}
