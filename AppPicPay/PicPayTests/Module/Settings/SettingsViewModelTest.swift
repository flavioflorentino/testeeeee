import Core
import FeatureFlag
import XCTest

@testable import PicPay

final class SettingsServiceTest: SettingsServicing {
    typealias Dependencies = HasKVStore & HasFeatureManager
    private let dependencies: Dependencies

    private let renewalSettingsSuccess: Bool
    private let valueBiometric: Int
    private let canEvaluateDeviceSecurity: Bool
    private let typeRemote: FeatureConfig?
    private let isActive: Bool
    private let isEnableCreditAux: Bool
    private let isPhysicalCardEnable: Bool
    private let isVirtualCardEnable: Bool
    private let checkStudentAccountStatusAux: Bool
    private let isCardSettingsEnableAux: Bool
    private let isCardCreditSettingsEnabled: Bool
    private let statusUpgrade: AccountStatus
    private let isLoanSettingsEnabledMock: Bool
    private let isRegistrationRenewalEnabledMock: Bool
    private let successLoanResponse = LoanSettingsResponse(showMenu: true)
    private let fetchP2PLendingSectionSettingsExpectedResult: Result<P2PLendingSectionSettings, ApiError>
    
    init(
        valueBiometric: Int,
        isActive: Bool,
        typeRemote: FeatureConfig? = nil,
        canEvaluateDeviceSecurity: Bool = true,
        isEnableCredit: Bool = true,
        isPhysicalCardEnable: Bool = true,
        isVirtualCardEnable: Bool = true,
        checkStudentAccountStatus: Bool,
        statusUpgrade: AccountStatus = .approved,
        isCardSettingsEnable: Bool = true,
        isCardCreditSettingsEnabled: Bool = true,
        isLoanSettingsEnabledMock: Bool = true,
        renewalSettingsSuccess: Bool = true,
        isRegistrationRenewalEnabledMock: Bool = true,
        fetchP2PLendingSectionSettingsExpectedResult: Result<P2PLendingSectionSettings, ApiError>,
        dependencies: Dependencies = DependencyContainerMock()
    ) {
        self.dependencies = dependencies
        self.valueBiometric = valueBiometric
        self.isActive = isActive
        self.typeRemote = typeRemote
        self.canEvaluateDeviceSecurity = canEvaluateDeviceSecurity
        self.isEnableCreditAux = isEnableCredit
        self.isPhysicalCardEnable = isPhysicalCardEnable
        self.isVirtualCardEnable = isVirtualCardEnable
        self.checkStudentAccountStatusAux = checkStudentAccountStatus
        self.statusUpgrade = statusUpgrade
        self.isCardSettingsEnableAux = isCardSettingsEnable
        self.isCardCreditSettingsEnabled = isCardCreditSettingsEnabled
        self.isLoanSettingsEnabledMock = isLoanSettingsEnabledMock
        self.isRegistrationRenewalEnabledMock = isRegistrationRenewalEnabledMock
        self.renewalSettingsSuccess = renewalSettingsSuccess
        self.fetchP2PLendingSectionSettingsExpectedResult = fetchP2PLendingSectionSettingsExpectedResult
    }
    
    var isRegistrationRenewalEnabled: Bool {
        isRegistrationRenewalEnabledMock
    }
    
    var isLoanSettingsEnabled: Bool {
        isLoanSettingsEnabledMock
    }
    
    func appVersion() -> String? {
        return "10.12.15"
    }
    
    func biometricType() -> BiometricTypeAuth.BiometricType {
        switch valueBiometric {
        case 0:
            return BiometricTypeAuth.BiometricType.none
        case 1:
            return BiometricTypeAuth.BiometricType.touchID
        case 2:
            return BiometricTypeAuth.BiometricType.faceID
        default:
            return BiometricTypeAuth.BiometricType.none
        }
    }

    func shouldShowAppProtectionRow() -> Bool {
        guard dependencies.featureManager.isActive(.releaseAppProtectionShowBool) else {
            return false
        }

        let valueForAppProtection = dependencies.kvStore.boolFor(AppProtectionKey.userDeviceProtectedValue)
        return canEvaluateDeviceSecurity || valueForAppProtection
    }
    
    func numberOfUnreadNotifications() -> Int {
        return 3
    }
    
    func isEnableCreditPicPay() -> Bool {
        return isEnableCreditAux
    }
    
    func isCardSettingsEnable(for account: CPAccount?) -> Bool {
        return isCardSettingsEnableAux
    }
    
    func isPhysicalCardEnable(for account: CPAccount) -> Bool {
        return isPhysicalCardEnable
    }
    
    func isVirtualCardEnable(for account: CPAccount) -> Bool {
        return isVirtualCardEnable
    }
    
    func isCardCreditSettingsEnabled(for account: CPAccount?) -> Bool {
        isCardCreditSettingsEnabled
    }
    
    func statusUpgradeAccount(onCacheLoaded: @escaping ((UpgradeAccountStatus) -> Void), completion: @escaping ((PicPayResult<UpgradeAccountStatus>) -> Void)) {
        var result: UpgradeAccountStatus
        switch statusUpgrade {
        case .disabled:
            result = try! MockCodable<UpgradeAccountStatus>().loadCodableObject(resource: "upgradeAccountDisable", typeDecoder: .useDefaultKeys)
        case .analysis:
           result = try! MockCodable<UpgradeAccountStatus>().loadCodableObject(resource: "upgradeAccountAnalysis", typeDecoder: .useDefaultKeys)
        case .approved:
            result = try! MockCodable<UpgradeAccountStatus>().loadCodableObject(resource: "upgradeAccountApproved", typeDecoder: .useDefaultKeys)
        case .pending:
            result = try! MockCodable<UpgradeAccountStatus>().loadCodableObject(resource: "upgradeAccountPending", typeDecoder: .useDefaultKeys)
        case .rejected:
            result = try! MockCodable<UpgradeAccountStatus>().loadCodableObject(resource: "upgradeAccountRejected", typeDecoder: .useDefaultKeys)
        case .notStarted:
            result = try! MockCodable<UpgradeAccountStatus>().loadCodableObject(resource: "upgradeAccountNotStarted", typeDecoder: .useDefaultKeys)
        }
        
        completion(PicPayResult<UpgradeAccountStatus>.success(result))
    }
    
    func loadLoanInfoSettings(_ completion: @escaping LoanInfoSettingsCompletionBlock) {
        completion(.success(successLoanResponse))
    }
    
    func loadRegistrationRenewalSettings(_ completion: @escaping RegistrationRenewalSettingsCompletionBlock) {
        guard renewalSettingsSuccess else {
            let error = PicPayError(message: "Error")
            completion(.failure(error))
            return
        }
        
        let model = RegistrationRenewalSettings(subtitle: "Subtitle", type: .alert)
        completion(.success(model))
    }
    
    func checkStudentAccountStatus() -> Bool {
        return checkStudentAccountStatusAux
    }
    
    func fetchP2PLendingSectionSettings(completion: @escaping SettingsCompletionBlock<P2PLendingSectionSettings, ApiError>) {
        completion(fetchP2PLendingSectionSettingsExpectedResult)
    }
    
    enum AccountStatus {
        case disabled
        case analysis
        case approved
        case pending
        case rejected
        case notStarted
    }
}

class IdentityValidationStatusServicingTest: IdentityValidationStatusServicing {
    private let status: IVIdentityValidationStatus.Status
    
    init(status: IVIdentityValidationStatus.Status){
        self.status = status
    }
    
    func status(_ completion: @escaping ((PicPayResult<IVIdentityValidationStatus>) -> Void)) {
        let mockJson = MockJSON()
        let json = mockJson.load(resource: status == .disabled ? "identityDisabled" : "IdentityVerified")
        let identity = IVIdentityValidationStatus(json: json)
        completion(PicPayResult.success(identity))
    }
}

class SettingsViewModelTest: XCTestCase {
    typealias Dependencies = HasKVStore & HasFeatureManager
    private lazy var featureManagerMock = FeatureManagerMock()
    private lazy var kvStoreMock = KVStoreMock()
    private lazy var container = DependencyContainerMock(featureManagerMock, kvStoreMock)

    func createViewModel(
        consumer: MBConsumer = MBConsumer(),
        valueBiometric: Int = 0,
        isActive: Bool = true, typeRemote: FeatureConfig? = nil,
        canEvaluateDeviceSecurity: Bool = true,
        identityStatus: IVIdentityValidationStatus.Status = .verified,
        isEnableCredit: Bool = true,
        checkStudentAccountStatus: Bool = true,
        isCardSettingsEnable: Bool = true,
        isVirtualCardEnable: Bool = true,
        statusUpgrade: SettingsServiceTest.AccountStatus = .approved,
        renewalSettingsSuccess: Bool = true,
        isRegistrationRenewalEnabledMock: Bool = true,
        fetchP2PLendingSectionSettingsExpectedResult: Result<P2PLendingSectionSettings, ApiError> = .failure(.serverError)
    ) -> SettingsViewModel {
        let worker = SettingsServiceTest(
            valueBiometric: valueBiometric,
            isActive: isActive,
            typeRemote: typeRemote,
            canEvaluateDeviceSecurity: canEvaluateDeviceSecurity,
            isEnableCredit: isEnableCredit,
            isVirtualCardEnable: isVirtualCardEnable, checkStudentAccountStatus: checkStudentAccountStatus,
            statusUpgrade: statusUpgrade,
            isCardSettingsEnable: isCardSettingsEnable,
            renewalSettingsSuccess: renewalSettingsSuccess,
            isRegistrationRenewalEnabledMock: isRegistrationRenewalEnabledMock,
            fetchP2PLendingSectionSettingsExpectedResult: fetchP2PLendingSectionSettingsExpectedResult,
            dependencies: DependencyContainerMock(featureManagerMock, kvStoreMock)
        )

        if let config = typeRemote {
            featureManagerMock.override(key: config, with: isActive)
        }
        featureManagerMock.override(key: .releaseP2PLending, with: true)
        let viewModel = SettingsViewModel(worker: ConsumerWorkerTest(consumer: consumer), settingsService: worker, identityWorker: IdentityValidationStatusServicingTest(status: identityStatus), dependencies: DependencyContainerMock(featureManagerMock, kvStoreMock))
        viewModel.isLoanSettingsActive = true
        return viewModel
    }
    
    private func arrayTitlesRow(viewModel: SettingsViewModel) -> [String] {
        var titles: [String] = []
        let sections = viewModel.numberOfSections()
        for section in 0..<sections {
            let rows = viewModel.numberOfRows(section: section)
            for row in 0..<rows {
                let title = viewModel.title(section: section, index: row)
                titles.append(title)
            }
        }
        
        return titles
    }
    
    private func arraySubtitlesRow(viewModel: SettingsViewModel) -> [String] {
        var subtitles: [String] = []
        let sections = viewModel.numberOfSections()
        for section in 0..<sections {
            let rows = viewModel.numberOfRows(section: section)
            for row in 0..<rows {
                let subtitle = viewModel.subtitle(section: section, index: row)
                subtitles.append(subtitle)
            }
        }
        
        return subtitles
    }
    
    private func rowDescription(viewModel: SettingsViewModel, title: String) -> String {
        let sections = viewModel.numberOfSections()
        for section in 0..<sections {
            let rows = viewModel.numberOfRows(section: section)
            for row in 0..<rows {
                let titleRow = viewModel.title(section: section, index: row)
                if titleRow == title {
                    return viewModel.subtitle(section: section, index: row)
                }
            }
        }
        
        return "-1"
    }
    
    private func arrayTitlesSection(viewModel: SettingsViewModel) -> [String] {
        var titles: [String] = []
        let sections = viewModel.numberOfSections()
        for section in 0..<sections {
            let title = viewModel.title(section: section)
            titles.append(title)
        }
        
        return titles
    }
    
    func testInstantiateAccountGivenIdentityStatusEnabled() {
        let consumer = MBConsumer()
        let viewModel = createViewModel(consumer: consumer, isActive: true, typeRemote: .identityVerification, identityStatus: .verified)
        let expectation = self.expectation(description: "API")
        
        viewModel.loadIdentityVerificationStatus { (_) in
            viewModel.instantiateSections()
            let titles = self.arrayTitlesRow(viewModel: viewModel)
            let result = titles.contains(SettingsStrings.rowIdentity.text)
            
            expectation.fulfill()
            XCTAssertTrue(result)
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }
   
    func testInstantiateUpgradeAccountDisable() {
        let viewModel = createViewModel(statusUpgrade: .disabled)
        let expectation = self.expectation(description: "API")
        
        viewModel.loadUpgradeAccountStatus(onCacheLoaded: {
            XCTFail()
        }, onSucess: {
            let titles = self.arrayTitlesRow(viewModel: viewModel)
            let result = titles.contains(SettingsStrings.rowUpgrade.text)
            
            expectation.fulfill()
            XCTAssertFalse(result)
        })
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testInstantiateUpgradeAccountStatusAnalysis() {
        let viewModel = createViewModel(statusUpgrade: .analysis)
        let expectation = self.expectation(description: "API")
        
        viewModel.loadUpgradeAccountStatus(onCacheLoaded: {
            XCTFail()
        }, onSucess: {
            viewModel.instantiateSections()
            let subtitle = self.rowDescription(viewModel: viewModel, title: SettingsStrings.rowUpgrade.text)
            
            expectation.fulfill()
            XCTAssertEqual(subtitle, "Em Analise")
        })
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testInstantiateUpgradeAccountStatusApproved() {
        let viewModel = createViewModel(statusUpgrade: .approved)
        let expectation = self.expectation(description: "API")
        
        viewModel.loadUpgradeAccountStatus(onCacheLoaded: {
            XCTFail()
        }, onSucess: {
            viewModel.instantiateSections()
            let subtitle = self.rowDescription(viewModel: viewModel, title: SettingsStrings.rowUpgrade.text)
            
            expectation.fulfill()
            XCTAssertEqual(subtitle, "Aprovado")
        })
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testInstantiateUpgradeAccountStatusPending() {
        let viewModel = createViewModel(statusUpgrade: .pending)
        let expectation = self.expectation(description: "API")
        
        viewModel.loadUpgradeAccountStatus(onCacheLoaded: {
            XCTFail()
        }, onSucess: {
            viewModel.instantiateSections()
            let subtitle = self.rowDescription(viewModel: viewModel, title: SettingsStrings.rowUpgrade.text)
            
            expectation.fulfill()
            XCTAssertEqual(subtitle, "Pendente")
        })
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testInstantiateUpgradeAccountStatusRejected() {
        let viewModel = createViewModel(statusUpgrade: .rejected)
        let expectation = self.expectation(description: "API")
        
        viewModel.loadUpgradeAccountStatus(onCacheLoaded: {
            XCTFail()
        }, onSucess: {
            viewModel.instantiateSections()
            let subtitle = self.rowDescription(viewModel: viewModel, title: SettingsStrings.rowUpgrade.text)
            
            expectation.fulfill()
            XCTAssertEqual(subtitle, "Rejeitado")
        })
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testInstantiateUpgradeAccountStatusNotStarted() {
        let viewModel = createViewModel(statusUpgrade: .notStarted)
        let expectation = self.expectation(description: "API")
        
        viewModel.loadUpgradeAccountStatus(onCacheLoaded: {
            XCTFail()
        }, onSucess: {
            viewModel.instantiateSections()
            let subtitle = self.rowDescription(viewModel: viewModel, title: SettingsStrings.rowUpgrade.text)
            
            expectation.fulfill()
            XCTAssertTrue(subtitle.isEmpty)
        })
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testInstantiateAccountGivenIdentityStatusDisable() {
        let consumer = MBConsumer()
        let viewModel = createViewModel(consumer: consumer, isActive: true, typeRemote: .identityVerification, identityStatus: .disabled)
        let expectation = self.expectation(description: "API")
        
        viewModel.loadIdentityVerificationStatus { (_) in
            viewModel.instantiateSections()
            let titles = self.arrayTitlesRow(viewModel: viewModel)
            let result = titles.contains(SettingsStrings.rowIdentity.text)
            
            expectation.fulfill()
            XCTAssertFalse(result)
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testInstantiateAccountGivenIdentityVerificationEnabled() {
        let consumer = MBConsumer()
        let viewModel = createViewModel(consumer: consumer, isActive: true, typeRemote: .identityVerification)
        let expectation = self.expectation(description: "API")
        
        viewModel.loadIdentityVerificationStatus { (_) in
            viewModel.instantiateSections()
            let titles = self.arrayTitlesRow(viewModel: viewModel)
            let result = titles.contains(SettingsStrings.rowIdentity.text)
            
            expectation.fulfill()
            XCTAssertTrue(result)
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testInstantiateAccountGivenIdentityVerificationDisable() {
        let consumer = MBConsumer()

        let viewModel = createViewModel(consumer: consumer, isActive: false, typeRemote: .identityVerification)
        
        let expectation = self.expectation(description: "API")
        
        viewModel.loadIdentityVerificationStatus { (_) in
            viewModel.instantiateSections()
            let titles = self.arrayTitlesRow(viewModel: viewModel)
            let result = titles.contains(SettingsStrings.rowIdentity.text)
            
            expectation.fulfill()
            XCTAssertFalse(result)
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testInstantiateAccountGivenTPSEnable() {
        let consumer = MBConsumer()
        
        let viewModel = createViewModel(consumer: consumer, isActive: true, typeRemote: .tPS)
        
        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowLinkedAccounts.text)
        
        XCTAssertTrue(result)
    }
    
    func testInstantiateAccountGivenTPSDisable() {
        let consumer = MBConsumer()
        
        let viewModel = createViewModel(consumer: consumer, isActive: false, typeRemote: .tPS)
        
        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowLinkedAccounts.text)
        
        XCTAssertFalse(result)
    }
    
    func testInstantiateAccountGivenAddressEnable() {
        let consumer = MBConsumer()
        
        let viewModel = createViewModel(consumer: consumer, isActive: true, typeRemote: .address)
        
        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowMyAddress.text)
        
        XCTAssertTrue(result)
    }
    
    func testInstantiateAccountGivenAddressDisable() {
        let consumer = MBConsumer()
  
        let viewModel = createViewModel(consumer: consumer, isActive: false, typeRemote: .address)
        
        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowMyAddress.text)
        
        XCTAssertFalse(result)
    }
    
    func testInstantiateAccountGivenBankAccountNil() {
        let consumer = MBConsumer()
        
        let viewModel = createViewModel(consumer: consumer)
        
        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowBankAccount.text)
        
        XCTAssertTrue(result)
    }
    
    func testInstantiateAccountGivenBankAccountNotNil() {
        let consumer = MBConsumer()
        consumer.bankAccount = PPBankAccount()
        
        let viewModel = createViewModel(consumer: consumer)
        
        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowBankAccount.text)
        
        XCTAssertTrue(result)
    }
    
    func testInstantiateSections_WhenIncomeTaxReturnIsEnabled_ShouldInstantiateIncomeTaxReturnItem() {
        let viewModel = createViewModel(consumer: MBConsumer(), isActive: true, typeRemote: .isIncomeStatementsAvailable)
        
        viewModel.instantiateSections()
        
        let titles = arrayTitlesRow(viewModel: viewModel)
        XCTAssertTrue(titles.contains(SettingsStrings.incomeStatements.text))
    }
    
    func testInstantiateSections_WhenIncomeTaxReturnIsDisabled_ShouldNotInstantiateIncomeTaxReturnItem() {
        let viewModel = createViewModel(consumer: MBConsumer(), isActive: false, typeRemote: .isIncomeStatementsAvailable)
        
        viewModel.instantiateSections()
        
        let titles = arrayTitlesRow(viewModel: viewModel)
        XCTAssertFalse(titles.contains(SettingsStrings.incomeStatements.text))
    }
    
    func testInstantiateAccountGivenVerifiedPhoneNumberNil() {
        let consumer = MBConsumer()
        
        let viewModel = createViewModel(consumer: consumer)
        
        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowMyNumber.text)
        
        XCTAssertTrue(result)
    }
    
    func testInstantiateAccountGivenVerifiedPhoneNumberNotNil() {
        let consumer = MBConsumer()
        consumer.verifiedPhoneNumber = "27997053363"
        
        let viewModel = createViewModel(consumer: consumer)
        
        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowMyNumber.text)
        
        XCTAssertTrue(result)
    }
    
    
    func testPhoneNumberMaskObfuscation_WhenFlagTrue_ShouldShowPhoneNumberWithMask() {
        let consumer = MBConsumer()
        let expectedResult = "(**) *****-3363"
        consumer.verifiedPhoneNumber = "27997053363"
        
        let viewModel = createViewModel(consumer: consumer, typeRemote: .featureAppSecObfuscationMask)
        viewModel.instantiateSections()
        
        let subtitles = arraySubtitlesRow(viewModel: viewModel)
        let result = subtitles.contains(expectedResult)
        
        XCTAssertTrue(result)
    }
    
    func testPhoneNumberMaskObfuscation_WhenFlagFalse_ShouldNotShowPhoneNumberWithMask() {
        let consumer = MBConsumer()
        let maksResult = "(**) *****-3363"
        consumer.verifiedPhoneNumber = "27997053363"
        
        let viewModel = createViewModel(consumer: consumer, isActive: false, typeRemote: .featureAppSecObfuscationMask)
        viewModel.instantiateSections()
        
        let subtitles = arraySubtitlesRow(viewModel: viewModel)
        let result = subtitles.contains(maksResult)
        
        XCTAssertFalse(result)
    }
    
    func testEmailMaskObfuscation_WhenFlagTrue_ShouldShowEmailWithMask() {
        let consumer = MBConsumer()
        let expectedResult = "t*********l@p****y.com"
        consumer.email = "teste.email@picpay.com"
        
        let viewModel = createViewModel(consumer: consumer, typeRemote: .featureAppSecObfuscationMask)
        viewModel.instantiateSections()
        
        let subtitles = arraySubtitlesRow(viewModel: viewModel)
        let result = subtitles.contains(expectedResult)
        
        XCTAssertTrue(result)
    }
    
    func testEmailMaskObfuscation_WhenEmailNameHasDot_ShouldShowEmailWithMask() {
        let consumer = MBConsumer()
        let expectedResult = "t*********l@p****y.com"
        consumer.email = "teste.email@picpay.com"
        
        let viewModel = createViewModel(consumer: consumer, typeRemote: .featureAppSecObfuscationMask)
        viewModel.instantiateSections()
        
        let subtitles = arraySubtitlesRow(viewModel: viewModel)
        let result = subtitles.contains(expectedResult)
        
        XCTAssertTrue(result)
    }
    
    func testPhoneNumberMaskObfuscation_WhenDomainHasTwoDots_ShouldNotShowEmailWithMask() {
        let consumer = MBConsumer()
        let expectedResult = "t********l@p****y.com.br"
        consumer.email = "testeEmail@picpay.com.br"
        
        let viewModel = createViewModel(consumer: consumer, typeRemote: .featureAppSecObfuscationMask)
        viewModel.instantiateSections()
        
        let subtitles = arraySubtitlesRow(viewModel: viewModel)
        let result = subtitles.contains(expectedResult)
        
        XCTAssertTrue(result)
    }
    
    func testPhoneNumberMaskObfuscation_WhenFlagFalse_ShouldNotShowEmailWithMask() {
        let consumer = MBConsumer()
        let maksResult = "t*********l@p****y.com"
        consumer.email = "teste.email@picpay.com"
        
        let viewModel = createViewModel(consumer: consumer, isActive: false, typeRemote: .featureAppSecObfuscationMask)
        
        viewModel.instantiateSections()
        let subtitles = arraySubtitlesRow(viewModel: viewModel)
        let result = subtitles.contains(maksResult)
        
        XCTAssertFalse(result)
    }
    
    func testInstantiateCreditPicpayGivenCreditPicpayDisable() throws {
        let consumer = MBConsumer()
        let mockJson = MockJSON()
        let accountJson = mockJson.load(resource: "accountCompletedTrue")
        let account = try XCTUnwrap(CPAccount(json: accountJson))
        
        let viewModel = createViewModel(consumer: consumer, isActive: false, isCardSettingsEnable: false)
        viewModel.accountOfCreditPicPay = account
        
        viewModel.instantiateSections()
        let titles = arrayTitlesSection(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.sectionCreditPicpay.text)
        
        XCTAssertFalse(result)
    }
    
    func testInstantiateCreditPicpayGivenCreditPicpayEnable() throws {
        let consumer = MBConsumer()
        let mockJson = MockJSON()
        let accountJson = mockJson.load(resource: "accountCompletedTrue")
        let account = try XCTUnwrap(CPAccount(json: accountJson))
        
        let viewModel = createViewModel(consumer: consumer, isActive: true)
        viewModel.accountOfCreditPicPay = account
        
        viewModel.instantiateSections()
        let titles = arrayTitlesSection(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.sectionCreditPicpay.text)
        
        XCTAssertTrue(result)
    }
    
    func testVirtualCardPicpayGivenEnabledPayloadOptionShows() throws {
        let consumer = MBConsumer()
        let mockJson = MockJSON()
        let accountJson = mockJson.load(resource: "accountCompletedWithVirtualCard")
        let account = try XCTUnwrap(CPAccount(json: accountJson))
        
        let viewModel = createViewModel(consumer: consumer,
                                        isActive: true,
                                        isCardSettingsEnable: true,
                                        isVirtualCardEnable: true)
        viewModel.accountOfCreditPicPay = account
        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowPicPayPro.text)
        
        XCTAssertTrue(result)
    }
    
    func testInstantiateCreditPicpayGivenCreditPicpayLimitAjustmentEnable() throws {
        let consumer = MBConsumer()
        let mockJson = MockJSON()
        let accountJson = mockJson.load(resource: "accountCompletedTrue")
        let account = try XCTUnwrap(CPAccount(json: accountJson))
        
        let viewModel = createViewModel(consumer: consumer, isActive: true)
        viewModel.accountOfCreditPicPay = account
        
        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowCreditLimit.text)
        
        XCTAssertTrue(result)
    }
    
    func testInstantiatePromotionGivenMGBEnable() {
        let consumer = MBConsumer()
        
        let viewModel = createViewModel(consumer: consumer, isActive: true, typeRemote: .mGB)
        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowRecommendStores.text)
        
        XCTAssertTrue(result)
    }
    
    func testInstantiatePromotionGivenMGBDisable() {
        let consumer = MBConsumer()
        
        let viewModel = createViewModel(consumer: consumer, isActive: false, typeRemote: .mGB)
        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowRecommendStores.text)
        
        XCTAssertFalse(result)
    }
    
    func testInstantiatePROGivenBusinessAccountTrue() {
        let consumer = MBConsumer()
        consumer.businessAccount = true
        
        let viewModel = createViewModel(consumer: consumer)
        viewModel.instantiateSections()
        let titles = arrayTitlesSection(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.sectionPro.text)
        
        XCTAssertTrue(result)
    }
    
    func testInstantiatePROGivenBusinessAccountFalse() {
        let consumer = MBConsumer()
        consumer.businessAccount = false
        
        let viewModel = createViewModel(consumer: consumer)
        viewModel.instantiateSections()
        let titles = arrayTitlesSection(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.sectionPro.text)
        
        XCTAssertFalse(result)
    }
    
    
    func testInstantiateBussinessGivenBusinessAccountFalse() {
        let consumer = MBConsumer()
        consumer.businessAccount = false
        
        let viewModel = createViewModel(consumer: consumer)
        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowPicPayPro.text)
        
        XCTAssertTrue(result)
    }
    
    func testInstantiateBussinessGivenBusinessAccountTrue() {
        let consumer = MBConsumer()
        consumer.businessAccount = true
        
        let viewModel = createViewModel(consumer: consumer)
        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowPicPayPro.text)
        
        XCTAssertFalse(result)
    }
    
    func testInstantiateSettingsGivenBiometricNone() {
        let viewModel = createViewModel(valueBiometric: 0)
        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result1 = titles.contains(SettingsStrings.rowFaceId.text)
        let result2 = titles.contains(SettingsStrings.rowTouchId.text)
        
        XCTAssertFalse(result1)
        XCTAssertFalse(result2)
    }
    
    func testInstantiateSettingsGivenBiometricFaceId() {
        let viewModel = createViewModel(valueBiometric: 2)
        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowFaceId.text)
        
        XCTAssertTrue(result)
    }
    
    func testInstantiateSettingsGivenBiometricTouchID() {
        let viewModel = createViewModel(valueBiometric: 1)
        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowTouchId.text)
        
        XCTAssertTrue(result)
    }
    
    func testNumberOfSections_WhenPixKeysIsInactive_ShouldBe9() {
        featureManagerMock.override(key: .releasePixKeyManagementOnSettingsBool, with: false)
        let viewModel = createViewModel()
        viewModel.instantiateSections()
        let result = viewModel.numberOfSections()
        XCTAssertEqual(result, 9)
    }

    func testNumberOfSections_WhenPixKeysIsActive_ShouldBe10() {
        featureManagerMock.override(key: .releasePixKeyManagementOnSettingsBool, with: true)
        let viewModel = createViewModel()
        viewModel.instantiateSections()
        let result = viewModel.numberOfSections()
        XCTAssertEqual(result, 10)
    }
    
    func testNumberOfSections_WhenStudentAccountIsActiveAndPixKeysInactive_ShouldBe10() {
        featureManagerMock.override(key: .releasePixKeyManagementOnSettingsBool, with: false)
        let viewModel = createViewModel()
        viewModel.checkStudentAccountStatus()
        viewModel.instantiateSections()
        let result = viewModel.numberOfSections()
        
        XCTAssertEqual(result, 10)
    }
    
    func testNumberOfSections_WhenStudentAccountAndPixKeysAreInactive_ShouldBe9() {
        featureManagerMock.override(key: .releasePixKeyManagementOnSettingsBool, with: false)
        let viewModel = createViewModel(checkStudentAccountStatus: false)
        viewModel.checkStudentAccountStatus()
        viewModel.instantiateSections()
        let result = viewModel.numberOfSections()
        
        XCTAssertEqual(result, 9)
    }
    
    func testRowBetaTestIsEnable() {
        let viewModel = createViewModel(isActive: true, typeRemote: .featureJoinBeta)
        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowBeta.text)
        
        XCTAssertTrue(result)
    }
    
    func testRowBetaTestIsDisable() {
        let viewModel = createViewModel(isActive: false, typeRemote: .featureJoinBeta)
        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowBeta.text)
        
        XCTAssertFalse(result)
    }
    
    func testLoadRegistrationRenewalStatus_WhenRenewalSettingsSuccessAndRemoteTrue_ShouldCallCompletionAndAddRow() {
        let viewModel = createViewModel(
            checkStudentAccountStatus: false,
            renewalSettingsSuccess: true,
            isRegistrationRenewalEnabledMock: true
        )
        let expectation = XCTestExpectation(description: "completion loadRegistrationRenewalStatus")
        
        viewModel.instantiateSections()
        let before = viewModel.numberOfRows(section: 0)
        
        viewModel.loadRegistrationRenewalStatus {
            viewModel.instantiateSections()
            let after = viewModel.numberOfRows(section: 0)
            XCTAssertEqual(after, before + 1)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 3.0)
    }
    
    func testLoadRegistrationRenewalStatus_WhenRenewalSettingsSuccessAndRemoteFalse_ShouldCallCompletionAndAddRow() {
        let viewModel = createViewModel(
            checkStudentAccountStatus: false,
            renewalSettingsSuccess: true,
            isRegistrationRenewalEnabledMock: false
        )
        let expectation = XCTestExpectation(description: "completion loadRegistrationRenewalStatus")
        
        viewModel.instantiateSections()
        let before = viewModel.numberOfRows(section: 0)
        
        viewModel.loadRegistrationRenewalStatus {
            viewModel.instantiateSections()
            let after = viewModel.numberOfRows(section: 0)
            XCTAssertEqual(after, before)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 3.0)
    }
    
    func testLoadRegistrationRenewalStatus_WhenRenewalSettingsFailure_ShouldCallCompletionAndNotAddRow() {
        let viewModel = createViewModel(
            checkStudentAccountStatus: false,
            renewalSettingsSuccess: false,
            isRegistrationRenewalEnabledMock: true
        )
        let expectation = XCTestExpectation(description: "completion loadRegistrationRenewalStatus")
        
        viewModel.instantiateSections()
        let before = viewModel.numberOfRows(section: 0)
        
        viewModel.loadRegistrationRenewalStatus {
            viewModel.instantiateSections()
            let after = viewModel.numberOfRows(section: 0)
            XCTAssertEqual(after, before)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 3.0)
    }
    
    func testFetchP2PLendingSectionSettings_WhenReceiveSuccessFromService_ShouldCallCompletionAndAddSection() {
        let sectionRows = P2PLendingRowsSettings(myOffers: true, myInvestments: false, help: false)
        let sectionSettings = P2PLendingSectionSettings(enable: true, options: sectionRows)
        let viewModel = createViewModel(fetchP2PLendingSectionSettingsExpectedResult: .success(sectionSettings))
        let expectation = XCTestExpectation(description: "completion fetchP2PLendingSectionSettings")
        
        viewModel.instantiateSections()
        let expectedNumberOfSections = viewModel.numberOfSections() + 1
        
        viewModel.fetchP2PLendingSectionSettings {
            viewModel.instantiateSections()
            XCTAssertEqual(expectedNumberOfSections, viewModel.numberOfSections())
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testFetchP2PLendingSectionSettings_WhenReceiveFailureFromService_ShouldNotCallCompletion() {
        let viewModel = createViewModel(fetchP2PLendingSectionSettingsExpectedResult: .failure(.serverError))
        let expectation = XCTestExpectation(description: "completion fetchP2PLendingSectionSettings")
        expectation.isInverted = true
        
        viewModel.instantiateSections()
        viewModel.fetchP2PLendingSectionSettings {
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testInstantiateSections_WhenPixKeysAndDailyLimitFlagAreDisabled_ShouldSectionNotContainPixSection() {
        featureManagerMock.override(key: .releasePixKeyManagementOnSettingsBool, with: false)
        featureManagerMock.override(key: .isPixDailyLimitAvailable, with: false)
        let viewModel = createViewModel()
        viewModel.instantiateSections()
        let sectionsTitles = arrayTitlesSection(viewModel: viewModel)
        let result = sectionsTitles.contains(SettingsStrings.sectionPix.text)
        
        XCTAssertFalse(result)
    }
    
    func testInstantiateSections_WhenPixKeysAndDailyLimitFlagAreEnabled_ShouldSectionContainAllPixRow() {
        featureManagerMock.override(key: .releasePixKeyManagementOnSettingsBool, with: true)
        featureManagerMock.override(key: .isPixDailyLimitAvailable, with: true)
        let viewModel = createViewModel()
        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowMyKeys.text)
            && titles.contains(SettingsStrings.rowMyDailyLimit.text)

        XCTAssertTrue(result)
    }

    func testInstantiateSections_WhenAppProtectionFlagIsInactive_ShouldNotDisplayProtectAccessRow() {
        featureManagerMock.override(keys: .releaseAppProtectionShowBool, with: false)
        let viewModel = createViewModel()

        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowProtectAccess.text)

        XCTAssertFalse(result)
    }

    func testInstantiateSections_WhenAppProtectionFlagIsActive_WhenCanEvaluateBiometryAndAppProtectionIsTrue_ShouldDisplayProtectAccessRow() {
        featureManagerMock.override(keys: .releaseAppProtectionShowBool, with: true)
        kvStoreMock.setBool(true, with: AppProtectionKey.userDeviceProtectedValue)
        let viewModel = createViewModel(canEvaluateDeviceSecurity: true)

        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowProtectAccess.text)

        XCTAssertTrue(result)
    }

    func testInstantiateSections_WhenAppProtectionFlagIsActive_WhenCannotEvaluateBiometryAndAppProtectionIsTrue_ShouldDisplayProtectAccessRow() {
        featureManagerMock.override(keys: .releaseAppProtectionShowBool, with: true)
        kvStoreMock.setBool(true, with: AppProtectionKey.userDeviceProtectedValue)
        let viewModel = createViewModel(canEvaluateDeviceSecurity: false)

        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowProtectAccess.text)

        XCTAssertTrue(result)
    }

    func testInstantiateSections_WhenAppProtectionFlagIsActive_WhenCanEvaluateBiometryAndAppProtectionIsFalse_ShouldDisplayProtectAccessRow() {
        featureManagerMock.override(keys: .releaseAppProtectionShowBool, with: true)
        kvStoreMock.setBool(false, with: AppProtectionKey.userDeviceProtectedValue)
        let viewModel = createViewModel(canEvaluateDeviceSecurity: true)

        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowProtectAccess.text)

        XCTAssertTrue(result)
    }

    func testInstantiateSections_WhenAppProtectionFlagIsActive_WhenCannotEvaluateBiometryAndAppProtectionIsFalse_ShouldNotDisplayProtectAccessRow() {
        featureManagerMock.override(keys: .releaseAppProtectionShowBool, with: true)
        kvStoreMock.setBool(false, with: AppProtectionKey.userDeviceProtectedValue)
        let viewModel = createViewModel(canEvaluateDeviceSecurity: false)

        viewModel.instantiateSections()
        let titles = arrayTitlesRow(viewModel: viewModel)
        let result = titles.contains(SettingsStrings.rowProtectAccess.text)

        XCTAssertFalse(result)
    }
}
