import XCTest
import PermissionsKit
@testable import PicPay
import UI

final class PrivacyPermissionsHelperTests: XCTestCase {
    private lazy var sut = PrivacyPermissionsHelper()

    // MARK: - Contacts Text & Color

    func testContactsStateTextAndColor_WhenContactStatusIsAuthorized_ShouldBeActivated() {
        sut.contactStatus = .authorized
        sut.updateContactsState()

        XCTAssertEqual(sut.contactsStateText, .permissionActivated)
        XCTAssertEqual(sut.contactsStateColor.cgColor, Colors.success900.color.cgColor)
    }

    func testContactsStateTextAndColor_WhenContactStatusIsDenied_ShouldBeDeactivated() {
        sut.contactStatus = .denied
        sut.updateContactsState()

        XCTAssertEqual(sut.contactsStateText, .permissionDeactivated)
        XCTAssertEqual(sut.contactsStateColor.cgColor, Colors.critical900.color.cgColor)
    }

    func testContactsStateTextAndColor_WhenContactStatusIsNotDetermined_ShouldBeDeactivated() {
        sut.contactStatus = .notDetermined
        sut.updateContactsState()

        XCTAssertEqual(sut.contactsStateText, .permissionDeactivated)
        XCTAssertEqual(sut.contactsStateColor.cgColor, Colors.critical900.color.cgColor)
    }

    func testContactsStateTextAndColor_WhenContactStatusIsDisabled_ShouldBeDeactivated() {
        sut.contactStatus = .disabled
        sut.updateContactsState()

        XCTAssertEqual(sut.contactsStateText, .permissionDeactivated)
        XCTAssertEqual(sut.contactsStateColor.cgColor, Colors.critical900.color.cgColor)
    }

    // MARK: - Location Text & Color

    func testLocationStateTextAndColor_WhenLocationAlwaysIsAuthorized_WhenLocationWhenInUseIsAuthorized_ShouldBeActivated() {
        sut.locationAlwaysStatus = .authorized
        sut.locationWhenInUseStatus = .authorized
        sut.updateLocationState()

        XCTAssertEqual(sut.locationStateText, .permissionActivated)
        XCTAssertEqual(sut.locationStateColor.cgColor, Colors.success900.color.cgColor)
    }

    func testLocationStateTextAndColor_WhenLocationAlwaysIsAuthorized_WhenLocationWhenInUseIsDenied_ShouldBeActivated() {
        sut.locationAlwaysStatus = .authorized
        sut.locationWhenInUseStatus = .denied
        sut.updateLocationState()

        XCTAssertEqual(sut.locationStateText, .permissionActivated)
        XCTAssertEqual(sut.locationStateColor.cgColor, Colors.success900.color.cgColor)
    }

    func testLocationStateTextAndColor_WhenLocationAlwaysIsAuthorized_WhenLocationWhenInUseIsNotDetermined_ShouldBeActivated() {
        sut.locationAlwaysStatus = .authorized
        sut.locationWhenInUseStatus = .notDetermined
        sut.updateLocationState()

        XCTAssertEqual(sut.locationStateText, .permissionActivated)
        XCTAssertEqual(sut.locationStateColor.cgColor, Colors.success900.color.cgColor)
    }

    func testLocationStateTextAndColor_WhenLocationAlwaysIsAuthorized_WhenLocationWhenInUseIsDisabled_ShouldBeActivated() {
        sut.locationAlwaysStatus = .authorized
        sut.locationWhenInUseStatus = .disabled
        sut.updateLocationState()

        XCTAssertEqual(sut.locationStateText, .permissionActivated)
        XCTAssertEqual(sut.locationStateColor.cgColor, Colors.success900.color.cgColor)
    }

    func testLocationStateTextAndColor_WhenLocationAlwaysIsDenied_WhenLocationWhenInUseIsAuthorized_ShouldBeActivated() {
        sut.locationAlwaysStatus = .denied
        sut.locationWhenInUseStatus = .authorized
        sut.updateLocationState()

        XCTAssertEqual(sut.locationStateText, .permissionActivated)
        XCTAssertEqual(sut.locationStateColor.cgColor, Colors.success900.color.cgColor)
    }

    func testLocationStateTextAndColor_WhenLocationAlwaysIsDenied_WhenLocationWhenInUseIsDenied_ShouldBeDeactivated() {
        sut.locationAlwaysStatus = .denied
        sut.locationWhenInUseStatus = .denied
        sut.updateLocationState()

        XCTAssertEqual(sut.contactsStateText, .permissionDeactivated)
        XCTAssertEqual(sut.contactsStateColor.cgColor, Colors.critical900.color.cgColor)
    }

    func testLocationStateTextAndColor_WhenLocationAlwaysIsDenied_WhenLocationWhenInUseIsNotDetermined_ShouldBeDeactivated() {
        sut.locationAlwaysStatus = .denied
        sut.locationWhenInUseStatus = .notDetermined
        sut.updateLocationState()

        XCTAssertEqual(sut.contactsStateText, .permissionDeactivated)
        XCTAssertEqual(sut.contactsStateColor.cgColor, Colors.critical900.color.cgColor)
    }

    func testLocationStateTextAndColor_WhenLocationAlwaysIsDenied_WhenLocationWhenInUseIsDisabled_ShouldBeDeactivated() {
        sut.locationAlwaysStatus = .denied
        sut.locationWhenInUseStatus = .disabled
        sut.updateLocationState()

        XCTAssertEqual(sut.contactsStateText, .permissionDeactivated)
        XCTAssertEqual(sut.contactsStateColor.cgColor, Colors.critical900.color.cgColor)
    }

    func testLocationStateTextAndColor_WhenLocationAlwaysIsNotDetermined_WhenLocationWhenInUseIsAuthorized_ShouldBeActivated() {
        sut.locationAlwaysStatus = .notDetermined
        sut.locationWhenInUseStatus = .authorized
        sut.updateLocationState()

        XCTAssertEqual(sut.locationStateText, .permissionActivated)
        XCTAssertEqual(sut.locationStateColor.cgColor, Colors.success900.color.cgColor)
    }

    func testLocationStateTextAndColor_WhenLocationAlwaysIsNotDetermined_WhenLocationWhenInUseIsDenied_ShouldBeDeactivated() {
        sut.locationAlwaysStatus = .notDetermined
        sut.locationWhenInUseStatus = .denied
        sut.updateLocationState()

        XCTAssertEqual(sut.contactsStateText, .permissionDeactivated)
        XCTAssertEqual(sut.contactsStateColor.cgColor, Colors.critical900.color.cgColor)
    }

    func testLocationStateTextAndColor_WhenLocationAlwaysIsNotDetermined_WhenLocationWhenInUseIsNotDetermined_ShouldBeDeactivated() {
        sut.locationAlwaysStatus = .notDetermined
        sut.locationWhenInUseStatus = .notDetermined
        sut.updateLocationState()

        XCTAssertEqual(sut.contactsStateText, .permissionDeactivated)
        XCTAssertEqual(sut.contactsStateColor.cgColor, Colors.critical900.color.cgColor)
    }

    func testLocationStateTextAndColor_WhenLocationAlwaysIsNotDetermined_WhenLocationWhenInUseIsDisabled_ShouldBeDeactivated() {
        sut.locationAlwaysStatus = .notDetermined
        sut.locationWhenInUseStatus = .disabled
        sut.updateLocationState()

        XCTAssertEqual(sut.contactsStateText, .permissionDeactivated)
        XCTAssertEqual(sut.contactsStateColor.cgColor, Colors.critical900.color.cgColor)
    }

    func testLocationStateTextAndColor_WhenLocationAlwaysIsDisabled_WhenLocationWhenInUseIsAuthorized_ShouldBeActivated() {
        sut.locationAlwaysStatus = .disabled
        sut.locationWhenInUseStatus = .authorized
        sut.updateLocationState()

        XCTAssertEqual(sut.locationStateText, .permissionActivated)
        XCTAssertEqual(sut.locationStateColor.cgColor, Colors.success900.color.cgColor)
    }

    func testLocationStateTextAndColor_WhenLocationAlwaysIsDisabled_WhenLocationWhenInUseIsDenied_ShouldBeDeactivated() {
        sut.locationAlwaysStatus = .disabled
        sut.locationWhenInUseStatus = .denied
        sut.updateLocationState()

        XCTAssertEqual(sut.contactsStateText, .permissionDeactivated)
        XCTAssertEqual(sut.contactsStateColor.cgColor, Colors.critical900.color.cgColor)
    }

    func testLocationStateTextAndColor_WhenLocationAlwaysIsDisabled_WhenLocationWhenInUseIsNotDetermined_ShouldBeDeactivated() {
        sut.locationAlwaysStatus = .disabled
        sut.locationWhenInUseStatus = .notDetermined
        sut.updateLocationState()

        XCTAssertEqual(sut.contactsStateText, .permissionDeactivated)
        XCTAssertEqual(sut.contactsStateColor.cgColor, Colors.critical900.color.cgColor)
    }

    func testLocationStateTextAndColor_WhenLocationAlwaysIsDisabled_WhenLocationWhenInUseIsDisabled_ShouldBeDeactivated() {
        sut.locationAlwaysStatus = .disabled
        sut.locationWhenInUseStatus = .disabled
        sut.updateLocationState()

        XCTAssertEqual(sut.contactsStateText, .permissionDeactivated)
        XCTAssertEqual(sut.contactsStateColor.cgColor, Colors.critical900.color.cgColor)
    }

    // MARK: - Should show Contacts settings

    func testContactSettings_WhenContactsStatusIsAuthorized_ShouldShow() {
        sut.contactStatus = .authorized

        XCTAssertTrue(sut.shouldShowContactSettings)
    }

    func testContactSettings_WhenContactsStatusIsDenied_ShouldShow() {
        sut.contactStatus = .denied

        XCTAssertTrue(sut.shouldShowContactSettings)
    }

    func testContactSettings_WhenContactsStatusIsNotDetermined_ShouldNotShow() {
        sut.contactStatus = .notDetermined

        XCTAssertFalse(sut.shouldShowContactSettings)
    }

    func testContactSettings_WhenContactsStatusIsDisabled_ShouldShow() {
        sut.contactStatus = .disabled

        XCTAssertTrue(sut.shouldShowContactSettings)
    }

    // MARK: - Should show Location settings

    func testLocationSettings_WhenLocationAlwaysIsAuthorized_WhenLocationWhenInUseIsAuthorized_ShouldShow() {
        sut.locationAlwaysStatus = .authorized
        sut.locationWhenInUseStatus = .authorized

        XCTAssertTrue(sut.shouldShowLocationSettings)
    }

    func testLocationSettings_WhenLocationAlwaysIsAuthorized_WhenLocationWhenInUseIsDenied_ShouldShow() {
        sut.locationAlwaysStatus = .authorized
        sut.locationWhenInUseStatus = .denied

        XCTAssertTrue(sut.shouldShowLocationSettings)
    }

    func testLocationSettings_WhenLocationAlwaysIsAuthorized_WhenLocationWhenInUseIsNotDetermined_ShouldShow() {
        sut.locationAlwaysStatus = .authorized
        sut.locationWhenInUseStatus = .notDetermined

        XCTAssertTrue(sut.shouldShowLocationSettings)
    }

    func testLocationSettings_WhenLocationAlwaysIsAuthorized_WhenLocationWhenInUseIsDisabled_ShouldShow() {
        sut.locationAlwaysStatus = .authorized
        sut.locationWhenInUseStatus = .disabled

        XCTAssertTrue(sut.shouldShowLocationSettings)
    }

    func testLocationSettings_WhenLocationAlwaysIsDenied_WhenLocationWhenInUseIsAuthorized_ShouldShow() {
        sut.locationAlwaysStatus = .denied
        sut.locationWhenInUseStatus = .authorized

        XCTAssertTrue(sut.shouldShowLocationSettings)
    }

    func testLocationSettings_WhenLocationAlwaysIsDenied_WhenLocationWhenInUseIsDenied_ShouldShow() {
        sut.locationAlwaysStatus = .denied
        sut.locationWhenInUseStatus = .denied

        XCTAssertTrue(sut.shouldShowLocationSettings)
    }

    func testLocationSettings_WhenLocationAlwaysIsDenied_WhenLocationWhenInUseIsNotDetermined_ShouldShow() {
        sut.locationAlwaysStatus = .denied
        sut.locationWhenInUseStatus = .notDetermined

        XCTAssertTrue(sut.shouldShowLocationSettings)
    }

    func testLocationSettings_WhenLocationAlwaysIsDenied_WhenLocationWhenInUseIsDisabled_ShouldShow() {
        sut.locationAlwaysStatus = .denied
        sut.locationWhenInUseStatus = .disabled

        XCTAssertTrue(sut.shouldShowLocationSettings)
    }

    func testLocationSettings_WhenLocationAlwaysIsNotDetermined_WhenLocationWhenInUseIsAuthorized_ShouldShow() {
        sut.locationAlwaysStatus = .notDetermined
        sut.locationWhenInUseStatus = .authorized

        XCTAssertTrue(sut.shouldShowLocationSettings)
    }

    func testLocationSettings_WhenLocationAlwaysIsNotDetermined_WhenLocationWhenInUseIsDenied_ShouldShow() {
        sut.locationAlwaysStatus = .notDetermined
        sut.locationWhenInUseStatus = .denied

        XCTAssertTrue(sut.shouldShowLocationSettings)
    }

    func testLocationSettings_WhenLocationAlwaysIsNotDetermined_WhenLocationWhenInUseIsNotDetermined_ShouldNotShow() {
        sut.locationAlwaysStatus = .notDetermined
        sut.locationWhenInUseStatus = .notDetermined

        XCTAssertFalse(sut.shouldShowLocationSettings)
    }

    func testLocationSettings_WhenLocationAlwaysIsNotDetermined_WhenLocationWhenInUseIsDisabled_ShouldShow() {
        sut.locationAlwaysStatus = .notDetermined
        sut.locationWhenInUseStatus = .disabled

        XCTAssertTrue(sut.shouldShowLocationSettings)
    }

    func testLocationSettings_WhenLocationAlwaysIsDisabled_WhenLocationWhenInUseIsAuthorized_ShouldShow() {
        sut.locationAlwaysStatus = .disabled
        sut.locationWhenInUseStatus = .authorized

        XCTAssertTrue(sut.shouldShowLocationSettings)
    }

    func testLocationSettings_WhenLocationAlwaysIsDisabled_WhenLocationWhenInUseIsDenied_ShouldShow() {
        sut.locationAlwaysStatus = .disabled
        sut.locationWhenInUseStatus = .denied

        XCTAssertTrue(sut.shouldShowLocationSettings)
    }

    func testLocationSettings_WhenLocationAlwaysIsDisabled_WhenLocationWhenInUseIsNotDetermined_ShouldShow() {
        sut.locationAlwaysStatus = .disabled
        sut.locationWhenInUseStatus = .notDetermined

        XCTAssertTrue(sut.shouldShowLocationSettings)
    }

    func testLocationSettings_WhenLocationAlwaysIsDisabled_WhenLocationWhenInUseIsDisabled_ShouldShow() {
        sut.locationAlwaysStatus = .disabled
        sut.locationWhenInUseStatus = .disabled

        XCTAssertTrue(sut.shouldShowLocationSettings)
    }
}
