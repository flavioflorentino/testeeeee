import AnalyticsModule
import OHHTTPStubs
import FeatureFlag
@testable import PicPay
import XCTest

private class PrivacyPermissionsHelperSpy: PrivacyPermissionsHelperProtocol {
    private(set) var callUpdateContactsStateCount = 0
    private(set) var callUpdateLocationStateCount = 0
    private(set) var callAskForContactsPermissionCount = 0
    private(set) var callAskForLocationPermissionCount = 0

    var contactsStateText: SettingsLocalizable = .accountLimitsTitle
    var contactsStateColor: UIColor = .clear
    var locationStateText: SettingsLocalizable = .accountLimitsTitle
    var locationStateColor: UIColor = .clear

    var shouldShowContactSettings: Bool = false
    var shouldShowLocationSettings: Bool = false

    func updateContactsState() {
        callUpdateContactsStateCount += 1
    }

    func updateLocationState() {
        callUpdateLocationStateCount += 1
    }

    func askForContactsPermission(completion: @escaping () -> Void) {
        callAskForContactsPermissionCount += 1
        completion()
    }

    func askForLocationWhenInUsePermission(completion: @escaping () -> Void) {
        callAskForLocationPermissionCount += 1
        completion()
    }
}

private class ApplicationSpy: UIApplicationContract {
    private(set) var callOpenUrlCount = 0

    func open(
        _ url: URL,
        options: [UIApplication.OpenExternalURLOptionsKey : Any],
        completionHandler completion: ((Bool) -> Void)?
    ) {
        callOpenUrlCount += 1
    }

    func canOpenURL(_ url: URL) -> Bool {
        true
    }
}

private class ToggleSettingsViewControllerDelegateSpy: ToggleSettingsViewControllerDelegate {
    private(set) var callReloadTableViewCellCount = 0
    private(set) var callShowAlertCount = 0

    func reloadTableViewCell(at indexPath: IndexPath) {
        callReloadTableViewCellCount += 1
    }

    func showAlert(withMessage message: String) {
        callShowAlertCount += 1
    }
    
    func showConfirmationPopup(with model: PrivacySettingsPopupViewModel) { }
    
    func showProfileConfirmationPopup(_ yesCompletion: @escaping () -> Void, _ noCompletion: @escaping () -> Void) { }
}

class PrivacySettingsViewModelTest: XCTestCase {
    private let permissionsHelperSpy = PrivacyPermissionsHelperSpy()
    private let applicationSpy = ApplicationSpy()
    private let controllerSpy = ToggleSettingsViewControllerDelegateSpy()
    private lazy var analyticsSpy = AnalyticsSpy()
    private lazy var featureManagerMock = FeatureManagerMock()
    
    private lazy var sut: PrivacySettingsViewModel = {
        let viewModel = PrivacySettingsViewModel(
            application: applicationSpy,
            permissionsHelper: permissionsHelperSpy,
            dependencies: DependencyContainerMock(featureManagerMock, analyticsSpy)
        )
        viewModel.delegate = controllerSpy
        return viewModel
    }()
    
    override func setUp() {
        super.setUp()
        ConsumerManager.shared.consumer?.balance = 0.00
        ConsumerManager.shared.privacyConfig = FeedItemVisibilityInt.friends.rawValue
    }
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func fetchPrivacySettingsWithSuccess() {
        let expectation = XCTestExpectation(description: "response")
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(at: "privacySettings.json")
        }
        
        sut.loadSettings(onSuccess: {
            expectation.fulfill()
        }, onError: { _ in
            XCTFail()
        })
        wait(for: [expectation], timeout: 5.0)
    }
    
    func fetchPrivacySettingsWithFailure() {
        let expectation = XCTestExpectation(description: "response")
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.fail()
        }
        
        sut.loadSettings(onSuccess: {
            XCTFail()
        }, onError: { _ in
            expectation.fulfill()
            
        })
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testNumOfSectionsSuccess() {
        fetchPrivacySettingsWithSuccess()
        let expectedNumOfSections = 6
        XCTAssertEqual(sut.numberOfSections(), expectedNumOfSections)
    }
    
    func testNumOfSectionsApiFailure() {
        fetchPrivacySettingsWithFailure()
        let expectedNumOfSections = 0
        XCTAssertEqual(sut.numberOfSections(), expectedNumOfSections)
    }
    
    func testNumOfRowsAtSectionSuccess() {
        fetchPrivacySettingsWithSuccess()
        let expectedNumOfRowsOnFirstSection = 1
        let expectedNumOfRowsOnSecondSection = 1
        XCTAssertEqual(sut.numberOfRowsAtSection(0), expectedNumOfRowsOnFirstSection)
        XCTAssertEqual(sut.numberOfRowsAtSection(1), expectedNumOfRowsOnSecondSection)
    }
    
    func testNumOfRowsAtSectionApiFailure() {
        fetchPrivacySettingsWithFailure()
        let expectedNumOfRows = 0
        XCTAssertEqual(sut.numberOfRowsAtSection(0), expectedNumOfRows)
        XCTAssertEqual(sut.numberOfRowsAtSection(1), expectedNumOfRows)
    }
    
    func testNumOfRowsAtSectionWrongIndexFailure() {
        fetchPrivacySettingsWithFailure()
        let expectedNumOfRows = 0
        XCTAssertEqual(sut.numberOfRowsAtSection(2), expectedNumOfRows)
        XCTAssertEqual(sut.numberOfRowsAtSection(-1), expectedNumOfRows)
    }
    
    func testCellModelSuccess() {
        fetchPrivacySettingsWithSuccess()
        var cellModel = sut.cellModelFor(section: 0, row: 0)
        XCTAssertEqual(cellModel?.label, SettingsLocalizable.openedAccount.text)
        XCTAssertEqual(cellModel?.type, ToggleSettingsKey.closedAccount)
        XCTAssertEqual(cellModel?.isOn, false)
        XCTAssertEqual(cellModel?.isLoading, false)
        cellModel = sut.cellModelFor(section: 1, row: 0)
        XCTAssertEqual(cellModel?.label, SettingsLocalizable.receiveInPublic.text)
        XCTAssertEqual(cellModel?.type, ToggleSettingsKey.receiveInPrivate )
        XCTAssertEqual(cellModel?.isOn, true)
        XCTAssertEqual(cellModel?.isLoading, false)
        cellModel = sut.cellModelFor(section: 2, row: 0)
        XCTAssertEqual(cellModel?.label, SettingsLocalizable.payInPublic.text)
        XCTAssertEqual(cellModel?.type, ToggleSettingsKey.paymentInPrivate)
        XCTAssertEqual(cellModel?.isOn, true)
        XCTAssertEqual(cellModel?.isLoading, false)
        cellModel = sut.cellModelFor(section: 3, row: 0)
        XCTAssertEqual(cellModel?.label, SettingsLocalizable.notifyBirthday.text)
        XCTAssertEqual(cellModel?.type, ToggleSettingsKey.notifyBirthday)
        XCTAssertEqual(cellModel?.isOn, true)
        XCTAssertEqual(cellModel?.isLoading, false)
    }
    
    func testCellModelApiFailure() {
        fetchPrivacySettingsWithFailure()
        var cellModel = sut.cellModelFor(section: 0, row: 0)
        XCTAssertNil(cellModel)
        cellModel = sut.cellModelFor(section: 1, row: 0)
        XCTAssertNil(cellModel)
    }
    
    func testCellModelWrongIndexFailure() {
        fetchPrivacySettingsWithSuccess()
        var cellModel = sut.cellModelFor(section: 0, row: 1)
        XCTAssertNil(cellModel)
        cellModel = sut.cellModelFor(section: 9, row: 0)
        XCTAssertNil(cellModel)
    }
    
    func testTitleForHeaderAtSectionSuccess() {
        fetchPrivacySettingsWithSuccess()
        XCTAssertEqual(sut.titleForHeaderAtSection(0), "")
        XCTAssertEqual(sut.titleForHeaderAtSection(1), "")
    }
    
    func testTitleForHeaderAtSectionApiFailure() {
        fetchPrivacySettingsWithFailure()
        XCTAssertEqual(sut.titleForHeaderAtSection(0), "")
        XCTAssertEqual(sut.titleForHeaderAtSection(1), "")
    }
    
    func testTitleForHeaderAtSectionWrongIndexFailure() {
        fetchPrivacySettingsWithFailure()
        XCTAssertEqual(sut.titleForHeaderAtSection(2), "")
        XCTAssertEqual(sut.titleForHeaderAtSection(-1), "")
    }
    
    func testTitleForFooterAtSectionSuccess() {
        fetchPrivacySettingsWithSuccess()
        XCTAssertEqual(sut.titleForFooterInSection(0), SettingsLocalizable.openedAccountDescription.text)
        XCTAssertEqual(sut.titleForFooterInSection(1), SettingsLocalizable.receiveInPublicDescription.text)
    }
    
    func testTitleForFooterAtSectionApiFailure() {
        fetchPrivacySettingsWithFailure()
        XCTAssertEqual(sut.titleForFooterInSection(0), "")
        XCTAssertEqual(sut.titleForFooterInSection(1), "")
    }
    
    func testTitleForFooterAtSectionWrongIndexFailure() {
        fetchPrivacySettingsWithFailure()
        XCTAssertEqual(sut.titleForFooterInSection(2), "")
        XCTAssertEqual(sut.titleForFooterInSection(-1), "")
    }
    
    func testUpdateCellInfoState() {
        fetchPrivacySettingsWithSuccess()
        
        var isLoading = false
        var isOn = true
        sut.updateCellInfoState(at: IndexPath(row: 0, section: 0), isLoading: isLoading, isOn: isOn)
        let followerPushCell = sut.cellModelFor(section: 0, row: 0)
        XCTAssertEqual(followerPushCell?.isLoading, isLoading)
        XCTAssertEqual(followerPushCell?.isOn, isOn)
        
        isLoading = true
        isOn = false
        sut.updateCellInfoState(at: IndexPath(row: 0, section: 2), isLoading: isLoading, isOn: isOn)
        let paymentEmailCell = sut.cellModelFor(section: 2, row: 0)
        XCTAssertEqual(paymentEmailCell?.isLoading, isLoading)
        XCTAssertEqual(paymentEmailCell?.isOn, isOn)
    }
    
    func testUpdateSettingsSuccessSwitchOn() throws {
        fetchPrivacySettingsWithSuccess()
        var isOn = true
        let expectationCloseAccountOn = XCTestExpectation(description: "switchedClosedAccountOn")
        let closedAccountSettingKey = ToggleSettingsKey.closedAccount
        let closedAccountConfigName = try XCTUnwrap(closedAccountSettingKey.rawValue.snakeCased())
        sut.updateSetting(
            at: IndexPath(row: 0, section: 0),
            isOn: isOn,
            settingName: closedAccountConfigName,
            settingKey: closedAccountSettingKey,
            onSuccess: {
                expectationCloseAccountOn.fulfill()
            }
        ) { (error) in
            XCTFail()
        }
        wait(for: [expectationCloseAccountOn], timeout: 5.0)
        let openedAccountCell = sut.cellModelFor(section: 0, row: 0)
        XCTAssertEqual(openedAccountCell?.isOn, isOn)
        
        // Alterado o teste para não ser barrado com o popup para confirmar alterações passadas
        isOn = true
        let expectationPayInPrivateOn = XCTestExpectation(description: "switchedPayInPrivateOn")
        let payInPrivateSettingKey = ToggleSettingsKey.paymentInPrivate
        let payInPrivateConfigName = try XCTUnwrap(payInPrivateSettingKey.rawValue.snakeCased())
        sut.updateSetting(
            at: IndexPath(row: 0, section: 2),
            isOn: isOn,
            settingName: payInPrivateConfigName,
            settingKey: payInPrivateSettingKey,
            onSuccess: {
                XCTAssertEqual(ConsumerManager.shared.privacyConfig, FeedItemVisibilityInt.friends.rawValue)
                expectationPayInPrivateOn.fulfill()
            }
        ) { (error) in
            XCTFail()
        }
        let payInPrivateCell = sut.cellModelFor(section: 2, row: 0)
        XCTAssertEqual(payInPrivateCell?.isOn, isOn)
        wait(for: [expectationPayInPrivateOn], timeout: 5.0)
    }
    
    func testUpdateSettingsSuccessSwitchOff() throws {
        fetchPrivacySettingsWithSuccess()
        let isOn = true
        let expectation = XCTestExpectation(description: "response")
        let settingKey = ToggleSettingsKey.closedAccount
        let closedAccountConfigName = try XCTUnwrap(settingKey.rawValue.snakeCased())
        sut.updateSetting(
            at: IndexPath(row: 0, section: 0),
            isOn: isOn,
            settingName: closedAccountConfigName,
            settingKey: settingKey,
            onSuccess: {
                expectation.fulfill()
            }
        ) { (error) in
            XCTFail()
        }
        wait(for: [expectation], timeout: 5.0)
        let closedAccountCell = sut.cellModelFor(section: 0, row: 0)
        XCTAssertEqual(closedAccountCell?.isOn, isOn)
    }
    
    func testUpdateSettingsFailure() throws {
        fetchPrivacySettingsWithFailure()
        let expectation = XCTestExpectation(description: "response")
        let settingKey = ToggleSettingsKey.closedAccount
        let closedAccountConfigName = try XCTUnwrap(settingKey.rawValue.snakeCased())
        sut.updateSetting(
            at: IndexPath(row: 0, section: 0),
            isOn: true,
            settingName: closedAccountConfigName,
            settingKey: settingKey,
            onSuccess: {
                XCTFail()
            }
        ) { (error) in
            XCTAssertNotNil(error)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testUpdateSettings_WhenKeyIsPaymentRequestAndIsSwitchOnIsSuccess_ShouldCallAnalitycs() throws {
        fetchPrivacySettingsWithSuccess()
        let expectation = XCTestExpectation(description: "response")
        let settingKey = ToggleSettingsKey.paymentRequest
        let closedAccountConfigName = try XCTUnwrap(settingKey.rawValue.snakeCased())
        sut.updateSetting(
            at: IndexPath(row: 0, section: 0),
            isOn: true,
            settingName: closedAccountConfigName,
            settingKey: settingKey,
            onSuccess: {
                expectation.fulfill()
            }
        ) { (error) in
            XCTFail()
        }
        wait(for: [expectation], timeout: 5.0)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssert(analyticsSpy.equals(to: PaymentRequestUserEvent.privacyChange(origin: .adjustments, isOn: true).event()))
    }

    func testConfigureTableView_ShouldUpdatePermissions() {
        let settingsModel = PrivacySettingsModel(closedAccount: false, receiveInPrivate: false, paymentInPrivate: false, notifyBirthday: false, paymentRequest: nil, reviewed: false)
        sut.configureTableView(settingsModel)

        XCTAssertEqual(permissionsHelperSpy.callUpdateContactsStateCount, 1)
        XCTAssertEqual(permissionsHelperSpy.callUpdateLocationStateCount, 1)
    }

    func testConfigureTableView_ShouldAddContactsSection() {
        let settingsModel = PrivacySettingsModel(closedAccount: false, receiveInPrivate: false, paymentInPrivate: false, notifyBirthday: false, paymentRequest: nil, reviewed: false)
        sut.configureTableView(settingsModel)

        let model = sut.cellModelFor(section: 4, row: 0)

        XCTAssertEqual(model?.type, .contactsPermission)
    }

    func testConfigureTableView_ShouldAddLocationSection() {
        let settingsModel = PrivacySettingsModel(closedAccount: false, receiveInPrivate: false, paymentInPrivate: false, notifyBirthday: false, paymentRequest: nil, reviewed: false)
        sut.configureTableView(settingsModel)

        let model = sut.cellModelFor(section: 5, row: 0)

        XCTAssertEqual(model?.type, .locationPermission)
    }

    func testSettingsButton_WhenKeyIsContacts_WhenShouldShowSettings_ShouldShowOSPermissions() {
        permissionsHelperSpy.shouldShowContactSettings = true
        sut.didTapChevronButton(settingKey: .contactsPermission, indexPath: IndexPath(row: 0, section: 0))

        XCTAssertEqual(applicationSpy.callOpenUrlCount, 1)
    }

    func testSettingsButton_WhenKeyIsLocation_WhenShouldShowSettings_ShouldShowOSPermissions() {
        permissionsHelperSpy.shouldShowLocationSettings = true
        sut.didTapChevronButton(settingKey: .locationPermission, indexPath: IndexPath(row: 0, section: 0))

        XCTAssertEqual(applicationSpy.callOpenUrlCount, 1)
    }

    func testSettingsButton_WhenKeyIsNotContactsOrLocation_ShouldDoNothing() {
        sut.didTapChevronButton(settingKey: .birthdayPush, indexPath: IndexPath(row: 0, section: 0))

        XCTAssertEqual(applicationSpy.callOpenUrlCount, 0)
    }

    func testSettingsButton_WhenKeyIsContacts_WhenShouldNotShowSettings_ShouldAskForPermission() {
        sut.didTapChevronButton(settingKey: .contactsPermission, indexPath: IndexPath(row: 0, section: 0))

        XCTAssertEqual(permissionsHelperSpy.callAskForContactsPermissionCount, 1)
    }

    func testSettingsButton_WhenKeyIsLocation_WhenShouldNotShowSettings_ShouldAskForPermission() {
        sut.didTapChevronButton(settingKey: .locationPermission, indexPath: IndexPath(row: 0, section: 0))

        XCTAssertEqual(permissionsHelperSpy.callAskForLocationPermissionCount, 1)
    }

    func testSettingsButton_WhenKeyIsContacts_WhenShouldNotShowSettings_ShouldReloadTableViewAfterPermission() {
        let settingsModel = PrivacySettingsModel(closedAccount: false, receiveInPrivate: false, paymentInPrivate: false, notifyBirthday: false, paymentRequest: nil, reviewed: false)
        sut.configureTableView(settingsModel)

        sut.didTapChevronButton(settingKey: .contactsPermission, indexPath: IndexPath(row: 0, section: 0))

        XCTAssertEqual(permissionsHelperSpy.callUpdateContactsStateCount, 1)
        XCTAssertEqual(controllerSpy.callReloadTableViewCellCount, 1)
    }

    func testSettingsButton_WhenKeyIsLocation_WhenShouldNotShowSettings_ShouldReloadTableViewAfterPermission() {
        let settingsModel = PrivacySettingsModel(closedAccount: false, receiveInPrivate: false, paymentInPrivate: false, notifyBirthday: false, paymentRequest: nil, reviewed: false)
        sut.configureTableView(settingsModel)

        sut.didTapChevronButton(settingKey: .locationPermission, indexPath: IndexPath(row: 0, section: 0))

        XCTAssertEqual(permissionsHelperSpy.callUpdateContactsStateCount, 1)
        XCTAssertEqual(controllerSpy.callReloadTableViewCellCount, 1)
    }
}
