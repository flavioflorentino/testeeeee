import AnalyticsModule
import DirectMessageSB
import XCTest
import OHHTTPStubs
@testable import PicPay

enum Stubs {
    static let notificationSettings = NotificationSettingsModel(
        likePush: true,
        commentPush: true,
        followerPush: true,
        paymentPush: true,
        birthdayPush: true,
        newsPush: true,
        paymentEmail: true,
        sendChargeToConsumerPush: true
    )
}

private final class ApplicationSpy: UIApplicationContract {
    private(set) var callOpenUrlCount = 0

    func open(
        _ url: URL,
        options: [UIApplication.OpenExternalURLOptionsKey : Any],
        completionHandler completion: ((Bool) -> Void)?
    ) {
        callOpenUrlCount += 1
    }

    func canOpenURL(_ url: URL) -> Bool {
        true
    }
}

private final class ToggleSettingsViewControllerDelegateSpy: ToggleSettingsViewControllerDelegate {
    private(set) var didCallReloadTableViewCellCount = 0
    private(set) var didCallShowAlertCount = 0
    
    private(set) var alertMessage = ""

    func reloadTableViewCell(at indexPath: IndexPath) {
        didCallReloadTableViewCellCount += 1
    }

    func showAlert(withMessage message: String) {
        didCallShowAlertCount += 1
        alertMessage = message
    }
    
    func showConfirmationPopup(with model: PrivacySettingsPopupViewModel) { }
    func showProfileConfirmationPopup(_ yesCompletion: @escaping () -> Void, _ noCompletion: @escaping () -> Void) { }
}

final class NotificationSettingsViewModelTest: XCTestCase {
    private let delegateSpy = ToggleSettingsViewControllerDelegateSpy()
    private let analyticsSpy = AnalyticsSpy()
    private let applicationSpy = ApplicationSpy()
    private let featureManagerMock = FeatureManagerMock()
    private let chatApiManagerMock = ChatApiManagerMock()
    private lazy var viewModel: NotificationSettingsViewModel = {
        let viewModel = NotificationSettingsViewModel(
            analytics: analyticsSpy,
            application: applicationSpy,
            featureManager: featureManagerMock,
            chatApiManager: chatApiManagerMock
        )
        viewModel.delegate = delegateSpy
        return viewModel
    }()
    
    override func setUp() {
        super.setUp()
        ConsumerManager.shared.consumer?.balance = 0.00
    }
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func fetchNotificationSettingsWithSuccess() {
        let expectation = XCTestExpectation(description: "response")
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(at: "notificationSettings.json")
        }
        viewModel.loadSettings(onSuccess: {
            expectation.fulfill()
        }, onError: { _ in
            XCTFail()
        })
        wait(for: [expectation], timeout: 5.0)
    }
    
    func fetchNotificationSettingsWithFailure() {
        let expectation = XCTestExpectation(description: "response")
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.fail()
        }
        viewModel.loadSettings(onSuccess: {
            XCTFail()
        }, onError: { _ in
            expectation.fulfill()
            
        })
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testNumOfSectionsSuccess() {
        fetchNotificationSettingsWithSuccess()
        let expectedNumOfSections = 2
        XCTAssertEqual(viewModel.numberOfSections(), expectedNumOfSections)
    }
    
    func testNumOfSectionsApiFailure() {
        fetchNotificationSettingsWithFailure()
        let expectedNumOfSections = 0
        XCTAssertEqual(viewModel.numberOfSections(), expectedNumOfSections)
    }
    
    func testNumOfRowsAtSectionSuccess_WhenDirectMessageIsOff() {
        featureManagerMock.override(key: .directMessageSBEnabled, with: false)
        fetchNotificationSettingsWithSuccess()
        let expectedNumOfRowsOnFirstSection = 7
        let expectedNumOfRowsOnSecondSection = 1
        XCTAssertEqual(viewModel.numberOfRowsAtSection(0), expectedNumOfRowsOnFirstSection)
        XCTAssertEqual(viewModel.numberOfRowsAtSection(1), expectedNumOfRowsOnSecondSection)
    }
    
    func testNumOfRowsAtSectionSuccess_WhenDirectMessageIsOn() {
        featureManagerMock.override(key: .directMessageSBEnabled, with: true)
        fetchNotificationSettingsWithSuccess()
        let expectedNumOfRowsOnFirstSection = 8
        let expectedNumOfRowsOnSecondSection = 1
        XCTAssertEqual(viewModel.numberOfRowsAtSection(0), expectedNumOfRowsOnFirstSection)
        XCTAssertEqual(viewModel.numberOfRowsAtSection(1), expectedNumOfRowsOnSecondSection)
    }
    
    func testNumOfRowsAtSectionApiFailure() {
        fetchNotificationSettingsWithFailure()
        let expectedNumOfRows = 0
        XCTAssertEqual(viewModel.numberOfRowsAtSection(0), expectedNumOfRows)
        XCTAssertEqual(viewModel.numberOfRowsAtSection(1), expectedNumOfRows)
    }
    
    func testNumOfRowsAtSectionWrongIndexFailure() {
        fetchNotificationSettingsWithFailure()
        let expectedNumOfRows = 0
        XCTAssertEqual(viewModel.numberOfRowsAtSection(2), expectedNumOfRows)
        XCTAssertEqual(viewModel.numberOfRowsAtSection(-1), expectedNumOfRows)
    }
    
    func testCellModelSuccess() {
        fetchNotificationSettingsWithSuccess()
        var cellModel = viewModel.cellModelFor(section: 0, row: 2)
        XCTAssertEqual(cellModel?.label, SettingsLocalizable.newFollowers.text)
        XCTAssertEqual(cellModel?.type, ToggleSettingsKey.followerPush)
        XCTAssertEqual(cellModel?.isOn, true)
        XCTAssertEqual(cellModel?.isLoading, false)
        cellModel = viewModel.cellModelFor(section: 0, row: 4)
        XCTAssertEqual(cellModel?.label, SettingsLocalizable.birthdaysFromWhoIFollow.text)
        XCTAssertEqual(cellModel?.type, ToggleSettingsKey.birthdayPush)
        XCTAssertEqual(cellModel?.isOn, false)
        XCTAssertEqual(cellModel?.isLoading, false)
    }
    
    func testCellModelApiFailure() {
        fetchNotificationSettingsWithFailure()
        var cellModel = viewModel.cellModelFor(section: 0, row: 2)
        XCTAssertNil(cellModel)
        cellModel = viewModel.cellModelFor(section: 0, row: 4)
        XCTAssertNil(cellModel)
    }
    
    func testCellModelWrongIndexFailure_WhenDirectMessageIsOff() {
        featureManagerMock.override(key: .directMessageSBEnabled, with: false)
        fetchNotificationSettingsWithSuccess()
        var cellModel = viewModel.cellModelFor(section: 0, row: 7)
        XCTAssertNil(cellModel)
        cellModel = viewModel.cellModelFor(section: 2, row: 0)
        XCTAssertNil(cellModel)
    }
    
    func testCellModelWrongIndexFailure_WhenDirectMessageIsOn() {
        featureManagerMock.override(key: .directMessageSBEnabled, with: true)
        fetchNotificationSettingsWithSuccess()
        var cellModel = viewModel.cellModelFor(section: 0, row: 8)
        XCTAssertNil(cellModel)
        cellModel = viewModel.cellModelFor(section: 2, row: 0)
        XCTAssertNil(cellModel)
    }
    
    func testTitleForHeaderAtSectionSuccess() {
        fetchNotificationSettingsWithSuccess()
        XCTAssertEqual(viewModel.titleForHeaderAtSection(0), SettingsLocalizable.receiveNotificationsOnPhone.text)
        XCTAssertEqual(viewModel.titleForHeaderAtSection(1), SettingsLocalizable.receiveNotificationsByEmail.text)
    }
    
    func testTitleForHeaderAtSectionApiFailure() {
        fetchNotificationSettingsWithFailure()
        XCTAssertEqual(viewModel.titleForHeaderAtSection(0), "")
        XCTAssertEqual(viewModel.titleForHeaderAtSection(1), "")
    }
    
    func testTitleForHeaderAtSectionWrongIndexFailure() {
        fetchNotificationSettingsWithFailure()
        XCTAssertEqual(viewModel.titleForHeaderAtSection(2), "")
        XCTAssertEqual(viewModel.titleForHeaderAtSection(-1), "")
    }
    
    func testTitleForFooterAtSectionSuccess() {
        fetchNotificationSettingsWithSuccess()
        XCTAssertEqual(viewModel.titleForFooterInSection(0), "")
        XCTAssertEqual(viewModel.titleForFooterInSection(1), "")
    }
    
    func testTitleForFooterAtSectionApiFailure() {
        fetchNotificationSettingsWithFailure()
        XCTAssertEqual(viewModel.titleForFooterInSection(0), "")
        XCTAssertEqual(viewModel.titleForFooterInSection(1), "")
    }
    
    func testTitleForFooterAtSectionWrongIndexFailure() {
        fetchNotificationSettingsWithFailure()
        XCTAssertEqual(viewModel.titleForFooterInSection(2), "")
        XCTAssertEqual(viewModel.titleForFooterInSection(-1), "")
    }
    
    func testUpdateCellInfoState() {
        fetchNotificationSettingsWithSuccess()
        
        var isLoading = true
        var isOn = false
        viewModel.updateCellInfoState(at: IndexPath(row: 2, section: 0), isLoading: isLoading, isOn: isOn)
        let followerPushCell = viewModel.cellModelFor(section: 0, row: 2)
        XCTAssertEqual(followerPushCell?.isLoading, isLoading)
        XCTAssertEqual(followerPushCell?.isOn, isOn)
        
        isLoading = false
        isOn = true
        viewModel.updateCellInfoState(at: IndexPath(row: 0, section: 1), isLoading: isLoading, isOn: isOn)
        let paymentEmailCell = viewModel.cellModelFor(section: 1, row: 0)
        XCTAssertEqual(paymentEmailCell?.isLoading, isLoading)
        XCTAssertEqual(paymentEmailCell?.isOn, isOn)
    }
    
    func testUpdateSettingsSuccessSwitchOn() throws {
        fetchNotificationSettingsWithSuccess()
        let isOn = true
        let expectation = XCTestExpectation(description: "response")
        let likePushConfigName = try XCTUnwrap(ToggleSettingsKey.likePush.rawValue.snakeCased())
        viewModel.updateSetting(
            at: IndexPath(row: 0, section: 0),
            isOn: isOn,
            settingName: likePushConfigName,
            settingKey: .likePush,
            onSuccess: {
                expectation.fulfill()
            }
        ) { (error) in
            XCTFail()
        }
        wait(for: [expectation], timeout: 5.0)
        let paymentEmailCell = viewModel.cellModelFor(section: 0, row: 0)
        XCTAssertEqual(paymentEmailCell?.isOn, isOn)
    }
    
    func testUpdateSettingsSuccessSwitchOff() throws {
        fetchNotificationSettingsWithSuccess()
        let isOn = false
        let expectation = XCTestExpectation(description: "response")
        let settingKey = ToggleSettingsKey.paymentPush
        let paymentPushConfigName = try XCTUnwrap(settingKey.rawValue.snakeCased())
        viewModel.updateSetting(
            at: IndexPath(row: 3, section: 0),
            isOn: isOn,
            settingName: paymentPushConfigName,
            settingKey: settingKey,
            onSuccess: {
                expectation.fulfill()
            }
        ) { (error) in
            XCTFail()
        }
        wait(for: [expectation], timeout: 5.0)
        let paymentEmailCell = viewModel.cellModelFor(section: 0, row: 3)
        XCTAssertEqual(paymentEmailCell?.isOn, isOn)
    }
    
    func testUpdateSettingsFailure() {
        fetchNotificationSettingsWithFailure()
        let expectation = XCTestExpectation(description: "response")
        viewModel.updateSetting(
            at: IndexPath(row: 0, section: 3),
            isOn: true,
            settingName: "",
            settingKey: .likePush,
            onSuccess: {
                XCTFail()
            }
        ) { (error) in
            XCTAssertNotNil(error)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testUpdateSettings_WhenKeyIsSendChargeToConsumerPushAndIsSwitchOnIsSuccess_ShouldCallAnalitycs() throws {
        fetchNotificationSettingsWithSuccess()
        let isOn = true
        let expectation = XCTestExpectation(description: "response")
        let settingKey = ToggleSettingsKey.sendChargeToConsumerPush
        let likePushConfigName = try XCTUnwrap(settingKey.rawValue.snakeCased())
        viewModel.updateSetting(
            at: IndexPath(row: 0, section: 0),
            isOn: isOn,
            settingName: likePushConfigName,
            settingKey:
            settingKey, onSuccess: {
                expectation.fulfill()
            }
        ) { (error) in
            XCTFail()
        }
        wait(for: [expectation], timeout: 5.0)
        XCTAssertEqual(analyticsSpy.logCalledCount, 1)
        XCTAssert(analyticsSpy.equals(to: PaymentRequestUserEvent.notificationChange(origin: .adjustments, isOn: true).event()))
    }

    func testDidTapPrivacySettingsButton_ShouldShowOSPermissions() {
        viewModel.didTapChevronButton(settingKey: .locationPermission, indexPath: IndexPath(row: 0, section: 0))

        XCTAssertEqual(applicationSpy.callOpenUrlCount, 1)
    }
    
    func testLoadDirectMessageSBCellModel_WhenLoadingDirectMessageCell_UpdateCell() throws {
        // Given
        let loadDirectMessageCellExpectation = expectation(description: "Should load direct message cell model")
        featureManagerMock.override(key: .directMessageSBEnabled, with: true)
        chatApiManagerMock.expectedGetPushNotificationOptionResult = .success(.all)
        viewModel.configureTableView(Stubs.notificationSettings)
        
        viewModel.loadDirectMessageSBCellModel { success in
            XCTAssertTrue(success)
            loadDirectMessageCellExpectation.fulfill()
        }
        
        // Then
        waitForExpectations(timeout: 0.5, handler: nil)
        let section = try XCTUnwrap(viewModel.directMessagePushIndexPath?.section)
        let row = try XCTUnwrap(viewModel.directMessagePushIndexPath?.row)
        let cellModel = viewModel.cellModelFor(section: section, row: row)
        
        XCTAssertEqual(cellModel?.label, SettingsLocalizable.directMessageSendBirdPush.text)
        XCTAssertEqual(cellModel?.type, ToggleSettingsKey.directMessageSendBirdPush)
        XCTAssertEqual(cellModel?.isOn, true)
        XCTAssertEqual(cellModel?.isLoading, false)
        XCTAssertEqual(chatApiManagerMock.didGetPushNotificationOptionCount, 1)
        XCTAssertEqual(delegateSpy.didCallReloadTableViewCellCount, 1)
    }
    
    func testLoadDirectMessageSBCellModel_WhenFailingToLoadDirectMessageCell_ShouldUpdateCell() throws {
        // Given
        let loadDirectMessageCellExpectation = expectation(description: "Should load direct message cell model")
        featureManagerMock.override(key: .directMessageSBEnabled, with: true)
        chatApiManagerMock.expectedGetPushNotificationOptionResult = .failure(DirectMessageSB.Stubs.genericMessagingClientError)
        viewModel.configureTableView(Stubs.notificationSettings)
        
        viewModel.loadDirectMessageSBCellModel { success in
            XCTAssertFalse(success)
            loadDirectMessageCellExpectation.fulfill()
        }
        
        // Then
        waitForExpectations(timeout: 0.5, handler: nil)
        let section = try XCTUnwrap(viewModel.directMessagePushIndexPath?.section)
        let row = try XCTUnwrap(viewModel.directMessagePushIndexPath?.row)
        let cellModel = try XCTUnwrap(viewModel.cellModelFor(section: section, row: row))
        
        XCTAssertEqual(cellModel.label, SettingsLocalizable.directMessageSendBirdPush.text)
        XCTAssertEqual(cellModel.type, ToggleSettingsKey.directMessageSendBirdPush)
        XCTAssertEqual(cellModel.isOn, true)
        XCTAssertEqual(cellModel.isLoading, true)
        XCTAssertEqual(chatApiManagerMock.didGetPushNotificationOptionCount, 1)
        XCTAssertEqual(delegateSpy.didCallReloadTableViewCellCount, 1)
    }
    
    func testUpdateDirectMessageSBPushOption_WhenTurningOnNotification_ShouldSuccessfullySetNotificationOptionAndUpdateCell() throws {
        // Given
        let updatePushNotificationOptionExpectation = expectation(description: "Should update Direct Message notification option")
        let isOn = true
        featureManagerMock.override(key: .directMessageSBEnabled, with: true)
        chatApiManagerMock.expectedSetPushNotificationOptionResult = .success(())
        viewModel.configureTableView(Stubs.notificationSettings)
        
        viewModel.updateDirectMessageSBPushOption(to: isOn) { success in
            updatePushNotificationOptionExpectation.fulfill()
        }
        
        // Then
        waitForExpectations(timeout: 0.5, handler: nil)
        let section = try XCTUnwrap(viewModel.directMessagePushIndexPath?.section)
        let row = try XCTUnwrap(viewModel.directMessagePushIndexPath?.row)
        let cellModel = viewModel.cellModelFor(section: section, row: row)
        XCTAssertEqual(cellModel?.label, SettingsLocalizable.directMessageSendBirdPush.text)
        XCTAssertEqual(cellModel?.type, ToggleSettingsKey.directMessageSendBirdPush)
        XCTAssertEqual(cellModel?.isOn, isOn)
        XCTAssertEqual(cellModel?.isLoading, false)
    }
    
    func testUpdateDirectMessageSBPushOption_WhenTurningOffNotification_ShouldSuccessfullySetNotificationOptionAndUpdateCell() throws {
        // Given
        let updatePushNotificationOptionExpectation = expectation(description: "Should update Direct Message notification option")
        let isOn = false
        featureManagerMock.override(key: .directMessageSBEnabled, with: true)
        chatApiManagerMock.expectedSetPushNotificationOptionResult = .success(())
        viewModel.configureTableView(Stubs.notificationSettings)
        
        viewModel.updateDirectMessageSBPushOption(to: isOn) { success in
            updatePushNotificationOptionExpectation.fulfill()
        }
        
        // Then
        waitForExpectations(timeout: 0.5, handler: nil)
        let section = try XCTUnwrap(viewModel.directMessagePushIndexPath?.section)
        let row = try XCTUnwrap(viewModel.directMessagePushIndexPath?.row)
        let cellModel = viewModel.cellModelFor(section: section, row: row)
        XCTAssertEqual(cellModel?.label, SettingsLocalizable.directMessageSendBirdPush.text)
        XCTAssertEqual(cellModel?.type, ToggleSettingsKey.directMessageSendBirdPush)
        XCTAssertEqual(cellModel?.isOn, isOn)
        XCTAssertEqual(cellModel?.isLoading, false)
    }
    
    func testUpdateDirectMessageSBPushOption_WhenTurningOnNotification_ShouldFailToSetNotificationOptionAndUpdateCellAndShowAlert() throws {
        // Given
        let updatePushNotificationOptionExpectation = expectation(description: "Should update Direct Message notification option")
        let isOn = true
        featureManagerMock.override(key: .directMessageSBEnabled, with: true)
        chatApiManagerMock.expectedSetPushNotificationOptionResult = .failure(DirectMessageSB.Stubs.genericMessagingClientError)
        viewModel.configureTableView(Stubs.notificationSettings)
        
        viewModel.updateDirectMessageSBPushOption(to: isOn) { success in
            updatePushNotificationOptionExpectation.fulfill()
        }
        
        // Then
        waitForExpectations(timeout: 0.5, handler: nil)
        let section = try XCTUnwrap(viewModel.directMessagePushIndexPath?.section)
        let row = try XCTUnwrap(viewModel.directMessagePushIndexPath?.row)
        let cellModel = viewModel.cellModelFor(section: section, row: row)
        XCTAssertEqual(cellModel?.label, SettingsLocalizable.directMessageSendBirdPush.text)
        XCTAssertEqual(cellModel?.type, ToggleSettingsKey.directMessageSendBirdPush)
        XCTAssertEqual(cellModel?.isOn, !isOn)
        XCTAssertEqual(cellModel?.isLoading, false)
        XCTAssertEqual(delegateSpy.didCallReloadTableViewCellCount, 2)
        XCTAssertEqual(delegateSpy.didCallShowAlertCount, 1)
        XCTAssertEqual(delegateSpy.alertMessage, SettingsLocalizable.directMessageSendBirdPushTurningOnError.text)
    }
    
    func testUpdateDirectMessageSBPushOption_WhenTurningOffNotification_ShouldFailToSetNotificationOptionAndUpdateCellAndShowAlert() throws {
        // Given
        let updatePushNotificationOptionExpectation = expectation(description: "Should update Direct Message notification option")
        let isOn = false
        featureManagerMock.override(key: .directMessageSBEnabled, with: true)
        chatApiManagerMock.expectedSetPushNotificationOptionResult = .failure(DirectMessageSB.Stubs.genericMessagingClientError)
        viewModel.configureTableView(Stubs.notificationSettings)
        
        viewModel.updateDirectMessageSBPushOption(to: isOn) { success in
            updatePushNotificationOptionExpectation.fulfill()
        }
        
        // Then
        waitForExpectations(timeout: 0.5, handler: nil)
        let section = try XCTUnwrap(viewModel.directMessagePushIndexPath?.section)
        let row = try XCTUnwrap(viewModel.directMessagePushIndexPath?.row)
        let cellModel = viewModel.cellModelFor(section: section, row: row)
        XCTAssertEqual(cellModel?.label, SettingsLocalizable.directMessageSendBirdPush.text)
        XCTAssertEqual(cellModel?.type, ToggleSettingsKey.directMessageSendBirdPush)
        XCTAssertEqual(cellModel?.isOn, !isOn)
        XCTAssertEqual(cellModel?.isLoading, false)
        XCTAssertEqual(delegateSpy.didCallReloadTableViewCellCount, 2)
        XCTAssertEqual(delegateSpy.didCallShowAlertCount, 1)
        XCTAssertEqual(delegateSpy.alertMessage, SettingsLocalizable.directMessageSendBirdPushTurningOffError.text)
    }
}
