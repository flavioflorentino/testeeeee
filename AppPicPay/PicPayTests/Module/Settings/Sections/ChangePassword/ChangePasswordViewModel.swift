import XCTest
@testable import PicPay

class TestChangePasswordApi: ChangePasswordApi {
    func passwordChangeRequest(currentPassword: String, newPassword: String, completion: @escaping (Bool, Error?) -> Void) {
        if currentPassword == "1234" {
            completion(true, nil)
        } else {
            let erro = NSError()
            completion(false, erro)
        }
    }
}

class ChangePasswordViewModelTest: XCTestCase {
    var viewModel: ChangePasswordViewModel!
    
    override func setUp() {
        super.setUp()
        viewModel = ChangePasswordViewModel(api: TestChangePasswordApi())
    }
    
    func testSucessSaveChangePassword() {
        viewModel.saveChangePassword(currentPassword: "1234", newPassword: "123456") { erro in
            XCTAssert(erro == nil)
        }
    }
    
    func testErroSaveChangePassword() {
        viewModel.saveChangePassword(currentPassword: "4321", newPassword: "123456") { erro in
            XCTAssert(erro != nil)
        }
    }
    
    func testVerifyPasswordValid() {
        let isValid = viewModel.verifyPassword(newPassword: "1234", repeatPassword: "1234")
        XCTAssertTrue(isValid)
    }
    
    func testVerifyPasswordInvalid() {
        let isValid = viewModel.verifyPassword(newPassword: "1234", repeatPassword: "4321")
        XCTAssertFalse(isValid)
    }
}
