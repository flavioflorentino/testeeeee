import XCTest
@testable import PicPay
import Validator
import UI

class PersonalDetailsViewModelTest: XCTestCase {

    var viewModel: PersonalDetailsViewModel!
    
    override func setUp() {
        super.setUp()
        
        let consumer = MBConsumer()
        consumer.name = "Bill Gates"
        consumer.cpf = "*********-27"
        consumer.birth_date = "28/10/1955"
        consumer.name_mother = "Mary Gates"
        
        let service: PersonalDetailsServicing = PersonalDetailsService()
        viewModel = PersonalDetailsViewModel(service: service, consumer: consumer)
    }
    
    override func tearDown() { }
    
    
    // MARK: - Helpers
    
    func textfield(text: String) -> UIPPFloatingTextField {
        let textfield = UIPPFloatingTextField()
        textfield.text = text
        return textfield
    }
    
    func textFieldSet(setting: TextfieldSetting) -> UIPPFloatingTextField {
        var textfield = UIPPFloatingTextField()
        textfield.text = setting.text
        textfield.validationRules = setting.rule
        return textfield
    }
    
    
    // MARK: - Tests
    
    func testBuildModelForUpdate() {
        
        let name = self.textfield(text: "Steve Jobs")
        let birthdate = self.textfield(text: "24/02/1955")
        let mothersName = self.textfield(text: "Clara Jobs")
        
        let newModel = viewModel.buildModelForUpdate(name: name, birthdate: birthdate, mothersName: mothersName)
        
        XCTAssertEqual(name.text, newModel.name)
    }
    
    func testBuildTextfieldSettingName() {
        let setting = viewModel.buildTextfieldSettingName()
        XCTAssertEqual(setting.placeholder, "Seu nome")
        XCTAssertEqual(setting.text, "Bill Gates")
        XCTAssertNotNil(setting.rule)
        XCTAssertNil(setting.mask)
    }
    
    func testBuildTextfieldSettingDocument() {
        let setting = viewModel.buildTextfieldSettingDocument()
        XCTAssertEqual(setting.placeholder, "CPF")
        XCTAssertEqual(setting.text, "*********-27")
        XCTAssertNil(setting.rule)
        XCTAssertNil(setting.mask)
    }
    
    func testBuildTextfieldSettingBirthdate() {
        let setting = viewModel.buildTextfieldSettingBirthdate()
        XCTAssertEqual(setting.placeholder, "Data de nascimento")
        XCTAssertEqual(setting.text, "28/10/1955")
        XCTAssertNotNil(setting.rule)
        XCTAssertNotNil(setting.mask)
        XCTAssertEqual(setting.mask, "00/00/0000")
    }
    
    func testBuildTextfieldSettingMothersName() {
        let setting = viewModel.buildTextfieldSettingMothersName()
        XCTAssertEqual(setting.placeholder, "Nome completo da sua mãe")
        XCTAssertEqual(setting.text, "Mary Gates")
        XCTAssertNil(setting.rule)
        XCTAssertNil(setting.mask)
    }
}
