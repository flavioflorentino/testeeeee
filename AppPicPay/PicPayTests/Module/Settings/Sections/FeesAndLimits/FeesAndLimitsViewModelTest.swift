import XCTest
@testable import PicPay

class FeesAndLimitsServiceTest: FeesAndLimitsWorker {
    private let isSuccess: Bool
    private let showSection: Bool
    private let isPro: Bool
    
    init(isSuccess: Bool, showSection: Bool, isPro: Bool) {
        self.isSuccess = isSuccess
        self.showSection = showSection
        self.isPro = isPro
    }
    
    func userIsPro() -> Bool {
        return isPro
    }
    
    func getFeesAndLimits(completion: @escaping (PicPayResult<FeesAndLimits>) -> Void) {
        if isSuccess {
            let model: FeesAndLimits = try! MockCodable().loadCodableObject(resource: "TaxsLimits", typeDecoder: .useDefaultKeys)
            completion(PicPayResult.success(model))
        } else {
            let error = PicPayError(message: "Error")
            completion(PicPayResult.failure(error))
        }
    }
    
    func showSectionPro() -> Bool {
        return showSection
    }
    
    func showSectionLimits() -> Bool {
        return showSection
    }
    
    func showSectionPersonal() -> Bool {
        return showSection
    }
}

class FeesAndLimitsViewModelTest: XCTestCase {
    var viewModel: FeesAndLimitsViewModel!
    
    override func setUp() {
        super.setUp()
        let expectation = self.expectation(description: "Api")
        viewModel = FeesAndLimitsViewModel(api: FeesAndLimitsServiceTest(isSuccess: true, showSection: true, isPro: false))
        viewModel.loadFeesAndLimits(onSuccess: {
             expectation.fulfill()
        }, onError: { (_) in
            XCTFail()
        })
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testShowSectionPersonalEnable() {
        let result = viewModel.showSection(section: .personal)
        XCTAssertTrue(result)
    }
    
    func testShowSectionPersonalDisable() {
        let viewModel = FeesAndLimitsViewModel(api: FeesAndLimitsServiceTest(isSuccess: true, showSection: false, isPro: false))
        let result = viewModel.showSection(section: .personal)
        XCTAssertFalse(result)
    }
    
    func testShowSectionLimitEnable() {
        let result = viewModel.showSection(section: .limit)
        XCTAssertTrue(result)
    }
    
    func testShowSectionLimitDisable() {
        let viewModel = FeesAndLimitsViewModel(api: FeesAndLimitsServiceTest(isSuccess: true, showSection: false, isPro: false))
        let result = viewModel.showSection(section: .limit)
        XCTAssertFalse(result)
    }
    
    func testShowSectionProEnable() {
        let result = viewModel.showSection(section: .pro)
        XCTAssertTrue(result)
    }
    
    func testShowSectionProDisable() {
        let viewModel = FeesAndLimitsViewModel(api: FeesAndLimitsServiceTest(isSuccess: true, showSection: false, isPro: false))
        let result = viewModel.showSection(section: .pro)
        XCTAssertFalse(result)
    }
    
    func testShowHeaderPersonal() {
        let result = viewModel.showHeader(section: .personal)
        XCTAssertTrue(result)
    }
    
    func testShowHeaderProUserNotPro() {
        let result = viewModel.showHeader(section: .pro)
        XCTAssertTrue(result)
    }
    
    func testShowHeaderProUserIsPro() {
        let viewModel = FeesAndLimitsViewModel(api: FeesAndLimitsServiceTest(isSuccess: true, showSection: false, isPro: true))
        let result = viewModel.showHeader(section: .pro)
        XCTAssertFalse(result)
    }
    
    func testShowHeaderLimit() {
        let result = viewModel.showHeader(section: .limit)
        XCTAssertFalse(result)
    }
    
    func testShowFooterPersonal() {
        let result = viewModel.showFooter(section: .personal)
        XCTAssertFalse(result)
    }
    
    func testShowFooterPro() {
        let result = viewModel.showFooter(section: .pro)
        XCTAssertTrue(result)
    }
    
    func testShowFooterLimit() {
        let result = viewModel.showFooter(section: .limit)
        XCTAssertTrue(result)
    }
    
    func testTitlePersonal() {
        let result = viewModel.title(section: .personal)
        XCTAssertEqual(result, "Conta Pessoal")
    }
    
    func testTitlePro() {
        let result = viewModel.title(section: .pro)
        XCTAssertEqual(result, "Conta PRO")
    }
    
    func testTitleLimit() {
        let result = viewModel.title(section: .limit)
        XCTAssertEqual(result, "Limites de movimentação")
    }

    
    func testHeaderTitlePersonal() {
        let result = viewModel.headerTitle(section: .personal)
        XCTAssertEqual(result, "As únicas taxas")
    }
    
    func testHeaderTitlePro() {
        let result = viewModel.headerTitle(section: .pro)
        XCTAssertEqual(result, "Quer usar o PicPay")
    }
    
    func testHeaderTitleLimit() {
        let result = viewModel.headerTitle(section: .limit)
        XCTAssertTrue(result.isEmpty)
    }
    
    func testFooterTitlePersonal() {
        let result = viewModel.footerTitle(section: .personal)
        XCTAssertTrue(result.isEmpty)
    }
    
    func testFooterTitlePro() {
        let result = viewModel.footerTitle(section: .pro)
        XCTAssertEqual(result, "*Pagamentos parcelados")
    }
    
    func testFooterTitleLimit() {
        let result = viewModel.footerTitle(section: .limit)
        let value = Double(3500)
        XCTAssertEqual(result, "Para ter mais de \(value.getFormattedPrice()) de saldo PicPay e fazer aportes sem limites mensais, faça upgrade de sua conta.")
    }
    
    func testHeaderHighlightPersonal() {
        let result = viewModel.headerHighlight(section: .personal)
        XCTAssertTrue(result.isEmpty)
    }
    
    func testHeaderHighlightPro() {
        let result = viewModel.headerHighlight(section: .pro)
        XCTAssertEqual(result, "Mude para uma conta PRO.")
    }
    
    func testHeaderHighlightLimit() {
        let result = viewModel.headerHighlight(section: .limit)
        XCTAssertTrue(result.isEmpty)
    }
    
    func testFooterHighlightPersonal() {
        let result = viewModel.footerHighlight(section: .personal)
        XCTAssertTrue(result.isEmpty)
    }
    
    func testFooterHighlightPro() {
        let result = viewModel.footerHighlight(section: .pro)
        XCTAssertTrue(result.isEmpty)
    }
    
    func testFooterHighlightLimit() {
        let result = viewModel.footerHighlight(section: .limit)
        XCTAssertEqual(result, "faça upgrade de sua conta.")
    }
    
    func testRowPersonal() {
        let result = viewModel.row(section: .personal, row: 0)
        XCTAssertEqual(result.0, "Pagar boletos via")
        XCTAssertEqual(result.1, "x,xx% do valor do boleto")
    }
    
    func testRowPro() {
        let result = viewModel.row(section: .pro, row: 0)
        XCTAssertEqual(result.0, "Pagar boletos")
        XCTAssertEqual(result.1, "valor do boleto")
    }
    
    func testRowLimitFirst() {
        let result = viewModel.row(section: .limit, row: 0)
        let value = Double(3500)
        XCTAssertEqual(result.0, "Limite de saldo PicPay")
        XCTAssertEqual(result.1, value.getFormattedPrice())
    }
    
    func testRowLimitSecond() {
        let result = viewModel.row(section: .limit, row: 1)
        let value = Double(8000)
        XCTAssertEqual(result.0, "Limite de aportes mensais")
        XCTAssertEqual(result.1, value.getFormattedPrice())
    }
    
    func testUserIsPro() {
        let viewModel = FeesAndLimitsViewModel(api: FeesAndLimitsServiceTest(isSuccess: true, showSection: false, isPro: true))
        let result = viewModel.userIsPro()
        XCTAssertTrue(result)
    }
    
    func testNumberOfRowPersonal() {
        let result = viewModel.numberOfRows(section: .personal)
        XCTAssertEqual(result, 4)
    }
    
    func testNumberOfRowPro() {
        let result = viewModel.numberOfRows(section: .pro)
        XCTAssertEqual(result, 3)
    }
    
    func testNumberOfRowLimit() {
        let result = viewModel.numberOfRows(section: .limit)
        XCTAssertEqual(result, 2)
    }
    
    func testRowIsSelectableFalse() {
        let result = viewModel.rowIsSelectable(section: .personal, row: 0)
        XCTAssertFalse(result)
    }
    
    func testRowIsSelectableTrue() {
        let result = viewModel.rowIsSelectable(section: .limit, row: 1)
        XCTAssertTrue(result)
    }
    
    func testProgressDeposit() {
        let result = viewModel.progressDeposit()
        XCTAssertEqual(result, 0.125)
    }
    
    func testLimitDepositText() {
        let result = viewModel.limitDepositText()
        let value = Double(1000)
        XCTAssertEqual(result, "Você já adicionou \(value.getFormattedPrice()) em seu saldo PicPay este mês.")
    }
    
    func testLimitDepositHighlight() {
        let result = viewModel.limitDepositHighlight()
        let value = Double(1000)
        XCTAssertEqual(result, value.getFormattedPrice())
    }
    
    func testLoadTaxsAndLimitsSuccess() {
        let viewModel = FeesAndLimitsViewModel(api: FeesAndLimitsServiceTest(isSuccess: true, showSection: true, isPro: false))
        let expectation = self.expectation(description: "API")
        viewModel.loadFeesAndLimits(onSuccess: {
            expectation.fulfill()
        }, onError: {(_) in
            XCTFail()
        })
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testLoadTaxsAndLimitsError() {
        let viewModel = FeesAndLimitsViewModel(api: FeesAndLimitsServiceTest(isSuccess: false, showSection: true, isPro: false))
        let expectation = self.expectation(description: "API")
        viewModel.loadFeesAndLimits(onSuccess: {
            XCTFail()
        }, onError: {(_) in
            expectation.fulfill()
        })
        waitForExpectations(timeout: 5, handler: nil)
    }
}
