import XCTest
@testable import PicPay

class SettingsUsernameViewControllerTest: XCTestCase {
    func testInstantiateSettingsViewController() {
        let viewModel = SettingsUsernameViewModel()
        let vc = SettingsUsernameViewController(with: viewModel)
        vc.loadViewIfNeeded()
        
        XCTAssertNotNil(vc)
    }
}
