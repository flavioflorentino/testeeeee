import XCTest
@testable import PicPay

class TestSettingsUsernameWorker: SettingsUsernameWorker {
    private let success: Bool
    
    init(success: Bool) {
        self.success = success
    }
    
    func updateUsername(username: String, completion: @escaping (Error?) -> Void) {
        if success {
            completion(nil)
        } else {
            let erro = PicPayError(message: "Teste")
            completion(erro)
        }
    }
}

class SettingsUsernameViewModelTest: XCTestCase {
    func mockViewModel(success: Bool = true) -> SettingsUsernameViewModel {
        let consumer = MBConsumer()
        let worker = TestSettingsUsernameWorker(success: success)
        consumer.username = "gustavo.storck"
        
        let viewModel = SettingsUsernameViewModel(consumerWorker: ConsumerWorkerTest(consumer: consumer), worker: worker)
        return viewModel
    }
    
    func testSanitizedUsernameValid() {
        let viewModel = mockViewModel()
        let validCharacters = "abcdefghijklmnopqrstuvxzwyk0123456789._"
        let result = viewModel.sanitizedUsername(validCharacters)
        XCTAssertEqual(result, validCharacters)
    }

    func testSanitizedUsernamInvalid() {
        let viewModel = mockViewModel()
        let invalidCharacters = "ABCDEFGHIJ KLMNOPQRSTUVXZWY !@#$%ˆ&*()-+=`~:;?\\/><,'\""
        let result = viewModel.sanitizedUsername(invalidCharacters)
        XCTAssertEqual(result, "")
    }
    
    func testUsername() {
        let viewModel = mockViewModel()
        let result = viewModel.username
        
        XCTAssertEqual(result, "gustavo.storck")
    }
    
    func testSaveUsernameSuccess() {
        let viewModel = mockViewModel(success: true)
        let expectation = self.expectation(description: "Api")
        
        viewModel.saveUsername(username: "Teste", onSuccess: {
            let result = viewModel.username
            expectation.fulfill()
            XCTAssert(true)
            XCTAssertEqual(result, "Teste")
        }) { (_) in
            XCTFail()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testSaveUsernameFail() {
        let viewModel = mockViewModel(success: false)
        let expectation = self.expectation(description: "Api")
        
        viewModel.saveUsername(username: "Teste", onSuccess: {
            XCTFail()
        }) { (_) in
            expectation.fulfill()
            XCTAssert(true)
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }
}
