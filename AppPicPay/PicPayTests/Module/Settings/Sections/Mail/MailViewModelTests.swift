//
//  MailViewModelTests.swift
//  PicPayTests
//
//  Created by Leonardo Saragiotto on 20/09/19.
//

import XCTest
@testable import PicPay

class MailViewModelTests: XCTestCase {
    
    private var mailViewModel: MailViewModel?
    private var outputsMock: MailViewModelOutputsMock?
    private var serviceMock: MailServiceMock?
    
    override func setUp() {
        outputsMock = MailViewModelOutputsMock()
        serviceMock = MailServiceMock()
        
        mailViewModel = MailViewModel(service: serviceMock ?? MailServiceMock())
        mailViewModel?.outputs = outputsMock
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testViewModelLoad() {
        mailViewModel?.viewDidLoad()
        XCTAssertEqual(outputsMock?.email, "leonardo@picpay.com")
        XCTAssertEqual(outputsMock?.currentState, .verified)
    }
    
    func testViewModelLoadForPendingStatus() {
        serviceMock?.pendingVerificationState()
        mailViewModel?.viewDidLoad()
        XCTAssertEqual(outputsMock?.email, "leonardo@picpay.com")
        XCTAssertEqual(outputsMock?.currentState, .pending)
    }
    
    func testViewModelLoadForUnverifiesStatus() {
        serviceMock?.unverifiedState()
        mailViewModel?.viewDidLoad()
        XCTAssertEqual(outputsMock?.email, "leonardo@picpay.com")
        XCTAssertEqual(outputsMock?.currentState, .unverified)
    }
    
    func testNewEmailTypedValid() {
        mailViewModel?.viewDidLoad()
        mailViewModel?.newEmailTyped("leonardo@gmail.com")
        XCTAssertEqual(outputsMock?.currentState, .changed)
        XCTAssertTrue(outputsMock?.saveButtonEnable ?? false)
    }
    
    func testNewEmailTypedInvalid() {
        mailViewModel?.viewDidLoad()
        mailViewModel?.newEmailTyped("leonardo@gmail")
        XCTAssertEqual(outputsMock?.currentState, .changed)
        XCTAssertFalse(outputsMock?.saveButtonEnable ?? true)
    }
    
    func testResendEmail() {
        mailViewModel?.viewDidLoad()
        mailViewModel?.resendEmail()
        XCTAssertEqual(outputsMock?.alertTitle, MailLocalizable.newVerificationMail.text)
        XCTAssertEqual(outputsMock?.alertMessage, String(format: MailLocalizable.accessSentLink.text, outputsMock?.email ?? ""))
    }
}

class MailViewModelOutputsMock: MailViewModelOutputs {
    public var email: String?
    enum ViewState {
        case verified, unverified, changed, pending
    }
    var currentState: ViewState?
    var saveButtonEnable: Bool?
    var alertTitle: String?
    var alertMessage: String?
    
    func currentEmail(_ email: String) {
        self.email = email
    }
    
    func changeViewToEmailVerified() {
        currentState = .verified
    }
    
    func changeViewToEmailUnverified() {
        currentState = .unverified
    }
    
    func changeViewToEmailChanged() {
        currentState = .changed
    }
    
    func changeViewToEmailPending() {
        currentState = .pending
    }
    
    func changeSaveButtonStatus(_ enable: Bool) {
        saveButtonEnable = enable
    }
    
    func presentErrorAlert(_ message: String) { }
    
    func presentAlert(_ title: String, message: String) {
        alertTitle = title
        alertMessage = message
    }
    
    func startLoadingView() { }
    
    func stopLoadingView() { }
    
    func askForChangeConfirmation(from previousEmail: String, to newEmail: String) { }
}

class MailServiceMock: MailServicing {
    private var consumerEmail: String
    private var emailVerified: Bool
    private var pendingVerification: Bool
    private var updateResponse: Bool
    private var resendResponse: Bool
    
    init() {
        consumerEmail = "leonardo@picpay.com"
        emailVerified = true
        pendingVerification = false
        updateResponse = true
        resendResponse = true
    }
    
    func pendingVerificationState() {
        pendingVerification = true
    }
    
    func unverifiedState() {
        emailVerified = false
    }
    
    func updateUsersEmail(_ email: String, completion: @escaping CompletionRequestSuccess) {
        guard updateResponse else {
            completion(.failure(.userCancel))
            return
        }
        
        completion(.success(()))
    }
    
    func resendConfirmationEmail(_ completion: @escaping CompletionRequestSuccess) {
        guard resendResponse else {
            completion(.failure(.userCancel))
            return
        }
        
        completion(.success(()))
    }
    
    func getConsumerEmail() -> String {
        return consumerEmail
    }
    
    func getConsumerEmailVerified() -> Bool {
        return emailVerified
    }
    
    func getConsumerPendingVerification() -> Bool {
        return pendingVerification
    }
}
