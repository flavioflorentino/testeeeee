import XCTest
@testable import PicPay

class FakeMyPhoneNumberService: MyPhoneNumberServicing {
    var phoneNumber: String?
}

class FakeMyPhoneNumberViewController: UIViewController, MyPhoneNumberViewModelOutputs {
    var isUpdatePhoneNumberCalled = false
    var phoneNumber: String?
    
    func updatePhoneNumber(withValue value: String) {
        isUpdatePhoneNumberCalled = true
        phoneNumber = value
    }
}

class MyPhoneNumberViewModelTest: XCTestCase {
    var viewModel: MyPhoneNumberViewModelType!
    var viewController: FakeMyPhoneNumberViewController!
    var service: FakeMyPhoneNumberService!
    
    override func setUp() {
        super.setUp()
        service = FakeMyPhoneNumberService()
        viewModel = MyPhoneNumberViewModel(service: service)
        viewController = FakeMyPhoneNumberViewController()
        viewModel.outputs = viewController
    }
    
    func testPhoneNumberLabelUpdate() {
        service.phoneNumber = "27988850460"
        viewModel.inputs.viewDidLoad()
        XCTAssert(viewController.isUpdatePhoneNumberCalled)
    }
    
    func testPhoneNumberLabelCellphoneMask() {
        service.phoneNumber = "27988850460"
        viewModel.inputs.viewDidLoad()
        XCTAssertEqual(viewController.phoneNumber, "(27) 98885-0460")
    }
    
    func testPhoneNumberLabelLandlineMask() {
        service.phoneNumber = "2732526914"
        viewModel.inputs.viewDidLoad()
        XCTAssertEqual(viewController.phoneNumber, "(27) 3252-6914")
    }
    
    func testPhoneNumberLabelOtherMask() {
        service.phoneNumber = "080012345678"
        viewModel.inputs.viewDidLoad()
        XCTAssertEqual(viewController.phoneNumber, "080012345678")
    }
    
    func testPhoneNumberLabelIncompleteMask() {
        service.phoneNumber = "1234as*f34"
        viewModel.inputs.viewDidLoad()
        XCTAssertEqual(viewController.phoneNumber, "(12) 34")
    }
    
    func testPhoneNumberLabelIgnoredByMask() {
        service.phoneNumber = "123456789"
        viewModel.inputs.viewDidLoad()
        XCTAssertEqual(viewController.phoneNumber, "123456789")
    }
}
