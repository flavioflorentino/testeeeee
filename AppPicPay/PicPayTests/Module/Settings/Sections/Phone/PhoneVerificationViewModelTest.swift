import XCTest
@testable import PicPay

class FakePhoneVerificationCodeService: PhoneVerificationCodeServicing {
    var phoneVerificationNumber: String? = "988850460"
    
    var phoneVerificationDDD: String? = "27"
    
    var delayToMakeVerificationThroughCall: TimeInterval = 0.0
    
    var delayToMakeVerificationThroughSMS: TimeInterval =  0.0
    
    var lastPhoneVerificationDate: Date = Date()
    
    var isCallVerificationEnabled: Bool = false
    
    func finishPhoneStep(ddd: String?, phoneNumber: String?, completion: @escaping (Result<Void, Error>) -> Void) {
        completion(responseApiReturnMock)
    }
    
    func requestCallToPhone(ddd: String?, phoneNumber: String?, completion: @escaping (Result<Void, Error>) -> Void) {
        completion(responseApiReturnMock)
    }
    
    func verifyPhoneNumber(code: String, completion: @escaping (Result<Void, Error>) -> Void) {
        completion(responseApiReturnMock)
    }
    
    var responseApiReturnMock = Result<Void, Error>.success(())
}

class FakePhoneVerificationCodeViewController: PhoneVerificationCodeViewModelOutputs {
    var didShowConfirmationAlert = false
    var didShowYouWillReceiveACallAlert = false
    var isErrorMessageVisible = false
    var isSendSMSButtonEnable = true
    var isCallButtonEnable = true
    var wasPhoneNumberVerifiedWithSuccess = false
    
    func updateCallButton(isEnabled: Bool, title: String) {
        isCallButtonEnable = isEnabled
    }
    
    func updateSendSMSButton(isEnabled: Bool, title: String) {
        isSendSMSButtonEnable = isEnabled
    }
    
    func disableSendSMSButton() {
        isSendSMSButtonEnable = true
    }
    
    func disableCallButton() {}
    
    func hideCallButton() {}
    
    func showErrorMessage(labelMessage: String?, textFieldMessage: String?) {
        isErrorMessageVisible = true
    }
    
    func hideErrorMessage() {
        isErrorMessageVisible = false
    }
    
    func startLoading() {}
    
    func stopLoading() {}
    
    func phoneNumberVerifiedWithSuccess() {
        wasPhoneNumberVerifiedWithSuccess = true
    }
    
    func showConfirmationAlertWith(ddd: String, phoneNumber: String) {
        didShowConfirmationAlert = true
    }
    
    func showYouWillReceiveACallAlert() {
        didShowYouWillReceiveACallAlert = true
    }
}

class PhoneVerificationViewModelTest: XCTestCase {
    var viewModel: PhoneVerificationCodeViewModelType!
    var viewController: FakePhoneVerificationCodeViewController!
    var service: FakePhoneVerificationCodeService!
    
    override func setUp() {
        super.setUp()
        
        service = FakePhoneVerificationCodeService()
        viewModel = PhoneVerificationCodeViewModel(service: service)
        viewController = FakePhoneVerificationCodeViewController()
        viewModel.outputs = viewController
    }
    
    func setUpViewModelWithoutPhoneNumber() {
        service = FakePhoneVerificationCodeService()
        service.phoneVerificationDDD = nil
        service.phoneVerificationNumber = nil
        viewModel = PhoneVerificationCodeViewModel(service: service)
        viewModel.outputs = viewController
    }
    
    func testSendSMSCodeWithPhoneNumber() {
        viewModel.inputs.confirmSendSMSCode()
        XCTAssert(viewController.didShowConfirmationAlert)
    }
    
    func testSendSMSCodeWithoutPhoneNumber() {
        setUpViewModelWithoutPhoneNumber()
        viewModel.inputs.confirmSendSMSCode()
        XCTAssertFalse(viewController.didShowConfirmationAlert)
    }
    
    func sendSMSCodeRequestSuccess() {
        viewModel.inputs.sendSMSCodeRequest()
        XCTAssertFalse(viewController.isErrorMessageVisible)
    }
    
    func sendSMSCodeRequestFailure() {
        service.responseApiReturnMock = .failure(MockError.connectionError)
        viewModel.inputs.sendSMSCodeRequest()
        XCTAssert(viewController.isErrorMessageVisible)
    }
    
    func testCallUserPhoneSuccess() {
        viewModel.inputs.callUserPhone()
        XCTAssert(viewController.didShowYouWillReceiveACallAlert)
    }
    
    func testCallUserPhoneFailure() {
        service.responseApiReturnMock = .failure(MockError.connectionError)
        viewModel.inputs.callUserPhone()
        XCTAssert(viewController.isErrorMessageVisible)
    }
    
    func testCallUserPhoneWithoutNumber() {
        setUpViewModelWithoutPhoneNumber()
        service.responseApiReturnMock = .failure(MockError.connectionError)
        viewModel.inputs.callUserPhone()
        XCTAssertFalse(viewController.didShowYouWillReceiveACallAlert)
        XCTAssertFalse(viewController.isErrorMessageVisible)
    }
    
    func testValidateCodeSuccess() {
        viewModel.inputs.validateCode(code: "1234")
        XCTAssertFalse(viewController.isErrorMessageVisible)
        XCTAssert(viewController.wasPhoneNumberVerifiedWithSuccess)
    }
    
    func testValidateCodeFailureApi() {
        viewModel.inputs.validateCode(code: "1234")
        service.responseApiReturnMock = .failure(MockError.genericError)
        XCTAssertFalse(viewController.isErrorMessageVisible)
    }
    
    func testValidateCodeFailureTooShort() {
        viewModel.inputs.validateCode(code: "156")
        XCTAssert(viewController.isErrorMessageVisible)
    }
    
    func testValidateCodeFailureTooLong() {
        viewModel.inputs.validateCode(code: "86377")
        XCTAssert(viewController.isErrorMessageVisible)
    }
}
