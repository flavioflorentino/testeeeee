import XCTest
@testable import PicPay

class FakeChangePhoneNumberService: ChangePhoneNumberServicing {
    var phoneHasNoVerificationNumber: Bool = true
    
    var delayToMakeVerificationThroughSMS: TimeInterval = 0.0
    
    var lastPhoneVerificationDate: Date = Date()
    
    func sendSMSVerification(ddd: String?, phoneNumber: String?, completion: @escaping (Result<Void, Error>) -> Void) {
        completion(sendSMSVerificationMockReturn)
    }
    
    var sendSMSVerificationMockReturn = Result<Void, Error>.success(())
}

class FakeChangePhoneNumberViewController: ChangePhoneNumberViewModelOutputs {
    var isSendSMSButtonEnabled = true
    var sendSMSButtonTitle: String?
    var errorDDDFieldMessage: String?
    var errorPhoneNumberFieldMessage: String?
    var isHidenAlredyHaveACodeButton = false
    var confirmationAlertPhoneNumber: (ddd: String, number: String)?
    var didPresentVerifyCodeStep = false
    var didPresentSMSVerificationError = false
    
    func updateSendSMSButton(isEnabled: Bool, title: String) {
        isSendSMSButtonEnabled = isEnabled
        sendSMSButtonTitle = title
    }
    
    func updateErrorDDDField(errorMessage: String?) {
        errorDDDFieldMessage = errorMessage
    }
    
    func updateErrorPhoneNumberField(errorMessage: String?) {
        errorPhoneNumberFieldMessage = errorMessage
    }
    
    func presentNumberConfirmationAlert(ddd: String, phoneNumber: String) {
        confirmationAlertPhoneNumber = (ddd, phoneNumber)
    }
    
    func hideAlredyHaveACodeButton() {
        isHidenAlredyHaveACodeButton = true
    }
    
    func presentSMSVerificationError(_ error: Error) {
        didPresentSMSVerificationError = true
    }
    
    func presentVerifyCodeStep() {
        didPresentVerifyCodeStep = true
    }
}

class ChangePhoneViewModelTest: XCTestCase {
    var viewModel: ChangePhoneNumberViewModelType!
    var viewController: FakeChangePhoneNumberViewController!
    var service: FakeChangePhoneNumberService!
    
    override func setUp() {
        super.setUp()
        
        service = FakeChangePhoneNumberService()
        viewModel = ChangePhoneNumberViewModel(service: service)
        viewController = FakeChangePhoneNumberViewController()
        viewModel.outputs = viewController
    }
    
    func testUpdateAlreadyHaveACodeButtonStateAndShow() {
        service.phoneHasNoVerificationNumber = true
        viewModel.inputs.updateAlreadyHaveACodeButtonState()
        XCTAssert(viewController.isHidenAlredyHaveACodeButton)
    }
    
   func testUpdateAlreadyHaveACodeButtonStateAndHide() {
        service.phoneHasNoVerificationNumber = false
        viewModel.inputs.updateAlreadyHaveACodeButtonState()
        XCTAssertFalse(viewController.isHidenAlredyHaveACodeButton)
    }
    
    func testDidTapSentSMSButtonSuccess() {
        let ddd = "27"
        let phoneNumber = "988850460"
        viewModel.inputs.didTapSentSMSButton(withDDD: ddd, phoneNumber: phoneNumber)
        XCTAssertEqual(viewController.confirmationAlertPhoneNumber?.ddd, ddd)
        XCTAssertEqual(viewController.confirmationAlertPhoneNumber?.number, phoneNumber)
    }
    
    func testDidTapSentSMSButtonWithInvalidDDD() {
        let ddd = "2"
        let phoneNumber = "988850460"
        viewModel.inputs.didTapSentSMSButton(withDDD: ddd, phoneNumber: phoneNumber)
        XCTAssertNil(viewController.confirmationAlertPhoneNumber)
    }
    
    func testDidTapSentSMSButtonWithInvalidNumber() {
        let ddd = "27"
        let phoneNumber = "9888504"
        viewModel.inputs.didTapSentSMSButton(withDDD: ddd, phoneNumber: phoneNumber)
        XCTAssertNil(viewController.confirmationAlertPhoneNumber)
    }
    
    func testDidTapSentSMSButtonWithNilNumber() {
        viewModel.inputs.didTapSentSMSButton(withDDD: nil, phoneNumber: nil)
        XCTAssertNil(viewController.confirmationAlertPhoneNumber)
    }
    
    func testSendSMSVerificationSuccess() {
        viewModel.inputs.sendSMSVerification()
        XCTAssert(viewController.didPresentVerifyCodeStep)
    }
    
    func testSendSMSVerificationFailure() {
        service.sendSMSVerificationMockReturn = .failure(MockError.genericError)
        viewModel.inputs.sendSMSVerification()
        XCTAssert(viewController.didPresentSMSVerificationError)
    }
}
