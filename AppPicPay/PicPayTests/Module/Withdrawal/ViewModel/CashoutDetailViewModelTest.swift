//
//  CashoutDetailViewModelTest.swift
//  PicPayTests
//
//  Created by Raphael Carletti on 23/01/19.
//

import XCTest
@testable import PicPay

class CashoutDetailViewModelTest: XCTestCase {
    func viewModelSuccessApi() -> CashoutDetailViewModel {
        let cashoutRequestPending = CashoutRequestPending(affiliationCode: "123456", userId: "27 99889 0902", pin: "1234", value: "R$100,00", disclaimer: "<font color=\"#8f929d\"><strong>Importante: </strong>O pedido de saque é válido até </font><font color=\"#ff5075\">22:59</font><font color=\"#8f929d\"> de hoje (08/01/2019). O saque não pode ser feito por terceiros. Nunca passe os seu dados para outras pessoas além do atendente da lotérica.</font>")
        return CashoutDetailViewModel(cashoutRequestPending: cashoutRequestPending, api: WithdrawApiSuccessMock(type: .cancelCashout))
    }
    
    func viewModelErrorApi() -> CashoutDetailViewModel {
        let cashoutRequestPending = CashoutRequestPending(affiliationCode: "123456", userId: "27 99889 0902", pin: "1234", value: "R$100,00", disclaimer: "<font color=\"#8f929d\"><strong>Importante: </strong>O pedido de saque é válido até </font><font color=\"#ff5075\">22:59</font><font color=\"#8f929d\"> de hoje (08/01/2019). O saque não pode ser feito por terceiros. Nunca passe os seu dados para outras pessoas além do atendente da lotérica.</font>")
        return CashoutDetailViewModel(cashoutRequestPending: cashoutRequestPending, api: WithdrawApiErrorMock())
    }
    
    func testCancelCashoutSuccess() {
        let viewModel = viewModelSuccessApi()
        viewModel.cancelCashout { (error) in
            XCTAssertNil(error)
        }
    }
    
    func testCancelCashoutError() {
        let viewModel = viewModelErrorApi()
        viewModel.cancelCashout { (error) in
            XCTAssertNotNil(error)
        }
    }
    
    func testAfilliationCodeSuccess() {
        let viewModel = viewModelSuccessApi()
        XCTAssertEqual(viewModel.getSetupCashoutInfo(for: .affiliationCode).value, "123456")
    }
    
    func testAfilliationCodeError() {
        let viewModel = viewModelSuccessApi()
        XCTAssertNotEqual(viewModel.getSetupCashoutInfo(for: .affiliationCode).value, "test")
    }
    
    func testUserIdSuccess() {
        let viewModel = viewModelSuccessApi()
        XCTAssertEqual(viewModel.getSetupCashoutInfo(for: .userId).value, "27 99889 0902")
    }
    
    func testUserIdError() {
        let viewModel = viewModelSuccessApi()
        XCTAssertNotEqual(viewModel.getSetupCashoutInfo(for: .userId).value, "test")
    }
    
    func testPinSuccess() {
        let viewModel = viewModelSuccessApi()
        XCTAssertEqual(viewModel.getSetupCashoutInfo(for: .pin).value, "1234")
    }
    
    func testPinError() {
        let viewModel = viewModelSuccessApi()
        XCTAssertNotEqual(viewModel.getSetupCashoutInfo(for: .pin).value, "test")
    }
    
    func testValueSuccess() {
        let viewModel = viewModelSuccessApi()
        XCTAssertEqual(viewModel.getSetupCashoutInfo(for: .value).value, "R$100,00")
    }
    
    func testValueError() {
        let viewModel = viewModelSuccessApi()
        XCTAssertNotEqual(viewModel.getSetupCashoutInfo(for: .value).value, "test")
    }
    
    func testDisclaimerSuccess() {
        let viewModel = viewModelSuccessApi()
        XCTAssertEqual(viewModel.disclaimer, "<font color=\"#8f929d\"><strong>Importante: </strong>O pedido de saque é válido até </font><font color=\"#ff5075\">22:59</font><font color=\"#8f929d\"> de hoje (08/01/2019). O saque não pode ser feito por terceiros. Nunca passe os seu dados para outras pessoas além do atendente da lotérica.</font>")
    }
    
    func testDisclaimerError() {
        let viewModel = viewModelSuccessApi()
        XCTAssertNotEqual(viewModel.disclaimer, "test")
    }
    
    func testInstanciateController() {
        let viewModel = viewModelSuccessApi()
        XCTAssertNotNil(CashoutDetailViewController(viewModel: viewModel))
    }
    
    func testCashoutDetailControllerInstantiate() {
        let viewModel = viewModelSuccessApi()
        let controller = CashoutDetailViewController(viewModel: viewModel)
        XCTAssertNotNil(controller)
    }
}
