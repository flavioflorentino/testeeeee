import FeatureFlag
import XCTest
@testable import PicPay

class WithdrawalFormViewModelTest: XCTestCase {
    private let featureManagerMock = FeatureManagerMock()
    func viewModelSuccessMock(type: WithdrawType = .lotericas, hasPendingWithdrawal: Bool = false, mockType: WithdrawMockType = .withdrawAllOptions) -> WithdrawalFormViewModel {
        return WithdrawalFormViewModel(withdrawalType: type, hasPendingWithdrawal: hasPendingWithdrawal, api: WithdrawApiSuccessMock(type: mockType), dependencies: DependencyContainerMock(featureManagerMock))
    }
    
    func viewModelErrorMock(type: WithdrawType = .bank, hasPendingWithdrawal: Bool = false) -> WithdrawalFormViewModel {
        return WithdrawalFormViewModel(withdrawalType: type, hasPendingWithdrawal: hasPendingWithdrawal, api: WithdrawApiErrorMock(), dependencies: DependencyContainerMock(featureManagerMock))
    }
    
    func testGetFormHeaderBank() {
        let viewModel = viewModelSuccessMock(type: .bank)
        let header = viewModel.formHeader()
        XCTAssertTrue(header is WithdrawalFormHeaderView)
    }
    
    func testGetFormHeaderOriginal() {
        let viewModel = viewModelSuccessMock(type: .original)
        let header = viewModel.formHeader()
        XCTAssertTrue(header is WithdrawalFormHeaderView)
    }
    
    func testGetFormHeaderCashout() {
        let viewModel = viewModelSuccessMock(type: .lotericas)
        let header = viewModel.formHeader()
        XCTAssertTrue(header.isKind(of: UIView.self))
        XCTAssertEqual(header.subviews.count, 1)
        XCTAssertTrue(header.subviews[0] is UILabel)
    }
    
    func testMakeWithdrawal() {
        let viewModel = viewModelSuccessMock(type: .lotericas, mockType: .cashoutNewRequest)
        viewModel.makeWithdrawal(value: 5.00, pin: "1234") { (error) in
            if error == nil {
                let cashoutRequest = viewModel.cashoutRequest
                XCTAssertEqual(cashoutRequest?.status, .pending)
                XCTAssertEqual(cashoutRequest?.withdrawalCef?.affiliationCode, "123456")
                XCTAssertEqual(cashoutRequest?.withdrawalCef?.userId, "27 99889 0902")
                XCTAssertEqual(cashoutRequest?.withdrawalCef?.pin, "1234")
                XCTAssertEqual(cashoutRequest?.withdrawalCef?.value, "R$100,00")
                XCTAssertEqual(cashoutRequest?.withdrawalCef?.disclaimer, "<font color=\"#8f929d\"><strong>Importante: </strong>O pedido de saque é válido até </font><font color=\"#ff5075\">22:59</font><font color=\"#8f929d\"> de hoje (08/01/2019). O saque não pode ser feito por terceiros. Nunca passe os seu dados para outras pessoas além do atendente da lotérica.</font>")
            } else {
                XCTFail(error!.localizedDescription)
            }
        }
    }
    
    func testMakeWithdrawalFailure() {
        let viewModel = viewModelErrorMock(type: .lotericas)
        viewModel.makeWithdrawal(value: 5.00, pin: "1234") { (error) in
            XCTAssertNotNil(error)
        }
    }
    
    func testGetWithdrawCashoutRequestAvailable() {
        let viewModel = viewModelSuccessMock(type: .lotericas, mockType: .getCashoutLimit)
        viewModel.requestLastWithdraw(hasPendingWithdrawal: false, completion: { needToProcess, error in
            if error == nil {
                let cashoutRequest = viewModel.cashoutRequest
                XCTAssertNotNil(cashoutRequest?.limit)
                XCTAssertNil(cashoutRequest?.withdrawalCef)
                XCTAssertFalse(needToProcess)
            } else {
                XCTFail(error!.localizedDescription)
            }
        },  showSchedule: { _,_  in })
    }
    
    func testGetWithdrawCashoutRequestPending() {
        let viewModel = viewModelSuccessMock(type: .lotericas, mockType: .getCashoutPending)
        viewModel.requestLastWithdraw(hasPendingWithdrawal: false, completion: { needToProcess, error in
            if error == nil {
                let cashoutRequest = viewModel.cashoutRequest
                XCTAssertNil(cashoutRequest?.limit)
                XCTAssertNotNil(cashoutRequest?.withdrawalCef)
                XCTAssertTrue(needToProcess)
            } else {
                XCTFail(error!.localizedDescription)
            }
        },  showSchedule: { _,_  in })
    }
    
    func testGetActionButtonTextWithdrawSuccess() {
        let viewModel = viewModelSuccessMock(type: .bank)
        XCTAssertEqual(viewModel.actionButtonText(), WithdrawStrings.withdrawActionButtonText)
    }
    
    func testGetActionButtonTextWithdrawError() {
        let viewModel = viewModelSuccessMock(type: .bank)
        XCTAssertNotEqual(viewModel.actionButtonText(), "test")
    }
    
    func testGetActionButtonTextCashoutSuccess() {
        let viewModel = viewModelSuccessMock(type: .lotericas)
        XCTAssertEqual(viewModel.actionButtonText(), WithdrawStrings.cashoutActionButtonText)
    }
    
    func testGetActionButtonTextCashoutError() {
        let viewModel = viewModelSuccessMock(type: .lotericas)
        XCTAssertNotEqual(viewModel.actionButtonText(), "test2")
    }
    
    func testGetTitleWithdrawalLabelTextWithdrawSuccess() {
        let viewModel = viewModelSuccessMock(type: .bank)
        XCTAssertEqual(viewModel.titleWithdrawalLabelText(), WithdrawStrings.withdrawTitleText)
    }
    
    func testGetTitleWithdrawalLabelTextWithdrawError() {
        let viewModel = viewModelSuccessMock(type: .bank)
        XCTAssertNotEqual(viewModel.titleWithdrawalLabelText(), "test")
    }
    
    func testGetTitleWithdrawalLabelTextCashoutSuccess() {
        let viewModel = viewModelSuccessMock(type: .lotericas)
        XCTAssertEqual(viewModel.titleWithdrawalLabelText(), WithdrawStrings.cashoutTitleText)
    }
    
    func testGetTitleWithdrawalLabelTextCashoutError() {
        let viewModel = viewModelSuccessMock(type: .lotericas)
        XCTAssertNotEqual(viewModel.titleWithdrawalLabelText(), "test2")
    }
    
    func testGetAvalibleWithdrawlLabelTextWithdrawSuccess() {
        let viewModel = viewModelSuccessMock(type: .lotericas, mockType: .getCashoutLimit)
        viewModel.requestLastWithdraw(hasPendingWithdrawal: false, completion: { _, error in
            if error == nil {
                XCTAssertTrue(viewModel.avalibleWithdrawlLabelText().string.contains(WithdrawStrings.cashoutAvailableFounds))
            } else {
                XCTFail(error!.localizedDescription)
            }
        },showSchedule: { _,_  in })
    }
    
    func testGetAvalibleWithdrawlLabelTextWithdrawError() {
        let viewModel = viewModelSuccessMock(type: .lotericas, mockType: .getCashoutLimit)
        viewModel.requestLastWithdraw(hasPendingWithdrawal: false, completion: { _, error in
            if error == nil {
                XCTAssertFalse(viewModel.avalibleWithdrawlLabelText().string.contains("test"))
            } else {
                XCTFail(error!.localizedDescription)
            }
        }, showSchedule: { _,_  in })
    }
    
    func testGetHeaderBorderWidthCashoutSuccess() {
        let viewModel = viewModelSuccessMock(type: .lotericas)
        XCTAssertEqual(viewModel.headerBorderWidth(), 0.0)
    }
    
    func testGetHeaderBorderWidthCashoutError() {
        let viewModel = viewModelSuccessMock(type: .lotericas)
        XCTAssertNotEqual(viewModel.headerBorderWidth(), 1.0)
    }
    
    func testGetHeaderBorderWidthWithdrawSuccess() {
        let viewModel = viewModelSuccessMock(type: .bank)
        XCTAssertEqual(viewModel.headerBorderWidth(), 0.5)
    }
    
    func testGetHeaderBorderWidthWithdrawError() {
        let viewModel = viewModelSuccessMock(type: .bank)
        XCTAssertNotEqual(viewModel.headerBorderWidth(),  1.0)
    }
    
    func testInstanciateController() {
        let viewModel = viewModelSuccessMock()
        XCTAssertNotNil(WithdrawalFormViewController(with: viewModel))
    }
    
    func testSanitizedValueSuccess() {
        let viewModel = viewModelSuccessMock()
        XCTAssertEqual(viewModel.sanitized(value: "200,00"), 200.00)
    }
    
    func testSanitizedValueBiggerSuccess() {
        let viewModel = viewModelSuccessMock()
        XCTAssertEqual(viewModel.sanitized(value: "1.200,00"), 1200.00)
    }
    
    func testSanitizedValueError() {
        let viewModel = viewModelSuccessMock()
        XCTAssertNotEqual(viewModel.sanitized(value: "R$ 200,00"), 3.00)
    }
    
    func testSanitizedValueNotNil() {
        let viewModel = viewModelSuccessMock()
        XCTAssertNotNil(viewModel.sanitized(value: "R$ 200,00"))
    }
    
    func testSanitizedValueNil() {
        let viewModel = viewModelSuccessMock()
        XCTAssertNil(viewModel.sanitized(value: nil))
    }
    
}
