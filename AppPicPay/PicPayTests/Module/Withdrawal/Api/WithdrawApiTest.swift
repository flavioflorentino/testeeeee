import Foundation
import SwiftyJSON
@testable import PicPay

enum WithdrawMockType: String {
    case withdrawAllOptions = "WithdrawAllOptions"
    case cashoutNewRequest = "CashoutNewRequest"
    case getCashoutLimit = "GetCashoutLimit"
    case getCashoutPending = "GetCashoutPending"
    case cancelCashout = "CancelCashout"
}

class WithdrawApiSuccessMock: WithdrawApiProtocol {
    let type: WithdrawMockType
    
    init(type: WithdrawMockType) {
        self.type = type
    }
    
    func getJSON() -> JSON {
        return MockJSON().load(resource: type.rawValue)
    }
    
    func getLastWithdrawal(completion: @escaping (PicPayResult<LastWithdrawalData>) -> Void) {
        let ppWithdraw = PPWithdrawal(dictionary: [:])
        ppWithdraw.windowInfo = CashoutScheduleAlert(warnUser: true, title: "Titulo", text: "Texto")
        
        let data = LastWithdrawalData(withdraw: ppWithdraw, availableWithdrawalModel: nil, bank: nil)
            completion(.success(data))
    }
    
    
    
    func loadWithdrawCashoutRequest(success: @escaping (CashoutRequest) -> Void, failure: @escaping (PicPayError) -> Void) {
        let json = getJSON()
        let decoder = JSONDecoder()
        if let data = try? json.rawData(),
            let cashoutRequest = try? decoder.decode(CashoutRequest.self, from: data) {
            success(cashoutRequest)
        } else {
            let ppError = PicPayError(message: "JSON inválido")
            failure(ppError)
        }
    }
    
    func cancelCashoutRequest(completion: @escaping (PicPayResult<BaseApiGenericResponse>) -> Void) {
        let json = getJSON()
        completion(PicPayResult<BaseApiGenericResponse>.success(BaseApiGenericResponse(json: json)!))
    }
    
    func makeNewCashoutRequest(value: Double, success: @escaping (CashoutRequestPending) -> Void, failure: @escaping (PicPayError) -> Void) {
        let json = getJSON()
        let decoder = JSONDecoder()
        if let data = try? json.rawData(),
            let cashoutRequest = try? decoder.decode(CashoutRequestPending.self, from: data) {
            success(cashoutRequest)
        } else {
            let ppError = PicPayError(message: "JSON inválido")
            failure(ppError)
        }
    }
}

class WithdrawApiErrorMock: WithdrawApiProtocol {
    func loadWithdrawCashoutRequest(success: @escaping (CashoutRequest) -> Void, failure: @escaping (PicPayError) -> Void) {
        failure(PicPayError(message: "Error"))
    }
    
    func cancelCashoutRequest(completion: @escaping (PicPayResult<BaseApiGenericResponse>) -> Void) {
        completion(PicPayResult.failure(PicPayError(message: "Error")))
    }
    
    func makeNewCashoutRequest(value: Double, success: @escaping (CashoutRequestPending) -> Void, failure: @escaping (PicPayError) -> Void) {
        failure(PicPayError(message: "Error"))
    }
    func getLastWithdrawal(completion: @escaping (PicPayResult<LastWithdrawalData>) -> Void) {
        completion(.failure(PicPayError(message: "Error")))
    }
}
