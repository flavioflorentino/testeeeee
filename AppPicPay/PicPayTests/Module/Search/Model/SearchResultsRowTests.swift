import FeatureFlag
import XCTest

@testable import PicPay

final class SearchResultsRowTests: XCTestCase {
    func testInit_WithNewConsumerRowType_ShouldPopulateCorrectFields() throws {
        let newRow = SearchRowMockFactory.createNewSearchRow(type: .consumer)
        let oldRow = LegacySearchResultRow(row: newRow)
        let contact = try XCTUnwrap(oldRow.fullData as? PPContact)

        XCTAssertEqual(oldRow.type, .person)
        XCTAssertEqual(oldRow.basicData.type, .person)
        XCTAssertEqual(oldRow.basicData.id, "1234")
        XCTAssertEqual(oldRow.basicData.imageUrl, "url")
        XCTAssertEqual(oldRow.basicData.title, "@ashley")
        XCTAssertEqual(oldRow.basicData.descriptionText, "Ashley Johnson")
        XCTAssertEqual(oldRow.basicData.titleDetail, nil)
        XCTAssertEqual(oldRow.basicData.isPro, true)
        XCTAssertEqual(oldRow.basicData.isVerified, true)
        XCTAssertEqual(oldRow.basicData.canReceivePaymentRequest, false)

        XCTAssertEqual(contact.wsId, 1234)
        XCTAssertEqual(contact.isVerified, true)
        XCTAssertEqual(contact.onlineName, "Ashley Johnson")
        XCTAssertEqual(contact.businessAccount, true)
        XCTAssertEqual(contact.imgUrl, "url")
        XCTAssertEqual(contact.imgUrlLarge, "url")
        XCTAssertEqual(contact.isPicpayUser, true)
        XCTAssertEqual(contact.contactType, .profilePopUp)
        XCTAssertEqual(contact.username, "ashley")
    }

    func testInit_WithNewStoreRowType_ShouldPopulateCorrectFields() throws {
        let newRow = SearchRowMockFactory.createNewSearchRow(type: .store)
        let oldRow = LegacySearchResultRow(row: newRow)
        let store = try XCTUnwrap(oldRow.fullData as? PPStore)

        XCTAssertEqual(oldRow.type, .store)
        XCTAssertEqual(oldRow.basicData.type, .store)
        XCTAssertEqual(oldRow.basicData.id, "1234")
        XCTAssertEqual(oldRow.basicData.imageUrl, "url")
        XCTAssertEqual(oldRow.basicData.title, "Bar da esquina")
        XCTAssertEqual(oldRow.basicData.descriptionText, "Avenida Paulista, 2000")
        XCTAssertEqual(oldRow.basicData.titleDetail, "9728 km")
        XCTAssertEqual(oldRow.basicData.isPro, nil)
        XCTAssertEqual(oldRow.basicData.isVerified, nil)
        XCTAssertEqual(oldRow.basicData.canReceivePaymentRequest, nil)

        XCTAssertEqual(store.storeId, "1234")
        XCTAssertEqual(store.isVerified, true)
        XCTAssertEqual(store.img_url, "url")
        XCTAssertEqual(store.isParkingPayment, 2)
        XCTAssertEqual(store.distance, 9728320)
    }

    func testInit_WithNewDigitalRowType_ShouldPopulateCorrectFields() throws {
        let newRow = SearchRowMockFactory.createNewSearchRow(type: .digitalGood(service: .phoneRecharge))
        let oldRow = LegacySearchResultRow(row: newRow)
        let dgItem = try XCTUnwrap(oldRow.fullData as? DGItem)

        XCTAssertEqual(oldRow.type, .digitalGood)
        XCTAssertEqual(oldRow.basicData.type, .digitalGood)
        XCTAssertEqual(oldRow.basicData.id, "1234")
        XCTAssertEqual(oldRow.basicData.imageUrl, "url")
        XCTAssertEqual(oldRow.basicData.title, "Recarga de Celular")
        XCTAssertEqual(oldRow.basicData.descriptionText, "Vivo, Claro, Tim, Oi")
        XCTAssertEqual(oldRow.basicData.titleDetail, nil)
        XCTAssertEqual(oldRow.basicData.isPro, nil)
        XCTAssertEqual(oldRow.basicData.isVerified, nil)
        XCTAssertEqual(oldRow.basicData.canReceivePaymentRequest, nil)

        XCTAssertEqual(dgItem.name, "Recarga de Celular")
        XCTAssertEqual(dgItem.itemDescription, "Vivo, Claro, Tim, Oi")
        XCTAssertEqual(dgItem.imgUrl, "url")
        XCTAssertEqual(dgItem.isStudentAccount, true)
    }

    func testInit_WithNewP2MRowType_ShouldPopulateCorrectFields() throws {
        let newRow = SearchRowMockFactory.createNewSearchRow(type: .p2m(service: .cielo))
        let oldRow = LegacySearchResultRow(row: newRow)
        let p2mItem = try XCTUnwrap(oldRow.fullData as? P2MItem)

        XCTAssertEqual(oldRow.type, .p2m)
        XCTAssertEqual(oldRow.basicData.type, .p2m)
        XCTAssertEqual(oldRow.basicData.id, "1234")
        XCTAssertEqual(oldRow.basicData.imageUrl, "url")
        XCTAssertEqual(oldRow.basicData.title, "Pagar nas Maquininhas")
        XCTAssertEqual(oldRow.basicData.descriptionText, "Pague com o PicPay em máquinas Cielo e Getnet escaneando o QR code na máquina")
        XCTAssertEqual(oldRow.basicData.titleDetail, nil)
        XCTAssertEqual(oldRow.basicData.isPro, nil)
        XCTAssertEqual(oldRow.basicData.isVerified, nil)
        XCTAssertEqual(oldRow.basicData.canReceivePaymentRequest, nil)

        XCTAssertEqual(p2mItem.name, "Pagar nas Maquininhas")
        XCTAssertEqual(p2mItem.descriptionText, "Pague com o PicPay em máquinas Cielo e Getnet escaneando o QR code na máquina")
        XCTAssertEqual(p2mItem.service, .cielo)
    }

    func testInit_WithNewMembershipRowType_ShouldPopulateCorrectFields() throws {
        let featureManagerMock = FeatureManagerMock()
        featureManagerMock.override(key: .subscription, with: true)
        let newRow = SearchRowMockFactory.createNewSearchRow(type: .membership)
        let oldRow = LegacySearchResultRow(row: newRow, featureManager: featureManagerMock)
        let p2mItem = try XCTUnwrap(oldRow.fullData as? ProducerProfileItem)

        XCTAssertEqual(oldRow.type, .subscription)
        XCTAssertEqual(oldRow.basicData.type, .subscription)
        XCTAssertEqual(oldRow.basicData.id, "1234")
        XCTAssertEqual(oldRow.basicData.imageUrl, "url")
        XCTAssertEqual(oldRow.basicData.title, "gugacast")
        XCTAssertEqual(oldRow.basicData.descriptionText, "Guguinha dos Paranaue_2.0")
        XCTAssertEqual(oldRow.basicData.titleDetail, "bio")
        XCTAssertEqual(oldRow.basicData.isPro, nil)
        XCTAssertEqual(oldRow.basicData.isVerified, nil)
        XCTAssertEqual(oldRow.basicData.canReceivePaymentRequest, nil)

        XCTAssertEqual(p2mItem.name, "gugacast")
        XCTAssertEqual(p2mItem.username, "gugacast")
        XCTAssertEqual(p2mItem.profileImageUrl, "url")
        XCTAssertEqual(p2mItem.profileDescription, "bio")
    }
}

