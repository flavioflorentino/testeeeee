import XCTest
@testable import PicPay

final class SearchResultsSectionTests: XCTestCase {
    func testInit_WithNewConsumerRowType_ShouldPopulateCorrectFields() throws {
        let newSection = createSection()
        let oldSection = LegacySearchResultSection(section: newSection)

        XCTAssertEqual(oldSection.title, "LOJAS")
        XCTAssertEqual(oldSection.rows.count, 2)

        let firstRow = try XCTUnwrap(oldSection.rows.first)
        let lastRow = try XCTUnwrap(oldSection.rows.last)

        XCTAssertEqual(firstRow.basicData.title, "Recarga de Celular")
        XCTAssertEqual(lastRow.basicData.title, "Recarga de Celular")
        XCTAssertEqual(oldSection.viewType, .list)
        XCTAssertEqual(oldSection.hasMore, false)
    }

    private func createSection() -> SearchResultSection {
        let rows = [SearchRowMockFactory.createNewSearchRow(type: .digitalGood(service: .phoneRecharge)),
                    SearchRowMockFactory.createNewSearchRow(type: .digitalGood(service: .digitalcodes))]
        return SearchResultSection(sectionTitle: "LOJAS", rows: rows, totalCount: 2)
    }
}
