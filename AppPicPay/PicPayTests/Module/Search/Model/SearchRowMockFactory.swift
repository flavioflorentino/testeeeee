@testable import PicPay

final class SearchRowMockFactory {
    class func createNewSearchRow(type: SearchResultRowTestType) -> SearchResultRow {
        let row = SearchResultRow(type: type.searchType,
                                  id: "1234",
                                  title: type.title,
                                  body: type.body,
                                  footer: type.footer,
                                  profileImageUrl: "url",
                                  profileTags: .init(isVerified: true, isPro: true, isStudent: true),
                                  legacyTags: .init(isCielo: true, isParking: true, service: type.service))

        return row
    }

    enum SearchResultRowTestType {
        case p2m(service: P2MItem.ServiceType)
        case digitalGood(service: DGService)
        case consumer
        case store
        case membership

        var searchType: SearchType {
            switch self {
            case .p2m: return .p2m
            case .digitalGood: return .digitalGood
            case .membership: return .membership
            case .store: return .store
            case .consumer: return .consumer
            }
        }

        var service: String {
            switch self {
            case .p2m(let service): return service.rawValue
            case .digitalGood(let service): return service.rawValue
            default: return ""
            }
        }

        var title: String {
            switch self {
            case .consumer: return "ashley"
            case .store: return "Bar da esquina"
            case .digitalGood: return "Recarga de Celular"
            case .p2m: return "Pagar nas Maquininhas"
            case .membership: return "gugacast"
            }
        }

        var body: String {
            switch self {
            case .consumer: return "Ashley Johnson"
            case .store: return "Avenida Paulista, 2000"
            case .digitalGood: return "Vivo, Claro, Tim, Oi"
            case .p2m: return "Pague com o PicPay em máquinas Cielo e Getnet escaneando o QR code na máquina"
            case .membership: return "Guguinha dos Paranaue_2.0"
            }
        }

        var footer: String? {
            switch self {
            case .store: return "9728320.875857478"
            case .membership: return "bio"
            default: return nil
            }
        }
    }
}
