import XCTest
@testable import PicPay

final class ParkingVehicleEditViewModelTest: XCTestCase {
    private let mock = ParkingStore.mockWithPlate()
    private var types: [ParkingType] = []
    private lazy var coordinator = ParkingCoordinator(viewController: UIViewController())
    
    override func setUpWithError() throws {
        types = try XCTUnwrap(mock?.areas.first?.types)
    }
    
    func testIfPlateCameFilled() {
        var model = ParkingVehicleEditViewModel(types: types, coordinator: coordinator, plate: "PQP1212")
        
        let plate = model.getPlateAlreadyFilled()
        model.updatePlate(plate)
        model.updateType("Carro")
        XCTAssertTrue(model.canProceed())
    }

    func testUpdatePlateWithNil() {
        var model = ParkingVehicleEditViewModel(types: types, coordinator: coordinator)
        model.onStateChange = { state in
            XCTAssertFalse(state.plate)
        }
        model.updatePlate(nil)
    }
    
    func testUpdatePlateWithInvalidPlate() {
        var model = ParkingVehicleEditViewModel(types: types, coordinator: coordinator)
        model.onStateChange = { state in
            XCTAssertFalse(state.plate)
        }
        model.updatePlate("A111ABC")
    }
    
    func testUpdatePlateWithValidOldPlate() {
        var model = ParkingVehicleEditViewModel(types: types, coordinator: coordinator)
        model.onStateChange = { state in
            XCTAssertTrue(state.plate)
        }
        model.updatePlate("AAA1212")
    }
    
    func testUpdatePlateWithValidNewPlate() {
        var model = ParkingVehicleEditViewModel(types: types, coordinator: coordinator)
        model.onStateChange = { state in
            XCTAssertTrue(state.plate)
        }
        model.updatePlate("AAA1A12")
    }
    
    func testUpdateWithTypeNil() {
        var model = ParkingVehicleEditViewModel(types: types, coordinator: coordinator)
        model.onStateChange = { state in
            XCTAssertFalse(state.type)
        }
        model.updateType(nil)
    }
    
    func testUpdateWithTypeInvalid() {
        var model = ParkingVehicleEditViewModel(types: types, coordinator: coordinator)
        model.onStateChange = { state in
            XCTAssertFalse(state.type)
        }
        model.updateType("Avião")
    }
    
    func testUpdateWithValidType() {
        var model = ParkingVehicleEditViewModel(types: types, coordinator: coordinator)
        model.onStateChange = { state in
            XCTAssertTrue(state.type)
        }
        model.updateType("Moto")
        XCTAssertNil(model.vehicle())
    }
    
    func testUpdateBothOk() {
        var model = ParkingVehicleEditViewModel(types: types, coordinator: coordinator)
        model.updateType("Moto")
        model.updatePlate("AAA1A12")
        XCTAssertTrue(model.canProceed())
        XCTAssertNotNil(model.vehicle())
    }
    
    func testIfVehicleWasUpdatedOnParkingRoot() throws {
        var model = ParkingVehicleEditViewModel(types: types, coordinator: coordinator)
        
        let plate = "AAA1A12"
        model.updateType("Moto")
        model.updatePlate(plate)
        
        let vehicle = try XCTUnwrap(model.vehicle())
        model.goBackToRoot(with: vehicle)
        
        let selectedPlate = try XCTUnwrap(coordinator.parkingViewModel.userSelectedVehicle)
        XCTAssertEqual(selectedPlate, plate)
    }
}
