import XCTest
@testable import PicPay

class PlateValidatorTest: XCTestCase {

    func testValidOldPlate() {
        let plate = ["MMT0000", "UYY7563", "RSA0099"]
        plate.forEach( { plate in XCTAssertTrue(PlateValidator.isValid(plate)) })
    }
    
    func testValidNewPlate() {
        let plate = ["AAA1A11", "ABC1D12", "PPP9D88"]
        plate.forEach( { plate in XCTAssertTrue(PlateValidator.isValid(plate)) })
    }
    
    func testInvalidPlate() {
        let plate = ["1AAAAAA", "AA1AAAAA", "0", "MMT000A", "AAA0AA0", "[AB1234", "^AB1234", "_AB1234", "`AB1234", " AB1234"]
        plate.forEach( { plate in XCTAssertFalse(PlateValidator.isValid(plate)) })
    }
    
    func testTryUnmaskNewPlate() {
        let newMaskedPlate = "AAA 1A11"
        XCTAssertEqual("AAA1A11", PlateValidator.unmask(newMaskedPlate))
    }
    
    func testTryUnmaskOldPlate() {
        let newMaskedPlate = "UYY 7563"
        XCTAssertEqual("UYY7563", PlateValidator.unmask(newMaskedPlate))
    }
    
    func testTryUnmaskIncompletePlate() {
        let newMaskedIncompletePlate = "UYY 7A"
        XCTAssertEqual("UYY7A", PlateValidator.unmask(newMaskedIncompletePlate))
    }
    
    func testTryUnmaskInvalidPlate() {
        let newMaskedIncompletePlate = "1AAAAAA1AAAAAA1AAAAAA1AAAAAA"
        XCTAssertEqual("1AAAAAA1AAAAAA1AAAAAA1AAAAAA", PlateValidator.unmask(newMaskedIncompletePlate))
    }
    
    func testTryMaskInvalid() {
        let invalidPlate = "[1234"
        XCTAssertEqual(PlateValidator.mask(invalidPlate), "")
    }
}
