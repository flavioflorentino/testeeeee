@testable import PicPay

extension ParkingStore {
    static func mock() -> ParkingStore? {
        return ParkingStore.loadFromMockWith(resource: "ParkingMock")
    }
    
    static func mockWithPlate() -> ParkingStore? {
        return ParkingStore.loadFromMockWith(resource: "ParkingMockHavePlate")
    }
    
    static func mockWithArea() -> ParkingStore? {
        return ParkingStore.loadFromMockWith(resource: "ParkingMockSelectedArea")
    }
    
    static func mockWithAreaAndPlate() -> ParkingStore? {
        return ParkingStore.loadFromMockWith(resource: "ParkingMockSelectedAreaAndPlate")
    }
    
    static func mockWithAreaSelectedAndVehicleWithoutType() -> ParkingStore? {
        return ParkingStore.loadFromMockWith(resource: "ParkingMockSelectedAreaAndVehicleWithoutType")
    }
    
    static func loadFromMockWith(resource: String) -> ParkingStore? {
        let bundle = Bundle.init(for: ParkingRootViewModelTest.self)
        if let path = bundle.path(forResource: resource, ofType: "json") {
            if let jsonData = NSData(contentsOfFile: path) {
                let decoder = JSONDecoder()
                if let parkingStore = try? decoder.decode(ParkingStore.self, from: jsonData as Data) {
                    return parkingStore
                }
            }
        }
        return nil
    }
}
