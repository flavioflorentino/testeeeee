import XCTest
@testable import PicPay

class ParkingModelTest: XCTestCase {
    func testMinutesFareLabel() {
        struct TestFare {
            let fare: ParkingFare
            let result: String
        }
        
        let testFares = [TestFare(fare: ParkingFare(minutes: "10", cents: "", amount: ""), result: "10min"),
                         TestFare(fare: ParkingFare(minutes: "60", cents: "", amount: ""), result: "1 hora"),
                         TestFare(fare: ParkingFare(minutes: "70", cents: "", amount: ""), result: "1 hora e 10min"),
                         TestFare(fare: ParkingFare(minutes: "120", cents: "", amount: ""), result: "2 horas"),
                         TestFare(fare: ParkingFare(minutes: "130", cents: "", amount: ""), result: "2 horas e 10min"),
                         TestFare(fare: ParkingFare(minutes: "88", cents: "", amount: ""), result: "1 hora e 28min"),
                         TestFare(fare: ParkingFare(minutes: "1440", cents: "", amount: ""), result: "24 horas"),
                         TestFare(fare: ParkingFare(minutes: "2880", cents: "", amount: ""), result: "48 horas"),
                         TestFare(fare: ParkingFare(minutes: "InvalidString", cents: "", amount: ""), result: "")]
        
        testFares.forEach { testFare in
            XCTAssertEqual(testFare.fare.minutesLabel, testFare.result)
        }
    }
    
    func testAddVehicle() {
        var model = ParkingStore.mockWithPlate()
        let vehicle = ParkingVehicle(plate: "ABC9090", type: "car")
        model?.updateOrAddVehicle(vehicle)
        XCTAssertTrue(model?.vehicles?.count == 5)
    }
    
    func testUpdateVehicle() {
        var model = ParkingStore.mockWithPlate()
        let vehicle = ParkingVehicle(plate: "ABC 1212", type: "bike")
        model?.updateOrAddVehicle(vehicle)
        XCTAssertTrue(model?.vehicleForPlate("ABC 1212")?.type == "bike")
    }
    
    func testUpdateVehicleWithType() {
        var model = ParkingStore.mockWithPlate()
        XCTAssertTrue(model?.vehicleForPlate("RTX 2080")?.type == nil)
        
        let vehicle = ParkingVehicle(plate: "RTX 2080", type: "bike")
        model?.updateOrAddVehicle(vehicle)
        XCTAssertTrue(model?.vehicleForPlate("RTX 2080")?.type == "bike")
        XCTAssertTrue(model?.vehicles?.count == 4)
    }
    
    func testRemoveVehicle() {
        var model = ParkingStore.mockWithPlate()
        let plate = "ABC 1212"
        model?.removeVehicle(plate)
        XCTAssertTrue(model?.vehicles?.count == 3)
    }
    
    func testRemoveVehicleWhenThereAreNoVehicles() {
        var model = ParkingStore.mock()
        let plate = "ABC 1212"
        let vehicles = model?.vehicles
        model?.removeVehicle(plate)
        XCTAssertEqual(vehicles, model?.vehicles)
    }
    
    func testHasAreaSelected() {
        let model = ParkingStore.mockWithArea()
        let areaSelected = model?.areaSelected
        XCTAssertEqual("1", areaSelected?.id)
        XCTAssertEqual("Centro / Vila Rubim", areaSelected?.name)
    }
    
    func testDoesntHaveArea() {
        let model = ParkingStore.mock()
        XCTAssertNil(model?.areaSelected)
    }
    
    func testGoToVehicleAreaNotSelected() {
        let mock = ParkingStore.mock()
        let viewModel = ParkingRootViewModel()
        viewModel.store = mock
        let expectation = XCTestExpectation(description: "expected needNeighborhood")
        viewModel.onToolTipAlert = { (messagen, state) in
            switch state {
            case .needNeighborhood:
                expectation.fulfill()
            default:
                XCTFail("Was expecting .needNeighborhood")
            }
        }
        viewModel.goToVehicle()
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testGoToVehiclePickOnList() {
        let mock = ParkingStore.mockWithAreaAndPlate()
        let viewModel = ParkingRootViewModel()
        viewModel.store = mock
        let expectation = XCTestExpectation(description: "expected pickOnList")
        viewModel.onTap = { goTo in
            switch goTo {
            case .pickOnList:
                expectation.fulfill()
                XCTAssert(true)
            default:
                XCTFail("expected .pickOnList")
            }
        }
        viewModel.goToVehicle()
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testGoToVehicleAddNew() {
        let mock = ParkingStore.mockWithArea()
        let viewModel = ParkingRootViewModel()
        viewModel.store = mock
        let expectation = XCTestExpectation(description: "expected addNew")
        viewModel.onTap = { goTo in
            switch goTo {
            case .addNew:
                expectation.fulfill()
                XCTAssert(true)
            default:
                XCTFail("expected .addNew")
            }
        }
        viewModel.goToVehicle()
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testGetBalanceWithoutSelectedFare() {
        let mock = ParkingStore.mockWithAreaAndPlate()
        let viewModel = ParkingRootViewModel()
        viewModel.store = mock
        XCTAssertNil(viewModel.getBalance())
    }
    
    func testGetBalanceAllFromUserBalance() {
        let mock = ParkingStore.mockWithAreaAndPlate()
        let viewModel = ParkingRootViewModel()
        viewModel.store = mock
        viewModel.userSelectedFare = ParkingFare(minutes: "30", cents: "120", amount: "R$ 1,20")
        
        let consumer = MBConsumer()
        consumer.balance = NSDecimalNumber(value: 2.0)
        ConsumerManager.shared.consumer = consumer
        ConsumerManager.setUseBalance(true)
        XCTAssertEqual(viewModel.getBalance()?.fromUserBalance, "1.2")
    }
    
    func testGetBalanceAllFromCredit() {
        let mock = ParkingStore.mockWithAreaAndPlate()
        let viewModel = ParkingRootViewModel()
        viewModel.store = mock
        viewModel.userSelectedFare = ParkingFare(minutes: "30", cents: "120", amount: "R$ 1,20")
        
        let consumer = MBConsumer()
        consumer.balance = NSDecimalNumber(value: 0.0)
        ConsumerManager.shared.consumer = consumer
        ConsumerManager.setUseBalance(true)
        XCTAssertEqual(viewModel.getBalance()?.fromCreditCard, "1.2")
    }
    
    func testGetBalanceFromBothCreditAndBalance() {
        let mock = ParkingStore.mockWithAreaAndPlate()
        let viewModel = ParkingRootViewModel()
        viewModel.store = mock
        viewModel.userSelectedFare = ParkingFare(minutes: "30", cents: "120", amount: "R$ 1,20")
        
        let consumer = MBConsumer()
        consumer.balance = NSDecimalNumber(value: 1.0)
        ConsumerManager.shared.consumer = consumer
        ConsumerManager.setUseBalance(true)
        XCTAssertEqual(viewModel.getBalance()?.fromCreditCard, "0.2")
        XCTAssertEqual(viewModel.getBalance()?.fromUserBalance, "1")
    }
    
    func testAllFromCreditUserChoseNotToUseBalance() {
        let mock = ParkingStore.mockWithAreaAndPlate()
        let viewModel = ParkingRootViewModel()
        viewModel.store = mock
        viewModel.userSelectedFare = ParkingFare(minutes: "30", cents: "120", amount: "R$ 1,20")
        
        let consumer = MBConsumer()
        consumer.balance = NSDecimalNumber(value: 1.0)
        ConsumerManager.shared.consumer = consumer
        _ = viewModel.getBalance()
        ConsumerManager.setUseBalance(false)
        
        XCTAssertEqual(viewModel.getBalance()!.fromCreditCard, "1.2")
        XCTAssertEqual(viewModel.getBalance()!.fromUserBalance, "0")
    }
    
    func testTriedToPayBeforeFillingAreaAndVehicle() {
        let mock = ParkingStore.mock()
        let viewModel = ParkingRootViewModel()
        viewModel.store = mock
        let expectation = XCTestExpectation(description: "expected needBoth")
        
        viewModel.onToolTipAlert = { (messagen, state) in
            switch state {
            case .needBoth:
                expectation.fulfill()
            default:
                XCTFail("Was expecting .needBoth")
            }
        }
        
        viewModel.pay()
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testTriedToPayBeforeFillingVehicle() {
        let mock = ParkingStore.mockWithArea()
        let viewModel = ParkingRootViewModel()
        viewModel.store = mock
        let expectation = XCTestExpectation(description: "expected needVehicle")
        
        viewModel.onToolTipAlert = { (messagen, state) in
            switch state {
            case .needVehicle:
                expectation.fulfill()
            default:
                XCTFail("Was expecting .needVehicle")
            }
        }
        
        viewModel.pay()
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testTrieToPayEverythingOk() {
        let mock = ParkingStore.mockWithAreaAndPlate()
        let viewModel = ParkingRootViewModel()
        viewModel.store = mock
        
        let expectation = XCTestExpectation(description: "expected pay")
        
        viewModel.onTap = { goTo in
            switch goTo {
            case .pay:
                expectation.fulfill()
                XCTAssert(true)
            default:
                XCTFail("expected .pay")
            }
        }
        
        viewModel.pay()
        wait(for: [expectation], timeout: 5.0)
    }
}
