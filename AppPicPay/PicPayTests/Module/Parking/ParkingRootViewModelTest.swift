import XCTest
@testable import PicPay

class ParkingRootViewModelTest: XCTestCase {
    
    func testViewModelInitialState() {
        struct TestViewModelState {
            let store: ParkingStore?
            let result: ParkingRootViewModel.State
        }
        
        let testCases = [TestViewModelState(store: ParkingStore.mock(), result: .needBoth),
                         TestViewModelState(store: ParkingStore.mockWithPlate(), result: .needNeighborhood),
                         TestViewModelState(store: ParkingStore.mockWithArea(), result: .needVehicle),
                         TestViewModelState(store: ParkingStore.mockWithAreaAndPlate(), result: .goodToGo),
                         TestViewModelState(store: ParkingStore.mockWithAreaSelectedAndVehicleWithoutType(), result: .needVehicle),
                         TestViewModelState(store: nil, result: .notLoaded)]
        
        testCases.forEach { testCase in
            let viewModel = ParkingRootViewModel()
            viewModel.store = testCase.store
            XCTAssertEqual(viewModel.currentState(), testCase.result)
        }
    }
    
    func testGetTheCorrectArea() {
        let mock = ParkingStore.mockWithArea()
        var viewModel = ParkingRootViewModel()
        viewModel.store = mock
        
        var result = viewModel.getArea!
        XCTAssertEqual("1", result.id)
        XCTAssertEqual("Centro / Vila Rubim", result.name)
        
        //User Selected another area
        viewModel.userSelectedArea = "2"
        result = viewModel.getArea!
        XCTAssertEqual("2", result.id)
        XCTAssertEqual("Bento Ferreira", result.name)
        
        //ParkingStore without Area Selected
        viewModel = ParkingRootViewModel()
        viewModel.store = ParkingStore.mock()
        XCTAssertNil(viewModel.getArea)
        
        //Invalid input fallback to default
        viewModel = ParkingRootViewModel()
        viewModel.store = ParkingStore.mockWithArea()
        viewModel.userSelectedArea = "21234"
        result = viewModel.getArea!
        XCTAssertEqual("1", result.id)
        XCTAssertEqual("Centro / Vila Rubim", result.name)
    }
    
    func testGetTheCorrectVehicle() {
        let mock = ParkingStore.mockWithPlate()
        let viewModel = ParkingRootViewModel()
        viewModel.store = mock
        
        var result = viewModel.getVehicle!
        XCTAssertEqual("PQP 1212", result.plate)
        XCTAssertEqual("car", result.type)
        
        viewModel.userSelectedVehicle = "MOT 1A12"
        result = viewModel.getVehicle!

        XCTAssertEqual("MOT 1A12", result.plate)
        XCTAssertEqual("bike", result.type)
    }
    
    func testVehicleLabel() {
        let mock = ParkingStore.mockWithAreaAndPlate()
        let viewModel = ParkingRootViewModel()
        viewModel.store = mock
        XCTAssertEqual("Carro - PQP 1212", viewModel.vehicleLabel)
    }
    
    func testIfFilteredListWithVehiclesWithCorrectTypeOrNil() {
        let mock = ParkingStore.mockWithPlate() //adicionar um veículo sem tipo
        let viewModel = ParkingRootViewModel()
        viewModel.store = mock
        viewModel.userSelectedArea = "2" //Bento-Ferreira has only cars allowed
        let withTypeBike = viewModel.filteredVehiclesList().first(where: { $0.type == "bike" })
        XCTAssertNil(withTypeBike)
        
        let withTypeNull = viewModel.filteredVehiclesList().first(where: { $0.type == nil })
        XCTAssertNotNil(withTypeNull)
    }
}
