import XCTest
import SwiftyJSON
@testable import PicPay

class AddNewAddressViewModelTest: XCTestCase {
    var addressExampleA: ConsumerAddressItem?
    
    override func setUp() {
        super.setUp()
        addressExampleA = try! MockCodable().loadCodableObject(resource: "AddressExampleA")
    }
    
    func viewModel(for state: ApiConsumerAddressMock.ApiConsumerAddressState) -> AddNewAddressViewModel {
        return AddNewAddressViewModel(api: ApiConsumerAddressMock(state: state))
    }

    func testSearchAddress() {
        let viewModel = self.viewModel(for: .addressListNotEmpty)
        let newAddressItem = ConsumerAddressItem(name: "Meu apartamento")
        newAddressItem.zipCode = "29175055"
        newAddressItem.streetNumber = "346"
        viewModel.newAddressItem = newAddressItem
        let expectedAddressItem = addressExampleA
        viewModel.searchAddress(by: newAddressItem.zipCode!) { (address, _) in
            XCTAssert(expectedAddressItem?.isLike(viewModel.newAddressItem) ?? false)
        }
    }

}
