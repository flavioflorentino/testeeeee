//
//  ApiConsumerAddressMock.swift
//  PicPayTests
//
//  Created by PicPay Eduardo on 12/24/18.
//

import Foundation
import SwiftyJSON
@testable import PicPay

class ApiConsumerAddressMock: BaseApi, ApiConsumerAddressProtocol {
    enum ApiConsumerAddressState {
        case undefined
        case addressListNotEmpty
        case addressListEmpty
        case addressListNil
        case genericError
        case connectionError
        
        var resource: String {
            switch self {
            case .addressListNotEmpty:
                return "AddressListNotEmpty"
            case .addressListEmpty:
                return "AddressListEmpty"
            default:
                return ""
            }
        }
    }
    
    private var state: ApiConsumerAddressState
    
    init(state: ApiConsumerAddressState){
        self.state = state
        super.init()
    }
    
    private func loadAddressListMock() -> [ConsumerAddressItem]? {
        switch state {
        case .addressListNotEmpty, .addressListEmpty:
            let json = MockJSON().load(resource: state.resource)
           var addressList = [ConsumerAddressItem]()
            if let jsonArray = json.array {
                for jsonAddress in jsonArray {
                    let address: ConsumerAddressItem? = jsonAddress.decoded()
                    if let address = address {
                        addressList.append(address)
                    }
                }
            }
            return addressList
            
        default:
            return nil
        }
    }
    
    func save(address: ConsumerAddressItem, _ completion: @escaping ((PicPayResult<ConsumerAddressItem>) -> Void)) {
        let savedAddress: ConsumerAddressItem = try! MockCodable().loadCodableObject(resource: "AddressExampleA")
        let result = PicPayResult<ConsumerAddressItem>.success(savedAddress)
        completion(result)
    }
    
    func update(address: ConsumerAddressItem, _ completion: @escaping ((PicPayResult<ConsumerAddressItem>) -> Void)) {
        let savedAddress: ConsumerAddressItem = try! MockCodable().loadCodableObject(resource: "AddressExampleA")
        let result = PicPayResult<ConsumerAddressItem>.success(savedAddress)
        completion(result)
    }
    
    func remove(addressId: Int, _ completion: @escaping ((PicPayResult<BaseCodableEmptyResponse>) -> Void)) {
        let response = BaseCodableEmptyResponse()
        completion(PicPayResult<BaseCodableEmptyResponse>.success(response))
    }
    
    func getAddressList(_ completion: @escaping ((PicPayResult<[ConsumerAddressItem]>) -> Void)) {
        switch state {
        case .addressListNotEmpty, .addressListEmpty:
            let baseResponse: [ConsumerAddressItem] = try! MockCodable().loadCodableObject(resource: state.resource)
            let result = PicPayResult<[ConsumerAddressItem]>.success(baseResponse)
            completion(result)
            
        case .connectionError:
            let result = PicPayResult<[ConsumerAddressItem]>.failure(MockError.connectionError)
            completion(result)
            
        case .genericError:
            let result = PicPayResult<[ConsumerAddressItem]>.failure(MockError.genericError)
            completion(result)
            
        default:
            break
        }
    }
    
    func getAddress(byId: String, _ completion: @escaping ((PicPayResult<ConsumerAddressItem>) -> Void)) {
        let savedAddress: ConsumerAddressItem = try! MockCodable().loadCodableObject(resource: "AddressExampleA")
        let result = PicPayResult<ConsumerAddressItem>.success(savedAddress)
        completion(result)
    }
    
    func getAddress(by zipcode: String, _ completion: @escaping ((PicPayResult<ConsumerAddressItem>) -> Void)) {
        let address: ConsumerAddressItem = try! MockCodable().loadCodableObject(resource: "AddressExampleA")
        let result = PicPayResult<ConsumerAddressItem>.success(address)
        completion(result)
    }
    
    
}
