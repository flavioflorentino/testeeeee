import XCTest
import SwiftyJSON
@testable import PicPay

class AddressListViewModelTest: XCTestCase {
    var addressExampleA: ConsumerAddressItem?
    var addressExampleB: ConsumerAddressItem?
    
    override func setUp() {
        super.setUp()
        addressExampleA = try! MockCodable().loadCodableObject(resource: "AddressExampleA")
        addressExampleB = try! MockCodable().loadCodableObject(resource: "AddressExampleB")
    }
    
    func viewModel(for state: ApiConsumerAddressMock.ApiConsumerAddressState) -> AddressListViewModel {
        return AddressListViewModel(api: ApiConsumerAddressMock(state: state))
    }
    
    func fetchAddressList(viewModel: AddressListViewModel) {
        let expectation = XCTestExpectation(description: "AddressListLoaded")
        viewModel.getAddressList { (_) in
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    // MARK: - Address list loaded
    
    func testAddressListLoaded() {
        let viewModel = self.viewModel(for: .addressListNotEmpty)
        fetchAddressList(viewModel: viewModel)
        XCTAssert(viewModel.isAddressListLoaded())
    }
    
    func testAddressListNotLoaded() {
        let viewModel = self.viewModel(for: .addressListNotEmpty)
        XCTAssertFalse(viewModel.isAddressListLoaded())
    }
    
    // MARK: - Get address
    
    func testGetAddressNotNil() {
        let viewModel = self.viewModel(for: .addressListNotEmpty)
        fetchAddressList(viewModel: viewModel)
        XCTAssertNotNil(viewModel.getAddress(at: 3))
    }
    
    func testGetAddressNilListEmpty() {
        let viewModel = self.viewModel(for: .addressListEmpty)
        fetchAddressList(viewModel: viewModel)
        XCTAssertNil(viewModel.getAddress(at: 0))
    }
    
    func testGetAddressNilListNil() {
        let viewModel = self.viewModel(for: .addressListNil)
        XCTAssertNil(viewModel.getAddress(at: 0))
    }
    
    // MARK: - Number of rows
    
    func testNumberOfRowsListNotEmpty() {
        let viewModel = self.viewModel(for: .addressListNotEmpty)
        fetchAddressList(viewModel: viewModel)
        let numOfRows = viewModel.getNumberOfRows()
        let expectedNumOfRows = viewModel.getAddressListSize() + 1
        XCTAssertEqual(numOfRows, expectedNumOfRows)
    }
    
    func testNumberOfRowsListNotEmptyFalse() {
        let viewModel = self.viewModel(for: .addressListNotEmpty)
        fetchAddressList(viewModel: viewModel)
        let numOfRows = viewModel.getNumberOfRows()
        let wrongNumOfRows = viewModel.getAddressListSize()
        XCTAssertNotEqual(numOfRows, wrongNumOfRows)
    }
    
    func testNumberOfRowsListEmpty() {
        let viewModel = self.viewModel(for: .addressListEmpty)
        fetchAddressList(viewModel: viewModel)
        let numOfRows = viewModel.getNumberOfRows()
        let expectedNumOfRows = 0
        XCTAssertEqual(numOfRows, expectedNumOfRows)
    }
    
    func testNumberOfRowsListEmptyFalse() {
        let viewModel = self.viewModel(for: .addressListEmpty)
        fetchAddressList(viewModel: viewModel)
        let numOfRows = viewModel.getNumberOfRows()
        let wrongNumOfRows = 1
        XCTAssertNotEqual(numOfRows, wrongNumOfRows)
    }
    
    // MARK: - Address deletion
    
    func testRemoveAddress() {
        let viewModel = self.viewModel(for: .addressListNotEmpty)
        let removedExpectation = XCTestExpectation(description: "AddressRemoved")
        fetchAddressList(viewModel: viewModel)
        
        let addressToRemove = viewModel.getAddress(at: 2)!
        let expectedFinalSize = viewModel.getAddressListSize() - 1
        viewModel.removeAddress(addressToRemove, { (_) in
            removedExpectation.fulfill()
        })
        
        wait(for: [removedExpectation], timeout: 5.0)
        let finalSize = viewModel.getAddressListSize()
        XCTAssertEqual(finalSize, expectedFinalSize)
    }
    
    func testRemoveAddressFalse() {
        let viewModel = self.viewModel(for: .addressListNotEmpty)
        let removedExpectation = XCTestExpectation(description: "AddressRemoved")
        fetchAddressList(viewModel: viewModel)
        
        let addressToRemove = ConsumerAddressItem(name: "Casa")
        let wrongFinalSize = viewModel.getAddressListSize() - 1
        viewModel.removeAddress(addressToRemove, { (_) in
            removedExpectation.fulfill()
        })
        
        wait(for: [removedExpectation], timeout: 5.0)
        let finalSize = viewModel.getAddressListSize()
        XCTAssertNotEqual(finalSize, wrongFinalSize)
    }
    
    // MARK: - Address List size
    
    func testAddresListSizeListNotEmpty() {
        let viewModel = self.viewModel(for: .addressListNotEmpty)
        fetchAddressList(viewModel: viewModel)
        XCTAssertEqual(viewModel.getAddressListSize(), 4)
    }
    
    func testAddresListSizeListNotEmptyFalse() {
        let viewModel = self.viewModel(for: .addressListNotEmpty)
        fetchAddressList(viewModel: viewModel)
        XCTAssertNotEqual(viewModel.getAddressListSize(), 0)
    }
    
    func testAddresListSizeListEmpty() {
        let viewModel = self.viewModel(for: .addressListEmpty)
        fetchAddressList(viewModel: viewModel)
        XCTAssertEqual(viewModel.getAddressListSize(), 0)
    }
    
    func testAddresListSizeListEmptyFalse() {
        let viewModel = self.viewModel(for: .addressListEmpty)
        fetchAddressList(viewModel: viewModel)
        XCTAssertNotEqual(viewModel.getAddressListSize(), 1)
    }
    
    func testAddresListSizeListNil() {
        let viewModel = self.viewModel(for: .addressListNil)
        XCTAssertEqual(viewModel.getAddressListSize(), 0)
    }
    
    func testAddresListSizeListNilFalse() {
        let viewModel = self.viewModel(for: .addressListNil)
        XCTAssertNotEqual(viewModel.getAddressListSize(), 1)
    }
    
    // MARK: - Address Selection
    
    func testAddressSelectionTrue() {
        let viewModel = self.viewModel(for: .undefined)
        let selectedAddress = addressExampleA!
        viewModel.selectedAddress = selectedAddress
        XCTAssert(viewModel.isSelected(address: selectedAddress))
    }
    
    func testAddressSelectionFalseDifferentAddresses() {
        let viewModel = self.viewModel(for: .undefined)
        let selectedAddress = addressExampleA!
        viewModel.selectedAddress = selectedAddress
        let otherAddress = addressExampleB!
        XCTAssertFalse(viewModel.isSelected(address: otherAddress))
    }
    
    func testAddressSelectionFalseNilAddress() {
        let viewModel = self.viewModel(for: .undefined)
        let selectedAddress = addressExampleA!
        viewModel.selectedAddress = selectedAddress
        let otherAddress: ConsumerAddressItem? = nil
        XCTAssertFalse(viewModel.isSelected(address: otherAddress))
    }
    
    func testAddressSelectionFalseNilSelection() {
        let viewModel = self.viewModel(for: .undefined)
        viewModel.selectedAddress = nil
        let otherAddress = addressExampleB!
        XCTAssertFalse(viewModel.isSelected(address: otherAddress))
    }
    
    // MARK: - Address update
    
    func testUpdateAddress() {
        let viewModel = self.viewModel(for: .addressListNotEmpty)
        fetchAddressList(viewModel: viewModel)
        let address = addressExampleA!
        viewModel.updateAddress(address: address, onIndex: 2)
        let returnedAddress = viewModel.getAddress(at: 2)
        XCTAssert(returnedAddress?.isLike(address) ?? false)
    }
    
    //MARK - TableHeader state
    
    func testTableHeaderStateGenericError() {
        let viewModel = self.viewModel(for: .genericError)
        fetchAddressList(viewModel: viewModel)
        XCTAssertEqual(viewModel.tableHeaderState, .connectionError)
    }
    
    func testTableHeaderStateConnectionError() {
        let viewModel = self.viewModel(for: .connectionError)
        fetchAddressList(viewModel: viewModel)
        XCTAssertEqual(viewModel.tableHeaderState, .connectionError)
    }
    
    func testTableHeaderStateGenericErrorListNotEmpty() {
        let viewModel = self.viewModel(for: .addressListNotEmpty)
        fetchAddressList(viewModel: viewModel)
        viewModel.computeTableHeaderState(error: MockError.genericError)
        XCTAssertEqual(viewModel.tableHeaderState, .tableItemError)
    }
    
    func testTableHeaderStateGenericErrorListEmpty() {
        let viewModel = self.viewModel(for: .addressListEmpty)
        fetchAddressList(viewModel: viewModel)
        viewModel.computeTableHeaderState(error: MockError.genericError)
        XCTAssertEqual(viewModel.tableHeaderState, .tableItemError)
    }
    
    func testTableHeaderStateConnectionErrorListNotEmpty() {
        let viewModel = self.viewModel(for: .addressListNotEmpty)
        fetchAddressList(viewModel: viewModel)
        viewModel.computeTableHeaderState(error: MockError.connectionError)
        XCTAssertEqual(viewModel.tableHeaderState, .connectionError)
    }
    
    func testTableHeaderStateConnectionErrorListEmpty() {
        let viewModel = self.viewModel(for: .addressListEmpty)
        fetchAddressList(viewModel: viewModel)
        viewModel.computeTableHeaderState(error: MockError.connectionError)
        XCTAssertEqual(viewModel.tableHeaderState, .connectionError)
    }
    
    func testTableHeaderStateListNotEmpty() {
        let viewModel = self.viewModel(for: .addressListNotEmpty)
        fetchAddressList(viewModel: viewModel)
        XCTAssertEqual(viewModel.tableHeaderState, .noHeader)
    }
    
    func testTableHeaderStateListEmpty() {
        let viewModel = self.viewModel(for: .addressListEmpty)
        fetchAddressList(viewModel: viewModel)
        XCTAssertEqual(viewModel.tableHeaderState, .emptyList)
    }
    
    func testTableHeaderStateDisplayContextHeaderListEmpty() {
        let viewModel = self.viewModel(for: .addressListEmpty)
        viewModel.displayContextHeader = true
        fetchAddressList(viewModel: viewModel)
        XCTAssertEqual(viewModel.tableHeaderState, .emptyList)
    }
    
    func testTableHeaderStateDisplayContextHeaderListNotEmpty() {
        let viewModel = self.viewModel(for: .addressListNotEmpty)
        viewModel.displayContextHeader = true
        fetchAddressList(viewModel: viewModel)
        XCTAssertEqual(viewModel.tableHeaderState, .contextHeader)
    }
}
