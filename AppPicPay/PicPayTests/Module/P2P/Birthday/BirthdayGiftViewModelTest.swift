import XCTest
@testable import PicPay
@testable import PCI

class MockBirthdayGiftService: BirthdayGiftServiceServicing {
    private var privacy: FeedItemVisibilityInt
    private var cvvCard: String?
    private var valueSubtotal: NSDecimalNumber = 0.0
    private var useBalance: Bool
    private var valueBalance: NSDecimalNumber
    private var valueCardTotal: NSDecimalNumber
    private var valueBalanceTotal: NSDecimalNumber
    private var valuePciCvvIsEnable: Bool
        
    var privacyConfig: FeedItemVisibilityInt {
        return privacy
    }
    
    var subtotal: NSDecimalNumber {
        return valueSubtotal
    }
    
    var defauldCardId: String {
        return "1"
    }
    
    var balance: NSDecimalNumber {
        return valueBalance
    }
    
    var usePicPayBalance: Bool {
        return useBalance
    }
    
    var cardTotal: NSDecimalNumber {
        return valueCardTotal
    }
    
    var balanceTotal: NSDecimalNumber {
        return valueBalanceTotal
    }
    
    var defauldCard: CardBank? {
        let cards =  try? MockCodable<[CardBank]>().loadCodableObject(resource: "CardsBankNoDebit")
        return cards?.first
    }
    
    func pciCvvIsEnable(cardBank: CardBank, cardValue: Double) -> Bool {
        return valuePciCvvIsEnable
    }
    
    init(
        privacy: FeedItemVisibilityInt = .private,
        useBalance: Bool = true,
        cvvCard: String? = nil,
        error3DS: Bool = false,
        valueBalance: NSDecimalNumber = .zero,
        valueCardTotal: NSDecimalNumber = .zero,
        valueBalanceTotal: NSDecimalNumber = .zero,
        valuePciCvvIsEnable: Bool = false
    ) {
        
        self.privacy = privacy
        self.useBalance = useBalance
        self.cvvCard = cvvCard
        self.valueBalance = valueBalance
        self.valueCardTotal = valueCardTotal
        self.valueBalanceTotal = valueBalanceTotal
        self.valuePciCvvIsEnable = valuePciCvvIsEnable
    }
    
    func saveCvvCard(id: String, value: String?) {
        self.cvvCard = value
    }
    
    func configurePayment(value: Double, installment: Int) {
        valueSubtotal = NSDecimalNumber(value: value)
    }
    
    func cvvCard(id: String) -> String? {
        return cvvCard
    }
    
    func getPrivacyConfig() -> FeedItemVisibilityInt {
        return privacy
    }
    
    func buttonTitle() -> String {
        return "Pagar Amiguinhos"
    }
    
    func textGift(type: Gift.GiftType) -> String {
        switch type {
        case .iceCream:
            return "Picole"
        
        case .beer:
            return "Agua"
            
        case .cake:
            return "Bolinho Ana Maria"
            
        }
    }
    
    func sendNotificationPicPayPro(payload: [String : Any]) {
    }
}

class MockBirthdayPaymentService: BirthdayPaymentServicing {
    private var isSuccess: Bool
    private var error3DS: Bool
    
    var cvvRequest: String? = nil
    
    init(isSuccess: Bool = true, error3DS: Bool = false) {
        self.isSuccess = isSuccess
        self.error3DS = error3DS
    }
    
    func makePayment(request: P2PRequest, cvv: String?, completion: @escaping(Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        cvvRequest = cvv
        guard isSuccess else {
            if error3DS {
                let dict = MockJSON().load(resource: "identifyShopper")
                let error = PicPayError(message: "3DSTransaction", code: String(AdyenConstant.code), dictionary: dict.dictionaryObject! as NSDictionary)
                completion(.failure(error))
            } else {
                let error = PicPayError(message: "API")
                completion(.failure(error))
            }
            
            return
        }
        let viewModel = ReceiptWidgetViewModel(type: .P2P)
        completion(.success(viewModel))
    }
}

class MockBirthdayGiftViewController: NSObject, ADYChallengeDelegate, BirthdayGiftViewModelOutputs {
    var calledDidNextStep = false
    var calledDidReceiveAnError = false
    var calledButtonTitle = false
    var calledAskDefaultPrivacy = false
    var calledUpdateProfile = false
    var calledUpdatePrivacy = false
    var calledUpdatePaymentMethod = false
    var calledSelectGift = false
    var calledAskPrivacy = false
    var calledAdyenWarning = false
    var calledChallengeDidFinish = false
    var calledChallengeDidFail = false
    
    var didNextStepAction: BirthdayGiftAction?
    
    var profileUsername: String?
    var profileImage: String?
    
    var privacyTitle: String?
    var privacyImage: UIImage?
    
    var buttonTitle: String?
    
    var error: PicPayError?
    
    var gifts: [Gift] = []
    
    func didNextStep(action: BirthdayGiftAction) {
        calledDidNextStep = true
        didNextStepAction = action
    }
    
    func didReceiveAnError(error: PicPayError) {
        calledDidReceiveAnError = true
        self.error = error
    }
    
    func buttonTitle(title: String) {
        calledButtonTitle = true
        buttonTitle = title
    }
    
    func updateProfile(username: String, image: String) {
        calledUpdateProfile = true
        profileUsername = username
        profileImage = image
    }
    
    func updatePrivacy(title: String, image: UIImage) {
        calledUpdatePrivacy = true
        privacyTitle = title
        privacyImage = image
    }
    
    func updatePaymentMethod(image: UIImage) {
        calledUpdatePaymentMethod = true
    }
    
    func selectGift(gifts: [Gift]) {
        calledSelectGift = true
        self.gifts = gifts
    }
    
    func askPrivacy(title: String, message: String, publicButton: String, privateButton: String, cancelAction: String) {
        calledAskPrivacy = true
    }
    
    func showAdyenWarning() {
        calledAdyenWarning = true
    }
    
    func challengeDidFinish(with result: ADYChallengeResult) {
        calledChallengeDidFinish = true
    }
    
    func challengeDidFailWithError(_ error: Error) {
        calledChallengeDidFail = true
    }
    
    func displayLimitExceeded(model: LimitExceededError) {
    }
}

class BirthdayGiftViewModelTest: XCTestCase {
    func cratePPContact() -> PPContact {
        let json = MockJSON().load(resource: "PPContact")
        return PPContact(profileDictionary: json.toDictionary()!)
    }
    
    func testViewDidLoadShouldCallUpdateProfile() {
        let adyen = MockAdyenService()
        let service = MockBirthdayGiftService()
        let payment = MockBirthdayPaymentService()
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        XCTAssertTrue(mockViewController.calledUpdateProfile)
        XCTAssertEqual(mockViewController.profileUsername!, "@gustavo.storck")
        XCTAssertEqual(mockViewController.profileImage!, "https://picpay-dev.s3.amazonaws.com/")
    }
    
    func testViewDidLoadShouldCallSelectGift() {
        let adyen = MockAdyenService()
        let service = MockBirthdayGiftService()
        let payment = MockBirthdayPaymentService()
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        XCTAssertTrue(mockViewController.calledSelectGift)
        XCTAssertEqual(mockViewController.gifts.count, 3)
        XCTAssertEqual(mockViewController.gifts[2].description, "Bolinho Ana Maria")
    }
    
    func testViewDidLoadShouldCallUpdatePrivacy() {
        let adyen = MockAdyenService()
        let service = MockBirthdayGiftService(privacy: .friends)
        let payment = MockBirthdayPaymentService()
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        XCTAssertTrue(mockViewController.calledUpdatePrivacy)

        XCTAssertEqual(mockViewController.privacyTitle, DefaultLocalizable.privacyPublic.text)
        XCTAssertEqual(mockViewController.privacyImage, #imageLiteral(resourceName: "icoPrivAmigos"))
    }
    
    func testViewDidLoadShouldCallButtonTitle() {
        let adyen = MockAdyenService()
        let service = MockBirthdayGiftService(privacy: .friends)
        let payment = MockBirthdayPaymentService()
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        XCTAssertTrue(mockViewController.calledButtonTitle)
        
        XCTAssertEqual(mockViewController.buttonTitle, "Pagar Amiguinhos")
    }
    
    func testViewDidAppearShouldCallUpdatePaymentMethod() {
        let adyen = MockAdyenService()
        let service = MockBirthdayGiftService()
        let payment = MockBirthdayPaymentService()
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.viewDidAppear()
        
        XCTAssertTrue(mockViewController.calledUpdatePaymentMethod)
    }
    
    func testGiftSelectedShouldCallUpdatePaymentMethod() {
        let adyen = MockAdyenService()
        let service = MockBirthdayGiftService()
        let payment = MockBirthdayPaymentService()
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.giftSelected(index: 1)
        
        XCTAssertTrue(mockViewController.calledUpdatePaymentMethod)
    }

    func testGiftSelectedShouldCallUpdatePaymentMethodReturnBalance() {
        let adyen = MockAdyenService()
        let service = MockBirthdayGiftService(useBalance: true, valueBalance: 10.0, valueCardTotal: .zero, valueBalanceTotal: .zero)
        let payment = MockBirthdayPaymentService()
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.giftSelected(index: 1)
        
        XCTAssertTrue(mockViewController.calledUpdatePaymentMethod)
    }
    
    func testGiftSelectedShouldCallUpdatePaymentMethodReturnCardAndBalance() {
        let adyen = MockAdyenService()
        let service = MockBirthdayGiftService(useBalance: true, valueBalance: 10.0, valueCardTotal: 10.0, valueBalanceTotal: 10.0)
        let payment = MockBirthdayPaymentService()
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.giftSelected(index: 1)
        
        XCTAssertTrue(mockViewController.calledUpdatePaymentMethod)
    }
    
    func testGiftSelectedShouldCallUpdatePaymentMethodReturnCard() {
        let adyen = MockAdyenService()
        let service = MockBirthdayGiftService(useBalance: true, valueBalance: .zero, valueCardTotal: 10.0, valueBalanceTotal: .zero)
        let payment = MockBirthdayPaymentService()
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.giftSelected(index: 1)
        
        XCTAssertTrue(mockViewController.calledUpdatePaymentMethod)
    }
    
    func testGiftSelectedShouldCallUpdatePaymentMethodReturnBalance2() {
        let adyen = MockAdyenService()
        let service = MockBirthdayGiftService(useBalance: true, valueBalance: 10.0, valueCardTotal: .zero, valueBalanceTotal: 10.0)
        let payment = MockBirthdayPaymentService()
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.giftSelected(index: 1)
        
        XCTAssertTrue(mockViewController.calledUpdatePaymentMethod)
    }
    
    func testCloseShouldCallDidNextStep() {
        let adyen = MockAdyenService()
        let service = MockBirthdayGiftService()
        let payment = MockBirthdayPaymentService(isSuccess: false)
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.close()
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch mockViewController.didNextStepAction! {
        case .close:
            XCTAssertTrue(true)
            
        default:
            XCTFail()
        }
    }
    
    func testDidTapPaymentMethodsShouldCallDidNextStep() {
        let adyen = MockAdyenService()
        let service = MockBirthdayGiftService()
        let payment = MockBirthdayPaymentService(isSuccess: false)
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.didTapPaymentMethods()
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch mockViewController.didNextStepAction! {
        case .changePaymentMethods:
            XCTAssertTrue(true)
            
        default:
            XCTFail()
        }
    }
    
    func testChangePaymentPrivacyShouldCallUpdatePrivacy() {
        let adyen = MockAdyenService()
        let service = MockBirthdayGiftService(privacy: .friends)
        let payment = MockBirthdayPaymentService()
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.changePaymentPrivacy(type: .friends)
        
        XCTAssertTrue(mockViewController.calledUpdatePrivacy)
        XCTAssertEqual(mockViewController.privacyTitle, DefaultLocalizable.privacyPublic.text)
        XCTAssertEqual(mockViewController.privacyImage, #imageLiteral(resourceName: "icoPrivAmigos"))
    }
    
    func testDidTapPrivacyShouldCallAskPrivacy() {
        let adyen = MockAdyenService()
        let service = MockBirthdayGiftService(privacy: .friends)
        let payment = MockBirthdayPaymentService()
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.didTapPrivacy()
        
        XCTAssertTrue(mockViewController.calledAskPrivacy)
    }
    
    func testChangePaymentPrivacyNotShouldCallAskDefault() {
        let adyen = MockAdyenService()
        let service = MockBirthdayGiftService(privacy: .friends)
        let payment = MockBirthdayPaymentService()
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.changePaymentPrivacy(type: .friends)
        
        XCTAssertFalse(mockViewController.calledAskDefaultPrivacy)
    }
    
    func testMakePaymentShouldCallDidNextStep() {
        let adyen = MockAdyenService()
        let service = MockBirthdayGiftService()
        let payment = MockBirthdayPaymentService(isSuccess: true)
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.makePayment(password: "1234", isBiometry: true)
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch mockViewController.didNextStepAction! {
        case .paymentSuccess(let value):
            XCTAssertNotNil(value)
            
        default:
            XCTFail()
        }
    }
    
    func testMakePaymentShouldCallDidReceiveAnError() {
        let adyen = MockAdyenService()
        let service = MockBirthdayGiftService()
        let payment = MockBirthdayPaymentService(isSuccess: false)
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.makePayment(password: "1234", isBiometry: true)
        
        XCTAssertTrue(mockViewController.calledDidReceiveAnError)
        XCTAssertNotNil(mockViewController.error)
    }
    
    func testMakePaymentShouldReturnNilCvvRequestWhenCardZero() {
        let adyen = MockAdyenService()
        let service = MockBirthdayGiftService(cvvCard: "123", valueCardTotal: .zero)
        let payment = MockBirthdayPaymentService(isSuccess: true)
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.makePayment(password: "1234", isBiometry: true)
        
        XCTAssertNil(payment.cvvRequest)
    }
    
    func testMakePaymentShouldReturnNilCvvRequestWhenCvvNil() {
        let adyen = MockAdyenService()
        let service = MockBirthdayGiftService(cvvCard: nil, valueCardTotal: .zero)
        let payment = MockBirthdayPaymentService(isSuccess: true)
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.makePayment(password: "1234", isBiometry: true)
        
        XCTAssertNil(payment.cvvRequest)
    }
    
    func testMakePaymentShouldReturnNotNilCvvRequest() {
        let adyen = MockAdyenService()
        let service = MockBirthdayGiftService(cvvCard: "1234", valueCardTotal: 10.0)
        let payment = MockBirthdayPaymentService(isSuccess: true)
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.makePayment(password: "1234", isBiometry: true)
        
        XCTAssertNotNil(payment.cvvRequest)
    }
    
    func testMakePaymentShouldCallShowAdyenWarning() {
        let adyen = MockAdyenService(firstAccess: true)
        let service = MockBirthdayGiftService()
        let payment = MockBirthdayPaymentService(isSuccess: false, error3DS: true)
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.makePayment(password: "1234", isBiometry: true)
        
        XCTAssertTrue(mockViewController.calledAdyenWarning)
    }
    
    func testMakePaymentNotShouldCallShowAdyenWarning() {
        let adyen = MockAdyenService(firstAccess: true)
        let service = MockBirthdayGiftService()
        let payment = MockBirthdayPaymentService(isSuccess: false, error3DS: false)
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.makePayment(password: "1234", isBiometry: true)
        
        XCTAssertFalse(mockViewController.calledAdyenWarning)
    }
    
    func testChallengeFinishSuccessShouldCallDidNextStep() {
        let adyen = MockAdyenService(completeSuccess: true)
        let service = MockBirthdayGiftService()
        let payment = MockBirthdayPaymentService()
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.challengeFinishSuccess()
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch mockViewController.didNextStepAction! {
        case .paymentSuccess(let value):
            XCTAssertNotNil(value)
            XCTAssertTrue(true)
            
        default:
            XCTFail()
        }
    }
    
    func testChallengeFinishSuccessShouldCallDidReceiveAnError() {
        let adyen = MockAdyenService(completeSuccess: false)
        let service = MockBirthdayGiftService()
        let payment = MockBirthdayPaymentService()
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.challengeFinishSuccess()
        
        XCTAssertTrue(mockViewController.calledDidReceiveAnError)
        XCTAssertNotNil(mockViewController.error)
    }
    
    func testChallengeFinishErrorNotShouldCallDidReceiveAnError() {
        let adyen = MockAdyenService(completeSuccess: false)
        let service = MockBirthdayGiftService()
        let payment = MockBirthdayPaymentService()
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        let error = NSError(domain: "3DSTransaction", code: Int(AdyenConstant.errorAdyChallengeCancelled), userInfo: nil)
        viewModel.inputs.challengeFinishError(error: error)
        
        XCTAssertFalse(mockViewController.calledDidReceiveAnError)
    }
    
    func testChallengeFinishErrorShouldCallDidReceiveAnError() {
        let adyen = MockAdyenService(completeSuccess: false)
        let service = MockBirthdayGiftService()
        let payment = MockBirthdayPaymentService()
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        let error = NSError(domain: "3DSTransaction", code: 10, userInfo: nil)
        viewModel.inputs.challengeFinishError(error: error)
        
        XCTAssertTrue(mockViewController.calledDidReceiveAnError)
        XCTAssertNotNil(mockViewController.error)
    }
    
    func testContinueOn3DSTransactionShouldCallDidReceiveAnError() {
        let adyen = MockAdyenService(transactionSuccess: false)
        let service = MockBirthdayGiftService()
        let payment = MockBirthdayPaymentService()
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.continueOn3DSTransaction()
        
        XCTAssertTrue(mockViewController.calledDidReceiveAnError)
        XCTAssertNotNil(mockViewController.error)
    }
    
    func testContinueOn3DSTransactionShouldCallDidNextStep() {
        let adyen = MockAdyenService(transactionSuccess: true)
        let service = MockBirthdayGiftService()
        let payment = MockBirthdayPaymentService()
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.continueOn3DSTransaction()
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch mockViewController.didNextStepAction! {
        case .paymentSuccess(let value):
            XCTAssertNotNil(value)
            XCTAssertTrue(true)
            
        default:
            XCTFail()
        }
    }
    
    func testMakePaymentShouldCallOpenCvv() {
        let adyen = MockAdyenService(transactionSuccess: false)
        let service = MockBirthdayGiftService(valueCardTotal: 10.0, valuePciCvvIsEnable: true)
        let payment = MockBirthdayPaymentService()
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.makePayment(password: "1234", isBiometry: true)
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch mockViewController.didNextStepAction! {
        case .openCvv:
            XCTAssertTrue(true)
            
        default:
            XCTFail()
        }
    }
    
    func testMakePaymentNotShouldCallOpenCvvWhenPciFalse() {
        let adyen = MockAdyenService(transactionSuccess: false)
        let service = MockBirthdayGiftService(valueCardTotal: 10.0, valuePciCvvIsEnable: false)
        let payment = MockBirthdayPaymentService()
        let model = cratePPContact()
        let viewModel = BirthdayGiftViewModel(service: service, paymentService: payment, model: model, adyen: adyen)
        let mockViewController = MockBirthdayGiftViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.makePayment(password: "1234", isBiometry: true)
        
        switch mockViewController.didNextStepAction! {
        case .openCvv:
            XCTFail()
            
        default:
            XCTAssertTrue(true)
        }
    }
}
