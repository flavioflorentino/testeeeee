import XCTest
@testable import PicPay

class MockBirthdayGiftLoad: BirthdayGiftLoadServicing {
    private let isSuccess: Bool
    private let newBirthday: Bool
    
    var newBirthdayPaymentEnable: Bool {
        return newBirthday
    }
    
    init(isSuccess: Bool = true, newBirthday: Bool = false) {
        self.isSuccess = isSuccess
        self.newBirthday = newBirthday
    }
    
    func getConsumer(id: String, completion: @escaping(Result<PPContact, PicPayError>) -> Void) {
        guard isSuccess else {
            let error = PicPayError(message: "Error")
            completion(.failure(error))
            return
        }
        
        let json = MockJSON().load(resource: "PPContact")
        let model = PPContact(profileDictionary: json.toDictionary()!)
        completion(.success(model))
    }
}

class MockBirthdayGiftLoadViewController: BirthdayGiftLoadViewModelOutputs {
    var calledDidNextStep = false
    var didNextStepAction: BirthdayGiftLoadAction?
    var calledDidReceivedAnError = false
    var error: PicPayError?
    
    func didNextStep(action: BirthdayGiftLoadAction) {
        calledDidNextStep = true
        didNextStepAction = action
    }
    
    func didReceiveAnError(error: PicPayErrorDisplayable) {
        calledDidReceivedAnError = true
        self.error = error as? PicPayError
    }
}

class BirthdayGiftLoadViewModelTest: XCTestCase {
    func testViewDidLoad_ShouldCall_DidNextStepLegacy() throws {
        let service = MockBirthdayGiftLoad(isSuccess: true, newBirthday: false)
        let viewModel = BirthdayGiftLoadViewModel(service: service, userId: "1")
        let mockViewController = MockBirthdayGiftLoadViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        XCTAssert(mockViewController.calledDidNextStep)
        switch try mockViewController.didNextStepAction.safe() {
        case .openLegacyBirthday(let value):
            XCTAssertNotNil(value)
            
        default:
            XCTFail("Test ViewDidLoad No Call didNextStep -> openLegacyBirthday")
        }
    }
    
    func testViewDidLoad_ShouldCall_DidNextStepNew() throws {
        let service = MockBirthdayGiftLoad(isSuccess: true, newBirthday: true)
        let viewModel = BirthdayGiftLoadViewModel(service: service, userId: "1")
        let mockViewController = MockBirthdayGiftLoadViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        XCTAssert(mockViewController.calledDidNextStep)
        switch try XCTUnwrap(mockViewController.didNextStepAction) {
        case .openBirthday(let value):
            XCTAssertNotNil(value)
            
        default:
            XCTFail()
        }
    }
    
    func testViewDidLoadShouldCallDidReceiveAnError(){
        let service = MockBirthdayGiftLoad(isSuccess: false)
        let viewModel = BirthdayGiftLoadViewModel(service: service, userId: "1")
        let mockViewController = MockBirthdayGiftLoadViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        XCTAssert(mockViewController.calledDidReceivedAnError)
        XCTAssertNotNil(mockViewController.error)
    }
    
    func testCloseShouldCallDidNextStep(){
        let service = MockBirthdayGiftLoad()
        let viewModel = BirthdayGiftLoadViewModel(service: service, userId: "1")
        let mockViewController = MockBirthdayGiftLoadViewController()
        
        viewModel.outputs = mockViewController
        viewModel.inputs.viewDidLoad()
        
        viewModel.inputs.close()
        
        XCTAssertTrue(mockViewController.calledDidNextStep)
        switch mockViewController.didNextStepAction! {
        case .close:
            XCTAssertTrue(true)
            
        default:
            XCTFail()
        }
    }
}
