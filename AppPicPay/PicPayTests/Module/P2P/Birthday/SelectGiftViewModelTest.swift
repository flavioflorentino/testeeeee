import XCTest
import CoreLegacy
@testable import PicPay

class MockSelectGiftView: SelectGiftViewModelOutputs {
    var calledAddRow = false
    var calleGiftChecked = false
    var calledGiftSelected = false
    
    var countAddRow = 0
    var rowImage: String?
    var rowValue: String?
    var rowIndex: Int?
    
    var giftCheckedOldIndex: Int?
    var giftCheckedNewIndex: Int?
    var giftCheckedNewIsCheck: Bool?
    var giftCheckedOldIsCheck: Bool?
    
    var updateDescription: String?
    
    var giftSelected: Int?
    
    func addRow(image: String, value: String, index: Int) {
        calledAddRow = true
        
        if countAddRow == 0 {
            rowImage = image
            rowValue = value
            rowIndex = index
        }
        
        countAddRow += 1
    }
    
    func giftChecked(index: Int, isCheck: Bool) {
        calleGiftChecked = true
        giftCheckedOldIndex = giftCheckedNewIndex
        giftCheckedNewIndex = index
        giftCheckedOldIsCheck = giftCheckedNewIsCheck
        giftCheckedNewIsCheck = isCheck
    }
    
    func giftSelected(index: Int, description: String) {
        calledGiftSelected = true
        giftSelected = index
        updateDescription = description
    }
}

class SelectGiftViewModelTest: XCTestCase {
    func createModel() -> [Gift] {
        let row1 = Gift(type: .iceCream, value: 2.00, description: "Row1")
        let row2 = Gift(type: .beer, value: 5.00, description: "Row2")
        let row3 = Gift(type: .cake, value: 15.00, description: "Row3")
        
        return [row1, row2, row3]
    }
    
    func testViewConfigureShouldCallAddRowThreeTimes() {
        let model = createModel()
        let viewModel = SelectGiftViewModel()
        let mockView = MockSelectGiftView()
        
        viewModel.outputs = mockView
        viewModel.inputs.viewConfigure(model: model)
        
        XCTAssertTrue(mockView.calledAddRow)
        XCTAssertEqual(mockView.countAddRow, 3)
    }
    
    func testViewConfigureReturnRowValues() {
        let model = createModel()
        let viewModel = SelectGiftViewModel()
        let mockView = MockSelectGiftView()
        
        viewModel.outputs = mockView
        viewModel.inputs.viewConfigure(model: model)
        
        XCTAssertEqual(mockView.rowImage, "🍦")
        XCTAssertEqual(mockView.rowValue, CurrencyFormatter.brazillianRealString(from: NSNumber(value: 2.00)))
        XCTAssertEqual(mockView.rowIndex, 0)
    }
    
    func testViewConfigureCallGiftSelected() {
        let model = createModel()
        let viewModel = SelectGiftViewModel()
        let mockView = MockSelectGiftView()
        
        viewModel.outputs = mockView
        viewModel.inputs.viewConfigure(model: model)
        
        XCTAssertTrue(mockView.calledGiftSelected)
        XCTAssertEqual(mockView.giftSelected, 0)
        XCTAssertEqual(mockView.updateDescription, "Row1")

    }
    
    func testViewConfigureCallGiftChecked() {
        let model = createModel()
        let viewModel = SelectGiftViewModel()
        let mockView = MockSelectGiftView()
        
        viewModel.outputs = mockView
        viewModel.inputs.viewConfigure(model: model)
        
        XCTAssertTrue(mockView.calleGiftChecked)
        XCTAssertEqual(mockView.giftCheckedNewIndex, 0)
        XCTAssertTrue(mockView.giftCheckedNewIsCheck!)
    }
    
    func testDidTapGiftCallGiftSelected() {
        let model = createModel()
        let viewModel = SelectGiftViewModel()
        let mockView = MockSelectGiftView()
        
        viewModel.outputs = mockView
        viewModel.inputs.viewConfigure(model: model)
        
        viewModel.inputs.didTapGift(index: 1)
        
        XCTAssertTrue(mockView.calledGiftSelected)
        XCTAssertEqual(mockView.giftSelected, 1)
        XCTAssertEqual(mockView.updateDescription, "Row2")
        
    }
    
    func testDidTapGiftCheckedSelected() {
        let model = createModel()
        let viewModel = SelectGiftViewModel()
        let mockView = MockSelectGiftView()

        viewModel.outputs = mockView
        viewModel.inputs.viewConfigure(model: model)

        viewModel.inputs.didTapGift(index: 1)

        XCTAssertTrue(mockView.calleGiftChecked)
        XCTAssertEqual(mockView.giftCheckedNewIndex, 1)
        XCTAssertEqual(mockView.giftCheckedOldIndex, 0)
        XCTAssertFalse(mockView.giftCheckedOldIsCheck!)
        XCTAssertTrue(mockView.giftCheckedNewIsCheck!)
    }
}
