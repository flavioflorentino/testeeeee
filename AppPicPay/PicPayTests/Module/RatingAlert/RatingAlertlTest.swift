import UI
import XCTest
@testable import PicPay

class FakeRatingAlertViewController: LegacyViewController<RatingAlertViewModelType, RatingAlertCoordinating, UIView>, RatingAlertViewModelOutputs {
    var didDismiss = false
    var didDismissWithCompletion = false
    var isSendButtonEnabled = false
    var didShowThankYouPopup = false
    var didOpenStoreReview = false

    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        super.dismiss(animated: false)
        didDismiss = true
        didDismissWithCompletion = completion != nil
    }

    func enableSendButton() {
        isSendButtonEnabled = true
    }

    func disableSendButton() {
        isSendButtonEnabled = false
    }

    func showThankYouPopup(storeReviewURL: URL?) {
        didShowThankYouPopup = true
        didOpenStoreReview = storeReviewURL != nil
    }
}


class RatingAlertViewModelTest: XCTestCase {
    var viewController: FakeRatingAlertViewController!
    var viewModel: RatingAlertViewModelType!
    
    override func setUp() {
        super.setUp()
        var coordinator: RatingAlertCoordinating = RatingAlertCoordinator()
        viewModel = RatingAlertViewModel()
        viewController = FakeRatingAlertViewController(viewModel: viewModel, coordinator: coordinator)
        coordinator.viewController = viewController
        viewModel.outputs = viewController
    }
    
    func testSendButtonEnabled() {
        viewModel.inputs.updateRateNumber(1)
        XCTAssert(viewController.isSendButtonEnabled)
    }
    
    func testSendButtonDisabled() {
        viewModel.inputs.updateRateNumber(0)
        XCTAssertFalse(viewController.isSendButtonEnabled)
    }
    
    func testShowThankYouPopupAndOpenStore() {
        viewModel.inputs.updateRateNumber(5)
        viewModel.inputs.createAndShowThankYouPopup()
        XCTAssert(viewController.didShowThankYouPopup)
        XCTAssert(viewController.didOpenStoreReview)
    }
    
    func testShowThankYouPopupNotOpeningStore() {
        viewModel.inputs.updateRateNumber(4)
        viewModel.inputs.createAndShowThankYouPopup()
        XCTAssert(viewController.didShowThankYouPopup)
        XCTAssertFalse(viewController.didOpenStoreReview)
    }
}

class RatingAlertCoordinatorTest: XCTestCase {
    var viewController: FakeRatingAlertViewController!
    var coordinator: RatingAlertCoordinating!
    
    override func setUp() {
        super.setUp()
        coordinator = RatingAlertCoordinator()
        let viewModel: RatingAlertViewModelType = RatingAlertViewModel()
        viewController = FakeRatingAlertViewController(viewModel: viewModel, coordinator: coordinator)
        coordinator.viewController = viewController
        viewModel.outputs = viewController
    }
    
    func testCloseAction() {
        coordinator.perform(action: .close)
        XCTAssert(viewController.didDismiss)
        XCTAssertFalse(viewController.didDismissWithCompletion)
    }
    
    func testCloseAndShowThankYouPopupAction() {
        coordinator.perform(action: .showThankYouPopup(nil))
        XCTAssert(viewController.didDismiss)
        XCTAssert(viewController.didDismissWithCompletion)
    }
}
