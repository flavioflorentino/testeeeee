import Core
import CoreLegacy
import XCTest
import OHHTTPStubs
@testable import PicPay

class RegistrationViewModelTest: XCTestCase {
    var viewModel: RegistrationViewModel!
    
    override func setUp() {
        super.setUp()
        viewModel = RegistrationViewModel()
        ConsumerManager.shared.consumer?.balance = 0.00
        AppParameters.global().setVerifiedNumber("(27) 99999-9999")
        AppParameters.global().setHasAtLeastOneFeedOnce(true)
        AppParameters.global().setOnboardSocial(false)
        KVStore().setBool(false, with: .isRegisterUsernameStep)
    }
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testFinishEmailStepSuccess() {
        let expectation = XCTestExpectation(description: "response")
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(at: "isValidEmailSuccess.json")
        }
        
        viewModel.finishEmailStep(onSuccess: {
            expectation.fulfill()
        }) { error in
            XCTFail()
        }
        wait(for: [expectation], timeout: 5.0)
    }

    func finishEmailStepWithInvalidEmail() {
        let expectation = XCTestExpectation(description: "response")
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(at: "isValidEmailFailure.json")
        }
        
        viewModel.finishEmailStep(onSuccess: {
            XCTFail()
        }) { error in
            XCTAssertEqual(error.localizedDescription, "O email informado é inválido")
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testFinishEmailStepFailure() {
        XCTAssertEqual(self.viewModel.invalidEmailCounter, 0)
        finishEmailStepWithInvalidEmail()
        XCTAssertEqual(self.viewModel.invalidEmailCounter, 1)
        finishEmailStepWithInvalidEmail()
        XCTAssertEqual(self.viewModel.invalidEmailCounter, 2)
    }
    
    func finishRegistrationWithInvalidCpf() {
        let expectation = XCTestExpectation(description: "response")
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(at: "addConsumerFailure.json")
        }
        
        viewModel.finishRegistration(onSuccess: {
            XCTFail()
        }) { error in
            XCTAssertEqual(error.localizedDescription, "CPF inválido")
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testFinishPicPayUsernameStepWithResponseValuesNull() {
        let expectation = XCTestExpectation(description: "response")
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(at: "createConsumerUsernameFailure.json")
        }
        
        viewModel.finishUsernameStep(1, usedSuggestion: false){ error in
            XCTAssertEqual(error?.localizedDescription, "Ops, tivemos problemas ao enviar suas informações, por favor, tente novamente.")
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1.0)
    }
    
    func testFinishRegistrationSuccess() {
        let expectation = XCTestExpectation(description: "response")
        
        stub(condition: pathEndsWith("api/getAllConsumerData.json")) { request in
            return StubResponse.success(at: "getAllConsumerData.json")
        }

        stub(condition: pathEndsWith("api/addConsumer.json")) { request in
            return StubResponse.success(at: "addConsumerSuccess.json")
        }
        
        viewModel.finishRegistration(onSuccess: {
            XCTAssertEqual(User.token, "e025952956bf4cf9834a8f43a6dbb952")
            XCTAssertEqual(AppParameters.global().verifiedNumber(), "")
            XCTAssertFalse(AppParameters.global().hasAtLeastOneFeedOnce())
            XCTAssertTrue(AppParameters.global().getOnboardSocial())
            XCTAssertTrue(KVStore().boolFor(.isRegisterUsernameStep))
            expectation.fulfill()
        }) { error in
            XCTFail()
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testFinishRegistrationFailure() {
        XCTAssertEqual(viewModel.invalidCpfCounter, 0)
        finishRegistrationWithInvalidCpf()
        XCTAssertEqual(viewModel.invalidCpfCounter, 1)
        finishRegistrationWithInvalidCpf()
        XCTAssertEqual(viewModel.invalidCpfCounter, 2)
    }
}
