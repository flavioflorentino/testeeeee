import XCTest
@testable import PicPay

class InstallmentsSelectorWorkerMock: InstallmentsSelectorWorkerProtocol {
    func loadBusinnesAccount(with payeedId: String, _ completion: @escaping ([Any]?, Error?) -> Void) {
        let mock = MockJSON().load(resource: "getInstallmentsBusinessAccount")
        let installments = mock["data"]["installments"].arrayObject
        DispatchQueue.main.async {
            completion(installments, nil)
        }
    }
    
    func loadSellerAccount(with sellertId: String, _ completion: @escaping ([Any]?, Error?) -> Void) {
        let mock = MockJSON().load(resource: "getInstallmentsSeller")
        let installments = mock["data"]["installments"].arrayObject
        DispatchQueue.main.async {
            completion(installments, nil)
        }
    }
}

class InstallmentsSelectorViewModelTest: XCTestCase {
    var paymentManager: PPPaymentManager?
    var worker = InstallmentsSelectorWorkerMock()
    
    override func setUp() {
        super.setUp()
        let paymentManager = PPPaymentManager()!
        let consumer = MBConsumer()
        consumer.balance = NSDecimalNumber(floatLiteral: 500)
        ConsumerManager.shared.consumer = consumer
        ConsumerManager.setUseBalance(false)
        paymentManager.subtotal = 3000.00
        self.paymentManager = paymentManager
    }
    
    override func tearDown() {
        super.tearDown()
        ConsumerManager.setUseBalance(true)
        ConsumerManager.shared.consumer = nil
        paymentManager = nil
    }
    
    func testNumberOfSections() {
        let viewModel = InstallmentsSelectorViewModel(paymentManager: paymentManager!,
                                                      payeeId: 123,
                                                      sellerId: nil,
                                                      origin: nil,
                                                      titleHeader: nil,
                                                      descriptionHeader: nil,
                                                      worker: worker)
        let expectation = self.expectation(description: "request")
        viewModel.loadData(onSuccess: {
            let sections = viewModel.numberOfSections()
            let expected = 1
            XCTAssertEqual(expected, sections)
            
            expectation.fulfill()
        }, onError: { _ in })
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testNumberOfRows() {
        let viewModel = InstallmentsSelectorViewModel(paymentManager: paymentManager!,
                                                      payeeId: 123,
                                                      sellerId: nil,
                                                      origin: nil,
                                                      titleHeader: nil,
                                                      descriptionHeader: nil,
                                                      worker: worker)
        let expectation = self.expectation(description: "request")
        viewModel.loadData(onSuccess: {
            let rows = viewModel.numberOfRows(in: 1)
            let expected = 12
            XCTAssertEqual(expected, rows)
            
            expectation.fulfill()
        }, onError: { _ in })
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testTitleAndDescriptionOfHeaderPayeedId() {
        let viewModel = InstallmentsSelectorViewModel(paymentManager: paymentManager!,
                                                      payeeId: 123,
                                                      sellerId: nil,
                                                      origin: nil,
                                                      titleHeader: nil,
                                                      descriptionHeader: nil,
                                                      worker: worker)
        let expectation = self.expectation(description: "request")
        viewModel.loadData(onSuccess: {
            let section = 1
            let title = viewModel.headerViewTitle(for: section)
            let description = viewModel.headerViewDescription(for: section)
            let heightHeader = viewModel.headerHeight(for: section)
            
            XCTAssertNil(title)
            XCTAssertNil(description)
            XCTAssertEqual(heightHeader, CGFloat.leastNonzeroMagnitude)
            expectation.fulfill()
        }, onError: { _ in })
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testTitleAndDescriptionOfHeaderPayeedIdOrigemP2P() {
        let viewModel = InstallmentsSelectorViewModel(paymentManager: paymentManager!,
                                                      payeeId: 123,
                                                      sellerId: nil,
                                                      origin: "p2p",
                                                      titleHeader: nil,
                                                      descriptionHeader: nil,
                                                      worker: worker)
        let expectation = self.expectation(description: "request")
        viewModel.loadData(onSuccess: {
            let section = 1
            let title = viewModel.headerViewTitle(for: section)
            let description = viewModel.headerViewDescription(for: section)
            let heightHeader = viewModel.headerHeight(for: section)
            let titleExpected = "Você paga parcelado e o destinatário recebe à vista."
            
            XCTAssertEqual(title, titleExpected)
            XCTAssertNil(description)
            XCTAssertEqual(heightHeader, UITableView.automaticDimension)
            
            expectation.fulfill()
        }, onError: { _ in })
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testTitleAndDescriptionOfHeaderPayeedIdOrigemP2PUseBalanceTrue() {
        ConsumerManager.setUseBalance(true)
        let viewModel = InstallmentsSelectorViewModel(paymentManager: paymentManager!,
                                                      payeeId: 123,
                                                      sellerId: nil,
                                                      origin: "p2p",
                                                      titleHeader: nil,
                                                      descriptionHeader: nil,
                                                      worker: worker)
        let expectation = self.expectation(description: "request")
        viewModel.loadData(onSuccess: {
            let section = 1
            let title = viewModel.headerViewTitle(for: section)
            let description = viewModel.headerViewDescription(for: section)
            let heightHeader = viewModel.headerHeight(for: section)
            
            let titleExpected = "Você paga parcelado e o destinatário recebe à vista."
            let descriptionExpected = "Este pagamento é composto por saldo na carteira e cartão de crédito. Apenas o valor pago com o cartão de crédito será parcelado."
            
            XCTAssertEqual(title, titleExpected)
            XCTAssertEqual(description, descriptionExpected)
            XCTAssertEqual(heightHeader, UITableView.automaticDimension)
            
            expectation.fulfill()
        }, onError: { _ in })
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testTitleAndDescriptionOfHeaderPayeedIdOrigemP2PNotBalance() {
        paymentManager!.forceCreditCard = true
        let viewModel = InstallmentsSelectorViewModel(paymentManager: paymentManager!,
                                                      payeeId: 123,
                                                      sellerId: nil,
                                                      origin: "p2p",
                                                      titleHeader: nil,
                                                      descriptionHeader: nil,
                                                      worker: worker)
        let expectation = self.expectation(description: "request")
        viewModel.loadData(onSuccess: {
            let section = 1
            let title = viewModel.headerViewTitle(for: section)
            let description = viewModel.headerViewDescription(for: section)
            let heightHeader = viewModel.headerHeight(for: section)
            
            let titleExpected = "Você paga parcelado e o destinatário recebe à vista."
            
            XCTAssertEqual(title, titleExpected)
            XCTAssertNil(description)
            XCTAssertEqual(heightHeader, UITableView.automaticDimension)
            
            expectation.fulfill()
        }, onError: { _ in })
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testTitleAndDescriptionOfHeaderPayeedIdOrigemP2PBalanceZero() {
        ConsumerManager.shared.consumer?.balance = NSDecimalNumber.zero
        let viewModel = InstallmentsSelectorViewModel(paymentManager: paymentManager!,
                                                      payeeId: 123,
                                                      sellerId: nil,
                                                      origin: "p2p",
                                                      titleHeader: nil,
                                                      descriptionHeader: nil,
                                                      worker: worker)
        let expectation = self.expectation(description: "request")
        viewModel.loadData(onSuccess: {
            let section = 1
            let title = viewModel.headerViewTitle(for: section)
            let description = viewModel.headerViewDescription(for: section)
            let heightHeader = viewModel.headerHeight(for: section)
            
            let titleExpected = "Você paga parcelado e o destinatário recebe à vista."
            
            XCTAssertEqual(title, titleExpected)
            XCTAssertNil(description)
            XCTAssertEqual(heightHeader, UITableView.automaticDimension)
            
            expectation.fulfill()
        }, onError: { _ in })
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testTitleAndDescriptionOfHeaderPayeedIdOrigemP2PNoUseBalance() {
        ConsumerManager.setUseBalance(false)
        let viewModel = InstallmentsSelectorViewModel(paymentManager: paymentManager!,
                                                      payeeId: 123,
                                                      sellerId: nil,
                                                      origin: "p2p",
                                                      titleHeader: nil,
                                                      descriptionHeader: nil,
                                                      worker: worker)
        let expectation = self.expectation(description: "request")
        viewModel.loadData(onSuccess: {
            let section = 1
            let title = viewModel.headerViewTitle(for: section)
            let description = viewModel.headerViewDescription(for: section)
            let heightHeader = viewModel.headerHeight(for: section)
            
            let titleExpected = "Você paga parcelado e o destinatário recebe à vista."
            
            XCTAssertEqual(title, titleExpected)
            XCTAssertNil(description)
            XCTAssertEqual(heightHeader, UITableView.automaticDimension)
            
            expectation.fulfill()
        }, onError: { _ in })
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testTitleAndDescriptionOfHeaderSellerId() {
        let viewModel = InstallmentsSelectorViewModel(paymentManager: paymentManager!,
                                                      payeeId: nil,
                                                      sellerId: "2322",
                                                      origin: nil,
                                                      titleHeader: nil,
                                                      descriptionHeader: nil,
                                                      worker: worker)
        let expectation = self.expectation(description: "request")
        viewModel.loadData(onSuccess: {
            let section = 1
            let title = viewModel.headerViewTitle(for: section)
            let description = viewModel.headerViewDescription(for: section)
            let heightHeader = viewModel.headerHeight(for: section)
            
            XCTAssertNil(title)
            XCTAssertNil(description)
            XCTAssertEqual(heightHeader, CGFloat.leastNonzeroMagnitude)
            
            expectation.fulfill()
        }, onError: { _ in })
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testTitleAndDescriptionOfHeaderPayeedIdOrigemP2PWithSetedTitleAndDescription() {
        let viewModel = InstallmentsSelectorViewModel(paymentManager: paymentManager!,
                                                      payeeId: 123,
                                                      sellerId: nil,
                                                      origin: "p2p",
                                                      titleHeader: "Você paga parcelado e o destinatário recebe à vista.",
                                                      descriptionHeader: "Este pagamento é composto por saldo na carteira e cartão de crédito. Apenas o valor pago com o cartão de crédito será parcelado.",
                                                      worker: worker)
        let expectation = self.expectation(description: "request")
        viewModel.loadData(onSuccess: {
            let section = 1
            let title = viewModel.headerViewTitle(for: section)
            let description = viewModel.headerViewDescription(for: section)
            let heightHeader = viewModel.headerHeight(for: section)
            
            let titleExpected = "Você paga parcelado e o destinatário recebe à vista."
            let descriptionExpected = "Este pagamento é composto por saldo na carteira e cartão de crédito. Apenas o valor pago com o cartão de crédito será parcelado."
            
            XCTAssertEqual(title, titleExpected)
            XCTAssertEqual(description, descriptionExpected)
            XCTAssertEqual(heightHeader, UITableView.automaticDimension)
            
            expectation.fulfill()
        }, onError: { _ in })
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testTextCell() {
        let viewModel = InstallmentsSelectorViewModel(paymentManager: paymentManager!,
                                                      payeeId: nil,
                                                      sellerId: "12312312",
                                                      origin: nil,
                                                      titleHeader: nil,
                                                      descriptionHeader: nil,
                                                      worker: worker)
        let expectation = self.expectation(description: "request")
        viewModel.loadData(onSuccess: {
            let expected = "1x de R$ 3.000,00 sem acréscimo"
            let text = viewModel.textForCell(in: IndexPath(row: 0, section: 1))
            XCTAssertEqual(expected, text)
            
            let expected2 = "5x de R$ 664,26 "
            let text2 = viewModel.textForCell(in: IndexPath(row: 4, section: 1))
            XCTAssertEqual(expected2, text2)
            
            XCTAssertNil(viewModel.textForCell(in: IndexPath(row: 90, section: 1)))
            
            expectation.fulfill()
        }, onError: { _ in })
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testDidSelectedRow() {
        let viewModel = InstallmentsSelectorViewModel(paymentManager: paymentManager!,
                                                      payeeId: 123,
                                                      sellerId: nil,
                                                      origin: "p2p",
                                                      titleHeader: nil,
                                                      descriptionHeader: nil,
                                                      worker: worker)
        let expectation = self.expectation(description: "request")
        viewModel.loadData(onSuccess: {
            viewModel.didSelectedRow(at: IndexPath(row: 0, section: 1))
            XCTAssertEqual(self.paymentManager!.selectedQuotaQuantity, 1)
            
            viewModel.didSelectedRow(at: IndexPath(row: 1, section: 1))
            XCTAssertEqual(self.paymentManager!.selectedQuotaQuantity, 2)
            
            let isChecked = viewModel.cellIsChecked(at: IndexPath(row: 1, section: 1))
            XCTAssertTrue(isChecked)
            
            let isNotChecked = viewModel.cellIsChecked(at: IndexPath(row: 3, section: 1))
            XCTAssertFalse(isNotChecked)
            
            expectation.fulfill()
        }, onError: { _ in })
        waitForExpectations(timeout: 5.0, handler: nil)
    }
}

extension InstallmentsSelectorViewModelTest: InstallmentsSelectorViewModelDelegate {
    func didSelectedInstallment(_ paymentManager: PPPaymentManager) {
        self.paymentManager = paymentManager
    }
}
