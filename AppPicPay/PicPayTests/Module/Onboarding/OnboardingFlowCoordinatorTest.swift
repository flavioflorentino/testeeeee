import Core
import FeatureFlag
import XCTest

@testable import PicPay

final class OnboardingFlowCoordinatorTest: XCTestCase {
    private var featureManagerMock = FeatureManagerMock()
    private lazy var mockDependencies = DependencyContainerMock(featureManagerMock)
    private var mockNavController = NavigationControllerMock(rootViewController: UIViewController())
    private lazy var sut = OnboardingFlowCoordinator(navigationController: mockNavController, dependencies: mockDependencies)
    
    func setupFlowCoordinatorWithSteps(_ steps: [OnboardingFlowCoordinator.OnboardingStep]) {
        sut = OnboardingFlowCoordinator(navigationController: mockNavController, steps: steps, dependencies: mockDependencies)
    }

    func skipToNextAndCheckSocialStep() {
        sut.didFinishOnboardingSocial()
        XCTAssert(mockNavController.pushedViewController is FindFriendsViewController)
    }
    
    func skipToNextAndCheckCardStep() {
        sut.didFinishOnboardingSocial()
        XCTAssert(mockNavController.pushedViewController is AddCardOnboardingViewController)
    }
    
    func skipToNextAndCheckFirstActionStep() {
        sut.didFinishOnboardingSocial()
        XCTAssert(mockNavController.pushedViewController is FirstActionOnboardingViewController)
    }
    

    func testPresentSocialStep() {
        setupFlowCoordinatorWithSteps([.social])
        skipToNextAndCheckSocialStep()
    }
    
    func testPresentCardStep() {
        setupFlowCoordinatorWithSteps([.card])
        skipToNextAndCheckCardStep()
    }
    
    func testPresentFirstActionStep() {
        setupFlowCoordinatorWithSteps([.firstAction])
        skipToNextAndCheckFirstActionStep()
    }
    
    func testPresentAllSteps() {
        setupFlowCoordinatorWithSteps([.social, .card, .firstAction])
        skipToNextAndCheckSocialStep()
        skipToNextAndCheckCardStep()
        skipToNextAndCheckFirstActionStep()
    }
    
    func testPresentCardAndFirstActionSteps() {
        setupFlowCoordinatorWithSteps([.card, .firstAction])
        skipToNextAndCheckCardStep()
        skipToNextAndCheckFirstActionStep()
    }

    func testPresentSocialAndFirstActionSteps() {
        setupFlowCoordinatorWithSteps([.social, .firstAction])
        skipToNextAndCheckSocialStep()
        skipToNextAndCheckFirstActionStep()
    }
}
