@testable import PicPay
import XCTest

class FakeOnboardingAddCardCoordinator: OnboardingAddCardDelegate {
    var didFinishOnboardingAddCardExecuted = false
    
    func didFinishOnboardingAddCard() {
        didFinishOnboardingAddCardExecuted = true
    }
}


class AddCardOnboardingCoordinatorTest: XCTestCase {
    var coordinator: AddCardOnboardingCoordinating!
    var flowCoordinator: FakeOnboardingAddCardCoordinator!
    var navigationController: NavigationControllerMock!
    
    override func setUp() {
        super.setUp()
        coordinator = AddCardOnboardingCoordinator()
        flowCoordinator = FakeOnboardingAddCardCoordinator()
        
        let service: AddCardOnboardingServiceProtocol = AddCardOnboardingService(cardManager: CreditCardManager.shared)
        let viewModel: AddCardOnboardingViewModelType = AddCardOnboardingViewModel(service: service)
        coordinator = AddCardOnboardingCoordinator()
        let viewController = AddCardOnboardingViewController(viewModel: viewModel, coordinator: coordinator)
        navigationController = NavigationControllerMock(rootViewController: viewController)
        
        coordinator.viewController = viewController
        coordinator.delegate = flowCoordinator
        viewModel.outputs = viewController
    }
    
    func testPerformShowContactsToFollow() {
        coordinator.perform(action: .addCreditCard)
        XCTAssert(navigationController.pushedViewController!.isKind(of: AddNewCreditCardViewController.self))
    }
    
    func testPerformSkip() {
        coordinator.perform(action: .skip)
        XCTAssert(flowCoordinator.didFinishOnboardingAddCardExecuted)
    }
}
