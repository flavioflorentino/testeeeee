@testable import PicPay
import XCTest

class FakeAddCardOnboardingViewController: AddCardOnboardingViewModelOutputs {
    var didSkipCardRegistration = false
    var didOpenAddCreditCard = false
    
    func perform(action: AddCardOnboardingAction) {
        switch action {
        case .skip:
            didSkipCardRegistration = true
        case .addCreditCard:
            didOpenAddCreditCard = true
        }
    }
}

class FakeAddCardOnboardingService: AddCardOnboardingServiceProtocol {
    var isCreditCardRegistered: Bool = false
}

class AddCardOnboardingViewModelTest: XCTestCase {
    var viewModel: AddCardOnboardingViewModel!
    var controller: FakeAddCardOnboardingViewController!
    var service: FakeAddCardOnboardingService!
    
    override func setUp() {
        super.setUp()
        service = FakeAddCardOnboardingService()
        viewModel = AddCardOnboardingViewModel(service: service)
        controller = FakeAddCardOnboardingViewController()
        viewModel.outputs = controller
    }
    
    func testSkipIfCreditCardRegisteredTrue() {
        service.isCreditCardRegistered = true
        viewModel.skipIfCreditCardRegistered()
        XCTAssert(controller.didSkipCardRegistration)
        XCTAssertFalse(controller.didOpenAddCreditCard)
    }
    
    func testSkipIfCreditCardRegisteredFalse() {
        service.isCreditCardRegistered = false
        viewModel.skipIfCreditCardRegistered()
        XCTAssertFalse(controller.didSkipCardRegistration)
        XCTAssertFalse(controller.didOpenAddCreditCard)
    }
    
    func testDidTapAddCardButton() {
        viewModel.didTapAddCardButton()
        XCTAssertFalse(controller.didSkipCardRegistration)
        XCTAssert(controller.didOpenAddCreditCard)
    }
    
    func testDidTapNoCardButton() {
        viewModel.didTapNoCardButton()
        XCTAssert(controller.didSkipCardRegistration)
        XCTAssertFalse(controller.didOpenAddCreditCard)
    }
    
    func testDidTapAddCardLaterButton() {
        viewModel.didTapAddCardLaterButton()
        XCTAssert(controller.didSkipCardRegistration)
        XCTAssertFalse(controller.didOpenAddCreditCard)
    }
    
    func testDidTapSkipButton() {
        viewModel.didTapSkipButton()
        XCTAssert(controller.didSkipCardRegistration)
        XCTAssertFalse(controller.didOpenAddCreditCard)
    }
}
