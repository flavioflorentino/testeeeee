@testable import PicPay
import XCTest

class FakeCashInCarouselViewController: CashInCarouselDisplay {
    private(set) var dataToSetupCarousel: [CashInCarouselPageModel]?
    private(set) var pageModelToDisplayButtons: CashInCarouselPageModel?
    private(set) var didDisplayNextPage = false
    private(set) var didDisplayLastPage = false
    
    func setupCarousel(with data: [CashInCarouselPageModel]) {
       dataToSetupCarousel = data
    }
    
    func displayButtons(for pageModel: CashInCarouselPageModel) {
        pageModelToDisplayButtons = pageModel
    }
    
    func displayNextPage() {
        didDisplayNextPage = true
    }
    
    func displayLastPage() {
        didDisplayLastPage = true
    }
}

class FakeCashInCarouselCoordinator: CashInCarouselCoordinating {
    var delegate: OnboardingCashInDelegate?
    
    var viewController: UIViewController?
    private(set) var actionPerformed: CashInCarouselAction?
    
    func perform(action: CashInCarouselAction) {
        actionPerformed = action
    }
}

class CashInCarouselPresenterTest: XCTestCase {
    private var presenter: CashInCarouselPresenting?
    private let viewController = FakeCashInCarouselViewController()
    private let coordinator = FakeCashInCarouselCoordinator()
    
    override func setUp() {
        super.setUp()
        presenter = CashInCarouselPresenter(coordinator: coordinator)
        presenter?.viewController = viewController
    }
    
    func testSetupCarousel() throws {
        let carouselModelMock: [CashInCarouselPageModel] = [
            CashInCarouselPageModel(
                page: .beginCashInCarouselPage,
                image: #imageLiteral(resourceName: "ilu_cashin_pig"),
                title: .beginCashInCarouselTitle,
                message: .beginCashInCarouselMessage,
                skipButtonTitle: .skip,
                nextButtonTitle: .btNext
            ),
            CashInCarouselPageModel(
                page: .cardCashInCarouselPage,
                image: #imageLiteral(resourceName: "ilu_cashin_card"),
                title: .cardCashInCarouselTitle,
                message: .cardCashInCarouselMessage,
                skipButtonTitle: .skip,
                nextButtonTitle: .btNext
            )
        ]
        
        presenter?.setupCarousel(with: carouselModelMock)
        let data = try viewController.dataToSetupCarousel.safe()
        XCTAssertEqual(data.count, 2)
        XCTAssertEqual(data[0].page, "INICIO")
        XCTAssertEqual(data[1].page, "CARTÃO")
    }
    
    func testSetupCarouselWithEmptyData() throws {
        presenter?.setupCarousel(with: [])
        let data = try viewController.dataToSetupCarousel.safe()
        XCTAssert(data.isEmpty)
    }
    
    func testDisplayButtons() throws {
        let pageModelMock = CashInCarouselPageModel(
            page: .balanceCashInCarouselPage,
            image: #imageLiteral(resourceName: "ilu_cashin_money"),
            title: .balanceCashInCarouselTitle,
            message: .balanceCashInCarouselMessage,
            skipButtonTitle: nil,
            nextButtonTitle: .begin
        )
        presenter?.displayButtons(for: pageModelMock)
        let pageModel = try viewController.pageModelToDisplayButtons.safe()
        XCTAssertEqual(pageModel.page, pageModelMock.page)
    }
    
    func testDisplayNextPage() {
        presenter?.displayNextPage()
        XCTAssert(viewController.didDisplayNextPage)
    }
    
    func testDisplayLastPage() {
        presenter?.displayLastPage()
        XCTAssert(viewController.didDisplayLastPage)
    }
    
    func testPerform() {
        presenter?.perform(action: .cashInOptions)
        XCTAssertEqual(coordinator.actionPerformed, .cashInOptions)
    }
}
