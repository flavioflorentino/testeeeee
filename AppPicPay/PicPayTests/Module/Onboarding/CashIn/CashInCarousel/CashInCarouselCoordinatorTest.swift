@testable import PicPay
import XCTest

class CashInCarouselCoordinatorTest: XCTestCase {
    var coordinator: CashInCarouselCoordinating?
    var navigationController: NavigationControllerMock?
        
    override func setUp() {
        super.setUp()
        coordinator = CashInCarouselCoordinator()
        let viewController = UIViewController()
        navigationController = NavigationControllerMock(rootViewController: viewController)
        coordinator?.viewController = viewController
    }
    
    func testPerform() {
        coordinator?.perform(action: .cashInOptions)
        XCTAssert(navigationController?.pushedViewController?.isKind(of: CashInOptionsViewController.self) ?? false)
    }
}
