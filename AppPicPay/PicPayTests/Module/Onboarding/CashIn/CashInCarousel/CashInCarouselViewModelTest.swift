@testable import PicPay
import XCTest

class FakeCashInCarouselPresenter: CashInCarouselPresenting {
    weak var viewController: CashInCarouselDisplay?
    
    private(set) var dataToSetupCarousel: [CashInCarouselPageModel]?
    private(set) var pageModelToDisplayButtons: CashInCarouselPageModel?
    private(set) var didDisplayNextPage = false
    private(set) var didDisplayLastPage = false
    private(set) var actionPerformed: CashInCarouselAction?
    
    func setupCarousel(with data: [CashInCarouselPageModel]) {
        dataToSetupCarousel = data
    }
    
    func displayButtons(for pageModel: CashInCarouselPageModel) {
        pageModelToDisplayButtons = pageModel
    }
    
    func displayNextPage() {
        didDisplayNextPage = true
    }
    
    func displayLastPage() {
        didDisplayLastPage = true
    }
    
    func perform(action: CashInCarouselAction) {
        actionPerformed = action
    }
}

class CashInCarouselViewModelTest: XCTestCase {
    private let presenter = FakeCashInCarouselPresenter()
    private var viewModel: CashInCarouselViewModelInputs?
    
    override func setUp() {
        super.setUp()
        viewModel = CashInCarouselViewModel(presenter: presenter)
    }
    
    func testViewDidLoad() throws {
        viewModel?.viewDidLoad()
        let data = try presenter.dataToSetupCarousel.safe()
        XCTAssertEqual(data.count, 3)
        XCTAssertEqual(data[0].page, "INICIO")
        XCTAssertEqual(data[1].page, "CARTÃO")
        XCTAssertEqual(data[2].page, "SALDO")
    }
    
    func testDidScrollToPage() throws {
        viewModel?.didScrollToPage(at: 0)
        var pageModel = try presenter.pageModelToDisplayButtons.safe()
        XCTAssertEqual(pageModel.skipButtonTitle, "Pular")
        XCTAssertEqual(pageModel.nextButtonTitle, "Próximo")
        
        viewModel?.didScrollToPage(at: 1)
        pageModel = try presenter.pageModelToDisplayButtons.safe()
        XCTAssertEqual(pageModel.skipButtonTitle, "Pular")
        XCTAssertEqual(pageModel.nextButtonTitle, "Próximo")
        
        viewModel?.didScrollToPage(at: 2)
        pageModel = try presenter.pageModelToDisplayButtons.safe()
        XCTAssertEqual(pageModel.skipButtonTitle, nil)
        XCTAssertEqual(pageModel.nextButtonTitle, "Começar")
    }
    
    func testDidScrollToPageWrongIndex() throws {
        viewModel?.didScrollToPage(at: 3)
        XCTAssertNil(presenter.pageModelToDisplayButtons)
    }
    
    func testDidTapNextButtonOverFirstPage() {
        viewModel?.didTapNextButton(overLastPage: false)
        XCTAssert(presenter.didDisplayNextPage)
    }
    
    func testDidTapNextButtonOverLastPage() {
        viewModel?.didTapNextButton(overLastPage: true)
        XCTAssertEqual(presenter.actionPerformed, .cashInOptions)
    }
    
    func testDidTapSkipButton() {
        viewModel?.didTapSkipButton()
        XCTAssertEqual(presenter.actionPerformed, .cashInOptions)
    }
}
