@testable import PicPay
import XCTest
import UI

class FakeCashInOptionsViewController: CashInOptionsDisplay {
    var sections: CashInSections?
    var bankOptions: [RechargeMethod]?
    
    var didDisplayError = false
    var didDisplayLoading = false
    var didHideLoading = false
    
    func displayCashInSections(_ sections: CashInSections) {
        self.sections = sections
    }
    
    func displayError() {
        didDisplayError = true
    }
    
    func displayLoading() {
        didDisplayLoading = true
    }
    
    func hideLoading() {
        didHideLoading = true
    }
        
    func displayBankOptions(_ bankOptions: [RechargeMethod]) {
        self.bankOptions = bankOptions
    }
}

class FakeCashInOptionsCoordinator: CashInOptionsCoordinating {
    var viewController: UIViewController?
    var delegate: OnboardingCashInDelegate?
    
    var didSetNextStepBill = false
    var didSetNextStepBank = false
    var didSetNextStepOriginal = false
    var didSetNextStepCredit = false
    var didSkipToNextStep = false
    
    func perform(action: CashInOptionsAction) {
        switch action {
        case .bill:
            didSetNextStepBill = true
        case .bank:
            didSetNextStepBank = true
        case .original:
            didSetNextStepOriginal = true
        case .credit:
            didSetNextStepCredit = true
        case .skip:
            didSkipToNextStep = true
        }
    }
}

class CashInOptionsPresenterTest: XCTestCase {
    private var presenter: CashInOptionsPresenting?
    private let viewController = FakeCashInOptionsViewController()
    private let coordinator = FakeCashInOptionsCoordinator()
    
    private func rechargeMethods() throws -> [RechargeMethod] {
        return try MockDecodable().loadCodableObject(resource: "rechargeMethodsMock", typeDecoder: .useDefaultKeys)
    }
    
    override func setUp() {
        super.setUp()
        presenter = CashInOptionsPresenter(coordinator: coordinator)
        presenter?.viewController = viewController
    }
    
    func testDisplayCashInSections() {
        let section = Section<CashInSectionHeader, CashInOption>(
            header: CashInSectionHeader(title: OnboardingLocalizable.addCard.text),
            items: []
        )
        presenter?.displayCashInSections([section])
        XCTAssertNotNil(viewController.sections)
        XCTAssertEqual(viewController.sections?.count, 1)
    }
    
    func testShowErrorMessage() {
        presenter?.showErrorMessage()
        XCTAssert(viewController.didDisplayError)
    }
    
    func testDisplayLoading() {
        presenter?.displayLoading()
        XCTAssert(viewController.didDisplayLoading)
    }
    
    func testHideLoading() {
        presenter?.hideLoading()
        XCTAssert(viewController.didHideLoading)
    }
    
    func testDidNextStepBill() throws {
        let method = try rechargeMethods()[0]
        presenter?.didNextStep(action: .bill(method))
        XCTAssert(coordinator.didSetNextStepBill)
    }
    
    func testDidNextStepBank() throws {
        let method = try rechargeMethods()[1]
        presenter?.didNextStep(action: .bank(method))
        XCTAssert(coordinator.didSetNextStepBank)
    }
    
    func testDidNextStepOriginal() throws {
        let method = try rechargeMethods()[2]
        presenter?.didNextStep(action: .original(method))
        XCTAssert(coordinator.didSetNextStepOriginal)
    }
    
    func testDidNextStepCredit() {
        presenter?.didNextStep(action: .credit)
        XCTAssert(coordinator.didSetNextStepCredit)
    }
    
    func testDidNextStepSkip() {
        presenter?.didNextStep(action: .skip)
        XCTAssert(coordinator.didSkipToNextStep)
    }
    
    func testDisplayBankOptionsEmpty() {
        presenter?.displayBankOptions([])
        XCTAssertNotNil(viewController.bankOptions)
        XCTAssert(viewController.bankOptions.isEmpty)
    }
    
    func testDisplayBankOptionsFilled() throws {
        presenter?.displayBankOptions(try rechargeMethods())
        XCTAssertNotNil(viewController.bankOptions)
        XCTAssertEqual(viewController.bankOptions?.count, 3)
    }
}
