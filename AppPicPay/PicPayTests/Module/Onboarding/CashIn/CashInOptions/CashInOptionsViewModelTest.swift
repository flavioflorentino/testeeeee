@testable import PicPay
import XCTest

class FakeCashInOptionsPresenter: CashInOptionsPresenting {
    var viewController: CashInOptionsDisplay?
    var sectionsToDisplay: CashInSections?
    var didShowErrorMessage = false
    
    var didDisplayLoading = false
    var didHideLoading = false
    
    var didSetNextStepBill = false
    var didSetNextStepBank = false
    var didSetNextStepOriginal = false
    var didSetNextStepCredit = false
    var didSkipToNextStep = false
    var rechargeMethodToNextStep: RechargeMethod?
    
    var bankOptionsDisplayed: [RechargeMethod]?
    
    func displayCashInSections(_ sections: CashInSections) {
        sectionsToDisplay = sections
    }
    
    func showErrorMessage() {
        didShowErrorMessage = true
    }
    
    func displayLoading() {
        didDisplayLoading = true
    }
    
    func hideLoading() {
        didHideLoading = true
    }
    
    func displayBankOptions(_ bankOptions: [RechargeMethod]) {
        bankOptionsDisplayed = bankOptions
    }
    
    func didNextStep(action: CashInOptionsAction) {
        switch action {
        case .bill(let rechargeMethod):
            didSetNextStepBill = true
            rechargeMethodToNextStep = rechargeMethod
        case .bank(let rechargeMethod):
            didSetNextStepBank = true
            rechargeMethodToNextStep = rechargeMethod
        case .original(let rechargeMethod):
            didSetNextStepOriginal = true
            rechargeMethodToNextStep = rechargeMethod
        case .credit:
            didSetNextStepCredit = true
        case .skip:
            didSkipToNextStep = true
        }
    }
}

class FakeCashInOptionsService: CashInOptionsServicing {
    var response: (Result<[RechargeMethod], PicPayError>)?
    
    func getRechargeOptions(_ completion: @escaping ((Result<[RechargeMethod], PicPayError>) -> Void)) {
        guard let response = response else {
            XCTFail("Mocked response did not set on FakeCashInOptionsService")
            return
        }
        completion(response)
    }
}

class CashInOptionsViewModelTest: XCTestCase {
    private let presenter = FakeCashInOptionsPresenter()
    private let service = FakeCashInOptionsService()
    private var viewModel: CashInOptionsViewModelInputs?
    
    private func rechargeMethods() throws -> [RechargeMethod] {
        return try MockDecodable().loadCodableObject(resource: "rechargeMethodsMock", typeDecoder: .useDefaultKeys)
    }
    
    override func setUp() {
        super.setUp()
        viewModel = CashInOptionsViewModel(service: service, presenter: presenter)
    }
    
    func testFetchCashInOptionsSuccess() throws {
        service.response = Result.success(try rechargeMethods())
        viewModel?.fetchCashInOptions()
        XCTAssertFalse(presenter.didShowErrorMessage)
        XCTAssertNotNil(presenter.sectionsToDisplay)
        
        let sections = try presenter.sectionsToDisplay.safe()
        XCTAssertEqual(sections.count, 2)
        XCTAssertEqual(sections.first?.items.count, 1)
        XCTAssertEqual(sections.last?.items.count, 3)
    }

    func testFetchCashInOptionsFailure() throws {
        service.response = Result.failure(MockError.genericError)
        viewModel?.fetchCashInOptions()
        XCTAssert(presenter.didDisplayLoading)
        XCTAssert(presenter.didHideLoading)
        XCTAssertNil(presenter.sectionsToDisplay)
        XCTAssert(presenter.didShowErrorMessage)
    }
    
    func testDidTapSkipButton() {
        viewModel?.didTapSkipButton()
        XCTAssert(presenter.didSkipToNextStep)
    }
    
    func testDidSelectOptionBill() throws {
        let billMethod = try rechargeMethods()[0]
        viewModel?.didSelectOption(type: .bill, rechargeMethod: billMethod)
        XCTAssert(presenter.didSetNextStepBill)
        XCTAssertEqual(presenter.rechargeMethodToNextStep?.name, billMethod.name)
    }
    
    func testDidSelectOptionBank() throws {
        let bankMethod = try rechargeMethods()[1]
        viewModel?.didSelectOption(type: .bank, rechargeMethod: bankMethod)
        XCTAssertNotNil(presenter.bankOptionsDisplayed)
        XCTAssertEqual(presenter.bankOptionsDisplayed?.count, 4)
    }
    
    func testDidSelectOptionBankWithNoOptions() throws {
        let bankMethodWithNoOptions = try rechargeMethods()[2]
        viewModel?.didSelectOption(type: .bank, rechargeMethod: bankMethodWithNoOptions)
        XCTAssertNil(presenter.bankOptionsDisplayed)
    }
    
    func testDidSelectOptionOriginalNotLinked() throws {
        let originalMethod = try rechargeMethods()[2]
        originalMethod.linked = false
        viewModel?.didSelectOption(type: .original, rechargeMethod: originalMethod)
        XCTAssert(presenter.didSetNextStepOriginal)
        XCTAssertEqual(presenter.rechargeMethodToNextStep?.name, originalMethod.name)
    }
    
    func testDidSelectOptionOriginalLinked() throws {
        let originalMethod = try rechargeMethods()[2]
        originalMethod.linked = true
        viewModel?.didSelectOption(type: .original, rechargeMethod: originalMethod)
        XCTAssert(presenter.didSetNextStepBank)
        XCTAssertEqual(presenter.rechargeMethodToNextStep?.name, originalMethod.name)
    }
    
    func testDidSelectOptionCredit() {
        viewModel?.didSelectOption(type: .credit, rechargeMethod: nil)
        XCTAssert(presenter.didSetNextStepCredit)
        XCTAssertNil(presenter.rechargeMethodToNextStep)
    }
    
    func testDidSelectOptionNone() throws {
        let rechargeMethod = try rechargeMethods()[2]
        viewModel?.didSelectOption(type: .none, rechargeMethod: rechargeMethod)
        XCTAssertFalse(presenter.didSkipToNextStep)
    }
    
    func testDidSelectOptionInvalidRechargeMethod() {
        viewModel?.didSelectOption(type: .bill, rechargeMethod: nil)
        XCTAssertFalse(presenter.didSetNextStepBill)
        XCTAssertFalse(presenter.didSetNextStepCredit)
        XCTAssertFalse(presenter.didSetNextStepOriginal)
        XCTAssertFalse(presenter.didSetNextStepBank)
        XCTAssertFalse(presenter.didSkipToNextStep)
    }
    
    func testDidSTapSkipButton() {
        viewModel?.didTapSkipButton()
        XCTAssert(presenter.didSkipToNextStep)
        XCTAssertNil(presenter.rechargeMethodToNextStep)
    }
    
    func testDidSelectBank() throws {
        let selectedBankMethod = try rechargeMethods()[1].options.safe().first.safe()
        viewModel?.didSelectBank(with: selectedBankMethod)
        XCTAssert(presenter.didSetNextStepBank)
        XCTAssertEqual(presenter.rechargeMethodToNextStep?.name, selectedBankMethod.name)
    }
}
