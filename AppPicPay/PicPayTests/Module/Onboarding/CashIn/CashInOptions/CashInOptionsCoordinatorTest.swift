@testable import PicPay
import XCTest

class FakeOnboardingFlowCoordinator: OnboardingCashInDelegate {
    var isFinishedOnboardingCashIn = false
    
    func didFinishOnboardingCashIn() {
        isFinishedOnboardingCashIn = true
    }
}

class CashInOptionsCoordinatorTest: XCTestCase {
    private let coordinator = CashInOptionsCoordinator()
    private let flowCoordinator = FakeOnboardingFlowCoordinator()
    private var navigationController: NavigationControllerMock?
    
    private func rechargeMethods() throws -> [RechargeMethod] {
        return try MockDecodable().loadCodableObject(resource: "rechargeMethodsMock", typeDecoder: .useDefaultKeys)
    }
    
    override func setUp() {
        super.setUp()
        let viewController = UIViewController()
        navigationController = NavigationControllerMock(rootViewController: viewController)
        coordinator.viewController = viewController
        coordinator.delegate = flowCoordinator
    }
    
    func testPerformSkip() throws {
        coordinator.perform(action: .skip)
        XCTAssertTrue(flowCoordinator.isFinishedOnboardingCashIn)
        XCTAssert(navigationController?.isDismissViewControllerCalled ?? false)
    }
    
    func testPerformCredit() throws {
        coordinator.perform(action: .credit)
        XCTAssert(navigationController?.viewControllerPresented?.isKind(of: UINavigationController.self) ?? false)
        let rechargeController = (navigationController?.viewControllerPresented as? UINavigationController)?.topViewController
        XCTAssert(rechargeController?.isKind(of: AddNewCreditCardViewController.self) ?? false)
    }
    
    func testPerformBill() throws {
        let rechargeMethod = try rechargeMethods()[0]
        coordinator.perform(action: .bill(rechargeMethod))
        XCTAssert(navigationController?.viewControllerPresented?.isKind(of: UINavigationController.self) ?? false)
        let rechargeController = (navigationController?.viewControllerPresented as? UINavigationController)?.topViewController
        XCTAssert(rechargeController?.isKind(of: RechargeValueViewController.self) ?? false)
    }
    
    func testPerformBank() throws {
        let rechargeMethod = try rechargeMethods()[1]
        coordinator.perform(action: .bill(rechargeMethod))
        XCTAssert(navigationController?.viewControllerPresented?.isKind(of: UINavigationController.self) ?? false)
        let rechargeController = (navigationController?.viewControllerPresented as? UINavigationController)?.topViewController
        XCTAssert(rechargeController?.isKind(of: RechargeValueViewController.self) ?? false)
    }
    
    func testPerformOriginal() throws {
        let rechargeMethod = try rechargeMethods()[2]
        coordinator.perform(action: .original(rechargeMethod))
        XCTAssert(navigationController?.viewControllerPresented?.isKind(of: UINavigationController.self) ?? false)
        let rechargeController = (navigationController?.viewControllerPresented as? UINavigationController)?.topViewController
        XCTAssert(try rechargeController.safe().isKind(of: RechargeOriginalViewController.self))
    }
}
