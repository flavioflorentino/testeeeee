@testable import PicPay
import XCTest

class FakeOnboardingFirstActionCoordinator: OnboardingFirstActionDelegate {
    var didExitOnboarding = false
    var didSkipToNextStep = false
    
    func exitOnboarding() {
        didExitOnboarding = true
    }
    
    func skipToNextStep() {
        didSkipToNextStep = true
    }
}


class FirstActionOnboardingCoordinatorTest: XCTestCase {
    private let flowCoordinator = FakeOnboardingFirstActionCoordinator()
    private lazy var coordinator: FirstActionOnboardingCoordinating = {
        let coordinator = FirstActionOnboardingCoordinator()
        coordinator.delegate = flowCoordinator
        return coordinator
    }()
    
    func testPerformShowContactsToFollow() {
        coordinator.perform(action: .openDeeplink(nil))
        XCTAssert(flowCoordinator.didExitOnboarding)
    }
    
    func testPerformSkip() {
        coordinator.perform(action: .close)
        XCTAssert(flowCoordinator.didExitOnboarding)
    }
}
