import Core
import FeatureFlag
import XCTest

@testable import PicPay

class FakeFirstActionOnboardingService: FirstActionOnboardingServicing {
    var firstActionOptions: [FirstActionCellPresenter] = []
    var userFullName: String?
}

class FakeFirstActionOnboardingViewController: FirstActionOnboardingViewModelOutputs {
    var data: [FirstActionCellPresenter] = []
    var didOpenDeeplink = false
    var deeplinkUrl: URL?
    var didCloseOnboarding = false
    var customTitle: NSAttributedString?
    
    
    func setupDataSourceHandler(withData data: [FirstActionCellPresenter]) {
        self.data = data
    }
    
    func perform(action: FirstActionOnboardingAction) {
        switch action {
        case .openDeeplink(let url):
            didOpenDeeplink = true
            deeplinkUrl = url
        case .close:
            didCloseOnboarding = true
        }
    }
    
    func setupCustomTitle(_ title: NSAttributedString) {
        customTitle = title
    }
}

class FirstActionOnboardingViewModelTest: XCTestCase {
    private var featureManagerMock = FeatureManagerMock()
    private lazy var mockDependencies = DependencyContainerMock(featureManagerMock)
    private let service = FakeFirstActionOnboardingService()
    private let viewController = FakeFirstActionOnboardingViewController()
    private lazy var viewModel: FirstActionOnboardingViewModelType = {
        let viewModel = FirstActionOnboardingViewModel(service: service, dependencies: mockDependencies)
        viewModel.outputs = viewController
        return viewModel
    }()
    
    func testFetchCollectionData() {
        let actions: [FirstActionCellPresenter] = try! MockCodable().loadCodableObject(resource: "FirstActionList")
        service.firstActionOptions = actions
        viewModel.inputs.fetchData()
        XCTAssertEqual(actions.count, viewController.data.count)
    }
    
    func fetchCustomTitle() {
        service.userFullName = "Fulano Silva de Oliveira"
        viewModel.inputs.fetchData()
        XCTAssertEqual(viewController.customTitle?.string, "Fulano")
    }
    
    func testTapActionCell() {
        let actions: [FirstActionCellPresenter] = try! MockCodable().loadCodableObject(resource: "FirstActionList")
        service.firstActionOptions = actions
        viewModel.inputs.fetchData()
        
        let indexTapped = 3
        viewModel.inputs.tapActionCell(at: indexTapped)
        XCTAssert(viewController.didOpenDeeplink)
        XCTAssertEqual(actions[indexTapped].deeplinkUrl, viewController.deeplinkUrl)
    }
    
    func testDidTapGoToHomeButton_WhenAppTourFeatureIsActive_ShouldSetFlagShowAppTourTrueAndCloseOnboarding() {
        KVStore().setBool(false, with: .showAppTour)
        featureManagerMock.override(keys: .appTour, with: true)
        
        viewModel.inputs.didTapGoToHomeButton()
        
        XCTAssert(viewController.didCloseOnboarding)
        XCTAssert(KVStore().boolFor(.showAppTour))
    }
    
    func testDidTapGoToHomeButton_WhenAppTourFeatureIsInactive_ShouldCloseOnboarding() {
        KVStore().setBool(false, with: .showAppTour)
        featureManagerMock.override(keys: .appTour, with: false)
        
        viewModel.inputs.didTapGoToHomeButton()
        
        XCTAssert(viewController.didCloseOnboarding)
        XCTAssertFalse(KVStore().boolFor(.showAppTour))
    }
}
