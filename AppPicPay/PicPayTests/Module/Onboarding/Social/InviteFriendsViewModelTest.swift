@testable import PicPay
import XCTest

class FakeInviteFriendsViewController: InviteFriendsViewModelOutputs {
    var title: String?
    var message: String?
    var image: UIImage?
    var didSkipToNextStep = false
    var didOpenShareCode = false
    
    func updateScreen(image: UIImage, title: String, message: String) {
        self.title = title
        self.message = message
        self.image = image
    }
    
    func perform(action: InviteFriendsAction) {
        switch action {
        case .skip:
            didSkipToNextStep = true
        case .shareCode:
            didOpenShareCode = true
        }
    }
}

class FakeInviteFriendsService: InviteFriendsServiceProtocol {
    var mgmConfigsScreenTitle: String?
    var mgmConfigsScreenMessage: String?
    var mgmEnabled: Bool = false
}

class InviteFriendViewModelTest: XCTestCase {
    let exampleTitle = "Any title"
    let exampleMessage = "Any message"
    
    var viewModel: InviteFriendsViewModel!
    var controller: FakeInviteFriendsViewController!
    var service: FakeInviteFriendsService!
    
    override func setUp() {
        super.setUp()
        service = FakeInviteFriendsService()
        viewModel = InviteFriendsViewModel(service: service)
        controller = FakeInviteFriendsViewController()
        viewModel.outputs = controller
    }
    
    func testFetchAndUpdateScreenTextMgmEnabled() {
        service.mgmEnabled = true
        service.mgmConfigsScreenTitle = exampleTitle
        service.mgmConfigsScreenMessage = exampleMessage
        viewModel.fetchAndUpdateScreenText()
        XCTAssertEqual(controller.title!, exampleTitle)
        XCTAssertEqual(controller.message!, exampleMessage)
        XCTAssertEqual(controller.image!, #imageLiteral(resourceName: "ico_mgm_invite"))
    }
    
    func testFetchAndUpdateScreenTextMgmDisabled() {
        service.mgmEnabled = false
        service.mgmConfigsScreenTitle = exampleTitle
        service.mgmConfigsScreenMessage = exampleMessage
        viewModel.fetchAndUpdateScreenText()
        XCTAssertEqual(controller.title!, OnboardingLocalizable.inviteFriendsDefaultTitle.text)
        XCTAssertEqual(controller.message!, OnboardingLocalizable.inviteFriendsDefaultMessage.text)
        XCTAssertEqual(controller.image!, #imageLiteral(resourceName: "empty_contact_list_popup"))
    }
    
    func testSkipIfPresentedSharedCodeScreenTrue() {
        viewModel.didTapInviteFriendsButton()
        viewModel.skipIfPresentedSharedCodeScreen()
        XCTAssert(controller.didSkipToNextStep)
    }
    
    func testSkipIfPresentedSharedCodeScreenFalse() {
        viewModel.skipIfPresentedSharedCodeScreen()
        XCTAssertFalse(controller.didSkipToNextStep)
    }
    
    func testDidTapInviteFriendsButton() {
        viewModel.didTapInviteFriendsButton()
        XCTAssert(controller.didOpenShareCode)
        XCTAssertFalse(controller.didSkipToNextStep)
    }
    
    func testDidTapCancelButton() {
        viewModel.didTapCancelButton()
        XCTAssertFalse(controller.didOpenShareCode)
        XCTAssert(controller.didSkipToNextStep)
    }
}
