@testable import PicPay
import XCTest
import AnalyticsModule
import CoreLegacy

class FakeOnboardingSocialCoordinator: OnboardingSocialDelegate {
    var didFinishOnboardSocialExecuted = false
    
    func didFinishOnboardingSocial() {
        didFinishOnboardSocialExecuted = true
    }
}


class FindFriendsCoordinatorTest: XCTestCase {
    var coordinator: FindFriendsCoordinating!
    var flowCoordinator: FakeOnboardingSocialCoordinator!
    var navigationController: NavigationControllerMock!
    var dependencies: DependencyContainerMock!
    
    override func setUp() {
        super.setUp()
        coordinator = FindFriendsCoordinator()
        flowCoordinator = FakeOnboardingSocialCoordinator()
        dependencies = DependencyContainerMock()
        
        let service: FindFriendsServiceProtocol = FindFriendsService(dependencies: dependencies)
        let viewModel: FindFriendsViewModelType = FindFriendsViewModel(
            service: service,
            dependencies: DependencyContainerMock(AnalyticsSpy())
        )
        
        let viewController = FindFriendsViewController(viewModel: viewModel, coordinator: coordinator)
        navigationController = NavigationControllerMock(rootViewController: viewController)
        coordinator.viewController = viewController
        coordinator.delegate = flowCoordinator
        viewModel.outputs = viewController
    }
    
    func testPerformShowContactsToFollow() {
        coordinator.perform(action: .showContactsToFollow)
        XCTAssert(navigationController.pushedViewController!.isKind(of: SocialOnboardingViewController.self))
    }
    
    func testPerformSkip() {
        coordinator.perform(action: .showRecommendedContactsToFollow)
        XCTAssert(navigationController.pushedViewController!.isKind(of: SocialOnboardingViewController.self))
    }
}

class InviteFriendsCoordinatorTest: XCTestCase {
    var coordinator: InviteFriendsCoordinating!
    var flowCoordinator: FakeOnboardingSocialCoordinator!
    var navigationController: NavigationControllerMock!
    
    override func setUp() {
        super.setUp()
        coordinator = InviteFriendsCoordinator()
        flowCoordinator = FakeOnboardingSocialCoordinator()
        
        let service: InviteFriendsServiceProtocol = InviteFriendsService(mgmConfigs: AppParameters.global().mgmConfigs)
        let viewModel: InviteFriendsViewModelType = InviteFriendsViewModel(service: service)
        coordinator = InviteFriendsCoordinator()
        let viewController = InviteFriendsViewController(viewModel: viewModel, coordinator: coordinator)
        navigationController = NavigationControllerMock(rootViewController: viewController)
        
        coordinator.viewController = viewController
        coordinator.delegate = flowCoordinator
        viewModel.outputs = viewController
    }
    
    func testPerformShareCode() {
        coordinator.perform(action: .shareCode)
        XCTAssert(navigationController.pushedViewController!.isKind(of: SocialShareCodeViewController.self))
    }
    
    func testPerformSkip() {
        coordinator.perform(action: .skip)
        XCTAssert(flowCoordinator.didFinishOnboardSocialExecuted)
    }
}
