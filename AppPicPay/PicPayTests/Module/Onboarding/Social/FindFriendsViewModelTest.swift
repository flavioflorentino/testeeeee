@testable import PicPay
import AnalyticsModule
import PermissionsKit
import XCTest

class FakeFindFriendsViewController: FindFriendsViewModelOutputs {
    var didShowContactsToFollow = false
    var didSkipFindFriends = false
    
    func perform(action: FindFriendsAction) {
        switch action {
        case .showRecommendedContactsToFollow:
            didSkipFindFriends = true
        case .showContactsToFollow:
            didShowContactsToFollow = true
        }
    }
}

class FakeFindFriendsService: FindFriendsServiceProtocol {
    var isAdressBookAuthorized: Bool = false
    var requestAddressBookPermissionStatus: PermissionStatus = .notDetermined
    var getFriendsRecommendation: Bool = false
    var getFriendsSuggestions: Bool = false
    
    func requestAddressBookPermission(_ completion: @escaping Permission.Callback) {
        completion(requestAddressBookPermissionStatus)
    }
    
    func getFriendsRecommendations(_ phoneNumber: String, completion: @escaping (Result<[RecommendedFriendItem], Error>) -> Void) {
        getFriendsRecommendation = true
    }
    
    func getFriendsSuggestions(completion: @escaping ([PPContact]?, Error?) -> Void) {
        getFriendsSuggestions = true
    }
}

class FindFriendsViewModelTest: XCTestCase {
    var viewModel: FindFriendsViewModel!
    var controller: FakeFindFriendsViewController!
    var service: FakeFindFriendsService!
    
    override func setUp() {
        super.setUp()
        service = FakeFindFriendsService()
        viewModel = FindFriendsViewModel(service: service, dependencies: DependencyContainerMock(AnalyticsSpy()))
        controller = FakeFindFriendsViewController()
        viewModel.outputs = controller
    }
    
    func testDidTapAuthorizeButtonSuccess() {
        service.isAdressBookAuthorized = true
        viewModel.didTapAuthorizeButton()
        XCTAssertFalse(controller.didSkipFindFriends)
        XCTAssert(controller.didShowContactsToFollow)
    }
    
    func testDidTapAuthorizeButtonWithRequestSuccess() {
        service.isAdressBookAuthorized = false
        service.requestAddressBookPermissionStatus = .authorized
        
        viewModel.didTapAuthorizeButton()
        XCTAssertFalse(controller.didSkipFindFriends)
        XCTAssert(controller.didShowContactsToFollow)
    }
    
    func testDidTapAuthorizeButtonWithRequestFailure() {
        service.isAdressBookAuthorized = false
        
        service.requestAddressBookPermissionStatus = .denied
        viewModel.didTapAuthorizeButton()
        
        service.requestAddressBookPermissionStatus = .disabled
        viewModel.didTapAuthorizeButton()
        
        service.requestAddressBookPermissionStatus = .notDetermined
        viewModel.didTapAuthorizeButton()
        
        XCTAssertFalse(controller.didSkipFindFriends)
        XCTAssertFalse(controller.didShowContactsToFollow)
    }
    
    func testDidTapNotNowButton() {
        viewModel.didTapNotNowButton()
        XCTAssert(controller.didSkipFindFriends)
        XCTAssertFalse(controller.didShowContactsToFollow)
    }
    
    func testDidTapSkipButton() {
        viewModel.didTapSkipButton()
        XCTAssert(controller.didSkipFindFriends)
        XCTAssertFalse(controller.didShowContactsToFollow)
    }
}
