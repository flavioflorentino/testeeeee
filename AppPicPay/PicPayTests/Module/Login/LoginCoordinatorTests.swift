@testable import PicPay
import XCTest

final class LoginCoordinatorTests: XCTestCase {
    private lazy var sut: LoginCoordinator = {
        let coordinator = LoginCoordinator(navigationController: UINavigationController(rootViewController: UIViewController()))
        return coordinator
    }()
    
    func testTfa_WhenLoginReceivedTFAId_ShouldStartTFAFlow() {
        var completedText = ""
        
        sut.start()
        sut.tfa(id: "123") {
            completedText = "Completed"
        }
        
        XCTAssertNotNil(sut.tfaCoordinator)
        
        sut.tfaCoordinator?.finish(completed: true)
        XCTAssertEqual(completedText, "Completed")
    }
    
    func testForgotPassword_ShouldStartPasswordResetCoordinator() {
        sut.forgotPassword()
        
        XCTAssertNotNil(sut.passwordResetCoordinator)
    }
}
