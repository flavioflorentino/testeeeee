import XCTest
import SecurityModule
import Registration
@testable import PicPay

class LoginPicpayWorkerTest: LoginPicpayWorker {
    private let loginStatus: Bool
    private let reactivateAccountStatus: Bool
    private let incompleteAccountStatus: Bool
    private let loginUserWithRestrictionStatus: Bool
    private let config: (validConsumer: Bool, notNilNumber: Bool, notEmptyNumber: Bool, validSms: Bool)
    private let errorCode: String
    
    init(loginStatus: Bool, reactivateAccountStatus: Bool, incompleteAccountStatus: Bool, loginUserWithRestrictionStatus: Bool,
         config: (validConsumer: Bool, notNilNumber: Bool, notEmptyNumber: Bool, validSms: Bool), errorCode: String) {
        self.loginStatus = loginStatus
        self.reactivateAccountStatus = reactivateAccountStatus
        self.incompleteAccountStatus = incompleteAccountStatus
        self.loginUserWithRestrictionStatus = loginUserWithRestrictionStatus
        self.config = config
        self.errorCode = errorCode
    }
    
    func isIncompleteAccount() -> Bool {
        return incompleteAccountStatus
    }
    
    func getConsumer() -> MBConsumer? {
        if config.validConsumer {
            let consumer = MBConsumer()
            consumer.verifiedPhoneNumber = config.notNilNumber ? "(27)997053363" : nil
            if config.notNilNumber {
                consumer.verifiedPhoneNumber = config.notEmptyNumber ? "(27)997053363" : ""
            }
            consumer.bypass_sms = config.validSms
            return consumer
        } else {
            return nil
        }
    }
    
    func loginUserWithRestriction(login: String, password: String, completion: @escaping (Result<RegistrationComplianceLoginModel, PicPayError>) -> Void) {
        if loginUserWithRestrictionStatus {
            let model = RegistrationComplianceLoginModel(
                step: .success,
                token: RegistrationComplianceToken(code: "someCode", type: .temporary),
                consumer: nil
            )
            completion(.success(model))
        } else {
            let picpayError = PicPayError(message: "Error", code: errorCode)
            completion(.failure(picpayError))
        }
    }
    
    func loginUser(login: String, password: String, anonymousId: String, captchaCode: String?, onSuccess: @escaping() -> Void, onError: @escaping(PicPayError) -> Void) {
        if loginStatus {
            onSuccess()
        } else {
            let picpayError = PicPayError(message: "Error", code: errorCode)
            onError(picpayError)
        }
    }
    
    func reactivateAccount(login: String, password: String, onSuccess: @escaping() -> Void, onError: @escaping(Error) -> Void) {
        if reactivateAccountStatus {
            onSuccess()
        } else {
            let picpayError = PicPayError(message: "Error")
            onError(picpayError)
        }
    }
}

class LoginViewModelTest: XCTestCase {
     private lazy var logDetectionServiceSpy = LogDetectionServiceSpy()
    
    func createViewModel(loginStatus: Bool = true,
                        reactivateAccountStatus: Bool = true,
                        incompleteAccountStatus: Bool = true,
                        loginUserWithRestrictionStatus: Bool = true,
                        config: (validConsumer: Bool, notNilNumber: Bool, notEmptyNumber: Bool, validSms: Bool) = (true, true, true, true),
                        errorCode: String = "-1",
                        logDetectionService: LogDetectionServicing = LogDetectionService()) -> LoginViewModel {
        let worker = LoginPicpayWorkerTest(loginStatus: loginStatus, reactivateAccountStatus: reactivateAccountStatus, incompleteAccountStatus: incompleteAccountStatus, loginUserWithRestrictionStatus: loginUserWithRestrictionStatus, config: config, errorCode: errorCode)
        let viewModel = LoginViewModel(worker: worker, logDetectionService: logDetectionService)
        
        return viewModel
    }
    
    func testLoginUserConsumerIsNilError() {
        let viewModel = createViewModel(config: (validConsumer: false, notNilNumber: true, notEmptyNumber: true, validSms: true))
        let expectation = XCTestExpectation(description: "API")
        
        viewModel.loginUser(login: "gustavo.storck", password: "1234", onSuccess: { (screen) in
            XCTFail()
        }, onError: { (type, error) in
            XCTAssertEqual(type, .none)
            XCTAssertNotNil(error)
            expectation.fulfill()
        })
            
        wait(for: [expectation], timeout: 5)
    }
    
    func testLoginUserConsumerToPersonalData() {
        let viewModel = createViewModel(incompleteAccountStatus: true, config: (validConsumer: true, notNilNumber: true, notEmptyNumber: true, validSms: true))
        let expectation = XCTestExpectation(description: "API")
        
        viewModel.loginUser(login: "gustavo.storck", password: "1234", onSuccess: { (screen) in
            XCTAssertEqual(screen, .personalData)
            expectation.fulfill()
        }, onError: { (type, error) in
            XCTFail()
        })
        
        wait(for: [expectation], timeout: 5)
    }
    
    func testLoginUserConsumerToHome_1() {
        let viewModel = createViewModel(incompleteAccountStatus: false, config: (validConsumer: true, notNilNumber: false, notEmptyNumber: true, validSms: true))
        let expectation = XCTestExpectation(description: "API")
        
        viewModel.loginUser(login: "gustavo.storck", password: "1234", onSuccess: { (screen) in
            XCTAssertEqual(screen, .home)
            expectation.fulfill()
        }, onError: { (type, error) in
            XCTFail()
        })
        
        wait(for: [expectation], timeout: 5)
    }
    
    func testLoginUserConsumerToHome_2() {
        let viewModel = createViewModel(incompleteAccountStatus: false, config: (validConsumer: true, notNilNumber: true, notEmptyNumber: false, validSms: true))
        let expectation = XCTestExpectation(description: "API")
        
        viewModel.loginUser(login: "gustavo.storck", password: "1234", onSuccess: { (screen) in
            XCTAssertEqual(screen, .home)
            expectation.fulfill()
        }, onError: { (type, error) in
            XCTFail()
        })
        
        wait(for: [expectation], timeout: 5)
    }
    
    func testLoginUserConsumerToHome_3() {
        let viewModel = createViewModel(incompleteAccountStatus: false, config: (validConsumer: true, notNilNumber: true, notEmptyNumber: true, validSms: false))
        let expectation = XCTestExpectation(description: "API")
        
        viewModel.loginUser(login: "gustavo.storck", password: "1234", onSuccess: { (screen) in
            XCTAssertEqual(screen, .home)
            expectation.fulfill()
        }, onError: { (type, error) in
            XCTFail()
        })
        
        wait(for: [expectation], timeout: 5)
    }
    
    func testLoginUserConsumerToHome_4() {
        let viewModel = createViewModel(incompleteAccountStatus: false, config: (validConsumer: true, notNilNumber: true, notEmptyNumber: true, validSms: true))
        let expectation = XCTestExpectation(description: "API")
        
        viewModel.loginUser(login: "gustavo.storck", password: "1234", onSuccess: { (screen) in
            XCTAssertEqual(screen, .home)
            expectation.fulfill()
        }, onError: { (type, error) in
            XCTFail()
        })
        
        wait(for: [expectation], timeout: 5)
    }
        
    func testLoginUserConsumerError_1() {
        let viewModel = createViewModel(loginStatus: false, errorCode: "1007")
        let expectation = XCTestExpectation(description: "API")
        
        viewModel.loginUser(login: "gustavo.storck", password: "1234", onSuccess: { (screen) in
            XCTFail()
        }, onError: { (type, error) in
            XCTAssertEqual(type, .reactivateAccount)
            XCTAssertNotNil(error)
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 5)
    }
    
    func testLoginUserConsumerError_2() {
        let viewModel = createViewModel(loginStatus: false, errorCode: "10584")
        let expectation = XCTestExpectation(description: "API")
        
        viewModel.loginUser(login: "gustavo.storck", password: "1234", onSuccess: { (screen) in
            XCTFail()
        }, onError: { (type, error) in
            XCTAssertEqual(type, .tfa)
            XCTAssertNotNil(error)
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 5)
    }
    
    func testLoginUserConsumerError_3() {
        let viewModel = createViewModel(loginStatus: false, errorCode: "2674")
        let expectation = XCTestExpectation(description: "API")
        
        viewModel.loginUser(login: "gustavo.storck", password: "1234", onSuccess: { (screen) in
            XCTFail()
        }, onError: { (type, error) in
            XCTAssertEqual(type, .captcha)
            XCTAssertNotNil(error)
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 5)
    }
    
    func testLoginUserConsumerError_4() {
        let viewModel = createViewModel(loginStatus: false, errorCode: "407")
        let expectation = XCTestExpectation(description: "API")
        
        viewModel.loginUser(login: "gustavo.storck", password: "1234", onSuccess: { (screen) in
            XCTFail()
        }, onError: { (type, error) in
            XCTAssertEqual(type, .none)
            XCTAssertNotNil(error)
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 5)
    }
    
    func testReactivateAccountSuccess() {
        let viewModel = createViewModel()
        let expectation = XCTestExpectation(description: "API")
        
        viewModel.reactivateAccount(login: "gustavo.storck", password: "1234", onSuccess: {
            expectation.fulfill()
        }, onError: { (error) in
            XCTFail()
        })
        wait(for: [expectation], timeout: 5)
    }
    
    func testReactivateAccountError() {
        let viewModel = createViewModel(reactivateAccountStatus: false)
        let expectation = XCTestExpectation(description: "API")
        
        viewModel.reactivateAccount(login: "gustavo.storck", password: "4321", onSuccess: {
            XCTFail()
        }, onError: { (error) in
            XCTAssertNotNil(error)
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 5)
    }
    
    func testLoginAndPasswordIsValid() {
        let viewModel = createViewModel()
        
        let result1 = viewModel.fieldsAreValid(login: "gustavo.storck", password: "1234")
        XCTAssertTrue(result1.login)
        XCTAssertTrue(result1.password)
        
        let result2 = viewModel.fieldsAreValid(login: "gustavo", password: "abc1234")
        XCTAssertTrue(result2.login)
        XCTAssertTrue(result1.password)
        
        let result3 = viewModel.fieldsAreValid(login: "gustavo_storck", password: "abcd")
        XCTAssertTrue(result3.login)
        XCTAssertTrue(result1.password)
    }
    
    func testLoginIsInvalid() {
        let viewModel = createViewModel()
        
        let result1 = viewModel.fieldsAreValid(login: "", password: "1234")
        XCTAssertFalse(result1.login)
        XCTAssertTrue(result1.password)
        
        let result2 = viewModel.fieldsAreValid(login: "", password: "abc1234")
        XCTAssertFalse(result2.login)
        XCTAssertTrue(result1.password)
        
        let result3 = viewModel.fieldsAreValid(login: "", password: "abcd")
        XCTAssertFalse(result3.login)
        XCTAssertTrue(result1.password)
    }
    
    func testPasswordIsInvalid() {
        let viewModel = createViewModel()
        let result1 = viewModel.fieldsAreValid(login: "gustavo.storck", password: "")
        XCTAssertTrue(result1.login)
        XCTAssertFalse(result1.password)
        
        let result2 = viewModel.fieldsAreValid(login: "gustavo", password: "")
        XCTAssertTrue(result2.login)
        XCTAssertFalse(result2.password)
        
        let result3 = viewModel.fieldsAreValid(login: "gustavo_storck", password: "")
        XCTAssertTrue(result3.login)
        XCTAssertFalse(result3.password)
    }
    
    func testLoginAndPasswordIsIsInvalid() {
        let viewModel = createViewModel()
        
        let result = viewModel.fieldsAreValid(login: "", password: "")
        XCTAssertFalse(result.login)
        XCTAssertFalse(result.password)
    }
    
    func testSendLogDetection_WhenCalled_ShouldSendLog() {
        let viewModel = createViewModel(logDetectionService: logDetectionServiceSpy)
        viewModel.sendLogDetection()
        XCTAssertEqual(logDetectionServiceSpy.sendLogCallCount, 1)
    }
    
    func testLogin_WhenRestrictionError_ShouldCallLoginWithRestriction() {
        let viewModel = createViewModel(
            loginStatus: false,
            loginUserWithRestrictionStatus: true,
            errorCode: "4015"
        )
        
        let expectation = XCTestExpectation(description: "API")
        
        viewModel.loginUser(login: "gustavo.storck", password: "1234", onSuccess: { (screen) in
            XCTAssert(screen == .registrationCompliance(result: .success, consumer: nil))
            expectation.fulfill()
        }, onError: { (type, error) in
            XCTFail()
        })
        
        wait(for: [expectation], timeout: 5)
    }
}
