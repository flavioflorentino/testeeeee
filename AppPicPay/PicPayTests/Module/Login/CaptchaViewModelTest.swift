import XCTest
@testable import PicPay

class CaptchaViewModelTest: XCTestCase {
    var captchaViewModel: CaptchaViewModel!
    
    override func setUp() {
        super.setUp()
        captchaViewModel = CaptchaViewModel()
    }
    
    func testGetCaptchaHtmlFilePathNotNil() {
        let path = captchaViewModel.getCaptchaHtmlFilePath()
        XCTAssertNotNil(path)
    }
    
    func testGetStringContentFromFileNotNil() {
        var content: String?
        if let path = captchaViewModel.getCaptchaHtmlFilePath() {
            content = captchaViewModel.getStringContentFromFile(at: path)
        }
        XCTAssertNotNil(content)
    }
}
