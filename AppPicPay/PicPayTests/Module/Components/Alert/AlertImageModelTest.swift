import XCTest
import SwiftyJSON
@testable import PicPay

class AlertImageModelTest: XCTestCase {
    func testAlertImageInitWithStyleSourceName() {
        let alertImage = Alert.Image(name: "image_123", style: .round, source: .url)
        XCTAssertEqual(alertImage.value, "image_123")
        XCTAssertEqual(alertImage.styleType, .round)
        XCTAssertEqual(alertImage.sourceType, .url)
    }
    
    func testAlertImageInitWithName() {
        let alertImage = Alert.Image(name: "image_123")
        XCTAssertEqual(alertImage.value, "image_123")
        XCTAssertEqual(alertImage.styleType, .standard)
        XCTAssertEqual(alertImage.sourceType, .asset)
    }
    
    func testAlertImageInitWithImageAndStyleType() {
        let image = UIImage()
        let alertImage = Alert.Image(with: image, styleType: .square)
        XCTAssert(alertImage.value.isEmpty)
        XCTAssertEqual(alertImage.styleType, .square)
        XCTAssertEqual(alertImage.sourceType, .url)
    }
    
    func testAlertImageInitWithValidIncompleteJson() {
        let url = "https://s3-sa-east-1.amazonaws.com/picpay-dev/sellers/5675678fas6578fads.png"
        let dictionary: [String: Any] = ["url": url]
        let json = JSON(dictionary)

        let alertImage = Alert.Image(json: json)!
        XCTAssertEqual(alertImage.value, url)
        XCTAssertEqual(alertImage.styleType, .standard)
        XCTAssertEqual(alertImage.sourceType, .url)
    }
    
    func testAlertImageInitWithValidCompleteJson() {
        let asset = "image_123"
        let dictionary: [String: Any] = ["asset": asset, "type": "square", "sourceType": "asset"]
        let json = JSON(dictionary)

        let alertImage = Alert.Image(json: json)!
        XCTAssertEqual(alertImage.value, asset)
        XCTAssertEqual(alertImage.styleType, .square)
        XCTAssertEqual(alertImage.sourceType, .asset)
    }
    
    func testAlertImageInitWithInvalidJson() {
        let dictionary: [String: Any] = ["type": "square", "sourceType": "asset"]
        let json = JSON(dictionary)

        let alertImage = Alert.Image(json: json)
        XCTAssertNil(alertImage)
    }
    
    func testAlertImageInitWithEmptyJson() {
        let dictionary: [String: Any] = [:]
        let json = JSON(dictionary)

        let alertImage = Alert.Image(json: json)
        XCTAssertNil(alertImage)
    }
}
