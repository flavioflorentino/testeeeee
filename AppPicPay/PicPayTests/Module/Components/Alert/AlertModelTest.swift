import XCTest
import SwiftyJSON
@testable import PicPay

class AlertModelTest: XCTestCase {
    func testAlertInitWithTitleAndTextString() {
        let title = "Um titulo qualquer"
        let text = "Um texto qualquer"
        let alert = Alert(title: title, text: text)
        XCTAssertEqual(alert.title, NSAttributedString(string: title))
        XCTAssertEqual(alert.text, NSAttributedString(string: text))
        
        XCTAssertEqual(alert.textType, .plain)
        XCTAssertNil(alert.image)
        XCTAssertTrue(alert.buttons.isEmpty)
        XCTAssertFalse(alert.showCloseButton)
        XCTAssertTrue(alert.dismissWithGesture)
        XCTAssertTrue(alert.dismissOnTouchBackground)
        XCTAssertNil(alert.overlayMaskAlpha)
    }
    
    func testAlertInitWithTitleAndTextAttributedString() {
        let title = NSAttributedString(string: "Um titulo qualquer")
        let text = NSAttributedString(string: "Um texto qualquer")
        let alert = Alert(title: title, text: text)
        XCTAssertEqual(alert.title, title)
        XCTAssertEqual(alert.text, text)
        
        XCTAssertEqual(alert.textType, .plain)
        XCTAssertNil(alert.image)
        XCTAssertTrue(alert.buttons.isEmpty)
        XCTAssertFalse(alert.showCloseButton)
        XCTAssertTrue(alert.dismissWithGesture)
        XCTAssertTrue(alert.dismissOnTouchBackground)
        XCTAssertNil(alert.overlayMaskAlpha)
    }
    
    func testAlertInitWithError() {
        let error = PicPayError(message: "Tente novamente", code: "0", title: "Erro", dictionary: nil)
        let image = Alert.Image(name: "image_123", style: .round, source: .asset)
        let alert = Alert(with: error, dismissOnTouch: false, dismissWithGesture: false, image: image)
        
        XCTAssertEqual(alert.title, NSAttributedString(string: "Erro"))
        XCTAssertEqual(alert.textType, .plain)
        XCTAssertEqual(alert.text, NSAttributedString(string: "Tente novamente"))
        XCTAssertNotNil(alert.image)
        
        XCTAssertTrue(alert.buttons.isEmpty)
        XCTAssertFalse(alert.showCloseButton)
        XCTAssertFalse(alert.dismissWithGesture)
        XCTAssertFalse(alert.dismissOnTouchBackground)
        XCTAssertNil(alert.overlayMaskAlpha)
    }
    
    func testAlertInitWithTitleTextAndButtons() {
        let title = NSAttributedString(string: "Um titulo qualquer")
        let text = NSAttributedString(string: "Um texto qualquer")
        let buttons = [
            Button(title: "Próximo", type: .cta, action: .handler),
            Button(title: "Fechar", type: .destructive, action: .close)
        ]
        let alert = Alert(with: title, text: text, buttons: buttons)
        
        XCTAssertEqual(alert.title, title)
        XCTAssertEqual(alert.textType, .plain)
        XCTAssertEqual(alert.text, text)
        XCTAssertNil(alert.image)
        
        XCTAssertEqual(alert.buttons.count, 2)
        
        let button0 = alert.buttons[0]
        XCTAssertEqual(button0.action, .handler)
        XCTAssertEqual(button0.type, .cta)
        XCTAssertEqual(button0.title,  "Próximo")
        
        let button1 = alert.buttons[1]
        XCTAssertEqual(button1.action, .close)
        XCTAssertEqual(button1.type, .destructive)
        XCTAssertEqual(button1.title, "Fechar")
        
        XCTAssertFalse(alert.showCloseButton)
        XCTAssertTrue(alert.dismissWithGesture)
        XCTAssertTrue(alert.dismissOnTouchBackground)
        XCTAssertNil(alert.overlayMaskAlpha)
    }
    
    func testAlertInitWithJson() {
        let json = MockJSON().load(resource: "alertSubscribePlanUserAgreement")
        let alert = Alert(json: json)!
        
        let expectedTitle = NSAttributedString(string: "Termos de uso de @gugacast")
        let expectedText = NSAttributedString(string: "Para continuar, é preciso aceitar os <a href=\"https://membership-qa.s3.amazonaws.com/terms/14312.html\">termos de uso</a> desta assinatura.")
            
        XCTAssertEqual(alert.title, expectedTitle)
        XCTAssertEqual(alert.textType, .html)
        XCTAssertEqual(alert.text, expectedText)
        XCTAssertNotNil(alert.image)
        
        XCTAssertEqual(alert.buttons.count, 2)
        
        let button0 = alert.buttons[0]
        XCTAssertEqual(button0.action, .callback)
        XCTAssertEqual(button0.type, .cta)
        XCTAssertEqual(button0.title, "Eu aceito os termos de uso")
        
        let button1 = alert.buttons[1]
        XCTAssertEqual(button1.action, .close)
        XCTAssertEqual(button1.type, .clean)
        XCTAssertEqual(button1.title, "Cancelar")
        
        XCTAssertFalse(alert.showCloseButton)
        XCTAssertTrue(alert.dismissWithGesture)
        XCTAssertTrue(alert.dismissOnTouchBackground)
        XCTAssertNil(alert.overlayMaskAlpha)
    }
    
    func testAlertInitWithTitleTextButtonsAndImages() {
        let title = "Um titulo qualquer"
        let text = "Um texto qualquer"
        let buttons = [Button(title: "OK", type: .underlined, action: .confirm)]
        let image = Alert.Image(name: "image_123", style: .round, source: .asset)
        let alert = Alert(with: title, text: text, buttons: buttons, image: image)
        
        XCTAssertEqual(alert.title, NSAttributedString(string: title))
        XCTAssertEqual(alert.textType, .plain)
        XCTAssertEqual(alert.text, NSAttributedString(string: text))
        XCTAssertNotNil(alert.image)
        
        XCTAssertEqual(alert.buttons.count, 1)
        let button = alert.buttons[0]
        XCTAssertEqual(button.action, .confirm)
        XCTAssertEqual(button.type, .underlined)
        XCTAssertEqual(button.title,  "OK")

        XCTAssertFalse(alert.showCloseButton)
        XCTAssertTrue(alert.dismissWithGesture)
        XCTAssertTrue(alert.dismissOnTouchBackground)
        XCTAssertNil(alert.overlayMaskAlpha)
    }
    
    func testAlertInitWithDictionary() {
        let json = MockJSON().load(resource: "alertSubscribePlanUserAgreement")
        let dictionary = json.toDictionary()!
        let alert = Alert(dictionary: dictionary)!
        
        let expectedTitle = NSAttributedString(string: "Termos de uso de @gugacast")
        let expectedText = NSAttributedString(string: "Para continuar, é preciso aceitar os <a href=\"https://membership-qa.s3.amazonaws.com/terms/14312.html\">termos de uso</a> desta assinatura.")
            
        XCTAssertEqual(alert.title, expectedTitle)
        XCTAssertEqual(alert.textType, .html)
        XCTAssertEqual(alert.text, expectedText)
        XCTAssertNotNil(alert.image)
        
        XCTAssertEqual(alert.buttons.count, 2)
        
        let button0 = alert.buttons[0]
        XCTAssertEqual(button0.action, .callback)
        XCTAssertEqual(button0.type, .cta)
        XCTAssertEqual(button0.title, "Eu aceito os termos de uso")
        
        let button1 = alert.buttons[1]
        XCTAssertEqual(button1.action, .close)
        XCTAssertEqual(button1.type, .clean)
        XCTAssertEqual(button1.title, "Cancelar")
        
        XCTAssertFalse(alert.showCloseButton)
        XCTAssertTrue(alert.dismissWithGesture)
        XCTAssertTrue(alert.dismissOnTouchBackground)
        XCTAssertNil(alert.overlayMaskAlpha)
    }
}
