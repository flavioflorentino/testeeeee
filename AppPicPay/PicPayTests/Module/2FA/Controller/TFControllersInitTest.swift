//
//  TFControllersInitTest.swift
//  PicPayTests
//
//  Created by Luiz Henrique Guimaraes on 08/02/19.
//

import XCTest
@testable import PicPay

class TFControllersInitTest: XCTestCase {
    func testShouldAccessNotificationViewNotNilWhenInitWithViewModel() {
        let controller = TFAAccessNotificationViewController(model: TFAAccessNotificationViewModel(device: "", date: "", email: "", changePasswordUrl: ""))
        XCTAssertNotNil(controller.view)
    }
}
