import XCTest
import OHHTTPStubs
@testable import PicPay

class StudentAccountCoordinatorTest: XCTestCase {
    var startViewController = UINavigationController(rootViewController: UIViewController())
    var coordinator: StudentAccountCoordinator!
    var topViewController: UIViewController? {
        return coordinator.navigation.topViewController
    }
    
    override func setUp() {
        super.setUp()
        coordinator = StudentAccountCoordinator(from: startViewController)
    }
    
    func testStartOnStatusViewController() {
        coordinator.start(from: .default)
        XCTAssertTrue(topViewController is StudentStatusViewController)
    }
    
    func testPushStatusViewController() {
        let selectUniversity = UniversitySelectViewController(with: UniversitySelectViewModel(with: coordinator))
        buildNavigationStack(with: selectUniversity)
        coordinator.pushStatusViewController()
        
        let expectation = self.expectation(description: "wait main thread")
        DispatchQueue.main.async { [weak self] in
            XCTAssertTrue(self?.coordinator.navigation.viewControllers.count == 1)
            XCTAssertTrue(self?.topViewController is StudentStatusViewController)
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testPushSelectUniversity() {
        coordinator.pushSelectUniversity()
        XCTAssertTrue(topViewController is UniversitySelectViewController)
    }
    
    func testPushApproved() {
        let status = StatusPayload(status: .active, title: "", message: "", image: "")
        coordinator.pushApproved(status)
        XCTAssertTrue(topViewController is StudentStatusApprovedViewController)
    }
    
    func testPushUploadDocuments() {
        let university = University(code: "1", name: "Hogwarts", acronym: "H", state: "California", city: "Los Santos")
        coordinator.pushDocuments(with: university)
        XCTAssertTrue(topViewController is UniversityDocumentsViewController)
    }
    
    func testPresentBenefits() {
        coordinator.openBenefits()
        testWebView(url: "conta-universitaria/benefits")
    }
    
    func testPresentRules() {
        coordinator.openRules()
        testWebView(url: "conta-universitaria/terms")
    }
    
    func testWebView(url: String) {
        XCTAssertTrue(topViewController is SettingsWebViewController)
        if let webview = topViewController as? SettingsWebViewController {
            XCTAssertTrue(webview.urlLoaded.contains(url))
        } else {
            XCTFail()
        }
    }
    
    private func buildNavigationStack(with controller: UIViewController) {
        coordinator.navigation.addChild(UIViewController())
        coordinator.navigation.addChild(UIViewController())
        coordinator.navigation.addChild(controller)
        coordinator.navigation.addChild(UIViewController())
    }
}
