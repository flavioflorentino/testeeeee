import XCTest
import OHHTTPStubs
@testable import PicPay

final class StudentStatusViewModelTest: XCTestCase {
    private lazy var viewModel = StudentStatusViewModel(with: coordinator)
    private lazy var coordinator = StudentAccountCoordinator(from: UIViewController())
    private let timeout: Double = 2
    private let loadStatusStubCondition: OHHTTPStubsTestBlock = isPath("/studentaccount/students/status")
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testShowLoading() {
        let loadingHasShown = XCTestExpectation(description: "expected loading to appear")
        let loadingHidden = XCTestExpectation(description: "expected loading to disappear")
        
        stub(condition: loadStatusStubCondition) { request in
            return StubResponse.success(with: Payload.pending.content)
        }
        
        viewModel.isLoading = { isLoading in
            guard isLoading else {
                loadingHidden.fulfill()
                return
            }
            loadingHasShown.fulfill()
        }
        
        viewModel.loadStatus()
        wait(for: [loadingHasShown, loadingHidden], timeout: timeout, enforceOrder: true)
    }
    
    func testShowError() {
        let expectation = XCTestExpectation(description: "expected error")
        stub(condition: loadStatusStubCondition) { request in
            return StubResponse.fail()
        }
        
        viewModel.onStatusError = { _ in
            expectation.fulfill()
        }
        
        viewModel.loadStatus()
        wait(for: [expectation], timeout: timeout)
    }
    
    func testLoadedStatusPayload() {
        let expectation = XCTestExpectation(description: "expected to load payloadStatus")
        
        stub(condition: loadStatusStubCondition) { request in
            return StubResponse.success(with: Payload.notApproved.content)
        }
        
        viewModel.onStatusLoad = { _ in
            expectation.fulfill()
        }
        
        viewModel.loadStatus()
        wait(for: [expectation], timeout: timeout)
    }
    
    func testIfShouldShowStatusOrRoute() {
        let expectation = XCTestExpectation(description: "expected to be called 4 times")
        expectation.expectedFulfillmentCount = 4
        
        viewModel.onStatusLoad = { _ in
            expectation.fulfill()
        }
        let status: [StudentStatus] = [.available, .active, .notApproved, .pending, .waitingAdjust, .expired]
        for stat in status {
            viewModel.showStatusOrRoute(with: StatusPayload(status: stat, title: "", message: "", image: nil))
        }
        
        wait(for: [expectation], timeout: timeout)
    }
    
    func testButtonActionOnWaitingAdjust() {
        viewModel.status = .waitingAdjust
        viewModel.buttonAction()
        coordinator.pushStatusViewController()

        let expectation = self.expectation(description: "Call dismiss")
        DispatchQueue.main.async { [weak self] in
            XCTAssertNil(self?.coordinator.navigation.topViewController)
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: timeout, handler: nil)
    }
    
    func testIfButtonActionClosedForStatus() {
        let statuses: [StudentStatus] = [.active, .notApproved, .pending, .expired]
        for status in statuses {
            viewModel.status = status
            viewModel.buttonAction()
            XCTAssertNil(coordinator.navigation.topViewController)
        }
    }
}

// Mark - Helpers
extension StudentStatusViewModelTest {
    enum Payload {
        case available
        case approved
        case notApproved
        case pending
        case waitingAdjust
        case expired
        
        var content: Data {
            return self.rawContent().data(using: .utf8)!
        }
        
        private func rawContent() -> String {
            switch self {
            case .available:
                return"""
                {
                "status": "available",
                "title": "Disponível",
                "message": "Disponível pra abrir Conta Universitária! :)"
                }
                """
            case .approved:
                return"""
                {
                "status": "approved",
                "title": "Aprovado",
                "message": "Aprovado"
                }
                """
            case .notApproved:
                return"""
                {
                "status": "not_approved",
                "title": "Não aprovado",
                "message": "Não aprovado"
                }
                """
            case .pending:
                return"""
                {
                "status": "pending",
                "title": "Aguardando aprovação",
                "message": "Aguardando aprovação"
                }
                """
            case .waitingAdjust:
                return"""
                {
                "status": "waiting_adjust",
                "title": "Aguardando ajustes",
                "message": "Aguardando ajustes"
                }
                """
            case .expired:
                return"""
                {
                "status": "expired",
                "title": "Expirado",
                "message": "Expirado"
                }
                """
            }
        }
    }
}
