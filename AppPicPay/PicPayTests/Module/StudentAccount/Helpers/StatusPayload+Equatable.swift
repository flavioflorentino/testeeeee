@testable import PicPay

extension StatusPayload: Equatable {
    public static func == (lhs: StatusPayload, rhs: StatusPayload) -> Bool {
        return lhs.status == rhs.status &&
            lhs.title == rhs.title &&
            lhs.message == rhs.message &&
            lhs.image == rhs.image
    }
}
