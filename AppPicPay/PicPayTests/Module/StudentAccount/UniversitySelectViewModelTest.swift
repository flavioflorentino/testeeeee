import XCTest
import OHHTTPStubs
@testable import PicPay

class UniversitySelectViewModelTest: XCTestCase {
    var viewModel: UniversitySelectViewModel!
    
    override func setUp() {
        super.setUp()
        startViewModel()
        
    }
    
    override func tearDown() {
        super.tearDown()
        OHHTTPStubs.removeAllStubs()
        startViewModel()
    }
    
    private func startViewModel() {
        viewModel = UniversitySelectViewModel(with: StudentAccountCoordinator(from: UIViewController()))
    }
    
    func mockUniversitiesList() {
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(with: UniversitySelectViewModelTest.payload)
        }
        viewModel.loadUniversities()
    }
    
    func testLoadUniversities() {
        let loadingExpectation = XCTestExpectation(description: "expected to show loading universities")
        let needInputExpectation = XCTestExpectation(description: "expected to change state to needInput")
        
        viewModel.update = { state in
            switch state {
            case .loading:
                loadingExpectation.fulfill()
            case .needInput:
                needInputExpectation.fulfill()
            default:
                XCTFail()
            }
        }
        
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(with: UniversitySelectViewModelTest.payload)
        }
        viewModel.loadUniversities()
        
        wait(for: [loadingExpectation, needInputExpectation], timeout: 5.0, enforceOrder: true)
    }
    
    func testErrorLoadingUniversities() {
        let loadingExpectation = XCTestExpectation(description: "expected to show loading universities")
        let loadingError = XCTestExpectation(description: "expected error")
        
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.fail()
        }
        
        viewModel.update = { state in
            switch state {
            case .loading:
                loadingExpectation.fulfill()
            case .errorLoading:
                loadingError.fulfill()
            default:
                XCTFail("expected an error")
            }
        }
        
        viewModel.loadUniversities()
        wait(for: [loadingExpectation, loadingError], timeout: 5.0, enforceOrder: true)
    }
    
    func testFilterEmpty() {
        viewModel.update = { state in
            XCTAssertEqual(state, UniversitySelectView.State.needInput)
        }
        
        viewModel.searchBar(UISearchBar(), textDidChange: "")
    }
    
    func testFilterUserInputResults() {
        let loadingExpectation = XCTestExpectation(description: "expected to show loading universities")
        let needInputExpectation = XCTestExpectation(description: "expected to change state to needInput")
        let filterExpectation = XCTestExpectation(description: "expected to filter results")
        filterExpectation.expectedFulfillmentCount = 4
        let notFoundExpectation = XCTestExpectation(description: "expected notFound results")
        let selectedUniversityExpectation = XCTestExpectation(description: "expected to select university")
        
        viewModel.update = { state in
            switch state {
            case .loading:
                loadingExpectation.fulfill()
            case .needInput:
                needInputExpectation.fulfill()
                self.mockUserInputs()
            case .showingResult:
                filterExpectation.fulfill()
            case .notFound:
                notFoundExpectation.fulfill()
            default:
                XCTFail()
            }
        }
        
        viewModel.didSelectUniversity = { _ in
            selectedUniversityExpectation.fulfill()
        }
        
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(with: UniversitySelectViewModelTest.payload)
        }
        
        viewModel.loadUniversities()
        wait(for: [loadingExpectation, needInputExpectation, notFoundExpectation, filterExpectation, selectedUniversityExpectation], timeout: 10.0, enforceOrder: true)
    }
    
    func mockUserInputs() {
        let searchString = ["Hogwarts School of Wizardry", "UNIVERSIDADE", "UFMT", "MT", "CUIABA", "BRASILIA"]
        searchString.forEach { search in viewModel.searchBar(UISearchBar(), textDidChange: search) }
        viewModel.tableView(UITableView(), didSelectRowAt: IndexPath(row: 0, section: 0))
    }
    
    func testSelectUniversityWithoutFiltering() {
        let loadingExpectation = XCTestExpectation(description: "expected to show loading universities")
        let needInputExpectation = XCTestExpectation(description: "expected to change state to needInput")
        let selectedUniversityExpectation = XCTestExpectation(description: "expected to select university")
        
        viewModel.update = { state in
            switch state {
            case .loading:
                loadingExpectation.fulfill()
            case .needInput:
                needInputExpectation.fulfill()
                self.viewModel.tableView(UITableView(), didSelectRowAt: IndexPath(row: 5, section: 0))
            default:
                XCTFail()
            }
        }
        
        viewModel.didSelectUniversity = { university in
            selectedUniversityExpectation.fulfill()
            XCTAssertEqual("UNIVERSIDADE FEDERAL DE OURO PRETO", university)
        }
        
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(with: UniversitySelectViewModelTest.payload)
        }
        
        viewModel.loadUniversities()
        wait(for: [loadingExpectation, needInputExpectation, selectedUniversityExpectation], timeout: 10.0, enforceOrder: true)
    }
    
    func testSearchBarCancel() {
        viewModel.update = { state in
            XCTAssertEqual(state, UniversitySelectView.State.needInput)
        }
        
        viewModel.searchBarCancelButtonClicked(UISearchBar())
    }
    
    func testProceedWithSelectedUniversity() {
        
    }
}

extension UniversitySelectViewModelTest {
    static let payload =
        
        """
    {
    "data":[
    {
    "code":"1",
    "name":"UNIVERSIDADE FEDERAL DE MATO GROSSO",
    "acronym":"UFMT",
    "state":"MT",
    "city":"CUIABA"
    },
    {
    "code":"2",
    "name":"UNIVERSIDADE DE BRASÍLIA",
    "acronym":"UNB",
    "state":"DF",
    "city":"BRASILIA"
    },
    {
    "code":"3",
    "name":"UNIVERSIDADE FEDERAL DE SERGIPE",
    "acronym": null,
    "state":"SE",
    "city":"SAO CRISTOVAO"
    },
    {
    "code":"4",
    "name":"UNIVERSIDADE FEDERAL DO AMAZONAS",
    "acronym":"UFAM",
    "state":"AM",
    "city": null
    },
    {
    "code":"5",
    "name":"UNIVERSIDADE FEDERAL DO PIAU",
    "acronym":"UFPI",
    "state":"PI",
    "city":"TERESINA"
    },
    {
    "code":"6",
    "name":"UNIVERSIDADE FEDERAL DE OURO PRETO",
    "acronym":"UFOP",
    "state":"MG",
    "city":"OURO PRETO"
    },
    {
    "code":"7",
    "name":"UNIVERSIDADE FEDERAL DE SÃO CARLOS",
    "acronym":"UFSCAR",
    "state":"SP",
    "city":"SAO CARLOS"
    },
    {
    "code":"8",
    "name":"UNIVERSIDADE FEDERAL DE VIÇOSA",
    "acronym":"UFV",
    "state":"MG",
    "city":"VICOSA"
    }
    ]
    }
""".data(using: .utf8)!
}
