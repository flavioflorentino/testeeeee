import XCTest
import OHHTTPStubs
@testable import PicPay

class UniversityDocumentsViewModelTest: XCTestCase {
    var viewModel: UniversityDocumentsViewModel!
    var coordinator: StudentAccountCoordinator!
    
    override func setUp() {
        super.setUp()
        startViewModel()
        
    }
    
    override func tearDown() {
        super.tearDown()
        OHHTTPStubs.removeAllStubs()
        startViewModel()
    }
    
    private func startViewModel() {
        coordinator = StudentAccountCoordinator(from: UIViewController())
        viewModel = UniversityDocumentsViewModel(with: coordinator)
    }
    
    private func mockSelfie() {
        let image = #imageLiteral(resourceName: "avatar_person")
        viewModel.selectedPicture(image, type: .selfie)
    }
    
    private func mockDocument() {
        let image = #imageLiteral(resourceName: "ilu_identity_validation_proc")
        viewModel.selectedPicture(image, type: .document)
    }
    
    private func mockSelfieAndDocument() {
        mockSelfie()
        mockDocument()
    }
    
    func testDeleteSelfie() {
        mockSelfie()
        viewModel.showDeleteDialog = { route in
            XCTAssertEqual(route, UniversityDocumentType.selfie)
        }
        
        viewModel.tap(.selfie)
    }
    
    func testDeleteDocument() {
        mockDocument()
        viewModel.showDeleteDialog = { route in
            XCTAssertEqual(route, UniversityDocumentType.document)
        }
        
        viewModel.tap(.document)
    }
    
    func testTakeSelfie() {
        viewModel.showActionSheet = { route in
            XCTAssertEqual(route, UniversityDocumentType.selfie)
        }
        
        viewModel.takePicture(.selfie)
    }
    
    func testAttachDocument() {
        viewModel.showActionSheet = { route in
            XCTAssertEqual(route, UniversityDocumentType.document)
        }
        
        viewModel.takePicture(.document)
    }
    
    func testShowActionSheetSelfie() {
        viewModel.showActionSheet = { route in
            XCTAssertEqual(route, UniversityDocumentType.selfie)
        }
        viewModel.tap(.selfie)
    }
    
    func testShowActionSheetDocument() {
        viewModel.showActionSheet = { route in
            XCTAssertEqual(route, UniversityDocumentType.document)
        }
        viewModel.tap(.document)
    }
    
    func testIsValid() {
        mockSelfie()
        viewModel.updateState = { isValid in
            XCTAssertTrue(isValid)
        }
        
        mockDocument()
    }
    
    func testImageSelfie() {
        let image = UIImage()
        viewModel.selectedPicture(image, type: .selfie)
        XCTAssertEqual(viewModel.selfie, image)
    }
    
    func testImageDocument() {
        let image = UIImage()
        viewModel.selectedPicture(image, type: .document)
        XCTAssertEqual(viewModel.document, image)
    }
    
    func mockSelectedUniversity() {
        coordinator.selectedUniversity = University(code: "1", name: "Hogwarts", acronym: "HW", state: "Paraíba", city: "Fundão")
    }
    
    func testUploadWithOnlySelfie() {
        let loadingExpectation = XCTestExpectation(description: "expected to show loading")
        let errorExpectation = XCTestExpectation(description: "expected to receive an error")
        
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(with: Data())
        }
        
        mockSelectedUniversity()
        mockSelfie()
        
        viewModel.showLoading = {
            loadingExpectation.fulfill()
        }
        
        viewModel.errorUploading = {
            errorExpectation.fulfill()
        }
        
        viewModel.upload()
        wait(for: [loadingExpectation, errorExpectation], timeout: 5.0, enforceOrder: true)
    }
    
    func testUploadErrorApi() {
        let loadingExpectation = XCTestExpectation(description: "expected to show loading")
        let errorExpectation = XCTestExpectation(description: "expected to receive an error")
        
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.fail()
        }
        
        mockSelectedUniversity()
        mockSelfieAndDocument()
        
        viewModel.showLoading = {
            loadingExpectation.fulfill()
        }
        
        viewModel.errorUploading = {
            errorExpectation.fulfill()
        }
        
        viewModel.upload()
        wait(for: [loadingExpectation, errorExpectation], timeout: 5.0, enforceOrder: true)
    }
    
    func testSuccessUpload() {
        let successExpectation = XCTestExpectation(description: "expected success upload")
        
        stub(condition: isHost💚.PicPay) { request in
            return StubResponse.success(with: Data())
        }
        
        viewModel.successUpload = {
            successExpectation.fulfill()
        }
        
        mockSelectedUniversity()
        mockSelfieAndDocument()
        viewModel.upload()
        
        wait(for: [successExpectation], timeout: 5.0)
    }
}
