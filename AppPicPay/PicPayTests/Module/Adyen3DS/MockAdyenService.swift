@testable import PicPay
import XCTest

class MockAdyenService: AdyenServicing {
    private let firstAccess: Bool
    private let completeSuccess: Bool
    private let transactionSuccess: Bool
    private let errorCode: Int
    
    init(
        firstAccess: Bool = true,
        completeSuccess: Bool = true,
        transactionSuccess: Bool = true,
        errorCode: Int = 400
    ) {
        self.firstAccess = firstAccess
        self.completeSuccess = completeSuccess
        self.transactionSuccess = transactionSuccess
        self.errorCode = errorCode
    }
    
    var firstAccess3DS: Bool {
        return firstAccess
    }
    
    func transaction(with payload: [String: String],
                     hasChallenge: @escaping (ADYTransactionProtocol, ADYChallengeParametersProtocol, [String: Any]) -> Void,
                     succeeded: @escaping (ReceiptWidgetViewModel) -> Void,
                     error: @escaping (PicPayError) -> Void,
                     injecting service: ADYServiceProtocol.Type) {
        
        guard transactionSuccess else {
            let picpayError = PicPayError(message: "Transaction", code: String(errorCode))
            error(picpayError)
            return
        }
        let viewModel = ReceiptWidgetViewModel(type: .P2P)
        succeeded(viewModel)
        
    }
    
    func complete(payload: [String: Any], succeeded: @escaping (ReceiptWidgetViewModel) -> Void, error: @escaping (PicPayError) -> Void) {
        guard completeSuccess else {
            let picpayError = PicPayError(message: "Complete", code: String(errorCode))
            error(picpayError)
            return
        }
        let viewModel = ReceiptWidgetViewModel(type: .P2P)
        succeeded(viewModel)
    }
}
