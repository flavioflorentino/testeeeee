import XCTest
@testable import PicPay

class Adyen3DSTest: XCTestCase {
    let token = "BQABAQCyKfbu0RxOt07da3OOSjGXC5bNM0zXWbXFuRSpURGkig67jA9KSGqocmv63478tDNGfGulZqmI9nxCH49yo1NJNME/5QeaDBmKB2AhfeQua8pyi1L8Y826K6J68Zn6Z7A2pW2AZCwDgyKQxOXIAQ9mkf6R/nSxAwiOZwRIuJxoclI2hQjjQxHTffZByBu1pqhzOCIM9j33AxIPNJRgoIbIhzRYSUtBPKSEcB8OpKucrjFgSyIzXUuCHlnKyKiisOxYSHwQ8YZNywi3DXCP3jSU5Gw5CNp1uL2ljNmHDszoCHvEsBrsw5lOEYcp3vxErWvnaYmPfdSC9prMeRRxQ+PDEFrtyMf40vdB3J4d6lwvOpYAABgupnp+h8CtcYG2q8pEE/yfc1N4gAcmNtu3uoGy2Un0q6I/UPOAck/Gw9ElYab4U9g2VtMCE7srC4aBqs+FAlb1pv3yi1pmg4a9b/xjOl1SRebYUakgCfxDkvI+3723v62C72dc1ftaEUhklWZNOE2xCMGm4JB+3YXHbBy9CPakdhxo6GsP3SySEgOyvwwzQjIUN3WLw1p0NDHQ+AbepyuD7WbXpjjZzFJ7gY1fmLpfOlII2bzi1yZIGbKWH0dGZsf+kWGeaSBZjhdKiyjikhwJbyT9Q9tvGOppAOosf3f0tYwpEDzXLHmg4nIjT6dzVemrFZ+iDxsfnIrxBFnVtrp6Ak8Y4GYG/wFjKW6hwxYkXmqPDkA3zlMWK8F5lBZPCHOV0ODl/7pHKuhz52o5nb84sMoLA0ukgnETaAGdz9X3ZfnLSe9k66hXlOzBNkrsn8OjtEthmxJDNYwpqmG3J70nQ8FfvgWJDXo6TvdVxGmYFLijVqcVer3WnLn4/hPgwscOROTVtFTmCkkwmsPhOoV9wOCCxNt4ss5c1ECwMYAAZDvnbmhhS1pypRkH5ipVDH8="
    let serverId = "4fb7ddce-98ab-49e3-9d8c-a31bebe70943"
    let directoryServerId = "A000000003"
    let directoryPublicKey = "eyJrdHkiOiJSU0EiLCJlIjoiQVFBQiIsIm4iOiI4VFBxZkFOWk4xSUEzcHFuMkdhUVZjZ1g4LUpWZ1Y0M2diWURtYmdTY0N5SkVSN3lPWEJqQmQyaTBEcVFBQWpVUVBXVUxZU1FsRFRKYm91bVB1aXVoeVMxUHN2NTM4UHBRRnEySkNaSERkaV85WThVZG9hbmlrU095c2NHQWtBVmJJWHA5cnVOSm1wTTBwZ0s5VGxJSWVHYlE3ZEJaR01OQVJLQXRKeTY3dVlvbVpXV0ZBbWpwM2d4SDVzNzdCR2xkaE9RUVlQTFdybDdyS0pLQlUwNm1tZlktUDNpazk5MmtPUTNEak02bHR2WmNvLThET2RCR0RKYmdWRGFmb29LUnVNd2NUTXhDdTRWYWpyNmQyZkppVXlqNUYzcVBrYng4WDl6a1c3UmlxVno2SU1qdE54NzZicmg3aU9Vd2JiWmoxYWF6VG1GQ2xEb0dyY2JxOV80NncifQ=="
    
    let id = "1234"
    let type = "p2p"
    
    func testIfStatusCodeIsValid() {
         let error = PicPayError(message: "3DS", code: String(AdyenConstant.code))
        XCTAssertTrue(Adyen3DS.is3ds(error: error))
    }
    
    func testIfStatusCodeIsInvalid() {
        let error = PicPayError(message: "3DS", code: "428")
        XCTAssertFalse(Adyen3DS.is3ds(error: error))
    }
    
    func testIfErrorAdyChallengeCancelled() {
        let error = NSError(domain: "3DSTransaction", code: 5, userInfo: nil)
        XCTAssertTrue(Adyen3DS.challengeCancelled(error))
    }
    
    func testAdyenPayloadNotNil() throws {
        let dict = MockJSON().load(resource: "identifyShopper")
        let dictionary = try dict.dictionaryObject.safe()
        let error = PicPayError(message: "3DSTransaction", code: String(AdyenConstant.code), dictionary: dictionary as NSDictionary)
        XCTAssertNotNil(Adyen3DS.adyenPayload(error: error))
    }
  
    func testAdyenPayloadNil() {
        let error = PicPayError(message: "3DSTransaction", code: String(AdyenConstant.code))
        XCTAssertNil(Adyen3DS.adyenPayload(error: error))
    }
    
    func testParseAdyenPayloadSuccess() {
        let model = loadFromMock(with: "3DSRedirectSuccess")!
        XCTAssertNotNil(model)
        XCTAssertEqual(model.transactionId, id)
        XCTAssertEqual(model.transactionType, type)
        XCTAssertEqual(model.threeDS2Token, token)
        XCTAssertEqual(model.threeDSServerTransID, serverId)
        XCTAssertEqual(model.directoryServerId, directoryServerId)
        XCTAssertEqual(model.directoryServerPublicKey, directoryPublicKey)
        
        let dict = model.dict
        XCTAssertEqual(dict["transactionId"], id)
        XCTAssertEqual(dict["transactionType"], type)
        XCTAssertEqual(dict["threeDS2Token"], token)
        XCTAssertEqual(dict["threeDSServerTransID"], serverId)
        XCTAssertEqual(dict["directoryServerId"], directoryServerId)
        XCTAssertEqual(dict["directoryServerPublicKey"], directoryPublicKey)
        
        let objcFromDict = AdyenPayload(dict)
        XCTAssertEqual(objcFromDict.transactionId, id)
        XCTAssertEqual(objcFromDict.transactionType, type)
        XCTAssertEqual(objcFromDict.threeDS2Token, token)
        XCTAssertEqual(objcFromDict.threeDSServerTransID, serverId)
        XCTAssertEqual(objcFromDict.directoryServerId, directoryServerId)
        XCTAssertEqual(objcFromDict.directoryServerPublicKey, directoryPublicKey)
    }
    
    func testParseAdyenPayloadFail() {
        let model = loadFromMock(with: "3DSRedirectFail")
        XCTAssertNil(model)
    }
    
    func loadFromMock(with resource: String) -> AdyenPayload? {
        let bundle = Bundle.init(for: Adyen3DSTest.self)
        if let path = bundle.path(forResource: resource, ofType: "json") {
            if let jsonData = NSData(contentsOfFile: path) {
                let decoder = JSONDecoder()
                if let adyenPayload = try? decoder.decode(AdyenPayload.self, from: jsonData as Data) {
                    return adyenPayload
                }
            }
        }
        return nil
    }
    
    func testServiceParametersFromPayload() {
        guard let model = loadFromMock(with: "3DSRedirectSuccess") else {
            XCTFail("Should have AdyenPayload here")
            return
        }
        
        let serviceParameters = ADYServiceParameters.with(model)
        XCTAssertEqual(serviceParameters.directoryServerIdentifier, directoryServerId)
        XCTAssertEqual(serviceParameters.directoryServerPublicKey, directoryPublicKey)
    }
    
    
    class MockAdyRequestParameterers: ADYAuthenticationRequestParametersProtocol {
        var deviceInformation: String = ""
        var sdkEphemeralPublicKey: String = ""
        var sdkApplicationIdentifier: String = ""
        var sdkReferenceNumber: String = ""
        var sdkTransactionIdentifier: String = "9812739812739-312-3-12-321-3"
        
        init(valid: Bool = true) {
            self.deviceInformation = "iPhoneXS"
            self.sdkApplicationIdentifier = "sdkApplicationIdentifier"
            self.sdkReferenceNumber = "sdkReferenceNumber"
            self.sdkTransactionIdentifier = "981z27a398s129-981w27c398s129"
            self.sdkEphemeralPublicKey = valid ? validPublicKeyJSON() : ""
        }
        
        func validPublicKeyJSON() -> String {
            let bundle = Bundle.init(for: Adyen3DSTest.self)
            let path = bundle.path(forResource: "3DSEphemeralKey", ofType: "json")!
            let jsonData = NSData(contentsOfFile: path)!
            let string = String.init(data: jsonData as Data, encoding: .utf8)!
            return string
        }
    }
    
    func testParametersAdyenFailInvalidPublicKey() {
        let worker = AdyenWorker()
        guard let payload = loadFromMock(with: "3DSRedirectSuccess") else {
            XCTFail("Should have AdyenPayload here")
            return
        }
        let parameters = worker.parameters(with: MockAdyRequestParameterers(valid: false), and: payload)
        XCTAssertTrue(parameters.isEmpty)
    }
    
    func testParametersAdyenSuccessValidPublicKey() {
        let worker = AdyenWorker()
        guard let payload = loadFromMock(with: "3DSRedirectSuccess") else {
            XCTFail("Should have AdyenPayload here")
            return
        }
        let parameters = worker.parameters(with: MockAdyRequestParameterers(), and: payload)
        let aquirerData = parameters["acquirer_data"] as! [String: Any]
        let threeDS2RequestData = aquirerData["threeDS2RequestData"] as! [String: Any]
        let sdkEphemKey = threeDS2RequestData["sdkEphemPubKey"] as! [String: String]
        XCTAssertEqual(sdkEphemKey["x"], "V1UePQ-499vtjF12iBgX49ugHYlsXcEATiynxqux2JA")
        XCTAssertEqual(sdkEphemKey["y"], "jd12h05TbomQwprYkZjZ1MbsaE4a9iQMSdCBScGkG_8")
        XCTAssertEqual(sdkEphemKey["crv"], "P-256")
        XCTAssertEqual(sdkEphemKey["kty"], "EC")
        
        XCTAssertEqual(threeDS2RequestData["sdkAppID"] as? String, "sdkApplicationIdentifier")
        XCTAssertEqual(threeDS2RequestData["sdkEncData"] as? String, "iPhoneXS")
        XCTAssertEqual(threeDS2RequestData["sdkReferenceNumber"] as? String, "sdkReferenceNumber")
        XCTAssertEqual(threeDS2RequestData["sdkTransID"] as? String, "981z27a398s129-981w27c398s129")
        XCTAssertEqual(aquirerData["threeDS2Token"] as? String, token)
        
        let transaction = parameters["transaction"] as! [String: String]
        XCTAssertEqual(transaction["id"], "1234")
        XCTAssertEqual(transaction["type"], "p2p")
        
    }
    
    func testParametersChallengeComplete() {
        let worker = AdyenWorker()
        guard let payload = loadFromMock(with: "3DSRedirectSuccess") else {
            XCTFail("Should have AdyenPayload here")
            return
        }
        let parameters = worker.parameters(with: MockAdyRequestParameterers(), and: payload)
        let challenge = worker.completeParameters(with: parameters)
        
        let transaction = challenge["transaction"] as! [String: Any]
        let acquirer_data = challenge["acquirer_data"] as! [String: Any]
        let threeDS2Result = acquirer_data["threeDS2Result"] as! [String: Any]
        
        XCTAssertEqual(transaction["id"] as? String, "1234")
        XCTAssertEqual(transaction["type"] as? String, "p2p")
        XCTAssertEqual(acquirer_data["threeDS2Token"] as? String, token)
        XCTAssertEqual(threeDS2Result["transStatus"] as? String, "Y")
        
    }
    
    func testTransactionFailAdyen() {
        let worker = AdyenWorker()
        let expectation = XCTestExpectation(description: "Expecting to fail")
        
        worker.transaction(with: [:], hasChallenge: { _,_,_   in
        }, succeeded: { _ in
        }, error: { (error) in
            expectation.fulfill()
        }, injecting: MockAdyServiceWithError.self)
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    //TODO: check how to initiate a ADYTransaction to mock the success
    class MockAdyServiceWithError: ADYServiceProtocol {
        static func transaction(with parameters: ADYServiceParameters!, appearanceConfiguration: ADYAppearanceConfiguration?, completionHandler: ((ADYTransaction?, [ADYWarning]?, Error?) -> Void)!) {
            completionHandler(nil, nil, AdyenConstant.errorAdyTransactionInternal)
        }
    }
    
    func testTreatTransactionLegacyError() {
        let result = AdyenWorker().treatTransaction(legacyError(), id: "")
        switch result {
        case .error(let error):
            XCTAssertEqual(error.localizedDescription, "Erro ao concluir transação.")
        default:
            XCTFail()
        }

    }

    func testTreatTransactionError() {
        let result = AdyenWorker().treatTransaction(error(), id: "")
        switch result {
        case .error(let error):
            XCTAssertEqual(error.localizedDescription, "Transaction error")
        default:
            XCTFail()
        }
    }
    
    func testTreatTransactionSuccessWithoutChallenge() {
        let result = AdyenWorker().treatTransaction(receipt(), id: "")
        switch result {
        case .succeeded(let viewModel):
            XCTAssertNotNil(viewModel)
        default:
            XCTFail()
        }
    }
    
    func testTreatTransactionSuccessWithChallenge() {
        let result = AdyenWorker().treatTransaction(challengeToDo(), id: "")
        switch result {
        case .challenge(let payload):
            XCTAssertNotNil(payload)
        default:
            XCTFail()
        }
    }
    
    
    func challengeToDo() -> PicPayResult<BaseApiGenericResponse> {
        let json = MockJSON().load(resource: "3DSPerformChallengeWithChallengeSuccess")
        return PicPayResult.success(BaseApiGenericResponse(json: json)!)
    }
    
    func receipt() -> PicPayResult<BaseApiGenericResponse> {
        let json = MockJSON().load(resource: "3DSPerformChallengeWithoutChallengeSuccess")
        return PicPayResult.success(BaseApiGenericResponse(json: json)!)
    }
    
    func legacyError() -> PicPayResult<BaseApiGenericResponse> {
        let json = MockJSON().load(resource: "LegacyError")
        let error = PicPayError(apiJSON: json.toDictionary()!["Error"] as! NSDictionary)
        return PicPayResult.failure(error)
    }
    
    func error() -> PicPayResult<BaseApiGenericResponse> {
        return PicPayResult.failure(PicPayError(message: "Transaction error"))
    }
}
