@testable import PicPay

class ApiPaymentMethodsMock: WalletApiProtocol {
    enum Status {
        case success
        case failure
    }
    var status: Status
    
    init(status: Status = .success) {
        self.status = status
    }
    
    func cardsList(onCacheLoaded: @escaping ((PaymentMethodsListResponse) -> Void), completion: @escaping ((PicPayResult<PaymentMethodsListResponse>) -> Void)) {
        switch status {
        case .success:
            let cardListResponse = try! MockCodable<PaymentMethodsListResponse>().loadCodableObject(resource: "cardList")
            let result = PicPayResult.success(cardListResponse)
            completion(result)
        case .failure:
            let result = PicPayResult<PaymentMethodsListResponse>.failure(MockError.genericError)
            completion(result)
        }
    }
    
    func cardDelete(card: String, _ completion: @escaping ((PicPayResult<BaseCodableEmptyResponse>) -> Void)) {
        switch status {
        case .success:
            let result = PicPayResult.success(BaseCodableEmptyResponse())
            completion(result)
        case .failure:
            let result = PicPayResult<BaseCodableEmptyResponse>.failure(MockError.genericError)
            completion(result)
        }
    }
    
    func cardEdit(card: String, alias: String?, address: ConsumerAddressItem?, _ completion: @escaping ((PicPayResult<BaseCodableEmptyResponse>) -> Void)) {
        switch status {
        case .success:
            let result = PicPayResult.success(BaseCodableEmptyResponse())
            completion(result)
        case .failure:
            let result = PicPayResult<BaseCodableEmptyResponse>.failure(MockError.genericError)
            completion(result)
        }
    }
}
