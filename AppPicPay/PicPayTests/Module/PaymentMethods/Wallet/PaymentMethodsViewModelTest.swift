import XCTest
@testable import PicPay
import Statement
import Core
import FeatureFlag

private final class StatementEnabledServiceMock: StatementEnabledServicing {
    var result: Result<Bool, ApiError>?
    
    func isStatementEnabled(completion: @escaping (Result<Bool, ApiError>) -> Void) {
        guard let result = result else {
            XCTFail("Result not defined")
            return
        }
        
        completion(result)
    }
}

final class PaymentMethodsViewModelTest: XCTestCase {
    var viewModel: PaymentMethodsViewModel!
    var api: ApiPaymentMethodsMock!
    private var statementEnabledService: StatementEnabledServiceMock!
    var featureManagerMock = FeatureManagerMock()
    
    override func setUp() {
        super.setUp()
        api = ApiPaymentMethodsMock()
        statementEnabledService = StatementEnabledServiceMock()
        
        viewModel = PaymentMethodsViewModel(
            apiPaymentMethod: api,
            statementEnabledService: statementEnabledService,
            consumerApiService: ConsumerApi(),
            dependencies: DependencyContainerMock(featureManagerMock)
        )
    }
    
    func testIsStatementEnabled_WhenFlagExperimentWalletHeaderLayoutV2PresentingBoolIsFalse_ShouldIsStatementEnabledEqualFalse() {
        featureManagerMock.override(key: .experimentWalletHeaderLayoutV2PresentingBool, with: false)
        
        let expectation = XCTestExpectation(description: "isStatementEnabled")
        
        viewModel.isStatementEnabled { isStatementEnabled in
            XCTAssertFalse(isStatementEnabled)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 5)
    }
    
    func testIsStatementEnabled_WhenFlagExperimentWalletHeaderLayoutV2PresentingBoolIsTrueAndResultIsSuccessAndTrue_ShouldIsStatementEnabledEqualTrue() {
        featureManagerMock.override(key: .experimentWalletHeaderLayoutV2PresentingBool, with: true)
        statementEnabledService.result = .success(true)
        
        let expectation = XCTestExpectation(description: "isStatementEnabled")
        
        viewModel.isStatementEnabled { isStatementEnabled in
            XCTAssertTrue(isStatementEnabled)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 5)
    }
    
    func testIsStatementEnabled_WhenFlagExperimentWalletHeaderLayoutV2PresentingBoolIsTrueAndResultIsSuccessAndFalse_ShouldIsStatementEnabledEqualFalse() {
        featureManagerMock.override(key: .experimentWalletHeaderLayoutV2PresentingBool, with: true)
        statementEnabledService.result = .success(false)
        
        let expectation = XCTestExpectation(description: "isStatementEnabled")
        
        viewModel.isStatementEnabled { isStatementEnabled in
            XCTAssertFalse(isStatementEnabled)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 5)
    }
    
    func testIsStatementEnabled_WhenFlagExperimentWalletHeaderLayoutV2PresentingBoolIsTrueAndResultIsFailure_ShouldIsStatementEnabledEqualFalse() {
        featureManagerMock.override(key: .experimentWalletHeaderLayoutV2PresentingBool, with: true)
        statementEnabledService.result = .failure(.bodyNotFound)
        
        let expectation = XCTestExpectation(description: "isStatementEnabled")
        
        viewModel.isStatementEnabled { isStatementEnabled in
            XCTAssertFalse(isStatementEnabled)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 5)
    }
    
    func testCardsListSuccess() {
        api.status = .success
        let expectation = XCTestExpectation(description: "cardsList")
        viewModel.cardsList(cache: {
        }) { error in
            XCTAssertNil(error)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
        
        let card = viewModel.card(at: IndexPath(item: 2, section: 0))
        XCTAssertEqual(card.alias, "meu cartao")
        XCTAssertEqual(card.expired, false)
        
        let address = card.address!
        XCTAssertEqual(address.neighborhood, "Morada de Laranjeiras")
        XCTAssertEqual(address.city, "Serra")
        XCTAssertEqual(address.addressName, nil)
        XCTAssertEqual(address.addressComplement, "")
        XCTAssertEqual(address.state, "ES")
        XCTAssertEqual(address.streetName, "R dos Morangos")
        XCTAssertEqual(address.zipCode, "29166830")
        XCTAssertEqual(address.country, "BR")
        XCTAssertEqual(address.streetNumber, "44")
        XCTAssertEqual(card.type.rawValue, "credit_card")
        XCTAssertEqual(card.lastDigits, "1212")
        XCTAssertEqual(card.image, "https://cdn.picpay.com/apps/picpay/imgs/visa.png")
        XCTAssertEqual(card.id, "419250")
        XCTAssertEqual(card.description, "Cartão com final 1212")
        XCTAssertEqual(card.creditCardBandeiraId, "3")
        XCTAssertEqual(card.verifyStatus, .waitingFillValue)
    }
    
    func testCardsListFailure() {
        api.status = .failure
        let expectation = XCTestExpectation(description: "cardList")
        viewModel.cardsList(cache: {
        }) { error in
            XCTAssertNotNil(error)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testCardDeleteSuccess() {
        api.status = .success
        let expectationList = XCTestExpectation(description: "cardList")
        viewModel.cardsList(cache: {
        }) { error in
            XCTAssertNil(error)
            expectationList.fulfill()
        }
        wait(for: [expectationList], timeout: 5)
   
        let expectationDelete = XCTestExpectation(description: "cardDelete")
        viewModel.cardDelete(cardId: "419250") { error in
            XCTAssertNil(error)
            expectationDelete.fulfill()
        }
        wait(for: [expectationDelete], timeout: 5)
    }
    
    func testeShowPromotionalCodeTrue() {
        viewModel.flowContext = .payment
        let result = viewModel.showPromotionalCode
        XCTAssertFalse(result)
    }
    
    func testeShowPromotionalCodeFalse() {
        viewModel.flowContext = .wallet
        let result = viewModel.showPromotionalCode
        XCTAssertTrue(result)
    }
    
    func testCardDeleteFailure() {
        api.status = .success
        let expectationList = XCTestExpectation(description: "cardList")
        viewModel.cardsList(cache: {
        }) { error in
            XCTAssertNil(error)
            expectationList.fulfill()
        }
        wait(for: [expectationList], timeout: 5)

        api.status = .failure
        let expectationDelete = XCTestExpectation(description: "cardDelete")
        viewModel.cardDelete(cardId: "000000") { error in
            XCTAssertNotNil(error)
            expectationDelete.fulfill()
        }
        wait(for: [expectationDelete], timeout: 5)
    }
    
    func testDeleteCardBankSuccess() {
        api.status = .success
        let expectationList = XCTestExpectation(description: "cardList")
        viewModel.cardsList(cache: {
        }) { error in
            XCTAssertNil(error)
            expectationList.fulfill()
        }
        wait(for: [expectationList], timeout: 5)
        
        let card = try! MockCodable<CardBank>().loadCodableObject(resource: "card")
        
        let expectationDelete = XCTestExpectation(description: "cardDelete")
        viewModel.deleteCard(card, addedCardToDelete: {}, completion: { error in
            XCTAssertNil(error)
            expectationDelete.fulfill()
        })
        wait(for: [expectationDelete], timeout: 5)
    }
    
    func testDeleteCardBankFailure() {
        api.status = .success
        let expectationList = XCTestExpectation(description: "cardList")
        viewModel.cardsList(cache: {
        }) { error in
            XCTAssertNil(error)
            expectationList.fulfill()
        }
        wait(for: [expectationList], timeout: 5)
        
        let card = try! MockCodable<CardBank>().loadCodableObject(resource: "card")
        
        api.status = .failure
        let expectationDelete = XCTestExpectation(description: "cardDelete")
        viewModel.deleteCard(card, addedCardToDelete: {}, completion: { error in
            XCTAssertNotNil(error)
            expectationDelete.fulfill()
        })
        wait(for: [expectationDelete], timeout: 5)
    }
    
    
    func test_isVirtualCardAvailable_shouldBeFalseWhenValueIsNil() {
        viewModel.accountOfCreditPicpay = nil
        
        XCTAssertFalse(viewModel.isVirtualCardAvailable)
    }
}
