import XCTest
@testable import PicPay

class EditCardAliasViewModelTest: XCTestCase {
    var viewModel: EditCardAliasViewModel!
    var api: ApiPaymentMethodsMock!
    var card: CardBank!
    
    override func setUp() {
        super.setUp()
        api = ApiPaymentMethodsMock()
        card = try! MockCodable<CardBank>().loadCodableObject(resource: "card")
        viewModel = EditCardAliasViewModel(with: card, api: api)
    }
    
    func testCardEditSuccess() {
        api.status = .success
        let newAlias = "abc"
        let expectation = XCTestExpectation(description: "cardEdit")
        viewModel.cardEdit(newAlias: newAlias, completion: { error in
            XCTAssertNil(error)
            XCTAssertEqual(self.viewModel.card.alias, newAlias)
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 5)
    }
    
    func testCardEditFailure() {
        api.status = .failure
        let newAlias = "abc"
        let expectation = XCTestExpectation(description: "cardEdit")
        viewModel.cardEdit(newAlias: newAlias, completion: { error in
            XCTAssertNotNil(error)
            XCTAssertNotEqual(self.viewModel.card.alias, newAlias)
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 5)
    }
}
