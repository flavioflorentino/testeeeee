import XCTest
@testable import PicPay

class AddNewCreditCardViewControllerTest: XCTestCase {
    
    let viewModel = AddNewCreditCardViewModel(origin: .none)
    
    override func setUp() {
        super.setUp()
        ConsumerManager.shared.consumer?.balance = 0.00
    }

    func testSanitizedCardName() {
        let validCharacters = "ABCDEFGHIJKLMNOPQRSTUVXZWYK abcdefghijklmnopqrstuvxzwyk"
        let validCharactersResponse = "ABCDEFGHIJKLMNOPQRSTUVXZWYK abcdefghijklmnopqrstuvxzwyk"
        
        let replaceCharactersUpper = "ABC123456789 DEFGH!@#$%ˆ&*()_-+=`~ IJKLMNOPQRSTUVXZWY :;?\\/><.,'\""
        let replaceCharactersDown = "abc123456789 defgh!@#$%ˆ&*()_-+=`~ ijklmnopqrstuvxzwy :;?\\/><.,'\""
        let replaceCharactersUpperResponse = "ABC DEFGH IJKLMNOPQRSTUVXZWY "
        let replaceCharactersDownResponse = "abc defgh ijklmnopqrstuvxzwy "
       
        var responseTeste = viewModel.sanitizedCardName(validCharacters)
        XCTAssert(responseTeste == validCharactersResponse)
        
        responseTeste = viewModel.sanitizedCardName(replaceCharactersUpper)
        XCTAssert(responseTeste == replaceCharactersUpperResponse)
        
        responseTeste = viewModel.sanitizedCardName(replaceCharactersDown)
        XCTAssert(responseTeste == replaceCharactersDownResponse)
    }
}
