import XCTest
import Core
import PCI
@testable import PicPay

fileprivate enum ValidateResult {
    case error
    case validateAskCpf
    case validateDontAskCpf
}

fileprivate enum InsertResult {
    case error
    case insertCreditCard
    case insertDebitCard
}

private final class MockCardRegistrationService: CardRegistrationServicing {
    private let insertResult: InsertResult
    private let validateResult: ValidateResult

    init(
        insertResult: InsertResult = .insertCreditCard,
        validateResult: ValidateResult = .validateAskCpf
    ) {
        self.insertResult = insertResult
        self.validateResult = validateResult
    }

    func validate(card: CardPayload, completion: @escaping CardValidationCompletion) {
        switch validateResult {
        case .validateDontAskCpf:
            let model = try! MockDecodable<PCI.ValidateCreditCard>().loadCodableObject(resource: "ValidateCreditCardDontAskCpf", typeDecoder: .convertFromSnakeCase)
            completion(.success(model))
        case .validateAskCpf:
            let model = try! MockDecodable<PCI.ValidateCreditCard>().loadCodableObject(resource: "ValidateCreditCardAskCpf", typeDecoder: .convertFromSnakeCase)
            completion(.success(model))
        case .error:
            var error = RequestError()
            error.message = "Error validateResult"
            completion(.failure(ApiError.notFound(body: error)))
        }
    }

    func save(card: CardPayload, completion: @escaping CardRegistrationCompletion) {
        switch insertResult {
        case .insertCreditCard:
            let model = try! MockDecodable<CreditCard>().loadCodableObject(resource: "InsertCreditCard", typeDecoder: .convertFromSnakeCase)
            completion(.success(model))
        case .insertDebitCard:
            let model = try! MockDecodable<CreditCard>().loadCodableObject(resource: "InsertDebitCard", typeDecoder: .convertFromSnakeCase)
            completion(.success(model))
        case .error:
            var error = RequestError()
            error.message = "Error insertCreditCard"
            completion(.failure(ApiError.notFound(body: error)))
        }
    }
}

private final class MockAddNewCreditCardService: AddNewCreditCardProtocol {
    private let environmentEnabled: Bool
    private let insertResult: InsertResult
    private let validateResult: ValidateResult

    init(
        environmentEnabled: Bool = true,
        insertResult: InsertResult = .insertCreditCard,
        validateResult: ValidateResult = .validateAskCpf
    ) {
        self.environmentEnabled = environmentEnabled
        self.insertResult = insertResult
        self.validateResult = validateResult
    }

    var newEnvironmentEnabled: Bool {
        return environmentEnabled
    }

    func insertCreditCard(addressCard: AddressCard, insertCard: InsertCard, completion: @escaping (PicPayResult<CardBank>) -> Void) {
        switch insertResult {
        case .insertCreditCard:
            let model = try! MockDecodable<CardBank>().loadCodableObject(resource: "InsertCreditCard", typeDecoder: .convertFromSnakeCase)
            completion(.success(model))
        case .insertDebitCard:
            let model = try! MockDecodable<CardBank>().loadCodableObject(resource: "InsertDebitCard", typeDecoder: .convertFromSnakeCase)
            completion(.success(model))
        case .error:
            let error = PicPayError(message: "Error insertCreditCard")
            completion(.failure(error))
        }
    }

    func validateCreditCard(validateCard: ValidateCard, completion: @escaping (PicPayResult<PicPay.ValidateCreditCard>) -> Void) {
        switch validateResult {
        case .validateDontAskCpf:
            let model = try! MockDecodable<PicPay.ValidateCreditCard>().loadCodableObject(resource: "ValidateCreditCardDontAskCpf", typeDecoder: .convertFromSnakeCase)
            completion(.success(model))
        case .validateAskCpf:
            let model = try! MockDecodable<PicPay.ValidateCreditCard>().loadCodableObject(resource: "ValidateCreditCardAskCpf", typeDecoder: .convertFromSnakeCase)
            completion(.success(model))
        case .error:
            let error = PicPayError(message: "Error validateCreditCard")
            completion(.failure(error))
        }
    }
}

final class AddNewCreditCardViewModelTest: XCTestCase {
    private lazy var logDetectionServiceSpy = LogDetectionServiceSpy()

    func testValidateCreditCardShouldReturnAskCpfWhenEnvironmentEnabledFalse() {
        ConsumerManager.shared.consumer?.balance = 0.00
        let service = MockAddNewCreditCardService(environmentEnabled: false, validateResult: .validateAskCpf)
        let viewModel = AddNewCreditCardViewModel(origin: .none, api: service)
        viewModel.consumerAddressCard = ConsumerAddressItem(name: "Alegre")

        let expectation = XCTestExpectation(description: "ValidateCreditCardAskCpf")
        viewModel.validateCredicard { (error, askCpf) in
            XCTAssertNil(error)
            XCTAssertNotNil(askCpf)
            XCTAssertTrue(askCpf!)
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 5)
    }

    func testValidateCreditCardShouldReturnDontAskCpfWhenEnvironmentEnabledFalse() {
        ConsumerManager.shared.consumer?.balance = 0.00
        let service = MockAddNewCreditCardService(environmentEnabled: false, validateResult: .validateDontAskCpf)
        let viewModel = AddNewCreditCardViewModel(origin: .none, api: service)
        viewModel.consumerAddressCard = ConsumerAddressItem(name: "Alegre")

        let expectation = XCTestExpectation(description: "ValidateCreditCardDontAskCpf")
        viewModel.validateCredicard { (error, askCpf) in
            XCTAssertNil(error)
            XCTAssertNotNil(askCpf)
            XCTAssertFalse(askCpf!)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }

    func testValidateCreditCardShouldReturnErrorWhenEnvironmentEnabledFalse() {
        ConsumerManager.shared.consumer?.balance = 0.00
        let service = MockAddNewCreditCardService(environmentEnabled: false, validateResult: .error)
        let viewModel = AddNewCreditCardViewModel(origin: .none, api: service)
        viewModel.consumerAddressCard = ConsumerAddressItem(name: "Alegre")

        let expectation = XCTestExpectation(description: "ValidateCreditCardError")
        viewModel.validateCredicard { (error, askCpf) in
            XCTAssertNotNil(error)
            XCTAssertNil(askCpf)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }

    func testInsertCreditCardShouldReturnSuccessWhenEnvironmentEnabledFalse() {
        ConsumerManager.shared.consumer?.balance = 0.00
        let service = MockAddNewCreditCardService(environmentEnabled: false, insertResult: .insertCreditCard)
        let viewModel = AddNewCreditCardViewModel(origin: .none, api: service)
        viewModel.consumerAddressCard = ConsumerAddressItem(name: "Alegre")

        let expectation = XCTestExpectation(description: "InsertCreditCardSuccess")
        viewModel.saveCard(creditSuccess: {
            XCTAssertEqual(CreditCardManager.shared.defaultCreditCardId, 419503)
            expectation.fulfill()
        }, debitSuccess: {
            XCTFail()
        }, onError: { _ in
            XCTFail()
        })

        wait(for: [expectation], timeout: 5)
    }

    func testInsertCreditCardShouldReturnErrorWhenEnvironmentEnabledFalse() {
        ConsumerManager.shared.consumer?.balance = 0.00
        let service = MockAddNewCreditCardService(environmentEnabled: false, insertResult: .error)
        let viewModel = AddNewCreditCardViewModel(origin: .none, api: service)
        viewModel.consumerAddressCard = ConsumerAddressItem(name: "Alegre")

        let expectation = XCTestExpectation(description: "InsertCreditCardError")

        viewModel.saveCard(creditSuccess: {
            XCTFail()
        }, debitSuccess: {
            XCTFail()
        }, onError: { error in
            XCTAssertNotNil(error)
            expectation.fulfill()
        })

        wait(for: [expectation], timeout: 5)
    }

    func testInsertDebitCardShouldReturnFlowDebitSuccessWhenEnvironmentEnabledFalseAndOriginRecharge() {
        ConsumerManager.shared.consumer?.balance = 0.00
        let service = MockAddNewCreditCardService(environmentEnabled: false, insertResult: .insertDebitCard)
        let viewModel = AddNewCreditCardViewModel(origin: .recharge, api: service)
        viewModel.consumerAddressCard = ConsumerAddressItem(name: "Alegre")

        let expectation = XCTestExpectation(description: "InsertDebitCardFlow")
        viewModel.saveCard(creditSuccess: {
            XCTFail()
        }, debitSuccess: {
            expectation.fulfill()
        }, onError: { _ in
            XCTFail()
        })

        wait(for: [expectation], timeout: 5)
    }

    func testInsertCreditCardShouldReturnFlowCreditSuccessWhenEnvironmentEnabledFalse() {
        ConsumerManager.shared.consumer?.balance = 0.00
        let service = MockAddNewCreditCardService(environmentEnabled: false, insertResult: .insertDebitCard)
        let viewModel = AddNewCreditCardViewModel(origin: .none, api: service)
        viewModel.consumerAddressCard = ConsumerAddressItem(name: "Alegre")

        let expectation = XCTestExpectation(description: "InsertCreditCardFlow")
        viewModel.saveCard(creditSuccess: {
            expectation.fulfill()
        }, debitSuccess: {
            XCTFail()
        }, onError: { _ in
            XCTFail()
        })

        wait(for: [expectation], timeout: 5)
    }

    // With PCI

    func testValidateCreditCardShouldReturnAskCpfWhenEnvironmentEnabledTrue() {
        ConsumerManager.shared.consumer?.balance = 0.00
        let pci = MockCardRegistrationService(validateResult: .validateAskCpf)
        let service = MockAddNewCreditCardService(environmentEnabled: true)
        let viewModel = AddNewCreditCardViewModel(origin: .none, api: service, service: pci)
        viewModel.consumerAddressCard = ConsumerAddressItem(name: "Alegre")

        let expectation = XCTestExpectation(description: "ValidateCreditCardAskCpfTrue")
        viewModel.validateCredicard { (error, askCpf) in
            XCTAssertNil(error)
            XCTAssertNotNil(askCpf)
            XCTAssertTrue(askCpf!)
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 5)
    }

    func testValidateCreditCardShouldReturnDontAskCpfWhenEnvironmentEnabledTrue() {
        ConsumerManager.shared.consumer?.balance = 0.00
        let pci = MockCardRegistrationService(validateResult: .validateDontAskCpf)
        let service = MockAddNewCreditCardService(environmentEnabled: true)
        let viewModel = AddNewCreditCardViewModel(origin: .none, api: service, service: pci)
        viewModel.consumerAddressCard = ConsumerAddressItem(name: "Alegre")

        let expectation = XCTestExpectation(description: "ValidateCreditCardDontAskCpfTrue")
        viewModel.validateCredicard { (error, askCpf) in
            XCTAssertNil(error)
            XCTAssertNotNil(askCpf)
            XCTAssertFalse(askCpf!)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }

    func testValidateCreditCardShouldReturnErrorWhenEnvironmentEnabledTrue() {
        ConsumerManager.shared.consumer?.balance = 0.00
        let pci = MockCardRegistrationService(validateResult: .error)
        let service = MockAddNewCreditCardService(environmentEnabled: true)
        let viewModel = AddNewCreditCardViewModel(origin: .none, api: service, service: pci)
        viewModel.consumerAddressCard = ConsumerAddressItem(name: "Alegre")

        let expectation = XCTestExpectation(description: "ValidateCreditCardErrorTrue")
        viewModel.validateCredicard { (error, askCpf) in
            XCTAssertNotNil(error)
            XCTAssertNil(askCpf)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }

    func testInsertCreditCardShouldReturnSuccessWhenEnvironmentEnabledTrue() {
        ConsumerManager.shared.consumer?.balance = 0.00
        let pci = MockCardRegistrationService(insertResult: .insertCreditCard)
        let service = MockAddNewCreditCardService(environmentEnabled: true)
        let viewModel = AddNewCreditCardViewModel(origin: .none, api: service, service: pci)
        viewModel.consumerAddressCard = ConsumerAddressItem(name: "Alegre")

        let expectation = XCTestExpectation(description: "InsertCreditCardSuccessTrue")
        viewModel.saveCard(creditSuccess: {
            XCTAssertEqual(CreditCardManager.shared.defaultCreditCardId, 419503)
            expectation.fulfill()
        }, debitSuccess: {
            XCTFail()
        }, onError: { _ in
            XCTFail()
        })

        wait(for: [expectation], timeout: 5)
    }

    func testInsertCreditCardShouldReturnErrorWhenEnvironmentEnabledTrue() {
        ConsumerManager.shared.consumer?.balance = 0.00
        let pci = MockCardRegistrationService(insertResult: .error)
        let service = MockAddNewCreditCardService(environmentEnabled: true)
        let viewModel = AddNewCreditCardViewModel(origin: .none, api: service, service: pci)
        let expectation = XCTestExpectation(description: "InsertCreditCardErrorTrue")
        viewModel.consumerAddressCard = ConsumerAddressItem(name: "Alegre")

        viewModel.saveCard(creditSuccess: {
            XCTFail()
        }, debitSuccess: {
            XCTFail()
        }, onError: { error in
            XCTAssertNotNil(error)
            expectation.fulfill()
        })

        wait(for: [expectation], timeout: 5)
    }

    func testInsertDebitCardShouldReturnFlowDebitSuccessWhenEnvironmentEnabledTrueAndOriginRecharge() {
        ConsumerManager.shared.consumer?.balance = 0.00
        let pci = MockCardRegistrationService(insertResult: .insertDebitCard)
        let service = MockAddNewCreditCardService(environmentEnabled: true)
        let viewModel = AddNewCreditCardViewModel(origin: .recharge, api: service, service: pci)
        viewModel.consumerAddressCard = ConsumerAddressItem(name: "Alegre")

        let expectation = XCTestExpectation(description: "InsertDebitCardFlowTrue")
        viewModel.saveCard(creditSuccess: {
            XCTFail()
        }, debitSuccess: {
            expectation.fulfill()
        }, onError: { _ in
            XCTFail()
        })

        wait(for: [expectation], timeout: 5)
    }

    func testInsertCreditCardShouldReturnFlowCreditSuccessWhenEnvironmentEnabledTrue() {
        ConsumerManager.shared.consumer?.balance = 0.00
        let pci = MockCardRegistrationService(insertResult: .insertDebitCard)
        let service = MockAddNewCreditCardService(environmentEnabled: true)
        let viewModel = AddNewCreditCardViewModel(origin: .none, api: service, service: pci)
        viewModel.consumerAddressCard = ConsumerAddressItem(name: "Alegre")

        let expectation = XCTestExpectation(description: "InsertCreditCardFlowTrue")
        viewModel.saveCard(creditSuccess: {
            expectation.fulfill()
        }, debitSuccess: {
            XCTFail()
        }, onError: { _ in
            XCTFail()
        })

        wait(for: [expectation], timeout: 5)
    }
    
    func testSendLogDetection_WhenCalled_ShouldSendLog() {
        let service = MockAddNewCreditCardService(environmentEnabled: false, validateResult: .validateAskCpf)
        let viewModel = AddNewCreditCardViewModel(origin: .none, api: service, logDetectionService: logDetectionServiceSpy)
        viewModel.sendLogDetection()
        XCTAssertEqual(logDetectionServiceSpy.sendLogCallCount, 1)
    }
}
