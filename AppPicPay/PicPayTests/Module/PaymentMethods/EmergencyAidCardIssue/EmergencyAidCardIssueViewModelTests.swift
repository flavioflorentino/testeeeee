import XCTest
@testable import PicPay

final class EmergencyAidCardIssuePresenterSpy: EmergencyAidCardIssuePresenting {
    private(set) var action: EmergencyAidCardIssueAction? = nil
    private(set) var didNextStepCallsCount = 0
    var viewController: EmergencyAidCardIssueDisplay?

    func didNextStep(action: EmergencyAidCardIssueAction) {
        self.action = action
        didNextStepCallsCount += 1
    }
}

final class EmergencyAidCardIssueViewModelTests: XCTestCase {
    private let presenterSpy = EmergencyAidCardIssuePresenterSpy()
    private lazy var sut = EmergencyAidCardIssueViewModel(presenter: presenterSpy)

    func testTryAgain_ShouldCallPresenterWithActionClose() {
        let expectedAction = EmergencyAidCardIssueAction.close
        let expectedNumberOfCalls = 1

        sut.tryAgain()

        XCTAssertEqual(expectedAction, presenterSpy.action)
        XCTAssertEqual(expectedNumberOfCalls, presenterSpy.didNextStepCallsCount)
    }

    func testOpenHelpCenter_ShouldCallPresenterWithActionClose() throws {
        let urlString = "picpay://picpay/helpcenter?query=preciso-de-ajuda-debito-caixa"
        let expectedURL = try URL(string: urlString).safe()
        let expectedAction = EmergencyAidCardIssueAction.openHelpCenter(url: expectedURL)
        let expectedNumberOfCalls = 1

        sut.openHelpCenter()

        XCTAssertEqual(expectedAction, presenterSpy.action)
        XCTAssertEqual(expectedNumberOfCalls, presenterSpy.didNextStepCallsCount)
    }
}
