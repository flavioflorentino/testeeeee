import Foundation
@testable import SecurityModule

final class LogDetectionServiceSpy: LogDetectionServicing {
    private (set) var sendLogCallCount = 0
    
    func sendLog(log: LogInfoContract) {
        sendLogCallCount += 1
    }
}
