@testable import PicPay

class MockError {
    static let genericError = PicPayError(message: "Erro", code: "0", title: nil, dictionary: nil)
    static let connectionError = PicPayError(message: "Sem conexão com a internet", code: "22", title: nil, dictionary: nil)
}
