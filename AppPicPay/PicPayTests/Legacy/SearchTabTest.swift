import XCTest
import Core
import FeatureFlag
import SwiftyJSON
@testable import PicPay

final class SearchTabTest: XCTestCase {
    let searchTabValidJson = """
{
   "tabs":[
      {
         "name":"Serviços",
         "id":"main",
         "group":"MAIN",
         "ask_permission":true,
         "auto_refresh":true
      },
      {
         "name":"Locais",
         "id":"places",
         "group":"PLACES",
         "ask_permission":true,
         "auto_refresh":true
      },
      {
         "name":"Store",
         "id":"store",
         "group":"STORE",
         "ask_permission":false,
         "auto_refresh":false
      }
   ]
}
"""
    
        let searchTabWrongDictionaryKeyValidJson = """
    {
       "tab":[
          {
             "name":"Serviços",
             "id":"main",
             "group":"MAIN",
             "ask_permission":true,
             "auto_refresh":true
          },
          {
             "name":"Locais",
             "id":"places",
             "group":"PLACES",
             "ask_permission":true,
             "auto_refresh":true
          },
          {
             "name":"Store",
             "id":"store",
             "group":"STORE",
             "ask_permission":false,
             "auto_refresh":false
          }
       ]
    }
    """
    
        let searchTabInvalidJson = """
    kjasdkjlasdkjnaskjdn
    """
    
        let searchTabJsonWithEmptyValues = """
    {
       "tabs":[
          {
             "name":"",
             "id":"",
             "group":"",
             "ask_permission":null,
             "auto_refresh":null
          },
          {
             "name":"",
             "id":"",
             "group":null,
             "ask_permission":null,
             "auto_refresh":null
          },
          {
             "name":null,
             "id":null,
             "group":null,
             "ask_permission":null,
             "auto_refresh":null
          }
       ]
    }
    """
    
    let searchTabJsonWithLowercasedName = """
    {
       "tabs":[
          {
             "name":"serviços",
             "id":"main",
             "group":"MAIN",
             "ask_permission":true,
             "auto_refresh":true
          },
          {
             "name":"locais",
             "id":"places",
             "group":"PLACES",
             "ask_permission":true,
             "auto_refresh":true
          },
          {
             "name":"store",
             "id":"store",
             "group":"STORE",
             "ask_permission":false,
             "auto_refresh":false
          }
       ]
    }
    """
    
    func testParseSuccess_WithValidJSON_ShouldSucceed() {
        // Given
        let validJson = searchTabValidJson
        
        // When
        var tabs: [SearchTab]
        
        tabs = SearchTab.parse(json: validJson)
        
        // Then
        assertTabCount(tabs: tabs)
        assertIfTabsAreValid(tabs: tabs)
    }
    
    func testFallback_WithInWrongDictionaryKey_ShouldFallbackToTogglesPlist() {
        // Given
        let inValidJson = searchTabWrongDictionaryKeyValidJson
        
        // When
        var tabs: [SearchTab]
        
        tabs = SearchTab.parse(json: inValidJson)
        
        // Then
        assertTabCount(tabs: tabs)
        assertIfTabsAreValid(tabs: tabs)
    }
    
    func testFallback_WithInvalidJSON_ShouldFallbackToTogglesPlist() {
        // Given
        let inValidJson = searchTabInvalidJson
        
        // When
        var tabs: [SearchTab]
        
        tabs = SearchTab.parse(json: inValidJson)
        
        // Then
        assertTabCount(tabs: tabs)
        assertIfTabsAreValid(tabs: tabs)
    }
    
    func testFallback_WithEmptyValuesJSON_ShouldFallbackToTogglesPlist() {
        // Given
        let inValidJson = searchTabJsonWithEmptyValues
        
        // When
        var tabs: [SearchTab]
        
        tabs = SearchTab.parse(json: inValidJson)
        
        // Then
        assertTabCount(tabs: tabs)
        assertIfTabsAreValid(tabs: tabs)
    }
    
    func testSuccess_WithLowercasedName_ShouldCapitalizeIt() {
        // Given
        let lowercasedNameJson = searchTabJsonWithLowercasedName
        
        // When
        var tabs: [SearchTab]
        
        tabs = SearchTab.parse(json: lowercasedNameJson)
        
        // Then
        assertTabCount(tabs: tabs)
        assertIfTabsAreValid(tabs: tabs)
    }
    
    func testInit_ShouldSucceed() {
        let tab = SearchTab(name: "name", group: "group", id: "id", shouldAskPermissions: true, autoRefresh: true)
        XCTAssertEqual(tab.name, "name")
        XCTAssertEqual(tab.group, "group")
        XCTAssertEqual(tab.id, "id")
        XCTAssertEqual(tab.shouldAskPermissions, true)
        XCTAssertEqual(tab.autoRefresh, true)
    }
    
    func testToJson_WithSearchTab_ShouldSucceed() {
        let tab = SearchTab(name: "name", group: "group", id: "id", shouldAskPermissions: true, autoRefresh: true)
        let json = tab.toJson()
        XCTAssertEqual(json["name"].string, "name")
        XCTAssertEqual(json["group"].string, "group")
        XCTAssertEqual(json["id"].string, "id")
        XCTAssertEqual(json["ask_permission"].bool, true)
        XCTAssertEqual(json["auto_refresh"].bool, true)
    }
    
    private func assertTabCount(tabs: [SearchTab]) {
        guard tabs.count == 3 else {
            XCTFail("Should have three tabs")
            return
        }
    }
    
    private func assertIfTabsAreValid(tabs: [SearchTab]) {
        XCTAssertEqual(tabs[0].name, "Serviços")
        XCTAssertEqual(tabs[0].id, "main")
        XCTAssertEqual(tabs[0].group, "MAIN")
        XCTAssertEqual(tabs[0].shouldAskPermissions, true)
        XCTAssertEqual(tabs[0].autoRefresh, true)
        
        XCTAssertEqual(tabs[1].name, "Locais")
        XCTAssertEqual(tabs[1].id, "places")
        XCTAssertEqual(tabs[1].group, "PLACES")
        XCTAssertEqual(tabs[1].shouldAskPermissions, true)
        XCTAssertEqual(tabs[1].autoRefresh, true)
        
        XCTAssertEqual(tabs[2].name, "Store")
        XCTAssertEqual(tabs[2].id, "store")
        XCTAssertEqual(tabs[2].group, "STORE")
        XCTAssertEqual(tabs[2].shouldAskPermissions, false)
        XCTAssertEqual(tabs[2].autoRefresh, false)
    }
}
