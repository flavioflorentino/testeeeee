@testable import PicPay
import XCTest

final class ScannerInfoCoordinatorTests: XCTestCase {
    private let controller = ViewControllerMock()
    private var navigationController: NavigationControllerMock?
    private lazy var coordinator: ScannerInfoCoordinator = {
        let coordinator = ScannerInfoCoordinator()
        coordinator.viewController = controller
        return coordinator
    }()
    
    // MARK: - Setup
    override func setUpWithError() throws {
        try super.setUpWithError()
        navigationController = NavigationControllerMock(rootViewController: controller)
    }
    
    // MARK: - perform
    func testPerform_WhenActionIsDeeplink_ShouldCallPushViewController() throws {
        let faqUrl = try XCTUnwrap(URL(string: "https://meajuda.picpay.com/hc/pt-br/articles/360051880771-Tudo-sobre-pagamentos-com-Pix"))
        coordinator.perform(action: .deeplink(url: faqUrl))
        XCTAssertEqual(navigationController?.isPushViewControllerCalled, true)
    }
    
    func testPerform_WhenActionIsClose_ShouldCallPopViewController() {
        coordinator.perform(action: .close)
        XCTAssertEqual(navigationController?.isPopViewControllerCalled, true)
    }
}
