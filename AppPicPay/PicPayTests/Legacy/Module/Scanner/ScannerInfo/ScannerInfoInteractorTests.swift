import AnalyticsModule
@testable import PicPay
import XCTest

private final class ScannerInfoPresenterSpy: ScannerInfoPresenting {
    var viewController: ScannerInfoDisplaying?
    private(set) var didNextStepCallsCount = 0
    private(set) var action: ScannerInfoAction?
    
    func didNextStep(action: ScannerInfoAction) {
        didNextStepCallsCount += 1
        self.action = action
    }
}

final class ScannerInfoInteractorTests: XCTestCase {
    private let analytics = AnalyticsSpy()
    private let presenter = ScannerInfoPresenterSpy()
    private lazy var interactor = ScannerInfoInteractor(
        presenter: presenter,
        origin: .sendQrCodeScreen,
        dependencies: DependencyContainerMock(analytics)
    )
    
    // MARK: - interactKnowMore
    func testInteractKnowMore_ShouldCallDidNextStepWithActionDeeplink() throws {
        interactor.interactKnowMore()
        let action = try XCTUnwrap(presenter.action)
        XCTAssertEqual(presenter.didNextStepCallsCount, 1)
        guard case ScannerInfoAction.deeplink(_) = action else {
            XCTFail("Wrong action type")
            return
        }
    }
    
    // MARK: - interactOk
    func testInteractOk_ShouldCallDidNextStepWithActionClose() throws {
        interactor.interactOk()
        let action = try XCTUnwrap(presenter.action)
        XCTAssertEqual(presenter.didNextStepCallsCount, 1)
        guard case ScannerInfoAction.close = action else {
            XCTFail("Wrong action type")
            return
        }
    }
}
