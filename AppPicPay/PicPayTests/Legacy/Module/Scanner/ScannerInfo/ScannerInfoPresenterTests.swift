@testable import PicPay
import XCTest

private final class ScannerInfoViewControllerSpy: ScannerInfoDisplaying {
}

private final class ScannerInfoCoordinatorSpy: ScannerInfoCoordinating {
    var viewController: UIViewController?
    private(set) var performCallsCount = 0
    private(set) var action: ScannerInfoAction?
    
    func perform(action: ScannerInfoAction) {
        performCallsCount += 1
        self.action = action
    }
}

final class ScannerInfoPresenterTests: XCTestCase {
    private let viewController = ScannerInfoViewControllerSpy()
    private let coordinator = ScannerInfoCoordinatorSpy()
    private lazy var presenter: ScannerInfoPresenter = {
        let presenter = ScannerInfoPresenter(coordinator: coordinator)
        presenter.viewController = viewController
        return presenter
    }()
    
    // MARK: - didNextStep
    func testDidNextStep_WhenActionIsDeeplink_ShouldCallPerformWithActionDeeplink() throws {
        let initialUrl = try XCTUnwrap(URL(string: "http://www.picpay.com"))
        let action = ScannerInfoAction.deeplink(url: initialUrl)
        presenter.didNextStep(action: action)
        XCTAssertEqual(coordinator.performCallsCount, 1)
        XCTAssertEqual(coordinator.action, action)
    }
    
    func testDidNextStep_WhenActionIsClose_ShouldCallPerformWithActionClose() {
        let action = ScannerInfoAction.close
        presenter.didNextStep(action: action)
        XCTAssertEqual(coordinator.performCallsCount, 1)
        XCTAssertEqual(coordinator.action, action)
    }
}
