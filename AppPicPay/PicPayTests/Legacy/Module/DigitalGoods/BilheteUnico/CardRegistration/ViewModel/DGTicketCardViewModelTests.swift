import XCTest
import SwiftyJSON
@testable import PicPay

private class DGTicketCardServiceMock: DGTicketCardServicing {
    
    var dgTicketCardrResult: Result<RegisterYourCityPlaceholder, PicPayError>?
    
    func confirmPermission(city: DGTicketCity, completion: @escaping (Result<RegisterYourCityPlaceholder, PicPayError>) -> Void) {
        guard let result = dgTicketCardrResult else {
           XCTFail("Expected result not defined")
            return
        }
        completion(result)
    }
}

private struct Seeds {
    static let picPayErrorInfosOnData: Data? = """
        {
            "code": 100,
            "short_message": "Sem cartão cadastrado",
            "message": "Antes de continuar você deve realizar um cadastro no site Giro, depois disso você já poderá utilizar seu cartão aqui no PicPay.",
            "data": {
                "confirmButtonTitle": "Ir para o cadastro",
                "dismissButtonTitle": "Cancelar",
                "redirectUrl": "https://www.metrorio.com.br/giro"
            }
        }
    """.data(using: .utf8)
    
    static let defaultPicPayError: Data? = """
          {
            "code":100,
            "short_message":"Erro!",
            "message":"Houve um erro na sua requisição."
          }
      """.data(using: .utf8)
    
    static let picPayErrorInfosWrongOnData: Data? = """
        {
            "code": 100,
            "short_message": "Sem cartão cadastrado",
            "message": "Antes de continuar você deve realizar um cadastro no site Giro, depois disso você já poderá utilizar seu cartão aqui no PicPay.",
            "data": {
                "confirmButtonTitleFoo": "Ir para o cadastro",
                "dismissButtonTitleBar": "Cancelar",
                "redirectUrl": "https://www.metrorio.com.br/giro"
            }
        }
    """.data(using: .utf8)
    
    static let successPlaceholder: Data? = """
        {
            "cardNumber": {
                "text": "5566775389326788",
                "isEditable": false
            },
            "cardName": {
                "isEditable": true
            }
        }
    """.data(using: .utf8)
}

final class DGTicketCardViewModelTests: XCTestCase {
    
    private lazy var serviceMock = DGTicketCardServiceMock()
    private lazy var sut = DGTicketCardViewModel(service: serviceMock)

    func testConfirmPermission_WhenErrorWithData_ShouldConfigureAlertButtonsAndMessage() throws {
        let data = try XCTUnwrap(Seeds.picPayErrorInfosOnData)
        let json = try JSON(data: data)
        let error = PicPayError(json: json)
        
        let expectedResult : Result<RegisterYourCityPlaceholder, PicPayError> = .failure(error)
        
        serviceMock.dgTicketCardrResult = expectedResult
        
        let expectedTitle = "Sem cartão cadastrado"
        let expectedMessage = "Antes de continuar você deve realizar um cadastro no site Giro, depois disso você já poderá utilizar seu cartão aqui no PicPay."
        
        sut.didFailOnConfirmPermission = { alert in
            XCTAssertEqual(expectedTitle, alert.title?.string)
            XCTAssertEqual(expectedMessage, alert.text?.string)
            XCTAssertEqual("Ir para o cadastro", alert.buttons[0].title)
            XCTAssertEqual("Cancelar", alert.buttons[1].title)
        }
        
        sut.confirmPermission()
    }
    
    func testConfirmPermission_WhenDefaultError_ShouldConfigureAlertButtonsAndMessage() throws {
        let data = try XCTUnwrap(Seeds.defaultPicPayError)
        let json = try JSON(data: data)
        let error = PicPayError(json: json)
        
        let expectedResult : Result<RegisterYourCityPlaceholder, PicPayError> = .failure(error)
        
        serviceMock.dgTicketCardrResult = expectedResult
        
        let expectedTitle = "Erro!"
        let expectedMessage = "Houve um erro na sua requisição."
        
        sut.didFailOnConfirmPermission = { alert in
            XCTAssertEqual(expectedTitle, alert.title?.string)
            XCTAssertEqual(expectedMessage, alert.text?.string)
        }
        
        sut.confirmPermission()
    }
    
    func testConfirmPermission_WhenErrorWithWrongData_ShouldConfigureAlertButtonsAndMessage() throws {
        let data = try XCTUnwrap(Seeds.picPayErrorInfosWrongOnData)
        let json = try JSON(data: data)
        let error = PicPayError(json: json)
        
        let expectedResult : Result<RegisterYourCityPlaceholder, PicPayError> = .failure(error)
        
        serviceMock.dgTicketCardrResult = expectedResult
        
        let expectedTitle = "Sem cartão cadastrado"
        let expectedMessage = "Antes de continuar você deve realizar um cadastro no site Giro, depois disso você já poderá utilizar seu cartão aqui no PicPay."
        
        sut.didFailOnConfirmPermission = { alert in
            XCTAssertEqual(expectedTitle, alert.title?.string)
            XCTAssertEqual(expectedMessage, alert.text?.string)
            XCTAssertEqual(DefaultLocalizable.btOkUnderstood.text, alert.buttons[0].title)
        }
        
        sut.confirmPermission()
    }
    
    func testConfirmPermission_WhenSuccess_ShouldCallConfirmPermissionWithPlaceholder() throws {
        let data = try XCTUnwrap(Seeds.successPlaceholder)
        let json = try JSON(data: data)
        let placeHolderResponse = try XCTUnwrap(RegisterYourCityPlaceholder(json: json))
        
        let expectedResult : Result<RegisterYourCityPlaceholder, PicPayError> = .success(placeHolderResponse)
        
        serviceMock.dgTicketCardrResult = expectedResult
        
        sut.didConfirmPermission = { placeHolder in
            XCTAssertEqual(placeHolder, placeHolderResponse)
        }
        
        sut.confirmPermission()
    }
}
