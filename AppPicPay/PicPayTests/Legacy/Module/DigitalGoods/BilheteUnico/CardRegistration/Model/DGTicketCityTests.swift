import XCTest
import SwiftyJSON
@testable import PicPay

private enum Seeds {
    static let defaultData = """
                            {
                            "seller_id" : "14364",
                            "url_info" : "",
                            "_id" : "5e3073b277a67600383b8442",
                            "description_short" : "Metrô Rio / RJ",
                            "name" : "Metrô Rio Boladão",
                            "city_id" : "123123",
                            "card_digits_between" : "",
                            "supplier_id" : "metrorio",
                            "url_logo" : "https://www.picpay.com/static/images/bullets/store.png",
                            "description" : "Recarregue seu ticket do Metrô Rio / RJ",
                            "created_at" : "2020-01-28 15:47:20",
                            "issuer_id" : "2",
                            "service_id" : "",
                            "supplier" : "metrorio",
                            "offline" : false,
                            "address" : "",
                            "url_banner" : "https://pngimage.net/wp-content/uploads/2018/06/picpay-png-3.png",
                            "receipt" : "",
                            "offline_message" : "",
                            "url_info_transaction" : "",
                            "lat" : "-22.9035",
                            "enabled" : true,
                            "updated_at" : "2020-02-27 20:32:30",
                            "surcharge" : 0,
                            "disclaimer" : "",
                            "lng" : "-43.2096"
                            }
                            """.data(using: .utf8)
    
    static let defaultWarningAlertData = """
                            {
                              "seller_id" : "14364",
                              "url_info" : "",
                              "_id" : "5e3073b277a67600383b8442",
                              "description_short" : "Metrô Rio / RJ",
                              "name" : "Metrô Rio Boladão",
                              "city_id" : "123123",
                              "card_digits_between" : "",
                              "supplier_id" : "metrorio",
                              "url_logo" : "https://www.picpay.com/static/images/bullets/store.png",
                              "description" : "Recarregue seu ticket do Metrô Rio / RJ",
                              "created_at" : "2020-01-28 15:47:20",
                              "issuer_id" : "2",
                              "service_id" : "",
                              "supplier" : "metrorio",
                              "offline" : false,
                              "address" : "",
                              "url_banner" : "https://pngimage.net/wp-content/uploads/2018/06/picpay-png-3.png",
                              "receipt" : "",
                              "offline_message" : "",
                              "url_info_transaction" : "",
                              "lat" : "-22.9035",
                              "enabled" : true,
                              "updated_at" : "2020-02-27 20:32:30",
                              "surcharge" : 0,
                              "disclaimer" : "",
                              "lng" : "-43.2096",
                              "warning_alert" : {
                                  "title": "Oi, precisamos da sua permissão para consultar seu cartão no site do Giro",
                                  "message": "Aceito compartilhar meus dados cadastrais com o Giro MetrôRio",
                                  "confirm_button_title": "Sim, Aceito",
                                  "cancel_button_title": "Cancelar"
                              }
                            }
                            """.data(using: .utf8)
}

class DGTicketCityTests: XCTestCase {
    var sut: DGTicketCity?
    
    func testInitJSON_WhemCalledWithDefaultJSON_ShouldInitialize() throws {
        
        let defaultData = try XCTUnwrap(Seeds.defaultData)
        let defaultJSON = try JSON(data: defaultData)
               
        sut = DGTicketCity(json: defaultJSON)
        XCTAssertNotNil(sut)
    }
    
    func testInitJSON_WhemCalledWithJSONWithWarningAlert_ShouldInitialize() throws {
        
        let defaultWarningAlertData = try XCTUnwrap(Seeds.defaultWarningAlertData)
        let defaultWarningAlertJSON = try JSON(data: defaultWarningAlertData)
        
        sut = DGTicketCity(json: defaultWarningAlertJSON)
        XCTAssertNotNil(sut)
        
    }
    
    func testInitJSON_WhemCalledWithJSONWithWarningAlert_ShouldSetRightProperties() throws {
        
        let defaultWarningAlertData = try XCTUnwrap(Seeds.defaultWarningAlertData)
        let defaultWarningAlertJSON = try JSON(data: defaultWarningAlertData)
        
        sut = DGTicketCity(json: defaultWarningAlertJSON)
        
        let expectedTitle = "Oi, precisamos da sua permissão para consultar seu cartão no site do Giro"
        let expectedMessage = "Aceito compartilhar meus dados cadastrais com o Giro MetrôRio"
        let expectedConfirmButtonTitle = "Sim, Aceito"
        let expectedCancelButtonTitle = "Cancelar"
        
        XCTAssertEqual(sut?.warningAlert?.title, expectedTitle)
        XCTAssertEqual(sut?.warningAlert?.message, expectedMessage)
        XCTAssertEqual(sut?.warningAlert?.confirmButtonTitle, expectedConfirmButtonTitle)
        XCTAssertEqual(sut?.warningAlert?.cancelButtonTitle, expectedCancelButtonTitle)
    }
    
}
