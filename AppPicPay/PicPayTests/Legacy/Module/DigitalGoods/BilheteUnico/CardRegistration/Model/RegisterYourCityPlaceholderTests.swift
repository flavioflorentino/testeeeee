import XCTest
import SwiftyJSON
@testable import PicPay

private enum Seeds {
    static let defaultData = """
                            {
                                "cardNumber": {
                                    "text": "5566775389326788",
                                    "isEditable": false
                                },
                                "cardName": {
                                    "isEditable": true
                                }
                            }
                            """.data(using: .utf8)
}

final class RegisterYourCityPlaceholderTests: XCTestCase {
    var sut: RegisterYourCityPlaceholder?
    
    func testInitJSON_WhenCalledWithDefaultJSON_ShouldInitialize() throws {
        let defaultData = try XCTUnwrap(Seeds.defaultData)
        let defaultJSON = try JSON(data: defaultData)
        
        sut = RegisterYourCityPlaceholder(json: defaultJSON)
        XCTAssertNotNil(sut)
    }
    
    func testInitJSON_WhenCalledWithDefaultJSON_ShouldSetRightProperties() throws {
        let defaultData = try XCTUnwrap(Seeds.defaultData)
        let defaultJSON = try JSON(data: defaultData)
              
        sut = RegisterYourCityPlaceholder(json: defaultJSON)
        
        let expectedTextCardNumber = "5566775389326788"
        let expectedIsEditableCardNumber = false
        
        let expectedTextCardName : String? = nil
        let expectedIsEditableCardName = true
        
        XCTAssertEqual(sut?.cardNumber?.text, expectedTextCardNumber)
        XCTAssertEqual(sut?.cardNumber?.isEditable, expectedIsEditableCardNumber)
        
        XCTAssertEqual(sut?.cardName?.text, expectedTextCardName)
        XCTAssertEqual(sut?.cardName?.isEditable, expectedIsEditableCardName)
        
    }
    
}
