import XCTest
import SwiftyJSON
@testable import PicPay

private enum Seeds {
    static let defaultData = """
                            {
                                "title": "Oi, precisamos da sua permissão para consultar seu cartão no site do Giro",
                                "message": "Aceito compartilhar meus dados cadastrais com o Giro MetrôRio",
                                "confirm_button_title": "Sim, Aceito",
                                "cancel_button_title": "Cancelar"
                            }
                            """.data(using: .utf8)
}

class DGTicketCityWarningAlertTests: XCTestCase {
    
    var sut: DGTicketCityWarningAlert?
    
    func testInitJSON_WhemCalledWithDefaultJSON_ShouldInitialize() throws {
        let defaultData = try XCTUnwrap(Seeds.defaultData)
        let defaultJSON = try JSON(data: defaultData)
        
        sut = DGTicketCityWarningAlert(json: defaultJSON)
        XCTAssertNotNil(sut)
    }

    
    func testInitJSON_WhemCalledWithDefaultJSON_ShouldSetRightProperties() throws {
        let defaultData = try XCTUnwrap(Seeds.defaultData)
        let defaultJSON = try JSON(data: defaultData)
        
        sut = DGTicketCityWarningAlert(json: defaultJSON)
        
        let expectedTitle = "Oi, precisamos da sua permissão para consultar seu cartão no site do Giro"
        let expectedMessage = "Aceito compartilhar meus dados cadastrais com o Giro MetrôRio"
        let expectedConfirmButtonTitle = "Sim, Aceito"
        let expectedCancelButtonTitle = "Cancelar"
        
        XCTAssertEqual(sut?.title, expectedTitle)
        XCTAssertEqual(sut?.message, expectedMessage)
        XCTAssertEqual(sut?.confirmButtonTitle, expectedConfirmButtonTitle)
        XCTAssertEqual(sut?.cancelButtonTitle, expectedCancelButtonTitle)
    }
}
