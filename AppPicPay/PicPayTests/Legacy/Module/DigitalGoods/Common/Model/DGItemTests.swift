import XCTest
@testable import PicPay

final class DGItemTests: XCTestCase {
    func testPouplateWithServiceData_ShouldPopulateCorrectFields() throws {
        let service = getServiceData()
        let digitalGood = DGItem(id: "1")
        digitalGood.populate(with: service)

        XCTAssertEqual(digitalGood.sellerId, "1234")
        XCTAssertEqual(digitalGood.bannerImgUrl, "bannerUrl")
        XCTAssertEqual(digitalGood.infoUrl, "infoUrl")
        XCTAssertEqual(digitalGood.longDescriptionMarkdown, "descLarge")
        XCTAssertEqual(digitalGood.disclaimerMarkdown, "disclaimer")
        XCTAssertEqual(digitalGood.displayType, .list)
    }

    func testPouplateWithServiceData_WhenSelectionTypeIsNil_ShouldNotChangeDisplayType() throws {
        let service = getServiceData(selectionType: nil)
        let digitalGood = DGItem(id: "1")
        digitalGood.populate(with: service)
        digitalGood.displayType = .grid

        XCTAssertEqual(digitalGood.sellerId, "1234")
        XCTAssertEqual(digitalGood.bannerImgUrl, "bannerUrl")
        XCTAssertEqual(digitalGood.infoUrl, "infoUrl")
        XCTAssertEqual(digitalGood.longDescriptionMarkdown, "descLarge")
        XCTAssertEqual(digitalGood.disclaimerMarkdown, "disclaimer")
        XCTAssertEqual(digitalGood.displayType, .grid)
    }

    private func getServiceData(selectionType: DGDisplayType? = .list) -> DGInfo {
        .init(sellerId: 1234,
              bannerUrl: "bannerUrl",
              infoUrl: "infoUrl",
              descriptionLargeMarkdown: "descLarge",
              disclaimerMarkdown: "disclaimer",
              selectionType: selectionType)
    }
}
