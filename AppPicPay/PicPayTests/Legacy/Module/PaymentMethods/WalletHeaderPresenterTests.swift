import Core
import FeatureFlag
@testable import PicPay
import XCTest

private class WalletHeaderDisplaySpy: WalletHeaderDisplay {
    // MARK: - DisplaySavings
    private(set) var displaySavingsCallsCount = 0
    private(set) var isBadgeHiden = false

    func displaySavings(isBadgeHiden: Bool) {
        displaySavingsCallsCount += 1
        self.isBadgeHiden = isBadgeHiden
    }

    // MARK: - HideSavings
    private(set) var hideSavingsCallsCount = 0

    func hideSavings() {
        hideSavingsCallsCount += 1
    }

    // MARK: - Display
    private(set) var displayBalanceDescriptionCallsCount = 0
    private(set) var balanceDescription: NSAttributedString?

    func display(balanceDescription: NSAttributedString) {
        displayBalanceDescriptionCallsCount += 1
        self.balanceDescription = balanceDescription
    }

    // MARK: - Display
    private(set) var displayBalanceCallsCount = 0
    private(set) var balance: String?

    func display(balance: String) {
        displayBalanceCallsCount += 1
        self.balance = balance
    }
}

final class WalletHeaderPresenterTest: XCTestCase {
    private let viewSpy = WalletHeaderDisplaySpy()
    private lazy var kvStoreMock = KVStoreMock()
    private lazy var featureManagerMock = FeatureManagerMock()
    private lazy var container = DependencyContainerMock(featureManagerMock, kvStoreMock)
    private let walletRemoteValues = WalletRemoteValues(
        incomeDescription: .init(
            noDeposit: "Seu saldo rende <b>210% do CDI</b>",
            withDeposit: "Seu saldo está rendendo <b>210% do CDI</b>"
        )
    )
    
    
    func testPresentSavingsIfNeeded_WhenSavingsIsNotActive_ShouldHideSavings() {
        let sut = WalletHeaderPresenter(dependencies: container, isSavingsActive: false)
        sut.view = viewSpy
        
        sut.presentSavingsIfNeeded(balance: NSDecimalNumber(integerLiteral: 100))
        
        XCTAssertEqual(viewSpy.hideSavingsCallsCount, 1)
    }
    
    func testPresentSavingsIfNeeded_WhenSavingsIsActiveAndDepositIsNotZero_ShouldDisplaySavingsAndSetDescription() {
        featureManagerMock.override(key: .walletConfigValues, with: walletRemoteValues)
        let sut = WalletHeaderPresenter(dependencies: container, isSavingsActive: true)
        sut.view = viewSpy
        
        sut.presentSavingsIfNeeded(balance: NSDecimalNumber(integerLiteral: 100))
        
        XCTAssertEqual(viewSpy.displaySavingsCallsCount, 1)
        XCTAssertFalse(viewSpy.isBadgeHiden)
        XCTAssertEqual(viewSpy.displayBalanceDescriptionCallsCount, 1)
        XCTAssertEqual(viewSpy.balanceDescription?.string, "Seu saldo está rendendo 210% do CDI")
    }
    
    func testPresentSavingsIfNeeded_WhenSavingsIsActiveAndUserSawItAndDepositIsNotZero_ShouldDisplaySavingsAndSetDescriptionAndHideBadge() {
        kvStoreMock.setBool(true, with: KVKey.savings_onboarding_saw)
        featureManagerMock.override(key: .walletConfigValues, with: walletRemoteValues)
        let sut = WalletHeaderPresenter(dependencies: container, isSavingsActive: true)
        sut.view = viewSpy
        
        sut.presentSavingsIfNeeded(balance: NSDecimalNumber(integerLiteral: 100))
        
        XCTAssertEqual(viewSpy.displaySavingsCallsCount, 1)
        XCTAssertTrue(viewSpy.isBadgeHiden)
        XCTAssertEqual(viewSpy.displayBalanceDescriptionCallsCount, 1)
        XCTAssertEqual(viewSpy.balanceDescription?.string, "Seu saldo está rendendo 210% do CDI")
    }
    
    func testPresentSavingsIfNeeded_WhenSavingsIsActiveAndDepositIsZero_ShouldDisplaySavingsAndSetDescription() {
        featureManagerMock.override(key: .walletConfigValues, with: walletRemoteValues)
        let sut = WalletHeaderPresenter(dependencies: container, isSavingsActive: true)
        sut.view = viewSpy
        
        sut.presentSavingsIfNeeded(balance: NSDecimalNumber(integerLiteral: 0))
        
        XCTAssertEqual(viewSpy.displaySavingsCallsCount, 1)
        XCTAssertEqual(viewSpy.displayBalanceDescriptionCallsCount, 1)
        XCTAssertEqual(viewSpy.balanceDescription?.string, "Seu saldo rende 210% do CDI")
    }
    
    func testPresentBalancce_WhenBalanceIsANumber_ShouldDisplayFormatedBalance() {
        let sut = WalletHeaderPresenter(dependencies: container, isSavingsActive: true)
        sut.view = viewSpy
        
        sut.presentBalance(NSDecimalNumber(integerLiteral: 1500))
        
        XCTAssertEqual(viewSpy.displayBalanceCallsCount, 1)
        XCTAssertEqual(viewSpy.balance, "1.500,00")
    }
    
    func testPresentBalancce_WhenBalanceIsNotANumber_ShouldDisplayEmptyBalance() {
        let sut = WalletHeaderPresenter(dependencies: container, isSavingsActive: true)
        sut.view = viewSpy
        
        sut.presentBalance(NSDecimalNumber())
        
        XCTAssertEqual(viewSpy.displayBalanceCallsCount, 1)
        XCTAssertEqual(viewSpy.balance, "")
    }
}
