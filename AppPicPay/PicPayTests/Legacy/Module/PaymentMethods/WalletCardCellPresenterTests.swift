import Core
import FeatureFlag
@testable import PicPay
import XCTest

private class WalletCardCellDisplaySpy: WalletCardCellDisplay {
    // MARK: - DisplayVerifyCardStatus
    private(set) var displayVerifyCardStatusCallsCount = 0

    func displayVerifyCardStatus() {
        displayVerifyCardStatusCallsCount += 1
    }

    // MARK: - HideVerifyCardStatus
    private(set) var hideVerifyCardStatusCallsCount = 0

    func hideVerifyCardStatus() {
        hideVerifyCardStatusCallsCount += 1
    }

    // MARK: - DisplayCardImage
    private(set) var displayCardImageUrlCallsCount = 0
    private(set) var displayCardImageUrlReceivedInvocations: [URL?] = []

    func displayCardImage(url: URL?) {
        displayCardImageUrlCallsCount += 1
        displayCardImageUrlReceivedInvocations.append(url)
    }

    // MARK: - DisplayCardAsSelected
    private(set) var displayCardAsSelectedCallsCount = 0

    func displayCardAsSelected() {
        displayCardAsSelectedCallsCount += 1
    }

    // MARK: - DisplayCardAsUnselected
    private(set) var displayCardAsUnselectedCallsCount = 0

    func displayCardAsUnselected() {
        displayCardAsUnselectedCallsCount += 1
    }

    // MARK: - DisplayCardDescription
    private(set) var displayCardDescriptionCallsCount = 0
    private(set) var displayCardDescriptionReceivedInvocations: [(text: String, isWarning: Bool)] = []

    func displayCardDescription(text: String, isWarning: Bool) {
        displayCardDescriptionCallsCount += 1
        displayCardDescriptionReceivedInvocations.append((text: text, isWarning: isWarning))
    }

    // MARK: - HideCardDescription
    private(set) var hideCardDescriptionCallsCount = 0

    func hideCardDescription() {
        hideCardDescriptionCallsCount += 1
    }

    // MARK: - DisplayCardName
    private(set) var displayCardNameCallsCount = 0
    private(set) var displayCardNameReceivedInvocations: [String] = []

    func displayCardName(text: String) {
        displayCardNameCallsCount += 1
        displayCardNameReceivedInvocations.append(text)
    }

    // MARK: - DisplayLoadingIndicator
    private(set) var displayLoadingIndicatorCallsCount = 0

    func displayLoadingIndicator() {
        displayLoadingIndicatorCallsCount += 1
    }

    // MARK: - HideLoadingIndicator
    private(set) var hideLoadingIndicatorCallsCount = 0

    func hideLoadingIndicator() {
        hideLoadingIndicatorCallsCount += 1
    }
}

final class WalletCardCellPresenterTest: XCTestCase {
    private let viewSpy = WalletCardCellDisplaySpy()
    private lazy var sut: WalletCardCellPresenter = {
        let presenter = WalletCardCellPresenter()
        presenter.view = viewSpy
        return presenter
    }()

    func testConfigure_WhenCardHasVerifiableStatusAndNotHideStatus_ShouldDisplayVerifyStatus() throws {
        let cardMock = try CardBank.mock(status: .verifiable)
        
        sut.configure(card: cardMock, hideStatus: false, isSelected: false, isUpdating: false)
        
        XCTAssertEqual(viewSpy.displayVerifyCardStatusCallsCount, 1)
    }
    
    func testConfigure_WhenCardHasWaitingFillValueStatusAndNotHideStatus_ShouldDisplayVerifyStatus() throws {
        let cardMock = try CardBank.mock(status: .waitingFillValue)
        
        sut.configure(card: cardMock, hideStatus: false, isSelected: false, isUpdating: false)
        
        XCTAssertEqual(viewSpy.displayVerifyCardStatusCallsCount, 1)
    }
    
    func testConfigure_WhenCardHasVerifiedStatusAndNotHideStatus_ShouldHideVerifyStatus() throws {
        let cardMock = try CardBank.mock(status: .verified)
        
        sut.configure(card: cardMock, hideStatus: false, isSelected: false, isUpdating: false)
        
        XCTAssertEqual(viewSpy.hideVerifyCardStatusCallsCount, 1)
    }
    
    func testConfigure_ShouldDisplayCardImage() throws {
        let cardMock = try CardBank.mock()
        
        sut.configure(card: cardMock, hideStatus: false, isSelected: false, isUpdating: false)
        
        XCTAssertEqual(viewSpy.displayCardImageUrlCallsCount, 1)
        XCTAssertEqual(viewSpy.displayCardImageUrlReceivedInvocations, [URL(string: cardMock.image)])
    }
    
    func testConfigure_WhenIsSelected_ShouldDisplayCardAsSelected() throws {
        let cardMock = try CardBank.mock()
        
        sut.configure(card: cardMock, hideStatus: false, isSelected: true, isUpdating: false)
        
        XCTAssertEqual(viewSpy.displayCardAsSelectedCallsCount, 1)
    }
    
    func testConfigure_WhenIsNotSelected_ShouldDisplayCardAsSelected() throws {
        let cardMock = try CardBank.mock()
        
        sut.configure(card: cardMock, hideStatus: false, isSelected: false, isUpdating: false)
        
        XCTAssertEqual(viewSpy.displayCardAsUnselectedCallsCount, 1)
    }
    
    func testConfigure_WhenIsExpiredAndTypeDda_ShouldDisplayCardDescription() throws {
        let cardMock = try CardBank.mock(expired: true, type: CardBank.CardType.dda)
        
        sut.configure(card: cardMock, hideStatus: false, isSelected: false, isUpdating: false)
        
        XCTAssertEqual(viewSpy.displayCardDescriptionCallsCount, 1)
        XCTAssertEqual(viewSpy.displayCardDescriptionReceivedInvocations.first?.text,
                       PaymentMethodsLocalizable.reconnectYourAccount.text)
        XCTAssertEqual(viewSpy.displayCardDescriptionReceivedInvocations.first?.isWarning, true)
    }
    
    func testConfigure_WhenIsExpiredAndTypeNotDda_ShouldDisplayCardDescription() throws {
        let cardMock = try CardBank.mock(expired: true, type: CardBank.CardType.credit)
        
        sut.configure(card: cardMock, hideStatus: false, isSelected: false, isUpdating: false)
        
        XCTAssertEqual(viewSpy.displayCardDescriptionCallsCount, 1)
        XCTAssertEqual(viewSpy.displayCardDescriptionReceivedInvocations.first?.text,
                       PaymentMethodsLocalizable.cardDefeated.text)
        XCTAssertEqual(viewSpy.displayCardDescriptionReceivedInvocations.first?.isWarning, true)
    }
    
    func testConfigure_WhenIsNotExpiredAndTypeCreditPicPay_ShouldHideCardDescription() throws {
        let cardMock = try CardBank.mock(expired: false, cardNetwork: .picpay)
        
        sut.configure(card: cardMock, hideStatus: false, isSelected: false, isUpdating: false)
        
        XCTAssertEqual(viewSpy.hideCardDescriptionCallsCount, 1)
    }
    
    func testConfigure_WhenIsNotExpiredAndNotPicPay_ShouldDisplayCardDescription() throws {
        let cardMock = try CardBank.mock(expired: false, cardNetwork: .visa)
        
        sut.configure(card: cardMock, hideStatus: false, isSelected: false, isUpdating: false)
        
        XCTAssertEqual(viewSpy.displayCardDescriptionCallsCount, 1)
        XCTAssertEqual(viewSpy.displayCardDescriptionReceivedInvocations.first?.text, cardMock.description)
        XCTAssertEqual(viewSpy.displayCardDescriptionReceivedInvocations.first?.isWarning, false)
    }
    
    func testConfigure_WhenCardIsNotExpiredAndNetworkIsPicPay_ShouldDisplayCardName() throws {
        let cardMock = try CardBank.mock(cardNetwork: .picpay)
        
        sut.configure(card: cardMock, hideStatus: false, isSelected: false, isUpdating: false)
        
        XCTAssertEqual(viewSpy.displayCardNameCallsCount, 1)
        XCTAssertEqual(viewSpy.displayCardNameReceivedInvocations.first, cardMock.description)
    }
    
    func testConfigure_WhenCardIsNotExpiredAndNetworkIsNotPicPay_ShouldDisplayCardName() throws {
        let cardMock = try CardBank.mock(cardNetwork: .visa)
        
        sut.configure(card: cardMock, hideStatus: false, isSelected: false, isUpdating: false)
        
        XCTAssertEqual(viewSpy.displayCardNameCallsCount, 1)
        XCTAssertEqual(viewSpy.displayCardNameReceivedInvocations.first, cardMock.alias)
    }
    
    func testConfigure_WhenCardIsUpdating_ShouldDisplayLoadingIndicator() throws {
        let cardMock = try CardBank.mock()
        
        sut.configure(card: cardMock, hideStatus: false, isSelected: false, isUpdating: true)
        
        XCTAssertEqual(viewSpy.displayLoadingIndicatorCallsCount, 1)
    }
    
    func testConfigure_WhenCardIsNotUpdating_ShouldHideLoadingIndicator() throws {
        let cardMock = try CardBank.mock()
        
        sut.configure(card: cardMock, hideStatus: false, isSelected: false, isUpdating: true)
        
        XCTAssertEqual(viewSpy.displayLoadingIndicatorCallsCount, 1)
    }
}

private extension CardBank {
    static func mock(
        status: Status = .unknown,
        expired: Bool = false,
        type: CardType = .credit,
        cardNetwork: CreditCardFlagTypeId = .visa
    ) throws -> CardBank {
        let cardData = """
        {
            "id": "12345",
            "type": "\(type.rawValue)",
            "description": "some description for a card",
            "image": "https://somecardimage.com/image",
            "expired": \(expired ? "true" : "false"),
            "alias": "some alias",
            "address": { "name": "some name address" },
            "lastDigits": "456",
            "verifyStatus": "\(status.rawValue)",
            "creditCardBandeiraId": "\(cardNetwork.rawValue)",
            "cardBrand": "some brand",
            "cardIssuer": "someIssuer",
            "isOneShotCvv": false
        }
        """.data(using: .utf8)
        
        let cardBankData = try XCTUnwrap(cardData)
        let card = try JSONDecoder().decode(CardBank.self, from: cardBankData)
        return card
    }
}
