@testable import PicPay
import XCTest

final class ConsumerAddressItemTest: XCTestCase {
    private func createConsumerAddressItem() throws -> ConsumerAddressItem {
        return try MockDecodable<ConsumerAddressItem>().loadCodableObject(resource: "consumerAddress")
    }
    
    func testGetSubtitle_WhenAddressFull_ShouldCallDisplayTitle() throws {
        let model = try XCTUnwrap(createConsumerAddressItem())
        
        XCTAssertEqual(model.getSubtitle(), "R dos Araçaris, 72, Apartamento 1302 - Serra, ES - 29168665")
    }
    
    func testGetSubtitle_WhenStreetNumberNil_ShouldCallDisplayTitle() throws {
        let model = try XCTUnwrap(createConsumerAddressItem())
        model.streetNumber = nil
        
        XCTAssertEqual(model.getSubtitle(), "R dos Araçaris, Apartamento 1302 - Serra, ES - 29168665")
    }
    
    func testGetSubtitle_WhenZipCodeNil_ShouldCallDisplayTitle() throws {
        let model = try XCTUnwrap(createConsumerAddressItem())
        model.zipCode = nil
        
        XCTAssertEqual(model.getSubtitle(), "R dos Araçaris, 72, Apartamento 1302 - Serra, ES")
    }
    
    func testGetSubtitle_WhenAddressComplementNil_ShouldCallDisplayTitle() throws {
        let model = try XCTUnwrap(createConsumerAddressItem())
        model.addressComplement = nil
        
        XCTAssertEqual(model.getSubtitle(), "R dos Araçaris, 72, Serra, ES - 29168665")
    }
    
    func testGetSubtitle_WhenStreetNumberAnAddressComplementNil_ShouldCallDisplayTitle() throws {
        let model = try XCTUnwrap(createConsumerAddressItem())
        model.streetNumber = ""
        model.addressComplement = nil
        
        XCTAssertEqual(model.getSubtitle(), "R dos Araçaris, Serra, ES - 29168665")
    }
    
    func testGetSubtitle_WhenStreetNameEmpty_ShouldCallDisplayTitle() throws {
        let model = try XCTUnwrap(createConsumerAddressItem())
        model.streetName = ""
        
        XCTAssertEqual(model.getSubtitle(), "72, Apartamento 1302 - Serra, ES - 29168665")
    }
}
