import XCTest
import AnalyticsModule
@testable import PicPay

final class SearchViewModelsTests: XCTestCase {

    private let analyticsMock = AnalyticsSpy()
    private lazy var container = DependencyContainerMock(self.analyticsMock)
    private lazy var sut = SearchViewModel(dependencies: container)

    private func createRow() -> LegacySearchResultRow {
        let row = LegacySearchResultRow(type: .person, fullData: .init())
        row.basicData.id = "1234"
        row.basicData.title = "title"

        return row
    }
}
