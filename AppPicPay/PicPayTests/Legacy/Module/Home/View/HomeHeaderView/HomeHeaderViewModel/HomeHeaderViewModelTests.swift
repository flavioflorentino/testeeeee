import Advertising
import Core
import FeatureFlag
import XCTest

@testable import PicPay

fileprivate final class HomeHeaderViewPresentingSpy: HomeHeaderViewPresenting {
    var viewController: HomeHeaderViewDisplay?

    //MARK: - showHeaderViewLoadingError
    private(set) var showHeaderViewLoadingErrorForSegmentCallsCount = 0
    private(set) var showHeaderViewLoadingErrorForSegmentReceivedInvocations: [Int] = []
    //MARK: - hideHeaderViewLoadingError
    private(set) var hideHeaderViewLoadingErrorForSegmentCallsCount = 0
    private(set) var hideHeaderViewLoadingErrorForSegmentReceivedInvocations: [Int] = []

    func showHeaderViewLoadingError(forSegment index: Int) {
        showHeaderViewLoadingErrorForSegmentCallsCount += 1
        showHeaderViewLoadingErrorForSegmentReceivedInvocations.append(index)
    }

    func hideHeaderViewLoadingError(forSegment index: Int) {
        hideHeaderViewLoadingErrorForSegmentCallsCount += 1
        hideHeaderViewLoadingErrorForSegmentReceivedInvocations.append(index)
    }
}

fileprivate final class ContainerPinnedBannersViewRefreshingSpy: ContainerPinnedBannersViewRefreshing {

    //MARK: - refresh
    private(set) var refreshCallsCount = 0

    func refresh() {
        refreshCallsCount += 1
    }
}

final class HomeHeaderViewModelTests: XCTestCase {
    typealias Dependencies = HasFeatureManager
    fileprivate let featureManager = FeatureManagerMock()
    fileprivate let presenterSpy = HomeHeaderViewPresentingSpy()
    fileprivate let containerPinnerBannersSpy = ContainerPinnedBannersViewRefreshingSpy()
    fileprivate lazy var dependencies: Dependencies = DependencyContainerMock(featureManager)
    fileprivate lazy var sut = HomeHeaderViewModel(presenter: presenterSpy, dependencies: dependencies)
    
    func testRefreshPinnedBanners_WhenIsCalledFromViewController_ShouldRefreshWhenFlagIsEnabled() {
        featureManager.override(key: .opsCardPinnedBannersHomeBool, with: true)
        sut.refreshPinnedBanners(containerPinnedBanners: containerPinnerBannersSpy)
        XCTAssertEqual(containerPinnerBannersSpy.refreshCallsCount, 1)
    }
    
    func testRefreshPinnedBanners_WhenIsCalledFromViewController_ShouldNotRefreshWhenFlagIsNotEnabled() {
        featureManager.override(key: .opsCardPinnedBannersHomeBool, with: false)
        sut.refreshPinnedBanners(containerPinnedBanners: containerPinnerBannersSpy)
        XCTAssertEqual(containerPinnerBannersSpy.refreshCallsCount, 0)
    }
    
    func testCheckConnectionResult_WhenIsCalledWithSuggestionIndex_ShouldHideLoadingErrorOnPresenter() {
        let suggestionIndex = 0
        sut.suggestionConnectionResult = .success
        sut.checkConnectionResult(forIndex: suggestionIndex)
        XCTAssertEqual(presenterSpy.hideHeaderViewLoadingErrorForSegmentCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideHeaderViewLoadingErrorForSegmentReceivedInvocations.last, suggestionIndex)
    }
    
    func testCheckConnectionResult_WhenIsCalledWithFavoritesIndex_ShouldHideLoadingErrorOnPresenter() {
        let favoritesIndex = 1
        sut.favoritesConnectionResult = .success
        sut.checkConnectionResult(forIndex: favoritesIndex)
        XCTAssertEqual(presenterSpy.hideHeaderViewLoadingErrorForSegmentCallsCount, 1)
        XCTAssertEqual(presenterSpy.hideHeaderViewLoadingErrorForSegmentReceivedInvocations.last, favoritesIndex)
    }
    
    func testCheckConnectionResult_WhenIsCalledWithSuggestionIndex_ShouldShowLoadingErrorOnPresenter() {
        let suggestionIndex = 0
        sut.suggestionConnectionResult = .failure(ApiError.unknown(nil))
        sut.checkConnectionResult(forIndex: suggestionIndex)
        XCTAssertEqual(presenterSpy.showHeaderViewLoadingErrorForSegmentCallsCount, 1)
        XCTAssertEqual(presenterSpy.showHeaderViewLoadingErrorForSegmentReceivedInvocations.last, suggestionIndex)
    }
    
    func testCheckConnectionResult_WhenIsCalledWithFavoritesIndex_ShouldShowLoadingErrorOnPresenter() {
        let favoritesIndex = 1
        sut.favoritesConnectionResult = .failure(ApiError.unknown(nil))
        sut.checkConnectionResult(forIndex: favoritesIndex)
        XCTAssertEqual(presenterSpy.showHeaderViewLoadingErrorForSegmentCallsCount, 1)
        XCTAssertEqual(presenterSpy.showHeaderViewLoadingErrorForSegmentReceivedInvocations.last, favoritesIndex)
    }
}
