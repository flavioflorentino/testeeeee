@testable import PicPay
import SwiftyJSON
import XCTest

class AddressSearchTests: XCTestCase {

    func testParserHomeAdress_WhenReceiveValue_ThenUpdateCreditForm() {
        let json = Mock.json(.adressSearch)
        let creditForm = CreditForm()
        creditForm.parserHomeAddress(with: json)
        XCTAssertEqual(creditForm.homeAddress?.zipCode, "05317020")
        XCTAssertNil(creditForm.homeAddress?.addressComplement)
        XCTAssertEqual(creditForm.homeAddress?.city, "São Paulo")
        XCTAssertEqual(creditForm.homeAddress?.cityId, "9")
        XCTAssertNil(creditForm.homeAddress?.neighborhood)
        XCTAssertEqual(creditForm.homeAddress?.state, "SP")
        XCTAssertEqual(creditForm.homeAddress?.stateId, "1")
        XCTAssertEqual(creditForm.homeAddress?.streetName, "Rua")
        XCTAssertEqual(creditForm.homeAddress?.streetNumber, "291")
        XCTAssertEqual(creditForm.homeAddress?.streetType, "Av.")
        XCTAssertEqual(creditForm.homeAddress?.street, "Manuel Bandeira")
    }
    
    func testConvertAddressToParams_WhenSendForSync_ThenCreateADictionary() {
        let creditForm = CreditForm()
        let homeAddress = AddressSearch(zipCode: "05317-020",
                                        addressComplement: "casa",
                                        city: "São Paulo",
                                        cityId: "9",
                                        neighborhood: nil,
                                        state: "SP",
                                        stateId: "1",
                                        streetName: "Rua",
                                        streetNumber: "291",
                                        streetType: "Av.",
                                        street: "Manuel Bandeira")
        creditForm.homeAddress = homeAddress
        let dict = creditForm.convertAddressToParams(homeAddress)
        XCTAssertEqual(dict["zip_code"] as? String, "05317020")
        XCTAssertEqual(dict["address_complement"] as? String, "casa")
        XCTAssertNil(dict["city"] as? String)
        XCTAssertEqual(dict["city_id"] as? String, "9")
        XCTAssertNil(dict["neighborhood"] as? String)
        XCTAssertEqual(dict["state"] as? String, "SP")
        XCTAssertNil(dict["state_id"] as? String)
        XCTAssertEqual(dict["street_number"] as? String, "291")
        XCTAssertEqual(dict["street_type"] as? String, "Av.")
        XCTAssertEqual(dict["street"] as? String, "Manuel Bandeira")
    }
}

private enum Mock: String {
    case adressSearch
    
    static func json(_ type: Mock) -> JSON {
        MockJSON().load(resource: type.rawValue)
    }
}
