@testable import PicPay
import Foundation

public final class CreditCoordinatorManagerSpy: CPRegistrationCoordinatorProtocol {
    private(set) var calledCount: Int = 0
    private(set) var calledPushCount: Int = 0
    private(set) var step: CPStep?
    
    public func getStepProgressForController(with currentStep: CPStep) -> Float {
        step = currentStep
        calledCount += 1
        return 1
    }
    
    public func pushController(with creditForm: CreditForm) {
        calledPushCount += 1
    }
}
