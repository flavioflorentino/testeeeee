import XCTest
import AnalyticsModule
@testable import PicPay

class CPPendingViewModelTests: XCTestCase {
    typealias Dependencies = HasAnalytics
    private let analytics = AnalyticsSpy()
    private lazy var dependencies: Dependencies = DependencyContainerMock(analytics)
    
    private lazy var sut = CPPendingViewModel(
        with: nil,
        dependencies: dependencies
    )
    
    func testTrackSolveAfter_WhenCalledFromViewController_ShouldSendSolveAfterEvent() {
        sut.trackSolveAfter()
        
        let (eventName, eventAction) = getEventProperties()
        XCTAssertEqual(eventName, "PicPay Card - Request - Sign Up - Info Pending")
        XCTAssertEqual(eventAction, "act-do-after")
    }
    
    func testTrackResolveProblems_WhenCalledFromViewController_ShouldSendResolveProblemsEvent() {
        sut.trackResolveProblems()
        
        let (eventName, eventAction) = getEventProperties()
        XCTAssertEqual(eventName, "PicPay Card - Request - Sign Up - Info Pending")
        XCTAssertEqual(eventAction, "act-fix-info-pending")
    }
    
    private func getEventProperties() -> (String?, String?) {
        let event = analytics.events.last
        let action = event?.properties["action"] as? String
        return (event?.name, action)
    }
}
