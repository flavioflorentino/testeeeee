import FeatureFlag
import OHHTTPStubs
import XCTest

@testable import PicPay

class ApiCreditTests: XCTestCase {
    private let featureManagerMock = FeatureManagerMock()
    private lazy var dependenciesMock = DependencyContainerMock(featureManagerMock)
    private lazy var apiCredit = ApiCredit(with: dependenciesMock)
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testDocumentTypeForUser_WhenNewApiIsEnabled_ShouldHaveSuccess() {
        featureManagerMock.override(keys: .opsTransitionCreditApiObjToCore, with: true)
        
        let expectation = XCTestExpectation(description: "Expected to have a valid document Types")
        
        stub(condition: isHost💚.PicPay) { request in
            StubResponse.success(at: "documentTypeForUser.json")
        }
        
        apiCredit.documentTypeForUser() { result in
            switch result {
            case .success(let models):
                XCTAssertTrue(models.isNotEmpty)
                expectation.fulfill()
            case .failure:
                XCTFail("expected to succeed")
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testDocumentTypeForUser_WhenNewApiIsNotEnabled_ShouldHaveSuccess() {
        featureManagerMock.override(keys: .opsTransitionCreditApiObjToCore, with: false)
        
        let expectation = XCTestExpectation(description: "Expected to have a valid document Types")
        
        stub(condition: isHost💚.PicPay) { request in
            StubResponse.success(at: "documentTypeForUser.json")
        }
        
        apiCredit.documentTypeForUser(credit: nil) { result in
            switch result {
            case .success(let models):
                XCTAssertTrue(models.isNotEmpty)
                expectation.fulfill()
            case .failure:
                XCTFail("expected to succeed")
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testDocumentTypes_WhenNewApiIsEnabled_ShouldHaveSuccess() {
        featureManagerMock.override(keys: .opsTransitionCreditApiObjToCore, with: true)
        
        let expectation = XCTestExpectation(description: "Expected to have a valid document Types")
        
        stub(condition: isHost💚.PicPay) { request in
            StubResponse.success(at: "documentTypes.json")
        }
        
        apiCredit.documentTypes { result in
            switch result {
            case .success(let model):
                XCTAssertTrue(model.list.isNotEmpty)
                expectation.fulfill()
            case .failure:
                XCTFail("expected to succeed")
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testDocumentTypes_WhenNewApiIsNotEnabled_ShouldHaveSuccess() {
        featureManagerMock.override(keys: .opsTransitionCreditApiObjToCore, with: false)
        
        let expectation = XCTestExpectation(description: "Expected to have a valid document Types")
        
        stub(condition: isHost💚.PicPay) { request in
            StubResponse.success(at: "documentTypes.json")
        }
        
        apiCredit.documentTypes { result in
            switch result {
            case .success(let model):
                XCTAssertTrue(model.list.isNotEmpty)
                expectation.fulfill()
            case .failure:
                XCTFail("expected to succeed")
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testPatrimonies_WhenNewApiIsEnabled_ShouldHaveSuccess() {
        featureManagerMock.override(keys: .opsTransitionCreditApiObjToCore, with: true)
        
        let expectation = XCTestExpectation(description: "Expected to have a valid document Types")
        
        stub(condition: isHost💚.PicPay) { request in
            StubResponse.success(at: "patrimonies.json")
        }
        
        apiCredit.patrimonies { result in
            switch result {
            case .success(let model):
                XCTAssertTrue(model.list.isNotEmpty)
                expectation.fulfill()
            case .failure:
                XCTFail("expected to succeed")
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testPatrimonies_WhenNewApiIsNotEnabled_ShouldHaveSuccess() {
        featureManagerMock.override(keys: .opsTransitionCreditApiObjToCore, with: false)
        
        let expectation = XCTestExpectation(description: "Expected to have a valid document Types")
        
        stub(condition: isHost💚.PicPay) { request in
            StubResponse.success(at: "patrimonies.json")
        }
        
        apiCredit.patrimonies { result in
            switch result {
            case .success(let model):
                XCTAssertTrue(model.list.isNotEmpty)
                expectation.fulfill()
            case .failure:
                XCTFail("expected to succeed")
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
}
