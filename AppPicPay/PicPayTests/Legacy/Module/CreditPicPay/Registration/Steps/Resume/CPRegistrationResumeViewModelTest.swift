import XCTest
import AnalyticsModule
@testable import PicPay

class CPRegistrationResumeViewModelTest: XCTestCase {
    typealias Dependencies = HasAnalytics
    private let analytics = AnalyticsSpy()
    private lazy var dependencies: Dependencies = DependencyContainerMock(analytics)
    
    private lazy var sut = CPRegistrationResumeViewModel(
        with: CreditForm(),
        container: dependencies
    )
    
    func testTrackBackScreenEvent_WhenCalledFromViewController_ShouldSendTapBackScreenEvent() {
        sut.trackBackScreenEvent()

        let (eventName, eventAction) = getEventProperties()
        XCTAssertEqual(eventName, "PicPay Card - Request - Sign Up - Info Summary")
        XCTAssertEqual(eventAction, "act-previous-step")
    }
    
    func testTrackNextStepEvent_WhenCalledFromViewController_ShouldSendTapContinueEvent() {
        sut.trackNextStepEvent()
        
        let (eventName, eventAction) = getEventProperties()
        XCTAssertEqual(eventName, "PicPay Card - Request - Sign Up - Info Summary")
        XCTAssertEqual(eventAction, "act-next-step")
    }
    
    func testTrackSelectedOption_WhenCalledFromViewController_ShouldSendSelectedOptionEvent() {
        sut.trackSelectedOption(type: .personalInfo)
        var (eventName, eventAction) = getEventProperties()
        XCTAssertEqual(eventName, "PicPay Card - Request - Sign Up - Info Summary")
        XCTAssertEqual(eventAction, "act-fix-personal-info")
        
        sut.trackSelectedOption(type: .birthPlace)
        (eventName, eventAction) = getEventProperties()
        XCTAssertEqual(eventName, "PicPay Card - Request - Sign Up - Info Summary")
        XCTAssertEqual(eventAction, "act-fix-birthplace")
        
        sut.trackSelectedOption(type: .personalAddress)
        (eventName, eventAction) = getEventProperties()
        XCTAssertEqual(eventName, "PicPay Card - Request - Sign Up - Info Summary")
        XCTAssertEqual(eventAction, "act-fix-personal-address")
        
        sut.trackSelectedOption(type: .documentId)
        (eventName, eventAction) = getEventProperties()
        XCTAssertEqual(eventName, "PicPay Card - Request - Sign Up - Info Summary")
        XCTAssertEqual(eventAction, "act-fix-document-id")
        
        sut.trackSelectedOption(type: .additionalInfo)
        (eventName, eventAction) = getEventProperties()
        XCTAssertEqual(eventName, "PicPay Card - Request - Sign Up - Info Summary")
        XCTAssertEqual(eventAction, "act-fix-additional-info")
    }
    
    private func getEventProperties() -> (String?, String?) {
        let event = analytics.events.last
        let action = event?.properties["action"] as? String
        return (event?.name, action)
    }
}
