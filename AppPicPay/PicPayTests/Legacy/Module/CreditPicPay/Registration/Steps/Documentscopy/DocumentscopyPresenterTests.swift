import XCTest
@testable import PicPay

private final class DocumentscopyPresenterViewControllerSpy: DocumentscopyDisplaying {
    // MARK: - Variables
    private(set) var title = ""
    private(set) var description = ""
    private(set) var buttonTitle = ""
    private(set) var step: Float = 0
    private(set) var callScreenCount = 0
    private(set) var displayDocumentscopyCount = 0
    private(set) var loadingEnable: Bool?
    private(set) var callLoadingCount = 0
    private(set) var displayError: Error?
    private(set) var callPresentErrorCount: Int = 0
    
    func displayScreen(image: UIImage, title: String, description: String, buttonTitle: String, step: Float, hiddenLaterButton: Bool) {
        self.title = title
        self.description = description
        self.buttonTitle = buttonTitle
        self.step = step
        callScreenCount = 0
    }
    
    func displayDocumentscopy() {
        displayDocumentscopyCount += 1
    }
    
    func displayLoading(enable: Bool) {
        callLoadingCount += 1
        loadingEnable = enable
    }
    
    func displayError(_ error: Error) {
        callPresentErrorCount += 1
        displayError = error
    }
}

private final class DocumentscopyPresenterCoordinatorSpy: DocumentscopyCoordinating {
    var viewController: UIViewController?
    
    private(set) var actionSelected: DocumentscopyAction?
    
    func perform(action: DocumentscopyAction) {
        actionSelected = action
    }
}

final class DocumentscopyPresenterPresenterTests: XCTest {
    // MARK: - Variables
    typealias Dependencies = HasCreditCoordinatorManager
    private let coordinatorRegistration = CreditCoordinatorManagerSpy()
    private lazy var dependencies: Dependencies = DependencyContainerMock(coordinatorRegistration)
    
    private var viewControllerSpy = DocumentscopyPresenterViewControllerSpy()
    private let coordinatorSpy = DocumentscopyPresenterCoordinatorSpy()
    
    private lazy var sut: DocumentscopyPresenter = {
        let sut = DocumentscopyPresenter(isReview: false, coordinator: coordinatorSpy, dependencies: dependencies)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    // MARK: - Tests
    func testPresentScreen_WhenProccessIsTrue_ShouldDisplayScreen () {
        sut.presentScreen(proccessSuccess: true)
        XCTAssertEqual(viewControllerSpy.callScreenCount, 1)
        XCTAssertEqual(viewControllerSpy.buttonTitle, "Próximo")
        XCTAssertEqual(viewControllerSpy.title, "Já validamos sua identidade")
        XCTAssertEqual(viewControllerSpy.description, "Sua foto, documento e informações estão corretas. Agora, seu PicPay Card está ainda mais seguro.")
        XCTAssertEqual(viewControllerSpy.step, 0)
        XCTAssertEqual(coordinatorRegistration.calledCount, 1)
    }
    
    func testPresentScreen_WhenProccessIsFalse_ShouldDisplayScreen () {
        sut.presentScreen(proccessSuccess: false)
        XCTAssertEqual(viewControllerSpy.callScreenCount, 1)
        XCTAssertEqual(viewControllerSpy.buttonTitle, "Validar identidade")
        XCTAssertEqual(viewControllerSpy.title, "Mais segurança para seu PicPay Card")
        XCTAssertEqual(viewControllerSpy.description, "Precisamos de uma foto do seu rosto e de um documento com foto para deixar seu PicPay Card mais seguro.")
        XCTAssertEqual(viewControllerSpy.step, 0)
        XCTAssertEqual(coordinatorRegistration.calledCount, 1)
    }
    
    func testPresentLoading_WhenLoadingIsTrue_ShouldEnableLoading() {
        sut.presentLoading(enable: true)
        XCTAssertEqual(viewControllerSpy.loadingEnable, true)
        XCTAssertEqual(viewControllerSpy.callLoadingCount, 1)
    }
    
    func testPresentLoading_WhenLoadingIsFalse_ShouldDisableLoading() {
        sut.presentLoading(enable: false)
        XCTAssertEqual(viewControllerSpy.loadingEnable, false)
        XCTAssertEqual(viewControllerSpy.callLoadingCount, 1)
    }
    
    func testPresentDocumentscopy_WhenConfirmAction_ShouldDisplayDocumentscopy() {
        sut.presentDocumentscopy()
        XCTAssertEqual(viewControllerSpy.displayDocumentscopyCount, 1)
    }
}
