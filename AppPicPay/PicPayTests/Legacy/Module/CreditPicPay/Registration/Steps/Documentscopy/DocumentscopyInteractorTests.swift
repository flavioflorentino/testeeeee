import AnalyticsModule
import Core
import XCTest
@testable import PicPay

private final class DocumentscopyPresentingSpy: DocumentscopyPresenting {
    // MARK: - Variables
    var viewController: DocumentscopyDisplaying?
    private(set) var callPresentScreen = 0
    private(set) var proccessSuccess: Bool?
    private(set) var callLoadingCount = 0
    private(set) var loadingEnable: Bool?
    private(set) var callErrorCount = 0
    private(set) var callDocumentscopyCount = 0
    private(set) var action: [DocumentscopyAction] = []
    
    func presentScreen(proccessSuccess: Bool) {
        self.proccessSuccess = proccessSuccess
        callPresentScreen += 1
    }
    
    func presentLoading(enable: Bool) {
        loadingEnable = enable
        callLoadingCount += 1
    }
    
    func presentError(_ error: Error) {
        callErrorCount += 1
    }
    
    func presentDocumentscopy() {
        callDocumentscopyCount += 1
    }
    
    func didNextStep(action: DocumentscopyAction) {
        self.action.append(action)
    }
}

private final class DocumentscopyServiceSpy: DocumentscopyServicing {
    // MARK: - Variables
    var expectedResult: Result<CardBiometricProcess, ApiError> = .success(CardBiometricProcess(identityAlreadyValidated: true))
    
    func documentscopyProcess(completion: @escaping BiometricProccess) {
        completion(expectedResult)
    }
        
    func setCreditRegistration(creditForm: CreditForm, isCompleted: Bool, completion: @escaping (Result<Void, Error>) -> Void) { }
}

final class DocumentscopyInteractorTests: XCTestCase {
    // MARK: - Variables
    private var presenterSpy = DocumentscopyPresentingSpy()
    private let serviceSpy = DocumentscopyServiceSpy()
    
    typealias Dependencies = HasAnalytics & HasCreditCoordinatorManager
    private let analytics = AnalyticsSpy()
    private let coordinatorRegistration = CreditCoordinatorManagerSpy()
    private lazy var dependencies: Dependencies = DependencyContainerMock(analytics, coordinatorRegistration)
    
    private let form = CreditForm()
    private lazy var sut = DocumentscopyInteractor(service: serviceSpy, presenter: presenterSpy, creditForm: form, isReview: false)
    
    // MARK: - Public Methods
    func testCheckProcess_WhenAlreadyValidatedIsTrue_ShouldPresentScreen() {
        serviceSpy.expectedResult = .success(CardBiometricProcess(identityAlreadyValidated: true))
        sut.checkProcess()
        XCTAssertEqual(presenterSpy.callLoadingCount, 2)
        XCTAssertEqual(presenterSpy.loadingEnable, false)
        XCTAssertEqual(presenterSpy.callPresentScreen, 1)
    }
    
    func testCheckProcess_WhenAlreadyValidatedIsFalse_ShouldPresentScreen() {
        serviceSpy.expectedResult = .success(CardBiometricProcess(identityAlreadyValidated: false))
        sut.checkProcess()
        XCTAssertEqual(presenterSpy.callLoadingCount, 2)
        XCTAssertEqual(presenterSpy.loadingEnable, false)
        XCTAssertEqual(presenterSpy.callPresentScreen, 1)
    }
    
    func testCheckProcess_WhenTimeout_ShouldPresentError() {
        serviceSpy.expectedResult = .failure(ApiError.timeout)
        sut.checkProcess()
        XCTAssertEqual(presenterSpy.callErrorCount, 1)
    }
    
    func testConfirm_WhenAlreadyValidatedIsFalse_ShouldPresentDocumentscopy() {
        serviceSpy.expectedResult = .success(CardBiometricProcess(identityAlreadyValidated: false))
        sut.checkProcess()
        sut.confirm()
        XCTAssertEqual(presenterSpy.callDocumentscopyCount, 1)
    }
    
    func testCheckProcess_WhenAlreadyValidatedIsTrueAndIsReviewIsTrue_ShouldPresentScreen() {
        sut.isReview = true
        serviceSpy.expectedResult = .success(CardBiometricProcess(identityAlreadyValidated: true))
        sut.checkProcess()
        sut.confirm()
        XCTAssertEqual(presenterSpy.callLoadingCount, 3)
        XCTAssertEqual(presenterSpy.loadingEnable, true)
        XCTAssertEqual(presenterSpy.callPresentScreen, 1)
    }
    
    func testCheckProcess_WhenAlreadyValidatedIsFalseAndIsReviewIsTrue_ShouldPresentScreen() {
        sut.isReview = true
        serviceSpy.expectedResult = .success(CardBiometricProcess(identityAlreadyValidated: false))
        sut.checkProcess()
        XCTAssertEqual(presenterSpy.callLoadingCount, 2)
        XCTAssertEqual(presenterSpy.loadingEnable, false)
        XCTAssertEqual(presenterSpy.callPresentScreen, 1)
    }
}
