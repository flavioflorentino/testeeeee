import AnalyticsModule
import FeatureFlag
import XCTest

@testable import PicPay

class CreditAttachDocViewModelTests: XCTestCase {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let analytics = AnalyticsSpy()
    private let featureManager = FeatureManagerMock()
    private lazy var dependencies: Dependencies = DependencyContainerMock(analytics, featureManager)
    
    private lazy var sut = CreditAttachDocViewModel(
        with: CreditForm(),
        container: dependencies
    )
    
    func testCanShowBiometricScreen_WhenLoanAndLoanFlagFalse_ShouldReturnFalse() {
        sut.creditForm.isLoanFlow = true
        featureManager.override(key: .releaseFeatureLoanSelfie, with: false)
        XCTAssertFalse(sut.canShowBiometricScreen())
    }
    
    func testCanShowBiometricScreen_WhenLoanAndLoanFlagTrue_ShouldReturnTrue() {
        sut.creditForm.isLoanFlow = true
        featureManager.override(key: .releaseFeatureLoanSelfie, with: true)
        XCTAssertTrue(sut.canShowBiometricScreen())
    }
    
    func testCanShowBiometricScreen_WhenLoanOffAndBiometricFlagFalse_ShouldReturFalse() {
        sut.creditForm.isLoanFlow = false
        
        featureManager.override(key: .featureCardBiometric, with: false)
        XCTAssertFalse(sut.canShowBiometricScreen())
    }
    
    func testCanShowBiometricScreen_WhenLoanOffAndBiometricFlagTrue_ShouldReturTrue() {
        sut.creditForm.isLoanFlow = false
        featureManager.override(key: .featureCardBiometric, with: true)
        XCTAssertTrue(sut.canShowBiometricScreen())
    }
    
    func testTrackBackScreenEvent_WhenCalledFromViewController_ShouldSendBackScreenEvent() {
        sut.trackBackScreenEvent()

        let (eventName, eventAction) = getEventProperties()
        XCTAssertEqual(eventName, "PicPay Card - Request - Sign Up - Document Photo")
        XCTAssertEqual(eventAction, "act-previous-step")
    }
    
    func testTrackContinueEvent_WhenCalledFromViewController_ShouldSendBackScreenEvent() {
        sut.trackContinueEvent()

        let (eventName, eventAction) = getEventProperties()
        XCTAssertEqual(eventName, "PicPay Card - Request - Sign Up - Document Photo")
        XCTAssertEqual(eventAction, "act-next-step")
    }
    
    func testtrackTapDocumentType_WhenCalledFromViewController_ShouldSendBackScreenEvent() {
        sut.trackTapDocumentType(documentType: .documentFront)
        
        var (eventName, eventAction) = getEventProperties()
        XCTAssertEqual(eventName, "PicPay Card - Request - Sign Up - Document Photo")
        XCTAssertEqual(eventAction, "act-take-document-front")
        
        sut.trackTapDocumentType(documentType: .documentBack)
        
        (eventName, eventAction) = getEventProperties()
        XCTAssertEqual(eventName, "PicPay Card - Request - Sign Up - Document Photo")
        XCTAssertEqual(eventAction, "act-take-document-back")
            
        sut.trackTapDocumentType(documentType: .selfie)
        
        (eventName, eventAction) = getEventProperties()
        XCTAssertEqual(eventName, "PicPay Card - Request - Sign Up - Document Photo")
        XCTAssertEqual(eventAction, "act-take-selfie")
        
        sut.trackTapDocumentType(documentType: .proofOfAddress)
        
        (eventName, eventAction) = getEventProperties()
        XCTAssertEqual(eventName, "PicPay Card - Request - Sign Up - Document Photo")
        XCTAssertEqual(eventAction, "act-take-proof-address")
    }
    
    private func getEventProperties() -> (String?, String?) {
        let event = analytics.events.last
        let action = event?.properties["action"] as? String
        return (event?.name, action)
    }
}
