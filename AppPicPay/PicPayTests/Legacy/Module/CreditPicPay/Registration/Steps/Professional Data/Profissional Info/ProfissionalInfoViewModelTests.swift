import XCTest
import AnalyticsModule
@testable import PicPay

private final class ProfissionalInfoPresentingSpy: ProfissionalInfoPresenting {
    // MARK: - Variables
    var viewController: ProfissionalInfoDisplay?
    private(set) var callProgressValueCount = 0
    private(set) var callNextStepCount = 0
    private(set) var callSetupInitialCount = 0
    private(set) var callIncomeErrorCount = 0
    private(set) var callEnableContinueButtonCount = 0
    private(set) var progressValue: Float = 0
    private(set) var incomeErrorMessage: String?
    private(set) var enableButton: Bool?
    
    func setupProgressView(value: Float) {
        progressValue = value
        callProgressValueCount += 1
    }
    
    func didNextStep(creditForm: CreditForm) {
        callNextStepCount += 1
    }
    
    func setupInitialForm(creditForm: CreditForm) {
        callSetupInitialCount += 1
    }
    
    func presentIncomeError(message: String?) {
        incomeErrorMessage = message
        callIncomeErrorCount += 1
    }
    
    func enableContinueButton(enable: Bool) {
        enableButton = enable
        callEnableContinueButtonCount += 1
    }
}

private final class ProfissionalInfoServiceSpy: ProfissionalInfoServicing {
    // MARK: - Variables
    private(set) var loadListProfessionCount = 0
    private(set) var setCreditRegistrationCount = 0
    
    func loadListPatrimonies(completion: @escaping CompletionGetProfessionalData) {
        loadListProfessionCount += 1
    }
    
    func setCreditRegistration(creditForm: CreditForm) {
        setCreditRegistrationCount += 1
    }
}

final class ProfissionalInfoViewModelTests: XCTestCase {
    // MARK: - Variables
    private var presenterSpy = ProfissionalInfoPresentingSpy()
    private let serviceSpy = ProfissionalInfoServiceSpy()
    
    typealias Dependencies = HasAnalytics & HasCreditCoordinatorManager
    private let analytics = AnalyticsSpy()
    private let coordinatorRegistration = CreditCoordinatorManagerSpy()
    private lazy var dependencies: Dependencies = DependencyContainerMock(analytics, coordinatorRegistration)
    
    private let form = CreditForm()
    private lazy var sut = ProfissionalInfoViewModel(with: form, service: serviceSpy, presenter: presenterSpy, dependencies: dependencies)
    
    // MARK: - Public Methods
    func testSetupInitialForm_WhenDidLoadScreen_ShouldUpdateScreen() {
        sut.setupInitialForm()
        XCTAssertEqual(presenterSpy.callSetupInitialCount, 1)
    }
    
    func testGetRegistration_WhenGetCurrentStatus_ShouldUpdateProgressView() {
        sut.getRegistrationStep()
        XCTAssertEqual(coordinatorRegistration.calledCount, 1)
        XCTAssertEqual(coordinatorRegistration.step, CPStep.professionalData)
        XCTAssertEqual(presenterSpy.progressValue, 1)
        XCTAssertEqual(presenterSpy.callProgressValueCount, 1)
    }
    
    func testValidateAndEnableContinue_WhenHasCorrectValueAndId_ShouldEnableButton() throws {
        sut.updatePatrimony(with: 1)
        sut.validateAndEnableContinue(text: "R$ 90,00")
        let enable = try XCTUnwrap(presenterSpy.enableButton)
        XCTAssertTrue(enable)
        XCTAssertEqual(presenterSpy.callEnableContinueButtonCount, 1)
    }
    
    func testCheckIncomeText_WhenHasEmptyErrorMessage_ShouldClearError() {
        sut.checkIncomeText(text: nil)
        XCTAssertNil(presenterSpy.incomeErrorMessage)
        XCTAssertEqual(presenterSpy.callIncomeErrorCount, 0)
    }
    
    func testCheckIncomeText_WhenHasErrorMessgae_ShouldShowError() throws {
        sut.checkIncomeText(text: "")
        let message = try XCTUnwrap(presenterSpy.incomeErrorMessage)
        XCTAssertEqual(message, "Renda mensal é obrigatório")
    }

    func testValidateAndEnableContinue_WhenHasNilValue_ShouldIgnoreResult() {
        sut.validateAndEnableContinue(text: nil)
        XCTAssertNil(presenterSpy.enableButton)
    }
    
    func testValidateAndEnableContinue_WhenHasCorrectValue_ShouldDisableButton() throws {
        sut.validateAndEnableContinue(text: "R$ 90,00")
        let enable = try XCTUnwrap(presenterSpy.enableButton)
        XCTAssertFalse(enable)
    }
    
    func testBackScreenEvent_WhenDismissViewController_ShouldSendBackScreenEvent() {
        sut.backScreenEvent()

        let (eventName, eventAction) = getEventProperties()
        XCTAssertEqual(eventName, "PicPay Card Request - Sign Up - Additional Personal Info")
        XCTAssertEqual(eventAction, "act-previous-step")
    }
    
    func testNextStepEvent_WhenClickSaveButton_ShouldSendNextScreenEvent() {
        sut.updatePatrimony(with: 1)
        sut.save(incomeText: "R$ 90,00")

        let (eventName, eventAction) = getEventProperties()
        XCTAssertEqual(eventName, "PicPay Card Request - Sign Up - Additional Personal Info")
        XCTAssertEqual(eventAction, "act-next-step")
        XCTAssertEqual(presenterSpy.callNextStepCount, 1)
    }
    
    private func getEventProperties() -> (String?, String?) {
        let event = analytics.events.last
        let action = event?.properties["action"] as? String
        return (event?.name, action)
    }
}
