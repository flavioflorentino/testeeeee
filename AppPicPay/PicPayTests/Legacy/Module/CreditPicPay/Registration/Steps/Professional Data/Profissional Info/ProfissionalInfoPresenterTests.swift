import XCTest
@testable import PicPay

private final class ProfissionalInfoViewControllerSpy: ProfissionalInfoDisplay {
    // MARK: - Variables
    private(set) var callUpdateProgressViewCount: Int = 0
    private(set) var callDisplayInitialFormCount: Int = 0
    private(set) var callPresentErrorCount: Int = 0
    private(set) var callContinueButtonCount: Int = 0
    private(set) var progressViewValue: Float = 0
    private(set) var buttonDisplayTitle: String?
    private(set) var incomeMessageError: String?
    private(set) var enableButton: Bool?
    
    func updateProgressView(value: Float) {
        progressViewValue = value
        callUpdateProgressViewCount += 1
    }
    
    func displayInitialForm(creditForm: CreditForm, buttonTitle: String) {
        buttonDisplayTitle = buttonTitle
        callDisplayInitialFormCount += 1
    }
    
    func displayIncomeError(message: String?) {
        callPresentErrorCount += 1
        incomeMessageError = message
    }
    
    func enableContinueButton(enable: Bool) {
        callContinueButtonCount += 1
        enableButton = enable
    }
}

private final class ProfissionalInfoCoordinatorSpy: ProfissionalInfoCoordinating {
    var viewController: UIViewController?
    
    private(set) var actionSelected: ProfissionalInfoAction?
    
    func perform(action: ProfissionalInfoAction) {
        actionSelected = action
    }
}

final class ProfissionalInfoPresenterTests: XCTest {
    // MARK: - Variables
    private var viewControllerSpy = ProfissionalInfoViewControllerSpy()
    private let coordinatorSpy = ProfissionalInfoCoordinatorSpy()
    private let container = DependencyContainer()
    
    private lazy var sut: ProfissionalInfoPresenter = {
        let sut = ProfissionalInfoPresenter(coordinator: coordinatorSpy, isEditingForm: false)
        sut.viewController = viewControllerSpy
        return sut
    }()
    
    // MARK: - Tests
    func testPresentIncomeError_WhenErrorMessage_ShouldPresentError() {
        let message = "ErrorMessage"
        sut.presentIncomeError(message: message)
        XCTAssertEqual(viewControllerSpy.incomeMessageError, message)
    }
    
    func testPresentIncomeError_WhenErrorMessageIsNil_ShouldPresentError() {
        sut.presentIncomeError(message: nil)
        XCTAssertNil(viewControllerSpy.incomeMessageError)
    }
    
    func testSetupInitialForm() {
        let form = CreditForm()
        sut.setupInitialForm(creditForm: form)
        XCTAssertEqual(viewControllerSpy.buttonDisplayTitle, "Continuar")
    }
    
    func testDidNextStep_WhenSelectNext_ShouldActionClose() {
        let form = CreditForm()
        sut.didNextStep(creditForm: form)
        XCTAssertEqual(coordinatorSpy.actionSelected, ProfissionalInfoAction.close)
    }
    
    func testShouldEnableContinueButton_WhenEnable_ShouldEnableButton() throws {
        sut.enableContinueButton(enable: true)
        let enable = try XCTUnwrap(viewControllerSpy.enableButton)
        XCTAssertTrue(enable)
        XCTAssertEqual(viewControllerSpy.callContinueButtonCount, 1)
    }
    
    func testShouldEnableContinueButton_WhenDisable_ShouldDisableButton() throws {
        sut.enableContinueButton(enable: false)
        let enable = try XCTUnwrap(viewControllerSpy.enableButton)
        XCTAssertFalse(enable)
        XCTAssertEqual(viewControllerSpy.callContinueButtonCount, 1)
    }
}

extension ProfissionalInfoAction: Equatable {
    public static func == (lhs: ProfissionalInfoAction, rhs: ProfissionalInfoAction) -> Bool {
        switch (lhs, rhs) {
        case (.close, .close):
            return true
        case (.nextStep, .nextStep):
            return true
        default:
            return false
        }
    }
}
