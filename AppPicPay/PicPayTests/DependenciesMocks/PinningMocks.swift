import Core
@testable import PicPay

typealias ChallengeHandler = (URLSession.AuthChallengeDisposition, URLCredential?) -> Void

final class PinningHandlerSpy: PinningHandlerProvider {
    private (set) var challenge: URLAuthenticationChallenge?
    
    func handle(challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        self.challenge = challenge
        completionHandler(.cancelAuthenticationChallenge, nil)
    }
}

final class SessionDelegateDummy: NSObject, URLAuthenticationChallengeSender {
    func use(_ credential: URLCredential, for challenge: URLAuthenticationChallenge) { }
    func continueWithoutCredential(for challenge: URLAuthenticationChallenge) { }
    func cancel(_ challenge: URLAuthenticationChallenge) { }
}
