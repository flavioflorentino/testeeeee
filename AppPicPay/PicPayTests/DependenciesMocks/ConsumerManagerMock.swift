import Foundation
import XCTest

@testable import PicPay

final class ConsumerManagerMock: MockVerifier {
    // MARK: - Properties
    private var loadConsumerBalanceCount: Int = 0

    var consumer: MBConsumer?
    var privacyConfig: Int = 0
    var lastChosenPrivacyConfig: Int = 0
    var isBalanceHidden: Bool = false
    var isIncompleteAccount: Bool = false
    
    var loadConsumerDataCachedResponse: (success: Bool, error: Error?)?

    // MARK: - Verifiers
    func verifyLoadConsumerBalance(file: StaticString = #file, line: UInt = #line) {
        verifyMethodCalledOnce(methodName: "loadConsumerBalance", callCount: loadConsumerBalanceCount, file: file, line: line)
    }
}

extension ConsumerManagerMock: ConsumerManagerContract {
    func loadConsumerBalance() {
        loadConsumerBalanceCount += 1
    }

    func setConsumerBalance(_ value: NSDecimalNumber) { }

    func checkFirstInstallAndClearKeychainIfNeeded() { }

    func privacyConfigSuggestionPromptCount() -> Int { 0 }

    func incrementPrivacyConfigSuggestionPromptCountWithLastPrivacy(privacy: Int) { }

    func resetAllLocalConsumerInformation() { }

    func toggleBalanceVisibility() -> Bool { false }

    func loadConsumerData(completion: ((_ success: Bool, _ error: Error?) -> Void)?) { }

    func loadConsumerDataCached(useCache: Bool, completion: ((_ success: Bool, _ error: Error?) -> Void)?) {
        guard let response = loadConsumerDataCachedResponse else {
            return
        }

        completion?(response.success, response.error)
    }

    func setGetAllConsumerData(cachedData: [String: Any]) { }

    func getAllConsumerDataCache() -> [String: Any]? { [:] }

    func setUseBalance(_ use: Bool) { }

    func useBalance() -> Bool { false}

    static func setUseBalance(_ use: Bool) { }
    static func useBalance() -> Bool { false }
}
