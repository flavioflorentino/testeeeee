@testable import PicPay

final class UserManagerMock: UserManagerContract {
    var isAuthenticated: Bool = false
    var token: String?
    
    func updateToken(_ token: String) {
        self.token = token
    }
    
    func deleteToken() {
        token = nil
    }
}
