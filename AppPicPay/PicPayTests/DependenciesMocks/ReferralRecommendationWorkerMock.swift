@testable import PicPay

struct ReferralRecommendationWorkerMock: ReferralRecommendationProtocol {
    var isFromReferral: Bool = false
    var recommendationName: String?
}
