@testable import PicPay

struct AuthManagerMock: AuthManagerContract {
    var resultError: Error?
    
    func logout(fromContext context: UIViewController?, completion: ((Error?) -> Void)?) {
        completion?(resultError)
    }
}
