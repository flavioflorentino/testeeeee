import XCTest

@testable import PicPay

final class StudentWorkerMock: StudentWorkerContract {
    var loadStatusResponse: PicPayResult<StatusPayload>?
    
    func loadStatus(_ completion: @escaping (PicPayResult<StatusPayload>) -> Void) {
        guard let response = loadStatusResponse else {
            XCTFail("No status response to load")
            return
        }
        completion(response)
    }
    
    func loadStatus(consumerId: String, _ completion: @escaping (PicPayResult<StatusPayload>) -> Void) {
        guard let response = loadStatusResponse else {
            XCTFail("No status response to load")
            return
        }
        completion(response)
    }
}
