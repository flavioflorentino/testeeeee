@testable import PicPay

final class DynamicLinkHelperMock: DynamicLinkHelperContract {
    var referralCodeToRegistration: String?
    var callGetRegisteredLinkCount = 0
    var callUnregisterLinkCount = 0
    var mockedUrl: URL?
    
    func getRegisteredLink() -> URL? {
        callGetRegisteredLinkCount += 1
        return mockedUrl
    }
    
    func unregisterDynamicLink() {
        callGetRegisteredLinkCount += 1
    }
}
