import AccessibilityKit
import AnalyticsModule
import Core
import CoreLegacy
import CustomerSupport
import DirectMessageSB
import FeatureFlag
import Socket
import XCTest

@testable import PicPay

/// Don't use this class for testing, use DependencyContainerMock
final class DependencyContainer {
    init() {
        fatalError("Use a DependencyContainerMock on test target")
    }
}

final class DependencyContainerMock: AppDependencies {
    lazy var mainQueue: DispatchQueue = resolve()
    lazy var notificationCenter: NotificationCenter = resolve()
    lazy var globalQueue: DispatchQueue = resolve()
    lazy var featureManager: FeatureManagerContract = resolve()
    lazy var consumerManager: ConsumerManagerContract = resolve()
    lazy var creditCardManager: CreditCardManagerContract = resolve()
    lazy var paymentManager: PaymentManagerContract = resolve()
    lazy var authManager: AuthManagerContract = resolve()
    lazy var appManager: AppManagerContract = resolve()
    lazy var locationManager: LocationManagerContract = resolve()
    lazy var userManager: UserManagerContract = resolve()
    lazy var dynamicLinkHelper: DynamicLinkHelperContract = resolve()
    lazy var studentWorker: StudentWorkerContract = resolve()
    lazy var analytics: AnalyticsProtocol = resolve()
    lazy var kvStore: KVStoreContract = resolve()
    lazy var dispatchGroup: DispatchGroup = resolve()
    lazy var consumerApi: ConsumerApiProtocol = resolve()
    lazy var registrationCoordinator: CPRegistrationCoordinatorProtocol? = resolve()
    lazy var referralManager: ReferralManageable = resolve()
    lazy var socketManager: SocketManageable = resolve()
    lazy var appParameters: AppParametersContract = resolve()
    lazy var customerSupport: CustomerSupportContract = resolve()
    lazy var chatApiManager: ChatApiManaging = resolve()
    lazy var chatNotifier: ChatNotifying = resolve()
    lazy var accessibilityManager: AccessibilityManagerContract = resolve()
    lazy var keychain: KeychainManagerContract = KeychainManagerMock(isPersistent: false)
    lazy var keychainWithPersistent: KeychainManagerContract = KeychainManagerMock(isPersistent: true)
    lazy var connectionStateFactory: ConnectionStateManufacturing = resolve()
    
    private let dependencies: [Any]
    
    init(_ dependencies: Any...) {
        self.dependencies = dependencies
    }
}

extension DependencyContainerMock {
    func resolve<T>() -> T {
        let resolved = dependencies.compactMap { $0 as? T }
                
        switch resolved.first {
        case .none:
            fatalError("DependencyContainerMock could not resolve dependency: \(T.self)\n")
        case .some where resolved.count > 1:
            fatalError("DependencyContainerMock resolved mutiple dependencies for: \(T.self)\n")
        case .some(let mock):
            return mock
        }
    }
    
    func resolve() -> DispatchQueue {
        let resolved = dependencies.compactMap { $0 as? DispatchQueue }.first
        return resolved ?? DispatchQueue(label: "DependencyContainerMock")
    }
    
    func resolve() -> DispatchGroup {
        let resolved = dependencies.compactMap { $0 as? DispatchGroup }.first
        return resolved ?? DispatchGroup()
    }

    func resolve() -> ChatApiManaging {
        let resolved = dependencies.compactMap { $0 as? ChatApiManaging }.first
        return resolved ?? ChatApiManagerMock()
    }
    
    func resolve() -> NotificationCenter {
        dependencies.compactMap { $0 as? NotificationCenter }
            .first ?? .default
    }
    
    func resolve() -> ChatNotifying {
        dependencies.compactMap { $0 as? ChatNotifying }
            .first ?? ChatNotifierSpy()
    }

    func resolve() -> ConnectionStateManufacturing {
        let resolved = dependencies.compactMap { $0 as? ConnectionStateManufacturing }.first
        return resolved ?? ConnectionStateFactory()
    }
}
