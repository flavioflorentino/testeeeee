import Foundation
import UIKit

public final class AlertControllerMock: UIAlertController {
    private(set) var didPresentAlertCount = 0
    override public func viewDidLoad() {
        super.viewDidLoad()
        didPresentAlertCount += 1
    }
}
