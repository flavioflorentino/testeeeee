@testable import PicPay

public final class NavigationControllerMock: UINavigationController {
    public var isPushViewControllerCalled: Bool = false
    public var isPopViewControllerCalled: Bool = false
    public var isPresentViewControllerCalled: Bool = false
    public var isDismissViewControllerCalled: Bool = false
    public var isFadeReplaceViewControllerCalled: Bool = false
    public var isSetViewControllersCalled: Bool = false
    public var isPopToRootViewControllerCalled: Bool = false
    
    public var pushedViewController: UIViewController?
    public var popedViewController: UIViewController?
    public var viewControllerPresented: UIViewController?
    
    public override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        pushedViewController = viewController
        isPushViewControllerCalled = true
        super.pushViewController(viewController, animated: false)
    }
    
    public override func popViewController(animated: Bool) -> UIViewController? {
        let viewController = super.popViewController(animated: false)
        popedViewController = viewController
        isPopViewControllerCalled = true
        return viewController
    }
    
    public override func popToRootViewController(animated: Bool) -> [UIViewController]? {
        let viewControllersList = super.popToRootViewController(animated: false)
        isPopToRootViewControllerCalled = true
        return viewControllersList
    }
    
    public override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        viewControllerPresented = viewControllerToPresent
        isPresentViewControllerCalled = true
        super.present(viewControllerToPresent, animated: false)
        completion?()
    }
    
    public override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        isDismissViewControllerCalled = true
        super.dismiss(animated: false)
        completion?()
    }
    
    public override func fadeReplace(_ vc: UIViewController, by secondVc: UIViewController?) {
        isFadeReplaceViewControllerCalled = true
        super.fadeReplace(vc, by: secondVc)
    }
    
    public override func setViewControllers(_ viewControllers: [UIViewController], animated: Bool) {
        isSetViewControllersCalled = true
        super.setViewControllers(viewControllers, animated: false)
    }

    // MARK: - Helpers
    public func tapAtBackBarButton() {
        let controllers = viewControllers.filter { $0 != topViewController }

        guard let controller = controllers.first else {
            return
        }

        setViewControllers(controllers, animated: false)
        delegate?.navigationController?(self, didShow: controller, animated: true)
    }
}
