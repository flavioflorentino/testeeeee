import AnalyticsModule
import Core
import FeatureFlag
import XCTest

@testable import PicPay

private final class MockRootViewControllerManager: RootViewControllerManager {
    func changeRootViewController(_ viewController: UIViewController) {
    }
}

final class MainCoordinatorTest: XCTestCase {
    private var featureManagerMock = FeatureManagerMock()
    private var analyticsSpy = AnalyticsSpy()

    private lazy var mockDependencies = DependencyContainerMock(featureManagerMock, analyticsSpy)
    private lazy var mainCoordinator = MainCoordinator(
        dependencies: mockDependencies,
        rootViewControllerManager: MockRootViewControllerManager(),
        needsSyncRemoteConfigLoad: false
    )
    private var window = UIWindow(frame: .zero)
    private var navigationController: UINavigationController? {
         guard let navigationController = window.rootViewController as? UINavigationController else {
             return nil
         }
         return navigationController
     }
    
    private var mainTabBar: MainTabBarController {
        guard let pagination = window.rootViewController as? ControllerPagination,
        let tab = pagination.childrens[ControllerPagination.Page.application.rawValue].viewController as? MainTabBarController else {
            fatalError("Expected an ControllerPagination")
        }
        return tab
    }
    
    func testStart_WhenNeedsSyncRemoteConfigLoadEnabled_ShouldPresentLandingLoading() {
        mainCoordinator = MainCoordinator(
            dependencies: mockDependencies,
            rootViewControllerManager: MockRootViewControllerManager(),
            needsSyncRemoteConfigLoad: true
        )
        
        window = mainCoordinator.start(with: nil)
        
        XCTAssertTrue(window.rootViewController is LandingLoaderViewController)
    }
    
    func testStart_ShouldPresentLandingScreen() {
        Scene.logout.run()
        window = mainCoordinator.start(with: nil)
        
        XCTAssertTrue(navigationController?.topViewController is SignUpViewController)
    }
    
    func testStart_WhenRegistrationCacheIsNotNil_ShouldPresentContinueRegistrationScreen() {
        let cachedStepSequence: [RegistrationStep] = [.password, .email, .document, .name]
        featureManagerMock.override(key: .continueRegistration, with: true)
        featureManagerMock.override(key: .dynamicRegistrationStepsSequence, with: cachedStepSequence)
        Scene.logout.run()
        
        let registrationCacheHelper = RegistrationCacheHelper(dependencies: mockDependencies)
        registrationCacheHelper.storeFinishedSteps(
            model: RegistrationModel(),
            statistics: RegistrationStatisticsModel(),
            stepSequence: cachedStepSequence,
            interruptedStep: .name
        )
        XCTAssertNotNil(registrationCacheHelper.cache)

        window = mainCoordinator.start(with: nil)

        XCTAssertNotNil(registrationCacheHelper.cache)
        XCTAssertTrue(navigationController?.topViewController is ContinueRegistrationViewController)
    }
    
    func testStart_WhenRegistrationCacheIsNotNilButContinueRegistrationFlagIsOff_ShouldPresentLandingScreen() {
        let cachedStepSequence: [RegistrationStep] = [.password, .email, .document, .name]
        featureManagerMock.override(with: [.continueRegistration: false,
                                           .dynamicRegistrationStepsSequence: cachedStepSequence])
        Scene.logout.run()
        
        let registrationCacheHelper = RegistrationCacheHelper(dependencies: mockDependencies)
        registrationCacheHelper.storeFinishedSteps(
            model: RegistrationModel(),
            statistics: RegistrationStatisticsModel(),
            stepSequence: cachedStepSequence,
            interruptedStep: .name
        )
        XCTAssertNotNil(registrationCacheHelper.cache)

        window = mainCoordinator.start(with: nil)

        XCTAssertNotNil(registrationCacheHelper.cache)
        XCTAssertTrue(navigationController?.topViewController is SignUpViewController)
    }
    
    func testStart_WhenRegistrationCacheIsNil_ShouldPresentLandingScreen() {
        let registrationCacheHelper = RegistrationCacheHelper(dependencies: mockDependencies)
        registrationCacheHelper.cleanCachedData()
        Scene.logout.run()
        
        window = mainCoordinator.start(with: nil)
        
        XCTAssertNil(registrationCacheHelper.cache)
        XCTAssertTrue(navigationController?.topViewController is SignUpViewController)
    }
    
    func testStart_WhenRegistrationCacheIsNilBecauseStepSequenceChanged_ShouldPresentLandingScreen() {
        let cachedStepSequence: [RegistrationStep] = [.password, .email, .document, .name]
        let newStepSequence: [RegistrationStep] = [.email, .document, .password, .name]
        featureManagerMock.override(with: [.dynamicRegistrationStepsSequence: newStepSequence])
        
        let registrationCacheHelper = RegistrationCacheHelper(dependencies: mockDependencies)
        registrationCacheHelper.storeFinishedSteps(
            model: RegistrationModel(),
            statistics: RegistrationStatisticsModel(),
            stepSequence: cachedStepSequence,
            interruptedStep: .name
        )
        Scene.logout.run()
        
        window = mainCoordinator.start(with: nil)
        
        XCTAssertNil(registrationCacheHelper.cache)
        XCTAssertTrue(navigationController?.topViewController is SignUpViewController)
    }
    
    func testIsLoggedIn() {
        Scene.loggedIn.run()
        window = mainCoordinator.start(with: nil)
        
        XCTAssertTrue(window.rootViewController is ControllerPagination)
    }
    
    func testIsOldRegisterUsernameStep() {
        featureManagerMock.override(key: .dynamicRegistrationSteps, with: false)
        
        Scene.userNeedToCreateUsername.run()
        window = mainCoordinator.start(with: nil)
        
        XCTAssertTrue(navigationController?.topViewController is RegistrationStepViewController)
    }
    
    func testIsNewRegisterUsernameStep() {
        featureManagerMock.override(key: .dynamicRegistrationSteps, with: true)
        
        Scene.userNeedToCreateUsername.run()
        window = mainCoordinator.start(with: nil)
        
        XCTAssertTrue(navigationController?.topViewController is RegistrationUsernameViewController)
    }
    
    func testOpenDeepLink_ShouldPresentLandingScreen() {
        Scene.logout.run()
        deepLinkInWindow(with: Payload.openUrl)

        XCTAssertTrue(navigationController?.topViewController is SignUpViewController)
    }
    
    func testNotificationOpenUrl() {
        Scene.loggedIn.run()
        deepLinkInWindow(with: Payload.openUrl)
                
        // Workarround while there is a feature toggle to the new WebViewController
        XCTAssertTrue(String(describing: mainTabBar.currentController().self).contains("WebViewController"))
        //XCTAssertTrue(mainTabBar.currentController() is WebViewController)
    }
}

// Mark - Helpers
extension MainCoordinatorTest {
    enum Scene {
        case logout
        case loggedIn
        case userNeedToCreateUsername
        
        func run() {
            switch self {
            case .logout:
                User.deleteToken()
                KVStore().setBool(false, with: .isRegisterUsernameStep)
            case .loggedIn:
                User.updateToken("fakeToken")
                KVStore().setBool(false, with: .isRegisterUsernameStep)
            case .userNeedToCreateUsername:
                User.updateToken("fakeToken")
                KVStore().setBool(true, with: .isRegisterUsernameStep)
            }
        }
    }
    
    enum Payload {
        case openUrl
        case sendbirdNotification
        
        func content() -> String {
            switch self {
            case .openUrl:
                return """
                {
                "v": "999.99",
                "i": "loreeeemmmm",
                "title": "title",
                "aps": {
                "alert": "string...",
                "title": "string...."
                },
                "l": ["3", "google.com"]
                }
                """
            case .sendbirdNotification:
                return """
                {
                    "\(UIApplication.LaunchOptionsKey.remoteNotification.rawValue)": {
                        "sendbird": { }
                    }
                }
                """
            }
        }
    }
    
    func deepLinkInWindow(with payload: Payload) {
        let payloadTest = payload.content()
        guard let data = payloadTest.data(using: .utf8), let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [AnyHashable: Any] else {
            XCTFail()
            return
        }
        
        window = mainCoordinator.start(with: json)
    }
    
}
