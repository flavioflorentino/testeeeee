@testable import PicPay
import XCTest

private final class SpyRootViewControllerManager: RootViewControllerManager {
    private(set) var newViewControllerSet: UIViewController?
    
    func changeRootViewController(_ viewController: UIViewController) {
        newViewControllerSet = viewController
    }
}

final class LandingLoaderCoordinatorTest: XCTestCase {
    private let rootViewControllerManager = SpyRootViewControllerManager()
    private lazy var coordinator = LandingLoaderCoordinator(rootViewControllerManager: rootViewControllerManager)
    
    func testPerform_WhenActionIsNext_ShouldPresentLandingScreen() throws {
        coordinator.perform(action: .next)
        
        XCTAssertNotNil(rootViewControllerManager.newViewControllerSet)
    }
}
