@testable import PicPay
import FirebaseRemoteConfig
import XCTest

private final class SpyLandingLoaderPresenter: LandingLoaderPresenting {
    private(set) var didNextStep = false
    
    func didNextStep(action: LandingLoaderAction) {
        didNextStep = true
    }
}

private final class SpyLandingLoaderService: LandingLoaderServicing {
    private(set) var didFetchRemoteConfig = false
    
    func fetchRemoteConfig(timeout: TimeInterval, completionHandler: @escaping () -> Void) {
        didFetchRemoteConfig = true
        completionHandler()
    }
}

final class LandingLoaderViewModelTest: XCTestCase {
    private let service = SpyLandingLoaderService()
    private let presenter = SpyLandingLoaderPresenter()
    private lazy var viewModel = LandingLoaderViewModel(service: service, presenter: presenter)

    func testViewDidLoad_ShouldPresentNextStep() {
        viewModel.viewDidLoad()
        
        XCTAssert(service.didFetchRemoteConfig)
        XCTAssert(presenter.didNextStep)
    }
}
