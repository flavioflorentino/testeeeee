@testable import PicPay
import XCTest

private final class SpyLandingLoaderCoordinator: LandingLoaderCoordinating {
    private(set) var didNextStep = false
    
    var viewController: UIViewController?

    func perform(action: LandingLoaderAction) {
        didNextStep = true
    }
}

final class LandingLoaderPresenterTest: XCTestCase {
    private let coordinator = SpyLandingLoaderCoordinator()
    private lazy var presenter = LandingLoaderPresenter(coordinator: coordinator)
    
    func testDidNextStep_WhenActionIsNext_ShouldPresentNextStep() {
        presenter.didNextStep(action: .next)
        
        XCTAssert(coordinator.didNextStep)
    }
}
