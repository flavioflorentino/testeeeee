import XCTest

enum XCTestError: Error {
    case couldNotUnwrap
    var localizedDescription: String {
        return "Could not unwrap value"
    }
}
extension Optional {
    func safe(_ file: StaticString = #file, line: UInt = #line) throws -> Wrapped {
        guard let safeUnwraped = self else {
            XCTFail("Could not unwrap \(type(of: self))", file: file, line: line)
            throw XCTestError.couldNotUnwrap
        }
        return safeUnwraped
    }
}
