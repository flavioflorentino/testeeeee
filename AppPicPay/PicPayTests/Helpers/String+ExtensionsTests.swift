import XCTest
@testable import PicPay

class String_ExtensionsTests: XCTestCase {

    func testPhoneNumberFormat() {
        XCTAssertEqual("".formatAsPhoneNumberWithHyphen(), "")
        XCTAssertEqual("1".formatAsPhoneNumberWithHyphen(), "1")
        XCTAssertEqual("12".formatAsPhoneNumberWithHyphen(), "12")
        XCTAssertEqual("123".formatAsPhoneNumberWithHyphen(), "123")
        XCTAssertEqual("1234".formatAsPhoneNumberWithHyphen(), "1234")
        XCTAssertEqual("12345".formatAsPhoneNumberWithHyphen(), "1234-5")
        XCTAssertEqual("123456".formatAsPhoneNumberWithHyphen(), "1234-56")
        XCTAssertEqual("1234567".formatAsPhoneNumberWithHyphen(), "1234-567")
        XCTAssertEqual("12345678".formatAsPhoneNumberWithHyphen(), "1234-5678")
        XCTAssertEqual("123456789".formatAsPhoneNumberWithHyphen(), "12345-6789")
        XCTAssertEqual("1234567890".formatAsPhoneNumberWithHyphen(), "12345-6789")
        XCTAssertEqual("12345678901".formatAsPhoneNumberWithHyphen(), "12345-6789")
        
        XCTAssertEqual("12xyz".formatAsPhoneNumberWithHyphen(), "12xy-z")
        XCTAssertEqual("123abc4567".formatAsPhoneNumberWithHyphen(), "123ab-c456")
        XCTAssertEqual("123abc45678".formatAsPhoneNumberWithHyphen(), "123ab-c456")
        XCTAssertEqual("123abc456789".formatAsPhoneNumberWithHyphen(), "123ab-c456")
    }
}
