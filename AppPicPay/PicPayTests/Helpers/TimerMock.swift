import Foundation

final class TimerMock: Timer {
    // MARK: - Variables
    private var block: ((Timer) -> Void)?
    private(set) static var currentTimer: TimerMock?
    
    var invalidateCallCount = 0
    
    // MARK: - Timer
    override func fire() {
        block?(self)
    }
    
    override func invalidate() {
        invalidateCallCount += 1
    }
    
    override class func scheduledTimer(withTimeInterval interval: TimeInterval, repeats: Bool, block: @escaping (Timer) -> Void) -> Timer {
        let timer = TimerMock()
        timer.block = block
        currentTimer = timer
        return timer
    }
}
