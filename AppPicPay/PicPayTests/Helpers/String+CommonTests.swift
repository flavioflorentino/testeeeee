import XCTest
@testable import PicPay

class String_CommonTest: XCTestCase {
    let str = "Raphael Carletti"
    
    func testChopPrefixCount1() {
        XCTAssertTrue(str.chopPrefix() == "aphael Carletti")
    }
    
    func testChopPrefixCountMoreThan1() {
        XCTAssertTrue(str.chopPrefix(5) == "el Carletti")
    }
    
    func testChopSuffixCount1() {
        XCTAssertTrue(str.chopSuffix() == "Raphael Carlett")
    }
    
    func testChopSuffixCountMoreThan1() {
        XCTAssertTrue(str.chopSuffix(5) == "Raphael Car")
    }
}
