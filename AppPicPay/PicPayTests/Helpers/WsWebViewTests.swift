import XCTest
import UIKit

@testable import PicPay

final class WsWebViewTests: XCTestCase {

    func testWsWebView_WhenCalledEndpoint_IsNotNilForSure() {
        XCTAssertNotNil(WsWebViewURL.loanFAQ.endpoint)
        XCTAssertNotNil(WebServiceInterface.apiEndpoint(WsWebViewURL.loanFAQ.endpoint))
    }

}
