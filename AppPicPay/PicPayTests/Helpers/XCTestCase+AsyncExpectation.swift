import XCTest

extension XCTestCase {
    @discardableResult
    func expectation(waitForCondition: @autoclosure @escaping () -> Bool, then completion: @escaping () -> Void = {}) -> XCTestExpectation {
        let predicate = NSPredicate { _, _ -> Bool in
            return waitForCondition()
        }

        return expectation(for: predicate, evaluatedWith: nil) {
            completion()
            return true
        }
    }
}
