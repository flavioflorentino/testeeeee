import XCTest
import UI
import UIKit
@testable import PicPay

class UIColor_ExtensionTests: XCTestCase {
    let lightModeTraitCollection = UITraitCollection(userInterfaceStyle: .light)
    let darkModeTraitCollection = UITraitCollection(userInterfaceStyle: .dark)
    
    func testPpColorBranding100() {
        let expected = #colorLiteral(red: 0.1019607843, green: 0.8901960784, blue: 0.6549019608, alpha: 1) // #1AE3A7
        let ppColorBranding100 = Palette.ppColorBranding100.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorBranding100.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorBranding100.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorBranding100.cgColor)
        }
    }
    
    func testPpColorBranding200() {
        let expected = #colorLiteral(red: 0.07450980392, green: 0.8078431373, blue: 0.4901960784, alpha: 1) // #13CE7D
        let ppColorBranding200 = Palette.ppColorBranding200.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorBranding200.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorBranding200.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorBranding200.cgColor)
        }
    }

    func testPpColorBranding300() {
        let expected = #colorLiteral(red: 0.06666666667, green: 0.7803921569, blue: 0.4352941176, alpha: 1) // #11C76F
        let ppColorBranding300 = Palette.ppColorBranding300.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorBranding300.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorBranding300.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorBranding300.cgColor)
        }
    }
    
    func testPpColorBranding400() {
        let expected = #colorLiteral(red: 0, green: 0.6588235294, blue: 0.1960784314, alpha: 1) // #00A832
        let ppColorBranding400 = Palette.ppColorBranding400.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorBranding400.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorBranding400.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorBranding400.cgColor)
        }
    }
    
    func testPpColorBranding500() {
        let expected = #colorLiteral(red: 0, green: 0.5019607843, blue: 0, alpha: 1) // #008000
        let ppColorBranding500 = Palette.ppColorBranding500.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorBranding500.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorBranding500.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorBranding500.cgColor)
        }
    }
    
    func testPpColorBranding600() {
        let expectedLight = #colorLiteral(red: 0.035, green: 0.247, blue: 0.196, alpha: 1) // #093F32
        let expectedDark = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) //#FFFFFF
        let ppColorBranding600 = Palette.ppColorBranding600.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.cgColor, ppColorBranding600.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expectedDark.cgColor, ppColorBranding600.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expectedLight.cgColor, ppColorBranding600.cgColor)
        }
    }
    
    func testPpColorPositive100() {
        let expected = #colorLiteral(red: 0.1019607843, green: 0.8901960784, blue: 0.6549019608, alpha: 1) // #1AE3A7
        let ppColorPositive100 = Palette.ppColorPositive100.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorPositive100.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorPositive100.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorPositive100.cgColor)
        }
    }
    
    func testPpColorPositive200() {
        let expected = #colorLiteral(red: 0.07450980392, green: 0.8078431373, blue: 0.4901960784, alpha: 1) // #13CE7D
        let ppColorPositive200 = Palette.ppColorPositive200.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorPositive200.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorPositive200.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorPositive200.cgColor)
        }
    }
    
    func testPpColorPositive300() {
        let expected = #colorLiteral(red: 0.06666666667, green: 0.7803921569, blue: 0.4352941176, alpha: 1) // #11C76F
        let ppColorPositive300 = Palette.ppColorPositive300.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorPositive300.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorPositive300.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorPositive300.cgColor)
        }
    }
    
    func testPpColorPositive400() {
        let expected = #colorLiteral(red: 0, green: 0.6588235294, blue: 0.1960784314, alpha: 1) // #00A832
        let ppColorPositive400 = Palette.ppColorPositive400.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorPositive400.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorPositive400.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorPositive400.cgColor)
        }
    }
    
    func testPpColorPositive500() {
        let expected = #colorLiteral(red: 0, green: 0.5019607843, blue: 0, alpha: 1) // #008000
        let ppColorPositive500 = Palette.ppColorPositive500.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorPositive500.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorPositive500.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorPositive500.cgColor)
        }
    }
    
    func testPpColorPositive600() {
        let expectedLight = #colorLiteral(red: 0.137254902, green: 0.2588235294, blue: 0.2274509804, alpha: 1) // #23423A
        let expectedDark = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) // #FFFFFF
        let ppColorPositive600 = Palette.ppColorPositive600.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.cgColor, ppColorPositive600.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expectedDark.cgColor, ppColorPositive600.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expectedLight.cgColor, ppColorPositive600.cgColor)
        }
    }
    
    func testPpColorPositive700() {
        let expectedLight = #colorLiteral(red: 0.6196078431, green: 0.6941176471, blue: 0.6352941176, alpha: 1) // #9EB1A2
        let expectedDark = #colorLiteral(red: 0.5607843137, green: 0.5725490196, blue: 0.6156862745, alpha: 1) // #8F929D
        let ppColorPositive700 = Palette.ppColorPositive700.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.cgColor, ppColorPositive700.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expectedDark.cgColor, ppColorPositive700.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expectedLight.cgColor, ppColorPositive700.cgColor)
        }
    }
    
    func testPpColorNegative100() {
        let expected = #colorLiteral(red: 1, green: 0.6705882353, blue: 0.8117647059, alpha: 1) // #FFABCF
        let ppColorNegative100 = Palette.ppColorNegative100.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorNegative100.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorNegative100.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorNegative100.cgColor)
        }
    }
    
    func testPpColorNegative200() {
        let expected = #colorLiteral(red: 1, green: 0.4274509804, blue: 0.5843137255, alpha: 1) // #FF6D95
        let ppColorNegative200 = Palette.ppColorNegative200.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorNegative200.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorNegative200.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorNegative200.cgColor)
        }
    }
    
    func testPpColorNegative300() {
        let expected = #colorLiteral(red: 1, green: 0.3137254902, blue: 0.4588235294, alpha: 1) // #FF5075
        let ppColorNegative300 = Palette.ppColorNegative300.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorNegative300.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorNegative300.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorNegative300.cgColor)
        }
    }
    
    func testPpColorNegative400() {
        let expected = #colorLiteral(red: 1, green: 0.1254901961, blue: 0.2823529412, alpha: 1) // #FF2048
        let ppColorNegative400 = Palette.ppColorNegative400.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorNegative400.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorNegative400.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorNegative400.cgColor)
        }
    }
    
    func testPpColorNegative500() {
        let expected = #colorLiteral(red: 1, green: 0.03137254902, blue: 0.1568627451, alpha: 1) // #FF0828
        let ppColorNegative500 = Palette.ppColorNegative500.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorNegative500.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorNegative500.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorNegative500.cgColor)
        }
    }
    
    func testPpColorNeutral100() {
        let expected = #colorLiteral(red: 0, green: 0.8431372549, blue: 0.9647058824, alpha: 1) // #00D7F6
        let ppColorNeutral100 = Palette.ppColorNeutral100.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorNeutral100.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorNeutral100.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorNeutral100.cgColor)
        }
    }
    
    func testPpColorNeutral200() {
        let expected = #colorLiteral(red: 0, green: 0.7254901961, blue: 0.9411764706, alpha: 1) // #00B9F0
        let ppColorNeutral200 = Palette.ppColorNeutral200.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorNeutral200.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorNeutral200.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorNeutral200.cgColor)
        }
    }
    
    func testPpColorNeutral300() {
        let expected = #colorLiteral(red: 0, green: 0.6862745098, blue: 0.9333333333, alpha: 1) // #00AFEE
        let ppColorNeutral300 = Palette.ppColorNeutral300.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorNeutral300.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorNeutral300.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorNeutral300.cgColor)
        }
    }
    
    func testPpColorNeutral400() {
        let expected = #colorLiteral(red: 0, green: 0.4509803922, blue: 0.8823529412, alpha: 1) // #0073E1
        let ppColorNeutral400 = Palette.ppColorNeutral400.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorNeutral400.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorNeutral400.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorNeutral400.cgColor)
        }
    }
    
    func testPpColorNeutral500() {
        let expected = #colorLiteral(red: 0, green: 0.1960784314, blue: 0.8039215686, alpha: 1) // #0032CD
        let ppColorNeutral500 = Palette.ppColorNeutral500.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorNeutral500.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorNeutral500.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorNeutral500.cgColor)
        }
    }
    
    func testPpColorAttentive100() {
        let expected = #colorLiteral(red: 0.9960784314, green: 0.9490196078, blue: 0.4509803922, alpha: 1) // #FEF273
        let ppColorAttentive100 = Palette.ppColorAttentive100.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorAttentive100.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorAttentive100.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorAttentive100.cgColor)
        }
    }
    
    func testPpColorAttentive200() {
        let expected = #colorLiteral(red: 0.9882352941, green: 0.8431372549, blue: 0.3215686275, alpha: 1) // #FCD752
        let ppColorAttentive200 = Palette.ppColorAttentive200.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorAttentive200.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorAttentive200.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorAttentive200.cgColor)
        }
    }
    
    func testPpColorAttentive300() {
        let expected = #colorLiteral(red: 0.9843137255, green: 0.7921568627, blue: 0.2588235294, alpha: 1) // #FBCA42
        let ppColorAttentive300 = Palette.ppColorAttentive300.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorAttentive300.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorAttentive300.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorAttentive300.cgColor)
        }
    }
    
    func testPpColorAttentive400() {
        let expected = #colorLiteral(red: 0.968627451, green: 0.6784313725, blue: 0.03529411765, alpha: 1) // #F7AD09
        let ppColorAttentive400 = Palette.ppColorAttentive400.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorAttentive400.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorAttentive400.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorAttentive400.cgColor)
        }
    }
    
    func testPpColorAttentive500() {
        let expected = #colorLiteral(red: 0.9607843137, green: 0.5254901961, blue: 0, alpha: 1) // #F58600
        let ppColorAttentive500 = Palette.ppColorAttentive500.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorAttentive500.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorAttentive500.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorAttentive500.cgColor)
        }
    }
    
    func testPpColorGrayscale000() {
        let expectedLight = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) // #FFFFFF
        let expectedDark = #colorLiteral(red: 0.1176470588, green: 0.137254902, blue: 0.1647058824, alpha: 1) // #1E232A
        let ppColorGrayscale000 = Palette.ppColorGrayscale000.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.cgColor, ppColorGrayscale000.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expectedDark.cgColor, ppColorGrayscale000.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expectedLight.cgColor, ppColorGrayscale000.cgColor)
        }
    }
    
    func testPpColorGrayscale100() {
        let expectedLight = #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9647058824, alpha: 1) // #F4F4F6
        let expectedDark = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) // #000000
        let ppColorGrayscale100 = Palette.ppColorGrayscale100.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.cgColor, ppColorGrayscale100.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expectedDark.cgColor, ppColorGrayscale100.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expectedLight.cgColor, ppColorGrayscale100.cgColor)
        }
    }
    
    func testPpColorGrayscale200() {
        let expectedLight = #colorLiteral(red: 0.9137254902, green: 0.9254901961, blue: 0.9294117647, alpha: 1) // #E9ECED
        let expectedDark = #colorLiteral(red: 0.2392156863, green: 0.2666666667, blue: 0.3176470588, alpha: 1) // #3D4451
        let ppColorGrayscale200 = Palette.ppColorGrayscale200.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.cgColor, ppColorGrayscale200.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expectedDark.cgColor, ppColorGrayscale200.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expectedLight.cgColor, ppColorGrayscale200.cgColor)
        }
    }
    
    func testPpColorGrayscale300() {
        let expectedLight = #colorLiteral(red: 0.8509803922, green: 0.862745098, blue: 0.8901960784, alpha: 1) // #D9DCE3
        let expectedDark = #colorLiteral(red: 0.5607843137, green: 0.5725490196, blue: 0.6156862745, alpha: 1) // #8F929D
        let ppColorGrayscale300 = Palette.ppColorGrayscale300.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.cgColor, ppColorGrayscale300.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expectedDark.cgColor, ppColorGrayscale300.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expectedLight.cgColor, ppColorGrayscale300.cgColor)
        }
    }
    
    func testPpColorGrayscale400() {
        let expectedLight = #colorLiteral(red: 0.5607843137, green: 0.5725490196, blue: 0.6156862745, alpha: 1) // #8F929D
        let expectedDark = #colorLiteral(red: 0.8509803922, green: 0.862745098, blue: 0.8901960784, alpha: 1) // #D9DCE3
        let ppColorGrayscale400 = Palette.ppColorGrayscale400.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.cgColor, ppColorGrayscale400.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expectedDark.cgColor, ppColorGrayscale400.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expectedLight.cgColor, ppColorGrayscale400.cgColor)
        }
    }
    
    func testPpColorGrayscale500() {
        let expectedLight = #colorLiteral(red: 0.2392156863, green: 0.2666666667, blue: 0.3176470588, alpha: 1) // #3D4451
        let expectedDark = #colorLiteral(red: 0.9137254902, green: 0.9254901961, blue: 0.9294117647, alpha: 1) // #E9ECED
        let ppColorGrayscale500 = Palette.ppColorGrayscale500.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.cgColor, ppColorGrayscale500.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expectedDark.cgColor, ppColorGrayscale500.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expectedLight.cgColor, ppColorGrayscale500.cgColor)
        }
    }
    
    func testPpColorGrayscale600() {
        let expectedLight = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) // #000000
        let expectedDark = #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9647058824, alpha: 1) // #F4F4F6
        let ppColorGrayscale600 = Palette.ppColorGrayscale600.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.cgColor, ppColorGrayscale600.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expectedDark.cgColor, ppColorGrayscale600.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expectedLight.cgColor, ppColorGrayscale600.cgColor)
        }
    }
    
    func testPpColorLightGreen() {
        let expected = #colorLiteral(red: 0.6470588235, green: 0.937254902, blue: 0.4941176471, alpha: 1) // #A5EF7E
        let ppColorLightGreen = Palette.ppColorLightGreen.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorLightGreen.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorLightGreen.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorLightGreen.cgColor)
        }
    }
    
    func testPpColorDarkGreen() {
        let expected = #colorLiteral(red: 0.05490196078, green: 0.4823529412, blue: 0.2196078431, alpha: 1) // #0E7B38
        let ppColorDarkGreen = Palette.ppColorDarkGreen.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorDarkGreen.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorDarkGreen.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorDarkGreen.cgColor)
        }
    }
    
    func testTooltip() {
        let expectedLight = #colorLiteral(red: 0, green: 0.6862745098, blue: 0.9333333333, alpha: 1) // #00AFEE
        let expectedDark = #colorLiteral(red: 0.2392156863, green: 0.2666666667, blue: 0.3176470588, alpha: 1) // #3D4451
        let tooltip = Palette.tooltip.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.cgColor, tooltip.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expectedDark.cgColor, tooltip.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expectedLight.cgColor, tooltip.cgColor)
        }
    }
    
    func testBlack() {
        let expected = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) // #000000
        let black = Palette.black.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, black.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, black.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, black.cgColor)
        }
    }
    
    func testWhite() {
        let expected = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) // #FFFFFF
        let white = Palette.white.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, white.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, white.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, white.cgColor)
        }
    }
    
    func testDefaultColor() {
        let expected = UIColor.black
        let ppColorBrandingPlus300 = Palette.ppColorBrandingPlus300.color
        let ppColorBrandingPlus400 = Palette.ppColorBrandingPlus400.color
        let ppColorGrayscalePlus = Palette.ppColorGrayscalePlus.color

        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, ppColorBrandingPlus300.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorBrandingPlus300.resolvedColor(with: darkModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorBrandingPlus400.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorBrandingPlus400.resolvedColor(with: darkModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorGrayscalePlus.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, ppColorGrayscalePlus.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, ppColorBrandingPlus300.cgColor)
            XCTAssertEqual(expected.cgColor, ppColorBrandingPlus400.cgColor)
            XCTAssertEqual(expected.cgColor, ppColorGrayscalePlus.cgColor)
        }
    }
    
    func testGradientColorPpColorBrandingPlus300() {
        let expectedFrom = #colorLiteral(red: 0.04705882353, green: 0.9098039216, blue: 0.4901960784, alpha: 1) // #0CE87D
        let expectedTo = #colorLiteral(red: 0.05882352941, green: 0.6823529412, blue: 0.3803921569, alpha: 1) // #0FAE61
        let ppColorBrandingPlus300Gradient = Palette.ppColorBrandingPlus300.gradientColor
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedFrom.cgColor, ppColorBrandingPlus300Gradient.from.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expectedTo.cgColor, ppColorBrandingPlus300Gradient.to.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expectedFrom.cgColor, ppColorBrandingPlus300Gradient.from.resolvedColor(with: darkModeTraitCollection).cgColor)
            XCTAssertEqual(expectedTo.cgColor, ppColorBrandingPlus300Gradient.to.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expectedFrom.cgColor, ppColorBrandingPlus300Gradient.from.cgColor)
            XCTAssertEqual(expectedTo.cgColor, ppColorBrandingPlus300Gradient.to.cgColor)
        }
    }

    func testGradientColorPpColorBrandingPlus400() {
        let expectedFrom = #colorLiteral(red: 0.1647058824, green: 0.4431372549, blue: 0.431372549, alpha: 1) // #2A716E
        let expectedTo = #colorLiteral(red: 0.0431372549, green: 0.168627451, blue: 0.1803921569, alpha: 1) // #0B2B2E
        let ppColorBrandingPlus400Gradient = Palette.ppColorBrandingPlus400.gradientColor
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedFrom.cgColor, ppColorBrandingPlus400Gradient.from.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expectedTo.cgColor, ppColorBrandingPlus400Gradient.to.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expectedFrom.cgColor, ppColorBrandingPlus400Gradient.from.resolvedColor(with: darkModeTraitCollection).cgColor)
            XCTAssertEqual(expectedTo.cgColor, ppColorBrandingPlus400Gradient.to.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expectedFrom.cgColor, ppColorBrandingPlus400Gradient.from.cgColor)
            XCTAssertEqual(expectedTo.cgColor, ppColorBrandingPlus400Gradient.to.cgColor)
        }
    }

    func testGradientColorPpColorGrayscalePlus() {
        let expectedFrom = #colorLiteral(red: 0.7254901961, green: 0.7254901961, blue: 0.7254901961, alpha: 1) // #B9B9B9
        let expectedTo = #colorLiteral(red: 0.5333333333, green: 0.5333333333, blue: 0.5333333333, alpha: 1) // #888888
        let ppColorGrayscalePlusGradient = Palette.ppColorGrayscalePlus.gradientColor
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedFrom.cgColor, ppColorGrayscalePlusGradient.from.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expectedTo.cgColor, ppColorGrayscalePlusGradient.to.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expectedFrom.cgColor, ppColorGrayscalePlusGradient.from.resolvedColor(with: darkModeTraitCollection).cgColor)
            XCTAssertEqual(expectedTo.cgColor, ppColorGrayscalePlusGradient.to.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expectedFrom.cgColor, ppColorGrayscalePlusGradient.from.cgColor)
            XCTAssertEqual(expectedTo.cgColor, ppColorGrayscalePlusGradient.to.cgColor)
        }
    }

    func testGradientColorDefault() {
        let expectedFrom = UIColor.black
        let expectedTo = UIColor.white
        let defaultGradient = Palette.ppColorLightGreen.gradientColor
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedFrom.cgColor, defaultGradient.from.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expectedTo.cgColor, defaultGradient.to.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expectedFrom.cgColor, defaultGradient.from.resolvedColor(with: darkModeTraitCollection).cgColor)
            XCTAssertEqual(expectedTo.cgColor, defaultGradient.to.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expectedFrom.cgColor, defaultGradient.from.cgColor)
            XCTAssertEqual(expectedTo.cgColor, defaultGradient.to.cgColor)
        }
    }
    
    func testColorcolor() {
        let expectedLight = #colorLiteral(red: 0.1019607843, green: 0.8901960784, blue: 0.6549019608, alpha: 1) // #1AE3A7
        let expectedDark = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) // #FFFFFF
        let customColor = Palette.ppColorBranding100.color(withCustomDark: .ppColorGrayscale000)
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.cgColor, customColor.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expectedDark.cgColor, customColor.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expectedLight.cgColor, customColor.cgColor)
            XCTAssertEqual(expectedDark.cgColor, customColor.cgColor)
        }
    }
    
    func testFacebookColor() {
        let expected = #colorLiteral(red: 0.231372549, green: 0.3490196078, blue: 0.5960784314, alpha: 1) // #3B5998
        let facebook = Palette.SocialMediaColor.facebook.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, facebook.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, facebook.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, facebook.cgColor)
        }
    }
    
    func testTwitterColor() {
        let expected = #colorLiteral(red: 0.3333333333, green: 0.6745098039, blue: 0.9333333333, alpha: 1) // #55ACEE
        let twitter = Palette.SocialMediaColor.twitter.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, twitter.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, twitter.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, twitter.cgColor)
        }
    }
    
    func testWhatsAppColor() {
        let expected = #colorLiteral(red: 0.1450980392, green: 0.8274509804, blue: 0.4, alpha: 1) // #25D366
        let whatsapp = Palette.SocialMediaColor.whatsapp.color
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.cgColor, whatsapp.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expected.cgColor, whatsapp.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expected.cgColor, whatsapp.cgColor)
        }
    }
    
    func testHexColorSendindParameter666666() {
        let expectedLight = #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1) // #666666
        let expectedDark = #colorLiteral(red: 0.9137254902, green: 0.9254901961, blue: 0.9294117647, alpha: 1) // #E9ECED
        let color = Palette.hexColor(with: "#666666")
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.description, color!.resolvedColor(with: lightModeTraitCollection).description)
            XCTAssertEqual(expectedDark.description, color!.resolvedColor(with: darkModeTraitCollection).description)
        } else {
            XCTAssertEqual(expectedLight.description, color!.description)
        }
    }

    func testHexColorSendindParameter989898() {
        let expectedLight = #colorLiteral(red: 0.5960784314, green: 0.5960784314, blue: 0.5960784314, alpha: 1) // #989898
        let expectedDark = #colorLiteral(red: 0.5607843137, green: 0.5725490196, blue: 0.6156862745, alpha: 1) // #8F929D
        let color = Palette.hexColor(with: "#989898")
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.description, color!.resolvedColor(with: lightModeTraitCollection).description)
            XCTAssertEqual(expectedDark.description, color!.resolvedColor(with: darkModeTraitCollection).description)
        } else {
            XCTAssertEqual(expectedLight.description, color!.description)
        }
    }
    
    func testHexColorSendindParameterBBBBBB() {
        let expectedLight = #colorLiteral(red: 0.7333333333, green: 0.7333333333, blue: 0.7333333333, alpha: 1) // #BBBBBB
        let expectedDark = #colorLiteral(red: 0.5607843137, green: 0.5725490196, blue: 0.6156862745, alpha: 1) // #8F929D
        let color = Palette.hexColor(with: "#BBBBBB")
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.description, color!.resolvedColor(with: lightModeTraitCollection).description)
            XCTAssertEqual(expectedDark.description, color!.resolvedColor(with: darkModeTraitCollection).description)
        } else {
            XCTAssertEqual(expectedLight.description, color!.description)
        }
    }
    
    func testHexColorSendindParameter21c25e() {
        let expected = #colorLiteral(red: 0.06666666667, green: 0.7803921569, blue: 0.4352941176, alpha: 1) // #11C76F
        let color = Palette.hexColor(with: "#21C25E")
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.description, color!.resolvedColor(with: lightModeTraitCollection).description)
            XCTAssertEqual(expected.description, color!.resolvedColor(with: darkModeTraitCollection).description)
        } else {
            XCTAssertEqual(expected.description, color!.description)
        }
    }
    
    func testHexColorSendindParameterFFFFFF() {
        let expectedLight = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) // #FFFFFF
        let expectedDark = #colorLiteral(red: 0.1176470588, green: 0.137254902, blue: 0.1647058824, alpha: 1) // #1E232A
        let color = Palette.hexColor(with: "#FFFFFF")
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.description, color!.resolvedColor(with: lightModeTraitCollection).description)
            XCTAssertEqual(expectedDark.description, color!.resolvedColor(with: darkModeTraitCollection).description)
        } else {
            XCTAssertEqual(expectedLight.description, color!.description)
        }
    }
    
    func testHexColorSendindParameterF4F4F6() {
        let expected = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) // #000000
        let color = Palette.hexColor(with: "#F4F4F6")
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.description, color!.resolvedColor(with: lightModeTraitCollection).description)
            XCTAssertEqual(expected.description, color!.resolvedColor(with: darkModeTraitCollection).description)
        } else {
            XCTAssertEqual(expected.description, color!.description)
        }
    }
    
    func testHexColorSendindParameter8F929D() {
        let expectedLight = #colorLiteral(red: 0.5960784314, green: 0.5960784314, blue: 0.5960784314, alpha: 1) // #989898
        let expectedDark = #colorLiteral(red: 0.5607843137, green: 0.5725490196, blue: 0.6156862745, alpha: 1) // #8F929D
        let color = Palette.hexColor(with: "#8F929D")
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.description, color!.resolvedColor(with: lightModeTraitCollection).description)
            XCTAssertEqual(expectedDark.description, color!.resolvedColor(with: darkModeTraitCollection).description)
        } else {
            XCTAssertEqual(expectedLight.description, color!.description)
        }
    }
    
    func testHexColorSendindParameterE9ECED() {
        let expectedLight = #colorLiteral(red: 0.9137254902, green: 0.9254901961, blue: 0.9294117647, alpha: 1) // #E9ECED
        let expectedDark = #colorLiteral(red: 0.2392156863, green: 0.2666666667, blue: 0.3176470588, alpha: 1) // #3D4451
        let color = Palette.hexColor(with: "#E9ECED")
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.description, color!.resolvedColor(with: lightModeTraitCollection).description)
            XCTAssertEqual(expectedDark.description, color!.resolvedColor(with: darkModeTraitCollection).description)
        } else {
            XCTAssertEqual(expectedLight.description, color!.description)
        }
    }
    
    func testHexColorSendindParameterD9DCE3() {
        let expectedLight = #colorLiteral(red: 0.8509803922, green: 0.862745098, blue: 0.8901960784, alpha: 1) // #D9DCE3
        let expectedDark = #colorLiteral(red: 0.5607843137, green: 0.5725490196, blue: 0.6156862745, alpha: 1) // #8F929D
        let color = Palette.hexColor(with: "#D9DCE3")
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.description, color!.resolvedColor(with: lightModeTraitCollection).description)
            XCTAssertEqual(expectedDark.description, color!.resolvedColor(with: darkModeTraitCollection).description)
        } else {
            XCTAssertEqual(expectedLight.description, color!.description)
        }
    }
    
    func testHexColorSendindParameter3D4451() {
        let expectedLight = #colorLiteral(red: 0.9137254902, green: 0.9254901961, blue: 0.9294117647, alpha: 1) // #E9ECED
        let expectedDark = #colorLiteral(red: 0.2392156863, green: 0.2666666667, blue: 0.3176470588, alpha: 1) // #3D4451
        let color = Palette.hexColor(with: "#3D4451")
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.description, color!.resolvedColor(with: lightModeTraitCollection).description)
            XCTAssertEqual(expectedDark.description, color!.resolvedColor(with: darkModeTraitCollection).description)
        } else {
            XCTAssertEqual(expectedLight.description, color!.description)
        }
    }
    
    func testHexColorSendindParameter000000() {
        let expected = #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9647058824, alpha: 1) // #F4F4F6
        let color = Palette.hexColor(with: "#000000")
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.description, color!.resolvedColor(with: lightModeTraitCollection).description)
            XCTAssertEqual(expected.description, color!.resolvedColor(with: darkModeTraitCollection).description)
        } else {
            XCTAssertEqual(expected.description, color!.description)
        }
    }
    
    func testHexColorSendindParameter1E232A() {
        let expectedLight = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) // #FFFFFF
        let expectedDark = #colorLiteral(red: 0.1176470588, green: 0.137254902, blue: 0.1647058824, alpha: 1) // #1E232A
        let color = Palette.hexColor(with: "#1E232A")
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.description, color!.resolvedColor(with: lightModeTraitCollection).description)
            XCTAssertEqual(expectedDark.description, color!.resolvedColor(with: darkModeTraitCollection).description)
        } else {
            XCTAssertEqual(expectedLight.description, color!.description)
        }
    }
    
    func testHexColorWithoutHashtag() {
        let expected = #colorLiteral(red: 0.3058823529, green: 0.3058823529, blue: 0.3058823529, alpha: 1)
        let color = Palette.hexColor(with: "4e4e4e")    
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expected.description, color!.resolvedColor(with: lightModeTraitCollection).description)
            XCTAssertEqual(expected.description, color!.resolvedColor(with: darkModeTraitCollection).description)
        } else {
            XCTAssertEqual(expected.description, color!.description)
        }
    }
    
    func testHexColorFail() {
        let expectedLight = Palette.ppColorGrayscale000.color
        let expectedDark = Palette.ppColorGrayscale600.color
        let color = Palette.hexColor(with: "PicPay")
        
        if #available(iOS 13.0, *) {
            XCTAssertEqual(expectedLight.resolvedColor(with: lightModeTraitCollection).cgColor, color!.resolvedColor(with: lightModeTraitCollection).cgColor)
            XCTAssertEqual(expectedDark.resolvedColor(with: darkModeTraitCollection).cgColor, color!.resolvedColor(with: darkModeTraitCollection).cgColor)
        } else {
            XCTAssertEqual(expectedLight.cgColor, color!.cgColor)
        }
    }
}
