import XCTest
@testable import PicPay

class PPDeeplinkWebviewTest: XCTestCase {
    let validUrls = [
        // picpay.com
        "https://www.picpay.com/",
        "https://www.picpay.com",
        "http://www.picpay.com",
        "https://www.picpay.com/site/central-de-ajuda",
        "https://ajuda.picpay.com/picpay",
        "https://ajuda.picpay.com/picpay/adicionar-e-retirar-dinheiro-1",
        "https://picpay.com/site/privacidade-seguranca#privacidade",
        "https://ajuda.picpay.com/picpay/qual-prazo-de-compensacao-do-boleto",
        // ppay.me
        "https://www.ppay.me/",
        "https://www.ppay.me",
        "http://www.ppay.me",
        "https://www.ppay.me/site/central-de-ajuda",
        "https://ajuda.ppay.me/picpay",
        "https://ajuda.ppay.me/picpay/adicionar-e-retirar-dinheiro-1",
        "https://ajuda.ppay.me/site/privacidade-seguranca#privacidade",
        "https://ajuda.ppay.me/picpay/qual-prazo-de-compensacao-do-boleto"
    ]
    let invalidUrls = [
        "https://www.google.com",
        "http://www.abcd.com",
        "http://abcd.com",
        "https://dfsefe.google.com",
        "http://www.dfsefe.google.com",
        "http://www.aeiou/picpay.com",
        "http://aeiou/picpay.com"
    ]
    
    func createDeeplinkWebview(websiteUrl: String) -> PPDeeplinkWebview {
        let deeplinkUrl = URL(string: "picpay://picpay/webview?url=\(websiteUrl)")!
        return PPDeeplinkWebview(url: deeplinkUrl)
    }
    
    func testDeeplinksWithValidUrls() {
        for url in validUrls {
            let deeplink = createDeeplinkWebview(websiteUrl: url)
            XCTAssertNotNil(deeplink.destinationUrl)
        }
    }
    
    func testDeeplinksWithInvalidUrls() {
        for url in invalidUrls {
            let deeplink = createDeeplinkWebview(websiteUrl: url)
            XCTAssertNil(deeplink.destinationUrl)
        }
    }
}
