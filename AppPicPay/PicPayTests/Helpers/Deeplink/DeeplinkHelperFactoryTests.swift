import XCTest
@testable import PicPay

class DeeplinkHelperFactoryTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }
    
    func testShouldGeneratePaymentDeeplinkHelperForPaymentType() {
        let type = PPDeeplinkType.payment
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is PaymentDeepLinkHelper)
    }
    
    func testShouldGeneratePicpayMeDeeplinkHelperForPicpayMeType() {
        let type = PPDeeplinkType.picpayMe
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is PicpayMeDeepLinkHelper)
    }

    func testShouldGenerateNotificationDeeplinkHelperForNotificationType() {
        let type = PPDeeplinkType.notification
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is NotificationDeeplinkHelper)
    }
    
    func testShouldGenerateCreditDeeplinkHelperForCreditType() {
        let type = PPDeeplinkType.creditPicpay
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is CreditPicpayDeeplinkHelper)
    }
    
    func testShouldGenerateCheckoutDeeplinkHelperForCheckoutType() {
        let type = PPDeeplinkType.checkout
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is CheckoutDeepLinkHelper)
    }
    
    func testShouldGenerateCieloDeeplinkHelperForCieloType() {
        let type = PPDeeplinkType.cielo
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is CieloScannerDeeplinkHelper)
    }
    
    func testShouldGenerateIdentityAnalysisDeeplinkHelperForIdentityAnalysisType() {
        let type = PPDeeplinkType.identityAnalysis
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is IdentityAnalysisDeeplinkHelper)
    }
    
    func testShouldGenerateLinkedAccountsDeeplinkHelperForLinkedAccountsType() {
        let type = PPDeeplinkType.linkedAccounts
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is LinkedAccountsDeeplinkHelper)
    }
    
    func testShouldGenerateUpgradeChecklistDeeplinkHelperForUpgradeChecklistType() {
        let type = PPDeeplinkType.upgradeChecklist
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is UpgradeChecklistDeeplinkHelper)
    }
    
    func testShouldGenerateSettingsDeeplinkHelperForInviteType() {
        let type = PPDeeplinkType.invite
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is SettingsDeeplinkHelper)
    }
    
    func testShouldGenerateSettingsDeeplinkHelperForPromoType() {
        let type = PPDeeplinkType.promo
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is SettingsDeeplinkHelper)
    }
    
    func testShouldGeneratePaymentBarDeeplinkHelperForPlacesType() {
        let type = PPDeeplinkType.places
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is PaymentBarDeeplinkHelper)
    }
    
    func testShouldGeneratePaymentBarDeeplinkHelperForStoreType() {
        let type = PPDeeplinkType.store
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is PaymentBarDeeplinkHelper)
    }
    
    func testShouldGeneratePaymentBarDeeplinkHelperForRechargeType() {
        let type = PPDeeplinkType.recharge
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is PaymentBarDeeplinkHelper)
    }
    
    func testShouldGeneratePaymentBarDeeplinkHelperForSearchType() {
        let type = PPDeeplinkType.search
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is PaymentBarDeeplinkHelper)
    }
    
    func testShouldGenerateBillDeeplinkHelperForBillType() {
        let type = PPDeeplinkType.bill
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is BillDeeplinkHelper)
    }
    
    func testShouldGenerateProfileDeeplinkHelperForProfileType() {
        let type = PPDeeplinkType.profile
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is ProfileDeepLinkHelper)
    }
    
    func testShouldGenerateAccelerateAnalysisDeeplinkHelperForAccelerateAnalysisType() {
        let type = PPDeeplinkType.accelerateAnalysis
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is AccelerateAnalysisDeeplinkHelper)
    }
    
    func testShouldGenerateStudentAccountDeeplinkHelperForStudentAccountType() {
        let type = PPDeeplinkType.studentAccount
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is StudentAccountDeeplinkHelper)
    }
    
    func testShouldGenerateMyQrcodeDeeplinkHelperForMyQrcodeType() {
        let type = PPDeeplinkType.myQrcode
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is MyQrcodeDeeplinkHelper)
    }
    
    func testShouldGenerateCardRegistrationDeeplinkHelperForCardRegistrationType() {
        let type = PPDeeplinkType.cardRegistration
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is CardRegistrationDeeplinkHelper)
    }
    
    func testShouldGenerateWebviewDeeplinkHelperForWebviewType() {
        let type = PPDeeplinkType.webview
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is WebviewDeeplinkHelper)
    }
    
    func testShouldGenerateUserStatementDeeplinkHelperForUserStatementType() {
        let type = PPDeeplinkType.userstatement
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is StatementDeeplinkHelper)
    }
    
    func testShouldReturnNilForTypeNone() {
        let type = PPDeeplinkType.none
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertNil(helper)
    }
    
    func testShouldGenerateIncomeStatementsDeeplinkHelperForIncomeStatementsType() {
        let type = PPDeeplinkType.incomeStatements
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is IncomeStatementsDeeplinkHelper)
    }
    
    func testShouldGenerateReceiptDeeplinkHelperForReceiptType() {
        let type = PPDeeplinkType.receipt
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is ReceiptDeeplinkHelper)
    }
    
    func testShouldGenerateBillTimelineDeeplinkHelperForBillTimelineType() {
        let type = PPDeeplinkType.billTimeline
        let helper = DeeplinkHelperFactory.generateDeeplinkHelper(withType: type)
        
        XCTAssertTrue(helper is BillTimelineDeeplinkHelper)
    }
}
