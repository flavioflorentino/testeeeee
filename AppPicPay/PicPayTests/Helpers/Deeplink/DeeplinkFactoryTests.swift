import XCTest
@testable import PicPay

class DeeplinkFactoryTests: XCTestCase {

    func testShouldGeneratePicpayMeDeeplinkWithPicpayMeURL() {
        let url = URL(string: "https://picpay.me/joao")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deepLink, forDeeplinkType: PPDeeplinkMe.self, andDeeplinkRawType: PPDeeplinkType.picpayMe)
    }
    
    func testShouldGenerateCheckoutDeeplinkWithCheckoutURL() {
        let url = URL(string: "picpay://picpay/checkout/NWQwN2NlOThhODU0NGU3MzI5N2Y2MDYy")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deepLink, forDeeplinkType: PPDeeplinkCheckout.self, andDeeplinkRawType: PPDeeplinkType.checkout)
    }
    
    func testShouldGenerateIdentityAnalysisDeeplinkWithIdentityAnalysisURL() {
        let url = URL(string: "picpay://picpay/identityanalysis")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deepLink, forDeeplinkType: PPDeeplinkIdentityAnalysis.self, andDeeplinkRawType: PPDeeplinkType.identityAnalysis)
    }
    
    func testShouldGenerateCreditDeeplinkWithCreditURL() {
        let url = URL(string: "picpay://picpay/credit/home")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deepLink, forDeeplinkType: PPDeeplinkCreditPicpay.self, andDeeplinkRawType: PPDeeplinkType.creditPicpay)
    }
    
    func testShouldGenerateCieloDeeplinkWithPosURL() {
        let url = URL(string: "picpay://picpay/pos")! //picpay://picpay/POS
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deepLink, forDeeplinkType: PPDeeplinkCielo.self, andDeeplinkRawType: PPDeeplinkType.cielo)
    }
    
    func testShouldGenerateNotificationDeeplinkWithNotificationURL() {
        let url = URL(string: "picpay://picpay/n/boleto")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deepLink, forDeeplinkType: PPDeeplinkNotification.self, andDeeplinkRawType: PPDeeplinkType.notification)
    }
    
    func testShouldGenerateLinkedAccountsDeeplinkWithLinkedAccountsURL() {
        let url = URL(string: "picpay://picpay/tps")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deepLink, forDeeplinkType: PPDeeplinkLinkedAccounts.self, andDeeplinkRawType: PPDeeplinkType.linkedAccounts)
    }
    
    func testShouldGenerateUpgradeAccountDeeplinkWithUpgradeAccountURL() {
        let url = URL(string: "https://app.picpay.com/upgradeaccount")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        
        XCTAssertEqual(deepLink?.type, PPDeeplinkType.upgradeChecklist, "deeplink.type:\(deepLink?.type.rawValue ?? -1) is not equal \(PPDeeplinkType.upgradeChecklist.rawValue)")
    }
    
    func testShouldGenerateInviteDeeplinkWithInviteURL() {
        let url = URL(string: "https://app.picpay.com/invite")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        
        XCTAssertEqual(deepLink?.type, PPDeeplinkType.invite, "deeplink.type:\(deepLink?.type.rawValue ?? -1) is not equal \(PPDeeplinkType.invite.rawValue)")
    }
    
    func testShouldGeneratePromocodeDeeplinkWithPromocodeURL() {
        let url = URL(string: "https://app.picpay.com/promocode")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        
        XCTAssertEqual(deepLink?.type, PPDeeplinkType.promo, "deeplink.type:\(deepLink?.type.rawValue ?? -1) is not equal \(PPDeeplinkType.promo.rawValue)")
    }
    
    
    func testShouldGeneratePlacesDeeplinkWithPlacesURL() {
        let url = URL(string: "https://app.picpay.com/places")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        
        XCTAssertEqual(deepLink?.type, PPDeeplinkType.places, "deeplink.type:\(deepLink?.type.rawValue ?? -1) is not equal \(PPDeeplinkType.places.rawValue)")
    }
    
    func testShouldGenerateStoreDeeplinkWithStoreURL() {
        let url = URL(string: "https://app.picpay.com/store")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deepLink, forDeeplinkType: PPDeeplinkStore.self, andDeeplinkRawType: PPDeeplinkType.store)
    }
    
    func testGenerateDeepLink_WhenIsCaseUser_ShouldCreatePicPayMeDeeplink() throws {
        let url = try XCTUnwrap(URL(string: "picpay://app.picpay.com/user/iowaz"))
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deepLink, forDeeplinkType: PPDeeplinkMe.self, andDeeplinkRawType: PPDeeplinkType.picpayMe)
    }
    
    func testGenerateDeepLink_WhenIsCaseUserWithValue_ShouldCreatePicPayMeDeeplink() throws {
        let url = try XCTUnwrap(URL(string: "picpay://app.picpay.com/user/iowaz/12,00"))
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deepLink, forDeeplinkType: PPDeeplinkMe.self, andDeeplinkRawType: PPDeeplinkType.picpayMe)
    }
    
    func testShouldGenerateRechargeDeeplinkWithRechargeURL() {
        let url = URL(string: "https://app.picpay.com/recharge")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        
        XCTAssertEqual(deepLink?.type, PPDeeplinkType.recharge, "deeplink.type:\(deepLink?.type.rawValue ?? -1) is not equal \(PPDeeplinkType.recharge.rawValue)")
    }
    
    func testShoudlGenerateSearchDeeplinkWithSearchURL() {
        let url = URL(string: "picpay://picpay/search?text=fulano%20silva")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deepLink, forDeeplinkType: PPDeeplinkSearch.self, andDeeplinkRawType: PPDeeplinkType.search)
    }
    
    func testShouldGenerateBillDeeplinkWithBillURL() {
        let url = URL(string: "picpay://picpay/boleto")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        
        XCTAssertEqual(deepLink?.type, PPDeeplinkType.bill, "deeplink.type:\(deepLink?.type.rawValue ?? -1) is not equal \(PPDeeplinkType.bill.rawValue)")
    }
    
    func testShouldGeneratePaymentDeeplinkWithPaymentURL() {
        let url = URL(string: "picpay://picpay/payment?id=17")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deepLink, forDeeplinkType: PPDeeplinkPayment.self, andDeeplinkRawType: PPDeeplinkType.payment)
    }
    
    func testShouldGenerateProfileDeeplinkWithProfileURL() {
        let url = URL(string: "picpay://picpay/profile/17")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deepLink, forDeeplinkType: PPDeeplinkProfile.self, andDeeplinkRawType: PPDeeplinkType.profile)
    }
    
    func testShouldGenerateAccelerateAnalysisDeeplinkWithAccelerateAnalysisURL() {
        let url = URL(string: "picpay://picpay/accelerateAnalysis?transactionId=4509196&transactionType=openP2PTransactionVerification")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deepLink, forDeeplinkType: PPDeeplinkAccelerateAnalysis.self, andDeeplinkRawType: PPDeeplinkType.accelerateAnalysis)
    }
    
    func testShouldGenerateStudentAccountDeeplinkWithStudentAccountURL() {
        let url = URL(string: "picpay://picpay/student_account")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        XCTAssertEqual(deepLink?.type, PPDeeplinkType.studentAccount, "deeplink.type:\(deepLink?.type.rawValue ?? -1) is not equal \(PPDeeplinkType.studentAccount.rawValue)")
    }
    
    func testShouldGenerateWebviewDeeplinkWithWebviewURL() {
        let url = URL(string: "picpay://picpay/webview?url=https://www.picpay.com/)")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        XCTAssertEqual(deepLink?.type, PPDeeplinkType.webview, "deeplink.type:\(deepLink?.type.rawValue ?? -1) is not equal \(PPDeeplinkType.webview.rawValue)")
    }
    
    func testShouldGenerateMyQrcodeDeeplinkWithMyQrcodeURL() {
        let url = URL(string: "picpay://picpay/myqrcode")!
        let deeplink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deeplink, forDeeplinkType: PPDeeplink.self, andDeeplinkRawType: PPDeeplinkType.myQrcode)
    }
    
    func testShouldGenerateCardRegistrationDeeplinkWithCardRegistrationURL() {
        let url = URL(string: "picpay://picpay/cardregistration")!
        let deeplink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deeplink, forDeeplinkType: PPDeeplink.self, andDeeplinkRawType: PPDeeplinkType.cardRegistration)
    }
    
    func testShouldReturnNilForInvalidURL(){
        let url = URL(string: "picpay://picpay/")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        
        XCTAssertNil(deepLink)
    }
    
    func test_ShoudlGenerateSearchMainDeeplink_WithSearchURL() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/search/main?text=fulano%20silva"))
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deepLink, forDeeplinkType: PPDeeplinkSearch.self, andDeeplinkRawType: PPDeeplinkType.search)
    }
    
    func test_ShoudlGenerateSearchConsumersDeeplink_WithSearchURL() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/search/consumers?text=fulano%20silva"))
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deepLink, forDeeplinkType: PPDeeplinkSearch.self, andDeeplinkRawType: PPDeeplinkType.search)
    }
    
    func test_ShoudlGenerateSearchPlacesDeeplink_WithSearchURL() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/search/places?text=fulano%20silva"))
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deepLink, forDeeplinkType: PPDeeplinkSearch.self, andDeeplinkRawType: PPDeeplinkType.search)
    }
    
    func test_ShoudlGenerateSearchStoreDeeplink_WithSearchURL() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/search/store?text=fulano%20silva"))
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deepLink, forDeeplinkType: PPDeeplinkSearch.self, andDeeplinkRawType: PPDeeplinkType.search)
    }
    
    func testShouldGenerateReceiptDeeplinkWithReceiptURL() {
        let url = URL(string: "picpay://picpay/receipt/pav/6026f02a6cf1c700011c9518")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deepLink, forDeeplinkType: PPDeeplink.self, andDeeplinkRawType: PPDeeplinkType.receipt)
    }
    
    func testShouldGenerateBillTimelineDeeplinkWithBillTimelineURL() {
        let url = URL(string: "picpay://picpay/bills/detail/6026f02a6cf1c700011c9518/123")!
        let deepLink = DeeplinkFactory.generateDeepLink(withUrl: url)
        validateDeeplink(deepLink, forDeeplinkType: PPDeeplink.self, andDeeplinkRawType: PPDeeplinkType.billTimeline)
    }
    
    private func validateDeeplink(_ deeplink: PPDeeplink?, forDeeplinkType deeplinkType: PPDeeplink.Type, andDeeplinkRawType rawType: PPDeeplinkType) {
        XCTAssertTrue(deeplink?.isKind(of: deeplinkType) ?? false, "factory returned \(String(describing: deeplink.self)) when it should return \(deeplinkType)")
        XCTAssertEqual(deeplink?.type, rawType, "deeplink.type:\(deeplink?.type.rawValue ?? -1) is not equal \(rawType.rawValue)")
    }
}
