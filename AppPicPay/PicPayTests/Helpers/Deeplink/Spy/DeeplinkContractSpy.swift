import XCTest
import Core

final class DeeplinkContractSpy: DeeplinkContract {
    var shouldOpenUrl: Bool = false
    
    private(set) var openedUrl: URL?
    private(set) var openCount = 0
    @discardableResult
    func open(url: URL) -> Bool {
        if shouldOpenUrl {
            openedUrl = url
            openCount += 1
        }
        return shouldOpenUrl
    }
}
