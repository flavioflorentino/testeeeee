import XCTest
import Core

@testable import PicPay

private class DeeplinkResolverSpy: DeeplinkResolver {
    required init(){}
    
    //MARK: - canHandle
    private(set) var canHandleUrlIsAuthenticatedCallsCount = 0
    private(set) var canHandleUrlIsAuthenticatedReceivedInvocations: [(url: URL, isAuthenticated: Bool)] = []
    var canHandleUrlIsAuthenticatedReturnValue: DeeplinkResolverResult = .handleable
    
    //MARK: - open
    private(set) var openUrlIsAuthenticatedFromCallsCount = 0
    private(set) var openUrlIsAuthenticatedFromReceivedInvocations: [(url: URL, isAuthenticated: Bool)] = []
    var openUrlIsAuthenticatedFromReturnValue: Bool = false
    
    func open(url: URL, isAuthenticated: Bool) -> Bool {
        openUrlIsAuthenticatedFromCallsCount += 1
        openUrlIsAuthenticatedFromReceivedInvocations.append((url: url, isAuthenticated: isAuthenticated))
        return openUrlIsAuthenticatedFromReturnValue
    }
    
    func canHandle(url: URL, isAuthenticated: Bool) -> DeeplinkResolverResult {
        canHandleUrlIsAuthenticatedCallsCount += 1
        canHandleUrlIsAuthenticatedReceivedInvocations.append((url: url, isAuthenticated: isAuthenticated))
        return canHandleUrlIsAuthenticatedReturnValue
    }
}


class DeeplinkResolversHelperTests: XCTestCase {
    private var resolverSpy = DeeplinkResolverSpy()
    
    override func setUp() {
        DeeplinkResolversHelper.resolvers.removeAll()
        DeeplinkResolversHelper.register(resolverSpy)
    }
    
    override func tearDown() {
        DeeplinkResolversHelper.resolvers.removeAll()
    }
    
    func testFindValidResolverWith_WhenResolverCanHandle_ShouldReturnResolver() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/card/credit/request"))
        
        resolverSpy.canHandleUrlIsAuthenticatedReturnValue = .handleable
        let resolverFound = DeeplinkResolversHelper.findValidResolverWith(url)
        XCTAssertNotNil(resolverFound)
        XCTAssertEqual(resolverSpy.canHandleUrlIsAuthenticatedCallsCount, 1)
        XCTAssertEqual(resolverSpy.canHandleUrlIsAuthenticatedReceivedInvocations.last?.url, url)
    }
    
    func testFindValidResolverWith_WhenResolverNeedAuthenticationToHandle_ShouldReturnResolver() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/card/credit/request"))
        
        resolverSpy.canHandleUrlIsAuthenticatedReturnValue = .onlyWithAuth
        let resolverFound = DeeplinkResolversHelper.findValidResolverWith(url)
        XCTAssertNotNil(resolverFound)
        XCTAssertEqual(resolverSpy.canHandleUrlIsAuthenticatedCallsCount, 1)
        XCTAssertEqual(resolverSpy.canHandleUrlIsAuthenticatedReceivedInvocations.last?.url, url)
    }
    
    func testFindValidResolverWith_WhenResolverCannotHandle_ShouldNotReturnResolver() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/card/credit/request"))
        
        resolverSpy.canHandleUrlIsAuthenticatedReturnValue = .notHandleable
        let resolverFound = DeeplinkResolversHelper.findValidResolverWith(url)
        XCTAssertNil(resolverFound)
        XCTAssertEqual(resolverSpy.canHandleUrlIsAuthenticatedCallsCount, 1)
        XCTAssertEqual(resolverSpy.canHandleUrlIsAuthenticatedReceivedInvocations.last?.url, url)
    }
    
    func testResolve_WhenResolverNeedAuthenticationToHandle_ShouldNotResolve() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/card/credit/request"))
        
        resolverSpy.canHandleUrlIsAuthenticatedReturnValue = .onlyWithAuth
        let resolverFound = try XCTUnwrap(DeeplinkResolversHelper.findValidResolverWith(url))
        let resolverResult = DeeplinkResolversHelper.resolve(resolverFound, url: url)
        XCTAssertFalse(resolverResult)
        XCTAssertEqual(resolverSpy.openUrlIsAuthenticatedFromCallsCount, 0)
        XCTAssertTrue(resolverSpy.openUrlIsAuthenticatedFromReceivedInvocations.isEmpty)
    }
    
    func testResolve_WhenResolverCanHandle_ShouldNotResolve() throws {
        let url = try XCTUnwrap(URL(string: "picpay://picpay/card/credit/request"))
        
        resolverSpy.openUrlIsAuthenticatedFromReturnValue = true
        resolverSpy.canHandleUrlIsAuthenticatedReturnValue = .handleable
        let resolverFound = try XCTUnwrap(DeeplinkResolversHelper.findValidResolverWith(url))
        let resolverResult = DeeplinkResolversHelper.resolve(resolverFound, url: url)
        XCTAssertTrue(resolverResult)
        XCTAssertEqual(resolverSpy.openUrlIsAuthenticatedFromCallsCount, 1)
        XCTAssertEqual(resolverSpy.openUrlIsAuthenticatedFromReceivedInvocations.last?.url, url)
    }
}
