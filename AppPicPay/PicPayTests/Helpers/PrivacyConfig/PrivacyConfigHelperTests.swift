import Core
import FeatureFlag
import XCTest
@testable import PicPay

private final class PrivacySettingsApiSpy: PrivacySettingsApiProtocol {
    private(set) var callGetPrivacySettingsCount = 0
    
    func getPrivacySettings(completion: @escaping (PicPayResult<PrivacySettingsModel>) -> Void) {
        callGetPrivacySettingsCount += 1
    }
    
    func setPrivacySettings(configs: [String: Bool], completion: @escaping (PicPayResult<BaseCodableEmptyResponse>) -> Void) { }
}

private final class UpdatePrivacyServiceSpy: UpdatePrivacyServicing {
    private(set) var callCheckRevisionStatusCount = 0
    
    func updateSettings(configs: [String : Bool], reviewedFrom: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) { }
    
    func checkRevisionStatus(completion: @escaping (Result<RevisionStatusModel, ApiError>) -> Void) {
        callCheckRevisionStatusCount += 1
    }
}

final class PrivacyConfigHelperTests: XCTestCase {
    private lazy var kvStoreMock = KVStoreMock()
    private lazy var featureManagerMock = FeatureManagerMock()
    private lazy var dependenciesMock = DependencyContainerMock(kvStoreMock, featureManagerMock)
    private lazy var apiSettingsSpy = PrivacySettingsApiSpy()
    private lazy var updatePrivacyServiceSpy = UpdatePrivacyServiceSpy()
    
    lazy var sut: PrivacyConfigHelper = PrivacyConfigHelper(updatePrivacyService: updatePrivacyServiceSpy,
                                                            dependencies: dependenciesMock,
                                                            apiSettings: apiSettingsSpy)
    
    func testSetPaymentInPrivateConfig_WithoutConfig_ShouldCallServiceMethod() {
        kvStoreMock.set(value: 0, with: KVKey.privacyCconfig)
        sut.setPaymentInPrivateConfig()
        
        XCTAssertEqual(apiSettingsSpy.callGetPrivacySettingsCount, 1)
    }
    
    func testSetPaymentInPrivateConfig_WithConfig_ShouldNotCallServiceMethod() {
        kvStoreMock.set(value: FeedItemVisibilityInt.friends.rawValue, with: KVKey.privacyCconfig)
        sut.setPaymentInPrivateConfig()
        
        XCTAssertEqual(apiSettingsSpy.callGetPrivacySettingsCount, 0)
    }
    
    func testCheckReviewPrivacyAtReceipt_WhenFlagIsDisabledAndUserNotReviewed_ShouldNotCallServiceMethod() {
        featureManagerMock.override(key: FeatureConfig.isReviewPrivacyAvailable, with: false)
        kvStoreMock.set(value: false, with: KVKey.isUserReviewPrivacy)
        
        sut.checkReviewPrivacy(cameFromReceipt: true) { _ in }
        
        XCTAssertEqual(apiSettingsSpy.callGetPrivacySettingsCount, 0)
    }
    
    func testCheckReviewPrivacyAtReceipt_WhenFlagIsDisabledAndUserReviewed_ShouldNotCallServiceMethod() {
        featureManagerMock.override(key: FeatureConfig.isReviewPrivacyAvailable, with: false)
        kvStoreMock.set(value: true, with: KVKey.isUserReviewPrivacy)
        
        sut.checkReviewPrivacy(cameFromReceipt: true) { _ in }
        
        XCTAssertEqual(apiSettingsSpy.callGetPrivacySettingsCount, 0)
    }
    
    func testCheckReviewPrivacyAtReceipt_WhenFlagIsEnableAndUserReviewed_ShouldNotCallServiceMethod() {
        featureManagerMock.override(key: FeatureConfig.isReviewPrivacyAvailable, with: true)
        kvStoreMock.set(value: true, with: KVKey.isUserReviewPrivacy)
        
        sut.checkReviewPrivacy(cameFromReceipt: true) { _ in }
        
        XCTAssertEqual(apiSettingsSpy.callGetPrivacySettingsCount, 0)
    }
    
    func testCheckReviewPrivacyAtReceipt_WhenFlagIsEnableAndUserNotReviewed_ShouldCallServiceMethod() {
        featureManagerMock.override(key: FeatureConfig.isReviewPrivacyAvailable, with: true)
        kvStoreMock.set(value: false, with: KVKey.isUserReviewPrivacy)
        
        sut.checkReviewPrivacy(cameFromReceipt: true) { _ in }
        
        XCTAssertEqual(apiSettingsSpy.callGetPrivacySettingsCount, 1)
    }
    
    func testCheckReviewPrivacyAtHome_WhenFlagIsDisabledAndUserNotReviewed_ShouldNotCallServiceMethod() {
        featureManagerMock.override(key: FeatureConfig.isReviewPrivacyHomeAvailable, with: false)
        kvStoreMock.set(value: false, with: KVKey.isUserReviewPrivacy)
        
        sut.checkReviewPrivacy(cameFromReceipt: false) { _ in }
        
        XCTAssertEqual(updatePrivacyServiceSpy.callCheckRevisionStatusCount, 0)
    }
    
    func testCheckReviewPrivacyAtHome_WhenFlagIsDisabledAndUserReviewed_ShouldNotCallServiceMethod() {
        featureManagerMock.override(key: FeatureConfig.isReviewPrivacyHomeAvailable, with: false)
        kvStoreMock.set(value: true, with: KVKey.isUserReviewPrivacy)
        
        sut.checkReviewPrivacy(cameFromReceipt: false) { _ in }
        
        XCTAssertEqual(updatePrivacyServiceSpy.callCheckRevisionStatusCount, 0)
    }
    
    func testCheckReviewPrivacyAtHome_WhenFlagIsEnableAndUserReviewed_ShouldNotCallServiceMethod() {
        featureManagerMock.override(key: FeatureConfig.isReviewPrivacyHomeAvailable, with: true)
        kvStoreMock.set(value: true, with: KVKey.isUserReviewPrivacy)
        
        sut.checkReviewPrivacy(cameFromReceipt: false) { _ in }
        
        XCTAssertEqual(updatePrivacyServiceSpy.callCheckRevisionStatusCount, 0)
    }
    
    func testCheckReviewPrivacyAtHome_WhenFlagIsEnableAndUserNotReviewed_ShouldCallServiceMethod() {
        featureManagerMock.override(key: FeatureConfig.isReviewPrivacyHomeAvailable, with: true)
        kvStoreMock.set(value: false, with: KVKey.isUserReviewPrivacy)
        
        sut.checkReviewPrivacy(cameFromReceipt: false) { _ in }
        
        XCTAssertEqual(updatePrivacyServiceSpy.callCheckRevisionStatusCount, 1)
    }
}
