//
//  Date+ExtensionsTests.swift
//  PicPayTests
//
//  Created by Luiz Henrique Guimaraes on 05/02/19.
//

import XCTest
@testable import PicPay

class Date_ExtensionsTests: XCTestCase {
    
    struct FormattedTimeIntervalTest {
        let timeInterval: TimeInterval
        let expectedResult: String
        
        init(interval: TimeInterval, result: String){
            self.timeInterval = interval
            self.expectedResult = result
        }
    }
    
    func testSecondsIntervalFromDateWithArbitraryDate() {
        let arbitrarySecondsInterval = Int.random(in: 10..<10000)
        
        let now = Date()
        let dateFinish = Date(timeInterval: TimeInterval(arbitrarySecondsInterval), since: now)
        
        let seconds = dateFinish.seconds(from: now)
        XCTAssertEqual(seconds, arbitrarySecondsInterval)
    }
    
    func testWhenSecondsIntervalExpectMinutesAndSencodsFormat() {
        let now = Date()
        
        let tests:[FormattedTimeIntervalTest] = [
            FormattedTimeIntervalTest(interval: 2, result: "00:02"),
            FormattedTimeIntervalTest(interval: 23, result: "00:23"),
            FormattedTimeIntervalTest(interval: 35, result: "00:35"),
            FormattedTimeIntervalTest(interval: 59, result: "00:59"),
            ]
        
        tests.forEach { (test) in
            let dateFinish = Date(timeInterval: test.timeInterval, since: now)
            let formattedSecondsWithMinutes = dateFinish.formattedTime(from: now)
            
            XCTAssertEqual(formattedSecondsWithMinutes, test.expectedResult)
        }
    }
    
    func testWhenMinutesIntervalExpectMinutesAndSencodsFormat() {
        let now = Date()
        
        let tests:[FormattedTimeIntervalTest] = [
            FormattedTimeIntervalTest(interval: 193, result: "03:13"),
            FormattedTimeIntervalTest(interval: 560, result: "09:20"),
            FormattedTimeIntervalTest(interval: 1346, result: "22:26"),
            FormattedTimeIntervalTest(interval: 3550, result: "59:10"),
            ]
        
        tests.forEach { (test) in
            let dateFinish = Date(timeInterval: test.timeInterval, since: now)
            let formattedSecondsWithMinutes = dateFinish.formattedTime(from: now)
            
            XCTAssertEqual(formattedSecondsWithMinutes, test.expectedResult)
        }
    }
    
    func testWhenHoursIntervalExpectHoursAndMinutesAndSencodsFormat() {
        let now = Date()
        let dateFinish = Date(timeInterval: 10985, since: now)
        
        let formattedSecondsWithHours = dateFinish.formattedTime(from: now)
        XCTAssertEqual(formattedSecondsWithHours, "03:03:05")
    }
}
