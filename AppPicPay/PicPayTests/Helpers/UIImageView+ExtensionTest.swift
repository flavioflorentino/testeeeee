import OHHTTPStubs
import XCTest
import UIKit

@testable import PicPay

final class UIImageViewExtensionTest: XCTestCase {
    private let timeout: TimeInterval = 5.0

    private let image = UIImage(named: "picpaySmall")
    private let imageUrl = URL(string: "https://picsum.photos/1024")

    private let badRequestUrl = URL(string: "https://bad-request.com")
    private let placeholderImage = UIImage(named: "document_placeholder")
    
    override func setUp() {
        super.setUp()

        URLCache.shared.removeAllCachedResponses()

        if let imageUrl = imageUrl {
            stub(condition: isAbsoluteURLString(imageUrl.absoluteString)) { _ in
                let data = self.image?.pngData() ?? Data()
                return OHHTTPStubsResponse(data: data, statusCode: 200, headers: nil)
            }
        }

        if let badRequestUrl = badRequestUrl {
            stub(condition: isAbsoluteURLString(badRequestUrl.absoluteString)) { _ in
                return OHHTTPStubsResponse(data: Data(), statusCode: 400, headers: nil)
            }
        }
    }
    
    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
        URLCache.shared.removeAllCachedResponses()

        super.tearDown()
    }

    func testSetImage_WhenRequestWasSucceeded_ShouldSetImage() {
        let testView = UIImageView()
        XCTAssertNil(testView.image)

        testView.setImage(url: imageUrl)
        self.expectation(waitForCondition: testView.image != nil) {
            AssertImageEqual(testView.image, self.image)
        }
        waitForExpectations(timeout: timeout)
    }
    
    func testSetImage_WhenRequestWasFailed_ShouldSetPlaceholder() {
        let testView = UIImageView()
        XCTAssertNil(testView.image)

        testView.setImage(url: badRequestUrl, placeholder: placeholderImage)

        self.expectation(waitForCondition: testView.image != nil) {
            AssertImageEqual(testView.image, self.placeholderImage)
        }
        waitForExpectations(timeout: timeout)
    }
    
    func testShowActivityIndicator_WhenTrue_ShouldShowActivityIndicator() {
        let testView = UIImageView()
        
        XCTAssertNil(testView.image)
        
        testView.showActivityIndicator = true
        XCTAssert(testView.showActivityIndicator)

        testView.setImage(url: imageUrl)

        self.expectation(waitForCondition: testView.image != nil) {
            XCTAssertFalse(testView.showActivityIndicator)
        }
        waitForExpectations(timeout: timeout)
    }

    func testSetImage_WhenCalledConcurrent_ShouldNotLockMainThread() {
        let testView = UIImageView()
        XCTAssertNil(testView.image)

        self.expectation(waitForCondition: testView.image != nil)
        let setImage = {
            testView.setImage(url: self.imageUrl)
        }

        DispatchQueue.global().async(execute: setImage)
        DispatchQueue.global().async(execute: setImage)
        DispatchQueue.global().async(execute: setImage)

        waitForExpectations(timeout: timeout)
    }

    func testGetImage_WhenCalledConcurrent_ShouldNotLockMainThread() {
        let testView = UIImageView()
        XCTAssertNil(testView.image)

        self.expectation(waitForCondition: testView.image != nil)

        let setImage = {
            testView.setImage(url: self.imageUrl)
        }

        let printImage = {
            let image = testView.image
            print(String(describing: image))
        }

        DispatchQueue.global().async(execute: setImage)
        DispatchQueue.main.async(execute: printImage)
        DispatchQueue.global().async(execute: setImage)
        DispatchQueue.main.async(execute: printImage)

        waitForExpectations(timeout: timeout)
    }

    func testCancelRequest_WhenGetOrSetImageConcurrent_ShouldNotInvalidateSession() {
        let testView = UIImageView()
        XCTAssertNil(testView.image)

        self.expectation(waitForCondition: testView.image != nil)

        let setImage = {
            testView.setImage(url: self.imageUrl)
        }

        let printImage = {
            let image = testView.image
            print(String(describing: image))
        }

        let cancelRequest = {
            testView.cancelRequest()
        }

        DispatchQueue.global().async(execute: setImage)
        DispatchQueue.main.async(execute: cancelRequest)
        DispatchQueue.global().async(execute: setImage)
        DispatchQueue.main.async(execute: printImage)

        waitForExpectations(timeout: timeout)
    }
}
