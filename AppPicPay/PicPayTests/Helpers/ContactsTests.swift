//
//  ContactsTests.swift
//  PicPayTests
//
//  Created by Douglas Santos on 10/07/19.
//

import XCTest
import Contacts
import OHHTTPStubs
@testable import PicPay

class ContactsTests: XCTestCase {

    var sut: Contacts!
    
    override func setUp() {
        super.setUp()
        sut = Contacts()
        ConsumerManager.shared.resetAllLocalConsumerInformation()
    }

    override func tearDown() {
        super.tearDown()
        sut = nil
    }
    
    func testShouldNotReceiveContactsWhenIdentifyWithUserNotAuthenticated() {
        let expect = expectation(description: "not authenticated expectation")
        sut.identifyPicpayContacts { (status) in
            XCTAssertEqual(status, .notAuthenticated)
            expect.fulfill()
        }
        waitForExpectations(timeout: 1, handler: nil)
    }
}


