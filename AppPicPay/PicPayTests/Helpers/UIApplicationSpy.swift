@testable import PicPay
import Foundation

final class UIApplicationSpy: UIApplicationContract {
    var openUrl: URL?
    var canOpenUrl: Bool?
    
    func open(_ url: URL, options: [UIApplication.OpenExternalURLOptionsKey: Any], completionHandler completion: ((Bool) -> Void)?) {
        openUrl = url
    }
    
    func canOpenURL(_ url: URL) -> Bool {
        return canOpenUrl ?? false
    }
}
