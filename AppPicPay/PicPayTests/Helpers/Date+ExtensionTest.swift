import XCTest
@testable import PicPay

class Date_ExtensionTest: XCTestCase {
    func testFormmaterddMMyyyy() {
        let espected = "23/02/1991"
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "pt_BR_POSIX")
        formatter.dateFormat = "dd/MM/yyyy"
        
        let date = formatter.date(from: espected)
        XCTAssertEqual(espected, date?.stringFormatted(with: .ddMMyyyy))
    }
    
    func testFormatterddDeMMM() {
        let espected = "23 de Fevereiro"
        let dateString = "23/02/1991"
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "pt_BR_POSIX")
        formatter.dateFormat = "dd/MM/yyyy"
        
        let date = formatter.date(from: dateString)
        XCTAssertEqual(espected, date?.stringFormatted(with: .ddDeMMMM))
    }
}
