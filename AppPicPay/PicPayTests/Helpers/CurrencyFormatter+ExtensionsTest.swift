import XCTest
import CoreLegacy
@testable import PicPay

class CurrencyFormatter_ExtensionsTest: XCTestCase {
    func testtwoDecimalsStringWithManydecimals() {
        let num = NSNumber(value: 1.33957836755)
        let expectedString = "1,33"
        let result = CurrencyFormatter.twoDecimalsString(from: num)!
        XCTAssertEqual(expectedString, result)
    }
    
    func testtwoDecimalsStringWithOneDecimal() {
        let num = NSNumber(value: 1.9)
        let expectedString = "1,90"
        let result = CurrencyFormatter.twoDecimalsString(from: num)!
        XCTAssertEqual(expectedString, result)
    }
    
    func testtwoDecimalsStringWithNoDecimals() {
        let num = NSNumber(value: 0)
        let expectedString = "0,00"
        let result = CurrencyFormatter.twoDecimalsString(from: num)!
        XCTAssertEqual(expectedString, result)
    }
    
    func testtwoDecimalsStringWithSeparators() {
        let num = NSNumber(value: 145689574896.98)
        let expectedString = "145.689.574.896,98"
        let result = CurrencyFormatter.twoDecimalsString(from: num)!
        XCTAssertEqual(expectedString, result)
    }
}
