import XCTest
@testable import PicPay
import Core
import UIKit

class UIDevice_ExtensionTest: XCTestCase {
    typealias Dependencies = HasKeychainManagerPersisted
    let dependencies: Dependencies = DependencyContainerMock()
    
    func testGivenInstallationIdSavedShouldReturnSavedInstallationId() {
        let installationId = "1C083846-56C8-4CFD-86B9-5A33FC93B343"
        KVStore().setString(installationId, with: .installation_id)
        
        XCTAssertEqual(UIDevice.current.installationId, installationId)
    }
    
    func testGivenNoInstallationIdSavedShouldReturnIdentifierForVendor() {
        KVStore().removeObjectFor(.installation_id)
        
        let installationId = UIDevice.current.installationId
        XCTAssertEqual(UIDevice.current.identifierForVendor?.uuidString, installationId)
    }
    
    func testPicPayDeviceIdShouldReturnPicPayDeviceIdFromKeyChain() {
        let deviceId = "E24ED963-C239-4048-8C95-368ADC53E031"
        let keychain = dependencies.keychainWithPersistent
        keychain.set(key: KeychainKey.ppDeviceId, value: deviceId)
        XCTAssertEqual(UIDevice.current.picpayDeviceId(dependencies: dependencies), deviceId)
    }
}
