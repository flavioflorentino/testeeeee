@testable import PicPay
import Foundation
import XCTest

final class HelpCenterOptionsTest: XCTestCase {
    func testAdicaoViaCartaoDeDebitoCaixaOption() {
        XCTAssertEqual(HelpCenterOptions.adicaoViaCartaoDeDebitoCaixa.productionFaqOption, .category(id: "360003814672"))
        XCTAssertEqual(HelpCenterOptions.adicaoViaCartaoDeDebitoCaixa.developmentFaqOption, .home)
    }

    func testAdicionarDinheiroNoPicpayOption() {
        XCTAssertEqual(HelpCenterOptions.adicionarDinheiroNoPicpay.productionFaqOption, .category(id: "360003814572"))
        XCTAssertEqual(HelpCenterOptions.adicionarDinheiroNoPicpay.developmentFaqOption, .home)
    }

    func testComoAlteroMeuEnderecoVinculadoAoPicpayCardOption() {
        XCTAssertEqual(HelpCenterOptions.comoAlteroMeuEnderecoVinculadoAoPicpayCard.productionFaqOption, .category(id: "360003297112"))
        XCTAssertEqual(HelpCenterOptions.comoAlteroMeuEnderecoVinculadoAoPicpayCard.developmentFaqOption, .home)
    }

    func testComoCalcularMeuPatrimonioOption() {
        XCTAssertEqual(HelpCenterOptions.comoCalcularMeuPatrimonio.productionFaqOption, .category(id: "360045897791"))
        XCTAssertEqual(HelpCenterOptions.comoCalcularMeuPatrimonio.developmentFaqOption, .home)
    }

    func testDuvidasSobreOMeuEmprestimoPessoalOption() {
        XCTAssertEqual(HelpCenterOptions.duvidasSobreOMeuEmprestimoPessoal.productionFaqOption, .category(id: "360003819012"))
        XCTAssertEqual(HelpCenterOptions.duvidasSobreOMeuEmprestimoPessoal.developmentFaqOption, .home)
    }

    func testFaqVirtualCardHelperOption() {
        XCTAssertEqual(HelpCenterOptions.faqVirtualCardHelper.productionFaqOption, .category(id: "360003814572"))
        XCTAssertEqual(HelpCenterOptions.faqVirtualCardHelper.developmentFaqOption, .home)
    }

    func testFaqVirtualCardRegistrationOption() {
        XCTAssertEqual(HelpCenterOptions.faqVirtualCardRegistration.productionFaqOption, .category(id: "360003814572"))
        XCTAssertEqual(HelpCenterOptions.faqVirtualCardRegistration.developmentFaqOption, .home)
    }

    func testFuncaoDebitoOption() {
        XCTAssertEqual(HelpCenterOptions.funcaoDebito.productionFaqOption, .category(id: "360003297112"))
        XCTAssertEqual(HelpCenterOptions.funcaoDebito.developmentFaqOption, .home)
    }

    func testJaPagueiOBoletoOption() {
        XCTAssertEqual(HelpCenterOptions.jaPagueiOBoleto.productionFaqOption, .category(id: "360003297072"))
        XCTAssertEqual(HelpCenterOptions.jaPagueiOBoleto.developmentFaqOption, .home)
    }

    func testNaoRecebiOSmsComOCodigoParaCompletarMeuCadastroOption() {
        XCTAssertEqual(HelpCenterOptions.naoRecebiOSmsComOCodigoParaCompletarMeuCadastro.productionFaqOption, .category(id: "360003433711"))
        XCTAssertEqual(HelpCenterOptions.naoRecebiOSmsComOCodigoParaCompletarMeuCadastro.developmentFaqOption, .home)
    }

    func testOQueEValidacaoDeIdentidadeOption() {
        XCTAssertEqual(HelpCenterOptions.oQueEValidacaoDeIdentidade.productionFaqOption, .category(id: "360003433711"))
        XCTAssertEqual(HelpCenterOptions.oQueEValidacaoDeIdentidade.developmentFaqOption, .home)
    }

    func testOfertaCreditoPessoalOption() {
        XCTAssertEqual(HelpCenterOptions.ofertaCreditoPessoal.productionFaqOption, .category(id: "360003819012"))
        XCTAssertEqual(HelpCenterOptions.ofertaCreditoPessoal.developmentFaqOption, .home)
    }

    func testPagamentoDeIptuEOutrosTributosOption() {
        XCTAssertEqual(HelpCenterOptions.pagamentoDeIptuEOutrosTributos.productionFaqOption, .category(id: "360003297072"))
        XCTAssertEqual(HelpCenterOptions.pagamentoDeIptuEOutrosTributos.developmentFaqOption, .home)
    }

    func testPasswordOption() {
        XCTAssertEqual(HelpCenterOptions.password.productionFaqOption, .category(id: "360003433711"))
        XCTAssertEqual(HelpCenterOptions.password.developmentFaqOption, .home)
    }

    func testPicpayCardOption() {
        XCTAssertEqual(HelpCenterOptions.picpayCard.productionFaqOption, .category(id: "360003297112"))
        XCTAssertEqual(HelpCenterOptions.picpayCard.developmentFaqOption, .home)
    }

    func testPorQueMeuPedidoNaoFoiAceitoOption() {
        XCTAssertEqual(HelpCenterOptions.porQueMeuPedidoNaoFoiAceito.productionFaqOption, .category(id: "360003819012"))
        XCTAssertEqual(HelpCenterOptions.porQueMeuPedidoNaoFoiAceito.developmentFaqOption, .home)
    }

    func testPorQueNaoConsigoFazerOUpgradeDeContaOption() {
        XCTAssertEqual(HelpCenterOptions.porQueNaoConsigoFazerOUpgradeDeConta.productionFaqOption, .category(id: "360003433711"))
        XCTAssertEqual(HelpCenterOptions.porQueNaoConsigoFazerOUpgradeDeConta.developmentFaqOption, .home)
    }

    func testPorQuePrecisoAceitarOContratoDeAdesaoOption() {
        XCTAssertEqual(HelpCenterOptions.porQuePrecisoAceitarOContratoDeAdesao.productionFaqOption, .category(id: "360003819012"))
        XCTAssertEqual(HelpCenterOptions.porQuePrecisoAceitarOContratoDeAdesao.developmentFaqOption, .home)
    }

    func testPorQuePrecisoVerificarMeuCartaoOption() {
        XCTAssertEqual(HelpCenterOptions.porQuePrecisoVerificarMeuCartao.productionFaqOption, .category(id: "360003433711"))
        XCTAssertEqual(HelpCenterOptions.porQuePrecisoVerificarMeuCartao.developmentFaqOption, .home)
    }

    func testRetirarDinheiroDoPicpayOption() {
        XCTAssertEqual(HelpCenterOptions.retirarDinheiroDoPicpay.productionFaqOption, .category(id: "360003814672"))
        XCTAssertEqual(HelpCenterOptions.retirarDinheiroDoPicpay.developmentFaqOption, .home)
    }

    func testSaqueNoBanco24HorasOption() {
        XCTAssertEqual(HelpCenterOptions.saqueNoBanco24Horas.productionFaqOption, .category(id: "360003814672"))
        XCTAssertEqual(HelpCenterOptions.saqueNoBanco24Horas.developmentFaqOption, .home)
    }

    func testSimulacaoCreditoPessoalOption() {
        XCTAssertEqual(HelpCenterOptions.simulacaoCreditoPessoal.productionFaqOption, .category(id: "360003819012"))
        XCTAssertEqual(HelpCenterOptions.simulacaoCreditoPessoal.developmentFaqOption, .home)
    }

    func testTaxasELimitesDePagamentosOption() {
        XCTAssertEqual(HelpCenterOptions.taxasELimitesDePagamentos.productionFaqOption, .category(id: "360003322111"))
        XCTAssertEqual(HelpCenterOptions.taxasELimitesDePagamentos.developmentFaqOption, .home)
    }

    func testTentaramEntregarMeuCartaoENinguemRecebeuOption() {
        XCTAssertEqual(HelpCenterOptions.tentaramEntregarMeuCartaoENinguemRecebeu.productionFaqOption, .category(id: "360003297112"))
        XCTAssertEqual(HelpCenterOptions.tentaramEntregarMeuCartaoENinguemRecebeu.developmentFaqOption, .home)
    }

    func testUpgradeDeContaOption() {
        XCTAssertEqual(HelpCenterOptions.upgradeDeConta.productionFaqOption, .category(id: "360003433711"))
        XCTAssertEqual(HelpCenterOptions.upgradeDeConta.developmentFaqOption, .home)
    }
}
