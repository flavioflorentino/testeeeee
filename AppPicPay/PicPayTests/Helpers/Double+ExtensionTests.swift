import XCTest
@testable import PicPay

class Double_ExtensionTests: XCTestCase {
    let priceUnformatted: Double = 25.566
    func testPriceFormat() {
        XCTAssertTrue(priceUnformatted.getFormattedPrice() == "R$ 25,57")
    }
    
    func testPriceFormatFail() {
        XCTAssertFalse(priceUnformatted.getFormattedPrice() == "R$ 25,58")
    }
}
