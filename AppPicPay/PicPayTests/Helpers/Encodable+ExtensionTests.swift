import XCTest
@testable import PicPay

class Encodable_ExtensionTest: XCTestCase {
    private struct MockModel: Codable {
        let id: Int
        let name: String
        let startDate: String
        let value: Double
        let sellValue: Double?
        let isActive: Bool
    }
    
    private let mock1 = MockModel(id: 1, name: "Passat Tunado", startDate: "05/05/1990", value: 7500.99, sellValue: nil, isActive: true)
    private let mock2 = MockModel(id: 2, name: "Ford Ka", startDate: "01/12/2012", value: 9000, sellValue: 3500, isActive: false)
    
    func testEncodableToDictSuccessSnakeCase() {
        guard let dict = mock1.toDictionary() else {
            XCTFail("Was expecting a dictionary")
            return
        }
        
        guard let id = dict["id"] as? Int,
            let name = dict["name"] as? String,
            let startDate = dict["start_date"] as? String,
            let value = dict["value"] as? Double,
            let isActive = dict["is_active"] as? Bool else {
                XCTFail("Was expecting to have all keys")
                return
        }
        
        XCTAssertEqual(id, 1)
        XCTAssertEqual(name, "Passat Tunado")
        XCTAssertEqual(startDate, "05/05/1990")
        XCTAssertEqual(value, 7500.99)
        XCTAssertEqual(isActive, true)
    }
    
    func testEncodableToDictSuccessCamelCase() {
        guard let dict = mock2.toDictionary(strategy: .useDefaultKeys) else {
            XCTFail("Was expecting a dictionary")
            return
        }
        
        guard let id = dict["id"] as? Int,
            let name = dict["name"] as? String,
            let startDate = dict["startDate"] as? String,
            let value = dict["value"] as? Double,
            let sellValue = dict["sellValue"] as? Double,
            let isActive = dict["isActive"] as? Bool else {
                XCTFail("Was expecting to have all keys")
                return
        }
        
        XCTAssertEqual(id, 2)
        XCTAssertEqual(name, "Ford Ka")
        XCTAssertEqual(startDate, "01/12/2012")
        XCTAssertEqual(sellValue, 3500.00)
        XCTAssertEqual(value, 9000)
        XCTAssertEqual(isActive, false)
    }
}
