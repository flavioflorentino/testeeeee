import XCTest
@testable import PicPay

class JSONDecoder_ExtensionTest: XCTestCase {
    let jsonWithNestedData = """
{
"data": {
"rating": 1,
"isEnabled": true,
"name": "Burgão do Zé"
}
}
"""
    
    let jsonWithDataOnRoot = """
{
"rating": 1,
"isEnabled": true,
"name": "Burgão do Zé"
}
"""
    
    
    func testIso8601Time() {
        XCTAssertNoThrow(try MockCodable<TestObject>().loadCodableObject(resource: "TextObjectValidDateISO8601Time"))
    }
    
    func testIso8601noTime() {
        XCTAssertNoThrow(try MockCodable<TestObject>().loadCodableObject(resource: "TextObjectValidDateISO8601noTime"))
    }
    
    func testIso8601noFS() {
        XCTAssertNoThrow(try MockCodable<TestObject>().loadCodableObject(resource: "TextObjectValidDateISO8601noFS"))
    }
    
    func testInvalidDate() {
        XCTAssertThrowsError(try MockCodable<TestObject>().loadCodableObject(resource: "TextObjectInvalidDate"))
    }
    
    func testDecodeWithNestedData() {
        struct NestedStore: Codable, NestedDecodable {
            let rating: Int
            let isEnabled: Bool
            let name: String
        }
        
        guard let data = jsonWithNestedData.data(using: .utf8) else {
            XCTFail("Was not able to convert String to Data")
            return
        }
        
        guard let store = try? JSONDecoder().decodeNestedData(NestedStore.self, from: data) else {
            XCTFail("Failed to instantiate Store with Nested Json")
            return
        }
        
        XCTAssertEqual(store.rating, 1)
        XCTAssertEqual(store.isEnabled, true)
        XCTAssertEqual(store.name, "Burgão do Zé")
    }
    
    func testDecodeWithoutData() {
        struct Store: Codable {
            let rating: Int
            let isEnabled: Bool
            let name: String
        }
        
        guard let data = jsonWithDataOnRoot.data(using: .utf8) else {
            XCTFail("Was not able to convert String to Data")
            return
        }
        
        guard let store = try? JSONDecoder().decodeNestedData(Store.self, from: data) else {
            XCTFail("Failed to instantiate Store with Nested Json")
            return
        }
        
        XCTAssertEqual(store.rating, 1)
        XCTAssertEqual(store.isEnabled, true)
        XCTAssertEqual(store.name, "Burgão do Zé")
    }
}

extension JSONDecoder_ExtensionTest {
    struct TestObject: Codable {
        let text: String
        let date: Date
    }
}
