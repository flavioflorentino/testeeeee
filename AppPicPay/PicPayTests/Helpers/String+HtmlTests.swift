import XCTest
@testable import PicPay

class String_HtmlTests: XCTestCase {
    let urlString = "Foo &#xA9; bar &#x1D306; baz &#x2603; qux"
    
    func testDecodeHTMLEntities() {
       XCTAssertTrue(urlString.decodingHTMLEntities() == "Foo © bar 𝌆 baz ☃ qux")
    }
    
    func testDecodeHTMLEntitiesFail() {
        XCTAssertFalse(urlString.decodingHTMLEntities() == "")
    }

}
