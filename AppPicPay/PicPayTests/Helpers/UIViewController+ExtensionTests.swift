import XCTest
@testable import PicPay
import UIKit

class UIViewController_ExtensionTests: XCTestCase {
    let typeList = [SignUpViewController.self]
    
    func testIsOfType() {
        let controller = SignUpViewController(viewModel: SignUpViewModelMock())
        XCTAssertTrue(controller.isOfType(inList: typeList))
    }
    
    func testIsNotOfType() {
        let viewModel = PhoneVerificationCodeViewModelMock()
        let coordinator = PhoneVerificationCodeCoordinatorMock()
        let controller = PhoneVerificationCodeViewController(viewModel: viewModel, coordinator: coordinator)
        XCTAssertTrue(controller.isNotOfType(inList: typeList))
    }
    
    func testIsOfTypeFail() {
        let viewModel = PhoneVerificationCodeViewModelMock()
        let coordinator = PhoneVerificationCodeCoordinatorMock()
        let controller = PhoneVerificationCodeViewController(viewModel: viewModel, coordinator: coordinator)
        XCTAssertFalse(controller.isOfType(inList: typeList))
    }
    
    func testIsNotOfTypeFail() {
        let controller = SignUpViewController(viewModel: SignUpViewModelMock())
        XCTAssertFalse(controller.isNotOfType(inList: typeList))
    }
}

private final class SignUpViewModelMock: SignUpViewModelInputs {
    func viewDidLoad() {}
    func didTapRegisterButton() {}
    func didTapLoginButton() {}
    func didTapUsePromoCodeButton() {}
    func didTapHelpButton() {}
    func newConversationStarted() {}
    func didActivatePromoCode(validPromoCode: ValidPromoCode) {}
}

fileprivate final class ViewModelInputsMock: PhoneVerificationCodeViewModelInputs {
    func startTimerToUpdateSMSAndCallButtonsState() {}
    func clearTimer() {}
    func confirmSendSMSCode() {}
    func sendSMSCodeRequest() {}
    func callUserPhone() {}
    func validateCode(code: String) {}
}

fileprivate final class ViewModelOutputsMock: PhoneVerificationCodeViewModelOutputs {
    func updateCallButton(isEnabled: Bool, title: String) {}
    func updateSendSMSButton(isEnabled: Bool, title: String) {}
    func disableSendSMSButton() {}
    func disableCallButton() {}
    func hideCallButton() {}
    func showErrorMessage(labelMessage: String?, textFieldMessage: String?) {}
    func hideErrorMessage() {}
    func startLoading() {}
    func stopLoading() {}
    func phoneNumberVerifiedWithSuccess() {}
    func showConfirmationAlertWith(ddd: String, phoneNumber: String) {}
    func showYouWillReceiveACallAlert() {}
}

fileprivate final class PhoneVerificationCodeViewModelMock: PhoneVerificationCodeViewModelType {
    var inputs: PhoneVerificationCodeViewModelInputs
    var outputs: PhoneVerificationCodeViewModelOutputs?
    
    init() {
        self.inputs = ViewModelInputsMock()
        self.outputs = ViewModelOutputsMock()
    }
}

fileprivate final class PhoneVerificationCodeCoordinatorMock: PhoneVerificationCodeCoordinating {
    var viewController: UIViewController?
    func perform(action: PhoneVerificationCodeAction) { }
}
