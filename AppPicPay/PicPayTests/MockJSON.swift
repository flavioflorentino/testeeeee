import SwiftyJSON
@testable import PicPay

class MockJSON {
    func load(resource: String) -> JSON {
        let bundle = Bundle(for: type(of: self))
        let path = bundle.path(forResource: resource, ofType: "json")!
        let data = try! Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
        return try! JSON(data: data)
    }
}

class MockCodable<T: Decodable> {
    lazy var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .customISO8601
        return decoder
    }()
    
    @discardableResult
    func loadCodableObject(resource: String, typeDecoder: JSONDecoder.KeyDecodingStrategy = .convertFromSnakeCase) throws -> T {
        decoder.keyDecodingStrategy = typeDecoder
        let bundle = Bundle(for: type(of: self))
        let path = bundle.path(forResource: resource, ofType: "json")!
        let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
        
        let object = try decoder.decode(T.self, from: data)
        return object
    }
}

class MockDecodable<T: Decodable> {
    lazy var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .customISO8601
        return decoder
    }()
    
    @discardableResult
    func loadCodableObject(resource: String, typeDecoder: JSONDecoder.KeyDecodingStrategy = .convertFromSnakeCase) throws -> T {
        decoder.keyDecodingStrategy = typeDecoder
        let bundle = Bundle(for: type(of: self))
        let path = bundle.path(forResource: resource, ofType: "json")!
        let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
        
        let object = try decoder.decodeNestedData(T.self, from: data)
        return object
    }
}
