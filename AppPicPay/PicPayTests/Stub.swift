import Foundation
import OHHTTPStubs
@testable import PicPay

class StubResponse {
    static func success(at path: String) -> OHHTTPStubsResponse {
    return OHHTTPStubsResponse(fileAtPath: File().at(path),
                               statusCode: StatusCode.success,
                               headers: Headers.contentTypeJson)
    }
    
    static func success(with obj: [String : Any]) -> OHHTTPStubsResponse {
        return OHHTTPStubsResponse(jsonObject: obj,
                                   statusCode: StatusCode.success,
                                   headers: Headers.contentTypeJson)
    }
    
    static func success(with data: Data) -> OHHTTPStubsResponse {
        return OHHTTPStubsResponse(data: data,
                                   statusCode: StatusCode.success,
                                   headers: Headers.contentTypeJson)
    }
    
    static func fail() -> OHHTTPStubsResponse {
        let obj = ["key":"jiberish", "key2":["value2A","value2B"]] as [String : Any]
        return OHHTTPStubsResponse(jsonObject: obj,
                                   statusCode: StatusCode.fail500,
                                   headers: Headers.contentTypeJson)
    }
}

class isHost💚 {
    static let PicPay: OHHTTPStubsTestBlock = isHost("gateway.service.ppay.me")
}

class File {
    func at(_ path: String) -> String {
        return OHPathForFile(path, type(of: self))!
    }
}

struct StatusCode {
    static let success: Int32 = 200
    static let fail500: Int32 = 500
}

struct Headers {
    static let contentTypeJson = ["Content-Type": "application/json"]
}
