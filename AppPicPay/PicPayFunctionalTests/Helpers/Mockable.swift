public protocol Mockable {
    static func mockAll()
}

public protocol MockableTestFlow {
    func mockFlows(using mocks: Mockable.Type...)
}

extension MockableTestFlow {
    /// Mocks flows completly by calling the `mockAll` function for every `Mockable`
    /// - Parameter flows: The flows to mock completly
    public func mockFlows(using mocks: Mockable.Type...) {
        mocks.forEach { mock in mock.mockAll() }
    }
}
