import FeatureFlag
import KIF
@testable import PicPay

extension XCTestCase {
    
    public func tester(file: String = #file, _ line: Int = #line) -> KIFUITestActor {
        return KIFUITestActor(inFile: file, atLine: line, delegate: self)
    }
    
    public func viewTester(file: String = #file, _ line: Int = #line) -> KIFUIViewTestActor {
        return KIFUIViewTestActor(inFile: file, atLine: line, delegate: self)
    }
    
    public func system(file: String = #file, _ line: Int = #line) -> KIFSystemTestActor {
        return KIFSystemTestActor(inFile: file, atLine: line, delegate: self)
    }
    
    public func tap(_ text: String) {
        viewTester()
            .usingIdentifier(text)?
            .waitForTappableView()?
            .tap()
    }
}

extension XCTestCase {
    /// Método responsável por navegar até a Home do aplicativo e aceitando o alerta nativo caso necessário
    public func navigateToHome() {
        LoginMock.mockAll()
        
        viewTester()
            .usingIdentifier(SignUpViewController.AccessibilityIdentifier.loginButton.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LoginViewController.AccessibilityIdentifier.loginTextField.rawValue)?
            .enterText("v.fnunes")
        viewTester()
            .usingIdentifier(LoginViewController.AccessibilityIdentifier.passwordTextField.rawValue)?
            .enterText("123456789")
        viewTester()
            .usingIdentifier(LoginViewController.AccessibilityIdentifier.loginButton.rawValue)?
            .tap()
    }
    
    /// Método responsavél por navegar até a tela de registro
    public func navigateToRegister() {
        viewTester().usingIdentifier(SignUpViewController.AccessibilityIdentifier.registerButton.rawValue)?.tap()
    }
    
    public func initialApplicationState() {
        User.deleteToken()
        
        let remoteConfigMock = RemoteConfigMock()
        RemoteConfigSetup.inject(instance: remoteConfigMock)
        remoteConfigMock.override(key: .featureOpenAppTabV2, with: #"{ "tab": "feed", "sub_tab": "" }"#)
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let controller = UINavigationController(rootViewController: SignUpFactory.make())
        UIView.setAnimationsEnabled(false)
        guard let window = appDelegate?.window else {
            fatalError("Tried to UnloggedViewController but there was no UIWindow on AppDelegate")
        }
        
        window.rootViewController = controller
    }
    
    public func checkIfHomeScreen() {
        viewTester().usingIdentifier("scannerButton")?.usingTraits(.button)?.waitForView()
        viewTester().usingLabel("Meu saldo")?.waitForView()
        viewTester().usingIdentifier("inviteButton")?.usingTraits(.button)?.waitForView()
    }
    
    /// Método responsável por navegar até os Ajustes do aplicativo
    public func navigateToConfig() {
        navigateToHome()
        viewTester().usingLabel("Ajustes")?.waitForTappableView()?.tap()
    }
    
    public func getCellIndexPath(tableViewIdentifier: String, cellLabel: String) -> IndexPath? {
        if let tableView = viewTester().usingIdentifier(tableViewIdentifier)?.waitForView() as? UITableView,
            let cell = viewTester().usingLabel(cellLabel)?.waitForTappableView() as? UITableViewCell {
            return tableView.indexPath(for: cell)
        }
        return nil
    }
    
    private func existsViewWithAccessibilityLabel(_ label: String) -> Bool {
        viewTester().wait(forTimeInterval: 10)
        
        do {
            try tester().tryFindingView(withAccessibilityLabel: label)
            return true
        } catch {
            return false
        }
    }
    
    public func navigateToWallet() {
        navigateToHome()
        viewTester()
            .usingLabel("Carteira")?
            .tap()
    }
}
