import Foundation
import OHHTTPStubs
@testable import PicPay

class StubResponse {
    static func success(at path: String) -> OHHTTPStubsResponse {
        return OHHTTPStubsResponse(fileAtPath: File().at(path),
                                   statusCode: StatusCode.success,
                                   headers: Headers.contentTypeJson)
    }
    
    static func success(with obj: [String : Any], statusCode: Int32 = StatusCode.success) -> OHHTTPStubsResponse {
        return OHHTTPStubsResponse(jsonObject: obj,
                                   statusCode: statusCode,
                                   headers: Headers.contentTypeJson)
    }
    
    static func success(with data: Data) -> OHHTTPStubsResponse {
        return OHHTTPStubsResponse(data: data,
                                   statusCode: StatusCode.success,
                                   headers: Headers.contentTypeJson)
    }
    
    static func fail(with statusCode: Int32 = StatusCode.badRequest, fileAt path: String) -> OHHTTPStubsResponse {
        OHHTTPStubsResponse(fileAtPath: File().at(path), statusCode: statusCode, headers: Headers.contentTypeJson)
    }
    
    static func fail(with statusCode: Int32 = StatusCode.fail500) -> OHHTTPStubsResponse {
        let obj = ["key":"jiberish", "key2":["value2A","value2B"]] as [String : Any]
        return OHHTTPStubsResponse(jsonObject: obj,
                                   statusCode: statusCode,
                                   headers: Headers.contentTypeJson)
    }
}
// swiftlint:disable:next type_name
class isHost💚 {
    static let PicPay: OHHTTPStubsTestBlock = isHost("gateway.service.ppay.me")
}

class File {
    func at(_ path: String) -> String {
        return OHPathForFile(path, type(of: self))!
    }
}

struct StatusCode {
    static let success: Int32 = 200
    static let fail500: Int32 = 500
    static let badRequest: Int32 = 400
}

struct Headers {
    static let contentTypeJson = ["Content-Type": "application/json"]
}
