import KIF
import OHHTTPStubs
@testable import PicPay

class RegistrationStepsTests: KIFTestCase {
    private enum Scene: String {
        case verifyPhoneNumber = "VerifyPhoneNumber.json"
    }
    
    override func beforeEach() {
        super.beforeEach()
        initialApplicationState()
        viewTester().acknowledgeSystemAlert()
        let remoteConfigMock = RemoteConfigMock()
        RemoteConfigSetup.inject(instance: remoteConfigMock)
        remoteConfigMock.override(key: .dynamicRegistrationSteps, with: false)
        remoteConfigMock.override(key: .phoneRegisterWithWhatsApp, with: true)
        remoteConfigMock.override(key: .featureOpenAppTabV2, with: #"{ "tab": "feed", "sub_tab": "" }"#)
    }
    
    override func afterEach() {
        super.afterEach()
        OHHTTPStubs.removeAllStubs()
    }
    
//    TODO: Adapt to new registration flow
//    func testRegistrationSteps() {
//        navigateToRegister()
//        stepNameRegistration()
//        stepPhoneNumberRegistration()
//        stepCodeNumberRegistration()
//        stepEmailRegistration()
//        stepPasswordRegistration()
//        stepDocumentNumberAndBirthdayDateRegistration()
//        stepUserNameRegistration()
//        checkIfHomeScreen()
//    }
    
    func testPhoneRegistrationInvalidInput() {
        navigateToRegister()
        stepNameRegistration()
        stepPhoneNumberInvalidInput()
    }
}

private extension RegistrationStepsTests {
    
    func stepNameRegistration() {
        stub(condition: isPath("/api/isValidName.json")) { _ in
            return StubResponse.success(with: ["data": ["isValidName": true, "name": "Alberto Santos"]])
        }
        
        viewTester().usingLabel("Oi! Qual é o seu nome?")?.waitForView()
        viewTester().usingLabel("É necessário que você informe seu nome verdadeiro. Não utilize nome de terceiros.")?.waitForView()
        viewTester().usingValue("Nome")?.enterText("Alberto")
        viewTester().usingValue("Sobrenome")?.enterText("Santos")
        viewTester().usingLabel("Oi, Alberto Santos!")?.waitForView()
        viewTester().usingLabel("Avançar")?.usingTraits(.button)?.tap()
    }
    
    func stepPhoneNumberRegistration() {
        
        stub(condition: isHost💚.PicPay) { _ in
            return StubResponse.success(with: ["Error": ["description_pt": "Code 2538", "id": "2001"]])
        }
        
        viewTester().usingLabel("Qual é o seu telefone?")?.waitForView()
        viewTester().usingLabel("Já possuo o código")?.usingTraits(.button)?.waitForView()
        viewTester().usingValue("DDD")?.enterText("11")
        viewTester().usingValue("Número do seu celular")?.enterText("01111-1111")
        viewTester().usingLabel("Pedir código de confirmação")?.usingTraits(.button).tap()
        viewTester().usingLabel("Confirme o número:")?.waitForView()
        viewTester().usingLabel("(11) 01111-1111 \n\nO número acima está correto?")?.waitForView()
        viewTester().usingLabel("Ok")?.usingTraits(.button)?.tap()
        viewTester().usingLabel("Pedir código de confirmação")?.waitForTappableView()
    }
    
    func stepCodeNumberRegistration() {
        
        stub(condition: isHost💚.PicPay) { _ in
            return StubResponse.success(at: Scene.verifyPhoneNumber.rawValue)
        }
        
        let label = viewTester().usingIdentifier("messageErrorLabel")?.waitForView() as? UILabel
        let code = label?.text?.components(separatedBy: .whitespaces).last
        
        viewTester().usingLabel("Já possuo o código")?.tap()
        viewTester().usingLabel("Para continuar, digite aqui o código de 4 dígitos que te enviamos por SMS.")?.waitForView()
        viewTester().usingLabel("Reenviar SMS com código")?.usingTraits(.button)?.waitForView()
        viewTester().usingValue("XXXX")?.enterText(code)
    }
    
    func stepEmailRegistration() {
        
        stub(condition: isHost💚.PicPay) { _ in
            return StubResponse.success(with: ["data": ["auto_correct": nil, "email": "teste.nave10@picpay.com", "isValidName": true, "status": nil]])
        }
        
        viewTester().usingLabel("Informe seu email")?.waitForView()
        viewTester().usingLabel("Precisamos do seu email. Vamos usá-lo para enviar informações importantes a você.")?.waitForView()
        viewTester().usingValue("Email")?.enterText("teste.nave10@picpay.com")
        viewTester().usingLabel("Avançar")?.usingTraits(.button)?.tap()
    }
    
    func stepPasswordRegistration() {
        
        stub(condition: isHost💚.PicPay) { _ in
            return StubResponse.success(with: ["data": ["isValidPass": true]])
        }
        
        viewTester().usingLabel("Crie sua senha")?.waitForView()
        viewTester().usingLabel("Guarde sua senha. Você precisará dela sempre que fizer um pagamento. Ela deve ter apenas números e, no mínimo, 4 dígitos.")?.waitForView()
        viewTester().usingValue("Senha")?.enterText("123456789")
        viewTester().usingLabel("Avançar")?.usingTraits(.button)?.tap()
    }
    
    func stepDocumentNumberAndBirthdayDateRegistration() {
        
        stub(condition: isHost💚.PicPay) { _ in
            return StubResponse.success(with: ["data": ["mgm": nil, "token": "16452227c6e44847a78b76de3d79c1c7"]])
        }
        
        viewTester().usingLabel("Seu CPF e data de nascimento")?.waitForView()
        viewTester().usingLabel("Alberto, precisamos do seu CPF e da sua data de nascimento. Saiba mais sobre isso.")?.waitForView()
        viewTester().usingValue("CPF (somente números)")?.enterText("123.456.789-09")
        viewTester().usingValue("Data de nascimento (dd/mm/aaaa)")?.enterText("06/08/1993")
        viewTester().usingLabel("Confirmar")?.usingTraits(.button)?.tap()
    }
    
    func stepUserNameRegistration() {
        
        FeedMock.mockAllConsumerData()
        
        viewTester().usingLabel("Qual vai ser seu PicPay?")?.waitForView()
        viewTester().usingLabel("Este vai ser o seu nome de usuário. Você vai informá-lo sempre que alguém precisar te procurar por aqui.")?.waitForView()
        viewTester().usingIdentifier("UsernameTextField")?.enterText("teste.picpay")
        viewTester().usingLabel("Concluir")?.tap()
    }
    
    func stepPhoneNumberInvalidInput() {
        viewTester().usingLabel("Qual é o seu telefone?")?.waitForView()
        viewTester().usingLabel("Já possuo o código")?.usingTraits(.button)?.waitForView()
        viewTester().usingValue("DDD")?.enterText("11asd", expectedResult: "11")
        viewTester().usingValue("Número do seu celular")?.enterText("01111asd-1111", expectedResult: "01111-1111")
        viewTester().usingLabel("Receber código via WhatsApp")?.tap()
        viewTester().usingLabel("Confirme o número:")?.waitForView()
        
        viewTester().usingLabel("(11) 01111-1111 \n\nO número acima está correto?")?.waitForView()
    }
}
