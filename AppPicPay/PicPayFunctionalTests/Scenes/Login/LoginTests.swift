import KIF
import OHHTTPStubs
@testable import PicPay

class LoginTests: KIFTestCase, MockableTestFlow {
    override func beforeEach() {
        super.beforeEach()
        initialApplicationState()
        
        let remoteConfigMock = RemoteConfigMock()
        RemoteConfigSetup.inject(instance: remoteConfigMock)
        remoteConfigMock.override(key: .featureOpenAppTabV2, with: #"{ "tab": "feed", "sub_tab": "" }"#)
    }
    
    override func afterEach() {
        super.afterEach()
        OHHTTPStubs.removeAllStubs()
    }
    
    func testLogin() {
        mockFlows(using: LoginMock.self, FeedMock.self)
        viewTester()
            .usingIdentifier(SignUpViewController.AccessibilityIdentifier.loginButton.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LoginViewController.AccessibilityIdentifier.loginTextField.rawValue)?
            .enterText("v.fnunes")
        viewTester()
            .usingIdentifier(LoginViewController.AccessibilityIdentifier.passwordTextField.rawValue)?
            .enterText("123456789")
        viewTester()
            .usingIdentifier(LoginViewController.AccessibilityIdentifier.loginButton.rawValue)?
            .tap()
        checkIfHomeScreen()
    }
    
    func testRecoverPassword() {
        mockFlows(using: LoginMock.self, ForgotLoginPasswordMock.self)
        viewTester()
            .usingIdentifier(SignUpViewController.AccessibilityIdentifier.loginButton.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LoginViewController.AccessibilityIdentifier.forgotPasswordButton.rawValue)?
            .tap()
        
        viewTester().usingLabel("Esqueci minha senha")?.waitForView()
        
    }
    
}
