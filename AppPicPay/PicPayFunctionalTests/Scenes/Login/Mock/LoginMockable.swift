import OHHTTPStubs

protocol LoginMockable: Mockable {
    static func mockLogin()
    static func mockVersionUpdate()
}

extension LoginMockable {
    static func mockLogin() {
        stub(condition: isPath("/auth/login")) { _ in
            return StubResponse.success(with: ["token": "7605be4811284e3b9dfdc82ae4a906c4"])
        }
    }
    
    static func mockVersionUpdate() {
        stub(condition: isPath("/api/verifyVersionUpdate.json")) { _ in
            return StubResponse.success(at: "verifyVersionUpdate.json")
        }
    }
}

final class LoginMock: LoginMockable {
    static func mockAll() {
        mockLogin()
        mockVersionUpdate()
    }
}

