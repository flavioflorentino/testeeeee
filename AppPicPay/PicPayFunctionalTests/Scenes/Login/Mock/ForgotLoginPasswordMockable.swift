import OHHTTPStubs

protocol ForgotLoginPasswordMockable: Mockable {
    static func mockForgotPassword()
}

extension ForgotLoginPasswordMockable {
    static func mockForgotPassword() {
        let response = StubResponse.success(at: "ForgotLoginPasswordResponse.html")
        stub(condition: isPath("/api/ForgotLoginPasswordResponse.html")) { _ in
            return response
        }
    }
}

final class ForgotLoginPasswordMock: ForgotLoginPasswordMockable {
    static func mockAll() {
        mockForgotPassword()
    }
}
