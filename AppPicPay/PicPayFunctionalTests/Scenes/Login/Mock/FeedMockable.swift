import OHHTTPStubs

protocol FeedMockable: Mockable {
    static func mockAllConsumerData()
    static func mockPaymentMethods()
    static func mockSearchSuggestions()
    static func mockGlobalCredit()
    static func mockFeed()
    static func mockStudentAccount()
}

extension FeedMockable {
    static func mockAllConsumerData() {
        stub(condition: isPath("/api/getAllConsumerData.json")) { _ in
            return StubResponse.success(at: "GetAllConsumerData.json")
        }
    }
    
    static func mockPaymentMethods() {
        stub(condition: isPath("/api/getPaymentMethods.json")) { _ in
            return StubResponse.success(at: "GetPaymentMethods.json")
        }
    }
    
    static func mockSearchSuggestions() {
        stub(condition: isPath("/search")) { _ in
            return StubResponse.success(at: "SearchGroupSuggestion.json")
        }
    }
    
    static func mockGlobalCredit() {
        stub(condition: isPath("/api/getGlobalCredit.json")) { _ in
            return StubResponse.success(at: "GetGlobalCredit.json")
        }
    }
    
    static func mockFeed() {
        stub(condition: isPath("/api/getFeed.json")) { _ in
            return StubResponse.success(at: "GetFeed.json")
        }
    }
    
    static func mockStudentAccount() {
        stub(condition: isPath("/studentaccount/students/status")) { _ in
            return StubResponse.success(at: "studentAccount.json")
        }
    }
}

final class FeedMock: FeedMockable {
    static func mockAll() {
        mockAllConsumerData()
        mockPaymentMethods()
        mockSearchSuggestions()
        mockGlobalCredit()
        mockFeed()
        mockStudentAccount()
    }
}
