import KIF
import OHHTTPStubs
@testable import PicPay

final class RegisterBankAccountSplashTests: KIFTestCase, MockableTestFlow {
    
    override func beforeEach() {
        super.beforeEach()
        initialApplicationState()
        let remoteConfigMock = RemoteConfigMock()
        RemoteConfigSetup.inject(instance: remoteConfigMock)
        remoteConfigMock.override(key: .featureOpenAppTabV2, with: #"{ "tab": "feed", "sub_tab": "" }"#)
        mockFlows(using: LoginMock.self, FeedMock.self, FavoritesMock.self, SettingsMock.self)
    }
    
    override func afterEach() {
        super.afterEach()
        OHHTTPStubs.removeAllStubs()
    }
    
    func testRegisterBankAccountSplash() {
        ConsumerManager.shared.consumer?.bankAccount = nil
        navigateToBankAccount()
        
        viewTester()
            .usingIdentifier(RegisterBankAccountSplashViewController.AccessibilityIdentifier.headlineLabel.rawValue)?
            .expect(toContainText: BankAccountLocalizable.registerBankAccountHeadline.text)
        
        viewTester()
            .usingIdentifier(RegisterBankAccountSplashViewController.AccessibilityIdentifier.captionLabel.rawValue)?
            .expect(toContainText: BankAccountLocalizable.registerBankAccountCaption.text)
        
        viewTester()
            .usingIdentifier(RegisterBankAccountSplashViewController.AccessibilityIdentifier.registerBankAccountButton.rawValue)?
            .expect(toContainText: BankAccountLocalizable.registerBankAccountButtonTitle.text)
        
        viewTester()
            .usingIdentifier(RegisterBankAccountSplashViewController.AccessibilityIdentifier.registerBankAccountButton.rawValue)?
            .tap()
    }
    
    private func navigateToBankAccount() {
        navigateToConfig()
        viewTester().usingLabel("Conta bancária")?.tap()
    }
}
