import XCTest
import KIF
import OHHTTPStubs
@testable import PicPay

final class BanksTests: KIFTestCase, MockableTestFlow {
    override func beforeEach() {
        super.beforeEach()
        initialApplicationState()
        let remoteConfigMock = RemoteConfigMock()
        RemoteConfigSetup.inject(instance: remoteConfigMock)
        remoteConfigMock.override(key: .featureOpenAppTabV2, with: #"{ "tab": "feed", "sub_tab": "" }"#)
        mockFlows(using: LoginMock.self, FeedMock.self, FavoritesMock.self, SettingsMock.self)
    }
    
    override func afterEach() {
        super.afterEach()
        OHHTTPStubs.removeAllStubs()
    }
    
    private func navigateToBankList() {
        ConsumerManager.shared.consumer?.bankAccount = nil
        navigateToConfig()
        viewTester().usingLabel("Conta bancária")?.tap()
        
        viewTester()
            .usingIdentifier(RegisterBankAccountSplashViewController.AccessibilityIdentifier.registerBankAccountButton.rawValue)?
            .tap()
    }
    
    // MARK: Tests
    
    func testSuccessfullyLoadBanksWhenTapRowShouldDisplayBankAccountForm() {
        ConsumerManager.shared.consumer?.bankAccount = nil
        let indexPath = IndexPath(row: 0, section: 0)
        BanksMock.fetchBanks(successfully: true)
        navigateToBankList()
        
        viewTester()
            .usingIdentifier(BanksViewController.AccessibilityIdentifier.banksTableView.rawValue)?
            .tapRowInTableView(at: indexPath)
        
        viewTester()
            .usingLabel("Cadastrar Conta")?
            .waitForView()
    }
    
    func testSuccessfullyLoadBanksWhenTypeSearchQueryShouldFilterResults() {
        ConsumerManager.shared.consumer?.bankAccount = nil
        BanksMock.fetchBanks(successfully: true)
        navigateToBankList()
        
        viewTester()
            .usingIdentifier(BanksViewController.AccessibilityIdentifier.banksSearchBar.rawValue)?
            .enterText("Ita")
    
        viewTester()
            .usingIdentifier(BanksViewController.AccessibilityIdentifier.banksTableView.rawValue)?
            .expect(toContainText: "Ita")
    }
    
    func testLoadBanksWhenReceiveErrorFromServerDisplayErrorView() {
        ConsumerManager.shared.consumer?.bankAccount = nil
        BanksMock.fetchBanks(successfully: false)
        navigateToBankList()
        
        viewTester()
            .usingIdentifier(BanksViewController.AccessibilityIdentifier.errorView.rawValue)?
            .waitForView()
    }
    
    func testSuccessfullyLoadBanksWhenTypeSearchQueryWithoutResultsShouldDisplayEmptySearchResultsView() {
        ConsumerManager.shared.consumer?.bankAccount = nil
        BanksMock.fetchBanks(successfully: true)
        navigateToBankList()
        
        viewTester()
            .usingIdentifier(BanksViewController.AccessibilityIdentifier.banksSearchBar.rawValue)?
            .enterText("Some bank")
        
        viewTester()
            .usingIdentifier(BanksViewController.AccessibilityIdentifier.emptySearchResultsView.rawValue)?
            .expect(toContainText: BankAccountLocalizable.bankNotFound.text)
    }
}
