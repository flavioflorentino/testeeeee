import KIF
import OHHTTPStubs
@testable import PicPay

final class NewBankAccountFormTests: KIFTestCase, MockableTestFlow {
    override func beforeEach() {
        super.beforeEach()
        initialApplicationState()
        let remoteConfigMock = RemoteConfigMock()
        RemoteConfigSetup.inject(instance: remoteConfigMock)
        mockFlows(using: LoginMock.self, FeedMock.self, FavoritesMock.self, SettingsMock.self, BanksMock.self)
    }
    
    override func afterEach() {
        super.afterEach()
        OHHTTPStubs.removeAllStubs()
    }
    
    func testNewBankAccountForm_WhenTapSaveButtonWithoutFillingTheForm_ShouldDisplayValidationErrors() {
        navigateToBankAccountForm()
        
        viewTester()
            .usingIdentifier(NewBankAccountFormViewController.AccessibilityIdentifier.saveBankAccountButton.rawValue)?
            .tap()
        
        viewTester()
            .usingLabel(NewBankAccountFormValidationError.invalidBranchNumber.localizedDescription)?
            .waitForView()
        viewTester()
            .usingLabel(NewBankAccountFormValidationError.invalidAccountNumber.localizedDescription)?
            .waitForView()
        viewTester()
            .usingLabel(NewBankAccountFormValidationError.invalidAccountDigit.localizedDescription)?
            .waitForView()
        viewTester()
            .usingLabel(NewBankAccountFormValidationError.invalidAccountType.localizedDescription)?
            .waitForView()
    }
    
    func testNewBankAccountForm_WhenTapBankView_ShouldShowBankList() {
        navigateToBankAccountForm()
        
        viewTester()
            .usingIdentifier(NewBankAccountFormViewController.AccessibilityIdentifier.changeBankButton.rawValue)?
            .tap()
        
        viewTester()
            .usingIdentifier(BanksViewController.AccessibilityIdentifier.banksTableView.rawValue)?
            .waitForView()
    }
    
    func testNewBankAccountForm_WhenTapNotMyAccountCheckbox_ShouldDisplayDocumentNumberTextField() {
        navigateToBankAccountForm()
        
        viewTester()
            .usingIdentifier(NewBankAccountFormViewController.AccessibilityIdentifier.notMyAccountButton.rawValue)?
            .tap()
        
        viewTester()
            .usingIdentifier(NewBankAccountFormViewController.AccessibilityIdentifier.documentHolderNumberTextField.rawValue)?
            .waitForView()
    }
    
    func testNewBankAccountForm_WhenSaveBankAccountAndReceiveSuccess_ShouldPopBankAccount() {
        NewBankAccountFromMock.addBank(successfully: true)
        
        navigateToBankAccountForm()
        
        viewTester()
            .usingIdentifier(NewBankAccountFormViewController.AccessibilityIdentifier.branchNumberTextField.rawValue)?
            .enterText("1234")
        
        viewTester()
            .usingIdentifier(NewBankAccountFormViewController.AccessibilityIdentifier.accountNumberTextField.rawValue)?
            .enterText("12345")
        
        viewTester()
            .usingIdentifier(NewBankAccountFormViewController.AccessibilityIdentifier.accountDigitTextField.rawValue)?
            .enterText("1")
        
        viewTester()
            .usingIdentifier(NewBankAccountFormViewController.AccessibilityIdentifier.accountTypeTextField.rawValue)?
            .tap()
        
        viewTester()
            .usingIdentifier(NewBankAccountFormViewController.AccessibilityIdentifier.accountTypePickerView.rawValue)?
            .selectPickerViewRow(withTitle: "Poupança")
        
        viewTester()
            .usingIdentifier(NewBankAccountFormViewController.AccessibilityIdentifier.saveBankAccountButton.rawValue)?
            .tap()
        
        viewTester()
            .usingIdentifier(BankAccountViewController.AccessibilityIdentifier.bankAccountsTableView.rawValue)?
            .waitForView()
    }
    
    func testNewBankAccountForm_WhenSaveBankAccountAndReceiveFailure_ShouldDisplayErrorAlert() {
        NewBankAccountFromMock.addBank(successfully: false)
        
        navigateToBankAccountForm()
        
        viewTester()
            .usingIdentifier(NewBankAccountFormViewController.AccessibilityIdentifier.branchNumberTextField.rawValue)?
            .enterText("4321")
        
        viewTester()
            .usingIdentifier(NewBankAccountFormViewController.AccessibilityIdentifier.accountNumberTextField.rawValue)?
            .enterText("54321")
        
        viewTester()
            .usingIdentifier(NewBankAccountFormViewController.AccessibilityIdentifier.accountDigitTextField.rawValue)?
            .enterText("2")
        
        viewTester()
            .usingIdentifier(NewBankAccountFormViewController.AccessibilityIdentifier.accountTypeTextField.rawValue)?
            .tap()
        
        viewTester()
            .usingIdentifier(NewBankAccountFormViewController.AccessibilityIdentifier.accountTypePickerView.rawValue)?
            .selectPickerViewRow(withTitle: "Conta Corrente")
        
        viewTester()
            .usingIdentifier(NewBankAccountFormViewController.AccessibilityIdentifier.saveBankAccountButton.rawValue)?
            .tap()
        
        viewTester()
            .usingLabel(DefaultLocalizable.btOk.text)?
            .tap()
    }
    
    private func navigateToBankAccountForm() {
        ConsumerManager.shared.consumer?.bankAccount = nil
        navigateToConfig()
        viewTester().usingLabel("Conta bancária")?.tap()
        
        viewTester()
            .usingIdentifier(RegisterBankAccountSplashViewController.AccessibilityIdentifier.registerBankAccountButton.rawValue)?
            .tap()
        
        viewTester()
            .usingIdentifier(BanksViewController.AccessibilityIdentifier.banksTableView.rawValue)?
            .tapRowInTableView(at: IndexPath(row: 3, section: 0))
    }
}
