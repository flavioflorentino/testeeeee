import KIF
import OHHTTPStubs
@testable import PicPay

final class BankAccountTests: KIFTestCase, MockableTestFlow {
    // MARK: - Mocks
    
    private lazy var bankAccountMock: PPBankAccount = {
        let bank = PPBank()
        bank.name = "itaú"
        bank.imageUrl = "https://www.host.com/someImage.png"
        let bankAccount = PPBankAccount()
        bankAccount.wsId = "1"
        bankAccount.agencyNumber = "1234"
        bankAccount.agencyDigit = "1"
        bankAccount.accountNumber = "12345"
        bankAccount.accountDigit = "1"
        bankAccount.bank = bank
        return bankAccount
    }()
    
    override func beforeEach() {
        super.beforeEach()
        initialApplicationState()
        let remoteConfigMock = RemoteConfigMock()
        RemoteConfigSetup.inject(instance: remoteConfigMock)
        remoteConfigMock.override(key: .featureOpenAppTabV2, with: #"{ "tab": "feed", "sub_tab": "" }"#)
        mockFlows(using: LoginMock.self, FeedMock.self, FavoritesMock.self, SettingsMock.self)
    }
    
    override func afterEach() {
        super.afterEach()
        OHHTTPStubs.removeAllStubs()
    }
    
    func testBankAccountDisplay() {
        navigateToBankAccount()
        
        viewTester()
            .usingIdentifier(BankAccountViewController.AccessibilityIdentifier.removeBarButtonItem.rawValue)?
            .expect(toContainText: DefaultLocalizable.remove.text)
        
        viewTester()
            .usingIdentifier(BankAccountViewController.AccessibilityIdentifier.changeBankButton.rawValue)?
            .expect(toContainText: BankAccountLocalizable.changeBankAccountButtonTitle.text)
        
        viewTester()
            .usingIdentifier(BankAccountViewController.AccessibilityIdentifier.bankAccountsTableView.rawValue)
    }
    
    func testRemoveBankAccountSuccessfully() {
        navigateToBankAccount()
        BankAccountMock.mockDeleteBankAccount(successfully: true)
        
        viewTester()
            .usingIdentifier(BankAccountViewController.AccessibilityIdentifier.removeBarButtonItem.rawValue)?
            .tap()
        
        viewTester()
            .usingLabel(BankAccountLocalizable.bankRemoveAccount.text)?.tap()
        
        viewTester()
            .usingIdentifier(RegisterBankAccountSplashViewController.AccessibilityIdentifier.headlineLabel.rawValue)?
            .expect(toContainText: BankAccountLocalizable.registerBankAccountHeadline.text)
    }
    
    func testRemoveBankAccountUnsuccessfully() {
        navigateToBankAccount()
        BankAccountMock.mockDeleteBankAccount(successfully: false)
        
        viewTester()
            .usingIdentifier(BankAccountViewController.AccessibilityIdentifier.removeBarButtonItem.rawValue)?
            .tap()
        
        viewTester()
            .usingLabel(BankAccountLocalizable.bankRemoveAccount.text)
            .tap()
        
        viewTester()
            .usingLabel(DefaultLocalizable.btOk.text)?
            .tap()
    }
    
    private func navigateToBankAccount() {
        navigateToConfig()
        viewTester().usingLabel("Conta bancária")?.waitForTappableView()?.tap()
    }
    
    override func navigateToConfig() {
        navigateToHome()
        ConsumerManager.shared.consumer?.bankAccount = bankAccountMock
        viewTester().usingLabel("Ajustes")?.tap()
    }
}
