import OHHTTPStubs

protocol BanksMockable: Mockable {
    static func fetchBanks(successfully: Bool)
}

extension BanksMockable {
    static func fetchBanks(successfully: Bool) {
        let response = successfully ? StubResponse.success(at: "BankList.json") : StubResponse.fail()
        stub(condition: isPath("/api/getBanksList.json")) { _ in
            return response
        }
    }
}

final class BanksMock: BanksMockable {
    static func mockAll() {
        fetchBanks(successfully: true)
    }
}
