import OHHTTPStubs

protocol NewBankAccountFormMockable: Mockable {
    static func addBank(successfully: Bool)
}

extension NewBankAccountFormMockable {
    static func addBank(successfully: Bool) {
        let response = successfully ? StubResponse.success(at: "AddBankAccount.json") : StubResponse.fail(with: 500)
        stub(condition: pathMatches("/api/addBankAccount.json")) { (request) -> OHHTTPStubsResponse in
            return response
        }
    }
}

final class NewBankAccountFromMock: NewBankAccountFormMockable {
    static func mockAll() {
        addBank(successfully: true)
    }
}
