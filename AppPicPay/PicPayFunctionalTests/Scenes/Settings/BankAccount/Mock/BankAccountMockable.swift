import OHHTTPStubs

protocol BankAccountMockable: Mockable {
    static func mockDeleteBankAccount(successfully: Bool)
}

extension BankAccountMockable {
    static func mockDeleteBankAccount(successfully: Bool) {
        let response = successfully ? StubResponse.success(at: "DeleteBankAccount.json") : StubResponse.fail()
        stub(condition: isPath("/api/deleteBankAccount.json")) { _ in
            return response
        }
    }
}

final class BankAccountMock: BankAccountMockable {
    static func mockAll() {
        mockDeleteBankAccount(successfully: true)
    }
}
