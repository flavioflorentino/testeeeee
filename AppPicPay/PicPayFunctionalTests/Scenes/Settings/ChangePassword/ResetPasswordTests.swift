import KIF
import OHHTTPStubs
@testable import PicPay

final class ResetPasswordTests: KIFTestCase, MockableTestFlow {
    override func beforeEach() {
        super.beforeEach()
        initialApplicationState()
        let remoteConfigMock = RemoteConfigMock()
        RemoteConfigSetup.inject(instance: remoteConfigMock)
        remoteConfigMock.override(key: .featureOpenAppTabV2, with: #"{ "tab": "feed", "sub_tab": "" }"#)
        mockFlows(using: LoginMock.self, FeedMock.self, FavoritesMock.self, SettingsMock.self)
    }
    
    override func afterEach() {
        super.afterEach()
        OHHTTPStubs.removeAllStubs()
    }
    
    func testResetPasswordNewPasswordDoesNotMatch() {
        navigateToResetPassword()
        
        viewTester().usingIdentifier("current_password_textfield")?.enterText("123456789")
        
        viewTester().usingIdentifier("new_password_textfield")?.enterText("123456789")
        viewTester().usingIdentifier("new_password_confirmation_textfield")?.enterText("1234")
        
        viewTester().usingIdentifier("save_button")?.tap()
        
        viewTester().usingLabel("As senhas novas estão diferentes.")?.expect(toContainText: "As senhas novas estão diferentes.")
        viewTester().usingLabel("Ok")?.tap()
    }
    
    func testResetPasswordSuccessfully() {
        navigateToResetPassword()
        
        viewTester().usingIdentifier("current_password_textfield")?.enterText("123456789")
        
        viewTester().usingIdentifier("new_password_textfield")?.enterText("123456789")
        viewTester().usingIdentifier("new_password_confirmation_textfield")?.enterText("123456789")
        
        ResetPasswordMock.mockChangePassword(successfully: true)
        
        viewTester().usingIdentifier("save_button")?.tap()
        
        viewTester().usingLabel("Senha alterada com sucesso!")?.expect(toContainText: "Senha alterada com sucesso!")
        viewTester().usingLabel("Ok")?.tap()
        
    }
    
    func testResetPasswordRequestFailed() {
        navigateToResetPassword()
        
        viewTester().usingIdentifier("current_password_textfield")?.enterText("123456789")
        
        viewTester().usingIdentifier("new_password_textfield")?.enterText("123456789")
        viewTester().usingIdentifier("new_password_confirmation_textfield")?.enterText("123456789")
        
        ResetPasswordMock.mockChangePassword(successfully: false)
        
        viewTester().usingIdentifier("save_button")?.tap()
        
        viewTester().usingLabel("Ok")?.tap()
    }
    
    private func navigateToResetPassword() {
        navigateToConfig()
        
        // open reset password
        
        viewTester().usingLabel("Alterar senha")?.tap()
        viewTester().usingLabel("Redefinir senha")?.tap()
    }
}
