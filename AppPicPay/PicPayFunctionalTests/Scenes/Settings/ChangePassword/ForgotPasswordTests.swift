import KIF
import OHHTTPStubs
@testable import PicPay

final class ForgotPasswordTests: KIFTestCase, MockableTestFlow {
    private lazy var remoteConfigMock = RemoteConfigMock()
    
    override func beforeEach() {
        super.beforeEach()
        initialApplicationState()
        RemoteConfigSetup.inject(instance: remoteConfigMock)
        remoteConfigMock.override(key: .featureOpenAppTabV2, with: #"{ "tab": "feed", "sub_tab": "" }"#)
        mockFlows(using: LoginMock.self, FeedMock.self, FavoritesMock.self, SettingsMock.self, ForgotPasswordMock.self)
    }
    
    override func afterEach() {
        super.afterEach()
        // This makes sure that the obfuscationView is dismissed before another test begins
        NotificationCenter.default.post(name: UIApplication.didBecomeActiveNotification, object: nil)
        
        OHHTTPStubs.removeAllStubs()
    }
    
    private func navigateToForgotPassword() {
        navigateToConfig()
        
        // open Forgot password
        viewTester().usingLabel("Alterar senha")?.tap()
        viewTester().usingLabel("Esqueci minha senha")?.tap()        
    }
    
    func testObfuscationOnForgotPasswordScreen_WhenFlagIsTrue_ShouldShowObfuscationScreen() {
        remoteConfigMock.override(key: .featureAppSwitcherObfuscation, with: true)
        navigateToForgotPassword()
        
        NotificationCenter.default.post(name: UIApplication.willResignActiveNotification, object: nil)
        
        viewTester().usingIdentifier("obfuscationView")?.waitForView()
    }
    
    func testObfuscationOnForgotPasswordScreen_WhenFlagIsFalse_ShouldNotShowObfuscationScreen() {
        remoteConfigMock.override(key: .featureAppSwitcherObfuscation, with: false)
        navigateToForgotPassword()
        
        NotificationCenter.default.post(name: UIApplication.willResignActiveNotification, object: nil)
        XCTAssertEqual(viewTester().usingIdentifier("obfuscationView")?.tryFindingView(), false)
    }
    
    func testDismissObfuscationOnForgotPasswordScreen_WhenFlagIsTrue_ShouldShowObfuscationScreenAndDismiss() {
        remoteConfigMock.override(key: .featureAppSwitcherObfuscation, with: true)        
        navigateToForgotPassword()
        
        NotificationCenter.default.post(name: UIApplication.willResignActiveNotification, object: nil)
        viewTester().usingIdentifier("obfuscationView")?.waitForView()
        
        NotificationCenter.default.post(name: UIApplication.didBecomeActiveNotification, object: nil)
        viewTester().usingIdentifier("obfuscationView")?.waitForAbsenceOfView()
    }
}

