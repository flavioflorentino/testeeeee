import OHHTTPStubs

protocol ResetPasswordMockable: Mockable {
    static func mockChangePassword(successfully: Bool)
}

extension ResetPasswordMockable {
    static func mockChangePassword(successfully: Bool = true) {
        let response = successfully ? StubResponse.success(at: "ChangePassword.json") : StubResponse.fail(with: 500)
        stub(condition: isPath("/api/changePassword.json")) { _ in
            return response
        }
    }
}

final class ResetPasswordMock: ResetPasswordMockable {
    static func mockAll() {
        mockChangePassword()
    }
}
