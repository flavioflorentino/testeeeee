import OHHTTPStubs

protocol ForgotPasswordMockable: Mockable {
    static func mockChangePassword()
}

extension ForgotPasswordMockable {
    static func mockChangePassword() {
        let response = StubResponse.success(at: "ForgotChangePasswordResponse.html")
        stub(condition: isPath("/api/ForgotChangePasswordResponse.html")) { _ in
            return response
        }
    }
}

final class ForgotPasswordMock: ForgotPasswordMockable {
    static func mockAll() {
        mockChangePassword()
    }
}
