import OHHTTPStubs

protocol SettingsMockable: Mockable {
    static func mockPaymentAcccountStatus()
    static func mockCreditAccount()
    static func mockVerificationsIdentitiyStatus()
}

extension SettingsMockable {
    static func mockPaymentAcccountStatus() {
        stub(condition: isPath("/payment_account/status")) { _ in
            return StubResponse.fail(with: 401)
        }
    }
    
    static func mockCreditAccount() {
        stub(condition: isPath("/credit/account")) { _ in
            return StubResponse.fail(with: 401)
        }
    }
    
    static func mockVerificationsIdentitiyStatus() {
        stub(condition: isPath("/verifications/identity/status")) { _ in
            return StubResponse.fail(with: 401)
        }
    }
}

final class SettingsMock: SettingsMockable {
    static func mockAll() {
        mockPaymentAcccountStatus()
        mockCreditAccount()
        mockVerificationsIdentitiyStatus()
    }
}
