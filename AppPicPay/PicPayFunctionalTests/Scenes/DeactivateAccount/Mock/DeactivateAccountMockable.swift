import OHHTTPStubs

protocol DeactivateAccountMockable: Mockable {
    static func mockDeactivateAccount()
}

extension DeactivateAccountMockable {
    static func mockDeactivateAccount() {
        stub(condition: isPath("/api/deactivateConsumer.json")) { _ in
            return StubResponse.success(with: ["data": ["status": "ok"]])
        }
    }
}

final class DeactivateAccountMock: DeactivateAccountMockable {
    static func mockAll() {
        mockDeactivateAccount()
    }
}
