import KIF
import OHHTTPStubs
import FeatureFlag
@testable import PicPay

class DeactivateAccountTest: KIFTestCase, MockableTestFlow {
    override func beforeEach() {
        super.beforeEach()
        initialApplicationState()
        mockFlows(using: LoginMock.self, FeedMock.self)
        let remoteConfigMock = RemoteConfigMock()
        RemoteConfigSetup.inject(instance: remoteConfigMock)
        remoteConfigMock.override(key: .featureOpenAppTabV2, with: #"{ "tab": "feed", "sub_tab": "" }"#)
    }
    
    override func afterEach() {
        super.afterEach()
        OHHTTPStubs.removeAllStubs()
    }
    
    func testDeactivateAccountSteps() {
        navigateToConfig()
        navigateToDeactivate()
        tapDeactivateAccountButton()
        tapConfirmDeactivateAccountButton()
        fillAuthentication()
        tapContinueButton()
        tapOkButton()
        userIsUnlogged()
    }
}

extension DeactivateAccountTest {
    func navigateToDeactivate() {
        viewTester().usingLabel("Desativar minha conta")?.waitForTappableView()
        viewTester().usingLabel("Desativar minha conta")?.tap()
    }
    
    func tapDeactivateAccountButton() {
        viewTester().usingIdentifier("deactivateButton")?.waitForTappableView()
        viewTester().usingIdentifier("deactivateButton")?.tap()
    }
    
    func tapConfirmDeactivateAccountButton() {
        viewTester().usingLabel("Desativar conta")?.waitForTappableView()
        viewTester().usingLabel("Desativar conta")?.tap()
    }
    
    func tapContinueButton() {
        DeactivateAccountMock.mockDeactivateAccount()
        
        viewTester().usingLabel("Continuar")?.waitForTappableView()
        viewTester().usingLabel("Continuar")?.tap()
    }
    
    func fillAuthentication() {
        viewTester().usingLabel("Digite sua senha")?.enterText("1234")
    }
    
    func tapOkButton() {
        viewTester().usingLabel("Ok")?.waitForTappableView()
        viewTester().usingLabel("Ok")?.tap()
    }
    
    func userIsUnlogged() {
        viewTester().usingLabel("Cadastrar")?.waitForView()
    }
}
