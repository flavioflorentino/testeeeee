import OHHTTPStubs

protocol FavoritesMockable: Mockable {
    static func mockFavoriteList()
    static func mockProfile()
    static func mockItemByStore()
    static func mockFavoritesHome()
}

extension FavoritesMockable {
    static func mockFavoriteList() {
        stub(condition: isPath("/favorites")) { _ in
            return StubResponse.success(at: "Favorites.json")
        }
    }
    
    static func mockProfile() {
        stub(condition: isPath("/api/getProfile.json")) { _ in
            return StubResponse.success(at: "GetProfile.json")
        }
    }
    
    static func mockItemByStore() {
        stub(condition: isPath("/api/itemByStoreId.json")) { _ in
            return StubResponse.success(at: "ItemByStoreId.json")
        }
    }
    
    static func mockFavoritesHome() {
        stub(condition: isPath("/favorites/home")) { _ in
            return StubResponse.fail(with: 401)
        }
    }
}

final class FavoritesMock: FavoritesMockable {
    static func mockAll() {
        mockFavoriteList()
        mockProfile()
        mockItemByStore()
        mockFavoritesHome()
    }
}
