import KIF
import OHHTTPStubs
@testable import PicPay

class FavoritesTests: KIFTestCase, MockableTestFlow {
    override func beforeEach() {
        super.beforeEach()
        initialApplicationState()
        
        mockFlows(using: LoginMock.self, FeedMock.self)
        let remoteConfigMock = RemoteConfigMock()
        RemoteConfigSetup.inject(instance: remoteConfigMock)
        remoteConfigMock.override(key: .featureOpenAppTabV2, with: #"{ "tab": "feed", "sub_tab": "" }"#)
    }
    
    override func afterEach() {
        super.afterEach()
        OHHTTPStubs.removeAllStubs()
    }

    func testFavoriteList() {
        navigateToHome()
        openFavoriteList()
        openProfileConsumerByFavorites()
        openProfileStoreByFavorite()
    }
}

private extension FavoritesTests {
    func openFavoriteList() {
        FavoritesMock.mockFavoriteList()
        
        viewTester().usingLabel("Ajustes")?.tap()
        viewTester().usingLabel("Meus Favoritos")?.tap()
        viewTester().usingLabel("Meus Favoritos")?.usingTraits(.header)?.waitForView()
        
        viewTester().usingLabel("Panela Veia")?.usingTraits(.staticText)?.waitForView()
        viewTester().usingLabel("Rua Guanabara, 1140")?.usingTraits(.staticText)?.waitForView()
        
        viewTester().usingLabel("João Victor Lana")?.usingTraits(.staticText)?.waitForView()
        viewTester().usingLabel("joao")?.usingTraits(.staticText)?.waitForView()
        
        viewTester().usingLabel("Adicionar aos favoritos")?.usingTraits(.staticText)?.waitForView()
    }
    
    func openProfileConsumerByFavorites() {
        FavoritesMock.mockProfile()
        
        viewTester().waitForCellInTableView(at: IndexPath(row: 1, section: 0))?.tap()
        viewTester().usingLabel("Cancelar")?.usingTraits(.button)?.waitForTappableView()
        viewTester().usingLabel("Nova Transação")?.usingTraits(.header)?.waitForView()
        viewTester().usingLabel("@joao")?.usingTraits(.staticText)?.waitForView()
        viewTester().usingLabel("João Victor Lana")?.usingTraits(.staticText)?.waitForView()
        viewTester().usingLabel("Pagar")?.usingTraits(.button)?.waitForTappableView()
        viewTester().usingLabel("Cancelar")?.usingTraits(.button)?.tap()
    }
    
    func openProfileStoreByFavorite() {
        FavoritesMock.mockItemByStore()
        
        viewTester().waitForCellInTableView(at: IndexPath(row: 0, section: 0))?.tap()
        viewTester().usingLabel("Cancelar")?.usingTraits(.button)?.waitForTappableView()
        viewTester().usingLabel("Nova Transação")?.usingTraits(.header)?.waitForView()
        viewTester().usingLabel("Panela Veia")?.usingTraits(.staticText)?.waitForView()
        viewTester().usingLabel("Pagar")?.usingTraits(.button)?.waitForTappableView()
        viewTester().usingLabel("Cancelar")?.usingTraits(.button)?.tap()
    }
}
