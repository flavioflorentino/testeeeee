import OHHTTPStubs

protocol WalletMockable: Mockable {
    static func mockCreditAccount()
    static func mockPaymentAccount()
}

extension WalletMockable {
    static func mockCreditAccount() {
        stub(condition: isPath("/credit/account")) { _ in
            StubResponse.success(at: "CreditAccount.json")
        }
    }
    
    static func mockPaymentAccount() {
        stub(condition: isPath("/payment_account")) { _ in
            StubResponse.success(at: "PaymentAccount.json")
        }
    }
}

final class WalletMock: WalletMockable {
    static func mockAll() {
        mockCreditAccount()
        mockPaymentAccount()
    }
}
