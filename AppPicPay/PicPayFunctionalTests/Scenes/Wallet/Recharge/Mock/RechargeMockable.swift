import OHHTTPStubs

protocol RechargeMockable: Mockable {
    static func mockGetLastRecharge()
}

extension RechargeMockable {
    static func mockGetLastRecharge() {
        stub(condition: isPath("/api/getLastRecharge.json")) { _ in
            StubResponse.success(at: "GetLastRecharge.json")
        }
    }
}

final class RechargeMock: RechargeMockable {
    static func mockAll() {
        mockGetLastRecharge()
    }
}
