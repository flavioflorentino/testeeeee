import KIF
import XCTest
@testable import P2PLending
import OHHTTPStubs

final class LendingProposalAmountTests: KIFTestCase, MockableTestFlow {
    override func beforeEach() {
        super.beforeEach()
        initialApplicationState()
        mockFlows(using: LoginMock.self, FeedMock.self, FavoritesMock.self, WalletMock.self, LendingMock.self)
    }
    
    override func afterEach() {
        super.afterEach()
        OHHTTPStubs.removeAllStubs()
    }
    
    func navigateToProposalAmount() {
        navigateToRechargeMethods()
        let firstRow = IndexPath(row: 0, section: 0)
        viewTester()
            .tapRowInTableView(at: firstRow)
        viewTester()
            .usingIdentifier(LendingGreetingsViewController.AccessibilityIdentifier.legalTermsCheckbox.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LendingGreetingsViewController.AccessibilityIdentifier.makeProposalButton.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LendingProposalObjectivesViewController.AccessibilityIdentifier.objectivesTableView.rawValue)?
            .tapRowInTableView(at: firstRow)
        viewTester()
            .usingIdentifier(LendingProposalFriendsViewController.AccessibilityIdentifier.friendsTableView.rawValue)?
            .tapRowInTableView(at: firstRow)
        viewTester()
            .usingIdentifier(LendingProposalDescriptionViewController.AccessibilityIdentifier.continueButton.rawValue)?
            .tap()
    }
    
    func navigateToRechargeMethods() {
        navigateToHome()
        viewTester().usingLabel("Carteira")?.tap()
        viewTester().usingLabel("Adicionar")?.tap()
    }
    
    func testProposalAmount_WhenComingFromLendingGreetings_ShouldDisableContinueButtonAndDisplayAmountInstructions() throws {
        navigateToProposalAmount()
        
        let minimumFormattedValue = 100.toCurrencyString() ?? ""
        let maximumFormattedValue = 3_850.01.toCurrencyString() ?? ""
        
        let continueButton = try XCTUnwrap(
            viewTester()
                .usingIdentifier(LendingProposalAmountViewController.AccessibilityIdentifier.continueButton.rawValue)?
                .view as? UIButton
        )
        
        viewTester()
            .usingIdentifier(LendingProposalAmountViewController.AccessibilityIdentifier.headlineLabel.rawValue)?
            .waitForView()

        viewTester()
            .usingIdentifier(LendingProposalAmountViewController.AccessibilityIdentifier.amountInstructions.rawValue)?
            .expect(
                toContainText: Strings.Lending.Proposal.Amount.amountValueInstructions(
                    minimumFormattedValue,
                    maximumFormattedValue
                )
        )
        
        XCTAssertFalse(continueButton.isEnabled)
    }
    
    func testProposalAmount_WhenTypeValidValue_ShouldEnableContinueButtonAndDisplayAmountInstructions() throws {
        navigateToProposalAmount()
        
        let minimumFormattedValue = 100.toCurrencyString() ?? ""
        let maximumFormattedValue = 3_850.01.toCurrencyString() ?? ""
        
        let continueButton = try XCTUnwrap(
            viewTester()
                .usingIdentifier(LendingProposalAmountViewController.AccessibilityIdentifier.continueButton.rawValue)?
                .view as? UIButton
        )
        
        viewTester()
            .usingIdentifier(LendingProposalAmountViewController.AccessibilityIdentifier.amountTextField.rawValue)?
            .enterText("200000")
        
        viewTester()
            .usingIdentifier(LendingProposalAmountViewController.AccessibilityIdentifier.amountInstructions.rawValue)?
            .expect(
                toContainText: Strings.Lending.Proposal.Amount.amountValueInstructions(
                    minimumFormattedValue,
                    maximumFormattedValue
                )
        )
        
        XCTAssertTrue(continueButton.isEnabled)
    }
    
    func testProposalAmount_WhenTypeInvalidValue_ShouldDisplayOutOfBoundsMessageAndDisableContinueButton() throws {
        navigateToProposalAmount()
        
        let minimumFormattedValue = 100.toCurrencyString() ?? ""
        let maximumFormattedValue = 3_850.01.toCurrencyString() ?? ""
        
        let continueButton = try XCTUnwrap(
            viewTester()
                .usingIdentifier(LendingProposalAmountViewController.AccessibilityIdentifier.continueButton.rawValue)?
                .view as? UIButton
        )
        
        viewTester()
            .usingIdentifier(LendingProposalAmountViewController.AccessibilityIdentifier.amountTextField.rawValue)?
            .enterText("2000000")
        
        viewTester()
            .usingIdentifier(LendingProposalAmountViewController.AccessibilityIdentifier.amountInstructions.rawValue)?
            .expect(
                toContainText: Strings.Lending.Proposal.Amount.Error.amountValueOutOfBounds(
                    minimumFormattedValue,
                    maximumFormattedValue
                )
        )
        
        XCTAssertFalse(continueButton.isEnabled)
    }
    
}
