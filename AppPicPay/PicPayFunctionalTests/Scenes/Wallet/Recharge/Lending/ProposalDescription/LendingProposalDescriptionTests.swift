import KIF
import XCTest
import OHHTTPStubs
@testable import P2PLending

class LendingProposalDescriptionTests: KIFTestCase, MockableTestFlow {
    override func beforeEach() {
        super.beforeEach()
        initialApplicationState()
        mockFlows(using: LoginMock.self, FeedMock.self, FavoritesMock.self, WalletMock.self, LendingMock.self)
    }
    
    override func afterEach() {
        super.afterEach()
        OHHTTPStubs.removeAllStubs()
    }

    func navigateToProposalDescription() {
        navigateToRechargeMethods()
        let firstRow = IndexPath(row: 0, section: 0)
        viewTester()
            .tapRowInTableView(at: firstRow)
        viewTester()
            .usingIdentifier(LendingGreetingsViewController.AccessibilityIdentifier.legalTermsCheckbox.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LendingGreetingsViewController.AccessibilityIdentifier.makeProposalButton.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LendingProposalObjectivesViewController.AccessibilityIdentifier.objectivesTableView.rawValue)?
            .tapRowInTableView(at: firstRow)
        viewTester()
            .usingIdentifier(LendingProposalFriendsViewController.AccessibilityIdentifier.friendsTableView.rawValue)?
            .tapRowInTableView(at: firstRow)
    }
    
    func navigateToRechargeMethods() {
        navigateToHome()
        viewTester().usingLabel("Carteira")?.tap()
        viewTester().usingLabel("Adicionar")?.tap()
    }
    
    func testEnterTextViewText_WhenUserTapsContinue_ShouldContinue() {
        navigateToProposalDescription()
        viewTester()
            .usingIdentifier(LendingProposalDescriptionViewController.AccessibilityIdentifier.textView.rawValue)?
            .enterText("Ei, vou fazer o curso TOP que te falei! Acha que essas condições estão boas para você? Obrigado 😁")
        
        viewTester()
            .usingIdentifier(LendingProposalDescriptionViewController.AccessibilityIdentifier.continueButton.rawValue)?
            .tap()
        
        viewTester()
            .usingLabel(Strings.Lending.Proposal.Description.headline)?
            .waitForAbsenceOfView()
    }
}
