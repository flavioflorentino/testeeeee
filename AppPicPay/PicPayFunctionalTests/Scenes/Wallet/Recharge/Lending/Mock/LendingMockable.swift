@testable import P2PLending
import OHHTTPStubs

protocol LendingMockable: Mockable {
    static func mockProposalConfiguration(response: OHHTTPStubsResponse)
    static func mockProposalConditionsConfiguration(response: OHHTTPStubsResponse)
    static func mockProposalConditionsSimulation(response: OHHTTPStubsResponse)
    static func mockProposalFriends(response: OHHTTPStubsResponse)
    static func mockCCB(response: OHHTTPStubsResponse)
    static func mockProposalSend(response: OHHTTPStubsResponse)
    static func mockOpportunityOffer(identifier: UUID, response: OHHTTPStubsResponse)
    static func mockDeclineOffer(identifier: UUID, response: OHHTTPStubsResponse)
    static func mockConfigsMenu(response: OHHTTPStubsResponse)
    static func mockProposalList(response: OHHTTPStubsResponse)
}

extension LendingMockable {
    static func mockProposalConfiguration(response: OHHTTPStubsResponse = StubResponse.success(at: "GreetingsResponse.json")) {
        stub(condition: isPath("/p2plending/configs")) { _ in
            response
        }
    }
    
    static func mockProposalConditionsConfiguration(response: OHHTTPStubsResponse = StubResponse.success(at: "ProposalConditionsConfiguration.json")) {
        stub(condition: isPath("/p2plending/loan/simulation/config")) { _ in
            response
        }
    }
    
    static func mockProposalConditionsSimulation(response: OHHTTPStubsResponse = StubResponse.success(at: "ProposalConditionsSimulation.json")) {
        stub(condition: isPath("/p2plending/loan/simulation")) { _ in
            response
        }
    }
    
    static func mockProposalFriends(response: OHHTTPStubsResponse = StubResponse.success(at: "ProposalFriends.json")) {
        stub(condition: isPath("/p2plending/friends")) { _ in
            response
        }
    }
    
    static func mockCCB(response: OHHTTPStubsResponse = StubResponse.success(at: "CCBData.json")) {
        stub(condition: isPath("/p2plending/loan/simulation/ccb")) { _ in
            response
        }
    }
    
    static func mockProposalSend(response: OHHTTPStubsResponse = StubResponse.success(with: [:])) {
        stub(condition: isPath("/p2plending/loan/offers")) { _ in
            response
        }
    }
    
    static func mockOpportunityOffer(identifier: UUID, response: OHHTTPStubsResponse = StubResponse.success(at: "OpportunityOfferMock.json")) {
        stub(condition: isPath("/p2plending/loan/offers/\(identifier.uuidString)")) { _ in
            response
        }
    }
    
    static func mockDeclineOffer(identifier: UUID, response: OHHTTPStubsResponse = StubResponse.success(with: [:])) {
        stub(condition: isPath("/p2plending/loan/offers/\(identifier.uuidString)/investor/reject")) { _ in
            response
        }
    }
    
    static func mockConfigsMenu(response: OHHTTPStubsResponse = StubResponse.success(at: "ConfigsMenu.json")) {
        stub(condition: isPath("/p2plending/configs/menu")) { _ in
            response
        }
    }
    
    static func mockProposalList(response: OHHTTPStubsResponse = StubResponse.success(at: "ProposalList.json")) {
        stub(condition: isPath("/p2plending/loan/offers")) { _ in
            response
        }
    }
}

final class LendingMock: LendingMockable, RechargeMockable {
    static func mockAll() {
        mockGetLastRecharge()
        mockProposalConfiguration()
        mockProposalConditionsConfiguration()
        mockProposalConditionsSimulation()
        mockProposalFriends()
        mockProposalSend()
        mockCCB()
        mockConfigsMenu()
        mockProposalList()
    }
    
    static func mockGetLastRecharge() {
        stub(condition: isPath("/api/getLastRecharge.json")) { _ in
            StubResponse.success(at: "GetLastRechargeLending.json")
        }
    }
}
