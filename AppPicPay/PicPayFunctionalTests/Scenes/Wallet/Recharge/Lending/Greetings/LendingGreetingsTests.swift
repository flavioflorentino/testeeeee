import KIF
import XCTest
@testable import P2PLending
import OHHTTPStubs

final class LendingGreetingsTests: KIFTestCase, MockableTestFlow {
    override func beforeEach() {
        super.beforeEach()
        initialApplicationState()
        mockFlows(using: LoginMock.self, FeedMock.self, FavoritesMock.self, WalletMock.self)
        LendingMock.mockGetLastRecharge()
    }
    
    override func afterEach() {
        super.afterEach()
        OHHTTPStubs.removeAllStubs()
    }
    
    func navigateToRechargeMethods() {
        navigateToHome()
        viewTester().usingLabel("Carteira")?.tap()
        viewTester().usingLabel("Adicionar")?.tap()
    }
    
    func testOpenLendingGreetings_WhenReceiveSuccessStatus_ShouldDisplayGreetings() {
        navigateToRechargeMethods()
        LendingMock.mockProposalConfiguration()
        
        viewTester()
            .tapRowInTableView(at: IndexPath(row: 0, section: 0))
        viewTester()
            .usingIdentifier(LendingGreetingsViewController.AccessibilityIdentifier.bodyLabel.rawValue)?
            .waitForView()
        viewTester()
            .usingIdentifier(LendingGreetingsViewController.AccessibilityIdentifier.legalTermsCheckbox.rawValue)?
            .tap()
    }
    
    func testOpenLendingGreetings_WhenReceiveFailureBadRequestStatus_ShouldDisplayDetailedError() {
        navigateToRechargeMethods()
        LendingMock.mockProposalConfiguration(response: StubResponse.fail(fileAt: "GreetingsResponseError.json"))
        
        viewTester()
            .tapRowInTableView(at: IndexPath(row: 0, section: 0))
        viewTester()
            .usingIdentifier(LendingDetailedErrorViewController.AccessibilityIdentifier.titleLabel.rawValue)?
            .waitForView()
        viewTester()
            .usingIdentifier(LendingDetailedErrorViewController.AccessibilityIdentifier.dismissButton.rawValue)?
            .tap()
    }
    
    func testOpenLendingGreetings_WhenReceiveFailureServerErrorStatus_ShouldDisplayAlertAndExit() {
        navigateToRechargeMethods()
        LendingMock.mockProposalConfiguration(response: StubResponse.fail())
        
        viewTester()
            .tapRowInTableView(at: IndexPath(row: 0, section: 0))
        viewTester()
            .usingLabel(GlobalLocalizable.dismissButtonTitle)?
            .tap()
    }
}
