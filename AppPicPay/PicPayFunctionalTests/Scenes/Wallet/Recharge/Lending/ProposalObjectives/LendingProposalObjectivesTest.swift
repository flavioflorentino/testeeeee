import KIF
import XCTest
import OHHTTPStubs
@testable import PicPay
@testable import P2PLending

final class LendingProposalObjectivesTest: KIFTestCase, MockableTestFlow {
    override func beforeEach() {
        super.beforeEach()
        initialApplicationState()
        mockFlows(using: LoginMock.self, FeedMock.self, FavoritesMock.self, WalletMock.self, LendingMock.self)
    }
    
    override func afterEach() {
        super.afterEach()
        OHHTTPStubs.removeAllStubs()
    }
    
    func navigateToProposalObjectives() {
        navigateToRechargeMethods()
        viewTester()
            .tapRowInTableView(at: IndexPath(row: 0, section: 0))
        viewTester()
            .usingIdentifier(LendingGreetingsViewController.AccessibilityIdentifier.legalTermsCheckbox.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LendingGreetingsViewController.AccessibilityIdentifier.makeProposalButton.rawValue)?
            .tap()
    }
    
    func navigateToRechargeMethods() {
        navigateToHome()
        viewTester().usingLabel(DefaultLocalizable.wallet.text)?.tap()
        viewTester().usingLabel("Adicionar")?.tap()
    }
    
    func testSelectObjective_WhenObjectiveIsSelected_ShouldLeaveScene() {
        navigateToProposalObjectives()
        let indexPath = IndexPath(row: 0, section: 0)
        
        viewTester()
            .usingIdentifier(LendingProposalObjectivesViewController.AccessibilityIdentifier.objectivesTableView.rawValue)?
            .tapRowInTableView(at: indexPath)
        
        viewTester()
            .usingLabel(Strings.Lending.Proposal.Objectives.headline)?
            .waitForAbsenceOfView()
    }
}
