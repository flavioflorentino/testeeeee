import KIF
import XCTest
import OHHTTPStubs
@testable import P2PLending

final class LendingProposalConditionsTests: KIFTestCase, MockableTestFlow {
    override func beforeEach() {
        super.beforeEach()
        initialApplicationState()
        mockFlows(using: LoginMock.self, FeedMock.self, FavoritesMock.self, WalletMock.self, LendingMock.self)
    }
    
    override func afterEach() {
        super.afterEach()
        OHHTTPStubs.removeAllStubs()
    }
    
    func navigateToProposalConditions() {
        navigateToRechargeMethods()
        let firstRow: IndexPath = IndexPath(row: 0, section: 0)
        viewTester()
            .tapRowInTableView(at: firstRow)
        viewTester()
            .usingIdentifier(LendingGreetingsViewController.AccessibilityIdentifier.legalTermsCheckbox.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LendingGreetingsViewController.AccessibilityIdentifier.makeProposalButton.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LendingProposalObjectivesViewController.AccessibilityIdentifier.objectivesTableView.rawValue)?
            .tapRowInTableView(at: firstRow)
        viewTester()
            .usingIdentifier(LendingProposalFriendsViewController.AccessibilityIdentifier.friendsTableView.rawValue)?
            .tapRowInTableView(at: firstRow)
        viewTester()
            .usingIdentifier(LendingProposalDescriptionViewController.AccessibilityIdentifier.continueButton.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LendingProposalAmountViewController.AccessibilityIdentifier.amountTextField.rawValue)?
            .enterText("200000")
        viewTester()
            .usingIdentifier(LendingProposalAmountViewController.AccessibilityIdentifier.continueButton.rawValue)?
            .tap()
    }
    
    func navigateToRechargeMethods() {
        navigateToHome()
        viewTester().usingLabel("Carteira")?.tap()
        viewTester().usingLabel("Adicionar")?.tap()
    }
    
    func testDisplayConditions_WhenReceiveSuccessFromService_ShouldDisplayConditions() {
        navigateToProposalConditions()
        LendingMock.mockProposalConditionsSimulation()
        viewTester()
            .usingIdentifier(LendingProposalConditionsViewController.AccessibilityIdentifier.installmentsStepper.rawValue)?
            .enterText("12")
        
        viewTester()
            .usingIdentifier(LendingProposalConditionsViewController.AccessibilityIdentifier.interstStepper.rawValue)?
            .enterText("0")
        
        viewTester()
            .usingIdentifier(LendingProposalConditionsViewController.AccessibilityIdentifier.summaryConditions.rawValue)?
            .expect(toContainText: Strings.Lending.Proposal.Conditions.footerText("12x", "R$166,67"))
    }
    
    func testDisplayConditions_WhenReceiveFailureFromService_ShouldDisplayError() {
        navigateToProposalConditions()
        LendingMock.mockProposalConditionsSimulation(response: StubResponse.fail())
        viewTester()
            .usingIdentifier(LendingProposalConditionsViewController.AccessibilityIdentifier.installmentsStepper.rawValue)?
            .enterText("12")
        
        viewTester()
            .usingIdentifier(LendingProposalConditionsViewController.AccessibilityIdentifier.interstStepper.rawValue)?
            .enterText("0")
        
        viewTester()
            .usingIdentifier(LendingProposalConditionsViewController.AccessibilityIdentifier.summaryConditions.rawValue)?
            .expect(toContainText: Strings.Lending.Proposal.Conditions.simulationErrorText)
    }
    
    func testTryAgain_WhenReceiveFailureFromService_ShouldDisplayErrorAndTryAgain() {
        LendingMock.mockProposalConditionsSimulation(response: StubResponse.fail())
        
        navigateToProposalConditions()
        
        LendingMock.mockProposalConditionsSimulation()
        
        viewTester()
            .usingIdentifier(LendingProposalConditionsViewController.AccessibilityIdentifier.summaryConditions.rawValue)?
            .tap()
        
        viewTester()
            .usingIdentifier(LendingProposalConditionsViewController.AccessibilityIdentifier.summaryConditions.rawValue)?
            .expect(toContainText: Strings.Lending.Proposal.Conditions.footerText("12x", "R$166,67"))
    }
}
