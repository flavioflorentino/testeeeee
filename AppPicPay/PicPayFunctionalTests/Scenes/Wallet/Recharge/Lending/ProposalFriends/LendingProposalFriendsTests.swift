import KIF
import XCTest
import OHHTTPStubs
@testable import P2PLending

final class LendingProposalFriendsTests: KIFTestCase, MockableTestFlow {
    override func beforeEach() {
        super.beforeEach()
        initialApplicationState()
        mockFlows(using: LoginMock.self, FeedMock.self, FavoritesMock.self, WalletMock.self, LendingMock.self)
    }
    
    override func afterEach() {
        super.afterEach()
        OHHTTPStubs.removeAllStubs()
    }
    
    func navigateToProposalFriends() {
        navigateToRechargeMethods()
        let firstRow: IndexPath = IndexPath(row: 0, section: 0)
        viewTester()
            .tapRowInTableView(at: firstRow)
        viewTester()
            .usingIdentifier(LendingGreetingsViewController.AccessibilityIdentifier.legalTermsCheckbox.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LendingGreetingsViewController.AccessibilityIdentifier.makeProposalButton.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LendingProposalObjectivesViewController.AccessibilityIdentifier.objectivesTableView.rawValue)?
            .tapRowInTableView(at: firstRow)
    }
    
    func navigateToRechargeMethods() {
        navigateToHome()
        viewTester().usingLabel("Carteira")?.tap()
        viewTester().usingLabel("Adicionar")?.tap()
    }
    
    func testSelectFriend_WhenSelectRowFromTableView_ShouldLeaveView() {
        navigateToProposalFriends()
        
        viewTester()
            .usingIdentifier(LendingProposalFriendsViewController.AccessibilityIdentifier.friendsTableView.rawValue)?
            .tapRowInTableView(at: IndexPath(row: 0, section: 0))
        
        viewTester()
            .usingIdentifier(LendingProposalFriendsViewController.AccessibilityIdentifier.friendsTableView.rawValue)?
            .waitForAbsenceOfView()
    }
    
    func testSearchResult_WhenTypeSearchText_ShouldFilterTableWithContent() {
        navigateToProposalFriends()
        
        LendingMock.mockProposalFriends(response: StubResponse.success(at: "ProposalFriendsSearchResults.json"))
        
        viewTester()
            .usingIdentifier(LendingProposalFriendsViewController.AccessibilityIdentifier.searchBar.rawValue)?
            .enterText("Michelle")
        
        viewTester()
            .wait(forTimeInterval: 0.5)
        
        viewTester()
            .usingIdentifier(LendingProposalFriendsViewController.AccessibilityIdentifier.friendsTableView.rawValue)?
            .expect(toContainText: "Michelle")
    }
    
    func testEmptyState_WhenReceiveSuccessFromServiceWithEmptyList_ShouldDisplayEmptyListMessage() throws {
        let emptyData = try XCTUnwrap(try JSONEncoder().encode([String]()), "unable to enconde empty array")
        
        LendingMock.mockProposalFriends(response: StubResponse.success(with: emptyData))
        navigateToProposalFriends()
        
        viewTester()
            .usingIdentifier(LendingProposalFriendsViewController.AccessibilityIdentifier.friendsTableView.rawValue)?
            .expect(toContainText: Strings.Lending.Proposal.Friends.emptyListTitle)
    }
    
    func testEmptySearchResults_WhenReceiveSuccessFromServiceWithEmptyList_ShouldDisplayEmptySearchResultsMessage() throws {
        let emptyData = try XCTUnwrap(try JSONEncoder().encode([String]()), "unable to enconde empty array")
        
        navigateToProposalFriends()
        LendingMock.mockProposalFriends(response: StubResponse.success(with: emptyData))
        viewTester()
            .usingIdentifier(LendingProposalFriendsViewController.AccessibilityIdentifier.searchBar.rawValue)?
            .enterText("Some query")
        
        viewTester()
            .usingIdentifier(LendingProposalFriendsViewController.AccessibilityIdentifier.friendsTableView.rawValue)?
            .expect(toContainText: Strings.Lending.Proposal.Friends.emptySearchResultsTitle)
    }
    
    func testErrorAlert_WhenReceiveFailureFromService_ShouldDisplayErrorAlert() {
        LendingMock.mockProposalFriends(response: StubResponse.fail())
        navigateToProposalFriends()
        
        viewTester()
            .usingIdentifier(LendingProposalFriendsViewController.AccessibilityIdentifier.friendsTableView.rawValue)?
            .expect(toContainText: Strings.Lending.Proposal.Friends.emptyListTitle)
    }
}
