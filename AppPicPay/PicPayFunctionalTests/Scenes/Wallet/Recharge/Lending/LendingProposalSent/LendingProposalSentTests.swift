import KIF
import XCTest
import OHHTTPStubs
@testable import P2PLending

final class LendingProposalSentTests: KIFTestCase, MockableTestFlow {
    override func beforeEach() {
        super.beforeEach()
        initialApplicationState()
        mockFlows(using: LoginMock.self, FeedMock.self, FavoritesMock.self, WalletMock.self, LendingMock.self)
    }
    
    override func afterEach() {
        super.afterEach()
        OHHTTPStubs.removeAllStubs()
    }

    func navigateToProposalSent() {
        navigateToRechargeMethods()
        let firstRow = IndexPath(row: 0, section: 0)
        viewTester()
            .tapRowInTableView(at: firstRow)
        viewTester()
            .usingIdentifier(LendingGreetingsViewController.AccessibilityIdentifier.legalTermsCheckbox.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LendingGreetingsViewController.AccessibilityIdentifier.makeProposalButton.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LendingProposalObjectivesViewController.AccessibilityIdentifier.objectivesTableView.rawValue)?
            .tapRowInTableView(at: firstRow)
        viewTester()
            .usingIdentifier(LendingProposalFriendsViewController.AccessibilityIdentifier.friendsTableView.rawValue)?
            .tapRowInTableView(at: firstRow)
        viewTester()
            .usingIdentifier(LendingProposalDescriptionViewController.AccessibilityIdentifier.continueButton.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LendingProposalAmountViewController.AccessibilityIdentifier.amountTextField.rawValue)?
            .enterText("200000")
        viewTester()
            .usingIdentifier(LendingProposalAmountViewController.AccessibilityIdentifier.continueButton.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LendingProposalConditionsViewController.AccessibilityIdentifier.continueButton.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LendingProposalSummaryViewController.AccessibilityIdentifier.continueButton.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LendingCCBViewController.AccessibilityIdentifier.continueButton.rawValue)?
            .tap()
    }
    
    func navigateToRechargeMethods() {
        navigateToHome()
        viewTester().usingLabel("Carteira")?.tap()
        viewTester().usingLabel("Adicionar")?.tap()
    }
    
    func testDismissViewController_WhenTapDismissButton_ShouldDismissViewController() {
        navigateToProposalSent()
        
        viewTester()
            .usingIdentifier(LendingSuccessViewController.AccessibilityIdentifier.titleLabel.rawValue)?
            .waitForView()
        
        viewTester()
            .usingIdentifier(LendingSuccessViewController.AccessibilityIdentifier.continueButton.rawValue)?
            .tap()
        
        viewTester()
            .usingIdentifier(LendingSuccessViewController.AccessibilityIdentifier.titleLabel.rawValue)?
            .waitForAbsenceOfView()
    }
}
