import KIF
import XCTest
import OHHTTPStubs
@testable import P2PLending

final class LendingOpportunityOfferTests: KIFTestCase, MockableTestFlow {
    private typealias Localizable = Strings.Lending.Opportunity.Offer
    
    private lazy var identifier = UUID()
    
    
    override func beforeEach() {
        super.beforeEach()
        initialApplicationState()
        mockFlows(using: LoginMock.self, FeedMock.self, FavoritesMock.self, WalletMock.self)
    }
    
    override func afterEach() {
        super.afterEach()
        OHHTTPStubs.removeAllStubs()
    }
    
    func navigateToOpportunityOffer() {
        navigateToHome()
        let app = UIApplication.shared
        let currentController = app.keyWindow?.rootViewController
        let opportunityOfferController = LendingOpportunityOfferFactory.make(offerIdentifier: identifier)
        let navigationController = UINavigationController(rootViewController: opportunityOfferController)
        navigationController.modalPresentationStyle = .fullScreen
        currentController?.present(navigationController, animated: true, completion: nil)
    }
    
    func testLoadOpportunityOffer_WhenReceiveSuccessFromServer_ShouldDisplayOffer() {
        navigateToOpportunityOffer()
        LendingMock.mockOpportunityOffer(identifier: identifier)
        
        viewTester()
            .usingLabel(Localizable.installmentsItemTitle)?
            .waitForView()
    }
    
    func testLoadOpportunityOffer_WhenReceiveFailureFromServer_ShouldDisplayErrorAlert() {
        navigateToOpportunityOffer()
        LendingMock.mockOpportunityOffer(identifier: identifier, response: StubResponse.fail(with: 500))
        
        viewTester()
            .usingLabel(GlobalErrorAlertLocalizable.title)?
            .waitForView()
    }
    
    func testDeclineOpportunityOffer_WhenReceiveSuccessFromServer_ShouldDisplaySuccessController() {
        navigateToOpportunityOffer()
        LendingMock.mockOpportunityOffer(identifier: identifier)
        LendingMock.mockDeclineOffer(identifier: identifier)
        
        viewTester()
            .usingIdentifier(LendingOpportunityOfferViewController.AccessibilityIdentifier.declineButton.rawValue)?
            .tap()
        viewTester()
            .usingLabel(Localizable.declineOfferTitle)?
            .waitForView()
        viewTester()
            .usingLabel(Localizable.declineOfferButtonTitle)?
            .tap()
        viewTester()
            .usingLabel(Localizable.offerDeclinedTitle)?
            .waitForView()
    }
    
    func testDeclineOpportunityOffer_WhenReceiveFailureFromServer_ShouldDisplayErrorAlert() {
        navigateToOpportunityOffer()
        LendingMock.mockOpportunityOffer(identifier: identifier)
        LendingMock.mockDeclineOffer(identifier: identifier, response: StubResponse.fail())
        
        viewTester()
            .usingIdentifier(LendingOpportunityOfferViewController.AccessibilityIdentifier.declineButton.rawValue)?
            .tap()
        viewTester()
            .usingLabel(Localizable.declineOfferTitle)?
            .waitForView()
        viewTester()
            .usingLabel(Localizable.declineOfferButtonTitle)?
            .tap()
        viewTester()
            .usingLabel(GlobalLocalizable.errorAlertTitle)?
            .waitForView()
    }
}
