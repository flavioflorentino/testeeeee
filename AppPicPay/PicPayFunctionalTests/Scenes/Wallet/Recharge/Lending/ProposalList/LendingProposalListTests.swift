import KIF
import XCTest
import OHHTTPStubs
import FeatureFlag
@testable import PicPay
@testable import P2PLending

final class LendingProposalListTests: KIFTestCase, MockableTestFlow {
    private typealias Localizable = P2PLending.Strings.Lending.Proposal.List
    
    override func beforeEach() {
        super.beforeEach()
        initialApplicationState()
        let remoteConfigMock = RemoteConfigMock()
        RemoteConfigSetup.inject(instance: remoteConfigMock)
        remoteConfigMock.override(key: .releaseP2PLending, with: true)
        mockFlows(using: LoginMock.self, FeedMock.self, FavoritesMock.self, LendingMock.self)
    }
    
    override func afterEach() {
        super.afterEach()
        OHHTTPStubs.removeAllStubs()
    }
    
    func testOpenMyProposals_WhenTapMyProposalsRowInSettings_ShouldShowMyProposalsList() {
        navigateToConfig()
        
        viewTester()
            .usingLabel(SettingsStrings.rowMyProposals.rawValue)?
            .tap()
        
        viewTester()
            .usingLabel(Localizable.borrowerHeadline)?
            .waitForView()
    }
    
    func testShowMyInvestments_WhenTapMyInvestmentsRowInSettings_ShouldShowMyInvestmentsList() {
        navigateToConfig()
        
        viewTester()
            .usingLabel(SettingsStrings.rowMyInvestments.rawValue)?
            .tap()
        
        viewTester()
            .usingLabel(Localizable.investorHeadline)?
            .waitForView()
    }
}
