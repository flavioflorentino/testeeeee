import KIF
import XCTest
import OHHTTPStubs
@testable import P2PLending

final class LendingProposalSummaryTests: KIFTestCase, MockableTestFlow {
    override func beforeEach() {
        super.beforeEach()
        initialApplicationState()
        mockFlows(using: LoginMock.self, FeedMock.self, FavoritesMock.self, WalletMock.self, LendingMock.self)
    }
    
    override func afterEach() {
        super.afterEach()
        OHHTTPStubs.removeAllStubs()
    }

    func navigateToProposalSummary() {
        navigateToRechargeMethods()
        let firstRow = IndexPath(row: 0, section: 0)
        viewTester()
            .tapRowInTableView(at: firstRow)
        viewTester()
            .usingIdentifier(LendingGreetingsViewController.AccessibilityIdentifier.legalTermsCheckbox.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LendingGreetingsViewController.AccessibilityIdentifier.makeProposalButton.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LendingProposalObjectivesViewController.AccessibilityIdentifier.objectivesTableView.rawValue)?
            .tapRowInTableView(at: firstRow)
        viewTester()
            .usingIdentifier(LendingProposalFriendsViewController.AccessibilityIdentifier.friendsTableView.rawValue)?
            .tapRowInTableView(at: firstRow)
        viewTester()
            .usingIdentifier(LendingProposalDescriptionViewController.AccessibilityIdentifier.continueButton.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LendingProposalAmountViewController.AccessibilityIdentifier.amountTextField.rawValue)?
            .enterText("200000")
        viewTester()
            .usingIdentifier(LendingProposalAmountViewController.AccessibilityIdentifier.continueButton.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(LendingProposalConditionsViewController.AccessibilityIdentifier.continueButton.rawValue)?
            .tap()
    }
    
    func navigateToRechargeMethods() {
        navigateToHome()
        viewTester().usingLabel("Carteira")?.tap()
        viewTester().usingLabel("Adicionar")?.tap()
    }
    
    func testExpandViewButton_WhenTapped_ShouldExpandToShowIndividualCostsArea() {
        navigateToProposalSummary()
        
        viewTester()
            .usingIdentifier(LendingProposalSummaryViewController.AccessibilityIdentifier.expandableViewButtonToggle.rawValue)?
            .tap()
        
        viewTester()
            .usingIdentifier(LendingProposalSummaryViewController.AccessibilityIdentifier.individualCostLabel.rawValue)?
            .waitForView()
    }
    
    func testExpandViewButton_WhenTappedTwice_ShouldExpandToShowAndHideIndividualCostsArea() {
        navigateToProposalSummary()
        
        viewTester()
            .usingIdentifier(LendingProposalSummaryViewController.AccessibilityIdentifier.expandableViewButtonToggle.rawValue)?
            .tap()
        
        viewTester()
            .usingIdentifier(LendingProposalSummaryViewController.AccessibilityIdentifier.individualCostLabel.rawValue)?
            .waitForView()
        
        viewTester()
            .usingIdentifier(LendingProposalSummaryViewController.AccessibilityIdentifier.expandableViewButtonToggle.rawValue)?
            .tap()
        
        viewTester()
            .usingIdentifier(LendingProposalSummaryViewController.AccessibilityIdentifier.individualCostLabel.rawValue)?
            .waitForAbsenceOfView()
    }
}
