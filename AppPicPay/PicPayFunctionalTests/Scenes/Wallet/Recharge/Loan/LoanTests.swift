import KIF
import OHHTTPStubs
@testable import PicPay
@testable import Loan

final class LoanTests: KIFTestCase, MockableTestFlow {
    
    private lazy var remoteConfigMock = RemoteConfigMock()
    
    override func beforeEach() {
        super.beforeEach()
        initialApplicationState()
        RemoteConfigSetup.inject(instance: remoteConfigMock)
        remoteConfigMock.override(key: .featureOpenAppTabV2, with: #"{ "tab": "feed", "sub_tab": "" }"#)
        remoteConfigMock.override(key: .featureLoan, with: true)

        mockFlows(using: LoginMock.self, FavoritesMock.self, FeedMock.self, LoanMock.self)
    }

    override func afterEach() {
        super.afterEach()
        OHHTTPStubs.removeAllStubs()
    }
    
    func testSimulation_WhenTappingMinusInstallments_ShouldChangeDescriptionValues() {
        navigateToSimulationScreen()
        
        // Minus
        tap(StepperView.Accessibility.minusButton.rawValue)
        viewTester()
            .usingIdentifier(InstallmentsInfoView.Accessibility.installmentInfoLabel.rawValue)?
            .expect(toContainText: "Você vai pagar em\n3x de R$ 201,85")
        tap(StepperView.Accessibility.minusButton.rawValue)
        viewTester()
            .usingIdentifier(InstallmentsInfoView.Accessibility.installmentInfoLabel.rawValue)?
            .expect(toContainText: "Você vai pagar em\n2x de R$ 295,92")
        tap(StepperView.Accessibility.minusButton.rawValue)
        viewTester()
            .usingIdentifier(InstallmentsInfoView.Accessibility.installmentInfoLabel.rawValue)?
            .expect(toContainText: "Você vai pagar em\n1x de R$ 578,37")
        
        // Plus
        tap(StepperView.Accessibility.plusButton.rawValue)
        viewTester()
            .usingIdentifier(InstallmentsInfoView.Accessibility.installmentInfoLabel.rawValue)?
            .expect(toContainText: "Você vai pagar em\n2x de R$ 295,92")
        tap(StepperView.Accessibility.plusButton.rawValue)
        viewTester()
            .usingIdentifier(InstallmentsInfoView.Accessibility.installmentInfoLabel.rawValue)?
            .expect(toContainText: "Você vai pagar em\n3x de R$ 201,85")
        tap(StepperView.Accessibility.plusButton.rawValue)
        viewTester()
            .usingIdentifier(InstallmentsInfoView.Accessibility.installmentInfoLabel.rawValue)?
            .expect(toContainText: "Você vai pagar em\n4x de R$ 154,87")
    }
    
    func testTermsOfLoan_WhenTappingContractDetails_ShouldShowErrorAlertAndThenOpenExternalWebView() {
        navigateToSimulationScreen()
        
        viewTester()
            .usingIdentifier(SimulationViewController.Accessibility.nextButton.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(TermsViewController.Accessibility.hireLoanButton.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(TermsViewController.Accessibility.alertOkButton.rawValue)?
            .tap()
        viewTester()
            .usingLabel("Eu concordo com o Resumo Contratual e a Cédula de Crédito Bancário.")?
            .tap()
    }
    
    func testSimulation_WhenFailedBackend_ShouldShowErrorAlert() {
        LoanMock.mockSimulations(false)

        navigateToSimulationScreen()
        viewTester()
            .usingIdentifier(SimulationViewController.Accessibility.alertOkButton.rawValue)?
            .tap()
    }

    func testSimulation_WhenTappingConfirm_ShouldShowTermsOfContract() {
        navigateToSimulationScreen()

        tap(SimulationViewController.Accessibility.nextButton.rawValue)
    }

    func testSimulation_WhenTappingConfirmWithFailedBackend_ShouldShowErrorAlert() {
        LoanMock.mockProposals(false)
        navigateToSimulationScreen()

        viewTester()
            .usingIdentifier(SimulationViewController.Accessibility.nextButton.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(SimulationViewController.Accessibility.alertOkButton.rawValue)?
            .waitForTappableView()
            .tap()
    }
    
    func testSuccessCredit_WhenDoingCompleteFlow_ShouldShowSuccessScreen() {
        navigateToSimulationScreen()
        
        tap(SimulationViewController.Accessibility.nextButton.rawValue)
        tap(TermsViewController.Accessibility.hireLoanButton.rawValue)
        tap(TermsViewController.Accessibility.alertOkButton.rawValue)
        tap(AgreementContractView.Accessibility.radialButton.rawValue)
        tap(TermsViewController.Accessibility.hireLoanButton.rawValue)
        viewTester().wait(forTimeInterval: 1)
        tap(ConfirmationViewController.Accessibility.hireLoanButton.rawValue)
        viewTester().usingLabel("Digite sua senha")?.enterText("12345")
        viewTester().usingLabel("Continuar")?.tap()
        tap(LoanWarningViewController.Accessibility.actionButton.rawValue)
    }
    
    func testRechargeMethods_WhenFeatureFlagIsOff_ShouldNotShowGreenBanner() {
        remoteConfigMock.override(key: .featureLoan, with: false)
        
        navigateToRechargeMethods()
        viewTester()
            .usingIdentifier(RechargeMethodsViewController.Accessibility.loanTableViewCell.rawValue)?
            .waitForAbsenceOfView()
    }
    
    private func navigateToRechargeMethods() {
        navigateToWallet()
        viewTester()
            .usingLabel("Adicionar")?
            .tap()
    }
    
    private func navigateToSimulationScreen() {
        navigateToRechargeMethods()
        viewTester()
            .usingIdentifier(RechargeMethodsViewController.Accessibility.loanTableViewCell.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(IntroViewController.Accessibility.nextButton.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(ReasonView.Accessibility.backgroundView.rawValue)?
            .tap()
        viewTester()
            .usingIdentifier(SimulationViewController.Accessibility.backgroundView.rawValue)?
            .tap()
    }
}
