import OHHTTPStubs

protocol LoanMockable: Mockable {
    static func mockLimits(_ success: Bool)
    static func mockSimulations(_ success: Bool)
    static func mockProposals(_ success: Bool)
    static func mockContractURL(_ success: Bool)
    static func mockConfirmation(_ success: Bool)
    static func mockIdentifyContacts()
}

extension LoanMockable {
    static func mockLimits(_ success: Bool = true) {
        stub(condition: isPath("/personal-credit/limits")) { _ in
            return success ? StubResponse.success(at: "limits.json") : StubResponse.fail(with: 500)
        }
    }
    
    static func mockSimulations(_ success: Bool = true) {
        stub(condition: isPath("/personal-credit/simulations")) { _ in
            let response = success ? StubResponse.success(at: "simulations.json") : StubResponse.fail(with: 500)
            return response
        }
    }
    
    static func mockProposals(_ success: Bool = true) {
        stub(condition: isPath("/personal-credit/simulations/5e7248e5553667645f434e12/proposals")) { _ in
            let response = success ? StubResponse.success(at: "proposals.json") : StubResponse.fail(with: 500)
            return response
        }
    }
    
    static func mockContractURL(_ success: Bool = true) {
        stub(condition: isPath("/personal-credit/simulations/5e7248e5553667645f434e12/previews")) { _ in
            let response = success ? StubResponse.success(at: "contracturl.json") : StubResponse.fail(with: 500)
            return response
        }
    }
    
    static func mockIdentifyContacts() {
        stub(condition: isPath("/api/identifyContacts.json")) { _ in
            return StubResponse.success(at: "identifyContacts.json")
        }
    }
    
    static func mockConfirmation(_ success: Bool = true) {
        stub(condition: isPath("/personal-credit/contracts")) { _ in
            return StubResponse.success(at: "confirmation.json")
        }
    }
}

final class LoanMock: LoanMockable, WalletMockable, RechargeMockable {
    static func mockAll() {
        mockPing()
        mockCreditAccount()
        mockGetLastRecharge()
        mockPaymentAccount()
        mockAdvertisingBannerHome()
        mockLimits()
        mockSimulations()
        mockProposals()
        mockContractURL()
        mockIdentifyContacts()
        mockConfirmation()
    }
    
    static func mockGetLastRecharge() {
        stub(condition: isPath("/api/getLastRecharge.json")) { _ in
            return StubResponse.success(at: "getLastRechargeLoan.json")
        }
    }
    
    static func mockAdvertisingBannerHome() {
        stub(condition: isPath("/advertising/banner")) { _ in
            return StubResponse.success(at: "advertising_banner.json")
        }
    }
    
    static func mockPing() {
        stub(condition: isPath("/api/ping.json")) { _ in
            return StubResponse.success(with: [:])
        }
    }
}

