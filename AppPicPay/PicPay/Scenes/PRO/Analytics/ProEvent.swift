import Foundation
import AnalyticsModule

enum ProEvent: AnalyticsKeyProtocol {
    case offerViewed
    case offerInteracted(option: OptionOfferPro)
    case installmentsConfigurationSelected
    case installmentsConfigurationEnabled(numberOfInstallments: Int)
    case leaveViewed
    case leaveSelected
    case leaveConfirmationSelected
    
    private var name: String {
        switch self {
        case .offerViewed:
            return "PicPay PRO Offer Viewed"
        case .offerInteracted:
            return "Offer Interacted"
        case .installmentsConfigurationSelected:
            return "Interest-free installments Selected"
        case .installmentsConfigurationEnabled:
            return "Interest-free installments Enabled"
        case .leaveViewed:
            return "Leave PRO Viewed"
        case .leaveSelected:
            return "Leave PRO Selected"
        case .leaveConfirmationSelected:
            return "Leave PRO Confirmation Selected"
        }
    }
    
    var properties: [String: Any] {
        switch self {
        case let .offerInteracted(option: option):
            return ["option": option.rawValue]
        case let .installmentsConfigurationEnabled(installments):
            return ["installments": installments]
        default:
            return [:]
        }
    }
    
    var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("PRO - \(name)", properties: properties, providers: providers)
    }
}

extension ProEvent {
    enum OptionOfferPro: String {
        case business = "empresa"
        case liberalProfessional = "profissional liberal"
    }
}
