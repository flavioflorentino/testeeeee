import Foundation
import WebKit

struct ScriptMessageHandler {
    // MARK: - Properties
    private let wrapper: ScriptMessageWrapper

    let messageName: String
    var returnMessageName: String?
    var returnMessageValueClosure: (() -> String?)?

    // MARK: - Initialization
    init(messageName: String, returnMessageName: String? = nil, returnMessageValueClosure: (() -> String?)? = nil) {
        self.messageName = messageName
        self.returnMessageName = returnMessageName
        self.returnMessageValueClosure = returnMessageValueClosure

        wrapper = ScriptMessageWrapper(messageName: messageName)
        wrapper.didReceiveMessage = didReceiveMessage
    }

    // MARK: - Methods
    func register(at userContentController: WKUserContentController) {
        userContentController.add(wrapper, name: messageName)
    }
}

// MARK: - Private extension
private extension ScriptMessageHandler {
    var returnMessage: String? {
        guard let returnName = returnMessageName, let value = returnMessageValueClosure?() else {
            return nil
        }

        return "\(returnName)('\(value)')"
    }

    func didReceiveMessage(_ message: WKScriptMessage) {
        guard let returnMessage = returnMessage else {
            return
        }

        message.webView?.evaluateJavaScript(returnMessage)
    }
}
