extension ScriptMessageHandler {
    static func userToken(userManager: UserManagerContract) -> ScriptMessageHandler {
        ScriptMessageHandler(name: .getToken, returnName: .setToken) {
            userManager.token
        }
    }

    static func consumerId(consumerManager: ConsumerManagerContract) -> ScriptMessageHandler {
        ScriptMessageHandler(name: .getConsumerId, returnName: .setConsumerId) {
            guard let id = consumerManager.consumer?.wsId else {
                return nil
            }
            return "\(id)"
        }
    }

    static func installationId(installationId: String?) -> ScriptMessageHandler {
        ScriptMessageHandler(name: .getInstallationId, returnName: .setInstallationId) {
            installationId
        }
    }
}

// MARK: - Private extension
private extension ScriptMessageHandler {
    init(name: MessageName, returnName: MessageName?, returnValueClosure: (() -> String?)?) {
        self.init(messageName: name.rawValue,
                  returnMessageName: returnName?.rawValue,
                  returnMessageValueClosure: returnValueClosure)
    }
}

// MARK: - MessageNames nested type
private extension ScriptMessageHandler {
    enum MessageName: String {
        // MARK: - Token
        case getToken
        case setToken

        // MARK: - Consumer Id
        case getConsumerId
        case setConsumerId

        // MARK: - Installation Id
        case getInstallationId
        case setInstallationId
    }
}
