import Foundation
import WebKit

final class ScriptMessageWrapper: NSObject {
    // MARK: - Properties
    let messageName: String
    var didReceiveMessage: ((WKScriptMessage) -> Void)?

    // MARK: - Initialization
    init(messageName: String) {
        self.messageName = messageName
    }
}

// MARK: - WKScriptMessageHandler extension
extension ScriptMessageWrapper: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        guard let host = message.webView?.url?.host,
            host.contains("picpay.com") || host.contains("picpay.me") || host.contains("ppay.me") else {
            return
        }

        guard message.name == messageName else {
            return
        }

        didReceiveMessage?(message)
    }
}
