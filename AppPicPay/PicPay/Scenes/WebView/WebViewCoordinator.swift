import UIKit

enum WebViewAction {
    case closeModal
    case completed(_ error: Error?)
    case open(url: URL?)
}

protocol WebViewCoordinatorProtocol: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: WebViewAction)
}

final class WebViewCoordinator: WebViewCoordinatorProtocol {
    typealias WebViewCompletion = (_ error: Error?) -> Void
    weak var viewController: UIViewController?
    
    private let completion: WebViewCompletion?
    
    init(withCompletion: WebViewCompletion? = nil) {
        self.completion = withCompletion
    }
    
    func perform(action: WebViewAction) {
        switch action {
        case .closeModal:
            viewController?.dismiss(animated: true)
            
        case .completed(let error):
            viewController?.navigationController?.popViewController(animated: true)
            completion?(error)
            
        case .open(let url?):
            UIApplication.shared.open(url)
        
        default:
            break
        }
    }
}
