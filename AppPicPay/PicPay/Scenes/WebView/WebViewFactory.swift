import Core
import Foundation

final class WebViewFactory: NSObject {
    typealias WebViewCompletion = (_ error: Error?) -> Void
    
    @objc
    static func make(
        with url: URL,
        includeHeaders: Bool = false,
        customHeaders: [String: String]? = [:],
        shouldObfuscate: Bool = false,
        completion: WebViewCompletion? = nil
    ) -> UIViewController {
        let container = DependencyContainer()
        let service = WebViewService(dependencies: container)

        let scriptHandlers: [ScriptMessageHandler] = [
            .userToken(userManager: container.userManager),
            .consumerId(consumerManager: container.consumerManager),
            .installationId(installationId: service.installationId)
        ]
        
        let viewModel: WebViewModelProtocol = WebViewModel(
            withURL: url,
            sendHeaders: includeHeaders,
            service: service,
            shouldObfuscate: shouldObfuscate,
            customHeaders: customHeaders ?? [:],
            scriptHandlers: scriptHandlers
        )
        
        let coordinator: WebViewCoordinatorProtocol = WebViewCoordinator(withCompletion: completion)
        let viewController = WebViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController

        return viewController
    }
}

enum WsWebViewURL: String {
    case termsOfUse = "api/terms"
    case mgmTerms = "api/mgmTerms"
    case privacyPolicy = "api/privacy"
    case passRemember = "https://picpay.com/password"
    case greenAugust = "api/greenMonthAboutWebview"
    case proAboutWebview = "api/proAboutWebview"
    case bizAboutWebview = "api/bizAboutWebview"
    case aboutMGB = "https://cdn.picpay.com/webview/mgm/mgm-biz.html"
    case mgbTerms = "https://www.picpay.com/app_webviews/terms_mgm_biz/"
    case originalOAuth = "api/originalOpenBankingAuthUrl"
    case streamlabs = "https://iw9hbq7q6h.execute-api.us-east-1.amazonaws.com/prod/streamlabs"
    case streamlabsToken = "https://iw9hbq7q6h.execute-api.us-east-1.amazonaws.com/prod/streamlabs/token"
    case aboutSubscriptions = "https://assinaturas.picpay.com"
    case aboutCielo = "https://cdn.picpay.com/apps/picpay/cielo/cielo-webview.html"
    case aboutP2mAcquirers = "https://s3.amazonaws.com/cdn.picpay.com/apps/picpay/maquinhas/adquirente.html"
    case loanFAQ = "picpay://picpay/helpcenter/article/360050390651"
    case taxAndLimits = "https://cdn.picpay.com/CashIn/102020/Taxas_e_limites/webview.html"
    
    var endpoint: String {
        WebServiceInterface.apiEndpoint(self.rawValue)
    }
}

extension Result where Success == Void {
    static var success: Result {
        Result.success(())
    }
    
    public func getError() -> Error? {
        guard case .failure(let error) = self else {
            return nil
        }
        return error
    }
}
