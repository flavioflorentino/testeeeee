import Core
import CoreTelephony
import Foundation

protocol WebViewServiceProtocol {
    var consumerWsId: Int? { get }
    var userToken: String? { get }
    var appVersion: String? { get }
    var machineName: String? { get }
    var carrierName: String? { get }
    var installationId: String? { get }
    var location: LocationDescriptor? { get }
}

final class WebViewService: WebViewServiceProtocol {
    typealias Dependencies = HasConsumerManager & HasLocationManager & HasUserManager
    private var dependencies: Dependencies

    var consumerWsId: Int? {
        dependencies.consumerManager.consumer?.wsId
    }

    var userToken: String? {
        let userManager = dependencies.userManager
        return userManager.isAuthenticated ? userManager.token : nil
    }

    var appVersion: String? {
        Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
    }

    var machineName: String? {
        UIDevice.current.deviceModel
    }

    var carrierName: String? {
        let netInfo = CTTelephonyNetworkInfo()
        return netInfo.subscriberCellularProvider?.carrierName
    }

    var installationId: String? {
        UIDevice.current.installationId
    }
    
    var location: LocationDescriptor? {
        LocationDescriptor(dependencies.locationManager.authorizedLocation)
    }
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}
