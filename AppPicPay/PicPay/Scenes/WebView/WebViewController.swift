import Core
import Loan
import SecurityModule
import UI
import UIKit
import WebKit

final class WebViewController: LegacyViewController<WebViewModelProtocol, WebViewCoordinatorProtocol, UIView>, Obfuscable, FAQWebView {
    enum Layout {
        static let topOffset: CGFloat = 10.0
        static let leftOffset: CGFloat = 20.0
        static let dismissSize: CGFloat = 48.0
    }
    
    private lazy var webView: WKWebView = {
        let webview = WKWebView(frame: .zero)
        webview.translatesAutoresizingMaskIntoConstraints = false
        webview.isUserInteractionEnabled = true
        webview.isMultipleTouchEnabled = true
        webview.navigationDelegate = self
        return webview
    }()
    
    private lazy var dismissButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "ic_dismiss_webview"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(didTapDismiss), for: .touchUpInside)
        return button
    }()
    
    private lazy var loadingView: LoadingTableViewLine = {
        let loadingView = LoadingTableViewLine(width: UIScreen.main.bounds.width)
        loadingView.isHidden = true
        view.addSubview(loadingView)
        return loadingView
    }()
    
    // Screen obfuscation
    var appSwitcherObfuscator: AppSwitcherObfuscating?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        presentingViewController != nil ? .lightContent : .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.inputs.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.inputs.viewWillAppear()
        setupCloseButtons()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewModel.inputs.viewDidDisappear()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(webView)
    }
    
    override func configureViews() {
        title = DefaultLocalizable.webviewLoading.text
        
        if #available(iOS 13.0, *) {
            webView.scrollView.contentInsetAdjustmentBehavior = .never
        }
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.constraintAllEdges(from: webView, to: view)
    }
    
    private func setupCloseButtons() {
        guard
            presentingViewController != nil,
            navigationController?.viewControllers.count == 1
            else {
                return
        }
        
        if navigationController?.isNavigationBarHidden == false {
            let closeButton = UIBarButtonItem(
                title: DefaultLocalizable.btClose.text,
                style: .plain,
                target: self,
                action: #selector(didTapDismiss)
            )
            navigationController?.topViewController?.navigationItem.leftBarButtonItem = closeButton
        } else {
            view.addSubview(dismissButton)
            NSLayoutConstraint.activate([
                dismissButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Layout.leftOffset),
                dismissButton.topAnchor.constraint(equalTo: view.topAnchor, constant: Layout.topOffset),
                dismissButton.widthAnchor.constraint(equalToConstant: Layout.dismissSize),
                dismissButton.heightAnchor.constraint(equalToConstant: Layout.dismissSize)
            ])
        }
    }
    
    @objc
    private func didTapDismiss() {
        coordinator.perform(action: .closeModal)
    }
    
    private func startLoadingIndicator() {
        loadingView.isHidden = false
        loadingView.startAnimating()
    }
    
    private func stopLoadingIndicator() {
        loadingView.isHidden = true
    }
}

// MARK: View Model Outputs
extension WebViewController: WebViewModelOutputs {
    func load(request: URLRequest) {
        webView.load(request)
        startLoadingIndicator()
    }
    
    func didNextAction(_ action: WebViewAction) {
        coordinator.perform(action: action)
    }
    
    func addScriptHandler(scriptHandler: ScriptMessageHandler) {
        scriptHandler.register(at: webView.configuration.userContentController) 
    }
    
    func setupObfuscation() {
        setupObfuscationConfiguration()
    }
    
    func deinitObfuscation() {
        deinitObfuscator()
    }
    
    func setupUIDelegate() {
        webView.uiDelegate = self
    }
}

// MARK: WKNavigationDelegate
extension WebViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation?) {
        viewModel.inputs.didFinishLoad(url: webView.url)
        stopLoadingIndicator()
        
        webView.evaluateJavaScript("document.title") { [weak self] value, _ in
            self?.title = value as? String
        }
    }
    
    func webView(
        _ webView: WKWebView,
        decidePolicyFor navigationAction: WKNavigationAction,
        decisionHandler: ((WKNavigationActionPolicy) -> Void)
    ) {
        viewModel.inputs.shouldStartWebLoad(with: navigationAction.request) { shouldLoad in
            decisionHandler(shouldLoad ? .allow : .cancel)
        }
    }
    
    func webView(
        _ webView: WKWebView,
        didReceive challenge: URLAuthenticationChallenge,
        completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void
    ) {
        let pinningHandler = PinningHandler()
        pinningHandler.handle(challenge: challenge, completionHandler: completionHandler)
    }
}

extension WebViewController: WKUIDelegate {
    func webView(
        _ webView: WKWebView,
        runJavaScriptAlertPanelWithMessage message: String,
        initiatedByFrame frame: WKFrameInfo,
        completionHandler: @escaping () -> Void
    ) {
        let alert = Alert(title: message, text: nil)
        let okButton = Button(title: DefaultLocalizable.btOk.text, type: .cta) { alertController, _ in
            alertController.dismiss(animated: true) {
                completionHandler()
            }
        }
        alert.buttons = [okButton]
        AlertMessage.showAlert(alert, controller: self)
    }
}
