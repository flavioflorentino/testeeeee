import Foundation
import WebKit

protocol WebViewModelInputs {
    func viewDidLoad()
    func viewWillAppear()
    func viewDidDisappear()
    func shouldStartWebLoad(with request: URLRequest, decisionHandler: (Bool) -> Void)
    func didFinishLoad(url: URL?)
}

protocol WebViewModelOutputs: AnyObject {
    func load(request: URLRequest)
    func didNextAction(_ action: WebViewAction)
    func addScriptHandler(scriptHandler: ScriptMessageHandler)
    func setupObfuscation()
    func deinitObfuscation()
    func setupUIDelegate()
}

protocol WebViewModelProtocol: AnyObject {
    var inputs: WebViewModelInputs { get }
    var outputs: WebViewModelOutputs? { get set }
}

final class WebViewModel: WebViewModelProtocol, WebViewModelInputs {
    enum WsWebViewAction: String {
        case closeWebview = "api/closeWebview"
        case errorWebview = "api/errorWebview"
    }

    var inputs: WebViewModelInputs {
        self
    }

    weak var outputs: WebViewModelOutputs?
    private let service: WebViewServiceProtocol
    private let url: URL
    private let sendHeaders: Bool
    private let customHeaders: [String: String]
    private let scriptHandlers: [ScriptMessageHandler]
    private var isPasswordUrl: Bool {
        url.absoluteString.contains("picpay.com/password")
    }
    
    // Property used to choose which webViews should have obfuscation
    let shouldObfuscate: Bool
    
    init(
        withURL: URL,
        sendHeaders: Bool,
        service: WebViewServiceProtocol,
        shouldObfuscate: Bool = false,
        customHeaders: [String: String] = [:],
        scriptHandlers: [ScriptMessageHandler] = []
    ) {
        self.url = withURL
        self.sendHeaders = sendHeaders
        self.customHeaders = customHeaders
        self.scriptHandlers = scriptHandlers
        self.shouldObfuscate = shouldObfuscate
        self.service = service
    }
}

// MARK: Inputs
extension WebViewModel {
    func viewDidLoad() {
        scriptHandlers.forEach {
            outputs?.addScriptHandler(scriptHandler: $0)
        }
        
        let request = makeRequest(with: customHeaders)
        setupUIDelegateInWebView()
        outputs?.load(request: request)
    }
    
    func viewWillAppear() {
        if shouldObfuscate {
            outputs?.setupObfuscation()
        }
    }
    
    func viewDidDisappear() {
        if shouldObfuscate {
            outputs?.deinitObfuscation()
        }
    }
    
    func shouldStartWebLoad(with request: URLRequest, decisionHandler: (Bool) -> Void) {
        switch request.url {
        case let value? where value.absoluteString.hasPrefix("picpay://"):
            outputs?.didNextAction(.open(url: value))
            decisionHandler(false)
            return
            
        case let value? where value.absoluteURL.host == "picpay.me":
            let urlString = String(format: "picpay://%@%@", value.absoluteURL.host ?? "", value.absoluteURL.path)
            let newUrl = URL(string: urlString)
            outputs?.didNextAction(.open(url: newUrl))
            decisionHandler(false)
            return

        case let value? where value.absoluteString.hasPrefix(WsWebViewURL.streamlabsToken.endpoint):
            let hasConsumerIdHeader = request.allHTTPHeaderFields?.keys.contains("consumer_id")

            if hasConsumerIdHeader == false, let wsId = service.consumerWsId {
                let headers = ["consumer_id": String(wsId)]
                let newRequest = makeRequest(with: headers)
        
                decisionHandler(false)
                outputs?.load(request: newRequest)
                return
            }

        case let url? where url.queryParams.keys.contains("ppexternal"):
            guard let value = url.queryParams["ppexternal"],
                value.lowercased() == "true" || value == "1" else {
                    decisionHandler(false)
                    return
            }

            outputs?.didNextAction(.open(url: url))
            decisionHandler(false)
            return
            
        case let url? where url.path.contains("picpay-empresas"):
            outputs?.didNextAction(.open(url: url))
            decisionHandler(false)
            return
            
        default:
            break
        }
        
        decisionHandler(true)
    }
    
    func didFinishLoad(url: URL?) {
        switch url?.absoluteString {
        case let value? where value.contains(WsWebViewAction.closeWebview.rawValue):
            outputs?.didNextAction(.completed(nil))
            
        case let value? where value.contains(WsWebViewAction.errorWebview.rawValue):
            let errorTitle = DefaultLocalizable.webviewError.text
            let error = WebServiceInterface.error(withTitle: errorTitle, code: 132_312)
            outputs?.didNextAction(.completed(error))
            
        default:
            break
        }
    }
}

// MARK: Private functions
extension WebViewModel {
    private func makeRequest(with customHeaders: [String: String]) -> URLRequest {
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 10)
        guard sendHeaders else {
            request.allHTTPHeaderFields = customHeaders
            return request
        }

        let allHeaders = makeHeaders(with: customHeaders)
        request.allHTTPHeaderFields = allHeaders
        return request
    }
    
    private func makeHeaders(with customHeaders: [String: String]) -> [String: String] {
        var headers = customHeaders
        
        headers["device_os"] = "ios"
        headers["token"] = service.userToken
        headers["carrier"] = service.carrierName
        headers["app_version"] = service.appVersion
        headers["device_model"] = service.machineName
        headers["installation_id"] = service.installationId
        
        guard let location = service.location else {
            return headers
        }
        headers["latitude"] = location.latitude
        headers["longitude"] = location.longitude
        headers["gps_acc"] = location.accuracy
        headers["location_timestamp"] = location.locationTimestamp
        headers["current_timestamp"] = location.currentTimestamp

        return headers
    }
    
    private func setupUIDelegateInWebView() {
        guard isPasswordUrl else { return }
        outputs?.setupUIDelegate()
    }
}
