import Foundation
import UIKit

final class InvoicePromoFactory {
}

// MARK: - InvoiceScannerFactory
extension InvoicePromoFactory: InvoiceScannerFactoryProtocol {
    func makeInvoiceScanner() -> InvoiceScannerViewController {
        let container = DependencyContainer()
        let service: InvoiceScannerServicing = InvoiceScannerService(dependencies: container)
        let coordinator: InvoiceScannerCoordinating = InvoiceScannerCoordinator()
        let presenter: InvoiceScannerPresenting = InvoiceScannerPresenter(coordinator: coordinator)
        let interactor = InvoiceScannerInteractor(service: service, presenter: presenter)
        let viewController = InvoiceScannerViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
