import Foundation

struct InvoiceScannerModel: Encodable {
    let dataQrcode: String
}
