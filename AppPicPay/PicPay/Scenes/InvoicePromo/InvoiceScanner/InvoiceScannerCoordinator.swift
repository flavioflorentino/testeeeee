import UIKit

enum InvoiceScannerAction {
    case nextStep
    case close
}

protocol InvoiceScannerCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: InvoiceScannerAction)
}

final class InvoiceScannerCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - InvoiceScannerCoordinating
extension InvoiceScannerCoordinator: InvoiceScannerCoordinating {
    func perform(action: InvoiceScannerAction) {
        switch action {
        case .nextStep:
            break // TODO: Proximo PR
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
