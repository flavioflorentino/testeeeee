import UI
import AssetsKit
import UIKit

protocol InvoiceScannerDisplaying: AnyObject {
    func startScanner()
    func displayAlert()
}

private extension InvoiceScannerViewController.Layout {
    enum Size {
        static let imageHeight: CGFloat = 90.0
    }
}

final class InvoiceScannerViewController: ViewController<InvoiceScannerInteracting, UIView> {
    fileprivate enum Layout { }
    private typealias Localizable = Strings.InvoicePromo

    private lazy var scannerTitleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.text, Localizable.scannerTitle)
            .with(\.textColor, Colors.white.lightColor)
            .with(\.textAlignment, .center)

        return label
    }()

    private lazy var scannerFrameView = UIView()

    private lazy var qrcodeFrame: UIImageView = {
        let imageView = UIImageView(image: Assets.Atm24.qrcodeFrame.image)
        imageView.contentMode = .scaleAspectFit

        return imageView
    }()

    private lazy var tipTitleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium))
            .with(\.text, Localizable.tipTitle)
            .with(\.textColor, Colors.branding300.lightColor)
            .with(\.textAlignment, .center)

        return label
    }()

    private lazy var tipOneLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.text, Localizable.tipOne)
            .with(\.textColor, Colors.grayscale050.lightColor)
            .with(\.textAlignment, .center)

        return label
    }()

    private lazy var tipTwoLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.text, Localizable.tipTwo)
            .with(\.textColor, Colors.grayscale050.lightColor)
            .with(\.textAlignment, .center)

        return label
    }()

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }

    private var scanner: PPScanner?

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.tryStartScanner()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        scanner?.stop()
    }

    override func buildViewHierarchy() {
        view.addSubviews(scannerTitleLabel, scannerFrameView, qrcodeFrame, tipTitleLabel, tipOneLabel, tipTwoLabel)
    }

    override func setupConstraints() {
        scannerTitleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base05)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        scannerFrameView.snp.makeConstraints {
            $0.top.equalTo(scannerTitleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base07)
            $0.height.equalTo(scannerFrameView.snp.width)
        }

        qrcodeFrame.snp.makeConstraints {
            $0.top.equalTo(scannerFrameView)
            $0.leading.trailing.equalTo(scannerFrameView)
            $0.height.equalTo(scannerFrameView)
        }

        tipTitleLabel.snp.makeConstraints {
            $0.top.equalTo(qrcodeFrame.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        tipOneLabel.snp.makeConstraints {
            $0.top.equalTo(tipTitleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalTo(tipTitleLabel)
        }

        tipTwoLabel.snp.makeConstraints {
            $0.top.equalTo(tipOneLabel.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalTo(tipOneLabel)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.black.lightColor
        setupScannerNavigationBarStyle()
    }
}

// MARK: - InvoiceScannerDisplaying
extension InvoiceScannerViewController: InvoiceScannerDisplaying {
    func startScanner() {
        initScanner()
    }

    func displayAlert() {
        let primaryAlertButton = ApolloAlertAction(title: Localizable.titlePrimaryButtonAlert, dismissOnCompletion: true) {
            self.initScanner()
        }

        let secondaryAlertButton = ApolloAlertAction(title: Localizable.titleSecondaryButtonAlert, dismissOnCompletion: true) {
            self.interactor.close()
        }

        showApolloAlert(image: Resources.Illustrations.iluConstruction.image,
                        title: Localizable.errorAlertTitle,
                        subtitle: Localizable.errorAlertMessage,
                        primaryButtonAction: primaryAlertButton,
                        linkButtonAction: secondaryAlertButton)
    }
}

private extension InvoiceScannerViewController {
    @objc
    func close(_ barButton: UIBarButtonItem) {
        scanner?.prepareForDismiss()
        interactor.close()
    }

    func setupScannerNavigationBarStyle() {
        let barButton = UIBarButtonItem(title: Localizable.btnCancel, style: .plain, target: self, action: #selector(close(_:)))

        barButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: Colors.white.lightColor], for: .normal)
        navigationController?.navigationBar.topItem?.leftBarButtonItem = barButton
        navigationController?.navigationBar.barTintColor = Colors.black.lightColor
    }

    func initScanner() {
        guard scanner == nil else {
            scanner?.start()
            return
        }

        scanner = PPScanner(origin: .main, view: scannerFrameView, originPosition: CGPoint.zero, onRead: { [weak self] text in
            self?.scanner?.preventNewReads = true
            self?.interactor.onReadQRCode(scannerText: text)
        })
    }
}
