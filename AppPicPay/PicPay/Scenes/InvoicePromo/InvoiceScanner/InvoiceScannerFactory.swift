import UIKit

protocol InvoiceScannerFactoryProtocol {
    func makeInvoiceScanner() -> InvoiceScannerViewController
}
