import Foundation

protocol InvoiceScannerPresenting: AnyObject {
    var viewController: InvoiceScannerDisplaying? { get set }
    func didStartScanner()
    func didNextStep(action: InvoiceScannerAction)
    func presentAlert()
}

final class InvoiceScannerPresenter {
    private let coordinator: InvoiceScannerCoordinating
    weak var viewController: InvoiceScannerDisplaying?

    init(coordinator: InvoiceScannerCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - InvoiceScannerPresenting
extension InvoiceScannerPresenter: InvoiceScannerPresenting {
    func didStartScanner() {
        viewController?.startScanner()
    }

    func didNextStep(action: InvoiceScannerAction) {
        coordinator.perform(action: action)
    }

    func presentAlert() {
        viewController?.displayAlert()
    }
}
