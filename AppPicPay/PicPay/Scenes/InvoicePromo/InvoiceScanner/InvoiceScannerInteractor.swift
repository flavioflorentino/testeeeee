import Foundation

protocol InvoiceScannerInteracting: AnyObject {
    func tryStartScanner()
    func onReadQRCode(scannerText: String)
    func close()
}

final class InvoiceScannerInteractor {
    private let service: InvoiceScannerServicing
    private let presenter: InvoiceScannerPresenting

    init(service: InvoiceScannerServicing, presenter: InvoiceScannerPresenting) {
        self.service = service
        self.presenter = presenter
    }
}

// MARK: - InvoiceScannerInteracting
extension InvoiceScannerInteractor: InvoiceScannerInteracting {
    func tryStartScanner() {
        service.requestCameraPermission { [weak self] status in
            switch status {
            case .authorized:
                self?.presenter.didStartScanner()
            default:
                break // TODO: Verificar com a PD um fallback no caso do usuário não autorizar o uso da câmera.
            }
        }
    }

    func onReadQRCode(scannerText: String) {
        service.sendQRCode(scannerText) { [weak self] result in
            switch result {
            case .success:
                self?.presenter.didNextStep(action: .nextStep)
            case .failure:
                self?.presenter.presentAlert()
            }
        }
    }

    func close() {
        presenter.didNextStep(action: .close)
    }
}
