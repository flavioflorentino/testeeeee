import Foundation
import Core

enum InvoiceScannerEndpoint {
    case qrCode(InvoiceScannerModel)
}

extension InvoiceScannerEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .qrCode:
            return "/receiver-qrcode/v1/scan-qrcode/data"
        }
    }

    var method: HTTPMethod {
        switch self {
        case .qrCode:
            return .post
        }
    }

    var body: Data? {
        switch self {
        case let .qrCode(model):
            return try? JSONEncoder().encode(model)
        }
    }
}
