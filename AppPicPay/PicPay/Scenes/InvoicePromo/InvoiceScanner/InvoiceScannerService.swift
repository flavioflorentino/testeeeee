import Core
import Foundation
import PermissionsKit

protocol InvoiceScannerServicing {
    func requestCameraPermission(completion: @escaping (_ permission: PermissionStatus) -> Void)
    func sendQRCode(_ scannerText: String, completion: @escaping (Result<NoContent, ApiError>) -> Void)
}

final class InvoiceScannerService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    private typealias Localizable = Strings.InvoicePromo

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - InvoiceScannerServicing
extension InvoiceScannerService: InvoiceScannerServicing {
    func requestCameraPermission(completion: @escaping (PermissionStatus) -> Void) {
        let permission = PPPermission.camera(withDeniedAlert: PPPermission.Alert(
            title: Localizable.cameraDeniedAlertTitle,
            message: Localizable.cameraDeniedAlertMessage,
            settings: Localizable.cameraDeniedAlertDestination
        ))

        PPPermission.request(permission: permission, { [weak self] status in
            self?.dependencies.mainQueue.async {
                completion(status)
            }
        })
    }

    func sendQRCode(_ scannerText: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let endpoint = InvoiceScannerEndpoint.qrCode(InvoiceScannerModel(dataQrcode: scannerText))
        Api<NoContent>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
