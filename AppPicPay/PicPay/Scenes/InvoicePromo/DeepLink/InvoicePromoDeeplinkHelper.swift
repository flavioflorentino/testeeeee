import AnalyticsModule
import FeatureFlag
import UI
import UIKit

final class InvoicePromoDeeplinkHelper {
    typealias Dependencies = HasAppManager & HasFeatureManager

    // MARK: - Properties
    private let dependencies: Dependencies
    private var childCoordinator: Coordinating?

    // MARK: - Initialization
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - DeeplinkHelperProtocol
extension InvoicePromoDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        guard let deeplink = deeplink as? InvoiceCashbackDeeplink else {
            return false
        }

        return setUpInvoiceDeepLink(deeplink)
    }
}

// MARK: - Private
private extension InvoicePromoDeeplinkHelper {
    func runInvoiceScanner(viewController: UIViewController) -> Bool {
        let coordinator = InvoicePromoCoordinator(viewController: viewController,
                                                  controllerFactory: InvoicePromoFactory())
        coordinator.start()

        return true
    }

    func setUpInvoiceDeepLink(_ deeplink: InvoiceCashbackDeeplink) -> Bool {
        guard let destination = deeplink.destination,
              dependencies.featureManager.isActive(.isInvoiceCashbackPromotionAvailable),
              let currentController = dependencies.appManager.mainScreenCoordinator?.currentController() else {
            return false
        }

        switch destination {
        case .scanner:
            return runInvoiceScanner(viewController: currentController)
        case .scanList:
            break // TODO: Proximos PRs
        case let .scanDetail(id):
            break // TODO: Proximos PRs
        }

        return false
    }
}
