import Foundation

final class InvoiceCashbackDeeplink: PPDeeplink {
    var destination: Destination?

    init(url: URL) {
        super.init(url: url)
        self.type = .invoiceCashback
        destination = setUpDestination(of: url)
    }
}

extension InvoiceCashbackDeeplink {
    enum Destination {
        case scanner
        case scanList
        case scanDetail(String)
    }

    enum DestinationParam: String {
        case scanner
        case scan
    }

    enum ScanDestinationParam: String {
        case list
        case details
    }
    
    private func setUpDestination(of url: URL) -> Destination? {
        var filteredParams = url.pathComponents.filter { $0 != "n" && $0 != "/" && $0 != "invoicecashback" }
        
        guard let firstParam = filteredParams.first,
            let destinationParam = DestinationParam(rawValue: firstParam) else {
            return nil
        }
        
        switch destinationParam {
        case .scanner:
            return .scanner
        case .scan:
            filteredParams.removeFirst()
            return setUpScanDestination(with: filteredParams)
        }
    }

    private func setUpScanDestination(with params: [String]) -> Destination? {
        guard let firstParam = params.first,
            let destinationParam = ScanDestinationParam(rawValue: firstParam) else {
            return nil
        }
        
        switch destinationParam {
        case .list:
            return .scanList
        case .details:
            var param = params
            param.removeFirst()

            return .scanDetail(param.first ?? "")
        }
    }
}
