import Core
import FeatureFlag
import UI
import UIKit

protocol InvoicePromoCoordinating: Coordinating {
    var didFinishFlow: (() -> Void)? { get set }
}

final class InvoicePromoCoordinator {
    typealias ControllerFactory = InvoiceScannerFactoryProtocol

    // MARK: - Properties
    private let controllerFactory: ControllerFactory

    var childViewController: [UIViewController] = []
    var viewController: UIViewController?
    var didFinishFlow: (() -> Void)?

    // MARK: - Initialization
    init(viewController: UIViewController,
         controllerFactory: ControllerFactory) {
        self.viewController = viewController
        self.controllerFactory = controllerFactory
    }
}

// MARK: - InvoicePromoCoordinator
extension InvoicePromoCoordinator: InvoicePromoCoordinating {
    func start() {
        showInvoiceScannerScreen()
    }
}

// MARK: - Private
private extension InvoicePromoCoordinator {
    func showInvoiceScannerScreen() {
        let controller = controllerFactory.makeInvoiceScanner()
        let navigation = UINavigationController(rootViewController: controller)
        navigation.modalPresentationStyle = .fullScreen

        viewController?.present(navigation, animated: true)
    }
}
