import Core
import CorePayment
import Foundation
import PCI

final class P2MPaymentPciService: ServiceReceiptProtocol {
    private let pciService: P2MServiceProtocol
    private let orchestrator: P2MPaymentOrchestratorModel
    
    init(pci: P2MServiceProtocol, orchestrator: P2MPaymentOrchestratorModel) {
        self.pciService = pci
        self.orchestrator = orchestrator
    }
    
    func createTransaction(payment: Payment, completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        guard let p2mPayload = createP2MPayload(payment: payment) else {
            let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
            completion(PicPayResult.failure(error))
            return
        }
        let cvv = createCVVPayload(cvv: payment.cvv)
        let payload = PaymentPayload<P2MPayload>(cvv: cvv, generic: p2mPayload)
        
        pciService.createTransaction(
            password: payment.password,
            payload: payload,
            isNewArchitecture: true
        ) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    self?.transactionSuccess(response: response, completion: completion)
                    
                case .failure(let error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }
    
    private func transactionSuccess(response: P2MResponse, completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let receiptViewModel = ReceiptWidgetViewModel(transactionId: response.transactionId ?? "", type: .PAV)
        let widgets = response.receiptWidgets.compactMap { WSReceipt.createReceiptWidgetItem(jsonDict: $0) }
        receiptViewModel.setReceiptWidgets(items: widgets)
        NotificationCenter.default.post(name: Notification.Name.Payment.new, object: nil, userInfo: ["type": "P2M"])
                           
        completion(.success(receiptViewModel))
    }
    
    private func createCVVPayload(cvv: String?) -> CVVPayload? {
        guard let cvv = cvv else {
            return nil
        }
        
        return CVVPayload(value: cvv)
    }
    
    private func createP2MPayload(payment: Payment) -> P2MPayload? {
        guard let payload = orchestrator.session else {
            return nil
        }
        
        let session = AnyCodable(value: payload)
        
        return P2MPayload(
            origin: orchestrator.origin.value,
            biometry: payment.biometry,
            cardId: payment.cardId,
            value: String(payment.cardValueWithoutInterest),
            credit: String(payment.balanceValue),
            installments: String(payment.installment),
            someErrorOccurred: payment.someErrorOccurred,
            privacyConfig: payment.privacyConfig.stringValue,
            session: session
        )
    }
    
    private func errorCreateP2MPayload(completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
        completion(.failure(error))
    }
}
