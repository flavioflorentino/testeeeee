import Foundation

struct P2MPaymentOrchestratorModel {
    let origin: Origin
    let isModal: Bool
    let session: [String: Any]?
}

extension P2MPaymentOrchestratorModel {
    enum Origin {
        case mainScanner
        case none
        
        var value: String {
            switch self {
            case .mainScanner:
                return "Scanner Principal"
            case .none:
                return ""
            }
        }
    }
}
