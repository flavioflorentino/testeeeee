import AnalyticsModule
import CorePayment
import Foundation

final class P2MPaymentAnalytics: PaymentActionsDelegate {
    var paymentMethod: PaymentMethod?
    var installment: Int?
    var extraParams: Decodable?
    
    func paymentSuccess(transactionId: String, value: Double) {
        Analytics.shared.log(PaymentEvent.transaction(.p2m, id: transactionId, value: Decimal(value)))
        PPAnalytics.trackEvent("Transacao POS", properties: nil)
    }
}
