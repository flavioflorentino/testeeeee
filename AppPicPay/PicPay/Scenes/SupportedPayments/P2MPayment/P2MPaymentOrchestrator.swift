import FeatureFlag
import Foundation
import PCI

final class P2MPaymentOrchestrator {
    private let paymentInfo: P2MPaymentInfo
    private let orchestrator: P2MPaymentOrchestratorModel
    
    var paymentViewController: UIViewController? {
        let viewController = createContainerPayment()
        if #available(iOS 13.0, *) {
            viewController?.isModalInPresentation = true
        }
        
        return viewController
    }
    
    init(paymentInfo: P2MPaymentInfo, orchestrator: P2MPaymentOrchestratorModel) {
        self.paymentInfo = paymentInfo
        self.orchestrator = orchestrator
    }
    
    private func createContainerPayment() -> UIViewController? {
        let headerPaymentContract = createHeaderPaymentContract()
        let paymentService = P2MPaymentPciService(pci: P2MService(), orchestrator: orchestrator)
        let analytics = P2MPaymentAnalytics()
        let toolbarViewController = PaymentToolbarFactory.make()
        
        return ContainerPaymentReceiptFactory.make(
            paymentService: paymentService,
            actionsDelegate: analytics,
            headerViewController: headerPaymentContract,
            accessoryViewController: nil,
            toolbarViewController: toolbarViewController
        )
    }
    
    private func createHeaderPaymentContract() -> HeaderPaymentContract {
        let modelHeader = createModelPayment()
        let modelPresenter = createModelHeader()
        
        return PaymentQrCodeHeaderFactory.make(modelPresenter: modelPresenter, model: modelHeader)
    }
    
    private func createModelHeader() -> HeaderQrCode {
        HeaderQrCode(
            profileName: paymentInfo.sellerName,
            backgroundImage: nil,
            profileImage: paymentInfo.sellerImage,
            orderNumber: nil,
            isModal: orchestrator.isModal
        )
    }
    
    private func createModelPayment() -> HeaderPayment {
        HeaderPayment(
            sellerId: nil,
            payeeId: paymentInfo.id,
            value: paymentInfo.value.doubleValue,
            installments: paymentInfo.installments,
            payeeType: .none,
            paymentType: .p2m
        )
    }
}
