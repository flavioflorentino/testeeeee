import FeatureFlag
import Foundation

final class InvoiceCardPaymentOrchestrator {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    private let model: InvoiceCardPaymentModel
    private let disclaimer: InvoiceCardDisclaimerInfo
    
    var paymentViewController: UIViewController {
        let viewController = createContainerPayment()
        if #available(iOS 13.0, *) {
            viewController.isModalInPresentation = true
        }
        
        viewController.title = CreditPicPayLocalizable.paymentTitle.text
        
        return viewController
    }
    
    init(
        model: InvoiceCardPaymentModel,
        disclaimer: InvoiceCardDisclaimerInfo,
        dependencies: Dependencies = DependencyContainer()
    ) {
        self.model = model
        self.disclaimer = disclaimer
        self.dependencies = dependencies
    }
    
    private func createContainerPayment() -> UIViewController {
        let paymentService = InvoiceCardPaymentPciService(invoiceId: model.payeeId)
        
        let headerPaymentContract = createPaymentHeader()
        let accessoryPaymentContract = createPaymentAccessory()
        let toolbarViewController = PaymentToolbarFactory.make()
        
        return ContainerPaymentReceiptFactory.make(
            paymentService: paymentService,
            actionsDelegate: nil,
            headerViewController: headerPaymentContract,
            accessoryViewController: accessoryPaymentContract,
            toolbarViewController: toolbarViewController
        )
    }
    
    private func createPaymentAccessory() -> AccessoryPaymentContract {
        let accessory = CardPaymentAccessory(
            total: model.value,
            tax: disclaimer.convenienceTax,
            disclaimer: disclaimer.description
        )
        
        return CardPaymentAccessoryFactory.make(model: accessory)
    }
    
    private func createPaymentHeader() -> HeaderPaymentContract {
        let modelHeader = createModelPayment()
        let modelPresenter = createModelHeader()
        
        return PaymentStandardHeaderFactory.make(modelPresenter: modelPresenter, model: modelHeader)
    }
    
    private func createModelHeader() -> HeaderStandard {
        HeaderStandard(
            title: model.title,
            description: nil,
            image: .image(Assets.OldGeneration.creditreceipt.image),
            isEditable: false,
            link: nil,
            newInstallmentEnable: dependencies.featureManager.isActive(.featureInstallmentNewButton),
            isInstallmentsEnabled: true
        )
    }
    
    private func createModelPayment() -> HeaderPayment {
        HeaderPayment(
            sellerId: nil,
            payeeId: model.payeeId,
            value: model.value,
            installments: 1,
            payeeType: .none,
            paymentType: .invoiceCard
        )
    }
}
