import Core
import CorePayment
import Foundation
import PCI
import SwiftyJSON

final class InvoiceCardPaymentPciService: ServiceReceiptProtocol {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private let invoiceId: String
    private let pciService: InvoiceCardServiceProtocol
    
    init(
        invoiceId: String,
        pci: InvoiceCardServiceProtocol = InvoiceCardService(),
        dependencies: Dependencies = DependencyContainer()
    ) {
        self.invoiceId = invoiceId
        self.pciService = pci
        self.dependencies = dependencies
    }
    
    func createTransaction(payment: Payment, completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let cvv = CVVPayload(value: payment.cvv)
        let payload = createInvoiceCardPayload(payment: payment)
        let paymentPayload = PaymentPayload<InvoiceCardPayload>(cvv: cvv, generic: payload)
        
        pciService.createTransaction(
            password: payment.password,
            invoiceId: invoiceId,
            payload: paymentPayload,
            isNewArchitecture: true
        ) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let response):
                    let receiptViewModel = ReceiptWidgetViewModel(type: .PAV)
                    let widgets = response.receipt.compactMap { WSReceipt.createReceiptWidgetItem(jsonDict: $0) }
                    receiptViewModel.setReceiptWidgets(items: widgets)
                    
                    completion(.success(receiptViewModel))
                    
                case .failure(let error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }
    
    private func createInvoiceCardPayload(payment: Payment) -> InvoiceCardPayload {
        InvoiceCardPayload(
            origin: "credit_invoice_payment",
            biometry: payment.biometry,
            credit: payment.balanceValue.toString() ?? "",
            value: payment.cardValueWithoutInterest.toString() ?? "",
            feedVisibility: payment.privacyConfig.stringValue,
            cardId: Int(payment.cardId) ?? 0,
            installments: payment.installment
        )
    }
}
