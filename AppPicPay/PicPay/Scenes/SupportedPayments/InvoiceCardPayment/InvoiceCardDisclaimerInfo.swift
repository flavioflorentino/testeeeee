import Foundation

struct InvoiceCardDisclaimerInfo: Decodable {
    let description: String?
    let paymentDisabled: Bool
    let convenienceTax: Double
}

private extension InvoiceCardDisclaimerInfo {
    enum CodingKeys: String, CodingKey {
        case description = "text"
        case paymentDisabled = "payment_disabled"
        case convenienceTax = "convenience_tax"
    }
}
