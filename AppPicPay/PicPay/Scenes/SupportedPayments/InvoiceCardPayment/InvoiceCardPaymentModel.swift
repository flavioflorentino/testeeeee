import Foundation

struct InvoiceCardPaymentModel {
    let payeeId: String
    let title: String
    let value: Double
}
