import FeatureFlag
import Foundation

final class DigitalGoodsVoucherService: DigitalGoodsPaymentServicing {
    private let dependencies: Dependencies
    private let product: DGVoucherProduct
    private let item: DGItem
    private let parser: DigitalGoodsPayloadParsing
   
    init(
        dependencies: Dependencies = DependencyContainer(),
        product: DGVoucherProduct,
        item: DGItem,
        parser: DigitalGoodsPayloadParsing = DigitalGoodsPayloadParser()
    ) {
        self.dependencies = dependencies
        self.product = product
        self.item = item
        self.parser = parser
    }
    
    func createPaymentService() -> DigitalGoodsPaymentReceiptService {
        VoucherPciPaymentService(dependencies: dependencies, product: product, item: item, parser: parser)
    }
}
