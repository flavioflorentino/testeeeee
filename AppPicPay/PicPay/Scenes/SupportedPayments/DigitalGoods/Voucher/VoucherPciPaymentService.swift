import Core
import CorePayment
import Foundation
import PCI

final class VoucherPciPaymentService: DigitalGoodsPaymentReceiptService {
    typealias Success = ReceiptWidgetViewModel
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private let parser: DigitalGoodsPayloadParsing
    private let servicePci: VoucherServiceProtocol
    private let product: DGVoucherProduct
    private let item: DGItem
   
    init(dependencies: Dependencies, servicePci: VoucherServiceProtocol = VoucherService(), product: DGVoucherProduct, item: DGItem, parser: DigitalGoodsPayloadParsing) {
        self.dependencies = dependencies
        self.servicePci = servicePci
        self.product = product
        self.item = item
        self.parser = parser
    }
    
    override func createTransaction(payment: Payment, completion: @escaping (Result<Success, PicPayError>) -> Void) {
        let cvv = parser.createCVVPayload(cvv: payment.cvv)
        let voucherPayload = createVoucherRequest(payment: payment)
        let payload = PaymentPayload<DigitalGoodsPayload<Voucher>>(cvv: cvv, generic: voucherPayload)
        
        servicePci.createTransaction(
            password: payment.password,
            payload: payload,
            isNewArchitecture: true
        ) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let result):
                    self?.createTransactionSuccess(data: result.json, completion: completion)
                case .failure(let error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }
    
    private func createVoucherRequest(payment: Payment) -> DigitalGoodsPayload<Voucher> {
        let voucherPayload = Voucher(type: "digitalcodes", productId: product.id)
        return parser.createDigitalGoodsRequest(
            digitalGoodId: item.id,
            sellerId: item.sellerId,
            payment: payment,
            digitalGoods: voucherPayload
        )
    }
    
    private func createTransactionSuccess(data: [String: Any], completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        parser.createTransactionSuccess(data: data, completion: completion)
    }
}
