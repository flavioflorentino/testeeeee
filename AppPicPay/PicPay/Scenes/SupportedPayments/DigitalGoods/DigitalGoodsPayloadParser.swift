import CorePayment
import PCI
import Foundation
import SwiftyJSON

protocol DigitalGoodsPayloadParsing {
    func createDigitalGoodsRequest<T: Codable>(digitalGoodId: String, sellerId: String, payment: Payment, digitalGoods: T) -> DigitalGoodsPayload<T>
    func createCVVPayload(cvv: String?) -> CVVPayload?
    func createTransactionSuccess(data: [String: Any], completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void)
}

struct DigitalGoodsPayloadParser: DigitalGoodsPayloadParsing {    
    func createDigitalGoodsRequest<T: Codable>(digitalGoodId: String, sellerId: String, payment: Payment, digitalGoods: T) -> DigitalGoodsPayload<T> {
        DigitalGoodsPayload<T>(
            digitalGoodId: digitalGoodId,
            digitalGoods: digitalGoods,
            sellerId: sellerId,
            origin: "DigitalGoods",
            password: payment.password,
            biometry: payment.biometry,
            credit: String(payment.balanceValue),
            value: String(payment.cardValueWithoutInterest),
            installments: payment.installment,
            planType: "A",
            addressId: "0",
            cardId: payment.cardId,
            someErrorOccurred: payment.someErrorOccurred ? 1 : 0,
            privacyConfig: payment.privacyConfig.stringValue
        )
    }
    
    func createTransactionSuccess(data: [String: Any], completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        guard
            let jsonData = data.toData(),
            let json = try? JSON(data: jsonData),
            let data = json.dictionary?["data"]?.dictionary,
            let receipt = data["receipt"]?.array,
            let transaction = data["Transaction"]?.dictionary,
            let id = transaction["id"]?.string
            else {
                let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
                completion(.failure(error))
                return
        }
        
        NotificationCenter.default.post(name: Notification.Name.Payment.new, object: nil, userInfo: ["type": "Digital Goods"])
        
        let widgets = WSReceipt.createReceiptWidgetItem(receipt)
        let receiptViewModel = ReceiptWidgetViewModel(transactionId: id, type: .PAV)
        receiptViewModel.setReceiptWidgets(items: widgets)
        
        completion(.success(receiptViewModel))
    }
    
    func createCVVPayload(cvv: String?) -> CVVPayload? {
        guard let cvv = cvv else {
            return nil
        }
    
        return CVVPayload(value: cvv)
    }
}
