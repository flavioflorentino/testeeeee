import Foundation

struct DigitalGoodsPayment {
    let title: String
    let image: String?
    let link: String?
    
    let payeeId: String
    let value: Double
    let paymentType: HeaderPayment.PaymentType
}
