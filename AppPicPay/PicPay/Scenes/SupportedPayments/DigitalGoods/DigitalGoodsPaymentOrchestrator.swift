import FeatureFlag
import Foundation
import UIKit

final class DigitalGoodsPaymentOrchestrator {
    private let items: DigitalGoodsPayment
    private let paymentDetails: [DGPaymentDetailsBase]
    private let service: DigitalGoodsPaymentServicing
    
    var paymentViewController: UIViewController {
        let viewController = createContainerPayment()
        if #available(iOS 13.0, *) {
            viewController.isModalInPresentation = true
        }
        
        viewController.title = DefaultLocalizable.voucherPaymentTitle.text
        return viewController
    }
    
    init(items: DigitalGoodsPayment, paymentDetails: [DGPaymentDetailsBase], service: DigitalGoodsPaymentServicing) {
        self.items = items
        self.paymentDetails = paymentDetails
        self.service = service
    }
    
    private func createContainerPayment() -> UIViewController {
        let headerPaymentContract = createHeaderPaymentContract()
        let accessory = createPaymentAccessory()
        let paymentService = service.createPaymentService()
        let analytics = DigitalGoodsPaymentAnalytics(digitalGoodsPayment: items)
        let toolbarViewController = PaymentToolbarFactory.make()
        
        return ContainerPaymentReceiptFactory.make(
            paymentService: paymentService,
            actionsDelegate: analytics,
            headerViewController: headerPaymentContract,
            accessoryViewController: accessory,
            toolbarViewController: toolbarViewController
        )
    }
    
    private func createPaymentAccessory() -> AccessoryPaymentContract {
        var itens: [ItensPayment] = []
        
        for paymentDetail in paymentDetails {
            switch paymentDetail {
            case let payment as DGPaymentLongDetailItem:
                itens.append(PaymentDetailCell(detail: payment.text))
            case let payment as DGPaymentImportantDetailItem:
                itens.append(PaymentMarkdownCell(markdown: payment.text ?? ""))
            case let payment as DGPaymentDetailsItem:
                itens.append(PaymentTwoLabelsCell(left: payment.name, right: payment.detail, buttonIsEnable: payment.showInfo, popup: nil))
            default:
                break
            }
        }
        
        return DGTableViewPaymentFactory.make(model: itens)
    }
    
    private func createHeaderPaymentContract() -> HeaderPaymentContract {
        let modelHeader = createModelPayment()
        let modelPresenter = createModelHeader()
        
        return PaymentStandardHeaderFactory.make(modelPresenter: modelPresenter, model: modelHeader)
    }
    
    private func createModelHeader() -> HeaderStandard {
        HeaderStandard(
            title: items.title,
            description: String(),
            image: .url(items.image),
            isEditable: false,
            link: items.link,
            newInstallmentEnable: FeatureManager.isActive(.featureInstallmentNewButton),
            isInstallmentsEnabled: true
        )
    }
    
    private func createModelPayment() -> HeaderPayment {
        HeaderPayment(
            sellerId: nil,
            payeeId: items.payeeId,
            value: items.value,
            installments: 1,
            payeeType: .none,
            paymentType: items.paymentType
        )
    }
}
