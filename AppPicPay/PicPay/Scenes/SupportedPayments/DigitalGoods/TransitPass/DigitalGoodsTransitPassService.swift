import Foundation
import FeatureFlag
import PCI

final class DigitalGoodsTransitPassService: DigitalGoodsPaymentServicing {
    private let dependencies: Dependencies
    private let model: DigitalGoodsTransitPass
    private let transitPass: TransitPass
    private let parser: DigitalGoodsPayloadParsing
    
    init(
        dependencies: Dependencies = DependencyContainer(),
        model: DigitalGoodsTransitPass,
        transitPass: TransitPass,
        parser: DigitalGoodsPayloadParsing = DigitalGoodsPayloadParser()
    ) {
        self.dependencies = dependencies
        self.model = model
        self.transitPass = transitPass
        self.parser = parser
    }
    
    func createPaymentService() -> DigitalGoodsPaymentReceiptService {
        TransitPassPciPaymentService(dependencies: dependencies, model: model, transitPass: transitPass, parser: parser)
    }
}
