import Core
import CorePayment
import Foundation
import PCI

final class TransitPassPciPaymentService: DigitalGoodsPaymentReceiptService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private let parser: DigitalGoodsPayloadParsing
    private let servicePci: TransitPassServiceProtocol
    private let model: DigitalGoodsTransitPass
    private let transitPass: TransitPass
    
    init(
        dependencies: Dependencies,
        servicePci: TransitPassServiceProtocol = TransitPassService(),
        model: DigitalGoodsTransitPass,
        transitPass: TransitPass,
        parser: DigitalGoodsPayloadParsing
    ) {
        self.dependencies = dependencies
        self.servicePci = servicePci
        self.model = model
        self.transitPass = transitPass
        self.parser = parser
    }
    
    override func createTransaction(payment: Payment, completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let cvv = parser.createCVVPayload(cvv: payment.cvv)
        let transitPassPayload = createTransitPassRequest(payment: payment)
        let payload = PaymentPayload<DigitalGoodsPayload<TransitPass>>(cvv: cvv, generic: transitPassPayload)
        
        servicePci.createTransaction(
            password: payment.password,
            payload: payload,
            isNewArchitecture: true
        ) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let result):
                    self?.createTransactionSuccess(data: result.json, completion: completion)
                case .failure(let error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }
    
    private func createTransitPassRequest(payment: Payment) -> DigitalGoodsPayload<TransitPass> {
        parser.createDigitalGoodsRequest(
            digitalGoodId: model.digitalGoodId,
            sellerId: model.sellerId,
            payment: payment,
            digitalGoods: transitPass
        )
    }
    
    private func createTransactionSuccess(data: [String: Any], completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        parser.createTransactionSuccess(data: data, completion: completion)
    }
}
