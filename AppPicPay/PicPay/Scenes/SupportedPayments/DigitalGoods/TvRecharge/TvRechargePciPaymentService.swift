import Core
import CorePayment
import Foundation
import PCI

final class TvRechargePciPaymentService: DigitalGoodsPaymentReceiptService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private let parser: DigitalGoodsPayloadParsing
    private let servicePci: TvRechargeServiceProtocol
    private let model: DigitalGoodsTvRecharge
    private let item: DGItem
    
    init(
        dependencies: Dependencies,
        servicePci: TvRechargeServiceProtocol = TvRechargeService(),
        model: DigitalGoodsTvRecharge,
        item: DGItem,
        parser: DigitalGoodsPayloadParsing
    ) {
        self.dependencies = dependencies
        self.servicePci = servicePci
        self.model = model
        self.item = item
        self.parser = parser
    }
    
    override func createTransaction(payment: Payment, completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let cvv = parser.createCVVPayload(cvv: payment.cvv)
        let tvRechargePayload = createTvRechargeRequest(payment: payment)
        let payload = PaymentPayload<DigitalGoodsPayload<TvRecharge>>(cvv: cvv, generic: tvRechargePayload)
        
        servicePci.createTransaction(
            password: payment.password,
            payload: payload,
            isNewArchitecture: true
        ) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let result):
                    self?.parser.createTransactionSuccess(data: result.json, completion: completion)
                case .failure(let error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }
    
    private func createTvRechargeRequest(payment: Payment) -> DigitalGoodsPayload<TvRecharge> {
        let tvRechargePayload = TvRecharge(type: "tvrecharges", subscriberCode: model.subscriberCode, productId: model.productId)
        return parser.createDigitalGoodsRequest(
            digitalGoodId: item.id,
            sellerId: item.sellerId,
            payment: payment,
            digitalGoods: tvRechargePayload
        )
    }
}
