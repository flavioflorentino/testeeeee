import FeatureFlag
import Foundation

final class DigitalGoodsTvRechargeService: DigitalGoodsPaymentServicing {
    private let dependencies: Dependencies
    private let model: DigitalGoodsTvRecharge
    private let item: DGItem
    private let parser: DigitalGoodsPayloadParsing
    
    init(
        dependencies: Dependencies = DependencyContainer(),
        model: DigitalGoodsTvRecharge,
        item: DGItem,
        parser: DigitalGoodsPayloadParsing = DigitalGoodsPayloadParser()
    ) {
        self.dependencies = dependencies
        self.model = model
        self.item = item
        self.parser = parser
    }
    
    func createPaymentService() -> DigitalGoodsPaymentReceiptService {
        TvRechargePciPaymentService(dependencies: dependencies, model: model, item: item, parser: parser)
    }
}
