import Foundation

struct DigitalGoodsTvRecharge {
    let subscriberCode: String
    let productId: String
}
