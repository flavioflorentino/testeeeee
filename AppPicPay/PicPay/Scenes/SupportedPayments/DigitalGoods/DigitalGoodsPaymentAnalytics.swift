import AnalyticsModule
import CorePayment
import Foundation

final class DigitalGoodsPaymentAnalytics: PaymentActionsDelegate {
    var paymentMethod: PaymentMethod?
    var installment: Int?
    var extraParams: Decodable?
    
    private var digitalGoodsPayment: DigitalGoodsPayment
    
    init(digitalGoodsPayment: DigitalGoodsPayment) {
        self.digitalGoodsPayment = digitalGoodsPayment
    }
    
    func paymentSuccess(transactionId: String, value: Double) {
        Analytics.shared.log(PaymentEvent.transaction(.digitalGoods, id: transactionId, value: Decimal(value)))
        Analytics.shared.log(
            DGPaymentAnalytics.paymentRequestSuccess(
                sellerId: digitalGoodsPayment.payeeId,
                sellerName: digitalGoodsPayment.title
            )
        )
    }
    
    func paymentFailure(error: PicPayError) {
        Analytics.shared.log(
            DGPaymentAnalytics.paymentRequestError(
                sellerId: digitalGoodsPayment.payeeId,
                sellerName: digitalGoodsPayment.title,
                errorTitle: error.title,
                errorCode: error.code,
                errorMessage: error.message
            )
        )
    }
}
