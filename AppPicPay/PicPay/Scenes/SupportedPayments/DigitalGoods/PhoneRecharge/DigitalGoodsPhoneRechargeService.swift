import FeatureFlag
import Foundation

final class DigitalGoodsPhoneRechargeService: DigitalGoodsPaymentServicing {
    private let dependencies: Dependencies
    private let model: DigitalGoodsPhoneRecharge
    private let item: DGItem
    private let parser: DigitalGoodsPayloadParsing

    init(
        dependencies: Dependencies = DependencyContainer(),
        model: DigitalGoodsPhoneRecharge,
        item: DGItem,
        parser: DigitalGoodsPayloadParsing = DigitalGoodsPayloadParser()
    ) {
        self.dependencies = dependencies
        self.model = model
        self.item = item
        self.parser = parser
    }

    func createPaymentService() -> DigitalGoodsPaymentReceiptService {
        PhoneRechargePciPaymentService(dependencies: dependencies, model: model, item: item, parser: parser)
    }
}
