import Foundation

struct DigitalGoodsPhoneRecharge {
    let productId: String
    let ddd: String
    let phone: String
    let carrier: String
}
