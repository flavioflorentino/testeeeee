import Core
import CorePayment
import Foundation
import PCI

final class PhoneRechargePciPaymentService: DigitalGoodsPaymentReceiptService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private let parser: DigitalGoodsPayloadParsing
    private let servicePci: PhoneRechargeServiceProtocol
    private let model: DigitalGoodsPhoneRecharge
    private let item: DGItem
    
    init(
        dependencies: Dependencies,
        servicePci: PhoneRechargeServiceProtocol = PhoneRechargeService(),
        model: DigitalGoodsPhoneRecharge,
        item: DGItem,
        parser: DigitalGoodsPayloadParsing
    ) {
        self.dependencies = dependencies
        self.servicePci = servicePci
        self.model = model
        self.item = item
        self.parser = parser
    }
    
    override func createTransaction(payment: Payment, completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let cvv = parser.createCVVPayload(cvv: payment.cvv)
        let phoneRechargePayload = createPhoneRechargeRequest(payment: payment)
        let payload = PaymentPayload<DigitalGoodsPayload<PhoneRecharge>>(cvv: cvv, generic: phoneRechargePayload)
        
        servicePci.createTransaction(
            password: payment.password,
            payload: payload,
            isNewArchitecture: true
        ) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let result):
                    self?.createTransactionSuccess(data: result.json, completion: completion)
                case .failure(let error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }
    
    private func createPhoneRechargeRequest(payment: Payment) -> DigitalGoodsPayload<PhoneRecharge> {
        let phoneRecharge = PhoneRecharge(
            type: "phonerecharges",
            productId: model.productId,
            ddd: model.ddd,
            phone: model.phone,
            carrier: model.carrier
        )
        
        return parser.createDigitalGoodsRequest(
            digitalGoodId: item.id,
            sellerId: item.sellerId,
            payment: payment,
            digitalGoods: phoneRecharge
        )
    }
    
    private func createTransactionSuccess(data: [String: Any], completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        parser.createTransactionSuccess(data: data, completion: completion)
    }
}
