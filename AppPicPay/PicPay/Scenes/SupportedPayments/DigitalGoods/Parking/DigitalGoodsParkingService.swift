import Foundation

final class DigitalGoodsParkingService: DigitalGoodsPaymentServicing {
    private let dependencies: Dependencies
    private let model: DigitalGoodsParking
    private let item: DGItem
    private let parser: DigitalGoodsPayloadParsing
    
    init(
        dependencies: Dependencies = DependencyContainer(),
        model: DigitalGoodsParking,
        item: DGItem,
        parser: DigitalGoodsPayloadParsing = DigitalGoodsPayloadParser()
    ) {
        self.dependencies = dependencies
        self.model = model
        self.item = item
        self.parser = parser
    }
    
    func createPaymentService() -> DigitalGoodsPaymentReceiptService {
        ParkingPciPaymentService(dependencies: dependencies, model: model, item: item, parser: parser)
    }
}
