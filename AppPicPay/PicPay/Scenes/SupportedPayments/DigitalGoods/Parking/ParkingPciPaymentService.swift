import Core
import CorePayment
import Foundation
import PCI

final class ParkingPciPaymentService: DigitalGoodsPaymentReceiptService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private let parser: DigitalGoodsPayloadParsing
    private let servicePci: ParkingServiceProtocol
    private let model: DigitalGoodsParking
    private let item: DGItem
    
    init(
        dependencies: Dependencies,
        servicePci: ParkingServiceProtocol = ParkingService(),
        model: DigitalGoodsParking,
        item: DGItem,
        parser: DigitalGoodsPayloadParsing
    ) {
        self.dependencies = dependencies
        self.servicePci = servicePci
        self.model = model
        self.item = item
        self.parser = parser
    }
    
    override func createTransaction(payment: Payment, completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let cvv = parser.createCVVPayload(cvv: payment.cvv)
        let parkingPayload = createParkingRequest(payment: payment)
        let payload = PaymentPayload<DigitalGoodsPayload<Parking>>(cvv: cvv, generic: parkingPayload)
        
        servicePci.createTransaction(
            password: payment.password,
            payload: payload,
            isNewArchitecture: true
        ) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let result):
                    self?.parser.createTransactionSuccess(data: result.json, completion: completion)
                case .failure(let error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }
    
    private func createParkingRequest(payment: Payment) -> DigitalGoodsPayload<Parking> {
        let parkingPayload = Parking(type: "parkings", ticket: model.ticket, price: model.price)
        return parser.createDigitalGoodsRequest(
            digitalGoodId: item.id,
            sellerId: item.sellerId,
            payment: payment,
            digitalGoods: parkingPayload
        )
    }
}
