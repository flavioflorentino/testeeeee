import Core
import CorePayment
import FeatureFlag
import Foundation

protocol DigitalGoodsPaymentServicing {
    typealias Dependencies = HasMainQueue & HasFeatureManager
    func createPaymentService() -> DigitalGoodsPaymentReceiptService
}

class DigitalGoodsPaymentReceiptService: LegacyPaymentServicing {
    typealias Success = ReceiptWidgetViewModel
    func createTransaction(payment: Payment, completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) { }
}
