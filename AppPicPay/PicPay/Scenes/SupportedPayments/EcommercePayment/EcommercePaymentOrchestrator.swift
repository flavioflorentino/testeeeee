import CorePayment
import FeatureFlag
import Foundation
import PCI

final class EcommercePaymentOrchestrator {
    private let orchestrator: EcommercePaymentOrchestratorModel
    private let paymentInfo: EcommercePaymentInfo
    
    var paymentViewController: UIViewController {
        let viewController = createContainerPayment()
        if #available(iOS 13.0, *) {
            viewController.isModalInPresentation = true
        }
        
        return viewController
    }
    
    init(paymentInfo: EcommercePaymentInfo, orchestrator: EcommercePaymentOrchestratorModel) {
        self.paymentInfo = paymentInfo
        self.orchestrator = orchestrator
    }
    
    private func createContainerPayment() -> UIViewController {
        let headerPaymentContract = createHeaderPaymentContract()
        let paymentService = EcommercePciPaymentService(paymentInfo: paymentInfo, pci: EcommercePciService(), orchestrator: orchestrator)
        let analytics = EcommercePaymentAnalytics()
        let toolbarViewController = PaymentToolbarFactory.make()
        
        return ContainerPaymentReceiptFactory.make(
            paymentService: paymentService,
            actionsDelegate: analytics,
            headerViewController: headerPaymentContract,
            accessoryViewController: nil,
            toolbarViewController: toolbarViewController
        )
    }
    
    private func createHeaderPaymentContract() -> HeaderPaymentContract {
        let modelHeader = createModelPayment()
        let modelPresenter = createModelHeader()
        
        return PaymentQrCodeHeaderFactory.make(modelPresenter: modelPresenter, model: modelHeader)
    }
    
    private func createModelHeader() -> HeaderQrCode {
        HeaderQrCode(
            profileName: paymentInfo.sellerName,
            backgroundImage: paymentInfo.sellerImage,
            profileImage: paymentInfo.sellerImage,
            orderNumber: paymentInfo.orderReference,
            isModal: orchestrator.isModal
        )
    }
    
    private func createModelPayment() -> HeaderPayment {
        HeaderPayment(
            sellerId: nil,
            payeeId: paymentInfo.id,
            value: paymentInfo.value.doubleValue,
            installments: 1,
            payeeType: .none,
            paymentType: .ecommerce
        )
    }
}
