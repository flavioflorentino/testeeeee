import Foundation

struct EcommercePaymentOrchestratorModel {
    let origin: Origin
    let isModal: Bool
}

extension EcommercePaymentOrchestratorModel {
    enum Origin: String, RawRepresentable {        
        case scanner = "Scanner"
        case notification = "Notification"
        case deeplink = "Deeplink"
        
        var value: String {
            rawValue
        }
    }
}
