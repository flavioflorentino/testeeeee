import AnalyticsModule
import CorePayment
import Foundation

final class EcommercePaymentAnalytics: PaymentActionsDelegate {
    var paymentMethod: PaymentMethod?
    var installment: Int?
    var extraParams: Decodable?
    
    func paymentSuccess(transactionId: String, value: Double) {
        Analytics.shared.log(PaymentEvent.transaction(.ecommerce, id: transactionId, value: Decimal(value)))
    }
}
