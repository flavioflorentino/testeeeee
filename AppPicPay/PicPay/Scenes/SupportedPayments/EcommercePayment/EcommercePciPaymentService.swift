import Core
import CorePayment
import Foundation
import PCI

final class EcommercePciPaymentService: ServiceReceiptProtocol {
    private let paymentInfo: EcommercePaymentInfo
    private let pciService: EcommercePciServicing
    private let orchestrator: EcommercePaymentOrchestratorModel
    
    init(paymentInfo: EcommercePaymentInfo, pci: EcommercePciServicing, orchestrator: EcommercePaymentOrchestratorModel) {
        self.paymentInfo = paymentInfo
        self.pciService = pci
        self.orchestrator = orchestrator
    }
    
    func createTransaction(payment: Payment, completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let ecommercePayload = createEcommercePayload(payment: payment)
        let cvv = createCVVPayload(cvv: payment.cvv)
        let payload = PaymentPayload<EcommercePayload>(cvv: cvv, generic: ecommercePayload)
        
        pciService.createTransaction(
            password: payment.password,
            orderId: paymentInfo.orderNumber,
            payload: payload,
            isNewArchitecture: true
        ) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let value):
                    self?.transactionSuccess(response: value, completion: completion)
                    
                case .failure(let error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }
    
    private func transactionSuccess(response: EcommerceResponse, completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let viewModel = ReceiptWidgetViewModel(transactionId: response.transactionId, type: .PAV)
        let receiptWidgets = response.receiptWidgets.compactMap { WSReceipt.createReceiptWidgetItem(jsonDict: $0) }
        viewModel.setReceiptWidgets(items: receiptWidgets)
        NotificationCenter.default.post(name: Notification.Name.Payment.new, object: nil, userInfo: ["type": "Ecommerce"])
        
        completion(.success(viewModel))
    }
    
    private func createCVVPayload(cvv: String?) -> CVVPayload? {
        guard let cvv = cvv else {
            return nil
        }
        
        return CVVPayload(value: cvv)
    }
    
    private func createEcommercePayload(payment: Payment) -> EcommercePayload {
        EcommercePayload(
            orderId: paymentInfo.orderNumber,
            biometry: payment.biometry,
            sellerId: paymentInfo.sellerId,
            cardId: payment.cardId,
            value: String(payment.cardValueWithoutInterest),
            credit: String(payment.balanceValue),
            installments: String(payment.installment),
            someErrorOccurred: payment.someErrorOccurred,
            privacyConfig: payment.privacyConfig.stringValue,
            origin: orchestrator.origin.value
        )
    }
}
