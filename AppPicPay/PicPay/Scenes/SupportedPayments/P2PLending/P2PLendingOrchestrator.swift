import CorePayment
import P2PLending
import UI

final class P2PLendingOrchestrator {
    let borrower: LendingFriend
    let value: Double
    let offerIdentifier: UUID
    
    init(borrower: LendingFriend, value: Double, offerIdentifier: UUID) {
        self.borrower = borrower
        self.value = value
        self.offerIdentifier = offerIdentifier
    }
    
    func createPaymentContainer() -> ContainerPaymentViewController {
        let paymentController = ContainerPaymentFactory.make(
            coordinatorSuccess: P2PLendingPaymentCoordinator(),
            paymentService: P2PLendingPaymentService(offerIddentifier: offerIdentifier, dependencies: DependencyContainer()),
            actionsDelegate: nil,
            headerViewController: createHeaderController(),
            accessoryViewController: CommentPaymentFactory.make(),
            toolbarViewController: toolbarController(),
            paymentManager: createPaymentManager()
        )
        paymentController.shouldDisplayCloseButton = false
        if #available(iOS 11.0, *) {
            paymentController.navigationItem.largeTitleDisplayMode = .never
        }
        paymentController.view.tintColor = Colors.branding300.color
        paymentController.title = PaymentLocalizable.p2pLendingPaymentSceneTitle.text
        return paymentController
    }
    
    func createPaymentManager() -> PPPaymentManager {
        // swiftlint:disable:next redundant_type_annotation
        let paymentManager: PPPaymentManager = PPPaymentManager()
        paymentManager.forceBalance = true
        return paymentManager
    }
    
    func createHeaderController() -> HeaderPaymentContract {
        PaymentStandardHeaderFactory.make(modelPresenter: standartHeader(), model: paymentHeader())
    }
    
    func standartHeader() -> HeaderStandard {
        HeaderStandard(
            title: borrower.userName,
            description: borrower.name,
            image: .url(borrower.profileUrl?.absoluteString),
            isEditable: false,
            link: nil,
            newInstallmentEnable: false,
            isInstallmentsEnabled: false
        )
    }
    
    func paymentHeader() -> HeaderPayment {
        HeaderPayment(
            sellerId: nil,
            payeeId: "",
            value: value,
            installments: 1,
            payeeType: .none,
            paymentType: .p2pLending
        )
    }
    
    func toolbarController() -> ToolbarPaymentContract {
        P2PLendingPaymentToolbarFactory.make()
    }
}
