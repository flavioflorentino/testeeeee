import AnalyticsModule
import CorePayment
import Foundation
import P2PLending

final class P2PLendingPaymentCoordinator: PaymentSuccessCoordinating {
    typealias Success = ReceiptWidgetViewModel
    
    func showPaymentSuccess(model: ReceiptWidgetViewModel, paymentType: PaymentType, viewController: UIViewController?) {
        guard let viewController = viewController else {
            return
        }
        
        let bounds = viewController.view.bounds
        TaskCompleteAnimation.showSuccess(onView: viewController.view, wrapperBounds: bounds) {
            let successViewController = LendingSuccessFactory.make(content: PaymentSentSuccessContent(), delegate: self)
            viewController.show(successViewController, sender: nil)
        }
    }
}

extension P2PLendingPaymentCoordinator: LendingSuccessDelegate {
    func lendingSuccessControllerDidDismiss(_ lendingSuccessViewController: UIViewController) {
        Analytics.shared.log(LendingSuccessEvent.didFinishFlow(.investor))
    }
}
