import Core
import CorePayment
import Foundation

final class P2PLendingPaymentService: LegacyPaymentServicing {
    let isFlowPci: Bool = false
    let offerIdentifier: UUID
    
    typealias Depentencies = HasMainQueue
    let dependencies: Depentencies
    
    init(offerIddentifier: UUID, dependencies: Depentencies) {
        self.offerIdentifier = offerIddentifier
        self.dependencies = dependencies
    }
    
    func createTransaction(payment: Payment, completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let endpoint = P2PLendingTransactionEndpoint(offerIdentifier: offerIdentifier,
                                                     password: payment.password,
                                                     message: payment.message)
        Api<NoContent>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success:
                    let receiptWidget = ReceiptWidgetViewModel(type: .p2pLending)
                    receiptWidget.needsDismissPresentingControllers = false
                    completion(.success(receiptWidget))
                case .failure(let error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }
}
