import Core
import Foundation

struct P2PLendingTransactionEndpoint: ApiEndpointExposable {
    let offerIdentifier: UUID
    let password: String
    let message: String
    
    let path: String = "p2plending/loan/transaction"
    let method: HTTPMethod = .post
    var customHeaders: [String: String] {
        ["password": password]
    }
    
    var body: Data? {
        ["message": message, "offer_uuid": offerIdentifier.uuidString].toData()
    }
}
