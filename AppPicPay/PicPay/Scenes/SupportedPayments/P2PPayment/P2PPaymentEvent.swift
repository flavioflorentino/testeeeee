import AnalyticsModule
import Foundation

enum P2PPaymentEvent: AnalyticsKeyProtocol {
    case willAppear
    case willAppearFirebase
    case willAppearFirst
    case didTapPayment(
        wentCards: Bool,
        wentInstallments: Bool,
        usedSearch: Bool,
        paidUsername: Bool,
        username: String,
        origin: String)
    case paymentSuccess
    case transactionSuccess(transactionId: String)
    
    private var name: String {
        switch self {
        case .willAppear:
            return "Visualizacao de tela"
        case .willAppearFirebase:
            return "visualizacao_de_tela"
        case .willAppearFirst:
            return "FT tela transação P2P"
        case .didTapPayment:
            return "Finalização P2P"
        case .paymentSuccess:
            return "Visualizacao de tela"
        case .transactionSuccess:
            return "transaction"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .willAppear:
            return ["nome": "transacao p2p"]
        case .willAppearFirebase:
            return ["nome": "transacao_p2p"]
        case let .didTapPayment(wentCards, wentInstallments, usedSearch, paidUsername, username, origin):
            return [
                "Abriu cartões": wentCards ? Assertion.yes.text : Assertion.no.text,
                "Abriu parcelamento": wentInstallments ? Assertion.yes.text : Assertion.no.text,
                "Usou pesquisa": usedSearch ? Assertion.yes.text : Assertion.no.text,
                "Pagou username": paidUsername ? Assertion.yes.text : Assertion.no.text,
                "Username": username,
                "origin": origin
            ]
        case .paymentSuccess:
            return ["nome": "concluiu transacao p2p"]
            
        case let .transactionSuccess(transactionId):
            return ["type": "p2p", "transactionId": transactionId]
        default:
            return [:]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        switch self {
        case .willAppearFirebase:
            return [.firebase]
        default:
            return [.mixPanel, .firebase, .appsFlyer]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

extension P2PPaymentEvent {
    private enum Assertion: String {
        case yes = "Sim"
        case no = "Não"
        
        var text: String { self.rawValue }
    }
}
