import CorePayment
import FeatureFlag
import Foundation
import PCI

final class P2PPaymentPciService: ServiceReceiptProtocol {
    private let pciService: P2PServiceProtocol
    private let contact: PPContact
    private let orchestrator: P2PPaymentOrchestratorModel
    
    init(pci: P2PServiceProtocol, contact: PPContact, orchestrator: P2PPaymentOrchestratorModel) {
        self.pciService = pci
        self.contact = contact
        self.orchestrator = orchestrator
    }
        
    func createTransaction(payment: Payment, completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let p2PPayload = createP2PPayload(payment: payment)
        let isFeedzai = FeatureManager.shared.isActive(.transaction_p2p_v2_feedzai)
        let cvv = createCVVPayload(cvv: payment.cvv)
        let payload = PaymentPayload<P2PPayload>(cvv: cvv, generic: p2PPayload)
        
        pciService.createTransaction(
            password: payment.password,
            isFeedzai: isFeedzai,
            payload: payload,
            isNewArchitecture: true
        ) { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let value):
                    let receiptViewModel = ReceiptWidgetViewModel(transactionId: value.p2pId, type: .P2P)
                    let widgets = value.receiptWidgets.compactMap { WSReceipt.createReceiptWidgetItem(jsonDict: $0) }
                    receiptViewModel.setReceiptWidgets(items: widgets)
                    NotificationCenter.default.post(name: Notification.Name.Payment.new, object: nil, userInfo: ["type": "P2P"])
                    
                    completion(.success(receiptViewModel))
                    
                case .failure(let error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }
    
    private func createCVVPayload(cvv: String?) -> CVVPayload? {
        guard let cvv = cvv else {
            return nil
        }
        
        return CVVPayload(value: cvv)
    }
    
    private func createP2PPayload(payment: Payment) -> P2PPayload {
        P2PPayload(
            password: payment.password,
            biometry: payment.biometry,
            payeeId: String(contact.wsId),
            value: payment.value,
            credit: payment.balanceValue,
            installments: payment.installment,
            cardId: payment.cardId,
            someErrorOccurred: payment.someErrorOccurred,
            privacyConfig: payment.privacyConfig.stringValue,
            message: payment.message,
            extra: orchestrator.extra,
            surcharge: payment.surcharge
        )
    }
}
