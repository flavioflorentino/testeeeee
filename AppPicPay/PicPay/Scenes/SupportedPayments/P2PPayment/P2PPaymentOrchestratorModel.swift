import Foundation

struct P2PPaymentOrchestratorModel {
    let origin: Origin
    let usedSearch: Bool
    let paidUsername: Bool
    let extra: String?
}

extension P2PPaymentOrchestratorModel {
    enum Origin {
        case homeSugestions
        case none
        
        var value: String {
            switch self {
            case .homeSugestions:
                return "Seção Início"
            case .none:
                return ""
            }
        }
    }
}
