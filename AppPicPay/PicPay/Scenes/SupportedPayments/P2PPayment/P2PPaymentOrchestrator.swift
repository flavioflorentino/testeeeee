import FeatureFlag
import Foundation
import PCI

final class P2PPaymentOrchestrator {
    private let contact: PPContact
    private let orchestrator: P2PPaymentOrchestratorModel
    
    var paymentViewController: UIViewController? {
        let viewController = createContainerPayment()
        if #available(iOS 13.0, *) {
            viewController?.isModalInPresentation = true
        }

        viewController?.title = DefaultLocalizable.paymentTitle.text
        
        return viewController
    }
    
    init(contact: PPContact, orchestrator: P2PPaymentOrchestratorModel) {
        self.contact = contact
        self.orchestrator = orchestrator
    }
    
    private func createContainerPayment() -> UIViewController? {
        guard let headerPaymentContract = createHeaderPaymentContract() else {
            return nil
        }
        
        let paymentService = P2PPaymentPciService(pci: P2PService(), contact: contact, orchestrator: orchestrator)
        let analytics = P2PPaymentAnalytics(orchestrator: orchestrator, username: contact.username ?? "")
        let accessoryPaymentContract = CommentPaymentFactory.make()
        let toolbarViewController = PaymentToolbarFactory.make()
        
        return ContainerPaymentReceiptFactory.make(
            paymentService: paymentService,
            actionsDelegate: analytics,
            headerViewController: headerPaymentContract,
            accessoryViewController: accessoryPaymentContract,
            toolbarViewController: toolbarViewController
        )
    }
    
    private func createHeaderPaymentContract() -> HeaderPaymentContract? {
        guard
            let modelHeader = createModelPayment(contact: contact),
            let modelPresenter = createModelHeader(contact: contact)
            else {
                return nil
            }
        
        return PaymentStandardHeaderFactory.make(modelPresenter: modelPresenter, model: modelHeader)
    }
    
    private func createModelHeader(contact: PPContact) -> HeaderStandard? {
        guard
            let name = contact.onlineName,
            let username = contact.username
            else {
                return nil
            }
        
        return HeaderStandard(
            title: username,
            description: name,
            image: .url(contact.imgUrl),
            isEditable: true,
            link: nil,
            newInstallmentEnable: FeatureManager.isActive(.featureInstallmentNewButton),
            isInstallmentsEnabled: true
        )
    }
    
    private func createModelPayment(contact: PPContact) -> HeaderPayment? {
        let type = typeUser(contact: contact)
        return HeaderPayment(
            sellerId: nil,
            payeeId: String(contact.wsId),
            value: 0.0,
            installments: 1,
            payeeType: type,
            paymentType: .p2p
        )
    }
    
    private func typeUser(contact: PPContact) -> HeaderPayment.PayeeType {
        if contact.businessAccount {
            return .pro
        } else if contact.isVerified {
            return .verify
        }
        return .none
    }
}
