import AnalyticsModule
import CorePayment
import Foundation

final class P2PPaymentAnalytics: PaymentActionsDelegate {
    var paymentMethod: PaymentMethod?
    var installment: Int?
    var extraParams: Decodable?
    
    private let orchestrator: P2PPaymentOrchestratorModel
    private let username: String
    
    init(orchestrator: P2PPaymentOrchestratorModel, username: String) {
        self.orchestrator = orchestrator
        self.username = username
    }
    
    func viewWillAppear() {
        Analytics.shared.log(P2PPaymentEvent.willAppear)
        Analytics.shared.log(P2PPaymentEvent.willAppearFirebase)
        PPAnalytics.trackFirstTimeOnlyEvent("FT tela transação P2P", properties: nil)
    }
    
    func didTapPayment(wentToCards: Bool, wentToInstallments: Bool) {
        Analytics.shared.log(P2PPaymentEvent.didTapPayment(
            wentCards: wentToCards,
            wentInstallments: wentToInstallments,
            usedSearch: orchestrator.usedSearch,
            paidUsername: orchestrator.paidUsername,
            username: username,
            origin: orchestrator.origin.value
        ))
    }
    
    func paymentSuccess(transactionId: String, value: Double) {
        Analytics.shared.log(P2PPaymentEvent.paymentSuccess)
        Analytics.shared.log(P2PPaymentEvent.transactionSuccess(transactionId: transactionId))
    }
}
