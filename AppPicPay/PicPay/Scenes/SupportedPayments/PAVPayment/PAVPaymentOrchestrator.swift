import Core
import FeatureFlag
import Foundation
import PCI

final class PAVPaymentOrchestrator {
    typealias Dependencies = HasMainQueue & HasLocationManager & HasFeatureManager
    
    private let orchestrator: PAVPaymentOrchestratorModel
    private let dependency: Dependencies
    private let actions: PAVPaymentActions?
    
    var paymentViewController: UIViewController? {
        let viewController = createContainerPayment()
        if #available(iOS 13.0, *) {
            viewController?.isModalInPresentation = true
        }
        
        viewController?.title = DefaultLocalizable.paymentTitle.text
        
        return viewController
    }
    
    init(orchestrator: PAVPaymentOrchestratorModel, actions: PAVPaymentActions?, dependency: Dependencies) {
        self.orchestrator = orchestrator
        self.dependency = dependency
        self.actions = actions
    }
    
    private func createContainerPayment() -> UIViewController? {
        guard let headerPaymentContract = createHeaderPaymentContract() else {
            return nil
        }
        
        let paymentService = PAVPaymentPciService(orchestrator: orchestrator, dependency: dependency)
        let accessoryPaymentContract = PaymentItemsFactory.make(paymentItems: orchestrator.items)
        let toolbarViewController = PaymentToolbarFactory.make()
        
        return ContainerPaymentReceiptFactory.make(
            paymentService: paymentService,
            actionsDelegate: actions,
            headerViewController: headerPaymentContract,
            accessoryViewController: accessoryPaymentContract,
            toolbarViewController: toolbarViewController
        )
    }
    
    private func createHeaderPaymentContract() -> HeaderPaymentContract? {
        guard
            let modelHeader = createModelPayment(item: orchestrator.paymentItem),
            let modelPresenter = createModelHeader(item: orchestrator.paymentItem, isFillableValue: orchestrator.isFillableValue)
            else {
                return nil
        }
        
        return PaymentStandardHeaderFactory.make(modelPresenter: modelPresenter, model: modelHeader)
    }
    
    private func createModelHeader(item: MBItem, isFillableValue: Bool) -> HeaderStandard? {
        HeaderStandard(
            title: item.seller.name,
            description: nil,
            image: .url(item.seller.logoUrl),
            isEditable: isFillableValue,
            link: nil,
            newInstallmentEnable: dependency.featureManager.isActive(.featureInstallmentNewButton),
            isInstallmentsEnabled: true
        )
    }
    
    private func createModelPayment(item: MBItem) -> HeaderPayment? {
        HeaderPayment(
            sellerId: item.sellerId,
            payeeId: String(item.wsId),
            value: orchestrator.totalValue,
            installments: 1,
            payeeType: .none,
            paymentType: .pav
        )
    }
}
