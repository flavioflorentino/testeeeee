import Core
import CorePayment
import FeatureFlag
import Foundation
import PCI
import SwiftyJSON

final class PAVPaymentPciService: ServiceReceiptProtocol {
    typealias Dependencies = HasMainQueue & HasLocationManager & HasFeatureManager
    private let dependency: Dependencies
    
    private let pciService: PAVServiceProtocol
    private let orchestrator: PAVPaymentOrchestratorModel
    
    private var location: LocationDescriptor? {
        LocationDescriptor(dependency.locationManager.lastAvailableLocation)
    }
    
    init(orchestrator: PAVPaymentOrchestratorModel, dependency: Dependencies, pciService: PAVServiceProtocol = PAVService()) {
        self.pciService = pciService
        self.orchestrator = orchestrator
        self.dependency = dependency
    }
    
    func createTransaction(payment: Payment, completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let pavPayload = createPavPayload(payment: payment)
        let isFeedzai = dependency.featureManager.isActive(.transaction_pav_v2_feedzai)
        let cvv = createCVVPayload(cvv: payment.cvv)
        let payload = PaymentPayload<PAVPayload>(cvv: cvv, generic: pavPayload)
        
        pciService.createTransaction(
            password: payment.password,
            isFeedzai: isFeedzai,
            payload: payload,
            isNewArchitecture: true
        ) { [weak self] result in
            guard let self = self else {
                return
            }
            
            self.dependency.mainQueue.async {
                switch result {
                case .success(let value):
                    guard let receiptViewModel = self.createReceiptWidgetViewModel(value: value.json) else {
                        let picpayError = PicPayError(message: DefaultLocalizable.unexpectedError.text)
                        completion(.failure(picpayError))
                        return
                    }
                    
                    receiptViewModel.needsDismissPresentingControllers = self.orchestrator.dismissPresentingAfterSuccess
                    
                    NotificationCenter.default.post(name: Notification.Name.Payment.new, object: nil, userInfo: ["type": "PAV"])
                    completion(.success(receiptViewModel))
                    
                case .failure(let error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }
    
    private func createReceiptWidgetViewModel(value: [String: Any]) -> ReceiptWidgetViewModel? {
      guard
        let jsonData = value.toData(),
        let json = try? JSON(data: jsonData),
        let receipt = json["receipt"].array,
        let transaction = json["Transaction"].dictionary,
        let id = transaction["id"]?.string
        else {
          return nil
      }
        
      let widgets = WSReceipt.createReceiptWidgetItem(receipt)
      let receiptViewModel = ReceiptWidgetViewModel(transactionId: id, type: .PAV)
      receiptViewModel.setReceiptWidgets(items: widgets)
      return receiptViewModel
    }
    
    private func createPavPayload(payment: Payment) -> PAVPayload {
        PAVPayload(
            password: payment.password,
            biometry: String(payment.biometry),
            sellerId: String(orchestrator.paymentItem.seller.wsId),
            planType: orchestrator.paymentItem.seller.currentPlanType.planTypeIdToString(),
            ignoreBalance: true,
            value: String(payment.cardValueWithoutInterest),
            credit: String(payment.balanceValue),
            cardId: payment.cardId,
            installments: String(payment.installment),
            items: [PAVItems(pavId: String(orchestrator.paymentItem.selectedCombination), amount: "1")],
            someErrorOccurred: String(payment.someErrorOccurred),
            latitude: location?.latitude ?? "0.0",
            longitude: location?.longitude ?? "0.0",
            gpsAcc: location?.accuracy ?? "0.0",
            privacyConfig: payment.privacyConfig.stringValue,
            additionalInfo: AnyCodable(value: ["charge_link": orchestrator.additionalInfo])
        )
    }
    
    private func createCVVPayload(cvv: String?) -> CVVPayload? {
        guard let cvv = cvv else {
            return nil
        }
        
        return CVVPayload(value: cvv)
    }
}
