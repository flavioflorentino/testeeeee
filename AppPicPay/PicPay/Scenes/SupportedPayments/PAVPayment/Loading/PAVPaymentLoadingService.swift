import Core
import Foundation

typealias CompletionPaymentLoading = (Result<MBItem, PicPayError>) -> Void
typealias CompletionPAVInfoLoading = (Result<PAVPaymentHashInfo, PicPayError>) -> Void

protocol PAVPaymentLoadingServicing {
    func loadPAVInfo(hash: String, completion: @escaping CompletionPAVInfoLoading)
    func loadItem(storeId: String, sellerId: String, completion: @escaping CompletionPaymentLoading)
}

final class PAVPaymentLoadingService: PAVPaymentLoadingServicing {
    typealias Dependencies = HasMainQueue
    private let dependency: Dependencies
    
    init(dependency: Dependencies) {
        self.dependency = dependency
    }
    
    func loadItem(storeId: String, sellerId: String, completion: @escaping CompletionPaymentLoading) {
        WSItemRequest.item(byStoreId: storeId, andSellerId: sellerId) { item, error in
            self.dependency.mainQueue.async {
                completion(self.handle(item: item, error: error))
            }
        }
    }
    
    func loadPAVInfo(hash: String, completion: @escaping CompletionPAVInfoLoading) {
        let api = Api<PAVPaymentHashInfo>(endpoint: PAVPaymentLoadingEndpoint.PAVInfo(hash: hash))
        
        api.execute { [weak self] result in
            let mappedResult = result
                .mapError(\.picpayError)
                .map(\.model)
            self?.dependency.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
}

private extension PAVPaymentLoadingService {
    func handle(item: MBItem?, error: Error?) -> Result<MBItem, PicPayError> {
        if let item = item,
            item.seller.currentPlanType.planTypeIdToString() == "A" {
            return Result.success(item)
        } else {
            let picpayError = error as? PicPayError ?? PicPayError(message: DefaultLocalizable.unexpectedError.text)
            return .failure(picpayError)
        }
    }
}
