import UIKit

enum PAVPaymentLoadingAction {
    case payment(_ params: OpenPaymentParams)
    case close
}

protocol PAVPaymentLoadingCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: PAVPaymentLoadingAction)
}

final class PAVPaymentLoadingCoordinator: PAVPaymentLoadingCoordinating {
    weak var viewController: UIViewController?
    var paymentOrchestrator: PAVPaymentOrchestrator?
    private let dependency: DependencyContainer
    private let actions: PAVPaymentActions?
    
    init(dependency: DependencyContainer, actions: PAVPaymentActions?) {
        self.dependency = dependency
        self.actions = actions
    }
    
    func perform(action: PAVPaymentLoadingAction) {
        switch action {
        case let .payment(params):
            openPayment(params)
        case .close:
            viewController?.dismiss(animated: true) { [weak self] in
                self?.actions?.paymentCancel()
            }
        }
    }
}

private extension PAVPaymentLoadingCoordinator {
    func openPayment(_ params: OpenPaymentParams ) {
        let model = PAVPaymentOrchestratorModel(
            origin: .none,
            paymentItem: params.paymentItem,
            totalValue: params.totalValue,
            items: params.items,
            isFillableValue: !params.fixedValue,
            dismissPresentingAfterSuccess: params.dismissPresentingAfterSuccess,
            additionalInfo: params.additionalInfo
        )
        paymentOrchestrator = PAVPaymentOrchestrator(orchestrator: model, actions: actions, dependency: dependency)
        
        guard let paymentViewController = paymentOrchestrator?.paymentViewController else {
            return
        }
        
        viewController?.navigationController?.pushViewController(paymentViewController, animated: false)
    }
}
