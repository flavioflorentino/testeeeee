import Foundation

protocol PAVPaymentLoadingDisplay: AnyObject {
    func displayFailure(title: String, description: String)
}
