import Foundation

struct PAVPaymentHashInfo: Decodable {
    let sellerId: Int
    let combinationId: Int?
    let totalValue: String?
    let fixedValue: Bool
    let originId: String
    let date: Int
    let origin: String?
}

private extension PAVPaymentHashInfo {
    private enum CodingKeys: String, CodingKey {
        case sellerId = "seller_id"
        case combinationId = "catalog_item_combination_id"
        case fixedValue = "fixed_value"
        case totalValue = "value"
        case originId = "origin_id"
        case origin
        case date = "created_at"
    }
}
