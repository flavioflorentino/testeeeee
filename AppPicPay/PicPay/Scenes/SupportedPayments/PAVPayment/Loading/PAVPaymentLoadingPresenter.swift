import Core
import Foundation

protocol PAVPaymentLoadingPresenting: AnyObject {
    var viewController: PAVPaymentLoadingDisplay? { get set }
    func displayError(title: String, description: String)
    func didNextStep(action: PAVPaymentLoadingAction)
}

final class PAVPaymentLoadingPresenter: PAVPaymentLoadingPresenting {
    private let coordinator: PAVPaymentLoadingCoordinating
    var viewController: PAVPaymentLoadingDisplay?

    init(coordinator: PAVPaymentLoadingCoordinating) {
        self.coordinator = coordinator
    }
    
    func displayError(title: String, description: String) {
        viewController?.displayFailure(title: title, description: description)
    }
    
    func didNextStep(action: PAVPaymentLoadingAction) {
        coordinator.perform(action: action)
    }
}
