import Core

enum PAVPaymentLoadingEndpoint {
  case PAVInfo(hash: String)
}

extension PAVPaymentLoadingEndpoint: ApiEndpointExposable {
  var path: String {
    switch self {
    case let .PAVInfo(hash):
      return "biz/api/charge/\(hash)"
    }
  }
}
