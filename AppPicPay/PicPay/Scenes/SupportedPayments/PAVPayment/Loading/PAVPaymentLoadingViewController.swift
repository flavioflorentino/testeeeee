import UI
import UIKit

final class PAVPaymentLoadingViewController: ViewController<PAVPaymentLoadingViewModelInputs, UIView> {
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.color = Palette.ppColorGrayscale400.color
        return activityIndicator
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadPayment()
    }
    
    override func buildViewHierarchy() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        view.addSubview(activityIndicator)
    }
    
    override func configureViews() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: DefaultLocalizable.btClose.text,
            style: .plain,
            target: self,
            action: #selector(close)
        )
        title = DefaultLocalizable.paymentTitle.text
        activityIndicator.startAnimating()
    }
    
    override func setupConstraints() {
        activityIndicator.layout {
            $0.centerX == view.centerXAnchor
            $0.centerY == view.centerYAnchor
        }
    }
    
    @objc
    private func close() {
        viewModel.close()
    }
}

extension PAVPaymentLoadingViewController: PAVPaymentLoadingDisplay {
    func displayFailure(title: String, description: String) {
        AlertMessage.showCustomAlert(title: title, message: description, controller: self) {
            self.viewModel.close()
        }
    }
}
