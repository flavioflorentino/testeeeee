import AnalyticsModule
import Foundation

protocol PAVPaymentLoadingViewModelInputs: AnyObject {
    func loadPayment()
    func close()
}

struct OpenPaymentParams {
    let paymentItem: MBItem
    let totalValue: Double
    let items: [PaymentItem]
    let dismissPresentingAfterSuccess: Bool
    let fixedValue: Bool
    let additionalInfo: AnyHashable?
}

final class PAVPaymentLoadingViewModel {
    private let transactionHash: String?
    private let paymentInfo: PAVPaymentInfo?
    private let dismissPresentingAfterSuccess: Bool
    private let orderItems: [PaymentItem]
    private let service: PAVPaymentLoadingServicing
    private let presenter: PAVPaymentLoadingPresenting
    private var fromScanner: Bool = false
    
    init(
        transactionHash: String?,
        paymentInfo: PAVPaymentInfo?,
        dismissPresentingAfterSuccess: Bool,
        orderItems: [PaymentItem],
        service: PAVPaymentLoadingServicing,
        presenter: PAVPaymentLoadingPresenting,
        fromScanner: Bool = false
    ) {
        self.transactionHash = transactionHash
        self.paymentInfo = paymentInfo
        self.dismissPresentingAfterSuccess = dismissPresentingAfterSuccess
        self.orderItems = orderItems
        self.service = service
        self.presenter = presenter
        self.fromScanner = fromScanner
    }
}

extension PAVPaymentLoadingViewModel: PAVPaymentLoadingViewModelInputs {
    func loadPayment() {
        guard let hash = transactionHash else {
            loadPaymentPAV()
            return
        }
        loadPaymentChargeLink(hash: hash)
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
    
    private func loadPaymentPAV() {
        guard
            let storeId = paymentInfo?.storeId,
            let sellerId = paymentInfo?.sellerId,
            let totalValue = paymentInfo?.totalValue
        else {
            presenter.displayError(title: DefaultLocalizable.oops.text, description: DefaultLocalizable.unexpectedError.text)
            return
        }
        
        service.loadItem(storeId: storeId, sellerId: sellerId) { [weak self] result in
            switch result {
            case .success(let item):
                item.sellerId = sellerId
                self?.openPaymentScreen(item: item, totalValue: totalValue, fixedValue: true, addInfo: nil)
            case .failure(let error):
                self?.presenter.displayError(title: error.title, description: error.message)
            }
        }
    }
    
    private func loadPaymentChargeLink(hash: String) {
        service.loadPAVInfo(hash: hash) { [weak self] result in
            switch result {
            case let .success(paymentInfo):
                Analytics.shared.log(StoreDeeplinkEvent.didCallDeeplink(params: paymentInfo))
                self?.loadPaymentScreenChargeLink(pavItem: paymentInfo)
            case let .failure(error):
                self?.presenter.displayError(title: error.title, description: error.message)
            }
        }
    }
    
    private func loadPaymentScreenChargeLink(pavItem: PAVPaymentHashInfo) {
        let totalValue = formatValue(pavItem.totalValue?.onlyNumbers ?? "")
        let origin = fromScanner ? "qrcode" : (pavItem.origin ?? "")
        let sellerId = String(pavItem.sellerId)
        let additionalInfo: AnyHashable = ["origin_id": pavItem.originId, "origin": origin]
        
        service.loadItem(storeId: "", sellerId: sellerId) { [weak self] result in
            switch result {
            case .success(let item):
                item.sellerId = sellerId
                if let combinationID = pavItem.combinationId {
                    item.selectedCombination = Int(combinationID)
                }
                self?.openPaymentScreen(item: item, totalValue: totalValue, fixedValue: pavItem.fixedValue, addInfo: additionalInfo)
            case .failure(let error):
                self?.presenter.displayError(title: error.title, description: error.message)
            }
        }
    }
    
    private func openPaymentScreen(item: MBItem, totalValue: Double, fixedValue: Bool, addInfo: AnyHashable?) {
        let paymentParams = OpenPaymentParams(
            paymentItem: item,
            totalValue: totalValue,
            items: orderItems,
            dismissPresentingAfterSuccess: dismissPresentingAfterSuccess,
            fixedValue: fixedValue,
            additionalInfo: addInfo)
        
        presenter.didNextStep(action: .payment(paymentParams))
    }
    
    private func formatValue(_ value: String) -> Double {
        var totalValue = value

        switch totalValue.length {
        case let lenght where lenght <= 1:
            totalValue.insert(contentsOf: "0.0", at: totalValue.startIndex)
        case let lenght where lenght == 2:
            totalValue.insert(contentsOf: "0.", at: totalValue.startIndex)
        default:
            totalValue.insert(contentsOf: ".", at: totalValue.index(totalValue.endIndex, offsetBy: -2))
        }
        return Double(totalValue) ?? 0.0
    }
}
