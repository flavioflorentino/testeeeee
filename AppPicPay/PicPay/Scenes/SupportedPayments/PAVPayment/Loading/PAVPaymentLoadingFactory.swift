import Foundation
import VendingMachine

struct PAVPaymentInfo {
    let storeId: String
    let sellerId: String
    let totalValue: Double
}

enum PAVPaymentLoadingFactory {
    static func make(
        transactionHash: String? = nil,
        paymentInfo: PAVPaymentInfo?,
        orderItems: [PaymentItem],
        dismissPresentingAfterSuccess: Bool,
        dependency: DependencyContainer,
        actions: PAVPaymentActions?,
        fromScanner: Bool = false
    ) -> PAVPaymentLoadingViewController {
        let service: PAVPaymentLoadingServicing = PAVPaymentLoadingService(dependency: dependency)
        let coordinator: PAVPaymentLoadingCoordinating = PAVPaymentLoadingCoordinator(dependency: dependency, actions: actions)
        let presenter: PAVPaymentLoadingPresenting = PAVPaymentLoadingPresenter(coordinator: coordinator)
        let viewModel = PAVPaymentLoadingViewModel(
            transactionHash: transactionHash,
            paymentInfo: paymentInfo,
            dismissPresentingAfterSuccess: dismissPresentingAfterSuccess,
            orderItems: orderItems,
            service: service,
            presenter: presenter,
            fromScanner: fromScanner
        )
        let viewController = PAVPaymentLoadingViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
