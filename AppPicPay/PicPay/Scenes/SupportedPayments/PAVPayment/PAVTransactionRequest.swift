import Foundation

struct PAVTransactionRequest {
    let pin: String
    let sellerId: String
    let planType: String
    let totalValue2: Double
    let shipping = "0"
    let shippingId = ""
    let creditCardId: String
    let addressId = ""
    let selectedInstallment: Int
    let consumerCredit: Double
    let items: [[String: String]]
    let surcharge = ""
    let fromExplore: Bool
    let someErrorOccurred: Bool
    let biometry: Bool
    let paymentPrivacy: String
    let latitude: String
    let longitude: String
    let accuracy: String
    
    func args() -> [Any] {
        [
            pin,
            sellerId,
            planType,
            NSDecimalNumber(value: totalValue2).stringValue,
            shipping,
            shippingId,
            creditCardId,
            addressId,
            "\(selectedInstallment)",
            NSDecimalNumber(value: consumerCredit).stringValue,
            items,
            surcharge,
            fromExplore ? "1":"0",
            someErrorOccurred ? "1":"0",
            biometry ? "1":"0",
            accuracy,
            latitude,
            longitude,
            "",
            paymentPrivacy
        ]
    }
}
