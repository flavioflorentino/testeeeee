import CorePayment
import Foundation
import VendingMachine

final class PAVPaymentActions: PaymentActionsDelegate {
    var paymentMethod: PaymentMethod?
    var installment: Int?
    var extraParams: Decodable?
    
    private weak var vmPaymentInputs: VendingMachinePaymentInputs?
    
    init(vmPaymentInputs: VendingMachinePaymentInputs?) {
        self.vmPaymentInputs = vmPaymentInputs
    }
    
    func paymentSuccess(transactionId: String, value: Double) {
        vmPaymentInputs?.paymentSuccess(transactionId: transactionId)
    }
    
    func paymentCancel() {
        vmPaymentInputs?.paymentCancel()
    }
}
