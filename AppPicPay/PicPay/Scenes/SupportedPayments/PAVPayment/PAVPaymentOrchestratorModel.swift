import Foundation

struct PAVPaymentOrchestratorModel {
    let origin: Origin
    let paymentItem: MBItem
    let totalValue: Double
    let items: [PaymentItem]
    let isFillableValue: Bool
    let dismissPresentingAfterSuccess: Bool
    let additionalInfo: AnyHashable?
}

extension PAVPaymentOrchestratorModel {
    enum Origin {
        case none
        
        var value: String {
            ""
        }
    }
}
