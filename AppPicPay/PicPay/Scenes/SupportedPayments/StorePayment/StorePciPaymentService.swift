import Core
import CorePayment
import Foundation
import Store
import PCI
import SwiftyJSON

final class StorePciPaymentService: LegacyPaymentServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    private let servicePci: StoreServiceProtocol
    private let item: StorePayment
    
    let isFlowPci: Bool = true
    
    init(item: StorePayment,
         servicePci: StoreServiceProtocol = StoreService(),
         dependencies: Dependencies = DependencyContainer()) {
        self.item = item
        self.servicePci = servicePci
        self.dependencies = dependencies
    }
    
    func createTransaction(payment: Payment, completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let payload = createPayload(payment: payment)
        servicePci.createTransaction(
            password: payment.password,
            payload: payload,
            isNewArchitecture: true
        ) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let result):
                    self?.createTransactionSuccess(data: result.json, completion: completion)
                case .failure(let error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }
    
    private func createPayload(payment: Payment) -> PaymentPayload<StoreRequestPayload> {
        let inputData = item.inputData.map {
            StoreData.Model(key: $0.key, value: "\($0.value)")
        }
        let externalData = item.externalData.map {
            StoreData.Model(key: $0.key, value: $0.value)
        }
        let creditCard = PaymentMethod.UserCreditCard(id: payment.cardId,
                                                      amount: "\(payment.cardValueWithoutInterest)",
                                                      installments: payment.installment)
        let balance = PaymentMethod.UserBalance(amount: "\(payment.balanceValue)")
        
        let data = StoreData(inputData: inputData, externalData: externalData)
        let paymentMethod = PaymentMethod(userBalance: balance, creditCard: creditCard)
        let payload = PaymentPayload(cvv: CVVPayload(value: payment.cvv),
                                     generic: StoreRequestPayload(storeId: item.storeId,
                                                                  feedVisibility: payment.privacyConfig.stringValue,
                                                                  paymentMethod: paymentMethod,
                                                                  data: data))
        return payload
    }
    
    func createTransactionSuccess(data: [String: Any], completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        guard
            let jsonData = data.toData(),
            let json = try? JSON(data: jsonData),
            let receipt = json.dictionary?["receipt"]?.array,
            let transaction = json.dictionary?["Transaction"]?.dictionary,
            let id = transaction["id"]?.string
        else {
            let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
            completion(.failure(error))
            return
        }
        
        NotificationCenter.default.post(name: Notification.Name.Payment.new, object: nil, userInfo: ["type": "Digital Goods"])
        
        let widgets = WSReceipt.createReceiptWidgetItem(receipt)
        let receiptViewModel = ReceiptWidgetViewModel(transactionId: id, type: .store)
        receiptViewModel.setReceiptWidgets(items: widgets)
        
        completion(.success(receiptViewModel))
    }
}
