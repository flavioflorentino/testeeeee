import FeatureFlag
import Foundation
import Store

final class StorePaymentOrchestrator {
    lazy var paymentViewController: UIViewController? = {
        let viewController = createContainerPayment()
        if #available(iOS 13.0, *) {
            viewController?.isModalInPresentation = true
        }
        
        viewController?.title = DefaultLocalizable.voucherPaymentTitle.text
        return viewController
    }()
    
    private let item: StorePayment
    
    init(_ item: StorePayment) {
        self.item = item
    }
    
    private func createContainerPayment() -> UIViewController? {
        let paymentService = StorePciPaymentService(item: item)
        let accessory = createPaymentAccessory()
        let headerPaymentContract = createHeaderPaymentContract()
        let toolbarViewController = PaymentToolbarFactory.make()

        return ContainerPaymentReceiptFactory.make(
            paymentService: paymentService,
            actionsDelegate: nil,
            headerViewController: headerPaymentContract,
            accessoryViewController: accessory,
            toolbarViewController: toolbarViewController
        )
    }
    
    private func createPaymentAccessory() -> AccessoryPaymentContract {
        var items: [ItensPayment] = []
        
        for style in item.styles {
            switch style {
            case let style as MarkdownStyle:
                items.append(PaymentMarkdownCell(markdown: style.markdown))
            case let style as DescriptionStyle:
                items.append(PaymentDetailCell(detail: style.description))
            case let style as SideBySideStyle:
                let popup = SurchargePayment(title: style.popup?.title ?? "",
                                             description: style.popup?.description ?? "",
                                             surcharge: style.popup?.surcharge ?? 0,
                                             surchargeAmount: style.popup?.surchargeAmount ?? 0)
                
                items.append(PaymentTwoLabelsCell(left: style.left, right: style.right, buttonIsEnable: style.buttonIsEnable, popup: popup))
            default:
                break
            }
        }
        
        return DGTableViewPaymentFactory.make(model: items)
    }
    
    private func createHeaderPaymentContract() -> HeaderPaymentContract {
        let modelHeader = createModelPayment()
        let modelPresenter = createModelHeader()
        
        return PaymentStandardHeaderFactory.make(modelPresenter: modelPresenter, model: modelHeader)
    }
    
    private func createModelHeader() -> HeaderStandard {
        HeaderStandard(
            title: item.description,
            description: "",
            image: .url(item.imageURL),
            isEditable: false,
            link: item.infoURL,
            newInstallmentEnable: FeatureManager.isActive(.featureInstallmentNewButton),
            isInstallmentsEnabled: true
        )
    }
    
    private func createModelPayment() -> HeaderPayment {
        HeaderPayment(
            sellerId: nil,
            payeeId: item.storeId,
            value: item.value,
            installments: 1,
            payeeType: .none,
            paymentType: .store
        )
    }
}
