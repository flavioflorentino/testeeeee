import Billet
import Core
import Foundation

struct BilletPaymentOrchestratorModel {
    let origin: BilletOrigin
    let model: BilletResponse
    let description: String?
}
