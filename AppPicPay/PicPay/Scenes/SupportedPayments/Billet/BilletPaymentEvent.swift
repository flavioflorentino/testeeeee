import AnalyticsModule
import CorePayment
import Foundation
import Billet

enum BilletPaymentEvent: AnalyticsKeyProtocol {
    case transactionViewed(installment: Int, paymentMethod: PaymentMethod, origin: BilletOrigin)
    case transactionAccomplished(installment: Int, origin: BilletOrigin)
    case transactionScheduled(paymentMethod: PaymentMethod, scheduledDate: String, origin: BilletOrigin)
    case paymentDueDateDenied(origin: BilletOrigin)
    case paymentTimeoutDenied(origin: BilletOrigin)
    case cardOnBillsOptionSelected(optionSelected: BilletEventOptionSelection)
    case cardOnBillsVersion(isSplitApplied: Bool)
    case receipt(isASchedulling: Bool)
    
    private var name: String {
        switch self {
        case .transactionViewed:
            return "Transaction Screen"
        case .transactionAccomplished:
            return "Transaction Accomplished"
        case .transactionScheduled:
            return "Transaction Scheduled"
        case .paymentDueDateDenied:
            return "Payment Due Date Denied"
        case .paymentTimeoutDenied:
            return "Payment Timeout Denied"
        case .cardOnBillsOptionSelected:
            return "Card On Bills"
        case .cardOnBillsVersion:
            return "Card On Bills Version"
        case .receipt:
            return "Receipt"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .transactionViewed(installment, paymentMethod, origin):
            return [
                "split_viewd": "\(installment)",
                "origin_payment_method": method(for: paymentMethod),
                "origin": origin.rawValue
            ]
            
        case let .transactionAccomplished(installment, origin):
            guard installment > 1 else {
                return [
                    "tax_applied": "card",
                    "origin": origin.rawValue
                ]
            }
            
            return [
                "tax_applied": "card+split",
                "split_viewd": "\(installment)",
                "origin": origin.rawValue
            ]
            
        case let .transactionScheduled(paymentMethod, date, origin):
            return [
                "payment_method": method(for: paymentMethod),
                "payment_date_scheduled": date,
                "origin": origin.rawValue
            ]
            
        case let .cardOnBillsOptionSelected(optionSelected):
            return [
                "option_selected": optionSelected.rawValue
            ]
            
        case let .cardOnBillsVersion(isSplitApplied):
            return [
                "split_applied": isSplitApplied
            ]
        case .paymentDueDateDenied(let origin):
            return [
                "origin": origin.rawValue
            ]
        case .paymentTimeoutDenied(let origin):
            return [
                "origin": origin.rawValue
            ]
        case .receipt(let isASchedulling):
            return [
                "scheduled": isASchedulling
            ]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .eventTracker]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("Bills - \(name)", properties: properties, providers: providers)
    }
    
    private func method(for paymentMethod: PaymentMethod) -> String {
        switch paymentMethod {
        case .accountBalance:
            return "wallet"
        case .card:
            return "card"
        case .balanceAndCard:
            return "wallet+card"
        }
    }
}
