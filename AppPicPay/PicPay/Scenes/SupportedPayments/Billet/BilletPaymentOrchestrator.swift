import AssetsKit
import Billet
import Core
import CorePayment
import FeatureFlag
import Foundation
import PCI
import UIKit

final class BilletPaymentOrchestrator {
    typealias Dependencies = HasFeatureManager
    
    private let orchestrator: BilletPaymentOrchestratorModel
    private let dependencies: Dependencies
    
    var paymentViewController: UIViewController? {
        let viewController = createContainerPayment()
        if #available(iOS 13.0, *) {
            viewController?.isModalInPresentation = true
        }
        
        viewController?.title = DefaultLocalizable.paymentTitle.text
        
        return viewController
    }
    
    init(orchestrator: BilletPaymentOrchestratorModel, dependencies: Dependencies = DependencyContainer()) {
        self.orchestrator = orchestrator
        self.dependencies = dependencies
    }
    
    private func createContainerPayment() -> UIViewController? {
        guard let headerPaymentContract = createHeaderPaymentContract() else { return nil }
        
        let accessoryViewController = BilletAccessoryFactory.make(with: orchestrator.model,
                                                                  andDescription: orchestrator.description)
        let toolbarViewController = PaymentToolbarFactory.make()
        
        if dependencies.featureManager.isActive(.isNewReceiptAvailable) {
            let paymentService = BilletPaymentService(pci: PaymentBillService(), orchestrator: orchestrator)
            
            return ContainerPaymentFactory.make(coordinatorSuccess: BilletPaymentCoordinator(),
                                                paymentService: paymentService,
                                                actionsDelegate: BilletPaymentAnalytics(origin: orchestrator.origin),
                                                headerViewController: headerPaymentContract,
                                                accessoryViewController: accessoryViewController,
                                                toolbarViewController: toolbarViewController)
        } else {
            let paymentService = BilletLegacyPaymentService(pci: PaymentBillService(), orchestrator: orchestrator)
            
            return ContainerPaymentFactory.make(coordinatorSuccess: BilletLegacyPaymentCoordinator(),
                                                paymentService: paymentService,
                                                actionsDelegate: BilletPaymentAnalytics(origin: orchestrator.origin),
                                                headerViewController: headerPaymentContract,
                                                accessoryViewController: accessoryViewController,
                                                toolbarViewController: toolbarViewController)
        }
    }
    
    private func createHeaderPaymentContract() -> HeaderPaymentContract? {
        guard
            let modelHeader = createModelPayment(),
            let modelPresenter = createModelHeader()
            else { return nil }
        
        return PaymentStandardHeaderFactory.make(modelPresenter: modelPresenter, model: modelHeader)
    }
    
    private func createModelHeader() -> HeaderStandard? {
        HeaderStandard(
            title: DefaultLocalizable.voucherPaymentTitle.text,
            description: nil,
            image: .image(Resources.Placeholders.placeholderBarcode.image),
            isEditable: false,
            link: dependencies.featureManager.text(.opsBilletPaymentFaqUrl),
            newInstallmentEnable: dependencies.featureManager.isActive(.featureInstallmentNewButton),
            isInstallmentsEnabled: true
        )
    }
    
    private func createModelPayment() -> HeaderPayment? {
        let value = Double(orchestrator.model.amount) ?? 0
        return HeaderPayment(
            sellerId: nil,
            payeeId: "",
            value: value,
            installments: 1,
            payeeType: .none,
            paymentType: .billet
        )
    }
}
