import AnalyticsModule
import CorePayment
import Billet
import Foundation

final class BilletPaymentAnalytics: PaymentActionsDelegate {
    var paymentMethod: PaymentMethod?
    var installment: Int?
    var extraParams: Decodable?
    
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private var origin: BilletOrigin
    
    init(origin: BilletOrigin, dependencies: Dependencies = DependencyContainer()) {
        self.origin = origin
        self.dependencies = dependencies
    }
    
    func viewWillAppear() {
        guard let installment = installment, let paymentMethod = paymentMethod else { return }
        
        let event: BilletPaymentEvent = .transactionViewed(installment: installment, paymentMethod: paymentMethod, origin: origin)
        dependencies.analytics.log(event)
    }
    
    func paymentSuccess(transactionId: String, value: Double) {
        guard let installment = installment, let paymentMethod = paymentMethod else { return }
        
        var event: BilletPaymentEvent
        
        event = .transactionAccomplished(installment: installment, origin: origin)
        dependencies.analytics.log(event)
        
        event = .cardOnBillsVersion(isSplitApplied: installment > 1)
        dependencies.analytics.log(event)
        
        if let params = extraParams as? BilletPaymentExtraParams, case .scheduling = params.type {
            event = .transactionScheduled(paymentMethod: paymentMethod, scheduledDate: params.date, origin: origin)
            dependencies.analytics.log(event)
        }
    }
    
    func paymentFailure(error: PaymentActionErrorViewModel) {
        let event: BilletPaymentEvent
        
        switch error.code {
        case 16_032_001:
            event = .paymentDueDateDenied(origin: origin)
        case 16_032_000:
            event = .paymentTimeoutDenied(origin: origin)
        default:
            return
        }
        
        dependencies.analytics.log(event)
    }
}
