import Billet
import Core
import CorePayment
import Foundation
import PCI

final class BilletLegacyPaymentService: ServiceReceiptProtocol {
    typealias Dependencies = HasMainQueue
    
    private let pciService: PaymentBillServiceProtocol
    private let orchestrator: BilletPaymentOrchestratorModel
    private let dependencies: Dependencies
    
    init(pci: PaymentBillServiceProtocol,
         orchestrator: BilletPaymentOrchestratorModel,
         dependencies: Dependencies = DependencyContainer()) {
        self.pciService = pci
        self.orchestrator = orchestrator
        self.dependencies = dependencies
    }
        
    func createTransaction(payment: Payment, completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        guard let billetPayload = createBilletPayload(payment: payment) else {
            return completion(.failure(PicPayError(message: DefaultLocalizable.unexpectedError.text)))
        }
        
        let cvv = createCVVPayload(cvv: payment.cvv)
        let payload = PaymentPayload<BillPayload>(cvv: cvv, generic: billetPayload)
        
        pciService.pay(password: payment.password, payload: payload, isNewArchitecture: true) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let value):
                    let response = BaseApiGenericResponse(dictionary: value.json)

                    switch payment.paymentType {
                    case .default:
                        self?.createReceipt(with: response, completion: completion)
                        
                    case .scheduling:
                        self?.createSchedulingReceipt(with: response, completion: completion)
                    }
                    
                case .failure(let error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }
}

private extension BilletLegacyPaymentService {
    func createCVVPayload(cvv: String?) -> CVVPayload? {
        guard let cvv = cvv else { return nil }
        return CVVPayload(value: cvv)
    }
    
    func createBilletPayload(payment: Payment) -> BillPayload? {
        guard let params = payment.extra as? BilletPaymentExtraParams, let cip = orchestrator.model.cipObj else {
            return nil
        }
        
        return BillPayload(
            code: orchestrator.model.linecode,
            description: orchestrator.description ?? "",
            origin: orchestrator.origin.rawValue,
            password: payment.password,
            biometry: payment.biometry,
            credit: String(format: "%.2f", payment.balanceValue),
            value: String(format: "%.2f", payment.cardValueWithoutInterest),
            installments: payment.installment,
            planType: "A",
            addressId: "0",
            someErrorOccurred: payment.someErrorOccurred ? 1 : 0,
            privacyConfig: payment.privacyConfig.stringValue,
            cardId: payment.cardId,
            paymentType: params.type.rawValue,
            schedulePaymentDate: params.date,
            cip: cip
        )
    }
    
    func createReceipt(with response: BaseApiGenericResponse,
                       completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        guard let data = response.json["data"].dictionary else {
            return completion(.failure(PicPayError(message: DefaultLocalizable.unexpectedError.text)))
        }
        
        var transactionId = ""
        
        if let id = data["id"]?.string {
            transactionId = id
        } else if let transaction = data["Transaction"]?.dictionary, let id = transaction["id"]?.string {
            transactionId = id
        }
        
        guard transactionId.isNotEmpty, let receipt = data["receipt"]?.array else {
            return completion(.failure(PicPayError(message: DefaultLocalizable.unexpectedError.text)))
        }
        
        let widgets: [ReceiptWidgetItem] = WSReceipt.createReceiptWidgetItem(receipt)
        let receiptViewModel = ReceiptWidgetViewModel(transactionId: transactionId, type: .PAV)
        
        if widgets.isNotEmpty {
            receiptViewModel.setReceiptWidgets(items: widgets)
        }
        
        NotificationCenter.default.post(name: Notification.Name.Payment.new, object: nil, userInfo: ["type": "Digital Goods"])
        
        completion(.success(receiptViewModel))
    }
    
    func createSchedulingReceipt(with response: BaseApiGenericResponse,
                                 completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        completion(.failure(PicPayError(message: DefaultLocalizable.unexpectedError.text)))
    }
}
