import AnalyticsModule
import AssetsKit
import Billet
import CorePayment
import Foundation
import UI
import UIKit

final class BilletLegacyPaymentCoordinator: PaymentSuccessCoordinating {
    typealias Success = ReceiptWidgetViewModel
    
    func showPaymentSuccess(model: ReceiptWidgetViewModel, paymentType: PaymentType, viewController: UIViewController?) {
        guard let viewController = viewController else {
            return
        }
        
        switch paymentType {
        case .default:
            TransactionReceipt.showReceiptSuccess(viewController: viewController, receiptViewModel: model)
        case .scheduling:
            presentSchedulingSuccessFlow(model: model, viewController: viewController)
        }
    }
    
    private func presentSchedulingSuccessFlow(model: ReceiptWidgetViewModel, viewController: UIViewController) {
        let description = NSAttributedString(string: PaymentLocalizable.billetPaymentSuccessSchedullingMessage.text)
        let feedbackContent = ApolloFeedbackViewContent(image: Resources.Illustrations.iluPersonInfo.image,
                                                        title: PaymentLocalizable.billetPaymentSuccessSchedullingTitle.text,
                                                        description: description,
                                                        primaryButtonTitle: PaymentLocalizable.okUnderstood.text,
                                                        secondaryButtonTitle: "")
        let feedbackController = ApolloFeedbackViewController(content: feedbackContent)
        feedbackController.didTapPrimaryButton = {
            feedbackController.dismiss(animated: true) {
                TransactionReceipt.showReceipt(viewController: viewController, receiptViewModel: model)
            }
        }
        
        viewController.present(feedbackController, animated: true, completion: nil)
    }
}
