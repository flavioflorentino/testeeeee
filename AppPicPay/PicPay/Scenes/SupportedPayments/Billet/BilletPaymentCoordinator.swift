import AnalyticsModule
import AssetsKit
import Billet
import CorePayment
import Foundation
import ReceiptKit
import UI
import UIKit

final class BilletPaymentCoordinator: PaymentSuccessCoordinating {
    typealias Success = ReceiptEntryPoint
    
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func showPaymentSuccess(model: ReceiptEntryPoint, paymentType: PaymentType, viewController: UIViewController?) {
        guard
            let viewController = viewController,
            let rootViewController = UIApplication.shared.keyWindow?.rootViewController
        else {
            return
        }
        
        viewController.dismiss(animated: true) {
            switch paymentType {
            case .default:
                let receiptController = DefaultReceiptFactory.make(with: model)
                self.animateReceiptTransition(receiptViewController: receiptController, rootViewController: rootViewController)
             
            case .scheduling:
                self.presentSchedulingSuccessFlow(model: model, rootViewController: rootViewController)
            }
        }
        
        let event: BilletPaymentEvent = .receipt(isASchedulling: paymentType == .scheduling)
        dependencies.analytics.log(event)
    }
    
    private func presentSchedulingSuccessFlow(model: ReceiptEntryPoint, rootViewController: UIViewController) {
        let description = NSAttributedString(string: PaymentLocalizable.billetPaymentSuccessSchedullingMessage.text)
        let feedbackContent = ApolloFeedbackViewContent(image: Resources.Illustrations.iluSuccess.image,
                                                        title: PaymentLocalizable.billetPaymentSuccessSchedullingTitle.text,
                                                        description: description,
                                                        primaryButtonTitle: PaymentLocalizable.okUnderstood.text,
                                                        secondaryButtonTitle: "")
        let feedbackController = ApolloFeedbackViewController(content: feedbackContent)
        feedbackController.didTapPrimaryButton = {
            feedbackController.dismiss(animated: true) {
                let service = BilletReceiptService(entryPoint: model, dependencies: DependencyContainer())
                let receiptNavController = DefaultReceiptFactory.make(with: service)
                rootViewController.present(receiptNavController, animated: true)
            }
        }
        
        rootViewController.present(feedbackController, animated: true)
    }
    
    private func animateReceiptTransition(receiptViewController: UIViewController, rootViewController: UIViewController) {
        TaskCompleteAnimation.showSuccess(onView: rootViewController.view) {
            rootViewController.present(receiptViewController, animated: true)
        }
    }
}
