import CorePayment
import PIX
import UI
import UIKit

private extension QRCodeAccessoryPaymentViewController.Layout {
    static let lineHeight: CGFloat = 0.5
}

final class QRCodeAccessoryPaymentViewController: UIViewController, PaymentAccessoryInput {
    fileprivate enum Layout { }
    
    private let items: [[QRCodeAcessoryPaymentModel]]
    weak var delegate: PaymentAccessoryOutput?

    init(items: [[QRCodeAcessoryPaymentModel]]) {
        self.items = items
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private let containerView = UIView()
    private let scrollView = UIScrollView()

    private let stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.distribution = .equalSpacing
        view.spacing = Spacing.base01
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
    }

    private func buildItems() {
        for (index, itemArray) in items.enumerated() {
            itemArray.forEach {
                stackView.addArrangedSubview(buildItemView(item: $0))
            }
            if index != items.count - 1 {
                stackView.addArrangedSubview(buildLineView())
            }
        }
    }

    private func buildItemView(item: QRCodeAcessoryPaymentModel) -> UIView {
        let titleLabel = buildItemTitleLabelWith(text: item.title)
        let valueLabel = buildItemValueLabelWith(text: item.value)
        return UIStackView(arrangedSubviews: [titleLabel, valueLabel])
    }

    private func buildItemTitleLabelWith(text: String) -> UILabel {
        let label = UILabel()
        label.font = Typography.bodySecondary(.default).font()
        label.textColor = Colors.grayscale700.color
        label.text = text
        return label
    }

    private func buildItemValueLabelWith(text: String) -> UILabel {
        let label = UILabel()
        label.font = Typography.bodySecondary(.highlight).font()
        label.textColor = Colors.grayscale700.color
        label.text = text
        label.textAlignment = .right
        return label
    }

    private func buildLineView() -> UIView {
        let containerView = UIView()
        let lineView = UIView()
        containerView.addSubview(lineView)
        lineView.backgroundColor = Colors.grayscale100.color
        lineView.snp.makeConstraints {
            $0.height.equalTo(Layout.lineHeight)
            $0.centerY.leading.trailing.equalToSuperview()
        }
        containerView.snp.makeConstraints {
            $0.height.equalTo(Spacing.base04)
        }
        return containerView
    }
}

extension QRCodeAccessoryPaymentViewController: ViewConfiguration {
    func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(stackView)
    }

    func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        containerView.snp.makeConstraints {
            $0.edges.width.equalToSuperview().inset(Spacing.base02)
        }
        stackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        buildItems()
    }
}
