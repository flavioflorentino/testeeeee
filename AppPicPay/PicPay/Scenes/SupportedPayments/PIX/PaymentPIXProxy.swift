import AnalyticsModule
import PIX
import UIKit

final class PaymentPIXProxy: PIXPaymentOpening {
    func openPaymentWithDetails(
        _ details: PIXPaymentDetails,
        onViewController controller: UIViewController,
        origin: PixPaymentOrigin,
        flowType: PixPaymentFlowType
    ) {
        let orchestrator = PIXPaymentOrchestrator(paymentDetails: details, origin: origin, type: flowType)
        guard let paymentController = orchestrator.paymentViewController else {
            return
        }
        controller.navigationController?.pushViewController(paymentController, animated: true)
    }
}
