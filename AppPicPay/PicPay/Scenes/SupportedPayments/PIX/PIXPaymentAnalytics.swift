import AnalyticsModule
import CorePayment
import Foundation
import PIX

final class PIXPaymentAnalytics: PaymentActionsDelegate {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let origin: PixPaymentOrigin
    
    var paymentMethod: PaymentMethod?
    var installment: Int?
    var extraParams: Decodable?

    init(origin: PixPaymentOrigin, dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
        self.origin = origin
    }

    func viewWillAppear() {
        dependencies.analytics.log(PixPaymentUserEvent.transactionViewed(origin: origin))
    }

    func didTapInstallments() {
        dependencies.analytics.log(PixPaymentUserEvent.didTapInstallments)
    }

    func didTapChangePaymentMethod() {
        dependencies.analytics.log(PixPaymentUserEvent.didChangePaymentMethod)
    }
}
