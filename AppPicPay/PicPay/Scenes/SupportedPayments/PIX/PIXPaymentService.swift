import Core
import CorePayment
import PIX
import SwiftyJSON

final class PIXPaymentService: ServiceReceiptProtocol {
    typealias Dependencies = HasMainQueue

    enum PaymentType {
        case payment(params: PIXParams)
        case `return`(transactionId: String)
    }

    private let dependencies: Dependencies
    private let paymentType: PaymentType

    init(paymentType: PaymentType, dependencies: Dependencies = DependencyContainer()) {
        self.paymentType = paymentType
        self.dependencies = dependencies
    }
    
    func createTransaction(payment: Payment, completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let endpoint = getEndpoint(payment: payment)

        let api = Api<PIXPaymentResponse>(endpoint: endpoint)
        api.execute { [weak self] result in
            guard let self = self else { return }
            let mappedResult = result.map(\.model)
            self.dependencies.mainQueue.async {
                switch mappedResult {
                case let .success(receipt):
                    guard let receipt = self.createReceiptWidgetViewModel(value: receipt.json) else {
                        completion(.failure(.generalError))
                        return
                    }
                    completion(.success(receipt))
                case let .failure(error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }

    private func createReceiptWidgetViewModel(value: [String: Any]) -> ReceiptWidgetViewModel? {
        guard
            let jsonData = value.toData(),
            let json = try? JSON(data: jsonData),
            let receipt = json["receipt"].array
            else {
                return nil
        }

        let widgets = WSReceipt.createReceiptWidgetItem(receipt)
        let receiptViewModel = ReceiptWidgetViewModel(type: .P2P)
        receiptViewModel.setReceiptWidgets(items: widgets)
        return receiptViewModel
    }

    private func getEndpoint(payment: Payment) -> PixPaymentServiceEndpoint {
        switch paymentType {
        case let .payment(pixParams):
            let paymentParams = createPaymentParams(payment: payment)
            return .sendPayment(paymentParams, pixParams, password: payment.password)
        case let .`return`(transactionId):
            let returnParams = createReturnParams(payment: payment, transactionId: transactionId)
            return .returnPayment(params: returnParams, password: payment.password)
        }
    }

    private func createPaymentParams(payment: Payment) -> PIXPaymentParams {
        PIXPaymentParams(biometry: payment.biometry,
                         consumerValue: String(payment.value),
                         credit: String(payment.cardValueWithoutInterest),
                         installments: payment.installment,
                         creditCardId: payment.cardId,
                         feedVisibility: payment.privacyConfig.stringValue,
                         description: payment.message,
                         ignoreBalance: false)
    }

    private func createReturnParams(payment: Payment, transactionId: String) -> PIXReturnParams {
        PIXReturnParams(biometry: payment.biometry,
                        consumerValue: String(payment.value),
                        credit: String(payment.cardValueWithoutInterest),
                        installments: payment.installment,
                        creditCardId: payment.cardId,
                        feedVisibility: payment.privacyConfig.stringValue,
                        description: payment.message,
                        ignoreBalance: false,
                        originalTransactionId: transactionId)
    }
}
