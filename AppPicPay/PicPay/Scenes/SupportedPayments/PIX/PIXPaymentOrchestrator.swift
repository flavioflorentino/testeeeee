import Foundation
import PIX

final class PIXPaymentOrchestrator {
    var paymentViewController: ContainerPaymentViewController? {
        let viewController = createContainerPayment()
        if #available(iOS 13.0, *) {
            viewController?.isModalInPresentation = true
        }

        viewController?.title = paymentDetails.title
        
        return viewController
    }
    
    private let paymentDetails: PIXPaymentDetails
    private let origin: PixPaymentOrigin
    private let type: PixPaymentFlowType
    private let paymentManager: PPPaymentManager = {
        let ppManager = PPPaymentManager()
        ppManager?.forceBalance = true
        return ppManager ?? PPPaymentManager()
    }()
    
    init(paymentDetails: PIXPaymentDetails, origin: PixPaymentOrigin, type: PixPaymentFlowType = .default) {
        self.paymentDetails = paymentDetails
        self.origin = origin
        self.type = type
    }
    
    private func createContainerPayment() -> ContainerPaymentViewController? {
        let headerPaymentContract = createHeaderPaymentContract()

        let paymentType = getPaymentType()
        let paymentService = PIXPaymentService(paymentType: paymentType)

        let acessoryPaymentController: AccessoryPaymentContract

        switch paymentDetails.screenType {
        case let .default(commentPlaceholder):
            acessoryPaymentController = CommentPaymentFactory.make(commentPlaceholder: commentPlaceholder)
        case let .qrCode(items):
            acessoryPaymentController = QRCodeAccessoryPaymentViewController(items: items)
        }

        let toolbarViewController = PaymentToolbarFactory.make(paymentToolbarType: getPaymentToolbarType())

        let analytics = PIXPaymentAnalytics(origin: origin)
        
        let containerViewController = ContainerPaymentReceiptFactory.make(
            paymentService: paymentService,
            actionsDelegate: analytics,
            headerViewController: headerPaymentContract,
            accessoryViewController: acessoryPaymentController,
            toolbarViewController: toolbarViewController,
            paymentManager: paymentManager
        )

        if case .default = type {
            containerViewController.shouldDisplayCloseButton = false
        }

        return containerViewController
    }

    private func createHeaderPaymentContract() -> HeaderPaymentContract {
        let modelHeader = createModelPayment()
        let modelPresenter = createModelHeader()
        
        return PaymentStandardHeaderFactory.make(modelPresenter: modelPresenter, model: modelHeader, paymentManager: paymentManager)
    }
    
    private func createModelHeader() -> HeaderStandard {
        HeaderStandard(
            title: paymentDetails.payee.name,
            description: paymentDetails.payee.description,
            image: .url(paymentDetails.payee.imageURLString),
            isEditable: paymentDetails.isEditable,
            link: nil,
            newInstallmentEnable: true,
            isInstallmentsEnabled: true
        )
    }
    
    private func createModelPayment() -> HeaderPayment {
        HeaderPayment(
            sellerId: nil,
            payeeId: paymentDetails.payee.id ?? String(),
            value: paymentDetails.value,
            installments: 1,
            payeeType: .none,
            paymentType: .pix
        )
    }

    private func getPaymentToolbarType() -> PaymentToolbarType {
        let changePaymentAlert = createChangePaymentMethodAlert()
        switch type {
        case .default:
            return .pix(alert: changePaymentAlert)
        case .pixReturn:
            return .pixReturn(changePaymentAlert: changePaymentAlert, changePrivacyAlert: createChangePrivacyAlert())
        }
    }

    private func createChangePaymentMethodAlert() -> Alert {
        let buttons = [Button(title: DefaultLocalizable.btOkUnderstood.text, type: .cta)]
        let alert = Alert(
            with: PaymentLocalizable.changeUnavailable.text,
            text: PaymentLocalizable.cantPayOtherWayWithPIX.text,
            buttons: buttons
        )
        return alert
    }

    private func createChangePrivacyAlert() -> Alert {
        let buttons = [Button(title: DefaultLocalizable.btOkUnderstood.text, type: .cta)]
        let alert = Alert(
            with: PaymentLocalizable.pixPrivateReturnTitle.text,
            text: PaymentLocalizable.pixPrivateReturnDescription.text,
            buttons: buttons
        )
        return alert
    }

    private func getPaymentType() -> PIXPaymentService.PaymentType {
        let paymentType: PIXPaymentService.PaymentType

        switch type {
        case .`default`:
            paymentType = .payment(params: paymentDetails.pixParams)
        case let .pixReturn(transactionId):
            paymentType = .return(transactionId: transactionId)
        }

        return paymentType
    }
}
