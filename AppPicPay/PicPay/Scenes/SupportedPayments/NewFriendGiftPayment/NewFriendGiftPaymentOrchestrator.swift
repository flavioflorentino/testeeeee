import FeatureFlag
import Foundation
import PCI

final class NewFriendGiftPaymentOrchestrator {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    private let contact: PPContact
    private let followingStatus: FollowerStatus
    private let model: NewFriendGiftPaymentModel
    
    var paymentViewController: UIViewController {
        let viewController = containerPayment
        if #available(iOS 13.0, *) {
            viewController.isModalInPresentation = true
        }
        
        viewController.title = model.title
        return viewController
    }
    
    private var containerPayment: UIViewController {
        let paymentService = GiftPaymentPciService(pci: P2PService(), payeeId: contact.wsId, paymentType: .newFriend)
        return ContainerPaymentReceiptFactory.make(
            paymentService: paymentService,
            actionsDelegate: NewFriendGiftPaymentAnalytics(),
            headerViewController: paymentHeader,
            accessoryViewController: paymentAccessory,
            toolbarViewController: paymentToolbar
        )
    }
    
    private var paymentHeader: HeaderPaymentContract {
        let modelPresenter = HeaderProfile(
            username: contact.username ?? "",
            image: contact.imgUrl,
            description: model.description,
            showFollowButton: true
        )
        let modelPayment = HeaderPayment(
            sellerId: nil,
            payeeId: String(contact.wsId),
            value: 0.0,
            installments: 1,
            payeeType: userType,
            paymentType: .newFriendGift
        )
        
        return PaymentProfileHeaderFactory.make(
            modelPresenter: modelPresenter,
            model: modelPayment,
            analytics: NewFriendHeaderPaymentAnalytics(),
            followingStatus: followingStatus
        )
    }
    
    private var paymentAccessory: AccessoryPaymentContract {
        var indexId = 0
        let gifts: [PaymentGift] = model.gifts.compactMap {
            guard let imageUrl = URL(string: $0.image) else {
                return nil
            }
            indexId += 1
            return PaymentGift(
                id: String(indexId),
                imageGift: .url(imageUrl),
                description: $0.message,
                value: $0.amount,
                isDefaultSelection: $0.default
            )
        }
        
        return PaymentGiftAccessoryFactory.make(model: gifts, analytics: NewFriendAccessoryPaymentAnalytics())
    }
    
    private var paymentToolbar: ToolbarPaymentContract {
        let presenterModel = PaymentToolbarLarge(title: model.payButtonTitle)
        return PaymentToolbarLargeFactory.make(presenterModel: presenterModel)
    }
    
    private var userType: HeaderPayment.PayeeType {
        if contact.businessAccount {
            return .pro
        } else if contact.isVerified {
            return .verify
        }
        return .none
    }
    
    init(contact: PPContact, followingStatus: FollowerStatus, model: NewFriendGiftPaymentModel, dependencies: Dependencies) {
        self.dependencies = dependencies
        self.contact = contact
        self.followingStatus = followingStatus
        self.model = model
    }
}
