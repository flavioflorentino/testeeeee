import Foundation

struct NewFriendGiftModel: Decodable {
    let amount: Double
    let message: String
    let image: String
    let `default`: Bool
}

struct NewFriendGiftPaymentModel: Decodable {
    let title: String
    let description: String
    let payButtonTitle: String
    let gifts: [NewFriendGiftModel]
}
