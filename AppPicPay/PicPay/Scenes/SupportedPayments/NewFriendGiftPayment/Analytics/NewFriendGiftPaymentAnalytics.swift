import AnalyticsModule
import CorePayment

final class NewFriendGiftPaymentAnalytics {
    var paymentMethod: PaymentMethod?
    var installment: Int?
    var extraParams: Decodable?
    
    typealias Dependencies = HasAnalytics
    private let analytics: AnalyticsProtocol
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.analytics = dependencies.analytics
    }
}

extension NewFriendGiftPaymentAnalytics: PaymentActionsDelegate {
    func viewWillAppear() {
        analytics.log(NewFriendGiftEvent.screenViewed)
    }
    
    func paymentSuccess(transactionId: String, value: Double) {
        analytics.log(NewFriendGiftEvent.didTryToSendGift(success: true))
    }
    
    func paymentFailure() {
        analytics.log(NewFriendGiftEvent.didTryToSendGift(success: false))
    }
    
    func paymentCancel() {
        analytics.log(NewFriendGiftEvent.didSelectOption(.close))
    }
    
    func didTapPay(privacyType: PaymentPrivacy, hasMessage: Bool, requestValue: String) {
        analytics.log(NewFriendGiftEvent.askedForPassword)
    }
    
    func didTapPayment(wentToCards: Bool, wentToInstallments: Bool) {
        analytics.log(NewFriendGiftEvent.didConfirmPassword)
    }
}
