import AnalyticsModule

enum NewFriendGiftEvent: AnalyticsKeyProtocol {
    case screenViewed
    case didSelectOption(OptionSelected)
    case didSelectGift(value: Double)
    case didTryToSendGift(success: Bool)
    case askedForPassword
    case didConfirmPassword
    
    private var name: String {
        switch self {
        case .screenViewed:
            return "New Friend Give Gift - Viewed"
        case .didSelectOption:
            return "New Friend Profile - Option Selected"
        case .didSelectGift:
            return "New Friend Give Gift Value - Option Selected"
        case .didTryToSendGift:
            return "New Friend Give Gift - Option Selected"
        case .askedForPassword, .didConfirmPassword:
            return "New Friend Give Gift Password - Viewed"
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.firebase, .mixPanel]
    }
    
    private var properties: [String: Any] {
        switch self {
        case .didSelectOption(let option):
            return ["option_selected": option.rawValue]
        case .didSelectGift(let value):
            return ["selected_value": String(value)]
        case .didTryToSendGift(let success):
            return ["give_gift": success ? "success" : "failure"]
        case .didConfirmPassword:
            return ["act-confirm-password": ""]
        default:
            return [:]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

extension NewFriendGiftEvent {
    enum OptionSelected: String {
        case openProfile = "PROFILE"
        case follow = "FOLLOW"
        case close = "CLOSE”"
    }
}
