import AnalyticsModule

final class NewFriendHeaderPaymentAnalytics {
    typealias Dependencies = HasAnalytics
    private let analytics: AnalyticsProtocol
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.analytics = dependencies.analytics
    }
}

extension NewFriendHeaderPaymentAnalytics: PaymentProfileHeaderAnalytics {
    func willOpenProfile() {
        analytics.log(NewFriendGiftEvent.didSelectOption(.openProfile))
    }
    
    func willFollow() {
        analytics.log(NewFriendGiftEvent.didSelectOption(.follow))
    }
}
