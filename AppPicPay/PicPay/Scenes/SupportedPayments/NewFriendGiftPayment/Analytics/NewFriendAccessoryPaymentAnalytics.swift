import AnalyticsModule

final class NewFriendAccessoryPaymentAnalytics {
    typealias Dependencies = HasAnalytics
    private let analytics: AnalyticsProtocol
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.analytics = dependencies.analytics
    }
}

extension NewFriendAccessoryPaymentAnalytics: PaymentGiftAcessoryAnalytics {
    func didSelectGift(_ gift: PaymentGift) {
        analytics.log(NewFriendGiftEvent.didSelectGift(value: gift.value))
    }
}
