import CorePayment
import FeatureFlag
import Foundation
import PCI

final class BirthdayPaymentOrchestrator {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    private let contact: PPContact
    
    var paymentViewController: UIViewController {
        let viewController = createContainerPayment()
        if #available(iOS 13.0, *) {
            viewController.isModalInPresentation = true
        }
        
        viewController.title = BirthdayLocalizable.title.text
        
        return viewController
    }
    
    init(contact: PPContact, dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
        self.contact = contact
    }
    
    private func createContainerPayment() -> UIViewController {
        let paymentService = GiftPaymentPciService(pci: P2PService(), payeeId: contact.wsId, paymentType: .birthday)
        
        let headerPaymentContract = createPaymentHeader()
        let accessoryPaymentContract = createPaymentAccessory()
        let toolbarViewController = createPaymentToolbar()
        
        return ContainerPaymentReceiptFactory.make(
            paymentService: paymentService,
            actionsDelegate: nil,
            headerViewController: headerPaymentContract,
            accessoryViewController: accessoryPaymentContract,
            toolbarViewController: toolbarViewController
        )
    }
    
    private func createPaymentAccessory() -> AccessoryPaymentContract {
        let iceCreamDescription = dependencies.featureManager.text(.firstGift)
        let beerDescription = dependencies.featureManager.text(.secondGift)
        let cakeDescription = dependencies.featureManager.text(.thirdGift)
        
        let iceCream = PaymentGift(
            id: "1",
            imageGift: .emoji("🍦"),
            description: iceCreamDescription,
            value: 2.0,
            isDefaultSelection: true
        )
        let beer = PaymentGift(
            id: "2",
            imageGift: .emoji("🍺"),
            description: beerDescription,
            value: 5.0,
            isDefaultSelection: false
        )
        let cake = PaymentGift(
            id: "3",
            imageGift: .emoji("🍰"),
            description: cakeDescription,
            value: 15.0,
            isDefaultSelection: false
        )
        
        return PaymentGiftAccessoryFactory.make(model: [iceCream, beer, cake])
    }
    
    private func createPaymentToolbar() -> ToolbarPaymentContract {
        let title = dependencies.featureManager.text(.buttonGift)
        let presenterModel = PaymentToolbarLarge(title: title)
        return PaymentToolbarLargeFactory.make(presenterModel: presenterModel)
    }
    
    private func createPaymentHeader() -> HeaderPaymentContract {
        let modelHeader = createModelPayment(contact: contact)
        let modelPresenter = createModelHeader(contact: contact)
        
        return PaymentProfileHeaderFactory.make(modelPresenter: modelPresenter, model: modelHeader)
    }
    
    private func createModelHeader(contact: PPContact) -> HeaderProfile {
        HeaderProfile(
            username: contact.username ?? "",
            image: contact.imgUrl,
            description: BirthdayLocalizable.chooseGift.text,
            showFollowButton: false
        )
    }
    
    private func createModelPayment(contact: PPContact) -> HeaderPayment {
        let type = typeUser(contact: contact)
        return HeaderPayment(
            sellerId: nil,
            payeeId: String(contact.wsId),
            value: 0.0,
            installments: 1,
            payeeType: type,
            paymentType: .birthday
        )
    }
    
    private func typeUser(contact: PPContact) -> HeaderPayment.PayeeType {
        if contact.businessAccount {
            return .pro
        } else if contact.isVerified {
            return .verify
        }
        return .none
    }
}
