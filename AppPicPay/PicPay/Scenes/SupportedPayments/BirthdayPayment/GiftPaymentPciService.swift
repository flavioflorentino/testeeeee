import Core
import CorePayment
import FeatureFlag
import Foundation
import PCI

enum GiftPaymentType: String {
    case birthday = "Aniversário"
    case newFriend = "Novo amigo"
}

final class GiftPaymentPciService: ServiceReceiptProtocol {
    typealias Dependencies = HasMainQueue & HasFeatureManager
    private let dependencies: Dependencies
    
    private let pciService: P2PServiceProtocol
    private let payeeId: Int
    private let paymentType: GiftPaymentType
    
    init(pci: P2PServiceProtocol, payeeId: Int, dependencies: Dependencies = DependencyContainer(), paymentType: GiftPaymentType) {
        self.pciService = pci
        self.payeeId = payeeId
        self.dependencies = dependencies
        self.paymentType = paymentType
    }
    
    func createTransaction(payment: Payment, completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        let p2PPayload = createP2PPayload(payment: payment)
        let isFeedzai = dependencies.featureManager.isActive(.transaction_p2p_v2_feedzai)
        let cvv = createCVVPayload(cvv: payment.cvv)
        let payload = PaymentPayload<P2PPayload>(cvv: cvv, generic: p2PPayload)
        
        pciService.createTransaction(
            password: payment.password,
            isFeedzai: isFeedzai,
            payload: payload,
            isNewArchitecture: true
        ) { [weak self] result in
            guard let self = self else {
                return
            }
            
            self.dependencies.mainQueue.async {
                switch result {
                case .success(let value):
                    let receiptViewModel = ReceiptWidgetViewModel(transactionId: value.p2pId, type: .P2P)
                    let widgets = value.receiptWidgets.compactMap { WSReceipt.createReceiptWidgetItem(jsonDict: $0) }
                    receiptViewModel.setReceiptWidgets(items: widgets)
                    NotificationCenter.default.post(
                        name: Notification.Name.Payment.new,
                        object: nil,
                        userInfo: ["type": self.paymentType.rawValue]
                    )
                    
                    completion(.success(receiptViewModel))
                    
                case .failure(let error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }
    
    private func createCVVPayload(cvv: String?) -> CVVPayload? {
        CVVPayload(value: cvv)
    }
    
    private func createP2PPayload(payment: Payment) -> P2PPayload {
        P2PPayload(
            password: payment.password,
            biometry: payment.biometry,
            payeeId: String(payeeId),
            value: payment.value,
            credit: payment.balanceValue,
            installments: payment.installment,
            cardId: payment.cardId,
            someErrorOccurred: payment.someErrorOccurred,
            privacyConfig: payment.privacyConfig.stringValue,
            message: payment.message,
            surcharge: payment.surcharge
        )
    }
}
