import Foundation

enum ContactsListLocalizable: String, Localizable {
    case contactsListTitle
    case contactsListSearchBarPlaceholder
    
    case contactsListErrorTitle
    case contactsListErrorDescription
    case contactsListErrorButtonTitle
    
    case contactsListRequestAuthTitle
    case contactsListRequestAuthDescription
    case contactsListRequestAuthButtonTitle
    
    case contactsNumberSelectionDescription
    case contactsNumberSelectionCancelButtonTitle
    
    var key: String {
        rawValue
    }
    
    var file: LocalizableFile {
        .contactsList
    }
}
