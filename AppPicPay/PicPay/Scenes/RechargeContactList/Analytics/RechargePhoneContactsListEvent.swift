import AnalyticsModule

enum RechargePhoneContactsListEvent: AnalyticsKeyProtocol {
    case contactsAccessed
    case contactsListSearchAccessed
    case contactSelected
    case contactMultipleCellphonesSelected
    
    private var name: String {
        switch self {
        case .contactsAccessed:
            return "Contacts Accessed"
        case .contactsListSearchAccessed:
            return "Contacts List Search Accessed"
        case .contactSelected:
            return "Contact Selected"
        case .contactMultipleCellphonesSelected:
            return "Contact Multiple Cellphones Selected"
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, providers: [.mixPanel, .appsFlyer, .firebase])
    }
}
