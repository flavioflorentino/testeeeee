import AnalyticsModule
import Foundation

enum ContactsEndStateViewType {
    case error
    case auth
    case none
}

protocol ContactsListViewModelInputs: AnyObject {
    var endStateView: ContactsEndStateViewType { get set }
    func getUserContacts()
    func filterContactsList(for text: String)
    func goBack(index: Int)
    func requestContactsAccess()
}

public final class ContactsListViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let service: ContactsListServicing
    private let presenter: ContactsListPresenting
    
    private var userContacts: [UserAgendaContact]
    private var filteredUserContacts: [UserAgendaContact]
    
    var endStateView: ContactsEndStateViewType

    init(service: ContactsListServicing, presenter: ContactsListPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.userContacts = []
        self.filteredUserContacts = []
        self.endStateView = .none
    }
}

extension ContactsListViewModel: ContactsListViewModelInputs {
    func getUserContacts() {
        service.checkUserGrantContactsPermission { [weak self] userAuthorized in
            guard userAuthorized else {
                self?.presenter.presentRequestAuthorizationView()
                self?.endStateView = .auth
                return
            }
            
            self?.service.getContacts { [weak self] result in
                switch result {
                case .success(let userAgendaContacts):
                    self?.userContacts = userAgendaContacts
                    self?.presenter.updateContactsList(with: self?.userContacts)
                    self?.endStateView = .none
                case .failure:
                    self?.presenter.presentErrorView()
                    self?.endStateView = .error
                }
            }
        }
    }
    
    func goBack(index: Int) {
        if !filteredUserContacts.isEmpty {
            presenter.goBack(withUserContact: filteredUserContacts[index])
        } else {
            presenter.goBack(withUserContact: userContacts[index])
        }
    }
    
    func requestContactsAccess() {
        dependencies.analytics.log(ContactListPermissionEvent.contactsPermission(.authorize, .recharge))
        service.requestPermissionToAccessContacts { [weak self] status in
            if status {
                self?.presenter.retryLoadingContacts()
            }
        }
    }
    
    func filterContactsList(for text: String) {
        filteredUserContacts = userContacts.filter { $0.fullName.lowercased().contains(text.lowercased()) }
        if filteredUserContacts.isEmpty && text.isEmpty {
            self.presenter.updateContactsList(with: userContacts)
        } else {
            self.presenter.updateContactsList(with: filteredUserContacts)
        }
    }
}
