import AnalyticsModule
import UI
import SkeletonView

// MARK: Layout Extension
private extension ContactsListViewController.Layout {
    enum Size {
        static let size45 = CGFloat(45)
    }
}

final class ContactsListViewController: ViewController<ContactsListViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
    private lazy var searchBar: SearchNavigationBar = {
        let searchBar = SearchNavigationBar()
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.backgroundColor = Palette.ppColorGrayscale000.color(withCustomDark: Palette.Dark.ppColorGrayscale600)
        searchBar.setPlaceholder(placeholder: ContactsListLocalizable.contactsListSearchBarPlaceholder.text)
        searchBar.searchTextChanged = { text in
            self.filterTableView(with: text)
        }
        searchBar.cancelButtonTapped = {
            self.tableView.reloadData()
        }
        searchBar.searchTextDidBeginEditing = { _ in
            Analytics.shared.log(RechargePhoneContactsListEvent.contactsListSearchAccessed)
        }
        searchBar.shouldChageNormalStateWhenEndEdit = true
        return searchBar
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = Palette.ppColorGrayscale000.color
        tableView.register(ContactsListCell.self, forCellReuseIdentifier: String(describing: ContactsListCell.self))
        return tableView
    }()
    
    private var isFiltering: Bool = false
    private var dataSource: TableViewHandler<String, ContactsListCellViewModel, ContactsListCell>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(searchBar)
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        if #available(iOS 11.0, *) {
            searchBar.layout {
                $0.top == view.safeAreaLayoutGuide.topAnchor
                $0.leading == view.leadingAnchor
                $0.trailing == view.trailingAnchor
                $0.bottom == tableView.topAnchor
                $0.height == Layout.Size.size45
            }
            
            tableView.layout {
                $0.top == searchBar.bottomAnchor
                $0.leading == view.leadingAnchor
                $0.trailing == view.trailingAnchor
                $0.bottom == view.bottomAnchor
            }
        }
    }
    
    override func configureViews() {
        title = ContactsListLocalizable.contactsListTitle.text
        navigationController?.navigationBar.backgroundColor = .white
        updateNavigationBarAppearance()
    }
    
    private func filterTableView(with text: String) {
        viewModel.filterContactsList(for: text)
    }
    
    private func loadData() {
        beginState()
        viewModel.getUserContacts()
    }
}

extension ContactsListViewController: ContactsListDisplay {
    func updateContactsList(with contacts: [Section<String, ContactsListCellViewModel>]) {
        dataSource = TableViewHandler(
            data: contacts,
            cellType: ContactsListCell.self,
            configureCell: { _, viewModel, cell in
                viewModel.presenter.viewController = cell
                cell.viewModel = viewModel
            },
            configureDidSelectRow: { [weak self] indexPath, _ in
                Analytics.shared.log(RechargePhoneContactsListEvent.contactSelected)
                self?.viewModel.goBack(index: indexPath.row)
            }
        )
        
        tableView.delegate = dataSource
        tableView.dataSource = dataSource
        tableView.reloadData()
        
        endState()
    }
    
    func presentErrorView() {
        endState(model: StatefulErrorViewModel(
            image: Assets.CreditPicPay.Registration.sad3.image,
            content: (
                title: ContactsListLocalizable.contactsListErrorTitle.text,
                description: DefaultLocalizable.requestError.text
            ),
            button: (image: nil, title: ContactsListLocalizable.contactsListErrorButtonTitle.text))
        )
    }
    
    func presentRequestAuthorizationView() {
        endState(model: StatefulErrorViewModel(
            image: Assets.SocialOnboarding.onboardingSocialPopup.image,
            content: (
                title: ContactsListLocalizable.contactsListRequestAuthTitle.text,
                description: ContactsListLocalizable.contactsListRequestAuthDescription.text
            ),
            button: (image: nil, title: ContactsListLocalizable.contactsListRequestAuthButtonTitle.text))
        )
    }
    
    func retryLoadingContacts() {
        loadData()
    }
}

extension ContactsListViewController {
    func updateNavigationBarAppearance() {
        navigationController?.navigationBar.tintColor = Palette.ppColorBranding300.color
        navigationController?.navigationBar.backgroundColor = Palette.ppColorGrayscale000.color(
            withCustomDark: Palette.Dark.ppColorGrayscale600
        )

        if #available(iOS 11, *) {
            extendedLayoutIncludesOpaqueBars = true
            navigationItem.largeTitleDisplayMode = .always
            navigationController?.navigationBar.prefersLargeTitles = true
        }
    }
}

extension ContactsListViewController: StatefulTransitionViewing {
    public func didTryAgain() {
        switch viewModel.endStateView {
        case .error:
            loadData()
        case .auth:
            viewModel.requestContactsAccess()
        case .none:
            break
        }
    }
    
    public func statefulViewForLoading() -> StatefulViewing {
        FavoriteSkeletonView()
    }
}
