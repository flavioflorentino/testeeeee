import Core
import Contacts
import Foundation
import PermissionsKit

public enum ContactsError: Error {
    case failedToFetchContacts
    case failedToParseWithKeys
}

public protocol ContactsListServicing {
    func getContacts(completion: @escaping (Result<[UserAgendaContact], ContactsError>) -> Void)
    func checkUserGrantContactsPermission(completion: @escaping (_ status: Bool) -> Void)
    func requestPermissionToAccessContacts(completion: @escaping (_ status: Bool) -> Void)
}

final class ContactsListService: ContactsListServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    private func createFetchRequestForContacts() -> CNContactFetchRequest? {
        guard
            let keys = [
                CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                CNContactPhoneNumbersKey,
                CNContactImageDataKey
            ] as? [CNKeyDescriptor] else {
                return nil
        }
        let fetchRequest = CNContactFetchRequest(keysToFetch: keys)
        fetchRequest.sortOrder = .userDefault
        return fetchRequest
    }
    
    func getContacts(completion: @escaping (Result<[UserAgendaContact], ContactsError>) -> Void) {
        DispatchQueue.global().async { [weak self] in
            var contacts = [UserAgendaContact]()
            guard let request = self?.createFetchRequestForContacts() else {
                completion(.failure(.failedToParseWithKeys))
                return
            }
            do {
                try CNContactStore().enumerateContacts(with: request, usingBlock: { contact, _ in
                    if !contact.phoneNumbers.isEmpty && !contact.givenName.isEmpty {
                        let userAgendaContact = UserAgendaContact(contact: contact)
                        contacts.append(userAgendaContact)
                    }
                })
            } catch {
                self?.dependencies.mainQueue.asyncAfter(deadline: .now() + 1) {
                    completion(.failure(.failedToFetchContacts))
                }
            }
            self?.dependencies.mainQueue.asyncAfter(deadline: .now() + 1) {
                completion(.success(contacts))
            }
        }
    }
    
    func checkUserGrantContactsPermission(completion: @escaping (_ status: Bool) -> Void) {
        dependencies.mainQueue.asyncAfter(deadline: .now() + 1) {
            switch Permission.contacts.status {
            case .authorized:
                completion(true)
            default:
                completion(false)
            }
        }
    }
    
    func requestPermissionToAccessContacts(completion: @escaping (Bool) -> Void) {
        Permission.contacts.request { status in
            switch status {
            case .authorized:
                completion(true)
            default:
                completion(false)
            }
        }
    }
}
