import UI

public protocol ContactsListDisplay: AnyObject {
    func updateContactsList(with contacts: [Section<String, ContactsListCellViewModel>])
    func presentErrorView()
    func presentRequestAuthorizationView()
    func retryLoadingContacts()
}
