import Contacts 
import Foundation

public final class UserAgendaContact: Decodable {
    let id: String
    let givenName: String
    let familyName: String
    let image: String
    let phoneNumbers: [String]
    
    var fullName: String {
        "\(givenName) \(familyName)"
    }
    
    init(contact: CNContact) {
        self.id = contact.identifier
        self.givenName = contact.givenName
        self.familyName = contact.familyName
        self.image = contact.imageData?.base64EncodedString() ?? ""
        self.phoneNumbers = contact.phoneNumbers.map { $0.value.stringValue }
    }
}
