import Foundation

final class ContactsPhoneNumber {
    let areaCode: String
    let number: String
    
    init(_ phoneNumber: String) {
        let phoneNumberComposition = phoneNumber.getPhoneNumberPrefixAndRawValue()
        self.areaCode = phoneNumberComposition.0
        self.number = phoneNumberComposition.1
    }
}
