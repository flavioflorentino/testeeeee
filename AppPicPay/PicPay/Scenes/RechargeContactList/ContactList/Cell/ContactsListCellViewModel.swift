public protocol ContactsListCellViewModelInputs: AnyObject {
    func configureView()
}

public final class ContactsListCellViewModel: ContactsListCellViewModelInputs {
    public private(set) var presenter: ContactsListCellPresenting
    
    private let contact: UserAgendaContact
    
    init(presenter: ContactsListCellPresenting, contact: UserAgendaContact) {
        self.presenter = presenter
        self.contact = contact
    }
    
    public func configureView() {
        presenter.configureView(contact)
    }
}
