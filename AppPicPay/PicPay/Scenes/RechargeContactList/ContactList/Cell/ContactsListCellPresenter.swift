import Core

public protocol ContactsListCellPresenting: AnyObject {
    var viewController: ContactsListCellDisplay? { get set }
    func configureView(_ contact: UserAgendaContact)
}

public final class ContactsListCellPresenter: ContactsListCellPresenting {
    public weak var viewController: ContactsListCellDisplay?
    
    public func configureView(_ contact: UserAgendaContact) {
        let name = contact.fullName
        // create a formatter function for phone number
        let number = contact.phoneNumbers.first ?? ""
        guard let data = Data(base64Encoded: contact.image), !contact.image.isEmpty else {
            viewController?.configureView(with: Assets.NewGeneration.avatarPerson.image, name, number)
            return
        }
        viewController?.configureView(with: UIImage(data: data), name, number)
    }
}
