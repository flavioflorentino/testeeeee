public protocol ContactsListCellDisplay: AnyObject {
    func configureView(with image: UIImage?, _ contactName: String, _ contactNumber: String)
}
