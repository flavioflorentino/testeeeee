import UI

// MARK: Layout Extension
private extension ContactsListCell.Layout {
    enum Margin {
        static let margin04 = CGFloat(4)
        static let margin08 = CGFloat(8)
        static let margin16 = CGFloat(16)
        static let margin24 = CGFloat(24)
        static let margin64 = CGFloat(64)
    }
}

final class ContactsListCell: UITableViewCell {
    fileprivate enum Layout { }
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isSkeletonable = true
        return view
    }()
    
    private lazy var profileImageView: UICircularImageView = {
        let imageView = UICircularImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isSkeletonable = true
        return imageView
    }()
    
    private lazy var stackView: UIStackView = {
        let view = UIStackView()
        view.alignment = .leading
        view.axis = .vertical
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var contactNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.isSkeletonable = true
        return label
    }()
    
    private lazy var phoneNumberLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.isSkeletonable = true
        return label
    }()
    
    var viewModel: ContactsListCellViewModel? {
        didSet {
            update()
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        contactNameLabel.text = ""
        phoneNumberLabel.text = ""
        profileImageView.image = nil
    }
    
    private func update() {
        viewModel?.configureView()
    }
}

extension ContactsListCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(containerView)
        containerView.addSubview(profileImageView)
        containerView.addSubview(stackView)
        stackView.addArrangedSubviews(contactNameLabel, phoneNumberLabel)
    }
    
    func setupConstraints() {
        containerView.layout {
            $0.top == contentView.topAnchor + Layout.Margin.margin08
            $0.leading == contentView.leadingAnchor + Layout.Margin.margin24
            $0.trailing == contentView.trailingAnchor - Layout.Margin.margin24
            $0.bottom == contentView.bottomAnchor - Layout.Margin.margin08
        }
        
        profileImageView.layout {
            $0.top == containerView.topAnchor
            $0.leading == containerView.leadingAnchor
            $0.bottom == containerView.bottomAnchor
            $0.width == Layout.Margin.margin64
            $0.height == Layout.Margin.margin64
        }
        
        stackView.layout {
            $0.centerY == profileImageView.centerYAnchor
            $0.leading == profileImageView.trailingAnchor + Layout.Margin.margin04
            $0.trailing == containerView.trailingAnchor
        }
    }
    
    private func setupView() {
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
}

extension ContactsListCell: ContactsListCellDisplay {
    func configureView(with image: UIImage?, _ contactName: String, _ contactNumber: String) {
        profileImageView.image = image
        contactNameLabel.text = contactName
        phoneNumberLabel.text = contactNumber
    }
}
