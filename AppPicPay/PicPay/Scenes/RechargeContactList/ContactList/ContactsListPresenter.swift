import Core
import UI

public protocol ContactsListPresenting: AnyObject {
    var viewController: ContactsListDisplay? { get set }
    func updateContactsList(with contacts: [UserAgendaContact]?)
    func presentErrorView()
    func presentRequestAuthorizationView()
    func goBack(withUserContact contact: UserAgendaContact)
    func dismiss()
    func retryLoadingContacts()
}

final class ContactsListPresenter: ContactsListPresenting {
    private let coordinator: ContactsListCoordinating
    private let queue: DispatchQueue
    
    weak var viewController: ContactsListDisplay?

    init(coordinator: ContactsListCoordinating, queue: DispatchQueue = .main) {
        self.coordinator = coordinator
        self.queue = queue
    }
    
    func goBack(withUserContact contact: UserAgendaContact) {
        coordinator.perform(action: .select(contact))
    }
    
    func updateContactsList(with contacts: [UserAgendaContact]?) {
        guard let contacts = contacts else {
            return
        }
        
        queue.async { [weak self] in
            let items = contacts.map { contact -> ContactsListCellViewModel in
                let presenter: ContactsListCellPresenting = ContactsListCellPresenter()
                return ContactsListCellViewModel(presenter: presenter, contact: contact)
            }
            self?.viewController?.updateContactsList(with: [Section(items: items)])
        }
    }
    
    func dismiss() {
        coordinator.perform(action: .dismiss)
    }
    
    func presentErrorView() {
        viewController?.presentErrorView()
    }
    
    func presentRequestAuthorizationView() {
        viewController?.presentRequestAuthorizationView()
    }
    
    func retryLoadingContacts() {
        viewController?.retryLoadingContacts()
    }
}
