import UIKit

public enum ContactsListAction {
    case select(_ contact: UserAgendaContact)
    case dismiss
}

public protocol ContactsListSelecting: AnyObject {
    func presentContactPhones(contact: UserAgendaContact)
}

public protocol ContactsListCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: ContactsListSelecting? { get set }
    func perform(action: ContactsListAction)
}

final class ContactsListCoordinator: ContactsListCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: ContactsListSelecting?
    
    func perform(action: ContactsListAction) {
        switch action {
        case .select(let contact):
            viewController?.navigationController?.popViewController(animated: true)
            delegate?.presentContactPhones(contact: contact)    
        case .dismiss:
            viewController?.navigationController?.popViewController(animated: true)
        }
    }
}
