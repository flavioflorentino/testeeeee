import Foundation

enum ContactsListFactory {
    static func make(delegate: ContactsListSelecting) -> ContactsListViewController {
        let container = DependencyContainer()
        let service: ContactsListServicing = ContactsListService(dependencies: container)
        let coordinator: ContactsListCoordinating = ContactsListCoordinator()
        let presenter: ContactsListPresenting = ContactsListPresenter(coordinator: coordinator)
        let viewModel = ContactsListViewModel(service: service, presenter: presenter, dependencies: container)
        let viewController = ContactsListViewController(viewModel: viewModel)

        coordinator.delegate = delegate
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
