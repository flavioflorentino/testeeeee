enum PayPeopleLocalizable: String {
    case payPeople
}

// MARK: - Localizable
extension PayPeopleLocalizable: Localizable {
    var key: String { rawValue }
    var file: LocalizableFile { .payPeople }
}
