import UIKit

final class PayPeopleDeeplinkHelper {
    typealias Dependencies = HasAppManager

    // MARK: - Properties
    private let dependencies: Dependencies

    // MARK: - Initialization
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - DeeplinkHelperProtocol
extension PayPeopleDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        guard let mainScreenCoordinator = dependencies.appManager.mainScreenCoordinator else { return false }

        switch deeplink.type {
        case .payPeople:
            mainScreenCoordinator.handle(deeplink: deeplink)
            return true
        default:
            return false
        }
    }
}
