import FeatureFlag

struct PayPeopleApplicationEventProperty {
    typealias Dependency = HasFeatureManager

    // MARK: - Properties
    private let dependency: Dependency

    init(dependency: Dependency) {
        self.dependency = dependency
    }
}

extension PayPeopleApplicationEventProperty: ApplicationEventProperty {
    var properties: [String: Any] {
        ["is_access_pay_person": dependency.featureManager.isActive(.experimentPayPersonBool)]
    }
}
