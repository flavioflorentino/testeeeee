import Search
import UIKit

protocol SearchCoordinatorFactory {
    func makeSearchCoordinator(
        title: String,
        searchType: Search.SearchType,
        shouldShowContacts: Bool,
        navigationController: UINavigationController,
        analyticsData: SearchAnalyticsData?
    ) -> SearchCoordinating
}
