import UIKit

protocol PayPeopleCoordinatorFactory {
    func makePayPeopleCoordinator(title: String?, navigationController: UINavigationController) -> PayPeopleCoordinating
}
