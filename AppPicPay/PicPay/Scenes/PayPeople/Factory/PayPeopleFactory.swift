import Search
import UIKit

final class PayPeopleFactory { }

// MARK: - PayPeopleCoordinatorFactory
extension PayPeopleFactory: PayPeopleCoordinatorFactory {
    func makePayPeopleCoordinator(title: String?, navigationController: UINavigationController) -> PayPeopleCoordinating {
        PayPeopleCoordinator(title: title, navigationController: navigationController, factory: self)
    }
}

// MARK: - SearchCoordinatorFactory
extension PayPeopleFactory: SearchCoordinatorFactory {
    func makeSearchCoordinator(
        title: String,
        searchType: Search.SearchType,
        shouldShowContacts: Bool,
        navigationController: UINavigationController,
        analyticsData: SearchAnalyticsData?
    ) -> SearchCoordinating {
        SearchFactory(dependencies: DependencyContainer()).makeSearchCoordinator(
            title: title,
            searchType: searchType,
            shouldShowContacts: shouldShowContacts,
            navigationController: navigationController,
            analyticsData: analyticsData
        )
    }
}
