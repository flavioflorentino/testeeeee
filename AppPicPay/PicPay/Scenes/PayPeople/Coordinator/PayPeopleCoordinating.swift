import UI

protocol PayPeopleCoordinating: Coordinating {
    var didFinishFlow: (() -> Void)? { get set }
}
