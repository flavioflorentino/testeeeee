import Search
import UI
import UIKit

final class PayPeopleCoordinator {
    // MARK: - Properties
    private let title: String
    private let navigationController: UINavigationController
    private let factory: SearchCoordinatorFactory

    private var children: [Coordinating] = []

    var childViewController: [UIViewController] = []
    var viewController: UIViewController?

    var didFinishFlow: (() -> Void)?

    // MARK: - Initialization
    init(title: String?, navigationController: UINavigationController, factory: SearchCoordinatorFactory) {
        self.title = title ?? PayPeopleLocalizable.payPeople.text
        self.navigationController = navigationController
        self.factory = factory
    }
}

// MARK: - PayPeopleCoordinating
extension PayPeopleCoordinator: PayPeopleCoordinating {
    func start() {
        let analyticsData = SearchAnalyticsData(
            cardContactsPermissionOrigin: "Pagar pessoas",
            paymentItemAccessedSection: "Pagar pessoas",
            searchAccessedSection: "Pagar pessoas",
            searchResultViewedSection: "Pagar pessoas"
        )

        let searchCoordinator = factory.makeSearchCoordinator(
            title: title,
            searchType: .consumers,
            shouldShowContacts: true,
            navigationController: navigationController,
            analyticsData: analyticsData
        )

        searchCoordinator.didFinishFlow = { [weak self] selection in
            self?.didSelect(item: selection)
        }

        children.append(searchCoordinator)

        searchCoordinator.start()
    }
}

// MARK: - Private
private extension PayPeopleCoordinator {
    // MARK: - Show methods
    func didSelect(item: SearchSelection?) {
        guard let item = item else {
            didFinishFlow?()
            return
        }

        guard case let .single(result) = item, let controller = NewTransactionViewController.fromStoryboard() else {
            return
        }

        switch result {
        case let .consumer(data), let .contact(data):
            controller.showCancel = true
            controller.loadConsumerInfo(data.id)
            controller.touchOrigin = "Pagar pessoas"

            navigationController.present(UINavigationController(rootViewController: controller), animated: true)
        default:
            return
        }
    }
}
