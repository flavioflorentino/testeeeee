import Core

enum PaymentRequestFeedDetailEndpoint {
    case paymentRequestStatus(transactionId: String)
    case paymentRequestFeedTransaction(transactionId: String)
}

// MARK: - ApiEndpointExposable
extension PaymentRequestFeedDetailEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case let .paymentRequestStatus(transactionId):
            return "p2p-charge/transactions/\(transactionId)/feed"
        case let .paymentRequestFeedTransaction(transactionId):
            return "api/rendered/feeds/transaction/\(transactionId)/P2pTransaction"
        }
    }
}
