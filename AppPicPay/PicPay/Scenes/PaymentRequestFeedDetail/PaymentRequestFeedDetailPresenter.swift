import UI

protocol PaymentRequestFeedDetailPresenting: AnyObject {
    var viewController: PaymentRequestFeedDetailDisplay? { get set }
    func didNextStep(action: PaymentRequestFeedDetailAction)
    func presentFeedInfo(feedItem: DefaultFeedItem)
    func presentTransactions(_ transactions: [PaymentRequestTransaction])
    func presentLoader()
    func presentGenericError()
}

final class PaymentRequestFeedDetailPresenter {
    // MARK: - Variables
    private let coordinator: PaymentRequestFeedDetailCoordinating
    weak var viewController: PaymentRequestFeedDetailDisplay?

    // MARK: - Life Cycle
    init(coordinator: PaymentRequestFeedDetailCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - PaymentRequestFeedDetailPresenting
extension PaymentRequestFeedDetailPresenter: PaymentRequestFeedDetailPresenting {
    // MARK: - didNextStep
    func didNextStep(action: PaymentRequestFeedDetailAction) {
        coordinator.perform(action: action)
    }
    
    // MARK: - presentFeedInfo
    func presentFeedInfo(feedItem: DefaultFeedItem) {
        let imageUrl = URL(string: feedItem.image?.url ?? "")
        let privacyImage = feedItem.timeAgoIcon?.icon == "private" ? Assets.Icons.icoPrivate.image : Assets.Icons.icoPublic.image
        let title = createAttributedTitle(title: feedItem.title)
        let message = createMessageAttributed(text: feedItem.text?.value ?? "")
        let value = createValueAttributed(text: feedItem.money?.value ?? "")
        let date = createDateAttributed(text: feedItem.timeAgo?.value ?? "")
        viewController?.displayFeedImage(url: imageUrl, privacyImage: privacyImage)
        viewController?.displayFeedText(title: title, message: message, value: value, date: date)
    }
    
    private func createAttributedTitle(title: TextLinkedFeedWidget) -> NSAttributedString {
        let textColor = Colors.grayscale700.color
        let font = Typography.bodyPrimary().font()
        let attributedTitle = StringHtmlTextHelper.htmlToAttributedString(string: title.value, font: font, foregroundColor: textColor)
        return BaseFeedCellNode.convertUsernameLinks(title, attributeString: attributedTitle, detail: true)
    }
    
    private func createMessageAttributed(text: String) -> NSAttributedString {
        let isEmoji = text.count <= 3 && text.containsOnlyEmoji
        let textColor = Colors.grayscale700.color
        let font = isEmoji ? Typography.title(.xLarge).font() : Typography.bodySecondary().font()
        return StringHtmlTextHelper.htmlToAttributedString(string: text, font: font, foregroundColor: textColor)
    }
    
    private func createValueAttributed(text: String) -> NSAttributedString {
        let font = Typography.linkSecondary().font()
        return StringHtmlTextHelper.htmlToAttributedString(string: text, font: font)
    }
    
    private func createDateAttributed(text: String) -> NSAttributedString {
        let textColor = Colors.grayscale300.color
        let font = Typography.caption().font()
        return StringHtmlTextHelper.htmlToAttributedString(string: text, font: font, foregroundColor: textColor)
    }
    
    // MARK: - presentTransactions
    func presentTransactions(_ transactions: [PaymentRequestTransaction]) {
        let presentableTransactions = transactions.map(transformToPresentableInfo)
        let sections = [Section<String, PaymentRequestFeedDetailViewController.CellType>(items: presentableTransactions)]
        viewController?.displayTableViewContent(sections)
    }
    
    private func transformToPresentableInfo(_ transaction: PaymentRequestTransaction) -> PaymentRequestFeedDetailViewController.CellType {
        let username = "@\(transaction.consumerPayerUsername)"
        let imageUrl = URL(string: transaction.consumerPayerSmallImageUrl)
        let statusText = transaction.p2pTransactionId == nil ? PaymentRequestLocalizable.pendingPayment.text : PaymentRequestLocalizable.paymentMade.text
        let statusTextColor = transaction.p2pTransactionId == nil ? Colors.grayscale400.color : Colors.branding600.color
        let isArrowHidden = transaction.p2pTransactionId == nil
        let transaction = PaymentRequestPresentableTransaction(
            username: username,
            imageUrl: imageUrl,
            statusText: statusText,
            statusColor: statusTextColor,
            isArrowHidden: isArrowHidden
        )
        return .default(transaction)
    }
    
    // MARK: - presentLoader
    func presentLoader() {
        let sections = [Section<String, PaymentRequestFeedDetailViewController.CellType>(items: [.loading])]
        viewController?.displayTableViewContent(sections)
    }
    
    // MARK: - presentGenericError
    func presentGenericError() {
        let text = PaymentRequestLocalizable.weCouldntVerifyPayments.text
        let buttonText = DefaultLocalizable.tryAgain.text
        let error: PaymentRequestFeedDetailViewController.CellType = .error(text, buttonText)
        let sections = [Section<String, PaymentRequestFeedDetailViewController.CellType>(items: [error])]
        viewController?.displayTableViewContent(sections)
    }
}
