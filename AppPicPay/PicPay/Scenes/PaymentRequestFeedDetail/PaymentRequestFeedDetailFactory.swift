import Foundation

enum PaymentRequestFeedDetailFactory {
    static func make(feedItem: DefaultFeedItem) -> PaymentRequestFeedDetailViewController {
        let container = DependencyContainer()
        let service: PaymentRequestFeedDetailServicing = PaymentRequestFeedDetailService(dependencies: container)
        let coordinator: PaymentRequestFeedDetailCoordinating = PaymentRequestFeedDetailCoordinator()
        let presenter: PaymentRequestFeedDetailPresenting = PaymentRequestFeedDetailPresenter(coordinator: coordinator)
        let viewModel = PaymentRequestFeedDetailViewModel(service: service, presenter: presenter, feedItem: feedItem)
        let viewController = PaymentRequestFeedDetailViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
