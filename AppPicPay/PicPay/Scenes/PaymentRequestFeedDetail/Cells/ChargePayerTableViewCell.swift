import UI

private extension ChargePayerTableViewCell.Layout {
    enum Size {
        static let userImageView = CGSize(width: 48, height: 48)
        static let arrowImageView = CGSize(width: 16, height: 16)
    }
}

final class ChargePayerTableViewCell: UITableViewCell {
    fileprivate enum Layout {}
    
    // MARK: - Visual Components
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var textStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        return stackView
    }()
    
    private lazy var arrowImageView = UIImageView(image: Assets.LinkedAccounts.rightArrowlightGray.image)
    
    private lazy var userImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = Layout.Size.userImageView.height / 2
        return imageView
    }()
    
    private lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var paymentStatusLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
        return label
    }()
    
    // MARK: - Life Cycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        userImageView.cancelRequest()
    }
    
    // MARK: - Content
    func set(imageUrl url: URL?, username: String, paymentStatus: String, paymentStatusColor: UIColor, isArrowHidden: Bool) {
        userImageView.setImage(url: url, placeholder: PPContact.photoPlaceholder())
        usernameLabel.text = username
        paymentStatusLabel.text = paymentStatus
        paymentStatusLabel.textColor = paymentStatusColor
        arrowImageView.isHidden = isArrowHidden
    }
}

// MARK: - ViewConfiguration
extension ChargePayerTableViewCell: ViewConfiguration {
    func setupConstraints() {
        userImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.userImageView)
        }
        
        arrowImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.arrowImageView)
        }
        
        contentStackView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().inset(Spacing.base03)
        }
    }
    
    func configureViews() {
        selectionStyle = .none
    }
    
    func buildViewHierarchy() {
        addSubview(contentStackView)
        contentStackView.addArrangedSubview(userImageView)
        contentStackView.addArrangedSubview(textStackView)
        contentStackView.addArrangedSubview(arrowImageView)
        textStackView.addArrangedSubview(usernameLabel)
        textStackView.addArrangedSubview(paymentStatusLabel)
    }
}
