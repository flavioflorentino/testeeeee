import UI

protocol PaymentRequestFeedDetailDisplay: AnyObject {
    func displayFeedImage(url: URL?, privacyImage: UIImage)
    func displayFeedText(title: NSAttributedString, message: NSAttributedString, value: NSAttributedString, date: NSAttributedString)
    func displayTableViewContent(_ sections: [Section<String, PaymentRequestFeedDetailViewController.CellType>])
}

private extension PaymentRequestFeedDetailViewController.Layout {
    enum Size {
        static let privacyImageView = CGSize(width: 24, height: 24)
        static let lineHeight: CGFloat = 1
        static let titleHeight: CGFloat = 55
    }
}

final class PaymentRequestFeedDetailViewController: ViewController<PaymentRequestFeedDetailViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    enum CellType {
        case `default`(PaymentRequestPresentableTransaction)
        case error(String, String)
        case loading
    }
    
    // MARK: - Visual Components
    private lazy var headerView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.backgroundPrimary.color
        return view
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale050.color
        return view
    }()
    
    private lazy var feedStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var feedHeaderStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var feedFooterStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base03
        return stackView
    }()
    
    private lazy var feedInfoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.imageStyle(AvatarImageStyle(size: .medium))
        imageView.backgroundColor = Colors.grayscale050.color
        return imageView
    }()
    
    private lazy var privacyImageView = UIImageView()
    
    private lazy var titleTextView: UITextView = {
        let textView = UITextView()
        textView.isEditable = false
        textView.isSelectable = false
        textView.isScrollEnabled = false
        textView.backgroundColor = Colors.backgroundPrimary.color
        return textView
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var valueLabel = UILabel()
    
    private lazy var dateLabel = UILabel()
    
    private lazy var payersTitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        label.text = PaymentRequestLocalizable.payment.text
        return label
    }()
    
    private lazy var payersTableView: UITableView = {
        let tableView = UITableView()
        tableView.tableHeaderView = headerView
        tableView.register(
            ChargePayerTableViewCell.self,
            forCellReuseIdentifier: String(describing: ChargePayerTableViewCell.self)
        )
        tableView.register(
            UsersListComponentLoadingViewCell.self,
            forCellReuseIdentifier: String(describing: UsersListComponentLoadingViewCell.self)
        )
        tableView.register(
            UsersListComponentErrorViewCell.self,
            forCellReuseIdentifier: String(describing: UsersListComponentErrorViewCell.self)
        )
        tableView.separatorStyle = .none
        return tableView
    }()
    
    // MARK: - Variables
    private var tableViewHandler: TableViewHandler<String, CellType, ChargePayerTableViewCell>?
    private var headerHeight: CGFloat = 0
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.callScreenAnalytics()
        viewModel.loadFeedData()
        viewModel.loadTransactionStatus()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        configureTableViewHeaderHeight()
    }

    private func configureTableViewHeaderHeight() {
        let headerSize = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        guard headerSize.height > headerHeight else {
            return
        }
        
        headerHeight = headerSize.height
        payersTableView.tableHeaderView?.frame.size = CGSize(width: view.frame.width, height: headerHeight)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(payersTableView)
        headerView.addSubview(feedStackView)
        headerView.addSubview(lineView)
        headerView.addSubview(payersTitleLabel)
        feedStackView.addArrangedSubview(feedHeaderStackView)
        feedStackView.addArrangedSubview(messageLabel)
        feedStackView.addArrangedSubview(feedFooterStackView)
        feedHeaderStackView.addArrangedSubview(iconImageView)
        feedHeaderStackView.addArrangedSubview(titleTextView)
        feedFooterStackView.addArrangedSubview(valueLabel)
        feedFooterStackView.addArrangedSubview(feedInfoStackView)
        feedInfoStackView.addArrangedSubview(privacyImageView)
        feedInfoStackView.addArrangedSubview(dateLabel)
    }
    
    override func setupConstraints() {
        titleTextView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.titleHeight)
        }
        
        headerView.snp.makeConstraints {
            $0.width.equalToSuperview()
        }
        
        feedHeaderStackView.snp.makeConstraints {
            $0.trailing.equalToSuperview()
        }
        
        privacyImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.privacyImageView)
        }
        
        feedStackView.snp.makeConstraints {
            $0.leading.top.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
        
        lineView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.lineHeight)
            $0.top.equalTo(feedStackView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
        }
        
        payersTitleLabel.snp.makeConstraints {
            $0.top.equalTo(lineView.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.bottom.equalToSuperview().inset(Spacing.base02)
        }
        
        payersTableView.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }

    override func configureViews() {
        title = PaymentRequestLocalizable.charge.text
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    // MARK: - TableViewHandler
    private func cellIdentifier(at indexPath: IndexPath, type: CellType) -> String {
        switch type {
        case .default:
            return String(describing: ChargePayerTableViewCell.self)
        case .loading:
            return String(describing: UsersListComponentLoadingViewCell.self)
        case .error:
            return String(describing: UsersListComponentErrorViewCell.self)
        }
    }
    
    private func configureMultiCell(row: Int, type: CellType, cell: UITableViewCell) {
        switch type {
        case let .default(transaction):
            guard let cell = cell as? ChargePayerTableViewCell else {
                return
            }
            cell.set(
                imageUrl: transaction.imageUrl,
                username: transaction.username,
                paymentStatus: transaction.statusText,
                paymentStatusColor: transaction.statusColor,
                isArrowHidden: transaction.isArrowHidden
            )
            
        case .loading:
            guard let cell = cell as? UsersListComponentLoadingViewCell else {
                return
            }
            cell.configureCell()
        case let .error(message, buttonText):
            guard let cell = cell as? UsersListComponentErrorViewCell else {
                return
            }
            cell.configureCellWith(text: message, buttonText: buttonText)
            cell.delegate = self
        }
    }
    
    private func didSelectCell(at indexPath: IndexPath, type: CellType) {
        switch type {
        case .default:
            viewModel.loadTransactionDetail(at: indexPath)
        default:
            break
        }
    }
    
    private func configRowHeight(type: CellType) -> RowHeightType {
        switch type {
        case .default:
            return .custom(UITableView.automaticDimension)
        case .loading, .error:
            return .full
        }
    }
}

// MARK: PaymentRequestFeedDetailDisplay
extension PaymentRequestFeedDetailViewController: PaymentRequestFeedDetailDisplay {
    func displayFeedImage(url: URL?, privacyImage: UIImage) {
        iconImageView.setImage(url: url)
        privacyImageView.image = privacyImage
    }
    
    func displayFeedText(title: NSAttributedString, message: NSAttributedString, value: NSAttributedString, date: NSAttributedString) {
        titleTextView.attributedText = title
        messageLabel.attributedText = message
        valueLabel.attributedText = value
        dateLabel.attributedText = date
    }
    
    func displayTableViewContent(_ sections: [Section<String, PaymentRequestFeedDetailViewController.CellType>]) {
        tableViewHandler = TableViewHandler(
            data: sections,
            cellIdentifier: cellIdentifier(at:type:),
            configureMultiCell: configureMultiCell(row:type:cell:),
            configureDidSelectRow: didSelectCell(at:type:),
            configureCellHeightHandler: configRowHeight(type:))
        payersTableView.dataSource = tableViewHandler
        payersTableView.delegate = tableViewHandler
        payersTableView.reloadData()
    }
}

// MARK: - UserListComponentErrorViewCellDelegate
extension PaymentRequestFeedDetailViewController: UserListComponentErrorViewCellDelegate {
    func didTouchOnButton() {
        viewModel.loadTransactionStatus()
    }
}
