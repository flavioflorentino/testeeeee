import Core

protocol PaymentRequestFeedDetailServicing {
    func paymentRequestStatus(transactionId: String, completion: @escaping (Result<(model: [PaymentRequestTransaction], data: Data?), DialogError>) -> Void)
    func paymentRequestFeedTransaction(transactionId: String, completion: @escaping (Result<(model: DefaultFeedItem, data: Data?), DialogError>) -> Void)
}

final class PaymentRequestFeedDetailService {
    typealias Dependencies = HasMainQueue
    // MARK: - Variables
    private let dependencies: Dependencies
    
    // MARK: - Life Cycle
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - PaymentRequestFeedDetailServicing
extension PaymentRequestFeedDetailService: PaymentRequestFeedDetailServicing {
    func paymentRequestStatus(transactionId: String, completion: @escaping (Result<(model: [PaymentRequestTransaction], data: Data?), DialogError>) -> Void) {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        let endpoint = PaymentRequestFeedDetailEndpoint.paymentRequestStatus(transactionId: transactionId)
        Api<[PaymentRequestTransaction]>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.mapError(DialogError.transformApiError)
                completion(mappedResult)
            }
        }
    }
    
    func paymentRequestFeedTransaction(transactionId: String, completion: @escaping (Result<(model: DefaultFeedItem, data: Data?), DialogError>) -> Void) {
        let decoder = JSONDecoder(.convertFromSnakeCase)
        let endpoint = PaymentRequestFeedDetailEndpoint.paymentRequestFeedTransaction(transactionId: transactionId)
        Api<AnyCodable>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case let .success((model, data)):
                    guard
                        let resultModel = model.value as? [String: Any],
                        let resultData = resultModel["data"] as? [String: Any],
                        let feedItems = resultData["feed"] as? [[String: Any]],
                        let firstItem = feedItems.first,
                        let feedItem = DefaultFeedItem(jsonDict: firstItem)
                        else {
                            completion(.failure(.default(.unknown(nil))))
                            return
                    }
                    completion(.success((model: feedItem, data: data)))
                case let .failure(error):
                    completion(.failure(DialogError.transformApiError(error)))
                }
            }
        }
    }
}
