import Foundation

struct PaymentRequestTransaction: Decodable {
    let p2pTransactionId: Int?
    let consumerPayerUsername: String
    let consumerPayerSmallImageUrl: String
}

struct PaymentRequestPresentableTransaction {
    let username: String
    let imageUrl: URL?
    let statusText: String
    let statusColor: UIColor
    let isArrowHidden: Bool
}
