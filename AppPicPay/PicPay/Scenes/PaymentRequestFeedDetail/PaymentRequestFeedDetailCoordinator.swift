import UIKit

enum PaymentRequestFeedDetailAction {
    case showDetail(feedItem: DefaultFeedItem)
}

protocol PaymentRequestFeedDetailCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: PaymentRequestFeedDetailAction)
}

final class PaymentRequestFeedDetailCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - PaymentRequestFeedDetailCoordinating
extension PaymentRequestFeedDetailCoordinator: PaymentRequestFeedDetailCoordinating {
    func perform(action: PaymentRequestFeedDetailAction) {
        if case let PaymentRequestFeedDetailAction.showDetail(feedItem) = action {
            let feedDetailModel = FeedDetailViewModel(feedItem: feedItem)
            let controller = FeedDetailViewController(model: feedDetailModel)
            controller.appearWithOpenKeyboard = false
            controller.hidesBottomBarWhenPushed = true
            viewController?.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
