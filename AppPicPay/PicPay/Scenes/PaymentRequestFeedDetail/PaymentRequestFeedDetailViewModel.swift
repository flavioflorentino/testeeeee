import AnalyticsModule
import Foundation

protocol PaymentRequestFeedDetailViewModelInputs: AnyObject {
    func callScreenAnalytics()
    func loadFeedData()
    func loadTransactionStatus()
    func loadTransactionDetail(at indexPath: IndexPath)
}

final class PaymentRequestFeedDetailViewModel {
    typealias Dependencies = HasAnalytics
    
    // MARK: - Variables
    private let service: PaymentRequestFeedDetailServicing
    private let presenter: PaymentRequestFeedDetailPresenting
    private let feedItem: DefaultFeedItem
    private var transactions: [PaymentRequestTransaction] = []
    private let dependencies: Dependencies
    
    // MARK: - Life Cycle
    init(
        service: PaymentRequestFeedDetailServicing,
        presenter: PaymentRequestFeedDetailPresenting,
        feedItem: DefaultFeedItem,
        dependencies: Dependencies = DependencyContainer()
    ) {
        self.service = service
        self.presenter = presenter
        self.feedItem = feedItem
        self.dependencies = dependencies
    }
}

// MARK: - PaymentRequestFeedDetailViewModelInputs
extension PaymentRequestFeedDetailViewModel: PaymentRequestFeedDetailViewModelInputs {
    func callScreenAnalytics() {
        dependencies.analytics.log(PaymentRequestUserEvent.feedItemViewed)
    }
    
    func loadFeedData() {
        presenter.presentFeedInfo(feedItem: feedItem)
    }
    
    func loadTransactionStatus() {
        presenter.presentLoader()
        service.paymentRequestStatus(transactionId: feedItem.transactionId) { [weak self] result in
            switch result {
            case let .success(response):
                self?.paymentRequestStatusSuccess(transactions: response.model)
            case .failure:
                self?.paymentRequestStatusFailure()
            }
        }
    }
    
    private func paymentRequestStatusSuccess(transactions: [PaymentRequestTransaction]) {
        dependencies.analytics.log(PaymentRequestUserEvent.paidDetail)
        self.transactions = transactions
        presenter.presentTransactions(transactions)
    }
    
    private func paymentRequestStatusFailure() {
        presenter.presentGenericError()
    }
    
    func loadTransactionDetail(at indexPath: IndexPath) {
        guard
            indexPath.row < transactions.count,
            let id = transactions[indexPath.row].p2pTransactionId
            else {
                return
        }
        presenter.presentLoader()
        service.paymentRequestFeedTransaction(transactionId: String(id)) { [weak self] result in
            switch result {
            case let .success(response):
                self?.paymentRequestTransactionDetailSuccess(feedItem: response.model)
            case .failure:
                self?.paymentRequestTransactionDetailFailure()
            }
        }
    }
    
    private func paymentRequestTransactionDetailSuccess(feedItem: DefaultFeedItem) {
        presenter.presentTransactions(transactions)
        presenter.didNextStep(action: .showDetail(feedItem: feedItem))
    }
    
    private func paymentRequestTransactionDetailFailure() {
        presenter.presentGenericError()
    }
}
