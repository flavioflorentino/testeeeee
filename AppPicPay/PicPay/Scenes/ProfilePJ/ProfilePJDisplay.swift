import Foundation
import UIKit

struct ProfilePJDisplayData: Equatable {
    let title: String
    let description: String
    let address: String?
    let items: [Item]
    let imageUrl: URL?
    let phone: String?
}

extension ProfilePJDisplayData {
    enum Item: Equatable {
        case info(PromotionSeller.Item)
        case map(title: String, map: PromotionSeller.Map)
    }
}
