import UIKit

enum RecommendationIncentiveAction {
    case close
    case share(text: String)
}

protocol RecommendationIncentiveCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RecommendationIncentiveAction)
}

final class RecommendationIncentiveCoordinator: RecommendationIncentiveCoordinating {
    weak var viewController: UIViewController?

    func perform(action: RecommendationIncentiveAction) {
        switch action {
        case .close:
            close()
        case let .share(text):
            presentShareController(text)
        }
    }
}

extension RecommendationIncentiveCoordinator {
    private func presentShareController(_ shareText: String?) {
        let controller = UIActivityViewController(
            activityItems: [shareText as Any],
            applicationActivities: nil
        )
        
        controller.completionWithItemsHandler = { _, _, _, _ in
            self.close()
        }
        
        viewController?.present(controller, animated: true)
    }
    
    private func close() {
        viewController?.dismiss(animated: true)
    }
}
