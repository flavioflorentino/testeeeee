import AnalyticsModule

enum RecommendationIncentiveEvent: AnalyticsKeyProtocol {
    case close
    case copyCode
    case inviteIncentive
    
    private var name: String {
        switch self {
        case .close:
            return "Close MGM Incentive Screen"
        case .copyCode:
            return "Copy Code Incentive Screen"
        case .inviteIncentive:
            return "Invite Friend Incentive Screen"
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("MGM - APP - \(name)", providers: providers)
    }
}
