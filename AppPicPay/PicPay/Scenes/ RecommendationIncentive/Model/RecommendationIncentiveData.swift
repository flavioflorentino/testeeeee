import Foundation

struct RecommendationIncentiveData {
    let referralCode: String
    let cashBackValue: Double
    let shareMessage: String?
}
