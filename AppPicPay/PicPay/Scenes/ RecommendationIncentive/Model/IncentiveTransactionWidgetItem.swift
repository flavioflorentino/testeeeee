import Foundation

struct IncentiveTransactionSocialShare {
    struct IncentiveTransactionShareItem {
        let link: URL?
        let text: String
        
        init?(jsonDict: [AnyHashable: Any]?) {
            guard let urlString = jsonDict?["link"] as? String,
                let text = jsonDict?["text"] as? String else {
                    return nil
            }
            self.link = URL(string: urlString)
            self.text = text
        }
    }
    
    let whatsApp: IncentiveTransactionShareItem?
    let twitter: IncentiveTransactionShareItem?
    let facebook: IncentiveTransactionShareItem?
    
    init(jsonDict: [AnyHashable: Any]) {
        whatsApp = IncentiveTransactionShareItem(jsonDict: jsonDict["whats_app"] as? [AnyHashable: Any])
        twitter = IncentiveTransactionShareItem(jsonDict: jsonDict["twitter"] as? [AnyHashable: Any])
        facebook = IncentiveTransactionShareItem(jsonDict: jsonDict["facebook"] as? [AnyHashable: Any])
    }
}

struct IncentiveTransactionUserInfo {
    let name: String
    let code: String
    
    init(jsonDict: [AnyHashable: Any]) {
        self.name = jsonDict["name"] as? String ?? ""
        self.code = jsonDict["mgm_code"] as? String ?? ""
    }
}

final class IncentiveTransactionWidgetItem: ReceiptWidgetItem {
    enum ActionType: String {
        case modal
        case unknown
    }
    
    let actionType: ActionType
    let cashbackValue: Double
    let incentiveTransactioSocialShare: IncentiveTransactionSocialShare
    let userInfo: IncentiveTransactionUserInfo
    
    init?(jsonDict: [AnyHashable: Any]) {
        guard
            let actionType = jsonDict["action_type"] as? String,
            let cashbackValue = jsonDict["cash_back_value"] as? Double,
            let incentiveTransactioSocialShare = jsonDict["share"] as? [AnyHashable: Any],
            let userInfo = jsonDict["user_info"] as? [AnyHashable: Any] else {
                return nil
        }
        self.actionType = ActionType(rawValue: actionType) ?? .unknown
        self.cashbackValue = cashbackValue
        self.incentiveTransactioSocialShare = IncentiveTransactionSocialShare(jsonDict: incentiveTransactioSocialShare)
        self.userInfo = IncentiveTransactionUserInfo(jsonDict: userInfo)
    }
}
