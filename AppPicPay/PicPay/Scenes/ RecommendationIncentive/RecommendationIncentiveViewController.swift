import UI
import UIKit

extension RecommendationIncentiveViewController.Layout {
    enum Size {
        static let imageHeight: CGFloat = 118
        static let imageWidth: CGFloat = 98
        static let referralCodeViewHeight: CGFloat = 40
        static let recommendationButtonHeight: CGFloat = 48
    }
    
    enum Font {
        static let titleFont = UIFont.systemFont(ofSize: 16, weight: .semibold)
        static let subtitleFont = UIFont.systemFont(ofSize: 14)
    }
    
    enum Spacing {
        static let buttonImageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 16)
    }
}

final class RecommendationIncentiveViewController: ViewController<RecommendationIncentiveViewModelInputs, UIView> {
    typealias Localizable = Strings.RecommendationIncentive
    fileprivate struct Layout {}
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.RecommendationIncentive.icoPig.image
        
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.titleFont
        label.numberOfLines = .zero
        label.textAlignment = .center
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.subtitleFont
        label.numberOfLines = .zero
        label.textAlignment = .center
        return label
    }()
    
    private lazy var referralCodeView: ReferralCodeView = {
        let referralView = ReferralCodeView(didCopyCodeHandler: viewModel.copyCode)
        referralView.canCopyOnTap = true
        
        return referralView
    }()
    
    private lazy var recommendationButton: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = Layout.Size.recommendationButtonHeight / 2
        button.clipsToBounds = true
        button.backgroundColor = Colors.branding300.color
        button.setTitleColor(Colors.white.color, for: .normal)
        button.setTitle(Localizable.buttonTitle, for: .normal)
        button.setImage(Assets.RecommendationIncentive.icoWhatsapp.image, for: .normal)
        button.imageEdgeInsets = Layout.Spacing.buttonImageInsets
        button.addTarget(self, action: #selector(didTapShareButton), for: .touchUpInside)
        
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateNavigationBarAppearance()
        viewModel.viewDidLoad()
    }
    
    // MARK: - Private Methods Helper
    private func updateNavigationBarAppearance() {
        let barButtonItem = UIBarButtonItem(
            image: Assets.Icons.iconClose.image,
            style: .plain,
            target: self,
            action: #selector(tapClose)
        )
        
        barButtonItem.tintColor = Colors.branding300.color
        navigationItem.rightBarButtonItem = barButtonItem
        navigationController?.navigationBar.barTintColor = Colors.white.color
    }
    
    override func buildViewHierarchy() {
        view.addSubview(imageView)
        view.addSubview(titleLabel)
        view.addSubview(subtitleLabel)
        view.addSubview(referralCodeView)
        view.addSubview(recommendationButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.white.color
    }
    
    override func setupConstraints() {
        imageView.layout {
            $0.top == view.topAnchor + Spacing.base04
            $0.centerX == view.centerXAnchor
            $0.height == Layout.Size.imageHeight
            $0.width == Layout.Size.imageWidth
        }
        
        titleLabel.layout {
            $0.top == imageView.bottomAnchor + Spacing.base02
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
        }
        
        subtitleLabel.layout {
            $0.top == titleLabel.bottomAnchor + Spacing.base03
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
        }
        
        referralCodeView.layout {
            $0.top == subtitleLabel.bottomAnchor + Spacing.base04
            $0.leading == view.leadingAnchor + Spacing.base03
            $0.trailing == view.trailingAnchor - Spacing.base03
            $0.height == Layout.Size.referralCodeViewHeight
        }
        
        recommendationButton.layout {
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
            $0.bottom == view.compatibleSafeAreaLayoutGuide.bottomAnchor - Spacing.base02
            $0.height == Layout.Size.recommendationButtonHeight
        }
    }
}

@objc
extension RecommendationIncentiveViewController {
    private func tapClose() {
        viewModel.close()
    }
    
    private func didTapShareButton() {
        viewModel.shareCode()
    }
}

// MARK: View Model Outputs
extension RecommendationIncentiveViewController: RecommendationIncentiveDisplay {
    func displayData(cashBackValue: String, referralCode: String) {
        referralCodeView.referralCode = referralCode
        titleLabel.text = Localizable.title(cashBackValue)
        subtitleLabel.text = Localizable.subtitle(cashBackValue)
    }
}
