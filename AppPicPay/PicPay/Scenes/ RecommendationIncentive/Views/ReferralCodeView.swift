import UIKit
import UI

extension ReferralCodeView.Layout {
    enum Spacing {
        static let stackViewItemsSpace: CGFloat = 7
    }
    
    enum Font {
        static let labelFont = UIFont.systemFont(ofSize: 20, weight: .semibold)
    }
    
    enum AnimationDuration {
        static let fadeIn: TimeInterval = 0.3
        static let fadeOut: TimeInterval = 0.5
    }
}

final class ReferralCodeView: UIView {
    fileprivate enum Layout {}
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view
            .viewStyle(RoundedViewStyle())
            .with(\.border, .light(color: .grayscale100(.default)))
        
        if canCopyOnTap {
            let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(copyCode))
            view.addGestureRecognizer(gestureRecognizer)
        }
        
        return view
    }()
    
    private lazy var label: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .branding300(.default))
            .with(\.textAlignment, .center)
        label.text = referralCode
        
        return label
    }()
    
    private lazy var copyView: UIView = {
        let view = UIView()
        view
            .viewStyle(RoundedViewStyle(cornerRadius: .light))
            .with(\.border, .light(color: .grayscale100()))
        view.backgroundColor = backgroundColor
        view.alpha = .zero
        
        return view
    }()
    
    private lazy var copyLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .branding300(.default))
            .with(\.textAlignment, .center)
        
        label.text = Strings.RecommendationIncentive.copyCode
        
        return label
    }()
    
    var referralCode: String? {
        didSet {
            buildLayout()
        }
    }
    
    private var didCopyCodeHandler: ((String?) -> Void)?
    var canCopyOnTap: Bool = true
    
    init(didCopyCodeHandler: ((String?) -> Void)? = nil) {
        self.didCopyCodeHandler = didCopyCodeHandler
        super.init(frame: .zero)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    func copyCode() {
        UIView.animate(withDuration: Layout.AnimationDuration.fadeIn, animations: {
            self.copyView.alpha = 1
            self.label.alpha = .zero
        }, completion: { _ in
            UIView.animate(withDuration: Layout.AnimationDuration.fadeOut) {
                self.copyView.alpha = .zero
                self.label.alpha = 1
            }
        })
        
        didCopyCodeHandler?(referralCode)
    }
}

extension ReferralCodeView: ViewConfiguration {
    func buildViewHierarchy() {
        copyView.addSubview(copyLabel)
        contentView.addSubview(label)
        addSubviews(contentView)
        addSubview(copyView)
    }
    
    func setupConstraints() {
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        label.snp.makeConstraints {
            $0.centerX.centerY.leading.equalToSuperview()
        }
        
        copyView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        copyLabel.snp.makeConstraints {
            $0.centerX.centerY.leading.equalToSuperview()
        }
    }
}
