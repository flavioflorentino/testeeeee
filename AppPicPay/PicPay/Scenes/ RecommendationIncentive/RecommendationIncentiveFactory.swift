import Foundation

enum RecommendationIncentiveFactory {
    static func make(recommendationIncentiveData: RecommendationIncentiveData) -> RecommendationIncentiveViewController {
        let coordinator: RecommendationIncentiveCoordinating = RecommendationIncentiveCoordinator()
        let presenter: RecommendationIncentivePresenting = RecommendationIncentivePresenter(coordinator: coordinator)
        let viewModel = RecommendationIncentiveViewModel(
            presenter: presenter,
            recommendationIncentiveData: recommendationIncentiveData,
            dependencies: DependencyContainer()
        )
        
        let viewController = RecommendationIncentiveViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
