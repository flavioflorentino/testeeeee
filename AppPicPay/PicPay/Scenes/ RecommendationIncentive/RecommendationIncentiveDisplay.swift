import UIKit

protocol RecommendationIncentiveDisplay: AnyObject {
    func displayData(cashBackValue: String, referralCode: String)
}
