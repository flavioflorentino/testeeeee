import Foundation
import AnalyticsModule

protocol RecommendationIncentiveViewModelInputs: AnyObject {
    func viewDidLoad()
    func close()
    func shareCode()
    func copyCode(code: String?)
}

final class RecommendationIncentiveViewModel {
    typealias Dependencies = HasAnalytics
    
    private let presenter: RecommendationIncentivePresenting
    private let recommendationIncentiveData: RecommendationIncentiveData
    private let dependencies: Dependencies
    
    init(
        presenter: RecommendationIncentivePresenting,
        recommendationIncentiveData: RecommendationIncentiveData,
        dependencies: Dependencies
    ) {
        self.presenter = presenter
        self.recommendationIncentiveData = recommendationIncentiveData
        self.dependencies = dependencies
    }
}

extension RecommendationIncentiveViewModel: RecommendationIncentiveViewModelInputs {
    func viewDidLoad() {
        presenter.presentData(data: recommendationIncentiveData)
    }
    
    func close() {
        dependencies.analytics.log(RecommendationIncentiveEvent.close)
        presenter.didNextStep(action: .close)
    }
    
    func shareCode() {
        dependencies.analytics.log(RecommendationIncentiveEvent.inviteIncentive)
        
        guard let text = recommendationIncentiveData.shareMessage else {
            return
        }
        
        guard let shareUrl = URL(string: "whatsapp://send?text=\(text)"),
            UIApplication.shared.canOpenURL(shareUrl) else {
                presenter.didNextStep(action: .share(text: text))
                return
        }
        
        UIApplication.shared.open(shareUrl, options: [:])
    }
    
    func copyCode(code: String?) {
        dependencies.analytics.log(RecommendationIncentiveEvent.copyCode)
        
        UIPasteboard.general.string = code
    }
}
