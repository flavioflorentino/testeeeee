import Core
import Foundation

protocol RecommendationIncentivePresenting: AnyObject {
    var viewController: RecommendationIncentiveDisplay? { get set }
    func presentData(data: RecommendationIncentiveData)
    func didNextStep(action: RecommendationIncentiveAction)
}

final class RecommendationIncentivePresenter: RecommendationIncentivePresenting {
    private let coordinator: RecommendationIncentiveCoordinating
    weak var viewController: RecommendationIncentiveDisplay?

    init(coordinator: RecommendationIncentiveCoordinating) {
        self.coordinator = coordinator
    }
    
    func presentData(data: RecommendationIncentiveData) {
        viewController?.displayData(
            cashBackValue: "R$ \(Int(data.cashBackValue))",
            referralCode: data.referralCode
        )
    }
    
    func didNextStep(action: RecommendationIncentiveAction) {
        coordinator.perform(action: action)
    }
}
