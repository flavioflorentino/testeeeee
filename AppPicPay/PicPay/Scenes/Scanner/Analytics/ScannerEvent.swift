import AnalyticsModule

enum ScannerQRCodeOrigin: String {
    case pay = "pagar"
    case menuTransactions = "menu_transactions"
    case home
}

enum ScannerShareType: String {
    case shareImage = "share_image"
    case shareLink = "share_link"
}

enum ScannerSegmentName: String {
    case reader
    case myQrCode = "my_qr_code"
}

enum ScannerQRCodeType: String {
    case unknown
    case parking
    case pav
    case p2p
    case ecommerceOld = "ecommerce_old"
    case ecommerce
    case p2m
    case cashout24h = "cashout_24h"
    case linx
    case iptu
    case deeplink
    case pix
    case vendingMachine = "vending_machine"
    case digitalGood
}

enum ScannerEvent: AnalyticsKeyProtocol {
    case qrcodeAccessed(_ origin: ScannerQRCodeOrigin)
    case billsScannerAccessed
    case qrcodeScanned(_ origin: ScannerQRCodeType, _ isBrCode: Bool = false)
    case qrcodeShare(_ type: ScannerShareType)
    case qrcodeInformationAccessed
    case qrcodeSegmentedChanged(_ segment: ScannerSegmentName)
    case permissionCameraClicked
    
    private var name: String {
        switch self {
        case .qrcodeAccessed:
            return "Qr Code Accessed"
        case .billsScannerAccessed:
            return "Bills Scanner Accessed"
        case .qrcodeScanned:
            return "QR Code Scanned"
        case .qrcodeShare:
            return "QR Code Share"
        case .qrcodeInformationAccessed:
            return "QR Code Information Accessed"
        case .qrcodeSegmentedChanged:
            return "QR Code Segment Changed"
        case .permissionCameraClicked:
            return "Permission Camera Clicked"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .qrcodeAccessed(origin):
            return ["origin": origin.rawValue]
        case let .qrcodeScanned(type, isBrCode):
            return ["origin": type.rawValue, "is_brcode": isBrCode]
        case let .qrcodeShare(type):
            return ["type": type.rawValue]
        case let .qrcodeSegmentedChanged(segment):
            return ["item_name": segment.rawValue]
        default:
            return [:]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.mixPanel])
    }
}
