import UIKit

public enum ProfilePromoCodeAction {
    case finish
}

public protocol ProfilePromoCodeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ProfilePromoCodeAction)
}

public final class ProfilePromoCodeCoordinator: ProfilePromoCodeCoordinating {
    public weak var viewController: UIViewController?
    
    public func perform(action: ProfilePromoCodeAction) {
        if case .finish = action {
            viewController?.dismiss(animated: true)
        }
    }
}
