import SwiftyJSON

struct FollowActionResponse {
    let followerStatus: FollowerStatus
    let consumerStatus: FollowerStatus
}
