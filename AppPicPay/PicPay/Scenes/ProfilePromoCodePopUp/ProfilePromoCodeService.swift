import Core
import Foundation

protocol ProfilePromoCodeServicing {
    typealias FollowersActionCompletion = (Result<FollowActionResponse, PicPayError>) -> Void
    func executeFollowAction(action: FollowerButtonAction, follower: String, completion: @escaping FollowersActionCompletion)
}

final class ProfilePromoCodeService: ProfilePromoCodeServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    func executeFollowAction(action: FollowerButtonAction, follower: String, completion: @escaping FollowersActionCompletion) {
        let mainQueue = dependencies.mainQueue
        
        WSSocial.followAction(action, follower: follower) { success, followerStatus, consumerStatus, error in
            mainQueue.async {
                if let error = error, !success {
                    let picPayError = PicPayError(message: error.localizedDescription)
                    completion(.failure(picPayError))
                    return
                }
                
                let response = FollowActionResponse(followerStatus: consumerStatus, consumerStatus: followerStatus)
                completion(.success(response))
            }
        }
    }
}
