import UI

protocol MainViewDelegate: AnyObject {
    func closeScreen()
    func followAction(button: UIPPFollowButton)
}

final class ProfilePromoCodeMainView: UIView {
    private lazy var closeButton: UIButton = {
        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(closeScreen), for: .touchUpInside)
        let image = #imageLiteral(resourceName: "close_popup.png")
        button.setImage(image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var accountView: ProfilePromoCodeAccountView = {
        let view = ProfilePromoCodeAccountView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var followView: ProfilePromoCodeFollowView = {
        let view = ProfilePromoCodeFollowView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.delegate = self
        return view
    }()
    
    private lazy var okButton: UIPPButton = {
        let button = UIPPButton(title: DefaultLocalizable.btOkUpperCase.text, target: self, action: #selector(closeScreen), type: .custom)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        button.tintImage = true
        button.leftIconPadding = 10
        button.cornerRadius = 15
        button.normalBackgrounColor = Palette.ppColorPositive400.color
        button.normalTitleColor = Palette.white.color
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    weak var delegate: MainViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildViewHierarchy()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(contact: PPContact, isPro: Bool, showFollow: Bool) {
        accountView.configureViews(userName: contact.username ?? " ", name: contact.onlineName ?? " ", imgUrl: contact.imgUrlLarge ?? "", isPro: isPro, isVerified: contact.isVerified)
        let followerStatus = FollowerStatus(rawValue: contact.followerStatus) ?? .notFollowing
        followView.configureViews(following: contact.following, followers: contact.followers, followerStatus: followerStatus)
    }
    
    func changeFollowStatus(response: FollowActionResponse) {
        followView.changeFollowStatus(response: response)
    }
    
    func getFollowButtonAction() -> FollowerButtonAction {
        followView.getFollowButtonAction()
    }
    
    @objc
    private func closeScreen(sender: UIButton) {
        delegate?.closeScreen()
    }
}

extension ProfilePromoCodeMainView: FollowViewDelegate {
    func followAction(button: UIPPFollowButton) {
        delegate?.followAction(button: button)
    }
}

extension ProfilePromoCodeMainView: ViewConfiguration {
    func configureViews() {}
    
    func buildViewHierarchy() {
        addSubview(closeButton)
        addSubview(accountView)
        addSubview(followView)
        addSubview(okButton)
        setupConstraints()
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            closeButton.topAnchor.constraint(equalTo: topAnchor),
            closeButton.trailingAnchor.constraint(equalTo: trailingAnchor),
            closeButton.heightAnchor.constraint(equalToConstant: 40),
            closeButton.widthAnchor.constraint(equalToConstant: 40)
        ])
        
        NSLayoutConstraint.activate([
            accountView.topAnchor.constraint(equalTo: closeButton.bottomAnchor),
            accountView.trailingAnchor.constraint(equalTo: trailingAnchor),
            accountView.leadingAnchor.constraint(equalTo: leadingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            followView.trailingAnchor.constraint(equalTo: trailingAnchor),
            followView.leadingAnchor.constraint(equalTo: leadingAnchor),
            followView.topAnchor.constraint(equalTo: accountView.bottomAnchor, constant: 20),
            followView.bottomAnchor.constraint(equalTo: okButton.topAnchor)
        ])

        NSLayoutConstraint.activate([
            okButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            okButton.widthAnchor.constraint(equalToConstant: 74),
            okButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20)
        ])
    }
}
