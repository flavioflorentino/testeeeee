import UI

final class FollowsView: UIView {
    private lazy var accountLabel: UILabel = {
        let label = UILabel()
        label.text = "0"
        label.textColor = Palette.ppColorGrayscale600.color
        label.font = UIFont.systemFont(ofSize: 14)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.text = PromoCodePopUpLocalizable.followingLabel.text
        label.textColor = Palette.ppColorGrayscale400.color
        label.font = UIFont.systemFont(ofSize: 11)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    func configure(account: String, info: String) {
        accountLabel.text = account
        infoLabel.text = info
        buildViewHierarchy()
    }
}

extension FollowsView: ViewConfiguration {
    func configureViews() {}
    
    func buildViewHierarchy() {
        addSubview(accountLabel)
        addSubview(infoLabel)
        setupConstraints()
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            accountLabel.topAnchor.constraint(equalTo: topAnchor),
            accountLabel.bottomAnchor.constraint(equalTo: infoLabel.topAnchor, constant: -4),
            accountLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            accountLabel.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
            
        NSLayoutConstraint.activate([
            infoLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            infoLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            infoLabel.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
}
