import UI

protocol FollowViewDelegate: AnyObject {
    func followAction(button: UIPPFollowButton)
}

final class ProfilePromoCodeFollowView: UIView {
    private lazy var barTopView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale400.color
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var barBottomView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale400.color
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var followsView: FollowsView = {
        let view = FollowsView()
        view.configure(account: "0", info: PromoCodePopUpLocalizable.followingLabel.text)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var followersView: FollowsView = {
        let view = FollowsView()
        view.configure(account: "0", info: PromoCodePopUpLocalizable.followersLabel.text)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var followStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.alignment = .center
        stack.distribution = .fillProportionally
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private lazy var followButton: UIPPFollowButton = {
        let button = UIPPFollowButton(title: PromoCodePopUpLocalizable.followTitleButton.text, target: self, action: #selector(followAction))
        let image = #imageLiteral(resourceName: "ico_nav_friends.png")
        button.setImage(image, for: .normal)
        button.tintImage = true
        button.centerLoading = true
        button.setupTintImage()
        button.cornerRadius = 13.5
        button.borderColor = Palette.ppColorPositive400.color
        button.borderWidth = 1
        button.disabledBorderColor = Palette.ppColorGrayscale500.color
        button.disabledTitleColor = Palette.white.color
        button.disabledBackgrounColor = Palette.ppColorGrayscale500.color
        button.leftIconPadding = 12
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        button.type = 0
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    weak var delegate: FollowViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildViewHierarchy()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews(following: Int, followers: Int, followerStatus: FollowerStatus) {
        followsView.configure(account: "\(following)", info: PromoCodePopUpLocalizable.followingLabel.text)
        followersView.configure(account: "\(followers)", info: PromoCodePopUpLocalizable.followersLabel.text)
        followButton.changeButtonWithStatus(.notFollowing, consumerStatus: .notFollowing)
    }
    
    func changeFollowStatus(response: FollowActionResponse) {
        followButton.changeButtonWithStatus(response.followerStatus, consumerStatus: response.consumerStatus)
    }
    
    func getFollowButtonAction() -> FollowerButtonAction {
        followButton.action
    }
    
    @objc
    private func followAction(sender: UIPPFollowButton) {
        delegate?.followAction(button: sender)
    }
}

extension ProfilePromoCodeFollowView: ViewConfiguration {
    func configureViews() {}
    
    func buildViewHierarchy() {
        followStack.addArrangedSubview(followsView)
        followStack.addArrangedSubview(followersView)
        addSubview(barTopView)
        addSubview(followStack)
        addSubview(followButton)
        addSubview(barBottomView)
        setupConstraints()
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            barTopView.topAnchor.constraint(equalTo: topAnchor),
            barTopView.heightAnchor.constraint(equalToConstant: 1),
            barTopView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            barTopView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15)
        ])
        
        NSLayoutConstraint.activate([
            followStack.topAnchor.constraint(equalTo: barTopView.bottomAnchor, constant: 10),
            followStack.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            followStack.trailingAnchor.constraint(equalTo: followButton.leadingAnchor),
            followStack.bottomAnchor.constraint(equalTo: barBottomView.topAnchor, constant: -10)
        ])

        NSLayoutConstraint.activate([
            barBottomView.heightAnchor.constraint(equalToConstant: 1),
            barBottomView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            barBottomView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            barBottomView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15)
        ])
        
        NSLayoutConstraint.activate([
            followButton.centerYAnchor.constraint(equalTo: followStack.centerYAnchor),
            followButton.heightAnchor.constraint(equalToConstant: 27),
            followButton.widthAnchor.constraint(lessThanOrEqualToConstant: 100),
            followButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15)
        ])
        followButton.setContentHuggingPriority(.defaultLow, for: .horizontal)
    }
}
