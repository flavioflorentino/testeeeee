import UI

final class ProfilePromoCodeAccountView: UIView {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = DefaultLocalizable.congratulations.text
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 20, weight: .bold)
        return label
    }()
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.text = PromoCodePopUpLocalizable.popUpPromoCodeTitle.text
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 14, weight: .regular)
        return label
    }()
    
    private lazy var userNameLabel: UILabel = {
        let label = UILabel()
        label.text = PromoCodePopUpLocalizable.placeHolderUserName.text
        label.textColor = Palette.ppColorGrayscale600.color
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 17, weight: .semibold)
        return label
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.text = PromoCodePopUpLocalizable.placeHolderName.text
        label.textColor = Palette.ppColorGrayscale400.color
        label.numberOfLines = 2
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 14, weight: .regular)
        return label
    }()
    
    private lazy var avatarImage: UICircularImageView = {
        let image = #imageLiteral(resourceName: "avatar_person.png")
        let imageView = UICircularImageView(image: image)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    private lazy var verifiedProfileImage: UIImageView = {
        let image = #imageLiteral(resourceName: "ico_verified_profile.png")
        let imageView = UIImageView(image: image)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    private lazy var proBadgeImage: UIImageView = {
        let image = #imageLiteral(resourceName: "ico_pro_badge_profile.png")
        let imageView = UIImageView(image: image)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .center
        return imageView
    }()
    
    init() {
        super.init(frame: CGRect.zero)
        buildViewHierarchy()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews(userName: String, name: String, imgUrl: String, isPro: Bool, isVerified: Bool) {
        userNameLabel.text = "@" + userName
        nameLabel.text = name
        avatarImage.setImage(url: URL(string: imgUrl))
        verifiedProfileImage.isHidden = !isVerified
        proBadgeImage.isHidden = isPro
    }
}

extension ProfilePromoCodeAccountView: ViewConfiguration {
    func configureViews() {}
    
    func buildViewHierarchy() {
        addSubview(titleLabel)
        addSubview(infoLabel)
        addSubview(avatarImage)
        addSubview(verifiedProfileImage)
        addSubview(proBadgeImage)
        addSubview(userNameLabel)
        addSubview(nameLabel)
        setupConstraints()
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            titleLabel.bottomAnchor.constraint(equalTo: infoLabel.topAnchor)
        ])
        
        NSLayoutConstraint.activate([
            infoLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            infoLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            infoLabel.bottomAnchor.constraint(equalTo: avatarImage.topAnchor, constant: -15)
        ])
        
        NSLayoutConstraint.activate([
            avatarImage.centerXAnchor.constraint(equalTo: centerXAnchor),
            avatarImage.widthAnchor.constraint(equalToConstant: 100),
            avatarImage.heightAnchor.constraint(equalToConstant: 100)
        ])
        
        NSLayoutConstraint.activate([
            verifiedProfileImage.leadingAnchor.constraint(equalTo: avatarImage.trailingAnchor, constant: -30),
            verifiedProfileImage.topAnchor.constraint(equalTo: avatarImage.bottomAnchor, constant: -30),
            verifiedProfileImage.widthAnchor.constraint(equalToConstant: 32),
            verifiedProfileImage.heightAnchor.constraint(equalToConstant: 32)
        ])
        
        NSLayoutConstraint.activate([
            proBadgeImage.centerXAnchor.constraint(equalTo: avatarImage.centerXAnchor),
            proBadgeImage.widthAnchor.constraint(equalToConstant: 49),
            proBadgeImage.heightAnchor.constraint(equalToConstant: 15),
            proBadgeImage.topAnchor.constraint(equalTo: avatarImage.bottomAnchor, constant: -14)
        ])
        
        NSLayoutConstraint.activate([
            userNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 5),
            userNameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -5),
            userNameLabel.topAnchor.constraint(equalTo: avatarImage.bottomAnchor, constant: 10),
            userNameLabel.bottomAnchor.constraint(equalTo: nameLabel.topAnchor, constant: -3)
        ])
        
        NSLayoutConstraint.activate([
            nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            nameLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
