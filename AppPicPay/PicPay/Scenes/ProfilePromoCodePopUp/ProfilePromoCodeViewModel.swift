import Foundation

protocol ProfilePromoCodeViewModelInputs: AnyObject {
    func viewDidLoad()
    func followAction(action: FollowerButtonAction)
    func actionFollowStatus(action: FollowerButtonAction)
    func nextStep()
}

final class ProfilePromoCodeViewModel {
    private let service: ProfilePromoCodeServicing
    private let presenter: ProfilePromoCodePresenting
    private let contact: PPContact
    private let wsId: Int?
    
    init(service: ProfilePromoCodeServicing, presenter: ProfilePromoCodePresenting, contact: PPContact, wsId: Int?) {
        self.service = service
        self.presenter = presenter
        self.contact = contact
        self.wsId = wsId
    }
}

extension ProfilePromoCodeViewModel: ProfilePromoCodeViewModelInputs {
    func viewDidLoad() {
        var verified = false
        var hiddenFollow = false
        
        if contact.imgUrlLarge == nil || contact.imgUrlLarge.isEmpty {
            contact.imgUrlLarge = contact.imgUrl
        }
        
        if !contact.businessAccount || contact.isVerified {
            verified = true
        }
        
        hiddenFollow = wsId == contact.wsId
        
        presenter.showProfileInviter(contact: contact, isPro: verified, showFollow: hiddenFollow)
    }
    
    func followAction(action: FollowerButtonAction) {
        guard let userName = contact.username else {
            Log.debug(.model, "Contact model no have user name")
            return
        }
        switch action {
        case .cancelRequest:
            presenter.showAlert(with: PromoCodePopUpLocalizable.unfollowActionTitle.text, userName: userName)
        case .unfollow:
            presenter.showAlert(with: PromoCodePopUpLocalizable.cancelFollowActionTitle.text, userName: userName)
        default:
            actionFollowStatus(action: action)
        }
    }
    
    func actionFollowStatus(action: FollowerButtonAction) {
        service.executeFollowAction(action: action, follower: "\(contact.wsId)") { [weak self] result in
            switch result {
            case .success(let response):
                self?.presenter.changeFollowButton(with: response)
            case .failure(let error):
                let response = FollowActionResponse(followerStatus: .notFollowing, consumerStatus: .notFollowing)
                self?.presenter.changeFollowButton(with: response)
                self?.presenter.showAlertError(with: error)
            }
        }
        let response = FollowActionResponse(followerStatus: .loading, consumerStatus: .loading)
        presenter.changeFollowButton(with: response)
    }
    
    func nextStep() {
        presenter.closeScreen(action: .finish)
    }
}
