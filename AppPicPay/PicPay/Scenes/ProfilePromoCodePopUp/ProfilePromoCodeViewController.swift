import UI
import UIKit

final class ProfilePromoCodeViewController: LegacyViewController<ProfilePromoCodeViewModelInputs, ProfilePromoCodeCoordinating, UIView> {
    private lazy var mainView: ProfilePromoCodeMainView = {
        let view = ProfilePromoCodeMainView()
        view.backgroundColor = Palette.ppColorGrayscale000.color
        view.layer.cornerRadius = 10.0
        view.layer.masksToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        view.delegate = self
        return view
    }()
    
    private lazy var opacityView: UIView = {
        let view = UIView(frame: UIScreen.main.bounds)
        view.backgroundColor = Palette.black.color
        view.alpha = 0.7
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        viewModel.viewDidLoad()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(opacityView)
        view.addSubview(mainView)
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.activate([
            mainView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            mainView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            mainView.widthAnchor.constraint(equalToConstant: 280)
        ])
        
        NSLayoutConstraint.activate([
            opacityView.topAnchor.constraint(equalTo: view.topAnchor),
            opacityView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            opacityView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            opacityView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}

// MARK: View Model Outputs
extension ProfilePromoCodeViewController: ProfilePromoCodeDisplay {
    func showProfileInviter(contact: PPContact, isPro: Bool, showFollow: Bool) {
        mainView.configure(contact: contact, isPro: isPro, showFollow: showFollow)
    }

    func showAlertError(with error: Error?) {
        AlertMessage.showCustomAlertWithError(error, controller: self)
    }

    func changeFollowButton(with followerResponse: FollowActionResponse) {
        mainView.changeFollowStatus(response: followerResponse)
    }

    func showAlert(with message: String) {
        let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)

        let okAction = UIAlertAction(title: DefaultLocalizable.btYes.text, style: .default) { _ in
            self.viewModel.actionFollowStatus(action: self.mainView.getFollowButtonAction())
        }
        let cancelAction = UIAlertAction(title: DefaultLocalizable.btNo.text, style: .cancel, handler: nil)
        alert.addAction(okAction)
        alert.addAction(cancelAction)

        present(alert, animated: true)
    }

    func closeScreen(action: ProfilePromoCodeAction) {
        coordinator.perform(action: action)
    }
}

extension ProfilePromoCodeViewController: MainViewDelegate {
    func followAction(button: UIPPFollowButton) {
        viewModel.followAction(action: button.action)
    }
    
    func closeScreen() {
        viewModel.nextStep()
    }
}
