import UIKit

protocol ProfilePromoCodeDisplay: AnyObject {
    func showProfileInviter(contact: PPContact, isPro: Bool, showFollow: Bool)
    func showAlert(with message: String)
    func showAlertError(with error: Error?)
    func changeFollowButton(with followerResponse: FollowActionResponse)
    func closeScreen(action: ProfilePromoCodeAction)
}
