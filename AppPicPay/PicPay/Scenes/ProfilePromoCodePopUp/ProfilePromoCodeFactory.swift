import Foundation

enum ProfilePromoCodeFactory {
    static func make(contact: PPContact, wsId: Int?) -> ProfilePromoCodeViewController {
        let container = DependencyContainer()
        let service: ProfilePromoCodeServicing = ProfilePromoCodeService(dependencies: container)
        let presenter: ProfilePromoCodePresenting = ProfilePromoCodePresenter()
        let viewModel = ProfilePromoCodeViewModel(service: service, presenter: presenter, contact: contact, wsId: wsId)
        let coordinator: ProfilePromoCodeCoordinating = ProfilePromoCodeCoordinator()
        let viewController = ProfilePromoCodeViewController(viewModel: viewModel, coordinator: coordinator)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
