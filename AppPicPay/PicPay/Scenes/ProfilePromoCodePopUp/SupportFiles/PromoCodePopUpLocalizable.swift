enum PromoCodePopUpLocalizable: String, Localizable {
    case unfollowActionTitle
    case cancelFollowActionTitle
    case popUpPromoCodeTitle
    case placeHolderUserName
    case placeHolderName
    case followingLabel
    case followersLabel
    case followTitleButton
    
    var key: String {
        rawValue
    }
    var file: LocalizableFile {
        .promoCodePopUp
    }
}
