import Core
import Foundation

protocol ProfilePromoCodePresenting: AnyObject {
    var viewController: ProfilePromoCodeDisplay? { get set }
    func showProfileInviter(contact: PPContact, isPro: Bool, showFollow: Bool)
    func showAlert(with message: String, userName: String)
    func showAlertError(with error: Error)
    func changeFollowButton(with followerResponse: FollowActionResponse)
    func closeScreen(action: ProfilePromoCodeAction)
}

final class ProfilePromoCodePresenter: ProfilePromoCodePresenting {
    var viewController: ProfilePromoCodeDisplay?
    
    func showProfileInviter(contact: PPContact, isPro: Bool, showFollow: Bool) {
        viewController?.showProfileInviter(contact: contact, isPro: isPro, showFollow: showFollow)
    }

    func showAlertError(with error: Error) {
        viewController?.showAlertError(with: error)
    }

    func changeFollowButton(with followerResponse: FollowActionResponse) {
        viewController?.changeFollowButton(with: followerResponse)
    }

    func showAlert(with message: String, userName: String) {
        let newMessage = message + " @\(userName)"
        viewController?.showAlert(with: newMessage)
    }

    func closeScreen(action: ProfilePromoCodeAction) {
        viewController?.closeScreen(action: action)
    }
}
