enum PromoCodeLocalizable: String, Localizable {
    case understand
    case createUniAccount
    case regulation
    case uniAccount
    case student
    
    var key: String {
        rawValue
    }
    
    var file: LocalizableFile {
        .promoCodeRegister
    }
}
