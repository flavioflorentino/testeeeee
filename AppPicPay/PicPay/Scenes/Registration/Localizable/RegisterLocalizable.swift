import Foundation

public enum RegisterLocalizable: String, Localizable {
    case loading
    case showPassword
    case hidePassword
    case alreadyRegistered
    case quit
    case nameStepTitle
    case nameStepTitleWithName
    case nameStepDescription
    case nameStepFirstName
    case nameStepLastName
    case nameStepError
    case emailStepTitle
    case emailStepDescription
    case emailStepFieldPlaceholder
    case invalidEmail
    
    // Phone registration
    case phoneStepTitle
    case phoneStepText
    case phoneStepWhatsAppTitle
    case phoneStepSMSTitle
    case phoneStepDDD
    case phoneStepPhoneNumber
    case phoneCodeTitle
    case alertPhoneConfirmTitle
    case alertPhoneConfirmMessage
    case invalidDDDAndPhone
    case invalidDDD
    case invalidPhone
    case sendSMS
    case sendSMSDelay
    case codePlaceholder
    case callMe
    case callMeDelay
    case resendCode
    case resendCodedelay
    case editPhone
    case needHelp
    case alertTextBackToPhoneStep
    case haveCode
    case codeStepDefaultTextLabel
    case codeStepPhoneTextLabel
    case invalidCode
    case wrongInformations
    case passwordStepTitle
    case passwordStepDescription
    case passwordStepFieldPlaceholder
    case invalidPassword
    case documentStepTitle
    case documentStepDescription
    case cpfPlaceholder
    case dateOfBirthPlaceholder
    case alertDocumentHelpTitle
    case alertDocumentHelpText
    case documentFeatureDefaultTitle
    case documentFeatureDefaultText
    case documentFeatureDefaultButtonTitle
    case invalidCpfAndDateOfBirth
    case invalidCpf
    case invalidDateOfBirth
    case alertDocumentAlreadyRegisteredTitle
    case alertDocumentAlreadyRegisteredText
    case alertDocumentAlreadyRegisteredLoginButton
    case alertDocumentAlreadyRegisteredForgotPasswordButton
    case termsOfService
    case privacyPolicy
    case termsAndPrivacyDescription
    case continueRegistrationTitle
    case continueRegistrationTitleWithName
    case continueRegistrationDescription
    case continueRegistration
    case startNewRegistration
    case buttonTitleDefault
    case nameStepMustContainAtLeast
    case lastNameStepMustContainAtLeast
    case codeUsed
    case enterYourCode
    case yourPromotionalCode
    case promotionalCode
    case useAnotherCode
    case usernameTitle
    case usernameDescription
    case usernameUseCode
    case usernameCodeUsed
    case usernameConclude
    case usernameInvalid
    case alertMinimumAgeTitle
    case alertMinimumAgeText
    case alertMinimumAgeButton
    
    var file: LocalizableFile {
        .register
    }
    
    var key: String {
        rawValue
    }
}
