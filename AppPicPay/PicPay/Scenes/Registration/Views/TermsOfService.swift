import UI

final class TermsOfServiceTextView: UITextView {
    private var textToPresent: NSAttributedString = {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        
        let text = NSMutableAttributedString(
            string: RegisterLocalizable.termsAndPrivacyDescription.text,
            attributes: [
                .paragraphStyle: paragraphStyle,
                .font: UIFont.systemFont(ofSize: 12)
            ]
        )
        text.setAsLink(RegisterLocalizable.termsOfService.text, linkURL: "api/terms")
        text.setAsLink(RegisterLocalizable.privacyPolicy.text, linkURL: "api/privacy")
        return text
    }()
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        configureView()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureView() {
        attributedText = textToPresent
        isUserInteractionEnabled = true
        isScrollEnabled = false
        delaysContentTouches = false
        isSelectable = true
        isEditable = false
        linkTextAttributes = [.foregroundColor: Palette.ppColorBranding300.color]
        textColor = Palette.ppColorGrayscale400.color(withCustomDark: .ppColorGrayscale200)
        accessibilityTraits = .staticText
        backgroundColor = .clear
    }
}
