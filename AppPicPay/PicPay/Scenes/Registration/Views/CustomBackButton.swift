import UI
import UIKit

private extension CustomBackButton.Layout {
    enum Font {
        static let button: UIFont = .systemFont(ofSize: 17)
    }
}

final class CustomBackButton: UIButton {
    fileprivate enum Layout {}
    
    convenience init() {
        self.init(type: .system)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configureView()
    }
    
    func configureView() {
        let attributedString = NSAttributedString(
            string: DefaultLocalizable.back.text, attributes: [NSAttributedString.Key.font: Layout.Font.button]
        )
        
        setAttributedTitle(attributedString, for: .normal)
        setImage(Assets.Icons.chevronLeft.image, for: .normal)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: Spacing.base00, bottom: 0, right: -Spacing.base00)
        
        sizeToFit() // Fix bug on iOS 10
    }
}
