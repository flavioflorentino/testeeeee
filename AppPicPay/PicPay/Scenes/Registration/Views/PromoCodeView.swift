import UI

private extension PromoCodeView.Layout {
    enum Size {
        static let insetContainer = UIEdgeInsets(
            top: Spacing.base01,
            left: Spacing.base02,
            bottom: Spacing.base01,
            right: Spacing.base02
        )
        static let imageSize: CGFloat = 28
        static let borderWidth: CGFloat = 0.5
    }
}

final class PromoCodeView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var promocCodeLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary().font()
        label.textColor = Colors.grayscale500.color
        label.text = RegisterLocalizable.usernameCodeUsed.text.uppercased()
        return label
    }()
    
    private lazy var promoCodeContainerView = UIView()
    
    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base02
        stackView.alignment = .center
        return stackView
    }()
    
    private lazy var textStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = Layout.Size.imageSize / 2
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.bodyPrimary().font()
        label.textColor = Colors.black.color
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.caption().font()
        label.textColor = Colors.grayscale400.color
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.bodyPrimary().font()
        label.textColor = Colors.grayscale400.color
        label.textAlignment = .right
        return label
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        drawBorders()
    }
    
    func buildViewHierarchy() {
        textStackView.addArrangedSubview(titleLabel)
        textStackView.addArrangedSubview(descriptionLabel)
        
        containerStackView.addArrangedSubview(imageView)
        containerStackView.addArrangedSubview(textStackView)
        containerStackView.addArrangedSubview(valueLabel)
        
        promoCodeContainerView.addSubview(containerStackView)
        
        addSubview(promocCodeLabel)
        addSubview(promoCodeContainerView)
    }
    
    func setupConstraints() {
        promocCodeLabel.snp.makeConstraints {
            $0.top.trailing.equalToSuperview()
            $0.leading.equalTo(Spacing.base02)
        }
        
        promoCodeContainerView.snp.makeConstraints {
            $0.top.equalTo(promocCodeLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.bottom.equalToSuperview()
        }
        
        containerStackView.snp.makeConstraints {
            $0.edges.equalTo(Layout.Size.insetContainer)
        }
        
        imageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.imageSize)
        }
    }
    
    func configureStyles() {
        promoCodeContainerView.backgroundColor = Colors.white.color
    }
    
    func configure(title: String, description: String, image: UIImage) {
        updateLabels(title: title, description: description, value: nil)
        imageView.image = image
    }
    
    func configure(title: String, description: String, value: String?, imageURL: String?, placeholder: UIImage?) {
        updateLabels(title: title, description: description, value: value)
        imageView.image = placeholder
        
        if let imageURL = imageURL {
            imageView.setImage(url: URL(string: imageURL), placeholder: placeholder)
        }
    }
    
    private func updateLabels(title: String, description: String, value: String?) {
        titleLabel.text = title
        descriptionLabel.text = description
        valueLabel.isHidden = value == nil ? true : false
        valueLabel.text = value
    }
    
    private func drawBorders() {
        let topBorder = CALayer()
        topBorder.frame = CGRect(x: 0, y: 0, width: promoCodeContainerView.frame.width, height: Layout.Size.borderWidth)
        topBorder.backgroundColor = Colors.grayscale300.color.cgColor
        
        let bottonBorder = CALayer()
        bottonBorder.frame = CGRect(x: 0, y: promoCodeContainerView.frame.height - 1, width: self.frame.width, height: Layout.Size.borderWidth)
        bottonBorder.backgroundColor = Colors.grayscale300.color.cgColor
        
        promoCodeContainerView.layer.addSublayer(topBorder)
        promoCodeContainerView.layer.addSublayer(bottonBorder)
    }
}
