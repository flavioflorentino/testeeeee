import UI
import UIKit

private extension RoundedTextField.Layout {
    enum Color {
        static let background: UIColor = Palette.white.color
        static let text: UIColor = Palette.black.color
        static let borderError: CGColor = Palette.ppColorNegative500.cgColor
    }
    
    enum Size {
        static let inset: CGFloat = 16.0
        static let defaultCornerRadius: CGFloat = 20.0
    }
    
    enum Font {
        static let text: UIFont = .systemFont(ofSize: 14)
    }
}

final class RoundedTextField: UITextField {
    fileprivate enum Layout {}
    
    private var insetX: CGFloat = Layout.Size.inset
    
    override var placeholder: String? {
        didSet {
            guard let placeholder = placeholder else {
                return
            }
            attributedPlaceholder = NSAttributedString(
                string: placeholder,
                attributes: [
                    .foregroundColor: Palette.ppColorGrayscale400.color(withCustomDark: .ppColorGrayscale400)
                ]
            )
        }
    }
    
    var placeHolderTextColor: UIColor? {
        willSet {
            guard let placeholderText = placeholder,
                  let newValue = newValue else {
                return
            }
            
            attributedPlaceholder = NSAttributedString(string: placeholderText, attributes: [.foregroundColor: newValue])
        }
    }
    
    var cornerRadius: CGFloat = Layout.Size.defaultCornerRadius {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
        
    var hasError: Bool = false {
        didSet {
            if hasError {
                layer.borderWidth = 1.0
                layer.borderColor = Layout.Color.borderError
            } else if let backgroundColor = backgroundColor {
                layer.borderWidth = 0.0
                layer.borderColor = backgroundColor.cgColor
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    convenience init(insetX: CGFloat) {
        self.init(frame: .zero)
        self.insetX = insetX
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureView() {
        backgroundColor = Layout.Color.background
        textColor = Layout.Color.text
        font = Layout.Font.text
        
        borderStyle = .none
        layer.borderColor = UIColor.clear.cgColor
        layer.borderWidth = 0.0
        layer.masksToBounds = true
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        bounds.insetBy(dx: insetX, dy: 0)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        bounds.insetBy(dx: insetX, dy: 0)
    }
}

extension RoundedTextField {
    func setBorderedStyle() {
        border = .light(color: .black())
        textColor = Colors.black.color
        backgroundColor = .clear
        guard let placeholder = placeholder else {
            return
        }
        attributedPlaceholder = NSAttributedString(
            string: placeholder,
            attributes: [
                .foregroundColor: Colors.grayscale600.color
            ]
        )
    }
}
