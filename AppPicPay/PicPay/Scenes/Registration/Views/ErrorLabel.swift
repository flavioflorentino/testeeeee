import UI

private extension ErrorLabel.Layout {
    enum Style {
        static let font: UIFont = .systemFont(ofSize: 12)
        static let textColor = Palette.ppColorNegative500.color
        static let textAlignment: NSTextAlignment = .center
        static let numberOfLines = 0
    }
}

final class ErrorLabel: UILabel {
    fileprivate struct Layout {}
    
    init() {
        super.init(frame: .zero)
        configureView()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configureView()
    }
    
    private func configureView() {
        font = Layout.Style.font
        textColor = Layout.Style.textColor
        textAlignment = Layout.Style.textAlignment
        numberOfLines = Layout.Style.numberOfLines
    }
}
