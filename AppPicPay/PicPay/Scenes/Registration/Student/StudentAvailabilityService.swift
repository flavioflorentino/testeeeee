import Core
import FeatureFlag

protocol StudentAvailabilityServicing {
    var hasStudentSignUpFlow: Bool { get }
    func saveStudentValidatedApiId(_ validatedApiId: String)
    func checkConsumerStudentAvailability(
        cpf: String,
        dateOfBirth: String,
        completion: @escaping (Result<StudentAvailabilityResponse, ApiError>) -> Void
    )
}

final class StudentAvailabilityService: StudentAvailabilityServicing {
    typealias Dependencies = HasFeatureManager & HasKVStore & HasMainQueue
    private let dependencies: Dependencies
    
    var hasStudentSignUpFlow: Bool {
        dependencies.featureManager.isActive(.featureStudentSignUpFlow)
    }

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func saveStudentValidatedApiId(_ validatedApiId: String) {
        dependencies.kvStore.setString(validatedApiId, with: UniversityAccountKey.consumerStudentValidatedApiId)
    }
    
    func checkConsumerStudentAvailability(
        cpf: String,
        dateOfBirth: String,
        completion: @escaping (Result<StudentAvailabilityResponse, ApiError>) -> Void
    ) {
        let endpoint = UniversityAccountEndpoint.validateStudent(cpf: cpf, birth: dateOfBirth)
        Api<StudentAvailabilityResponse>(endpoint: endpoint).execute { result in
            self.dependencies.mainQueue.async {
                let mappedResult = result
                    .mapError { $0 as ApiError }
                    .map { $0.model }
                completion(mappedResult)
            }
        }
    }
}
