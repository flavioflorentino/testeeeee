import Foundation

enum StudentAvailabilityFactory {
    static func make() -> StudentAvailabilityInteractorContract {
        let container = DependencyContainer()
        let service: StudentAvailabilityServicing = StudentAvailabilityService(dependencies: container)
        let interactor: StudentAvailabilityInteractorContract = StudentAvailabilityInteractor(service: service)
        return interactor
    }
}
