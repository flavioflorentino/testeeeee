import Core
import UI

protocol StudentAvailabilityInteractorContract {
    func checkConsumerStudentAvailability(cpf: String, dateOfBirth: String)
}

final class StudentAvailabilityInteractor: StudentAvailabilityInteractorContract {
    private let service: StudentAvailabilityServicing
    
    init(service: StudentAvailabilityServicing) {
        self.service = service
    }
    
    func checkConsumerStudentAvailability(cpf: String, dateOfBirth: String) {
        guard service.hasStudentSignUpFlow else {
            return
        }

        service.checkConsumerStudentAvailability(cpf: cpf, dateOfBirth: dateOfBirth) { result in
            if case let .success(response) = result, response.isStudent {
                self.handleStudentApiId(validatedApiId: response.validatedApiId)
            }
        }
    }
    
    private func handleStudentApiId(validatedApiId: String?) {
        guard let apiId = validatedApiId else {
            return
        }
        
        service.saveStudentValidatedApiId(apiId)
    }
}
