struct RegisterStudentPromoCodeModel {
    let image: UIImage
    let title: NSAttributedString
    let description: NSAttributedString
    let benefits: [String]?
}
