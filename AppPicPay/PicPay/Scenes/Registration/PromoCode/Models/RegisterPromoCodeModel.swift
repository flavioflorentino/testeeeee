struct RegisterPromoCodeModel {
    let image: UIImage
    let title: NSAttributedString
    let description: NSAttributedString
    let regulationButtonText: NSAttributedString
    let link: String?
    
    var hasLink: Bool {
        !link.isEmpty
    }
}
