import UI

private extension RegisterPromoCodeHeaderView.Layout {
    static let containerSpacing: CGFloat = 20
    static let titleFontSize: CGFloat = 16
    static let informationFontSize: CGFloat = 14
    static let numberOfLines = 0
}

final class RegisterPromoCodeHeaderView: UIView, ViewConfiguration {
    fileprivate struct Layout {}
    
    private let image: UIImage
    private let title: NSAttributedString
    private let information: NSAttributedString
    
    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = Layout.containerSpacing
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var imageView = UIImageView(image: image)
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: Layout.titleFontSize)
        label.textColor = Palette.ppColorGrayscale500.color
        label.textAlignment = .center
        label.numberOfLines = Layout.numberOfLines
        label.attributedText = title
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var informationLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: Layout.informationFontSize)
        label.textColor = Palette.ppColorGrayscale500.color
        label.textAlignment = .center
        label.numberOfLines = Layout.numberOfLines
        label.attributedText = information
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    init(image: UIImage, title: NSAttributedString, information: NSAttributedString) {
        self.image = image
        self.title = title
        self.information = information
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(containerStackView)
        containerStackView.addArrangedSubviews(imageView, titleLabel, informationLabel)
    }
    
    func setupConstraints() {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.constraintAllEdges(from: containerStackView, to: self)
    }
}
