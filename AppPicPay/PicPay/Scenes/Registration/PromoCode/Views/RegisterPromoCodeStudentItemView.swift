import UI

private extension RegisterPromoCodeStudentItemView.Layout {
    static let spacing: CGFloat = 16
    static let titleFontSize: CGFloat = 15
    static let imageSize: CGFloat = 24
    static let numberOfLines = 0
}

final class RegisterPromoCodeStudentItemView: UIView, ViewConfiguration {
    fileprivate struct Layout {}
    
    private let title: String
    
    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Layout.spacing
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var imageView = UIImageView(image: Assets.Icons.icoGreenCheck.image)
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = title
        label.font = .systemFont(ofSize: Layout.titleFontSize, weight: .medium)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = Layout.numberOfLines
        return label
    }()
    
    init(title: String) {
        self.title = title
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraints() {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.constraintAllEdges(from: containerStackView, to: self)
    
        imageView.layout {
            $0.width == Layout.imageSize
            $0.height == Layout.imageSize
        }
    }
    
    func buildViewHierarchy() {
        addSubview(containerStackView)
        containerStackView.addArrangedSubview(imageView)
        containerStackView.addArrangedSubview(titleLabel)
    }
}
