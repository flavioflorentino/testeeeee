import AnalyticsModule

protocol RegisterPromoCodeViewModelInputs: AnyObject {
    func viewDidLoad()
    func trackScreen()
    func understandAction()
    func openRegulationAction()
}

final class RegisterPromoCodeViewModel {
    private let presenter: RegisterPromoCodePresenting
    private let validPromoCode: ValidPromoCode
    private let type: RegistrationEvent.ValidationCodeOptionType

    init(presenter: RegisterPromoCodePresenting, validPromoCode: ValidPromoCode) {
        self.presenter = presenter
        self.validPromoCode = validPromoCode
        self.type = validPromoCode.type == .mgm ? .mgm : .promo
    }
}

extension RegisterPromoCodeViewModel: RegisterPromoCodeViewModelInputs {
    func viewDidLoad() {
        presenter.buildModel(validPromoCode.data)
    }
    
    func trackScreen() {
        Analytics.shared.log(RegistrationEvent.validationCode(type, action: nil))
    }
    
    func understandAction() {
        Analytics.shared.log(RegistrationEvent.validationCode(type, action: .understand))
        presenter.didNextStep(action: .createStudentAccount(validPromoCode: validPromoCode))
    }
    
    func openRegulationAction() {
        guard
            let faqLink = validPromoCode.data.faqLink,
            let validURL = URL(string: faqLink)
            else {
                return
        }
        
        Analytics.shared.log(RegistrationEvent.validationCode(type, action: .regulation))
        presenter.didNextStep(action: .openRegulation(validURL))
    }
}
