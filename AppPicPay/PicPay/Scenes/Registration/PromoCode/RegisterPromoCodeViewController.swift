import UI
import UIKit

private extension RegisterPromoCodeViewController.Layout {
    static let defaultMargin: CGFloat = 24
    static let stackViewSpacing: CGFloat = 16
    static let actionButtonMargin: CGFloat = 16
    static let actionButtonHeight: CGFloat = 48
    static let regulationFontSize: CGFloat = 12
}

final class RegisterPromoCodeViewController: ViewController<RegisterPromoCodeViewModelInputs, UIView> {
    fileprivate struct Layout {}
    
    private lazy var headerContainerView = UIView()
    private lazy var buttonContainerView = UIView()
    private lazy var contentView = UIStackView()
    
    private lazy var buttonsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Layout.stackViewSpacing
        return stackView
    }()
    
    private lazy var actionButton: UIPPButton = {
        let button = UIPPButton()
        button.setTitle(PromoCodeLocalizable.understand.text, for: .normal)
        button.configureButtonCta()
        button.cornerRadius = Layout.actionButtonHeight / 2
        button.addTarget(self, action: #selector(understandAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var regulationButton: UIButton = {
        let button = UIButton(type: .system)
        button.titleLabel?.font = .systemFont(ofSize: Layout.regulationFontSize, weight: .semibold)
        button.tintColor = Palette.ppColorGrayscale400.color
        button.addTarget(self, action: #selector(openRegulationAction), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad()
        setupNavBarColor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        viewModel.trackScreen()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(headerContainerView)
        view.addSubview(buttonContainerView)
        headerContainerView.addSubview(contentView)
        buttonContainerView.addSubview(buttonsStackView)
        buttonsStackView.addArrangedSubviews(actionButton, regulationButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale100.color
        regulationButtonIsEnabled(false)
    }
    
    private func setupNavBarColor() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale100.color
        navigationController?.navigationBar.tintColor = Palette.ppColorGrayscale400.color
    }
    
    override func setupConstraints() {
        headerContainerView.layout {
            $0.top == view.compatibleSafeAreaLayoutGuide.topAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
        }
        
        buttonContainerView.layout {
            $0.top == headerContainerView.bottomAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
            $0.bottom == view.compatibleSafeAreaLayoutGuide.bottomAnchor - Layout.defaultMargin
        }
        
        contentView.layout {
            $0.leading == headerContainerView.leadingAnchor + Layout.defaultMargin
            $0.trailing == headerContainerView.trailingAnchor - Layout.defaultMargin
            $0.centerY == headerContainerView.centerYAnchor
        }
        
        buttonsStackView.layout {
            $0.top == buttonContainerView.topAnchor
            $0.leading == buttonContainerView.leadingAnchor + Layout.actionButtonMargin
            $0.trailing == buttonContainerView.trailingAnchor - Layout.actionButtonMargin
            $0.bottom == buttonContainerView.bottomAnchor
        }
        
        actionButton.layout {
            $0.height == Layout.actionButtonHeight
        }
    }
    
    private func regulationButtonIsEnabled(_ isEnabled: Bool) {
        regulationButton.isEnabled = isEnabled
        regulationButton.isHidden = !isEnabled
    }
    
    @objc
    private func understandAction() {
        viewModel.understandAction()
    }
    
    @objc
    private func openRegulationAction() {
        viewModel.openRegulationAction()
    }
}

// MARK: View Model Outputs
extension RegisterPromoCodeViewController: RegisterPromoCodeDisplay {
    func displayModel(_ model: RegisterPromoCodeModel) {
        let headerView = RegisterPromoCodeHeaderView(image: model.image, title: model.title, information: model.description)
        contentView.addArrangedSubview(headerView)
        regulationButton.setAttributedTitle(model.regulationButtonText, for: .normal)
        regulationButtonIsEnabled(model.hasLink)
    }
}
