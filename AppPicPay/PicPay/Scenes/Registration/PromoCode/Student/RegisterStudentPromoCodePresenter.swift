import UI

protocol RegisterStudentPromoCodePresenting: AnyObject {
    var viewController: RegisterStudentPromoCodeDisplay? { get set }
    func buildModel(_ response: PromoCodeValidationData)
    func didNextStep(action: RegisterStudentPromoCodeAction)
}

final class RegisterStudentPromoCodePresenter: RegisterStudentPromoCodePresenting {
    private enum Layout {
        static let fontSize: CGFloat = 14
    }

    private let coordinator: RegisterStudentPromoCodeCoordinating
    weak var viewController: RegisterStudentPromoCodeDisplay?

    init(coordinator: RegisterStudentPromoCodeCoordinating) {
        self.coordinator = coordinator
    }
    
    func buildModel(_ response: PromoCodeValidationData) {
        let image = Assets.Icons.iconUniversitario.image
        
        let title = NSMutableAttributedString(string: response.title)
        title.textColor(text: PromoCodeLocalizable.student.text, color: Palette.ppColorBranding300.color)
        
        let description = NSMutableAttributedString(string: response.description)
        description.font(text: PromoCodeLocalizable.uniAccount.text, font: .boldSystemFont(ofSize: Layout.fontSize))
        description.textColor(text: PromoCodeLocalizable.uniAccount.text, color: Palette.ppColorNeutral500.color.withCustomDarkColor(Palette.ppColorNeutral300.color))
        if let promoAmount = response.promoAmount {
            description.textColor(text: promoAmount, color: Palette.white.color.withCustomDarkColor(Palette.black.color))
            description.textBackgroundColor(text: promoAmount, color: Palette.ppColorBranding300.color)
        }
        
        let model = RegisterStudentPromoCodeModel(
            image: image,
            title: title,
            description: description,
            benefits: response.benefits
        )
        viewController?.displayModel(model)
    }
    
    func didNextStep(action: RegisterStudentPromoCodeAction) {
        coordinator.perform(action: action)
    }
}
