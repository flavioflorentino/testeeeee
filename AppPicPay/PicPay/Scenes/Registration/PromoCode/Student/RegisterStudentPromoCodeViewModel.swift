import AnalyticsModule

protocol RegisterStudentPromoCodeViewModelInputs: AnyObject {
    func viewDidLoad()
    func trackScreen()
    func createStudentAccountAction()
}

final class RegisterStudentPromoCodeViewModel {
    private let presenter: RegisterStudentPromoCodePresenting
    private let validPromoCode: ValidPromoCode

    init(presenter: RegisterStudentPromoCodePresenting, validPromoCode: ValidPromoCode) {
        self.presenter = presenter
        self.validPromoCode = validPromoCode
    }
}

extension RegisterStudentPromoCodeViewModel: RegisterStudentPromoCodeViewModelInputs {
    func viewDidLoad() {
        presenter.buildModel(validPromoCode.data)
    }
    
    func trackScreen() {
        Analytics.shared.log(RegistrationEvent.validationCode(.uniAccount, action: nil))
    }
    
    func createStudentAccountAction() {
        Analytics.shared.log(RegistrationEvent.validationCode(.uniAccount, action: .createUniAccount))
        presenter.didNextStep(action: .createStudentAccount(validPromoCode: validPromoCode))
    }
}
