import Foundation

enum RegisterStudentPromoCodeFactory {
    static func make(with validPromoCode: ValidPromoCode, delegate: RegisterStudentPromoCodeCoordinatorDelegate) -> RegisterStudentPromoCodeViewController {
        let coordinator: RegisterStudentPromoCodeCoordinating = RegisterStudentPromoCodeCoordinator()
        let presenter: RegisterStudentPromoCodePresenting = RegisterStudentPromoCodePresenter(coordinator: coordinator)
        let viewModel = RegisterStudentPromoCodeViewModel(presenter: presenter, validPromoCode: validPromoCode)
        let viewController = RegisterStudentPromoCodeViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
