import UI
import UIKit

private extension RegisterStudentPromoCodeViewController.Layout {
    static let defaultMargin: CGFloat = 24
    static let actionButtonMargin: CGFloat = 16
    static let actionButtonHeight: CGFloat = 48
    static let stackViewSpacing: CGFloat = 20
    static let pointStart = CGPoint(x: 12, y: 0)
}

final class RegisterStudentPromoCodeViewController: ViewController<RegisterStudentPromoCodeViewModelInputs, UIView> {
    fileprivate struct Layout {}
    
    private lazy var headerContainerView = UIView()
    private lazy var buttonContainerView = UIView()
    
    private lazy var contentView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Layout.stackViewSpacing
        return stackView
    }()
    
    private lazy var itemsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Layout.stackViewSpacing
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var actionButton: UIPPButton = {
        let button = UIPPButton()
        button.setTitle(PromoCodeLocalizable.createUniAccount.text, for: .normal)
        button.configureButtonCta()
        button.cornerRadius = Layout.actionButtonHeight / 2
        button.addTarget(self, action: #selector(createStudentAccountAction), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad()
        setupNavBarColor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        viewModel.trackScreen()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        drawDottedLine()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(headerContainerView)
        view.addSubview(buttonContainerView)
        headerContainerView.addSubview(contentView)
        buttonContainerView.addSubview(actionButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale100.color
    }
    
    private func setupNavBarColor() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale100.color
        navigationController?.navigationBar.tintColor = Palette.ppColorGrayscale400.color
    }
    
    override func setupConstraints() {
        headerContainerView.layout {
            $0.top == view.compatibleSafeAreaLayoutGuide.topAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
        }
        
        buttonContainerView.layout {
            $0.top == headerContainerView.bottomAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
            $0.bottom == view.compatibleSafeAreaLayoutGuide.bottomAnchor - Layout.defaultMargin
        }
        
        contentView.layout {
            $0.leading == headerContainerView.leadingAnchor + Layout.defaultMargin
            $0.trailing == headerContainerView.trailingAnchor - Layout.defaultMargin
            $0.centerY == headerContainerView.centerYAnchor
        }
        
        actionButton.layout {
            $0.top == buttonContainerView.topAnchor
            $0.leading == buttonContainerView.leadingAnchor + Layout.actionButtonMargin
            $0.trailing == buttonContainerView.trailingAnchor - Layout.actionButtonMargin
            $0.height == Layout.actionButtonHeight
            $0.bottom == buttonContainerView.bottomAnchor
        }
    }
    
    @objc
    private func createStudentAccountAction() {
        viewModel.createStudentAccountAction()
    }
    
    private func drawDottedLine() {
        let pointStart = Layout.pointStart
        let pointEnd = CGPoint(x: Layout.pointStart.x, y: itemsStackView.frame.height)
        let convertedPointStart = itemsStackView.convert(pointStart, to: view)
        let convertedPointEnd = itemsStackView.convert(pointEnd, to: view)
        view.drawDottedLine(start: convertedPointStart, end: convertedPointEnd, view: view)
    }
    
    private func configureStudentItem(_ title: String) {
        itemsStackView.addArrangedSubview(RegisterPromoCodeStudentItemView(title: title))
    }
}

// MARK: View Model Outputs
extension RegisterStudentPromoCodeViewController: RegisterStudentPromoCodeDisplay {
    func displayModel(_ model: RegisterStudentPromoCodeModel) {
        let headerView = RegisterPromoCodeHeaderView(
            image: model.image,
            title: model.title,
            information: model.description
        )
        contentView.addArrangedSubview(headerView)
        contentView.addArrangedSubview(itemsStackView)
        model.benefits?.forEach { title in
            configureStudentItem(title)
        }
    }
}
