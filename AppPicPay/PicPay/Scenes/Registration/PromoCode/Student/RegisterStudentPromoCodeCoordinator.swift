import UIKit

protocol RegisterStudentPromoCodeCoordinatorDelegate: AnyObject {
    func presentUserRegistration(with promoCode: ValidPromoCode)
}

enum RegisterStudentPromoCodeAction {
    case createStudentAccount(validPromoCode: ValidPromoCode)
}

protocol RegisterStudentPromoCodeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: RegisterStudentPromoCodeCoordinatorDelegate? { get set }
    func perform(action: RegisterStudentPromoCodeAction)
}

final class RegisterStudentPromoCodeCoordinator: RegisterStudentPromoCodeCoordinating {
    private var childCoordinator: Coordinator?
    
    weak var viewController: UIViewController?
    weak var delegate: RegisterStudentPromoCodeCoordinatorDelegate?
    
    func perform(action: RegisterStudentPromoCodeAction) {
        guard case let .createStudentAccount(validPromoCode) = action else {
            return
        }
        delegate?.presentUserRegistration(with: validPromoCode)
    }
}
