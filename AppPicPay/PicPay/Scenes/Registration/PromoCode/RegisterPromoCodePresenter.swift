import UI

protocol RegisterPromoCodePresenting: AnyObject {
    var viewController: RegisterPromoCodeDisplay? { get set }
    func buildModel(_ response: PromoCodeValidationData)
    func didNextStep(action: RegisterPromoCodeAction)
}

final class RegisterPromoCodePresenter: RegisterPromoCodePresenting {
    private enum Layout {
        static let fontSize: CGFloat = 14
        static let lineSpace: CGFloat = 5
    }
    
    private let coordinator: RegisterPromoCodeCoordinating
    weak var viewController: RegisterPromoCodeDisplay?

    init(coordinator: RegisterPromoCodeCoordinating) {
        self.coordinator = coordinator
    }
    
    func buildModel(_ response: PromoCodeValidationData) {
        let image = Assets.SocialOnboarding.onboardingSocialPopup.image
        
        let title = NSMutableAttributedString(string: response.title)
        if let promoAmount = response.promoAmount {
            title.textColor(text: promoAmount, color: Palette.ppColorBranding300.color)
        }
        
        let description = NSMutableAttributedString(string: response.description)
        if let referralName = response.referralName {
            description.font(text: referralName, font: .boldSystemFont(ofSize: Layout.fontSize))
        }
        description.font(text: response.referralCode, font: .boldSystemFont(ofSize: Layout.fontSize))
        description.paragraph(aligment: .center, lineSpace: Layout.lineSpace)
        
        let regulationButtonText = NSAttributedString(string: "<u>\(PromoCodeLocalizable.regulation.text)</u>").underlinefy()
        
        let model = RegisterPromoCodeModel(
            image: image,
            title: title,
            description: description,
            regulationButtonText: regulationButtonText,
            link: response.faqLink
        )
        viewController?.displayModel(model)
    }
    
    func didNextStep(action: RegisterPromoCodeAction) {
        coordinator.perform(action: action)
    }
}
