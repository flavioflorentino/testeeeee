import UIKit

protocol RegisterPromoCodeCoordinatorDelegate: AnyObject {
    func presentUserRegistration(with promoCode: ValidPromoCode)
}

enum RegisterPromoCodeAction {
    case createStudentAccount(validPromoCode: ValidPromoCode)
    case openRegulation(URL)
}

protocol RegisterPromoCodeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: RegisterPromoCodeCoordinatorDelegate? { get set }
    func perform(action: RegisterPromoCodeAction)
}

final class RegisterPromoCodeCoordinator: RegisterPromoCodeCoordinating {
    private var childCoordinator: Coordinator?
    
    weak var viewController: UIViewController?
    weak var delegate: RegisterPromoCodeCoordinatorDelegate?
    
    func perform(action: RegisterPromoCodeAction) {
        switch action {
        case .createStudentAccount(let validPromoCode):
            delegate?.presentUserRegistration(with: validPromoCode)
        case .openRegulation(let url):
            let webview = WebViewFactory.make(with: url)
            viewController?.navigationController?.present(UINavigationController(rootViewController: webview), animated: true)
        }
    }
}
