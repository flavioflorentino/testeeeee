import Foundation

enum RegisterPromoCodeFactory {
    static func make(
        with validPromoCode: ValidPromoCode,
        delegate: RegisterPromoCodeCoordinatorDelegate
    ) -> RegisterPromoCodeViewController {
        let coordinator: RegisterPromoCodeCoordinating = RegisterPromoCodeCoordinator()
        let presenter: RegisterPromoCodePresenting = RegisterPromoCodePresenter(coordinator: coordinator)
        let viewModel = RegisterPromoCodeViewModel(presenter: presenter, validPromoCode: validPromoCode)
        let viewController = RegisterPromoCodeViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
