import Core
import FeatureFlag
import Registration
import UI

protocol RegistrationFlowCoordinatorDelegate: AnyObject {
    func moveToLogin()
    func moveToPreviousRegistrationStep()
}

protocol RegistrationLoadingDisplay: UIViewController {
    func showLoading()
    func hideLoading()
}

final class RegistrationFlowCoordinator: Coordinator {
    typealias Dependencies = HasFeatureManager
    
    var currentCoordinator: Coordinator?
    var viewController: UIViewController?
    
    private let dependencies: Dependencies
    private let navigationController: UINavigationController
    private let defaultStepSequence: [RegistrationStep] = [.name, .phone, .email, .password, .document]
    private let addConsumerHelper: RegistrationAddConsumerHelperContract
    private let registrationCacheHelper: RegistrationCacheHelperContract
    private let studentAvailabilityInteractor: StudentAvailabilityInteractorContract
    private var registrationModel = RegistrationModel()
    private var registrationStatistics = RegistrationStatisticsModel()
    private var interruptedStep: RegistrationStep?
    private var registrationAccountSetupContract: RegistrationAccountSetupContract
    
    private lazy var stepSequence: [RegistrationStep] = {
        guard
            let dynamicStepSequence = dependencies.featureManager.object(.dynamicRegistrationStepsSequence, type: [RegistrationStep].self),
            dynamicStepSequence.isNotEmpty
        else {
            return defaultStepSequence
        }
        return dynamicStepSequence
    }()
    
    private var currentStep: RegistrationStep?
    
    private var isLastStep: Bool {
        guard
            let currentStep = currentStep,
            let lastStep = stepSequence.last
        else {
            return false
        }
        return currentStep == lastStep
    }
    
    private var isFirstStep: Bool {
        guard
            let currentStep = currentStep,
            let firstStep = stepSequence.first
        else {
            return false
        }
        return currentStep == firstStep
    }
    
    private var nextStep: RegistrationStep? {
        guard
            let currentStep = currentStep,
            let currentIndex = stepSequence.firstIndex(of: currentStep),
            currentIndex + 1 < stepSequence.count
        else {
            return nil
        }
        return stepSequence[currentIndex + 1]
    }
    
    private var previousStep: RegistrationStep? {
        guard
            let currentStep = currentStep,
            let currentIndex = stepSequence.firstIndex(of: currentStep),
            currentIndex - 1 >= .zero
        else {
            return nil
        }
        return stepSequence[currentIndex - 1]
    }
    
    private var shouldMoveToPreviousRegistrationStep: Bool {
        if let lastStep = previousStep, lastStep == .phone {
            return false
        }
        
        return true
    }
    
    init(
        dependencies: Dependencies = DependencyContainer(),
        addConsumerHelper: RegistrationAddConsumerHelperContract = RegistrationAddConsumerFactory.make(),
        registrationCacheHelper: RegistrationCacheHelperContract = RegistrationCacheHelper(),
        studentAvailabilityInteractor: StudentAvailabilityInteractorContract = StudentAvailabilityFactory.make(),
        navigationController: UINavigationController,
        cachedRegistration: RegistrationCache? = nil,
        registrationAccountSetupContract: RegistrationAccountSetupContract = RegistrationAccountSetupFactory.make(),
        promoCode: String? = nil
    ) {
        self.dependencies = dependencies
        self.addConsumerHelper = addConsumerHelper
        self.registrationCacheHelper = registrationCacheHelper
        self.studentAvailabilityInteractor = studentAvailabilityInteractor
        self.navigationController = navigationController
        self.registrationAccountSetupContract = registrationAccountSetupContract
        
        navigationController.interactivePopGestureRecognizer?.isEnabled = false
        
        guard let cached = cachedRegistration else {
            registrationModel.promoCode = promoCode
            return
        }
        stepSequence = cached.stepSequence
        registrationModel = cached.model
        registrationStatistics = cached.statistics
        interruptedStep = cached.interruptedStep
    }
    
    func start() {
        RegistrationCacheHelper().cleanCachedData()
        setUpTemporaryReferences()
        
        guard let initialStep = stepSequence.first else {
            return
        }
        presentRegistrationStep(initialStep, isFirstStep: true)
    }
    
    func start(onTopOf viewController: UIViewController) {
        setUpTemporaryReferences()
        
        if let interruptedStep = interruptedStep {
            restoreSavedSteps(until: interruptedStep, onTopOf: viewController)
            return
        }
        
        guard let initialStep = stepSequence.first else {
            return
        }
        RegistrationCacheHelper().cleanCachedData()
        currentStep = initialStep
        
        let stepController = instatiateViewController(for: initialStep, showTermsOfService: true)
        navigationController.setViewControllers([viewController, stepController], animated: true)
    }
    
    private func restoreSavedSteps(until interruptedStep: RegistrationStep, onTopOf viewController: UIViewController) {
        var viewControllers = [viewController]
        let stepCount = stepSequence.count
        for i in 0..<stepCount {
            let stepController = instatiateViewController(for: stepSequence[i], showTermsOfService: i == 0)
            viewControllers.append(stepController)
            
            if stepSequence[i] == interruptedStep {
                break
            }
        }
        currentStep = interruptedStep
        navigationController.setViewControllers(viewControllers, animated: true)
    }
    
    private func presentRegistrationStep(_ step: RegistrationStep, isFirstStep: Bool = false) {
        let controller = instatiateViewController(for: step, showTermsOfService: isFirstStep)
        navigationController.pushViewController(controller, animated: true)
        currentStep = step
    }
    
    private func instatiateViewController(for step: RegistrationStep, showTermsOfService: Bool = false) -> UIViewController {
        switch step {
        case .name:
            return RegistrationNameFactory.make(
                delegate: self,
                firstName: registrationModel.firstName,
                lastName: registrationModel.lastName,
                showTermsOfService: showTermsOfService
            )
        case .phone:
            return RegistrationPhoneNumberFactory.make(
                delegate: self,
                ddd: registrationModel.ddd,
                number: registrationModel.phoneNumber,
                showTermsOfService: showTermsOfService
            )
        case .email:
            return RegistrationEmailFactory.make(
                delegate: self,
                email: registrationModel.email,
                showTermsOfService: showTermsOfService
            )
        case .password:
            return RegistrationPasswordFactory.make(
                delegate: self,
                password: registrationModel.password,
                showTermsOfService: showTermsOfService
            )
        case .document:
            let model = RegistrationDocumentModel(
                cpf: registrationModel.cpf,
                dateOfBirth: registrationModel.dateOfBirth,
                name: registrationModel.firstName,
                showTermsOfService: showTermsOfService
            )
            return RegistrationDocumentFactory.make(delegate: self, model: model)
        }
    }
    
    private func finishRegistration() {
        var lastStepViewController: RegistrationLoadingDisplay?
        if let lastViewController = navigationController.viewControllers.last as? RegistrationLoadingDisplay {
            lastStepViewController = lastViewController
        }
        
        lastStepViewController?.showLoading()

        if dependencies.featureManager.isActive(.registrationCompliance) {
            addConsumerWithCompliance(lastStepViewController: lastStepViewController)
        } else {
            addConsumer(lastStepViewController: lastStepViewController)
        }
    }
    
    private func addConsumer(lastStepViewController: RegistrationLoadingDisplay?) {
        addConsumerHelper.addConsumer(
            with: registrationModel,
            statistics: registrationStatistics
        ) { [weak self] result in
            lastStepViewController?.hideLoading()
            
            guard let self = self else {
                return
            }
            
            switch result {
            case .success:
                self.pushUsernameStep()
                self.releaseTemporaryReferences()
            case .failure(let error):
                AlertMessage.showCustomAlertWithError(error, controller: self.navigationController)
            }
        }
    }
    
    private func moveToNextRegistrationStep() {
        guard
            !isLastStep,
            let nextStep = nextStep
        else {
            finishRegistration()
            return
        }
        
        if dependencies.featureManager.isActive(.continueRegistration) {
            registrationCacheHelper.storeFinishedSteps(
                model: registrationModel,
                statistics: registrationStatistics,
                stepSequence: stepSequence,
                interruptedStep: nextStep
            )
        }
        
        presentRegistrationStep(nextStep)
    }
    
    private func moveToLoginScreen() {
        navigationController.popToRootViewController(animated: true)
        releaseTemporaryReferences()
        
        guard let signUpController = navigationController.viewControllers.first as? SignUpViewController else {
            return
        }
        signUpController.tapLoginButton()
    }
    
    private func pushUsernameStep() {
        let controller = RegistrationUsernameFactory.make(promoCode: registrationModel.promoCode)
        navigationController.setViewControllers([controller], animated: true)
    }
    
    private func pushRegistrationComplianceStatus(result: RegistrationComplianceResult?) {
        guard let result = result else {
            self.pushUsernameStep()
            self.releaseTemporaryReferences()
            return
        }
        
        let coordinator = RegistrationComplianceFlowCoordinator(with: navigationController)
        coordinator.startCompliance(status: RegistrationComplianceStatus.status(with: result))
    }
    
    private func setUpTemporaryReferences() {
        SessionManager.sharedInstance.currentCoordinator = self
    }
    
    private func releaseTemporaryReferences() {
        SessionManager.sharedInstance.currentCoordinator = nil
    }
}

extension RegistrationFlowCoordinator {
    private func setupAccount() {
        guard
            let document = registrationModel.cpf,
            let birthdate = registrationModel.dateOfBirth,
            let firstName = registrationModel.firstName,
            let lastName = registrationModel.lastName,
            let areaCode = registrationModel.ddd,
            let phone = registrationModel.phoneNumber else {
            return
        }
        
        let model = RegistrationAccountSetupModel(
            document: document,
            birthdate: birthdate,
            phone: phone,
            areaCode: areaCode,
            firstName: firstName,
            lastName: lastName
        )
        
        registrationAccountSetupContract.setupAccount(model: model)
    }
    
    private func addConsumerWithCompliance(lastStepViewController: RegistrationLoadingDisplay?) {
        addConsumerHelper.addComplianceConsumer(
            with: registrationModel,
            statistics: registrationStatistics) { [weak self] result in
            lastStepViewController?.hideLoading()
            guard let self = self else { return }

            switch result {
            case .success(let model):
                guard let step = model.step else {
                    self.pushUsernameStep()
                    self.releaseTemporaryReferences()
                    return
                }
                self.pushRegistrationComplianceStatus(result: step)
            case .failure(let error):
                AlertMessage.showCustomAlertWithError(error, controller: self.navigationController)
            }
        }
    }
    
    private func pushRegistrationComplianceStatus(result: RegistrationComplianceResult) {
        RegistrationCacheHelper().cleanCachedData()
        releaseTemporaryReferences()
        
        let coordinator = RegistrationComplianceFlowCoordinator(with: navigationController)
        
        guard result == .inconsistency,
              let firstName = registrationModel.firstName,
              let lastName = registrationModel.lastName,
              let birthdate = registrationModel.dateOfBirth,
              let cpf = registrationModel.cpf else {
            coordinator.startCompliance(status: RegistrationComplianceStatus.status(with: result))
            return
        }
        
        coordinator.startReview(model: RegistrationCompReviewModel(
                                    firstName: firstName,
                                    lastName: lastName,
                                    birthdate: birthdate,
                                    cpf: cpf)
        )
    }
}

// Check Student Availability
extension RegistrationFlowCoordinator {
    private func checkConsumerStudentAvailability() {
        guard let cpf = registrationModel.cpf, let dateOfBirth = registrationModel.dateOfBirth else {
            return
        }
        
        studentAvailabilityInteractor.checkConsumerStudentAvailability(cpf: cpf, dateOfBirth: dateOfBirth)
    }
}

// Back button alert methods
extension RegistrationFlowCoordinator {
    private func showBackToPhoneStepAlert() {
        let alert = Alert(title: nil, text: RegisterLocalizable.alertTextBackToPhoneStep.text)
        let confirmButton = Button(title: DefaultLocalizable.btOk.text, type: .cta) { [weak self] popupController, _ in
            popupController.dismiss(animated: true) {
                self?.popToPhoneStepViewController()
            }
        }
        let cancelButton = Button(title: DefaultLocalizable.btCancel.text, type: .clean, action: .close)
        alert.buttons = [confirmButton, cancelButton]
        
        AlertMessage.showAlert(alert, controller: navigationController)
    }
    
    private func popToPhoneStepViewController() {
        let phoneViewController = navigationController.viewControllers.first {
            $0.isKind(of: RegistrationPhoneNumberViewController.self)
        }
        guard let viewController = phoneViewController else {
            return
        }
        navigationController.popToViewController(viewController, animated: true)
        currentStep = .phone
    }
}

extension RegistrationFlowCoordinator: RegistrationFlowCoordinatorDelegate {
    func moveToLogin() {
        moveToLoginScreen()
    }
    
    func moveToPreviousRegistrationStep() {
        guard shouldMoveToPreviousRegistrationStep else {
            showBackToPhoneStepAlert()
            return
        }
        
        if isFirstStep {
            releaseTemporaryReferences()
        }
        
        currentStep = previousStep
        
        navigationController.popViewController(animated: true)
    }
}

extension RegistrationFlowCoordinator: RegistrationNameCoordinatorDelegate {
    func moveToNextStep(withFirstName firstName: String, lastName: String) {
        registrationModel.firstName = firstName
        registrationModel.lastName = lastName
        moveToNextRegistrationStep()
    }
}

extension RegistrationFlowCoordinator: RegistrationPhoneCodeCoordinatorDelegate {
    func moveToNextStep(withDdd ddd: String, phoneNumber: String) {
        registrationModel.ddd = ddd
        registrationModel.phoneNumber = phoneNumber
        moveToNextRegistrationStep()
        
        if dependencies.featureManager.isActive(.registrationCompliance) {
            setupAccount()
        }
    }
}

extension RegistrationFlowCoordinator: RegistrationDocumentCoordinatorDelegate {
    func moveToNextStep(withCpf cpf: String, dateOfBirth: String, invalidCpfCounter: Int, copyPasteCpf: Bool) {
        registrationModel.cpf = cpf
        registrationModel.dateOfBirth = dateOfBirth
        registrationStatistics.invalidCpfCounter = invalidCpfCounter
        registrationStatistics.copyPasteCpf = copyPasteCpf
        checkConsumerStudentAvailability()
        moveToNextRegistrationStep()
    }
}

extension RegistrationFlowCoordinator: RegistrationPasswordCoordinatorDelegate {
    func moveToNextStep(withPassword password: String) {
        registrationModel.password = password
        moveToNextRegistrationStep()
    }
}

extension RegistrationFlowCoordinator: RegistrationEmailCoordinatorDelegate {
    func moveToNextStep(withEmail email: String, invalidEmailCounter: Int) {
        registrationModel.email = email
        registrationStatistics.invalidEmailCounter = invalidEmailCounter
        moveToNextRegistrationStep()
    }
}
