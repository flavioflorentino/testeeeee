struct AddConsumerResponse: Decodable, NestedDecodable {
    let token: String
    let mgm: String?
}

struct RegisterResponse<T: Decodable>: Decodable {
    enum CodingKeys: String, CodingKey {
        case data
        case error = "Error"
    }
    
    let data: T?
    let error: RegisterError?
}

struct PhoneNumberCodeResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case smsDelay, callDelay
        case whatsAppDelay = "whatsappDelay"
        case callEnabled = "callEnable"
    }
    
    let smsDelay: Int?
    let whatsAppDelay: Int?
    let callEnabled: Bool
    let callDelay: Int
}

struct PhoneNumberValidationResponse {
    let ddd: String
    let number: String
}

extension PhoneNumberValidationResponse: Decodable {
    private enum CodingKeys: String, CodingKey {
        case PhoneNumberVerifications
        case areaCode
        case number
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let nestedContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .PhoneNumberVerifications)
        let ddd = try nestedContainer.decode(String.self, forKey: .areaCode)
        let number = try nestedContainer.decode(String.self, forKey: .number)
        
        self.init(ddd: ddd, number: number)
    }
}

struct RegisterNameResponse: Decodable {
    let isValidName: Bool
    let name: String
}

struct RegisterError: Decodable, LocalizedError {
    enum CodingKeys: String, CodingKey {
        case id
        case descriptionPt
    }
    
    let id: Int
    let description: String
    
    var errorDescription: String? {
        description
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let idString = try? container.decode(String.self, forKey: .id) {
            id = Int(idString) ?? 0
        } else {
            id = try container.decode(Int.self, forKey: .id)
        }
        
        description = try container.decode(String.self, forKey: .descriptionPt)
    }
}

struct StudentAvailabilityResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case isStudent = "validate_student"
        case validatedApiId = "validate_api_id"
    }

    let isStudent: Bool
    let validatedApiId: String?
}
