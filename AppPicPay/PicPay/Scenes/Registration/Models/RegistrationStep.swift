enum RegistrationStep: String, Codable {
    case name
    case phone
    case email
    case password
    case document = "cpf_birthday"
}
