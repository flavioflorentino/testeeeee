struct RegistrationModel: Codable {
    var firstName: String?
    var lastName: String?
    var email: String?
    var ddd: String?
    var phoneNumber: String?
    var phoneCode: String?
    var cpf: String?
    var dateOfBirth: String?
    var password: String?
    var username: String?
    var promoCode: String?
}
