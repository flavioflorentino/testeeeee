struct RegistrationStatisticsModel: Codable {
    let beginRegistrationTime: CFTimeInterval = CACurrentMediaTime()
    var invalidEmailCounter = 0
    var invalidCpfCounter = 0
    var copyPasteCpf = false
}
