struct DocumentFeature: Decodable {
    let title: String
    let text: String
    let buttonTitle: String
}
