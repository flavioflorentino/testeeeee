import Foundation
import Registration

enum RegistrationComplianceTokenType: String, Decodable {
    case temporary
    case permanent
}

struct RegistrationComplianceModel: Decodable {
    let token: String
    let mgm: String?
    let tokenType: RegistrationComplianceTokenType?
    let step: RegistrationComplianceResult?
}
