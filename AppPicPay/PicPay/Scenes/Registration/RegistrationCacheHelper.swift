import AnalyticsModule
import Core
import FeatureFlag

struct RegistrationCache: Codable {
    var model: RegistrationModel
    var statistics: RegistrationStatisticsModel
    var stepSequence: [RegistrationStep]
    var interruptedStep: RegistrationStep
}

protocol RegistrationCacheHelperContract {
    var cache: RegistrationCache? { get }
    func storeFinishedSteps(
        model: RegistrationModel,
        statistics: RegistrationStatisticsModel,
        stepSequence: [RegistrationStep],
        interruptedStep: RegistrationStep
    )
    func cleanCachedData()
}

final class RegistrationCacheHelper: RegistrationCacheHelperContract {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    private let encoder = PropertyListEncoder()
    private let decoder = PropertyListDecoder()
    
    var cache: RegistrationCache? {
        guard
            let registrationModel = KVStore().objectForKey(.unfinishedRegistrationCache) as? Data,
            let decodedRegistrationCache = try? decoder.decode(RegistrationCache.self, from: registrationModel),
            canRestoreCachedRegistration(decodedRegistrationCache)
            else {
                return nil
        }
        return decodedRegistrationCache
    }
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func storeFinishedSteps(
        model: RegistrationModel,
        statistics: RegistrationStatisticsModel,
        stepSequence: [RegistrationStep],
        interruptedStep: RegistrationStep
    ) {
        let cachedRegistration = RegistrationCache(
            model: model,
            statistics: statistics,
            stepSequence: stepSequence,
            interruptedStep: interruptedStep
        )
        guard let encodedModel = try? encoder.encode(cachedRegistration) else {
            return
        }
        KVStore().set(value: encodedModel, with: .unfinishedRegistrationCache)
    }
    
    func cleanCachedData() {
        KVStore().removeObjectFor(.unfinishedRegistrationCache)
    }
    
    private func canRestoreCachedRegistration(_ cache: RegistrationCache) -> Bool {
        guard
            let currentStepSequence = dependencies.featureManager.object(.dynamicRegistrationStepsSequence, type: [RegistrationStep].self),
            cache.stepSequence.elementsEqual(currentStepSequence)
            else {
                cleanCachedData()
                Analytics.shared.log(RegistrationEvent.restoringCachedRegistrationError)
                return false
        }
        return true
    }
}
