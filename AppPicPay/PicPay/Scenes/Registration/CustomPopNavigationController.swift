protocol CustomPopNavigationControllerDelegate: AnyObject {
    func shouldPop() -> Bool
}

class CustomPopNavigationController: UINavigationController {
    weak var popDelegate: CustomPopNavigationControllerDelegate?
}

extension CustomPopNavigationController: UINavigationBarDelegate {
    func navigationBar(_ navigationBar: UINavigationBar, shouldPop item: UINavigationItem) -> Bool {
        // verions of iOS lower than 13.0 have an issue with this method call
        if #available(iOS 13.0, *) {
            return popDelegate?.shouldPop() ?? true
        } else {
            guard let should = popDelegate?.shouldPop() else {
                return true
            }

            shouldPopItem(should)
            return false
        }
    }

    private func shouldPopItem(_ should: Bool) {
        if should {
            popViewController(animated: true)
        } else {
            // Prevent the back button from staying in an disabled state
            for view in navigationBar.subviews where view.alpha < 1.0 {
                UIView.animate(withDuration: 0.25, animations: {
                    view.alpha = 1.0
                })
            }
        }
    }
}
