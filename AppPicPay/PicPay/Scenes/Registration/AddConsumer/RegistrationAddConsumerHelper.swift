import AnalyticsModule
import Core
import CoreLegacy
import UI

protocol RegistrationAddConsumerHelperContract {
    func addConsumer(
        with consumerModel: RegistrationModel,
        statistics: RegistrationStatisticsModel,
        completion: @escaping (Result<Void, Error>) -> Void
    )
    
    func addComplianceConsumer(
        with consumerModel: RegistrationModel,
        statistics: RegistrationStatisticsModel,
        completion: @escaping (Result<RegistrationComplianceModel, Error>) -> Void
    )
}

final class RegistrationAddConsumerHelper: RegistrationAddConsumerHelperContract {
    typealias Dependencies = HasConsumerManager & HasUserManager & HasMainQueue
    private let dependencies: Dependencies
    private let service: RegistrationAddConsumerServicing
    
    private let referalCodeBannerDismissTime: Double = 12_600 // 3h30m
    
    init(dependencies: Dependencies, service: RegistrationAddConsumerServicing) {
        self.dependencies = dependencies
        self.service = service
    }
    
    func addConsumer(
        with consumerModel: RegistrationModel,
        statistics: RegistrationStatisticsModel,
        completion: @escaping (Result<Void, Error>) -> Void
    ) {
        service.addConsumer(with: consumerModel, statistics: statistics) { [weak self] result in
            guard let self = self else {
                return
            }
            
            switch result {
            case .success(let response):
                self.setupAddConsumerSuccess(consumerModel: consumerModel, token: response.token) {
                    self.dependencies.mainQueue.async {
                        completion(.success)
                    }
                }
            case .failure(let error):
                Analytics.shared.log(RegistrationEvent.documentError(.errorInserting))
                let message = self.parseErrorMessage(error: error) ?? DefaultLocalizable.requestError.text
                completion(.failure(PicPayError(message: message)))
            }
        }
    }
    
    func addComplianceConsumer(
        with consumerModel: RegistrationModel,
        statistics: RegistrationStatisticsModel,
        completion: @escaping (Result<RegistrationComplianceModel, Error>) -> Void
    ) {
        service.addComplianceConsumer(with: consumerModel, statistics: statistics) { result in
            switch result {
            case .success(let response):
                guard response.step != nil else {
                    self.setupAddConsumerSuccess(consumerModel: consumerModel, token: response.token) {
                        self.dependencies.mainQueue.async {
                             completion(.success(response))
                        }
                    }
                    return
                }
                
                self.dependencies.userManager.updateToken(response.token)
                completion(.success(response))
                
            case .failure(let error):
                Analytics.shared.log(RegistrationEvent.documentError(.errorInserting))
                let message = self.parseErrorMessage(error: error) ?? DefaultLocalizable.requestError.text
                completion(.failure(PicPayError(message: message)))
            }
        }
    }
    
    private func setupAddConsumerSuccess(
        consumerModel: RegistrationModel,
        token: String,
        completion: @escaping () -> Void
    ) {
        self.dependencies.userManager.updateToken(token)
        
        self.dependencies.consumerManager.loadConsumerDataCached(useCache: false) { [weak self] _, _ in
            DispatchQueue.global(qos: .userInitiated).async {
                Contacts().identifyPicpayContacts()
            }
            
            self?.updateGlobalData(consumerModel: consumerModel)
            completion()
        }
    }
    
    private func parseErrorMessage(error: ApiError) -> String? {
        switch error {
        case .badRequest(let data):
            return data.message
        case .connectionFailure:
            return DefaultLocalizable.errorConnectionViewSubtitle.text
        default:
            return nil
        }
    }
    
    private func updateGlobalData(consumerModel: RegistrationModel) {
        RegistrationCacheHelper().cleanCachedData()
        PPAuth.clearAuthenticationData()
        
        // Stores temporally the password used to create the account. This password will be used for enabling touch id.
        PPAuthSwift.setTemporaryAuthenticationToken(consumerModel.password, status: PPAuthTempTokenStatusRegister)
        
        AppParameters.global().referalCodeBannerDismissDate = Date().addingTimeInterval(referalCodeBannerDismissTime)
        AppParameters.global().setHasAtLeastOneFeedOnce(false)
        AppParameters.global().setOnboardSocial(true)
        
        // Set that the current user need to create a username
        KVStore().setBool(true, with: .isRegisterUsernameStep)
        
        sendFinishRegistrationEvents()
    }
    
    private func sendFinishRegistrationEvents() {
        ThirdParties().identify(isNewUser: true)
        Analytics.shared.log(UserEvent.register)
    }
}
