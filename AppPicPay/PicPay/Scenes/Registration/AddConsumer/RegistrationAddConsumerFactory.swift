import Foundation

enum RegistrationAddConsumerFactory {
    static func make() -> RegistrationAddConsumerHelperContract {
        let container = DependencyContainer()
        let service: RegistrationAddConsumerServicing = RegistrationAddConsumerService(dependencies: container)
        let helper: RegistrationAddConsumerHelperContract = RegistrationAddConsumerHelper(
            dependencies: container,
            service: service
        )
        return helper
    }
}
