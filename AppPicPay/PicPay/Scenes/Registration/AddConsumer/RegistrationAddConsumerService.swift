import Core

protocol RegistrationAddConsumerServicing {
    func addConsumer(
        with consumerModel: RegistrationModel,
        statistics: RegistrationStatisticsModel,
        completion: @escaping (Result<AddConsumerResponse, ApiError>) -> Void
    )
    
    func addComplianceConsumer(
        with consumerModel: RegistrationModel,
        statistics: RegistrationStatisticsModel,
        completion: @escaping (Result<RegistrationComplianceModel, ApiError>) -> Void
    )
}

final class RegistrationAddConsumerService: RegistrationAddConsumerServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func addComplianceConsumer(
        with consumerModel: RegistrationModel,
        statistics: RegistrationStatisticsModel,
        completion: @escaping (Result<RegistrationComplianceModel, ApiError>) -> Void
    ) {
        let endpoint = RegisterEndpoint.addComplianceConsumer(consumerModel: consumerModel, statistics: statistics)
        let decoder = JSONDecoder(.convertFromSnakeCase)
        Api<RegisterResponse<RegistrationComplianceModel>>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let response):
                    self?.processesSuccessResponse(response: response.model, completion: completion)
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    func addConsumer(
        with consumerModel: RegistrationModel,
        statistics: RegistrationStatisticsModel,
        completion: @escaping (Result<AddConsumerResponse, ApiError>) -> Void
    ) {
        let endpoint = RegisterEndpoint.addConsumer(consumerModel: consumerModel, statistics: statistics)
        let decoder = JSONDecoder(.convertFromSnakeCase)
        Api<RegisterResponse<AddConsumerResponse>>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let response):
                    self?.processesSuccessResponse(response: response.model, completion: completion)
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}

extension RegistrationAddConsumerService {
    private func processesSuccessResponse<T>(
        response: RegisterResponse<T>,
        completion: @escaping (Result<T, ApiError>) -> Void
    ) {
        if let error = response.error {
            var requestError = RequestError()
            requestError.message = error.description
            completion(.failure(ApiError.badRequest(body: requestError)))
            return
        }
        
        guard let data = response.data else {
            completion(Result.failure(ApiError.bodyNotFound))
            return
        }
        
        completion(Result.success(data))
    }
}
