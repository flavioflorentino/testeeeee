import Core
import Foundation

protocol ContinueRegistrationPresenting: AnyObject {
    var viewController: ContinueRegistrationDisplay? { get set }
    func displayTitle()
    func displayTitle(withName name: String)
    func didNextStep(action: ContinueRegistrationAction)
}

final class ContinueRegistrationPresenter: ContinueRegistrationPresenting {
    private let coordinator: ContinueRegistrationCoordinating
    weak var viewController: ContinueRegistrationDisplay?

    init(coordinator: ContinueRegistrationCoordinating) {
        self.coordinator = coordinator
    }
    
    func displayTitle() {
        viewController?.display(title: RegisterLocalizable.continueRegistrationTitle.text)
    }
    
    func displayTitle(withName name: String) {
        let title = String(format: RegisterLocalizable.continueRegistrationTitleWithName.text, name)
        viewController?.display(title: title)
    }
    
    func didNextStep(action: ContinueRegistrationAction) {
        coordinator.perform(action: action)
    }
}
