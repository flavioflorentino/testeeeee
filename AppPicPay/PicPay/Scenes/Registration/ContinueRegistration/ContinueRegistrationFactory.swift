import Foundation

enum ContinueRegistrationFactory {
    static func make(cache: RegistrationCache) -> ContinueRegistrationViewController {
        let coordinator: ContinueRegistrationCoordinating = ContinueRegistrationCoordinator(cache: cache)
        let presenter: ContinueRegistrationPresenting = ContinueRegistrationPresenter(coordinator: coordinator)
        let viewModel = ContinueRegistrationViewModel(presenter: presenter, name: cache.model.firstName ?? "")
        let viewController = ContinueRegistrationViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
