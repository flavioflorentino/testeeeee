import UI
import UIKit

private extension ContinueRegistrationViewController.Layout {
    enum Style {
        static let backgroundImage = Assets.Registration.backgroundContinueRegistration.image
        static let titleFont = UIFont.systemFont(ofSize: 28, weight: .semibold)
        static let descriptionFont = UIFont.systemFont(ofSize: 14)
        static let textColor = Palette.ppColorGrayscale100.color(withCustomDark: .ppColorGrayscale100)
        static let numberOfLines = 0
        static let startNewRegistrationButtonFont: UIFont = .systemFont(ofSize: 14, weight: .medium)
        static let startNewRegistrationButtonTextColor = Palette.white.color
    }
    
    enum Size {
        static let buttonHeight: CGFloat = 40
        static let buttonCornerRadius: CGFloat = buttonHeight / 2
    }
}

final class ContinueRegistrationViewController: ViewController<ContinueRegistrationViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    private lazy var backgroungImageView: UIImageView = {
        let imageView = UIImageView(image: Layout.Style.backgroundImage)
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private lazy var containerView = UIView()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Style.titleFont
        label.textColor = Layout.Style.textColor
        label.numberOfLines = Layout.Style.numberOfLines
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Style.descriptionFont
        label.textColor = Layout.Style.textColor
        label.text = RegisterLocalizable.continueRegistrationDescription.text
        label.numberOfLines = Layout.Style.numberOfLines
        return label
    }()
    
    private lazy var continueButton: UIPPButton = {
        let button = UIPPButton(
            title: RegisterLocalizable.continueRegistration.text,
            target: self,
            action: #selector(tapContinueButton)
        )
        button.cornerRadius = Layout.Size.buttonCornerRadius
        return button
    }()
    
    private lazy var startNewRegistrationButton: UIButton = {
        let title = NSAttributedString(string: "<u>\(RegisterLocalizable.startNewRegistration.text)</u>").underlinefy()
        let button = UIButton(type: .system)
        button.tintColor = Layout.Style.startNewRegistrationButtonTextColor
        button.titleLabel?.font = Layout.Style.startNewRegistrationButtonFont
        button.setAttributedTitle(title, for: .normal)
        button.addTarget(self, action: #selector(tapStartNewRegistrationButton), for: .touchUpInside)
        return button
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.setupViewContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        viewModel.trackView()
    }

    override func buildViewHierarchy() {
        view.addSubview(backgroungImageView)
        view.addSubview(containerView)
        
        containerView.addSubview(titleLabel)
        containerView.addSubview(descriptionLabel)
        containerView.addSubview(continueButton)
        containerView.addSubview(startNewRegistrationButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale100.color
    }
    
    override func setupConstraints() {
        backgroungImageView.layout {
            $0.top == view.topAnchor
            $0.bottom == view.bottomAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
        }
        
        containerView.layout {
            $0.leading == view.leadingAnchor + Spacing.base05
            $0.trailing == view.trailingAnchor - Spacing.base05
            $0.bottom == view.compatibleSafeAreaLayoutGuide.bottomAnchor - Spacing.base03
        }
        
        titleLabel.layout {
            $0.top == containerView.topAnchor
            $0.leading == containerView.leadingAnchor
            $0.trailing == containerView.trailingAnchor
        }
        
        descriptionLabel.layout {
            $0.top == titleLabel.bottomAnchor + Spacing.base01
            $0.leading == containerView.leadingAnchor
            $0.trailing == containerView.trailingAnchor
        }
        
        continueButton.layout {
            $0.top == descriptionLabel.bottomAnchor + Spacing.base03
            $0.leading == containerView.leadingAnchor
            $0.trailing == containerView.trailingAnchor
            $0.height == Layout.Size.buttonHeight
        }
        
        startNewRegistrationButton.layout {
            $0.top == continueButton.bottomAnchor + Spacing.base03
            $0.centerX == containerView.centerXAnchor
            $0.bottom == containerView.bottomAnchor
        }
    }
}

@objc
private extension ContinueRegistrationViewController {
    func tapContinueButton() {
        viewModel.didContinueRegistration()
    }
    
    func tapStartNewRegistrationButton() {
        viewModel.didStartNewRegistration()
    }
}

// MARK: View Model Outputs
extension ContinueRegistrationViewController: ContinueRegistrationDisplay {
    func display(title: String) {
        titleLabel.text = title
    }
}
