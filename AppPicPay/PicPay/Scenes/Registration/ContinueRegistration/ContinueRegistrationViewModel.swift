import AnalyticsModule

protocol ContinueRegistrationViewModelInputs: AnyObject {
    func setupViewContent()
    func trackView()
    func didContinueRegistration()
    func didStartNewRegistration()
}

final class ContinueRegistrationViewModel {
    private let presenter: ContinueRegistrationPresenting
    private let name: String

    init(presenter: ContinueRegistrationPresenting, name: String = "") {
        self.presenter = presenter
        self.name = name
    }
}

extension ContinueRegistrationViewModel: ContinueRegistrationViewModelInputs {
    func setupViewContent() {
        guard name.isNotEmpty else {
            presenter.displayTitle()
            return
        }
        
        presenter.displayTitle(withName: name)
    }
    
    func trackView() {
        Analytics.shared.log(RegistrationEvent.continuedScreenViewed)
    }
    
    func didContinueRegistration() {
        Analytics.shared.log(RegistrationEvent.continuedScreenOptionSelected(.continueRegistration))
        presenter.didNextStep(action: .continueRegistration)
    }
    
    func didStartNewRegistration() {
        Analytics.shared.log(RegistrationEvent.continuedScreenOptionSelected(.startNewRegistration))
        presenter.didNextStep(action: .startNewRegistration)
    }
}
