import UIKit

protocol ContinueRegistrationDisplay: AnyObject {
    func display(title: String)
}
