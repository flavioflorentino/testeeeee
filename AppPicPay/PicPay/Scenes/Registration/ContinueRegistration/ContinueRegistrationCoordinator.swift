import UIKit

enum ContinueRegistrationAction {
    case continueRegistration
    case startNewRegistration
}

protocol ContinueRegistrationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ContinueRegistrationAction)
}

final class ContinueRegistrationCoordinator: ContinueRegistrationCoordinating {
    weak var viewController: UIViewController?
    private let cache: RegistrationCache
    
    init(cache: RegistrationCache) {
        self.cache = cache
    }
    
    func perform(action: ContinueRegistrationAction) {
        guard let navController = viewController?.navigationController else {
            return
        }
        
        let initialViewController = SignUpFactory.make()
        
        let registrationCoordinator = RegistrationFlowCoordinator(
            dependencies: DependencyContainer(),
            navigationController: navController,
            cachedRegistration: action == .continueRegistration ? cache : nil
        )
        
        registrationCoordinator.start(onTopOf: initialViewController)
    }
}
