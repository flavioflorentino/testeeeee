import Core
import CoreLegacy

private enum Keys: String {
    case name, email, pass, ddd, number, cpf, birth
    case verificationCode = "verification_code"
}

enum RegisterEndpoint {
    case verifyName(_ name: String)
    case isValidEmail(String)
    case isValidPassword(String)
    case requestSmsCode(ddd: String, number: String)
    case requestWhatsAppCode(ddd: String, number: String)
    case verifyPhoneNumber(code: String)
    case callVerification(ddd: String, number: String)
    case isValidCpfAndBirth(cpf: String, birth: String, isCompliance: Bool)
    case addConsumer(consumerModel: RegistrationModel, statistics: RegistrationStatisticsModel)
    case addComplianceConsumer(consumerModel: RegistrationModel, statistics: RegistrationStatisticsModel)
}

extension RegisterEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .verifyName:
            return "api/isValidName.json"
        case .requestSmsCode:
            return "api/sendSmsVerification.json"
        case .requestWhatsAppCode:
            return "api/sendWhatsappVerification.json"
        case .isValidEmail:
            return "api/isValidEmail.json"
        case .isValidPassword:
            return "api/isValidPass.json"
        case .verifyPhoneNumber:
            return "api/verifyPhoneNumber.json"
        case .callVerification:
            return "api/callVerification.json"
        case let .isValidCpfAndBirth(_, _, isCompliance):
            return isCompliance ? "registration-validation/validate-document" : "api/isValidCpfAndBirth.json"
        case .addConsumer, .addComplianceConsumer:
            return "api/addConsumer.json"
        }
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var customHeaders: [String: String] {
        guard case .addComplianceConsumer = self else {
            return [:]
        }
        
        return ["account_setup_registration_manager": "true"]
    }
    
    var body: Data? {
        switch self {
        case let .verifyName(name):
            return [Keys.name.rawValue: name].toData()
        case let .isValidEmail(email):
            return [Keys.email.rawValue: email].toData()
        case let .isValidPassword(password):
            return [Keys.pass.rawValue: password].toData()
        case let .requestSmsCode(ddd, number), let .requestWhatsAppCode(ddd, number), let .callVerification(ddd, number):
            return [Keys.ddd.rawValue: ddd, Keys.number.rawValue: number].toData()
        case let .verifyPhoneNumber(code):
            return [Keys.verificationCode.rawValue: code].toData()
        case let .isValidCpfAndBirth(cpf, birth, _):
            return [Keys.cpf.rawValue: cpf, Keys.birth.rawValue: birth].toData()
        case let .addConsumer(consumerModel, registrationStatistics),
             let .addComplianceConsumer(consumerModel, registrationStatistics):
            return addConsumerDictionary(consumerModel: consumerModel, statistics: registrationStatistics).toData()
        }
    }
    
    var isTokenNeeded: Bool {
        false
    }
    
    private func addConsumerDictionary(
        consumerModel: RegistrationModel,
        statistics: RegistrationStatisticsModel
    ) -> [String: Any] {
        guard
            let firstName = consumerModel.firstName,
            let lastName = consumerModel.lastName,
            let email = consumerModel.email,
            let password = consumerModel.password,
            let cpf = consumerModel.cpf,
            let dateOfBirth = consumerModel.dateOfBirth
            else {
                return [:]
        }
        
        let consumerDict: [String: Any] = [
            "email": email,
            "pass": password,
            "cpf": cpf,
            "birth_date": dateOfBirth,
            "name": "\(firstName) \(lastName)",
            "indication": "",
            "notification_token": AppParameters.global().pushToken ?? "",
            "distinct_id": PPAnalytics.distinctId() ?? "",
            "appsflyer_id": PPAnalytics.appsFlyerId() ?? "",
            "advertising_id": PPAnalytics.idfa() ?? ""
        ]
        
        let registrationTimeInMiliseconds = Int((CACurrentMediaTime() - statistics.beginRegistrationTime) * 1_000)
        let additionalInfo: [String: Any] = [
            "registration_elapsed_time": String(registrationTimeInMiliseconds),
            "registration_tried_invalid_email": String(statistics.invalidEmailCounter),
            "registration_tried_invalid_cpf": String(statistics.invalidCpfCounter),
            "registration_cpf_copy_paste": String(statistics.copyPasteCpf)
        ]
        
        return consumerDict.deepMerge(additionalInfo)
    }
}
