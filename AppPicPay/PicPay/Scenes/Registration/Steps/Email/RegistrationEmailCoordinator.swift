protocol RegistrationEmailCoordinatorDelegate: AnyObject {
    func moveToNextStep(withEmail email: String, invalidEmailCounter: Int)
}

typealias RegistrationEmailDelegate = RegistrationEmailCoordinatorDelegate & RegistrationFlowCoordinatorDelegate

enum RegistrationEmailAction {
    case back
    case moveToNextStep(withEmail: String, invalidEmailCounter: Int)
    case alreadyRegistered
    case webview(url: URL)
}

protocol RegistrationEmailCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: RegistrationEmailDelegate? { get set }
    func perform(action: RegistrationEmailAction)
}

final class RegistrationEmailCoordinator: RegistrationEmailCoordinating {
    weak var delegate: RegistrationEmailDelegate?
    weak var viewController: UIViewController?
    
    func perform(action: RegistrationEmailAction) {
        switch action {
        case .back:
            delegate?.moveToPreviousRegistrationStep()
        case let .moveToNextStep(email, invalidEmailCounter):
            delegate?.moveToNextStep(withEmail: email, invalidEmailCounter: invalidEmailCounter)
        case .alreadyRegistered:
            delegate?.moveToLogin()
        case .webview(let url):
            let webviewController = WebViewFactory.make(with: url)
            viewController?.navigationController?.present(PPNavigationController(rootViewController: webviewController), animated: true)
        }
    }
}
