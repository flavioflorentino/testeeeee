import Core

protocol RegistrationEmailServicing {
    func validateEmail(email: String, completion: @escaping (Result<Void, ApiError>) -> Void)
}

final class RegistrationEmailService: RegistrationEmailServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func validateEmail(email: String, completion: @escaping (Result<Void, ApiError>) -> Void) {
        let endpoint = RegisterEndpoint.isValidEmail(email)
        let decoder = JSONDecoder(.convertFromSnakeCase)
        Api<RegisterResponse<NoContent>>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let responseData):
                    guard let error = responseData.model.error else {
                        completion(.success)
                        return
                    }
                    var requestError = RequestError()
                     requestError.code = String(error.id)
                    requestError.message = error.description
                    completion(.failure(ApiError.badRequest(body: requestError)))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}
