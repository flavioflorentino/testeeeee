protocol RegistrationEmailDisplay: AnyObject {
    func fillEmailTextField(with email: String)
    func displayTermsOfService()
    func displayError(message: String)
    func hideErrors()
    func showLoading()
    func hideLoading()
    func dismissKeyboard()
    func enableControls()
}
