import AnalyticsModule
import Core

protocol RegistrationEmailViewModelInputs: AnyObject {
    func setupViewContent()
    func trackView()
    func updateEmail(with updatedEmail: String?)
    func didConfirm()
    func didAlreadyRegistered()
    func didInteractWithUrl(_ url: URL)
    func didTapBack()
}

final class RegistrationEmailViewModel {
    private enum Constants {
        static let invalidEmailCode = 5_004
        static let alreadyRegisteredEmailCode = 5_005
    }
    
    private let service: RegistrationEmailServicing
    private let presenter: RegistrationEmailPresenting
    private var email: String
    private let showTermsOfService: Bool
    private let eventName = "cadastro_email"
    private var invalidEmailCounter = 0

    init(service: RegistrationEmailServicing, presenter: RegistrationEmailPresenting, email: String? = nil, showTermsOfService: Bool) {
        self.service = service
        self.presenter = presenter
        self.email = email ?? ""
        self.showTermsOfService = showTermsOfService
    }
    
    private func regexEmailValidation(_ emailString: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailString)
    }
    
    private func validateEmail(_ emailString: String) {
        presenter.dismissKeyboard()
        presenter.showLoading()
        service.validateEmail(email: email) { [weak self] result in
            guard let self = self else {
                return
            }
            self.presenter.hideLoading()
            self.presenter.hideErrors()
            
            switch result {
            case .success:
                self.trackSuccessToNextStep()
                self.presenter.didNextStep(action: .moveToNextStep(withEmail: emailString, invalidEmailCounter: self.invalidEmailCounter))
            case .failure(let error):
                if let codeString = error.requestError?.code, let code = Int(codeString) {
                    self.countInvalidEmailInputs(code: code)
                }
                let message = self.parseErrorMessage(error: error) ?? DefaultLocalizable.requestError.text
                self.presenter.displayError(message: message)
            }
        }
    }
    
    private func parseErrorMessage(error: ApiError) -> String? {
        switch error {
        case .badRequest(let data):
            return data.message
        case .connectionFailure:
            return DefaultLocalizable.errorConnectionViewSubtitle.text
        default:
            return nil
        }
    }
    
    private func trackSuccessToNextStep() {
        Analytics.shared.log(RegistrationEvent.email)
    }

    private func countInvalidEmailInputs(code: Int) {
        guard code == Constants.invalidEmailCode || code == Constants.alreadyRegisteredEmailCode else {
            return
        }
        invalidEmailCounter += 1
    }
}

extension RegistrationEmailViewModel: RegistrationEmailViewModelInputs {
    func trackView() {
        PPAnalytics.trackEvent(eventName, properties: nil)
    }
    
    func setupViewContent() {
        if showTermsOfService {
            presenter.displayTermsOfService()
        }
        if email.isNotEmpty {
            presenter.fillEmailTextField(with: email)
        }
        presenter.enableControls()
    }
    
    func updateEmail(with updatedEmail: String?) {
        guard let updatedEmail = updatedEmail else {
            return
        }
        self.email = updatedEmail
    }
    
    func didConfirm() {
        guard regexEmailValidation(email) else {
            presenter.displayInputError()
            return
        }
        validateEmail(email)
    }
    
    func didAlreadyRegistered() {
        presenter.didNextStep(action: .alreadyRegistered)
    }
    
    func didInteractWithUrl(_ url: URL) {
        guard
            let webviewUrlString = WebServiceInterface.apiEndpoint(url.absoluteString),
            let webviewUrl = URL(string: webviewUrlString)
            else {
                return
        }
        presenter.didNextStep(action: .webview(url: webviewUrl))
    }
    
    func didTapBack() {
        presenter.didNextStep(action: .back)
    }
}
