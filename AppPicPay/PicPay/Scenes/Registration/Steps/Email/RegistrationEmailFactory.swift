import Foundation

enum RegistrationEmailFactory {
    static func make(
        delegate: RegistrationEmailDelegate,
        email: String?,
        showTermsOfService: Bool
    ) -> RegistrationEmailViewController {
        let container = DependencyContainer()
        let service: RegistrationEmailServicing = RegistrationEmailService(dependencies: container)
        let coordinator: RegistrationEmailCoordinating = RegistrationEmailCoordinator()
        let presenter: RegistrationEmailPresenting = RegistrationEmailPresenter(coordinator: coordinator)
        let viewModel = RegistrationEmailViewModel(
            service: service,
            presenter: presenter,
            email: email,
            showTermsOfService: showTermsOfService
        )
        let viewController = RegistrationEmailViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
