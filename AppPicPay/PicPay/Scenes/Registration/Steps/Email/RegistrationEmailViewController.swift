import UI
import UIKit

private extension RegistrationEmailViewController.Layout {
    static let textFieldHeight: CGFloat = Spacing.base06
    static let buttonHeight: CGFloat = Spacing.base06
    static let buttonRadius: CGFloat = 24.0
}

final class RegistrationEmailViewController: RegistrationBaseViewController<RegistrationEmailViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textAlignment, .center)
        label.text = RegisterLocalizable.emailStepTitle.text
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale600.color)
            .with(\.textAlignment, .center)
        label.textAlignment = .center
        label.text = RegisterLocalizable.emailStepDescription.text
        label.setContentHuggingPriority(.defaultLow, for: .vertical)
        return label
    }()
    
    private lazy var emailTextField: RoundedTextField = {
        let textField = RoundedTextField()
        textField.cornerRadius = Layout.textFieldHeight / 2
        textField.placeholder = RegisterLocalizable.emailStepFieldPlaceholder.text
        textField.border = .light(color: .black(.default))
        textField.textColor = Colors.black.color
        textField.placeHolderTextColor = Colors.grayscale600.color
        textField.backgroundColor = .clear
        textField.keyboardType = .emailAddress
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        textField.delegate = self
        return textField
    }()
    
    private lazy var confirmButton: UIPPButton = {
        let button = UIPPButton()
        button.setBackgroundColor(Colors.branding600.color)
        button.setTitleColor(Colors.white.lightColor)
        button.cornerRadius = Layout.buttonRadius
        button.loadingTitleColor = .clear
    
        button.setTitle(DefaultLocalizable.advance.text, for: .normal)
        button.addTarget(self, action: #selector(tapConfirmButton), for: .touchUpInside)
        
        return button
    }()
    
    private lazy var stepDescriptionStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var inputStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var supportStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base05
        return stackView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.setupViewContent()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        openKeyboardIfNecessary(emailTextField)
    }
 
    override func buildViewHierarchy() {
        super.buildViewHierarchy()
        stepDescriptionStackView.addArrangedSubviews(titleLabel, descriptionLabel)
        inputStackView.addArrangedSubviews(emailTextField, errorLabel)
        supportStackView.addArrangedSubviews(stepDescriptionStackView, inputStackView)
        contentStackView.addArrangedSubviews(supportStackView, termsOfServiceView)
        view.addSubview(confirmButton)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        emailTextField.layout {
            $0.height == Layout.textFieldHeight
        }

        confirmButton.layout {
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
            $0.height == Layout.buttonHeight
            primaryButtonBottomConstraint = $0.bottom == view.bottomAnchor - Spacing.base03
        }
    }
    
    override func tapBackButton() {
        viewModel.didTapBack()
    }
    
    override func didInteractWithUrl(_ url: URL) {
        viewModel.didInteractWithUrl(url)
    }
    
    override func tapAlreadyRegisteredButton() {
        viewModel.updateEmail(with: emailTextField.text)
        viewModel.didAlreadyRegistered()
    }
}

@objc
private extension RegistrationEmailViewController {
    func tapConfirmButton() {
        viewModel.updateEmail(with: emailTextField.text)
        viewModel.didConfirm()
    }
}

extension RegistrationEmailViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        viewModel.updateEmail(with: emailTextField.text)
        viewModel.didConfirm()
        return true
    }
}

extension RegistrationEmailViewController: RegistrationEmailDisplay, RegistrationLoadingDisplay {
    func fillEmailTextField(with email: String) {
        emailTextField.text = email
        emailTextField.layoutIfNeeded()
    }
    
    func displayTermsOfService() {
        termsOfServiceView.isHidden = false
    }
    
    func enableControls() {
        alreadyRegisteredButton.isHidden = true
    }
    
    func displayError(message: String) {
        errorLabel.text = message
        errorLabel.isHidden = false
        emailTextField.hasError = true
    }
    
    func hideErrors() {
        errorLabel.text = ""
        errorLabel.isHidden = true
        emailTextField.hasError = false
    }
    
    func showLoading() {
        confirmButton.startLoadingAnimating()
    }
    
    func hideLoading() {
        confirmButton.stopLoadingAnimating()
    }
}
