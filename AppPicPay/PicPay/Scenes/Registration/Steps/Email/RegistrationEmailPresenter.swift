protocol RegistrationEmailPresenting: AnyObject {
    var viewController: RegistrationEmailDisplay? { get set }
    func fillEmailTextField(with email: String)
    func displayTermsOfService()
    func didNextStep(action: RegistrationEmailAction)
    func displayInputError()
    func displayError(message: String)
    func hideErrors()
    func showLoading()
    func hideLoading()
    func dismissKeyboard()
    func enableControls()
}

final class RegistrationEmailPresenter: RegistrationEmailPresenting {
    private let coordinator: RegistrationEmailCoordinating
    weak var viewController: RegistrationEmailDisplay?

    init(coordinator: RegistrationEmailCoordinating) {
        self.coordinator = coordinator
    }
    
    func fillEmailTextField(with email: String) {
        viewController?.fillEmailTextField(with: email)
    }
    
    func displayTermsOfService() {
        viewController?.displayTermsOfService()
    }
    
    func enableControls() {
        viewController?.enableControls()
    }
    
    func didNextStep(action: RegistrationEmailAction) {
        coordinator.perform(action: action)
    }
    
    func displayInputError() {
        viewController?.displayError(message: RegisterLocalizable.invalidEmail.text)
    }
    
    func displayError(message: String) {
        viewController?.displayError(message: message)
    }
    
    func hideErrors() {
        viewController?.hideErrors()
    }
    
    func showLoading() {
        viewController?.showLoading()
    }
    
    func hideLoading() {
        viewController?.hideLoading()
    }
    
    func dismissKeyboard() {
        viewController?.dismissKeyboard()
    }
}
