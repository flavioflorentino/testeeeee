import UI
import UIKit

private extension RegistrationNameViewController.Layout {
    enum Style {
        static let inputCornerRadius: CGFloat = 20
        static let buttonsCornerRadius: CGFloat = Size.buttonHeight / 2
        static let textAlignment: NSTextAlignment = .center
        static let numberOfLines = 0
        static let titleFont: UIFont = .systemFont(ofSize: 18, weight: .medium)
        static let titleTextColor = Palette.ppColorGrayscale500.color
        static let descriptionFont: UIFont = .systemFont(ofSize: 14)
        static let descriptionTextColor = Palette.ppColorGrayscale400.color
        static let alreadyRegisteredButtonTextColor = Palette.ppColorGrayscale500.color
        static let alreadyRegisteredButtonFont: UIFont = .systemFont(ofSize: 12)
    }
    
    enum Size {
        static let inputWidth: CGFloat = 110
        static let inputHeight: CGFloat = 40
        static let buttonHeight: CGFloat = Spacing.base06
    }
}

final class RegistrationNameViewController: RegistrationBaseViewController<RegistrationNameViewModelInputs, UIView> {
    fileprivate struct Layout {}
    
    private lazy var stepDescriptionStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var inputStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var nameInputStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Style.titleFont
        label.textColor = Layout.Style.titleTextColor
        label.textAlignment = Layout.Style.textAlignment
        label.numberOfLines = Layout.Style.numberOfLines
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = RegisterLocalizable.nameStepDescription.text
        label.font = Layout.Style.descriptionFont
        label.textColor = Layout.Style.descriptionTextColor
        label.textAlignment = Layout.Style.textAlignment
        label.numberOfLines = Layout.Style.numberOfLines
        return label
    }()
    
    private lazy var firstNameTextField: RoundedTextField = {
        let textField = RoundedTextField()
        textField.cornerRadius = Layout.Style.inputCornerRadius
        textField.placeholder = RegisterLocalizable.nameStepFirstName.text
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField.delegate = self
        return textField
    }()
    
    private lazy var lastNameTextField: RoundedTextField = {
        let textField = RoundedTextField()
        textField.cornerRadius = Layout.Style.inputCornerRadius
        textField.placeholder = RegisterLocalizable.nameStepLastName.text
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField.delegate = self
        return textField
    }()
    
    private lazy var confirmButton: UIPPButton = {
        let button = UIPPButton()
        button.setTitle(DefaultLocalizable.advance.text, for: .normal)
        button.configureButtonCta()
        button.cornerRadius = Layout.Style.buttonsCornerRadius
        button.addTarget(self, action: #selector(continueAction), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadInformations()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.fillTextFieldsIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        openKeyboardIfNecessary(firstNameTextField)
        viewModel.trackView()
    }

    override func buildViewHierarchy() {
        super.buildViewHierarchy()
        view.addSubview(alreadyRegisteredButton)
        contentStackView.addArrangedSubviews(stepDescriptionStackView, inputStackView, confirmButton, termsOfServiceView)
        stepDescriptionStackView.addArrangedSubviews(titleLabel, descriptionLabel)
        inputStackView.addArrangedSubviews(nameInputStackView, errorLabel)
        nameInputStackView.addArrangedSubviews(firstNameTextField, lastNameTextField)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        firstNameTextField.layout {
            $0.width == Layout.Size.inputWidth
            $0.height == Layout.Size.inputHeight
        }
        
        lastNameTextField.layout {
            $0.height == Layout.Size.inputHeight
        }
        
        confirmButton.layout {
            $0.height == Layout.Size.buttonHeight
        }
        
        alreadyRegisteredButton.layout {
            $0.centerX == view.centerXAnchor
            $0.bottom == view.compatibleSafeAreaLayoutGuide.bottomAnchor
            $0.height == Layout.Size.buttonHeight
        }
    }
    
    override func tapBackButton() {
        viewModel.didTapBack()
    }
    
    override func didInteractWithUrl(_ url: URL) {
        viewModel.didInteractWithUrl(url)
    }
    
    override func tapAlreadyRegisteredButton() {
        viewModel.didAlreadyRegistered()
    }
}

@objc
private extension RegistrationNameViewController {
    func textFieldDidChange(_ textField: UITextField) {
        if textField == firstNameTextField {
            viewModel.updateFirstName(firstNameTextField.text)
        } else {
            viewModel.updateLastName(lastNameTextField.text)
        }
    }
    
    func continueAction() {
        viewModel.verifyName()
    }
}

// MARK: View Model Outputs
extension RegistrationNameViewController: RegistrationNameDisplay, RegistrationLoadingDisplay {
    func fillTextFields(with firstName: String, lastName: String) {
        firstNameTextField.text = firstName
        lastNameTextField.text = lastName
    }
    
    func displayTermsOfService() {
        termsOfServiceView.isHidden = false
    }
    
    func setTitle(_ title: NSAttributedString) {
        titleLabel.attributedText = title
    }
    
    func showLoading() {
        confirmButton.startLoadingAnimating()
        firstNameTextField.isEnabled = false
        lastNameTextField.isEnabled = false
    }
    
    func hideLoading() {
        confirmButton.stopLoadingAnimating()
        firstNameTextField.isEnabled = true
        lastNameTextField.isEnabled = true
    }
    
    func displayError(_ message: String, isFirstNameError: Bool, isLastNameError: Bool) {
        errorLabel.isHidden = false
        errorLabel.text = message
        firstNameTextField.hasError = isFirstNameError
        lastNameTextField.hasError = isLastNameError
    }
    
    func hideErrorMessage() {
        errorLabel.isHidden = true
        errorLabel.text = nil
        firstNameTextField.hasError = false
        lastNameTextField.hasError = false
    }
}

extension RegistrationNameViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == firstNameTextField {
            lastNameTextField.becomeFirstResponder()
        } else {
            continueAction()
        }
        
        return true
    }
}
