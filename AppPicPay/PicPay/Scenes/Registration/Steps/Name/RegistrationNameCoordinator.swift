protocol RegistrationNameCoordinatorDelegate: AnyObject {
    func moveToNextStep(withFirstName firstName: String, lastName: String)
}

typealias RegistrationNameDelegate = RegistrationFlowCoordinatorDelegate & RegistrationNameCoordinatorDelegate

enum RegistrationNameAction {
    case back
    case moveToLogin
    case moveToNextStep(withFirstName: String, lastName: String)
    case webview(url: URL)
}

protocol RegistrationNameCoordinating: AnyObject {
    var delegate: RegistrationNameDelegate? { get set }
    var viewController: UIViewController? { get set }
    func perform(action: RegistrationNameAction)
}

final class RegistrationNameCoordinator: RegistrationNameCoordinating {
    weak var delegate: RegistrationNameDelegate?
    weak var viewController: UIViewController?
    
    func perform(action: RegistrationNameAction) {
        switch action {
        case .back:
            delegate?.moveToPreviousRegistrationStep()
        case let .moveToNextStep(firstName, lastName):
            delegate?.moveToNextStep(withFirstName: firstName, lastName: lastName)
        case .moveToLogin:
            delegate?.moveToLogin()
        case .webview(let url):
            let webviewController = WebViewFactory.make(with: url)
            viewController?.navigationController?.present(PPNavigationController(rootViewController: webviewController), animated: true)
        }
    }
}
