import UIKit

protocol RegistrationNameDisplay: AnyObject {
    func fillTextFields(with firstName: String, lastName: String)
    func displayTermsOfService()
    func setTitle(_ title: NSAttributedString)
    func showLoading()
    func hideLoading()
    func displayError(_ message: String, isFirstNameError: Bool, isLastNameError: Bool)
    func hideErrorMessage()
}
