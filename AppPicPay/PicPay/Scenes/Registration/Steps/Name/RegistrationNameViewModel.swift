import AnalyticsModule
import Core

protocol RegistrationNameViewModelInputs: AnyObject {
    func loadInformations()
    func fillTextFieldsIfNeeded()
    func trackView()
    func updateFirstName(_ firstName: String?)
    func updateLastName(_ lastName: String?)
    func verifyName()
    func didAlreadyRegistered()
    func didInteractWithUrl(_ url: URL)
    func didTapBack()
}

final class RegistrationNameViewModel {
    private let service: RegistrationNameServicing
    private let presenter: RegistrationNamePresenting
    private var firstName: String
    private var lastName: String
    private let showTermsOfService: Bool
    private let minimumOfCharacters = 3
    private let eventName = "cadastro_nome"
    
    private var isValidFirstName: Bool {
        firstName.trim().count >= minimumOfCharacters
    }
    
    private var isValidLastName: Bool {
        lastName.trim().count >= minimumOfCharacters
    }

    init(
        service: RegistrationNameServicing,
        presenter: RegistrationNamePresenting,
        firstName: String? = nil,
        lastName: String? = nil,
        showTermsOfService: Bool
    ) {
        self.service = service
        self.presenter = presenter
        self.firstName = firstName ?? ""
        self.lastName = lastName ?? ""
        self.showTermsOfService = showTermsOfService
    }
    
    private func buildTitle() {
        var title = ""
        
        if !firstName.isEmpty {
            title = firstName
        }
        
        if !lastName.isEmpty {
            title = title.isEmpty ? lastName : "\(title) \(lastName)"
        }
        
        guard !title.isEmpty else {
            presenter.showDefaultTitle()
            return
        }
        
        presenter.buildTitle(title)
    }
    
    private func parseErrorMessage(error: ApiError) -> String? {
        switch error {
        case .badRequest(let data):
            return data.message
        case .connectionFailure:
            return DefaultLocalizable.errorConnectionViewSubtitle.text
        default:
            return nil
        }
    }
    
    private func isValidName() -> Bool {
        if !isValidFirstName, !isValidLastName {
            presenter.presentError(RegisterLocalizable.nameStepError.text, isFirstNameError: true, isLastNameError: true)
            return false
        }
        
        if !isValidFirstName {
            presenter.presentError(
                RegisterLocalizable.nameStepMustContainAtLeast.text,
                isFirstNameError: true,
                isLastNameError: false
            )
            return false
        }
        
        if !isValidLastName {
            presenter.presentError(
                RegisterLocalizable.lastNameStepMustContainAtLeast.text,
                isFirstNameError: false,
                isLastNameError: true
            )
            return false
        }
        
        return true
    }
    
    private func trackSuccessToNextStep() {
        Analytics.shared.log(RegistrationEvent.name)
    }
}

extension RegistrationNameViewModel: RegistrationNameViewModelInputs {
    func trackView() {
        PPAnalytics.trackEvent(eventName, properties: nil)
    }
    
    func loadInformations() {
        buildTitle()
        
        if showTermsOfService {
            presenter.presentTermsOfService()
        }
    }
    
    func fillTextFieldsIfNeeded() {
        if firstName.isNotEmpty && lastName.isNotEmpty {
            presenter.fillTextFields(with: firstName, lastName: lastName)
        }
    }
    
    func updateFirstName(_ firstName: String?) {
        self.firstName = firstName ?? ""
        buildTitle()
    }
    
    func updateLastName(_ lastName: String?) {
        self.lastName = lastName ?? ""
        buildTitle()
    }
    
    func verifyName() {
        presenter.hideErrorMessage()
        
        guard isValidName() else {
            return
        }
        
        presenter.showLoadingAnimation()
        
        service.verifyName("\(firstName) \(lastName)") { [weak self] error in
            guard let self = self else {
                return
            }
            
            self.presenter.hideLoadingAnimation()
            
            guard let error = error else {
                self.trackSuccessToNextStep()
                self.presenter.didNextStep(action: .moveToNextStep(withFirstName: self.firstName, lastName: self.lastName))
                return
            }
            
            let message = self.parseErrorMessage(error: error) ?? DefaultLocalizable.requestError.text
            self.presenter.presentError(message, isFirstNameError: true, isLastNameError: true)
        }
    }
    
    func didAlreadyRegistered() {
        presenter.didNextStep(action: .moveToLogin)
    }
  
    func didInteractWithUrl(_ url: URL) {
        guard
            let webviewUrlString = WebServiceInterface.apiEndpoint(url.absoluteString),
            let webviewUrl = URL(string: webviewUrlString)
            else {
                return
        }
        
        presenter.didNextStep(action: .webview(url: webviewUrl))
    }
    
    func didTapBack() {
        presenter.didNextStep(action: .back)
    }
}
