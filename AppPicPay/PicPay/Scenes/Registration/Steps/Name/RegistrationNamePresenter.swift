import Core
import UI

protocol RegistrationNamePresenting: AnyObject {
    var viewController: RegistrationNameDisplay? { get set }
    func fillTextFields(with firstName: String, lastName: String)
    func presentTermsOfService()
    func showDefaultTitle()
    func buildTitle(_ message: String)
    func presentError(_ message: String, isFirstNameError: Bool, isLastNameError: Bool)
    func showLoadingAnimation()
    func hideLoadingAnimation()
    func hideErrorMessage()
    func didNextStep(action: RegistrationNameAction)
}

private extension RegistrationNamePresenter.Layout {
    enum Text {
        static let `default` = RegisterLocalizable.nameStepTitle.text
        static let withName = RegisterLocalizable.nameStepTitleWithName.text
    }
}

final class RegistrationNamePresenter: RegistrationNamePresenting {
    fileprivate struct Layout {}
    
    private let coordinator: RegistrationNameCoordinating
    weak var viewController: RegistrationNameDisplay?

    init(coordinator: RegistrationNameCoordinating) {
        self.coordinator = coordinator
    }
    
    func presentTermsOfService() {
        viewController?.displayTermsOfService()
    }
    
    func fillTextFields(with firstName: String, lastName: String) {
        viewController?.fillTextFields(with: firstName, lastName: lastName)
    }
    
    func buildTitle(_ message: String) {
        let title = NSMutableAttributedString(string: String(format: Layout.Text.withName, message))
        title.textColor(text: message, color: Palette.ppColorBranding300.color)
        viewController?.setTitle(title)
    }
    
    func showDefaultTitle() {
        viewController?.setTitle(NSMutableAttributedString(string: Layout.Text.default))
    }
    
    func showLoadingAnimation() {
        viewController?.showLoading()
    }
    
    func hideLoadingAnimation() {
        viewController?.hideLoading()
    }
    
    func presentError(_ message: String, isFirstNameError: Bool, isLastNameError: Bool) {
        viewController?.displayError(message, isFirstNameError: isFirstNameError, isLastNameError: isLastNameError)
    }
    
    func hideErrorMessage() {
        viewController?.hideErrorMessage()
    }
    
    func didNextStep(action: RegistrationNameAction) {
        coordinator.perform(action: action)
    }
}
