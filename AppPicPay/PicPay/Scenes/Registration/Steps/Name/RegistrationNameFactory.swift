import Foundation

enum RegistrationNameFactory {
    static func make(
        delegate: RegistrationNameDelegate,
        firstName: String?,
        lastName: String?,
        showTermsOfService: Bool
    ) -> RegistrationNameViewController {
        let service: RegistrationNameServicing = RegistrationNameService()
        let coordinator: RegistrationNameCoordinating = RegistrationNameCoordinator()
        let presenter: RegistrationNamePresenting = RegistrationNamePresenter(coordinator: coordinator)
        let viewModel = RegistrationNameViewModel(
            service: service,
            presenter: presenter,
            firstName: firstName,
            lastName: lastName,
            showTermsOfService: showTermsOfService
        )
        let viewController = RegistrationNameViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
