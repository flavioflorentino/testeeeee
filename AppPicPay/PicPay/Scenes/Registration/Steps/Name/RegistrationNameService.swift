import Core

protocol RegistrationNameServicing {
    func verifyName(_ name: String, completion: @escaping(ApiError?) -> Void)
}

final class RegistrationNameService: RegistrationNameServicing {
    func verifyName(_ name: String, completion: @escaping (ApiError?) -> Void) {
        let endpoint = RegisterEndpoint.verifyName(name)
        let decoder = JSONDecoder(.convertFromSnakeCase)
        Api<RegisterResponse<RegisterNameResponse>>(endpoint: endpoint).execute(jsonDecoder: decoder) { result in
            DispatchQueue.main.async {
                let mappedResult = result.map { $0.model }
                switch mappedResult {
                case.success(let response):
                    guard let error = response.error else {
                        completion(nil)
                        return
                    }
                    var requestError = RequestError()
                    requestError.message = error.description
                    completion(ApiError.badRequest(body: requestError))
                case .failure(let error):
                    completion(error)
                }
            }
        }
    }
}
