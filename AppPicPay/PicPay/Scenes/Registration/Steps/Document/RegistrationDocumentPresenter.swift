import AssetsKit
import UI
import UIKit

protocol RegistrationDocumentPresenting: AnyObject {
    var viewController: RegistrationDocumentDisplay? { get set }
    func didNextStep(action: RegistrationDocumentAction)
    func updateTexts(_ textsModel: DocumentFeature)
    func displayTermsOfService()
    func fillTextFields(withCpf cpf: String, dateOfBirth: String)
    func displayError(message: String, isCpfError: Bool, isDateOfBirthError: Bool)
    func displayDocumentAlreadyRegisteredAlert()
    func hideErrors()
    func showLoading()
    func hideLoading()
    func dismissKeyboard()
    func showDocumentHelp(title: String, message: String, buttonTitle: String)
    func showAlertMinimumAge()
}

final class RegistrationDocumentPresenter: RegistrationDocumentPresenting {
    private let coordinator: RegistrationDocumentCoordinating
    weak var viewController: RegistrationDocumentDisplay?

    init(coordinator: RegistrationDocumentCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: RegistrationDocumentAction) {
        coordinator.perform(action: action)
    }
    
    func updateTexts(_ textsModel: DocumentFeature) {
        let learnMoreButtonTitle = NSAttributedString(
            string: textsModel.buttonTitle,
            attributes: [
                .foregroundColor: Palette.ppColorBranding300.color,
                .underlineStyle: NSUnderlineStyle.single.rawValue,
                .font: UIFont.systemFont(ofSize: 14, weight: .regular)
            ]
        )
        viewController?.updateTexts(title: textsModel.title, description: textsModel.text, buttonTitle: learnMoreButtonTitle)
    }
    
    func displayTermsOfService() {
        viewController?.displayTermsOfService()
    }
    
    func fillTextFields(withCpf cpf: String, dateOfBirth: String) {
        viewController?.fillDocumentTextFields(cpf: cpf, dateOfBirth: dateOfBirth)
    }
    
    func displayError(message: String, isCpfError: Bool, isDateOfBirthError: Bool) {
        viewController?.displayError(message: message, isCpfError: isCpfError, isDateOfBirthError: isDateOfBirthError)
    }
    
    func displayDocumentAlreadyRegisteredAlert() {
        let alert = Alert(
            title: RegisterLocalizable.alertDocumentAlreadyRegisteredTitle.text,
            text: RegisterLocalizable.alertDocumentAlreadyRegisteredText.text
        )
        
        let loginButton = Button(
            title: RegisterLocalizable.alertDocumentAlreadyRegisteredLoginButton.text,
            type: .cta,
            action: .confirm
        )
        
        let forgotPasswordButton = Button(
            title: RegisterLocalizable.alertDocumentAlreadyRegisteredForgotPasswordButton.text,
            type: .cleanUnderlined,
            action: .close
        )
        
        alert.buttons = [loginButton, forgotPasswordButton]
        alert.showCloseButton = true
        
        viewController?.displayDocumentAlreadyRegisteredAlert(alert)
    }
    
    func hideErrors() {
        viewController?.hideErrors()
    }
    
    func showLoading() {
        viewController?.showLoading()
    }
    
    func hideLoading() {
        viewController?.hideLoading()
    }
    
    func dismissKeyboard() {
        viewController?.dismissKeyboard()
    }
    
    func showDocumentHelp(title: String, message: String, buttonTitle: String) {
        let alert = Alert(title: title, text: message)
        let okButton = Button(title: buttonTitle, type: .cta, action: .close)
        alert.buttons = [okButton]
        alert.showCloseButton = false
        
        viewController?.displayPopupHelp(alert)
    }
    
    func showAlertMinimumAge() {
        let alert = Alert(
            title: RegisterLocalizable.alertMinimumAgeTitle.text,
            text: RegisterLocalizable.alertMinimumAgeText.text
        )
        
        let loginButton = Button(
            title: RegisterLocalizable.alertMinimumAgeButton.text,
            type: .cta,
            action: .close
        )
        alert.image = Alert.Image(with: Resources.Icons.icoWarningBig.image) 
        alert.buttons = [loginButton]
        
        viewController?.displayPopupHelp(alert)
    }
}
