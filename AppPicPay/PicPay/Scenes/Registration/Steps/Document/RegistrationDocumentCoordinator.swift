protocol RegistrationDocumentCoordinatorDelegate: AnyObject {
    func moveToNextStep(withCpf cpf: String, dateOfBirth: String, invalidCpfCounter: Int, copyPasteCpf: Bool)
}

typealias RegistrationDocumentDelegate = RegistrationDocumentCoordinatorDelegate & RegistrationFlowCoordinatorDelegate

enum RegistrationDocumentAction {
    case back
    case moveToNextStep(withCpf: String, dateOfBirth: String, invalidCpfCounter: Int, copyPasteCpf: Bool)
    case alreadyRegistered
    case webview(url: URL)
}

protocol RegistrationDocumentCoordinating: AnyObject {
    var delegate: RegistrationDocumentDelegate? { get set }
    var viewController: UIViewController? { get set }
    func perform(action: RegistrationDocumentAction)
}

final class RegistrationDocumentCoordinator: RegistrationDocumentCoordinating {
    weak var delegate: RegistrationDocumentDelegate?
    weak var viewController: UIViewController?

    func perform(action: RegistrationDocumentAction) {
        switch action {
        case .back:
            delegate?.moveToPreviousRegistrationStep()
        case let .moveToNextStep(cpf, dateOfBirth, invalidCpfCounter, copyPasteCpf):
            delegate?.moveToNextStep(
                withCpf: cpf,
                dateOfBirth: dateOfBirth,
                invalidCpfCounter: invalidCpfCounter,
                copyPasteCpf: copyPasteCpf
            )
        case .alreadyRegistered:
            delegate?.moveToLogin()
        case .webview(let url):
            let webviewController = WebViewFactory.make(with: url)
            viewController?.navigationController?.present(PPNavigationController(rootViewController: webviewController), animated: true)
        }
    }
}
