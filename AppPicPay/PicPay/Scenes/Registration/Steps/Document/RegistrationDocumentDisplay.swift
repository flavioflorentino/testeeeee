protocol RegistrationDocumentDisplay: AnyObject {
    func fillDocumentTextFields(cpf: String, dateOfBirth: String)
    func updateTexts(title: String, description: String, buttonTitle: NSAttributedString)
    func displayTermsOfService()
    func displayError(message: String, isCpfError: Bool, isDateOfBirthError: Bool)
    func displayDocumentAlreadyRegisteredAlert(_ alert: Alert)
    func hideErrors()
    func showLoading()
    func hideLoading()
    func dismissKeyboard()
    func displayPopupHelp(_ popup: Alert)
}
