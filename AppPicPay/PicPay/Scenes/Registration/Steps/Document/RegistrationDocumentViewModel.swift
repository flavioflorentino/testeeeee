import AnalyticsModule
import Core

protocol RegistrationDocumentViewModelInputs: AnyObject {
    func trackView()
    func displayTermsOfServiceIfNeeded()
    func setupViewContent()
    func updateDocument(withCpf cpf: String?, dateOfBirth: String?)
    func didConfirm()
    func didAlreadyRegistered()
    func didInteractWithUrl(_ url: URL)
    func didLearnMore()
    func didCopyPasteCpf()
    func didLogin()
    func didForgotPassword()
    func didTapBack()
}

final class RegistrationDocumentViewModel {
    private enum Constants {
        static let minimumCpfLength = 14
        static let minimumDateOfBirthLength = 10
        static let minimumOfAge = 16
        static let codeDocumentInvalid = 5_060
        static let codeDocumentAlreadyRegistered = 5_061
    }
    
    private let service: RegistrationDocumentServicing
    private let presenter: RegistrationDocumentPresenting
    private var model: RegistrationDocumentModel
    
    private var invalidCpfCounter = 0
    private var copyPasteCpf = false
    
    private lazy var defaultDescriptionText: String = {
        var replacementString = ""
        if let name = model.name {
            replacementString = ", \(name)"
        }
        
        return String(format: RegisterLocalizable.documentStepDescription.text, replacementString)
    }()
    
    private lazy var defaultFormText = DocumentFeature(
        title: RegisterLocalizable.documentStepTitle.text,
        text: defaultDescriptionText,
        buttonTitle: DefaultLocalizable.learnMore.text
    )

    init(service: RegistrationDocumentServicing, presenter: RegistrationDocumentPresenting, model: RegistrationDocumentModel) {
        self.service = service
        self.presenter = presenter
        self.model = model
    }
    
    private func isValidCpf(_ cpf: String) -> Bool {
        cpf.length >= Constants.minimumCpfLength
    }
    
    private func isValidDateOfBirth(_ dateOfBirth: String) -> Bool {
        dateOfBirth.length >= Constants.minimumDateOfBirthLength
    }
    
    private func validate(cpf: String, dateOfBirth: String) -> Bool {
        let invalidCpf = !isValidCpf(cpf)
        let invalidDateOfBirth = !isValidDateOfBirth(dateOfBirth)
        
        if invalidCpf, invalidDateOfBirth {
            Analytics.shared.log(RegistrationEvent.documentError(.invalidNumber))
            
            presenter.displayError(
                message: RegisterLocalizable.invalidCpfAndDateOfBirth.text,
                isCpfError: true,
                isDateOfBirthError: true
            )
            return false
        }
        
        if invalidCpf {
            Analytics.shared.log(RegistrationEvent.documentError(.invalidNumber))
            
            presenter.displayError(
                message: RegisterLocalizable.invalidCpf.text,
                isCpfError: true,
                isDateOfBirthError: false
            )
            return false
        }
        
        if invalidDateOfBirth {
            presenter.displayError(
                message: RegisterLocalizable.invalidDateOfBirth.text,
                isCpfError: false,
                isDateOfBirthError: true
            )
            return false
        } else if !validateMinimumAge(dateOfBirthString: dateOfBirth) {
            presenter.showAlertMinimumAge()
            return false
        }
        
        return true
    }
    
    func validateMinimumAge(dateOfBirthString: String) -> Bool {
        let calendar = Calendar.current
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.calendar = calendar
        let dateCurrent = Date()

        guard let dateOfBirth = dateFormatter.date(from: dateOfBirthString) else {
            return false
        }

        let components: Set<Calendar.Component> = [.second, .minute, .hour, .day, .month, .year]
        
        guard let differenceYear = calendar.dateComponents(components, from: dateOfBirth, to: dateCurrent).year else {
            return false
        }
        
        return differenceYear >= Constants.minimumOfAge
    }
    
    private func validateOnApi(cpf: String, dateOfBirth: String) {
        presenter.dismissKeyboard()
        presenter.hideErrors()
        presenter.showLoading()
        
        service.validateDocument(cpf: cpf, dateOfBirth: dateOfBirth) { [weak self] result in
            guard let self = self else {
                return
            }
            self.presenter.hideLoading()
            
            switch result {
            case .success:
                self.trackSuccessToNextStep()
                let action: RegistrationDocumentAction = .moveToNextStep(
                    withCpf: cpf,
                    dateOfBirth: dateOfBirth,
                    invalidCpfCounter: self.invalidCpfCounter,
                    copyPasteCpf: self.copyPasteCpf
                )
                self.presenter.didNextStep(action: action)
            case .failure(let error):
                self.displayError(error)
            }
        }
    }
    
    private func displayError(_ error: ApiError) {
        let message = parseErrorMessage(error: error) ?? DefaultLocalizable.requestError.text
        
        guard let code = getStatusCodeError(error) else {
            presenter.displayError(message: message, isCpfError: true, isDateOfBirthError: true)
            return
        }
        
        trackDocumentEventError(code: code)
        countInvalidCpfInputs(code: code)
        
        guard
            service.documentInUseDialog,
            code == Constants.codeDocumentAlreadyRegistered
        else {
            presenter.displayError(message: message, isCpfError: true, isDateOfBirthError: true)
            return
        }
        
        presenter.displayDocumentAlreadyRegisteredAlert()
        Analytics.shared.log(RegistrationEvent.documentModalViewed)
    }
    
    private func parseErrorMessage(error: ApiError) -> String? {
        switch error {
        case .badRequest(let data):
            return data.message
        case .connectionFailure:
            return DefaultLocalizable.errorConnectionViewSubtitle.text
        default:
            return nil
        }
    }
    
    private func getStatusCodeError(_ error: ApiError) -> Int? {
        guard
            let codeString = error.requestError?.code,
            let code = Int(codeString)
        else {
            return nil
        }
        
        return code
    }
    
    private func trackDocumentEventError(code: Int) {
        if code == Constants.codeDocumentInvalid {
            Analytics.shared.log(RegistrationEvent.documentError(.invalidNumber))
        } else if code == Constants.codeDocumentAlreadyRegistered {
            Analytics.shared.log(RegistrationEvent.documentError(.errorInserting))
            Analytics.shared.log(RegistrationEvent.documentInUse)
        }
    }
    
    private func trackSuccessToNextStep() {
        Analytics.shared.log(RegistrationEvent.cpf)
    }

    private func countInvalidCpfInputs(code: Int) {
        guard code == Constants.codeDocumentInvalid else {
            return
        }
        invalidCpfCounter += 1
    }
}

extension RegistrationDocumentViewModel: RegistrationDocumentViewModelInputs {
    func trackView() {
        Analytics.shared.log(RegistrationEvent.registerDocument)
    }
    
    func displayTermsOfServiceIfNeeded() {
        guard model.showTermsOfService else {
            return
        }
        presenter.displayTermsOfService()
    }
    
    func setupViewContent() {
        presenter.updateTexts(defaultFormText)
        
        guard model.cpf.isNotEmpty, model.dateOfBirth.isNotEmpty else {
            return
        }
        
        presenter.fillTextFields(withCpf: model.cpf, dateOfBirth: model.dateOfBirth)
    }
    
    func updateDocument(withCpf cpf: String?, dateOfBirth: String?) {
        model.cpf = cpf ?? ""
        model.dateOfBirth = dateOfBirth ?? ""
    }
    
    func didConfirm() {
        guard validate(cpf: model.cpf, dateOfBirth: model.dateOfBirth) else {
            return
        }
        validateOnApi(cpf: model.cpf, dateOfBirth: model.dateOfBirth)
    }
    
    func didAlreadyRegistered() {
        presenter.didNextStep(action: .alreadyRegistered)
    }
    
    func didInteractWithUrl(_ url: URL) {
        guard
            let webviewUrlString = WebServiceInterface.apiEndpoint(url.absoluteString),
            let webviewUrl = URL(string: webviewUrlString)
        else {
            return
        }
        presenter.didNextStep(action: .webview(url: webviewUrl))
    }
    
    func didLearnMore() {
        Analytics.shared.log(RegistrationEvent.documentHelp)
        
        presenter.showDocumentHelp(
            title: RegisterLocalizable.alertDocumentHelpTitle.text,
            message: RegisterLocalizable.alertDocumentHelpText.text,
            buttonTitle: DefaultLocalizable.btOkUnderstood.text
        )
    }
    
    func didLogin() {
        didAlreadyRegistered()
        Analytics.shared.log(RegistrationEvent.documentModalOptionSelected(.login))
    }
    
    func didForgotPassword() {
        guard let url = URL(string: service.documentInUseFaqURL) else {
            return
        }
        
        presenter.didNextStep(action: .webview(url: url))
        Analytics.shared.log(RegistrationEvent.documentModalOptionSelected(.forgotPassword))
    }
    
    func didCopyPasteCpf() {
        copyPasteCpf = true
    }
    
    func didTapBack() {
        presenter.didNextStep(action: .back)
    }
}
