import Core
import FeatureFlag

protocol RegistrationDocumentServicing {
    var documentInUseDialog: Bool { get }
    var documentInUseFaqURL: String { get }
    func validateDocument(cpf: String, dateOfBirth: String, completion: @escaping (Result<Void, ApiError>) -> Void)
}

final class RegistrationDocumentService: RegistrationDocumentServicing {
    typealias Dependencies = HasMainQueue & HasFeatureManager
    private let dependencies: Dependencies
    
    var documentInUseDialog: Bool {
        dependencies.featureManager.isActive(.documentInUseDialog)
    }
    
    var documentInUseFaqURL: String {
        dependencies.featureManager.text(.documentInUseFaqURL)
    }
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func validateDocument(cpf: String, dateOfBirth: String, completion: @escaping (Result<Void, ApiError>) -> Void) {
        let endpoint = RegisterEndpoint.isValidCpfAndBirth(
            cpf: cpf,
            birth: dateOfBirth,
            isCompliance: dependencies.featureManager.isActive(.registrationCompliance)
        )
        let decoder = JSONDecoder(.convertFromSnakeCase)
        Api<RegisterResponse<NoContent>>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let responseData):
                    guard let error = responseData.model.error else {
                        completion(.success)
                        return
                    }
                    var requestError = RequestError()
                    requestError.code = String(error.id)
                    requestError.message = error.description
                    completion(.failure(ApiError.badRequest(body: requestError)))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}
