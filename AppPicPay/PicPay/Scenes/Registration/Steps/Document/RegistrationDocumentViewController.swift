import UI

private extension RegistrationDocumentViewController.Layout {
    static let textFieldHeight: CGFloat = Spacing.base05
    static let buttonHeight: CGFloat = Spacing.base06
    static let learnMorelinkHeight: CGFloat = Spacing.base03
}

final class RegistrationDocumentViewController: RegistrationBaseViewController<RegistrationDocumentViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale600.color
        label.font = UIFont.systemFont(ofSize: 18)
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale400.color
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.setContentHuggingPriority(.defaultLow, for: .vertical)
        return label
    }()
    
    private lazy var learnMoreButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(tapLearnMoreButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var learnMoreView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var cpfTextField: RoundedTextField = {
        let textField = RoundedTextField()
        textField.cornerRadius = Layout.textFieldHeight / 2
        textField.placeholder = RegisterLocalizable.cpfPlaceholder.text
        textField.keyboardType = .numberPad
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        textField.delegate = self
        return textField
    }()
    
    private lazy var dateOfBirthTextField: RoundedTextField = {
        let textField = RoundedTextField()
        textField.cornerRadius = Layout.textFieldHeight / 2
        textField.placeholder = RegisterLocalizable.dateOfBirthPlaceholder.text
        textField.keyboardType = .numberPad
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        textField.delegate = self
        return textField
    }()
    
    private lazy var confirmButton: UIPPButton = {
        let button = UIPPButton(title: DefaultLocalizable.advance.text, target: self, action: #selector(tapConfirmButton), type: .cta)
        button.cornerRadius = Layout.buttonHeight / 2
        return button
    }()
    
    private lazy var stepDescriptionStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, descriptionLabel, learnMoreView])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var inputStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [cpfTextField, dateOfBirthTextField, errorLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var cpfNumberMasker: TextFieldMasker = {
        let cpfMask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf)
        return TextFieldMasker(textMask: cpfMask)
    }()
    
    private lazy var dateNumberMasker: TextFieldMasker = {
        let dateMask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.date)
        return TextFieldMasker(textMask: dateMask)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.displayTermsOfServiceIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.setupViewContent()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        openKeyboardIfNecessary(cpfTextField)
        viewModel.trackView()
    }
    
    override func buildViewHierarchy() {
        super.buildViewHierarchy()
        learnMoreView.addSubview(learnMoreButton)
        contentStackView.addArrangedSubviews(stepDescriptionStackView, inputStackView, confirmButton)
        view.addSubview(termsOfServiceView)
    }
    
    override func configureViews() {
        super.configureViews()
        cpfNumberMasker.bind(to: cpfTextField)
        dateNumberMasker.bind(to: dateOfBirthTextField)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        learnMoreView.layout {
            $0.height == Layout.learnMorelinkHeight
        }
        
        learnMoreButton.layout {
            $0.centerY == learnMoreView.centerYAnchor
            $0.centerX == learnMoreView.centerXAnchor
        }
        
        cpfTextField.layout {
            $0.height == Layout.textFieldHeight
        }
        
        dateOfBirthTextField.layout {
            $0.height == Layout.textFieldHeight
        }
        
        confirmButton.layout {
            $0.height == Layout.buttonHeight
        }
        
        termsOfServiceView.layout {
            $0.top == contentStackView.bottomAnchor + Spacing.base03
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
        }
    }
    
    override func tapBackButton() {
        viewModel.didTapBack()
    }
    
    override func didInteractWithUrl(_ url: URL) {
        viewModel.didInteractWithUrl(url)
    }
    
    override func tapAlreadyRegisteredButton() {
        viewModel.updateDocument(withCpf: cpfTextField.text, dateOfBirth: dateOfBirthTextField.text)
        viewModel.didAlreadyRegistered()
    }
}

@objc
private extension RegistrationDocumentViewController {
    func tapConfirmButton() {
        viewModel.updateDocument(withCpf: cpfTextField.text, dateOfBirth: dateOfBirthTextField.text)
        viewModel.didConfirm()
    }

    func tapLearnMoreButton() {
        viewModel.didLearnMore()
    }
}

extension RegistrationDocumentViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        viewModel.updateDocument(withCpf: cpfTextField.text, dateOfBirth: dateOfBirthTextField.text)
        textField == cpfTextField ? _ = dateOfBirthTextField.becomeFirstResponder() : viewModel.didConfirm()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let pasteboardString = UIPasteboard.general.string, !pasteboardString.isEmpty, string.contains(pasteboardString) {
            viewModel.didCopyPasteCpf()
        }
        
        guard textField == cpfTextField else {
            if string.isEmpty, textField.text?.count == range.length {
                dateOfBirthTextField.text = ""
                cpfTextField.becomeFirstResponder()
                return false
            }
            return true
        }
        
        if let newCPF = textField.text, newCPF.count >= 14, string.isNotEmpty {
            dateOfBirthTextField.becomeFirstResponder()
            return false
        }
        return true
    }
}

extension RegistrationDocumentViewController: RegistrationDocumentDisplay, RegistrationLoadingDisplay {
    func fillDocumentTextFields(cpf: String, dateOfBirth: String) {
        cpfTextField.text = cpf
        dateOfBirthTextField.text = dateOfBirth
        cpfTextField.layoutIfNeeded()
    }
    
    func updateTexts(title: String, description: String, buttonTitle: NSAttributedString) {
        titleLabel.text = title
        descriptionLabel.text = description
        learnMoreButton.setAttributedTitle(buttonTitle, for: .normal)
    }

    func displayTermsOfService() {
        termsOfServiceView.isHidden = false
    }

    func displayError(message: String, isCpfError: Bool, isDateOfBirthError: Bool) {
        errorLabel.text = message
        errorLabel.isHidden = false
        cpfTextField.hasError = isCpfError
        dateOfBirthTextField.hasError = isDateOfBirthError
    }
    
    func displayDocumentAlreadyRegisteredAlert(_ alert: Alert) {
        AlertMessage.showAlert(alert, controller: self) { [weak self] alertController, button, _ in
            switch button.action {
            case .confirm:
                alertController.dismiss(animated: true) {
                    self?.viewModel.didLogin()
                }
            case .close:
                self?.viewModel.didForgotPassword()
            default: ()
            }
        }
    }

    func hideErrors() {
        errorLabel.text = ""
        errorLabel.isHidden = true
        cpfTextField.hasError = false
        dateOfBirthTextField.hasError = false
    }

    func showLoading() {
        confirmButton.startLoadingAnimating()
    }

    func hideLoading() {
        confirmButton.stopLoadingAnimating()
    }
    
    func displayPopupHelp(_ popup: Alert) {
        AlertMessage.showAlert(popup, controller: self)
    }
}
