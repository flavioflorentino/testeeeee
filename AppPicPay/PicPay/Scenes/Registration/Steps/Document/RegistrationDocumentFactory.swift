import Foundation

enum RegistrationDocumentFactory {
    static func make(
        delegate: RegistrationDocumentDelegate,
        model: RegistrationDocumentModel
    ) -> RegistrationDocumentViewController {
        let container = DependencyContainer()
        let service: RegistrationDocumentServicing = RegistrationDocumentService(dependencies: container)
        let coordinator: RegistrationDocumentCoordinating = RegistrationDocumentCoordinator()
        let presenter: RegistrationDocumentPresenting = RegistrationDocumentPresenter(coordinator: coordinator)
        let viewModel = RegistrationDocumentViewModel(
            service: service,
            presenter: presenter,
            model: model
        )
        let viewController = RegistrationDocumentViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
