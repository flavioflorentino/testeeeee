struct RegistrationDocumentModel {
    var cpf: String
    var dateOfBirth: String
    let name: String?
    let showTermsOfService: Bool
    
    init(cpf: String?, dateOfBirth: String?, name: String?, showTermsOfService: Bool) {
        self.cpf = cpf ?? ""
        self.dateOfBirth = dateOfBirth ?? ""
        self.name = name
        self.showTermsOfService = showTermsOfService
    }
}
