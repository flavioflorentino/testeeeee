import UIKit

final class PhoneTextFieldDelegate: NSObject, UITextFieldDelegate {
    private let previousTextField: UITextField?
    
    init(previousTextField: UITextField? = nil) {
        self.previousTextField = previousTextField
        super.init()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        formatPhoneTextField(textField, charactersIn: range, replacementString: string)
    }
    
    private func formatPhoneTextField(_ textField: UITextField, charactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldValue = textField.text as NSString?
        if string.isEmpty && textField.text?.count == range.length {
            textField.text = ""
            previousTextField?.becomeFirstResponder()
            return false
        }
        
        var newPhone = textFieldValue?.replacingCharacters(in: range, with: string) ?? ""
        newPhone = newPhone.replacingOccurrences(of: "-", with: "").onlyNumbers
        
        if newPhone.isEmpty && !string.isEmpty {
            return false
        }
        
        if string == UIPasteboard.general.string ?? "" && newPhone.count <= 4 {
            textField.text = newPhone
            return false
        }
        
        if newPhone.count > 4 {
            textField.text = newPhone.formatAsPhoneNumberWithHyphen()
            return false
        }
        
        return true
    }
}
