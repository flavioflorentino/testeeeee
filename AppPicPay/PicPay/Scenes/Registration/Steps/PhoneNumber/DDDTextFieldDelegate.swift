import UIKit

final class DDDTextFieldDelegate: NSObject, UITextFieldDelegate {
    private let nextTextField: UITextField?
    
    init(nextTextField: UITextField? = nil) {
        self.nextTextField = nextTextField
        super.init()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        formatDDDTextfield(textField, charactersIn: range, replacementString: string)
    }
    
    private func formatDDDTextfield(_ textField: UITextField, charactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldValue = textField.text as NSString?
        let newDDD = textFieldValue?.replacingCharacters(in: range, with: string).onlyNumbers ?? ""
        
        if newDDD.isEmpty && !string.isEmpty {
            return false
        }
        
        if string == UIPasteboard.general.string ?? "" && newDDD.count < 2 {
            textField.text = newDDD
            return false
        }
        
        if newDDD.count >= 2 && !string.isEmpty {
            let range: Range<String.Index> = newDDD.startIndex..<newDDD.index(newDDD.startIndex, offsetBy: 2)
            textField.text = String(newDDD[range])
            nextTextField?.becomeFirstResponder()
            return false
        }

        return true
    }
}
