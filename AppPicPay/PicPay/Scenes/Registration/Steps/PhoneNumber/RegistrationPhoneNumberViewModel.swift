import AnalyticsModule
import Core

protocol RegistrationPhoneNumberViewModelInputs: AnyObject {
    func trackView()
    func trackPhoneConfirmationEditEvent()
    func setupViewContent()
    func fillPhoneNumberIfNeeded()
    func confirmPhoneNumber(with type: RegistrationPhoneCodeType, ddd: String?, number: String?)
    func requestCode(with type: RegistrationPhoneCodeType, ddd: String, number: String)
    func validatePhone(ddd: String?, number: String?) -> Bool
    func didAlreadyCode(ddd: String?, number: String?)
    func didAlreadyRegistered()
    func didInteractWithUrl(_ url: URL)
    func didTapBack()
}

enum RegistrationPhoneCodeType {
    case sms
    case whatsApp
}

final class RegistrationPhoneNumberViewModel {
    private let service: RegistrationPhoneNumberServicing
    private let presenter: RegistrationPhoneNumberPresenting
    private let phoneParametersProvider: PhoneVerificationParametersProvider
    private let validDDDLength = 2
    private let validPhoneRange = 9...10
    private let showTermsOfService: Bool
    private let ddd: String
    private let number: String
    private let eventName = "Cadastro telefone"

    init(
        service: RegistrationPhoneNumberServicing,
        presenter: RegistrationPhoneNumberPresenting,
        phoneParametersProvider: PhoneVerificationParametersProvider,
        ddd: String? = nil,
        number: String? = nil,
        showTermsOfService: Bool
    ) {
        self.service = service
        self.presenter = presenter
        self.phoneParametersProvider = phoneParametersProvider
        self.ddd = ddd ?? ""
        self.number = number ?? ""
        self.showTermsOfService = showTermsOfService
    }
    
    private func requestValidationCode(ddd: String, number: String, codeType: RegistrationPhoneCodeType) {
        presenter.showLoadingAnimation(with: codeType)
        presenter.disableControls()
        
        service.requestValidationCode(ddd: ddd, number: number, codeType: codeType) { [weak self] result in
            self?.presenter.hideLoadingAnimation(with: codeType)
            self?.presenter.enableControls()
            
            switch result {
            case let .success(response):
                self?.updateVerificationData(
                    date: Date(),
                    callDelay: response.callDelay,
                    callEnabled: response.callEnabled,
                    smsDelay: response.smsDelay ?? 0,
                    whatsAppDelay: response.whatsAppDelay ?? 0
                )
                self?.trackSuccessToNextStep()
                self?.presenter.didNextStep(action: .moveToNextStep(withDDD: ddd, number: number, codeType: codeType))
            case let .failure(error):
                let parsedError = self?.parseErrorMessage(error: error) ?? DefaultLocalizable.requestError.text
                self?.presenter.presentError(parsedError)
            }
        }
    }
    
    private func parseErrorMessage(error: ApiError) -> String? {
        switch error {
        case .badRequest(let data):
            return data.message
        case .connectionFailure:
            return DefaultLocalizable.errorConnectionViewSubtitle.text
        default:
            return nil
        }
    }
    
    private func updateVerificationData(date: Date, callDelay: Int, callEnabled: Bool, smsDelay: Int = 0, whatsAppDelay: Int = 0) {
        phoneParametersProvider.setPhoneVerificationSentDate(date)
        phoneParametersProvider.setPhoneVerificationSMSDelay(smsDelay)
        phoneParametersProvider.setPhoneVerificationWhatsAppDelay(whatsAppDelay)
        phoneParametersProvider.setPhoneVerificationCallDelay(callDelay)
        phoneParametersProvider.setEnabledPhoneVerificationCall(callEnabled)
    }
    
    private func trackRequestCodeEvent(with type: RegistrationPhoneCodeType) {
        let eventType: RegistrationEvent.ReceiveCodeOptionType = type == .sms ? .sms : .whatsApp
        Analytics.shared.log(RegistrationEvent.receiveCodeOption(eventType))
    }
    
    private func trackSuccessToNextStep() {
        Analytics.shared.log(RegistrationEvent.phone)
    }
    
    private func trackReceiveCodeWhatsAppFlowEvent(_ active: Bool) {
        Analytics.shared.log(RegistrationEvent.receiveCodeWhatsAppFlow(active))
    }
}

extension RegistrationPhoneNumberViewModel: RegistrationPhoneNumberViewModelInputs {
    func trackView() {
        PPAnalytics.trackEvent(eventName, properties: nil)
    }
    
    func trackPhoneConfirmationEditEvent() {
        Analytics.shared.log(RegistrationEvent.phoneConfirmation(.edit))
    }
    
    func setupViewContent() {
        trackReceiveCodeWhatsAppFlowEvent(service.whatsAppIsEbabled)
        
        if showTermsOfService {
            presenter.displayTermsOfService()
        }
    }
    
    func fillPhoneNumberIfNeeded() {
        guard ddd.isNotEmpty && number.isNotEmpty else {
            return
        }
        
        presenter.fillDDDTextField(with: ddd)
        presenter.fillPhoneTextField(with: number)
    }
    
    func confirmPhoneNumber(with type: RegistrationPhoneCodeType, ddd: String?, number: String?) {
        guard validatePhone(ddd: ddd, number: number) else {
            return
        }
        
        trackRequestCodeEvent(with: type)
        presenter.hideErrorsMessages()
        presenter.displayAlert(ddd: ddd, number: number, codeType: type)
    }
    
    func requestCode(with type: RegistrationPhoneCodeType, ddd: String, number: String) {
        guard validatePhone(ddd: ddd, number: number) else {
            return
        }
        
        Analytics.shared.log(RegistrationEvent.phoneConfirmation(.ok))
        presenter.hideErrorsMessages()
        requestValidationCode(ddd: ddd, number: number, codeType: type)
    }
    
    func validatePhone(ddd: String?, number: String?) -> Bool {
        guard
            let ddd = ddd,
            let number = number
            else {
                presenter.showPhoneNumberError()
                return false
        }
        
        var isValid = true
        let isValidNumberRange = validPhoneRange ~= number.count
        
        if ddd.count != validDDDLength && !isValidNumberRange {
            isValid = false
            presenter.showPhoneNumberError()
        } else if ddd.count != validDDDLength {
            isValid = false
            presenter.showDDDError()
        } else if !isValidNumberRange {
            isValid = false
            presenter.showNumberError()
        }
        
        return isValid
    }
    
    func didAlreadyCode(ddd: String?, number: String?) {
        Analytics.shared.log(RegistrationEvent.receiveCodeOption(.haveCode))
        presenter.didNextStep(action: .alreadyCode(withDDD: ddd ?? "", number: number ?? ""))
    }
    
    func didAlreadyRegistered() {
        presenter.didNextStep(action: .alreadyRegistered)
    }
    
    func didInteractWithUrl(_ url: URL) {
        guard
            let webviewUrlString = WebServiceInterface.apiEndpoint(url.absoluteString),
            let webviewUrl = URL(string: webviewUrlString)
            else {
                return
        }
        
        presenter.didNextStep(action: .webview(url: webviewUrl))
    }
    
    func didTapBack() {
        presenter.didNextStep(action: .back)
    }
}
