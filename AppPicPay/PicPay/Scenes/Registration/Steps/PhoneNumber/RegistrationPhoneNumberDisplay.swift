import UI

protocol RegistrationPhoneNumberDisplay: AnyObject {
    func fillDDDTextField(with ddd: String)
    func fillPhoneTextField(with number: String)
    func displayTermsOfService()
    func displayAlert(title: String, message: String, ddd: String, number: String, codeType: RegistrationPhoneCodeType)
    func showLoadingAnimation(with codeType: RegistrationPhoneCodeType)
    func hideLoadingAnimation(with codeType: RegistrationPhoneCodeType)
    func displayError(_ message: String, isDDDError: Bool, isNumberError: Bool)
    func hideErrorsMessages()
    func disableControls()
    func enableControls()
}
