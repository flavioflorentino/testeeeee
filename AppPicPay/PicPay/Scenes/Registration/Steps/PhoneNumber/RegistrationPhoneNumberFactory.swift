import Foundation
import CoreLegacy

enum RegistrationPhoneNumberFactory {
    static func make(
        delegate: RegistrationPhoneDelegate,
        ddd: String?,
        number: String?,
        showTermsOfService: Bool
    ) -> RegistrationPhoneNumberViewController {
        let container = DependencyContainer()
        let service: RegistrationPhoneNumberServicing = RegistrationPhoneNumberService(dependencies: container)
        let coordinator: RegistrationPhoneNumberCoordinating = RegistrationPhoneNumberCoordinator()
        let presenter: RegistrationPhoneNumberPresenting = RegistrationPhoneNumberPresenter(coordinator: coordinator)
        let viewModel = RegistrationPhoneNumberViewModel(
            service: service,
            presenter: presenter,
            phoneParametersProvider: AppParameters.global(),
            ddd: ddd,
            number: number,
            showTermsOfService: showTermsOfService
            )
        let viewController = RegistrationPhoneNumberViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
