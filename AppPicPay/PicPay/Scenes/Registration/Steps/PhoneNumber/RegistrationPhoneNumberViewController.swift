import UI

private extension RegistrationPhoneNumberViewController.Layout {
    enum Style {
        static let inputCornerRadius: CGFloat = 24
        static let buttonCornerRadius: CGFloat = 24
        static let numberOfLines = 0
        static let descriptionFont: UIFont = .systemFont(ofSize: 14)
        static let descriptionTextColor = Palette.ppColorGrayscale400.color
        static let haveCodeButtonTextColor = Palette.ppColorBranding300.color
        static let alreadyRegisteredButtonTextColor = Palette.ppColorGrayscale500.color
        static let secundaryButtonFont: UIFont = .systemFont(ofSize: 12)
    }
    
    enum Size {
        static let inputWidth: CGFloat = 80
        static let inputHeight: CGFloat = 48
        static let buttonHeight: CGFloat = Spacing.base06
    }
}

final class RegistrationPhoneNumberViewController: RegistrationBaseViewController<RegistrationPhoneNumberViewModelInputs, UIView> {
    fileprivate struct Layout {}
    
    private var dddTextFieldHandler: DDDTextFieldDelegate?
    private var phoneTextFieldHandler: PhoneTextFieldDelegate?
    
    private lazy var stepDescriptionStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, descriptionLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var inputStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [phoneNumberInputStackView, errorLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var phoneNumberInputStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [dddTextField, phoneTextField])
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textAlignment, .center)
        label.text = RegisterLocalizable.phoneStepTitle.text
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale600.color)
            .with(\.textAlignment, .center)
        label.text = RegisterLocalizable.phoneStepText.text
        label.numberOfLines = Layout.Style.numberOfLines
        return label
    }()
    
    private lazy var dddTextField: RoundedTextField = {
        let textField = RoundedTextField()
        textField.cornerRadius = Layout.Style.inputCornerRadius
        textField.placeholder = RegisterLocalizable.phoneStepDDD.text
        textField.placeHolderTextColor = Colors.grayscale600.color
        textField.textAlignment = .center
        textField.backgroundColor = .clear
        textField.keyboardType = .numberPad
        textField.setBorderedStyle()
        return textField
    }()
    
    private lazy var phoneTextField: RoundedTextField = {
        let textField = RoundedTextField()
        textField.cornerRadius = Layout.Style.inputCornerRadius
        textField.placeholder = RegisterLocalizable.phoneStepPhoneNumber.text
        textField.placeHolderTextColor = Colors.grayscale600.color
        textField.backgroundColor = .clear
        textField.keyboardType = .numberPad
        textField.setBorderedStyle()
        return textField
    }()
    
    private lazy var smsButton: UIPPButton = {
        let button = UIPPButton()
        button.setBackgroundColor(Colors.branding600.color)
        button.setTitleColor(Colors.white.lightColor)
        button.cornerRadius = Layout.Style.buttonCornerRadius
        button.loadingTitleColor = Colors.white.color
        button.setTitle(RegisterLocalizable.phoneStepSMSTitle.text, for: .normal)
        button.addTarget(self, action: #selector(smsAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var haveCodeButton: UnderlinedButton = {
        let button = UnderlinedButton()
        button.setTitle(RegisterLocalizable.haveCode.text, for: .normal)
        button.updateColor(
            Colors.branding600.color,
            highlightedColor: Colors.branding600.color.withAlphaComponent(0.6),
            font: Typography.linkSecondary().font()
        )
        
        button.addTarget(self, action: #selector(haveCodeAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var supportStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base06
        stackView.axis = .vertical
        
        return stackView
    }()
    
    override func tapBackButton() {
        viewModel.didTapBack()
    }
    
    override func didInteractWithUrl(_ url: URL) {
        viewModel.didInteractWithUrl(url)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.setupViewContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.fillPhoneNumberIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        openKeyboardIfNecessary(dddTextField)
        viewModel.trackView()
    }
 
    override func buildViewHierarchy() {
        super.buildViewHierarchy()
        supportStackView.addArrangedSubviews(stepDescriptionStackView, inputStackView)
        contentStackView.addArrangedSubview(supportStackView)
        view.addSubviews(smsButton, haveCodeButton, termsOfServiceView)
    }
    
    override func configureViews() {
        super.configureViews()
        alreadyRegisteredButton.isHidden = true
        dddTextFieldHandler = DDDTextFieldDelegate(nextTextField: phoneTextField)
        dddTextField.delegate = dddTextFieldHandler
        phoneTextFieldHandler = PhoneTextFieldDelegate(previousTextField: dddTextField)
        phoneTextField.delegate = phoneTextFieldHandler
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        dddTextField.layout {
            $0.width == Layout.Size.inputWidth
            $0.height == Layout.Size.inputHeight
        }
        
        phoneTextField.layout {
            $0.height == Layout.Size.inputHeight
        }
        
        smsButton.layout {
            $0.leading == view.leadingAnchor + Spacing.base03
            $0.trailing == view.trailingAnchor - Spacing.base03
            $0.height == Layout.Size.buttonHeight
            primaryButtonBottomConstraint = $0.bottom == haveCodeButton.topAnchor + (isSmallScreenDevice ? Spacing.base01 : Spacing.base03)
        }
        
        haveCodeButton.layout {
            $0.centerX == view.centerXAnchor
            $0.bottom == view.compatibleSafeAreaLayoutGuide.bottomAnchor - (isSmallScreenDevice ? Spacing.base00 : Spacing.base03)
        }
        
        termsOfServiceView.layout {
            $0.leading == containerView.leadingAnchor
            $0.trailing == containerView.trailingAnchor
            $0.bottom == alreadyRegisteredButton.topAnchor
        }
    }
}

@objc
private extension RegistrationPhoneNumberViewController {
    func whatsAppAction() {
        viewModel.confirmPhoneNumber(with: .whatsApp, ddd: dddTextField.text, number: phoneTextField.text)
    }
    
    func smsAction() {
        viewModel.confirmPhoneNumber(with: .sms, ddd: dddTextField.text, number: phoneTextField.text)
    }
    
    func haveCodeAction() {
        viewModel.didAlreadyCode(ddd: dddTextField.text, number: phoneTextField.text)
    }
    
    func alreadyRegisteredAction() {
        viewModel.didAlreadyRegistered()
    }
}

extension RegistrationPhoneNumberViewController: RegistrationPhoneNumberDisplay {
    func fillDDDTextField(with ddd: String) {
        dddTextField.text = ddd
    }
    
    func fillPhoneTextField(with number: String) {
        phoneTextField.text = number
    }
    
    func displayTermsOfService() {
        termsOfServiceView.isHidden = false
    }
    
    func displayAlert(title: String, message: String, ddd: String, number: String, codeType: RegistrationPhoneCodeType) {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertView.addAction(UIAlertAction(title: DefaultLocalizable.btEdit.text, style: .default, handler: { [weak self] _ in
            self?.dddTextField.becomeFirstResponder()
            self?.viewModel.trackPhoneConfirmationEditEvent()
        }))
        
        alertView.addAction(UIAlertAction(title: DefaultLocalizable.btOk.text, style: .default, handler: { [weak self] _ in
            self?.viewModel.requestCode(with: codeType, ddd: ddd, number: number)
        }))
        
        present(alertView, animated: true)
    }
    
    func showLoadingAnimation(with codeType: RegistrationPhoneCodeType) {
        var style: UIActivityIndicatorView.Style = .gray
        if #available(iOS 13.0, *), traitCollection.userInterfaceStyle == .dark {
            style = .white
        }
        smsButton.startLoadingAnimating(style: style, isEnabled: false)
    }
    
    func hideLoadingAnimation(with codeType: RegistrationPhoneCodeType) {
        smsButton.stopLoadingAnimating()
    }
    
    func displayError(_ message: String, isDDDError: Bool, isNumberError: Bool) {
        errorLabel.text = message
        errorLabel.isHidden = false
        isDDDError ? dddTextField.hasError = isDDDError : phoneTextField.setBorderedStyle()
        isNumberError ? phoneTextField.hasError = isNumberError : phoneTextField.setBorderedStyle()
    }
    
    func hideErrorsMessages() {
        errorLabel.text = nil
        errorLabel.isHidden = true
        dddTextField.hasError = false
        phoneTextField.hasError = false
        
        dddTextField.setBorderedStyle()
        phoneTextField.setBorderedStyle()
    }
    
    func disableControls() {
        dddTextField.isEnabled = false
        phoneTextField.isEnabled = false
    }
    
    func enableControls() {
        dddTextField.isEnabled = true
        phoneTextField.isEnabled = true
    }
}
