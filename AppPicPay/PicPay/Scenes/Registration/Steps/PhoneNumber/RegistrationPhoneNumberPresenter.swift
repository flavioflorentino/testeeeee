import Core
import Foundation

protocol RegistrationPhoneNumberPresenting: AnyObject {
    var viewController: RegistrationPhoneNumberDisplay? { get set }
    func fillDDDTextField(with ddd: String)
    func fillPhoneTextField(with number: String)
    func displayTermsOfService()
    func showPhoneNumberError()
    func showDDDError()
    func showNumberError()
    func presentError(_ message: String)
    func hideErrorsMessages()
    func showLoadingAnimation(with codeType: RegistrationPhoneCodeType)
    func hideLoadingAnimation(with codeType: RegistrationPhoneCodeType)
    func disableControls()
    func enableControls()
    func displayAlert(ddd: String?, number: String?, codeType: RegistrationPhoneCodeType)
    func didNextStep(action: RegistrationPhoneNumberAction)
}

final class RegistrationPhoneNumberPresenter: RegistrationPhoneNumberPresenting {
    private let coordinator: RegistrationPhoneNumberCoordinating
    weak var viewController: RegistrationPhoneNumberDisplay?

    init(coordinator: RegistrationPhoneNumberCoordinating) {
        self.coordinator = coordinator
    }
    
    func fillDDDTextField(with ddd: String) {
        viewController?.fillDDDTextField(with: ddd)
    }
    
    func fillPhoneTextField(with number: String) {
        viewController?.fillPhoneTextField(with: number.formatAsPhoneNumberWithHyphen())
    }
    
    func displayTermsOfService() {
        viewController?.displayTermsOfService()
    }
    
    func showPhoneNumberError() {
        viewController?.displayError(RegisterLocalizable.invalidDDDAndPhone.text, isDDDError: true, isNumberError: true)
    }
    
    func showDDDError() {
        viewController?.displayError(RegisterLocalizable.invalidDDD.text, isDDDError: true, isNumberError: false)
    }
    
    func showNumberError() {
        viewController?.displayError(RegisterLocalizable.invalidPhone.text, isDDDError: false, isNumberError: true)
    }
    
    func presentError(_ message: String) {
        viewController?.displayError(message, isDDDError: true, isNumberError: true)
    }
    
    func hideErrorsMessages() {
        viewController?.hideErrorsMessages()
    }
    
    func showLoadingAnimation(with codeType: RegistrationPhoneCodeType) {
        viewController?.showLoadingAnimation(with: codeType)
    }
    
    func hideLoadingAnimation(with codeType: RegistrationPhoneCodeType) {
        viewController?.hideLoadingAnimation(with: codeType)
    }
    
    func disableControls() {
        viewController?.disableControls()
    }
    
    func enableControls() {
        viewController?.enableControls()
    }
    
    func displayAlert(ddd: String?, number: String?, codeType: RegistrationPhoneCodeType) {
        guard let ddd = ddd, let number = number else {
            return
        }
        
        viewController?.displayAlert(
            title: RegisterLocalizable.alertPhoneConfirmTitle.text,
            message: String(format: RegisterLocalizable.alertPhoneConfirmMessage.text, ddd, number),
            ddd: ddd,
            number: number,
            codeType: codeType
        )
    }
    
    func didNextStep(action: RegistrationPhoneNumberAction) {
        coordinator.perform(action: action)
    }
}
