import Core
import FeatureFlag

protocol RegistrationPhoneNumberServicing {
    var whatsAppIsEbabled: Bool { get }
    func requestValidationCode(
        ddd: String,
        number: String,
        codeType: RegistrationPhoneCodeType,
        completion: @escaping(Result<PhoneNumberCodeResponse, ApiError>) -> Void
    )
}

final class RegistrationPhoneNumberService: RegistrationPhoneNumberServicing {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    private let jsonDecoder: JSONDecoder
    
    var whatsAppIsEbabled: Bool {
        dependencies.featureManager.isActive(.phoneRegisterWithWhatsApp)
    }
    
    init(dependencies: Dependencies, jsonDecoder: JSONDecoder = JSONDecoder()) {
        self.dependencies = dependencies
        self.jsonDecoder = jsonDecoder
        self.jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
    }
    
    func requestValidationCode(
        ddd: String,
        number: String,
        codeType: RegistrationPhoneCodeType,
        completion: @escaping (Result<PhoneNumberCodeResponse, ApiError>) -> Void
    ) {
        let endpoint = codeType == .sms ? RegisterEndpoint.requestSmsCode(ddd: ddd, number: number) : RegisterEndpoint.requestWhatsAppCode(ddd: ddd, number: number)
        let decoder = JSONDecoder(.convertFromSnakeCase)
        Api<RegisterResponse<PhoneNumberCodeResponse>>(endpoint: endpoint).execute(jsonDecoder: decoder) { result in
            DispatchQueue.main.async {
                let mappedResult = result.map { $0.model }
                switch mappedResult {
                case let .success(response):
                    guard let data = response.data else {
                        if let error = response.error {
                            var requestError = RequestError()
                            requestError.message = error.description
                            return completion(.failure(ApiError.badRequest(body: requestError)))
                        }
                        
                        return completion(.failure(ApiError.unknown(nil)))
                    }
                    
                    return completion(.success(data))
                case let .failure(error):
                    return completion(.failure(error))
                }
            }
        }
    }
}
