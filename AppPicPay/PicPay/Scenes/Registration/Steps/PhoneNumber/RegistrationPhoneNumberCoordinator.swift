import UIKit

typealias RegistrationPhoneDelegate = RegistrationFlowCoordinatorDelegate & RegistrationPhoneCodeCoordinatorDelegate

enum RegistrationPhoneNumberAction {
    case back
    case moveToNextStep(withDDD: String, number: String, codeType: RegistrationPhoneCodeType)
    case alreadyCode(withDDD: String, number: String)
    case alreadyRegistered
    case webview(url: URL)
}

protocol RegistrationPhoneNumberCoordinating: AnyObject {
    var delegate: RegistrationPhoneDelegate? { get set }
    var viewController: UIViewController? { get set }
    func perform(action: RegistrationPhoneNumberAction)
}

final class RegistrationPhoneNumberCoordinator: RegistrationPhoneNumberCoordinating {
    weak var delegate: RegistrationPhoneDelegate?
    weak var viewController: UIViewController?

    func perform(action: RegistrationPhoneNumberAction) {
        switch action {
        case .back:
            delegate?.moveToPreviousRegistrationStep()
        case let .moveToNextStep(ddd, number, codeType):
            let controller = RegistrationPhoneCodeFactory.make(delegate: delegate, ddd: ddd, number: number, codeType: codeType)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case let .alreadyCode(ddd, number):
            let controller = RegistrationPhoneCodeFactory.make(delegate: delegate, ddd: ddd, number: number)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .alreadyRegistered:
            delegate?.moveToLogin()
        case .webview(let url):
            let webviewController = WebViewFactory.make(with: url)
            viewController?.navigationController?.present(PPNavigationController(rootViewController: webviewController), animated: true)
        }
    }
}
