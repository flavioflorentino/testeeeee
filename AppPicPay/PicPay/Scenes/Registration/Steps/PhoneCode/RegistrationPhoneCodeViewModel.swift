import AnalyticsModule
import Core

protocol RegistrationPhoneCodeViewModelInputs: AnyObject {
    var timerDelay: Timer? { get }
    var timerCallDelay: Timer? { get }
    var validCodeLength: Int { get }
    func trackView()
    func loadDescription()
    func configureTimers()
    func verifyPhoneNumber(with code: String?)
    func confirmPhoneWithResendCode()
    func confirmPhoneWithReceiveCall()
    func requestCode()
    func requestCall()
    func didAlreadyRegister()
    func didHelp()
    func didEditPhoneNumber()
    func didTapBack()
}

final class RegistrationPhoneCodeViewModel {
    private let service: RegistrationPhoneCodeServicing
    private let presenter: RegistrationPhoneCodePresenting
    private var ddd: String
    private var number: String
    private let codeType: RegistrationPhoneCodeType
    private(set) var timerDelay: Timer?
    private(set) var timerCallDelay: Timer?
    let validCodeLength = 4
    private let validDDDLength = 2
    private let validPhoneRange = 9...10
    
    private var isDDDValid: Bool {
        ddd.count == validDDDLength
    }
    
    private var isNumberValid: Bool {
        validPhoneRange ~= number.count
    }
    
    private var isPhoneValid: Bool {
       isDDDValid && isNumberValid
    }
    
    private var timerDelayIsNotValid: Bool {
        timerDelay?.isValid == false
    }
    
    private var timerCallDelayIsNotValid: Bool {
        timerCallDelay?.isValid == false
    }

    init(
        service: RegistrationPhoneCodeServicing,
        presenter: RegistrationPhoneCodePresenting,
        ddd: String,
        number: String,
        codeType: RegistrationPhoneCodeType
    ) {
        self.service = service
        self.presenter = presenter
        self.ddd = ddd
        self.number = number
        self.codeType = codeType
    }
}

private extension RegistrationPhoneCodeViewModel {
    func displayDescription() {
        if isPhoneValid {
            presenter.displayDescription(ddd: ddd, number: number)
        } else {
            presenter.displayDescription()
        }
    }
    
    func displayCallButtonIfNeeded() {
        if service.isPhoneVerificationCallEnabled {
            presenter.displayCallButton()
        }
    }
    
    func updateButtonTitles() {
        presenter.updateCodeButtonTitle()
        
        if service.isPhoneVerificationCallEnabled {
            presenter.updateCallButtonTitle()
        }
    }
    
    func parseErrorMessage(error: ApiError) -> String? {
        switch error {
        case .badRequest(let data):
            return data.message
        case .connectionFailure:
            return DefaultLocalizable.errorConnectionViewSubtitle.text
        default:
            return nil
        }
    }
    
    func configureCodeTimerIfNeeded() {
        let phoneVerificationSentDate = service.phoneVerificationSentDate
        let currentDelay = codeType == .sms ? service.phoneVerificationSMSDelay : service.phoneVerificationWhatsAppDelay
        
        guard currentDelay > 0 else {
            return
        }
        
        timerDelay = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { [weak self] _ in
            self?.scheduledTimer(
                phoneVerificationDate: phoneVerificationSentDate,
                delay: currentDelay,
                update: { interval in
                    self?.presenter.updateCodeButtonTitle(interval: interval)
                    self?.presenter.codeButtonIsActive(false)
                }, completion: {
                    self?.timerDelay?.invalidate()
                    self?.presenter.codeButtonIsActive(true)
                })
        })
        
        timerDelay?.fire()
    }
    
    func configureCallTimerIfNeeded() {
        let phoneVerificationSentDate = service.phoneVerificationSentDate
        let phoneVerificationCallDelay = service.phoneVerificationCallDelay
        
        guard
            service.isPhoneVerificationCallEnabled,
            phoneVerificationCallDelay > 0
            else {
                return
        }
        
        timerCallDelay = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { [weak self] _ in
            self?.scheduledTimer(
                phoneVerificationDate: phoneVerificationSentDate,
                delay: phoneVerificationCallDelay,
                update: { interval in
                    self?.presenter.updateCallButtonTitle(interval: interval)
                    self?.presenter.callButtonIsActive(false)
                }, completion: {
                    self?.timerCallDelay?.invalidate()
                    self?.presenter.callButtonIsActive(true)
                })
        })
        
        timerCallDelay?.fire()
    }
    
    func scheduledTimer(phoneVerificationDate: Date, delay: Int, update: (Int) -> Void, completion: () -> Void) {
        var interval = Int(phoneVerificationDate.timeIntervalSinceNow)
        interval = delay - (interval * -1)
        
        update(interval)
        
        if interval <= 0 {
            completion()
        }
    }
    
    func displayError(_ error: ApiError) {
        let message = parseErrorMessage(error: error) ?? DefaultLocalizable.requestError.text
        presenter.displayError(message: message)
    }
    
    func displayLoadingState() {
        presenter.hideError()
        presenter.showLoading()
        presenter.disableControls()
        
        if timerDelayIsNotValid {
            presenter.codeButtonIsActive(false)
        }
        
        if timerCallDelayIsNotValid {
            presenter.callButtonIsActive(false)
        }
    }
    
    func displayResultState() {
        presenter.hideLoading()
        presenter.enableControls()
        
        if timerDelayIsNotValid {
            presenter.codeButtonIsActive(true)
        }
        
        if timerCallDelayIsNotValid {
            presenter.callButtonIsActive(true)
        }
    }
    
    func handleResult(_ result: Result<PhoneNumberCodeResponse, ApiError>) {
        displayResultState()
        
        switch result {
        case let .success(response):
            service.updateVerificationData(
                date: Date(),
                smsDelay: response.smsDelay ?? 0,
                whatsAppDelay: response.whatsAppDelay ?? 0,
                callDelay: response.callDelay,
                callEnabled: response.callEnabled
            )
            configureTimers()
        case let .failure(error):
            displayError(error)
        }
    }
    
    private func trackSuccessToNextStep() {
        Analytics.shared.log(RegistrationEvent.phoneCode)
    }
    
    private func trackPhoneCodeVerificationEdit() {
        Analytics.shared.log(RegistrationEvent.smsConfirmation(.editNumber))
        Analytics.shared.log(RegistrationEvent.phoneConfirmation(.edit))
    }
    
    private func trackPhoneCodeVerificationOk(verificationOptionType: RegistrationEvent.VerificationOptionType) {
        Analytics.shared.log(RegistrationEvent.phoneConfirmation(.ok))
        Analytics.shared.log(RegistrationEvent.verificationOption(verificationOptionType))
    }
}

extension RegistrationPhoneCodeViewModel: RegistrationPhoneCodeViewModelInputs {
    func trackView() {
        Analytics.shared.log(RegistrationEvent.phoneCodeVerification)
    }
    
    func loadDescription() {
        displayDescription()
        displayCallButtonIfNeeded()
        updateButtonTitles()
    }
    
    func configureTimers() {
        configureCodeTimerIfNeeded()
        configureCallTimerIfNeeded()
    }
    
    func verifyPhoneNumber(with code: String?) {
        guard
            let validCode = code,
            validCode.count == validCodeLength
            else {
                presenter.displayError(message: RegisterLocalizable.invalidCode.text)
                return
        }
        
        displayLoadingState()
        
        service.verifyPhoneNumber(with: validCode) { [weak self] result in
            self?.displayResultState()
            
            switch result {
            case let .success(response):
                self?.ddd = response.ddd
                self?.number = response.number
                self?.trackSuccessToNextStep()
                self?.presenter.didNextStep(action: .moveToNextStep(with: response.ddd, number: response.number))
            case let .failure(error):
                Analytics.shared.log(RegistrationEvent.codeError)
                self?.displayError(error)
            }
        }
    }
    
    func confirmPhoneWithResendCode() {
        Analytics.shared.log(RegistrationEvent.smsConfirmation(.resendCode))
        
        guard isPhoneValid else {
            presenter.displayAlertError()
            return
        }
        
        presenter.displayConfirmResendCodeAlert(ddd: ddd, number: number)
    }
    
    func confirmPhoneWithReceiveCall() {
        guard isPhoneValid else {
            presenter.displayAlertError()
            return
        }
        
        presenter.displayConfirmReceiveCallAlert(ddd: ddd, number: number)
    }
    
    func requestCode() {
        trackPhoneCodeVerificationOk(verificationOptionType: .resendSMS)
        
        guard isPhoneValid else {
            presenter.displayAlertError()
            return
        }
        
        displayLoadingState()
        
        service.requestValidationCode(ddd: ddd, number: number, codeType: codeType) { [weak self] result in
            self?.handleResult(result)
        }
    }
    
    func requestCall() {
        trackPhoneCodeVerificationOk(verificationOptionType: .receiveCall)
        
        guard isPhoneValid else {
            presenter.displayAlertError()
            return
        }
        
        displayLoadingState()
        
        service.requestCallVerification(ddd: ddd, number: number) { [weak self] result in
            self?.handleResult(result)
        }
    }
    
    func didAlreadyRegister() {
        presenter.didNextStep(action: .alreadyRegistered)
    }
    
    func didHelp() {
        Analytics.shared.log(RegistrationEvent.smsConfirmation(.help))
        presenter.didNextStep(action: .openHelp(with: service.helpPath))
    }
    
    func didEditPhoneNumber() {
        trackPhoneCodeVerificationEdit()
        presenter.didNextStep(action: .editPhoneNumber)
    }
    
    func didTapBack() {
        presenter.didNextStep(action: .back)
    }
}
