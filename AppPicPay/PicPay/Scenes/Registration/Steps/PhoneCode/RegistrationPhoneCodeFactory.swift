import Foundation
import CoreLegacy

enum RegistrationPhoneCodeFactory {
    static func make(
        delegate: RegistrationPhoneCodeCoordinatorDelegate?,
        ddd: String,
        number: String,
        codeType: RegistrationPhoneCodeType = .sms
    ) -> RegistrationPhoneCodeViewController {
        let container = DependencyContainer()
        let service: RegistrationPhoneCodeServicing = RegistrationPhoneCodeService(dependencies: container, phoneParametersProvider: AppParameters.global())
        let coordinator: RegistrationPhoneCodeCoordinating = RegistrationPhoneCodeCoordinator(dependencies: container)
        let presenter: RegistrationPhoneCodePresenting = RegistrationPhoneCodePresenter(coordinator: coordinator, dependencies: container)
        let viewModel = RegistrationPhoneCodeViewModel(
            service: service,
            presenter: presenter,
            ddd: ddd,
            number: number,
            codeType: codeType
        )
        let viewController = RegistrationPhoneCodeViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
