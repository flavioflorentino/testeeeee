import Core

protocol RegistrationPhoneCodeDisplay: AnyObject {
    func displayDescription(_ description: NSAttributedString)
    func displayCallButton()
    func displayConfirmResendCodeAlert(_ alert: Alert)
    func displayConfirmReceiveCallAlert(_ alert: Alert)
    func displayAlertError(_ alert: Alert)
    func codeButtonIsActive(_ active: Bool)
    func callButtonIsActive(_ active: Bool)
    func updateCodeButtonTitle(_ title: String)
    func updateCallButtonTitle(_ title: String)
    func displayError(message: String)
    func hideError()
    func showLoading()
    func hideLoading()
    func disableControls()
    func enableControls()
}
