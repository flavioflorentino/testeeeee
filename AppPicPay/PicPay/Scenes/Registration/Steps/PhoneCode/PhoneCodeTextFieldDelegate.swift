import UIKit

protocol PhoneCodeTextFieldHandlerDelegate: AnyObject {
    func phoneCodeComplete(textField: UITextField)
}

final class PhoneCodeTextFieldHandler: NSObject, UITextFieldDelegate {
    private let minimumSizeValidCode: Int
    weak var delegate: PhoneCodeTextFieldHandlerDelegate?
    
    init(minimumSizeValidCode: Int, delegate: PhoneCodeTextFieldHandlerDelegate? = nil) {
        self.minimumSizeValidCode = minimumSizeValidCode
        self.delegate = delegate
        super.init()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldValue = textField.text as NSString?
        let newCode = textFieldValue?.replacingCharacters(in: range, with: string).onlyNumbers ?? ""
        
        guard newCode.count >= minimumSizeValidCode else {
            return true
        }
        
        let range: Range<String.Index> = newCode.startIndex..<newCode.index(newCode.startIndex, offsetBy: minimumSizeValidCode)
        textField.text = String(newCode[range])
        delegate?.phoneCodeComplete(textField: textField)
        return false
    }
}
