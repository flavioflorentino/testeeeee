import CustomerSupport
import FeatureFlag
import UIKit

protocol RegistrationPhoneCodeCoordinatorDelegate: AnyObject {
    func moveToNextStep(withDdd ddd: String, phoneNumber: String)
    func moveToLogin()
}

enum RegistrationPhoneCodeAction {
    case back
    case moveToNextStep(with: String, number: String)
    case alreadyRegistered
    case editPhoneNumber
    case openHelp(with: String)
}

protocol RegistrationPhoneCodeCoordinating: AnyObject {
    var delegate: RegistrationPhoneCodeCoordinatorDelegate? { get set }
    var viewController: UIViewController? { get set }
    func perform(action: RegistrationPhoneCodeAction)
}

final class RegistrationPhoneCodeCoordinator: RegistrationPhoneCodeCoordinating {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    weak var delegate: RegistrationPhoneCodeCoordinatorDelegate?
    weak var viewController: UIViewController?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func perform(action: RegistrationPhoneCodeAction) {
        switch action {
        case .back, .editPhoneNumber:
           viewController?.navigationController?.popViewController(animated: true)
        case let .moveToNextStep(ddd, number):
            delegate?.moveToNextStep(withDdd: ddd, phoneNumber: number)
        case .alreadyRegistered:
            delegate?.moveToLogin()
        case let .openHelp(path):
            let chatbotUrlPath = FeatureManager.shared.text(.chatbotUnlogedUser)
            let chatbotViewController = ChatBotFactory.make(urlPath: chatbotUrlPath)
            let navigation = UINavigationController(rootViewController: chatbotViewController)
            viewController?.present(navigation, animated: true)
        }
    }
}
