import Foundation

protocol RegistrationPhoneCodePresenting: AnyObject {
    var viewController: RegistrationPhoneCodeDisplay? { get set }
    func displayDescription()
    func displayDescription(ddd: String, number: String)
    func displayCallButton()
    func displayConfirmResendCodeAlert(ddd: String, number: String)
    func displayConfirmReceiveCallAlert(ddd: String, number: String)
    func displayAlertError()
    func codeButtonIsActive(_ active: Bool)
    func callButtonIsActive(_ active: Bool)
    func updateCodeButtonTitle()
    func updateCodeButtonTitle(interval: Int)
    func updateCallButtonTitle()
    func updateCallButtonTitle(interval: Int)
    func displayError(message: String)
    func hideError()
    func showLoading()
    func hideLoading()
    func disableControls()
    func enableControls()
    func didNextStep(action: RegistrationPhoneCodeAction)
}

final class RegistrationPhoneCodePresenter: RegistrationPhoneCodePresenting {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let coordinator: RegistrationPhoneCodeCoordinating
    weak var viewController: RegistrationPhoneCodeDisplay?

    init(coordinator: RegistrationPhoneCodeCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    private func makeAlert(ddd: String, number: String) -> Alert {
        let confirmButton = Button(title: DefaultLocalizable.btOk.text, type: .cta, action: .confirm)
        let editButton = Button(title: DefaultLocalizable.btEdit.text, type: .clean, action: .close)
        
        return Alert(
            with: RegisterLocalizable.alertPhoneConfirmTitle.text,
            text: String(format: RegisterLocalizable.alertPhoneConfirmMessage.text, ddd, number),
            buttons: [confirmButton, editButton]
        )
    }
    
    func displayDescription() {
        viewController?.displayDescription(NSAttributedString(string: RegisterLocalizable.codeStepDefaultTextLabel.text))
    }
    
    func displayDescription(ddd: String, number: String) {
        let description = String(format: RegisterLocalizable.codeStepPhoneTextLabel.text, "<b>(\(ddd)) \(number)</b>")
        let attributedString = NSAttributedString(string: description).boldfyWithSystemFont(ofSize: 14)
        viewController?.displayDescription(attributedString)
    }

    func displayCallButton() {
        viewController?.displayCallButton()
    }
    
    func displayConfirmResendCodeAlert(ddd: String, number: String) {
        let alert = makeAlert(ddd: ddd, number: number)
        viewController?.displayConfirmResendCodeAlert(alert)
    }
    
    func displayConfirmReceiveCallAlert(ddd: String, number: String) {
        let alert = makeAlert(ddd: ddd, number: number)
        viewController?.displayConfirmReceiveCallAlert(alert)
    }
    
    func displayAlertError() {
        let confirmButton = Button(title: DefaultLocalizable.btOk.text, type: .cta, action: .close)
        let alert = Alert(
            with: DefaultLocalizable.attention.text,
            text: RegisterLocalizable.wrongInformations.text,
            buttons: [confirmButton]
        )
        viewController?.displayAlertError(alert)
    }
    
    func codeButtonIsActive(_ active: Bool) {
        viewController?.codeButtonIsActive(active)
    }
    
    func callButtonIsActive(_ active: Bool) {
        viewController?.callButtonIsActive(active)
    }
    
    func updateCodeButtonTitle() {
        viewController?.updateCodeButtonTitle(RegisterLocalizable.resendCode.text)
    }
    
    func updateCodeButtonTitle(interval: Int) {
        guard interval > 0 else {
            updateCodeButtonTitle()
            return
        }
        
        viewController?.updateCodeButtonTitle(String(format: RegisterLocalizable.resendCodedelay.text, timeFormatted(interval)))
    }
    
    func updateCallButtonTitle() {
        viewController?.updateCallButtonTitle(RegisterLocalizable.callMe.text)
    }
    
    func updateCallButtonTitle(interval: Int) {
        guard interval > 0 else {
            updateCallButtonTitle()
            return
        }
        
        viewController?.updateCallButtonTitle(String(format: RegisterLocalizable.callMeDelay.text, timeFormatted(interval)))
    }
    
    func displayError(message: String) {
        viewController?.displayError(message: message)
    }
    
    func hideError() {
        viewController?.hideError()
    }
    
    func showLoading() {
        viewController?.showLoading()
    }
    
    func hideLoading() {
        viewController?.hideLoading()
    }
    
    func enableControls() {
        viewController?.enableControls()
    }
    
    func disableControls() {
        viewController?.disableControls()
    }
    
    func didNextStep(action: RegistrationPhoneCodeAction) {
        coordinator.perform(action: action)
    }
}

private extension RegistrationPhoneCodePresenter {
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        let hours: Int = totalSeconds / 3_600
        
        if hours > 0 {
            return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
        } else {
            return String(format: "%02d:%02d", minutes, seconds)
        }
    }
}
