import Core
import UI

private extension RegistrationPhoneCodeViewController.Layout {
    enum Style {
        static let inputCornerRadius: CGFloat = 20
        static let textAlignment: NSTextAlignment = .center
        static let numberOfLines = 0
        static let descriptionFont: UIFont = .systemFont(ofSize: 14)
        static let descriptionTextColor = Palette.ppColorGrayscale400.color
        static let alphaComponent: CGFloat = 0.5
        static let secondariesTextsColor = Palette.ppColorBranding300.color
        static let secondariesTextsFontSize: UIFont = .systemFont(ofSize: 12)
        static let grayTextsColor = Palette.ppColorGrayscale400.color
        static let alreadyRegisteredButtonTextColor = Palette.ppColorGrayscale500.color
        static let alreadyRegisteredButtonFont: UIFont = .systemFont(ofSize: 12)
    }
    
    enum Size {
        static let inputSize = CGSize(width: 108, height: 40)
        static let buttonHeight: CGFloat = Spacing.base06
        static let imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
    }
}

final class RegistrationPhoneCodeViewController: RegistrationBaseViewController<RegistrationPhoneCodeViewModelInputs, UIView> {
    fileprivate struct Layout {}
    private var codeTextFieldHandler: PhoneCodeTextFieldHandler?
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Style.descriptionFont
        label.textColor = Layout.Style.descriptionTextColor
        label.textAlignment = Layout.Style.textAlignment
        label.numberOfLines = Layout.Style.numberOfLines
        return label
    }()
    
    private lazy var inputStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [codeTextField, errorLabel])
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var codeTextField: RoundedTextField = {
        let textField = RoundedTextField()
        textField.cornerRadius = Layout.Style.inputCornerRadius
        textField.textAlignment = Layout.Style.textAlignment
        textField.keyboardType = .numberPad
        textField.placeholder = RegisterLocalizable.codePlaceholder.text
        return textField
    }()
    
    private lazy var sendButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(Layout.Style.secondariesTextsColor, for: .normal)
        button.setTitleColor(Layout.Style.secondariesTextsColor.withAlphaComponent(Layout.Style.alphaComponent), for: .disabled)
        button.titleLabel?.font = Layout.Style.secondariesTextsFontSize
        button.setImage(Assets.NewGeneration.iconMessage.image, for: .normal)
        button.imageEdgeInsets = Layout.Size.imageEdgeInsets
        button.addTarget(self, action: #selector(tapSendButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var callButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(Layout.Style.secondariesTextsColor, for: .normal)
        button.setTitleColor(Layout.Style.secondariesTextsColor.withAlphaComponent(Layout.Style.alphaComponent), for: .disabled)
        button.titleLabel?.font = Layout.Style.secondariesTextsFontSize
        button.setImage(Assets.NewGeneration.iconPhone.image, for: .normal)
        button.imageEdgeInsets = Layout.Size.imageEdgeInsets
        button.addTarget(self, action: #selector(tapCallButton), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    private lazy var optionsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [editButton, orLabel, helpButton])
        stackView.alignment = .center
        stackView.spacing = Spacing.base00
        return stackView
    }()
    
    private lazy var editButton: UIButton = {
        let title = NSAttributedString(string: "<u>\(RegisterLocalizable.editPhone.text)</u>").underlinefy()
        let button = UIButton(type: .system)
        button.tintColor = Layout.Style.grayTextsColor
        button.titleLabel?.font = Layout.Style.secondariesTextsFontSize
        button.setAttributedTitle(title, for: .normal)
        button.addTarget(self, action: #selector(tapEditButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var helpButton: UIButton = {
        let title = NSAttributedString(string: "<u>\(RegisterLocalizable.needHelp.text)</u>").underlinefy()
        let button = UIButton(type: .system)
        button.tintColor = Layout.Style.grayTextsColor
        button.titleLabel?.font = Layout.Style.secondariesTextsFontSize
        button.addTarget(self, action: #selector(tapHelpButton), for: .touchUpInside)
        button.setAttributedTitle(title, for: .normal)
        return button
    }()
    
    private lazy var orLabel: UILabel = {
        let label = UILabel()
        label.text = DefaultLocalizable.or.text
        label.textColor = Layout.Style.grayTextsColor
        label.font = Layout.Style.secondariesTextsFontSize
        return label
    }()
    
    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        var indicatorStyle: UIActivityIndicatorView.Style = .gray
        if #available(iOS 12.0, *), (self.traitCollection.userInterfaceStyle == .dark) {
            indicatorStyle = .white
        }
        return UIActivityIndicatorView(style: indicatorStyle)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadDescription()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.configureTimers()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        openKeyboardIfNecessary(codeTextField)
        viewModel.trackView()
    }

    override func buildViewHierarchy() {
        view.addSubviews(containerView, alreadyRegisteredButton)
        containerView.addSubviews(contentStackView, optionsStackView, activityIndicatorView)
        contentStackView.addArrangedSubviews(descriptionLabel, inputStackView, sendButton, callButton)
        navigationItem.leftBarButtonItem = customBackButton
    }
    
    override func configureViews() {
        super.configureViews()
        codeTextFieldHandler = PhoneCodeTextFieldHandler(minimumSizeValidCode: viewModel.validCodeLength, delegate: self)
        codeTextField.delegate = codeTextFieldHandler
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        optionsStackView.layout {
            $0.top == contentStackView.bottomAnchor + Spacing.base02
            $0.centerX == containerView.centerXAnchor
        }
        
        activityIndicatorView.layout {
            $0.top == optionsStackView.bottomAnchor + Spacing.base02
            $0.centerX == optionsStackView.centerXAnchor
        }
        
        codeTextField.layout {
            $0.width == Layout.Size.inputSize.width
            $0.height == Layout.Size.inputSize.height
        }
    }
    
    override func tapBackButton() {
        viewModel.didTapBack()
    }
    
    override func tapAlreadyRegisteredButton() {
        viewModel.didAlreadyRegister()
    }
}

extension RegistrationPhoneCodeViewController: RegistrationPhoneCodeDisplay, RegistrationLoadingDisplay {
    func displayDescription(_ description: NSAttributedString) {
        descriptionLabel.attributedText = description
    }
    
    func displayCallButton() {
        callButton.isHidden = false
    }
    
    func displayConfirmResendCodeAlert(_ alert: Alert) {
        AlertMessage.showAlert(alert, controller: self) { [weak self] alertController, button, _ in
            guard case Button.Action.confirm = button.action else {
                self?.viewModel.didEditPhoneNumber()
                return
            }
            alertController.dismiss(animated: true) {
                self?.viewModel.requestCode()
            }
        }
    }
    
    func displayConfirmReceiveCallAlert(_ alert: Alert) {
        AlertMessage.showAlert(alert, controller: self) { [weak self] alertController, button, _ in
            guard case Button.Action.confirm = button.action else {
                self?.viewModel.didEditPhoneNumber()
                return
            }
            alertController.dismiss(animated: true) {
                self?.viewModel.requestCall()
            }
        }
    }
    
    func displayAlertError(_ alert: Alert) {
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func codeButtonIsActive(_ active: Bool) {
        sendButton.isEnabled = active
    }
    
    func callButtonIsActive(_ active: Bool) {
        callButton.isEnabled = active
    }
    
    func updateCodeButtonTitle(_ title: String) {
        sendButton.setTitle(title, for: .normal)
    }
    
    func updateCallButtonTitle(_ title: String) {
        callButton.setTitle(title, for: .normal)
    }
    
    func displayError(message: String) {
        errorLabel.isHidden = false
        errorLabel.text = message
        codeTextField.hasError = true
    }
    
    func hideError() {
        errorLabel.isHidden = true
        errorLabel.text = nil
        codeTextField.hasError = false
    }
    
    func showLoading() {
        activityIndicatorView.startAnimating()
    }
    
    func hideLoading() {
        activityIndicatorView.stopAnimating()
    }
    
    func enableControls() {
        codeTextField.isEnabled = true
        editButton.isEnabled = true
        helpButton.isEnabled = true
        alreadyRegisteredButton.isEnabled = true
    }
    
    func disableControls() {
        codeTextField.isEnabled = false
        editButton.isEnabled = false
        helpButton.isEnabled = false
        alreadyRegisteredButton.isEnabled = false
    }
}

@objc
private extension RegistrationPhoneCodeViewController {
    func tapSendButton() {
        viewModel.confirmPhoneWithResendCode()
    }
    
    func tapCallButton() {
        viewModel.confirmPhoneWithReceiveCall()
    }
    
    func tapEditButton() {
        viewModel.didEditPhoneNumber()
    }

    func tapHelpButton() {
        viewModel.didHelp()
    }
    
    func tapAlreadyRegisterButton() {
        viewModel.didAlreadyRegister()
    }
}

extension RegistrationPhoneCodeViewController: PhoneCodeTextFieldHandlerDelegate {
    func phoneCodeComplete(textField: UITextField) {
        viewModel.verifyPhoneNumber(with: textField.text)
    }
}
