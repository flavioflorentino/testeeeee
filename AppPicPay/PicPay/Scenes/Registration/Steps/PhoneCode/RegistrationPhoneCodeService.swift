import Core
import FeatureFlag

protocol RegistrationPhoneCodeServicing {
    var isPhoneVerificationCallEnabled: Bool { get }
    var helpPath: String { get }
    var phoneVerificationSentDate: Date { get }
    var phoneVerificationCallDelay: Int { get }
    var phoneVerificationSMSDelay: Int { get }
    var phoneVerificationWhatsAppDelay: Int { get }
    func updateVerificationData(date: Date, smsDelay: Int, whatsAppDelay: Int, callDelay: Int, callEnabled: Bool)
    func verifyPhoneNumber(with code: String, completion: @escaping (Result<PhoneNumberValidationResponse, ApiError>) -> Void)
    func requestValidationCode(
        ddd: String,
        number: String,
        codeType: RegistrationPhoneCodeType,
        completion: @escaping(Result<PhoneNumberCodeResponse, ApiError>) -> Void
    )
    func requestCallVerification(
        ddd: String,
        number: String,
        completion: @escaping(Result<PhoneNumberCodeResponse, ApiError>) -> Void
    )
}

final class RegistrationPhoneCodeService: RegistrationPhoneCodeServicing {
    typealias Dependencies = HasFeatureManager & HasMainQueue
    private let dependencies: Dependencies
    private let phoneParametersProvider: PhoneVerificationParametersProvider
    
    var isPhoneVerificationCallEnabled: Bool {
        phoneParametersProvider.isEnabledPhoneVerificationCall()
    }
    
    var helpPath: String {
        dependencies.featureManager.text(.codeVerifyHelpCenterPath)
    }
    
    var phoneVerificationSentDate: Date {
        phoneParametersProvider.getPhoneVerificationSentDate() ?? Date()
    }
    
    var phoneVerificationCallDelay: Int {
        phoneParametersProvider.getPhoneVerificationCallDelay()
    }
    
    var phoneVerificationSMSDelay: Int {
        phoneParametersProvider.getPhoneVerificationSMSDelay()
    }
    
    var phoneVerificationWhatsAppDelay: Int {
        phoneParametersProvider.getPhoneVerificationWhatsAppDelay()
    }
    
    init(dependencies: Dependencies, phoneParametersProvider: PhoneVerificationParametersProvider) {
        self.dependencies = dependencies
        self.phoneParametersProvider = phoneParametersProvider
    }
    
    func verifyPhoneNumber(with code: String, completion: @escaping (Result<PhoneNumberValidationResponse, ApiError>) -> Void) {
        let endpoint = RegisterEndpoint.verifyPhoneNumber(code: code)
        let decoder = JSONDecoder(.convertFromSnakeCase)
        Api<RegisterResponse<PhoneNumberValidationResponse>>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map { $0.model }
                switch mappedResult {
                case let .success(response):
                    guard let data = response.data else {
                        if let error = response.error {
                            var requestError = RequestError()
                            requestError.message = error.description
                            return completion(.failure(ApiError.badRequest(body: requestError)))
                        }
                        
                        return completion(.failure(ApiError.unknown(nil)))
                    }
                    
                    return completion(.success(data))
                case let .failure(error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    func requestValidationCode(
        ddd: String,
        number: String,
        codeType: RegistrationPhoneCodeType,
        completion: @escaping (Result<PhoneNumberCodeResponse, ApiError>) -> Void
    ) {
        let endpoint = codeType == .sms ?
            RegisterEndpoint.requestSmsCode(ddd: ddd, number: number) :
            RegisterEndpoint.requestWhatsAppCode(ddd: ddd, number: number)
        let decoder = JSONDecoder(.convertFromSnakeCase)
        Api<RegisterResponse<PhoneNumberCodeResponse>>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map { $0.model }
                self?.handleResult(mappedResult, completion: completion)
            }
        }
    }
    
    func requestCallVerification(
        ddd: String,
        number: String,
        completion: @escaping (Result<PhoneNumberCodeResponse, ApiError>) -> Void
    ) {
        let endpoint = RegisterEndpoint.callVerification(ddd: ddd, number: number)
        let decoder = JSONDecoder(.convertFromSnakeCase)
        Api<RegisterResponse<PhoneNumberCodeResponse>>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map { $0.model }
                self?.handleResult(mappedResult, completion: completion)
            }
        }
    }
    
    func updateVerificationData(date: Date,
                                smsDelay: Int = 0,
                                whatsAppDelay: Int = 0,
                                callDelay: Int,
                                callEnabled: Bool) {
        phoneParametersProvider.setPhoneVerificationSentDate(date)
        phoneParametersProvider.setPhoneVerificationSMSDelay(smsDelay)
        phoneParametersProvider.setPhoneVerificationWhatsAppDelay(whatsAppDelay)
        phoneParametersProvider.setPhoneVerificationCallDelay(callDelay)
        phoneParametersProvider.setEnabledPhoneVerificationCall(callEnabled)
    }
}

private extension RegistrationPhoneCodeService {
    func handleResult(
        _ result: Result<RegisterResponse<PhoneNumberCodeResponse>, ApiError>,
        completion: @escaping (Result<PhoneNumberCodeResponse, ApiError>) -> Void
    ) {
        switch result {
        case let .success(response):
            guard let data = response.data else {
                if let error = response.error {
                    var requestError = RequestError()
                    requestError.message = error.description
                    return completion(.failure(ApiError.badRequest(body: requestError)))
                }
                
                return completion(.failure(ApiError.unknown(nil)))
            }
            
            return completion(.success(data))
        case let .failure(error):
            return completion(.failure(error))
        }
    }
}
