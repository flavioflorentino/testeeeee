import UI
import UIKit

class RegistrationBaseViewController<ViewModel, View: UIView>: ViewController<ViewModel, View>, UITextViewDelegate {
    lazy var containerView = UIView()
    
    lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = isSmallScreenDevice ? Spacing.base02 : Spacing.base03
        return stackView
    }()
    
    lazy var customBackButton: UIBarButtonItem = {
        let customView = CustomBackButton()
        customView.addTarget(self, action: #selector(tapBackButton), for: .touchUpInside)
        return UIBarButtonItem(customView: customView)
    }()
    
    lazy var termsOfServiceView: TermsOfServiceTextView = {
        let view = TermsOfServiceTextView()
        view.backgroundColor = .clear
        view.isHidden = true
        view.delegate = self
        return view
    }()
    
    lazy var errorLabel: UILabel = {
        let label = ErrorLabel()
        label.isHidden = true
        return label
    }()
    
    lazy var alreadyRegisteredButton: UIButton = {
        let button = UIButton()
        button.setTitle(RegisterLocalizable.alreadyRegistered.text, for: .normal)
        button.setTitleColor(Colors.grayscale700.color, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        button.addTarget(self, action: #selector(tapAlreadyRegisteredButton), for: .touchUpInside)
        return button
    }()
    
    let isSmallScreenDevice = UIScreen.main.bounds.height <= 568
    
    var primaryButtonBottomConstraint: NSLayoutConstraint?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBarColor()
        setupKeyboardObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    override func buildViewHierarchy() {
        view.addSubviews(containerView, alreadyRegisteredButton)
        containerView.addSubview(contentStackView)
        navigationItem.leftBarButtonItem = customBackButton
    }
    
    override func setupConstraints() {
        containerView.layout {
            $0.top == view.compatibleSafeAreaLayoutGuide.topAnchor
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
            $0.bottom == view.compatibleSafeAreaLayoutGuide.bottomAnchor - Spacing.base06
        }
        
        contentStackView.layout {
            $0.top == containerView.topAnchor + Spacing.base04
            $0.leading == containerView.leadingAnchor
            $0.trailing == containerView.trailingAnchor
        }
        
        alreadyRegisteredButton.layout {
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
            $0.bottom == view.compatibleSafeAreaLayoutGuide.bottomAnchor
            $0.height == Spacing.base06
        }
    }
    
    func openKeyboardIfNecessary(_ textField: UITextField) {
        guard !UIAccessibility.isVoiceOverRunning else {
            return
        }
        
        textField.becomeFirstResponder()
    }
    
    func didInteractWithUrl(_ url: URL) {}
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        let zeroRange = NSRange(location: 0, length: 0)
        if !NSEqualRanges(textView.selectedRange, zeroRange) {
            textView.selectedRange = zeroRange
        }
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        didInteractWithUrl(URL)
        return false
    }
    
    @objc
    func tapBackButton() {}
    
    @objc
    func tapAlreadyRegisteredButton() {}
    
    @objc
    func keyboardWillShow(notification: NSNotification) {
        guard
            let userInfo = notification.userInfo,
            let keyboardRectValue = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
            let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue
            else {
                return
        }
        let spacing = isSmallScreenDevice ? Spacing.base01 : Spacing.base02
        primaryButtonBottomConstraint?.constant = -(keyboardRectValue.height + spacing)
        
        if isSmallScreenDevice && navigationController?.navigationBar.isHidden == false {
            navigationController?.setNavigationBarHidden(true, animated: true)
        }

        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc
    func keyboardWillHide(notification: NSNotification) {
        guard
            let userInfo = notification.userInfo,
            let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue
            else {
                return
        }
        
        primaryButtonBottomConstraint?.constant = isSmallScreenDevice ? -Spacing.base01 : -Spacing.base03
        
        if isSmallScreenDevice && navigationController?.navigationBar.isHidden == true {
            navigationController?.setNavigationBarHidden(false, animated: true)
        }

        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

private extension RegistrationBaseViewController {
    func setupNavBarColor() {
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.tintColor = Colors.branding600.color
        if isSmallScreenDevice {
            navigationController?.view.backgroundColor = Colors.grayscale050.color
        }
    }
    
    func setupKeyboardObservers() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapGesture)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
}
