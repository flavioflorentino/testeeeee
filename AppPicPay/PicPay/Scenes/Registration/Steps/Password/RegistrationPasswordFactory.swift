import Foundation

enum RegistrationPasswordFactory {
    static func make(
        delegate: RegistrationPasswordDelegate,
        password: String?,
        showTermsOfService: Bool
    ) -> RegistrationPasswordViewController {
        let container = DependencyContainer()
        let service: RegistrationPasswordServicing = RegistrationPasswordService(dependencies: container)
        let coordinator: RegistrationPasswordCoordinating = RegistrationPasswordCoordinator()
        let presenter: RegistrationPasswordPresenting = RegistrationPasswordPresenter(coordinator: coordinator)
        let viewModel = RegistrationPasswordViewModel(
            service: service,
            presenter: presenter,
            password: password,
            showTermsOfService: showTermsOfService
        )
        let viewController = RegistrationPasswordViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
