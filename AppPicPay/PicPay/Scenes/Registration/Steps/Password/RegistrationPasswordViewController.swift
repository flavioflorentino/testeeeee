import UI
import UIKit

private extension RegistrationPasswordViewController.Layout {
    static let titleFont = UIFont.systemFont(ofSize: 18, weight: .regular)
    static let descriptionFont = UIFont.systemFont(ofSize: 14, weight: .regular)
    static let smallFont = UIFont.systemFont(ofSize: 12)
    static let textFieldHeight: CGFloat = Spacing.base05
    static let buttonHeight: CGFloat = Spacing.base06
}

final class RegistrationPasswordViewController: RegistrationBaseViewController<RegistrationPasswordViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale600.color
        label.font = Layout.titleFont
        label.numberOfLines = 0
        label.textAlignment = .center
        label.text = RegisterLocalizable.passwordStepTitle.text
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale400.color
        label.font = Layout.descriptionFont
        label.numberOfLines = 0
        label.textAlignment = .center
        label.text = RegisterLocalizable.passwordStepDescription.text
        label.setContentHuggingPriority(.defaultLow, for: .vertical)
        return label
    }()
    
    private lazy var passwordVisibilityButton: UIButton = {
        let button = UIButton()
        button.setImage(UI.Assets.icoPasswordEyeOpened.image, for: .normal)
        button.addTarget(self, action: #selector(tapVisibilityButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var passwordTextField: RoundedTextField = {
        let textField = RoundedTextField()
        textField.cornerRadius = Layout.textFieldHeight / 2
        textField.placeholder = RegisterLocalizable.passwordStepFieldPlaceholder.text
        textField.keyboardType = .numberPad
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        textField.rightView = passwordVisibilityButton
        textField.rightViewMode = .always
        textField.delegate = self
        return textField
    }()
    
    private lazy var confirmButton: UIPPButton = {
        let button = UIPPButton(
            title: DefaultLocalizable.advance.text,
            target: self,
            action: #selector(tapConfirmButton),
            type: .cta
        )
        button.cornerRadius = Layout.buttonHeight / 2
        return button
    }()
    
    private lazy var stepDescriptionStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var inputStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.displayTermsOfServiceIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.fillPasswordTextFieldIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        openKeyboardIfNecessary(passwordTextField)
        viewModel.trackView()
    }
    
    override func buildViewHierarchy() {
        super.buildViewHierarchy()
        stepDescriptionStackView.addArrangedSubviews(titleLabel, descriptionLabel)
        inputStackView.addArrangedSubviews(passwordTextField, errorLabel)
        contentStackView.addArrangedSubviews(stepDescriptionStackView, inputStackView, confirmButton, termsOfServiceView)
        view.addSubview(alreadyRegisteredButton)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        passwordTextField.layout {
            $0.height == Layout.textFieldHeight
        }
        
        confirmButton.layout {
            $0.height == Layout.buttonHeight
        }
        
        passwordVisibilityButton.layout {
            $0.width == Layout.textFieldHeight
            $0.height == Layout.textFieldHeight
        }
    }
    
    override func tapBackButton() {
        viewModel.didTapBack()
    }
    
    override func didInteractWithUrl(_ url: URL) {
        viewModel.didInteractWithUrl(url)
    }
    
    override func tapAlreadyRegisteredButton() {
        viewModel.updatePassword(with: passwordTextField.text)
        viewModel.didAlreadyRegistered()
    }
}

@objc
private extension RegistrationPasswordViewController {
    func tapConfirmButton() {
        viewModel.updatePassword(with: passwordTextField.text)
        viewModel.didConfirm()
    }
    
    func tapVisibilityButton() {
        viewModel.didChangePasswordVisibility(fromVisible: !passwordTextField.isSecureTextEntry)
    }
}

extension RegistrationPasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        viewModel.updatePassword(with: passwordTextField.text)
        viewModel.didConfirm()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
}

extension RegistrationPasswordViewController: RegistrationPasswordDisplay, RegistrationLoadingDisplay {
    func fillPasswordTextField(with password: String) {
        passwordTextField.text = password
        passwordTextField.layoutIfNeeded()
    }
    
    func updatePasswordVisibility(isVisible: Bool) {
        passwordTextField.isSecureTextEntry = !isVisible
        let buttonImage = isVisible ? UI.Assets.icoPasswordEyeOpened.image : UI.Assets.icoPasswordEyeClosed.image
        passwordVisibilityButton.setImage(buttonImage, for: .normal)
    }
    
    func displayTermsOfService() {
        termsOfServiceView.isHidden = false
    }
    
    func displayError(message: String) {
        errorLabel.text = message
        errorLabel.isHidden = false
        passwordTextField.hasError = true
    }
    
    func hideErrors() {
        errorLabel.text = ""
        errorLabel.isHidden = true
        passwordTextField.hasError = false
    }
    
    func showLoading() {
        confirmButton.startLoadingAnimating()
    }
    
    func hideLoading() {
        confirmButton.stopLoadingAnimating()
    }
}
