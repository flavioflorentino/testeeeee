import UIKit

protocol RegistrationPasswordDisplay: AnyObject {
    func fillPasswordTextField(with password: String)
    func updatePasswordVisibility(isVisible: Bool)
    func displayTermsOfService()
    func displayError(message: String)
    func hideErrors()
    func showLoading()
    func hideLoading()
    func dismissKeyboard()
}
