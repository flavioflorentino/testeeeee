import AnalyticsModule
import Core

protocol RegistrationPasswordViewModelInputs: AnyObject {
    func displayTermsOfServiceIfNeeded()
    func fillPasswordTextFieldIfNeeded()
    func trackView()
    func updatePassword(with updatedPassword: String?)
    func didChangePasswordVisibility(fromVisible: Bool)
    func didConfirm()
    func didAlreadyRegistered()
    func didInteractWithUrl(_ url: URL)
    func didTapBack()
}

final class RegistrationPasswordViewModel {
    private let service: RegistrationPasswordServicing
    private let presenter: RegistrationPasswordPresenting
    private var password: String
    private let showTermsOfService: Bool
    private let eventName = "cadastro_senha"

    init(service: RegistrationPasswordServicing,
         presenter: RegistrationPasswordPresenting,
         password: String? = nil,
         showTermsOfService: Bool) {
        self.service = service
        self.presenter = presenter
        self.password = password ?? ""
        self.showTermsOfService = showTermsOfService
    }
    
    private func validatePasswordLength(_ passwordString: String) -> Bool {
        passwordString.length >= 4
    }
    
    private func validatePasswordOnApi(_ passwordString: String) {
        presenter.dismissKeyboard()
        presenter.hideErrors()
        presenter.showLoading()
        
        service.validatePassword(passwordString) { [weak self] result in
            self?.presenter.hideLoading()
            
            switch result {
            case .success:
                self?.trackSuccessToNextStep()
                self?.presenter.didNextStep(action: .moveToNextStep(withPassword: passwordString))
            case .failure(let error):
                let message = self?.parseErrorMessage(error: error) ?? DefaultLocalizable.requestError.text
                self?.presenter.displayError(message: message)
            }
        }
    }
    
    private func parseErrorMessage(error: ApiError) -> String? {
        switch error {
        case .badRequest(let data):
            return data.message
        case .connectionFailure:
            return DefaultLocalizable.errorConnectionViewSubtitle.text
        default:
            return nil
        }
    }
    
    private func trackSuccessToNextStep() {
        Analytics.shared.log(RegistrationEvent.password)
    }
}

extension RegistrationPasswordViewModel: RegistrationPasswordViewModelInputs {
    func displayTermsOfServiceIfNeeded() {
        guard showTermsOfService else {
            return
        }
        presenter.displayTermsOfService()
    }
    
    func fillPasswordTextFieldIfNeeded() {
        guard password.isNotEmpty else {
            return
        }
        presenter.fillPasswordTextField(with: password)
    }
    
    func trackView() {
        PPAnalytics.trackEvent(eventName, properties: nil)
    }
    
    func updatePassword(with updatedPassword: String?) {
        guard let updatedPassword = updatedPassword else {
            return
        }
        self.password = updatedPassword
    }
    
    func didChangePasswordVisibility(fromVisible: Bool) {
        presenter.updatePasswordVisibility(isVisible: !fromVisible)
    }
    
    func didConfirm() {
        guard validatePasswordLength(password) else {
            presenter.displayInputError()
            return
        }
        validatePasswordOnApi(password)
    }
    
    func didAlreadyRegistered() {
        presenter.didNextStep(action: .alreadyRegistered)
    }
    
    func didInteractWithUrl(_ url: URL) {
        guard
            let webviewUrlString = WebServiceInterface.apiEndpoint(url.absoluteString),
            let webviewUrl = URL(string: webviewUrlString)
            else {
                return
        }
        presenter.didNextStep(action: .webview(url: webviewUrl))
    }
    
    func didTapBack() {
        presenter.didNextStep(action: .back)
    }
}
