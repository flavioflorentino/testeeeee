protocol RegistrationPasswordCoordinatorDelegate: AnyObject {
    func moveToNextStep(withPassword password: String)
}

typealias RegistrationPasswordDelegate = RegistrationPasswordCoordinatorDelegate & RegistrationFlowCoordinatorDelegate

enum RegistrationPasswordAction {
    case back
    case moveToNextStep(withPassword: String)
    case alreadyRegistered
    case webview(url: URL)
}

protocol RegistrationPasswordCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: RegistrationPasswordDelegate? { get set }
    func perform(action: RegistrationPasswordAction)
}

final class RegistrationPasswordCoordinator: RegistrationPasswordCoordinating {
    weak var delegate: RegistrationPasswordDelegate?
    weak var viewController: UIViewController?
    
    func perform(action: RegistrationPasswordAction) {
        switch action {
        case .back:
            delegate?.moveToPreviousRegistrationStep()
        case .moveToNextStep(let password):
            delegate?.moveToNextStep(withPassword: password)
        case .alreadyRegistered:
            delegate?.moveToLogin()
        case .webview(let url):
            let webviewController = WebViewFactory.make(with: url)
            viewController?.navigationController?.present(PPNavigationController(rootViewController: webviewController), animated: true)
        }
    }
}
