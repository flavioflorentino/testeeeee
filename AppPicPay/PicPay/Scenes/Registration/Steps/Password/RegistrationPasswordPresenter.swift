import Core
import Foundation

protocol RegistrationPasswordPresenting: AnyObject {
    var viewController: RegistrationPasswordDisplay? { get set }
    func didNextStep(action: RegistrationPasswordAction)
    func fillPasswordTextField(with password: String)
    func updatePasswordVisibility(isVisible: Bool)
    func displayTermsOfService()
    func displayInputError()
    func displayError(message: String)
    func hideErrors()
    func showLoading()
    func hideLoading()
    func dismissKeyboard()
}

final class RegistrationPasswordPresenter: RegistrationPasswordPresenting {
    private let coordinator: RegistrationPasswordCoordinating
    weak var viewController: RegistrationPasswordDisplay?

    init(coordinator: RegistrationPasswordCoordinating) {
        self.coordinator = coordinator
    }
    
    func fillPasswordTextField(with password: String) {
        viewController?.fillPasswordTextField(with: password)
    }
    
    func updatePasswordVisibility(isVisible: Bool) {
        viewController?.updatePasswordVisibility(isVisible: isVisible)
    }
    
    func displayTermsOfService() {
        viewController?.displayTermsOfService()
    }
    
    func didNextStep(action: RegistrationPasswordAction) {
        coordinator.perform(action: action)
    }
    
    func displayInputError() {
        viewController?.displayError(message: RegisterLocalizable.invalidPassword.text)
    }
    
    func displayError(message: String) {
        viewController?.displayError(message: message)
    }
    
    func hideErrors() {
        viewController?.hideErrors()
    }
    
    func showLoading() {
        viewController?.showLoading()
    }
    
    func hideLoading() {
        viewController?.hideLoading()
    }
    
    func dismissKeyboard() {
        viewController?.dismissKeyboard()
    }
}
