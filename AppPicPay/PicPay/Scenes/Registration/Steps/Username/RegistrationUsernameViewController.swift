import UI
import UIKit
import SecurityModule
import SnapKit

private extension RegistrationUsernameViewController.Layout {
    enum Style {
        static let inputCornerRadius: CGFloat = Size.textFieldHeight / 2
        static let buttonsCornerRadius: CGFloat = Size.buttonHeight / 2
        static let titleFont: UIFont = .systemFont(ofSize: 18, weight: .medium)
        static let descriptionFont: UIFont = .systemFont(ofSize: 14)
        static let secondariesTextsFont: UIFont = .systemFont(ofSize: 14)
        static let alphaComponent: CGFloat = 0.5
        static let stackViewLayoutMargins = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        static let atLeading: CGFloat = 12
    }
    
    enum Size {
        static let textFieldHeight: CGFloat = Spacing.base05
        static let buttonHeight: CGFloat = Spacing.base06
        static let limitHeight: CGFloat = 568
        static let screenHeight = UIScreen.main.bounds.height
        static let imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        static let textFieldInset: CGFloat = Spacing.base04
    }
}

final class RegistrationUsernameViewController: ViewController<RegistrationUsernameViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    private let isSmallScreenDevice = Layout.Size.screenHeight <= Layout.Size.limitHeight
    private lazy var containerView = UIView()
    
    private lazy var centerContainerView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [contentStackView])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        return stackView
    }()
    
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [stepDescriptionStackView, inputStackView, confirmButton, promoCodeButton])
        stackView.layoutMargins = Layout.Style.stackViewLayoutMargins
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.axis = .vertical
        stackView.spacing = isSmallScreenDevice ? Spacing.base02 : Spacing.base03
        return stackView
    }()
    
    private lazy var stepDescriptionStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, descriptionLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var inputStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [usernameTextField, errorLabel])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = RegisterLocalizable.usernameTitle.text
        label.font = Layout.Style.titleFont
        label.textColor = Colors.grayscale600.color
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = RegisterLocalizable.usernameDescription.text
        label.font = Layout.Style.descriptionFont
        label.textColor = Colors.grayscale400.color
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var usernameTextField: RoundedTextField = {
        let textField = RoundedTextField(insetX: Layout.Size.textFieldInset)
        textField.cornerRadius = Layout.Style.inputCornerRadius
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        textField.clearButtonMode = .whileEditing
        textField.delegate = self
        return textField
    }()
    
    private lazy var atLabel: UILabel = {
        let label = UILabel()
        label.text = "@"
        label.textColor = Colors.grayscale400.color
        return label
    }()
    
    private lazy var errorLabel: ErrorLabel = {
        let label = ErrorLabel()
        label.isHidden = true
        return label
    }()
    
    private lazy var confirmButton: UIPPButton = {
        let button = UIPPButton()
        button.setTitle(RegisterLocalizable.usernameConclude.text, for: .normal)
        button.configureButtonCta()
        button.cornerRadius = Layout.Style.buttonsCornerRadius
        button.addTarget(self, action: #selector(tapConfirmButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var promoCodeButton: UIButton = {
        let title = NSAttributedString(string: "<u>\(RegisterLocalizable.usernameUseCode.text)</u>").underlinefy()
        let button = UIButton(type: .system)
        button.tintColor = Colors.branding300.color
        button.titleLabel?.font = Layout.Style.secondariesTextsFont
        button.setImage(Assets.Icons.iconUseCode.image.withRenderingMode(.alwaysOriginal), for: .normal)
        button.imageEdgeInsets = Layout.Size.imageEdgeInsets
        button.setAttributedTitle(title, for: .normal)
        button.addTarget(self, action: #selector(tapUsePromoCodeButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var promoCodeStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var skipButton: UIBarButtonItem = {
        let barButtonItem = UIBarButtonItem(
            title: OnboardingLocalizable.skip.text,
            style: .plain,
            target: self,
            action: #selector(tapSkipButton)
        )
        return barButtonItem
    }()
    
    private var usernameCharSet: CharacterSet {
        let acceptableCharacters = "abcdefghijklmnopqrstuvwxyz0123456789_."
        return CharacterSet(charactersIn: acceptableCharacters)
    }
    
    private var containerBottomConstraint: Constraint?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationItem.hidesBackButton = true
        viewModel.loadUsernameSuggestion()
        viewModel.validatePromoCodeIfNeeded()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.sendLogDetection()
    }
    
    override func buildViewHierarchy() {
        usernameTextField.addSubview(atLabel)
        containerView.addSubview(centerContainerView)
        view.addSubview(containerView)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale100.color
        navigationItem.rightBarButtonItem = skipButton
        
        setupNavBarColor()
        setupKeyboardObservers()
    }
    
    override func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top)
            $0.leading.trailing.equalToSuperview()
            containerBottomConstraint = $0.bottom.equalTo(view.compatibleSafeArea.bottom).constraint
        }
        
        centerContainerView.snp.makeConstraints {
            $0.leading.trailing.centerY.equalToSuperview()
        }
        
        usernameTextField.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.textFieldHeight)
        }
        
        atLabel.snp.makeConstraints {
            $0.leading.equalTo(usernameTextField).offset(Layout.Style.atLeading)
            $0.centerY.equalTo(usernameTextField)
        }
        
        confirmButton.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.buttonHeight)
        }
    }
    
    private func setupNavBarColor() {
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale100.color
        navigationController?.navigationBar.tintColor = Colors.grayscale400.color
        
        if isSmallScreenDevice {
            navigationController?.view.backgroundColor = Palette.ppColorGrayscale100.color
        }
    }
    
    private func setupKeyboardObservers() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapGesture)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillOpen), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc
    private func tapConfirmButton() {
        viewModel.createUsername(usernameTextField.text)
        viewModel.checkStudentOffer()
    }
    
    @objc
    private func tapUsePromoCodeButton() {
        viewModel.didUsePromoCode()
    }
    
    @objc
    private func tapSkipButton() {
        viewModel.didSkip()
        viewModel.checkStudentOffer()
    }
    
    @objc
    private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc
    private func keyboardWillOpen(_ keyboardNotification: Notification) {
        guard
            let userInfo = keyboardNotification.userInfo,
            let keyboardRect = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
            let keyboardAnimation = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue
        else {
            return
        }

        containerBottomConstraint?.update(offset: -keyboardRect.height)
        
        if isSmallScreenDevice, navigationController?.navigationBar.isHidden == false {
            navigationController?.setNavigationBarHidden(true, animated: true)
        }
        
        UIView.animate(withDuration: keyboardAnimation) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc
    private func keyboardWillHide(_ keyboardNotification: Notification) {
        guard
            let userInfo = keyboardNotification.userInfo,
            let keyboardAnimation = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue
        else {
            return
        }
        
        containerBottomConstraint?.update(offset: -Layout.Size.buttonHeight)
        
        if isSmallScreenDevice, navigationController?.navigationBar.isHidden == true {
            navigationController?.setNavigationBarHidden(false, animated: true)
        }
        
        UIView.animate(withDuration: keyboardAnimation) {
            self.view.layoutIfNeeded()
        }
    }
    
    private func removePromoCodeButtonAndResetStackView() {
        promoCodeButton.isHidden = true
        centerContainerView.removeArrangedSubview(promoCodeStackView)
    }
    
    private func createAndInsertPromoCodeStackView() -> PromoCodeView {
        let promoCodeView = PromoCodeView()
        promoCodeStackView.addArrangedSubview(promoCodeView)
        centerContainerView.addArrangedSubview(promoCodeStackView)
        return promoCodeView
    }
}

// MARK: View Model Outputs
extension RegistrationUsernameViewController: RegistrationUsernameDisplay {
    func displayUsername(_ username: String) {
        usernameTextField.text = username
    }
    
    func displayPromoCodeAlert(_ data: PromoCodeAlertData) {
        let alertController = UIAlertController(title: data.title, message: data.message, preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = data.placeholder
        }
        
        alertController.addAction(UIAlertAction(title: data.cancelText, style: .cancel, handler: nil))
        alertController.addAction(UIAlertAction(title: data.confirmText, style: .default, handler: { _ in
            let promoCode = alertController.textFields?.first?.text
            self.viewModel.validadePromoCode(promoCode)
        }))
        
        present(alertController, animated: true)
    }
    
    func displayPromoCodeView(title: String, description: String, image: UIImage) {
        removePromoCodeButtonAndResetStackView()
        
        let promoCodeView = createAndInsertPromoCodeStackView()
        promoCodeView.configure(title: title, description: description, image: image)
    }
    
    func displayPromoCodeView(title: String, description: String, value: String?, imageURL: String?, placeholder: UIImage?) {
        removePromoCodeButtonAndResetStackView()
        
        let promoCodeView = createAndInsertPromoCodeStackView()
        promoCodeView.configure(title: title, description: description, value: value, imageURL: imageURL, placeholder: placeholder)
    }
    
    func displayInvalidPromoCodeAlert(_ alert: Alert) {
        AlertMessage.showAlert(alert, controller: self) { alertController, button, _ in
            guard case Button.Action.confirm = button.action else {
                return
            }
            
            alertController.dismiss(animated: true) {
                self.viewModel.didUsePromoCode()
            }
        }
    }
    
    func displayError(message: String) {
        errorLabel.isHidden = false
        errorLabel.text = message
    }
    
    func hideError() {
        errorLabel.isHidden = true
        errorLabel.text = nil
    }
    
    func showLoading() {
        confirmButton.startLoadingAnimating()
        promoCodeButton.isEnabled = false
        skipButton.isEnabled = false
    }
    
    func hideLoading() {
        confirmButton.stopLoadingAnimating()
        promoCodeButton.isEnabled = true
        skipButton.isEnabled = true
    }
}

extension RegistrationUsernameViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        viewModel.createUsername(textField.text)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        usernameCharSet.isSuperset(of: CharacterSet(charactersIn: string))
    }
}
