import FeatureFlag
import UIKit

enum RegistrationUsernameAction {
    case onboarding
    case startStudentAccount
    case presentWebView(String)
    case presentInviter(PPContact)
    case presentStudentAccountOffer(withValidatedApiId: String)
}

protocol RegistrationUsernameCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RegistrationUsernameAction)
}

final class RegistrationUsernameCoordinator: RegistrationUsernameCoordinating {
    typealias Dependencies = HasConsumerManager & HasFeatureManager
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func perform(action: RegistrationUsernameAction) {
        switch action {
        case .onboarding:
            presentOnboarding()
        case .presentWebView(let urlString):
            presentWebView(urlString: urlString)
        case .presentInviter(let contact):
            presentInviter(contact: contact, wsId: dependencies.consumerManager.consumer?.wsId)
        case .startStudentAccount:
            presentStudentAccount()
        case let .presentStudentAccountOffer(withValidatedApiId: apiId):
            presentStudentAccountOffer(withValidatedApiId: apiId)
        }
    }
    
    private func presentOnboarding() {
        guard let navigationController = viewController?.navigationController else {
            return
        }
        
        let onboardingCoordinator = OnboardingFlowCoordinator(
            navigationController: navigationController,
            dependencies: dependencies
        )
        
        onboardingCoordinator.start()
    }
    
    private func presentInviter(contact: PPContact, wsId: Int?) {
        let profileController = ProfilePromoCodeFactory.make(contact: contact, wsId: wsId)
        profileController.modalPresentationStyle = .overCurrentContext
        
        if #available(iOS 13.0, *) {
            profileController.isModalInPresentation = true
        }
        
        viewController?.present(profileController, animated: true)
    }
    
    private func presentWebView(urlString: String) {
        guard let currentViewController = viewController else {
            return
        }
        
        ViewsManager.presentWebViewController(withUrl: urlString, fromViewController: currentViewController)
    }
    
    private func presentStudentAccount() {
        guard let currentViewController = viewController else {
            return
        }
        
        StudentAccountCoordinator(from: currentViewController).start(from: .signUp)
    }
    
    private func presentStudentAccountOffer(withValidatedApiId apiId: String) {
        let studentOfferController = UniversityOfferRegisterFactory.make(validatedApiId: apiId)
        studentOfferController.modalPresentationStyle = .overCurrentContext

        if #available(iOS 13.0, *) {
            studentOfferController.isModalInPresentation = true
        }

        viewController?.present(studentOfferController, animated: true)
    }
}
