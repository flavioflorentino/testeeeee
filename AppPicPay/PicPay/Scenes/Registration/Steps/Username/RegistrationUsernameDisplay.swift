import UI

protocol RegistrationUsernameDisplay: AnyObject {
    func displayUsername(_ username: String)
    func displayPromoCodeAlert(_ data: PromoCodeAlertData)
    func displayPromoCodeView(title: String, description: String, image: UIImage)
    func displayPromoCodeView(title: String, description: String, value: String?, imageURL: String?, placeholder: UIImage?)
    func displayInvalidPromoCodeAlert(_ alert: Alert)
    func displayError(message: String)
    func hideError()
    func showLoading()
    func hideLoading()
}
