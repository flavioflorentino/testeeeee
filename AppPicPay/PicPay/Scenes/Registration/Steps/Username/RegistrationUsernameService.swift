import Core
import CoreLegacy
import FeatureFlag

protocol RegistrationUsernameServicing {
    var usernameFromCache: String? { get }
    var hasStudentSignUpFlow: Bool { get }
    var consumerStudentValidatedApiId: String? { get }
    func storeUsernameStepIsFinished()
    func removeReferralCodePersisted()
    func clearReferalCodeBannerDismissDate()
    func loadUsername(completion: @escaping (Result<String, ApiError>) -> Void)
    func createUsername(_ username: String, completion: @escaping (Result<Void, ApiError>) -> Void)
    func validadePromoCode(_ promoCode: String, completion: @escaping (Result<PPPromoCode, ApiError>) -> Void)
}

final class RegistrationUsernameService: RegistrationUsernameServicing {
    typealias Dependencies = HasConsumerManager & HasConsumerApi & HasMainQueue & HasKVStore & HasFeatureManager
    private let dependencies: Dependencies
    private let appParameters: AppParameters
    
    var usernameFromCache: String? {
        dependencies.consumerManager.consumer?.username
    }
    
    var hasStudentSignUpFlow: Bool {
        dependencies.featureManager.isActive(.featureStudentSignUpFlow)
    }
    
    var consumerStudentValidatedApiId: String? {
        dependencies.kvStore.stringFor(UniversityAccountKey.consumerStudentValidatedApiId)
    }

    init(dependencies: Dependencies, appParameters: AppParameters = AppParameters.global()) {
        self.dependencies = dependencies
        self.appParameters = appParameters
    }
    
    func storeUsernameStepIsFinished() {
        dependencies.kvStore.setBool(false, with: KVKey.isRegisterUsernameStep)
    }
    
    func removeReferralCodePersisted() {
        dependencies.kvStore.removeObjectFor(KVKey.firebase_dynamic_link_referral_code)
    }
    
    func clearReferalCodeBannerDismissDate() {
        appParameters.referalCodeBannerDismissDate = nil
    }
    
    func loadUsername(completion: @escaping (Result<String, ApiError>) -> Void) {
        dependencies.consumerManager.loadConsumerData { [weak self] success, error in
            guard
                success,
                let username = self?.usernameFromCache
                else {
                    self?.dependencies.mainQueue.async {
                        self?.handleLoadUsernameError(error, completion: completion)
                    }
                    return
            }
            
            self?.dependencies.mainQueue.async {
                completion(.success(username))
            }
        }
    }
    
    func createUsername(_ username: String, completion: @escaping (Result<Void, ApiError>) -> Void) {
        WSConsumer.createUsername(username) { [weak self] error in
            self?.dependencies.mainQueue.async {
                if let currentError = error {
                    var requestError = RequestError()
                    
                    if let err = currentError as NSError? {
                        requestError.code = String(err.code)
                    }
                    
                    requestError.message = currentError.localizedDescription
                    completion(.failure(ApiError.badRequest(body: requestError)))
                    return
                }

                completion(.success)
                self?.updateUsername(username)
            }
        }
    }
    
    func validadePromoCode(_ promoCode: String, completion: @escaping (Result<PPPromoCode, ApiError>) -> Void) {
        dependencies.consumerApi.validatePromotionalCode(promoCode) { [weak self] picPayResult in
            let mappedResult = picPayResult.mapError { picPayError -> ApiError in
                var requestError = RequestError()
                requestError.code = String(picPayError.code)
                requestError.message = picPayError.message
                return ApiError.badRequest(body: requestError)
            }
            
            self?.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
    
    private func handleLoadUsernameError(_ error: Error?, completion: @escaping (Result<String, ApiError>) -> Void) {
        guard let currentError = error else {
            completion(.failure(ApiError.badRequest(body: RequestError())))
            return
        }
        
        var requestError = RequestError()
        requestError.message = currentError.localizedDescription
        completion(.failure(ApiError.badRequest(body: requestError)))
    }
    
    private func updateUsername(_ username: String) {
        dependencies.consumerManager.consumer?.username = username
    }
}
