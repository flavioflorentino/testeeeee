import Foundation

enum RegistrationUsernameFactory {
    static func make(promoCode: String? = nil) -> RegistrationUsernameViewController {
        let container = DependencyContainer()
        let service: RegistrationUsernameServicing = RegistrationUsernameService(dependencies: container)
        let coordinator: RegistrationUsernameCoordinating = RegistrationUsernameCoordinator(dependencies: container)
        let presenter: RegistrationUsernamePresenting = RegistrationUsernamePresenter(
            coordinator: coordinator,
            dependencies: container
        )
        let viewModel = RegistrationUsernameViewModel(
            service: service,
            presenter: presenter,
            dependencies: container,
            promoCode: promoCode
        )
        let viewController = RegistrationUsernameViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
