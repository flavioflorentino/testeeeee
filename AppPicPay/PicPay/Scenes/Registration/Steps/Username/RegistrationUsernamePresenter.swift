import Foundation

struct PromoCodeAlertData {
    let title: String
    let message: String
    let confirmText: String
    let cancelText: String
    let placeholder: String
}

protocol RegistrationUsernamePresenting: AnyObject {
    var viewController: RegistrationUsernameDisplay? { get set }
    func presentUsernameSuggestion(_ username: String)
    func presentPromoCode(reward: PPRegisterRewardPromoCode, placeholder: UIImage?)
    func presentStudentPromoCode(_ code: String)
    func showLoading()
    func hideLoading()
    func presentPromoCodeAlert()
    func presentError(message: String)
    func hideError()
    func presentInvalidPromoCodeAlert(_ error: String)
    func didNextStep(action: RegistrationUsernameAction)
}

final class RegistrationUsernamePresenter: RegistrationUsernamePresenting {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let coordinator: RegistrationUsernameCoordinating
    weak var viewController: RegistrationUsernameDisplay?

    init(coordinator: RegistrationUsernameCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    func presentUsernameSuggestion(_ username: String) {
        viewController?.displayUsername(username)
    }
    
    func presentPromoCode(reward: PPRegisterRewardPromoCode, placeholder: UIImage?) {
       viewController?.displayPromoCodeView(
            title: reward.code,
            description: reward.inviter?.onlineName ?? "",
            value: reward.value,
            imageURL: reward.inviter?.imgUrl,
            placeholder: reward.inviter?.photoPlaceholder()
        )
    }
    
    func presentStudentPromoCode(_ code: String) {
        viewController?.displayPromoCodeView(
            title: code,
            description: StudentLocalizable.studentAccount.text,
            image: Assets.Icons.iconCapBlack.image
        )
    }
    
    func showLoading() {
        viewController?.showLoading()
    }
    
    func hideLoading() {
        viewController?.hideLoading()
    }
    
    func presentPromoCodeAlert() {
        let data = PromoCodeAlertData(
            title: RegisterLocalizable.enterYourCode.text,
            message: RegisterLocalizable.yourPromotionalCode.text,
            confirmText: SignUpLocalizable.activate.text,
            cancelText: DefaultLocalizable.btCancel.text,
            placeholder: RegisterLocalizable.promotionalCode.text
        )
        
        viewController?.displayPromoCodeAlert(data)
    }
    
    func presentError(message: String) {
        viewController?.displayError(message: message)
    }
    
    func hideError() {
        viewController?.hideError()
    }
    
    func presentInvalidPromoCodeAlert(_ error: String) {
        let alert = Alert(title: DefaultLocalizable.oops.text, text: error)
        let insertCodeButton = Button(title: RegisterLocalizable.useAnotherCode.text, type: .cta, action: .confirm)
        let cancelButton = Button(title: DefaultLocalizable.btCancel.text, type: .clean, action: .close)
        alert.buttons = [insertCodeButton, cancelButton]
        
        viewController?.displayInvalidPromoCodeAlert(alert)
    }
    
    func didNextStep(action: RegistrationUsernameAction) {
        coordinator.perform(action: action)
    }
}
