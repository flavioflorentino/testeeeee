import AnalyticsModule
import Core
import Foundation
import SecurityModule

protocol RegistrationUsernameViewModelInputs: AnyObject {
    func loadUsernameSuggestion()
    func validatePromoCodeIfNeeded()
    func validadePromoCode(_ promoCode: String?)
    func presentStudentPromoCode()
    func createUsername(_ username: String?)
    func didUsePromoCode()
    func didSkip()
    func checkStudentOffer()
    func sendLogDetection()
}

final class RegistrationUsernameViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let service: RegistrationUsernameServicing
    private let presenter: RegistrationUsernamePresenting
    private let minimumOfCharacters = 3
    private var promoCode: String?
    private var username = ""
    private var attempts = 0
    private var presentedStudentAccountRegister = false
    private let logDetectionService: LogDetectionServicing
    
    init(
        service: RegistrationUsernameServicing,
        presenter: RegistrationUsernamePresenting,
        dependencies: Dependencies,
        promoCode: String?,
        logDetectionService: LogDetectionServicing = LogDetectionService()
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.promoCode = promoCode
        self.logDetectionService = logDetectionService
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(presentStudentPromoCode),
            name: StudentAccountCoordinator.completedSignUp,
            object: nil
        )
    }
}

private extension RegistrationUsernameViewModel {
    func loadUsername() {
        presenter.showLoading()
        presenter.hideError()
        
        service.loadUsername { [weak self] result in
            self?.presenter.hideLoading()
            
            switch result {
            case .success(let username):
                self?.username = username
                self?.presenter.presentUsernameSuggestion(username)
            case .failure(let error):
                self?.handle(error: error)
            }
        }
    }
    
    func createUsername(_ username: String, attempts: Int, withSuggestion suggestion: Bool) {
        presenter.showLoading()
        presenter.hideError()
        
        service.createUsername(username) { [weak self] result in
            self?.presenter.hideLoading()
            
            let event: RegistrationEvent = .registerUsername(attempts: attempts, withSuggestion: suggestion)
            self?.dependencies.analytics.log(event)
            
            switch result {
            case .success:
                self?.goToOnboarding()
            case .failure(let error):
                self?.handle(error: error)
            }
        }
    }
    
    func goToOnboarding() {
        service.storeUsernameStepIsFinished()
        presenter.didNextStep(action: .onboarding)
    }
    
    func handle(error: ApiError) {
        let errorMessage = parseErrorMessage(error: error)
        presenter.presentError(message: errorMessage)
    }
    
    func parseErrorMessage(error: ApiError) -> String {
        switch error {
        case .badRequest(let data):
            return data.message
        case .connectionFailure:
            return DefaultLocalizable.errorConnectionViewSubtitle.text
        default:
            return DefaultLocalizable.requestError.text
        }
    }
    
    func verificationPromoCode(_ promoCode: PPPromoCode) {
        switch promoCode.data {
        case .inviterReward(let registerRewardPromoCode):
            presentPromoCodeType(reward: registerRewardPromoCode)
        case .studentAccount:
            presenter.didNextStep(action: .startStudentAccount)
            presentedStudentAccountRegister = true
        case .couponWeb(let couponWebPromoCodeData):
            presenter.didNextStep(action: .presentWebView(couponWebPromoCodeData.url))
        default: ()
        }
    }
    
    func presentPromoCodeType(reward: PPRegisterRewardPromoCode) {
        if let url = reward.webviewUrl {
            presenter.didNextStep(action: .presentWebView(url))
        } else if let contact = reward.inviter {
            presenter.didNextStep(action: .presentInviter(contact))
        }
        
        presenter.presentPromoCode(reward: reward, placeholder: reward.inviter?.photoPlaceholder())
    }
    
    func removeReferralCodePersisted() {
        service.removeReferralCodePersisted()
    }
}

extension RegistrationUsernameViewModel: RegistrationUsernameViewModelInputs {
    func loadUsernameSuggestion() {
        guard let username = service.usernameFromCache else {
            loadUsername()
            return
        }
        
        self.username = username
        presenter.presentUsernameSuggestion(username)
    }
    
    func validatePromoCodeIfNeeded() {
        guard
            let promoCode = promoCode,
            promoCode.isNotEmpty
            else {
                return
        }
        
        validadePromoCode(promoCode)
    }
    
    func validadePromoCode(_ promoCode: String?) {
        guard
            let currentPromoCode = promoCode,
            currentPromoCode.isNotEmpty
            else {
                return
        }
        
        presenter.showLoading()
        presenter.hideError()
        
        dependencies.analytics.log(RegistrationEvent.validatePromoCode(currentPromoCode))
        
        service.validadePromoCode(currentPromoCode) { [weak self] result in
            self?.presenter.hideLoading()
            
            switch result {
            case .success(let promoCode):
                self?.promoCode = currentPromoCode
                self?.verificationPromoCode(promoCode)
                self?.service.clearReferalCodeBannerDismissDate()
            case .failure(let error):
                self?.removeReferralCodePersisted()
                
                guard let errorMessage = self?.parseErrorMessage(error: error) else {
                    return
                }
                
                self?.presenter.presentInvalidPromoCodeAlert(errorMessage)
                
                self?.dependencies.analytics.log(RegistrationEvent.promoCodeError(errorMessage))
            }
        }
    }
    
    @objc
    func presentStudentPromoCode() {
        guard let promoCode = promoCode else {
            return
        }
        
        presenter.presentStudentPromoCode(promoCode)
    }
    
    func createUsername(_ username: String?) {
        guard
            let passedUsername = username,
            passedUsername.isNotEmpty,
            passedUsername.count >= minimumOfCharacters
            else {
                presenter.presentError(message: RegisterLocalizable.usernameInvalid.text)
                return
        }
        
        attempts += 1
        
        let withSuggestion = self.username.lowercased() == passedUsername.lowercased()
        createUsername(passedUsername, attempts: attempts, withSuggestion: withSuggestion)
    }
    
    func didUsePromoCode() {
        presenter.presentPromoCodeAlert()
        dependencies.analytics.log(RegistrationEvent.touchPromoCode)
    }
    
    func didSkip() {
        goToOnboarding()
    }
    
    func checkStudentOffer() {
        guard
            service.hasStudentSignUpFlow,
            !presentedStudentAccountRegister,
            let validatedApiId = service.consumerStudentValidatedApiId
            else {
                return
        }
        
        presenter.didNextStep(action: .presentStudentAccountOffer(withValidatedApiId: validatedApiId))
    }
    
    func sendLogDetection() {
        logDetectionService.sendLog()
    }
}
