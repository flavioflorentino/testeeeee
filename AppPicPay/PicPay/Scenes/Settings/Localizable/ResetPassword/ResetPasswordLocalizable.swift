enum ResetPasswordLocalizable: String, Localizable {
    case resetPasswordTitle
    case currentPasswordTextFieldPlaceholder
    case newPasswordTextFieldPlaceholder
    case newPasswordConfirmationTextFieldPlaceholder
    case resetPasswordSaveButtonTitle
    case resetPasswordOkAlertButtonTitle
    case resetPasswordSuccessAlertMessage
    case resetPasswordRequiredFieldErrorMessage
    case resetPasswordNewPasswordDoesNotMatchErrorMessage
    
    var key: String {
        self.rawValue
    }
    
    var file: LocalizableFile {
        .resetPassword
    }
}
