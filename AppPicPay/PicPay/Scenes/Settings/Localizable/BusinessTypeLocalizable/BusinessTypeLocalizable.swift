import Foundation

enum BusinessTypeLocalizable: String, Localizable {
    case title
    case selectionViewTitleLabel
    case selectionViewDescriptionLabel
    case selectionViewProButtonTitle
    case selectionViewBizButtonTitle
    
    var key: String {
        self.rawValue
    }
    
    var file: LocalizableFile {
        .businessType
    }
}
