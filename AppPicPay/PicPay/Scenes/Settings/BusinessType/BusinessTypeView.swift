import UI
import UIKit

protocol BusinessTypeSelecting: AnyObject {
    func didTapBizButton()
    func didTapProButton()
}

final class BusinessTypeView: UIView {
    private lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.text = BusinessTypeLocalizable.selectionViewTitleLabel.text
        view.textColor = Palette.ppColorGrayscale600.color
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = .center
        return view
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let view = UILabel()
        view.text = BusinessTypeLocalizable.selectionViewDescriptionLabel.text
        view.textColor = Palette.ppColorGrayscale400.color
        view.numberOfLines = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = .center
        return view
    }()
    
    private lazy var stackView: UIStackView = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .horizontal
        view.distribution = .fillEqually
        view.alignment = .fill
        view.spacing = 20
        return view
    }()
    
    private lazy var proButton: UIButtonBizType = {
        let view = UIButtonBizType()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.iconBackground = #imageLiteral(resourceName: "pro_user")
        view.setTitle(BusinessTypeLocalizable.selectionViewProButtonTitle.text, for: .normal)
        view.addTarget(self, action: #selector(proButtonAction(_:)), for: .touchUpInside)
        return view
    }()
    
    private lazy var bizButton: UIButtonBizType = {
        let view = UIButtonBizType()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.iconBackground = #imageLiteral(resourceName: "biz_user")
        view.setTitle(BusinessTypeLocalizable.selectionViewBizButtonTitle.text, for: .normal)
        view.addTarget(self, action: #selector(bizButtonAction(_:)), for: .touchUpInside)
        return view
    }()
    
    weak var delegate: BusinessTypeSelecting?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addComponents()
        setupView()
        setupConstraints()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    private func addComponents() {
        addSubviews(titleLabel, descriptionLabel, stackView)
        stackView.addArrangedSubviews(proButton, bizButton)
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 30),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            titleLabel.heightAnchor.constraint(equalToConstant: 30)
        ])

        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 24),
            descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30),
            descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -30)
        ])
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 30),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -30)
        ])
        
        NSLayoutConstraint.activate([
            proButton.heightAnchor.constraint(equalToConstant: 148),
            bizButton.heightAnchor.constraint(equalToConstant: 148)
        ])
    }
    
    @objc
    private func proButtonAction(_ sender: UIButtonBizType) {
        delegate?.didTapProButton()
    }
    
    @objc
    private func bizButtonAction(_ sender: UIButtonBizType) {
        delegate?.didTapBizButton()
    }
}
