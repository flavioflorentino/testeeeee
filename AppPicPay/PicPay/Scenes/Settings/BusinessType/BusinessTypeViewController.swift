import UI
import UIKit

final class BusinessTypeViewController: ViewController<BusinessTypeInteracting, BusinessTypeView> {
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.trackOpenView()
    }
    
    override func configureViews() {
        title = BusinessTypeLocalizable.title.text
        rootView.delegate = self
    }
}

extension BusinessTypeViewController: BusinessTypeSelecting {
    func didTapBizButton() {
        interactor.openBusinessWebview()
    }
    
    func didTapProButton() {
        interactor.openProWebview()
    }
}
