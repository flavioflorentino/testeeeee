import Foundation
import AnalyticsModule

public protocol BusinessTypeInteracting: AnyObject {
    func trackOpenView()
    func openProWebview()
    func openBusinessWebview()
}

final class BusinessTypeInteractor {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let presenter: BusinessTypePresenting

    init(presenter: BusinessTypePresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

extension BusinessTypeInteractor: BusinessTypeInteracting {
    func trackOpenView() {
        dependencies.analytics.log(ProEvent.offerViewed)
    }
    
    func openProWebview() {
        dependencies.analytics.log(ProEvent.offerInteracted(option: .liberalProfessional))
        presenter.openWebview(forBusinessType: .pro)
    }
    
    func openBusinessWebview() {
        dependencies.analytics.log(ProEvent.offerInteracted(option: .business))
        presenter.openWebview(forBusinessType: .business)
    }
}
