import UIKit

enum BusinessTypeAction {
    case openWebView(WebViewPro.Account)
}

protocol BusinessTypeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BusinessTypeAction)
}

final class BusinessTypeCoordinator: BusinessTypeCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: BusinessTypeAction) {
        switch action {
        case .openWebView(let account):
            WebViewPro.openWebView(type: account, navigationController: viewController?.navigationController)
        }
    }
}
