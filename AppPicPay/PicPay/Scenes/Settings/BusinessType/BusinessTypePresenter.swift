import Core
import Foundation

protocol BusinessTypePresenting: AnyObject {
    func openWebview(forBusinessType account: WebViewPro.Account)
}

final class BusinessTypePresenter: BusinessTypePresenting {
    private let coordinator: BusinessTypeCoordinating
    
    init(coordinator: BusinessTypeCoordinating) {
        self.coordinator = coordinator
    }
    
    func openWebview(forBusinessType account: WebViewPro.Account) {
        coordinator.perform(action: .openWebView(account))
    }
}
