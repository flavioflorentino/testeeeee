import Foundation

enum BusinessTypeFactory {
    static func make() -> BusinessTypeViewController {
        let container = DependencyContainer()
        let coordinator: BusinessTypeCoordinating = BusinessTypeCoordinator()
        let presenter: BusinessTypePresenting = BusinessTypePresenter(coordinator: coordinator)
        let interactor = BusinessTypeInteractor(presenter: presenter, dependencies: container)
        let viewController = BusinessTypeViewController(interactor: interactor)

        coordinator.viewController = viewController
        
        return viewController
    }
}
