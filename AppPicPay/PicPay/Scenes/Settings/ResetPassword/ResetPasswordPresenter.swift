import Core
import Foundation

protocol ResetPasswordPresenting: AnyObject {
    var viewController: ResetPasswordDisplay? { get set }
    func setNumericTextFieldForPasswordTextFields()
    func display(error: Error)
    func display(error: ResetPasswordError)
    func clearFormErrors()
    func displayLoading()
    func hideLoading()
    func displaySuccessfullyChangedPasswordAlert()
    func didNextStep(action: ResetPasswordAction)
}

final class ResetPasswordPresenter: ResetPasswordPresenting {
    private let coordinator: ResetPasswordCoordinating
    weak var viewController: ResetPasswordDisplay?

    init(coordinator: ResetPasswordCoordinating) {
        self.coordinator = coordinator
    }
    
    func setNumericTextFieldForPasswordTextFields() {
        viewController?.setNumericTextFieldForPasswordTextFields()
    }
    
    func display(error: Error) {
        viewController?.display(error: error)
    }
    
    func display(error: ResetPasswordError) {
        switch error {
        case .newPasswordDoesNotMatch:
            viewController?.displayAlert(message: error.localizedDescription)
        case .missingCurrentPassword:
            viewController?.displayCurrentPassword(errorMessage: error.localizedDescription)
        case .missingNewPassword:
            viewController?.displayNewPassword(errorMessage: error.localizedDescription)
        case .missingNewPasswordConfirmation:
            viewController?.displayNewPasswordConfirmation(errorMessage: error.localizedDescription)
        }
    }
    
    func clearFormErrors() {
        viewController?.displayCurrentPassword(errorMessage: nil)
        viewController?.displayNewPassword(errorMessage: nil)
        viewController?.displayNewPasswordConfirmation(errorMessage: nil)
    }
    
    func displaySuccessfullyChangedPasswordAlert() {
        viewController?.displaySuccessfullyChangePasswordAlert(message: ResetPasswordLocalizable.resetPasswordSuccessAlertMessage.text)
    }
    
    func displayLoading() {
        viewController?.displayLoading()
    }
    
    func hideLoading() {
        viewController?.hideLoading()
    }
    
    func didNextStep(action: ResetPasswordAction) {
        coordinator.perform(action: action)
    }
}
