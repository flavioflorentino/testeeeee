import Foundation

protocol ResetPasswordServicing {
    func change(currentPassword: String, with newPassword: String, completion: @escaping (Result<Bool, Error>) -> Void)
}

final class ResetPasswordService: ResetPasswordServicing {
    func change(currentPassword: String, with newPassword: String, completion: @escaping (Result<Bool, Error>) -> Void) {
        WSPasswordChangeRequest.changePassword(currentPassword, newPassword: newPassword) { success, error in
            DispatchQueue.main.async {
                guard let error = error else {
                    completion(.success(success))
                    return
                }
                completion(.failure(error))
            }
        }
    }
}
