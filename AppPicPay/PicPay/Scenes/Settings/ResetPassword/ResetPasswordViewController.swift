import SecurityModule
import UI
import UIKit
import Validator

private extension ResetPasswordViewController.Layout {
    static let spacingBetweenFields: CGFloat = 20
    static let spacingBetweenFormAndButton: CGFloat = 32
    
    static let saveButtonHeight: CGFloat = 44
    
    static let rootStackViewLayoutMargins = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    
    static let textFieldFont: UIFont = .systemFont(ofSize: 16)
}

final class ResetPasswordViewController: ViewController<ResetPasswordViewModelInputs, UIView>, Obfuscable {
    fileprivate enum Layout {}
    
    private enum AccessibilityIdentifiers: String {
        case currentPasswordTextField = "current_password_textfield"
        case newPasswordTextField = "new_password_textfield"
        case newPasswordConfirmationTextField = "new_password_confirmation_textfield"
        case saveButton = "save_button"
    }
    
    // MARK: - Fields
    
    private(set) lazy var currentPasswordTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = ResetPasswordLocalizable.currentPasswordTextFieldPlaceholder.text
        textField.isSecureTextEntry = true
        textField.font = Layout.textFieldFont
        textField.accessibilityIdentifier = AccessibilityIdentifiers.currentPasswordTextField.rawValue
        textField.keyboardType = .numberPad
        if #available(iOS 11.0, *) {
            textField.textContentType = .password
        }
        return textField
    }()
    
    private(set) lazy var newPasswordTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = ResetPasswordLocalizable.newPasswordTextFieldPlaceholder.text
        textField.isSecureTextEntry = true
        textField.font = Layout.textFieldFont
        textField.accessibilityIdentifier = AccessibilityIdentifiers.newPasswordTextField.rawValue
        textField.keyboardType = .numberPad
        if #available(iOS 12.0, *) {
            textField.textContentType = .newPassword
            textField.passwordRules = UITextInputPasswordRules(descriptor: "required: digit; allowed: digit; minlength: 4;")
        }
        return textField
    }()
    
    private(set) lazy var newPasswordConfirmationTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = ResetPasswordLocalizable.newPasswordConfirmationTextFieldPlaceholder.text
        textField.isSecureTextEntry = true
        textField.font = Layout.textFieldFont
        textField.accessibilityIdentifier = AccessibilityIdentifiers.newPasswordConfirmationTextField.rawValue
        textField.keyboardType = .numberPad
        if #available(iOS 12.0, *) {
            textField.textContentType = .newPassword
            textField.passwordRules = UITextInputPasswordRules(descriptor: "required: digit; allowed: digit; minlength: 4;")
        }
        return textField
    }()
    
    // MARK: Buttons
    
    private(set) lazy var saveButton: UIPPButton = {
        let button = UIPPButton(title: ResetPasswordLocalizable.resetPasswordSaveButtonTitle.text, target: self, action: #selector(saveButtonTapped(_:)))
        button.normalBackgrounColor = Palette.ppColorBranding300.color
        button.accessibilityIdentifier = AccessibilityIdentifiers.saveButton.rawValue
        return button
    }()
    
    // MARK: Containers
    
    private lazy var formStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [currentPasswordTextField, newPasswordTextField, newPasswordConfirmationTextField])
        stackView.axis = .vertical
        stackView.spacing = Layout.spacingBetweenFields
        stackView.distribution = .fill
        stackView.alignment = .fill
        return stackView
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [formStackView, saveButton])
        stackView.axis = .vertical
        stackView.spacing = Layout.spacingBetweenFormAndButton
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.layoutMargins = Layout.rootStackViewLayoutMargins
        stackView.preservesSuperviewLayoutMargins = false
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    private lazy var scrollView = UIScrollView()
    
    var appSwitcherObfuscator: AppSwitcherObfuscating?
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.fetchConsumerConfiguration()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupObfuscationConfiguration()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerForKeyboardNotifications()
        currentPasswordTextField.becomeFirstResponder()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        unregisterForKeyboardNotifications()
        deinitObfuscator()
    }
    
    // MARK: - Layout
 
    override func buildViewHierarchy() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        rootStackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        scrollView.addSubview(rootStackView)
    }
    
    override func setupConstraints() {
        scrollView.layout {
            $0.top == view.topAnchor
            $0.trailing == view.trailingAnchor
            $0.bottom == view.bottomAnchor
            $0.leading == view.leadingAnchor
        }
        
        rootStackView.layout {
            $0.top == scrollView.topAnchor
            $0.trailing == scrollView.trailingAnchor
            $0.bottom == scrollView.bottomAnchor
            $0.leading == scrollView.leadingAnchor
            $0.width == view.widthAnchor
        }
        
        saveButton.layout {
            $0.height == Layout.saveButtonHeight
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        title = ResetPasswordLocalizable.resetPasswordTitle.text
    }
    
    // MARK: - Keyboard handling
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(keyboardDidChange(_:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardDidChange(_:)), name: UIResponder.keyboardDidChangeFrameNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardDidHide), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    func unregisterForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self)
    }
}
extension ResetPasswordViewController {
    // MARK: - Actions
    
    @objc
    func saveButtonTapped(_ sender: Any) {
        viewModel.changePassword(
            currentPasswordTextField.text,
            newPassword: newPasswordTextField.text,
            newPasswordConfirmation: newPasswordConfirmationTextField.text
        )
    }
    
    // MARK: Keyboard events
    
    @objc
    func keyboardDidChange(_ notification: Notification) {
        guard let keyboardFrame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else {
            return
        }
        scrollView.contentInset.bottom = keyboardFrame.height
    }
    
    @objc
    func keyboardDidHide(_ notification: Notification) {
        scrollView.contentInset.bottom = 0
    }
}
// MARK: - View Model Outputs
extension ResetPasswordViewController: ResetPasswordDisplay {
    func display(error: Error) {
        AlertMessage.showCustomAlertWithError(error, controller: self)
    }
    
    func displayAlert(message: String) {
        AlertMessage.showAlert(Alert(title: nil, text: message), controller: self)
    }
    
    func displaySuccessfullyChangePasswordAlert(message: String) {
        let buttons = [Button(title: ResetPasswordLocalizable.resetPasswordOkAlertButtonTitle.text)]
        let alert = Alert(with: "", text: message, buttons: buttons)
        AlertMessage.showAlert(alert, controller: self) { [weak self] _, _, _ in
            self?.viewModel.successAlertOkButtonTapped()
        }
    }
    
    func displayCurrentPassword(errorMessage: String?) {
        currentPasswordTextField.errorMessage = errorMessage
    }
    
    func displayNewPassword(errorMessage: String?) {
        newPasswordTextField.errorMessage = errorMessage
    }
    
    func displayNewPasswordConfirmation(errorMessage: String?) {
        newPasswordConfirmationTextField.errorMessage = errorMessage
    }
    
    func setNumericTextFieldForPasswordTextFields() {
        currentPasswordTextField.keyboardType = .numberPad
        newPasswordTextField.keyboardType = .numberPad
        newPasswordConfirmationTextField.keyboardType = .numberPad
    }
    
    func displayLoading() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: UIView())
        currentPasswordTextField.isEnabled = false
        newPasswordTextField.isEnabled = false
        newPasswordConfirmationTextField.isEnabled = false
        saveButton.startLoadingAnimating()
    }
    
    func hideLoading() {
        navigationItem.leftBarButtonItem = nil
        currentPasswordTextField.isEnabled = true
        newPasswordTextField.isEnabled = true
        newPasswordConfirmationTextField.isEnabled = true
        saveButton.stopLoadingAnimating()
    }
}
