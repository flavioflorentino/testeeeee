import Foundation

enum ResetPasswordError: LocalizedError {
    case missingCurrentPassword
    case missingNewPassword
    case missingNewPasswordConfirmation
    case newPasswordDoesNotMatch
    
    var errorDescription: String? {
        switch self {
        case .missingCurrentPassword, .missingNewPassword, .missingNewPasswordConfirmation:
            return ResetPasswordLocalizable.resetPasswordRequiredFieldErrorMessage.text
        case .newPasswordDoesNotMatch:
            return ResetPasswordLocalizable.resetPasswordNewPasswordDoesNotMatchErrorMessage.text
        }
    }
}

protocol ResetPasswordViewModelInputs: AnyObject {
    func fetchConsumerConfiguration()
    func changePassword(_ currentPassword: String?, newPassword: String?, newPasswordConfirmation: String?)
    func successAlertOkButtonTapped()
}

final class ResetPasswordViewModel {
    private let service: ResetPasswordServicing
    private let presenter: ResetPasswordPresenting

    init(service: ResetPasswordServicing, presenter: ResetPasswordPresenting) {
        self.service = service
        self.presenter = presenter
    }
    
    func formValidation(currentPassword: String?, newPassword: String?, newPasswordConfirmation: String?) -> [ResetPasswordError] {
        var errors: [ResetPasswordError] = []
        
        if currentPassword.isNilOrEmpty {
            errors.append(.missingCurrentPassword)
        }
        if newPassword.isNilOrEmpty {
            errors.append(.missingNewPassword)
        }
        if newPasswordConfirmation.isNilOrEmpty {
            errors.append(.missingNewPasswordConfirmation)
        }
        
        if newPassword != newPasswordConfirmation && errors.isEmpty {
            errors.append(.newPasswordDoesNotMatch)
        }
        
        return errors
    }
}

extension ResetPasswordViewModel: ResetPasswordViewModelInputs {
    func fetchConsumerConfiguration() {
        if ConsumerManager.shared.consumer?.numericKeyboardForPassword == true {
            presenter.setNumericTextFieldForPasswordTextFields()
        }
    }
    
    func changePassword(_ currentPassword: String?, newPassword: String?, newPasswordConfirmation: String?) {
        presenter.clearFormErrors()
        let errors = formValidation(
            currentPassword: currentPassword,
            newPassword: newPassword,
            newPasswordConfirmation: newPasswordConfirmation
        )
        
        guard let currentPassword = currentPassword, let newPassword = newPassword, errors.isEmpty else {
            errors.forEach(presenter.display(error:))
            return
        }
        presenter.displayLoading()
        service.change(currentPassword: currentPassword, with: newPassword) { [weak self] result in
            self?.presenter.hideLoading()
            switch result {
            case .success:
                self?.presenter.displaySuccessfullyChangedPasswordAlert()
            case .failure(let error):
                self?.presenter.display(error: error)
            }
        }
    }
    
    func successAlertOkButtonTapped() {
        presenter.didNextStep(action: .dismiss)
    }
}
