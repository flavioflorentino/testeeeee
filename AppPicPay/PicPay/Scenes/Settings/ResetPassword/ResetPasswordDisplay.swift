import UIKit

protocol ResetPasswordDisplay: AnyObject {
    func display(error: Error)
    func displayAlert(message: String)
    func displaySuccessfullyChangePasswordAlert(message: String)
    func displayCurrentPassword(errorMessage: String?)
    func displayNewPassword(errorMessage: String?)
    func displayNewPasswordConfirmation(errorMessage: String?)
    func setNumericTextFieldForPasswordTextFields()
    func displayLoading()
    func hideLoading()
}
