import FeatureFlag
import UIKit

enum ResetPasswordFactory {
    static func make() -> UIViewController {
        guard FeatureManager.isActive(.newChangePassword) else {
            return ChangePasswordViewController()
        }
        let service: ResetPasswordServicing = ResetPasswordService()
        let coordinator: ResetPasswordCoordinating = ResetPasswordCoordinator()
        let presenter: ResetPasswordPresenting = ResetPasswordPresenter(coordinator: coordinator)
        let viewModel = ResetPasswordViewModel(service: service, presenter: presenter)
        let viewController = ResetPasswordViewController(viewModel: viewModel)
        
        coordinator.viewController = viewController
        presenter.viewController = viewController
        
        return viewController
    }
}
