import UIKit

enum ResetPasswordAction {
    case dismiss
}

protocol ResetPasswordCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ResetPasswordAction)
}

final class ResetPasswordCoordinator: ResetPasswordCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: ResetPasswordAction) {
        if action == .dismiss {
            guard let navigationController = viewController?.navigationController else {
                viewController?.dismiss(animated: true, completion: nil)
                return
            }
            navigationController.popViewController(animated: true)
        }
    }
}
