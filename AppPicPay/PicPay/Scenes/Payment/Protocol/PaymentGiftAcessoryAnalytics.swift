protocol PaymentGiftAcessoryAnalytics {
    func didSelectGift(_ gift: PaymentGift)
}

extension PaymentGiftAcessoryAnalytics {
    func didSelectGift(_ gift: PaymentGift) {}
}
