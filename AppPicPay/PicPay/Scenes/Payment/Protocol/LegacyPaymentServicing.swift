import CorePayment
import ReceiptKit

protocol ServiceReceiptProtocol: LegacyPaymentServicing where Success == ReceiptWidgetViewModel, Failure == PicPayError {}

protocol ReceiptServicing: LegacyPaymentServicing where Success == ReceiptEntryPoint, Failure == PicPayError {}
