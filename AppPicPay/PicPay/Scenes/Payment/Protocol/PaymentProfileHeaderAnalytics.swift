protocol PaymentProfileHeaderAnalytics {
    func willOpenProfile()
    func willFollow()
}

extension PaymentProfileHeaderAnalytics {
    func willOpenProfile() {}
    func willFollow() {}
}
