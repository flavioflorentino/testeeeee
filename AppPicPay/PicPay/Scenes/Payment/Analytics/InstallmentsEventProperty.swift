import FeatureFlag
import Foundation

struct InstallmentsEventProperty: ApplicationEventProperty {
    typealias Dependency = HasFeatureManager

    // MARK: - Properties
    private let dependency: Dependency

    init(dependency: Dependency) {
        self.dependency = dependency
    }
    
    var properties: [String: Any] {
        [
            "is_checkout_inverted_installment": dependency.featureManager.isActive(.experimentInvertedInstallment)
        ]
    }
}
