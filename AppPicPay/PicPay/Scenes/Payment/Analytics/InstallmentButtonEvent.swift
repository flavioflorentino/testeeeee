import AnalyticsModule

enum InstallmentButtonEvent: AnalyticsKeyProtocol {
    case installmentAccessed(_ paymentValue: String, _ isInvertedInstallment: Bool)
    case installmentSelected(_ installmentNumber: String, _ installmentValue: String, _ isInvertedInstallment: Bool)
    
    private var name: String {
        switch self {
        case .installmentAccessed:
            return "Installment Accessed"
        case .installmentSelected:
            return "Installments Selected"
        }
    }
    
    var properties: [String: Any] {
        switch self {
        case let .installmentAccessed(value, isInvertedInstallment):
            return [
                "value": value,
                "is_inverted_installment": isInvertedInstallment
            ]
        case let .installmentSelected(number, value, isInvertedInstallment):
            return [
                "installments": number,
                "value": value,
                "is_inverted_installment": isInvertedInstallment
            ]
        }
    }
    
    var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

extension InstallmentButtonEvent {
    enum InstallmentButtonType: Int {
        case old = 1
        case new
    }
}
