import Foundation

struct HeaderPayment {
    let sellerId: String?
    let payeeId: String
    let value: Double
    let installments: Int
    let payeeType: PayeeType
    let paymentType: PaymentType
}

extension HeaderPayment {
    enum PaymentType {
        case billet
        case p2p
        case p2m
        case pav
        case pix
        case ecommerce
        case voucher
        case phoneRecharge
        case tvRecharge
        case parking
        case birthday
        case newFriendGift
        case transitPass
        case invoiceCard
        case p2pLending
        case store
    }
    
    enum PayeeType {
        case pro
        case verify
        case none
    }
}
