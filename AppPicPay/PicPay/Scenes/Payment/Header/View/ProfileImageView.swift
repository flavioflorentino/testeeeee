import Foundation
import UI

final class ProfileImageView: UIView, ViewConfiguration {
    private enum Layout {
        static let marginTopBottom: CGFloat = 10
        static let marginLeadingTrailing: CGFloat = 12
        static let sizeImage: CGFloat = 60
        static let sizeVerified: CGFloat = 18
        static let widthPro: CGFloat = 32
        static let heightPro: CGFloat = 16
    }
    
    private lazy var imageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        image.translatesAutoresizingMaskIntoConstraints = false
        
        return image
    }()
    
    private lazy var badgeProImage: UIImageView = {
        let image = UIImageView()
        image.image = Assets.Icons.icoProBadgeProfile.image
        image.isHidden = true
        image.translatesAutoresizingMaskIntoConstraints = false
        
        return image
    }()
    
    private lazy var badgeVerifiedImage: UIImageView = {
        let image = UIImageView()
        image.image = Assets.Icons.icoVerifiedProfile.image
        image.isHidden = true
        image.layer.cornerRadius = Layout.sizeVerified / 2
        image.clipsToBounds = true
        image.translatesAutoresizingMaskIntoConstraints = false
        
        return image
    }()
    
    var sizeImage: CGFloat = Layout.sizeImage {
        didSet {
            imageView.layer.cornerRadius = sizeImage / 2
        }
    }
    
    var profileType: HeaderPayment.PayeeType = .none {
        didSet {
            switch profileType {
            case .pro:
                badgeProImage.isHidden = false
                badgeVerifiedImage.isHidden = true
            case .verify:
                badgeVerifiedImage.isHidden = false
                badgeProImage.isHidden = true
            case .none:
                badgeVerifiedImage.isHidden = true
                badgeProImage.isHidden = true
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
       fatalError("init(coder:) has not been implemented")
    }
      
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        buildLayout()
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
       
    func buildViewHierarchy() {
        addSubview(imageView)
        addSubview(badgeProImage)
        addSubview(badgeVerifiedImage)
    }
       
    func setupConstraints() {
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: Layout.marginTopBottom),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Layout.marginLeadingTrailing),
            imageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Layout.marginTopBottom),
            imageView.widthAnchor.constraint(equalToConstant: sizeImage),
            imageView.heightAnchor.constraint(equalToConstant: sizeImage)
        ])
        
        NSLayoutConstraint.activate([
            badgeVerifiedImage.bottomAnchor.constraint(equalTo: imageView.bottomAnchor),
            badgeVerifiedImage.trailingAnchor.constraint(equalTo: imageView.trailingAnchor),
            badgeVerifiedImage.widthAnchor.constraint(equalToConstant: Layout.sizeVerified),
            badgeVerifiedImage.heightAnchor.constraint(equalToConstant: Layout.sizeVerified)
        ])
        
        NSLayoutConstraint.activate([
            badgeProImage.centerYAnchor.constraint(equalTo: imageView.bottomAnchor),
            badgeProImage.centerXAnchor.constraint(equalTo: imageView.centerXAnchor),
            badgeProImage.widthAnchor.constraint(equalToConstant: Layout.widthPro),
            badgeProImage.heightAnchor.constraint(equalToConstant: Layout.heightPro)
        ])
    }
    
    func setImage(image: URL?, placeholder: UIImage) {
        imageView.setImage(url: image, placeholder: placeholder)
    }
}
