import Foundation

struct HeaderStandard {
    let title: String
    let description: String?
    let image: ImageHeader
    let isEditable: Bool
    let link: String?
    let newInstallmentEnable: Bool
    let isInstallmentsEnabled: Bool
}

extension HeaderStandard {
    enum ImageHeader {
        case url(String?)
        case image(UIImage)
    }
}
