import FeatureFlag
import Foundation

protocol PaymentStandardHeaderServicing {
    func usePicPayBalance() -> Bool
    func installmentActivated(type: HeaderPayment.PaymentType) -> Bool
    func numberInstallmentAvailable(total: Double, installment: Int) -> Int
    func cardValue(total: Double, installment: Int) -> Double
}

final class PaymentStandardHeaderService: PaymentStandardHeaderServicing {
    private let paymentManager: PPPaymentManager
    
    init(paymentManager: PPPaymentManager) {
        self.paymentManager = paymentManager
    }
    
    func usePicPayBalance() -> Bool {
        paymentManager.usePicPayBalance()
    }
    
    func cardValue(total: Double, installment: Int) -> Double {
        paymentManager.selectedQuotaQuantity = installment
        paymentManager.subtotal = NSDecimalNumber(value: total)
        
        return paymentManager.cardTotal()?.doubleValue ?? 0
    }
    
    func numberInstallmentAvailable(total: Double, installment: Int) -> Int {
        paymentManager.selectedQuotaQuantity = installment
        paymentManager.subtotal = NSDecimalNumber(value: total)
        
        return paymentManager.installmentsList()?.count ?? 0
    }
    
    func installmentActivated(type: HeaderPayment.PaymentType) -> Bool {
        type == .p2p ? FeatureManager.isActive(.isActiveInstallmentP2P) : false
    }
}
