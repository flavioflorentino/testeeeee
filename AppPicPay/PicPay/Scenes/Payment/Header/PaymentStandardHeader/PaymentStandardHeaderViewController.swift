import AnalyticsModule
import CorePayment
import UI
import UIKit

private extension PaymentStandardHeaderViewController.Layout {
    enum Size {
        static let widthButton: CGFloat = 130
        static let heightInputValue: CGFloat = 30
        static let sizeButton: CGFloat = 32
        static let sizeProfileImage: CGFloat = 60
        static let sizeLine: CGFloat = 1
        static let installmentButtonHeight: CGFloat = 30
    }
    
    enum Margin {
        static let marginLeadingTrailing: CGFloat = 12
        static let newInstallmentButtonContentInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        static let installmentButtonContentInsets = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
    }
    
    enum Font {
        static let font00 = UIFont.systemFont(ofSize: 15)
        static let font01 = UIFont.systemFont(ofSize: 27, weight: .regular)
    }
}

final class PaymentStandardHeaderViewController: ViewController<PaymentStandardHeaderViewModelInputs, UIView>, PaymentHeaderInput {
    typealias ButtonAction = (title: String, style: PopupAction.PopupActionStyle, action: AlertAction)
    enum AlertAction {
        case changePaymentMethod
        case okGotIt
        case notNow
    }
    fileprivate enum Layout { }
    
    private lazy var titleLabel = UILabel()
    private lazy var descLabel = UILabel()
    
    private lazy var profileImage: ProfileImageView = {
        let profileImage = ProfileImageView()
        profileImage.sizeImage = Layout.Size.sizeProfileImage
        
        return profileImage
    }()
    
    private lazy var installmentsTooltip: InstallmentsTooltip = {
        let tooltip = InstallmentsTooltip(view: view, toolbarView: nil)
        return tooltip
    }()
    
    private lazy var paymentInfoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .leading
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        
        return stackView
    }()
    
    private lazy var inputValue: CurrencyField = {
        let input = CurrencyField()
        input.fontCurrency = Layout.Font.font01
        input.fontValue = Layout.Font.font01
        input.placeholderColor = Palette.ppColorGrayscale400.color
        input.textColor = Colors.grayscale800.color
        input.positionCurrency = .center
        input.limitValue = 9
        input.delegate = self
        
        return input
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale200.color
        
        return view
    }()
    
    private lazy var installmentButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = Layout.Font.font00
        button.layer.cornerRadius = CornerRadius.light
        button.layer.borderWidth = Border.light
        button.addTarget(self, action: #selector(didTapInstallment), for: .touchUpInside)
        button.contentEdgeInsets = Layout.Margin.installmentButtonContentInsets
        
        return button
    }()
    
    private lazy var newInstallmentButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = Layout.Font.font00
        button.layer.cornerRadius = CornerRadius.light
        button.layer.borderWidth = Border.light
        button.addTarget(self, action: #selector(didTapInstallment), for: .touchUpInside)
        button.contentEdgeInsets = Layout.Margin.installmentButtonContentInsets
        
        return button
    }()
    
    weak var delegate: PaymentHeaderOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.updateValues()
        inputValue.becomeResponder()
    }
    
    override func configureViews() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        descLabel.translatesAutoresizingMaskIntoConstraints = false
        profileImage.translatesAutoresizingMaskIntoConstraints = false
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapImage))
        profileImage.addGestureRecognizer(gesture)
    }
    
    override func buildViewHierarchy() {
        paymentInfoStackView.addArrangedSubview(titleLabel)
        paymentInfoStackView.addArrangedSubview(descLabel)
        paymentInfoStackView.addArrangedSubview(inputValue)
        paymentInfoStackView.addArrangedSubview(newInstallmentButton)
        
        view.addSubview(profileImage)
        view.addSubview(paymentInfoStackView)
        view.addSubview(installmentButton)
        view.addSubview(lineView)
    }
    
    override func setupConstraints() {
        profileImage.layout {
            $0.top == view.topAnchor
            $0.leading == view.leadingAnchor + Spacing.base02
        }
        
        paymentInfoStackView.layout {
            $0.top == view.topAnchor + Spacing.base02
            $0.leading == profileImage.trailingAnchor
        }
        
        installmentButton.layout {
            $0.centerY == paymentInfoStackView.centerYAnchor
            $0.leading == paymentInfoStackView.trailingAnchor + Layout.Margin.marginLeadingTrailing
            $0.trailing == view.trailingAnchor - Layout.Margin.marginLeadingTrailing
            $0.width == Layout.Size.sizeButton
            $0.height == Layout.Size.sizeButton
        }
        
        newInstallmentButton.layout {
            $0.width == Layout.Size.widthButton
            $0.height == Layout.Size.sizeButton
        }
        
        lineView.layout {
            $0.top == paymentInfoStackView.bottomAnchor + Spacing.base02
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
            $0.bottom == view.bottomAnchor
            $0.height == Layout.Size.sizeLine
        }
        
        inputValue.layout {
            $0.height == Layout.Size.heightInputValue
        }
    }
    
    @objc
    private func didTapInstallment() {
        dismissKeyboard()
        delegate?.didTapInstallments()
        viewModel.didTapInstallment()
    }
    
    @objc
    private func didTapImage() {
        viewModel.didTapImage()
    }
    
    @objc
    private func didTapInfoButton() {
        viewModel.didTapInfoButton()
    }
    
    func appearKeyboard() {
        inputValue.becomeResponder()
    }
    
    func dismissKeyboard() {
        inputValue.resignResponder()
    }
    
    func changePayment(value: Double) {
        viewModel.changeValue(value: value)
    }
}

// MARK: View Model Outputs
extension PaymentStandardHeaderViewController: PaymentStandardHeaderDisplay {
    func displayInfoButton() {
        let infoButton = UIButton(type: .infoLight)
        infoButton.addTarget(self, action: #selector(didTapInfoButton), for: .touchUpInside)
        parent?.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: infoButton)
    }
    
    func displayTooltip(cardValue: Double) {
        installmentsTooltip.show(view: view, from: installmentButton, cardValue: NSDecimalNumber(value: cardValue))
    }
    
    func displayTooltipNewInstallment(cardValue: Double) {
        var frame = newInstallmentButton.frame
        frame.origin.x = paymentInfoStackView.frame.origin.x
        installmentsTooltip.show(view: view, from: frame, cardValue: NSDecimalNumber(value: cardValue))
    }
    
    func description(isHidden: Bool) {
        descLabel.isHidden = isHidden
    }
    
    func setupInput(value: Double, isActive: Bool) {
        inputValue.value = value
        inputValue.isEnabled = isActive
    }
    
    func updateValue(_ value: Double) {
        delegate?.headerUpdateValue(value: value)
    }
    
    func setupTitle(title: String, color: UIColor, ofSize: CGFloat) {
        titleLabel.text = title
        titleLabel.font = UIFont.systemFont(ofSize: ofSize)
        titleLabel.textColor = color
    }
    
    func setupDescription(description: String?, color: UIColor, ofSize: CGFloat) {
        descLabel.numberOfLines = 0
        descLabel.text = description
        descLabel.font = UIFont.systemFont(ofSize: ofSize)
        descLabel.textColor = color
    }
    
    func displayInstallmentButton(title: String?, image: UIImage?, color: UIColor, borderColor: UIColor) {
        installmentButton.setTitle(title, for: .normal)
        installmentButton.setTitleColor(color, for: .normal)
        installmentButton.tintColor = color
        installmentButton.layer.borderColor = borderColor.cgColor
        
        newInstallmentButton.setTitle(title, for: .normal)
        newInstallmentButton.setTitleColor(color, for: .normal)
        newInstallmentButton.tintColor = color
        newInstallmentButton.layer.borderColor = borderColor.cgColor
    }
    
    func oldInstallmentButton(isHidden: Bool) {
        installmentButton.isHidden = isHidden
    }
    
    func newInstallmentButton(isHidden: Bool) {
        newInstallmentButton.isHidden = isHidden
    }
    
    func updateInstallment(installment: Int, forceCreditCard: Bool) {
        delegate?.headerUpdateInstallment(installment: installment, forceCreditCard: forceCreditCard)
    }
    
    func setupImage(imageUrl: URL?, placeholder: UIImage, type: HeaderPayment.PayeeType) {
        profileImage.setImage(image: imageUrl, placeholder: placeholder)
        profileImage.profileType = type
    }

    func showAlert(_ alert: Alert) {
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func didReceiveAnInstallmentError(title: String, description: NSMutableAttributedString, image: UIImage?, buttons: [ButtonAction]) {
        let controller = PopupViewController(title: title, image: image)
        controller.setAttributeDescription(description)
        controller.hideCloseButton = true
        for button in buttons {
            var completion: (() -> Void)?
            switch button.action {
            case .changePaymentMethod:
                completion = { [weak self] in
                    self?.delegate?.didTapPaymentMethodSelection()
                    self?.viewModel.didTapInstallmentAlert(action: .changePaymentMethod)
                }
            default:
                completion = { [weak self] in
                    self?.viewModel.didTapInstallmentAlert(action: button.action)
                }
            }
            let newButton = PopupAction(title: button.title, style: button.style, completion: completion)
            controller.addAction(newButton)
        }
        controller.modalPresentationStyle = .overFullScreen        
        showPopup(controller)
    }
}

extension PaymentStandardHeaderViewController: CurrencyFieldDelegate {
    func onValueChange(value: Double) {
        viewModel.changeValue(value: value)
    }
}
