import UIKit

protocol PaymentStandardHeaderDisplay: AnyObject {
    func displayTooltip(cardValue: Double)
    func displayInfoButton()
    func displayTooltipNewInstallment(cardValue: Double)
    func description(isHidden: Bool)
    func oldInstallmentButton(isHidden: Bool)
    func newInstallmentButton(isHidden: Bool)
    func setupInput(value: Double, isActive: Bool)
    func updateValue(_ value: Double)
    func setupTitle(title: String, color: UIColor, ofSize: CGFloat)
    func setupDescription(description: String?, color: UIColor, ofSize: CGFloat)
    func displayInstallmentButton(title: String?, image: UIImage?, color: UIColor, borderColor: UIColor)
    func updateInstallment(installment: Int, forceCreditCard: Bool)
    func setupImage(imageUrl: URL?, placeholder: UIImage, type: HeaderPayment.PayeeType)
    func showAlert(_ alert: Alert)
    func didReceiveAnInstallmentError(title: String, description: NSMutableAttributedString, image: UIImage?, buttons: [PaymentStandardHeaderViewController.ButtonAction])
}
