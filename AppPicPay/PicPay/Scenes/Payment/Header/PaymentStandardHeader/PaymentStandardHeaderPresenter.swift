import AnalyticsModule
import Core
import Foundation
import UI

protocol PaymentStandardHeaderPresenting: AnyObject {
    var viewController: PaymentStandardHeaderDisplay? { get set }
    func viewDidLoad(value: Double, payeeType: HeaderPayment.PayeeType, paymentType: HeaderPayment.PaymentType)
    func displayInstallment(isEnable: Bool, installment: Int)
    func updateInput(value: Double)
    func updateCardValue(cardValue: Double)
    func errorInstallment(type: PaymentStandardHeaderPresenter.InstallmentError)
    func presentInstallmentsDisabledAlert(withDescription description: String)
    func presentError(message: String)
    func didTapInfoButton()
    func didNextStep(action: PaymentStandardHeaderAction)
}

final class PaymentStandardHeaderPresenter: PaymentStandardHeaderPresenting {
    enum InstallmentError {
        case emptyValueAndWrongPaymentMethod
        case wrongPaymentMethod
        case emptyValue
    }
    
    private let coordinator: PaymentStandardHeaderCoordinating
    private let model: HeaderStandard
    weak var viewController: PaymentStandardHeaderDisplay?
    
    init(model: HeaderStandard, coordinator: PaymentStandardHeaderCoordinating) {
        self.model = model
        self.coordinator = coordinator
    }
    
    func viewDidLoad(value: Double, payeeType: HeaderPayment.PayeeType, paymentType: HeaderPayment.PaymentType) {
        updateInput(value: value)
        setupInfoButton()
        switch paymentType {
        case .billet:
            setupBillet(type: payeeType)
        case .p2p:
            setupP2P(type: payeeType)
        case .pix:
            setupPIX(type: payeeType)
        case .pav:
            setupPAV(type: payeeType)
        case .p2pLending:
            setupP2PLending(type: payeeType)
        case .voucher, .phoneRecharge, .tvRecharge, .parking, .transitPass, .invoiceCard, .store:
            setupDigitalGoods(type: payeeType)
        default:
            break
        }
    }
    
    func updateInput(value: Double) {
        viewController?.updateValue(value)
        viewController?.setupInput(value: value, isActive: model.isEditable)
    }
    
    func updateCardValue(cardValue: Double) {
        guard model.isInstallmentsEnabled else {
            return
        }
        guard model.newInstallmentEnable else {
            viewController?.displayTooltip(cardValue: cardValue)
            return
        }
        viewController?.displayTooltipNewInstallment(cardValue: cardValue)
    }
    
    func didNextStep(action: PaymentStandardHeaderAction) {
        coordinator.perform(action: action)
    }
    
    func errorInstallment(type: InstallmentError) {
        let image = Assets.P2P.paymentInstallment.image
        switch type {
        case .emptyValueAndWrongPaymentMethod:
            presentInstallmentAlertEmptyValueAndWrongPaymentMethod(image: image)
        case .wrongPaymentMethod:
            presentInstallmentAlertWrongPaymentMethod(image: image)
        case .emptyValue:
            presentInstallmentAlertEmptyValue(image: image)
        }
    }
    
    private func createAttributedString(text: String) -> NSMutableAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        return NSMutableAttributedString(
            string: text,
            attributes: [
                .font: Typography.bodySecondary().font(),
                .foregroundColor: Colors.grayscale700.color,
                .paragraphStyle: paragraphStyle
            ]
        )
    }
    
    private func presentInstallmentAlertEmptyValueAndWrongPaymentMethod(image: UIImage?) {
        let title = PaymentLocalizable.alertInstallmentForbiddenValue.text
        let description = createAttributedString(text: PaymentLocalizable.alertInstallmentInsertValueAndWrongPaymentMethod.text)
        let firstButton: PaymentStandardHeaderViewController.ButtonAction = (
            title: PaymentLocalizable.changePaymentMethod.text,
            style: .fill,
            action: .changePaymentMethod
        )
        let secondButton: PaymentStandardHeaderViewController.ButtonAction = (
            title: DefaultLocalizable.notNow.text,
            style: .link,
            action: .notNow
        )
        viewController?.didReceiveAnInstallmentError(
            title: title,
            description: description,
            image: image,
            buttons: [firstButton, secondButton]
        )
    }
    
    private func presentInstallmentAlertWrongPaymentMethod(image: UIImage?) {
        let title = PaymentLocalizable.alertInstallmentAvailableForCredit.text
        let description = createAttributedString(text: PaymentLocalizable.alertInstallmentWrongPaymentMethod.text)
        let firstButton: PaymentStandardHeaderViewController.ButtonAction = (
            title: PaymentLocalizable.changePaymentMethod.text,
            style: .fill,
            action: .changePaymentMethod
        )
        let secondButton: PaymentStandardHeaderViewController.ButtonAction = (
            title: DefaultLocalizable.notNow.text,
            style: .link,
            action: .notNow
        )
        viewController?.didReceiveAnInstallmentError(
            title: title,
            description: description,
            image: image,
            buttons: [firstButton, secondButton]
        )
    }
    
    private func presentInstallmentAlertEmptyValue(image: UIImage?) {
        let title = PaymentLocalizable.alertInstallmentForbiddenValue.text
        let description = createAttributedString(text: PaymentLocalizable.alertInstallmentInsertValue.text)
        let firstButton: PaymentStandardHeaderViewController.ButtonAction = (
            title: DefaultLocalizable.btOkUnderstood.text,
            style: .fill,
            action: .okGotIt
        )
        viewController?.didReceiveAnInstallmentError(
            title: title,
            description: description,
            image: image,
            buttons: [firstButton]
        )
    }

    func presentInstallmentsDisabledAlert(withDescription description: String) {
        let buttons = [Button(title: DefaultLocalizable.btOkUnderstood.text, type: .cta)]
        let alert = Alert(
            with: PaymentLocalizable.alertInstallmentForbidden.text,
            text: description,
            buttons: buttons
        )
        viewController?.showAlert(alert)
    }
    
    func presentError(message: String) {
        let errorImage = Alert.Image(with: Assets.Icons.iconBad.image)
        let alert = Alert(with: PicPayError(message: message), image: errorImage)
        viewController?.showAlert(alert)
    }
    
    func displayInstallment(isEnable: Bool, installment: Int) {
        let text = formatInstallment(installment: installment)
        let color = isEnable ? Palette.ppColorBranding300.color : Palette.ppColorGrayscale300.color
        viewController?.displayInstallmentButton(title: text, image: nil, color: color, borderColor: color)
        viewController?.updateInstallment(installment: installment, forceCreditCard: false)
        viewController?.oldInstallmentButton(isHidden: model.newInstallmentEnable || !model.isInstallmentsEnabled)
        viewController?.newInstallmentButton(isHidden: !model.newInstallmentEnable || !model.isInstallmentsEnabled)
    }
    
    func didTapInfoButton() {
        guard
            let link = model.link,
            let url = URL(string: link)
            else {
                return
        }
        
        let isHttp = ["http", "https"].contains(url.scheme)
        let action: PaymentStandardHeaderAction = isHttp ?
            .openWebView(url: url) :
            .openUrl(url: url)
        
        didNextStep(action: action)
    }
    
    private func setupBillet(type: HeaderPayment.PayeeType) {
        viewController?.setupTitle(title: model.title, color: Palette.ppColorGrayscale600.color, ofSize: 14)
        setupImageHeader(image: model.image, placeholder: Assets.NewGeneration.avatarPerson.image, type: type)
        viewController?.description(isHidden: true)
    }
    
    private func setupP2P(type: HeaderPayment.PayeeType) {
        let username = formatUsername(username: model.title)
        
        viewController?.setupTitle(title: username, color: Palette.ppColorGrayscale600.color, ofSize: 11)
        viewController?.setupDescription(description: model.description, color: Palette.ppColorGrayscale400.color, ofSize: 12)
        setupImageHeader(image: model.image, placeholder: Assets.NewGeneration.avatarPerson.image, type: type)
        viewController?.description(isHidden: false)
    }
    
    private func setupPIX(type: HeaderPayment.PayeeType) {
        let username = model.title
        
        viewController?.setupTitle(title: username, color: Palette.ppColorGrayscale600.color, ofSize: 14)
        viewController?.setupDescription(description: model.description, color: Palette.ppColorGrayscale400.color, ofSize: 10)
        setupImageHeader(image: model.image, placeholder: Assets.NewGeneration.avatarPerson.image, type: type)
        viewController?.description(isHidden: false)
    }
    
    private func setupPAV(type: HeaderPayment.PayeeType) {
        let sellerName = model.title
        
        viewController?.setupTitle(title: sellerName, color: Palette.ppColorGrayscale600.color, ofSize: 11)
        setupImageHeader(image: model.image, placeholder: Assets.NewGeneration.avatarPlace.image, type: type)
        viewController?.description(isHidden: true)
    }
    
    private func setupDigitalGoods(type: HeaderPayment.PayeeType) {
        viewController?.setupTitle(title: model.title, color: Palette.ppColorGrayscale600.color, ofSize: 14)
        setupImageHeader(image: model.image, placeholder: Assets.NewGeneration.avatarPlace.image, type: type)
        viewController?.description(isHidden: true)
    }
    
    private func setupP2PLending(type: HeaderPayment.PayeeType) {
        let username = formatUsername(username: model.title)
        
        viewController?.setupTitle(title: username, color: Colors.black.color, ofSize: 14)
        viewController?.setupDescription(description: model.description, color: Colors.black.color, ofSize: 14)
        setupImageHeader(image: model.image, placeholder: Assets.NewGeneration.avatarPerson.image, type: type)
        viewController?.description(isHidden: false)
    }
    
    private func setupInfoButton() {
        guard let link = model.link, !link.isEmpty else {
            return
        }
        
        viewController?.displayInfoButton()
    }
    
    private func formatInstallment(installment: Int) -> String {
        guard model.newInstallmentEnable else {
            return "\(installment)x"
        }
        return " \(PaymentLocalizable.installmentIn.text) \(installment)x"
    }
    
    private func formatUsername(username: String) -> String {
        "@\(username)"
    }
    
    private func setupImageHeader(image: HeaderStandard.ImageHeader, placeholder: UIImage, type: HeaderPayment.PayeeType) {
        switch image {
        case .url(let url):
            viewController?.setupImage(
                imageUrl: URL(string: url ?? ""),
                placeholder: placeholder,
                type: type
            )
        case .image(let image):
            viewController?.setupImage(imageUrl: nil, placeholder: image, type: type)
        }
    }
}
