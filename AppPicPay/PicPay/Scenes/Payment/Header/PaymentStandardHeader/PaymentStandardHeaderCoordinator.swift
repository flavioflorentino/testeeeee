import AnalyticsModule
import CorePayment
import FeatureFlag
import UIKit

enum PaymentStandardHeaderAction {
    case openWebView(url: URL)
    case openUrl(url: URL)
    case openP2PProfile(payeeId: String)
    case openInstallmentOptions(PaymentInstallment, (Int) -> Void)
    case openInstallmentBusinessOptions(model: PaymentBusinessInstallment, completion: (Int) -> Void)
}

protocol PaymentStandardHeaderCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: PaymentStandardHeaderAction)
}

final class PaymentStandardHeaderCoordinator: PaymentStandardHeaderCoordinating {
    typealias Dependencies = HasFeatureManager & HasAnalytics
    private let dependencies: Dependencies = DependencyContainer()
    private var isInvertedInstallmentActive: Bool {
        dependencies.featureManager.isActive(.experimentInvertedInstallment)
    }
    
    private var completionInstallment: ((Int) -> Void)?
    weak var viewController: UIViewController?
    
    func perform(action: PaymentStandardHeaderAction) {
        switch action {
        case .openWebView(let url):
            openWebView(url: url)
        case .openP2PProfile(let payeeId):
            openP2PProfile(payeeId: payeeId)
            
        case let .openInstallmentOptions(model, completion):
            openInstallmentOptions(model: model, completion: completion)
            
        case let .openInstallmentBusinessOptions(model, completion):
            openInstallmentBusinessOptions(model: model, completion: completion)
    
        case let .openUrl(url):
            openUrl(url: url)
        }
    }
    
    private func openWebView(url: URL) {
        guard let viewController = viewController else {
            return
        }
        
        ViewsManager.presentSafariViewController(url, from: viewController)
    }
    
    private func openUrl(url: URL) {
        UIApplication.shared.open(url)
    }
    
    private func openP2PProfile(payeeId: String) {
        guard let viewController = viewController else {
            return
        }
        
        let controller = ProfileFactory.make(profileId: payeeId)
        viewController.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func openInstallmentBusinessOptions(model: PaymentBusinessInstallment, completion: @escaping (Int) -> Void) {
        guard let paymentManager = createPaymentManager(model.value, model.installment) else {
            return
        }
        
        completionInstallment = completion
        let viewModel = InstallmentsSelectorViewModel(
            paymentManager: paymentManager,
            sellerId: model.sellerId
        )
        
        openInstallment(viewModel)
    }
    
    private func openInstallmentOptions(model: PaymentInstallment, completion: @escaping (Int) -> Void) {
        guard let paymentManager = createPaymentManager(model.value, model.installment) else {
            return
        }
        
        completionInstallment = completion
        let viewModel = InstallmentsSelectorViewModel(
            paymentManager: paymentManager,
            payeeId: NSDecimalNumber(string: model.payeeId)
        )
        
        openInstallment(viewModel)
    }
    
    private func createPaymentManager(_ value: Double, _ installment: Int) -> PPPaymentManager? {
        let paymentManager = PPPaymentManager()
        paymentManager?.selectedQuotaQuantity = installment
        paymentManager?.subtotal = NSDecimalNumber(value: value)
        
        return paymentManager
    }
    
    private func openInstallment(_ viewModel: InstallmentsSelectorViewModel) {
        viewModel.delegate = self
        let controller = InstallmentsSelectorViewController(with: viewModel)
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}

extension PaymentStandardHeaderCoordinator: InstallmentsSelectorViewModelDelegate {
    func didSelectedInstallment(_ paymentManager: PPPaymentManager) {
        let installment = paymentManager.selectedQuotaQuantity
        completionInstallment?(installment)
        
        guard let value = paymentManager.subtotal as? Double else {
            return
        }
        dependencies.analytics.log(InstallmentButtonEvent.installmentSelected(
                                    installment.toString() ?? "",
                                    value.toString() ?? "",
                                    isInvertedInstallmentActive))
    }
}
