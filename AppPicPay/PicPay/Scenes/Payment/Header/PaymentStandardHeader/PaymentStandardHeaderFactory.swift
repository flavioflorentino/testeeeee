import Foundation

enum PaymentStandardHeaderFactory {
    static func make(
        modelPresenter: HeaderStandard,
        model: HeaderPayment,
        paymentManager: PPPaymentManager = PPPaymentManager()
    ) -> PaymentStandardHeaderViewController {
        let container = DependencyContainer()
        let service: PaymentStandardHeaderServicing = PaymentStandardHeaderService(paymentManager: paymentManager)
        let coordinator: PaymentStandardHeaderCoordinating = PaymentStandardHeaderCoordinator()
        let presenter: PaymentStandardHeaderPresenting = PaymentStandardHeaderPresenter(model: modelPresenter, coordinator: coordinator)
        let viewModel = PaymentStandardHeaderViewModel(model: model, service: service, presenter: presenter, dependencies: container)
        let viewController = PaymentStandardHeaderViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
