import AnalyticsModule
import CorePayment
import Foundation

protocol PaymentStandardHeaderViewModelInputs: AnyObject {    
    func viewDidLoad()
    func updateValues()
    func didTapImage()
    func didTapInfoButton()
    func changeValue(value: Double)
    func didTapInstallment()
    func didTapInstallmentAlert(action: PaymentStandardHeaderViewController.AlertAction)
}

final class PaymentStandardHeaderViewModel {
    typealias Dependencies = HasAnalytics & HasConsumerManager
    private let service: PaymentStandardHeaderServicing
    private let presenter: PaymentStandardHeaderPresenting
    
    private var model: HeaderPayment
    private var value: Double
    private var installments: Int
    private let dependencies: Dependencies
    
    init(model: HeaderPayment, service: PaymentStandardHeaderServicing, presenter: PaymentStandardHeaderPresenting, dependencies: Dependencies) {
        self.model = model
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies

        value = model.value
        installments = model.installments
    }
}

extension PaymentStandardHeaderViewModel: PaymentStandardHeaderViewModelInputs {
    func viewDidLoad() {
        presenter.viewDidLoad(value: value, payeeType: model.payeeType, paymentType: model.paymentType)
        updateInstallmente()
    }
    
    func updateValues() {
        updateInstallmente()
        updateValue()
    }
    
    func didTapImage() {
        guard model.paymentType == .p2p || (model.paymentType == .pix && model.payeeId != String()) else {
            return
        }
        
        presenter.didNextStep(action: .openP2PProfile(payeeId: model.payeeId))
    }
    
    func changeValue(value: Double) {
        self.value = value
        updateInstallmente()
        updateValue()
    }
    
    func didTapInstallment() {
        guard installmentRulesAreValid() else {
            processInstallmentRules()
            return
        }
        
        openInstallmentOptions()
    }
    
    func didTapInfoButton() {
        presenter.didTapInfoButton()
    }
    
    private func updateInstallmente() {
        let cartValue = service.cardValue(total: value, installment: installments)
        installments = smallestInstallmentAvailable()
        presenter.displayInstallment(isEnable: !cartValue.isZero, installment: installments)
    }
    
    private func smallestInstallmentAvailable() -> Int {
        let availableInstallments = service.numberInstallmentAvailable(total: value, installment: installments)
        guard installments > availableInstallments else {
            return installments
        }
        
        return availableInstallments > 0 ? availableInstallments : 1
    }
    
    private func updateValue() {
        let cartValue = service.cardValue(total: value, installment: installments)
        presenter.updateInput(value: value)
        presenter.updateCardValue(cardValue: cartValue)
    }
    
    private func openInstallmentOptions() {
        if model.paymentType == .pav {
            let installment = PaymentBusinessInstallment(sellerId: model.sellerId ?? "", value: value, installment: installments)
            
            presenter.didNextStep(action: .openInstallmentBusinessOptions(model: installment, completion: { [weak self] installments in
                self?.updateWithResultInstallments(installments)
            }))
        } else {
            let installment = PaymentInstallment(payeeId: model.payeeId, value: value, installment: installments)
            
            presenter.didNextStep(action: .openInstallmentOptions(installment, { [weak self] installments in
                self?.updateWithResultInstallments(installments)
            }))
        }
    }
    
    private func updateWithResultInstallments(_ installments: Int) {
        self.installments = installments
        updateInstallmente()
        updateValue()
    }
    
    private func installmentRulesAreValid() -> Bool {
        guard generalInstallmentRulesAreValid() else {
            return false
        }
        
        return installmentRulesTypeAreValid()
    }
    
    private func installmentRulesTypeAreValid() -> Bool {
        guard model.paymentType != .pix else {
            return false
        }

        guard case .p2p = model.paymentType else {
            return true
        }
        
        return p2pInstallmentRulesAreValid()
    }
    
    private func p2pInstallmentRulesAreValid() -> Bool {
        service.installmentActivated(type: .p2p) ? true : model.payeeType == .pro
    }
    
    private func generalInstallmentRulesAreValid() -> Bool {
        let cartValue = service.cardValue(total: value, installment: installments)
        
        return !value.isZero && !cartValue.isZero
    }
    
    private func processInstallmentRules() {
        guard model.paymentType != .pix else {
            presenter.presentInstallmentsDisabledAlert(withDescription: PaymentLocalizable.alertInstallmentForbiddenByPIX.text)
            return
        }

        let cardValue = service.cardValue(total: value, installment: installments)
        
        if !p2pInstallmentRulesAreValid() {
            presenter.presentError(message: PaymentLocalizable.errorInstallmentP2P.text)
        } else if service.usePicPayBalance() && cardValue.isZero {
            if value.isZero {
                presenter.errorInstallment(type: .emptyValueAndWrongPaymentMethod)
                createUserNavigatedEvent(from: .buttonInstallment, to: .modalInstallmentBalance)
            } else {
                presenter.errorInstallment(type: .wrongPaymentMethod)
                createUserNavigatedEvent(from: .buttonInstallment, to: .modalFilledBalance)
            }
        } else if cardValue.isZero {
            presenter.errorInstallment(type: .emptyValue)
            createUserNavigatedEvent(from: .buttonInstallment, to: .modalInstallmentCreditcard)
        } else {
            presenter.presentError(message: DefaultLocalizable.unexpectedError.text)
        }
    }
    
    private func createUserNavigatedEvent(from: PaymentEvent.ViewLocation, to: PaymentEvent.ViewLocation) {
        guard let consumerId = dependencies.consumerManager.consumer?.wsId else {
            return
        }
        let consumerIdString = String(consumerId)
        let timestamp = DateFormatter.brazillianFormatter(dateFormat: "yyyy-MM-dd'T'HH:mm:ssZZZZZ").string(from: Date())
        dependencies.analytics.log(
            PaymentEvent.userNavigated(
                from: from,
                to: to,
                consumerId: consumerIdString,
                timestamp: timestamp
            )
        )
    }
    
    func didTapInstallmentAlert(action: PaymentStandardHeaderViewController.AlertAction) {
        switch action {
        case .changePaymentMethod:
            createUserNavigatedEvent(from: .buttonChangePaymentMethod, to: .screenPaymentMethod)
        case .okGotIt:
            createUserNavigatedEvent(from: .buttonOk, to: .screenTransaction)
        case .notNow:
            createUserNavigatedEvent(from: .buttonNotNow, to: .screenTransaction)
        }
    }
}
