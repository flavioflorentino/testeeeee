import Foundation

enum PaymentProfileHeaderFactory {
    static func make(
        modelPresenter: HeaderProfile,
        model: HeaderPayment,
        analytics: PaymentProfileHeaderAnalytics? = nil,
        followingStatus: FollowerStatus? = nil
    ) -> PaymentProfileHeaderViewController {
        let coordinator: PaymentProfileHeaderCoordinating = PaymentProfileHeaderCoordinator()
        let presenter: PaymentProfileHeaderPresenting = PaymentProfileHeaderPresenter(model: modelPresenter, coordinator: coordinator)
        let service: PaymentProfileHeaderServicing = PaymentProfileHeaderService(dependencies: DependencyContainer())
        let viewModel = PaymentProfileHeaderViewModel(
            model: model,
            followingStatus: followingStatus,
            presenter: presenter,
            service: service,
            analytics: analytics
        )
        let viewController = PaymentProfileHeaderViewController(viewModel: viewModel)
        presenter.viewController = viewController
        coordinator.viewController = viewController

        return viewController
    }
}
