import CorePayment
import UI
import UIKit

extension PaymentProfileHeaderViewController.Layout {
    enum Size {
        static let sizeImage: CGFloat = 100.0
        static let followButton = CGSize(width: 128, height: Spacing.base04)
    }
    
    enum Font {
        static let description = UIFont.systemFont(ofSize: 14)
        static let profile = UIFont.systemFont(ofSize: 17, weight: .semibold)
    }
}

final class PaymentProfileHeaderViewController: ViewController<PaymentProfileHeaderViewModelInputs, UIView>, PaymentHeaderInput {
    fileprivate enum Layout {}
    
    private lazy var profileImage: ProfileImageView = {
        let profileImage = ProfileImageView()
        profileImage.sizeImage = Layout.Size.sizeImage
        
        return profileImage
    }()
    
    private lazy var profileImageView: ProfileImageView = {
        let profileImage = ProfileImageView()
        profileImage.sizeImage = Layout.Size.sizeImage
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(profileImageTap))
        profileImage.addGestureRecognizer(tapGesture)
        profileImage.isUserInteractionEnabled = true
        
        return profileImage
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.description
        label.textColor = Palette.ppColorGrayscale500.color
        label.textAlignment = .center
        
        return label
    }()
    
    private lazy var profileNameLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.profile
        label.textColor = Palette.ppColorGrayscale500.color
        label.textAlignment = .center
        
        return label
    }()
    
    private lazy var followButton: UIPPFollowButton = {
        let button = UIPPFollowButton(title: PromoCodePopUpLocalizable.followTitleButton.text, target: self, action: #selector(followButtonTap))
        button.cornerRadius = Layout.Size.followButton.height / 2
        button.setImage(Assets.Icons.icoNavFriends.image, for: .normal)
        button.tintImage = true
        button.setupTintImage()
        button.borderWidth = 1
        button.centerLoading = true
        
        button.normalTitleColor = Palette.ppColorBranding400.color
        button.normalBackgrounColor = .clear
        button.borderColor = Palette.ppColorPositive400.color
        
        button.isHidden = true
        return button
    }()
    
    weak var delegate: PaymentHeaderOutput?
    private var followButtonHeightConstraint: NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.setupPayeeType()
        viewModel.updateFollowStatus()
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override func buildViewHierarchy() {
        view.addSubview(profileImageView)
        view.addSubview(descriptionLabel)
        view.addSubview(profileNameLabel)
        view.addSubview(followButton)
    }
    
    override func setupConstraints() {
        profileImageView.layout {
            $0.top >= view.topAnchor
            $0.centerX == view.centerXAnchor
        }
        
        descriptionLabel.layout {
            $0.top == profileImageView.bottomAnchor + Spacing.base00
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
        }
        
        profileNameLabel.layout {
            $0.top == descriptionLabel.bottomAnchor + Spacing.base01
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
        }
        
        followButton.layout {
            $0.top == profileNameLabel.bottomAnchor + Spacing.base01
            $0.centerX == view.centerXAnchor
            followButtonHeightConstraint = $0.height == .zero
            $0.width == Layout.Size.followButton.width
            $0.bottom <= view.bottomAnchor
        }
    }
}

@objc
private extension PaymentProfileHeaderViewController {
    func followButtonTap(_ sender: UIPPFollowButton) {
        viewModel.checkIfCanFollow(sender.action)
    }
    
    func profileImageTap() {
        viewModel.openProfile()
    }
}

// MARK: View Model Outputs
extension PaymentProfileHeaderViewController: PaymentProfileHeaderDisplay {
    func displayName(_ name: String) {
        profileNameLabel.text = name
    }
    
    func displayImage(image: URL?, placeholder: UIImage, type: HeaderPayment.PayeeType) {
        profileImageView.setImage(image: image, placeholder: placeholder)
        profileImageView.profileType = type
    }
    
    func displayDescription(_ text: String?) {
        descriptionLabel.text = text
    }
    
    func displayFollowButton(withStatus status: FollowerStatus) {
        followButtonHeightConstraint?.constant = Layout.Size.followButton.height
        followButton.isHidden = false
        followButton.changeButtonWithStatus(.undefined, consumerStatus: status)
    }
    
    func displayFollowConfirmationPopup(_ alert: Alert, action: FollowerButtonAction) {
        AlertMessage.showAlert(alert, controller: self) { popupController, button, _ in
            popupController.dismiss(animated: true) {
                guard button.action == .confirm else {
                    return
                }
                self.viewModel.followAction(action)
            }
        }
    }
    
    func displayError(_ error: PicPayError) {
        AlertMessage.showAlert(error, controller: self)
    }
}
