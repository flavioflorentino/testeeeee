enum PaymentProfileHeaderAction {
    case openProfile(profileId: String)
}

protocol PaymentProfileHeaderCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: PaymentProfileHeaderAction)
}

final class PaymentProfileHeaderCoordinator: PaymentProfileHeaderCoordinating {
    var viewController: UIViewController?
    
    func perform(action: PaymentProfileHeaderAction) {
        guard case let .openProfile(profileId) = action else {
            return
        }
        let controller = ProfileFactory.make(profileId: profileId)
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}
