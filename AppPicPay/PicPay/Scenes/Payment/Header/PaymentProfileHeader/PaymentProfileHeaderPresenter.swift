import Core
import Foundation

protocol PaymentProfileHeaderPresenting: AnyObject {
    var viewController: PaymentProfileHeaderDisplay? { get set }
    func updateView(payeeType: HeaderPayment.PayeeType)
    func updateFollowButton(with status: FollowerStatus)
    func presentFollowConfirmationPopup(for action: FollowerButtonAction)
    func presentError(_ error: PicPayError)
    func didNextStep(action: PaymentProfileHeaderAction)
}

final class PaymentProfileHeaderPresenter: PaymentProfileHeaderPresenting {
    private let model: HeaderProfile
    private let coordinator: PaymentProfileHeaderCoordinating
    weak var viewController: PaymentProfileHeaderDisplay?
    
    init(model: HeaderProfile, coordinator: PaymentProfileHeaderCoordinating) {
        self.model = model
        self.coordinator = coordinator
    }
    
    func updateView(payeeType: HeaderPayment.PayeeType) {
        updateName()
        updateImage(payeeType: payeeType)
        updateDescription()
    }
    
    private func updateName() {
        let usermane = "@\(model.username)"
        viewController?.displayName(usermane)
    }
    
    private func updateImage(payeeType: HeaderPayment.PayeeType) {
        var url: URL?
        if let image = model.image {
            url = URL(string: image)
        }
        
        viewController?.displayImage(image: url, placeholder: Assets.NewGeneration.avatarPlaceholder.image, type: payeeType)
    }
    
    private func updateDescription() {
        viewController?.displayDescription(model.description)
    }
    
    func presentFollowConfirmationPopup(for action: FollowerButtonAction) {
        let titleFormat: PaymentLocalizable = action == .unfollow
            ? .unfollowConfirmationTitle
            : .cancelRequestConfirmationTitle
        let title = String(format: titleFormat.text, model.username)
        
        let alert = Alert(title: title, text: nil)
        let confirmButton = Button(title: DefaultLocalizable.btYes.text, type: .cta, action: .confirm)
        let cancelButton = Button(title: DefaultLocalizable.btNo.text, type: .clean, action: .close)
        alert.buttons = [confirmButton, cancelButton]
        
        viewController?.displayFollowConfirmationPopup(alert, action: action)
    }
    
    func updateFollowButton(with status: FollowerStatus) {
        guard model.showFollowButton else {
            return
        }
        viewController?.displayFollowButton(withStatus: status)
    }
    
    func presentError(_ error: PicPayError) {
        viewController?.displayError(error)
    }
    
    func didNextStep(action: PaymentProfileHeaderAction) {
        coordinator.perform(action: action)
    }
}
