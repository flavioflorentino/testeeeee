import Core
import Foundation

protocol PaymentProfileHeaderServicing: AnyObject {
    func followFriend(action: FollowerButtonAction, followerId: String, completion: @escaping (Result<FollowerStatus, PicPayError>) -> Void)
}

final class PaymentProfileHeaderService: PaymentProfileHeaderServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func followFriend(action: FollowerButtonAction, followerId: String, completion: @escaping (Result<FollowerStatus, PicPayError>) -> Void) {
        WSSocial.followAction(action, follower: followerId) { [weak self] isSuccess, consumerStatus, _, error in
            self?.dependencies.mainQueue.async {
                if let error = error, !isSuccess {
                    let picPayError = PicPayError(message: error.localizedDescription)
                    completion(.failure(picPayError))
                    return
                }
                completion(.success(consumerStatus))
            }
        }
    }
}
