import Foundation

protocol PaymentProfileHeaderViewModelInputs: AnyObject {
    func setupPayeeType()
    func updateFollowStatus()
    func checkIfCanFollow(_ action: FollowerButtonAction)
    func followAction(_ action: FollowerButtonAction)
    func openProfile()
}

final class PaymentProfileHeaderViewModel {
    private let presenter: PaymentProfileHeaderPresenting
    private let service: PaymentProfileHeaderServicing
    private let model: HeaderPayment
    private var followingStatus: FollowerStatus?
    private let analytics: PaymentProfileHeaderAnalytics?
    
    init(
        model: HeaderPayment,
        followingStatus: FollowerStatus?,
        presenter: PaymentProfileHeaderPresenting,
        service: PaymentProfileHeaderServicing,
        analytics: PaymentProfileHeaderAnalytics?
    ) {
        self.presenter = presenter
        self.service = service
        self.model = model
        self.followingStatus = followingStatus
        self.analytics = analytics
    }
}

extension PaymentProfileHeaderViewModel: PaymentProfileHeaderViewModelInputs {
    func setupPayeeType() {
        presenter.updateView(payeeType: model.payeeType)
    }
    
    func updateFollowStatus() {
        guard let status = followingStatus else {
            return
        }
        presenter.updateFollowButton(with: status)
    }
    
    func checkIfCanFollow(_ action: FollowerButtonAction) {
        if action == .unfollow || action == .cancelRequest {
            presenter.presentFollowConfirmationPopup(for: action)
        } else {
            followAction(action)
        }
    }
    
    func followAction(_ action: FollowerButtonAction) {
        analytics?.willFollow()
        presenter.updateFollowButton(with: .loading)
        service.followFriend(action: action, followerId: model.payeeId) { [weak self] result in
            switch result {
            case .success(let followingStatus):
                self?.followingStatus = followingStatus
            case .failure(let error):
                self?.presenter.presentError(error)
            }
            self?.updateFollowStatus()
        }
    }
    
    func openProfile() {
        analytics?.willOpenProfile()
        presenter.didNextStep(action: .openProfile(profileId: model.payeeId))
    }
}
