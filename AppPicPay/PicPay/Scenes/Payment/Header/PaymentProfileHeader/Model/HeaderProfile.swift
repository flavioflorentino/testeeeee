import Foundation

struct HeaderProfile {
    let username: String
    let image: String?
    let description: String?
    let showFollowButton: Bool
}
