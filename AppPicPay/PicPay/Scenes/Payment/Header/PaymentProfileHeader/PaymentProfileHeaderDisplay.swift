import UIKit

protocol PaymentProfileHeaderDisplay: AnyObject {
    func displayName(_ name: String)
    func displayImage(image: URL?, placeholder: UIImage, type: HeaderPayment.PayeeType)
    func displayDescription(_ text: String?)
    func displayFollowButton(withStatus status: FollowerStatus)
    func displayFollowConfirmationPopup(_ alert: Alert, action: FollowerButtonAction)
    func displayError(_ error: PicPayError)
}
