import Core
import Foundation
import UI

protocol PaymentQrCodeHeaderPresenting: AnyObject {
    var viewController: PaymentQrCodeHeaderDisplay? { get set }
    func viewDidLoad(value: Double, payeeType: HeaderPayment.PayeeType, paymentType: HeaderPayment.PaymentType)
    func displayInstallment(isEnable: Bool, installment: Int)
    func updateInstallment(installment: Int, forceCreditCard: Bool)
    func updateInput(value: Double)
    func didTapFinish()
    func didNextStep(action: PaymentQrCodeHeaderAction)
}

final class PaymentQrCodeHeaderPresenter: PaymentQrCodeHeaderPresenting {
    private let coordinator: PaymentQrCodeHeaderCoordinating
    private let model: HeaderQrCode
    weak var viewController: PaymentQrCodeHeaderDisplay?

    init(model: HeaderQrCode, coordinator: PaymentQrCodeHeaderCoordinating) {
        self.model = model
        self.coordinator = coordinator
    }
    
    func viewDidLoad(value: Double, payeeType: HeaderPayment.PayeeType, paymentType: HeaderPayment.PaymentType) {
        updateInput(value: value)
        displayNavigation()
        setupHeader(payeeType: payeeType, paymentType: paymentType)
        setupBackground()
        setupOrder()
    }
    
    func updateInput(value: Double) {
        viewController?.updateValue(value: value)
        viewController?.setupInput(value: value)
    }
    
    func displayInstallment(isEnable: Bool, installment: Int) {
        let text = formatInstallment(installment: installment)
        let color = isEnable ? Palette.ppColorBranding300.color : Palette.ppColorGrayscale300.color
        viewController?.displayInstallmentButton(title: text, color: color, borderColor: color)
    }
    
    func updateInstallment(installment: Int, forceCreditCard: Bool) {
        viewController?.updateInstallment(installment: installment, forceCreditCard: forceCreditCard)
    }
    
    func didTapFinish() {
        model.isModal ? didNextStep(action: .finishModal) : didNextStep(action: .finishPush)
    }
    
    func didNextStep(action: PaymentQrCodeHeaderAction) {
        coordinator.perform(action: action)
    }
    
    private func setupOrder() {
        guard let order = model.orderNumber else {
            viewController?.diplayOrder(model.orderNumber, isDisabled: true)
            return
        }
        
        let format = formatOrder(order)
        viewController?.diplayOrder(format, isDisabled: false)
    }
    
    private func displayNavigation() {
        model.isModal ? displayNavigationAsModal() : displayNavigationAsPush()
    }
    
    private func setupHeader(payeeType: HeaderPayment.PayeeType, paymentType: HeaderPayment.PaymentType) {
        let sellerImage = model.profileImage ?? ""
        let sellerName = model.profileName
        let placeholder = paymentType == .p2m ? Assets.Ilustration.iluP2mPlaceholder.image : Assets.NewGeneration.avatarPlace.image
            
        viewController?.diplayProfileName(sellerName)
        viewController?.diplayProfileImage(
            URL(string: sellerImage),
            placeholder: placeholder,
            type: payeeType
        )
    }
    
    private func setupBackground() {
        guard let image = model.backgroundImage else {
            viewController?.diplayBackgroundImage(nil)
            return
        }
        let url = URL(string: image)
        viewController?.diplayBackgroundImage(url)
    }
    
    private func displayNavigationAsModal() {
        viewController?.diplayFinishButton(title: DefaultLocalizable.btCancel.text, image: nil)
    }
    
    private func displayNavigationAsPush() {
        viewController?.diplayFinishButton(title: String(), image: Assets.back.image)
    }
    
    private func formatOrder(_ order: String) -> String {
        "\(PaymentLocalizable.order.text) \(order)"
    }
    
    private func formatInstallment(installment: Int) -> String {
        "\(PaymentLocalizable.installment.text) \(installment)x"
    }
}
