import CorePayment
import Foundation

protocol PaymentQrCodeHeaderViewModelInputs: AnyObject {
    func viewDidLoad()
    func viewWillAppear()
    func didTapFinish()
    func changeValue(value: Double)
    func didTapInstallment()
}

final class PaymentQrCodeHeaderViewModel {
    private let service: PaymentQrCodeHeaderServicing
    private let presenter: PaymentQrCodeHeaderPresenting
    
    private var model: HeaderPayment
    private var installments: Int
    private var value: Double
    
    init(model: HeaderPayment, service: PaymentQrCodeHeaderServicing, presenter: PaymentQrCodeHeaderPresenting) {
        self.model = model
        self.service = service
        self.presenter = presenter
        
        value = model.value
        installments = model.installments
    }
}

extension PaymentQrCodeHeaderViewModel: PaymentQrCodeHeaderViewModelInputs {
    func viewDidLoad() {
        presenter.viewDidLoad(value: value, payeeType: model.payeeType, paymentType: model.paymentType)
        updateInstallment()
    }
    
    func viewWillAppear() {
        updateInstallment()
        updateValue()
    }
    
    func changeValue(value: Double) {
        self.value = value
        updateInstallment()
        updateValue()
    }
    
    func didTapInstallment() {
        guard model.paymentType != .p2m, installmentIsAvailable() else {
            return
        }
        
        openInstallmentOptions()
    }
    
    func didTapFinish() {
        presenter.didTapFinish()
    }
    
    private func openInstallmentOptions() {
        let installment = PaymentInstallment(payeeId: model.payeeId, value: value, installment: installments)
        
        presenter.didNextStep(action: .openInstallmentOptions(installment, { [weak self] installments in
            self?.installments = installments
            self?.updateInstallment()
            self?.updateValue()
       }))
    }
    
    private func installmentIsAvailable() -> Bool {
        let cartValue = service.cardValue(total: value, installment: installments)
        return !cartValue.isZero
    }
    
    private func updateInstallment() {
        let installmentAvailable = installmentIsAvailable()
        let forceCreditCard = forceCreditCardPayment()
        
        presenter.updateInstallment(installment: installments, forceCreditCard: forceCreditCard)
        presenter.displayInstallment(isEnable: installmentAvailable, installment: installments)
    }
    
    private func forceCreditCardPayment() -> Bool {
        model.paymentType == .p2m && isInstallmentPayment() && service.paymentMethodIsBalance()
    }
    
    private func isInstallmentPayment() -> Bool {
        installments > 1
    }
    
    private func updateValue() {
        presenter.updateInput(value: value)
    }
}
