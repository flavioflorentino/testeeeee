import Foundation

enum PaymentQrCodeHeaderFactory {
    static func make(modelPresenter: HeaderQrCode, model: HeaderPayment) -> PaymentQrCodeHeaderViewController {
        let service: PaymentQrCodeHeaderServicing = PaymentQrCodeHeaderService(paymentManager: PPPaymentManager())
        let coordinator: PaymentQrCodeHeaderCoordinating = PaymentQrCodeHeaderCoordinator()
        let presenter: PaymentQrCodeHeaderPresenting = PaymentQrCodeHeaderPresenter(model: modelPresenter, coordinator: coordinator)
        let viewModel = PaymentQrCodeHeaderViewModel(model: model, service: service, presenter: presenter)
        let viewController = PaymentQrCodeHeaderViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
