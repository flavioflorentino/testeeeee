import UIKit

protocol PaymentQrCodeHeaderDisplay: AnyObject {
    func setupInput(value: Double)
    func updateValue(value: Double)
    func diplayProfileName(_ profileName: String)
    func diplayBackgroundImage(_ image: URL?)
    func diplayProfileImage(_ image: URL?, placeholder: UIImage, type: HeaderPayment.PayeeType)
    func diplayOrder(_ order: String?, isDisabled: Bool)
    func diplayFinishButton(title: String, image: UIImage?)
    func displayInstallmentButton(title: String?, color: UIColor, borderColor: UIColor)
    func updateInstallment(installment: Int, forceCreditCard: Bool)
}
