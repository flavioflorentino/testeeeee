import CorePayment
import UIKit

enum PaymentQrCodeHeaderAction {
    case openInstallmentOptions(PaymentInstallment, (Int) -> Void)
    case finishModal
    case finishPush
}

protocol PaymentQrCodeHeaderCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: PaymentQrCodeHeaderAction)
}

final class PaymentQrCodeHeaderCoordinator: PaymentQrCodeHeaderCoordinating {
    private var completionInstallment: ((Int) -> Void)?
    weak var viewController: UIViewController?
    
    func perform(action: PaymentQrCodeHeaderAction) {
        switch action {
        case .finishModal:
            viewController?.dismiss(animated: true)
            
        case .finishPush:
            viewController?.navigationController?.popViewController(animated: true)
            
        case let .openInstallmentOptions(model, completion):
            openInstallmentOptions(model: model, completion: completion)
        }
    }
    
    private func openInstallmentOptions(model: PaymentInstallment, completion: @escaping (Int) -> Void) {
        guard let paymentManager = PPPaymentManager() else {
            return
        }
        
        completionInstallment = completion
        paymentManager.selectedQuotaQuantity = model.installment
        paymentManager.subtotal = NSDecimalNumber(value: model.value)
        
        let viewModel = InstallmentsSelectorViewModel(
            paymentManager: paymentManager,
            payeeId: NSDecimalNumber(string: model.payeeId)
        )
        viewModel.delegate = self
        let controller = InstallmentsSelectorViewController(with: viewModel)
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}

extension PaymentQrCodeHeaderCoordinator: InstallmentsSelectorViewModelDelegate {
    func didSelectedInstallment(_ paymentManager: PPPaymentManager) {
        let installment = paymentManager.selectedQuotaQuantity
        completionInstallment?(installment)
    }
}
