import Foundation

protocol PaymentQrCodeHeaderServicing {
    func numberInstallmentAvailable(total: Double, installment: Int) -> Int
    func cardValue(total: Double, installment: Int) -> Double
    func paymentMethodIsBalance() -> Bool
}

final class PaymentQrCodeHeaderService: PaymentQrCodeHeaderServicing {
    private let paymentManager: PPPaymentManager
    
    init(paymentManager: PPPaymentManager) {
        self.paymentManager = paymentManager
    }
    
    func cardValue(total: Double, installment: Int) -> Double {
        paymentManager.selectedQuotaQuantity = installment
        paymentManager.subtotal = NSDecimalNumber(value: total)
        
        return paymentManager.cardTotal()?.doubleValue ?? 0
    }
    
    func numberInstallmentAvailable(total: Double, installment: Int) -> Int {
        paymentManager.selectedQuotaQuantity = installment
        paymentManager.subtotal = NSDecimalNumber(value: total)
        
        return paymentManager.installmentsList()?.count ?? 0
    }
    
    func paymentMethodIsBalance() -> Bool {
        paymentManager.usePicPayBalance()
    }
}
