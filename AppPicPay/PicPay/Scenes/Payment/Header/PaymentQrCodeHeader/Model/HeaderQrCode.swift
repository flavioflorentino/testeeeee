import Foundation

struct HeaderQrCode {
    let profileName: String
    let backgroundImage: String?
    let profileImage: String?
    let orderNumber: String?
    let isModal: Bool
}
