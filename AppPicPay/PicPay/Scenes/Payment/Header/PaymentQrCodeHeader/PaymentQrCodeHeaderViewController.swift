import CorePayment
import UI
import UIKit

private extension PaymentQrCodeHeaderViewController.Layout {
    static let sizeBackground: CGFloat = 180
    static let sizeProfile: CGFloat = 100
    static let margin: CGFloat = 16
    static let topInputValue: CGFloat = 12
    static let installmentHeight: CGFloat = 40
    static let installmentWidth: CGFloat = 140
    static let borderWidth: CGFloat = 1
}

final class PaymentQrCodeHeaderViewController: ViewController<PaymentQrCodeHeaderViewModelInputs, UIView>, PaymentHeaderInput {
    fileprivate enum Layout {}
    
    private lazy var backgroundImage: UIImageView = {
        let image = UIImageView()
        image.backgroundColor = Palette.ppColorGrayscale400.color
        image.contentMode = .scaleAspectFill
        image.isUserInteractionEnabled = true
        image.translatesAutoresizingMaskIntoConstraints = false
        
        return image
    }()
    
    private lazy var profileImage: ProfileImageView = {
        let image = ProfileImageView()
        image.sizeImage = Layout.sizeProfile
        image.translatesAutoresizingMaskIntoConstraints = false
        
        return image
    }()
    
    private lazy var profileNameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var valuePaymentLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.text = PaymentLocalizable.valuePayment.text
        label.textColor = Palette.ppColorGrayscale400.color
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var orderPaymentLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.text = PaymentLocalizable.valuePayment.text
        label.textColor = Palette.ppColorGrayscale400.color
        label.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var inputValue: CurrencyField = {
        let input = CurrencyField()
        input.fontCurrency = UIFont.systemFont(ofSize: 17, weight: .medium)
        input.fontValue = UIFont.systemFont(ofSize: 52, weight: .light)
        input.isUserInteractionEnabled = false
        input.placeholderColor = Palette.ppColorGrayscale300.color
        input.textColor = Palette.ppColorBranding300.color
        input.positionCurrency = .top
        input.limitValue = 9
        input.spacing = 2
        input.delegate = self
        input.translatesAutoresizingMaskIntoConstraints = false
        
        return input
    }()
    
    private lazy var installmentButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        button.layer.cornerRadius = Layout.installmentHeight / 2
        button.layer.borderWidth = Layout.borderWidth
        button.addTarget(self, action: #selector(didTapInstallment), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    private lazy var finishButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(Palette.ppColorGrayscale000.color, for: .normal)
        button.addTarget(self, action: #selector(didTapFinish), for: .touchUpInside)
        button.setTitle(DefaultLocalizable.btCancel.text, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    weak var delegate: PaymentHeaderOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.viewWillAppear()
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    override func buildViewHierarchy() {
        backgroundImage.addSubview(finishButton)
        view.addSubview(backgroundImage)
        view.addSubview(profileNameLabel)
        view.addSubview(inputValue)
        view.addSubview(valuePaymentLabel)
        view.addSubview(profileImage)
        view.addSubview(installmentButton)
        view.addSubview(orderPaymentLabel)
    }
    
    override func configureViews() {
        view.backgroundColor = .clear
    }
    
    override func setupConstraints() {
        finishButton.layout {
            $0.top == backgroundImage.topAnchor + Spacing.base04
            $0.leading == backgroundImage.leadingAnchor + Layout.margin
        }
        
        backgroundImage.layout {
            $0.top == view.topAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
            $0.height == Layout.sizeBackground
        }
        
        profileImage.layout {
            $0.centerY == backgroundImage.bottomAnchor
            $0.centerX == view.centerXAnchor
        }
        
        profileNameLabel.layout {
            $0.top == profileImage.bottomAnchor + Layout.margin
            $0.leading == view.leadingAnchor + Layout.margin
            $0.trailing == view.trailingAnchor - Layout.margin
        }
        
        valuePaymentLabel.layout {
            $0.top == profileNameLabel.bottomAnchor + Layout.margin
            $0.leading == view.leadingAnchor + Layout.margin
            $0.trailing == view.trailingAnchor - Layout.margin
        }
        
        inputValue.layout {
            $0.top == valuePaymentLabel.bottomAnchor + Layout.topInputValue
            $0.centerX == view.centerXAnchor
            $0.leading >= view.leadingAnchor + Layout.margin
            $0.trailing <= view.trailingAnchor - Layout.margin
        }
        
        installmentButton.layout {
            $0.top == inputValue.bottomAnchor + Layout.margin
            $0.centerX == view.centerXAnchor
            $0.leading >= view.leadingAnchor + Layout.margin
            $0.trailing <= view.trailingAnchor - Layout.margin
            $0.width == Layout.installmentWidth
            $0.height == Layout.installmentHeight
        }
        
        orderPaymentLabel.layout {
            $0.top == installmentButton.bottomAnchor + Layout.margin
            $0.leading == view.leadingAnchor + Layout.margin
            $0.trailing == view.trailingAnchor - Layout.margin
            $0.bottom <= view.bottomAnchor
        }
    }
    
    func dismissKeyboard() {
        inputValue.resignResponder()
    }
    
    func appearKeyboard() {
        inputValue.becomeResponder()
    }
    
    @objc
    private func didTapInstallment() {
        viewModel.didTapInstallment()
    }
    
    @objc
    private func didTapFinish() {
        viewModel.didTapFinish()
    }
    
    func changePayment(value: Double) {
        viewModel.changeValue(value: value)
    }
}

// MARK: View Model Outputs
extension PaymentQrCodeHeaderViewController: PaymentQrCodeHeaderDisplay {
    func setupInput(value: Double) {
        inputValue.value = value
    }
    
    func updateValue(value: Double) {
        delegate?.headerUpdateValue(value: value)
    }
    
    func diplayProfileName(_ profileName: String) {
        profileNameLabel.text = profileName
    }
    
    func diplayBackgroundImage(_ image: URL?) {
        backgroundImage.setImage(url: image)
    }
    
    func diplayProfileImage(_ image: URL?, placeholder: UIImage, type: HeaderPayment.PayeeType) {
        profileImage.setImage(image: image, placeholder: placeholder)
        profileImage.profileType = type
    }
    
    func diplayFinishButton(title: String, image: UIImage?) {
        finishButton.setTitle(title, for: .normal)
        finishButton.setImage(image, for: .normal)
    }
    
    func displayInstallmentButton(title: String?, color: UIColor, borderColor: UIColor) {
        installmentButton.setTitle(title, for: .normal)
        installmentButton.setTitleColor(color, for: .normal)
        installmentButton.tintColor = color
        installmentButton.layer.borderColor = borderColor.cgColor
    }
    
    func updateInstallment(installment: Int, forceCreditCard: Bool) {
        delegate?.headerUpdateInstallment(installment: installment, forceCreditCard: forceCreditCard)
    }
    
    func diplayOrder(_ order: String?, isDisabled: Bool) {
        orderPaymentLabel.text = order
        orderPaymentLabel.isHidden = isDisabled
    }
}

extension PaymentQrCodeHeaderViewController: CurrencyFieldDelegate {
    func onValueChange(value: Double) {
        viewModel.changeValue(value: value)
    }
}
