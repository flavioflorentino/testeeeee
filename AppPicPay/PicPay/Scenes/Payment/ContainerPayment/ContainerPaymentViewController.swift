import AssetsKit
import CorePayment
import UI
import UIKit

final class ContainerPaymentViewController: ViewController<ContainerPaymentViewModelInputs, UIView>, LoadingViewProtocol {
    private let headerViewController: HeaderPaymentContract
    private let accessoryViewController: AccessoryPaymentContract?
    private let toolbarViewController: ToolbarPaymentContract
    
    private var auth: PPAuth?
    private var bottomStack: NSLayoutConstraint?
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    }()
    
    lazy var loadingView = LoadingView()
    
    var shouldDisplayCloseButton = true
    
    init(
        viewModel: ContainerPaymentViewModelInputs,
        headerViewController: HeaderPaymentContract,
        accessoryViewController: AccessoryPaymentContract?,
        toolbarViewController: ToolbarPaymentContract
    ) {
        self.headerViewController = headerViewController
        self.accessoryViewController = accessoryViewController
        self.toolbarViewController = toolbarViewController
        
        super.init(viewModel: viewModel)
    }
    
    override func viewDidLoad() {
        addObservers()
        setupControllers()
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        viewModel.viewWillAppear()
    }
    
    override func configureViews() {
        if shouldDisplayCloseButton {
            navigationItem.leftBarButtonItem = UIBarButtonItem(
                title: DefaultLocalizable.btClose.text,
                style: .plain,
                target: self,
                action: #selector(didTapClose)
            )
        }
        navigationController?.view.backgroundColor = navigationController?.navigationBar.barTintColor
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
   
    override func buildViewHierarchy() {
        addHeaderViewController()
        addAccessoryViewController()
        addToolbarViewController()
        
        view.addSubview(stackView)
        stackView.bringSubviewToFront(headerViewController.view)
        view.sendSubviewToBack(stackView)
    }
    
    override func setupConstraints() {
        setupHuggingPrioritys()
        bottomStack = stackView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: view.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            bottomStack ?? stackView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    private func addHeaderViewController() {
        addChild(headerViewController)
        stackView.addArrangedSubview(headerViewController.view)
        headerViewController.didMove(toParent: self)
    }
    
    private func addAccessoryViewController() {
        guard let accessory = accessoryViewController else {
            return
        }
        
        addChild(accessory)
        stackView.addArrangedSubview(accessory.view)
        accessory.didMove(toParent: self)
    }
    
    private func addToolbarViewController() {
        addChild(toolbarViewController)
        stackView.addArrangedSubview(toolbarViewController.view)
        toolbarViewController.didMove(toParent: self)
    }
    
    private func setupHuggingPrioritys() {
        headerViewController.view.setContentHuggingPriority(.required, for: .vertical)
        accessoryViewController?.view.setContentHuggingPriority(.defaultLow, for: .vertical)
        toolbarViewController.view.setContentHuggingPriority(.required, for: .vertical)
    }
    
    private func setupControllers() {
        headerViewController.delegate = self
        accessoryViewController?.delegate = self
        toolbarViewController.delegate = self
    }
    
    private func addObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    @objc
    private func keyboardWillShow(notification: NSNotification) {
        guard
            let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue,
            let offset = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            else {
                return
            }
        
        bottomStack?.constant = keyboardSize.height == offset.height ? -keyboardSize.height : -offset.height
        view.layoutIfNeeded()
    }
    
    @objc
    private func keyboardWillHide(notification: NSNotification) {
        bottomStack?.constant = 0
        view.layoutIfNeeded()
    }
    
    @objc
    private func didTapClose() {
        viewModel.didTapClose()
    }
}

// MARK: View Model Outputs
extension ContainerPaymentViewController: ContainerPaymentDisplay {
    func displayRecoverPassword() {
        stopLoadingView()
        PPAuthSwift.handlePasswordError()
    }
    
    func displayLimitExceeded(model: LimitExceededError) {
        stopLoadingView()
        let popup = LimitExceededPopup(error: model)
        popup.view.backgroundColor = Palette.black.color.withAlphaComponent(0.6)
        popup.didClose = { [weak self] in
            self?.viewModel.limitExceededClose()
        }
        
        popup.buttonTapAction = { [weak self] in
            popup.dismiss(animated: false) {
                self?.startLoadingView()
                self?.viewModel.limitExceededPay(surcharge: model.surcharge.doubleValue)
            }
        }
        
        popup.waitButtonTapAction = { [weak self] limitError in
            self?.viewModel.limitExceededSendNotification(error: limitError)
        }
        
        popup.modalPresentationStyle = .overFullScreen
        present(popup, animated: false)
    }
    
    func displayTryAgain(withTitle title: String, andMessage message: String) {
        stopLoadingView()
        
        let popup = PopupViewController(title: title, description: message, image: Resources.Illustrations.iluError.image)
        
        let tryAgainAction = PopupAction(title: PaymentLocalizable.tryAgain.text, style: .fill) {
            self.displayPasswordConfirmation()
        }
        
        popup.addAction(tryAgainAction)
        
        showPopup(popup)
    }
    
    func didReceiveAnError(error: PicPayError) {
        stopLoadingView()
        AlertMessage.showCustomAlertWithErrorUI(error, controller: self)
    }
    
    func updateHeader(value: Double) {
        headerViewController.changePayment(value: value)
    }
    
    func becomeResponder() {
        headerViewController.appearKeyboard()
    }
    
    func updateToolbar(value: Double, installment: Int, forceCreditCard: Bool) {
        toolbarViewController.updateToolbar(value: value, installment: installment, forceCreditCard: forceCreditCard)
    }
    
    func updateToolbar(paymentMethod: PaymentMethod) {
        toolbarViewController.paymentMethod = paymentMethod
    }
    
    func updateAccessory(paymentMethod: PaymentMethod) {
        accessoryViewController?.changePaymentMethod(method: paymentMethod)
    }
    
    func updateAccessory(with values: AccessoryValue) {
        accessoryViewController?.changeAccessory(with: values)
    }
    
    func stopLoad() {
        stopLoadingView()
    }
    
    func displayPasswordConfirmation() {
        headerViewController.dismissKeyboard()
        auth = PPAuth.authenticate({ [weak self] password, biometry in
            guard
                let password = password,
                let self = self
                else {
                    return
            }
            // Evento: confirma senha
            self.startLoadingView()
            self.viewModel.makePayment(password: password, biometry: biometry)
            }, canceledByUserBlock: nil)
    }
}

extension ContainerPaymentViewController: PaymentHeaderOutput {
    func headerUpdateValue(value: Double) {
        viewModel.changeHeaderValue(value)
    }
    
    func headerUpdateInstallment(installment: Int, forceCreditCard: Bool) {
        viewModel.changeHeaderInstallment(installment, forceCreditCard: forceCreditCard)
    }
    
    func didTapPaymentMethodSelection() {
        toolbarViewController.showPaymentMethodSelection()
    }
    
    func didTapInstallments() {
        viewModel.didTapInstallments()
    }
}

extension ContainerPaymentViewController: PaymentAccessoryOutput {
    func accessoryUpdateValue(value: Double) {
        viewModel.changeAccessoryValue(value)
    }
    
    func accessoryUpdateCardFee(cardFee: Double) {
        viewModel.changeCardFee(cardFee)
    }
    
    func accessoryUpdateMessage(message: String) {
        viewModel.changeMessage(message)
    }
    
    func accessoryUpdatePaymentParams(_ param: Decodable) {
        viewModel.addExtraPayloadParam(param)
    }
    
    func update(paymentType: PaymentType) {
        viewModel.changePaymentType(paymentType)
        toolbarViewController.changePayButtonTitle(with: paymentType)
    }
    
    func showLoadingView() {
        startLoadingView()
    }
    
    func hideLoadingView() {
        stopLoadingView()
    }
}

extension ContainerPaymentViewController: PaymentToolbartOutput {
    func didTapPayment() {
        viewModel.didTapPay()
    }
    
    func changePrivacy(privacy: PaymentPrivacy) {
        viewModel.changePrivacy(privacy)
    }
    
    func completionPaymentMethods() {
        viewModel.completionPaymentMethods()
    }
}
