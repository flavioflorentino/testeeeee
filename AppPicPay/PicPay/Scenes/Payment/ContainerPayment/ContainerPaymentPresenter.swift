import Core
import CorePayment
import Foundation

protocol ContainerPaymentPresenting: AnyObject {
    associatedtype Success
    var viewController: ContainerPaymentDisplay? { get set }
    func paymentSuccess(model: Success, paymentType: PaymentType)
    func payment3DSSuccess(model: ReceiptWidgetViewModel)
    func headerHasUpdated(value: Double, installment: Int, forceCreditCard: Bool)
    func accessoryHasUpdated(value: Double)
    func updatePaymentMethod(_ paymentMethod: PaymentMethod)
    func presentAccessoryValues(_ values: AccessoryValue)
    func incorrectPassword()
    func presentTryAgain(withTitle title: String, andMessage message: String)
    func didReceiveAnError(error: PicPayError)
    func limitExceeded(error: PicPayError)
    func limitExceededClose()
    func didNextStep(action: ContainerPaymentAction)
    func presentPasswordConfirmation()
}

extension ContainerPaymentPresenting {
    func paymentSuccess(model: Success) {
        paymentSuccess(model: model, paymentType: .default)
    }
}

final class ContainerPaymentPresenter<T, Coordinator: ContainerPaymentCoordinating>: ContainerPaymentPresenting where Coordinator.Success == T {
    private let coordinator: Coordinator
    weak var viewController: ContainerPaymentDisplay?
    
    init(coordinator: Coordinator) {
        self.coordinator = coordinator
    }
    
    func headerHasUpdated(value: Double, installment: Int, forceCreditCard: Bool) {
        viewController?.updateToolbar(value: value, installment: installment, forceCreditCard: forceCreditCard)
    }
    
    func updatePaymentMethod(_ paymentMethod: PaymentMethod) {
        viewController?.updateToolbar(paymentMethod: paymentMethod)
        viewController?.updateAccessory(paymentMethod: paymentMethod)
    }
    
    func presentAccessoryValues(_ values: AccessoryValue) {
        viewController?.updateAccessory(with: values)
    }
    
    func accessoryHasUpdated(value: Double) {
        viewController?.updateHeader(value: value)
    }
    
    func didReceiveAnError(error: PicPayError) {
        feedbackError()
        viewController?.didReceiveAnError(error: error)
    }
    
    func incorrectPassword() {
        viewController?.displayRecoverPassword()
    }
    
    func presentTryAgain(withTitle title: String, andMessage message: String) {
        viewController?.displayTryAgain(withTitle: title, andMessage: message)
    }
    
    func limitExceeded(error: PicPayError) {
        guard let model = LimitExceededError(error: error) else {
            didReceiveAnError(error: error)
            return
        }
        
        viewController?.displayLimitExceeded(model: model)
    }
    
    func didNextStep(action: ContainerPaymentAction) {
        if flowInterruptLoading(action: action) {
            viewController?.stopLoad()
        }
        
        coordinator.perform(action: action)
    }
    
    func paymentSuccess(model: T, paymentType: PaymentType) {
        feedbackSuccess()
        coordinator.paymentSuccess(model: model, paymentType: paymentType)
    }
    
    func payment3DSSuccess(model: ReceiptWidgetViewModel) {
        feedbackSuccess()
        coordinator.perform(action: .payment3DSSuccess(model))
    }
    
    func limitExceededClose() {
        viewController?.becomeResponder()
    }
    
    func presentPasswordConfirmation() {
        viewController?.displayPasswordConfirmation()
    }
    
    private func flowInterruptLoading(action: ContainerPaymentAction) -> Bool {
        switch action {
        case .openCardChallenge, .openIdentityChallenge:
            return true
        default:
            return false
        }
    }
    
    private func feedbackError() {
        DispatchQueue.main.async {
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.error)
        }
    }
    
    private func feedbackSuccess() {
        DispatchQueue.main.async {
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.success)
        }
    }
}
