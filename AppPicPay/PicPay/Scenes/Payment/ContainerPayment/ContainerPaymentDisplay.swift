import CorePayment

protocol ContainerPaymentDisplay: AnyObject {
    func displayRecoverPassword()
    func becomeResponder()
    func stopLoad()
    func displayLimitExceeded(model: LimitExceededError)
    func displayTryAgain(withTitle title: String, andMessage message: String)
    func updateHeader(value: Double)
    func updateToolbar(value: Double, installment: Int, forceCreditCard: Bool)
    func updateToolbar(paymentMethod: PaymentMethod)
    func updateAccessory(paymentMethod: PaymentMethod)
    func updateAccessory(with values: AccessoryValue)
    func didReceiveAnError(error: PicPayError)
    func displayPasswordConfirmation()
}
