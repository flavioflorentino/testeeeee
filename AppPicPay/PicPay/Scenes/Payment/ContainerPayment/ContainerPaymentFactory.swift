import CorePayment
import Foundation

typealias HeaderPaymentContract = UIViewController & PaymentHeaderInput
typealias AccessoryPaymentContract = UIViewController & PaymentAccessoryInput
typealias ToolbarPaymentContract = UIViewController & PaymentToolbarInput

enum ContainerPaymentReceiptFactory<Service: LegacyPaymentServicing, Coordinator: PaymentReceiptCoordinator> where Service.Success == Coordinator.Success, Service.Failure == PicPayError {
    static func make(
        paymentService: Service,
        actionsDelegate: PaymentActionsDelegate?,
        headerViewController: HeaderPaymentContract,
        accessoryViewController: AccessoryPaymentContract?,
        toolbarViewController: ToolbarPaymentContract,
        paymentManager: PPPaymentManager = PPPaymentManager()
    ) -> ContainerPaymentViewController {
        ContainerPaymentFactory.make(coordinatorSuccess: PaymentReceiptCoordinator(),
                                     paymentService: paymentService,
                                     actionsDelegate: actionsDelegate,
                                     headerViewController: headerViewController,
                                     accessoryViewController: accessoryViewController,
                                     toolbarViewController: toolbarViewController,
                                     paymentManager: paymentManager)
    }
}

enum ContainerPaymentFactory<Service: LegacyPaymentServicing, Coordinator: PaymentSuccessCoordinating> where Service.Success == Coordinator.Success, Service.Failure == PicPayError {
    static func make(
        coordinatorSuccess: Coordinator,
        paymentService: Service,
        actionsDelegate: PaymentActionsDelegate?,
        headerViewController: HeaderPaymentContract,
        accessoryViewController: AccessoryPaymentContract?,
        toolbarViewController: ToolbarPaymentContract,
        paymentManager: PPPaymentManager = PPPaymentManager()
    ) -> ContainerPaymentViewController {
        let service: ContainerPaymentServicing = ContainerPaymentService(
            adyen: AdyenWorker(),
            cardManager: CreditCardManager.shared,
            paymentManager: paymentManager
        )
        let coordinator = ContainerPaymentCoordinator(coordinatorSuccess: coordinatorSuccess)
        let presenter = ContainerPaymentPresenter(coordinator: coordinator)
        let viewModel = ContainerPaymentViewModel(
            service: service,
            payment: paymentService,
            presenter: presenter,
            actionsDelegate: actionsDelegate
        )
        let viewController = ContainerPaymentViewController(
            viewModel: viewModel,
            headerViewController: headerViewController,
            accessoryViewController: accessoryViewController,
            toolbarViewController: toolbarViewController
        )
        
        coordinator.viewController = viewController
        presenter.viewController = viewController
        
        return viewController
    }
}
