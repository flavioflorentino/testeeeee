import Core
import CorePayment
import Foundation

protocol ContainerPaymentServicing {
    var firstAccess3DS: Bool { get }
    var defaultCardId: String { get }
    var useBalance: Bool { get }
    var flagCard: CreditCardFlagTypeId { get }

    func saveCvvCard(id: String, value: String?)
    func cvvCard(id: String) -> String?
    func createPayload3DS(error: PicPayError) -> [String: String]?
    func errorIs3ds(error: PicPayError) -> Bool
    func value(for paymentValue: PaymentValue, with payload: PaymentManagerPayload) -> Double
    func sendNotificationPicPayPro(payload: [String: Any])
    func continueOn3DSTransaction(adyenPayload: [String: String], completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void)
}

final class ContainerPaymentService: ContainerPaymentServicing {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies = DependencyContainer()
    private var cardManager: CreditCardManager
    private var paymentManager: PPPaymentManager
    private let adyen: AdyenServicing
    
    // MARK: - Adyen Stuff
    private var transaction: ADYTransaction?
    private var adyenParameters: [String: Any] = [:]
        
    var firstAccess3DS: Bool {
        adyen.firstAccess3DS
    }
    
    var defaultCardId: String {
        let card = cardManager.defaultCreditCard
        return card?.id ?? "0"
    }
    
    var useBalance: Bool {
        paymentManager.usePicPayBalance()
    }
    
    var flagCard: CreditCardFlagTypeId {
        guard
            let flagId = cardManager.defaultCreditCard?.creditCardBandeiraId,
            let flag = Int(flagId),
            let flagCard = CreditCardFlagTypeId(rawValue: flag)
            else {
                return .unknown
        }
        
        return flagCard
    }
    
    init(adyen: AdyenServicing, cardManager: CreditCardManager, paymentManager: PPPaymentManager) {
        self.adyen = adyen
        self.cardManager = cardManager
        self.paymentManager = paymentManager
    }
    
    func saveCvvCard(id: String, value: String?) {
        dependencies.keychain.set(key: KeychainKeyPF.cvv(id), value: value)
    }
    
    func cvvCard(id: String) -> String? {
        dependencies.keychain.getData(key: KeychainKeyPF.cvv(id))
    }
    
    func value(for paymentValue: PaymentValue, with payload: PaymentManagerPayload) -> Double {
        paymentManager.subtotal = NSDecimalNumber(value: payload.value)
        paymentManager.cardFee = NSDecimalNumber(value: payload.cardFee)
        paymentManager.selectedQuotaQuantity = payload.installment
        
        let value: NSDecimalNumber?

        switch paymentValue {
        case .balance:
            value = paymentManager.balanceTotal()
        case .card:
            value = paymentManager.cardTotal()
        case .cardWithoutInterest:
            value = paymentManager.cardTotalWithoutInterest()
        case .cardFee:
            value = paymentManager.cardConvenienceFee()
        case .installment:
            value = paymentManager.quotaValue()
        case .installmentFee:
            value = paymentManager.installmentsTotalFee()
        }
        
        return value?.doubleValue ?? 0.0
    }
    
    func createPayload3DS(error: PicPayError) -> [String: String]? {
        Adyen3DS.adyenPayload(error: error)
    }
    
    func errorIs3ds(error: PicPayError) -> Bool {
        Adyen3DS.is3ds(error: error)
    }
    
    /// In the old PRO the call was like this ¯\_(ツ)_/¯
    func sendNotificationPicPayPro(payload: [String: Any]) {
        DispatchQueue.global().async {
            WSConsumer.sendNotificationPicPayPro(with: payload) { _, _ in }
        }
    }
    
    func continueOn3DSTransaction(adyenPayload: [String: String], completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        adyen.transaction(with: adyenPayload,
                          hasChallenge: { [weak self] transaction, parameters, payload in
            guard
                let transaction = transaction as? ADYTransaction,
                let parameters = parameters as? ADYChallengeParameters
                else {
                    return
            }
            
            self?.transaction = transaction
            self?.adyenParameters = payload
            
            self?.performChallenge(parameters: parameters, completion: completion)
        }, succeeded: { receiptWidget in
            completion(.success(receiptWidget))
        }, error: { error in
            let picpayError = PicPayError(message: error.localizedDescription)
            completion(.failure(picpayError))
        }, injecting: ADYService.self)
    }
    
    private func performChallenge(parameters: ADYChallengeParameters, completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        transaction?.performChallenge(with: parameters) { [weak self] result, error in
            guard result != nil else {
                let picpayError = PicPayError(message: error?.localizedDescription ?? DefaultLocalizable.unexpectedError.text)
                completion(.failure(picpayError))
                return
            }
            
            self?.challengeSuccess(completion: completion)
        }
    }
    
    private func challengeSuccess(completion: @escaping (Result<ReceiptWidgetViewModel, PicPayError>) -> Void) {
        adyen.complete(
            payload: adyenParameters,
            succeeded: { receipt in
                completion(.success(receipt))
            }, error: { error in
                let picpayError = PicPayError(message: error.localizedDescription)
                completion(.failure(picpayError))
            }
        )
    }
}
