import CorePayment
import UI
import UIKit

enum ContainerPaymentAction {
    case openIdentityChallenge
    case payment3DSSuccess(ReceiptWidgetViewModel)
    case openCardChallenge(cardId: String)
    case openInsertCvv(id: String, completedCvv: (String) -> Void, completedNoCvv: () -> Void)
    case show3DSWarning(complete: () -> Void)
    case showBlockError(_ errorType: PaymentErrorType)
    case close
}

protocol ContainerPaymentCoordinating: AnyObject {
    associatedtype Success
    var viewController: UIViewController? { get set }
    func paymentSuccess(model: Success, paymentType: PaymentType)
    func perform(action: ContainerPaymentAction)
}

final class ContainerPaymentCoordinator<T, Coordinator: PaymentSuccessCoordinating>: ContainerPaymentCoordinating where Coordinator.Success == T {
    private var coordinator: Coordinating?
    private let coordinatorSuccess: Coordinator
    weak var viewController: UIViewController?
    
    init(coordinatorSuccess: Coordinator) {
        self.coordinatorSuccess = coordinatorSuccess
    }
    
    func paymentSuccess(model: T, paymentType: PaymentType) {
        coordinatorSuccess.showPaymentSuccess(model: model, paymentType: paymentType, viewController: viewController)
    }
    
    func perform(action: ContainerPaymentAction) {
        switch action {
        case .payment3DSSuccess(let model):
            guard let viewController = viewController else {
                return
            }
            
            TransactionReceipt.showReceiptSuccess(viewController: viewController, receiptViewModel: model)
            
        case .openIdentityChallenge:
            let controller = IdentityChallengeViewController()
            viewController?.present(UINavigationController(rootViewController: controller), animated: true)
        
        case .openCardChallenge(let cardId):
            let controller = CardChallengeViewController(cardId: cardId)
            viewController?.present(UINavigationController(rootViewController: controller), animated: true)
            
        case let .openInsertCvv(id, completedCvv, completedNoCvv):
            guard let navigation = viewController?.navigationController else {
                return
            }
            
            let coordinator = CvvRegisterFlowCoordinator(
                cardType: .id(id),
                navigationController: navigation,
                paymentType: .newPayment,
                finishedCvv: completedCvv,
                finishedWithoutCvv: completedNoCvv
            )
            coordinator.start()
        
            self.coordinator = coordinator
            
        case .show3DSWarning(let complete):
            guard let viewController = viewController else {
                return
            }
            
            AdyenWarning().show(in: viewController) {
                complete()
            }
            
        case .showBlockError(let errorType):
            presentBlockError(with: errorType)
            
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}

private extension ContainerPaymentCoordinator {
    func presentBlockError(with errorType: PaymentErrorType) {
        let errorInfo: ErrorInfoModel
        
        switch errorType {
        case .dueDateExceeded:
            errorInfo = ErrorInfoModel(
                image: Assets.Icons.sadIcon.image,
                title: PaymentLocalizable.billetOverdueTitle.text,
                message: PaymentLocalizable.billetOverdueMessage.text,
                primaryAction: ErrorAction(title: DefaultLocalizable.btOkUnderstood.text) { errorController in
                    let presentingController = errorController.presentingViewController
                    presentingController?.presentingViewController?.dismiss(animated: true)
                }
            )
            
        case .timeoutHourExceeded:
            errorInfo = ErrorInfoModel(
                image: Assets.Icons.sadIcon.image,
                title: PaymentLocalizable.billetTimeoutTitle.text,
                message: PaymentLocalizable.billetTimeoutMessage.text,
                primaryAction: ErrorAction(title: DefaultLocalizable.tryAgain.text) { errorController in
                    self.viewController?.navigationController?.popViewController(animated: true)
                    errorController.dismiss(animated: true)
                },
                secondaryAction: ErrorAction(title: DefaultLocalizable.notNow.text) { errorController in
                    let presentingController = errorController.presentingViewController
                    presentingController?.presentingViewController?.dismiss(animated: true)
                }
            )
            
        default:
            return
        }
        
        let errorController = ErrorFactory.make(with: errorInfo)
        viewController?.present(errorController, animated: true)
    }
}
