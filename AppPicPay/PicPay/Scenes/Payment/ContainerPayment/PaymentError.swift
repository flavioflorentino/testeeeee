import Foundation

enum PaymentErrorType: CaseIterable {
    case limitExceeded
    case incorrectPassword
    case identityChallenge
    case cardChallenge
    case dueDateExceeded
    case timeoutHourExceeded
    case billetNotScheduled
        
    init?(_ error: PicPayError) {
        let caseMatch = PaymentErrorType.allCases.first { $0.match(error) }
        guard let match = caseMatch else {
            return nil
        }
        self = match
    }
}

private extension PaymentErrorType {
    func match(_ error: PicPayError) -> Bool {
        switch self {
        case .limitExceeded:
            return isLimitExceededError(error)
        case .incorrectPassword:
            return isIncorrectPasswordError(error)
        case .identityChallenge:
            return isIdentityChallengeError(error)
        case .cardChallenge:
            return isCardChallengeError(error)
        case .dueDateExceeded:
            return isDueDateExceededError(error)
        case .timeoutHourExceeded:
            return isTimeoutHourExceededError(error)
        case .billetNotScheduled:
            return isBilletNotScheduledError(error)
        }
    }
    
    func isLimitExceededError(_ error: PicPayError) -> Bool {
        let errorType = PaymentErrorCode(rawValue: error.code)
        return errorType == .some(.limitExceededI) || errorType == .some(.limitExceededII)
    }
    
    func isIncorrectPasswordError(_ error: PicPayError) -> Bool {
        let errorType = PaymentErrorCode(rawValue: error.code)
        return errorType == .some(.incorrectPassword)
    }
    
    func isIdentityChallengeError(_ error: PicPayError) -> Bool {
        guard let data = error.dataDictionary(), let value = data["type"] as? String else {
            return false
        }
           
        return value == "verify_id"
    }
       
    func isCardChallengeError(_ error: PicPayError) -> Bool {
        guard let data = error.dataDictionary(), let value = data["type"] as? String else {
            return false
        }
       
        return value == "verify_card"
   }
    
    func isDueDateExceededError(_ error: PicPayError) -> Bool {
        let errorType = PaymentErrorCode(rawValue: error.code)
        return errorType == .some(.dueDateExceeded)
    }
    
    func isTimeoutHourExceededError(_ error: PicPayError) -> Bool {
        let errorType = PaymentErrorCode(rawValue: error.code)
        return errorType == .some(.timeoutHourExceeded)
    }
    
    func isBilletNotScheduledError(_ error: PicPayError) -> Bool {
        let errorType = PaymentErrorCode(rawValue: error.code)
        return errorType == .some(.billetNotScheduled)
    }
}

enum PaymentErrorCode: Int {
    case incorrectPassword = 6_003
    case limitExceededI = 1_338
    case limitExceededII = 1_340
    case dueDateExceeded = 16_032_001
    case timeoutHourExceeded = 16_032_000
    case billetNotScheduled = 16_032_006
}
