import CorePayment
import Foundation

protocol ContainerPaymentViewModelInputs: AnyObject {
    func viewWillAppear()
    func didTapInstallments()
    func completionPaymentMethods()
    func changeHeaderValue(_ value: Double)
    func changeHeaderInstallment(_ installment: Int, forceCreditCard: Bool)
    func changeAccessoryValue(_ value: Double)
    func changeCardFee(_ cardFee: Double)
    func changeMessage(_ message: String)
    func changePrivacy(_ privacy: PaymentPrivacy)
    func changePaymentType(_ paymentType: PaymentType)
    func didTapClose()
    func limitExceededClose()
    func limitExceededPay(surcharge: Double)
    func limitExceededSendNotification(error: LimitExceededError)
    func addExtraPayloadParam(_ param: Decodable)
    func makePayment(password: String, biometry: Bool)
    func didTapPay()
}

final class ContainerPaymentViewModel<T, Service: LegacyPaymentServicing, Presenter: ContainerPaymentPresenting> where Presenter.Success == T, Service.Success == T, Service.Failure == PicPayError {
    private let service: ContainerPaymentServicing
    private let payment: Service
    private let presenter: Presenter
    private let actions: PaymentActionsDelegate?
    
    private var value: Double
    private var installment: Int
    private var cardFee: Double
    private var forceCreditCard: Bool
    
    private var paymentType: PaymentType
    private var message: String
    private var extraPayloadParam: Decodable?
    private var privacy: PaymentPrivacy
    private var someErrorOccurred: Bool
    
    private var password: String
    private var biometry: Bool
    private var insertedCvv: String?
    
    // MARK: - Adyen Stuff
    private var adyenPayload: [String: String]
    
    // MARK: - Analytics
    private var wentToCards = false
    private var wentToInstallments = false
    
    init(
        service: ContainerPaymentServicing,
        payment: Service,
        presenter: Presenter,
        actionsDelegate: PaymentActionsDelegate?
    ) {
        self.service = service
        self.payment = payment
        self.presenter = presenter
        actions = actionsDelegate
        
        value = 0.0
        installment = 1
        cardFee = 0.0
        forceCreditCard = false
        
        paymentType = .default
        message = ""
        privacy = .public
        someErrorOccurred = false
        adyenPayload = [:]
        
        password = ""
        biometry = false
    }
}

extension ContainerPaymentViewModel: ContainerPaymentViewModelInputs {
    func viewWillAppear() {
        actions?.viewWillAppear()
    }
    
    func didTapInstallments() {
        wentToInstallments = true
        actions?.didTapInstallments()
    }
    
    func completionPaymentMethods() {
        wentToCards = true
        actions?.didTapChangePaymentMethod()
        updatePaymentMethod(total: value, installment: installment)
    }
    
    func changeHeaderValue(_ value: Double) {
        self.value = value
        updatePaymentMethod(total: value, installment: installment)
        updateAccessoryValues()
        presenter.headerHasUpdated(value: value, installment: installment, forceCreditCard: forceCreditCard)
    }
    
    func changeHeaderInstallment(_ installment: Int, forceCreditCard: Bool) {
        self.installment = installment
        self.forceCreditCard = forceCreditCard
        updatePaymentMethod(total: value, installment: installment)
        updateAccessoryValues()
        presenter.headerHasUpdated(value: value, installment: installment, forceCreditCard: forceCreditCard)
    }
    
    func changeMessage(_ message: String) {
        self.message = message
    }
    
    func changeAccessoryValue(_ value: Double) {
        self.value = value
        updatePaymentMethod(total: value, installment: installment)
        updateAccessoryValues()
        presenter.accessoryHasUpdated(value: value)
    }
    
    func changeCardFee(_ cardFee: Double) {
        self.cardFee = cardFee
        updateAccessoryValues()
    }
    
    func changePrivacy(_ privacy: PaymentPrivacy) {
        self.privacy = privacy
    }
    
    func changePaymentType(_ paymentType: PaymentType) {
        self.paymentType = paymentType
    }
    
    func didTapClose() {
        actions?.paymentCancel()
        presenter.didNextStep(action: .close)
    }
    
    func limitExceededClose() {
        presenter.limitExceededClose()
    }
    
    func limitExceededPay(surcharge: Double) {
        createTransaction(password: password, biometry: biometry, surcharge: surcharge, informedCvv: insertedCvv)
        insertedCvv = nil
    }
    
    func limitExceededSendNotification(error: LimitExceededError) {
        service.sendNotificationPicPayPro(payload: error.payload)
    }
    
    func addExtraPayloadParam(_ param: Decodable) {
        extraPayloadParam = param
        actions?.extraParams = param
    }
    
    func makePayment(password: String, biometry: Bool) {
        self.password = password
        self.biometry = biometry
        actions?.didTapPayment(wentToCards: wentToCards, wentToInstallments: wentToInstallments)
        
        guard paymentRulesAreValid() else {
            processPaymentRules(password: password, biometry: biometry)
            return
        }
        
        createTransaction(password: password, biometry: biometry)
    }
    
    func didTapPay() {
        actions?.didTapPay(
            privacyType: privacy,
            hasMessage: message.isEmpty == false,
            requestValue: String(value)
        )
        presenter.presentPasswordConfirmation()
    }
    
    private func processPaymentRules(password: String, biometry: Bool) {
        if !valueHasFilled() {
            errorUnfilledValue()
        } else if isCvvNeeded() {
            openFlowInsertCvv(password: password, biometry: biometry)
        } else {
            let picpayError = PicPayError(message: DefaultLocalizable.unexpectedError.text)
            presenter.didReceiveAnError(error: picpayError)
        }
    }
    
    private func errorUnfilledValue() {
        let picpayError = PicPayError(message: PaymentLocalizable.errorValue.text)
        presenter.didReceiveAnError(error: picpayError)
    }
    
    private func valueHasFilled() -> Bool {
        value > .zero
    }
    
    private func paymentRulesAreValid() -> Bool {
        valueHasFilled() && !isCvvNeeded()
    }
    
    private func openFlowInsertCvv(password: String, biometry: Bool) {
        let id = service.defaultCardId
        
        presenter.didNextStep(action: .openInsertCvv(id: id, completedCvv: { [weak self] cvv in
            self?.insertedCvv = cvv
            self?.createTransaction(password: password, biometry: biometry, informedCvv: cvv)
        }, completedNoCvv: { [weak self] in
            self?.insertCvvError()
        }))
    }
    
    private func insertCvvError() {
        let error = PicPayError(message: DefaultLocalizable.cvvNotInformed.text)
        presenter.didReceiveAnError(error: error)
    }
    
    private func createTransaction(password: String, biometry: Bool, surcharge: Double? = nil, informedCvv: String? = nil) {
        let payload = createPayload(password: password, biometry: biometry, surcharge: surcharge, informedCvv: informedCvv)
        
        payment.createTransaction(payment: payload) { [weak self] result in
            switch result {
            case .success(let transaction):
                let paymentType = self?.paymentType ?? .default
                let value = self?.value ?? 0
                self?.saveCvv(informedCvv: informedCvv)
                self?.presenter.paymentSuccess(model: transaction, paymentType: paymentType)
                self?.actions?.paymentSuccess(transactionId: transaction.transactionId, value: value)

            case .failure(let error):
                let errorViewModel = PaymentActionErrorViewModel(title: error.title, message: error.message, code: error.code)
                self?.actions?.paymentFailure(error: errorViewModel)
                self?.transactionError(error: error)
            }
        }
    }
    
    private func createPayload(password: String, biometry: Bool, surcharge: Double?, informedCvv: String?) -> Payment {
        let payload = PaymentManagerPayload(value: value, cardFee: cardFee, installment: installment)
        let balanceValue = service.value(for: .balance, with: payload)
        let cardValueWithoutInterest = service.value(for: .cardWithoutInterest, with: payload)
        let cvv = getCvvOnlyPaymentCardBank(informedCvv: informedCvv)
        
        return Payment(
            paymentType: paymentType,
            password: password,
            biometry: biometry,
            value: value,
            balanceValue: balanceValue,
            cardValueWithoutInterest: cardValueWithoutInterest,
            cardId: service.defaultCardId,
            installment: installment,
            privacyConfig: privacy,
            message: message,
            someErrorOccurred: someErrorOccurred,
            cvv: cvv,
            surcharge: surcharge,
            extra: extraPayloadParam
        )
    }
    
    private func transactionError(error: PicPayError) {
        guard !is3DSError(error) else {
            error3DS(error: error)
            return
        }
        
        switch PaymentErrorType(error) {
        case .identityChallenge:
            presenter.didNextStep(action: .openIdentityChallenge)
        case .cardChallenge:
            openCardChallenge(error: error)
        case .incorrectPassword:
            presenter.incorrectPassword()
        case .limitExceeded:
            presenter.limitExceeded(error: error)
        case .dueDateExceeded:
            presenter.didNextStep(action: .showBlockError(.dueDateExceeded))
        case .timeoutHourExceeded:
            presenter.didNextStep(action: .showBlockError(.timeoutHourExceeded))
        case .billetNotScheduled:
            presenter.presentTryAgain(withTitle: PaymentLocalizable.billetNotScheduledTitle.text,
                                      andMessage: PaymentLocalizable.billetNotScheduledMessage.text)
        default:
            handlerError(error: error)
        }
    }
    
    private func create3DSTransaction() {
        guard service.firstAccess3DS else {
            continueOn3DSTransaction()
            return
        }
        
        presenter.didNextStep(action: .show3DSWarning(complete: { [weak self] in
            self?.continueOn3DSTransaction()
        }))
    }
    
    private func canCreate3DSPayload(error: PicPayError) -> [String: String]? {
        service.createPayload3DS(error: error)
    }
    
    private func continueOn3DSTransaction() {
        service.continueOn3DSTransaction(adyenPayload: adyenPayload) { [weak self] result in
            switch result {
            case .success(let transaction):
                let value = self?.value ?? 0
                self?.actions?.paymentSuccess(transactionId: transaction.transactionId, value: value)
                self?.presenter.payment3DSSuccess(model: transaction)

            case .failure(let error):
                let errorViewModel = PaymentActionErrorViewModel(title: error.title, message: error.message, code: error.code)
                self?.actions?.paymentFailure(error: errorViewModel)
                self?.handlerError(error: error)
            }
        }
    }
    
    private func handlerError(error: PicPayError) {
        someErrorOccurred = true
        presenter.didReceiveAnError(error: error)
    }
    
    private func error3DS(error: PicPayError) {
        guard let payload = canCreate3DSPayload(error: error) else {
            presenter.didReceiveAnError(error: error)
            return
        }
        
        adyenPayload = payload
        create3DSTransaction()
    }
    
    private func isCvvNeeded() -> Bool {
        let payload = PaymentManagerPayload(value: value, cardFee: cardFee, installment: installment)
        let cardValue = service.value(for: .card, with: payload)
        let id = service.defaultCardId
        let typeFlag = service.flagCard
        
        return service.cvvCard(id: id) == nil && cardValue != .zero && typeFlag != .picpay
    }
    
    private func saveCvv(informedCvv: String?) {
        guard let cvv = informedCvv else {
            return
        }
        
        let id = service.defaultCardId
        service.saveCvvCard(id: id, value: cvv)
    }
    
    private func getCvvOnlyPaymentCardBank(informedCvv: String?) -> String? {
        guard let cvv = informedCvv else {
            return getCvvLocalStorePayload()
        }
        
        return cvv
    }
    
    private func getCvvLocalStorePayload() -> String? {
        let payload = PaymentManagerPayload(value: value, cardFee: cardFee, installment: installment)
        let cardValue = service.value(for: .card, with: payload)
        
        guard !cardValue.isZero else {
            return nil
        }
        
        let id = service.defaultCardId
        
        return service.cvvCard(id: id)
    }
    
    private func openCardChallenge(error: PicPayError) {
        guard let data = error.dataDictionary(), let cardId = data["payment_method_id"] as? String else {
            presenter.didReceiveAnError(error: error)
            return
        }
        
        presenter.didNextStep(action: .openCardChallenge(cardId: cardId))
    }
    
    private func is3DSError(_ error: PicPayError) -> Bool {
        service.errorIs3ds(error: error)
    }

    private func updateAccessoryValues() {
        let payload = PaymentManagerPayload(value: value, cardFee: cardFee, installment: installment)
        
        let balanceValue = service.value(for: .balance, with: payload)
        let cardFeeValue = service.value(for: .cardFee, with: payload)
        let installmentValue = service.value(for: .installment, with: payload)
        let installmentFeeValue = service.value(for: .installmentFee, with: payload)
        
        let values = AccessoryValue(total: balanceValue,
                                    cardFeeValue: cardFeeValue,
                                    installment: installment,
                                    installmentValue: installmentValue,
                                    installmentFeeValue: installmentFeeValue)
        
        presenter.presentAccessoryValues(values)
    }
    
    private func updatePaymentMethod(total: Double, installment: Int) {
        let method = paymentMethod(forChargeAmount: total, installments: installment)
        actions?.paymentMethod = method
        actions?.installment = installment
        presenter.updatePaymentMethod(method)
    }
    
    private func paymentMethod(forChargeAmount: Double, installments: Int) -> PaymentMethod {
        let payload = PaymentManagerPayload(value: value, cardFee: cardFee, installment: installment)
        
        let chargeToCard = service.value(for: .card, with: payload)
        let chargeToAccountBalance = service.value(for: .balance, with: payload)
        let useBalance = service.useBalance
        
        switch (chargeToCard, chargeToAccountBalance) {
        case (.zero, .zero):
            return useBalance ? .accountBalance : .card
            
        case let (.zero, balance) where balance != .zero:
            return .accountBalance
            
        case let (card, .zero) where card != .zero:
            return .card

        case let (card, balance) where card != .zero && balance != .zero:
            return .balanceAndCard

        default:
            return .card
        }
    }
}
