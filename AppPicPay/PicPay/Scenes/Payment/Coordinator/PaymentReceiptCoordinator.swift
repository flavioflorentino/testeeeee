import CorePayment
import UIKit

final class PaymentReceiptCoordinator: PaymentSuccessCoordinating {
    typealias Success = ReceiptWidgetViewModel
    
    func showPaymentSuccess(model: ReceiptWidgetViewModel, paymentType: PaymentType, viewController: UIViewController?) {
        guard let viewController = viewController else {
            return
        }
        
        TransactionReceipt.showReceiptSuccess(viewController: viewController, receiptViewModel: model)
    }
}
