import UIKit

protocol CardPaymentAccessoryDisplay: AnyObject {
    func displayTotalValue(leftText: String, rightText: String)
    func displayTaxValue(leftText: String, rightText: String)
    func displayDisclaimer(text: String)
    func taxValueIsHidden(_ value: Bool)
    func disclaimerViewIsHidden(_ value: Bool)
    func displayAlert(_ alert: Alert)
}
