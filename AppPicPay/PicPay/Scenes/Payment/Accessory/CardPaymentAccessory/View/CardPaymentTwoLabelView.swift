import UI
import SnapKit

extension CardPaymentTwoLabelView.Layout {
    enum Size {
        static let button: CGFloat = 14
    }
}

final class CardPaymentTwoLabelView: UIView, ViewConfiguration {
    fileprivate enum Layout { }
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.axis = .horizontal
        stackView.spacing = Spacing.base01
        
        return stackView
    }()
    
    private lazy var leftLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale500())
        
        return label
    }()
    
    private lazy var rightLabel: UILabel = {
        let label = UILabel()
        label.contentMode = .right
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale500())
        
        return label
    }()
    
    private lazy var infoButton: UIButton = {
        let button = UIButton(type: .infoLight)
        button.isHidden = true
        button.isUserInteractionEnabled = false
        
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
    
    func buildViewHierarchy() {
        stackView.addArrangedSubviews(leftLabel, rightLabel, infoButton)
        
        addSubview(stackView)
    }
    
    func setupConstraints() {
        leftLabel.setContentHuggingPriority(.defaultLow, for: .horizontal)
        rightLabel.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        
        stackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        infoButton.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.button)
        }
    }
    
    func setupView(left: String, right: String) {
        leftLabel.text = left
        rightLabel.text = right
    }
    
    func setupButton(enable: Bool) {
        infoButton.isHidden = !enable
    }
}
