import UI
import SnapKit

final class CardPaymentDisclaimerView: UIView, ViewConfiguration {
    private lazy var backgroundView: UIView = {
        let view = UIView()
        view.cornerRadius = .medium
        view.backgroundColor = .grayscale050()
        
        return view
    }()
    
    private lazy var disclaimerLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale500())
        
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
    
    func buildViewHierarchy() {
        backgroundView.addSubview(disclaimerLabel)
        addSubview(backgroundView)
    }
    
    func setupConstraints() {
        disclaimerLabel.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base02)
        }
        
        backgroundView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
        }
    }
    
    func setupDisclaimer(with text: String) {
        disclaimerLabel.text = text
    }
}
