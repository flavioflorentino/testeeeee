import Foundation

struct CardPaymentAccessory {
    let total: Double
    let tax: Double
    let disclaimer: String?
}
