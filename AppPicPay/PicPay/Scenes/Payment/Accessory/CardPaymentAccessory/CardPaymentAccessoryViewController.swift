import CorePayment
import SnapKit
import UI
import UIKit

final class CardPaymentAccessoryViewController: ViewController<CardPaymentAccessoryViewModelInputs, UIView>, PaymentAccessoryInput {
    private var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        
        return stackView
    }()
    
    private lazy var totalValueView: CardPaymentTwoLabelView = {
        let view = CardPaymentTwoLabelView()
        view.setupButton(enable: false)
        
        return view
    }()
    
    private lazy var taxValueView: CardPaymentTwoLabelView = {
        let view = CardPaymentTwoLabelView()
        view.setupButton(enable: true)
        
        return view
    }()
 
    private lazy var disclaimerView: CardPaymentDisclaimerView = {
        let view = CardPaymentDisclaimerView()
        view.isHidden = true
        
        return view
    }()
    
    weak var delegate: PaymentAccessoryOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.updateView()
    }
 
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapTax))
        taxValueView.addGestureRecognizer(tap)
    }
    
    override func buildViewHierarchy() {
        stackView.addArrangedSubview(totalValueView)
        stackView.addArrangedSubview(taxValueView)
        stackView.addArrangedSubview(disclaimerView)
        
        view.addSubview(stackView)
    }
    
    override func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.lessThanOrEqualToSuperview().inset(Spacing.base02)
        }
    }
    
    func changePaymentMethod(method: PaymentMethod) {
        viewModel.changePaymentMethod(method: method)
    }
}

@objc
private extension CardPaymentAccessoryViewController {
    func didTapTax() {
        viewModel.tapTax()
    }
}

// MARK: View Model Outputs
extension CardPaymentAccessoryViewController: CardPaymentAccessoryDisplay {
    func displayTotalValue(leftText: String, rightText: String) {
        totalValueView.setupView(left: leftText, right: rightText)
    }
    
    func displayTaxValue(leftText: String, rightText: String) {
        taxValueView.setupView(left: leftText, right: rightText)
    }
    
    func displayDisclaimer(text: String) {
        disclaimerView.setupDisclaimer(with: text)
    }
    
    func taxValueIsHidden(_ value: Bool) {
        taxValueView.isHidden = value
    }
    
    func disclaimerViewIsHidden(_ value: Bool) {
        disclaimerView.isHidden = value
    }
    
    func displayAlert(_ alert: Alert) {
        AlertMessage.showAlert(alert, controller: self)
    }
}
