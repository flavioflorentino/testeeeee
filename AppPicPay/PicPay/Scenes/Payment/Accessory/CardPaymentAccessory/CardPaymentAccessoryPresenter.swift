import Core
import Foundation

protocol CardPaymentAccessoryPresenting: AnyObject {
    var viewController: CardPaymentAccessoryDisplay? { get set }
    func presentView(total: Double, tax: Double, disclaimer: String?)
    func presentAlertTax(_ tax: Double)
    func showTax()
    func hiddenTax()
}

final class CardPaymentAccessoryPresenter: CardPaymentAccessoryPresenting {
    weak var viewController: CardPaymentAccessoryDisplay?
    
    func presentView(total: Double, tax: Double, disclaimer: String?) {
        setupTotalValue(total)
        setupTax(tax)
        setupDisclaimer(disclaimer)
    }
    
    func presentAlertTax(_ tax: Double) {
        let taxText = String(tax)
        let text = PaymentLocalizable.alertConvenienceTaxSubtitle.text.replacingOccurrences(of: "$tax", with: taxText)
        let alert = Alert(
            title: PaymentLocalizable.alertConvenienceTaxTitle.text,
            text: text
        )
        
        alert.buttons = [
            Button(title: PaymentLocalizable.alertAcceptButton.text, type: .cta, action: .confirm),
            Button(title: PaymentLocalizable.alertDeclineButton.text, type: .inline, action: .close)
        ]
        
        viewController?.displayAlert(alert)
    }
    
    func showTax() {
        viewController?.taxValueIsHidden(false)
    }
    
    func hiddenTax() {
        viewController?.taxValueIsHidden(true)
    }
    
    private func setupTotalValue(_ total: Double) {
        viewController?.displayTotalValue(leftText: PaymentLocalizable.billTotal.text, rightText: total.stringAmount)
    }
    
    private func setupTax(_ tax: Double) {
        viewController?.displayTaxValue(leftText: PaymentLocalizable.inconvenienceTax.text, rightText: String(tax))
    }
    
    private func setupDisclaimer(_ disclaimer: String?) {
        guard let disclaimer = disclaimer else {
            viewController?.disclaimerViewIsHidden(true)
            return
        }
        
        viewController?.displayDisclaimer(text: disclaimer)
        viewController?.disclaimerViewIsHidden(false)
    }
}
