import Foundation

enum CardPaymentAccessoryFactory {
    static func make(model: CardPaymentAccessory) -> CardPaymentAccessoryViewController {
        let presenter: CardPaymentAccessoryPresenting = CardPaymentAccessoryPresenter()
        let viewModel = CardPaymentAccessoryViewModel(model: model, presenter: presenter)
        let viewController = CardPaymentAccessoryViewController(viewModel: viewModel)

        presenter.viewController = viewController

        return viewController
    }
}
