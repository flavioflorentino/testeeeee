import CorePayment
import Foundation

protocol CardPaymentAccessoryViewModelInputs: AnyObject {
    func updateView()
    func changePaymentMethod(method: PaymentMethod)
    func tapTax()
}

final class CardPaymentAccessoryViewModel {
    private let presenter: CardPaymentAccessoryPresenting
    private let model: CardPaymentAccessory
    
    init(model: CardPaymentAccessory, presenter: CardPaymentAccessoryPresenting) {
        self.model = model
        self.presenter = presenter
    }
}

extension CardPaymentAccessoryViewModel: CardPaymentAccessoryViewModelInputs {
    func updateView() {
        presenter.presentView(total: model.total, tax: model.tax, disclaimer: model.disclaimer)
    }
    
    func changePaymentMethod(method: PaymentMethod) {
        switch method {
        case .card,
             .balanceAndCard:
            presenter.showTax()
        case .accountBalance:
            presenter.hiddenTax()
        }
    }
    
    func tapTax() {
        presenter.presentAlertTax(model.tax)
    }
}
