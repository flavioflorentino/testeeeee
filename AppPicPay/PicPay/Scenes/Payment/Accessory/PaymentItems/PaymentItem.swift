import Foundation

struct PaymentItem {
    let name: String
    let unitValue: Double
    let quantity: Int
}
