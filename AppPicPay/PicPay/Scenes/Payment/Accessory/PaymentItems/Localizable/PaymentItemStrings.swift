import Foundation

enum PaymentItemLocalizable: String, Localizable {
    case itemCellTitle
    
    var key: String {
        self.rawValue
    }
    var file: LocalizableFile {
        .paymentItem
    }
}
