import Core

protocol PaymentItemCellPresenting {
    var title: String { get }
    var product: String { get }
}

struct PaymentItemCellPresenter: PaymentItemCellPresenting {
    var title: String
    var product: String
}
