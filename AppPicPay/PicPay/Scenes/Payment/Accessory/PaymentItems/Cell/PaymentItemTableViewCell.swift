import UI
import UIKit

private extension PaymentItemTableViewCell.Layout {
    enum Font {
        static let leftLabel = UIFont.systemFont(ofSize: 12, weight: .regular)
        static let rightLabel = UIFont.systemFont(ofSize: 14, weight: .semibold)
    }
    
    enum Margin {
        static let top: CGFloat = 16
        static let bottom: CGFloat = 16
    }
}

final class PaymentItemTableViewCell: UITableViewCell {
    fileprivate enum Layout {}
    
    static let identifier = String(describing: PaymentItemTableViewCell.self)
    
    var presenter: PaymentItemCellPresenting? {
        didSet {
            update()
        }
    }
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        return stackView
    }()
    
    private lazy var leftLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.leftLabel
        label.textColor = Palette.ppColorGrayscale500.color
        return label
    }()
    
    private lazy var rightLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.rightLabel
        label.textColor = Palette.ppColorGrayscale500.color
        return label
    }()
        
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func update() {
        guard let presenter = presenter else {
            return
        }
        
        leftLabel.text = presenter.title
        rightLabel.text = presenter.product
    }
}

extension PaymentItemTableViewCell: ViewConfiguration {
    public func buildViewHierarchy() {
        contentView.addSubview(stackView)
        stackView.addArrangedSubview(leftLabel)
        stackView.addArrangedSubview(rightLabel)
    }
    
    public func setupConstraints() {
        stackView.layout {
            $0.leading == contentView.layoutMarginsGuide.leadingAnchor
            $0.trailing == contentView.layoutMarginsGuide.trailingAnchor
            $0.top == contentView.topAnchor + Layout.Margin.top
            $0.bottom == contentView.bottomAnchor - Layout.Margin.bottom
        }
    }
    
    public func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
        selectionStyle = .none
    }
}
