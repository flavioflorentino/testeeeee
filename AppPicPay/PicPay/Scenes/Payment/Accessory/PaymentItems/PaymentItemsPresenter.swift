import Core
import Foundation
import UI

protocol PaymentItemsPresenting: AnyObject {
    var viewController: PaymentItemsDisplay? { get set }
    func display(items: [PaymentItem])
}

final class PaymentItemsPresenter: PaymentItemsPresenting {
    weak var viewController: PaymentItemsDisplay?
    
    func display(items: [PaymentItem]) {
        let items = items.map {
            PaymentItemCellPresenter(title: PaymentItemLocalizable.itemCellTitle.text, product: $0.name)
        }
        viewController?.displayItems(sections: [Section(items: items)])
    }
}
