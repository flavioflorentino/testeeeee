import UI

protocol PaymentItemsDisplay: AnyObject {
    func displayItems(sections: [Section<String, PaymentItemCellPresenting>])
}
