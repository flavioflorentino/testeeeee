import Foundation

enum PaymentItemsFactory {
    static func make(paymentItems: [PaymentItem]) -> PaymentItemsViewController {
        let presenter: PaymentItemsPresenting = PaymentItemsPresenter()
        let viewModel = PaymentItemsViewModel(presenter: presenter, items: paymentItems)
        let viewController = PaymentItemsViewController(viewModel: viewModel)

        presenter.viewController = viewController

        return viewController
    }
}
