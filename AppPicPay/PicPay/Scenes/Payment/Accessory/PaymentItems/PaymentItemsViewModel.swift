import Foundation

protocol PaymentItemsViewModelInputs: AnyObject {
    func fetchPaymentItems()
}

final class PaymentItemsViewModel {
    private let presenter: PaymentItemsPresenting
    private let items: [PaymentItem]

    init(presenter: PaymentItemsPresenting, items: [PaymentItem]) {
        self.presenter = presenter
        self.items = items
    }
}

extension PaymentItemsViewModel: PaymentItemsViewModelInputs {
    func fetchPaymentItems() {
        presenter.display(items: items)
    }
}
