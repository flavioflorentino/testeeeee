import CorePayment
import UI
import UIKit

final class PaymentItemsViewController: ViewController<PaymentItemsViewModelInputs, UITableView>, PaymentAccessoryInput {
    weak var delegate: PaymentAccessoryOutput?
    private var dataSource: TableViewHandler<String, PaymentItemCellPresenting, PaymentItemTableViewCell>?

    override func viewDidLoad() {
        super.viewDidLoad()
        rootView.register(PaymentItemTableViewCell.self, forCellReuseIdentifier: PaymentItemTableViewCell.identifier)
        viewModel.fetchPaymentItems()
    }
    
    override func configureViews() {
        rootView.separatorStyle = .none
    }
}

// MARK: View Model Outputs
extension PaymentItemsViewController: PaymentItemsDisplay {
    public func displayItems(sections: [Section<String, PaymentItemCellPresenting>]) {
        dataSource = TableViewHandler(
            data: sections,
            cellType: PaymentItemTableViewCell.self,
            configureCell: { _, model, cell in
                cell.presenter = model
            }
        )
        
        rootView.dataSource = dataSource
        rootView.reloadData()
    }
}
