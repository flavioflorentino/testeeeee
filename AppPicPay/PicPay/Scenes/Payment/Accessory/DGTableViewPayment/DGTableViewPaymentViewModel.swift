import Foundation

protocol DGTableViewPaymentViewModelInputs: AnyObject {
    func viewDidLoad()
    func openMarkdownUrl(_ url: URL)
    func cellChangedHeight(index: IndexPath)
    func openInformationButton(index: IndexPath)
}

final class DGTableViewPaymentViewModel {
    private let presenter: DGTableViewPaymentPresenting
    private var model: [ItensPayment]
    
    init(model: [ItensPayment], presenter: DGTableViewPaymentPresenting) {
        self.presenter = presenter
        self.model = model
    }
}

extension DGTableViewPaymentViewModel: DGTableViewPaymentViewModelInputs {
    func viewDidLoad() {
        presenter.updateItensPayment(model)
    }
    
    func openMarkdownUrl(_ url: URL) {
        presenter.didNextStep(action: .openWebView(url))
    }
    
    func openInformationButton(index: IndexPath) {
        let row = index.row
        guard
            model.indices.contains(row),
            let cell = model[row] as? PaymentTwoLabelsCell,
            let alert = cell.popup
            else {
                return
        }
        
        presenter.didNextStep(action: .openSurchargeConfirmation(alert))
    }
    
    func cellChangedHeight(index: IndexPath) {
        let row = index.row
        guard
            model.indices.contains(row),
            var cell = model[row] as? PaymentMarkdownCell
            else {
                return
        }
        
        cell.needLoad = false
        model[row] = cell
        presenter.updateItensPayment(model)
    }
}
