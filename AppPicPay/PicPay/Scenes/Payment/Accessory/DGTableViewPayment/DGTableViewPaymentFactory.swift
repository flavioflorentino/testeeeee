import Foundation

enum DGTableViewPaymentFactory {
    static func make(model: [ItensPayment]) -> DGTableViewPaymentViewController {
        let coordinator: DGTableViewPaymentCoordinating = DGTableViewPaymentCoordinator()
        let presenter: DGTableViewPaymentPresenting = DGTableViewPaymentPresenter(coordinator: coordinator)
        let viewModel = DGTableViewPaymentViewModel(model: model, presenter: presenter)
        let viewController = DGTableViewPaymentViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
