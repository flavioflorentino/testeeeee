import UIKit

protocol DGTableViewPaymentDisplay: AnyObject {
    func displayItensPayment(_ data: [ItensPayment])
}
