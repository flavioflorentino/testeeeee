import Foundation

struct PaymentMarkdownCell: ItensPayment {
    var cellType: ItensPaymentType = .markdown
    let markdown: String
    var needLoad: Bool = true
}
