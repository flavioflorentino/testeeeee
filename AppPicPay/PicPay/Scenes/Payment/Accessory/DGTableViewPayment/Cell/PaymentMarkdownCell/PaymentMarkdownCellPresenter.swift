import Foundation

protocol PaymentMarkdownCellPresenterInputs {
    func setup(model: ItensPayment)
}

protocol PaymentMarkdownCellPresenterOutputs: AnyObject {
    func loadMarkdown(_ markdown: String)
}

protocol PaymentMarkdownCellPresenterProtocol: AnyObject {
    var inputs: PaymentMarkdownCellPresenterInputs { get }
    var outputs: PaymentMarkdownCellPresenterOutputs? { get set }
}

final class PaymentMarkdownCellPresenter: PaymentMarkdownCellPresenterProtocol, PaymentMarkdownCellPresenterInputs {
    var inputs: PaymentMarkdownCellPresenterInputs { self }
    weak var outputs: PaymentMarkdownCellPresenterOutputs?
    
    func setup(model: ItensPayment) {
        guard let model = model as? PaymentMarkdownCell, model.needLoad else {
            return
        }
        
        outputs?.loadMarkdown(model.markdown)
    }
}
