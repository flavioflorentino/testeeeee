import UI
import UIKit

protocol PaymentMarkdownCellViewDelegate: AnyObject {
    func didTapMarkdownView(url: URL)
    func changeHeight(index: IndexPath)
}

final class PaymentMarkdownCellView: UITableViewCell, PaymentCell {
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale200.color
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 4.0
        view.clipsToBounds = true
        
        return view
    }()
    
    private var index: IndexPath?
    
    //  💩 This view sucks we have to refactor it and stop using MarkdownView 💩
    private lazy var markdownView: MarkdownView = {
        let markdownView = MarkdownView()
        markdownView.isScrollEnabled = false
        markdownView.translatesAutoresizingMaskIntoConstraints = false
        
        return markdownView
    }()
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activity = UIActivityIndicatorView(style: .gray)
        activity.hidesWhenStopped = true
        activity.translatesAutoresizingMaskIntoConstraints = false
        
        return activity
    }()
    
    private lazy var presenter: PaymentMarkdownCellPresenterProtocol = {
        let presenter = PaymentMarkdownCellPresenter()
        presenter.outputs = self
        
        return presenter
    }()
    
    private var heightConstraint: NSLayoutConstraint?
    weak var delegate: PaymentMarkdownCellViewDelegate?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        configureMarkdownView()
        backgroundColor = .clear
        selectionStyle = .none
    }
    
    func buildViewHierarchy() {
        containerView.addSubview(markdownView)
        containerView.addSubview(activityIndicator)
        contentView.addSubview(containerView)
    }
    
    func setupConstraints() {
        heightConstraint = markdownView.heightAnchor.constraint(equalToConstant: 70)
        heightConstraint?.isActive = true
        
        markdownView.layout {
            $0.top == containerView.topAnchor + Spacing.base02
            $0.leading == containerView.leadingAnchor
            $0.trailing == containerView.trailingAnchor
            $0.bottom == containerView.bottomAnchor - Spacing.base02
        }
        
        activityIndicator.layout {
            $0.centerY == containerView.centerYAnchor
            $0.centerX == containerView.centerXAnchor
        }
        
        containerView.layout {
            $0.top == contentView.topAnchor + Spacing.base02
            $0.leading == contentView.leadingAnchor + Spacing.base02
            $0.trailing == contentView.trailingAnchor - Spacing.base02
            $0.bottom == contentView.bottomAnchor - Spacing.base02
        }
    }
    
    func setupView(model: ItensPayment, index: IndexPath) {
        self.index = index
        presenter.inputs.setup(model: model)
    }
    
    private func configureMarkdownView() {
        markdownView.onRendered = { [weak self] height in
            self?.activityIndicator.stopAnimating()
            self?.changeHeight(height)
        }
        
        markdownView.onTouchLink = { [weak self] urlRequest in
            guard let url = urlRequest.url else {
                return false
            }
            
            self?.delegate?.didTapMarkdownView(url: url)
            return true
        }
    }
    
    private func changeHeight(_ height: CGFloat) {
        guard let index = index else {
            return
        }
        heightConstraint?.constant = height
        layoutIfNeeded()
        delegate?.changeHeight(index: index)
    }
}

extension PaymentMarkdownCellView: PaymentMarkdownCellPresenterOutputs {
    func loadMarkdown(_ markdown: String) {
        activityIndicator.startAnimating()
        markdownView.load(markdown: markdown)
    }
}
