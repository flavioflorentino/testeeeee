import Foundation

struct PaymentDetailCell: ItensPayment {
    var cellType: ItensPaymentType = .detail
    let detail: String
}
