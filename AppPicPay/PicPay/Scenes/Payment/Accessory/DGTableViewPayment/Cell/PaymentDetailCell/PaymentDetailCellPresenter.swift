import Foundation

protocol PaymentDetailCellPresenterInputs {
    func setup(model: ItensPayment)
}

protocol PaymentDetailCellPresenterOutputs: AnyObject {
    func displayDetail(text: String)
}

protocol PaymentDetailCellPresenterProtocol: AnyObject {
    var inputs: PaymentDetailCellPresenterInputs { get }
    var outputs: PaymentDetailCellPresenterOutputs? { get set }
}

final class PaymentDetailCellPresenter: PaymentDetailCellPresenterProtocol, PaymentDetailCellPresenterInputs {
    var inputs: PaymentDetailCellPresenterInputs { self }
    weak var outputs: PaymentDetailCellPresenterOutputs?
    
    func setup(model: ItensPayment) {
        guard let model = model as? PaymentDetailCell else {
            return
        }
        
        outputs?.displayDetail(text: model.detail)
    }
}
