import UI
import UIKit

final class PaymentDetailCellView: UITableViewCell, PaymentCell {
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false

        return label
    }()
    
    private lazy var presenter: PaymentDetailCellPresenterProtocol = {
        let presenter = PaymentDetailCellPresenter()
        presenter.outputs = self
        
        return presenter
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        backgroundColor = Palette.white.color
        selectionStyle = .none
    }
    
    func buildViewHierarchy() {
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(descriptionLabel)
    }
    
    func setupConstraints() {
        descriptionLabel.layout {
            $0.top == contentView.topAnchor + Spacing.base02
            $0.leading == contentView.leadingAnchor + Spacing.base02
            $0.trailing == contentView.trailingAnchor - Spacing.base02
            $0.bottom == contentView.bottomAnchor - Spacing.base02
        }
    }
    
    func setupView(model: ItensPayment, index: IndexPath) {
        presenter.inputs.setup(model: model)
    }
}

extension PaymentDetailCellView: PaymentDetailCellPresenterOutputs {
    func displayDetail(text: String) {
        descriptionLabel.text = text
    }
}
