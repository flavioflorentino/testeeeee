import UI
import UIKit

protocol PaymentTwoLabelsCellViewDelegate: AnyObject {
    func didTapButton(index: IndexPath)
}

extension PaymentTwoLabelsCellView.Layout {
    enum Size {
        static let button: CGFloat = 24
    }
}

final class PaymentTwoLabelsCellView: UITableViewCell, PaymentCell {
    fileprivate enum Layout { }
    
    private var index: IndexPath?
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.axis = .horizontal
        stackView.spacing = Spacing.base01
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    }()
    
    private lazy var leftLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13.0)
        label.textColor = Palette.ppColorGrayscale500.color
        
        return label
    }()
    
    private lazy var rightLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13.0, weight: .semibold)
        label.textColor = Palette.ppColorGrayscale500.color
        
        return label
    }()
    
    private lazy var infoButton: UIButton = {
        let button = UIButton(type: .infoLight)
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
        
        return button
    }()
    
    private lazy var presenter: PaymentTwoLabelsCellPresenterProtocol = {
        let presenter = PaymentTwoLabelsCellPresenter()
        presenter.outputs = self
        
        return presenter
    }()
    
    weak var delegate: PaymentTwoLabelsCellViewDelegate?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        backgroundColor = Palette.white.color
        selectionStyle = .none
    }
    
    func buildViewHierarchy() {
        contentView.addSubview(stackView)
        stackView.addArrangedSubview(leftLabel)
        stackView.addArrangedSubview(rightLabel)
        stackView.addArrangedSubview(infoButton)
    }
    
    func setupConstraints() {
        leftLabel.setContentHuggingPriority(.defaultLow, for: .horizontal)
        rightLabel.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        
        stackView.layout {
            $0.top == contentView.topAnchor + Spacing.base02
            $0.leading == contentView.leadingAnchor + Spacing.base02
            $0.trailing == contentView.trailingAnchor - Spacing.base02
            $0.bottom == contentView.bottomAnchor - Spacing.base02
        }
        
        infoButton.layout {
            $0.height == Layout.Size.button
            $0.width == Layout.Size.button
        }
    }
    
    @objc
    private func didTapButton() {
        guard let index = index else {
            return
        }
        delegate?.didTapButton(index: index)
    }
    
    func setupView(model: ItensPayment, index: IndexPath) {
        self.index = index
        presenter.inputs.setup(model: model)
    }
}

extension PaymentTwoLabelsCellView: PaymentTwoLabelsCellPresenterOutputs {
    func displayTitle(text: String) {
        leftLabel.text = text
    }
    
    func displayDescription(text: String) {
        rightLabel.text = text
    }
    
    func buttonIsHidden(_ hidden: Bool) {
        infoButton.isHidden = hidden
    }
}
