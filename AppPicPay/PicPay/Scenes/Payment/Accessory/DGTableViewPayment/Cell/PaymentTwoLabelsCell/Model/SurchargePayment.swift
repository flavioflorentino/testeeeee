import Foundation

struct SurchargePayment {
    let title: String
    let description: String
    let surcharge: Double
    let surchargeAmount: Double
}
