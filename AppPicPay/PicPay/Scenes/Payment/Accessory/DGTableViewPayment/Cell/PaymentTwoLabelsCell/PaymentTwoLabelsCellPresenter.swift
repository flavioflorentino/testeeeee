import Foundation

protocol PaymentTwoLabelsCellPresenterInputs {
    func setup(model: ItensPayment)
}

protocol PaymentTwoLabelsCellPresenterOutputs: AnyObject {
    func displayTitle(text: String)
    func displayDescription(text: String)
    func buttonIsHidden(_ hidden: Bool)
}

protocol PaymentTwoLabelsCellPresenterProtocol: AnyObject {
    var inputs: PaymentTwoLabelsCellPresenterInputs { get }
    var outputs: PaymentTwoLabelsCellPresenterOutputs? { get set }
}

final class PaymentTwoLabelsCellPresenter: PaymentTwoLabelsCellPresenterProtocol, PaymentTwoLabelsCellPresenterInputs {
    var inputs: PaymentTwoLabelsCellPresenterInputs { self }
    weak var outputs: PaymentTwoLabelsCellPresenterOutputs?
    
    func setup(model: ItensPayment) {
        guard let model = model as? PaymentTwoLabelsCell else {
            return
        }
        
        let isHidden = !model.buttonIsEnable
        outputs?.displayTitle(text: model.left)
        outputs?.displayDescription(text: model.right)
        outputs?.buttonIsHidden(isHidden)
    }
}
