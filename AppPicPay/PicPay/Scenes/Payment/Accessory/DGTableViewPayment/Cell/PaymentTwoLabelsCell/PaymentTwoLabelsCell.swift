import Foundation

struct PaymentTwoLabelsCell: ItensPayment {
    var cellType: ItensPaymentType = .information
    let left: String
    let right: String
    let buttonIsEnable: Bool
    let popup: SurchargePayment?
}
