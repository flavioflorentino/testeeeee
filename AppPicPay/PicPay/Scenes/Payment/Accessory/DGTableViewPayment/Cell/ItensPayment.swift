import UI
import Foundation

enum ItensPaymentType {
    case information
    case detail
    case markdown
} 

protocol ItensPayment {
    var cellType: ItensPaymentType { get }
}

protocol PaymentCell: ViewConfiguration where Self: UITableViewCell {
    func setupView(model: ItensPayment, index: IndexPath)
}
