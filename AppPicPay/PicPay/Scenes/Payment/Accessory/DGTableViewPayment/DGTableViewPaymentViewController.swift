import CorePayment
import UI
import UIKit

final class DGTableViewPaymentViewController: ViewController<DGTableViewPaymentViewModelInputs, UIView>, PaymentAccessoryInput {
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableView.automaticDimension
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        return tableView
    }()

    private var data: [ItensPayment] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    weak var delegate: PaymentAccessoryOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.viewDidLoad()
        registerCellTableView()
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        tableView.layout {
            $0.top == view.topAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
            $0.bottom == view.bottomAnchor
        }
    }
    
    private func registerCellTableView() {
        tableView.register(
            PaymentDetailCellView.self,
            forCellReuseIdentifier: String(describing: PaymentDetailCellView.self)
        )
        tableView.register(
            PaymentTwoLabelsCellView.self,
            forCellReuseIdentifier: String(describing: PaymentTwoLabelsCellView.self)
        )
        tableView.register(
            PaymentMarkdownCellView.self,
            forCellReuseIdentifier: String(describing: PaymentMarkdownCellView.self)
        )
    }
}

extension DGTableViewPaymentViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        let model = data[row]
        
        switch model.cellType {
        case .detail:
            return createCell(type: PaymentDetailCellView.self, tableView: tableView, indexPath: indexPath, model: model)
        case .information:
            let cell = createCell(type: PaymentTwoLabelsCellView.self, tableView: tableView, indexPath: indexPath, model: model)
            cell.delegate = self
            
            return cell
        case .markdown:
            let cell = createCell(type: PaymentMarkdownCellView.self, tableView: tableView, indexPath: indexPath, model: model)
            cell.delegate = self
            
            return cell
        }
    }
    
    private func createCell<T: PaymentCell>(type: T.Type, tableView: UITableView, indexPath: IndexPath, model: ItensPayment) -> T {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: T.self), for: indexPath) as? T else {
            return T()
        }
        
        cell.setupView(model: model, index: indexPath)
        return cell
    }
}

// MARK: View Model Outputs
extension DGTableViewPaymentViewController: DGTableViewPaymentDisplay {
    func displayItensPayment(_ data: [ItensPayment]) {
        self.data = data
    }
}

extension DGTableViewPaymentViewController: PaymentTwoLabelsCellViewDelegate {
    func didTapButton(index: IndexPath) {
        viewModel.openInformationButton(index: index)
    }
}

extension DGTableViewPaymentViewController: PaymentMarkdownCellViewDelegate {    
    func didTapMarkdownView(url: URL) {
        viewModel.openMarkdownUrl(url)
    }
    
    func changeHeight(index: IndexPath) {
        viewModel.cellChangedHeight(index: index)
    }
}
