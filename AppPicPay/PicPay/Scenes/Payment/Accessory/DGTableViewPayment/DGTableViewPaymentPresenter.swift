import Core
import Foundation

protocol DGTableViewPaymentPresenting: AnyObject {
    var viewController: DGTableViewPaymentDisplay? { get set }
    func updateItensPayment(_ data: [ItensPayment])
    func didNextStep(action: DGTableViewPaymentAction)
}

final class DGTableViewPaymentPresenter: DGTableViewPaymentPresenting {
    private let coordinator: DGTableViewPaymentCoordinating
    weak var viewController: DGTableViewPaymentDisplay?

    init(coordinator: DGTableViewPaymentCoordinating) {
        self.coordinator = coordinator
    }
    
    func updateItensPayment(_ data: [ItensPayment]) {
        viewController?.displayItensPayment(data)
    }
    
    func didNextStep(action: DGTableViewPaymentAction) {
        coordinator.perform(action: action)
    }
}
