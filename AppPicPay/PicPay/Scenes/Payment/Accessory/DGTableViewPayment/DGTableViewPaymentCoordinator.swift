import UI
import UIKit

enum DGTableViewPaymentAction {
    case openWebView(URL)
    case openSurchargeConfirmation(SurchargePayment)
}

protocol DGTableViewPaymentCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: DGTableViewPaymentAction)
}

final class DGTableViewPaymentCoordinator: DGTableViewPaymentCoordinating {
    private let popupPresenter = PopupPresenter()
    weak var viewController: UIViewController?
    
    func perform(action: DGTableViewPaymentAction) {
        switch action {
        case .openWebView(let url):
            guard let viewController = viewController else {
                return
            }
            ViewsManager.presentSafariViewController(url, from: viewController)
            
        case .openSurchargeConfirmation(let model):
            guard let viewController = viewController else {
                return
            }
            
            let popup = SurchargeConfirmation()
            popup.setup(
                title: model.title,
                description: model.description,
                fees: [
                    (surcharge: "\(model.surcharge)", surchargeAmount: model.surchargeAmount.toCurrencyString() ?? "")
                ],
                shouldShowCancel: false
            )
            
            popupPresenter.showPopup(popup, fromViewController: viewController)
        }
    }
}
