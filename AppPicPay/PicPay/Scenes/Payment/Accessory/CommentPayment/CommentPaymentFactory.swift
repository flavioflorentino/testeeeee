import Foundation

enum CommentPaymentFactory {
    static func make(commentPlaceholder: String = PaymentLocalizable.commentPlaceholder.text) -> CommentPaymentViewController {
        let presenter: CommentPaymentPresenting = CommentPaymentPresenter(commentPlaceholder: commentPlaceholder)
        let viewModel = CommentPaymentViewModel(presenter: presenter)
        let viewController = CommentPaymentViewController(viewModel: viewModel)

        presenter.viewController = viewController

        return viewController
    }
}
