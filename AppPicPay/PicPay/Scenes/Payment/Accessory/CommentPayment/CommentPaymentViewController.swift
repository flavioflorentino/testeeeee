import CorePayment
import UI
import UIKit

final class CommentPaymentViewController: ViewController<CommentPaymentViewModelInputs, UIView>, PaymentAccessoryInput {
    private enum Layout {
        static let marginLeadingTrailing: CGFloat = 12
    }
    
    private lazy var textView: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: 15)
        textView.isScrollEnabled = false
        textView.backgroundColor = .clear
        textView.autocorrectionType = .no
        textView.delegate = self
        textView.translatesAutoresizingMaskIntoConstraints = false
        
        return textView
    }()
    
    private var message: String = "" {
        didSet {
            delegate?.accessoryUpdateMessage(message: message)
        }
    }
    
    weak var delegate: PaymentAccessoryOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(textView)
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.activate([
            textView.topAnchor.constraint(equalTo: view.topAnchor),
            textView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Layout.marginLeadingTrailing),
            textView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Layout.marginLeadingTrailing),
            textView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}

// MARK: ContainerPaymentDisplay
extension CommentPaymentViewController: CommentPaymentDisplay {
    func updateTextView(message: String) {
        textView.text = message
        self.message = message
    }
    
    func updatePaceholder(message: String) {
        textView.text = message
    }
    
    func updateColor(color: UIColor) {
        textView.textColor = color
    }
    
    func deleteBackward() {
        textView.deleteBackward()
    }
}

extension CommentPaymentViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        guard let text = textView.text else {
            return
        }
               
        viewModel.inputValue(message: text)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        viewModel.didBeginEditing()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        viewModel.didEndEditing()
    }
}
