import UIKit

protocol CommentPaymentDisplay: AnyObject {
    func updateTextView(message: String)
    func updatePaceholder(message: String)
    func updateColor(color: UIColor)
    func deleteBackward()
}
