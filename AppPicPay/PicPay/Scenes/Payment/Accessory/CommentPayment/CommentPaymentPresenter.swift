import Core
import Foundation
import UI

protocol CommentPaymentPresenting: AnyObject {
    var viewController: CommentPaymentDisplay? { get set }
    func displayComment()
    func inputValue(message: String)
    func didBeginEditing()
    func didEndEditing()
}

final class CommentPaymentPresenter: CommentPaymentPresenting {
    private let limitCharacters = 100
    private var countCharacters = 0
    private let commentPlaceholder: String
    weak var viewController: CommentPaymentDisplay?
    
    init(commentPlaceholder: String) {
        self.commentPlaceholder = commentPlaceholder
    }
    
    func displayComment() {
        displayPlaceholder()
    }
    
    func inputValue(message: String) {
        guard reachedCharacterLimit(message: message) else {
            deleteLastCharacter()
            return
        }
        
        updateInputMessage(message: message)
    }
    
    func didBeginEditing() {
        guard hasZeroCharacters() else {
            return
        }
        
        prepareUserInput()
    }
    
    func didEndEditing() {
        guard hasZeroCharacters() else {
            return
        }
        
        displayPlaceholder()
    }
    
    private func hasZeroCharacters() -> Bool {
        countCharacters == .zero
    }
    
    private func updateInputMessage(message: String) {
        countCharacters = message.count
        viewController?.updateTextView(message: message)
    }
    
    private func deleteLastCharacter() {
        viewController?.deleteBackward()
    }
    
    private func reachedCharacterLimit(message: String) -> Bool {
        message.count <= limitCharacters
    }
    
    private func prepareUserInput() {
        viewController?.updatePaceholder(message: String())
        viewController?.updateColor(color: Palette.ppColorGrayscale500.color)
    }
    
    private func displayPlaceholder() {
        viewController?.updateTextView(message: String())
        viewController?.updatePaceholder(message: commentPlaceholder)
        viewController?.updateColor(color: Palette.ppColorGrayscale300.color)
    }
}
