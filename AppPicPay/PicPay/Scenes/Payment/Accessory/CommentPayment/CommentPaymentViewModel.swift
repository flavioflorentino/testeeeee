import Foundation

protocol CommentPaymentViewModelInputs: AnyObject {
    func viewDidLoad()
    func inputValue(message: String)
    func didBeginEditing()
    func didEndEditing()
}

final class CommentPaymentViewModel {
    private let presenter: CommentPaymentPresenting

    init(presenter: CommentPaymentPresenting) {
        self.presenter = presenter
    }
}

extension CommentPaymentViewModel: CommentPaymentViewModelInputs {
    func viewDidLoad() {
        presenter.displayComment()
    }
    
    func inputValue(message: String) {
        presenter.inputValue(message: message)
    }
    
    func didBeginEditing() {
        presenter.didBeginEditing()
    }
    
    func didEndEditing() {
        presenter.didEndEditing()
    }
}
