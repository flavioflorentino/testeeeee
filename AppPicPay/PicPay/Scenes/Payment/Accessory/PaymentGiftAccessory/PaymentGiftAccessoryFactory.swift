import Foundation

enum PaymentGiftAccessoryFactory {
    static func make(model: [PaymentGift], analytics: PaymentGiftAcessoryAnalytics? = nil) -> PaymentGiftAccessoryViewController {
        let presenter: PaymentGiftAccessoryPresenting = PaymentGiftAccessoryPresenter()
        let viewModel = PaymentGiftAccessoryViewModel(model: model, presenter: presenter, analytics: analytics)
        let viewController = PaymentGiftAccessoryViewController(viewModel: viewModel)
        
        presenter.viewController = viewController

        return viewController
    }
}
