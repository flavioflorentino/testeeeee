protocol PaymentGiftAccessoryViewModelInputs: AnyObject {
    func updateViewWithDefaultSelection()
    func didTapCell(at indexPath: IndexPath)
}

final class PaymentGiftAccessoryViewModel {
    private let presenter: PaymentGiftAccessoryPresenting
    private var model: [PaymentGift]
    private let analytics: PaymentGiftAcessoryAnalytics?
    
    private var defaultGift: PaymentGift? {
        model.first { $0.isDefaultSelection }
    }
    
    private var cheapestGift: PaymentGift? {
        model.min(by: { $0.value < $1.value })
    }
    
    init(model: [PaymentGift], presenter: PaymentGiftAccessoryPresenting, analytics: PaymentGiftAcessoryAnalytics?) {
        self.model = model
        self.presenter = presenter
        self.analytics = analytics
    }
}

extension PaymentGiftAccessoryViewModel: PaymentGiftAccessoryViewModelInputs {
    func updateViewWithDefaultSelection() {
        presenter.updateList(data: model, selectedGift: defaultGift ?? cheapestGift)
    }
    
    func didTapCell(at indexPath: IndexPath) {
        guard model.indices.contains(indexPath.row) else {
            return
        }
        let selectedGift = model[indexPath.row]
        analytics?.didSelectGift(selectedGift)
        
        presenter.updateList(data: model, selectedGift: selectedGift)
    }
}
