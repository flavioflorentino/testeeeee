protocol PaymentGiftAccessoryPresenting: AnyObject {
    var viewController: PaymentGiftAccessoryDisplay? { get set }
    func updateList(data: [PaymentGift], selectedGift: PaymentGift?)
}

final class PaymentGiftAccessoryPresenter: PaymentGiftAccessoryPresenting {
    weak var viewController: PaymentGiftAccessoryDisplay?
    
    func updateList(data: [PaymentGift], selectedGift: PaymentGift?) {
        let cells = data.map {
            PaymentGiftCell(imageGift: $0.imageGift, value: $0.value, isActive: $0.id == selectedGift?.id)
        }
        viewController?.reloadList(model: cells)
        
        guard let selectedGift = selectedGift else {
            return
        }
        viewController?.updateMessage(message: selectedGift.description)
        viewController?.updateValue(value: selectedGift.value)
    }
}
