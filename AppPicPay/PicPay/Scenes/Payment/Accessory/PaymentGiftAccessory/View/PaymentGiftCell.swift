import Foundation

struct PaymentGiftCell {
    let imageGift: ImageGift
    let value: Double
    let isActive: Bool
}
