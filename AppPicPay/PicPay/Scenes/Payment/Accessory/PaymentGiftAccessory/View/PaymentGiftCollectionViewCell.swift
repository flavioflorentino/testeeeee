import UI
import UIKit

extension PaymentGiftCollectionViewCell.Layout {
    enum Size {
        static let checkImage: CGFloat = 14.0
    }
    
    enum Font {
        static let image = UIFont.systemFont(ofSize: 35)
        static let valueRegular = UIFont.systemFont(ofSize: 14, weight: .regular)
        static let valueSemibold = UIFont.systemFont(ofSize: 14, weight: .semibold)
    }
    
    enum Margin {
        static let margin: CGFloat = 5.0
        static let valueTop: CGFloat = 12.0
        static let valueLeadingTrailing: CGFloat = 2.0
        static let valueBottom: CGFloat = 6.0
    }
}

final class PaymentGiftCollectionViewCell: UICollectionViewCell, ViewConfiguration {
    fileprivate enum Layout { }
    
    private let checkImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.image = Assets.Icons.check14Px.image
        image.isHidden = true
        
        return image
    }()
    
    private lazy var imageLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.image
        label.textAlignment = .center
        
        return label
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.valueRegular
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        
        return label
    }()
    
    private lazy var presenter: PaymentGiftCellPresenterProtocol = {
        let presenter = PaymentGiftCellPresenter()
        presenter.view = self
        
        return presenter
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        contentView.backgroundColor = Palette.ppColorGrayscale000.color
        contentView.layer.cornerRadius = Spacing.base00
    }
    
    func buildViewHierarchy() {
        contentView.addSubview(imageLabel)
        contentView.addSubview(imageView)
        contentView.addSubview(valueLabel)
        contentView.addSubview(checkImage)
    }
    
    func setupConstraints() {
        checkImage.layout {
            $0.top == contentView.topAnchor + Layout.Margin.margin
            $0.trailing == contentView.trailingAnchor - Layout.Margin.margin
            $0.width == Layout.Size.checkImage
            $0.height == Layout.Size.checkImage
        }
        
        imageLabel.layout {
            $0.top == contentView.topAnchor + Layout.Margin.margin
            $0.centerX == contentView.centerXAnchor
            $0.trailing == checkImage.leadingAnchor - Layout.Margin.valueLeadingTrailing
        }
        
        imageView.layout {
            $0.leading == contentView.leadingAnchor + Spacing.base01
            $0.trailing == contentView.trailingAnchor - Spacing.base01
            $0.top == contentView.topAnchor + Spacing.base01
            $0.bottom == valueLabel.topAnchor - Spacing.base01
        }
        
        valueLabel.layout {
            $0.top == imageLabel.bottomAnchor + Layout.Margin.valueTop
            $0.leading == contentView.leadingAnchor + Layout.Margin.valueLeadingTrailing
            $0.trailing == contentView.trailingAnchor - Layout.Margin.valueLeadingTrailing
            $0.bottom == contentView.bottomAnchor - Layout.Margin.valueBottom
        }
    }
    
    func configureView(with model: PaymentGiftCell) {
        presenter.inputs.configureView(model: model)
    }
}

extension PaymentGiftCollectionViewCell: PaymentGiftCellDisplay {
    func displaySelectionState(isCheckHidden: Bool, font: UIFont, textColor: UIColor, backgroundColor: UIColor) {
        checkImage.isHidden = isCheckHidden
        valueLabel.font = font
        valueLabel.textColor = textColor
        contentView.backgroundColor = backgroundColor
    }
    
    func displayImageLabel(text: String) {
        imageLabel.text = text
        imageView.isHidden = true
        imageLabel.isHidden = false
    }
    
    func displayImageView(url: URL) {
        imageView.setImage(url: url, placeholder: Assets.NewGeneration.circularPlaceholder.image)
        imageView.isHidden = false
        imageLabel.isHidden = true
    }
    
    func displayValueLabel(value: String) {
        valueLabel.text = value
    }
}
