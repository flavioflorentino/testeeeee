import UI
import CoreLegacy

protocol PaymentGiftCellPresenterInputs {
    func configureView(model: PaymentGiftCell)
}

protocol PaymentGiftCellDisplay: AnyObject {
    func displayImageLabel(text: String)
    func displayImageView(url: URL)
    func displayValueLabel(value: String)
    func displaySelectionState(isCheckHidden: Bool, font: UIFont, textColor: UIColor, backgroundColor: UIColor)
}

protocol PaymentGiftCellPresenterProtocol: AnyObject {
    var inputs: PaymentGiftCellPresenterInputs { get }
    var view: PaymentGiftCellDisplay? { get set }
}

final class PaymentGiftCellPresenter: PaymentGiftCellPresenterProtocol, PaymentGiftCellPresenterInputs {
    var inputs: PaymentGiftCellPresenterInputs { self }
    weak var view: PaymentGiftCellDisplay?
    
    func configureView(model: PaymentGiftCell) {
        setupImage(model.imageGift)
        setupValue(model.value)
        setupIsActive(model.isActive)
    }
    
    private func setupImage(_ image: ImageGift) {
        switch image {
        case .emoji(let emojiString):
            view?.displayImageLabel(text: emojiString)
        case .url(let imageUrl):
            view?.displayImageView(url: imageUrl)
        }
    }
    
    private func setupValue(_ value: Double) {
        guard let value = CurrencyFormatter.compactBrazillianRealString(from: NSNumber(value: value)) else {
            return
        }
        
        view?.displayValueLabel(value: value)
    }
    
    private func setupIsActive(_ isActive: Bool) {
        let isCheckHidden = !isActive
        let font = UIFont.systemFont(ofSize: 14, weight: isActive ? .semibold : .regular)
        let textColor = isActive ? Colors.branding600.color : Colors.grayscale700.color
        let backgroundColor = isActive ? Colors.grayscale050.color : .clear
        view?.displaySelectionState(
            isCheckHidden: isCheckHidden,
            font: font,
            textColor: textColor,
            backgroundColor: backgroundColor
        )
    }
}
