import Foundation

enum ImageGift {
    case url(URL)
    case emoji(String)
}

struct PaymentGift {
    let id: String
    let imageGift: ImageGift
    let description: String
    let value: Double
    let isDefaultSelection: Bool
}
