import CorePayment
import UI
import UIKit

extension PaymentGiftAccessoryViewController.Layout {
    enum Size {
        static let cellWidth = 90
        static let cellHeight = 90
        static let collectionHeight: CGFloat = 120
        static let screenWidth: CGFloat = UIScreen.main.bounds.width
    }
    
    enum Space {
        static let topBottomCollectionView: CGFloat = Size.screenWidth <= 320.0 ? Spacing.none: Spacing.base02
        static let minInteritemSpacing: CGFloat = Spacing.base01
    }
}

final class PaymentGiftAccessoryViewController: ViewController<PaymentGiftAccessoryViewModelInputs, UIView>, PaymentAccessoryInput {
    fileprivate enum Layout { }
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = Layout.Space.minInteritemSpacing
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.alwaysBounceHorizontal = true
        collectionView.backgroundColor = .clear
        collectionView.delegate = self
        
        return collectionView
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = Palette.ppColorGrayscale500.color
        label.textAlignment = .center
        label.numberOfLines = 2
        
        return label
    }()
    
    private var dataHandler: CollectionViewHandler<PaymentGiftCell, PaymentGiftCollectionViewCell>?
    
    private var itemsCount: Int = 0
    
    weak var delegate: PaymentAccessoryOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.updateViewWithDefaultSelection()
    }
 
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        collectionView.register(
            PaymentGiftCollectionViewCell.self,
            forCellWithReuseIdentifier: String(describing: PaymentGiftCollectionViewCell.self)
        )
    }
    
    override func buildViewHierarchy() {
        view.addSubview(collectionView)
        view.addSubview(descriptionLabel)
    }
        
    override func setupConstraints() {
        collectionView.layout {
            $0.top == view.topAnchor + Layout.Space.topBottomCollectionView
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
            $0.height == Layout.Size.collectionHeight
        }
        
        descriptionLabel.layout {
            $0.top == collectionView.bottomAnchor + Layout.Space.topBottomCollectionView
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
            $0.bottom == view.bottomAnchor - Spacing.base02
        }
    }
}

extension PaymentGiftAccessoryViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        viewModel.didTapCell(at: indexPath)
    }
}

extension PaymentGiftAccessoryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: Layout.Size.cellWidth, height: Layout.Size.cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let totalCellWidth = CGFloat(Layout.Size.cellWidth * itemsCount)
        let totalInteritemSpacing = Layout.Space.minInteritemSpacing * CGFloat(itemsCount - 1)
        let totalContentWidth = totalCellWidth + totalInteritemSpacing
        
        let collectionViewWidth = view.frame.width
        
        let inset = (collectionViewWidth - totalCellWidth - totalInteritemSpacing) / 2

        return totalContentWidth >= collectionViewWidth ? .zero : UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
    }
}

// MARK: View Model Outputs
extension PaymentGiftAccessoryViewController: PaymentGiftAccessoryDisplay {
    func reloadList(model: [PaymentGiftCell]) {
        dataHandler = CollectionViewHandler(data: model, cellType: PaymentGiftCollectionViewCell.self, configureCell: { _, model, cell in
            cell.configureView(with: model)
        })
        
        collectionView.dataSource = dataHandler
        collectionView.reloadData()
        
        self.itemsCount = model.count
    }
    
    func updateValue(value: Double) {
        delegate?.accessoryUpdateValue(value: value)
    }
    
    func updateMessage(message: String) {
        descriptionLabel.text = message
        delegate?.accessoryUpdateMessage(message: message)
    }
}
