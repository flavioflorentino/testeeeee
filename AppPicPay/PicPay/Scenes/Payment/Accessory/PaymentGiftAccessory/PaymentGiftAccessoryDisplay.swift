protocol PaymentGiftAccessoryDisplay: AnyObject {
    func reloadList(model: [PaymentGiftCell])
    func updateValue(value: Double)
    func updateMessage(message: String)
}
