import Foundation

enum PaymentLocalizable: String, Localizable {
    case pay
    case schedule
    case tryAgain
    case okUnderstood
        
    case errorInstallmentZero
    case errorInstallmentCard
    case errorInstallmentP2P
    case errorValue
    case valuePayment
    case commentPlaceholder
    case installment
    case balanceDisable
    case order
    case newInstallmentButtonTitle
    case installmentIn
    case paymentMethods
    case paymentMethod
    case privacyPublic
    case privacyPrivate
    case buttonPrivacyPublic
    case buttonPrivacyPrivate
    case selectPrivacyTitle
    case selectPrivacyDesc
    case askPrivacyTitle
    case askPrivacyDesc
    case buttonYes
    case buttonNo
    case buttonCancel
    
    case balancePaymentMethodDescription

    case changeUnavailable
    case cantPayOtherWayWithPIX
    case pixPrivateReturnTitle
    case pixPrivateReturnDescription
    
    case alertConvenienceTaxTitle
    case alertConvenienceTaxSubtitle
    case alertAcceptButton
    case alertDeclineButton
    case inconvenienceTax
    case billTotal
    
    case alertInstallmentForbiddenValue
    case alertInstallmentForbidden
    case alertInstallmentForbiddenByPIX
    case alertInstallmentAvailableForCredit
    case alertInstallmentInsertValueAndWrongPaymentMethod
    case alertInstallmentWrongPaymentMethod
    case alertInstallmentInsertValue
    case changePaymentMethod

    case unfollowConfirmationTitle
    case cancelRequestConfirmationTitle
    
    case p2pLendingPaymentSceneTitle
    case p2pLendingPaymentMethodTitle
    case p2pLendingPaymentButtonTitle
    case p2pLendingPrivacyPopupTitle
    case p2pLendingPrivacyPopupBody
    case p2pLendingPaymentMethodPopupTitle
    case p2pLendingPaymentMethodPopupBody
    
    case billetPaymentSuccessSchedullingTitle
    case billetPaymentSuccessSchedullingMessage
    case billetOverdueTitle
    case billetOverdueMessage
    case billetTimeoutTitle
    case billetTimeoutMessage
    case billetNotScheduledTitle
    case billetNotScheduledMessage
    
    var key: String {
        self.rawValue
    }
    
    var file: LocalizableFile {
        .payment
    }
}
