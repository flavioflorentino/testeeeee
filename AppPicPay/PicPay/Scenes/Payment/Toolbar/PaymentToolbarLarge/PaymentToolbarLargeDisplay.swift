import CorePayment
import UIKit

protocol PaymentToolbarLargeDisplay: AnyObject {
    func completionPaymentMethod()
    func displayPayTitle(_ title: String)
    func displayPaymentMethod(_ image: UIImage)
    func displayPrivacy(title: String, image: UIImage, privacy: PaymentPrivacy)
    func displayAskPrivacy(title: String, message: String, publicButton: String, privateButton: String, cancelAction: String)
}
