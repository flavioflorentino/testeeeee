import UIKit

enum PaymentToolbarLargeAction {
    case openPaymentMethods(completion: () -> Void)
}

protocol PaymentToolbarLargeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: PaymentToolbarLargeAction)
}

final class PaymentToolbarLargeCoordinator: PaymentToolbarLargeCoordinating {
    private var completion: (() -> Void)?
    weak var viewController: UIViewController?
    
    func perform(action: PaymentToolbarLargeAction) {
        switch action {
        case let .openPaymentMethods(completion):
            self.completion = completion
            guard let controller = ViewsManager.paymentMethodsStoryboardViewController(withIdentifier: "PaymentMethodsViewController") as? PaymentMethodsViewController else {
                return
            }
            
            controller.viewModel.shouldShowPicPayBalanceSwitch = true
            controller.headerText = PaymentLocalizable.paymentMethods.text
            controller.delegate = self
            viewController?.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

extension PaymentToolbarLargeCoordinator: PaymentMethodsViewControllerDelegate {
    func paymentMethodsViewControllerDismissed() { }
    
    func paymentMethodsCompletion() {
        completion?()
    }
}
