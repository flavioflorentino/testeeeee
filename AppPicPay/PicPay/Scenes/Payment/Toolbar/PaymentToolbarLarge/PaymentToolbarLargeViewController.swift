import CorePayment
import UI
import UIKit

extension PaymentToolbarLargeViewController.Layout {
    enum Size {
        static let screenWidth: CGFloat = UIScreen.main.bounds.width
        static let heightStackView: CGFloat = screenWidth <= 320.0 ? 48.0 : 56.0
        static let heightButton: CGFloat = 48.0
        static let widthButton: CGFloat = 220.0
        static let borderWidth: CGFloat = 0.8
    }
    
    enum Spacing {
        static let maxStackViewTop: CGFloat = 120
        static let minStackViewTop: CGFloat = 16
    }
    
    enum Font {
        static let button = UIFont.systemFont(ofSize: 14)
    }
}

final class PaymentToolbarLargeViewController: ViewController<PaymentToolbarLargeViewModelInputs, UIView>, PaymentToolbarInput {
    fileprivate enum Layout {}
    
    private lazy var buttonPay: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = Layout.Size.heightButton / 2
        button.addTarget(self, action: #selector(tapPay), for: .touchUpInside)
        button.setImage(Assets.Icons.icoCash.image, for: .normal)
        button.adjustsImageWhenHighlighted = false
        
        return button
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.axis = .horizontal
        stackView.spacing = -0.5
        
        return stackView
    }()
    
    private lazy var buttonPrivacy: UIButton = {
        let button = UIButton()
        button.layer.borderWidth = Layout.Size.borderWidth
        button.layer.borderColor = Palette.ppColorGrayscale300.cgColor
        button.addTarget(self, action: #selector(tapPrivacy), for: .touchUpInside)
        button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        button.adjustsImageWhenHighlighted = false
        button.titleLabel?.font = Layout.Font.button
        button.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 10.0, bottom: 0.0, right: 0.0)
        button.imageView?.contentMode = .scaleAspectFill
        
        return button
    }()
    
    private lazy var buttonPaymentMethods: UIButton = {
        let button = UIButton()
        button.layer.borderWidth = Layout.Size.borderWidth
        button.layer.borderColor = Palette.ppColorGrayscale300.cgColor
        button.addTarget(self, action: #selector(tapPaymentMethods), for: .touchUpInside)
        button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        button.adjustsImageWhenHighlighted = false
        
        return button
    }()
    
    weak var delegate: PaymentToolbartOutput?
    
    var paymentMethod: PaymentMethod = .accountBalance {
        didSet {
            viewModel.changePaymentMethod(paymentMethod)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.setupView()
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override func buildViewHierarchy() {
        stackView.addArrangedSubview(buttonPrivacy)
        stackView.addArrangedSubview(buttonPaymentMethods)
        view.addSubview(buttonPay)
        view.addSubview(stackView)
    }
    
    override func setupConstraints() {
        buttonPay.layout {
            $0.top == view.topAnchor + Spacing.base02
            $0.centerX == view.centerXAnchor
            $0.width == Layout.Size.widthButton
            $0.height == Layout.Size.heightButton
        }
        
        stackView.layout {
            $0.top >= buttonPay.bottomAnchor + Layout.Spacing.minStackViewTop
            $0.top <= buttonPay.bottomAnchor + Layout.Spacing.maxStackViewTop
            $0.bottom == view.compatibleSafeAreaLayoutGuide.bottomAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
            $0.height == Layout.Size.heightStackView
        }
    }
}

@objc
private extension PaymentToolbarLargeViewController {
    func tapPrivacy() {
        viewModel.didTapPrivacy()
    }
    
    func tapPaymentMethods() {
        viewModel.didTapPaymentMethods()
    }
    
    func tapPay() {
        delegate?.didTapPayment()
    }
}

// MARK: View Model Outputs
extension PaymentToolbarLargeViewController: PaymentToolbarLargeDisplay {
    func displayPayTitle(_ title: String) {
        let button = Button(title: title)
        buttonPay.configure(with: button)
    }
    
    func displayPaymentMethod(_ image: UIImage) {
        buttonPaymentMethods.setImage(image, for: .normal)
    }
    
    func displayPrivacy(title: String, image: UIImage, privacy: PaymentPrivacy) {
        buttonPrivacy.setTitle(title, for: .normal)
        buttonPrivacy.setImage(image, for: .normal)
        
        delegate?.changePrivacy(privacy: privacy)
    }
    
    func displayAskPrivacy(title: String, message: String, publicButton: String, privateButton: String, cancelAction: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        let publicAction = UIAlertAction(title: publicButton, style: .default) { [weak self] _ in
            self?.viewModel.changePaymentPrivacy(.public)
        }
        
        let privateAction = UIAlertAction(title: privateButton, style: .default) { [weak self] _ in
            self?.viewModel.changePaymentPrivacy(.private)
        }
        
        let cancelAction = UIAlertAction(title: cancelAction, style: .cancel)
        
        alert.addAction(publicAction)
        alert.addAction(privateAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true)
    }
    
    func completionPaymentMethod() {
        delegate?.completionPaymentMethods()
    }
}
