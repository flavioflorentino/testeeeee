import CorePayment
import Foundation

protocol PaymentToolbarLargeServicing {
    var privacyConfig: PaymentPrivacy { get }
}

final class PaymentToolbarLargeService: PaymentToolbarLargeServicing {
    typealias Dependencies = HasConsumerManager & HasPaymentManager
    private var dependencies: Dependencies
    
    var privacyConfig: PaymentPrivacy {
        let privacy = dependencies.consumerManager.privacyConfig
        return PaymentPrivacy(rawValue: privacy) ?? .private
    }
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}
