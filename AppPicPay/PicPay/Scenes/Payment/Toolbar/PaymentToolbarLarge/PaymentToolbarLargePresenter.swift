import Core
import CorePayment
import Foundation

protocol PaymentToolbarLargePresenting: AnyObject {
    var viewController: PaymentToolbarLargeDisplay? { get set }
    func updatePaymentMethod(_ paymentMethod: PaymentMethod)
    func updatePrivacy(_ privacy: PaymentPrivacy)
    func updateView(paymentMethod: PaymentMethod, privacy: PaymentPrivacy)
    func presentAskPrivacy()
    func completionPaymentMethod()
    func didNextStep(action: PaymentToolbarLargeAction)
}

final class PaymentToolbarLargePresenter: PaymentToolbarLargePresenting {
    private let coordinator: PaymentToolbarLargeCoordinating
    private let model: PaymentToolbarLarge
    
    weak var viewController: PaymentToolbarLargeDisplay?

    init(model: PaymentToolbarLarge, coordinator: PaymentToolbarLargeCoordinating) {
        self.coordinator = coordinator
        self.model = model
    }
    
    func updateView(paymentMethod: PaymentMethod, privacy: PaymentPrivacy) {
        setupPaymentMethod(paymentMethod)
        setupPrivacy(privacy)
        setupPayTitle()
    }
    
    func updatePaymentMethod(_ paymentMethod: PaymentMethod) {
        setupPaymentMethod(paymentMethod)
    }
    
    func updatePrivacy(_ privacy: PaymentPrivacy) {
        setupPrivacy(privacy)
    }
    
    func presentAskPrivacy() {
        viewController?.displayAskPrivacy(
            title: PaymentLocalizable.selectPrivacyTitle.text,
            message: PaymentLocalizable.selectPrivacyDesc.text,
            publicButton: PaymentLocalizable.privacyPublic.text,
            privateButton: PaymentLocalizable.privacyPrivate.text,
            cancelAction: PaymentLocalizable.buttonCancel.text
        )
    }
    
    func completionPaymentMethod() {
        viewController?.completionPaymentMethod()
    }
    
    func didNextStep(action: PaymentToolbarLargeAction) {
        coordinator.perform(action: action)
    }
    
    private func setupPaymentMethod(_ paymentMethod: PaymentMethod) {
        let image: UIImage
        switch paymentMethod {
        case .accountBalance:
            image = Assets.NewGeneration.paymentOriginIconBalance.image
        case .balanceAndCard:
            image = Assets.NewGeneration.paymentOriginIconCardAndBalance.image
        case .card:
            image = Assets.NewGeneration.paymentOriginIconCard.image
        }
        
        viewController?.displayPaymentMethod(image)
    }
    
    private func setupPrivacy(_ privacy: PaymentPrivacy) {
        switch privacy {
        case .private:
            viewController?.displayPrivacy(
                title: PaymentLocalizable.privacyPrivate.text,
                image: Assets.NewGeneration.icoPrivPrivado.image,
                privacy: .private
            )
            
        case .public:
            viewController?.displayPrivacy(
                title: PaymentLocalizable.privacyPublic.text,
                image: Assets.NewGeneration.icoPrivAmigos.image,
                privacy: .public
            )
        }
    }
    
    private func setupPayTitle() {
        viewController?.displayPayTitle(model.title)
    }
}
