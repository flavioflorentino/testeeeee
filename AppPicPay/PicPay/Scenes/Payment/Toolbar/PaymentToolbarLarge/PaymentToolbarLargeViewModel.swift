import CorePayment
import Foundation

protocol PaymentToolbarLargeViewModelInputs: AnyObject {
    func setupView()
    func didTapPrivacy()
    func didTapPaymentMethods()
    func changePaymentMethod(_ paymentMethod: PaymentMethod)
    func changePaymentPrivacy(_ paymentPrivacy: PaymentPrivacy)
}

final class PaymentToolbarLargeViewModel {
    private let service: PaymentToolbarLargeServicing
    private let presenter: PaymentToolbarLargePresenting
    private var paymentPrivacy: PaymentPrivacy = .private
    private var paymentMethod: PaymentMethod = .accountBalance
    
    init(service: PaymentToolbarLargeServicing, presenter: PaymentToolbarLargePresenting) {
        self.service = service
        self.presenter = presenter
    }
}

extension PaymentToolbarLargeViewModel: PaymentToolbarLargeViewModelInputs {
    func setupView() {
        paymentPrivacy = service.privacyConfig
        presenter.updateView(paymentMethod: paymentMethod, privacy: paymentPrivacy)
    }
    
    func didTapPrivacy() {
        presenter.presentAskPrivacy()
    }
    
    func didTapPaymentMethods() {
        presenter.didNextStep(action: .openPaymentMethods { [weak self] in
            self?.presenter.completionPaymentMethod()
        })
    }
    
    func changePaymentMethod(_ paymentMethod: PaymentMethod) {
        self.paymentMethod = paymentMethod
        presenter.updatePaymentMethod(paymentMethod)
    }
    
    func changePaymentPrivacy(_ paymentPrivacy: PaymentPrivacy) {
        self.paymentPrivacy = paymentPrivacy
        presenter.updatePrivacy(paymentPrivacy)
    }
}
