import Foundation

enum PaymentToolbarLargeFactory {
    static func make(presenterModel: PaymentToolbarLarge) -> PaymentToolbarLargeViewController {
        let container = DependencyContainer()
        let service: PaymentToolbarLargeServicing = PaymentToolbarLargeService(dependencies: container)
        let coordinator: PaymentToolbarLargeCoordinating = PaymentToolbarLargeCoordinator()
        let presenter: PaymentToolbarLargePresenting = PaymentToolbarLargePresenter(model: presenterModel, coordinator: coordinator)
        let viewModel = PaymentToolbarLargeViewModel(service: service, presenter: presenter)
        let viewController = PaymentToolbarLargeViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
