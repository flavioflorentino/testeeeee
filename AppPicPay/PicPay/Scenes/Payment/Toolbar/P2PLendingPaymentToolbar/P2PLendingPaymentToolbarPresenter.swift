import Foundation

protocol P2PLendingPaymentToolbarPresenting: AnyObject {
    var viewController: P2PLendingPaymentToolbarDisplaying? { get set }
    func presentPaymentAuthentication()
    func presentPrivacyDialog()
    func presentPaymentMethodDialog()
}

final class P2PLendingPaymentToolbarPresenter: P2PLendingPaymentToolbarPresenting {
    private typealias Localizable = PaymentLocalizable
    weak var viewController: P2PLendingPaymentToolbarDisplaying?

    func presentPaymentAuthentication() {
        viewController?.displayPaymentAuthentication()
    }
    
    func presentPrivacyDialog() {
        viewController?.displayAlert(
            title: Localizable.p2pLendingPrivacyPopupTitle.text,
            message: Localizable.p2pLendingPrivacyPopupBody.text
        )
    }
    
    func presentPaymentMethodDialog() {
        viewController?.displayAlert(
            title: Localizable.p2pLendingPaymentMethodPopupTitle.text,
            message: Localizable.p2pLendingPaymentMethodPopupBody.text
        )
    }
}
