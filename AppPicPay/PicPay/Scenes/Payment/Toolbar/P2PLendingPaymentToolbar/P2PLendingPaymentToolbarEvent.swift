import Foundation
import AnalyticsModule

enum P2PLendingPaymentToolbarEvent: AnalyticsKeyProtocol {
    case didTapPay
    
    var name: String {
        "Button Clicked"
    }
    
    var properties: [String: Any] {
        switch self {
        case .didTapPay:
            return [
                EventKeys.screenName.rawValue: AnalyticsConstants.confirmLoan.rawValue,
                EventKeys.buttonName.rawValue: AnalyticsConstants.payButton.rawValue,
                EventKeys.context.rawValue: AnalyticsConstants.p2pLending.rawValue
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.eventTracker])
    }
}

enum EventKeys: String {
    case screenName = "screen_name"
    case buttonName = "button_name"
    case context = "context"
}

enum AnalyticsConstants: String {
    case confirmLoan = "P2P_LENDING_INVESTIDOR_FAZER_EMPRESTIMO"
    case payButton = "PAGAR"
    case p2pLending = "P2P_LENDING"
}
