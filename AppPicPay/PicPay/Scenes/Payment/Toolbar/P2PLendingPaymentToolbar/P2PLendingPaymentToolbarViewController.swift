import UI
import UIKit
import CorePayment
import AssetsKit

protocol P2PLendingPaymentToolbarDisplaying: AnyObject {
    func displayPaymentAuthentication()
    func displayAlert(title: String, message: String)
}

private extension P2PLendingPaymentToolbarViewController.Layout {
    enum Lenght {
        static let paymentButtonWidth: CGFloat = 120
    }
    
    enum Size {
        static let paymentMethodImage = CGSize(width: 22, height: 22)
    }
    
    enum Insets {
        static let `default`: EdgeInsets = {
            var insets: EdgeInsets = .rootView
            insets.top = Spacing.base01
            insets.bottom = Spacing.base01
            return insets
        }()
    }
}

final class P2PLendingPaymentToolbarViewController: ViewController<P2PLendingPaymentToolbarInteracting, UIView>, PaymentToolbarInput {
    fileprivate enum Layout { }
    
    // MARK: Privacy
    
    private lazy var privacyIconImageView: UIImageView = {
        let image = Assets.NewGeneration.icoPrivPrivado.image.withRenderingMode(.alwaysTemplate)
        let imageView = UIImageView(image: image)
        imageView.setContentHuggingPriority(.required, for: .horizontal)
        imageView.tintColor = Colors.grayscale500.color
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapPrivacy)))
        return imageView
    }()
    
    private lazy var privacyDescriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
        label.textColor = Colors.grayscale500.color
        label.text = PaymentLocalizable.privacyPrivate.text
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapPrivacy)))
        return label
    }()
    
    private lazy var privacyContainerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [privacyIconImageView, privacyDescriptionLabel])
        stackView.spacing = Spacing.base01
        stackView.compatibleLayoutMargins = .rootView
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.setContentHuggingPriority(.required, for: .vertical)
        return stackView
    }()
    
    // MARK: Payment method
    
    private lazy var paymentMethodIconImageView: UIImageView = {
        let image = Resources.Icons.icoCurrency.image.withRenderingMode(.alwaysTemplate)
        let imageView = UIImageView(image: image)
        imageView.tintColor = Colors.branding600.color
        imageView.setContentHuggingPriority(.required, for: .horizontal)
        return imageView
    }()
    
    private lazy var paymentMethodTitleLabel: UILabel = {
        let label = UILabel()
        label.text = PaymentLocalizable.p2pLendingPaymentMethodTitle.text
        label.labelStyle(BodySecondaryLabelStyle())
        label.textColor = Colors.grayscale700.color
        return label
    }()
    
    private lazy var paymentMethodValueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
        label.textColor = Colors.branding600.color
        label.text = PaymentLocalizable.balancePaymentMethodDescription.text
        return label
    }()
    
    private lazy var paymentMethodDescriptionStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [paymentMethodTitleLabel, paymentMethodValueLabel])
        stackView.axis = .vertical
        stackView.isUserInteractionEnabled = true
        stackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(paymentMethodTapped)))
        return stackView
    }()
    
    private lazy var payButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle(size: .small))
        button.setTitle(PaymentLocalizable.p2pLendingPaymentButtonTitle.text, for: .normal)
        button.addTarget(self, action: #selector(payButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private lazy var paymentMethodContainerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [paymentMethodIconImageView, paymentMethodDescriptionStackView, payButton])
        stackView.spacing = Spacing.base02
        stackView.alignment = .center
        stackView.compatibleLayoutMargins = .rootView
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.setContentHuggingPriority(.required, for: .vertical)
        return stackView
    }()
    
    private lazy var paymentMethodBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.backgroundSecondary.color
        return view
    }()
    
    // MARK: - Root
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [privacyContainerStackView, paymentMethodContainerStackView])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        stackView.setContentHuggingPriority(.required, for: .vertical)
        return stackView
    }()
    
    weak var delegate: PaymentToolbartOutput?
    var paymentMethod: PaymentMethod = .accountBalance {
        didSet {
            if paymentMethod != .accountBalance {
                paymentMethod = .accountBalance
            }
        }
    }

    override func buildViewHierarchy() {
        view.addSubview(rootStackView)
        paymentMethodContainerStackView.addSubview(paymentMethodBackgroundView)
        paymentMethodContainerStackView.sendSubviewToBack(paymentMethodBackgroundView)
    }
    
    override func setupConstraints() {
        paymentMethodBackgroundView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        rootStackView.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview()
            $0.top.greaterThanOrEqualToSuperview()
        }
        
        payButton.snp.makeConstraints {
            $0.width.equalTo(Layout.Lenght.paymentButtonWidth)
        }
        
        paymentMethodIconImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.paymentMethodImage)
        }
    }
}

@objc
extension P2PLendingPaymentToolbarViewController {
    func payButtonTapped() {
        interactor.requestPayment()
    }
    
    func didTapPrivacy() {
        interactor.showPrivacyDialog()
    }
    
    func paymentMethodTapped() {
        interactor.showPaymentMethodDialog()
    }
}

// MARK: - P2PLendingPaymentToolbarDisplaying
extension P2PLendingPaymentToolbarViewController: P2PLendingPaymentToolbarDisplaying {
    func displayPaymentAuthentication() {
        delegate?.didTapPayment()
        delegate?.changePrivacy(privacy: .private)
    }
    
    func displayAlert(title: String, message: String) {
        let popUp = PopupViewController(title: title, description: message, preferredType: .text)
        let dismissAction = PopupAction(title: DefaultLocalizable.btOkUnderstood.text, style: .fill)
        popUp.addAction(dismissAction)
        
        showPopup(popUp)
    }
}
