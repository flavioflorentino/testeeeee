import Foundation
import AnalyticsModule

protocol P2PLendingPaymentToolbarInteracting: AnyObject {
    func requestPayment()
    func showPrivacyDialog()
    func showPaymentMethodDialog()
}

final class P2PLendingPaymentToolbarInteractor: P2PLendingPaymentToolbarInteracting {
    typealias Dependencies = HasAnalytics
    private let presenter: P2PLendingPaymentToolbarPresenting
    private let dependencies: Dependencies

    init(presenter: P2PLendingPaymentToolbarPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }

    func requestPayment() {
        dependencies.analytics.log(P2PLendingPaymentToolbarEvent.didTapPay)
        presenter.presentPaymentAuthentication()
    }
    
    func showPrivacyDialog() {
        presenter.presentPrivacyDialog()
    }
    
    func showPaymentMethodDialog() {
        presenter.presentPaymentMethodDialog()
    }
}
