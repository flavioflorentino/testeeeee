import UIKit

enum P2PLendingPaymentToolbarFactory {
    static func make() -> P2PLendingPaymentToolbarViewController {
        let container = DependencyContainer()
        let presenter: P2PLendingPaymentToolbarPresenting = P2PLendingPaymentToolbarPresenter()
        let interactor = P2PLendingPaymentToolbarInteractor(presenter: presenter, dependencies: container)
        let viewController = P2PLendingPaymentToolbarViewController(interactor: interactor)
        
        presenter.viewController = viewController

        return viewController
    }
}
