import Core
import Foundation

protocol PaymentToolbarPresenting: AnyObject {
    var viewController: PaymentToolbarDisplay? { get set }
    func didNextStep(action: PaymentToolbarAction)
}

final class PaymentToolbarPresenter: PaymentToolbarPresenting {
    private let coordinator: PaymentToolbarCoordinating
    weak var viewController: PaymentToolbarDisplay?
    
    init(coordinator: PaymentToolbarCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: PaymentToolbarAction) {
        coordinator.perform(action: action)
    }
}
