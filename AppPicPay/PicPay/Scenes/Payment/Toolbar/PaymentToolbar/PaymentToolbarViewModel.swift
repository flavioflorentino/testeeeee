import Foundation

protocol PaymentToolbarViewModelInputs: AnyObject {
}

final class PaymentToolbarViewModel {
    private let service: PaymentToolbarServicing
    private let presenter: PaymentToolbarPresenting

    init(service: PaymentToolbarServicing, presenter: PaymentToolbarPresenting) {
        self.service = service
        self.presenter = presenter
    }
}

extension PaymentToolbarViewModel: PaymentToolbarViewModelInputs {
}
