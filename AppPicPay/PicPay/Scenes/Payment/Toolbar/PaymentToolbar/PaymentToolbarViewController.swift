import CorePayment
import UI
import UIKit

final class PaymentToolbarViewController: ViewController<PaymentToolbarViewModelInputs, UIView>, PaymentToolbarInput {
    private lazy var paymentToolbar: PaymentToolbar = {
        let toolbar = PaymentToolbar()
        toolbar.isUserInteractionEnabled = true
        toolbar.translatesAutoresizingMaskIntoConstraints = false
        
        return toolbar
    }()
    
    private lazy var toolbarController: PaymentToolbarController = {
        let toolbarController = PaymentToolbarController(
            parent: self,
            toolbar: paymentToolbar,
            paymentToolbarType: paymentToolbarType
        )
        
        return toolbarController
    }()
    
    weak var delegate: PaymentToolbartOutput?
    
    var paymentMethod: PaymentMethod = .accountBalance {
        didSet {
            // todo: No futuro fazer essa comunicação com a conteinerPayment
        }
    }

    private let paymentToolbarType: PaymentToolbarType

    init(
        viewModel: PaymentToolbarViewModelInputs,
        paymentToolbarType: PaymentToolbarType = .default
    ) {
        self.paymentToolbarType = paymentToolbarType
        super.init(viewModel: viewModel)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        toolbarController.payAction = { [weak self] privacy in
            self?.delegate?.didTapPayment()
            self?.delegate?.changePrivacy(privacy: privacy == FeedItemVisibility.Private.rawValue ? .private : .public)
        }
        
        toolbarController.changeMethod = { [weak self] in
            self?.delegate?.completionPaymentMethods()
        }
    }
 
    override func configureViews() {
        paintSafeAreaBottomInset(withColor: toolbarController.bottomBarBackgroundColor)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(paymentToolbar)
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.activate([
            paymentToolbar.topAnchor.constraint(equalTo: view.topAnchor),
            paymentToolbar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            paymentToolbar.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            paymentToolbar.bottomAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    func showPaymentMethodSelection() {
        toolbarController.paymentToolbarShowPaymentMethodSelection()
    }
    
    func updateToolbar(value: Double, installment: Int, forceCreditCard: Bool) {
        // swiftlint:disable overpowered_viewcontroller
        let paymentManager = PPPaymentManager()
        paymentManager?.forceBalance = forceBalance()
        paymentManager?.forceCreditCard = forceCreditCard
        paymentManager?.subtotal = NSDecimalNumber(value: value)
        paymentManager?.selectedQuotaQuantity = installment
        
        toolbarController.paymentMethodHeaderText = forceCreditCard ? PaymentLocalizable.balanceDisable.text : String()
        toolbarController.canUseBalance = !forceCreditCard
        toolbarController.paymentManager = paymentManager
        // swiftlint:enable overpowered_viewcontroller
    }
    
    func changePayButtonTitle(with type: PaymentType) {
        switch type {
        case .default:
            paymentToolbar.setPayButtonTitle(PaymentLocalizable.pay.text)
        case .scheduling:
            paymentToolbar.setPayButtonTitle(PaymentLocalizable.schedule.text)
        }
    }

    private func forceBalance() -> Bool {
        switch paymentToolbarType {
        case .pix, .pixReturn:
            return true
        default:
            return false
        }
    }
}

// MARK: View Model Outputs
extension PaymentToolbarViewController: PaymentToolbarDisplay {
}
