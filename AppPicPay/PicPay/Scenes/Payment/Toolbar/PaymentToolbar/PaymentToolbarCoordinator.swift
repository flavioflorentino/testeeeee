import UIKit

enum PaymentToolbarAction {
}

protocol PaymentToolbarCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: PaymentToolbarAction)
}

final class PaymentToolbarCoordinator: PaymentToolbarCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: PaymentToolbarAction) {
    }
}
