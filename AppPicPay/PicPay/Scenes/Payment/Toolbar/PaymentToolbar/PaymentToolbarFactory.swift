import Foundation

enum PaymentToolbarType {
    case pix(alert: Alert)
    case pixReturn(changePaymentAlert: Alert, changePrivacyAlert: Alert)
    case `default`
}

enum PaymentToolbarFactory {
    static func make(paymentToolbarType: PaymentToolbarType = .default) -> PaymentToolbarViewController {
        let service: PaymentToolbarServicing = PaymentToolbarService()
        let coordinator: PaymentToolbarCoordinating = PaymentToolbarCoordinator()
        let presenter: PaymentToolbarPresenting = PaymentToolbarPresenter(coordinator: coordinator)
        let viewModel = PaymentToolbarViewModel(service: service, presenter: presenter)
        let viewController = PaymentToolbarViewController(viewModel: viewModel, paymentToolbarType: paymentToolbarType)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
