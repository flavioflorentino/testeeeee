import Foundation

enum CancellationConfirmationLocalizable: String, Localizable {
    case cancellationSentencePrefix
    case cancellationSentenceInfix
    case cancellationSentenceSufix
    case cancellationAuthorizeButtonTitle
    case cancellationUnauthorizeButtonTitle
    case cancellationConfirmationTitle
    case cancellationConfirmationSubtitle
    case cancellationConfirmationButtonTitle
    case cancellationConfirmationDatePrefix
    case cancellationConfirmationDatePreposition
    case cancellationConfirmationDateSufix
    case cancellationConfirmationErrorTitle
    case cancellationConfirmationErrorButtomText
    case cancellationConfirmationErrorDefaultMessage
    
    var key: String {
        self.rawValue
    }
    var file: LocalizableFile {
        .cancellationConfirmation
    }
}
