import UIKit

enum CancellationConfirmationAction {
    case dismiss
}

protocol CancellationConfirmationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: CancellationConfirmationAction)
}

final class CancellationConfirmationCoordinator: CancellationConfirmationCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: CancellationConfirmationAction) {
        switch action {
        case .dismiss:
            viewController?.dismiss(animated: true)
        }
    }
}
