import Foundation
import Core

struct CancellationOrder {
    let uuid: String
    let sellerCode: String
    let storeName: String
    let value: Float
    let date: String
    let storeImage: String?
    let type: String
}

protocol CancellationConfirmationViewModelInputs: AnyObject {
    func startFlow()
    func authorizeCancel()
    func unauthorizeCancel()
    func confirmationFeedback()
    func exit()
}

final class CancellationConfirmationViewModel {
    private let service: CancellationConfirmationServicing
    private let presenter: CancellationConfirmationPresenting
    private let cancellationOrder: CancellationOrder
    private var userConfirmCancellation = false

    init(service: CancellationConfirmationServicing, presenter: CancellationConfirmationPresenting, cancellationOrder: CancellationOrder) {
        self.service = service
        self.presenter = presenter
        self.cancellationOrder = cancellationOrder
    }
}

extension CancellationConfirmationViewModel: CancellationConfirmationViewModelInputs {
    func startFlow() {
        let date = Date.decodableFormatter.date(from: cancellationOrder.date)
        let response = CancellationConfirmationPresenter.AskCancellationResponse(storeImage: cancellationOrder.storeImage, storeName: cancellationOrder.storeName, value: cancellationOrder.value, date: date)
        
        presenter.presentAskCancelationView(response: response)
        presenter.presentConfigureView()
    }
    
    private func handleErrorMessage(error: ApiError) -> String {
        switch error {
        case .badRequest(body: let error),
             .notFound(body: let error),
             .unauthorized(body: let error):
            return error.message
        default:
            return CancellationConfirmationLocalizable.cancellationConfirmationErrorDefaultMessage.text
        }
    }
    
    func confirmationFeedback() {
        presenter.didNextStep(action: .dismiss)
    }
    
    func authorizeCancel() {
        presenter.presentLoadStatus()
        
        let cancellation = CancellationConfirmationService.Cancellation(
            uuid: cancellationOrder.uuid,
            sellerCode: cancellationOrder.sellerCode,
            type: cancellationOrder.type,
            storeName: cancellationOrder.storeName,
            userAuthorization: true
        )
        service.responseToCancellation(cancellation: cancellation) { [weak self] result in
            switch result {
            case .success:
                let response = CancellationConfirmationPresenter.ConfirmationResponse(storeImage: self?.cancellationOrder.storeImage)
                self?.presenter.presentConfirmationCancelation(response: response)
                self?.userConfirmCancellation = true
            case.failure (let error):
                let message = self?.handleErrorMessage(error: error) ?? CancellationConfirmationLocalizable.cancellationConfirmationErrorDefaultMessage.text
                self?.presenter.presentError(message: message)
            }
        }
    }
    
    private func unauthorizeCancelLogic() {
        let cancellation = CancellationConfirmationService.Cancellation(
            uuid: cancellationOrder.uuid,
            sellerCode: cancellationOrder.sellerCode,
            type: cancellationOrder.type,
            storeName: cancellationOrder.storeName,
            userAuthorization: false
        )
               
        service.responseToCancellation(cancellation: cancellation, completionHandler: nil)
        presenter.didNextStep(action: .dismiss)
    }
    
    func unauthorizeCancel() {
        unauthorizeCancelLogic()
    }
    
    func exit() {
        guard self.userConfirmCancellation else {
            unauthorizeCancelLogic()
            return
        }
        presenter.didNextStep(action: .dismiss)
    }
}
