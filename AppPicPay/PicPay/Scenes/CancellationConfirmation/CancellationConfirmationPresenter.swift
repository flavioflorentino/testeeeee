import Core

protocol CancellationConfirmationPresenting: AnyObject {
    var viewController: CancellationConfirmationDisplay? { get set }
    func didNextStep(action: CancellationConfirmationAction)
    
    func presentAskCancelationView(response: CancellationConfirmationPresenter.AskCancellationResponse)
    func presentConfirmationCancelation(response: CancellationConfirmationPresenter.ConfirmationResponse)
    func presentError(message: String)
    func presentLoadStatus()
    func presentConfigureView()
}

final class CancellationConfirmationPresenter: CancellationConfirmationPresenting {
    fileprivate enum Layout {}
    
    struct AskCancellationResponse {
        let storeImage: String?
        let storeName: String
        let value: Float
        let date: Date?
    }
    
    struct ConfirmationResponse {
        let storeImage: String?
    }
    
    private let coordinator: CancellationConfirmationCoordinating
    weak var viewController: CancellationConfirmationDisplay?

    init(coordinator: CancellationConfirmationCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: CancellationConfirmationAction) {
        coordinator.perform(action: action)
    }
    
    private func formatAsCancellationDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "pt_BR")
        dateFormatter.dateFormat = "dd"
        let day = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "MMMM"
        let mounth = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "HH"
        let hour = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "mm"
        let minute = dateFormatter.string(from: date)
        
        let prefixSentence = "\(CancellationConfirmationLocalizable.cancellationConfirmationDatePrefix.text)"
        let dateSentence = " \(day) \(CancellationConfirmationLocalizable.cancellationConfirmationDatePreposition.text) \(mounth)"
        let hourSentence = " \(CancellationConfirmationLocalizable.cancellationConfirmationDateSufix.text) \(hour):\(minute)."
        
        let formatedDate = "\(prefixSentence)\(dateSentence)\(hourSentence)"
        
        return formatedDate
    }
    
    private func formatAskCancelationTitle(value: Float, storeName: String) -> NSAttributedString {
        let title = NSMutableAttributedString()
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = Layout.AskCancellation.titlelineHeightMultiple
        paragraphStyle.alignment = .center
        
        let tileHighlightAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: Layout.AskCancellation.titleFontSize), NSAttributedString.Key.paragraphStyle: paragraphStyle]
        
        let sentence1 = NSMutableAttributedString(string: CancellationConfirmationLocalizable.cancellationSentencePrefix.text)
        let value = value.toCurrencyString() ?? ""
        let currencyValue = NSMutableAttributedString(string: value, attributes: tileHighlightAttributes)
        let sentence2 = NSMutableAttributedString(string: CancellationConfirmationLocalizable.cancellationSentenceInfix.text)
        let storeName = NSMutableAttributedString(string: storeName, attributes: tileHighlightAttributes)
        let sentence3 = NSMutableAttributedString(string: CancellationConfirmationLocalizable.cancellationSentenceSufix.text)
        
        title.append(sentence1)
        title.append(currencyValue)
        title.append(sentence2)
        title.append(storeName)
        title.append(sentence3)
        
        return title
    }
    
    func presentAskCancelationView(response: CancellationConfirmationPresenter.AskCancellationResponse) {
        let title = formatAskCancelationTitle(value: response.value, storeName: response.storeName)
        
        var formatedDate = ""
        
        if let date = response.date {
            formatedDate = formatAsCancellationDate(date: date)
        }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = Layout.AskCancellation.subtitlelineHeightMultiple
        paragraphStyle.alignment = .center
        
        let subtitleAtributtes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: Layout.AskCancellation.subtitleFontSize), NSAttributedString.Key.paragraphStyle: paragraphStyle]
        let subtitle = NSMutableAttributedString(string: formatedDate, attributes: subtitleAtributtes)
        
        let authorizeButtonTitle = CancellationConfirmationLocalizable.cancellationAuthorizeButtonTitle.text
        let unauthorizeButtonTitle = CancellationConfirmationLocalizable.cancellationUnauthorizeButtonTitle.text
        
        var mainImage: ImageOption
        
        if let imagePath = response.storeImage {
            mainImage = ImageOption.url(imagePath, Assets.NewGeneration.avatarPlace.image)
        } else {
            mainImage = ImageOption.image(Assets.NewGeneration.avatarPlace.image)
        }
        
        let extraImage = ImageOption.image(Assets.Icons.cancelledFilledIcon.image)
        
        let viewElements = CancellationConfirmationBaseView.ViewElements(
            mainImage: mainImage,
            extraImage: extraImage,
            title: title,
            subtitle: subtitle,
            primaryButtonTitle: authorizeButtonTitle,
            secondaryButtonTitle: unauthorizeButtonTitle
        )
        
        viewController?.displayAskCancelationView(viewElements: viewElements)
    }

    func presentConfirmationCancelation(response: CancellationConfirmationPresenter.ConfirmationResponse) {
        let titleParagraphStyle = NSMutableParagraphStyle()
        titleParagraphStyle.lineHeightMultiple = Layout.CancellationConfirmation.titlelineHeightMultiple
        titleParagraphStyle.alignment = .center
        
        let titleAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: Layout.CancellationConfirmation.titleFontSize), NSAttributedString.Key.paragraphStyle: titleParagraphStyle]
        let title = NSMutableAttributedString(
            string: CancellationConfirmationLocalizable.cancellationConfirmationTitle.text,
            attributes: titleAttributes
        )
        
        let subtitleParagraphStyle = NSMutableParagraphStyle()
        subtitleParagraphStyle.lineHeightMultiple = Layout.CancellationConfirmation.subtitlelineHeightMultiple
        subtitleParagraphStyle.alignment = .center
        
        let subtitleAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: Layout.CancellationConfirmation.subtitleFontSize), NSAttributedString.Key.paragraphStyle: titleParagraphStyle]
        let subtitle = NSMutableAttributedString(
            string: CancellationConfirmationLocalizable.cancellationConfirmationSubtitle.text,
            attributes: subtitleAttributes
        )
        
        let buttonTitle = CancellationConfirmationLocalizable.cancellationConfirmationButtonTitle.text
        
        var mainImage: ImageOption
        
        if let imagePath = response.storeImage {
            mainImage = ImageOption.url(imagePath, Assets.NewGeneration.avatarPlace.image)
        } else {
            mainImage = ImageOption.image(Assets.NewGeneration.avatarPlace.image)
        }
        
        let extraImage = ImageOption.image(Assets.Icons.cancelledFilledIcon.image)
        
        let viewElements = CancellationConfirmationBaseView.ViewElements(
            mainImage: mainImage,
            extraImage: extraImage,
            title: title,
            subtitle: subtitle,
            primaryButtonTitle: buttonTitle,
            secondaryButtonTitle: nil
        )
        
        viewController?.displayConfirmationCancelation(viewElements: viewElements)
    }
    
    func presentError(message: String) {
        let cancellationConfirmationError = CancellationConfirmationViewController.CancellationConfirmationError(
            title: CancellationConfirmationLocalizable.cancellationConfirmationErrorTitle.text,
            message: message,
            confirmationButtonTitle: CancellationConfirmationLocalizable.cancellationConfirmationErrorButtomText.text
        )
        viewController?.displayError(error: cancellationConfirmationError)
    }
    
    func presentConfigureView() {
        let exitButtomImage = Assets.Savings.iconRoundCloseGreen.image
        let viewSetup = CancellationConfirmationViewController.ViewSetup(exitButtonImage: exitButtomImage)
        
        viewController?.displayConfigureView(viewSetup: viewSetup)
    }
    
    func presentLoadStatus() {
        viewController?.displayLoadStatus()
    }
}

extension CancellationConfirmationPresenter.Layout {
    enum AskCancellation {
        static let titlelineHeightMultiple: CGFloat = 1.07
        static let titleFontSize: CGFloat = 16.0
        static let subtitlelineHeightMultiple: CGFloat = 1.22
        static let subtitleFontSize: CGFloat = 14.0
    }
    
    enum CancellationConfirmation {
        static let titlelineHeightMultiple: CGFloat = 0.95
        static let titleFontSize: CGFloat = 18.0
        static let subtitlelineHeightMultiple: CGFloat = 1.07
        static let subtitleFontSize: CGFloat = 16.0
    }
}
