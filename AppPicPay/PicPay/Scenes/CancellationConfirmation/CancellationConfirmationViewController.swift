import UI
import UIKit

final class CancellationConfirmationViewController: ViewController<CancellationConfirmationViewModelInputs, UIView> {
    struct CancellationConfirmationError {
        let title: String
        let message: String
        let confirmationButtonTitle: String
    }
    
    struct ViewSetup {
        let exitButtonImage: UIImage
    }
    
    fileprivate enum Layout {}
    
    // MARK: View Elements
       
    private lazy var exitButton: UIButton = {
        let button = UIButton(type: .custom)
        button.layer.cornerRadius = Layout.ExitButton.cornerRadius
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(exitButtonPressed), for: .touchUpInside)
        return button
    }()
    
    private lazy var askCancelationView = CancellationConfirmationBaseView(style: .question)
    private lazy var confirmationView = CancellationConfirmationBaseView(style: .confirmation)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        viewModel.startFlow()
    }
 
    private func setup() {
        confirmationView.delegate = self
        askCancelationView.delegate = self
    }
    
    private func hideViews() {
        askCancelationView.isHidden = true
        confirmationView.isHidden = true
    }
    
    override func buildViewHierarchy() {
        view.addSubview(confirmationView)
        view.addSubview(askCancelationView)
        view.addSubview(exitButton)
        
        hideViews()
    }
    
    override func setupConstraints() {
        exitButton.layout {
            $0.height == Layout.ExitButton.height
            $0.width == Layout.ExitButton.width
            $0.leading == view.leadingAnchor + Layout.ExitButton.leadingMargin
            $0.top == view.topAnchor + Layout.ExitButton.topMargin
        }
        
        askCancelationView.layout {
            $0.centerX == view.centerXAnchor
            $0.centerY == view.centerYAnchor
            $0.height == view.heightAnchor
            $0.width == view.widthAnchor
        }
        
        confirmationView.layout {
            $0.centerX == view.centerXAnchor
            $0.centerY == view.centerYAnchor
            $0.height == view.heightAnchor
            $0.width == view.widthAnchor
        }
    }
    
    private func showAskCancelationView() {
        askCancelationView.isHidden = false
        confirmationView.isHidden = true
    }
    private func showConfirmationView() {
        askCancelationView.isHidden = true
        confirmationView.isHidden = false
    }
    
    @objc
    private func exitButtonPressed() {
        viewModel.exit()
    }
}

// MARK: View Model Outputs
extension CancellationConfirmationViewController: CancellationConfirmationDisplay {
    func displayAskCancelationView(viewElements: CancellationConfirmationBaseView.ViewElements) {
        askCancelationView.stopLoading()
        showAskCancelationView()
        askCancelationView.setViewElementes(viewElements: viewElements)
    }
    func displayConfirmationCancelation(viewElements: CancellationConfirmationBaseView.ViewElements) {
        askCancelationView.stopLoading()
        showConfirmationView()
        confirmationView.setViewElementes(viewElements: viewElements)
    }
    
    func displayError(error: CancellationConfirmationViewController.CancellationConfirmationError) {
        askCancelationView.stopLoading()
        
        let popup = PopupViewController(
            title: error.title,
            description: error.message,
            preferredType: .image(Assets.Icons.iconBad.image)
        )
        
        let action = PopupAction(title: error.confirmationButtonTitle, style: .fill)
        popup.addAction(action)
        showPopup(popup)
    }
    
    func displayLoadStatus() {
        askCancelationView.startLoading()
    }
    func displayConfigureView(viewSetup: CancellationConfirmationViewController.ViewSetup) {
        exitButton.setImage(viewSetup.exitButtonImage, for: .normal)
    }
}

extension CancellationConfirmationViewController.Layout {
    enum ExitButton {
        static let height: CGFloat = 30.0
        static let width: CGFloat = 30.0
        static let leadingMargin: CGFloat = 20.0
        static let topMargin: CGFloat = 20.0
        static let cornerRadius: CGFloat = 15.0
    }
}

extension CancellationConfirmationViewController: CancellationConfirmationBaseViewDelegate {
    func primaryButtonClicked(sender: CancellationConfirmationBaseView) {
        switch sender {
        case askCancelationView:
            viewModel.authorizeCancel()
        case confirmationView:
            viewModel.confirmationFeedback()
        default:
            break
        }
    }
    
    func secondaryButtonClicked(sender: CancellationConfirmationBaseView) {
        viewModel.unauthorizeCancel()
     }
}
