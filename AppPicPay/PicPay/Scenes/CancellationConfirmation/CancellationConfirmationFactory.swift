import Foundation

enum CancellationConfirmationFactory {
    static func make(cancellationOrder: CancellationOrder) -> CancellationConfirmationViewController {
        let container = DependencyContainer()
        let service: CancellationConfirmationServicing = CancellationConfirmationService(dependencies: container)
        let coordinator: CancellationConfirmationCoordinating = CancellationConfirmationCoordinator()
        let presenter: CancellationConfirmationPresenting = CancellationConfirmationPresenter(coordinator: coordinator)
        let viewModel = CancellationConfirmationViewModel(service: service, presenter: presenter, cancellationOrder: cancellationOrder)
        let viewController = CancellationConfirmationViewController(viewModel: viewModel)
        
        if #available(iOS 13, *) {
            viewController.isModalInPresentation = true
        }
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
