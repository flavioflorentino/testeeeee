import Foundation

final class CancellationConfirmationDeepLink: PPDeeplink {
    private enum Keys {
        static let uuid = "uuid"
        static let sellerCode = "sellerCode"
        static let storeName = "storeName"
        static let value = "value"
        static let date = "date"
        static let type = "type"
        static let storeImage = "store_image"
    }
    
    let uuid: String
    let sellerCode: String
    let storeName: String
    let value: Float
    let date: String
    let storeImage: String?
    let transactionType: String
    
    init?(url: URL) {
        guard let uuid = url.queryParams[Keys.uuid],
            let sellerCode = url.queryParams[Keys.sellerCode],
            let storeName = url.queryParams[Keys.storeName],
            let valueString = url.queryParams[Keys.value],
            let valueFloat = Float(valueString),
            let date = url.queryParams[Keys.date],
            let transactionType = url.queryParams[Keys.type] else {
            return nil
        }
        
        self.uuid = uuid
        self.sellerCode = sellerCode
        self.storeName = storeName
        self.value = valueFloat
        self.date = date
        self.storeImage = url.queryParams[Keys.storeImage]
        self.transactionType = transactionType
        
        super.init(url: url)
        self.type = .cancellationConfirmation
    }
}
