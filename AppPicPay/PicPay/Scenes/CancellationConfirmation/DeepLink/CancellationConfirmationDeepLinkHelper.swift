import Foundation
import UI

final class CancellationConfirmationDeepLinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        guard let cancellationDeepLink = deeplink as? CancellationConfirmationDeepLink else {
            return false
        }
        
        let cancellationOrder = CancellationOrder(
            uuid: cancellationDeepLink.uuid,
            sellerCode: cancellationDeepLink.sellerCode,
            storeName: cancellationDeepLink.storeName,
            value: cancellationDeepLink.value,
            date: cancellationDeepLink.date,
            storeImage: cancellationDeepLink.storeImage,
            type: cancellationDeepLink.transactionType
        )
        let cancellationController = CancellationConfirmationFactory.make(cancellationOrder: cancellationOrder)
        
        let mainScreen = AppManager.shared.mainScreenCoordinator?.mainScreen()
        if let from = controller ?? mainScreen {
            from.present(cancellationController, animated: true)
            return true
        }
        return false
    }
}
