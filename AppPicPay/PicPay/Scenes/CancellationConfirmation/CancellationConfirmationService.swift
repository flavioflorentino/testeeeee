import Foundation
import Core

protocol CancellationConfirmationServicing {
    typealias CancellationCompletion = (Result<Void, ApiError>) -> Void
    func responseToCancellation(cancellation: CancellationConfirmationService.Cancellation, completionHandler: CancellationCompletion?)
}

final class CancellationConfirmationService: CancellationConfirmationServicing {
    typealias Dependencies = HasMainQueue
    
    struct Cancellation {
        let uuid: String
        let sellerCode: String
        let type: String
        let storeName: String?
        let userAuthorization: Bool
    }
    
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func responseToCancellation(cancellation: Cancellation, completionHandler: CancellationConfirmationServicing.CancellationCompletion?) {
        let endPoinPoint = CancellationConfirmationEndPoint(
            uuid: cancellation.uuid,
            sellerCode: cancellation.sellerCode,
            type: cancellation.type,
            storeName: cancellation.storeName,
            userAuthorization: cancellation.userAuthorization
        )
        
        Api<NoContent>(endpoint: endPoinPoint).execute { [weak self] result in
            switch result {
            case .success:
                self?.dependencies.mainQueue.async {
                    completionHandler?(.success)
                }
            case .failure(let apiError):
                self?.dependencies.mainQueue.async {
                    completionHandler?(.failure(apiError))
                }
            }
        }
    }
}
