import UIKit
import UI

enum ImageOption {
    case image(UIImage)
    case url(String, UIImage?)
}

protocol CancellationConfirmationBaseViewDelegate: AnyObject {
    func primaryButtonClicked(sender: CancellationConfirmationBaseView)
    func secondaryButtonClicked(sender: CancellationConfirmationBaseView)
}

final class CancellationConfirmationBaseView: UIView {
    fileprivate enum Layout {}
    
    enum Style {
        case question
        case confirmation
    }
    
    struct ViewElements {
        let mainImage: ImageOption
        let extraImage: ImageOption
        let title: NSAttributedString
        let subtitle: NSAttributedString
        let primaryButtonTitle: String
        let secondaryButtonTitle: String?
    }
    
    // MARK: View Elements
    
    private lazy var imageContainner = UIView()
    
    private lazy var containnerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.alignment = .center
        
        return stackView
    }()
    
    private lazy var mainImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = Layout.mainImageViewCornerRadius
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        
        return imageView
    }()
    
    private lazy var extraImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = Layout.extraImageViewCornerRadius
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = Palette.ppColorGrayscale500.color
        
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        
        let color = style == .question ? Palette.ppColorGrayscale400.color : Palette.ppColorGrayscale500.color
        label.textColor = color
        
        return label
    }()
    
    private lazy var primaryButton: UIPPButton = {
        let button = UIPPButton()
        let color = style == .question ? Palette.white.color : Palette.ppColorBranding300.color
        button.normalTitleColor = style == .question ? Palette.white.color : Palette.ppColorBranding300.color
        button.highlightedTitleColor = style == .question ? Palette.ppColorBranding300.color : Palette.white.color
        button.titleLabel?.font = UIFont.systemFont(ofSize: Layout.buttonFontSize, weight: .semibold)
        button.borderColor = Palette.ppColorBranding300.color
        button.borderWidth = Layout.buttonBorderWidth
        button.cornerRadius = Layout.buttonCornerRadius
        button.normalBackgrounColor = style == .question ? Palette.ppColorBranding300.color : Palette.ppColorGrayscale000.color
        button.disabledBackgrounColor = style == .question ? Palette.ppColorBranding300.color : Palette.ppColorGrayscale000.color
        button.highlightedBackgrounColor = style == .question ? Palette.ppColorGrayscale000.color : Palette.ppColorBranding300.color
        button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        return button
    }()
    
    private lazy var secondaryButton: UIPPButton = {
        let button = UIPPButton()
        button.normalTitleColor = Palette.ppColorBranding300.color
        button.highlightedTitleColor = Palette.ppColorDarkGreen.color
        button.titleLabel?.font = UIFont.systemFont(ofSize: Layout.buttonFontSize, weight: .semibold)
        button.borderColor = Palette.ppColorBranding300.color
        button.highlightedBorderColor = Palette.ppColorDarkGreen.color
        button.borderWidth = Layout.buttonBorderWidth
        button.cornerRadius = Layout.buttonCornerRadius
        button.normalBackgrounColor = UIColor.clear
        button.highlightedBackgrounColor = UIColor.clear
        button.addTarget(self, action: #selector(secondaryButtonPressed), for: .touchUpInside)
        
        return button
    }()
    
    let style: Style
    weak var delegate: CancellationConfirmationBaseViewDelegate?
    
    override init(frame: CGRect) {
        self.style = .question
        super.init(frame: frame)
        buildLayout()
    }
    
    init(style: Style) {
        self.style = style
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setViewElementes(viewElements: ViewElements) {
        self.titleLabel.attributedText = viewElements.title
        self.subtitleLabel.attributedText = viewElements.subtitle
        
        switch viewElements.mainImage {
        case .image(let image):
            self.mainImageView.image = image
        case let .url(url, placeholderImage):
            self.mainImageView.setImage(url: URL(string: url), placeholder: placeholderImage)
        }
        
        switch viewElements.extraImage {
        case .image(let image):
            self.extraImageView.image = image
        case let .url(url, placeholderImage):
            self.extraImageView.setImage(url: URL(string: url), placeholder: placeholderImage)
        }
        
        self.primaryButton.setTitle(viewElements.primaryButtonTitle, for: .normal)
        self.secondaryButton.setTitle(viewElements.secondaryButtonTitle, for: .normal)
    }
}

extension CancellationConfirmationBaseView: ViewConfiguration {
    func buildViewHierarchy() {
        self.addSubview(containnerStackView)
        
        imageContainner.addSubview(mainImageView)
        imageContainner.addSubview(extraImageView)
        
        containnerStackView.addArrangedSubview(imageContainner)
        containnerStackView.addArrangedSubview(SpacerView(size: Layout.firstSpace))
        containnerStackView.addArrangedSubview(titleLabel)
        containnerStackView.addArrangedSubview(SpacerView(size: Layout.secondSpace))
        containnerStackView.addArrangedSubview(subtitleLabel)
        containnerStackView.addArrangedSubview(SpacerView(size: Layout.thirdSpace))
        containnerStackView.addArrangedSubview(primaryButton)
        
        if style == .question {
            containnerStackView.addArrangedSubview(SpacerView(size: Layout.fourthSpace))
            containnerStackView.addArrangedSubview(secondaryButton)
        }
    }
    
    func setupConstraints() {
        containnerStackView.layout {
            $0.centerX == centerXAnchor
            $0.centerY == centerYAnchor
        }
        
        imageContainner.layout {
            $0.width == Layout.containnerImageViewWidth
            $0.height == Layout.containnerImageViewHeigh
        }
        
        mainImageView.layout {
            $0.width == Layout.mainImageViewWidth
            $0.height == Layout.mainImageViewHeigh
        }
        
        extraImageView.layout {
            $0.width == Layout.extraImageViewWidth
            $0.height == Layout.extraImageViewHeight
            $0.trailing == imageContainner.trailingAnchor + Layout.extraImageViewTrailingSpace
            $0.bottom == imageContainner.bottomAnchor + Layout.extraImageViewBottomSpace
        }
        
        titleLabel.layout {
            $0.width == Layout.titleLabelWidth
        }
        
        subtitleLabel.layout {
            $0.width == Layout.subtitleLabelWidth
        }
        
        primaryButton.layout {
            $0.width == Layout.buttonWidth
            $0.height == Layout.buttonHeight
        }
        
        secondaryButton.layout {
            $0.width == Layout.buttonWidth
            $0.height == Layout.buttonHeight
        }
    }
    
    public func configureViews() {
        self.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    public func startLoading() {
        self.primaryButton.startLoadingAnimating()
    }
    
    public func stopLoading() {
        self.primaryButton.stopLoadingAnimating()
    }
    
    @objc
    private func buttonPressed() {
        self.delegate?.primaryButtonClicked(sender: self)
    }
    
    @objc
    private func secondaryButtonPressed() {
        self.delegate?.secondaryButtonClicked(sender: self)
    }
}

extension CancellationConfirmationBaseView.Layout {
    static let mainImageViewWidth: CGFloat = 100
    static let mainImageViewHeigh: CGFloat = 100
    static let mainImageViewCornerRadius: CGFloat = 50
    
    static let containnerImageViewWidth: CGFloat = 100
    static let containnerImageViewHeigh: CGFloat = 100
    
    static let extraImageViewWidth: CGFloat = 40
    static let extraImageViewHeight: CGFloat = 40
    static let extraImageViewBottomSpace: CGFloat = 0
    static let extraImageViewTrailingSpace: CGFloat = 0
    static let extraImageViewCornerRadius: CGFloat = 20
    
    static let titleLabelWidth: CGFloat = 280
    
    static let subtitleLabelWidth: CGFloat = 280
    
    static let buttonWidth: CGFloat = 280
    static let buttonHeight: CGFloat = 48
    static let buttonCornerRadius: CGFloat = 24
    static let buttonBorderWidth: CGFloat = 1
    static let buttonFontSize: CGFloat = 16
    
    static let firstSpace: CGFloat = 25
    static let secondSpace: CGFloat = 8
    static let thirdSpace: CGFloat = 24
    static let fourthSpace: CGFloat = 8
}
