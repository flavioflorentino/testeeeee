import Foundation
import Core

struct CancellationConfirmationEndPoint {
    let uuid: String
    let sellerCode: String
    let type: String
    let storeName: String?
    let userAuthorization: Bool
}

extension CancellationConfirmationEndPoint: ApiEndpointExposable {
    var path: String {
        "transactions/cancel/authorize"
    }
    var method: HTTPMethod {
        .patch
    }
    
    var body: Data? {
        [
            "uuid": uuid,
            "sellerCode": sellerCode,
            "type": type,
            "storeName": storeName as Any,
            "userAuthorization": userAuthorization
        ].toData()
    }
    
    var shouldAppendBody: Bool {
        true
    }
}
