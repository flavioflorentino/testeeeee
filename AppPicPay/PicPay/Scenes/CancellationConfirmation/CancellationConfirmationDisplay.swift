import UIKit

protocol CancellationConfirmationDisplay: AnyObject {
    func displayAskCancelationView(viewElements: CancellationConfirmationBaseView.ViewElements)
    func displayConfirmationCancelation(viewElements: CancellationConfirmationBaseView.ViewElements)
    func displayError(error: CancellationConfirmationViewController.CancellationConfirmationError)
    func displayConfigureView(viewSetup: CancellationConfirmationViewController.ViewSetup)
    func displayLoadStatus()
}
