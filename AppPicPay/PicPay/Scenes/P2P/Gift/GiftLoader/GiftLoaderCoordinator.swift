enum GiftLoaderAction {
    case newFriendGiftPayment(contact: PPContact, followingStatus: FollowerStatus, model: NewFriendGiftPaymentModel)
    case close
}

protocol GiftLoaderCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: GiftLoaderAction)
}

final class GiftLoaderCoordinator: GiftLoaderCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: GiftLoaderAction) {
        switch action {
        case let .newFriendGiftPayment(contact, followingStatus, model):
            let paymentOrchestrator = NewFriendGiftPaymentOrchestrator(
                contact: contact,
                followingStatus: followingStatus,
                model: model,
                dependencies: DependencyContainer()
            )
            let paymentController = paymentOrchestrator.paymentViewController
            viewController?.navigationController?.pushViewController(paymentController, animated: false)
            
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
