import Foundation

final class GiftLoaderFactory: NSObject {
    @objc
    static func make(userId: String) -> UIViewController {
        let service: GiftLoaderServicing = GiftLoaderService(dependencies: DependencyContainer())
        let coordinator: GiftLoaderCoordinating = GiftLoaderCoordinator()
        let presenter: GiftLoaderPresenting = GiftLoaderPresenter(coordinator: coordinator)
        let viewModel = GiftLoaderViewModel(service: service, presenter: presenter, userId: userId)
        let viewController = GiftLoaderViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
