protocol GiftLoaderViewModelInputs: AnyObject {
    func fetchData()
    func didClose()
}

final class GiftLoaderViewModel {
    private let service: GiftLoaderServicing
    private let presenter: GiftLoaderPresenting
    private let userId: String

    init(service: GiftLoaderServicing, presenter: GiftLoaderPresenting, userId: String) {
        self.service = service
        self.presenter = presenter
        self.userId = userId
    }
}

extension GiftLoaderViewModel: GiftLoaderViewModelInputs {
    func fetchData() {
        presenter.updateScreenTitle(service.newFriendGiftPaymentModel?.title)
        
        service.getConsumer(id: userId) { [weak self] result in
            switch result {
            case .success(let response):
                guard let model = self?.service.newFriendGiftPaymentModel else {
                    self?.presenter.didReceiveAnError(PicPayError(message: DefaultLocalizable.unexpectedError.text))
                    return
                }
                
                let action: GiftLoaderAction = .newFriendGiftPayment(
                    contact: response.contact,
                    followingStatus: response.followingStatus,
                    model: model
                )
                self?.presenter.didNextStep(action: action)
                
            case .failure(let error):
                self?.presenter.didReceiveAnError(error)
            }
        }
    }
    
    func didClose() {
        presenter.didNextStep(action: .close)
    }
}
