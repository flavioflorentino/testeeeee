import Core
import FeatureFlag

protocol GiftLoaderServicing {
    var newFriendGiftPaymentModel: NewFriendGiftPaymentModel? { get }
    func getConsumer(id: String, completion: @escaping(Result<GiftLoaderConsumerProfileResponse, PicPayError>) -> Void)
}

struct GiftLoaderConsumerProfileResponse {
    let contact: PPContact
    let followingStatus: FollowerStatus
}

final class GiftLoaderService: GiftLoaderServicing {
    typealias Dependencies = HasFeatureManager & HasMainQueue

    private let dependencies: Dependencies
    
    var newFriendGiftPaymentModel: NewFriendGiftPaymentModel? {
        dependencies.featureManager.object(.newFriendGiftTexts, type: NewFriendGiftPaymentModel.self)
    }
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func getConsumer(id: String, completion: @escaping(Result<GiftLoaderConsumerProfileResponse, PicPayError>) -> Void) {
        WSSocial.getConsumerProfile(id) { contact, followerStatus, _, error in
            self.dependencies.mainQueue.async {
                guard let contact = contact else {
                    let picpayError = PicPayError(message: error?.localizedDescription ?? DefaultLocalizable.unexpectedError.text)
                    completion(.failure(picpayError))
                    return
                }
                let response = GiftLoaderConsumerProfileResponse(contact: contact, followingStatus: followerStatus)
                completion(.success(response))
            }
        }
    }
}
