import UI
import UIKit

final class GiftLoaderViewController: ViewController<GiftLoaderViewModelInputs, UIView> {
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.hidesWhenStopped = true
        return activityIndicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchData()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(activityIndicator)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: DefaultLocalizable.btClose.text, style: .plain, target: self, action: #selector(didTapClose))
        activityIndicator.startAnimating()
    }
    
    override func setupConstraints() {
        activityIndicator.layout {
            $0.centerX == view.centerXAnchor
            $0.centerY == view.centerYAnchor
        }
    }
    
    @objc
    private func didTapClose() {
        viewModel.didClose()
    }
}

// MARK: View Model Outputs
extension GiftLoaderViewController: GiftLoaderDisplay {
    func updateScreenTitle(_ title: String?) {
        self.title = title
    }
    
    func displayAlert(_ alert: Alert) {
        AlertMessage.showAlert(alert, controller: self) { [weak self] _, _, _ in
            self?.viewModel.didClose()
        }
    }
}
