import UIKit

protocol GiftLoaderDisplay: AnyObject {
    func updateScreenTitle(_ title: String?)
    func displayAlert(_ alert: Alert)
}
