protocol GiftLoaderPresenting: AnyObject {
    var viewController: GiftLoaderDisplay? { get set }
    func updateScreenTitle(_ title: String?)
    func didNextStep(action: GiftLoaderAction)
    func didReceiveAnError(_ error: PicPayError)
}

final class GiftLoaderPresenter: GiftLoaderPresenting {
    private let coordinator: GiftLoaderCoordinating
    var viewController: GiftLoaderDisplay?

    init(coordinator: GiftLoaderCoordinating) {
        self.coordinator = coordinator
    }
    
    func updateScreenTitle(_ title: String?) {
        viewController?.updateScreenTitle(title)
    }
    
    func didNextStep(action: GiftLoaderAction) {
        coordinator.perform(action: action)
    }
    
    func didReceiveAnError(_ error: PicPayError) {
        let alert = Alert(with: error)
        viewController?.displayAlert(alert)
    }
}
