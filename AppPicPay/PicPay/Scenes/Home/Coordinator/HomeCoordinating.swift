import PIX
import UI

enum HomeCoordinatorAction {
    case pix
    case cashback
    case payWithPicPay
    case socialOnboarding
    case studentActive
    case reviewPrivacy
    case pixCopiedRandomKeyPopUp(keyType: KeyType, key: String)
    case pixCopyPastePopUp(code: String)
}

protocol HomeCoordinating: Coordinating {
    var didFinishFlow: (() -> Void)? { get set }
    
    func perform(action: HomeCoordinatorAction)
}
