import AnalyticsModule
import AssetsKit
import Core
import DirectMessageSB
import FeatureFlag
import Foundation
import PermissionsKit
import PIX
import UI
import UIKit

final class HomeCoordinator {
    typealias Dependencies = HasAnalytics & HasAppManager & HasAppParameters & HasFeatureManager & HasMainQueue
    typealias Factory = HomeControllerFactory
        & HomeNavigationBarComponentFactory
        & HomeSearchComponentFactory
        & HomeSuggestionsAndFavoritesFactory
        & HomePinnedBannerFactory
        & HomePromotionsAndMgmComponentFactory
        & HomeFeedComponentFactory
        & FeedComponentFactory
        & ConnectionStateManufacturing

    // MARK: - Properties
    private let navigationController: UINavigationController
    private let factory: Factory
    private let dependencies: Dependencies
    private var children: [Coordinating] = []
    private var favoritesFlowCoordinator: Coordinating?
    lazy var connectionStateInteractor: ConnectionStateInteracting = factory.makeInteractor(
        delegate: self, display: self, screen: .home
    )
    var childViewController: [UIViewController] = []
    var viewController: UIViewController?
    var didFinishFlow: (() -> Void)?

    // MARK: - Initialization
    init(navigationController: UINavigationController, factory: Factory, dependencies: Dependencies) {
        self.navigationController = navigationController
        self.factory = factory
        self.dependencies = dependencies
    }
}

// MARK: - HomeCoordinating
extension HomeCoordinator: HomeCoordinating {
    func start() {
        let controller = makeHomeViewController()
        navigationController.viewControllers = [controller]
    }
    
    func perform(action: HomeCoordinatorAction) {
        switch action {
        case .pix:
            showPixPopup()
        case .cashback:
            showCashbackPopup()
        case .payWithPicPay:
            showPayWithPicPayAlert()
        case .socialOnboarding:
            showSocialOnboarding()
        case .studentActive:
            showStudentActivePopup()
        case .reviewPrivacy:
            showReviewPrivacyFlow()
        case let .pixCopiedRandomKeyPopUp(keyType, key):
            showPixCopiedRandomKeyPopUp(keyType: keyType, key: key)
        case let .pixCopyPastePopUp(code):
            showPixCopyPastePopUp(code: code)
        }
    }
}

// MARK: - ModalRegistrationDelegate
extension HomeCoordinator: ModalRegistrationDelegate {
    func didTapRegisterKeyOnPix() {
        PIXConsumerFlowCoordinator(
            originViewController: navigationController,
            originFlow: .deeplink,
            destination: .keyManagement
        ).start()
    }
}

// MARK: - Private
private extension HomeCoordinator {
    func makeHomeViewController() -> HomeControllerFactory.Controller {
        factory.makeHomeControllerFactory(components: makeComponents(),
                                          coordinator: self,
                                          onViewWillDisappear: { [weak self] in
                                            self?.connectionStateInteractor.didCancelAction()
                                          })
    }

    // MARK: - Flows
    func runSearchFlow() {
        let searchController = SearchWrapper(ignorePermissionPrompt: true, dependencies: DependencyContainer())
        searchController.setTabs(tabs: [.main, .consumers, .places, .store], isOnlySearchEnabled: true)
        searchController.showContainerSearch = true
        searchController.shouldShowBackButton = true
        searchController.shouldFocusWhenAppear = true
        searchController.hidesBottomBarWhenPushed = true
        searchController.origin = .home

        navigationController.pushViewController(searchController, animated: true)
    }

    func runWalletFlow() {
        dependencies.analytics.log(ExperimentEvent.walletAccessedByBalance)
        dependencies.appManager.mainScreenCoordinator?.showWalletScreen()
    }

    func runPromotionsFlow() {
        let coordinator = PromotionsCoordinator(
            navigationController: navigationController,
            controllerFactory: PromotionsFactory(),
            coordinatorFactory: PromotionsFactory()
        )

        coordinator.didFinishFlow = { [weak self] in
            self?.children.removeAll(where: { $0 === coordinator })
        }

        coordinator.start()
        children.append(coordinator)

        dependencies.analytics.log(PromotionsEvent.promotionListAccessed(origin: .home))
    }

    func runSettingsFlow() {
        let settingsCoordinator = SettingsCoordinator(navigationController: navigationController)
        settingsCoordinator.start()

        dependencies.analytics.log(HomeEvent.settingsAccessed)
    }

    func runInviteFlow() {
        if dependencies.featureManager.isActive(.featureMgmIndicationCenter) {
            let controller = ReferralPaginationFactory.make()
            let navigationController = PPNavigationController(rootViewController: controller)
            navigationController.modalPresentationStyle = .fullScreen
            navigationController.visibleViewController?.present(navigationController, animated: true)
        } else {
            guard let viewController = ViewsManager
                    .peopleSearchStoryboardViewController("SocialShareCode") as? SocialShareCodeViewController else {
                return
            }

            dependencies.analytics.log(HomeEvent.invitePeople)
            viewController.hidesBottomBarWhenPushed = true
            navigationController.pushViewController(viewController, animated: true)
        }
    }

    func runScannerFlow() {
        dependencies.analytics.log(ScannerEvent.qrcodeAccessed(.home))
        PaginationCoordinator.shared.showScanner()
    }
    
    func runMessagesFlow() {
        let controller = ChatListFactory.make()
        navigationController.pushViewController(controller, animated: true)
    }

    func runPaymentFlow(section: LegacySearchResultSection, row: LegacySearchResultRow) {
        guard let controller = navigationController.topViewController else { return }
        SearchHelper.openPayment(withSection: section, row: row, from: controller, origin: .homeSugestions)
    }

    func runOpenFavoriteFlow(item: HomeFavorites.Item) {
        switch item {
        case let .action(value):
            runOpenFavoriteFlow(item: value)
        case let .consumer(value):
            runOpenFavoriteFlow(item: value)
        case let .digitalGood(value):
            runOpenFavoriteFlow(item: value)
        case let .store(value):
            runOpenFavoriteFlow(item: value)
        default:
            break
        }
    }

    func runOpenFavoriteFlow(item: HomeFavorites.ActionItem) {
        let coordinator = FavoriteFlowCoordinator(navigationController: navigationController)
        favoritesFlowCoordinator = coordinator

        switch item.action {
        case .favoriteOnboarding:
            dependencies.analytics.log(FavoritesEvent.favoritesOnboardingAccessed(.home))
            coordinator.goToOnboarding()
        case .allFavorites:
            dependencies.analytics.log(FavoritesEvent.favoritesListAccessed(.home))
            coordinator.goToFavoritesList()
        default:
            break
        }
    }

    func runOpenFavoriteFlow(item: HomeFavorites.ConsumerItem) {
        guard let controller = NewTransactionViewController.fromStoryboard() else {
            return
        }

        dependencies.analytics.log(ProfileEvent.touchPicture(from: .favorites))
        dependencies.analytics.log(CheckoutEvent.checkoutScreenViewed(CheckoutEvent.CheckoutEventOrigin.p2p))
        controller.touchOrigin = "Home"
        controller.showCancel = true
        controller.loadConsumerInfo(item.id)

        let navigation = PPNavigationController(rootViewController: controller)
        navigationController.visibleViewController?.present(navigation, animated: true)
    }

    func runOpenFavoriteFlow(item: HomeFavorites.DigitalGoodItem) {
        guard let viewController = navigationController.topViewController else { return }
        let digitalGood = dgItem(from: item)
        DGHelpers.openDigitalGoodFlow(for: digitalGood, viewController: viewController, origin: nil)
    }

    func runOpenFavoriteFlow(item: HomeFavorites.StoreItem) {
        guard item.store.isNewParking == false else {
            return openNewParkingPayment(item: item)
        }

        guard
            let store = PPStore(profileDictionary: storeDictionary(from: item)),
            let controller = ViewsManager.paymentStoryboardFirstViewController() as? PaymentViewController else {
            return
        }

        store.isParkingPayment = Int32(item.store.isParking)
        dependencies.analytics.log(
            CheckoutEvent.checkoutScreenViewed(
                store.isParkingPayment == 1 ? CheckoutEvent.CheckoutEventOrigin.parking : CheckoutEvent.CheckoutEventOrigin.p2m
            )
        )
        controller.store = store
        controller.touchOrigin = "Home"

        let navigation = PPNavigationController(rootViewController: controller)
        navigationController.visibleViewController?.present(navigation, animated: true)
    }

    // MARK: - Presentations
    func openNewParkingPayment(item: HomeFavorites.StoreItem) {
        let parkingViewController = ParkingCoordinator().start(sellerId: item.seller.id, storeId: item.store.id)
        dependencies.analytics.log(CheckoutEvent.checkoutScreenViewed(CheckoutEvent.CheckoutEventOrigin.parking))
        navigationController.visibleViewController?.present(parkingViewController, animated: true)
    }

    func showPixPopup() {
        let modal = ModalRegistrationFactory.make(delegate: self)
        navigationController.visibleViewController?.showPopup(modal)
    }

    func showCashbackPopup() {
        CashbackReceipWidgetItem.openCashbackPopup(in: navigationController.tabBarController ?? navigationController)
    }

    func showPayWithPicPayAlert() {
        let mainButton = Button(
            title: MGMIncentiveModalLocalizable.buttonTitle.text,
            type: .cta
        ) { [weak self] alertController, _ in
            self?.dependencies.analytics.log(IncentiveModalEvent.pay)

            alertController.dismiss(animated: true) { [weak self] in
                self?.dependencies.appManager.mainScreenCoordinator?.showPaymentScreen()
            }
        }

        let notNowButton = Button(
            title: MGMIncentiveModalLocalizable.notNow.text,
            type: .cleanUnderlined
        ) { [weak self] alertController, _ in
            self?.dependencies.analytics.log(IncentiveModalEvent.notNow)
            alertController.dismiss(animated: true)
        }

        let alert = Alert(
            with: MGMIncentiveModalLocalizable.title.text,
            text: MGMIncentiveModalLocalizable.subtitle.text,
            buttons: [
                mainButton,
                notNowButton
            ],
            image: Alert.Image(with: Assets.Icons.icoCoin.image)
        )

        AlertMessage.showAlert(alert, controller: navigationController.visibleViewController)
    }

    func showSocialOnboarding() {
        let onboardingAutorizeButton = Button(
            title: HomeLegacyLocalizable.authorizeContacts.text,
            type: .cta
        ) { [weak self] popupController, _ in
            popupController.dismiss(animated: true, completion: {
                self?.findFriends()
                self?.dependencies.appParameters.setOnboardSocial(false)
            })
        }
        let onboardingSkipButton = Button(
            title: DefaultLocalizable.notNow.text,
            type: .inline
        ) { [weak self] popupController, _ in
            popupController.dismiss(animated: true, completion: {
                self?.trackFindFriends(accept: false)
                self?.dependencies.appParameters.setOnboardSocial(false)
            })
        }

        let alert = Alert(
            with: HomeLegacyLocalizable.findYourFriends.text,
            text: HomeLegacyLocalizable.toFindOut.text,
            buttons: [onboardingAutorizeButton, onboardingSkipButton]
        )
        alert.image = Alert.Image(name: "onboarding_social_popup")

        AlertMessage.showAlert(alert, controller: navigationController.visibleViewController)
    }

    func showFollowSuggestions() {
        trackFindFriends(accept: true)

        guard let controller = FollowFriendsFactory.make(from: .home) else { return }

        let navigation = UINavigationController(rootViewController: controller)
        navigationController.visibleViewController?.present(navigation, animated: true)
    }

    func showStudentActivePopup() {
        let title = StudentLocalizable.activePopupTitle.text
        let message = String(
            format: StudentLocalizable.activePopupDescription.text,
            StudentLocalizable.activePopupBenefitsCashback.text
        )

        let range = (message as NSString).range(of: StudentLocalizable.activePopupBenefitsCashback.text)

        let benefitsAttributes = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .bold),
            NSAttributedString.Key.foregroundColor: Palette.ppColorBranding300.color
        ]
        let regularAttributes = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .regular),
            NSAttributedString.Key.foregroundColor: Palette.ppColorGrayscale500.color
        ]

        let attributedString = NSMutableAttributedString(string: message, attributes: regularAttributes)
        attributedString.addAttributes(benefitsAttributes, range: range)

        let image = Assets.Icons.iconUniversitario.image
        let popup = PopupViewController(title: title, description: message, preferredType: .image(image))
        dependencies.analytics.log(StudentAccountEvent.activatedAccountPopup)
        popup.setAttributeDescription(attributedString)

        let confirmAction = PopupAction(title: StudentLocalizable.activePopupButton.text, style: .fill) { [weak self] in
            self?.openStudentBenefits()
            self?.dependencies.analytics.log(StudentAccountEvent.benefits(from: .popup))
            popup.dismiss(animated: true)
        }

        let cancelAction = PopupAction(title: DefaultLocalizable.notNow.text, style: .default) {
            Analytics.shared.log(StudentAccountEvent.activatedAccountPopupDismiss)
            popup.dismiss(animated: true)
        }

        popup.didCloseDismiss = {
            Analytics.shared.log(StudentAccountEvent.activatedAccountPopupClose)
        }

        popup.addAction(confirmAction)
        popup.addAction(cancelAction)
        navigationController.visibleViewController?.showPopup(popup)
    }
    
    func showPixCopiedRandomKeyPopUp(keyType: KeyType, key: String) {
        let completion = {
            self.openPixKeySelector(keyType: keyType, key: key)
        }
        let subtitle = Strings.Home.copiedKeyPopUpSubtitle + key + Strings.Home.copiedKeyPopUpContinuationOfSubtitle
        showCopiedPixAlert(
            title: Strings.Home.copiedKeyPopUpTitle,
            subtitle: subtitle,
            buttonTitle: Strings.Home.copiedKeyPopUpButton,
            completion: completion
        )
    }
    
    func showPixCopyPastePopUp(code: String) {
        let completion = {
            self.openPixCopyPaste(code: code)
        }
        let subtitle = Strings.Home.copyPastePopUpSubtitle + code.substring(toIndex: min(code.length, 40)) +
            Strings.Home.copyPastePopUpContinuationOfSubtitle
        showCopiedPixAlert(
            title: Strings.Home.copyPastePopUpTitle,
            subtitle: subtitle,
            buttonTitle: Strings.Home.copyPastePopUpButton,
            completion: completion
        )
    }
    
    func showCopiedPixAlert(title: String, subtitle: String, buttonTitle: String, completion: @escaping () -> Void ) {
        navigationController.visibleViewController?.showApolloAlert(
            image: Resources.Icons.icoPix.image,
            title: title,
            attributedSubtitle: subtitle.attributedStringWith(
                normalFont: Typography.bodyPrimary().font(),
                highlightFont: Typography.bodyPrimary(.highlight).font(),
                normalColor: Colors.grayscale700.color,
                highlightColor: Colors.grayscale700.color,
                textAlignment: .center,
                underline: false
            ),
            primaryButtonAction: .init(title: buttonTitle, completion: completion),
            linkButtonAction: .init(title: Strings.Home.notNowButton, completion: {}),
            additionalContentView: nil,
            dismissCompletion: nil,
            dismissOnTouchOutside: false
        )
    }
    
    func openStudentBenefits() {
        let urlString = dependencies.featureManager.text(.universityAccountBenefitsWebview)
        guard let url = URL(string: urlString) else { return }

        let webViewController = WebViewFactory.make(with: url)
        webViewController.hidesBottomBarWhenPushed = true

        navigationController.pushViewController(webViewController, animated: true)
        navigationController.isNavigationBarHidden = false
    }
    
    func showReviewPrivacyFlow() {
        let viewController = UpdatePrivacyMessageFactory.make(origin: .home)
        navigationController.pushViewController(viewController, animated: true)
    }

    // MARK: - Helpers
    func storeDictionary(from item: HomeFavorites.StoreItem) -> [String: Any] {
        [
            "id": item.store.id,
            "seller_id": item.seller.id,
            "name": item.store.name,
            "address": item.store.address,
            "img_url": item.store.imageUrl?.absoluteString ?? "",
            "is_parking": "\(item.store.isParking)"
        ]
    }

    func dgItem(from item: HomeFavorites.DigitalGoodItem) -> DGItem {
        let digitalGood = DGItem(id: item.id, service: DGService(rawValue: item.service))
        digitalGood.name = item.name
        digitalGood.itemDescription = item.description
        digitalGood.sellerId = "\(item.sellerId)"
        digitalGood.displayType = DGDisplayType(rawValue: item.selectionType) ?? .grid
        digitalGood.imgUrl = item.imageUrl?.absoluteString
        digitalGood.infoUrl = item.infoUrl?.absoluteString
        digitalGood.longDescriptionMarkdown = item.descriptionLargeMarkdown
        digitalGood.bannerImgUrl = item.bannerImageUrl?.absoluteString
        digitalGood.isStudentAccount = item.studentAccount
        digitalGood.screenId = item.screenId ?? ""
        return digitalGood
    }

    func findFriends() {
        let permissionContact: Permission = PPPermission.contacts()
        guard permissionContact.status != .authorized else {
            return showFollowSuggestions()
        }

        PPPermission.requestContacts { [weak self] status in
            guard status == .authorized else { return }
            self?.showFollowSuggestions()
        }
    }

    func trackFindFriends(accept: Bool) {
        let properties = ["Procurou amigos": accept ? "Sim": "Não"]
        PPAnalytics.trackEvent("Onboarding social - Exibiu invite de seguir", properties: properties)
    }

    // MARK: - Components
    func makeComponents() -> [HomeComponent] {
        guard dependencies.featureManager.isActive(.featureNewFeed) else {
            let components: [HomeComponent?] = [
                makeHomeNavigationBarComponent(),
                makeHomeSearchComponent(),
                makeHomeSuggestionsAndFavoritesComponent(),
                makeHomePromotionsAndMgmComponent(),
                factory.makeHomePinnedBannerComponent(),
                makeHomeFeedComponent()
            ]

            return components.compactMap { $0 }
        }
        
        let components: [HomeComponent?] = [
            makeHomeNavigationBarComponent(),
            makeHomeSearchComponent(),
            makeHomeSuggestionsAndFavoritesComponent(),
            makeHomePromotionsAndMgmComponent(),
            makeFeedComponent()
        ]
        
        return components.compactMap { $0 }
    }

    func makeHomeNavigationBarComponent() -> HomeNavigationBarComponentFactory.Component {
        factory.makeHomeNavigationBarComponent(
            openConsumerWalletAction: { [weak self] in
                self?.runWalletFlow()
            },
            openPromotionsAction: { [weak self] in
                self?.runPromotionsFlow()
            },
            openInviteAction: { [weak self] in
                self?.runInviteFlow()
            },
            openScannerAction: { [weak self] in
                self?.runScannerFlow()
            },
            openSettingsAction: { [weak self] in
                self?.runSettingsFlow()
            },
            openMessagesAction: { [weak self] in
                self?.connectionStateInteractor.setConnectionActions()
            }
        )
    }

    func makeHomeSearchComponent() -> HomeSearchComponentFactory.Component {
        factory.makeHomeSearchComponent(openSearchAction: { [weak self] in
            self?.runSearchFlow()
        })
    }

    func makeHomeSuggestionsAndFavoritesComponent() -> HomeSuggestionsAndFavoritesFactory.Component {
        factory.makeHomeSuggestionsAndFavoritesComponent(
            openPaymentAction: { [weak self] paymentData in
                self?.runPaymentFlow(section: paymentData.section, row: paymentData.row)
            },
            openItemAction: { [weak self] item in
                self?.runOpenFavoriteFlow(item: item)
            }
        )
    }

    func makeHomeFeedComponent() -> HomeFeedComponentFactory.Component {
        factory.makeHomeFeedComponent()
    }
    
    func makeFeedComponent() -> FeedComponentFactory.Component {
        factory.makeFeedComponent()
    }

    func makeHomePromotionsAndMgmComponent() -> HomePromotionsAndMgmComponentFactory.Component? {
        guard dependencies.featureManager.isActive(.experimentNewAccessPromotionsMgmBool) else { return nil }
        return factory.makeHomePromotionsAndMgmComponent(openPromotionsAndMgmAction: { [weak self] in
            self?.runPromotionsFlow()
        })
    }
    
    func openPixKeySelector(keyType: KeyType, key: String) {
        let viewController = KeySelectorFactory.make(paymentOpening: PaymentPIXProxy(), selectedKeyType: keyType, key: key)
        let navContoller = UINavigationController(rootViewController: viewController)
        navigationController.present(navContoller, animated: true)
    }
    
    func openPixCopyPaste(code: String) {
        let viewController = CopyPastePFFactory.make(paymentOpening: PaymentPIXProxy(), origin: .home, code: code)
        let navContoller = UINavigationController(rootViewController: viewController)
        navigationController.present(navContoller, animated: true)
    }
}

extension HomeCoordinator: ConnectionStateSuccessDelegate {
    func didSucceedConnection() {
        runMessagesFlow()
    }
}

extension HomeCoordinator: ConnectionStateDisplaying, ConnectionStateSceneStandardFlow {
    var apolloLoader: ApolloLoadable? {
        navigationController.topViewController as? ApolloLoadable
    }

    func startLoading(loadingText: String = String()) {
        apolloLoader?.startApolloLoader(loadingText: loadingText)
    }

    func stopLoading(completion: (() -> Void)? = nil) {
        apolloLoader?.stopApolloLoader(completion: completion)
    }

    func showConnectionError(alert: ChatConnectionErrorAlert) {
        guard let viewController = navigationController.topViewController else { return }
        showConnectionError(alert: alert, in: viewController)
    }
}
