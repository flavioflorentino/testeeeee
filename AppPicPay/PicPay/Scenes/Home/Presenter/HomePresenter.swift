import PIX

final class HomePresenter {
    // MARK: - Properties
    weak var coordinator: HomeCoordinating?
    
    init(coordinator: HomeCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - HomePresenting
extension HomePresenter: HomePresenting {
    func presentPixKeyRegistration() {
        coordinator?.perform(action: .pix)
    }

    func presentCashbackReceipt() {
        coordinator?.perform(action: .cashback)
    }

    func presentPayWithPicPayAlert() {
        coordinator?.perform(action: .payWithPicPay)
    }

    func presentSocialOnboarding() {
        coordinator?.perform(action: .socialOnboarding)
    }

    func presentStudentActiveAlert() {
        coordinator?.perform(action: .studentActive)
    }
    
    func presentReviewPrivacy() {
        coordinator?.perform(action: .reviewPrivacy)
    }
    
    func presentPixCopiedRandomKeyPopUp(keyType: KeyType, key: String) {
        coordinator?.perform(action: .pixCopiedRandomKeyPopUp(keyType: keyType, key: key))
    }
    
    func presentPixCopyPastePopUp(code: String) {
        coordinator?.perform(action: .pixCopyPastePopUp(code: code))
    }
}
