import PIX

protocol HomePresenting {
    func presentPixKeyRegistration()
    func presentCashbackReceipt()
    func presentPayWithPicPayAlert()
    func presentSocialOnboarding()
    func presentStudentActiveAlert()
    func presentReviewPrivacy()
    func presentPixCopiedRandomKeyPopUp(keyType: KeyType, key: String)
    func presentPixCopyPastePopUp(code: String)
}
