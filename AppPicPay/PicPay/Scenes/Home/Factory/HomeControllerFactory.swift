import UI
import UIKit

protocol HomeControllerFactory {
    typealias Controller = UIViewController & HomeController & ApolloLoadable

    func makeHomeControllerFactory(components: [HomeComponent],
                                   coordinator: HomeCoordinating,
                                   onViewWillDisappear: (() -> Void)?) -> Controller
}
