import Advertising
import AsyncDisplayKit
import DirectMessageSB
import UI
import UIKit

final class HomeFactory {
    private lazy var dependencies = DependencyContainer()
}

// MARK: - HomeControllerFactory
extension HomeFactory: HomeControllerFactory {
    func makeHomeControllerFactory(components: [HomeComponent],
                                   coordinator: HomeCoordinating,
                                   onViewWillDisappear: (() -> Void)?) -> Controller {
        let presenter = HomePresenter(coordinator: coordinator)

        let interactor = HomeInteractor(
            presenter: presenter,
            mgmIncentiveVerifier: MGMIncentiveWorker(service: MGMIncentiveService(dependencies: dependencies)),
            dependencies: dependencies
        )
        let controller = HomeViewController(interactor: interactor, components: components, dependencies: dependencies)
        controller.onViewWillDisappear = onViewWillDisappear
        return controller
    }
}

// MARK: - HomeNavigationBarComponentFactory
extension HomeFactory: HomeNavigationBarComponentFactory {
    // swiftlint:disable:next function_parameter_count
    func makeHomeNavigationBarComponent(
        openConsumerWalletAction: (() -> Void)?,
        openPromotionsAction: (() -> Void)?,
        openInviteAction: (() -> Void)?,
        openScannerAction: (() -> Void)?,
        openSettingsAction: (() -> Void)?,
        openMessagesAction: (() -> Void)?
    ) -> HomeNavigationBarComponentFactory.Component {
        let presenter = HomeNavigationBarPresenter()
        let interactor = HomeNavigationBarInteractor(presenter: presenter, dependencies: dependencies)
        let component = HomeNavigationBarComponent(interactor: interactor, dependencies: dependencies)

        presenter.display = component
        presenter.openConsumerWalletAction = openConsumerWalletAction
        presenter.openPromotionsAction = openPromotionsAction
        presenter.openInviteAction = openInviteAction
        presenter.openScannerAction = openScannerAction
        presenter.openSettingsAction = openSettingsAction
        presenter.openMessagesAction = openMessagesAction

        return component
    }
}

// MARK: - HomeSearchComponentFactory
extension HomeFactory: HomeSearchComponentFactory {
    func makeHomeSearchComponent(openSearchAction: (() -> Void)?) -> HomeSearchComponentFactory.Component {
        let presenter = HomeSearchPresenter()
        let interactor = HomeSearchInteractor(presenter: presenter)
        let component = HomeSearchComponent(interactor: interactor, dependencies: dependencies)

        presenter.openSearchAction = openSearchAction

        return component
    }
}

// MARK: - HomeSuggestionsComponentFactory
extension HomeFactory: HomeSuggestionsComponentFactory {
    func makeHomeSuggestionsComponent(openPaymentAction: ((PaymentData) -> Void)?) -> HomeSuggestionsComponentFactory.Component {
        let presenter = HomeSuggestionsPresenter()
        let interactor = HomeSuggestionsInteractor(
            presenter: presenter,
            service: SuggestionsServiceWrapper(dependencies: dependencies),
            dependencies: dependencies
        )

        let component = HomeSuggestionsComponent(interactor: interactor)

        presenter.display = component
        presenter.openPaymentAction = openPaymentAction

        return component
    }
}

// MARK: - HomeFavoritesComponentFactory
extension HomeFactory: HomeFavoritesComponentFactory {
    func makeHomeFavoritesComponent(openItemAction: ((HomeFavorites.Item) -> Void)?) -> HomeFavoritesComponentFactory.Component {
        let presenter = HomeFavoritesPresenter()
        let interactor = HomeFavoritesInteractor(presenter: presenter, service: FavoritesService(dependencies: dependencies))
        let component = HomeFavoritesComponent(interactor: interactor)

        presenter.display = component
        presenter.openItemAction = openItemAction

        return component
    }
}

// MARK: - HomeSuggestionsAndFavoritesFactory
extension HomeFactory: HomeSuggestionsAndFavoritesFactory {
    func makeHomeSuggestionsAndFavoritesComponent(
        openPaymentAction: ((PaymentData) -> Void)?,
        openItemAction: ((HomeFavorites.Item) -> Void)?
    ) -> HomeSuggestionsAndFavoritesFactory.Component {
        let suggestionsComponent = makeHomeSuggestionsComponent(openPaymentAction: openPaymentAction)
        let favoritesComponent = makeHomeFavoritesComponent(openItemAction: openItemAction)

        return HomeSuggestionsAndFavoritesComponent(
            suggestionsComponent: suggestionsComponent,
            favoritesComponent: favoritesComponent,
            dependencies: dependencies
        )
    }
}

// MARK: - HomePromotionsAndMgmComponentFactory
extension HomeFactory: HomePromotionsAndMgmComponentFactory {
    func makeHomePromotionsAndMgmComponent(
        openPromotionsAndMgmAction: (() -> Void)?
    ) -> HomePromotionsAndMgmComponentFactory.Component {
        let presenter = HomePromotionsAndMgmPresenter()
        presenter.openPromotionsAndMgmAction = openPromotionsAndMgmAction

        let interactor = HomePromotionsAndMgmInteractor(presenter: presenter)

        return HomePromotionsAndMgmComponent(interactor: interactor)
    }
}

// MARK: - HomePinnedBannerFactory
extension HomeFactory: HomePinnedBannerFactory {
    func makeHomePinnedBannerComponent() -> HomePinnedBannerFactory.Component {
        HomePinnedBannerComponent(
            bannerContainerView: ContainerPinnedBannersFactory.make(source: .home),
            dependencies: dependencies
        )
    }
}

// MARK: - HomeFeedComponentFactory
extension HomeFactory: HomeFeedComponentFactory {
    func makeHomeFeedComponent() -> HomeFeedComponentFactory.Component {
        HomeFeedComponent(
            allFeedViewController: makeFeedViewController(visibility: .Friends),
            myFeedViewController: makeFeedViewController(visibility: .Private),
            dependencies: dependencies
        )
    }
}

// MARK: - FeedComponentFactory
extension HomeFactory: FeedComponentFactory {
    func makeFeedComponent() -> FeedComponentFactory.Component {
        FeedComponent()
    }
}

// MARK: - Private
private extension HomeFactory {
    func makeFeedViewController(visibility: FeedItemVisibility) -> FeedViewController {
        let node = ASTableNode(style: .plain)
        node.view.isScrollEnabled = false
        node.view.bounces = false

        let controller = FeedViewController(tableView: node)
        controller.isScrollViewEnable = false
        controller.changeFilter(visibilityInt(from: visibility))
        controller.configureModel()
        controller.configureTableView()
        controller.tableNode.backgroundColor = Colors.backgroundPrimary.color

        return controller
    }

    func visibilityInt(from visibility: FeedItemVisibility) -> Int {
        Int(visibility.rawValue) ?? FeedItemVisibilityInt.friends.rawValue
    }
}

extension HomeFactory: ConnectionStateManufacturing {
    func makeInteractor(delegate: ConnectionStateSuccessDelegate?,
                        display: ConnectionStateDisplaying?,
                        screen: TrackedScreen?) -> ConnectionStateInteracting {
        dependencies.connectionStateFactory.makeInteractor(
            delegate: delegate,
            display: display,
            screen: .home
        )
    }

    func makeCoordinator(viewController: ConnectionStateViewController?,
                         delegate: ConnectionStateSuccessDelegate?,
                         screen: TrackedScreen?) -> ConnectionStateCoordinating {
        dependencies.connectionStateFactory.makeCoordinator(
            viewController: viewController,
            delegate: delegate,
            screen: .home
        )
    }
}
