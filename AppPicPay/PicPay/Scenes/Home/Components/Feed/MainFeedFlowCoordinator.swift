import Foundation
import Feed
import PIX
import UI
import UIKit

final class MainFeedFlowCoordinator {
    private weak var rootViewController: UIViewController?
    private lazy var componentCoordinator: FeedFlowCoordinating = {
        let coordinator = FeedFlowCoordinator()
        coordinator.delegate = self
        return coordinator
    }()
    
    init(viewController: UIViewController) {
        rootViewController = viewController
    }
    
    func startFeed(options: FeedOptions, hasRefresh: Bool) -> Feed.FeedViewController {
        componentCoordinator.start(options: options, hasRefresh: hasRefresh)
    }
    
    func startFeedDetail(cardId: UUID) -> UIViewController {
        componentCoordinator.start(cardId: cardId)
    }
}

extension MainFeedFlowCoordinator: FeedFlowCoordinatorDelegate {
    func openPixRefundFlow(_ transactionId: String) {
        guard let rootViewController = rootViewController else {
            return 
        }
        let flow: PIXConsumerFlow = .refund(paymentOpening: PaymentPIXProxy(), transactionId: transactionId)
        let flowCoordinator = PIXConsumerFlowCoordinator(
            originViewController: rootViewController,
            originFlow: .undefined,
            destination: flow
        )
        flowCoordinator.start()
    }
}
