import CoreLegacy

protocol HomeFeedController: AnyObject {
    typealias Dependencies = HasAppParameters

    init(allFeedViewController: FeedViewController, myFeedViewController: FeedViewController, dependencies: Dependencies)
}
