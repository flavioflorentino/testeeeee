import SnapKit
import Feed
import PIX
import UI
import UIKit

protocol FeedComponentFactory {
    typealias Component = HomeComponent & HomeComponentRefreshable & HomeComponentPaginableScrollable
    func makeFeedComponent() -> Component
}

final class FeedComponent: HomeComponent {
    private lazy var mainFeedFlowCoordinator = MainFeedFlowCoordinator(viewController: self)
    
    private lazy var componentViewController: FeedComponentAdaptering = {
        mainFeedFlowCoordinator.startFeed(options: .mainOptions, hasRefresh: false)
    }()
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension FeedComponent: ViewConfiguration {
    func buildViewHierarchy() {
        componentViewController.willMove(toParent: self)
        view.addSubview(componentViewController.view)
        addChild(componentViewController)
        componentViewController.didMove(toParent: self)
    }
    
    func setupConstraints() {
        componentViewController.view.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

extension FeedComponent: HomeComponentRefreshable {
    func refresh(_ finish: @escaping () -> Void) {
        componentViewController.refresh(finish)
    }
}

extension FeedComponent: HomeComponentPaginableScrollable {
    var scrollViews: [UIScrollView] { componentViewController.scrollViews }
    var pinnedView: UIView? { componentViewController.pinnedView }
    
    func currentScrollView() -> UIScrollView? {
        componentViewController.scrollView
    }
}
