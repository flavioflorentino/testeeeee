import CoreLegacy
import Foundation
import SnapKit
import UI
import UIKit

// MARK: - Layout
private extension HomeFeedComponent.Layout {
    enum VisibilitySelector {
        static let height: CGFloat = 50.0
    }
}

final class HomeFeedComponent: HomeComponent, HomeFeedController {
    // MARK: - Nested types
    fileprivate enum Layout { }

    // MARK: - Properties
    private let allFeedViewController: FeedViewController
    private let myFeedViewController: FeedViewController
    private let dependencies: Dependencies

    private var finishRefreshAction: (() -> Void)?

    private var feedNeedReloadNotificationObserver: NSObjectProtocol?
    private weak var currentFeedController: FeedViewController?

    private lazy var visibilitySelector: LegacyHomeFeedHeaderView = {
        let view = LegacyHomeFeedHeaderView()
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        view.delegate = self

        return view
    }()

    private lazy var pageController: UIPageViewController = {
        let pageController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageController.view.backgroundColor = .backgroundPrimary()

        return pageController
    }()

    // MARK: - Initialization
    required init(
        allFeedViewController: FeedViewController,
        myFeedViewController: FeedViewController,
        dependencies: Dependencies
    ) {
        self.allFeedViewController = allFeedViewController
        self.myFeedViewController = myFeedViewController
        self.dependencies = dependencies

        super.init(nibName: nil, bundle: nil)

        buildLayout()

        allFeedViewController.delegate = self
        myFeedViewController.delegate = self
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        set(visibility: .Friends)
        currentFeedController?.reloadFeed()

        feedNeedReloadNotificationObserver = NotificationCenter.default.addObserver(
            forName: NSNotification.Name(rawValue: "FeedNeedReload"),
            object: nil,
            queue: .main,
            using: { [weak self] _ in
                self?.currentFeedController?.reloadFeed()
            }
        )
    }
}

// MARK: - ViewConfiguration
extension HomeFeedComponent: ViewConfiguration {
    func buildViewHierarchy() {
        view.addSubview(visibilitySelector)

        pageController.willMove(toParent: self)
        view.addSubview(pageController.view)

        addChild(pageController)
        pageController.didMove(toParent: self)
    }

    func setupConstraints() {
        visibilitySelector.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.VisibilitySelector.height)
        }

        pageController.view.snp.makeConstraints {
            $0.top.equalTo(visibilitySelector.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }

    func configureViews() {
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
}

// MARK: - HomeComponentRefreshable
extension HomeFeedComponent: HomeComponentRefreshable {
    func refresh(_ finish: @escaping () -> Void) {
        currentFeedController?.reloadFeed()
        finishRefreshAction = finish
    }
}

// MARK: - HomeComponentPaginableScrollable
extension HomeFeedComponent: HomeComponentPaginableScrollable {
    var scrollViews: [UIScrollView] {
        [allFeedViewController.tableNode.view, myFeedViewController.tableNode.view]
    }

    var pinnedView: UIView? { visibilitySelector }

    func currentScrollView() -> UIScrollView? {
        currentFeedController?.tableNode.view
    }
}

// MARK: - LegacyHomeFeedHeaderDelegate
extension HomeFeedComponent: LegacyHomeFeedHeaderDelegate {
    func feedHeaderDidChangeVisibility(visibility: FeedItemVisibility) {
        set(visibility: visibility)
    }
}

// MARK: - FeedViewControllerDelegate
extension HomeFeedComponent: FeedViewControllerDelegate {
    func feedScrollViewDidScroll(_ scrollView: UIScrollView) { }

    func feedWillReload() { }

    func feedDidLoadNewPage() { }

    func feedDidReload() {
        finishRefreshAction?()
        finishRefreshAction = nil

        guard dependencies.appParameters.hasAtLeastOneFeedOnce()
                && allFeedViewController.model?.isLoading == false else { return }

        var alpha: CGFloat = 0.0
        if allFeedViewController.model?.items.isNotEmpty == true {
            alpha = 1.0
            dependencies.appParameters.setHasAtLeastOneFeedOnce(true)
        }

        UIView.animate(withDuration: 0.25) {
            self.visibilitySelector.toolbarView.alpha = alpha
        }
    }
}

// MARK: - Private
private extension HomeFeedComponent {
    func set(visibility: FeedItemVisibility) {
        switch visibility {
        case .Friends:
            pageController.setViewControllers([allFeedViewController], direction: .reverse, animated: true, completion: nil)
            currentFeedController = allFeedViewController
        case .Private:
            pageController.setViewControllers([myFeedViewController], direction: .forward, animated: true, completion: nil)
            currentFeedController = myFeedViewController
        }

        visibilitySelector.selectVisibilty(visibility)

        let crumb = visibility == .Friends ? HomeLegacyLocalizable.all.text : HomeLegacyLocalizable.you.text
        BreadcrumbManager.shared.addCrumb(crumb, typeOf: .tab, withProperties: nil)
    }
}
