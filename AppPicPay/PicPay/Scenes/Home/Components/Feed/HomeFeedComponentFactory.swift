import UIKit

protocol HomeFeedComponentFactory {
    typealias Component = HomeComponent & HomeComponentRefreshable & HomeComponentPaginableScrollable & HomeFeedController

    func makeHomeFeedComponent() -> Component
}
