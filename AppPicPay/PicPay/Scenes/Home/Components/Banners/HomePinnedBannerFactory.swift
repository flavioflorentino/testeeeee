import UIKit

protocol HomePinnedBannerFactory {
    typealias Component = HomeComponent & HomeComponentRefreshable & HomePinnedBannerController

    func makeHomePinnedBannerComponent() -> Component
}
