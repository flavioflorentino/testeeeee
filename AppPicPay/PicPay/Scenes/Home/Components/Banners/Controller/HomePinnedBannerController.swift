import Advertising
import FeatureFlag

protocol HomePinnedBannerController: AnyObject {
    typealias Dependencies = HasFeatureManager

    init(bannerContainerView: ContainerPinnedBannersView, dependencies: Dependencies)
}
