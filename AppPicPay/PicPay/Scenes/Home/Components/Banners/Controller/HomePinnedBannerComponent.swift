import Advertising
import FeatureFlag
import SnapKit
import UI
import UIKit

final class HomePinnedBannerComponent: HomeComponent, HomePinnedBannerController {
    // MARK: - Typea alias
    typealias Dependencies = HasFeatureManager

    // MARK: - Properties
    private let containerView = UIView()
    private let bannerContainerView: ContainerPinnedBannersView
    private let dependencies: Dependencies

    // MARK: - Initialization
    required init(bannerContainerView: ContainerPinnedBannersView, dependencies: Dependencies) {
        self.bannerContainerView = bannerContainerView
        self.dependencies = dependencies

        super.init(nibName: nil, bundle: nil)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        refreshPinnedBanners()
    }
}

// MARK: - ViewConfiguration
extension HomePinnedBannerComponent: ViewConfiguration {
    func buildViewHierarchy() {
        view.addSubview(containerView)
        containerView.addSubview(bannerContainerView)
    }

    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        bannerContainerView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base00)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
        }

        bannerContainerView.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
    }

    func configureViews() {
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        containerView.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
}

// MARK: - HomeComponentRefreshable
extension HomePinnedBannerComponent: HomeComponentRefreshable {
    func refresh(_ finish: () -> Void) {
        refreshPinnedBanners()
        finish()
    }
}

// MARK: - Private
private extension HomePinnedBannerComponent {
    func refreshPinnedBanners() {
        guard dependencies.featureManager.isActive(.opsCardPinnedBannersHomeBool) else { return }
        bannerContainerView.refresh()
    }
}
