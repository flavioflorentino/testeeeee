import UIKit

protocol HomePromotionsAndMgmComponentFactory {
    typealias Component = HomeComponent & HomePromotionsAndMgmController

    func makeHomePromotionsAndMgmComponent(openPromotionsAndMgmAction: (() -> Void)?) -> Component
}
