final class HomePromotionsAndMgmPresenter {
    var openPromotionsAndMgmAction: (() -> Void)?
}

// MARK: - HomePromotionsAndMgmPresenting
extension HomePromotionsAndMgmPresenter: HomePromotionsAndMgmPresenting {
    func openPromotionsAndMgm() {
        openPromotionsAndMgmAction?()
    }
}
