final class HomePromotionsAndMgmInteractor {
    // MARK: - Properties
    private let presenter: HomePromotionsAndMgmPresenting

    // MARK: - Initialization
    init(presenter: HomePromotionsAndMgmPresenting) {
        self.presenter = presenter
    }
}

// MARK: - HomePromotionsAndMgmInteracting
extension HomePromotionsAndMgmInteractor: HomePromotionsAndMgmInteracting {
    func tapAtPromotionsAndMgm() {
        presenter.openPromotionsAndMgm()
    }
}
