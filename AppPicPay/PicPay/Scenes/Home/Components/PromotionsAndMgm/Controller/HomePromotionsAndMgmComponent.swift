import SnapKit
import UI
import UIKit

// MARK: - Layout
private extension HomePromotionsAndMgmComponent.Layout {
    enum UnifiedPromoMGMView {
        static let height: CGFloat = Spacing.base08
    }
}

final class HomePromotionsAndMgmComponent: HomeComponent, HomePromotionsAndMgmController {
    // MARK: - Nested types
    fileprivate enum Layout { }

    // MARK: - Properties
    private let interactor: HomePromotionsAndMgmInteracting

    private lazy var containerView: UIView = {
        let view = UIView()
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        view.addSubview(unifiedPromoMgmView)

        return view
    }()

    private lazy var unifiedPromoMgmView: UnifiedPromoMGMView = {
        let view = UnifiedPromoMGMView()

        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        view.onViewTap = { [weak self] in
            self?.didTapAtPromoAndMgmView()
        }

        return view
    }()

    // MARK: - Initialization
    required init(interactor: HomePromotionsAndMgmInteracting) {
        self.interactor = interactor

        super.init(nibName: nil, bundle: nil)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration
extension HomePromotionsAndMgmComponent: ViewConfiguration {
    func buildViewHierarchy() {
        view.addSubview(containerView)
    }

    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        unifiedPromoMgmView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base01)
            $0.height.equalTo(Layout.UnifiedPromoMGMView.height)
        }
    }

    func configureViews() {
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
}

// MARK: - Private
private extension HomePromotionsAndMgmComponent {
    func didTapAtPromoAndMgmView() {
        self.interactor.tapAtPromotionsAndMgm()
    }
}
