import AnalyticsModule
import FeatureFlag
import SnapKit
import UI
import UIKit

private extension HomeSuggestionsAndFavoritesComponent.Layout {
    enum Line {
        static let height: CGFloat = 2.0
    }

    enum SegmentedControl {
        static let height: CGFloat = 25.0
        static let leading: CGFloat = Spacing.base02
    }

    enum PageControl {
        static let height: CGFloat = 124.0
    }
}

final class HomeSuggestionsAndFavoritesComponent: HomeComponent, HomeSuggestionsAndFavoritesController {
    typealias Dependencies = HasAnalytics & HasFeatureManager

    typealias Component = HomeComponent & HomeComponentRefreshable
    typealias SuggestionsComponent = Component & HomeSuggestionsController
    typealias FavoritesComponent = Component & HomeFavoritesController

    // MARK: - Nested types
    fileprivate enum Layout { }

    // MARK: - Properties
    private let dependencies: Dependencies
    private let suggestionsComponent: SuggestionsComponent
    private let favoritesComponent: FavoritesComponent

    private weak var currentComponent: Component?

    private lazy var containerView: UIView = {
        let view = UIView()
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))

        return view
    }()

    private lazy var segmentedControl: CustomSegmentedControl = {
        let view = CustomSegmentedControl(
            buttonTitles: [
                FavoriteLocalizable.favoriteSegmentedControlOpt1.text,
                FavoriteLocalizable.favoriteSegmentedControlOpt2.text
            ]
        )

        view.textColor = Colors.grayscale700.color
        view.selectorViewColor = Colors.grayscale700.color
        view.selectorTextColor = Colors.grayscale700.color
        view.selectorBottomMargin = .zero
        view.delegate = self
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))

        return view
    }()

    private lazy var pageController: UIPageViewController = {
        let pageController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageController.view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))

        return pageController
    }()

    private lazy var lineView: UIView = {
        let view = UIView()
        view.viewStyle(BackgroundViewStyle(color: .backgroundSecondary()))
        view.isHidden = dependencies.featureManager.isActive(.experimentNewAccessPromotionsMgmBool)

        return view
    }()

    // MARK: - Initialization
    required init(suggestionsComponent: SuggestionsComponent, favoritesComponent: FavoritesComponent, dependencies: Dependencies) {
        self.dependencies = dependencies
        self.suggestionsComponent = suggestionsComponent
        self.favoritesComponent = favoritesComponent

        super.init(nibName: nil, bundle: nil)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        show(component: components.first)
    }
}

// MARK: - ViewConfiguration
extension HomeSuggestionsAndFavoritesComponent: ViewConfiguration {
    func buildViewHierarchy() {
        view.addSubview(containerView)

        containerView.addSubview(segmentedControl)

        pageController.willMove(toParent: self)
        containerView.addSubview(pageController.view)

        addChild(pageController)
        pageController.didMove(toParent: self)

        containerView.addSubview(lineView)
    }

    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        segmentedControl.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview().offset(Layout.SegmentedControl.leading)
            $0.height.equalTo(Layout.SegmentedControl.height)
        }

        pageController.view.snp.makeConstraints {
            $0.top.equalTo(segmentedControl.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.PageControl.height)
        }

        lineView.snp.makeConstraints {
            $0.top.equalTo(pageController.view.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.bottom.equalToSuperview()
            $0.height.equalTo(Layout.Line.height)
        }
    }
}

// MARK: - HomeComponentRefreshable
extension HomeSuggestionsAndFavoritesComponent: HomeComponentRefreshable {
    func refresh(_ finish: @escaping () -> Void) {
        currentComponent?.refresh(finish)
    }
}

// MARK: - CustomSegmentedControlDelegate
extension HomeSuggestionsAndFavoritesComponent: CustomSegmentedControlDelegate {
    func changeToIndex(index: Int) {
        guard let component = components[safe: index] else { return }
        show(component: component)
    }
}

// MARK: - Private
private extension HomeSuggestionsAndFavoritesComponent {
    var components: [Component] {
        [suggestionsComponent, favoritesComponent]
    }

    func show(component: Component?) {
        guard let component = component else { return }

        pageController.setViewControllers([component], direction: .reverse, animated: false, completion: nil)
        currentComponent = component
    }
}
