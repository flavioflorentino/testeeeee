import AnalyticsModule
import FeatureFlag

protocol HomeSuggestionsAndFavoritesController: AnyObject {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    typealias Component = HomeComponent & HomeComponentRefreshable
    typealias SuggestionsComponent = Component & HomeSuggestionsController
    typealias FavoritesComponent = Component & HomeFavoritesController

    init(suggestionsComponent: SuggestionsComponent, favoritesComponent: FavoritesComponent, dependencies: Dependencies)
}
