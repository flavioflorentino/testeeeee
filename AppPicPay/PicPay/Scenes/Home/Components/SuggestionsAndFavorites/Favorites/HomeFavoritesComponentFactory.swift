import UIKit

protocol HomeFavoritesComponentFactory {
    typealias Component = HomeComponent & HomeComponentRefreshable & HomeFavoritesController

    func makeHomeFavoritesComponent(openItemAction: ((HomeFavorites.Item) -> Void)?) -> Component
}
