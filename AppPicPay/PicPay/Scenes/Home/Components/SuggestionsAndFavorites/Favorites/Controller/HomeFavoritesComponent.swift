import UI
import UIKit

private extension HomeFavoritesComponent.Layout {
    enum Carousel {
        static let height: CGFloat = 124.0
    }
}

final class HomeFavoritesComponent: ViewController<HomeFavoritesInteracting, UIView>, HomeFavoritesController {
    // MARK: - Nested types
    fileprivate enum Layout { }

    // MARK: - Properties
    private var finishLoadAction: (() -> Void)?

    private lazy var containerView: UIView = {
        let view = UIView()
        view.addSubviews(activityIndicatorView, errorView, collectionView)

        return view
    }()

    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        var indicatorStyle: UIActivityIndicatorView.Style = .gray
        if #available(iOS 13.0, *) {
            indicatorStyle = .medium
        } else if #available(iOS 12.0, *), (traitCollection.userInterfaceStyle == .dark) {
            indicatorStyle = .white
        }

        let activityIndicatorView = UIActivityIndicatorView(style: indicatorStyle)
        activityIndicatorView.hidesWhenStopped = true

        return activityIndicatorView
    }()

    private lazy var errorView: FavoritesOnboardingLoadingErrorView = {
        let view = FavoritesOnboardingLoadingErrorView()
        view.isHidden = true
        view.delegate = self

        return view
    }()

    private lazy var collectionView: FavoriteCarouselCollectionView = {
        let collectionView = FavoriteCarouselCollectionView()
        collectionView.favoritesDelegate = self

        return collectionView
    }()

    // MARK: - View lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        interactor.fetchFavorites()
    }

    // MARK: - Setup
    override func buildViewHierarchy() {
        super.buildViewHierarchy()

        view.addSubview(containerView)
    }

    override func setupConstraints() {
        super.setupConstraints()

        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        collectionView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.equalTo(Layout.Carousel.height)
        }

        errorView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.trailing.bottom.equalToSuperview()
        }

        activityIndicatorView.snp.makeConstraints {
            $0.center.equalTo(collectionView.snp.center)
        }
    }

    override func configureViews() {
        super.configureViews()

        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        containerView.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
}

// MARK: - HomeComponentRefreshable
extension HomeFavoritesComponent: HomeComponentRefreshable {
    func refresh(_ finish: @escaping () -> Void) {
        finishLoadAction = finish
        interactor.fetchFavorites()
    }
}

// MARK: - RecentesCollectionViewDelegate
extension HomeFavoritesComponent: FavoriteCarouselCollectionViewDelegate {
    func didSelect(item: HomeFavorites.Item) {
        interactor.didSetect(item: item)
    }
}

// MARK: - FavoritesOnboardingLoadingErrorViewDelegate
extension HomeFavoritesComponent: FavoritesOnboardingLoadingErrorViewDelegate {
    func reloadFavoritesCollection() {
        interactor.fetchFavorites()
    }
}

// MARK: - HomeFavoritesDisplay
extension HomeFavoritesComponent: HomeFavoritesDisplay {
    func show(favorites: [HomeFavorites.Item]) {
        collectionView.setup(result: favorites, completion: {})
    }

    func showError() {
        collectionView.isHidden = true
        errorView.isHidden = false
    }

    func hideError() {
        errorView.isHidden = true
        collectionView.isHidden = false
    }

    func showLoading() {
        collectionView.isHidden = true
        errorView.isHidden = true

        activityIndicatorView.startAnimating()
    }

    func hideLoading() {
        activityIndicatorView.stopAnimating()

        collectionView.isHidden = false
        errorView.isHidden = true

        finishLoadAction?()
        finishLoadAction = nil
    }
}
