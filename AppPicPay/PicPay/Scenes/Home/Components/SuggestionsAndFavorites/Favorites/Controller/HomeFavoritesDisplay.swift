protocol HomeFavoritesDisplay: AnyObject {
    func show(favorites: [HomeFavorites.Item])

    func showError()
    func hideError()

    func showLoading()
    func hideLoading()
}
