import Foundation

final class HomeFavoritesInteractor {
    // MARK: - Properties
    private let presenter: HomeFavoritesPresenting
    private let service: FavoritesServicing

    // MARK: - Initialization
    init(presenter: HomeFavoritesPresenting, service: FavoritesServicing) {
        self.presenter = presenter
        self.service = service
    }
}

// MARK: - HomeFavoritesInteracting
extension HomeFavoritesInteractor: HomeFavoritesInteracting {
    func fetchFavorites() {
        presenter.showLoading()
        service.homeFavorites { [weak self] result in
            self?.presenter.hideLoading()

            switch result {
            case let .success(favorites):
                let favorites = self?.items(from: favorites) ?? []
                guard favorites.isNotEmpty else {
                    self?.presenter.present(error: .unknown(nil))
                    return
                }

                self?.presenter.present(favorites: favorites)
            case let .failure(error):
                self?.presenter.present(error: error)
            }
        }
    }

    func didSetect(item: HomeFavorites.Item) {
        presenter.open(item: item)
    }
}

// MARK: - Private
private extension HomeFavoritesInteractor {
    func items(from favorites: [HomeFavorites]) -> [HomeFavorites.Item] {
        favorites.flatMap { $0.items }.filter {
            if case HomeFavorites.Item.unknown = $0 {
                return false
            }

            return true
        }
    }
}
