protocol HomeFavoritesInteracting {
    func fetchFavorites()
    func didSetect(item: HomeFavorites.Item)
}
