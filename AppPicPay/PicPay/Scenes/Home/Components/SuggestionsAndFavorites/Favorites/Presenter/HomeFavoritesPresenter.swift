import Core

final class HomeFavoritesPresenter {
    // MARK: - Properties
    var openItemAction: ((HomeFavorites.Item) -> Void)?

    weak var display: HomeFavoritesDisplay?
}

// MARK: - HomeFavoritesPresenting
extension HomeFavoritesPresenter: HomeFavoritesPresenting {
    func present(error: ApiError) {
        display?.showError()
    }

    func present(favorites: [HomeFavorites.Item]) {
        display?.hideError()
        display?.show(favorites: favorites)
    }

    func open(item: HomeFavorites.Item) {
        openItemAction?(item)
    }

    func showLoading() {
        display?.showLoading()
    }

    func hideLoading() {
        display?.hideLoading()
    }
}
