import Core

protocol HomeFavoritesPresenting {
    func present(error: ApiError)
    func present(favorites: [HomeFavorites.Item])

    func open(item: HomeFavorites.Item)

    func showLoading()
    func hideLoading()
}
