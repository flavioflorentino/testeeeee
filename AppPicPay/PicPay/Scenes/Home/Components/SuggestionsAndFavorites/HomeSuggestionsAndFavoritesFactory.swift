import UIKit

protocol HomeSuggestionsAndFavoritesFactory {
    typealias Component = HomeComponent & HomeComponentRefreshable & HomeSuggestionsAndFavoritesController
    typealias PaymentData = (section: LegacySearchResultSection, row: LegacySearchResultRow)

    func makeHomeSuggestionsAndFavoritesComponent(
        openPaymentAction: ((PaymentData) -> Void)?,
        openItemAction: ((HomeFavorites.Item) -> Void)?
    ) -> Component
}
