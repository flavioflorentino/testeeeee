import Core

final class HomeSuggestionsPresenter {
    // MARK: - Type alias
    typealias PaymentData = (section: LegacySearchResultSection, row: LegacySearchResultRow)

    // MARK: - Properties
    weak var display: HomeSuggestionsDisplay?
    var openPaymentAction: ((PaymentData) -> Void)?
}

// MARK: - HomeSuggestionsPresenting
extension HomeSuggestionsPresenter: HomeSuggestionsPresenting {
    func present(error: ApiError) {
        display?.hideLoading()
    }

    func present(suggestions: LegacySearchResultSection) {
        display?.show(suggestions: suggestions)
    }

    func openPayment(section: LegacySearchResultSection, row: LegacySearchResultRow) {
        openPaymentAction?((section: section, row: row))
    }

    func showLoading() {
        display?.showLoading()
    }

    func hideLoading() {
        display?.hideLoading()
    }
}
