import Core

protocol HomeSuggestionsPresenting {
    func present(suggestions: LegacySearchResultSection)
    func present(error: ApiError)

    func showLoading()
    func hideLoading()

    func openPayment(section: LegacySearchResultSection, row: LegacySearchResultRow)
}
