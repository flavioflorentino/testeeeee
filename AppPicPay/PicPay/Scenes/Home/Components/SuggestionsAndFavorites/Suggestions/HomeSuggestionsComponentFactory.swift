import UIKit

protocol HomeSuggestionsComponentFactory {
    typealias Component = HomeComponent & HomeComponentRefreshable & HomeSuggestionsController
    typealias PaymentData = (section: LegacySearchResultSection, row: LegacySearchResultRow)

    func makeHomeSuggestionsComponent(openPaymentAction: ((PaymentData) -> Void)?) -> Component
}
