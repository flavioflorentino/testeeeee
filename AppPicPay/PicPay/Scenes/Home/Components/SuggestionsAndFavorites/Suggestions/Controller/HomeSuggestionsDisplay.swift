protocol HomeSuggestionsDisplay: AnyObject {
    func show(suggestions: LegacySearchResultSection)

    func showLoading()
    func hideLoading()
}
