import UI
import UIKit

private extension HomeSuggestionsComponent.Layout {
    enum Carousel {
        static let height: CGFloat = 124.0
        static let margin: CGFloat = Spacing.base02
    }
}

final class HomeSuggestionsComponent: ViewController<HomeSuggestionsInteracting, UIView>, HomeSuggestionsController {
    // MARK: - Nested types
    fileprivate enum Layout { }

    // MARK: - Properties
    private var finishLoadAction: (() -> Void)?

    private lazy var containerView: UIView = {
        let view = UIView()
        view.addSubviews(activityIndicatorView, collectionView)

        return view
    }()
    
    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        var indicatorStyle: UIActivityIndicatorView.Style = .gray
        if #available(iOS 13.0, *) {
            indicatorStyle = .medium
        } else if #available(iOS 12.0, *), (traitCollection.userInterfaceStyle == .dark) {
            indicatorStyle = .white
        }

        let activityIndicatorView = UIActivityIndicatorView(style: indicatorStyle)
        activityIndicatorView.hidesWhenStopped = true

        return activityIndicatorView
    }()

    private lazy var collectionView: RecentsCollectionView = {
        let collectionView = RecentsCollectionView()
        collectionView.recentsDelegate = self

        return collectionView
    }()

    // MARK: - View lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        interactor.fetchSuggestions()
    }

    // MARK: - Setup
    override func buildViewHierarchy() {
        super.buildViewHierarchy()

        view.addSubview(containerView)
    }

    override func setupConstraints() {
        super.setupConstraints()

        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        collectionView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.equalTo(Layout.Carousel.height)
        }

        activityIndicatorView.snp.makeConstraints {
            $0.center.equalTo(collectionView.snp.center)
        }
    }

    override func configureViews() {
        super.configureViews()

        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        containerView.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
}

// MARK: - HomeComponentRefreshable
extension HomeSuggestionsComponent: HomeComponentRefreshable {
    func refresh(_ finish: @escaping () -> Void) {
        finishLoadAction = finish
        interactor.fetchSuggestions()
    }
}

// MARK: - RecentesCollectionViewDelegate
extension HomeSuggestionsComponent: RecentsCollectionViewDelegate {
    func didSelectCell(at indexPath: IndexPath) { }

    func didSelectRow(section: LegacySearchResultSection, row: LegacySearchResultRow) {
        interactor.didSelect(section: section, row: row)
    }
}

// MARK: - HomeSuggestionsDisplay
extension HomeSuggestionsComponent: HomeSuggestionsDisplay {
    func show(suggestions: LegacySearchResultSection) {
        collectionView.setup(results: suggestions)
    }

    func showLoading() {
        activityIndicatorView.startAnimating()
        collectionView.isHidden = true
    }

    func hideLoading() {
        activityIndicatorView.stopAnimating()
        collectionView.isHidden = false

        finishLoadAction?()
        finishLoadAction = nil
    }
}
