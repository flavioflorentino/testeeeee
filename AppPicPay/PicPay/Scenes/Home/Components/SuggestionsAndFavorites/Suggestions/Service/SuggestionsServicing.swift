import Core

protocol SuggestionsServicing {
    func suggestions(completion: @escaping (Result<LegacySearchResultSection, ApiError>) -> Void)
}
