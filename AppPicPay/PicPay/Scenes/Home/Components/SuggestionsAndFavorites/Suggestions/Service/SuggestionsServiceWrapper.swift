import Core
import CoreLocation
import FeatureFlag

struct SuggestionsServiceWrapper {
    // MARK: - Type alias
    typealias Dependencies = HasFeatureManager & HasLocationManager & HasMainQueue

    // MARK: - Properties
    private let cacheKey = "home-suggestions-list"

    private let api: SearchApi
    private let dependencies: Dependencies

    // MARK: - Initialization
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
        self.api = SearchApi(featureManager: dependencies.featureManager)
    }
}

// MARK: - SuggestionsServicing
extension SuggestionsServiceWrapper: SuggestionsServicing {
    func suggestions(completion: @escaping (Result<LegacySearchResultSection, ApiError>) -> Void) {
        _ = api.search(
            request: makeSuggestionsRequest(),
            originFlow: .feed,
            cacheKey: cacheKey,
            onCacheLoaded: { cache in
                dependencies.mainQueue.async {
                    handle(result: .success(cache), completion: completion)
                }
            },
            completion: { result in
                dependencies.mainQueue.async {
                    switch result {
                    case let .success(response):
                        handle(result: .success(response), completion: completion)
                    case let .failure(error):
                        handle(result: .failure(apiError(from: error)), completion: completion)
                    }
                }
            }
        )
    }
}

// MARK: - Private
private extension SuggestionsServiceWrapper {
    func makeSuggestionsRequest() -> SearchRequest {
        let request = SearchRequest()
        request.group = "SUGGESTIONS"
        request.page = 0

        if let coordinate = dependencies.locationManager.lastAvailableLocation?.coordinate {
            request.latitude = coordinate.latitude
            request.longitude = coordinate.longitude
        }

        return request
    }

    func handle(
        result: Result<SearchApi.SearchApiResponse, ApiError>,
        completion: @escaping (Result<LegacySearchResultSection, ApiError>) -> Void
    ) {
        switch result {
        case let .success(response):
            guard let section = response.list.first else {
                return completion(.failure(.unknown(nil)))
            }

            completion(.success(section))
        case let .failure(error):
            completion(.failure(error))
        }
    }

    func apiError(from picpayError: PicPayError) -> ApiError {
        if picpayError == .generalError {
            return .unknown(picpayError)
        }

        if picpayError == .notConnectedToInternet {
            return .connectionFailure
        }

        var error = Core.RequestError()
        error.message = picpayError.message
        error.code = picpayError.picpayCode
        error.title = picpayError.title
        error.messageId = picpayError.messageId
        error.jsonData = try? JSONSerialization.data(withJSONObject: picpayError.data ?? [:], options: .fragmentsAllowed)

        return .otherErrors(body: error)
    }
}
