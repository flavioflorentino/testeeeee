import AnalyticsModule
import Core
import FeatureFlag
import Foundation

final class HomeSuggestionsInteractor {
    typealias Dependencies = HasAnalytics

    // MARK: - Properties
    private let dependencies: Dependencies
    private let presenter: HomeSuggestionsPresenting
    private let service: SuggestionsServicing

    private var currentSelectedRow: LegacySearchResultRow?

    // MARK: - Initialization
    init(presenter: HomeSuggestionsPresenting, service: SuggestionsServicing, dependencies: Dependencies) {
        self.dependencies = dependencies
        self.presenter = presenter
        self.service = service
    }
}

// MARK: - HomeSuggestionsInteracting
extension HomeSuggestionsInteractor: HomeSuggestionsInteracting {
    func fetchSuggestions() {
        presenter.showLoading()
        service.suggestions { [weak self] result in
            self?.presenter.hideLoading()

            switch result {
            case let .success(section):
                section.style = .home
                self?.presenter.present(suggestions: section)
            case let .failure(error):
                self?.presenter.present(error: error)
            }
        }
    }

    func didSelect(section: LegacySearchResultSection, row: LegacySearchResultRow) {
        if case .person = row.type {
            dependencies.analytics.log(ProfileEvent.touchPicture(from: .suggestions))
        }

        presenter.openPayment(section: section, row: row)

        guard currentSelectedRow != row, let position = section.rows.firstIndex(of: row) else { return }
        currentSelectedRow = row

        dependencies.analytics.log(
            PaymentEvent.itemAccessed(
                type: row.type.rawValue,
                id: row.basicData.id ?? "",
                name: row.basicData.title ?? "",
                section: (name: section.title ?? "", position: position)
            )
        )
    }
}
