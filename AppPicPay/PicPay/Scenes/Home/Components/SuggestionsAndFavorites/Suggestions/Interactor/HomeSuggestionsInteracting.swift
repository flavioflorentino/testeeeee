protocol HomeSuggestionsInteracting {
    func fetchSuggestions()
    func didSelect(section: LegacySearchResultSection, row: LegacySearchResultRow)
}
