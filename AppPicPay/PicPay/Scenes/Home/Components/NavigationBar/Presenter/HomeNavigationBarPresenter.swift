import CoreLegacy
import DirectMessageSB
import Foundation

final class HomeNavigationBarPresenter {
    weak var display: HomeNavigationBarDisplay?

    var openConsumerWalletAction: (() -> Void)?
    var openPromotionsAction: (() -> Void)?
    var openInviteAction: (() -> Void)?
    var openScannerAction: (() -> Void)?
    var openSettingsAction: (() -> Void)?
    var openMessagesAction: (() -> Void)?
}

// MARK: - HomeNavigationBarPresenting
extension HomeNavigationBarPresenter: HomeNavigationBarPresenting {
    func changeBalance(value: Decimal) {
        let value = CurrencyFormatter.brazillianRealString(from: NSDecimalNumber(decimal: value))
        display?.changeBalance(value: value)
    }

    func changeBalanceVisibility(isVisible: Bool) {
        display?.changeBalanceVisibility(isVisible: isVisible)
    }
    
    func changeMessagesVisibility(isVisible: Bool) {
        display?.changeMessagesVisibility(isVisible: isVisible)
    }

    func changeUnreadMessagesCount(to count: Int) {
        display?.changeUnreadMessagesCount(to: count)
    }
    
    func showPromotionsIndicator() {
        display?.showPromotionsIndicator()
    }

    func hidePromotionsIndicator() {
        display?.hidePromotionsIndicator()
    }

    func openConsumerWallet() {
        openConsumerWalletAction?()
    }

    func openPromotions() {
        openPromotionsAction?()
    }

    func openInvite() {
        openInviteAction?()
    }

    func openScanner() {
        openScannerAction?()
    }

    func openSettings() {
        openSettingsAction?()
    }

    func openMessages() {
        openMessagesAction?()
    }
}
