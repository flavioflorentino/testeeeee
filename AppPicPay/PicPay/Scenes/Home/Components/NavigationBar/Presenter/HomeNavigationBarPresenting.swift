import Foundation

protocol HomeNavigationBarPresenting {
    func changeBalance(value: Decimal)
    func changeBalanceVisibility(isVisible: Bool)

    func changeMessagesVisibility(isVisible: Bool)
    func changeUnreadMessagesCount(to count: Int)
    func showPromotionsIndicator()
    func hidePromotionsIndicator()

    func openConsumerWallet()
    func openPromotions()
    func openInvite()
    func openScanner()
    func openSettings()
    func openMessages()
}
