import AnalyticsModule
import DirectMessageSB

protocol HomeNavigationBarController: AnyObject {
    typealias Dependencies = HasAnalytics

    init(interactor: HomeNavigationBarInteracting, dependencies: Dependencies)
}
