import AnalyticsModule
import DirectMessageSB
import UI
import UIKit

private extension HomeNavigationBarComponent.Layout {
    enum NavigationBar {
        static let height: CGFloat = 50.0
    }
}

final class HomeNavigationBarComponent: ViewController<HomeNavigationBarInteracting, UIView>,
                                        HomeNavigationBarController {
    // MARK: - Nested types
    fileprivate enum Layout { }

    // MARK: - Properties
    private let dependencies: Dependencies

    private lazy var containerView: UIView = {
        let view = UIView()
        view.addSubview(navigationBar)
        view.backgroundColor = Colors.backgroundPrimary.color

        return view
    }()

    private lazy var navigationBar: HomeNavigationBar = {
        let navigationBar = HomeNavigationBar()
        navigationBar.backgroundColor = Colors.backgroundPrimary.color

        navigationBar.balanceAction = { [weak self] in
            self?.didTapAtBalance()
        }

        navigationBar.scannerButtonAction = { [weak self] in
            self?.didTapAtScannerButton()
        }

        navigationBar.settingsButtonAction = { [weak self] in
            self?.didTapAtSettingsButton()
        }

        navigationBar.inviteButtonAction = { [weak self] in
            self?.didTapAtInviteButton()
        }

        navigationBar.promotionsButtonAction = { [weak self] in
            self?.didTapAtPromotionsButton()
        }
        
        navigationBar.messagesButtonAction = { [weak self] messagesStatus in
            guard let self = self else { return }
            self.didTapAtMessagesButton()
            self.dependencies.analytics.log(.button(.clicked, .openChats(status: messagesStatus)))
        }

        return navigationBar
    }()

    // MARK: - Initialization
    required init(interactor: HomeNavigationBarInteracting, dependencies: Dependencies) {
        self.dependencies = dependencies

        super.init(interactor: interactor)
    }

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        dependencies.analytics.log(PromotionsEvent.promotionsHomeButtonViewed)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        interactor.loadConsumerBalance()
        interactor.checkPromotionsIndicator()
        interactor.checkIfMessagesAreAvailable()
    }

    // MARK: - Setup
    override func buildViewHierarchy() {
        super.buildViewHierarchy()

        view.addSubview(containerView)
    }

    override func setupConstraints() {
        super.setupConstraints()

        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        navigationBar.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.equalTo(Layout.NavigationBar.height)
        }
    }

    override func configureViews() {
        super.configureViews()

        view.backgroundColor = .backgroundPrimary()
    }
}

// MARK: - HomeNavigationBarDisplay
extension HomeNavigationBarComponent: HomeNavigationBarDisplay {
    func changeBalance(value: String?) {
        navigationBar.setBalance(value: value)
    }

    func changeBalanceVisibility(isVisible: Bool) {
        navigationBar.changeBalaceVisibility(isVisible == false)
    }

    func showPromotionsIndicator() {
        navigationBar.changePromotionButtonVisibility(false, showIndicator: true)
    }

    func hidePromotionsIndicator() {
        navigationBar.changePromotionButtonVisibility(false, showIndicator: false)
    }

    func changeMessagesVisibility(isVisible: Bool) {
        navigationBar.changeMessagesButtonVisibility(isVisible)
    }
    
    func changeUnreadMessagesCount(to count: Int) {
        navigationBar.changeUnreadMessagesCount(to: count)
    }
}

// MARK: - HomeComponentRefreshable
extension HomeNavigationBarComponent: HomeComponentRefreshable {
    func refresh(_ finish: @escaping () -> Void) {
        interactor.loadConsumerBalance()
        finish()
    }
}

// MARK: - Private
private extension HomeNavigationBarComponent {
    // MARK: - Actions
    func didTapAtBalance() {
        interactor.tapAtConsumerBalance()
    }

    func didTapAtScannerButton() {
        interactor.tapAtScanner()
    }

    func didTapAtSettingsButton() {
        interactor.tapAtSettings()
    }

    func didTapAtInviteButton() {
        interactor.tapAtInvite()
    }

    func didTapAtPromotionsButton() {
        interactor.tapAtPromotions()
    }
    
    func didTapAtMessagesButton() {
        interactor.tapAtMessages()
    }
}
