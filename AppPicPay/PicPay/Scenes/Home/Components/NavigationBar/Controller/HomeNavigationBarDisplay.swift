protocol HomeNavigationBarDisplay: AnyObject {
    func changeBalance(value: String?)
    func changeBalanceVisibility(isVisible: Bool)
    
    func changeMessagesVisibility(isVisible: Bool)
    func changeUnreadMessagesCount(to count: Int)

    func showPromotionsIndicator()
    func hidePromotionsIndicator()
}
