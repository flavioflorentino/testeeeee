protocol HomeNavigationBarInteracting {
    func loadConsumerBalance()

    func checkPromotionsIndicator()
    func checkIfMessagesAreAvailable()

    func tapAtScanner()
    func tapAtSettings()
    func tapAtConsumerBalance()
    func tapAtInvite()
    func tapAtPromotions()
    func tapAtMessages()
}
