import Core
import DirectMessageSB
import FeatureFlag
import Foundation

final class HomeNavigationBarInteractor {
    typealias Dependencies = HasConsumerManager
        & HasFeatureManager
        & HasKVStore
        & HasChatNotifier
        & HasNotificationCenter

    // MARK: - Properties
    private let dependencies: Dependencies
    private let presenter: HomeNavigationBarPresenting

    private var consumerChangedObserver: NSObjectProtocol?
    private var chatUnreadMessagesCountObserver: NSObjectProtocol?

    private var isDirectMessageSBEnabled: Bool {
        dependencies.featureManager.isActive(.directMessageSBEnabled)
    }

    // MARK: - Initialization
    init(presenter: HomeNavigationBarPresenting, dependencies: Dependencies) {
        self.dependencies = dependencies
        self.presenter = presenter

        consumerChangedObserver = dependencies.notificationCenter.addObserver(
            forName: Notification.Name.Consumer.balanceChanged,
            object: nil,
            queue: .main
        ) { [weak self] _ in
            self?.updateConsumerBalance()
        }
        
        self.addChatUnreadCountObserver()
    }
}

// MARK: - HomeNavigationBarInteracting
extension HomeNavigationBarInteractor: HomeNavigationBarInteracting {
    func loadConsumerBalance() {
        dependencies.consumerManager.loadConsumerBalance()

        updateConsumerBalanceVisibility()
        updateConsumerBalance()
    }

    func checkPromotionsIndicator() {
        guard dependencies.kvStore.getFirstTimeOnlyEvent(PromotionsUserDefaultKey.alreadyOpened.rawValue) == false else {
            return presenter.hidePromotionsIndicator()
        }

        presenter.showPromotionsIndicator()
    }

    func checkIfMessagesAreAvailable() {
        let isAvaliable = dependencies.featureManager.isActive(.directMessageSBEnabled)
        presenter.changeMessagesVisibility(isVisible: isAvaliable)
    }
    
    func tapAtScanner() {
        presenter.openScanner()
    }

    func tapAtSettings() {
        presenter.openSettings()
    }

    func tapAtConsumerBalance() {
        presenter.openConsumerWallet()
    }

    func tapAtInvite() {
        presenter.openInvite()
    }

    func tapAtPromotions() {
        presenter.openPromotions()

        dependencies.kvStore.setFirstTimeOnlyEvent(PromotionsUserDefaultKey.alreadyOpened.rawValue)
        presenter.hidePromotionsIndicator()
    }
    
    func tapAtMessages() {
        guard isDirectMessageSBEnabled else { return }
        presenter.openMessages()
    }
}

// MARK: - Private
private extension HomeNavigationBarInteractor {
    func updateConsumerBalanceVisibility() {
        presenter.changeBalanceVisibility(isVisible: isConsumerBalanceVisible())
    }

    func updateConsumerBalance() {
        guard let balance = dependencies.consumerManager.consumer?.balance else {
            return
        }
        presenter.changeBalance(value: balance.decimalValue)
    }

    func isConsumerBalanceVisible() -> Bool {
        !dependencies.consumerManager.isBalanceHidden
    }
}

// MARK: - Private DM Implementation
private extension HomeNavigationBarInteractor {
    func addChatUnreadCountObserver() {
        guard dependencies.featureManager.isActive(.directMessageSBEnabled) else { return }
        let notificationName = ChatNotifier.NotificationName.didUpdateUnreadMessagesCount.notificationName
        
        chatUnreadMessagesCountObserver = dependencies.notificationCenter
            .addObserver(forName: notificationName,
                         object: nil,
                         queue: .main) { [weak self] notification in
                self?.updateChatUnreadCount(with: notification)
            }
    }
    
    func updateChatUnreadCount(with notification: Notification) {
        guard let unreadCount = notification.userInfo?[ChatNotifier.NotificationUserInfoKey.totalCount] as? Int
        else { return }
        
        presenter.changeUnreadMessagesCount(to: unreadCount)
    }
}
