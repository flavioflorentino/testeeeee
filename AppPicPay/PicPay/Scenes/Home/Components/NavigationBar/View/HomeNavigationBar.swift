import AssetsKit
import DirectMessageSB
import FeatureFlag
import SnapKit
import UI
import UIKit

private extension HomeNavigationBar.Accessibility {
    enum ScannerButton {
        static let identifier = "scannerButton"
    }

    enum SettingsButton {
        static let identifier = "settingsButton"
        static let label = DefaultLocalizable.settings.text
    }

    enum InviteButton {
        static let identifier = "inviteButton"
    }

    enum PromotionsButton {
        static let identifier = "promotionsButton"
    }
    
    enum MessagesButton {
        static let identifier = "messagesButton"
    }
}

private extension HomeNavigationBar.Layout {
    enum Button {
        static let size = CGSize(width: Sizing.base06, height: Sizing.base06)
        static let imageSize = CGSize(width: 24.0, height: 24.0)
    }

    enum NewIndicator {
        static let size = CGSize(width: Spacing.base01, height: Spacing.base01)
        static let cornerRadius: CGFloat = size.height / 2.0
        static let marginTop: CGFloat = 2.0
        static let marginTrailing: CGFloat = 3.0
    }
    
    enum VisibilityOffView {
        static let width: CGFloat = 112
    }
}

final class HomeNavigationBar: UIView {
    // MARK: - Nested types
    fileprivate enum Accessibility { }
    fileprivate enum Layout { }

    typealias Dependencies = HasFeatureManager & HasConsumerManager

    // MARK: - Properties
    private let dependencies: Dependencies = DependencyContainer()
    
    // MARK: - Feature Flag variables
    private var isNewNavigationBarActive: Bool {
        dependencies.featureManager.isActive(.experimentNewAccessSettings)
    }
    
    private var isCenteredHeaderExperimentActive: Bool {
        dependencies.featureManager.isActive(.experimentNewFeedCenteredHeader)
    }
    
    private var visibilityOffIcon: UIImage {
        isNewNavigationBarActive ? Resources.Icons.icoPasswordInvisible.image : Assets.PaymentMethods.visibilityOff.image
    }

    private lazy var balanceView: UIView = {
        let view = UIView()
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapAtBalance)))
        view.addSubviews(titleLabel, balanceLabel, visibilityOffView, visibilityOffImage)
        view.isUserInteractionEnabled = true

        return view
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        guard isNewNavigationBarActive else {
            label.labelStyle(CaptionLabelStyle())
                .with(\.text, HomeLegacyLocalizable.myBalance.text)
                .with(\.textAlignment, .center)
                .with(\.textColor, Colors.black.color)
            return label
        }
        
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.text, String(format: HomeLegacyLocalizable.newNavigationTitlePrefix.text, (dependencies.consumerManager.consumer?.username ?? "")))
            .with(\.textColor, Colors.grayscale750.color)
            .with(\.textAlignment, (!isCenteredHeaderExperimentActive && isNewNavigationBarActive) ? .left : .center)

        return label
    }()

    private lazy var balanceLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, isNewNavigationBarActive ? Colors.branding600.color : Colors.black.color)
            .with(\.textAlignment, (!isCenteredHeaderExperimentActive && isNewNavigationBarActive) ? .left : .center)
        return label
    }()

    private lazy var visibilityOffImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = isNewNavigationBarActive ? .scaleToFill : .center
        imageView.image = visibilityOffIcon
        imageView.tintColor = isNewNavigationBarActive ? Colors.grayscale700.color : Colors.grayscale400.color
        imageView.isHidden = true

        return imageView
    }()
    
    private lazy var visibilityOffView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        
        return view
    }()

    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        var style: UIActivityIndicatorView.Style = .gray

        if #available(iOS 13.0, *) {
            style = .medium
        }

        let activityIndicatorView = UIActivityIndicatorView(style: style)
        activityIndicatorView.hidesWhenStopped = true

        return activityIndicatorView
    }()

    private lazy var leftStackContainerView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = .zero
        guard isNewNavigationBarActive else {
            stackView.addArrangedSubviews(scannerButton, settingsButton)
            return stackView
        }
        
        stackView.addArrangedSubviews(scannerButton, profileImageContainerView)
        
        return stackView
    }()

    private lazy var scannerButton: UIButton = {
        let button = makeButton(
            image: Assets.NewIconography.Navigationbar.newIconQrcode.image,
            selector: #selector(didTapAtScannerButton),
            accessibilityIdentifier: Accessibility.ScannerButton.identifier
        )

        return button
    }()

    private lazy var settingsButton: UIButton = {
        let button = makeButton(
            image: Assets.NewIconography.Navigationbar.newIconSettings.image,
            selector: #selector(didTapAtSettingsButton),
            accessibilityIdentifier: Accessibility.SettingsButton.identifier
        )
        button.accessibilityLabel = Accessibility.SettingsButton.label

        return button
    }()

    private lazy var rightStackContainerView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = .zero
        stackView.addArrangedSubviews(inviteButton, promotionsButton, messagesButton)

        return stackView
    }()

    private lazy var inviteButton: UIButton = {
        let button = makeButton(
            image: Assets.NewIconography.Navigationbar.newIconRecommendation.image,
            selector: #selector(didTapAtInviteButton),
            accessibilityIdentifier: Accessibility.InviteButton.identifier
        )
        
        return button
    }()

    private lazy var promotionsButton: UIButton = {
        let button = makeButton(
            image: Assets.NewIconography.Navigationbar.newIconPromotions.image,
            selector: #selector(didTapAtPromotionsButton),
            accessibilityIdentifier: Accessibility.PromotionsButton.identifier
        )
        button.addSubviews(newIndicatorForPromotion)
        button.isHidden = dependencies.featureManager.isActive(.experimentNewAccessPromotionsMgmBool)
            || !dependencies.featureManager.isActive(.directMessageSBEnabled)

        return button
    }()

    private lazy var newIndicatorForPromotion: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle(cornerRadius: .light))
            .with(\.backgroundColor, Colors.critical600.color)
        view.isHidden = true

        return view
    }()
    
    private lazy var messagesButton: MessageButton = {
        let button = MessageButton()
        
        button.accessibilityIdentifier = Accessibility.MessagesButton.identifier
        button.addTarget(self, action: #selector(didTapAtMessagesButton), for: .touchUpInside)
        return button
    }()
    
    // MARK: - New Navigation Bar Elements
    private lazy var profileImageContainerView: UIView = {
        let view = UIView()
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        view.addSubview(profileImageView)
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapAtSettingsButton)))

        return view
    }()

    private lazy var profileImageView: UIPPProfileImage = {
        let imageView = UIPPProfileImage()
        guard let consumer = dependencies.consumerManager.consumer else {
            imageView.setContact(PPContact())
            return imageView
        }

        imageView.setContact(PPContact(consumer))
        imageView.isUserInteractionEnabled = false
        imageView.isHomeNavigationBar = true

        return imageView
    }()

    var balanceAction: (() -> Void)?
    var scannerButtonAction: (() -> Void)?
    var settingsButtonAction: (() -> Void)?
    var inviteButtonAction: (() -> Void)?
    var promotionsButtonAction: (() -> Void)?
    var messagesButtonAction: ((TrackedMessagesStatus) -> Void)?

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods
    func setBalance(value: String?) {
        balanceLabel.text = value
    }

    func changeBalaceVisibility(_ isHidden: Bool) {
        balanceLabel.isHidden = isHidden
        visibilityOffImage.isHidden = !isHidden
        visibilityOffView.isHidden = !isHidden
    }

    func changePromotionButtonVisibility(_ isHidden: Bool, showIndicator: Bool) {
        guard !dependencies.featureManager.isActive(.experimentNewAccessPromotionsMgmBool) else { return } 

        promotionsButton.isHidden = isHidden

        guard isHidden == false else { return }
        newIndicatorForPromotion.isHidden = !showIndicator
    }
    
    func changeMessagesButtonVisibility(_ isVisible: Bool) {
        messagesButton.isHidden = !isVisible
        changePromotionButtonVisibility(isVisible, showIndicator: false)
    }
    
    func changeUnreadMessagesCount(to count: Int) {
        messagesButton.changeUnreadMessagesCount(to: count)
    }
}

// MARK: - ViewConfiguration
extension HomeNavigationBar: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(
            leftStackContainerView,
            rightStackContainerView,
            activityIndicatorView,
            balanceView
        )
    }

    func setupConstraints() {
        leftStackContainerView.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base01)
            $0.centerY.equalToSuperview()
        }

        scannerButton.snp.makeConstraints {
            $0.size.equalTo(Layout.Button.size)
        }

        if isNewNavigationBarActive {
            profileImageContainerView.snp.makeConstraints {
                $0.size.equalTo(Layout.Button.size)
            }

            profileImageView.snp.makeConstraints {
                $0.size.equalTo(Layout.Button.imageSize)
                $0.center.equalToSuperview()
            }
        } else {
            settingsButton.snp.makeConstraints {
                $0.size.equalTo(Layout.Button.size)
            }
        }

        activityIndicatorView.snp.makeConstraints {
            $0.center.equalToSuperview()
        }

        balanceView.snp.makeConstraints {
            guard isNewNavigationBarActive else {
                $0.top.bottom.equalToSuperview().inset(Spacing.base00)
                $0.leading.greaterThanOrEqualTo(leftStackContainerView.snp.trailing)
                $0.trailing.lessThanOrEqualTo(rightStackContainerView.snp.leading)
                $0.centerX.equalToSuperview()
                return
            }
            
            guard isCenteredHeaderExperimentActive else {
                $0.top.bottom.equalToSuperview().inset(Spacing.base00)
                $0.leading.equalTo(leftStackContainerView.snp.trailing).offset(Spacing.base01)
                $0.centerY.equalTo(leftStackContainerView.snp.centerY)
                return
            }
            
            $0.top.bottom.equalToSuperview()
            $0.center.equalToSuperview()
            $0.width.equalTo(titleLabel.snp.width)
        }

        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview()
            guard isNewNavigationBarActive else {
                $0.leading.trailing.equalToSuperview()
                return
            }
            
            guard isCenteredHeaderExperimentActive else {
                $0.trailing.lessThanOrEqualToSuperview()
                $0.leading.equalToSuperview()
                return
            }
            $0.leading.trailing.equalToSuperview()
        }

        balanceLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom)
            $0.bottom.leading.trailing.equalToSuperview()
        }
        
        if isNewNavigationBarActive {
            visibilityOffView.snp.makeConstraints {
                $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
                $0.height.equalTo(Spacing.base01)
                $0.leading.equalToSuperview()
            }
            
            visibilityOffImage.snp.makeConstraints {
                $0.centerY.equalTo(visibilityOffView)
                $0.width.height.equalTo(Spacing.base02)
                $0.leading.equalTo(visibilityOffView.snp.trailing).offset(Spacing.base01)
                $0.trailing.equalToSuperview()
            }
        } else {
            visibilityOffImage.snp.makeConstraints {
                $0.top.equalTo(titleLabel.snp.bottom)
                $0.leading.trailing.bottom.equalToSuperview()
            }
        }

        rightStackContainerView.snp.makeConstraints {
            $0.trailing.equalToSuperview().offset(-Spacing.base01)
            $0.centerY.equalToSuperview()
        }

        inviteButton.snp.makeConstraints {
            $0.size.equalTo(Layout.Button.size)
        }

        promotionsButton.snp.makeConstraints {
            $0.size.equalTo(Layout.Button.size)
        }
        
        messagesButton.snp.makeConstraints {
            $0.size.equalTo(Layout.Button.size)
        }

        guard let imageView = promotionsButton.imageView else { return }
        newIndicatorForPromotion.snp.makeConstraints {
            $0.size.equalTo(Layout.NewIndicator.size)
            $0.top.equalTo(imageView.snp.top).offset(-Layout.NewIndicator.marginTop)
            $0.trailing.equalTo(imageView.snp.trailing).offset(Layout.NewIndicator.marginTrailing)
        }
    }
    
    func configureViews() {
        visibilityOffView.layer.cornerRadius = Spacing.base01 / 2
    }
}

private extension HomeNavigationBar {
    // MARK: - Actions
    @objc
    func didTapAtBalance() {
        balanceAction?()
    }

    @objc
    func didTapAtScannerButton() {
        scannerButtonAction?()
    }

    @objc
    func didTapAtSettingsButton() {
        settingsButtonAction?()
    }

    @objc
    func didTapAtInviteButton() {
        inviteButtonAction?()
    }

    @objc
    func didTapAtPromotionsButton() {
        promotionsButtonAction?()
    }
    
    @objc
    func didTapAtMessagesButton() {
        messagesButtonAction?(messagesButton.messagesStatus)
    }

    // MARK: - Make methods
    func makeButton(image: UIImage, selector: Selector, accessibilityIdentifier: String? = nil) -> UIButton {
        let button = UIButton(type: .custom)
        button.addTarget(self, action: selector, for: .touchUpInside)
        button.adjustsImageWhenHighlighted = false
        button.setImage(image, for: .normal)

        button.tintColor = Colors.branding600.color
        button.accessibilityIdentifier = accessibilityIdentifier

        return button
    }
}
