import UIKit

protocol HomeNavigationBarComponentFactory {
    typealias Component = HomeComponent & HomeComponentRefreshable & HomeNavigationBarController

    func makeHomeNavigationBarComponent(
        openConsumerWalletAction: (() -> Void)?,
        openPromotionsAction: (() -> Void)?,
        openInviteAction: (() -> Void)?,
        openScannerAction: (() -> Void)?,
        openSettingsAction: (() -> Void)?,
        openMessagesAction: (() -> Void)?
    ) -> Component
}
