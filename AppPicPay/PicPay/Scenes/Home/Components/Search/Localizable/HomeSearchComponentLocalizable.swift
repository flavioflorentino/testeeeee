enum HomeSearchComponentLocalizable: String {
    case searchPlaceholder
}

// MARK: - Localizable
extension HomeSearchComponentLocalizable: Localizable {
    var file: LocalizableFile { .homeSearchComponent }
    var key: String { rawValue }
}
