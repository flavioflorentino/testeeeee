import FeatureFlag

struct HomeSearchApplicationEventProperty {
    typealias Dependency = HasFeatureManager

    // MARK: - Properties
    private let dependency: Dependency

    init(dependency: Dependency) {
        self.dependency = dependency
    }
}

// MARK: - ApplicationEventProperty
extension HomeSearchApplicationEventProperty: ApplicationEventProperty {
    var properties: [String: Any] {
        ["is_new_search_home": dependency.featureManager.isActive(.experimentSearchHomeBool)]
    }
}
