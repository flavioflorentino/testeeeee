import FeatureFlag

struct HomeNavigationApplicationEventProperty {
    typealias Dependency = HasFeatureManager
    
    // MARK: - Properties
    private let dependency: Dependency
    
    init(dependency: Dependency) {
        self.dependency = dependency
    }
}

// MARK: - ApplicationEventProperty
extension HomeNavigationApplicationEventProperty: ApplicationEventProperty {
    var properties: [String: Any] {
        ["is_centered_header": dependency.featureManager.isActive(.experimentNewFeedCenteredHeader)]
    }
}
