final class HomeSearchInteractor {
    // MARK: - Properties
    private let presenter: HomeSearchPresenting

    // MARK: - Initialization
    init(presenter: HomeSearchPresenting) {
        self.presenter = presenter
    }
}

// MARK: - HomeSearchInteracting
extension HomeSearchInteractor: HomeSearchInteracting {
    func tapAtSearch() {
        presenter.openSearch()
    }
}
