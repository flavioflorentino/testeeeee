import UIKit

protocol HomeSearchComponentFactory {
    typealias Component = HomeComponent & HomeSearchController

    func makeHomeSearchComponent(openSearchAction: (() -> Void)?) -> Component
}
