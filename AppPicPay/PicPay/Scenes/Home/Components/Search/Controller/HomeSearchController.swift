protocol HomeSearchController: AnyObject {
    typealias Dependencies = HasFeatureManager
    
    init(interactor: HomeSearchInteracting, dependencies: Dependencies)
}
