import Search
import SnapKit
import UI
import UIKit

// MARK: - Layout
private extension HomeSearchComponent.Layout {
    enum SearchBar {
        static let height: CGFloat = 40.0
    }
}

final class HomeSearchComponent: HomeComponent, HomeSearchController {
    // MARK: - Nested types
    fileprivate enum Layout { }

    // MARK: - Properties
    private let interactor: HomeSearchInteracting
    private let dependencies: Dependencies

    private lazy var containerView: UIView = {
        let view = UIView()
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        view.addSubview(searchBar)

        return view
    }()

    private lazy var searchBar: SearchComponentTextField = {
        let searchBar = SearchComponentTextFieldFactory.make()
        searchBar.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        searchBar.attributedPlaceholder = NSAttributedString(
            string: dependencies.featureManager.text(.searchBarLabel),
            attributes: [.foregroundColor: Colors.grayscale700.color]
        )

        searchBar.layer.borderWidth = 1.0
        searchBar.layer.borderColor = Colors.grayscale200.color.cgColor
        searchBar.tintColor = Colors.grayscale700.color
        searchBar.delegate = self

        return searchBar
    }()

    // MARK: - Initialization
    required init(interactor: HomeSearchInteracting, dependencies: Dependencies) {
        self.interactor = interactor
        self.dependencies = dependencies

        super.init(nibName: nil, bundle: nil)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration
extension HomeSearchComponent: ViewConfiguration {
    func buildViewHierarchy() {
        view.addSubview(containerView)
    }

    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        searchBar.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.SearchBar.height)
        }
    }

    func configureViews() {
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
}

// MARK: - UITextFieldDelegate
extension HomeSearchComponent: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        interactor.tapAtSearch()

        return false
    }
}
