final class HomeSearchPresenter {
    var openSearchAction: (() -> Void)?
}

// MARK: - HomeSearchPresenting
extension HomeSearchPresenter: HomeSearchPresenting {
    func openSearch() {
        openSearchAction?()
    }
}
