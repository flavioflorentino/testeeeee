import UIKit

protocol HomeComponentRefreshable {
    func refresh(_ finish: @escaping () -> Void)
}

protocol HomeComponentScrollable {
    var scrollView: UIScrollView { get }
}

protocol HomeComponentPaginableScrollable {
    var scrollViews: [UIScrollView] { get }
    var pinnedView: UIView? { get }

    func currentScrollView() -> UIScrollView?
}

extension HomeComponentPaginableScrollable {
    var pinnedView: UIView? { nil }
}

typealias HomeComponent = UIViewController
