import AnalyticsModule
import AssetsKit
import Dispatch
import SnapKit
import UI
import UIKit
import InAppReview

final class HomeViewController: ViewController<HomeInteracting, UIView>, HomeController, ApolloLoadable {
    // MARK: - Properties
    private let components: [HomeComponent]
    private let dependencies: Dependencies
    var onViewWillDisappear: (() -> Void)?

    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = Colors.branding600.color
        refreshControl.addTarget(self, action: #selector(refreshComponents), for: .valueChanged)

        return refreshControl
    }()

    private lazy var scrollView = NestedScrollView()

    // MARK: - Initialization
    required init(interactor: HomeInteracting, components: [HomeComponent], dependencies: Dependencies) {
        self.components = components
        self.dependencies = dependencies

        super.init(interactor: interactor)
    }

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.didFinishLoad()
        interactor.registerCustomerSupport()

        scrollView.refreshControl = refreshControl
        addComponents()

        sendDarkModeEvent()
        configureNotification()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(true, animated: animated)
        dependencies.analytics.log(HomeEvent.screenViewed)
        PaginationCoordinator.shared.configurePagination()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        onViewWillDisappear?()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        PaginationCoordinator.shared.configurePagination()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        setNeedsStatusBarAppearanceUpdate()

        interactor.didAppear()

        // swiftlint:disable:next overpowered_viewcontroller
        PPAnalytics.trackFirstTimeOnlyEvent("FT Inicio", properties: ["breadcrumb": BreadcrumbManager.shared.breadcrumb])
    }

    // MARK: - Setup
    override func buildViewHierarchy() {
        super.buildViewHierarchy()

        view.addSubview(scrollView)
    }

    override func setupConstraints() {
        super.setupConstraints()

        scrollView.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom)
            $0.leading.trailing.equalToSuperview()
        }
    }

    override func configureViews() {
        super.configureViews()

        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
}

// MARK: - Private
private extension HomeViewController {
    @objc
    func refreshComponents() {
        let group = DispatchGroup()

        for component in components.compactMap({ $0 as? HomeComponentRefreshable }) {
            group.enter()
            component.refresh { group.leave() }
        }

        group.notify(queue: .main) { [weak self] in
            self?.refreshControl.endRefreshing()
        }
    }

    func addComponents() {
        for component in components {
            component.willMove(toParent: self)

            switch component {
            case let paginableComponent as HomeComponentPaginableScrollable:
                scrollView.add(
                    element: .paginableScrollable(
                        container: component.view,
                        pinned: paginableComponent.pinnedView,
                        scrollViews: paginableComponent.scrollViews,
                        currentScrollView: { paginableComponent.currentScrollView() }
                    )
                )
            case let scrollableComponent as HomeComponentScrollable:
                scrollView.add(element: .scrollable(container: component.view, scrollView: scrollableComponent.scrollView))
            default:
                scrollView.add(element: .fixed(view: component.view))
            }

            addChild(component)
            component.didMove(toParent: self)
        }
    }

    func sendDarkModeEvent() {
        if #available(iOS 13.0, *) {
            dependencies.analytics.log(UserEvent.darkModeOn(traitCollection.userInterfaceStyle == .dark))
        }
    }
    
    func configureNotification() {
        let observer = NotificationCenter.default
        
        observer.addObserver(
            self,
            selector: #selector(handleNotificationAction),
            name: .homeScrollToTop,
            object: nil
        )
    }
    
    @objc
    func handleNotificationAction() {
        scrollView.setContentOffset(.zero, animated: true)
    }
}
