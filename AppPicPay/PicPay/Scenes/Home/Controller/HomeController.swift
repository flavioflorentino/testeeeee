import AnalyticsModule

protocol HomeController: AnyObject {
    typealias Dependencies = HasAnalytics

    init(interactor: HomeInteracting, components: [HomeComponent], dependencies: Dependencies)
}
