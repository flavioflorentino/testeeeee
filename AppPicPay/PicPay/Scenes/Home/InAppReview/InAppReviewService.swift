import Core
import protocol InAppReview.InAppReviewServicing

final class InAppReviewService: InAppReviewServicing {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func getTags() -> [String] {
        [
            "Taxas e Juros",
            "Promoções de cashback",
            "Navegação no app",
            "Rede de aceitação",
            "Problemas com pagamento",
            "Suporte"
        ]
    }
    
    func sendSelected(tags: [String], rating: Int) {
    }
}
