import Core
import CoreLegacy
import CustomerSupport
import FeatureFlag
import PIX
import ReceiptKit
import Validations

final class HomeInteractor {
    // MARK: - Properties
    private let presenter: HomePresenting
    private let mgmIncentiveVerifier: MGMIncentiveVerifiable
    private let dependencies: Dependencies
    private lazy var updatePrivacyService = UpdatePrivacyService(dependencies: dependencies)

    // MARK: - Initialization
    init(presenter: HomePresenting, mgmIncentiveVerifier: MGMIncentiveVerifiable, dependencies: Dependencies) {
        self.presenter = presenter
        self.mgmIncentiveVerifier = mgmIncentiveVerifier
        self.dependencies = dependencies
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(shouldOpenPrivacyReview),
                                               name: NSNotification.PrivacyReview.shouldOpen.name,
                                               object: nil)
    }
}

// MARK: - HomeInteracting
extension HomeInteractor: HomeInteracting {
    func didFinishLoad() {
        dependencies.appParameters.setNotFirstTimeOnApp()
        checkCopiedRandomKeyPopUp()
        checkCopyPastePopUp()
    }

    func didAppear() {
        checkPixKeyRegistration()
        checkCashbackReceipt()
        checkMgmIncentive()
        checkSocial()
        checkNotifications()
        setPaymentInPrivate()
        checkReviewPrivacy()
    }

    func registerCustomerSupport() {
        guard let token = dependencies.userManager.token else { return }
        dependencies.customerSupport.registerUserLoggedCredentials(accessToken: token)
        dependencies.customerSupport.register(notificationToken: token)
    }
}

// MARK: - Private
private extension HomeInteractor {
    var shouldDisplayPixKeyRegistration: Bool {
        dependencies.featureManager.isActive(.releasePixKeyRegistrationHomePopupBool)
            && !dependencies.kvStore.boolFor(KVKey.isPixModalHomeVisualized)
    }

    var shouldShowSocialOnboarding: Bool {
        !dependencies.featureManager.isActive(.newOnboarding) && dependencies.appParameters.getOnboardSocial()
    }

    func loadStudentStatus() {
        dependencies.studentWorker.loadStatus { [weak self] result in
            switch result {
            case let .success(payload) where payload.status == .active:
                self?.didLoadStudentStatus()
            default:
                self?.dependencies.kvStore.setBool(false, with: KVKey.hasStudentAccountActive)
            }
        }
    }

    func didLoadStudentStatus() {
        dependencies.kvStore.setBool(true, with: KVKey.hasStudentAccountActive)

        guard dependencies.kvStore.boolFor(KVKey.studentAccountActivePopup) else { return }

        presenter.presentStudentActiveAlert()
        dependencies.kvStore.setBool(false, with: KVKey.hasStudentAccountActive)
    }

    func checkPixKeyRegistration() {
        guard shouldDisplayPixKeyRegistration else { return }

        presenter.presentPixKeyRegistration()
    }

    func checkCashbackReceipt() {
        dependencies.mainQueue.asyncAfter(deadline: .now() + 1.0) { [weak self] in
            self?.presenter.presentCashbackReceipt()
        }
    }

    func checkMgmIncentive() {
        mgmIncentiveVerifier.updateHomeAccessCount()
        mgmIncentiveVerifier.checkCanShowIncentive { [weak self] canShow in
            guard canShow else { return }
            self?.presenter.presentPayWithPicPayAlert()
        }
    }

    func checkSocial() {
        guard shouldShowSocialOnboarding else {
            return
        }

        presenter.presentSocialOnboarding()
    }

    func checkNotifications() {
        guard !dependencies.appParameters.getOnboardSocial() else { return }
        PPPermission.requestNotifications()
    }
    
    func setPaymentInPrivate() {
        let helper = PrivacyConfigHelper(updatePrivacyService: updatePrivacyService)
        helper.setPaymentInPrivateConfig()
    }
    
    @objc
    func shouldOpenPrivacyReview() {
        checkReviewPrivacy(cameFromReceipt: true)
    }
    
    func checkReviewPrivacy(cameFromReceipt: Bool = false) {
        let helper = PrivacyConfigHelper(updatePrivacyService: updatePrivacyService)
        helper.checkReviewPrivacy(cameFromReceipt: cameFromReceipt) { privacyResult in
            guard let privacyResult = privacyResult else { return }
            self.trackingAPIResult(with: privacyResult)
            guard privacyResult.shouldReview else { return }
            self.presenter.presentReviewPrivacy()
        }
    }
    
    private func trackingAPIResult(with privacyResult: PrivacyConfig) {
        let consumerId = dependencies.consumerManager.consumer?.wsId.toString()
        
        if privacyResult.apiResultIsSuccess {
            dependencies.analytics.log(
                UpdatePrivacyAPIEvent.calledApiFromHomeSuccess(consumerId,
                                                               isReviewed: privacyResult.isReviewed,
                                                               hasTransactions: privacyResult.hasTransactions)
            )
        } else {
            dependencies.analytics.log(UpdatePrivacyAPIEvent.calledApiFromHomeError(consumerId))
        }
    }
    
    func isRandomKey(clipboard: String) -> Bool {
        do {
            try RegexValidator(descriptor: AccountRegexDescriptor.randomKey).validate(clipboard)
        } catch {
            return false
        }
        return true
    }
    
    func checkCopiedRandomKeyPopUp() {
        guard dependencies.featureManager.isActive(.isPixCopiedRandomKeyHomePopUpAvailable) else { return }
        guard let clipboard = UIPasteboard.general.string, dependencies.kvStore.stringFor(KVKey.lastPix) != clipboard,
              isRandomKey(clipboard: clipboard) else {
            return
        }
        dependencies.kvStore.setString(clipboard, with: KVKey.lastPix)
        presenter.presentPixCopiedRandomKeyPopUp(keyType: .random, key: clipboard)
    }
    
    func checkCopyPastePopUp() {
        guard dependencies.featureManager.isActive(.isPixCopyPasteHomePopUpAvailable) else { return }
        let brCodeDomain = "br.gov.bcb.pix"
        guard let clipboard = UIPasteboard.general.string, clipboard.lowercased().contains(brCodeDomain),
              dependencies.kvStore.stringFor(KVKey.lastPix) != clipboard else {
            return
        }
        dependencies.kvStore.setString(clipboard, with: KVKey.lastPix)
        presenter.presentPixCopyPastePopUp(code: clipboard)
    }
}
