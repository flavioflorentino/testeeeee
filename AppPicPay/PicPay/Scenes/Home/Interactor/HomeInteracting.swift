import AnalyticsModule
import Core
import CoreLegacy
import CustomerSupport
import FeatureFlag

protocol HomeInteracting {
    typealias Dependencies = HasAppParameters
        & HasCustomerSupport
        & HasFeatureManager
        & HasKVStore
        & HasMainQueue
        & HasStudentWorker
        & HasUserManager
        & HasConsumerManager
        & HasAnalytics

    init(presenter: HomePresenting, mgmIncentiveVerifier: MGMIncentiveVerifiable, dependencies: Dependencies)

    func didFinishLoad()
    func didAppear()
    func registerCustomerSupport()
}
