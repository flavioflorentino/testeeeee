import AnalyticsModule

enum ExternalTransferOrigin: String {
    case intro = "Intro"
    case detail = "Detail"
}

enum ExternalTransferResendLinkOrigin: String {
    case cardButton = "card_button"
    case detailOptions = "detail_options"
}

enum ExternalTransferReceiptOrigin: String {
    case detailOptions = "detail_options"
}

enum ExternalTransferStatus: String {
    case failure = "payment_refunded"
    case success = "transfer_succeeded"
    case processing = "waiting_receiver"
    
    static func convert(type: AlertFeedWidgetType?) -> ExternalTransferStatus? {
        guard let type = type else {
            return nil
        }
        switch type {
        case .failure:
            return .failure
        case .success:
            return .success
        case .processing:
            return .processing
        default:
            return nil
        }
    }
}

enum ExternalTransferUserEvent: AnalyticsKeyProtocol {
    case start(origin: String)
    case limitExceeded
    case insufficientFunds
    case introViewed
    case paymentSucceeded
    case sendLink
    case detailViewed
    case privacyOpened
    case paymentMethodOpened
    case sent(hasMessage: Bool, requestedValue: Double, privacy: String)
    case insufficientPayment
    case apiError(message: String)
    case addFunds
    case valueNotAllowed(value: String)
    case senderError(origin: ExternalTransferOrigin)
    case cardClicked(status: ExternalTransferStatus)
    case cardDetailViewed
    case resendLink(origin: ExternalTransferResendLinkOrigin)
    case detailOptions
    case receiptViewed(origin: ExternalTransferReceiptOrigin)
    case insufficientFundsBack
    
    private var name: String {
        switch self {
        case .start:
            return "Started"
        case .limitExceeded:
            return "Limit Exceeded"
        case .insufficientFunds:
            return "Insufficient Funds"
        case .introViewed:
            return "Intro Viewed"
        case .paymentSucceeded:
            return "Payment Succeeded"
        case .sendLink:
            return "Send Link"
        case .detailViewed:
            return "Detail Viewed"
        case .privacyOpened:
            return "Privacy Opened"
        case .paymentMethodOpened:
            return "Payment Method Opened"
        case .sent:
            return "Sent"
        case .insufficientPayment:
            return "Insufficient Payment"
        case .apiError:
            return "API Error"
        case .addFunds:
            return "Add Funds"
        case .valueNotAllowed:
            return "Value Not Allowed"
        case .senderError:
            return "Sender Error"
        case .cardClicked:
            return "Card Clicked"
        case .cardDetailViewed:
            return "Card Detail Viewed"
        case .resendLink:
            return "Resend Link"
        case .detailOptions:
            return "Detail Options"
        case .receiptViewed:
            return "Receipt Viewed"
        case .insufficientFundsBack:
            return "Insufficient Funds Back"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .start(origin):
            return ["origin": origin]
        case let .sent(hasMessage, requestedValue, privacy):
            return [
                "has_message": hasMessage,
                "request_value": requestedValue,
                "privacy_type": privacy
            ]
        case let .apiError(message):
            return ["message": message]
        case let .valueNotAllowed(value):
            return ["value": value]
        case let .senderError(origin):
            return ["origin": origin.rawValue]
        case let .cardClicked(status):
            return ["card_status": status.rawValue]
        case let .receiptViewed(origin):
            return ["origin": origin.rawValue]
        case let .resendLink(origin):
            return ["origin": origin.rawValue]
        default:
            return [:]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case .apiError:
            return AnalyticsEvent(name, properties: properties, providers: [.mixPanel, .appsFlyer, .firebase])
        default:
            return AnalyticsEvent("External Transfer \(name)", properties: properties, providers: [.mixPanel, .appsFlyer, .firebase])
        }
    }
}
