import UI
import UIKit

extension ExternalTransferDialogView.Layout {
    enum Font {
        static let small = UIFont.systemFont(ofSize: 14)
        static let regular = UIFont.systemFont(ofSize: 16)
        static let big = UIFont.boldSystemFont(ofSize: 20)
        static let numberOfLines: Int = 0
        static let lineHeightMultiple: CGFloat = 1.26
    }
    
    enum Size {
        static let blueBackgroundViewheight: CGFloat = 150
        static let moneyImageViewHeight: CGFloat = 150
        static let moneyImageViewWidth: CGFloat = 120
        static let letsGoButtonHeight: CGFloat = 48
        static let stackViewSpacing: CGFloat = 12
    }
    
    enum Margin {
        static let regular: CGFloat = 12
    }
}

protocol ExternalTransferDialogDelegate: AnyObject {
    func didTouchOnCancel()
    func didTouchOnConfirm()
}

final class ExternalTransferDialogView: UIView {
    fileprivate enum Layout {}
    
    // MARK: - Variables
    weak var delegate: ExternalTransferDialogDelegate?
    
    private lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.textColor = Palette.white.color
        button.titleLabel?.font = Layout.Font.small
        button.addTarget(self, action: #selector(didTouchOnCancel), for: .touchUpInside)
        return button
    }()
    
    private lazy var blueBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorNeutral200.color
        return view
    }()
    
    private lazy var moneyImageView = UIImageView(image: Assets.P2P.moneyWings.image)
    
    private lazy var textStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Layout.Size.stackViewSpacing
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.big
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = Layout.Font.numberOfLines
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.regular
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = Layout.Font.numberOfLines
        return label
    }()
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.small
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = Layout.Font.numberOfLines
        label.textAlignment = .center
        return label
    }()
    
    private lazy var infoContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale100.color
        view.layer.cornerRadius = 4
        return view
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = Palette.ppColorBranding300.color
        button.titleLabel?.textColor = Palette.white.color
        button.titleLabel?.font = Layout.Font.regular
        button.layer.cornerRadius = Layout.Size.letsGoButtonHeight / 2
        button.addTarget(self, action: #selector(didTouchOnConfirm), for: .touchUpInside)
        return button
    }()
    
    // MARK: Init
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - External functions
    func configureWith(
        title: String,
        subtitle: String,
        cancelButtonText: String,
        confirmButtonText: String,
        info: String? = nil
    ) {
        confirmButton.setTitle(confirmButtonText, for: .normal)
        cancelButton.setTitle(cancelButtonText, for: .normal)
        
        let style = NSMutableParagraphStyle()
        style.alignment = .center
        style.lineHeightMultiple = Layout.Font.lineHeightMultiple
        
        let titleAttrString = NSAttributedString(
            string: title,
            attributes: [.paragraphStyle: style]
        )
        titleLabel.attributedText = titleAttrString
        
        let subtitleAttrString = NSAttributedString(
            string: subtitle,
            attributes: [.paragraphStyle: style]
        )
        subtitleLabel.attributedText = subtitleAttrString
        
        guard
            let info = info,
            let infoAttrString = info.html2Attributed(font: infoLabel.font, color: infoLabel.textColor)
            else {
                return
        }
        
        let infoMutableAttrString = NSMutableAttributedString(
            attributedString: infoAttrString
        )
        
        infoMutableAttrString.addAttributes(
            [.paragraphStyle: style],
            range: NSRange(location: 0, length: infoMutableAttrString.length)
        )
        
        infoLabel.attributedText = infoMutableAttrString
        textStackView.addArrangedSubview(infoContainerView)
    }
}

private extension ExternalTransferDialogView {
    // MARK: - Private functions
    @objc
    func didTouchOnCancel() {
        delegate?.didTouchOnCancel()
    }
    
    @objc
    func didTouchOnConfirm() {
        delegate?.didTouchOnConfirm()
    }
}

extension ExternalTransferDialogView: ViewConfiguration {
    // MARK: - Life Cycle
    func buildViewHierarchy() {
        addSubview(blueBackgroundView)
        addSubview(cancelButton)
        addSubview(moneyImageView)
        addSubview(textStackView)
        addSubview(confirmButton)
        textStackView.addArrangedSubview(titleLabel)
        textStackView.addArrangedSubview(subtitleLabel)
        infoContainerView.addSubview(infoLabel)
    }
    
    func setupConstraints() {
        blueBackgroundView.layout {
            $0.top == topAnchor
            $0.leading == compatibleSafeAreaLayoutGuide.leadingAnchor
            $0.trailing == compatibleSafeAreaLayoutGuide.trailingAnchor
            $0.height == Layout.Size.blueBackgroundViewheight
        }
        
        cancelButton.layout {
            $0.top == topAnchor + Spacing.base02
            $0.leading == compatibleSafeAreaLayoutGuide.leadingAnchor + Layout.Margin.regular
        }
        
        moneyImageView.layout {
            $0.centerY == blueBackgroundView.bottomAnchor - Spacing.base03
            $0.centerX == centerXAnchor
            $0.height == Layout.Size.moneyImageViewHeight
            $0.width == Layout.Size.moneyImageViewWidth
        }
        
        textStackView.layout {
            $0.top == moneyImageView.bottomAnchor + Spacing.base02
            $0.centerX == centerXAnchor
            $0.leading >= compatibleSafeAreaLayoutGuide.leadingAnchor + Spacing.base03
        }
        
        infoLabel.layout {
            $0.top == infoContainerView.topAnchor + Layout.Margin.regular
            $0.leading == infoContainerView.leadingAnchor + Layout.Margin.regular
            $0.trailing == infoContainerView.trailingAnchor - Layout.Margin.regular
            $0.bottom == infoContainerView.bottomAnchor - Layout.Margin.regular
        }
        
        confirmButton.layout {
            $0.leading == compatibleSafeAreaLayoutGuide.leadingAnchor + Spacing.base02
            $0.trailing == compatibleSafeAreaLayoutGuide.trailingAnchor - Spacing.base02
            $0.bottom == compatibleSafeAreaLayoutGuide.bottomAnchor - Spacing.base03
            $0.height == Layout.Size.letsGoButtonHeight
        }
    }
    
    func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
    }
}
