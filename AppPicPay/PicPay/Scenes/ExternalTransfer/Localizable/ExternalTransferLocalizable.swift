import Foundation

enum ExternalTransferLocalizable: String, Localizable {
    case sendToExternal
    case useBalance
    case letsGo
    case somethingWentWrong
    case transfer
    case transferAvailable
    case shareYourTransfer
    case withdrawInTwoDays
    case sendPayment
    case paymentMethodAlertMessage
    case okUnderstood
    case privacyMessage
    case externalTransferShouldFillValueAlertTitle
    case opsSomethingWrong
    case tryAgainLater
    case limitExceeded
    case insuficientBalanceToEnter
    case insuficientBalanceToSend
    case valueNotAllowed
    case privatePayment
    case receiverData
    case cpf

    var key: String {
        rawValue
    }
    
    var file: LocalizableFile {
        .externalTransfer
    }
}
