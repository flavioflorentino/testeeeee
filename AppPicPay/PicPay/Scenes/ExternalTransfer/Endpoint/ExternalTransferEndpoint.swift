import Core

enum ExternalTransferServiceEndpoint {
    case permission
    case createExternalTransfer(message: String, value: Double, password: String)
}

extension ExternalTransferServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .permission:
            return "checkPermission/"
        case .createExternalTransfer:
            return "saveWithdrawal"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .permission:
            return .get
        case .createExternalTransfer:
            return .post
        }
    }
    
    var body: Data? {
        guard case let ExternalTransferServiceEndpoint.createExternalTransfer(message, value, _) = self else {
            return nil
        }
        return [
            "message": message,
            "value": value
        ].toData()
    }
    
    var customHeaders: [String: String] {
        guard case let ExternalTransferServiceEndpoint.createExternalTransfer(_, _, password) = self else {
            return [:]
        }
        return ["password": password]
    }
}
