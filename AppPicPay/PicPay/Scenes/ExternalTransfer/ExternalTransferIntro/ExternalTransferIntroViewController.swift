import AnalyticsModule
import UI

// MARK: - ExternalTransferIntroViewController
final class ExternalTransferIntroViewController: ViewController<ExternalTransferIntroViewModelInputs, UIView> {
    // MARK: - Variables
    
    private lazy var externalTransferDialogView: ExternalTransferDialogView = {
        let view = ExternalTransferDialogView()
        view.delegate = self
        return view
    }()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        Analytics.shared.log(ExternalTransferUserEvent.introViewed)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(externalTransferDialogView)
    }
    
    override func setupConstraints() {
        externalTransferDialogView.layout {
            $0.top == view.topAnchor
            $0.leading == view.compatibleSafeAreaLayoutGuide.leadingAnchor
            $0.trailing == view.compatibleSafeAreaLayoutGuide.trailingAnchor
            $0.bottom == view.compatibleSafeAreaLayoutGuide.bottomAnchor
        }
    }
    
    override func configureViews() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        view.backgroundColor = Palette.ppColorGrayscale000.color
        externalTransferDialogView.configureWith(
            title: ExternalTransferLocalizable.sendToExternal.text,
            subtitle: ExternalTransferLocalizable.useBalance.text,
            cancelButtonText: DefaultLocalizable.btCancel.text,
            confirmButtonText: ExternalTransferLocalizable.letsGo.text
        )
    }
}

extension ExternalTransferIntroViewController: ExternalTransferDialogDelegate {
    func didTouchOnCancel() {
        viewModel.dismiss()
    }
    func didTouchOnConfirm() {
        beginState()
        viewModel.handleExternalPermission()
    }
}

// MARK: ExternalTransferDisplay
extension ExternalTransferIntroViewController: ExternalTransferIntroDisplay {
    func displayError(with alertData: StatusAlertData) {
        endState()
        let alert = StatusAlertView.show(on: view, data: alertData)
        alert.statusAlertDelegate = self
    }
    
    func displayLimitError(titleAlert: NSAttributedString, descriptionAlert: NSAttributedString, buttonTitle: String) {
        endState()
        let button = Button(title: buttonTitle, type: .cta, action: .handler)
        button.handler = { controller, _ in
            controller.dismiss(animated: true) {
                self.viewModel.handleLimitError()
            }
        }
        let alert = Alert(
            with: titleAlert,
            text: descriptionAlert,
            buttons: [button]
        )
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func displayBalanceError(titleAlert: String, descriptionAlert: String, buttonTitle: String, backButtonTitle: String) {
        endState()
        let addBalanceButton = Button(title: buttonTitle, type: .cta, action: .handler)
        addBalanceButton.handler = { controller, _ in
            controller.dismiss(animated: true) {
                self.viewModel.showAddBalance()
            }
        }
        let backButton = Button(title: backButtonTitle, type: .cleanUnderlined, action: .handler)
        backButton.handler = { controller, _ in
            controller.dismiss(animated: true) {
                self.viewModel.handleBalanceBack()
            }
        }
        let alert = Alert(
            with: titleAlert,
            text: descriptionAlert,
            buttons: [addBalanceButton, backButton]
        )
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func hideLoader() {
        endState()
    }
}

// MARK: - StatusAlertViewDelegate
extension ExternalTransferIntroViewController: StatusAlertViewDelegate {
    func didTouchOnButton() {
        beginState()
        viewModel.handleExternalPermission()
    }
}

// MARK: - StatefulProviding
extension ExternalTransferIntroViewController: StatefulProviding {
}
