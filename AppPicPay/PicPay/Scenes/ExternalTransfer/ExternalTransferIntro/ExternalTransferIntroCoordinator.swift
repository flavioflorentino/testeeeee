import UIKit

enum ExternalTransferIntroAction {
    case close
    case next
    case addBalance
}

protocol ExternalTransferIntroCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ExternalTransferIntroAction)
}

final class ExternalTransferIntroCoordinator: ExternalTransferIntroCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: ExternalTransferIntroAction) {
        switch action {
        case .close:
            viewController?.navigationController?.dismiss(animated: true, completion: nil)
        case .next:
            viewController?.navigationController?.pushViewController(ExternalTransferCheckoutFactory.make(), animated: true)
        case .addBalance:
            let navigationController = UINavigationController(rootViewController: RechargeLoadFactory.make())
            viewController?.navigationController?.present(navigationController, animated: true)
        }
    }
}
