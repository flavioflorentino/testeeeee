import UI

protocol ExternalTransferIntroDisplay: AnyObject {
    func displayError(with alertData: StatusAlertData)
    func displayLimitError(titleAlert: NSAttributedString, descriptionAlert: NSAttributedString, buttonTitle: String)
    func displayBalanceError(titleAlert: String, descriptionAlert: String, buttonTitle: String, backButtonTitle: String)
    func hideLoader()
}
