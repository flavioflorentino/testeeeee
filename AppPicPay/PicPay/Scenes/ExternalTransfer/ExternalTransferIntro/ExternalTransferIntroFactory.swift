import AnalyticsModule
import FeatureFlag
import Foundation

final class ExternalTransferIntroFactory {
    // MARK: - Variables
    private let featureManager: FeatureManagerContract
    
    private var isFeatureActive: Bool {
        featureManager.isActive(.externalTransfer)
    }
    
    // MARK: - Life Cycle
    init(featureManager: FeatureManagerContract = FeatureManager.shared) {
        self.featureManager = featureManager
    }
    
    func make(origin: DGHelpers.Origin) -> UIViewController? {
        Analytics.shared.log(ExternalTransferUserEvent.start(origin: origin.rawValue))
        guard isFeatureActive else {
            return nil
        }
        return makeFlow()
    }
    
    private func makeFlow() -> UIViewController {
        let service: ExternalTransferIntroServicing = ExternalTransferIntroService(dependencies: DependencyContainer())
        let coordinator: ExternalTransferIntroCoordinating = ExternalTransferIntroCoordinator()
        let presenter: ExternalTransferIntroPresenting = ExternalTransferIntroPresenter(coordinator: coordinator)
        let viewModel = ExternalTransferIntroViewModel(service: service, presenter: presenter)
        let viewController = ExternalTransferIntroViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return UINavigationController(rootViewController: viewController)
    }
}
