import Core

protocol ExternalTransferIntroServicing {
    func getExternalTransferPermission(completion: @escaping (Result<(model: NoContent, data: Data?), DialogError>) -> Void)
}

final class ExternalTransferIntroService {
    // MARK: - Variables
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    // MARK: - Life Cycle
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ExternalTransferIntroServicing
extension ExternalTransferIntroService: ExternalTransferIntroServicing {
    func getExternalTransferPermission(completion: @escaping (Result<(model: NoContent, data: Data?), DialogError>) -> Void) {
        let endpoint = ExternalTransferServiceEndpoint.permission
        Api<NoContent>(endpoint: endpoint).execute { [weak self] result in
            guard let self = self else {
                return
            }
            self.dependencies.mainQueue.async {
                let mappedResult = result.mapError(DialogError.transformApiError)
                completion(mappedResult)
            }
        }
    }
}
