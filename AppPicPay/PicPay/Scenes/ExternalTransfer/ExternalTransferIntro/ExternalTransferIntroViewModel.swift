import AnalyticsModule
import Core

protocol ExternalTransferIntroViewModelInputs: AnyObject {
    func dismiss()
    func handleExternalPermission()
    func showAddBalance()
    func handleBalanceBack()
    func handleLimitError()
}

final class ExternalTransferIntroViewModel {
    // MARK: - Error Code
    private enum ErrorCode: String {
        case limitExceeded = "441"
        case insufficientFunds = "442"
    }
    
    // MARK: - Variables
    private let service: ExternalTransferIntroServicing
    private let presenter: ExternalTransferIntroPresenting
    
    // MARK: - Life Cycle
    init(service: ExternalTransferIntroServicing, presenter: ExternalTransferIntroPresenting) {
        self.service = service
        self.presenter = presenter
    }
}

// MARK: - ExternalTransferIntroViewModelInputs
extension ExternalTransferIntroViewModel: ExternalTransferIntroViewModelInputs {
    // MARK: - dismiss
    func dismiss() {
        presenter.didNextStep(action: .close)
    }
    
    // MARK: - handleExternalPermission
    func handleExternalPermission() {
        service.getExternalTransferPermission { [weak self] result in
            guard let self = self else {
                return
            }
            switch result {
            case .success:
                self.letsGoOnSuccess()
            case let .failure(error):
                self.letsGoOnFailure(error: error)
            }
        }
    }
    
    private func letsGoOnSuccess() {
        presenter.hideLoader()
        presenter.didNextStep(action: .next)
    }
    
    private func letsGoOnFailure(error: DialogError) {
        switch error {
        case let .dialog(response):
            showDialogError(errorCode: response.code, model: response.model)
        default:
            showGenericError()
        }
    }
    
    private func showDialogError(errorCode: String, model: DialogErrorModel) {
        guard let code = ErrorCode(rawValue: errorCode) else {
            showGenericError()
            return
        }
        switch code {
        case .limitExceeded:
            Analytics.shared.log(ExternalTransferUserEvent.limitExceeded)
            Analytics.shared.log(ExternalTransferUserEvent.apiError(message: ExternalTransferLocalizable.limitExceeded.text))
            presenter.presentLimitError(titleAlert: model.title, descriptionAlert: model.message, buttonTitle: model.buttonText)
        case .insufficientFunds:
            Analytics.shared.log(ExternalTransferUserEvent.insufficientFunds)
            Analytics.shared.log(ExternalTransferUserEvent.apiError(message: ExternalTransferLocalizable.insuficientBalanceToEnter.text))
            presenter.presentBalanceError(titleAlert: model.title, descriptionAlert: model.message, buttonTitle: model.buttonText)
        }
    }
    
    private func showGenericError() {
        Analytics.shared.log(ExternalTransferUserEvent.senderError(origin: .intro))
        Analytics.shared.log(ExternalTransferUserEvent.apiError(message: ExternalTransferLocalizable.opsSomethingWrong.text))
        presenter.presentError()
    }
    
    // MARK: - showAddBalance
    func showAddBalance() {
        Analytics.shared.log(ExternalTransferUserEvent.addFunds)
        presenter.didNextStep(action: .addBalance)
    }
    
    // MARK: - handleBalanceBack
    func handleBalanceBack() {
        Analytics.shared.log(ExternalTransferUserEvent.insufficientFundsBack)
        presenter.didNextStep(action: .close)
    }
    
    // MARK: - handleLimitError
    func handleLimitError() {
        presenter.didNextStep(action: .close)
    }
}
