import Core
import UI

protocol ExternalTransferIntroPresenting: AnyObject {
    var viewController: ExternalTransferIntroDisplay? { get set }
    func didNextStep(action: ExternalTransferIntroAction)
    func presentLimitError(titleAlert: String, descriptionAlert: String, buttonTitle: String)
    func presentBalanceError(titleAlert: String, descriptionAlert: String, buttonTitle: String)
    func presentError()
    func hideLoader()
}

final class ExternalTransferIntroPresenter: ExternalTransferIntroPresenting {
    private let coordinator: ExternalTransferIntroCoordinating
    weak var viewController: ExternalTransferIntroDisplay?

    init(coordinator: ExternalTransferIntroCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: ExternalTransferIntroAction) {
        coordinator.perform(action: action)
    }
    
    func presentLimitError(titleAlert: String, descriptionAlert: String, buttonTitle: String) {
        let title = NSAttributedString(string: titleAlert)
        let description = NSMutableAttributedString(string: descriptionAlert)
        viewController?.displayLimitError(
            titleAlert: title,
            descriptionAlert: description,
            buttonTitle: buttonTitle
        )
    }
    
    func presentBalanceError(titleAlert: String, descriptionAlert: String, buttonTitle: String) {
        viewController?.displayBalanceError(
            titleAlert: titleAlert,
            descriptionAlert: descriptionAlert,
            buttonTitle: buttonTitle,
            backButtonTitle: DefaultLocalizable.back.text
        )
    }
    
    func presentError() {
        let alertData = StatusAlertData(
            icon: Assets.Emotions.iluSadFace.image,
            title: ExternalTransferLocalizable.opsSomethingWrong.text,
            text: ExternalTransferLocalizable.tryAgainLater.text,
            buttonTitle: PaymentRequestLocalizable.tryAgain.text,
            isCloseButtonHidden: false
        )
        viewController?.displayError(with: alertData)
    }
    
    func hideLoader() {
        viewController?.hideLoader()
    }
}
