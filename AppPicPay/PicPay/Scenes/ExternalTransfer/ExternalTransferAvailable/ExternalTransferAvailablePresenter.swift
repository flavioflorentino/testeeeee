import Core
import Foundation

protocol ExternalTransferAvailablePresenting: AnyObject {
    var viewController: ExternalTransferAvailableDisplay? { get set }
    func didNextStep(action: ExternalTransferAvailableAction)
}

final class ExternalTransferAvailablePresenter: ExternalTransferAvailablePresenting {
    private let coordinator: ExternalTransferAvailableCoordinating
    weak var viewController: ExternalTransferAvailableDisplay?

    init(coordinator: ExternalTransferAvailableCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: ExternalTransferAvailableAction) {
        coordinator.perform(action: action)
    }
}
