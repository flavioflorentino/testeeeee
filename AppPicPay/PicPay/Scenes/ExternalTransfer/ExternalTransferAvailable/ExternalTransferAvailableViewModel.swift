import AnalyticsModule
import Foundation

protocol ExternalTransferAvailableViewModelInputs: AnyObject {
    func dismiss()
    func sendPayment()
}

final class ExternalTransferAvailableViewModel {
    private let presenter: ExternalTransferAvailablePresenting
    private let transferURLString: String

    init(
        presenter: ExternalTransferAvailablePresenting,
        transferURLString: String
    ) {
        self.presenter = presenter
        self.transferURLString = transferURLString
    }
}

extension ExternalTransferAvailableViewModel: ExternalTransferAvailableViewModelInputs {
    func dismiss() {
        presenter.didNextStep(action: .dismiss)
    }
    
    func sendPayment() {
        Analytics.shared.log(ExternalTransferUserEvent.sendLink)
        presenter.didNextStep(action: .sendPayment(transferURLString))
    }
}
