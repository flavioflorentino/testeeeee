import UIKit

enum ExternalTransferAvailableAction {
    case dismiss
    case sendPayment(String)
}

protocol ExternalTransferAvailableCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ExternalTransferAvailableAction)
}

final class ExternalTransferAvailableCoordinator: ExternalTransferAvailableCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: ExternalTransferAvailableAction) {
        switch action {
        case .dismiss:
            viewController?.dismiss(animated: true)
        case let .sendPayment(transferURLString):
            let controller = UIActivityViewController(activityItems: [transferURLString], applicationActivities: nil)
            viewController?.present(controller, animated: true)
        }
    }
}
