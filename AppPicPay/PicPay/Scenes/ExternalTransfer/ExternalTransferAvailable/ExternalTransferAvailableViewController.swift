import AnalyticsModule
import UI
import UIKit

final class ExternalTransferAvailableViewController: ViewController<ExternalTransferAvailableViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    private lazy var externalTransferDialogView: ExternalTransferDialogView = {
        let view = ExternalTransferDialogView()
        view.delegate = self
        return view
    }()
    
    // MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Analytics.shared.log(ExternalTransferUserEvent.paymentSucceeded)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(externalTransferDialogView)
    }
    
    override func setupConstraints() {
        externalTransferDialogView.layout {
            $0.top == view.topAnchor
            $0.leading == view.compatibleSafeAreaLayoutGuide.leadingAnchor
            $0.trailing == view.compatibleSafeAreaLayoutGuide.trailingAnchor
            $0.bottom == view.compatibleSafeAreaLayoutGuide.bottomAnchor
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        externalTransferDialogView.configureWith(
            title: ExternalTransferLocalizable.transferAvailable.text,
            subtitle: ExternalTransferLocalizable.shareYourTransfer.text,
            cancelButtonText: DefaultLocalizable.btClose.text,
            confirmButtonText: ExternalTransferLocalizable.sendPayment.text,
            info: ExternalTransferLocalizable.withdrawInTwoDays.text
        )
    }
}

extension ExternalTransferAvailableViewController: ExternalTransferDialogDelegate {
    func didTouchOnCancel() {
        viewModel.dismiss()
    }
    
    func didTouchOnConfirm() {
        viewModel.sendPayment()
    }
}

// MARK: View Model Outputs
extension ExternalTransferAvailableViewController: ExternalTransferAvailableDisplay {
}
