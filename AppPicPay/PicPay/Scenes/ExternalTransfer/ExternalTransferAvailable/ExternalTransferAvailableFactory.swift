import Foundation

enum ExternalTransferAvailableFactory {
    static func make(transferURLString: String) -> ExternalTransferAvailableViewController {
        let coordinator: ExternalTransferAvailableCoordinating = ExternalTransferAvailableCoordinator()
        let presenter: ExternalTransferAvailablePresenting = ExternalTransferAvailablePresenter(coordinator: coordinator)
        let viewModel = ExternalTransferAvailableViewModel(presenter: presenter, transferURLString: transferURLString)
        let viewController = ExternalTransferAvailableViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
