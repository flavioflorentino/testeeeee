import AnalyticsModule
import Core
import CorePayment
import Foundation

protocol ExternalTransferCheckoutViewModelInputs: AnyObject {
    func beginEditingTextView()
    func endEditingTextView()
    func changeTextView(text: String)
    func shouldChangeText(_ text: String) -> Bool
    func startGenerateExternalTransfer(value: Double)
    func generateExternalTransfer(message: String, value: Double, privacy: PaymentPrivacy, password: String)
    func privacySelection()
    func paymentMethodSelection()
}

final class ExternalTransferCheckoutViewModel {
    typealias Dependencies = HasConsumerManager
    // MARK: - Error Codes
    private enum ErrorCode: String {
        case limitExceeded = "441"
        case insufficientPayment = "442"
        case valueOutOfAcceptedRange = "443"
        case valueType = "444"
        case wrongPassword = "6003"
    }
    
    // MARK: - Variables
    private let service: ExternalTransferCheckoutServicing
    private let presenter: ExternalTransferCheckoutPresenting
    private var hasOnlyPlaceholderOnTextView: Bool = true
    private let messageMaxLength = 100
    private let dependencies: Dependencies
    
    // MARK: - Life Cycle
    init(service: ExternalTransferCheckoutServicing, presenter: ExternalTransferCheckoutPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - ExternalTransferCheckoutViewModelInputs
extension ExternalTransferCheckoutViewModel: ExternalTransferCheckoutViewModelInputs {
    func beginEditingTextView() {
        guard hasOnlyPlaceholderOnTextView else {
            return
        }
        presenter.showPlaceholderMessage(isVisible: false)
    }
    
    func endEditingTextView() {
        guard hasOnlyPlaceholderOnTextView else {
            return
        }
        presenter.showPlaceholderMessage(isVisible: true)
    }
    
    func changeTextView(text: String) {
        hasOnlyPlaceholderOnTextView = text.isEmpty
    }
    
    func shouldChangeText(_ text: String) -> Bool {
        text.count <= messageMaxLength
    }
    
    func startGenerateExternalTransfer(value: Double) {
        guard value > Double.zero else {
            presenter.presentShouldFillValueAlert()
            return
        }
        presenter.presentPasswordDisplay()
    }
    
    func generateExternalTransfer(message: String, value: Double, privacy: PaymentPrivacy, password: String) {
        let message = hasOnlyPlaceholderOnTextView ? "" : message
        let privacy = privacy == .private ? DefaultLocalizable.privacyPrivate.text : DefaultLocalizable.privacyPublic.text
        Analytics.shared.log(ExternalTransferUserEvent.sent(hasMessage: !hasOnlyPlaceholderOnTextView, requestedValue: value, privacy: privacy))
        service.createExternalTransfer(message: message, value: value, password: password) { [weak self] result in
            switch result {
            case let .success(response):
                self?.createExternalTranferSuccess(response: response.model)
                self?.dependencies.consumerManager.loadConsumerBalance()
                NotificationCenter.default.post(name: NSNotification.Name.feedNeedReload, object: nil)
            case let .failure(error):
                self?.createExternalTranferFailure(error: error, value: value)
            }
        }
    }
    
    private func createExternalTranferSuccess(response: ExternalTransferWithdrawal) {
        presenter.didNextStep(action: .next(url: response.url))
    }
    
    private func createExternalTranferFailure(error: DialogError, value: Double) {
        switch error {
        case let .dialog(response):
            showDialogError(errorCode: response.code, model: response.model, value: value)
        case let .default(apiError):
            showDialogError(apiError: apiError)
        }
    }
    
    private func showDialogError(apiError: ApiError) {
        if let code = apiError.requestError?.code,
            let errorCode = ErrorCode(rawValue: code) {
            if case .wrongPassword = errorCode {
                presenter.presentWrongPasswordAlert()
            } else {
                showGenericError()
            }
            return
        }

        guard let message = apiError.requestError?.message else {
            showGenericError()
            return
        }
        presenter.presentSystemAlert(title: message)
    }
    
    private func showDialogError(errorCode: String, model: DialogErrorModel, value: Double) {
        guard let code = ErrorCode(rawValue: errorCode) else {
            showGenericError()
            return
        }
        
        handleEventOnError(code: code, value: value)
        
        presenter.presentAlert(
            title: model.title,
            message: model.message,
            buttonTitle: model.buttonText
        )
    }
    
    private func handleEventOnError(code: ErrorCode, value: Double) {
        switch code {
        case .limitExceeded:
            Analytics.shared.log(ExternalTransferUserEvent.limitExceeded)
            Analytics.shared.log(ExternalTransferUserEvent.apiError(message: ExternalTransferLocalizable.limitExceeded.text))
        case .insufficientPayment:
            Analytics.shared.log(ExternalTransferUserEvent.insufficientPayment)
            Analytics.shared.log(ExternalTransferUserEvent.apiError(message: ExternalTransferLocalizable.insuficientBalanceToSend.text))
        case .valueOutOfAcceptedRange:
            let stringValue = String(format: "%.2f", value)
            Analytics.shared.log(ExternalTransferUserEvent.valueNotAllowed(value: stringValue))
            Analytics.shared.log(ExternalTransferUserEvent.apiError(message: ExternalTransferLocalizable.valueNotAllowed.text))
        default:
            break
        }
    }
    
    private func showGenericError() {
        Analytics.shared.log(ExternalTransferUserEvent.senderError(origin: .detail))
        Analytics.shared.log(ExternalTransferUserEvent.apiError(message: ExternalTransferLocalizable.opsSomethingWrong.text))
        presenter.presentError()
    }
    
    func privacySelection() {
        Analytics.shared.log(ExternalTransferUserEvent.privacyOpened)
        presenter.presentPrivacyAlert()
    }
    
    func paymentMethodSelection() {
        Analytics.shared.log(ExternalTransferUserEvent.paymentMethodOpened)
        presenter.presentPaymentMethodAlert()
    }
}
