import Foundation

enum ExternalTransferCheckoutFactory {
    static func make() -> ExternalTransferCheckoutViewController {
        let service: ExternalTransferCheckoutServicing = ExternalTransferCheckoutService(dependencies: DependencyContainer())
        let coordinator: ExternalTransferCheckoutCoordinating = ExternalTransferCheckoutCoordinator()
        let presenter: ExternalTransferCheckoutPresenting = ExternalTransferCheckoutPresenter(coordinator: coordinator)
        let viewModel = ExternalTransferCheckoutViewModel(service: service, presenter: presenter, dependencies: DependencyContainer())
        let viewController = ExternalTransferCheckoutViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
