import UI
import UIKit
import AnalyticsModule

extension ExternalTransferCheckoutViewController.Layout {
    enum Size {
        static let imageSide: CGFloat = 60
        static let lineHeight: CGFloat = 1
    }
    enum Font {
        static let username = UIFont.systemFont(ofSize: 14)
        static let currency = UIFont.systemFont(ofSize: 24)
    }
    static let currencyLimitValue: Int = 8
}

final class ExternalTransferCheckoutViewController: ViewController<ExternalTransferCheckoutViewModelInputs, UIView> {
    // MARK: - Layout
    fileprivate enum Layout {}
    
    private var auth: PPAuth?
    
    // MARK: - Visual Components
    private lazy var currencyStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var externalTransferIcon = UIImageView(image: Assets.P2P.externalTransferIcon.image)
    
    private lazy var valueCurrencyField: CurrencyField = {
        let textField = CurrencyField()
        textField.fontValue = Layout.Font.currency
        textField.fontCurrency = Layout.Font.currency
        textField.textColor = Palette.ppColorGrayscale600.color
        textField.placeholderColor = Palette.ppColorGrayscale400.color
        textField.limitValue = Layout.currencyLimitValue
        return textField
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale300.color
        return view
    }()
    
    private lazy var messageTextView: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = .clear
        textView.text = PaymentRequestLocalizable.messagePlaceholder.text
        textView.textColor = Palette.ppColorGrayscale400.color
        textView.delegate = self
        return textView
    }()
    
    private lazy var paymentToolbar: PaymentRequestToolbar = {
        let constructModel = PaymentRequestToolbarModel(
            sendButtonTitle: ProfileLocalizable.pay.text,
            initialPrivacy: .private,
            initialPaymentMethod: .accountBalance,
            isPrivacyEditable: false,
            paymentMethodAvailability: .uneditable,
            delegate: self
        )
        let toolbar = PaymentRequestToolbarFactory.make(constructModel: constructModel)
        toolbar.translatesAutoresizingMaskIntoConstraints = false
        return toolbar
    }()

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        Analytics.shared.log(ExternalTransferUserEvent.detailViewed)
        valueCurrencyField.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(currencyStackView)
        currencyStackView.addArrangedSubview(externalTransferIcon)
        currencyStackView.addArrangedSubview(valueCurrencyField)
        view.addSubview(lineView)
        view.addSubview(messageTextView)
    }
    
    override func configureViews() {
        navigationItem.title = ExternalTransferLocalizable.transfer.text
        view.backgroundColor = Palette.ppColorGrayscale000.color
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale100.color
        valueCurrencyField.inputAccessory = paymentToolbar
        messageTextView.inputAccessoryView = paymentToolbar
    }
    
    override func setupConstraints() {
        currencyStackView.layout {
            $0.leading == view.compatibleSafeAreaLayoutGuide.leadingAnchor + Spacing.base01
            $0.top == view.topAnchor + Spacing.base01
            $0.trailing == view.compatibleSafeAreaLayoutGuide.trailingAnchor - Spacing.base01
        }
        externalTransferIcon.layout {
            $0.height == Layout.Size.imageSide
            $0.width == Layout.Size.imageSide
        }
        lineView.layout {
            $0.top == currencyStackView.bottomAnchor + Spacing.base01
            $0.leading == view.compatibleSafeAreaLayoutGuide.leadingAnchor
            $0.trailing == view.compatibleSafeAreaLayoutGuide.trailingAnchor
            $0.height == Layout.Size.lineHeight
        }
        messageTextView.layout {
            $0.top == lineView.bottomAnchor + Spacing.base01
            $0.leading == currencyStackView.leadingAnchor
            $0.trailing == currencyStackView.trailingAnchor
            $0.bottom == view.compatibleSafeAreaLayoutGuide.bottomAnchor
        }
    }
}

// MARK: ExternalTransferCheckoutDisplay
extension ExternalTransferCheckoutViewController: ExternalTransferCheckoutDisplay {
    func showTextMessage(_ text: String) {
        messageTextView.text = text
    }
    
   func displaySystemAlert(title: String, message: String, buttonTitle: String) {
        let alertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        let handlerAction: ((UIAlertAction) -> Void)? = { _ in
            self.valueCurrencyField.becomeFirstResponder()
        }
        let action = UIAlertAction(
            title: buttonTitle,
            style: .default,
            handler: handlerAction
        )
        alertController.addAction(action)
        present(alertController, animated: true)
    }
    
    func displayAlert(title: String, message: String, buttonTitle: String) {
        let button = Button(title: buttonTitle, type: .cta, action: .handler)
        button.handler = { controller, _ in
            controller.dismiss(animated: true) {
                self.resetDisplay()
            }
        }
        let alert = Alert(
            with: title,
            text: message,
            buttons: [button]
        )
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func displayError(with alertData: StatusAlertData) {
        let alert = StatusAlertView.show(on: view, data: alertData)
        alert.statusAlertDelegate = self
    }
    
    func resetDisplay() {
        endState()
        valueCurrencyField.becomeFirstResponder()
    }
    
    func showPasswordDisplay() {
        auth = PPAuth.authenticate({ [weak self] password, _ in
            guard
                let self = self,
                let password = password
                else {
                    return
            }
            self.beginState()
            self.viewModel.generateExternalTransfer(
                message: self.messageTextView.text ?? "",
                value: self.valueCurrencyField.value,
                privacy: self.paymentToolbar.privacy,
                password: password
            )
        }, canceledByUserBlock: { [weak self] in
            self?.resetDisplay()
        })
    }
}

// MARK: - UITextViewDelegate
extension ExternalTransferCheckoutViewController: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        viewModel.beginEditingTextView()
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        viewModel.endEditingTextView()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let resultText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        viewModel.changeTextView(text: resultText)
        return viewModel.shouldChangeText(resultText)
    }
}

// MARK: - PaymentRequestToolbarDelegate
extension ExternalTransferCheckoutViewController: PaymentRequestToolbarDelegate {
    func didTouchOnSendButton() {
        valueCurrencyField.resignResponder()
        messageTextView.resignFirstResponder()
        viewModel.startGenerateExternalTransfer(value: valueCurrencyField.value)
    }
    
    func showPrivacyAlertSelector(alert: UIAlertController) {
        present(alert, animated: true, completion: nil)
    }
    
    func didTouchOnPaymentMethod() {
        viewModel.paymentMethodSelection()
    }
    
    func didTouchOnPrivacy() {
        viewModel.privacySelection()
    }
}

// MARK: - StatusAlertViewDelegate
extension ExternalTransferCheckoutViewController: StatusAlertViewDelegate {
    func didTouchOnButton() {
        resetDisplay()
    }
}

// MARK: - StatefulProviding
extension ExternalTransferCheckoutViewController: StatefulProviding {
}
