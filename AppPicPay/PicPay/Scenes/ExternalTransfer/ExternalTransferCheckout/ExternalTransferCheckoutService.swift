import Core

protocol ExternalTransferCheckoutServicing {
    func createExternalTransfer(message: String, value: Double, password: String, completion: @escaping (Result<(model: ExternalTransferWithdrawal, data: Data?), DialogError>) -> Void)
}

extension NSNotification.Name {
    static var feedNeedReload: NSNotification.Name { .init("FeedNeedReload") }
}

final class ExternalTransferCheckoutService {
    typealias Dependencies = HasMainQueue
    // MARK: - Variables
    private let dependencies: Dependencies
    
    // MARK: - Life Cycle
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ExternalTransferCheckoutServicing
extension ExternalTransferCheckoutService: ExternalTransferCheckoutServicing {
    func createExternalTransfer(message: String, value: Double, password: String, completion: @escaping (Result<(model: ExternalTransferWithdrawal, data: Data?), DialogError>) -> Void) {
        let endpoint = ExternalTransferServiceEndpoint.createExternalTransfer(
            message: message,
            value: value,
            password: password
        )
        Api<ExternalTransferWithdrawal>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.mapError(DialogError.transformApiError)
                completion(mappedResult)
            }
        }
    }
}
