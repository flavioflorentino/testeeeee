import UI

protocol ExternalTransferCheckoutDisplay: AnyObject {
    func showTextMessage(_ text: String)
    func displaySystemAlert(title: String, message: String, buttonTitle: String)
    func displayAlert(title: String, message: String, buttonTitle: String)
    func displayError(with alertData: StatusAlertData)
    func showPasswordDisplay()
    func resetDisplay()
}
