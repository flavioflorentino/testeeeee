import UIKit

enum ExternalTransferCheckoutAction {
    case next(url: String)
}

protocol ExternalTransferCheckoutCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ExternalTransferCheckoutAction)
}

final class ExternalTransferCheckoutCoordinator: ExternalTransferCheckoutCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: ExternalTransferCheckoutAction) {
        if case let ExternalTransferCheckoutAction.next(url) = action {
            let controller = ExternalTransferAvailableFactory.make(transferURLString: url)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
