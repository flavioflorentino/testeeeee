import Core
import UI

protocol ExternalTransferCheckoutPresenting: AnyObject {
    var viewController: ExternalTransferCheckoutDisplay? { get set }
    func didNextStep(action: ExternalTransferCheckoutAction)
    func showPlaceholderMessage(isVisible: Bool)
    func presentSystemAlert(title: String)
    func presentShouldFillValueAlert()
    func presentPaymentMethodAlert()
    func presentPrivacyAlert()
    func presentAlert(title: String, message: String, buttonTitle: String)
    func presentError()
    func presentWrongPasswordAlert()
    func presentPasswordDisplay()
}

final class ExternalTransferCheckoutPresenter {
    private let coordinator: ExternalTransferCheckoutCoordinating
    weak var viewController: ExternalTransferCheckoutDisplay?

    init(coordinator: ExternalTransferCheckoutCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ExternalTransferCheckoutPresenting
extension ExternalTransferCheckoutPresenter: ExternalTransferCheckoutPresenting {
    func showPlaceholderMessage(isVisible: Bool) {
        let text = isVisible ? PaymentRequestLocalizable.messagePlaceholder.text : ""
        viewController?.showTextMessage(text)
    }
    
    func didNextStep(action: ExternalTransferCheckoutAction) {
        coordinator.perform(action: action)
    }
    
    func presentSystemAlert(title: String) {
        let buttonTitle = DefaultLocalizable.btOk.text
        viewController?.displaySystemAlert(title: title, message: "", buttonTitle: buttonTitle)
    }
    
    func presentShouldFillValueAlert() {
        let title = ExternalTransferLocalizable.externalTransferShouldFillValueAlertTitle.text
        let buttonTitle = DefaultLocalizable.btOk.text
        viewController?.displaySystemAlert(title: title, message: "", buttonTitle: buttonTitle)
    }
    
    func presentPaymentMethodAlert() {
        viewController?.displayAlert(
            title: RechargeLocalizable.paymentMethods.text,
            message: ExternalTransferLocalizable.paymentMethodAlertMessage.text,
            buttonTitle: ExternalTransferLocalizable.okUnderstood.text
        )
    }
    
    func presentPrivacyAlert() {
        viewController?.displayAlert(
            title: ExternalTransferLocalizable.privatePayment.text,
            message: ExternalTransferLocalizable.privacyMessage.text,
            buttonTitle: ExternalTransferLocalizable.okUnderstood.text
        )
    }
    
    func presentAlert(title: String, message: String, buttonTitle: String) {
        viewController?.displayAlert(title: title, message: message, buttonTitle: buttonTitle)
    }
    
    func presentError() {
        let alertData = StatusAlertData(
            icon: Assets.Emotions.iluSadFace.image,
            title: ExternalTransferLocalizable.opsSomethingWrong.text,
            text: ExternalTransferLocalizable.tryAgainLater.text,
            buttonTitle: PaymentRequestLocalizable.tryAgain.text,
            isCloseButtonHidden: true
        )
        viewController?.displayError(with: alertData)
    }
    
    func presentWrongPasswordAlert() {
        PPAuthSwift.handlePasswordError()
        viewController?.resetDisplay()
    }
    
    func presentPasswordDisplay() {
        viewController?.showPasswordDisplay()
    }
}
