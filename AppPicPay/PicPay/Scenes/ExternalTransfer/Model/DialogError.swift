import Core

enum DialogError: Error {
    case `default`(ApiError)
    case dialog(DialogResponseError)
    
    static func transformApiError(_ error: ApiError) -> DialogError {
        guard
            let requestError = error.requestError,
            let data = requestError.dictionaryObject?["data"] as? [String: String],
            let buttonText = data["button_text"]
            else {
                return .default(error)
        }
        let dialogModel = DialogErrorModel(
            title: requestError.title,
            message: requestError.message,
            buttonText: buttonText
        )
        
        return .dialog(DialogResponseError(code: requestError.code, model: dialogModel))
    }
}

struct DialogResponseError {
    let code: String
    let model: DialogErrorModel
}

struct DialogErrorModel {
    let title: String
    let message: String
    let buttonText: String
}
