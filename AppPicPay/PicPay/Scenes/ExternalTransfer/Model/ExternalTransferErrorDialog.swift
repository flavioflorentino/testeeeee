import Foundation

struct ExternalTransferErrorDialog {
    let title: String
    let message: String
    let buttonText: String
}
