import Foundation

struct ExternalTransferWithdrawal: Decodable {
    let url: String
}
