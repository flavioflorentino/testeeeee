import Foundation

final class EcommerceAlertController: UIViewController {
    var alert: EcommerceAlert
    var action: ((EcommerceAlertButtonAction) -> Void)?
    
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var descriptionLabel: UILabel!
    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var actionButton: UIPPButton!
    @IBOutlet private var container: UIView!
    
    // MARK: - Initializer
    init(alert: EcommerceAlert) {
        self.alert = alert
        super.init(nibName: String(describing: EcommerceAlertController.self), bundle: Bundle.main)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = alert.title
        descriptionLabel.text = alert.message
        actionButton.setTitle(alert.buttonTitle, for: .normal)
        
        let imageResouce = alert.type == .positive ? Assets.Icons.icoGreenCheckmark : Assets.Icons.icoRedX
        imageView.image = imageResouce.image
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        container.layer.cornerRadius = 10
    }
    
    @IBAction private func didTapAction(_ sender: Any) {
        action?(alert.action)
    }
}
