import UI
import UIKit

enum EcommercePaymentAction {
    case showReceipt(transactionId: String, widgets: [ReceiptWidgetItem])
    case openInsertCvv(completedCvv: (String) -> Void, completedWithoutCvv: () -> Void)
    case installments(_ paymentManager: PPPaymentManager, sellerId: String, installmentsCallback: InstallmentsSelectorViewModelDelegate?)
    case close
    case dismiss
}

protocol EcommercePaymentCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: EcommercePaymentAction)
}

final class EcommercePaymentCoordinator: EcommercePaymentCoordinating {
    private var coordinator: Coordinating?
    weak var viewController: UIViewController?
    
    func perform(action: EcommercePaymentAction) {
        switch action {
        case let .showReceipt(transactionId, widgets):
            showRceipt(transactionId: transactionId, widgets: widgets)
            
        case let .installments(paymentManager, sellerId, installmentsCallback):
            let viewModel = InstallmentsSelectorViewModel(paymentManager: paymentManager, sellerId: sellerId)
            viewModel.delegate = installmentsCallback
            let controller = InstallmentsSelectorViewController(with: viewModel)
            viewController?.navigationController?.pushViewController(controller, animated: true)
            
        case .close:
            viewController?.navigationController?.popToRootViewController(animated: true)
            
        case .dismiss:
            viewController?.navigationController?.dismiss(animated: true)
            
        case let .openInsertCvv(completedCvv, completedWithoutCvv):
            showInsertCvv(completedCvv: completedCvv, completedWithoutCvv: completedWithoutCvv)
        }
    }
    
    func showRceipt(transactionId: String, widgets: [ReceiptWidgetItem]) {
        guard let viewController = self.viewController else {
            return
        }
        
        let receiptModel = ReceiptWidgetViewModel(transactionId: transactionId, type: .PAV)
        receiptModel.setReceiptWidgets(items: widgets)
        TransactionReceipt.showReceiptSuccess(viewController: viewController, receiptViewModel: receiptModel)
    }
    
    func showInsertCvv(completedCvv: @escaping (String) -> Void, completedWithoutCvv: @escaping () -> Void) {
        guard let navigationController = viewController?.navigationController else {
            return
        }
        
        let coordinator = CvvRegisterFlowCoordinator(
            navigationController: navigationController,
            paymentType: .ecommerce,
            finishedCvv: completedCvv,
            finishedWithoutCvv: completedWithoutCvv
        )
        coordinator.start()
        
        self.coordinator = coordinator
    }
}
