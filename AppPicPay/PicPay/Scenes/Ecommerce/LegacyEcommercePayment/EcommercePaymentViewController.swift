import Foundation
import UI

final class EcommercePaymentViewController: PPBaseViewController {
    var viewModel: EcommercePaymentViewModelInputs?
    private var toolbarController: PaymentToolbarController?
    private var privacyConfig: String?
    
    private lazy var loadingView: UIView? = {
        guard let loadingView = Loader.getLoadingView(EcommercePaymentLocalizable.loadingTransactionMessage.text) else {
            return nil
        }
        
        view.addSubview(loadingView)
        loadingView.isHidden = true
        return loadingView
    }()
    
    private lazy var blurEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = bannerView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        return blurEffectView
    }()
    
    override var shouldAddBreadcrumb: Bool {
        false
    }
    
    // MARK: Outlets
    @IBOutlet private var sellerImageView: UICircularImageView!
    @IBOutlet private var sellerNameLabel: UILabel!
    @IBOutlet private var valueLabel: UILabel!
    @IBOutlet private var orderNumberLabel: UILabel!
    @IBOutlet private var toolbarContainer: UIView!
    @IBOutlet private var bannerImageView: UIImageView!
    @IBOutlet private var bannerView: UIView!
    @IBOutlet private var installmentsButton: UIPPButton!
    
    // MARK: - Factory
    static func controllerFromStoryboard() -> EcommercePaymentViewController {
        EcommercePaymentViewController.controllerFromStoryboard(.ecommerce)
    }
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupContraints()
        
        viewModel?.loadOrUpdateData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true
        viewModel?.updatePaymentInfo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.navigationBar.isHidden = false
    }
    
    // MARK: - User Actions
    
    @IBAction private func didTapInstallment(_ sender: Any?) {
        viewModel?.navigateToInstallments()
    }
    
    @IBAction private func close(_ sender: Any?) {
        viewModel?.close(shouldDismiss: self.presentingViewController != nil)
    }

    func pay() {
        viewModel?.authenticate(privacyConfig: privacyConfig)
    }

    // MARK: - Setup Views
    
    private func setupViews() {
        // swiftlint:disable overpowered_viewcontroller
        if let paymentManager = viewModel?.paymentManager {
            toolbarController = PaymentToolbarController(paymentManager: paymentManager, parent: self)
        }
        // swiftlint:enable overpowered_viewcontroller
        
        if let toolbar = toolbarController?.toolbar {
            toolbarContainer.addSubview(toolbar)
        }
        
        toolbarController?.payAction = { [weak self] privacyConfig in
            self?.privacyConfig = privacyConfig
            self?.pay()
        }
        
        bannerView.addSubview(blurEffectView)
        
        if hasSafeAreaInsets {
            paintSafeAreaBottomInset(withColor: toolbarController?.bottomBarBackgroundColor ?? .clear)
        }
    }
    
    private func setupContraints() {
        toolbarController?.toolbar.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

// MARK: - Presenter Output

extension EcommercePaymentViewController: EcommercePaymentDisplayable {
    func enableLoading() {
        loadingView?.isHidden = false
    }
    
    func disableLoading() {
        loadingView?.isHidden = true
    }
    
    func presentCreateTransactionError(_ error: Error?) {
        disableLoading()
        
        AlertMessage.showAlertWithError(error, controller: self)
    }
    
    func didReceive(error: PicPayError) {
        disableLoading()
        AlertMessage.showAlertWithError(error, controller: self)
    }
    
    func updateScreenData(subtotal: String, sellerName: String, sellerImageURL: URL?, orderNumber: String, alert: EcommerceAlert?) {
        disableLoading()
        
        valueLabel.text = subtotal
        sellerNameLabel.text = sellerName
        sellerImageView.setImage(url: sellerImageURL, placeholder: PPStore.photoPlaceholder())
        bannerImageView.setImage(url: sellerImageURL, placeholder: PPStore.photoPlaceholder())
        orderNumberLabel.text = orderNumber
        
        viewModel?.updatePaymentInfo()
        
        if let alert = alert {
            presentPopupAlert(with: alert)
        }
    }
    
    func presentPopupAlert(with alert: EcommerceAlert) {
        let popup = EcommerceAlertController(alert: alert)

        popup.action = { [weak self] actionType in
            guard case .back = actionType else {
                return
            }

            popup.dismiss(animated: true) {
                self?.dismiss(animated: true, completion: nil)
            }
        }
        
        showPopup(popup)
    }
    
    func presentOrderInfoError(_ error: PicPayError) {
        loadingView?.isHidden = true
        
        AlertMessage.showAlert(error, controller: self) {
            self.close(nil)
        }
    }
    
    func updateInstallmentsInfo(_ selectedQuotaQuantity: String, hasInstallments: Bool) {
        installmentsButton.isEnabled = hasInstallments
        installmentsButton.setTitle(selectedQuotaQuantity, for: .normal)
    }
}
