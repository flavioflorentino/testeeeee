final class EcommercePaymentRequest: BaseApiRequest {
    // Private
    private var _biometry: Bool?
    
    var orderId: String?
    var sellerId: String?
    var creditCardId: String?
    var total: NSDecimalNumber?
    var credit: NSDecimalNumber?
    var parcelas: String?
    var someErrorOccurred: Bool?
    var biometry: Bool?
    var paymentPrivacyConfig: String?
    var origin: String?
    
    var paramsToBeMerged: [String: Any]?
    
    // All params are obligatory
    func obligatoryParams(params: [String: Any?]) throws -> [String: Any] {
        let nonNil = params.nilsRemoved
        
        if nonNil.keys.count < params.keys.count {
            throw PicPayError(message: "Missing params")
        }
        
        return nonNil
    }
    
    func mergeToParams(params: [String: Any]) {
        self.paramsToBeMerged = params
    }
    
    func toParams() throws -> [String: Any] {
        let params: [String: Any?] = [
            "orderId": orderId,
            "origin": origin ?? "ecommerce",
            "seller_id": sellerId,
            "consumer_credit_card_id": creditCardId ?? "0",
            "value": total?.stringValue ?? "0",
            "credit": credit?.stringValue ?? "0",
            "parcelas": parcelas ?? "1",
            "feed_visibility": paymentPrivacyConfig ?? "2",
            "biometry": biometry,
            "duplicated": someErrorOccurred
        ]
        
        return try obligatoryParams(params: params)
    }
}
