import SwiftyJSON

final class CreateTransactionResponse: BaseApiResponse {
    let transactionId: String
    let receiptWidgets: [ReceiptWidgetItem]
    
    init(transactionId: String = "", receiptWidgets: [ReceiptWidgetItem] = []) {
        self.transactionId = transactionId
        self.receiptWidgets = receiptWidgets
    }
    
    required init() {
        transactionId = ""
        receiptWidgets = []
    }
    
    required init?(json: JSON) {
        guard let receipt = json["coreTransactionReceipt"].array else {
            return nil
        }
        receiptWidgets = WSReceipt.createReceiptWidgetItem(receipt)
        transactionId = json["coreTransactionId"].string ?? ""
    }
}
