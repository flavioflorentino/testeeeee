import Foundation
import SwiftyJSON

final class EcommercePaymentInfo: BaseApiResponse {
    var needLoad: Bool = false
    var id: String
    var sellerImage: String?
    var value: NSNumber
    var orderNumber: String
    var expirationDate: String
    var sellerName: String
    var sellerId: String
    var orderReference: String
    var alert: EcommerceAlert?
    var forceCreditCard: Bool
    
    init(
        id: String = "",
        sellerImage: String? = nil,
        value: NSNumber = NSNumber(value: 0),
        orderNumber: String = "",
        expirationDate: String = "",
        sellerName: String = "",
        sellerId: String = "",
        orderReference: String = "",
        alert: EcommerceAlert? = nil,
        forceCreditCard: Bool = false
    ) {
        self.id = id
        self.sellerImage = sellerImage
        self.value = value
        self.orderNumber = orderNumber
        self.expirationDate = expirationDate
        self.sellerName = sellerName
        self.sellerId = sellerId
        self.orderReference = orderReference
        self.alert = alert
        self.forceCreditCard = forceCreditCard
    }
    
    required init() {
        id = ""
        sellerId = ""
        sellerName = ""
        value = NSNumber(value: 0)
        orderNumber = ""
        expirationDate = ""
        orderReference = ""
        forceCreditCard = false
    }
    
    required init?(json: JSON) {
        id = json["_id"].string ?? ""
        sellerImage = json["seller_image"].string
        sellerId = json["seller_id"].string ?? ""
        sellerName = json["seller_name"].string ?? ""
        value = json["value"].number ?? NSNumber(value: 0)
        orderNumber = json["order_number"].string ?? ""
        expirationDate = json["expiration_date"].string ?? ""
        orderReference = json["store_order_id"].string ?? ""
        alert = EcommerceAlert(json: json["alert"])
        forceCreditCard = json["force_credit_card"].bool ?? false
    }
}
