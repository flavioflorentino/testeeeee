//
//  EcommerceAlert.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 12/03/18.
//

import Foundation
import SwiftyJSON

enum EcommerceAlertType: String {
    // swiftlint:disable redundant_string_enum_value
    case positive = "positive"
    case negative = "negative"
    // swiftlint:enable redundant_string_enum_value
}

enum EcommerceAlertButtonAction: String {
    // swiftlint:disable redundant_string_enum_value
    case back = "back"
    // swiftlint:enable redundant_string_enum_value
}

final class EcommerceAlert: NSObject {
    var message = ""
    var title = ""
    var buttonTitle = ""
    var type: EcommerceAlertType = .positive
    var action: EcommerceAlertButtonAction = .back
    
    required init?(json: JSON) {
        guard
            let message = json["message"].string,
            let title = json["title"].string,
            let buttonTitle = json["button_title"].string,
            let typeString = json["type"].string,
            let actionString = json["action"].string
            else {
                return nil
        }
        
        self.message = message
        self.title = title
        self.buttonTitle = buttonTitle
        
        if let type = EcommerceAlertType(rawValue: typeString) {
            self.type = type
        }
        
        if let action = EcommerceAlertButtonAction(rawValue: actionString) {
            self.action = action
        }
    }
}
