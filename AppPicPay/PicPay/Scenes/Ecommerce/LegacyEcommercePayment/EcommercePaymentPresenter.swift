import AnalyticsModule
import CoreLegacy

protocol EcommercePaymentPresenting: AnyObject {
    var coordinator: EcommercePaymentCoordinating? { get set }
    var viewController: EcommercePaymentDisplayable? { get set }

    func handleEnableLoading()
    func handleDisableLoading()
    func didReceive(error: PicPayError)
    func handleSuccessCreateTransaction(transactionId: String, totalValue: Decimal, receiptWidgets: [ReceiptWidgetItem])
    func handleFailureCreateTransaction(error: Error?)
    func handleLoadOrUpdateDataInfo(subtotal: NSNumber, sellerName: String, sellerImage: String, orderReference: String, alert: EcommerceAlert?)
    func handleGetOrderInfoError(_ error: PicPayError)
    func handleInstallmentsInfo(_ selectedQuotaQuantity: Int, hasInstallments: Bool)
    func handleNavigateToInstallments(_ paymentManager: PPPaymentManager, sellerId: String, installmentsCallback: InstallmentsSelectorViewModelDelegate?)
    func handleClose(_ shouldDismiss: Bool)
    func handleInsertCvv(completedCvv: @escaping (String) -> Void, completedWithoutCvv: @escaping () -> Void)
}

final class EcommercePaymentPresenter {
    var coordinator: EcommercePaymentCoordinating?
    weak var viewController: EcommercePaymentDisplayable?
    private let queue: DispatchQueue
    
    private let realSymbol = EcommercePaymentLocalizable.realSymbol.text
    private let orderSymbol = EcommercePaymentLocalizable.orderSymbol.text
    private let orderString = EcommercePaymentLocalizable.orderString.text
    private let installmentsSymbol = EcommercePaymentLocalizable.installmentsSymbol.text
    private let installmentsString = EcommercePaymentLocalizable.installmentsString.text

    init(coordinator: EcommercePaymentCoordinating, queue: DispatchQueue = DispatchQueue.main) {
        self.coordinator = coordinator
        self.queue = queue
    }
}

extension EcommercePaymentPresenter: EcommercePaymentPresenting {
    func handleEnableLoading() {
        queue.async {
            self.viewController?.enableLoading()
        }
    }
    
    func handleDisableLoading() {
        queue.async {
            self.viewController?.disableLoading()
        }
    }
    
    func didReceive(error: PicPayError) {
        queue.async {
            self.viewController?.didReceive(error: error)
        }
    }
    
    func handleSuccessCreateTransaction(transactionId: String, totalValue: Decimal, receiptWidgets: [ReceiptWidgetItem]) {
        queue.async {
            NotificationCenter.default.post(name: Notification.Name.Payment.new, object: nil, userInfo: ["type": "Ecommerce"])
            Analytics.shared.log(PaymentEvent.transaction(.ecommerce, id: transactionId, value: totalValue))
            
            self.coordinator?.perform(action: .showReceipt(transactionId: transactionId, widgets: receiptWidgets))
        }
    }
    
    func handleFailureCreateTransaction(error: Error?) {
        queue.async {
            self.viewController?.presentCreateTransactionError(error)
        }
    }
    
    func handleLoadOrUpdateDataInfo(subtotal: NSNumber, sellerName: String, sellerImage: String, orderReference: String, alert: EcommerceAlert?) {
        let subtotalFormatted = CurrencyFormatter.brazillianRealString(from: subtotal).replacingOccurrences(of: realSymbol, with: "").trim()
        let sellerImageURL = URL(string: sellerImage)
        let orderNumber = orderString.replacingOccurrences(of: orderSymbol, with: orderReference)
        
        queue.async {
            self.viewController?.updateScreenData(
                subtotal: subtotalFormatted,
                sellerName: sellerName,
                sellerImageURL: sellerImageURL,
                orderNumber: orderNumber,
                alert: alert
            )
        }
    }
    
    func handleGetOrderInfoError(_ error: PicPayError) {
        queue.async {
            self.viewController?.presentOrderInfoError(error)
        }
    }
    
    func handleInstallmentsInfo(_ selectedQuotaQuantity: Int, hasInstallments: Bool) {
        let selectedQuotaQuantity = installmentsString.replacingOccurrences(of: installmentsSymbol, with: "\(selectedQuotaQuantity)")
        viewController?.updateInstallmentsInfo(selectedQuotaQuantity, hasInstallments: hasInstallments)
    }
    
    func handleNavigateToInstallments(_ paymentManager: PPPaymentManager, sellerId: String, installmentsCallback: InstallmentsSelectorViewModelDelegate?) {
        let action: EcommercePaymentAction = .installments(
            paymentManager,
            sellerId: sellerId,
            installmentsCallback: installmentsCallback
        )
        
        coordinator?.perform(action: action)
    }
    
    func handleClose(_ shouldDismiss: Bool) {
        coordinator?.perform(action: shouldDismiss ? .dismiss : .close)
    }
    
    func handleInsertCvv(completedCvv: @escaping (String) -> Void, completedWithoutCvv: @escaping () -> Void) {
        coordinator?.perform(action: .openInsertCvv(completedCvv: completedCvv, completedWithoutCvv: completedWithoutCvv))
    }
}
