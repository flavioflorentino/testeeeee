protocol EcommercePaymentDisplayable: AnyObject {
    func enableLoading()
    func disableLoading()
    func presentCreateTransactionError(_ error: Error?)
    func didReceive(error: PicPayError)
    func updateScreenData(subtotal: String, sellerName: String, sellerImageURL: URL?, orderNumber: String, alert: EcommerceAlert?)
    func presentOrderInfoError(_ error: PicPayError)
    func updateInstallmentsInfo(_ selectedQuotaQuantity: String, hasInstallments: Bool)
}
