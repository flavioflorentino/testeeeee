import FeatureFlag
import Foundation
import UIKit

@objc
enum EcommercePaymentOrigin: Int {
    case scanner = 0
    case notification
    case deeplink
    
    func toString() -> String {
        switch self {
        case .scanner:
            return "Scanner"
        case .notification:
            return "Notification"
        case .deeplink:
            return "Deeplink"
        }
    }
}

final class EcommercePaymentFactory: NSObject {
    static func make(origin: EcommercePaymentOrigin, paymentInfo: EcommercePaymentInfo) -> UIViewController {
        let service: EcommerceServicing = EcommerceService()
        let coordinator: EcommercePaymentCoordinating = EcommercePaymentCoordinator()
        let presenter: EcommercePaymentPresenting = EcommercePaymentPresenter(coordinator: coordinator)
        let paymentService: EcommercePaymentServicing = createEcommercePaymentService()
        let viewModel = EcommercePaymentViewModel(
            paymentInfo: paymentInfo,
            origin: origin,
            paymentService: paymentService,
            service: service,
            presenter: presenter
        )
        let viewController = EcommercePaymentViewController.controllerFromStoryboard()
        viewController.viewModel = viewModel
        
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
    
    @objc
    static func make(origin: EcommercePaymentOrigin, orderNumber: String) -> UIViewController {
        let service: EcommerceServicing = EcommerceService()
        let paymentService: EcommercePaymentServicing = createEcommercePaymentService()
        let coordinator: EcommercePaymentCoordinating = EcommercePaymentCoordinator()
        let presenter: EcommercePaymentPresenting = EcommercePaymentPresenter(coordinator: coordinator)
        let viewModel = EcommercePaymentViewModel(
            orderId: orderNumber,
            origin: origin,
            service: service,
            paymentService: paymentService,
            presenter: presenter
        )
        let viewController = EcommercePaymentViewController.controllerFromStoryboard()
        viewController.viewModel = viewModel
        
        coordinator.viewController = viewController
        presenter.viewController = viewController
        
        return viewController
    }
    
    private static func createEcommercePaymentService() -> EcommercePaymentServicing {
        FeatureManager.isActive(.pciEcommerce) ? LegacyEcommercePciPaymentService() : LegacyEcommercePaymentService()
     }
}
