protocol EcommercePaymentViewModelInputs: AnyObject {
    var paymentManager: PPPaymentManager { get }
    
    func loadOrUpdateData()
    func updateData()
    func loadOrderInfo()
    func updatePaymentInfo()
    func authenticate(privacyConfig: String?)
    func navigateToInstallments()
    func close(shouldDismiss: Bool)
}

final class EcommercePaymentViewModel {
    typealias Dependencies = HasFeatureManager

    private(set) var paymentManager: PPPaymentManager
    private var paymentInfo: EcommercePaymentInfo?
    private let origin: EcommercePaymentOrigin
    private let service: EcommerceServicing
    private let paymentService: EcommercePaymentServicing
    private let presenter: EcommercePaymentPresenting
    private let dependencies: Dependencies
    
    private var someErrorOccurred = false
    
    var auth: PPAuth?

    convenience init(
        orderId: String,
        origin: EcommercePaymentOrigin,
        service: EcommerceServicing,
        paymentService: EcommercePaymentServicing,
        presenter: EcommercePaymentPresenting,
        dependencies: Dependencies = DependencyContainer()
    ) {
        let paymentInfo = EcommercePaymentInfo()
        paymentInfo.orderNumber = orderId
        paymentInfo.needLoad = true
        
        self.init(
            paymentInfo: paymentInfo, 
            origin: origin, 
            paymentService: paymentService, 
            service: service, 
            presenter: presenter, 
            dependencies: dependencies
        )
    }
    
    init(
        paymentInfo: EcommercePaymentInfo,
        paymentManager: PPPaymentManager = PPPaymentManager(),
        origin: EcommercePaymentOrigin,
        paymentService: EcommercePaymentServicing,
        service: EcommerceServicing,
        presenter: EcommercePaymentPresenting,
        dependencies: Dependencies = DependencyContainer()
    ) {
        self.dependencies = dependencies
        self.paymentInfo = paymentInfo
        self.paymentManager = paymentManager
        self.paymentManager.subtotal = NSDecimalNumber(value: paymentInfo.value.doubleValue)

        if dependencies.featureManager.isActive(.isPagTesouroOnlyCreditCardAvailable) {
            self.paymentManager.forceCreditCard = paymentInfo.forceCreditCard
        }

        self.origin = origin
        self.paymentService = paymentService
        self.service = service
        self.presenter = presenter
    }
}

extension EcommercePaymentViewModel: EcommercePaymentViewModelInputs {
    func loadOrUpdateData() {
        guard paymentInfo?.needLoad ?? false else {
            updateData()
            return
        }
        
        loadOrderInfo()
    }
    
    func updateData() {
        self.presenter.handleLoadOrUpdateDataInfo(
            subtotal: paymentManager.subtotal,
            sellerName: paymentInfo?.sellerName ?? "",
            sellerImage: paymentInfo?.sellerImage ?? "",
            orderReference: paymentInfo?.orderReference ?? "",
            alert: paymentInfo?.alert
        )
    }
    
    func loadOrderInfo() {
        service.getOrderInfo(paymentInfo?.orderNumber ?? "") { [weak self] response in
            switch response {
            case let .success(paymentInfo):
                self?.handleUpdateDataInfo(with: paymentInfo)
                
            case let .failure(error):
                self?.presenter.handleGetOrderInfoError(error)
            }
        }
    }
    
    private func handleUpdateDataInfo(with paymentInfo: EcommercePaymentInfo) {
        self.paymentInfo = paymentInfo
        
        let subtotal = NSDecimalNumber(value: paymentInfo.value.doubleValue)
        
        DispatchQueue.main.async {
            self.paymentManager.subtotal = subtotal
        }
        
        self.presenter.handleLoadOrUpdateDataInfo(
            subtotal: subtotal,
            sellerName: self.paymentInfo?.sellerName ?? "",
            sellerImage: self.paymentInfo?.sellerImage ?? "",
            orderReference: self.paymentInfo?.orderReference ?? "",
            alert: self.paymentInfo?.alert
        )
    }
    
    func updatePaymentInfo() {
        if paymentManager.selectedQuotaQuantity > paymentManager.installmentsList().count {
            let installmentsList = paymentManager.installmentsList() ?? []
            paymentManager.selectedQuotaQuantity = installmentsList.isEmpty ? 1 : installmentsList.count
        }
    
        let hasInstallments = paymentManager.installmentsList().count > 1
        presenter.handleInstallmentsInfo(paymentManager.selectedQuotaQuantity, hasInstallments: hasInstallments)
    }
    
    func authenticate(privacyConfig: String?) {
        auth = PPAuth.authenticate({ [weak self] authToken, biometry in
            self?.presenter.handleEnableLoading()
            self?.makePayment(password: authToken ?? "", biometry: biometry, privacyConfig: privacyConfig)
        }, canceledByUserBlock: nil)
    }
    
    private func makePayment(password: String, biometry: Bool, privacyConfig: String?) {
       guard !isCvvNeeded() else {
           openFlowInsertCvv(password: password, biometry: biometry, privacyConfig: privacyConfig)
           return
       }

       createTransaction(pin: password, biometry: biometry, privacyConfig: privacyConfig)
    }
    
    func createTransaction(pin: String, biometry: Bool, privacyConfig: String?, informedCvv: String? = nil) {
        let cvv = getCvvOnlyPaymentCardBank(informedCvv: informedCvv)
        let request = EcommercePaymentRequest()
        request.orderId = paymentInfo?.orderNumber
        request.sellerId = paymentInfo?.sellerId
        request.creditCardId = String(format: "%ld", CreditCardManager.shared.defaultCreditCardId)
        request.credit = paymentManager.balanceTotal()
        request.total = paymentManager.cardTotalWithoutInterest()
        request.parcelas = String(format: "%ld", paymentManager.selectedQuotaQuantity)
        request.someErrorOccurred = someErrorOccurred
        request.paymentPrivacyConfig = privacyConfig
        request.biometry = biometry
        request.origin = origin.toString()
        
        paymentService.createTransaction(password: pin, cvv: cvv, request: request) { [weak self] response in
            switch response {
            case .success(let transaction):
                let totalValue = request.total?.decimalValue ?? 0
                self?.saveCvv(informedCvv: informedCvv)
                self?.presenter.handleSuccessCreateTransaction(
                    transactionId: transaction.transactionId,
                    totalValue: totalValue,
                    receiptWidgets: transaction.receiptWidgets
                )
                
            case .failure(let error):
                self?.someErrorOccurred = true
                self?.presenter.handleDisableLoading()
                self?.auth?.handleError(error) { [weak self] error in
                    self?.presenter.handleFailureCreateTransaction(error: error)
                }
            }
        }
    }
    
    func navigateToInstallments() {
        let sellerId = paymentInfo?.sellerId ?? ""
        presenter.handleNavigateToInstallments(paymentManager, sellerId: sellerId, installmentsCallback: self)
    }
    
    func close(shouldDismiss: Bool) {
        presenter.handleClose(shouldDismiss)
    }
    
    private func getCvvOnlyPaymentCardBank(informedCvv: String?) -> String? {
        guard let cvv = informedCvv else {
            return createLocalStoreCVVPayload()
        }
        
        return cvv
    }
    
    private func createLocalStoreCVVPayload() -> String? {
        let id = service.defauldCardId
        guard let cardValue = paymentManager.cardTotal()?.doubleValue, !cardValue.isZero else {
            return nil
        }
        
        return service.cvvCard(id: id)
    }
    
    private func openFlowInsertCvv(password: String, biometry: Bool, privacyConfig: String?) {
        presenter.handleInsertCvv(completedCvv: { [weak self] cvv in
            self?.createTransaction(pin: password, biometry: biometry, privacyConfig: privacyConfig, informedCvv: cvv)
        }, completedWithoutCvv: { [weak self] in
            self?.insertCvvError()
        })
    }
    
    private func isCvvNeeded() -> Bool {
        guard let cardValue = paymentManager.cardTotal()?.doubleValue, let cardBank = service.cardBank else {
            return false
        }
        
        return service.pciCvvIsEnable(cardBank: cardBank, cardValue: cardValue)
    }
    
    private func saveCvv(informedCvv: String?) {
        guard let cvv = informedCvv else {
            return
        }
        
        let id = service.defauldCardId
        service.saveCvvCard(id: id, value: cvv)
    }
    
    private func insertCvvError() {
        let error = PicPayError(message: DefaultLocalizable.cvvNotInformed.text)
        presenter.didReceive(error: error)
    }
}

extension EcommercePaymentViewModel: InstallmentsSelectorViewModelDelegate {
    func didSelectedInstallment(_ paymentManager: PPPaymentManager) {
        updatePaymentInfo()
    }
}
