import Core
import FeatureFlag

protocol EcommerceServicing {
    var defauldCardId: String { get }
    var cardBank: CardBank? { get }
    func saveCvvCard(id: String, value: String?)
    func cvvCard(id: String) -> String?
    func pciCvvIsEnable(cardBank: CardBank, cardValue: Double) -> Bool
    func getOrderInfo(_ id: String, _ completion: @escaping ((PicPayResult<EcommercePaymentInfo>) -> Void))
}

final class EcommerceService: BaseApi, EcommerceServicing {
    typealias Dependencies = HasFeatureManager & HasKeychainManager
    private let dependencies: Dependencies

    var defauldCardId: String {
        String(CreditCardManager.shared.defaultCreditCardId)
    }
    
    var cardBank: CardBank? {
        CreditCardManager.shared.defaultCreditCard
    }
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func saveCvvCard(id: String, value: String?) {
        dependencies.keychain.set(key: KeychainKeyPF.cvv(id), value: value)
    }
    
    func cvvCard(id: String) -> String? {
        dependencies.keychain.getData(key: KeychainKeyPF.cvv(id))
    }
    
    func pciCvvIsEnable(cardBank: CardBank, cardValue: Double) -> Bool {
        CvvRegisterFlowCoordinator.cvvFlowEnable(cardBank: cardBank, cardValue: cardValue, paymentType: .ecommerce)
    }
    
    func getOrderInfo(_ id: String, _ completion: @escaping ((PicPayResult<EcommercePaymentInfo>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let url = kWsUrlEcommercePayment.replacingOccurrences(of: "{orderId}", with: id)
            requestManager
                .apiRequest(endpoint: url, method: .get)
                .responseApiObject(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
}
