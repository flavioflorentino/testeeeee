import Core
import FeatureFlag
import Foundation

protocol EcommercePaymentServicing {
    func createTransaction(password: String,
                           cvv: String?,
                           request: EcommercePaymentRequest,
                           _ completion: @escaping ((PicPayResult<CreateTransactionResponse>) -> Void))
}

final class LegacyEcommercePaymentService: BaseApi, EcommercePaymentServicing {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func createTransaction(password: String,
                           cvv: String?,
                           request: EcommercePaymentRequest,
                           _ completion: @escaping ((PicPayResult<CreateTransactionResponse>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            do {
                let url = kWsUrlEcommercePayment.replacingOccurrences(of: "{orderId}", with: request.orderId ?? "")
                let params = try request.toParams()
                requestManager
                    .apiRequest(endpoint: url, method: .post, parameters: params, headers: nil, pin: password)
                    .responseApiObject(completionHandler: completion)
            } catch {
                guard let error = error as? PicPayError else {
                    return
                }
                completion(PicPayResult.failure(error))
            }
            return
        }
        // TODO: - adicionar helper para o core network
    }
}
