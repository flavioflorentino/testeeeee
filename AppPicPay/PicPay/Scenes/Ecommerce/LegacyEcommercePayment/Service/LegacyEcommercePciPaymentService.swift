import Foundation
import PCI

final class LegacyEcommercePciPaymentService: EcommercePaymentServicing {
    private let pciService: EcommercePciServicing
    
    init(pciService: EcommercePciServicing = EcommercePciService()) {
        self.pciService = pciService
    }
    
    func createTransaction(password: String, cvv: String?, request: EcommercePaymentRequest, _ completion: @escaping ((PicPayResult<CreateTransactionResponse>) -> Void)) {
        guard let ecommercePayload = createEcommercePayload(request: request), let orderId = request.orderId else {
            let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
            completion(.failure(error))
            return
        }
        
        let cvv = createCVVPayload(cvv: cvv)
        let payload = PaymentPayload<EcommercePayload>(cvv: cvv, generic: ecommercePayload)
        pciService.createTransaction(
            password: password,
            orderId: orderId,
            payload: payload,
            isNewArchitecture: false
        ) { result in
            switch result {
            case .success(let value):
                let id = value.transactionId
                let receiptWidgets = value.receiptWidgets.compactMap { WSReceipt.createReceiptWidgetItem(jsonDict: $0) }
                let response = CreateTransactionResponse(transactionId: id, receiptWidgets: receiptWidgets)
                completion(.success(response))
                
            case .failure(let error):
                completion(.failure(error.picpayError))
            }
        }
    }
    
    private func createEcommercePayload(request: EcommercePaymentRequest) -> EcommercePayload? {
        guard
            let params = try? request.toParams(),
            let data = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            else {
                return nil
        }
        
        return try? JSONDecoder.decode(data, to: EcommercePayload.self)
    }
    
    private func createCVVPayload(cvv: String?) -> CVVPayload? {
         guard let cvv = cvv else {
             return nil
         }

         return CVVPayload(value: cvv)
     }
}
