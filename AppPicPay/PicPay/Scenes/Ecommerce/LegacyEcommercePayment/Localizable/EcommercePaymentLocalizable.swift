enum EcommercePaymentLocalizable: String, Localizable {
    case loadingTransactionMessage
    case realSymbol
    case orderSymbol
    case orderString
    case installmentsSymbol
    case installmentsString
    
    var key: String {
        self.rawValue
    }
    var file: LocalizableFile {
        .ecommercePayment
    }
}
