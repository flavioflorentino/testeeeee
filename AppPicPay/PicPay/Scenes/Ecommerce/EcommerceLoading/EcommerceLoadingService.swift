import Core
import FeatureFlag
import Foundation

protocol EcommerceLoadingServicing {
    var newEcommerceIsActive: Bool { get }
    func getOrderInfo(_ id: String, _ completion: @escaping (Result<EcommercePaymentInfo, PicPayError>) -> Void)
}

final class EcommerceLoadingService: BaseApi, EcommerceLoadingServicing {
    typealias Dependencies = HasMainQueue & HasFeatureManager
    private let dependencies: Dependencies
    
    var newEcommerceIsActive: Bool {
        FeatureManager.isActive(.newEcommerce)
    }
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func getOrderInfo(_ id: String, _ completion: @escaping (Result<EcommercePaymentInfo, PicPayError>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let endpoint = kWsUrlEcommercePayment.replacingOccurrences(of: "{orderId}", with: id)
            requestManager
                .apiRequest(endpoint: endpoint, method: .get)
                .responseApiObject { [weak self] (result: PicPayResult<EcommercePaymentInfo>) in
                    self?.dependencies.mainQueue.async {
                        let mappedResult = result
                            .map { $0 }
                            .mapError { $0 }
                        
                        completion(mappedResult)
                    }
                }
            return
        }
        // TODO: - adicionar helper para o core network
    }
}
