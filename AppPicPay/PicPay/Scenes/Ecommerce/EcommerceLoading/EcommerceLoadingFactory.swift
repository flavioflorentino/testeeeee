import Foundation

final class EcommerceLoadingFactory: NSObject {
    @objc
    static func make(orderId: String, origin: EcommercePaymentOrigin) -> UIViewController {
        let container = DependencyContainer()
        let service: EcommerceLoadingServicing = EcommerceLoadingService(dependencies: container)
        let coordinator: EcommerceLoadingCoordinating = EcommerceLoadingCoordinator(dependencies: container)
        let presenter: EcommerceLoadingPresenting = EcommerceLoadingPresenter(coordinator: coordinator, dependencies: container)
        let viewModel = EcommerceLoadingViewModel(orderId: orderId, origin: origin, service: service, presenter: presenter)
        let viewController = EcommerceLoadingViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
