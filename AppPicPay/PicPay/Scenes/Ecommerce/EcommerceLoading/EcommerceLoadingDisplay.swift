import UIKit

protocol EcommerceLoadingDisplay: AnyObject {
    func didReceiveAnError(_ error: PicPayError)
    func displayEcommerceAlert(_ alert: EcommerceAlert)
}
