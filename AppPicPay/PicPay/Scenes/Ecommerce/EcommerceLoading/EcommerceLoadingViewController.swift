import UI

final class EcommerceLoadingViewController: ViewController<EcommerceLoadingViewModelInputs, UIView> {
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        
        return activityIndicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad()
    }
 
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale100.color
        activityIndicator.startAnimating()
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: DefaultLocalizable.btClose.text,
            style: .plain,
            target: self,
            action: #selector(didTapClose)
        )
    }
    
    override func buildViewHierarchy() {
        view.addSubview(activityIndicator)
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }
    
    @objc
    private func didTapClose() {
        viewModel.close()
    }
}

// MARK: View Model Outputs
extension EcommerceLoadingViewController: EcommerceLoadingDisplay {
    func didReceiveAnError(_ error: PicPayError) {
        activityIndicator.stopAnimating()
        AlertMessage.showCustomAlertWithError(error, controller: self) { [weak self] in
            self?.viewModel.close()
        }
    }
    
    func displayEcommerceAlert(_ alert: EcommerceAlert) {
        activityIndicator.stopAnimating()
        let popup = EcommerceAlertController(alert: alert)
        
        popup.action = { [weak self] _ in
            self?.closePopup(popup)
        }
        
        showPopup(popup)
    }
    
    private func closePopup(_ popup: EcommerceAlertController) {
        popup.dismiss(animated: true) { [weak self] in
            self?.viewModel.close()
        }
    }
}
