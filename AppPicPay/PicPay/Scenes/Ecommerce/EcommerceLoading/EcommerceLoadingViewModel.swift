import Foundation

protocol EcommerceLoadingViewModelInputs: AnyObject {
    func viewDidLoad()
    func close()
}

final class EcommerceLoadingViewModel {
    private let service: EcommerceLoadingServicing
    private let presenter: EcommerceLoadingPresenting
    private let orderId: String
    private let origin: EcommercePaymentOrigin
    
    init(orderId: String, origin: EcommercePaymentOrigin, service: EcommerceLoadingServicing, presenter: EcommerceLoadingPresenting) {
        self.orderId = orderId
        self.origin = origin
        self.service = service
        self.presenter = presenter
    }
}

extension EcommerceLoadingViewModel: EcommerceLoadingViewModelInputs {
    func viewDidLoad() {
        service.getOrderInfo(orderId) { [weak self] result in
            switch result {
            case .success(let value):
                self?.getOrderInfoSuccess(paymentInfo: value)
            case .failure(let error):
                self?.presenter.didReceiveAnError(error)
            }
        }
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
    
    private func getOrderInfoSuccess(paymentInfo: EcommercePaymentInfo) {
        guard let alert = paymentInfo.alert else {
            openEcommercePayment(paymentInfo: paymentInfo)
            return
        }
        
        presenter.showEcommerceAlert(alert)
    }
    
    private func openEcommercePayment(paymentInfo: EcommercePaymentInfo) {
        guard service.newEcommerceIsActive else {
            presenter.didNextStep(action: .openLegacyEcommercePayment(paymentInfo, origin))
            return
        }
        
        presenter.didNextStep(action: .openNewEcommercePayment(paymentInfo, origin))
    }
}
