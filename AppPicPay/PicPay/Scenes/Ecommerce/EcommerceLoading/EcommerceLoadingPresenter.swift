import Foundation

protocol EcommerceLoadingPresenting: AnyObject {
    var viewController: EcommerceLoadingDisplay? { get set }
    func didReceiveAnError(_ error: PicPayError)
    func showEcommerceAlert(_ alert: EcommerceAlert)
    func didNextStep(action: EcommerceLoadingAction)
}

final class EcommerceLoadingPresenter: EcommerceLoadingPresenting {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let coordinator: EcommerceLoadingCoordinating
    weak var viewController: EcommerceLoadingDisplay?

    init(coordinator: EcommerceLoadingCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    func didReceiveAnError(_ error: PicPayError) {
        viewController?.didReceiveAnError(error)
    }
    
    func showEcommerceAlert(_ alert: EcommerceAlert) {
        viewController?.displayEcommerceAlert(alert)
    }
    
    func didNextStep(action: EcommerceLoadingAction) {
        coordinator.perform(action: action)
    }
}
