import UIKit

enum EcommerceLoadingAction {
    case openLegacyEcommercePayment(EcommercePaymentInfo, EcommercePaymentOrigin)
    case openNewEcommercePayment(EcommercePaymentInfo, EcommercePaymentOrigin)
    case close
}

protocol EcommerceLoadingCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: EcommerceLoadingAction)
}

final class EcommerceLoadingCoordinator: EcommerceLoadingCoordinating {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies
    
    weak var viewController: UIViewController?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func perform(action: EcommerceLoadingAction) {
        switch action {
        case let .openLegacyEcommercePayment(paymentInfo, origin):
            let controller = EcommercePaymentFactory.make(
                origin: origin,
                paymentInfo: paymentInfo
            )
            
            viewController?.navigationController?.viewControllers = [controller]
            
        case let .openNewEcommercePayment(paymentInfo, origin):
            guard let origin = EcommercePaymentOrchestratorModel.Origin(rawValue: origin.toString()) else {
                return
            }
            
            let model = EcommercePaymentOrchestratorModel(origin: origin, isModal: true)
            let orchestrator = EcommercePaymentOrchestrator(paymentInfo: paymentInfo, orchestrator: model)
            
            viewController?.navigationController?.pushViewController(orchestrator.paymentViewController, animated: false)
            
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
