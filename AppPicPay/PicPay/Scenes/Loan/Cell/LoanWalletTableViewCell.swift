import UI
import UIKit

private extension LoanWalletTableViewCell.Layout {
    enum Size {
        static let icon = CGSize(width: 24, height: 24)
    }
}

public final class LoanWalletTableViewCell: UITableViewCell {
    fileprivate enum Layout { }
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view
            .viewStyle(CardViewStyle())
            .with(\.backgroundColor, Palette.ppColorGrayscale000.color)
            .with(\.layer.borderWidth, Border.none)
        return view
    }()

    private lazy var iconLabel: UILabel = {
        let label = UILabel()
        label.text = Iconography.moneyBillStack.rawValue
        label.labelStyle(IconLabelStyle(type: .medium))
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.success600.color)
        label.accessibilityElementsHidden = true
        return label
    }()
           
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.LoanWallet.walletCellTitle
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
             .with(\.textAlignment, .left)
             .with(\.textColor, Colors.grayscale700.color)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.LoanWallet.walletCellDescription
        label.labelStyle(BodySecondaryLabelStyle())
             .with(\.textAlignment, .left)
            .with(\.textColor, Colors.grayscale500.color)
        return label
    }()
    
    func configure() {
        buildLayout()
    }
}

extension LoanWalletTableViewCell: ViewConfiguration {
    public func buildViewHierarchy() {
        contentView.addSubview(containerView)
        containerView.addSubview(iconLabel)
        containerView.addSubview(titleLabel)
        containerView.addSubview(descriptionLabel)
    }
    
    public func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.bottom.equalToSuperview().inset(Spacing.base00)
        }
        
        iconLabel.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.icon)
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().inset(Spacing.base02)
        }
        
        titleLabel.snp.makeConstraints {
            $0.trailing.top.equalToSuperview().inset(Spacing.base02)
            $0.leading.equalTo(iconLabel.snp.trailing).offset(Spacing.base02)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom)
            $0.leading.equalTo(iconLabel.snp.trailing).offset(Spacing.base02)
            $0.trailing.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    public func configureViews() {
        backgroundColor = .clear
        selectionStyle = .none
    }
}
