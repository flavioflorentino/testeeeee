import Core
import LendingHub
import Foundation

protocol LoanWalletBannerServicing {
    func getWalletBanner(completion: @escaping (Result<LoanWalletBanner, ApiError>) -> Void)
}

final class LoanWalletBannerService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension LoanWalletBannerService: LoanWalletBannerServicing {
    func getWalletBanner(completion: @escaping (Result<LoanWalletBanner, ApiError>) -> Void) {
        let decoder = JSONDecoder(.useDefaultKeys) 
        let api = Api<LoanWalletBanner>(endpoint: LoanWalletBannerServiceEndpoint.loanWalletBanner)
        api.execute(jsonDecoder: decoder) { result in
            self.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
