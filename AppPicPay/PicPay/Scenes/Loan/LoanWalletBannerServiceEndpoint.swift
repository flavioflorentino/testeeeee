import Core
import Foundation

enum LoanWalletBannerServiceEndpoint {
    case loanWalletBanner
}

extension LoanWalletBannerServiceEndpoint: ApiEndpointExposable {
    var path: String {
         "personal-credit/offers/wallet-banner"
    }
}
