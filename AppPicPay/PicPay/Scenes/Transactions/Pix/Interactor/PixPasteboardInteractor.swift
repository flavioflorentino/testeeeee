import Core
import PIX
import Validations

struct PixPasteboardInteractor {
    typealias Dependencies = HasKVStore & HasFeatureManager
    
    // MARK: - Properties
    private let dependencies: Dependencies
    
    // MARK: - Initialization
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
}

extension PixPasteboardInteractor: PixPasteboardInteracting {
    func verifyIfHasCopiedKey(showCopiedKeyAlertAction: ((KeyType, String) -> Void)?) {
        guard dependencies.featureManager.isActive(.isPixCopiedKeyTransactionsPopUpAvailable) else { return }
        let personalData: [KeyType] = [.cpf, .cnpj, .email, .phone]
        let validators: [KeyType: StringValidationRule] = {
            var validators: [KeyType: StringValidationRule] = [:]
            personalData.forEach({ validators[$0] = self.validator(with: $0) })
            return validators
        }()
        guard let clipboard = UIPasteboard.general.string, dependencies.kvStore.stringFor(KVKey.lastPix) != clipboard,
              let keyType = keyTypeForKeyValidator(validators: validators, key: clipboard) else {
            return
        }
        dependencies.kvStore.setString(clipboard, with: KVKey.lastPix)
        showCopiedKeyAlertAction?(keyType, clipboard)
    }
}

private extension PixPasteboardInteractor {
    func validator(with keytype: KeyType) -> StringValidationRule {
        switch keytype {
        case .cpf:
            return CPFValidator()
        case .cnpj:
            return CNPJValidator()
        case .phone:
            if dependencies.featureManager.isActive(.featurePixkeyPhoneWithDDI) {
                return CellphoneWithDDDAndDDIValidator()
            } else {
                return CellphoneWithDDDValidator()
            }
        case .email:
            return RegexValidator(descriptor: AccountRegexDescriptor.email)
        case .random:
            return RegexValidator(descriptor: AccountRegexDescriptor.randomKey)
        }
    }

    func keyTypeForKeyValidator(validators: [KeyType: StringValidationRule], key: String) -> KeyType? {
        for keyType in validators.keys.sorted(by: { $0.rawValue > $1.rawValue }) {
            do {
                try validators[keyType]?.validate(key)
            } catch {
                continue
            }
            return keyType
        }
        return nil
    }
}
