import PIX

protocol PixPasteboardInteracting {
    func verifyIfHasCopiedKey(showCopiedKeyAlertAction: ((KeyType, String) -> Void)?)
}
