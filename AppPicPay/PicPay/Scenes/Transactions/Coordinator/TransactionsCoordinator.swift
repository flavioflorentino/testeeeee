import AnalyticsModule
import AssetsKit
import FeatureFlag
import Foundation
import Store
import UI
import UIKit
import PIX
import Billet

final class TransactionsCoordinator {
    typealias Factory = TransactionsFactory & TransactionsFactoring
    typealias Dependencies = HasAnalytics & HasAppManager & HasFeatureManager

    // MARK: - Properties
    private let navigationController: UINavigationController

    private let billetPasteboardInteractor: BilletPasteboardInteracting
    private let pixPasteboardInteractor: PixPasteboardInteracting

    private let factory: Factory
    private let dependencies: Dependencies

    private var children: [Coordinating] = []

    var childViewController: [UIViewController] = []
    var viewController: UIViewController?

    var didFinishFlow: (() -> Void)?

    // MARK: - Initialization
    init(
        navigationController: UINavigationController,
        factory: Factory,
        dependencies: Dependencies,
        billetPasteboardInteractor: BilletPasteboardInteracting,
        pixPasteboardInteractor: PixPasteboardInteracting
    ) {
        self.navigationController = navigationController
        self.billetPasteboardInteractor = billetPasteboardInteractor
        self.factory = factory
        self.dependencies = dependencies
        self.pixPasteboardInteractor = pixPasteboardInteractor
    }

    // MARK: - Type
    func open(deeplink type: TransactionItem.`Type`) {
        switch type {
        case .bills:
            runBillsFlow()
        case .p2m:
            runP2mFlow()
        case .charge:
            runChargeFlow()
        case .store:
            runStoreFlow()
        case .local:
            runLocalsFlow()
        case .payConsumer:
            runPayConsumerFlow()
        case .qrcode:
            runScannerFlow()

        default:
            break
        }
    }
}

// MARK: - TransactionsCoordinating
extension TransactionsCoordinator: TransactionsCoordinating {
    func start() {
        navigationController.navigationBarStyle = .large

        let viewController = factory.makeTransactionsController(
            openItemAction: { [weak self] item in
                self?.open(item: item)
            }, openSearchAction: { [weak self] in
                self?.runSearchFlow()
            }
        )

        if !dependencies.featureManager.isActive(.experimentShowScannerOnTransactionsMenu) {
            viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: makeScannerButton())
        }

        navigationController.setViewControllers([viewController], animated: false)
    }

    func resume() {
        billetPasteboardInteractor.verifyIfHasCopiedBillet { [weak self] code in
            self?.showBilletAlert(for: code)
        }

        pixPasteboardInteractor.verifyIfHasCopiedKey { [weak self] keyType, key in
            self?.showCopiedKeyPixAlert(keyType: keyType, key: key)
        }
    }
}

// MARK: - Private
private extension TransactionsCoordinator {
    func open(item: TransactionItem) {
        switch item.type {
        case .payConsumer:
            runPayConsumerFlow()
        case .deeplink:
            open(deeplink: item.deeplink)
        case .bills:
            runBillsFlow()
        case .p2m:
            runP2mFlow()
        case .charge:
            runChargeFlow()
        case .store:
            runStoreFlow()
        case .local:
            runLocalsFlow()
        case .qrcode:
            runScannerFlow()

        default:
            break
        }
    }

    func open(deeplink: URL?) {
        guard let deeplink = deeplink, UIApplication.shared.canOpenURL(deeplink) else { return }
        UIApplication.shared.open(deeplink)
    }

    // MARK: - Flow
    func runSearchFlow() {
        let searchController = SearchWrapper(ignorePermissionPrompt: true, dependencies: DependencyContainer())
        searchController.setTabs(tabs: [.main, .consumers, .places, .store], isOnlySearchEnabled: true)
        searchController.showContainerSearch = true
        searchController.shouldShowBackButton = true
        searchController.shouldFocusWhenAppear = true
        searchController.hidesBottomBarWhenPushed = true
        searchController.origin = .pay

        navigationController.pushViewController(searchController, animated: true)
    }

    @objc
    func runScannerFlow() {
        dependencies.analytics.log(UserEvent.qrCodeAccessed(type: .click, origin: .payment))
        PaginationCoordinator.shared.showScanner()
    }

    func runBillsFlow() {
        guard FeatureManager.isActive(.isNewBilletHubAvailable) else {
            guard let controller = DGHelpers.controllerForDigitalGoodFlow(for: DGItem(id: "", service: .boleto)) else { return }
            present(viewController: controller)
            return
        }
        
        let hubViewController = BilletHubFactory.make(with: .homePayButton)
        let navigation = UINavigationController(rootViewController: hubViewController)
        present(viewController: navigation)
    }

    func runChargeFlow() {
        guard let controller = DGHelpers.controllerForDigitalGoodFlow(for: DGItem(id: "", service: .charge)) else { return }
        present(viewController: controller)
    }

    func runLocalsFlow() {
        let navigationController = makeModalNavigation(rootViewController: factory.makeLocalsScreen())
        present(viewController: navigationController)
    }

    func runPayConsumerFlow() {
        let navigationController = makeNavigationController()

        let coordinator = factory.makePayPeopleCoordinator(
            title: PayPeopleLocalizable.payPeople.text,
            navigationController: navigationController
        )
        coordinator.didFinishFlow = { [weak self] in
            self?.children.removeAll(where: { $0 === coordinator })
        }

        coordinator.start()
        children.append(coordinator)

        addCloseBarButton(on: navigationController.viewControllers.first)

        present(viewController: navigationController)
    }

    func runP2mFlow() {
        guard let visibleViewController = navigationController.visibleViewController else { return }
        P2MHelpers.presentScanner(from: visibleViewController)
    }

    func runStoreFlow() {
        dependencies.appManager.mainScreenCoordinator?.showStoreScreen()
    }

    // MARK: - Show
    func showBilletAlert(for billetCode: String) {
        let alertController = factory.makeBilletAlert(
            billetCode: billetCode,
            okButtonAction: { [weak self] in
                self?.showBilletFormScreen(for: billetCode)
            },
            closeButtonAction: nil
        )

        navigationController.showPopup(alertController)
    }

    func showBilletFormScreen(for billetCode: String) {
        let billetFormController = factory.makeBilletFormScreen(billetCode: billetCode)
        navigationController.present(PPNavigationController(rootViewController: billetFormController), animated: true)
    }

    // MARK: - Screens
    func makeModalNavigation(rootViewController: UIViewController) -> UINavigationController {
        let navigationController = makeNavigationController(rootViewController: rootViewController)
        addCloseBarButton(on: rootViewController)

        return navigationController
    }

    func makeNavigationController(rootViewController: UIViewController? = nil) -> UINavigationController {
        guard let rootViewController = rootViewController else {
            return IgnoreHiddeNavigationBarController()
        }

        return IgnoreHiddeNavigationBarController(rootViewController: rootViewController)
    }

    func addCloseBarButton(on viewController: UIViewController?) {
        let leftBarButton = UIBarButtonItem(
            title: DefaultLocalizable.btClose.text,
            style: .plain,
            target: self,
            action: #selector(dismissModal)
        )

        viewController?.navigationItem.leftBarButtonItem = leftBarButton
    }

    func makeScannerButton() -> UIButton {
        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(runScannerFlow), for: .touchUpInside)
        button.adjustsImageWhenHighlighted = false
        button.setImage(Assets.NewIconography.Navigationbar.newIconQrcode.image, for: .normal)
        button.tintColor = Colors.branding600.color

        return button
    }

    func present(viewController: UIViewController) {
        guard let presentedViewController = navigationController.topViewController?.presentedViewController else {
            navigationController.topViewController?.present(viewController, animated: true)

            return
        }

        presentedViewController.dismiss(animated: true) { [weak self] in
            self?.present(viewController: viewController)
        }
    }
    
    func showCopiedKeyPixAlert(keyType: KeyType, key: String) {
        let completion = {
            self.openPixKeySelector(keyType: keyType, key: key)
        }
        let subtitle = Strings.Home.copiedKeyPopUpSubtitle + key + Strings.Home.copiedKeyPopUpContinuationOfSubtitle
        navigationController.visibleViewController?.showApolloAlert(
            image: Resources.Icons.icoPix.image,
            title: Strings.Home.copiedKeyPopUpTitle,
            attributedSubtitle: subtitle.attributedStringWith(
                normalFont: Typography.bodyPrimary().font(),
                highlightFont: Typography.bodyPrimary(.highlight).font(),
                normalColor: Colors.grayscale700.color,
                highlightColor: Colors.grayscale700.color,
                textAlignment: .center,
                underline: false
            ),
            primaryButtonAction: .init(title: Strings.Home.copiedKeyPopUpButton, completion: completion),
            linkButtonAction: .init(title: Strings.Home.notNowButton, completion: {}),
            additionalContentView: nil,
            dismissCompletion: nil,
            dismissOnTouchOutside: false
        )
    }
    
    func openPixKeySelector(keyType: KeyType, key: String) {
        let viewController = KeySelectorFactory.make(paymentOpening: PaymentPIXProxy(), selectedKeyType: keyType, key: key)
        let navContoller = UINavigationController(rootViewController: viewController)
        navigationController.present(navContoller, animated: true)
    }

    // MARK: - Actions
    @objc
    func dismissModal() {
        navigationController.dismiss(animated: true)
    }
}

// MARK: - Helpers
private final class IgnoreHiddeNavigationBarController: UINavigationController {
    override func setNavigationBarHidden(_ hidden: Bool, animated: Bool) {
        super.setNavigationBarHidden(false, animated: animated)
    }
}
