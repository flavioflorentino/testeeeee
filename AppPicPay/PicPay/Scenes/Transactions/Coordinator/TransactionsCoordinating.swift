import UI

protocol TransactionsCoordinating: Coordinating {
    var didFinishFlow: (() -> Void)? { get set }

    func resume()
}
