import FeatureFlag

struct TransactionsApplicationEventProperty {
    // MARK: - Type alias
    typealias Dependency = HasFeatureManager

    // MARK: - Properties
    private let dependency: Dependency
    private let externalTitle: String

    // MARK: - Initialization
    init(dependency: Dependency) {
        self.dependency = dependency
        self.externalTitle = dependency.featureManager.text(.experimentNewLabelTransactionLocalTitle)
    }
}

// MARK: - ApplicationEventProperty
extension TransactionsApplicationEventProperty: ApplicationEventProperty {
    var properties: [String: Any] {
        [
            "is_screen_menu_transactions": dependency.featureManager.isActive(.experimentNewTransactionScreenBool),
            "is_locals_in_transaction_new_label": externalTitle.isEmpty ? "control" : externalTitle,
            "is_search_on_transactions_menu_visible": dependency.featureManager.isActive(.experimentShowSearchOnTransactionsMenu),
            "is_new_label_transactions_tabbar": dependency.featureManager.isActive(.experimentNewLabelPayButtonTabBarBool),
            "is_scanner_on_transactions_menu": dependency.featureManager.isActive(.experimentShowScannerOnTransactionsMenu)
        ]
    }
}
