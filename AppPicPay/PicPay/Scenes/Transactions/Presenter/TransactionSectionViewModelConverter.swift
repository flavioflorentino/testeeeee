import FeatureFlag

struct TransactionSectionViewModelConverter {
    typealias Dependencies = HasFeatureManager

    // MARK: - Properties
    private let dependencies: Dependencies

    // MARK: - Initialization
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    func convertToViewModels(sections: [TransactionSection]) -> [TransactionSectionViewModel] {
        sections
            .lazy
            .filter { $0.type != .unknown }
            .map { convertToViewModel(section: $0) }
    }

    func convertToViewModel(section: TransactionSection) -> TransactionSectionViewModel {
        TransactionSectionViewModel(
            title: sectionName(from: section.type),
            items: section.groups.flatMap({ itemViewModels(from: $0) })
        )
    }
}

private extension TransactionSectionViewModelConverter {
    func sectionName(from sectionType: TransactionSection.`Type`) -> String? {
        guard sectionType == .services else { return nil }
        return DefaultLocalizable.services.text
    }

    func itemViewModels(from group: TransactionGroup) -> [TransactionSectionViewModel.TransactionItemViewModel] {
        switch group {
        case let .column(items) where items.contains { canShowItem(of: $0.type) }:
            return items.filter { canShowItem(of: $0.type) }.map { .item(value: $0) }

        case let .carousel(items) where items.contains { canShowItem(of: $0.type) }:
            return [.collection(items: items.filter { canShowItem(of: $0.type) })]

        default:
            return []
        }
    }

    func canShowItem(of type: TransactionItem.`Type`) -> Bool {
        switch type {
        case .unknown:
            return false
        case .qrcode:
            return dependencies.featureManager.isActive(.experimentShowScannerOnTransactionsMenu)
        default:
            return true
        }
    }
}

private extension TransactionSection {
    var groups: [TransactionGroup] {
        switch self {
        case let .main(groups),
             let .services(groups):
            return groups
        default:
            return []
        }
    }
}
