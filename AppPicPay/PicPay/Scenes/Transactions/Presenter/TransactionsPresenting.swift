import Core

protocol TransactionsPresenting {
    func present(transactions: [TransactionSection])
    func open(item: TransactionItem)

    func openSearch()

    func presentLoading()
    func hideLoading()

    func present(error: ApiError)
    func hideError()
}
