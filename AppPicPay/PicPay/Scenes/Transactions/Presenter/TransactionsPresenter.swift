import Core

final class TransactionsPresenter {
    // MARK: - Properties
    private let converter: TransactionSectionViewModelConverter

    // MARK: - Initialization
    init(converter: TransactionSectionViewModelConverter) {
        self.converter = converter
    }

    weak var display: TransactionsDisplay?

    var openItemAction: ((TransactionItem) -> Void)?
    var openSearchAction: (() -> Void)?
}

// MARK: - TransactionsPresenting
extension TransactionsPresenter: TransactionsPresenting {
    func present(transactions: [TransactionSection]) {
        display?.show(sections: converter.convertToViewModels(sections: transactions))
    }

    func open(item: TransactionItem) {
        openItemAction?(item)
    }

    func openSearch() {
        openSearchAction?()
    }

    func presentLoading() {
        display?.showLoading()
    }

    func hideLoading() {
        display?.hideLoading()
    }

    func present(error: ApiError) {
        guard case .connectionFailure = error else {
            display?.showUnknownError()
            return
        }

        display?.showNoConnectionError()
    }

    func hideError() {
        display?.hideError()
    }
}
