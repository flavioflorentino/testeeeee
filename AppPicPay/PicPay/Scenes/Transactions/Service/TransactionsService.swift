import Core
import Foundation

struct TransactionsService {
    typealias Dependencies = HasMainQueue

    // MARK: - Properties
    private let dependencies: Dependencies

    // MARK: - Initialization
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - TransactionsServicing
extension TransactionsService: TransactionsServicing {
    func sections(_ completion: @escaping (Result<[TransactionSection], ApiError>) -> Void) {
        let endpoint = TransactionsServiceEndpoint.sections
        Api<[TransactionSection]>(endpoint: endpoint).execute { result in
            self.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
