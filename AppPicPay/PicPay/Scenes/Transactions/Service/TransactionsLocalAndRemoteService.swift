import Core
import Foundation

struct TransactionsLocalAndRemoteService {
    // MARK: - Properties
    private let localService: TransactionsServicing
    private let remoteService: TransactionsServicing

    // MARK: - Initialization
    init(localService: TransactionsServicing, remoteService: TransactionsServicing) {
        self.localService = localService
        self.remoteService = remoteService
    }
}

// MARK: - TransactionsServicing
extension TransactionsLocalAndRemoteService: TransactionsServicing {
    func sections(_ completion: @escaping (Result<[TransactionSection], ApiError>) -> Void) {
        remoteService.sections { result in
            switch result {
            case let .success(transactions):
                guard transactions.isNotEmpty else {
                    sectionsFromLocalService(completion)
                    return
                }

                completion(.success(transactions))
            case .failure:
                sectionsFromLocalService(completion)
            }
        }
    }
}

// MARK: - Private
private extension TransactionsLocalAndRemoteService {
    func sectionsFromLocalService(_ completion: @escaping (Result<[TransactionSection], ApiError>) -> Void) {
        localService.sections { completion($0) }
    }
}
