import Core
import Dispatch
import Foundation

struct TransactionsLocalService {
    typealias Dependencies = HasMainQueue

    // MARK: - Properties
    private let file: (name: String, type: String) = (name: "transactions", type: "json")

    private let backgroundQueue: DispatchQueue
    private let dependencies: Dependencies

    // MARK: - Initialization
    init(dependencies: Dependencies) {
        self.backgroundQueue = DispatchQueue(label: String(describing: Self.self), qos: .userInitiated)
        self.dependencies = dependencies
    }
}

// MARK: - TransactionsServicing
extension TransactionsLocalService: TransactionsServicing {
    func sections(_ completion: @escaping (Result<[TransactionSection], ApiError>) -> Void) {
        guard let jsonPath = Bundle.main.path(forResource: file.name, ofType: file.type) else {
            return completion(.failure(.bodyNotFound))
        }

        backgroundQueue.async {
            do {
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: jsonPath), options: .mappedIfSafe)
                let result = try JSONDecoder().decode([TransactionSection].self, from: jsonData)

                self.dependencies.mainQueue.async {
                    completion(.success(result))
                }
            } catch {
                self.dependencies.mainQueue.async {
                    completion(.failure(.decodeError(error)))
                }
            }
        }
    }
}
