import Core

protocol TransactionsServicing {
    func sections(_ completion: @escaping (Result<[TransactionSection], ApiError>) -> Void)
}
