import Core
import Foundation

enum TransactionsServiceEndpoint {
    case sections
}

extension TransactionsServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .sections:
            return "transaction-menu/sections"
        }
    }
}
