import UIKit

protocol TransactionsControllerFactory {
    typealias Controller = UIViewController & TransactionsController

    func makeTransactionsController(openItemAction: ((TransactionItem) -> Void)?, openSearchAction: (() -> Void)?) -> Controller
}
