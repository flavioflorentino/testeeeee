import UIKit

protocol TransactionsFactoring {
    func makePayPeopleCoordinator(title: String, navigationController: UINavigationController) -> PayPeopleCoordinating

    func makeStoreScreen() -> UIViewController
    func makeLocalsScreen() -> UIViewController

    func makeBilletAlert(billetCode: String, okButtonAction: (() -> Void)?, closeButtonAction: (() -> Void)?) -> UIViewController
    func makeBilletFormScreen(billetCode: String) -> UIViewController
}
