import Billet
import Store
import UIKit

final class TransactionsFactory {
    private lazy var dependencies = DependencyContainer()
}

// MARK: - TransactionsControllerFactory
extension TransactionsFactory: TransactionsControllerFactory {
    func makeTransactionsController(openItemAction: ((TransactionItem) -> Void)?, openSearchAction: (() -> Void)?) -> Controller {
        let converter = TransactionSectionViewModelConverter(dependencies: dependencies)

        let presenter = TransactionsPresenter(converter: converter)
        presenter.openItemAction = openItemAction
        presenter.openSearchAction = openSearchAction

        let service = TransactionsLocalAndRemoteService(
            localService: TransactionsLocalService(dependencies: dependencies),
            remoteService: TransactionsService(dependencies: dependencies)
        )

        let interactor = TransactionsInteractor(
            service: service,
            presenter: presenter,
            dependencies: dependencies
        )

        let controller = TransactionsViewController(interactor: interactor, dependencies: dependencies)
        presenter.display = controller

        return controller
    }
}

// MARK: - TransactionsFactoring
extension TransactionsFactory: TransactionsFactoring {
    func makePayPeopleCoordinator(title: String, navigationController: UINavigationController) -> PayPeopleCoordinating {
        PayPeopleFactory().makePayPeopleCoordinator(title: title, navigationController: navigationController)
    }

    func makeStoreScreen() -> UIViewController {
        let controller = makeSearchViewController(for: .store)
        controller.origin = .store
        controller.title = DefaultLocalizable.store.text

        return controller
    }

    func makeLocalsScreen() -> UIViewController {
        let controller = makeSearchViewController(for: .places)
        controller.origin = .places
        controller.title = DefaultLocalizable.placesTitle.text

        return controller
    }

    func makeBilletAlert(billetCode: String, okButtonAction: (() -> Void)?, closeButtonAction: (() -> Void)?) -> UIViewController {
        let popup = BoletoClipboardPopup(boletoNumber: billetCode)

        popup.buttonTapAction = {
            popup.dismiss(animated: true) {
                okButtonAction?()
            }
        }

        popup.closeTapAction = {
            popup.dismiss(animated: true) {
                closeButtonAction?()
            }
        }

        return popup
    }

    func makeBilletFormScreen(billetCode: String) -> UIViewController {
        BilletFormFactory.make(with: .clipboard, linecode: billetCode)
    }
}

// MARK: - Private
private extension TransactionsFactory {
    func makeSearchViewController(for tabType: SearchWrapperTabType) -> SearchWrapper {
        let controller = SearchWrapper(ignorePermissionPrompt: false, dependencies: dependencies)
        controller.setTabs(tabs: [tabType], isOnlySearchEnabled: false)
        controller.selectMainTabBar(tab: tabType)
        controller.isPersonRequestingPayment = false
        controller.shouldHideTabsForSingleTab = true
        controller.showsSearchBarButtons = false
        controller.setInitialTabs(tabs: [initialSearchTab(for: tabType)])

        return controller
    }

    func initialSearchTab(for tabType: SearchWrapperTabType) -> SearchTab {
        guard tabType == .places else { return tabType.searchTab }

        let searchTab = tabType.searchTab
        searchTab.shouldAskPermissions = true
        searchTab.autoRefresh = true

        return searchTab
    }
}
