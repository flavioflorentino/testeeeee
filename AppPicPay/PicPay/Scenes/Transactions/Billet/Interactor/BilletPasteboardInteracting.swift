protocol BilletPasteboardInteracting {
    func verifyIfHasCopiedBillet(showCopiedBilletAlertAction: ((String) -> Void)?)
}
