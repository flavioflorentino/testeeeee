import Core

struct BilletPasteboardInteractor {
    // MARK: - Properties
    private let pasteboard: BilletPasteboardWrapping
    private let dependencies: HasKVStore

    // MARK: - Initialization
    init(pasteboard: BilletPasteboardWrapping = BilletPasteboardWrapper(), dependencies: HasKVStore = DependencyContainer()) {
        self.pasteboard = pasteboard
        self.dependencies = dependencies
    }
}

extension BilletPasteboardInteractor: BilletPasteboardInteracting {
    func verifyIfHasCopiedBillet(showCopiedBilletAlertAction: ((String) -> Void)?) {
        pasteboard.copiedBillet { result in
            switch result {
            case let .success(billetCode):
                guard dependencies.kvStore.stringFor(KVKey.lastBoleto) != billetCode else { return }

                dependencies.kvStore.setString(billetCode, with: KVKey.lastBoleto)

                let billetMask = billetCode.boletoMask()
                let formatted = billetMask.maskedText(from: billetCode) ?? ""

                showCopiedBilletAlertAction?(formatted)

            case .failure:
                break
            }
        }
    }
}
