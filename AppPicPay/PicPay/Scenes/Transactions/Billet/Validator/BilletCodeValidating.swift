protocol BilletCodeValidating {
    func validate(code: String?) -> Bool
}
