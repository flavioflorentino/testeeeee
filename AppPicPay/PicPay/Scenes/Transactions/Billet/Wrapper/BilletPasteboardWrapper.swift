import Core
import UIKit

struct BilletPasteboardWrapper {
    typealias Dependencies = HasMainQueue

    // MARK: - Properties
    private let pasteboard: UIPasteboard
    private let validator: BilletCodeValidating
    private let dependencies: Dependencies

    // MARK: - Initialization
    init(
        pasteboard: UIPasteboard = .general,
        validator: BilletCodeValidating = DGBoletoCompleteValidation(),
        dependencies: Dependencies = DependencyContainer()
    ) {
        self.pasteboard = pasteboard
        self.validator = validator
        self.dependencies = dependencies
    }
}

// MARK: - BilletPasteboardWrapping
extension BilletPasteboardWrapper: BilletPasteboardWrapping {
    func copiedBillet(_ completion: @escaping (Result<String, BilletPasteboardError>) -> Void) {
        guard pasteboard.hasStrings else {
            completion(.failure(.notABilletCode))
            return
        }

        pasteboardHasBilletCode { hasCode in
            guard hasCode,
                  let code = pasteboard.string?.onlyNumbers,
                  validator.validate(code: code)
            else {
                return completion(.failure(.notABilletCode))
            }

            completion(.success(code))
        }
    }
}

// MARK: - Private
private extension BilletPasteboardWrapper {
    func pasteboardHasBilletCode(_ completion: @escaping (Bool) -> Void) {
        guard #available(iOS 14.0, *) else {
            let value = pasteboard.string?.onlyNumbers ?? ""
            completion(!value.isEmpty)
            return
        }

        pasteboard.detectPatterns(for: [.number]) { result in
            dependencies.mainQueue.async {
                switch result {
                case let .success(detectedPatterns):
                    completion(detectedPatterns.contains(.number))
                case .failure:
                    completion(false)
                }
            }
        }
    }
}
