enum BilletPasteboardError: Error {
    case notABilletCode
}

protocol BilletPasteboardWrapping {
    func copiedBillet(_ completion: @escaping (Result<String, BilletPasteboardError>) -> Void)
}
