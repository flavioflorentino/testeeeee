protocol TransactionsInteracting {
    func fetchTransactions()
    func didSelect(item: TransactionItem)

    func tapAtSearch()
}
