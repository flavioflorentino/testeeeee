import AnalyticsModule

final class TransactionsInteractor {
    // MARK: - Type alias
    typealias Dependencies = HasAnalytics

    // MARK: - Properties
    private let service: TransactionsServicing
    private let presenter: TransactionsPresenting
    private let dependencies: Dependencies

    // MARK: - Initialization
    init(service: TransactionsServicing, presenter: TransactionsPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - TransactionsInteracting
extension TransactionsInteractor: TransactionsInteracting {
    func fetchTransactions() {
        presenter.hideError()
        presenter.presentLoading()

        service.sections { [weak self] result in
            self?.presenter.hideLoading()

            switch result {
            case let .success(transactions):
                self?.presenter.present(transactions: transactions.filter { $0.type != .unknown })
            case let .failure(error):
                self?.presenter.present(error: error)
            }
        }
    }

    func didSelect(item: TransactionItem) {
        dependencies.analytics.log(
            PaymentEvent.itemAccessed(
                type: "",
                id: nil,
                name: item.trackingData.type,
                section: (name: item.trackingData.section, position: item.trackingData.order)
            )
        )

        presenter.open(item: item)
    }

    func tapAtSearch() {
        presenter.openSearch()
    }
}
