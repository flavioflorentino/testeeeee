import FeatureFlag
import Foundation

protocol TransactionsController: AnyObject {
    typealias Dependencies = HasFeatureManager

    init(interactor: TransactionsInteracting, dependencies: Dependencies)
}
