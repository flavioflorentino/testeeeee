import AssetsKit
import SnapKit
import UI
import UIKit

// MARK: - Layout
private extension TransactionsViewController.Layout {
    enum SearchBar {
        static let height: CGFloat = 40.0
    }
}

final class TransactionsViewController: ViewController<TransactionsInteracting, UIView>, TransactionsController {
    // MARK: - Type alias
    fileprivate typealias SectionItem = TransactionSectionViewModel.TransactionItemViewModel

    // MARK: - Nested types
    fileprivate enum Layout { }

    // MARK: - Properties
    private let dependencies: Dependencies

    private var sections: [TransactionSectionViewModel] = [] {
        didSet {
            sectionsDidChange(oldValue)
        }
    }

    private lazy var searchBar: TransactionSearchView = {
        let searchBar = TransactionSearchView()
        // swiftlint:disable:next overpowered_viewcontroller
        searchBar.placeholder = dependencies.featureManager.text(.searchBarLabel)
        searchBar.didTapAtSearchAction = { [weak self] in
            self?.interactor.tapAtSearch()
        }

        return searchBar
    }()

    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.alwaysBounceVertical = true
        collectionView.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        collectionView.register(cellType: TransactionCarouselCollectionCell.self)
        collectionView.register(cellType: TransactionItemCollectionCell.self)
        collectionView.register(
            supplementaryViewType: TransactionSectionHeaderView.self,
            ofKind: UICollectionView.elementKindSectionHeader
        )
        collectionView.delegate = self

        return collectionView
    }()

    private lazy var collectionViewLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = Spacing.base01
        layout.minimumLineSpacing = Spacing.base01
        layout.sectionInset = UIEdgeInsets(top: Spacing.base02, left: Spacing.base02, bottom: Spacing.base03, right: Spacing.base02)

        return layout
    }()

    private lazy var dataSource: CollectionViewDataSource<Int, SectionItem> = {
        let dataSource = CollectionViewDataSource<Int, SectionItem>(view: collectionView)
        dataSource.itemProvider = { [weak self] _, indexPath, item in
            self?.cell(forIndexPath: indexPath, item: item)
        }
        dataSource.supplementaryViewProvider = { [weak self] _, kind, indexPath in
            self?.supplementaryView(of: kind, indexPath: indexPath)
        }

        return dataSource
    }()

    private weak var loadingView: TransactionsSkeletonView?
    private var currentErrorViewController: UIViewController?

    // MARK: - Initialization
    required init(interactor: TransactionsInteracting, dependencies: Dependencies) {
        self.dependencies = dependencies

        super.init(interactor: interactor)
    }

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        title = Strings.Transactions.transactions

        updateNavigationBarAppearance()

        interactor.fetchTransactions()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        PaginationCoordinator.shared.configurePagination()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        PaginationCoordinator.shared.configurePagination()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }

        // swiftlint:disable:next overpowered_viewcontroller
        if dependencies.featureManager.isActive(.experimentNewLabelPayButtonTabBarBool) {
            navigationController?.tabBarItem.title = DefaultLocalizable.pay.text
        } else {
            navigationController?.tabBarItem.title = nil
        }
    }

    // MARK: - ViewConfiguration
    override func buildViewHierarchy() {
        super.buildViewHierarchy()

        view.addSubview(collectionView)

        addSearchBarIfNeeded()
    }

    override func setupConstraints() {
        super.setupConstraints()

        collectionView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(view.compatibleSafeArea.bottom)
        }
    }

    override func configureViews() {
        super.configureViews()

        collectionView.dataSource = dataSource
        
        guard searchBar.superview != nil else { return }
        searchBar.isHidden.toggle()
    }
}

// MARK: - TransactionsDisplay
extension TransactionsViewController: TransactionsDisplay {
    func show(sections: [TransactionSectionViewModel]) {
        self.sections = sections
    }

    func showLoading() {
        sections = []

        let loadingView = TransactionsSkeletonView()
        collectionView.backgroundView = loadingView

        self.loadingView = loadingView
    }

    func hideLoading() {
        loadingView?.removeFromSuperview()
        collectionView.backgroundView = nil
        
        guard searchBar.superview != nil else { return }
        searchBar.isHidden.toggle()
    }

    func showNoConnectionError() {
        showError(
            image: Resources.Illustrations.iluNoConnection.image,
            title: Strings.Transactions.noInternetConnectionError,
            message: Strings.Transactions.checkYourInternetConnection
        ) { [weak self] in
            self?.interactor.fetchTransactions()
        }
    }

    func showUnknownError() {
        showError(
            image: Resources.Illustrations.iluNoConnection.image,
            title: Strings.Transactions.generalErrorTitle,
            message: Strings.Transactions.generalErrorMessage
        ) { [weak self] in
            self?.interactor.fetchTransactions()
        }
    }

    func hideError() {
        currentErrorViewController?.view.removeFromSuperview()
        currentErrorViewController = nil
        collectionView.backgroundView = nil
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension TransactionsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        referenceSizeForHeaderInSection section: Int
    ) -> CGSize {
        guard sections[section].title != nil else { return .zero }

        return TransactionSectionHeaderView.size(for: collectionView)
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        let item = sections[indexPath.section].items[indexPath.row]

        switch item {
        case .collection:
            return TransactionCarouselCollectionCell.size(for: collectionView)
        case let .item(value):
            return TransactionItemCollectionCell.size(for: value, collectionView: collectionView)
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard case let .item(value) = sections[indexPath.section].items[indexPath.row] else { return }
        didSelect(item: value)
    }
}

// MARK: - Private
private extension TransactionsViewController {
    func addSearchBarIfNeeded() {
        // swiftlint:disable:next overpowered_viewcontroller
        guard dependencies.featureManager.isActive(.experimentShowSearchOnTransactionsMenu) else { return }

        collectionView.addSubview(searchBar)

        searchBar.snp.makeConstraints {
            $0.top.equalToSuperview().offset(-TransactionSearchView.size.height)
            $0.leading.greaterThanOrEqualToSuperview()
            $0.trailing.lessThanOrEqualToSuperview()
            $0.centerX.equalToSuperview()
            $0.width.equalTo(TransactionSearchView.size.width)
        }

        let inset = UIEdgeInsets(
            top: TransactionSearchView.size.height,
            left: .zero,
            bottom: .zero,
            right: .zero
        )
        collectionView.contentInset = inset
        collectionView.scrollIndicatorInsets = inset
    }

    func didSelect(item: TransactionItem) {
        interactor.didSelect(item: item)
    }

    func updateNavigationBarAppearance() {
        navigationController?.navigationBar.tintColor = Colors.branding600.color
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
    }

    func sectionsDidChange(_ oldSections: [TransactionSectionViewModel]) {
        oldSections.enumerated().forEach { index, _ in
            dataSource.remove(section: index)
        }

        sections.enumerated().forEach { index, section in
            dataSource.add(items: section.items, to: index)
        }
    }

    func cell(forIndexPath indexPath: IndexPath, item: SectionItem) -> UICollectionViewCell? {
        switch item {
        case let .item(value):
            let cell = collectionView.dequeueReusableCell(for: indexPath) as TransactionItemCollectionCell
            cell.setup(item: value)

            return cell
        case let .collection(items):
            let cell = collectionView.dequeueReusableCell(for: indexPath) as TransactionCarouselCollectionCell
            cell.didSelect = { [weak self] item in
                self?.didSelect(item: item)
            }
            cell.setup(items: items)

            return cell
        }
    }

    func supplementaryView(of kind: String, indexPath: IndexPath) -> UICollectionReusableView? {
        guard let title = sections[indexPath.section].title, kind == UICollectionView.elementKindSectionHeader else { return nil }

        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, for: indexPath) as TransactionSectionHeaderView
        header.title = title

        return header
    }

    func showError(image: UIImage, title: String, message: String, action: (() -> Void)? = nil) {
        sections = []

        let primaryAction = ErrorAction(title: DefaultLocalizable.tryAgain.text) { _ in action?() }
        let errorInfoModel = ErrorInfoModel(image: image, title: title, message: message, primaryAction: primaryAction)

        let errorViewController = ErrorFactory.make(with: errorInfoModel)

        collectionView.backgroundView = errorViewController.view
        currentErrorViewController = errorViewController
    }
}
