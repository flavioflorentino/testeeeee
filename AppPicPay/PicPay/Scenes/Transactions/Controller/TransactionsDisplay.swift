protocol TransactionsDisplay: AnyObject {
    func show(sections: [TransactionSectionViewModel])

    func showLoading()
    func hideLoading()

    func showNoConnectionError()
    func showUnknownError()
    func hideError()
}
