import SnapKit
import UI
import UIKit

// MARK: - Layout
private extension TransactionItemSmallCollectionCell.Layout {
    static let size = CGSize(width: 104.0, height: 90.0)

    enum Card {
        static let margin: CGFloat = 2.0
    }

    enum Icon {
        static let size = CGSize(width: 24.0, height: 24.0)
    }
}

final class TransactionItemSmallCollectionCell: UICollectionViewCell {
    // MARK: - Nested types
    fileprivate enum Layout { }

    // MARK: - Properties
    private lazy var cardView: UIView = {
        let view = UIView()
        view.viewStyle(CardViewStyle())
            .with(\.backgroundColor, .backgroundPrimary())

        view.addSubviews(iconImageView, titleLabel)

        return view
    }()

    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
            .with(\.tintColor, Colors.branding600.color)
            .with(\.contentMode, .scaleAspectFit)

        return imageView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.backgroundColor, .backgroundPrimary())
            .with(\.contentMode, .center)
            .with(\.lineBreakMode, .byTruncatingTail)
            .with(\.numberOfLines, 2)
            .with(\.textAlignment, .left)
            .with(\.textColor, Colors.grayscale700.color)

        return label
    }()

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        cardView.viewStyle(CardViewStyle())
            .with(\.backgroundColor, .backgroundPrimary())
    }
}

// MARK: - ViewConfiguration
extension TransactionItemSmallCollectionCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(cardView)
    }

    func setupConstraints() {
        cardView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-Layout.Card.margin)
        }

        iconImageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base01)
            $0.size.equalTo(Layout.Icon.size)
        }

        titleLabel.snp.makeConstraints {
            $0.top.equalTo(iconImageView.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base01)
            $0.trailing.lessThanOrEqualToSuperview().offset(-Spacing.base01)
            $0.bottom.equalToSuperview().offset(-Spacing.base01)
        }
    }

    func configureViews() {
        viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        contentView.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
}

// MARK: - Setup
extension TransactionItemSmallCollectionCell {
    func setup(item: TransactionItem) {
        iconImageView.setImage(url: item.icon)
        titleLabel.text = item.title
    }
}

// MARK: - Static methods
extension TransactionItemSmallCollectionCell {
    static var size: CGSize { Layout.size }
}
