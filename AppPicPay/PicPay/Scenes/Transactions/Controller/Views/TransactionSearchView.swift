import SnapKit
import UI
import UIKit

// MARK: - Layout
private extension TransactionSearchView.Layout {
    enum SearchBar {
        static let height: CGFloat = 40.0
        static let padding: CGFloat = Spacing.base02
    }
}

final class TransactionSearchView: UIView {
    // MARK: - Nested types
    fileprivate enum Layout { }

    // MARK: - Properties
    private lazy var searchBar: SearchComponentTextField = {
        let searchBar = SearchComponentTextFieldFactory.make()
        searchBar.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        searchBar.layer.borderWidth = 1.0
        searchBar.layer.borderColor = Colors.grayscale200.color.cgColor
        searchBar.tintColor = Colors.grayscale700.color
        searchBar.delegate = self

        return searchBar
    }()

    var placeholder: String? {
        get { searchBar.attributedPlaceholder?.string }
        set {
            guard let value = newValue else {
                searchBar.attributedPlaceholder = nil
                return
            }

            searchBar.attributedPlaceholder = NSAttributedString(
                string: value,
                attributes: [.foregroundColor: Colors.grayscale700.color]
            )
        }
    }

    var didTapAtSearchAction: (() -> Void)?

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration
extension TransactionSearchView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(searchBar)
    }

    func setupConstraints() {
        searchBar.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Layout.SearchBar.padding)
            $0.height.equalTo(Layout.SearchBar.height)
        }
    }

    func configureViews() {
        viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
}

// MARK: - Static methods
extension TransactionSearchView {
    static var size: CGSize {
        CGSize(width: UIScreen.main.bounds.width, height: Layout.SearchBar.height + (2.0 * Layout.SearchBar.padding))
    }
}

// MARK: - UITextFieldDelegate
extension TransactionSearchView: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        didTapAtSearchAction?()
        return false
    }
}
