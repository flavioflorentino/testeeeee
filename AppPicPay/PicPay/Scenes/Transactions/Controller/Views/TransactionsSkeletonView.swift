import SkeletonView
import UI
import UIKit

// MARK: - Layout
private extension TransactionsSkeletonView.Layout {
    enum SmallCell {
        static let size = CGSize(width: 104.0, height: 88.0)
    }

    enum Cell {
        static let height: CGFloat = 88.0
    }
}

final class TransactionsSkeletonView: UIView {
    fileprivate enum Layout { }

    // MARK: - Properties
    private lazy var containerView: UIView = {
        let view = UIView()
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        view.addSubview(verticalStackView)

        return view
    }()

    private lazy var verticalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = Spacing.base02

        stackView.addArrangedSubview(horizontalStackView)
        cells.forEach { stackView.addArrangedSubviews($0) }

        return stackView
    }()

    private lazy var horizontalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = Spacing.base02
        smallCells.forEach { stackView.addArrangedSubviews($0) }

        return stackView
    }()

    private lazy var smallCells: [UIView] = {
        makeSmallCells()
    }()

    private lazy var cells: [UIView] = {
        [makeCell(), makeCell()]
    }()

    // MARK: - Initialization
    convenience init() {
        self.init(frame: .zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        smallCells.forEach { $0.showAnimatedGradientSkeleton() }
        cells.forEach { $0.showAnimatedGradientSkeleton() }
    }
}

// MARK: - ViewConfiguration
extension TransactionsSkeletonView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(containerView)
    }

    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01).priority(999)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02).priority(999)
            $0.bottom.equalToSuperview().priority(999)
        }

        verticalStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        smallCells.forEach { cell in
            cell.snp.makeConstraints {
                $0.size.equalTo(Layout.SmallCell.size).priority(999)
            }
        }

        cells.forEach { cell in
            cell.snp.makeConstraints {
                $0.height.equalTo(Layout.Cell.height).priority(999)
            }
        }
    }

    func configureViews() {
        viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
}

// MARK: - Private
private extension TransactionsSkeletonView {
    // MARK: - Make cells
    func makeSmallCells() -> [UIView] {
        let screenWidth = UIScreen.main.bounds.width
        let numberOfCells = Int((screenWidth / (Layout.SmallCell.size.width + Spacing.base02)).rounded(.down))

        return (0 ..< numberOfCells).map { _ in makeCell() }
    }

    func makeCell() -> UIView {
        let view = UIView()
        view.layer.cornerRadius = Spacing.base01
        view.clipsToBounds = true
        view.isSkeletonable = true

        return view
    }
}
