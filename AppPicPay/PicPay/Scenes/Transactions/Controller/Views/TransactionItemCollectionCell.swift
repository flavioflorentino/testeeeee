import SnapKit
import UI
import UIKit

// MARK: - Layout
private extension TransactionItemCollectionCell.Layout {
    static let height: CGFloat = 72.0

    enum Icon {
        static let size = CGSize(width: 24.0, height: 24.0)
    }

    enum Image {
        static let height: CGFloat = 76.0
    }
}

final class TransactionItemCollectionCell: UICollectionViewCell {
    // MARK: - Nested types
    fileprivate enum Layout { }

    // MARK: - Properties
    private var item: TransactionItem?

    private lazy var containerView: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle())
            .with(\.backgroundColor, .backgroundPrimary())

        view.addSubviews(stackView)
        view.clipsToBounds = true

        return view
    }()

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.addArrangedSubviews(descriptionView, imageView)
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .fill

        return stackView
    }()

    private lazy var descriptionView: UIView = {
        let view = UIView()
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        view.addSubviews(iconImageView, labelsContainerView)

        return view
    }()

    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
            .with(\.tintColor, Colors.branding600.color)
            .with(\.contentMode, .scaleAspectFit)

        return imageView
    }()

    private lazy var labelsContainerView: UIView = {
        let view = UIView()
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        view.addSubviews(titleLabel, descriptionLabel)

        return view
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.backgroundColor, .backgroundPrimary())
            .with(\.lineBreakMode, .byTruncatingTail)
            .with(\.numberOfLines, 1)
            .with(\.textAlignment, .left)
            .with(\.textColor, Colors.grayscale700.color)

        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.backgroundColor, .backgroundPrimary())
            .with(\.lineBreakMode, .byTruncatingTail)
            .with(\.numberOfLines, 1)
            .with(\.textAlignment, .left)
            .with(\.textColor, Colors.grayscale700.color)

        return label
    }()

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
            .with(\.contentMode, .scaleAspectFill)

        return imageView
    }()
    
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies

    // MARK: - Initialization
    override init(frame: CGRect) {
        self.dependencies = DependencyContainer()
        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        contentView.viewStyle(CardViewStyle())
            .with(\.backgroundColor, .backgroundPrimary())

        setupImage(url: imageURL(from: item))
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        self.item = nil
    }
}

// MARK: - ViewConfiguration
extension TransactionItemCollectionCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(containerView)
    }

    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        stackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        iconImageView.snp.makeConstraints {
            $0.top.greaterThanOrEqualToSuperview().offset(Spacing.base02)
            $0.top.equalTo(labelsContainerView.snp.top)
            $0.leading.equalToSuperview().offset(Spacing.base01)
            $0.bottom.lessThanOrEqualToSuperview().offset(-Spacing.base02)
            $0.size.equalTo(Layout.Icon.size)
        }

        labelsContainerView.snp.makeConstraints {
            $0.top.greaterThanOrEqualToSuperview().offset(Spacing.base02)
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base01)
            $0.bottom.lessThanOrEqualToSuperview().offset(-Spacing.base02)
            $0.centerY.equalToSuperview()
        }

        titleLabel.snp.makeConstraints {
            $0.top.leading.equalToSuperview()
            $0.trailing.lessThanOrEqualToSuperview()
        }

        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom)
            $0.leading.bottom.equalToSuperview()
            $0.trailing.lessThanOrEqualToSuperview()
        }

        imageView.snp.makeConstraints {
            $0.height.equalTo(Layout.Image.height)
        }
    }

    func configureViews() {
        backgroundColor = .clear
        contentView.viewStyle(CardViewStyle())
            .with(\.backgroundColor, .backgroundPrimary())
    }
}

// MARK: - Setup
extension TransactionItemCollectionCell {
    func setup(item: TransactionItem) {
        self.item = item

        iconImageView.setImage(url: item.icon)
        setTitleAndDescription(for: item)

        setupImage(url: imageURL(from: item))
    }
    
    func setTitleAndDescription(for item: TransactionItem) {
        titleLabel.text = item.title
        descriptionLabel.text = item.description
        
        if item.type == .local {
            let externalTitle = dependencies.featureManager.text(.experimentNewLabelTransactionLocalTitle)
            let externalDescription = dependencies.featureManager.text(.experimentNewLabelTransactionLocalDescription)
            
            guard !externalTitle.isEmpty else { return }
            
            titleLabel.text = externalTitle
            descriptionLabel.text = externalDescription
        }
    }
}

// MARK: - Static methods
extension TransactionItemCollectionCell {
    static func size(for item: TransactionItem, collectionView: UICollectionView) -> CGSize {
        let width = collectionView.size.width - (Spacing.base02 * 2)

        guard item.image != nil else {
            return CGSize(width: width, height: Layout.height)
        }

        return CGSize(width: width, height: Layout.height + Layout.Image.height)
    }
}

// MARK: - Private
private extension TransactionItemCollectionCell {
    func imageURL(from item: TransactionItem?) -> URL? {
        guard let item = item else { return nil }

        if #available(iOS 12.0, *) {
            guard traitCollection.userInterfaceStyle == .dark else {
                return item.image
            }

            return item.imageDark ?? item.image
        } else {
            return item.image
        }
    }

    func setupImage(url: URL?) {
        guard let url = url else {
            imageView.isHidden = true
            imageView.image = nil
            return
        }

        imageView.setImage(url: url) { [weak self] image in
            self?.imageView.image = image
            self?.imageView.isHidden = image == nil
        }
    }
}
