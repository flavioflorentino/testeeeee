import SnapKit
import UI
import UIKit

final class TransactionSectionHeaderView: UICollectionReusableView {
    // MARK: - Properties
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium))
            .with(\.backgroundColor, .backgroundPrimary())
            .with(\.lineBreakMode, .byTruncatingTail)
            .with(\.numberOfLines, 1)
            .with(\.textAlignment, .left)
            .with(\.textColor, Colors.grayscale700.color)

        return label
    }()

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration
extension TransactionSectionHeaderView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(titleLabel)
    }

    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.lessThanOrEqualToSuperview().offset(-Spacing.base02)
        }
    }

    func configureViews() {
        backgroundColor = .clear
    }
}

// MARK: - Setup
extension TransactionSectionHeaderView {
    var title: String? {
        get { titleLabel.text }
        set { titleLabel.text = newValue }
    }
}

// MARK: - Static methods
extension TransactionSectionHeaderView {
    static func size(for collectionView: UICollectionView) -> CGSize {
        CGSize(width: collectionView.size.width, height: 24.0)
    }
}
