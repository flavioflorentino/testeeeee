import SnapKit
import UI
import UIKit

// MARK: - Layout
private extension TransactionCarouselCollectionCell.Layout {
    enum CollectionView {
        static let height: CGFloat = TransactionItemSmallCollectionCell.size.height
    }
}

final class TransactionCarouselCollectionCell: UICollectionViewCell {
    // MARK: - Nested types
    fileprivate enum Layout { }

    private enum Section {
        case main
    }

    // MARK: - Properties
    private var items: [TransactionItem] = [] {
        didSet {
            dataSource.update(items: items, from: .main)
        }
    }

    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.register(cellType: TransactionItemSmallCollectionCell.self)
        collectionView.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        collectionView.delegate = self

        return collectionView
    }()

    private lazy var collectionViewLayout: UICollectionViewLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = TransactionItemSmallCollectionCell.size
        layout.minimumInteritemSpacing = Spacing.base01
        layout.minimumLineSpacing = Spacing.base01

        return layout
    }()

    private lazy var dataSource: CollectionViewDataSource<Section, TransactionItem> = {
        let dataSource = CollectionViewDataSource<Section, TransactionItem>(view: collectionView)
        dataSource.add(section: .main)
        dataSource.itemProvider = { [weak self] _, indexPath, item in
            self?.cell(forIndexPath: indexPath, item: item)
        }

        return dataSource
    }()

    var didSelect: ((TransactionItem) -> Void)?

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration
extension TransactionCarouselCollectionCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(collectionView)
    }

    func setupConstraints() {
        collectionView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.bottom.lessThanOrEqualToSuperview()
            $0.height.equalTo(Layout.CollectionView.height)
        }
    }

    func configureViews() {
        viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        contentView.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))

        collectionView.dataSource = dataSource
    }
}

// MARK: - Static methods
extension TransactionCarouselCollectionCell {
    static func size(for collectionView: UICollectionView) -> CGSize {
        let width = collectionView.size.width - (Spacing.base02 * 2)
        return CGSize(width: width, height: Layout.CollectionView.height)
    }
}

// MARK: - Setup
extension TransactionCarouselCollectionCell {
    func setup(items: [TransactionItem]) {
        self.items = items
    }
}

// MARK: - UICollectionViewDelegate
extension TransactionCarouselCollectionCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        didSelect?(item)
    }
}

// MARK: - Private
private extension TransactionCarouselCollectionCell {
    func cell(forIndexPath indexPath: IndexPath, item: TransactionItem) -> UICollectionViewCell? {
        let cell = collectionView.dequeueReusableCell(for: indexPath) as TransactionItemSmallCollectionCell
        cell.setup(item: item)

        return cell
    }
}
