struct TransactionSectionViewModel: Equatable {
    // MARK: - Type alias
    typealias Item = TransactionItemViewModel

    // MARK: - Properties
    let title: String?
    let items: [Item]
}

// MARK: - TransactionItemViewModel
extension TransactionSectionViewModel {
    enum TransactionItemViewModel: Equatable {
        case item(value: TransactionItem)
        case collection(items: [TransactionItem])
    }
}
