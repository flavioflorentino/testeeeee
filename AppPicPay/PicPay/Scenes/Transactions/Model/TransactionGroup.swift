enum TransactionGroup: Equatable {
    case carousel(items: [TransactionItem])
    case column(items: [TransactionItem])
    case unknown
}

private extension TransactionGroup {
    enum `Type`: String, Decodable {
        case carousel
        case column
    }
}

extension TransactionGroup: Decodable {
    private enum CodingKeys: String, CodingKey {
        case type
        case items
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        guard let type = try? container.decode(`Type`.self, forKey: .type) else {
            self = .unknown
            return
        }

        let items = try container.decode([TransactionItem].self, forKey: .items)

        switch type {
        case .carousel:
            self = .carousel(items: items)
        case .column:
            self = .column(items: items)
        }
    }
}
