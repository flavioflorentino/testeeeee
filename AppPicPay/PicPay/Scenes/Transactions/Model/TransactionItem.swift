import Foundation

struct TransactionItem: Equatable {
    let type: `Type`
    let title: String
    let description: String?
    let image: URL?
    let imageDark: URL?
    let icon: URL?
    let deeplink: URL?
    let trackingData: TrackingData
}

extension TransactionItem: Decodable {
    private enum CodingKeys: String, CodingKey {
        case type
        case title
        case description
        case image
        case imageDark
        case icon
        case deeplink
        case tracking
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let type = try container.decode(`Type`.self, forKey: .type)
        let title = try container.decode(String.self, forKey: .title)
        let description = try? container.decodeIfPresent(String.self, forKey: .description)
        let image = try? container.decodeIfPresent(URL.self, forKey: .image)
        let imageDark = try? container.decodeIfPresent(URL.self, forKey: .imageDark)
        let icon = try? container.decodeIfPresent(URL.self, forKey: .icon)
        let deeplink = try? container.decodeIfPresent(URL.self, forKey: .deeplink)
        let trackingData = try container.decode(TrackingData.self, forKey: .tracking)

        self.init(
            type: type,
            title: title,
            description: description,
            image: image,
            imageDark: imageDark,
            icon: icon,
            deeplink: deeplink,
            trackingData: trackingData
        )
    }
}

// MARK: - Type
extension TransactionItem {
    enum `Type`: String, Equatable {
        case payConsumer = "pay_consumer"
        case bills
        case p2m
        case charge
        case deeplink
        case store
        case local
        case qrcode
        case unknown
    }
}

extension TransactionItem.`Type`: Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let type = try container.decode(String.self)

        self = Self(rawValue: type) ?? .unknown
    }
}

// MARK: - TrackingData
extension TransactionItem {
    struct TrackingData: Decodable, Equatable {
        let type: String
        let section: String
        let order: Int
    }
}
