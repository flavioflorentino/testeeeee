import Foundation

enum TransactionSection: Equatable {
    case main(groups: [TransactionGroup])
    case services(groups: [TransactionGroup])
    case unknown
}

extension TransactionSection {
    enum `Type`: String, Decodable {
        case main = "transactions"
        case services
        case unknown
    }

    var type: `Type` {
        switch self {
        case .main:
            return .main
        case .services:
            return .services
        case .unknown:
            return .unknown
        }
    }
}

extension TransactionSection: Decodable {
    private enum CodingKeys: String, CodingKey {
        case section
        case displays
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        guard let type = try? container.decode(`Type`.self, forKey: .section) else {
            self = .unknown
            return
        }

        let groups = try container.decode([TransactionGroup].self, forKey: .displays)

        switch type {
        case .main:
            self = .main(groups: groups)
        case .services:
            self = .services(groups: groups)
        case .unknown:
            self = .unknown
        }
    }
}
