import AnalyticsModule
import Foundation

enum BacenEvent: AnalyticsKeyProtocol {
    case openRestrictedAccount(origin: String)
    case openLimitedAccount(origin: String)
    case openJudicialAccount(status: String, origin: String)
    case helpJudiciallyAccount
    case actionsRestrictedAccount
    
    private var name: String {
        switch self {
        case .openRestrictedAccount:
            return "Restricted account - Details"
        case .openLimitedAccount:
            return "Restricted account - Details"
        case .openJudicialAccount:
            return "Balance locked - info"
        case .helpJudiciallyAccount:
            return "Balance locked - helpcenter"
        case .actionsRestrictedAccount:
            return "Restricted account - actions"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .openRestrictedAccount(let origin):
            return [KeyStrings.origin: origin, KeyStrings.type: "PEP-V0-Restricted"]
        case .openLimitedAccount(let origin):
            return [KeyStrings.origin: origin, KeyStrings.type: "PEP-V0-Limited"]
        case let .openJudicialAccount(status, origin):
            return [KeyStrings.origin: origin, KeyStrings.status: status]
        case .actionsRestrictedAccount:
            return [KeyStrings.action: "Completar Cadastro", KeyStrings.type: "PEP-V0-Limited"]
        default:
            return [:]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.mixPanel, .appsFlyer, .firebase])
    }
}

extension BacenEvent {
    private enum KeyStrings {
        static let origin = "origin"
        static let status = "status"
        static let action = "action"
        static let type = "restriction_type"
    }
}
