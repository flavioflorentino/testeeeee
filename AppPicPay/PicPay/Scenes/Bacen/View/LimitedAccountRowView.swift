import UI
import UIKit

extension LimitedAccountRowView.Layout {
    enum Size {
        static let heightLine: CGFloat = 1.0
    }
    
    enum Font {
        static let light = UIFont.systemFont(ofSize: 14, weight: .light)
        static let semibold = UIFont.systemFont(ofSize: 14, weight: .semibold)
    }
}

final class LimitedAccountRowView: UIView, ViewConfiguration {
    fileprivate enum Layout { }
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = Spacing.base02
        
        return stackView
    }()
    
    private lazy var infoBalanceLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 3
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = Layout.Font.light
        
        return label
    }()
    
    private lazy var balanceLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorNegative400.color
        label.font = Layout.Font.semibold
        
        return label
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale300.color
        
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
    
    func buildViewHierarchy() {
        stackView.addArrangedSubview(infoBalanceLabel)
        stackView.addArrangedSubview(balanceLabel)
        addSubview(stackView)
        addSubview(lineView)
    }
    
    func setupConstraints() {
        infoBalanceLabel.setContentHuggingPriority(.defaultLow, for: .horizontal)
        balanceLabel.setContentHuggingPriority(.required, for: .horizontal)
        
        stackView.layout {
            $0.top == topAnchor + Spacing.base02
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
        }
        
        lineView.layout {
            $0.top == stackView.bottomAnchor + Spacing.base02
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.bottom == bottomAnchor
            $0.height == Layout.Size.heightLine
        }
    }
    
    func setupView(infoBalance: String, balance: String) {
        infoBalanceLabel.text = infoBalance
        balanceLabel.text = balance
    }
}
