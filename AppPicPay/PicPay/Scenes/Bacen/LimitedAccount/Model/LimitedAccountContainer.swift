import Foundation

struct LimitedAccountContainer: Decodable {
    let description: String
    let limits: [LimitedAccount]
}

struct LimitedAccount: Decodable {
    let name: String
    let value: Double
}
