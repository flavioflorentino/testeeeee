import Core
import Foundation

protocol LimitedAccountServicing {
    func getLimitedAccountStatus(completion: @escaping(Result<LimitedAccountContainer, PicPayError>) -> Void)
}

final class LimitedAccountService: LimitedAccountServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func getLimitedAccountStatus(completion: @escaping(Result<LimitedAccountContainer, PicPayError>) -> Void) {
        let api = Api<LimitedAccountContainer>(endpoint: BacenEndpoint.limitedAccount)
        api.execute { [weak self] result in
            let mappedResult = result
                .mapError { $0.picpayError }
                .map { $0.model }
            
            self?.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
}
