import UIKit

protocol LimitedAccountDisplay: AnyObject {
    func startLoad()
    func stopLoad()
    func displayDescription(_ description: String)
    func addLimitedAccountRow(infoBalance: String, balance: String)
    func displayButton()
    func didReceiveAnError(_ error: PicPayError)
}
