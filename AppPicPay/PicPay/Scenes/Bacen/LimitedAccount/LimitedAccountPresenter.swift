import Core
import CoreLegacy
import Foundation

protocol LimitedAccountPresenting: AnyObject {
    var viewController: LimitedAccountDisplay? { get set }
    func loadService()
    func updateView(data: LimitedAccountContainer)
    func didReceiveAnError(_ error: PicPayError)
    func didNextStep(action: LimitedAccountAction)
}

final class LimitedAccountPresenter: LimitedAccountPresenting {
    private let coordinator: LimitedAccountCoordinating
    weak var viewController: LimitedAccountDisplay?

    init(coordinator: LimitedAccountCoordinating) {
        self.coordinator = coordinator
    }
    
    func loadService() {
        viewController?.startLoad()
    }
    
    func updateView(data: LimitedAccountContainer) {
        viewController?.stopLoad()
        viewController?.displayButton()
        viewController?.displayDescription(data.description)
        setupRow(limits: data.limits)
    }
    
    func didReceiveAnError(_ error: PicPayError) {
        viewController?.stopLoad()
        viewController?.didReceiveAnError(error)
    }
    
    func didNextStep(action: LimitedAccountAction) {
        coordinator.perform(action: action)
    }
    
    private func setupRow(limits: [LimitedAccount]) {
        for limit in limits {
            let balance = currencyFormatter(balance: limit.value)
            viewController?.addLimitedAccountRow(infoBalance: limit.name, balance: balance)
        }
    }
    
    private func currencyFormatter(balance: Double) -> String {
        CurrencyFormatter.brazillianRealString(from: balance as NSNumber) ?? ""
    }
}
