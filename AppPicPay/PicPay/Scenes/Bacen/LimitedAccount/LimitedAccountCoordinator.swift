import UIKit

enum LimitedAccountAction {
    case openUpgradeChecklist
    case close
}

protocol LimitedAccountCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: LimitedAccountAction)
}

final class LimitedAccountCoordinator: LimitedAccountCoordinating {
    private var currentCoordinator: Coordinator?
    weak var viewController: UIViewController?
    
    func perform(action: LimitedAccountAction) {
        switch action {
        case .openUpgradeChecklist:
            guard let navigationController = viewController?.navigationController else {
                return
            }
            
            let coordinador = UpgradeCoordinator(navigationController: navigationController)
            coordinador.start()
            currentCoordinator = coordinador
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
