import UI
import UIKit

extension LimitedAccountViewController.Layout {
    enum Font {
        static let description = UIFont.systemFont(ofSize: 16, weight: .light)
    }
    enum Size {
        static let button: CGFloat = 44.0
    }
}

final class LimitedAccountViewController: ViewController<LimitedAccountViewModelInputs, UIView> {
    fileprivate enum Layout { }

    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        
        return view
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.backgroundColor = .clear
        
        return scrollView
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        
        return stackView
    }()
    
    private lazy var registerButton: UIPPButton = {
        let button = UIPPButton()
        button.isHidden = true
        button.cornerRadius = Layout.Size.button / 2
        button.configure(with: Button(title: BacenLocalizable.completeRegister.text))
        button.addTarget(self, action: #selector(didTapRegister), for: .touchUpInside)
        
        return button
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 5
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = Layout.Font.description
        
        return label
    }()
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.color = Palette.ppColorGrayscale400.color
        activityIndicator.hidesWhenStopped = true
        
        return activityIndicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.updateView()
    }
    
    override func configureViews() {
        title = BacenLocalizable.titleLimited.text
        view.backgroundColor = Palette.ppColorGrayscale000.color
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale000.color
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: Assets.Savings.iconRoundCloseGreen.image,
            style: .done,
            target: self,
            action: #selector(didTapClose)
        )
    }
    
    override func buildViewHierarchy() {
        containerView.addSubview(stackView)
        containerView.addSubview(descriptionLabel)
        containerView.addSubview(registerButton)
        
        scrollView.addSubview(containerView)
        
        view.addSubview(scrollView)
        view.addSubview(activityIndicator)
    }
    
    override func setupConstraints() {
        activityIndicator.layout {
            $0.centerX == view.centerXAnchor
            $0.centerY == view.centerYAnchor
        }
        
        scrollView.layout {
            $0.top == view.topAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
            $0.bottom == view.bottomAnchor
        }
        
        containerView.layout {
            $0.top == scrollView.topAnchor
            $0.leading == scrollView.leadingAnchor
            $0.trailing == scrollView.trailingAnchor
            $0.bottom == scrollView.bottomAnchor
            $0.width == view.widthAnchor
        }
        
        stackView.layout {
            $0.top == containerView.topAnchor + Spacing.base02
            $0.leading == containerView.leadingAnchor + Spacing.base02
            $0.trailing == containerView.trailingAnchor - Spacing.base02
        }
        
        descriptionLabel.layout {
            $0.top == stackView.bottomAnchor + Spacing.base04
            $0.leading == containerView.leadingAnchor + Spacing.base02
            $0.trailing == containerView.trailingAnchor - Spacing.base02
        }
        
        registerButton.layout {
            $0.top == descriptionLabel.bottomAnchor + Spacing.base02
            $0.leading == containerView.leadingAnchor + Spacing.base02
            $0.trailing == containerView.trailingAnchor - Spacing.base02
            $0.bottom == containerView.bottomAnchor - Spacing.base02
            $0.height == Layout.Size.button
        }
    }
}

@objc
private extension LimitedAccountViewController {
    func didTapClose() {
        viewModel.didTapClose()
    }
    
    func didTapRegister() {
        viewModel.didTapRegister()
    }
}

// MARK: View Model Outputs
extension LimitedAccountViewController: LimitedAccountDisplay {
    func startLoad() {
        activityIndicator.startAnimating()
    }
    
    func stopLoad() {
        activityIndicator.stopAnimating()
    }
    
    func displayDescription(_ description: String) {
        descriptionLabel.text = description
    }
    
    func addLimitedAccountRow(infoBalance: String, balance: String) {
        let row = LimitedAccountRowView()
        row.setupView(infoBalance: infoBalance, balance: balance)
        
        stackView.addArrangedSubview(row)
    }
    
    func displayButton() {
        registerButton.isHidden = false
    }
    
    func didReceiveAnError(_ error: PicPayError) {
        AlertMessage.showCustomAlertWithError(error, controller: self) { [weak self] in
            self?.viewModel.errorDisplayed()
        }
    }
}
