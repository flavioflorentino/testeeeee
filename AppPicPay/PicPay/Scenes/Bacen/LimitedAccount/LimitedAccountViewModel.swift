import AnalyticsModule
import Foundation

protocol LimitedAccountViewModelInputs: AnyObject {
    func updateView()
    func errorDisplayed()
    func didTapRegister()
    func didTapClose()
}

final class LimitedAccountViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let service: LimitedAccountServicing
    private let presenter: LimitedAccountPresenting
    private let origin: String
    
    init(origin: String, dependencies: Dependencies, service: LimitedAccountServicing, presenter: LimitedAccountPresenting) {
        self.origin = origin
        self.dependencies = dependencies
        self.service = service
        self.presenter = presenter
    }
}

extension LimitedAccountViewModel: LimitedAccountViewModelInputs {
    func updateView() {
        dependencies.analytics.log(BacenEvent.openLimitedAccount(origin: origin))
        
        presenter.loadService()
        service.getLimitedAccountStatus { [weak self] result in
            switch result {
            case .success(let value):
                self?.presenter.updateView(data: value)
            case .failure(let error):
                self?.presenter.didReceiveAnError(error)
            }
        }
    }
    
    func didTapRegister() {
        dependencies.analytics.log(BacenEvent.actionsRestrictedAccount)
        presenter.didNextStep(action: .openUpgradeChecklist)
    }
    
    func didTapClose() {
        presenter.didNextStep(action: .close)
    }
    
    func errorDisplayed() {
        presenter.didNextStep(action: .close)
    }
}
