import Foundation

enum LimitedAccountFactory {
    static func make(origin: String) -> LimitedAccountViewController {
        let container = DependencyContainer()
        let service: LimitedAccountServicing = LimitedAccountService(dependencies: container)
        let coordinator: LimitedAccountCoordinating = LimitedAccountCoordinator()
        let presenter: LimitedAccountPresenting = LimitedAccountPresenter(coordinator: coordinator)
        let viewModel = LimitedAccountViewModel(origin: origin, dependencies: container, service: service, presenter: presenter)
        let viewController = LimitedAccountViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
