import Foundation

struct RestrictedAccountHeader {
    let description: String
    let style: Style
}

extension RestrictedAccountHeader {
    enum Style {
        case header
        case headerSection
    }
}
