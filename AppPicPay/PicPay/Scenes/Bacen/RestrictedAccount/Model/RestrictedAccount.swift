import Foundation

struct RestrictedAccount: Decodable {
    let description: String
    let allowed: RestrictedAccountSection
    let unallowed: RestrictedAccountSection
}

extension RestrictedAccount {
    private enum CodingKeys: String, CodingKey {
        case description
        case allowed = "allowed_actions"
        case unallowed = "unallowed_actions"
    }
}

struct RestrictedAccountSection: Decodable {
    let title: String
    let items: [RestrictedAccountRow]
}

struct RestrictedAccountRow: Decodable {
    let value: String
    let action: String?
}
