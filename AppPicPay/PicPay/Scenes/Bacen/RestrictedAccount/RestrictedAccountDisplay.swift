import UI

protocol RestrictedAccountDisplay: AnyObject {
    func startLoad()
    func stopLoad()
    func displayItensPayment(data: [Section<RestrictedAccountHeader, RestrictedAccountCell>])
    func displayHeader(header: RestrictedAccountHeader)
    func didReceiveAnError(_ error: PicPayError)
}
