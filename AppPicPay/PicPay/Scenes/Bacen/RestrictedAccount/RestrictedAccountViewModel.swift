import AnalyticsModule
import Foundation

protocol RestrictedAccountViewModelInputs: AnyObject {
    func updateView()
    func didTapRow(index: IndexPath)
    func didTapClose()
    func errorDisplayed()
}

final class RestrictedAccountViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let service: RestrictedAccountServicing
    private let presenter: RestrictedAccountPresenting
    private let origin: String
    private var model: RestrictedAccount?
    
    init(
        origin: String,
        dependencies: Dependencies,
        service: RestrictedAccountServicing,
        presenter: RestrictedAccountPresenting
    ) {
        self.origin = origin
        self.dependencies = dependencies
        self.service = service
        self.presenter = presenter
    }
}

extension RestrictedAccountViewModel: RestrictedAccountViewModelInputs {
    func updateView() {
        dependencies.analytics.log(BacenEvent.openRestrictedAccount(origin: origin))
        
        presenter.loadService()
        service.getRestrictedAccountStatus { [weak self] result in
            switch result {
            case .success(let value):
                self?.model = value
                self?.presenter.updateView(data: value)
            case .failure(let error):
                self?.presenter.didReceiveAnError(error)
            }
        }
    }
    
    func didTapRow(index: IndexPath) {
        let row = index.row
        switch index.section {
        case SectionRestricted.allowed.value:
            didTapAllowedAccountSection(row: row)
        case SectionRestricted.unallowed.value:
            didTapUnallowedAccountSection(row: row)
        default:
            break
        }
    }
    
    func didTapClose() {
        presenter.didNextStep(action: .close)
    }
    
    func errorDisplayed() {
        presenter.didNextStep(action: .close)
    }
    
    private func didTapAllowedAccountSection(row: Int) {
        guard let items = model?.allowed.items else {
            return
        }
        
        openDeeplink(row: row, items: items)
    }
    
    private func didTapUnallowedAccountSection(row: Int) {
        guard let items = model?.unallowed.items else {
            return
        }
        
        openDeeplink(row: row, items: items)
    }
    
    private func openDeeplink(row: Int, items: [RestrictedAccountRow]) {
        guard
            items.indices.contains(row),
            let action = items[row].action,
            let url = URL(string: action)
            else {
                return
        }
        
        presenter.didNextStep(action: .openDeeplink(url: url))
    }
}

extension RestrictedAccountViewModel {
    enum SectionRestricted: Int {
        case allowed = 0
        case unallowed = 1
        
        var value: Int {
            self.rawValue
        }
    }
}
