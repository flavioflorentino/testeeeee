import Foundation

enum RestrictedAccountFactory {
    static func make(origin: String) -> RestrictedAccountViewController {
        let container = DependencyContainer()
        let service: RestrictedAccountServicing = RestrictedAccountService(dependencies: container)
        let coordinator: RestrictedAccountCoordinating = RestrictedAccountCoordinator()
        let presenter: RestrictedAccountPresenting = RestrictedAccountPresenter(coordinator: coordinator)
        let viewModel = RestrictedAccountViewModel(
            origin: origin,
            dependencies: container,
            service: service,
            presenter: presenter
        )
        let viewController = RestrictedAccountViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
