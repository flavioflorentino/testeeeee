import UI
import UIKit

extension RestrictedAccountCellView.Layout {
    enum Font {
        static let light = UIFont.systemFont(ofSize: 14, weight: .light)
    }
    
    enum Size {
        static let heightLine: CGFloat = 1.0
    }
}

final class RestrictedAccountCellView: UITableViewCell, ViewConfiguration {
    fileprivate enum Layout { }
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.light
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = 4

        return label
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale300.color
        
        return view
    }()
    
    private lazy var presenter: RestrictedAccountCellPresenterProtocol = {
        let presenter = RestrictedAccountCellPresenter()
        presenter.view = self
        
        return presenter
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
        selectionStyle = .none
    }
    
    func buildViewHierarchy() {
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(lineView)
    }
    
    func setupConstraints() {
        descriptionLabel.layout {
            $0.top == contentView.topAnchor + Spacing.base02
            $0.leading == contentView.leadingAnchor + Spacing.base02
            $0.trailing == contentView.trailingAnchor - Spacing.base02
        }
        
        lineView.layout {
            $0.top == descriptionLabel.bottomAnchor + Spacing.base02
            $0.leading == contentView.leadingAnchor + Spacing.base02
            $0.trailing == contentView.trailingAnchor - Spacing.base02
            $0.bottom == contentView.bottomAnchor
            $0.height == Layout.Size.heightLine
        }
    }
    
    func setupView(model: RestrictedAccountCell) {
        presenter.inputs.setup(model: model)
    }
}

extension RestrictedAccountCellView: RestrictedAccountCellPresenterDisplay {
    func displayDescription(_ attributedText: NSAttributedString) {
        descriptionLabel.attributedText = attributedText
    }
}
