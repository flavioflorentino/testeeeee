import UI

protocol RestrictedAccountCellPresenterInputs {
    func setup(model: RestrictedAccountCell)
}

protocol RestrictedAccountCellPresenterDisplay: AnyObject {
    func displayDescription(_ attributedText: NSAttributedString)
}

protocol RestrictedAccountCellPresenterProtocol: AnyObject {
    var inputs: RestrictedAccountCellPresenterInputs { get }
    var view: RestrictedAccountCellPresenterDisplay? { get set }
}

final class RestrictedAccountCellPresenter: RestrictedAccountCellPresenterProtocol, RestrictedAccountCellPresenterInputs {
    var inputs: RestrictedAccountCellPresenterInputs { self }
    weak var view: RestrictedAccountCellPresenterDisplay?
    
    private var underlineAttributed: [NSAttributedString.Key: Any] = {
        let font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        let color = Palette.ppColorPositive300.color
        
        return [.font: font, .foregroundColor: color, .underlineStyle: NSUnderlineStyle.single.rawValue]
    }()
    
    func setup(model: RestrictedAccountCell) {
        let attributedString = model.description.attributedString(separated: "[a]", attrs: underlineAttributed)
        view?.displayDescription(attributedString)
    }
}
