import UI

final class RestrictedAccountHeaderView: UIView, ViewConfiguration {
    private lazy var headerLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 8
        
        return label
    }()
    
    private lazy var presenter: RestrictedAccountHeaderPresenterProtocol = {
        let presenter = RestrictedAccountHeaderPresenter()
        presenter.view = self
        
        return presenter
    }()
    
    var text: String? {
        didSet {
            headerLabel.text = text
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
    
    func buildViewHierarchy() {
        addSubview(headerLabel)
    }
    
    func setupConstraints() {
        headerLabel.layout {
            $0.top == topAnchor + Spacing.base03
            $0.leading == leadingAnchor + Spacing.base02
            $0.trailing == trailingAnchor - Spacing.base02
            $0.bottom == bottomAnchor
        }
    }
    
    func setupView(model: RestrictedAccountHeader) {
        presenter.inputs.setup(model: model)
    }
}

extension RestrictedAccountHeaderView: RestrictedAccountHeaderPresenterDisplay {
    func displayDescription(_ attributedText: NSAttributedString) {
        headerLabel.attributedText = attributedText
    }
    
    func styleDescription(font: UIFont, color: UIColor) {
        headerLabel.font = font
        headerLabel.textColor = color
    }
}
