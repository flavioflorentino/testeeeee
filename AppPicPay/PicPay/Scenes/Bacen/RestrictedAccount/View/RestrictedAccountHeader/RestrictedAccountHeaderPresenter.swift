import UI

protocol RestrictedAccountHeaderPresenterInputs {
    func setup(model: RestrictedAccountHeader)
}

protocol RestrictedAccountHeaderPresenterDisplay: AnyObject {
    func displayDescription(_ attributedText: NSAttributedString)
    func styleDescription(font: UIFont, color: UIColor)
}

protocol RestrictedAccountHeaderPresenterProtocol: AnyObject {
    var inputs: RestrictedAccountHeaderPresenterInputs { get }
    var view: RestrictedAccountHeaderPresenterDisplay? { get set }
}

final class RestrictedAccountHeaderPresenter: RestrictedAccountHeaderPresenterProtocol, RestrictedAccountHeaderPresenterInputs {
    var inputs: RestrictedAccountHeaderPresenterInputs { self }
    weak var view: RestrictedAccountHeaderPresenterDisplay?
    
    private var alertAttributed: [NSAttributedString.Key: Any] = {
        let font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        let color = Palette.ppColorNegative400.color
        
        return [.font: font, .foregroundColor: color]
    }()
    
    func setup(model: RestrictedAccountHeader) {
        setupDescription(style: model.style)
        setupAttributed(description: model.description)
    }
    
    private func setupDescription(style: RestrictedAccountHeader.Style) {
        let color = Palette.ppColorGrayscale500.color
        let font: UIFont
        switch style {
        case .header:
            font = UIFont.systemFont(ofSize: 16, weight: .light)
        case .headerSection:
            font = UIFont.systemFont(ofSize: 14, weight: .bold)
        }
        
        view?.styleDescription(font: font, color: color)
    }
    
    private func setupAttributed(description: String) {
        let attributedString = description.attributedString(separated: "[r]", attrs: alertAttributed)
        view?.displayDescription(attributedString)
    }
}
