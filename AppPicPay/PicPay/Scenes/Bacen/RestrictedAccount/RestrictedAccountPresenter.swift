import Core
import UI

protocol RestrictedAccountPresenting: AnyObject {
    var viewController: RestrictedAccountDisplay? { get set }
    func loadService()
    func updateView(data: RestrictedAccount)
    func didReceiveAnError(_ error: PicPayError)
    func didNextStep(action: RestrictedAccountAction)
}

final class RestrictedAccountPresenter: RestrictedAccountPresenting {
    private let coordinator: RestrictedAccountCoordinating
    weak var viewController: RestrictedAccountDisplay?
    
    init(coordinator: RestrictedAccountCoordinating) {
        self.coordinator = coordinator
    }
    
    func loadService() {
        viewController?.startLoad()
    }
    
    func updateView(data: RestrictedAccount) {
        viewController?.stopLoad()
        setupSection(data: data)
        setupHeader(data.description)
    }
    
    func didReceiveAnError(_ error: PicPayError) {
        viewController?.stopLoad()
        viewController?.didReceiveAnError(error)
    }
    
    func didNextStep(action: RestrictedAccountAction) {
        coordinator.perform(action: action)
    }
    
    private func setupSection(data: RestrictedAccount) {
        let allowedSection = createSection(section: data.allowed)
        let unallowedSection = createSection(section: data.unallowed)
        
        viewController?.displayItensPayment(data: [allowedSection, unallowedSection])
    }
    
    private func setupHeader(_ description: String) {
        let header = RestrictedAccountHeader(description: description, style: .header)
        viewController?.displayHeader(header: header)
    }
    
    private func createSection(section: RestrictedAccountSection) -> Section<RestrictedAccountHeader, RestrictedAccountCell> {
        let title = RestrictedAccountHeader(description: section.title, style: .headerSection)
        let items = section.items.map { RestrictedAccountCell(description: $0.value) }
        
        return Section(header: title, items: items)
    }
}
