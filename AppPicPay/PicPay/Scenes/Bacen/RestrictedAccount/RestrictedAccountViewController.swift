import UI
import UIKit

final class RestrictedAccountViewController: ViewController<RestrictedAccountViewModelInputs, UIView> {
    private enum Layout {}

    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.estimatedRowHeight = 80
        tableView.estimatedSectionHeaderHeight = 80
        tableView.sectionFooterHeight = 0
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(
            RestrictedAccountCellView.self,
            forCellReuseIdentifier: String(describing: RestrictedAccountCellView.self)
        )
        return tableView
    }()
    
    private lazy var headerView = UIView()
    private lazy var restrictedHeader = RestrictedAccountHeaderView()
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.color = Palette.ppColorGrayscale400.color
        activityIndicator.hidesWhenStopped = true
        
        return activityIndicator
    }()
    
    private var dataSource: TableViewHandler<RestrictedAccountHeader, RestrictedAccountCell, RestrictedAccountCellView>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.updateView()
    }
 
    override func configureViews() {
        title = BacenLocalizable.titleRestricted.text
        view.backgroundColor = Palette.ppColorGrayscale000.color
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale000.color
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: Assets.Savings.iconRoundCloseGreen.image,
            style: .done,
            target: self,
            action: #selector(didTapClose)
        )
    }
    
    override func buildViewHierarchy() {
        headerView.addSubview(restrictedHeader)
        view.addSubview(tableView)
        view.addSubview(activityIndicator)
        tableView.tableHeaderView = headerView
    }
    
    override func setupConstraints() {
        tableView.layout {
            $0.top == view.topAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
            $0.bottom == view.bottomAnchor
        }
        
        activityIndicator.layout {
            $0.centerX == view.centerXAnchor
            $0.centerY == view.centerYAnchor
        }
        
        headerView.layout {
            $0.top == tableView.topAnchor
            $0.leading == tableView.leadingAnchor
            $0.trailing == tableView.trailingAnchor
            $0.centerX == tableView.centerXAnchor
        }
        
        restrictedHeader.layout {
            $0.top == headerView.topAnchor
            $0.leading == headerView.leadingAnchor
            $0.trailing == headerView.trailingAnchor
            $0.bottom == headerView.bottomAnchor
            $0.centerX == tableView.centerXAnchor
        }
    }
    
    @objc
    private func didTapClose() {
        viewModel.didTapClose()
    }
}

// MARK: View Model Outputs
extension RestrictedAccountViewController: RestrictedAccountDisplay {
    func startLoad() {
        activityIndicator.startAnimating()
    }
    
    func stopLoad() {
        activityIndicator.stopAnimating()
    }
    
    func displayItensPayment(data: [Section<RestrictedAccountHeader, RestrictedAccountCell>]) {        
        dataSource = TableViewHandler(
            data: data,
            cellType: RestrictedAccountCellView.self,
            configureCell: { _, item, cellView in
                cellView.setupView(model: item)
            },
            configureSection: { _, section -> Header? in
                guard let model = section.header else {
                    return nil
                }
                
                let header = RestrictedAccountHeaderView()
                header.setupView(model: model)
                
                return .view(header)
            },
            configureDidSelectRow: { [weak self] index, _ in
                self?.viewModel.didTapRow(index: index)
            })
        
        tableView.delegate = dataSource
        tableView.dataSource = dataSource
        tableView.reloadData()
    }
    
    func displayHeader(header: RestrictedAccountHeader) {
        restrictedHeader.setupView(model: header)
        tableView.tableHeaderView?.layoutIfNeeded()
    }
    
    func didReceiveAnError(_ error: PicPayError) {
        AlertMessage.showCustomAlertWithError(error, controller: self) { [weak self] in
            self?.viewModel.errorDisplayed()
        }
    }
}
