import UIKit

enum RestrictedAccountAction: Equatable {
    case openDeeplink(url: URL)
    case close
}

protocol RestrictedAccountCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RestrictedAccountAction)
}

final class RestrictedAccountCoordinator: RestrictedAccountCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: RestrictedAccountAction) {
        switch action {
        case .openDeeplink(let url):
            DeeplinkHelper.handleDeeplink(withUrl: url, from: viewController)
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
