import Core

protocol RestrictedAccountServicing {
    func getRestrictedAccountStatus(completion: @escaping(Result<RestrictedAccount, PicPayError>) -> Void)
}

final class RestrictedAccountService: RestrictedAccountServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func getRestrictedAccountStatus(completion: @escaping(Result<RestrictedAccount, PicPayError>) -> Void) {
        let api = Api<RestrictedAccount>(endpoint: BacenEndpoint.restrictedAccount)
        api.execute { result in
            let mappedResult = result
                .mapError { $0.picpayError }
                .map { $0.model }
            
            self.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
}
