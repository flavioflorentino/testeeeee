import Core

enum BacenEndpoint {
    case judicialBlock(id: String)
    case judicialBlockWithoutId
    case judicialProcessNumber(id: String, password: String)
    case judicialProcessNumberWithoutId(password: String)
    case limitedAccount
    case restrictedAccount
}

extension BacenEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .judicialBlockWithoutId, .judicialBlock:
            return "payment_account/block/ui_detail"
        case .limitedAccount:
            return "payment_account/pep/limited_ui"
        case .judicialProcessNumber, .judicialProcessNumberWithoutId:
            return "payment_account/block/process_number"
        case .restrictedAccount:
            return "payment_account/pep/restriction_ui"
        }
    }
    
    var parameters: [String: Any] {
        switch self {
        case .judicialBlock(let id),
             .judicialProcessNumber(let id, _):
            return [Parameters.id: id]
        default:
            return [:]
        }
    }
    
    var customHeaders: [String: String] {
        switch self {
        case .judicialProcessNumber(_, let password),
             .judicialProcessNumberWithoutId(let password):
            return ["Password": "\(password)"]
        default:
            return [:]
        }
    }
}

extension BacenEndpoint {
    private enum Parameters {
        static let id = "id"
    }
}
