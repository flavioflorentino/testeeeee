import Foundation

enum BacenLocalizable: String, Localizable {
    case judiciallyBlocked
    case blockedReason
    case knowMore
    case blockedBalance
    case balanceTransferred
    case process
    case status
    case copyCode
    case showNumber
    
    case completeRegister
    case titleLimited
    
    case titleRestricted
    
    var key: String {
        self.rawValue
    }
    
    var file: LocalizableFile {
        .bacen
    }
}
