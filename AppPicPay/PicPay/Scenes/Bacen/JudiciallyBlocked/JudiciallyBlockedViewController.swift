import AMPopTip
import UI
import UIKit

extension JudiciallyBlockedViewController.Layout {
    enum Font {
        static let titleBold = UIFont.systemFont(ofSize: 28, weight: .bold)
        static let light16 = UIFont.systemFont(ofSize: 16, weight: .light)
        static let semibold14 = UIFont.systemFont(ofSize: 14, weight: .semibold)
        static let light14 = UIFont.systemFont(ofSize: 14, weight: .light)
    }
}

final class JudiciallyBlockedViewController: ViewController<JudiciallyBlockedViewModelInputs, UIView>, LoadingViewProtocol {
    fileprivate enum Layout { }
    lazy var loadingView = LoadingView()
    
    private var auth: PPAuth?
    
    private lazy var popTip: PopTip = {
        let popTip = PopTip()
        popTip.bubbleColor = Palette.ppColorNeutral400.color
        popTip.padding = 10
        popTip.offset = 10
        
        return popTip
    }()
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.color = Palette.ppColorGrayscale400.color
        activityIndicator.hidesWhenStopped = true
        
        return activityIndicator
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        
        return view
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.backgroundColor = .clear
        
        return scrollView
    }()
    
    private lazy var balanceStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        
        return stackView
    }()
    
    private lazy var blockedDescriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale600.color
        label.numberOfLines = 0
        label.font = Layout.Font.light16
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var blockedReasonLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale600.color
        label.numberOfLines = 2
        label.font = Layout.Font.semibold14
        
        return label
    }()
    
    private lazy var statusLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale600.color
        label.numberOfLines = 2
        label.font = Layout.Font.light14
        
        return label
    }()
    
    private lazy var processLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale600.color
        label.numberOfLines = 2
        label.font = Layout.Font.light14
        
        return label
    }()
    
    private lazy var infoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.isUserInteractionEnabled = true
        stackView.spacing = Spacing.base00
        
        return stackView
    }()
    
    private lazy var knowMoreLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale600.color
        label.font = Layout.Font.light14
        label.isUserInteractionEnabled = true
        
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadBlockedStatus()
    }
 
    override func configureViews() {
        title = BacenLocalizable.judiciallyBlocked.text
        view.backgroundColor = Palette.ppColorGrayscale000.color
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale000.color
        setupGestureKnowMoreLabel()
        setupGestureInfoStackView()
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: Assets.Savings.iconRoundCloseGreen.image,
            style: .done,
            target: self,
            action: #selector(didTapClose)
        )
    }
    
    override func buildViewHierarchy() {
        infoStackView.addArrangedSubview(processLabel)
        infoStackView.addArrangedSubview(statusLabel)
        
        containerView.addSubview(balanceStackView)
        containerView.addSubview(blockedDescriptionLabel)
        containerView.addSubview(blockedReasonLabel)
        containerView.addSubview(infoStackView)
        containerView.addSubview(knowMoreLabel)
        
        scrollView.addSubview(containerView)
        
        view.addSubview(scrollView)
    }
    
    override func setupConstraints() {
        scrollView.layout {
            $0.top == view.topAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
            $0.bottom == view.bottomAnchor
        }
        
        containerView.layout {
            $0.top == scrollView.topAnchor
            $0.leading == scrollView.leadingAnchor
            $0.trailing == scrollView.trailingAnchor
            $0.bottom == scrollView.bottomAnchor
            $0.width == view.widthAnchor
        }
        
        balanceStackView.layout {
            $0.top == containerView.topAnchor + Spacing.base03
            $0.leading == containerView.leadingAnchor + Spacing.base02
            $0.trailing == containerView.trailingAnchor - Spacing.base02
        }
        
        blockedDescriptionLabel.layout {
            $0.top == balanceStackView.bottomAnchor + Spacing.base03
            $0.leading == balanceStackView.leadingAnchor
            $0.trailing == balanceStackView.trailingAnchor
        }
        
        blockedReasonLabel.layout {
            $0.top == blockedDescriptionLabel.bottomAnchor + Spacing.base03
            $0.leading == balanceStackView.leadingAnchor
            $0.trailing == balanceStackView.trailingAnchor
        }
        
        infoStackView.layout {
            $0.top == blockedReasonLabel.bottomAnchor + Spacing.base03
            $0.leading == balanceStackView.leadingAnchor
            $0.trailing == balanceStackView.trailingAnchor
        }
        
        knowMoreLabel.layout {
            $0.top == infoStackView.bottomAnchor + Spacing.base03
            $0.leading == balanceStackView.leadingAnchor
            $0.trailing == balanceStackView.trailingAnchor
            $0.bottom == containerView.bottomAnchor - Spacing.base03
        }
    }
    
    private func setupGestureKnowMoreLabel() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapKnowMore))
        knowMoreLabel.addGestureRecognizer(gesture)
    }
    
    private func setupGestureInfoStackView() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapInfo))
        infoStackView.addGestureRecognizer(gesture)
    }
}

@objc
private extension JudiciallyBlockedViewController {
    func didTapClose() {
        viewModel.didDismiss()
    }
    
    func didTapKnowMore() {
        viewModel.didKnowMore()
    }
    
    func didTapInfo() {
        viewModel.didTapInfo()
    }
}

// MARK: View Model Outputs
extension JudiciallyBlockedViewController: JudiciallyBlockedDisplay {
    func startLoading() {
        startLoadingView()
    }
    
    func stopLoading() {
        stopLoadingView()
    }
    
    func displayPopupPassword() {
        auth = PPAuth.authenticate({ [weak self] password, _ in
            self?.viewModel.requestProcessNumber(password: password)
        }, canceledByUserBlock: nil)
    }
    
    func addRowBalance(infoBalance: String, balance: String) {
        let row = LimitedAccountRowView()
        row.setupView(infoBalance: infoBalance, balance: balance)
        
        balanceStackView.addArrangedSubview(row)
    }
    
    func displayBlockedDescription(_ attributedText: NSAttributedString) {
        blockedDescriptionLabel.attributedText = attributedText
    }
    
    func displayBlockedReason(_ reason: String) {
        blockedReasonLabel.text = reason
    }
    
    func displayStatus(_ status: String) {
        statusLabel.text = status
    }
    
    func displayProcess(_ attributedText: NSAttributedString) {
        processLabel.attributedText = attributedText
    }
        
    func displayPopup(title: String) {
        popTip.show(text: title, direction: .down, maxWidth: 200, in: view, from: infoStackView.frame, duration: 1.5)
    }
    
    func displayKnowMore(_ attributedText: NSAttributedString) {
        knowMoreLabel.attributedText = attributedText
    }
    
    func didReceiveAnError(error: PicPayError) {
        AlertMessage.showCustomAlertWithError(error, controller: self) { [weak self] in
            self?.viewModel.didDismiss()
        }
    }
}
