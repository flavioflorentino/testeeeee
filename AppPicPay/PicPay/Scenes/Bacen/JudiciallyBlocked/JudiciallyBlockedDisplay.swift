import UIKit

protocol JudiciallyBlockedDisplay: AnyObject {
    func startLoading()
    func stopLoading()
    func displayPopupPassword()
    func displayStatus(_ status: String)
    func displayProcess(_ attributedText: NSAttributedString)
    func displayPopup(title: String)
    func addRowBalance(infoBalance: String, balance: String)
    func displayBlockedDescription(_ attributedText: NSAttributedString)
    func displayBlockedReason(_ reason: String)
    func displayKnowMore(_ attributedText: NSAttributedString)
    func didReceiveAnError(error: PicPayError)
}
