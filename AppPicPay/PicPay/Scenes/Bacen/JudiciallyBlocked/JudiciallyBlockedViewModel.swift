import AnalyticsModule
import Foundation

protocol JudiciallyBlockedViewModelInputs: AnyObject {
    func loadBlockedStatus()
    func requestProcessNumber(password: String?)
    func didDismiss()
    func didKnowMore()
    func didTapInfo()
}

final class JudiciallyBlockedViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let service: JudiciallyBlockedServicing
    private let presenter: JudiciallyBlockedPresenting
    private var model: BlockedStatus?
    private var processNumber: String?
    private let id: String?
    private let origin: JudicialBlockOrigin
    
    init(
        id: String?,
        dependencies: Dependencies,
        origin: JudicialBlockOrigin,
        service: JudiciallyBlockedServicing,
        presenter: JudiciallyBlockedPresenting
    ) {
        self.id = id
        self.dependencies = dependencies
        self.origin = origin
        self.service = service
        self.presenter = presenter
    }
}

extension JudiciallyBlockedViewModel: JudiciallyBlockedViewModelInputs {
    func loadBlockedStatus() {
        presenter.presenterLoad()
        let endpoint = buildEndpointBlockedStatus()
        service.getBlockedStatus(endpoint: endpoint) { [weak self] result in
            switch result {
            case .success(let value):
                self?.loadBlockedStatusSuccess(model: value)
                
            case .failure(let error):
                self?.presenter.didReceiveAnError(error: error)
            }
        }
    }
    
    func didDismiss() {
        presenter.didNextStep(action: .close)
    }
    
    func didKnowMore() {
        dependencies.analytics.log(BacenEvent.helpJudiciallyAccount)
        
        let link = service.linkFaq
        guard let url = URL(string: link) else {
            return
        }
        
        presenter.didNextStep(action: .openKnowMore(url))
    }
    
    func requestProcessNumber(password: String?) {
        guard let password = password else {
            return
        }
        
        presenter.presenterLoad()
        let endpoint = buildEndpointProcessNumber(password: password)
        
        service.getProcessNumber(endpoint: endpoint) { [weak self] result in
            switch result {
            case .success(let value):
                self?.processNumber = value
                self?.presenter.updateProcessNumber(value)
                
            case .failure(let error):
                self?.presenter.didReceiveAnError(error: error)
            }
        }
    }
    
    func didTapInfo() {
        guard let processNumber = processNumber else {
            presenter.presenterPopupPassword()
            return
        }
        
        presenter.copyProcessNumber(processNumber)
    }
    
    private func loadBlockedStatusSuccess(model: BlockedStatus) {
        self.model = model
        presenter.updateView(model: model)
        dependencies.analytics.log(BacenEvent.openJudicialAccount(status: model.status, origin: origin.rawValue))
    }
    
    private func buildEndpointBlockedStatus() -> BacenEndpoint {
        guard let id = id else {
            return BacenEndpoint.judicialBlockWithoutId
        }
        
        return BacenEndpoint.judicialBlock(id: id)
    }
    
    private func buildEndpointProcessNumber(password: String) -> BacenEndpoint {
        guard let id = id else {
            return BacenEndpoint.judicialProcessNumberWithoutId(password: password)
        }
        
        return BacenEndpoint.judicialProcessNumber(id: id, password: password)
    }
}
