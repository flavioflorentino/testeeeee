import Core
import FeatureFlag
import Foundation

protocol JudiciallyBlockedServicing {
    var linkFaq: String { get }
    func getBlockedStatus(endpoint: BacenEndpoint, completion: @escaping(Result<BlockedStatus, PicPayError>) -> Void)
    func getProcessNumber(endpoint: BacenEndpoint, completion: @escaping(Result<String, PicPayError>) -> Void)
}

final class JudiciallyBlockedService: JudiciallyBlockedServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    var linkFaq: String {
        FeatureManager.text(.faqJudicialBlockade)
    }
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func getBlockedStatus(endpoint: BacenEndpoint, completion: @escaping(Result<BlockedStatus, PicPayError>) -> Void) {
        let api = Api<BlockedStatus>(endpoint: endpoint)
        api.execute { [weak self] result in
            let mappedResult = result
                .mapError { $0.picpayError }
                .map { $0.model }
            
            self?.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
    
    func getProcessNumber(endpoint: BacenEndpoint, completion: @escaping(Result<String, PicPayError>) -> Void) {
        let api = Api<ProcessNumber>(endpoint: endpoint)
        api.execute { [weak self] result in
            let mappedResult = result
                .mapError { $0.picpayError }
                .map { $0.model.processNumber }
            
            self?.dependencies.mainQueue.async {
                completion(mappedResult)
            }
        }
    }
}
