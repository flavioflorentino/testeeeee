import Core
import CoreLegacy
import Foundation
import UI

protocol JudiciallyBlockedPresenting: AnyObject {
    var viewController: JudiciallyBlockedDisplay? { get set }
    func presenterLoad()
    func copyProcessNumber(_ number: String)
    func updateView(model: BlockedStatus)
    func presenterPopupPassword()
    func updateProcessNumber(_ process: String)
    func didReceiveAnError(error: PicPayError)
    func didNextStep(action: JudiciallyBlockedAction)
}

final class JudiciallyBlockedPresenter: JudiciallyBlockedPresenting {
    private let coordinator: JudiciallyBlockedCoordinating
    
    private var underlineAttributed: [NSAttributedString.Key: Any] = {
        let font = Typography.linkSecondary().font()
        let color = Palette.ppColorNeutral300.color
        
        return [.font: font, .foregroundColor: color, .underlineStyle: NSUnderlineStyle.single.rawValue]
    }()
    
    private var underlineAttributedGreen: [NSAttributedString.Key: Any] = {
        let font = Typography.linkSecondary().font()
        let color = Colors.branding300.color
        
        return [.font: font, .foregroundColor: color, .underlineStyle: NSUnderlineStyle.single.rawValue]
    }()
    
    private var boldAttributed: [NSAttributedString.Key: Any] = {
        let font = Typography.bodyPrimary(.highlight).font()
        
        return [.font: font]
    }()
    
    weak var viewController: JudiciallyBlockedDisplay?
    
    init(coordinator: JudiciallyBlockedCoordinating) {
        self.coordinator = coordinator
    }
    
    func presenterLoad() {
        viewController?.startLoading()
    }
    
    func updateView(model: BlockedStatus) {
        viewController?.stopLoading()
        
        setupBalanceView(model: model)
        setupBlockedDescription(model: model)
        setupBlockedReason()
        setupStatus(status: model.status)
        setupProcess(BacenLocalizable.showNumber.text)
        setupKnowMore()
    }
    
    func updateProcessNumber(_ process: String) {
        viewController?.stopLoading()
        setupProcess(process)
    }
    
    func didReceiveAnError(error: PicPayError) {
        viewController?.stopLoading()
        viewController?.didReceiveAnError(error: error)
    }
    
    func presenterPopupPassword() {
        viewController?.displayPopupPassword()
    }
    
    func copyProcessNumber(_ number: String) {
        UIPasteboard.general.string = number
        viewController?.displayPopup(title: BacenLocalizable.copyCode.text)
    }
    
    func didNextStep(action: JudiciallyBlockedAction) {
        coordinator.perform(action: action)
    }
    
    private func setupStatus(status: String) {
        let text = "\(BacenLocalizable.status.text) \(status)"
        viewController?.displayStatus(text)
    }
    
    private func setupProcess(_ process: String) {
        let text = "\(BacenLocalizable.process.text) \(process)"
        let attributed = text.attributedString(separated: "[u]", attrs: underlineAttributedGreen)
        viewController?.displayProcess(attributed)
    }
    
    private func setupBalanceView(model: BlockedStatus) {
        if let blockedBalance = model.blockedValue {
            addRowBalance(infoBalance: BacenLocalizable.blockedBalance.text, value: blockedBalance)
        }
        
        if let transferredValue = model.transferredValue {
            addRowBalance(infoBalance: BacenLocalizable.balanceTransferred.text, value: transferredValue)
        }
    }
    
    private func addRowBalance(infoBalance: String, value: Double) {
        let balance = CurrencyFormatter.brazillianRealString(from: value as NSNumber) ?? ""
        viewController?.addRowBalance(infoBalance: infoBalance, balance: balance)
    }
    
    private func setupBlockedDescription(model: BlockedStatus) {
        let attributed = model.description.attributedString(separated: "[b]", attrs: boldAttributed)
        viewController?.displayBlockedDescription(attributed)
    }
    
    private func setupBlockedReason() {
        viewController?.displayBlockedReason(BacenLocalizable.blockedReason.text)
    }
    
    private func setupKnowMore() {
        let text = BacenLocalizable.knowMore.text
        let attributed = text.attributedString(separated: "[u]", attrs: underlineAttributed)
        viewController?.displayKnowMore(attributed)
    }
}
