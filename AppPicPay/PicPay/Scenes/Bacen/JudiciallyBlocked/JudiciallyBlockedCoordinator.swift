import UIKit

enum JudiciallyBlockedAction: Equatable {
    case close
    case openKnowMore(URL)
}

protocol JudiciallyBlockedCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: JudiciallyBlockedAction)
}

final class JudiciallyBlockedCoordinator: JudiciallyBlockedCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: JudiciallyBlockedAction) {
        switch action {
        case .close:
            viewController?.dismiss(animated: true)
            
        case .openKnowMore(let url):
            guard let viewController = viewController else {
                return
            }
            
            DeeplinkHelper.handleDeeplink(withUrl: url, from: viewController)
        }
    }
}
