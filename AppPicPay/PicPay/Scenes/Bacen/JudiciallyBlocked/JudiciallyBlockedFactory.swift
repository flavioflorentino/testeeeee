import Foundation

enum JudicialBlockOrigin: String, Decodable {
    case feed = "FEED"
    case wallet = "WALLET"
    case notification = "NOTIFICATION"
    case popup = "POPUP"
}

enum JudiciallyBlockedFactory {
    static func make(id: String?, origin: JudicialBlockOrigin) -> JudiciallyBlockedViewController {
        let dependencies = DependencyContainer()
        let service: JudiciallyBlockedServicing = JudiciallyBlockedService(dependencies: dependencies)
        let coordinator: JudiciallyBlockedCoordinating = JudiciallyBlockedCoordinator()
        let presenter: JudiciallyBlockedPresenting = JudiciallyBlockedPresenter(coordinator: coordinator)
        let viewModel = JudiciallyBlockedViewModel(
            id: id,
            dependencies: dependencies,
            origin: origin,
            service: service,
            presenter: presenter
        )
        let viewController = JudiciallyBlockedViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
