import Foundation

struct BlockedStatus: Decodable {
    let description: String
    let blockedValue: Double?
    let transferredValue: Double?
    let status: String
}

struct ProcessNumber: Decodable {
    let processNumber: String
}

private extension ProcessNumber {
    private enum CodingKeys: String, CodingKey {
        case processNumber = "process_number"
    }
}

private extension BlockedStatus {
    private enum CodingKeys: String, CodingKey {
        case description
        case blockedValue = "blocked_value"
        case transferredValue = "transferred_value"
        case status
    }
}
