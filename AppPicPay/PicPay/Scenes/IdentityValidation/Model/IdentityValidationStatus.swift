import SwiftyJSON

final class IVIdentityValidationStatus: BaseApiResponse {
    enum Status: String {
        case notVerified = "NOT_VERIFIED"
        case notCreated = "NOT_CREATED"
        case verified = "VERIFIED"
        case toVerify = "TO_VERIFY"
        case rejected = "REJECTED"
        case undefined = "UNDEFINED"
        case pending = "PENDING"
        case disabled = "DISABLED"
    }
    
    var status: Status = .undefined
    var label: String = ""
    
    required init(json: JSON) {
        if let status = Status(rawValue: json["status"].string ?? "") {
            self.status = status
        }
        
        if let label = json["status_label"].string {
            self.label = label
        }
    }
}
