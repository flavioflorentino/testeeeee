import SwiftyJSON

final class IdentityValidationStepIcons {
    var darkMode: String = ""
    var lightMode: String = ""
    
    init(darkMode: String, lightMode: String) {
        self.darkMode = darkMode
        self.lightMode = lightMode
    }
    
    required init?(json: JSON) {
        if let darkMode = json["darkMode"].string {
            self.darkMode = darkMode
        }

        if let dayMode = json["lightMode"].string {
            self.lightMode = dayMode
        }
    }
}
