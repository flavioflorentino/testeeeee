enum IdentityValidationOriginFlow {
    case settings
    case upgradeChecklist
    case paymentChallenge
    case deeplink
    case notification
}
