import SwiftyJSON

final class IdentityValidationStep: BaseApiResponse, Equatable {
    enum StepType: String {
        case initial = "INITIAL"
        case questions = "QUESTIONS"
        case selfie = "SELFIE"
        case documentBack = "BACK_DOCUMENT"
        case documentFront = "FRONT_DOCUMENT"
        case processing = "PROCESSING"
        case done = "DONE"
    }
    
    var title: String = ""
    var text: String = ""
    var type: StepType = .initial
    var data: Any?
    var status: IVIdentityValidationStatus.Status?
    var icons: IdentityValidationStepIcons?
    var isLastStep: Bool {
        type == .done || type == .processing
    }
    
    init(
        type: StepType = .initial,
        title: String = "",
        text: String = "",
        data: Any? = nil,
        status: IVIdentityValidationStatus.Status? = nil,
        icons: IdentityValidationStepIcons? = nil
    ) {
        self.type = type
        self.title = title
        self.text = text
        self.data = data
        self.status = status
        self.icons = icons
    }
    
    required init?(json: JSON) {
        guard let type = StepType(rawValue: json["type"].string ?? "") else {
            return
        }
        
        self.type = type
        if type == .questions {
            if let questionsData = json["data"].array {
                var questions: [IdentityValidationQuestion] = []
                for qData in questionsData {
                    if let question = IdentityValidationQuestion(json: qData) {
                        questions.append(question)
                    }
                }
                self.data = questions
            }
        }
        
        if let title = json["label"].string {
            self.title = title
        }
        
        if let text = json["detail"].string {
            self.text = text
        }
        
        if let status = json["status"].string {
            self.status = IVIdentityValidationStatus.Status(rawValue: status)
        }
        
        self.icons = IdentityValidationStepIcons(json: json["icons"])
    }
    
    static func == (lhs: IdentityValidationStep, rhs: IdentityValidationStep) -> Bool {
        lhs.type == rhs.type && lhs.title == rhs.title && lhs.text == rhs.text
    }
}
