import SwiftyJSON

final class IdentityValidationQuestion: BaseApiResponse {
    var options: [String] = []
    var answer: String = ""
    var text: String = ""
    
    init(text: String, answer: String, options: [String]) {
        self.text = text
        self.answer = answer
        self.options = options
    }
    
    required init?(json: JSON) {
        if let text = json["question"].string {
            self.text = text
        }
        
        if let optionsJson = json["options"].array {
            var options: [String] = []
            for o in optionsJson {
                if let option = o.string {
                    options.append(option)
                }
                self.options = options
            }
        }
    }
}
