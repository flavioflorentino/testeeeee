import Foundation

enum FacialBiometricLocalizable: String, Localizable {
    // FacialBiometryIntro
    case selfieTitleText
    case selfieDescriptionText
    case takeSelfie
    case takeSelfieLater
    
    // FacialBiometryCamera
    case selfieTakePictureTip
    case shotButtonAccessibilityText
    case closepictureCaptureAccessibilityText
    
    // FacialBiometryPreview
    case previewInfoText
    case previewTakeAnother
    case previewSendPicture
    case previewUploadText
    
    // FacialBiometryTips
    case cameraPermissionConfigurationAlertButton
    case cameraPermissionAlertTitle
    case cameraPermissionAlertMessage

    // DocumentCaptureIntro
    case documentIntroTitle
    case documentIntroDescription

    // DocumentValidationCamera
    case documentFrontCameraTitle
    case documentFrontCameraHighlight
    case documentBackCameraTitle
    case documentBackCameraHighlight

    // DocumentCapturePreview
    case documentSendPicture
    case documentCaptureFrontTitle
    case documentCaptureBackTitle
    case previewUploadError

    var key: String {
        rawValue
    }
    var file: LocalizableFile {
        .facialBiometric
    }
}
