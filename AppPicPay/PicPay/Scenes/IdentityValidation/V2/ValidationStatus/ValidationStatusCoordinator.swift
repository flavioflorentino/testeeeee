import UIKit

enum ValidationStatusAction {
    case ok
}

protocol ValidationStatusCoordinating: AnyObject {
    func perform(action: ValidationStatusAction)
}

final class ValidationStatusCoordinator: ValidationStatusCoordinating {
    private weak var coordinatorDelegate: IdentityValidationCoordinating?

    init(coordinatorDelegate: IdentityValidationCoordinating?) {
        self.coordinatorDelegate = coordinatorDelegate
    }
    
    func perform(action: ValidationStatusAction) {
        if case .ok = action {
            coordinatorDelegate?.dismiss()
        }
    }
}
