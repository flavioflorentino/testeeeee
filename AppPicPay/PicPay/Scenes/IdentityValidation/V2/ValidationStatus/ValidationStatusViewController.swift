import UI
import UIKit

extension ValidationStatusViewController.Layout {
    enum Size {
        static let buttonHeight: CGFloat = 48
        static let image: CGFloat = 100
    }
}

final class ValidationStatusViewController: ViewController<ValidationStatusViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var confirmButton: ConfirmButton = {
        let button = ConfirmButton()
        button.setTitle(DefaultLocalizable.btOkUnderstood.text, for: .normal)
        button.addTarget(self, action: #selector(didTapOk), for: .touchUpInside)
        return button
    }()
    
    private lazy var centerStackView: UIStackView = {
        let stack = UIStackView()
        stack.alignment = .center
        stack.axis = .vertical
        stack.distribution = .fill
        stack.spacing = Spacing.base06
        return stack
    }()
    
    private lazy var textStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Spacing.base04
        return stack
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.sendAnalytics()
        configureViewsForTraits()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(centerStackView)
        centerStackView.addArrangedSubview(iconImageView)
        centerStackView.addArrangedSubview(textStackView)
        textStackView.addArrangedSubview(titleLabel)
        textStackView.addArrangedSubview(descriptionLabel)
        centerStackView.addArrangedSubview(confirmButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color.withCustomDarkColor(Colors.backgroundSecondary.darkColor)
    }
    
    override func setupConstraints() {
        centerStackView.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base06)
            $0.trailing.equalToSuperview().offset(-Spacing.base06)
            $0.centerY.equalToSuperview()
        }
        
        textStackView.snp.makeConstraints {
            $0.width.equalToSuperview()
        }
        
        confirmButton.snp.makeConstraints {
            $0.leading.equalToSuperview()
            $0.height.equalTo(Layout.Size.buttonHeight)
        }

        iconImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.image)
        }
    }
    
    override func configureStyles() {
        titleLabel
            .labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, .black())
            .with(\.textAlignment, .center)
        
        descriptionLabel
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        guard
            #available(iOS 13.0, *),
            traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection)
            else {
                return
        }
        configureViewsForTraits()
    }
}

// MARK: Private Actions
@objc
private extension ValidationStatusViewController {
    func didTapOk() {
        viewModel.handleOkAction()
    }
    
    func configureViewsForTraits() {
        if #available(iOS 13.0, *) {
            viewModel.configureViews(isDarkMode: view.traitCollection.userInterfaceStyle == .dark)
        } else {
            viewModel.configureViews(isDarkMode: false)
        }
    }
}

// MARK: View Model Outputs
extension ValidationStatusViewController: ValidationStatusDisplay {
    func displayStatusWith(imageUrl: URL?, title: String, description: String) {
        iconImageView.setImage(url: imageUrl) { [weak self] imageResult in
            self?.iconImageView.image = imageResult ?? Assets.IdentityValidation.iluIdentityValidationProc.image
        }
        titleLabel.text = title
        descriptionLabel.text = description
    }
}
