import Foundation

enum ValidationStatusFactory {
    static func make(
        coordinatorDelegate: IdentityValidationCoordinating?,
        step: IdentityValidationStep
    ) -> ValidationStatusViewController {
        let container = DependencyContainer()
        let coordinator: ValidationStatusCoordinating = ValidationStatusCoordinator(coordinatorDelegate: coordinatorDelegate)
        let presenter: ValidationStatusPresenting = ValidationStatusPresenter(coordinator: coordinator)
        let viewModel = ValidationStatusViewModel(presenter: presenter, step: step, dependencies: container)
        let viewController = ValidationStatusViewController(viewModel: viewModel)

        presenter.viewController = viewController

        return viewController
    }
}
