import UIKit

protocol ValidationStatusDisplay: AnyObject {
    func displayStatusWith(imageUrl: URL?, title: String, description: String)
}
