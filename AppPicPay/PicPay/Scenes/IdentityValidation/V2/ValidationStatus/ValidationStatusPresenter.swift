import Core
import Foundation

protocol ValidationStatusPresenting: AnyObject {
    var viewController: ValidationStatusDisplay? { get set }
    func configureViews(for step: IdentityValidationStep, isDarkMode: Bool)
    func handleOkAction()
}

final class ValidationStatusPresenter: ValidationStatusPresenting {
    private let coordinator: ValidationStatusCoordinating
    weak var viewController: ValidationStatusDisplay?
    
    init(coordinator: ValidationStatusCoordinating) {
        self.coordinator = coordinator
    }
    
    func configureViews(for step: IdentityValidationStep, isDarkMode: Bool) {
        let imageUrlString = isDarkMode ? step.icons?.darkMode : step.icons?.lightMode
        let imageUrl: URL? = URL(string: imageUrlString ?? "")
        viewController?.displayStatusWith(imageUrl: imageUrl, title: step.title, description: step.text)
    }
    
    func handleOkAction() {
        coordinator.perform(action: .ok)
    }
}
