import AnalyticsModule
import Foundation

protocol ValidationStatusViewModelInputs: AnyObject {
    func configureViews(isDarkMode: Bool)
    func sendAnalytics()
    func handleOkAction()
}

final class ValidationStatusViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let presenter: ValidationStatusPresenting
    private let step: IdentityValidationStep
    
    init(
        presenter: ValidationStatusPresenting,
        step: IdentityValidationStep,
        dependencies: Dependencies
    ) {
        self.presenter = presenter
        self.step = step
        self.dependencies = dependencies
    }
}

extension ValidationStatusViewModel: ValidationStatusViewModelInputs {
    func sendAnalytics() {
        sendAnalyticsEvent(step: step)
    }
    
    func configureViews(isDarkMode: Bool) {
        presenter.configureViews(for: step, isDarkMode: isDarkMode)
    }
    
    func handleOkAction() {
        presenter.handleOkAction()
    }
    
    private func sendAnalyticsEvent(step: IdentityValidationStep) {
        guard let status = step.status else {
            return
        }
        
        switch status {
        case .verified:
            dependencies.analytics.log(IdentityValidationEvent.validationEnd(status: .success))
        case .rejected:
            dependencies.analytics.log(IdentityValidationEvent.validationEnd(status: .reproved))
        default:
            dependencies.analytics.log(IdentityValidationEvent.validationEnd(status: .analysis))
        }
    }
}
