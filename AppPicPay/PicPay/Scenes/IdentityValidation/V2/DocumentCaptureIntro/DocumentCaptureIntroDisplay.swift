import Foundation

protocol DocumentCaptureIntroDisplayable: AnyObject {
    func presentStepInfo(title: String, description: String)
}
