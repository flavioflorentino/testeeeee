import AnalyticsModule
import Foundation

protocol DocumentCaptureIntroViewModelInputs: AnyObject {
    func loadTexts()
    func handleContinue()
    func handleClose()
}

final class DocumentCaptureIntroViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let step: IdentityValidationStep
    private let presenter: DocumentCaptureIntroPresenting

    init(step: IdentityValidationStep, presenter: DocumentCaptureIntroPresenting, dependencies: Dependencies) {
        self.step = step
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

extension DocumentCaptureIntroViewModel: DocumentCaptureIntroViewModelInputs {
    func loadTexts() {
        presenter.handleStepInfo()
        dependencies.analytics.log(IdentityValidationEvent.document(action: .none))
    }

    func handleContinue() {
        presenter.handleContinue(step)
        dependencies.analytics.log(IdentityValidationEvent.document(action: .started))
    }

    func handleClose() {
        presenter.handleClose()
        dependencies.analytics.log(IdentityValidationEvent.document(action: .drop))
    }
}
