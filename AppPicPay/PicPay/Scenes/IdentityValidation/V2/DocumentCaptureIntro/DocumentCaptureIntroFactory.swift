import Foundation

enum DocumentCaptureIntroFactory {
    static func make(
        coordinatorDelegate: IdentityValidationCoordinating?,
        step: IdentityValidationStep,
        navigationController: UINavigationController
    ) -> DocumentCaptureIntroViewController {
        let container = DependencyContainer()
        let coordinator: DocumentCaptureIntroCoordinating = DocumentCaptureIntroCoordinator(
            coordinatorDelegate: coordinatorDelegate,
            navigationController: navigationController
        )
        let presenter: DocumentCaptureIntroPresenting = DocumentCaptureIntroPresenter(coordinator: coordinator)
        let viewModel = DocumentCaptureIntroViewModel(step: step, presenter: presenter, dependencies: container)
        let viewController = DocumentCaptureIntroViewController(viewModel: viewModel)

        presenter.viewController = viewController

        return viewController
    }
}
