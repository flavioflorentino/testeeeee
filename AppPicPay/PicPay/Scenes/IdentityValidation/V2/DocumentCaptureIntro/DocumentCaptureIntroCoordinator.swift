import UIKit

enum DocumentCaptureIntroAction {
    case nextStep(step: IdentityValidationStep)
    case close
}

protocol DocumentCaptureIntroCoordinating: AnyObject {
    func perform(action: DocumentCaptureIntroAction)
}

final class DocumentCaptureIntroCoordinator: DocumentCaptureIntroCoordinating {
    private weak var coordinatorDelegate: IdentityValidationCoordinating?
    private let navigationController: UINavigationController

    init(coordinatorDelegate: IdentityValidationCoordinating?, navigationController: UINavigationController) {
        self.coordinatorDelegate = coordinatorDelegate
        self.navigationController = navigationController
    }

    func perform(action: DocumentCaptureIntroAction) {
        switch action {
        case let .nextStep(step):
            let tipsController = DocumentCaptureTipsFactory.make(
                coordinatorDelegate: coordinatorDelegate,
                step: step,
                navigationController: navigationController
            )
            navigationController.pushViewController(tipsController, animated: true)
        case .close:
            coordinatorDelegate?.dismiss()
        }
    }
}
