import UI
import UIKit

extension DocumentCaptureIntroViewController.Layout {
    enum Size {
        static let imageWidth: CGFloat = 120
        static let imageHeight: CGFloat = 96
        static let buttonHeight: CGFloat = 48
    }
}

final class DocumentCaptureIntroViewController: ViewController<DocumentCaptureIntroViewModelInputs, UIView> {
    fileprivate enum Layout { }

    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = Assets.IdentityValidation.documentIntro.image
        return imageView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()

    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()

    private lazy var continueButton: UIPPButton = {
        let button = UIPPButton(type: .system)
        button.setTitle(IdentityValidationLocalizable.takePicture.text, for: .normal)
        button.normalBackgrounColor = Colors.branding400.color
        button.highlightedBackgrounColor = Colors.branding400.color.withAlphaComponent(0.6)
        button.normalTitleColor = Colors.white.lightColor
        button.highlightedTitleColor = Colors.white.lightColor
        button.cornerRadius = Spacing.base03
        button.titleLabel?.font = Typography.bodyPrimary(.highlight).font()
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(didTapContinue), for: .touchUpInside)
        return button
    }()

    private lazy var laterButton: UnderlinedButton = {
        let button = UnderlinedButton()
        button.setTitle(IdentityValidationLocalizable.doItLater.text, for: .normal)
        button.updateColor(
            Colors.branding400.color,
            highlightedColor: Colors.branding400.color.withAlphaComponent(0.6),
            font: Typography.bodyPrimary(.highlight).font()
        )
        button.addTarget(self, action: #selector(didTapLater), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadTexts()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(iconImageView)
        view.addSubview(titleLabel)
        view.addSubview(subtitleLabel)
        view.addSubview(continueButton)
        view.addSubview(laterButton)
    }

    override func setupConstraints() {
        iconImageView.snp.makeConstraints {
            $0.bottom.equalTo(titleLabel.snp.top).offset(-Spacing.base04)
            $0.centerX.equalToSuperview()
            $0.width.equalTo(Layout.Size.imageWidth)
            $0.height.equalTo(Layout.Size.imageHeight)
        }

        titleLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base04)
            $0.trailing.equalToSuperview().offset(-Spacing.base04)
            $0.bottom.equalTo(subtitleLabel.snp.top).offset(-Spacing.base02)
        }

        subtitleLabel.snp.makeConstraints {
            $0.leading.equalTo(titleLabel.snp.leading)
            $0.trailing.equalTo(titleLabel.snp.trailing)
            $0.centerY.equalToSuperview().offset(Spacing.base04)
        }

        continueButton.snp.makeConstraints {
            $0.top.equalTo(subtitleLabel.snp.bottom).offset(Spacing.base04)
            $0.leading.equalTo(subtitleLabel.snp.leading)
            $0.trailing.equalTo(subtitleLabel.snp.trailing)
            $0.height.equalTo(Layout.Size.buttonHeight)
        }

        laterButton.snp.makeConstraints {
            $0.top.equalTo(continueButton.snp.bottom).offset(Spacing.base01)
            $0.centerX.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }

    override func configureStyles() {
        titleLabel
            .labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textAlignment, .center)

        subtitleLabel
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
    }
}

// MARK: UI Target Methods
@objc
private extension DocumentCaptureIntroViewController {
    func didTapContinue() {
        viewModel.handleContinue()
    }

    func didTapLater() {
        viewModel.handleClose()
    }
}

// MARK: - Presenter Outputs
extension DocumentCaptureIntroViewController: DocumentCaptureIntroDisplayable {
    func presentStepInfo(title: String, description: String) {
        titleLabel.text = title
        subtitleLabel.text = description
    }
}
