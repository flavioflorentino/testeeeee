import Foundation

protocol DocumentCaptureIntroPresenting: AnyObject {
    var viewController: DocumentCaptureIntroDisplayable? { get set }
    func handleStepInfo()
    func handleContinue(_ step: IdentityValidationStep)
    func handleClose()
}

final class DocumentCaptureIntroPresenter: DocumentCaptureIntroPresenting {
    weak var viewController: DocumentCaptureIntroDisplayable?
    private let coordinator: DocumentCaptureIntroCoordinating

    init(coordinator: DocumentCaptureIntroCoordinating) {
        self.coordinator = coordinator
    }

    func handleStepInfo() {
        viewController?.presentStepInfo(
            title: FacialBiometricLocalizable.documentIntroTitle.text,
            description: FacialBiometricLocalizable.documentIntroDescription.text
        )
    }

    func handleContinue(_ step: IdentityValidationStep) {
        coordinator.perform(action: .nextStep(step: step))
    }

    func handleClose() {
        coordinator.perform(action: .close)
    }
}
