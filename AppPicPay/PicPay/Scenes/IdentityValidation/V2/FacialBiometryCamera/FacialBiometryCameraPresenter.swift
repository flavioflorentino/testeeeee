import Core
import Foundation

protocol FacialBiometryCameraPresenting: AnyObject {
    var viewController: FacialBiometryCameraDisplay? { get set }
    func configureCameraAuthorized()
    func presentPermissionDeniedAlert()
    func close()
    func showPreview(with previewImage: UIImage)
    func handleCaptureError(_ error: Error?)
}

final class FacialBiometryCameraPresenter: FacialBiometryCameraPresenting {
    private let coordinator: FacialBiometryCameraCoordinating
    var viewController: FacialBiometryCameraDisplay?

    init(coordinator: FacialBiometryCameraCoordinating) {
        self.coordinator = coordinator
    }
    
    func configureCameraAuthorized() {
        viewController?.displayAuthorizedCameraAccess()
    }
    
    func presentPermissionDeniedAlert() {
        let alert = configurationsAlert()
        viewController?.displayAlert(alert)
    }
    
    func handleCaptureError(_ error: Error?) {
        viewController?.displayAlert(with: error)
    }
    
    func close() {
        coordinator.perform(action: .close)
    }
    
    func showPreview(with previewImage: UIImage) {
        coordinator.perform(action: .preview(image: previewImage))
    }
    
    private func configurationsAlert() -> Alert {
        let configurationsButton = Button(
            title: FacialBiometricLocalizable.cameraPermissionConfigurationAlertButton.text,
            type: .cta
        ) { popup, _ in
            popup.dismiss(animated: true) {
                self.coordinator.perform(action: .config)
            }
        }
        
        let doItLaterButton = Button(title: DefaultLocalizable.doItLater.text, type: .cleanUnderlined) { popup, _ in
            popup.dismiss(animated: true)
            self.coordinator.perform(action: .close)
        }
        
        let alert = Alert(
            with: FacialBiometricLocalizable.cameraPermissionAlertTitle.text,
            text: FacialBiometricLocalizable.cameraPermissionAlertMessage.text,
            buttons: [configurationsButton, doItLaterButton],
            image: Alert.Image(with: Assets.IdentityValidation.identityCameraPermission.image)
        )
        
        return alert
    }
}
