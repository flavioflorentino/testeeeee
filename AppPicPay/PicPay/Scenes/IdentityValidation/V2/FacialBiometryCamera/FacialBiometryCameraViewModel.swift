import AnalyticsModule
import Foundation

protocol FacialBiometryCameraViewModelInputs: AnyObject {
    func checkCameraPermission()
    func handleCaptureImageError(_ error: Error?)
    func handleCaptureImageSuccess(image: UIImage)
    func close()
    func trackCameraPresentation()
}

final class FacialBiometryCameraViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let service: FacialBiometryCameraServicing
    private let presenter: FacialBiometryCameraPresenting
    
    init(service: FacialBiometryCameraServicing, presenter: FacialBiometryCameraPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

extension FacialBiometryCameraViewModel: FacialBiometryCameraViewModelInputs {
    func checkCameraPermission() {
        service.checkCameraPermission { [weak self] status in
            switch status {
            case .authorized:
                self?.presenter.configureCameraAuthorized()
            case .notDetermined:
                self?.service.requestCameraPermission { _ in }
            case .denied, .disabled:
                self?.presenter.presentPermissionDeniedAlert()
            }
        }
    }
    
    func handleCaptureImageError(_ error: Error?) {
        presenter.handleCaptureError(error)
    }
    
    func handleCaptureImageSuccess(image: UIImage) {
        dependencies.analytics.log(IdentityValidationEvent.takeSelfie(action: .taken))
        presenter.showPreview(with: image)
    }
    
    func close() {
        dependencies.analytics.log(IdentityValidationEvent.takeSelfie(action: .close))
        presenter.close()
    }
    
    func trackCameraPresentation() {
        dependencies.analytics.log(IdentityValidationEvent.takeSelfie(action: .none))
    }
}
