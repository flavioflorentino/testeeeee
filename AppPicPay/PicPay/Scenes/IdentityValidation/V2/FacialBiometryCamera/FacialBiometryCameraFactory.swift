import Foundation

enum FacialBiometryCameraFactory {
    static func make(navigationController: UINavigationController) -> FacialBiometryCameraViewController {
        let container = DependencyContainer()
        let service: FacialBiometryCameraServicing = FacialBiometryCameraService()
        let coordinator: FacialBiometryCameraCoordinating = FacialBiometryCameraCoordinator(
            navigationController: navigationController
        )
        let presenter: FacialBiometryCameraPresenting = FacialBiometryCameraPresenter(coordinator: coordinator)
        let viewModel = FacialBiometryCameraViewModel(service: service, presenter: presenter, dependencies: container)
        let viewController = FacialBiometryCameraViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
