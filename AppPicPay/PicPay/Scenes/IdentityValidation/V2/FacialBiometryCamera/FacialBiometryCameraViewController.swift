import UI
import UIKit

extension FacialBiometryCameraViewController.Layout {
    enum Others {
        // This value was taken from the division of height by the width of the mask in the figma
        static let multiplier: CGFloat = 1.3333215059
    }
}

final class FacialBiometryCameraViewController: ViewController<FacialBiometryCameraViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    private lazy var centerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var transparentView = UIView()
    
    private lazy var closeButton: UIButton = {
        let image = Assets.Savings.iconRoundCloseGreen.image
        
        let button = UIButton()
        button.isHidden = true
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
        button.accessibilityLabel = FacialBiometricLocalizable.closepictureCaptureAccessibilityText.text
        return button
    }()
    
    private lazy var tipLabel: UILabel = {
        let label = UILabel()
        label.isHidden = true
        label.text = FacialBiometricLocalizable.selfieTakePictureTip.text
        label.textColor = Colors.white.lightColor
        label.textAlignment = .center
        label.font = Typography.bodyPrimary().font()
        return label
    }()
    
    private lazy var shotButton: UIButton = {
        let image = Assets.Biometry.biometryCamera.image
        let button = UIButton()
        button.isHidden = true
        button.setImage(image, for: .normal)
        button.backgroundColor = Colors.branding400.color
        button.layer.cornerRadius = Spacing.base04
        button.addTarget(self, action: #selector(didTapTake), for: .touchUpInside)
        button.accessibilityLabel = FacialBiometricLocalizable.shotButtonAccessibilityText.text
        return button
    }()
    
    private var cameraController: CameraController?

    override var prefersStatusBarHidden: Bool {
        true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.trackCameraPresentation()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(appBecameActive),
            name: UIApplication.didBecomeActiveNotification,
            object: nil
        )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        setNeedsStatusBarAppearanceUpdate()
        viewModel.checkCameraPermission()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopCamera()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(centerView)
        view.addSubview(transparentView)
        view.addSubview(closeButton)
        view.addSubview(tipLabel)
        view.addSubview(shotButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.white.color
    }
    
    override func setupConstraints() {
        centerView.layout {
            $0.leading == view.leadingAnchor
            $0.top == tipLabel.bottomAnchor
            $0.trailing == view.trailingAnchor
            $0.bottom == shotButton.topAnchor
        }
        
        transparentView.layout {
            $0.leading == view.leadingAnchor + Spacing.base06
            $0.trailing == view.trailingAnchor - Spacing.base06
            $0.centerY == centerView.centerYAnchor
            $0.height.equal(to: transparentView.widthAnchor, multiplier: Layout.Others.multiplier)
        }
        
        closeButton.layout {
            $0.trailing == view.trailingAnchor - Spacing.base02
            $0.top == view.compatibleSafeAreaLayoutGuide.topAnchor + Spacing.base03
            $0.height == Spacing.base04
            $0.width == Spacing.base04
        }
        
        tipLabel.layout {
            $0.leading == view.leadingAnchor + Spacing.base04
            $0.top == closeButton.bottomAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base04
        }
        
        shotButton.layout {
            $0.bottom == view.compatibleSafeAreaLayoutGuide.bottomAnchor - Spacing.base06
            $0.centerX == view.centerXAnchor
            $0.height == Spacing.base08
            $0.width == Spacing.base08
        }
    }
    
    private func setupOverlayLayout() {
        guard view.layer.sublayers?.contains(where: { $0 is CAShapeLayer }) == false else {
            return
        }
        
        view.layoutIfNeeded()
        
        let maskPath = UIBezierPath(rect: transparentView.frame)
        maskPath.cgPath = CGPath(ellipseIn: transparentView.frame, transform: nil)
        
        let overlayPath = UIBezierPath(rect: view.bounds)
        overlayPath.append(maskPath)
        overlayPath.usesEvenOddFillRule = true
        
        let position: UInt32 = 1
        
        let fillLayer = CAShapeLayer()
        fillLayer.path = overlayPath.cgPath
        fillLayer.fillRule = .evenOdd
        fillLayer.fillColor = Colors.black.lightColor.withAlphaComponent(0.7).cgColor
        view.layer.insertSublayer(fillLayer, at: position)
    }
    
    @objc
    private func appBecameActive() {
        viewModel.checkCameraPermission()
    }
}

// MARK: UI Target Methods
@objc
private extension FacialBiometryCameraViewController {
    func didTapClose() {
        viewModel.close()
    }
    
    func didTapTake() {
        captureImage()
    }
    
    func showViews() {
        shotButton.isHidden = false
        tipLabel.isHidden = false
        closeButton.isHidden = false
        setupOverlayLayout()
    }
}

// MARK: Camera Methods
private extension FacialBiometryCameraViewController {
    func configureCamera() {
        guard let cameraController = cameraController else {
            createCameraController()
            return
        }
        
        if !cameraController.captureSession.isRunning {
            cameraController.captureSession.startRunning()
        }
    }

    func createCameraController() {
        cameraController = CameraController()
        cameraController?.prepare { [weak self] error in
            guard
                let self = self,
                let cameraPlaceholder = self.view
                else {
                    return
            }
            
            if let error = error {
                self.viewModel.handleCaptureImageError(error)
                return
            }
            
            try? self.cameraController?.displayPreview(on: cameraPlaceholder)
            try? self.cameraController?.switchToFrontCamera()
            self.showViews()
        }
    }
    
    func captureImage() {
        cameraController?.captureImage { [weak self] image, error in
            guard let self = self else {
                return
            }
            
            guard let capturedImage = image else {
                self.viewModel.handleCaptureImageError(error)
                return
            }
            
            self.viewModel.handleCaptureImageSuccess(image: capturedImage)
            self.stopCamera()
        }
    }
    
    func stopCamera() {
        cameraController?.captureSession.stopRunning()
    }
}

// MARK: View Model Outputs
extension FacialBiometryCameraViewController: FacialBiometryCameraDisplay {
    func displayAuthorizedCameraAccess() {
        shotButton.isEnabled = true
        configureCamera()
    }
    
    func displayAlert(with error: Error?) {
        AlertMessage.showAlert(error, controller: self)
    }
    
    func displayAlert(_ alert: Alert) {
        AlertMessage.showAlert(alert, controller: self)
    }
}
