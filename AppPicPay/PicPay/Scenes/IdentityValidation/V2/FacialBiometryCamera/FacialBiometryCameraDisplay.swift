protocol FacialBiometryCameraDisplay: AnyObject {
    func displayAuthorizedCameraAccess()
    func displayAlert(with error: Error?)
    func displayAlert(_ alert: Alert)
}
