import UIKit

enum FacialBiometryCameraAction {
    case close
    case preview(image: UIImage)
    case config
}

protocol FacialBiometryCameraCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: FacialBiometryCameraAction)
}

final class FacialBiometryCameraCoordinator: FacialBiometryCameraCoordinating {
    weak var viewController: UIViewController?
    var navigationController: UINavigationController
    private var application: UIApplicationContract
    
    init(navigationController: UINavigationController, application: UIApplicationContract = UIApplication.shared) {
        self.navigationController = navigationController
        self.application = application
    }
    
    func perform(action: FacialBiometryCameraAction) {
        switch action {
        case .close:
            navigationController.dismiss(animated: true)
        case let .preview(image):
            let previewViewController = FacialBiometryPreviewFactory.make(
                withPreviewImage: image,
                navigationController: navigationController
            )
            navigationController.pushViewController(previewViewController, animated: true)
        case .config:
            guard
                let url = URL(string: UIApplication.openSettingsURLString),
                application.canOpenURL(url)
                else {
                    return
            }
            application.open(url)
        }
    }
}
