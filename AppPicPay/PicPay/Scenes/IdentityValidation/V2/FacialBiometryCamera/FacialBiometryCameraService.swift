import Foundation
import PermissionsKit

protocol FacialBiometryCameraServicing {
    func checkCameraPermission(completion: @escaping (_ permission: PermissionStatus) -> Void)
    func requestCameraPermission(completion: @escaping (_ permisson: PermissionStatus) -> Void)
}

final class FacialBiometryCameraService: FacialBiometryCameraServicing {
    func checkCameraPermission(completion: @escaping (PermissionStatus) -> Void) {
        let permissionCamera: Permission = PPPermission.camera()
        completion(permissionCamera.status)
    }
    
    func requestCameraPermission(completion: @escaping (PermissionStatus) -> Void) {
        let permissionCamera: Permission = PPPermission.camera()
        permissionCamera.request { status in
            completion(status)
        }
    }
}
