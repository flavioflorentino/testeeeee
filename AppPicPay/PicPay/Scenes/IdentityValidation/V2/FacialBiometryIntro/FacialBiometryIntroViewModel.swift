import AnalyticsModule
import Foundation

protocol FacialBiometryIntroViewModelInputs: AnyObject {
    func takeSelfie()
    func takeSelfieLater()
    func trackSelfieStepPresentation()
}

final class FacialBiometryIntroViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let presenter: FacialBiometryIntroPresenting

    init(presenter: FacialBiometryIntroPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

extension FacialBiometryIntroViewModel: FacialBiometryIntroViewModelInputs {
    func takeSelfie() {
        dependencies.analytics.log(IdentityValidationEvent.selfie(action: .started))
        presenter.presentTips()
    }
    
    func takeSelfieLater() {
        dependencies.analytics.log(IdentityValidationEvent.selfie(action: .drop))
        presenter.handleClose()
    }
    
    func trackSelfieStepPresentation() {
        dependencies.analytics.log(IdentityValidationEvent.selfie(action: .none))
    }
}
