import UIKit

enum FacialBiometryIntroAction {
    case close
    case tips
}

protocol FacialBiometryIntroCoordinating: AnyObject {
    func perform(action: FacialBiometryIntroAction)
}

final class FacialBiometryIntroCoordinator: FacialBiometryIntroCoordinating {
    private weak var coordinatorDelegate: IdentityValidationCoordinating?
    private let navigationController: UINavigationController
    
    init(coordinatorDelegate: IdentityValidationCoordinating?, navigationController: UINavigationController) {
        self.coordinatorDelegate = coordinatorDelegate
        self.navigationController = navigationController
    }
    
    func perform(action: FacialBiometryIntroAction) {
        switch action {
        case .close:
            coordinatorDelegate?.dismiss()
        case .tips:
            presentTips()
        }
    }
}

private extension FacialBiometryIntroCoordinator {
    func presentTips() {
        let tipsControler = FacialBiometryTipsFactory.make(
            coordinatorDelegate: coordinatorDelegate,
            navigationController: navigationController
        )
        navigationController.pushViewController(tipsControler, animated: true)
    }
}
