import UI
import UIKit

extension FacialBiometryIntroViewController.Layout {
    enum Fonts {
        static let descriptionFont = UIFont.systemFont(ofSize: 16, weight: .light)
        static let buttonFont = UIFont.systemFont(ofSize: 16, weight: .semibold)
        static let titleFont = UIFont.systemFont(ofSize: 20, weight: .bold)
        static let termsFont = UIFont.systemFont(ofSize: 12, weight: .light)
    }
    
    enum Size {
        static let image: CGFloat = 130
        static let buttonHeight: CGFloat = 48
    }
}

final class FacialBiometryIntroViewController: ViewController<FacialBiometryIntroViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
    private lazy var facialImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.IdentityValidation.selfieIntro.image
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = FacialBiometricLocalizable.selfieTitleText.text
        label.font = Layout.Fonts.titleFont
        label.textColor = Colors.black.color
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = FacialBiometricLocalizable.selfieDescriptionText.text
        label.font = Layout.Fonts.descriptionFont
        label.textColor = Colors.grayscale700.color
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    private lazy var selfieButton: UIPPButton = {
        let button = UIPPButton(type: .system)
        button.setTitle(FacialBiometricLocalizable.takeSelfie.text, for: .normal)
        button.normalBackgrounColor = Colors.branding400.color
        button.highlightedBackgrounColor = Colors.branding400.color.withAlphaComponent(0.6)
        button.normalTitleColor = Colors.white.lightColor
        button.highlightedTitleColor = Colors.white.lightColor
        button.cornerRadius = Spacing.base03
        button.titleLabel?.font = Layout.Fonts.buttonFont
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(didTapTake), for: .touchUpInside)
        return button
    }()
    
    private lazy var laterButton: UnderlinedButton = {
        let button = UnderlinedButton()
        button.setTitle(FacialBiometricLocalizable.takeSelfieLater.text, for: .normal)
        button.updateColor(
            Colors.branding400.color,
            highlightedColor: Colors.branding400.color.withAlphaComponent(0.6),
            font: Layout.Fonts.buttonFont
        )
        button.addTarget(self, action: #selector(didTapLater), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.trackSelfieStepPresentation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(facialImageView)
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(selfieButton)
        view.addSubview(laterButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.white.color
    }
    
    override func setupConstraints() {
        facialImageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.image)
        }

        titleLabel.snp.makeConstraints {
            $0.top.equalTo(facialImageView.snp.bottom).offset(Spacing.base04)
            $0.leading.equalToSuperview().offset(Spacing.base04)
            $0.trailing.equalToSuperview().offset(-Spacing.base04)
        }

        descriptionLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.equalTo(titleLabel.snp.leading)
            $0.trailing.equalTo(titleLabel.snp.trailing)
        }

        selfieButton.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base04)
            $0.leading.equalTo(descriptionLabel.snp.leading)
            $0.trailing.equalTo(descriptionLabel.snp.trailing)
            $0.height.equalTo(Layout.Size.buttonHeight)
        }

        laterButton.snp.makeConstraints {
            $0.top.equalTo(selfieButton.snp.bottom).offset(Spacing.base03)
            $0.centerX.equalToSuperview()
        }
    }
}

// MARK: UI Target Methods
private extension FacialBiometryIntroViewController {
    @objc
    func didTapTake() {
        viewModel.takeSelfie()
    }
    
    @objc
    func didTapLater() {
        viewModel.takeSelfieLater()
    }
}
