import Core
import UI

protocol FacialBiometryIntroPresenting: AnyObject {
    func handleClose()
    func presentTips()
}

final class FacialBiometryIntroPresenter: FacialBiometryIntroPresenting {
    private let coordinator: FacialBiometryIntroCoordinating

    init(coordinator: FacialBiometryIntroCoordinating) {
        self.coordinator = coordinator
    }
    
    func handleClose() {
        coordinator.perform(action: .close)
    }
    
    func presentTips() {
        coordinator.perform(action: .tips)
    }
}
