import Foundation

enum FacialBiometryIntroFactory {
    static func make(coordinatorDelegate: IdentityValidationCoordinating?, navigationController: UINavigationController) -> FacialBiometryIntroViewController {
        let container = DependencyContainer()
        let coordinator: FacialBiometryIntroCoordinating = FacialBiometryIntroCoordinator(
            coordinatorDelegate: coordinatorDelegate,
            navigationController: navigationController
        )
        let presenter: FacialBiometryIntroPresenting = FacialBiometryIntroPresenter(coordinator: coordinator)
        let viewModel = FacialBiometryIntroViewModel(presenter: presenter, dependencies: container)
        let viewController = FacialBiometryIntroViewController(viewModel: viewModel)

        return viewController
    }
}
