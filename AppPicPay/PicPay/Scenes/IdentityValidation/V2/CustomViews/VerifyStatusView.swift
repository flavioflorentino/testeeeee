import UI
import UIKit

private extension VerifyStatusView.Layout {
    enum Height {
        static let sendButtonHeight: CGFloat = 44
    }
    
    enum Alpha {
        static let overlayAlpha: CGFloat = 0.75
    }
}

protocol VerifyStatusViewDelegate: AnyObject {
    func didTouchConfirmAction()
    func didTouchDenialAction()
}

final class VerifyStatusView: UIView {
    fileprivate enum Layout { }
    
    private lazy var overlayContinerView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.black.lightColor.withAlphaComponent(Layout.Alpha.overlayAlpha)
        return view
    }()
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.text = FacialBiometricLocalizable.previewInfoText.text
        label.numberOfLines = 0
        label.font = Typography.bodyPrimary().font()
        label.textColor = Colors.white.lightColor
        return label
    }()
    
    private lazy var denialActionButton: UnderlinedButton = {
        let button = UnderlinedButton()
        button.setTitle(FacialBiometricLocalizable.previewTakeAnother.text, for: .normal)
        button.addTarget(self, action: #selector(denialButtonTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var confirmActionButton: ConfirmButton = {
        let button = ConfirmButton()
        button.cornerRadius(Layout.Height.sendButtonHeight / 2)
        button.setTitle(FacialBiometricLocalizable.previewSendPicture.text, for: .normal)
        button.addTarget(self, action: #selector(confirmButtonTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    weak var delegate: VerifyStatusViewDelegate?
    
    init() {
        super.init(frame: .zero)
        addComponents()
        layoutComponents()
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        addSubview(overlayContinerView)
        overlayContinerView.addSubview(infoLabel)
        overlayContinerView.addSubview(denialActionButton)
        overlayContinerView.addSubview(confirmActionButton)
    }
    
    private func layoutComponents() {
        overlayContinerView.layout {
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.top == topAnchor
            $0.bottom == bottomAnchor
        }
        
        infoLabel.layout {
            $0.leading == overlayContinerView.leadingAnchor + Spacing.base03
            $0.trailing == overlayContinerView.trailingAnchor - Spacing.base03
            $0.top == overlayContinerView.topAnchor + Spacing.base04
        }
        
        confirmActionButton.layout {
            $0.top == infoLabel.bottomAnchor + Spacing.base02
            $0.trailing == overlayContinerView.trailingAnchor - Spacing.base03
            $0.bottom == overlayContinerView.bottomAnchor - Spacing.base04
            $0.height == Layout.Height.sendButtonHeight
        }
        
        denialActionButton.layout {
            $0.centerY == confirmActionButton.centerYAnchor
            $0.leading == overlayContinerView.leadingAnchor + Spacing.base03
            $0.trailing >= confirmActionButton.leadingAnchor - Spacing.base03
        }
    }
    
    private func setup() {
        backgroundColor = .clear
    }
    
    @objc
    private func denialButtonTapped(_ sender: UIButton) {
        delegate?.didTouchDenialAction()
    }
    
    @objc
    private func confirmButtonTapped(_ sender: UIButton) {
        delegate?.didTouchConfirmAction()
    }
    
    func configureWith(infoText: String, denialButtonText: String, confirmButtonText: String) {
        infoLabel.text = infoText
        confirmActionButton.setTitle(confirmButtonText, for: .normal)
        denialActionButton.setTitle(denialButtonText, for: .normal)
    }
}
