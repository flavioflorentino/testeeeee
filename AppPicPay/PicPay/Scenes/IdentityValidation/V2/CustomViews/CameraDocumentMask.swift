import UI

struct CameraDocumentMask {
    enum Dimensions {
        static let cornerLine: CGFloat = UIScreen.main.bounds.width * 0.309_33
        static let cornerLineOffset: CGFloat = 3
        static let lineWidth: CGFloat = 6
    }
    
    func maskFor(step: IdentityValidationStep.StepType, forView: UIView, maskPath: UIBezierPath) -> CAShapeLayer {
        switch step {
        case .documentFront, .documentBack:
            return createMask(forView: forView, maskPath: maskPath)
        default:
            return createRoundMask(forView: forView, maskPath: maskPath)
        }
    }
    
    private func createMask(forView view: UIView, maskPath: UIBezierPath) -> CAShapeLayer {
        let overlayPath = UIBezierPath(rect: view.bounds)
        overlayPath.append(maskPath)
        overlayPath.usesEvenOddFillRule = true
        
        let fillLayer = CAShapeLayer()
        fillLayer.path = overlayPath.cgPath
        fillLayer.fillRule = .evenOdd
        fillLayer.fillColor = Colors.black.lightColor.withAlphaComponent(0.7).cgColor
        
        fillLayer.addSublayer(topLeftLayer(forPath: maskPath))
        fillLayer.addSublayer(topRightLayer(forPath: maskPath))
        fillLayer.addSublayer(bottomLeftLayer(forPath: maskPath))
        fillLayer.addSublayer(bottomRightLayer(forPath: maskPath))
        return fillLayer
    }
    
    private func createRoundMask(forView view: UIView, maskPath: UIBezierPath) -> CAShapeLayer {
        maskPath.cgPath = CGPath(ellipseIn: maskPath.bounds, transform: nil)
        
        let overlayPath = UIBezierPath(rect: view.bounds)
        overlayPath.append(maskPath)
        overlayPath.usesEvenOddFillRule = true
        
        let fillLayer = CAShapeLayer()
        fillLayer.path = overlayPath.cgPath
        fillLayer.fillRule = .evenOdd
        fillLayer.fillColor = Colors.black.lightColor.withAlphaComponent(0.7).cgColor
        return fillLayer
    }
    
    private func topLeftLayer(forPath path: UIBezierPath) -> CAShapeLayer {
        let point = CGPoint(x: path.bounds.minX, y: path.bounds.minY)
        let topLeftPath = UIBezierPath()
        
        topLeftPath.move(to: point)
        topLeftPath.addLine(to: CGPoint(x: point.x + Dimensions.cornerLine, y: point.y))
        topLeftPath.move(to: CGPoint(x: point.x, y: point.y - Dimensions.cornerLineOffset))
        topLeftPath.addLine(to: CGPoint(x: point.x, y: point.y + Dimensions.cornerLine))
        return shapeLayer(forPath: topLeftPath)
    }
    
    private func topRightLayer(forPath path: UIBezierPath) -> CAShapeLayer {
        let point = CGPoint(x: path.bounds.maxX, y: path.bounds.minY)
        let topRightPath = UIBezierPath()
        
        topRightPath.move(to: point)
        topRightPath.addLine(to: CGPoint(x: point.x - Dimensions.cornerLine, y: point.y))
        topRightPath.move(to: CGPoint(x: point.x, y: point.y - Dimensions.cornerLineOffset))
        topRightPath.addLine(to: CGPoint(x: point.x, y: point.y + Dimensions.cornerLine))
        return shapeLayer(forPath: topRightPath)
    }
    
    private func bottomLeftLayer(forPath path: UIBezierPath) -> CAShapeLayer {
        let point = CGPoint(x: path.bounds.minX, y: path.bounds.maxY)
        let bottomLeftPath = UIBezierPath()
        
        bottomLeftPath.move(to: point)
        bottomLeftPath.addLine(to: CGPoint(x: point.x + Dimensions.cornerLine, y: point.y))
        bottomLeftPath.move(to: CGPoint(x: point.x, y: point.y + Dimensions.cornerLineOffset))
        bottomLeftPath.addLine(to: CGPoint(x: point.x, y: point.y - Dimensions.cornerLine))
        return shapeLayer(forPath: bottomLeftPath)
    }
    
    private func bottomRightLayer(forPath path: UIBezierPath) -> CAShapeLayer {
        let point = CGPoint(x: path.bounds.maxX, y: path.bounds.maxY)
        let bottomRightPath = UIBezierPath()
        
        bottomRightPath.move(to: point)
        bottomRightPath.addLine(to: CGPoint(x: point.x - Dimensions.cornerLine, y: point.y))
        bottomRightPath.move(to: CGPoint(x: point.x, y: point.y + Dimensions.cornerLineOffset))
        bottomRightPath.addLine(to: CGPoint(x: point.x, y: point.y - Dimensions.cornerLine))
        return shapeLayer(forPath: bottomRightPath)
    }
    
    private func shapeLayer(forPath path: UIBezierPath) -> CAShapeLayer {
        let corner = CAShapeLayer()
        corner.strokeColor = Colors.success400.color.cgColor
        corner.path = path.cgPath
        corner.lineWidth = Dimensions.lineWidth
        corner.fillColor = UIColor.clear.cgColor
        return corner
    }
}
