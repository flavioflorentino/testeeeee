import UI
import UIKit

private extension UploadErrorView.Layout {
    enum Alpha {
        static let overlayAlpha: CGFloat = 0.75
    }
    
    enum Height {
        static let buttonHeight: CGFloat = 48
    }
}

protocol UploadErrorViewDelegate: AnyObject {
    func actionButtonTapped()
}

final class UploadErrorView: UIView {
    fileprivate enum Layout { }
    
    private lazy var overlayContinerView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.black.lightColor.withAlphaComponent(Layout.Alpha.overlayAlpha)
        return view
    }()
    
    private lazy var actionButton: ConfirmButton = {
        let button = ConfirmButton()
        button.cornerRadius(Spacing.base06 / 2)
        button.setTitle(FacialBiometricLocalizable.previewTakeAnother.text, for: .normal)
        button.addTarget(self, action: #selector(takeAnotherTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    weak var delegate: UploadErrorViewDelegate?
    
    init() {
        super.init(frame: .zero)
        addComponents()
        layoutComponents()
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        addSubview(overlayContinerView)
        overlayContinerView.addSubview(actionButton)
    }
    
    private func layoutComponents() {
        overlayContinerView.layout {
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.top == topAnchor
            $0.bottom == bottomAnchor
        }
        
        actionButton.layout {
            $0.leading == overlayContinerView.leadingAnchor + Spacing.base03
            $0.trailing == overlayContinerView.trailingAnchor - Spacing.base03
            $0.top == overlayContinerView.topAnchor + Spacing.base03
            $0.bottom == overlayContinerView.bottomAnchor - Spacing.base03
            $0.height == Layout.Height.buttonHeight
        }
    }
    
    private func setup() {
        backgroundColor = .clear
    }
    
    @objc
    private func takeAnotherTapped(_ sender: UIButton) {
        delegate?.actionButtonTapped()
    }
    
    func configureWith(actionText: String) {
        actionButton.setTitle(actionText, for: .normal)
    }
}
