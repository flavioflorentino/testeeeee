import UI
import UIKit

final class MessageErrorView: UIView {
    private lazy var continerView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorNegative300.color
        view.cornerRadius = .light
        return view
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.white.lightColor
        label.font = Typography.caption().font()
        label.numberOfLines = 0
        return label
    }()
    
    init() {
        super.init(frame: .zero)
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        addSubview(continerView)
        continerView.addSubview(messageLabel)
    }
    
    private func layoutComponents() {
        continerView.layout {
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.top == topAnchor
            $0.bottom == bottomAnchor
        }
        
        messageLabel.layout {
            $0.leading == continerView.leadingAnchor + Spacing.base02
            $0.trailing == continerView.trailingAnchor - Spacing.base02
            $0.top == continerView.topAnchor + Spacing.base02
            $0.bottom == continerView.bottomAnchor - Spacing.base02
        }
    }
    
    func setErrorMessage(_ message: String) {
        messageLabel.text = message
    }
}
