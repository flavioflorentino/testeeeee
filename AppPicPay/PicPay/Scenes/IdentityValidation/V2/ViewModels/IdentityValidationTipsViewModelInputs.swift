import Foundation

protocol IdentityValidationTipsViewModelInputs: AnyObject {
    func configureViews()
    func handleContinue()
    func handleClose()
}
