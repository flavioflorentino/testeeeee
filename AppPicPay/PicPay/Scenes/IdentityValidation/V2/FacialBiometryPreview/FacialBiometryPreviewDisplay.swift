import UIKit

protocol FacialBiometryPreviewDisplay: AnyObject {
    func displayPreviewImage(_ image: UIImage)
    func configureVerifyStatusViewInfo(titleText: String, denialActionText: String, confirmActionText: String)
    func configureUploadErrorViewInfo(actionText: String)
    func displayLoading(_ isLoading: Bool)
    func displayUploadErrorMessage(_ message: String)
}
