protocol FacialBiometryPreviewServicing {
    func uploadImage(imageData: Data, completion: @escaping (PicPayResult<IdentityValidationStep>) -> Void)
}

final class FacialBiometryPreviewService: BaseApi, FacialBiometryPreviewServicing {
    func uploadImage(imageData: Data, completion: @escaping (PicPayResult<IdentityValidationStep>) -> Void) {
        let files: [FileUpload] = [FileUpload(key: "image", data: imageData, fileName: "imagem.jpg", mimeType: "image/jpg")]
        requestManager.apiUpload(
            WebServiceInterface.apiEndpoint(kWsUrlIdentityValidationStepImage),
            method: .post,
            files: files,
            uploadProgress: nil,
            completion: completion
        )
    }
}
