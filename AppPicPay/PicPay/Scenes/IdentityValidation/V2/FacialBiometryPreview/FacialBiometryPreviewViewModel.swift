import AnalyticsModule

protocol FacialBiometryPreviewViewModelInputs: AnyObject {
    func configurePreview()
    func takeAnotherPicture()
    func sendPicture()
}

final class FacialBiometryPreviewViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let service: FacialBiometryPreviewServicing
    private let presenter: FacialBiometryPreviewPresenting
    private let previewImage: UIImage
    private let imageSize = CGSize(width: 720, height: 1_280)
    
    init(
        service: FacialBiometryPreviewServicing,
        presenter: FacialBiometryPreviewPresenting,
        dependencies: Dependencies,
        previewImage: UIImage
    ) {
        self.service = service
        self.presenter = presenter
        self.previewImage = previewImage
        self.dependencies = dependencies
    }
}

extension FacialBiometryPreviewViewModel: FacialBiometryPreviewViewModelInputs {
    func configurePreview() {
        dependencies.analytics.log(IdentityValidationEvent.sendSelfie(action: .none))
        presenter.presentPreviewImage(previewImage)
        presenter.configureSubviews()
    }
    
    func takeAnotherPicture() {
        dependencies.analytics.log(IdentityValidationEvent.sendSelfie(action: .drop))
        presenter.presentCamera()
    }
    
    func sendPicture() {
        dependencies.analytics.log(IdentityValidationEvent.sendSelfie(action: .finished))
        guard
            let imageForUpload = previewImage.resizeImage(targetSize: imageSize),
            let imageData = imageForUpload.jpegData(compressionQuality: 1)
            else {
                return
        }
        
        uploadImage(imageData: imageData)
    }
    
    private func uploadImage(imageData: Data) {
        presenter.presentLoading(true)
        
        service.uploadImage(imageData: imageData) { [weak self] result in
            self?.presenter.presentLoading(false)

            switch result {
            case .success(let step):
                self?.presenter.presentNextStep(step)
            case .failure(let error):
                self?.dependencies.analytics.log(IdentityValidationEvent.selfieError)
                self?.presenter.presentUploadError(error)
            }
        }
    }
}
