import UIKit

enum FacialBiometryPreviewAction {
    case takePhoto
    case nextStep(step: IdentityValidationStep)
}

protocol FacialBiometryPreviewCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: FacialBiometryPreviewAction)
}

final class FacialBiometryPreviewCoordinator: FacialBiometryPreviewCoordinating {
    weak var viewController: UIViewController?
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func perform(action: FacialBiometryPreviewAction) {
        switch action {
        case .takePhoto:
            navigationController.popViewController(animated: true)
        case .nextStep(let step):
            navigationController.dismiss(animated: true) {
                NotificationCenter.default.post(name: .didSendSelfie, object: step)
            }
        }
    }
}
