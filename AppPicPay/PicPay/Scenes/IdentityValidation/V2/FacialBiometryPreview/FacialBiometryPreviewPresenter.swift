import Core

protocol FacialBiometryPreviewPresenting: AnyObject {
    var viewController: FacialBiometryPreviewDisplay? { get set }
    func presentPreviewImage(_ image: UIImage)
    func configureSubviews()
    func presentCamera()
    func presentLoading(_ isLoading: Bool)
    func presentUploadError(_ error: PicPayError)
    func presentNextStep(_ step: IdentityValidationStep)
}

final class FacialBiometryPreviewPresenter: FacialBiometryPreviewPresenting {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private let coordinator: FacialBiometryPreviewCoordinating
    var viewController: FacialBiometryPreviewDisplay?
    
    init(coordinator: FacialBiometryPreviewCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    func presentPreviewImage(_ image: UIImage) {
        viewController?.displayPreviewImage(image)
    }
    
    func configureSubviews() {
        viewController?.configureVerifyStatusViewInfo(
            titleText: FacialBiometricLocalizable.previewInfoText.text,
            denialActionText: FacialBiometricLocalizable.previewTakeAnother.text,
            confirmActionText: FacialBiometricLocalizable.previewSendPicture.text
        )
        viewController?.configureUploadErrorViewInfo(actionText: FacialBiometricLocalizable.previewTakeAnother.text)
    }
    
    func presentCamera() {
        coordinator.perform(action: .takePhoto)
    }
    
    func presentLoading(_ isLoading: Bool) {
        dependencies.mainQueue.async { [weak self] in
            self?.viewController?.displayLoading(isLoading)
        }
    }
    
    func presentUploadError(_ error: PicPayError) {
        dependencies.mainQueue.async { [weak self] in
            self?.viewController?.displayUploadErrorMessage(error.message)
        }   
    }
    
    func presentNextStep(_ step: IdentityValidationStep) {
        dependencies.mainQueue.async { [weak self] in
            self?.coordinator.perform(action: .nextStep(step: step))
        }
    }
}
