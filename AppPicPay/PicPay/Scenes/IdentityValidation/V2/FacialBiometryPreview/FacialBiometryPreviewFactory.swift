enum FacialBiometryPreviewFactory {
    static func make(
        withPreviewImage previewImage: UIImage,
        navigationController: UINavigationController
    ) -> FacialBiometryPreviewViewController {
        let container = DependencyContainer()
        let service: FacialBiometryPreviewServicing = FacialBiometryPreviewService()
        let coordinator: FacialBiometryPreviewCoordinating = FacialBiometryPreviewCoordinator(
            navigationController: navigationController
        )
        let presenter: FacialBiometryPreviewPresenting = FacialBiometryPreviewPresenter(
            coordinator: coordinator,
            dependencies: container
        )
        let viewModel = FacialBiometryPreviewViewModel(
            service: service,
            presenter: presenter,
            dependencies: container,
            previewImage: previewImage
        )
        let viewController = FacialBiometryPreviewViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
