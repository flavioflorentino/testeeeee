import UI
import UIKit

final class FacialBiometryPreviewViewController: ViewController<FacialBiometryPreviewViewModelInputs, UIView> {
    private lazy var previewImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private lazy var checkStatusBottomView: VerifyStatusView = {
        let checkView = VerifyStatusView()
        checkView.delegate = self
        return checkView
    }()
    
    private lazy var uploadErrorBottomView: UploadErrorView = {
        let errorView = UploadErrorView()
        errorView.isHidden = true
        errorView.delegate = self
        return errorView
    }()
    
    private lazy var messageErrorView: MessageErrorView = {
        let errorMessageView = MessageErrorView()
        errorMessageView.isHidden = true
        return errorMessageView
    }()
    
    lazy var loadingView: LoadingView = {
        let loadingView = LoadingView()
        loadingView.text = FacialBiometricLocalizable.previewUploadText.text
        loadingView.setTextColor(Colors.white.lightColor)
        loadingView.backgroundColor = Colors.black.lightColor.withAlphaComponent(0.75)
        return loadingView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.configurePreview()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(previewImageView)
        view.addSubview(checkStatusBottomView)
        view.addSubview(uploadErrorBottomView)
        view.addSubview(messageErrorView)
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.white.lightColor  
    }
    
    override func setupConstraints() {
        previewImageView.layout {
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
            $0.top == view.topAnchor
            $0.bottom == view.bottomAnchor
        }
        
        checkStatusBottomView.layout {
            $0.bottom == view.bottomAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
        }
        
        uploadErrorBottomView.layout {
            $0.bottom == view.bottomAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
        }
        
        messageErrorView.layout {
            $0.top == view.topAnchor + Spacing.base06
            $0.leading == view.leadingAnchor + Spacing.base03
            $0.trailing == view.trailingAnchor - Spacing.base03
        }
    }
}

// MARK: Private Methods
private extension FacialBiometryPreviewViewController {
    func setErrorStateWith(message: String) {
        uploadErrorBottomView.isHidden = false
        checkStatusBottomView.isHidden = true
        messageErrorView.isHidden = false
        messageErrorView.setErrorMessage(message)
    }
}

// MARK: View Model Outputs
extension FacialBiometryPreviewViewController: FacialBiometryPreviewDisplay {
    func displayPreviewImage(_ image: UIImage) {
        previewImageView.image = image
    }
    
    func displayLoading(_ isLoading: Bool) {
        isLoading ? startLoadingView() : stopLoadingView()
        checkStatusBottomView.isHidden = isLoading
    }
    
    func displayUploadErrorMessage(_ message: String) {
        setErrorStateWith(message: message)
    }
    
    func configureVerifyStatusViewInfo(titleText: String, denialActionText: String, confirmActionText: String) {
        checkStatusBottomView.configureWith(
            infoText: titleText,
            denialButtonText: denialActionText,
            confirmButtonText: confirmActionText
        )
    }
    
    func configureUploadErrorViewInfo(actionText: String) {
        uploadErrorBottomView.configureWith(actionText: actionText)
    }
}

extension FacialBiometryPreviewViewController: VerifyStatusViewDelegate {
    func didTouchConfirmAction() {
        viewModel.sendPicture()
    }
    
    func didTouchDenialAction() {
        viewModel.takeAnotherPicture()
    }
}

extension FacialBiometryPreviewViewController: UploadErrorViewDelegate {
    func actionButtonTapped() {
        viewModel.takeAnotherPicture()
    }
}

extension FacialBiometryPreviewViewController: LoadingViewProtocol { }
