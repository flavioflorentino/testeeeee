import Foundation

enum FacialBiometryTipsFactory {
    static func make(coordinatorDelegate: IdentityValidationCoordinating?, navigationController: UINavigationController) -> DocumentCaptureTipsViewController {
        let container = DependencyContainer()
        let coordinator = FacialBiometryTipsCoordinator(coordinatorDelegate: coordinatorDelegate, navigationController: navigationController)
        let presenter = FacialBiometryTipsPresenter(coordinator: coordinator)
        let viewModel = FacialBiometryTipsViewModel(presenter: presenter, dependencies: container)
        let viewController = DocumentCaptureTipsViewController(viewModel: viewModel)

        presenter.viewController = viewController

        return viewController
    }
}
