import UIKit

enum FacialBiometryTipsAction {
    case camera
    case close
    case next(step: IdentityValidationStep)
}

protocol FacialBiometryTipsCoordinating: AnyObject {
    func perform(action: FacialBiometryTipsAction)
}

final class FacialBiometryTipsCoordinator {
    private weak var coordinatorDelegate: IdentityValidationCoordinating?
    private let navigationController: UINavigationController
    
    init(coordinatorDelegate: IdentityValidationCoordinating?, navigationController: UINavigationController) {
        self.coordinatorDelegate = coordinatorDelegate
        self.navigationController = navigationController
    }
}

// MARK: - FacialBiometryTipsCoordinating
extension FacialBiometryTipsCoordinator: FacialBiometryTipsCoordinating {
    func perform(action: FacialBiometryTipsAction) {
        switch action {
        case .camera:
            presentCamera()
        case .close:
            coordinatorDelegate?.dismiss()
        case .next(let step):
            coordinatorDelegate?.pushNextController(for: step)
        }
    }
    
    private func presentCamera() {
        let navController = UINavigationController()
        let cameraController = FacialBiometryCameraFactory.make(navigationController: navController)
        navController.viewControllers = [cameraController]
        navController.modalPresentationStyle = .fullScreen
        navigationController.present(navController, animated: true)
    }
}
