import AnalyticsModule
import Foundation

protocol FacialBiometryTipsViewModelInputs: IdentityValidationTipsViewModelInputs { }

final class FacialBiometryTipsViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let presenter: FacialBiometryTipsPresenting

    init(presenter: FacialBiometryTipsPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - FacialBiometryTipsViewModelInputs
extension FacialBiometryTipsViewModel: FacialBiometryTipsViewModelInputs {
    func configureViews() {
        dependencies.analytics.log(IdentityValidationEvent.selfieTips(action: .none))
        presenter.configureViews()
    }
    
    func handleContinue() {
        NotificationCenter.default.addObserver(self, selector: #selector(didSendSelfie(_:)), name: .didSendSelfie, object: nil)
        presenter.handleContinue()
    }

    func handleClose() {
        presenter.handleClose()
    }
    
    @objc
    func didSendSelfie(_ notification: Notification) {
        NotificationCenter.default.removeObserver(self)
        
        guard let step = notification.object as? IdentityValidationStep else {
            return
        }
        
        presenter.presentNextStep(step)
    }
}
