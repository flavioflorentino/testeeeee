import Foundation

protocol FacialBiometryTipsPresenting: AnyObject {
    var viewController: DocumentCaptureTipsDisplay? { get set }
    func configureViews()
    func handleContinue()
    func handleClose()
    func presentNextStep(_ step: IdentityValidationStep)
}

final class FacialBiometryTipsPresenter: FacialBiometryTipsPresenting {
    private let coordinator: FacialBiometryTipsCoordinating
    weak var viewController: DocumentCaptureTipsDisplay?

    init(coordinator: FacialBiometryTipsCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - FacialBiometryTipsPresenting
extension FacialBiometryTipsPresenter {
    func configureViews() {
        viewController?.displayTips(.selfie)
    }

    func handleContinue() {
        coordinator.perform(action: .camera)
    }

    func handleClose() {
        coordinator.perform(action: .close)
    }
    
    func presentNextStep(_ step: IdentityValidationStep) {
        coordinator.perform(action: .next(step: step))
    }
}
