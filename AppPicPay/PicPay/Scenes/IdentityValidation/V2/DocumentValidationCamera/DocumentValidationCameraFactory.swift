import Foundation

enum DocumentValidationCameraFactory {
    static func make(navigationController: UINavigationController, step: IdentityValidationStep) -> DocumentValidationCameraViewController {
        let container = DependencyContainer()
        let service: DocumentValidationCameraServicing = DocumentValidationCameraService()
        let coordinator: DocumentValidationCameraCoordinating = DocumentValidationCameraCoordinator(
            navigationController: navigationController
        )
        let presenter: DocumentValidationCameraPresenting = DocumentValidationCameraPresenter(coordinator: coordinator)
        let viewModel = DocumentValidationCameraViewModel(service: service, presenter: presenter, step: step, dependencies: container)
        let viewController = DocumentValidationCameraViewController(viewModel: viewModel)

        presenter.viewController = viewController

        return viewController
    }
}
