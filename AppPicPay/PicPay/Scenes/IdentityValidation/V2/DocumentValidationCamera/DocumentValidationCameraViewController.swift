import UI

extension DocumentValidationCameraViewController.Layout {
    enum Radius {
        static let shotButton = Size.shotButton / 2
    }

    enum Size {
        static let closeButton: CGFloat = 32
        static let shotButton: CGFloat = 64
    }

    enum SublayerIndex {
        static let mask: UInt32 = 1
    }
}

final class DocumentValidationCameraViewController: ViewController<DocumentValidationCameraViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
    private lazy var transparentView = UIView()
    
    private lazy var closeButton: UIButton = {
        let button = UIButton()
        let image = Assets.Savings.iconRoundCloseGreen.image
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
        button.accessibilityLabel = FacialBiometricLocalizable.closepictureCaptureAccessibilityText.text
        return button
    }()
    
    private lazy var tipLabel: UILabel = {
        let label = UILabel()
        label.text = FacialBiometricLocalizable.selfieTakePictureTip.text
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var shotButton: UIButton = {
        let image = Assets.Biometry.biometryCamera.image
        let button = UIButton()
        button.setImage(image, for: .normal)
        button.backgroundColor = Colors.branding400.color
        button.layer.cornerRadius = Layout.Radius.shotButton
        button.addTarget(self, action: #selector(didTapTake), for: .touchUpInside)
        button.accessibilityLabel = FacialBiometricLocalizable.shotButtonAccessibilityText.text
        return button
    }()
    
    private lazy var permissionInfoView: PermissionInfoView = {
        let permissionView = PermissionInfoView(frame: self.view.frame)
        permissionView.imageView.image = Assets.CreditCard.camera.image
        permissionView.textLabel.text = IdentityValidationLocalizable.cameraPermissionMessage.text
        permissionView.button.setTitle(IdentityValidationLocalizable.changeSettings.text, for: .normal)
        permissionView.button.addTarget(self, action: #selector(requestCameraPermission), for: .touchUpInside)
        return permissionView
    }()
    
    private var cameraController: CameraController?
    
    override var prefersStatusBarHidden: Bool { true }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        viewModel.checkCameraPermission()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopCamera()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        setupOverlayLayout()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(transparentView)
        view.addSubview(closeButton)
        view.addSubview(tipLabel)
        view.addSubview(shotButton)
    }
    
    override func setupConstraints() {
        closeButton.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base03)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.size.equalTo(Layout.Size.closeButton)
        }

        tipLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base04)
            $0.trailing.equalToSuperview().inset(Spacing.base04)
            $0.top.equalTo(closeButton.snp.bottom).offset(Spacing.base02)
        }

        transparentView.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base05)
            $0.trailing.equalToSuperview().inset(Spacing.base05)
            $0.top.equalTo(tipLabel.snp.bottom).offset(Spacing.base04)
            $0.bottom.equalTo(shotButton.snp.top).offset(-Spacing.base02)
        }

        shotButton.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.shotButton)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).inset(Spacing.base04)
        }
    }

    override func configureStyles() {
        view.backgroundColor = Colors.backgroundPrimary.color

        tipLabel
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .white())
            .with(\.textAlignment, .center)
    }
    
    private func setupOverlayLayout() {
        guard view.layer.sublayers?.contains(where: { $0 is CAShapeLayer }) == false else {
            return
        }

        view.layoutIfNeeded()
        viewModel.loadCameraInfo()
    }
}

// MARK: View Model Outputs
extension DocumentValidationCameraViewController: DocumentValidationCameraDisplay {
    func displayCameraInfo(maskType: IdentityValidationStep.StepType, hintText: NSAttributedString) {
        tipLabel.attributedText = hintText
        
        let mask = CameraDocumentMask().maskFor(step: maskType, forView: view, maskPath: UIBezierPath(rect: transparentView.frame))
        view.layer.insertSublayer(mask, at: Layout.SublayerIndex.mask)
    }
    
    func displayAuthorizedCameraAccess() {
        shotButton.isEnabled = true
        removePermissionPlaceholder()
        configureCamera()
    }
    
    func displayDeniedCameraAccess() {
        shotButton.isEnabled = false
        view.insertSubview(permissionInfoView, belowSubview: closeButton)
    }
    
    func displayAlert(with error: Error?) {
        AlertMessage.showAlert(error, controller: self)
    }
}

// MARK: UI Target Methods
@objc
private extension DocumentValidationCameraViewController {
    func didTapClose() {
        viewModel.close()
    }
    
    func didTapTake() {
        captureImage()
    }
    
    func requestCameraPermission() {
        viewModel.checkCameraPermission()
    }
}

// MARK: Camera Methods
private extension DocumentValidationCameraViewController {
    func removePermissionPlaceholder() {
        permissionInfoView.removeFromSuperview()
    }
    
    func configureCamera() {
        guard cameraController == nil else {
            cameraController?.captureSession.startRunning()
            return
        }
        
        cameraController = CameraController()
        cameraController?.prepare { [weak self] error in
            self?.handleCameraControllerPrepare(error)
        }
    }
    
    func captureImage() {
        cameraController?.captureImage { [weak self] image, error in
            guard let self = self else {
                return
            }
            
            guard let capturedImage = image else {
                self.viewModel.handleCaptureImageError(error)
                return
            }
            
            self.viewModel.handleCaptureImageSuccess(image: capturedImage)
            self.stopCamera()
        }
    }
    
    func stopCamera() {
        cameraController?.captureSession.stopRunning()
    }

    private func handleCameraControllerPrepare(_ error: Error?) {
        if let error = error {
            viewModel.handleCaptureImageError(error)
            return
        }

        try? cameraController?.displayPreview(on: view)
    }
}
