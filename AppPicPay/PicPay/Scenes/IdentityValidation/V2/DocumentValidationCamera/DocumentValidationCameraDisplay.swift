import UIKit

protocol DocumentValidationCameraDisplay: AnyObject {
    func displayCameraInfo(maskType: IdentityValidationStep.StepType, hintText: NSAttributedString)
    func displayAuthorizedCameraAccess()
    func displayDeniedCameraAccess()
    func displayAlert(with error: Error?)
}
