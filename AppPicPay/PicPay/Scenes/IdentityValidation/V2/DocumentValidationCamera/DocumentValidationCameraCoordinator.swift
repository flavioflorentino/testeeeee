import UIKit

enum DocumentValidationCameraAction {
    case close
    case preview(image: UIImage, currentStep: IdentityValidationStep)
}

protocol DocumentValidationCameraCoordinating: AnyObject {
    func perform(action: DocumentValidationCameraAction)
}

final class DocumentValidationCameraCoordinator: DocumentValidationCameraCoordinating {
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func perform(action: DocumentValidationCameraAction) {
        switch action {
        case .close:
            navigationController.dismiss(animated: true)
        case let .preview(image, step):
            let previewController = DocumentCapturePreviewFactory.make(
                navigationController: navigationController,
                previewImage: image,
                step: step
            )
            navigationController.pushViewController(previewController, animated: true)
        }
    }
}
