import Core
import UI

protocol DocumentValidationCameraPresenting: AnyObject {
    var viewController: DocumentValidationCameraDisplay? { get set }
    func presentCameraInfo(forStep step: IdentityValidationStep)
    func configureCameraAuthorized()
    func configureCameraDenied()
    func close()
    func handleCaptureError(error: Error?)
    func showPreview(with previewImage: UIImage, currentStep: IdentityValidationStep)
}

final class DocumentValidationCameraPresenter: DocumentValidationCameraPresenting {
    private let coordinator: DocumentValidationCameraCoordinating
    weak var viewController: DocumentValidationCameraDisplay?

    init(coordinator: DocumentValidationCameraCoordinating) {
        self.coordinator = coordinator
    }
    
    func presentCameraInfo(forStep step: IdentityValidationStep) {
        let hintText: String
        let highlightedText: String

        switch step.type {
        case .documentFront:
            hintText = FacialBiometricLocalizable.documentFrontCameraTitle.text
            highlightedText = FacialBiometricLocalizable.documentFrontCameraHighlight.text
        case .documentBack:
            hintText = FacialBiometricLocalizable.documentBackCameraTitle.text
            highlightedText = FacialBiometricLocalizable.documentBackCameraHighlight.text
        default:
            return
        }

        let attributedString = highlight(string: hintText, at: highlightedText)
        viewController?.displayCameraInfo(maskType: step.type, hintText: attributedString)
    }
    
    func configureCameraAuthorized() {
        viewController?.displayAuthorizedCameraAccess()
    }
    
    func configureCameraDenied() {
        viewController?.displayDeniedCameraAccess()
    }
    
    func close() {
        coordinator.perform(action: .close)
    }
    
    func handleCaptureError(error: Error?) {
        viewController?.displayAlert(with: error)
    }
    
    func showPreview(with previewImage: UIImage, currentStep: IdentityValidationStep) {
        coordinator.perform(action: .preview(image: previewImage, currentStep: currentStep))
    }
}

private extension DocumentValidationCameraPresenter {
    func highlight(string: String, at substring: String) -> NSMutableAttributedString {
        let attributedText = NSMutableAttributedString(string: string)
        let attributedRange = (attributedText.string as NSString).range(of: substring)

        attributedText.addAttributes(
            [.font: Typography.bodyPrimary(.highlight).font()],
            range: attributedRange
        )

        return attributedText
    }
}
