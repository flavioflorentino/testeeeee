import AnalyticsModule

protocol DocumentValidationCameraViewModelInputs: AnyObject {
    func loadCameraInfo()
    func checkCameraPermission()
    func handleCaptureImageError(_ error: Error?)
    func handleCaptureImageSuccess(image: UIImage)
    func close()
}

final class DocumentValidationCameraViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: DocumentValidationCameraServicing
    private let presenter: DocumentValidationCameraPresenting
    private let step: IdentityValidationStep

    init(
        service: DocumentValidationCameraServicing,
        presenter: DocumentValidationCameraPresenting,
        step: IdentityValidationStep,
        dependencies: Dependencies
    ) {
        self.service = service
        self.presenter = presenter
        self.step = step
        self.dependencies = dependencies
    }
}

extension DocumentValidationCameraViewModel: DocumentValidationCameraViewModelInputs {
    func loadCameraInfo() {
        logEvent(action: .none)
        presenter.presentCameraInfo(forStep: step)
    }
    
    func checkCameraPermission() {
        service.checkCameraPermission { [weak self] status in
            if status == .authorized {
                self?.presenter.configureCameraAuthorized()
            } else {
                self?.presenter.configureCameraDenied()
            }
        }
    }
    
    func handleCaptureImageError(_ error: Error?) {
        presenter.handleCaptureError(error: error)
    }
    
    func handleCaptureImageSuccess(image: UIImage) {
        logEvent(action: .taken)
        presenter.showPreview(with: image, currentStep: step)
    }
    
    func close() {
        logEvent(action: .close)
        presenter.close()
    }
}

private extension DocumentValidationCameraViewModel {
    func logEvent(action: IdentityValidationEvent.ActionStatus) {
        switch step.type {
        case .documentFront:
            dependencies.analytics.log(IdentityValidationEvent.documentFront(action: action))
        case .documentBack:
            dependencies.analytics.log(IdentityValidationEvent.documentBack(action: action))
        default:
            break
        }
    }
}
