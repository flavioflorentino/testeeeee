import Foundation
import PermissionsKit

protocol DocumentValidationCameraServicing {
    func checkCameraPermission(completion: @escaping (_ permission: PermissionStatus) -> Void)
}

final class DocumentValidationCameraService: DocumentValidationCameraServicing {
    func checkCameraPermission(completion: @escaping (PermissionStatus) -> Void) {
        let permissionCamera: Permission = PPPermission.camera()
        permissionCamera.request { status in
            completion(status)
        }
    }
}
