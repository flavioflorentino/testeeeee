import UI

protocol DocumentCapturePreviewDisplay: AnyObject {
    func displayPreviewImage(_ image: UIImage)
    func configureBottomView(titleText: String, denialActionText: String, confirmActionText: String)
    func startLoading()
    func stopLoading()
    func displayError(_ message: String)
}

private extension DocumentCapturePreviewViewController.Layout {
    enum Size {
        static let closeButton: CGFloat = 32
    }
}

final class DocumentCapturePreviewViewController: ViewController<DocumentCapturePreviewViewModelInputs, UIView>, LoadingViewProtocol {
    fileprivate enum Layout { }

    private lazy var previewImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.Savings.iconRoundCloseGreen.image, for: .normal)
        button.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
        button.accessibilityLabel = FacialBiometricLocalizable.closepictureCaptureAccessibilityText.text
        return button
    }()

    private lazy var checkStatusBottomView: VerifyStatusView = {
        let checkView = VerifyStatusView()
        checkView.delegate = self
        return checkView
    }()

    private lazy var uploadErrorBottomView: UploadErrorView = {
        let errorView = UploadErrorView()
        errorView.delegate = self
        return errorView
    }()

    private lazy var messageErrorView = MessageErrorView()

    lazy var loadingView: LoadingView = {
        let loadingView = LoadingView()
        loadingView.text = FacialBiometricLocalizable.previewUploadText.text
        loadingView.setTextColor(Colors.white.lightColor)
        loadingView.backgroundColor = Colors.black.lightColor.withAlphaComponent(0.75)
        return loadingView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.configurePreview()
    }

    override func buildViewHierarchy() {
        view.addSubview(previewImageView)
        view.addSubview(closeButton)
        view.addSubview(checkStatusBottomView)
        view.addSubview(uploadErrorBottomView)
        view.addSubview(messageErrorView)
    }

    override func setupConstraints() {
        previewImageView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        closeButton.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.trailing.equalToSuperview().inset(Spacing.base03)
            $0.size.equalTo(Layout.Size.closeButton)
        }

        checkStatusBottomView.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview()
        }

        uploadErrorBottomView.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview()
        }

        messageErrorView.snp.makeConstraints {
            $0.top.equalTo(closeButton.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.trailing.equalTo(closeButton.snp.trailing)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        setState(error: false)
    }

    private func setState(error: Bool) {
        closeButton.isHidden = !error
        uploadErrorBottomView.isHidden = !error
        messageErrorView.isHidden = !error
        checkStatusBottomView.isHidden = error
    }
}

@objc
private extension DocumentCapturePreviewViewController {
    func didTapClose() {
        viewModel.handleClose()
    }
}

extension DocumentCapturePreviewViewController: VerifyStatusViewDelegate {
    func didTouchConfirmAction() {
        viewModel.handleAcceptPicture()
    }

    func didTouchDenialAction() {
        viewModel.handleTakeAnotherPicture()
    }
}

extension DocumentCapturePreviewViewController: DocumentCapturePreviewDisplay {
    func displayPreviewImage(_ image: UIImage) {
        previewImageView.image = image
    }

    func configureBottomView(titleText: String, denialActionText: String, confirmActionText: String) {
        checkStatusBottomView.configureWith(
            infoText: titleText,
            denialButtonText: denialActionText,
            confirmButtonText: confirmActionText
        )
    }

    func displayError(_ message: String) {
        setState(error: true)
        messageErrorView.setErrorMessage(message)
    }

    func startLoading() {
        startLoadingView()
        setState(error: false)
        checkStatusBottomView.isHidden = true
    }

    func stopLoading() {
        stopLoadingView()
    }
}

extension DocumentCapturePreviewViewController: UploadErrorViewDelegate {
    func actionButtonTapped() {
        viewModel.handleRetry()
    }
}
