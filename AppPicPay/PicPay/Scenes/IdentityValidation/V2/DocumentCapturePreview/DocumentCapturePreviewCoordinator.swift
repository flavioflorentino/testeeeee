import UIKit

enum DocumentCapturePreviewAction {
    case close
    case takeAnotherPicture
    case nextStep(step: IdentityValidationStep)
}

protocol DocumentCapturePreviewCoordinating: AnyObject {
    func perform(action: DocumentCapturePreviewAction)
}

final class DocumentCapturePreviewCoordinator {
    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
}

// MARK: - DocumentCapturePreviewCoordinating
extension DocumentCapturePreviewCoordinator: DocumentCapturePreviewCoordinating {
    func perform(action: DocumentCapturePreviewAction) {
        switch action {
        case .close:
            navigationController.dismiss(animated: true)

        case .takeAnotherPicture:
            navigationController.popViewController(animated: true)

        case .nextStep(let step):
            navigationController.dismiss(animated: true) {
                NotificationCenter.default.post(name: .didSendDocument, object: step)
            }
        }
    }
}
