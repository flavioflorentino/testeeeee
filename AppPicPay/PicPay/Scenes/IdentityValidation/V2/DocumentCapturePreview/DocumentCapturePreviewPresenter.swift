import Foundation

protocol DocumentCapturePreviewPresenting: AnyObject {
    var viewController: DocumentCapturePreviewDisplay? { get set }
    func previewImage(_ image: UIImage, for step: IdentityValidationStep)
    func handleClose()
    func handleTakeAnotherPicture()
    func setLoadingState(_ loading: Bool)
    func displayError(message: String)
    func nextStep(_ step: IdentityValidationStep)
}

final class DocumentCapturePreviewPresenter {
    private typealias Localizable = FacialBiometricLocalizable

    private let coordinator: DocumentCapturePreviewCoordinating
    weak var viewController: DocumentCapturePreviewDisplay?

    init(coordinator: DocumentCapturePreviewCoordinating) {
        self.coordinator = coordinator
    }
}

extension DocumentCapturePreviewPresenter: DocumentCapturePreviewPresenting {
    func previewImage(_ image: UIImage, for step: IdentityValidationStep) {
        viewController?.displayPreviewImage(image)
        viewController?.configureBottomView(
            titleText: getTitleText(for: step.type),
            denialActionText: Localizable.previewTakeAnother.text,
            confirmActionText: Localizable.documentSendPicture.text
        )
    }

    func handleClose() {
        coordinator.perform(action: .close)
    }

    func handleTakeAnotherPicture() {
        coordinator.perform(action: .takeAnotherPicture)
    }

    func setLoadingState(_ loading: Bool) {
        if loading {
            self.viewController?.startLoading()
        } else {
            self.viewController?.stopLoading()
        }
    }

    func displayError(message: String) {
        self.viewController?.displayError(message)
    }

    func nextStep(_ step: IdentityValidationStep) {
        self.coordinator.perform(action: .nextStep(step: step))
    }
}

private extension DocumentCapturePreviewPresenter {
    func getTitleText(for stepType: IdentityValidationStep.StepType) -> String {
        if stepType == .documentFront {
            return Localizable.documentCaptureFrontTitle.text
        }
        return Localizable.documentCaptureBackTitle.text
    }
}
