import Core
import Foundation

protocol DocumentCapturePreviewServicing {
    func uploadImage(imageData: Data, completion: @escaping (PicPayResult<IdentityValidationStep>) -> Void)
}

final class DocumentCapturePreviewService: BaseApi, DocumentCapturePreviewServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    func uploadImage(imageData: Data, completion: @escaping (PicPayResult<IdentityValidationStep>) -> Void) {
        let files: [FileUpload] = [FileUpload(key: "image", data: imageData, fileName: "imagem.jpg", mimeType: "image/jpg")]

        requestManager.apiUpload(
            WebServiceInterface.apiEndpoint(kWsUrlIdentityValidationStepImage),
            method: .post,
            files: files,
            uploadProgress: nil
        ) { [weak self] response in
            self?.dependencies.mainQueue.async {
                completion(response)
            }
        }
    }
}
