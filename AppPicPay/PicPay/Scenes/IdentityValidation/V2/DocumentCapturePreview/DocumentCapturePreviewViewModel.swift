import AnalyticsModule

protocol DocumentCapturePreviewViewModelInputs: AnyObject {
    func configurePreview()
    func handleClose()
    func handleTakeAnotherPicture()
    func handleAcceptPicture()
    func handleRetry()
}

final class DocumentCapturePreviewViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: DocumentCapturePreviewServicing
    private let presenter: DocumentCapturePreviewPresenting
    private let step: IdentityValidationStep
    private let previewImage: UIImage

    private let imageSize = CGSize(width: 720, height: 1_280)
    private let imageQuality: CGFloat = 1

    init(service: DocumentCapturePreviewServicing, presenter: DocumentCapturePreviewPresenting, step: IdentityValidationStep, previewImage: UIImage, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.step = step
        self.previewImage = previewImage
        self.dependencies = dependencies
    }
}

// MARK: - DocumentCapturePreviewViewModelInputs
extension DocumentCapturePreviewViewModel: DocumentCapturePreviewViewModelInputs {
    func configurePreview() {
        logEvent(.didLoad)
        presenter.previewImage(previewImage, for: step)
    }

    func handleClose() {
        presenter.handleClose()
    }

    func handleTakeAnotherPicture() {
        logEvent(.takeAnother)
        presenter.handleTakeAnotherPicture()
    }

    func handleAcceptPicture() {
        guard let imageData = prepareImageForUpload() else {
            logEvent(.error)
            presenter.displayError(message: FacialBiometricLocalizable.previewUploadError.text)
            return
        }

        presenter.setLoadingState(true)

        service.uploadImage(imageData: imageData) { [weak self] result in
            self?.handleUploadResult(result)
        }
    }

    func handleRetry() {
        handleAcceptPicture()
    }
}

private extension DocumentCapturePreviewViewModel {
    func prepareImageForUpload() -> Data? {
        guard
            let imageForUpload = previewImage.resizeImage(targetSize: imageSize),
            let imageData = imageForUpload.jpegData(compressionQuality: imageQuality)
            else {
                return nil
        }
        return imageData
    }

    func handleUploadResult(_ result: PicPayResult<IdentityValidationStep>) {
        presenter.setLoadingState(false)

        switch result {
        case .success(let step):
            logEvent(.success)
            presenter.nextStep(step)
        case .failure(let error):
            logEvent(.error)
            presenter.displayError(message: error.message)
        }
    }
}

private extension DocumentCapturePreviewViewModel {
    enum AnalyticsEvent {
        case didLoad
        case success
        case takeAnother
        case error
    }

    func logEvent(_ event: AnalyticsEvent) {
        switch event {
        case .didLoad:
            logStatusForCurrentStep(action: .none)
        case .success:
            logStatusForCurrentStep(action: .finished)
        case .takeAnother:
            logStatusForCurrentStep(action: .drop)
        case .error:
            dependencies.analytics.log(IdentityValidationEvent.documentError)
        }
    }

    func logStatusForCurrentStep(action: IdentityValidationEvent.ActionStatus) {
        switch step.type {
        case .documentFront:
            dependencies.analytics.log(IdentityValidationEvent.sendDocumentFront(action: action))
        case .documentBack:
            dependencies.analytics.log(IdentityValidationEvent.sendDocumentBack(action: action))
        default:
            break
        }
    }
}
