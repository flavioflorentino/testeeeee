import Foundation

enum DocumentCapturePreviewFactory {
    static func make(navigationController: UINavigationController, previewImage: UIImage, step: IdentityValidationStep) -> DocumentCapturePreviewViewController {
        let container = DependencyContainer()
        let service = DocumentCapturePreviewService(dependencies: container)
        let coordinator = DocumentCapturePreviewCoordinator(navigationController: navigationController)
        let presenter = DocumentCapturePreviewPresenter(coordinator: coordinator)
        let viewModel = DocumentCapturePreviewViewModel(service: service, presenter: presenter, step: step, previewImage: previewImage, dependencies: container)
        let viewController = DocumentCapturePreviewViewController(viewModel: viewModel)

        presenter.viewController = viewController

        return viewController
    }
}
