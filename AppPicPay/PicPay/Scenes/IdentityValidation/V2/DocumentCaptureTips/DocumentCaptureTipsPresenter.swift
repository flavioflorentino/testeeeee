import Foundation

protocol DocumentCaptureTipsPresenting: AnyObject {
    var viewController: DocumentCaptureTipsDisplay? { get set }
    func configureViews(for step: IdentityValidationStep)
    func handleContinue(step: IdentityValidationStep)
    func handleNextStep(step: IdentityValidationStep)
    func handleClose()
}

final class DocumentCaptureTipsPresenter: DocumentCaptureTipsPresenting {
    private let coordinator: DocumentCaptureTipsCoordinating
    weak var viewController: DocumentCaptureTipsDisplay?
    
    init(coordinator: DocumentCaptureTipsCoordinating) {
        self.coordinator = coordinator
    }
    
    func configureViews(for step: IdentityValidationStep) {
        if case .selfie = step.type {
            viewController?.displayTips(.selfie)
            return
        }
        
        viewController?.displayTips(.document)
    }

    func handleContinue(step: IdentityValidationStep) {
        coordinator.perform(action: .presentCamera(step: step))
    }

    func handleNextStep(step: IdentityValidationStep) {
        coordinator.perform(action: .nextStep(step: step))
    }

    func handleClose() {
        coordinator.perform(action: .close)
    }
}
