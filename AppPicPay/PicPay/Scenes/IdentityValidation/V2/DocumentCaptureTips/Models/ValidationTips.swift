import Foundation

enum ValidationTips {
    case document
    case selfie
    
    struct Tip {
        let image: UIImage
        let text: String
        
        init(asset: ImageAsset, localizableString: DocumentCaptureLocalizable) {
            image = asset.image
            text = localizableString.text
        }
    }
    
    var title: String {
        switch self {
        case .document:
            return DocumentCaptureLocalizable.documentsTipsTitle.text
        case .selfie:
            return DocumentCaptureLocalizable.selfieTipsTitle.text
        }
    }
    
    var tipList: [Tip] {
        let asset = Assets.IdentityValidation.self
        switch self {
        case .document:
            return [
                Tip(asset: asset.documentPhone, localizableString: .documentsFirstTip),
                Tip(asset: asset.documentPhoto, localizableString: .documentsSecondTip),
                Tip(asset: asset.documentSearch, localizableString: .documentsThirdTip)
            ]
        case .selfie:
            return [
                Tip(asset: asset.selfieLight, localizableString: .selfieFirstTip),
                Tip(asset: asset.selfieAccessory, localizableString: .selfieSecondTip),
                Tip(asset: asset.selfieFace, localizableString: .selfieThirdTip)
            ]
        }
    }
}
