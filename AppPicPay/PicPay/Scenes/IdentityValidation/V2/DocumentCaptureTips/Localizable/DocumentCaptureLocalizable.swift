import Foundation

enum DocumentCaptureLocalizable: String, Localizable {
    // Documents
    case documentsTipsTitle
    case documentsFirstTip
    case documentsSecondTip
    case documentsThirdTip
    
    // Selfie
    case selfieTipsTitle
    case selfieFirstTip
    case selfieSecondTip
    case selfieThirdTip
    
    // Other
    case tipsContinueButton
    case tipsLaterButton
    
    var key: String {
       rawValue
    }
    var file: LocalizableFile {
       .documentCapture
    }
}
