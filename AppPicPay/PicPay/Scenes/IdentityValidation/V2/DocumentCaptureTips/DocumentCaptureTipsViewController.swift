import UI
import UIKit

extension DocumentCaptureTipsViewController.Layout {
    enum Size {
        static let buttonHeight: CGFloat = 48
    }
}

final class DocumentCaptureTipsViewController: ViewController<IdentityValidationTipsViewModelInputs, UIView> {
    fileprivate enum Layout { }

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = DocumentCaptureLocalizable.documentsTipsTitle.text
        label.numberOfLines = 0
        return label
    }()

    private lazy var tipsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base05
        return stackView
    }()

    private lazy var continueButton: UIPPButton = {
        let button = UIPPButton(type: .system)
        button.setTitle(DocumentCaptureLocalizable.tipsContinueButton.text, for: .normal)
        button.normalBackgrounColor = Colors.branding400.color
        button.highlightedBackgrounColor = Colors.branding400.color.withAlphaComponent(0.6)
        button.normalTitleColor = Colors.white.lightColor
        button.highlightedTitleColor = Colors.white.lightColor
        button.cornerRadius = Spacing.base03
        button.titleLabel?.font = Typography.bodyPrimary(.highlight).font()
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(didTapContinue), for: .touchUpInside)
        return button
    }()

    private lazy var laterButton: UnderlinedButton = {
        let button = UnderlinedButton()
        button.setTitle(DocumentCaptureLocalizable.tipsLaterButton.text, for: .normal)
        button.updateColor(
            Colors.branding400.color,
            highlightedColor: Colors.branding400.color.withAlphaComponent(0.6),
            font: Typography.bodyPrimary(.highlight).font()
        )
        button.addTarget(self, action: #selector(didTapLater), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.configureViews()
    }

    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(tipsStackView)
        view.addSubview(continueButton)
        view.addSubview(laterButton)
    }

    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base04)
            $0.trailing.equalToSuperview().offset(-Spacing.base04)
            $0.bottom.equalTo(tipsStackView.snp.top).offset(-Spacing.base06)
        }

        tipsStackView.snp.makeConstraints {
            $0.leading.equalTo(titleLabel.snp.leading)
            $0.trailing.equalTo(titleLabel.snp.trailing)
            $0.centerY.equalToSuperview().offset(-Spacing.base05)
        }

        continueButton.snp.makeConstraints {
            $0.top.equalTo(tipsStackView.snp.bottom).offset(Spacing.base06)
            $0.leading.equalTo(tipsStackView.snp.leading)
            $0.trailing.equalTo(tipsStackView.snp.trailing)
            $0.height.equalTo(Layout.Size.buttonHeight)
        }

        laterButton.snp.makeConstraints {
            $0.top.equalTo(continueButton.snp.bottom).offset(Spacing.base01)
            $0.centerX.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.white.color
    }

    override func configureStyles() {
        titleLabel
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
    }
}

// MARK: UI Target Methods
@objc
private extension DocumentCaptureTipsViewController {
    func didTapContinue() {
        viewModel.handleContinue()
    }

    func didTapLater() {
        viewModel.handleClose()
    }
}

extension DocumentCaptureTipsViewController: DocumentCaptureTipsDisplay {
    func displayTips(_ tips: ValidationTips) {
        titleLabel.text = tips.title
        tips.tipList.forEach {
            tipsStackView.addArrangedSubview(DocumentCaptureImageTextView(image: $0.image, text: $0.text))
        }
    }
}
