import UIKit

enum DocumentCaptureTipsAction {
    case presentCamera(step: IdentityValidationStep)
    case nextStep(step: IdentityValidationStep)
    case close
}

protocol DocumentCaptureTipsCoordinating: AnyObject {
    var viewModel: DocumentCaptureTipsViewModelInputs? { get set }
    func perform(action: DocumentCaptureTipsAction)
}

final class DocumentCaptureTipsCoordinator: DocumentCaptureTipsCoordinating {
    weak var viewModel: DocumentCaptureTipsViewModelInputs?
    private weak var coordinatorDelegate: IdentityValidationCoordinating?
    private let navigationController: UINavigationController

    init(coordinatorDelegate: IdentityValidationCoordinating?, navigationController: UINavigationController) {
        self.coordinatorDelegate = coordinatorDelegate
        self.navigationController = navigationController
    }

    func perform(action: DocumentCaptureTipsAction) {
        switch action {
        case .presentCamera(let step):
            presentCamera(step: step)
        case .nextStep(let step):
            handleNextStep(step)
        case .close:
            coordinatorDelegate?.dismiss()
        }
    }
}

private extension DocumentCaptureTipsCoordinator {
    func presentCamera(step: IdentityValidationStep) {
        let navController = UINavigationController()
        let cameraController = DocumentValidationCameraFactory.make(navigationController: navController, step: step)
        navController.pushViewController(cameraController, animated: false)
        navController.modalPresentationStyle = .fullScreen
        navigationController.present(navController, animated: true)
    }

    func handleNextStep(_ step: IdentityValidationStep) {
        if step.type == .documentBack {
            viewModel?.addDocumentObserver()
            presentCamera(step: step)
        } else {
            coordinatorDelegate?.pushNextController(for: step)
        }
    }
}
