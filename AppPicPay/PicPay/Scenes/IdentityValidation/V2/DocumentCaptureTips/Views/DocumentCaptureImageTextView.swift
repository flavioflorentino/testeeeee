import UI

extension DocumentCaptureImageTextView.Layout {
    enum Size {
        static let image: CGFloat = 40
    }
}

final class DocumentCaptureImageTextView: UIView, ViewConfiguration {
    fileprivate enum Layout { }

    private lazy var iconImageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        return image
    }()

    private lazy var tipLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()

    init(frame: CGRect = .zero, image: UIImage, text: String) {
        super.init(frame: frame)
        buildLayout()
        setupViews(image: image, text: text)
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func buildViewHierarchy() {
        addSubview(iconImageView)
        addSubview(tipLabel)
    }

    func setupConstraints() {
        iconImageView.snp.makeConstraints {
            $0.leading.equalToSuperview()
            $0.centerY.equalToSuperview()
            $0.size.equalTo(Layout.Size.image)
        }

        tipLabel.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
    }

    func configureStyles() {
        tipLabel
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
    }

    private func setupViews(image: UIImage, text: String) {
        iconImageView.image = image
        tipLabel.text = text
    }
}
