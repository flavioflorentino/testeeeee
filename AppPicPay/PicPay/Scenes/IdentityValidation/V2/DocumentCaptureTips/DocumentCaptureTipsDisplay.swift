import Foundation

protocol DocumentCaptureTipsDisplay: AnyObject {
    func displayTips(_ tips: ValidationTips)
}
