import AnalyticsModule
import Foundation

protocol DocumentCaptureTipsViewModelInputs: IdentityValidationTipsViewModelInputs {
    func addDocumentObserver()
}

final class DocumentCaptureTipsViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let presenter: DocumentCaptureTipsPresenting
    private let step: IdentityValidationStep

    init(presenter: DocumentCaptureTipsPresenting, step: IdentityValidationStep, dependencies: Dependencies) {
        self.presenter = presenter
        self.step = step
        self.dependencies = dependencies
    }
}

extension DocumentCaptureTipsViewModel: DocumentCaptureTipsViewModelInputs {
    func configureViews() {
        addDocumentObserver()
        dependencies.analytics.log(IdentityValidationEvent.documentTips(action: .none))
        presenter.configureViews(for: step)
    }
    
    func handleContinue() {
        presenter.handleContinue(step: step)
    }

    func handleClose() {
        presenter.handleClose()
    }

    func addDocumentObserver() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didSendDocument(_:)),
            name: .didSendDocument,
            object: nil
        )
    }
}

@objc
private extension DocumentCaptureTipsViewModel {
    func didSendDocument(_ notification: Notification) {
        NotificationCenter.default.removeObserver(self)

        guard let step = notification.object as? IdentityValidationStep else {
            return
        }

        presenter.handleNextStep(step: step)
    }
}
