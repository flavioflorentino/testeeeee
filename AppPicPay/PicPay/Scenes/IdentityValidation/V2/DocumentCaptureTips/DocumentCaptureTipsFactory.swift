import Foundation

enum DocumentCaptureTipsFactory {
    static func make(coordinatorDelegate: IdentityValidationCoordinating?, step: IdentityValidationStep, navigationController: UINavigationController) -> DocumentCaptureTipsViewController {
        let container = DependencyContainer()
        let coordinator: DocumentCaptureTipsCoordinating = DocumentCaptureTipsCoordinator(
            coordinatorDelegate: coordinatorDelegate,
            navigationController: navigationController
        )
        let presenter: DocumentCaptureTipsPresenting = DocumentCaptureTipsPresenter(coordinator: coordinator)
        let viewModel = DocumentCaptureTipsViewModel(presenter: presenter, step: step, dependencies: container)
        let viewController = DocumentCaptureTipsViewController(viewModel: viewModel)

        presenter.viewController = viewController
        coordinator.viewModel = viewModel
        return viewController
    }
}
