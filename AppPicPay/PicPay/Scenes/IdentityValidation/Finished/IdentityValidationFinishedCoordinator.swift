import UIKit

protocol IdentityValidationFinishedCoordinating: AnyObject {
    func finish()
}

final class IdentityValidationFinishedCoordinator: IdentityValidationFinishedCoordinating {
    private weak var coordinatorDelegate: IdentityValidationCoordinating?
    
    init(coordinatorDelegate: IdentityValidationCoordinating?) {
        self.coordinatorDelegate = coordinatorDelegate
    }
    
    func finish() {
        coordinatorDelegate?.dismiss()
    }
}
