import UIKit

protocol IdentityValidationFinishedDisplayable: AnyObject {
    func presentStepInfo(image: UIImage, title: String, description: String)
}
