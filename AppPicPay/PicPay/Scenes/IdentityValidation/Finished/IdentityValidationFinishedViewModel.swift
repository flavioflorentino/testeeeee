protocol IdentityValidationFinishedViewModelInputs: AnyObject {
    func loadStepInfo()
    func closeFlow()
}

final class IdentityValidationFinishedViewModel {
    private let step: IdentityValidationStep
    private let presenter: IdentityValidationFinishedPresenting

    init(step: IdentityValidationStep, presenter: IdentityValidationFinishedPresenting) {
        self.step = step
        self.presenter = presenter
    }
}

extension IdentityValidationFinishedViewModel: IdentityValidationFinishedViewModelInputs {
    func loadStepInfo() {
        presenter.handleStepInfo(step: step)
    }
    
    func closeFlow() {
        presenter.handleCloseFlow()
    }
}
