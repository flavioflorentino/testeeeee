import UI
import UIKit

final class IdentityValidationFinishedViewController: ViewController<IdentityValidationFinishedViewModelInputs, IdentityValidationStepDescriptionView> {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.hidesBackButton = true

        rootView.setupView(
            image: #imageLiteral(resourceName: "ilu_identity_validation_success"),
            title: IdentityValidationLocalizable.finishedStepDefautTitle.text,
            subtitle: IdentityValidationLocalizable.finishedStepDefautMessage.text,
            primaryButtonTitle: IdentityValidationLocalizable.gotIt.text
        )
        rootView.delegate = self
        
        viewModel.loadStepInfo()
    }
}

// MARK: Presenter Outputs
extension IdentityValidationFinishedViewController: IdentityValidationFinishedDisplayable {
    func presentStepInfo(image: UIImage, title: String, description: String) {
        rootView.update(image: image, title: title, subtitle: description)
    }
}

// MARK: - View Selecting

extension IdentityValidationFinishedViewController: IdentityValidationStepDescriptionSelecting {
    func didTapPrimaryButton() {
        viewModel.closeFlow()
    }
}
