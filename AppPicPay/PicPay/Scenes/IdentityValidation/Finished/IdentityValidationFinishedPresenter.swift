import AnalyticsModule

protocol IdentityValidationFinishedPresenting: AnyObject {
    var coordinator: IdentityValidationFinishedCoordinating? { get set }
    var viewController: IdentityValidationFinishedDisplayable? { get set }
    
    func handleStepInfo(step: IdentityValidationStep)
    func handleCloseFlow()
}

final class IdentityValidationFinishedPresenter: IdentityValidationFinishedPresenting {
    var coordinator: IdentityValidationFinishedCoordinating?
    weak var viewController: IdentityValidationFinishedDisplayable?

    init(coordinator: IdentityValidationFinishedCoordinating) {
        self.coordinator = coordinator
    }
    
    func handleStepInfo(step: IdentityValidationStep) {
        let image: UIImage = step.type == .processing ? #imageLiteral(resourceName: "ilu_identity_validation_proc") : #imageLiteral(resourceName: "ilu_identity_validation_success")
        viewController?.presentStepInfo(image: image, title: step.title, description: step.text)
        
        let statusText = step.type == .processing ? IdentityValidationLocalizable.underAnalysis.text : IdentityValidationLocalizable.validated.text
        Analytics.shared.log(IdentityValidationEvent.finished(statusText: statusText))
    }
    
    func handleCloseFlow() {
        coordinator?.finish()
    }
}
