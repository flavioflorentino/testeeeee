enum IdentityValidationFinishedFactory {
    static func make(coordinatorDelegate: IdentityValidationCoordinating?, step: IdentityValidationStep) -> IdentityValidationFinishedViewController {
        let coordinator = IdentityValidationFinishedCoordinator(coordinatorDelegate: coordinatorDelegate)
        let presenter: IdentityValidationFinishedPresenting = IdentityValidationFinishedPresenter(coordinator: coordinator)
        let viewModel = IdentityValidationFinishedViewModel(step: step, presenter: presenter)
        let viewController = IdentityValidationFinishedViewController(viewModel: viewModel)

        presenter.viewController = viewController

        return viewController
    }
}
