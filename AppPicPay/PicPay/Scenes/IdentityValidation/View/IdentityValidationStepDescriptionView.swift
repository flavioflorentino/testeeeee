import UI
import UIKit

protocol IdentityValidationStepDescriptionSelecting: AnyObject {
    func didTapPrimaryButton()
    func didTapSecondaryButton()
}

extension IdentityValidationStepDescriptionSelecting {
    func didTapSecondaryButton() { }
}

final class IdentityValidationStepDescriptionView: UIView {
    private lazy var descriptionView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private lazy var descriptionStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 20
        return stackView
    }()
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .center
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        label.textColor = Palette.ppColorGrayscale600.color
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.textColor = Palette.ppColorGrayscale400.color(withCustomDark: .ppColorGrayscale200)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    private lazy var buttonsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.alignment = .center
        return stackView
    }()
    lazy var primaryButton: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = 24
        button.configure(with: Button(title: "", type: .cta))
        button.addTarget(self, action: #selector(primaryButtonTapped), for: .touchUpInside)
        return button
    }()
    lazy var secondaryButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        button.addTarget(self, action: #selector(secondaryButtonTapped), for: .touchUpInside)
        return button
    }()
    
    weak var delegate: IdentityValidationStepDescriptionSelecting?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView(
        image: UIImage,
        title: String,
        subtitle: String,
        primaryButtonTitle: String,
        secondaryButtonTitle: String? = nil
    ) {
        imageView.image = image
        titleLabel.text = title
        subtitleLabel.text = subtitle
        primaryButton.setTitle(primaryButtonTitle, for: .normal)
        
        guard let secondaryButtonTitle = secondaryButtonTitle else {
            secondaryButton.isHidden = true
            return
        }
        
        secondaryButton.setTitle(secondaryButtonTitle, for: .normal)
    }
    
    func update(image: UIImage? = nil, title: String, subtitle: String) {
        imageView.image = image
        titleLabel.text = title
        subtitleLabel.text = subtitle
    }
    
    func startLoading() {
        primaryButton.startLoadingAnimating()
    }
    
    func stopLoading() {
        primaryButton.stopLoadingAnimating()
    }
}

// MARK: - Action methods

private extension IdentityValidationStepDescriptionView {
    @objc
    func primaryButtonTapped() {
        delegate?.didTapPrimaryButton()
    }
    
    @objc
    func secondaryButtonTapped() {
        delegate?.didTapSecondaryButton()
    }
}

// MARK: - ViewCode

extension IdentityValidationStepDescriptionView: ViewConfiguration {
    func buildViewHierarchy() {
        descriptionStackView.addArrangedSubview(imageView)
        descriptionStackView.addArrangedSubview(titleLabel)
        descriptionStackView.addArrangedSubview(subtitleLabel)
        descriptionView.addSubview(descriptionStackView)
        addSubview(descriptionView)
        
        buttonsStackView.addArrangedSubview(primaryButton)
        buttonsStackView.addArrangedSubview(secondaryButton)
        addSubview(buttonsStackView)
    }
    
    func setupConstraints() {
        var topAnchorConstraint = topAnchor
        var bottomAnchorConstraint = bottomAnchor
        var bottomConstant: CGFloat = -20
        if #available(iOS 11.0, *) {
            topAnchorConstraint = safeAreaLayoutGuide.topAnchor
            bottomAnchorConstraint = safeAreaLayoutGuide.bottomAnchor
            if let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom, bottom > 0 {
                bottomConstant = 0
            }
        }
        
        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalToConstant: 120),
            
            descriptionView.topAnchor.constraint(equalTo: topAnchorConstraint),
            descriptionView.bottomAnchor.constraint(equalTo: buttonsStackView.topAnchor),
            descriptionView.trailingAnchor.constraint(equalTo: trailingAnchor),
            descriptionView.leadingAnchor.constraint(equalTo: leadingAnchor),
            
            descriptionStackView.centerYAnchor.constraint(equalTo: descriptionView.centerYAnchor),
            descriptionStackView.trailingAnchor.constraint(equalTo: descriptionView.trailingAnchor, constant: -20),
            descriptionStackView.leadingAnchor.constraint(equalTo: descriptionView.leadingAnchor, constant: 20),
            
            buttonsStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            buttonsStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            buttonsStackView.bottomAnchor.constraint(equalTo: bottomAnchorConstraint, constant: bottomConstant),
            
            primaryButton.heightAnchor.constraint(equalToConstant: 48),
            primaryButton.leadingAnchor.constraint(equalTo: buttonsStackView.leadingAnchor),
            primaryButton.trailingAnchor.constraint(equalTo: buttonsStackView.trailingAnchor),
            
            secondaryButton.heightAnchor.constraint(equalToConstant: 48),
            secondaryButton.leadingAnchor.constraint(equalTo: buttonsStackView.leadingAnchor),
            secondaryButton.trailingAnchor.constraint(equalTo: buttonsStackView.trailingAnchor)
        ])
    }
    
    func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
    }
}
