import UIKit

struct IdentityValidationRouterModel {
    let step: IdentityValidationStep
    let coordinator: IdentityValidationCoordinating?
    let originFlow: IdentityValidationOriginFlow
    let navigation: UINavigationController
    let identityFlag: Bool
}

protocol IdentityValidationRouting {
    func getController(for model: IdentityValidationRouterModel) -> UIViewController?
}

struct IdentityValidationRouter: IdentityValidationRouting {
    func getController(for model: IdentityValidationRouterModel) -> UIViewController? {
        switch model.step.type {
        case .initial:
            return IdentityValidationInitialFactory.make(
                coordinatorDelegate: model.coordinator, 
                originFlow: model.originFlow,
                step: model.step,
                navigationController: model.navigation
            )

        case .questions:
            return getQuestionsController(
                step: model.step, 
                delegate: model.coordinator, 
                navigation: model.navigation
            )

        case .selfie:
            return getSelfieController(
                step: model.step, 
                delegate: model.coordinator, 
                navigation: model.navigation, 
                identityFlag: model.identityFlag
            )

        case
            .documentFront,
            .documentBack:
            return getDocumentController(
                step: model.step, 
                delegate: model.coordinator, 
                navigation: model.navigation, 
                identityFlag: model.identityFlag
            )

        case
            .processing,
            .done:
            return getStatusController(step: model.step, delegate: model.coordinator, identityFlag: model.identityFlag)
        }
    }
}

private extension IdentityValidationRouter {
    func getQuestionsController(
        step: IdentityValidationStep,
        delegate: IdentityValidationCoordinating?,
        navigation: UINavigationController
    ) -> UIViewController? {
        guard
            let questions = step.data as? [IdentityValidationQuestion],
            !questions.isEmpty
        else {
            return nil
        }

        let questionManager = IdentityValidationQuestionManager(questions: questions)
        return IdentityValidationQuestionsFactory.make(
            coordinatorDelegate: delegate,
            questionManager: questionManager,
            navigationController: navigation
        )
    }

    func getSelfieController(
        step: IdentityValidationStep,
        delegate: IdentityValidationCoordinating?,
        navigation: UINavigationController,
        identityFlag: Bool
    ) -> UIViewController {
        guard identityFlag else {
            return getPhotoController(step: step, delegate: delegate, navigation: navigation)
        }
        return FacialBiometryIntroFactory.make(coordinatorDelegate: delegate, navigationController: navigation)
    }

    func getDocumentController(
        step: IdentityValidationStep,
        delegate: IdentityValidationCoordinating?,
        navigation: UINavigationController,
        identityFlag: Bool
    ) -> UIViewController {
        guard identityFlag else {
            return getPhotoController(step: step, delegate: delegate, navigation: navigation)
        }
        return DocumentCaptureIntroFactory.make(coordinatorDelegate: delegate, step: step, navigationController: navigation)
    }

    func getPhotoController(
        step: IdentityValidationStep,
        delegate: IdentityValidationCoordinating?,
        navigation: UINavigationController
    ) -> UIViewController {
        IdentityValidationPhotoFactory.make(coordinatorDelegate: delegate, step: step, navigationController: navigation)
    }

    func getStatusController(step: IdentityValidationStep, delegate: IdentityValidationCoordinating?, identityFlag: Bool) -> UIViewController {
        guard identityFlag else {
            return IdentityValidationFinishedFactory.make(coordinatorDelegate: delegate, step: step)
        }
        return ValidationStatusFactory.make(coordinatorDelegate: delegate, step: step)
    }
}
