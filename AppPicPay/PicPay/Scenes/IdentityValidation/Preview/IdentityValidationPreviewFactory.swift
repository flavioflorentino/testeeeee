import UIKit

enum IdentityValidationPreviewFactory {
    static func make(image: UIImage, settings: IdentityValidationCameraViewSettings, navigationController: UINavigationController) -> IdentityValidationPreviewViewController {
        let coordinator: IdentityValidationPreviewCoordinating = IdentityValidationPreviewCoordinator(navigationController: navigationController)
        let presenter: IdentityValidationPreviewPresenting = IdentityValidationPreviewPresenter(coordinator: coordinator)
        let viewModel = IdentityValidationPreviewViewModel(previewImage: image, settings: settings, presenter: presenter)
        let viewController = IdentityValidationPreviewViewController.controllerFromStoryboard()
        viewController.viewModel = viewModel
        
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
