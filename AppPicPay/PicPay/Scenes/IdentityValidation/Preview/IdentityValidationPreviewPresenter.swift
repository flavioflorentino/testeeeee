import AnalyticsModule
import Core
import UIKit

protocol IdentityValidationPreviewPresenting: AnyObject {
    var coordinator: IdentityValidationPreviewCoordinating? { get set }
    var viewController: IdentityValidationPreviewDisplayable? { get set }
    
    func handlePreviewInfo(_ image: UIImage, confirmMessage: String, confirmButtonTitle: String)
    func handleConfirmPhoto(_ image: UIImage, photoType: IdentityValidationCameraViewSettings.MaskType)
    func handleTakeAnotherPhoto()
}

final class IdentityValidationPreviewPresenter: IdentityValidationPreviewPresenting {
    var coordinator: IdentityValidationPreviewCoordinating?
    weak var viewController: IdentityValidationPreviewDisplayable?

    init(coordinator: IdentityValidationPreviewCoordinating) {
        self.coordinator = coordinator
    }
    
    func handlePreviewInfo(_ image: UIImage, confirmMessage: String, confirmButtonTitle: String) {
        viewController?.presentPreviewInfo(image, confirmMessage: confirmMessage, confirmButtonTitle: confirmButtonTitle)
    }
    
    func handleConfirmPhoto(_ image: UIImage, photoType: IdentityValidationCameraViewSettings.MaskType) {
        switch photoType {
        case .selfie:
            Analytics.shared.log(IdentityValidationEvent.selfieSended)
        case .documentBack:
            Analytics.shared.log(IdentityValidationEvent.identityPhotosSended)
        default:
            break
        }
        
        coordinator?.perform(action: .usePhoto(image))
    }
    
    func handleTakeAnotherPhoto() {
        coordinator?.perform(action: .discardPhoto)
    }
}
