import UIKit

protocol IdentityValidationPreviewCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var navigationController: UINavigationController { get set }
    
    func perform(action: IdentityValidationAction)
}

final class IdentityValidationPreviewCoordinator: IdentityValidationPreviewCoordinating {
    weak var viewController: UIViewController?
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func perform(action: IdentityValidationAction) {
        switch action {
        case let .usePhoto(image):
            navigationController.dismiss(animated: true) {
                NotificationCenter.default.post(name: .didReceiveImage, object: image)
            }
            
        case .discardPhoto:
            navigationController.popViewController(animated: true)
            
        default:
            navigationController.dismiss(animated: true)
        }
    }
}
