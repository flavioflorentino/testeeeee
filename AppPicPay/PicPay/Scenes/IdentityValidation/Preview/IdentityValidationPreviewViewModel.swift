import UIKit

protocol IdentityValidationPreviewViewModelInputs: AnyObject {
    func loadPreviewInfo()
    func confirmPhoto()
    func takeAnotherPhoto()
}

final class IdentityValidationPreviewViewModel {
    private let previewImage: UIImage
    private let settings: IdentityValidationCameraViewSettings
    private let presenter: IdentityValidationPreviewPresenting

    init(previewImage: UIImage, settings: IdentityValidationCameraViewSettings, presenter: IdentityValidationPreviewPresenting) {
        self.previewImage = previewImage
        self.settings = settings
        self.presenter = presenter
    }
}

extension IdentityValidationPreviewViewModel: IdentityValidationPreviewViewModelInputs {
    func loadPreviewInfo() {
        presenter.handlePreviewInfo(previewImage, confirmMessage: settings.confirmMessage, confirmButtonTitle: settings.confirmButtonTitle)
    }
    
    func confirmPhoto() {
        presenter.handleConfirmPhoto(previewImage, photoType: settings.mask)
    }
    
    func takeAnotherPhoto() {
        presenter.handleTakeAnotherPhoto()
    }
}
