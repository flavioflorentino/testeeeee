import UIKit

protocol IdentityValidationPreviewDisplayable: AnyObject {
    func presentPreviewInfo(_ image: UIImage, confirmMessage: String, confirmButtonTitle: String)
}
