import UIKit

final class IdentityValidationPreviewViewController: PPBaseViewController {
    var viewModel: IdentityValidationPreviewViewModelInputs?
    
    override var prefersStatusBarHidden: Bool {
        true
    }
    
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var imageViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet private var imageViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet private var imageViewTopConstraint: NSLayoutConstraint!
    @IBOutlet private var imageViewTrailingConstraint: NSLayoutConstraint!

    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var confirmMessageLabel: UILabel!
    @IBOutlet private var confirmButton: UIPPButton!
    @IBOutlet private var confirmBackgroundView: UIView!
    
    // MARK: - Factory
    
    static func controllerFromStoryboard() -> IdentityValidationPreviewViewController {
        IdentityValidationPreviewViewController.controllerFromStoryboard(.identityValidation)
    }
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        
        let effect = UIBlurEffect(style: .dark)
        let effectView = UIVisualEffectView(effect: effect)
        confirmBackgroundView.addSubview(effectView)
        effectView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        viewModel?.loadPreviewInfo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        updateMinZoomScaleForSize(view.bounds.size)
    }
}
    
// MARK: - Internal Methods
    
private extension IdentityValidationPreviewViewController {
    @IBAction private func discardPhoto(_ sender: Any) {
        viewModel?.takeAnotherPhoto()
    }
    
    @IBAction private func usePhoto(_ sender: Any) {
        viewModel?.confirmPhoto()
    }
    
    private func updateMinZoomScaleForSize(_ size: CGSize) {
        let widthScale = size.width / imageView.bounds.width
        let heightScale = size.height / imageView.bounds.height
        let minScale = min(widthScale, heightScale)
        
        scrollView.minimumZoomScale = minScale
        scrollView.zoomScale = minScale
    }
    
    private func updateConstraintsForSize(_ size: CGSize) {
        let yOffset = max(0, (size.height - imageView.frame.height) / 2)
        imageViewTopConstraint.constant = yOffset
        imageViewBottomConstraint.constant = yOffset
        
        let xOffset = max(0, (size.width - imageView.frame.width) / 2)
        imageViewLeadingConstraint.constant = xOffset
        imageViewTrailingConstraint.constant = xOffset
        
        view.layoutIfNeeded()
    }
}

// MARK: - Presenter Output

extension IdentityValidationPreviewViewController: IdentityValidationPreviewDisplayable {
    func presentPreviewInfo(_ image: UIImage, confirmMessage: String, confirmButtonTitle: String) {
        imageView.image = image
        confirmMessageLabel.text = confirmMessage
        confirmButton.setTitle(confirmButtonTitle, for: .normal)
    }
}

// MARK: - UIScrollViewDelegate

extension IdentityValidationPreviewViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        imageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        updateConstraintsForSize(view.bounds.size)
    }
}
