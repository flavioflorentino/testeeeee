import UIKit

protocol IdentityValidationPhotoCoordinating: AnyObject {
    func presentCamera(for stepType: IdentityValidationStep.StepType)
    func nextViewController(for step: IdentityValidationStep)
    func dismiss()
}

final class IdentityValidationPhotoCoordinator: IdentityValidationPhotoCoordinating {
    private weak var coordinatorDelegate: IdentityValidationCoordinating?
    private let navigationController: UINavigationController
    
    init(coordinatorDelegate: IdentityValidationCoordinating?, navigationController: UINavigationController) {
        self.coordinatorDelegate = coordinatorDelegate
        self.navigationController = navigationController
    }

    func presentCamera(for stepType: IdentityValidationStep.StepType) {
        guard let cameraSettings = cameraSettings(for: stepType) else {
            dismiss()
            return
        }

        let navController = UINavigationController()
        let cameraController = IdentityValidationCameraFactory.make(settings: cameraSettings, navigationController: navController)
        navController.viewControllers = [cameraController]
        navController.modalPresentationStyle = .fullScreen
        navigationController.present(navController, animated: true)
    }

    func nextViewController(for step: IdentityValidationStep) {
        coordinatorDelegate?.pushNextController(for: step)
    }

    func dismiss() {
        coordinatorDelegate?.dismiss()
    }
}

private extension IdentityValidationPhotoCoordinator {
    func cameraSettings(for stepType: IdentityValidationStep.StepType) -> IdentityValidationCameraViewSettings? {
        switch stepType {
        case .selfie:
            return IdentityValidationCameraViewSettings(
                descriptionText: IdentityValidationLocalizable.selfieDescriptionText.text,
                takePictureButtonTitle: IdentityValidationLocalizable.takePicture.text,
                cameraDevice: .front,
                mask: .selfie,
                confirmButtonTitle: IdentityValidationLocalizable.chosePicture.text,
                confirmMessage: IdentityValidationLocalizable.selfieConfirmationMessage.text
            )

        case .documentBack:
            return IdentityValidationCameraViewSettings(
                descriptionText: IdentityValidationLocalizable.documentBackDescriptionText.text,
                takePictureButtonTitle: IdentityValidationLocalizable.takePicture.text,
                cameraDevice: .rear,
                mask: .documentBack,
                confirmButtonTitle: IdentityValidationLocalizable.isReadable.text,
                confirmMessage: IdentityValidationLocalizable.documentConfirmationMessage.text
            )

        case .documentFront:
            return IdentityValidationCameraViewSettings(
                descriptionText: IdentityValidationLocalizable.documentFrontDescriptionText.text,
                takePictureButtonTitle: IdentityValidationLocalizable.takePicture.text,
                cameraDevice: .rear,
                mask: .documentFront,
                confirmButtonTitle: IdentityValidationLocalizable.isReadable.text,
                confirmMessage: IdentityValidationLocalizable.documentConfirmationMessage.text
            )

        default:
            return nil
        }
    }
}
