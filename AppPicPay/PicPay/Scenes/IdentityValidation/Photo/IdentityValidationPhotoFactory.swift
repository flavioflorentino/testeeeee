enum IdentityValidationPhotoFactory {
    static func make(coordinatorDelegate: IdentityValidationCoordinating?, step: IdentityValidationStep, navigationController: UINavigationController) -> IdentityValidationPhotoViewController {
        let container = DependencyContainer()
        let coordinator: IdentityValidationPhotoCoordinating = IdentityValidationPhotoCoordinator(
            coordinatorDelegate: coordinatorDelegate,
            navigationController: navigationController
        )
        let service: IdentityValidationPhotoServicing = IdentityValidationPhotoService()
        let presenter: IdentityValidationPhotoPresenting = IdentityValidationPhotoPresenter(coordinator: coordinator, dependencies: container)
        let viewModel = IdentityValidationPhotoViewModel(step: step, service: service, presenter: presenter)
        let viewController = IdentityValidationPhotoViewController(viewModel: viewModel)

        presenter.viewController = viewController

        return viewController
    }
}
