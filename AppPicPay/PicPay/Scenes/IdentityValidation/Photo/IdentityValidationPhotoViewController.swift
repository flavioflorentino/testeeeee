import UI
import UIKit

final class IdentityValidationPhotoViewController: ViewController<IdentityValidationPhotoViewModelInputs, IdentityValidationStepDescriptionView> {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.hidesBackButton = true
        
        rootView.setupView(
            image: #imageLiteral(resourceName: "ilu_document_back"),
            title: IdentityValidationLocalizable.photoStepDefautTitle.text,
            subtitle: IdentityValidationLocalizable.photoStepDefautMessage.text,
            primaryButtonTitle: IdentityValidationLocalizable.takePicture.text,
            secondaryButtonTitle: IdentityValidationLocalizable.doItLater.text
        )
        rootView.delegate = self
        
        viewModel.loadStepInfo()
    }
}

// MARK: - Presenter Outputs
extension IdentityValidationPhotoViewController: IdentityValidationPhotoDisplayable {
    func presentStepInfo(image: UIImage, title: String, description: String) {
        rootView.update(image: image, title: title, subtitle: description)
    }
    
    func presentAlert(with error: PicPayError) {
        AlertMessage.showAlertWithError(error, controller: self)
    }
    
    func startLoading() {
        rootView.startLoading()
    }
    
    func stopLoading() {
        rootView.stopLoading()
    }
}

// MARK: - View Selecting

extension IdentityValidationPhotoViewController: IdentityValidationStepDescriptionSelecting {
    func didTapPrimaryButton() {
        viewModel.openCamera()
    }
    
    func didTapSecondaryButton() {
        viewModel.closeFlow()
    }
}
