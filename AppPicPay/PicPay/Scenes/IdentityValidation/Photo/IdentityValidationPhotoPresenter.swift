import Core

protocol IdentityValidationPhotoPresenting: AnyObject {
    var coordinator: IdentityValidationPhotoCoordinating? { get set }
    var viewController: IdentityValidationPhotoDisplayable? { get set }
    
    func handleStepInfo(_ step: IdentityValidationStep)
    func handleOpenCamera(with step: IdentityValidationStep)
    func handleStartLoading()
    func handleNextStep(_ step: IdentityValidationStep)
    func handleError(_ error: PicPayError)
    func handleCloseFlow()
}

final class IdentityValidationPhotoPresenter: IdentityValidationPhotoPresenting {
    typealias Dependencies = HasMainQueue

    var coordinator: IdentityValidationPhotoCoordinating?
    weak var viewController: IdentityValidationPhotoDisplayable?
    
    private let dependencies: Dependencies

    init(coordinator: IdentityValidationPhotoCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    func handleStepInfo(_ step: IdentityValidationStep) {
        var image: UIImage
        switch step.type {
        case .selfie:
            image = Assets.Ilustration.iluPhotoSelfie.image
        case .documentFront:
            image = Assets.Ilustration.iluDocumentFront.image
        default:
            image = Assets.Ilustration.iluDocumentBack.image
        }

        viewController?.presentStepInfo(image: image, title: step.title, description: step.text)
    }
    
    func handleOpenCamera(with step: IdentityValidationStep) {
        coordinator?.presentCamera(for: step.type)
    }
    
    func handleStartLoading() {
        viewController?.startLoading()
    }
    
    func handleNextStep(_ step: IdentityValidationStep) {
        dependencies.mainQueue.async {
            self.viewController?.stopLoading()
            self.coordinator?.nextViewController(for: step)
        }
    }
    
    func handleError(_ error: PicPayError) {
        dependencies.mainQueue.async {
            self.viewController?.presentAlert(with: error)
        }
    }
    
    func handleCloseFlow() {
        coordinator?.dismiss()
    }
}
