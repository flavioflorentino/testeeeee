import Core

protocol IdentityValidationPhotoViewModelInputs: AnyObject {
    func loadStepInfo()
    func openCamera()
    func didReceiveImage(_ notification: Notification)
    func uploadImage(imageData: Data, progress: @escaping (Double) -> Void)
    func closeFlow()
}

final class IdentityValidationPhotoViewModel {
    private let step: IdentityValidationStep
    private let service: IdentityValidationPhotoServicing
    private let presenter: IdentityValidationPhotoPresenting
    
    private let imageSize = CGSize(width: 1_200, height: 1_200)
    
    init(step: IdentityValidationStep, service: IdentityValidationPhotoServicing, presenter: IdentityValidationPhotoPresenting) {
        self.step = step
        self.service = service
        self.presenter = presenter
    }
}

extension IdentityValidationPhotoViewModel: IdentityValidationPhotoViewModelInputs {
    func loadStepInfo() {
        presenter.handleStepInfo(step)
    }
    
    func openCamera() {
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveImage(_:)), name: .didReceiveImage, object: nil)
        presenter.handleOpenCamera(with: step)
    }
    
    @objc
    func didReceiveImage(_ notification: Notification) {
        NotificationCenter.default.removeObserver(self)
        
        guard
            let image = notification.object as? UIImage,
            let imageForUpload = image.resizeImage(targetSize: imageSize),
            let imageData = imageForUpload.jpegData(compressionQuality: 0.8)
            else {
                return
        }
        
        uploadImage(imageData: imageData) { progress in
            debugPrint("Picture upload progress: \(progress)")
        }
    }
    
    func uploadImage(imageData: Data, progress: @escaping (Double) -> Void) {
        self.presenter.handleStartLoading()
        
        service.uploadImage(imageData: imageData, uploadProgress: progress) { [weak self] result in
            guard let self = self else {
                return
            }
            
            switch result {
            case .success(let step):
                self.presenter.handleNextStep(step)
            case .failure(let error):
                self.presenter.handleError(error)
            }
        }
    }
    
    func closeFlow() {
        presenter.handleCloseFlow()
    }
}
