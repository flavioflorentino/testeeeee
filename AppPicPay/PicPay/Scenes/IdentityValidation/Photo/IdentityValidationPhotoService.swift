protocol IdentityValidationPhotoServicing {
    func uploadImage(imageData: Data, uploadProgress: @escaping ((Double) -> Void), _ completion: @escaping ((PicPayResult<IdentityValidationStep>) -> Void))
}

final class IdentityValidationPhotoService: BaseApi, IdentityValidationPhotoServicing {
    func uploadImage(imageData: Data, uploadProgress: @escaping ((Double) -> Void), _ completion: @escaping ((PicPayResult<IdentityValidationStep>) -> Void) ) {
        let files: [FileUpload] = [FileUpload(key: "image", data: imageData, fileName: "imagem.jpg", mimeType: "image/jpg")]
        requestManager.apiUpload(WebServiceInterface.apiEndpoint(kWsUrlIdentityValidationStepImage), method: .post, files: files, uploadProgress: uploadProgress, completion: completion)
    }
}
