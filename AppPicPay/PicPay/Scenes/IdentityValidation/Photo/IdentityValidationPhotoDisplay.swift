import UIKit

protocol IdentityValidationPhotoDisplayable: AnyObject {
    func presentStepInfo(image: UIImage, title: String, description: String)
    func presentAlert(with error: PicPayError)
    func startLoading()
    func stopLoading()
}
