import Core
import FeatureFlag
import Foundation

protocol IdentityValidationQuestionsServicing {
    func sendAnswers(answers: [String], _ completion: @escaping ((PicPayResult<IdentityValidationStep>) -> Void))
}

final class IdentityValidationQuestionsService: BaseApi, IdentityValidationQuestionsServicing {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func sendAnswers(answers: [String], _ completion: @escaping ((PicPayResult<IdentityValidationStep>) -> Void)) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let params = ["answers": answers]
            requestManager
                .apiRequest(endpoint: kWsUrlIdentityValidationStepAnswers, method: .post, parameters: params)
                .responseApiObject { result in
                    completion(result)
                }
            return
        }
        // TODO: - adicionar helper para o core network
    }
}
