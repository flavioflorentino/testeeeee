import UI
import UIKit

protocol IdentityValidationQuestionsSelecting: AnyObject {
    func didSelectAnswer(answer: String)
    func didStopStopwatch()
    func didTapNextButton()
}

final class IdentityValidationQuestionsView: UIView {
    private lazy var containerView = UIView()
    private lazy var scrollView = UIScrollView()
    private lazy var questionNumeratorLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 13, weight: .semibold)
        return label
    }()
    private lazy var questionLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale600.color
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        return label
    }()
    private lazy var questionOptionsStack: UIStackView = {
        let stack = UIStackView()
        stack.alignment = .fill
        stack.distribution = .fill
        stack.axis = .vertical
        stack.spacing = 8
        return stack
    }()
    private lazy var nextButton: UIPPButton = {
        let button = UIPPButton()
        button.setTitle(DefaultLocalizable.next.text, for: .normal)
        button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        button.addTarget(self, action: #selector(didTapNextButton), for: .touchUpInside)
        button.borderWidth = 0
        return button
    }()
    private lazy var stopwatchLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorBranding300.color
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()
    
    weak var delegate: IdentityValidationQuestionsSelecting?
    
    private var answerViews: [IdentityValidationQuestionAnswerView] = []
    
    private var stopwatch: Int = 60 {
        didSet {
            let attributedText = NSMutableAttributedString(string: IdentityValidationLocalizable.stopwatchDescription.text)
            attributedText.append(NSAttributedString(string: "\(stopwatch)s", attributes: [.font: UIFont.systemFont(ofSize: 12.0, weight: .bold)]))
            stopwatchLabel.attributedText = attributedText
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews(questionNumber: String, question: String, questionOptions: [String]) {
        questionNumeratorLabel.text = questionNumber
        questionLabel.text = question

        questionOptions.forEach {
            let answerView = IdentityValidationQuestionAnswerView(text: $0)
            let answerTap = UITapGestureRecognizer(target: self, action: #selector(selectAnswer(_:)))
            answerView.addGestureRecognizer(answerTap)
            answerView.isUserInteractionEnabled = true
            
            answerViews.append(answerView)
            questionOptionsStack.addArrangedSubview(answerView)
        }
        
        startStopwatch()
    }
    
    func startLoading() {
        nextButton.startLoadingAnimating()
    }
    
    func stopLoading() {
        nextButton.stopLoadingAnimating()
    }
}

// MARK: - Private methods

private extension IdentityValidationQuestionsView {
    func startStopwatch(with time: Int = 60) {
        stopwatch = time
        
        guard stopwatch != .zero else {
            stopStopwatch()
            return
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.startStopwatch(with: self.stopwatch - 1)
        }
    }
    
    func stopStopwatch() {
        delegate?.didStopStopwatch()
    }
    
    @objc
    func selectAnswer(_ sender: UITapGestureRecognizer) {
        answerViews.forEach {
            $0.setIsSelected(false)
        }
        
        guard let answerView = sender.view as? IdentityValidationQuestionAnswerView else {
            return
        }
        
        answerView.setIsSelected(true)
        delegate?.didSelectAnswer(answer: answerView.getAnswer())
    }
    
    @objc
    func didTapNextButton() {
        delegate?.didTapNextButton()
    }
}

// MARK: - ViewCode

extension IdentityValidationQuestionsView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubviews(
            questionNumeratorLabel,
            questionLabel,
            questionOptionsStack,
            nextButton,
            stopwatchLabel
        )
    }
    
    public func setupConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: compatibleSafeAreaLayoutGuide.topAnchor),
            scrollView.leftAnchor.constraint(equalTo: leftAnchor),
            scrollView.rightAnchor.constraint(equalTo: rightAnchor),
            scrollView.bottomAnchor.constraint(equalTo: compatibleSafeAreaLayoutGuide.bottomAnchor),
            
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            containerView.leftAnchor.constraint(equalTo: scrollView.leftAnchor),
            containerView.rightAnchor.constraint(equalTo: scrollView.rightAnchor),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            containerView.widthAnchor.constraint(equalTo: widthAnchor),
            
            questionNumeratorLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 20),
            questionNumeratorLabel.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 20),
            questionNumeratorLabel.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -20),
            
            questionLabel.topAnchor.constraint(equalTo: questionNumeratorLabel.bottomAnchor, constant: 12),
            questionLabel.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 20),
            questionLabel.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -20),
            
            questionOptionsStack.topAnchor.constraint(equalTo: questionLabel.bottomAnchor, constant: 20),
            questionOptionsStack.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 20),
            questionOptionsStack.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -20),
            
            nextButton.topAnchor.constraint(equalTo: questionOptionsStack.bottomAnchor, constant: 20),
            nextButton.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 20),
            nextButton.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -20),
            
            stopwatchLabel.topAnchor.constraint(equalTo: nextButton.bottomAnchor, constant: 20),
            stopwatchLabel.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 20),
            stopwatchLabel.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -20),
            stopwatchLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -20)
        ])
    }
    
    public func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        containerView.translatesAutoresizingMaskIntoConstraints = false
        questionNumeratorLabel.translatesAutoresizingMaskIntoConstraints = false
        questionLabel.translatesAutoresizingMaskIntoConstraints = false
        questionOptionsStack.translatesAutoresizingMaskIntoConstraints = false
        nextButton.translatesAutoresizingMaskIntoConstraints = false
        stopwatchLabel.translatesAutoresizingMaskIntoConstraints = false
    }
}
