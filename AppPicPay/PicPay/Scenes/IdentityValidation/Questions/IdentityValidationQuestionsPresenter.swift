import AnalyticsModule
import Core

protocol IdentityValidationQuestionsPresenting: AnyObject {
    var coordinator: IdentityValidationQuestionsCoordinating? { get set }
    var viewController: IdentityValidationQuestionsDisplayable? { get set }
    
    func handleLoadInfoValues(with questionManager: IdentityValidationQuestionManager)
    func handleNextQuestion(with questionManager: IdentityValidationQuestionManager)
    func handleNextStep(_ step: IdentityValidationStep)
    func handleError(_ error: PicPayError)
    func handleCloseFlow()
}

final class IdentityValidationQuestionsPresenter {
    typealias Dependencies = HasMainQueue

    var coordinator: IdentityValidationQuestionsCoordinating?
    weak var viewController: IdentityValidationQuestionsDisplayable?

    private let dependencies: Dependencies
    
    init(coordinator: IdentityValidationQuestionsCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

extension IdentityValidationQuestionsPresenter: IdentityValidationQuestionsPresenting {
    func handleLoadInfoValues(with questionManager: IdentityValidationQuestionManager) {
        let questionNumber = String(format: IdentityValidationLocalizable.questionNumberDescription.text,
                                    questionManager.currentIndex + 1,
                                    questionManager.questions.count)
        
        viewController?.presentInfoValues(
            questionNumber: questionNumber,
            question: questionManager.currentQuestion.text,
            questionOptions: questionManager.currentQuestion.options
        )
    }
    
    func handleNextQuestion(with questionManager: IdentityValidationQuestionManager) {
        coordinator?.showNextQuestion(with: questionManager)
    }
    
    func handleNextStep(_ step: IdentityValidationStep) {
        Analytics.shared.log(IdentityValidationEvent.questionaryAnswered)
        
        dependencies.mainQueue.async {
            self.coordinator?.showNextController(for: step)
        }
    }
    
    func handleError(_ error: PicPayError) {
        dependencies.mainQueue.async {
            self.viewController?.presentAlert(with: error)
        }
    }
    
    func handleCloseFlow() {
        coordinator?.dismiss()
    }
}
