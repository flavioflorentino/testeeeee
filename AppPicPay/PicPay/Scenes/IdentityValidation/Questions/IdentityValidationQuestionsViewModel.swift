protocol IdentityValidationQuestionsViewModelInputs: AnyObject {
    func loadInfoValues()
    func selectAnswer(with answer: String)
    func hasNextQuestion() -> Bool
    func nextQuestion()
    func skipQuestion()
    func sendAnswers()
    func closeFlow()
}

final class IdentityValidationQuestionsViewModel {
    private var questionManager: IdentityValidationQuestionManager
    private let service: IdentityValidationQuestionsServicing
    private let presenter: IdentityValidationQuestionsPresenting
    
    init(
        questionManager: IdentityValidationQuestionManager,
        service: IdentityValidationQuestionsServicing,
        presenter: IdentityValidationQuestionsPresenting
    ) {
        self.questionManager = questionManager
        self.service = service
        self.presenter = presenter
    }
}

extension IdentityValidationQuestionsViewModel: IdentityValidationQuestionsViewModelInputs {
    func loadInfoValues() {
        presenter.handleLoadInfoValues(with: questionManager)
    }
    
    func selectAnswer(with answer: String) {
        questionManager.select(answer: answer)
    }
    
    func hasNextQuestion() -> Bool {
        questionManager.hasNextQuestion()
    }
    
    func nextQuestion() {
        if hasNextQuestion() {
            questionManager.changeToNextIndex()
            presenter.handleNextQuestion(with: questionManager)
        } else {
            sendAnswers()
        }
    }
    
    func skipQuestion() {
        if hasNextQuestion() {
            questionManager.clearCurrentAnswer()
            questionManager.changeToNextIndex()
            presenter.handleNextQuestion(with: questionManager)
        }
    }
    
    func sendAnswers() {
        let answers: [String] = questionManager.questions.map { $0.answer }
        
        service.sendAnswers(answers: answers) { [weak self] result in
            guard let self = self else {
                return
            }
            
            switch result {
            case .success(let step):
                self.presenter.handleNextStep(step)
            case .failure(let error):
                self.presenter.handleError(error)
            }
        }
    }
    
    func closeFlow() {
        presenter.handleCloseFlow()
    }
}
