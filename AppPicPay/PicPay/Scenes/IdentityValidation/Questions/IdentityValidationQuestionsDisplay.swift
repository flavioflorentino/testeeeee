protocol IdentityValidationQuestionsDisplayable: AnyObject {
    func presentInfoValues(questionNumber: String, question: String, questionOptions: [String])
    func presentAlert(with error: PicPayError)
}
