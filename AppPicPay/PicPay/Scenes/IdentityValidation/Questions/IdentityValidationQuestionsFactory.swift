enum IdentityValidationQuestionsFactory {
    static func make(coordinatorDelegate: IdentityValidationCoordinating?, questionManager: IdentityValidationQuestionManager, navigationController: UINavigationController) -> IdentityValidationQuestionsViewController {
        let container = DependencyContainer()
        let coordinator: IdentityValidationQuestionsCoordinating = IdentityValidationQuestionsCoordinator(
            coordinatorDelegate: coordinatorDelegate,
            navigationController: navigationController
        )
        let service: IdentityValidationQuestionsServicing = IdentityValidationQuestionsService()
        let presenter: IdentityValidationQuestionsPresenting = IdentityValidationQuestionsPresenter(coordinator: coordinator, dependencies: container)
        let viewModel = IdentityValidationQuestionsViewModel(questionManager: questionManager, service: service, presenter: presenter)
        let viewController = IdentityValidationQuestionsViewController(viewModel: viewModel)

        presenter.viewController = viewController

        return viewController
    }
}
