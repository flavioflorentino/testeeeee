import UI
import UIKit

final class IdentityValidationQuestionAnswerView: UIView {
    private lazy var backgroundView: CustomCommonView = {
        let view = CustomCommonView()
        view.cornerRadius = 5
        view.borderWidth = 1
        view.borderColor = Palette.ppColorGrayscale300.color
        return view
    }()
    private lazy var answerLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 16)
        return label
    }()
    private lazy var switchButton: UIButton = {
        let button = UIButton()
        button.tintColor = Palette.ppColorGrayscale300.color
        button.setImage(#imageLiteral(resourceName: "iconUnchecked").withRenderingMode(.alwaysTemplate), for: .disabled)
        return button
    }()
    
    private var isSelected: Bool = false {
        didSet {
            if isSelected {
                switchButton.setImage(#imageLiteral(resourceName: "iconChecked").withRenderingMode(.alwaysTemplate), for: .disabled)
                switchButton.tintColor = Palette.ppColorBranding300.color
                backgroundView.layer.borderColor = Palette.ppColorBranding300.color.cgColor
                backgroundView.layer.borderWidth = 2
            } else {
                switchButton.setImage(#imageLiteral(resourceName: "iconUnchecked").withRenderingMode(.alwaysTemplate), for: .disabled)
                switchButton.tintColor = Palette.ppColorGrayscale300.color
                backgroundView.layer.borderColor = Palette.ppColorGrayscale300.color.cgColor
                backgroundView.layer.borderWidth = 1
            }
        }
    }
    
    private var answer: String = ""
    
    init(text: String, frame: CGRect = .zero) {
        super.init(frame: frame)
        
        answer = text
        switchButton.isEnabled = false
        answerLabel.text = answer
        isSelected = false
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setIsSelected(_ isSelected: Bool) {
        self.isSelected = isSelected
    }
    
    func getAnswer() -> String {
        answer
    }
}

// MARK: - ViewCode

extension IdentityValidationQuestionAnswerView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(backgroundView)
        backgroundView.addSubview(answerLabel)
        backgroundView.addSubview(switchButton)
    }
    
    public func setupConstraints() {
        NSLayoutConstraint.activate([
            backgroundView.topAnchor.constraint(equalTo: topAnchor),
            backgroundView.leftAnchor.constraint(equalTo: leftAnchor),
            backgroundView.rightAnchor.constraint(equalTo: rightAnchor),
            backgroundView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            answerLabel.topAnchor.constraint(equalTo: backgroundView.topAnchor, constant: 10),
            answerLabel.leftAnchor.constraint(equalTo: backgroundView.leftAnchor, constant: 16),
            answerLabel.bottomAnchor.constraint(equalTo: backgroundView.bottomAnchor, constant: -10),
            answerLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 40),
            
            switchButton.centerYAnchor.constraint(equalTo: backgroundView.centerYAnchor),
            switchButton.leftAnchor.constraint(equalTo: answerLabel.rightAnchor, constant: 16),
            switchButton.rightAnchor.constraint(equalTo: backgroundView.rightAnchor, constant: -16),
            switchButton.widthAnchor.constraint(equalToConstant: 24),
            switchButton.heightAnchor.constraint(equalToConstant: 24)
        ])
    }
    
    public func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
        
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        answerLabel.translatesAutoresizingMaskIntoConstraints = false
        switchButton.translatesAutoresizingMaskIntoConstraints = false
    }
}
