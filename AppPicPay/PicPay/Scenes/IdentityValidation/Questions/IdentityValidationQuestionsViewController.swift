import UI
import UIKit

// swiftlint:disable type_name

final class IdentityValidationQuestionsViewController: ViewController<IdentityValidationQuestionsViewModelInputs, IdentityValidationQuestionsView> {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationController()
        
        viewModel.loadInfoValues()   
    }
    
    private func setupNavigationController() {
        let leftButton = UIBarButtonItem(
            title: DefaultLocalizable.btCancel.text,
            style: .plain,
            target: self,
            action: #selector(cancel)
        )
        let rightButton = UIBarButtonItem(
            title: IdentityValidationLocalizable.questionsJumpQuestionNavButtonTitle.text,
            style: .plain,
            target: self,
            action: #selector(skipQuestion)
        )
        
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.barTintColor = Palette.white.color
        navigationController?.navigationBar.tintColor = Palette.ppColorBranding300.color
        navigationItem.setLeftBarButton(leftButton, animated: true)
        navigationItem.setRightBarButton(rightButton, animated: true)
    }
}

// MARK: - Actions

private extension IdentityValidationQuestionsViewController {
    @objc
    func skipQuestion() {
        viewModel.skipQuestion()
    }
    
    @objc
    func cancel() {
        let alert = Alert(title: IdentityValidationLocalizable.questionsExitAlertTitle.text, text: IdentityValidationLocalizable.questionsExitAlertMessage.text)
        
        let cancelButton = Button(title: IdentityValidationLocalizable.questionsExitCancel.text, type: .cta, action: .close)
        let closeButton = Button(
            title: IdentityValidationLocalizable.questionsExitConfirmation.text,
            type: .clean
        ) { [weak self] popup, _ in
            popup.dismiss(animated: true) {
                self?.viewModel.closeFlow()
            }
        }
        alert.buttons = [cancelButton, closeButton]
        
        AlertMessage.showAlert(alert, controller: self)
    }

    func nextQuestion() {
        rootView.startLoading()
        viewModel.nextQuestion()
        rootView.delegate = nil
    }
}

// MARK: - Presenter Outputs

extension IdentityValidationQuestionsViewController: IdentityValidationQuestionsDisplayable {
    func presentInfoValues(questionNumber: String, question: String, questionOptions: [String]) {
        rootView.setupViews(questionNumber: questionNumber, question: question, questionOptions: questionOptions)
        rootView.delegate = self
    }
    
    func presentAlert(with error: PicPayError) {
        rootView.delegate = self
        rootView.stopLoading()
        AlertMessage.showAlertWithError(error, controller: self)
    }
}

// MARK: - View Delegate

extension IdentityValidationQuestionsViewController: IdentityValidationQuestionsSelecting {
    func didSelectAnswer(answer: String) {
        viewModel.selectAnswer(with: answer)
    }
    
    func didStopStopwatch() {
        nextQuestion()
    }
    
    func didTapNextButton() {
        nextQuestion()
    }
}
