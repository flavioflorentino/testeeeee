import UIKit

protocol IdentityValidationQuestionsCoordinating: AnyObject {
    func showNextQuestion(with questionManager: IdentityValidationQuestionManager)
    func showNextController(for step: IdentityValidationStep)
    func dismiss()
}

final class IdentityValidationQuestionsCoordinator: IdentityValidationQuestionsCoordinating {
    private weak var coordinatorDelegate: IdentityValidationCoordinating?
    private let navigationController: UINavigationController
    
    init(coordinatorDelegate: IdentityValidationCoordinating?, navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.coordinatorDelegate = coordinatorDelegate
    }

    func showNextQuestion(with manager: IdentityValidationQuestionManager) {
        let questionsController = IdentityValidationQuestionsFactory.make(
            coordinatorDelegate: coordinatorDelegate,
            questionManager: manager,
            navigationController: navigationController
        )
        navigationController.pushViewController(questionsController, animated: true)
    }

    func showNextController(for step: IdentityValidationStep) {
        coordinatorDelegate?.pushNextController(for: step)
    }

    func dismiss() {
        coordinatorDelegate?.dismiss()
    }
}
