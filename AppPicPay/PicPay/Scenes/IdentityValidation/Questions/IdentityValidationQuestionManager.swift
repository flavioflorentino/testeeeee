struct IdentityValidationQuestionManager {
    let questions: [IdentityValidationQuestion]
    
    var currentIndex: Int
    
    var currentQuestion: IdentityValidationQuestion {
        questions[currentIndex]
    }
    
    init(questions: [IdentityValidationQuestion], currentIndex: Int = 0) {
        self.questions = questions
        self.currentIndex = currentIndex
    }
    
    func select(answer: String) {
        currentQuestion.answer = answer
    }
    
    func clearCurrentAnswer() {
        currentQuestion.answer = ""
    }
    
    func hasNextQuestion() -> Bool {
        currentIndex < questions.count - 1
    }
    
    mutating func changeToNextIndex() {
        currentIndex += 1
    }
}

extension IdentityValidationQuestionManager: Equatable {
    static func == (lhs: IdentityValidationQuestionManager, rhs: IdentityValidationQuestionManager) -> Bool {
        lhs.currentQuestion.text == rhs.currentQuestion.text && lhs.currentIndex == rhs.currentIndex
    }
}
