import FeatureFlag
import UIKit

protocol IdentityValidationCoordinating: AnyObject {
    func presentLoadedController(for step: IdentityValidationStep)
    func pushNextController(for step: IdentityValidationStep)
    func dismiss()
}

final class IdentityValidationFlowCoordinator: Coordinator, IdentityValidationCoordinating {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies

    private let originViewController: UIViewController
    private let originFlow: IdentityValidationOriginFlow
    private let router: IdentityValidationRouting
    let navigationController: UINavigationController
    var currentCoordinator: Coordinator?
    var didBeginIdentityValidation: (() -> Void)?
    
    init(
        from viewController: UIViewController,
        originFlow: IdentityValidationOriginFlow,
        navigationController: UINavigationController = UINavigationController(),
        dependencies: Dependencies = DependencyContainer(),
        router: IdentityValidationRouting = IdentityValidationRouter()
    ) {
        self.originViewController = viewController
        self.originFlow = originFlow
        self.navigationController = navigationController
        self.dependencies = dependencies
        self.router = router
    }
    
    func start() {
        let loadingViewController = IdentityValidationLoaderFactory.make(coordinatorDelegate: self)
        navigationController.addChild(loadingViewController)
        loadingViewController.didMove(toParent: navigationController)
        originViewController.present(navigationController, animated: true, completion: didBeginIdentityValidation)
    }

    func presentLoadedController(for step: IdentityValidationStep) {
        guard
            let fromViewController = navigationController.viewControllers.last,
            let toViewController = getController(for: step)
            else {
                showErrorAlert()
                return
        }

        navigationController.fadeReplace(fromViewController, by: toViewController)
    }

    func pushNextController(for step: IdentityValidationStep) {
        guard let nextController = getController(for: step) else {
            showErrorAlert()
            return
        }
        navigationController.pushViewController(nextController, animated: true)
    }

    func dismiss() {
        navigationController.dismiss(animated: true)
    }
}

private extension IdentityValidationFlowCoordinator {
    func getController(for step: IdentityValidationStep) -> UIViewController? {
        let model = IdentityValidationRouterModel(
            step: step,
            coordinator: self,
            originFlow: originFlow,
            navigation: navigationController,
            identityFlag: dependencies.featureManager.isActive(.featureNewIdentityValidationFlow)
        )
        return router.getController(for: model)
    }

    func showErrorAlert() {
        let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
        AlertMessage.showAlert(error, controller: navigationController) {
            self.dismiss()
        }
    }
}

@objcMembers
final class IdentityValidationCoordinatorObjcWrapper: NSObject {
    let coordinator: IdentityValidationFlowCoordinator
    
    init(from viewController: UIViewController) {
        coordinator = IdentityValidationFlowCoordinator(from: viewController, originFlow: .notification)
    }
    
    func start() {
        coordinator.start()
    }
}
