import UIKit

protocol IdentityValidationInitialCoordinating: AnyObject {
    func presentTerms(with url: URL)
    func nextViewController(for step: IdentityValidationStep)
    func dismiss()
}

final class IdentityValidationInitialCoordinator: IdentityValidationInitialCoordinating {
    private weak var coordinatorDelegate: IdentityValidationCoordinating?
    private let navigationController: UINavigationController

    init(coordinatorDelegate: IdentityValidationCoordinating?, navigationController: UINavigationController) {
        self.coordinatorDelegate = coordinatorDelegate
        self.navigationController = navigationController
    }

    func presentTerms(with url: URL) {
        let controller = WebViewFactory.make(with: url)
        controller.title = SettingsLocalizable.termsOfUse.text
        navigationController.isNavigationBarHidden = false
        navigationController.pushViewController(controller, animated: true)
    }

    func nextViewController(for step: IdentityValidationStep) {
        coordinatorDelegate?.pushNextController(for: step)
    }

    func dismiss() {
        coordinatorDelegate?.dismiss()
    }
}
