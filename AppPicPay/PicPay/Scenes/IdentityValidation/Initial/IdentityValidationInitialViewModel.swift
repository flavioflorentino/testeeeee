protocol IdentityValidationInitialViewModelInputs: AnyObject {
    func loadStepInfo()
    func readTerms()
    func startValidation()
    func closeFlow()
}

final class IdentityValidationInitialViewModel {
    private let originFlow: IdentityValidationOriginFlow
    private let step: IdentityValidationStep
    private let service: IdentityValidationInitialServicing
    private let presenter: IdentityValidationInitialPresenting
    
    init(_ originFlow: IdentityValidationOriginFlow, step: IdentityValidationStep, service: IdentityValidationInitialServicing, presenter: IdentityValidationInitialPresenting) {
        self.originFlow = originFlow
        self.step = step
        self.service = service
        self.presenter = presenter
    }
}

extension IdentityValidationInitialViewModel: IdentityValidationInitialViewModelInputs {
    func loadStepInfo() {
        presenter.loadStepInfo(step)
    }

    func readTerms() {
        if let url = URL(string: WebServiceInterface.apiEndpoint(WsWebViewURL.termsOfUse.endpoint)) {
            presenter.presentTermsOfUse(url)
        }
    }
    
    func startValidation() {
        service.startValidation(originFlow: originFlow) { [weak self] result in
            guard let self = self else {
                return
            }
            
            switch result {
            case .success(let step):
                self.presenter.handleNextStep(step)
            case .failure(let error):
                self.presenter.handleError(error)
            }
        }
    }
    
    func closeFlow() {
        presenter.handleCloseFlow()
    }
}
