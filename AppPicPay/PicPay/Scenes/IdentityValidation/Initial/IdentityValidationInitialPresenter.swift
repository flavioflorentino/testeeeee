import AnalyticsModule
import Core
import UI

protocol IdentityValidationInitialPresenting: AnyObject {
    var coordinator: IdentityValidationInitialCoordinating? { get set }
    var viewController: IdentityValidationInitialDisplayable? { get set }
    
    func loadStepInfo(_ step: IdentityValidationStep)
    func presentTermsOfUse(_ url: URL)
    func handleNextStep(_ step: IdentityValidationStep)
    func handleError(_ error: PicPayError)
    func handleCloseFlow()
}

final class IdentityValidationInitialPresenter: IdentityValidationInitialPresenting {
    typealias Dependencies = HasMainQueue

    var coordinator: IdentityValidationInitialCoordinating?
    weak var viewController: IdentityValidationInitialDisplayable?

    private let dependencies: Dependencies

    init(coordinator: IdentityValidationInitialCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }

    func loadStepInfo(_ step: IdentityValidationStep) {
        let attributedText = NSMutableAttributedString(string: IdentityValidationLocalizable.termsOfUseFullMessage.text)
        let attributedRange = (attributedText.string as NSString).range(of: IdentityValidationLocalizable.termsOfUse.text)

        attributedText.addAttributes(
            [
                .foregroundColor: Colors.branding400.color,
                .underlineStyle: NSUnderlineStyle.single.rawValue
            ],
            range: attributedRange
        )

        viewController?.presentStepInfo(title: step.title, description: step.text, terms: attributedText)
    }

    func presentTermsOfUse(_ url: URL) {
        coordinator?.presentTerms(with: url)
    }
    
    func handleNextStep(_ step: IdentityValidationStep) {
        dependencies.mainQueue.async {
            self.coordinator?.nextViewController(for: step)
            
            Analytics.shared.log(IdentityValidationEvent.begin)
            self.viewController?.stopLoading()
        }
    }
    
    func handleError(_ error: PicPayError) {
        dependencies.mainQueue.async {
            self.viewController?.presentAlert(with: error)
        }
    }
    
    func handleCloseFlow() {
        coordinator?.dismiss()
    }
}
