import Core
import FeatureFlag
import Foundation

protocol IdentityValidationInitialServicing {
    func startValidation(originFlow: IdentityValidationOriginFlow,
                         _ completion: @escaping ((PicPayResult<IdentityValidationStep>) -> Void))
}

final class IdentityValidationInitialService: BaseApi, IdentityValidationInitialServicing {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func startValidation(originFlow: IdentityValidationOriginFlow,
                         _ completion: @escaping ((PicPayResult<IdentityValidationStep>) -> Void)) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let headers = originFlow == .settings ? nil : ["verification_type": "CREATE_ACCOUNT"]
            requestManager
                .apiRequest(endpoint: kWsUrlIdentityValidationStep, method: .post, headers: headers)
                .responseApiObject { result in
                    completion(result)
                }
            return
        }
        // TODO: - adicionar helper para o core network
    }
}
