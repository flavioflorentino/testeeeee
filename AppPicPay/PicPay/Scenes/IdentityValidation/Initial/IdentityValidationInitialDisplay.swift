protocol IdentityValidationInitialDisplayable: AnyObject {
    func presentStepInfo(title: String, description: String, terms: NSAttributedString)
    func presentAlert(with error: PicPayError)
    func stopLoading()
}
