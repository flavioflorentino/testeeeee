enum IdentityValidationInitialFactory {
    static func make(
        coordinatorDelegate: IdentityValidationCoordinating?,
        originFlow: IdentityValidationOriginFlow,
        step: IdentityValidationStep,
        navigationController: UINavigationController
    ) -> IdentityValidationInitialViewController {
        let container = DependencyContainer()
        let coordinator = IdentityValidationInitialCoordinator(coordinatorDelegate: coordinatorDelegate, navigationController: navigationController)
        let service: IdentityValidationInitialServicing = IdentityValidationInitialService()
        let presenter: IdentityValidationInitialPresenting = IdentityValidationInitialPresenter(coordinator: coordinator, dependencies: container)
        let viewModel = IdentityValidationInitialViewModel(originFlow,
                                                           step: step,
                                                           service: service,
                                                           presenter: presenter)
        let viewController = IdentityValidationInitialViewController(viewModel: viewModel)

        presenter.viewController = viewController
        
        return viewController
    }
}
