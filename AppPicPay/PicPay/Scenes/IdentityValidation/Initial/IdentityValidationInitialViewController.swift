import UI

extension IdentityValidationInitialViewController.Layout {
    enum Size {
        static let imageWidth: CGFloat = 240
        static let imageHeight: CGFloat = 130
        static let buttonHeight: CGFloat = 48
    }

    enum Radius {
        static let validateButton = Size.buttonHeight / 2
    }
}

final class IdentityValidationInitialViewController: ViewController<IdentityValidationInitialViewModelInputs, UIView> {
    fileprivate enum Layout { }

    private lazy var introImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.IdentityValidation.validacaoIntro.image
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = IdentityValidationLocalizable.initialStepDefaulTitle.text
        label
            .labelStyle(TitleLabelStyle(type: .large))
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = IdentityValidationLocalizable.initialStepDefaultMessage.text
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        return label
    }()

    private lazy var validateButton: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = Layout.Radius.validateButton
        button.configure(with: Button(title: IdentityValidationLocalizable.startValidation.text, type: .cta))
        button.addTarget(self, action: #selector(didTapValidate), for: .touchUpInside)
        return button
    }()

    private lazy var laterButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(IdentityValidationLocalizable.doItLater.text, for: .normal)
        button.addTarget(self, action: #selector(didTapLater), for: .touchUpInside)
        button.buttonStyle(LinkButtonStyle())
        return button
    }()

    private lazy var termsLabel: UILabel = {
        let label = UILabel()
        label.text = IdentityValidationLocalizable.termsOfUseFullMessage.text
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapTermsOfUse))
        label.addGestureRecognizer(tapRecognizer)
        label.isUserInteractionEnabled = true
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadStepInfo()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func buildViewHierarchy() {
        view.addSubview(introImageView)
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(validateButton)
        view.addSubview(laterButton)
        view.addSubview(termsLabel)
    }

    override func setupConstraints() {
        introImageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.width.equalTo(Layout.Size.imageWidth)
            $0.height.equalTo(Layout.Size.imageHeight)
        }

        titleLabel.snp.makeConstraints {
            $0.top.equalTo(introImageView.snp.bottom).offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base04)
            $0.trailing.equalToSuperview().offset(-Spacing.base04)
        }

        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(titleLabel.snp.leading)
            $0.trailing.equalTo(titleLabel.snp.trailing)
        }

        validateButton.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.equalTo(descriptionLabel.snp.leading)
            $0.trailing.equalTo(descriptionLabel.snp.trailing)
            $0.height.equalTo(Layout.Size.buttonHeight)
        }

        laterButton.snp.makeConstraints {
            $0.top.equalTo(validateButton.snp.bottom).offset(Spacing.base01)
            $0.centerX.equalToSuperview()
        }

        termsLabel.snp.makeConstraints {
            $0.top.equalTo(laterButton.snp.bottom).offset(Spacing.base03)
            $0.leading.equalTo(validateButton.snp.leading)
            $0.trailing.equalTo(validateButton.snp.trailing)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

@objc
private extension IdentityValidationInitialViewController {
    func didTapValidate() {
        validateButton.startLoadingAnimating()
        viewModel.startValidation()
    }

    func didTapLater() {
        viewModel.closeFlow()
    }

    func didTapTermsOfUse() {
        viewModel.readTerms()
    }
}

extension IdentityValidationInitialViewController: IdentityValidationInitialDisplayable {
    func presentStepInfo(title: String, description: String, terms: NSAttributedString) {
        titleLabel.text = title
        descriptionLabel.text = description
        termsLabel.attributedText = terms
    }
    
    func presentAlert(with error: PicPayError) {
        validateButton.stopLoadingAnimating()
        AlertMessage.showAlertWithError(error, controller: self)
    }
    
    func stopLoading() {
        validateButton.stopLoadingAnimating()
    }
}
