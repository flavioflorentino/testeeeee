import UIKit

enum IdentityValidationAction {
    case initial(_ step: IdentityValidationStep)
    case question(_ step: IdentityValidationStep)
    case nextQuestion(_ manager: IdentityValidationQuestionManager)
    case photo(_ step: IdentityValidationStep)
    case takePhoto(_ stepType: IdentityValidationStep.StepType)
    case photoPreview(_ image: UIImage)
    case usePhoto(_ image: UIImage)
    case discardPhoto
    case finished(_ step: IdentityValidationStep)
    case close
}
