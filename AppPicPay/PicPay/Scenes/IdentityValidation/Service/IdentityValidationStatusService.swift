import Core
import FeatureFlag
import Foundation

protocol IdentityValidationStatusServicing {
    func status(_ completion: @escaping ((PicPayResult<IVIdentityValidationStatus>) -> Void))
}

final class IdentityValidationStatusService: BaseApi, IdentityValidationStatusServicing {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func status(_ completion: @escaping ((PicPayResult<IVIdentityValidationStatus>) -> Void)) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            requestManager
                .apiRequest(endpoint: kWsUrlIdentityValidationStatus, method: .get)
                .responseApiObject(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
}
