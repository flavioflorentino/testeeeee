enum IdentityValidationLocalizable: String, Localizable {
    case doItLater
    case initialStepDefaulTitle
    case initialStepDefaultMessage
    case startValidation
    case termsOfUseFullMessage
    case termsOfUse
    
    case questionsJumpQuestionNavButtonTitle
    case questionsExitAlertTitle
    case questionsExitAlertMessage
    case questionsExitConfirmation
    case questionsExitCancel
    case questionNumberDescription
    case stopwatchDescription
    
    case photoStepDefautTitle
    case photoStepDefautMessage
    case takePicture
    
    case finishedStepDefautTitle
    case finishedStepDefautMessage
    case gotIt
    
    case underAnalysis
    case validated
    
    case cameraPermissionMessage
    case changeSettings
    
    case isReadable
    case chosePicture
    case selfieConfirmationMessage
    case documentConfirmationMessage
    
    case selfieDescriptionText
    case documentFrontDescriptionText
    case documentBackDescriptionText
    
    var key: String {
        self.rawValue
    }
    var file: LocalizableFile {
        .identityValidation
    }
}
