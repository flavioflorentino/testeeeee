extension Notification.Name {
    static let didReceiveImage = Notification.Name("didReceiveImage")
    static let didSendSelfie = Notification.Name("didSendSelfie")
    static let didSendDocument = Notification.Name("didSendDocument")
}
