import AVFoundation
import PermissionsKit
import UI
import UIKit

final class IdentityValidationCameraViewController: PPBaseViewController {
    typealias RectanglePoints = (pointA: CGPoint, pointB: CGPoint, pointC: CGPoint, pointD: CGPoint)

    lazy var permissionInfoView: PermissionInfoView = {
        let permissionInfoView = PermissionInfoView(frame: view.frame)
        permissionInfoView.imageView.image = #imageLiteral(resourceName: "camera")
        permissionInfoView.textLabel.text = IdentityValidationLocalizable.cameraPermissionMessage.text
        permissionInfoView.button.setTitle(IdentityValidationLocalizable.changeSettings.text, for: .normal)
        permissionInfoView.button.addTarget(self, action: #selector(requestCameraPermission), for: .touchUpInside)
        return permissionInfoView
    }()

    var viewModel: IdentityValidationCameraViewModelInputs?

    lazy var bluredBackground: UIView = {
        let blur = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        blur.frame = maskPlaceholder.frame
        blur.isUserInteractionEnabled = false
        return blur
    }()

    override var prefersStatusBarHidden: Bool {
        true
    }

    @IBOutlet private var headerLabel: UILabel!
    @IBOutlet private var cameraViewPlaceholder: UIView!
    @IBOutlet private var takePhotoButton: UIPPButton!
    @IBOutlet private var closeButton: UIButton!
    @IBOutlet private var maskPlaceholder: UIView!
    @IBOutlet private var maskUtilPlaceholder: UIView!
    @IBOutlet private var maskUtilPlaceholderTopConstraint: NSLayoutConstraint!

    // MARK: - Factory

    static func controllerFromStoryboard() -> IdentityValidationCameraViewController {
        IdentityValidationCameraViewController.controllerFromStoryboard(.identityValidation)
    }

    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel?.loadCameraInfo()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        setNeedsStatusBarAppearanceUpdate()
        checkCameraPermission()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel?.stopCamera()
    }

    // MARK: - User Action

    @IBAction private func takePhoto(_ sender: Any) {
        viewModel?.captureImage()
    }

    @IBAction private func close(_ sender: Any) {
        viewModel?.closeCamera()
    }
}

// MARK: - Presenter Output

extension IdentityValidationCameraViewController: IdentityValidationCameraDisplayable {
    func presentCameraInfo(_ maskType: IdentityValidationCameraViewSettings.MaskType, description: String, takePhotoButtonTitle: String) {
        takePhotoButton.setTitle(takePhotoButtonTitle, for: .normal)
        headerLabel.text = description

        addMask(with: maskType)
    }

    func presentAlert(with error: Error?) {
        AlertMessage.showAlert(error, controller: self)
    }
}

// MARK: - Camera Configuration

private extension IdentityValidationCameraViewController {
    private func configureCameraController() {
        takePhotoButton.isEnabled = true
        removePermissionPlaceholder()
        viewModel?.configureCamera(for: cameraViewPlaceholder)
    }

    private func addMask(with maskType: IdentityValidationCameraViewSettings.MaskType) {
        maskPlaceholder.setNeedsLayout()
        maskPlaceholder.layoutIfNeeded()
        maskUtilPlaceholder.setNeedsLayout()
        maskUtilPlaceholder.layoutIfNeeded()
        view.layoutIfNeeded()

        switch maskType {
        case .selfie:
            addSelfieMask()
        case .documentFront, .documentBack:
            addDocumentMask()
        default:
            maskPlaceholder.isHidden = true
            maskUtilPlaceholder.isHidden = true
        }
    }

    private func addSelfieMask() {
        maskPlaceholder.addSubview(bluredBackground)

        let path = UIBezierPath(roundedRect: bluredBackground.frame, cornerRadius: 0)

        let centerX = maskUtilPlaceholder.frame.size.width / 2
        let centerY = maskUtilPlaceholder.frame.size.height / 2 + maskUtilPlaceholderTopConstraint.constant
        var width = maskUtilPlaceholder.frame.size.width - 80 // screen width - margins
        var height = width / 0.73

        if height > maskUtilPlaceholder.frame.size.height {
            height = maskUtilPlaceholder.frame.size.height
            width = height * 0.73
        }

        let ovalPath = ovalPathFor(size: CGSize(width: width, height: height), center: CGPoint(x: centerX, y: centerY))
        path.append(ovalPath)
        path.usesEvenOddFillRule = true

        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        maskLayer.fillRule = .evenOdd

        let borderLayer = CAShapeLayer()
        borderLayer.path = ovalPath.cgPath
        borderLayer.strokeColor = Palette.ppColorGrayscale000.cgColor
        borderLayer.lineWidth = 6
        borderLayer.lineJoin = .round
        borderLayer.lineDashPattern = NSArray(array: [NSNumber(value: 16), NSNumber(value: 16)]) as? [NSNumber]

        bluredBackground.layer.addSublayer(borderLayer)
        bluredBackground.layer.mask = maskLayer
    }

    private func addDocumentMask() {
        maskPlaceholder.addSubview(bluredBackground)

        let path = UIBezierPath(roundedRect: bluredBackground.frame, cornerRadius: 0)

        let midX = maskUtilPlaceholder.frame.size.width / 2
        let midY = maskUtilPlaceholder.frame.size.height / 2 + maskUtilPlaceholderTopConstraint.constant
        var width = maskUtilPlaceholder.frame.size.width - 80 // screen width - margins
        var height = width / 0.65

        if height > maskUtilPlaceholder.frame.size.height {
            height = maskUtilPlaceholder.frame.size.height
            width = height * 0.65
        }

        let topY = midY - height / 2
        let bottonY = midY + height / 2

        let points = RectanglePoints(pointA: CGPoint(x: midX - width / 2, y: topY),
                                     pointB: CGPoint(x: midX + width / 2, y: topY),
                                     pointC: CGPoint(x: midX + width / 2, y: bottonY),
                                     pointD: CGPoint(x: midX - width / 2, y: bottonY))

        let rectPath = rectangularPath(connecting: points)
        path.append(rectPath)
        path.usesEvenOddFillRule = true

        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        maskLayer.fillRule = .evenOdd

        let borderLayer = CAShapeLayer()
        borderLayer.path = rectPath.cgPath

        bluredBackground.layer.addSublayer(borderLayer)
        bluredBackground.layer.mask = maskLayer

        let borderPath = borderAroundCornersPath(around: points)
        let corner = CAShapeLayer()
        corner.strokeColor = Palette.ppColorBranding300.cgColor
        corner.path = borderPath.cgPath
        corner.lineWidth = 10.0
        corner.fillColor = UIColor.clear.cgColor

        maskPlaceholder.layer.addSublayer(corner)
    }
}

// MARK: - Permission Manager

private extension IdentityValidationCameraViewController {
    func checkCameraPermission() {
        let permissionContact: Permission = PPPermission.camera()

        if permissionContact.status != .authorized {
            takePhotoButton.isEnabled = false
            view.insertSubview(permissionInfoView, belowSubview: closeButton)
            requestCameraPermission()
        } else {
            configureCameraController()
        }
    }

    @objc
    func requestCameraPermission() {
        PPPermission.requestCamera { [weak self] status in
            if status == .authorized {
                self?.configureCameraController()
            }
        }
    }

    func removePermissionPlaceholder() {
        permissionInfoView.removeFromSuperview()
    }
}

// MARK: - BazierPath

private extension IdentityValidationCameraViewController {
    func ovalPathFor(size: CGSize, center: CGPoint) -> UIBezierPath {
        let top = center.y - size.height / 2
        let bottom = center.y + size.height / 2

        let ovalPath = UIBezierPath()
        ovalPath.move(to: CGPoint(x: center.x, y: top))
        ovalPath.addCurve(to: CGPoint(x: center.x, y: bottom),
                          controlPoint1: CGPoint(x: center.x + size.width * 0.75, y: top),
                          controlPoint2: CGPoint(x: center.x + size.width * 0.5, y: bottom))
        ovalPath.addCurve(to: CGPoint(x: center.x, y: top),
                          controlPoint1: CGPoint(x: center.x - size.width * 0.5, y: bottom),
                          controlPoint2: CGPoint(x: center.x - size.width * 0.75, y: top))
        ovalPath.close()
        return ovalPath
    }

    func rectangularPath(connecting points: RectanglePoints) -> UIBezierPath {
        let rectPath = UIBezierPath()
        rectPath.move(to: points.pointA)
        rectPath.addLine(to: points.pointB)
        rectPath.addLine(to: points.pointC)
        rectPath.addLine(to: points.pointD)
        rectPath.close()
        return rectPath
    }

    func borderAroundCornersPath(around points: RectanglePoints) -> UIBezierPath {
        let borderPath = UIBezierPath()
        borderPath.move(to: CGPoint(x: points.pointA.x + 20.0, y: points.pointA.y))
        borderPath.addLine(to: CGPoint(x: points.pointA.x, y: points.pointA.y))
        borderPath.addLine(to: CGPoint(x: points.pointA.x, y: points.pointA.y + 25.0))

        borderPath.move(to: CGPoint(x: points.pointB.x - 20.0, y: points.pointB.y))
        borderPath.addLine(to: CGPoint(x: points.pointB.x, y: points.pointB.y))
        borderPath.addLine(to: CGPoint(x: points.pointB.x, y: points.pointB.y + 25.0))

        borderPath.move(to: CGPoint(x: points.pointC.x - 20.0, y: points.pointC.y))
        borderPath.addLine(to: CGPoint(x: points.pointC.x, y: points.pointC.y))
        borderPath.addLine(to: CGPoint(x: points.pointC.x, y: points.pointC.y - 25.0))

        borderPath.move(to: CGPoint(x: points.pointD.x + 20.0, y: points.pointD.y))
        borderPath.addLine(to: CGPoint(x: points.pointD.x, y: points.pointD.y))
        borderPath.addLine(to: CGPoint(x: points.pointD.x, y: points.pointD.y - 25.0))
        return borderPath
    }
}
