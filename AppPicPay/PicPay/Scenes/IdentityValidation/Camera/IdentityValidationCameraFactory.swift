enum IdentityValidationCameraFactory {
    static func make(settings: IdentityValidationCameraViewSettings, navigationController: UINavigationController) -> IdentityValidationCameraViewController {
        let coordinator: IdentityValidationCameraCoordinating = IdentityValidationCameraCoordinator(settings: settings, navigationController: navigationController)
        let presenter: IdentityValidationCameraPresenting = IdentityValidationCameraPresenter(coordinator: coordinator)
        let viewModel = IdentityValidationCameraViewModel(settings: settings, presenter: presenter)
        let viewController = IdentityValidationCameraViewController.controllerFromStoryboard()
        viewController.viewModel = viewModel
        
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
