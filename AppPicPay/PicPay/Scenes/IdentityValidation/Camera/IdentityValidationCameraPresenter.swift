import Core

protocol IdentityValidationCameraPresenting: AnyObject {
    var coordinator: IdentityValidationCameraCoordinating? { get set }
    var viewController: IdentityValidationCameraDisplayable? { get set }
    
    func handleCameraInfo(_ maskType: IdentityValidationCameraViewSettings.MaskType, description: String, takePhotoButtonTitle: String)
    func handleImagePreview(_ image: UIImage)
    func handleErrorPreview(_ error: Error?)
    func handleCloseCamera()
}

final class IdentityValidationCameraPresenter: IdentityValidationCameraPresenting {
    var coordinator: IdentityValidationCameraCoordinating?
    weak var viewController: IdentityValidationCameraDisplayable?

    init(coordinator: IdentityValidationCameraCoordinating) {
        self.coordinator = coordinator
    }
    
    func handleCameraInfo(_ maskType: IdentityValidationCameraViewSettings.MaskType, description: String, takePhotoButtonTitle: String) {
        viewController?.presentCameraInfo(maskType, description: description, takePhotoButtonTitle: takePhotoButtonTitle)
    }
    
    func handleImagePreview(_ image: UIImage) {
        coordinator?.perform(action: .photoPreview(image))
    }
    
    func handleErrorPreview(_ error: Error?) {
        viewController?.presentAlert(with: error)
    }
    
    func handleCloseCamera() {
        coordinator?.perform(action: .close)
    }
}
