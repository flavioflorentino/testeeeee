import UIKit

protocol IdentityValidationCameraCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var navigationController: UINavigationController { get set }
    var settings: IdentityValidationCameraViewSettings { get set }
    
    func perform(action: IdentityValidationAction)
}

final class IdentityValidationCameraCoordinator: IdentityValidationCameraCoordinating {
    weak var viewController: UIViewController?
    var navigationController: UINavigationController
    var settings: IdentityValidationCameraViewSettings
    
    init(settings: IdentityValidationCameraViewSettings, navigationController: UINavigationController) {
        self.settings = settings
        self.navigationController = navigationController
    }
    
    func perform(action: IdentityValidationAction) {
        if case let .photoPreview(image) = action {
            let previewController = IdentityValidationPreviewFactory.make(image: image, settings: settings, navigationController: navigationController)
            navigationController.pushViewController(previewController, animated: true)
        } else {
            navigationController.dismiss(animated: true)
        }
    }
}
