struct IdentityValidationCameraViewSettings {
    enum MaskType {
        case selfie
        case documentFront
        case documentBack
        case none
    }
    
    var descriptionText: String
    var takePictureButtonTitle: String
    var cameraDevice: CameraController.CameraPosition
    var mask: MaskType
    var confirmButtonTitle: String
    var confirmMessage: String
}
