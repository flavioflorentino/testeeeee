protocol IdentityValidationCameraDisplayable: AnyObject {
    func presentCameraInfo(_ maskType: IdentityValidationCameraViewSettings.MaskType, description: String, takePhotoButtonTitle: String)
    func presentAlert(with error: Error?)
}
