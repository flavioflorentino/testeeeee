import Core

protocol IdentityValidationCameraViewModelInputs: AnyObject {
    func loadCameraInfo()
    func configureCamera(for cameraView: UIView?)
    func captureImage()
    func stopCamera()
    func closeCamera()
}

final class IdentityValidationCameraViewModel {
    private var cameraController: CameraController?
    private let settings: IdentityValidationCameraViewSettings
    private let presenter: IdentityValidationCameraPresenting

    init(settings: IdentityValidationCameraViewSettings, presenter: IdentityValidationCameraPresenting) {
        self.settings = settings
        self.presenter = presenter
    }
}

extension IdentityValidationCameraViewModel: IdentityValidationCameraViewModelInputs {
    func loadCameraInfo() {
        presenter.handleCameraInfo(settings.mask,
                                   description: settings.descriptionText,
                                   takePhotoButtonTitle: settings.takePictureButtonTitle)
    }
    
    func configureCamera(for cameraView: UIView?) {
        guard cameraController == nil else {
            cameraController?.captureSession.startRunning()
            return
        }
        
        cameraController = CameraController()
        
        cameraController?.prepare { [weak self] error in
            guard
                let self = self,
                let cameraPlaceholder = cameraView
                else {
                return
            }
            
            if let error = error {
                debugPrint(error)
            }
            
            try? self.cameraController?.displayPreview(on: cameraPlaceholder)
            
            if self.settings.cameraDevice == .front {
                try? self.cameraController?.switchToFrontCamera()
            }
        }
    }
    
    func captureImage() {
        cameraController?.captureImage { [weak self] image, error in
            guard let self = self else {
                return
            }
            
            guard let capturedImage = image else {
                self.presenter.handleErrorPreview(error)
                return
            }
            
            self.presenter.handleImagePreview(capturedImage)
            self.stopCamera()
        }
    }
    
    func stopCamera() {
        cameraController?.captureSession.stopRunning()
    }
    
    func closeCamera() {
        presenter.handleCloseCamera()
    }
}
