import AnalyticsModule

enum IdentityValidationEvent: AnalyticsKeyProtocol {
    case begin
    case questionaryAnswered
    case selfieSended
    case identityPhotosSended
    case finished(statusText: String)
    case selfie(action: ActionStatus)
    case takeSelfie(action: ActionStatus)
    case sendSelfie(action: ActionStatus)
    case selfieError
    case document(action: ActionStatus)
    case documentError
    case documentFront(action: ActionStatus)
    case documentBack(action: ActionStatus)
    case sendDocumentFront(action: ActionStatus)
    case sendDocumentBack(action: ActionStatus)
    case documentTips(action: ActionStatus)
    case selfieTips(action: ActionStatus)
    case validationEnd(status: ValidationStatus)
    
    private var name: String {
        switch self {
        case .begin:
            return "ID Validation - Iniciou"
        case .questionaryAnswered:
            return "ID Validation - Respondeu questionário"
        case .selfieSended:
            return "ID Validation - Enviou selfie"
        case .identityPhotosSended:
            return "ID Validation - Enviou fotos da Identidade"
        case .finished:
            return "ID Validation - Finalizou"
        case .selfie:
            return "Identity Validation - Selfie"
        case .takeSelfie:
            return "Identity Validation - Take Selfie"
        case .sendSelfie:
            return "Identity Validation - Send Selfie"
        case .selfieError:
             return "Identity Validation - Selfie Error"
        case .document:
            return "Identity Validation - Document"
        case .documentError:
            return "Identity Validation - Document Error"
        case .documentFront:
            return "Identity Validation - Front Document Photo"
        case .documentBack:
            return "Identity Validation - Back Document Photo"
        case .sendDocumentFront:
            return "Identity Validation - Send Front Document"
        case .sendDocumentBack:
            return "Identity Validation - Send Back Document"
        case .documentTips:
            return "Identity Validation - Document Tips"
        case .selfieTips:
            return "Identity Validation - Selfie-Tips"
        case .validationEnd(let status):
            return "Identity Validation - \(status)"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .finished(statusText):
            return ["Status": statusText]
        case
            .selfie(let action),
            .takeSelfie(let action),
            .sendSelfie(let action),
            .document(let action),
            .documentTips(let action),
            .documentFront(let action),
            .documentBack(let action),
            .sendDocumentFront(let action),
            .sendDocumentBack(let action):
            return action.property
        case .validationEnd(let status):
            return status.property
        default:
            return [:]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

extension IdentityValidationEvent {
    enum ActionStatus: String, CustomStringConvertible {
        case drop = "DROP"
        case started = "STARTED"
        case close = "CLOSE"
        case taken = "TAKEN"
        case finished = "FINISHED"
        case none
        
        var description: String {
            self.rawValue
        }

        var property: [String: Any] {
            switch self {
            case .none:
                return [:]
            default:
                return ["status": self.description]
            }
        }
    }
    
    enum ValidationStatus: String, CustomStringConvertible {
        case success = "Success"
        case analysis = "Analysis"
        case reproved = "Reproved"
        
        var description: String {
            self.rawValue
        }

        var property: [String: Any] {
            switch self {
            case .reproved:
                return ["status": "AUTOMATIC"]
            default:
                return [:]
            }
        }
    }
}
