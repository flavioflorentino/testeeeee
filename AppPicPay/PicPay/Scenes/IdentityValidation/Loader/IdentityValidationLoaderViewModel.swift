import AnalyticsModule

protocol IdentityValidationLoaderViewModelInputs: AnyObject {
    func loadStep()
    func closeFlow()
    func sendEvents(for step: IdentityValidationStep)
}

final class IdentityValidationLoaderViewModel {
    private var step: IdentityValidationStep?
    private let service: IdentityValidationLoaderServicing
    private let presenter: IdentityValidationLoaderPresenting

    init(service: IdentityValidationLoaderServicing, presenter: IdentityValidationLoaderPresenting) {
        self.service = service
        self.presenter = presenter
    }
}

extension IdentityValidationLoaderViewModel: IdentityValidationLoaderViewModelInputs {
    func loadStep() {
        if let step = step {
            presenter.handleNextStep(step)
            return
        }
        
        service.step { [weak self] result in
            guard let self = self else {
                return
            }
            
            switch result {
            case let .success(step):
                self.sendEvents(for: step)
                self.presenter.handleNextStep(step)
                
            case let .failure(error):
                self.presenter.handleError(error)
            }
        }
    }
    
    func closeFlow() {
        presenter.handleCloseFlow()
    }
    
    func sendEvents(for step: IdentityValidationStep) {
        if case .initial = step.type {
            Analytics.shared.log(IdentityValidationEvent.begin)
        }
    }
}
