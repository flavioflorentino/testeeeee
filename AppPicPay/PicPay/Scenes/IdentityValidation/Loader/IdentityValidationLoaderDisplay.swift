protocol IdentityValidationLoaderDisplayable: AnyObject {
    func presentAlert(with error: PicPayError)
}
