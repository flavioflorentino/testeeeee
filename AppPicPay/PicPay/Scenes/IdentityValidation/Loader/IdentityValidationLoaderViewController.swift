import UI
import UIKit

final class IdentityValidationLoaderViewController: ViewController<IdentityValidationLoaderViewModelInputs, IdentityValidationLoaderView> {
     override func viewDidLoad() {
        super.viewDidLoad()
        rootView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
        viewModel.loadStep()
    }
}

// MARK: - Presenter Output

extension IdentityValidationLoaderViewController: IdentityValidationLoaderDisplayable {
    func presentAlert(with error: PicPayError) {
        AlertMessage.showAlert(error, controller: self) {
            self.viewModel.closeFlow()
        }
    }
}

// MARK: - View Selection

extension IdentityValidationLoaderViewController: IdentityValidationLoaderSelecting {
    func didTapButton() {
        viewModel.closeFlow()
    }
}
