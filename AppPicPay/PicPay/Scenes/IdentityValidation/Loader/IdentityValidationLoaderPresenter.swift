import Core

protocol IdentityValidationLoaderPresenting: AnyObject {
    var coordinator: IdentityValidationLoaderCoordinating? { get set }
    var viewController: IdentityValidationLoaderDisplayable? { get set }
    
    func handleNextStep(_ step: IdentityValidationStep)
    func handleError(_ error: PicPayError)
    func handleCloseFlow()
}

final class IdentityValidationLoaderPresenter: IdentityValidationLoaderPresenting {
    typealias Dependencies = HasMainQueue

    var coordinator: IdentityValidationLoaderCoordinating?
    weak var viewController: IdentityValidationLoaderDisplayable?
    
    private let dependencies: Dependencies

    init(coordinator: IdentityValidationLoaderCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }

    func handleNextStep(_ step: IdentityValidationStep) {
        dependencies.mainQueue.async {
            self.coordinator?.nextViewController(for: step)
        }
    }
    
    func handleError(_ error: PicPayError) {
        dependencies.mainQueue.async {
            self.viewController?.presentAlert(with: error)
        }
    }
    
    func handleCloseFlow() {
        coordinator?.dismiss()
    }
}
