import UIKit

protocol IdentityValidationLoaderCoordinating: AnyObject {
    func nextViewController(for step: IdentityValidationStep)
    func dismiss()
}

final class IdentityValidationLoaderCoordinator: IdentityValidationLoaderCoordinating {
    private weak var coordinatorDelegate: IdentityValidationCoordinating?

    init(coordinatorDelegate: IdentityValidationCoordinating?) {
        self.coordinatorDelegate = coordinatorDelegate
    }

    func nextViewController(for step: IdentityValidationStep) {
        coordinatorDelegate?.presentLoadedController(for: step)
    }

    func dismiss() {
        coordinatorDelegate?.dismiss()
    }
}
