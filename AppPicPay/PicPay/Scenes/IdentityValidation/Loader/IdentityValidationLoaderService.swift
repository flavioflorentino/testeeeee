import Core
import FeatureFlag
import Foundation

protocol IdentityValidationLoaderServicing {
    func step(_ completion: @escaping ((PicPayResult<IdentityValidationStep>) -> Void))
}

final class IdentityValidationLoaderService: BaseApi, IdentityValidationLoaderServicing {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func step(_ completion: @escaping ((PicPayResult<IdentityValidationStep>) -> Void) ) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            requestManager
                .apiRequest(endpoint: kWsUrlIdentityValidationStep, method: .get)
                .responseApiObject { result in
                    completion(result)
                }
            return
        }
        // TODO: - adicionar helper para o core network
    }
}
