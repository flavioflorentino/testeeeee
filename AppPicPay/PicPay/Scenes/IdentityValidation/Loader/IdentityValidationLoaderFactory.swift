enum IdentityValidationLoaderFactory {
    static func make(coordinatorDelegate: IdentityValidationCoordinating?) -> IdentityValidationLoaderViewController {
        let container = DependencyContainer()
        let coordinator = IdentityValidationLoaderCoordinator(coordinatorDelegate: coordinatorDelegate)
        let service: IdentityValidationLoaderServicing = IdentityValidationLoaderService()
        let presenter: IdentityValidationLoaderPresenting = IdentityValidationLoaderPresenter(coordinator: coordinator, dependencies: container)
        let viewModel = IdentityValidationLoaderViewModel(service: service, presenter: presenter)
        let viewController = IdentityValidationLoaderViewController(viewModel: viewModel)

        presenter.viewController = viewController

        return viewController
    }
}
