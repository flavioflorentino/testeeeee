import UI
import UIKit

protocol IdentityValidationLoaderSelecting: AnyObject {
    func didTapButton()
}

final class IdentityValidationLoaderView: UIView {
    private lazy var loadingView = LoadingView()
    private lazy var dismissButton: UIPPButton = {
        let button = UIPPButton()
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.borderWidth = 0
        button.normalTitleColor = Palette.ppColorBranding300.color
        button.normalBackgrounColor = .clear
        button.highlightedTitleColor = Palette.ppColorBranding300.color.withAlphaComponent(0.5)
        button.highlightedBackgrounColor = .clear
        button.setTitle(IdentityValidationLocalizable.doItLater.text, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    weak var delegate: IdentityValidationLoaderSelecting?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Palette.ppColorGrayscale000.color
        loadingView.isHidden = false
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc
    private func buttonAction() {
        delegate?.didTapButton()
    }
}

// MARK: - ViewCode

extension IdentityValidationLoaderView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(loadingView)
        addSubview(dismissButton)
    }
    
    public func setupConstraints() {
        var bottomAnchorConstraint = bottomAnchor
        var bottomConstant: CGFloat = -20
        if #available(iOS 11.0, *) {
            bottomAnchorConstraint = safeAreaLayoutGuide.bottomAnchor
            if let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom, bottom > 0 {
                bottomConstant = 0
            }
        }
        
        NSLayoutConstraint.activate([
            loadingView.centerXAnchor.constraint(equalTo: centerXAnchor),
            loadingView.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            dismissButton.heightAnchor.constraint(equalToConstant: 48),
            dismissButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            dismissButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            dismissButton.bottomAnchor.constraint(equalTo: bottomAnchorConstraint, constant: bottomConstant)
        ])
    }
    
    public func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
        loadingView.translatesAutoresizingMaskIntoConstraints = false
    }
}
