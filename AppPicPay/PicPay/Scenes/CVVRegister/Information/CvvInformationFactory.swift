import Foundation

enum CvvInformationFactory {
    static func make() -> CvvInformationViewController {
        let viewModel = CvvInformationViewModel()
        let viewController = CvvInformationViewController(viewModel: viewModel)
        
        return viewController
    }
}
