import UI
import UIKit

final class CvvInformationViewController: ViewController<CvvInformationViewModelInputs, UIView> {
    private let cardHeight: CGFloat = 144
    private let cardWidth: CGFloat = 232
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.text = CvvLocalizable.titleInformation.text
        label.textColor = Palette.ppColorGrayscale600.color
        label.font = UIFont.systemFont(ofSize: 28, weight: .bold)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 10
        label.text = CvvLocalizable.descInformation.text
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = UIFont.systemFont(ofSize: 16, weight: .light)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var cardImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.image = #imageLiteral(resourceName: "ic_cvv_card")
        image.translatesAutoresizingMaskIntoConstraints = false
        
        return image
    }()
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        navigationController?.navigationBar.topItem?.title = ""
    }
    
    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(cardImage)
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [titleLabel, descriptionLabel], constant: 20)
        
        NSLayoutConstraint.activate([
          titleLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 25),
          descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20),
          cardImage.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 22),
          cardImage.leadingAnchor.constraint(equalTo: descriptionLabel.leadingAnchor),
          cardImage.heightAnchor.constraint(equalToConstant: cardHeight),
          cardImage.widthAnchor.constraint(equalToConstant: cardWidth)
        ])
    }
}
