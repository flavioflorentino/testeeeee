import Core
import Foundation

struct CvvRegisterViewState {
    let title: String
    let isHiddenDescription: Bool
}

protocol CvvRegisterPresenting: AnyObject {
    var viewController: CvvRegisterDisplay? { get set }
    
    func displayNotFoundCard()
    func diplayNumMaxCvv(limit: Int)
    func displayCard(model: CvvRegister, isEmergencyAid: Bool)
    func displayButtonIsEnable(value: Bool)
    func didNextStep(action: CvvRegisterAction)
}

final class CvvRegisterPresenter: CvvRegisterPresenting {
    var viewController: CvvRegisterDisplay?

    func displayNotFoundCard() {
        viewController?.displayAlert(message: CvvLocalizable.notFoundCard.text)
    }
    
    func displayCard(model: CvvRegister, isEmergencyAid: Bool = false) {
        let viewState = createViewState(hasDescription: isEmergencyAid)
        
        viewController?.updateCard(model: model)
        viewController?.buttonIsEnable(value: false)
        viewController?.displayViewState(viewState: viewState)
        hideBarButtonIfDifferentEmergencyAid(isEmergencyAid: isEmergencyAid)
    }
    
    func displayButtonIsEnable(value: Bool) {
        viewController?.buttonIsEnable(value: value)
    }

    func diplayNumMaxCvv(limit: Int) {
        viewController?.configureInputField(limit: limit)
    }
    
    func didNextStep(action: CvvRegisterAction) {
        viewController?.didNextStep(action: action)
    }

    private func hideBarButtonIfDifferentEmergencyAid(isEmergencyAid: Bool) {
        if !isEmergencyAid {
            viewController?.hideRightBarButton()
        }
    }
    
    private func createViewState(hasDescription: Bool) -> CvvRegisterViewState {
        let title = hasDescription ? CvvLocalizable.alternativeTitle.text : CvvLocalizable.titleRegister.text
        return CvvRegisterViewState(title: title, isHiddenDescription: hasDescription == false)
    }
}
