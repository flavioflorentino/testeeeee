import AssetsKit
import UI
import UIKit

final class CvvRegisterViewController: LegacyViewController<CvvRegisterViewModelInputs, CvvRegisterCoordinating, UIView> {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = Palette.ppColorGrayscale600.color
        label.font = UIFont.systemFont(ofSize: 28, weight: .bold)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        let attributedText = NSAttributedString(
            string: CvvLocalizable.emergencyAidInformation.text,
            attributes: nil
        )
            .boldfyWithSystemFont(ofSize: 16, weight: UIFont.Weight.bold.rawValue)
        label.attributedText = attributedText
        label.numberOfLines = 0
        label.textColor = Colors.grayscale700.color
        label.translatesAutoresizingMaskIntoConstraints = false

        return label
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = false
        return scrollView
    }()

    private lazy var contentStack: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var inputField: CvvRegisterView = {
        let inputField = CvvRegisterView()
        inputField.limitValue = 4
        inputField.delegate = self
        inputField.translatesAutoresizingMaskIntoConstraints = false
        
        return inputField
    }()
    
    private lazy var footerView: CvvRegisterFooterView = {
        let footer = CvvRegisterFooterView()
        footer.delegate = self
        footer.translatesAutoresizingMaskIntoConstraints = false
        return footer
    }()
    
    private var keyboardHeight: CGFloat = 0 {
        didSet {
            updateFooterConstraints()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad()
        addObservers()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        inputField.firstResponder()
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        footerView.backgroundColor = Colors.backgroundPrimary.color
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_round_close_green"), style: .done, target: self, action: #selector(didTapClose))
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Resources.Icons.icoInformation.image,
            style: .done,
            target: self,
            action: #selector(didTapInfo)
        )
        addTapScrollToDismissAction()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        view.addSubview(footerView)
        scrollView.addSubview(contentStack)
        contentStack.addArrangedSubview(titleLabel)
        contentStack.addArrangedSubview(descriptionLabel)
        contentStack.addArrangedSubview(inputField)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.leading.top.trailing.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        contentStack.snp.makeConstraints {
            $0.leading.top.trailing.equalToSuperview().inset(Spacing.base03)
            $0.bottom.lessThanOrEqualToSuperview()
            $0.width.equalToSuperview().inset(Spacing.base03)
        }
        
        footerView.snp.makeConstraints {
            $0.top.equalTo(scrollView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.bottom.equalToSuperview().inset(Spacing.base03)
        }
    }
    
    private func updateFooterConstraints() {
        footerView.snp.updateConstraints {
            $0.bottom.equalToSuperview().inset(Spacing.base01 + keyboardHeight)
        }
        view.layoutIfNeeded()
    }
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func addTapScrollToDismissAction() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dissmissKeyboard))
        scrollView.addGestureRecognizer(tapGesture)
    }
    
    @objc
    private func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue,
            let offset = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
                return
        }
        
        keyboardHeight = keyboardSize.height == offset.height ? keyboardSize.height : offset.height
    }
    
    @objc
    private func keyboardWillHide(notification: NSNotification) {
        keyboardHeight = 0
    }
    
    @objc
   private func dissmissKeyboard() {
        view.endEditing(true)
   }
    
    @objc
    private func didTapClose() {
        viewModel.didTapClose()
    }

    @objc
    private func didTapInfo() {
        viewModel.didTapInformation()
    }
}

// MARK: View Model Outputs
extension CvvRegisterViewController: CvvRegisterDisplay {
    func displayViewState(viewState: CvvRegisterViewState) {
        titleLabel.text = viewState.title
        descriptionLabel.isHidden = viewState.isHiddenDescription
    }

    func hideRightBarButton() {
        navigationItem.rightBarButtonItem = nil
    }

    func updateCard(model: CvvRegister) {
        inputField.configure(with: model)
    }
    
    func buttonIsEnable(value: Bool) {
        footerView.isEnable = value
    }
    
    func displayAlert(message: String) {
        AlertMessage.showAlert(withMessage: message, controller: self)
    }
    
    func didNextStep(action: CvvRegisterAction) {
        view.endEditing(true)
        coordinator.perform(action: action)
    }
    
    func configureInputField(limit: Int) {
        inputField.limitValue = limit
    }
}

extension CvvRegisterViewController: CvvRegisterFooterViewDelegate {
    func didTapButton() {
        let value = inputField.value
        viewModel.didTapContinue(value: value)
    }
}

extension CvvRegisterViewController: CvvRegisterViewDelegate {
    func textDidChange(value: String) {
        viewModel.updateCvv(value: value)
    }
    
    func didTapInformation() {
        viewModel.didTapInformation()
    }
}
