import SkyFloatingLabelTextField
import UI
import UIKit

protocol CvvRegisterViewDelegate: AnyObject {
    func textDidChange(value: String)
    func didTapInformation()
}

final class CvvRegisterView: UIView {
    private let sizeCard: CGFloat = 32
    private let sizeIcon: CGFloat = 24
    
    private lazy var presenter: CvvRegisterViewPresenterProtocol = {
        let presenter = CvvRegisterViewPresenter()
        presenter.outputs = self
        
        return presenter
    }()
    
    private lazy var aliasLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var cardImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.translatesAutoresizingMaskIntoConstraints = false
        
        return image
    }()
    
    private lazy var iconImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.image = #imageLiteral(resourceName: "green_help_button")
        image.isUserInteractionEnabled = true
        image.translatesAutoresizingMaskIntoConstraints = false
        
        return image
    }()
    
    private lazy var cvvTextField: SkyFloatingLabelTextField = {
        let textField = SkyFloatingLabelTextField()
        textField.font = UIFont.systemFont(ofSize: 24)
        textField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        textField.keyboardType = .numberPad
        textField.rightViewMode = .always
        textField.rightView = iconImage
        textField.selectedLineColor = Palette.ppColorGrayscale400.color
        textField.lineColor = Palette.ppColorGrayscale400.color
        textField.textColor = Palette.ppColorGrayscale600.color
        textField.selectedTitleColor = Palette.ppColorGrayscale400.color
        textField.placeholder = CvvLocalizable.aliasPlaceholder.text
        textField.translatesAutoresizingMaskIntoConstraints = false
        
        return textField
    }()
    
    var value: String = "" {
        didSet {
            delegate?.textDidChange(value: value)
        }
    }
    
    var limitValue = Int.max {
        didSet {
            presenter.inputs.setup(limit: limitValue)
        }
    }
    
    weak var delegate: CvvRegisterViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .clear
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapInformation))
        iconImage.addGestureRecognizer(tap)
    }
    
    private func addComponents() {
        addSubview(cardImage)
        addSubview(aliasLabel)
        addSubview(cvvTextField)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            cardImage.topAnchor.constraint(equalTo: topAnchor),
            cardImage.leadingAnchor.constraint(equalTo: leadingAnchor),
            cardImage.heightAnchor.constraint(equalToConstant: sizeCard),
            cardImage.widthAnchor.constraint(equalToConstant: sizeCard)
        ])
        
        NSLayoutConstraint.activate([
            aliasLabel.centerYAnchor.constraint(equalTo: cardImage.centerYAnchor),
            aliasLabel.leadingAnchor.constraint(equalTo: cardImage.trailingAnchor, constant: 12),
            aliasLabel.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            cvvTextField.topAnchor.constraint(equalTo: cardImage.bottomAnchor, constant: 22),
            cvvTextField.leadingAnchor.constraint(equalTo: leadingAnchor),
            cvvTextField.trailingAnchor.constraint(equalTo: trailingAnchor),
            cvvTextField.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    @objc
    private func textDidChange(_ textField: UITextField) {
        guard let text = textField.text else {
            return
        }
        
        presenter.inputs.inputValue(value: text)
    }
    
    @objc
    private func didTapInformation() {
        delegate?.didTapInformation()
    }
    
    func firstResponder() {
        cvvTextField.becomeFirstResponder()
    }
    
    func configure(with model: CvvRegister) {
        presenter.inputs.setup(model: model)
    }
}

extension CvvRegisterView: CvvRegisterViewPresenterOutputs {
    func deleteBackward() {
        cvvTextField.deleteBackward()
    }
    
    func updateValue(value: String) {
        self.value = value
    }
    
    func updateCard(image: String, alias: String) {
        cardImage.setImage(url: URL(string: image))
        aliasLabel.text = alias
    }
}
