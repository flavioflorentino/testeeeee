import Foundation

protocol CvvRegisterViewPresenterInputs {
    func setup(limit: Int)
    func setup(model: CvvRegister)
    func inputValue(value: String)
}

protocol CvvRegisterViewPresenterOutputs: AnyObject {
    func deleteBackward()
    func updateValue(value: String)
    func updateCard(image: String, alias: String)
}

protocol CvvRegisterViewPresenterProtocol: AnyObject {
    var inputs: CvvRegisterViewPresenterInputs { get }
    var outputs: CvvRegisterViewPresenterOutputs? { get set }
}

final class CvvRegisterViewPresenter: CvvRegisterViewPresenterProtocol, CvvRegisterViewPresenterInputs {
    var inputs: CvvRegisterViewPresenterInputs { self }
    weak var outputs: CvvRegisterViewPresenterOutputs?
    
    private var limit = Int.max
    
    func setup(model: CvvRegister) {
        updateValue(alias: model.alias, lastDigits: model.lastDigits, image: model.image)
    }
    
    func setup(limit: Int) {
        self.limit = limit
    }
    
    func inputValue(value: String) {
        guard value.count > limit else {
            outputs?.updateValue(value: value)
            return
        }
        
        outputs?.deleteBackward()
    }
    
    private func updateValue(alias: String, lastDigits: String, image: String) {
        let alias = "\(alias) \(lastDigits)"
        
        outputs?.updateCard(image: image, alias: alias)
    }
}
