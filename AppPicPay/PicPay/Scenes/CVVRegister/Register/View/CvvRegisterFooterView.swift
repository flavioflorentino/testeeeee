import UIKit

protocol CvvRegisterFooterViewDelegate: AnyObject {
    func didTapButton()
}

final class CvvRegisterFooterView: UIView {
    private let heightButton: CGFloat = 48
    
    private lazy var button: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = heightButton / 2
        button.addTarget(self, action: #selector(tapButton), for: .touchUpInside)
        button.configure(with: Button(title: DefaultLocalizable.next.text))
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    var isEnable: Bool = false {
        didSet {
            button.isEnabled = isEnable
        }
    }
    
    weak var delegate: CvvRegisterFooterViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .clear
    }
    
    private func addComponents() {
        addSubview(button)
    }
    
    private func layoutComponents() {
        if #available(iOS 11.0, *) {
            button.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -5).isActive = true
        } else {
            button.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5).isActive = true
        }
        
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: topAnchor),
            button.leadingAnchor.constraint(equalTo: leadingAnchor),
            button.trailingAnchor.constraint(equalTo: trailingAnchor),
            button.heightAnchor.constraint(equalToConstant: heightButton)
        ])
    }
    
    @objc
    private func tapButton() {
        delegate?.didTapButton()
    }
}
