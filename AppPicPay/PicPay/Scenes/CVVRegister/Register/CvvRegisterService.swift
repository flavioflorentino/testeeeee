import Foundation

protocol CvvRegisterServicing {
    var cards: [CardBank] { get }
    var creditCard: CardBank? { get }
    var debitCard: CardBank? { get }
}

final class CvvRegisterService: CvvRegisterServicing {
    private let cardManager: CreditCardManagerContract
    
    var cards: [CardBank] {
        cardManager.cardList
    }
    
    var creditCard: CardBank? {
        cardManager.defaultCreditCard
    }
    
    var debitCard: CardBank? {
        cardManager.defaultDebitCard()
    }
        
    init(cardManager: CreditCardManager) {
        self.cardManager = cardManager
    }
}
