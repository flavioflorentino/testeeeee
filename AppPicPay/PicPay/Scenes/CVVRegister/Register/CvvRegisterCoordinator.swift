import UIKit

enum CvvRegisterAction {
    case success(cvv: String)
    case openInformation
    case emergencyAidInformation
    case close
}

protocol CardActiveCoordinatorDelegate: AnyObject {
    func cvvInformed(cvv: String)
    func cvvNotInformed()
    func didOpenInformation()
    func didOpenEmergencyAidInformation()
}

protocol CvvRegisterCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: CardActiveCoordinatorDelegate? { get set }
    func perform(action: CvvRegisterAction)
}

final class CvvRegisterCoordinator: CvvRegisterCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: CardActiveCoordinatorDelegate?
    
    func perform(action: CvvRegisterAction) {
        switch action {
        case .success(let cvv):
            delegate?.cvvInformed(cvv: cvv)
            
        case .openInformation:
            delegate?.didOpenInformation()

        case .emergencyAidInformation:
            delegate?.didOpenEmergencyAidInformation()

        case .close:
            delegate?.cvvNotInformed()
        }
    }
}
