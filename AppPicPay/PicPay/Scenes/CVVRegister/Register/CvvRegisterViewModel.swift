import AnalyticsModule
import Foundation

protocol CvvRegisterViewModelInputs: AnyObject { 
    func viewDidLoad()
    func updateCvv(value: String)
    func didTapInformation()
    func didTapContinue(value: String)
    func didTapClose()
}

final class CvvRegisterViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let service: CvvRegisterServicing
    private let origin: String
    private let presenter: CvvRegisterPresenting
    private let cardType: CardType
    private let isEmergencyAid: Bool
    
    init(
        origin: String,
        isEmergencyAid: Bool,
        cardType: CardType,
        service: CvvRegisterServicing,
        presenter: CvvRegisterPresenting,
        dependencies: Dependencies
    ) {
        self.dependencies = dependencies
        self.origin = origin
        self.isEmergencyAid = isEmergencyAid
        self.cardType = cardType
        self.service = service
        self.presenter = presenter
    }
}

extension CvvRegisterViewModel: CvvRegisterViewModelInputs {
    private var selectedCard: CardBank? {
        switch cardType {
        case .credit:
            return service.creditCard
        case .debit:
            return service.debitCard
        case let .id(id):
            return service.cards.first(where: { $0.id == id })
        }
    }
    
    func viewDidLoad() {
        guard let model = createCvvRegisterWithType() else {
            presenter.displayNotFoundCard()
            return
        }
        
        configureNumMaxCvv()
        presenter.displayCard(model: model, isEmergencyAid: isEmergencyAid)
    }
    
    func updateCvv(value: String) {
        presenter.displayButtonIsEnable(value: value.count >= 3)
    }
    
    func didTapContinue(value: String) {
        logEnterCvv(with: cardType)
        presenter.didNextStep(action: .success(cvv: value))
    }
    
    func didTapInformation() {
        let action: CvvRegisterAction = isEmergencyAid ? .emergencyAidInformation : .openInformation
        presenter.didNextStep(action: action)
    }
    
    func didTapClose() {
        logNotEnterCvv(with: cardType)
        presenter.didNextStep(action: .close)
    }
    
    private func configureNumMaxCvv() {
        guard
            let selectedCard = selectedCard,
            let flagId = Int(selectedCard.creditCardBandeiraId),
            let flag = CreditCardFlagTypeId(rawValue: flagId)
        else {
            presenter.diplayNumMaxCvv(limit: 4)
            return
        }
        
        presenter.diplayNumMaxCvv(limit: flag == .amex ? 4 : 3)
    }
    
    private func createCvvRegisterWithType() -> CvvRegister? {
        guard let selectedCard = selectedCard else {
            return nil
        }
        
        return CvvRegister(alias: selectedCard.alias, lastDigits: selectedCard.lastDigits, image: selectedCard.image)
    }
    
    private func logEnterCvv(with type: CardType) {
        guard let properties = createEventProperties(with: type) else {
            return
        }
        
        dependencies.analytics.log(CvvRegisterEvent.insertedCvv(properties: properties))
    }
    
    private func logNotEnterCvv(with type: CardType) {
        guard let properties = createEventProperties(with: type) else {
            return
        }
        
        dependencies.analytics.log(CvvRegisterEvent.notEnterCvv(properties: properties))
    }
    
    private func createEventProperties(with type: CardType) -> CvvRegisterEvent.CvvEventProperties? {
        guard let selectedCard = selectedCard else {
            return nil
        }
        
        return CvvRegisterEvent.CvvEventProperties(
            isDynamicCvv: selectedCard.isOneShotCvv ?? false,
            origin: origin,
            cardId: selectedCard.id
        )
    }
}

extension CvvRegisterViewModel {
    enum CardType: Equatable {
        case id(String)
        case debit
        case credit
    }
}
