import Foundation

enum CvvRegisterFactory {
    static func make(
        isEmergencyAid: Bool,
        origin: String,
        cardType: CvvRegisterViewModel.CardType,
        flow: CardActiveCoordinatorDelegate
    ) -> CvvRegisterViewController {
        let dependencies = DependencyContainer()
        let service: CvvRegisterServicing = CvvRegisterService(cardManager: CreditCardManager.shared)
        let presenter: CvvRegisterPresenting = CvvRegisterPresenter()
        let viewModel = CvvRegisterViewModel(
            origin: origin,
            isEmergencyAid: isEmergencyAid,
            cardType: cardType,
            service: service,
            presenter: presenter,
            dependencies: dependencies
        )
        let coordinator: CvvRegisterCoordinating = CvvRegisterCoordinator()
        let viewController = CvvRegisterViewController(viewModel: viewModel, coordinator: coordinator)

        coordinator.delegate = flow
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
