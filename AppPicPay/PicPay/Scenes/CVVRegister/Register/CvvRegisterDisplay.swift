import UIKit

protocol CvvRegisterDisplay: AnyObject {
    func displayAlert(message: String)
    func updateCard(model: CvvRegister)
    func buttonIsEnable(value: Bool)
    func configureInputField(limit: Int)
    func didNextStep(action: CvvRegisterAction)
    func displayViewState(viewState: CvvRegisterViewState)
    func hideRightBarButton()
}
