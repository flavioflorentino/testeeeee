import Foundation

protocol CvvEmergencyAidInformationViewModelInputs: AnyObject {
    func goBack()
    func openHelpCenter()
}

final class CvvEmergencyAidInformationViewModel {
    private let helpCenterURL = "picpay://picpay/helpcenter?query=preciso-de-ajuda-debito-caixa"

    private let presenter: CvvEmergencyAidInformationPresenting

    init(presenter: CvvEmergencyAidInformationPresenting) {
        self.presenter = presenter
    }
}

extension CvvEmergencyAidInformationViewModel: CvvEmergencyAidInformationViewModelInputs {
    func openHelpCenter() {
        guard let url = URL(string: helpCenterURL) else {
            return
        }
        presenter.didNextStep(action: .helpCenter(url: url))
    }

    func goBack() {
        presenter.didNextStep(action: .back)
    }
}
