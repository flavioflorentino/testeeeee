import Foundation
import UI
import UIKit

protocol CvvEmergencyAidInformationDisplay: AnyObject { }

private extension CvvEmergencyAidInformationViewController.Layout {
    enum Fonts {
        static let size: CGFloat = 16
        static let textFont = UIFont.systemFont(ofSize: size, weight: .regular)
        static let primaryButton = UIFont.systemFont(ofSize: 16, weight: .semibold)
    }
}

final class CvvEmergencyAidInformationViewController: ViewController<CvvEmergencyAidInformationViewModelInputs, UIView> {
    fileprivate enum Layout { }

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
        label.text = CvvLocalizable.cvvInfoTitle.text

        return label
    }()

    private lazy var descriptionTextView: UITextView = {
        let textView = UITextView()
        let attributedMessage = NSAttributedString(
            string: CvvLocalizable.cvvInformation.text,
            attributes: [.font: Layout.Fonts.textFont]
        )
            .boldfyWithSystemFont(ofSize: Layout.Fonts.size, weight: UIFont.Weight.bold.rawValue)
        textView.backgroundColor = .clear
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.textAlignment = .left
        textView.attributedText = attributedMessage
        textView.textColor = Palette.ppColorGrayscale500.color

        return textView
    }()

    private lazy var backButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.titleLabel?.font = Layout.Fonts.primaryButton
        button.setTitleColor(Colors.grayscale050.color, for: .normal)
        button.setTitle(DefaultLocalizable.back.text, for: .normal)
        button.addTarget(self, action: #selector(didTapBack), for: .touchUpInside)

        return button
    }()

    private lazy var needHelpButton: UIButton = {
        let button = UIButton()
        button.setTitle(EmergencyAidLocalizable.helperCenter.text, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(didTapHelpCenter), for: .touchUpInside)
        return button
    }()

    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(descriptionTextView)
        view.addSubview(backButton)
        view.addSubview(needHelpButton)
    }
    
    override func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        descriptionTextView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        backButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        needHelpButton.snp.makeConstraints {
            $0.top.equalTo(backButton.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base10)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }

    @objc
    private func didTapBack() {
        viewModel.goBack()
    }

    @objc
    private func didTapHelpCenter() {
        viewModel.openHelpCenter()
    }
}

extension CvvEmergencyAidInformationViewController: CvvEmergencyAidInformationDisplay { }
