import Foundation

protocol CvvEmergencyAidInformationPresenting: AnyObject {
    var viewController: CvvEmergencyAidInformationDisplay? { get set }
    func didNextStep(action: CvvEmergencyAidInformationAction)
}

final class CvvEmergencyAidInformationPresenter {
    private let coordinator: CvvEmergencyAidInformationCoordinating
    weak var viewController: CvvEmergencyAidInformationDisplay?

    init(coordinator: CvvEmergencyAidInformationCoordinating) {
        self.coordinator = coordinator
    }
}

extension CvvEmergencyAidInformationPresenter: CvvEmergencyAidInformationPresenting {    
    func didNextStep(action: CvvEmergencyAidInformationAction) {
        coordinator.perform(action: action)
    }
}
