import UIKit

enum CvvEmergencyAidInformationAction {
    case back
    case helpCenter(url: URL)
}

protocol CvvEmergencyAidInformationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: CvvEmergencyAidInformationAction)
}

final class CvvEmergencyAidInformationCoordinator {
    weak var viewController: UIViewController?
}

extension CvvEmergencyAidInformationCoordinator: CvvEmergencyAidInformationCoordinating {
    func perform(action: CvvEmergencyAidInformationAction) {
        switch action {
        case .back:
            viewController?.navigationController?.popViewController(animated: true)
        case .helpCenter(let url):
            guard let viewController = viewController else {
                return
            }
            DeeplinkHelper.handleDeeplink(withUrl: url, from: viewController)
        }
    }
}
