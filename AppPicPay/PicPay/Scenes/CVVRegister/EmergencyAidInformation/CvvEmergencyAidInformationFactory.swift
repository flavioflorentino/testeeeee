import Foundation

enum CvvEmergencyAidInformationFactory {
    static func make() -> CvvEmergencyAidInformationViewController {
        let coordinator: CvvEmergencyAidInformationCoordinating = CvvEmergencyAidInformationCoordinator()
        let presenter: CvvEmergencyAidInformationPresenting = CvvEmergencyAidInformationPresenter(coordinator: coordinator)
        let viewModel = CvvEmergencyAidInformationViewModel(presenter: presenter)
        let viewController = CvvEmergencyAidInformationViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
