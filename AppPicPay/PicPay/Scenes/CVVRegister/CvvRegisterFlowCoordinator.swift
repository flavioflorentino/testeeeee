import Core
import FeatureFlag
import UI
import UIKit

final class CvvRegisterFlowCoordinator: Coordinating {
    typealias Dependencies = HasKeychainManager
    private let rootNavigation: UINavigationController
    private let navigationController: UINavigationController
    private let cardType: CvvRegisterViewModel.CardType
    private let paymentType: PaymentType
    private let finishedCvv: ((String) -> Void)
    private let finishedWithoutCvv: (() -> Void)
    private let isEmergencyAid: Bool

    var childViewController: [UIViewController] = []
    var viewController: UIViewController?

    init(
        isEmergencyAid: Bool = false,
        cardType: CvvRegisterViewModel.CardType = .credit,
        navigationController: UINavigationController,
        paymentType: PaymentType,
        finishedCvv: @escaping ((String) -> Void),
        finishedWithoutCvv: @escaping (() -> Void)
    ) {
        self.isEmergencyAid = isEmergencyAid
        self.cardType = cardType
        self.rootNavigation = navigationController
        self.paymentType = paymentType
        self.navigationController = UINavigationController()
        self.finishedCvv = finishedCvv
        self.finishedWithoutCvv = finishedWithoutCvv
    }

    /// 💩💩💩💩💩💩💩💩💩💩💩💩💩
    /// Eu sei que isso não é responsablidade do CvvRegisterFlowCoordinator,  é apenas uma tentaiva de diminuir a repetição de código enquanto não temos
    /// uma base comum de pagamento.
    static func cvvFlowEnable(cardBank: CardBank, cardValue: Double, paymentType: PaymentType) -> Bool {
        let dependencies: Dependencies = DependencyContainer()
        let id = cardBank.id
        let flagId = cardBank.creditCardBandeiraId
        let typeFlag = CreditCardFlagTypeId(rawValue: Int(flagId) ?? 0)
        let cardCvv = dependencies.keychain.getData(key: KeychainKeyPF.cvv("\(id)"))

        guard paymentType.pciIsEnable else {
            return false
        }

        return cardCvv == nil && typeFlag != .picpay && cardValue != .zero
    }

    func start() {
        let controller = CvvRegisterFactory.make(isEmergencyAid: isEmergencyAid,
                                                 origin: paymentType.rawValue,
                                                 cardType: cardType,
                                                 flow: self)
        setupAppearance()
        navigationController.setViewControllers([controller], animated: true)
        rootNavigation.present(navigationController, animated: true)
    }

    private func setupAppearance() {
        #if swift(>=5.1)
        if #available(iOS 13.0, *) {
            navigationController.isModalInPresentation = true
        }
        #endif

        navigationController.modalPresentationStyle = .overCurrentContext
        navigationController.navigationBar.barTintColor = Palette.ppColorGrayscale000.color
        navigationController.view.backgroundColor = Palette.ppColorGrayscale000.color
    }
}

extension CvvRegisterFlowCoordinator: CardActiveCoordinatorDelegate {
    func cvvInformed(cvv: String) {
        navigationController.dismiss(animated: true) { [weak self] in
            self?.finishedCvv(cvv)
        }
    }

    func cvvNotInformed() {
        navigationController.dismiss(animated: true) { [weak self] in
            self?.finishedWithoutCvv()
        }
    }

    func didOpenInformation() {
        let controller = CvvInformationFactory.make()
        navigationController.pushViewController(controller, animated: true)
    }

    func didOpenEmergencyAidInformation() {
        let controller = CvvEmergencyAidInformationFactory.make()
        navigationController.pushViewController(controller, animated: true)
    }
}

extension CvvRegisterFlowCoordinator {
    enum PaymentType: String {
        case p2p
        case pav
        case p2m
        case linx
        case bluezone
        case subscription
        case invoice
        case birthday
        case recharge
        case ecommerce
        case bill
        case parking
        case phoneRecharge
        case transitPass
        case tvRecharge
        case voucher
        case friendGift
        case cardVerification
        case newPayment

        var pciIsEnable: Bool {
            switch self {
            case .p2p:
                return FeatureManager.isActive(.pciP2P)
            case .pav:
                return FeatureManager.isActive(.pciPAV)
            case .invoice:
                return FeatureManager.isActive(.pciInvoiceCard)
            case .p2m:
                return FeatureManager.isActive(.pciP2M)
            case .linx:
                return FeatureManager.isActive(.pciLinx)
            case .bluezone:
                return FeatureManager.isActive(.pciBluezone)
            case .subscription:
                return FeatureManager.isActive(.pciSubscription)
            case .recharge:
                return FeatureManager.isActive(.pciRecharge)
            case .birthday:
                return FeatureManager.isActive(.pciBirthday)
            case .ecommerce:
                return FeatureManager.isActive(.pciEcommerce)
            case .bill:
                return FeatureManager.isActive(.pciBill)
            case .parking:
                return FeatureManager.isActive(.pciParking)
            case .phoneRecharge:
                return FeatureManager.isActive(.pciPhoneRecharge)
            case .transitPass:
                return FeatureManager.isActive(.pciTransitPass)
            case .tvRecharge:
                return FeatureManager.isActive(.pciTvRecharge)
            case .voucher:
                return FeatureManager.isActive(.pciVoucher)
            case .friendGift:
                return FeatureManager.isActive(.pciNewFriendGift)
            case .cardVerification:
                return FeatureManager.isActive(.pciCardVerification)
            default:
                return false
            }
        }
    }
}
