import Foundation

enum CvvLocalizable: String, Localizable {
    case titleInformation
    case titleRegister
    case descInformation
    case aliasPlaceholder
    case notFoundCard
    case emergencyAidInformation
    case alternativeTitle
    case cvvInformation
    case cvvInfoTitle
    
    var key: String {
        rawValue
    }
    
    var file: LocalizableFile {
        .cvv
    }
}
