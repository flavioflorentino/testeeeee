import AnalyticsModule
import Foundation

enum CvvRegisterEvent: AnalyticsKeyProtocol {
    case insertedCvv(properties: CvvEventProperties)
    case notEnterCvv(properties: CvvEventProperties)
    
    private var name: String {
        "Tela Cvv"
    }
    
    private var properties: [String: Any] {
        switch self {
        case .insertedCvv(let properties):
            return [
                KeyStrings.method: properties.origin,
                KeyStrings.dynamicCvv: properties.isDynamicCvv,
                KeyStrings.cardId: properties.cardId,
                KeyStrings.inserted: true
            ]
        case .notEnterCvv(let properties):
            return [
                KeyStrings.method: properties.origin,
                KeyStrings.dynamicCvv: properties.isDynamicCvv,
                KeyStrings.cardId: properties.cardId,
                KeyStrings.inserted: false
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.mixPanel, .appsFlyer, .firebase])
    }
}

extension CvvRegisterEvent {
    private enum KeyStrings {
        static let method = "payment_method"
        static let dynamicCvv = "is_dynamic_cvv"
        static let inserted = "is_insert_cvv"
        static let cardId = "card_id"
    }
    
    struct CvvEventProperties {
        let isDynamicCvv: Bool
        let origin, cardId: String
    }
}
