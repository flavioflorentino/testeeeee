import Foundation

struct CvvRegister {
    let alias: String
    let lastDigits: String
    let image: String
}
