import Foundation
import CoreLegacy

protocol PaymentBalancePresenting: AnyObject {
    var viewController: PaymentBalanceDisplay? { get set }
    func displayBalance(value: Double, isEnable: Bool)
}

final class PaymentBalancePresenter: PaymentBalancePresenting {
    typealias Dependencies = HasNoDependency
    weak var viewController: PaymentBalanceDisplay?
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func displayBalance(value: Double, isEnable: Bool) {
        displayCurrency(value: value)
        viewController?.isBalanceOn(enable: isEnable)
    }
    
    private func displayCurrency(value: Double) {
        guard let formattedValue = CurrencyFormatter.brazillianRealString(from: value as NSNumber) else {
            let error = PicPayError(message: RechargeLocalizable.errorValue.text)
            viewController?.didReceiveAnError(error)
            return
        }
        
        viewController?.displayBalance(value: formattedValue)
    }
}
