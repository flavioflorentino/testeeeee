import UI
import UIKit

final class PaymentBalanceViewController: ViewController<PaymentBalanceViewModelInputs, UIView> {
    private enum Layout {
        static let marginLeadingTrailing: CGFloat = 16
        static let marginTopBottom: CGFloat = 8
    }
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.spacing = 8.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .leading
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 4.0
        
        return stackView
    }()
    
    private lazy var viewWhite: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale000.color
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.textColor = Palette.ppColorGrayscale500.color
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var titleBalanceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        label.textColor = Palette.ppColorGrayscale500.color
        
        return label
    }()
    
    private lazy var balanceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.textColor = Palette.ppColorGrayscale400.color
        
        return label
    }()
    
    private lazy var balanceSwitch: UISwitch = {
        let balance = UISwitch()
        balance.onTintColor = Palette.ppColorBranding300.color
        balance.addTarget(self, action: #selector(switchChanged), for: .valueChanged)
        
        return balance
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad()
    }
 
    override func buildViewHierarchy() {
        stackView.addArrangedSubview(titleBalanceLabel)
        stackView.addArrangedSubview(balanceLabel)
        
        rootStackView.addArrangedSubview(stackView)
        rootStackView.addArrangedSubview(balanceSwitch)
        
        viewWhite.addSubview(rootStackView)
        view.addSubview(titleLabel)
        view.addSubview(viewWhite)
    }
    
    override func configureViews() {
        view.backgroundColor = .clear
        titleLabel.text = RechargeLocalizable.walletBalance.text
        titleBalanceLabel.text = RechargeLocalizable.paymentBalance.text
    }
    
    override func setupConstraints() {
        let trailingTitle = titleLabel.trailingAnchor.constraint(
            equalTo: view.trailingAnchor,
            constant: -Layout.marginLeadingTrailing
        )
        let trailingView = viewWhite.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        trailingTitle.priority = UILayoutPriority(750)
        trailingView.priority = UILayoutPriority(750)
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(greaterThanOrEqualTo: view.topAnchor, constant: Layout.marginTopBottom),
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Layout.marginLeadingTrailing),
            trailingTitle
        ])
        
        NSLayoutConstraint.activate([
            viewWhite.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Layout.marginTopBottom),
            viewWhite.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            trailingView,
            viewWhite.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            rootStackView.topAnchor.constraint(equalTo: viewWhite.topAnchor, constant: Layout.marginTopBottom),
            rootStackView.leadingAnchor.constraint(equalTo: viewWhite.leadingAnchor, constant: Layout.marginLeadingTrailing),
            rootStackView.trailingAnchor.constraint(equalTo: viewWhite.trailingAnchor, constant: -Layout.marginLeadingTrailing),
            rootStackView.bottomAnchor.constraint(equalTo: viewWhite.bottomAnchor, constant: -Layout.marginTopBottom)
        ])
    }
    
    @objc
    func switchChanged(mySwitch: UISwitch) {
        let value = mySwitch.isOn
        viewModel.setUseBalance(value)
    }
}

// MARK: Presenter Outputs
extension PaymentBalanceViewController: PaymentBalanceDisplay {
    func displayBalance(value: String) {
        balanceLabel.text = value
    }
    
    func isBalanceOn(enable: Bool) {
        balanceSwitch.isOn = enable
    }
    
    func didReceiveAnError(_ error: PicPayError) {
        AlertMessage.showCustomAlertWithError(error, controller: self)
    }
}
