import UIKit

protocol PaymentBalanceDisplay: AnyObject {
    func displayBalance(value: String)
    func isBalanceOn(enable: Bool)
    func didReceiveAnError(_ error: PicPayError)
}
