import Core
import Foundation

protocol PaymentBalanceViewModelInputs {
    func viewDidLoad()
    func setUseBalance(_ value: Bool)
}

final class PaymentBalanceViewModel {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies
    private let service: PaymentBalanceServicing
    private let presenter: PaymentBalancePresenting

    init(service: PaymentBalanceServicing, presenter: PaymentBalancePresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

extension PaymentBalanceViewModel: PaymentBalanceViewModelInputs {
    func viewDidLoad() {
        let value = service.balanceValue
        let isEnable = service.balanceIsBeingUsed()
        presenter.displayBalance(value: value, isEnable: isEnable)
    }
    
    func setUseBalance(_ value: Bool) {
        service.setUseBalance(isEnable: value)
    }
}
