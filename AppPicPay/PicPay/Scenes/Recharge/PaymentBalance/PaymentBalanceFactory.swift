import Foundation

enum PaymentBalanceFactory {
    static func make() -> PaymentBalanceViewController {
        let container = DependencyContainer()
        let service: PaymentBalanceServicing = PaymentBalanceService(dependencies: container)
        let presenter: PaymentBalancePresenting = PaymentBalancePresenter(dependencies: container)
        let viewModel = PaymentBalanceViewModel(
            service: service,
            presenter: presenter,
            dependencies: container
        )
        let viewController = PaymentBalanceViewController(viewModel: viewModel)
        
        presenter.viewController = viewController
        
        return viewController
    }
}
