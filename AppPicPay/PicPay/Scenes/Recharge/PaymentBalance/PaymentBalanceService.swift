import Foundation

protocol PaymentBalanceServicing {
    var balanceValue: Double { get }
    func balanceIsBeingUsed() -> Bool
    func setUseBalance(isEnable: Bool)
}

final class PaymentBalanceService: PaymentBalanceServicing {
    typealias Dependencies = HasConsumerManager
    private var dependencies: Dependencies
    
    var balanceValue: Double {
        dependencies.consumerManager.consumer?.balance?.doubleValue ?? 0.0
    }
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func balanceIsBeingUsed() -> Bool {
        dependencies.consumerManager.useBalance()
    }
    
    func setUseBalance(isEnable: Bool) {
        dependencies.consumerManager.setUseBalance(isEnable)
    }
}
