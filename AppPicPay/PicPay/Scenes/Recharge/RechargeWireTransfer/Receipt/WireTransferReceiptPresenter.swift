import Foundation
import UI
import UIKit

protocol WireTransferReceiptPresenting: AnyObject {
    var viewController: WireTransferReceiptDisplaying? { get set }
    func populate(with viewModel: WireTransferReceiptViewModel)
    func didNextStep(action: WireTransferReceiptAction)
    
    func showLoading()
    func showError()
    func share(_ image: UIImage)
    func close()
}

final class WireTransferReceiptPresenter {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let coordinator: WireTransferReceiptCoordinating
    weak var viewController: WireTransferReceiptDisplaying?

    init(coordinator: WireTransferReceiptCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - WireTransferReceiptPresenting
extension WireTransferReceiptPresenter: WireTransferReceiptPresenting {
    func populate(with viewModel: WireTransferReceiptViewModel) {
        viewController?.display(state: .loaded(viewModel: viewModel))
    }
    
    func showLoading() {
        viewController?.display(state: .loading)
    }
    
    func showError() {
        let viewModel = StatefulErrorViewModel(
            image: Assets.Recharge.iluWireTransferReceiptErrorClock.image,
            content: (title: Strings.RechargeWireTransfer.receiptLoadingErrorTitle,
                      description: Strings.RechargeWireTransfer.receiptLoadingErrorMessage),
            button: (image: nil, title: Strings.RechargeWireTransfer.Default.Error.tryAgain)
        )
        viewController?.display(state: .error(viewModel: viewModel))
    }

    func didNextStep(action: WireTransferReceiptAction) {
        coordinator.perform(action: action)
    }
    
    func close() {
        viewController?.close()
    }

    func share(_ image: UIImage) {
        viewController?.share(image)
    }
}
