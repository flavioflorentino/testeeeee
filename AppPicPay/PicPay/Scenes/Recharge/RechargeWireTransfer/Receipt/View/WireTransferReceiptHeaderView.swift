import SnapKit
import UI
import UIKit

final class WireTransferReceiptHeaderView: UIView, ViewConfiguration {
    private typealias Texts = Strings.RechargeWireTransfer
    
    private(set) lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        return label
    }()
    
    private(set) lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
        return label
    }()
    
    private(set) lazy var imageView: UIView = {
        let imageView = UIImageView()
        imageView.image = Assets.Recharge.icoWireTransfer.image
        return imageView
    }()
    
    func buildViewHierarchy() {
        addSubviews(titleLabel,
                    infoLabel,
                    imageView)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.trailing.lessThanOrEqualToSuperview().offset(-Spacing.base02)
        }
        
        imageView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.top)
            $0.leading.equalToSuperview()
            $0.bottom.lessThanOrEqualToSuperview().offset(-Spacing.base01)
            $0.trailing.equalTo(titleLabel.snp.leading).offset(-Spacing.base02)
        }
        
        infoLabel.snp.makeConstraints {
            $0.leading.trailing.equalTo(titleLabel)
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.bottom.equalToSuperview()
        }
    }
    
    func setUp(with viewModel: WireTransferReceiptHeaderViewModel) {
        buildLayout()
        
        titleLabel.text = Texts.receiptHeaderTitle
        infoLabel.text = Texts.receiptHeaderInfo(viewModel.depositId, viewModel.depositDate)
    }
}
