import SnapKit
import UI
import UIKit

final class WireTransferReceiptView: UIView, ViewConfiguration {
    private(set) lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.isHidden = true
        return scrollView
    }()
    
    private(set) lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private(set) lazy var headerView = WireTransferReceiptHeaderView()
    private(set) lazy var valueView = WireTransferReceiptValueView()
    private(set) lazy var infoView = WireTransferReceiptInfoView()
    private(set) lazy var footerView = WireTransferReceiptContactView()
    
    func buildViewHierarchy() {
        addSubview(scrollView)
        scrollView.addSubview(stackView)
        stackView.addArrangedSubviews(headerView, valueView, infoView, footerView)
    }
    
    func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.width.equalToSuperview()
        }
        
        stackView.snp.makeConstraints {
            $0.leading.top.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().inset(Spacing.base04)
            $0.width.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        footerView.configureViews()
        viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        stackView.insertLine(after: headerView, color: Colors.grayscale500.color)
        stackView.insertLine(after: valueView, color: Colors.grayscale500.color)
        stackView.insertLine(after: infoView, color: Colors.grayscale500.color)
    }
    
    func setUp(with viewModel: WireTransferReceiptViewModel) {
        scrollView.isHidden = false
        headerView.setUp(with: viewModel.header)
        valueView.setUp(value: viewModel.value)
        infoView.setUp(with: viewModel.info)
        
        guard let contactInfo = viewModel.contactInfo else { return }
        if contactInfo.shouldDisplay {
            footerView.setUpContactInfo(title: contactInfo.title,
                                        channels: contactInfo.channels)
        }
        
        footerView.setUp(cnpj: contactInfo.cnpj)
    }
    
    func updateLogo() {
        footerView.updateLogo()
    }
}
