import SnapKit
import UI
import UIKit

final class WireTransferReceiptInfoView: UIStackView {
    private typealias Texts = Strings.RechargeWireTransfer
    
    private(set) lazy var originSectionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
        label.text = Texts.receiptOriginSection
        return label
    }()
    
    private(set) lazy var destinationSectionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
        label.text = Texts.receiptDestinationSection
        return label
    }()
    
    private func buildOriginStack(with info: ReceiptOriginInfoViewModel) {
        addArrangedSubviews(originSectionLabel,
                            WireTransferReceiptInfoComponent(title: Texts.receiptOriginName, info: info.name),
                            WireTransferReceiptInfoComponent(title: info.documentType.rawValue, info: info.document),
                            WireTransferReceiptInfoComponent(title: Texts.receiptOriginBank, info: info.bank),
                            WireTransferReceiptInfoComponent(title: Texts.receiptOrigingAccount, info: info.account))
    }
    
    private func buildDestinationStack(with info: ReceiptDestinationInfoViewModel) {
        addArrangedSubviews(destinationSectionLabel,
                            WireTransferReceiptInfoComponent(title: Texts.receiptDestinationName, info: info.name),
                            WireTransferReceiptInfoComponent(title: Texts.receiptDestinationAgency, info: info.agency),
                            WireTransferReceiptInfoComponent(title: Texts.receiptDestinationAccount, info: info.account))
    }
    
    func setUp(with info: WireTransferReceiptInfoViewModel) {
        axis = .vertical
        spacing = Spacing.base02
        removeAllSubviews()
        buildOriginStack(with: info.originInfo)
        buildDestinationStack(with: info.destinationInfo)
    }
}
