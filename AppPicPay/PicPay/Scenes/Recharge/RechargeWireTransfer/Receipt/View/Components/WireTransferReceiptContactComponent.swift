import SnapKit
import UI
import UIKit

final class WireTransferReceiptContactComponent: UIView, ViewConfiguration {
    private(set) lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale600.color)
        return label
    }()
    
    private(set) lazy var phoneLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, Colors.grayscale600.color)
        return label
    }()
    
    func buildViewHierarchy() {
        addSubviews(nameLabel,
                    phoneLabel)
    }
    
    func setupConstraints() {
        nameLabel.snp.makeConstraints {
            $0.top.leading.bottom.equalToSuperview()
        }
        
        phoneLabel.snp.makeConstraints {
            $0.leading.equalTo(nameLabel.snp.trailing).offset(Spacing.base01)
            $0.top.trailing.bottom.equalToSuperview()
        }
    }
    
    func setUp(with channel: ReceiptContactChannel) {
        buildLayout()
        
        nameLabel.text = channel.name
        phoneLabel.text = channel.phone
    }
}
