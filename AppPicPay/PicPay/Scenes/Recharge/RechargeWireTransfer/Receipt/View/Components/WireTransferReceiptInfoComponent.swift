import SnapKit
import UI
import UIKit

final class WireTransferReceiptInfoComponent: UIView, ViewConfiguration {
    private(set) lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale500())
        return label
    }()
    
    private(set) lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
        return label
    }()
    
    init(title: String, info: String) {
        super.init(frame: .zero)
        buildLayout()
        
        titleLabel.text = title
        infoLabel.text = info
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(titleLabel,
                    infoLabel)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.leading.top.trailing.equalToSuperview()
        }
        
        infoLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom)
            $0.leading.bottom.trailing.equalToSuperview()
        }
    }
}
