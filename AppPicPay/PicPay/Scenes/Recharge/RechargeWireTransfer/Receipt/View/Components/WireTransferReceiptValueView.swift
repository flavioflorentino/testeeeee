import SnapKit
import UI
import UIKit

final class WireTransferReceiptValueView: UIView, ViewConfiguration {
    private(set) lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
        label.text = Strings.RechargeWireTransfer.receiptValue
        return label
    }()
    
    private(set) lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
        return label
    }()
    
    func buildViewHierarchy() {
        addSubviews(titleLabel,
                    valueLabel)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview()
        }
        
        valueLabel.snp.makeConstraints {
            $0.leading.greaterThanOrEqualTo(titleLabel.snp.trailing).offset(Spacing.base01)
            $0.top.bottom.equalToSuperview()
            $0.trailing.equalToSuperview()
        }
    }
    
    func setUp(value: String) {
        buildLayout()
        
        valueLabel.text = value
    }
}
