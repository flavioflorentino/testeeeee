import SnapKit
import UI
import UIKit

final class WireTransferReceiptContactView: UIStackView {
    private(set) lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale600.color)
        return label
    }()
    
    private(set) lazy var logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = logoImage
        
        return imageView
    }()
    
    private(set) lazy var cnpjLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, Colors.grayscale400.color)
        return label
    }()
    
    private var logoImage: UIImage {
        if #available(iOS 12.0, *), traitCollection.userInterfaceStyle == .dark {
            return Assets.NewGeneration.logoPicPayReceiptFooterWhite.image
        }
        return Assets.Recharge.icoLogoPicpayReceiptFooterGray.image
    }
    
    func configureViews() {
        axis = .vertical
        alignment = .center
        spacing = Spacing.base01
    }
    
    func setUpContactInfo(title: String, channels: [ReceiptContactChannel]) {
        titleLabel.removeFromSuperview()
        titleLabel.text = title
        
        addArrangedSubview(titleLabel)
        
        channels.forEach {
            let channelComponent = WireTransferReceiptContactComponent()
            channelComponent.setUp(with: $0)
            addArrangedSubview(channelComponent)
        }
    }
    
    func setUp(cnpj: String) {
        logoImageView.removeFromSuperview()
        cnpjLabel.removeFromSuperview()
        
        cnpjLabel.text = cnpj
        
        addArrangedSubview(logoImageView)
        addArrangedSubview(cnpjLabel)
    }
    
    func updateLogo() {
        logoImageView.image = logoImage
    }
}
