import Foundation

enum WireTransferReceiptFactory {
    static func make(receiptId: String) -> WireTransferReceiptViewController {
        let container = DependencyContainer()
        let service: WireTransferReceiptServicing = WireTransferReceiptService(dependencies: container)
        let coordinator: WireTransferReceiptCoordinating = WireTransferReceiptCoordinator(dependencies: container)
        let presenter: WireTransferReceiptPresenting = WireTransferReceiptPresenter(coordinator: coordinator, dependencies: container)
        let interactor = WireTransferReceiptInteractor(receiptId: receiptId, service: service, presenter: presenter, dependencies: container)
        let viewController = WireTransferReceiptViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
