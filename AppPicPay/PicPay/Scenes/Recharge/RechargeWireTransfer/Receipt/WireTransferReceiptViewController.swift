import SnapKit
import UI
import UIKit

enum WireTransferReceiptState {
    case loading
    case error(viewModel: StatefulErrorViewModel)
    case loaded(viewModel: WireTransferReceiptViewModel)
}

protocol WireTransferReceiptDisplaying: AnyObject {
    func display(state: WireTransferReceiptState)
    func share(_ image: UIImage)
    func close()
}

final class WireTransferReceiptViewController: ViewController<WireTransferReceiptInteracting, WireTransferReceiptView> {
    private lazy var optionsBarButton: UIBarButtonItem = {
        let shareImage = Assets.Promotions.share.image.resizeImage(targetSize: CGSize(width: 32, height: 32))
        let barButtonItem = UIBarButtonItem(image: shareImage,
                                            style: .plain,
                                            target: self,
                                            action: #selector(shareScreenSnapshot))
        return barButtonItem
    }()
    
    private lazy var closeBarButtonItem: UIBarButtonItem = {
        let barButtonItem = UIBarButtonItem(image: Assets.Icons.iconClose.image,
                                            style: .plain,
                                            target: self,
                                            action: #selector(didTapClose))

        barButtonItem.accessibilityLabel = DefaultLocalizable.btClose.text
        return barButtonItem
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.loadInfo()
        setUpNavigation()
    }

    override func configureViews() {
        rootView.buildLayout()
    }

    private func setUpNavigation() {
        setUpNavigationBarDefaultAppearance()
        navigationItem.title = Strings.RechargeWireTransfer.receiptNavigationTitle
        navigationItem.leftBarButtonItem = closeBarButtonItem
    }
    
    private func setUpShareButton() {
        navigationItem.rightBarButtonItem = optionsBarButton
    }
    
    @objc
    private func didTapClose() {
        interactor.close()
    }

    @objc
    func shareScreenSnapshot() {
        if #available(iOS 13.0, *) {
            let currentStyle = traitCollection.userInterfaceStyle
            overrideUserInterfaceStyle = .light
            rootView.updateLogo()
            if let snapshot = takeSnapshot() {
                interactor.share(snapshot)
            }
            overrideUserInterfaceStyle = currentStyle
            rootView.updateLogo()
        } else if let snapshot = takeSnapshot() {
            interactor.share(snapshot)
        }
    }
    
    private func takeSnapshot() -> UIImage? {
        guard let receiptImage = rootView.snapshot else { return nil }
        let cropFrame = CGRect(x: 0, y: 0, width: rootView.frame.width, height: rootView.stackView.frame.height + Spacing.base04)
        let croppedImage = receiptImage.croppedImage(withFrame: cropFrame, angle: 0, circularClip: false)
        return croppedImage
    }
    
    private func showLoadedView(with viewModel: WireTransferReceiptViewModel) {
        setUpShareButton()
        rootView.setUp(with: viewModel)
        endState()
    }
}

// MARK: WireTransferReceiptDisplay
extension WireTransferReceiptViewController: WireTransferReceiptDisplaying {
    func display(state: WireTransferReceiptState) {
        switch state {
        case .loading:
            beginState()
            
        case .error(let error):
            endState(model: error)
            
        case .loaded(let viewModel):
            showLoadedView(with: viewModel)
        }
    }

    func share(_ image: UIImage) {
        let controller = UIActivityViewController(activityItems: [image], applicationActivities: nil)
        present(controller, animated: true)
    }
    
    func close() {
        self.dismiss(animated: true)
    }
}

extension WireTransferReceiptViewController: StatefulTransitionViewing {
    func didTryAgain() {
        interactor.loadInfo()
    }
}
