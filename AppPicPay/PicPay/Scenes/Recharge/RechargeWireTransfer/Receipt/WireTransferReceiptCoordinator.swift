import UIKit

enum WireTransferReceiptAction {
    case back
}

protocol WireTransferReceiptCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: WireTransferReceiptAction)
}

final class WireTransferReceiptCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - WireTransferReceiptCoordinating
extension WireTransferReceiptCoordinator: WireTransferReceiptCoordinating {
    func perform(action: WireTransferReceiptAction) {
    }
}
