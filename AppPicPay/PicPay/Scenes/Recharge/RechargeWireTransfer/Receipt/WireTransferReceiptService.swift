import Core
import FeatureFlag
import Foundation

protocol WireTransferReceiptServicing {
    func fetchReceiptInfo(receiptId: String, completion: @escaping (Result<WireTransferReceiptModel, ApiError>) -> Void)
    var contactInfo: WireTransferContactInfo? { get }
}

final class WireTransferReceiptService {
    typealias Dependencies = HasMainQueue & HasFeatureManager
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - WireTransferReceiptServicing
extension WireTransferReceiptService: WireTransferReceiptServicing {
    var contactInfo: WireTransferContactInfo? {
        dependencies.featureManager.object(.featureTransferReceiptContactInfo, type: WireTransferContactInfo.self)
    }
    
    func fetchReceiptInfo(receiptId: String, completion: @escaping (Result<WireTransferReceiptModel, ApiError>) -> Void) {
        let endpoint = WireTransferReceiptEndpoint.getWireTransferReceipt(receiptId: receiptId)
        let api = Api<WireTransferReceiptModel>(endpoint: endpoint)
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] response in
            self?.dependencies.mainQueue.async {
                completion(response.map(\.model))
            }
        }
    }
}

enum WireTransferReceiptEndpoint {
    case getWireTransferReceipt(receiptId: String)
}

extension WireTransferReceiptEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case let .getWireTransferReceipt(rechargeId):
            return "api/recharge/\(rechargeId)/receipt"
        }
    }
}
