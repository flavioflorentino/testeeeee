import Foundation

struct WireTransferContactInfo: Decodable {
    let isContactInformationEnabled: Bool
    let sacPhoneNumber: String
    let ombudsmanPhoneNumber: String
}
