import Foundation

struct WireTransferReceiptViewModel {
    let header: WireTransferReceiptHeaderViewModel
    let value: String
    let info: WireTransferReceiptInfoViewModel
    let contactInfo: ReceiptContactInfo?
}

struct WireTransferReceiptHeaderViewModel {
    let depositId: String
    let depositDate: String
}

struct WireTransferReceiptInfoViewModel {
    let originInfo: ReceiptOriginInfoViewModel
    let destinationInfo: ReceiptDestinationInfoViewModel
}

enum ReceiptDocumentType: String {
    case cpf = "CPF"
    case cnpj = "CNPJ"
    
    static func getType(from documentString: String) -> ReceiptDocumentType {
        if documentString.byRemovingPunctuation.count > 11 {
            return .cnpj
        } else {
            return .cpf
        }
    }
}

struct ReceiptOriginInfoViewModel {
    let name: String
    let documentType: ReceiptDocumentType
    let document: String
    let bank: String
    let account: String
}

struct ReceiptDestinationInfoViewModel {
    let name: String
    let agency: String
    let account: String
}

struct ReceiptContactInfo: Decodable {
    let shouldDisplay: Bool
    let title: String
    let channels: [ReceiptContactChannel]
    let cnpj: String
}

struct ReceiptContactChannel: Decodable {
    let name: String
    let phone: String
}
