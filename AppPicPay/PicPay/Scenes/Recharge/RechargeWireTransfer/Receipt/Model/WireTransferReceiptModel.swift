import Foundation

struct WireTransferReceiptModel: Decodable {
    let value: String
    let rechargeDate: String
    let rechargeId: String
    
    let sourceDocument: String
    let sourceFullName: String
    let sourceBankName: String
    let sourceAccountNumber: String
    let sourceAgency: String
    
    let destinationFullName: String
    let destinationAccountNumber: String
    let destinationAgency: String
    let destinationBankName: String
    
    private enum CodingKeys: String, CodingKey {
        case value
        case rechargeDate = "debitDateFooter"
        case rechargeId
        
        case sourceDocument
        case sourceFullName
        case sourceBankName
        case sourceAccountNumber = "sourceAccount"
        case sourceAgency
        
        case destinationFullName = "fullName"
        case destinationAccountNumber = "accountNumber"
        case destinationAgency = "accountAgency"
        case destinationBankName = "bankName"
    }
}
