import AnalyticsModule
import Foundation
import UIKit

protocol WireTransferReceiptInteracting: AnyObject {
    func loadInfo()
    func share(_ image: UIImage)
    func close()
}

final class WireTransferReceiptInteractor {
    typealias Localizable = Strings.RechargeWireTransfer.Receipt
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let receiptId: String
    private let service: WireTransferReceiptServicing
    private let presenter: WireTransferReceiptPresenting

    init(receiptId: String, service: WireTransferReceiptServicing, presenter: WireTransferReceiptPresenting, dependencies: Dependencies) {
        self.receiptId = receiptId
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
    
    private func sendShareEvent() {
        let event = RechargeEvent.receiptSharing
        dependencies.analytics.log(event)
    }
}

// MARK: - WireTransferReceiptInteracting
extension WireTransferReceiptInteractor: WireTransferReceiptInteracting {
    func loadInfo() {
        presenter.showLoading()
        service.fetchReceiptInfo(receiptId: receiptId) { [weak self] result in
            switch result {
            case let .success(data):
                guard let viewModel = self?.mapModel(with: data) else {
                    self?.presenter.showError()
                    return
                }

                self?.presenter.populate(with: viewModel)

            case .failure:
                self?.presenter.showError()
            }
        }
    }
    
    func close() {
        presenter.close()
    }
    
    func share(_ image: UIImage) {
        presenter.share(image)
        sendShareEvent()
    }
}

private extension WireTransferReceiptInteractor {
    func mapModel(with model: WireTransferReceiptModel) -> WireTransferReceiptViewModel {
        let documentType = ReceiptDocumentType.getType(from: model.sourceDocument)
        
        let header = WireTransferReceiptHeaderViewModel(depositId: model.rechargeId,
                                                        depositDate: model.rechargeDate)
        
        let originInfo = ReceiptOriginInfoViewModel(name: model.sourceFullName,
                                                    documentType: documentType,
                                                    document: model.sourceDocument,
                                                    bank: model.sourceBankName,
                                                    account: model.sourceAccountNumber)
        
        let destinationInfo = ReceiptDestinationInfoViewModel(name: model.destinationFullName,
                                                              agency: model.destinationAgency,
                                                              account: model.destinationAccountNumber)
        
        let info = WireTransferReceiptInfoViewModel(originInfo: originInfo,
                                                    destinationInfo: destinationInfo)
        
        return WireTransferReceiptViewModel(header: header,
                                            value: "R$ \(model.value)",
                                            info: info,
                                            contactInfo: mapContactInfo())
    }
    
    func mapContactInfo() -> ReceiptContactInfo? {
        guard let contactInfo = service.contactInfo else { return nil }
        return ReceiptContactInfo(shouldDisplay: contactInfo.isContactInformationEnabled,
                                  title: Localizable.ContactInfo.title,
                                  channels: [
                                    ReceiptContactChannel(name: Localizable.ContactInfo.sac,
                                                          phone: contactInfo.sacPhoneNumber),
                                    ReceiptContactChannel(name: Localizable.ContactInfo.ombudsman,
                                                          phone: contactInfo.ombudsmanPhoneNumber)
                                  ],
                                  cnpj: Strings.RechargeWireTransfer.Receipt.ContactInfo.cnpj)
    }
}
