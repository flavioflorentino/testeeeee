import UI
import UIKit

protocol RechargeWireTransferDisplay: AnyObject {
    func shareInfo(_ info: [Any])
    func setup(userInfo: RechargeWireTransferUserInfo, bankInfo: RechargeWireTransferBankInfo)
}

final class RechargeWireTransferViewController: ViewController<RechargeWireTransferInteracting, UIView> {
    fileprivate enum Layout { }

    private lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.alwaysBounceVertical = false
        return view
    }()

    private lazy var moreInfoBarButton: UIBarButtonItem = {
        let barButtonItem = UIBarButtonItem(image: Assets.UpgradeChecklist.greenHelpButton.image,
                                            style: .done,
                                            target: self,
                                            action: #selector(didTapMoreInfo))
        return barButtonItem
    }()

    private lazy var accountView = RechargeWireTransferAccountView(delegate: self)

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.loadInfo()
    }

    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(accountView)
    }

    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        accountView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.width.equalTo(view)
        }
    }

    override func configureViews() {
        navigationItem.title = Strings.RechargeWireTransfer.navigationTitle
        view.backgroundColor = Colors.backgroundPrimary.color

        navigationItem.rightBarButtonItem = moreInfoBarButton
        setUpNavigationBarDefaultAppearance()
    }

    @objc
    private func didTapMoreInfo() {
        interactor.moreInfo()
    }
}

// MARK: RechargeWireTransferDisplay
extension RechargeWireTransferViewController: RechargeWireTransferDisplay {
    func shareInfo(_ info: [Any]) {
        let controller = UIActivityViewController(activityItems: info, applicationActivities: nil)
        present(controller, animated: true)
    }
    
    func setup(userInfo: RechargeWireTransferUserInfo, bankInfo: RechargeWireTransferBankInfo) {
        accountView.setup(userInfo: userInfo, bankInfo: bankInfo)
    }
}

extension RechargeWireTransferViewController: RechargeWireTransferBankInfoDelegate {
    func shareBankInfo() {
        interactor.shareBankInfo()
    }
}
