import Foundation

struct RechargeWireTransferAccountInfo {
    let userInfo: RechargeWireTransferUserInfo
    let bankInfo: RechargeWireTransferBankInfo
}
