import Foundation

struct RechargeWireTransferUserInfo {
    let avatar: URL?
    let username: String
    let fullname: String
}
