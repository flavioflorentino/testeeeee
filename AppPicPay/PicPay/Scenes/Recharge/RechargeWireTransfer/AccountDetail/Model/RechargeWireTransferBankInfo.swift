import Foundation

struct RechargeWireTransferBankInfo {
    let bankNumber: String
    let agencyNumber: String
    let accountNumber: String
    let document: String
}
