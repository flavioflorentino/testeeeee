import CashIn
import Core
import UIKit

enum RechargeWireTransferAction {
    case back
    case startOnboarding
    case moreInfo
}

protocol RechargeWireTransferCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RechargeWireTransferAction)
}

final class RechargeWireTransferCoordinator {
    weak var viewController: UIViewController?

    private func startOnboarding() {
        let onboardingController = OnboardingFactory.make(for: .wireTransfer)
        let navigationController = UINavigationController(rootViewController: onboardingController)
        viewController?.present(navigationController, animated: true)
    }

    private func showMoreInfo() {
        let tutorialController = TutorialFactory.make(for: .account, delegate: nil)
        viewController?.navigationController?.pushViewController(tutorialController, animated: true)
    }
}

// MARK: - RechargeWireTransferCoordinating
extension RechargeWireTransferCoordinator: RechargeWireTransferCoordinating {
    func perform(action: RechargeWireTransferAction) {
        switch action {
        case .back:
            viewController?.navigationController?.popViewController(animated: true)
        case .startOnboarding:
            startOnboarding()
        case .moreInfo:
            showMoreInfo()
        }
    }
}
