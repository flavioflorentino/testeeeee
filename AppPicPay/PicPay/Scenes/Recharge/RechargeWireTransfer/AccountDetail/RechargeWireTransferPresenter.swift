import Foundation
import UIKit
import UI

protocol RechargeWireTransferPresenting: AnyObject {
    var viewController: RechargeWireTransferDisplay? { get set }
    func display(_ accountInfo: RechargeWireTransferAccountInfo)
    func share(accountInfo: [Any])
    func didNextStep(action: RechargeWireTransferAction)
}

final class RechargeWireTransferPresenter {
    private let coordinator: RechargeWireTransferCoordinating
    weak var viewController: RechargeWireTransferDisplay?

    init(coordinator: RechargeWireTransferCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - RechargeWireTransferPresenting
extension RechargeWireTransferPresenter: RechargeWireTransferPresenting {
    func display(_ accountInfo: RechargeWireTransferAccountInfo) {
        viewController?.setup(userInfo: accountInfo.userInfo, bankInfo: formatInfo(from: accountInfo.bankInfo))
    }
    
    func didNextStep(action: RechargeWireTransferAction) {
        coordinator.perform(action: action)
    }
    
    func share(accountInfo: [Any]) {
        viewController?.shareInfo(accountInfo)
    }
}

private extension RechargeWireTransferPresenter {
    func formatInfo(from bankInfo: RechargeWireTransferBankInfo) -> RechargeWireTransferBankInfo {
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf)
        let maskedCpf = mask.maskedText(from: bankInfo.document) ?? bankInfo.document
        
        return RechargeWireTransferBankInfo(bankNumber: bankInfo.bankNumber,
                                            agencyNumber: bankInfo.agencyNumber,
                                            accountNumber: bankInfo.accountNumber,
                                            document: maskedCpf)
    }
}
