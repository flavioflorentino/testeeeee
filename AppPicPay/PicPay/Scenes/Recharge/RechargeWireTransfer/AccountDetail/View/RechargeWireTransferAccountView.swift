import SnapKit
import UI
import UIKit

private extension RechargeWireTransferAccountView.Layout {
    enum Spacing {
        static let margin: CGFloat = 16
        static let bottomSpace: CGFloat = 70
    }
}

final class RechargeWireTransferAccountView: UIView, ViewConfiguration {
    fileprivate enum Layout { }
    
    private lazy var userInfoView = RechargeWireTransferUserInfoView()
    
    private lazy var bankInfoView = RechargeWireTransferBankInfoView(delegate: delegate)
    
    private lazy var footerContainer: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle(cornerRadius: .light))
            .with(\.backgroundColor, .grayscale050())
        return view
    }()
    
    private lazy var footerLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        return label
    }()
    
    private var footerText: NSAttributedString {
        let boldText = Strings.RechargeWireTransfer.footerRemind
        let defaultText = Strings.RechargeWireTransfer.footerInfo
        let text = boldText + defaultText
        let attributedText = text.attributedString(withBold: [boldText], using: Typography.bodyPrimary().font())
        return attributedText
    }
    
    private weak var delegate: RechargeWireTransferBankInfoDelegate?

    init(delegate: RechargeWireTransferBankInfoDelegate? = nil) {
        self.delegate = delegate
        super.init(frame: .zero)
        buildLayout()
    }

    override init(frame: CGRect) {
        self.delegate = nil
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(userInfoView, bankInfoView, footerContainer)
        footerContainer.addSubview(footerLabel)
    }
    
    func setupConstraints() {
        userInfoView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Layout.Spacing.margin)
            $0.leading.trailing.equalToSuperview()
        }
        
        bankInfoView.snp.makeConstraints {
            $0.top.equalTo(userInfoView.snp.bottom).offset(Layout.Spacing.margin)
            $0.leading.trailing.equalToSuperview()
        }
        
        footerContainer.snp.makeConstraints {
            $0.top.equalTo(bankInfoView.snp.bottom).offset(Layout.Spacing.margin)
            $0.leading.trailing.equalToSuperview().inset(Layout.Spacing.margin)
            $0.bottom.lessThanOrEqualToSuperview().inset(Layout.Spacing.bottomSpace)
        }
        
        footerLabel.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Layout.Spacing.margin)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
        footerLabel.attributedText = footerText
    }
}

extension RechargeWireTransferAccountView {
    func setup(userInfo: RechargeWireTransferUserInfo, bankInfo: RechargeWireTransferBankInfo) {
        userInfoView.setup(with: userInfo)
        bankInfoView.setup(with: bankInfo)
    }
}
