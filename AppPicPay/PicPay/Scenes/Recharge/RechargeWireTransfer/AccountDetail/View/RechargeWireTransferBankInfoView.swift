import SnapKit
import UI
import UIKit

protocol RechargeWireTransferBankInfoDelegate: WireTransferCopiableDelegate {
    func shareBankInfo()
}

private extension RechargeWireTransferBankInfoView.Layout {
    enum Size {
        static let titleHeight: CGFloat = 64
    }
    enum Spacing {
        static let verticalMargin: CGFloat = 24
        static let horizontalMargin: CGFloat = 16
    }
}

final class RechargeWireTransferBankInfoView: UIView, ViewConfiguration {
    fileprivate enum Layout { }

    private lazy var headerLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
        return label
    }()

    private lazy var copiableStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        return stack
    }()

    private lazy var stackTitle: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        return label
    }()

    private lazy var shareButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Strings.RechargeWireTransfer.bankInfoShare, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(shareBankInfo), for: .touchUpInside)
        return button
    }()

    private weak var delegate: RechargeWireTransferBankInfoDelegate?

    init(delegate: RechargeWireTransferBankInfoDelegate? = nil) {
        self.delegate = delegate
        super.init(frame: .zero)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc
    func shareBankInfo() {
        delegate?.shareBankInfo()
    }

    func buildViewHierarchy() {
       addSubviews(headerLabel,
                   copiableStack,
                   shareButton)
    }

    func setupConstraints() {
        headerLabel.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(Layout.Spacing.horizontalMargin)
        }

        copiableStack.snp.makeConstraints {
            $0.top.equalTo(headerLabel.snp.bottom).offset(Layout.Spacing.verticalMargin)
            $0.leading.trailing.equalToSuperview().inset(Layout.Spacing.horizontalMargin)
        }

        stackTitle.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.titleHeight)
        }

        shareButton.snp.makeConstraints {
            $0.top.equalTo(copiableStack.snp.bottom).offset(Layout.Spacing.verticalMargin)
            $0.leading.trailing.equalToSuperview().inset(Layout.Spacing.horizontalMargin)
            $0.bottom.equalToSuperview()
        }
    }

    func configureViews() {
        copiableStack.backgroundColor = Colors.backgroundPrimary.color
        headerLabel.text = Strings.RechargeWireTransfer.bankInfoHeader
    }
}

extension RechargeWireTransferBankInfoView {
    /// Populates the texts in the view
    func setup(with bankInfo: RechargeWireTransferBankInfo) {
        stackTitle.text = Strings.RechargeWireTransfer.bankInfoTitle

        let bankView = WireTransferCopiableInfoView(delegate: delegate)
        let agencyView = WireTransferCopiableInfoView(delegate: delegate)
        let accountView = WireTransferCopiableInfoView(delegate: delegate)
        let documentView = WireTransferCopiableInfoView(delegate: delegate)

        bankView.setUp(title: Strings.RechargeWireTransfer.bankInfoBank, info: bankInfo.bankNumber)
        agencyView.setUp(title: Strings.RechargeWireTransfer.bankInfoAgency, info: bankInfo.agencyNumber)
        accountView.setUp(title: Strings.RechargeWireTransfer.bankInfoAccount, info: bankInfo.accountNumber)
        documentView.setUp(title: Strings.RechargeWireTransfer.bankInfoDocument, info: bankInfo.document)

        copiableStack.addArrangedSubviews(stackTitle, bankView, agencyView, accountView, documentView)

        copiableStack.insertLine(after: stackTitle, color: Colors.grayscale400.color)
        copiableStack.insertLine(after: bankView, color: Colors.grayscale400.color)
        copiableStack.insertLine(after: agencyView, color: Colors.grayscale400.color)
        copiableStack.insertLine(after: accountView, color: Colors.grayscale400.color)
    }
}
