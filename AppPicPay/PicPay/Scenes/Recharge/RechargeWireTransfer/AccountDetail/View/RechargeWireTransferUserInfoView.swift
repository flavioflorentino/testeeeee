import SnapKit
import UI
import UIKit

final class RechargeWireTransferUserInfoView: UIView, ViewConfiguration {
    private lazy var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.imageStyle(AvatarImageStyle(size: .xLarge, hasBorder: false))
        return imageView
    }()

    private lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .center)
        return label
    }()

    private lazy var fullnameLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, .grayscale500())
        return label
    }()

    func buildViewHierarchy() {
        addSubviews(avatarImageView,
                    usernameLabel,
                    fullnameLabel)
    }

    func setupConstraints() {
        avatarImageView.snp.makeConstraints {
            $0.top.centerX.equalToSuperview()
        }

        usernameLabel.snp.makeConstraints {
            $0.top.equalTo(avatarImageView.snp.bottom).offset(24)
            $0.leading.trailing.equalToSuperview()
        }

        fullnameLabel.snp.makeConstraints {
            $0.top.equalTo(usernameLabel.snp.bottom).offset(8)
            $0.leading.bottom.trailing.equalToSuperview()
        }
    }
}

extension RechargeWireTransferUserInfoView {
    /// Populates the texts and images in the view
    func setup(with userInfo: RechargeWireTransferUserInfo) {
        buildLayout()

        usernameLabel.text = "@\(userInfo.username)"
        fullnameLabel.text = userInfo.fullname
        avatarImageView.setImage(url: userInfo.avatar,
                                 placeholder: Assets.NewGeneration.avatarPerson.image)
    }
}
