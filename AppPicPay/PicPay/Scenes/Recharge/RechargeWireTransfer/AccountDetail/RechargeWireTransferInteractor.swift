import AnalyticsModule
import Core
import Foundation

protocol RechargeWireTransferInteracting: AnyObject {
    func loadInfo()
    func close()
    func moreInfo()
    func shareBankInfo()
}

final class RechargeWireTransferInteractor {
    typealias Dependencies = HasAnalytics & HasKVStore
    private let dependencies: Dependencies

    private let accountInfo: RechargeWireTransferAccountInfo
    private let presenter: RechargeWireTransferPresenting

    private var onboardinguUserDefaultsKey: String {
        RechargeWireTransferUserDefaultKey.wireTransferOnboardingHasBeenShown.rawValue
    }

    private var onboardingHasBeenShown: Bool {
        dependencies.kvStore.getFirstTimeOnlyEvent(onboardinguUserDefaultsKey)
    }

    init(accountInfo: RechargeWireTransferAccountInfo, presenter: RechargeWireTransferPresenting, dependencies: Dependencies) {
        self.accountInfo = accountInfo
        self.presenter = presenter
        self.dependencies = dependencies
    }

    private func presentOnboardingIfNeeded() {
        guard !onboardingHasBeenShown else { return }
        presenter.didNextStep(action: .startOnboarding)
        dependencies.kvStore.setFirstTimeOnlyEvent(onboardinguUserDefaultsKey)
    }

    private func sendAnalyticsEvent() {
        let event = RechargeEvent.wireTransfer(.picpayAccount)
        dependencies.analytics.log(event)
    }
}

// MARK: - RechargeWireTransferInteracting
extension RechargeWireTransferInteractor: RechargeWireTransferInteracting {
    func loadInfo() {
        presenter.display(accountInfo)
        presentOnboardingIfNeeded()
        sendAnalyticsEvent()
    }

    func close() {
        presenter.didNextStep(action: .back)
    }

    func moreInfo() {
        presenter.didNextStep(action: .moreInfo)
    }

    func shareBankInfo() {
        let accountString = accountInfo.bankInfo.accountNumber.byRemovingPunctuation
        let documentString = accountInfo.bankInfo.document.byRemovingPunctuation
        let infoString = Strings.RechargeWireTransfer.bankInfoSharable(accountInfo.userInfo.fullname,
                                                                       accountInfo.bankInfo.bankNumber,
                                                                       accountInfo.bankInfo.agencyNumber,
                                                                       accountString,
                                                                       documentString)

        presenter.share(accountInfo: [infoString])
    }
}
