import Foundation

enum RechargeWireTransferFactory {
    static func make(for accountInfo: RechargeWireTransferAccountInfo) -> RechargeWireTransferViewController {
        let container = DependencyContainer()
        let coordinator: RechargeWireTransferCoordinating = RechargeWireTransferCoordinator()
        let presenter: RechargeWireTransferPresenting = RechargeWireTransferPresenter(coordinator: coordinator)
        let interactor = RechargeWireTransferInteractor(accountInfo: accountInfo, presenter: presenter, dependencies: container)
        let viewController = RechargeWireTransferViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
