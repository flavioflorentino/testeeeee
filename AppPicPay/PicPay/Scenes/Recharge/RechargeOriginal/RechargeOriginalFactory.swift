import Foundation

enum RechargeOriginalFactory {
    static func make() -> RechargeOriginalViewController {
        let service: RechargeOriginalServicing = RechargeOriginalService()
        let viewModel: RechargeOriginalViewModelType = RechargeOriginalViewModel(service: service)
        var coordinator: RechargeOriginalCoordinating = RechargeOriginalCoordinator()
        let viewController = RechargeOriginalViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController

        return viewController
    }
}
