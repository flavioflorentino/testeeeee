import Foundation

protocol RechargeOriginalViewModelInputs {
    func viewDidLoad()
    func numberOfRows() -> Int
    func cellModel(index: Int) -> PageOriginal?
    
    func changePage(nextPage: Int, currentPage: Int)
    func didTapDownloadOriginal()
    func didTapSignUpOriginal()
    func close()
}

protocol RechargeOriginalViewModelOutputs: AnyObject {
    func sendToAnalytics(swipes: Int)
    func didNextStep(action: RechargeOriginalAction)
    func signUp()
    func reloadData()
}

protocol RechargeOriginalViewModelType: AnyObject {
    var inputs: RechargeOriginalViewModelInputs { get }
    var outputs: RechargeOriginalViewModelOutputs? { get set }
}

final class RechargeOriginalViewModel: RechargeOriginalViewModelType, RechargeOriginalViewModelInputs {
    var inputs: RechargeOriginalViewModelInputs { self }
    weak var outputs: RechargeOriginalViewModelOutputs?

    private let service: RechargeOriginalServicing
    private var model: [PageOriginal]
    private var countSwipes: Int = 0
    
    init(service: RechargeOriginalServicing) {
        self.service = service
        self.model = []
    }

    func viewDidLoad() {
        let page1 = PageOriginal(title: service.onboardingTitle, description: service.onboardingDescription, image: .onboarding)
        let page2 = PageOriginal(title: RechargeLocalizable.titlePage1Original.text, description: RechargeLocalizable.descPage1Original.text, image: .recharge)
        let page3 = PageOriginal(title: RechargeLocalizable.titlePage2Original.text, description: RechargeLocalizable.descPage2Original.text, image: .transfer)
        
        model.append(page1)
        model.append(page2)
        model.append(page3)
        
        outputs?.reloadData()
    }
    
    func cellModel(index: Int) -> PageOriginal? {
        guard model.indices.contains(index) else {
            return nil
        }
        
        let page = model[index]
        return PageOriginal(title: page.title, description: page.description, image: page.image)
    }
    
    func numberOfRows() -> Int {
        model.count
    }
    
    func changePage(nextPage: Int, currentPage: Int) {
        guard nextPage > currentPage else {
            return
        }
        countSwipes += 1
    }
    
    func didTapDownloadOriginal() {
        outputs?.didNextStep(action: .downloadOriginal)
    }
    
    func didTapSignUpOriginal() {
        sendToAnalytics()
        outputs?.signUp()
    }
    
    func close() {
        sendToAnalytics()
        outputs?.didNextStep(action: .close)
    }
    
    private func sendToAnalytics() {
        outputs?.sendToAnalytics(swipes: countSwipes)
    }
}
