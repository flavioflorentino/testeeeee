import UI
import UIKit

protocol RechargeOriginalViewControllerDelegate: AnyObject {
    func originalOnConnect(controller: RechargeOriginalViewController)
}

final class RechargeOriginalViewController: LegacyViewController<RechargeOriginalViewModelType, RechargeOriginalCoordinating, UIView> {    
    private lazy var pageControl: UIPageControl = {
        let control = UIPageControl()
        control.currentPageIndicatorTintColor = Palette.ppColorBranding300.color
        control.pageIndicatorTintColor = Palette.ppColorGrayscale100.color
        control.backgroundColor = .clear
        control.translatesAutoresizingMaskIntoConstraints = false
        
        return control
    }()
    
    private lazy var footerView: FooterPageOriginal = {
        let view = FooterPageOriginal()
        view.delegate = self
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isPagingEnabled = true
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        return collectionView
    }()
    
    weak var delegate: RechargeOriginalViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollection()
        viewModel.inputs.viewDidLoad()
    }

    override func buildViewHierarchy() {
        view.addSubview(collectionView)
        view.addSubview(pageControl)
        view.addSubview(footerView)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale100.color
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale100.color
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "close_popup"), style: .done, target: self, action: #selector(didTapCloseButton))
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.topAnchor, constant: 60),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
        ])
        
        NSLayoutConstraint.activate([
            pageControl.topAnchor.constraint(equalTo: collectionView.bottomAnchor, constant: 10),
            pageControl.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
        
        if #available(iOS 11.0, *) {
            footerView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -30).isActive = true
        } else {
            footerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -30).isActive = true
        }
        
        NSLayoutConstraint.activate([
            footerView.topAnchor.constraint(equalTo: pageControl.bottomAnchor, constant: 20),
            footerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            footerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
        ])
    }
    
    private func setupCollection() {
        collectionView.register(PageOriginalCollectionViewCell.self, forCellWithReuseIdentifier: PageOriginalCollectionViewCell.nibName)
    }
    
    @objc
    private func didTapCloseButton() {
        viewModel.inputs.close()
    }
}

// MARK: View Model Outputs
extension RechargeOriginalViewController: RechargeOriginalViewModelOutputs {
    func sendToAnalytics(swipes: Int) {
        PPAnalytics.trackEvent("Saque Original - Vantagens Swipe", properties: ["Swipes": swipes])
    }
    
    func signUp() {
        delegate?.originalOnConnect(controller: self)
    }
    
    func reloadData() {
        pageControl.numberOfPages = viewModel.inputs.numberOfRows()
        collectionView.reloadData()
    }
    
    func didNextStep(action: RechargeOriginalAction) {
        coordinator.perform(action: action)
    }
}

extension RechargeOriginalViewController: FooterPageOriginalDelegate {
    func didTapButton() {
        viewModel.inputs.didTapSignUpOriginal()
    }
    
    func didTapOpenApp() {
        viewModel.inputs.didTapDownloadOriginal()
    }
}

extension RechargeOriginalViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.inputs.numberOfRows()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let row = indexPath.row
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PageOriginalCollectionViewCell.nibName, for: indexPath) as? PageOriginalCollectionViewCell,
              let model = viewModel.inputs.cellModel(index: row) else {
            return UICollectionViewCell()
        }
        
        cell.configure(with: model)
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let x = scrollView.contentOffset.x
        let w = scrollView.bounds.size.width
        let page = Float(x / w).rounded()
        
        let nextPage = Int(page)
        viewModel.inputs.changePage(nextPage: nextPage, currentPage: pageControl.currentPage)
        pageControl.currentPage = nextPage
    }
}

extension RechargeOriginalViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        collectionView.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        0
    }
}
