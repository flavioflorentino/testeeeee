import Foundation
import FeatureFlag

protocol RechargeOriginalServicing {
    var onboardingTitle: String { get }
    var onboardingDescription: String { get }
}

final class RechargeOriginalService: RechargeOriginalServicing {
    var onboardingTitle: String {
        FeatureManager.text(.originalOnboardingTitle)
    }
    
    var onboardingDescription: String {
        FeatureManager.text(.originalOnboardingDescription)
    }
}
