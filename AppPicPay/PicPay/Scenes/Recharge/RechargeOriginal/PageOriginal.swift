import Foundation

struct PageOriginal {
    enum ImageType {
        case onboarding
        case recharge
        case transfer
    }
    
    let title: String
    let description: String
    let image: ImageType
}
