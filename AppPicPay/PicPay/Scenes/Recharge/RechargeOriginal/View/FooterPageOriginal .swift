import UI
import UIKit

protocol FooterPageOriginalDelegate: AnyObject {
    func didTapButton()
    func didTapOpenApp()
}

final class FooterPageOriginal: UIView {
    private let buttonHeight: CGFloat = 48
    
    private lazy var button: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = buttonHeight / 2
        button.addTarget(self, action: #selector(tapButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.configure(with: Button(title: RechargeLocalizable.signUpOriginal.text))
        
        return button
    }()
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private var infoAttributed: [NSAttributedString.Key: Any] = {
        let font = UIFont.systemFont(ofSize: 14.0)
        let attributes: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: Palette.ppColorGrayscale600.color]
        
        return attributes
    }()
    
    private var highlightAttributed: [NSAttributedString.Key: Any] = {
        let font = UIFont.systemFont(ofSize: 14.0)
        let attributes: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: Palette.ppColorBranding300.color]
        
        return attributes
    }()
    
    weak var delegate: FooterPageOriginalDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = Palette.ppColorGrayscale100.color
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapLabel))
        infoLabel.addGestureRecognizer(tap)
        setupFooter()
    }
    
    private func addComponents() {
        addSubview(button)
        addSubview(infoLabel)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: topAnchor),
            button.leadingAnchor.constraint(equalTo: leadingAnchor),
            button.trailingAnchor.constraint(equalTo: trailingAnchor),
            button.heightAnchor.constraint(equalToConstant: buttonHeight)
        ])
        
        NSLayoutConstraint.activate([
            infoLabel.topAnchor.constraint(equalTo: button.bottomAnchor, constant: 20),
            infoLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            infoLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            infoLabel.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
    
    private func setupFooter() {
        let text = RechargeLocalizable.footerOriginal.text
        let range = NSString(string: text).range(of: RechargeLocalizable.highlightOriginal.text)
        
        let attributedTex = NSMutableAttributedString(string: text, attributes: infoAttributed)
        attributedTex.addAttributes(highlightAttributed, range: range)
        
        infoLabel.attributedText = attributedTex
    }
    
    @objc
    private func didTapLabel() {
        delegate?.didTapOpenApp()
    }
    
    @objc
    private func tapButton() {
        delegate?.didTapButton()
    }
}
