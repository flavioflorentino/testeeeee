import Foundation

protocol PageOriginalCellViewModelInputs {
    var title: String { get }
    var description: String { get }
    var image: UIImage { get }
    
    func viewConfigure(model: PageOriginal)
}

protocol PageOriginalCellViewModelOutputs: AnyObject {
    func reloadData()
}

protocol PageOriginalCellViewModelType: AnyObject {
    var inputs: PageOriginalCellViewModelInputs { get }
    var outputs: PageOriginalCellViewModelOutputs? { get set }
}

final class PageOriginalCellViewModel: PageOriginalCellViewModelType, PageOriginalCellViewModelInputs {
    var inputs: PageOriginalCellViewModelInputs { self }
    weak var outputs: PageOriginalCellViewModelOutputs?
    
    private var model: PageOriginal?
    
    var title: String {
        model?.title ?? ""
    }
    
    var description: String {
        model?.description ?? ""
    }
    
    var image: UIImage {
        guard let model = model else {
            return #imageLiteral(resourceName: "ilu_original_carrosel")
        }
        
        switch model.image {
        case .onboarding:
            return #imageLiteral(resourceName: "ilu_original_carrosel")
        case .recharge:
            return #imageLiteral(resourceName: "ilu_original_carrosel_rechar")
        case .transfer:
            return #imageLiteral(resourceName: "ilu_original_carrosel_trans")
        }
    }
    
    func viewConfigure(model: PageOriginal) {
        self.model = model
        outputs?.reloadData()
    }
}
