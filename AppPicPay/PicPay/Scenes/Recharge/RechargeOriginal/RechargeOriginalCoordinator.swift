import Core
import FeatureFlag
import UIKit

enum RechargeOriginalAction {
    case downloadOriginal
    case close
}

protocol RechargeOriginalCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: RechargeOriginalAction)
}

final class RechargeOriginalCoordinator: RechargeOriginalCoordinating {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    private var application: UIApplicationContract
    
    weak var viewController: UIViewController?
    
    init(dependencies: Dependencies = DependencyContainer(), application: UIApplicationContract = UIApplication.shared) {
        self.application = application
        self.dependencies = dependencies
    }
    
    func perform(action: RechargeOriginalAction) {
        switch action {
        case .downloadOriginal:
            let link = dependencies.featureManager.text(.featureOriginalDeeplink)
            guard let url = URL(string: link) else {
                return
            }
            application.open(url)
            
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
