import AnalyticsModule
import FeatureFlag
import Foundation
import Loan

protocol RechargeMethodsViewModelInputs {
    func viewDidLoad()
    func numberOfRows(section: Int) -> Int
    var numberOfSections: Int { get }
    
    func didTap(index: Int)
    func didTapBank(method: Int, bank: Int)
    func didTapHelpCenter()
    func close()
    
    func cardRecharge(index: Int) -> CardRecharge
    func type(index: Int) -> CardRecharge.TypeCard
}

protocol RechargeMethodsViewModelOutputs: AnyObject {
    func reloadData()
    func didNextStep(action: RechargeMethodsAction)
    func showBanks(method: Int, banks: [String])
    func didReceiveAnError(error: PicPayErrorDisplayable)
    func warningCard(title: String, desc: String)
    func addRightButton()
}

protocol RechargeMethodsViewModelType: AnyObject {
    var inputs: RechargeMethodsViewModelInputs { get }
    var outputs: RechargeMethodsViewModelOutputs? { get set }
}

final class RechargeMethodsViewModel: RechargeMethodsViewModelType, RechargeMethodsViewModelInputs {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    var inputs: RechargeMethodsViewModelInputs { self }
    weak var outputs: RechargeMethodsViewModelOutputs?
    private let dependencies: Dependencies
    
    private var model: [RechargeMethod]
    private let service: RechargeMethodsServicing
    private var isDepositDebitFeeEnabled: Bool {
        dependencies.featureManager.isActive(.featureDepositDebitCardChargeBool)
    }
    
    init(model with: [RechargeMethod], service: RechargeMethodsServicing, dependencies: Dependencies) {
        model = with
        self.service = service
        self.dependencies = dependencies
    }
    
    func viewDidLoad() {
        if !model.isEmpty {
            filterRechargeMethods()
            outputs?.reloadData()
        } else {
            showUnexpectedError()
        }
        
        configureRightButton()
    }
    
    func numberOfRows(section: Int) -> Int {
        section == 0 ? model.count : 0
    }
    
    var numberOfSections: Int { 1 }
    
    func type(index: Int) -> CardRecharge.TypeCard {
        if model[index].typeId == .debit && !containsDebitCard() {
            return .addDebit
        }
        
        switch model[index].typeId {
        case .original:
            return .original
        case .loan:
            return .loan
        default:
            return .standard
        }
    }
    
    func cardRecharge(index: Int) -> CardRecharge {
        var title = model[index].name
        let description = model[index].desc
        let register = model[index].registerDebit ?? ""
        let image = model[index].imgUrl ?? ""
        let typeCard = type(index: index)
        
        switch typeCard {
        case .standard:
            return CardRecharge(title: title, description: description, image: image, accessory: nil)
        case .loan:
            title = model[index].title ?? model[index].name
            return CardRecharge(title: title, description: description, image: image, accessory: nil)
        case .addDebit:
            return CardRecharge(title: title, description: register, image: image, accessory: nil)
        case .original:
            let accessory = RechargeLocalizable.descOriginal.text
            return CardRecharge(title: title, description: description, image: image, accessory: accessory)
        }
    }
    
    func didTap(index: Int) {
        let typeCard = model[index].typeId
        let method = model[index]
        
        switch typeCard {
        case .bill:
            outputs?.didNextStep(action: .bill(method))
        case .bank:
            openBankTransfer(index: index)
        case .original:
            openOriginalTransfer(method: method)
        case .emergencyAid:
            tapEmergencyAid(method: method)
        case .debit:
            tapDebitTransfer(method: method)
        case .loan:
            outputs?.didNextStep(action: .loan)
        case .lending:
            dependencies.analytics.log(RechargeEvent.tapP2PLending(.recharge))
            outputs?.didNextStep(action: .lending)
        case .picpayAccount:
            openPicpayAccount(method: method)
        case .deeplink:
            openDeeplink(with: method)
        }
    }
    
    func didTapHelpCenter() {
        guard let url = URL(string: service.linkHelpCenter) else { return showUnexpectedError() }
        outputs?.didNextStep(action: .deeplink(url))
    }
    
    func close() {
        outputs?.didNextStep(action: .close)
    }
    
    func didTapBank(method: Int, bank: Int) {
        guard let recharge = model[method].options else { return showUnexpectedError() }
        outputs?.didNextStep(action: .bank(recharge[bank]))
    }
    
    private func tapEmergencyAid(method: RechargeMethod) {
        guard containsVirtualCard() else {
            flowAddVirtualCard(method: method)
            return
        }
        guard let options = method.options, isDepositDebitFeeEnabled else {
            outputs?.didNextStep(action: .debit(method))
            return
        }
        
        outputs?.didNextStep(action: .governmentRecharge(options))
    }
    
    private func containsVirtualCard() -> Bool {
        service.getCardsBank().contains(where: { $0.isOneShotCvv ?? false })
    }
    
    private func containsDebitCard() -> Bool {
        service.getCardsBank().contains(where: { $0.type == .debit || $0.type == .creditDebit })
    }
    
    private func configureRightButton() {
        guard service.showRechargeDebitMethod else { return }
        outputs?.addRightButton()
    }
    
    private func tapDebitTransfer(method: RechargeMethod) {
        if containsDebitCard() {
            outputs?.didNextStep(action: .debit(method))
        } else {
           flowAddDebitCard(method: method)
        }
    }
    
    private func flowAddDebitCard(method: RechargeMethod) {
        outputs?.didNextStep(action: .addDebitCard({ [weak self] type in
            guard type == .debit else {
                self?.wrongTypeCard()
                return
            }
            
            self?.outputs?.reloadData()
            self?.outputs?.didNextStep(action: .debit(method))
        }))
    }
    
    private func flowAddVirtualCard(method: RechargeMethod) {
        outputs?.didNextStep(action: .addVirtualCard({ [weak self] _ in
            self?.outputs?.reloadData()
            self?.outputs?.didNextStep(action: .debit(method))
        }))
    }
    
    private func wrongTypeCard() {
        let message = RechargeLocalizable.warningCreditCard.text
        let title = DefaultLocalizable.attention.text
        outputs?.warningCard(title: title, desc: message)
    }
    
    private func filterRechargeMethods() {
        let showDebitMethod = service.showRechargeDebitMethod
        dependencies.analytics.log(RechargeEvent.rechargeEnable(.recharge, enable: showDebitMethod))
        
        var methodsToFilter: [RechargeMethod.RechargeType] = []
        if !showDebitMethod { methodsToFilter.append(.debit) }
        if !service.showPicpayAccount { methodsToFilter.append(.picpayAccount) }
        if !service.showLoanMethod { methodsToFilter.append(.loan) }
        
        if methodsToFilter.isNotEmpty {
            model = model.filter { !methodsToFilter.contains($0.typeId) }
        }
    }

    private func openBankTransfer(index: Int) {
        guard let method = model[index].options else { return showUnexpectedError() }
        
        let banks = method.compactMap { $0.name }
        outputs?.showBanks(method: index, banks: banks)
    }
    
    private func openOriginalTransfer(method: RechargeMethod) {
        if method.linked == false {
            outputs?.didNextStep(action: .original(method))
        } else {
            outputs?.didNextStep(action: .bank(method))
        }
    }
    
    private func openPicpayAccount(method: RechargeMethod) {
        guard let accountInfo = getAccountInfo(from: method) else { return showUnexpectedError() }
        outputs?.didNextStep(action: .picpayAccount(accountInfo))
    }
    
    private func openDeeplink(with method: RechargeMethod) {
        dependencies.analytics.log(RechargeEvent.deeplink)
        guard let urlString = method.deeplinkString,
              let url = URL(string: urlString) else { return showUnexpectedError() }
        outputs?.didNextStep(action: .deeplink(url))
    }
    
    func getAccountInfo(from method: RechargeMethod) -> RechargeWireTransferAccountInfo? {
        guard let bankNumber = method.bankId,
            let agencyNumber = method.accountAgency,
            let accountNumber = method.accountNumber,
            let document = method.consumerCpf,
            let username = method.consumerUsername,
            let fullname = method.consumerName else {
                showUnexpectedError()
                return nil
        }
        let bankInfo = RechargeWireTransferBankInfo(bankNumber: bankNumber,
                                                    agencyNumber: agencyNumber,
                                                    accountNumber: accountNumber,
                                                    document: document)
        
        let avatarURL = URL(string: method.consumerAvatar ?? "")
        let userInfo = RechargeWireTransferUserInfo(avatar: avatarURL, username: username, fullname: fullname)
        
        return RechargeWireTransferAccountInfo(userInfo: userInfo, bankInfo: bankInfo)
    }
    
    private func showUnexpectedError() {
        let picpayError = PicPayError(message: DefaultLocalizable.unexpectedError.text)
        outputs?.didReceiveAnError(error: picpayError)
    }
}
