import Foundation

enum RechargeMethodsFactory {
    static func make(with: [RechargeMethod]) -> UIViewController {
        let container = DependencyContainer()
        let service: RechargeMethodsServicing = RechargeMethodsService(dependencies: container)
        let viewModel: RechargeMethodsViewModelType = RechargeMethodsViewModel(model: with, service: service, dependencies: container)
        var coordinator: RechargeMethodsCoordinating = RechargeMethodsCoordinator()
        let viewController = RechargeMethodsViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController
        
        return viewController
    }
}
