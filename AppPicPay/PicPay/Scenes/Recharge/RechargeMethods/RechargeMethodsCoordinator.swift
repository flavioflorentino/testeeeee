import FeatureFlag
import Foundation
import Loan
import LoanOffer
import P2PLending
import UI
import UIKit

enum RechargeMethodsAction {
    case debit(RechargeMethod)
    case bill(RechargeMethod)
    case bank(RechargeMethod)
    case original(RechargeMethod)
    case addVirtualCard((CardListType) -> Void)
    case addDebitCard((CardListType) -> Void)
    case lending
    case close
    case loan
    case picpayAccount(RechargeWireTransferAccountInfo)
    case governmentRecharge([RechargeMethod])
    case deeplink(URL)
}

protocol RechargeMethodsCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: RechargeMethodsAction)
}

final class RechargeMethodsCoordinator: RechargeMethodsCoordinating {
    var viewController: UIViewController?
    private var additionalMethod: RechargeMethod?
    private var addCard: ((CardListType) -> Void)?
    private var auth: PPAuth?

    func perform(action: RechargeMethodsAction) {
        switch action {
        case .bank(let method),
             .bill(let method),
             .debit(let method):
            startRechargeValue(with: method)

        case .original(let method):
            let controller = RechargeOriginalFactory.make()
            controller.delegate = self
            additionalMethod = method
            viewController?.present(PPNavigationController(rootViewController: controller), animated: true)

        case let .addVirtualCard(completion):
            startVirtualCard(completion: completion)

        case let .addDebitCard(completion):
            startDebitCard(andCompletion: completion)

        case .lending:
            let controller = LendingGreetingsFactory.make()
            viewController?.navigationController?.show(controller, sender: nil)

        case .close:
            viewController?.dismiss(animated: true)

        case .loan:
            startLoan()

        case let .picpayAccount(accountInfo):
            startPicpayAccount(with: accountInfo)
            
        case .governmentRecharge(let options):
            startGovernmentRecharge(with: options)
            
        case let .deeplink(url):
            DeeplinkHelper.handleDeeplink(withUrl: url, from: viewController)
        }
    }
}

private extension RechargeMethodsCoordinator {
    func startRechargeValue(with method: RechargeMethod) {
        let controller = RechargeValueFactory.make(model: method)
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    func startGovernmentRecharge(with options: [RechargeMethod]) {
        let controller = GovernmentRechargeMethodFactory.make(for: options)
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }

    func startPicpayAccount(with accountInfo: RechargeWireTransferAccountInfo) {
        let controller = RechargeWireTransferFactory.make(for: accountInfo)
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    func startVirtualCard(completion: ((CardListType) -> Void)?) {
        addCard = completion
        let controller = VirtualCardRegistrationFactory.make(flow: self)
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }

    func startDebitCard(andCompletion completion: ((CardListType) -> Void)?) {
        guard let controller = ViewsManager.paymentMethodsStoryboardViewController(withIdentifier: "AddNewCreditCardViewController") as? AddNewCreditCardViewController else {
            return
        }

        addCard = completion
        controller.delegate = self
        let viewModel = AddNewCreditCardViewModel(origin: .recharge)
        controller.viewModel = viewModel
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }

    func startLoan() {
        guard let navigation = viewController?.navigationController else {
            return
        }
        
        let coordinator = LoanOfferCoordinator(with: navigation, from: .offer)
        SessionManager.sharedInstance.currentCoordinating = coordinator
        coordinator.start(flow: .hire) {
            SessionManager.sharedInstance.currentCoordinating = nil
        }
    }
}

extension RechargeMethodsCoordinator: AddNewCreditCardDelegate {
    func debitCardDidInsert() {
        addCard?(.debit)
    }

    func creditCardDidInsert() {
        addCard?(.credit)
    }
}

extension RechargeMethodsCoordinator: VirtualCardRegistrationCoordinatorDelegate {
    func finishAddDebitCard(cardType: CardListType) {
        viewController?.navigationController?.popViewController(animated: false)
        addCard?(cardType)
    }
}

extension RechargeMethodsCoordinator: RechargeOriginalViewControllerDelegate {
    func originalOnConnect(controller: RechargeOriginalViewController) {
        controller.dismiss(animated: true) { [weak self] in
            guard let method = self?.additionalMethod else {
                return
            }
            self?.presentWebViewOriginal(method: method)
        }
    }

    private func presentWebViewOriginal(method: RechargeMethod) {
        guard let vc = viewController else {
            return
        }

        ViewsManager.pushWebViewController(withUrl: method.authUrl, sendHeaders: false, customHeader: method.authHeaders, fromViewController: vc, title: DefaultLocalizable.bankOriginal.text) { [weak self] error in
            if let error = error {
                AlertMessage.showAlert(withMessage: error.localizedDescription, controller: self?.viewController)
            } else {
                method.linked = true
                self?.perform(action: .bank(method))
            }
        }
    }
}
