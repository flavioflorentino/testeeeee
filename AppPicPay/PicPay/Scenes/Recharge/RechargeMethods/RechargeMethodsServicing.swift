import Foundation
import FeatureFlag

protocol RechargeMethodsServicing {
    var linkHelpCenter: String { get }
    var showLoanMethod: Bool { get }
    var showPicpayAccount: Bool { get }
    var showRechargeDebitMethod: Bool { get }
    func getCardsBank() -> [CardBank]
}

final class RechargeMethodsService: RechargeMethodsServicing {
    typealias Dependencies = HasFeatureManager & HasCreditCardManager
    private var dependencies: Dependencies
    
    var showLoanMethod: Bool {
        dependencies.featureManager.isActive(.featureLoan)
    }
    
    var showPicpayAccount: Bool {
        dependencies.featureManager.isActive(.releasePicpayAccountPresentingBool)
    }
    
    var showRechargeDebitMethod: Bool {
        dependencies.featureManager.isActive(.featureDepositDebit)
    }
        
    var linkHelpCenter: String {
        dependencies.featureManager.text(.linkHelpDebit)
    }
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func getCardsBank() -> [CardBank] {
        dependencies.creditCardManager.cardList
    }
}
