import UI
import UIKit

final class RegisterDebitTableViewCell: CardRechargeTableViewCell {
    private enum Layout {
        static let imageSize: CGFloat = 32
        static let sizeLine: CGFloat = 1
    }
    
    private lazy var rootView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = RechargeLocalizable.addCard.text
        label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        label.textColor = Palette.ppColorBranding300.color
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var addImageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.image = #imageLiteral(resourceName: "iconCardAdd")
        image.translatesAutoresizingMaskIntoConstraints = false
        
        return image
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale300.color
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addComponents()
        layoutComponents()
    }
    
    private func addComponents() {
        rootView.addSubview(lineView)
        rootView.addSubview(addImageView)
        rootView.addSubview(titleLabel)
        rootStackView.addArrangedSubview(rootView)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            lineView.topAnchor.constraint(equalTo: rootView.topAnchor, constant: 20),
            lineView.leadingAnchor.constraint(equalTo: rootView.leadingAnchor),
            lineView.trailingAnchor.constraint(equalTo: rootView.trailingAnchor),
            lineView.heightAnchor.constraint(equalToConstant: Layout.sizeLine)
        ])
        
        NSLayoutConstraint.activate([
            addImageView.topAnchor.constraint(equalTo: lineView.bottomAnchor, constant: 16),
            addImageView.leadingAnchor.constraint(equalTo: rootView.leadingAnchor),
            addImageView.bottomAnchor.constraint(equalTo: rootView.bottomAnchor),
            addImageView.heightAnchor.constraint(equalToConstant: Layout.imageSize),
            addImageView.widthAnchor.constraint(equalToConstant: Layout.imageSize)
        ])
        
        NSLayoutConstraint.activate([
            titleLabel.centerYAnchor.constraint(equalTo: addImageView.centerYAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: addImageView.trailingAnchor, constant: 8),
            titleLabel.trailingAnchor.constraint(equalTo: rootView.trailingAnchor)
        ])
    }
}
