import UI
import UIKit

final class CardOriginalTableViewCell: CardRechargeTableViewCell {
    private lazy var accessoryLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.textColor = Palette.ppColorBranding300.color
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addComponents()
    }
    
    private func addComponents() {
        rootStackView.addArrangedSubview(accessoryLabel)
    }
    
    override func updateView() {
        super.updateView()
        accessoryLabel.text = viewModel.inputs.accessory
    }
}
