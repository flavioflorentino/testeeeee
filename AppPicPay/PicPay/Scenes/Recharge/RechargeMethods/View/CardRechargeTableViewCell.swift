import UI
import UIKit

class CardRechargeTableViewCell: UITableViewCell {
    private let sizeImage: CGFloat = 48
    
    lazy var viewModel: CardRechargeViewModelType = {
        let viewModel = CardRechargeViewModel()
        viewModel.outputs = self
        
        return viewModel
    }()
    
    lazy var rootStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 7.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    }()
    
    private lazy var rootView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.groupedBackgroundSecondary.color
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.spacing = 25.0
        
        return stackView
    }()
    
    private lazy var containerView = UIView()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale500())
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var typeImage = UIImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .clear
        rootView.clipsToBounds = true
        rootView.layer.cornerRadius = 6
        selectionStyle = .none
        rootView.addShadow(with: Colors.grayscale600.color, alpha: 0.1, radius: 6, offset: CGSize(width: 0, height: 2))
    }
    
    private func addComponents() {
        contentView.addSubview(rootView)
        rootView.addSubview(rootStackView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(descriptionLabel)
        stackView.addArrangedSubview(containerView)
        stackView.addArrangedSubview(typeImage)
        rootStackView.addArrangedSubview(stackView)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            rootView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
            rootView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5),
            rootView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            rootView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16)
        ])
        
        NSLayoutConstraint.activate([
            rootStackView.topAnchor.constraint(equalTo: rootView.topAnchor, constant: 16),
            rootStackView.bottomAnchor.constraint(equalTo: rootView.bottomAnchor, constant: -16),
            rootStackView.leadingAnchor.constraint(equalTo: rootView.leadingAnchor, constant: 16),
            rootStackView.trailingAnchor.constraint(equalTo: rootView.trailingAnchor, constant: -16)
        ])
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: containerView.topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 7),
            descriptionLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            descriptionLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            descriptionLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            typeImage.heightAnchor.constraint(equalToConstant: sizeImage),
            typeImage.widthAnchor.constraint(equalToConstant: sizeImage)
        ])
    }
    
    func updateView() {
        titleLabel.text = viewModel.inputs.title
        descriptionLabel.text = viewModel.inputs.description
        typeImage.setImage(url: viewModel.inputs.image)
    }
    
    func configure(with model: CardRecharge) {
        viewModel.inputs.viewConfigure(model: model)
    }
}

extension CardRechargeTableViewCell: CardRechargeViewModelOutputs {
    func reloadData() {
        updateView()
    }
}
