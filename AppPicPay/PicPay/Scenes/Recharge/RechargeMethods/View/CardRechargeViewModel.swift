import Foundation

protocol CardRechargeViewModelInputs {
    var title: String? { get }
    var description: String? { get }
    var image: URL? { get }
    var accessory: String? { get }
    
    func viewConfigure(model: CardRecharge)
}

protocol CardRechargeViewModelOutputs: AnyObject {
    func reloadData()
}

protocol CardRechargeViewModelType: AnyObject {
    var inputs: CardRechargeViewModelInputs { get }
    var outputs: CardRechargeViewModelOutputs? { get set }
}

final class CardRechargeViewModel: CardRechargeViewModelType, CardRechargeViewModelInputs {
    var inputs: CardRechargeViewModelInputs { self }
    weak var outputs: CardRechargeViewModelOutputs?
    
    private var model: CardRecharge?
    
    var title: String? {
        model?.title
    }
    
    var description: String? {
        model?.description
    }
    
    var image: URL? {
        URL(string: model?.image ?? "")
    }
    
    var accessory: String? {
        model?.accessory ?? ""
    }
    
    func viewConfigure(model: CardRecharge) {
        self.model = model
        outputs?.reloadData()
    }
}
