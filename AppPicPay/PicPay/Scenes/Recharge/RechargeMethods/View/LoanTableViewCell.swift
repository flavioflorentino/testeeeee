import UI

private extension LoanTableViewCell.Layout {
    static let height: CGFloat = 82
    static let descriptionHeight: CGFloat = 48
    static let imageWidth: CGFloat = 74
}

final class LoanTableViewCell: UITableViewCell {
    fileprivate enum Layout {}
    
    lazy var viewModel: CardRechargeViewModelType = {
        let viewModel = CardRechargeViewModel()
        viewModel.outputs = self
        
        return viewModel
    }()

    private lazy var rootView = UIView()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale500())
        return label
    }()
    
    private lazy var typeImage: UIImageView = {
        let image = UIImageView()
        image.backgroundColor = .clear
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .clear
        rootView.backgroundColor = Colors.groupedBackgroundSecondary.color
        rootView.clipsToBounds = true
        rootView.layer.cornerRadius = CornerRadius.light
        selectionStyle = .none
        rootView.addShadow(
            with: Colors.grayscale600.color,
            alpha: 0.1,
            radius: 2,
            offset: CGSize(width: 0, height: 2)
        )
    }
    
    private func addComponents() {
        contentView.addSubview(rootView)
        rootView.addSubview(titleLabel)
        rootView.addSubview(descriptionLabel)
        rootView.addSubview(typeImage)
    }
    
    private func layoutComponents() {
        rootView.snp.makeConstraints {
            $0.top.equalTo(contentView).offset(Spacing.base00)
            $0.bottom.equalTo(contentView).offset(-Spacing.base00)
            $0.leading.equalTo(contentView).offset(Spacing.base02)
            $0.trailing.equalTo(contentView).offset(-Spacing.base02)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.leading.equalTo(rootView).offset(Spacing.base02)
            $0.trailing.equalTo(typeImage.snp.leading)
        }

        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.bottom.equalTo(rootView).inset(Spacing.base02)
            $0.leading.equalTo(titleLabel)
            $0.trailing.equalTo(typeImage.snp.leading).offset(-Spacing.base01)
            $0.height.equalTo(Layout.descriptionHeight)
        }

        typeImage.snp.makeConstraints {
            $0.trailing.equalTo(rootView).offset(-Spacing.base02)
            $0.width.equalTo(Layout.imageWidth)
            $0.centerY.equalTo(rootView)
        }
    }
    
    func updateView() {
        titleLabel.text = viewModel.inputs.title
        descriptionLabel.text = viewModel.inputs.description

        typeImage.setImage(
            url: viewModel.inputs.image,
            placeholder: Assets.personalCredit.image
        )
    }
    
    func configure(with model: CardRecharge) {
        viewModel.inputs.viewConfigure(model: model)
    }
}

extension LoanTableViewCell: CardRechargeViewModelOutputs {
    func reloadData() {
        updateView()
    }
}
