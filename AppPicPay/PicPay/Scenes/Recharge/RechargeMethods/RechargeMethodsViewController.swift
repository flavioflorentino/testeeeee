import UI

final class RechargeMethodsViewController: LegacyViewController<RechargeMethodsViewModelType, RechargeMethodsCoordinating, UIView> {
    enum Accessibility: String {
        case loanTableViewCell
    }
    
    private let defaultCard = String(describing: CardRechargeTableViewCell.self)
    private let originalCard = String(describing: CardOriginalTableViewCell.self)
    private let registerCard = String(describing: RegisterDebitTableViewCell.self)
    private let loanCard = String(describing: LoanTableViewCell.self)
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableView.automaticDimension
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        return tableView
    }()
    
    private lazy var headerView: UIView = {
        let width = UIScreen.main.bounds.width
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 80))
        let header = RechargeMethodsHeader(frame: headerView.frame)
        headerView.addSubview(header)
        
        return headerView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        viewModel.inputs.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setUpNavigationBarDefaultAppearance(backgroundColor: Colors.groupedBackgroundPrimary.color)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override func configureViews() {
        title = RechargeLocalizable.title.text
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: DefaultLocalizable.btClose.text, style: .plain, target: self, action: #selector(tapCloseButton))
        view.backgroundColor = Colors.groupedBackgroundPrimary.color
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.constraintAllEdges(from: tableView, to: view)
    }
    
    private func setupTableView() {
        tableView.register(CardRechargeTableViewCell.self, forCellReuseIdentifier: defaultCard)
        tableView.register(CardOriginalTableViewCell.self, forCellReuseIdentifier: originalCard)
        tableView.register(RegisterDebitTableViewCell.self, forCellReuseIdentifier: registerCard)
        tableView.register(LoanTableViewCell.self, forCellReuseIdentifier: loanCard)
        tableView.tableHeaderView = headerView
    }
    
    @objc
    private func tapCloseButton() {
        viewModel.inputs.close()
    }
    
    @objc
    private func tapHelpCenter() {
        viewModel.inputs.didTapHelpCenter()
    }
}

extension RechargeMethodsViewController: RechargeMethodsViewModelOutputs {
    func addRightButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "green_help_button"), style: .done, target: self, action: #selector(tapHelpCenter))
    }
    
    func reloadData() {
        tableView.reloadData()
    }
    
    func didNextStep(action: RechargeMethodsAction) {
        coordinator.perform(action: action)
    }
    
    func didReceiveAnError(error: PicPayErrorDisplayable) {
        let alert = Alert(with: error)
        AlertMessage.showAlert(alert, controller: self) { [weak self] _, _, _ in
            self?.viewModel.inputs.close()
        }
    }
    
    func warningCard(title: String, desc: String) {
        let alert = Alert(title: title, text: desc)
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func showBanks(method: Int, banks: [String]) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        for (index, element) in banks.enumerated() {
            actionSheet.addAction(UIAlertAction(title: element, style: .default) { [weak self] _ in
                self?.viewModel.inputs.didTapBank(method: method, bank: index)
            })
        }
        actionSheet.addAction(UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil))
        present(actionSheet, animated: true, completion: nil)
    }
}

extension RechargeMethodsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        viewModel.inputs.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.inputs.numberOfRows(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        let type = viewModel.inputs.type(index: row)
        
        switch type {
        case .standard:
            return configureCardCell(tableView: tableView, indexPath: indexPath)
        case .addDebit:
            return configureRegisterDebitCell(tableView: tableView, indexPath: indexPath)
        case .original:
            return configureCardOriginalCell(tableView: tableView, indexPath: indexPath)
        case .loan:
            return configureLoanCell(tableView: tableView, indexPath: indexPath)
        }
    }
    
    private func configureCardCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: defaultCard, for: indexPath) as? CardRechargeTableViewCell else {
            return UITableViewCell()
        }
        
        let row = indexPath.row
        let card = viewModel.inputs.cardRecharge(index: row)
        cell.configure(with: card)
        
        return cell
    }
    
    private func configureLoanCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: loanCard, for: indexPath) as? LoanTableViewCell else {
            return UITableViewCell()
        }
        
        let row = indexPath.row
        let card = viewModel.inputs.cardRecharge(index: row)
        cell.configure(with: card)
        cell.accessibilityIdentifier = Accessibility.loanTableViewCell.rawValue
        
        return cell
    }
    
    private func configureCardOriginalCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: originalCard, for: indexPath) as? CardOriginalTableViewCell else {
            return UITableViewCell()
        }
        
        let row = indexPath.row
        let card = viewModel.inputs.cardRecharge(index: row)
        cell.configure(with: card)
        
        return cell
    }
    
    private func configureRegisterDebitCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: registerCard, for: indexPath) as? RegisterDebitTableViewCell else {
            return UITableViewCell()
        }
        
        let row = indexPath.row
        let card = viewModel.inputs.cardRecharge(index: row)
        cell.configure(with: card)
        
        return cell
    }
}

extension RechargeMethodsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        viewModel.inputs.didTap(index: row)
    }
}
