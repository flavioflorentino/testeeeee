import UI
import UIKit

final class RechargeBankViewController: LegacyViewController<RechargeBankViewModelType, RechargeBankCoordinating, UIView> {
    private lazy var headerView: HeaderRechargeBank = {
        let header = HeaderRechargeBank()
        header.translatesAutoresizingMaskIntoConstraints = false
        
        return header
    }()
    
    private lazy var footerView: FooterRechargeBank = {
        let footer = FooterRechargeBank()
        footer.delegate = self
        footer.translatesAutoresizingMaskIntoConstraints = false
        
        return footer
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var instructionsView: RechargeBankInstructionsView = {
        let view = RechargeBankInstructionsView()
        view.delegate = self
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.backgroundColor = .clear
        scrollView.showsVerticalScrollIndicator = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        
        return scrollView
    }()
    
    private lazy var imagePickerController: UIImagePickerController = {
        let picker = UIImagePickerController()
        picker.allowsEditing = false
        picker.delegate = self
        
        return picker
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.inputs.viewDidLoad()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(headerView)
        contentView.addSubview(footerView)
        contentView.addSubview(instructionsView)
    }
    
    override func configureViews() {
        title = RechargeLocalizable.addMoney.text
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: DefaultLocalizable.btClose.text, style: .plain, target: self, action: #selector(clickLeftButton))
        view.backgroundColor = Palette.ppColorGrayscale100.color
        instructionsView.configure(with: viewModel.inputs.rechargeBankModel)
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),

            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            contentView.widthAnchor.constraint(equalTo: view.widthAnchor),

            headerView.topAnchor.constraint(equalTo: contentView.topAnchor),
            headerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            headerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            instructionsView.topAnchor.constraint(equalTo: headerView.bottomAnchor),
            instructionsView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            instructionsView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            footerView.topAnchor.constraint(equalTo: instructionsView.bottomAnchor, constant: 20),
            footerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20),
            footerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            footerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
        ])
    }
    
    private func showOptions() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let takePicture = UIAlertAction(title: RechargeLocalizable.takePicture.text, style: .default) { [weak self] _ in
            self?.openCamera()
        }
        
        let chooseLibrary = UIAlertAction(title: RechargeLocalizable.chooseLibrary.text, style: .default) { [weak self] _ in
            self?.openLibrary()
        }
        
        let cancel = UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel)
        
        actionSheet.addAction(takePicture)
        actionSheet.addAction(chooseLibrary)
        actionSheet.addAction(cancel)
        
        present(actionSheet, animated: true)
    }
    
    private func openCamera() {
        guard PPPermission.camera().status == .authorized else {
            requestPermissionCamera()
            return
        }
        
        imagePickerController.sourceType = UIImagePickerController.isSourceTypeAvailable(.camera) ? .camera : .photoLibrary
        present(imagePickerController, animated: true)
    }
    
    private func openLibrary() {
        imagePickerController.sourceType = .photoLibrary
        present(imagePickerController, animated: true)
    }
    
    private func requestPermissionCamera() {
        PPPermission.requestCamera { [weak self] status in
            guard status == .authorized else {
                return
            }
            
            self?.openCamera()
        }
    }
    
    @objc
    private func clickLeftButton() {
        viewModel.inputs.close()
    }
}

// MARK: View Model Outputs
extension RechargeBankViewController: RechargeBankViewModelOutputs {
    func reloadData() {
        let header = viewModel.inputs.header()
        headerView.configureHeader(title: header.title, highligh: header.highlight)
        footerView.textDescription = viewModel.inputs.footer
        footerView.sendButtonText = viewModel.inputs.buttonTitle()
    }
    
    func didReceiveAnError(error: PicPayErrorDisplayable) {
        AlertMessage.showCustomAlertWithError(error, controller: self)
    }
    
    func didNextStep(action: RechargeBankAction) {
        coordinator.perform(action: action)
    }
    
    func successSendPhoto() {
        footerView.stopLoading()
        let alert = Alert(title: nil, text: RechargeLocalizable.successSending.text)
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func showOptionsUpload() {
        showOptions()
    }
    
    func showWarningUpload() {
        let alert = Alert(title: RechargeLocalizable.attention.text, text: RechargeLocalizable.askResubmit.text)
        
        let buttonConfirm = Button(title: DefaultLocalizable.btYes.text, type: .cta) { [weak self] controller, _ in
            controller.dismiss(animated: false)
            self?.showOptions()
        }
        let buttonCancel = Button(title: DefaultLocalizable.btCancel.text, type: .clean, action: .close)
        
        alert.buttons = [buttonConfirm, buttonCancel]
        AlertMessage.showAlert(alert, controller: self)
    }
}

extension RechargeBankViewController: FooterRechargeBankDelegate {
    func didTapSendButton() {
        viewModel.inputs.didTapUploadPhoto()
    }
    
    func didTapCancelButton() {
        let alert = Alert(title: RechargeLocalizable.cancelRechargeTitle.text, text: RechargeLocalizable.cancelRechargeMessage.text)
        
        let buttonConfirm = Button(title: RechargeLocalizable.confirmCancelRecharge.text, type: .destructive) { [weak self] controller, _ in
            self?.footerView.startLoading(action: .cancel)
            self?.viewModel.inputs.cancelRecharge()
            controller.dismiss(animated: false)
        }
        let buttonCancel = Button(title: RechargeLocalizable.noCancelRecharge.text, type: .clean, action: .close)
        alert.buttons = [buttonConfirm, buttonCancel]
        
        AlertMessage.showAlert(alert, controller: self)
    }
}

extension RechargeBankViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true)
        guard let image = info[.originalImage] as? UIImage else {
            return
        }
        
        footerView.startLoading(action: .send)
        viewModel.inputs.uploadRechargePhoto(image: image)
    }
}

extension RechargeBankViewController: RechargeBankInstructionsDelegate {
    func didLongPressField(value: String) {
        UIPasteboard.general.string = value
    }
}
