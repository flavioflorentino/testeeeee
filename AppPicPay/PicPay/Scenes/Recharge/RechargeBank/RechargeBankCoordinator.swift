import UIKit

enum RechargeBankAction {
    case cancelRecharge
    case close
}

protocol RechargeBankCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: RechargeBankAction)
}

final class RechargeBankCoordinator: RechargeBankCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: RechargeBankAction) {
        switch action {
        case .cancelRecharge:
            let controller = RechargeLoadFactory.make()
            viewController?.navigationController?.pushViewController(controller, animated: true)
            
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
