import Foundation

enum RechargeBankFactory {
    static func make(model: Recharge) -> RechargeBankViewController {
        let container = DependencyContainer()
        let service: RechargeBankServicing = RechargeBankService(dependencies: container)
        let viewModel: RechargeBankViewModelType = RechargeBankViewModel(model: model, service: service)
        var coordinator: RechargeBankCoordinating = RechargeBankCoordinator()
        let viewController = RechargeBankViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController

        return viewController
    }
}
