import Foundation

protocol RechargeBankViewModelInputs {
    var rechargeBankModel: RechargeBank { get }
    var footer: String { get }
    
    func viewDidLoad()
    func cancelRecharge()
    func buttonTitle() -> String
    func didTapUploadPhoto()
    func uploadRechargePhoto(image: UIImage)
    func header() -> (title: String, highlight: String)
    func close()
}

protocol RechargeBankViewModelOutputs: AnyObject {
    func reloadData()
    func didReceiveAnError(error: PicPayErrorDisplayable)
    func didNextStep(action: RechargeBankAction)
    func showWarningUpload()
    func showOptionsUpload()
    func successSendPhoto()
}

protocol RechargeBankViewModelType: AnyObject {
    var inputs: RechargeBankViewModelInputs { get }
    var outputs: RechargeBankViewModelOutputs? { get set }
}

final class RechargeBankViewModel: RechargeBankViewModelType, RechargeBankViewModelInputs {
    var inputs: RechargeBankViewModelInputs { self }
    weak var outputs: RechargeBankViewModelOutputs?

    private let service: RechargeBankServicing
    private var model: Recharge
    
    var footer: String {
        model.instructions.text ?? ""
    }
    
    var rechargeBankModel: RechargeBank {
        let bankName = model.bankName ?? ""
        let bankImage = model.bankImgUrl ?? ""
        let value = model.value
        
        return RechargeBank(bankName: bankName, bankImage: bankImage, value: value, instructions: model.instructions)
    }
    
    init(model: Recharge, service: RechargeBankServicing) {
        self.service = service
        self.model = model
    }
    
    func viewDidLoad() {
        outputs?.reloadData()
    }

    func header() -> (title: String, highlight: String) {
        switch model.destinationBank {
        case .tedDoc:
            let title = RechargeLocalizable.headerBank.text
            let highlight = RechargeLocalizable.headerBankExtra.text
            return (title: title, highlight: highlight)
            
        default:
            let title = RechargeLocalizable.titleHeaderBank.text
            let highlight = RechargeLocalizable.highlightHeaderBank.text
            return (title: title, highlight: highlight)
        }
    }
    
    func buttonTitle() -> String {
        guard model.rechargeDepositProofId != nil else {
            return RechargeLocalizable.sendPhoto.text
        }
        
        return RechargeLocalizable.resubmitPhoto.text
    }
    
    func cancelRecharge() {
        service.cancel(id: model.rechargeId) { [weak self] result in
            switch result {
            case .success:
                self?.outputs?.didNextStep(action: .cancelRecharge)
                
            case .failure(let error):
               self?.outputs?.didReceiveAnError(error: error)
            }
        }
    }
    
    func didTapUploadPhoto() {
        guard model.rechargeDepositProofId != nil else {
            outputs?.showOptionsUpload()
            return
        }
        
        outputs?.showWarningUpload()
    }
    
    func uploadRechargePhoto(image: UIImage) {
        service.uploadPhoto(rechargeId: model.rechargeId, image: image) { [weak self] result in
            switch result {
            case .success(let value):
                self?.model = value
                self?.outputs?.successSendPhoto()
                self?.outputs?.reloadData()
            
            case .failure(let error):
                self?.outputs?.didReceiveAnError(error: error)
            }
        }
    }
    
    func close() {
        outputs?.didNextStep(action: .close)
    }
}
