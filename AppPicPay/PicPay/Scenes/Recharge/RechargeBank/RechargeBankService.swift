import Core
import Foundation

protocol RechargeBankServicing {
    func uploadPhoto(rechargeId: String, image: UIImage, completion: @escaping (Result<Recharge, PicPayError>) -> Void)
    func cancel(id: String, completion: @escaping (Result<Void, PicPayError>) -> Void)
}

final class RechargeBankService: RechargeBankServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func uploadPhoto(rechargeId: String, image: UIImage, completion: @escaping (Result<Recharge, PicPayError>) -> Void) {
        let mainQueue = dependencies.mainQueue
        
        WSConsumer.uploadRechargePhoto(rechargeId, image: image) { recharge, error in
            mainQueue.async {
                guard let recharge = recharge else {
                    let picpayError = PicPayError(message: error?.localizedDescription ?? DefaultLocalizable.unexpectedError.text)
                    completion(.failure(picpayError))
                    return
                }
                completion(.success(recharge))
            }
        }
    }
    
    func cancel(id: String, completion: @escaping (Result<Void, PicPayError>) -> Void) {
        let mainQueue = dependencies.mainQueue
        
        WSConsumer.cancelRecharge(id) { _, error in
            mainQueue.async {
                guard let error = error else {
                    completion(.success(()))
                    return
                }
                let picpayError = PicPayError(message: error.localizedDescription)
                completion(.failure(picpayError))
            }
        }
    }
}
