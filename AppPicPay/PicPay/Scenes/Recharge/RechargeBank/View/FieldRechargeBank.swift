import Foundation
import UI

protocol FieldRechargeBankDelegate: AnyObject {
    func didTapField(value: String)
}

extension FieldRechargeBank.Layout {
    enum Font {
        static let title = UIFont.systemFont(ofSize: 17)
        static let description = UIFont.systemFont(ofSize: 17, weight: .medium)
        static let copy = UIFont.systemFont(ofSize: 17)
    }
    static let currencyLimitValue: Int = 8
}

final class FieldRechargeBank: UIView {
    fileprivate enum Layout {}

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.alignment = .center
        stackView.spacing = 7.0
        stackView.layoutMargins = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 0)
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = Layout.Font.title
        label.textColor = Palette.ppColorGrayscale400.color
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = Layout.Font.description
        label.textColor = Palette.ppColorGrayscale600.color
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var copiedLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = Layout.Font.copy
        label.textAlignment = .center
        label.isHidden = true
        label.textColor = Palette.ppColorGrayscale600.color
        label.text = RechargeLocalizable.copiedField.text
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale100.color
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var viewModel: FieldRechargeBankViewModelType = {
        let viewModel = FieldRechargeBankViewModel()
        viewModel.outputs = self
        
        return viewModel
    }()
    
    weak var delegate: FieldRechargeBankDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = Palette.ppColorGrayscale000.color
        addGesture()
    }
    
    private func addComponents() {
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(descriptionLabel)
        stackView.addArrangedSubview(copiedLabel)
        addSubview(stackView)
        addSubview(lineView)
    }
    
    private func layoutComponents() {
        titleLabel.setContentHuggingPriority(UILayoutPriority.required, for: .horizontal)
        descriptionLabel.setContentHuggingPriority(UILayoutPriority.defaultLow, for: .horizontal)
        copiedLabel.setContentHuggingPriority(UILayoutPriority.defaultLow, for: .horizontal)
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20)
        ])
        
        NSLayoutConstraint.activate([
            lineView.topAnchor.constraint(equalTo: stackView.bottomAnchor),
            lineView.bottomAnchor.constraint(equalTo: bottomAnchor),
            lineView.leadingAnchor.constraint(equalTo: leadingAnchor),
            lineView.trailingAnchor.constraint(equalTo: trailingAnchor),
            lineView.heightAnchor.constraint(equalToConstant: 1.0)
        ])
    }

    private func addGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapField))
        stackView.addGestureRecognizer(tap)
    }
    
    @objc
    private func didTapField() {
        guard
            let description = descriptionLabel.text,
            viewModel.inputs.copyField()
            else {
                return
        }
        showCopiedMessage()
        delegate?.didTapField(value: description)
    }
    
    private func showCopiedMessage() {
        titleLabel.isHidden = true
        descriptionLabel.isHidden = true
        copiedLabel.isHidden = false
        copiedLabel.alpha = 0
        UIView.animate(
            withDuration: 0.3,
            delay: 0,
            options: .curveEaseOut,
            animations: {
                self.copiedLabel.alpha = 1
            }, completion: { _ in
                UIView.animate(
                    withDuration: 0.3,
                    delay: 1,
                    options: .curveEaseOut,
                    animations: {
                        self.copiedLabel.alpha = 0
                    }, completion: { _ in
                        self.copiedLabel.isHidden = true
                        self.titleLabel.isHidden = false
                        self.descriptionLabel.isHidden = false
                    }
                )
            }
        )
}
    
    func configureView(model: FieldRechargeRow) {
        viewModel.inputs.viewConfigure(model: model)
    }
}

extension FieldRechargeBank: FieldRechargeBankViewModelOutputs {
    func reloadData() {
        titleLabel.text = viewModel.inputs.title
        descriptionLabel.text = viewModel.inputs.description
        descriptionLabel.textColor = viewModel.inputs.highlight ? Palette.ppColorNegative400.color : Palette.ppColorGrayscale600.color
    }
}
