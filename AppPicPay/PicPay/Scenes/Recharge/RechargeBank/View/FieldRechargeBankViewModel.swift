import Foundation

protocol FieldRechargeBankViewModelInputs {
    var title: String? { get }
    var description: String? { get }
    var highlight: Bool { get }
    
    func viewConfigure(model: FieldRechargeRow)
    func copyField() -> Bool
}

protocol FieldRechargeBankViewModelOutputs: AnyObject {
    func reloadData()
}

protocol FieldRechargeBankViewModelType: AnyObject {
    var inputs: FieldRechargeBankViewModelInputs { get }
    var outputs: FieldRechargeBankViewModelOutputs? { get set }
}

final class FieldRechargeBankViewModel: FieldRechargeBankViewModelType, FieldRechargeBankViewModelInputs {
    var inputs: FieldRechargeBankViewModelInputs { self }
    weak var outputs: FieldRechargeBankViewModelOutputs?
    
    private var model: FieldRechargeRow?
    
    var title: String? {
        model?.title
    }
    
    var description: String? {
        model?.description
    }
    
    var highlight: Bool {
        model?.highlight ?? false
    }
    
    func viewConfigure(model: FieldRechargeRow) {
        self.model = model
        outputs?.reloadData()
    }
    
    func copyField() -> Bool {
        model?.copyField ?? false
    }
}
