import Foundation
import UI

final class FieldHeaderRechargeBank: UIView {
    private let sizeImage: CGFloat = 35.0
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.alignment = .center
        stackView.spacing = 7.0
        stackView.layoutMargins = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 0)
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        label.textColor = Palette.ppColorGrayscale600.color
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()

    private lazy var imageView: UIImageView = {
        let image = UIImageView()
        image.backgroundColor = Palette.ppColorGrayscale100.color
        image.contentMode = .scaleAspectFill
        image.layer.cornerRadius = sizeImage / 2
        image.clipsToBounds = true
        
        return image
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale100.color
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    private func addComponents() {
        addSubview(stackView)
        addSubview(lineView)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20)
        ])
        
        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalToConstant: sizeImage),
            imageView.widthAnchor.constraint(equalToConstant: sizeImage)
        ])
        
        NSLayoutConstraint.activate([
            lineView.topAnchor.constraint(equalTo: stackView.bottomAnchor),
            lineView.bottomAnchor.constraint(equalTo: bottomAnchor),
            lineView.leadingAnchor.constraint(equalTo: leadingAnchor),
            lineView.trailingAnchor.constraint(equalTo: trailingAnchor),
            lineView.heightAnchor.constraint(equalToConstant: 1.0)
        ])
    }
    
    func configureView(image: String, title: String) {
        imageView.setImage(url: URL(string: image))
        titleLabel.text = title
        
        stackView.addArrangedSubview(imageView)
        stackView.addArrangedSubview(titleLabel)
    }
}
