import Foundation
import UI

protocol RechargeBankInstructionsDelegate: AnyObject {
    func didLongPressField(value: String)
}

final class RechargeBankInstructionsView: UIView {
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.spacing = 0.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    }()
    
    private lazy var viewModel: RechargeBankInstructionsViewModelType = {
        let viewModel = RechargeBankInstructionsViewModel()
        viewModel.outputs = self
        
        return viewModel
    }()
   
    weak var delegate: RechargeBankInstructionsDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    private func addComponents() {
        addSubview(stackView)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.constraintAllEdges(from: stackView, to: self)
    }
    
    func configure(with model: RechargeBank) {
        viewModel.inputs.viewConfigure(model: model)
    }
}

extension RechargeBankInstructionsView: RechargeBankInstructionsViewModelOutputs {
    func addHeader(title: String, image: String) {
        let row = FieldHeaderRechargeBank()
        row.configureView(image: image, title: title)
        
        stackView.addArrangedSubview(row)
    }
    
    func addRow(model: FieldRechargeRow) {
        let row = FieldRechargeBank()
        row.delegate = self
        row.configureView(model: model)
        
        stackView.addArrangedSubview(row)
    }
}

extension RechargeBankInstructionsView: FieldRechargeBankDelegate {
    func didTapField(value: String) {
        delegate?.didLongPressField(value: value)
    }
}
