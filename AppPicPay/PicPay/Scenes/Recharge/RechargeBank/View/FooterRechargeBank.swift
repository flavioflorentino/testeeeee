import Foundation
import UI

protocol FooterRechargeBankDelegate: AnyObject {
    func didTapSendButton()
    func didTapCancelButton()
}

final class FooterRechargeBank: UIView {
    private let sizeButton: CGFloat = 44.0
    
    private lazy var sendButton: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = sizeButton / 2
        button.setImage(#imageLiteral(resourceName: "ico_camera_white"), for: .normal)
        button.addTarget(self, action: #selector(didTapSendButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    private lazy var cancelButton: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = sizeButton / 2
        button.addTarget(self, action: #selector(didTapCancelButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = Palette.ppColorGrayscale400.color
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    weak var delegate: FooterRechargeBankDelegate?
    
    var textDescription: String = "" {
        didSet {
            descriptionLabel.text = textDescription
        }
    }
    
    var sendButtonText: String = "" {
        didSet {
            let button = Button(title: sendButtonText, type: .cta)
            sendButton.configure(with: button)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .clear
        cancelButton.configure(with: Button(title: RechargeLocalizable.cancelRecharge.text, type: .cleanDestructive))
    }
    
    private func addComponents() {
        addSubview(descriptionLabel)
        addSubview(sendButton)
        addSubview(cancelButton)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: topAnchor),
            descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20)
        ])
        
        NSLayoutConstraint.activate([
            sendButton.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 20),
            sendButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            sendButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            sendButton.heightAnchor.constraint(equalToConstant: sizeButton)
        ])
        
        NSLayoutConstraint.activate([
            cancelButton.topAnchor.constraint(equalTo: sendButton.bottomAnchor, constant: 10),
            cancelButton.bottomAnchor.constraint(equalTo: bottomAnchor),
            cancelButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            cancelButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            cancelButton.heightAnchor.constraint(equalToConstant: sizeButton)
        ])
    }
    
    @objc
    private func didTapSendButton() {
        delegate?.didTapSendButton()
    }
    
    @objc
    private func didTapCancelButton() {
        delegate?.didTapCancelButton()
    }
    
    func startLoading(action: Action) {
        guard action == .send else {
            cancelButton.startLoadingAnimating(style: .gray)
            sendButton.isEnabled = false
            return
        }
        
        sendButton.startLoadingAnimating()
        cancelButton.isEnabled = false
    }
    
    func stopLoading() {
        sendButton.stopLoadingAnimating()
        cancelButton.stopLoadingAnimating()
    }
}

extension FooterRechargeBank {
    enum Action {
        case send
        case cancel
    }
}
