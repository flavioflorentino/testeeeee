import Foundation
import CoreLegacy

protocol RechargeBankInstructionsViewModelInputs {
    func viewConfigure(model: RechargeBank)
}

protocol RechargeBankInstructionsViewModelOutputs: AnyObject {
    func addHeader(title: String, image: String)
    func addRow(model: FieldRechargeRow)
}

protocol RechargeBankInstructionsViewModelType: AnyObject {
    var inputs: RechargeBankInstructionsViewModelInputs { get }
    var outputs: RechargeBankInstructionsViewModelOutputs? { get set }
}

final class RechargeBankInstructionsViewModel: RechargeBankInstructionsViewModelType, RechargeBankInstructionsViewModelInputs {
    var inputs: RechargeBankInstructionsViewModelInputs { self }
    weak var outputs: RechargeBankInstructionsViewModelOutputs?
    
    private var model: RechargeBank?
    
    func viewConfigure(model: RechargeBank) {
        self.model = model
        setupHeader()
        setupInstructions()
    }
    
    private func setupHeader() {
        guard let model = model else {
            return
        }
        
        let title = model.bankName
        let image = model.bankImage
        outputs?.addHeader(title: title, image: image)
    }
    
    private func setupInstructions() {
        guard let model = model else {
            return
        }
        let value = CurrencyFormatter.brazillianRealString(from: NSDecimalNumber(string: model.value))
        var array: [FieldRechargeRow?] = []
        array.append(FieldRechargeRow(title: RechargeLocalizable.valueField.text, description: value))
        array.append(FieldRechargeRow(title: RechargeLocalizable.agencyField.text, description: model.instructions.agency, copyField: true))
        array.append(FieldRechargeRow(title: RechargeLocalizable.accountField.text, description: model.instructions.account, copyField: true))
        array.append(FieldRechargeRow(title: RechargeLocalizable.cnpjField.text, description: model.instructions.cnpj, copyField: true, highlight: false))
        array.append(FieldRechargeRow(title: RechargeLocalizable.identifierField.text, description: model.instructions.identifier, copyField: true, highlight: true))
        
        for element in array {
            guard let element = element else {
                return
            }
            outputs?.addRow(model: element)
        }
    }
}
