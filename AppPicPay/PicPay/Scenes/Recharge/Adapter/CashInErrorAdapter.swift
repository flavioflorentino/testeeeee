import CashIn
import Foundation

protocol CashInErrorAdapting {
    static func transform(error: PicPayError) -> CashInErrorModel?
}

enum CashInErrorAdapter: CashInErrorAdapting {
    static func transform(error: PicPayError) -> CashInErrorModel? {
        let decoder = JSONDecoder()
        let errorTitle = !error.title.isEmpty ? error.title : Strings.RechargeWireTransfer.Default.Error.title
        let defaultError = CashInErrorModel(code: 0,
                                            title: errorTitle,
                                            message: error.message,
                                            image: "",
                                            buttonText: Strings.RechargeWireTransfer.Default.action,
                                            buttonUrl: "CASHIN")
        
        guard let jsonData = error.data?.dictionaryObject?.toData(),
              let cashInError = try? decoder.decode(CashInErrorModel.self, from: jsonData) else {
            return defaultError
        }
        
        return cashInError
    }
}
