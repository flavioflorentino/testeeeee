import UI
import UIKit

final class RechargeStatusFooterView: UIView, ViewConfiguration {
    private enum Layout {
        static let marginTopBottom: CGFloat = 14
        static let marginLeadingTrailing: CGFloat = 16
        static let sizeLine: CGFloat = 1
    }
    
    private lazy var valueFooter: StatusFooterView = {
        let status = StatusFooterView()
        status.title = RechargeLocalizable.valueField.text
        status.translatesAutoresizingMaskIntoConstraints = false
        
        return status
    }()
    
    private lazy var cardFooter: StatusFooterView = {
        let status = StatusFooterView()
        status.title = RechargeLocalizable.cardField.text
        status.translatesAutoresizingMaskIntoConstraints = false
        
        return status
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale300.color
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
    
    func buildViewHierarchy() {
        addSubview(lineView)
        addSubview(valueFooter)
        addSubview(cardFooter)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            lineView.topAnchor.constraint(equalTo: topAnchor),
            lineView.leadingAnchor.constraint(equalTo: leadingAnchor),
            lineView.trailingAnchor.constraint(equalTo: trailingAnchor),
            lineView.heightAnchor.constraint(equalToConstant: Layout.sizeLine)
        ])
        
        NSLayoutConstraint.activate([
            valueFooter.topAnchor.constraint(equalTo: lineView.bottomAnchor, constant: Layout.marginTopBottom),
            valueFooter.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor, constant: Layout.marginLeadingTrailing),
            valueFooter.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor, constant: -Layout.marginLeadingTrailing),
            valueFooter.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            cardFooter.topAnchor.constraint(equalTo: valueFooter.bottomAnchor, constant: 4),
            cardFooter.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor, constant: Layout.marginLeadingTrailing),
            cardFooter.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor, constant: -Layout.marginLeadingTrailing),
            cardFooter.centerXAnchor.constraint(equalTo: centerXAnchor),
            cardFooter.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Layout.marginTopBottom)
        ])
    }
    
    func setupView(value: String, card: String) {
        valueFooter.value = value
        cardFooter.value = card
    }
}
