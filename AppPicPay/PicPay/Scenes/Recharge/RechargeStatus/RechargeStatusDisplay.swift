import UIKit

protocol RechargeStatusDisplay: AnyObject {
    func displayHeader(image: UIImage, title: String, description: String)
    func displayMainButton(title: String, isHidden: Bool)
    func displayAccessoryButton(title: String, isHidden: Bool)
    func displayFooter(value: String, aliasCard: String)
}
