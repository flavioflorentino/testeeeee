enum RechargeStatusFactory {
    static func make(model: RechargeStatus) -> RechargeStatusViewController {
        let service: RechargeStatusServicing = RechargeStatusService()
        let coordinator: RechargeStatusCoordinating = RechargeStatusCoordinator()
        let presenter: RechargeStatusPresenting = RechargeStatusPresenter(coordinator: coordinator)
        let viewModel = RechargeStatusViewModel(model: model, presenter: presenter, service: service)
        let viewController = RechargeStatusViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
