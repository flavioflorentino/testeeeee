import CoreLegacy

protocol RechargeStatusPresenting: AnyObject {
    var viewController: RechargeStatusDisplay? { get set }
    func viewDidLoad(model: RechargeStatus)
    func didNextStep(action: RechargeStatusAction)
}

final class RechargeStatusPresenter: RechargeStatusPresenting {
    private let coordinator: RechargeStatusCoordinating
    var viewController: RechargeStatusDisplay?

    init(coordinator: RechargeStatusCoordinating) {
        self.coordinator = coordinator
    }
    
    func viewDidLoad(model: RechargeStatus) {
        updateViewType(type: model.type)
        updateFooter(value: model.value, aliasCard: model.aliasCard)
    }
    
    private func updateViewType(type: RechargeStatus.StatusType) {
        switch type {
        case .success:
            configureSuccess()
        case .faq(let description):
            configureFaq(description: description)
        case .failure:
            configureFailure()
        case let .failureWithMessage(description, _):
            configureFailure(description: description)
        }
    }
    
    private func configureSuccess() {
        viewController?.displayHeader(
            image: Assets.Icons.icoGreenCheckmark.image,
            title: RechargeLocalizable.statusSuccessTitle.text,
            description: RechargeLocalizable.statusSuccessDescription.text
        )
        viewController?.displayMainButton(title: RechargeLocalizable.backToStart.text, isHidden: false)
        viewController?.displayAccessoryButton(title: String(), isHidden: true)
    }
    
    private func configureFaq(description: String) {
        viewController?.displayHeader(
            image: Assets.CreditPicPay.Registration.sad3.image,
            title: RechargeLocalizable.statusFailureTitle.text,
            description: description
        )
        
        viewController?.displayMainButton(title: RechargeLocalizable.knowMore.text, isHidden: false)
        viewController?.displayAccessoryButton(title: RechargeLocalizable.backToStart.text, isHidden: false)
    }
    
    private func configureFailure(description: String? = nil) {
        viewController?.displayHeader(
            image: Assets.CreditPicPay.Registration.sad3.image,
            title: RechargeLocalizable.statusFailureTitle.text,
            description: description ?? RechargeLocalizable.statusFailureDescription.text
        )
           
        viewController?.displayMainButton(title: DefaultLocalizable.tryAgain.text, isHidden: false)
        viewController?.displayAccessoryButton(title: RechargeLocalizable.backToStart.text, isHidden: false)
    }
    
    private func updateFooter(value: Double, aliasCard: String) {
        guard let currency = CurrencyFormatter.brazillianRealString(from: NSNumber(value: value)) else {
            return
        }
        
        viewController?.displayFooter(value: currency, aliasCard: aliasCard)
    }
    
    func didNextStep(action: RechargeStatusAction) {
        coordinator.perform(action: action)
    }
}
