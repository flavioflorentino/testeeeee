protocol RechargeStatusViewModelInputs: AnyObject {
    func viewDidLoad()
    func didTapClose()
    func didTapMainButton()
    func didTapAccessoryButton()
}

final class RechargeStatusViewModel {
    private let model: RechargeStatus
    private let presenter: RechargeStatusPresenting
    private let service: RechargeStatusServicing
    
    init(model: RechargeStatus, presenter: RechargeStatusPresenting, service: RechargeStatusServicing) {
        self.model = model
        self.presenter = presenter
        self.service = service
    }
}

extension RechargeStatusViewModel: RechargeStatusViewModelInputs {
    func viewDidLoad() {
        presenter.viewDidLoad(model: model)
    }
    
    func didTapClose() {
        presenter.didNextStep(action: .close)
    }
    
    func didTapMainButton() {
        switch model.type {
        case .success:
            presenter.didNextStep(action: .close)
        case .failure(let completion), .failureWithMessage(_, let completion):
            didFailureWithCompletion(completion: completion)
        case .faq:
            openFaq()
        }
    }
    
    func didFailureWithCompletion(completion: (() -> Void)?) {
        presenter.didNextStep(action: .back)
        completion?()
    }
    
    func didTapAccessoryButton() {
        presenter.didNextStep(action: .close)
    }
    
    private func openFaq() {
        guard let url = URL(string: service.linkFaq) else {
            return
        }
        
        presenter.didNextStep(action: .openFaq(url))
    }
}
