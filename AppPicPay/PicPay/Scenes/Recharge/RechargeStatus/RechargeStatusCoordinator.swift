import UIKit

enum RechargeStatusAction {
    case close
    case openFaq(URL)
    case back
}

protocol RechargeStatusCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RechargeStatusAction)
}

final class RechargeStatusCoordinator: RechargeStatusCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: RechargeStatusAction) {
        switch action {
        case .close:
            viewController?.dismiss(animated: false) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: FeedViewController.Observables.FeedNeedReload.rawValue), object: nil)
                AppManager.shared.mainScreenCoordinator?.showHomeScreen()
            }

        case .openFaq(let url):
            guard let viewController = viewController else {
                return
            }
            
            DeeplinkHelper.handleDeeplink(withUrl: url, from: viewController)
        case .back:
            viewController?.navigationController?.popViewController(animated: false)
        }
    }
}
