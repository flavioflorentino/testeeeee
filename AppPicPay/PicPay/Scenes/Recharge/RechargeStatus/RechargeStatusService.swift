import FeatureFlag
import Foundation

protocol RechargeStatusServicing {
    var linkFaq: String { get }
}

final class RechargeStatusService: RechargeStatusServicing {
    var linkFaq: String {
        FeatureManager.text(.faqErrorDepositDebit)
    }
}
