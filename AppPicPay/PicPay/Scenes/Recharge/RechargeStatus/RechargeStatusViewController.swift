import UI
import UIKit

final class RechargeStatusViewController: ViewController<RechargeStatusViewModelInputs, UIView> {
    private enum Layout {
        static let imageSize: CGFloat = 100
        static let margin: CGFloat = 24
        static let sizeButton: CGFloat = 48
    }

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 28, weight: .semibold)
        label.textAlignment = .center
        label.textColor = Palette.ppColorGrayscale600.color
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.textAlignment = .center
        label.textColor = Palette.ppColorGrayscale600.color
        label.numberOfLines = 5
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var footerView: RechargeStatusFooterView = {
        let footerView = RechargeStatusFooterView()
        footerView.translatesAutoresizingMaskIntoConstraints = false
        
        return footerView
    }()
    
    private lazy var mainButton: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = Layout.sizeButton / 2
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(tapMainButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    private lazy var accessoryButton: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = Layout.sizeButton / 2
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(tapAccessoryButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.viewDidLoad()
    }
     
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale100.color
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: #imageLiteral(resourceName: "icon_round_close_green"),
            style: .plain,
            target: self,
            action: #selector(didTapClose)
        )
    }
    
    override func buildViewHierarchy() {
        view.addSubview(imageView)
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(mainButton)
        view.addSubview(accessoryButton)
        view.addSubview(footerView)
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 90),
            imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            imageView.heightAnchor.constraint(equalToConstant: Layout.imageSize),
            imageView.widthAnchor.constraint(equalToConstant: Layout.imageSize)
        ])
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: Layout.margin),
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Layout.margin),
            titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Layout.margin)
        ])
        
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8),
            descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Layout.margin),
            descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Layout.margin)
        ])
        
        NSLayoutConstraint.activate([
            mainButton.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: Layout.margin),
            mainButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Layout.margin),
            mainButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Layout.margin),
            mainButton.heightAnchor.constraint(equalToConstant: Layout.sizeButton)
        ])
        
        NSLayoutConstraint.activate([
            accessoryButton.topAnchor.constraint(equalTo: mainButton.bottomAnchor, constant: 4),
            accessoryButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Layout.margin),
            accessoryButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Layout.margin),
            accessoryButton.bottomAnchor.constraint(lessThanOrEqualTo: footerView.topAnchor),
            accessoryButton.heightAnchor.constraint(equalToConstant: Layout.sizeButton)
        ])
        
        NSLayoutConstraint.activate([
            footerView.bottomAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.bottomAnchor),
            footerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            footerView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
    
    @objc
    private func tapMainButton() {
        viewModel.didTapMainButton()
    }
    
    @objc
    private func tapAccessoryButton() {
        viewModel.didTapAccessoryButton()
    }
    
    @objc
    private func didTapClose() {
        viewModel.didTapClose()
    }
}

// MARK: View Model Outputs
extension RechargeStatusViewController: RechargeStatusDisplay {
    func displayHeader(image: UIImage, title: String, description: String) {
        imageView.image = image
        titleLabel.text = title
        descriptionLabel.text = description
    }
    
    func displayMainButton(title: String, isHidden: Bool) {
        let button = Button(title: title)
        mainButton.configure(with: button)
        mainButton.isHidden = isHidden
    }
    
    func displayAccessoryButton(title: String, isHidden: Bool) {
        let button = Button(title: title, type: .cleanUnderlined)
        accessoryButton.configure(with: button)
        accessoryButton.isHidden = isHidden
    }
    
    func displayFooter(value: String, aliasCard: String) {
        footerView.setupView(value: value, card: aliasCard)
    }
}
