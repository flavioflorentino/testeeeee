import Foundation

struct RechargeRequest: Encodable {
    let rechargeTypeId: Int
    let value: String
    let cardSelected: String
    let biometry: Bool
    let password: String
    let cvv: String?
    let chargedValue: String?
    let serviceFee: String?
}

extension RechargeRequest {
    private enum CodingKeys: String, CodingKey {
        case rechargeTypeId = "recharge_type_id"
        case value
        case cardSelected = "debit_card_id"
        case biometry
        case chargedValue = "value_charged"
        case serviceFee = "service_charge"
    }
}
