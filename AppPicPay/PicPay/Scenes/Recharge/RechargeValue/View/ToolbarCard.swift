import Foundation
import UI

final class ToolbarCard: UIView {
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.spacing = 8.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    private var titleAttributed: [NSAttributedString.Key: Any] = {
        let font = UIFont.systemFont(ofSize: 12.0, weight: .medium)
        let attributes: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: Palette.ppColorGrayscale600.color]
        
        return attributes
    }()
    
    private var highlightAttributed: [NSAttributedString.Key: Any] = {
        let font = UIFont.systemFont(ofSize: 12.0, weight: .medium)
        let attributes: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: Palette.ppColorBranding300.color, .underlineStyle: NSUnderlineStyle.single.rawValue]
        
        return attributes
    }()
    
    private lazy var cardImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        
        return image
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .clear
    }
    
    private func addComponents() {
        addSubview(stackView)
        stackView.addArrangedSubview(cardImage)
        stackView.addArrangedSubview(titleLabel)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
            stackView.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            cardImage.heightAnchor.constraint(equalToConstant: 16),
            cardImage.widthAnchor.constraint(equalToConstant: 24)
        ])
    }
    
    func setupView(title: String, description: String, image: String) {
        let text = title + " " + description
        let range = NSString(string: text).range(of: description)
        
        let attributedTex = NSMutableAttributedString(string: text, attributes: titleAttributed)
        attributedTex.addAttributes(highlightAttributed, range: range)
        
        titleLabel.attributedText = attributedTex
        let url = URL(string: image)
        cardImage.setImage(url: url)
    }
}
