import Foundation
import UI

protocol ValueMoneyDelegate: AnyObject {
    func onTextChange(value: Double)
}

final class ValueMoney: UIView {
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 2.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        label.textAlignment = .center
        
        return label
    }()
    
    private lazy var currencyStackView: ValueCurrencyStackView = {
        let stackView = ValueCurrencyStackView()
        stackView.onTextChange = { [weak self] value in
            self?.delegate?.onTextChange(value: value)
        }
        return stackView
    }()
    
    var valueFieldEnabled: Bool = false {
        didSet {
           currencyStackView.valueTextField.isEnabled = valueFieldEnabled
        }
    }
    
    var value: Double {
        guard let text = currencyStackView.valueTextField.text else {
            return 0.0
        }
        
        return sanitizedCurrency(value: text) ?? 0.0
    }
    
    weak var delegate: ValueMoneyDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        titleLabel.text = RechargeLocalizable.questionAdd.text
    }
    
    private func addComponents() {
        rootStackView.addArrangedSubview(titleLabel)
        rootStackView.addArrangedSubview(currencyStackView)
        addSubview(rootStackView)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.constraintAllEdges(from: rootStackView, to: self)
    }
    
    private func sanitizedCurrency(value: String) -> Double? {
        let sanitizedValue = value.replacingOccurrences(of: ".", with: "").replacingOccurrences(of: ",", with: ".").trim()
        return Double(sanitizedValue)
    }
    
    func resignValueResponder() {
        currencyStackView.valueTextField.resignFirstResponder()
    }
}
