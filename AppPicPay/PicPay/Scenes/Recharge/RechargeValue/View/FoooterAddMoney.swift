import Foundation

protocol FoooterAddMoneyDelegate: AnyObject {
    func didTapChangeCard()
    func didTapButton()
}

final class FoooterAddMoney: UIView {
    private enum Layout {
        static let marginButton: CGFloat = 20
        static let heightButton: CGFloat = 48
    }
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 8.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    }()
    
    private lazy var toolBar: ToolbarCard = {
        let toolBar = ToolbarCard()
        toolBar.translatesAutoresizingMaskIntoConstraints = false
        
        return toolBar
    }()
    
    private lazy var button: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = Layout.heightButton / 2
        button.addTarget(self, action: #selector(tapButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    var titleButton: String = "" {
        didSet {
            button.configure(with: Button(title: titleButton))
        }
    }
    
    var buttonIsEnabled: Bool = false {
        didSet {
            button.isEnabled = buttonIsEnabled
        }
    }
    
    var toolBarIsHidden: Bool = true {
        didSet {
            toolBar.isHidden = toolBarIsHidden
        }
    }
    
    weak var delegate: FoooterAddMoneyDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .clear
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapCard))
        toolBar.addGestureRecognizer(tap)
    }
    
    private func addComponents() {
        stackView.addArrangedSubview(toolBar)
        stackView.addArrangedSubview(button)
        addSubview(stackView)
    }
    
    private func layoutComponents() {
        if #available(iOS 11.0, *) {
            stackView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -Layout.marginButton).isActive = true
        } else {
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Layout.marginButton).isActive = true
        }
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Layout.marginButton),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Layout.marginButton)
        ])
        
        NSLayoutConstraint.activate([
            button.heightAnchor.constraint(equalToConstant: Layout.heightButton)
        ])
    }
    
    @objc
    private func tapCard() {
        delegate?.didTapChangeCard()
    }
    
    @objc
    private func tapButton() {
        delegate?.didTapButton()
    }
    
    func setupCard(title: String, image: String) {
        toolBar.setupView(title: title, description: RechargeLocalizable.changeCard.text, image: image)
    }
    
    func startLoad() {
        button.startLoadingAnimating()
        toolBar.isUserInteractionEnabled = false
    }
    
    func stopLoad() {
        button.stopLoadingAnimating()
        toolBar.isUserInteractionEnabled = true
    }
}
