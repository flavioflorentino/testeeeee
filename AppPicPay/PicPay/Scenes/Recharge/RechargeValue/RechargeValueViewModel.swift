import AnalyticsModule
import Core
import FeatureFlag
import Foundation

final class RechargeValueViewModel: RechargeValueViewModelType, RechargeValueViewModelInputs {
    typealias Dependencies = HasKVStore & HasFeatureManager
    private let dependencies: Dependencies
    
    var inputs: RechargeValueViewModelInputs { self }
    weak var outputs: RechargeValueViewModelOutputs?

    private let service: RechargeValueServicing
    private let paymentService: RechargePaymentServicing
    private let model: RechargeMethod
    private var value: Double
    private var informedCvv: String?
    private var rechargeFeeInfo: RechargeFeeInfo?

    //Mark - Adyen Stuff
    private let adyen: AdyenServicing
    private var transaction: ADYTransaction?
    private var adyenParameters: [String: Any]
    private var adyenPayload: [String: String]
    private let tracking3dsManager: RechargeValue3DSTrackingManaging
    
    private var governmentOnboardinguUserDefaultsKey: String {
        RechargeValueUserDefaultKey.governmentRechargeValueOnboardingHasBeenShown.rawValue
    }
    
    private var governmentOnboardingHasBeenShown: Bool {
        dependencies.kvStore.getFirstTimeOnlyEvent(governmentOnboardinguUserDefaultsKey)
    }
    
    private var isCashInRedesignAvailable: Bool {
        dependencies.featureManager.isActive(.isCashInRedesignAvailable)
    }

    init(
        model: RechargeMethod,
        service: RechargeValueServicing,
        paymentService: RechargePaymentServicing,
        dependencies: Dependencies,
        adyen: AdyenServicing = AdyenWorker(),
        tracking3dsManager: RechargeValue3DSTrackingManaging = RechargeValue3DSTrackingManager()
    ) {
        self.model = model
        self.service = service
        self.paymentService = paymentService
        self.value = 0
        self.adyen = adyen
        self.adyenParameters = [:]
        self.adyenPayload = [:]
        self.tracking3dsManager = tracking3dsManager
        self.dependencies = dependencies
    }

    func viewDidAppear() {
        guard model.typeId == .debit else {
            return
        }
        Analytics.shared.log(RechargeEvent.recharge(.rechargeDebit))
    }

    func headerInfo() -> (title: String, desc: String?) {
        switch model.typeId {
        case .bank, .original, .loan:
            return (title: RechargeLocalizable.bankTransfer.text, desc: model.optionSelectorTitle)
        case .bill:
            return (title: RechargeLocalizable.billTransfer.text, desc: nil)
        case .debit, .emergencyAid:
            return (title: RechargeLocalizable.debitTransfer.text, desc: nil)
        case .lending, .picpayAccount, .deeplink:
            fatalError("wrong flow")
        }
    }

    func titleButton() -> String {
        switch model.typeId {
        case .bank, .original, .debit, .loan, .emergencyAid:
            return RechargeLocalizable.addMoney.text
        case .bill:
            return RechargeLocalizable.addBill.text
        case .lending, .picpayAccount, .deeplink:
            fatalError("wrong flow")
        }
    }

    func didTapChangeCard() {
        Analytics.shared.log(RechargeEvent.tapChangeCard)
        let onlyVirtualCards = model.typeId == .emergencyAid
        outputs?.didNextStep(action: .changeCard(onlyVirtualCards: onlyVirtualCards))
    }

    func shouldEnableButton(forValue value: Double) -> Bool {
        value > 0
    }

    func displayCard() -> (title: String, image: String) {
        guard let card = cardSelected else {
            let picpay = PicPayError(message: RechargeLocalizable.errorDebit.text)
            outputs?.didReceiveAnError(error: picpay)
            return("", "")
        }

        return (title: card.alias, card.image)
    }

    func makeAuthenticatedRecharge(value: Double, password: String, biometry: Bool) {
        guard checkNeedCvv(value: value) else {
            authenticatedRecharge(value: value, password: password, biometry: biometry)
            return
        }

        openFlowInsertCvv(isEmergencyAid: model.typeId == .emergencyAid,
                          value: value,
                          password: password,
                          biometry: biometry)
    }

    func makeRecharge(value: Double, token: String) {
        let decimal = NSDecimalNumber(value: value)

        service.makeRecharge(method: model, value: decimal, security: token) { [weak self] result in
            switch result {
            case .success(let recharge):
                self?.trackEvent(recharge: recharge)
                self?.makeRechargeSuccess(with: recharge)

            case .failure(let error):
                self?.makeRechargeError(error: error)
            }
        }
    }

    func challengeFinishSuccess() {
        adyen.complete(payload: adyenParameters,
                       succeeded: { [weak self] _ in
                        self?.tracking3dsManager.analytics3DSResponse(type: .authorized,
                                                                      cardBrand: self?.analyticsCardInfo()?.cardBrand,
                                                                      cardIssuer: self?.analyticsCardInfo()?.cardIssuer)
                        self?.authenticatedRechargeSuccess()
                       },
                       error: { [weak self] error in
                        self?.authenticatedRechargeError(error)
                        self?.tracking3dsManager.analytics3DSResponse(type: .refused,
                                                                      cardBrand: self?.analyticsCardInfo()?.cardBrand,
                                                                      cardIssuer: self?.analyticsCardInfo()?.cardIssuer)
                       })
    }

    func challengeFinishError(error: Error) {
        let nsError = error as NSError
        guard Adyen3DS.challengeCancelled(nsError) else {
            let picpayError = PicPayError(message: nsError.localizedDescription, code: String(nsError.code))
            tracking3dsManager.analyticsErrorChallenge(description: nsError.localizedDescription, cardBrand: analyticsCardInfo()?.cardBrand)
            authenticatedRechargeError(picpayError) {
                self.outputs?.showLoading()
                self.continueOn3DSTransaction()
            }
            return
        }

        tracking3dsManager.analyticsCancelledChallenge(cardBrand: analyticsCardInfo()?.cardBrand, cardIssuer: analyticsCardInfo()?.cardIssuer)
    }

    func continueOn3DSTransaction() {
        adyen.transaction(with: adyenPayload,
                          hasChallenge: { [weak self] transaction, parameters, payload in
                            guard let transaction = transaction as? ADYTransaction,
                                  let parameters = parameters as? ADYChallengeParameters,
                                  let controller = self?.outputs,
                                  let self = self else { return }
                            
                            self.transaction = transaction
                            self.adyenParameters = payload
                            
                            self.analyticsStartChallenge()
                            self.transaction?.performChallenge(with: parameters, delegate: controller)
                          },
                          succeeded: { [weak self] _ in
                            self?.authenticatedRechargeSuccess()
                          },
                          error: { [weak self] error in
                            self?.authenticatedRechargeError(error) {
                                self?.outputs?.showLoading()
                                self?.continueOn3DSTransaction()
                            }
                          },
                          injecting: ADYService.self)
    }

    func rechargeIsAuthenticated() -> Bool {
        isAuthenticatedRecharge
    }

    func showOriginalWarning() {
        outputs?.didNextStep(action: .close)
    }

    func shouldFullScreenLoading() -> Bool {
        isAuthenticatedRecharge
    }

    func checkoutAuthenticatedProcess(value: Double) {
        guard model.typeId == .emergencyAid, service.isDepositDebitFeeEnabled else {
            outputs?.displayPasswordPopup()
            return
        }
        outputs?.didNextStep(action: .showRechargeDebitFee(value: value) { [weak self] rechargeFeeInfo in
            self?.rechargeFeeInfo = rechargeFeeInfo
            self?.outputs?.displayPasswordPopup()
        })
    }
}

extension RechargeValueViewModel {
    private var isAuthenticatedRecharge: Bool {
        model.typeId == .debit || model.typeId == .emergencyAid
    }

    private var cardSelected: CardBank? {
        model.typeId == .debit ? service.getDebitCardSelected() : virtualCard
    }

    private var virtualCard: CardBank? {
        let idDebitCardSelected = service.getDebitCardSelected()?.id ?? "0"
        let virtualCards = service.getCardsBank().filter { $0.isOneShotCvv ?? false }

        guard virtualCards.contains(where: { $0.id == idDebitCardSelected }) else {
            return virtualCards.first
        }

        return service.getDebitCardSelected()
    }

    private func authenticatedRecharge(value: Double, password: String, biometry: Bool) {
        guard let request = createRechargeRequest(value: value, password: password, biometry: biometry) else {
            let picpay = PicPayError(message: RechargeLocalizable.errorDebit.text)
            outputs?.didReceiveAnError(error: picpay)
            return
        }

        self.value = value
        paymentService.authenticatedRecharge(request: request) { [weak self] result in
            switch result {
            case .success:
                self?.authenticatedRechargeSuccess()
            case .failure(let error):
                self?.errorAuthenticatedTransaction(error: error)
            }
        }
    }

    private func createRechargeRequest(value: Double, password: String, biometry: Bool) -> RechargeRequest? {
        guard let cardId = cardSelected?.id else {
            return nil
        }

        let typeId = model.typeId.rawValue
        let amount = String(format: "%.4f", value)
        let cvv = getCvvDebitCard(id: cardId)

        return RechargeRequest(rechargeTypeId: typeId,
                               value: amount,
                               cardSelected: cardId,
                               biometry: biometry,
                               password: password,
                               cvv: cvv,
                               chargedValue: rechargeFeeInfo?.chargedValue,
                               serviceFee: rechargeFeeInfo?.serviceFee)
    }

    private func checkNeedCvv(value: Double) -> Bool {
        guard let cardBank = cardSelected else {
            return false
        }

        let isVirtualCard = cardBank.isOneShotCvv ?? false
        return isVirtualCard ? true : service.pciCvvIsEnable(cardBank: cardBank, cardValue: value)
    }

    private func openFlowInsertCvv(isEmergencyAid: Bool, value: Double, password: String, biometry: Bool) {
        guard let id = cardSelected?.id else {
            return
        }

        outputs?.didNextStep(action: .openCvv(isEmergencyAid: isEmergencyAid, id: id, completedCvv: { [weak self] cvv in
            self?.informedCvv = cvv
            self?.authenticatedRecharge(value: value, password: password, biometry: biometry)
        }, completedNoCvv: { [weak self] in
            let error = PicPayError(message: DefaultLocalizable.cvvNotInformed.text)
            self?.outputs?.didReceiveAnError(error: error)
        }))
    }

    private func getCvvDebitCard(id: String) -> String? {
        informedCvv ?? service.cvvCard(id: id)
    }

    private func saveCvv() {
        guard let id = cardSelected?.id,
            let cvv = informedCvv else { return }

        service.saveCvvCard(id: id, value: cvv)
    }

    private func makeRechargeSuccess(with recharge: Recharge) {
        if case .bill = model.typeId, isCashInRedesignAvailable {
            outputs?.didNextStep(action: .redesignBoletoSuccess(recharge))
        } else if case .original = model.typeId {
            outputs?.didNextStep(action: .rechargeOriginalSuccess)
        } else {
            outputs?.didNextStep(action: .rechargeSuccess)
        }
    }

    private func makeRechargeError(error: NSError) {
        guard model.typeId != .original else {
            errorOriginal(error: error)
            return
        }
        
        if let picpayError = error as? PicPayError {
            outputs?.didReceiveAnError(error: picpayError)
        } else {
            outputs?.didReceiveAnError(error: error)
        }

    }

    private func errorOriginal(error: NSError) {
        switch error.code {
        case WSConsumerMakeRechargeOpenBank.token.rawValue:
            outputs?.originalRequestToken(error: error)

        case WSConsumerMakeRechargeOpenBank.push.rawValue:
            outputs?.originalRequestPush(title: DefaultLocalizable.bankOriginal.text, message: error.localizedDescription)

        case 401:
            guard let authUrl = model.authUrl, let authHeader = model.authHeaders else {
                return
            }

            let completion: RechargeValueAction.OriginalCallback = { [weak self] error in
                guard let error = error else {
                    self?.model.linked = true
                    self?.outputs?.originalWebSuccess()
                    return
                }

                let picpayError = PicPayError(message: error.localizedDescription)
                self?.outputs?.didReceiveAnError(error: picpayError)
            }

            outputs?.didNextStep(action: .openOriginal(authUrl, authHeader, completion))

        default:
            outputs?.didReceiveAnError(error: error)
        }
    }

    private func errorAuthenticatedTransaction(error: PicPayError) {
        if Adyen3DS.is3ds(error: error) {
            guard let payload = Adyen3DS.adyenPayload(error: error) else {
                return
            }
            adyenPayload = payload
            create3DSTransaction()
        } else if isNotAuthorized3DS(error: error) {
            authenticatedRechargeError(error)
        } else {
            informedCvv = nil
            if hasSecurityRisk(error: error) {
                outputs?.didNextStep(action: .securityRisk(error: error))
            } else {
                outputs?.didReceiveAnError(error: error)
            }
        }
    }

    private func hasSecurityRisk(error: PicPayError) -> Bool {
        error.data != nil && model.typeId == .emergencyAid
    }

    private func authenticatedRechargeError(_ error: PicPayError, _ tryAgainCompletion: (() -> Void)? = nil) {
        let aliasCard = cardSelected?.alias ?? ""
        let description = RechargeValueErrorHelper.presentableMessage(error: error) ?? error.description
        informedCvv = nil
        guard isNotAuthorized3DS(error: error) else {
            let errorDisplayableMessage = RechargeValueErrorHelper.presentableMessage(error: error)
            outputs?.didNextStep(action: .rechargeAuthenticatedFailure(value: value, aliasCard: aliasCard, message: errorDisplayableMessage, tryAgainCompletion: tryAgainCompletion))
            return
        }

        outputs?.didNextStep(action: .rechargeAuthenticatedFaq(description: description, value: value, aliasCard: aliasCard))

        tracking3dsManager.analytics3DSError(error: error, cardBrand: analyticsCardInfo()?.cardBrand) { [weak self] in
            self?.tracking3dsManager.analyticsErrorChallenge(description: error.description, cardBrand: analyticsCardInfo()?.cardBrand)
        }
    }

    private func isNotAuthorized3DS(error: PicPayError) -> Bool {
        RechargeValueErrorHelper.faqCodeErros.contains(error.code)
    }

    private func authenticatedRechargeSuccess() {
        if case .emergencyAid = model.typeId {
            outputs?.didNextStep(action: .showEmergencyAidSuccess(showTutorial: !governmentOnboardingHasBeenShown))
            if !governmentOnboardingHasBeenShown {
                dependencies.kvStore.setFirstTimeOnlyEvent(governmentOnboardinguUserDefaultsKey)
            }
        } else {
            let aliasCard = cardSelected?.alias ?? ""
            saveCvv()
            outputs?.didNextStep(action: .rechargeAuthenticatedSuccess(value: value, aliasCard: aliasCard))
        }
    }

    private func create3DSTransaction() {
        if adyen.firstAccess3DS {
            outputs?.showAdyenWarning()
        } else {
            continueOn3DSTransaction()
        }
    }

    private func trackEvent(recharge: Recharge) {
        guard model.typeId != .debit else {
            Analytics.shared.log(RechargeEvent.recharge(.rechargeDebitSuccess))
            return
        }

        if recharge.rechargeType == .bill {
            Analytics.shared.log(RechargeEvent.rechargeValue(.bill, value: recharge.value))
        } else if recharge.rechargeType == .deposit {
            trackEventRechargeType(recharge: recharge)
        }
    }

    private func analyticsStartChallenge() {
        guard let info = analyticsCardInfo() else {
            return
        }

        Analytics.shared.log(RechargeEvent.start3DSChallenge(cardBrand: info.cardBrand, cardIssuer: info.cardIssuer))
    }

    func analyticsCardInfo() -> (cardBrand: String, cardIssuer: String)? {
        guard
            let card = cardSelected,
            let cardBrand = card.cardBrand,
            let cardIssuer = card.cardIssuer
            else {
                return nil
        }

        return (cardBrand: cardBrand, cardIssuer: cardIssuer)
    }

    private func trackEventRechargeType(recharge: Recharge) {
        var event: RechargeEvent.RechargeMethods?

        switch recharge.destinationBank {
        case .bancoDoBrasil:
            event = .bancoDoBrasil
        case .bradesco:
            event = .bradesco
        case .itau:
            event = .itau
        case .original:
            event = .original
        default:
            break
        }

        guard let type = event else {
            return
        }
        Analytics.shared.log(RechargeEvent.rechargeValue(type, value: recharge.value))
    }
}
