import Foundation

protocol RechargeValueViewModelInputs {
    func viewDidAppear()
    func headerInfo() -> (title: String, desc: String?)
    func displayCard() -> (title: String, image: String)
    func titleButton() -> String
    func shouldEnableButton(forValue value: Double) -> Bool
    func shouldFullScreenLoading() -> Bool
    
    func rechargeIsAuthenticated() -> Bool
    func didTapChangeCard()
    func makeRecharge(value: Double, token: String)
    func makeAuthenticatedRecharge(value: Double, password: String, biometry: Bool)
    func challengeFinishSuccess()
    func challengeFinishError(error: Error)
    func continueOn3DSTransaction()
    func showOriginalWarning()
    func checkoutAuthenticatedProcess(value: Double)
}

protocol RechargeValueViewModelOutputs: AnyObject, ADYChallengeDelegate {
    func didNextStep(action: RechargeValueAction)
    func didReceiveAnError(error: NSError)
    func didReceiveAnError(error: PicPayError)
    func originalRequestToken(error: NSError)
    func originalRequestPush(title: String, message: String)
    func originalWebSuccess()
    func showAdyenWarning()
    func showLoading()
    func displayPasswordPopup()
}

protocol RechargeValueViewModelType: AnyObject {
    var inputs: RechargeValueViewModelInputs { get }
    var outputs: RechargeValueViewModelOutputs? { get set }
}
