import Foundation
import AnalyticsModule

protocol RechargeValue3DSTrackingManaging {
    func analytics3DSError(error: PicPayError, cardBrand: String?, failedCompletion: () -> Void)
    func analyticsCancelledChallenge(cardBrand: String?, cardIssuer: String?)
    func analytics3DSResponse(type: RechargeEvent.ChallengeResponse, cardBrand: String?, cardIssuer: String?)
    func analyticsErrorChallenge(description: String, cardBrand: String?)
}

final class RechargeValue3DSTrackingManager: RechargeValue3DSTrackingManaging {
    func analytics3DSError(error: PicPayError, cardBrand: String?, failedCompletion: () -> Void) {
        guard
            let errorPayloadData = error.data?.dictionaryObject?.toData(),
            let cardBrand = cardBrand,
            let errorPayload = try? JSONDecoder().decode(AdyenErrorPayload.self, from: errorPayloadData)
            else {
                failedCompletion()
                return
        }
        
        let description = errorPayload.transactionStatus ?? errorPayload.refusalReason
        let event = RechargeEvent.error3DSTransaction(
            description: description ?? error.description,
            cardBrand: cardBrand,
            reason: errorPayload.transactionStatusReason ?? .unknown
        )
        Analytics.shared.log(event)
    }
    
    func analyticsCancelledChallenge(cardBrand: String?, cardIssuer: String?) {
        guard
            let brand = cardBrand,
            let issuer = cardIssuer
            else {
                return
        }
        
        Analytics.shared.log(RechargeEvent.cancelled3DSChallenge(cardBrand: brand, cardIssuer: issuer))
    }
    
    func analytics3DSResponse(type: RechargeEvent.ChallengeResponse, cardBrand: String?, cardIssuer: String?) {
        guard
            let brand = cardBrand,
            let issuer = cardIssuer
            else {
                return
        }
        
        Analytics.shared.log(RechargeEvent.response3DSChallenge(response: type, cardBrand: brand, cardIssuer: issuer))
    }
    
    func analyticsErrorChallenge(description: String, cardBrand: String?) {
        guard let brand = cardBrand else {
            return
        }
        
        Analytics.shared.log(RechargeEvent.error3DSChallenge(description: description, cardBrand: brand))
    }
}
