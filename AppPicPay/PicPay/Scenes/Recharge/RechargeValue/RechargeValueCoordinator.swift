import CashIn
import UI
import UIKit

enum RechargeValueAction {
    case rechargeAuthenticatedSuccess(value: Double, aliasCard: String)
    case rechargeAuthenticatedFailure(value: Double, aliasCard: String, message: String?, tryAgainCompletion: (() -> Void)?)
    case openCvv(isEmergencyAid: Bool, id: String, completedCvv: (String) -> Void, completedNoCvv: () -> Void)
    case rechargeAuthenticatedFaq(description: String, value: Double, aliasCard: String)
    case rechargeSuccess
    case rechargeOriginalSuccess
    case openOriginal(_ authUrl: String, _ authHeader: [String: String], _ completion: OriginalCallback)
    case changeCard(onlyVirtualCards: Bool)
    case securityRisk(error: PicPayError)
    case close
    case showRechargeDebitFee(value: Double, onContinuePayment: (RechargeFeeInfo) -> Void)
    case showEmergencyAidSuccess(showTutorial: Bool)
    case redesignBoletoSuccess(_ recharge: Recharge)

    typealias OriginalCallback = (PicPayError?) -> Void
}

protocol RechargeValueCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: RechargeValueAction)
}

final class RechargeValueCoordinator: RechargeValueCoordinating {
    private var coordinator: Coordinating?
    weak var viewController: UIViewController?
    var onContinuePayment: ((RechargeFeeInfo) -> Void)?

    func perform(action: RechargeValueAction) {
        switch action {
        case .changeCard(let onlyVirtualCards):
            openChangeCard(onlyVirtualCards: onlyVirtualCards)

        case let .openCvv(isEmergencyAid, id, withCvv, withoutCVV):
            openCvv(isEmergencyAid: isEmergencyAid, id: id, completedCvv: withCvv, completedNoCvv: withoutCVV)

        case .rechargeSuccess:
            openRechargeSuccess()

        case .rechargeOriginalSuccess:
            viewController?.navigationController?.dismiss(animated: true)

        case let .rechargeAuthenticatedSuccess(value, aliasCard):
            openRechargeAuthenticatedSuccess(value: value, aliasCard: aliasCard)

        case let .rechargeAuthenticatedFailure(value, aliasCard, message, tryAgainCompletion):
            openRechargeAuthenticatedFailure(value: value, aliasCard: aliasCard, message: message, tryAgainCompletion: tryAgainCompletion)

        case let .rechargeAuthenticatedFaq(description, value, aliasCard):
            openRechargeAuthenticatedFaq(description: description, value: value, aliasCard: aliasCard)

        case .close:
            viewController?.navigationController?.dismiss(animated: true)

        case let .openOriginal (authUrl, authHeader, completion):
            openOriginal(authUrl: authUrl, authHeader: authHeader, completion: completion)

        case .securityRisk(let error):
            openSecurityRisk(error: error)

        case let .showRechargeDebitFee(value, completion):
            openRechargeTransferSummaryView(value: value, completion: completion)
            
        case let .showEmergencyAidSuccess(showTutorial):
            openEmergencyAidSuccess(showTutorial: showTutorial)
            
        case let .redesignBoletoSuccess(recharge):
            openBoletoDetails(with: recharge)
        }
    }

    private func openRechargeTransferSummaryView(value: Double, completion: @escaping (RechargeFeeInfo) -> Void) {
        onContinuePayment = completion
        let controller = RechargeTransferSummaryFactory.make(rechargeValue: value, rechargeCoordinator: self)
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }

    private func openCvv(
        isEmergencyAid: Bool,
        id: String,
        completedCvv: @escaping (String) -> Void,
        completedNoCvv: @escaping () -> Void
    ) {
        guard let navigation = viewController?.navigationController else {
            return
        }

        let coordinator = CvvRegisterFlowCoordinator(
            isEmergencyAid: isEmergencyAid,
            cardType: .id(id),
            navigationController: navigation,
            paymentType: .recharge,
            finishedCvv: completedCvv,
            finishedWithoutCvv: completedNoCvv
        )
        coordinator.start()

        self.coordinator = coordinator
    }

    private func openChangeCard(onlyVirtualCards: Bool) {
        let controller = CardListFactory.make(
            typeFilter: [.debit, .creditDebit],
            balanceEnable: false,
            onlyVirtualCards: onlyVirtualCards,
            analytics: CardListDebitEvent()
        )
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }

    private func openRechargeSuccess() {
        let controller = RechargeLoadFactory.make()
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func openRechargeAuthenticatedSuccess(value: Double, aliasCard: String) {
        let model = RechargeStatus(type: .success, value: value, aliasCard: aliasCard)
        let controller = RechargeStatusFactory.make(model: model)
        viewController?.navigationController?.pushViewController(controller, animated: false)
    }

    private func openEmergencyAidSuccess(showTutorial: Bool) {
        TaskCompleteAnimation.showSuccess(onView: viewController?.navigationController?.view) { [weak self] in
            if showTutorial {
                self?.showEmergencyAidTutorial()
            } else {
                self?.perform(action: .close)
            }
        }
    }
    
    private func showEmergencyAidTutorial() {
        let controller = OnboardingFactory.make(for: .emergencyAid)
        viewController?.navigationController?.pushViewController(controller, animated: false)
    }

    private func openRechargeAuthenticatedFailure(value: Double, aliasCard: String, message: String?, tryAgainCompletion: (() -> Void)?) {
        let type: RechargeStatus.StatusType

        if let message = message {
            type = .failureWithMessage(message, tryAgain: tryAgainCompletion)
        } else {
            type = .failure(tryAgainCompletion)
        }

        let model = RechargeStatus(type: type, value: value, aliasCard: aliasCard)
        let controller = RechargeStatusFactory.make(model: model)
        viewController?.navigationController?.pushViewController(controller, animated: false)
    }

    private func openRechargeAuthenticatedFaq(description: String, value: Double, aliasCard: String) {
        let model = RechargeStatus(type: .faq(description), value: value, aliasCard: aliasCard)
        let controller = RechargeStatusFactory.make(model: model)
        viewController?.navigationController?.pushViewController(controller, animated: false)
    }

    private func openOriginal(authUrl: String, authHeader: [String: String], completion: @escaping RechargeValueAction.OriginalCallback) {
        guard let vc = viewController else {
            return
        }

        ViewsManager.pushWebViewController(withUrl: authUrl, sendHeaders: false, customHeader: authHeader, fromViewController: vc, title: DefaultLocalizable.bankOriginal.text) { error in
            let message = error?.localizedDescription ?? DefaultLocalizable.unexpectedError.text
            let picpayError = PicPayError(message: message)
            completion(picpayError)
        }
    }

    private func openSecurityRisk(error: PicPayError) {
        let cashInError = CashInErrorAdapter.transform(error: error)
        let controller = CashInErrorFactory.make(for: cashInError)
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func openBoletoDetails(with recharge: Recharge) {
        guard let navigationController = viewController?.navigationController,
              let rootViewController = navigationController.viewControllers.first else { return }
        
        let controller = RechargeBillFactory.make(recharge: recharge)
        navigationController.setViewControllers(
            [rootViewController, controller],
            animated: true
        )
    }
}

extension RechargeValueCoordinator: RechargeValueCompletionCoordinating {
    func didCompletePayment(with info: RechargeFeeInfo) {
        onContinuePayment?(info)
    }
}
