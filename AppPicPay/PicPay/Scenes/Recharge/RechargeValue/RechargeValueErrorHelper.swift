import Foundation

enum RechargeErrorWithMessage: Int {
    case notAuthorized = 7_666
    case invalidFormatOrValue = 203
    case invalidValue = 201
}

enum RechargeValueErrorHelper {
    static let faqCodeErros = [
        RechargeErrorWithMessage.invalidValue.rawValue,
        RechargeErrorWithMessage.notAuthorized.rawValue
    ]
    
    static func presentableMessage(error: PicPayError) -> String? {
        switch Int(error.picpayCode) {
        case RechargeErrorWithMessage.invalidFormatOrValue.rawValue:
            return RechargeLocalizable.invalidCodeMessage.text
        case RechargeErrorWithMessage.invalidValue.rawValue:
            return RechargeLocalizable.emitterErrorMessage.text
        default:
            return nil
        }
    }
}
    
