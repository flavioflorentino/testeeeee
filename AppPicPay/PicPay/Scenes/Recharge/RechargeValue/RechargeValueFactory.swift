import FeatureFlag
import Foundation

enum RechargeValueFactory {
    static func make(model: RechargeMethod) -> RechargeValueViewController {
        let container = DependencyContainer()
        
        var paymentService: RechargePaymentServicing = RechargePaymentService()
        if FeatureManager.isActive(.pciRecharge) {
            paymentService = RechargePaymentPci()
        }
        
        let service: RechargeValueServicing = RechargeValueService(dependencies: container)
        let viewModel: RechargeValueViewModelType = RechargeValueViewModel(
            model: model,
            service: service,
            paymentService: paymentService,
            dependencies: container
        )
        var coordinator: RechargeValueCoordinating = RechargeValueCoordinator()
        let viewController = RechargeValueViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController

        return viewController
    }
}
