import Foundation

protocol RechargePaymentServicing {
    func authenticatedRecharge(request: RechargeRequest, completion: @escaping (Result<Void, PicPayError>) -> Void)
}

final class RechargePaymentService: RechargePaymentServicing {
    func authenticatedRecharge(request: RechargeRequest, completion: @escaping (Result<Void, PicPayError>) -> Void) {
        guard let parameters = request.toDictionary() else {
            return
        }
        
        var header = [request.password: "password"]
        if let cvv = request.cvv {
            header[cvv] = "cvv"
        }
        
        WebServiceInterface.dictionary(withPostVarsSwift: parameters, serviceMethodUrl: kWsUrlRechargeByDebit, andHeaders: header) { [weak self] _, _, _, error in
            DispatchQueue.main.async {
                guard let error = error else {
                    completion(.success(()))
                    return
                }
                
                self?.authenticatedRechargeError(error: error, completion: completion)
            }
        }
    }
    
    private func authenticatedRechargeError(error: Error?, completion: @escaping (Result<Void, PicPayError>) -> Void) {
        guard let error = error as NSError?, let picpayError = error as? PicPayError else {
            let picpayError = PicPayError(message: DefaultLocalizable.unexpectedError.text)
            completion(.failure(picpayError))
            return
        }
        
        completion(.failure(picpayError))
    }
}
