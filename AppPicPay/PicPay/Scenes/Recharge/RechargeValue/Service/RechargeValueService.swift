import Core
import FeatureFlag
import Foundation

protocol RechargeValueServicing {
    var isDepositDebitFeeEnabled: Bool { get }
    func pciCvvIsEnable(cardBank: CardBank, cardValue: Double) -> Bool
    func saveCvvCard(id: String, value: String?)
    func cvvCard(id: String) -> String?
    func getDebitCardSelected() -> CardBank?
    func getCardsBank() -> [CardBank]
    func makeRecharge(method: RechargeMethod, value: NSDecimalNumber, security: String, completion: @escaping (Result<Recharge, NSError>) -> Void)
}

final class RechargeValueService: BaseApi, RechargeValueServicing {
    typealias Dependencies = HasFeatureManager & HasKeychainManager
    private let dependencies: Dependencies
    private let cardManager = CreditCardManager.shared
    
    var isDepositDebitFeeEnabled: Bool {
        dependencies.featureManager.isActive(.featureDepositDebitCardChargeBool)
    }
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func getCardsBank() -> [CardBank] {
        cardManager.cardList
    }
    
    func pciCvvIsEnable(cardBank: CardBank, cardValue: Double) -> Bool {
        CvvRegisterFlowCoordinator.cvvFlowEnable(cardBank: cardBank, cardValue: cardValue, paymentType: .recharge)
    }
    
    func saveCvvCard(id: String, value: String?) {
        dependencies.keychain.set(key: KeychainKeyPF.cvv(id), value: value)
    }
    
    func cvvCard(id: String) -> String? {
        dependencies.keychain.getData(key: KeychainKeyPF.cvv(id))
    }
    
    func getDebitCardSelected() -> CardBank? {
        cardManager.defaultDebitCard()
    }
    
    func makeRecharge(method: RechargeMethod, value: NSDecimalNumber, security: String, completion: @escaping (Result<Recharge, NSError>) -> Void) {
        WSConsumer.makeRecharge(method, value: value, securityRequest: security) { [weak self] recharge, error in
            DispatchQueue.main.async {
                guard let recharge = recharge else {
                    self?.makeRechargeError(error: error as NSError?, completion: completion)
                    return
                }
                completion(.success(recharge))
            }
        }
    }
    
    private func makeRechargeError(error: NSError?, completion: @escaping (Result<Recharge, NSError>) -> Void) {
        guard let picpayError = error else {
            let picpayError = PicPayError(message: DefaultLocalizable.unexpectedError.text)
            completion(.failure(picpayError))
            return
        }
        
        completion(.failure(picpayError))
    }
}
