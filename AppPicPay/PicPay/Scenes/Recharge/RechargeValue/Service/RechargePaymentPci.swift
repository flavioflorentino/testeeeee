import Foundation
import PCI

final class RechargePaymentPci: RechargePaymentServicing {
    private let pci: RechargeServiceProtocol
    
    init(pci: RechargeServiceProtocol = RechargeService()) {
        self.pci = pci
    }
    
    func authenticatedRecharge(request: RechargeRequest, completion: @escaping (Result<Void, PicPayError>) -> Void) {
        guard let rechargePayload = createRechargePayload(request: request) else {
            let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
            completion(.failure(error))
            return
        }
        let cvv = createCVVPayload(cvv: request.cvv)
        let payload = PaymentPayload<RechargePayload>(cvv: cvv, generic: rechargePayload)
        
        pci.createTransaction(
            password: request.password,
            payload: payload,
            isNewArchitecture: false
        ) { result in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    completion(.success(()))

                case .failure(let error):
                    completion(.failure(error.picpayError))
                }
            }
        }
    }
    
    private func createRechargePayload(request: RechargeRequest) -> RechargePayload? {
        guard let data = JSONEncoder.data(request) else {
            return nil
        }
        
        return try? JSONDecoder.decode(data, to: RechargePayload.self)
    }
    
    private func createCVVPayload(cvv: String?) -> CVVPayload? {
        guard let cvv = cvv else {
            return nil
        }
        
        return CVVPayload(value: cvv)
    }
}
