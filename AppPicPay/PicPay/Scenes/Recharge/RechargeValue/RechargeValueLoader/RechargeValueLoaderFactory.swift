import UIKit

enum RechargeValueLoaderFactory {
    static func make(methodId: Int) -> RechargeValueLoaderViewController {
        let container = DependencyContainer()
        let service = RechargeValueLoaderService(dependencies: container)
        let coordinator = RechargeValueLoaderCoordinator()
        let presenter = RechargeValueLoaderPresenter(coordinator: coordinator)
        let interactor = RechargeValueLoaderInteractor(methodId: methodId, service: service, presenter: presenter)
        let viewController = RechargeValueLoaderViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
