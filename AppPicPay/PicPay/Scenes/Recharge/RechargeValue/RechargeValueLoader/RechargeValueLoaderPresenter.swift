import Core
import Foundation
import UI

protocol RechargeValueLoaderPresenting: AnyObject {
    var viewController: RechargeValueLoaderDisplaying? { get set }

    func didNextStep(action: RechargeValueLoaderAction)
    func showLoading()
    func showError(error: ApiError)
}

final class RechargeValueLoaderPresenter {
    private let coordinator: RechargeValueLoaderCoordinating
    weak var viewController: RechargeValueLoaderDisplaying?

    init(coordinator: RechargeValueLoaderCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - RechargeValueLoaderPresenting
extension RechargeValueLoaderPresenter: RechargeValueLoaderPresenting {
    func showLoading() {
        viewController?.showLoading()
    }
    
    func showError(error: ApiError) {
        let viewModel = StatefulErrorViewModel(
            image: Assets.Recharge.iluWireTransferReceiptErrorClock.image,
            content: (title: error.picpayError.title,
                      description: error.picpayError.errorDescription),
            button: (image: nil, title: Strings.RechargeWireTransfer.receiptLoadingErrorAction)
        )
        viewController?.showError(viewModel)
    }
    
    func didNextStep(action: RechargeValueLoaderAction) {
        coordinator.perform(action: action)
    }
}
