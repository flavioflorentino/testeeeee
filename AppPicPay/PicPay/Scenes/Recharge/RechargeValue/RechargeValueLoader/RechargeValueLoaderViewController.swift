import UI
import UIKit

protocol RechargeValueLoaderDisplaying: AnyObject {
    func showLoading()
    func showError(_ error: StatefulErrorViewModel)
}

private extension RechargeValueLoaderViewController.Layout { }

final class RechargeValueLoaderViewController: ViewController<RechargeValueLoaderInteracting, UIView> {
    fileprivate enum Layout { }

    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.loadDetails()
    }

    override func buildViewHierarchy() { }
    
    override func setupConstraints() { }

    override func configureViews() {
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
}

// MARK: - RechargeValueLoaderDisplaying
extension RechargeValueLoaderViewController: RechargeValueLoaderDisplaying {
    func showLoading() {
        beginState()
    }
    
    func showError(_ error: StatefulErrorViewModel) {
        endState(model: error)
    }
}

extension RechargeValueLoaderViewController: StatefulTransitionViewing {
    func didTryAgain() {
        interactor.loadDetails()
    }
}
