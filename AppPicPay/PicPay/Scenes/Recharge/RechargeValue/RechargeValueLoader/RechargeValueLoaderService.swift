import Core
import Foundation

protocol RechargeValueLoaderServicing {
    func fetchAddValueDetails(methodId: Int, completion: @escaping (Result<RechargeDetail, ApiError>) -> Void)
}

final class RechargeValueLoaderService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - RechargeValueLoaderServicing
extension RechargeValueLoaderService: RechargeValueLoaderServicing {
    func fetchAddValueDetails(methodId: Int, completion: @escaping (Result<RechargeDetail, ApiError>) -> Void) {
        let endpoint = RechargeValueLoaderEndpoint.getAddValueDetails(methodId: methodId)
        let api = Api<RechargeDetail>(endpoint: endpoint)
        api.execute { [weak self] response in
            self?.dependencies.mainQueue.async {
                completion(response.map(\.model))
            }
        }
    }
}

enum RechargeValueLoaderEndpoint {
    case getAddValueDetails(methodId: Int)
}

extension RechargeValueLoaderEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case let .getAddValueDetails(id):
            return "api/getDetailsRechargeMethod/\(id)"
        }
    }
}
