import Foundation

protocol RechargeValueLoaderInteracting: AnyObject {
    func loadDetails()
}

final class RechargeValueLoaderInteractor {
    private let methodId: Int
    private let service: RechargeValueLoaderServicing
    private let presenter: RechargeValueLoaderPresenting

    init(methodId: Int, service: RechargeValueLoaderServicing, presenter: RechargeValueLoaderPresenting) {
        self.methodId = methodId
        self.service = service
        self.presenter = presenter
    }
}

// MARK: - RechargeValueLoaderInteracting
extension RechargeValueLoaderInteractor: RechargeValueLoaderInteracting {
    func loadDetails() {
        presenter.showLoading()
        service.fetchAddValueDetails(methodId: methodId) { [weak self] result in
            switch result {
            case let .success(detail):
                self?.handle(detail)
            
            case let .failure(error):
                self?.presenter.showError(error: error)
            }
        }
    }
}

private extension RechargeValueLoaderInteractor {
    func handle(_ detail: RechargeDetail) {
        if let boleto = detail.boleto {
            let recharge = makeRecharge(with: boleto)
            presenter.didNextStep(action: .activeRecharge(recharge: recharge))
        } else {
            startAddValueFlow()
        }
    }
    
    func startAddValueFlow() {
        guard let rechargeType = RechargeMethod.RechargeType(rawValue: methodId) else { return }
        let rechargeMethod = RechargeMethod(rechargeType: rechargeType, name: "", description: "", bankId: "")
        presenter.didNextStep(action: .addValue(method: rechargeMethod))
    }
    
    func makeRecharge(with boleto: BoletoDetail) -> Recharge {
        Recharge(rechargeId: boleto.id,
                 value: boleto.value,
                 rechargeType: .bill,
                 destinationBank: .bancoDoBrasil,
                 instructions: RechargeInstructions(text: boleto.instructions, billValidity: boleto.dueDate),
                 billCode: boleto.code)
    }
}
