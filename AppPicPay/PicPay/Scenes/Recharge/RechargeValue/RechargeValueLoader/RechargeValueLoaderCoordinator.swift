import UIKit

enum RechargeValueLoaderAction: Equatable {
    case addValue(method: RechargeMethod)
    case activeRecharge(recharge: Recharge)
}

protocol RechargeValueLoaderCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RechargeValueLoaderAction)
}

final class RechargeValueLoaderCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - RechargeValueLoaderCoordinating
extension RechargeValueLoaderCoordinator: RechargeValueLoaderCoordinating {
    func perform(action: RechargeValueLoaderAction) {
        guard let currentController = viewController,
              let navigationController = currentController.navigationController else { return }
        
        switch action {
        case let .addValue(method):
            let rechargeValueController = RechargeValueFactory.make(model: method)
            navigationController.replace(currentController, by: rechargeValueController, animated: true)
            
        case let .activeRecharge(recharge):
            guard let rootViewController = navigationController.viewControllers.first else { return }
            let rechargeBillController = RechargeBillFactory.make(recharge: recharge)
            navigationController.setViewControllers(
                [rootViewController, rechargeBillController],
                animated: true
            )
        }
    }
}
