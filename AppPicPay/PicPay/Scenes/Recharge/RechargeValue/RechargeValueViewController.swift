import CashIn
import UI
import UIKit

final class RechargeValueViewController: LegacyViewController<RechargeValueViewModelType, RechargeValueCoordinating, UIView>, LoadingViewOverlayNavigationProtocol {
    private lazy var footerView: FoooterAddMoney = {
        let footer = FoooterAddMoney()
        footer.delegate = self
        footer.translatesAutoresizingMaskIntoConstraints = false
        
        return footer
    }()
    
    private lazy var headerView: HeaderAddMoney = {
        let header = HeaderAddMoney()
        header.translatesAutoresizingMaskIntoConstraints = false
        
        return header
    }()
    
    private lazy var moneyView: ValueMoney = {
        let money = ValueMoney()
        money.delegate = self
        money.translatesAutoresizingMaskIntoConstraints = false
        
        return money
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var loadingView: LoadingView = {
        let loadingView = LoadingView()
        loadingView.backgroundColor = Palette.black.color.withAlphaComponent(0.8)
        loadingView.text = DefaultLocalizable.completingTransaction.text
        loadingView.setTextColor(Palette.white.color)
        
        return loadingView
    }()

    private var bottomFooter: NSLayoutConstraint?
    private var auth: PPAuth?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureHeader()
        addObservers()
    }
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureFooterView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.inputs.viewDidAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopLoad()
    }
    
    override func buildViewHierarchy() {    
        containerView.addSubview(moneyView)
        view.addSubview(footerView)
        view.addSubview(headerView)
        view.addSubview(containerView)
    }
    
    override func configureViews() {
        title = RechargeLocalizable.addMoney.text
        view.backgroundColor = Palette.ppColorGrayscale000.color
        footerView.titleButton = viewModel.inputs.titleButton()
        footerView.buttonIsEnabled = false
        setUpNavigationBarDefaultAppearance()
    }
    
    override func setupConstraints() {
        bottomFooter = footerView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: view.topAnchor),
            headerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            headerView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            bottomFooter ?? footerView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            footerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            footerView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: headerView.bottomAnchor),
            containerView.bottomAnchor.constraint(equalTo: footerView.topAnchor),
            containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            moneyView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            moneyView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            moneyView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor)
        ])
    }
    
    private func configureFooterView() {
        guard viewModel.inputs.rechargeIsAuthenticated() else {
            footerView.toolBarIsHidden = true
            return
        }
        let result = viewModel.inputs.displayCard()
        footerView.setupCard(title: result.title, image: result.image)
        footerView.toolBarIsHidden = false
    }
    
    private func configureHeader() {
        let header = viewModel.inputs.headerInfo()
        headerView.configureView(title: header.title, description: header.desc)
    }
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc
    private func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue,
            let offset = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
                return
        }
        
        if keyboardSize.height == offset.height {
            bottomFooter?.constant = -keyboardSize.height
        } else {
            bottomFooter?.constant = -offset.height
        }
        view.layoutIfNeeded()
    }
    
    @objc
    private func keyboardWillHide(notification: NSNotification) {
        bottomFooter?.constant = 0
        view.layoutIfNeeded()
    }
    
    private func startLoad() {
        viewModel.inputs.shouldFullScreenLoading() ? startLoadingView() : footerView.startLoad()
        moneyView.valueFieldEnabled = false
    }
    
    private func stopLoad() {
        viewModel.inputs.shouldFullScreenLoading() ? stopLoadingView() : footerView.stopLoad()
        moneyView.valueFieldEnabled = true
    }
}

// MARK: View Model Outputs
extension RechargeValueViewController: RechargeValueViewModelOutputs {
    func displayPasswordPopup() {
        auth = PPAuth.authenticate({ [weak self] password, biometry in
            guard let value = self?.moneyView.value,
                let password = password else {
                    return
            }
            
            self?.startLoad()
            self?.viewModel.inputs.makeAuthenticatedRecharge(value: value, password: password, biometry: biometry)
        }, canceledByUserBlock: nil)
    }
    
    func showLoading() {
        startLoad()
    }
    
    func didNextStep(action: RechargeValueAction) {
        coordinator.perform(action: action)
    }
    
    func didReceiveAnError(error: NSError) {
        stopLoad()
        AlertMessage.showCustomAlertWithErrorUI(error, controller: self)
    }
    
    func didReceiveAnError(error: PicPayError) {
        stopLoad()
        coordinator.perform(action: .securityRisk(error: error))
    }
    
    func showAdyenWarning() {
        HapticFeedback.notificationFeedbackError()
        AdyenWarning().show(in: self) { [weak self] in
            self?.viewModel.inputs.continueOn3DSTransaction()
        }
    }
    
    func originalWebSuccess() {
        checkoutProcess()
    }
    
    func originalRequestPush(title: String, message: String) {
        stopLoad()
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let button = UIAlertAction(title: DefaultLocalizable.btOk.text, style: .default, handler: { [weak self] _ in
            self?.viewModel.inputs.showOriginalWarning()
        })
        
        alert.addAction(button)
        present(alert, animated: true)
    }
    
    func originalRequestToken(error: NSError) {
        stopLoad()
        
        let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
        alert.addTextField(configurationHandler: { textfield in
            textfield.keyboardType = .numberPad
        })
        
        alert.addAction(UIAlertAction(title: DefaultLocalizable.btOk.text, style: .default, handler: { [weak self] _ in
            if let token = alert.textFields?.first?.text {
                self?.checkoutProcess(token: token)
            }
        }))
        
        alert.addAction(UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil))
        
        present(alert, animated: true)
    }
}

extension RechargeValueViewController: FoooterAddMoneyDelegate {
    func didTapChangeCard() {
        moneyView.resignValueResponder()
        viewModel.inputs.didTapChangeCard()
    }
    
    func didTapButton() {
        moneyView.resignValueResponder()
        guard viewModel.inputs.rechargeIsAuthenticated() else {
            checkoutProcess()
            return
        }
        viewModel.inputs.checkoutAuthenticatedProcess(value: moneyView.value)
    }
    
    private func checkoutProcess(token: String = "") {
        let value = moneyView.value
        startLoad()
        viewModel.inputs.makeRecharge(value: value, token: token)
    }
}

extension RechargeValueViewController: ValueMoneyDelegate {
    func onTextChange(value: Double) {
        footerView.buttonIsEnabled = viewModel.inputs.shouldEnableButton(forValue: value)
    }
}

extension RechargeValueViewController: ADYChallengeDelegate {
    func challengeDidFinish(with result: ADYChallengeResult) {
        viewModel.inputs.challengeFinishSuccess()
    }
    
    func challengeDidFailWithError(_ error: Error) {
        stopLoad()
        viewModel.inputs.challengeFinishError(error: error)
    }
}
