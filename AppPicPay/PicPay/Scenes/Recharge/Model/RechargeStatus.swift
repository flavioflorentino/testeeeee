struct RechargeStatus {
    let type: StatusType
    let value: Double
    let aliasCard: String
}

extension RechargeStatus {
    enum StatusType {
        case success
        case faq(String)
        case failure(_ tryAgain: (() -> Void)? = nil)
        case failureWithMessage(String, tryAgain: (() -> Void)? = nil)
    }
}
