import Foundation

// swiftlint:disable discouraged_optional_boolean
// swiftlint:disable discouraged_optional_collection
@objcMembers
final class RechargeMethod: NSObject, Decodable {
    let typeId: RechargeType
    let name: String
    let title: String?
    let desc: String
    let imgUrl: String?
    let bankId: String?
    let registerDebit: String?
    let options: [RechargeMethod]?
    let authHeaders: [String: String]?
    let optionSelectorTitle: String?
    let authUrl: String?
    var linked: Bool?
    let value: String?
    let serviceCharge: Decimal?
    let deeplinkString: String?
    
    // PicPayAccount attributes
    let accountAgency: String?
    let accountNumber: String?
    let consumerAvatar: String?
    let consumerCpf: String?
    let consumerName: String?
    let consumerUsername: String?
    
    class func create(from array: NSArray) -> RechargeMethod? {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .customISO8601
        decoder.keyDecodingStrategy = .useDefaultKeys
        
        guard let data = try? JSONSerialization.data(withJSONObject: array, options: .prettyPrinted) else {
            return nil
        }
        
        return try? decoder.decodeNestedData(RechargeMethod.self, from: data)
    }
    
    init(rechargeType: RechargeType,
         name: String,
         description: String,
         title: String? = nil,
         imgUrl: String? = nil,
         bankId: String? = nil,
         registerDebit: String? = nil,
         options: [RechargeMethod]? = nil,
         authHeaders: [String: String]? = nil,
         optionSelectorTitle: String? = nil,
         authUrl: String? = nil,
         linked: Bool? = nil,
         value: String? = nil,
         serviceCharge: Decimal? = nil,
         deeplinkString: String? = nil,
         accountAgency: String? = nil,
         accountNumber: String? = nil,
         consumerAvatar: String? = nil,
         consumerCpf: String? = nil,
         consumerName: String? = nil,
         consumerUsername: String? = nil) {
        self.typeId = rechargeType
        self.name = name
        self.desc = description
        
        self.title = title
        self.imgUrl = imgUrl
        self.bankId = bankId
        self.registerDebit = registerDebit
        self.options = options
        self.authHeaders = authHeaders
        self.optionSelectorTitle = optionSelectorTitle
        self.authUrl = authUrl
        self.linked = linked
        self.value = value
        self.serviceCharge = serviceCharge
        self.deeplinkString = deeplinkString
        self.accountAgency = accountAgency
        self.accountNumber = accountNumber
        self.consumerAvatar = consumerAvatar
        self.consumerCpf = consumerCpf
        self.consumerName = consumerName
        self.consumerUsername = consumerUsername
    }
}

extension RechargeMethod {
    static func == (lhs: RechargeMethod, rhs: RechargeMethod) -> Bool {
        lhs.typeId == rhs.typeId
    }
}

extension RechargeMethod {
    private enum CodingKeys: String, CodingKey {
        case typeId = "recharge_type_id"
        case name = "recharge_type_name"
        case title = "recharge_type_title"
        case desc = "recharge_type_description"
        case imgUrl = "recharge_type_image"
        case bankId = "bank_id"
        case registerDebit = "recharge_type_description_no_debit_card"
        case options = "options"
        case optionSelectorTitle = "selector"
        case authUrl = "auth_url"
        case linked = "linked"
        case authHeaders = "auth_headers"
        case value = "recharge_type_value"
        case serviceCharge = "service_charge"
        case deeplinkString = "deeplink"
        
        case accountAgency = "account_agency"
        case accountNumber = "account_number"
        case consumerAvatar = "consumer_avatar"
        case consumerCpf = "consumer_cpf"
        case consumerName = "consumer_name"
        case consumerUsername = "consumer_username"
    }
    
    @objc
    enum RechargeType: Int, Codable {
        case bill = 1
        case bank = 2
        case original = 4
        case debit = 5
        case loan = 6
        case lending = 7
        case emergencyAid = 8
        case picpayAccount = 9
        case deeplink = 10
    }
}
// swiftlint:enable discouraged_optional_boolean
// swiftlint:enable discouraged_optional_collection
