import Foundation

@objcMembers
final class RechargeInstructions: Codable {
    private enum CodingKeys: String, CodingKey {
        case cnpj = "cnpj"
        case identifier = "identificador"
        case account = "conta"
        case holder = "titular"
        case agency = "agencia"
        case billValidity = "validade"
        case text = "texto"
    }
    
    let cnpj: String?
    let identifier: String?
    let account: String?
    let holder: String?
    let agency: String?
    let billValidity: String?
    let text: String?
    
    init(text: String?, billValidity: String?) {
        self.text = text
        self.billValidity = billValidity
        
        self.cnpj = nil
        self.identifier = nil
        self.account = nil
        self.holder = nil
        self.agency = nil
    }
}
