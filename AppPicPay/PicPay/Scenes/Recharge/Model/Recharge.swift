import Foundation

@objcMembers
final class Recharge: NSObject, Codable {
    private enum CodingKeys: String, CodingKey {
        case rechargeId = "id"
        case value = "value"
        case statusId = "status_id"
        case rechargeType = "recharge_method_type_id"
        case destinationBank = "banco_id"
        case bankName = "bank_name"
        case bankImgUrl = "bank_img"
        case billUrl = "url"
        case billCode = "code"
        case rechargeDepositProofId = "recharge_deposit_proof_id"
        case instructions = "instructions"
    }
    
    let rechargeId: String
    let value: String
    let statusId: String
    let rechargeType: RechargeType
    let destinationBank: DestinationBank
    let bankName: String?
    let bankImgUrl: String?
    let billUrl: String?
    let billCode: String?
    let rechargeDepositProofId: Int?
    let instructions: RechargeInstructions
    
    init(rechargeId: String,
         value: String,
         statusId: String = "",
         rechargeType: RechargeType,
         destinationBank: DestinationBank,
         instructions: RechargeInstructions,
         bankName: String? = nil,
         bankImgUrl: String? = nil,
         billUrl: String? = nil,
         billCode: String? = nil,
         rechargeDepositProofId: Int? = nil) {
        self.rechargeId = rechargeId
        self.value = value
        self.statusId = statusId
        self.rechargeType = rechargeType
        self.destinationBank = destinationBank
        self.bankName = bankName
        self.bankImgUrl = bankImgUrl
        self.billUrl = billUrl
        self.billCode = billCode
        self.rechargeDepositProofId = rechargeDepositProofId
        self.instructions = instructions
    }
    
    class func create(from dictionary: NSDictionary) -> Recharge? {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .customISO8601
        decoder.keyDecodingStrategy = .useDefaultKeys
        
        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            return nil
        }
        
        return try? decoder.decodeNestedData(Recharge.self, from: data)
    }
    
    func isProofUploaded() -> Bool {
        rechargeDepositProofId != nil
    }
}

extension Recharge {
    enum RechargeType: String, Codable {
        case bill = "1"
        case deposit = "2"
    }
    
    enum DestinationBank: String, Codable {
        case bancoDoBrasil = "001"
        case bradesco = "237"
        case itau = "341"
        case original = "212"
        case tedDoc = "999"
    }
}
