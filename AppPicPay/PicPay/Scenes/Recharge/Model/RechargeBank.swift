import Foundation

struct RechargeBank {
    let bankName: String
    let bankImage: String
    let value: String
    let instructions: RechargeInstructions
}
