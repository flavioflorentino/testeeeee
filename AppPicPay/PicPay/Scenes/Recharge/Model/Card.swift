import Foundation

struct RechargeCard {
    let title: String
    let description: String
    let image: String
    let status: Status
}

extension RechargeCard {
    enum Status {
        case loading
        case selected
        case none
    }
}
