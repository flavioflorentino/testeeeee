import Foundation

struct FieldRechargeRow {
    let title: String
    let description: String
    let copyField: Bool
    let highlight: Bool
    
    init?(title: String, description: String?, copyField: Bool = false, highlight: Bool = false) {
        guard let desc = description else {
            return nil
        }
        
        self.title = title
        self.description = desc
        self.copyField = copyField
        self.highlight = highlight
    }
}
