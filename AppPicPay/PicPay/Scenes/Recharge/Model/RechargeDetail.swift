import Foundation

struct RechargeDetail: Decodable, Equatable {
    let boleto: BoletoDetail?
    let options: [RechargeMethod]
    let bankId: String?
    let rechargeDescription: String?
    
    private enum CodingKeys: String, CodingKey {
        case options, bankId
        case boleto = "recharge"
        case rechargeDescription = "recharge_type_description_no_debit_card"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        boleto = try container.decodeIfPresent(BoletoDetail.self, forKey: .boleto)
        options = try container.decodeIfPresent([RechargeMethod].self, forKey: .options) ?? []
        bankId = try container.decodeIfPresent(String.self, forKey: .bankId)
        rechargeDescription = try container.decodeIfPresent(String.self, forKey: .rechargeDescription)
    }
}

struct BoletoDetail: Decodable, Equatable {
    let id: String
    let value: String
    let url: String
    let code: String
    let instructions: String
    let dueDate: String
}
