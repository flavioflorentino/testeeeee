import Foundation

struct CardRecharge {
    let title: String
    let description: String
    let image: String
    let accessory: String?
}

extension CardRecharge {
    enum TypeCard {
        case original
        case addDebit
        case standard
        case loan
    }
}
