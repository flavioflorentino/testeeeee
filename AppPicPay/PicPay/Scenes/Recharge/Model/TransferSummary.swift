import Foundation

struct TransferSummary: Decodable {
    let serviceTax: Double
    let rechargeValue: Double
    let serviceTaxValue: String
    let rechargeFinalValue: String
    
    private enum CodingKeys: String, CodingKey {
        case serviceTax = "service_charge"
        case rechargeValue = "recharge_value"
        case serviceTaxValue = "service_charge_value"
        case rechargeFinalValue = "recharge_final_value"
    }
    
    var rechargeValueCurrency: String {
        NumberFormatter.toCurrencyString(with: rechargeValue) ?? ""
    }
    
    var serviceTaxString: String {
        String(serviceTax).replacingOccurrences(of: ".", with: ",")
    }
    
    var serviceTaxValueCurrency: String {
        "-R$ \(String(describing: serviceTaxValue).replacingOccurrences(of: ".", with: ","))"
    }
    
    var rechargeFinalValueCurrency: String {
        "<b>R$ \(String(describing: rechargeFinalValue).replacingOccurrences(of: ".", with: ","))</b>"
    }
}
