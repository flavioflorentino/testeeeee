import Foundation

public struct InstructionInfo {
    let titleText: String?
    let descriptionText: NSAttributedString
    let buttonTitle: String?
    let linkTitle: String?
}
