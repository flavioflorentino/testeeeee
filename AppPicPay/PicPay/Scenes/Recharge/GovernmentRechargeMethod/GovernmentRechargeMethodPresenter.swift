import AssetsKit
import Foundation
import UI

protocol GovernmentRechargeMethodPresenting: AnyObject {
    var viewController: GovernmentRechargeMethodDisplaying? { get set }
    func display(methods: [RechargeMethod])
    func didNextStep(action: GovernmentRechargeMethodAction)
    func displayLoading(_ shouldDisplay: Bool)
    func displayError()
}

final class GovernmentRechargeMethodPresenter {
    typealias ErrorStrings = Strings.RechargeWireTransfer.Default.Error

    private let coordinator: GovernmentRechargeMethodCoordinating
    weak var viewController: GovernmentRechargeMethodDisplaying?

    init(coordinator: GovernmentRechargeMethodCoordinating) {
        self.coordinator = coordinator
    }
    
    private func getViewModel(from rechargeMethod: RechargeMethod) -> GovernmentRechargeMethodCardViewModel {
        GovernmentRechargeMethodCardViewModel(rechargeType: rechargeMethod.typeId,
                                              iconUrl: URL(string: rechargeMethod.imgUrl ?? ""),
                                              title: rechargeMethod.name,
                                              info: rechargeMethod.desc)
    }
}

// MARK: - GovernmentRechargeMethodPresenting
extension GovernmentRechargeMethodPresenter: GovernmentRechargeMethodPresenting {
    func display(methods: [RechargeMethod]) {
        guard let controller = viewController else { return }
        let viewModels = methods.map { getViewModel(from: $0) }
        controller.setUp(title: Strings.GovernmentRecharge.ChooseMethod.title)
        controller.emptyStack()
        for index in 0 ..< viewModels.count {
            let viewModel = viewModels[index]
            let lastAddedCard = controller.addCardToStack(with: viewModel)
            
            if index < methods.count - 1 {
                controller.addLineToStack(after: lastAddedCard)
            }
        }
    }
    
    func didNextStep(action: GovernmentRechargeMethodAction) {
        coordinator.perform(action: action)
    }
    
    func displayLoading(_ shouldDisplay: Bool) {
        viewController?.displayLoading(shouldDisplay)
    }
    
    func displayError() {
        let errorViewModel = StatefulErrorViewModel(image: Resources.Illustrations.iluNotFoundCircledBackground.image,
                                                    content: (title: ErrorStrings.title,
                                                              description: ErrorStrings.message),
                                                    button: (image: nil, title: ErrorStrings.tryAgain))
        viewController?.displayError(with: errorViewModel)
    }
}
