import SnapKit
import UI
import UIKit

protocol GovernmentRechargeMethodCard: UIView {
    func setUp(with viewModel: GovernmentRechargeMethodCardViewModel)
}

protocol GovernmentRechargeMethodCardDelegate: AnyObject {
    func didChooseRecharge(type: RechargeMethod.RechargeType)
}

final class GovernmentRechargeMethodCardComponent: UIView, ViewConfiguration {
    weak var delegate: GovernmentRechargeMethodCardDelegate?
    private var rechargeType: RechargeMethod.RechargeType?
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        return label
    }()
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
        return label
    }()
    
    private lazy var arrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.Images.greenArrow.image
        return imageView
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
        setUpTapAction()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(iconImageView,
                    titleLabel,
                    infoLabel,
                    arrowImageView)
    }
    
    func setupConstraints() {
        iconImageView.setContentHuggingPriority(.required, for: .horizontal)
        iconImageView.snp.makeConstraints {
            $0.centerY.equalTo(titleLabel)
            $0.width.height.equalTo(Spacing.base03)
            $0.leading.equalToSuperview().inset(Spacing.base02)
        }
        
        titleLabel.setContentCompressionResistancePriority(.required, for: .vertical)
        titleLabel.snp.makeConstraints {
            $0.leading.equalTo(iconImageView.snp.trailing).offset(Spacing.base02)
            $0.trailing.equalTo(arrowImageView.snp.leading).offset(-Spacing.base02)
            $0.top.equalToSuperview().inset(Spacing.base02)
        }
        
        infoLabel.setContentCompressionResistancePriority(.required, for: .vertical)
        infoLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalTo(titleLabel)
            $0.bottom.equalToSuperview().inset(Spacing.base02)
        }
        
        arrowImageView.setContentHuggingPriority(.required, for: .horizontal)
        arrowImageView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func setUpTapAction() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTap))
        addGestureRecognizer(gesture)
    }
    
    @objc
    func didTap() {
        guard let type = rechargeType else { return }
        delegate?.didChooseRecharge(type: type)
    }
}

private extension GovernmentRechargeMethodCardComponent {
    func getAttributedText(from string: String) -> NSAttributedString {
        let font = Typography.bodyPrimary().font()
        let attributedString = NSAttributedString(string: string, attributes: [.font: font])
        return attributedString.boldfyWithSystemFont(ofSize: font.pointSize, weight: UIFont.Weight.bold.rawValue)
    }
}

extension GovernmentRechargeMethodCardComponent: GovernmentRechargeMethodCard {
    func setUp(with viewModel: GovernmentRechargeMethodCardViewModel) {
        backgroundColor = Colors.backgroundPrimary.color
        rechargeType = viewModel.rechargeType
        titleLabel.text = viewModel.title
        infoLabel.attributedText = getAttributedText(from: viewModel.info ?? "")
        iconImageView.setImage(url: viewModel.iconUrl)
    }
}
