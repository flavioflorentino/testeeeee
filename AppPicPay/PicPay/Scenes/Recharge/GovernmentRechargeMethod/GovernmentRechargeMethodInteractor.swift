import AnalyticsModule
import FeatureFlag
import Foundation

protocol GovernmentRechargeMethodInteracting: AnyObject {
    func setUpMethods()
    func choose(method: RechargeMethod.RechargeType)
    func getHelp()
    func loadMethods()
}

final class GovernmentRechargeMethodInteractor {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies

    private var methods: [RechargeMethod]
    private let presenter: GovernmentRechargeMethodPresenting
    private let service: GovernmentRechargeMethodServicing

    init(methods: [RechargeMethod],
         presenter: GovernmentRechargeMethodPresenting,
         service: GovernmentRechargeMethodServicing,
         dependencies: Dependencies) {
        self.methods = methods
        self.presenter = presenter
        self.service = service
        self.dependencies = dependencies
    }
}

// MARK: - GovernmentRechargeMethodInteracting
extension GovernmentRechargeMethodInteractor: GovernmentRechargeMethodInteracting {
    var userHasValidCard: Bool {
        service.getCards().contains(where: { $0.isOneShotCvv ?? false })
    }
    
    func shouldStartVirtualCardFlow(for method: RechargeMethod.RechargeType) -> Bool {
        userHasValidCard && method == .emergencyAid
    }
    
    func setUpMethods() {
        methods.isEmpty ? loadMethods() : presenter.display(methods: methods)
    }
    
    func choose(method: RechargeMethod.RechargeType) {
        guard let rechargeMethod = methods.first(where: { $0.typeId == method }) else { return }
        if rechargeMethod.typeId == .bill {
            presenter.didNextStep(action: .boleto(rechargeMethod))
        } else if shouldStartVirtualCardFlow(for: rechargeMethod.typeId) {
            presenter.didNextStep(action: .addCard(rechargeMethod))
        } else {
            presenter.didNextStep(action: .debit(rechargeMethod))
        }
    }
    
    func getHelp() {
        let urlString = dependencies.featureManager.text(.opsHelpGovernmentRechargeString)
        presenter.didNextStep(action: .faq(URL(string: urlString)))
    }
    
    func loadMethods() {
        presenter.displayLoading(true)
        service.fetchMethods { [weak self] result in
            self?.presenter.displayLoading(false)
            switch result {
            case let .success(options):
                let methods = options.compactMap { $0.convertToLegacyMethod() }
                self?.methods = methods
                self?.presenter.display(methods: methods)
                
            case .failure:
                self?.presenter.displayError()
            }
        }
    }
}
