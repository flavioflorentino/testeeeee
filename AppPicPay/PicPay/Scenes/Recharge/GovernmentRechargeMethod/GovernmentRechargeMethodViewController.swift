import CashIn
import SnapKit
import UI
import UIKit

protocol GovernmentRechargeMethodDisplaying: AnyObject {
    func setUp(title: String)
    func emptyStack()
    func addCardToStack(with viewModel: GovernmentRechargeMethodCardViewModel) -> GovernmentRechargeMethodCard
    func addLineToStack(after view: UIView)
    func displayLoading(_ shouldDisplay: Bool)
    func displayError(with viewModel: StatefulErrorViewModel)
}

final class GovernmentRechargeMethodViewController: ViewController<GovernmentRechargeMethodInteracting, UIView> {
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceHorizontal = false
        return scrollView
    }()
    
    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        stackView.contentMode = .top
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .large))
        return label
    }()
    
    private lazy var faqBarButton = UIBarButtonItem(
        image: Assets.UpgradeChecklist.greenHelpButton.image,
        style: .done,
        target: self,
        action: #selector(didTapFaq)
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.setUpMethods()
    }

    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubviews(titleLabel, containerStackView)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.width.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints {
            $0.leading.top.trailing.width.equalToSuperview().inset(Spacing.base02)
        }
        
        containerStackView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base06)
            $0.leading.trailing.width.equalToSuperview()
            $0.bottom.equalToSuperview().inset(Spacing.base03)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        navigationItem.rightBarButtonItem = faqBarButton
        setUpNavigationBarDefaultAppearance()
    }
    
    private func addCardsToStackView(with methods: [GovernmentRechargeMethodCardViewModel]) {
        for index in 0 ..< methods.count {
            let method = methods[index]
            let card = GovernmentRechargeMethodCardComponent()
            containerStackView.addArrangedSubview(card)
            card.delegate = self
            card.setUp(with: method)
            
            if index < methods.count - 1 {
                containerStackView.insertLine(after: card, color: Colors.grayscale200.color)
            }
        }
    }
    
    @objc
    private func didTapFaq() {
        interactor.getHelp()
    }
}

// MARK: - GovernmentRechargeMethodDisplaying
extension GovernmentRechargeMethodViewController: GovernmentRechargeMethodDisplaying {
    func setUp(title: String) {
        titleLabel.text = title
    }
    
    func addCardToStack(with viewModel: GovernmentRechargeMethodCardViewModel) -> GovernmentRechargeMethodCard {
        let card = GovernmentRechargeMethodCardComponent()
        containerStackView.addArrangedSubview(card)
        card.delegate = self
        card.setUp(with: viewModel)
        return card
    }
    
    func addLineToStack(after view: UIView) {
        containerStackView.insertLine(after: view, color: Colors.grayscale200.color)
    }
    
    func emptyStack() {
        containerStackView.removeAllArrangedSubviews()
    }
    
    func displayLoading(_ shouldDisplay: Bool) {
        shouldDisplay ? beginState() : endState()
    }
    
    func displayError(with viewModel: StatefulErrorViewModel) {
        endState(model: viewModel)
    }
}

extension GovernmentRechargeMethodViewController: GovernmentRechargeMethodCardDelegate {
    func didChooseRecharge(type: RechargeMethod.RechargeType) {
        interactor.choose(method: type)
    }
}

extension GovernmentRechargeMethodViewController: StatefulTransitionViewing {
    func didTryAgain() {
        interactor.loadMethods()
    }
    
    func statefulViewForError() -> StatefulViewing {
        let view = EmptyStateView()
        view.buildLayout()
        view.delegate = self
        return view
    }
}
