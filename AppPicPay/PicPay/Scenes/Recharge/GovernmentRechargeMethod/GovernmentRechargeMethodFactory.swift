import UIKit

enum GovernmentRechargeMethodFactory {
    static func make(for methods: [RechargeMethod]) -> GovernmentRechargeMethodViewController {
        let container = DependencyContainer()
        let coordinator = GovernmentRechargeMethodCoordinator(dependencies: container)
        let presenter = GovernmentRechargeMethodPresenter(coordinator: coordinator)
        let service = GovernmentRechargeMethodService(dependencies: container)
        let interactor = GovernmentRechargeMethodInteractor(methods: methods,
                                                            presenter: presenter,
                                                            service: service,
                                                            dependencies: container)
        let viewController = GovernmentRechargeMethodViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
