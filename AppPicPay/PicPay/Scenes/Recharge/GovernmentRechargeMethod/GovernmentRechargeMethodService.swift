import Core
import Foundation

protocol GovernmentRechargeMethodServicing {
    func getCards() -> [CardBank]
    func fetchMethods(completion: @escaping (Result<[GovernmentRechargeOption], ApiError>) -> Void)
}

final class GovernmentRechargeMethodService {
    typealias Dependencies = HasMainQueue & HasCreditCardManager
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - GovernmentRechargeMethodServicing
extension GovernmentRechargeMethodService: GovernmentRechargeMethodServicing {
    func fetchMethods(completion: @escaping (Result<[GovernmentRechargeOption], ApiError>) -> Void) {
        let api = Api<[GovernmentRechargeOption]>(endpoint: GovernmentRechargeMethodEndpoint.methods)
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func getCards() -> [CardBank] {
        dependencies.creditCardManager.cardList
    }
}

enum GovernmentRechargeMethodEndpoint {
    case methods
}

extension GovernmentRechargeMethodEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .methods:
            return "api/getDetailsRechargeMethod/\(RechargeMethod.RechargeType.emergencyAid.rawValue)"
        }
    }
}
