import AssetsKit
import CashIn
import UI
import UIKit

enum GovernmentRechargeMethodAction: Equatable {
    case addCard(RechargeMethod)
    case debit(RechargeMethod)
    case boleto(RechargeMethod)
    case faq(URL?)
}

protocol GovernmentRechargeMethodCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: GovernmentRechargeMethodAction)
}

final class GovernmentRechargeMethodCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?
    private var tutorialProceedMethod: RechargeMethod?
    private var childCoordinator: Coordinating?
    private var addCardMethod: RechargeMethod?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - GovernmentRechargeMethodCoordinating
extension GovernmentRechargeMethodCoordinator: GovernmentRechargeMethodCoordinating {
    func perform(action: GovernmentRechargeMethodAction) {
        switch action {
        case let .addCard(method):
            addCardMethod = method
            startAddCardFlow()
            
        case let .debit(method):
            startRechargeFlow(with: method)
            
        case let .boleto(method):
            startTutorialFlow(with: method)
            
        case let .faq(url):
            startFaqFlow(from: url)
        }
    }
}

extension GovernmentRechargeMethodCoordinator: TutorialCoordinatorDelegate {
    func proceed(from context: TutorialContext) {
        guard case .boleto = context,
              let method = tutorialProceedMethod else { return }
        
        dismissModal()
        startRechargeFlow(with: method)
    }
}

private extension GovernmentRechargeMethodCoordinator {
    func startTutorialFlow(with method: RechargeMethod) {
        tutorialProceedMethod = method
        let controller = TutorialFactory.make(for: .boleto, delegate: self)
        let navigationController = UINavigationController(rootViewController: controller)
        let barButton = UIBarButtonItem(image: Resources.Icons.icoClose.image, style: .plain, target: self, action: #selector(dismissModal))
        controller.navigationItem.rightBarButtonItem = barButton

        viewController?.navigationController?.present(navigationController, animated: true)
    }
    
    func startAddCardFlow() {
        let controller = VirtualCardRegistrationFactory.make(flow: self)
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    func startRechargeFlow(with method: RechargeMethod) {
        let controller = RechargeValueFactory.make(model: method)
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    func startFaqFlow(from url: URL?) {
        DeeplinkHelper.handleDeeplink(withUrl: url, from: viewController)
    }
    
    @objc
    func dismissModal() {
        viewController?.dismiss(animated: true)
    }
}

extension GovernmentRechargeMethodCoordinator: VirtualCardRegistrationCoordinatorDelegate {
    func finishAddDebitCard(cardType: CardListType) {
        guard let currentViewController = viewController,
              let rechargeMethod = addCardMethod else { return }
        currentViewController.navigationController?.popToViewController(currentViewController, animated: true)
        startRechargeFlow(with: rechargeMethod)
        addCardMethod = nil
    }
}
