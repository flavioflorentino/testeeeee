struct GovernmentRechargeMethodCardViewModel {
    var rechargeType: RechargeMethod.RechargeType
    var iconUrl: URL?
    var title: String
    var info: String?
}
