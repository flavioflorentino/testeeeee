import Foundation

struct GovernmentRechargeOption: Decodable {
    let rechargeTypeId: String
    let rechargeTypeDescription: String
    let rechargeTypeName: String
    let rechargeTypeImage: String
    let serviceCharge: Decimal?
    
    func convertToLegacyMethod() -> RechargeMethod? {
        guard let typeInt = Int(rechargeTypeId),
              let typeId = RechargeMethod.RechargeType(rawValue: typeInt) else { return nil }
        
        let method = RechargeMethod(rechargeType: typeId,
                                    name: rechargeTypeName,
                                    description: rechargeTypeDescription,
                                    imgUrl: rechargeTypeImage,
                                    serviceCharge: serviceCharge)
        
        return method
    }
}
