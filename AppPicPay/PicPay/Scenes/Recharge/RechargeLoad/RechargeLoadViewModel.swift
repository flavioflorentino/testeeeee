import AnalyticsModule
import LoanOffer

protocol RechargeLoadViewModelInputs {
    func viewDidLoad()
    func close()
}

protocol RechargeLoadViewModelOutputs: AnyObject {
    func didNextStep(action: RechargeLoadAction)
    func didReceiveAnError(error: PicPayErrorDisplayable)
}

protocol RechargeLoadViewModelType: AnyObject {
    var inputs: RechargeLoadViewModelInputs { get }
    var outputs: RechargeLoadViewModelOutputs? { get set }
}

final class RechargeLoadViewModel: RechargeLoadViewModelType, RechargeLoadViewModelInputs {
    var inputs: RechargeLoadViewModelInputs { self }
    weak var outputs: RechargeLoadViewModelOutputs?

    private let service: RechargeLoadServicing
    private var recharge: Recharge?
    private var rechargeMethod: [RechargeMethod]
    
    init(service: RechargeLoadServicing) {
        self.service = service
        rechargeMethod = []
    }
    
    func viewDidLoad() {
        service.getRecharge(onSuccess: { [weak self] recharge, rechargeMethod in
            self?.recharge = recharge
            self?.rechargeMethod = rechargeMethod
            self?.trackLoanIfExists()
            self?.decideNextStep()
        }, onError: { [weak self] error in
            self?.outputs?.didReceiveAnError(error: error)
        })
    }
    
    func close() {
        outputs?.didNextStep(action: .close)
    }
    
    private func decideNextStep() {
        if let recharge = recharge, (recharge.statusId == "O") || (recharge.statusId == "E") {
            showRecharge(recharge)
        } else {
            outputs?.didNextStep(action: .rechargeMethods(rechargeMethod))
        }
    }
    
    private func showRecharge(_ recharge: Recharge) {
        if recharge.rechargeType == .bill {
            outputs?.didNextStep(action: .rechargeBill(recharge))
        } else if recharge.rechargeType == .deposit {
            outputs?.didNextStep(action: .rechargeDeposit(recharge))
        }
    }
    
    private func trackLoanIfExists() {
        let loanMethod = rechargeMethod.filter {
            $0.typeId == .loan
        }
        
        guard
            loanMethod.isNotEmpty,
            let loan = loanMethod.first
            else {
                return
        }
        
        Analytics.shared.log(
            LoanEvent.didSeeOffer(
                origin: .offer,
                bankId: loan.bankId ?? ""
            )
        )
    }
}
