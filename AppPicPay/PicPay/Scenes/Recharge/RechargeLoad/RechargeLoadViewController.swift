import UI

final class RechargeLoadViewController: LegacyViewController<RechargeLoadViewModelType, RechargeLoadCoordinating, UIView> {
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        
        return activityIndicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.inputs.viewDidLoad()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(activityIndicator)
    }
    
    override func configureViews() {
        title = RechargeLocalizable.title.text
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: DefaultLocalizable.btClose.text, style: .plain, target: self, action: #selector(clickLeftButton))
        view.backgroundColor = Palette.ppColorGrayscale100.color
        activityIndicator.startAnimating()
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }
    
    @objc
    private func clickLeftButton() {
        viewModel.inputs.close()
    }
}

extension RechargeLoadViewController: RechargeLoadViewModelOutputs {
    func didNextStep(action: RechargeLoadAction) {
        coordinator.perform(action: action)
    }
    
    func didReceiveAnError(error: PicPayErrorDisplayable) {
        let alert = Alert(with: error)
        AlertMessage.showAlert(alert, controller: self) { [weak self] _, _, _ in
            self?.viewModel.inputs.close()
        }
    }
}
