import Foundation

enum RechargeLoadAction {
    case rechargeBill(Recharge)
    case rechargeDeposit(Recharge)
    case rechargeMethods([RechargeMethod])
    case close
}

protocol RechargeLoadCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: RechargeLoadAction)
}

final class RechargeLoadCoordinator: RechargeLoadCoordinating {
    var viewController: UIViewController?
    
    func perform(action: RechargeLoadAction) {
        switch action {
        case .rechargeBill(let recharge):
            let controller = RechargeBillFactory.make(recharge: recharge)
            viewController?.navigationController?.pushViewController(controller, animated: false)
            
        case .rechargeDeposit(let recharge):
            let controller = RechargeBankFactory.make(model: recharge)
            viewController?.navigationController?.pushViewController(controller, animated: false)
            
        case .rechargeMethods(let rechargeMethod):
            let controller = RechargeMethodsFactory.make(with: rechargeMethod)
            viewController?.navigationController?.pushViewController(controller, animated: false)
            
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
