import Core
import Foundation

protocol RechargeLoadServicing {
    func getRecharge(onSuccess: @escaping(Recharge?, [RechargeMethod]) -> Void, onError: @escaping (PicPayErrorDisplayable) -> Void)
}

final class RechargeLoadService: RechargeLoadServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func getRecharge(onSuccess: @escaping(Recharge?, [RechargeMethod]) -> Void, onError: @escaping (PicPayErrorDisplayable) -> Void) {
        let mainQueue = dependencies.mainQueue
        
        WSConsumer.getLastRecharge { recharge, rechargeMethod, error in
            mainQueue.async {
                if let error = error {
                    let picpayError = PicPayError(message: error.localizedDescription)
                    onError(picpayError)
                } else {
                    let avaliableRecharge = rechargeMethod as? [RechargeMethod] ?? []    
                    onSuccess(recharge, avaliableRecharge)
                }
            }
        }
    }
}
