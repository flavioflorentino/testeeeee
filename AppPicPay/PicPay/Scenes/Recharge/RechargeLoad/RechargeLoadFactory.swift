import CashIn
import Foundation

enum RechargeLoadFactory {
    typealias Dependencies = HasFeatureManager
    
    static func make(with dependencies: Dependencies = DependencyContainer()) -> UIViewController {
        let isRedesignAvailable = dependencies.featureManager.isActive(.isCashInRedesignAvailable)
        return isRedesignAvailable ? makeCashInMethods() : makeLoad()
    }
}

private extension RechargeLoadFactory {
   static func makeLoad() -> UIViewController {
        let container = DependencyContainer()
        let service: RechargeLoadServicing = RechargeLoadService(dependencies: container)
        let viewModel: RechargeLoadViewModelType = RechargeLoadViewModel(service: service)
        var coordinator: RechargeLoadCoordinating = RechargeLoadCoordinator()
        let viewController = RechargeLoadViewController(viewModel: viewModel, coordinator: coordinator)
        
        if #available(iOS 13.0, *) {
            viewController.isModalInPresentation = true
        }
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController
        
        return viewController
    }
    
    static func makeCashInMethods() -> UIViewController {
        CashInMethodsFactory.make()
    }
}
