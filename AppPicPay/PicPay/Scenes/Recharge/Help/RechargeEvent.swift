import AnalyticsModule
import Foundation

enum RechargeEvent: AnalyticsKeyProtocol {
    case tapChangeCard
    case switchedDebitCard(cardBrand: String, cardIssuer: String)
    
    case start3DSChallenge(cardBrand: String, cardIssuer: String)
    case cancelled3DSChallenge(cardBrand: String, cardIssuer: String)
    case response3DSChallenge(response: ChallengeResponse, cardBrand: String, cardIssuer: String)
    case error3DSChallenge(description: String, cardBrand: String)
    case error3DSTransaction(description: String, cardBrand: String, reason: AdyenErrorStatusReason)
    
    case recharge(_ name: RechargeName)
    case rechargeValue(_ method: RechargeMethods, value: String)
    case rechargeEnable(_ name: RechargeName, enable: Bool)
    case wireTransfer(_ name: RechargeName)
    case faqDeeplink(_ name: RechargeName)
    case tapP2PLending(_ name: RechargeName)
    case notification(_ name: RechargeNotification)
    case receiptSharing
    case deeplink
    
    private var name: String {
        switch self {
        case .tapChangeCard:
            return "Cash-in - Touched Swicth Debt Card"
        case .switchedDebitCard:
            return "Cash-in - Debt Card Switched"
        case .start3DSChallenge:
            return "3DS - Challenge Started"
        case .cancelled3DSChallenge:
            return "3DS - Challenge Cancelled"
        case .response3DSChallenge:
            return "3DS - Challenge Response"
        case .error3DSChallenge, .error3DSTransaction:
            return "3DS - Error"
        case .rechargeValue:
            return "Solicitou Recarga"
        case .faqDeeplink:
            return "Cashin - tutorial"
        case .tapP2PLending:
            return "Button Clicked"
        case .receiptSharing:
            return "Cashin receipt - Share"
        case .deeplink:
            return "Cash-in Pix User Navigated"
        case .recharge, .rechargeEnable, .wireTransfer, .notification:
            return "Visualizacao de tela"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .switchedDebitCard(cardBrand, cardIssuer):
            return [KeyStrings.cardBrand: cardBrand, KeyStrings.cardIssuer: cardIssuer]
            
        case let .start3DSChallenge(cardBrand, cardIssuer):
            return [
                KeyStrings.cardBrand: cardBrand,
                KeyStrings.cardIssuer: cardIssuer,
                KeyStrings.transactionType: RechargeName.cashIn.description
            ]
            
        case let .cancelled3DSChallenge(cardBrand, cardIssuer):
            return [
                KeyStrings.cardBrand: cardBrand,
                KeyStrings.cardIssuer: cardIssuer,
                KeyStrings.transactionType: RechargeName.cashIn.description
            ]
            
        case let .response3DSChallenge(response, cardBrand, cardIssuer):
            return [
                KeyStrings.response: response.description,
                KeyStrings.cardBrand: cardBrand,
                KeyStrings.cardIssuer: cardIssuer,
                KeyStrings.transactionType: RechargeName.cashIn.description
            ]
            
        case let .error3DSChallenge(description, cardBrand):
            return [
                KeyStrings.error: description,
                KeyStrings.cardFlag: cardBrand,
                KeyStrings.transactionType: RechargeName.cashIn.description
            ]
            
        case let .error3DSTransaction(description, cardBrand, reason):
            return [
                KeyStrings.error: description,
                KeyStrings.cardFlag: cardBrand,
                KeyStrings.transactionType: RechargeName.cashIn.description,
                KeyStrings.errorCode: reason.hashValue
            ]
            
        case let .recharge(name):
            return [KeyStrings.name: name.description]
            
        case let .rechargeValue(method, value):
            return [KeyStrings.paymentMethods: method.description, KeyStrings.value: value]
            
        case let .rechargeEnable(name, enable):
            return [KeyStrings.name: name.description, KeyStrings.type: enable]
            
        case let .wireTransfer(name):
            return [KeyStrings.name: name.description]
            
        case let .faqDeeplink(name):
            return [KeyStrings.faq: name.description]
            
        case let .tapP2PLending(name):
            return [
                KeyStrings.screenName: name.description.uppercased().replacingOccurrences(of: " ", with: "_"),
                KeyStrings.buttonName: RechargeName.p2pLending,
                KeyStrings.context: KeyStrings.beginning
            ]
            
        case let .notification(name):
            return [KeyStrings.name: name.description]
            
        case .tapChangeCard, .receiptSharing, .deeplink:
            return [:]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.mixPanel, .appsFlyer, .firebase])
    }
}

extension RechargeEvent {
    private enum KeyStrings {
        static let errorCode = "error_code"
        static let cardBrand = "card_network"
        static let cardIssuer = "card_issuer"
        static let transactionType = "transaction_type"
        static let error = "error_description"
        static let cardFlag = "card_flag"
        static let name = "nome"
        static let value = "valor"
        static let type = "type"
        static let response = "response"
        static let paymentMethods = "Método pagamento"
        static let page = "Page"
        static let faq = "FAQ"
        static let screenName = "screen_name"
        static let buttonName = "button_name"
        static let context = "context"
        static let beginning = "INÍCIO"
    }
    
    enum RechargeName: String, CustomStringConvertible {
        case recharge = "Adicionar dinheiro"
        case debitPopup = "Adicionar dinheiro - Popup cadastro débito"
        case changeCard = "Trocar cartao de debito"
        case rechargeDebit = "Adicionar dinheiro - Debito"
        case rechargeDebitSuccess = "Adicionar dinheiro - Debito - Concluido"
        case cashIn = "CASH-IN"
        case picpayAccount = "Picpay Account - viewed"
        case howToAddMoney = "Picpay Account - tutorial"
        case moreAboutWireTransfer = "Saiba mais sobre transferência"
        case p2pLending = "P2P_LENDING"
        
        var description: String {
            rawValue
        }
    }
    
    enum RechargeNotification: String, CustomStringConvertible {
        case upgradeAccount = "Transferência Devolvida"
        case reminder = "Engajamento Conta PicPay"
        
        var description: String {
            rawValue
        }
    }
    
    enum RechargeMethods: String, CustomStringConvertible {
        case bill = "boleto"
        case bancoDoBrasil = "deposito bb"
        case bradesco = "deposito bradesco"
        case itau = "deposito itau"
        case original = "deposito original"
        
        var description: String {
            rawValue
        }
    }
    
    enum ChallengeResponse: String, CustomStringConvertible {
        case authorized = "AUTHORIZED"
        case refused = "NOT AUTHORIZE"
        
        var description: String {
            rawValue
        }
    }
}
