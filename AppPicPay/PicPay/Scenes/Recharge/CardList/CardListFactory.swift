import Foundation

enum CardListFactory {
    static func make(
        typeFilter: [CardBank.CardType],
        balanceEnable: Bool,
        onlyVirtualCards: Bool,
        analytics: CardListEventing
    ) -> CardListViewController {
        let container = DependencyContainer()
        let service: CardListServicing = CardListService(dependencies: container)
        let viewModel: CardListViewModelType = CardListViewModel(
            service: service,
            analytics: analytics,
            typeFilter: typeFilter,
            onlyVirtualCards: onlyVirtualCards,
            balanceEnable: balanceEnable
        )
        
        var coordinator: CardListCoordinating = CardListCoordinator()
        let viewController = CardListViewController(viewModel: viewModel, coordinator: coordinator)
        
        viewModel.outputs = viewController
        coordinator.viewController = viewController
        
        return viewController
    }
}
