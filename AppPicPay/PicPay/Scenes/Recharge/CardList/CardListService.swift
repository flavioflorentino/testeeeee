import Foundation

protocol CardListServicing {
    func deleteCard(id: String, _ completion: @escaping (Result<Void, PicPayError>) -> Void)
    func getCardsList(onCacheLoaded: @escaping ([CardBank]) -> Void, completion: @escaping (Result<[CardBank], PicPayError>) -> Void)
    func getDebitCardSelected() -> CardBank?
    func updateDebitCardSelected(id: String)
    func getCreditCardSelected() -> CardBank?
    func updateCreditCardSelected(id: String)
}

final class CardListService: BaseApi, CardListServicing {
    typealias Dependencies = HasCreditCardManager
    private var dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func getCreditCardSelected() -> CardBank? {
        dependencies.creditCardManager.defaultCreditCard
    }
    
    func updateCreditCardSelected(id: String) {
        let idCard = Int(id) ?? 0
        dependencies.creditCardManager.saveDefaultCreditCardId(idCard)
    }
    
    func getDebitCardSelected() -> CardBank? {
        dependencies.creditCardManager.defaultDebitCard()
    }

    func updateDebitCardSelected(id: String) {
        dependencies.creditCardManager.saveDefaultDebitCard(id)
    }

    func deleteCard(id: String, _ completion: @escaping (Result<Void, PicPayError>) -> Void) {
        let params = ["credit_card_id": id]
        requestManager.apiRequest(
            endpoint: WebServiceInterface.apiEndpoint(kWsUrlDeleteCreditCard),
            method: .post,
            parameters: params
        )
        .responseApiCodable { [weak self] (result: PicPayResult<BaseCodableEmptyResponse>) in
            switch result {
            case .success:
                self?.dependencies.creditCardManager.deleteCard(id: id)
                completion(.success(()))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func getCardsList(onCacheLoaded: @escaping ([CardBank]) -> Void, completion: @escaping (Result<[CardBank], PicPayError>) -> Void) {
        let cards = dependencies.creditCardManager.cardList
        onCacheLoaded(cards)
        
        requestManager.apiRequest(
            endpoint: kWsUrlGetPaymentMethods,
            method: .get
        )
        .responseApiCodable { [weak self] (result: PicPayResult<PaymentMethodsListResponse>) in
            switch result {
            case .success(let value):
                let card = value.paymentMethods
                self?.dependencies.creditCardManager.cardList = card
                completion(.success(card))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
