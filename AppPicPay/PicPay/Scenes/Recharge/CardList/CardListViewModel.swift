import Foundation

protocol CardListViewModelInputs {
    func viewDidLoad()
    func numberOfRows(section: Int) -> Int
    func numberOfSections() -> Int
    func balanceIsEnable() -> Bool
    func titleSection() -> String
    func deleteCard(index: Int)
    func confirmedCardDeletion(cardId: String)
    
    func didTapCard(index: Int)
    func didTapFooter()
    func card(index: Int) -> RechargeCard
    func close()
}

protocol CardListViewModelOutputs: AnyObject {
    func reloadData()
    func didReceiveAnError(error: PicPayErrorDisplayable)
    func warningCard(title: String, desc: String)
    func didNextStep(action: CardListAction)
    func showWarningDeleteCard(title: String, subtitle: String, cardId: String)
}

protocol CardListViewModelType: AnyObject {
    var inputs: CardListViewModelInputs { get }
    var outputs: CardListViewModelOutputs? { get set }
}

final class CardListViewModel: CardListViewModelType, CardListViewModelInputs {
    var inputs: CardListViewModelInputs { self }
    weak var outputs: CardListViewModelOutputs?

    private let service: CardListServicing
    private let analytics: CardListEventing
    private var model: [CardBank]
    private var typeFilter: [CardBank.CardType]
    private let balanceEnable: Bool
    private var idCardLoading: String?
    private var onlyVirtualCards: Bool
    
    init(
        service: CardListServicing,
        analytics: CardListEventing,
        typeFilter: [CardBank.CardType] = [.credit],
        onlyVirtualCards: Bool = false,
        balanceEnable: Bool = true
    ) {
        self.service = service
        self.analytics = analytics
        self.typeFilter = typeFilter
        self.onlyVirtualCards = onlyVirtualCards
        self.balanceEnable = balanceEnable
        self.model = []
    }

    func viewDidLoad() {
        reloadCards()
    }
    
    func card(index: Int) -> RechargeCard {
        let title = model[index].alias
        let description = model[index].description
        let image = model[index].image
        let status = cardStatus(index: index)
        
        return RechargeCard(title: title, description: description, image: image, status: status)
    }
    
    func didTapCard(index: Int) {
        let id = model[index].id
        
        if typeFilter.contains(.credit) {
            service.updateCreditCardSelected(id: id)
        } else {
            service.updateDebitCardSelected(id: id)
            analyticsSwitchedDebitCard()
        }
        
        outputs?.reloadData()
    }
    
    func balanceIsEnable() -> Bool {
        balanceEnable
    }
    
    func numberOfRows(section: Int) -> Int {
        model.count
    }
    
    func numberOfSections() -> Int {
        1
    }
    
    func didTapFooter() {
        analytics.tapAddCard()
        outputs?.didNextStep(action: .addNewCard({ [weak self] type in
            guard type != .debit else {
                self?.reloadCards()
                return
            }
            
            let message = RechargeLocalizable.warningCreditCard.text
            let title = DefaultLocalizable.attention.text
            self?.outputs?.warningCard(title: title, desc: message)
        }))
    }
    
    func close() {
        outputs?.didNextStep(action: .close)
    }
    
    func titleSection() -> String {
        typeFilter.contains(.credit) ? RechargeLocalizable.paymentMethods.text : RechargeLocalizable.debitRegistered.text
    }
    
    func deleteCard(index: Int) {
        guard model.indices.contains(index) else {
            return
        }
        let card = model[index]
        let title = EditCreditCardLocalizable.removeCard.text
        let subtitle = String(format: EditCreditCardLocalizable.removeCardWithLastDigits.text, card.lastDigits)
        
        outputs?.showWarningDeleteCard(title: title, subtitle: subtitle, cardId: card.id)
    }
    
    func confirmedCardDeletion(cardId: String) {
        showLoadingCard(cardId: cardId)
        
        service.deleteCard(id: cardId) { [weak self] result in
            switch result {
            case .success:
                self?.deleteCardSuccess(cardId: cardId)
            case .failure(let error):
                self?.deleteCardfailure(error: error)
            }
        }
    }
    
    private func showLoadingCard(cardId: String) {
        idCardLoading = cardId
        outputs?.reloadData()
    }
    
    private func deleteCardSuccess(cardId: String) {
        idCardLoading = nil
        reloadCards()
    }
    
    private func deleteCardfailure(error: PicPayError) {
        idCardLoading = nil
        outputs?.reloadData()
        outputs?.didReceiveAnError(error: error)
    }
    
    private func reloadCards() {
        service.getCardsList(onCacheLoaded: { [weak self] value in
            self?.model = value
            self?.filterCards()
        }, completion: { [weak self] result in
            self?.reloadCardsCompletion(result: result)
        })
    }
    
    private func reloadCardsCompletion(result: Result<[CardBank], PicPayError>) {
        switch result {
        case .success(let value):
            model = value
            filterCards()
        case .failure(let error):
            reciveError(error: error)
        }
    }
    
    private func cardStatus(index: Int) -> RechargeCard.Status {
        if cardIsLoading(index: index) {
            return .loading
        } else if cardIsSelected(index: index) {
            return .selected
        }
        
        return .none
    }
    
    private func cardIsSelected(index: Int) -> Bool {
        guard model.indices.contains(index) else {
            return false
        }
        
        let id = model[index].id
        
        return typeFilter.contains(.credit) ? id == service.getCreditCardSelected()?.id : id == service.getDebitCardSelected()?.id
    }
    
    private func cardIsLoading(index: Int) -> Bool {
        guard model.indices.contains(index) else {
            return false
        }
        
        return model[index].id == idCardLoading
    }
    
    private func filterCards() {
        if onlyVirtualCards {
            model = model.filter { $0.isOneShotCvv ?? false }
        } else {
            model = model.filter { typeFilter.contains($0.type) }
        }
        outputs?.reloadData()
    }
    
    private func reciveError(error: PicPayErrorDisplayable) {
        if model.isEmpty {
            outputs?.didReceiveAnError(error: error)
        }
    }
    
    private func analyticsSwitchedDebitCard() {
        guard let card = service.getDebitCardSelected(), let cardBrand = card.cardBrand, let cardIssuer = card.cardIssuer else {
            return
        }
        analytics.changeDefaultDebitCard(cardBrand: cardBrand, cardIssuer: cardIssuer)
    }
}
