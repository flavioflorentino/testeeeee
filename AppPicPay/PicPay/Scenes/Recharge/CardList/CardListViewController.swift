import AnalyticsModule
import UI
import UIKit

final class CardListViewController: LegacyViewController<CardListViewModelType, CardListCoordinating, UIView> {
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = 80
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        return tableView
    }()
    
    private lazy var headerViewController: PaymentBalanceViewController = {
        PaymentBalanceFactory.make()
    }()
    
    private lazy var footerView: UIView = {
        let width = UIScreen.main.bounds.width
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 80))
        let footer = FooterCardListView(frame: footerView.frame)
        footer.delegate = self
        footerView.addSubview(footer)
        
        return footerView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        viewModel.inputs.viewDidLoad()
    }
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.shared.log(RechargeEvent.recharge(.changeCard))
    }
    
    private func setupTableView() {
        tableView.register(CardViewTableViewCell.self, forCellReuseIdentifier: String(describing: CardViewTableViewCell.self))
        if viewModel.inputs.balanceIsEnable() {
            createHeader()
        }
        tableView.tableFooterView = footerView
    }
    
    private func createHeader() {
        let width = UIScreen.main.bounds.width
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 100))
        
        addChild(headerViewController)
        headerView.addSubview(headerViewController.view)
        headerViewController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.constraintAllEdges(from: headerViewController.view, to: headerView)
        headerViewController.didMove(toParent: self)
        
        tableView.tableHeaderView = headerView
    }
    
    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override func configureViews() {
        title = RechargeLocalizable.changeCard.text
        view.backgroundColor = Palette.ppColorGrayscale100.color
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.constraintAllEdges(from: tableView, to: view)
    }
}

// MARK: View Model Outputs
extension CardListViewController: CardListViewModelOutputs {
    func reloadData() {
        tableView.reloadData()
    }
    
    func didReceiveAnError(error: PicPayErrorDisplayable) {
        let alert = Alert(with: error)
        AlertMessage.showAlert(alert, controller: self) { [weak self] _, _, _ in
            self?.viewModel.inputs.close()
        }
    }
    
    func warningCard(title: String, desc: String) {
        let alert = Alert(title: title, text: desc)
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func didNextStep(action: CardListAction) {
        coordinator.perform(action: action)
    }
    
    func showWarningDeleteCard(title: String, subtitle: String, cardId: String) {
        let alert = Alert(title: title, text: subtitle)
        let removeButton = Button(title: EditCreditCardLocalizable.removeCard.text, type: .destructive) { [weak self] popupController, _ in
            self?.viewModel.inputs.confirmedCardDeletion(cardId: cardId)
            popupController.dismiss(animated: true)
        }
        let cancelButton = Button(title: DefaultLocalizable.btCancel.text, type: .clean, action: .close)
        alert.buttons = [removeButton, cancelButton]
        AlertMessage.showAlert(alert, controller: self)
    }
}

extension CardListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        viewModel.inputs.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.inputs.numberOfRows(section: section)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let title = viewModel.inputs.titleSection()
        let view = HeaderCardListView()
        view.configureView(title: title)
        
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CardViewTableViewCell.self), for: indexPath) as? CardViewTableViewCell else {
            return UITableViewCell()
        }
        
        let index = indexPath.row
        let model = viewModel.inputs.card(index: index)
        cell.configure(with: model)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: DefaultLocalizable.delete.text) { [weak self] _, _ in
            self?.viewModel.inputs.deleteCard(index: indexPath.row)
        }
        
        return [delete]
    }
}

extension CardListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        viewModel.inputs.didTapCard(index: row)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension CardListViewController: FooterCardListViewDelegate {
    func didTapFooter() {
        viewModel.inputs.didTapFooter()
    }
}
