import UIKit

enum CardListType {
    case debit
    case credit
}

enum CardListAction {
    case addNewCard((CardListType) -> Void)
    case close
}

protocol CardListCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: CardListAction)
}

final class CardListCoordinator: CardListCoordinating {
    weak var viewController: UIViewController?
    private var addCard: ((CardListType) -> Void)?
    
    func perform(action: CardListAction) {
        switch action {
        case .addNewCard(let completion):
            guard let addCreditCardVC = ViewsManager.paymentMethodsStoryboardViewController(withIdentifier: "AddNewCreditCardViewController") as? AddNewCreditCardViewController else {
                return
            }
            
            addCard = completion
            let viewModel = AddNewCreditCardViewModel(origin: .recharge)
            addCreditCardVC.viewModel = viewModel
            addCreditCardVC.delegate = self
            viewController?.navigationController?.pushViewController(addCreditCardVC, animated: true)
            
        case .close:
            viewController?.navigationController?.popViewController(animated: true)
        }
    }
}

extension CardListCoordinator: AddNewCreditCardDelegate {
    func debitCardDidInsert() {
        addCard?(.debit)
    }
    
    func creditCardDidInsert() {
        addCard?(.credit)
    }
}
