import UI
import UIKit

final class CardContainerView: UIView {
    private let sizeImage: CGFloat = 32
    
    lazy var rootStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.spacing = 12.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    }()
    
    lazy var image: UIImageView = {
        let image = UIImageView()
        image.backgroundColor = Palette.ppColorGrayscale000.color
        
        return image
    }()

    private lazy var rootView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale000.color
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .clear
        rootView.clipsToBounds = true
        rootView.layer.cornerRadius = 5
        rootView.addShadow(with: Palette.ppColorGrayscale600.color, alpha: 0.1, radius: 6, offset: CGSize(width: 0, height: 2))
    }
    
    private func addComponents() {
        rootStackView.addArrangedSubview(image)
        rootView.addSubview(rootStackView)
        addSubview(rootView)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            rootView.topAnchor.constraint(equalTo: topAnchor, constant: 3),
            rootView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -3),
            rootView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12),
            rootView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12)
        ])
        
        NSLayoutConstraint.activate([
            rootStackView.topAnchor.constraint(equalTo: rootView.topAnchor, constant: 15),
            rootStackView.bottomAnchor.constraint(equalTo: rootView.bottomAnchor, constant: -15),
            rootStackView.leadingAnchor.constraint(equalTo: rootView.leadingAnchor, constant: 17),
            rootStackView.trailingAnchor.constraint(equalTo: rootView.trailingAnchor, constant: -17)
        ])
        
        NSLayoutConstraint.activate([
            image.heightAnchor.constraint(equalToConstant: sizeImage),
            image.widthAnchor.constraint(equalToConstant: sizeImage)
        ])
    }
}
