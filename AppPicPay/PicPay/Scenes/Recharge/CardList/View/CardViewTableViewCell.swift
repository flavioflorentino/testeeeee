import UI

final class CardViewTableViewCell: UITableViewCell, ViewConfiguration {
    private lazy var conteinerView: CardContainerView = {
        let view = CardContainerView()
        view.translatesAutoresizingMaskIntoConstraints = false

        return view
    }()

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 2.0

        return stackView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = 3

        return label
    }()

    private lazy var descLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.textColor = Palette.ppColorGrayscale400.color

        return label
    }()

    private lazy var selectedLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 11, weight: .regular)
        label.textColor = Palette.ppColorGrayscale300.color
        label.textAlignment = .right

        return label
    }()

    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.stopAnimating()
        activityIndicator.hidesWhenStopped = true

        return activityIndicator
    }()

    private lazy var presenter: CardPresenterType = {
        let presenter = CardPresenter()
        presenter.outputs = self

        return presenter
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configureViews() {
        backgroundColor = .clear
        selectedLabel.text = RechargeLocalizable.mainCard.text
        selectionStyle = .none
    }

    func buildViewHierarchy() {
        contentView.addSubview(conteinerView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(descLabel)
        conteinerView.rootStackView.addArrangedSubview(stackView)
        conteinerView.rootStackView.addArrangedSubview(selectedLabel)
        conteinerView.rootStackView.addArrangedSubview(activityIndicator)
    }

    func setupConstraints() {
        NSLayoutConstraint.activate([
            conteinerView.topAnchor.constraint(equalTo: contentView.topAnchor),
            conteinerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            conteinerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            conteinerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
        ])
    }

    func configure(with model: RechargeCard) {
        presenter.inputs.viewConfigure(model: model)
    }
}

extension CardViewTableViewCell: CardPresenterOutputs {
    func displayTitle(_ title: String) {
        titleLabel.text = title
    }

    func displayDescription(_ description: String) {
        descLabel.text = description
    }

    func displayImage(_ image: URL?) {
        conteinerView.image.setImage(url: image)
    }

    func cardSelected(isHidden: Bool) {
        selectedLabel.isHidden = isHidden
        layoutIfNeeded()
    }

    func startLoading() {
        activityIndicator.startAnimating()
        layoutIfNeeded()
    }

    func stopLoading() {
        activityIndicator.stopAnimating()
        layoutIfNeeded()
    }
}
