import Foundation

protocol CardPresenterInputs {
    func viewConfigure(model: RechargeCard)
}

protocol CardPresenterOutputs: AnyObject {
    func displayTitle(_ title: String)
    func displayDescription(_ description: String)
    func displayImage(_ image: URL?)
    func cardSelected(isHidden: Bool)
    func startLoading()
    func stopLoading()
}

protocol CardPresenterType: AnyObject {
    var inputs: CardPresenterInputs { get }
    var outputs: CardPresenterOutputs? { get set }
}

final class CardPresenter: CardPresenterType, CardPresenterInputs {
    var inputs: CardPresenterInputs { self }
    weak var outputs: CardPresenterOutputs?
    
    func viewConfigure(model: RechargeCard) {
        outputs?.displayTitle(model.title)
        outputs?.displayDescription(model.description)
        outputs?.displayImage(URL(string: model.image))
        
        displayAnimation(status: model.status)
        displayText(status: model.status)
    }
    
    private func displayAnimation(status: RechargeCard.Status) {
        status == .loading ? outputs?.startLoading() : outputs?.stopLoading()
    }
    
    private func displayText(status: RechargeCard.Status) {
        let isEnable = status == .selected
        outputs?.cardSelected(isHidden: !isEnable)
    }
}
