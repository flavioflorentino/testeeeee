import UI
import UIKit

protocol FooterCardListViewDelegate: AnyObject {
    func didTapFooter()
}

final class FooterCardListView: UIView, ViewConfiguration {
    private lazy var conteinerView: CardContainerView = {
        let view = CardContainerView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        label.textColor = Palette.ppColorBranding300.color
        
        return label
    }()
    
    weak var delegate: FooterCardListViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        buildLayout()
        addGesture()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        backgroundColor = .clear
        titleLabel.text = RechargeLocalizable.addCard.text
        conteinerView.image.image = Assets.PaymentMethods.iconCardAdd.image
    }
    
    func buildViewHierarchy() {
        addSubview(conteinerView)
        conteinerView.rootStackView.addArrangedSubview(titleLabel)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            conteinerView.topAnchor.constraint(equalTo: topAnchor),
            conteinerView.bottomAnchor.constraint(equalTo: bottomAnchor),
            conteinerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            conteinerView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
    
    private func addGesture() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapCard))
        addGestureRecognizer(gesture)
    }
    
    @objc
    private func didTapCard() {
        delegate?.didTapFooter()
    }
}
