import UI

final class HeaderCardListView: UIView, ViewConfiguration {
    private enum Layout {
        static let marginLeadingTrailing: CGFloat = 16
        static let marginTopBottom: CGFloat = 12
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        backgroundColor = .clear
    }
    
    func buildViewHierarchy() {
        addSubview(titleLabel)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: Layout.marginLeadingTrailing),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Layout.marginTopBottom),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Layout.marginTopBottom),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Layout.marginLeadingTrailing)
        ])
    }
    
    func configureView(title: String) {
        titleLabel.text = title
    }
}
