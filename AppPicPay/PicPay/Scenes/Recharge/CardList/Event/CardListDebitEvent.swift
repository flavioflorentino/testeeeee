import AnalyticsModule
import Foundation

protocol CardListEventing {
    func changeDefaultDebitCard(cardBrand: String, cardIssuer: String)
    func tapAddCard()
}

final class CardListDebitEvent: CardListEventing {
    func changeDefaultDebitCard(cardBrand: String, cardIssuer: String) {
        Analytics.shared.log(RechargeEvent.switchedDebitCard(cardBrand: cardBrand, cardIssuer: cardIssuer))
    }
    
    func tapAddCard() {
    }
}
