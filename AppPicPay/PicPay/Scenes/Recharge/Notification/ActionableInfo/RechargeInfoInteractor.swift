import Core
import AnalyticsModule
import FeatureFlag
import Foundation

protocol RechargeInfoInteracting: AnyObject {
    func loadInfo()
    func handleAction()
    func handleLink()
    func close()
    func fireDisplayEvent()
}

final class RechargeInfoInteractor {
    typealias Localizable = Strings.RechargeWireTransfer
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies

    private let incentiveId: IncentiveId
    private let presenter: RechargeInfoPresenting

    init(incentiveId: IncentiveId, presenter: RechargeInfoPresenting, dependencies: Dependencies) {
        self.incentiveId = incentiveId
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

private extension RechargeInfoInteractor {
    var image: UIImage {
        Assets.Recharge.iluWireTransferIncentive.image
    }
    
    var title: String {
        switch incentiveId {
        case .reminder:
            return Localizable.Incentive.Reminder.title
        case .upgrade:
            return Localizable.Incentive.UpgradeAccount.title
        }
    }
    
    var message: String {
        switch incentiveId {
        case .reminder:
            return Localizable.Incentive.Reminder.message
        case .upgrade:
            return Localizable.Incentive.UpgradeAccount.message
        }
    }
    
    var actionTitle: String {
        switch incentiveId {
        case .reminder:
            return Localizable.Default.action
        case .upgrade:
            return Localizable.Default.continue
        }
    }
    
    var url: URL? {
        let featureKey: FeatureConfig
        switch incentiveId {
        case .reminder:
            featureKey = .opsCashInNotificationReminderString
        case .upgrade:
            featureKey = .opsCashInNotificationUpgradeAccountString
        }
        return URL(string: dependencies.featureManager.text(featureKey))
    }
    
    var shouldDisplayLink: Bool {
        switch incentiveId {
        case .reminder:
            return false
        case .upgrade:
            return true
        }
    }
    
    var notificationEvent: RechargeEvent.RechargeNotification {
        switch incentiveId {
        case .reminder:
            return .reminder
        case .upgrade:
            return .upgradeAccount
        }
    }
}

// MARK: - RechargeInfoInteracting
extension RechargeInfoInteractor: RechargeInfoInteracting {
    func fireDisplayEvent() {
        let event = RechargeEvent.notification(notificationEvent)
        dependencies.analytics.log(event)
    }
    
    func loadInfo() {
        presenter.setUp(image: image,
                        title: title,
                        message: message)
        presenter.setUpAction(with: actionTitle)
        if shouldDisplayLink {
            presenter.setUpLink(with: Localizable.Default.close)
        }
    }
    
    func handleAction() {
        guard let url = url else {
            presenter.showError(title: DefaultLocalizable.oops.text,
                                message: Strings.RechargeWireTransfer.Default.Action.Error.message)
            return
        }
        
        switch incentiveId {
        case .reminder:
            presenter.didNextStep(action: .webview(url: url))
        case .upgrade:
            presenter.didNextStep(action: .deeplink(url: url))
        }
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
    
    func handleLink() {
        if shouldDisplayLink {
            presenter.didNextStep(action: .close)
        }
    }
    
    private func showError() {
        presenter.showError(title: DefaultLocalizable.oops.text,
                            message: Strings.RechargeWireTransfer.Default.Action.Error.message)
    }
}
