import Foundation

enum RechargeInfoFactory {
    static func make(for incentiveId: IncentiveId) -> RechargeInfoViewController {
        let container = DependencyContainer()
        let coordinator: RechargeInfoCoordinating = RechargeInfoCoordinator()
        let presenter: RechargeInfoPresenting = RechargeInfoPresenter(coordinator: coordinator, dependencies: container)
        let interactor: RechargeInfoInteracting = RechargeInfoInteractor(incentiveId: incentiveId,
                                                                         presenter: presenter,
                                                                         dependencies: container)
        
        let viewController = RechargeInfoViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
