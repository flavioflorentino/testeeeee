import Foundation

protocol RechargeInfoPresenting: AnyObject {
    var viewController: RechargeInfoDisplay? { get set }
    func didNextStep(action: RechargeInfoAction)
    
    func setUp(image: UIImage, title: String, message: String)
    func setUpAction(with title: String)
    func setUpLink(with title: String)
    func showError(title: String, message: String)
}

final class RechargeInfoPresenter {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let coordinator: RechargeInfoCoordinating
    weak var viewController: RechargeInfoDisplay?

    init(coordinator: RechargeInfoCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
}

// MARK: - RechargeInfoPresenting
extension RechargeInfoPresenter: RechargeInfoPresenting {
    func setUp(image: UIImage, title: String, message: String) {
        viewController?.setUp(image: image, title: title, message: message)
    }
    
    func setUpAction(with title: String) {
        viewController?.setUpAction(with: title)
    }
    
    func setUpLink(with title: String) {
        viewController?.setUpLink(with: title)
    }
    
    func didNextStep(action: RechargeInfoAction) {
        coordinator.perform(action: action)
    }
    
    func showError(title: String, message: String) {
        viewController?.showError(title: title, message: message)
    }
}
