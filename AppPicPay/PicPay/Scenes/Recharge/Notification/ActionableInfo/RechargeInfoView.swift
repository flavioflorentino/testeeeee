import SnapKit
import UI
import UIKit

protocol RechargeInfoViewDelegate: AnyObject {
    func didTapAction()
    func didTapLink()
}

final class RechargeInfoView: UIView, ViewConfiguration {
    private lazy var illustrationImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.setContentHuggingPriority(.defaultLow, for: .vertical)
        return imageView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .large))
            .with(\.textAlignment, .center)
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, Colors.grayscale700.color)
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()
    
    private lazy var footerActionsContainer: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var actionButton: UIButton = {
        let button = UIButton(type: .custom)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var linkButton: UIButton = {
        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(didTapLink), for: .touchUpInside)
        return button
    }()
    
    weak var delegate: RechargeInfoViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(illustrationImageView,
                    titleLabel,
                    descriptionLabel,
                    footerActionsContainer)
    }
    
    func setupConstraints() {
        illustrationImageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base04)
            $0.centerX.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(illustrationImageView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.bottom.lessThanOrEqualTo(footerActionsContainer.snp.top).offset(-Spacing.base02)
        }
        
        footerActionsContainer.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().inset(Spacing.base04)
        }
    }
    
    func configureViews() {
        viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
    }
    
    func setUp(image: UIImage, title: String, message: String) {
        illustrationImageView.image = image
        titleLabel.text = title
        
        let font = Typography.bodyPrimary().font()
        let attributedMessage = NSAttributedString(
            string: message,
            attributes: [.font: font]
        )
        .boldfyWithSystemFont(ofSize: font.pointSize, weight: UIFont.Weight.bold.rawValue)
        descriptionLabel.attributedText = attributedMessage
    }
    
    func setUpActionButton(with title: String) {
        actionButton.setTitle(title, for: .normal)
        if actionButton.superview == nil {
            footerActionsContainer.addArrangedSubview(actionButton)
        }
    }
    
    func setUpLinkButton(with title: String) {
        linkButton.setTitle(title, for: .normal)
        linkButton.buttonStyle(LinkButtonStyle())
        if linkButton.superview == nil {
            footerActionsContainer.addArrangedSubview(linkButton)
        }
    }
}

@objc
private extension RechargeInfoView {
    func didTapAction() {
        delegate?.didTapAction()
    }
    
    func didTapLink() {
        delegate?.didTapLink()
    }
}
