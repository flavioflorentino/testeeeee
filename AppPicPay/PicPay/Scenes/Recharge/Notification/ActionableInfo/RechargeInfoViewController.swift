import UIKit
import UI

protocol RechargeInfoDisplay: AnyObject {
    func setUp(image: UIImage, title: String, message: String)
    func setUpAction(with title: String)
    func setUpLink(with title: String)
    func showError(title: String, message: String)
}

final class RechargeInfoViewController: ViewController<RechargeInfoInteracting, RechargeInfoView> {
    private lazy var closeBarButtonItem: UIBarButtonItem = {
        let barButtonItem = UIBarButtonItem(image: Assets.Icons.iconClose.image,
                                            style: .plain,
                                            target: self,
                                            action: #selector(didTapClose))

        barButtonItem.accessibilityLabel = DefaultLocalizable.btClose.text
        return barButtonItem
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        interactor.loadInfo()
    }
    
    override func configureViews() {
        rootView.delegate = self
        rootView.buildLayout()
        setUpNavigation()
    }
    
    private func setUpNavigation() {
        setUpNavigationBarDefaultAppearance()
        navigationItem.leftBarButtonItem = closeBarButtonItem
    }
    
    @objc
    private func didTapClose() {
        interactor.close()
    }
}

// MARK: RechargeInfoDisplay
extension RechargeInfoViewController: RechargeInfoDisplay {
    func setUp(image: UIImage, title: String, message: String) {
        rootView.setUp(image: image, title: title, message: message)
    }
    
    func setUpAction(with title: String) {
        rootView.setUpActionButton(with: title)
    }
    
    func setUpLink(with title: String) {
        rootView.setUpLinkButton(with: title)
    }
    
    func showError(title: String, message: String) {
        let alert = Alert(title: title, text: message)
        let controller = AlertPopupViewController(alert: alert, controller: self)
        showPopup(controller)
    }
}

extension RechargeInfoViewController: RechargeInfoViewDelegate {
    func didTapAction() {
        interactor.handleAction()
    }
    
    func didTapLink() {
        interactor.handleLink()
    }
}
