import UIKit

enum RechargeInfoAction: Equatable {
    case deeplink(url: URL)
    case webview(url: URL)
    case close
}

protocol RechargeInfoCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RechargeInfoAction)
}

final class RechargeInfoCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - RechargeInfoCoordinating
extension RechargeInfoCoordinator: RechargeInfoCoordinating {
    func perform(action: RechargeInfoAction) {
        switch action {
        case let .deeplink(url):
            viewController?.dismiss(animated: true, completion: {
                DeeplinkHelper.handleDeeplink(withUrl: url)
            })
        case let .webview(url):
            let webview = WebViewFactory.make(with: url)
            viewController?.present(UINavigationController(rootViewController: webview), animated: true)
        case .close:
            viewController?.dismiss(animated: true, completion: nil)
        }
    }
}
