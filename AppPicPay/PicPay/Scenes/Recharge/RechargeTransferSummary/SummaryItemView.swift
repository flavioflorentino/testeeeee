import Foundation
import UI
import UIKit

final class SummaryItemView: UIView, ViewConfiguration {
    private lazy var descriptionLabel = UILabel()
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.setContentCompressionResistancePriority(.required, for: .horizontal)
        label.setContentHuggingPriority(.required, for: .horizontal)
        
        return label
    }()
    
    private lazy var itemStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.spacing = Spacing.base01

        return stack
    }()
    
    func setup(for description: String, itemValue: String) {
        let font = Typography.bodyPrimary().font()
        let attributedMessage = NSAttributedString(
            string: description,
            attributes: [.font: font]
        )
        .boldfyWithSystemFont(ofSize: font.pointSize, weight: UIFont.Weight.bold.rawValue)
        descriptionLabel.attributedText = attributedMessage
        
        let attributedMessageValue = NSAttributedString(
            string: itemValue,
            attributes: [.font: font]
        )
        .boldfyWithSystemFont(ofSize: font.pointSize, weight: UIFont.Weight.bold.rawValue)
        valueLabel.attributedText = attributedMessageValue
    }
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        itemStackView.addArrangedSubview(descriptionLabel)
        itemStackView.addArrangedSubview(valueLabel)

        self.addSubview(itemStackView)
    }
    
    func setupConstraints() {
        itemStackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}
