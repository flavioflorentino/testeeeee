import AssetsKit
import UI
import UIKit

protocol RechargeTransferSummaryDisplaying: AnyObject {
    func setupSummaryInfo(_ summary: TransferSummary)
    func presentError()
    func startLoading()
    func stopLoading()
}

private extension RechargeTransferSummaryViewController.Layout {
    enum Image {
        static let size = CGSize(width: 130.0, height: 130.0)
    }
}

final class RechargeTransferSummaryViewController: ViewController<RechargeTransferSummaryInteracting, UIView> {
    fileprivate enum Layout { }
    private typealias Localizable = Strings.GovernmentRecharge
    
    lazy var loadingView: LoadingView = {
        let loadingView = LoadingView()
        return loadingView
    }()

    private lazy var scrollView = UIScrollView()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView(image: Assets.Recharge.iluRechargeMoney.image)
        imageView.contentMode = .scaleAspectFit
        imageView.size = Layout.Image.size
        
        return imageView
    }()
    
    private lazy var descriptionText: UILabel = {
        let description = UILabel()
        description.numberOfLines = 2
        description.textAlignment = .center
        let font = Typography.bodyPrimary().font()
        let attributedMessage = NSAttributedString(
            string: Strings.GovernmentRecharge.transferSummaryDescription,
            attributes: [.font: font]
        )
        .boldfyWithSystemFont(ofSize: font.pointSize, weight: UIFont.Weight.bold.rawValue)
        description.attributedText = attributedMessage
        
        return description
    }()
    
    private lazy var actionButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapContinue), for: .touchUpInside)
        button.setTitle(Localizable.transferSummaryButton, for: .normal)
        button.isHidden = true
        
        return button
    }()
    
    private lazy var linkButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.transferSummaryLink, for: .normal)
        button.buttonStyle(LinkButtonStyle()) // pimary-default, 16pt, color 00BC54
        button.addTarget(self, action: #selector(didTapNotNow), for: .touchUpInside)
        button.isHidden = true
        
        return button
    }()
    
    private lazy var valueAddedItem = SummaryItemView()
    private lazy var valueServiceTaxItem = SummaryItemView()
    private lazy var valueFinalValueItem = SummaryItemView()
    
    private lazy var rootStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .equalSpacing
        stack.spacing = Spacing.base06
        stack.compatibleLayoutMargins = .rootView
        stack.isLayoutMarginsRelativeArrangement = true
        
        return stack
    }()
    
    private lazy var topStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Spacing.base04
        
        return stack
    }()
    
    private lazy var middleStackView: UIStackView = {
        let stack = UIStackView()
        stack.spacing = Spacing.base02
        stack.axis = .vertical
        
        return stack
    }()
    
    private lazy var bottomStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Spacing.base01
        
        return stack
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor.fetchSummaryInfo()
    }
    
    override func buildViewHierarchy() {
        topStackView.addArrangedSubview(imageView)
        topStackView.addArrangedSubview(descriptionText)
        topStackView.addArrangedSubview(middleStackView)
        
        middleStackView.addArrangedSubview(SeparatorView(style: BackgroundViewStyle(color: .grayscale100())))
        middleStackView.addArrangedSubview(valueAddedItem)
        middleStackView.addArrangedSubview(SeparatorView(style: BackgroundViewStyle(color: .grayscale100())))
        middleStackView.addArrangedSubview(valueServiceTaxItem)
        middleStackView.addArrangedSubview(SeparatorView(style: BackgroundViewStyle(color: .grayscale100())))
        middleStackView.addArrangedSubview(valueFinalValueItem)
        
        bottomStackView.addArrangedSubview(actionButton)
        bottomStackView.addArrangedSubview(linkButton)
        
        rootStackView.addArrangedSubview(topStackView)
        rootStackView.addArrangedSubview(bottomStackView)
        
        scrollView.addSubview(rootStackView)
        view.addSubview(scrollView)
    }
    
    override func setupConstraints() {
        rootStackView.snp.makeConstraints {
            $0.edges.width.equalToSuperview()
            $0.height.greaterThanOrEqualTo(view.layoutMarginsGuide)
        }
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color

        actionButton.isHidden = false
        linkButton.isHidden = false
        
        navigationItem.title = Strings.GovernmentRecharge.transferSummaryScreenTitle
        let image = Assets.UpgradeChecklist.greenHelpButton.image
        let item = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(goToHelpCenter))
        navigationItem.rightBarButtonItem = item
    }
    
    @objc
    private func didTapContinue() {
        interactor.didTapContinue()
    }
    
    @objc
    private func didTapNotNow() {
        interactor.didTapNotNow()
    }
    
    @objc
    private func goToHelpCenter() {
        interactor.goToHelpCenter()
    }
    
    private func reloadView() {
        interactor.fetchSummaryInfo()
    }
}

// MARK: - RechargeTransferSummaryDisplaying
extension RechargeTransferSummaryViewController: RechargeTransferSummaryDisplaying, LoadingViewProtocol {
    func setupSummaryInfo(_ summary: TransferSummary) {
        valueAddedItem.setup(for: Localizable.transferSummaryValueAdd, itemValue: summary.rechargeValueCurrency)
        valueFinalValueItem.setup(for: Localizable.transferSummaryFinalValue, itemValue: summary.rechargeFinalValueCurrency)
        valueServiceTaxItem.setup(for: Localizable.transferSummaryServiceTax(summary.serviceTaxString),
                                  itemValue: summary.serviceTaxValueCurrency)
    }
    
    func presentError() {
        let popupViewController = PopupViewController(title: Localizable.transferSummaryErrorTitle,
                                                      description: Localizable.transferSummaryErrorDescription,
                                                      preferredType: .image(Resources.Illustrations.iluConstruction.image))

        let tryAction = PopupAction(title: Localizable.transferSummaryErrorButtonTitle, style: .tryAgain) { [weak self] in
            self?.reloadView()
        }
        
        popupViewController.addAction(tryAction)

        showPopup(popupViewController)
    }
    
    func startLoading() {
        startLoadingView()
    }

    func stopLoading() {
        stopLoadingView()
    }
}
