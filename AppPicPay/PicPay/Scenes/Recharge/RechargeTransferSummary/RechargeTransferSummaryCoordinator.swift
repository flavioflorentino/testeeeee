import CustomerSupport
import UIKit

enum RechargeTransferSummaryAction {
    case continuePayment(RechargeFeeInfo)
    case notNow
    case helpCenter(articleId: String)
}

protocol RechargeValueCompletionCoordinating: AnyObject {
    func didCompletePayment(with info: RechargeFeeInfo)
}

protocol RechargeTransferSummaryCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RechargeTransferSummaryAction)
}

final class RechargeTransferSummaryCoordinator {
    private weak var rechargeCoordinator: RechargeValueCompletionCoordinating?
    weak var viewController: UIViewController?

    init(rechargeCoordinator: RechargeValueCompletionCoordinating) {
        self.rechargeCoordinator = rechargeCoordinator
    }
}

// MARK: - RechargeTransferSummaryCoordinating
extension RechargeTransferSummaryCoordinator: RechargeTransferSummaryCoordinating {
    func perform(action: RechargeTransferSummaryAction) {
        switch action {
        case let .continuePayment(feeInfo):
            viewController?.navigationController?.popViewController(animated: false)
            rechargeCoordinator?.didCompletePayment(with: feeInfo)

        case .notNow:
            viewController?.dismiss(animated: true)

        case .helpCenter(let articleId):
            let options: FAQOptions = .article(id: articleId)
            let controller = FAQFactory.make(option: options)
            let navigation = UINavigationController(rootViewController: controller)
            viewController?.navigationController?.present(navigation, animated: true)
        }
    }
}
