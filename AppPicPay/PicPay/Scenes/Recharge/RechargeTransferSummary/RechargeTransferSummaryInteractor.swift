import FeatureFlag
import Foundation

protocol RechargeTransferSummaryInteracting: AnyObject {
    func fetchSummaryInfo()
    func goToHelpCenter()
    func didTapContinue()
    func didTapNotNow()
}

final class RechargeTransferSummaryInteractor {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies

    private let service: RechargeTransferSummaryServicing
    private let presenter: RechargeTransferSummaryPresenting
    private let rechargeValue: Double
    private var rechargeFeeInfo: RechargeFeeInfo?

    init(service: RechargeTransferSummaryServicing,
         presenter: RechargeTransferSummaryPresenting,
         dependencies: Dependencies,
         rechargeValue: Double) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.rechargeValue = rechargeValue
    }
}

private extension RechargeTransferSummaryInteractor {
    func saveRechargeFeeInfo(from summary: TransferSummary) {
        rechargeFeeInfo = RechargeFeeInfo(chargedValue: summary.rechargeFinalValue, serviceFee: summary.serviceTaxValue)
    }
}

// MARK: - RechargeTransferSummaryInteracting
extension RechargeTransferSummaryInteractor: RechargeTransferSummaryInteracting {
    func didTapContinue() {
        guard let feeInfo = rechargeFeeInfo else {
            presenter.presentError()
            return
        }
        presenter.didNextStep(action: .continuePayment(feeInfo))
    }

    func didTapNotNow() {
        presenter.didNextStep(action: .notNow)
    }

    func fetchSummaryInfo() {
        presenter.startLoading()

        service.fetchSummaryInfo(rechargeValue: rechargeValue) { [weak self] result in
            switch result {
            case .success(let transferSummary):
                self?.saveRechargeFeeInfo(from: transferSummary)
                self?.handleSuccess(summary: transferSummary)

            case .failure:
                self?.presenter.presentError()
            }
        }
    }

    func goToHelpCenter() {
        let articleId = dependencies.featureManager.text(.opsHelpRechargeTransactionSummaryString)

        presenter.didNextStep(action: .helpCenter(articleId: articleId))
    }

    private func handleSuccess(summary: TransferSummary) {
        guard let serviceTaxValue = Double(summary.serviceTaxValue),
              serviceTaxValue >= 0,
              let rechargeFinalValue = Double(summary.rechargeFinalValue),
              rechargeFinalValue >= 0,
              summary.serviceTax >= 0,
              summary.rechargeValue >= 0 else {
            presenter.presentError()
            return
        }
        presenter.stopLoading()
        presenter.displaySummary(summary)
    }
}
