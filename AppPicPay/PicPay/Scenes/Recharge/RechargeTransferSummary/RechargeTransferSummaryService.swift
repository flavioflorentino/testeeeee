import Core
import Foundation

protocol RechargeTransferSummaryServicing {
    func fetchSummaryInfo(rechargeValue: Double, completion: @escaping (Result<TransferSummary, ApiError>) -> Void)
}

final class RechargeTransferSummaryService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - RechargeTransferSummaryServicing
extension RechargeTransferSummaryService: RechargeTransferSummaryServicing {
    func fetchSummaryInfo(rechargeValue: Double, completion: @escaping (Result<TransferSummary, ApiError>) -> Void) {
        let api = Api<TransferSummary>(endpoint: RechargeEndpoint.getSummaryTransaction(rechargeValue: rechargeValue))
        api.execute { response in
            self.dependencies.mainQueue.async {
                completion(response.map(\.model))
            }
        }
    }
}

enum RechargeEndpoint {
    case getSummaryTransaction(rechargeValue: Double)
}

extension RechargeEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .getSummaryTransaction(let rechargeValue):
            return "api/servicecharge?value=\(rechargeValue)"
        }
    }
}
