import Core
import Foundation

protocol RechargeTransferSummaryPresenting: AnyObject {
    var viewController: RechargeTransferSummaryDisplaying? { get set }
    func displaySummary(_ summary: TransferSummary)
    func didNextStep(action: RechargeTransferSummaryAction)
    func presentError()
    func startLoading()
    func stopLoading()
}

final class RechargeTransferSummaryPresenter {
    private let coordinator: RechargeTransferSummaryCoordinating
    weak var viewController: RechargeTransferSummaryDisplaying?

    init(coordinator: RechargeTransferSummaryCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - RechargeTransferSummaryPresenting
extension RechargeTransferSummaryPresenter: RechargeTransferSummaryPresenting {
    func presentError() {
        viewController?.presentError()
    }
    
    func displaySummary(_ summary: TransferSummary) {
        viewController?.setupSummaryInfo(summary)
    }
    
    func didNextStep(action: RechargeTransferSummaryAction) {
        coordinator.perform(action: action)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
}
