import UIKit

enum RechargeTransferSummaryFactory {
    static func make(rechargeValue: Double, rechargeCoordinator: RechargeValueCompletionCoordinating) -> RechargeTransferSummaryViewController {
        let container = DependencyContainer()
        let service: RechargeTransferSummaryServicing = RechargeTransferSummaryService(dependencies: container)
        let coordinator: RechargeTransferSummaryCoordinating = RechargeTransferSummaryCoordinator(rechargeCoordinator: rechargeCoordinator)
        let presenter: RechargeTransferSummaryPresenting = RechargeTransferSummaryPresenter(coordinator: coordinator)
        let interactor = RechargeTransferSummaryInteractor(service: service, presenter: presenter, dependencies: container, rechargeValue: rechargeValue)
        let viewController = RechargeTransferSummaryViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
