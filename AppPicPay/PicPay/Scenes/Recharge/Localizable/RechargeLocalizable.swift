import Foundation
enum RechargeLocalizable: String, Localizable {
    case title
    case walletBalance
    case paymentBalance
    case balanceAvailable
    case paymentMethods
    case debitRegistered
    case changeCard
    case addCard
    case questionAdd
    case mainCard
    case errorValue
    case addDebitTitle
    case addDebitDesc
    case debitConfirm
    case debitCancel
    case debitTransfer
    case billTransfer
    case bankTransfer
    case addMoney
    case addBill
    case errorDebit
    case optionsBill
    case cancelRecharge
    case value
    case dueDate
    case tapCopy
    case cancelRechargeMessage
    case cancelRechargeTitle
    case confirmCancelRecharge
    case noCancelRecharge
    case linkBill
    case copyBillNumber
    case printBill
    case lookBill
    case shareBill
    case copyCodeBill
    case signUpOriginal
    case footerOriginal
    case highlightOriginal
    case titlePage1Original
    case descPage1Original
    case titlePage2Original
    case descPage2Original
    case titleHeaderBank
    case highlightHeaderBank
    case headerBank
    case headerBankExtra
    case wishCancel
    case yesCancel
    case noCancel
    case takePicture
    case chooseLibrary
    case resubmitPhoto
    case sendPhoto
    case attention
    case askResubmit
    case successSending
    case descOriginal
    case choosePayment
    
    case cardField
    case valueField
    case agencyField
    case accountField
    case cnpjField
    case identifierField
    
    case backToStart
    case knowMore
    case statusSuccessTitle
    case statusSuccessDescription
    case statusFailureTitle
    case statusFailureDescription
    case warningCreditCard
    case invalidCodeMessage
    case emitterErrorMessage
    
    case copiedField
    
    var key: String {
        self.rawValue
    }
    
    var file: LocalizableFile {
        .recharge
    }
}
