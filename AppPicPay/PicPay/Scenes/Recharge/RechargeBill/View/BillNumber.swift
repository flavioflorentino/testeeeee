import UI
import UIKit

protocol BillNumberDelegate: AnyObject {
    func didTapView()
}

class BillNumber: UIView {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale600.color
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var button: UIButton = {
        let button = UIButton()
        button.setTitle(RechargeLocalizable.tapCopy.text, for: .normal)
        button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        button.isUserInteractionEnabled = false
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    private lazy var lineView: UIView = {
        let view = UIButton()
        view.backgroundColor = Palette.ppColorGrayscale100.color
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    var billNumber: String = "" {
        didSet {
            titleLabel.text = billNumber
        }
    }
    
    var frameNumber: CGRect {
        titleLabel.frame
    }
    
    weak var delegate: BillNumberDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = Palette.ppColorGrayscale000.color
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapView))
        addGestureRecognizer(tap)
    }
    
    private func addComponents() {
        addSubview(titleLabel)
        addSubview(button)
        addSubview(lineView)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20)
        ])
        
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            button.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            button.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            lineView.topAnchor.constraint(equalTo: button.bottomAnchor, constant: 20),
            lineView.bottomAnchor.constraint(equalTo: bottomAnchor),
            lineView.leadingAnchor.constraint(equalTo: leadingAnchor),
            lineView.trailingAnchor.constraint(equalTo: trailingAnchor),
            lineView.heightAnchor.constraint(equalToConstant: 1.0)
        ])
    }
    
    @objc
    private func didTapView() {
        delegate?.didTapView()
    }
}
