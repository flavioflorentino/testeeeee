import UI
import UIKit

protocol BillActionDelegate: AnyObject {
    func didTapOptions()
    func didTapCancel()
}

class BillAction: UIView {
    private let heightButton: CGFloat = 48
    
    private var titleAttributed: [NSAttributedString.Key: Any] = {
        let font = UIFont.systemFont(ofSize: 14.0)
        let attributes: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: Palette.ppColorGrayscale400.color]
        
        return attributes
    }()
    
    private var highlightAttributed: [NSAttributedString.Key: Any] = {
        let font = UIFont.systemFont(ofSize: 14.0, weight: .semibold)
        let attributes: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: Palette.ppColorGrayscale400.color]
        
        return attributes
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var optionsButton: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = heightButton / 2
        button.addTarget(self, action: #selector(didTapOptions), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    private lazy var cancelButton: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = heightButton / 2
        button.addTarget(self, action: #selector(didTapCancel), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false

        return button
    }()
    
    weak var delegate: BillActionDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = Palette.ppColorGrayscale000.color
        optionsButton.configure(with: Button(title: RechargeLocalizable.optionsBill.text))
        cancelButton.configure(with: Button(title: RechargeLocalizable.cancelRecharge.text, type: .cleanDestructive))
    }
    
    private func addComponents() {
        addSubview(messageLabel)
        addSubview(optionsButton)
        addSubview(cancelButton)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            messageLabel.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            messageLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20)
        ])
        
        NSLayoutConstraint.activate([
            optionsButton.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 30),
            optionsButton.leadingAnchor.constraint(equalTo: messageLabel.leadingAnchor),
            optionsButton.trailingAnchor.constraint(equalTo: messageLabel.trailingAnchor),
            optionsButton.heightAnchor.constraint(equalToConstant: heightButton)
        ])

        NSLayoutConstraint.activate([
            cancelButton.topAnchor.constraint(equalTo: optionsButton.bottomAnchor, constant: 10),
            cancelButton.leadingAnchor.constraint(equalTo: messageLabel.leadingAnchor),
            cancelButton.trailingAnchor.constraint(equalTo: messageLabel.trailingAnchor),
            cancelButton.heightAnchor.constraint(equalToConstant: heightButton),
            cancelButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -30)
        ])
    }
    
    @objc
    private func didTapOptions() {
        delegate?.didTapOptions()
    }
    
    @objc
    private func didTapCancel() {
        delegate?.didTapCancel()
    }
    
    func setUp(with instructions: String) {
        let font = Typography.bodyPrimary().font()
        let attributedString = NSAttributedString(string: instructions, attributes: [.font: font])
        messageLabel.attributedText = attributedString.boldfyWithSystemFont(ofSize: font.pointSize, weight: UIFont.Weight.bold.rawValue)
    }
    
    func startLoading(action: Action) {
        guard action == .options else {
            cancelButton.startLoadingAnimating(style: .gray)
            optionsButton.isEnabled = false
            return
        }

        optionsButton.startLoadingAnimating()
        cancelButton.isEnabled = false
    }
    
    func stopLoading() {
        optionsButton.stopLoadingAnimating()
        cancelButton.stopLoadingAnimating()
    }
}

extension BillAction {
    enum Action {
        case cancel
        case options
    }
}
