import UI
import UIKit

class BillValue: UIView {
    private lazy var stackValue: UIStackView = {
        let stack = UIStackView()
        stack.distribution = .fill
        stack.axis = .horizontal
        stack.spacing = 2
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        return stack
    }()
    
    private lazy var stackDate: UIStackView = {
        let stack = UIStackView()
        stack.distribution = .fill
        stack.axis = .horizontal
        stack.spacing = 2
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        return stack
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = UIFont.systemFont(ofSize: 14)
        
        return label
    }()
    
    private lazy var dueDateLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = UIFont.systemFont(ofSize: 14)
        
        return label
    }()
    
    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale600.color
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        
        return label
    }()
    
    private lazy var currencyLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale600.color
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        
        return label
    }()
    
    private lazy var lineView: UIView = {
        let view = UIButton()
        view.backgroundColor = Palette.ppColorGrayscale100.color
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    var value: String = "" {
        didSet {
            currencyLabel.text = value
        }
    }
    
    var date: String = "" {
        didSet {
            dateLabel.text = date
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = Palette.ppColorGrayscale000.color
        valueLabel.text = RechargeLocalizable.value.text
        dueDateLabel.text = RechargeLocalizable.dueDate.text
    }
    
    private func addComponents() {
        stackValue.addArrangedSubview(valueLabel)
        stackValue.addArrangedSubview(currencyLabel)
        stackDate.addArrangedSubview(dueDateLabel)
        stackDate.addArrangedSubview(dateLabel)
        
        addSubview(stackValue)
        addSubview(stackDate)
        addSubview(lineView)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            stackValue.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            stackValue.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            stackDate.topAnchor.constraint(equalTo: stackValue.bottomAnchor, constant: 5),
            stackDate.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            lineView.topAnchor.constraint(equalTo: stackDate.bottomAnchor, constant: 20),
            lineView.bottomAnchor.constraint(equalTo: bottomAnchor),
            lineView.leadingAnchor.constraint(equalTo: leadingAnchor),
            lineView.trailingAnchor.constraint(equalTo: trailingAnchor),
            lineView.heightAnchor.constraint(equalToConstant: 1.0)
        ])
    }
}
