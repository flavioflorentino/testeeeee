import FeatureFlag
import Foundation
import CoreLegacy

protocol RechargeBillViewModelInputs {
    var billNumber: String { get }
    var billValue: String { get }
    var instructions: String { get }
    var billDate: String { get }
    var shouldAddCloseButton: Bool { get }
    
    func printBill()
    func openUrlBill()
    func shareItems() -> [Any]
    func cancelRecharge()
    func close()
}

protocol RechargeBillViewModelOutputs: AnyObject {
    func didNextStep(action: RechargeBillAction)
    func didReceiveAnError(error: PicPayErrorDisplayable)
    func didReceiveAnData(data: Data)
}

protocol RechargeBillViewModelType: AnyObject {
    var inputs: RechargeBillViewModelInputs { get }
    var outputs: RechargeBillViewModelOutputs? { get set }
}

final class RechargeBillViewModel: RechargeBillViewModelType, RechargeBillViewModelInputs {
    typealias Dependencies = HasFeatureManager
    private var dependencies: Dependencies
    
    var inputs: RechargeBillViewModelInputs { self }
    weak var outputs: RechargeBillViewModelOutputs?

    private let service: RechargeBillServicing
    private let model: Recharge
    
    var billDate: String {
        model.instructions.billValidity ?? ""
    }
    
    var billNumber: String {
        model.billCode ?? ""
    }
    
    var billValue: String {
        let value = NSDecimalNumber(string: model.value)
        return CurrencyFormatter.brazillianRealString(from: value)
    }
    
    var instructions: String {
        model.instructions.text ?? ""
    }
    
    var shouldAddCloseButton: Bool {
        !isCashInRedesignAvailable
    }
    
    init(service: RechargeBillServicing, recharge: Recharge, dependencies: Dependencies) {
        self.service = service
        self.model = recharge
        self.dependencies = dependencies
    }
    
    func close() {
        let action: RechargeBillAction = isCashInRedesignAvailable ? .popToRoot : .close
        outputs?.didNextStep(action: action)
    }
    
    func cancelRecharge() {
        let id = model.rechargeId
        let successAction: RechargeBillAction = isCashInRedesignAvailable ? .popToRoot : .cancelRecharge
        service.cancel(id: id,
                       onSuccess: { [weak self] in
            self?.outputs?.didNextStep(action: successAction)
        }, onError: { [weak self] error in
            self?.outputs?.didReceiveAnError(error: error)
        })
    }
    
    func printBill() {
        guard let billUrl = model.billUrl,
            let url = URL(string: billUrl + "=pdf") else {
            return
        }
        
        service.downloadBill(url: url,
                             onSuccess: { [weak self] data in
            self?.outputs?.didReceiveAnData(data: data)
        }, onError: { [weak self] error in
            self?.outputs?.didReceiveAnError(error: error)
        })
    }
    
    func shareItems() -> [Any] {
        guard let billUrl = model.billUrl,
            let url = URL(string: billUrl + "=pdf") else {
                return []
        }
        var message = RechargeLocalizable.shareBill.text.replacingOccurrences(of: "&value&", with: "\(billValue)")
        message = message.replacingOccurrences(of: "&number&", with: "\(billNumber)")
       
        return [message, url]
    }
    
    func openUrlBill() {
        guard let billUrl = model.billUrl else {
            return
        }
        
        let url = billUrl + "=pdf"
        outputs?.didNextStep(action: .openPDF(url))
    }
}

private extension RechargeBillViewModel {
    var isCashInRedesignAvailable: Bool {
        dependencies.featureManager.isActive(.isCashInRedesignAvailable)
    }
}
