import UIKit

enum RechargeBillAction {
    case openPDF(String)
    case cancelRecharge
    case close
    case popToRoot
}

protocol RechargeBillCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: RechargeBillAction)
}

final class RechargeBillCoordinator: RechargeBillCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: RechargeBillAction) {
        switch action {
        case .openPDF(let urlString):
            guard let url = URL(string: urlString) else {
                return
            }
            let webViewController = WebViewFactory.make(with: url)
            viewController?.navigationController?.pushViewController(webViewController, animated: true)
            
        case .cancelRecharge:
            let controller = RechargeLoadFactory.make()
            viewController?.navigationController?.pushViewController(controller, animated: true)
            
        case .close:
            viewController?.dismiss(animated: true)
            
        case .popToRoot:
            viewController?.navigationController?.popToRootViewController(animated: true)
        }
    }
}
