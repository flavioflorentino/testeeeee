import AMPopTip
import UI
import UIKit

final class RechargeBillViewController: LegacyViewController<RechargeBillViewModelType, RechargeBillCoordinating, UIView> {
    private lazy var popTip: PopTip = {
        let popTip = PopTip()
        popTip.bubbleColor = Palette.ppColorNeutral400.color
        popTip.padding = 10
        popTip.offset = 10
        
        return popTip
    }()
    
    private lazy var billNumber: BillNumber = {
        let view = BillNumber()
        view.delegate = self
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var billValue: BillValue = {
        let view = BillValue()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var billAction: BillAction = {
        let view = BillAction()
        view.delegate = self
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateView()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(billNumber)
        view.addSubview(billValue)
        view.addSubview(billAction)
    }
    
    override func configureViews() {
        title = RechargeLocalizable.addMoney.text
        if viewModel.inputs.shouldAddCloseButton {
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: DefaultLocalizable.btClose.text,
                                                               style: .plain,
                                                               target: self,
                                                               action: #selector(clickLeftButton))
        }
        
        view.backgroundColor = Palette.ppColorGrayscale100.color
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.activate([
            billNumber.topAnchor.constraint(equalTo: view.topAnchor),
            billNumber.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            billNumber.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])

        NSLayoutConstraint.activate([
            billValue.topAnchor.constraint(equalTo: billNumber.bottomAnchor),
            billValue.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            billValue.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])

        NSLayoutConstraint.activate([
            billAction.topAnchor.constraint(equalTo: billValue.bottomAnchor),
            billAction.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            billAction.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
    
    private func updateView() {
        billNumber.billNumber = viewModel.inputs.billNumber
        billValue.value = viewModel.inputs.billValue
        billValue.date = viewModel.inputs.billDate
        billAction.setUp(with: viewModel.inputs.instructions)
    }
    
    private func showOptions() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        let bill = UIAlertAction(title: RechargeLocalizable.lookBill.text, style: .default) { [weak self] _ in
            self?.viewModel.inputs.openUrlBill()
        }
        
        let print = UIAlertAction(title: RechargeLocalizable.printBill.text, style: .default) { [weak self] _ in
            self?.billAction.startLoading(action: .options)
            self?.viewModel.inputs.printBill()
        }
 
        let copy = UIAlertAction(title: RechargeLocalizable.copyBillNumber.text, style: .default) { [weak self] _ in
            self?.copyBillNumber()
        }
        
        let share = UIAlertAction(title: RechargeLocalizable.linkBill.text, style: .default) { [weak self] _ in
            self?.share()
        }
        
        let cancel = UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel)

        actionSheet.addAction(bill)
        actionSheet.addAction(print)
        actionSheet.addAction(copy)
        actionSheet.addAction(share)
        actionSheet.addAction(cancel)
        
        present(actionSheet, animated: true)
    }
    
    private func cancelRecharge() {
        let alert = Alert(title: RechargeLocalizable.cancelRechargeTitle.text, text: RechargeLocalizable.cancelRechargeMessage.text)
    
        let buttonConfirm = Button(title: RechargeLocalizable.confirmCancelRecharge.text, type: .destructive) { [weak self] controller, _ in
            self?.viewModel.inputs.cancelRecharge()
            self?.billAction.startLoading(action: .cancel)
            controller.dismiss(animated: false)
        }
        let buttonCancel = Button(title: RechargeLocalizable.noCancelRecharge.text, type: .clean, action: .close)
        alert.buttons = [buttonConfirm, buttonCancel]
        
        AlertMessage.showAlert(alert, controller: self)
    }
    
    private func share() {
        let activityController = UIActivityViewController(activityItems: viewModel.inputs.shareItems(), applicationActivities: nil)
        present(activityController, animated: true)
    }
    
    private func copyBillNumber() {
        UIPasteboard.general.string = viewModel.inputs.billNumber
        popTip.show(text: RechargeLocalizable.copyCodeBill.text, direction: .down, maxWidth: 200, in: view, from: billNumber.frameNumber, duration: 1.5)
    }
    
    private func presentPrintPdf(with data: Data) {
        let printInfo = UIPrintInfo.printInfo()
        printInfo.outputType = .general
        let printController = UIPrintInteractionController.shared
        printController.printInfo = printInfo
        printController.printingItem = data
        printController.present(animated: true)
    }
    
    @objc
    private func clickLeftButton() {
        viewModel.inputs.close()
    }
}

// MARK: View Model Outputs
extension RechargeBillViewController: RechargeBillViewModelOutputs {
    func didReceiveAnData(data: Data) {
        billAction.stopLoading()
        presentPrintPdf(with: data)
    }
    
    func didNextStep(action: RechargeBillAction) {
        coordinator.perform(action: action)
    }
    
    func didReceiveAnError(error: PicPayErrorDisplayable) {
        billAction.stopLoading()
        AlertMessage.showCustomAlertWithError(error, controller: self)
    }
}

extension RechargeBillViewController: BillActionDelegate {
    func didTapOptions() {
        showOptions()
    }
    
    func didTapCancel() {
        cancelRecharge()
    }
}

extension RechargeBillViewController: BillNumberDelegate {
    func didTapView() {
        copyBillNumber()
    }
}
