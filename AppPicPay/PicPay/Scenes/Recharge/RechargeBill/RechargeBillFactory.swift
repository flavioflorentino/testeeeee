import Foundation

enum RechargeBillFactory {
    static func make(recharge: Recharge) -> RechargeBillViewController {
        let container = DependencyContainer()
        let service: RechargeBillServicing = RechargeBillService(dependencies: container)
        let viewModel: RechargeBillViewModelType = RechargeBillViewModel(service: service,
                                                                         recharge: recharge,
                                                                         dependencies: container)
        var coordinator: RechargeBillCoordinating = RechargeBillCoordinator()
        let viewController = RechargeBillViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController

        return viewController
    }
}
