import Core
import Foundation

protocol RechargeBillServicing {
    func cancel(id: String, onSuccess: @escaping() -> Void, onError: @escaping(PicPayErrorDisplayable) -> Void)
    func downloadBill(url: URL, onSuccess: @escaping(Data) -> Void, onError: @escaping(PicPayErrorDisplayable) -> Void)
}

final class RechargeBillService: RechargeBillServicing {
    typealias Dependencies = HasConsumerManager & HasMainQueue
    private var dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func cancel(id: String, onSuccess: @escaping() -> Void, onError: @escaping(PicPayErrorDisplayable) -> Void) {
        let mainQueue = dependencies.mainQueue
        
        WSConsumer.cancelRecharge(id) { _, error in
            mainQueue.async {
                guard let error = error else {
                    onSuccess()
                    return
                }
                let picpayError = PicPayError(message: error.localizedDescription)
                onError(picpayError)
            }
        }
    }
    
    func downloadBill(url: URL, onSuccess: @escaping(Data) -> Void, onError: @escaping(PicPayErrorDisplayable) -> Void) {
        let mainQueue = dependencies.mainQueue
        
        DispatchQueue.global().async {
            guard let data = try? Data(contentsOf: url) else {
                mainQueue.async {
                    let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
                    onError(error)
                }
                return
            }
            mainQueue.async {
                onSuccess(data)
            }
        }
    }
}
