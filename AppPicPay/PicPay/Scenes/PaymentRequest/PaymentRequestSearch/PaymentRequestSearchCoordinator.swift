import UI

enum PaymentRequestSearchAction {
    case forward(users: [PaymentRequestSearchViewModel.SelectedUser])
}

protocol PaymentRequestSearchCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: PaymentRequestSearchAction)
}

final class PaymentRequestSearchCoordinator: PaymentRequestSearchCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: PaymentRequestSearchAction) {
        if case let .forward(users) = action {
            let controller = PaymentRequestDetailFactory.make(
                origin: PaymentRequestUserEvent.PaymentRequestDetailOrigin.userListing.rawValue,
                users: users
            )
            viewController?.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
