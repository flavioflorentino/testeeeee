import Core

enum PaymentRequestSearchEndpoint {
    case search(name: String)
    case contacts(page: Int)
}

extension PaymentRequestSearchEndpoint.Parameters {
    static let term = "term"
    static let page = "page"
    static let group = "group"
    
    static let consumers = "CONSUMERS"
    static let contacts = "CONTACTS"
}

extension PaymentRequestSearchEndpoint: ApiEndpointExposable {
    fileprivate enum Parameters { }
    
    var path: String {
         "search"
    }
    
    var method: HTTPMethod {
        .get
    }
    
    var parameters: [String: Any] {
        switch self {
        case let .search(name):
            return [Parameters.term: name, Parameters.group: Parameters.consumers]
        case let .contacts(page):
            return [Parameters.page: page, Parameters.group: Parameters.contacts]
        }
    }
}
