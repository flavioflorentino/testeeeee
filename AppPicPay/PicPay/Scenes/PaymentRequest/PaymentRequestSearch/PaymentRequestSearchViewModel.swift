import AnalyticsModule
import Core
import UI

protocol PaymentRequestSearchViewModelInputs: AnyObject {
    func registerDidOpenSearchScreenAnalytics()
    func registerDidTapSearchField()
    func startContactsRequest()
    func searchUser(named name: String, useDelayIfNeeded: Bool)
    func searchMore()
    func addSelectionGrouper(user: UserListComponentModel)
    func removeSelectionGrouper(user: UserListComponentModel)
    func removeSelectionList(user: GroupedUser)
    func blockedToSelectANewUser(maxUsers: Int)
    func forward()
    func requestContactsPermission()
}

final class PaymentRequestSearchViewModel {
    typealias User = (url: URL?, name: String)
    typealias BruteUser = (user: PaymentRequestSearchUser, isSelected: Bool)
    typealias SelectedUser = (id: String, name: String, url: URL?)
    typealias Dependencies = HasMainQueue & HasAnalytics
    
    private enum PaymentRequestSearchDataType {
        case contacts
        case users
    }
    
    // MARK: - Variables
    private let service: PaymentRequestSearchServicing
    private let presenter: PaymentRequestSearchPresenting
    private var timer: Timer?
    private var task: URLSessionTask?
    private var contactsPage = 0
    private var isRequestingContacts = false
    private var isRequestingNewPage = false
    private var currentType: PaymentRequestSearchDataType = .contacts
    private var hasNextPageContacts = true
    private var selectedUserIds: [String: User] = [:]
    private var contacts: [BruteUser] = []
    private var needsToIdentifyContacts = true
    private let timerType: Timer.Type
    private let contactsManager: ContactsProtocol
    private let contactsPermission: PaymentRequestContactsPermission
    private let dependencies: Dependencies
    
    // MARK: - Life Cycle
    init(
        service: PaymentRequestSearchServicing,
        presenter: PaymentRequestSearchPresenting,
        timerType: Timer.Type = Timer.self,
        contactsManager: ContactsProtocol = Contacts(),
        contactsPermission: PaymentRequestContactsPermission = PaymentRequestContactsPermissionHelper(),
        dependencies: Dependencies = DependencyContainer()
    ) {
        self.service = service
        self.presenter = presenter
        self.timerType = timerType
        self.contactsManager = contactsManager
        self.contactsPermission = contactsPermission
        self.dependencies = dependencies
    }
}

// MARK: - PaymentRequestSearchViewModelInputs
extension PaymentRequestSearchViewModel: PaymentRequestSearchViewModelInputs {
    func registerDidOpenSearchScreenAnalytics() {
        dependencies.analytics.log(PaymentRequestUserEvent.didOpenSearchScreen)
    }
    
    func registerDidTapSearchField() {
        dependencies.analytics.log(PaymentRequestUserEvent.didTapSearchField)
    }
    
    func requestContactsPermission() {
        contactsPermission.requestContacts { [weak self] check in
            guard .authorized == check else {
                return
            }
            self?.identifyPicpayContacts()
        }
    }
    
    func startContactsRequest() {
        guard .authorized == contactsPermission.status else {
            presenter.presentRequestContactPermission()
            return
        }
        isRequestingContacts = true
        currentType = .contacts
        needsToIdentifyContacts ? identifyPicpayContacts() : showContacts()
    }
    
    func searchUser(named name: String, useDelayIfNeeded: Bool) {
        guard name.count > 2 else {
            if currentType == .users {
                cancelRequest()
            }
            if isRequestingContacts == false {
                startContactsRequest()
            }
            return
        }
        cancelRequest()
        
        currentType = .users
        
        useDelayIfNeeded ? createUsersSearchTimer(with: name) : requestUsers(withName: name)
    }
    
    private func showContacts() {
        guard contacts.isEmpty else {
            contacts = verifySelections(users: contacts.map { $0.user })
            presenter.presentUsers(contacts, title: PaymentRequestLocalizable.contacts.text)
            isRequestingContacts = false
            return
        }
        guard hasNextPageContacts else {
            handleContactsEmptyState()
            isRequestingContacts = false
            return
        }
        presenter.presentLoading()
        requestContacts()
    }
    
    private func searchMoreContacts() {
        guard
            isRequestingNewPage == false,
            needsToIdentifyContacts == false,
            hasNextPageContacts
            else {
                return
        }
        isRequestingNewPage = true
        requestContacts()
    }
    
    func searchMore() {
        guard currentType == .contacts else {
            return
        }
        searchMoreContacts()
    }
    
    private func cancelRequest() {
        task?.cancel()
        timer?.invalidate()
        isRequestingNewPage = false
    }
    
    private func createUsersSearchTimer(with name: String) {
        timer = timerType.scheduledTimer(withTimeInterval: 0.5, repeats: false) { [weak self] _ in
            self?.requestUsers(withName: name)
        }
    }
    
    private func requestContacts() {
        if contactsPage > 0 {
            presenter.presentListLoading()
        }
        service.contacts(page: contactsPage) { [weak self] response in
            guard let self = self else {
                return
            }
            self.isRequestingNewPage = false
            self.isRequestingContacts = false
            self.presenter.hideListLoading()
            switch response {
            case let .success(response):
                self.requestContactsSuccess(response: response)
            case .failure:
                guard
                    self.contactsPage == 0,
                    self.currentType == .contacts
                    else {
                        return
                }
                self.presenter.presentError()
            }
        }
    }
    
    private func requestUsers(withName name: String) {
        presenter.presentLoading()
        task = service.searchUser(named: name) { [weak self] response in
            self?.isRequestingNewPage = false
            switch response {
            case let .success(response) :
                self?.requestUsersSuccess(response: response)
            case .failure:
                self?.presenter.presentError()
            }
        }
    }
    
    private func requestUsersSuccess(response: (model: [PaymentRequestSearchResponse], data: Data?)) {
        guard let newUsers = response.model.first?.rows, newUsers.isNotEmpty else {
            handleUsersEmptyState()
            return
        }
        let users = verifySelections(users: newUsers)
        presenter.presentUsers(users, title: PaymentRequestLocalizable.results.text)
    }
    
    private func requestContactsSuccess(response: (model: [PaymentRequestSearchResponse], data: Data?)) {
        guard let newUsers = response.model.first?.rows, newUsers.isNotEmpty else {
            hasNextPageContacts = false
            if contactsPage == 0 {
                handleContactsEmptyState()
            }
            return
        }
        contactsPage += 1
        contacts.append(contentsOf: verifySelections(users: newUsers))
        if currentType == .contacts {
            presenter.presentUsers(contacts, title: PaymentRequestLocalizable.contacts.text)
        }
    }
    
    private func handleUsersEmptyState() {
        presenter.presentEmptyStateWithMessage(
            PaymentRequestLocalizable.usersListEmptyState.text,
            title: nil
        )
    }
    
    private func handleContactsEmptyState() {
        presenter.presentEmptyStateWithMessage(
            PaymentRequestLocalizable.contactsListEmptyState.text,
            title: PaymentRequestLocalizable.contacts.text
        )
    }
    
    private func verifySelections(users: [PaymentRequestSearchUser]) -> [BruteUser] {
        users.map { user -> BruteUser in
            let isSelected = selectedUserIds[user.data.id] != nil
            return (user: user, isSelected: isSelected)
        }
    }
    
    private func identifyPicpayContacts() {
        presenter.presentLoading()
        contactsManager.identifyPicpayContacts { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .receivedContactsFromServer, .usedContactsFromCache:
                    self?.needsToIdentifyContacts = false
                    self?.showContacts()
                default:
                    self?.needsToIdentifyContacts = true
                    self?.presenter.presentError()
                }
            }
        }
    }
        
    func addSelectionGrouper(user: UserListComponentModel) {
        if selectedUserIds.isEmpty {
            presenter.presentEnableForwardButton()
        }
        selectedUserIds[user.id] = (user.urlImage, user.username)
        presenter.presentSelectionGrouper(user: user)
    }
    
    func removeSelectionGrouper(user: UserListComponentModel) {
        handleDeselection(id: user.id)
        presenter.presentDeselectionGrouper(userId: user.id)
    }
    
    func removeSelectionList(user: GroupedUser) {
        handleDeselection(id: user.id)
        presenter.presentDeselectionList(userId: user.id)
    }
    
    private func handleDeselection(id: String) {
        selectedUserIds.removeValue(forKey: id)
        if selectedUserIds.isEmpty {
            presenter.presentDisableForwardButton()
        }
    }
    
    func blockedToSelectANewUser(maxUsers: Int) {
        presenter.presentBlockedToSelectANewUser(maxUsers: maxUsers)
    }
    
    func forward() {
        guard selectedUserIds.isNotEmpty else {
            return
        }
        dependencies.analytics.log(PaymentRequestUserEvent.selectedUsers(ids: selectedUserIds.map { $0.key }))
        let selectedUsers = selectedUserIds.map { (id: $0, name: $1.name, url: $1.url) }
        presenter.didNextStep(action: .forward(users: selectedUsers))
    }
}
