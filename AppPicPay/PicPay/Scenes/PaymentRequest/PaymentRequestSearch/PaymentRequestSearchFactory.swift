import AnalyticsModule
import FeatureFlag
import Foundation

final class PaymentRequestSearchFactory {
    typealias Dependencies = HasFeatureManager & HasAnalytics
    // MARK: - Variables
    private let dependencies: Dependencies
    
    // MARK: - Life Cycle
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    // MARK: - make
    func make(maxUsers: Int? = nil) -> UIViewController {
        guard
            let maxUsers = maxUsers,
            dependencies.featureManager.isActive(.paymentRequestV2)
            else {
                return createOldScene()
        }
        return createNewScene(maxUsers: maxUsers)
    }
    
    private func createOldScene() -> UIViewController {
        let searchWrapper = SearchWrapper(ignorePermissionPrompt: true, dependencies: dependencies)
        searchWrapper.setTabs(
            tabs: [.consumers],
            isOnlySearchEnabled: true
        )
        searchWrapper.showContainerSearch = true
        searchWrapper.setSearchPlaceHolder(PaymentRequestLocalizable.paymentRequestSearchPlaceholder.text)
        searchWrapper.isPersonRequestingPayment = true
        return searchWrapper
    }
    
    private func createNewScene(maxUsers: Int) -> UIViewController {
        let container = DependencyContainer()
        let coordinator = PaymentRequestSearchCoordinator()
        let presenter = PaymentRequestSearchPresenter(coordinator: coordinator)
        let service = PaymentRequestSearchService(dependencies: container)
        let viewModel = PaymentRequestSearchViewModel(service: service, presenter: presenter)
        let controller = PaymentRequestSearchViewController(viewModel: viewModel, maxUsers: maxUsers)
        presenter.viewController = controller
        coordinator.viewController = controller
        return controller
    }
}
