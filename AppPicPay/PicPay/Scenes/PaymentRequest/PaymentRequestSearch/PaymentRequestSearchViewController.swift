import UI

protocol PaymentRequestSearchDisplay: AnyObject {
    func displayUsers(_ users: [UserListComponentModel], title: String?)
    func displayRequestContactPermission()
    func displayEmptyStateWithMessage(_ message: String, title: String?)
    func displaySelectionGrouper(user: GroupedUser)
    func displayDeselectionGrouper(userId: String)
    func displayDeselectionList(userId: String)
    func displayBlockedToSelectANewUser(title: String, message: String, buttonTitle: String)
    func displayLoading()
    func displayListLoading()
    func hideListLoading()
    func displayErrorWith(message: String, buttonText: String)
    func displayEnableForwardButton()
    func displayDisableForwardButton()
}

extension PaymentRequestSearchViewController.Layout {
    enum Size {
        static let searchTextFieldHeight: CGFloat = 40.0
        static let buttonHeight: CGFloat = 48.0
        static let lineHeight: CGFloat = 1.0
        static let stackViewHeader = CGSize(width: UIScreen.main.bounds.width, height: 119)
    }
}

final class PaymentRequestSearchViewController: ViewController<PaymentRequestSearchViewModelInputs, UIView> {
    fileprivate struct Layout {}
    
    // MARK: - Visual Components
    private lazy var searchTextField: SearchComponentTextField = {
        let view = SearchComponentTextFieldFactory.make()
        view.backgroundColor = Colors.backgroundPrimary.color
        view.addTarget(self, action: #selector(searchEditingDidBegin), for: .editingDidBegin)
        view.addTarget(self, action: #selector(search), for: .editingChanged)
        return view
    }()
    
    private lazy var stackViewHeader: UIStackView = {
        let frame = CGRect(origin: .zero, size: Layout.Size.stackViewHeader)
        let stackView = UIStackView(frame: frame)
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var userGrouper: UserGrouping = {
        let userGrouper = UserGrouperFactory.make(
            title: PaymentRequestLocalizable.selected.text,
            quantityText: PaymentRequestLocalizable.quantityText.text,
            maxUserQuantity: maxUsers,
            initialUsers: [],
            placeholderImage: PPContact.photoPlaceholder()
        )
        userGrouper.groupingDelegate = self
        return userGrouper
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale050.color
        return view
    }()
    
    private lazy var userListComponentView = UsersListComponentFactory.make(maxUsersSelected: maxUsers, delegate: self)
    
    private lazy var forwardButton: UIPPButton = {
        let button = UIPPButton(title: DefaultLocalizable.advance.text, target: self, action: #selector(forwardAction))
        button.configureButtonCta()
        button.isEnabled = false
        return button
    }()
    
    // MARK: - Variables
    private let maxUsers: Int
    
    // MARK: - Life Cycle
    init(viewModel: PaymentRequestSearchViewModelInputs, maxUsers: Int) {
        self.maxUsers = maxUsers
        super.init(viewModel: viewModel)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.registerDidOpenSearchScreenAnalytics()
        viewModel.startContactsRequest()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(searchTextField)
        view.addSubview(userListComponentView)
        view.addSubview(forwardButton)
        
        stackViewHeader.addArrangedSubview(userGrouper)
        stackViewHeader.addArrangedSubview(lineView)
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.grayscale050.color
        navigationController?.navigationBar.barTintColor = Colors.grayscale050.color
        navigationItem.title = PaymentRequestLocalizable.paymentRequest.text
        userGrouper.backgroundColor = Colors.backgroundPrimary.color
        
        configureKeyboardHideWhenTappingAround()
    }
    
    override func setupConstraints() {
        searchTextField.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.Size.searchTextFieldHeight)
        }
        
        userListComponentView.snp.makeConstraints {
            $0.top.equalTo(searchTextField.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
        }
        
        forwardButton.snp.makeConstraints {
            $0.top.equalTo(userListComponentView.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.Size.buttonHeight)
        }
    }
    
    // MARK: - Private functions
    private func configureKeyboardHideWhenTappingAround() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
}

// MARK: - Actions
@objc
private extension PaymentRequestSearchViewController {
    func search() {
        guard let text = searchTextField.text else {
            return
        }
        viewModel.searchUser(named: text, useDelayIfNeeded: true)
    }
    
    func forwardAction() {
        viewModel.forward()
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func searchEditingDidBegin() {
        viewModel.registerDidTapSearchField()
    }
}

// MARK: PaymentRequestSearchDisplay
extension PaymentRequestSearchViewController: PaymentRequestSearchDisplay {
    func displayUsers(_ users: [UserListComponentModel], title: String?) {
        userListComponentView.displayUsers(users, title: title)
    }
    
    func displayRequestContactPermission() {
        userListComponentView.displayRequestContactPermission()
    }
    
    func displayEmptyStateWithMessage(_ message: String, title: String?) {
        userListComponentView.displayEmptyStateWithMessage(message, title: title)
    }
    
    func displaySelectionGrouper(user: GroupedUser) {
        userGrouper.add(newUser: user)
    }
    
    func displayDeselectionGrouper(userId: String) {
        userGrouper.removeUser(id: userId)
    }
    
    func displayDeselectionList(userId: String) {
        userListComponentView.deselectUser(with: userId)
    }
    
    func displayBlockedToSelectANewUser(title: String, message: String, buttonTitle: String) {
        let button = Button(title: buttonTitle)
        let alert = Alert(
            with: title,
            text: message,
            buttons: [button]
        )
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func displayLoading() {
        userListComponentView.displayLoading()
    }
    
    func displayListLoading() {
        userListComponentView.displayListLoading()
    }
    
    func hideListLoading() {
        userListComponentView.hideListLoading()
    }
    
    func displayErrorWith(message: String, buttonText: String) {
        userListComponentView.displayErrorWith(message: message, buttonText: buttonText)
    }
    
    func displayEnableForwardButton() {
        forwardButton.isEnabled = true
        userListComponentView.tableHeaderView = stackViewHeader
    }
    
    func displayDisableForwardButton() {
        forwardButton.isEnabled = false
        userListComponentView.tableHeaderView = nil
    }
}

// MARK: - UserGrouperDelegate
extension PaymentRequestSearchViewController: UserGrouperDelegate {
    func didRemove(user: GroupedUser) {
        viewModel.removeSelectionList(user: user)
    }
}

// MARK: - UsersListComponentViewModelDelegate
extension PaymentRequestSearchViewController: UsersListComponentViewModelDelegate {
    func blockedToSelectANewUser() {
        viewModel.blockedToSelectANewUser(maxUsers: maxUsers)
    }
    
    func didRemoveUser(_ user: UserListComponentModel) {
        viewModel.removeSelectionGrouper(user: user)
    }
    
    func didAddUser(_ user: UserListComponentModel) {
        viewModel.addSelectionGrouper(user: user)
    }
    
    func didTryAgainOnError() {
        guard let text = searchTextField.text else {
            return
        }
        viewModel.searchUser(named: text, useDelayIfNeeded: false)
    }
    
    func didScrollReachBottomList() {
        viewModel.searchMore()
    }
    
    func didTouchOnRequestPermission() {
        viewModel.requestContactsPermission()
    }
}
