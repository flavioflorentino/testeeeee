import Core
import UI

protocol PaymentRequestSearchPresenting: AnyObject {
    var viewController: PaymentRequestSearchDisplay? { get set }
    func didNextStep(action: PaymentRequestSearchAction)
    func presentUsers(_ users: [PaymentRequestSearchViewModel.BruteUser], title: String?)
    func presentRequestContactPermission()
    func presentEmptyStateWithMessage(_ message: String, title: String?)
    func presentSelectionGrouper(user: UserListComponentModel)
    func presentDeselectionGrouper(userId: String)
    func presentDeselectionList(userId: String)
    func presentBlockedToSelectANewUser(maxUsers: Int)
    func presentLoading()
    func presentListLoading()
    func hideListLoading()
    func presentError()
    func presentEnableForwardButton()
    func presentDisableForwardButton()
}

final class PaymentRequestSearchPresenter {
    // MARK: - Variables
    private let coordinator: PaymentRequestSearchCoordinating
    weak var viewController: PaymentRequestSearchDisplay?

    // MARK: - Life Cycle
    init(coordinator: PaymentRequestSearchCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - PaymentRequestSearchPresenting
extension PaymentRequestSearchPresenter: PaymentRequestSearchPresenting {
    func didNextStep(action: PaymentRequestSearchAction) {
        coordinator.perform(action: action)
    }
    
    func presentUsers(_ users: [PaymentRequestSearchViewModel.BruteUser], title: String?) {
        let userListComponentModel = users.map { createUserModel(data: $0) }
        viewController?.displayUsers(userListComponentModel, title: title)
    }
    
    func presentRequestContactPermission() {
        viewController?.displayRequestContactPermission()
    }
    
    func presentEmptyStateWithMessage(_ message: String, title: String?) {
        viewController?.displayEmptyStateWithMessage(message, title: title)
    }
    
    private func createUserModel(data: PaymentRequestSearchViewModel.BruteUser) -> UserListComponentModel {
        let username = "@\(data.user.data.username)"
        return UserListComponentModel(
            id: data.user.data.id,
            username: username,
            fullName: data.user.data.name,
            urlImage: data.user.data.imageUrlSmall,
            paymentRequestAllowed: data.user.data.config.paymentRequest,
            isSelected: data.isSelected
        )
    }
    
    func presentSelectionGrouper(user: UserListComponentModel) {
        let groupedUser = GroupedUser(id: user.id, imageUrl: user.urlImage, name: user.username)
        viewController?.displaySelectionGrouper(user: groupedUser)
    }
    
    func presentDeselectionGrouper(userId: String) {
        viewController?.displayDeselectionGrouper(userId: userId)
    }
    
    func presentDeselectionList(userId: String) {
        viewController?.displayDeselectionList(userId: userId)
    }
    
    func presentBlockedToSelectANewUser(maxUsers: Int) {
        let title = PaymentRequestLocalizable.limitUsersReachedTitle.text
        let message = String(format: PaymentRequestLocalizable.limitUsersReachedMessage.text, maxUsers)
        let buttonTitle = ExternalTransferLocalizable.okUnderstood.text
        viewController?.displayBlockedToSelectANewUser(title: title, message: message, buttonTitle: buttonTitle)
    }
        
    func presentLoading() {
        viewController?.displayLoading()
    }
    
    func presentListLoading() {
        viewController?.displayListLoading()
    }
    
    func hideListLoading() {
        viewController?.hideListLoading()
    }
    
    func presentError() {
        viewController?.displayErrorWith(
            message: PaymentRequestLocalizable.weCouldntCompleteYourSearch.text,
            buttonText: DefaultLocalizable.tryAgain.text
        )
    }
    
    func presentEnableForwardButton() {
        viewController?.displayEnableForwardButton()
    }
    
    func presentDisableForwardButton() {
        viewController?.displayDisableForwardButton()
    }
}
