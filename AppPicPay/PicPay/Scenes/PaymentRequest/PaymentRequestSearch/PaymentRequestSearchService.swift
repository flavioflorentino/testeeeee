import Core

protocol PaymentRequestSearchServicing {
    func searchUser(
        named name: String,
        completion: @escaping (Result<(model: [PaymentRequestSearchResponse], data: Data?), ApiError>) -> Void
    ) -> URLSessionTask?
    func contacts(
        page: Int,
        completion: @escaping (Result<(model: [PaymentRequestSearchResponse], data: Data?), ApiError>) -> Void
    )
}

final class PaymentRequestSearchService {
    typealias Dependencies = HasMainQueue
    // MARK: - Variables
    private let dependencies: Dependencies
    
    // MARK: - Life Cycle
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - PaymentRequestSearchServicing
extension PaymentRequestSearchService: PaymentRequestSearchServicing {
    func searchUser(named name: String, completion: @escaping (Result<(model: [PaymentRequestSearchResponse], data: Data?), ApiError>) -> Void) -> URLSessionTask? {
        let endpoint = PaymentRequestSearchEndpoint.search(
            name: name
        )
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        
        return Api<[PaymentRequestSearchResponse]>(endpoint: endpoint).execute(
            jsonDecoder: jsonDecoder
        ) { [weak self] result in
            if case let .failure(error) = result,
                case ApiError.cancelled = error {
                return
            }
            self?.dependencies.mainQueue.async {
                completion(result)
            }
        }
    }
    
    func contacts(
        page: Int,
        completion: @escaping (Result<(model: [PaymentRequestSearchResponse], data: Data?), ApiError>) -> Void
    ) {
        let endpoint = PaymentRequestSearchEndpoint.contacts(page: page)
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        
        Api<[PaymentRequestSearchResponse]>(endpoint: endpoint).execute(
            jsonDecoder: jsonDecoder
        ) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result)
            }
        }
    }
}
