import CorePayment
import UI

protocol PaymentRequestToolbarDisplay: AnyObject {
    func displayPrivacy(text: String, image: UIImage, tintColor: UIColor)
    func displayPaymentMethod(method: String, mainImage: UIImage, secondaryImage: UIImage?, secondaryImageIsHidden: Bool)
    func hidePaymentMethod()
    func displayPrivacySelection(alertTitle: String, alertMessage: String, buttonCancel: String, buttonPublic: String, buttonPrivate: String)
    func displayQuestioningDefaultPrivacySelection(alertTitle: String, alertMessage: String, buttonCancel: String, buttonOk: String)
}

// MARK: - Layout
extension PaymentRequestToolbar.Layout {
    enum Size {
        static let sendButtonHeight: CGFloat = 40
        static let sendButtonWidth: CGFloat = 110
        static let imageSide: CGFloat = 24
    }
    
    enum Font {
        static let large = UIFont.systemFont(ofSize: 14, weight: .medium)
        static let medium = UIFont.systemFont(ofSize: 12)
    }
}

final class PaymentRequestToolbar: UIToolbar {
    // MARK: - Layout
    fileprivate enum Layout {}
    
    // MARK: - Visual Components
    private lazy var sendButton: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = Layout.Size.sendButtonHeight / 2
        button.setTitleColor(Palette.white.color, for: .normal)
        button.backgroundColor = Palette.ppColorBranding300.color
        button.addTarget(self, action: #selector(didTouchOnSendButton), for: .touchUpInside)
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: Spacing.base01, bottom: 0, right: Spacing.base01)
        return button
    }()
    
    private lazy var privacyImageView = UIImageView()
    private lazy var mainPaymentMethodImageView = UIImageView()
    private lazy var secondaryPaymentMethodImageView = UIImageView()
    
    private lazy var privacyLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.large
        label.textColor = Palette.ppColorGrayscale400.color
        return label
    }()
    
    private lazy var paymentMethodTitleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.medium
        label.textColor = Palette.ppColorGrayscale400.color
        label.text = PaymentRequestToolbarLocalizable.paymentMethod.text
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        label.setContentCompressionResistancePriority(.required, for: .horizontal)
        return label
    }()
    
    private lazy var paymentMethodContentLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.large
        label.textColor = Palette.ppColorBranding300.color
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()
    
    private lazy var bottomContentBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale100.color
        return view
    }()
    
    private lazy var paymentMethodView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var paymentMethodTextStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        return stackView
    }()
    
    private lazy var privacyStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = Spacing.base02
        return stackView
    }()
    
    private lazy var bottomContentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        return stackView
    }()
    
    // MARK: - Variables
    private(set) var privacy: PaymentPrivacy
    private(set) var paymentMethod: PaymentMethod?
    private let viewModel: PaymentRequestToolbarViewModelInputs
    
    // MARK: - Life Cycle
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(
        sendButtonTitle: String,
        initialPrivacy: PaymentPrivacy,
        initialPaymentMethod: PaymentMethod?,
        viewModel: PaymentRequestToolbarViewModelInputs
    ) {
        self.viewModel = viewModel
        self.privacy = initialPrivacy
        self.paymentMethod = initialPaymentMethod
        super.init(frame: .zero)
        buildLayout()
        sendButton.setTitle(sendButtonTitle, for: .normal)
    }
    
    private func createTapGesture() {
        let privacyTapGesture = UITapGestureRecognizer()
        privacyTapGesture.addTarget(self, action: #selector(didTouchOnPrivacy))
        privacyStackView.addGestureRecognizer(privacyTapGesture)
        let paymentMethodTapGesture = UITapGestureRecognizer()
        paymentMethodTapGesture.addTarget(self, action: #selector(didTouchOnPaymentMethod))
        paymentMethodView.addGestureRecognizer(paymentMethodTapGesture)
    }
    
    func displayInitialContent() {
        viewModel.showInitialState()
    }
    
    // MARK: - Action
    @objc
    private func didTouchOnSendButton() {
        viewModel.send()
    }
    
    @objc
    private func didTouchOnPrivacy() {
        viewModel.showPrivacySelection()
    }
    
    @objc
    private func didTouchOnPaymentMethod() {
        viewModel.showPaymentMethodSelection()
    }
}

// MARK: - ViewConfiguration
extension PaymentRequestToolbar: ViewConfiguration {
    func setupConstraints() {
        contentStackView.layout {
            $0.top == topAnchor
            $0.bottom == bottomAnchor
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
        }
        bottomContentStackView.layout {
            $0.top == bottomContentBackgroundView.topAnchor + Spacing.base01
            $0.bottom == bottomContentBackgroundView.bottomAnchor - Spacing.base01
            $0.leading == bottomContentBackgroundView.leadingAnchor + Spacing.base02
            $0.trailing == bottomContentBackgroundView.trailingAnchor - Spacing.base02
        }
        sendButton.layout {
            $0.height == Layout.Size.sendButtonHeight
            $0.width >= Layout.Size.sendButtonWidth
        }
        privacyImageView.layout {
            $0.height == Layout.Size.imageSide
            $0.width == Layout.Size.imageSide
            $0.leading == contentStackView.leadingAnchor + Spacing.base02
        }
        mainPaymentMethodImageView.layout {
            $0.height == Layout.Size.imageSide
            $0.width == Layout.Size.imageSide
            $0.leading == paymentMethodView.leadingAnchor
            $0.centerY == paymentMethodView.centerYAnchor
        }
        secondaryPaymentMethodImageView.layout {
            $0.height == Layout.Size.imageSide
            $0.width == Layout.Size.imageSide
            $0.top == mainPaymentMethodImageView.centerYAnchor
            $0.leading == mainPaymentMethodImageView.leadingAnchor + Spacing.base00
        }
        paymentMethodTextStackView.layout {
            $0.top == paymentMethodView.topAnchor
            $0.bottom == paymentMethodView.bottomAnchor
            $0.trailing <= paymentMethodView.trailingAnchor
            $0.leading == mainPaymentMethodImageView.trailingAnchor + Spacing.base02
        }
    }
    
    func buildViewHierarchy() {
        addSubview(contentStackView)
        contentStackView.addArrangedSubview(privacyStackView)
        contentStackView.addArrangedSubview(bottomContentBackgroundView)
        privacyStackView.addArrangedSubview(privacyImageView)
        privacyStackView.addArrangedSubview(privacyLabel)
        bottomContentBackgroundView.addSubview(bottomContentStackView)
        bottomContentStackView.addArrangedSubview(paymentMethodView)
        bottomContentStackView.addArrangedSubview(sendButton)
        paymentMethodView.addSubview(mainPaymentMethodImageView)
        paymentMethodView.addSubview(secondaryPaymentMethodImageView)
        paymentMethodView.addSubview(paymentMethodTextStackView)
        paymentMethodTextStackView.addArrangedSubview(paymentMethodTitleLabel)
        paymentMethodTextStackView.addArrangedSubview(paymentMethodContentLabel)
    }
    
    func configureViews() {
        clipsToBounds = true
        barTintColor = Palette.ppColorGrayscale000.color
        isTranslucent = false
        createTapGesture()
    }
}

// MARK: - PaymentRequestToolbarDisplay
extension PaymentRequestToolbar: PaymentRequestToolbarDisplay {
    func displayPrivacy(text: String, image: UIImage, tintColor: UIColor) {
        privacyLabel.text = text
        privacyLabel.textColor = tintColor
        privacyImageView.image = image
        privacyImageView.tintColor = tintColor
    }
    
    func displayPaymentMethod(method: String, mainImage: UIImage, secondaryImage: UIImage?, secondaryImageIsHidden: Bool) {
        paymentMethodContentLabel.text = method
        mainPaymentMethodImageView.image = mainImage
        secondaryPaymentMethodImageView.isHidden = secondaryImageIsHidden
        secondaryPaymentMethodImageView.image = secondaryImage
    }
    
    func hidePaymentMethod() {
        paymentMethodView.isHidden = true
    }
    
    func displayPrivacySelection(alertTitle: String, alertMessage: String, buttonCancel: String, buttonPublic: String, buttonPrivate: String) {
        let alert = UIAlertController(
            title: alertTitle,
            message: alertMessage,
            preferredStyle: .actionSheet
        )
        let cancelAction = UIAlertAction(title: buttonCancel, style: .cancel)
        let friendsAction = UIAlertAction(title: buttonPublic, style: .default) { _ in
            self.privacy = .public
            self.viewModel.updatePrivacyState(privacy: .public)
        }
        let privateAction = UIAlertAction(title: buttonPrivate, style: .default ) { _ in
            self.privacy = .private
            self.viewModel.updatePrivacyState(privacy: .private)
        }
        
        alert.addAction(friendsAction)
        alert.addAction(privateAction)
        alert.addAction(cancelAction)
        viewModel.showAlert(alert)
    }
    
    func displayQuestioningDefaultPrivacySelection(alertTitle: String, alertMessage: String, buttonCancel: String, buttonOk: String) {
        let alert = UIAlertController(
            title: alertTitle,
            message: alertMessage,
            preferredStyle: .actionSheet
        )
        let cancelAction = UIAlertAction(title: buttonCancel, style: .cancel) { _ in
            self.viewModel.defaultPrivacySelectionResult(accepted: false)
        }
        let okAction = UIAlertAction(title: buttonOk, style: .default ) { _ in
            self.viewModel.defaultPrivacySelectionResult(accepted: true)
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        viewModel.showAlert(alert)
    }
}
