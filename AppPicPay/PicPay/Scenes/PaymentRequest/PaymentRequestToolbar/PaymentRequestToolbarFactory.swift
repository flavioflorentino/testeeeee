import CorePayment
import Foundation

enum PaymentMethodAvailability {
    case editable
    case uneditable
    case hidden
}

struct PaymentRequestToolbarModel {
    let sendButtonTitle: String
    let initialPrivacy: PaymentPrivacy
    let initialPaymentMethod: PaymentMethod?
    let isPrivacyEditable: Bool
    let paymentMethodAvailability: PaymentMethodAvailability
    // swiftlint:disable:next weak_delegate
    let delegate: PaymentRequestToolbarDelegate?
}

enum PaymentRequestToolbarFactory {
    static func make(constructModel model: PaymentRequestToolbarModel) -> PaymentRequestToolbar {
        let presenter = PaymentRequestToolbarPresenter()
        let viewModel = PaymentRequestToolbarViewModel(
            privacy: model.initialPrivacy,
            paymentMethod: model.initialPaymentMethod,
            isPrivacyEditable: model.isPrivacyEditable,
            paymentMethodAvailability: model.paymentMethodAvailability,
            delegate: model.delegate,
            presenter: presenter,
            dependencies: DependencyContainer()
        )
        let toolbar = PaymentRequestToolbar(
            sendButtonTitle: model.sendButtonTitle,
            initialPrivacy: model.initialPrivacy,
            initialPaymentMethod: model.initialPaymentMethod,
            viewModel: viewModel
        )
        presenter.view = toolbar
        toolbar.displayInitialContent()
        return toolbar
    }
}
