import AnalyticsModule

enum PaymentRequestToolbarEvents: AnalyticsKeyProtocol {
    case privacy(type: String, modifiedDefault: Bool)
    
    private var name: String {
        "Privacidade Alterada"
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .privacy(type, modifiedDefault):
            return [
                "Privacidade": type,
                "Origem": "Pagamento",
                "Alterou padrão": modifiedDefault
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.mixPanel, .appsFlyer, .firebase])
    }
}
