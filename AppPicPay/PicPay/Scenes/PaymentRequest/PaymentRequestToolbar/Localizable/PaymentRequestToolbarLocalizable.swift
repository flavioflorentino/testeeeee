import Foundation

enum PaymentRequestToolbarLocalizable: String, Localizable {
    case paymentMethod
    case accountBalance
    case privacyPublic
    case privacyPrivate
    case selectPrivacyTitle
    case selectPrivacyDesc
    case askPrivacyTitle
    case askPrivacyDesc
    
    var key: String {
        rawValue
    }
    
    var file: LocalizableFile {
        .paymentRequestToolbar
    }
}
