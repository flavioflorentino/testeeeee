import AnalyticsModule
import CorePayment
import UI

@objc
protocol PaymentRequestToolbarDelegate: AnyObject {
    func didTouchOnSendButton()
    @objc
    optional func didChangePrivacyState(isPrivate: Bool)
    @objc
    optional func didTouchOnPrivacy()
    @objc
    optional func didTouchOnPaymentMethod()
    @objc
    optional func showPrivacyAlertSelector(alert: UIAlertController)
}

protocol PaymentRequestToolbarViewModelInputs {
    func showInitialState()
    func showPrivacySelection()
    func showPaymentMethodSelection()
    func showAlert(_ alert: UIAlertController)
    func updatePrivacyState(privacy: PaymentPrivacy)
    func defaultPrivacySelectionResult(accepted: Bool)
    func send()
}

final class PaymentRequestToolbarViewModel {
    typealias Dependencies = HasConsumerManager
    // MARK: - Variables
    private let privacy: PaymentPrivacy
    private let paymentMethod: PaymentMethod?
    private let isPrivacyEditable: Bool
    private let paymentMethodAvailability: PaymentMethodAvailability
    private let presenter: PaymentRequestToolbarPresenting
    private var dependencies: Dependencies
    private weak var delegate: PaymentRequestToolbarDelegate?
    
    // MARK: - Life Cycle
    init(
        privacy: PaymentPrivacy,
        paymentMethod: PaymentMethod?,
        isPrivacyEditable: Bool,
        paymentMethodAvailability: PaymentMethodAvailability,
        delegate: PaymentRequestToolbarDelegate?,
        presenter: PaymentRequestToolbarPresenting,
        dependencies: Dependencies
    ) {
        self.privacy = privacy
        self.paymentMethod = paymentMethod
        self.isPrivacyEditable = isPrivacyEditable
        self.paymentMethodAvailability = paymentMethodAvailability
        self.presenter = presenter
        self.delegate = delegate
        self.dependencies = dependencies
    }
}

// MARK: - PaymentRequestToolbarViewModelInputs
extension PaymentRequestToolbarViewModel: PaymentRequestToolbarViewModelInputs {
    func showInitialState() {
        presenter.presentPrivacy(privacy: privacy, isPrivacyEditable: isPrivacyEditable)
        guard
            paymentMethodAvailability != .hidden,
            let paymentMethod = paymentMethod
            else {
                presenter.hidePaymentMethod()
                return
        }
        presentPaymentMethod(paymentMethod)
    }
    
    func showPrivacySelection() {
        delegate?.didTouchOnPrivacy?()
        guard isPrivacyEditable else {
            return
        }
        presenter.presentPrivacySelection()
    }
    
    func showPaymentMethodSelection() {
        delegate?.didTouchOnPaymentMethod?()
    }
    
    func showAlert(_ alert: UIAlertController) {
        delegate?.showPrivacyAlertSelector?(alert: alert)
    }
    
    func updatePrivacyState(privacy: PaymentPrivacy) {
        guard privacy == .private else {
            updatePublicPrivacy()
            return
        }
        updatePrivatePrivacy()
    }
    
    private func updatePublicPrivacy() {
        dependencies.consumerManager.privacyConfig = PaymentPrivacy.public.rawValue
        updatePrivacyValues(privacy: .public)
    }
    
    private func updatePrivatePrivacy() {
        if dependencies.consumerManager.privacyConfigSuggestionPromptCount() < 2 {
            presenter.presentQuestioningDefaultPrivacySelection()
        } else {
            dependencies.consumerManager.privacyConfig = dependencies.consumerManager.lastChosenPrivacyConfig
            updatePrivacyValues(privacy: .private)
        }
    }
    
    private func updatePrivacyValues(privacy: PaymentPrivacy) {
        let privacyText = privacy == .private ? "Privado" : "Público"
        Analytics.shared.log(PaymentRequestToolbarEvents.privacy(type: privacyText, modifiedDefault: true))
        delegate?.didChangePrivacyState?(isPrivate: privacy == .private)
        presenter.presentPrivacy(privacy: privacy, isPrivacyEditable: isPrivacyEditable)
    }
    
    private func presentPaymentMethod(_ method: PaymentMethod) {
        if case PaymentMethod.accountBalance = method {
            presenter.presentPaymentMethod(
                method: PaymentRequestToolbarLocalizable.accountBalance.text,
                mainImage: Assets.PaymentToolbar.ppBalanceIcon.image,
                secondaryImage: nil
            )
        }
    }
    
    func defaultPrivacySelectionResult(accepted: Bool) {
        guard accepted else {
            dependencies.consumerManager.incrementPrivacyConfigSuggestionPromptCountWithLastPrivacy(privacy: PaymentPrivacy.public.rawValue)
            Analytics.shared.log(PaymentRequestToolbarEvents.privacy(type: "Privado", modifiedDefault: false))
            return
        }
        dependencies.consumerManager.privacyConfig = privacy.rawValue
        dependencies.consumerManager.incrementPrivacyConfigSuggestionPromptCountWithLastPrivacy(privacy: PaymentPrivacy.private.rawValue)
        updatePrivacyValues(privacy: .private)
    }
    
    func send() {
        delegate?.didTouchOnSendButton()
    }
}
