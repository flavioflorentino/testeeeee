import CorePayment
import UI

protocol PaymentRequestToolbarPresenting {
    func presentPrivacy(privacy: PaymentPrivacy, isPrivacyEditable: Bool)
    func presentPaymentMethod(method: String, mainImage: UIImage, secondaryImage: UIImage?)
    func hidePaymentMethod()
    func presentPrivacySelection()
    func presentQuestioningDefaultPrivacySelection()
}

final class PaymentRequestToolbarPresenter {
    // MARK: - Variables
    weak var view: PaymentRequestToolbarDisplay?
}

// MARK: - PaymentRequestToolbarPresenting
extension PaymentRequestToolbarPresenter: PaymentRequestToolbarPresenting {
    func presentPrivacy(privacy: PaymentPrivacy, isPrivacyEditable: Bool) {
        let text = privacy == .private ? DefaultLocalizable.privacyPrivate.text : DefaultLocalizable.privacyPublic.text
        let tint: Palette = isPrivacyEditable ? .ppColorBranding300 : .ppColorGrayscale400
        let image: UIImage
        if privacy == .private {
            image = Assets.NewGeneration.icoPrivPrivado.image
        } else {
            image = Assets.NewGeneration.icoPrivAmigos.image
        }
        view?.displayPrivacy(text: text, image: image.withRenderingMode(.alwaysTemplate), tintColor: tint.color)
    }
    
    func presentPaymentMethod(method: String, mainImage: UIImage, secondaryImage: UIImage?) {
        view?.displayPaymentMethod(
            method: method,
            mainImage: mainImage,
            secondaryImage: secondaryImage,
            secondaryImageIsHidden: secondaryImage == nil
        )
    }
    
    func hidePaymentMethod() {
        view?.hidePaymentMethod()
    }
    
    func presentPrivacySelection() {
        let alertTitle = PaymentRequestToolbarLocalizable.selectPrivacyTitle.text
        let alertMessage = PaymentRequestToolbarLocalizable.selectPrivacyDesc.text
        let buttonCancel = DefaultLocalizable.btCancel.text
        let buttonPublic = PaymentRequestToolbarLocalizable.privacyPublic.text
        let buttonPrivate = PaymentRequestToolbarLocalizable.privacyPrivate.text
        view?.displayPrivacySelection(
            alertTitle: alertTitle,
            alertMessage: alertMessage,
            buttonCancel: buttonCancel,
            buttonPublic: buttonPublic,
            buttonPrivate: buttonPrivate
        )
    }
    
    func presentQuestioningDefaultPrivacySelection() {
        let alertTitle = PaymentRequestToolbarLocalizable.askPrivacyTitle.text
        let alertMessage = PaymentRequestToolbarLocalizable.askPrivacyDesc.text
        let buttonCancel = DefaultLocalizable.btNo.text
        let buttonOk = DefaultLocalizable.btYes.text
        view?.displayQuestioningDefaultPrivacySelection(
            alertTitle: alertTitle,
            alertMessage: alertMessage,
            buttonCancel: buttonCancel,
            buttonOk: buttonOk
        )
    }
}
