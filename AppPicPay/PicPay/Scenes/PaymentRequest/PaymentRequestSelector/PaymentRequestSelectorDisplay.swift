import UI

protocol PaymentRequestSelectorDisplay: AnyObject {
    func setTableView(isScrollEnabled: Bool)
    func displayOptions(_ options: [Section<String, PaymentRequestOption>])
    func displayRetryLimitExceeded(with alertData: StatusAlertAttributedData)
    func displayPicPaySelectionFailure(with alertData: StatusAlertData)
    func updateLoader(isVisible: Bool)
}
