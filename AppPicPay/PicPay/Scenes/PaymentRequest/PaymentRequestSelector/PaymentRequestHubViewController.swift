import AnalyticsModule
import UI

extension PaymentRequestHubViewController.Layout {
    enum Size {
        static let icon = CGSize(width: 123, height: 128)
        static let buttonSide: CGFloat = 32
        static let rowHeight: CGFloat = 104
    }
    
    enum Font {
        static let title = UIFont.systemFont(ofSize: 24, weight: .bold)
        static let description = UIFont.systemFont(ofSize: 14, weight: .medium)
    }
}

final class PaymentRequestHubViewController: ViewController<PaymentRequestSelectorViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    // MARK: - Visual Components
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        return stackView
    }()
    
    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.Savings.iconRoundCloseGreen.image, for: .normal)
        button.addTarget(self, action: #selector(didTapOnClose), for: .touchUpInside)
        return button
    }()
    
    private lazy var paymentRequestImageView = UIImageView(image: Assets.P2P.mainCharacter.image)
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = PaymentRequestLocalizable.paymentRequest.text
        label.font = Layout.Font.title
        label.textColor = Colors.grayscale700.color
        label.textAlignment = .center
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = PaymentRequestLocalizable.paymentRequestOptionsDescription.text
        label.font = Layout.Font.description
        label.textColor = Colors.grayscale700.color
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    private lazy var optionsTableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.rowHeight = Layout.Size.rowHeight
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.register(
            PaymentRequestOptionV2TableViewCell.self,
            forCellReuseIdentifier: String(describing: PaymentRequestOptionV2TableViewCell.self)
        )
        return tableView
    }()
    
    private var dataSource: TableViewHandler<String, PaymentRequestOption, PaymentRequestOptionV2TableViewCell>?
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.presentOptions()
        Analytics.shared.log(PaymentRequestUserEvent.didOpenHub)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.setTableViewScroll(
            rowHeight: Layout.Size.rowHeight,
            screenSize: view.bounds.size,
            tableViewOrigin: optionsTableView.frame.origin,
            bottomMargin: 0
        )
    }
    
    override func buildViewHierarchy() {
        view.addSubview(contentStackView)
        view.addSubview(closeButton)
        view.addSubview(optionsTableView)
        contentStackView.addArrangedSubview(paymentRequestImageView)
        contentStackView.addArrangedSubview(titleLabel)
        contentStackView.addArrangedSubview(descriptionLabel)
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    override func setupConstraints() {
        contentStackView.setSpacing(Spacing.base02, after: paymentRequestImageView)
        contentStackView.setSpacing(Spacing.base01, after: titleLabel)
        
        paymentRequestImageView.layout {
            $0.width == Layout.Size.icon.width
            $0.height == Layout.Size.icon.height
        }
        
        closeButton.layout {
            $0.width == Layout.Size.buttonSide
            $0.height == Layout.Size.buttonSide
            $0.top == view.compatibleSafeAreaLayoutGuide.topAnchor + Spacing.base03
            $0.trailing == view.trailingAnchor - Spacing.base02
        }
        
        contentStackView.layout {
            $0.top == closeButton.topAnchor
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
        }
        
        optionsTableView.layout {
            $0.top == contentStackView.bottomAnchor + Spacing.base02
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
            $0.bottom == view.compatibleSafeAreaLayoutGuide.bottomAnchor
        }
    }
    
    // MARK: - Actions
    @objc
    private func didTapOnClose() {
        viewModel.dismissViewController()
    }
}

// MARK: PaymentRequestSelectorDisplay
extension PaymentRequestHubViewController: PaymentRequestSelectorDisplay {
    func setTableView(isScrollEnabled: Bool) {
        optionsTableView.isScrollEnabled = isScrollEnabled
    }
    
    func displayOptions(_ options: [Section<String, PaymentRequestOption>]) {
        dataSource = TableViewHandler(data: options, cellType: PaymentRequestOptionV2TableViewCell.self, configureCell: { _, option, cell in
            cell.setContent(icon: option.icon, title: option.title, subtitle: option.subTitle)
        })
        optionsTableView.dataSource = dataSource
        optionsTableView.reloadData()
    }
    
    func displayRetryLimitExceeded(with alertData: StatusAlertAttributedData) {
        endState()
        let alert = StatusAlertView.show(on: view, data: alertData)
        alert.statusAlertDelegate = self
    }
    
    func displayPicPaySelectionFailure(with alertData: StatusAlertData) {
        endState()
        let alert = StatusAlertView.show(on: view, data: alertData)
        alert.statusAlertDelegate = self
    }
    
    func updateLoader(isVisible: Bool) {
        isVisible ? beginState() : endState()
    }
}

// MARK: - UITableViewDelegate
extension PaymentRequestHubViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.selectOption(at: indexPath.row)
    }
}

// MARK: - StatusAlertViewDelegate
extension PaymentRequestHubViewController: StatusAlertViewDelegate {
    func didTouchOnLink(URL: URL) {
        viewModel.didTouchOnLink(URL: URL)
    }
    func didTouchOnButton() {
        Analytics.shared.log(PaymentRequestUserEvent.didRequestLimitExceededRestart)
    }
}

// MARK: - StatefulProviding
extension PaymentRequestHubViewController: StatefulProviding {
}
