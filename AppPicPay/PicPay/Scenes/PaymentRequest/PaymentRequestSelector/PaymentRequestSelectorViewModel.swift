import AnalyticsModule
import Core
import FeatureFlag

protocol PaymentRequestSelectorViewModelInputs: AnyObject {
    func setTableViewScroll(rowHeight: CGFloat, screenSize: CGSize, tableViewOrigin: CGPoint, bottomMargin: CGFloat)
    func presentOptions()
    func selectOption(at row: Int)
    func dismissViewController()
    func didTouchOnLink(URL: URL)
}

final class PaymentRequestSelectorViewModel {
    typealias Dependencies = HasFeatureManager
    private enum Links {
        static let knowMore = "/\(PaymentRequestLocalizable.paymentRequestExceededAlertKnowMoreButton.text)"
    }
    // MARK: - Error Codes
    enum ErrorCode: String {
        case limitExceeded = "901"
    }
    
    // MARK: - Variables
    private let dependencies: Dependencies
    private let service: PaymentRequestSelectorServicing
    private let presenter: PaymentRequestSelectorPresenting
    private var options: [PaymentRequestOption] = []
    private let origin: DGHelpers.Origin

    // MARK: - Life Cycle
    init(dependencies: Dependencies, service: PaymentRequestSelectorServicing, presenter: PaymentRequestSelectorPresenting, origin: DGHelpers.Origin) {
        self.dependencies = dependencies
        self.service = service
        self.presenter = presenter
        self.origin = origin
    }
}

// MARK: - PaymentRequestSelectorViewModelInputs
extension PaymentRequestSelectorViewModel: PaymentRequestSelectorViewModelInputs {
    func setTableViewScroll(rowHeight: CGFloat, screenSize: CGSize, tableViewOrigin: CGPoint, bottomMargin: CGFloat) {
        let contentHeight = rowHeight * CGFloat(options.count)
        let maxHeight = screenSize.height - bottomMargin - tableViewOrigin.y
        presenter.setTableView(isScrollEnabled: contentHeight > maxHeight)
    }
    
    func presentOptions() {
        options = getOptions()
        presenter.presentOptions(options)
    }
    
    private func getOptions() -> [PaymentRequestOption] {
        guard dependencies.featureManager.isActive(.featurePixHubReceiving) else {
            return [.picpay, .qrcode, .link]
        }
        return PaymentRequestOption.allCases
    }
    
    func selectOption(at row: Int) {
        let selectedType = options[row]
        switch selectedType {
        case .picpay:
            Analytics.shared.log(PaymentRequestUserEvent.didTapMethodCharge(.picpay))
            guard dependencies.featureManager.isActive(.paymentRequestV2) else {
                picpaySelection()
                return
            }
            picpaySelectionV2()
        case .pix:
            presenter.presentPixReceivementScene()
            Analytics.shared.log(PaymentRequestUserEvent.didTapMethodCharge(.pix))
        case .qrcode:
            presenter.presentChargeUserValueSelection(origin: origin, type: .qrCode)
            Analytics.shared.log(PaymentRequestUserEvent.didTapMethodCharge(.qrcode))
        case .link:
            presenter.presentChargeUserValueSelection(origin: origin, type: .link)
            Analytics.shared.log(PaymentRequestUserEvent.didTapMethodCharge(.link))
        }
    }
    
    private func picpaySelection() {
        let permissionCompletion: (Result<(model: PaymentRequestPermissionData, data: Data?), ApiError>) -> Void = { [weak self] result in
            guard let self = self else {
                return
            }
            switch result {
            case let .success(response):
                guard !response.model.data.isP2PBlockDayChargesEnabled else {
                    self.presenter.presentRetryLimitExceeded()
                    Analytics.shared.log(PaymentRequestUserEvent.didRequestLimitExceeded)
                    return
                }
                self.picpaySelectionSuccess()
            case .failure:
                self.picpaySelectionFailure()
            }
        }
        presenter.updateLoader(isVisible: true)
        service.getPaymentRequestPermission(completion: permissionCompletion)
    }
    
    private func picpaySelectionSuccess() {
        presenter.updateLoader(isVisible: false)
        presenter.presentConsumersSearch()
    }
    
    private func picpaySelectionFailure() {
        presenter.presentPicPaySelectionFailure()
    }
    
    private func picpaySelectionV2() {
        let permissionCompletion: (Result<(model: PaymentRequestPermissionV2, data: Data?), DialogError>) -> Void = { [weak self] result in
            switch result {
            case let .success(response):
                self?.picpaySelectionSuccessV2(permission: response.model)
            case let .failure(error):
                self?.picpaySelectionFailureV2(error: error)
            }
        }
        presenter.updateLoader(isVisible: true)
        service.getPaymentRequestPermissionV2(completion: permissionCompletion)
    }
    
    private func picpaySelectionSuccessV2(permission: PaymentRequestPermissionV2) {
        presenter.updateLoader(isVisible: false)
        presenter.presentConsumersSearchV2(maxUsers: permission.maxUsers)
    }
    
    private func picpaySelectionFailureV2(error: DialogError) {
        switch error {
        case let .dialog(dialogError):
            guard let errorCode = ErrorCode(rawValue: dialogError.code) else {
                presenter.presentPicPaySelectionFailure()
                return
            }
            handleErrorCode(errorCode, dialogError: dialogError)
        default:
            presenter.presentPicPaySelectionFailure()
        }
    }
    
    private func handleErrorCode(_ errorCode: ErrorCode, dialogError: DialogResponseError) {
        if errorCode == .limitExceeded {
            let title = dialogError.model.title
            let message = dialogError.model.message
            let buttonTitle = dialogError.model.buttonText
            presenter.presentRetryLimitExceededV2(title: title, message: message, buttonTitle: buttonTitle)
            Analytics.shared.log(PaymentRequestUserEvent.didRequestLimitExceeded)
        }
    }
    
    func dismissViewController() {
        presenter.dismissViewController()
    }
    
    func didTouchOnLink(URL: URL) {
        guard URL.path == Links.knowMore else {
            return
        }
        Analytics.shared.log(PaymentRequestUserEvent.didTapRequestLimitExceededKnowMore)
        presenter.presentKnowMore()
    }
}
