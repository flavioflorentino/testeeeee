import AnalyticsModule
import FeatureFlag
import Foundation

final class PaymentRequestSelectorFactory {
    typealias Dependencies = HasFeatureManager
    // MARK: - Variables
    let dependencies: Dependencies
    
    // MARK: - Life Cycle
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    // MARK: - Make
    func make(origin: DGHelpers.Origin) -> UIViewController {
        Analytics.shared.log(PaymentRequestUserEvent.start(origin: origin.rawValue))
        let container = DependencyContainer()
        let service: PaymentRequestSelectorServicing = PaymentRequestSelectorService(dependencies: container)
        let coordinator: PaymentRequestSelectorCoordinating = PaymentRequestSelectorCoordinator()
        let presenter: PaymentRequestSelectorPresenting = PaymentRequestSelectorPresenter(coordinator: coordinator)
        let viewModel = PaymentRequestSelectorViewModel(dependencies: container, service: service, presenter: presenter, origin: origin)
        let viewController = PaymentRequestHubViewController(viewModel: viewModel)
        coordinator.viewController = viewController
        presenter.viewController = viewController
        let navigationController = UINavigationController(rootViewController: viewController)
        return navigationController
    }
}
