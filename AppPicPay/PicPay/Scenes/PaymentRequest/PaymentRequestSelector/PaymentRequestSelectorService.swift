import Core

protocol PaymentRequestSelectorServicing {
    func getPaymentRequestPermission(completion: @escaping (Result<(model: PaymentRequestPermissionData, data: Data?), ApiError>) -> Void)
    func getPaymentRequestPermissionV2(completion: @escaping (Result<(model: PaymentRequestPermissionV2, data: Data?), DialogError>) -> Void)
}

final class PaymentRequestSelectorService {
    typealias Dependencies = HasMainQueue
    
    // MARK: - Variables
    private let dependencies: Dependencies
    
    // MARK: - Life Cycle
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - PaymentRequestSelectorServicing
extension PaymentRequestSelectorService: PaymentRequestSelectorServicing {
    func getPaymentRequestPermission(completion: @escaping (Result<(model: PaymentRequestPermissionData, data: Data?), ApiError>) -> Void) {
        let endpoint = PaymentRequestServiceEndpoint.paymentRequestPermission
        Api<PaymentRequestPermissionData>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result)
            }
        }
    }
    
    func getPaymentRequestPermissionV2(completion: @escaping (Result<(model: PaymentRequestPermissionV2, data: Data?), DialogError>) -> Void) {
        let endpoint = PaymentRequestServiceEndpoint.paymentRequestPermissionV2
        Api<PaymentRequestPermissionV2>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.mapError(DialogError.transformApiError)
                completion(mappedResult)
            }
        }
    }
}
