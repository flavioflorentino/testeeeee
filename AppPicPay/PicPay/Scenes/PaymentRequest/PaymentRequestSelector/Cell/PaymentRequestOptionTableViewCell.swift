import UI

final class PaymentRequestOptionTableViewCell: UITableViewCell {
    // MARK: - Layout
    private enum Layout {
        static let margin: CGFloat = 15
        static let iconSide: CGFloat = 50
        static let arrowSide: CGFloat = 24
        static let bottomMargin: CGFloat = 1
        
        static let spacing: CGFloat = 20
        
        static let numberOfLines: Int = 0
        
        static let fontTitle: CGFloat = 12
        static let fontSubTitle: CGFloat = 10
    }
    // MARK: - Visual Components
    private lazy var contentBackgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale000.color
        return view
    }()
    
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = Layout.spacing
        stackView.alignment = .center
        stackView.axis = .horizontal
        return stackView
    }()
    
    private lazy var textStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var arrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = #imageLiteral(resourceName: "outlined_left_arrow")
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: Layout.fontTitle, weight: .regular)
        label.numberOfLines = Layout.numberOfLines
        return label
    }()
    
    private lazy var subTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: Layout.fontSubTitle, weight: .bold)
        label.numberOfLines = Layout.numberOfLines
        return label
    }()
    
    // MARK: - Life Cycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildViewHierarchy()
        configureViews()
        setupConstraints()
        selectionStyle = .none
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func buildViewHierarchy() {
        addSubview(contentBackgroundView)
        contentBackgroundView.addSubview(contentStackView)
        contentStackView.addArrangedSubview(iconImageView)
        contentStackView.addArrangedSubview(textStackView)
        contentStackView.addArrangedSubview(arrowImageView)
        textStackView.addArrangedSubview(titleLabel)
        textStackView.addArrangedSubview(subTitleLabel)
    }
    
    private func configureViews() {
        backgroundColor = .clear
        contentView.backgroundColor = .clear
    }

    private func setupConstraints() {
        NSLayoutConstraint.activate([
            contentBackgroundView.topAnchor.constraint(equalTo: topAnchor),
            contentBackgroundView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Layout.bottomMargin),
            contentBackgroundView.leadingAnchor.constraint(equalTo: leadingAnchor),
            contentBackgroundView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            contentStackView.topAnchor.constraint(equalTo: contentBackgroundView.topAnchor, constant: Layout.margin),
            contentStackView.bottomAnchor.constraint(equalTo: contentBackgroundView.bottomAnchor, constant: -Layout.margin),
            contentStackView.leadingAnchor.constraint(equalTo: contentBackgroundView.leadingAnchor, constant: Layout.margin),
            contentStackView.trailingAnchor.constraint(equalTo: contentBackgroundView.trailingAnchor, constant: -Layout.margin)
        ])
        
        NSLayoutConstraint.activate([
            iconImageView.heightAnchor.constraint(equalToConstant: Layout.iconSide),
            iconImageView.widthAnchor.constraint(equalToConstant: Layout.iconSide)
        ])
        
        NSLayoutConstraint.activate([
            arrowImageView.heightAnchor.constraint(equalToConstant: Layout.arrowSide),
            arrowImageView.widthAnchor.constraint(equalToConstant: Layout.arrowSide)
        ])
    }
    
    // MARK: - Content
    func setContent(icon: UIImage?, title: String, subTitle: String) {
        iconImageView.image = icon
        titleLabel.text = title
        subTitleLabel.text = subTitle
    }
}
