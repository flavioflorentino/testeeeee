import UI

extension PaymentRequestOptionV2TableViewCell.Layout {
    enum Size {
        static let icon = CGSize(width: 64, height: 64)
    }
    
    enum Font {
        static let title = UIFont.systemFont(ofSize: 18, weight: .semibold)
        static let subtitle = UIFont.systemFont(ofSize: 14, weight: .medium)
    }
}

final class PaymentRequestOptionV2TableViewCell: UITableViewCell {
    fileprivate enum Layout {}
    
    // MARK: - Visual Components
    private lazy var contentBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.backgroundPrimary.color
        view.layer.cornerRadius = CornerRadius.medium
        view.layer.borderColor = Colors.grayscale050.color.cgColor
        view.layer.borderWidth = 1
        view.layer.shouldRasterize = true
        view.layer.rasterizationScale = UIScreen.main.scale
        return view
    }()
    
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base02
        stackView.alignment = .center
        stackView.axis = .horizontal
        return stackView
    }()
    
    private lazy var textStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base00
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var iconImageView = UIImageView()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.title
        label.textColor = Colors.grayscale700.color
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.subtitle
        label.textColor = Colors.grayscale700.color
        label.numberOfLines = 0
        return label
    }()
    
    // MARK: - Life Cycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
        selectionStyle = .none
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentBackgroundView.layer.setShadow(radius: ShadowRadius.medium.rawValue, offSet: ShadowOffset.medium.rawValue, color: .black, opacity: 0.1)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        contentBackgroundView.layer.borderColor = Colors.grayscale050.color.cgColor
    }
    
    // MARK: - Content
    func setContent(icon: UIImage?, title: String, subtitle: String) {
        iconImageView.image = icon
        titleLabel.text = title
        subtitleLabel.text = subtitle
    }
}

// MARK: - ViewConfiguration
extension PaymentRequestOptionV2TableViewCell: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(contentBackgroundView)
        contentBackgroundView.addSubview(contentStackView)
        contentStackView.addArrangedSubview(iconImageView)
        contentStackView.addArrangedSubview(textStackView)
        textStackView.addArrangedSubview(titleLabel)
        textStackView.addArrangedSubview(subtitleLabel)
    }
    
    func configureViews() {
        backgroundColor = .clear
        contentView.backgroundColor = .clear
    }

    func setupConstraints() {
        contentBackgroundView.layout {
            $0.top == topAnchor + Spacing.base00
            $0.trailing == trailingAnchor - Spacing.base02
            $0.leading == leadingAnchor + Spacing.base02
            $0.bottom == bottomAnchor - Spacing.base00
        }
        
        contentStackView.layout {
            $0.top == contentBackgroundView.topAnchor + Spacing.base02
            $0.leading == contentBackgroundView.leadingAnchor + Spacing.base02
            $0.trailing == contentBackgroundView.trailingAnchor - Spacing.base02
            $0.bottom == contentBackgroundView.bottomAnchor - Spacing.base02
        }
        
        iconImageView.layout {
            $0.height == Layout.Size.icon.height
            $0.width == Layout.Size.icon.width
        }
    }
}
