import PIX
import UIKit

enum PaymentRequestSelectorAction {
    case consumersSearch
    case consumersSearchV2(maxUsers: Int)
    case chargeUserValue(origin: DGHelpers.Origin, type: PaymentRequestExternalType)
    case close
    case knowMore
    case pixReceivement
}

enum PaymentRequestExternalType {
    case link
    case qrCode
}

protocol PaymentRequestSelectorCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: PaymentRequestSelectorAction)
}

final class PaymentRequestSelectorCoordinator: PaymentRequestSelectorCoordinating {
    struct SearchDefinition {
        let tabs: [SearchWrapperTabType]
        let isOnlySearchEnabled: Bool
        let showContainerSearch: Bool
        let ignorePermissionPrompt: Bool
        let searchPlaceHolder: String
    }
    
    weak var viewController: UIViewController?
    
    // MARK: - Perform
    func perform(action: PaymentRequestSelectorAction) {
        switch action {
        case .consumersSearch:
            let controller = PaymentRequestSearchFactory().make()
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case let .consumersSearchV2(maxUsers):
            let controller = PaymentRequestSearchFactory().make(maxUsers: maxUsers)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case let .chargeUserValue(origin, type):
            let controller = PaymentRequestValueFactory.make(origin: origin, type: type, hasNavigationController: false)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        case .close:
            viewController?.navigationController?.dismiss(animated: true)
        case .knowMore:
            break
        case .pixReceivement:
            guard let navController = viewController?.navigationController else {
                return
            }
            let coordinator = PIXConsumerFlowCoordinator(
                originViewController: navController,
                originFlow: .paymentRequest,
                destination: .receivement(origin: .paymentRequestHub)
            )
            coordinator.start(isModalPresentation: false)
        }
    }
}
