import Core
import UI

protocol PaymentRequestSelectorPresenting: AnyObject {
    var viewController: PaymentRequestSelectorDisplay? { get set }
    func setTableView(isScrollEnabled: Bool)
    func presentOptions(_ options: [PaymentRequestOption])
    func presentConsumersSearch()
    func presentConsumersSearchV2(maxUsers: Int)
    func presentChargeUserValueSelection(origin: DGHelpers.Origin, type: PaymentRequestExternalType)
    func dismissViewController()
    func presentRetryLimitExceeded()
    func presentRetryLimitExceededV2(title: String, message: String, buttonTitle: String)
    func presentKnowMore()
    func presentPicPaySelectionFailure()
    func updateLoader(isVisible: Bool)
    func presentPixReceivementScene()
}

final class PaymentRequestSelectorPresenter {
    // MARK: - Variables
    private let coordinator: PaymentRequestSelectorCoordinating
    weak var viewController: PaymentRequestSelectorDisplay?
    
    // MARK: - Life Cycle
    init(coordinator: PaymentRequestSelectorCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - PaymentRequestSelectorPresenting
extension PaymentRequestSelectorPresenter: PaymentRequestSelectorPresenting {
    func setTableView(isScrollEnabled: Bool) {
        viewController?.setTableView(isScrollEnabled: isScrollEnabled)
    }
    
    func presentOptions(_ options: [PaymentRequestOption]) {
        let sections = [Section<String, PaymentRequestOption>(items: options)]
        viewController?.displayOptions(sections)
    }
    
    func presentConsumersSearch() {
        coordinator.perform(action: .consumersSearch)
    }
    
    func presentConsumersSearchV2(maxUsers: Int) {
        coordinator.perform(action: .consumersSearchV2(maxUsers: maxUsers))
    }
    
    func presentChargeUserValueSelection(origin: DGHelpers.Origin, type: PaymentRequestExternalType) {
        coordinator.perform(action: .chargeUserValue(origin: origin, type: type))
    }
    
    func dismissViewController() {
        coordinator.perform(action: .close)
    }
    
    func presentRetryLimitExceeded() {
        let text = PaymentRequestLocalizable.paymentRequestExceededAlertText.text
        let attributedText = createAttributedTextRetryLimitExceeded(text: text)
        let alertData = StatusAlertAttributedData(
            icon: Assets.Emotions.iluSadFace.image,
            title: PaymentRequestLocalizable.paymentRequestExceededAlertTitle.text,
            text: attributedText,
            buttonTitle: PaymentRequestLocalizable.paymentRequestExceededAlertOtherButton.text
        )
        viewController?.displayRetryLimitExceeded(with: alertData)
    }
    
    func presentRetryLimitExceededV2(title: String, message: String, buttonTitle: String) {
        let attributedText = createAttributedTextRetryLimitExceeded(text: message)
        let alertData = StatusAlertAttributedData(
            icon: Assets.Emotions.iluSadFace.image,
            title: title,
            text: attributedText,
            buttonTitle: buttonTitle
        )
        viewController?.displayRetryLimitExceeded(with: alertData)
    }
    
    private func createAttributedTextRetryLimitExceeded(text: String) -> NSAttributedString {
        let attributedText = NSMutableAttributedString(string: text)
        
        let normalAttributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: Palette.ppColorGrayscale500.color,
            .font: UIFont.systemFont(ofSize: 14)
        ]
        
        attributedText.addAttributes(normalAttributes, range: NSRange(location: 0, length: attributedText.length))
        
        return attributedText
    }
    
    func presentKnowMore() {
        coordinator.perform(action: .knowMore)
    }
    
    func presentPicPaySelectionFailure() {
        let alertData = StatusAlertData(
            icon: Assets.Emotions.iluSadFace.image,
            title: PaymentRequestLocalizable.somethingWentWrong.text,
            text: PaymentRequestLocalizable.requestNotCompleted.text,
            buttonTitle: PaymentRequestLocalizable.tryAgain.text
        )
        viewController?.displayPicPaySelectionFailure(with: alertData)
    }
    
    func updateLoader(isVisible: Bool) {
        viewController?.updateLoader(isVisible: isVisible)
    }
    
    func presentPixReceivementScene() {
        coordinator.perform(action: .pixReceivement)
    }
}
