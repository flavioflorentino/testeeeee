import PermissionsKit

protocol PaymentRequestContactsPermission {
    var status: PermissionStatus { get }
    func requestContacts(completion: @escaping Permission.Callback)
}

struct PaymentRequestContactsPermissionHelper: PaymentRequestContactsPermission {
    var status: PermissionStatus {
        Permission.contacts.status
    }
    
    func requestContacts(completion: @escaping Permission.Callback) {
        PPPermission.requestContacts { completion($0) }
    }
}
