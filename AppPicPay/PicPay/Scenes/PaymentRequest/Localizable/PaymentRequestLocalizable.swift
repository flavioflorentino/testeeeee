enum PaymentRequestLocalizable: String, Localizable {
    case valueScreenMessage
    case doNotInformValue
    
    case paymentCodeScreenMessage
    
    case informValue
    case shareCode
    case shareLink
    
    case shareLinkMessageNoValue
    case shareLinkMessageWithValue
    
    case payment
    case charge
    case paymentRequest
    case paymentRequestOptionsDescription
    case paymentRequestOptionTitlePicPay
    case paymentRequestOptionTitleQrcode
    case paymentRequestOptionTitleLink
    case paymentRequestOptionSubTitlePicPay
    case paymentRequestOptionSubTitleQrcode
    case paymentRequestOptionSubTitleLink
    case paymentRequestOptionTitlePix
    case paymentRequestOptionSubTitlePix
    
    case paymentRequestSearchPlaceholder
    
    case paymentRequestExceededAlertTitle
    case paymentRequestExceededAlertText
    case paymentRequestExceededAlertOtherButton
    case paymentRequestExceededAlertKnowMoreButton
    
    case paymentRequestAlertTitleV2
    case paymentRequestAlertTextV2
    case paymentRequestAlertButtonV2
    
    case paymentRequestAlertTitle
    case paymentRequestAlertText
    case paymentRequestAlertButton
    case paymentRequestAlertTitleError
    case paymentRequestAlertTextError
    case paymentRequestAlertButtonTryAgain
    case paymentRequestShouldFillValueAlertTitle
    
    case sendPaymentRequest
    case messagePlaceholder
    case privacyAlertTitle
    case privacyAlertText
    case privacyAlertButton
    case valuePerPerson
    
    case somethingWentWrong
    case requestNotCompleted
    case tryAgain
    
    case paymentRequestNotAllowedTitle
    case paymentRequestNotAllowedMessage
    
    case selected
    case quantityText
    
    case limitUsersReachedTitle
    case limitUsersReachedMessage
    
    case results
    case contacts
    
    case usersListEmptyState
    case contactsListEmptyState
    case weCouldntCompleteYourSearch
    
    case paymentMade
    case pendingPayment
    case weCouldntVerifyPayments
    
    case qrCodeUnavailable
    
    var key: String {
        rawValue
    }
    
    var file: LocalizableFile {
        .paymentRequest
    }
}
