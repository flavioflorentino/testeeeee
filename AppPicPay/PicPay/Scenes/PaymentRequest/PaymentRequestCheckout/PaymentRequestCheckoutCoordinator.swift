import UI
import UIKit

enum PaymentRequestCheckoutAction {
    case back
    case close
}

protocol PaymentRequestCheckoutCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: PaymentRequestCheckoutAction)
    func performShowProfile(profileImage: UIPPProfileImage, contact: PPContact)
    func presentSuccessDialog()
    func present(alertController: UIAlertController)
}

final class PaymentRequestCheckoutCoordinator: PaymentRequestCheckoutCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: PaymentRequestCheckoutAction) {
        switch action {
        case .back:
            viewController?.navigationController?.popViewController(animated: true)
        case .close:
            viewController?.navigationController?.dismiss(animated: true)
        }
    }
    
    func performShowProfile(profileImage: UIPPProfileImage, contact: PPContact) {
        guard let viewController = viewController else {
            return
        }
        NewProfileProxy.openProfile(contact: contact, parent: viewController, originView: profileImage)
    }
    
    func presentSuccessDialog() {
        let alertData = StatusAlertData(
            icon: Assets.P2P.paymentRequestMoney.image,
            title: PaymentRequestLocalizable.paymentRequestAlertTitle.text,
            text: PaymentRequestLocalizable.paymentRequestAlertText.text,
            buttonTitle: PaymentRequestLocalizable.paymentRequestAlertButton.text
        )
        let controller = StatusAlertViewController(data: alertData)
        controller.didTouchOnButtonClosure = { [weak self] in
            self?.viewController?.navigationController?.dismiss(animated: false)
        }
        viewController?.present(controller, animated: true, completion: nil)
    }
    
    func present(alertController: UIAlertController) {
        viewController?.present(alertController, animated: true, completion: nil)
    }
}
