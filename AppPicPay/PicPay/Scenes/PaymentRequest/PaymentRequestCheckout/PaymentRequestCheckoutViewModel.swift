import AnalyticsModule
import Foundation

protocol PaymentRequestCheckoutViewModelInputs: AnyObject {
    func loadProfile()
    func sendPaymentRequest(message: String, value: Double)
    func beginEditingTextView()
    func endEditingTextView()
    func changeTextView(text: String)
    func touchOnPrivacyButton()
    func didTapCloseButton()
    func shouldChangeText(_ text: String) -> Bool
    func showProfile(profileImage: UIPPProfileImage, contact: PPContact)
    func dismissViewController()
}

final class PaymentRequestCheckoutViewModel {
    // MARK: - Variables
    private let service: PaymentRequestCheckoutServicing
    private let presenter: PaymentRequestCheckoutPresenting
    private let contact: PPContact
    private var hasOnlyPlaceholderOnTextView: Bool = true
    private let backButtonClose: Bool
    private let messageMaxLength = 100

    // MARK: - Life Cycle
    init(
        service: PaymentRequestCheckoutServicing,
        presenter: PaymentRequestCheckoutPresenting,
        contact: PPContact,
        backButtonClose: Bool
    ) {
        self.service = service
        self.presenter = presenter
        self.contact = contact
        self.backButtonClose = backButtonClose
    }
}

// MARK: - PaymentRequestCheckoutViewModelInputs
extension PaymentRequestCheckoutViewModel: PaymentRequestCheckoutViewModelInputs {
    func loadProfile() {
        presenter.displayProfile(with: contact)
    }
    
    func sendPaymentRequest(message: String, value: Double) {
        guard value > Double.zero else {
            presenter.presentShouldFillValueAlert()
            return
        }
        
        Analytics.shared.log(PaymentRequestUserEvent.didTapSendPicPayCharge(value: value, hasMessage: message.isEmpty))
        presenter.startLoading()
        
        let message = hasOnlyPlaceholderOnTextView ? "" : message
        service.sendPaymentRequest(idPayer: contact.wsId, message: message, value: value) { [weak self] result in
            switch result {
            case let .success(response):
                if response.model.success {
                    Analytics.shared.log(PaymentRequestUserEvent.picpayPaymentRequestSuccess)
                    self?.presenter.presentPaymentRequestSuccess()
                } else {
                    Analytics.shared.log(PaymentRequestUserEvent.picpayPaymentRequestError)
                    self?.presenter.presentPaymentRequestError()
                }
            case .failure:
                Analytics.shared.log(PaymentRequestUserEvent.picpayPaymentRequestError)
                self?.presenter.presentPaymentRequestError()
            }
        }
    }
    
    func beginEditingTextView() {
        guard hasOnlyPlaceholderOnTextView else {
            return
        }
        presenter.showPlaceholderMessage(isVisible: false)
    }
    
    func endEditingTextView() {
        guard hasOnlyPlaceholderOnTextView else {
            return
        }
        presenter.showPlaceholderMessage(isVisible: true)
    }
    
    func changeTextView(text: String) {
        hasOnlyPlaceholderOnTextView = text.isEmpty
    }
    
    func touchOnPrivacyButton() {
        Analytics.shared.log(PaymentRequestUserEvent.didTapOnPrivacy)
        presenter.presentPrivacyAlert()
    }
    
    func didTapCloseButton() {
        if backButtonClose {
            presenter.dismissViewController()
        } else {
            presenter.popViewController()
        }
    }
    
    func shouldChangeText(_ text: String) -> Bool {
        text.count <= messageMaxLength
    }
    
    func showProfile(profileImage: UIPPProfileImage, contact: PPContact) {
        presenter.presentProfile(profileImage: profileImage, contact: contact)
    }
    
    func dismissViewController() {
        presenter.dismissViewController()
    }
}
