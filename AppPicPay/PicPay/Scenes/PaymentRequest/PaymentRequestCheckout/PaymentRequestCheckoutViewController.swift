import AnalyticsModule
import CorePayment
import UI
import UIKit

final class PaymentRequestCheckoutViewController: ViewController<PaymentRequestCheckoutViewModelInputs, UIView> {
    // MARK: - Layout
    private enum Layout {
        static let marginOffset: CGFloat = 9
        static let imageSide: CGFloat = 60
        static let lineHeight: CGFloat = 1
        
        static let currencyLimitValue: Int = 8
        
        static let fontUsername: CGFloat = 14
        static let fontCurrency: CGFloat = 24
    }
    
    // MARK: - Visual Components
    private lazy var closeButton: UIBarButtonItem = {
        let barButton = UIBarButtonItem(
            image: #imageLiteral(resourceName: "back"),
            style: .plain,
            target: self,
            action: #selector(didTapCloseButton)
        )
        barButton.tintColor = Palette.ppColorBranding300.color
        return barButton
    }()
    
    private lazy var userStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .center
        stackView.spacing = Layout.marginOffset
        return stackView
    }()
    
    private lazy var textStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var contactProfileImage: UIPPProfileImage = {
        let profileImage = UIPPProfileImage()
        profileImage.translatesAutoresizingMaskIntoConstraints = false
        profileImage.delegate = self
        return profileImage
    }()
    
    private lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: Layout.fontUsername)
        label.textColor = Palette.ppColorGrayscale600.color
        return label
    }()
    
    private lazy var valueCurrencyField: CurrencyField = {
        let textField = CurrencyField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.fontValue = UIFont.systemFont(ofSize: Layout.fontCurrency)
        textField.fontCurrency = UIFont.systemFont(ofSize: Layout.fontCurrency)
        textField.textColor = Palette.ppColorGrayscale600.color
        textField.placeholderColor = Palette.ppColorGrayscale400.color
        textField.limitValue = Layout.currencyLimitValue
        return textField
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale300.color
        return view
    }()
    
    private lazy var messageTextView: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = .clear
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.text = PaymentRequestLocalizable.messagePlaceholder.text
        textView.textColor = Palette.ppColorGrayscale400.color
        textView.delegate = self
        return textView
    }()
    
    private lazy var paymentRequestToolbar: PaymentRequestToolbar = {
        let constructModel = PaymentRequestToolbarModel(
            sendButtonTitle: PaymentRequestLocalizable.sendPaymentRequest.text,
            initialPrivacy: .private,
            initialPaymentMethod: nil,
            isPrivacyEditable: false,
            paymentMethodAvailability: .hidden,
            delegate: self
        )
        let toolbar = PaymentRequestToolbarFactory.make(constructModel: constructModel)
        toolbar.translatesAutoresizingMaskIntoConstraints = false
        return toolbar
    }()

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        Analytics.shared.log(PaymentRequestUserEvent.didOpenPaymentRequestScreen)
        viewModel.loadProfile()
        valueCurrencyField.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(userStackView)
        userStackView.addArrangedSubview(contactProfileImage)
        userStackView.addArrangedSubview(textStackView)
        textStackView.addArrangedSubview(usernameLabel)
        textStackView.addArrangedSubview(valueCurrencyField)
        view.addSubview(lineView)
        view.addSubview(messageTextView)
    }
    
    override func configureViews() {
        navigationItem.title = PaymentRequestLocalizable.paymentRequest.text
        view.backgroundColor = Palette.ppColorGrayscale000.color
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale100.color
        navigationItem.leftBarButtonItem = closeButton
        valueCurrencyField.inputAccessory = paymentRequestToolbar
        messageTextView.inputAccessoryView = paymentRequestToolbar
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.activate([
            userStackView.leadingAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.leadingAnchor, constant: Layout.marginOffset),
            userStackView.topAnchor.constraint(equalTo: view.topAnchor, constant: Layout.marginOffset),
            userStackView.trailingAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.trailingAnchor, constant: -Layout.marginOffset)
        ])
        NSLayoutConstraint.activate([
            contactProfileImage.heightAnchor.constraint(equalToConstant: Layout.imageSide),
            contactProfileImage.widthAnchor.constraint(equalToConstant: Layout.imageSide)
        ])
        NSLayoutConstraint.activate([
            lineView.topAnchor.constraint(equalTo: userStackView.bottomAnchor, constant: Layout.marginOffset),
            lineView.leadingAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.leadingAnchor),
            lineView.trailingAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.trailingAnchor),
            lineView.heightAnchor.constraint(equalToConstant: Layout.lineHeight)
        ])
        NSLayoutConstraint.activate([
            messageTextView.topAnchor.constraint(equalTo: lineView.bottomAnchor, constant: Layout.marginOffset),
            messageTextView.leadingAnchor.constraint(equalTo: userStackView.leadingAnchor),
            messageTextView.trailingAnchor.constraint(equalTo: userStackView.trailingAnchor),
            messageTextView.bottomAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    // MARK: - Action
    @objc
    private func didTapCloseButton() {
        viewModel.didTapCloseButton()
    }
}

extension PaymentRequestCheckoutViewController: StatefulTransitionViewing {
    func didTryAgain() {
        didTouchOnSendButton()
    }
}

// MARK: PaymentRequestCheckoutDisplay
extension PaymentRequestCheckoutViewController: PaymentRequestCheckoutDisplay {
    func displayProfile(with contact: PPContact, username: String) {
        contactProfileImage.setContact(contact)
        usernameLabel.text = username
    }
    
    func showTextMessage(_ text: String) {
        messageTextView.text = text
    }
    
    func displayPrivacyAlert() {
        let button = Button(title: PaymentRequestLocalizable.privacyAlertButton.text)
        let alert = Alert(
            with: PaymentRequestLocalizable.privacyAlertTitle.text,
            text: PaymentRequestLocalizable.privacyAlertText.text,
            buttons: [button]
        )
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func showError(with model: StatefulViewModeling) {
        endState(model: model)
    }
    
    func startLoading() {
        view.endEditing(true)
        beginState()
    }
}

// MARK: - UIPPProfileImageDelegate
extension PaymentRequestCheckoutViewController: UIPPProfileImageDelegate {
    func profileImageViewDidTap(_ profileImage: UIPPProfileImage, contact: PPContact) {
        viewModel.showProfile(profileImage: profileImage, contact: contact)
    }
}

// MARK: - PaymentRequestToolbarDelegate
extension PaymentRequestCheckoutViewController: PaymentRequestToolbarDelegate {
    func didTouchOnPrivacy() {
        viewModel.touchOnPrivacyButton()
    }
    
    func didTouchOnSendButton() {
        viewModel.sendPaymentRequest(
            message: messageTextView.text ?? "",
            value: valueCurrencyField.value
        )
    }
}

// MARK: - UITextViewDelegate
extension PaymentRequestCheckoutViewController: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        viewModel.beginEditingTextView()
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        viewModel.endEditingTextView()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let resultText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        viewModel.changeTextView(text: resultText)
        return viewModel.shouldChangeText(resultText)
    }
}

// MARK: - StatusAlertViewDelegate
extension PaymentRequestCheckoutViewController: StatusAlertViewDelegate {
    func didTouchOnButton() {
        viewModel.dismissViewController()
    }
}
