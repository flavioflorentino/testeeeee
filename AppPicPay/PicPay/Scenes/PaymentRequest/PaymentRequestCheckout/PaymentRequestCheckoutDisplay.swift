import UI

protocol PaymentRequestCheckoutDisplay: AnyObject {
    func displayProfile(with contact: PPContact, username: String)
    func showTextMessage(_ text: String)
    func displayPrivacyAlert()
    func showError(with model: StatefulViewModeling)
    func startLoading()
}
