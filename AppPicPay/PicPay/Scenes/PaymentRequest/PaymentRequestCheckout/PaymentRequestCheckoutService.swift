import Core

protocol PaymentRequestCheckoutServicing {
    func sendPaymentRequest(idPayer: Int, message: String, value: Double, completion: @escaping (Result<(model: PaymentRequestCheckoutResponse, data: Data?), ApiError>) -> Void)
}

final class PaymentRequestCheckoutService {
    typealias Dependencies = HasMainQueue
    // MARK: - Variables
    let dependencies: HasMainQueue
    
    // MARK: - Life Cycle
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension PaymentRequestCheckoutService: PaymentRequestCheckoutServicing {
    func sendPaymentRequest(idPayer: Int, message: String, value: Double, completion: @escaping (Result<(model: PaymentRequestCheckoutResponse, data: Data?), ApiError>) -> Void) {
        let endpoint = PaymentRequestServiceEndpoint.checkout(
            idPayer: idPayer,
            totalValue: value,
            message: message
        )
        Api<PaymentRequestCheckoutResponse>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result)
            }
        }
    }
}
