enum PaymentRequestCheckoutFactory {
    static func make(contact: PPContact, backButtonClose: Bool) -> PaymentRequestCheckoutViewController {
        let service: PaymentRequestCheckoutServicing = PaymentRequestCheckoutService(dependencies: DependencyContainer())
        let coordinator: PaymentRequestCheckoutCoordinating = PaymentRequestCheckoutCoordinator()
        let presenter: PaymentRequestCheckoutPresenting = PaymentRequestCheckoutPresenter(coordinator: coordinator)
        let viewModel = PaymentRequestCheckoutViewModel(service: service, presenter: presenter, contact: contact, backButtonClose: backButtonClose)
        let viewController = PaymentRequestCheckoutViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
