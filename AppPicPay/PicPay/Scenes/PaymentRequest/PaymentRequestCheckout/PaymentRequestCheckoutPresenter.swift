import Core
import UI

protocol PaymentRequestCheckoutPresenting: AnyObject {
    var viewController: PaymentRequestCheckoutDisplay? { get set }
    func displayProfile(with contact: PPContact)
    func presentPrivacyAlert()
    func showPlaceholderMessage(isVisible: Bool)
    func popViewController()
    func presentProfile(profileImage: UIPPProfileImage, contact: PPContact)
    func presentPaymentRequestSuccess()
    func presentPaymentRequestError()
    func presentShouldFillValueAlert()
    func dismissViewController()
    func startLoading()
}

final class PaymentRequestCheckoutPresenter: PaymentRequestCheckoutPresenting {
    private let coordinator: PaymentRequestCheckoutCoordinating
    weak var viewController: PaymentRequestCheckoutDisplay?

    init(coordinator: PaymentRequestCheckoutCoordinating) {
        self.coordinator = coordinator
    }
    
    func displayProfile(with contact: PPContact) {
        let username = "@\(contact.username ?? "")"
        viewController?.displayProfile(with: contact, username: username)
    }
    
    func presentPrivacyAlert() {
        viewController?.displayPrivacyAlert()
    }
    
    func showPlaceholderMessage(isVisible: Bool) {
        let text = isVisible ? PaymentRequestLocalizable.messagePlaceholder.text : ""
        viewController?.showTextMessage(text)
    }
    
    func popViewController() {
        coordinator.perform(action: .back)
    }
    
    func presentProfile(profileImage: UIPPProfileImage, contact: PPContact) {
        coordinator.performShowProfile(profileImage: profileImage, contact: contact)
    }
    
    func presentPaymentRequestSuccess() {
        coordinator.presentSuccessDialog()
    }
    
    func presentPaymentRequestError() {
        viewController?.showError(
            with: StatefulErrorViewModel(
                image: Assets.P2P.dialogSadFace.image,
                content: (
                    title: PaymentRequestLocalizable.paymentRequestAlertTitleError.text,
                    description: PaymentRequestLocalizable.paymentRequestAlertTextError.text
                ),
                button: (image: nil, title: PaymentRequestLocalizable.paymentRequestAlertButtonTryAgain.text)
            )
        )
    }
    
    func presentShouldFillValueAlert() {
        let controller = UIAlertController(
            title: PaymentRequestLocalizable.paymentRequestShouldFillValueAlertTitle.text,
            message: String(),
            preferredStyle: .alert
        )
        let action = UIAlertAction(
            title: DefaultLocalizable.btOk.text,
            style: .default
        )
        controller.addAction(action)
        coordinator.present(alertController: controller)
    }
    
    func dismissViewController() {
        coordinator.perform(action: .close)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
}
