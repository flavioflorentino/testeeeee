enum PaymentRequestQrCodeAction {
    case back
    case present(UIViewController)
}

protocol PaymentRequestQrCodeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: PaymentRequestQrCodeAction)
}

final class PaymentRequestQrCodeCoordinator: PaymentRequestQrCodeCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: PaymentRequestQrCodeAction) {
        switch action {
        case .back:
            viewController?.navigationController?.popViewController(animated: true)
        case let .present(controller):
            viewController?.present(controller, animated: true)
        }
    }
}
