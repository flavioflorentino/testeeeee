protocol PaymentRequestQrCodeServicing {
    var username: String? { get }
}

final class PaymentRequestQrCodeService: PaymentRequestQrCodeServicing {
    typealias Dependencies = HasConsumerManager
    private let dependencies: Dependencies
    
    var username: String? {
        dependencies.consumerManager.consumer?.username
    }
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}
