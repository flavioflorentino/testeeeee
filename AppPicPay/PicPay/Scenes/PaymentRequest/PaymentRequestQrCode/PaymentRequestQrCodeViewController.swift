import AnalyticsModule
import UI
import UIKit

protocol PaymentRequestQrCodeDisplay: AnyObject {
    func showInformValueButton()
    func showPaymentValue(_ value: Double)
    func setUpPaymentCode(paymentValue: Double?)
    func displayLoader()
    func displayQrCodeSuccess()
    func displayError(icon: UIImage?, title: String, description: NSAttributedString, buttonTitle: String)
}

final class PaymentRequestQrCodeViewController: ViewController<PaymentRequestQrCodeInteracting & QrCodeDelegate, UIView> {
    private enum Layout {
        static let buttonHeight: CGFloat = 48.0
        static let margin: CGFloat = 16.0
        static let paymentCodeSize: CGFloat = UIScreen.main.bounds.width * (3 / 5)
        
        static let numberOfLines: Int = 0
        
        static let fontMessage: CGFloat = 16
        
        static let lineWidth: CGFloat = 1
        static let lineDashPattern: [NSNumber] = [2, 2]
    }
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.text = PaymentRequestLocalizable.paymentCodeScreenMessage.text
        label.numberOfLines = Layout.numberOfLines
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: Layout.fontMessage)
        label.textColor = Palette.ppColorGrayscale500.color
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var paymentCodeView: QrCodeView = {
        let qrCodeSize = CGSize(width: Layout.paymentCodeSize, height: Layout.paymentCodeSize)
        let view = QrCodeFactory.make(size: qrCodeSize, delegate: interactor)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var dashedLine: CAShapeLayer = {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = Palette.ppColorBranding300.cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = Layout.lineWidth
        shapeLayer.lineDashPattern = Layout.lineDashPattern
        return shapeLayer
    }()
    
    private lazy var valueView: ValueCurrencyStackView = {
        let view = ValueCurrencyStackView()
        view.valueTextField.isUserInteractionEnabled = false
        view.setupColorsForPayValue()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapValue))
        view.addGestureRecognizer(tapGesture)
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var informValueButton: UIPPButton = {
        let button = UIPPButton()
        button.configure(with: Button(title: PaymentRequestLocalizable.informValue.text, type: .cleanUnderlined))
        button.addTarget(self, action: #selector(didTapInformValue), for: .touchUpInside)
        button.isHidden = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var sendChargeButton: UIPPButton = {
        let button = UIPPButton()
        button.configure(with: Button(title: PaymentRequestLocalizable.shareCode.text, type: .cta))
        button.cornerRadius = Layout.buttonHeight / 2
        button.addTarget(self, action: #selector(didTapSendCharge), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addDottedLineUnderValueView()
    }
 
    override func buildViewHierarchy() {
        containerView.addSubview(messageLabel)
        containerView.addSubview(paymentCodeView)
        containerView.addSubview(valueView)
        containerView.addSubview(informValueButton)
        view.addSubview(containerView)
        view.addSubview(sendChargeButton)
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [containerView, sendChargeButton], constant: Layout.margin)
        NSLayoutConstraint.leadingTrailing(equalTo: containerView, for: [valueView, messageLabel, informValueButton])
        
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.topAnchor),
            containerView.bottomAnchor.constraint(equalTo: sendChargeButton.topAnchor, constant: -Layout.margin),
            
            sendChargeButton.bottomAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.bottomAnchor, constant: -Layout.margin),
            sendChargeButton.heightAnchor.constraint(equalToConstant: Layout.buttonHeight),
            
            paymentCodeView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            paymentCodeView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            paymentCodeView.widthAnchor.constraint(equalToConstant: Layout.paymentCodeSize),
            paymentCodeView.heightAnchor.constraint(equalToConstant: Layout.paymentCodeSize),
            
            messageLabel.bottomAnchor.constraint(equalTo: paymentCodeView.topAnchor, constant: -20),
            
            valueView.topAnchor.constraint(equalTo: paymentCodeView.bottomAnchor, constant: 20),
            
            informValueButton.topAnchor.constraint(equalTo: paymentCodeView.bottomAnchor, constant: 20)
        ])
    }
    
    override func configureViews() {
        navigationItem.title = PaymentRequestLocalizable.paymentRequest.text
        view.backgroundColor = Palette.ppColorGrayscale000.color
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale100.color
        interactor.showPaymentView()
        interactor.generatePaymentCode()
    }
    
    private func addDottedLineUnderValueView() {
        let path = CGMutablePath()
        let halfWidth = valueView.componentsWidth / 2
        let point1 = CGPoint(x: valueView.bounds.midX - halfWidth, y: valueView.bounds.maxY - 4)
        let point2 = CGPoint(x: valueView.bounds.midX + halfWidth, y: valueView.bounds.maxY - 4)
        path.addLines(between: [point1, point2])
        dashedLine.path = path
        valueView.layer.addSublayer(dashedLine)
    }
    
    @objc
    private func didTapValue() {
        Analytics.shared.log(PaymentRequestUserEvent.didTapValue)
        interactor.back()
    }
    
    @objc
    private func didTapInformValue() {
        Analytics.shared.log(PaymentRequestUserEvent.didTapInformValue)
        interactor.back()
    }
    
    @objc
    private func didTapSendCharge() {
        let codeImage = paymentCodeView.qrCodeImage
        let userImage = paymentCodeView.consumerImage
        interactor.sendCharge(withQrCode: codeImage, userImage: userImage)
    }
}

// MARK: View Model Outputs
extension PaymentRequestQrCodeViewController: PaymentRequestQrCodeDisplay {
    func showInformValueButton() {
        informValueButton.isHidden = false
        valueView.isHidden = true
    }
    
    func showPaymentValue(_ value: Double) {
        informValueButton.isHidden = true
        valueView.isHidden = false
        valueView.value = value
    }
    
    func setUpPaymentCode(paymentValue: Double?) {
        paymentCodeView.generateQrCode(amount: paymentValue)
    }
    
    func displayLoader() {
        beginState()
        messageLabel.isHidden = true
        sendChargeButton.isHidden = true
        informValueButton.isHidden = true
        valueView.isHidden = true
    }
    
    func displayQrCodeSuccess() {
        endState()
        messageLabel.isHidden = false
        sendChargeButton.isHidden = false
    }
    
    func displayError(icon: UIImage?, title: String, description: NSAttributedString, buttonTitle: String) {
        endState()
        let content = ApolloFeedbackViewContent(
            image: icon,
            title: title,
            description: description,
            primaryButtonTitle: buttonTitle,
            secondaryButtonTitle: String()
        )
        let errorView = ApolloFeedbackView(content: content)
        errorView.secondaryButtonIsHidden(true)
        errorView.didTapPrimaryButton = { [weak self] in
            errorView.removeFromSuperview()
            self?.interactor.generatePaymentCode()
        }
        view.addSubview(errorView)
        errorView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

// MARK: - StatefulProviding
extension PaymentRequestQrCodeViewController: StatefulProviding { }
