import AnalyticsModule

protocol PaymentRequestQrCodeInteracting {
    func generatePaymentCode()
    func showPaymentView()
    func back()
    func sendCharge(withQrCode: UIImage?, userImage: UIImage?)
}

final class PaymentRequestQrCodeInteractor: PaymentRequestQrCodeInteracting {
    private let presenter: PaymentRequestQrCodePresenting
    private let service: PaymentRequestQrCodeServicing
    
    private var paymentValue: Double
    
    init(service: PaymentRequestQrCodeServicing, presenter: PaymentRequestQrCodePresenting, paymentValue: Double = .zero) {
        self.service = service
        self.presenter = presenter
        self.paymentValue = paymentValue
    }
    
    func generatePaymentCode() {
        presenter.setUpPaymentCode(paymentValue: paymentValue.isZero ? nil : paymentValue)
    }
    
    func showPaymentView() {
        paymentValue.isZero ? presenter.showInformValueButton() : presenter.showPaymentValue(paymentValue)
    }
    
    func back() {
        presenter.perform(action: .back)
    }
    
    func sendCharge(withQrCode qrCode: UIImage?, userImage: UIImage?) {
        Analytics.shared.log(PaymentRequestUserEvent.didTapSendCharge)
        
        guard
            let username = service.username,
            let qrCode = qrCode,
            let userImage = userImage
            else {
                return
        }
        
        presenter.shareQrCode(
            withUsername: username,
            paymentValue: paymentValue,
            qrcode: qrCode,
            userImage: userImage
        )
    }
}

// MARK: - QrCodeDelegate
extension PaymentRequestQrCodeInteractor: QrCodeDelegate {
    func startRequest() {
        presenter.presentLoader()
    }
    
    func finishRequest(isSuccess: Bool) {
        guard isSuccess else {
            presenter.presentError()
            return
        }
        paymentValue.isZero ? presenter.showInformValueButton() : presenter.showPaymentValue(paymentValue)
        presenter.presentQrCodeSuccess()
    }
}
