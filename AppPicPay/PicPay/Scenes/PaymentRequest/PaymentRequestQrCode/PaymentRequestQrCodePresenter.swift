import AnalyticsModule
import AssetsKit
import UI
import UIKit

protocol PaymentRequestQrCodePresenting: AnyObject {
    var viewController: PaymentRequestQrCodeDisplay? { get set }
    func setUpPaymentCode(paymentValue: Double?)
    func showInformValueButton()
    func showPaymentValue(_ value: Double)
    func perform(action: PaymentRequestQrCodeAction)
    func shareQrCode(
        withUsername username: String,
        paymentValue: Double,
        qrcode: UIImage,
        userImage: UIImage
    )
    func presentLoader()
    func presentQrCodeSuccess()
    func presentError()
}

final class PaymentRequestQrCodePresenter: PaymentRequestQrCodePresenting {
    private let coordinator: PaymentRequestQrCodeCoordinating
    weak var viewController: PaymentRequestQrCodeDisplay?
    
    init(coordinator: PaymentRequestQrCodeCoordinating) {
        self.coordinator = coordinator
    }
    
    func setUpPaymentCode(paymentValue: Double?) {
        viewController?.setUpPaymentCode(paymentValue: paymentValue)
    }
    
    func showInformValueButton() {
        viewController?.showInformValueButton()
    }
    
    func showPaymentValue(_ value: Double) {
        viewController?.showPaymentValue(value)
    }
    
    func perform(action: PaymentRequestQrCodeAction) {
        coordinator.perform(action: action)
    }
    
    func shareQrCode(
        withUsername username: String,
        paymentValue: Double,
        qrcode: UIImage,
        userImage: UIImage
    ) {
        guard
            let shareImageController = activityToShareImage(
                with: username,
                qrCode: qrcode,
                userImage: userImage
            )
            else {
                return
        }
        coordinator.perform(action: .present(shareImageController))
    }
    
    func presentLoader() {
        viewController?.displayLoader()
    }
    
    func presentQrCodeSuccess() {
        viewController?.displayQrCodeSuccess()
    }
    
    func presentError() {
        let description = NSAttributedString(
            string: PaymentRequestLocalizable.qrCodeUnavailable.text,
            attributes: [
                .font: Typography.bodyPrimary().font(),
                .foregroundColor: Colors.grayscale700.color
            ]
        )
        viewController?.displayError(
            icon: Resources.Illustrations.iluFalling.image,
            title: PaymentRequestLocalizable.somethingWentWrong.text,
            description: description,
            buttonTitle: DefaultLocalizable.btOkUnderstood.text
        )
    }
}

private extension PaymentRequestQrCodePresenter {
    func activityToShareImage(with username: String, qrCode: UIImage, userImage: UIImage?) -> UIActivityViewController? {
        guard
            let views = Bundle.main.loadNibNamed(ShareCodeView.identifier, owner: nil, options: nil),
            let shareCodeView = views.first as? ShareCodeView
            else {
                return nil
        }
        shareCodeView.setup(username: username, qrCodeImage: qrCode, userImage: userImage)
        
        guard let snapshot = shareCodeView.snapshot else {
            return nil
        }
        return UIActivityViewController(activityItems: [snapshot], applicationActivities: nil)
    }
    
    func generatePaymentLink(with username: String, paymentValue: Double) -> String {
       var link = String.ppMeLink(username: username)
       if !paymentValue.isZero {
           let stringValue = String(format: "%.2f", paymentValue).replacingOccurrences(of: ".", with: ",")
           link += "/" + stringValue + "?fixed_value=true"
       }
       return link
    }
}
