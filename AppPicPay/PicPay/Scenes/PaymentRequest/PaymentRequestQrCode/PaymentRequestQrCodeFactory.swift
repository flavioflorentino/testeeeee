enum PaymentRequestQrCodeFactory {
    static func make(withPaymentValue value: Double = .zero) -> PaymentRequestQrCodeViewController {
        let service: PaymentRequestQrCodeServicing = PaymentRequestQrCodeService(dependencies: DependencyContainer())
        let coordinator: PaymentRequestQrCodeCoordinating = PaymentRequestQrCodeCoordinator()
        let presenter: PaymentRequestQrCodePresenting = PaymentRequestQrCodePresenter(coordinator: coordinator)
        let interactor = PaymentRequestQrCodeInteractor(service: service, presenter: presenter, paymentValue: value)
        let viewController = PaymentRequestQrCodeViewController(interactor: interactor)
        
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
