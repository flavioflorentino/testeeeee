import AnalyticsModule
import UI
import UIKit

protocol PaymentRequestValueDisplay: AnyObject {
    func displayConfirmState(isEnabled: Bool)
}

extension PaymentRequestValueViewController.Layout {
    enum Size {
        static let buttonHeight: CGFloat = 48.0
    }
    
    enum Text {
        static let fontMessage = UIFont.systemFont(ofSize: 14)
        static let numberOfLines: Int = 0
    }
}

final class PaymentRequestValueViewController: ViewController<PaymentRequestValueViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    // MARK: - Variables
    private lazy var closeButton: UIBarButtonItem = {
        let barButton = UIBarButtonItem(
            image: Assets.back.image,
            style: .plain,
            target: self,
            action: #selector(didTapCloseButton)
        )
        barButton.tintColor = Palette.ppColorBranding300.color
        return barButton
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.text = PaymentRequestLocalizable.valueScreenMessage.text
        label.numberOfLines = Layout.Text.numberOfLines
        label.textAlignment = .center
        label.font = Layout.Text.fontMessage
        label.textColor = Palette.ppColorGrayscale500.color
        return label
    }()
    
    private lazy var valueField: ValueCurrencyStackView = {
        let field = ValueCurrencyStackView()
        field.onTextChange = { [weak self] value in
            self?.viewModel.newValue(value)
        }
        return field
    }()
    
    private lazy var valueContainerView: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var confirmButton: UIPPButton = {
        let button = UIPPButton()
        let buttonTitle = type == .link ? PaymentRequestLocalizable.shareLink.text : DefaultLocalizable.btConfirm.text
        button.configure(with: Button(title: buttonTitle, type: .cta))
        button.cornerRadius = Layout.Size.buttonHeight / 2
        button.isEnabled = false
        button.addTarget(self, action: #selector(didTapConfirmButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var doNotSetValueButton: UIPPButton = {
        let button = UIPPButton()
        button.configure(with: Button(title: PaymentRequestLocalizable.doNotInformValue.text, type: .cleanUnderlined))
        button.addTarget(self, action: #selector(didTapDoNotSetValueButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var buttonsStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Spacing.base01
        return stack
    }()
    
    private lazy var bottomConstraintButtonsStackView = buttonsStackView.bottomAnchor.constraint(
        equalTo: view.compatibleSafeAreaLayoutGuide.bottomAnchor,
        constant: -Spacing.base02
    )
    
    private let origin: String
    private let type: PaymentRequestExternalType
    
    // MARK: - Life Cycle
    init(viewModel: PaymentRequestValueViewModelInputs, type: PaymentRequestExternalType, origin: String) {
        self.origin = origin
        self.type = type
        super.init(viewModel: viewModel)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addKeyboardObservers()
        Analytics.shared.log(PaymentRequestUserEvent.didStartValueScreen(origin: origin))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        valueField.valueTextField.becomeFirstResponder()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(messageLabel)
        valueContainerView.addSubview(valueField)
        view.addSubview(valueContainerView)
        buttonsStackView.addArrangedSubview(confirmButton)
        buttonsStackView.addArrangedSubview(doNotSetValueButton)
        view.addSubview(buttonsStackView)
    }
    
    override func configureViews() {
        navigationItem.title = PaymentRequestLocalizable.paymentRequest.text
        view.backgroundColor = Palette.ppColorGrayscale000.color
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale100.color
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        if navigationController?.viewControllers.first is Self {
            navigationItem.leftBarButtonItem = closeButton
        }
    }
    
    override func setupConstraints() {
        messageLabel.layout {
            $0.top == view.compatibleSafeAreaLayoutGuide.topAnchor + Spacing.base04
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
        }
        
        valueField.layout {
            $0.centerY == valueContainerView.centerYAnchor
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
        }
        
        valueContainerView.layout {
            $0.top == messageLabel.bottomAnchor
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
            $0.bottom == buttonsStackView.topAnchor
        }
        
        buttonsStackView.layout {
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
        }
        
        confirmButton.layout {
            $0.height == Layout.Size.buttonHeight
        }
        
        doNotSetValueButton.layout {
            $0.height == Layout.Size.buttonHeight
        }
        
        NSLayoutConstraint.activate([
            bottomConstraintButtonsStackView
        ])
    }
    
    // MARK: - Actions
    @objc
    private func didTapCloseButton() {
        viewModel.dismiss()
    }
    
    @objc
    private func didTapConfirmButton() {
        viewModel.next()
    }
    
    @objc
    private func didTapDoNotSetValueButton() {
        viewModel.nextWithoutValue()
    }
    
    // MARK: - Keyboard
    private func addKeyboardObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    @objc
    private func keyboardWillShow(notification: NSNotification) {
        guard
            let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue,
            let offset = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            else {
                return
        }
        let offsetValue = keyboardSize.height == offset.height ? -keyboardSize.height : -offset.height
        let constant = -Spacing.base02 + offsetValue
        bottomConstraintButtonsStackView.constant = constant
        view.layoutIfNeeded()
    }
    
    @objc
    private func keyboardWillHide(notification: NSNotification) {
        bottomConstraintButtonsStackView.constant = -Spacing.base02
        view.layoutIfNeeded()
    }
}

// MARK: - PaymentRequestValueDisplay
extension PaymentRequestValueViewController: PaymentRequestValueDisplay {
    func displayConfirmState(isEnabled: Bool) {
        confirmButton.isEnabled = isEnabled
    }
}
