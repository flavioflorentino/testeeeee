import AnalyticsModule

protocol PaymentRequestValueViewModelInputs {
    func dismiss()
    func next()
    func nextWithoutValue()
    func newValue(_ value: Double)
}

final class PaymentRequestValueViewModel {
    // MARK: - Variables
    private var value: Double = .zero
    private let isBackActionEnabled: Bool
    private let presenter: PaymentRequestValuePresenting
    
    // MARK: - Life Cycle
    init(presenter: PaymentRequestValuePresenting, isBackActionEnabled: Bool) {
        self.presenter = presenter
        self.isBackActionEnabled = isBackActionEnabled
    }
}

// MARK: - PaymentRequestValueViewModelInputs
extension PaymentRequestValueViewModel: PaymentRequestValueViewModelInputs {
    func dismiss() {
        presenter.perform(action: isBackActionEnabled ? .back : .close)
    }
    
    func next() {
        Analytics.shared.log(PaymentRequestUserEvent.didInformValue)
        presenter.perform(action: .confirmValue(value))
    }
    
    func nextWithoutValue() {
        Analytics.shared.log(PaymentRequestUserEvent.didNotInformValue)
        presenter.perform(action: .doNotSetValue)
    }
    
    func newValue(_ value: Double) {
        self.value = value
        presenter.presentConfirmState(isEnabled: value > .zero)
    }
}
