import Foundation

protocol PaymentRequestValuePresenting: AnyObject {
    func perform(action: PaymentRequestValueAction)
    func presentConfirmState(isEnabled: Bool)
}

final class PaymentRequestValuePresenter {
    // MARK: - Variables
    private let coordinator: PaymentRequestValueCoordinating
    weak var viewController: PaymentRequestValueDisplay?
    
    // MARK: - Life Cycle
    init(coordinator: PaymentRequestValueCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - PaymentRequestValuePresenting
extension PaymentRequestValuePresenter: PaymentRequestValuePresenting {
    func perform(action: PaymentRequestValueAction) {
        coordinator.perform(action: action)
    }
    
    func presentConfirmState(isEnabled: Bool) {
        viewController?.displayConfirmState(isEnabled: isEnabled)
    }
}
