enum PaymentRequestValueFactory {
    static func make(
        origin: DGHelpers.Origin,
        type: PaymentRequestExternalType,
        hasNavigationController: Bool = true
    ) -> UIViewController {
        let coordinator = PaymentRequestValueCoordinator(type: type, dependencies: DependencyContainer())
        let presenter = PaymentRequestValuePresenter(coordinator: coordinator)
        let viewModel = PaymentRequestValueViewModel(presenter: presenter, isBackActionEnabled: !hasNavigationController)
        let viewController = PaymentRequestValueViewController(viewModel: viewModel, type: type, origin: origin.rawValue)
        presenter.viewController = viewController
        coordinator.viewController = viewController

        guard hasNavigationController else {
            return viewController
        }
        return UINavigationController(rootViewController: viewController)
    }
}
