import AnalyticsModule
import Foundation
import UIKit

enum PaymentRequestValueAction {
    case close
    case back
    case confirmValue(Double)
    case doNotSetValue
}

protocol PaymentRequestValueCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: PaymentRequestValueAction)
}

final class PaymentRequestValueCoordinator: PaymentRequestValueCoordinating {
    typealias Dependencies = HasConsumerManager
    
    weak var viewController: UIViewController?
    private let type: PaymentRequestExternalType
    private let dependencies: Dependencies
    
    init(type: PaymentRequestExternalType, dependencies: Dependencies) {
        self.type = type
        self.dependencies = dependencies
    }
    
    func perform(action: PaymentRequestValueAction) {
        switch action {
        case .close:
            viewController?.navigationController?.dismiss(animated: true)
        case .back:
            viewController?.navigationController?.popViewController(animated: true)
        case .confirmValue(let value):
            switch type {
            case .link:
                presentShareLinkControllerWithValue(value)
            case .qrCode:
                let paymentCodeController = PaymentRequestQrCodeFactory.make(withPaymentValue: value)
                viewController?.navigationController?.pushViewController(paymentCodeController, animated: true)
            }
        case .doNotSetValue:
            switch type {
            case .link:
                presentShareLinkControllerWithValue(Double.zero)
            case .qrCode:
                let paymentCodeController = PaymentRequestQrCodeFactory.make()
                viewController?.navigationController?.pushViewController(paymentCodeController, animated: true)
            }
        }
    }
}

private extension PaymentRequestValueCoordinator {
    // MARK: - presentShareLinkControllerWithValue
    func presentShareLinkControllerWithValue(_ value: Double) {
        guard let username = dependencies.consumerManager.consumer?.username else {
            return
        }
        
        Analytics.shared.log(PaymentRequestUserEvent.didTapSendCharge)
        
        let stringItem: String
        if value.isZero {
            stringItem = String(format: PaymentRequestLocalizable.shareLinkMessageNoValue.text, username)
        } else {
            let stringValue = String(format: "%.2f", value).replacingOccurrences(of: ".", with: ",")
            stringItem = String(format: PaymentRequestLocalizable.shareLinkMessageWithValue.text, username, stringValue, stringValue, username)
        }
        let controller = UIActivityViewController(activityItems: [stringItem], applicationActivities: nil)
        viewController?.present(controller, animated: true)
    }
}
