import Foundation

protocol UsersListComponentPresenting: AnyObject {
    var view: UsersListComponentDisplay? { get set }
    func displayUsers(_ users: [UserListComponentModel], title: String?)
    func presentEmptyStateWithMessage(_ message: String, title: String?)
    func checkSelected(rows: [Int])
    func deselectUser(at row: Int)
}

final class UsersListComponentPresenter: UsersListComponentPresenting {
    weak var view: UsersListComponentDisplay?

    func displayUsers(_ users: [UserListComponentModel], title: String?) {
        let mappedUsers = users.map(UserListCellType.default)
        view?.displayData(type: .list, section: .init(header: title, items: mappedUsers))
    }
    
    func presentEmptyStateWithMessage(_ message: String, title: String?) {
        view?.displayData(type: .single, section: .init(header: title, items: [.empty(message: message)]))
    }
    
    func checkSelected(rows: [Int]) {
        view?.checkSelected(at: rows)
    }
    
    func deselectUser(at row: Int) {
        view?.deselectUser(at: row)
    }
}
