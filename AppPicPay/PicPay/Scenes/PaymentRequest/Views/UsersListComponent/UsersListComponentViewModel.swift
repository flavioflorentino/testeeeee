import Foundation

public protocol UsersListComponentViewModelDelegate: AnyObject {
    func didRemoveUser(_ user: UserListComponentModel)
    func didAddUser(_ user: UserListComponentModel)
    func blockedToSelectANewUser()
    func didTryAgainOnError()
    func didScrollReachBottomList()
    func didTouchOnRequestPermission()
}

protocol UsersListComponentViewModelInputs: AnyObject {
    func displayUsers(_ users: [UserListComponentModel], title: String?)
    func displayEmptyStateWithMessage(_ message: String, title: String?)
    func deselectUserWithId(_ id: String)
    @discardableResult
    func updateUserListSelection(at index: Int) -> Bool
    func didTryAgainOnError()
    func didScrollReachBottomWithType(_ type: UsersListComponentDataType)
    func didTouchOnRequestPermission()
}

final class UsersListComponentViewModel {
    private let presenter: UsersListComponentPresenting
    private weak var delegate: UsersListComponentViewModelDelegate?
    private let maxUsersSelected: Int
    private var users: [UserListComponentModel]
    private var selectedUsersIDs: [String: Bool]

    init(
        presenter: UsersListComponentPresenting,
        delegate: UsersListComponentViewModelDelegate? = nil,
        maxUsersSelected: Int,
        users: [UserListComponentModel] = [],
        selectedUsersIDs: [String: Bool] = [:]
    ) {
        self.presenter = presenter
        self.delegate = delegate
        self.maxUsersSelected = maxUsersSelected
        self.users = users
        self.selectedUsersIDs = selectedUsersIDs
    }
}

extension UsersListComponentViewModel: UsersListComponentViewModelInputs {
    func displayUsers(_ users: [UserListComponentModel], title: String?) {
        self.users = users
        presenter.displayUsers(users, title: title)
        checkSelectedUsers()
    }
    
    func displayEmptyStateWithMessage(_ message: String, title: String?) {
        presenter.presentEmptyStateWithMessage(message, title: title)
    }
    
    @discardableResult
    func updateUserListSelection(at index: Int) -> Bool {
        guard
            index < users.count,
            users[index].paymentRequestAllowed
            else {
                return false
            }
        let user = users[index]
        if user.isSelected {
            return deselectUserWithIndex(index)
        } else {
            return selectUserWithIndex(index)
        }
    }
    
    func deselectUserWithId(_ id: String) {
        if selectedUsersIDs[id] != nil {
            selectedUsersIDs.removeValue(forKey: id)
        }
        guard let index = users.firstIndex(where: {
            $0.id == id
        }) else {
            return
        }
        guard users[index].isSelected else {
            return
        }
        deselectUserWithIndex(index)
        presenter.deselectUser(at: index)
    }
    
    func didTryAgainOnError() {
        delegate?.didTryAgainOnError()
    }
    
    func didScrollReachBottomWithType(_ type: UsersListComponentDataType) {
        guard type == .list else {
            return
        }
        delegate?.didScrollReachBottomList()
    }
    
    func didTouchOnRequestPermission() {
        delegate?.didTouchOnRequestPermission()
    }
    
    // MARK: - Private functions
    private func checkSelectedUsers() {
        let selectedIndexes = users.indices.filter { users[$0].isSelected }
        presenter.checkSelected(rows: selectedIndexes)
    }
    
    @discardableResult
    private func deselectUserWithIndex(_ index: Int) -> Bool {
        selectedUsersIDs.removeValue(forKey: users[index].id)
        users[index].isSelected = false
        delegate?.didRemoveUser(users[index])
        return true
    }
    
    private func selectUserWithIndex(_ index: Int) -> Bool {
        guard selectedUsersIDs.count < maxUsersSelected else {
            delegate?.blockedToSelectANewUser()
            return false
        }
        selectedUsersIDs[users[index].id] = true
        users[index].isSelected = true
        delegate?.didAddUser(users[index])
        return true
    }
}
