import UIKit

public struct UserListComponentModel {
    public let id: String
    public let username: String
    let fullName: String
    public let urlImage: URL?
    let paymentRequestAllowed: Bool
    var isSelected: Bool
    
    public init(
        id: String,
        username: String,
        fullName: String,
        urlImage: URL?,
        paymentRequestAllowed: Bool,
        isSelected: Bool
    ) {
        self.id = id
        self.username = username
        self.fullName = fullName
        self.urlImage = urlImage
        self.paymentRequestAllowed = paymentRequestAllowed
        self.isSelected = isSelected
    }
}
