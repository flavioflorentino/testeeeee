import UI
import UIKit

protocol UserListing: UIView {
    var tableHeaderView: UIView? { get set }
    func displayUsers(_ users: [UserListComponentModel], title: String?)
    func displayEmptyStateWithMessage(_ message: String, title: String?)
    func displayRequestContactPermission()
    func displayLoading()
    func displayListLoading()
    func hideListLoading()
    func displayErrorWith(message: String, buttonText: String)
    func deselectUser(with id: String)
}

enum UserListCellType {
    case `default`(UserListComponentModel)
    case loading
    case empty(message: String)
    case error(message: String, buttonText: String)
    case requestPermission
}

enum UsersListComponentDataType {
    case list
    case single
}

final class UsersListComponentView: UIView {
    private enum Layout {
        static let rowHeight: CGFloat = 72
    }
    
    // MARK: - Public vars
    var cellErrorMessage: String?
    var requestPermissionClosure: (() -> Void)?
    
    // MARK: - Private vars
    private let viewModel: UsersListComponentViewModelInputs
    private var tableViewHandler: TableViewHandler<String, UserListCellType, UsersListComponentViewCell>?
    private var currentType: UsersListComponentDataType = .single
    
    private lazy var usersTableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = Colors.backgroundPrimary.color
        tableView.separatorStyle = .none
        tableView.allowsMultipleSelection = true
        tableView.keyboardDismissMode = .onDrag
        tableView.register(
            UsersListComponentViewCell.self,
            forCellReuseIdentifier: String(describing: UsersListComponentViewCell.self)
        )
        tableView.register(
            UsersListComponentLoadingViewCell.self,
            forCellReuseIdentifier: String(describing: UsersListComponentLoadingViewCell.self)
        )
        tableView.register(
            UsersListComponentErrorViewCell.self,
            forCellReuseIdentifier: String(describing: UsersListComponentErrorViewCell.self)
        )
        tableView.register(
            UsersListComponentPermissionViewCell.self,
            forCellReuseIdentifier: String(describing: UsersListComponentPermissionViewCell.self)
        )
        return tableView
    }()
    
    // MARK: - Life Cycle
    init(
        frame: CGRect = .zero,
        viewModel: UsersListComponentViewModelInputs
    ) {
        self.viewModel = viewModel
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration
extension UsersListComponentView: ViewConfiguration {
    public func setupConstraints() {
        usersTableView.snp.makeConstraints {
            $0.edges.equalTo(self)
        }
    }
    
    public func buildViewHierarchy() {
        addSubview(usersTableView)
    }
    
    public func configureViews() {
        backgroundColor = Colors.groupedBackgroundPrimary.color
    }
}

// MARK: - UserListing
extension UsersListComponentView: UserListing {
    var tableHeaderView: UIView? {
        get {
            usersTableView.tableHeaderView
        }
        set {
            usersTableView.tableHeaderView = newValue
        }
    }
    
    func displayRequestContactPermission() {
        displayData(type: .list, section: .init(items: [.requestPermission]))
    }
    
    func displayLoading() {
        displayData(type: .single, section: .init(items: [.loading]))
    }
    
    func displayListLoading() {
        let view = UsersListComponentLoadingViewCell()
        view.configureCell()
        view.backgroundColor = Colors.groupedBackgroundSecondary.color
        usersTableView.tableFooterView = view
    }
    
    func hideListLoading() {
        usersTableView.tableFooterView = nil
    }
    
    func displayErrorWith(message: String, buttonText: String) {
        displayData(
            type: .single,
            section: .init(items: [.error(message: message, buttonText: buttonText)])
        )
    }
    
    func displayUsers(_ users: [UserListComponentModel], title: String?) {
        viewModel.displayUsers(users, title: title)
    }
    
    func displayEmptyStateWithMessage(_ message: String, title: String?) {
        viewModel.displayEmptyStateWithMessage(message, title: title)
    }
    
    func deselectUser(with id: String) {
        viewModel.deselectUserWithId(id)
    }
}

// MARK: UsersListComponentDisplay
extension UsersListComponentView: UsersListComponentDisplay {
    func displayData(type: UsersListComponentDataType, section: Section<String, UserListCellType>) {
        configureTableViewWithDataType(type)
        tableViewHandler = TableViewHandler(
            data: [section],
            cellIdentifier: { [weak self] _, type in
                self?.cellIdentifierFor(type: type) ?? String()
            }, configureMultiCell: { [weak self] _, type, cell in
                self?.configure(cell: cell, withType: type)
            }, configureSection: { _, section in
                guard let title = section.header else {
                    return nil
                }
                return .view(UsersListComponentViewHeader(title: title))
            }, configureDidSelectRow: { [weak self] indexPath, _ in
                guard let self = self else {
                    return
                }
                if self.viewModel.updateUserListSelection(at: indexPath.row) == false {
                    self.usersTableView.deselectRow(at: indexPath, animated: false)
                }
            }, configureDidDeselectRow: { [weak self] indexPath, _ in
                self?.viewModel.updateUserListSelection(at: indexPath.row)
            }, configureCellHeightHandler: { type in
                switch type {
                case .default:
                    return .custom(Layout.rowHeight)
                case .loading, .empty, .error:
                    return .full
                case .requestPermission:
                    return .custom()
                }
            }, scrollViewDidScrollHandler: { [weak self] in
                guard let self = self else {
                    return
                }
                if self.usersTableView.contentOffset.y >= (self.usersTableView.contentSize.height - self.usersTableView.frame.size.height) {
                    self.viewModel.didScrollReachBottomWithType(self.currentType)
                }
            }
        )
        usersTableView.dataSource = tableViewHandler
        usersTableView.delegate = tableViewHandler
        usersTableView.reloadData()
    }
    
    func checkSelected(at rows: [Int]) {
        rows.forEach { row in
            usersTableView.selectRow(
                at: IndexPath(row: row, section: 0),
                animated: false,
                scrollPosition: .none
            )
        }
    }
    
    func deselectUser(at row: Int) {
        usersTableView.deselectRow(
            at: IndexPath(row: row, section: 0),
            animated: true
        )
    }
}

private extension UsersListComponentView {
    func configureTableViewWithDataType(_ type: UsersListComponentDataType) {
        currentType = type
        switch type {
        case .list:
            usersTableView.allowsMultipleSelection = true
        case .single:
            usersTableView.allowsSelection = false
        }
    }
    
    func cellIdentifierFor(type: UserListCellType) -> String {
        switch type {
        case .default:
            return String(describing: UsersListComponentViewCell.self)
        case .loading:
            return String(describing: UsersListComponentLoadingViewCell.self)
        case .empty, .error:
            return String(describing: UsersListComponentErrorViewCell.self)
        case .requestPermission:
            return String(describing: UsersListComponentPermissionViewCell.self)
        }
    }
    
    func configure(cell: UITableViewCell, withType type: UserListCellType) {
        switch type {
        case let .default(user):
            guard let cell = cell as? UsersListComponentViewCell else {
                return
            }
            cell.warningMessage = cellErrorMessage
            cell.configureWith(
                urlImage: user.urlImage,
                username: user.username,
                fullName: user.fullName,
                isEnabled: user.paymentRequestAllowed
            )
            
        case .loading:
            guard let cell = cell as? UsersListComponentLoadingViewCell else {
                return
            }
            cell.configureCell()
        case let .empty(text):
            guard let cell = cell as? UsersListComponentErrorViewCell else {
                return
            }
            cell.configureCellWith(text: text)
        case let .error(message, buttonText):
            guard let cell = cell as? UsersListComponentErrorViewCell else {
                return
            }
            cell.configureCellWith(text: message, buttonText: buttonText)
            cell.delegate = self
        case .requestPermission:
            guard let cell = cell as? UsersListComponentPermissionViewCell else {
                return
            }
            cell.configureCell()
            cell.delegate = self
        }
    }
}

extension UsersListComponentView: UserListComponentErrorViewCellDelegate {
    public func didTouchOnButton() {
        viewModel.didTryAgainOnError()
    }
}

extension UsersListComponentView: UsersListComponentPermissionViewCellDelegate {
    func didTouchOnRequestPermission() {
        viewModel.didTouchOnRequestPermission()
    }
}
