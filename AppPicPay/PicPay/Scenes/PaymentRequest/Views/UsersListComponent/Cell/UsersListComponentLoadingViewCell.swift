import UI
import UIKit

public final class UsersListComponentLoadingViewCell: UITableViewCell {
    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(style: .whiteLarge)
        activityView.color = Colors.grayscale500.color
        return activityView
    }()
    
    func configureCell() {
        backgroundColor = Colors.groupedBackgroundPrimary.color
        buildLayout()
        activityIndicatorView.startAnimating()
    }
}

extension UsersListComponentLoadingViewCell: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(activityIndicatorView)
    }
    
    public func configureViews() {
        selectionStyle = .none
    }
    
    public func setupConstraints() {
        activityIndicatorView.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
}
