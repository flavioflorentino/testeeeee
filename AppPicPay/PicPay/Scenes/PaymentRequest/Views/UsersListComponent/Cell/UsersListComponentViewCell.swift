import UI
import UIKit

protocol UserListComponentViewCellProtocol: UITableViewCell {
    func configureWith(
        urlImage: URL?,
        username: String,
        fullName: String,
        isEnabled: Bool
    )
}

private extension UsersListComponentViewCell.Layout {
    static let iconSide: CGFloat = 48
    static let stackViewSpacing: CGFloat = 12.0
    static let iconImageViewCornerRadius: CGFloat = iconSide / 2
}

final class UsersListComponentViewCell: UITableViewCell {
    // MARK: - Layout
    fileprivate enum Layout { }
    
    // MARK: - Public vars
    var warningMessage: String? {
        get {
            warningLabel.text
        }
        
        set {
            warningLabel.text = newValue
        }
    }
    
    // MARK: - Visual Components
    private lazy var contentBackgroundView = UIView()
    
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Layout.stackViewSpacing
        stackView.alignment = .center
        stackView.axis = .horizontal
        return stackView
    }()
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = Layout.iconImageViewCornerRadius
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    private lazy var textStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        return stackView
    }()
    
    private lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.bodyPrimary(.highlight).font()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var fullNameLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.caption().font()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var warningLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.caption().font()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var checkmarkImageView: UIImageView = {
        let imageView = UIImageView(image: Assets.icoCheck.image)
        imageView.tintColor = Colors.success600.color
        return imageView
    }()
    
    // MARK: - Life Cycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
        selectionStyle = .none
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        warningLabel.removeFromSuperview()
    }
    
    // MARK: - Private functions
    private func configureDisabledLayout() {
        contentBackgroundView.backgroundColor = Colors.grayscale050.color
        usernameLabel.textColor = Colors.grayscale500.color
        fullNameLabel.textColor = Colors.grayscale600.color
        warningLabel.textColor = Colors.critical600.color
        textStackView.addArrangedSubview(warningLabel)
    }
    
    private func configureEnabledLayout() {
        contentBackgroundView.backgroundColor = Colors.backgroundPrimary.color
        usernameLabel.textColor = Colors.grayscale700.color
        fullNameLabel.textColor = Colors.grayscale600.color
    }
}

extension UsersListComponentViewCell: ViewConfiguration {
    func buildViewHierarchy() {
       contentView.addSubview(contentBackgroundView)
       contentBackgroundView.addSubview(contentStackView)
       contentStackView.addArrangedSubview(iconImageView)
       contentStackView.addArrangedSubview(textStackView)
       textStackView.addArrangedSubview(usernameLabel)
       textStackView.addArrangedSubview(fullNameLabel)
   }
    
    func setupConstraints() {
        contentBackgroundView.layout {
            $0.top == topAnchor
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.bottom == bottomAnchor
        }
        
        contentStackView.layout {
            $0.top == topAnchor
            $0.bottom == bottomAnchor
            $0.leading == leadingAnchor + Spacing.base02
            $0.trailing == trailingAnchor - Spacing.base02
        }
        
        iconImageView.layout {
            $0.height == Layout.iconSide
            $0.width == Layout.iconSide
        }
    }
}

extension UsersListComponentViewCell: UserListComponentViewCellProtocol {
    // MARK: - Content
    func configureWith(
        urlImage: URL?,
        username: String,
        fullName: String,
        isEnabled: Bool
    ) {
        iconImageView.setImage(url: urlImage, placeholder: Assets.NewGeneration.avatarPerson.image)
        usernameLabel.text = username
        fullNameLabel.text = fullName
        updateCheckmark(isSelected: isSelected)
        if isEnabled {
            configureEnabledLayout()
        } else {
            configureDisabledLayout()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
       super.setSelected(selected, animated: animated)
       updateCheckmark(isSelected: selected)
    }
    
    func updateCheckmark(isSelected: Bool) {
        accessoryView = isSelected ? checkmarkImageView : .none
    }
}
