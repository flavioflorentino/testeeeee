import PermissionsKit
import UIKit
import UI

protocol UsersListComponentPermissionViewCellDelegate: AnyObject {
    func didTouchOnRequestPermission()
}

final class UsersListComponentPermissionViewCell: UITableViewCell {
    // MARK: - Public vars
    weak var delegate: UsersListComponentPermissionViewCellDelegate?
    
    // MARK: - Private vars
    private lazy var requestPermissionView = PermissionRequestView(
        setup: .full(requestType: .contacts(), buttonStyle: .clear, authorizeHandler: { [weak self] in
            self?.delegate?.didTouchOnRequestPermission()
        })
    )
    
    // MARK: - Public functions
    func configureCell() {
        backgroundColor = Colors.backgroundPrimary.color
        buildLayout()
    }
}

// MARK: - View Configuration
extension UsersListComponentPermissionViewCell: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(requestPermissionView)
    }
    
    func setupConstraints() {
        requestPermissionView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}
