import UI
import UIKit

extension UsersListComponentViewHeader.Layout {
    static let titleFont = Typography.caption(.highlight).font()
    static let marginBottom: CGFloat = 12.0
}

final class UsersListComponentViewHeader: UIView {
    // MARK: - Layout
    fileprivate enum Layout { }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.titleFont
        label.textColor = Colors.grayscale700.color
        return label
    }()
    
    // MARK: - Life Cycle
    
    init(frame: CGRect = .zero, title: String?) {
        super.init(frame: frame)
        titleLabel.text = title
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension UsersListComponentViewHeader: ViewConfiguration {
    func setupConstraints() {
        titleLabel.layout {
            $0.top == topAnchor + Spacing.base02
            $0.bottom == bottomAnchor - Layout.marginBottom
            $0.leading == leadingAnchor + Spacing.base02
        }
    }
    
    func buildViewHierarchy() {
        addSubview(titleLabel)
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}
