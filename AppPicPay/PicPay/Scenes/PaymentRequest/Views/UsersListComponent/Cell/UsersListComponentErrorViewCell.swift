import UIKit
import UI

public protocol UserListComponentErrorViewCellDelegate: AnyObject {
    func didTouchOnButton()
}

public final class UsersListComponentErrorViewCell: UITableViewCell {
    // MARK: - Public vars
    public weak var delegate: UserListComponentErrorViewCellDelegate?
    
    // MARK: - Private vars
    private lazy var label: UILabel = {
        let view = UILabel()
        view
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, .grayscale400())
        view.backgroundColor = Colors.backgroundPrimary.color
        return view
    }()
    
    private lazy var button: UIButton = {
        let view = UIButton()
        view.buttonStyle(SecondaryButtonStyle())
        view.addTarget(self, action: #selector(didTouchOnButton), for: .touchUpInside)
        return view
    }()
    
    private lazy var stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = Spacing.base02
        view.distribution = .fill
        return view
    }()
    
    // MARK: - Lifecycle
    override public func prepareForReuse() {
        super.prepareForReuse()
        button.removeFromSuperview()
    }
    
    // MARK: - Public functions
    public func configureCellWith(text: String?, buttonText: String? = nil) {
        backgroundColor = Colors.backgroundPrimary.color
        label.text = text
        buildLayout()
        
        guard let buttonText = buttonText else {
            return
        }
        configureButtonText(buttonText)
    }
}

// MARK: - Private functions
private extension UsersListComponentErrorViewCell {
    @objc
    func didTouchOnButton() {
        delegate?.didTouchOnButton()
    }
    
    private func configureButtonText(_ text: String) {
        button.setTitle(text, for: .normal)
        stackView.addArrangedSubview(button)
    }
}

// MARK: - View Configuration
extension UsersListComponentErrorViewCell: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(stackView)
        stackView.addArrangedSubview(label)
    }
    
    public func configureViews() {
        selectionStyle = .none
    }
    
    public func setupConstraints() {
        stackView.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
}
