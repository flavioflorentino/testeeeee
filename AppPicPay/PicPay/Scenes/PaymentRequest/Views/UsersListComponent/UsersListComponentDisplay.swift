import Foundation
import UI

protocol UsersListComponentDisplay: AnyObject {
    func displayData(type: UsersListComponentDataType, section: Section<String, UserListCellType>)
    func checkSelected(at rows: [Int])
    func deselectUser(at row: Int)
}
