import Foundation

enum UsersListComponentFactory {
    static func make(
        maxUsersSelected: Int,
        delegate: UsersListComponentViewModelDelegate? = nil
    ) -> UserListing {
        let presenter: UsersListComponentPresenting = UsersListComponentPresenter()
        let viewModel = UsersListComponentViewModel(presenter: presenter, delegate: delegate, maxUsersSelected: maxUsersSelected)
        let view = UsersListComponentView(viewModel: viewModel)

        presenter.view = view

        return view
    }
}
