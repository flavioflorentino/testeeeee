import Core

enum PaymentRequestServiceEndpoint {
    case paymentRequestPermission
    case paymentRequestPermissionV2
    case checkout(idPayer: Int, totalValue: Double, message: String)
    case checkoutV2(users: [(value: Double, id: Int)], message: String)
    
    private func createUserDicts(user: (value: Double, id: Int)) -> [String: Any] {
        [
            "value": user.value,
            "consumer_payer_id": "\(user.id)"
        ]
    }
}

// MARK: - ApiEndpointExposable
extension PaymentRequestServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .paymentRequestPermission:
            return "api/getConsumerChargeLimit.json"
        case .paymentRequestPermissionV2:
            return "p2p-charge/permissions"
        case .checkout:
            return "api/createP2PChargeTransaction.json"
        case .checkoutV2:
            return "p2p-charge/transactions"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .paymentRequestPermissionV2:
            return .get
        default:
            return .post
        }
    }
    
    var body: Data? {
        switch self {
        case let .checkout(idPayer, totalValue, message):
            return [
                "consumer_id_payer": idPayer,
                "total_value": totalValue,
                "message": message
            ].toData()
            
        case let .checkoutV2(users, message):
            return [
                "main_message": message,
                "consumers_payers": users.map(createUserDicts)
            ].toData()
        default:
            return nil
        }
    }
}
