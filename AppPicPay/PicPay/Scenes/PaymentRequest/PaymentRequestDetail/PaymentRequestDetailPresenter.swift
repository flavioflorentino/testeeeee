import UI

protocol PaymentRequestDetailPresenting {
    func perform(action: PaymentRequestDetailAction)
    func presentInitialData(users: [PaymentRequestSearchViewModel.SelectedUser])
    func presentPrivacyAlert()
    func presentPlaceholderMessage(isVisible: Bool)
    func presentPaymentRequestSuccess()
    func presentPaymentRequestError()
    func presentPaymentRequestAlertError(title: String, message: String, buttonTitle: String)
    func presentShouldFillValueAlert()
    func startLoading()
}

final class PaymentRequestDetailPresenter {
    // MARK: - Variables
    weak var viewController: PaymentRequestDetailDisplay?
    private let coordinator: PaymentRequestDetailCoordinating
    
    // MARK: - Life Cycle
    init(coordinator: PaymentRequestDetailCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - PaymentRequestDetailPresenting
extension PaymentRequestDetailPresenter: PaymentRequestDetailPresenting {
    func perform(action: PaymentRequestDetailAction) {
        coordinator.perform(action: action)
    }
    
    func presentInitialData(users: [PaymentRequestSearchViewModel.SelectedUser]) {
        let presentableUsers = users.map(transformToUserCounterModel)
        let title: String
        if users.count == 1, let firstName = users.first?.name {
            title = firstName
        } else {
            title = PaymentRequestLocalizable.valuePerPerson.text
        }
        viewController?.display(users: presentableUsers)
        viewController?.display(title: title)
    }
    
    private func transformToUserCounterModel(user: PaymentRequestSearchViewModel.SelectedUser) -> UserCounterModel {
        UserCounterModel(imgUrl: user.url)
    }
    
    func presentPrivacyAlert() {
        viewController?.displayPrivacyAlert()
    }
    
    func presentPlaceholderMessage(isVisible: Bool) {
        let text = isVisible ? PaymentRequestLocalizable.messagePlaceholder.text : ""
        viewController?.displayTextMessage(text)
    }
    
    func presentPaymentRequestSuccess() {
        let alertData = StatusAlertData(
            icon: Assets.P2P.paymentRequestSuccess.image,
            title: PaymentRequestLocalizable.paymentRequestAlertTitleV2.text,
            text: PaymentRequestLocalizable.paymentRequestAlertTextV2.text,
            buttonTitle: PaymentRequestLocalizable.paymentRequestAlertButtonV2.text
        )
        viewController?.displaySuccess(alertData: alertData)
    }
    
    func presentPaymentRequestError() {
        let model = StatefulErrorViewModel(
            image: Assets.P2P.dialogSadFace.image,
            content: (
                title: PaymentRequestLocalizable.paymentRequestAlertTitleError.text,
                description: PaymentRequestLocalizable.paymentRequestAlertTextError.text
            ),
            button: (image: nil, title: PaymentRequestLocalizable.paymentRequestAlertButtonTryAgain.text)
        )
        viewController?.displayError(model)
    }
    
    func presentPaymentRequestAlertError(title: String, message: String, buttonTitle: String) {
        viewController?.displayAlert(title: title, message: message, buttonTitle: buttonTitle)
    }
    
    func presentShouldFillValueAlert() {
        let title = PaymentRequestLocalizable.paymentRequestShouldFillValueAlertTitle.text
        let buttonTitle = DefaultLocalizable.btOk.text
        viewController?.displayAlertController(title: title, message: "", buttonTitle: buttonTitle)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
}
