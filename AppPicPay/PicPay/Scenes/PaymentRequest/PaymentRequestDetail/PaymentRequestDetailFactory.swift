import UI

enum PaymentRequestDetailFactory {
    static func make(origin: String, users: [PaymentRequestSearchViewModel.SelectedUser]) -> UIViewController {
        let container = DependencyContainer()
        let coordinator = PaymentRequestDetailCoordinator()
        let presenter = PaymentRequestDetailPresenter(coordinator: coordinator)
        let service = PaymentRequestDetailService(dependencies: container)
        let viewModel = PaymentRequestDetailViewModel(
            users: users,
            dependencies: container,
            presenter: presenter,
            service: service
        )
        let viewController = PaymentRequestDetailViewController(origin: origin, dependencies: container, viewModel: viewModel)
        presenter.viewController = viewController
        coordinator.viewController = viewController
        return viewController
    }
}
