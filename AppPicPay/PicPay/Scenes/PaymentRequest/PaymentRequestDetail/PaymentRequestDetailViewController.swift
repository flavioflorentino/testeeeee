import AnalyticsModule
import UI

protocol PaymentRequestDetailDisplay: AnyObject {
    func display(users: [UserCounterModel])
    func display(title: String)
    func displayPrivacyAlert()
    func displayTextMessage(_ message: String)
    func displayError(_ model: StatefulErrorViewModel)
    func displayAlert(title: String, message: String, buttonTitle: String)
    func displayAlertController(title: String, message: String, buttonTitle: String)
    func displaySuccess(alertData: StatusAlertData)
    func startLoading()
}

extension PaymentRequestDetailViewController.Layout {
    enum Size {
        static let lineHeight: CGFloat = 1
    }
}

final class PaymentRequestDetailViewController: ViewController<PaymentRequestDetailViewModelInputs, UIView> {
    typealias Dependencies = HasAnalytics
    fileprivate enum Layout {}
    
    // MARK: - Visual Components
    private lazy var userStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .center
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var textStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var userCounterView: UIView = {
        UserCounterFactory.make(
            users: users,
            backgroundColor: Colors.backgroundPrimary.color,
            placeholder: PPContact.photoPlaceholder()
        )
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale700())
        return label
    }()
    
    private lazy var valueCurrencyField: CurrencyField = {
        let textField = CurrencyField()
        textField.fontValue = Typography.title(.xLarge).font()
        textField.fontCurrency = Typography.title(.xLarge).font()
        textField.textColor = Colors.grayscale700.color
        textField.placeholderColor = Colors.grayscale700.color
        textField.limitValue = 8
        textField.inputAccessory = paymentRequestToolbar
        return textField
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale050.color
        return view
    }()
    
    private lazy var messageTextView: UITextView = {
        let textView = UITextView()
        textView.font = Typography.bodySecondary().font()
        textView.backgroundColor = Colors.backgroundPrimary.color
        textView.text = PaymentRequestLocalizable.messagePlaceholder.text
        textView.textColor = Colors.grayscale700.color
        textView.delegate = self
        textView.inputAccessoryView = paymentRequestToolbar
        return textView
    }()
    
    private lazy var paymentRequestToolbar: PaymentRequestToolbar = {
        let constructModel = PaymentRequestToolbarModel(
            sendButtonTitle: PaymentRequestLocalizable.sendPaymentRequest.text,
            initialPrivacy: .private,
            initialPaymentMethod: nil,
            isPrivacyEditable: false,
            paymentMethodAvailability: .hidden,
            delegate: self
        )
        let toolbar = PaymentRequestToolbarFactory.make(constructModel: constructModel)
        toolbar.translatesAutoresizingMaskIntoConstraints = false
        return toolbar
    }()
    
    // MARK: - Variables
    private var users: [UserCounterModel] = []
    private let dependencies: Dependencies
    private let origin: String
    
    // MARK: - Life Cycle
    init(origin: String, dependencies: Dependencies, viewModel: PaymentRequestDetailViewModelInputs) {
        self.dependencies = dependencies
        self.origin = origin
        super.init(viewModel: viewModel)
    }
    
    override func viewDidLoad() {
        viewModel.loadInitialData()
        super.viewDidLoad()
        dependencies.analytics.log(PaymentRequestUserEvent.didOpenPaymentDetailScreen(origin: origin))
        valueCurrencyField.becomeFirstResponder()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(userStackView)
        view.addSubview(lineView)
        view.addSubview(messageTextView)
        userStackView.addArrangedSubview(userCounterView)
        userStackView.addArrangedSubview(textStackView)
        textStackView.addArrangedSubview(titleLabel)
        textStackView.addArrangedSubview(valueCurrencyField)
    }
    
    override func configureViews() {
        navigationItem.title = PaymentRequestLocalizable.paymentRequest.text
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    override func setupConstraints() {
        userStackView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
        }
        
        lineView.snp.makeConstraints {
            $0.top.equalTo(userStackView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Size.lineHeight)
        }
        
        messageTextView.snp.makeConstraints {
            $0.top.equalTo(lineView.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview()
        }
    }
}

// MARK: - StatefulTransitionViewing
extension PaymentRequestDetailViewController: StatefulTransitionViewing {
    func didTryAgain() {
        didTouchOnSendButton()
    }
}

// MARK: PaymentRequestDetailDisplay
extension PaymentRequestDetailViewController: PaymentRequestDetailDisplay {
    func display(users: [UserCounterModel]) {
        self.users = users
    }
    
    func display(title: String) {
        titleLabel.text = title
    }
    
    func displayPrivacyAlert() {
        let button = Button(title: PaymentRequestLocalizable.privacyAlertButton.text)
        let alert = Alert(
            with: PaymentRequestLocalizable.privacyAlertTitle.text,
            text: PaymentRequestLocalizable.privacyAlertText.text,
            buttons: [button]
        )
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func displayTextMessage(_ message: String) {
        messageTextView.text = message
    }
    
    func displayError(_ model: StatefulErrorViewModel) {
        endState(model: model)
    }
    
    func displayAlert(title: String, message: String, buttonTitle: String) {
        endState()
        let button = Button(title: buttonTitle)
        let alert = Alert(
            with: title,
            text: message,
            buttons: [button]
        )
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func displayAlertController(title: String, message: String, buttonTitle: String) {
        let controller = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        let action = UIAlertAction(
            title: buttonTitle,
            style: .default
        )
        controller.addAction(action)
        present(controller, animated: true)
    }
    
    func displaySuccess(alertData: StatusAlertData) {
        endState()
        let controller = StatusAlertViewController(data: alertData)
        controller.didTouchOnButtonClosure = { [weak self] in
            self?.viewModel.endflow()
        }
        present(controller, animated: true)
    }
    
    func startLoading() {
        view.endEditing(true)
        beginState()
    }
}

// MARK: - PaymentRequestToolbarDelegate
extension PaymentRequestDetailViewController: PaymentRequestToolbarDelegate {
    func didTouchOnPrivacy() {
        viewModel.touchOnPrivacyButton()
    }
    
    func didTouchOnSendButton() {
        viewModel.sendPaymentRequest(
            message: messageTextView.text ?? "",
            value: valueCurrencyField.value
        )
    }
}

// MARK: - UITextViewDelegate
extension PaymentRequestDetailViewController: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        viewModel.beginEditingTextView()
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        viewModel.endEditingTextView()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let resultText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        viewModel.changeTextView(text: resultText)
        return viewModel.shouldChangeText(resultText)
    }
}
