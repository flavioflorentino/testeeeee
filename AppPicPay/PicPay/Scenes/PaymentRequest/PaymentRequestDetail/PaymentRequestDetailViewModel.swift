import AnalyticsModule

protocol PaymentRequestDetailViewModelInputs {
    func loadInitialData()
    func sendPaymentRequest(message: String, value: Double)
    func beginEditingTextView()
    func shouldChangeText(_ text: String) -> Bool
    func changeTextView(text: String)
    func endEditingTextView()
    func touchOnPrivacyButton()
    func endflow()
}

final class PaymentRequestDetailViewModel {
    typealias Dependencies = HasAnalytics
    
    enum ErrorCode: String {
        case minimumValue = "900"
        case paymentRequestDisabled = "901"
        case limitExceeded = "902"
    }
    // MARK: - Variables
    private let dependencies: Dependencies
    private let presenter: PaymentRequestDetailPresenting
    private let service: PaymentRequestDetailServicing
    private let users: [PaymentRequestSearchViewModel.SelectedUser]
    private var hasOnlyPlaceholderOnTextView: Bool = true
    private let messageMaxLength = 100
    
    // MARK: - Life Cycle
    init(users: [PaymentRequestSearchViewModel.SelectedUser], dependencies: Dependencies, presenter: PaymentRequestDetailPresenting, service: PaymentRequestDetailServicing) {
        self.users = users
        self.dependencies = dependencies
        self.presenter = presenter
        self.service = service
    }
}

// MARK: - PaymentRequestDetailViewModelInputs
extension PaymentRequestDetailViewModel: PaymentRequestDetailViewModelInputs {
    func loadInitialData() {
        presenter.presentInitialData(users: users)
    }
    
    func sendPaymentRequest(message: String, value: Double) {
        guard value > Double.zero else {
            presenter.presentShouldFillValueAlert()
            return
        }
        let message = hasOnlyPlaceholderOnTextView ? "" : message
        let requestUsers = users.compactMap {
            transformUserToRequest(user: $0, value: value)
        }
        presenter.startLoading()
        service.sendPaymentRequest(users: requestUsers, message: message) { [weak self] result in
            switch result {
            case .success:
                self?.sendPaymentRequestSuccess(value: value, isMessageEmpty: message.isEmpty)
            case let .failure(error):
                self?.sendPaymentRequestFailure(error: error)
            }
        }
    }
    
    private func transformUserToRequest(user: PaymentRequestSearchViewModel.SelectedUser, value: Double) -> (Double, Int)? {
        guard let id = Int(user.id) else {
            return nil
        }
        return (value, id)
    }
    
    private func sendPaymentRequestSuccess(value: Double, isMessageEmpty: Bool) {
        dependencies.analytics.log(PaymentRequestUserEvent.didTapSendPicPayCharge(value: value, hasMessage: isMessageEmpty))
        dependencies.analytics.log(PaymentRequestUserEvent.picpayPaymentRequestSuccess)
        presenter.presentPaymentRequestSuccess()
    }
    
    private func sendPaymentRequestFailure(error: DialogError) {
        dependencies.analytics.log(PaymentRequestUserEvent.picpayPaymentRequestError)
        switch error {
        case .default:
            genericError()
        case let .dialog(error):
            handleDialog(error: error)
        }
    }
    
    private func handleDialog(error: DialogResponseError) {
        guard ErrorCode(rawValue: error.code) != nil else {
            genericError()
            return
        }
        presenter.presentPaymentRequestAlertError(
            title: error.model.title,
            message: error.model.message,
            buttonTitle: error.model.buttonText
        )
    }
    
    private func genericError() {
        presenter.presentPaymentRequestError()
    }
    
    func beginEditingTextView() {
        guard hasOnlyPlaceholderOnTextView else {
            return
        }
        presenter.presentPlaceholderMessage(isVisible: false)
    }
    
    func shouldChangeText(_ text: String) -> Bool {
        text.count <= messageMaxLength
    }
    
    func changeTextView(text: String) {
        hasOnlyPlaceholderOnTextView = text.isEmpty
    }
    
    func endEditingTextView() {
        guard hasOnlyPlaceholderOnTextView else {
            return
        }
        presenter.presentPlaceholderMessage(isVisible: true)
    }
    
    func touchOnPrivacyButton() {
        Analytics.shared.log(PaymentRequestUserEvent.didTapOnPrivacy)
        presenter.presentPrivacyAlert()
    }
    
    func endflow() {
        presenter.perform(action: .endFlow)
    }
}
