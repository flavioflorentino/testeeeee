import Core

protocol PaymentRequestDetailServicing {
    func sendPaymentRequest(users: [(Double, Int)], message: String, completion: @escaping (Result<(model: NoContent, data: Data?), DialogError>) -> Void)
}

final class PaymentRequestDetailService {
    typealias Dependencies = HasMainQueue
    // MARK: - Variables
    private let dependencies: Dependencies
    
    // MARK: - Life Cycle
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - PaymentRequestDetailServicing
extension PaymentRequestDetailService: PaymentRequestDetailServicing {
    func sendPaymentRequest(users: [(Double, Int)], message: String, completion: @escaping (Result<(model: NoContent, data: Data?), DialogError>) -> Void) {
        let endpoint = PaymentRequestServiceEndpoint.checkoutV2(users: users, message: message)
        Api<NoContent>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.mapError(DialogError.transformApiError)
                completion(mappedResult)
            }
        }
    }
}
