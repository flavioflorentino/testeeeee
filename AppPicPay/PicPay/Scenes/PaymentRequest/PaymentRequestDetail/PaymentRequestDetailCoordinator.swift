import UI

enum PaymentRequestDetailAction {
    case endFlow
}

protocol PaymentRequestDetailCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: PaymentRequestDetailAction)
}

final class PaymentRequestDetailCoordinator {
    // MARK: - Variables
    weak var viewController: UIViewController?
}

// MARK: - PaymentRequestDetailCoordinating
extension PaymentRequestDetailCoordinator: PaymentRequestDetailCoordinating {
    func perform(action: PaymentRequestDetailAction) {
        if action == .endFlow {
            viewController?.navigationController?.dismiss(animated: true)
        }
    }
}
