struct PaymentRequestSearchResponse: Decodable {
    let rows: [PaymentRequestSearchUser]
}

struct PaymentRequestSearchUser: Decodable {
    let data: PaymentRequestSearchUserData
}

struct PaymentRequestSearchUserData: Decodable {
    let username: String
    let name: String
    let id: String
    let config: PaymentRequestSearchUserConfig
    let imageUrlSmall: URL?
}

struct PaymentRequestSearchUserConfig: Decodable {
    let paymentRequest: Bool
}
