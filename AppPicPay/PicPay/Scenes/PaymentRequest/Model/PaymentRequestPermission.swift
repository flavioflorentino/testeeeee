import Foundation

struct PaymentRequestPermission: Decodable {
    enum CodingKeys: String, CodingKey {
        case isP2PBlockDayChargesEnabled = "p2p_block_day_charges"
    }
    
    let isP2PBlockDayChargesEnabled: Bool
}

struct PaymentRequestPermissionData: Decodable {
    let data: PaymentRequestPermission
}

struct PaymentRequestPermissionV2: Decodable {
    enum CodingKeys: String, CodingKey {
        case maxUsers = "max_users"
    }
    
    let maxUsers: Int
}
