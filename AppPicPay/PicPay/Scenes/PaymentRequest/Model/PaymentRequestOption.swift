import Foundation

enum PaymentRequestOption: Int, CaseIterable {
    case picpay
    case pix
    case qrcode
    case link
    
    var icon: UIImage? {
        switch self {
        case .picpay:
            return Assets.P2P.paymentRequestPicpay.image
        case .pix:
            return Assets.P2P.paymentRequestPix.image
        case .qrcode:
            return Assets.P2P.paymentRequestQrcode.image
        case .link:
            return Assets.P2P.paymentRequestLink.image
        }
    }
    
    var title: String {
        switch self {
        case .picpay:
            return PaymentRequestLocalizable.paymentRequestOptionTitlePicPay.text
        case .pix:
            return PaymentRequestLocalizable.paymentRequestOptionTitlePix.text
        case .qrcode:
            return PaymentRequestLocalizable.paymentRequestOptionTitleQrcode.text
        case .link:
            return PaymentRequestLocalizable.paymentRequestOptionTitleLink.text
        }
    }
    
    var subTitle: String {
        switch self {
        case .picpay:
            return PaymentRequestLocalizable.paymentRequestOptionSubTitlePicPay.text
        case .pix:
            return PaymentRequestLocalizable.paymentRequestOptionSubTitlePix.text
        case .qrcode:
            return PaymentRequestLocalizable.paymentRequestOptionSubTitleQrcode.text
        case .link:
            return PaymentRequestLocalizable.paymentRequestOptionSubTitleLink.text
        }
    }
}
