protocol AppTourCoordinatorDelegate: AnyObject {
    func didFinishTutorial()
}

enum AppTourAction {
    case exit
}

protocol AppTourCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: AppTourCoordinatorDelegate? { get set }
    func perform(action: AppTourAction)
}

final class AppTourCoordinator: AppTourCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: AppTourCoordinatorDelegate?
    
    func perform(action: AppTourAction) {
        if case .exit = action {
            viewController?.dismiss(animated: true) {
                self.delegate?.didFinishTutorial()
            }
        }
    }
}
