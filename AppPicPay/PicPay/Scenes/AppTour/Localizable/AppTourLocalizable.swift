enum AppTourLocalizable: String, Localizable {
    case closeTutorial
    case tapIlustrationDescription
    
    case payButtonStepTitle
    case payButtonStepDescription

    case scanButtonStepTitle
    case scanButtonStepDescription
    
    case walletButtonStepTitle
    case walletButtonStepDescription
    case walletButtonTitle
    
    var key: String {
        rawValue
    }
    
    var file: LocalizableFile {
        .appTour
    }
}
