import AnalyticsModule
import Core

enum AppTourEvent: AnalyticsKeyProtocol {
    case viewed(step: StepDescription)
    case tapped(_ region: TappedRegion, forStep: StepDescription)
   
    private var name: String {
        switch self {
        case .viewed(let step):
            return "\(step.rawValue) - Viewed"
        case .tapped(_, let step):
            return "\(step.rawValue) - Option Selected"
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.firebase, .mixPanel]
    }
    
    private var properties: [String: Any] {
        switch self {
        case .tapped(let region, _):
            return ["option_selected": region.rawValue]
        default:
            return [:]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

extension AppTourEvent {
    enum StepDescription: String {
        case intro = "Tour"
        case payButton = "Tour Pay Button"
        case qrCodeButton = "Tour QR Code"
        case walletButton = "Tour Wallet"
        
        init(forStep step: AppTourStep) {
            switch step {
            case .pay:
                self = .payButton
            case .qrCode:
                self = .qrCodeButton
            case .wallet:
                self = .walletButton
            }
        }
    }
    
    enum TappedRegion: String {
        case closeButton = "CLOSE"
        case overScreen = "TAP"
        case payButton = "PAY"
        case qrCodeButton = "QR CODE"
        case walletButton = "WALLET"
    }
}
