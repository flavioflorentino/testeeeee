import UIKit

protocol AppTourDisplay: AnyObject {
    func displayTutorialHelp()
    func display(step: AppTourStep)
    func removeStep()
    func displayTutorialProgress(_ steps: Int)
    func setProgressViewStep(_ step: Int)
    func hideCloseButtonText()
}
