import AnalyticsModule

protocol AppTourPresenting: AnyObject {
    var viewController: AppTourDisplay? { get set }
    func didNextStep(action: AppTourAction)
    func presentTutorialHelpView()
    func removePreviousStep()
    func presentTutorialProgressView(steps: Int)
    func hideCloseButtonText()
    func presentStep(_ step: AppTourStep, andProgress: Int)
}

final class AppTourPresenter: AppTourPresenting {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    private let coordinator: AppTourCoordinating
    weak var viewController: AppTourDisplay?

    init(coordinator: AppTourCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    func presentTutorialHelpView() {
        viewController?.displayTutorialHelp()
    }
    
    func removePreviousStep() {
        viewController?.removeStep()
    }
    
    func presentTutorialProgressView(steps: Int) {
         viewController?.displayTutorialProgress(steps)
    }
    
    func presentStep(_ step: AppTourStep, andProgress progress: Int) {
        viewController?.display(step: step)
        viewController?.setProgressViewStep(progress)
        dependencies.analytics.log(AppTourEvent.viewed(step: .init(forStep: step)))
    }
    
    func hideCloseButtonText() {
        viewController?.hideCloseButtonText()
    }
    
    func didNextStep(action: AppTourAction) {
        coordinator.perform(action: action)
    }
}
