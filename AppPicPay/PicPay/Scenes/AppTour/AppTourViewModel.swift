import AnalyticsModule

protocol AppTourViewModelInputs: AnyObject {
    func presentTutorialHelp()
    func didTapOverScreen()
    func didTapHighlightedButton()
    func didTapClose()
}

enum AppTourStep {
    case pay
    case qrCode
    case wallet
}

final class AppTourViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let presenter: AppTourPresenting
    private let steps: [AppTourStep] = [.pay, .qrCode, .wallet]
    private var nextStepIndex = 0
    
    private var analyticsStepDescription: AppTourEvent.StepDescription {
        let currentStepIndex = nextStepIndex - 1
        if steps.indices.contains(currentStepIndex) {
            let step = steps[currentStepIndex]
            return AppTourEvent.StepDescription(forStep: step)
        } else {
            return .intro
        }
    }
    
    private var totalSteps: Int {
        steps.count
    }
    
    private var nextStepIsFirst: Bool {
        nextStepIndex == 0
    }
    
    init(dependencies: Dependencies, presenter: AppTourPresenting) {
        self.dependencies = dependencies
        self.presenter = presenter
        dependencies.analytics.log(AppTourEvent.viewed(step: .intro))
    }
    
    private func nextStep() {
        guard steps.indices.contains(nextStepIndex) else {
            exitTour()
            return
        }
        
        showTutorialProgressViewIfNeeded()
        hideCloseButtonTextIfNeeded()
        
        let step = steps[nextStepIndex]
        presenter.removePreviousStep()
        presenter.presentStep(step, andProgress: nextStepIndex)
        nextStepIndex += 1
    }
    
    private func showTutorialProgressViewIfNeeded() {
        if nextStepIsFirst {
            presenter.presentTutorialProgressView(steps: totalSteps)
        }
    }
    
    private func hideCloseButtonTextIfNeeded() {
        if nextStepIsFirst {
            presenter.hideCloseButtonText()
        }
    }
    
    private func exitTour() {
        presenter.didNextStep(action: .exit)
    }
    
    private func sendHighlightedButtonTapEvent() {
        let tappedRegion: AppTourEvent.TappedRegion
        let stepDescription = analyticsStepDescription
        switch stepDescription {
        case .payButton:
            tappedRegion = .payButton
        case .qrCodeButton:
            tappedRegion = .qrCodeButton
        case .walletButton:
            tappedRegion = .walletButton
        case .intro:
            return
        }
        
        dependencies.analytics.log(AppTourEvent.tapped(tappedRegion, forStep: stepDescription))
    }
}

extension AppTourViewModel: AppTourViewModelInputs {
    func presentTutorialHelp() {
        presenter.presentTutorialHelpView()
    }
    
    func didTapOverScreen() {
        dependencies.analytics.log(AppTourEvent.tapped(.overScreen, forStep: analyticsStepDescription))
        nextStep()
    }
    
    func didTapHighlightedButton() {
        sendHighlightedButtonTapEvent()
        nextStep()
    }
    
    func didTapClose() {
        dependencies.analytics.log(AppTourEvent.tapped(.closeButton, forStep: analyticsStepDescription))
        exitTour()
    }
}
