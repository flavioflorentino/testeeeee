import UI

private extension TutorialProgressView.Layout {
    enum Style {
        static let viewedColor = Colors.Style.white()
        static let normalColor = Colors.Style.grayscale300()
        static let selectedColor = Colors.Style.branding300()
    }
    
    enum Space {
        static let itemSpacing: CGFloat = 1.6
    }
}

final class TutorialProgressView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private let steps: Int
    private var selectedStep = 0
    
    private lazy var containerStackView: UIStackView = {
        let views = (0..<steps).map { _ in UIView() }
        let stackView = UIStackView(arrangedSubviews: views)
        stackView.distribution = .fillEqually
        stackView.spacing = Layout.Space.itemSpacing
        return stackView
    }()
    
    init(steps: Int) {
        self.steps = steps
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        configureViewsCornerRadius()
    }
    
    func set(step: Int) {
        containerStackView.arrangedSubviews[selectedStep].backgroundColor = Layout.Style.viewedColor
        
        guard containerStackView.arrangedSubviews.indices.contains(step) else {
           return
        }
        
        containerStackView.arrangedSubviews[step].backgroundColor = Layout.Style.selectedColor

        selectedStep = step
    }
    
    func buildViewHierarchy() {
        addSubview(containerStackView)
    }
    
    func setupConstraints() {
        containerStackView.layout {
            $0.top == topAnchor
            $0.bottom == bottomAnchor
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
        }
    }
    
    func configureStyles() {
        containerStackView.arrangedSubviews.forEach {
            $0.backgroundColor = Layout.Style.normalColor
        }
        
        set(step: selectedStep)
    }
    
    private func configureViewsCornerRadius() {
        let cornerRadius = (containerStackView.arrangedSubviews.first?.frame.height ?? 0) / 2
        
        containerStackView.arrangedSubviews.forEach {
            $0.layer.cornerRadius = cornerRadius
        }
    }
}
