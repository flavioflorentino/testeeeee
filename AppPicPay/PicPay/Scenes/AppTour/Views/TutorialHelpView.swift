import UI

private extension TutorialHelpView.Layout {
    enum Size {
        static let screenBounds = UIScreen.main.bounds
        static let tapImageSize = screenBounds.width / 2.5
    }
    
    enum Font {
        static let tapButtonTitle: UIFont = .systemFont(ofSize: 16, weight: .bold)
    }
    
    enum Style {
        static let numberOfLines = 0
        static let textAlignment: NSTextAlignment = .center
    }
}

final class TutorialHelpView: UIView, AppTourStepView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var tapImageView: UIImageView = {
        let imageView = UIImageView(image: Assets.AppTour.iluTapGesture.image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var tapLabel: UILabel = {
        let label = UILabel()
        label.text = AppTourLocalizable.tapIlustrationDescription.text
        label.font = Layout.Font.tapButtonTitle
        label.numberOfLines = Layout.Style.numberOfLines
        label.textAlignment = Layout.Style.textAlignment
        label.textColor = Palette.white.color
        return label
    }()

    private lazy var tapStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        return stackView
    }()
    
    weak var delegate: AppTourStepViewDelegate?
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        tapStackView.addArrangedSubview(tapImageView)
        tapStackView.addArrangedSubview(tapLabel)
        addSubview(tapStackView)
    }
    
    func setupConstraints() {
        tapImageView.layout {
            $0.height == Layout.Size.tapImageSize
            $0.width == Layout.Size.tapImageSize
        }

        tapStackView.layout {
            $0.centerX == centerXAnchor
            $0.centerY == centerYAnchor
        }
    }
}
