import UI
import UIKit

final class LineConnectorView: UIView {
    enum Direction {
        case upAndRight
        case upAndLeft
        case downAndRight
        case downAndLeft
    }
    
    private struct ReferencePoints {
        var start: CGPoint
        var end: CGPoint
        var curveAxis: CGPoint
    }
    
    var padding: CGFloat = Spacing.base00
    var thickness: CGFloat = 1
    var cornerRadius: CGFloat = Spacing.base02
    var lineColor: CGColor = Palette.ppColorNegative200.cgColor
    
    private let direction: Direction
    
    private lazy var referencePoints: ReferencePoints = {
        var refPoints = ReferencePoints(start: .zero, end: .zero, curveAxis: .zero)
        
        switch direction {
        case .downAndLeft:
            refPoints.start = CGPoint(x: frame.width - padding, y: padding)
            refPoints.end = CGPoint(x: padding, y: frame.height - padding)
            refPoints.curveAxis = CGPoint(x: refPoints.start.x - cornerRadius, y: refPoints.end.y - cornerRadius)
        case .downAndRight:
            refPoints.start = CGPoint(x: padding, y: padding)
            refPoints.end = CGPoint(x: frame.width - padding, y: frame.height - padding)
            refPoints.curveAxis = CGPoint(x: refPoints.start.x + cornerRadius, y: refPoints.end.y - cornerRadius)
        case .upAndLeft:
            refPoints.start = CGPoint(x: frame.width - padding, y: frame.height - padding)
            refPoints.end = CGPoint(x: padding, y: padding)
            refPoints.curveAxis = CGPoint(x: refPoints.start.x - cornerRadius, y: refPoints.end.y + cornerRadius)
        case .upAndRight:
            refPoints.start = CGPoint(x: padding, y: frame.height - padding)
            refPoints.end = CGPoint(x: frame.width - padding, y: padding)
            refPoints.curveAxis = CGPoint(x: refPoints.start.x + cornerRadius, y: refPoints.end.y + cornerRadius)
        }
        return refPoints
    }()
    
    private lazy var lineShapeLayer: CAShapeLayer = {
        let startPoint = referencePoints.start
        let endPoint = referencePoints.end
        let curveAxis = referencePoints.curveAxis
        let beforeCurvePoint = CGPoint(x: startPoint.x, y: curveAxis.y)
        let afterCurvePoint = CGPoint(x: curveAxis.x, y: endPoint.y)
        let controlPoint = CGPoint(x: startPoint.x, y: endPoint.y)
        
        let path = UIBezierPath()
        path.move(to: startPoint)
        path.addLine(to: beforeCurvePoint)
        path.addQuadCurve(to: afterCurvePoint, controlPoint: controlPoint)
        path.addLine(to: endPoint)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.fillColor = .none
        shapeLayer.strokeColor = lineColor
        shapeLayer.lineWidth = thickness
        shapeLayer.path = path.cgPath
        return shapeLayer
    }()
    
    private lazy var circleShapeLayer: CAShapeLayer = {
        let circlePath = UIBezierPath(
            arcCenter: referencePoints.start,
            radius: 3,
            startAngle: 0,
            endAngle: 2 * .pi,
            clockwise: true
        )
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.fillColor = lineColor
        shapeLayer.path = circlePath.cgPath
        return shapeLayer
    }()
    
    init(direction: Direction) {
        self.direction = direction
        super.init(frame: .zero)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func display(animate: Bool) {
        layer.addSublayer(circleShapeLayer)
        layer.addSublayer(lineShapeLayer)
        
        if animate {
            animateShapeLayer(lineShapeLayer)
        }
    }
    
    private func animateShapeLayer(_ shapeLayer: CAShapeLayer) {
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = 0
        animation.toValue = 1
        animation.duration = 1
        animation.timingFunction = CAMediaTimingFunction(name: .easeOut)
        shapeLayer.add(animation, forKey: "path")
    }
}
