import UI
import UIKit

private extension AppTourPayStepView.Layout {
    enum Size {
        static let buttonView: CGFloat = 54
    }
    
    enum Space {
        static let connectorAndLabel: CGFloat = Spacing.base01
    }
    
    enum Font {
        static let stepTitle: UIFont = .systemFont(ofSize: 16, weight: .bold)
        static let stepDescription: UIFont = .systemFont(ofSize: 14, weight: .regular)
    }
}

final class AppTourPayStepView: UIView, AppTourStepView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var connectorView = LineConnectorView(direction: .upAndRight)
    
    private lazy var payButton: UIControl = {
        let buttonFrame = CGRect(x: .zero, y: .zero, width: Layout.Size.buttonView, height: Layout.Size.buttonView)
        let button = MainTabPayButton(frame: buttonFrame)
        button.addTarget(self, action: #selector(tapButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.stepTitle
        label.textColor = Palette.ppColorBranding300.color
        label.text = AppTourLocalizable.payButtonStepTitle.text
        label.numberOfLines = 0
        label.alpha = 0
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.stepDescription
        label.textColor = Palette.white.color
        label.text = AppTourLocalizable.payButtonStepDescription.text
        label.numberOfLines = 0
        label.alpha = 0
        return label
    }()
    
    weak var delegate: AppTourStepViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        display()
    }
    
    func buildViewHierarchy() {
        addSubview(payButton)
        addSubview(connectorView)
        addSubview(descriptionLabel)
        addSubview(titleLabel)
    }
    
    func setupConstraints() {
        payButton.layout {
            $0.bottom == compatibleSafeAreaLayoutGuide.bottomAnchor - Spacing.base00
            $0.centerX == centerXAnchor
            $0.width == Layout.Size.buttonView
            $0.height == Layout.Size.buttonView
        }
        
        descriptionLabel.layout {
            $0.leading == centerXAnchor + Layout.Space.connectorAndLabel
            $0.trailing == trailingAnchor
            $0.bottom == payButton.topAnchor - Spacing.base06
        }
        
        connectorView.layout {
            $0.leading == descriptionLabel.leadingAnchor - (Layout.Space.connectorAndLabel + Spacing.base00)
            $0.trailing == trailingAnchor - Spacing.base01
            $0.top == descriptionLabel.topAnchor - Layout.Space.connectorAndLabel
            $0.bottom == payButton.topAnchor
        }
        
        titleLabel.layout {
            $0.leading == descriptionLabel.leadingAnchor
            $0.trailing == trailingAnchor
            $0.bottom == connectorView.topAnchor - Spacing.base00
        }
    }
    
    @objc
    private func tapButton() {
        delegate?.didTapHighlightedButton()
    }
    
    private func display() {
        connectorView.display(animate: true)
        
        UIView.animate(withDuration: 1, delay: 0.3, options: .curveEaseOut, animations: {
            self.titleLabel.alpha = 1
        })
        UIView.animate(withDuration: 1, delay: 0.8, options: .curveEaseOut, animations: {
            self.descriptionLabel.alpha = 1
        })
    }
}
