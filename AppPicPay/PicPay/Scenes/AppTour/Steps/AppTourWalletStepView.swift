import UI
import UIKit

private extension AppTourWalletStepView.Layout {
    enum Size {
        static let buttonView: CGFloat = 28
        static let tabBarButtonWidth: CGFloat = UIScreen.main.bounds.width / 5
        static let textLabelWidth: CGFloat = 200
    }
    
    enum Space {
        static let connectorAndLabel: CGFloat = Spacing.base01
        static let walletButtonCenterToLeading: CGFloat = Size.tabBarButtonWidth * 1.5
        static let walletButtonCenterOffset: CGFloat = 6
    }
    
    enum Font {
        static let stepTitle: UIFont = .systemFont(ofSize: 16, weight: .bold)
        static let stepDescription: UIFont = .systemFont(ofSize: 14, weight: .regular)
        static let tabBarButtonTitle: UIFont = .systemFont(ofSize: 11, weight: .light)
    }
}

final class AppTourWalletStepView: UIView, AppTourStepView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var connectorView = LineConnectorView(direction: .upAndRight)
    
    private lazy var walletButton: UIButton = {
        let button = UIButton()
        let image = Assets.Tab.icoTabWallet.image.withRenderingMode(.alwaysTemplate)
        button.setImage(image, for: .normal)
        button.tintColor = Palette.ppColorBranding300.color
        button.addTarget(self, action: #selector(tapButton), for: .touchUpInside)
        return button
    }()
    
    private let walletLabel: UILabel = {
        let label = UILabel()
        label.text = AppTourLocalizable.walletButtonTitle.text
        label.textAlignment = .center
        label.textColor = Palette.ppColorBranding300.color
        label.font = Layout.Font.tabBarButtonTitle
        return label
    }()
    
    private lazy var walletStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.stepTitle
        label.textColor = Palette.ppColorBranding300.color
        label.text = AppTourLocalizable.walletButtonStepTitle.text
        label.numberOfLines = 0
        label.alpha = 0
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.stepDescription
        label.textColor = Palette.white.color
        label.text = AppTourLocalizable.walletButtonStepDescription.text
        label.numberOfLines = 0
        label.alpha = 0
        return label
    }()
    
    weak var delegate: AppTourStepViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        display()
    }
    
    func buildViewHierarchy() {
        walletStackView.addArrangedSubviews(walletButton, walletLabel)
        addSubview(walletStackView)
        addSubview(connectorView)
        addSubview(descriptionLabel)
        addSubview(titleLabel)
    }
    
    func setupConstraints() {
        walletButton.layout {
            $0.height == Layout.Size.buttonView
        }
        
        walletStackView.layout {
            $0.bottom == compatibleSafeAreaLayoutGuide.bottomAnchor
            $0.centerX == leadingAnchor + Layout.Space.walletButtonCenterToLeading
        }
        
        descriptionLabel.layout {
            $0.leading == walletStackView.centerXAnchor + Layout.Space.connectorAndLabel
            $0.width == Layout.Size.textLabelWidth
            $0.bottom == walletStackView.topAnchor - Spacing.base06
        }
        
        connectorView.layout {
            $0.leading == descriptionLabel.leadingAnchor - (Layout.Space.connectorAndLabel + Layout.Space.walletButtonCenterOffset)
            $0.trailing == descriptionLabel.trailingAnchor
            $0.top == descriptionLabel.topAnchor - Layout.Space.connectorAndLabel
            $0.bottom == walletButton.topAnchor
        }
        
        titleLabel.layout {
            $0.leading == descriptionLabel.leadingAnchor
            $0.trailing == descriptionLabel.trailingAnchor
            $0.bottom == connectorView.topAnchor - Spacing.base00
        }
    }
    
    @objc
    private func tapButton() {
        delegate?.didTapHighlightedButton()
    }
    
    private func display() {
        connectorView.display(animate: true)
        
        UIView.animate(withDuration: 1, delay: 0.3, options: .curveEaseOut, animations: {
            self.titleLabel.alpha = 1
        })
        UIView.animate(withDuration: 1, delay: 0.8, options: .curveEaseOut, animations: {
            self.descriptionLabel.alpha = 1
        })
    }
}
