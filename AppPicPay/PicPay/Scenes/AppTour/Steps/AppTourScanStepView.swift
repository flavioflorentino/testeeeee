import UI
import UIKit

private extension AppTourScanStepView.Layout {
    enum Size {
        static let buttonView: CGFloat = 28
        static let textLabelWidth: CGFloat = 200
    }
    
    enum Space {
        static let connectorAndLabel: CGFloat = Spacing.base01
        static let statusBarHeight: CGFloat = UIApplication.shared.statusBarFrame.height
    }
    
    enum Font {
        static let stepTitle: UIFont = .systemFont(ofSize: 16, weight: .bold)
        static let stepDescription: UIFont = .systemFont(ofSize: 14, weight: .regular)
    }
}

final class AppTourScanStepView: UIView, AppTourStepView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var connectorView = LineConnectorView(direction: .downAndRight)
    
    private lazy var scanButton: UIButton = {
        let button = UIButton()
        let image = Assets.Icons.iconeScannerUpdated.image.withRenderingMode(.alwaysTemplate)
        button.setImage(image, for: .normal)
        button.tintColor = Palette.ppColorBranding300.color
        button.addTarget(self, action: #selector(tapButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.stepTitle
        label.textColor = Palette.ppColorBranding300.color
        label.text = AppTourLocalizable.scanButtonStepTitle.text
        label.numberOfLines = 0
        label.alpha = 0
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.stepDescription
        label.textColor = Palette.white.color
        label.text = AppTourLocalizable.scanButtonStepDescription.text
        label.numberOfLines = 0
        label.alpha = 0
        return label
    }()
    
    weak var delegate: AppTourStepViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        display()
    }
    
    func buildViewHierarchy() {
        addSubview(scanButton)
        addSubview(connectorView)
        addSubview(descriptionLabel)
        addSubview(titleLabel)
    }
    
    func setupConstraints() {
        scanButton.layout {
            if #available(iOS 11, *) {
                $0.top == compatibleSafeAreaLayoutGuide.topAnchor + Spacing.base01
            } else {
                $0.top == topAnchor + (Layout.Space.statusBarHeight + Spacing.base01)
            }
            $0.leading == leadingAnchor + Spacing.base02
            $0.width == Layout.Size.buttonView
            $0.height == Layout.Size.buttonView
        }
        
        descriptionLabel.layout {
            $0.leading == scanButton.leadingAnchor + (Layout.Size.buttonView / 2 + Layout.Space.connectorAndLabel)
            $0.width == Layout.Size.textLabelWidth
            $0.top == scanButton.bottomAnchor + Spacing.base06
        }
        
        connectorView.layout {
            $0.top == scanButton.bottomAnchor
            $0.leading == descriptionLabel.leadingAnchor - (Layout.Space.connectorAndLabel + Spacing.base00)
            $0.width == descriptionLabel.widthAnchor
            $0.bottom == descriptionLabel.bottomAnchor + (Layout.Space.connectorAndLabel + Spacing.base01)
        }
        
        titleLabel.layout {
            $0.leading == descriptionLabel.leadingAnchor
            $0.trailing == descriptionLabel.trailingAnchor
            $0.top == connectorView.bottomAnchor + Spacing.base00
        }
    }
    
    @objc
    private func tapButton() {
        delegate?.didTapHighlightedButton()
    }
    
    private func display() {
        connectorView.display(animate: true)
        
        UIView.animate(withDuration: 1, delay: 0.3, options: .curveEaseOut, animations: {
            self.titleLabel.alpha = 1
        })
        UIView.animate(withDuration: 1, delay: 0.8, options: .curveEaseOut, animations: {
            self.descriptionLabel.alpha = 1
        })
    }
}
