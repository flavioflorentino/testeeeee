protocol AppTourStepViewDelegate: AnyObject {
    func didTapHighlightedButton()
}

protocol AppTourStepView: UIView {
    var delegate: AppTourStepViewDelegate? { get set }
}
