enum AppTourFactory {
    static func make(delegate: AppTourCoordinatorDelegate) -> AppTourViewController {
        let container = DependencyContainer()
        let coordinator: AppTourCoordinating = AppTourCoordinator()
        let presenter: AppTourPresenting = AppTourPresenter(coordinator: coordinator, dependencies: container)
        let viewModel = AppTourViewModel(dependencies: container, presenter: presenter)
        let viewController = AppTourViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.delegate = delegate
        presenter.viewController = viewController

        return viewController
    }
}
