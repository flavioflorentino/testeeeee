import UI
import UIKit

private extension AppTourViewController.Layout {
   enum Font {
        static let closeButtonTitle: UIFont = .systemFont(ofSize: 12, weight: .bold)
    }
    
    enum Spacing {
        static let topLimit: CGFloat = 44
    }
}

final class AppTourViewController: ViewController<AppTourViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    private lazy var visualEffectView = UIView()
    private lazy var containerView = UIView()
    private var progressView: TutorialProgressView?
    
    private lazy var closeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(Assets.Savings.iconRoundCloseGreen.image, for: .normal)
        button.setTitle(AppTourLocalizable.closeTutorial.text, for: .normal)
        button.tintColor = Palette.ppColorBranding300.color
        button.titleLabel?.font = Layout.Font.closeButtonTitle
        button.addTarget(self, action: #selector(tapCloseButton), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.presentTutorialHelp()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(visualEffectView)
        view.addSubview(containerView)
        view.addSubview(closeButton)
    }
    
    override func configureViews() {
        view.backgroundColor = .clear
        visualEffectView.backgroundColor = Colors.black.lightColor
        visualEffectView.layer.opacity = 0.8
        setupTapGestureRecognizer()
    }
    
    override func setupConstraints() {
        visualEffectView.layout {
            $0.top == view.topAnchor
            $0.bottom == view.bottomAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
        }
        
        containerView.layout {
            $0.top == view.topAnchor
            $0.bottom == view.bottomAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
        }
        
        closeButton.layout {
            $0.bottom == view.compatibleSafeAreaLayoutGuide.bottomAnchor - Spacing.base02
            $0.leading == view.leadingAnchor + Spacing.base02
        }
    }
    
    private func setupTapGestureRecognizer() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOverScreen))
        containerView.addGestureRecognizer(tap)
    }
    
    private func configureStepView(_ stepView: AppTourStepView) {
        containerView.addSubview(stepView)
        stepView.layout {
            $0.top == view.topAnchor
            $0.bottom == view.bottomAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
        }
        stepView.delegate = self
    }
    
    private func configureProgressView(_ progressView: TutorialProgressView) {
        view.addSubview(progressView)
        
        progressView.layout {
            if #available(iOS 11, *) {
                let topSpacing = view.safeAreaInsets.top < Layout.Spacing.topLimit ? Spacing.base00 : .zero
                $0.top == view.safeAreaLayoutGuide.topAnchor + topSpacing
            } else {
                $0.top == view.topAnchor + Spacing.base03
            }
            
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
            $0.height == Spacing.base00
        }
        
        self.progressView = progressView
    }
    
    @objc
    private func tapOverScreen() {
        viewModel.didTapOverScreen()
    }
    
    @objc
    private func tapCloseButton() {
        viewModel.didTapClose()
    }
}

extension AppTourViewController: AppTourStepViewDelegate {
    func didTapHighlightedButton() {
        viewModel.didTapHighlightedButton()
    }
}

// MARK: View Model Outputs
extension AppTourViewController: AppTourDisplay {
    func displayTutorialHelp() {
        let view = TutorialHelpView()
        configureStepView(view)
    }
    
    func display(step: AppTourStep) {
        let currentView: AppTourStepView
        
        switch step {
        case .pay:
            currentView = AppTourPayStepView()
        case .qrCode:
            currentView = AppTourScanStepView()
        case .wallet:
            currentView = AppTourWalletStepView()
        }
        
        configureStepView(currentView)
    }
    
    func removeStep() {
        containerView.removeAllSubviews()
    }
    
    func displayTutorialProgress(_ steps: Int) {
        let view = TutorialProgressView(steps: steps)
        configureProgressView(view)
    }
    
    func setProgressViewStep(_ step: Int) {
        progressView?.set(step: step)
    }
    
    func hideCloseButtonText() {
        closeButton.setTitle(nil, for: .normal)
    }
}
