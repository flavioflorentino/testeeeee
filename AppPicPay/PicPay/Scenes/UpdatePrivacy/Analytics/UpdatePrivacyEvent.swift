import Foundation
import AnalyticsModule

enum DialogOrigin: String {
    case reviewPrivacy = "SIGILO_BANCARIO_PRIVACIDADE"
    case privacySetting = "PRIVACIDADE"
}

enum UpdatePrivacyEvent: AnalyticsKeyProtocol {
    typealias Localizable = Strings.UpdatePrivacy
    
    case saveReviewed(_ consumerId: String?,
                      _ origin: String,
                      _ isPublicProfile: Bool,
                      _ isPublicPayments: Bool,
                      _ isPublicReceives: Bool,
                      _ isHiddenPastPayments: Bool,
                      _ isHiddenPastReceives: Bool)
    case receiveDialogViewed(_ consumerId: String?, _ origin: DialogOrigin)
    case paymentDialogViewed(_ consumerId: String?, _ origin: DialogOrigin)
    case accountDialogViewed(_ consumerId: String?, _ origin: DialogOrigin)
    case screenErrorViewed(_ consumerId: String?, _ origin: String)
    
    var name: String {
        switch self {
        case .saveReviewed:
            return Constants.userSettingsChangedEventName.rawValue
        case .screenErrorViewed:
            return Constants.screenEventName.rawValue
        case .receiveDialogViewed, .paymentDialogViewed, .accountDialogViewed:
            return Constants.dialogEventName.rawValue
        }
    }
    
    var properties: [String: Any] {
        switch self {
        case let .saveReviewed(consumerId, origin,
                               isPublicProfile, isPublicPayments, isPublicReceives,
                               isHiddenPastPayments, isHiddenPastReceives):
            return [
                Keys.userId.rawValue: consumerId ?? "",
                Keys.screenName.rawValue: Constants.privacySuccessScreenName.rawValue,
                Keys.businessContext.rawValue: Constants.privacy.rawValue,
                Keys.origin.rawValue: origin.uppercased(),
                Keys.isPublicProfile.rawValue: isPublicProfile,
                Keys.isPublicPayments.rawValue: isPublicPayments,
                Keys.isPublicReceives.rawValue: isPublicReceives,
                Keys.isHiddenPastPayments.rawValue: isHiddenPastPayments,
                Keys.isHiddenPastReceives.rawValue: isHiddenPastReceives
            ]
        case let .screenErrorViewed(consumerId, origin):
            return [
                Keys.userId.rawValue: consumerId ?? "",
                Keys.screenName.rawValue: Constants.privacyErrorScreenName.rawValue,
                Keys.businessContext.rawValue: Constants.privacy.rawValue,
                Keys.origin.rawValue: origin.uppercased()
            ]
        case let .paymentDialogViewed(consumerId, origin):
            return [
                Keys.userId.rawValue: consumerId ?? "",
                Keys.screenName.rawValue: origin.rawValue,
                Keys.businessContext.rawValue: Constants.privacy.rawValue,
                Keys.dialogName.rawValue: Constants.paymentsDialogName
            ]
        case let .receiveDialogViewed(consumerId, origin):
            return [
                Keys.userId.rawValue: consumerId ?? "",
                Keys.screenName.rawValue: origin.rawValue,
                Keys.businessContext.rawValue: Constants.privacy.rawValue,
                Keys.dialogName.rawValue: Constants.receivesDialogName
            ]
        case let .accountDialogViewed(consumerId, origin):
            return [
                Keys.userId.rawValue: consumerId ?? "",
                Keys.screenName.rawValue: origin.rawValue,
                Keys.businessContext.rawValue: Constants.privacy.rawValue,
                Keys.dialogName.rawValue: Constants.accountDialogName
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.eventTracker])
    }
}

private extension UpdatePrivacyEvent {
    enum Keys: String {
        case screenName = "screen_name"
        case userId = "user_id"
        case businessContext = "business_context"
        case dialogName = "dialog_name"
        case isPublicProfile = "is_public_profile"
        case isPublicPayments = "is_public_payments"
        case isPublicReceives = "is_public_receives"
        case isHiddenPastPayments = "is_hidden_past_payments"
        case isHiddenPastReceives = "is_hidden_past_receives"
        case origin
    }
    enum Constants: String {
        case userSettingsChangedEventName = "User Settings Changed"
        case screenEventName = "Screen Viewed"
        case dialogEventName = "Dialog Viewed"
        case privacyErrorScreenName = "SIGILO_BANCARIO_PRIVACIDADE_ERRO"
        case privacySuccessScreenName = "SIGILO_BANCARIO_PRIVACIDADE_SUCESSO"
        case receivesDialogName = "OCULTAR_ATIVIDADES_RECEBIMENTOS"
        case paymentsDialogName = "OCULTAR_ATIVIDADES_PAGAMENTOS"
        case accountDialogName = "TORNAR_PERFIL_FECHADO"
        case privacy = "PRIVACIDADE"
    }
}
