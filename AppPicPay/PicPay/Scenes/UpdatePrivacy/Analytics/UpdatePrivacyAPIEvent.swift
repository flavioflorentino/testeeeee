import Foundation
import AnalyticsModule

enum UpdatePrivacyAPIEvent: AnalyticsKeyProtocol {
    case calledApiFromReceiptSuccess(_ consumerId: String?, isReviewed: Bool)
    case calledApiFromReceiptError(_ consumerId: String?)
    case calledApiFromHomeSuccess(_ consumerId: String?, isReviewed: Bool, hasTransactions: Bool)
    case calledApiFromHomeError(_ consumerId: String?)
    
    var name: String {
        Constants.eventName.rawValue
    }
    
    var properties: [String: Any] {
        switch self {
        case let .calledApiFromReceiptSuccess(consumerId, isReviewed):
            return [
                Keys.apiReturnedAt.rawValue: Date(),
                Keys.endpoint.rawValue: Constants.endpointConsumer.rawValue,
                Keys.apiReturnParameters.rawValue: "{reviewed = \(isReviewed)}",
                Keys.apiName.rawValue: Constants.apiReceiptOrigin.rawValue,
                Keys.callOrigem.rawValue: Constants.receipt.rawValue,
                Keys.userId.rawValue: consumerId ?? "",
                Keys.status.rawValue: Constants.success.rawValue
            ]
        case let .calledApiFromReceiptError(consumerId):
            return [
                Keys.apiReturnedAt.rawValue: Date(),
                Keys.endpoint.rawValue: Constants.endpointConsumer.rawValue,
                Keys.apiName.rawValue: Constants.apiReceiptOrigin.rawValue,
                Keys.callOrigem.rawValue: Constants.receipt.rawValue,
                Keys.userId.rawValue: consumerId ?? "",
                Keys.status.rawValue: Constants.error.rawValue
            ]
        case let .calledApiFromHomeSuccess(consumerId, isReviewed, hasTransactions):
            return [
                Keys.apiReturnedAt.rawValue: Date(),
                Keys.endpoint.rawValue: Constants.endpointConsumer.rawValue,
                Keys.apiReturnParameters.rawValue: "{reviewed = \(isReviewed); hasTransactions = \(hasTransactions)}",
                Keys.apiName.rawValue: Constants.apiHomeOrigin.rawValue,
                Keys.callOrigem.rawValue: Constants.home.rawValue,
                Keys.userId.rawValue: consumerId ?? "",
                Keys.status.rawValue: Constants.success.rawValue
            ]
        case let .calledApiFromHomeError(consumerId):
            return [
                Keys.apiReturnedAt.rawValue: Date(),
                Keys.endpoint.rawValue: Constants.endpointConsumer.rawValue,
                Keys.apiName.rawValue: Constants.apiHomeOrigin.rawValue,
                Keys.callOrigem.rawValue: Constants.home.rawValue,
                Keys.userId.rawValue: consumerId ?? "",
                Keys.status.rawValue: Constants.error.rawValue
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.eventTracker])
    }
}

private extension UpdatePrivacyAPIEvent {
    enum Keys: String {
        case apiReturnedAt = "api_returned_at"
        case endpoint
        case apiReturnParameters = "api_return_parameters"
        case apiName = "api_name"
        case callOrigem = "call_origin"
        case userId = "user_id"
        case status
    }
    enum Constants: String {
        case eventName = "API Returned"
        case endpointConsumer = "consumer:prod"
        case apiReceiptOrigin = "/configs/review"
        case apiHomeOrigin = "/bank-secrecy/revision-status"
        case receipt = "RECIBO"
        case home = "HOME"
        case error = "ERRO"
        case success = "SUCESSO"
    }
}
