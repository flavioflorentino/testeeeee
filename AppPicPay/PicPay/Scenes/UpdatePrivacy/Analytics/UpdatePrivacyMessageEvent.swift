import Foundation
import AnalyticsModule

enum UpdatePrivacyMessageEvent: AnalyticsKeyProtocol {
    typealias Localizable = Strings.UpdatePrivacy
    
    case didTapSaveDefaultConfigs(_ consumerId: String?, _ firstTextOption: Bool)
    case didTapReviewPrivacySettings(_ consumerId: String?, _ firstTextOption: Bool)
    case screenIntroViewed(_ consumerId: String?, _ origin: ReviewPrivacyOrigin)
    case screenErrorViewed(_ consumerId: String?)
    case screenSuccessViewed(_ consumerId: String?)
    
    var name: String {
        switch self {
        case .didTapSaveDefaultConfigs, .didTapReviewPrivacySettings:
            return Constants.buttonClicked.rawValue
        case .screenIntroViewed, .screenSuccessViewed, .screenErrorViewed:
            return Constants.screenEventName.rawValue
        }
    }
    
    var properties: [String: Any] {
        switch self {
        case let .didTapSaveDefaultConfigs(consumerId, firstTextOption):
            return [
                Keys.consumerId.rawValue: consumerId ?? "",
                Keys.screenName.rawValue: Constants.privacyIntroScreenName.rawValue,
                Keys.buttonName.rawValue: Constants.useDefaultButtonName.rawValue,
                Keys.businessContext.rawValue: Constants.bussinessContextOption.rawValue,
                Keys.firstTextOption.rawValue: firstTextOption
            ]
        case let .didTapReviewPrivacySettings(consumerId, firstTextOption):
            return [
                Keys.consumerId.rawValue: consumerId ?? "",
                Keys.screenName.rawValue: Constants.privacyIntroScreenName.rawValue,
                Keys.buttonName.rawValue: Constants.reviewSettingsButtonName.rawValue,
                Keys.businessContext.rawValue: Constants.bussinessContextOption.rawValue,
                Keys.firstTextOption.rawValue: firstTextOption
            ]
        case let .screenIntroViewed(consumerId, origin):
            return [
                Keys.consumerId.rawValue: consumerId ?? "",
                Keys.screenName.rawValue: Constants.privacyIntroScreenName.rawValue,
                Keys.businessContext.rawValue: getBusinessContext(origin)
            ]
        case let .screenSuccessViewed(consumerId):
            return [
                Keys.consumerId.rawValue: consumerId ?? "",
                Keys.screenName.rawValue: Constants.privacySuccessScreenName.rawValue,
                Keys.businessContext.rawValue: Constants.bussinessContextOption.rawValue
            ]
        case let .screenErrorViewed(consumerId):
            return [
                Keys.consumerId.rawValue: consumerId ?? "",
                Keys.screenName.rawValue: Constants.privacyErrorScreenName.rawValue,
                Keys.businessContext.rawValue: Constants.bussinessContextOption.rawValue
            ]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.eventTracker])
    }
    
    private func getBusinessContext(_ origin: ReviewPrivacyOrigin) -> String {
        origin.rawValue == ReviewPrivacyOrigin.home.rawValue
            ? Constants.updatePrivacyMessageEventOriginHome.rawValue
            : Constants.updatePrivacyMessageEventOriginTransaction.rawValue
    }
}

private extension UpdatePrivacyMessageEvent {
    enum Keys: String {
        case consumerId = "consumer_id"
        case firstTextOption
        case businessContext = "business_context"
        case screenName = "screen_name"
        case buttonName = "button_name"
    }
    enum Constants: String {
        case screenEventName = "Screen Viewed"
        case buttonClicked = "Button Clicked"
        case privacyIntroScreenName = "SIGILO_BANCARIO_PRIVACIDADE_INTRO"
        case privacyErrorScreenName = "SIGILO_BANCARIO_PRIVACIDADE_ERRO"
        case privacySuccessScreenName = "SIGILO_BANCARIO_PRIVACIDADE_SUCESSO"
        case updatePrivacyMessageEventOriginHome = "INICIO"
        case updatePrivacyMessageEventOriginTransaction = "TRANSACAO"
        case useDefaultButtonName = "CONCORDO_USAR_PADRAO"
        case reviewSettingsButtonName = "REVISAR_CONFIGURACOES"
        case bussinessContextOption = "PRIVACIDADE"
    }
}
