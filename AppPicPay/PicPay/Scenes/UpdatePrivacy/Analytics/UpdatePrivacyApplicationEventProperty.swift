import FeatureFlag

struct UpdatePrivacyApplicationEventProperty {
    typealias Dependency = HasFeatureManager
    
    // MARK: - Properties
    private let dependency: Dependency
    
    init(dependency: Dependency) {
        self.dependency = dependency
    }
}

// MARK: - ApplicationEventProperty
extension UpdatePrivacyApplicationEventProperty: ApplicationEventProperty {
    var properties: [String: Any] {
        [
            "is_review_privacy_available": dependency.featureManager.isActive(.isReviewPrivacyAvailable),
            "is_review_privacy_home_available": dependency.featureManager.isActive(.isReviewPrivacyHomeAvailable)
        ]
    }
}
