struct RevisionStatusModel: Codable {
    let hasTransactions: Bool
    let reviewed: Bool
}
