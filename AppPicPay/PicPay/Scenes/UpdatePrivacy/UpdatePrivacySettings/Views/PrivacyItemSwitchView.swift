import UI
import UIKit

protocol PrivacyItemSwitchDelegate: AnyObject {
    func didSwitchChange(type: PrivacyItemType, isOn: Bool)
}

final class PrivacyItemSwitchView: UIView {
    let privacyItemType: PrivacyItemType
    
    var itemIsOn = true
    
    weak var delegate: PrivacyItemSwitchDelegate?
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium))
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
        return label
    }()
    
    private lazy var switchButton: UISwitch = {
        let switchButton = UISwitch()
        switchButton.translatesAutoresizingMaskIntoConstraints = false
        switchButton.isOn = itemIsOn
        switchButton.addTarget(self, action: #selector(switchDidChange), for: .valueChanged)
        return switchButton
    }()
    
    init(type: PrivacyItemType) {
        self.privacyItemType = type
        super.init(frame: .zero)
        buildLayout()
    }

    func setValues(title: String, description: NSAttributedString) {
        titleLabel.text = title
        descriptionLabel.attributedText = description
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func changeSwitchValue(with newValue: Bool) {
        switchButton.isOn = newValue
    }
}

private extension PrivacyItemSwitchView {
    @objc
    func switchDidChange() {
        itemIsOn = switchButton.isOn
        delegate?.didSwitchChange(type: privacyItemType, isOn: itemIsOn)
    }
}

extension PrivacyItemSwitchView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(titleLabel, descriptionLabel, switchButton)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.leading.equalToSuperview()
            $0.trailing.equalTo(switchButton.snp.leading).offset(-Spacing.base03)
        }
        switchButton.snp.makeConstraints {
            $0.top.equalToSuperview().inset(Spacing.base02)
            $0.trailing.equalToSuperview()
        }
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.trailing.equalTo(switchButton.snp.leading).offset(-Spacing.base03)
            $0.leading.bottom.equalToSuperview()
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}
