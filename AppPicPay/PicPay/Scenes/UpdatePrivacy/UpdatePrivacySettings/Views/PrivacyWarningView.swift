import UI
import UIKit

private extension PrivacyWarningView.Layout {
    enum Size {
        static let icon = CGSize(width: 24, height: 24)
    }
}
final class PrivacyWarningView: UIView {
    fileprivate enum Layout { }
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.viewStyle(CardViewStyle())
        return view
    }()
    
    private lazy var iconView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        guard #available(iOS 13, *), traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) else {
            return
        }
        
        containerView.viewStyle(CardViewStyle())
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setValues(image: UIImage, description: NSAttributedString) {
        self.iconView.image = image
        self.descriptionLabel.attributedText = description
    }
}

extension PrivacyWarningView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(containerView)
        containerView.addSubviews(iconView, descriptionLabel)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        iconView.snp.makeConstraints {
            $0.top.leading.equalToSuperview().inset(Spacing.base02)
            $0.size.equalTo(Layout.Size.icon)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.trailing.bottom.equalToSuperview().inset(Spacing.base02)
            $0.leading.equalTo(iconView.snp.trailing).offset(Spacing.base01)
        }
    }
}
