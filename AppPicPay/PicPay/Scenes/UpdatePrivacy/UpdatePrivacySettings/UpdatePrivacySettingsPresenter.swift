import Foundation
import AssetsKit
import UI

enum UpdatePrivacySettingsFeedbackState {
    case success
    case error
}

protocol UpdatePrivacySettingsPresenting: AnyObject {
    var viewController: UpdatePrivacySettingsDisplaying? { get set }
    func goToHome()
    func configurePrivacySettings()
    func startLoading()
    func showSuccess()
    func showError()
    func configureConfirmationPopup()
    func privacySettingsPopupCallback(data: PopupSettingsOption)
}

final class UpdatePrivacySettingsPresenter {
    typealias Localizable = Strings.UpdatePrivacy
    
    var viewController: UpdatePrivacySettingsDisplaying?
    private let coordinator: UpdatePrivacySettingsCoordinating

    init(coordinator: UpdatePrivacySettingsCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - UpdatePrivacySettingsPresenting
extension UpdatePrivacySettingsPresenter: UpdatePrivacySettingsPresenting {
   func configurePrivacySettings() {
        viewController?.configureWarning(with: Resources.Icons.icoBlueInfo.image,
                                         and: getAttributeText(Localizable.updatePrivacySettingsWarning))
        viewController?.configureDescription(with: Localizable.updatePrivacySettingsDecription)
        viewController?.configureProfileSwitch(with: Localizable.updatePrivacySettingPublicProfileTitle,
                                               and: getAttributeText(Localizable.updatePrivacySettingPublicProfileDescription))
        viewController?.configurePaymentSwitch(with: Localizable.updatePrivacySettingPublicPaymentsTitle,
                                               and: getAttributeText(Localizable.updatePrivacySettingPublicPaymentsDescription))
        viewController?.configureReceiveSwitch(with: Localizable.updatePrivacySettingPublicReceivesTitle,
                                               and: getAttributeText(Localizable.updatePrivacySettingPublicReceivesDescription))
    }
    
    func showSuccess() {
        viewController?.displaySuccess(with: buildModel(with: .success))
    }
    
    func showError() {
        viewController?.displayError(with: buildModel(with: .error))
    }
    
    func startLoading() {
        viewController?.startLoading()
    }

    func goToHome() {
        coordinator.dismiss()
    }
    
    func configureConfirmationPopup() {
        let options = [
            PopupSettingsOptionData(.hideFromNow, Localizable.updatePrivacyPopupSecondOptionTitle),
            PopupSettingsOptionData(.hidePastActivities, Localizable.updatePrivacyPopupFirstOptionTitle)
        ]
        
        let model = PrivacySettingsPopupViewModel(title: Localizable.updatePrivacyPopupTitle,
                                                  primaryButtonTitle: Localizable.updatePrivacyPopupButtonTitle,
                                                  options: options,
                                                  primaryButtonAction: privacySettingsPopupCallback)
        
        viewController?.openPopup(with: model)
    }
    
    func privacySettingsPopupCallback(data: PopupSettingsOption) {
        viewController?.setConfirmationProperties(with: data)
    }
    
    // MARK: Private method
    private func getAttributeText(_ string: String) -> NSAttributedString {
        string.attributedStringWith(
            normalFont: Typography.bodySecondary().font(),
            highlightFont: Typography.bodySecondary(.highlight).font(),
            normalColor: Colors.grayscale500.color,
            highlightColor: Colors.grayscale500.color,
            underline: false
        )
    }
    
    private func buildModel(with state: UpdatePrivacySettingsFeedbackState) -> StatefulFeedbackViewModel {
        switch state {
        case .success:
            return StatefulFeedbackViewModel(
                image: Resources.Illustrations.iluSuccessWithBackground.image,
                content: (
                    title: Localizable.updatePrivacyConfirmationTitle,
                    description: Localizable.updatePrivacyConfirmationDescription
                ),
                button: (image: nil, title: Localizable.updatePrivacyConfirmationButtonClose),
                secondaryButton: nil
            )
        case .error:
            return StatefulFeedbackViewModel(
                image: Resources.Icons.icoSad.image,
                content: (
                    title: Localizable.updatePrivacyErrorTitle,
                    description: Localizable.updatePrivacyErrorDescription
                ),
                button: (image: nil, title: Localizable.updatePrivacyErrorButtonTitle),
                secondaryButton: nil
            )
        }
    }
}
