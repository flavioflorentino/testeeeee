import UIKit

enum UpdatePrivacySettingsFactory {
    static func make(origin: ReviewPrivacyOrigin) -> UpdatePrivacySettingsViewController {
        let container = DependencyContainer()
        let service: UpdatePrivacyServicing = UpdatePrivacyService(dependencies: container)
        let coordinator: UpdatePrivacySettingsCoordinating = UpdatePrivacySettingsCoordinator()
        let presenter: UpdatePrivacySettingsPresenting = UpdatePrivacySettingsPresenter(coordinator: coordinator)
        let interactor = UpdatePrivacySettingsInteractor(service: service, presenter: presenter, origin: origin, dependencies: container)
        let viewController = UpdatePrivacySettingsViewController(interactor: interactor)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
