import Core
import AnalyticsModule
import FeatureFlag
import Foundation

enum SettingsKey: String, CaseIterable {
    // Privacy keys
    case closedAccount
    case receiveInPrivate
    case paymentInPrivate
    case hidePastPayments
    case hidePastReceives
}

protocol UpdatePrivacySettingsInteracting: AnyObject {
    func loadContent()
    func updateConfigs(defaultConfigs: [String: Bool], payInPublic: Bool)
    func dismiss()
    func loadConfirmationPopup()
    func sendAnalyticsEventForShowingDialog(type: PrivacyItemType)
}

final class UpdatePrivacySettingsInteractor {
    typealias Dependencies = HasKVStore & HasAnalytics & HasConsumerManager & HasFeatureManager
    private let dependencies: Dependencies
    
    private let service: UpdatePrivacyServicing
    private let presenter: UpdatePrivacySettingsPresenting
    private let origin: ReviewPrivacyOrigin
    
    private var consumerId: String? {
        dependencies.consumerManager.consumer?.wsId.toString()
    }

    init(service: UpdatePrivacyServicing, presenter: UpdatePrivacySettingsPresenting, origin: ReviewPrivacyOrigin, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.origin = origin
        self.dependencies = dependencies
    }
}

// MARK: - UpdatePrivacySettingsInteracting
extension UpdatePrivacySettingsInteractor: UpdatePrivacySettingsInteracting {
    func loadContent() {
        presenter.configurePrivacySettings()
    }
    
    func dismiss() {
        presenter.goToHome()
    }
    
    func updateConfigs(defaultConfigs: [String: Bool], payInPublic: Bool) {
        presenter.startLoading()
        ConsumerManager.shared.privacyConfig = payInPublic
            ? FeedItemVisibilityInt.friends.rawValue
            : FeedItemVisibilityInt.private.rawValue
        
        service.updateSettings(configs: defaultConfigs, reviewedFrom: origin.rawValue) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success:
                self.dependencies.kvStore.setBool(true, with: KVKey.isUserReviewPrivacy)
                self.sendAnalyticsEventsForSavingConfigs(configs: defaultConfigs)
                self.presenter.showSuccess()
            case .failure:
                self.dependencies.analytics.log(UpdatePrivacyMessageEvent.screenErrorViewed(self.consumerId))
                self.presenter.showError()
            }
        }
    }
    
    func loadConfirmationPopup() {
        presenter.configureConfirmationPopup()
    }
    
    func sendAnalyticsEventForShowingDialog(type: PrivacyItemType) {
        let origin = DialogOrigin.reviewPrivacy
        
        switch type {
        case .payment:
            dependencies.analytics.log(UpdatePrivacyEvent.paymentDialogViewed(consumerId, origin))
        case .receive:
            dependencies.analytics.log(UpdatePrivacyEvent.receiveDialogViewed(consumerId, origin))
        case .account:
            dependencies.analytics.log(UpdatePrivacyEvent.accountDialogViewed(consumerId, origin))
        }
    }
}

// MARK: Helper method
private extension UpdatePrivacySettingsInteractor {
    func sendAnalyticsEventsForSavingConfigs(configs: [String: Bool]) {
        guard let isPublicProfile = configs[getSettingKey(of: SettingsKey.closedAccount.rawValue.snakeCased())],
              let isPublicPayments = configs[getSettingKey(of: SettingsKey.paymentInPrivate.rawValue.snakeCased())],
              let isPublicReceives = configs[getSettingKey(of: SettingsKey.receiveInPrivate.rawValue.snakeCased())],
              let isHiddenPastPayments = configs[getSettingKey(of: SettingsKey.hidePastPayments.rawValue.snakeCased())],
              let isHiddenPastReceives = configs[getSettingKey(of: SettingsKey.hidePastReceives.rawValue.snakeCased())]
        else { return }
        
        dependencies.analytics.log(UpdatePrivacyEvent.saveReviewed(consumerId,
                                                                   origin.rawValue,
                                                                   !isPublicProfile,
                                                                   !isPublicPayments,
                                                                   !isPublicReceives,
                                                                   isHiddenPastPayments,
                                                                   isHiddenPastReceives))
    }
    
    func getSettingKey(of text: String?) -> String {
        guard let key = text else { return "" }
        return key
    }
}
