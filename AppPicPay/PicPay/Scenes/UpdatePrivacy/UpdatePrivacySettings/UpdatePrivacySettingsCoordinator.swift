import UIKit

enum UpdatePrivacySettingsAction {
    case dismiss
    case privacySettings
}

protocol UpdatePrivacySettingsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func dismiss()
}

final class UpdatePrivacySettingsCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - UpdatePrivacySettingsCoordinating
extension UpdatePrivacySettingsCoordinator: UpdatePrivacySettingsCoordinating {
    func dismiss() {
       viewController?.navigationController?.popToRootViewController(animated: true)
    }
}
