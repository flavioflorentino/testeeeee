import UI
import UIKit
import AssetsKit

enum PrivacyItemType {
    case account
    case payment
    case receive
}

protocol UpdatePrivacySettingsDisplaying: AnyObject {
    func configureWarning(with image: UIImage, and description: NSAttributedString)
    func configureDescription(with text: String)
    func configureProfileSwitch(with title: String, and description: NSAttributedString)
    func configurePaymentSwitch(with title: String, and description: NSAttributedString)
    func configureReceiveSwitch(with title: String, and description: NSAttributedString)
    func startLoading()
    func displaySuccess(with success: StatefulFeedbackViewModel)
    func displayError(with error: StatefulFeedbackViewModel)
    func openPopup(with model: PrivacySettingsPopupViewModel)
    func setConfirmationProperties(with data: PopupSettingsOption)
}

final class UpdatePrivacySettingsViewController: ViewController<UpdatePrivacySettingsInteracting, UIView> {
    typealias Localizable = Strings.UpdatePrivacy
    
    private var selectedType: PrivacyItemType?
    private var hidePastPayments = false
    private var hidePastReceives = false
    
    private lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        
        if #available(iOS 11.0, *) {
            scroll.contentInsetAdjustmentBehavior = .never
        }
        
        scroll.alwaysBounceVertical = true
        scroll.showsVerticalScrollIndicator = false

        return scroll
    }()
    
    private lazy var containerView = UIView()
    
    private lazy var warningView = PrivacyWarningView()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale500.color)
        return label
    }()
        
    private lazy var profileSwitch: PrivacyItemSwitchView = {
        let switchItem = PrivacyItemSwitchView(type: .account)
        switchItem.delegate = self
        return switchItem
    }()
    
    private lazy var paymentsSwitch: PrivacyItemSwitchView = {
        let switchItem = PrivacyItemSwitchView(type: .payment)
        switchItem.delegate = self
        return switchItem
    }()
    
    private lazy var receivesSwitch: PrivacyItemSwitchView = {
        let switchItem = PrivacyItemSwitchView(type: .receive)
        switchItem.delegate = self
        return switchItem
    }()
    
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [profileSwitch, paymentsSwitch, receivesSwitch])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03
        return stackView
    }()
    
    private lazy var saveButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(Localizable.updatePrivacySettingButtonSave, for: .normal)
        button.addTarget(self, action: #selector(saveButtonTapped), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.loadContent()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func buildViewHierarchy() {
        containerView.addSubviews(warningView, descriptionLabel, contentStackView, saveButton)
        scrollView.addSubview(containerView)
        
        view.addSubview(scrollView)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalTo(view)
        }
        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.width.equalTo(view)
            $0.height.greaterThanOrEqualTo(view)
        }
        warningView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base06)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base04)
        }
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(warningView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base04)
        }
        contentStackView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base06)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        saveButton.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(contentStackView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base03)
        }
    }

    override func configureViews() {
        configureNavigation()
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

private extension UpdatePrivacySettingsViewController {
    func configureNavigation() {
        title = Localizable.updatePrivacySettingTitle
        let backButton = UIBarButtonItem()
        backButton.title = Localizable.updatePrivacySettingBackButtonTitle
        backButton.tintColor = Colors.brandingBase.color
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        navigationController?.navigationBar.navigationStyle(StandardNavigationStyle())
    }
    
    @objc
    func saveButtonTapped() {
        var configs: [String: Bool] = [:]
        
        shouldAddKey(settingsKey: SettingsKey.closedAccount.rawValue.snakeCased(), value: !profileSwitch.itemIsOn, to: &configs)
        shouldAddKey(settingsKey: SettingsKey.receiveInPrivate.rawValue.snakeCased(), value: !receivesSwitch.itemIsOn, to: &configs)
        shouldAddKey(settingsKey: SettingsKey.paymentInPrivate.rawValue.snakeCased(), value: !paymentsSwitch.itemIsOn, to: &configs)
        shouldAddKey(settingsKey: SettingsKey.hidePastPayments.rawValue.snakeCased(), value: hidePastPayments, to: &configs)
        shouldAddKey(settingsKey: SettingsKey.hidePastReceives.rawValue.snakeCased(), value: hidePastReceives, to: &configs)
        
        interactor.updateConfigs(defaultConfigs: configs, payInPublic: paymentsSwitch.itemIsOn)
    }
    
    func shouldAddKey(settingsKey: String?, value: Bool, to dictionary: inout [String: Bool]) {
        if let key = settingsKey {
            dictionary[key] = value
        }
    }
}

extension UpdatePrivacySettingsViewController: UpdatePrivacySettingsDisplaying {
    func startLoading() {
        containerView.isHidden = true
        navigationController?.setNavigationBarHidden(true, animated: false)
        beginState(animated: true, model: StateLoadingViewModel(message: ""))
    }
    
    func displaySuccess(with success: StatefulFeedbackViewModel) {
        endState(model: success)
    }
    
    func displayError(with error: StatefulFeedbackViewModel) {
        endState(model: error)
    }
    
    func configureWarning(with image: UIImage, and description: NSAttributedString) {
        warningView.setValues(image: image, description: description)
    }
    
    func configureDescription(with text: String) {
        descriptionLabel.text = text
    }
    
    func configureProfileSwitch(with title: String, and description: NSAttributedString) {
        profileSwitch.setValues(title: title, description: description)
    }
    
    func configurePaymentSwitch(with title: String, and description: NSAttributedString) {
        paymentsSwitch.setValues(title: title, description: description)
    }
    
    func configureReceiveSwitch(with title: String, and description: NSAttributedString) {
        receivesSwitch.setValues(title: title, description: description)
    }
    
    func openPopup(with model: PrivacySettingsPopupViewModel) {
        let confirmationPopup = PrivacySettingsPopupViewController(viewModel: model)
        present(confirmationPopup, animated: true)
    }
    
    func setConfirmationProperties(with data: PopupSettingsOption) {
        guard let selectedType = self.selectedType else { return }
        
        let value = data == .hidePastActivities
        
        if selectedType == .payment {
            hidePastPayments = value
        } else {
            hidePastReceives = value
        }
    }
}

// MARK: - StatefulProviding
extension UpdatePrivacySettingsViewController: StatefulTransitionViewing {
    // Componente de StatefulTransitionViewing será refatorado para as ações ficarem mais genéricas
    // (por isso o nome não está condizente com a ação)
    public func didTryAgain() {
        interactor.dismiss()
    }
    
    public func statefulViewForError() -> StatefulViewing {
        StatefulFeedbackView()
    }
}

// MARK: - private extension
private extension UpdatePrivacySettingsViewController {
    func buildProfileWarningPopup() {
        let primaryButtonAction = ApolloAlertAction(title: Localizable.updatePrivacyPopupProfileNo,
                                                    completion: cancelPrivateProfileAction)
        let linkButtonAction = ApolloAlertAction(title: Localizable.updatePrivacyPopupProfileYes,
                                                 completion: { })
        
        showApolloAlert(title: Localizable.updatePrivacyPopupProfileTitle,
                        subtitle: Localizable.updatePrivacyPopupProfileDescription,
                        primaryButtonAction: primaryButtonAction,
                        linkButtonAction: linkButtonAction,
                        dismissOnTouchOutside: false)
    }
    
    func cancelPrivateProfileAction() {
        profileSwitch.changeSwitchValue(with: true)
    }
}

// MARK: - PrivacyItemSwitchDelegate
extension UpdatePrivacySettingsViewController: PrivacyItemSwitchDelegate {
    func didSwitchChange(type: PrivacyItemType, isOn: Bool) {
        switch type {
        case .account:
            guard !isOn else { return }
            buildProfileWarningPopup()
        case .payment, .receive:
            selectedType = type

            guard isOn else {
                interactor.sendAnalyticsEventForShowingDialog(type: type)
                interactor.loadConfirmationPopup()
                return
            }

            if type == .payment {
                hidePastPayments = false
            } else {
                hidePastReceives = false
            }
        }
    }
}
