import UI
import UIKit
import AssetsKit

protocol UpdatePrivacyMessageDisplaying: AnyObject {
    func setTitle(with text: String)
    func setDescription<S: LabelStyle>(with text: String, and style: S)
    func setFirstItemDescription(with text: String, and image: UIImage)
    func setSecondItemDescription(with text: String, and image: UIImage)
    func setThirdItemDescription(with text: String, and image: UIImage)
    func setButtonTitle(with text: String)
    func startLoading()
    func displaySuccess(with success: StatefulFeedbackViewModel)
    func displayError(with error: StatefulFeedbackViewModel)
}

final class UpdatePrivacyMessageViewController: ViewController<UpdatePrivacyMessageInteracting, UIView> {
    typealias Localizable = Strings.UpdatePrivacy
    
    private lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        
        if #available(iOS 11.0, *) {
            scroll.contentInsetAdjustmentBehavior = .never
        }
        
        scroll.alwaysBounceVertical = true
        scroll.showsVerticalScrollIndicator = false
        return scroll
    }()
    
    private lazy var contentView = UIView()

    private lazy var imageView: UIImageView = {
        let view = UIImageView(image: Resources.Illustrations.iluSocialInteraction.image)
        view.contentMode = .scaleAspectFit
        return  view
    }()
    
    private(set) lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .small))
        return label
    }()
    
    private(set) lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale500.color)
        return label
    }()
    
    private(set) lazy var itemDescriptionFirst: ItemDescriptionView = {
        let item = ItemDescriptionView()
        item.isHidden = true
        return item
    }()
    
    private(set) lazy var itemDescriptionSecond: ItemDescriptionView = {
        let item = ItemDescriptionView()
        item.isHidden = true
        return item
    }()
    
    private(set) lazy var itemDescriptionThird: ItemDescriptionView = {
        let item = ItemDescriptionView()
        item.isHidden = true
        return item
    }()
    
    private(set) lazy var containerStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            descriptionLabel,
            itemDescriptionFirst,
            itemDescriptionSecond,
            itemDescriptionThird
        ])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private(set) lazy var primaryButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.updatePrivacyMessagePrimaryButtonTitle, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(primaryButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private(set) lazy var secondaryButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.updatePrivacyMessageSecondaryButtonTitle, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(secondaryButtonTapped), for: .touchUpInside)
        return button
    }()

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        interactor.sendAnalyticsForViewingIntroScreen()
        interactor.loadContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func buildViewHierarchy() {
        view.addSubviews(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubviews(imageView, titleLabel, containerStackView, primaryButton, secondaryButton)
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalTo(view)
        }
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.width.equalTo(view)
            $0.height.greaterThanOrEqualTo(view)
        }
        
        imageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base09)
            $0.centerX.equalToSuperview()
        }
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base05)
        }
        containerStackView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base05)
        }

        primaryButton.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(containerStackView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalTo(secondaryButton.snp.top).offset(-Spacing.base01)
        }

        secondaryButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base03)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

extension UpdatePrivacyMessageViewController: UpdatePrivacyMessageDisplaying {
    func setTitle(with text: String) {
        titleLabel.text = text
    }

    func setDescription<S>(with text: String, and style: S) where S: LabelStyle {
        descriptionLabel.text = text
        descriptionLabel.labelStyle(style)
            .with(\.textColor, Colors.grayscale500.color)
    }
        
    func setFirstItemDescription(with text: String, and image: UIImage) {
        itemDescriptionFirst.setValues(image: image, description: text)
        itemDescriptionFirst.isHidden = false
    }
    
    func setSecondItemDescription(with text: String, and image: UIImage) {
        itemDescriptionSecond.setValues(image: image, description: text)
        itemDescriptionSecond.isHidden = false
    }
    
    func setThirdItemDescription(with text: String, and image: UIImage) {
        itemDescriptionThird.setValues(image: image, description: text)
        itemDescriptionThird.isHidden = false
    }
    
    func setButtonTitle(with text: String) {
        primaryButton.setTitle(text, for: .normal)
    }
    
    func startLoading() {
        beginState(animated: true, model: StateLoadingViewModel(message: ""))
    }
    
    func displaySuccess(with success: StatefulFeedbackViewModel) {
        endState(model: success)
    }
    
    func displayError(with error: StatefulFeedbackViewModel) {
        endState(model: error)
    }
}

private extension UpdatePrivacyMessageViewController {
    @objc
    func secondaryButtonTapped() {
        interactor.goToReviewPrivacySettings()
    }

    @objc
    func primaryButtonTapped() {
        interactor.saveDefaultConfigs()
    }
}

// MARK: - StatefulProviding
extension UpdatePrivacyMessageViewController: StatefulTransitionViewing {
    // Componente de StatefulTransitionViewing será refatorado para as ações ficarem mais genéricas
    // (por isso o nome não está condizente com a ação)
    public func didTryAgain() {
        interactor.dismiss()
    }
    
    public func statefulViewForError() -> StatefulViewing {
        StatefulFeedbackView()
    }
}
