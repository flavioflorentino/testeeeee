import Core
import FeatureFlag
import Foundation
import AnalyticsModule

protocol UpdatePrivacyMessageInteracting: AnyObject {
    func loadContent()
    func goToReviewPrivacySettings()
    func saveDefaultConfigs()
    func dismiss()
    func sendAnalyticsForViewingIntroScreen()
}

final class UpdatePrivacyMessageInteractor {
    private typealias Localizable = Strings.UpdatePrivacy

    typealias Dependencies = HasConsumerManager & HasKVStore & HasFeatureManager & HasAnalytics
    private let dependencies: Dependencies
    
    private let presenter: UpdatePrivacyMessagePresenting
    private let service: UpdatePrivacyServicing
    private let origin: ReviewPrivacyOrigin
    
    private var firstTextOption = false
    
    private var consumerId: String? {
        self.dependencies.consumerManager.consumer?.wsId.toString()
    }

    init(service: UpdatePrivacyServicing, presenter: UpdatePrivacyMessagePresenting, origin: ReviewPrivacyOrigin, dependencies: Dependencies) {
        self.presenter = presenter
        self.service = service
        self.origin = origin
        self.dependencies = dependencies
    }
}

// MARK: - UpdatePrivacyMessageInteracting
extension UpdatePrivacyMessageInteractor: UpdatePrivacyMessageInteracting {
    func loadContent() {
        firstTextOption = dependencies.featureManager.isActive(.experimentReviewPrivacyText)
        
        if firstTextOption {
            presenter.configureView()
        } else {
            presenter.configureViewWithSecondTextOption()
        }
    }
    
    func saveDefaultConfigs() {
        dependencies.analytics.log(UpdatePrivacyMessageEvent.didTapSaveDefaultConfigs(consumerId, firstTextOption))
        presenter.startLoading()
        
        service.updateSettings(configs: [:], reviewedFrom: origin.rawValue) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success:
                self.dependencies.analytics.log(UpdatePrivacyMessageEvent.screenErrorViewed(self.consumerId))
                self.dependencies.kvStore.setBool(true, with: KVKey.isUserReviewPrivacy)
                self.presenter.showSuccess()
            case .failure:
                self.dependencies.analytics.log(UpdatePrivacyMessageEvent.screenErrorViewed(self.consumerId))
                self.presenter.showError()
            }
        }
    }
    
    func goToReviewPrivacySettings() {
        dependencies.analytics.log(UpdatePrivacyMessageEvent.didTapReviewPrivacySettings(consumerId, firstTextOption))
        presenter.didNextStep(action: .reviewPrivacySettings)
    }
    
    func dismiss() {
        presenter.didNextStep(action: .dismiss)
    }
    
    func sendAnalyticsForViewingIntroScreen() {
        dependencies.analytics.log(UpdatePrivacyMessageEvent.screenIntroViewed(consumerId, origin))
    }
}
