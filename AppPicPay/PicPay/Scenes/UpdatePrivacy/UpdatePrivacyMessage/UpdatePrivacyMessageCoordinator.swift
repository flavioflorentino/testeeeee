import UIKit

enum UpdatePrivacyMessageAction {
    case dismiss
    case reviewPrivacySettings
}

protocol UpdatePrivacyMessageCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: UpdatePrivacyMessageAction)
}

final class UpdatePrivacyMessageCoordinator {
    weak var viewController: UIViewController?
    
    private let origin: ReviewPrivacyOrigin
    
    init(origin: ReviewPrivacyOrigin) {
        self.origin = origin
    }
}

// MARK: - UpdatePrivacyMessageCoordinating
extension UpdatePrivacyMessageCoordinator: UpdatePrivacyMessageCoordinating {
    func perform(action: UpdatePrivacyMessageAction) {
        switch action {
        case .dismiss:
            viewController?.navigationController?.popToRootViewController(animated: true)
        case .reviewPrivacySettings:
            let controller = UpdatePrivacySettingsFactory.make(origin: origin)
            viewController?.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
