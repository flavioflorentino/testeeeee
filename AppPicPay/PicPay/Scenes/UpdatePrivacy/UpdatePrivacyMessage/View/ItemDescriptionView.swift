import UI
import UIKit

private extension ItemDescriptionView.Layout {
    enum Size {
        static let icon = CGSize(width: 24, height: 24)
    }
}

final class ItemDescriptionView: UIView {
    fileprivate enum Layout { }

    private lazy var iconView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, Colors.grayscale500.color)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setValues(image: UIImage, description: String) {
        self.iconView.image = image
        self.descriptionLabel.text = description
    }
}

extension ItemDescriptionView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(iconView, descriptionLabel)
    }
    
    func setupConstraints() {
        iconView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base01)
            $0.size.equalTo(Layout.Size.icon)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.trailing.bottom.equalToSuperview().inset(Spacing.base02)
            $0.leading.equalTo(iconView.snp.trailing).offset(Spacing.base02)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}
