import UIKit

enum ReviewPrivacyOrigin: String {
    case transaction
    case home
}

enum UpdatePrivacyMessageFactory {
    static func make(origin: ReviewPrivacyOrigin) -> UpdatePrivacyMessageViewController {
        let container = DependencyContainer()
        let service: UpdatePrivacyServicing = UpdatePrivacyService(dependencies: container)
        let coordinator: UpdatePrivacyMessageCoordinating = UpdatePrivacyMessageCoordinator(origin: origin)
        let presenter: UpdatePrivacyMessagePresenting = UpdatePrivacyMessagePresenter(coordinator: coordinator)
        let interactor = UpdatePrivacyMessageInteractor(service: service, presenter: presenter, origin: origin, dependencies: container)
        let viewController = UpdatePrivacyMessageViewController(interactor: interactor)
        viewController.hidesBottomBarWhenPushed = true
        
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
