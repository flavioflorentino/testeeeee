import Foundation
import UI
import AssetsKit

protocol UpdatePrivacyMessagePresenting: AnyObject {
    var viewController: UpdatePrivacyMessageDisplaying? { get set }
    func configureView()
    func configureViewWithSecondTextOption()
    func startLoading()
    func showSuccess()
    func showError()
    func didNextStep(action: UpdatePrivacyMessageAction)
}

final class UpdatePrivacyMessagePresenter {
    typealias Localizable = Strings.UpdatePrivacy
    weak var viewController: UpdatePrivacyMessageDisplaying?

    private let coordinator: UpdatePrivacyMessageCoordinating

    init(coordinator: UpdatePrivacyMessageCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - UpdatePrivacyMessagePresenting
extension UpdatePrivacyMessagePresenter: UpdatePrivacyMessagePresenting {
    func configureView() {
        viewController?.setTitle(with: Localizable.updatePrivacyMessageTitle)
        viewController?.setDescription(with: Localizable.updatePrivacyMessageDescription, and: BodySecondaryLabelStyle())
        viewController?.setFirstItemDescription(with: Localizable.updatePrivacyMessageDescriptionItem1,
                                                and: Resources.Icons.icoValueGray.image)
        viewController?.setSecondItemDescription(with: Localizable.updatePrivacyMessageDescriptionItem2,
                                                 and: Resources.Icons.icoLock.image)
        viewController?.setThirdItemDescription(with: Localizable.updatePrivacyMessageDescriptionItem3,
                                                and: Resources.Icons.icoSafe.image)
        viewController?.setButtonTitle(with: Localizable.updatePrivacyMessagePrimaryButtonTitle)
    }
    
    func configureViewWithSecondTextOption() {
        viewController?.setTitle(with: Localizable.updatePrivacyMessageTitleSecondOption)
        viewController?.setDescription(with: Localizable.updatePrivacyMessageDescriptionSecondOption, and: BodySecondaryLabelStyle(type: .highlight))
        viewController?.setFirstItemDescription(with: Localizable.updatePrivacyMessageDescriptionItem1SecondOption,
                                                and: Resources.Icons.icoValueGray.image)
        viewController?.setSecondItemDescription(with: Localizable.updatePrivacyMessageDescriptionItem2SecondOption,
                                                 and: Resources.Icons.icoLock.image)
        viewController?.setButtonTitle(with: Localizable.updatePrivacyMessagePrimaryButtonTitleSecondOption)
    }
    
    func showSuccess() {
        viewController?.displaySuccess(with: buildModel(with: .success))
    }
    
    func showError() {
        viewController?.displayError(with: buildModel(with: .error))
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func didNextStep(action: UpdatePrivacyMessageAction) {
        coordinator.perform(action: action)
    }
    
    // MARK: Private method
    private func buildModel(with state: UpdatePrivacySettingsFeedbackState) -> StatefulFeedbackViewModel {
        switch state {
        case .success:
            return StatefulFeedbackViewModel(
                image: Resources.Illustrations.iluSuccessWithBackground.image,
                content: (
                    title: Localizable.updatePrivacyConfirmationTitle,
                    description: Localizable.updatePrivacyConfirmationDescription
                ),
                button: (image: nil, title: Localizable.updatePrivacyConfirmationButtonClose),
                secondaryButton: nil
            )
        case .error:
            return StatefulFeedbackViewModel(
                image: Resources.Icons.icoSad.image,
                content: (
                    title: Localizable.updatePrivacyErrorTitle,
                    description: Localizable.updatePrivacyErrorDescription
                ),
                button: (image: nil, title: Localizable.updatePrivacyErrorButtonTitle),
                secondaryButton: nil
            )
        }
    }
}
