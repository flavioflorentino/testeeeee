enum PopupSettingsOption: String {
    case hidePastActivities
    case hideFromNow
}

typealias PopupSettingsOptionData = (value: PopupSettingsOption, description: String)
