import UIKit
import UI

struct PrivacySettingsPopupViewModel {
    let title, primaryButtonTitle: String
    let options: [PopupSettingsOptionData]
    let primaryButtonAction: ((_: PopupSettingsOption) -> Void)?
}

fileprivate extension PrivacySettingsPopupViewController.Layout {
    static let maxWidth = CGFloat(300)
}

final class PrivacySettingsPopupViewController: ViewController<PrivacySettingsPopupViewModel, UIView> {
    fileprivate enum Layout {}
    
    private lazy var dialogTransitioning = DialogTransitioningDelegate()
        
    private lazy var contentView: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle(cornerRadius: .strong))
            .with(\.backgroundColor, Colors.backgroundPrimary.color)
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, Colors.grayscale900.color)
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var radioButtonForm: RadioButtonForm = {
        let form = RadioButtonForm()
        form.viewStyle(RoundedViewStyle())
        return form
    }()

    private lazy var primaryButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapPrimaryButton), for: .touchUpInside)
        return button
    }()
    
    override func buildViewHierarchy() {
        view.addSubview(contentView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(radioButtonForm)
        contentView.addSubview(primaryButton)
    }
    
    override func setupConstraints() {
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.width.equalTo(Layout.maxWidth)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }
        
        radioButtonForm.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
        }

        primaryButton.snp.makeConstraints {
            $0.top.equalTo(radioButtonForm.snp.bottom).offset(Spacing.base04)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base04)
        }
    }
    
    override func configureViews() {
        configureTitleLabel()
        configurePrimaryButtonTitle()
        configureRadioForm()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        updatePreferredContentSize()
    }
    
    override init(viewModel: PrivacySettingsPopupViewModel) {
        super.init(viewModel: viewModel)
        modalPresentationStyle = .custom
        transitioningDelegate = dialogTransitioning
        definesPresentationContext = true
    }
}

private extension PrivacySettingsPopupViewController {
    private func updatePreferredContentSize() {
        let maximumWidth = Layout.maxWidth
        let targetSize = CGSize(width: maximumWidth, height: .leastNonzeroMagnitude)
        preferredContentSize = contentView.systemLayoutSizeFitting(
            targetSize,
            withHorizontalFittingPriority: .required,
            verticalFittingPriority: .defaultLow
        )
    }
    
    func configureTitleLabel() {
        titleLabel.text = viewModel.title
    }
    
    func configurePrimaryButtonTitle() {
        primaryButton.setTitle(viewModel.primaryButtonTitle, for: .normal)
    }
    
    func configureRadioForm() {
        radioButtonForm.data = viewModel.options
        radioButtonForm.selectFirstRadio()
    }
}

@objc
private extension PrivacySettingsPopupViewController {
    func didTapPrimaryButton() {
        guard let selectedOption = radioButtonForm.selectedValue() as? PopupSettingsOption else { return }
        dismiss(animated: true, completion: {
            self.viewModel.primaryButtonAction?(selectedOption)
        })
    }
}
