import Core

enum UpdatePrivacyEndpoint {
    case saveSettings(configs: [String: Bool], reviewdFrom: String)
    case revisionStatus
}

extension UpdatePrivacyEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .saveSettings:
            return "me/configs/review"
        case .revisionStatus:
            return "bank-secrecy/revision-status"
        }
    }
    
    var body: Data? {
        switch self {
        case let .saveSettings(configs, reviewdFrom):
            return ["configs": configs, "reviewed_from": reviewdFrom].toData()
        default:
            return nil
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .saveSettings:
            return .patch
        case .revisionStatus:
            return .get
        }
    }
}
