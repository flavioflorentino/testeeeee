import Core
import Foundation

protocol UpdatePrivacyServicing {
    func updateSettings(configs: [String: Bool], reviewedFrom: String, completion: @escaping (Result<NoContent, ApiError>) -> Void)
    func checkRevisionStatus(completion: @escaping (Result<RevisionStatusModel, ApiError>) -> Void)
}

final class UpdatePrivacyService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - UpdatePrivacySettingsServicing
extension UpdatePrivacyService: UpdatePrivacyServicing {
    func updateSettings(configs: [String: Bool], reviewedFrom: String, completion: @escaping (Result<NoContent, ApiError>) -> Void) {
        let endpoint = UpdatePrivacyEndpoint.saveSettings(configs: configs, reviewdFrom: reviewedFrom)
        let api = Api<NoContent>(endpoint: endpoint)
        
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func checkRevisionStatus(completion: @escaping (Result<RevisionStatusModel, ApiError>) -> Void) {
        let endpoint = UpdatePrivacyEndpoint.revisionStatus
        let api = Api<RevisionStatusModel>(endpoint: endpoint)
        
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
