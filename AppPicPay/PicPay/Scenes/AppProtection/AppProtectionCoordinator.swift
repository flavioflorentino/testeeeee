import UIKit

enum AppProtectionAction {
    case dismiss
}

protocol AppProtectionCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: AppProtectionAction)
}

final class AppProtectionCoordinator: AppProtectionCoordinating {
    weak var viewController: UIViewController?

    func perform(action: AppProtectionAction) {
        switch action {
        case .dismiss:
            viewController?.dismiss(animated: true)
        }
    }
}
