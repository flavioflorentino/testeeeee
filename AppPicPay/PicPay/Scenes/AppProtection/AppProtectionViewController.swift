import AssetsKit
import UI
import UIKit

protocol AppProtectionDisplaying: AnyObject {
    func setBiometricType(text: String)
    func setSwitch(value: Bool)
    func showDescriptionLabel()
    func showPopupController(title: String, message: String, buttonTitle: String)
    func hideCloseButton()
}

private extension AppProtectionViewController.Layout {
    enum Height {
        static let separatorView: CGFloat = 1
    }
}

final class AppProtectionViewController: ViewController<AppProtectionInteracting, UIView>, AppProtectionDisplaying {
    fileprivate enum Layout { }
    typealias Localizable = AppProtectionLocalizable

    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(Resources.Icons.icoClose.image, for: .normal)
        button.addTarget(self, action: #selector(didTapCloseButton), for: .touchUpInside)
        return button
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .grayscale750())
            .with(\.text, Localizable.titleLabel.text)
        return label
    }()

    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale750())
            .with(\.text, Localizable.subtitleLabel.text)
        return label
    }()

    private lazy var topLineSeparator: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        return view
    }()

    private lazy var biometricTypeLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale750())
        return label
    }()

    private lazy var protectionSwitch: UISwitch = {
        let protectionSwitch = UISwitch()
        protectionSwitch.addTarget(self, action: #selector(protectionSwitchValueChanged), for: .valueChanged)
        return protectionSwitch
    }()

    private lazy var bottomLineSeparator: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale100.color
        return view
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale750())
            .with(\.text, Localizable.descriptionLabel.text)
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.setupView()
        interactor.getProtectionSwitchValue()
    }

    override func buildViewHierarchy() {
        view.addSubview(closeButton)
        view.addSubview(titleLabel)
        view.addSubview(subtitleLabel)
        view.addSubview(topLineSeparator)
        view.addSubview(biometricTypeLabel)
        view.addSubview(protectionSwitch)
        view.addSubview(bottomLineSeparator)
        view.addSubview(descriptionLabel)
    }

    override func setupConstraints() {
        closeButton.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }

        titleLabel.snp.makeConstraints {
            $0.top.equalTo(closeButton.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }

        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.equalTo(titleLabel)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }

        topLineSeparator.snp.makeConstraints {
            $0.top.equalTo(subtitleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalTo(subtitleLabel)
            $0.height.equalTo(Layout.Height.separatorView)
        }

        biometricTypeLabel.snp.makeConstraints {
            $0.top.equalTo(topLineSeparator.snp.bottom).offset(Spacing.base03)
            $0.leading.equalTo(topLineSeparator)
            $0.trailing.equalTo(protectionSwitch.snp.leading)
        }

        protectionSwitch.snp.makeConstraints {
            $0.centerY.equalTo(biometricTypeLabel)
            $0.trailing.equalTo(topLineSeparator)
        }

        bottomLineSeparator.snp.makeConstraints {
            $0.top.equalTo(biometricTypeLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalTo(topLineSeparator)
            $0.height.equalTo(Layout.Height.separatorView)
        }

        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(bottomLineSeparator.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalTo(bottomLineSeparator)
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        descriptionLabel.isHidden = true
    }

    // MARK: - AppProtectionDisplaying

    func setBiometricType(text: String) {
        biometricTypeLabel.text = text
    }

    func setSwitch(value: Bool) {
        protectionSwitch.setOn(value, animated: true)
    }

    func showDescriptionLabel() {
        descriptionLabel.isHidden = false
    }

    func showPopupController(title: String, message: String, buttonTitle: String) {
        let popupController = PopupViewController(
            title: title,
            description: message,
            preferredType: .text
        )
        popupController.hideCloseButton = true
        popupController.addAction(PopupAction(title: buttonTitle, style: .fill))
        showPopup(popupController)
    }

    func hideCloseButton() {
        closeButton.isHidden = true
        closeButton.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.size.equalTo(CGSize.zero)
        }
    }
}

@objc
private extension AppProtectionViewController {
    func protectionSwitchValueChanged() {
        interactor.switchDidChange(value: protectionSwitch.isOn)
    }

    func didTapCloseButton() {
        interactor.close()
    }
}
