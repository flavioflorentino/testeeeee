import Foundation

enum AppProtectionLocalizable: String, Localizable {
    case titleLabel
    case subtitleLabel
    case descriptionLabel

    case popupErrorTitle
    case popupErrorMessage
    case popupErrorButton

    case biometricTouchType
    case biometricFaceType
    case pinProtectionType

    case disableBiometryReason

    var key: String {
        rawValue
    }

    var file: LocalizableFile {
        .appProtection
    }
}
