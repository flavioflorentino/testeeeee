import Core
import Foundation
import SecurityModule

protocol AppProtectionServicing {
    var biometricType: DeviceBiometricType { get }
    var valueForAppProtection: Bool { get }
    func changeAppProtection(_ enable: Bool)
    func evaluateBiometry(reason: String, completion: @escaping (Bool, LAError?) -> Void)
}

final class AppProtectionService: AppProtectionServicing {
    typealias Dependencies = HasKVStore
    private let dependencies: Dependencies

    private var context = LAContext()

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    // MARK: - AppProtectionServicing

    var biometricType: DeviceBiometricType {
        DeviceSecurity.biometricType()
    }

    var valueForAppProtection: Bool {
        dependencies.kvStore.boolFor(AppProtectionKey.userDeviceProtectedValue)
    }

    func changeAppProtection(_ enable: Bool) {
        dependencies.kvStore.setBool(enable, with: AppProtectionKey.userDeviceProtectedValue)
    }

    func evaluateBiometry(reason: String,
                          completion: @escaping (Bool, LAError?) -> Void) {
        DeviceSecurity.evaluateBiometry(reasonDescription: reason, completion: completion)
    }
}
