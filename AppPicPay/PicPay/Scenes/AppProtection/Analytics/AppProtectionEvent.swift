import AnalyticsModule

enum AppProtectionEvent: AnalyticsKeyProtocol {
    case appear(switchValue: Bool)
    case switchTouched(switchValue: Bool)
    case switchChanged(switchValue: Bool)

    private var name: String {
        switch self {
        case .appear:
            return "Screen Viewed"
        case .switchTouched, .switchChanged:
            return "Button Clicked"
        }
    }

    private var properties: [String: Any] {
        switch self {
        case let .appear(switchValue):
            return ["screen_name": "APP_PROTECTION", "app_protection_status": switchStatus(switchValue)]

        case let .switchTouched(switchValue):
            return ["switch_app_protection_status": switchStatus(switchValue)]

        case let .switchChanged(switchValue):
            return ["switch_app_protection_changed": switchStatus(switchValue)]
        }
    }

    private func switchStatus(_ value: Bool) -> String {
        value ? "ENABLED" : "DISABLED"
    }

    private var providers: [AnalyticsProvider] {
        [.eventTracker, .mixPanel]
    }

    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
