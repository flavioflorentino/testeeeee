import Foundation
import SecurityModule

protocol AppProtectionPresenting: AnyObject {
    var viewController: AppProtectionDisplaying? { get set }
    func setBiometricText(for type: DeviceBiometricType)
    func hideCloseButton()
    func setSwitch(value: Bool)
    func showPasscodeNotSetError()
    func didNextStep(action: AppProtectionAction)
}

final class AppProtectionPresenter: AppProtectionPresenting {
    weak var viewController: AppProtectionDisplaying?
    typealias Localizable = AppProtectionLocalizable

    private let coordinator: AppProtectionCoordinating

    init(coordinator: AppProtectionCoordinating) {
        self.coordinator = coordinator
    }

    // MARK: - AppProtectionPresenting

    func setBiometricText(for type: DeviceBiometricType) {
        let text: String

        switch type {
        case .touchID:
            text = Localizable.biometricTouchType.text
        case .faceID:
            text = Localizable.biometricFaceType.text
        case .none:
            text = Localizable.pinProtectionType.text
            viewController?.showDescriptionLabel()
        }

        viewController?.setBiometricType(text: text)
    }

    func hideCloseButton() {
        viewController?.hideCloseButton()
    }

    func setSwitch(value: Bool) {
        viewController?.setSwitch(value: value)
    }

    func showPasscodeNotSetError() {
        viewController?.showPopupController(title: Localizable.popupErrorTitle.text,
                                            message: Localizable.popupErrorMessage.text,
                                            buttonTitle: Localizable.popupErrorButton.text)
    }

    func didNextStep(action: AppProtectionAction) {
        coordinator.perform(action: action)
    }
}
