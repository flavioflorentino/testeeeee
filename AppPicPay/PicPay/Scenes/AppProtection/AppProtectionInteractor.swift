import AnalyticsModule
import Foundation

protocol AppProtectionInteracting: AnyObject {
    func setupView()
    func getProtectionSwitchValue()
    func switchDidChange(value: Bool)
    func close()
}

final class AppProtectionInteractor: AppProtectionInteracting {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: AppProtectionServicing
    private let presenter: AppProtectionPresenting
    private let shouldHideCloseButton: Bool

    init(
        service: AppProtectionServicing,
        presenter: AppProtectionPresenting,
        shouldHideCloseButton: Bool,
        dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.shouldHideCloseButton = shouldHideCloseButton
    }

    // MARK: - AppProtectionInteracting

    func setupView() {
        presenter.setBiometricText(for: service.biometricType)
        if shouldHideCloseButton {
            presenter.hideCloseButton()
        }
    }

    func getProtectionSwitchValue() {
        let switchValue = service.valueForAppProtection
        presenter.setSwitch(value: switchValue)
        dependencies.analytics.log(AppProtectionEvent.appear(switchValue: switchValue))
    }

    func switchDidChange(value: Bool) {
        dependencies.analytics.log(AppProtectionEvent.switchTouched(switchValue: !value))

        if value {
            changeAppProtection(true)
        } else {
            shouldDisableAppProtection()
        }
    }

    func close() {
        presenter.didNextStep(action: .dismiss)
    }
}

private extension AppProtectionInteractor {
    func changeAppProtection(_ enable: Bool) {
        service.changeAppProtection(enable)
        presenter.setSwitch(value: enable)
        dependencies.analytics.log(AppProtectionEvent.switchChanged(switchValue: enable))
    }

    func shouldDisableAppProtection() {
        service.evaluateBiometry(reason: AppProtectionLocalizable.disableBiometryReason.text) { [weak self] authorized, error in
            if case .passcodeNotSet = error?.code {
                self?.presenter.showPasscodeNotSetError()
            }
            self?.changeAppProtection(!authorized)
        }
    }
}
