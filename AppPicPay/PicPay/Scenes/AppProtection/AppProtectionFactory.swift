import Foundation

enum AppProtectionFactory {
    static func make(shouldHideCloseButton: Bool = true) -> AppProtectionViewController {
        let container = DependencyContainer()
        let coordinator = AppProtectionCoordinator()
        let service = AppProtectionService(dependencies: container)
        let presenter = AppProtectionPresenter(coordinator: coordinator)
        let interactor = AppProtectionInteractor(service: service,
                                                 presenter: presenter,
                                                 shouldHideCloseButton: shouldHideCloseButton,
                                                 dependencies: container)
        let viewController = AppProtectionViewController(interactor: interactor)

        presenter.viewController = viewController
        coordinator.viewController = viewController

        return viewController
    }
}
