import Core

enum AppProtectionKey: String, KVKeyContract {
    case userDeviceProtectedValue

    var description: String {
        rawValue
    }
}
