import Foundation

enum VirtualCardRegistrationFactory {
    static func make(flow: VirtualCardRegistrationCoordinatorDelegate) -> VirtualCardRegistrationViewController {
        let container = DependencyContainer()
        let service: VirtualCardRegistrationServicing = VirtualCardRegistrationService(dependencies: container)
        let coordinator: VirtualCardRegistrationCoordinating = VirtualCardRegistrationCoordinator(flow: flow)
        let presenter: VirtualCardRegistrationPresenting = VirtualCardRegistrationPresenter(coordinator: coordinator)
        let viewModel = VirtualCardRegistrationViewModel(service: service, presenter: presenter)
        let viewController = VirtualCardRegistrationViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
