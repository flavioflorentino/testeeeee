import Foundation

protocol VirtualCardRegistrationViewModelInputs: AnyObject {
    func didTapRegisterCard()
    func didTapknowMore()
    func didTapInfo()
}

final class VirtualCardRegistrationViewModel {
    private let service: VirtualCardRegistrationServicing
    private let presenter: VirtualCardRegistrationPresenting

    init(service: VirtualCardRegistrationServicing, presenter: VirtualCardRegistrationPresenting) {
        self.service = service
        self.presenter = presenter
    }
}

extension VirtualCardRegistrationViewModel: VirtualCardRegistrationViewModelInputs {
    func didTapRegisterCard() {
        presenter.didNextStep(action: .addDebitCard)
    }
    
    func didTapknowMore() {
        presenter.didNextStep(action: .helpGenerateVirtualCard)
    }
    
    func didTapInfo() {
        let link = service.faqVirtualCardRegistration
        guard let url = URL(string: link) else {
            let error = PicPayError(message: EmergencyAidLocalizable.errorLink.text)
            presenter.didReceiveAnError(error)
            return
        }
        
        presenter.didNextStep(action: .openHelpCenter(url: url))
    }
}
