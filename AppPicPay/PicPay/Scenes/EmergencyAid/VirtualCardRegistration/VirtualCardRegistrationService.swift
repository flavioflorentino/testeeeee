import FeatureFlag
import Foundation

protocol VirtualCardRegistrationServicing {
    var faqVirtualCardRegistration: String { get }
}

final class VirtualCardRegistrationService: VirtualCardRegistrationServicing {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies

    var faqVirtualCardRegistration: String {
        dependencies.featureManager.text(.faqVirtualCardRegistration)
    }
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}
