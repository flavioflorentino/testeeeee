import Core
import Foundation

protocol VirtualCardRegistrationPresenting: AnyObject {
    var viewController: VirtualCardRegistrationDisplay? { get set }
    func didReceiveAnError(_ error: PicPayError)
    func didNextStep(action: VirtualCardRegistrationAction)
}

final class VirtualCardRegistrationPresenter: VirtualCardRegistrationPresenting {
    private let coordinator: VirtualCardRegistrationCoordinating
    weak var viewController: VirtualCardRegistrationDisplay?

    init(coordinator: VirtualCardRegistrationCoordinating) {
        self.coordinator = coordinator
    }
    
    func didReceiveAnError(_ error: PicPayError) {
        viewController?.didReceiveAnError(error)
    }
    
    func didNextStep(action: VirtualCardRegistrationAction) {
        coordinator.perform(action: action)
    }
}
