import UIKit
enum VirtualCardRegistrationAction {
    case helpGenerateVirtualCard
    case openHelpCenter(url: URL)
    case addDebitCard
}

protocol VirtualCardRegistrationCoordinatorDelegate: AnyObject {
    func finishAddDebitCard(cardType: CardListType)
}

protocol VirtualCardRegistrationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: VirtualCardRegistrationAction)
}

final class VirtualCardRegistrationCoordinator: VirtualCardRegistrationCoordinating {
    private var additionalMethod: RechargeMethod?
    private var addCard: ((CardListType, RechargeMethod) -> Void)?
    private weak var delegate: VirtualCardRegistrationCoordinatorDelegate?
    
    weak var viewController: UIViewController?
    
    init(flow delegate: VirtualCardRegistrationCoordinatorDelegate) {
        self.delegate = delegate
    }
    
    func perform(action: VirtualCardRegistrationAction) {
        switch action {
        case .helpGenerateVirtualCard:
            let controller = VirtualCardHelperFactory.make()
            viewController?.navigationController?.pushViewController(controller, animated: true)
            
        case .openHelpCenter(let url):
            guard let viewController = viewController else {
                return
            }
            
            DeeplinkHelper.handleDeeplink(withUrl: url, from: viewController)
            
        case .addDebitCard:
            guard let controller = ViewsManager.paymentMethodsStoryboardViewController(withIdentifier: "AddNewCreditCardViewController") as? AddNewCreditCardViewController else {
                return
            }
            
            controller.delegate = self
            let viewModel = AddNewCreditCardViewModel(origin: .emergencyAid)
            controller.viewModel = viewModel
            viewController?.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

extension VirtualCardRegistrationCoordinator: AddNewCreditCardDelegate {
    func debitCardDidInsert() {
        delegate?.finishAddDebitCard(cardType: .debit)
    }
    
    func creditCardDidInsert() {
        delegate?.finishAddDebitCard(cardType: .credit)
    }
}
