import UI
import UIKit

extension VirtualCardRegistrationViewController.Layout {
    enum Font {
        static let title = UIFont.systemFont(ofSize: 20, weight: .bold)
    }
    
    enum Size {
        static let button: CGFloat = 48.0
        static let cardImage: CGFloat = 68.0
    }
}

final class VirtualCardRegistrationViewController: ViewController<VirtualCardRegistrationViewModelInputs, UIView> {
    fileprivate enum Layout { }

    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        
        return view
    }()
    
    private lazy var cardImageView: UIImageView = {
        let image = UIImageView()
        image.image = Assets.CreditCard.cardElo.image
        image.contentMode = .scaleAspectFill
        
        return image
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = EmergencyAidLocalizable.virtualCardRegistration.text
        label.numberOfLines = 8
        label.font = Layout.Font.title
        label.textAlignment = .center
        label.textColor = Palette.ppColorGrayscale600.color
        
        return label
    }()
    
    private lazy var registerCardButton: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = Layout.Size.button / 2
        button.configure(with: Button(title: EmergencyAidLocalizable.registerCardButton.text))
        button.addTarget(self, action: #selector(didTapRegisterCard), for: .touchUpInside)
        
        return button
    }()
    
    private lazy var knowMoreButton: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = Layout.Size.button / 2
        button.configure(with: Button(title: EmergencyAidLocalizable.knowMoreButton.text, type: .underlined))
        button.addTarget(self, action: #selector(didTapknowMore), for: .touchUpInside)
        
        return button
    }()
    
    override func configureViews() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Assets.Icons.iconInfo.image,
            style: .done,
            target: self,
            action: #selector(didTapInfo)
        )
        
        title = EmergencyAidLocalizable.registerTitle.text
        view.backgroundColor = Colors.backgroundPrimary.color
        setUpNavigationBarDefaultAppearance()
    }
    
    override func buildViewHierarchy() {
        containerView.addSubview(cardImageView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(registerCardButton)
        containerView.addSubview(knowMoreButton)
        
        view.addSubview(containerView)
    }
        
    override func setupConstraints() {
        containerView.layout {
            $0.top >= view.topAnchor
            $0.leading == view.leadingAnchor + Spacing.base03
            $0.trailing == view.trailingAnchor - Spacing.base03
            $0.bottom <= view.bottomAnchor
            $0.centerY == view.centerYAnchor
        }
        
        cardImageView.layout {
            $0.top == containerView.topAnchor
            $0.centerX == containerView.centerXAnchor
            $0.height == Layout.Size.cardImage
            $0.width == Layout.Size.cardImage
        }
        
        titleLabel.layout {
            $0.top == cardImageView.bottomAnchor + Spacing.base02
            $0.leading == containerView.leadingAnchor
            $0.trailing == containerView.trailingAnchor
        }
        
        registerCardButton.layout {
            $0.top == titleLabel.bottomAnchor + Spacing.base04
            $0.leading == containerView.leadingAnchor
            $0.trailing == containerView.trailingAnchor
            $0.height == Layout.Size.button
        }
        
        knowMoreButton.layout {
            $0.top == registerCardButton.bottomAnchor + Spacing.base01
            $0.leading == containerView.leadingAnchor
            $0.trailing == containerView.trailingAnchor
            $0.bottom == containerView.bottomAnchor
            $0.height == Layout.Size.button
        }
    }
}

@objc
private extension VirtualCardRegistrationViewController {
    func didTapRegisterCard() {
        viewModel.didTapRegisterCard()
    }
    
    func didTapknowMore() {
        viewModel.didTapknowMore()
    }
    
    func didTapInfo() {
        viewModel.didTapInfo()
    }
}

// MARK: View Model Outputs
extension VirtualCardRegistrationViewController: VirtualCardRegistrationDisplay {
    func didReceiveAnError(_ error: PicPayError) {
        AlertMessage.showCustomAlertWithError(error, controller: self)
    }
}
