import UIKit

protocol VirtualCardRegistrationDisplay: AnyObject {
    func didReceiveAnError(_ error: PicPayError)
}
