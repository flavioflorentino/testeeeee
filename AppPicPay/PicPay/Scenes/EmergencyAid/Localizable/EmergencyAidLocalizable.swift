import Foundation

enum EmergencyAidLocalizable: String, Localizable {
    case registerTitle
    case virtualCardRegistration
    case registerCardButton
    case knowMoreButton
    case errorLink
    case step01Caixa
    case step02Caixa
    case step03Caixa
    case step04Caixa
    case step05Caixa
    case step06Caixa
    case step07Caixa
    case backButton
    case helperCenter
    case notVirtualCard
    case tryFixCardNumber
    
    var key: String {
        self.rawValue
    }
    
    var file: LocalizableFile {
        .emergencyAid
    }
}
