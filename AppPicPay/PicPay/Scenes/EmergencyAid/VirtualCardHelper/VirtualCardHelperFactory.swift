import Foundation

enum VirtualCardHelperFactory {
    static func make() -> VirtualCardHelperViewController {
        let container = DependencyContainer()
        let coordinator: VirtualCardHelperCoordinating = VirtualCardHelperCoordinator()
        let presenter: VirtualCardHelperPresenting = VirtualCardHelperPresenter(coordinator: coordinator)
        let viewModel = VirtualCardHelperViewModel(presenter: presenter, dependencies: container)
        let viewController = VirtualCardHelperViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
