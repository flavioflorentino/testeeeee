import FeatureFlag
import Foundation

protocol VirtualCardHelperViewModelInputs: AnyObject {
    func didTapBack()
    func didTapHelper()
    func updateView()
}

final class VirtualCardHelperViewModel {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    private let presenter: VirtualCardHelperPresenting
    
    init(presenter: VirtualCardHelperPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

extension VirtualCardHelperViewModel: VirtualCardHelperViewModelInputs {
    func didTapBack() {
        presenter.didNextStep(action: .back)
    }
    
    func didTapHelper() {
        let link = dependencies.featureManager.text(.faqVirtualCardHelper)
        guard let url = URL(string: link) else {
            return
        }
        presenter.didNextStep(action: .openHelpCenter(url: url))
    }
    
    func updateView() {
        presenter.updateView()
    }
}
