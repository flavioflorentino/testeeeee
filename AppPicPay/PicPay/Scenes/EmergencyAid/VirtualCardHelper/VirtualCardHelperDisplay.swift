import UIKit

protocol VirtualCardHelperDisplay: AnyObject {
    func addRow(attributedText: NSAttributedString)
}
