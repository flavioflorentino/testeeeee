import Foundation
import UI
import UIKit

extension VirtualCardHelperViewController.Layout {
    enum Size {
        static let button: CGFloat = 48.0
    }
    enum Font {
        static let normal = UIFont.systemFont(ofSize: 16, weight: .regular)
    }
}

final class VirtualCardHelperViewController: ViewController<VirtualCardHelperViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        
        return view
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceVertical = true
        scrollView.backgroundColor = .clear
        
        return scrollView
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = Spacing.base02
        
        return stackView
    }()
    
    private lazy var backButton: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = Layout.Size.button / 2
        button.configure(with: Button(title: EmergencyAidLocalizable.backButton.text))
        button.addTarget(self, action: #selector(didTapBackButton), for: .touchUpInside)
        
        return button
    }()
    
    private lazy var helpCenterButton: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = Layout.Size.button / 2
        button.configure(with: Button(title: EmergencyAidLocalizable.helperCenter.text, type: .underlined))
        button.addTarget(self, action: #selector(didTapHelperCenter), for: .touchUpInside)
        
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.updateView()
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override func buildViewHierarchy() {
        containerView.addSubview(stackView)
        containerView.addSubview(backButton)
        containerView.addSubview(helpCenterButton)
        scrollView.addSubview(containerView)
        view.addSubview(scrollView)
    }
    
    override func setupConstraints() {
        scrollView.layout {
            $0.top == view.topAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
            $0.bottom == view.bottomAnchor
        }
        
        containerView.layout {
            $0.top == scrollView.topAnchor
            $0.leading == scrollView.leadingAnchor
            $0.trailing == scrollView.trailingAnchor
            $0.bottom == scrollView.bottomAnchor
            $0.width == view.widthAnchor
        }
        
        stackView.layout {
            $0.top == containerView.topAnchor + Spacing.base03
            $0.leading == containerView.leadingAnchor + Spacing.base03
            $0.trailing == containerView.trailingAnchor - Spacing.base03
        }
        
        backButton.layout {
            $0.top == stackView.bottomAnchor + Spacing.base05
            $0.leading == containerView.leadingAnchor + Spacing.base03
            $0.trailing == containerView.trailingAnchor - Spacing.base03
            $0.height == Layout.Size.button
        }
        
        helpCenterButton.layout {
            $0.top == backButton.bottomAnchor + Spacing.base02
            $0.leading == containerView.leadingAnchor + Spacing.base03
            $0.trailing == containerView.trailingAnchor - Spacing.base03
            $0.bottom == containerView.bottomAnchor - Spacing.base02
            $0.height == Layout.Size.button
        }
    }
}

// MARK: View Model Outputs
extension VirtualCardHelperViewController: VirtualCardHelperDisplay {
    func addRow(attributedText: NSAttributedString) {
        let label = UILabel()
        label.font = Layout.Font.normal
        label.textColor = Colors.grayscale400.color
        label.numberOfLines = 0
        label.attributedText = attributedText
        stackView.addArrangedSubview(label)
    }
}

@objc
private extension VirtualCardHelperViewController {
    func didTapBackButton() {
        viewModel.didTapBack()
    }
    
    func didTapHelperCenter() {
        viewModel.didTapHelper()
    }
}
