import UIKit

enum VirtualCardHelperAction {
    case back
    case openHelpCenter(url: URL)
}

protocol VirtualCardHelperCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: VirtualCardHelperAction)
}

final class VirtualCardHelperCoordinator: VirtualCardHelperCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: VirtualCardHelperAction) {
        switch action {
        case .back:
            viewController?.navigationController?.popViewController(animated: true)
            
        case .openHelpCenter(let url):
            guard let viewController = viewController else {
              return
            }
            DeeplinkHelper.handleDeeplink(withUrl: url, from: viewController)
        }
    }
}
