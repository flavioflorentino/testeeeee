import Core
import Foundation

protocol VirtualCardHelperPresenting: AnyObject {
    var viewController: VirtualCardHelperDisplay? { get set }
    func updateView()
    func didNextStep(action: VirtualCardHelperAction)
}

final class VirtualCardHelperPresenter: VirtualCardHelperPresenting {
    private let coordinator: VirtualCardHelperCoordinating
    
    private var boldAttributed: [NSAttributedString.Key: Any] = {
      let font = UIFont.systemFont(ofSize: 16.0, weight: .semibold)
      return [.font: font]
    }()
    
    private var steps: [String] {
        [
            EmergencyAidLocalizable.step01Caixa.text,
            EmergencyAidLocalizable.step02Caixa.text,
            EmergencyAidLocalizable.step03Caixa.text,
            EmergencyAidLocalizable.step04Caixa.text,
            EmergencyAidLocalizable.step05Caixa.text,
            EmergencyAidLocalizable.step06Caixa.text,
            EmergencyAidLocalizable.step07Caixa.text
        ]
    }
    
    weak var viewController: VirtualCardHelperDisplay?

    init(coordinator: VirtualCardHelperCoordinating) {
        self.coordinator = coordinator
    }
    
    func updateView() {
        for step in steps {
            let attributedString = step.attributedString(separated: "[b]", attrs: boldAttributed)
            viewController?.addRow(attributedText: attributedString)
        }
    }
    
    func didNextStep(action: VirtualCardHelperAction) {
        coordinator.perform(action: action)
    }
}
