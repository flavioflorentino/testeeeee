import Foundation

protocol VirtualCardHelperServicing {
}

final class VirtualCardHelperService: VirtualCardHelperServicing {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}
