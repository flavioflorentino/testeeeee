import UIKit

enum EmergencyAidCardIssueAction: Equatable {
    case openHelpCenter(url: URL)
    case close
}

protocol EmergencyAidCardIssueCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: EmergencyAidCardIssueAction)
}

final class EmergencyAidCardIssueCoordinator {
    weak var viewController: UIViewController?
}

extension EmergencyAidCardIssueCoordinator: EmergencyAidCardIssueCoordinating {
    func perform(action: EmergencyAidCardIssueAction) {
        switch action {
        case .openHelpCenter(let url):
            DeeplinkHelper.handleDeeplink(withUrl: url, from: viewController)
        case .close:
            viewController?.dismiss(animated: true, completion: nil)
        }
    }
}
