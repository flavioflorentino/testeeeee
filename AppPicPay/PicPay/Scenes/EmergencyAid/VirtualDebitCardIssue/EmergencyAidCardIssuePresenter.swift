import Foundation

protocol EmergencyAidCardIssuePresenting: AnyObject {
    var viewController: EmergencyAidCardIssueDisplay? { get set }
    func didNextStep(action: EmergencyAidCardIssueAction)
}

final class EmergencyAidCardIssuePresenter {
    private let coordinator: EmergencyAidCardIssueCoordinating
    weak var viewController: EmergencyAidCardIssueDisplay?

    init(coordinator: EmergencyAidCardIssueCoordinating) {
        self.coordinator = coordinator
    }
}

extension EmergencyAidCardIssuePresenter: EmergencyAidCardIssuePresenting {
    func didNextStep(action: EmergencyAidCardIssueAction) {
        coordinator.perform(action: action)
    }
}
