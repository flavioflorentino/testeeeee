import Foundation

enum EmergencyAidCardIssueFactory {
    static func make() -> EmergencyAidCardIssueViewController {
        let coordinator: EmergencyAidCardIssueCoordinating = EmergencyAidCardIssueCoordinator()
        let presenter: EmergencyAidCardIssuePresenting = EmergencyAidCardIssuePresenter(coordinator: coordinator)
        let viewModel = EmergencyAidCardIssueViewModel(presenter: presenter)
        let viewController = EmergencyAidCardIssueViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
