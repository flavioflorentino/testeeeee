import AssetsKit
import Foundation
import UI
import UIKit

protocol EmergencyAidCardIssueDisplay: AnyObject { }

private extension EmergencyAidCardIssueViewController.Layout {
    enum Size {
        static let image: CGFloat = 120.0
    }

    enum Font {
        static let primaryButton = UIFont.systemFont(ofSize: 16, weight: .semibold)
    }
}

final class EmergencyAidCardIssueViewController: ViewController<EmergencyAidCardIssueViewModelInputs, UIView> {
    fileprivate enum Layout { }
    private lazy var overlayView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.6

        return view
    }()

    private lazy var contentView: UIView = {
        let view = UIView()
        view.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
        view.layer.cornerRadius = CornerRadius.strong

        return view
    }()

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Resources.Illustrations.iluEloVirtualcardError.image
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base01
        stackView.alignment = .center
        stackView.axis = .vertical
        return stackView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = EmergencyAidLocalizable.notVirtualCard.text
        label.labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textAlignment, .center)
            .with(\.textColor, .grayscale600())

        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = EmergencyAidLocalizable.tryFixCardNumber.text
        label.labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, .grayscale500())

        return label
    }()

    private lazy var tryAgainButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.titleLabel?.font = Layout.Font.primaryButton
        button.setTitleColor(Colors.grayscale050.color, for: .normal)
        button.setTitle(DefaultLocalizable.tryAgain.text, for: .normal)
        button.addTarget(self, action: #selector(didTapTryAgain), for: .touchUpInside)

        return button
    }()

    private lazy var helpCenterButton: UIButton = {
        let button = UIButton()
        button.setTitle(EmergencyAidLocalizable.helperCenter.text, for: .normal)
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(didTapHelpCenter), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func buildViewHierarchy() {
        view.addSubview(overlayView)
        view.addSubview(contentView)
        contentView.addSubview(imageView)
        contentView.addSubview(stackView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(descriptionLabel)
        contentView.addSubview(tryAgainButton)
        contentView.addSubview(helpCenterButton)
    }
    
    override func setupConstraints() {
        overlayView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        contentView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base07)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        imageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base05)
            $0.size.equalTo(Layout.Size.image)
            $0.centerX.equalToSuperview()
        }

        stackView.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        tryAgainButton.snp.makeConstraints {
            $0.top.equalTo(stackView.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }

        helpCenterButton.snp.makeConstraints {
            $0.top.equalTo(tryAgainButton.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }

    override func configureViews() {
        view.backgroundColor = .clear
    }
    
    @objc
    private func didTapTryAgain() {
        viewModel.tryAgain()
    }

    @objc
    private func didTapHelpCenter() {
        viewModel.openHelpCenter()
    }
}

extension EmergencyAidCardIssueViewController: EmergencyAidCardIssueDisplay { }
