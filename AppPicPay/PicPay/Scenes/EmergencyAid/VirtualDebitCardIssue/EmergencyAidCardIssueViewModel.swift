import Foundation

protocol EmergencyAidCardIssueViewModelInputs: AnyObject {
    func openHelpCenter()
    func tryAgain()
}

final class EmergencyAidCardIssueViewModel {
    private let presenter: EmergencyAidCardIssuePresenting
    private let helpCenterURL = "picpay://picpay/helpcenter?query=preciso-de-ajuda-debito-caixa"

    init(presenter: EmergencyAidCardIssuePresenting) {
        self.presenter = presenter
    }
}

extension EmergencyAidCardIssueViewModel: EmergencyAidCardIssueViewModelInputs {
    func openHelpCenter() {
        guard let url = URL(string: helpCenterURL) else {
            return
        }
        presenter.didNextStep(action: .openHelpCenter(url: url))
    }

    func tryAgain() {
        presenter.didNextStep(action: .close)
    }
}
