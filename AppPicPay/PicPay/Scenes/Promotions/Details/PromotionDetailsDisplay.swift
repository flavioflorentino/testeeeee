import Foundation
import UIKit

struct PromotionDetailsDisplayData: Equatable {
    let title: String
    let description: String
    let address: String?
    let items: [Item]
    let imageUrl: URL?
    let phone: String?
}

extension PromotionDetailsDisplayData {
    enum Item: Equatable {
        case info(PromotionSeller.Item)
        case map(title: String, map: PromotionSeller.Map)
    }
}

protocol PromotionDetailsDisplay: AnyObject {
    typealias ErrorData = (
        image: UIImage?,
        content: (title: String, description: String),
        button: (image: UIImage?, title: String)
    )

    func show(details data: PromotionDetailsDisplayData)
    func show(error data: ErrorData)
    func show(share activity: UIActivityViewController)

    func updateState(_ state: FavoriteState)
    func whatsAppButton(isHidden: Bool)
}
