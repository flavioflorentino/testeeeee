import AnalyticsModule
import CoreImage
import UI
import UIKit

// MARK: - Layout
private extension PromotionDetailsViewController.Layout {
    enum Cover {
        static let topInset: CGFloat = 40.0
        static let alpha: CGFloat = 0.6
    }

    enum Icon {
        static let size = CGSize(width: 80.0, height: 80.0)
        static let cornerRadius = size.height / 2.0
    }

    enum Margin {
        static let largeTitleLeading: CGFloat = 20.0
    }
    
    enum Table {
        static let estimatedRowHeight: CGFloat = 110.0
    }
}

final class PromotionDetailsViewController: ViewController<PromotionDetailsInteracting, UIView> {
    fileprivate enum Layout { }
    private typealias Localizable = Strings.Promotions

    // MARK: - Properties
    private var favoriteState: FavoriteState = .unavailable {
        didSet {
            updateStarButton()
        }
    }

    private var items: [PromotionDetailsDisplayData.Item] = [] {
        didSet {
            tableView.reloadData()
        }
    }

    private lazy var starButton: UIButton = {
        let button = UIButton()
        button.isHidden = true
        button.addTarget(self, action: #selector(didTapAtStarButton), for: .touchUpInside)
        button.setImage(Assets.Favorite.starOutline.image.withRenderingMode(.alwaysTemplate), for: .normal)
        button.setImage(Assets.Favorite.starFill.image.withRenderingMode(.alwaysTemplate), for: .selected)
        button.setImage(Assets.Favorite.starFill.image.withRenderingMode(.alwaysTemplate), for: .highlighted)
        button.tintColor = Palette.white.color

        return button
    }()

    private lazy var coverImageView: PromotionDetailsCoverImageView = {
        let imageView = PromotionDetailsCoverImageView()
        imageView.contentMode = .center
        imageView.clipsToBounds = true

        return imageView
    }()

    private lazy var coverOverlayView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = Layout.Cover.alpha

        return view
    }()

    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.border = .light(color: .white())
        imageView.layer.cornerRadius = Layout.Icon.cornerRadius
        imageView.layer.masksToBounds = true

        return imageView
    }()

    private lazy var navigationTitleView = PromotionDetailsNavigationTitleView()

    private lazy var tableHeaderView: PromotionDetailsTableHeaderView = {
        let view = PromotionDetailsTableHeaderView()
        view.didTapAtCallAction = { [weak self] in
            self?.interactor.callPhone()
        }
        
        view.didTapAtShareAction = { [weak self] in
            self?.interactor.shareEstablishment()
        }

        return view
    }()

    private lazy var payButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle(Localizable.sellerDetailsPayButtonTitle, for: .normal)
        button.buttonStyle(PrimaryButtonStyle())
        button.addTarget(self, action: #selector(didTapAtPayButton), for: .touchUpInside)

        return button
    }()
    
    private lazy var whatsAppButton: UIButton = {
        let button = UIButton()
        button.setTitle(Localizable.sellerWhatsAppButtonTitle, for: .normal)
        button.buttonStyle(SecondaryButtonStyle(size: .default, icon: (name: .whatsapp, alignment: .left)))
        button.isHidden = true
        button.addTarget(self, action: #selector(didTapAtWhatsAppButton), for: .touchUpInside)
        return button
    }()

    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.estimatedRowHeight = Layout.Table.estimatedRowHeight
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = .leastNonzeroMagnitude
        tableView.sectionFooterHeight = .leastNonzeroMagnitude
        tableView.separatorColor = Colors.grayscale200.color
        tableView.backgroundColor = Palette.ppColorGrayscale000.color
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.register(PromotionDetailsCell.self, forCellReuseIdentifier: String(describing: PromotionDetailsCell.self))
        tableView.register(
            PromotionDisabledDetailsCell.self,
            forCellReuseIdentifier: String(describing: PromotionDisabledDetailsCell.self)
        )
        tableView.register(
            PromotionDetailsMapCell.self,
            forCellReuseIdentifier: String(describing: PromotionDetailsMapCell.self)
        )
        tableView.dataSource = self
        tableView.delegate = self

        return tableView
    }()

    private var coverAnimator: PromotionDetailsCoverAnimator?

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        view.preservesSuperviewLayoutMargins = true
        updateNavigationBarAppearance()
        loadDetails()

        tableView.contentInset.top = Layout.Cover.topInset
        tableView.scrollIndicatorInsets.top = tableView.contentInset.top

        coverAnimator = PromotionDetailsCoverAnimator(
            viewProvider: self,
            topInset: Layout.Cover.topInset,
            avatarHeight: Layout.Icon.size.height
        )

        coverAnimator?.navigationBarFrameDidChange(navigationController?.navigationBar.frame ?? .zero)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.tintColor = Palette.white.color
        navigationController?.navigationBar.clipsToBounds = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale000.color
        navigationController?.navigationBar.tintColor = Palette.ppColorBranding300.color
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        coverAnimator?.navigationBarFrameDidChange(navigationController?.navigationBar.frame ?? .zero)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        setupTableHeaderAndFooterView()
    }

    // MARK: - Setup
    override func buildViewHierarchy() {
        view.addSubviews(tableView, coverImageView, iconImageView, payButton, whatsAppButton)
        coverImageView.addSubview(coverOverlayView)
    }

    override func setupConstraints() {
        coverImageView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(view.compatibleSafeArea.top).offset(Layout.Cover.topInset)
        }

        coverOverlayView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        iconImageView.snp.makeConstraints {
            $0.centerX.equalTo(view.snp.centerX)
            $0.centerY.equalTo(coverImageView.snp.bottom)
            $0.size.equalTo(Layout.Icon.size)
        }

        tableView.snp.makeConstraints {
            $0.trailing.leading.equalToSuperview()
            $0.top.equalTo(view.compatibleSafeArea.top)
        }

        payButton.snp.makeConstraints {
            $0.top.equalTo(tableView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Layout.Margin.largeTitleLeading)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base02)
        }
    }

    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color

        navigationItem.backBarButtonItem = UIBarButtonItem(
            title: DefaultLocalizable.back.text,
            style: .plain,
            target: nil,
            action: nil
        )
    }
}

// MARK: - PromotionDetailsDisplay
extension PromotionDetailsViewController: PromotionDetailsDisplay {
    func show(details data: PromotionDetailsDisplayData) {
        coverImageView.setImage(url: data.imageUrl, placeholder: Assets.Icons.icoPromotionsPlaceholder.image)
        iconImageView.setImage(url: data.imageUrl, placeholder: Assets.Icons.icoPromotionsPlaceholder.image)

        tableHeaderView.setup(data)

        navigationTitleView.setTitle(data.title)
        navigationTitleView.setAddress(data.address)
        
        tableView.tableHeaderView = tableHeaderView
        payButton.isHidden = false

        items = data.items

        setupTableHeaderAndFooterView()

        endState()
    }

    func show(error data: ErrorData) {
        endState(
            model: StatefulErrorViewModel(
                image: data.image,
                content: (title: data.content.title, description: data.content.description),
                button: (image: data.button.image, title: data.button.title)
            )
        )
    }
    
    func show(share activity: UIActivityViewController) {
        present(activity, animated: true)
    }

    func updateState(_ state: FavoriteState) {
        favoriteState = state
    }
    
    func whatsAppButton(isHidden: Bool) {
        whatsAppButton.isHidden = isHidden
        
        if !isHidden {
            updateConstraintsToPresentWhatsAppButton()
        }
    }
}

// MARK: - StatefulTransitionViewing
extension PromotionDetailsViewController: StatefulTransitionViewing {
    func didTryAgain() {
        loadDetails()
    }

    func statefulViewForLoading() -> StatefulViewing {
        PromotionDetailsSkeletonView()
    }
}

extension PromotionDetailsViewController: UITableViewDataSource {
    private typealias Cell = UITableViewCell & PromotionDetailsItemCell

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.row]
        let hideSeparatorForNonMapItem = items.last?.isMap == true && indexPath.row == items.count - 2

        let cell = item.tableView(tableView, cellForRowAt: indexPath)

        if hideSeparatorForNonMapItem || item.isMap {
            cell.separatorInset = .init(top: Spacing.none, left: tableView.bounds.width, bottom: Spacing.none, right: Spacing.none)
        } else {
            cell.separatorInset = .init(top: Spacing.none, left: Spacing.base02, bottom: Spacing.none, right: Spacing.base02)
        }

        return cell
    }
}

extension PromotionDetailsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        110.0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard case .map = items[indexPath.row] else {
            return
        }
        
        Analytics.shared.log(PromotionsEvent.promotionMapAccessed)
        interactor.openMap()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        coverAnimator?.scrollViewWillBeginDragging(scrollView)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        coverAnimator?.scrollViewDidScroll(scrollView)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        coverAnimator?.scrollViewDidEndDragging(scrollView, willDecelerate: decelerate)
    }
}

// MARK: - Private
private extension PromotionDetailsViewController {
    func updateNavigationBarAppearance() {
        navigationController?.navigationBar.tintColor = Palette.ppColorBranding300.color
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale000.color

        if #available(iOS 11, *) {
            extendedLayoutIncludesOpaqueBars = true
        }

        navigationItem.titleView = navigationTitleView
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: starButton)
    }

    func setupTableHeaderAndFooterView() {
        if let headerView = tableView.tableHeaderView {
            updateTableHeaderOrFooterHeight(headerView)
            tableView.tableHeaderView = headerView
        }

        if let footerView = tableView.tableFooterView {
            updateTableHeaderOrFooterHeight(footerView)
            tableView.tableFooterView = footerView
        }

        tableView.layoutIfNeeded()
    }

    func updateTableHeaderOrFooterHeight(_ headerOrFooter: UIView) {
        let targetSize = CGSize(width: headerOrFooter.bounds.width, height: UIView.layoutFittingCompressedSize.height)
        let size = headerOrFooter.systemLayoutSizeFitting(targetSize)

        guard headerOrFooter.frame.height != size.height else {
            return
        }

        headerOrFooter.frame.size.height = size.height
    }
    
    func updateConstraintsToPresentWhatsAppButton() {
        payButton.snp.remakeConstraints {
            $0.top.equalTo(tableView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview().inset(Layout.Margin.largeTitleLeading)
            $0.bottom.equalTo(whatsAppButton.snp.top).offset(-Spacing.base01)
        }
        
        whatsAppButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Layout.Margin.largeTitleLeading)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base02)
        }
    }

    @objc
    func didTapAtStarButton() {
        interactor.changeFavoriteState(favoriteState)
        starButton.isSelected.toggle()
    }

    func loadDetails() {
        payButton.isHidden = true
        tableView.tableHeaderView = nil

        beginState()
        interactor.loadDetails()
    }

    @objc
    func didTapAtPayButton() {
        interactor.performPayment()
    }
    
    @objc
    func didTapAtWhatsAppButton() {
        interactor.openWhatsAppConversation()
    }

    func updateStarButton() {
        switch favoriteState {
        case .favorited:
            starButton.isSelected = true
            starButton.isHidden = false
        case .unfavorited:
            starButton.isSelected = false
            starButton.isHidden = false
        case .unavailable:
            starButton.isHidden = true
        }
    }
}

extension PromotionDetailsViewController: CoverAnimationViewProvider {
    var descriptionTopInset: CGFloat {
        tableHeaderView.addressContainerView.frame.maxY
    }

    var coverElement: UIView {
        coverImageView
    }

    var avatarElement: UIView {
        iconImageView
    }

    var titleElement: UIView {
        tableHeaderView.titleLabel
    }

    var addressElement: UIView {
        tableHeaderView.addressContainerView
    }

    var navigationTitleElement: UIView {
        navigationTitleView
    }

    func bringToFront(_ element: UIView) {
        guard view.subviews.contains(element) else {
            return
        }

        view.bringSubviewToFront(element)
    }
}

private extension PromotionDetailsDisplayData.Item {
    typealias InfoCell = UITableViewCell & PromotionDetailsItemCell
    typealias MapCell = PromotionDetailsMapCell

    private var cellIdentifier: String {
        switch self {
        case let .info(item):
            guard item.enabled else {
                return String(describing: PromotionDisabledDetailsCell.self)
            }

            return String(describing: PromotionDetailsCell.self)
        case .map:
            return String(describing: PromotionDetailsMapCell.self)
        }
    }

    var isMap: Bool {
        guard case .map = self else {
            return false
        }

        return true
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self {
        case let .info(item):
            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? InfoCell else {
                return UITableViewCell()
            }

            cell.setup(with: item)

            return cell

        case let .map(title, map):
            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? MapCell else {
                return UITableViewCell()
            }

            cell.setup(with: title, latitude: map.latitude, longitude: map.longitude)

            return cell
        }
    }
}
