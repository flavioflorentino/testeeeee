import UIKit

protocol PromotionDetailsCoordinatorFactory {
    func makeDetailsCoordinator(
        seller: PromotionDetailsCoordinating.Seller,
        navigationController: UINavigationController
    ) -> PromotionDetailsCoordinating
}
