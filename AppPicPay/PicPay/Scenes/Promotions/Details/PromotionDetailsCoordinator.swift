import AnalyticsModule
import Core
import UI
import UIKit

protocol PromotionDetailsCoordinating: Coordinating {
    typealias Seller = (idType: PromotionDetailsIDType, name: String?)

    var didFinishFlow: (() -> Void)? { get set }

    func startWithoutShowing() -> UIViewController
}

final class PromotionDetailsCoordinator: NSObject {
    typealias Factory = PromotionDetailsFactory
    typealias Dependency = HasConsumerManager & HasAnalytics

    private typealias Localizable = Strings.Promotions

    // MARK: - Properties
    private let navigationController: UINavigationController
    private let factory: Factory
    private let dependency: Dependency
    private let seller: PromotionDetailsCoordinating.Seller

    private weak var originalNavigationControllerDelegate: UINavigationControllerDelegate?

    var childViewController: [UIViewController] = []
    var viewController: UIViewController?

    var didFinishFlow: (() -> Void)?

    // MARK: - Initialization
    init(
        navigationController: UINavigationController,
        factory: Factory,
        seller: PromotionDetailsCoordinating.Seller,
        dependency: Dependency = DependencyContainer()
    ) {
        self.navigationController = navigationController
        self.factory = factory
        self.dependency = dependency
        self.seller = seller

        super.init()

        originalNavigationControllerDelegate = navigationController.delegate
        navigationController.delegate = self
    }
}

// MARK: - PromotionDetailsCoordinating
extension PromotionDetailsCoordinator: PromotionDetailsCoordinating {
    func start() {
        showDetailsScreen(seller: seller)
    }

    func startWithoutShowing() -> UIViewController {
        createPromotionDetailsScreen(seller)
    }
}

// MARK: - Private
private extension PromotionDetailsCoordinator {
    enum MapOption {
        case appleMaps
        case googleMaps
        case waze
    }

    func createPromotionDetailsScreen(_ seller: PromotionDetailsCoordinating.Seller) -> PromotionDetailsViewController {
        factory.makeDetailsController(
            title: "",
            idType: seller.idType,
            callPhone: { [weak self] phone in
                self?.callPhone(phone)
            },
            openMap: { [weak self] mapData in
                self?.showOpenMapOptions { [weak self] in
                    self?.showMap($0, data: mapData)
                }
            },
            performPayment: { [weak self] seller in
                self?.runPaymentFlow(seller)
            },
            openWhatsApp: { [weak self] whatsAppSeller in
                self?.openWhatsApp(whatsAppSeller)
            }
        )
    }

    func showDetailsScreen(seller: PromotionDetailsCoordinating.Seller) {
        let controller = createPromotionDetailsScreen(seller)
        controller.hidesBottomBarWhenPushed = true

        navigationController.pushViewController(controller, animated: true)
    }

    func callPhone(_ number: String?) {
        guard
            let number = number,
            let telUrl = URL(string: "tel://\(number)"),
            UIApplication.shared.canOpenURL(telUrl)
            else {
                return
        }

        UIApplication.shared.open(telUrl, options: [:])
    }

    func showOpenMapOptions(_ completion: @escaping (MapOption) -> Void) {
        let options: [MapOption] = [.waze, .googleMaps, .appleMaps]

        let alertController = UIAlertController(
            title: Localizable.seeLocationOnMap,
            message: nil,
            preferredStyle: .actionSheet
        )

        options.forEach { option in
            alertController.addAction(UIAlertAction(title: option.openTitle, style: .default) { _ in
                completion(option)
            })
        }

        alertController.addAction(UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel) { _ in
            alertController.dismiss(animated: true)
        })

        navigationController.present(alertController, animated: true)
    }

    func showMap(_ option: MapOption, data: PromotionDetailsFactory.MapData) {
        guard let url = option.openUrl(title: data.title, latitude: data.latitude, longitude: data.longitude) else {
            return
        }

        UIApplication.shared.open(url)
    }

    func runPaymentFlow(_ seller: PromotionSeller) {
        guard let controller = ViewsManager.paymentStoryboardFirstViewController() as? PaymentViewController,
              let paymentInfo = seller.paymentInfo,
              let tracking = seller.tracking else {
            return
        }
        
        controller.store = PPStore(profileDictionary: ["seller_id": "\(paymentInfo.sellerId ?? 0)"])
        controller.touchOrigin = "promotions"
        navigationController.present(UINavigationController(rootViewController: controller), animated: true)

        dependency.analytics.log(
            PromotionsEvent.promotionPaymentItemAccessed(
                type: tracking.type,
                itemName: tracking.title,
                consumerId: dependency.consumerManager.consumer?.wsId ?? 0,
                sellerId: tracking.sellerId,
                section: tracking.section
            )
        )
    }
    
    func openWhatsApp(_ whatsAppSeller: WhatsAppSeller) {
        var allowed = CharacterSet.alphanumerics
        allowed.insert(charactersIn: "-._~")
        let text = whatsAppSeller.text.addingPercentEncoding(withAllowedCharacters: allowed) ?? ""
        
        guard let url = URL(string: "https://api.whatsapp.com/send?phone=\(whatsAppSeller.phone)&text=\(text)") else {
            return
        }
        
        UIApplication.shared.open(url)
    }
}

// MARK: - UINavigationControllerDelegate
extension PromotionDetailsCoordinator: UINavigationControllerDelegate {
    func navigationController(
        _ navigationController: UINavigationController,
        didShow viewController: UIViewController,
        animated: Bool
    ) {
        originalNavigationControllerDelegate?
            .navigationController?(navigationController, didShow: viewController, animated: animated)

        guard navigationController.viewControllers.contains(where: { $0 is PromotionDetailsViewController }) == false else {
            return
        }

        navigationController.delegate = originalNavigationControllerDelegate
        didFinishFlow?()
    }
}

private extension PromotionDetailsCoordinator.MapOption {
    var openTitle: String {
        switch self {
        case .appleMaps:
            return Strings.Promotions.openOnAppleMaps
        case .googleMaps:
            return Strings.Promotions.openOnGoogleMaps
        case .waze:
            return Strings.Promotions.openOnWaze
        }
    }

    func openUrl(title: String, latitude: Double, longitude: Double) -> URL? {
        switch self {
        case .waze:
            return URL(string: "https://www.waze.com/ul?ll=\(latitude),\(longitude)")
        case .googleMaps:
            return URL(string: "https://www.google.com/maps/search/?api=1&query=\(latitude),\(longitude)")
        case .appleMaps:
            guard let encodedTitle = title.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
                return nil
            }

            return URL(string: "https://maps.apple.com/?q=\(encodedTitle)&ll=\(latitude),\(longitude)")
        }
    }
}
