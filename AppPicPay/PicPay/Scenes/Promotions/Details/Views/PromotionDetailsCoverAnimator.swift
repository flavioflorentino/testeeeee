import Foundation
import QuartzCore
import UIKit

protocol CoverAnimationViewProvider: AnyObject {
    var descriptionTopInset: CGFloat { get }

    var coverElement: UIView { get }
    var avatarElement: UIView { get }
    var titleElement: UIView { get }
    var addressElement: UIView { get }
    var navigationTitleElement: UIView { get }

    func bringToFront(_ element: UIView)
}

final class PromotionDetailsCoverAnimator: NSObject {
    private weak var viewProvider: CoverAnimationViewProvider?

    private let topInset: CGFloat
    private let avatarHeight: CGFloat

    private var navigationBarMaxY: CGFloat = 0.0

    private var userIsScrolling: Bool = false
    private var lastContentOffset: CGPoint = .zero

    private var coverOffset: CGFloat = 0.0
    private var avatarOffset: CGFloat = 0.0
    private var titleOffset: CGFloat = 0.0
    private var navigationTitleOffset: CGFloat = 0.0

    init(
        viewProvider: CoverAnimationViewProvider,
        topInset: CGFloat,
        avatarHeight: CGFloat
    ) {
        self.topInset = topInset
        self.avatarHeight = avatarHeight

        self.viewProvider = viewProvider

        super.init()
    }

    func navigationBarFrameDidChange(_ newValue: CGRect) {
        navigationBarMaxY = newValue.maxY

        let transform = CATransform3DTranslate(CATransform3DIdentity, 0.0, navigationBarMaxY, 0.0)
        viewProvider?.navigationTitleElement.layer.transform = transform
    }
}

extension PromotionDetailsCoverAnimator {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        userIsScrolling = true
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard userIsScrolling else {
            return lastContentOffset = scrollView.contentOffset
        }
        
        contentOffsetDidChange(scrollView)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        userIsScrolling = false
    }
}

private extension PromotionDetailsCoverAnimator {
    var canAnimateCoverDuringScrollUp: Bool {
        coverOffset < topInset
    }

    var canAnimateCoverDuringScrollDown: Bool {
        coverOffset > 0 && avatarOffset == 0
    }

    var isAnimatingCover: Bool {
        coverOffset >= 0 && coverOffset < topInset
    }

    var canAnimateAvatarDuringScrollUp: Bool {
        coverOffset == topInset && avatarOffset < avatarHeight
    }

    var canAnimateAvatarDuringScrollDown: Bool {
        avatarOffset > 0 && titleOffset <= topInset
    }

    var canAnimateTitleDuringScrollUp: Bool {
        guard let viewProvider = viewProvider else {
            return false
        }

        return avatarOffset == avatarHeight && titleOffset <= viewProvider.descriptionTopInset
    }

    var canAnimateNavigationTitleViewDuringScrollUp: Bool {
        titleOffset > topInset && navigationTitleOffset < navigationBarMaxY
    }

    var canAnimateNavigationTitleViewDuringScrollDown: Bool {
        navigationTitleOffset > 0
    }

    var canAnimateTitlesDuringScrollDown: Bool {
        avatarOffset == avatarHeight && titleOffset > topInset
    }

    func contentOffsetDidChange(_ scrollView: UIScrollView) {
        let yOffset = scrollView.contentOffset.y
        let distance = yOffset - lastContentOffset.y

        if distance > 0 {
            scrollingUp(scrollView, distance: distance)
        } else {
            scrollingDown(scrollView, distance: distance)
        }

        lastContentOffset = scrollView.contentOffset
    }

    func scrollingUp(_ scrollView: UIScrollView, distance: CGFloat) {
        guard let viewProvider = viewProvider else {
            return
        }

        if canAnimateCoverDuringScrollUp {
            animateCover(scrollView, viewProvider: viewProvider, distance: distance)
        }

        if canAnimateAvatarDuringScrollUp {
            animateAvatarDuringScrollUp(scrollView, viewProvider: viewProvider, distance: distance)
        }

        animateTitlesDuringScrollUp(scrollView, viewProvider: viewProvider, distance: distance)
    }

    func scrollingDown(_ scrollView: UIScrollView, distance: CGFloat) {
        guard let viewProvider = viewProvider else {
            return
        }

        if canAnimateTitlesDuringScrollDown {
            animateTitlesDuringScrollDown(scrollView, viewProvider: viewProvider, distance: distance)
        }

        if canAnimateAvatarDuringScrollDown {
            animateAvatarDuringScrollDown(scrollView, viewProvider: viewProvider, distance: distance)
        }

        if canAnimateCoverDuringScrollDown {
            animateCover(scrollView, viewProvider: viewProvider, distance: distance)
        }
    }

    func updateScrollView(_ scrollView: UIScrollView, contentOffset: CGPoint) {
        userIsScrolling = false
        scrollView.contentOffset = contentOffset
        userIsScrolling = true
    }

    func updateScrollView(_ scrollView: UIScrollView, contentInset: UIEdgeInsets) {
        userIsScrolling = false
        scrollView.contentInset = contentInset
        scrollView.scrollIndicatorInsets = contentInset
        userIsScrolling = true
    }

    func animateCover(_ scrollView: UIScrollView, viewProvider: CoverAnimationViewProvider, distance: CGFloat) {
        coverOffset = max(0.0, min(topInset, coverOffset + distance))

        viewProvider.bringToFront(viewProvider.avatarElement)
        viewProvider.coverElement.layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0.0, -coverOffset, 0.0)

        if isAnimatingCover {
            var contentOffset = scrollView.contentOffset
            contentOffset.y = -scrollView.contentInset.top

            updateScrollView(scrollView, contentOffset: contentOffset)
        }
    }

    func animateAvatarDuringScrollUp(_ scrollView: UIScrollView, viewProvider: CoverAnimationViewProvider, distance: CGFloat) {
        animateAvatar(scrollView, viewProvider: viewProvider, distance: distance)

        var contentInset = scrollView.contentInset
        contentInset.top = max(-viewProvider.descriptionTopInset, contentInset.top - distance)

        updateScrollView(scrollView, contentInset: contentInset)

        titleOffset = -contentInset.top
    }

    func animateAvatarDuringScrollDown(_ scrollView: UIScrollView, viewProvider: CoverAnimationViewProvider, distance: CGFloat) {
        animateAvatar(scrollView, viewProvider: viewProvider, distance: distance)

        var contentInset = scrollView.contentInset
        contentInset.top = min(topInset, contentInset.top - distance)

        updateScrollView(scrollView, contentInset: contentInset)

        titleOffset = -contentInset.top
    }

    func animateAvatar(_ scrollView: UIScrollView, viewProvider: CoverAnimationViewProvider, distance: CGFloat) {
        avatarOffset = max(0.0, min(avatarHeight, avatarOffset + distance))

        viewProvider.bringToFront(viewProvider.coverElement)
        viewProvider.avatarElement.layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0.0, -avatarOffset, 0.0)
    }

    func animateTitlesDuringScrollUp(_ scrollView: UIScrollView, viewProvider: CoverAnimationViewProvider, distance: CGFloat) {
        if canAnimateTitleDuringScrollUp {
            titleOffset = min(viewProvider.descriptionTopInset, titleOffset + distance)

            var contentInset = scrollView.contentInset
            contentInset.top = -titleOffset

            updateScrollView(scrollView, contentInset: contentInset)

            let alpha = 1.0 - ((titleOffset - topInset) / navigationBarMaxY)
            animateTitleAndAddressAlpha(alpha)
        }

        if canAnimateNavigationTitleViewDuringScrollUp {
            let previousNavigationTitleViewOffset = navigationTitleOffset
            navigationTitleOffset = min(navigationBarMaxY, navigationTitleOffset + distance)

            animateNavigationTitleView(
                viewProvider: viewProvider,
                distance: navigationTitleOffset - previousNavigationTitleViewOffset
            )
        }
    }

    func animateTitlesDuringScrollDown(_ scrollView: UIScrollView, viewProvider: CoverAnimationViewProvider, distance: CGFloat) {
        let previousTitleOffset = titleOffset

        titleOffset = max(topInset, min(viewProvider.descriptionTopInset, titleOffset + distance))

        var contentInset = scrollView.contentInset
        contentInset.top = -titleOffset

        updateScrollView(scrollView, contentInset: contentInset)

        let alpha = 1 - ((titleOffset - topInset) / navigationBarMaxY)
        animateTitleAndAddressAlpha(alpha)

        if canAnimateNavigationTitleViewDuringScrollDown {
            let previousNavigationTitleViewOffset = navigationTitleOffset
            navigationTitleOffset = max(0.0, navigationTitleOffset + (titleOffset - previousTitleOffset))

            animateNavigationTitleView(
                viewProvider: viewProvider,
                distance: navigationTitleOffset - previousNavigationTitleViewOffset
            )
        }
    }

    func animateTitleAndAddressAlpha(_ value: CGFloat) {
        viewProvider?.titleElement.alpha = value
        viewProvider?.addressElement.alpha = value
    }

    func animateNavigationTitleView(viewProvider: CoverAnimationViewProvider, distance: CGFloat) {
        let currentTransform = viewProvider.navigationTitleElement.layer.transform
        let transform = CATransform3DTranslate(currentTransform, 0.0, -distance, 0.0)

        viewProvider.navigationTitleElement.layer.transform = transform
    }
}
