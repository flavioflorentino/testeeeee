import SkeletonView
import UI
import UIKit

// MARK: - Layout
private extension PromotionDetailsSkeletonView.Layout {
    enum TableHeader { }
    enum Cell { }

    enum Margin {
        static let largeTitleLeading: CGFloat = 20.0
    }

    enum Size {
        static let line: CGFloat = 12.0
    }

    enum CornerRadius {
        static let line: CGFloat = Size.line / 2.0
    }
}

private extension PromotionDetailsSkeletonView.Layout.TableHeader {
    enum Margin {
        static let firstLineTrailing: CGFloat = 32.0
        static let secondLineTrailing: CGFloat = 57.0
        static let thirdLineTrailing: CGFloat = 224.0
    }
}

private extension PromotionDetailsSkeletonView.Layout.Cell {
    enum Size {
        static let container: CGFloat = 100.0
        static let icon: CGFloat = 48.0
    }

    enum Margin {
        static let secondLineTrailing: CGFloat = 126.0
    }

    enum CornerRadius {
        static let container: CGFloat = 4.0
        static let icon: CGFloat = Size.icon / 2.0
    }
}

final class PromotionDetailsSkeletonView: UIView, StatefulViewing {
    fileprivate enum Layout { }

    // MARK: - Properties
    private lazy var tableHeader = TableHeader()

    private lazy var cellsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [Cell(), Cell(), Cell()])
        stackView.axis = .vertical
        stackView.spacing = Spacing.base01

        return stackView
    }()

    var viewModel: StatefulViewModeling?
    weak var delegate: StatefulDelegate?

    // MARK: - Initialization
    convenience init() {
        self.init(frame: .zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration
extension PromotionDetailsSkeletonView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(tableHeader)
        addSubview(cellsStackView)
    }

    func setupConstraints() {
        tableHeader.layout {
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.top == compatibleSafeAreaLayoutGuide.topAnchor + Spacing.base01
        }

        cellsStackView.layout {
            $0.top == tableHeader.bottomAnchor + Spacing.base04
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.bottom <= bottomAnchor - Spacing.base01
        }
    }

    func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
    }
}

// MARK: - Nested types
private extension PromotionDetailsSkeletonView {
    final class TableHeader: UIView, ViewConfiguration {
        private lazy var firstLineView: UIView = {
            let view = UIView()
            view.layer.cornerRadius = Layout.CornerRadius.line
            view.clipsToBounds = true
            view.isSkeletonable = true

            return view
        }()

        private lazy var secondLineView: UIView = {
            let view = UIView()
            view.layer.cornerRadius = Layout.CornerRadius.line
            view.clipsToBounds = true
            view.isSkeletonable = true

            return view
        }()

        private lazy var thirdLineView: UIView = {
            let view = UIView()
            view.layer.cornerRadius = Layout.CornerRadius.line
            view.clipsToBounds = true
            view.isSkeletonable = true

            return view
        }()

        // MARK: - Initialization
        convenience init() {
            self.init(frame: .zero)
        }

        override init(frame: CGRect) {
            super.init(frame: frame)

            buildLayout()
        }

        @available(*, unavailable)
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        func buildViewHierarchy() {
            addSubview(firstLineView)
            addSubview(secondLineView)
            addSubview(thirdLineView)
        }

        func setupConstraints() {
            firstLineView.layout {
                $0.top == topAnchor
                $0.trailing == trailingAnchor - Layout.TableHeader.Margin.firstLineTrailing
                $0.leading == leadingAnchor + Layout.Margin.largeTitleLeading
                $0.height == Layout.Size.line
            }

            secondLineView.layout {
                $0.top == firstLineView.bottomAnchor + Spacing.base01
                $0.trailing == trailingAnchor - Layout.TableHeader.Margin.secondLineTrailing
                $0.leading == leadingAnchor + Layout.Margin.largeTitleLeading
                $0.height == Layout.Size.line
            }

            thirdLineView.layout {
                $0.top == secondLineView.bottomAnchor + Spacing.base01
                $0.trailing == trailingAnchor - Layout.TableHeader.Margin.thirdLineTrailing
                $0.bottom == bottomAnchor
                $0.leading == leadingAnchor + Layout.Margin.largeTitleLeading
                $0.height == Layout.Size.line
            }
        }

        func configureViews() {
            backgroundColor = Palette.ppColorGrayscale000.color
        }

        override func layoutSubviews() {
            super.layoutSubviews()

            showAnimatedGradientSkeleton()
        }
    }
}

private extension PromotionDetailsSkeletonView {
    final class Cell: UIView, ViewConfiguration {
        private lazy var containerView: UIView = {
            let view = UIView()
            view.backgroundColor = Palette.ppColorGrayscale000.color

            return view
        }()

        private lazy var iconView: UIView = {
            let view = UIView()
            view.layer.cornerRadius = Layout.Cell.CornerRadius.icon
            view.clipsToBounds = true
            view.isSkeletonable = true

            return view
        }()

        private lazy var linesContainerView = UIView()

        private lazy var firstLineView: UIView = {
            let view = UIView()
            view.layer.cornerRadius = Layout.CornerRadius.line
            view.clipsToBounds = true
            view.isSkeletonable = true

            return view
        }()

        private lazy var secondLineView: UIView = {
            let view = UIView()
            view.layer.cornerRadius = Layout.CornerRadius.line
            view.clipsToBounds = true
            view.isSkeletonable = true

            return view
        }()

        private lazy var thirdLineView: UIView = {
            let view = UIView()
            view.layer.cornerRadius = Layout.CornerRadius.line
            view.clipsToBounds = true
            view.isSkeletonable = true

            return view
        }()

        // MARK: - Initialization
        convenience init() {
            self.init(frame: .zero)
        }

        override init(frame: CGRect) {
            super.init(frame: frame)

            buildLayout()
        }

        @available(*, unavailable)
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        func buildViewHierarchy() {
            addSubview(containerView)

            containerView.addSubview(iconView)
            containerView.addSubview(linesContainerView)

            linesContainerView.addSubview(firstLineView)
            linesContainerView.addSubview(secondLineView)
            linesContainerView.addSubview(thirdLineView)
        }

        func setupConstraints() {
            containerView.layout {
                $0.top == topAnchor
                $0.trailing == trailingAnchor
                $0.bottom == bottomAnchor
                $0.leading == leadingAnchor + Layout.Margin.largeTitleLeading
                $0.height == Layout.Cell.Size.container
            }

            iconView.layout {
                $0.width == Layout.Cell.Size.icon
                $0.height == Layout.Cell.Size.icon
                $0.top == containerView.topAnchor + Spacing.base03
                $0.leading == containerView.leadingAnchor
            }

            linesContainerView.layout {
                $0.centerY == containerView.centerYAnchor
                $0.top == containerView.topAnchor + Spacing.base03
                $0.bottom <= containerView.bottomAnchor - Spacing.base02
                $0.leading == iconView.trailingAnchor + Spacing.base02
                $0.trailing == containerView.trailingAnchor - Spacing.base06
            }

            firstLineView.layout {
                $0.top == linesContainerView.topAnchor
                $0.leading == linesContainerView.leadingAnchor
                $0.trailing == linesContainerView.trailingAnchor
                $0.height == Layout.Size.line
            }

            secondLineView.layout {
                $0.top == firstLineView.bottomAnchor + Spacing.base01
                $0.leading == linesContainerView.leadingAnchor
                $0.trailing == linesContainerView.trailingAnchor - Layout.Cell.Margin.secondLineTrailing
                $0.height == Layout.Size.line
            }

            thirdLineView.layout {
                $0.top == secondLineView.bottomAnchor + Spacing.base01
                $0.leading == linesContainerView.leadingAnchor
                $0.trailing == linesContainerView.trailingAnchor - Layout.Cell.Margin.secondLineTrailing
                $0.bottom <= linesContainerView.bottomAnchor
                $0.height == Layout.Size.line
            }
        }

        func configureViews() {
            backgroundColor = Palette.ppColorGrayscale000.color
        }

        override func layoutSubviews() {
            super.layoutSubviews()

            iconView.showAnimatedGradientSkeleton()
            firstLineView.showAnimatedGradientSkeleton()
            secondLineView.showAnimatedGradientSkeleton()
            thirdLineView.showAnimatedGradientSkeleton()
        }
    }
}
