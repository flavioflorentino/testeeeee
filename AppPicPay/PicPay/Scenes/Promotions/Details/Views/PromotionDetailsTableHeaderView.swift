import FeatureFlag
import UI
import UIKit

// MARK: - Layout
private extension PromotionDetailsTableHeaderView.Layout {
    enum Font {
        static let title: UIFont = .systemFont(ofSize: 28.0, weight: .bold)
        static let address: UIFont = .systemFont(ofSize: 12.0)
        static let description: UIFont = .systemFont(ofSize: 16.0)
        static let lineHeightMultiplier: CGFloat = 1.26
    }

    enum Margin {
        static let betweenPlacePinAndAddress: CGFloat = 5.36
        static let telephone: CGFloat = 11.0
    }

    enum Size {
        static let placePin = CGSize(width: 9.18, height: 11.08)
        static let telephoneContainerHeight: CGFloat = 32.0
        static let telephoneIcon = CGSize(width: 10.0, height: 14.0)
        static let shareIcon = CGSize(width: 44.0, height: 44.0)
    }
}

final class PromotionDetailsTableHeaderView: UIView {
    fileprivate enum Layout { }
    private typealias Localizable = Strings.Promotions

    // MARK: - Properties
    private (set) lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.title
        label.textColor = Palette.ppColorGrayscale600.color

        return label
    }()

    private (set) lazy var addressContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale000.color

        return view
    }()

    private lazy var placePinImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = Palette.ppColorGrayscale000.color
        imageView.image = Assets.Search.stablishment.image.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = Palette.ppColorGrayscale400.color

        return imageView
    }()

    private lazy var addressLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = Palette.ppColorGrayscale000.color
        label.font = Layout.Font.address
        label.textColor = Palette.ppColorGrayscale400.color

        return label
    }()

    private(set) lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base03

        return stackView
    }()

    private lazy var telephoneContainerView: UIView = {
        let view = UIView()
        view
            .viewStyle(RoundedViewStyle(cornerRadius: .light))
            .with(\.border, .light(color: .grayscale300()))
        view.isHidden = true

        view.layer.borderWidth = 0.5

        return view
    }()

    private lazy var telephoneIconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.Icons.icoSmartphone.image.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = Colors.grayscale700.color

        return imageView
    }()

    private lazy var telephoneLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .left)

        return label
    }()

    private lazy var callButton: UIButton = {
        let button = UIButton(type: .system)
        button.typography = .caption()
        button.textColor = (color: .branding400(), state: .normal)
        
        button.setTitle(Localizable.call, for: .normal)
        button.addTarget(self, action: #selector(didTapAtCallButton), for: .touchUpInside)
        button.backgroundColor = .clear

        return button
    }()
    
    private lazy var shareButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTapAtShareButton), for: .touchUpInside)
        button.setImage(Assets.Promotions.share.image, for: .normal)
        button.contentMode = .scaleToFill
        return button
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = Palette.ppColorGrayscale000.color
        label.font = Layout.Font.description
        label.numberOfLines = 0
        label.textColor = Palette.ppColorGrayscale500.color

        return label
    }()

    var didTapAtCallAction: (() -> Void)?
    var didTapAtShareAction: (() -> Void)?

    // MARK: - Initialization
    convenience init() {
        self.init(frame: .zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup(_ data: PromotionDetailsDisplayData) {
        titleLabel.text = data.title
        addressLabel.text = data.address

        setDescription(data.description)
        setPhone(data.phone)
    }
}

// MARK: - ViewConfiguration
extension PromotionDetailsTableHeaderView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(titleLabel, addressContainerView, stackView, shareButton)
        addressContainerView.addSubviews(placePinImageView, addressLabel)

        stackView.addArrangedSubviews(telephoneContainerView, descriptionLabel)
        telephoneContainerView.addSubviews(telephoneIconImageView, telephoneLabel, callButton)
    }

    func setupConstraints() {
        titleLabel.layout {
            $0.top == topAnchor + Spacing.base08
            $0.centerX == centerXAnchor
            $0.leading >= leadingAnchor + Spacing.base02
            $0.trailing <= trailingAnchor - Spacing.base02
        }

        shareButton.layout {
            $0.top == topAnchor + Spacing.base00
            $0.trailing == trailingAnchor - Spacing.base00
            $0.height == Layout.Size.shareIcon.height
            $0.width == Layout.Size.shareIcon.width
        }
        
        setupAddressContainerConstraints()

        stackView.layout {
            $0.top == addressContainerView.bottomAnchor + Spacing.base02
            $0.leading == leadingAnchor + Spacing.base02
            $0.trailing == trailingAnchor - Spacing.base02
            $0.bottom == bottomAnchor - Spacing.base01
        }

        setupTelephoneContainerConstraints()
    }

    func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
    }
}

// MARK: - Private
private extension PromotionDetailsTableHeaderView {
    func setDescription(_ description: String?) {
        guard let description = description else {
            return descriptionLabel.attributedText = nil
        }

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = Layout.Font.lineHeightMultiplier

        let attributedText = NSAttributedString(string: description, attributes: [.paragraphStyle: paragraphStyle])
        descriptionLabel.attributedText = attributedText
    }

    func setPhone(_ phone: String?) {
        guard
            FeatureManager.shared.isActive(.featurePromotionDetailsPhoneNumber),
            let phone = phone,
            phone.count >= 10 && phone.count <= 11
            else {
                return telephoneContainerView.isHidden = true
        }

        let mask = phone.count == 10 ? "(00) 0000 - 0000" : "(00) 00000 - 0000"
        let masker = CustomStringMask(mask: mask)
        let maskedPhone = masker.maskedText(from: phone)

        telephoneContainerView.isHidden = maskedPhone == nil
        telephoneLabel.text = maskedPhone
    }

    // MARK: - Constraints
    func setupAddressContainerConstraints() {
        addressContainerView.layout {
            $0.top == titleLabel.bottomAnchor + Spacing.base02
            $0.leading == leadingAnchor + Spacing.base02
            $0.trailing <= trailingAnchor - Spacing.base02
        }

        placePinImageView.layout {
            $0.top == addressContainerView.topAnchor
            $0.bottom == addressContainerView.bottomAnchor
            $0.width == Layout.Size.placePin.width
            $0.height == Layout.Size.placePin.height
            $0.centerY == addressLabel.centerYAnchor
            $0.leading == addressContainerView.leadingAnchor
        }

        addressLabel.layout {
            $0.leading == placePinImageView.trailingAnchor + Layout.Margin.betweenPlacePinAndAddress
            $0.trailing == addressContainerView.trailingAnchor
            $0.centerY == addressContainerView.centerYAnchor
            $0.top >= addressContainerView.topAnchor
            $0.bottom <= addressContainerView.bottomAnchor
        }
    }

    func setupTelephoneContainerConstraints() {
        telephoneContainerView.layout {
            $0.height == Layout.Size.telephoneContainerHeight
        }

        telephoneIconImageView.layout {
            $0.leading == telephoneContainerView.leadingAnchor + Layout.Margin.telephone
            $0.centerY == telephoneContainerView.centerYAnchor
            $0.height == Layout.Size.telephoneIcon.height
            $0.width == Layout.Size.telephoneIcon.width
        }

        telephoneLabel.layout {
            $0.leading == telephoneIconImageView.trailingAnchor + Layout.Margin.telephone
            $0.centerY == telephoneContainerView.centerYAnchor
        }

        callButton.layout {
            $0.leading == telephoneLabel.trailingAnchor + Spacing.base02
            $0.trailing == telephoneContainerView.trailingAnchor - Spacing.base02
            $0.centerY == telephoneContainerView.centerYAnchor
        }
    }

    // MARK: - Actions
    @objc
    func didTapAtCallButton() {
        didTapAtCallAction?()
    }
    
    @objc
    func didTapAtShareButton() {
        didTapAtShareAction?()
    }
}
