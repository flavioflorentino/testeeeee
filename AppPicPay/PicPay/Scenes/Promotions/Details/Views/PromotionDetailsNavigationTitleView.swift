import UI
import UIKit

// MARK: - Layout
private extension PromotionDetailsNavigationTitleView.Layout {
    enum Font {
        static let title: UIFont = .systemFont(ofSize: 14.0)
        static let address: UIFont = .systemFont(ofSize: 12.0)
    }

    enum Margin {
        static let betweenPlacePinAndAddress: CGFloat = 5.28
    }

    enum Size {
        static let placePin = CGSize(width: 9.18, height: 11.08)
    }
}

final class PromotionDetailsNavigationTitleView: UIView {
    fileprivate enum Layout { }

    // MARK: - Properties
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Spacing.base00
        stackView.alignment = .center

        return stackView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.title
        label.textAlignment = .center
        label.textColor = Palette.white.color

        return label
    }()

    private lazy var addressContainerView = UIView()

    private lazy var placePinImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.Search.stablishment.image.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = Palette.white.color

        return imageView
    }()

    private lazy var addressLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.address
        label.textAlignment = .center
        label.textColor = Palette.white.color

        return label
    }()

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setTitle(_ title: String?) {
        titleLabel.text = title
    }

    func setAddress(_ address: String?) {
        addressLabel.text = address
    }
}

// MARK: - ViewConfiguration
extension PromotionDetailsNavigationTitleView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(stackView)
        stackView.addArrangedSubviews(titleLabel, addressContainerView)
        addressContainerView.addSubviews(placePinImageView, addressLabel)
    }

    func setupConstraints() {
        stackView.layout {
            $0.top == topAnchor
            $0.bottom == bottomAnchor
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
        }

        placePinImageView.layout {
            $0.top == addressContainerView.topAnchor
            $0.bottom == addressContainerView.bottomAnchor
            $0.leading >= addressContainerView.leadingAnchor
            $0.width == Layout.Size.placePin.width
            $0.height == Layout.Size.placePin.height
        }

        addressLabel.layout {
            $0.top >= addressContainerView.topAnchor
            $0.bottom <= addressContainerView.bottomAnchor
            $0.leading == placePinImageView.trailingAnchor + Layout.Margin.betweenPlacePinAndAddress
            $0.trailing <= addressContainerView.trailingAnchor
            $0.centerY == addressContainerView.centerYAnchor
        }
    }
}
