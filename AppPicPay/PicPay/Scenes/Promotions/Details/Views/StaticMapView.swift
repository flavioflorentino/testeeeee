import MapKit
import UI
import UIKit

final class StaticMapView: UIView {
    struct Coordinate {
        let latitude: Double
        let longitude: Double
    }

    // MARK: - Properties
    private var currentLocationCoordinate: CLLocationCoordinate2D?
    private var currentPinImage: UIImage?

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit

        return imageView
    }()

    override var backgroundColor: UIColor? {
        didSet {
            imageView.backgroundColor = backgroundColor
        }
    }

    // MARK: - Initialization
    convenience init() {
        self.init(frame: .zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        renderMap()
    }

    // MARK: - Methods
    func showMap(for coordinate: Coordinate, pinImage: UIImage? = nil) {
        currentLocationCoordinate = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
        currentPinImage = pinImage

        renderMap()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        guard imageView.image == nil else {
            return
        }

        renderMap()
    }
}

// MARK: - ViewConfiguration
extension StaticMapView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(imageView)
    }

    func setupConstraints() {
        imageView.layout {
            $0.top == topAnchor
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.bottom == bottomAnchor
        }
    }

    func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
    }
}

// MARK: - Private
private extension StaticMapView {
    func renderMap() {
        imageView.image = nil

        let snapshotSize = imageView.bounds.size
        guard let coordinate = currentLocationCoordinate, snapshotSize != .zero else {
            return
        }

        takeMapSnapshot(for: coordinate, pinImage: currentPinImage, snapshotSize: imageView.bounds.size) { [weak self] image in
            self?.imageView.image = image
        }
    }

    func takeMapSnapshot(
        for coordinate: CLLocationCoordinate2D,
        pinImage: UIImage?,
        snapshotSize imageSize: CGSize,
        completion: @escaping (UIImage?) -> Void
    ) {
        let options = MKMapSnapshotter.Options()
        options.size = imageSize
        options.region = MKCoordinateRegion(center: coordinate, latitudinalMeters: 1_000, longitudinalMeters: 1_000)

        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate

        let pin = MKPinAnnotationView(annotation: annotation, reuseIdentifier: nil)
        pin.animatesDrop = false
        pin.image = pinImage

        let snapshotter = MKMapSnapshotter(options: options)
        snapshotter.start(with: .global(qos: .default)) { snapshot, _ in
            guard let snapshot = snapshot else {
                return completion(nil)
            }

            let compositeImage = self.compositeImage(
                snapshot: snapshot.image,
                pinImage: pin.image,
                pinPoint: snapshot.point(for: coordinate),
                pinCenterOffset: pin.centerOffset
            )

            DispatchQueue.main.async {
                completion(compositeImage)
            }
        }
    }

    func compositeImage(snapshot image: UIImage, pinImage: UIImage?, pinPoint: CGPoint, pinCenterOffset: CGPoint) -> UIImage {
        UIGraphicsImageRenderer(size: image.size).image { _ in
            image.draw(at: .zero)

            if let pinImage = pinImage {
                var point = pinPoint
                point.x += pinCenterOffset.x - (pinImage.size.width / 2.0)
                point.y += pinCenterOffset.y - (pinImage.size.height / 2.0)

                pinImage.draw(at: point)
            }
        }
    }
}
