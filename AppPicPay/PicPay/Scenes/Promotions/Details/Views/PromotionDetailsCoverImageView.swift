import UIKit

final class PromotionDetailsCoverImageView: UIImageView {
    override var image: UIImage? {
        get {
            super.image
        }
        set {
            super.image = blur(newValue)
        }
    }
}

// MARK: - Private
private extension PromotionDetailsCoverImageView {
    func blur(_ image: UIImage?) -> UIImage? {
        guard let image = image else {
            return nil
        }

        let ciImage = CIImage(image: image)
        let blurFilter = CIFilter(name: "CIGaussianBlur")
        blurFilter?.setValue(ciImage, forKey: kCIInputImageKey)
        blurFilter?.setValue(25.0, forKey: kCIInputRadiusKey)

        guard let outputImage = blurFilter?.outputImage else {
            return image
        }

        return UIImage(ciImage: outputImage)
    }
}
