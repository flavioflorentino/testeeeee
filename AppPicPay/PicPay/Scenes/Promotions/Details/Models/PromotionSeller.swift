import Foundation

struct PromotionSeller: Equatable {
    // MARK: - Properties
    let title: String
    let address: String?
    var phone: String?
    let imageUrl: URL?
    let description: String
    let items: [Item]
    let paymentInfo: PaymentInfo?
    let tracking: Tracking?
    let map: Map?
}

// MARK: - Decodable
extension PromotionSeller: Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let title = try container.decode(String.self, forKey: .title)
        let address = try container.decodeIfPresent(String.self, forKey: .address)
        let phone = try container.decodeIfPresent(String.self, forKey: .phone)
        let imageUrl = try? container.decodeIfPresent(URL.self, forKey: .imageUrl)
        let description = try container.decode(String.self, forKey: .description)
        let items = try container.decode([Item].self, forKey: .items)
        let paymentInfo = try container.decodeIfPresent(PaymentInfo.self, forKey: .paymentInfo)
        let tracking = try container.decode(Tracking.self, forKey: .tracking)
        let map = try? container.decodeIfPresent(Map.self, forKey: .map)

        self.init(
            title: title,
            address: address,
            phone: phone,
            imageUrl: imageUrl,
            description: description,
            items: items,
            paymentInfo: paymentInfo,
            tracking: tracking,
            map: map
        )
    }
}

// MARK: - Private extension
private extension PromotionSeller {
    enum CodingKeys: String, CodingKey {
        case title
        case address
        case phone
        case description
        case items = "data"
        case paymentInfo = "paymentData"
        case tracking
        case map
        case imageUrl = "image"
    }
}

// MARK: - Nested types
extension PromotionSeller {
    struct Item: Equatable {
        let id: String
        let title: String
        let iconUrl: URL?
        let iconTimeUrl: URL?
        let badge: String
        let maxValue: String
        let remainingTime: String?
        let enabled: Bool
    }
}

extension PromotionSeller.Item: Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let id = try container.decode(String.self, forKey: .id)
        let title = try container.decode(String.self, forKey: .title)
        let iconUrl = try? container.decode(URL.self, forKey: .iconUrl)
        let iconTimeUrl = try? container.decodeIfPresent(URL.self, forKey: .iconTimeUrl)
        let badge = try container.decode(String.self, forKey: .badge)
        let maxValue = try container.decode(String.self, forKey: .maxValue)
        let remainingTime = try? container.decodeIfPresent(String.self, forKey: .remainingTime)
        let enabled = try? container.decodeIfPresent(Bool.self, forKey: .enabled)

        self.init(
            id: id,
            title: title,
            iconUrl: iconUrl,
            iconTimeUrl: iconTimeUrl,
            badge: badge,
            maxValue: maxValue,
            remainingTime: remainingTime,
            enabled: enabled ?? true
        )
    }
}

private extension PromotionSeller.Item {
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case iconUrl
        case iconTimeUrl
        case badge
        case maxValue
        case remainingTime
        case enabled
    }
}

extension PromotionSeller {
    struct PaymentInfo: Equatable {
        let sellerId: Int?
        let storeId: Int?
    }
}

extension PromotionSeller.PaymentInfo: Decodable {
}

extension PromotionSeller {
    struct Tracking: Equatable {
        let type: String
        let title: String
        let sellerId: Int
        let storeId: Int?
        let section: String
    }
}

extension PromotionSeller.Tracking: Decodable {
}

extension PromotionSeller {
    struct Map: Equatable {
        let latitude: Double
        let longitude: Double
        let pinUrl: URL?
    }
}

extension PromotionSeller.Map: Decodable {
}
