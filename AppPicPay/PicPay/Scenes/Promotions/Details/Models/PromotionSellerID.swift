import Foundation

struct PromotionSellerID: Decodable {
    enum CodingKeys: String, CodingKey {
        case data
        case seller = "Seller"
        case id
    }

    let sellerId: String

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let data = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        let seller = try data.nestedContainer(keyedBy: CodingKeys.self, forKey: .seller)
        self.sellerId = try seller.decode(String.self, forKey: .id)
    }
}
