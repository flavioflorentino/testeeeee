struct WhatsAppSeller: Equatable {
    let phone: String
    let text: String
}

// MARK: - Decodable
extension WhatsAppSeller: Decodable {
    enum CodingKeys: String, CodingKey {
        case data, phone, text
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let data = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        phone = try data.decode(String.self, forKey: .phone)
        text = try data.decode(String.self, forKey: .text)
    }
}
