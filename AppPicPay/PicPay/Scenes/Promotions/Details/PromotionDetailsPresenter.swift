import Core

protocol PromotionDetailsPresenting {
    func show(details promotionSeller: PromotionSeller)
    func show(error: ApiError)

    func updateState(_ state: FavoriteState)

    func callPhone(_ number: String?)
    func performPayment(seller: PromotionSeller)
    func openMap(title: String, latitude: Double, longitude: Double)
    func openWhatsApp(whatsapp: WhatsAppSeller)
    
    func presentShare(withText text: String)
    func presentWhatsAppButton(isHidden: Bool)
}

final class PromotionDetailsPresenter {
    typealias Display = PromotionDetailsDisplay
    private typealias Localizable = Strings.Promotions

    // MARK: - Properties
    weak var display: Display?

    var callPhoneAction: ((String?) -> Void)?
    var performPaymentAction: ((PromotionSeller) -> Void)?
    var openMapAction: ((PromotionDetailsFactory.MapData) -> Void)?
    var openWhatsAppAction: ((WhatsAppSeller) -> Void)?
}

// MARK: - PromotionDetailsPresenting
extension PromotionDetailsPresenter: PromotionDetailsPresenting {
    func show(details promotionSeller: PromotionSeller) {
        var items = promotionSeller.items.map { PromotionDetailsDisplayData.Item.info($0) }
        if let map = promotionSeller.map {
            items.append(.map(title: promotionSeller.title, map: map))
        }

        let data = PromotionDetailsDisplayData(title: promotionSeller.title,
                                               description: promotionSeller.description,
                                               address: promotionSeller.address,
                                               items: items,
                                               imageUrl: promotionSeller.imageUrl,
                                               phone: promotionSeller.phone)
        display?.show(details: data)
    }

    func show(error: ApiError) {
        display?.show(error: errorData(from: error))
    }

    func callPhone(_ number: String?) {
        callPhoneAction?(number)
    }

    func performPayment(seller: PromotionSeller) {
        performPaymentAction?(seller)
    }

    func openMap(title: String, latitude: Double, longitude: Double) {
        let mapData = PromotionDetailsFactory.MapData(title: title, latitude: latitude, longitude: longitude)
        openMapAction?(mapData)
    }

    func openWhatsApp(whatsapp: WhatsAppSeller) {
        openWhatsAppAction?(whatsapp)
    }
    
    func updateState(_ state: FavoriteState) {
        display?.updateState(state)
    }
    
    func presentShare(withText text: String) {
        let activityController = UIActivityViewController(activityItems: [text], applicationActivities: nil)
        display?.show(share: activityController)
    }
    
    func presentWhatsAppButton(isHidden: Bool) {
        display?.whatsAppButton(isHidden: isHidden)
    }
}

// MARK: - Private
private extension PromotionDetailsPresenter {
    func errorData(from error: ApiError) -> PromotionListDisplay.ErrorData {
        switch error {
        case .connectionFailure:
            return (
                image: Assets.iluSearchErrorCon.image,
                content: (
                    title: Localizable.noInternetConnectionError,
                    description: Localizable.checkYourInternetConnection
                ),
                button: (
                    image: Assets.Icons.icoRefresh.image,
                    title: DefaultLocalizable.tryAgain.text
                )
            )
        default:
            return (
                image: Assets.CreditPicPay.Registration.sad3.image,
                content: (
                    title: Localizable.unknownErrorTitle,
                    description: Localizable.weAreDownError
                ),
                button: (
                    image: Assets.Icons.icoRefresh.image,
                    title: DefaultLocalizable.tryAgain.text
                )
            )
        }
    }
}
