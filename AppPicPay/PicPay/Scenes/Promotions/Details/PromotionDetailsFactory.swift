protocol PromotionDetailsFactory {
    typealias MapData = (title: String, latitude: Double, longitude: Double)

    func makeDetailsController(
        title: String?,
        idType: PromotionDetailsIDType,
        callPhone: @escaping (String?) -> Void,
        openMap: @escaping (MapData) -> Void,
        performPayment: @escaping (PromotionSeller) -> Void,
        openWhatsApp: @escaping (WhatsAppSeller) -> Void
    ) -> PromotionDetailsViewController
}
