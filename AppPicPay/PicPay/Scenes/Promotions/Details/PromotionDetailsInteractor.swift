import AnalyticsModule
import Core

protocol PromotionDetailsInteracting: AnyObject {
    func loadDetails()

    func callPhone()
    func performPayment()
    func openWhatsAppConversation()
    func openMap()

    func changeFavoriteState(_ state: FavoriteState)
    
    func shareEstablishment()
}

enum PromotionDetailsIDType {
    case seller(id: String)
    case store(id: String)
}

final class PromotionDetailsInteractor {
    private typealias Localizable = Strings.Promotions
    typealias Dependencies = HasAnalytics & HasFeatureManager
    
    // MARK: - Properties
    private let dependencies: Dependencies
    private var idType: PromotionDetailsIDType
    private let service: PromotionsServicing
    private let presenter: PromotionDetailsPresenting
    private let favoriteInteractor: FavoriteInteracting
    private var whatsAppSeller: WhatsAppSeller?
    private var sellerId: String {
        guard case let .seller(id: id) = idType else {
            return ""
        }
        return id
    }

    private var seller: PromotionSeller?

    // MARK: - Initialization
    init(
        idType: PromotionDetailsIDType,
        service: PromotionsServicing,
        presenter: PromotionDetailsPresenting,
        favoriteInteractor: FavoriteInteracting,
        dependencies: Dependencies
    ) {
        self.idType = idType
        self.service = service
        self.presenter = presenter
        self.favoriteInteractor = favoriteInteractor
        self.dependencies = dependencies
    }
}

// MARK: - PromotionDetailsInteracting
extension PromotionDetailsInteractor: PromotionDetailsInteracting {
    func loadDetails() {
        switch idType {
        case .store(let storeId):
            loadSellerId(from: storeId, then: loadSellerDetails)
        case .seller:
            loadSellerDetails()
        }
    }

    func callPhone() {
        presenter.callPhone(seller?.phone)
    }

    func performPayment() {
        guard let seller = seller else {
            return
        }

        dependencies.analytics.log(CheckoutEvent.checkoutScreenViewed(.ecommerce))
        presenter.performPayment(seller: seller)
    }
    
    func openWhatsAppConversation() {
        guard let whatsapp = whatsAppSeller else {
            presenter.presentWhatsAppButton(isHidden: true)
            return
        }
        // TODO: tracking
        presenter.openWhatsApp(whatsapp: whatsapp)
    }

    func openMap() {
        guard let seller = seller, let map = seller.map else {
            return
        }

        presenter.openMap(title: seller.title, latitude: map.latitude, longitude: map.longitude)
    }

    func checkFavoriteState() {
        favoriteInteractor.checkState(id: sellerId, type: .store) { [weak self] state in
            self?.presenter.updateState(state)
        }
    }

    func changeFavoriteState(_ state: FavoriteState) {
        let id = sellerId
        favoriteInteractor.changeState(state, id: id, type: .store) { [weak self] newState in
            self?.presenter.updateState(newState)

            if newState != state && newState != .unavailable {
                self?.dependencies.analytics.log(FavoritesEvent.statusChanged(newState == .favorited, id: id, origin: .profile))
            }
        }
    }
    
    func shareEstablishment() {
        guard let seller = seller, let tracking = seller.tracking else {
            return
        }

        let event = PromotionsEvent.promotionShare(
            type: tracking.type,
            itemName: tracking.title,
            sellerId: tracking.sellerId
        )
        let consumerName = service.consumerName ?? Localizable.defaultConsumer
        let textMessage = Localizable.shareEstablishment(consumerName, seller.title, sellerId)

        dependencies.analytics.log(event)
        presenter.presentShare(withText: textMessage)
    }
}

// MARK: - Private methods
private extension PromotionDetailsInteractor {
    func loadSellerId(from storeId: String, then completion: @escaping () -> Void) {
        service.loadSellerID(from: storeId) { result in
            switch result {
            case let .success(sellerId):
                self.idType = .seller(id: sellerId)
                completion()
            case let .failure(error):
                self.presenter.show(error: error)
            }
        }
    }

    func loadSellerDetails() {
        loadWhatsApp()
        service.sellerDetails(id: sellerId) { [weak self] result in
            switch result {
            case let .success(seller):
                self?.seller = seller
                self?.presenter.show(details: seller)
                self?.checkFavoriteState()
            case let .failure(error):
                self?.presenter.show(error: error)
            }
        }
    }
    
    func loadWhatsApp() {
        guard dependencies.featureManager.isActive(.isWhatsappSellerAvailable) else { return }
        service.loadWhatsAppNumber(from: sellerId) { [weak self] result in
            if case let Result.success(whatsapp) = result, !whatsapp.phone.isEmpty {
                self?.whatsAppSeller = whatsapp
                self?.presenter.presentWhatsAppButton(isHidden: false)
            }
        }
    }
}
