import UI
import UIKit

// MARK: - Layout
private extension PromotionDetailsMapCell.Layout {
    enum Font {
        static let title: UIFont = .systemFont(ofSize: 12.0, weight: .bold)
        static let info: UIFont = .systemFont(ofSize: 12.0)
    }

    enum CornerRadius {
        static let container: CGFloat = 4.0
        static let map: CGFloat = 8.0
    }
    enum Size {
        static let containerMultiplier: CGFloat = 187.0 / 288.0
        static let arrow = CGSize(width: 8.0, height: 13.0)
    }

    enum Margin {
        static let size00: CGFloat = 2.0
        static let largeTitleLeading: CGFloat = 20.0
    }
}

final class PromotionDetailsMapCell: UITableViewCell {
    fileprivate enum Layout { }
    private typealias Localizable = Strings.Promotions

    // MARK: - Properties
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale000.color
        view.layer.cornerRadius = Layout.CornerRadius.container
        view.layer.shadowColor = Palette.ppColorGrayscale600.color.withAlphaComponent(0.1).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize(width: 1.0, height: 2.0)
        view.layer.shadowRadius = 2
        view.layer.borderWidth = 0.1
        view.layer.borderColor = Palette.ppColorGrayscale300.color.cgColor

        return view
    }()

    private lazy var mapView: StaticMapView = {
        let imageView = StaticMapView()
        imageView.backgroundColor = Palette.ppColorGrayscale000.color
        imageView.layer.cornerRadius = Layout.CornerRadius.map
        imageView.layer.masksToBounds = true

        return imageView
    }()

    private lazy var labelsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Layout.Margin.size00

        return stackView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = Palette.ppColorGrayscale000.color
        label.font = Layout.Font.title
        label.textColor = Palette.ppColorGrayscale500.color

        return label
    }()

    private lazy var howToArriveLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = Palette.ppColorGrayscale000.color
        label.font = Layout.Font.info
        label.text = Localizable.howToArrive
        label.textColor = Palette.ppColorGrayscale500.color

        return label
    }()

    private lazy var arrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = Palette.ppColorGrayscale000.color
        imageView.image = Assets.Icons.rightArrow.image.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = Palette.ppColorBranding300.color

        return imageView
    }()

    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Hit testing
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let convertedPoint = convert(point, to: containerView)
        return containerView.hitTest(convertedPoint, with: event)
    }
}

// MARK: - PromotionDetailsItemCell
extension PromotionDetailsMapCell {
    func setup(with title: String, latitude: Double, longitude: Double) {
        titleLabel.text = title
        mapView.showMap(
            for: .init(latitude: latitude, longitude: longitude),
            pinImage: Assets.Icons.icoPromotionsMapPin.image
        )
    }
}

// MARK: - ViewConfiguration
extension PromotionDetailsMapCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(containerView)
        containerView.addSubviews(mapView, labelsStackView, arrowImageView)
        labelsStackView.addArrangedSubviews(titleLabel, howToArriveLabel)
    }

    func setupConstraints() {
        containerView.layout {
            $0.top == contentView.topAnchor + Spacing.base00
            $0.leading == contentView.leadingAnchor + Layout.Margin.largeTitleLeading
            $0.trailing == contentView.trailingAnchor - Layout.Margin.largeTitleLeading
            $0.bottom == contentView.bottomAnchor - Spacing.base02
        }

        containerView.heightAnchor.constraint(
            equalTo: containerView.widthAnchor,
            multiplier: Layout.Size.containerMultiplier
        ).isActive = true

        mapView.layout {
            $0.top == containerView.topAnchor + Spacing.base02
            $0.leading == containerView.leadingAnchor + Spacing.base02
            $0.trailing == containerView.trailingAnchor - Spacing.base02
        }

        labelsStackView.layout {
            $0.top == mapView.bottomAnchor + Spacing.base02
            $0.leading == containerView.leadingAnchor + Spacing.base02
            $0.bottom == containerView.bottomAnchor - Spacing.base02
        }

        arrowImageView.layout {
            $0.centerY == labelsStackView.centerYAnchor
            $0.leading == labelsStackView.trailingAnchor + Spacing.base02
            $0.trailing == mapView.trailingAnchor
            $0.height == Layout.Size.arrow.height
            $0.width == Layout.Size.arrow.width
        }
    }

    func configureViews() {
        layoutMargins = .zero
        separatorInset = UIEdgeInsets(top: Spacing.none, left: Spacing.base02, bottom: Spacing.none, right: Spacing.base02)
        selectionStyle = .none
        backgroundColor = Palette.ppColorGrayscale000.color
    }
}
