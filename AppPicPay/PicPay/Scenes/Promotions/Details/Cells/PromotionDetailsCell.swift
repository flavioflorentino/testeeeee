import UI
import UIKit

// MARK: - Layout
private extension PromotionDetailsCell.Layout {
    enum Font {
        static let description: UIFont = .systemFont(ofSize: 14.0, weight: .semibold)
        static let badge: UIFont = .systemFont(ofSize: 12.0, weight: .semibold)
        static let info: UIFont = .systemFont(ofSize: 12.0)
    }

    enum Size {
        static let icon: CGFloat = 32.0
        static let badge: CGFloat = 16.0
    }
}

final class PromotionDetailsCell: UITableViewCell {
    fileprivate enum Layout { }

    // MARK: - Properties
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = Palette.ppColorGrayscale000.color
        imageView.contentMode = .scaleAspectFit

        return imageView
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = Palette.ppColorGrayscale000.color
        label.font = Layout.Font.description
        label.numberOfLines = 0
        label.textColor = Palette.ppColorGrayscale500.color

        return label
    }()

    private let badgeContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorBranding300.color
        view.roundCorners([.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner], radius: 3.0)

        return view
    }()

    private let badgeLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = Palette.ppColorBranding300.color
        label.font = Layout.Font.badge
        label.numberOfLines = 1
        label.textColor = Palette.ppColorGrayscale000.color
        label.textAlignment = .center
        label.contentMode = .center

        return label
    }()

    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = Palette.ppColorGrayscale000.color
        label.font = Layout.Font.info
        label.numberOfLines = 1
        label.textColor = Palette.ppColorGrayscale500.color

        return label
    }()

    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Reusing
    override func prepareForReuse() {
        super.prepareForReuse()

        iconImageView.cancelRequest()
        iconImageView.image = nil
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        badgeContainerView.roundCorners([.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner], radius: 3.0)
    }
}

// MARK: - PromotionDetailsItemCell
extension PromotionDetailsCell: PromotionDetailsItemCell {
    func setup(with item: PromotionSeller.Item) {
        iconImageView.setImage(url: item.iconUrl)
        update(description: item.title)

        badgeLabel.text = item.badge
        infoLabel.text = item.maxValue

        badgeContainerView.setNeedsLayout()
        badgeContainerView.layoutIfNeeded()
    }
}

// MARK: - ViewConfiguration
extension PromotionDetailsCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubviews(iconImageView, descriptionLabel, badgeContainerView, infoLabel)
        badgeContainerView.addSubview(badgeLabel)
    }

    func setupConstraints() {
        iconImageView.layout {
            $0.width == Layout.Size.icon
            $0.height == Layout.Size.icon
            $0.top == contentView.topAnchor + Spacing.base03
            $0.leading == contentView.leadingAnchor + Spacing.base04
        }

        descriptionLabel.layout {
            $0.top == contentView.topAnchor + Spacing.base03
            $0.leading == iconImageView.trailingAnchor + Spacing.base02
            $0.trailing <= contentView.trailingAnchor - Spacing.base04
        }

        badgeContainerView.layout {
            $0.top == descriptionLabel.bottomAnchor + Spacing.base01
            $0.leading == descriptionLabel.leadingAnchor
            $0.trailing <= contentView.trailingAnchor - Spacing.base04
            $0.height >= Layout.Size.badge
        }

        badgeLabel.layout {
            $0.top == badgeContainerView.topAnchor
            $0.leading == badgeContainerView.leadingAnchor + Spacing.base01
            $0.trailing == badgeContainerView.trailingAnchor - Spacing.base01
            $0.bottom == badgeContainerView.bottomAnchor
        }

        infoLabel.layout {
            $0.top == badgeContainerView.bottomAnchor + Spacing.base01
            $0.leading == descriptionLabel.leadingAnchor
            $0.trailing <= contentView.trailingAnchor - Spacing.base04
            $0.bottom <= contentView.bottomAnchor - Spacing.base02
        }
    }

    func configureViews() {
        layoutMargins = .zero
        separatorInset = UIEdgeInsets(top: Spacing.none, left: Spacing.base02, bottom: Spacing.none, right: Spacing.base02)
        selectionStyle = .none
        backgroundColor = Palette.ppColorGrayscale000.color
    }
}

// MARK: - Private
private extension PromotionDetailsCell {
    func update(description: String?) {
        guard let description = description else {
            return descriptionLabel.attributedText = nil
        }

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.26

        let attributedText = NSAttributedString(
            string: description,
            attributes: [.paragraphStyle: paragraphStyle]
        )

        descriptionLabel.attributedText = attributedText
    }
}
