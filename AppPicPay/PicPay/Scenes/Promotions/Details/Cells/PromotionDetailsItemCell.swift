protocol PromotionDetailsItemCell: AnyObject {
    func setup(with item: PromotionSeller.Item)
}
