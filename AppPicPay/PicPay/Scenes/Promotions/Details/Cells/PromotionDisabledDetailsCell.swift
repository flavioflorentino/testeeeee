import UI
import UIKit

// MARK: - Layout
private extension PromotionDisabledDetailsCell.Layout {
    enum Font {
        static let description: UIFont = .systemFont(ofSize: 14.0, weight: .semibold)
        static let info: UIFont = .systemFont(ofSize: 12.0, weight: .semibold)
    }

    enum Size {
        static let icon: CGFloat = 32.0
        static let timeIcon: CGFloat = 24.0
    }

    enum Color {
        static let text: UIColor = Palette.ppColorGrayscale700.color
    }
}

final class PromotionDisabledDetailsCell: UITableViewCell {
    fileprivate enum Layout { }

    // MARK: - Properties
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = Palette.ppColorGrayscale000.color
        imageView.contentMode = .scaleAspectFit

        return imageView
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = Palette.ppColorGrayscale000.color
        label.font = Layout.Font.description
        label.numberOfLines = 0
        label.textColor = Layout.Color.text

        return label
    }()

    private lazy var timeIconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = Palette.ppColorGrayscale000.color
        imageView.contentMode = .scaleAspectFit

        return imageView
    }()

    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = Palette.ppColorGrayscale000.color
        label.font = Layout.Font.info
        label.numberOfLines = 0
        label.textColor = Layout.Color.text

        return label
    }()

    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Reusing
    override func prepareForReuse() {
        super.prepareForReuse()

        iconImageView.cancelRequest()
        iconImageView.image = nil

        timeIconImageView.cancelRequest()
        timeIconImageView.image = nil
    }
}

// MARK: - PromotionDetailsItemCell
extension PromotionDisabledDetailsCell: PromotionDetailsItemCell {
    func setup(with item: PromotionSeller.Item) {
        iconImageView.setImage(url: item.iconUrl)
        timeIconImageView.setImage(url: item.iconTimeUrl, placeholder: Assets.Icons.icoClock.image)

        descriptionLabel.attributedText = attributedText(item.title)
        infoLabel.attributedText = attributedText(item.remainingTime)
    }
}

// MARK: - ViewConfiguration
extension PromotionDisabledDetailsCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubviews(iconImageView, descriptionLabel, timeIconImageView, infoLabel)
    }

    func setupConstraints() {
        iconImageView.layout {
            $0.width == Layout.Size.icon
            $0.height == Layout.Size.icon
            $0.top == contentView.topAnchor + Spacing.base03
            $0.leading == contentView.leadingAnchor + Spacing.base04
        }

        descriptionLabel.layout {
            $0.top == contentView.topAnchor + Spacing.base03
            $0.leading == iconImageView.trailingAnchor + Spacing.base02
            $0.trailing <= contentView.trailingAnchor - Spacing.base04
        }

        timeIconImageView.layout {
            $0.width == Layout.Size.timeIcon
            $0.height == Layout.Size.timeIcon
            $0.leading == descriptionLabel.leadingAnchor
            $0.top >= descriptionLabel.bottomAnchor + Spacing.base01
            $0.bottom <= contentView.bottomAnchor - Spacing.base01
            $0.centerY == infoLabel.centerYAnchor
        }

        infoLabel.layout {
            $0.top == descriptionLabel.bottomAnchor + Spacing.base02
            $0.leading == timeIconImageView.trailingAnchor + Spacing.base01
            $0.trailing <= contentView.trailingAnchor - Spacing.base04
            $0.bottom <= contentView.bottomAnchor - Spacing.base02
        }
    }

    func configureViews() {
        layoutMargins = .zero
        separatorInset = UIEdgeInsets(top: Spacing.none, left: Spacing.base02, bottom: Spacing.none, right: Spacing.base02)
        selectionStyle = .none
        backgroundColor = Palette.ppColorGrayscale000.color
    }
}

// MARK: - Private
private extension PromotionDisabledDetailsCell {
    func attributedText(_ text: String?) -> NSAttributedString? {
        guard let text = text else {
            return nil
        }

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.26

        return NSAttributedString(string: text, attributes: [.paragraphStyle: paragraphStyle])
    }
}
