import UI
import UIKit

// MARK: - Layout
private extension PromotionListTableFooterView.Layout {
    enum Font {
        static let title: UIFont = .systemFont(ofSize: 14.0, weight: .semibold)
    }

    enum Margin {
        static let largeTitleLeading: CGFloat = 20.0
    }
}

final class PromotionListTableFooterView: UIView {
    // MARK: - Properties
    fileprivate enum Layout { }
    private typealias Localizable = Strings.Promotions

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.title
        label.text = Localizable.nearbyLocations
        label.textColor = Colors.grayscale700.color

        return label
    }()

    private lazy var warningView = PromotionsLocationWarningView()

    var authorizeNowAction: (() -> Void)? {
        get {
            warningView.authorizeNowAction
        }
        set {
            warningView.authorizeNowAction = newValue
        }
    }

    // MARK: - Initialization
    convenience init() {
        self.init(frame: .zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration
extension PromotionListTableFooterView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(titleLabel)
        addSubview(warningView)
    }

    func setupConstraints() {
        titleLabel.layout {
            $0.top == topAnchor + Spacing.base03
            $0.leading == leadingAnchor + Layout.Margin.largeTitleLeading
            $0.trailing <= trailingAnchor - Spacing.base02
        }

        warningView.layout {
            $0.top == titleLabel.bottomAnchor
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.bottom == bottomAnchor
        }
    }

    func configureViews() {
        backgroundColor = .clear
    }
}
