import UI
import UIKit

final class PromotionListTableHeaderView: UIView {
    private typealias Localizable = Strings.Promotions

    // MARK: - Properties
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textColor, .grayscale600())
        label.text = Localizable.promotionsDescription

        return label
    }()

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }

    convenience init() {
        self.init(frame: .zero)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration
extension PromotionListTableHeaderView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(descriptionLabel)
    }

    func setupConstraints() {
        descriptionLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.bottom.equalToSuperview().offset(-Spacing.base03)
        }
    }

    func configureViews() {
        backgroundColor = .clear
    }

    func setup(description: String) {
        descriptionLabel.text = description
    }
}
