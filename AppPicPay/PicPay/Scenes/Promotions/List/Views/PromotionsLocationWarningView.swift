import UI
import UIKit

// MARK: - Layout
extension PromotionsLocationWarningView.Layout {
    enum Button {
        static let size = CGSize(width: 288.0, height: 48.0)
        static let cornerRadius: CGFloat = size.height / 2.0
        static let font: UIFont = .systemFont(ofSize: 16.0, weight: .semibold)
    }

    enum Font {
        static let title: UIFont = .systemFont(ofSize: 16.0, weight: .semibold)
        static let description: UIFont = .systemFont(ofSize: 12.0)

        static let lineHeightMultiplier: CGFloat = 1.26

        static func attributedText(_ text: String) -> NSAttributedString {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = lineHeightMultiplier

            return NSAttributedString(string: text, attributes: [.paragraphStyle: paragraphStyle])
        }
    }

    enum Margin {
        static let largeTitleLeading: CGFloat = 20
    }

    enum Size {
        static let icon: CGFloat = 80.54
    }
}

final class PromotionsLocationWarningView: UIView {
    // MARK: - Properties
    fileprivate enum Layout { }
    private typealias Localizable = Strings.Promotions

    private lazy var iconImageView = UIImageView(image: Assets.Authorization.iluLocationAuth.image)

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.title
        label.attributedText = Layout.Font.attributedText(Localizable.discoverStoresNearYou)
        label.lineBreakMode = .byWordWrapping
        label.textColor = Colors.black.color
        label.textAlignment = .center
        label.numberOfLines = 0

        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.description
        label.attributedText = Layout.Font.attributedText(Localizable.discoverStoresNearYouDescription)
        label.lineBreakMode = .byWordWrapping
        label.textColor = Colors.grayscale700.color
        label.textAlignment = .center
        label.numberOfLines = 0

        return label
    }()

    private lazy var authorizeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle(Localizable.authorizeLocationButton, for: .normal)
        button.backgroundColor = Colors.branding400.color
        button.layer.cornerRadius = Layout.Button.cornerRadius
        button.layer.masksToBounds = false
        button.titleLabel?.font = Layout.Button.font
        button.setTitleColor(Colors.white.color, for: .normal)
        button.addTarget(self, action: #selector(didTapAtAuthorizeNowButton), for: .touchUpInside)

        return button
    }()

    var authorizeNowAction: (() -> Void)?

    // MARK: - Initialization
    convenience init() {
        self.init(frame: .zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration
extension PromotionsLocationWarningView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(iconImageView)
        addSubview(titleLabel)
        addSubview(descriptionLabel)
        addSubview(authorizeButton)
    }

    func setupConstraints() {
        iconImageView.layout {
            $0.top == topAnchor + Spacing.base03
            $0.centerX == centerXAnchor
            $0.width == Layout.Size.icon
            $0.height == Layout.Size.icon
        }

        titleLabel.layout {
            $0.top == iconImageView.bottomAnchor + Spacing.base03
            $0.centerX == centerXAnchor
            $0.leading >= leadingAnchor + Spacing.base02
            $0.trailing <= trailingAnchor - Spacing.base02
        }

        descriptionLabel.layout {
            $0.top == titleLabel.bottomAnchor + Spacing.base02
            $0.centerX == centerXAnchor
            $0.leading >= leadingAnchor + Spacing.base02
            $0.trailing <= trailingAnchor - Spacing.base02
        }

        authorizeButton.layout {
            $0.top == descriptionLabel.bottomAnchor + Spacing.base02
            $0.leading == leadingAnchor + Layout.Margin.largeTitleLeading
            $0.trailing == trailingAnchor - Layout.Margin.largeTitleLeading
            $0.centerX == centerXAnchor
            $0.bottom == bottomAnchor - Spacing.base02
            $0.height == Layout.Button.size.height
        }
    }

    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - Private
private extension PromotionsLocationWarningView {
    @objc
    func didTapAtAuthorizeNowButton() {
        authorizeNowAction?()
    }
}
