import UI
import UIKit

final class PromotionListSectionHeaderView: UITableViewHeaderFooterView {
    // MARK: - Properties
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: Layout.fontSize, weight: .semibold)
        label.textColor = Colors.grayscale700.color
        label.numberOfLines = 1

        return label
    }()

    // MARK: - Initialization
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup(with text: String?) {
        titleLabel.text = text
    }
}

// MARK: - ViewConfiguration
extension PromotionListSectionHeaderView: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(titleLabel)
    }

    func setupConstraints() {
        titleLabel.layout {
            $0.top == contentView.topAnchor + Layout.margin300
            $0.leading == contentView.leadingAnchor + Layout.margin400
            $0.trailing <= contentView.trailingAnchor - Layout.margin300
            $0.bottom == contentView.bottomAnchor - Layout.margin100
        }
    }

    func configureViews() {
        contentView.backgroundColor = .clear
        contentView.translatesAutoresizingMaskIntoConstraints = false
    }
}

// MARK: - Private
private extension PromotionListSectionHeaderView {
    enum Layout {
        static let fontSize: CGFloat = 14.0

        static let margin000: CGFloat = 4.0
        static let margin100: CGFloat = 14.0
        static let margin300: CGFloat = 16.0
        static let margin400: CGFloat = 20.0
    }
}
