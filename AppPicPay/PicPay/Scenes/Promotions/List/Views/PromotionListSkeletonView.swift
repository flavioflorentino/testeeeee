import SkeletonView
import UI
import UIKit

// MARK: - Layout
private extension PromotionListSkeletonView.Layout {
    enum TableHeader { }
    enum Cell { }

    enum Margin {
        static let size200: CGFloat = 8.0
        static let size300: CGFloat = 16.0
        static let size400: CGFloat = 20.0

        static let tableHeaderToSectionHeader: CGFloat = 36.0
        static let sectionHeaderTrailing: CGFloat = 118.0
    }

    enum Size {
        static let line: CGFloat = 12.0
    }

    enum CornerRadius {
        static let line: CGFloat = Size.line / 2.0
    }
}

private extension PromotionListSkeletonView.Layout.TableHeader {
    enum Margin {
        static let firstLineTrailing: CGFloat = 32.0
        static let secondLineTrailing: CGFloat = 57.0
        static let thirdLineTrailing: CGFloat = 224.0
    }
}

private extension PromotionListSkeletonView.Layout.Cell {
    enum Size {
        static let container: CGFloat = 96.0
        static let icon: CGFloat = 48.0
        static let indicator: CGFloat = 18.0
    }

    enum Margin {
        static let secondLineTrailing: CGFloat = 38.0
        static let indicatorTrailing: CGFloat = 19.0
    }

    enum CornerRadius {
        static let container: CGFloat = 4.0
        static let icon: CGFloat = Size.icon / 2.0
        static let indicator: CGFloat = Size.indicator / 2.0
    }
}

final class PromotionListSkeletonView: UIView, StatefulViewing {
    fileprivate enum Layout { }

    // MARK: - Properties
    private lazy var tableHeader = TableHeader()
    private lazy var sectionHeader: UIView = {
        let view = UIView()
        view.layer.cornerRadius = Layout.CornerRadius.line
        view.clipsToBounds = true
        view.isSkeletonable = true

        return view
    }()

    private lazy var cellsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [Cell(), Cell()])
        stackView.axis = .vertical
        stackView.spacing = Layout.Margin.size200

        return stackView
    }()

    var viewModel: StatefulViewModeling?
    weak var delegate: StatefulDelegate?

    // MARK: - Initialization
    convenience init() {
        self.init(frame: .zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func updateConstraints() {
        super.updateConstraints()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        showAnimatedGradientSkeleton()
    }
}

// MARK: - ViewConfiguration
extension PromotionListSkeletonView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(tableHeader)
        addSubview(sectionHeader)
        addSubview(cellsStackView)
    }

    func setupConstraints() {
        tableHeader.layout {
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.top == compatibleSafeAreaLayoutGuide.topAnchor + Layout.Margin.size200
        }

        sectionHeader.layout {
            $0.top == tableHeader.bottomAnchor + Layout.Margin.tableHeaderToSectionHeader
            $0.leading == leadingAnchor + Layout.Margin.size400
            $0.trailing == trailingAnchor - Layout.Margin.sectionHeaderTrailing
            $0.height == Layout.Size.line
        }

        cellsStackView.layout {
            $0.top == sectionHeader.bottomAnchor + Layout.Margin.size300
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.bottom <= bottomAnchor - Layout.Margin.size200
        }
    }

    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - Nested types
private extension PromotionListSkeletonView {
    final class TableHeader: UIView, ViewConfiguration {
        private lazy var firstLineView: UIView = {
            let view = UIView()
            view.layer.cornerRadius = Layout.CornerRadius.line
            view.clipsToBounds = true
            view.isSkeletonable = true

            return view
        }()

        private lazy var secondLineView: UIView = {
            let view = UIView()
            view.layer.cornerRadius = Layout.CornerRadius.line
            view.clipsToBounds = true
            view.isSkeletonable = true

            return view
        }()

        private lazy var thirdLineView: UIView = {
            let view = UIView()
            view.layer.cornerRadius = Layout.CornerRadius.line
            view.clipsToBounds = true
            view.isSkeletonable = true

            return view
        }()

        // MARK: - Initialization
        convenience init() {
            self.init(frame: .zero)
        }

        override init(frame: CGRect) {
            super.init(frame: frame)

            buildLayout()
        }

        @available(*, unavailable)
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        func buildViewHierarchy() {
            addSubview(firstLineView)
            addSubview(secondLineView)
            addSubview(thirdLineView)
        }

        func setupConstraints() {
            firstLineView.layout {
                $0.top == topAnchor
                $0.trailing == trailingAnchor - Layout.TableHeader.Margin.firstLineTrailing
                $0.leading == leadingAnchor + Layout.Margin.size400
                $0.height == Layout.Size.line
            }

            secondLineView.layout {
                $0.top == firstLineView.bottomAnchor + Layout.Margin.size200
                $0.trailing == trailingAnchor - Layout.TableHeader.Margin.secondLineTrailing
                $0.leading == leadingAnchor + Layout.Margin.size400
                $0.height == Layout.Size.line
            }

            thirdLineView.layout {
                $0.top == secondLineView.bottomAnchor + Layout.Margin.size200
                $0.trailing == trailingAnchor - Layout.TableHeader.Margin.thirdLineTrailing
                $0.bottom == bottomAnchor
                $0.leading == leadingAnchor + Layout.Margin.size400
                $0.height == Layout.Size.line
            }
        }

        func configureViews() {
            backgroundColor = Colors.backgroundPrimary.color
        }

        override func layoutSubviews() {
            super.layoutSubviews()

            showAnimatedGradientSkeleton()
        }
    }
}

private extension PromotionListSkeletonView {
    final class Cell: UIView, ViewConfiguration {
        private lazy var containerView: UIView = {
            let view = UIView()
            view.layer.cornerRadius = Layout.Cell.CornerRadius.container
            view.layer.shadowColor = Colors.black.color.withAlphaComponent(0.1).cgColor
            view.layer.shadowOpacity = 1
            view.layer.shadowOffset = CGSize(width: 1.0, height: 2.0)
            view.layer.shadowRadius = 2
            view.layer.borderWidth = 0.1
            view.layer.borderColor = Colors.grayscale200.color.cgColor
            view.isSkeletonable = true

            return view
        }()

        private lazy var iconView: UIView = {
            let view = UIView()
            view.layer.cornerRadius = Layout.Cell.CornerRadius.icon
            view.clipsToBounds = true
            view.isSkeletonable = true

            return view
        }()

        private lazy var linesContainerView = UIView()

        private lazy var firstLineView: UIView = {
            let view = UIView()
            view.layer.cornerRadius = Layout.CornerRadius.line
            view.clipsToBounds = true
            view.isSkeletonable = true

            return view
        }()

        private lazy var secondLineView: UIView = {
            let view = UIView()
            view.layer.cornerRadius = Layout.CornerRadius.line
            view.clipsToBounds = true
            view.isSkeletonable = true

            return view
        }()

        private lazy var indicatorView: UIView = {
            let view = UIView()
            view.layer.cornerRadius = Layout.Cell.CornerRadius.indicator
            view.clipsToBounds = true
            view.isSkeletonable = true

            return view
        }()

        // MARK: - Initialization
        convenience init() {
            self.init(frame: .zero)
        }

        override init(frame: CGRect) {
            super.init(frame: frame)

            buildLayout()
        }

        @available(*, unavailable)
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        func buildViewHierarchy() {
            addSubview(containerView)

            containerView.addSubview(iconView)
            containerView.addSubview(linesContainerView)
            containerView.addSubview(indicatorView)

            linesContainerView.addSubview(firstLineView)
            linesContainerView.addSubview(secondLineView)
        }

        func setupConstraints() {
            containerView.layout {
                $0.top == topAnchor
                $0.trailing == trailingAnchor - Layout.Margin.size300
                $0.bottom == bottomAnchor
                $0.leading == leadingAnchor + Layout.Margin.size400
                $0.height == Layout.Cell.Size.container
            }

            iconView.layout {
                $0.width == Layout.Cell.Size.icon
                $0.height == Layout.Cell.Size.icon
                $0.centerY == containerView.centerYAnchor
                $0.top >= containerView.topAnchor + Layout.Margin.size300
                $0.bottom <= containerView.bottomAnchor - Layout.Margin.size300
                $0.leading == containerView.leadingAnchor + Layout.Margin.size300
            }

            linesContainerView.layout {
                $0.centerY == containerView.centerYAnchor
                $0.top >= containerView.topAnchor + Layout.Margin.size300
                $0.bottom <= containerView.bottomAnchor - Layout.Margin.size300
                $0.leading == iconView.trailingAnchor + Layout.Margin.size300
            }

            firstLineView.layout {
                $0.top == linesContainerView.topAnchor
                $0.leading == linesContainerView.leadingAnchor
                $0.trailing == linesContainerView.trailingAnchor
                $0.height == Layout.Size.line
            }

            secondLineView.layout {
                $0.top == firstLineView.bottomAnchor + Layout.Margin.size200
                $0.leading == linesContainerView.leadingAnchor
                $0.trailing == linesContainerView.trailingAnchor - Layout.Cell.Margin.secondLineTrailing
                $0.bottom == linesContainerView.bottomAnchor
                $0.height == Layout.Size.line
            }

            indicatorView.layout {
                $0.width == Layout.Cell.Size.indicator
                $0.height == Layout.Cell.Size.indicator
                $0.centerY == containerView.centerYAnchor
                $0.leading == linesContainerView.trailingAnchor + Layout.Cell.Margin.indicatorTrailing
                $0.trailing == containerView.trailingAnchor - Layout.Margin.size300
            }
        }

        func configureViews() {
            backgroundColor = Colors.backgroundPrimary.color
        }

        override func layoutSubviews() {
            super.layoutSubviews()

            showAnimatedGradientSkeleton()

            firstLineView.showAnimatedGradientSkeleton()
            secondLineView.showAnimatedGradientSkeleton()
        }
    }
}
