import Lottie
import UI
import UIKit
import AssetsKit

extension PromotionListEmptyView.Layout {
    enum Size {
        static let image = CGSize(width: 220, height: 245)
    }
}

final class PromotionListEmptyView: UIView {
    private typealias Localizable = Strings.Promotions
    
    // MARK: - Properties
    fileprivate enum Layout {}
    
    private lazy var emptyImageView: UIImageView = {
        let image = UIImageView()
        image.image = Resources.Illustrations.iluEmpty.image
        return image
    }()
    
    private lazy var messageTitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textAlignment, .center)
        label.text = Localizable.noPromotionsTitle
        return label
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle())
            .with(\.textAlignment, .center)
            .with(\.textColor, .grayscale600())
        label.text = Localizable.noPromotionsDescription
        return label
    }()
    
    // MARK: - Initialization
    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration
extension PromotionListEmptyView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(emptyImageView, messageTitleLabel, messageLabel)
    }
    
    func setupConstraints() {
        emptyImageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base11)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.image)
        }
        
        messageTitleLabel.snp.makeConstraints {
            $0.top.equalTo(emptyImageView.snp.bottom).offset(Spacing.base03)
            $0.trailing.leading.equalToSuperview().inset(Spacing.base02)
            $0.centerX.equalToSuperview()
        }
        
        messageLabel.snp.makeConstraints {
            $0.top.equalTo(messageTitleLabel.snp.bottom)
            $0.trailing.leading.equalToSuperview().inset(Spacing.base02)
            $0.centerX.equalToSuperview()
        }
    }
}
