import UI

protocol PromotionListDisplay: AnyObject {
    typealias ErrorData = (
        image: UIImage?,
        content: (title: String, description: String),
        button: (image: UIImage?, title: String)
    )

    func list(_ promotions: [Promotions])
    func showLocationWarning()
    func hideLocationWarning()
    func show(error data: ErrorData)
}

protocol EmptyStateDisplay: AnyObject {
    func showEmptyState()
    func hideEmptyState()
}
