import AssetsKit
import UI
import UIKit

final class PromotionListViewController: ViewController<PromotionListViewModelInputs, UIView> {
    private typealias Localizable = Strings.Promotions

    // MARK: - Properties
    private var data: [Promotions] = [] {
        didSet {
            tableView.reloadData()
        }
    }

    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.separatorStyle = .none
        tableView.register(PromotionListViewGlobalCell.self,
                           forCellReuseIdentifier: PromotionListViewGlobalCell.identifier)
        tableView.register(PromotionListViewEstablishmentCell.self,
                           forCellReuseIdentifier: PromotionListViewEstablishmentCell.identifier)
        tableView.register(PromotionListSectionHeaderView.self,
                           forHeaderFooterViewReuseIdentifier: PromotionListSectionHeaderView.identifier)
        tableView.rowHeight = 104.0
        tableView.sectionFooterHeight = .leastNonzeroMagnitude
        tableView.backgroundColor = .clear
        tableView.dataSource = self
        tableView.delegate = self

        return tableView
    }()

    private var dataSource: TableViewHandler<String, Promotion, PromotionListViewGlobalCell>?

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        updateNavigationBarAppearance()
        loadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }

        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.navigationBar.setNeedsDisplay()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        setupTableHeaderAndFooterView()
    }

    // MARK: - Setup
    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }

    override func setupConstraints() {
        tableView.layout {
            $0.trailing == view.trailingAnchor
            $0.bottom == view.bottomAnchor
            $0.leading == view.leadingAnchor
            $0.top == view.compatibleSafeAreaLayoutGuide.topAnchor
        }
    }

    override func configureViews() {
        title = Localizable.promotions
        view.backgroundColor = Colors.backgroundPrimary.color

        navigationItem.backBarButtonItem = UIBarButtonItem(
            title: DefaultLocalizable.back.text,
            style: .plain,
            target: nil,
            action: nil
        )
    }
}

// MARK: - PromotionListDisplay
extension PromotionListViewController: PromotionListDisplay {
    func list(_ promotions: [Promotions]) {
        data = promotions
        endState()
    }

    func show(error data: ErrorData) {
        endState(
            model: StatefulErrorViewModel(
                image: data.image,
                content: (title: data.content.title, description: data.content.description),
                button: (image: data.button.image, title: data.button.title)
            )
        )
    }

    func showLocationWarning() {
        let tableFooterView = PromotionListTableFooterView()
        tableFooterView.authorizeNowAction = { [weak self] in
            self?.viewModel.requestLocationAuthorization { [weak self] in
                self?.loadData()
            }
        }

        tableView.tableFooterView = tableFooterView
    }

    func hideLocationWarning() {
        tableView.tableFooterView = UIView(frame: .zero)
    }
}

// MARK: - EmptyStateDisplay
extension PromotionListViewController: EmptyStateDisplay {
    func showEmptyState() {
        tableView.tableHeaderView = nil
        tableView.backgroundView = PromotionListEmptyView()
    }

    func hideEmptyState() {
        tableView.tableHeaderView = PromotionListTableHeaderView()

        tableView.backgroundView = nil

        setupTableHeaderAndFooterView()
    }
}

// MARK: - StatefulTransitionViewing
extension PromotionListViewController: StatefulTransitionViewing {
    func didTryAgain() {
        loadData()
    }

    func statefulViewForLoading() -> StatefulViewing {
        PromotionListSkeletonView()
    }
}

extension PromotionListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        data.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data[section].items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = data[indexPath.section]
        let item = section.items[indexPath.row]

        switch section.category {
        case .generic:
            let identifier = PromotionListViewGlobalCell.identifier
            guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier,
                                                           for: indexPath) as? PromotionListViewGlobalCell else {
                return UITableViewCell()
            }

            cell.setup(with: item)

            return cell

        case .location:
            let identifier = PromotionListViewEstablishmentCell.identifier
            guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier,
                                                           for: indexPath) as? PromotionListViewEstablishmentCell else {
                return UITableViewCell()
            }

            cell.setup(with: item)

            return cell
        }
    }
}

extension PromotionListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard
            let headerView = tableView
                .dequeueReusableHeaderFooterView(withIdentifier: PromotionListSectionHeaderView.identifier)
            as? PromotionListSectionHeaderView
            else {
                return nil
        }

        headerView.setup(with: data[section].title)

        return headerView
    }

    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        UITableView.automaticDimension
    }

    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        tableView.sectionFooterHeight
    }

    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        50.0
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = data[indexPath.section].items[indexPath.row]
        viewModel.didSelect(item)
    }
}

// MARK: - Private
private extension PromotionListViewController {
    func updateNavigationBarAppearance() {
        navigationController?.navigationBar.tintColor = Colors.branding300.color
        navigationController?.navigationBar.barTintColor = Colors.backgroundPrimary.color

        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: Assets.Icons.iconInfo.image,
            style: .plain,
            target: self,
            action: #selector(didTapAtInfoButton)
        )

        if #available(iOS 11, *) {
            extendedLayoutIncludesOpaqueBars = true
        }
    }

    func setupTableHeaderAndFooterView() {
        if let headerView = tableView.tableHeaderView {
            updateTableHeaderOrFooterHeight(headerView)
            tableView.tableHeaderView = headerView
        }

        if let footerView = tableView.tableFooterView {
            updateTableHeaderOrFooterHeight(footerView)
            tableView.tableFooterView = footerView
        }

        tableView.layoutIfNeeded()
    }

    func updateTableHeaderOrFooterHeight(_ headerOrFooter: UIView) {
        let targetSize = CGSize(width: headerOrFooter.bounds.width, height: UIView.layoutFittingCompressedSize.height)
        let size = headerOrFooter.systemLayoutSizeFitting(targetSize)

        guard headerOrFooter.frame.height != size.height else {
            return
        }

        headerOrFooter.frame.size.height = size.height
    }

    func loadData() {
        beginState()
        viewModel.loadData()
    }

    @objc
    func didTapAtInfoButton() {
        viewModel.openOnboarding()
    }
}
