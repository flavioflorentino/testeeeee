import UI
import UIKit

protocol PromotionListNewDisplay: AnyObject {
    func displaySomething()
}

private extension PromotionListNewViewController.Layout {
    enum Height {
        static let tableViewRow: CGFloat = 104.0
    }
}

final class PromotionListNewViewController: ViewController<PromotionListNewInteracting, UIView> {
    fileprivate enum Layout { }
    
    enum Section {
        case main
    }
    
    private lazy var tableViewDataSource: TableViewDataSource<Section, PromotionViewCellModel> = {
        let dataSource = TableViewDataSource<Section, PromotionViewCellModel>(view: tableView)
        dataSource.add(section: .main)
        dataSource.itemProvider = { tableView, indexPath, item -> PromotionListViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: PromotionListViewCell.identifier, for: indexPath) as? PromotionListViewCell
            cell?.setup(with: item)
            return cell
        }
        return dataSource
    }()
    
    private lazy var tableHeaderView = PromotionListTableHeaderView()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.separatorStyle = .none
        tableView.register(PromotionListViewCell.self, forCellReuseIdentifier: PromotionListViewCell.identifier)
        tableView.estimatedRowHeight = Layout.Height.tableViewRow
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionFooterHeight = CGFloat.leastNonzeroMagnitude
        tableView.backgroundColor = Colors.white.color
        
        return tableView
    }()
    
    private lazy var infoBarButton: UIBarButtonItem = {
        let item = UIBarButtonItem(
            image: Assets.Icons.iconInfo.image,
            style: .plain,
            target: self,
            action: #selector(didTapAtInfoButton)
        )
        item.tintColor = Colors.branding600.color
        return item
    }()

    // MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        updateNavigationBarAppearance()
        interactor.doSomething()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }

        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.navigationBar.setNeedsDisplay()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        setupTableHeaderView()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }

    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.trailing.bottom.leading.equalToSuperview()
            $0.top.equalTo(view.compatibleSafeArea.top)
            $0.bottom.equalToSuperview()
        }
    }

    override func configureViews() {
        view.backgroundColor = Colors.white.color
        tableView.dataSource = tableViewDataSource
        tableView.delegate = self
    }
}

// MARK: Private functions
private extension PromotionListNewViewController {
    func updateNavigationBarAppearance() {
        navigationController?.navigationBar.tintColor = Colors.branding600.color
        navigationController?.navigationBar.barTintColor = Colors.white.color
        navigationItem.rightBarButtonItem = infoBarButton

        if #available(iOS 11, *) {
            extendedLayoutIncludesOpaqueBars = true
        }
    }
    
    func setupTableHeaderView() {
        if let headerView = tableView.tableHeaderView {
            updateTableHeaderHeight(headerView)
            tableView.tableHeaderView = headerView
        }

        tableView.layoutIfNeeded()
    }
    
    func updateTableHeaderHeight(_ header: UIView) {
        let targetSize = CGSize(width: header.bounds.width, height: UIView.layoutFittingCompressedSize.height)
        let size = header.systemLayoutSizeFitting(targetSize)
        guard header.frame.height != size.height else {
            return
        }
        header.frame.size.height = size.height
    }
    
    @objc
    func didTapAtInfoButton() {
        // todo next PR
        // openOnboarding()
    }
}

// MARK: PromotionListNewDisplay
extension PromotionListNewViewController: PromotionListNewDisplay {
    func displaySomething() {
        tableView.tableHeaderView = tableHeaderView
        setupTableHeaderView()
    }
}

// MARK: - TableViewDelegate
extension PromotionListNewViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // todo next PR
        // didSelect(at: indexPath)
    }
}
