import Foundation

// swiftlint:disable function_parameter_count
protocol PromotionListNewFactory {
    func makeNewListController(
        promotion: Promotion?,
        openWebView: @escaping (URL?) -> Void,
        openDeepLink: @escaping (URL?) -> Void,
        openOnboarding: @escaping () -> Void,
        openDetails: @escaping (Promotion, PromotionDetailsIDType) -> Void,
        openPromotions: @escaping (Promotion) -> Void
    ) -> PromotionListNewViewController
}
