import Foundation

protocol PromotionListNewPresenting: AnyObject {
    func displaySomething()
}

final class PromotionListNewPresenter {
    private typealias Localizable = Strings.Promotions
    typealias Display = PromotionListNewDisplay
    
    // MARK: - Properties
    weak var viewController: Display?
}

// MARK: - PromotionListNewPresenting
extension PromotionListNewPresenter: PromotionListNewPresenting {
    func displaySomething() {
        viewController?.displaySomething()
    }
}
