import Foundation

protocol PromotionListNewInteracting: AnyObject {
    func doSomething()
}

final class PromotionListNewInteractor {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    private let service: PromotionsServicing
    private let presenter: PromotionListNewPresenting

    init(service: PromotionsServicing, presenter: PromotionListNewPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - PromotionListNewViewModelInputs
extension PromotionListNewInteractor: PromotionListNewInteracting {
    func doSomething() {
        presenter.displaySomething()
    }
}
