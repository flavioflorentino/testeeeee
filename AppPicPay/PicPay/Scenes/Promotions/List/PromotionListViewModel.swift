import AnalyticsModule
import Core
import FeatureFlag
import Foundation
import PermissionsKit

protocol PromotionListViewModelInputs: AnyObject {
    func loadData()
    func didSelect(_ promotion: Promotion)

    func openOnboarding()
    func requestLocationAuthorization(_ tryAgainAction: (() -> Void)?)
}

final class PromotionListViewModel {
    typealias Dependencies = HasLocationManager & HasAnalytics & HasFeatureManager
    private typealias Localizable = Strings.Promotions

    // MARK: - Properties
    private let service: PromotionsServicing
    private let presenter: PromotionListPresenting
    private let dependencies: Dependencies

    // MARK: - Initialization
    init(service: PromotionsServicing, presenter: PromotionListPresenting) {
        self.service = service
        self.presenter = presenter
        self.dependencies = DependencyContainer()
    }
}

// MARK: - PromotionListViewModelInputs
extension PromotionListViewModel: PromotionListViewModelInputs {
    func loadData() {
        guard
            Permission.locationWhenInUse.status == .authorized,
            let coordinate = dependencies.locationManager.lastAvailableLocation?.coordinate
            else {
                return loadPromotions()
        }

        loadPromotions(for: coordinate)
    }

    func didSelect(_ promotion: Promotion) {
        Analytics.shared.log(PromotionsEvent.promotionDetailAccessed(
            type: promotion.tracking.type,
            origin: promotion.tracking.origin)
        )

        switch promotion.action {
        case let .webView(url):
            presenter.showDetail(url)
        case let .deeplink(data):
            presenter.open(deepLink: URL(string: data))
        case let .detail(data):
            presenter.showDetails(promotion: promotion, idType: .seller(id: data))
        default:
            return
        }
    }

    func requestLocationAuthorization(_ tryAgainAction: (() -> Void)?) {
        dependencies.analytics.log(PromotionsEvent.promotionLocationPermission)
        let permission = Permission.locationWhenInUse
        permission.updateAlertToRequest()

        permission.request { _ in
            tryAgainAction?()
        }
    }

    func openOnboarding() {
        presenter.openOnboarding()
    }
}

// MARK: - Private
private extension PromotionListViewModel {
    func loadPromotions(for coordinate: CLLocationCoordinate2D) {
        service.promotions(latitude: coordinate.latitude, longitude: coordinate.longitude) { [weak self] result in
            self?.handle(result, showsWarning: false)
        }
    }

    func loadPromotions() {
        service.promotions { [weak self] result in
            self?.handle(result, showsWarning: true)
        }
    }

    func handle(_ result: PromotionsServicing.PromotionsResult, showsWarning: Bool) {
        switch result {
        case let .success(promotions):
            let customPromotions = handlePromotions(promotions: promotions)
            presenter.list(customPromotions, showsLocationWarning: showsWarning)
        case let .failure(error):
            presenter.show(error: error)
        }
    }
    
    func handlePromotions(promotions: [Promotions]) -> [Promotions] {
        guard dependencies.featureManager.isActive(.experimentNewAccessPromotionsMgmBool) else {
            return promotions
        }
        
        var handledPromotions: [Promotions] = []

        if let invoiceSection = getInvoiceCashbackSectionIfAvailable() {
            handledPromotions.append(invoiceSection)
        }

        let mgmPromoTracking = Promotion.Tracking(type: Localizable.mgmTrackingType,
                                                  origin: Localizable.mgmTrakcingOrigin)

        let mgmItem = Promotion(title: nil,
                                description: Localizable.mgmItemDescription,
                                imageUrl: nil,
                                badge: nil,
                                distance: nil,
                                action: .deeplink(data: Localizable.mgmItemDeeplink),
                                tracking: mgmPromoTracking)

        let mgmSection = Promotions(title: Localizable.mgmSectionTitle, category: .generic, items: [mgmItem])

        handledPromotions.append(mgmSection)
        handledPromotions.append(contentsOf: promotions)

        return handledPromotions
    }

    func getInvoiceCashbackSectionIfAvailable() -> Promotions? {
        guard dependencies.featureManager.isActive(.isInvoiceCashbackPromotionAvailable) else {
            return nil
        }

        let invoicePromoTracking = Promotion.Tracking(type: Localizable.invoiceTrackingType,
                                                      origin: Localizable.invoiceTrakcingOrigin)

        let invoiceItem = Promotion(title: nil,
                                    description: Localizable.invoiceDescription,
                                    imageUrl: nil,
                                    badge: nil,
                                    distance: nil,
                                    action: .deeplink(data: Localizable.invoiceDeepLink),
                                    tracking: invoicePromoTracking)

        let invoiceSection = Promotions(title: "", category: .generic, items: [invoiceItem])

        return invoiceSection
    }
}

private extension Permission {
    private var alert: PermissionAlert? {
        switch status {
        case .denied:
            return deniedAlert
        case .disabled:
            return disabledAlert
        default:
            return nil
        }
    }

    func updateAlertToRequest() {
        guard type == .locationWhenInUse else {
            return
        }

        let titles: [PermissionStatus: String] = [
            .denied: Strings.Promotions.locationWarningDeniedTitle,
            .disabled: Strings.Promotions.locationWarningDisabledTitle
        ]

        let messages: [PermissionStatus: String] = [
            .denied: Strings.Promotions.locationWarningDeniedMessage,
            .disabled: Strings.Promotions.locationWarningDisabledMessage
        ]

        alert?.title = titles[status]
        alert?.message = messages[status]
    }
}
