import Core
import UI

protocol PromotionListPresenting {
    func list(_ promotions: [Promotions], showsLocationWarning: Bool)
    func showDetail(_ url: URL?)
    func showDetails(promotion: Promotion, idType: PromotionDetailsIDType)
    func show(error: ApiError)
    func open(deepLink: URL?)
    func openOnboarding()
}

extension PromotionListPresenting {
    func list(_ promotions: [Promotions]) {
        list(promotions, showsLocationWarning: false)
    }
}

final class PromotionListPresenter {
    typealias Display = PromotionListDisplay & EmptyStateDisplay
    private typealias Localizable = Strings.Promotions

    // MARK: - Properties
    weak var display: Display?
    var openWebViewAction: ((URL?) -> Void)?
    var openDeepLinkAction: ((URL?) -> Void)?
    var openDetailsAction: ((Promotion, PromotionDetailsIDType) -> Void)?
    var openOnboardingAction: (() -> Void)?
}

// MARK: - PromotionListPresenting
extension PromotionListPresenter: PromotionListPresenting {
    func list(_ promotions: [Promotions], showsLocationWarning: Bool) {
        display?.hideLocationWarning()

        guard promotions.isNotEmpty else {
            display?.list([Promotions]())
            display?.showEmptyState()
            return
        }

        display?.hideEmptyState()
        display?.list(promotions)

        guard showsLocationWarning else {
            return
        }

        display?.showLocationWarning()
    }

    func show(error: ApiError) {
        display?.show(error: errorData(from: error))
    }

    func showDetail(_ url: URL?) {
        openWebViewAction?(url)
    }

    func open(deepLink: URL?) {
        openDeepLinkAction?(deepLink)
    }

    func openOnboarding() {
        openOnboardingAction?()
    }

    func showDetails(promotion: Promotion, idType: PromotionDetailsIDType) {
        openDetailsAction?(promotion, idType)
    }
}

// MARK: - Private
private extension PromotionListPresenter {
    func errorData(from error: ApiError) -> PromotionListDisplay.ErrorData {
        switch error {
        case .connectionFailure:
            return (
                image: Assets.iluSearchErrorCon.image,
                content: (
                    title: Localizable.noInternetConnectionError,
                    description: Localizable.checkYourInternetConnection
                ),
                button: (
                    image: Assets.Icons.icoRefresh.image,
                    title: DefaultLocalizable.tryAgain.text
                )
            )
        default:
            return (
                image: Assets.CreditPicPay.Registration.sad3.image,
                content: (
                    title: Localizable.unknownErrorTitle,
                    description: Localizable.weAreDownError
                ),
                button: (
                    image: Assets.Icons.icoRefresh.image,
                    title: DefaultLocalizable.tryAgain.text
                )
            )
        }
    }
}
