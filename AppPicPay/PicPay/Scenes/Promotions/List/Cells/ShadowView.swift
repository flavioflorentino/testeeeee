import UI

extension ShadowView.Layout {
    enum Container {
        static let shadowRadius: CGFloat = 2.0
        static let shadowOffset = CGSize(width: 1.0, height: 2.0)
        static let shadowOpacity: Float = 1.0
        static let borderWidth: CGFloat = 0.1
    }
}

final class ShadowView: UIView {
    fileprivate enum Layout { }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.dropShadow(
            color: Colors.black.color.withAlphaComponent(0.1),
            opacity: Layout.Container.shadowOpacity,
            offSet: Layout.Container.shadowOffset,
            radius: Layout.Container.shadowRadius,
            scale: true
        )
    }
}
