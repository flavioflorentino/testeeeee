import AssetsKit
import UI
import UIKit

final class PromotionListViewGlobalCell: UITableViewCell {
    private typealias Localizable = Strings.Promotions

    // MARK: - Properties
    private lazy var containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 4
        view.layer.shadowColor = Colors.black.color.withAlphaComponent(0.1).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize(width: 1.0, height: 2.0)
        view.layer.shadowRadius = 2
        view.layer.borderWidth = 0.1
        view.layer.borderColor = Colors.grayscale400.color.cgColor

        return view
    }()

    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = Layout.iconSize / 2.0
        imageView.clipsToBounds = true

        return imageView
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: Layout.descriptionFontSize, weight: .regular)
        label.numberOfLines = 0
        label.textColor = Colors.grayscale700.color

        return label
    }()

    private lazy var arrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.Icons.rightArrow.image.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = Colors.branding400.color

        return imageView
    }()

    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Reusing
    override func prepareForReuse() {
        super.prepareForReuse()

        iconImageView.cancelRequest()
        iconImageView.image = nil

        update(description: nil)
    }

    // MARK: - Hit testing
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let convertedPoint = convert(point, to: containerView)
        return containerView.hitTest(convertedPoint, with: event)
    }

    func setup(with promotion: Promotion) {
        iconImageView.setImage(
            url: promotion.imageUrl,
            placeholder: Assets.Icons.icoPromotionsPlaceholder.image
        )

        update(description: promotion.description)
        checkIfPromotionIsInviteOrInvoice(promotion: promotion)
    }
}

extension PromotionListViewGlobalCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(containerView)

        containerView.addSubview(iconImageView)
        containerView.addSubview(descriptionLabel)
        containerView.addSubview(arrowImageView)
    }

    func setupConstraints() {
        containerView.layout {
            $0.top == contentView.topAnchor + Layout.margin000
            $0.trailing == contentView.trailingAnchor - Layout.margin300
            $0.bottom == contentView.bottomAnchor - Layout.margin000
            $0.leading == contentView.leadingAnchor + Layout.margin400
        }

        iconImageView.layout {
            $0.width == Layout.iconSize
            $0.height == Layout.iconSize
            $0.centerY == containerView.centerYAnchor
            $0.top >= containerView.topAnchor + Layout.margin300
            $0.bottom <= containerView.bottomAnchor - Layout.margin300
            $0.leading == containerView.leadingAnchor + Layout.margin300
        }

        descriptionLabel.layout {
            $0.centerY == containerView.centerYAnchor
            $0.top >= containerView.topAnchor + Layout.margin300
            $0.bottom <= containerView.bottomAnchor - Layout.margin300
            $0.leading == iconImageView.trailingAnchor + Layout.margin300
        }

        arrowImageView.layout {
            $0.width == Layout.arrowSize.width
            $0.height == Layout.arrowSize.height
            $0.centerY == containerView.centerYAnchor
            $0.leading >= descriptionLabel.trailingAnchor + Layout.margin200
            $0.trailing == containerView.trailingAnchor - Layout.margin300
        }
    }

    func configureViews() {
        layoutMargins = .zero
        selectionStyle = .none
        backgroundColor = .clear
    }
}

// MARK: - Private
private extension PromotionListViewGlobalCell {
    enum Layout {
        static let descriptionFontSize: CGFloat = 14.0

        static let iconSize: CGFloat = 48.0
        static let arrowRightMargin: CGFloat = 19.0
        static let arrowSize = CGSize(width: 8.0, height: 13.0)

        static let margin000: CGFloat = 4.0
        static let margin100: CGFloat = 8.0
        static let margin200: CGFloat = 12.0
        static let margin300: CGFloat = 16.0
        static let margin400: CGFloat = 20.0
    }

    func update(description: String?) {
        guard let description = description else {
            return descriptionLabel.attributedText = nil
        }

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.26

        let attributedText = NSAttributedString(
            string: description,
            attributes: [.paragraphStyle: paragraphStyle])

        descriptionLabel.attributedText = attributedText
    }
    
    func checkIfPromotionIsInviteOrInvoice(promotion: Promotion) {
        if promotion.tracking.type == Localizable.mgmTrackingType {
            iconImageView.setImage(url: promotion.imageUrl,
                                   placeholder: Resources.Illustrations.iluSocial1.image)
        } else if promotion.tracking.type == Localizable.invoiceTrackingType {
            iconImageView.layer.cornerRadius = 0
            iconImageView.size = CGSize(width: Layout.iconSize / 2.0, height: Layout.iconSize / 2.0)
            iconImageView.contentMode = .scaleAspectFit
            iconImageView.setImage(url: promotion.imageUrl, placeholder: Resources.Icons.icoInvoicePromotion.image)
        }
    }
}
