import UI
import UIKit

extension PromotionListViewCell.Layout {
    enum Container {
        static let shadowRadius: CGFloat = 2.0
        static let shadowOffset = CGSize(width: 1.0, height: 2.0)
        static let shadowOpacity: Float = 1.0
        static let borderWidth: CGFloat = 0.1
    }
    
    enum Size {
        static let arrow = CGSize(width: 8.0, height: 13.0)
        static let pin = CGSize(width: 10.0, height: 10.0)
        static let warningIcon = CGSize(width: 24.0, height: 24.0)
    }
    
    enum Margin {
        static let largeTitleLeading: CGFloat = 20.0
    }
}

final class PromotionListViewCell: UITableViewCell {
    private typealias Localizable = Strings.Promotions
    
    fileprivate enum Layout { }
    
    private lazy var containerView: ShadowView = {
        let view = ShadowView()
        view
            .viewStyle(RoundedViewStyle(cornerRadius: .light))
            .with(\.backgroundColor, .groupedBackgroundSecondary())
        view.layer.borderWidth = Layout.Container.borderWidth
        view.layer.borderColor = Colors.grayscale200.color.cgColor
        
        return view
    }()
    
    private lazy var containerStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fill
        stack.alignment = .center
        stack.spacing = Spacing.base02
        
        return stack
    }()
    
    private lazy var primaryContentStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.alignment = .center
        stack.spacing = Spacing.base02
        
        return stack
    }()
    
    // MARK: - image
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView
            .imageStyle(RoundedImageStyle(size: .medium, cornerRadius: .full))
            .with(\.backgroundColor, .white(.light))
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = Assets.NewGeneration.avatarPlace.image
        
        return imageView
    }()
    
    // MARK: - content
    private lazy var containerInformationStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .leading
        stack.distribution = .fill
        stack.spacing = Spacing.base00
        
        return stack
    }()
    
    private lazy var informationStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .leading
        stack.distribution = .fill
        stack.spacing = Spacing.base00
        
        return stack
    }()
    
    private lazy var badgeContainerView: UIView = {
        let view = UIView()
        view
            .viewStyle(RoundedViewStyle(cornerRadius: .light))
            .with(\.backgroundColor, .neutral600())
        
        return view
    }()
    
    private lazy var badgeLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .white(.light))
        label.numberOfLines = 1
        
        return label
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .black(.light))
        label.numberOfLines = 1
        
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
        label.numberOfLines = 1
        
        return label
    }()
    
    private lazy var distanceStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.distribution = .fill
        stack.spacing = Spacing.base00
        stack.isHidden = true
        
        return stack
    }()
    
    private lazy var placeLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale400())
        label.numberOfLines = 1
        label.text = Localizable.place
        
        return label
    }()
    
    private lazy var placePinIconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.Search.stablishment.image.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = Colors.grayscale400.color
        
        return imageView
    }()
    
    private lazy var distanceLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale400())
        label.numberOfLines = 1
        
        return label
    }()
    
    // MARK: - Warning
    private lazy var warningView: UIView = {
        let view = UIView()
        view
            .viewStyle(RoundedViewStyle(cornerRadius: .light))
            .with(\.backgroundColor, .grayscale050())
        view.isHidden = true
        
        return view
    }()
    
    private lazy var warningContainterStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.alignment = .center
        stack.spacing = Spacing.base02
        
        return stack
    }()
    
    private lazy var warningIconLabel: UILabel = {
        let label = UILabel()
        label.text = Iconography.exclamationTriangle.rawValue
        label
            .labelStyle(IconLabelStyle(type: .medium))
            .with(\.textColor, .warning600())
        
        return label
    }()
    
    private lazy var warningInformationStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .fill
        stack.distribution = .fill
        stack.spacing = Spacing.base01
        
        return stack
    }()
    
    private lazy var warningDescriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle())
        
        return label
    }()
    
    private lazy var warningLinkLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(LinkSecondaryLabelStyle())
            .with(\.textColor, .branding600())
        
        return label
    }()
    
    // MARK: - Arrow
    private lazy var arrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.Icons.rightArrow.image.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = Colors.branding600.color
        
        return imageView
    }()
    
    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Reusing
    override func prepareForReuse() {
        super.prepareForReuse()
        
        iconImageView.cancelRequest()
        iconImageView.image = nil
        
        badgeContainerView.isHidden = false
        warningView.isHidden = true
        badgeContainerView.backgroundColor = Colors.neutral600.color
        badgeLabel.textColor = Colors.white.lightColor
    }
    
    // MARK: - Setup
    func setup(with promotion: PromotionViewCellModel) {
        iconImageView.setImage(
            url: promotion.image,
            placeholder: Assets.Icons.icoPromotionsPlaceholder.image
        )

        titleLabel.text = promotion.title
        subtitleLabel.text = promotion.description
        subtitleLabel.numberOfLines = 0
        
        badgeLabel.text = promotion.badge
        
        if let distance = promotion.distance {
            distanceStack.isHidden = false
            distanceLabel.text = distance
        }
        
        if let warningDescription = promotion.permission?.description, let warningLink = promotion.permission?.link {
            warningView.isHidden = false
            badgeContainerView.backgroundColor = Colors.warning600.color
            badgeLabel.textColor = Colors.black.lightColor
            warningDescriptionLabel.text = warningDescription
            warningLinkLabel.text = warningLink
        }
    }
    
    // MARK: - Custom Highlighted
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        self.containerView.backgroundColor = highlighted ? .clear : Colors.groupedBackgroundSecondary.color
    }
}

extension PromotionListViewCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(containerView)
        containerView.addSubview(containerStack)
        containerStack.addArrangedSubviews(primaryContentStack, warningView)
        warningView.addSubview(warningContainterStack)
        warningContainterStack.addArrangedSubviews(warningIconLabel, warningInformationStack)
        warningInformationStack.addArrangedSubviews(warningDescriptionLabel, warningLinkLabel)
        primaryContentStack.addArrangedSubviews(iconImageView, containerInformationStack, arrowImageView)
        containerInformationStack.addArrangedSubviews(informationStack, distanceStack)
        informationStack.addArrangedSubviews(badgeContainerView, titleLabel, subtitleLabel)
        badgeContainerView.addSubview(badgeLabel)
        distanceStack.addArrangedSubviews(placeLabel, placePinIconImageView, distanceLabel)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base00)
            $0.bottom.equalToSuperview().offset(-Spacing.base00)
            $0.leading.equalToSuperview().offset(Layout.Margin.largeTitleLeading)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        containerStack.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base02)
        }
        
        warningView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
        }
        
        warningContainterStack.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Spacing.base02)
        }
        
        warningIconLabel.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.warningIcon)
        }
    }
    
    func configureViews() {
        layoutMargins = .zero
        selectionStyle = .none
        backgroundColor = .clear
    }
}
