import UI
import UIKit

// MARK: - Layout
private extension PromotionListViewEstablishmentCell.Layout {
    enum Font {
        static let name: UIFont = .systemFont(ofSize: 14.0, weight: .medium)
        static let address: UIFont = .systemFont(ofSize: 12.0)
        static let distance: UIFont = .systemFont(ofSize: 10.0)
        static let badge: UIFont = .systemFont(ofSize: 10.0, weight: .semibold)
    }

    enum Margin {
        static let size00: CGFloat = 2.0
        static let largeTitleLeading: CGFloat = 20.0
    }

    enum Size {
        static let icon: CGFloat = 48.0
        static let badge: CGFloat = 16.0
        static let placePin = CGSize(width: 9.18, height: 11.08)
        static let arrow = CGSize(width: 8.0, height: 13.0)
    }

    enum Container {
        static let cornerRadius: CGFloat = 4.0
        static let shadowRadius: CGFloat = 2.0
        static let shadowOffset = CGSize(width: 1.0, height: 2.0)
        static let shadowOpacity: Float = 1.0
        static let borderWidth: CGFloat = 0.1
    }
}

final class PromotionListViewEstablishmentCell: UITableViewCell {
    fileprivate enum Layout { }

    // MARK: - Properties
    private lazy var containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = Layout.Container.cornerRadius
        view.layer.shadowColor = Colors.black.color.withAlphaComponent(0.1).cgColor
        view.layer.shadowOpacity = Layout.Container.shadowOpacity
        view.layer.shadowOffset = Layout.Container.shadowOffset
        view.layer.shadowRadius = Layout.Container.shadowRadius
        view.layer.borderWidth = Layout.Container.borderWidth
        view.layer.borderColor = Colors.grayscale200.color.cgColor

        return view
    }()

    private let badgeContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.branding400.color
        view.roundCorners([.layerMinXMinYCorner, .layerMinXMaxYCorner], radius: 3.0)

        return view
    }()

    private let badgeLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = Colors.branding400.color
        label.font = Layout.Font.badge
        label.numberOfLines = 1
        label.textColor = Colors.white.color
        label.textAlignment = .center

        return label
    }()

    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = Layout.Size.icon / 2.0
        imageView.clipsToBounds = true
        imageView.layer.borderWidth = 1.0
        imageView.layer.borderColor = Colors.grayscale200.color.cgColor

        return imageView
    }()

    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.name
        label.numberOfLines = 1
        label.textColor = Colors.black.color

        return label
    }()

    private lazy var addressLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.address
        label.numberOfLines = 1
        label.textColor = Colors.grayscale700.color

        return label
    }()

    private lazy var placeLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.address
        label.numberOfLines = 1
        label.textColor = Colors.grayscale400.color
        label.text = "Local"

        return label
    }()

    private lazy var placePinImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.Search.stablishment.image.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = Colors.grayscale400.color

        return imageView
    }()

    private lazy var distanceLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.distance
        label.numberOfLines = 1
        label.textColor = Colors.grayscale700.color

        return label
    }()

    private lazy var arrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.Icons.rightArrow.image.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = Colors.branding400.color

        return imageView
    }()

    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Reusing
    override func prepareForReuse() {
        super.prepareForReuse()

        iconImageView.cancelRequest()
        iconImageView.image = nil

        badgeContainerView.isHidden = false
    }

    // MARK: - Hit testing
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let convertedPoint = convert(point, to: containerView)
        return containerView.hitTest(convertedPoint, with: event)
    }

    func setup(with promotion: Promotion) {
        iconImageView.setImage(
            url: promotion.imageUrl,
            placeholder: Assets.Icons.icoPromotionsPlaceholder.image
        )

        nameLabel.text = promotion.title
        addressLabel.text = promotion.description

        badgeContainerView.isHidden = promotion.badge == nil
        badgeLabel.text = promotion.badge

        badgeContainerView.setNeedsLayout()
        badgeContainerView.layoutIfNeeded()

        if let distance = promotion.distance {
            distanceLabel.text = "• \(distance)"
        } else {
            distanceLabel.text = nil
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        badgeContainerView.roundCorners([.layerMinXMinYCorner, .layerMinXMaxYCorner], radius: 3.0)
    }
}

extension PromotionListViewEstablishmentCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(containerView)

        containerView.addSubviews(
            iconImageView,
            nameLabel,
            addressLabel,
            placeLabel,
            placePinImageView,
            distanceLabel,
            badgeContainerView,
            arrowImageView
        )

        badgeContainerView.addSubview(badgeLabel)
    }

    // swiftlint:disable:next function_body_length
    func setupConstraints() {
        containerView.layout {
            $0.top == contentView.topAnchor + Spacing.base00
            $0.trailing == contentView.trailingAnchor - Spacing.base02
            $0.bottom == contentView.bottomAnchor - Spacing.base00
            $0.leading == contentView.leadingAnchor + Layout.Margin.largeTitleLeading
        }

        iconImageView.layout {
            $0.width == Layout.Size.icon
            $0.height == Layout.Size.icon
            $0.centerY == containerView.centerYAnchor
            $0.top >= containerView.topAnchor + Spacing.base02
            $0.bottom <= containerView.bottomAnchor - Spacing.base02
            $0.leading == containerView.leadingAnchor + Spacing.base02
        }

        badgeContainerView.layout {
            $0.top == containerView.topAnchor + Spacing.base01
            $0.leading >= iconImageView.trailingAnchor + Spacing.base02
            $0.trailing == containerView.trailingAnchor
            $0.height == Layout.Size.badge
        }

        badgeLabel.layout {
            $0.top == badgeContainerView.topAnchor
            $0.leading == badgeContainerView.leadingAnchor + Spacing.base01
            $0.trailing == badgeContainerView.trailingAnchor - Spacing.base01
            $0.bottom == badgeContainerView.bottomAnchor
        }

        nameLabel.layout {
            $0.top == badgeContainerView.bottomAnchor + Spacing.base01
            $0.leading == iconImageView.trailingAnchor + Spacing.base02
            $0.trailing <= arrowImageView.leadingAnchor - Spacing.base02
        }

        addressLabel.layout {
            $0.top == nameLabel.bottomAnchor + Layout.Margin.size00
            $0.leading == nameLabel.leadingAnchor
            $0.trailing <= arrowImageView.leadingAnchor - Spacing.base02
        }

        placeLabel.layout {
            $0.top == addressLabel.bottomAnchor + Layout.Margin.size00
            $0.leading == nameLabel.leadingAnchor
            $0.bottom <= containerView.bottomAnchor - Spacing.base01
        }

        placePinImageView.layout {
            $0.width == Layout.Size.placePin.width
            $0.height == Layout.Size.placePin.height
            $0.centerY == placeLabel.centerYAnchor
            $0.leading == placeLabel.trailingAnchor + Spacing.base00
        }

        distanceLabel.layout {
            $0.centerY == placeLabel.centerYAnchor
            $0.leading == placePinImageView.trailingAnchor + Spacing.base01
            $0.trailing <= arrowImageView.leadingAnchor - Spacing.base02
        }

        arrowImageView.layout {
            $0.width == Layout.Size.arrow.width
            $0.height == Layout.Size.arrow.height
            $0.centerY == containerView.centerYAnchor
            $0.trailing == containerView.trailingAnchor - Layout.Margin.largeTitleLeading
        }
    }

    func configureViews() {
        layoutMargins = .zero
        selectionStyle = .none
        backgroundColor = .clear
    }
}

// MARK: - Private
private extension PromotionListViewEstablishmentCell {
    func update(description: String?) {
        guard let description = description else {
            return nameLabel.attributedText = nil
        }

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.26

        let attributedText = NSAttributedString(
            string: description,
            attributes: [.paragraphStyle: paragraphStyle])

        nameLabel.attributedText = attributedText
    }
}
