protocol PromotionListFactory {
    func makeListController(
        openWebView: @escaping (URL?) -> Void,
        openDeepLink: @escaping (URL?) -> Void,
        openOnboarding: @escaping () -> Void,
        openDetails: @escaping (Promotion, PromotionDetailsIDType) -> Void
    ) -> PromotionListViewController

    func makeWebController(_ url: URL) -> UIViewController
}
