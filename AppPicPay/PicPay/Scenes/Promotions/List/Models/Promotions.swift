struct Promotions {
    // MARK: - Properties
    let title: String
    let category: Category
    let items: [Promotion]
}

// MARK: - Decodable
extension Promotions: Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let title = try container.decode(String.self, forKey: .title)
        let category = try container.decode(String.self, forKey: .category)
        let items = try container.decode([Promotion].self, forKey: .items)

        self.init(title: title, category: .init(rawValue: category), items: items)
    }
}

// MARK: - Private
private extension Promotions {
    enum CodingKeys: String, CodingKey {
        case title
        case category
        case items = "data"
    }
}

// MARK: - Nested type
extension Promotions {
    enum Category: String {
        case generic = "generic_promo"
        case location = "location_promo"

        init(rawValue: String) {
            switch rawValue {
            case Promotions.Category.generic.rawValue:
                self = .generic
            case Promotions.Category.location.rawValue:
                self = .location
            default:
                self = .generic
            }
        }
    }
}
