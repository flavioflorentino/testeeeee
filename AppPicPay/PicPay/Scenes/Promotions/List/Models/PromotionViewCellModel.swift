import Foundation

struct PromotionViewCellModel {
    let image: URL?
    let badge: String
    let title: String
    let description: String
    let distance: String?
    var permission: (description: String, link: String)?
}
