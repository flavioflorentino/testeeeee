import Foundation

struct Promotion {
    // MARK: - Properties
    let title: String?
    let description: String
    let imageUrl: URL?
    let badge: String?
    let distance: String?
    let action: Action
    let tracking: Tracking
}

// MARK: - Decodable
extension Promotion: Decodable {
    enum CodingKeys: String, CodingKey {
        case title
        case description
        case imageUrl = "image"
        case badge
        case distance
        case action
        case tracking
    }
}

// MARK: - Nested types
extension Promotion {
    struct Tracking: Decodable {
        let type: String
        let origin: String
    }
}

extension Promotion {
    enum Action {
        case webView(url: URL?)
        case deeplink(data: String)
        case detail(data: String)
        case other(data: String)
    }
}

extension Promotion.Action: Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let type = try container.decode(String.self, forKey: .type)

        var data = ""
        if let value = try? container.decode(Int.self, forKey: .data) {
            data = "\(value)"
        } else {
            data = try container.decode(String.self, forKey: .data)
        }

        self = .actionOf(type: type, data: data)
    }
}

private extension Promotion.Action {
    enum CodingKeys: String, CodingKey {
        case type
        case data
    }

    static func actionOf(type: String, data: String) -> Promotion.Action {
        switch type {
        case "deeplink":
            return .deeplink(data: data)
        case "webview":
            return .webView(url: URL(string: data))
        case "detail":
            return .detail(data: data)
        default:
            return .other(data: data)
        }
    }
}
