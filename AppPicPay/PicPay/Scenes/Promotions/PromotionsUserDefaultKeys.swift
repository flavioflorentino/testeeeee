enum PromotionsUserDefaultKey: String {
    case alreadyShownOnboading = "promotions-already-shown-onboarding"
    case alreadyOpened = "promotions-already-opened"
}
