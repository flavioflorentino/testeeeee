import Core
import Foundation

enum PromotionsServiceEndpoint {
    case promotions
    case promotionsByCoordinate(latitude: Double, longitude: Double)
    case sellerDetails(id: String)
    case sellerId(storeId: String)
    case whatsApp(sellerId: String)
}

extension PromotionsServiceEndpoint: ApiEndpointExposable {
    var method: HTTPMethod {
        switch self {
        case .sellerId:
            return .post
        default:
            return .get
        }
    }

    var path: String {
        switch self {
        case .promotions,
             .promotionsByCoordinate:
            return "v2/rewards"
        case let .sellerDetails(id):
            return "v2/rewards/seller/\(id)"
        case .sellerId:
            return "api/itemByStoreId.json"
        case let .whatsApp(sellerId):
            return "seller/public/whatsapp/\(sellerId)"
        }
    }

    var parameters: [String: Any] {
        switch self {
        case let .promotionsByCoordinate(latitude, longitude):
            return [
                "latitude": "\(latitude)",
                "longitude": "\(longitude)"
            ]
        default:
            return [:]
        }
    }

    var body: Data? {
        switch self {
        case let .sellerId(storeId):
            return ["store_id": storeId].toData()
        default:
            return nil
        }
    }
}
