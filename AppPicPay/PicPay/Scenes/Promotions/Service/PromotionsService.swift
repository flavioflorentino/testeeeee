import Core
import Dispatch

protocol PromotionsServicing {
    typealias PromotionsResult = Result<[Promotions], ApiError>

    var consumerName: String? { get }
    
    func promotions(latitude: Double, longitude: Double, _ completion: @escaping (PromotionsResult) -> Void)
    func promotions(_ completion: @escaping (PromotionsResult) -> Void)
    func sellerDetails(id: String, _ completion: @escaping (Result<PromotionSeller, ApiError>) -> Void)
    func loadSellerID(from storeId: String, _ completion: @escaping (Result<String, ApiError>) -> Void)
    func loadWhatsAppNumber(from sellerId: String, _ completion: @escaping (Result<WhatsAppSeller, ApiError>) -> Void)
}

struct PromotionsService {
    typealias Dependencies = HasMainQueue & HasConsumerManager
    
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension PromotionsService: PromotionsServicing {
    var consumerName: String? {
        dependencies.consumerManager.consumer?.name
    }
    
    func promotions(latitude: Double, longitude: Double, _ completion: @escaping (PromotionsServicing.PromotionsResult) -> Void) {
        let endpoint = PromotionsServiceEndpoint.promotionsByCoordinate(latitude: latitude, longitude: longitude)
        Api<[Promotions]>(endpoint: endpoint).execute { result in
            self.dependencies.mainQueue.async {
                let mappedResult = result
                    .mapError { $0 }
                    .map { $0.model }
                completion(mappedResult)
            }
        }
    }

    func promotions(_ completion: @escaping (PromotionsServicing.PromotionsResult) -> Void) {
        Api<[Promotions]>(endpoint: PromotionsServiceEndpoint.promotions).execute { result in
            self.dependencies.mainQueue.async {
                let mappedResult = result
                    .mapError { $0 }
                    .map { $0.model }
                completion(mappedResult)
            }
        }
    }

    func sellerDetails(id: String, _ completion: @escaping (Result<PromotionSeller, ApiError>) -> Void) {
        let api = Api<PromotionSeller>(endpoint: PromotionsServiceEndpoint.sellerDetails(id: id))
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { result in
            self.dependencies.mainQueue.async {
                let mappedResult = result
                    .mapError { $0 }
                    .map { $0.model }
                completion(mappedResult)
            }
        }
    }

    func loadSellerID(from storeId: String, _ completion: @escaping (Result<String, ApiError>) -> Void) {
        let api = Api<PromotionSellerID>(endpoint: PromotionsServiceEndpoint.sellerId(storeId: storeId))

        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { result in
            self.dependencies.mainQueue.async {
                let mappedResult = result
                    .mapError { $0 }
                    .map { $0.model.sellerId }
                completion(mappedResult)
            }
        }
    }
    
    func loadWhatsAppNumber(from sellerId: String, _ completion: @escaping (Result<WhatsAppSeller, ApiError>) -> Void) {
        let api = Api<WhatsAppSeller>(endpoint: PromotionsServiceEndpoint.whatsApp(sellerId: sellerId))
        
        api.execute { result in
            self.dependencies.mainQueue.async {
                let mappedResult = result
                    .mapError { $0 }
                    .map(\.model)
                completion(mappedResult)
            }
        }
    }
}
