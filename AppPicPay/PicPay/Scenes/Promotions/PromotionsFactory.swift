import Foundation
import UIKit

final class PromotionsFactory {
}

// MARK: - PromotionDetailsCoordinatorFactory
extension PromotionsFactory: PromotionDetailsCoordinatorFactory {
    func makeDetailsCoordinator(
        seller: PromotionDetailsCoordinating.Seller,
        navigationController: UINavigationController
    ) -> PromotionDetailsCoordinating {
        PromotionDetailsCoordinator(navigationController: navigationController, factory: PromotionsFactory(), seller: seller)
    }
}

// MARK: - PromotionListNewFactory
extension PromotionsFactory: PromotionListNewFactory {
    // swiftlint:disable function_parameter_count
    func makeNewListController(
        promotion: Promotion?,
        openWebView: @escaping (URL?) -> Void,
        openDeepLink: @escaping (URL?) -> Void,
        openOnboarding: @escaping () -> Void,
        openDetails: @escaping (Promotion, PromotionDetailsIDType) -> Void,
        openPromotions: @escaping (Promotion) -> Void
    ) -> PromotionListNewViewController {
        let dependencies = DependencyContainer()
        let presenter = PromotionListNewPresenter()
        let service = PromotionsService(dependencies: dependencies)
        let interactor = PromotionListNewInteractor(service: service, presenter: presenter, dependencies: dependencies)
        let controller = PromotionListNewViewController(interactor: interactor)
        presenter.viewController = controller

        return controller
    }
    // swiftlint:enable function_parameter_count
}

// MARK: - PromotionListFactory
extension PromotionsFactory: PromotionListFactory {
    func makeListController(
        openWebView: @escaping (URL?) -> Void,
        openDeepLink: @escaping (URL?) -> Void,
        openOnboarding: @escaping () -> Void,
        openDetails: @escaping (Promotion, PromotionDetailsIDType) -> Void
    ) -> PromotionListViewController {
        let presenter = PromotionListPresenter()
        presenter.openWebViewAction = openWebView
        presenter.openDeepLinkAction = openDeepLink
        presenter.openDetailsAction = openDetails
        presenter.openOnboardingAction = openOnboarding
        
        let dependencies = DependencyContainer()
        let service = PromotionsService(dependencies: dependencies)

        let viewModel = PromotionListViewModel(service: service, presenter: presenter)

        let controller = PromotionListViewController(viewModel: viewModel)
        presenter.display = controller

        return controller
    }

    func makeWebController(_ url: URL) -> UIViewController {
        WebViewFactory.make(with: url)
    }
}

// MARK: - PromotionsOnboardingFactory
extension PromotionsFactory: PromotionsOnboardingFactory {
    func makeOnboardingController() -> PromotionsOnboardingViewController {
        let dependencies = DependencyContainer()
        let presenter = PromotionsOnboardingPresenter()
        let interactor = PromotionsOnboardingInteractor(presenter: presenter, dependencies: dependencies)
        let controller = PromotionsOnboardingViewController(interactor: interactor)
        presenter.viewController = controller
        
        return controller
    }
}

// MARK: - PromotionDetailsFactory
extension PromotionsFactory: PromotionDetailsFactory {
    // swiftlint:disable function_parameter_count
    func makeDetailsController(
        title: String?,
        idType: PromotionDetailsIDType,
        callPhone: @escaping (String?) -> Void,
        openMap: @escaping (MapData) -> Void,
        performPayment: @escaping (PromotionSeller) -> Void,
        openWhatsApp: @escaping (WhatsAppSeller) -> Void
    ) -> PromotionDetailsViewController {
        let presenter = PromotionDetailsPresenter()
        presenter.performPaymentAction = performPayment
        presenter.openMapAction = openMap
        presenter.callPhoneAction = callPhone
        presenter.openWhatsAppAction = openWhatsApp

        let dependencies = DependencyContainer()
        let favoriteInteractor = FavoriteInteractor(service: FavoritesService(dependencies: dependencies), dependencies: dependencies)
        let service = PromotionsService(dependencies: dependencies)
        
        let interactor = PromotionDetailsInteractor(
            idType: idType,
            service: service,
            presenter: presenter,
            favoriteInteractor: favoriteInteractor,
            dependencies: dependencies
        )

        let controller = PromotionDetailsViewController(interactor: interactor)
        controller.title = title

        presenter.display = controller

        return controller
    }
}
