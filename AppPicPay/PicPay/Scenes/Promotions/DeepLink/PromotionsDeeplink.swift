import Foundation

final class PromotionsDeeplink: PPDeeplink {
    let origin: String?
    
    // MARK: - Initialization
    init(url: URL) {
        origin = url.queryParams["origin"]

        super.init(url: url)
        type = .promotions
    }
}
