import AnalyticsModule
import FeatureFlag
import UI

final class PromotionsDeeplinkHelper {
    typealias Dependencies = HasAppManager & HasFeatureManager & HasAnalytics

    // MARK: - Properties
    private let dependencies: Dependencies
    private var childCoordinator: Coordinating?

    // MARK: - Initialization
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - DeeplinkHelperProtocol
extension PromotionsDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        guard dependencies.featureManager.isActive(.userPromotionsList),
              let navigationController = dependencies.appManager.mainScreenCoordinator?.homeNavigation() else {
            return false
        }
        dependencies.appManager.mainScreenCoordinator?.showHomeScreen()
        
        switch deeplink {
        case let deeplink as PromotionsDeeplink:
            return runPromotionsFlow(origin: deeplink.origin, navigationController: navigationController)
        case let deeplink as PromotionDetailsDeeplink:
            return runPromotionDetailsFlow(deeplink.sellerId, navigationController: navigationController)
        default:
            return false
        }
    }
}

// MARK: - Private
private extension PromotionsDeeplinkHelper {
    func runPromotionsFlow(origin: String?, navigationController: UINavigationController) -> Bool {
        let coordinator = PromotionsCoordinator(
            navigationController: navigationController,
            controllerFactory: PromotionsFactory(),
            coordinatorFactory: PromotionsFactory()
        )

        coordinator.didFinishFlow = {
            self.childCoordinator = nil
        }

        coordinator.start()
        childCoordinator = coordinator
        
        if let origin = origin, let originEvent = PromotionsEvent.ListEventOrigin(rawValue: origin) {
            dependencies.analytics.log(PromotionsEvent.promotionListAccessed(origin: originEvent))
        }

        return true
    }

    func runPromotionDetailsFlow(_ sellerId: String?, navigationController: UINavigationController) -> Bool {
        guard let sellerId = sellerId else { return false }

        let coordinator = PromotionsCoordinator(
            navigationController: navigationController,
            controllerFactory: PromotionsFactory(),
            coordinatorFactory: PromotionsFactory()
        )

        coordinator.didFinishFlow = {
            self.childCoordinator = nil
        }

        coordinator.start(sellerId: sellerId)
        childCoordinator = coordinator

        return true
    }
}
