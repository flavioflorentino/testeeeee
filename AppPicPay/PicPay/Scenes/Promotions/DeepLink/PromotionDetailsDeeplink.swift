import Foundation

final class PromotionDetailsDeeplink: PPDeeplink {
    let sellerId: String?

    // MARK: - Initialization
    convenience init(url: URL) {
        self.init(url: url, type: .promotionDetails)
    }

    override init(url: URL, worker: DeeplinkWorkerProtocol = DeeplinkWorker()) {
        sellerId = url
            .pathComponents
            .first(where: { $0 != "/" && $0 != DeeplinkType.promotionDetails.rawValue })

        super.init(url: url, worker: worker)
    }
}
