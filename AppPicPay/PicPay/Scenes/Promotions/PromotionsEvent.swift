import AnalyticsModule

enum PromotionsEvent {
    case promotionsHomeButtonViewed
    case promotionsOnboardingViewed
    case promotionsOnboardingAccessed
    case promotionsOnboardingClosed(origin: OnboardingCloseOrigin)
    case promotionListAccessed(origin: ListEventOrigin)
    case promotionDetailAccessed(type: String, origin: String)
    case promotionPaymentItemAccessed(type: String, itemName: String, consumerId: Int, sellerId: Int, section: String)
    case promotionMapAccessed
    case promotionShare(type: String, itemName: String, sellerId: Int)
    case promotionLocationPermission
}

// MARK: - Nested types
extension PromotionsEvent {
    enum ListEventOrigin: String {
        case home = "inicio"
        case settings = "ajustes"
        case feed
    }

    enum OnboardingCloseOrigin: String {
        case button
        case bottomSheet = "button_sheet"
    }
}

// MARK: - Private
private extension PromotionsEvent {
    var name: String {
        switch self {
        case .promotionsHomeButtonViewed:
            return "Promotions Home Button Viewed"
        case .promotionsOnboardingViewed:
            return "Promotions Onboarding Viewed"
        case .promotionsOnboardingAccessed:
            return "Promotions Onboarding Accessed"
        case .promotionsOnboardingClosed:
            return "Promotions Onboarding Closed"
        case .promotionListAccessed:
            return "Promotions Page Accessed"
        case .promotionDetailAccessed:
            return "Promotions Detail Accessed"
        case .promotionPaymentItemAccessed:
            return "Payment Item Accessed"
        case .promotionMapAccessed:
            return "Map Accessed"
        case .promotionShare:
            return "Share Button Clicked"
        case .promotionLocationPermission:
            return "Location Permission"
        }
    }

    var properties: [String: Any] {
        let originKey = "origin"
        let typeKey = "type"
        let itemNameKey = "item_name"

        switch self {
        case .promotionsHomeButtonViewed,
             .promotionsOnboardingViewed,
             .promotionsOnboardingAccessed,
             .promotionMapAccessed:
            return [:]
        case let .promotionsOnboardingClosed(origin):
            return [originKey: origin.rawValue]
        case let .promotionListAccessed(origin):
            return [originKey: origin.rawValue]
        case let .promotionDetailAccessed(type, origin):
            return [
                originKey: origin,
                typeKey: type
            ]
        case let .promotionPaymentItemAccessed(type, itemName, consumerId, sellerId, section):
            return [
                typeKey: type,
                itemNameKey: itemName,
                "consumer_id": consumerId,
                "seller_id": sellerId,
                "section": section,
                "section_position": ""
            ]
        case let .promotionShare(type, itemName, sellerId):
            return [
                typeKey: type,
                itemNameKey: itemName,
                "id": sellerId
            ]
        case .promotionLocationPermission:
            return [
                "action": "authorize",
                originKey: "promotions"
            ]
        }
    }

    var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
}

// MARK: - AnalyticsKeyProtocol
extension PromotionsEvent: AnalyticsKeyProtocol {
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
