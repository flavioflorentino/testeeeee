protocol PromotionsOnboardingFactory {
    func makeOnboardingController() -> PromotionsOnboardingViewController
}
