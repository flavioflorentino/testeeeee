import Foundation

protocol PromotionsOnboardingPresenting: AnyObject {
    var viewController: PromotionsOnboardingDisplay? { get set }
    func displayBenefits()
}

final class PromotionsOnboardingPresenter {
    weak var viewController: PromotionsOnboardingDisplay?
}

// MARK: - PromotionsOnboardingPresenting
extension PromotionsOnboardingPresenter: PromotionsOnboardingPresenting {
    func displayBenefits() {
        viewController?.configureBenefitsView()
    }
}
