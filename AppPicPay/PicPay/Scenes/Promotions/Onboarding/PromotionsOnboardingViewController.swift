import UI
import UIKit

protocol PromotionsOnboardingDisplay: AnyObject {
    func configureBenefitsView()
}

// MARK: - Layout
extension PromotionsOnboardingViewController.Layout {
    static let titleFontSize: CGFloat = 20.0
    static let descriptionFontSize: CGFloat = 16.0
    static let buttonFontSize: CGFloat = 14.0

    static let imageSize = CGSize(width: 200.0, height: 124.0)
    static let buttonSize = CGSize(width: 287.0, height: 40.0)

    static let margin200: CGFloat = 16.0
    static let margin300: CGFloat = 24.0
}

// MARK: -
final class PromotionsOnboardingViewController: ViewController<PromotionsOnboardingInteracting, UIView> {
    private typealias Localizable = Strings.Promotions

    // MARK: - Properties
    private var didDismissByButton = false

    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale000.color

        return view
    }()

    private lazy var imageView = UIImageView(image: Assets.Ilustration.iluPayments.image)

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: Layout.titleFontSize, weight: .bold)
        label.text = Localizable.promotionsOnboardingTitle
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = Palette.ppColorGrayscale500.color

        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: Layout.descriptionFontSize)
        label.numberOfLines = 0
        label.textColor = Palette.ppColorGrayscale500.color

        return label
    }()

    private lazy var okButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = Palette.ppColorBranding300.color
        button.titleLabel?.font = .systemFont(ofSize: Layout.buttonFontSize, weight: .medium)
        button.setTitle(Localizable.onboardingOkButton, for: .normal)
        button.setTitleColor(Palette.ppColorGrayscale000.color, for: .normal)
        button.layer.cornerRadius = Layout.buttonSize.height / 2
        button.layer.masksToBounds = true

        button.addTarget(self, action: #selector(didTapAtOkButton), for: .touchUpInside)

        return button
    }()

    // MARK: - View lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor.loadView()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        interactor.dismissView(from: didDismissByButton ? .button : .bottomSheet)
    }

    // MARK: - Setup
    override func buildViewHierarchy() {
        view.addSubview(containerView)

        containerView.addSubview(imageView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(descriptionLabel)
        containerView.addSubview(okButton)
    }

    override func setupConstraints() {
        containerView.layout {
            $0.top >= view.topAnchor
            $0.leading >= view.leadingAnchor
            $0.trailing <= view.trailingAnchor
            $0.bottom <= view.bottomAnchor
            $0.centerX == view.centerXAnchor
            $0.centerY == view.centerYAnchor
        }

        imageView.layout {
            $0.height == Layout.imageSize.height
            $0.width == Layout.imageSize.width
            $0.top == containerView.topAnchor
            $0.centerX == containerView.centerXAnchor
        }

        titleLabel.layout {
            $0.top == imageView.bottomAnchor + Layout.margin200
            $0.leading >= containerView.leadingAnchor + Layout.margin200
            $0.trailing <= containerView.trailingAnchor - Layout.margin200
            $0.centerX == containerView.centerXAnchor
        }

        descriptionLabel.layout {
            $0.top == titleLabel.bottomAnchor + Layout.margin200
            $0.leading >= containerView.leadingAnchor + Layout.margin200
            $0.trailing <= containerView.trailingAnchor - Layout.margin200
            $0.centerX == containerView.centerXAnchor
        }

        okButton.layout {
            $0.height == Layout.buttonSize.height
            $0.width == Layout.buttonSize.width
            $0.top == descriptionLabel.bottomAnchor + Layout.margin300
            $0.leading >= containerView.leadingAnchor + Layout.margin200
            $0.trailing <= containerView.trailingAnchor - Layout.margin200
            $0.bottom <= containerView.bottomAnchor - Layout.margin200
            $0.centerX == containerView.centerXAnchor
        }
    }

    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        configureDescription(with: Localizable.promotionsOnboardingDescription)
    }
}

// MARK: - Private
private extension PromotionsOnboardingViewController {
    enum Layout {
    }
    
    func configureDescription(with text: String) {
        let text = NSString(string: text)

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.lineHeightMultiple = 1.26

        let attributedText = NSMutableAttributedString(
            string: text as String,
            attributes: [.paragraphStyle: paragraphStyle]
        )

        let highlightedRanges = [
            text.range(of: Localizable.onboardingCashback),
            text.range(of: Localizable.onboardingPicPay),
            text.range(of: Localizable.onboardingRemember)
        ]

        highlightedRanges.forEach {
            attributedText.addAttribute(.foregroundColor, value: Palette.ppColorBranding300.color, range: $0)
        }

        descriptionLabel.attributedText = attributedText
    }

    @objc
    func didTapAtOkButton() {
        didDismissByButton = true
        dismiss(animated: true)
    }
}

// MARK: PromotionsOnboardingDisplay
extension PromotionsOnboardingViewController: PromotionsOnboardingDisplay {
    func configureBenefitsView() {
        titleLabel.text = Localizable.benefitsOnboardingTitle
        configureDescription(with: Localizable.benefitsOnboardingDescription)
    }
}
