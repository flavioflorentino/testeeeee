import AnalyticsModule
import FeatureFlag
import Foundation

protocol PromotionsOnboardingInteracting: AnyObject {
    func loadView()
    func dismissView(from origin: PromotionsEvent.OnboardingCloseOrigin)
}

final class PromotionsOnboardingInteractor {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies

    private let presenter: PromotionsOnboardingPresenting

    init(presenter: PromotionsOnboardingPresenting, dependencies: Dependencies) {
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - PromotionsOnboardingInteracting
extension PromotionsOnboardingInteractor: PromotionsOnboardingInteracting {
    func loadView() {
        if dependencies.featureManager.isActive(.featurePromotionsListV2) {
            presenter.displayBenefits()
        }
        dependencies.analytics.log(PromotionsEvent.promotionsOnboardingViewed)
    }
    
    func dismissView(from origin: PromotionsEvent.OnboardingCloseOrigin) {
        dependencies.analytics.log(PromotionsEvent.promotionsOnboardingClosed(origin: origin))
    }
}
