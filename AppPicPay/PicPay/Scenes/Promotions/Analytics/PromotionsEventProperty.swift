import FeatureFlag
import Foundation

struct PromotionsEventProperty: ApplicationEventProperty {
    typealias Dependency = HasFeatureManager

    // MARK: - Properties
    private let dependency: Dependency

    init(dependency: Dependency) {
        self.dependency = dependency
    }
    
    var properties: [String: Any] {
        [
            "is_new_access_promotions_mgm": dependency.featureManager.isActive(.experimentNewAccessPromotionsMgmBool)
        ]
    }
}
