import AnalyticsModule
import Core
import FeatureFlag
import UI
import UIKit

protocol PromotionsCoordinating: Coordinating {
    var didFinishFlow: (() -> Void)? { get set }

    func start(sellerId: String)
}

final class PromotionsCoordinator: NSObject {
    typealias ControllerFactory = PromotionListFactory & PromotionsOnboardingFactory & PromotionListNewFactory
    typealias CoordinatorFactory = PromotionDetailsCoordinatorFactory
    typealias Dependencies = HasAnalytics & HasKVStore & HasFeatureManager

    private typealias Localizable = Strings.Promotions

    // MARK: - Properties
    private let navigationController: UINavigationController
    private let controllerFactory: ControllerFactory
    private let coordinatorFactory: CoordinatorFactory
    private let dependencies: Dependencies

    private var childCoordinators: [Coordinating] = []

    private weak var originalNavigationControllerDelegate: UINavigationControllerDelegate?

    var childViewController: [UIViewController] = []
    var viewController: UIViewController?

    var didFinishFlow: (() -> Void)?

    // MARK: - Initialization
    init(
        navigationController: UINavigationController,
        controllerFactory: ControllerFactory,
        coordinatorFactory: CoordinatorFactory,
        dependencies: Dependencies = DependencyContainer()
    ) {
        self.navigationController = navigationController
        self.controllerFactory = controllerFactory
        self.coordinatorFactory = coordinatorFactory
        self.dependencies = dependencies

        super.init()

        originalNavigationControllerDelegate = navigationController.delegate
        navigationController.delegate = self
    }
}

// MARK: - PromotionsCoordinating
extension PromotionsCoordinator: PromotionsCoordinating {
    func start() {
        showPromotionsListScreen()
        showOnboardingIfNeeded()
    }

    func start(sellerId: String) {
        var controllers = navigationController.viewControllers

        if controllers.contains(where: { $0 is PromotionListViewController }) == false {
            let controller = createPromotionsListScreen()
            controller.hidesBottomBarWhenPushed = true

            controllers.append(controller)
        }

        controllers.removeAll(where: { $0 is PromotionDetailsViewController })

        let coordinator = createDetailsCoordinator(name: Localizable.promotion, idType: .seller(id: sellerId))
        childCoordinators = [coordinator]

        let controller = coordinator.startWithoutShowing()
        controller.hidesBottomBarWhenPushed = true

        controllers.append(controller)

        navigationController.setViewControllers(controllers, animated: true)
    }
}

// MARK: - Private
private extension PromotionsCoordinator {
    enum MapOption {
        case appleMaps
        case googleMaps
        case waze
    }

    var needToShowOnboarding: Bool {
        dependencies.kvStore
            .getFirstTimeOnlyEvent(PromotionsUserDefaultKey.alreadyShownOnboading.rawValue) == false
    }

    func createPromotionsListScreen() -> PromotionListViewController {
        controllerFactory.makeListController(openWebView: { [weak self] url in
            self?.showWebView(url: url)
        }, openDeepLink: { [weak self] deepLink in
            self?.handleDeepLink(deepLink)
        }, openOnboarding: { [weak self] in
            self?.dependencies.analytics.log(PromotionsEvent.promotionsOnboardingAccessed)
            self?.showOnboarding()
        }, openDetails: { [weak self] promotion, idType in
            self?.runDetailsFlow(name: promotion.title, idType: idType)
        })
    }
    
    func createNewPromotionsListScreen(promotion: Promotion?) -> PromotionListNewViewController {
        controllerFactory.makeNewListController(
            promotion: promotion,
            openWebView: { [weak self] url in
                self?.showWebView(url: url)
            }, openDeepLink: { [weak self] deepLink in
                self?.handleDeepLink(deepLink)
            }, openOnboarding: { [weak self] in
                self?.dependencies.analytics.log(PromotionsEvent.promotionsOnboardingAccessed)
                self?.showOnboarding()
            }, openDetails: { [weak self] promotion, idType in
                self?.runDetailsFlow(name: promotion.title, idType: idType)
            }, openPromotions: { [weak self] promotion in
                self?.showPromotionsListScreen(promotion: promotion)
            }
        )
    }

    func showPromotionsListScreen(promotion: Promotion? = nil) {
        let controller = createPromotionsListScreen()
        controller.hidesBottomBarWhenPushed = true

        navigationController.pushViewController(controller, animated: true)
    }

    func showOnboarding() {
        let controller = controllerFactory.makeOnboardingController()
        navigationController.present(controller, animated: true)
    }

    func showWebView(url: URL?) {
        guard let url = url else {
            return
        }

        let controller = controllerFactory.makeWebController(url)
        navigationController.pushViewController(controller, animated: true)
    }

    func showOnboardingIfNeeded() {
        guard needToShowOnboarding else {
            return
        }

        dependencies.kvStore.setFirstTimeOnlyEvent(PromotionsUserDefaultKey.alreadyShownOnboading.rawValue)
        showOnboarding()
    }

    func createDetailsCoordinator(name: String?, idType: PromotionDetailsIDType) -> PromotionDetailsCoordinating {
        let coordinator = coordinatorFactory.makeDetailsCoordinator(
            seller: (idType: idType, name: name),
            navigationController: navigationController
        )

        coordinator.didFinishFlow = { [weak self, weak coordinator] in
            self?.childCoordinators.removeAll(where: { $0 === coordinator })
        }

        return coordinator
    }

    func runDetailsFlow(name: String?, idType: PromotionDetailsIDType) {
        let coordinator = createDetailsCoordinator(name: name, idType: idType)
        coordinator.start()

        childCoordinators.append(coordinator)
    }

    func handleDeepLink(_ deepLink: URL?) {
        if let url = deepLink,
           let deepLinkType = DeeplinkFactory.generateDeepLink(withUrl: url, fromScanner: false)?.type,
           deepLinkType == .invite || deepLinkType == .invoiceCashback {
            return runInviteOrInvoiceFlow(type: deepLinkType)
        }

        guard DeeplinkHelper.handleDeeplink(withUrl: deepLink) else {
            return
        }

        navigationController.popToRootViewController(animated: false)
        didFinishFlow?()
    }

    func runInviteOrInvoiceFlow(type: PPDeeplinkType) {
        if type == .invoiceCashback, let topController = navigationController.topViewController {
            let coordinator = InvoicePromoCoordinator(viewController: topController, controllerFactory: InvoicePromoFactory())
            coordinator.start()
        } else if let controller = ViewsManager.peopleSearchStoryboardViewController("SocialShareCode") {
            navigationController.pushViewController(controller, animated: true)
        }
    }
}

// MARK: - UINavigationControllerDelegate
extension PromotionsCoordinator: UINavigationControllerDelegate {
    func navigationController(
        _ navigationController: UINavigationController,
        didShow viewController: UIViewController,
        animated: Bool
    ) {
        originalNavigationControllerDelegate?
            .navigationController?(navigationController, didShow: viewController, animated: animated)

        guard navigationController.viewControllers.contains(where: { $0 is PromotionListViewController }) == false else {
            return
        }

        navigationController.delegate = originalNavigationControllerDelegate
        didFinishFlow?()
    }
}
