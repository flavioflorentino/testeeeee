import Foundation
import UIKit

final class EstablishmentCoordinatorFactory: NSObject {
}

// MARK: - EstablishmentDetailsCoordinatorFactory
extension EstablishmentCoordinatorFactory {
    @objc
    func makeDetailsCoordinator(
        store: PPStore,
        navigationController: UINavigationController?,
        fromViewController: UIViewController
    ) -> EstablishmentDetailsCoordinator {
        EstablishmentDetailsCoordinator(
            store: store,
            navigationController: navigationController,
            fromViewController: fromViewController,
            coordinatorFactory: PromotionsFactory()
        )
    }
}
