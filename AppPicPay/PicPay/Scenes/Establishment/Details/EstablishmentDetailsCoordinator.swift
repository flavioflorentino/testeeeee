import AnalyticsModule
import Core
import FeatureFlag
import UI
import UIKit

protocol EstablishmentDetailsCoordinating: Coordinating {
    var didFinishFlow: (() -> Void)? { get set }
}

final class EstablishmentDetailsCoordinator: NSObject {
    typealias Controller = UIViewController & PopupPresentable
    typealias CoordinatorFactory = PromotionDetailsCoordinatorFactory
    typealias Dependency = HasFeatureManager

    // MARK: - Properties
    private let store: PPStore
    private let navigationController: UINavigationController?
    private let fromViewController: Controller
    private let coordinatorFactory: CoordinatorFactory
    private let dependency: Dependency

    private var childCoordinator: Coordinating?

    var childViewController: [UIViewController] = []
    var viewController: UIViewController?

    var didFinishFlow: (() -> Void)?

    // MARK: - Initialization
    init(
        store: PPStore,
        navigationController: UINavigationController?,
        fromViewController: Controller,
        coordinatorFactory: CoordinatorFactory,
        dependency: Dependency = DependencyContainer()
    ) {
        self.store = store
        self.navigationController = navigationController
        self.fromViewController = fromViewController
        self.coordinatorFactory = coordinatorFactory
        self.dependency = dependency

        super.init()
    }
}

// MARK: - EstablishmentDetailsCoordinating
extension EstablishmentDetailsCoordinator: EstablishmentDetailsCoordinating {
    @objc
    func start() {
        guard store.isCielo == false else {
            return showPopUpProfileScreen()
        }

        if let sellerId = store.sellerId, sellerId.isNotEmpty {
            showPromotionDetailsScreen(idType: .seller(id: sellerId), name: store.name)
        } else if let storeId = store.storeId, storeId.isNotEmpty {
            showPromotionDetailsScreen(idType: .store(id: storeId), name: store.name)
        }
    }

    @objc
    func setDidFinishFlow(_ didFinishFlow: @escaping () -> Void) {
        self.didFinishFlow = didFinishFlow
    }
}

// MARK: - Private
private extension EstablishmentDetailsCoordinator {
    func showPopUpProfileScreen() {
        let controller = ProfileStoreViewController(store: store, controller: fromViewController)
        controller.dismissBlock = { [weak self] in
            self?.didFinishFlow?()
        }

        fromViewController.showPopup(controller)
    }

    func showPromotionDetailsScreen(idType: PromotionDetailsIDType, name: String?) {
        guard let navigationController = navigationController else {
            return
        }

        let coordinator = coordinatorFactory.makeDetailsCoordinator(
            seller: (idType: idType, name: name),
            navigationController: navigationController
        )
        childCoordinator = coordinator

        coordinator.didFinishFlow = { [weak self] in
            self?.childCoordinator = nil
            self?.didFinishFlow?()
        }

        coordinator.start()
    }
}
