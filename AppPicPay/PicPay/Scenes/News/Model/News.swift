import Foundation
import SwiftyJSON

final class News: NSObject {
    let name: String
    let descriptionText: String
    let viewed: Bool
    let imageURL: String
    let url: String
    let id: String
    
    init?(json: JSON, type: SearchResultType) {
        self.name = json["name"].string ?? ""
        self.descriptionText = json["description"].string ?? ""
        self.viewed = json["viewed"].bool ?? false
        self.imageURL = json ["image_url"].string ?? ""
        self.url = json["url"].string ?? ""
        self.id = json["id"].string ?? ""
    }
}
