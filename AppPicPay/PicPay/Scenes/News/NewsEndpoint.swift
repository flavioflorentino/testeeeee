import Core

enum NewsEndpoint {
    case viewed(id: String)
}

extension NewsEndpoint: ApiEndpointExposable {
    typealias Dependencies = HasKeychainManager
    
    var path: String {
        "/search/consumer-news/"
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var isTokenNeeded: Bool {
        false
    }
    
    var body: Data? {
        switch self {
        case let .viewed(id):
            return ["newsId": id].toData()
        }
    }
    
    var customHeaders: [String: String] {
        let dependencies: Dependencies = DependencyContainer()
        return ["token": dependencies.keychain.getData(key: KeychainKey.token) ?? String()]
    }
}
