struct OtherValueData {
    let minusButtonEnable: Bool
    let plusButtonEnabled: Bool
    let plus50ButtonEnabled: Bool
    let plus100ButtonEnabled: Bool
    let currentCashoutValue: Double
    let currentCashoutValueAttributedString: NSAttributedString
}

protocol OtherValueConfigViewViewModelInputs {
    func configureValues(withAvailable value: Double)
    func configureViewState(addingValue value: Double)
}

protocol OtherValueConfigViewViewModelOutputs: AnyObject {
    func updateViewState(withData data: OtherValueData)
}

protocol OtherValueConfigViewViewModelType: AnyObject {
    var inputs: OtherValueConfigViewViewModelInputs { get }
    var outputs: OtherValueConfigViewViewModelOutputs? { get set }
}

final class OtherValueConfigViewViewModel: OtherValueConfigViewViewModelType, OtherValueConfigViewViewModelInputs {
    var inputs: OtherValueConfigViewViewModelInputs { self }
    weak var outputs: OtherValueConfigViewViewModelOutputs?
    
    private var availableValue: Double = 0.0
    private var currentCashoutValue: Double = 100.0
    
    func configureValues(withAvailable value: Double) {
        self.availableValue = value
        configureViewState(addingValue: 0.0)
    }
    
    func configureViewState(addingValue value: Double) {
        guard currentCashoutValue + value <= availableValue else {
            return
        }
        
        currentCashoutValue += value
        let isMinusButtonEnabled = currentCashoutValue > 100
        let isPlusButtonEnabled = currentCashoutValue + 10.0 <= availableValue
        let isPlus50ButtonEnabled = currentCashoutValue + 50.0 <= availableValue
        let isPlus100ButtonEnabled = currentCashoutValue + 100 <= availableValue
        
        let data = OtherValueData(minusButtonEnable: isMinusButtonEnabled,
                                  plusButtonEnabled: isPlusButtonEnabled,
                                  plus50ButtonEnabled: isPlus50ButtonEnabled,
                                  plus100ButtonEnabled: isPlus100ButtonEnabled,
                                  currentCashoutValue: currentCashoutValue,
                                  currentCashoutValueAttributedString: configureCashoutLabelAttributes(withValue: currentCashoutValue))
        
        outputs?.updateViewState(withData: data)
    }
    
    private func configureCashoutLabelAttributes(withValue value: Double) -> NSAttributedString {
        let symbol = Strings.Atm24.currencySymbol
        let completeText = "\(symbol)\(Int(value))"
        
        let rangeOfValue = (completeText as NSString).range(of: "\(Int(value))")
        let rangeOfSymbol = (completeText as NSString).range(of: symbol)
        
        let attributedString = NSMutableAttributedString(string: completeText)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 52), range: rangeOfValue)
        attributedString.addAttribute(NSAttributedString.Key.baselineOffset, value: NSNumber(18), range: rangeOfSymbol)
        return attributedString
    }
}
