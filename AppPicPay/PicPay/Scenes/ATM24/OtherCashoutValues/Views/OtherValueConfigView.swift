import UI
import UIKit

protocol OtherValueConfigViewDelegate: AnyObject {
    func didChangeWithdrawValue(view: OtherValueConfigView, value: Double)
}

final class OtherValueConfigView: UIView {
    enum Layout {
        static let circularButtonsSize: CGFloat = 48
        static let circularButtonsCornerRadius: CGFloat = 24
        static let subButtonsWidth: CGFloat = 104
        static let subButtonsHeight: CGFloat = 32
        static let subButtonsCornerRaidus: CGFloat = 16
        static let subButtonsTopSpace: CGFloat = 24
        static let subButtonsHorizontalSpace: CGFloat = 16
        static let stackViewSpacing: CGFloat = 28
    }
    
    lazy var viewModel: OtherValueConfigViewViewModelType = {
        let viewModel = OtherValueConfigViewViewModel()
        viewModel.outputs = self
        return viewModel
    }()
    
    private lazy var minusButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(#imageLiteral(resourceName: "btnMinusSign").withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = Palette.ppColorBranding300.color
        button.backgroundColor = Palette.ppColorGrayscale100.color
        button.layer.cornerRadius = Layout.circularButtonsCornerRadius
        button.addTarget(self, action: #selector(minusButtonTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var plusButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(#imageLiteral(resourceName: "btnPlusSign").withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = Palette.ppColorBranding300.color
        button.backgroundColor = Palette.ppColorGrayscale100.color
        button.layer.cornerRadius = Layout.circularButtonsCornerRadius
        button.addTarget(self, action: #selector(plusButtonTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var currentCashoutValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorBranding300.color
        label.text = Strings.Atm24.currencySymbol
        label.font = UIFont.systemFont(ofSize: 22)
        label.minimumScaleFactor = 0.75
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    private lazy var cashoutStackView: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.alignment = .center
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.spacing = Layout.stackViewSpacing
        return stack
    }()
    
    private lazy var subButtonsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale000.color
        return view
    }()
    
    private lazy var plus50Button: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(Strings.Atm24.plus50, for: .normal)
        button.layer.borderColor = Palette.ppColorBranding300.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = Layout.subButtonsCornerRaidus
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        button.addTarget(self, action: #selector(plus50ButtonTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var plus100Button: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(Strings.Atm24.plus100, for: .normal)
        button.layer.borderColor = Palette.ppColorBranding300.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = Layout.subButtonsCornerRaidus
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        button.setTitleColor(Palette.ppColorBranding300.color, for: .normal)
        button.addTarget(self, action: #selector(plus100ButtonTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    weak var delegate: OtherValueConfigViewDelegate?
    
    init() {
        super.init(frame: CGRect.zero)
        addComponents()
        layoutComponents()
        configureViews()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addComponents() {
        addSubview(cashoutStackView)
        cashoutStackView.addArrangedSubview(minusButton)
        cashoutStackView.addArrangedSubview(currentCashoutValueLabel)
        cashoutStackView.addArrangedSubview(plusButton)
        addSubview(subButtonsContainerView)
        subButtonsContainerView.addSubview(plus50Button)
        subButtonsContainerView.addSubview(plus100Button)
    }
    
    func layoutComponents() {
        NSLayoutConstraint.leadingTrailing(equalTo: self, for: [cashoutStackView], constant: 12)
        cashoutStackView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        
        NSLayoutConstraint.activate([
            plusButton.widthAnchor.constraint(equalToConstant: Layout.circularButtonsSize),
            plusButton.heightAnchor.constraint(equalToConstant: Layout.circularButtonsSize),
            minusButton.heightAnchor.constraint(equalToConstant: Layout.circularButtonsSize),
            minusButton.widthAnchor.constraint(equalToConstant: Layout.circularButtonsSize)
        ])
        
        NSLayoutConstraint.activate([
            subButtonsContainerView.topAnchor.constraint(equalTo: cashoutStackView.bottomAnchor, constant: Layout.subButtonsTopSpace),
            subButtonsContainerView.bottomAnchor.constraint(equalTo: bottomAnchor),
            subButtonsContainerView.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            plus50Button.topAnchor.constraint(equalTo: subButtonsContainerView.topAnchor),
            plus50Button.widthAnchor.constraint(equalToConstant: Layout.subButtonsWidth),
            plus50Button.heightAnchor.constraint(equalToConstant: Layout.subButtonsHeight),
            plus50Button.leadingAnchor.constraint(equalTo: subButtonsContainerView.leadingAnchor),
            plus50Button.trailingAnchor.constraint(equalTo: plus100Button.leadingAnchor, constant: -Layout.subButtonsHorizontalSpace),
            plus50Button.bottomAnchor.constraint(equalTo: subButtonsContainerView.bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            plus100Button.topAnchor.constraint(equalTo: subButtonsContainerView.topAnchor),
            plus100Button.heightAnchor.constraint(equalToConstant: Layout.subButtonsHeight),
            plus100Button.widthAnchor.constraint(equalToConstant: Layout.subButtonsWidth),
            plus100Button.trailingAnchor.constraint(equalTo: subButtonsContainerView.trailingAnchor),
            plus100Button.bottomAnchor.constraint(equalTo: subButtonsContainerView.bottomAnchor)
        ])
    }
    
    func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    func setAvailableValue(_ value: Double) {
        viewModel.inputs.configureValues(withAvailable: value)
    }
    
    @objc
    private func plusButtonTapped(_ sender: UIButton) {
        viewModel.inputs.configureViewState(addingValue: 10)
    }
    
    @objc
    private func minusButtonTapped(_ sender: UIButton) {
        viewModel.inputs.configureViewState(addingValue: -10)
    }
    
    @objc
    private func plus50ButtonTapped(_ sender: UIButton) {
        viewModel.inputs.configureViewState(addingValue: 50)
    }
    
    @objc
    private func plus100ButtonTapped(_ sender: UIButton) {
        viewModel.inputs.configureViewState(addingValue: 100)
    }
    
    private func setSubButton(_ button: UIButton, enabled: Bool) {
        button.isEnabled = enabled
        button.layer.borderColor = enabled ? Palette.ppColorBranding300.cgColor : Palette.ppColorGrayscale400.cgColor
        button.setTitleColor(enabled ? Palette.ppColorBranding300.color : Palette.ppColorGrayscale400.color, for: .normal)
    }
}

extension OtherValueConfigView: OtherValueConfigViewViewModelOutputs {
    func updateViewState(withData data: OtherValueData) {
        currentCashoutValueLabel.attributedText = data.currentCashoutValueAttributedString
        minusButton.isEnabled = data.minusButtonEnable
        plusButton.isEnabled = data.plusButtonEnabled
        setSubButton(plus50Button, enabled: data.plus50ButtonEnabled)
        setSubButton(plus100Button, enabled: data.plus100ButtonEnabled)
        delegate?.didChangeWithdrawValue(view: self, value: data.currentCashoutValue)
    }
}
