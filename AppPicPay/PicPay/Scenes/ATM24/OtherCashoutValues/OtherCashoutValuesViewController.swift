import UI
import UIKit

final class OtherCashoutValuesViewController: LegacyViewController<OtherCashoutValuesViewModelType, OtherCashoutValuesCoordinating, UIView> {
    enum Layout {
        static let headerHeight: CGFloat = 40
        static let continueButtonHeight: CGFloat = 48
        static let continueButtonBottomOffset: CGFloat = 25
        static let continueButtonHorizontalMargin: CGFloat = 20
        static let continueButtonCornerRadius: CGFloat = 24
    }
    private lazy var balanceInfoHeader: BalanceInfoHeader = {
        let header = BalanceInfoHeader()
        header.translatesAutoresizingMaskIntoConstraints = false
        header.delegate = self
        return header
    }()
    
    private lazy var centerStackView: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.alignment = .center
        stack.axis = .vertical
        stack.distribution = .fill
        stack.spacing = 25
        return stack
    }()
    
    private lazy var valueQuestionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = Strings.Atm24.withdrawAmount
        label.textColor = Palette.ppColorGrayscale500.color
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 16)
        return label
    }()
    
    private lazy var otherValueConfigView: OtherValueConfigView = {
        let view = OtherValueConfigView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.delegate = self
        return view
    }()
    
    private lazy var continueButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = Layout.continueButtonCornerRadius
        button.setTitle(DefaultLocalizable.next.text, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        button.setTitleColor(Palette.ppColorGrayscale000.color, for: .normal)
        button.backgroundColor = Palette.ppColorBranding300.color
        button.addTarget(self, action: #selector(continueButtonTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    private var currentCashoutValue: Double = 0.0
    
    override var preferredStatusBarStyle: UIStatusBarStyle { .default }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.inputs.viewDidLoad()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(balanceInfoHeader)
        view.addSubview(centerStackView)
        centerStackView.addArrangedSubview(valueQuestionLabel)
        centerStackView.addArrangedSubview(otherValueConfigView)
        view.addSubview(continueButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        title = Strings.Atm24.withdrawMoney
        navigationItem.backBarButtonItem = UIBarButtonItem(title: nil, style: .plain, target: nil, action: nil)
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [balanceInfoHeader])
        balanceInfoHeader.heightAnchor.constraint(equalToConstant: Layout.headerHeight).isActive = true
        balanceInfoHeader.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [centerStackView])
        centerStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true

        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [continueButton], constant: Layout.continueButtonHorizontalMargin)
        NSLayoutConstraint.activate([
            continueButton.bottomAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.bottomAnchor, constant: -Layout.continueButtonBottomOffset),
            continueButton.heightAnchor.constraint(equalToConstant: Layout.continueButtonHeight)
        ])
    }
    
    @objc
    func continueButtonTapped(_ sender: UIButton) {
        viewModel.inputs.didContinue(withSelectedValue: currentCashoutValue)
    }
}

// MARK: View Model Outputs
extension OtherCashoutValuesViewController: OtherCashoutValuesViewModelOutputs {
    func updateView(withData data: OtherCashoutValuesPresenter) {
        balanceInfoHeader.setValue(data.availableWithdrawValue)
        otherValueConfigView.setAvailableValue(data.availableWithdrawValue)
    }
    
    func showNextStep(withCashoutData: CashoutValues, selectedValue: Double) {
        coordinator.perform(action: .scanner(selectedValue: selectedValue, cashoutData: withCashoutData))
    }
    
    func showHelpInfo(with data: CashoutValues) {
        coordinator.perform(action: .help(cashoutData: data))
    }
}

extension OtherCashoutValuesViewController: OtherValueConfigViewDelegate {
    func didChangeWithdrawValue(view: OtherValueConfigView, value: Double) {
        currentCashoutValue = value
    }
}

extension OtherCashoutValuesViewController: BalanceInfoHeaderDelegate {
    func didTouchHelpButton() {
        viewModel.inputs.loadHelpInfo()
    }
}
