import AnalyticsModule
import Foundation

struct OtherCashoutValuesPresenter {
    var availableWithdrawValue: Double
}

protocol OtherCashoutValuesViewModelInputs {
    func viewDidLoad()
    func didContinue(withSelectedValue value: Double)
    func loadHelpInfo()
}

protocol OtherCashoutValuesViewModelOutputs: AnyObject {
    func updateView(withData data: OtherCashoutValuesPresenter)
    func showNextStep(withCashoutData: CashoutValues, selectedValue: Double)
    func showHelpInfo(with data: CashoutValues)
}

protocol OtherCashoutValuesViewModelType: AnyObject {
    var inputs: OtherCashoutValuesViewModelInputs { get }
    var outputs: OtherCashoutValuesViewModelOutputs? { get set }
}

final class OtherCashoutValuesViewModel: OtherCashoutValuesViewModelType, OtherCashoutValuesViewModelInputs {
    var inputs: OtherCashoutValuesViewModelInputs { self }
    weak var outputs: OtherCashoutValuesViewModelOutputs?

    private let service: OtherCashoutValuesServicing
    private var cashoutData: CashoutValues
    
    init(service: OtherCashoutValuesServicing, data: CashoutValues) {
        self.service = service
        self.cashoutData = data
    }

    func viewDidLoad() {
        outputs?.updateView(withData: OtherCashoutValuesPresenter(availableWithdrawValue: cashoutData.availableWithdrawalValue))
    }
    
    func didContinue(withSelectedValue value: Double) {
        Analytics.shared.log(ATM24Events.selectValue(.other, selectedValue: value))
        outputs?.showNextStep(withCashoutData: cashoutData, selectedValue: value)
    }
    
    func loadHelpInfo() {
        outputs?.showHelpInfo(with: cashoutData)
    }
}
