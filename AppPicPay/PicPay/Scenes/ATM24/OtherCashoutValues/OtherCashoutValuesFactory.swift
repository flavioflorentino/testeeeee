import Foundation

enum OtherCashoutValuesFactory {
    static func make(withData cashoutData: CashoutValues) -> OtherCashoutValuesViewController {
        let service: OtherCashoutValuesServicing = OtherCashoutValuesService()
        let viewModel: OtherCashoutValuesViewModelType = OtherCashoutValuesViewModel(service: service, data: cashoutData)
        var coordinator: OtherCashoutValuesCoordinating = OtherCashoutValuesCoordinator()
        let viewController = OtherCashoutValuesViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController

        return viewController
    }
}
