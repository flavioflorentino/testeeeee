import UIKit

enum OtherCashoutValuesAction {
    case scanner(selectedValue: Double, cashoutData: CashoutValues)
    case help(cashoutData: CashoutValues)
}

protocol OtherCashoutValuesCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: OtherCashoutValuesAction)
}

final class OtherCashoutValuesCoordinator: OtherCashoutValuesCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: OtherCashoutValuesAction) {
        switch action {
        case let .scanner(selectedValue, cashoutData):
            let scannerVC = QRCodeScannerFactory.make(withData: cashoutData, selectedValue: selectedValue)
            let navigation = UINavigationController(rootViewController: scannerVC)
            navigation.modalPresentationStyle = .fullScreen
            viewController?.present(navigation, animated: true)
        case .help(let cashoutData):
            let limitInfoVC = CashoutLimitsFactory.make(withCashoutData: cashoutData)
            viewController?.navigationController?.pushViewController(limitInfoVC, animated: true)
        }
    }
}
