import Core
import Foundation

enum ATM24Endpoint {
    case cashout
    case authorization(data: AuthorizationInfoRequestData)
    case verifyStatus(transactionId: String)
}

extension ATM24Endpoint: ApiEndpointExposable {
    typealias Dependencies = HasKeychainManager
    
    var path: String {
        switch self {
        case .cashout:
            return "cashout/banco24horas"
        case .authorization:
            return "cashout/banco24horas/authorization/"
        case .verifyStatus(let transactionId):
            return "cashout/banco24horas/\(transactionId)"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .authorization:
            return .post
        default:
            return .get
        }
    }
    
    var body: Data? {
        switch self {
        case .authorization(let data):
            let authorizationData: [String: Any] = [
                "qr_code": data.qrCode,
                "withdrawal_value": data.withdrawValue
            ]
            let data = try? JSONSerialization.data(withJSONObject: authorizationData)
            return data
        default:
            return nil
        }
    }
    
    var customHeaders: [String: String] {
        let dependencies: Dependencies = DependencyContainer()
        switch self {
        case .authorization(let data):
            return ["Token": dependencies.keychain.getData(key: KeychainKey.token) ?? "", "password": data.password]
        default:
            return ["Token": dependencies.keychain.getData(key: KeychainKey.token) ?? ""]
        }
    }
    
    var isTokenNeeded: Bool {
        false
    }
}
