import Foundation

enum ScannerHowItWorksLocalizable: String, Localizable {
    case step1
    case step1Info
    case step1HighlightedText
    case step2
    case step2Info
    case step2HighlightedText
    case step3
    case step3Info
    case step3HighlightedText

    var key: String { rawValue }
    var file: LocalizableFile { .scannerHowItWorks }
}
