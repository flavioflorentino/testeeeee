import UIKit

enum DragDirection {
    case up
    case down
}

enum DraggablePosition {
    case collapsed
    case open
    
    var defaultBottomTopOffset: CGFloat { 96 }
    
    var dimAlpha: CGFloat {
        switch self {
        case .collapsed:
            return 0.0
        case .open:
            return 0.45
        }
    }
    
    // This is the minimum of movement to make the view open
    func upBoundary(for maxHeight: CGFloat, inContainerFrame frame: CGRect) -> CGFloat {
        frame.height - defaultBottomTopOffset * 1.5
    }
    
    // This is the minimum of movement to make the view collapse
    func downBoundary(for maxHeight: CGFloat, inContainerFrame frame: CGRect) -> CGFloat {
        frame.height - maxHeight + defaultBottomTopOffset
    }
    
    func originY(for maxHeight: CGFloat, inContainerFrame frame: CGRect) -> CGFloat {
        if self == .collapsed {
            return frame.height - defaultBottomTopOffset
        } else {
            return frame.height - maxHeight
        }
    }
}

protocol QRCodeScannerHowItWorksViewViewModelInputs {
    func calculateOffset(forGesture gesture: UIPanGestureRecognizer, inView view: UIView, containedIn containerFrame: CGRect)
}

protocol QRCodeScannerHowItWorksViewViewModelOutputs: AnyObject {
    func didChangePosition(to: DraggablePosition)
}

protocol QRCodeScannerHowItWorksViewViewModelType: AnyObject {
    var inputs: QRCodeScannerHowItWorksViewViewModelInputs { get }
    var outputs: QRCodeScannerHowItWorksViewViewModelOutputs? { get set }
}

final class QRCodeScannerHowItWorksViewViewModel: QRCodeScannerHowItWorksViewViewModelType, QRCodeScannerHowItWorksViewViewModelInputs {
    var inputs: QRCodeScannerHowItWorksViewViewModelInputs { self }
    weak var outputs: QRCodeScannerHowItWorksViewViewModelOutputs?
    
    private var draggablePosition: DraggablePosition = .collapsed
    private var dragDirection: DragDirection = .up
    private let translationSpeedFactor: CGFloat = 10 // movement 10 times slower
    private var animator: UIViewPropertyAnimator?
    
    func configureAnimator() {
        let springTiming = UISpringTimingParameters(dampingRatio: 0.7, initialVelocity: CGVector(dx: 0, dy: 10))
        
        animator = UIViewPropertyAnimator(duration: 0.8, timingParameters: springTiming)
        animator?.isInterruptible = true
    }
    
    func calculateOffset(forGesture gesture: UIPanGestureRecognizer, inView view: UIView, containedIn containerFrame: CGRect) {
        if animator == nil {
            configureAnimator()
        }
        let translation = gesture.translation(in: view)
        dragDirection = translation.y > 0 ? .down : .up
        let currentOriginY = view.frame.origin.y
        
        let newOffset = currentOriginY + (translation.y / translationSpeedFactor)
        
        if newOffset <= containerFrame.height - view.frame.height { // blocks the movement if It is too high
            updateOffset(containerFrame.height - view.frame.height, fromGesture: gesture, atView: view, inDirection: dragDirection)
            return
        }
        updateOffset(newOffset, fromGesture: gesture, atView: view, inDirection: dragDirection)
    }
    
    func updateOffset(_ offset: CGFloat, fromGesture gesture: UIPanGestureRecognizer, atView view: UIView, inDirection: DragDirection) {
        switch gesture.state {
        case .changed, .began:
            view.frame.origin.y = offset
        case .ended:
            animate(view: view, offset, inDirection: inDirection)
        default:
            break
        }
    }
    
    private func animate(view: UIView, _ dragOffset: CGFloat, inDirection direction: DragDirection) {
        guard let containerFrame = view.superview?.frame else {
            return
        }
        switch direction {
        case .up:
            let upBoundary = DraggablePosition.open.upBoundary(for: view.frame.height, inContainerFrame: containerFrame)
            animate(view: view, to: dragOffset < upBoundary ? .open : .collapsed)
        case .down:
            let downBoundary = DraggablePosition.collapsed.downBoundary(for: view.frame.height, inContainerFrame: containerFrame)
            animate(view: view, to: dragOffset > downBoundary ? .collapsed : .open)
        }
    }
    
    private func animate(view: UIView, to position: DraggablePosition) {
        guard let animator = animator else {
            return
        }
        
        animator.addAnimations { [weak self] in
            guard let parent = view.superview else {
                return
            }
            
            view.frame.origin.y = position.originY(for: view.frame.height, inContainerFrame: parent.frame)
            self?.outputs?.didChangePosition(to: position)
        }
        
        animator.startAnimation()
    }
}
