import UI
import UIKit

protocol QRCodeScannerHowItWorksViewDelegate: AnyObject {
    func didChange(view: QRCodeScannerHowItWorksView, dragPosition: DraggablePosition)
}

final class QRCodeScannerHowItWorksView: UIView {
    private lazy var viewModel: QRCodeScannerHowItWorksViewViewModelType = {
        let viewModel = QRCodeScannerHowItWorksViewViewModel()
        viewModel.outputs = self
        return viewModel
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale000.color
        return view
    }()
    
    private lazy var handleView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale400.color
        return view
    }()
    
    private lazy var headerLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorGrayscale500.color
        label.text = Strings.Atm24.stepByStep
        return label
    }()
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale100.color
        return view
    }()

    private lazy var step1: QRCodeScannerStepView = {
        let step = QRCodeScannerStepData(stepNumber: ScannerHowItWorksLocalizable.step1.text, stepInfo: ScannerHowItWorksLocalizable.step1Info.text, highlightedText: ScannerHowItWorksLocalizable.step1HighlightedText.text, isLastStep: false)
        let view = QRCodeScannerStepView(withStepData: step)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var step2: QRCodeScannerStepView = {
         let step = QRCodeScannerStepData(stepNumber: ScannerHowItWorksLocalizable.step2.text, stepInfo: ScannerHowItWorksLocalizable.step2Info.text, highlightedText: ScannerHowItWorksLocalizable.step2HighlightedText.text, isLastStep: false)
        let view = QRCodeScannerStepView(withStepData: step)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var step3: QRCodeScannerStepView = {
        let step = QRCodeScannerStepData(stepNumber: ScannerHowItWorksLocalizable.step3.text, stepInfo: ScannerHowItWorksLocalizable.step3Info.text, highlightedText: ScannerHowItWorksLocalizable.step3HighlightedText.text, isLastStep: true)
        let view = QRCodeScannerStepView(withStepData: step)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var animator: UIViewPropertyAnimator?
    weak var delegate: QRCodeScannerHowItWorksViewDelegate?
    
    init() {
        super.init(frame: .zero)
        buildViewHierarchy()
        configureViews()
        setupConstraints()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(containerView)
        containerView.addSubview(handleView)
        containerView.addSubview(headerLabel)
        containerView.addSubview(separatorView)
        containerView.addSubview(step1)
        containerView.addSubview(step2)
        containerView.addSubview(step3)
    }
    
    func configureViews() {
        backgroundColor = .clear
        containerView.applyRadiusTopCorners()
        handleView.applyRadiusCorners(radius: 2.5)
        addPanGesture()
    }
    
    func setupConstraints() {
        NSLayoutConstraint.constraintAllEdges(from: containerView, to: self)
        NSLayoutConstraint.activate([
            handleView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            handleView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 12),
            handleView.widthAnchor.constraint(equalToConstant: 44),
            handleView.heightAnchor.constraint(equalToConstant: 4)
        ])
        
        NSLayoutConstraint.activate([
            headerLabel.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            headerLabel.topAnchor.constraint(equalTo: handleView.bottomAnchor, constant: 20)
        ])
        
        NSLayoutConstraint.leadingTrailing(equalTo: containerView, for: [separatorView], constant: 22)
        NSLayoutConstraint.activate([
            separatorView.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: 10),
            separatorView.heightAnchor.constraint(equalToConstant: 1)
        ])
        
        NSLayoutConstraint.leadingTrailingCenterX(equalTo: containerView, for: [step1, step2, step3], constant: 22)
        NSLayoutConstraint.activate([
            step1.topAnchor.constraint(equalTo: separatorView.bottomAnchor, constant: 20),
            step2.topAnchor.constraint(equalTo: step1.bottomAnchor, constant: 8),
            step3.topAnchor.constraint(equalTo: step2.bottomAnchor, constant: 8),
            step3.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -24)
        ])
    }
    
    func drawDottedLine(on view: UIView) {
        let iconXCenter = step1.roundView.center.x
        let margin: CGFloat = 2.0
        let iconYMax = step1.roundView.center.y + step1.roundView.frame.height / 2 + margin
        let icon2YMin = step2.roundView.center.y - step2.roundView.frame.height / 2 - margin
        let icon2YMax = step2.roundView.center.y + step2.roundView.frame.height / 2 + margin
        let icon3YMin = step3.roundView.center.y - step3.roundView.frame.height / 2 - margin
        
        let icon1Center = CGPoint(x: iconXCenter, y: iconYMax)
        let icon2CenterMin = CGPoint(x: iconXCenter, y: icon2YMin)
        let icon2CenterMax = CGPoint(x: iconXCenter, y: icon2YMax)
        let icon3Center = CGPoint(x: iconXCenter, y: icon3YMin)
        
        let iconStepCenter = step1.convert(icon1Center, to: view)
        let iconStep2CenterMin = step2.convert(icon2CenterMin, to: view)
        let iconStep2CenterMax = step2.convert(icon2CenterMax, to: view)
        let iconStep3Center = step3.convert(icon3Center, to: view)
        
        drawDottedLine(start: iconStepCenter, end: iconStep2CenterMin, view: view)
        drawDottedLine(start: iconStep2CenterMax, end: iconStep3Center, view: view)
    }
    
    private func addPanGesture() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(didPan(gesture:)))
        self.addGestureRecognizer(panGesture)
    }
    
    @objc
    func didPan(gesture: UIPanGestureRecognizer) {
        guard let containerFrame = self.superview?.frame else {
            return
        }
        viewModel.inputs.calculateOffset(forGesture: gesture, inView: self, containedIn: containerFrame)
    }
}

extension QRCodeScannerHowItWorksView: QRCodeScannerHowItWorksViewViewModelOutputs {
    func didChangePosition(to: DraggablePosition) {
        delegate?.didChange(view: self, dragPosition: to)
    }
}
