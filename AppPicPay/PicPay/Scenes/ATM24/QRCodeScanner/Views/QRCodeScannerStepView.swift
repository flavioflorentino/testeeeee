import UI
import UIKit

struct QRCodeScannerStepData {
    let stepNumber: String
    let stepInfo: String
    let highlightedText: String
    let isLastStep: Bool
}

final class QRCodeScannerStepView: UIView {
    lazy var roundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale000.color
        view.layer.cornerRadius = 14
        view.layer.borderWidth = 1
        view.layer.borderColor = Palette.ppColorPositive300.cgColor
        return view
    }()
    
    private lazy var stepLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorPositive300.color
        return label
    }()
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorGrayscale400.color
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        return label
    }()
    
    init(withStepData data: QRCodeScannerStepData) {
        super.init(frame: .zero)
        setup(data)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(_ stepData: QRCodeScannerStepData) {
        backgroundColor = Palette.ppColorGrayscale000.color
        addComponents()
        layoutComponents()
        configureWithStepData(stepData)
    }
    
    func addComponents() {
        addSubview(roundView)
        roundView.addSubview(stepLabel)
        addSubview(infoLabel)
    }
    
    func layoutComponents() {
        NSLayoutConstraint.activate([
            roundView.leadingAnchor.constraint(equalTo: leadingAnchor),
            roundView.topAnchor.constraint(equalTo: topAnchor),
            roundView.widthAnchor.constraint(equalToConstant: 28),
            roundView.heightAnchor.constraint(equalToConstant: 28)
        ])
        
        NSLayoutConstraint.activate([
            stepLabel.centerYAnchor.constraint(equalTo: roundView.centerYAnchor),
            stepLabel.centerXAnchor.constraint(equalTo: roundView.centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            infoLabel.leadingAnchor.constraint(equalTo: roundView.trailingAnchor, constant: 8),
            infoLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            infoLabel.topAnchor.constraint(equalTo: roundView.topAnchor, constant: 4),
            infoLabel.bottomAnchor.constraint(greaterThanOrEqualTo: bottomAnchor, constant: -20)
        ])
    }
    
    private func configureWithStepData(_ step: QRCodeScannerStepData) {
        stepLabel.text = step.stepNumber
        infoLabel.attributedText = step.stepInfo.attributedString(withBold: [step.highlightedText], using: UIFont.systemFont(ofSize: 14))
    }
}
