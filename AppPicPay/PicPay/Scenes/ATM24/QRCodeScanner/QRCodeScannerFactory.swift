import Foundation

enum QRCodeScannerFactory {
    static func make(withData cashoutData: CashoutValues, selectedValue: Double) -> QRCodeScannerViewController {
        let service: QRCodeScannerServicing = QRCodeScannerService()
        let viewModel: QRCodeScannerViewModelType = QRCodeScannerViewModel(service: service, data: cashoutData, selectedValue: selectedValue)
        var coordinator: QRCodeScannerCoordinating = QRCodeScannerCoordinator()
        let viewController = QRCodeScannerViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController

        return viewController
    }
}
