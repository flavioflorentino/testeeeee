import AnalyticsModule
import UI
import UIKit

final class QRCodeScannerViewController: LegacyViewController<QRCodeScannerViewModelType, QRCodeScannerCoordinating, UIView> {
    enum Layout {
        static let scannerHorizontalMargin: CGFloat = 60
        static let scannerTopOffset: CGFloat = 75
        static let scannerMarkerOffset: CGFloat = 6
        static let howItWorksViewInitialBottomSpace: CGFloat = 96
    }
    
    private lazy var scannerTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = Strings.Atm24.scannerTitle
        label.textColor = Palette.white.color
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        return label
    }()
    
    private lazy var scannerFrameView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var qrcodeFrame: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = #imageLiteral(resourceName: "qrcodeFrame")
        return imageView
    }()
    
    private lazy var howItWorksView: QRCodeScannerHowItWorksView = {
        let view = QRCodeScannerHowItWorksView()
        view.delegate = self
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var dimmingView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.alpha = 0
        view.backgroundColor = Palette.black.color
        return view
    }()
    
    private var scanner: PPScanner?
    private var howItWorksEventFired = false
    
    override var shouldAutorotate: Bool {
        false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        howItWorksView.drawDottedLine(on: howItWorksView)
        viewModel.inputs.tryStartScanner()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        scanner?.stop()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Analytics.shared.log(ATM24Events.openScanner)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scannerTitleLabel)
        view.addSubview(scannerFrameView)
        view.addSubview(qrcodeFrame)
        view.addSubview(howItWorksView)
        view.insertSubview(dimmingView, at: 1)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.black.color
        setupScannerNavigationBarStyle()
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.leadingTrailingCenterX(equalTo: view, for: [scannerTitleLabel], constant: Layout.scannerHorizontalMargin)
        scannerTitleLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: Layout.scannerTopOffset).isActive = true
        
        NSLayoutConstraint.leadingTrailingCenterX(equalTo: view, for: [scannerFrameView], constant: Layout.scannerHorizontalMargin)
        scannerFrameView.topAnchor.constraint(equalTo: scannerTitleLabel.bottomAnchor, constant: 15).isActive = true
        scannerFrameView.heightAnchor.constraint(equalTo: scannerFrameView.widthAnchor).isActive = true
        
        NSLayoutConstraint.leadingTrailing(equalTo: scannerFrameView, for: [qrcodeFrame], constant: -Layout.scannerMarkerOffset)
        NSLayoutConstraint.activate([
            qrcodeFrame.topAnchor.constraint(equalTo: scannerFrameView.topAnchor, constant: -Layout.scannerMarkerOffset),
            qrcodeFrame.bottomAnchor.constraint(equalTo: scannerFrameView.bottomAnchor, constant: Layout.scannerMarkerOffset)
        ])
        
        NSLayoutConstraint.leadingTrailingCenterX(equalTo: view, for: [howItWorksView])

        NSLayoutConstraint.activate([
            howItWorksView.topAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.bottomAnchor, constant: -Layout.howItWorksViewInitialBottomSpace)
        ])
        
        NSLayoutConstraint.constraintAllEdges(from: dimmingView, to: view)
    }
    
    @objc
    private func close(_ barButton: UIBarButtonItem) {
        scanner?.prepareForDismiss()
        dismiss(animated: true, completion: nil)
    }
    
    private func setupScannerNavigationBarStyle() {
        let logoImage = UIImageView(image: #imageLiteral(resourceName: "atm24PicPayLogo"))
        logoImage.contentMode = .scaleAspectFit
        navigationItem.titleView = logoImage
        let barButton = UIBarButtonItem(title: DefaultLocalizable.btCancel.text, style: .plain, target: self, action: #selector(close(_:)))
        barButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: Palette.white.color], for: .normal)
        navigationController?.navigationBar.topItem?.leftBarButtonItem = barButton
        navigationController?.navigationBar.barTintColor = Palette.black.color
    }
}

// MARK: View Model Outputs
extension QRCodeScannerViewController: QRCodeScannerViewModelOutputs {
    func didStartScanner() {
        initScanner()
    }
    
    func proccedToConfirmationStep(withQRCode qrCode: String, selectedValue: Double, cashoutData: CashoutValues) {
        coordinator.perform(action: .confirmation(qrCode: qrCode, selectedValue: selectedValue, cashoutData: cashoutData))
    }
    
    func close() {
        coordinator.perform(action: .close)
    }
}

// MARK: Scanner functions
extension QRCodeScannerViewController {
    private func initScanner() {
        guard scanner == nil else {
            scanner?.start()
            return
        }
        
        scanner = PPScanner(origin: .main, view: scannerFrameView, originPosition: CGPoint.zero, onRead: { [weak self] text in
            self?.scanner?.preventNewReads = true
            self?.viewModel.inputs.onReadQRCode(scannedText: text)
        })
    }
}

extension QRCodeScannerViewController: QRCodeScannerHowItWorksViewDelegate {
    func fireHowItWorksEvent(dragPosition: DraggablePosition) {
        if !howItWorksEventFired && dragPosition == .open {
            howItWorksEventFired = true
            Analytics.shared.log(ATM24Events.howItWorks)
        }
    }

    func didChange(view: QRCodeScannerHowItWorksView, dragPosition: DraggablePosition) {
        navigationController?.navigationBar.alpha = dragPosition == .open ? dragPosition.dimAlpha : 1
        dimmingView.alpha = dragPosition.dimAlpha
        fireHowItWorksEvent(dragPosition: dragPosition)
    }
}
