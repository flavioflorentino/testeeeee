enum QRCodeScannerAction {
    case confirmation(qrCode: String, selectedValue: Double, cashoutData: CashoutValues)
    case close
}

protocol QRCodeScannerCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: QRCodeScannerAction)
}

final class QRCodeScannerCoordinator: QRCodeScannerCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: QRCodeScannerAction) {
        switch action {
        case let .confirmation(qrCode, selectedValue, cashoutData):
            let confirmationVC = CashoutConfirmationFactory.make(withQrCode: qrCode, selectedValue: selectedValue, cashoutData: cashoutData)
            viewController?.navigationController?.setViewControllers([confirmationVC], animated: true)
        case .close:
            viewController?.navigationController?.dismiss(animated: true)
        }
    }
}
