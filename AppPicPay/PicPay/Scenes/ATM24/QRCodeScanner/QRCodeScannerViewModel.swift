import AnalyticsModule
import Foundation

protocol QRCodeScannerViewModelInputs {
    func tryStartScanner()
    func onReadQRCode(scannedText: String)
}

protocol QRCodeScannerViewModelOutputs: AnyObject {
    func didStartScanner()
    func proccedToConfirmationStep(withQRCode qrCode: String, selectedValue: Double, cashoutData: CashoutValues)
    func close()
}

protocol QRCodeScannerViewModelType: AnyObject {
    var inputs: QRCodeScannerViewModelInputs { get }
    var outputs: QRCodeScannerViewModelOutputs? { get set }
}

final class QRCodeScannerViewModel: QRCodeScannerViewModelType, QRCodeScannerViewModelInputs {
    var inputs: QRCodeScannerViewModelInputs { self }
    weak var outputs: QRCodeScannerViewModelOutputs?

    private let service: QRCodeScannerServicing
    let cashoutData: CashoutValues
    let selectedValue: Double
    
    init(service: QRCodeScannerServicing, data: CashoutValues, selectedValue: Double) {
        self.service = service
        self.cashoutData = data
        self.selectedValue = selectedValue
    }

    func tryStartScanner() {
        service.requestCameraPermission { [weak self] status in
            switch status {
            case .authorized:
                self?.outputs?.didStartScanner()
            default:
                break
            }
        }
    }
    
    func onReadQRCode(scannedText: String) {
        Analytics.shared.log(ATM24Events.didScan)
        outputs?.proccedToConfirmationStep(withQRCode: scannedText, selectedValue: selectedValue, cashoutData: cashoutData)
    }
}
