import Foundation
import PermissionsKit

protocol QRCodeScannerServicing {
    func requestCameraPermission(completion: @escaping (_ permission: PermissionStatus) -> Void)
}

final class QRCodeScannerService: QRCodeScannerServicing {
    func requestCameraPermission(completion: @escaping (PermissionStatus) -> Void) {
        let permission = PPPermission.camera(withDeniedAlert: PPPermission.Alert(
            title: Strings.Atm24.cameraDeniedAlertTitle,
            message: Strings.Atm24.cameraDeniedAlertMessage,
            settings: Strings.Atm24.cameraDeniedAlertDestination
        ))
        
        PPPermission.request(permission: permission, { status in
            guard status == .authorized else {
                completion(PermissionStatus.denied)
                return
            }
            completion(status)
        })
    }
}
