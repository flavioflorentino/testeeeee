import SkeletonView
import UI
import UIKit

final class CircularValueOptionCell: UICollectionViewCell {
    static var identifier = "CircularValueOptionCell"
    
    private lazy var backgroundCircularView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale000.color
        view.layer.borderColor = Palette.ppColorGrayscale300.color.cgColor
        view.layer.cornerRadius = contentView.frame.width / 2.0
        view.clipsToBounds = true
        view.layer.masksToBounds = true
        view.isSkeletonable = true
        return view
    }()
    
    private lazy var internalContentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale000.color
        view.layer.cornerRadius = (contentView.frame.width - 6) / 2.0
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorGrayscale300.color
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.text = Strings.Atm24.otherValue
        label.numberOfLines = 2
        label.textAlignment = .center
        label.isHidden = true
        return label
    }()
    
    override var isSelected: Bool {
        didSet(newValue) {
            setSelected(self.isSelected)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        isSkeletonable = true
        addComponents()
        layoutComponents()
        backgroundCircularView.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: Palette.ppColorGrayscale300.color))
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        contentView.addSubview(backgroundCircularView)
        backgroundCircularView.addSubview(internalContentView)
        internalContentView.addSubview(textLabel)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.constraintAllEdges(from: backgroundCircularView, to: contentView)
        NSLayoutConstraint.leadingTrailing(equalTo: backgroundCircularView, for: [internalContentView], constant: 3)
        NSLayoutConstraint.activate([
            internalContentView.topAnchor.constraint(equalTo: backgroundCircularView.topAnchor, constant: 3),
            internalContentView.bottomAnchor.constraint(equalTo: backgroundCircularView.bottomAnchor, constant: -3)
        ])
        NSLayoutConstraint.activate([
            textLabel.centerXAnchor.constraint(equalTo: internalContentView.centerXAnchor),
            textLabel.centerYAnchor.constraint(equalTo: internalContentView.centerYAnchor)
        ])
    }
    
    private func hideAnimationsOfSkeletonView() {
        backgroundCircularView.hideSkeleton()
    }
    
    func configure(withState state: CashoutValueState) {
        hideAnimationsOfSkeletonView()
        switch state {
        case .higherThanZero(let value):
            configureAttributedText(withValue: value)
        case .otherValue:
            textLabel.text = Strings.Atm24.otherValue
        }
        backgroundCircularView.layer.borderWidth = 1
        textLabel.isHidden = false
    }
    
    private func configureAttributedText(withValue value: Double) {
            let attributedText = NSMutableAttributedString(string: "R$\n\(Int(value))")
            let currencyRange = (attributedText.string as NSString).range(of: "R$")
            let valueRange = (attributedText.string as NSString).range(of: "\(Int(value))")
            attributedText.addAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 10)], range: currencyRange)
            attributedText.addAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20)], range: valueRange)
            textLabel.attributedText = attributedText
    }
    
    func setSelected(_ selected: Bool) {
        textLabel.textColor = selected ? Palette.ppColorGrayscale000.color : Palette.ppColorGrayscale300.color
        internalContentView.backgroundColor = selected ? Palette.ppColorBranding300.color : Palette.ppColorGrayscale000.color
        backgroundCircularView.layer.borderColor = selected ? Palette.ppColorBranding300.cgColor : Palette.ppColorGrayscale300.cgColor
    }
}
