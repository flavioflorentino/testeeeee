import Foundation

struct CashoutValuesData {
    var values: [Double]
    var containerFrame: CGRect
}

protocol CashoutValuesViewViewModelInputs {
    func viewConfigure(withFrame frame: CGRect)
    func updateValues(_ values: [Double], withContainerFrame frame: CGRect)
}

protocol CashoutValuesViewViewModelOutputs: AnyObject {
    func updateView(withData data: CashoutValuesData)
    func showLoading(_ loading: Bool)
}

protocol CashoutValuesViewViewModelType: AnyObject {
    var inputs: CashoutValuesViewViewModelInputs { get }
    var outputs: CashoutValuesViewViewModelOutputs? { get set }
}

final class CashoutValuesViewViewModel: CashoutValuesViewViewModelType, CashoutValuesViewViewModelInputs {
    var inputs: CashoutValuesViewViewModelInputs { self }
    weak var outputs: CashoutValuesViewViewModelOutputs?

    func viewConfigure(withFrame frame: CGRect) {
        updateValues([], withContainerFrame: frame)
    }
    
    func updateValues(_ values: [Double], withContainerFrame frame: CGRect) {
        outputs?.showLoading(values.isEmpty)
        outputs?.updateView(withData: CashoutValuesData(values: values, containerFrame: frame))
    }
}
