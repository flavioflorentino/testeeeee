import SkeletonView
import UI
import UIKit

protocol CashoutValuesViewDelegate: AnyObject {
    func didSelectValue(cashoutValuesView: CashoutValuesView, value: Double)
}

final class CashoutValuesView: UIView, UICollectionViewDelegate {
    enum Layout {
        static let cellsPerRow: CGFloat = 4
        static let spaceBetweenCells: CGFloat = 10
        static let marginsSpace: CGFloat = 32
        static let defaultNumberOfCells = 8
    }
    
    lazy var viewModel: CashoutValuesViewViewModelType = {
        let viewModel = CashoutValuesViewViewModel()
        viewModel.outputs = self
        return viewModel
    }()
    
    private lazy var collection: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.isSkeletonable = true
        collectionView.register(CircularValueOptionCell.self, forCellWithReuseIdentifier: CircularValueOptionCell.identifier)
        return collectionView
    }()
    
    weak var delegate: CashoutValuesViewDelegate?
    
    private var cashoutValues = [Double]()
    private var containerViewFrame = CGRect.zero
    private var heightConstraint: NSLayoutConstraint?
    private var widthConstraint: NSLayoutConstraint?
    
    private var cellSize: CGSize {
        if containerViewFrame == .zero {
            return calculaCellSize(withWidth: UIScreen.main.bounds.width - Layout.marginsSpace)
        }
        return calculaCellSize(withWidth: containerViewFrame.width)
    }
    
    private var totalCount: CGFloat {
        CGFloat(!cashoutValues.isEmpty ? cashoutValues.count : Layout.defaultNumberOfCells)
    }
    
    init(containerFrame: CGRect = .zero) {
        super.init(frame: containerFrame)
        isSkeletonable = true
        addComponents()
        layoutComponents()
        viewModel.inputs.viewConfigure(withFrame: containerFrame)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func calculaCellSize(withWidth width: CGFloat) -> CGSize {
        let finalWidth = (width - (Layout.cellsPerRow - 1.0) * Layout.spaceBetweenCells) / Layout.cellsPerRow
        return CGSize(width: finalWidth, height: finalWidth)
    }
    
    private func addComponents() {
        addSubview(collection)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            collection.topAnchor.constraint(equalTo: topAnchor),
            collection.bottomAnchor.constraint(equalTo: bottomAnchor),
            collection.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
    
    func updateValues(with values: [Double], withContainerFrame frame: CGRect = .zero) {
        viewModel.inputs.updateValues(values, withContainerFrame: frame)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard !cashoutValues.isEmpty else {
            return
        }
        delegate?.didSelectValue(cashoutValuesView: self, value: cashoutValues[indexPath.item])
    }
}

extension CashoutValuesView: SkeletonCollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        cashoutValues.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CircularValueOptionCell.identifier, for: indexPath) as? CircularValueOptionCell else {
            return CircularValueOptionCell()
        }
        cell.configure(withState: CashoutValueState(value: cashoutValues[indexPath.item]))
        return cell
    }
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        Layout.defaultNumberOfCells
    }
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        CircularValueOptionCell.identifier
    }
}

extension CashoutValuesView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        Layout.spaceBetweenCells
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        Layout.spaceBetweenCells
    }
}

extension CashoutValuesView: CashoutValuesViewViewModelOutputs {
    func updateView(withData data: CashoutValuesData) {
        containerViewFrame = data.containerFrame
        cashoutValues = data.values
        let rows = ceil(totalCount / Layout.cellsPerRow)
        
        let height = ((rows + 2) * Layout.spaceBetweenCells) + (rows * cellSize.width)
        heightConstraint?.isActive = false
        heightConstraint = collection.heightAnchor.constraint(equalToConstant: height)
        heightConstraint?.isActive = true
        
        widthConstraint?.isActive = false
        widthConstraint = collection.widthAnchor.constraint(equalToConstant: collectionWidth())
        widthConstraint?.isActive = true
        collection.reloadData()
    }
    
    private func collectionWidth() -> CGFloat {
        let selectedCount = cashoutValues.isEmpty ? Layout.cellsPerRow : totalCount
        let finalCount = selectedCount >= 4 ? Layout.cellsPerRow : selectedCount
        let width = (finalCount * cellSize.width) + Layout.marginsSpace
        return width
    }
    
    func showLoading(_ loading: Bool) {
        if loading {
            showAnimatedSkeleton()
        } else {
            hideSkeleton()
        }
    }
}
