import Foundation

enum CashoutValuesFactory {
    static func make() -> CashoutValuesViewController {
        let dependencies = DependencyContainer()
        let service: CashoutValuesServicing = CashoutValuesService(dependencies: dependencies)
        var coordinator: CashoutValuesCoordinating = CashoutValuesCoordinator()
        let viewModel: CashoutValuesViewModelType = CashoutValuesViewModel(service: service, dependencies: dependencies, coordinator: coordinator)
        let viewController = CashoutValuesViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController

        return viewController
    }
}
