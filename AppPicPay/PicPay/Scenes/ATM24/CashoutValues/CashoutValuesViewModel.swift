import AnalyticsModule
import Foundation
import Core
import FeatureFlag

struct CashoutValuesPresenter {
    let cashoutValues: [Double]
    let availableBalance: Double
    let availableFreeWithdrawal: Int
    let withdrawalExhibitionFee: Double
}

protocol CashoutValuesViewModelInputs {
    func loadCashoutValues()
    func loadCashoutValuesIfNeeded()
    func selectCashoutValue(_ value: Double)
    func loadHelpInfo()
    func shouldShowWithdrawTextInfo()
    func next()
    func back()
    func trackDidAcceptFeeLimit()
    func trackDidTapFooterFeeLimit()
    func trackOpenCashoutLimitErrorInfoView()
    func trackTapContinueButtonWhenLimiteExceeded()
    func trackBackToHomeWhenLimiteExceeded()
    func trackTapBackButtonWhenLimiteExceeded()
    func trackCashoutFirstLimitNotification()
}

protocol CashoutValuesViewModelOutputs: AnyObject {
    func showLoading(_ loading: Bool)
    func updateView(with status: LimitErrorStatus?, data: CashoutValuesPresenter)
    func showErrorView(_ error: Error?)
    func showNextStep(step: CashoutValuesAction)
    func showHelpInfo(withData data: CashoutValues)
    func showWithdrawAlert(withLimit limit: Int)
    func showWithdrawTextInfo()
}

protocol CashoutValuesViewModelType: AnyObject {
    var inputs: CashoutValuesViewModelInputs { get }
    var outputs: CashoutValuesViewModelOutputs? { get set }
}

final class CashoutValuesViewModel: CashoutValuesViewModelType, CashoutValuesViewModelInputs {
    typealias Dependency = HasKVStore & HasAnalytics & HasFeatureManager
    var inputs: CashoutValuesViewModelInputs { self }
    weak var outputs: CashoutValuesViewModelOutputs?
    
    private let service: CashoutValuesServicing
    private var shouldLoadCashoutValues = true
    private var cashoutValues: CashoutValues?
    private var dependencies: Dependency
    private var coordinator: CashoutValuesCoordinating
    private var value: Double?
    private var status: LimitErrorStatus?
    private var showWithdrawFeeEnabled: Bool {
        dependencies.featureManager.isActive(.featureShowWithdrawFeeEnabled)
    }
    
    init(service: CashoutValuesServicing, dependencies: Dependency, coordinator: CashoutValuesCoordinating) {
        self.service = service
        self.dependencies = dependencies
        self.coordinator = coordinator
    }
    
    func loadCashoutValues() {
        outputs?.showLoading(true)
        service.loadCashoutValues(success: { [weak self] cashoutValues in
            self?.cashoutValues = cashoutValues
            self?.outputs?.showLoading(false)
            self?.verifyCashout(with: cashoutValues)
            self?.showWithdrawAlertIfNeeded(with: cashoutValues.maxFreeWithdrawalsMonthly)
            }, failure: { [weak self] error in
                self?.outputs?.showLoading(false)
                self?.outputs?.showErrorView(error)
        })
    }
    
    func loadCashoutValuesIfNeeded() {
        if shouldLoadCashoutValues {
            shouldLoadCashoutValues = false
            loadCashoutValues()
        }
    }
    
    func selectCashoutValue(_ value: Double) {
        guard let cashoutData = cashoutValues else {
            return
        }
        if showWithdrawFeeEnabled && cashoutData.availableFreeWithdrawal == 0 {
            self.value = value
            showWithdrawLimitExceeded(with: cashoutData, value: value)
            inputs.trackOpenCashoutLimitErrorInfoView()
            return
        }
        if value == 0 {
            outputs?.showNextStep(step: .otherValues(cashoutData: cashoutData))
        } else {
            Analytics.shared.log(ATM24Events.selectValue(.predifined, selectedValue: value))
            outputs?.showNextStep(step: .scanner(selectedValue: value, cashoutData: cashoutData))
        }
    }
    
    func showWithdrawLimitExceeded(with data: CashoutValues, value: Double) {
        let presenterData = CashoutValuesPresenter(cashoutValues: data.availableOptions,
                                                   availableBalance: data.availableWithdrawalValue,
                                                   availableFreeWithdrawal: data.availableFreeWithdrawal,
                                                   withdrawalExhibitionFee: data.withdrawalExhibitionFee)
        let status = LimitErrorStatus.exceededFreeFeeLimit(
            maxFreeWithdrawalsMonthly: "\(data.maxFreeWithdrawalsMonthly)",
            withdrawalExhibitionFee: "\(data.withdrawalExhibitionFee.stringAmount)"
        )
        self.status = status
        outputs?.updateView(with: status, data: presenterData)
    }
    
    func loadHelpInfo() {
        guard let cashoutData = cashoutValues else {
            return
        }
        outputs?.showHelpInfo(withData: cashoutData)
    }
    
    func shouldShowWithdrawTextInfo() {
        guard showWithdrawFeeEnabled else {
            outputs?.showWithdrawTextInfo()
            return
        }
    }
    
    private func verifyCashout(with data: CashoutValues) {
        let presenterData = CashoutValuesPresenter(cashoutValues: data.availableOptions,
                                                   availableBalance: data.availableWithdrawalValue,
                                                   availableFreeWithdrawal: data.availableFreeWithdrawal,
                                                   withdrawalExhibitionFee: data.withdrawalExhibitionFee)
        
        if data.withdrawalBalance >= data.minWithdrawalValue && data.availableWithdrawalValue < data.minWithdrawalValue {
            Analytics.shared.log(ATM24Events.cannotWithdraw(.dailyLimit))
            outputs?.updateView(with: .dailyLimit, data: presenterData)
        } else if data.withdrawalBalance < data.minWithdrawalValue {
            Analytics.shared.log(ATM24Events.cannotWithdraw(.insufficientFunds))
            outputs?.updateView(with: .insufficientFunds, data: presenterData)
        } else {
            outputs?.updateView(with: nil, data: presenterData)
        }
    }
    
    func next() {
        guard let data = cashoutValues, let value = value else {
            return
        }
        if value == 0.0 {
            outputs?.showNextStep(step: .otherValues(cashoutData: data))
        } else {
            coordinator.perform(action: .scanner(selectedValue: value, cashoutData: data))
        }
    }
    
    func back() {
        coordinator.perform(action: .back)
    }
    
    func trackDidAcceptFeeLimit() {
        dependencies.analytics.log(ATM24Events.popupFeeLimit)
    }
    
    func trackDidTapFooterFeeLimit() {
        dependencies.analytics.log(ATM24Events.footerFeeLimit)
    }
    
    func trackOpenCashoutLimitErrorInfoView() {
        dependencies.analytics.log(ATM24Events.openCashoutLimiteExceeded)
    }
    func trackTapContinueButtonWhenLimiteExceeded() {
        dependencies.analytics.log(ATM24Events.tapContinueLimiteExceeded)
    }
    
    func trackBackToHomeWhenLimiteExceeded() {
        dependencies.analytics.log(ATM24Events.tapBackToHomeLimiteExceeded)
    }
    
    func trackTapBackButtonWhenLimiteExceeded() {
        if case .exceededFreeFeeLimit = status {
            dependencies.analytics.log(ATM24Events.tapBackLimiteExceeded)
        }
    }
    
    func trackCashoutFirstLimitNotification() {
        dependencies.analytics.log(ATM24Events.cashoutFirstLimitNotification)
    }
}

private extension CashoutValuesViewModel {
    enum CashoutValuesUserDefaultKey: String {
        case alreadyShowWithdrawlAlert
    }
    var needToShowWithdrawlAlert: Bool {
        dependencies.kvStore.getFirstTimeOnlyEvent(CashoutValuesUserDefaultKey.alreadyShowWithdrawlAlert.rawValue) == false
    }
    
    func showWithdrawAlertIfNeeded(with freeWithdrawl: Int) {
        if showWithdrawFeeEnabled && needToShowWithdrawlAlert {
            dependencies.kvStore.setFirstTimeOnlyEvent(CashoutValuesUserDefaultKey.alreadyShowWithdrawlAlert.rawValue)
            outputs?.showWithdrawAlert(withLimit: freeWithdrawl)
        }
    }
}
