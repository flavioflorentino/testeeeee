import AnalyticsModule
import SkeletonView
import UI
import UIKit

final class CashoutValuesViewController: LegacyViewController<CashoutValuesViewModelType, CashoutValuesCoordinating, UIView> {
    enum Layout {
        static let headerHeight: CGFloat = 40
    }
    
    private lazy var balanceInfoHeader: BalanceInfoHeader = {
        let header = BalanceInfoHeader()
        header.translatesAutoresizingMaskIntoConstraints = false
        header.isSkeletonable = true
        header.delegate = self
        return header
    }()
    
    private lazy var centerStackView: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.alignment = .center
        stack.axis = .vertical
        stack.distribution = .fill
        stack.spacing = 25
        stack.isSkeletonable = true
        return stack
    }()
    
    private lazy var valueQuestionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = Strings.Atm24.withdrawAmount
        label.textColor = Palette.ppColorGrayscale500.color
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.isSkeletonable = true
        return label
    }()
    
    private lazy var cashoutValuesView: CashoutValuesView = {
        let valuesView = CashoutValuesView()
        valuesView.translatesAutoresizingMaskIntoConstraints = false
        valuesView.delegate = self
        return valuesView
    }()
    
    private lazy var errorView: GenericErrorView = {
        let genericErrorView = GenericErrorView()
        genericErrorView.tryAgainTapped = { [weak self] in
            self?.retryLoading()
        }
        genericErrorView.translatesAutoresizingMaskIntoConstraints = false
        genericErrorView.isHidden = true
        return genericErrorView
    }()
    
    private lazy var limitErrorInfoView: LimitErrorInfoView = {
        let view = LimitErrorInfoView()
        view.delegate = self
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var limitFooterInfoView: LimitFooterInfoView = {
        let footer = LimitFooterInfoView()
        footer.translatesAutoresizingMaskIntoConstraints = false
        footer.isSkeletonable = true
        return footer
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Analytics.shared.log(ATM24Events.valueOptions)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.inputs.loadCashoutValuesIfNeeded()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isMovingFromParent {
            viewModel.inputs.trackTapBackButtonWhenLimiteExceeded()
        }
    }
    
    override func buildViewHierarchy() {
        view.addSubview(balanceInfoHeader)
        view.addSubview(centerStackView)
        centerStackView.addArrangedSubview(valueQuestionLabel)
        centerStackView.addArrangedSubview(cashoutValuesView)
        view.addSubview(errorView)
        view.addSubview(limitErrorInfoView)
        view.addSubview(limitFooterInfoView)
        
        viewModel.inputs.shouldShowWithdrawTextInfo()
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        title = Strings.Atm24.withdrawMoney
        navigationItem.backBarButtonItem = UIBarButtonItem(title: nil, style: .plain, target: nil, action: nil)
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [balanceInfoHeader])
        balanceInfoHeader.heightAnchor.constraint(equalToConstant: Layout.headerHeight).isActive = true
        balanceInfoHeader.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        
        NSLayoutConstraint.leadingTrailingCenterX(equalTo: view, for: [centerStackView], constant: 16)
        centerStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        NSLayoutConstraint.leadingTrailing(equalTo: centerStackView, for: [cashoutValuesView])
        NSLayoutConstraint.constraintAllEdges(from: errorView, to: view)
        
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [limitErrorInfoView])
        NSLayoutConstraint.activate([
            limitErrorInfoView.topAnchor.constraint(equalTo: balanceInfoHeader.bottomAnchor),
            limitErrorInfoView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [balanceInfoHeader])
        limitFooterInfoView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.1).isActive = true
        limitFooterInfoView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    private func presentErrorView(_ error: Error?) {
        DispatchQueue.main.async { [weak self] in
            self?.errorView.isHidden = false
        }
    }
    
    private func retryLoading() {
        errorView.isHidden = true
        viewModel.inputs.loadCashoutValues()
    }
    
    private func updateData(_ cashoutData: CashoutValuesPresenter, forStatus status: LimitErrorStatus?) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapFooter))
        limitFooterInfoView.addGestureRecognizer(tap)
        DispatchQueue.main.async { [weak self] in
            self?.balanceInfoHeader.setValue(cashoutData.availableBalance)
            self?.balanceInfoHeader.setHelpButtonHidden(false)
            self?.limitFooterInfoView.setValue(cashoutData.availableFreeWithdrawal)
            guard let status = status else {
                self?.cashoutValuesView.updateValues(with: cashoutData.cashoutValues)
                return
            }
            self?.valueQuestionLabel.isHidden = true
            self?.limitErrorInfoView.isHidden = false
            self?.centerStackView.isHidden = true
            self?.limitErrorInfoView.setStatus(status)
        }
    }
    
    private func updateLoadingState(_ loading: Bool) {
        DispatchQueue.main.async { [weak self] in
            if loading {
                self?.balanceInfoHeader.setHelpButtonHidden(true)
                self?.view.showAnimatedSkeleton()
            } else {
                self?.view.hideSkeleton()
            }
        }
    }
    
    @objc
    private func close() {
        self.navigationController?.dismiss(animated: true)
    }
}

// MARK: View Model Outputs
extension CashoutValuesViewController: CashoutValuesViewModelOutputs {
    func showWithdrawTextInfo() {
        limitFooterInfoView.isHidden = true
    }
    
    func showLoading(_ loading: Bool) {
        updateLoadingState(loading)
    }
    
    func showErrorView(_ error: Error?) {
        presentErrorView(error)
    }
    
    func updateView(with status: LimitErrorStatus?, data: CashoutValuesPresenter) {
        updateData(data, forStatus: status)
    }
    
    func showNextStep(step: CashoutValuesAction) {
        coordinator.perform(action: step)
    }
    
    func showHelpInfo(withData data: CashoutValues) {
        coordinator.perform(action: .help(cashoutData: data))
    }
    
    private func getWithdrawText(fromLimitValue limitValue: Int) -> String {
        guard limitValue > 1 else {
            return Strings.Atm24.limitAdvice(limitValue)
        }
        return Strings.Atm24.limitAdvices(limitValue)
    }
    
    func showWithdrawAlert(withLimit limit: Int) {
        let popup = PopupViewController(
            title: Strings.Atm24.withdrawLimitTitle,
            description: getWithdrawText(fromLimitValue: limit),
            preferredType: .image(Assets.WithdrawIcons.iconSaqueP.image)
        )
        popup.hideCloseButton = true
        let action = PopupAction(title: Strings.Atm24.understood, style: .fill) {
            self.viewModel.inputs.trackDidAcceptFeeLimit()
        }
        popup.addAction(action)
        showPopup(popup)
        viewModel.inputs.trackCashoutFirstLimitNotification()
    }
}

extension CashoutValuesViewController: CashoutValuesViewDelegate {
    func didSelectValue(cashoutValuesView: CashoutValuesView, value: Double) {
        viewModel.inputs.selectCashoutValue(value)
    }
}

extension CashoutValuesViewController: BalanceInfoHeaderDelegate {
    func didTouchHelpButton() {
        viewModel.inputs.loadHelpInfo()
    }
}

extension CashoutValuesViewController: LimitErrorInfoViewDelegate {
    func didTapProceed() {
        viewModel.inputs.next()
        viewModel.inputs.trackTapContinueButtonWhenLimiteExceeded()
    }
    func didTapToBackCashoutOptions() {
        viewModel.inputs.back()
        viewModel.inputs.trackBackToHomeWhenLimiteExceeded()
    }
}

extension CashoutValuesViewController: LimitFooterInfoViewDelegate {
    @objc
    func didTapFooter() {
        viewModel.inputs.trackDidTapFooterFeeLimit()
    }
}
