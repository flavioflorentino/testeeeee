import Foundation

public struct CashoutValues: Decodable, Equatable {
    enum CodingKeys: String, CodingKey {
        case availableWithdrawalValue = "available_withdrawal_value"
        case withdrawalBalance = "withdrawal_balance"
        case dailyWithdrawalLimit = "daily_withdrawal_limit"
        case maxWithdrawalValue = "max_withdrawal_value"
        case minWithdrawalValue = "min_withdrawal_value"
        case maxFreeWithdrawalsMonthly = "max_num_free_withdrawals_monthly"
        case withdrawalFee = "withdrawal_fee"
        case availableOptions = "options"
        case availableFreeWithdrawal = "available_num_free_withdrawals"
        case withdrawalExhibitionFee = "withdrawal_exhibition_fee"
    }
    
    let availableWithdrawalValue: Double
    let withdrawalBalance: Double
    let dailyWithdrawalLimit: Double
    let maxWithdrawalValue: Double
    let minWithdrawalValue: Double
    let maxFreeWithdrawalsMonthly: Int
    let withdrawalFee: Double
    let availableOptions: [Double]
    let availableFreeWithdrawal: Int
    let withdrawalExhibitionFee: Double
}

enum CashoutValueState {
    case higherThanZero(value: Double)
    case otherValue
    
    init(value: Double) {
        if value > 0 {
            self = .higherThanZero(value: value)
        } else {
            self = .otherValue
        }
    }
}
