import UIKit

enum CashoutValuesAction {
    case scanner(selectedValue: Double, cashoutData: CashoutValues)
    case otherValues(cashoutData: CashoutValues)
    case help(cashoutData: CashoutValues)
    case back
}

protocol CashoutValuesCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: CashoutValuesAction)
}

final class CashoutValuesCoordinator: CashoutValuesCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: CashoutValuesAction) {
        switch action {
        case let .scanner(selectedValue, cashoutData):
            let scannerVC = QRCodeScannerFactory.make(withData: cashoutData, selectedValue: selectedValue)
            let navigation = UINavigationController(rootViewController: scannerVC)
            navigation.modalPresentationStyle = .fullScreen
            viewController?.present(navigation, animated: true)
        case .otherValues(let cashoutData):
            let otherValuesVC = OtherCashoutValuesFactory.make(withData: cashoutData)
            viewController?.navigationController?.pushViewController(otherValuesVC, animated: true)
        case .help(let cashoutData):
            let limitInfoVC = CashoutLimitsFactory.make(withCashoutData: cashoutData)
            viewController?.navigationController?.pushViewController(limitInfoVC, animated: true)
        case .back:
            viewController?.navigationController?.popViewController(animated: true)
        }
    }
}
