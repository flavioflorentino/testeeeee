import Core
import Foundation

protocol CashoutValuesServicing {
    func loadCashoutValues(success: @escaping (_ cashoutValues: CashoutValues) -> Void, failure: @escaping (_ error: Error) -> Void)
}

final class CashoutValuesService: CashoutValuesServicing {
    typealias Dependency = HasMainQueue
    private let dependencies: Dependency
    
    init(dependencies: Dependency) {
        self.dependencies = dependencies
    }
    
    func loadCashoutValues(success: @escaping (CashoutValues) -> Void, failure: @escaping (Error) -> Void) {
        Api<CashoutValues>(endpoint: ATM24Endpoint.cashout).execute { result in
            self.dependencies.mainQueue.async {
                let mappedResult = result.map { $0.model }
    
                switch mappedResult {
                case .success(let cashout):
                    success(cashout)
                case .failure(let error):
                    failure(error)
                }
            }
        }
    }
}
