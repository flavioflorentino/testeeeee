import AnalyticsModule
import UI
import UIKit

final class ATM24OfferPopup: UIViewController {
    typealias ATM24PopupButtonAction = () -> Void
    
    enum Layout {
        static let imageSize: CGFloat = 72
        static let imageTopOffset: CGFloat = 35
        static let titleHorizontalMargin: CGFloat = 60
        static let subImageHorizontalMargin: CGFloat = 76
        static let smallTopOffset: CGFloat = 10
        static let defaultHorizontalOffset: CGFloat = 16
        static let popupTextHorizontalMargin: CGFloat = 24
        static let popupTextTopOffset: CGFloat = 16
        static let confirmButtonHeight: CGFloat = 48
        static let confirmButtonTopOffset: CGFloat = 16
        static let cancelButtonTopOffset: CGFloat = 12
        static let cancelButtonBottomOffset: CGFloat = 48
    }
    
    let confirmAction: ATM24PopupButtonAction?
    let cancelAction: ATM24PopupButtonAction?
    
    private lazy var popupImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = #imageLiteral(resourceName: "icon-saque-p")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var popupTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = Strings.Atm24.popupOfferTitle
        label.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        label.textColor = Palette.ppColorGrayscale600.color
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var popupSubImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = #imageLiteral(resourceName: "logo-banco24h")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var popupText: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = Strings.Atm24.popupOfferText
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var popupConfirmButton: UIPPButton = {
        let button = UIPPButton(title: Strings.Atm24.withdrawMoney, target: self, action: #selector(confirmButtonTapped), type: .cta)
        button.cornerRadius = 24
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var popupCancelButton: UIPPButton = {
        let button = UIPPButton(title: Strings.Atm24.notNow, target: self, action: #selector(cancelButtonTapped), type: .cleanUnderlined)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    init(confirmAction: ATM24PopupButtonAction?, cancelAction: ATM24PopupButtonAction?) {
        self.confirmAction = confirmAction
        self.cancelAction = cancelAction
        super.init(nibName: nil, bundle: nil)
        view.frame = .zero
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Palette.ppColorGrayscale000.color
        addComponents()
        layoutComponents()
        Analytics.shared.log(ATM24Events.popupOffer)
    }
    
    func addComponents() {
        view.addSubview(popupImage)
        view.addSubview(popupTitle)
        view.addSubview(popupSubImage)
        view.addSubview(popupText)
        view.addSubview(popupConfirmButton)
        view.addSubview(popupCancelButton)
    }
    
    func layoutComponents() {
        NSLayoutConstraint.activate([
            popupImage.widthAnchor.constraint(equalToConstant: Layout.imageSize),
            popupImage.heightAnchor.constraint(equalToConstant: Layout.imageSize),
            popupImage.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            popupImage.topAnchor.constraint(equalTo: view.topAnchor, constant: Layout.imageTopOffset)
        ])
        
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [popupTitle], constant: Layout.titleHorizontalMargin)
        
        NSLayoutConstraint.activate([
            popupTitle.topAnchor.constraint(equalTo: popupImage.bottomAnchor, constant: Layout.smallTopOffset),
            popupTitle.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
        
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [popupSubImage], constant: Layout.subImageHorizontalMargin)
        NSLayoutConstraint.activate([
            popupSubImage.topAnchor.constraint(equalTo: popupTitle.bottomAnchor, constant: Layout.smallTopOffset),
            popupSubImage.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
        
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [popupText], constant: Layout.popupTextHorizontalMargin)
        NSLayoutConstraint.activate([
            popupText.topAnchor.constraint(equalTo: popupSubImage.bottomAnchor, constant: Layout.popupTextTopOffset),
            popupText.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
        
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [popupConfirmButton], constant: Layout.defaultHorizontalOffset)
        NSLayoutConstraint.activate([
            popupConfirmButton.topAnchor.constraint(equalTo: popupText.bottomAnchor, constant: Layout.confirmButtonTopOffset),
            popupConfirmButton.heightAnchor.constraint(equalToConstant: Layout.confirmButtonHeight),
            popupConfirmButton.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            popupCancelButton.topAnchor.constraint(equalTo: popupConfirmButton.bottomAnchor, constant: Layout.cancelButtonTopOffset),
            popupCancelButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            popupCancelButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Layout.cancelButtonBottomOffset)
        ])
    }
    
    @objc
    private func confirmButtonTapped() {
        dismiss(animated: true) { [weak self] in
            self?.confirmAction?()
        }
    }
    
    @objc
    private func cancelButtonTapped() {
        dismiss(animated: true) { [weak self] in
            self?.cancelAction?()
        }
    }
}
