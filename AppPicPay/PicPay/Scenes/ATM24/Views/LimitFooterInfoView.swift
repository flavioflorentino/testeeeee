import SkeletonView
import UI
import UIKit

protocol LimitFooterInfoViewDelegate: AnyObject {
    func didTapFooter()
}

final class LimitFooterInfoView: UIView {
    enum Layout {
        static let infoLabelHorizontalMargin: CGFloat = 77
        static let infoLabelVerticalMargin: CGFloat = 12
    }
    
    private lazy var limitLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.text = Strings.Atm24.availableFreeWithdrawals
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = UIFont.systemFont(ofSize: 12)
        label.isSkeletonable = true
        return label
    }()
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale300.color
        return view
    }()
        
    init() {
        super.init(frame: CGRect.zero)
        addComponents()
        layoutComponents()
        configureViews()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addComponents() {
        addSubviews(limitLabel, separatorView)
    }
    
    func layoutComponents() {
        NSLayoutConstraint.activate([
            limitLabel.leadingAnchor.constraint(lessThanOrEqualTo: leadingAnchor, constant: Layout.infoLabelHorizontalMargin),
            limitLabel.trailingAnchor.constraint(greaterThanOrEqualTo: trailingAnchor, constant: -Layout.infoLabelHorizontalMargin),
            separatorView.topAnchor.constraint(equalTo: topAnchor, constant: Layout.infoLabelVerticalMargin),
            separatorView.bottomAnchor.constraint(equalTo: limitLabel.topAnchor, constant: -Layout.infoLabelVerticalMargin)
        ])
        limitLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        NSLayoutConstraint.leadingTrailing(equalTo: self, for: [separatorView])
        separatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
    
    func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    private func getWithdrawText(fromLimitValue limitValue: Int) -> String {
        guard limitValue > 1 else {
            return Strings.Atm24.withdraw
        }
        return Strings.Atm24.withdrawals
    }
    
    func setValue(_ value: Int) {
        let withdrawText = getWithdrawText(fromLimitValue: value)
        let attributedText = NSMutableAttributedString(string: "\(Strings.Atm24.availableFreeWithdrawals)\(value)\(withdrawText)")
        let range = (attributedText.string as NSString).range(of: "\(value)\(withdrawText)")
        attributedText.addAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: Palette.ppColorBranding300.color], range: range)
        limitLabel.attributedText = attributedText
    }
}
