import SkeletonView
import UI
import UIKit

protocol BalanceInfoHeaderDelegate: AnyObject {
    func didTouchHelpButton()
}

final class BalanceInfoHeader: UIView {
    enum Layout {
        static let infoLabelHorizontalMargin: CGFloat = 77
        static let infoLabelVerticalMargin: CGFloat = 12
    }
    private lazy var balanceInfoLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.text = Strings.Atm24.availableBalance
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = UIFont.systemFont(ofSize: 12)
        label.isSkeletonable = true
        return label
    }()
    
    private lazy var helpButton: UIButton = {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(#imageLiteral(resourceName: "green_help_button"), for: .normal)
        button.addTarget(self, action: #selector(helpButtonTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale300.color
        return view
    }()
    
    weak var delegate: BalanceInfoHeaderDelegate?
    
    init() {
        super.init(frame: CGRect.zero)
        addComponents()
        layoutComponents()
        configureViews()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addComponents() {
        addSubview(balanceInfoLabel)
        addSubview(helpButton)
        addSubview(separatorView)
    }
    
    func layoutComponents() {
        NSLayoutConstraint.activate([
            balanceInfoLabel.leadingAnchor.constraint(lessThanOrEqualTo: leadingAnchor, constant: Layout.infoLabelHorizontalMargin),
            balanceInfoLabel.trailingAnchor.constraint(greaterThanOrEqualTo: trailingAnchor, constant: -Layout.infoLabelHorizontalMargin),
            balanceInfoLabel.topAnchor.constraint(equalTo: topAnchor, constant: Layout.infoLabelVerticalMargin),
            balanceInfoLabel.bottomAnchor.constraint(equalTo: separatorView.topAnchor, constant: -Layout.infoLabelVerticalMargin)
        ])
        balanceInfoLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        NSLayoutConstraint.activate([
            helpButton.centerYAnchor.constraint(equalTo: balanceInfoLabel.centerYAnchor),
            helpButton.leftAnchor.constraint(equalTo: balanceInfoLabel.rightAnchor, constant: 4)
        ])
        
        NSLayoutConstraint.leadingTrailing(equalTo: self, for: [separatorView])
        separatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
    
    func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    func setValue(_ value: Double) {
        let attributedText = NSMutableAttributedString(string: "\(Strings.Atm24.availableForWithdraw)\(value.stringAmount)")
        let range = (attributedText.string as NSString).range(of: "\(value.stringAmount)")
        attributedText.addAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 12)], range: range)
        balanceInfoLabel.attributedText = attributedText
    }
    
    func setHelpButtonHidden(_ hidden: Bool) {
        helpButton.isHidden = hidden
    }
    
    @objc
    private func helpButtonTapped(_ sender: UIButton) {
        delegate?.didTouchHelpButton()
    }
}
