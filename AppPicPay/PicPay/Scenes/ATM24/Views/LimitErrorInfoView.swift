import SnapKit
import UI
import UIKit

protocol LimitErrorInfoViewDelegate: AnyObject {
    func didTapProceed()
    func didTapToBackCashoutOptions()
}

private extension LimitErrorInfoView.Layout {
    enum Size {
        static let imageSize: CGFloat = 80
        static let subImageSize: CGFloat = 28
        static let proceedButtonWidth: CGFloat = 228
    }
}

enum LimitErrorStatus {
    case dailyLimit
    case insufficientFunds
    case exceededFreeFeeLimit(maxFreeWithdrawalsMonthly: String, withdrawalExhibitionFee: String)
    
    var image: UIImage {
        switch self {
        case .dailyLimit:
            return #imageLiteral(resourceName: "icon-withdraw-wait-mark")
        case .insufficientFunds:
            return #imageLiteral(resourceName: "icon-withdraw-no-limit-mark")
        case .exceededFreeFeeLimit:
            return #imageLiteral(resourceName: "icon-withdraw-wait-mark")
        }
    }
    
    var infoTitle: String {
        switch self {
        case .dailyLimit:
            return Strings.Atm24.dailyLimitReachedTitle
        case .insufficientFunds:
            return Strings.Atm24.insufficientFunds
        case .exceededFreeFeeLimit:
            return Strings.Atm24.freeFeeLimitExceededTitle
        }
    }
    
    var infoSubtitle: String {
        switch self {
        case .dailyLimit:
            return Strings.Atm24.dailyLimitReachedSubtitle
        case .insufficientFunds:
            return Strings.Atm24.mininumValueForWithdraw
        case let .exceededFreeFeeLimit(maxFreeWithdrawalsMonthly, withdrawalExhibitionFee):
            return Strings.Atm24.freeFeeLimitExceededSubtitle(maxFreeWithdrawalsMonthly, withdrawalExhibitionFee)
        }
    }
}

final class LimitErrorInfoView: UIView {
    fileprivate enum Layout { }
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = #imageLiteral(resourceName: "icon-withdraw-unavailable")
        return imageView
    }()
    
    private lazy var subIconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = #imageLiteral(resourceName: "icon-withdraw-wait-mark")
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        label.numberOfLines = 0
        label.textColor = Palette.ppColorGrayscale600.color
        label.textAlignment = .center
        label.text = Strings.Atm24.dailyLimitReachedTitle
        return label
    }()
    
    private lazy var subTitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = Palette.ppColorGrayscale400.color
        label.textAlignment = .center
        label.text = Strings.Atm24.dailyLimitReachedSubtitle
        label.numberOfLines = 0
        return label
    }()
    
    weak var delegate: LimitErrorInfoViewDelegate?
    
    private lazy var proceedButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.Atm24.proceed, for: .normal)
        button.addTarget(self, action: #selector(didTapProceedButton), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()
    
    private lazy var backToCashoutOptionsButton: UIButton = {
        let button = UIButton()
        button.setTitle(Strings.Atm24.backToStart, for: .normal)
        button.addTarget(self, action: #selector(didTapBackToCashoutOptionsButton), for: .touchUpInside)
        button.buttonStyle(LinkButtonStyle())
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setStatus(_ status: LimitErrorStatus) {
        subIconImageView.image = status.image
        titleLabel.text = status.infoTitle
        subTitleLabel.text = status.infoSubtitle
        
        if case .exceededFreeFeeLimit = status {
            proceedButton.isHidden = false
        } else {
            proceedButton.isHidden = true
        }
    }
}

extension LimitErrorInfoView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubviews(imageView, subIconImageView, titleLabel, subTitleLabel, proceedButton, backToCashoutOptionsButton)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base03)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
        }
        
        subTitleLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
        }
        
        imageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.imageSize)
        }
        
        subIconImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.subImageSize)
            $0.top.equalTo(imageView.snp.bottom).offset(-Spacing.base04)
            $0.trailing.equalTo(imageView).offset(-Spacing.base00)
        }
        
        subIconImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.subImageSize)
            $0.top.equalTo(imageView.snp.bottom).offset(-Spacing.base04)
            $0.trailing.equalTo(imageView).offset(-Spacing.base00)
        }
        
        proceedButton.snp.makeConstraints {
            $0.top.equalTo(subTitleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
        }
        
        backToCashoutOptionsButton.snp.makeConstraints {
            $0.top.equalTo(proceedButton.snp.bottom).offset(Spacing.base03)
            $0.centerX.equalToSuperview()
        }
    }
}

@objc
private extension LimitErrorInfoView {
    func didTapProceedButton() {
        delegate?.didTapProceed()
    }

    func didTapBackToCashoutOptionsButton() {
        delegate?.didTapToBackCashoutOptions()
    }
}
