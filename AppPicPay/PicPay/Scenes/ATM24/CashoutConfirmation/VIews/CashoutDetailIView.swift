import UI
import UIKit

final class CashoutDetailIView: UIView {
    private lazy var withdrawValueInfoLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = Strings.Atm24.withdrawValue
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = Palette.ppColorGrayscale400.color
        return label
    }()
    
    private lazy var withdrawValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = Palette.ppColorGrayscale400.color
        return label
    }()
    
    private lazy var withdrawFeeInfoLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = Strings.Atm24.withdrawFee
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = Palette.ppColorGrayscale400.color
        return label
    }()
    
    private lazy var withdrawFeeValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = Palette.ppColorGrayscale400.color
        return label
    }()
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale200.color
        return view
    }()
    
    private lazy var withdrawTotalInfoLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = Strings.Atm24.totalValue
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = Palette.ppColorGrayscale400.color
        return label
    }()
    
    private lazy var withdrawTotalValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = Palette.ppColorGrayscale400.color
        return label
    }()
    
    init() {
        super.init(frame: .zero)
        addComponents()
        layoutComponents()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        addSubview(withdrawValueInfoLabel)
        addSubview(withdrawValueLabel)
        addSubview(withdrawFeeInfoLabel)
        addSubview(withdrawFeeValueLabel)
        addSubview(separatorView)
        addSubview(withdrawTotalInfoLabel)
        addSubview(withdrawTotalValueLabel)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            withdrawValueInfoLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            withdrawValueInfoLabel.topAnchor.constraint(equalTo: topAnchor)
        ])
        
        NSLayoutConstraint.activate([
            withdrawValueLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            withdrawValueLabel.centerYAnchor.constraint(equalTo: withdrawValueInfoLabel.centerYAnchor)
        ])
        
        NSLayoutConstraint.activate([
            withdrawFeeInfoLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            withdrawFeeInfoLabel.topAnchor.constraint(equalTo: withdrawValueInfoLabel.bottomAnchor, constant: 5)
        ])
        
        NSLayoutConstraint.activate([
            withdrawFeeValueLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            withdrawFeeValueLabel.centerYAnchor.constraint(equalTo: withdrawFeeInfoLabel.centerYAnchor)
        ])
        
        NSLayoutConstraint.leadingTrailing(equalTo: self, for: [separatorView])
        NSLayoutConstraint.activate([
            separatorView.topAnchor.constraint(equalTo: withdrawFeeInfoLabel.bottomAnchor, constant: 5),
            separatorView.heightAnchor.constraint(equalToConstant: 1)
        ])
        
        NSLayoutConstraint.activate([
            withdrawTotalInfoLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            withdrawTotalInfoLabel.topAnchor.constraint(equalTo: separatorView.bottomAnchor, constant: 5),
            withdrawTotalInfoLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20)
        ])
        
        NSLayoutConstraint.activate([
            withdrawTotalValueLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            withdrawTotalValueLabel.centerYAnchor.constraint(equalTo: withdrawTotalInfoLabel.centerYAnchor)
        ])
    }
    
    func setInfo(selectedValue: Double, fee: Double) {
        let total = selectedValue + fee
        withdrawValueLabel.text = selectedValue.stringAmount
        withdrawFeeValueLabel.text = fee.stringAmount
        withdrawTotalValueLabel.text = total.stringAmount
    }
}
