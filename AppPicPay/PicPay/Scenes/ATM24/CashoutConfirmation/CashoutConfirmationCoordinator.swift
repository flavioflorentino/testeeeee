import FeatureFlag
import UIKit

enum CashoutConfirmationAction: Equatable {
    case cancel
    case finish
    case help(_ cashoutData: CashoutValues)
    case backToValues
    case contactHelpCenter
}

protocol CashoutConfirmationCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: CashoutConfirmationAction)
}

final class CashoutConfirmationCoordinator: CashoutConfirmationCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: CashoutConfirmationAction) {
        switch action {
        case .cancel:
            viewController?.dismiss(animated: true)
        case .finish:
            NotificationCenter.default.post(
                name: NSNotification.Name(rawValue: FeedViewController.Observables.FeedNeedReload.rawValue),
                object: nil
            )
            
            guard let vc = firstViewControllerOnHierarchy(ofType: ControllerPagination.self) else {
                viewController?.dismiss(animated: true)
                return
            }
            
            // show first Tab
            if let pagination = vc as? ControllerPagination,
               let mainTab = pagination.children.last as? MainTabBarController {
                mainTab.showHomeScreen()
            }
            vc.dismiss(animated: true)
        case .help(let cashoutData):
            let limitInfoVC = CashoutLimitsFactory.make(withCashoutData: cashoutData)
            viewController?.navigationController?.pushViewController(limitInfoVC, animated: true)
        case .backToValues:
            viewController?.navigationController?.dismiss(animated: true)
        case .contactHelpCenter:
            guard let url = URL(string: helpCenterDeepLink) else { return }
            DeeplinkHelper.handleDeeplink(withUrl: url)
        }
    }
    
    private func firstViewControllerOnHierarchy(ofType type: UIViewController.Type) -> UIViewController? {
        guard var presentingVC = viewController?.presentingViewController else {
            return nil
        }
        
        while !presentingVC.isKind(of: type) {
            if presentingVC.isKind(of: UINavigationController.self) {
                presentingVC = (presentingVC as? UINavigationController)?.viewControllers.first ?? presentingVC
            } else {
                presentingVC = presentingVC.presentingViewController ?? presentingVC
            }
        }
        return presentingVC
    }
}

private extension CashoutConfirmationCoordinator {
    var helpCenterDeepLink: String {
        let reasonTag = FeatureManager.text(.cashoutHelpcenterContactReasontag)
        return "picpay://picpay/helpcenter/reason/\(reasonTag)"
    }
}
