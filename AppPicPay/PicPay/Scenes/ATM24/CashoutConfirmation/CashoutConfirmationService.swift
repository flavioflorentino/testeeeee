import Core
import Foundation

protocol CashoutConfirmationServicing {
    func authenticateUser(completion: @escaping (_ pin: String?) -> Void)
    func authorizeWithdraw(
        withData data: AuthorizationInfoRequestData,
        completion: @escaping (Result<WithdrawAuthorization, Error>) -> Void
    )
    func verifyTransactionStatus(
        forTransactionId id: String,
        completion: @escaping (Result<WithdrawTransactionStatus, Error>) -> Void
    )
}

final class CashoutConfirmationService: CashoutConfirmationServicing {
    typealias Dependency = HasMainQueue
    private var dependencies: Dependency
    private var auth: PPAuth?
    
    init(dependencies: Dependency) {
        self.dependencies = dependencies
    }
    
    func authenticateUser(completion: @escaping (String?) -> Void) {
        auth = PPAuth.authenticate({ pin, _ in
            completion(pin)
        }, canceledByUserBlock: nil)
    }

    func authorizeWithdraw(
        withData data: AuthorizationInfoRequestData,
        completion: @escaping (Result<WithdrawAuthorization, Error>) -> Void
    ) {
        Api<WithdrawAuthorization>(endpoint: ATM24Endpoint.authorization(data: data)).execute { result in
            self.dependencies.mainQueue.async {
                let mappedResult = result
                    .map { $0.model }
                    .mapError { self.parseError($0) ?? $0 }
                
                completion(mappedResult)
            }
        }
    }
    func verifyTransactionStatus(
        forTransactionId id: String,
        completion: @escaping (Result<WithdrawTransactionStatus, Error>) -> Void
    ) {
        Api<WithdrawTransactionStatus>(endpoint: ATM24Endpoint.verifyStatus(transactionId: id)).execute { result in
            self.dependencies.mainQueue.async {
                let mappedResult = result
                    .map { $0.model }
                    .mapError { self.parseError($0) ?? $0 }
                
                completion(mappedResult)
            }
        }
    }
    
    private func parseError(_ apiError: ApiError) -> Error? {
        switch apiError {
        case .unauthorized(let responseBody), .badRequest(let responseBody):
            guard let data = responseBody.jsonData,
                let authError: AuthorizationError = data.decoded() else {
                    return apiError
            }
            return authError
        default:
            return apiError
        }
    }
}
