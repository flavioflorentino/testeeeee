import Foundation

enum CashoutConfirmationFactory {
    static func make(
        withQrCode code: String,
        selectedValue: Double,
        cashoutData: CashoutValues
    ) -> CashoutConfirmationViewController {
        let dependencies = DependencyContainer()
        let service: CashoutConfirmationServicing = CashoutConfirmationService(dependencies: dependencies)
        var coordinator: CashoutConfirmationCoordinating = CashoutConfirmationCoordinator()
        let presenter: CashoutConfirmationPresenting = CashoutConfirmationPresenter(coordinator: coordinator)
        let interactor: CashoutConfirmationInteracting = CashoutConfirmationInteractor(
            dependencies: dependencies,
            service: service,
            presenter: presenter,
            qrCode: code,
            selectedValue: selectedValue,
            cashoutData: cashoutData
        )
        let viewController = CashoutConfirmationViewController(viewModel: interactor)
        
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
