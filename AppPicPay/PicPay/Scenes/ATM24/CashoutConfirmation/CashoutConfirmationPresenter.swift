import Foundation
import UI
import UIKit

struct CashoutDataInteractor: Equatable {
    let selectedValue: Double
    let withdrawalFee: Double
    let selectedValueAttributedText: NSAttributedString
    
    static func == (lhs: CashoutDataInteractor, rhs: CashoutDataInteractor) -> Bool {
        lhs.selectedValue == rhs.selectedValue &&
            lhs.withdrawalFee == rhs.withdrawalFee &&
            lhs.selectedValueAttributedText == rhs.selectedValueAttributedText
    }
}

protocol CashoutConfirmationPresenting: AnyObject {
    var viewController: CashoutConfirmationDisplay? { get set }
    func updateView(selectedValue: Double, withdrawFee: Double)
    func showLoading(_ loading: Bool)
    func showWithdrawSuccess()
    func showLastFeeWithdrawSuccessWithAlert()
    func showSecondToLastFeeWithdrawSuccessWithAlert()
    func perform(action: CashoutConfirmationAction)
    func show(error: ATM24CodeError)
}

final class CashoutConfirmationPresenter: CashoutConfirmationPresenting {
    private let coordinator: CashoutConfirmationCoordinating
    weak var viewController: CashoutConfirmationDisplay?
    
    init(coordinator: CashoutConfirmationCoordinating) {
        self.coordinator = coordinator
    }
    
    func updateView(selectedValue: Double, withdrawFee: Double) {
        let attributedText = configureCashoutLabelAttributes(withValue: selectedValue)
        let data = CashoutDataInteractor(
            selectedValue: selectedValue,
            withdrawalFee: withdrawFee,
            selectedValueAttributedText: attributedText
        )
        viewController?.updateView(withData: data)
    }
    
    func showLoading(_ loading: Bool) {
        viewController?.showLoading(loading)
    }
    
    func showWithdrawSuccess() {
        viewController?.showWithdrawSuccess()
    }
    
    func showLastFeeWithdrawSuccessWithAlert() {
        viewController?.showWithdrawSuccessWithAlert(
            subtitle: Strings.Atm24.lastWithdrawl,
            icon: Assets.WithdrawIcons.iconWithdrawSadMoney.image
        )
    }
    
    func showSecondToLastFeeWithdrawSuccessWithAlert() {
        viewController?.showWithdrawSuccessWithAlert(
            subtitle: Strings.Atm24.secondToLast,
            icon: Assets.WithdrawIcons.iconWithdrawSmile.image
        )
    }
    
    func perform(action: CashoutConfirmationAction) {
        coordinator.perform(action: action)
    }

    func show(error: ATM24CodeError) {
        switch error.presentationType {
        case .dialog:
            viewController?.showDialogError(error: error)
        case .screen:
            viewController?.showScreenError(error: error)
        }
    }
}

private extension CashoutConfirmationPresenter {
    func configureCashoutLabelAttributes(withValue value: Double) -> NSAttributedString {
        let symbol = Strings.Atm24.currencySymbol
        let completeText = "\(value.stringAmount)"
        
        let valueString = value.stringAmount
            .replacingOccurrences(of: symbol, with: "")
            .replacingOccurrences(of: " ", with: "")
        
        let rangeOfValue = (completeText as NSString).range(of: valueString)
        let rangeOfSymbol = (completeText as NSString).range(of: symbol)
        
        let attributedString = NSMutableAttributedString(string: completeText)
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 52), range: rangeOfValue)
        attributedString.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: 22), range: rangeOfSymbol)
        attributedString.addAttribute(.baselineOffset, value: NSNumber(20), range: rangeOfSymbol)
        return attributedString
    }
}
