import AnalyticsModule
import Core
import Foundation
import FeatureFlag

protocol CashoutConfirmationInteracting {
    func showDataToView()
    func confirmWithdraw()
    func verifyTransactionStatus()
    func loadHelp()
    func acceptedTransaction()
    func cancelTransaction()
    func trackAlert()
    func cancel()
    func finish()
    func performPrimaryActionError(withId: String)
    func performSecondaryActionError(withId: String)
    func perform(errorAction: CashoutConfirmationAction)
}

final class CashoutConfirmationInteractor: CashoutConfirmationInteracting {
    typealias Dependencies = HasAnalytics & HasFeatureManager

    private let service: CashoutConfirmationServicing
    private let presenter: CashoutConfirmationPresenting
    private let qrCode: String
    private let cashoutData: CashoutValues
    private let selectedValue: Double
    private var authorization: WithdrawAuthorization?
    private let dependencies: Dependencies
    private var timer: RepeatingTimer?
    private var startTime: Date?
    private let timeout: Double
    private let requestInterval: Double
    private var showWithdrawFeeEnabled: Bool {
        dependencies.featureManager.isActive(.featureShowWithdrawFeeEnabled)
    }

    private lazy var errorsManager: ATM24ErrorsManager = {
        ATM24ErrorsManager(errors: [
            ATM24ResetPasswordError(),
            ATM24DuplicatedTransactionUUIDError(),
            ATM24InvalidQRCodeError(),
            ATM24LegacyGetAvailableWithdrawFailError(),
            ATM24LegacyCreateCashoutMovementFailError(),
            ATM24OriginalCommunicationFailError(),
            ATM24InvalidNotesError(),
            ATM24UnavailableNotesError(),
            ATM24ConsumerWithRestrictionsError(),
            ATM24OriginalDeniesWithdrawError(),
            ATM24WithdrawDisabledError(),
            ATM24TechnicalProblemError(),
            ATM24UnableToGetLockerForWithdrawError()
        ])
    }()
    
    init(dependencies: Dependencies,
         service: CashoutConfirmationServicing,
         presenter: CashoutConfirmationPresenting,
         qrCode: String,
         selectedValue: Double,
         cashoutData: CashoutValues,
         requestInterval: Double = 5.0,
         timeout: Double = 60.0) {
        self.dependencies = dependencies
        self.service = service
        self.presenter = presenter
        self.qrCode = qrCode
        self.selectedValue = selectedValue
        self.cashoutData = cashoutData
        self.timeout = timeout
        self.requestInterval = requestInterval
    }
    
    func showDataToView() {
        presenter.updateView(selectedValue: selectedValue,
                             withdrawFee: cashoutData.withdrawalFee)
        dependencies.analytics.log(ATM24Events.openConfirmationWithdrawScreen)
    }
    
    func confirmWithdraw() {
        dependencies.analytics.log(ATM24Events.confirmWithdraw)
        service.authenticateUser { [weak self] authPin in
            guard let self = self, let pin = authPin else {
                return
            }
            
            let withdrawData = AuthorizationInfoRequestData(
                qrCode: self.qrCode,
                password: pin,
                withdrawValue: self.selectedValue
            )
            self.presenter.showLoading(true)
            
            self.withdrawAuthorization(with: withdrawData)
        }
    }
    
    func withdrawAuthorization(with data: AuthorizationInfoRequestData) {
        self.service.authorizeWithdraw(withData: data) { result in
            switch result {
            case let .success(authorization):
                self.authorization = authorization
                self.verifyTransactionStatus()
            case let .failure(error):
                self.parseAuthorizationError(error)
            }
        }
    }
    
    func verifyTransactionStatus() {
        let timeDiff = Date().timeIntervalSince(startTime ?? Date())
        if timeDiff >= timeout {
            transactionSuccess()
            return
        }
        
        guard let transactionId = authorization?.transactionId else {
            return transactionCanceled()
        }
        
        service.verifyTransactionStatus(forTransactionId: transactionId) { [weak self] result in
            switch result {
            case .success(let status):
                self?.parseTransactionStatus(status)
            case .failure(let error):
                self?.parseAuthorizationError(error)
            }
        }
    }
    
    func loadHelp() {
        presenter.perform(action: .help(cashoutData))
    }
    
    func acceptedTransaction() {
        let isWithdrawalFeeGreaterThenZero = cashoutData.withdrawalFee > 0.00
        dependencies.analytics.log(ATM24Events.confirmWithdrawTransaction(isWithdrawalFeeGreaterThenZero))
    }
    
    func cancelTransaction() {
        let isWithdrawalFeeGreaterThenZero = cashoutData.withdrawalFee > 0.00
        dependencies.analytics.log(ATM24Events.cancelWithdrawTransaction(isWithdrawalFeeGreaterThenZero))
        cancel()
    }
    
    func trackAlert() {
        let lastWithdrawal = 1
        let secondToLast = 2
        switch cashoutData.availableFreeWithdrawal {
        case lastWithdrawal:
            dependencies.analytics.log(ATM24Events.confirmLastWithdrawTransactionAlert)
        case secondToLast:
            dependencies.analytics.log(ATM24Events.confirmSecondToLastWithdrawTransactionAlert)
        default:
            break
        }
    }
    
    func cancel() {
        presenter.perform(action: .cancel)
    }
    
    func finish() {
        presenter.perform(action: .finish)
    }

    func performPrimaryActionError(withId type: String) {
        presenter.perform(action: errorsManager.error(byId: type).primaryAction())
    }
    
    func performSecondaryActionError(withId type: String) {
        guard let errorAction = errorsManager.error(byId: type).secondaryAction() else { return }
        presenter.perform(action: errorAction)
    }

    func perform(errorAction: CashoutConfirmationAction) {
        presenter.perform(action: errorAction)
    }
}

private extension CashoutConfirmationInteractor {
    func startTimer() {
        startTime = Date()
        self.timer = RepeatingTimer(timeInterval: requestInterval)
        self.timer?.eventHandler = { [weak self] in
            self?.verifyTransactionStatus()
        }
        timer?.resume()
    }
    
    func stopTimer() {
        if timer != nil {
            timer?.suspend()
            timer = nil
            startTime = nil
        }
    }
    
    func parseTransactionStatus(_ status: WithdrawTransactionStatus) {
        switch status.status {
        case .confirmed:
            transactionSuccess()
        case .canceled:
            transactionCanceled()
        default:
            if startTime == nil {
                startTimer()
            }
        }
    }
    
    func transactionCanceled() {
        stopTimer()
        presenter.showLoading(false)
        presenter.show(error: ATM24GenericError())
    }
    
    func transactionSuccess() {
        stopTimer()
        presenter.showLoading(false)
        if showWithdrawFeeEnabled {
            shouldShowAlert()
        } else {
            presenter.showWithdrawSuccess()
        }
    }
    
    func shouldShowAlert() {
        let lastWithdrawal = 1
        let secondToLast = 2
        switch cashoutData.availableFreeWithdrawal {
        case lastWithdrawal:
            presenter.showLastFeeWithdrawSuccessWithAlert()
            dependencies.analytics.log(ATM24Events.openLastWithdrawTransactionAlert)
        case secondToLast:
            presenter.showSecondToLastFeeWithdrawSuccessWithAlert()
            dependencies.analytics.log(ATM24Events.openSecondToLastWithdrawTransactionAlert)
        default:
            presenter.showWithdrawSuccess()
        }
    }
    
    func parseAuthorizationError(_ error: Error) {
        presenter.showLoading(false)
        guard
            let authorizationError = error as? AuthorizationError,
            let code = authorizationError.code,
            let title = authorizationError.title,
            let message = authorizationError.message
            else {
            return presenter.show(error: ATM24GenericError())
        }

        var atm24Error = errorsManager.error(byId: code)
        atm24Error.title = title
        atm24Error.message = message

        presenter.show(error: atm24Error)
        dependencies.analytics.log(ATM24Events.authorizationError(message: message, code: code))
    }
}
