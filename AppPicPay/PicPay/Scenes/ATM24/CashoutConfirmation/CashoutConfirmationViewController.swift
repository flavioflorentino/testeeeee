import SnapKit
import UI
import UIKit

protocol CashoutConfirmationDisplay: AnyObject {
    func updateView(withData data: CashoutDataInteractor)
    func showLoading(_ loading: Bool)
    func showError(title: String, message: String)
    func showWithdrawSuccess()
    func showWithdrawSuccessWithAlert(subtitle: String, icon: UIImage)
    func showDialogError(error: ATM24CodeError)
    func showScreenError(error: ATM24CodeError)
}

private extension CashoutConfirmationViewController.Layout {
    static let imageSize = CGSize(width: 80, height: 80)
}

final class CashoutConfirmationViewController: ViewController<CashoutConfirmationInteracting, UIView>, StatefulProviding {
    fileprivate enum Layout {}

    private lazy var confirmationValueStackView: UIStackView = {
        let stack = UIStackView()
        stack.alignment = .center
        stack.axis = .vertical
        return stack
    }()
    
    private lazy var withdrawImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "icon-saque-p")
        imageView.contentMode = .scaleAspectFit
        
        return imageView
    }()
    
    private lazy var cashoutValueConfirmationLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.Atm24.cashoutValueConfirmation
        label.textAlignment = .center
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        return label
    }()
    
    private lazy var cashoutValueLabel: UILabel = {
        let label = UILabel()
        label.textColor = Colors.branding400.color
        label.textAlignment = .center
        return label
    }()
    
    private lazy var detailInfoView = CashoutDetailIView()
    
    private lazy var confirmationButton: UIButton = {
        let button = UIButton()
        button.setTitle(DefaultLocalizable.btConfirm.text, for: .normal)
        button.addTarget(self, action: #selector(confirmationButtonTapped(_:)), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()
    
    private var errorCode: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.showDataToView()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(confirmationValueStackView)
        confirmationValueStackView.addArrangedSubview(withdrawImageView)
        confirmationValueStackView.addArrangedSubview(cashoutValueConfirmationLabel)
        confirmationValueStackView.addArrangedSubview(cashoutValueLabel)
        confirmationValueStackView.addArrangedSubview(detailInfoView)
        view.addSubview(confirmationButton)
    }
    
    override func configureViews() {
        title = Strings.Atm24.withdrawMoney
        view.backgroundColor = .backgroundPrimary()
        let barButton = UIBarButtonItem(
            title: DefaultLocalizable.btCancel.text,
            style: .plain,
            target: self,
            action: #selector(cancel(_:))
        )
        barButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: Colors.branding400.color], for: .normal)
        navigationController?.navigationBar.topItem?.leftBarButtonItem = barButton
    }
    
    override func setupConstraints() {
        confirmationValueStackView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.centerY.equalToSuperview().offset(-Spacing.base03)
        }
        
        if #available(iOS 11.0, *) {
            confirmationValueStackView.setCustomSpacing(Spacing.base02, after: withdrawImageView)
            confirmationValueStackView.setCustomSpacing(Spacing.base01, after: cashoutValueConfirmationLabel)
            confirmationValueStackView.setCustomSpacing(Spacing.base03, after: cashoutValueLabel)
        } else {
            confirmationValueStackView.setSpacing(Spacing.base02, after: withdrawImageView)
            confirmationValueStackView.setSpacing(Spacing.base01, after: cashoutValueConfirmationLabel)
            confirmationValueStackView.setSpacing(Spacing.base03, after: cashoutValueLabel)
        }
        
        withdrawImageView.snp.makeConstraints {
            $0.size.equalTo(Layout.imageSize)
        }
        
        confirmationButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.bottom.equalTo(view.compatibleSafeArea.bottom).offset(-Spacing.base03)
        }

        detailInfoView.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
        }
    }
}

@objc
private extension CashoutConfirmationViewController {
    func cancel(_ sender: UIButton) {
        interactor.cancelTransaction()
    }
    
    func confirmationButtonTapped(_ sender: UIButton) {
        interactor.acceptedTransaction()
        interactor.confirmWithdraw()
    }
}

// MARK: View Model Outputs
extension CashoutConfirmationViewController: CashoutConfirmationDisplay {
    func updateView(withData data: CashoutDataInteractor) {
        cashoutValueLabel.attributedText = data.selectedValueAttributedText
        detailInfoView.setInfo(selectedValue: data.selectedValue, fee: data.withdrawalFee)
    }
    
    func showLoading(_ loading: Bool) {
        navigationItem.leftBarButtonItem?.isEnabled = !loading
        loading ? beginState() : endState()
    }
    
    func showError(title: String, message: String) {
        showAlert(withTitle: title, message: message) { [weak self] in
            self?.interactor.cancel()
        }
    }
    
    func showWithdrawSuccess() {
        guard let view = navigationController?.view else {
            return
        }
        TaskCompleteAnimation.showSuccess(onView: view) { [weak self] in
            self?.interactor.finish()
        }
    }
    
    func showWithdrawSuccessWithAlert(subtitle: String, icon: UIImage) {
       guard let navigation = navigationController else {
            return
        }
        let popup = PopupViewController(
            title: Strings.Atm24.withdrawLimitTitle,
            description: subtitle,
            preferredType: .image(icon)
        )
        popup.hideCloseButton = true
        
        let understoodAction = PopupAction(title: Strings.Atm24.understood, style: .fill) {
            self.interactor.trackAlert()
            TaskCompleteAnimation.showSuccess(onView: navigation.view) { [weak self] in
                self?.interactor.finish()
            }
        }
        popup.addAction(understoodAction)
    
        showPopup(popup)
    }
    
    func showDialogError(error: ATM24CodeError) {
        let primaryButtonAction = ApolloAlertAction(title: error.primaryButtonTitle) { [weak self] in
            self?.interactor.perform(errorAction: error.primaryAction())
        }

        var secondaryButtonAction: ApolloAlertAction?
        if let secondaryButtonTitle = error.secondaryButtonTitle {
            secondaryButtonAction = ApolloAlertAction(title: secondaryButtonTitle) { [weak self] in
                self?.interactor.perform(errorAction: error.secondaryAction() ?? .finish)
            }
        }

        showApolloAlert(image: error.icon,
                        title: error.title,
                        subtitle: error.message,
                        primaryButtonAction: primaryButtonAction,
                        linkButtonAction: secondaryButtonAction)
    }
    
    func showScreenError(error: ATM24CodeError) {
        let content = ApolloFeedbackViewContent(image: error.icon,
                                                title: error.title,
                                                description: NSAttributedString(string: error.message),
                                                primaryButtonTitle: error.primaryButtonTitle,
                                                secondaryButtonTitle: error.secondaryButtonTitle ?? "")
        let feedbackView = ApolloFeedbackView(content: content, style: .confirmation)
        
        feedbackView.didTapPrimaryButton = { [weak self] in
            feedbackView.removeFromSuperview()
            self?.interactor.perform(errorAction: error.primaryAction())
        }
        
        feedbackView.didTapSecondaryButton = { [weak self] in
            feedbackView.removeFromSuperview()
            self?.interactor.perform(errorAction: error.secondaryAction() ?? .finish)
        }

        feedbackView.secondaryButtonIsHidden(error.secondaryButtonTitle == nil)
        
        view.addSubview(feedbackView)
        feedbackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

extension CashoutConfirmationViewController {
    private func showAlert(
        withTitle title: String,
        message: String,
        showCancelButton: Bool = false,
        tryAgainBlock: @escaping () -> Void
    ) {
        let alert = Alert(title: title, text: message)
        var buttons: [Button] = []
        
        let tryAgainButton = Button(title: DefaultLocalizable.tryAgain.text, type: .cta) { popup, _ in
            popup.dismiss(animated: true) {
                tryAgainBlock()
            }
        }
        buttons.append(tryAgainButton)
        
        if showCancelButton {
            let cancelButton = Button(title: DefaultLocalizable.btCancel.text, type: .clean) { popup, _ in
                popup.dismiss(animated: true, completion: nil)
            }
            buttons.append(cancelButton)
        }
        
        alert.buttons = buttons

        AlertMessage.showAlert(alert, controller: self)
    }
}
