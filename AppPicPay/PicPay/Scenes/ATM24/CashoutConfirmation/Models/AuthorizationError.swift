import Foundation

struct AuthorizationError: Error, Decodable, Equatable {
    enum CodingKeys: String, CodingKey {
        case code
        case title = "short_message"
        case message
    }
    
    let code: String?
    let title: String?
    let message: String?
}
