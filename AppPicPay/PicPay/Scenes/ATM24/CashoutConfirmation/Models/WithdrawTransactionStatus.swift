import Foundation

struct WithdrawTransactionStatus: Decodable {
    enum Status: String, Decodable {
        case pending = "PENDING"
        case confirmed = "CONFIRMED"
        case canceled = "CANCELED"
    }
    
    enum CodingKeys: String, CodingKey {
        case transactionId = "transaction_uuid"
        case status
        case withdrawalValue = "withdrawal_value"
        case withdrawalFee = "withdrawal_fee"
    }
    
    let transactionId: String
    let status: Status
    let withdrawalValue: Double
    let withdrawalFee: Double
}
