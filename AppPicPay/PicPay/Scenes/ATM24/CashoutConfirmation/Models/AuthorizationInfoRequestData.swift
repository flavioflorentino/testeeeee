import Foundation

struct AuthorizationInfoRequestData {
    let qrCode: String
    let password: String
    let withdrawValue: Double
}
