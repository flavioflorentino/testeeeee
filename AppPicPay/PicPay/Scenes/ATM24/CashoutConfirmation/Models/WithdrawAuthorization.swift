import Foundation

struct WithdrawAuthorization: Decodable {
    enum CodingKeys: String, CodingKey {
        case transactionId = "transaction_uuid"
        case withdrawValue = "withdrawal_value"
        case withdrawalFee = "withdrawal_fee"
    }
    
    let transactionId: String
    let withdrawValue: Double
    let withdrawalFee: Double
}
