import AssetsKit
import Foundation

struct ATM24ResetPasswordError: ATM24CodeError {
    let id: String = "6003"
    var title: String = Strings.Atm24.somethingWrong
    var message: String = Strings.Atm24.couldNotCompleteWithdraw
    var icon: UIImage = Resources.Icons.icoWarningBig.image
    var primaryButtonTitle: String = Strings.Atm24.resetPassword
    var secondaryButtonTitle: String? = Strings.Atm24.tryAgain

    func primaryAction() -> CashoutConfirmationAction {
        .backToValues
    }

    func secondaryAction() -> CashoutConfirmationAction? {
        .cancel
    }
}

struct ATM24DuplicatedTransactionUUIDError: ATM24CodeError {
    let id: String = "S24H-10005"
    var title: String = Strings.Atm24.somethingWrong
    var message: String = Strings.Atm24.couldNotCompleteWithdraw
    var icon: UIImage = Resources.Illustrations.iluQrcodeError.image

    func primaryAction() -> CashoutConfirmationAction {
        .backToValues
    }
}

struct ATM24InvalidQRCodeError: ATM24CodeError {
    let id: String = "S24H-10004"
    var title: String = Strings.Atm24.somethingWrong
    var message: String = Strings.Atm24.couldNotCompleteWithdraw
    var icon: UIImage = Resources.Illustrations.iluQrcodeError.image

    func primaryAction() -> CashoutConfirmationAction {
        .backToValues
    }
}

struct ATM24LegacyGetAvailableWithdrawFailError: ATM24CodeError {
    let id: String = "S24H-10501"
    var title: String = Strings.Atm24.somethingWrong
    var message: String = Strings.Atm24.couldNotCompleteWithdraw

    func primaryAction() -> CashoutConfirmationAction {
        .backToValues
    }
}

struct ATM24LegacyCreateCashoutMovementFailError: ATM24CodeError {
    let id: String = "S24H-10502"
    var title: String = Strings.Atm24.somethingWrong
    var message: String = Strings.Atm24.couldNotCompleteWithdraw

    func primaryAction() -> CashoutConfirmationAction {
        .backToValues
    }
}

struct ATM24OriginalCommunicationFailError: ATM24CodeError {
    let id: String = "S24H-10503"
    var title: String = Strings.Atm24.somethingWrong
    var message: String = Strings.Atm24.couldNotCompleteWithdraw

    func primaryAction() -> CashoutConfirmationAction {
        .backToValues
    }
}

struct ATM24InvalidNotesError: ATM24CodeError {
    let id: String = "S24H-10014"
    var title: String = Strings.Atm24.somethingWrong
    var message: String = Strings.Atm24.couldNotCompleteWithdraw
    var primaryButtonTitle: String = Strings.Atm24.chooseAnotherValue

    func primaryAction() -> CashoutConfirmationAction {
        .backToValues
    }
}

struct ATM24UnavailableNotesError: ATM24CodeError {
    let id: String = "S24H-10015"
    var title: String = Strings.Atm24.somethingWrong
    var message: String = Strings.Atm24.couldNotCompleteWithdraw
    var presentationType: ATM24ErrorPresentationType = .screen
    var primaryButtonTitle: String = Strings.Atm24.understood

    func primaryAction() -> CashoutConfirmationAction {
        .finish
    }
}

struct ATM24ConsumerWithRestrictionsError: ATM24CodeError {
    let id: String = "S24H-10008"
    var title: String = Strings.Atm24.somethingWrong
    var message: String = Strings.Atm24.couldNotCompleteWithdraw
    var icon: UIImage = Resources.Illustrations.iluTool.image
    var primaryButtonTitle: String = Strings.Atm24.helpContact
    var presentationType: ATM24ErrorPresentationType = .screen

    func primaryAction() -> CashoutConfirmationAction {
        .contactHelpCenter
    }

    func secondaryAction() -> CashoutConfirmationAction? {
        .finish
    }
}

struct ATM24OriginalDeniesWithdrawError: ATM24CodeError {
    let id: String = "S24H-10010"
    var title: String = Strings.Atm24.somethingWrong
    var message: String = Strings.Atm24.couldNotCompleteWithdraw
    var icon: UIImage = Resources.Illustrations.iluTool.image
    var primaryButtonTitle: String = Strings.Atm24.understood
    var presentationType: ATM24ErrorPresentationType = .screen

    func primaryAction() -> CashoutConfirmationAction {
        .finish
    }
}

struct ATM24WithdrawDisabledError: ATM24CodeError {
    let id: String = "S24H-10905"
    var title: String = Strings.Atm24.somethingWrong
    var message: String = Strings.Atm24.couldNotCompleteWithdraw
    var icon: UIImage = Resources.Illustrations.iluTool.image
    var primaryButtonTitle: String = Strings.Atm24.understood
    var presentationType: ATM24ErrorPresentationType = .screen

    func primaryAction() -> CashoutConfirmationAction {
        .finish
    }
}

struct ATM24TechnicalProblemError: ATM24CodeError {
    let id: String = "S24H-10500"
    var title: String = Strings.Atm24.somethingWrong
    var message: String = Strings.Atm24.couldNotCompleteWithdraw
    var icon: UIImage = Resources.Illustrations.iluTool.image
    var primaryButtonTitle: String = Strings.Atm24.understood
    var presentationType: ATM24ErrorPresentationType = .screen

    func primaryAction() -> CashoutConfirmationAction {
        .finish
    }
}

struct ATM24UnableToGetLockerForWithdrawError: ATM24CodeError {
    let id: String = "S24H-10504"
    var title: String = Strings.Atm24.somethingWrong
    var message: String = Strings.Atm24.couldNotCompleteWithdraw
    var icon: UIImage = Resources.Illustrations.iluTool.image
    var primaryButtonTitle: String = Strings.Atm24.understood
    var presentationType: ATM24ErrorPresentationType = .screen

    func primaryAction() -> CashoutConfirmationAction {
        .finish
    }
}

struct ATM24GenericError: ATM24CodeError {
    let id: String = ""
    var title: String = Strings.Atm24.somethingWrong
    var message: String = Strings.Atm24.couldNotCompleteWithdraw

    func primaryAction() -> CashoutConfirmationAction {
        .backToValues
    }
}
