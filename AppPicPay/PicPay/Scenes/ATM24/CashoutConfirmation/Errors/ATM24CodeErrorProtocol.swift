import AssetsKit
import Foundation

enum ATM24ErrorPresentationType {
    case dialog
    case screen
}

protocol ATM24CodeError {
    var id: String { get }
    var title: String { get set }
    var message: String { get set }
    var presentationType: ATM24ErrorPresentationType { get }
    var icon: UIImage { get }
    var primaryButtonTitle: String { get }
    var secondaryButtonTitle: String? { get }

    func primaryAction() -> CashoutConfirmationAction
    func secondaryAction() -> CashoutConfirmationAction?
}

extension ATM24CodeError {
    var presentationType: ATM24ErrorPresentationType {
        .dialog
    }

    var icon: UIImage {
        Resources.Illustrations.iluPackOfMoneyError.image
    }

    var title: String {
        Strings.Atm24.somethingWrong
    }

    var message: String {
        Strings.Atm24.couldNotCompleteWithdraw
    }

    var primaryButtonTitle: String {
        Strings.Atm24.tryAgain
    }

    var secondaryButtonTitle: String? {
        nil
    }

    func secondaryAction() -> CashoutConfirmationAction? { nil }
}
