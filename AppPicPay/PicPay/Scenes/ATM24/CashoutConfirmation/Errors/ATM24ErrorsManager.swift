import Foundation

final class ATM24ErrorsManager {
    var errors: [String: ATM24CodeError] = [:]

    init(errors: [ATM24CodeError]) {
        register(errors: errors)
    }

    private func register(errors: [ATM24CodeError]) {
        errors.forEach(register)
    }

    private func register(error: ATM24CodeError) {
        errors[error.id] = error
    }

    func error(byId id: String) -> ATM24CodeError {
        errors[id] ?? ATM24GenericError()
    }
}
