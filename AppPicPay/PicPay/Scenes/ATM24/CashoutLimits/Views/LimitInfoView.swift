import UI
import UIKit

struct LimitInfoData {
    let limitName: String
    let limitValue: Double
}

final class LimitInfoView: UIView {
    private lazy var limitNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: .light)
        label.textColor = Palette.ppColorGrayscale500.color
        label.textAlignment = .left
        return label
    }()
    
    private lazy var limitValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.textColor = Palette.ppColorGrayscale500.color
        label.textAlignment = .right
        return label
    }()
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale200.color
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        backgroundColor = Palette.ppColorGrayscale000.color
        addComponents()
        layoutComponents()
    }
    
    private func addComponents() {
        addSubview(limitNameLabel)
        addSubview(limitValueLabel)
        addSubview(separatorView)
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            limitNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            limitNameLabel.topAnchor.constraint(equalTo: topAnchor)
        ])
        
        NSLayoutConstraint.activate([
            limitValueLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            limitValueLabel.topAnchor.constraint(equalTo: topAnchor)
        ])
        
        NSLayoutConstraint.activate([
            separatorView.leadingAnchor.constraint(equalTo: leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: trailingAnchor),
            separatorView.topAnchor.constraint(equalTo: limitNameLabel.bottomAnchor, constant: 18),
            separatorView.bottomAnchor.constraint(equalTo: bottomAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
    
    func configure(withData data: LimitInfoData) {
        limitNameLabel.text = data.limitName
        limitValueLabel.text = data.limitValue.stringAmount
    }

    func configureFee(withData data: LimitInfoData) {
        limitNameLabel.text = data.limitName
        limitValueLabel.text = Int(data.limitValue).toString()
    }
}
