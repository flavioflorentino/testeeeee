import Foundation
import UI
import FeatureFlag

struct CashoutLimitsDataPresenter {
    let dailyLimit: LimitInfoData
    let maxValuePerWithdraw: LimitInfoData
    let minValuePerWithdraw: LimitInfoData
    let maxNumFreeWithDrawalsMonthly: LimitInfoData
    let withdrawalFee: LimitInfoData
    let availableValue: LimitInfoData
    let attributedMoreInfoText: NSAttributedString
}

protocol CashoutLimitsViewModelInputs {
    func viewDidLoad()
    func shouldShowWithdrawTextInfo()
}

protocol CashoutLimitsViewModelOutputs: AnyObject {
    func updateView(withData data: CashoutLimitsDataPresenter)
    func showWithdrawTextInfo()
}

protocol CashoutLimitsViewModelType: AnyObject {
    var inputs: CashoutLimitsViewModelInputs { get }
    var outputs: CashoutLimitsViewModelOutputs? { get set }
}

final class CashoutLimitsViewModel: CashoutLimitsViewModelType, CashoutLimitsViewModelInputs {
    var inputs: CashoutLimitsViewModelInputs { self }
    weak var outputs: CashoutLimitsViewModelOutputs?
    
    private let service: CashoutLimitsServicing
    private let cashoutData: CashoutValues
    private var showWithdrawFeeEnabled: Bool {
        FeatureManager.shared.isActive(.featureShowWithdrawFeeEnabled)
    }
    
    init(service: CashoutLimitsServicing, cashoutData: CashoutValues) {
        self.service = service
        self.cashoutData = cashoutData
    }
    
    func viewDidLoad() {
        let dailyLimit = LimitInfoData(limitName: Strings.Atm24.dailyLimit, limitValue: cashoutData.dailyWithdrawalLimit)
        let maxValue = LimitInfoData(limitName: Strings.Atm24.maxValuePerWithdraw, limitValue: cashoutData.maxWithdrawalValue)
        let minValue = LimitInfoData(limitName: Strings.Atm24.minValuePerWithdraw, limitValue: cashoutData.minWithdrawalValue)
        let availableLimit = LimitInfoData(limitName: Strings.Atm24.availableCashoutLimit, limitValue: cashoutData.availableWithdrawalValue)
        let freeWithDrawalsMonthly = LimitInfoData(limitName: Strings.Atm24.freeWithDrawalsMonthly, limitValue: Double(cashoutData.maxFreeWithdrawalsMonthly))
        let withdrawalFee = LimitInfoData(limitName: Strings.Atm24.withdrawalFee, limitValue: cashoutData.withdrawalExhibitionFee)
        
        let dataPresenter = CashoutLimitsDataPresenter(dailyLimit: dailyLimit,
                                                       maxValuePerWithdraw: maxValue,
                                                       minValuePerWithdraw: minValue,
                                                       maxNumFreeWithDrawalsMonthly: freeWithDrawalsMonthly,
                                                       withdrawalFee: withdrawalFee,
                                                       availableValue: availableLimit,
                                                       attributedMoreInfoText: attributedInfoText())
        outputs?.updateView(withData: dataPresenter)
    }
    
    private func attributedInfoText() -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: "\(Strings.Atm24.tapHere) \(Strings.Atm24.toKnowMore)")
        
        let firstRange = (attributedString.string as NSString).range(of: Strings.Atm24.tapHere)
        let firstRangeAttributes: [NSAttributedString.Key: Any] = [
            .underlineStyle: NSUnderlineStyle.single.rawValue,
            .font: UIFont.systemFont(ofSize: 14, weight: .medium),
            .foregroundColor: Palette.ppColorNeutral300.color
        ]
        
        let secondRange = (attributedString.string as NSString).range(of: Strings.Atm24.toKnowMore)
        let secondRangeAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 14, weight: .light),
            .foregroundColor: Palette.ppColorGrayscale600.color
        ]
        
        attributedString.addAttributes(firstRangeAttributes, range: firstRange)
        attributedString.addAttributes(secondRangeAttributes, range: secondRange)
        return attributedString
    }
    
    func shouldShowWithdrawTextInfo() {
        guard showWithdrawFeeEnabled else {
            outputs?.showWithdrawTextInfo()
            return
        }
    }
}
