import UI
import UIKit

final class CashoutLimitsViewController: LegacyViewController<CashoutLimitsViewModelType, CashoutLimitsCoordinating, UIView> {
    enum Layout {
        static let defaultHorizontalMargin: CGFloat = 20
        static let listItemBottomMargin: CGFloat = 24
        static let defaultTopMargin: CGFloat = 28
    }

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorGrayscale600.color
        label.text = Strings.Atm24.limitTitle
        label.font = UIFont.boldSystemFont(ofSize: 28)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var subTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorGrayscale600.color
        label.text = Strings.Atm24.limitSubtitle
        label.font = UIFont.systemFont(ofSize: 16, weight: .light)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var infoStackView: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.alignment = .fill
        stack.axis = .vertical
        stack.distribution = .fill
        stack.spacing = 18
        return stack
    }()
    
    private lazy var dailyLimitInfoView: LimitInfoView = {
        let view = LimitInfoView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var maxValuePerWithdrawInfoView: LimitInfoView = {
        let view = LimitInfoView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var minValuePerWithdrawInfoView: LimitInfoView = {
        let view = LimitInfoView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var availableLimitInfoView: LimitInfoView = {
        let view = LimitInfoView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var freeWithDrawalsMonthlyInfoView: LimitInfoView = {
        let view = LimitInfoView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var withdrawalFeeInfoView: LimitInfoView = {
        let view = LimitInfoView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var moreInfoLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorNeutral300.color
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(moreInfoLabelTapped(gesture:))))
        label.textAlignment = .left
        return label
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .default
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.inputs.viewDidLoad()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(subTitleLabel)
        view.addSubview(infoStackView)
        infoStackView.addArrangedSubview(dailyLimitInfoView)
        infoStackView.addArrangedSubview(maxValuePerWithdrawInfoView)
        infoStackView.addArrangedSubview(minValuePerWithdrawInfoView)
        infoStackView.addArrangedSubview(availableLimitInfoView)
        infoStackView.addArrangedSubview(freeWithDrawalsMonthlyInfoView)
        infoStackView.addArrangedSubview(withdrawalFeeInfoView)
        view.addSubview(moreInfoLabel)

        viewModel.inputs.shouldShowWithdrawTextInfo()
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        navigationItem.backBarButtonItem = UIBarButtonItem(title: nil, style: .plain, target: nil, action: nil)
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [titleLabel, subTitleLabel, infoStackView, moreInfoLabel], constant: Layout.defaultHorizontalMargin)
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: Layout.defaultTopMargin),
            subTitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Layout.listItemBottomMargin)
        ])
        
        NSLayoutConstraint.activate([
            infoStackView.topAnchor.constraint(equalTo: subTitleLabel.bottomAnchor, constant: Layout.listItemBottomMargin)
        ])
        
        NSLayoutConstraint.activate([
            moreInfoLabel.topAnchor.constraint(equalTo: infoStackView.bottomAnchor, constant: Layout.defaultTopMargin)
        ])
    }
    
    @objc
    private func moreInfoLabelTapped(gesture: UITapGestureRecognizer) {
        coordinator.perform(action: .moreInfo)
    }
}

// MARK: View Model Outputs
extension CashoutLimitsViewController: CashoutLimitsViewModelOutputs {
    func showWithdrawTextInfo() {
        freeWithDrawalsMonthlyInfoView.isHidden = true
        withdrawalFeeInfoView.isHidden = true
    }
    
    func updateView(withData data: CashoutLimitsDataPresenter) {
        dailyLimitInfoView.configure(withData: data.dailyLimit)
        maxValuePerWithdrawInfoView.configure(withData: data.maxValuePerWithdraw)
        minValuePerWithdrawInfoView.configure(withData: data.minValuePerWithdraw)
        availableLimitInfoView.configure(withData: data.availableValue)
        freeWithDrawalsMonthlyInfoView.configureFee(withData: data.maxNumFreeWithDrawalsMonthly)
        withdrawalFeeInfoView.configure(withData: data.withdrawalFee)
        moreInfoLabel.attributedText = data.attributedMoreInfoText
    }
}
