import Foundation

enum CashoutLimitsFactory {
    static func make(withCashoutData data: CashoutValues) -> CashoutLimitsViewController {
        let service: CashoutLimitsServicing = CashoutLimitsService()
        let viewModel: CashoutLimitsViewModelType = CashoutLimitsViewModel(service: service, cashoutData: data)
        var coordinator: CashoutLimitsCoordinating = CashoutLimitsCoordinator()
        let viewController = CashoutLimitsViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController

        return viewController
    }
}
