import CustomerSupport
import FeatureFlag
import UI
import UIKit

enum CashoutLimitsAction {
    case moreInfo
}

protocol CashoutLimitsCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: CashoutLimitsAction)
}

final class CashoutLimitsCoordinator: CashoutLimitsCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: CashoutLimitsAction) {
        switch action {
        case .moreInfo:
            let option = HelpCenterOptions.saqueNoBanco24Horas.faqOption
            let faq = FAQFactory.make(option: option)
            let navigation = UINavigationController(rootViewController: faq)
            viewController?.present(navigation, animated: true)
        }
    }
}
