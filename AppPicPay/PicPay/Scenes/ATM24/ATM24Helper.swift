import Core
import Foundation

final class ATM24Helper {
    var hasPresentedAtm24OfferAlert: Bool {
        get {
            KVStore().boolFor(.atm24OfferAlert)
        }
        set(value) {
            KVStore().setBool(value, with: .atm24OfferAlert)
        }
    }
}
