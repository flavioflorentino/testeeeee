import AnalyticsModule

enum ATM24Events: AnalyticsKeyProtocol {
    case valueOptions
    case openScanner
    case didScan
    case howItWorks
    case confirmWithdraw
    case cannotWithdraw(_ motive: Motive)
    case selectValue(_ origin: Origin, selectedValue: Double)
    case popupOffer
    case authorizationError(message: String, code: String)
    case popupFeeLimit
    case footerFeeLimit
    case openCashoutLimiteExceeded
    case tapContinueLimiteExceeded
    case tapBackToHomeLimiteExceeded
    case tapBackLimiteExceeded
    case cashoutFirstLimitNotification
    case openConfirmationWithdrawScreen
    case confirmWithdrawTransaction(_ tax: Bool)
    case cancelWithdrawTransaction(_ tax: Bool)
    case openSecondToLastWithdrawTransactionAlert
    case confirmSecondToLastWithdrawTransactionAlert
    case openLastWithdrawTransactionAlert
    case confirmLastWithdrawTransactionAlert
    
    private var name: String {
        switch self {
        case .selectValue:
            return "Saque24Horas - Selecionou valor"
        case .valueOptions:
            return "Saque24Horas - Opções de valores"
        case .openScanner:
            return "Saque24Horas - Entrou Scanner"
        case .didScan:
            return "Saque24Horas - Scanneou"
        case .howItWorks:
            return "Saque24Horas - Scanner Informações"
        case .confirmWithdraw:
            return "Saque24Horas - Confirmou"
        case .cannotWithdraw:
            return "Saque24Horas - Não foi possível"
        case .popupOffer:
            return "Saque24horas - Show Popup"
        case .authorizationError:
            return "Saque24Horas - Erro Autorizacao"
        case .popupFeeLimit:
            return "Cash-out First Limit Ok"
        case .footerFeeLimit:
            return "Cash-out Limit Area"
        case .openCashoutLimiteExceeded:
            return "Cash-out Limit exceeded Notification"
        case .tapContinueLimiteExceeded:
            return "Cash-out Limit Continue Button"
        case .tapBackToHomeLimiteExceeded:
            return "Cash-out Limit Back to Start"
        case .tapBackLimiteExceeded:
            return "Cash-out Limit Icon Back to Start"
        case .cashoutFirstLimitNotification:
            return "Cash-out First Limit Notification"
        case .openConfirmationWithdrawScreen:
            return "Cash-out Confirm Value Cash"
        case .confirmWithdrawTransaction:
            return "Cash-out Confirm Value Cash Button"
        case .cancelWithdrawTransaction:
            return "Cash-out Button Canceled Value Cash"
        case .openSecondToLastWithdrawTransactionAlert:
            return "Cash-out Modal Final Limit"
        case .confirmSecondToLastWithdrawTransactionAlert:
            return "Cash-out Confirm Final Limit"
        case .openLastWithdrawTransactionAlert:
            return "Cash-out Modal Last Limit"
        case .confirmLastWithdrawTransactionAlert:
            return "Cash-out Confirm Last Limit"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .selectValue(origin, selectedValue):
            return ["Origem": origin, "Valor": selectedValue]
        case .cannotWithdraw(let motive):
            return ["Motivo": motive]
        case let .authorizationError(message, code):
            return ["message": message, "error_code": code]
        case .confirmWithdrawTransaction(let tax):
            return ["Tax": tax]
        case .cancelWithdrawTransaction(let tax):
            return ["Tax": tax]
        default:
            return [:]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: [.mixPanel, .appsFlyer, .firebase])
    }
}
extension ATM24Events {
    enum Origin: String, CustomStringConvertible {
        case predifined = "Valor pré-definido"
        case other = "Outro valor"
        
        var description: String {
             self.rawValue
        }
    }
    
    enum Motive: String, CustomStringConvertible {
        case dailyLimit = "Atingiu o limite de saque"
        case insufficientFunds = "Saldo insuficiente"
        
        var description: String {
             self.rawValue
        }
    }
}
