import UI
import UIKit

final class HelpCenterButton: UIButton, ViewConfiguration {
    private enum Layout {
        static let badgeSize: CGFloat = 14
        static let badgeOffset: CGFloat = 3
    }
    
    var displayBadge: Bool = false {
        didSet {
            badgeView.isHidden = !displayBadge
        }
    }
    
    private lazy var badgeView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = Layout.badgeSize / 2
        view.backgroundColor = Palette.ppColorNegative400.color
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(badgeView)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            badgeView.rightAnchor.constraint(equalTo: rightAnchor, constant: -Layout.badgeOffset),
            badgeView.topAnchor.constraint(equalTo: topAnchor, constant: Layout.badgeOffset),
            badgeView.heightAnchor.constraint(equalToConstant: Layout.badgeSize),
            badgeView.widthAnchor.constraint(equalToConstant: Layout.badgeSize)
        ])
    }
    
    func configureViews() {
        setImage(#imageLiteral(resourceName: "icon_help"), for: .normal)
    }
}
