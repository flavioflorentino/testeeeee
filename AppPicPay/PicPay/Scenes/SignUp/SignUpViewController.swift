import UI
import UIKit
import CoreLegacy

final class SignUpViewController: ViewController<SignUpViewModelInputs, UIView> {
    enum AccessibilityIdentifier: String {
        case loginButton
        case registerButton
    }
    
    private enum Layout {
        static let hostTextFieldHeight: CGFloat = 36.0
        static let buttonHeight: CGFloat = 40.0
        static let stackleadingTrailingMargin: CGFloat = 34.0
        static let stackViewSpacing: CGFloat = 16.0
        static let stackDistanceToHelpButton: CGFloat = 46.0
        static let bottomMargin: CGFloat = 10.0
        static let hostTextFieldMargin: CGFloat = 20.0
        static let helpButtonTrailing: CGFloat = 20.0
    }
    
    private lazy var hostTextField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = Palette.white.color
        textField.textColor = Palette.ppColorGrayscale400.color(withCustomDark: .ppColorGrayscale400)
        textField.textAlignment = .center
        textField.layer.cornerRadius = 4
        textField.adjustsFontSizeToFitWidth = true
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.isHidden = true
        
        #if DEVELOPMENT
        textField.isHidden = false
        textField.delegate = self
        #endif
        
        return textField
    }()
    
    private lazy var backgroundImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 24, weight: .semibold)
        label.textColor = Palette.white.color
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var registerButton: UIPPButton = {
        let button = UIPPButton()
        let buttonConfig = Button(title: SignUpLocalizable.register.text, type: .cta)
        button.configure(with: buttonConfig)
        button.accessibilityLabel = buttonConfig.title
        button.cornerRadius = Layout.buttonHeight / 2
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(tapRegisterButton), for: .touchUpInside)
        button.accessibilityLabel = SignUpLocalizable.register.text
        button.accessibilityIdentifier = AccessibilityIdentifier.registerButton.rawValue
        return button
    }()
    
    private lazy var loginButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle(SignUpLocalizable.enter.text, for: .normal)
        button.setTitleColor(Palette.white.color, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        button.layer.cornerRadius = Layout.buttonHeight / 2
        button.layer.borderWidth = 1
        button.layer.borderColor = Palette.white.color.cgColor
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(tapLoginButton), for: .touchUpInside)
        button.accessibilityLabel = SignUpLocalizable.enter.text
        button.accessibilityIdentifier = AccessibilityIdentifier.loginButton.rawValue
        return button
    }()
    
    private lazy var messageAndButtonsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Layout.stackViewSpacing
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var usePromoCodeButton: UIButton = {
        let button = UIButton()
        let attributedTitle = NSAttributedString(
            string: SignUpLocalizable.iAlreadyHaveACode.text,
            attributes: [
                .underlineStyle: NSUnderlineStyle.single.rawValue,
                .foregroundColor: Palette.white.color,
                .font: UIFont.systemFont(ofSize: 14, weight: .semibold)
            ]
        )
        button.setAttributedTitle(attributedTitle, for: .normal)
        button.addTarget(self, action: #selector(tapUsePromoCodeButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var helpButton: HelpCenterButton = {
        let button = HelpCenterButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.accessibilityLabel = SignUpLocalizable.helpCenter.text
        button.addTarget(self, action: #selector(tapHelpButton), for: .touchUpInside)
        return button
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        setNeedsStatusBarAppearanceUpdate()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(backgroundImageView)
        messageAndButtonsStackView.addArrangedSubview(messageLabel)
        messageAndButtonsStackView.addArrangedSubview(registerButton)
        messageAndButtonsStackView.addArrangedSubview(loginButton)
        view.addSubview(messageAndButtonsStackView)
        view.addSubview(helpButton)
        view.addSubview(usePromoCodeButton)
        view.addSubview(hostTextField)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorBranding200.color(withCustomDark: .ppColorGrayscale100)
    }
    
    override func setupConstraints() {
        var bottomConstant: CGFloat = -Layout.bottomMargin
        if #available(iOS 11.0, *) {
            if let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom, bottom > .zero {
                bottomConstant = .zero
            }
        }
        
        NSLayoutConstraint.constraintAllEdges(from: backgroundImageView, to: view)
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [messageAndButtonsStackView], constant: Layout.stackleadingTrailingMargin)
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [hostTextField], constant: Layout.hostTextFieldMargin)
        
        NSLayoutConstraint.activate([
            messageAndButtonsStackView.bottomAnchor.constraint(equalTo: helpButton.topAnchor, constant: -Layout.stackDistanceToHelpButton),
            
            registerButton.heightAnchor.constraint(equalToConstant: Layout.buttonHeight),
            loginButton.heightAnchor.constraint(equalToConstant: Layout.buttonHeight),
            
            helpButton.heightAnchor.constraint(equalToConstant: Layout.buttonHeight),
            helpButton.widthAnchor.constraint(equalToConstant: Layout.buttonHeight),
            helpButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Layout.helpButtonTrailing),
            helpButton.bottomAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.bottomAnchor, constant: bottomConstant),
            
            usePromoCodeButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            usePromoCodeButton.centerYAnchor.constraint(equalTo: helpButton.centerYAnchor),
            
            hostTextField.heightAnchor.constraint(equalToConstant: Layout.hostTextFieldHeight),
            hostTextField.topAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.topAnchor, constant: Layout.hostTextFieldMargin)
        ])
    }
    
    @objc
    private func tapRegisterButton() {
        viewModel.didTapRegisterButton()
    }
    
    @objc
    func tapLoginButton() {
        viewModel.didTapLoginButton()
    }
    
    @objc
    private func tapUsePromoCodeButton() {
        viewModel.didTapUsePromoCodeButton()
    }
    
    @objc
    private func tapHelpButton() {
        viewModel.didTapHelpButton()
    }
}

#if DEVELOPMENT
extension SignUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        AppParameters.global().setDevelopmentApiHost(hostTextField.text)
        UserDefaults.standard.set(textField.text, forKey: "USER_DEFINED_HOST")
        textField.resignFirstResponder()
        return true
    }
}
#endif

extension SignUpViewController: InputPromoCodeDelegate {
    func didActivatePromoCode(validPromoCode: ValidPromoCode) {
        viewModel.didActivatePromoCode(validPromoCode: validPromoCode)
    }
}

// MARK: View Model Outputs
extension SignUpViewController: SignUpDisplay {
    func updateHostTextField(text: String?) {
        hostTextField.text = text
    }
    
    func updateMainText(text: String) {
        messageLabel.text = text
    }
    
    func updateBackgroundImage(image: UIImage) {
        backgroundImageView.image = image
    }
    
    func updateHelpButton(isBadgeVisible: Bool) {
        helpButton.displayBadge = isBadgeVisible
    }
    
    func alertUserToEnableNotifications(alert: Alert) {
        DispatchQueue.main.async { [weak self] in
            AlertMessage.showAlert(alert, controller: self)
        }
    }
    
    func presentPromoCodePopup() {
        showPopup(InputPromoCodeFactory.make(delegate: self))
    }
    
    func updatePromoCodeButton(isEnabled: Bool) {
        usePromoCodeButton.isHidden = !isEnabled
    }
}
