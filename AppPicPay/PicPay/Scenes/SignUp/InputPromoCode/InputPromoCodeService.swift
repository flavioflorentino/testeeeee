import Core

protocol InputPromoCodeServicing {
    func verifyPromoCode(_ promoCode: String, completion: @escaping(Result<PromoCodeValidationResponse, ApiError>) -> Void)
}

final class InputPromoCodeService: InputPromoCodeServicing {
    func verifyPromoCode(_ promoCode: String, completion: @escaping(Result<PromoCodeValidationResponse, ApiError>) -> Void) {
        Api<PromoCodeValidationResponse>(endpoint: InputPromoCodeEndpoint.verifyPromoCode(promoCode)).execute { result in
            DispatchQueue.main.async {
                let mappedResult = result
                    .map { $0.model }
                               
                completion(mappedResult)
            }
        }
    }
}
