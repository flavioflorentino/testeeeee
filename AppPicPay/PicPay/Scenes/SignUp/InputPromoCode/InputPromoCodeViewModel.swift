import Core
import AnalyticsModule

protocol InputPromoCodeViewModelInputs: AnyObject {
    func didTapCloseButton()
    func didTapConfirmButton(promoCode: String?)
    func didTapCancelButton()
    func didUpdateCodeTextField(with code: String?)
}

final class InputPromoCodeViewModel {
    private let service: InputPromoCodeServicing
    private let presenter: InputPromoCodePresenting

    init(service: InputPromoCodeServicing, presenter: InputPromoCodePresenting) {
        self.service = service
        self.presenter = presenter
    }
    
    private func verifyPromoCode(_ promoCode: String) {
        presenter.hideErrorMessage()
        presenter.showLoadingAnimation()
        service.verifyPromoCode(promoCode) { [weak self] result in
            self?.presenter.hideLoadingAnimation()
            
            switch result {
            case .success(let validationResponse):
                self?.processSuccessfullResponse(validationResponse)
            case .failure(let apiError):
                let errorMessage = self?.parseErrorMessage(error: apiError) ?? DefaultLocalizable.requestError.text
                self?.presenter.displayErrorMessage(errorMessage)
                Analytics.shared.log(SignUpEvent.insertCodeError)
            }
        }
    }
    
    private func processSuccessfullResponse(_ validationResponse: PromoCodeValidationResponse) {
        if let validPromoCodeType = parseValidPromoCodeType(validationResponse.type) {
            let validPromoCode = ValidPromoCode(type: validPromoCodeType, data: validationResponse.data)
            presenter.didNextStep(action: .success(validPromoCode))
            Analytics.shared.log(SignUpEvent.insertCodeSuccess)
        } else {
            presenter.displayErrorMessage(SignUpLocalizable.codeIsNotValid.text)
            Analytics.shared.log(SignUpEvent.insertCodeError)
        }
    }
    
    private func parseErrorMessage(error: ApiError) -> String? {
        switch error {
        case .notFound(let data):
            return data.message
        case .connectionFailure:
            return DefaultLocalizable.errorConnectionViewSubtitle.text
        default:
            return nil
        }
    }
    
    private func parseValidPromoCodeType(_ type: PromoCodeValidationResponse.PromoCodeType) -> PromoCodeValidType? {
        switch type {
        case .mgm:
            return .mgm
        case .promo:
            return .promo
        case .studentAccount:
            return .studentAccount
        default:
            return nil
        }
    }
}

extension InputPromoCodeViewModel: InputPromoCodeViewModelInputs {
    func didTapCloseButton() {
        presenter.didNextStep(action: .close)
    }
    
    func didTapConfirmButton(promoCode: String?) {
        guard let promoCode = promoCode else {
            return
        }
        verifyPromoCode(promoCode)
    }
    
    func didTapCancelButton() {
        presenter.didNextStep(action: .close)
    }
    
    func didUpdateCodeTextField(with code: String?) {
        presenter.hideErrorMessage()
        
        if let code = code, !code.isEmpty {
            presenter.activateConfirmButton()
        } else {
            presenter.deactivateConfirmButton()
        }
    }
}
