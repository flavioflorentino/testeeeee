import Foundation

enum InputPromoCodeFactory {
    static func make(delegate: InputPromoCodeDelegate) -> InputPromoCodeViewController {
        let service: InputPromoCodeServicing = InputPromoCodeService()
        let coordinator: InputPromoCodeCoordinating = InputPromoCodeCoordinator()
        let presenter: InputPromoCodePresenting = InputPromoCodePresenter(coordinator: coordinator)
        let viewModel = InputPromoCodeViewModel(service: service, presenter: presenter)
        let viewController = InputPromoCodeViewController(viewModel: viewModel)

        coordinator.delegate = delegate
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
