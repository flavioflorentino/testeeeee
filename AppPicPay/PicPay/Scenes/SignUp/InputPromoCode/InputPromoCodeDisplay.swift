import UIKit

protocol InputPromoCodeDisplay: AnyObject {
    func showLoadingAnimation()
    func hideLoadingAnimation()
    func deactivateConfirmButton()
    func activateConfirmButton()
    func displayErrorMessage(_ message: String)
    func hideErrorMessage()
}
