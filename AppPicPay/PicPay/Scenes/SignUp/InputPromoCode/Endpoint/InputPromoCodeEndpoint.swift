import Core

enum InputPromoCodeEndpoint {
    case verifyPromoCode(String)
}

extension InputPromoCodeEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .verifyPromoCode(let promoCode):
           return "bff/promo/check-promotional-code/\(promoCode)"
        }
    }
    
    var isTokenNeeded: Bool { false }
    
    var method: HTTPMethod { .post }
}
