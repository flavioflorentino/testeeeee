struct PromoCodeValidationResponse: Decodable {
    enum PromoCodeType: String, Decodable {
        case mgm
        case studentAccount = "student_account"
        case promo
        case invalid
        case notFound = "not_found"
    }
    
    let type: PromoCodeType
    let data: PromoCodeValidationData
}

struct PromoCodeValidationData: Decodable {
    private enum CodingKeys: String, CodingKey {
        case referralName = "referral_name"
        case referralCode = "referral_code"
        case faqLink = "faq_link"
        case promoAmount = "promo_amount"
        case title
        case description
        case benefits
    }
    
    let referralName: String?
    let referralCode: String
    let faqLink: String?
    let promoAmount: String?
    let title: String
    let description: String
    let benefits: [String]?
}
