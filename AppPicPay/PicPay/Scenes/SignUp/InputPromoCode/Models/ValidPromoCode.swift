enum PromoCodeValidType {
    case mgm
    case studentAccount
    case promo
}

struct ValidPromoCode {
    let type: PromoCodeValidType
    let data: PromoCodeValidationData
}
