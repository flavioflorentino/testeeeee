protocol InputPromoCodeDelegate: AnyObject {
    func didActivatePromoCode(validPromoCode: ValidPromoCode)
}

enum InputPromoCodeAction {
    case success(ValidPromoCode)
    case close
}

protocol InputPromoCodeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: InputPromoCodeDelegate? { get set }
    func perform(action: InputPromoCodeAction)
}

final class InputPromoCodeCoordinator: InputPromoCodeCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: InputPromoCodeDelegate?
    
    func perform(action: InputPromoCodeAction) {
        switch action {
        case .success(let validPromoCode):
            viewController?.dismiss(animated: true) {
                self.delegate?.didActivatePromoCode(validPromoCode: validPromoCode)
            }
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
