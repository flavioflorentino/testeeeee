import SkyFloatingLabelTextField
import UI
import AnalyticsModule

final class InputPromoCodeViewController: ViewController<InputPromoCodeViewModelInputs, UIView> {
    private enum Layout {
        static let popupWidth: CGFloat = min(UIScreen.main.bounds.width - (2 * 12), 300)
        static let titleFont = UIFont.systemFont(ofSize: 18, weight: .bold)
        static let messageFont = UIFont.systemFont(ofSize: 14, weight: .regular)
        static let messageLineHeight: CGFloat = 1.26
        static let errorFont = UIFont.systemFont(ofSize: 12, weight: .regular)
        static let ppButtonHeight: CGFloat = 44.0
        static let closeButtonSize: CGFloat = 40.0
        static let internalSpacing: CGFloat = 8.0
        static let externalMargin: CGFloat = 24.0
        static let titleTopMargin: CGFloat = 40.0
        static let confirmButtonTopSpacing: CGFloat = 20.0
        static let cancelButtonBottomSpacing: CGFloat = 16.0
    }
    
    private lazy var closeButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(Assets.NewGeneration.closePopup.image, for: .normal)
        button.setTitleColor(Palette.ppColorGrayscale400.color, for: .normal)
        button.tintColor = Palette.ppColorGrayscale400.color
        button.imageView?.tintColor = Palette.ppColorGrayscale400.color
        button.addTarget(self, action: #selector(tapCloseButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = Layout.titleFont
        label.text = SignUpLocalizable.insertYourCode.text
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = Layout.messageLineHeight
        paragraphStyle.alignment = .center
        paragraphStyle.lineBreakMode = .byWordWrapping
        
        label.attributedText = NSAttributedString(
            string: SignUpLocalizable.typeYourCodeBelow.text,
            attributes: [
                .paragraphStyle: paragraphStyle,
                .font: Layout.messageFont,
                .foregroundColor: Palette.ppColorGrayscale400.color
            ]
        )
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var codeTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.lineColor = Palette.ppColorBranding300.color
        textField.selectedLineColor = Palette.ppColorBranding300.color
        textField.textAlignment = .center
        textField.autocorrectionType = .no
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField.autocapitalizationType = .allCharacters
        return textField
    }()
    
    private lazy var errorLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = Layout.errorFont
        label.textColor = Palette.ppColorNegative500.color
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        label.isHidden = true
        return label
    }()
    
    private lazy var textFieldStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Layout.internalSpacing
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private lazy var confirmButton: UIPPButton = {
        let title = SignUpLocalizable.activate.text
        let button = UIPPButton(title: title, target: self, action: #selector(tapConfirmButton), type: .cta)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isEnabled = false
        return button
    }()
    
    private lazy var cancelButton: UIPPButton = {
        let title = DefaultLocalizable.btCancel.text
        let button = UIPPButton(title: title, target: self, action: #selector(tapCancelButton), type: .cleanUnderlined)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        codeTextField.becomeFirstResponder()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(closeButton)
        view.addSubview(titleLabel)
        view.addSubview(messageLabel)
        
        textFieldStackView.addArrangedSubview(codeTextField)
        textFieldStackView.addArrangedSubview(errorLabel)
        view.addSubview(textFieldStackView)

        view.addSubview(confirmButton)
        view.addSubview(cancelButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(
            equalTo: view,
            for: [titleLabel, messageLabel, codeTextField, confirmButton, cancelButton],
            constant: Layout.externalMargin
        )
        NSLayoutConstraint.activate([
            closeButton.topAnchor.constraint(equalTo: view.topAnchor),
            closeButton.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            closeButton.heightAnchor.constraint(equalToConstant: Layout.closeButtonSize),
            closeButton.widthAnchor.constraint(equalToConstant: Layout.closeButtonSize),
            
            titleLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: Layout.titleTopMargin),
            messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Layout.internalSpacing),
            
            textFieldStackView.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: Layout.internalSpacing),
            
            confirmButton.topAnchor.constraint(equalTo: textFieldStackView.bottomAnchor, constant: Layout.confirmButtonTopSpacing),
            confirmButton.heightAnchor.constraint(equalToConstant: Layout.ppButtonHeight),
            
            cancelButton.topAnchor.constraint(equalTo: confirmButton.bottomAnchor, constant: Layout.internalSpacing),
            cancelButton.heightAnchor.constraint(equalToConstant: Layout.ppButtonHeight),
            cancelButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Layout.cancelButtonBottomSpacing),
            
            view.widthAnchor.constraint(equalToConstant: Layout.popupWidth)
        ])
    }
}

@objc
private extension InputPromoCodeViewController {
    func tapCloseButton() {
        viewModel.didTapCloseButton()
    }
    
    func tapConfirmButton() {
        viewModel.didTapConfirmButton(promoCode: codeTextField.text)
        Analytics.shared.log(SignUpEvent.insertCode(.activate))
    }
    
    func tapCancelButton() {
        viewModel.didTapCancelButton()
        Analytics.shared.log(SignUpEvent.insertCode(.cancel))
    }
    
    func keyboardWillShow(notification: NSNotification) {
        guard
            let userInfo = notification.userInfo,
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            else {
                return
        }
        let screenSizeAvailable = UIScreen.main.bounds.height - endFrame.size.height
        view.frame.origin.y = (screenSizeAvailable / 2) - (view.frame.size.height / 2)
    }
    
    func keyboardWillHide(notification: NSNotification) {
        view.frame.origin.y = (UIScreen.main.bounds.height / 2) - (view.frame.size.height / 2)
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        viewModel.didUpdateCodeTextField(with: textField.text)
    }
}

extension InputPromoCodeViewController: InputPromoCodeDisplay {
    func showLoadingAnimation() {
        confirmButton.startLoadingAnimating()
        codeTextField.isEnabled = false
    }
    
    func hideLoadingAnimation() {
        confirmButton.stopLoadingAnimating()
        codeTextField.isEnabled = true
    }
    
    func deactivateConfirmButton() {
        confirmButton.isEnabled = false
    }
    
    func activateConfirmButton() {
        confirmButton.isEnabled = true
    }
    
    func displayErrorMessage(_ message: String) {
        errorLabel.isHidden = false
        errorLabel.text = message
    }
    
    func hideErrorMessage() {
       errorLabel.text = ""
       errorLabel.isHidden = true
    }
}
