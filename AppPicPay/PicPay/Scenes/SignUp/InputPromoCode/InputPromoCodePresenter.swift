import Core
import Foundation

protocol InputPromoCodePresenting: AnyObject {
    var viewController: InputPromoCodeDisplay? { get set }
    func didNextStep(action: InputPromoCodeAction)
    func showLoadingAnimation()
    func hideLoadingAnimation()
    func deactivateConfirmButton()
    func activateConfirmButton()
    func displayErrorMessage(_ message: String)
    func hideErrorMessage()
}

final class InputPromoCodePresenter: InputPromoCodePresenting {
    private let coordinator: InputPromoCodeCoordinating
    var viewController: InputPromoCodeDisplay?

    init(coordinator: InputPromoCodeCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: InputPromoCodeAction) {
        coordinator.perform(action: action)
    }
    
    func showLoadingAnimation() {
        viewController?.showLoadingAnimation()
    }
    
    func hideLoadingAnimation() {
        viewController?.hideLoadingAnimation()
    }
    
    func deactivateConfirmButton() {
        viewController?.deactivateConfirmButton()
    }
    
    func activateConfirmButton() {
        viewController?.activateConfirmButton()
    }
    
    func displayErrorMessage(_ message: String) {
        viewController?.displayErrorMessage(message)
    }
    
    func hideErrorMessage() {
        viewController?.hideErrorMessage()
    }
}
