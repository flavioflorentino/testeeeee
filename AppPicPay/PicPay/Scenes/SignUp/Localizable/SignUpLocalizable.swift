enum SignUpLocalizable: String, Localizable {
    case configurations
    case authorize
    case enableNotificationAlertTitle
    case enableNotificationAlertMessage
    case register
    case enter
    case helpCenter
    case iAlreadyHaveACode
    
    case insertYourCode
    case typeYourCodeBelow
    case activate
    case codeIsNotValid
    
    var key: String { rawValue }
    
    var file: LocalizableFile { .signUp }
}
