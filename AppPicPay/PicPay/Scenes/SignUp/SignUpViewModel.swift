import AnalyticsModule
import Core
import FeatureFlag

protocol SignUpViewModelInputs: AnyObject {
    func viewDidLoad()
    func didTapRegisterButton()
    func didTapLoginButton()
    func didTapUsePromoCodeButton()
    func didTapHelpButton()
    func newConversationStarted()
    func didActivatePromoCode(validPromoCode: ValidPromoCode)
}

final class SignUpViewModel {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    
    private let service: SignUpServicing
    private let presenter: SignUpPresenting
    private let referralWorker: ReferralRecommendationProtocol
    private let dependencies: Dependencies

    private lazy var backgroundImage = Assets.SignUp.iluManGreenHair.image

    init(service: SignUpServicing, presenter: SignUpPresenting, referralWorker: ReferralRecommendationProtocol, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.referralWorker = referralWorker
        self.dependencies = dependencies
    }
    
    private func createAlertToEnableNotifications(alreadyAsked: Bool) -> Alert {
        let cancelButtonTitle = alreadyAsked ? DefaultLocalizable.btCancel.text : DefaultLocalizable.notNow.text
        let cancelButton = Button(title: cancelButtonTitle, type: .clean, action: .close)
        
        var okButton: Button
        if alreadyAsked {
            okButton = Button(title: SignUpLocalizable.configurations.text, type: .cta) { popupController, _ in
                popupController.dismiss(animated: true)
                guard let url = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        } else {
            okButton = Button(title: SignUpLocalizable.authorize.text, type: .cta) { popupController, _ in
                popupController.dismiss(animated: true)
                PPPermission.requestNotifications()
            }
        }
        
        let alert = Alert(with: SignUpLocalizable.enableNotificationAlertTitle.text, text: SignUpLocalizable.enableNotificationAlertMessage.text)
        alert.buttons = [okButton, cancelButton]
        return alert
    }
    
    private func sendDidLoadAnalytics() {
        logRemoteConfigLoaded()
        logScreenViewed()
        logFirstTimeLaunch()
    }
    
    private func logRemoteConfigLoaded() {
        let landingScreenConfig = SignUpEvent.LandingScreenConfig(
            remoteConfigLoaded: KVStore().boolFor(.isRemoteConfigLoaded),
            imagesV2: dependencies.featureManager.text(.landingScreenImagesV2),
            textV2: dependencies.featureManager.text(.landingScreenTextV2)
        )
        let remoteConfigLoadedEvent = SignUpEvent.remoteConfigLoaded(landingScreenConfig: landingScreenConfig)
        dependencies.analytics.log(remoteConfigLoadedEvent)
    }
    
    private func logScreenViewed() {
        dependencies.analytics.log(SignUpEvent.screenViewed(promoCodeButtonVisible: false))
    }
    
    private func logFirstTimeLaunch() {
        guard service.isFirstTimeOnApp else {
            return
        }
        dependencies.analytics.log(SignUpEvent.firstTimeOpened)
        service.setNotFirstTimeOnApp()
    }
}

extension SignUpViewModel: SignUpViewModelInputs {
    func viewDidLoad() {
        presenter.updateHostTextField(text: service.devApiHost)
        presenter.updateMainText(text: service.mainText)
        presenter.updateBackgroundImage(backgroundImage)
        presenter.updatePromoCodeButton(isEnabled: service.enablePromoCodeButton)
        sendDidLoadAnalytics()
    }
    
    func didTapRegisterButton() {
        dependencies.analytics.log(SignUpEvent.didTapRegister)
        dependencies.analytics.log(SignUpEvent.optionSelected(.register))
        presenter.didNextStep(action: .register)
        registerReferralEventIfNeeded(event: .register)
    }
    
    func didTapLoginButton() {
        registerReferralEventIfNeeded(event: .login)
        dependencies.analytics.log(SignUpEvent.optionSelected(.login))
        presenter.didNextStep(action: .login)
    }
    
    func didTapUsePromoCodeButton() {
        presenter.presentPromoCodePopup()
    }
    
    func didTapHelpButton() {
        registerReferralEventIfNeeded(event: .help)
        dependencies.analytics.log(SignUpEvent.optionSelected(.help))
        presenter.didNextStep(action: .helpCenter)
    }
    
    func registerReferralEventIfNeeded(event: ReferralRecommendationEvent.OptionSelected) {
        if referralWorker.isFromReferral {
            dependencies.analytics.log(ReferralRecommendationEvent.optionSelected(event))
        }
    }
    
    func newConversationStarted() {
        // When a user starts a new conversation on chat, the app needs to use notifications to send the response.
        service.getNotificationAuthorizationStatus { [weak self] status in
            guard
                let self = self,
                status == .denied
                else {
                    return
            }
            
            let alreadyAsked = self.service.askedForRegistrationRemoteNotification
            let alert = self.createAlertToEnableNotifications(alreadyAsked: alreadyAsked)
            self.presenter.alertUserToEnableNotifications(alert: alert)
        }
    }
    
    func didActivatePromoCode(validPromoCode: ValidPromoCode) {
        presenter.didNextStep(action: .promoCodeActivated(validPromoCode: validPromoCode))
    }
}
