import Core
import CoreLegacy
import FeatureFlag

protocol SignUpServicing {
    var devApiHost: String? { get }
    var mainText: String { get }
    var enablePromoCodeButton: Bool { get }
    var isUserAuthenticated: Bool { get }
    var askedForRegistrationRemoteNotification: Bool { get }
    var isFirstTimeOnApp: Bool { get }
    func setNotFirstTimeOnApp()
    func getNotificationAuthorizationStatus(_ completion: @escaping (UNAuthorizationStatus) -> Void)
}

final class SignUpService: SignUpServicing {
    typealias Dependencies = HasUserManager & HasKeychainManager
    private let dependencies: Dependencies
    
    private let appSessionParameters: AppSessionParametersProvider
    private let referralWorker: ReferralRecommendationProtocol?
    
    var devApiHost: String? {
        #if DEVELOPMENT
        return dependencies.keychain.getData(key: KeychainKey.hostBaseUrl) ?? AppParameters.global().developmentApiHost() ?? Environment.apiUrl
        #else
        return nil
        #endif
    }
    
    var mainText: String {
        guard
            let referralWorker = referralWorker,
            let recommendationName = referralWorker.recommendationName,
            referralWorker.isFromReferral else {
                  return FeatureManager.text(.landingScreenTextV2)
        }

        return String(format: FeatureManager.text(.referralScreenText), recommendationName)
    }
    
    var enablePromoCodeButton: Bool {
        FeatureManager.isActive(.enterWithPromotionalCode)
    }
    
    var isUserAuthenticated: Bool {
        dependencies.userManager.isAuthenticated
    }
    
    var askedForRegistrationRemoteNotification: Bool {
        KVStore().boolFor(.askedForRegistrationRemoteNotification)
    }
    
    var isFirstTimeOnApp: Bool {
        !appSessionParameters.notFirstTimeOnApp()
    }
    
    init(
        appSessionParameters: AppSessionParametersProvider,
        dependencies: Dependencies,
        referralWorker: ReferralRecommendationProtocol?
    ) {
        self.appSessionParameters = appSessionParameters
        self.dependencies = dependencies
        self.referralWorker = referralWorker
    }
    
    func setNotFirstTimeOnApp() {
        appSessionParameters.setNotFirstTimeOnApp()
    }
    
    func getNotificationAuthorizationStatus(_ completion: @escaping (UNAuthorizationStatus) -> Void) {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            completion(settings.authorizationStatus)
        }
    }
}
