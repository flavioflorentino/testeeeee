import AnalyticsModule
import Core
import FeatureFlag

enum SignUpEvent: AnalyticsKeyProtocol {
    case firstTimeOpened
    case didTapRegister
    case screenViewed(promoCodeButtonVisible: Bool)
    case optionSelected(_ option: OptionSelected)
    case remoteConfigLoaded(landingScreenConfig: LandingScreenConfig)
    case insertCode(_ type: InsertCodeOptionType)
    case insertCodeSuccess
    case insertCodeError
   
    private var name: String {
        switch self {
        case .firstTimeOpened:
            return "First time open"
        case .didTapRegister:
            return "cadastro_criar_carteira_picpay"
        case .screenViewed:
            return "Viewed"
        case .optionSelected:
            return "Option Selected"
        case .remoteConfigLoaded:
            return "Remote Config Loaded"
        case .insertCode:
            return "Insert Code - Option Selected"
        case .insertCodeSuccess:
            return "Inseriu código promocional - sucesso"
        case .insertCodeError:
            return "Inseriu código promocional - erro"
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.firebase, .mixPanel]
    }
    
    private var properties: [String: Any] {
        switch self {
        case .screenViewed(let promoCodeButtonVisible):
            return ["screen_promo_code": promoCodeButtonVisible.stringValue]
        case .optionSelected(let option):
            return ["option_selected": option.rawValue]
        case let .remoteConfigLoaded(landingScreenConfig):
            return [
                "remote_config_ready": landingScreenConfig.remoteConfigLoaded.stringValue,
                "feature_landing_images_v2": landingScreenConfig.imagesV2,
                "feature_landing_text_v2": landingScreenConfig.textV2
            ]
        case .insertCode(let type):
            return ["option_selected": type.rawValue]
        default:
            return [:]
        }
    }
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case .screenViewed, .optionSelected:
            return AnalyticsEvent("Start Screen - " + name, properties: properties, providers: providers)
        case .remoteConfigLoaded:
            return AnalyticsEvent("Landing Screen - " + name, properties: properties, providers: providers)
        default:
            return AnalyticsEvent(name, properties: properties, providers: providers)
        }
    }
}

extension SignUpEvent {
    enum OptionSelected: String {
        case register = "CADASTRAR"
        case login = "ENTRAR"
        case help = "AJUDA"
        case promoCode = "JA TENHO CODIGO"
    }
    
    enum InsertCodeOptionType: String {
        case activate = "ATIVAR"
        case cancel = "CANCELAR"
    }
    
    struct LandingScreenConfig {
        let remoteConfigLoaded: Bool
        let imagesV2: String
        let textV2: String
    }
}

private extension Bool {
    var stringValue: String {
        self ? "TRUE" : "FALSE"
    }
}

@objcMembers
final class OBJCSignUpAnalytics: NSObject {
    static func logRemoteConfigLoaded() {
        let landingScreenConfig = SignUpEvent.LandingScreenConfig(
            remoteConfigLoaded: KVStore().boolFor(.isRemoteConfigLoaded),
            imagesV2: FeatureManager.text(.landingScreenImagesV2),
            textV2: FeatureManager.text(.landingScreenTextV2)
        )
        let remoteConfigLoadedEvent = SignUpEvent.remoteConfigLoaded(landingScreenConfig: landingScreenConfig)
        Analytics.shared.log(remoteConfigLoadedEvent)
    }
}
