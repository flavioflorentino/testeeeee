import Core
import Foundation

protocol SignUpPresenting: AnyObject {
    var viewController: SignUpDisplay? { get set }
    func updateHostTextField(text: String?)
    func updateMainText(text: String)
    func updateBackgroundImage(_ image: UIImage)
    func didNextStep(action: SignUpAction)
    func updateHelpButton(isBadgeVisible: Bool)
    func alertUserToEnableNotifications(alert: Alert)
    func presentPromoCodePopup()
    func updatePromoCodeButton(isEnabled: Bool)
}

final class SignUpPresenter: SignUpPresenting {
    private let coordinator: SignUpCoordinating
    var viewController: SignUpDisplay?

    init(coordinator: SignUpCoordinating) {
        self.coordinator = coordinator
    }
    
    func updateHostTextField(text: String?) {
        viewController?.updateHostTextField(text: text)
    }
    
    func updateMainText(text: String) {
        let multilineText = text.replacingOccurrences(of: "\\n", with: "\n")
        viewController?.updateMainText(text: multilineText)
    }
    
    func updateBackgroundImage(_ image: UIImage) {
        viewController?.updateBackgroundImage(image: image)
    }
    
    func didNextStep(action: SignUpAction) {
        coordinator.perform(action: action)
    }
    
    func updateHelpButton(isBadgeVisible: Bool) {
        viewController?.updateHelpButton(isBadgeVisible: isBadgeVisible)
    }
    
    func alertUserToEnableNotifications(alert: Alert) {
        viewController?.alertUserToEnableNotifications(alert: alert)
    }
    
    func presentPromoCodePopup() {
        viewController?.presentPromoCodePopup()
    }
    
    func updatePromoCodeButton(isEnabled: Bool) {
        viewController?.updatePromoCodeButton(isEnabled: isEnabled)
    }
}
