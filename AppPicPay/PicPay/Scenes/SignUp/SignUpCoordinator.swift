import AnalyticsModule
import CustomerSupport
import FeatureFlag
import Registration

enum SignUpAction {
    case login
    case helpCenter
    case register
    case promoCodeActivated(validPromoCode: ValidPromoCode)
}

protocol SignUpCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: SignUpAction)
}

final class SignUpCoordinator: SignUpCoordinating {
    typealias Dependencies = HasFeatureManager & HasDynamicLinkHelper & HasAnalytics
    private let dependencies: Dependencies
    private let registrationCacheHelper: RegistrationCacheHelperContract
    private let registrationAccountSetupContract: RegistrationAccountSetupContract
    private lazy var isDynamicRegistrationActive = dependencies.featureManager.isActive(.dynamicRegistrationSteps)
    
    weak var viewController: UIViewController?
    private var childCoordinator: Coordinator?
    
    init(
        dependencies: Dependencies,
        registrationCacheHelper: RegistrationCacheHelperContract = RegistrationCacheHelper(),
        registrationAccountSetupContract: RegistrationAccountSetupContract = RegistrationAccountSetupFactory.make()) {
        self.dependencies = dependencies
        self.registrationCacheHelper = registrationCacheHelper
        self.registrationAccountSetupContract = registrationAccountSetupContract
    }
    
    func perform(action: SignUpAction) {
        switch action {
        case .login:
            guard let navController = viewController?.navigationController else {
                return
            }
            let loginCoordinator = LoginCoordinator(navigationController: navController)
            childCoordinator = loginCoordinator
            loginCoordinator.start()
        
        case .register:
            performUserRegistration()
        case .helpCenter:
            guard let viewController = viewController else { return }
            let chatbotUrlPath = dependencies.featureManager.text(.chatbotUnlogedUser)
            let chatBotController = ChatBotFactory.make(urlPath: chatbotUrlPath)
            let navigationController = PPNavigationController(rootViewController: chatBotController)
            viewController.navigationController?.present(navigationController, animated: true)
            
        case .promoCodeActivated(let validPromoCode):
            performPromoCodeActivated(validPromoCode: validPromoCode)
        }
    }
    
    private func performPromoCodeActivated(validPromoCode: ValidPromoCode) {
        let nextViewController: UIViewController
        
        switch validPromoCode.type {
        case .mgm, .promo:
            nextViewController = RegisterPromoCodeFactory.make(with: validPromoCode, delegate: self)
        case .studentAccount:
            nextViewController = RegisterStudentPromoCodeFactory.make(with: validPromoCode, delegate: self)
        }
        
        viewController?.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    private func performUserRegistration(with promoCode: ValidPromoCode? = nil) {
        if isDynamicRegistrationActive {
            pushNewUserRegistration(with: promoCode)
        } else {
            pushOldUserRegistration(with: promoCode)
        }
    }
    
    private func pushNewUserRegistration(with validPromoCode: ValidPromoCode?) {
        guard let navController = viewController?.navigationController else {
            return
        }
        
        childCoordinator = RegistrationFlowCoordinator(
            dependencies: dependencies,
            navigationController: navController,
            promoCode: validPromoCode?.data.referralCode ?? dependencies.dynamicLinkHelper.referralCodeToRegistration
        )
        childCoordinator?.start()
        logScreenViewedRegistration()
    }
    
    private func pushOldUserRegistration(with validPromoCode: ValidPromoCode?) {
        guard
            let navigationController = viewController?.navigationController,
            let registrationController = ViewsManager.registrationStoryboardViewController(withIdentifier: "RegistrationNameStep") as? RegistrationStepViewController
            else {
                return
        }
        
        registrationController.setup(RegistrationViewModel(promoCode: validPromoCode?.data.referralCode))
        navigationController.pushViewController(registrationController, animated: true)
        logScreenViewedRegistration()
    }
    
    private func logScreenViewedRegistration() {
        var event: RegistrationEvent
        let defaultStepSequence: [RegistrationStep] = [.name, .phone, .email, .password, .document]
        let dynamicStepSequence = dependencies.featureManager.object(.dynamicRegistrationStepsSequence, type: [RegistrationStep].self)
        
        if isDynamicRegistrationActive {
            event = .startScreenViewed(dynamicRegistrations: true, registrationSteps: dynamicStepSequence ?? defaultStepSequence)
        } else {
            event = .startScreenViewed(dynamicRegistrations: false, registrationSteps: defaultStepSequence)
        }
        dependencies.analytics.log(event)
    }
}

extension SignUpCoordinator: RegisterPromoCodeCoordinatorDelegate, RegisterStudentPromoCodeCoordinatorDelegate {
    func presentUserRegistration(with promoCode: ValidPromoCode) {
        performUserRegistration(with: promoCode)
    }
}
