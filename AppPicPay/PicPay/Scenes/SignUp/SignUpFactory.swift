import Foundation
import CoreLegacy

enum SignUpFactory {
    static func make() -> SignUpViewController {
        let container = DependencyContainer()
        let referralWorker: ReferralRecommendationProtocol = ReferralRecommendationWorker()
        let service: SignUpServicing = SignUpService(appSessionParameters: AppParameters.global(), dependencies: container, referralWorker: referralWorker)
        let coordinator: SignUpCoordinating = SignUpCoordinator(dependencies: container)
        let presenter: SignUpPresenting = SignUpPresenter(coordinator: coordinator)
        let viewModel = SignUpViewModel(service: service, presenter: presenter, referralWorker: referralWorker, dependencies: container)
        let viewController = SignUpViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
