import UIKit

protocol SignUpDisplay: AnyObject {
    func updateHostTextField(text: String?)
    func updateMainText(text: String)
    func updateBackgroundImage(image: UIImage)
    func updateHelpButton(isBadgeVisible: Bool)
    func alertUserToEnableNotifications(alert: Alert)
    func presentPromoCodePopup()
    func updatePromoCodeButton(isEnabled: Bool)
}
