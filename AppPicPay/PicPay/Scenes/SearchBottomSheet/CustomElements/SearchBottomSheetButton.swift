import SkeletonView
import UI

final class SearchBottomSheetButton: UIButton {
    var viewNeedsBottomLine = true
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        semanticContentAttribute = .forceRightToLeft
        contentHorizontalAlignment = .left
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(withTitles titles: (normal: String, selected: String), andIcons iconImages: (normal: UIImage?, selected: UIImage?)) {
        setTitle(titles.normal, for: .normal)
        setTitle(titles.selected, for: .selected)
        setImage(iconImages.normal, for: .normal)
        setImage(iconImages.selected, for: .selected)
        titleLabel?.font = Typography.bodyPrimary().font()
        titleLabel?.linesCornerRadius = Int(Spacing.base01)
    }
    
    override func draw(_ rect: CGRect) {
        guard viewNeedsBottomLine else {
            return
        }
        drawBottomLine()
    }
    
    private func drawBottomLine() {
        Colors.grayscale200.color.setStroke()
        let line = UIBezierPath()
        line.lineWidth = 1
        line.move(to: CGPoint(x: bounds.origin.x, y: frame.height))
        line.addLine(to: CGPoint(x: bounds.width, y: frame.height))
        line.stroke()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let titleWidth = titleLabel?.frame.width ?? 0
        let imageWidth = imageView?.frame.width ?? 0
        let leftInset = frame.width - (imageWidth + titleWidth)
        imageEdgeInsets = UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: 0)
    }
}
