import Core

protocol SearchBottomSheetServicing {
    func loadPayableOptions(
        forType type: SearchBottomSheetType,
        andID id: String,
        completion: @escaping (_ response: Result<[SearchBottomSheetItem], ApiError>) -> Void
    )
}

final class SearchBottomSheetService: SearchBottomSheetServicing {
    func loadPayableOptions(
        forType type: SearchBottomSheetType,
        andID id: String,
        completion: @escaping (_ response: Result<[SearchBottomSheetItem], ApiError>) -> Void
    ) {
        Api<[SearchBottomSheetItem]>(endpoint: SearchBottomSheetEndpoint.options(type, id)).execute { result in
            switch result {
            case let .success(response):
                completion(.success(response.model))
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
}
