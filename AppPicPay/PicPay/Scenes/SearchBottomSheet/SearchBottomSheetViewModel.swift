import AnalyticsModule
import FeatureFlag
import Foundation

protocol SearchBottomSheetViewModelInputs: AnyObject {
    func loadPayableOptions()
    func getOptionsCount() -> Int
    func updateSheetInfo()
    func checkIfLastCellIsOnDisplay(indexPath: IndexPath) -> Bool
    func getCustomHeight(fromTableView tableView: UITableView) -> CGFloat
    func getOptionsForCell(indexPath: IndexPath) -> SearchBottomSheetItem
    func didTapButton(option: SearchBottomSheetItem)
    func loadContactProfile(image: UIPPProfileImage, contact: PPContact)
    func loadStoreProfile(image: UIPPProfileImage, store: PPStore)
}

final class SearchBottomSheetViewModel {
    typealias Dependencies = AppDependencies & HasFeatureManager
    private let dependencies: Dependencies
    private let service: SearchBottomSheetServicing
    private let presenter: SearchBottomSheetPresenting
    private let favoritesInteractor: FavoriteInteracting
    private let data: SearchResultBasics
    private let cellObject: NSObject
    
    private var favoriteType: Favorite.`Type` = .unknown
    private var itemId: String = ""
    private var cellType: SearchResultType = .unknown
    private var payableOptions: [SearchBottomSheetItem] = []
    var payableOptionsCount = 0

    init(
        service: SearchBottomSheetServicing,
        presenter: SearchBottomSheetPresenting,
        data: SearchResultBasics,
        object: NSObject,
        interactor: FavoriteInteracting,
        dependencies: Dependencies,
        tabId: String?
    ) {
        self.service = service
        self.presenter = presenter
        self.data = data
        self.cellObject = object
        self.favoritesInteractor = interactor
        self.dependencies = dependencies
        setupObjectBasicData()
        
        dependencies.analytics.log(SearchBottomSheetEvent.bottomSheetAccessed(favoriteType, data.title ?? "no title", tabId ?? "no tab"))
    }
    
    private func setupObjectBasicData() {
        guard
            let id = data.id,
            let type = data.type
            else {
                return
        }
        itemId = id
        cellType = type
        
        switch type {
        case .person:
            favoriteType = .consumer
        case .store:
            guard let sellerId = data.sellerId else {
                break
            }
            itemId = sellerId
            favoriteType = .store
        case .digitalGood:
            favoriteType = .digitalGood
        default:
            break
        }
    }
    
    private func createNotListedItem(
        withItemType itemType: BottomSheetItemType,
        andActionType actionType: BottomSheetActionType
    ) {
        let action = SearchBottomSheetItemAction(type: actionType, data: "")
        let notListedItem = SearchBottomSheetItem(type: itemType, title: "", action: action)
        payableOptions = [notListedItem]
    }
    
    private func loadNotListedOptions() {
        guard let title = data.title else {
            return
        }
        
        switch title {
        case SearchBottomSheetLocalizable.p2mQrcode.text:
            createNotListedItem(withItemType: .activable, andActionType: .qrcode)
        default:
            createNotListedItem(withItemType: .activable, andActionType: .p2m)
        }
        
        dependencies.mainQueue.async { [weak self] in
            self?.presenter.listPayableOptions(options: self?.payableOptions ?? [], isDefault: true)
        }
    }
    
    private func performFavoritesCheck(completion: @escaping (FavoriteState) -> Void) {
        favoritesInteractor.checkState(id: itemId, type: favoriteType) { state in
            completion(state)
        }
    }
    
    private func performFollowingCheck(completion: @escaping (SearchBottomSheetFollowingState) -> Void) {
        guard
            let option = payableOptions.first(where: { $0.action.type == .following }),
            let state = SearchBottomSheetFollowingState(rawValue: (option.action.data ?? "not_following"))
            else {
                return
        }
        completion(state)
    }
        
    private func performOptionsChecking(completion: @escaping (FavoriteState, String, SearchResultType, Favorite.`Type`) -> Void) {
        var favoriteType: Favorite.`Type` = .unknown
        var itemId: String = ""
        
        guard
            let id = data.id,
            let type = data.type
            else {
                return
        }
        itemId = id
        
        switch type {
        case .person:
            favoriteType = .consumer
        case .store:
            guard let sellerId = data.sellerId else {
                break
            }
            itemId = sellerId
            favoriteType = .store
        default:
            break
        }
        
        payableOptions.forEach { option in
            switch option.action.type {
            case .favorite:
                favoritesInteractor.checkState(id: itemId, type: favoriteType) { state in
                    completion(state, itemId, type, favoriteType)
                }
            default:
                break
            }
        }
    }
    
    private func updateFavoriteButtonState(_ state: FavoriteState) {
        guard let action = payableOptions.first(where: { $0.action.type == .favorite })?.action else {
            return
        }

        switch state {
        case .unavailable:
            action.isFavorite = nil
        case .favorited:
            action.isFavorite = true
        case .unfavorited:
            action.isFavorite = false
        }
    }
    
    private func updateFollowingButtonState(_ state: SearchBottomSheetFollowingState) {
        payableOptions.filter { $0.action.type == .following }[0].action.isFollowing = state
    }
    
    private func checkOptionsStates() {
        if payableOptions.filter({ $0.action.type == .favorite }).isNotEmpty {
            dependencies.dispatchGroup.enter()
            performFavoritesCheck { [weak self] state in
                self?.updateFavoriteButtonState(state)
                self?.dependencies.dispatchGroup.leave()
            }
        }
        
        if payableOptions.filter({ $0.action.type == .following }).isNotEmpty {
            dependencies.dispatchGroup.enter()
            performFollowingCheck { [weak self] state in
                self?.updateFollowingButtonState(state)
                self?.dependencies.dispatchGroup.leave()
            }
        }
        
        dependencies.dispatchGroup.notify(queue: dependencies.mainQueue) { [weak self] in
            self?.presenter.listPayableOptions(
                options: self?.payableOptions ?? [],
                isDefault: false,
                cellType: self?.cellType ?? .unknown,
                cellId: self?.itemId ?? ""
            )
        }
    }
    
    private func checkIfUserIsFavoriteToUpdateButton() {
        performOptionsChecking { [weak self] state, id, type, _ in
            self?.updateFavoriteButtonState(state)
            self?.presenter.listPayableOptions(options: self?.payableOptions ?? [], isDefault: false, cellType: type, cellId: id)
        }
    }
    
    private func checkStateForFollowingButtonAction(state: SearchBottomSheetFollowingState) {
        switch state {
        case .notFollowing:
            dependencies.analytics.log(SearchBottomSheetEvent.socialStatusChangedFollowing(.waiting, oldStatus: state))
            WSSocial.follow(itemId) { success, _, _, _ in
                if success {
                    self.updateFollowingButtonState(.waiting)
                    self.dependencies.dispatchGroup.leave()
                }
            }
        default:
            dependencies.analytics.log(SearchBottomSheetEvent.socialStatusChangedFollowing(.notFollowing, oldStatus: state))
            WSSocial.unfollow(itemId) { success, _, _, _ in
                if success {
                    self.updateFollowingButtonState(.notFollowing)
                    self.dependencies.dispatchGroup.leave()
                }
            }
        }
    }
}

extension SearchBottomSheetViewModel: SearchBottomSheetViewModelInputs {
    func updateSheetInfo() {
        presenter.updateInfo(withCellObject: cellObject, andName: data.title ?? "")
    }
    
    func loadPayableOptions() {
        guard
            let type = data.type,
            let bottomSheetType = SearchBottomSheetType(rawValue: type.rawValue),
            let id = data.id
            else {
                self.loadNotListedOptions()
                return
        }
        
        var customId = id
        
        if type == .store {
            customId = data.sellerId ?? ""
        }
        
        service.loadPayableOptions(
            forType: bottomSheetType,
            andID: customId
        ) { [weak self] result in
            switch result {
            case let .success(response):
                self?.payableOptions = response
                self?.checkOptionsStates()
            case .failure:
                self?.loadNotListedOptions()
                self?.checkIfUserIsFavoriteToUpdateButton()
            }
        }
    }
    
    func getOptionsCount() -> Int {
        payableOptions.count
    }
    
    func checkIfLastCellIsOnDisplay(indexPath: IndexPath) -> Bool {
        indexPath.row == (getOptionsCount() - 1)
    }
    
    func getCustomHeight(fromTableView tableView: UITableView) -> CGFloat {
        let indexPath = IndexPath(row: 0, section: 0)
        let numberOfRows = tableView.numberOfRows(inSection: indexPath.section)
        let cell = tableView.cellForRow(at: indexPath)
        return CGFloat(cell?.frame.size.height ?? 0) * CGFloat(numberOfRows - 1)
    }
    
    func getOptionsForCell(indexPath: IndexPath) -> SearchBottomSheetItem {
        payableOptions[indexPath.row]
    }
    
    func didTapButton(option: SearchBottomSheetItem) {
        switch option.action.type {
        case .favorite:
            performFavoritesCheck { [weak self] currentState  in
                self?.dependencies.analytics.log(
                    SearchBottomSheetEvent.socialStatusChangedFavorites(
                        currentState != .favorited,
                        oldStatus: currentState == .favorited
                    )
                )

                self?.favoritesInteractor.changeState(
                    currentState,
                    id: self?.itemId ?? "",
                    type: self?.favoriteType ?? .unknown
                ) { [weak self] state in
                    self?.updateFavoriteButtonState(state)
                    self?.presenter.listPayableOptions(
                        options: self?.payableOptions ?? [],
                        isDefault: true,
                        cellType: self?.cellType ?? .unknown,
                        cellId: self?.itemId ?? ""
                    )
                }
            }
            
        case .following:
            dependencies.dispatchGroup.enter()
            performFollowingCheck { [weak self] state in
                self?.checkStateForFollowingButtonAction(state: state)
            }
            
            dependencies.dispatchGroup.notify(queue: dependencies.mainQueue) { [weak self] in
                self?.presenter.listPayableOptions(
                    options: self?.payableOptions ?? [],
                    isDefault: true,
                    cellType: self?.cellType ?? .unknown,
                    cellId: self?.itemId ?? ""
                )
            }
            
        case .deeplink:
            dependencies.analytics.log(SearchBottomSheetEvent.bottomSheetItemClicked(option.action.data ?? ""))
            presenter.present(deepLinkAction: option.action.data ?? "")
            
        case .p2pCharge:
            dependencies.analytics.log(SearchBottomSheetEvent.bottomSheetItemClicked("cobrar"))
            presenter.perform(action: .presentPaymentRequestFlow)
            
        default:
            presenter.perform(action: .presentDefaultCellAction)
        }
    }
    
    func loadContactProfile(image: UIPPProfileImage, contact: PPContact) {
        presenter.perform(action: .presentContactProfile(image, contact))
        dependencies.analytics.log(SearchBottomSheetEvent.bottomSheetItemClicked("perfil"))
    }
    
    func loadStoreProfile(image: UIPPProfileImage, store: PPStore) {
        presenter.perform(action: .presentStoreProfile(image, store))
        dependencies.analytics.log(SearchBottomSheetEvent.bottomSheetItemClicked("perfil"))
    }
}
