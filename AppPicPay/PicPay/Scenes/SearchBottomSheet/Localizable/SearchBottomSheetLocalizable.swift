import Foundation

enum SearchBottomSheetLocalizable: String, Localizable {
    case p2mQrcode
    case cieloTitle
    case normalFavoriteButton
    case selectedFavoriteButton
    case selectedFollowingButton
    case normalFollowButton
    case selectedFollowButton
    case goToQrcodeButton
    case goToDetailsButton
    
    var key: String {
        self.rawValue
    }
    
    var file: LocalizableFile {
        .searchBottomSheetLocalizable
    }
}
