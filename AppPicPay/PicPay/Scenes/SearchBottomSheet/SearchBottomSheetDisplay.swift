import UI

protocol SearchBottomSheetDisplay: AnyObject {
    func updateInfo(withCellObject object: NSObject, andName name: String)
    func listOptions(_ options: [Section<String, SearchBottomSheetCellViewModel>], isDefault: Bool)
    func displayDeeplink(deeplink: String)
}
