import Core

enum SearchBottomSheetEndpoint {
    case options(_ type: SearchBottomSheetType, _ id: String)
}

extension SearchBottomSheetEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case let .options(type, id):
            return "payable-options/\(type.description)/\(id)"
        }
    }
    
    var isTokenNeeded: Bool {
        true
    }
    
    var method: HTTPMethod {
        .get
    }
}
