import Core
import UI

protocol SearchBottomSheetPresenting: AnyObject {
    var viewController: SearchBottomSheetDisplay? { get set }
    func updateInfo(withCellObject object: NSObject, andName name: String)
    func listPayableOptions(options: [SearchBottomSheetItem], isDefault: Bool, cellType: SearchResultType, cellId: String)
    func present(deepLinkAction deeplink: String)
    func perform(action: SearchBottomSheetAction)
}

extension SearchBottomSheetPresenting {
    func listPayableOptions(
        options: [SearchBottomSheetItem],
        isDefault: Bool,
        cellType: SearchResultType = .unknown,
        cellId: String = ""
    ) {
        listPayableOptions(options: options, isDefault: isDefault, cellType: cellType, cellId: cellId)
    }
}

final class SearchBottomSheetPresenter: SearchBottomSheetPresenting {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    private let coordinator: SearchBottomSheetCoordinating
    weak var viewController: SearchBottomSheetDisplay?

    init(coordinator: SearchBottomSheetCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    func updateInfo(withCellObject object: NSObject, andName name: String) {
        dependencies.mainQueue.async { [weak viewController] in
            viewController?.updateInfo(withCellObject: object, andName: name)
        }
    }
    
    func listPayableOptions(
        options: [SearchBottomSheetItem],
        isDefault: Bool,
        cellType: SearchResultType = .unknown,
        cellId: String = ""
    ) {
        let container = DependencyContainer()
        let presenter: SearchBottomSheetCellPresenting = SearchBottomSheetCellPresenter(dependencies: container)
        let favoriteService: FavoritesServicing = FavoritesService(dependencies: container)
        let interactor: FavoriteInteracting = FavoriteInteractor(service: favoriteService, dependencies: container)
        let options = options.map { option -> SearchBottomSheetCellViewModel in
            SearchBottomSheetCellViewModel(presenter: presenter,
                                           option: option,
                                           interactor: interactor,
                                           cellData: (cellType, cellId))
        }
        
        dependencies.mainQueue.async { [weak self] in
            self?.viewController?.listOptions([Section(items: options)], isDefault: isDefault)
        }
    }
    
    func present(deepLinkAction deeplink: String) {
        dependencies.mainQueue.async { [weak viewController] in
            viewController?.displayDeeplink(deeplink: deeplink)
        }
    }
    
    func perform(action: SearchBottomSheetAction) {
        dependencies.mainQueue.async { [weak self] in
            self?.coordinator.perform(action: action)
        }
    }
}
