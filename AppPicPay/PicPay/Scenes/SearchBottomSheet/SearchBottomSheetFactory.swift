import Foundation

enum SearchBottomSheetFactory {
    static func make(
        data: SearchResultBasics,
        object: NSObject,
        uippdelegate: UIPPProfileImageDelegate?,
        delegates: (
            bsDelegate: SearchBottomSheetDelegate,
            coordinatorDelegate: SearchBottomSheetCoordinatorDelegate
        ),
        tabId: String?
    ) -> SearchBottomSheetViewController {
        let container = DependencyContainer()
        let coordinator = SearchBottomSheetCoordinator()
        let service: SearchBottomSheetServicing = SearchBottomSheetService()
        let presenter: SearchBottomSheetPresenting = SearchBottomSheetPresenter(coordinator: coordinator, dependencies: container)
        let favoritesService = FavoritesService(dependencies: container)
        let favoritesInteractor = FavoriteInteractor(service: favoritesService, dependencies: container)
        
        let viewModel = SearchBottomSheetViewModel(
            service: service,
            presenter: presenter,
            data: data,
            object: object,
            interactor: favoritesInteractor,
            dependencies: container,
            tabId: tabId
        )
        let viewController = SearchBottomSheetViewController(viewModel: viewModel, profileImageDelegate: uippdelegate)
        
        viewController.delegate = delegates.bsDelegate
        coordinator.viewController = viewController
        coordinator.delegate = delegates.coordinatorDelegate
        presenter.viewController = viewController

        return viewController
    }
}
