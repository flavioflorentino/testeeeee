import Foundation

final class SearchBottomSheetItem: Decodable {
    let type: BottomSheetItemType
    let title: String?
    var action: SearchBottomSheetItemAction
    
    init(type: BottomSheetItemType, title: String, action: SearchBottomSheetItemAction) {
        self.type = type
        self.title = title
        self.action = action
    }
}

final class SearchBottomSheetItemAction: Decodable {
    let type: BottomSheetActionType
    let data: String?
    var isFavorite: Bool?
    var isFollowing: SearchBottomSheetFollowingState?

    init(type: BottomSheetActionType, data: String) {
        self.type = type
        self.data = data
        self.isFavorite = nil
    }
}
