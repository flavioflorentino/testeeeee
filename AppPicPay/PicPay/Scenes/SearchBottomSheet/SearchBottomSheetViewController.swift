import UI
import UIKit
import SkeletonView

private extension SearchBottomSheetViewController.Layout {
    enum Size {
        static let nameLabelWidth: CGFloat = 250
        static let lineViewHeight: CGFloat = 5
        static let lineViewWidth: CGFloat = 45
        static let initialViewHeight: CGFloat = 250
        static let cellHeight: CGFloat = 45
    }
}

protocol SearchBottomSheetDelegate: AnyObject {
    func handleDeeplink(deeplink: String)
}

final class SearchBottomSheetViewController: ViewController<SearchBottomSheetViewModelInputs, UIView> {
    fileprivate enum Layout { }
    weak var delegate: SearchBottomSheetDelegate?
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.grayscale300.color
        view.layer.cornerRadius = Layout.Size.lineViewHeight / 2
        
        return view
    }()
    
    private lazy var imageView: UIPPProfileImage = {
        let imageView = UIPPProfileImage()
        imageView.shouldHideBadgePro = true
        imageView.layer.cornerRadius = 36
        imageView.isUserInteractionEnabled = true
        imageView.clipsToBounds = true
        imageView.isSkeletonable = true
        
        return imageView
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.isSkeletonable = true
        label.linesCornerRadius = Int(Spacing.base01)
        label.clipsToBounds = true
        
        return label
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.isScrollEnabled = false
        tableView.backgroundColor = .clear
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.register(
            SearchBottomSheetTableViewCell.self,
            forCellReuseIdentifier: String(describing: SearchBottomSheetTableViewCell.self)
        )
        
        return tableView
    }()
    
    private var dataSource: TableViewHandler<String, SearchBottomSheetCellViewModel, SearchBottomSheetTableViewCell>?
    
    init(viewModel: SearchBottomSheetViewModelInputs, profileImageDelegate: UIPPProfileImageDelegate?) {
        super.init(viewModel: viewModel)
        imageView.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        loadData()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        view.layoutSkeletonIfNeeded()
    }

    override func buildViewHierarchy() {
        view.addSubviews(lineView, imageView, nameLabel, tableView)
    }

    override func setupConstraints() {
        lineView.layout {
            $0.top == view.topAnchor + Spacing.base01
            $0.centerX == view.centerXAnchor
            $0.height == Layout.Size.lineViewHeight
            $0.width == Layout.Size.lineViewWidth
        }

        imageView.layout {
            $0.top == lineView.bottomAnchor + Spacing.base02
            $0.centerX == view.centerXAnchor
            $0.width == Spacing.base09
            $0.height == Spacing.base09
        }

        nameLabel.layout {
            $0.top == imageView.bottomAnchor + Spacing.base01
            $0.centerX == view.centerXAnchor
            $0.width == Layout.Size.nameLabelWidth
            $0.height == Spacing.base03
        }

        tableView.layout {
            $0.top == nameLabel.bottomAnchor + Spacing.base02
            $0.leading == view.leadingAnchor + Spacing.base01
            $0.trailing == view.trailingAnchor - Spacing.base01
            $0.bottom == view.bottomAnchor - Spacing.base02
        }
    }
    
    override func configureViews() {
        preferredContentSize = CGSize(width: self.view.frame.width, height: Layout.Size.initialViewHeight)
    }
    
    override func configureStyles() {
        view.backgroundColor = Colors.backgroundSecondary.color
        view.applyRadiusTopCorners(radius: Spacing.base02)
    }
    
    private func loadData() {
        viewModel.loadPayableOptions()
        view.showAnimatedGradientSkeleton()
    }
    
    private func updateSheetFrame() {
        let customHeight = Layout.Size.initialViewHeight + self.viewModel.getCustomHeight(fromTableView: self.tableView)
        self.preferredContentSize = CGSize(width: self.view.frame.width, height: customHeight)
        UIView.animate(
            withDuration: 0.5,
            delay: 0,
            usingSpringWithDamping: 1,
            initialSpringVelocity: 0,
            options: .curveEaseOut,
            animations: {
                let origin = CGPoint(x: 0, y: UIScreen.main.bounds.height - self.preferredContentSize.height)
                self.view.frame = CGRect(origin: origin, size: self.preferredContentSize)
            }
        )
    }
}

extension SearchBottomSheetViewController: SearchBottomSheetDisplay {
    func displayDeeplink(deeplink: String) {
        dismiss(animated: true) { [weak self] in
            self?.delegate?.handleDeeplink(deeplink: deeplink)
        }
    }
    
    func listOptions(_ options: [Section<String, SearchBottomSheetCellViewModel>], isDefault: Bool) {
        dataSource = TableViewHandler(data: options, cellType: SearchBottomSheetTableViewCell.self, configureCell: { _, viewModel, cell in
            viewModel.presenter.viewController = cell
            cell.viewModel = viewModel
            cell.delegate = self
            cell.customInit()
        })
        
        tableView.dataSource = dataSource
        tableView.reloadData()

        view.hideSkeleton()
        
        isDefault ? nil : updateSheetFrame()

        if options.flatMap({ $0.items }).isEmpty {
            didFinishCellUpdate()
        }
    }
    
    func updateInfo(withCellObject object: NSObject, andName name: String) {
        imageView.setAny(object)
        nameLabel.text = name
    }
}

extension SearchBottomSheetViewController: SearchBottomSheetCellDelegate {
    func didTapButton(option: SearchBottomSheetItem) {
        viewModel.didTapButton(option: option)
    }
    
    func didFinishCellUpdate() {
        self.viewModel.updateSheetInfo()
        self.view.hideSkeleton()
    }
}

extension SearchBottomSheetViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        Layout.Size.cellHeight
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard
            viewModel.checkIfLastCellIsOnDisplay(indexPath: indexPath),
            let cell = cell as? SearchBottomSheetTableViewCell
            else {
                return
            }
        cell.isLastCell = true
    }
}

extension SearchBottomSheetViewController: UIPPProfileImageDelegate {
    func profileImageViewDidTap(_ profileImage: UIPPProfileImage, contact: PPContact) {
        viewModel.loadContactProfile(image: profileImage, contact: contact)
    }
    
    func profileImageViewDidTap(_ profileImage: UIPPProfileImage, store: PPStore) {
        viewModel.loadStoreProfile(image: profileImage, store: store)
    }
}
