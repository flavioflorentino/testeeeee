import AnalyticsModule

enum SearchBottomSheetEvent: AnalyticsKeyProtocol {
    case bottomSheetAccessed(_ type: Favorite.`Type`, _ itemName: String, _ origin: String)
    case bottomSheetItemClicked(_ type: String)
    case socialStatusChangedFavorites(_ status: Bool, oldStatus: Bool)
    case socialStatusChangedFollowing(_ status: SearchBottomSheetFollowingState, oldStatus: SearchBottomSheetFollowingState)
    
    private var name: String {
        switch self {
        case .bottomSheetAccessed:
            return "Bottom Sheet Accessed"
        case .bottomSheetItemClicked:
            return "Bottom Sheet Item Clicked"
        case .socialStatusChangedFollowing, .socialStatusChangedFavorites:
            return "Social Status Changed"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .bottomSheetAccessed(type, itemName, origin):
            return [
                "type": type.rawValue,
                "item_name": itemName,
                "origin": origin
            ]
        case let .bottomSheetItemClicked(type):
            return [ "type": type ]

        case let .socialStatusChangedFollowing(status, oldStatus):
            return [
                "follow_status_changed": status.rawValue,
                "last_follow_status": oldStatus.rawValue
            ]
        case let .socialStatusChangedFavorites(status, oldStatus):
            return [
                "favorites_status_changed": status,
                "last_favorites_status": oldStatus
            ]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}
