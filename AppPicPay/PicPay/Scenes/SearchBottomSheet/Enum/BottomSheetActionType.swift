import Core
import UI

enum BottomSheetActionType: String, Decodable {
    case consumer
    case favorite
    case following
    case deeplink
    case p2m
    case p2pCharge = "p2p_charge"
    case qrcode
    case financialService = "financial_service"
}

extension BottomSheetActionType {
    var images: (normal: UIImage?, selected: UIImage?) {
        switch self {
        case .favorite:
            return (
                normal: Assets.Search.BottomSheet.favoriteOutline.image,
                selected: Assets.Search.BottomSheet.favoriteFill.image
            )
        case .following:
            return (
                normal: Assets.Search.BottomSheet.followingOutline.image,
                selected: Assets.Search.BottomSheet.waitingOutline.image
            )
        default:
            return (
                normal: Assets.LinkedAccounts.rightArrowlightGray.image,
                selected: Assets.LinkedAccounts.rightArrowlightGray.image
            )
        }
    }
    
    var titles: (normal: String, selected: String) {
        switch self {
        case .favorite:
            return (
                normal: SearchBottomSheetLocalizable.normalFavoriteButton.text,
                selected: SearchBottomSheetLocalizable.selectedFavoriteButton.text
            )
        case .following:
            return (
                normal: SearchBottomSheetLocalizable.normalFollowButton.text,
                selected: SearchBottomSheetLocalizable.selectedFollowButton.text
            )
        case .qrcode:
            return (
                normal: SearchBottomSheetLocalizable.goToQrcodeButton.text,
                selected: SearchBottomSheetLocalizable.goToQrcodeButton.text
            )
        default:
            return (
                normal: SearchBottomSheetLocalizable.goToDetailsButton.text,
                selected: SearchBottomSheetLocalizable.goToDetailsButton.text
            )
        }
    }
}
