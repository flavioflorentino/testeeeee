import Foundation

enum SearchBottomSheetFollowingState: String, Decodable {
    case notFollowing = "not_following"
    case following
    case waiting
}
