import Foundation

enum SearchBottomSheetType: String, CustomStringConvertible {
    case seller = "store"
    case consumer
    case digitalGood = "digital_good"
    
    var description: String {
        switch self {
        case .digitalGood:
            return "DIGITAL_GOOD"
        case .seller:
            return "SELLER"
        default:
            return self.rawValue.uppercased()
        }
    }
}
