import Foundation

enum BottomSheetItemType: String, Decodable {
    case activable
    case navigation
    case generic
}
