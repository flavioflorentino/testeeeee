import Core
import UI

protocol SearchBottomSheetCellPresenting: AnyObject {
    var viewController: SearchBottomSheetCellDisplay? { get set }
    func presentCellInfo(
        withTitles titles: (normal: String, selected: String),
        andIcons iconImages: (normal: UIImage?, selected: UIImage?)
    )
}

final class SearchBottomSheetCellPresenter: SearchBottomSheetCellPresenting {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    weak var viewController: SearchBottomSheetCellDisplay?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func presentCellInfo(
        withTitles titles: (normal: String, selected: String),
        andIcons iconImages: (normal: UIImage?, selected: UIImage?)
    ) {
        dependencies.mainQueue.async { [weak viewController] in
            viewController?.displayCellInfo(withTitles: titles, andIcons: iconImages)
        }
    }
}
