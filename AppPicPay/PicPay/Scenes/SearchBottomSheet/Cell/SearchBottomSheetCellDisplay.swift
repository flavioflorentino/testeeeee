import UI

public protocol SearchBottomSheetCellDisplay: AnyObject {
    func displayCellInfo(
        withTitles titles: (normal: String, selected: String),
        andIcons iconImages: (normal: UIImage?, selected: UIImage?)
    )
}
