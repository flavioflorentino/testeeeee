import Core

protocol SearchBottomSheetCellViewModelInputs: AnyObject {
    func updateCellInfo()
}

final class SearchBottomSheetCellViewModel {
    private(set) var presenter: SearchBottomSheetCellPresenting
    private let option: SearchBottomSheetItem
    private let favoritesInteractor: FavoriteInteracting
    private var cellId: String
    private var cellType: SearchResultType
    
    private var titlesFromOption: (String, String)? {
        if let title = option.title, !title.isEmpty {
            return (normal: title, selected: title)
        }
        return nil
    }
    
    var optionActionType: SearchBottomSheetItemAction {
        option.action
    }
    
    var isFavorite: Bool {
        option.action.isFavorite ?? false
    }
    
    var followerState: SearchBottomSheetFollowingState? {
        option.action.isFollowing
    }

    var cellOptionObject: SearchBottomSheetItem {
        option
    }

    init(
        presenter: SearchBottomSheetCellPresenting,
        option: SearchBottomSheetItem,
        interactor: FavoriteInteracting,
        cellData: (cellType: SearchResultType, cellId: String)
    ) {
        self.presenter = presenter
        self.option = option
        self.favoritesInteractor = interactor
        self.cellType = cellData.cellType
        self.cellId = cellData.cellId
    }
}

extension SearchBottomSheetCellViewModel: SearchBottomSheetCellViewModelInputs {
    func updateCellInfo() {
        var titles = (normal: "", selected: "")
        guard
            let images = BottomSheetActionType(rawValue: option.action.type.rawValue)?.images,
            let customTitles = BottomSheetActionType(rawValue: option.action.type.rawValue)?.titles
            else {
                return
        }
        titles = customTitles
        
        if let originalTitle = titlesFromOption {
            titles = originalTitle
        }
        presenter.presentCellInfo(withTitles: titles, andIcons: images)
    }
}
