import UI

protocol SearchBottomSheetCellDelegate: AnyObject {
    func didFinishCellUpdate()
    func didTapButton(option: SearchBottomSheetItem)
}

final class SearchBottomSheetTableViewCell: UITableViewCell, ViewConfiguration {
    weak var delegate: SearchBottomSheetCellDelegate?
    
    private lazy var containerView = UIView()
    
    private lazy var optionButton: SearchBottomSheetButton = {
        let button = SearchBottomSheetButton()
        button.setTitleColor(Colors.black.color, for: .normal)
        button.addTarget(self, action: #selector(didTapButton(_:)), for: .touchUpInside)
        
        return button
    }()
    
    var viewModel: SearchBottomSheetCellViewModel?
    var isLastCell: Bool = false {
        didSet {
            optionButton.viewNeedsBottomLine = !isLastCell
        }
    }
    
    func customInit() {
        viewModel?.updateCellInfo()
        buildLayout()
    }
    
    func buildViewHierarchy() {
        contentView.addSubview(containerView)
        containerView.addSubview(optionButton)
    }
    
    func setupConstraints() {
        containerView.layout {
            $0.top == contentView.topAnchor
            $0.leading == contentView.leadingAnchor + Spacing.base01
            $0.trailing == contentView.trailingAnchor - Spacing.base01
            $0.bottom == contentView.bottomAnchor
        }
        
        optionButton.layout {
            $0.top == containerView.topAnchor
            $0.leading == containerView.leadingAnchor
            $0.trailing == containerView.trailingAnchor
            $0.bottom == containerView.bottomAnchor
        }
    }
    
    func configureViews() { 
        backgroundColor = .clear
        
        switch viewModel?.optionActionType.type {
        case .favorite:
            optionButton.isSelected = viewModel?.isFavorite ?? false
        case .following:
            guard let state = viewModel?.followerState else {
                return
            }
            optionButton.isSelected = (state == .notFollowing) ? false : true
        default:
            break
        }
    }
    
    @objc
    func didTapButton(_ sender: UIButton) {
        sender.isSelected.toggle()
        guard let option = viewModel?.cellOptionObject else {
            return
        }
        delegate?.didTapButton(option: option)
    }
    
    private func checkStateToDisplayCellInfo(defaultTitles: (String, String), defaultImages: (UIImage?, UIImage?)) {
        guard
            let state = viewModel?.followerState,
            state == .following
            else {
                optionButton.setup(withTitles: defaultTitles, andIcons: defaultImages)
                return
        }
        optionButton.setup(
            withTitles: (
                normal: SearchBottomSheetLocalizable.normalFollowButton.text,
                selected: SearchBottomSheetLocalizable.selectedFollowingButton.text
            ),
            andIcons: (
                normal: Assets.Search.BottomSheet.followingOutline.image,
                selected: Assets.Search.BottomSheet.followingFill.image
            )
        )
    }
}

extension SearchBottomSheetTableViewCell: SearchBottomSheetCellDisplay {
    func displayCellInfo(withTitles titles: (normal: String, selected: String), andIcons iconImages: (normal: UIImage?, selected: UIImage?)) {
        if case .following = viewModel?.optionActionType.type {
            checkStateToDisplayCellInfo(defaultTitles: titles, defaultImages: iconImages)
        } else {
            optionButton.setup(withTitles: titles, andIcons: iconImages)
        }
        delegate?.didFinishCellUpdate()
    }
}
