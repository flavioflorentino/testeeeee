import Foundation
import UIKit

enum SearchBottomSheetAction {
    case presentPaymentRequestFlow
    case presentContactProfile(_ image: UIPPProfileImage, _ contact: PPContact)
    case presentStoreProfile(_ image: UIPPProfileImage, _ contact: PPStore)
    case presentDefaultCellAction
}

protocol SearchBottomSheetCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: SearchBottomSheetAction)
}

protocol SearchBottomSheetCoordinatorDelegate: AnyObject {
    func presentPaymentRequestValueFlow()
    func displayContactProfile(_ image: UIPPProfileImage, _ contact: PPContact)
    func displayStoreProfile(_ image: UIPPProfileImage, _ store: PPStore)
    func displayDefaultCellAction()
}

final class SearchBottomSheetCoordinator: SearchBottomSheetCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: SearchBottomSheetCoordinatorDelegate?
    
    func perform(action: SearchBottomSheetAction) {
        switch action {
        case .presentPaymentRequestFlow:
            viewController?.dismiss(animated: true) { [weak self] in
                self?.delegate?.presentPaymentRequestValueFlow()
            }
        case let .presentContactProfile(image, contact):
            viewController?.dismiss(animated: true) { [weak self] in
                self?.delegate?.displayContactProfile(image, contact)
            }
        case let .presentStoreProfile(image, contact):
            viewController?.dismiss(animated: true) { [weak self] in
                self?.delegate?.displayStoreProfile(image, contact)
            }
        case .presentDefaultCellAction:
            viewController?.dismiss(animated: true) { [weak self] in
                self?.delegate?.displayDefaultCellAction()
            }
        }
    }
}
