import Foundation

protocol TFACreditCardViewModelInputs: AnyObject {
    func loadCards()
    func selectedItem(_ indexPath: IndexPath)
}

final class TFACreditCardViewModel {
    private let service: TFACreditCardServicing
    private let presenter: TFACreditCardPresenting
    private let tfaRequestData: TFARequestData
    private var cardList: [TFACreditCardData] = []
    
    init(
        service: TFACreditCardServicing,
        presenter: TFACreditCardPresenting,
        tfaRequestData: TFARequestData
    ) {
        self.service = service
        self.presenter = presenter
        self.tfaRequestData = tfaRequestData
    }
}

extension TFACreditCardViewModel: TFACreditCardViewModelInputs {
    func loadCards() {
        presenter.presentLoading(true)
        
        service.loadTFACard(with: tfaRequestData) { [weak self] result in
            self?.presenter.presentLoading(false)
            
            switch result {
            case let .success(cardList):
                self?.verifyCards(cardList.creditCard)
            case let .failure(error):
                self?.presenter.presentCardLoadError(error)
            }
        }
    }
    
    func selectedItem(_ indexPath: IndexPath) {
        let selectedCard = cardList[indexPath.row]
        presenter.didNextStep(action: .validateSelectedCard(tfaCard: selectedCard))
    }
    
    private func verifyCards(_ cards: [TFACreditCardData]) {
        cardList = cards
        guard
            cards.count == 1,
            let card = cards.first
            else {
                presenter.presentCardList(cards)
                return
        }
        
        presenter.didNextStep(action: .validateSelectedCard(tfaCard: card))
    }
}
