import Core
import Foundation
import UI

protocol TFACreditCardPresenting: AnyObject {
    var viewController: TFACreditCardDisplay? { get set }
    func didNextStep(action: TFACreditCardAction)
    func presentLoading(_ loading: Bool)
    func presentCardList(_ cardList: [TFACreditCardData])
    func presentCardLoadError(_ error: ApiError)
}

final class TFACreditCardPresenter: TFACreditCardPresenting {
    private let coordinator: TFACreditCardCoordinating
    weak var viewController: TFACreditCardDisplay?
    
    init(coordinator: TFACreditCardCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: TFACreditCardAction) {
        coordinator.perform(action: action)
    }
    
    func presentLoading(_ loading: Bool) {
        viewController?.displayLoading(loading)
    }
    
    func presentCardList(_ cardList: [TFACreditCardData]) {
        let items = cardList.map { item -> TFACardCellViewModel in
            let presenter: TFACardCellPresenting = TFACardCellPresenter()
            return TFACardCellViewModel(presenter: presenter, item: item)
        }
        
        viewController?.displayCards([Section(items: items)])
    }
    
    func presentCardLoadError(_ error: ApiError) {
        let tryAgainButton = Button(title: DefaultLocalizable.tryAgain.text, type: .cta) { popup, _ in
            popup.dismiss(animated: true) {
                self.viewController?.displayRetryLoadCardList()
            }
        }
        
        let anotherAuthorizationButton = Button(
            title: TFALocalizable.tfaAnotherAuthorizationWay.text,
            type: .cleanUnderlined) { popup, _ in
                popup.dismiss(animated: true) {
                    self.coordinator.perform(action: .tryAnotherAuthorizationMethod)
                }
        }
        
        let alert = Alert(
            with: TFALocalizable.tfaCardListNotLoadedAlertTitle.text,
            text: TFALocalizable.tfaCardListNotLoadedAlertMessage.text,
            buttons: [tryAgainButton, anotherAuthorizationButton],
            image: nil
        )
        
        viewController?.displayCardListLoadErrorAlert(alert)
    }
}
