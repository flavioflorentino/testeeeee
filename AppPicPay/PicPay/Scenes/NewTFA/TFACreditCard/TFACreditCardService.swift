import Core
import Foundation

protocol TFACreditCardServicing {
    func loadTFACard(with data: TFARequestData, completion: @escaping (Result<TFACreditCardList, ApiError>) -> Void)
}

final class TFACreditCardService: TFACreditCardServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func loadTFACard(with data: TFARequestData, completion: @escaping (Result<TFACreditCardList, ApiError>) -> Void) {
        let api = Api<TFACreditCardList>(endpoint: DynamicTFAEndpoint.getCreditCards(tfaRequestData: data))
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result
                    .map({ $0.model })
                    .mapError({ $0 })
                completion(mappedResult)
            }
        }
    }
}
