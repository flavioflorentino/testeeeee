import UI
import UIKit

protocol TFACreditCardDisplay: AnyObject {
    func displayLoading(_ loading: Bool)
    func displayCards(_ cards: [Section<String, TFACardCellViewModel>])
    func displayCardListLoadErrorAlert(_ alert: Alert)
    func displayRetryLoadCardList()
}
