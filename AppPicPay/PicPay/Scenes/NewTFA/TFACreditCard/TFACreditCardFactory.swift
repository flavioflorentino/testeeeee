import Foundation

enum TFACreditCardFactory {
    static func make(with data: TFARequestData, coordinatorDelegate: TFACreditCardCoordinatorDelegate) -> TFACreditCardViewController {
        let container = DependencyContainer()
        let service: TFACreditCardServicing = TFACreditCardService(dependencies: container)
        let coordinator: TFACreditCardCoordinating = TFACreditCardCoordinator()
        let presenter: TFACreditCardPresenting = TFACreditCardPresenter(coordinator: coordinator)
        
        let viewModel = TFACreditCardViewModel(
            service: service,
            presenter: presenter,
            tfaRequestData: data
        )
        
        let viewController = TFACreditCardViewController(viewModel: viewModel)
        coordinator.delegate = coordinatorDelegate
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
