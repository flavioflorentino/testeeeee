import UI
import UIKit

private extension TFACreditCardViewController.Layout {
    enum Fonts {
        static let titleLabelFont = UIFont.boldSystemFont(ofSize: 28)
        static let descriptionLabelFont = UIFont.systemFont(ofSize: 16)
    }
    
    enum Spacing {
        static let defaultOffset: CGFloat = 20
    }
    
    enum Height {
        static let cellEstimatedRowHeight: CGFloat = 80
    }
}

final class TFACreditCardViewController: ViewController<TFACreditCardViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
    lazy var loadingView = LoadingView()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = TFALocalizable.tfaCreditCardTitle.text
        label.textColor = Palette.ppColorGrayscale600.color
        label.font = Layout.Fonts.titleLabelFont
        label.numberOfLines = 0
        label.textAlignment = .left
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.descriptionLabelFont
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = 0
        label.textAlignment = .left
        label.text = TFALocalizable.tfaCreditCardDescription.text
        return label
    }()
    
    private lazy var tableView: UITableView = {
        let table = UITableView()
        table.backgroundColor = Palette.ppColorGrayscale000.color
        table.separatorStyle = .none
        table.showsVerticalScrollIndicator = false
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = Layout.Height.cellEstimatedRowHeight
        table.register(TFACardCell.self, forCellReuseIdentifier: String(describing: TFACardCell.self))
        return table
    }()
    
    private var dataSource: TableViewHandler<String, TFACardCellViewModel, TFACardCell>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadCards()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(tableView)
    }
    
    override func configureViews() {
        navigationItem.backBarButtonItem = .noContextTitlelessBackButton
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override func setupConstraints() {
        titleLabel.layout {
            $0.top == view.topAnchor + Layout.Spacing.defaultOffset
            $0.leading == view.leadingAnchor + Layout.Spacing.defaultOffset
            $0.trailing == view.trailingAnchor - Layout.Spacing.defaultOffset
            $0.bottom == descriptionLabel.topAnchor - Layout.Spacing.defaultOffset
        }
        
        descriptionLabel.layout {
            $0.leading == view.leadingAnchor + Layout.Spacing.defaultOffset
            $0.trailing == view.trailingAnchor - Layout.Spacing.defaultOffset
            $0.bottom == tableView.topAnchor - Layout.Spacing.defaultOffset
        }
        
        tableView.layout {
            $0.leading == view.leadingAnchor + Layout.Spacing.defaultOffset
            $0.trailing == view.trailingAnchor - Layout.Spacing.defaultOffset
            $0.bottom == view.bottomAnchor
        }
    }
}

// MARK: View Model Outputs
extension TFACreditCardViewController: TFACreditCardDisplay {
    func displayLoading(_ loading: Bool) {
        loading ? self.startLoadingView() : self.stopLoadingView()
    }
    
    func displayCards(_ cards: [Section<String, TFACardCellViewModel>]) {
        self.dataSource = TableViewHandler(
            data: cards,
            cellType: TFACardCell.self,
            configureCell: { _, cellViewModel, cell in
                cellViewModel.presenter.viewController = cell
                cell.viewModel = cellViewModel
            }, configureDidSelectRow: { indexPath, _ in
                self.viewModel.selectedItem(indexPath)
            }
        )
        
        self.tableView.dataSource = self.dataSource
        self.tableView.delegate = self.dataSource
        self.tableView.reloadData()
    }
    
    func displayCardListLoadErrorAlert(_ alert: Alert) {
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func displayRetryLoadCardList() {
        viewModel.loadCards()
    }
}

extension TFACreditCardViewController: LoadingViewProtocol { }
