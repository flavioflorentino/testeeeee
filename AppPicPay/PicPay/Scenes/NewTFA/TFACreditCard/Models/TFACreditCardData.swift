public struct TFACreditCardData: Decodable {
    let id: Int
    let flagName: String
    let lastFour: String
    let urlFlagLogoImage: String
}

public struct TFACreditCardList: Decodable {
    let creditCard: [TFACreditCardData]
}
