public protocol TFACardCellDisplay: AnyObject {
    func configureView(imageUrl: String, lastDigits: String)
}
