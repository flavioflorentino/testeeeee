import UI
import UIKit

private extension TFACardCell.Layout {
    enum Fonts {
        static let titleLabelFont = UIFont.systemFont(ofSize: 16, weight: .semibold)
    }
    
    enum Spacing {
        static let imageViewVerticalMargin: CGFloat = 10
        static let imageViewHorizontalMargin: CGFloat = 8
        static let titleLabelTrailingMargin: CGFloat = 8
    }
    
    enum Width {
        static let imageViewWidth: CGFloat = 52
        static let disclosureIndicatorWidth: CGFloat = 20
    }
    
    enum Height {
        static let imageViewHeight: CGFloat = 52
    }
    
    enum Other {
        static let shadowOpacity: Float = 0.05
    }
}

final class TFACardCell: UITableViewCell {
    fileprivate enum Layout { }
    
    var viewModel: TFACardCellViewModel? {
        didSet {
            update()
        }
    }
    
    private lazy var mainBackground: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale000.color
        view.layer.cornerRadius = CornerRadius.light
        return view
    }()
    
    private lazy var cardFlagImageView: UIImageView = {
        let imageView = UIImageView(image: Assets.TwoFA.tfaCardPlaceholder.image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var tfaTitleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.titleLabelFont
        label.textColor = Palette.ppColorGrayscale600.color
        label.numberOfLines = 0
        label.textAlignment = .left
        return label
    }()
    
    private lazy var tfaDisclosureIndicatorView: UIImageView = {
        let imageView = UIImageView(image: Assets.Icons.rightArrow.image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addComponents()
        layoutComponents()
        selectionStyle = .none
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        mainBackground.layer.setShadow(
            radius: .light,
            offset: .medium,
            color: .ppColorGrayscale600,
            opacity: Layout.Other.shadowOpacity
        )
    }
    
    private func addComponents() {
        addSubview(mainBackground)
        mainBackground.addSubview(cardFlagImageView)
        mainBackground.addSubview(tfaTitleLabel)
        mainBackground.addSubview(tfaDisclosureIndicatorView)
    }
    
    private func layoutComponents() {
        mainBackground.layout {
            $0.top == topAnchor
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.bottom == bottomAnchor - Spacing.base00
        }
        
        cardFlagImageView.layout {
            $0.top == mainBackground.topAnchor + Layout.Spacing.imageViewVerticalMargin
            $0.leading == mainBackground.leadingAnchor + Layout.Spacing.imageViewHorizontalMargin
            $0.trailing == tfaTitleLabel.leadingAnchor - Layout.Spacing.imageViewHorizontalMargin
            $0.bottom == mainBackground.bottomAnchor - Layout.Spacing.imageViewVerticalMargin
            $0.width == Layout.Width.imageViewWidth
            $0.height == Layout.Height.imageViewHeight
        }
        
        tfaTitleLabel.layout {
            $0.centerY == cardFlagImageView.centerYAnchor
            $0.trailing == tfaDisclosureIndicatorView.leadingAnchor - Layout.Spacing.titleLabelTrailingMargin
        }
        
        tfaDisclosureIndicatorView.layout {
            $0.centerY == tfaTitleLabel.centerYAnchor
            $0.width == Layout.Width.disclosureIndicatorWidth
            $0.trailing == mainBackground.trailingAnchor
        }
    }
    
    private func update() {
        viewModel?.configure()
    }
}

extension TFACardCell: TFACardCellDisplay {
    func configureView(imageUrl: String, lastDigits: String) {
        tfaTitleLabel.text = "\(TFALocalizable.tfaCreditCardOptionTitle.text)\(lastDigits)"
        cardFlagImageView.setImage(url: URL(string: imageUrl), placeholder: Assets.TwoFA.tfaCardPlaceholder.image)
    }
}
