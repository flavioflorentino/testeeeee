import UIKit

public protocol TFACardCellPresenting: AnyObject {
    var viewController: TFACardCellDisplay? { get set }
    func configureView(item: TFACreditCardData)
}

public final class TFACardCellPresenter: TFACardCellPresenting {
    public weak var viewController: TFACardCellDisplay?
    
    public func configureView(item: TFACreditCardData) {
        viewController?.configureView(imageUrl: item.urlFlagLogoImage, lastDigits: item.lastFour)
    }
}
