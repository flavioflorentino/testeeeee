import Foundation

protocol TFACardCellViewModelInputs: AnyObject {
    func configure()
}

final class TFACardCellViewModel {
    private let item: TFACreditCardData
    private(set) var presenter: TFACardCellPresenting
    
    init(presenter: TFACardCellPresenting, item: TFACreditCardData) {
        self.presenter = presenter
        self.item = item
    }
}

extension TFACardCellViewModel: TFACardCellViewModelInputs {
    func configure() {
        presenter.configureView(item: item)
    }
}
