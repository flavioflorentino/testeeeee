import UIKit

enum TFACreditCardAction {
    case tryAnotherAuthorizationMethod
    case validateSelectedCard(tfaCard: TFACreditCardData)
}

protocol TFACreditCardCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: TFACreditCardCoordinatorDelegate? { get set }
    func perform(action: TFACreditCardAction)
}

protocol TFACreditCardCoordinatorDelegate: AnyObject {
    func didTryAnotherAuthorizationMethod()
    func didTryValidateTfaCard(with card: TFACreditCardData)
}

final class TFACreditCardCoordinator: TFACreditCardCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: TFACreditCardCoordinatorDelegate?

    func perform(action: TFACreditCardAction) {
        switch action {
        case .tryAnotherAuthorizationMethod:
            delegate?.didTryAnotherAuthorizationMethod()
        case let .validateSelectedCard(tfaCard):
            delegate?.didTryValidateTfaCard(with: tfaCard)
        }
    }
}
