import Foundation

enum TFALocalizable: String, Localizable {
    case infoTitle
    case infoDescription
    case onboardingTitle
    case onboardingDescription
    case onboardingInfoButtonTitle
    case optionsTitle
    case optionsDescription
    case optionsOldDevice
    case optionsSMS
    case optionsInfoButtonMain
    case optionsInfoButtonSubStr
    
    // Phone Verification
    case yourPhone
    case completeNumberDescription
    case genericNumberDescription
    case phoneNumber
    case phoneNumberMask
    case invalidPhoneAlertTitle
    case invalidPhoneAlertMessage
    
    // Generic
    case genericBackText
    case attempLimitAlertTitle
    case attempLimitAlertMessage
    case genericErrorTitle
    case genericErrorMessage
    case continueAuthorization
    case cancelAuthorization
    case exitAuhorizationAlertTitle
    case exitAuhorizationAlertMessage
    
    // TFA Options
    case newOptionsTitleText
    case newOptionsDescriptionText
    case fallbackOptionDefatulText
    case tfaOptionsListNotLoadedAlertTitle
    case tfaOptionsListNotLoadedAlertMessage
    case tfaOptionsFallbackButton
    
    // TFACode
    case tfaCodeSmsTitle
    case tfaCodeEmailTitle
    case tfaCodeTitleText
    case tfaCodeTitleTextForEmail
    case resendEmailButton
    case resendSMSButton
    case resendNotificationButton
    case resendButtonText
    case oldDeviceNotificationSent
    case smsNotificationSentGeneric
    case emailNotificationSentGeneric
    case smsNotificationWithMaskPhone
    case emailNotificationWithMaskEmail
    case didNotReceivedCode
    case authorizationCodePlaceholder
    case authorizationSuccessTitle
    case authorizationSuccessMessage
    case authorizationEnterButton
    
    // TFACreditCard
    case tfaCreditCardTitle
    case tfaCreditCardDescription
    case tfaCreditCardOptionTitle
    case tfaAnotherAuthorizationWay
    case tfaCardListNotLoadedAlertTitle
    case tfaCardListNotLoadedAlertMessage
    
    // TFACreditCardValidate
    case tfaCreditCardValidateTitle
    case tfaCreditCardValidateFirstDigitsPlaceholder
    case tfaCreditCardValidateDueDatePlaceholder
    case tfaCreditCardValidateFirstDigitsFieldMask
    case tfaCreditCardValidateDueDateFieldMask
    case tfaCreditCardValidateCardNumberRequiredError
    case tfaCreditCardValidateInvalidDueDateError
    
    case createNewPasswordLegacy
    case createNewPasswordPicpay
    case authorizedDevice
    case deviceHasBeenAuthorized
    case didNotReceive
    case weSentNotification
    case accessThePicpayApp
    case getTheCodeSent
    case weSentAnEmail
    
    // TFAStatus
    case approvedStatusTitle
    case deniedStatusTitle
    case inconclusiveStatusTitle
    case waitingAnalysisStatusTitle
    
    case approvedStatusDescription
    case deniedStatusDescription
    case inconclusiveStatusDescription
    case waitingAnalysisStatusDescription
    
    case understood
    case inconclusiveStatusAction
    case doItLater
    
    var key: String {
        self.rawValue
    }
    
    var file: LocalizableFile {
        .tfa
    }
}
