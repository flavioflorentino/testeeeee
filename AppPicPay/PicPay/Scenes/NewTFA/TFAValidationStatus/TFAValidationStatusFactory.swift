import Foundation

enum TFAValidationStatusFactory {
    static func make(
        withStatus status: TFAStatus,
        coordinatorDelegate: TFAValidationStatusCoordinatorDelegate
    ) -> TFAValidationStatusViewController {
        let container = DependencyContainer()
        let coordinator: TFAValidationStatusCoordinating = TFAValidationStatusCoordinator()
        let presenter: TFAValidationStatusPresenting = TFAValidationStatusPresenter(coordinator: coordinator)
        let viewModel = TFAValidationStatusViewModel(presenter: presenter, status: status, dependencies: container)
        
        let viewController = TFAValidationStatusViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        presenter.viewController = viewController

        return viewController
    }
}
