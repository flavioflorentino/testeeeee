import AnalyticsModule
import Foundation
import IdentityValidation

protocol TFAValidationStatusViewModelInputs: AnyObject {
    func configureViews()
    func handleButtonAction()
    func doItLaterAction()
}

final class TFAValidationStatusViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let presenter: TFAValidationStatusPresenting
    private let tfaStatus: TFAStatus

    init(presenter: TFAValidationStatusPresenting, status: TFAStatus, dependencies: Dependencies) {
        self.presenter = presenter
        self.tfaStatus = status
        self.dependencies = dependencies
    }
}

// MARK: - TFAValidationStatusViewModelInputs
extension TFAValidationStatusViewModel: TFAValidationStatusViewModelInputs {
    func configureViews() {
        dependencies.analytics.log(TFAEvents.didReceivedIdentityStatus(tfaStatus))
        presenter.presentStatus(tfaStatus, showSecondaryButton: tfaStatus == .inconclusive)
    }
    
    func handleButtonAction() {
        presenter.handleActionForStatus(tfaStatus)
    }
    
    func doItLaterAction() {
        presenter.doItLater()
    }
}
