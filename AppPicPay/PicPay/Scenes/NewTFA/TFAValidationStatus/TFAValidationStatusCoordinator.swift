import UIKit

enum TFAValidationStatusAction {
    case enter
    case showOptions
    case understood
}

protocol TFAValidationStatusCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: TFAValidationStatusCoordinatorDelegate? { get set }
    func perform(action: TFAValidationStatusAction)
}

protocol TFAValidationStatusCoordinatorDelegate: AnyObject {
    func didReceivedApprovedStatus()
    func closeTFAFlow()
    func presentTFAOptions()
}

final class TFAValidationStatusCoordinator {
    weak var viewController: UIViewController?
    weak var delegate: TFAValidationStatusCoordinatorDelegate?
}

// MARK: - TFAValidationStatusCoordinating
extension TFAValidationStatusCoordinator: TFAValidationStatusCoordinating {
    func perform(action: TFAValidationStatusAction) {
        switch action {
        case .enter:
            delegate?.didReceivedApprovedStatus()
        case .showOptions:
            delegate?.presentTFAOptions()
        case .understood:
            delegate?.closeTFAFlow()
        }
    }
}
