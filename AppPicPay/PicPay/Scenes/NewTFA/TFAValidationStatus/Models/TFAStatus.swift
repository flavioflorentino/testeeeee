import AssetsKit
import Foundation

enum TFAStatus {
    case approved
    case denied
    case inconclusive
    case waitingAnalysis
    
    var title: String {
        switch self {
        case .approved:
            return TFALocalizable.approvedStatusTitle.text
        case .denied:
            return TFALocalizable.deniedStatusTitle.text
        case .inconclusive:
            return TFALocalizable.inconclusiveStatusTitle.text
        case .waitingAnalysis:
            return TFALocalizable.waitingAnalysisStatusTitle.text
        }
    }
    
    var statusDescription: String {
        switch self {
        case .approved:
            return TFALocalizable.approvedStatusDescription.text
        case .denied:
            return TFALocalizable.deniedStatusDescription.text
        case .inconclusive:
            return TFALocalizable.inconclusiveStatusDescription.text
        case .waitingAnalysis:
            return TFALocalizable.waitingAnalysisStatusDescription.text
        }
    }
    
    var statusImage: UIImage {
        switch self {
        case .approved:
            return Assets.TwoFA.iconApproved.image
        case .denied:
            return Resources.Illustrations.iluRegisterDenied.image
        case .inconclusive:
            return Assets.TwoFA.iconInconclusive.image
        case .waitingAnalysis:
            return Resources.Illustrations.iluClock.image
        }
    }
    
    var actionTitle: String {
        switch self {
        case .approved:
            return TFALocalizable.authorizationEnterButton.text
        case .denied, .waitingAnalysis:
            return TFALocalizable.understood.text
        case .inconclusive:
            return TFALocalizable.inconclusiveStatusAction.text
        }
    }
}
