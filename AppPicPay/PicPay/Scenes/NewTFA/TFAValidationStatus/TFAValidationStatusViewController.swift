import UI

protocol TFAValidationStatusDisplay: AnyObject {
    func displayStatus(_ status: TFAStatus, showSecondaryButton: Bool)
}

private extension TFAValidationStatusViewController.Layout {
    enum Size {
        static let imageNormal: CGFloat = 176
        static let imageSmall: CGFloat = 124
    }
    
    enum Height {
        static let smallScreenHeight: CGFloat = 568
    }
}

final class TFAValidationStatusViewController: ViewController<TFAValidationStatusViewModelInputs, UIView> {
    fileprivate enum Layout { }
    private let isSmallScreenDevice = UIScreen.main.bounds.height <= Layout.Height.smallScreenHeight
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, .black())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        return label
    }()
    
    private lazy var actionButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(DefaultLocalizable.btOkUnderstood.text, for: .normal)
        button.addTarget(self, action: #selector(didTapOk), for: .touchUpInside)
        return button
    }()
    
    private lazy var doItLaterButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(TFALocalizable.doItLater.text, for: .normal)
        button.addTarget(self, action: #selector(didTapDoItLater), for: .touchUpInside)
        button.buttonStyle(LinkButtonStyle())
        button.isHidden = true
        return button
    }()
    
    private lazy var centerStackView: UIStackView = {
        let stack = UIStackView()
        stack.alignment = .center
        stack.axis = .vertical
        stack.distribution = .fill
        stack.spacing = Spacing.base03
        return stack
    }()
    
    private lazy var textStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = Spacing.base03
        return stack
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.configureViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(centerStackView)
        centerStackView.addArrangedSubview(iconImageView)
        centerStackView.addArrangedSubview(textStackView)
        textStackView.addArrangedSubview(titleLabel)
        textStackView.addArrangedSubview(descriptionLabel)
        centerStackView.addArrangedSubview(actionButton)
        centerStackView.addArrangedSubview(doItLaterButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.change(.dark, to: Colors.backgroundSecondary.color).color
    }
    
    override func setupConstraints() {
        centerStackView.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.centerY.equalToSuperview()
        }
        
        textStackView.snp.makeConstraints {
            $0.width.equalToSuperview()
        }
        
        actionButton.snp.makeConstraints {
            $0.leading.equalToSuperview()
        }
        
        iconImageView.snp.makeConstraints {
            $0.size.equalTo(isSmallScreenDevice ? Layout.Size.imageSmall : Layout.Size.imageNormal)
        }
    }
}

@objc
private extension TFAValidationStatusViewController {
    func didTapOk() {
        viewModel.handleButtonAction()
    }
    
    func didTapDoItLater() {
        viewModel.doItLaterAction()
    }
}

// MARK: TFAValidationStatusDisplay
extension TFAValidationStatusViewController: TFAValidationStatusDisplay {
    func displayStatus(_ status: TFAStatus, showSecondaryButton: Bool) {
        iconImageView.image = status.statusImage
        titleLabel.text = status.title
        descriptionLabel.text = status.statusDescription
        actionButton.setTitle(status.actionTitle, for: .normal)
        doItLaterButton.isHidden = !showSecondaryButton
    }
}
