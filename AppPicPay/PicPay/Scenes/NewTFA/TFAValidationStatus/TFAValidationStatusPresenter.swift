import Foundation

protocol TFAValidationStatusPresenting: AnyObject {
    var viewController: TFAValidationStatusDisplay? { get set }
    func presentStatus(_ status: TFAStatus, showSecondaryButton: Bool)
    func handleActionForStatus(_ status: TFAStatus)
    func doItLater()
}

final class TFAValidationStatusPresenter: TFAValidationStatusPresenting {
    private let coordinator: TFAValidationStatusCoordinating
    weak var viewController: TFAValidationStatusDisplay?

    init(coordinator: TFAValidationStatusCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - TFAValidationStatusPresenting
extension TFAValidationStatusPresenter {
    func presentStatus(_ status: TFAStatus, showSecondaryButton: Bool) {
        viewController?.displayStatus(status, showSecondaryButton: showSecondaryButton)
    }
    
    func handleActionForStatus(_ status: TFAStatus) {
        switch status {
        case .approved:
            coordinator.perform(action: .enter)
        case .inconclusive:
            coordinator.perform(action: .showOptions)
        case .denied, .waitingAnalysis:
            coordinator.perform(action: .understood)
        }
    }
    
    func doItLater() {
        coordinator.perform(action: .understood)
    }
}
