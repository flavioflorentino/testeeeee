import UI
import UIKit

private extension TFASelectedCardView.Layout {
    enum Fonts {
        static let bankNameFont = UIFont.systemFont(ofSize: 14)
    }
}

final class TFASelectedCardView: UIView {
    fileprivate enum Layout { }
    
    var viewModel: TFASelectedCardViewViewModel? {
        didSet {
            update()
        }
    }

    private lazy var cardImage = UIImageView(image: Assets.TwoFA.tfaCardPlaceholder.image)

    private lazy var cardName: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.bankNameFont
        label.textColor = Palette.ppColorGrayscale600.color
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: .zero)
        addComponents()
        layoutComponents()
        setup()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func addComponents() {
        addSubview(cardImage)
        addSubview(cardName)
    }

    private func layoutComponents() {
        cardImage.layout {
            $0.leading == leadingAnchor + Spacing.base02
            $0.top == topAnchor + Spacing.base02
            $0.bottom == bottomAnchor - Spacing.base02
            $0.trailing == cardName.leadingAnchor - Spacing.base01
            $0.width == Spacing.base04
            $0.height == Spacing.base04
        }

        cardName.layout {
            $0.centerY == cardImage.centerYAnchor
            $0.trailing == trailingAnchor - Spacing.base01
        }
    }

    private func setup() {
        backgroundColor = Palette.ppColorGrayscale100.color.withCustomDarkColor(#colorLiteral(red: 0.2235294118, green: 0.2745098039, blue: 0.3019607843, alpha: 1))
        layer.cornerRadius = CornerRadius.medium
    }
    
    private func update() {
        viewModel?.configure()
    }
}

extension TFASelectedCardView: TFASelectedCardViewDisplay {
    func configureView(with logoUrl: URL?, cardTitle: String, placeholderImage: UIImage) {
        cardName.text = cardTitle
        cardImage.setImage(url: logoUrl, placeholder: placeholderImage)
    }
}
