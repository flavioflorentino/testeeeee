import UIKit

public protocol TFASelectedCardViewDisplay: AnyObject {
    func configureView(with logoUrl: URL?, cardTitle: String, placeholderImage: UIImage)
}
