public protocol TFASelectedCardViewViewModelInputs: AnyObject {
    func configure()
}

final class TFASelectedCardViewViewModel {
    private let card: TFACreditCardData
    private(set) var presenter: TFASelectedCardViewPresenting
    
    init(presenter: TFASelectedCardViewPresenting, card: TFACreditCardData) {
        self.card = card
        self.presenter = presenter
    }
}

extension TFASelectedCardViewViewModel: TFASelectedCardViewViewModelInputs {
    func configure() {
        presenter.configureView(with: card)
    }
}
