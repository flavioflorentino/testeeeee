import UIKit

public protocol TFASelectedCardViewPresenting: AnyObject {
    var viewController: TFASelectedCardViewDisplay? { get set }
    func configureView(with card: TFACreditCardData)
}

public final class TFASelectedCardViewPresenter: TFASelectedCardViewPresenting {
    public var viewController: TFASelectedCardViewDisplay?
    
    public func configureView(with card: TFACreditCardData) {
        viewController?.configureView(
            with: URL(string: card.urlFlagLogoImage),
            cardTitle: "\(TFALocalizable.tfaCreditCardOptionTitle.text) \(card.lastFour)",
            placeholderImage: Assets.TwoFA.tfaCardPlaceholder.image
        )
    }
}
