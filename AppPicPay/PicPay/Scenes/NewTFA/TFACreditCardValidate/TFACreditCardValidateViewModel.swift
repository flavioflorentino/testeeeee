import AnalyticsModule
import Core
import Validator

protocol TFACreditCardValidateViewModelInputs: AnyObject {
    func configureView()
    func checkCardNumberFieldWith(mask: String?, text: String, validationResult: ValidationResult)
    func checkDueDateFieldWith(mask: String?, text: String, validationResult: ValidationResult)
    func validateCreditCardWith(cardNumber: String, dueDate: String)
    func checkFieldsAndEnableContinue(cardNumberValidationResult: ValidationResult, dueDateValidationResult: ValidationResult)
    func startTimerForNewValidation(retryTime: Int)
}

final class TFACreditCardValidateViewModel {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private let service: TFACreditCardValidateServicing
    private let presenter: TFACreditCardValidatePresenting
    private let selectedCard: TFACreditCardData
    private let tfaRequestData: TFARequestData
    
    private var timer: RepeatingTimer?
    private var endDate = Date(timeInterval: 180, since: Date())
    private let maxDigits = 6
    private let carNumberValidationMinimumDigits = 17
    
    init(
        service: TFACreditCardValidateServicing,
        presenter: TFACreditCardValidatePresenting,
        selectedCard: TFACreditCardData,
        tfaRequestData: TFARequestData,
        dependencies: Dependencies,
        timer: RepeatingTimer? = RepeatingTimer(timeInterval: 1)
    ) {
        self.service = service
        self.presenter = presenter
        self.selectedCard = selectedCard
        self.tfaRequestData = tfaRequestData
        self.timer = timer
        self.dependencies = dependencies
    }
}

extension TFACreditCardValidateViewModel: TFACreditCardValidateViewModelInputs {
    func configureView() {
        buildTextfieldSettings()
        presenter.presentCardInfo(selectedCard)
    }
    
    func checkCardNumberFieldWith(mask: String?, text: String, validationResult: ValidationResult) {
        presenter.presentCardNumberFieldMaskedText(
            withMask: mask,
            text: text,
            errorMessage: validationErrorMessageFor(result: validationResult)
        )
    }
    
    func checkDueDateFieldWith(mask: String?, text: String, validationResult: ValidationResult) {
        presenter.presentDueDateFieldMaskedText(
            withMask: mask,
            text: text,
            errorMessage: validationErrorMessageFor(result: validationResult)
        )
    }
    
    func checkFieldsAndEnableContinue(cardNumberValidationResult: ValidationResult, dueDateValidationResult: ValidationResult) {
        let enable = cardNumberValidationResult.isValid && dueDateValidationResult.isValid
        presenter.shouldEnableContinue(enable)
    }
    
    func validateCreditCardWith(cardNumber: String, dueDate: String) {
        let creditCardValidateData = TFACreditCardValidateData(
            id: selectedCard.id,
            firstDigits: formatCardNumber(cardNumber),
            dueData: dueDate
        )
        
        presenter.presentLoading(true)
        service.validatePersonalData(
            data: creditCardValidateData,
            tfaRequestData: tfaRequestData) { [weak self] result in
                self?.presenter.presentLoading(false)
                switch result {
                case let .success(response):
                    Analytics.shared.log(TFAEvents.didValidateCard(.creditCard))
                    self?.presenter.didNextStep(action: .validateCard(nextStep: response.nextStep))
                case let .failure(apiError):
                    self?.parseError(apiError)
                }
        }
    }
    
    func startTimerForNewValidation(retryTime: Int) {
        runCountDown(retryTime)
    }
    
    private func parseError(_ apiError: ApiError) {
        switch apiError {
        case .tooManyRequests(let requestError),
             .unauthorized(let requestError),
             .badRequest(let requestError):
            verifyRequestError(requestError)
        default:
            presenter.presentGenericErrorAlert(genericErrorAlert())
        }
    }
    
    private func verifyRequestError(_ requestError: RequestError) {
        guard
            let errorData = requestError.data?.dictionaryObject?["data"] as? [String: Any],
            let retryTime = errorData["retry_time"] as? Int
            else {
                presenter.presentValidationErrorAlert(validationErrorAlert(error: requestError))
                return
            }
        
        presenter.presentAttemptLimitErrorAlert(attemptLimitErrorAlert(error: requestError), retryTime: retryTime)
    }
    
    private func genericErrorAlert() -> Alert {
        let alert = Alert(
            with: TFALocalizable.genericErrorTitle.text,
            text: TFALocalizable.genericErrorMessage.text,
            buttons: [
                Button(title: DefaultLocalizable.back.text, type: .cta)
            ]
        )
        
        return alert
    }
    
    private func validationErrorAlert(error: RequestError) -> Alert {
        let alert = Alert(title: error.title, text: error.message)
        alert.buttons = [Button(title: DefaultLocalizable.back.text, type: .cta)]
        return alert
    }
    
    private func attemptLimitErrorAlert(error: RequestError) -> Alert {
        Analytics.shared.log(TFAEvents.didReceivedExceededDataValidationLimitError(tfaType: .creditCard, currentStep: .creditCard))
        
        let alert = Alert(title: error.title, text: error.message)
        alert.buttons = [Button(title: DefaultLocalizable.back.text, type: .cta)]
        return alert
    }
    
    private func formatCardNumber(_ cardNumber: String) -> String {
        let cardNumberWithoutSpaces = cardNumber.replacingOccurrences(of: " ", with: "")
        let digitsCount = cardNumberWithoutSpaces.count
        return digitsCount <= maxDigits ? cardNumberWithoutSpaces : cardNumberWithoutSpaces.substring(from: 0, to: maxDigits)
    }
    
    private func buildTextfieldSettings() {
        let settings = TFACreditCardValidateTextFieldSettings(
            cardNumber: buildTextfieldSettingCardNumber(),
            dueDate: buildTextfieldSettingDueDate()
        )
        
        presenter.presentTextFieldSettings(settings)
    }
    
    private func buildTextfieldSettingCardNumber() -> TextfieldSetting {
        var cardNumberRules = ValidationRuleSet<String>()
        let minimumLengthRule = ValidationRuleLength(
            min: carNumberValidationMinimumDigits,
            lengthType: .characters,
            error: TFACreditCardFieldCheckValidationError.cardNumberRequired
        )
        
        cardNumberRules.add(rule: minimumLengthRule)
        let placeholder = TFALocalizable.tfaCreditCardValidateFirstDigitsPlaceholder.text
        let maskString = TFALocalizable.tfaCreditCardValidateFirstDigitsFieldMask.text
        
        return TextfieldSetting(placeholder: placeholder, text: nil, rule: cardNumberRules, mask: maskString)
    }
    
    private func buildTextfieldSettingDueDate() -> TextfieldSetting {
        var rule = ValidationRuleSet<String>()
        rule.add(rule: ValidationRuleLength(min: 1, error: TFACreditCardFieldCheckValidationError.invalidDuedate))
        rule.add(rule: ValidationRuleLength(min: 5, error: TFACreditCardFieldCheckValidationError.invalidDuedate))
        
        let placeholder = TFALocalizable.tfaCreditCardValidateDueDatePlaceholder.text
        let mask = TFALocalizable.tfaCreditCardValidateDueDateFieldMask.text
        return TextfieldSetting(placeholder: placeholder, text: nil, rule: rule, mask: mask)
    }
    
    private func validationErrorMessageFor(result: ValidationResult) -> String? {
        var message: String?
        if case let .invalid(error) = result,
            let err = error.first {
            message = String(describing: err)
        }
        
        return message
    }
    
    private func runCountDown(_ retryTime: Int) {
        if timer == nil {
            timer = RepeatingTimer(timeInterval: 1)
        }
        
        timer?.eventHandler = { [weak self] in
            self?.updateTimer()
        }
        
        endDate = Date(timeInterval: TimeInterval(retryTime), since: Date())
        timer?.resume()
    }
    
    private func updateTimer() {
        dependencies.mainQueue.async { [weak self] in
            guard let self = self else {
                return
            }
            
            let endTime = self.endDate.timeIntervalSince(Date()).rounded(.toNearestOrEven)
            self.presenter.updateContinueButtonRemainingTime(endTime)
            
            if endTime <= 0 {
                self.stopTimer()
                self.presenter.shouldEnableContinue(true)
                return
            }
        }
    }
    
    private func stopTimer() {
        timer?.eventHandler = nil
        timer?.suspend()
        timer = nil
    }
}
