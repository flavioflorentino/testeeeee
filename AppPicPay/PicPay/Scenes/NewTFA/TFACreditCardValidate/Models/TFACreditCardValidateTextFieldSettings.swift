import Foundation
import Validator

struct TFACreditCardValidateTextFieldSettings {
    let cardNumber: TextfieldSetting
    let dueDate: TextfieldSetting
}

enum TFACreditCardFieldCheckValidationError: String, Error, ValidationError {
    case cardNumberRequired
    case invalidDuedate

    var message: String {
        switch self {
        case .cardNumberRequired:
            return TFALocalizable.tfaCreditCardValidateCardNumberRequiredError.text
        case .invalidDuedate:
            return TFALocalizable.tfaCreditCardValidateInvalidDueDateError.text
        }
    }
}

extension TFACreditCardFieldCheckValidationError: CustomStringConvertible {
    var description: String {
        message
    }
}
