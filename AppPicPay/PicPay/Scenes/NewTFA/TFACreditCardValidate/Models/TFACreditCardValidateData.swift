public struct TFACreditCardValidateData {
    let id: Int
    let firstDigits: String
    let dueData: String
}
