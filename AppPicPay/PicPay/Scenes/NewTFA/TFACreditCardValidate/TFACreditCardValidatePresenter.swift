import Core
import UI
import Validator

protocol TFACreditCardValidatePresenting: AnyObject {
    var viewController: TFACreditCardValidateDisplay? { get set }
    func didNextStep(action: TFACreditCardValidateAction)
    func presentCardInfo(_ card: TFACreditCardData)
    func presentCardNumberFieldMaskedText(withMask mask: String?, text: String, errorMessage: String?)
    func presentDueDateFieldMaskedText(withMask mask: String?, text: String, errorMessage: String?)
    func presentTextFieldSettings(_ settings: TFACreditCardValidateTextFieldSettings)
    func shouldEnableContinue(_ enable: Bool)
    func presentLoading(_ loading: Bool)
    func presentGenericErrorAlert(_ alert: Alert)
    func presentValidationErrorAlert(_ alert: Alert)
    func presentAttemptLimitErrorAlert(_ alert: Alert, retryTime: Int)
    func updateContinueButtonRemainingTime(_ remainingTime: TimeInterval)
}

final class TFACreditCardValidatePresenter: TFACreditCardValidatePresenting {
    private let coordinator: TFACreditCardValidateCoordinating
    weak var viewController: TFACreditCardValidateDisplay?
    
    init(coordinator: TFACreditCardValidateCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: TFACreditCardValidateAction) {
        coordinator.perform(action: action)
    }
    
    func presentCardInfo(_ card: TFACreditCardData) {
        viewController?.displayCardInfo(with: card)
    }
    
    func presentTextFieldSettings(_ settings: TFACreditCardValidateTextFieldSettings) {
        viewController?.displayTextFieldSettings(settings)
    }
    
    func presentCardNumberFieldMaskedText(withMask mask: String?, text: String, errorMessage: String?) {
        viewController?.displayCardNumberFieldMaskedTextWith(
            generateMaskedText(with: mask, andText: text),
            errorMessage: errorMessage
        )
    }
    
    func presentDueDateFieldMaskedText(withMask mask: String?, text: String, errorMessage: String?) {
        viewController?.displayDueDateFieldMaskedText(
            generateMaskedText(with: mask, andText: text),
            errorMessage: errorMessage
        )
    }
    
    func shouldEnableContinue(_ enable: Bool) {
        viewController?.displayContinueButtonEnabled(enable)
    }
    
    func presentLoading(_ loading: Bool) {
        viewController?.displayLoading(loading)
    }
    
    func presentGenericErrorAlert(_ alert: Alert) {
        viewController?.displayGenericErrorAlert(alert)
    }
    
    func presentValidationErrorAlert(_ alert: Alert) {
        viewController?.displayValidationErrorAlert(alert)
    }
    
    func presentAttemptLimitErrorAlert(_ alert: Alert, retryTime: Int) {
        viewController?.displayAttemptLimitErrorAlert(alert, retryTime: retryTime)
    }
    
    func updateContinueButtonRemainingTime(_ remainingTime: TimeInterval) {
        let buttonText = "\(DefaultLocalizable.wait.text) \(self.timeString(time: Int(remainingTime)))"
        viewController?.displayContinueButton(with: buttonText)
    }
    
    private func generateMaskedText(with maskString: String?, andText text: String) -> String {
        guard let maskText = maskString else {
            return text
        }
        
        let customMask = CustomStringMask(mask: maskText)
        guard let maskedString = customMask.maskedText(from: text) else {
            return text
        }
        
        return maskedString
    }
    
    private func timeString(time: Int) -> String {
        let minutes = (time / 60) % 60
        let seconds = time % 60
        return String(format: "%02i:%02i", minutes, seconds)
    }
}
