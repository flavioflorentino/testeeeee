import Foundation

enum TFACreditCardValidateFactory {
    static func make(
        with selectedCard: TFACreditCardData,
        tfaRequestData: TFARequestData,
        coordinatorDelegate: TFACreditCardValidateCoordinatorDelegate
    ) -> TFACreditCardValidateViewController {
        let containerDependency = DependencyContainer()
        let service: TFACreditCardValidateServicing = TFACreditCardValidateService(dependencies: containerDependency)
        let coordinator: TFACreditCardValidateCoordinating = TFACreditCardValidateCoordinator()
        let presenter: TFACreditCardValidatePresenting = TFACreditCardValidatePresenter(coordinator: coordinator)
        
        let viewModel = TFACreditCardValidateViewModel(
            service: service,
            presenter: presenter,
            selectedCard: selectedCard,
            tfaRequestData: tfaRequestData,
            dependencies: containerDependency
        )
        
        let viewController = TFACreditCardValidateViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        presenter.viewController = viewController

        return viewController
    }
}
