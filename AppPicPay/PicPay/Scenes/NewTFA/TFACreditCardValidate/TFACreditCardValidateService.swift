import Core

protocol TFACreditCardValidateServicing {
    func validatePersonalData(
        data: TFACreditCardValidateData,
        tfaRequestData: TFARequestData,
        completion: @escaping (Result<TFAValidationResponse, ApiError>) -> Void
    )
}

final class TFACreditCardValidateService: TFACreditCardValidateServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func validatePersonalData(
        data: TFACreditCardValidateData,
        tfaRequestData: TFARequestData,
        completion: @escaping (Result<TFAValidationResponse, ApiError>) -> Void
    ) {
        let api = Api<TFAValidationResponse>(
            endpoint: DynamicTFAEndpoint.validateCreditCard(cardData: data, tfaRequestData: tfaRequestData)
        )
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result
                    .map({ $0.model })
                    .mapError({ $0 })
                completion(mappedResult)
            }
        }
    }
}
