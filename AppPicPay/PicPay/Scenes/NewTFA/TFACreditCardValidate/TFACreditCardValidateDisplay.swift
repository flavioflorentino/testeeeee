import UIKit

protocol TFACreditCardValidateDisplay: AnyObject {
    func displayCardInfo(with card: TFACreditCardData)
    func displayCardNumberFieldMaskedTextWith(_ text: String, errorMessage: String?)
    func displayDueDateFieldMaskedText(_ text: String, errorMessage: String?)
    func displayContinueButtonEnabled(_ enabled: Bool)
    func displayTextFieldSettings(_ settings: TFACreditCardValidateTextFieldSettings)
    func displayLoading(_ loading: Bool)
    func displayGenericErrorAlert(_ alert: Alert)
    func displayValidationErrorAlert(_ alert: Alert)
    func displayAttemptLimitErrorAlert(_ alert: Alert, retryTime: Int)
    func displayContinueButton(with currentRemainingTime: String)
}
