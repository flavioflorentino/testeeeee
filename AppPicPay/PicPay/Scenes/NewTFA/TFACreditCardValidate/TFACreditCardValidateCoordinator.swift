import UIKit

enum TFACreditCardValidateAction {
    case validateCard(nextStep: TFAStep)
}

protocol TFACreditCardValidateCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: TFACreditCardValidateCoordinatorDelegate? { get set }
    func perform(action: TFACreditCardValidateAction)
}

protocol TFACreditCardValidateCoordinatorDelegate: AnyObject {
    func didValidateCard(nextStep: TFAStep)
}

final class TFACreditCardValidateCoordinator: TFACreditCardValidateCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: TFACreditCardValidateCoordinatorDelegate?
    
    func perform(action: TFACreditCardValidateAction) {
        if case let .validateCard(nextStep) = action {
            delegate?.didValidateCard(nextStep: nextStep)
        }
    }
}
