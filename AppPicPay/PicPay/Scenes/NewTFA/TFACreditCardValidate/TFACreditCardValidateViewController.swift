import UI
import UIKit

private extension TFACreditCardValidateViewController.Layout {
    enum Fonts {
        static let titleLabelFont = UIFont.boldSystemFont(ofSize: 28)
    }
    
    enum Spacing {
        static let defaultOffset: CGFloat = UI.Spacing.base02
    }
}

final class TFACreditCardValidateViewController: ViewController<TFACreditCardValidateViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
    private lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.backgroundColor = .clear
        return scroll
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = TFALocalizable.tfaCreditCardValidateTitle.text
        label.textColor = Palette.ppColorGrayscale600.color
        label.font = Layout.Fonts.titleLabelFont
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var selectedCardView = TFASelectedCardView()
    
    private lazy var cardNumberTextfield: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.defaultForm()
        textfield.addTarget(self, action: #selector(checkFirstDigitsTextField(_:)), for: .editingChanged)
        textfield.keyboardType = .numberPad
        return textfield
    }()
    
    private lazy var dueDateTextfield: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.defaultForm()
        textfield.addTarget(self, action: #selector(checkDueDateTextField(_:)), for: .editingChanged)
        textfield.keyboardType = .numberPad
        return textfield
    }()
    
    private lazy var continueButton: UIPPButton = {
        let button = UIPPButton()
        button.addTarget(self, action: #selector(didTapContinue(_:)), for: .touchUpInside)
        button.normalBackgrounColor = Palette.ppColorBranding300.color
        button.normalTitleColor = Palette.ppColorGrayscale000.color(withCustomDark: .ppColorGrayscale200)
        button.disabledTitleColor = Palette.ppColorGrayscale000.color
        button.disabledBackgrounColor = Palette.ppColorGrayscale400.color
        button.setTitle(DefaultLocalizable.next.text, for: .normal)
        button.setTitle(DefaultLocalizable.next.text, for: .disabled)
        button.cornerRadius = Spacing.base06 / 2
        button.isEnabled = false
        return button
    }()
    
    private lazy var keyboardScrollViewHandler = KeyboardScrollViewHandler(scrollView: scrollView)
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        keyboardScrollViewHandler.registerForKeyboardNotifications()
        viewModel.configureView()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(selectedCardView)
        contentView.addSubview(cardNumberTextfield)
        contentView.addSubview(dueDateTextfield)
        contentView.addSubview(continueButton)
    }
    
    override func configureViews() {
        navigationItem.backBarButtonItem = .noContextTitlelessBackButton
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override func setupConstraints() {
        scrollView.layout {
            $0.top == view.topAnchor
            $0.bottom == view.bottomAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
        }
        
        contentView.layout {
            $0.top == scrollView.topAnchor
            $0.bottom == scrollView.bottomAnchor
            $0.leading == scrollView.leadingAnchor
            $0.trailing == scrollView.trailingAnchor
            $0.width == view.widthAnchor
        }
        
        titleLabel.layout {
            $0.top == contentView.topAnchor + Spacing.base02
            $0.bottom == selectedCardView.topAnchor - Spacing.base02
            $0.leading == contentView.leadingAnchor + Layout.Spacing.defaultOffset
            $0.trailing == contentView.trailingAnchor - Layout.Spacing.defaultOffset
        }
        
        selectedCardView.layout {
            $0.bottom == cardNumberTextfield.topAnchor - Spacing.base03
            $0.leading == contentView.leadingAnchor + Layout.Spacing.defaultOffset
            $0.trailing == contentView.trailingAnchor - Layout.Spacing.defaultOffset
        }
        
        cardNumberTextfield.layout {
            $0.leading == contentView.leadingAnchor + Layout.Spacing.defaultOffset
            $0.trailing == contentView.trailingAnchor - Layout.Spacing.defaultOffset
        }
        
        dueDateTextfield.layout {
            $0.top == cardNumberTextfield.bottomAnchor + Spacing.base04
            $0.bottom == continueButton.topAnchor - Spacing.base03
            $0.leading == contentView.leadingAnchor + Layout.Spacing.defaultOffset
            $0.trailing == contentView.trailingAnchor - Layout.Spacing.defaultOffset
        }
        
        continueButton.layout {
            $0.bottom == contentView.bottomAnchor - Layout.Spacing.defaultOffset
            $0.height == Spacing.base06
            $0.leading == contentView.leadingAnchor + Layout.Spacing.defaultOffset
            $0.trailing == contentView.trailingAnchor - Layout.Spacing.defaultOffset
        }
    }
    
    @objc
    private func checkFirstDigitsTextField(_ sender: UIPPFloatingTextField) {
        viewModel.checkCardNumberFieldWith(mask: sender.maskString, text: sender.text ?? "", validationResult: sender.validate())
        checkValidState()
    }
    
    @objc
    private func checkDueDateTextField(_ sender: UIPPFloatingTextField) {
        viewModel.checkDueDateFieldWith(mask: sender.maskString, text: sender.text ?? "", validationResult: sender.validate())
        checkValidState()
    }
    
    @objc
    private func didTapContinue(_ sender: UIButton) {
        view?.endEditing(true)
        viewModel.validateCreditCardWith(cardNumber: cardNumberTextfield.text ?? "", dueDate: dueDateTextfield.text ?? "")
    }
    
    private func checkValidState() {
        viewModel.checkFieldsAndEnableContinue(
            cardNumberValidationResult: cardNumberTextfield.validate(),
            dueDateValidationResult: dueDateTextfield.validate()
        )
    }
}

// MARK: View Model Outputs
extension TFACreditCardValidateViewController: TFACreditCardValidateDisplay {
    func displayCardInfo(with card: TFACreditCardData) {
        let cardViewModel = TFASelectedCardViewViewModel(presenter: TFASelectedCardViewPresenter(), card: card)
        cardViewModel.presenter.viewController = selectedCardView
        selectedCardView.viewModel = cardViewModel
    }
    
    func displayTextFieldSettings(_ settings: TFACreditCardValidateTextFieldSettings) {
        cardNumberTextfield.placeholder = settings.cardNumber.placeholder
        cardNumberTextfield.maskString = settings.cardNumber.mask
        cardNumberTextfield.validationRules = settings.cardNumber.rule
        
        dueDateTextfield.placeholder = settings.dueDate.placeholder
        dueDateTextfield.maskString = settings.dueDate.mask
        dueDateTextfield.validationRules = settings.dueDate.rule
    }
    
    func displayCardNumberFieldMaskedTextWith(_ text: String, errorMessage: String?) {
        cardNumberTextfield.text = text
        cardNumberTextfield.errorMessage = errorMessage
    }
    
    func displayDueDateFieldMaskedText(_ text: String, errorMessage: String?) {
        dueDateTextfield.text = text
        dueDateTextfield.errorMessage = errorMessage
    }
    
    func displayContinueButtonEnabled(_ enabled: Bool) {
        continueButton.isEnabled = enabled
        continueButton.setTitle(DefaultLocalizable.next.text, for: .normal)
        continueButton.setTitle(DefaultLocalizable.next.text, for: .disabled)
    }
    
    func displayLoading(_ loading: Bool) {
        loading ? continueButton.startLoadingAnimating() : continueButton.stopLoadingAnimating()
    }
    
    func displayGenericErrorAlert(_ alert: Alert) {
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func displayValidationErrorAlert(_ alert: Alert) {
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func displayAttemptLimitErrorAlert(_ alert: Alert, retryTime: Int) {
        AlertMessage.showAlert(alert, controller: self)
        viewModel.startTimerForNewValidation(retryTime: retryTime)
    }
    
    func displayContinueButton(with currentRemainingTime: String) {
        continueButton.isEnabled = false
        continueButton.setTitle(currentRemainingTime, for: .disabled)
    }
}
