import Core
import Foundation

protocol TFASuccessPresenting: AnyObject {
    func didNextStep(action: TFASuccessAction)
}

final class TFASuccessPresenter: TFASuccessPresenting {
    private let coordinator: TFASuccessCoordinating

    init(coordinator: TFASuccessCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: TFASuccessAction) {
        coordinator.perform(action: action)
    }
}
