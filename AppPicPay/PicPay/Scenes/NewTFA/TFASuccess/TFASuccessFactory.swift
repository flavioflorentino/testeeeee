import Foundation

enum TFASuccessFactory {
    static func make(with coordinatorDelegate: TFASuccessCoordinatorDelegate) -> TFASuccessViewController {
        let coordinator: TFASuccessCoordinating = TFASuccessCoordinator()
        let presenter: TFASuccessPresenting = TFASuccessPresenter(coordinator: coordinator)
        let viewModel = TFASuccessViewModel(presenter: presenter)
        let viewController = TFASuccessViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate

        return viewController
    }
}
