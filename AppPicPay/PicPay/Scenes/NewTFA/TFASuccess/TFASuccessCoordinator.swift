import UIKit

enum TFASuccessAction {
    case enter
}

protocol TFASuccessCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: TFASuccessCoordinatorDelegate? { get set }
    func perform(action: TFASuccessAction)
}

protocol TFASuccessCoordinatorDelegate: AnyObject {
    func didEnter()
}

final class TFASuccessCoordinator: TFASuccessCoordinating {
    weak var delegate: TFASuccessCoordinatorDelegate?
    weak var viewController: UIViewController?
    
    func perform(action: TFASuccessAction) {
        if case .enter = action {
            delegate?.didEnter()
        }
    }
}
