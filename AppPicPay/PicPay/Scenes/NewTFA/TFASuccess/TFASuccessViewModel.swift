import Foundation

protocol TFASuccessViewModelInputs: AnyObject {
    func enter()
}

final class TFASuccessViewModel {
    private let presenter: TFASuccessPresenting

    init(presenter: TFASuccessPresenting) {
        self.presenter = presenter
    }
}

extension TFASuccessViewModel: TFASuccessViewModelInputs {
    func enter() {
        presenter.didNextStep(action: .enter)
    }
}
