import UI
import UIKit

private extension TFASuccessViewController.Layout {
    enum Fonts {
        static let titleFont = UIFont.boldSystemFont(ofSize: 28)
        static let descriptionFont = UIFont.systemFont(ofSize: 16)
        static let enterButtonFont = UIFont.boldSystemFont(ofSize: 15)
    }
    
    enum Spacing {
        static let defaultSpacing: CGFloat = 20
    }
    
    enum Width {
        static let successImageWidth: CGFloat = 128
    }
    
    enum Height {
        static let enterButtonHeight: CGFloat = 44
        static let successImageHeight: CGFloat = 96
    }
}

final class TFASuccessViewController: ViewController<TFASuccessViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
    private lazy var centerStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .center
        stack.distribution = .fill
        stack.spacing = Layout.Spacing.defaultSpacing
        return stack
    }()
    
    private lazy var successImage = UIImageView(image: #imageLiteral(resourceName: "ilu_2fa_onboarding"))
    
    private lazy var titleTextLabel: UILabel = {
        let label = UILabel()
        label.text = TFALocalizable.authorizationSuccessTitle.text
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = Palette.ppColorGrayscale600.color
        label.font = Layout.Fonts.titleFont
        return label
    }()
    
    private lazy var descriptionTextLabel: UILabel = {
        let label = UILabel()
        label.text = TFALocalizable.authorizationSuccessMessage.text
        label.textColor = Palette.ppColorGrayscale500.color
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = Layout.Fonts.descriptionFont
        return label
    }()
    
    private lazy var enterButton: UIPPButton = {
        let button = UIPPButton()
        button.addTarget(self, action: #selector(enterButtonTapped(_:)), for: .touchUpInside)
        button.normalBackgrounColor = Palette.ppColorBranding300.color
        button.normalTitleColor = Palette.ppColorGrayscale000.color(withCustomDark: .ppColorGrayscale200)
        button.setTitle(TFALocalizable.authorizationEnterButton.text, for: .normal)
        button.titleLabel?.font = Layout.Fonts.enterButtonFont
        button.cornerRadius = Layout.Height.enterButtonHeight / 2
        return button
    }()
    
    override func buildViewHierarchy() {
        view.addSubview(centerStackView)
        centerStackView.addArrangedSubview(successImage)
        centerStackView.addArrangedSubview(titleTextLabel)
        centerStackView.addArrangedSubview(descriptionTextLabel)
        centerStackView.addArrangedSubview(enterButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override func setupConstraints() {
        centerStackView.layout {
            $0.leading == view.leadingAnchor + Layout.Spacing.defaultSpacing
            $0.trailing == view.trailingAnchor - Layout.Spacing.defaultSpacing
            $0.centerX == view.centerXAnchor
            $0.centerY == view.centerYAnchor
        }
        
        successImage.layout {
            $0.width == Layout.Width.successImageWidth
            $0.height == Layout.Height.successImageHeight
            $0.centerX == centerStackView.centerXAnchor
        }
        
        titleTextLabel.layout {
            $0.leading == centerStackView.leadingAnchor
            $0.trailing == centerStackView.trailingAnchor
        }
        
        descriptionTextLabel.layout {
            $0.leading == centerStackView.leadingAnchor
            $0.trailing == centerStackView.trailingAnchor
        }
        
        enterButton.layout {
            $0.leading == centerStackView.leadingAnchor
            $0.trailing == centerStackView.trailingAnchor
            $0.height == Layout.Height.enterButtonHeight
        }
    }
    
    @objc
    func enterButtonTapped(_ sender: UIButton) {
        viewModel.enter()
    }
}
