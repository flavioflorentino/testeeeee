import Foundation

enum BankAccountCheckLocalizable: String, Error, CustomStringConvertible, Localizable {
    case bankAccountCheckTitle
    case agencyPlaceholder
    case accountPlaceholder
    case digitPlaceholder
    case bankAccountAlertTitle
    case bankAccountAlertMessage
    case invalidAccountAlertTitle
    case invalidAccountAlertMessage
    case registeredBank
    
    var key: String {
        self.rawValue
    }
    var file: LocalizableFile {
        .bankAccountCheck
    }
    
    var description: String {
        self.rawValue
    }
}
