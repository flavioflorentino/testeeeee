struct TFABankAccountInfo: Decodable {
    let bank: TFABankAccount
}

struct TFABankAccount: Decodable {
    let bankName: String
    let imageUrl: String?
    let agencyDv: Bool?
}
