struct BankAccountData {
    let agency: String
    let agencyDigit: String?
    let account: String
    let accountDigit: String
    
    init(agency: String?, agencyDigit: String?, account: String?, accountDigit: String?) {
        self.agency = agency ?? ""
        self.agencyDigit = agencyDigit
        self.account = account ?? ""
        self.accountDigit = accountDigit ?? ""
    }
}
