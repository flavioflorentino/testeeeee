import Foundation
import Validator

struct BankAccountCheckTextFieldSettings {
    let bankAgency: TextfieldSetting
    let bankAccount: TextfieldSetting
    let accountDigit: TextfieldSetting
    let agencyDigit: TextfieldSetting?
}

enum BankAccountCheckValidationError: String, CustomStringConvertible, Error, ValidationError {
    case agencyRequired = "Agência obrigatória"
    case accountNumberRequired = "Número da conta obrigatória"
    case digitRequired = "Dígito obrigatório"
    
    var message: String {
        self.rawValue
    }
    
    var description: String {
        self.rawValue
    }
}
