import UI
import UIKit

private extension BankAccountCheckViewController.Layout {
    enum Fonts {
        static let titleLabelFont = UIFont.boldSystemFont(ofSize: 28)
        static let continueButtonFont = UIFont.boldSystemFont(ofSize: 15)
    }
    
    enum Height {
        static let continueButtonHeight: CGFloat = 44
    }
    
    enum Width {
        static let accountDigitFieldWidth: CGFloat = 84
    }
}

final class BankAccountCheckViewController: ViewController<BankAccountCheckViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
    lazy var loadingView = LoadingView()
    
    private lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.backgroundColor = .clear
        return scroll
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = BankAccountCheckLocalizable.bankAccountCheckTitle.text
        label.font = Layout.Fonts.titleLabelFont
        label.numberOfLines = 0
        label.textAlignment = .left
        return label
    }()
    
    private lazy var bankAccountView: BankAccountView = {
        let bankAccountView = BankAccountView()
        bankAccountView.delegate = self
        return bankAccountView
    }()
    
    private lazy var bankAgencyTextfield: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.defaultForm()
        textfield.addTarget(self, action: #selector(checkAgencyNumberTextField(_:)), for: .editingChanged)
        textfield.delegate = self
        textfield.keyboardType = .numberPad
        return textfield
    }()
    
    private lazy var agencyDigitTextfield: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.defaultForm()
        textfield.addTarget(self, action: #selector(checkAgencyDigitTextField(_:)), for: .editingChanged)
        textfield.delegate = self
        textfield.isHidden = true
        return textfield
    }()
    
    private lazy var bankAccountTextfield: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.defaultForm()
        textfield.addTarget(self, action: #selector(checkAccountNumberTextField(_:)), for: .editingChanged)
        textfield.keyboardType = .numberPad
        textfield.delegate = self
        return textfield
    }()
    
    private lazy var accountDigitTextfield: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.defaultForm()
        textfield.addTarget(self, action: #selector(checkAccountDigitTextField(_:)), for: .editingChanged)
        textfield.delegate = self
        return textfield
    }()
    
    private lazy var continueButton: UIPPButton = {
        let button = UIPPButton()
        button.addTarget(self, action: #selector(didTapContinueButton), for: .touchUpInside)
        button.normalBackgrounColor = Palette.ppColorBranding300.color
        button.normalTitleColor = Palette.ppColorGrayscale000.color
        button.setTitleColor(Palette.ppColorGrayscale000.color)
        button.disabledTitleColor = Palette.ppColorGrayscale000.color
        button.disabledBackgrounColor = Palette.ppColorGrayscale400.color
        button.setTitle(DefaultLocalizable.next.text, for: .normal)
        button.setTitle(DefaultLocalizable.next.text, for: .disabled)
        button.titleLabel?.font = Layout.Fonts.continueButtonFont
        button.cornerRadius = Layout.Height.continueButtonHeight / 2
        return button
    }()
    
    private var agencyWithDigitConstraint: NSLayoutConstraint?
    private var agencyWithoutDigitConstraint: NSLayoutConstraint?
    private lazy var keyboardScrollViewHandler = KeyboardScrollViewHandler(scrollView: scrollView)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        keyboardScrollViewHandler.registerForKeyboardNotifications()
        viewModel.loadInfo()
        navigationItem.backBarButtonItem = .noContextTitlelessBackButton
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(bankAccountView)
        contentView.addSubview(bankAgencyTextfield)
        contentView.addSubview(agencyDigitTextfield)
        contentView.addSubview(bankAccountTextfield)
        contentView.addSubview(accountDigitTextfield)
        contentView.addSubview(continueButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override func setupConstraints() {
        scrollViewConstraints()
        labelConstraints()
        bankViewConstraints()
        textFieldConstraints()
        buttonConstraints()
    }
    
    private func scrollViewConstraints() {
        scrollView.layout {
            $0.top == view.topAnchor
            $0.bottom == view.bottomAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
        }
        
        contentView.layout {
            $0.top == scrollView.topAnchor
            $0.bottom == scrollView.bottomAnchor
            $0.leading == scrollView.leadingAnchor
            $0.trailing == scrollView.trailingAnchor
            $0.width == view.widthAnchor
        }
    }
    
    private func labelConstraints() {
        titleLabel.layout {
            $0.top == contentView.topAnchor + Spacing.base02
            $0.bottom == bankAccountView.topAnchor - Spacing.base04
            $0.leading == contentView.leadingAnchor + Spacing.base02
            $0.trailing == contentView.trailingAnchor - Spacing.base02
        }
    }
    
    private func bankViewConstraints() {
        bankAccountView.layout {
            $0.bottom == bankAgencyTextfield.topAnchor - Spacing.base03
            $0.leading == contentView.leadingAnchor + Spacing.base02
            $0.trailing == contentView.trailingAnchor - Spacing.base02
        }
    }
    
    private func textFieldConstraints() {
        bankAgencyTextfield.layout {
            $0.leading == contentView.leadingAnchor + Spacing.base02
            agencyWithDigitConstraint = $0.trailing == agencyDigitTextfield.leadingAnchor - Spacing.base02
            agencyWithDigitConstraint?.isActive = false
            agencyWithoutDigitConstraint = $0.trailing == contentView.trailingAnchor - Spacing.base02
        }
        
        agencyDigitTextfield.layout {
            $0.trailing == contentView.trailingAnchor - Spacing.base02
            $0.centerY == bankAgencyTextfield.centerYAnchor
            $0.width == Layout.Width.accountDigitFieldWidth
        }
        
        bankAccountTextfield.layout {
            $0.top == bankAgencyTextfield.bottomAnchor + Spacing.base04
            $0.bottom == continueButton.topAnchor - Spacing.base04
            $0.leading == contentView.leadingAnchor + Spacing.base02
            $0.trailing == accountDigitTextfield.leadingAnchor - Spacing.base02
        }
        
        accountDigitTextfield.layout {
            $0.trailing == contentView.trailingAnchor - Spacing.base02
            $0.centerY == bankAccountTextfield.centerYAnchor
            $0.width == Layout.Width.accountDigitFieldWidth
        }
    }
    
    private func buttonConstraints() {
        continueButton.layout {
            $0.bottom == contentView.bottomAnchor - Spacing.base03
            $0.height == Spacing.base06
            $0.leading == contentView.leadingAnchor + Spacing.base02
            $0.trailing == contentView.trailingAnchor - Spacing.base02
        }
    }
    
    @objc
    private func checkAccountNumberTextField(_ sender: UIPPFloatingTextField) {
        viewModel.checkAccountNumberFieldWith(validationResult: sender.validate())
        checkValidState()
    }
    
    @objc
    private func checkAccountDigitTextField(_ sender: UIPPFloatingTextField) {
        viewModel.checkAccountDigitFieldWith(validationResult: sender.validate())
        checkValidState()
    }
    
    @objc
    private func checkAgencyNumberTextField(_ sender: UIPPFloatingTextField) {
        viewModel.checkAgencyNumberFieldWith(validationResult: sender.validate())
        checkValidState()
    }
    
    @objc
    private func checkAgencyDigitTextField(_ sender: UIPPFloatingTextField) {
        viewModel.checkAgencyDigitFieldWith(validationResult: sender.validate())
        checkValidState()
    }
    
    private func checkValidState() {
        viewModel.checkFieldsAndEnableContinue(
            accountNumberValidationResult: bankAccountTextfield.validate(),
            accountDigitValidationResult: accountDigitTextfield.validate(),
            agencyValidationResult: bankAgencyTextfield.validate(),
            agencyDigitValidationResult: agencyDigitTextfield.validate()
        )
    }
    
    @objc
    private func didTapContinueButton() {
        view?.endEditing(true)
        viewModel.validateForm(
            agency: bankAgencyTextfield.text,
            agencyDigit: agencyDigitTextfield.text,
            account: bankAccountTextfield.text,
            accountDigit: accountDigitTextfield.text
        )
    }
    
    private func setLoadingState(loading: Bool) {
        bankAgencyTextfield.isEnabled = !loading
        bankAccountTextfield.isEnabled = !loading
        accountDigitTextfield.isEnabled = !loading
        agencyDigitTextfield.isEnabled = !loading
        loading ? continueButton.startLoadingAnimating() : continueButton.stopLoadingAnimating()
    }
    
    private func enableFields(enable: Bool) {
        bankAgencyTextfield.isEnabled = enable
        bankAccountTextfield.isEnabled = enable
        accountDigitTextfield.isEnabled = enable
        agencyDigitTextfield.isEnabled = enable
    }
    
    private func enableValidation() {
        continueButton.isEnabled = true
        continueButton.setTitle(DefaultLocalizable.next.text, for: .disabled)
        enableFields(enable: true)
    }
}

// MARK: View Model Outputs
extension BankAccountCheckViewController: BankAccountCheckDisplay {
    func setFieldRules(settings: BankAccountCheckTextFieldSettings) {
        bankAgencyTextfield.placeholder = settings.bankAgency.placeholder
        bankAgencyTextfield.validationRules = settings.bankAgency.rule
        
        bankAccountTextfield.placeholder = settings.bankAccount.placeholder
        bankAccountTextfield.validationRules = settings.bankAccount.rule
        
        accountDigitTextfield.placeholder = settings.accountDigit.placeholder
        accountDigitTextfield.validationRules = settings.accountDigit.rule
        
        agencyDigitTextfield.placeholder = settings.agencyDigit?.placeholder
        agencyDigitTextfield.validationRules = settings.agencyDigit?.rule
        
        checkValidState()
    }
    
    func displayBankInfo(bank: TFABankAccount?, hasAgencyField: Bool) {
        bankAccountView.setBankInfo(with: bank)
        agencyDigitTextfield.isHidden = !hasAgencyField
        agencyWithDigitConstraint?.isActive = hasAgencyField
        agencyWithoutDigitConstraint?.isActive = !hasAgencyField
    }
    
    func displayBankAccountHelpInfo(alert: Alert) {
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func displayGenericError(alert: Alert) {
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func displayLoading(loading: Bool) {
        setLoadingState(loading: loading)
    }
    
    func displayLoadingScreen(loading: Bool) {
        loading ? startLoadingView() : stopLoadingView()
    }
    
    func displayValidationInfoError(alert: Alert) {
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func displayAttempLimitError(alert: Alert, retryTime: TimeInterval) {
        viewModel.startTimerForNewValidation(retryTime: retryTime)
        
        enableFields(enable: false)
        continueButton.isEnabled = false
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func displayContinueButton(with currentRemainingTime: String, enabled: Bool) {
        if enabled {
            enableValidation()
        } else {
            continueButton.setTitle(currentRemainingTime, for: .disabled)
        }
    }
    
    func displayAccountNumberFieldErrorMessage(_ message: String?) {
        bankAccountTextfield.errorMessage = message
    }
    
    func displayAccountDigitFieldErrorMessage(_ message: String?) {
        accountDigitTextfield.errorMessage = message
    }
    
    func displayAgencyNumberFieldErrorMessage(_ message: String?) {
        bankAgencyTextfield.errorMessage = message
    }
    
    func displayAgencyDigitFieldErrorMessage(_ message: String?) {
        agencyDigitTextfield.errorMessage = message
    }
    
    func displayContinueButtonEnabled(_ enabled: Bool) {
        continueButton.isEnabled = enabled
        continueButton.setTitle(DefaultLocalizable.next.text, for: .normal)
        continueButton.setTitle(DefaultLocalizable.next.text, for: .disabled)
    }
}

extension BankAccountCheckViewController: BankAccountViewProtocol {
    func didTouchHelpButton(banckAccountView: BankAccountView) {
        viewModel.bankAccountHelpInfo()
    }
}

extension BankAccountCheckViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        configTextField(with: textField, range: range, replacementString: string)
    }
    
    private func configTextField(with textField: UITextField, range: NSRange, replacementString string: String) -> Bool {
        guard
            let ppTextField = textField as? UIPPFloatingTextField,
            let textValue = textField.text else {
                return true
        }
        
        let newText = (textValue as NSString).replacingCharacters(in: range, with: string)
        if ppTextField == accountDigitTextfield || ppTextField == agencyDigitTextfield {
            return newText.length <= 1
        }
        
        return true
    }
}

extension BankAccountCheckViewController: LoadingViewProtocol { }
