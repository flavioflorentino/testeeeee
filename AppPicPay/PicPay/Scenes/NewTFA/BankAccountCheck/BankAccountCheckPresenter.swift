import AnalyticsModule
import Core
import Foundation
import UI

protocol BankAccountCheckPresenting: AnyObject {
    var viewController: BankAccountCheckDisplay? { get set }
    func setFieldsRules(settings: BankAccountCheckTextFieldSettings)
    func presentBankInfo(bank: TFABankAccount?)
    func validateData(with type: TFAType, nextStep: TFAStep)
    func presentBankAccountHelpInfo()
    func setLoading(loading: Bool)
    func presentLoadingScreen(_ isLoading: Bool)
    func presentContinueButtonUpdated(with remainingTime: TimeInterval)
    func presentValidationInfoError(tfaError: RequestError)
    func presentAttemptLimitError(tfaError: RequestError, retryTime: Int, tfaType: TFAType)
    func presentGenericError()
    func presentAccountNumberFieldWith(errorMessage: String?)
    func presentAccountDigitFieldWith(errorMessage: String?)
    func presentAgencyNumberFieldWith(errorMessage: String?)
    func presentAgencyDigitFieldWith(errorMessage: String?)
    func shouldEnableContinue(_ enable: Bool)
}

final class BankAccountCheckPresenter: BankAccountCheckPresenting {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies
    
    private let coordinator: BankAccountCheckCoordinating
    var viewController: BankAccountCheckDisplay?
    private let tfaTimerformatter = TFATimerFormatter()
    
    init(coordinator: BankAccountCheckCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    func setFieldsRules(settings: BankAccountCheckTextFieldSettings) {
        viewController?.setFieldRules(settings: settings)
    }
    
    func presentBankInfo(bank: TFABankAccount?) {
        viewController?.displayBankInfo(bank: bank, hasAgencyField: bank?.agencyDv ?? false)
    }
    
    func validateData(with tfaType: TFAType, nextStep: TFAStep) {
        coordinator.perform(action: .accountDataValidateAndStartNextStep(tfaType: tfaType, nextStep: nextStep))
    }
    
    func presentBankAccountHelpInfo() {
        let alert = Alert(
            with: BankAccountCheckLocalizable.bankAccountAlertTitle.text,
            text: BankAccountCheckLocalizable.bankAccountAlertMessage.text,
            buttons: [
                Button(title: DefaultLocalizable.back.text, type: .cta)
            ]
        )
        
        self.viewController?.displayBankAccountHelpInfo(alert: alert)
    }
    
    func setLoading(loading: Bool) {
        viewController?.displayLoading(loading: loading)
    }
    
    func presentLoadingScreen(_ isLoading: Bool) {
        viewController?.displayLoadingScreen(loading: isLoading)
    }
    
    func presentGenericError() {
        let alert = Alert(
            with: TFALocalizable.genericErrorTitle.text,
            text: TFALocalizable.genericErrorMessage.text,
            buttons: [
                Button(title: DefaultLocalizable.back.text, type: .cta)
            ]
        )
        
        self.viewController?.displayGenericError(alert: alert)
    }
    
    func presentValidationInfoError(tfaError: RequestError) {
        let alert = Alert(title: tfaError.title, text: tfaError.message)
        alert.buttons = [Button(title: DefaultLocalizable.back.text, type: .cta)]
        
        viewController?.displayValidationInfoError(alert: alert)
    }
    
    func presentAttemptLimitError(tfaError: RequestError, retryTime: Int, tfaType: TFAType) {
        let alert = Alert(title: tfaError.title, text: tfaError.message)
        alert.buttons = [Button(title: DefaultLocalizable.back.text, type: .cta)]
        
        Analytics.shared.log(TFAEvents.didReceivedExceededDataValidationLimitError(tfaType: tfaType, currentStep: .bankData))
        viewController?.displayAttempLimitError(alert: alert, retryTime: TimeInterval(retryTime))
    }
    
    func presentContinueButtonUpdated(with remainingTime: TimeInterval) {
        let formatedTime = tfaTimerformatter.timeString(with: remainingTime)
        let buttonText = "\(DefaultLocalizable.wait.text) \(formatedTime)"
        viewController?.displayContinueButton(with: buttonText, enabled: remainingTime <= 0)
    }
    
    func presentAccountNumberFieldWith(errorMessage: String?) {
        viewController?.displayAccountNumberFieldErrorMessage(errorMessage)
    }
    
    func presentAccountDigitFieldWith(errorMessage: String?) {
        viewController?.displayAccountDigitFieldErrorMessage(errorMessage)
    }
    
    func presentAgencyNumberFieldWith(errorMessage: String?) {
        viewController?.displayAgencyNumberFieldErrorMessage(errorMessage)
    }
    
    func presentAgencyDigitFieldWith(errorMessage: String?) {
        viewController?.displayAgencyDigitFieldErrorMessage(errorMessage)
    }
    
    func shouldEnableContinue(_ enable: Bool) {
        viewController?.displayContinueButtonEnabled(enable)
    }
}
