import Core
import Foundation

protocol BankAccountCheckServicing {
    func validateBankAccountData(
        bankAccount: BankAccountData,
        tfaRequestData: TFARequestData,
        completion: @escaping (Result<TFAValidationResponse, ApiError>) -> Void
    )
    func getBankAccountData(tfaRequestData: TFARequestData, completion: @escaping (Result<TFABankAccountInfo, ApiError>) -> Void)
}

final class BankAccountCheckService: BankAccountCheckServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func validateBankAccountData(
        bankAccount: BankAccountData,
        tfaRequestData: TFARequestData,
        completion: @escaping (Result<TFAValidationResponse, ApiError>) -> Void
    ) {
        let api = Api<TFAValidationResponse>(
            endpoint: DynamicTFAEndpoint.validateBankData(bankAccount: bankAccount, tfaRequestData: tfaRequestData)
        )
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result
                    .map { $0.model }
                completion(mappedResult)
            }
        }
    }
    
    func getBankAccountData(
        tfaRequestData: TFARequestData,
        completion: @escaping (Result<TFABankAccountInfo, ApiError>) -> Void
    ) {
        let api = Api<TFABankAccountInfo>(endpoint: DynamicTFAEndpoint.getBankData(tfaRequestData: tfaRequestData))
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result
                    .map { $0.model }
                completion(mappedResult)
            }
        }
    }
}
