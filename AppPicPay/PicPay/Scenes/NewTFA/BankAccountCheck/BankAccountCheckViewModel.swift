import AnalyticsModule
import Core
import Foundation
import Validator

protocol BankAccountCheckViewModelInputs: AnyObject {
    func loadInfo()
    func checkFieldsAndEnableContinue(
        accountNumberValidationResult: ValidationResult,
        accountDigitValidationResult: ValidationResult,
        agencyValidationResult: ValidationResult,
        agencyDigitValidationResult: ValidationResult
    )
    func checkAccountNumberFieldWith(validationResult: ValidationResult)
    func checkAccountDigitFieldWith(validationResult: ValidationResult)
    func checkAgencyNumberFieldWith(validationResult: ValidationResult)
    func checkAgencyDigitFieldWith(validationResult: ValidationResult)
    func validateForm(agency: String?, agencyDigit: String?, account: String?, accountDigit: String?)
    func bankAccountHelpInfo()
    func startTimerForNewValidation(retryTime: TimeInterval)
}

final class BankAccountCheckViewModel {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    private let service: BankAccountCheckServicing
    private let presenter: BankAccountCheckPresenting
    private let bankInfo: TFABankAccount?
    private let tfaRequestData: TFARequestData
    
    private var timer: RepeatingTimer?
    private var endDate = Date()
    
    init(
        service: BankAccountCheckServicing,
        presenter: BankAccountCheckPresenting,
        bankInfo: TFABankAccount?,
        tfaRequestData: TFARequestData,
        dependencies: Dependencies,
        timer: RepeatingTimer? = RepeatingTimer(timeInterval: 1)
    ) {
        self.service = service
        self.presenter = presenter
        self.bankInfo = bankInfo
        self.tfaRequestData = tfaRequestData
        self.dependencies = dependencies
        self.timer = timer
    }
}

extension BankAccountCheckViewModel: BankAccountCheckViewModelInputs {
    func loadInfo() {
        guard let info = bankInfo else {
            loadBankInfo()
            return
        }
        presenter.presentBankInfo(bank: info)
        buildTextfieldSettings(hasAgencyField: info.agencyDv ?? false)
    }
    
    func checkFieldsAndEnableContinue(
        accountNumberValidationResult: ValidationResult,
        accountDigitValidationResult: ValidationResult,
        agencyValidationResult: ValidationResult,
        agencyDigitValidationResult: ValidationResult
    ) {
        let enable = accountNumberValidationResult.isValid &&
            accountDigitValidationResult.isValid &&
            agencyValidationResult.isValid &&
            agencyDigitValidationResult.isValid
        
        presenter.shouldEnableContinue(enable)
    }
    
    func checkAccountNumberFieldWith(validationResult: ValidationResult) {
        presenter.presentAccountNumberFieldWith(errorMessage: validationErrorMessageFor(result: validationResult))
    }
    
    func checkAccountDigitFieldWith(validationResult: ValidationResult) {
        presenter.presentAccountDigitFieldWith(errorMessage: validationErrorMessageFor(result: validationResult))
    }
    
    func checkAgencyNumberFieldWith(validationResult: ValidationResult) {
        presenter.presentAgencyNumberFieldWith(errorMessage: validationErrorMessageFor(result: validationResult))
    }
    
    func checkAgencyDigitFieldWith(validationResult: ValidationResult) {
        presenter.presentAgencyDigitFieldWith(errorMessage: validationErrorMessageFor(result: validationResult))
    }
    
    func validateForm(agency: String?, agencyDigit: String?, account: String?, accountDigit: String?) {
        let bankAccountData = BankAccountData(
            agency: agency,
            agencyDigit: agencyDigit,
            account: account,
            accountDigit: accountDigit
        )

        validateFormForNextStep(bankAccountData: bankAccountData)
    }
    
    func bankAccountHelpInfo() {
        presenter.presentBankAccountHelpInfo()
    }
    
    func startTimerForNewValidation(retryTime: TimeInterval) {
        endDate = Date(timeInterval: retryTime, since: Date())
        runCountDown()
    }
}

private extension BankAccountCheckViewModel {
    private func loadBankInfo() {
        presenter.presentLoadingScreen(true)
        
        service.getBankAccountData(tfaRequestData: tfaRequestData) { [weak self] (result: Result<TFABankAccountInfo, ApiError>) in
            self?.parseBankAccountResponse(with: result)
        }
    }
    
    private func parseBankAccountResponse(with result: Result<TFABankAccountInfo, ApiError>) {
        presenter.presentLoadingScreen(false)
        switch result {
        case let .success(info):
            presenter.presentBankInfo(bank: info.bank)
            buildTextfieldSettings(hasAgencyField: info.bank.agencyDv ?? false)
        case .failure(let apiError):
            parseError(error: apiError)
        }
    }
    
    private func validateFormForNextStep(bankAccountData: BankAccountData) {
        presenter.setLoading(loading: true)

        service.validateBankAccountData(
            bankAccount: bankAccountData,
            tfaRequestData: tfaRequestData
        ) { [weak self] (result: Result<TFAValidationResponse, ApiError>) in
            guard let self = self else {
                return
            }
            self.presenter.setLoading(loading: false)
            switch result {
            case let .success(response):
                Analytics.shared.log(TFAEvents.didValidateBankAccount(self.tfaRequestData.flow))
                self.presenter.validateData(with: self.tfaRequestData.flow, nextStep: response.nextStep)
            case let .failure(error):
                self.parseError(error: error)
            }
        }
    }
    
    private func buildTextfieldSettings(hasAgencyField: Bool) {
        let settings = BankAccountCheckTextFieldSettings(
            bankAgency: buildTextfieldSettingBankAgency(),
            bankAccount: buildTextfieldSettingBankAccount(),
            accountDigit: buildTextfieldSettingOneDigitField(),
            agencyDigit: hasAgencyField ? buildTextfieldSettingOneDigitField() : nil
        )
        
        presenter.setFieldsRules(settings: settings)
    }
    
    private func buildTextfieldSettingBankAgency() -> TextfieldSetting {
        var bankAgencyRules = ValidationRuleSet<String>()
        let minimumLengthRule = ValidationRuleLength(
            min: 1,
            lengthType: .characters,
            error: BankAccountCheckValidationError.agencyRequired
        )
        
        bankAgencyRules.add(rule: minimumLengthRule)
        
        let placeholder = BankAccountCheckLocalizable.agencyPlaceholder.text
        return TextfieldSetting(placeholder: placeholder, text: nil, rule: bankAgencyRules, mask: nil)
    }
    
    private func buildTextfieldSettingBankAccount() -> TextfieldSetting {
        var bankAccountRules = ValidationRuleSet<String>()
        let minimumLengthRule = ValidationRuleLength(
            min: 1,
            lengthType: .characters,
            error: BankAccountCheckValidationError.accountNumberRequired
        )
        
        bankAccountRules.add(rule: minimumLengthRule)
        
        let placeholder = BankAccountCheckLocalizable.accountPlaceholder.text
        return TextfieldSetting(placeholder: placeholder, text: nil, rule: bankAccountRules, mask: nil)
    }
    
    private func buildTextfieldSettingOneDigitField() -> TextfieldSetting {
        var accountDigitRules = ValidationRuleSet<String>()
        let minimumAndMaxLengthRule = ValidationRuleLength(
            min: 1,
            max: 1,
            lengthType: .characters,
            error: BankAccountCheckValidationError.digitRequired
        )
        
        accountDigitRules.add(rule: minimumAndMaxLengthRule)
        
        let placeholder = BankAccountCheckLocalizable.digitPlaceholder.text
        return TextfieldSetting(placeholder: placeholder, text: nil, rule: accountDigitRules, mask: nil)
    }
    
    private func validationErrorMessageFor(result: ValidationResult) -> String? {
        guard
            case let .invalid(error) = result,
            let err = error.first
            else {
                return nil
        }
        
        return String(describing: err)
    }
    
    private func parseError(error: ApiError) {
        switch error {
        case .tooManyRequests(let requestError), .unauthorized(let requestError), .badRequest(let requestError):
            guard
                let errorData = requestError.data?.dictionaryObject?["data"] as? [String: Any],
                let retryTime = errorData["retry_time"] as? Int
                else {
                    presenter.presentValidationInfoError(tfaError: requestError)
                    return
            }
            
            presenter.presentAttemptLimitError(tfaError: requestError, retryTime: retryTime, tfaType: tfaRequestData.flow)
        default:
            presenter.presentGenericError()
        }
    }
    
    private func stopTimer() {
        timer?.eventHandler = nil
        timer?.suspend()
        timer = nil
    }
    
    private func runCountDown() {
        if timer == nil {
            timer = RepeatingTimer(timeInterval: 1)
        }
        
        timer?.eventHandler = { [weak self] in
            self?.updateTimer()
        }
        timer?.resume()
    }
    
    private func updateTimer() {
        dependencies.mainQueue.async { [weak self] in
            guard let self = self else {
                return
            }
         
            let endTime = self.endDate.timeIntervalSince(Date()).rounded(.toNearestOrEven)
            self.presenter.presentContinueButtonUpdated(with: endTime)
            
            if endTime <= 0 {
                self.stopTimer()
                self.presenter.shouldEnableContinue(true)
                return
            }
        }
    }
}
