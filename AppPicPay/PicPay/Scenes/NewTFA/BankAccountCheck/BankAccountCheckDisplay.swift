import UI

protocol BankAccountCheckDisplay: AnyObject {
    func setFieldRules(settings: BankAccountCheckTextFieldSettings)
    func displayBankInfo(bank: TFABankAccount?, hasAgencyField: Bool)
    func displayBankAccountHelpInfo(alert: Alert)
    func displayLoading(loading: Bool)
    func displayLoadingScreen(loading: Bool)
    func displayValidationInfoError(alert: Alert)
    func displayAttempLimitError(alert: Alert, retryTime: TimeInterval)
    func displayGenericError(alert: Alert)
    func displayContinueButton(with currentRemainingTime: String, enabled: Bool)
    func displayAccountNumberFieldErrorMessage(_ message: String?)
    func displayAccountDigitFieldErrorMessage(_ message: String?)
    func displayAgencyNumberFieldErrorMessage(_ message: String?)
    func displayAgencyDigitFieldErrorMessage(_ message: String?)
    func displayContinueButtonEnabled(_ enabled: Bool)
}
