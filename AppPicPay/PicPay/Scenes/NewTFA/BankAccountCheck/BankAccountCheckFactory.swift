import Foundation

enum BankAccountCheckFactory {
    static func make(
        with bankInfo: TFABankAccount? = nil,
        tfaRequestData: TFARequestData,
        coordinatorDelegate: BankAccountCheckCoordinatorDelegate
    ) -> BankAccountCheckViewController {
        let container = DependencyContainer()
        let service: BankAccountCheckServicing = BankAccountCheckService(dependencies: container)
        let coordinator: BankAccountCheckCoordinating = BankAccountCheckCoordinator()
        let presenter: BankAccountCheckPresenting = BankAccountCheckPresenter(coordinator: coordinator, dependencies: container)
        let viewModel = BankAccountCheckViewModel(
            service: service,
            presenter: presenter,
            bankInfo: bankInfo,
            tfaRequestData: tfaRequestData,
            dependencies: container
        )
        let viewController = BankAccountCheckViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        presenter.viewController = viewController

        return viewController
    }
}
