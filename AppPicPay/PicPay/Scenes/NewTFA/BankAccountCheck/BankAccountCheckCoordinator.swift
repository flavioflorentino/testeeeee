import UIKit

enum BankAccountCheckAction {
    case accountDataValidateAndStartNextStep(tfaType: TFAType, nextStep: TFAStep)
}

protocol BankAccountCheckCoordinatorDelegate: AnyObject {
    func didValidateAccountData(with Type: TFAType, nextStep: TFAStep)
}

protocol BankAccountCheckCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: BankAccountCheckCoordinatorDelegate? { get set }
    func perform(action: BankAccountCheckAction)
}

final class BankAccountCheckCoordinator: BankAccountCheckCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: BankAccountCheckCoordinatorDelegate?
    
    func perform(action: BankAccountCheckAction) {
        if case let .accountDataValidateAndStartNextStep(tfaType, nextStep) = action {
            delegate?.didValidateAccountData(with: tfaType, nextStep: nextStep)
        }
    }
}
