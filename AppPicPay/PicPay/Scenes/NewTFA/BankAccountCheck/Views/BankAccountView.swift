import UI
import UIKit

private extension BankAccountView.Layout {
    enum Fonts {
        static let bankNameFont = UIFont.systemFont(ofSize: 14)
    }
}

protocol BankAccountViewProtocol: AnyObject {
    func didTouchHelpButton(banckAccountView: BankAccountView)
}

final class BankAccountView: UIView {
    fileprivate enum Layout { }

    private lazy var bankImage: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = imageView.frame.height / 2.0
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        return imageView
    }()

    private lazy var bankName: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.bankNameFont
        label.textColor = Palette.ppColorGrayscale500.color
        return label
    }()

    private lazy var infoButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.UpgradeChecklist.greenHelpButton.image, for: .normal)
        button.addTarget(self, action: #selector(helpButtonTapped(_:)), for: .touchUpInside)
        return button
    }()

    weak var delegate: BankAccountViewProtocol?

    init() {
        super.init(frame: .zero)
        addComponents()
        layoutComponents()
        setup()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func addComponents() {
        addSubview(bankImage)
        addSubview(bankName)
        addSubview(infoButton)
    }

    private func layoutComponents() {
        bankImage.layout {
            $0.leading == leadingAnchor + Spacing.base02
            $0.top == topAnchor + Spacing.base02
            $0.bottom == bottomAnchor - Spacing.base02
            $0.trailing == bankName.leadingAnchor - Spacing.base01
            $0.width == Spacing.base04
            $0.height == Spacing.base04
        }

        bankName.layout {
            $0.centerY == bankImage.centerYAnchor
            $0.trailing == infoButton.leadingAnchor - Spacing.base01
        }

        infoButton.layout {
            $0.trailing == trailingAnchor - Spacing.base02
            $0.centerY == bankImage.centerYAnchor
            $0.width == Spacing.base03
            $0.height == Spacing.base03
        }
    }

    private func setup() {
        backgroundColor = Palette.ppColorGrayscale100.color.withCustomDarkColor(#colorLiteral(red: 0.2235294118, green: 0.2745098039, blue: 0.3019607843, alpha: 1))
        layer.cornerRadius = Spacing.base01
    }

    @objc
    private func helpButtonTapped(_ sender: UIButton) {
        delegate?.didTouchHelpButton(banckAccountView: self)
    }
    
    func setBankInfo(with bank: TFABankAccount?) {
        bankName.text = bank?.bankName ?? BankAccountCheckLocalizable.registeredBank.text
        bankImage.setImage(url: URL(string: bank?.imageUrl ?? ""), placeholder: Assets.Icons.icoBankPlaceholder.image)
    }
}
