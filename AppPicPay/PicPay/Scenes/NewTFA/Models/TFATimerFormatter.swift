import Foundation

struct TFATimerFormatter {
    private let timeFormatter: DateComponentsFormatter
    
    init(
        units: NSCalendar.Unit = [.minute, .second],
        zeroFormattingBehavior: DateComponentsFormatter.ZeroFormattingBehavior = .pad
    ) {
        timeFormatter = DateComponentsFormatter()
        timeFormatter.allowedUnits = units
        timeFormatter.collapsesLargestUnit = true
        timeFormatter.zeroFormattingBehavior = zeroFormattingBehavior
    }
    
    func timeString(with timeInterval: TimeInterval) -> String {
        timeFormatter.string(from: timeInterval) ?? ""
    }
}
