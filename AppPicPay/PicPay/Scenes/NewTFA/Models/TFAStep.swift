enum TFAStep: String, Decodable {
    case initial = "INITIAL"
    case selfie = "SELFIE"
    case document = "DOCUMENT"
    case userData = "DATA_CONSUMER"
    case bankData = "BANK_ACCOUNT"
    case phoneNumber = "PHONE_NUMBER"
    case creditCard = "CREDIT_CARD"
    case code = "CODE"
    case final = "FINAL"
}
