import Foundation

struct TFASelfieValidationData: Decodable {
    struct Selfie: Decodable {
        let token: String
        let flow: String
    }
    
    let selfie: Selfie
}
