import AnalyticsModule
import Core
import FeatureFlag
import Foundation
import SecurityModule

protocol NewTFAOptionsViewModelInputs: AnyObject {
    func loadOptions()
    func showLoading(_ loading: Bool)
    func selectedItem(_ indexPath: IndexPath)
    func selectFallback()
}

final class NewTFAOptionsViewModel {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies

    private let service: NewTFAOptionsServicing
    private let presenter: NewTFAOptionsPresenting
    private var items: [NewTFAOption] = []
    private var fallbackOption: NewTFAOption?
    private let tfaRequestData: TFARequestData
    private let logDetectionService: LogDetectionServicing
    private let tfaBlockedCode = "tfa_blocked_code"
    
    init(
        service: NewTFAOptionsServicing,
        presenter: NewTFAOptionsPresenting,
        dependencies: Dependencies,
        tfaRequestData: TFARequestData,
        logDetectionService: LogDetectionServicing
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.tfaRequestData = tfaRequestData
        self.logDetectionService = logDetectionService
    }
}

extension NewTFAOptionsViewModel: NewTFAOptionsViewModelInputs {
    func loadOptions() {
        presenter.presentLoading(true)
        service.tfaOptions(with: tfaRequestData) { [weak self] result in
            self?.presenter.presentLoading(false)
            switch result {
            case .success(let options):
                self?.verifyOptions(options)
            case .failure(let error):
                guard let requestError = error.requestError else {
                    self?.presenter.presentError(error: RequestError())
                    return
                }
                
                self?.verifyError(requestError)
            }
        }
    }
    
    func showLoading(_ loading: Bool) {
        presenter.presentLoading(loading)
    }
    
    func selectedItem(_ indexPath: IndexPath) {
        logDetectionService.sendLog()

        let selectedOption = items[indexPath.row]
        guard selectedOption.flow == .validationID else {
            presenter.presentNextStepFor(selectedOption)
            return
        }
        
        startValidationFlow(with: selectedOption)
    }

    func selectFallback() {
        guard let fallback = fallbackOption else { return }
        startValidationFlow(with: fallback)
    }
    
    private func verifyError(_ error: RequestError) {
        if error.code == tfaBlockedCode {
            presenter.presentTFABlockedError(error)
        } else {
            presenter.presentError(error: error)
        }
    }
    
    private func verifyOptions(_ options: NewTFAOptions) {
        var filteredOptions = options.options
        if !dependencies.featureManager.isActive(.featureTfaCreditcardFlowActive) {
            filteredOptions = options.options.filter({ $0.flow != .creditCard })
        }

        if let fallback = options.fallback {
            fallbackOption = fallback
            presenter.presentFallback(fallback.title)
        }

        Analytics.shared.log(TFAEvents.authorizationMethodsViewed(filteredOptions.map({ $0.flow })))
        
        items = filteredOptions
        presenter.presentOptions(filteredOptions)
    }
    
    private func startValidationFlow(with option: NewTFAOption) {
        let tfaData = TFARequestData(flow: option.flow, step: option.firstStep, id: tfaRequestData.id)
        
        presenter.presentLoading(true)
        service.startValidation(with: tfaData) { [weak self] result in
            self?.presenter.presentLoading(false)
            switch result {
            case .success(let response):
                self?.presenter.presentSelfieValidation(with: response, andOption: option)
            case .failure(let error):
                guard let requestError = error.requestError else {
                    self?.presenter.presentSelfieValidationError(error: RequestError())
                    return
                }
                
                self?.presenter.presentSelfieValidationError(error: requestError)
            }
        }
    }
}
