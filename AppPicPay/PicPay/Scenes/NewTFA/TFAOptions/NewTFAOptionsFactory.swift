import Foundation
import SecurityModule

enum NewTFAOptionsFactory {
    static func make(with coordinatorDelegate: NewTFAOptionsCoordinatorDelegate, tfaRequestData: TFARequestData) -> NewTFAOptionsViewController {
        let container = DependencyContainer()
        let service: NewTFAOptionsServicing = NewTFAOptionsService(dependencies: container)
        let coordinator: NewTFAOptionsCoordinating = NewTFAOptionsCoordinator()
        let presenter: NewTFAOptionsPresenting = NewTFAOptionsPresenter(coordinator: coordinator)
        
        let logDetectionService: LogDetectionServicing = LogDetectionService()
        let viewModel = NewTFAOptionsViewModel(service: service,
                                               presenter: presenter,
                                               dependencies: container,
                                               tfaRequestData: tfaRequestData,
                                               logDetectionService: logDetectionService)
        
        let viewController = NewTFAOptionsViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        presenter.viewController = viewController

        return viewController
    }
}
