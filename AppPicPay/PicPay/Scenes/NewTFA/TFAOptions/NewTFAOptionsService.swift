import Core
import Foundation

protocol NewTFAOptionsServicing {
    func tfaOptions(with data: TFARequestData, completion: @escaping (Result<NewTFAOptions, ApiError>) -> Void)
    func startValidation(with data: TFARequestData, completion: @escaping (Result<TFASelfieValidationData, ApiError>) -> Void)
}

final class NewTFAOptionsService: NewTFAOptionsServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func tfaOptions(with data: TFARequestData, completion: @escaping (Result<NewTFAOptions, ApiError>) -> Void) {
        let api = Api<NewTFAOptions>(endpoint: DynamicTFAEndpoint.tfaOptions(tfaRequestData: data))
        
        api.execute { result in
            self.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func startValidation(
        with data: TFARequestData,
        completion: @escaping (Result<TFASelfieValidationData, ApiError>) -> Void
    ) {
        let api = Api<TFASelfieValidationData>(endpoint: DynamicTFAEndpoint.validation(tfaRequestData: data))
        
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
