import UI
import UIKit

private extension NewTFAOptionsViewController.Layout {
    enum Fonts {
        static let titleLabelFont = UIFont.boldSystemFont(ofSize: 28)
        static let descriptionLabelFont = UIFont.systemFont(ofSize: 16)
        static let fallbackLabelFont = UIFont.systemFont(ofSize: 13)
    }
    
    enum Spacing {
        static let defaultOffset: CGFloat = 20
        static let separatorViewTopOffset: CGFloat = 24
        static let fallbackLabelTopOffset: CGFloat = 24
        static let fallbackLabelBottomOffset: CGFloat = 24
    }
    enum Height {
        static let separatorViewHeight: CGFloat = 1
        static let cellEstimatedRowHeight: CGFloat = 80
    }
}

final class NewTFAOptionsViewController: ViewController<NewTFAOptionsViewModelInputs, UIView>, LoadingViewProtocol {
    fileprivate enum Layout { }
    
    lazy var loadingView = LoadingView()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.titleLabelFont
        label.textColor = Palette.ppColorGrayscale600.color
        label.textAlignment = .left
        label.numberOfLines = 0
        label.text = TFALocalizable.newOptionsTitleText.text
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.descriptionLabelFont
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = 0
        label.textAlignment = .left
        label.text = TFALocalizable.newOptionsDescriptionText.text
        return label
    }()
    
    private lazy var tableView: UITableView = {
        let table = UITableView()
        table.backgroundColor = Palette.ppColorGrayscale000.color
        table.separatorStyle = .none
        table.showsVerticalScrollIndicator = false
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = Layout.Height.cellEstimatedRowHeight
        table.register(TFAOptionCell.self, forCellReuseIdentifier: String(describing: TFAOptionCell.self))
        return table
    }()
    
    private var dataSource: TableViewHandler<String, TFAOptionCellViewModel, TFAOptionCell>?

    private lazy var fallbackView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()

    private lazy var fallbackMessageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .center)
        return label
    }()

    private lazy var fallbackActionButton: UIButton = {
        let button = UIButton()
        button.setTitle(TFALocalizable.tfaOptionsFallbackButton.text, for: .normal)
        button.addTarget(self, action: #selector(didTapFallbackButton), for: .touchUpInside)
        button.buttonStyle(LinkButtonStyle())
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadOptions()
        navigationItem.backBarButtonItem = .noContextTitlelessBackButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(tableView)
        view.addSubview(fallbackView)
        fallbackView.addSubview(fallbackMessageLabel)
        fallbackView.addSubview(fallbackActionButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        fallbackView.isHidden = true
    }
    
    override func setupConstraints() {
        titleLabel.layout {
            $0.top == view.topAnchor + Layout.Spacing.defaultOffset
            $0.leading == view.leadingAnchor + Layout.Spacing.defaultOffset
            $0.trailing == view.trailingAnchor - Layout.Spacing.defaultOffset
            $0.bottom == descriptionLabel.topAnchor - Layout.Spacing.defaultOffset
        }
        
        descriptionLabel.layout {
            $0.leading == view.leadingAnchor + Layout.Spacing.defaultOffset
            $0.trailing == view.trailingAnchor - Layout.Spacing.defaultOffset
            $0.bottom == tableView.topAnchor - Layout.Spacing.separatorViewTopOffset
        }
        
        tableView.layout {
            $0.leading == view.leadingAnchor + Layout.Spacing.defaultOffset
            $0.trailing == view.trailingAnchor - Layout.Spacing.defaultOffset
        }

        fallbackView.snp.makeConstraints {
            $0.top.equalTo(tableView.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base08)
        }

        fallbackMessageLabel.snp.makeConstraints {
            $0.leading.trailing.top.equalToSuperview()
            $0.bottom.equalTo(fallbackActionButton.snp_topMargin).offset(Spacing.base00)
        }

        fallbackActionButton.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
}

// MARK: View Model Outputs
extension NewTFAOptionsViewController: NewTFAOptionsDisplay {
    func displayOptions(_ options: [Section<String, TFAOptionCellViewModel>]) {
        dataSource = TableViewHandler(
            data: options,
            cellType: TFAOptionCell.self,
            configureCell: { row, cellViewModel, cell in
                cellViewModel.presenter.viewController = cell
                cell.tableRow = row
                cell.viewModel = cellViewModel
            }, configureDidSelectRow: { indexPath, _ in
                self.viewModel.selectedItem(indexPath)
            }
        )
        
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        tableView.reloadData()
    }

    func displayFallback(message: String) {
        fallbackMessageLabel.text = message
        fallbackView.isHidden = false
    }
    
    func displayLoading(_ loading: Bool) {
        loading ? startLoadingView() : stopLoadingView()
    }
    
    func displayError(alert: Alert) {
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func displayRetryLoadOptions() {
        viewModel.loadOptions()
    }
}

@objc
private extension NewTFAOptionsViewController {
    func didTapFallbackButton() {
        viewModel.selectFallback()
    }
}
