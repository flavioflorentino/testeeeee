public struct NewTFAOption: Decodable {
    enum CodingKeys: String, CodingKey {
        case flow
        case firstStep = "first_step"
        case iconUrl = "icon_url"
        case title
    }
    
    let flow: TFAType
    let firstStep: TFAStep
    let iconUrl: String
    let title: String
}

public struct NewTFAOptions: Decodable {
    let options: [NewTFAOption]
    let fallback: NewTFAOption?
}
