public protocol TFAOptionCellViewModelInputs: AnyObject {
    func configure()
}

final class TFAOptionCellViewModel {
    private let item: NewTFAOption
    private(set) var presenter: TFAOptionCellPresenting
    
    init(presenter: TFAOptionCellPresenting, item: NewTFAOption) {
        self.item = item
        self.presenter = presenter
    }
}

extension TFAOptionCellViewModel: TFAOptionCellViewModelInputs {
    func configure() {
        presenter.configureView(item: item)
    }
}
