public protocol TFAOptionCellDisplay: AnyObject {
    func configureView(with title: String, imageUrl: String, placeHolderImage: UIImage)
}
