import UIKit

public protocol TFAOptionCellPresenting: AnyObject {
    var viewController: TFAOptionCellDisplay? { get set }
    func configureView(item: NewTFAOption)
}

public final class TFAOptionCellPresenter: TFAOptionCellPresenting {
    public weak var viewController: TFAOptionCellDisplay?
    
    public func configureView(item: NewTFAOption) {
        viewController?.configureView(with: item.title, imageUrl: item.iconUrl, placeHolderImage: tfaIconFor(item.flow))
    }
    
    private func tfaIconFor(_ type: TFAType) -> UIImage {
        switch type {
        case .creditCard:
            return Assets.TwoFA.tfaCard.image
        case .sms:
            return Assets.TwoFA.tfaSms.image
        case .email:
            return Assets.TwoFA.tfaEmail.image
        default:
            return Assets.TwoFA.tfaDevice.image
        }
    }
}
