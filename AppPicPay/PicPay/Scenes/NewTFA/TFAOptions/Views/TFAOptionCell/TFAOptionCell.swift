import UI
import UIKit

private extension TFAOptionCell.Layout {
    enum Fonts {
        static let titleLabelFont = UIFont.systemFont(ofSize: 16, weight: .semibold)
    }
    
    enum Spacing {
        static let imageViewTopMargin: CGFloat = 16
        static let imageViewBottomMargin: CGFloat = 16
        static let imageViewTrailingMargin: CGFloat = 4
        static let titleLabelTrailingMargin: CGFloat = 8
    }
    
    enum Others {
        static let cornerRadius: CGFloat = 4
    }
    
    enum Width {
        static let imageViewWidth: CGFloat = 48
        static let disclosureIndicatorWidth: CGFloat = 20
    }
    
    enum Height {
        static let imageViewHeight: CGFloat = 48
        static let separatorViewHeight: CGFloat = 1
    }
}

final class TFAOptionCell: UITableViewCell {
    fileprivate enum Layout { }
    
    var viewModel: TFAOptionCellViewModel? {
        didSet {
            update()
        }
    }
    
    var tableRow: Int? {
        didSet {
            topSeparatorView.isHidden = self.tableRow == 0 ? false : true
        }
    }
    
    private lazy var topSeparatorView: UIView = {
        let view = UIView()
        view.isHidden = true
        view.backgroundColor = Palette.ppColorGrayscale200.color
        return view
    }()
    
    private lazy var tfaImageView = UIImageView()
    
    private lazy var tfaTitleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.titleLabelFont
        label.textColor = Palette.ppColorGrayscale600.color
        label.numberOfLines = 0
        label.textAlignment = .left
        return label
    }()
    
    private lazy var tfaDisclosureIndicatorView: UIImageView = {
        let imageView = UIImageView(image: Assets.Icons.rightArrow.image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var bottomSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale200.color
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addComponents()
        layoutComponents()
        selectionStyle = .none
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        addSubview(topSeparatorView)
        addSubview(tfaImageView)
        addSubview(tfaTitleLabel)
        addSubview(tfaDisclosureIndicatorView)
        addSubview(bottomSeparatorView)
    }
    
    private func layoutComponents() {
        topSeparatorView.layout {
            $0.height == Layout.Height.separatorViewHeight
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.top == topAnchor
            $0.bottom == tfaImageView.topAnchor - Layout.Spacing.imageViewTopMargin
        }
        
        tfaImageView.layout {
            $0.leading == leadingAnchor
            $0.trailing == tfaTitleLabel.leadingAnchor - Layout.Spacing.imageViewTrailingMargin
            $0.bottom == bottomSeparatorView.topAnchor - Layout.Spacing.imageViewBottomMargin
            $0.width == Layout.Width.imageViewWidth
            $0.height == Layout.Height.imageViewHeight
        }
        
        tfaTitleLabel.layout {
            $0.centerY == tfaImageView.centerYAnchor
            $0.trailing == tfaDisclosureIndicatorView.leadingAnchor - Layout.Spacing.titleLabelTrailingMargin
        }
        
        tfaDisclosureIndicatorView.layout {
            $0.centerY == tfaTitleLabel.centerYAnchor
            $0.width == Layout.Width.disclosureIndicatorWidth
            $0.trailing == trailingAnchor
        }
        
        bottomSeparatorView.layout {
            $0.height == Layout.Height.separatorViewHeight
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.bottom == bottomAnchor
        }
    }
    
    private func update() {
        viewModel?.configure()
    }
}

extension TFAOptionCell: TFAOptionCellDisplay {
    func configureView(with title: String, imageUrl: String, placeHolderImage: UIImage) {
        tfaTitleLabel.text = title
        tfaImageView.setImage(url: URL(string: imageUrl), placeholder: placeHolderImage)
    }
}
