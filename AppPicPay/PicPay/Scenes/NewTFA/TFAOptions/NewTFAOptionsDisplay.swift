import UI
import UIKit

protocol NewTFAOptionsDisplay: AnyObject {
    func displayOptions(_ options: [Section<String, TFAOptionCellViewModel>])
    func displayFallback(message: String)
    func displayLoading(_ loading: Bool)
    func displayError(alert: Alert)
    func displayRetryLoadOptions()
}
