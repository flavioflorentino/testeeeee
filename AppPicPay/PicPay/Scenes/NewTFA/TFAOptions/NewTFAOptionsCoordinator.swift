import UIKit

enum NewTFAOptionsAction {
    case selectedOption(option: NewTFAOption)
    case validationId(validationData: TFASelfieValidationData, option: NewTFAOption)
    case close
    case blocked
}

protocol NewTFAOptionsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: NewTFAOptionsCoordinatorDelegate? { get set }
    func perform(action: NewTFAOptionsAction)
}

protocol NewTFAOptionsCoordinatorDelegate: AnyObject {
    func didSelectOption(option: NewTFAOption)
    func didSelectValidationId(validationData: TFASelfieValidationData, option: NewTFAOption)
    func didReceivedBlockedError()
}

final class NewTFAOptionsCoordinator: NewTFAOptionsCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: NewTFAOptionsCoordinatorDelegate?

    func perform(action: NewTFAOptionsAction) {
        switch action {
        case .close:
            viewController?.navigationController?.popViewController(animated: true)
        case .selectedOption(let option):
            delegate?.didSelectOption(option: option)
        case let .validationId(validationData, option):
            delegate?.didSelectValidationId(validationData: validationData, option: option)
        case .blocked:
            delegate?.didReceivedBlockedError()
        }
    }
}
