import Core
import Foundation
import UI

protocol NewTFAOptionsPresenting: AnyObject {
    var viewController: NewTFAOptionsDisplay? { get set }
    func presentOptions(_ options: [NewTFAOption])
    func presentFallback(_ title: String)
    func presentNextStepFor(_ option: NewTFAOption)
    func presentLoading(_ loading: Bool)
    func presentError(error: RequestError)
    func presentSelfieValidationError(error: RequestError)
    func presentSelfieValidation(with validationData: TFASelfieValidationData, andOption option: NewTFAOption)
    func presentTFABlockedError(_ error: RequestError)
}

final class NewTFAOptionsPresenter: NewTFAOptionsPresenting {
    private let coordinator: NewTFAOptionsCoordinating
    weak var viewController: NewTFAOptionsDisplay?
    
    init(coordinator: NewTFAOptionsCoordinating) {
        self.coordinator = coordinator
    }
    
    func presentOptions(_ options: [NewTFAOption]) {
        let items = options.map { item -> TFAOptionCellViewModel in
            let presenter: TFAOptionCellPresenting = TFAOptionCellPresenter()
            return TFAOptionCellViewModel(presenter: presenter, item: item)
        }
        
        viewController?.displayOptions([Section(items: items)])
    }

    func presentFallback(_ title: String) {
        viewController?.displayFallback(message: title)
    }
    
    func presentLoading(_ loading: Bool) {
        self.viewController?.displayLoading(loading)
    }
    
    func presentNextStepFor(_ option: NewTFAOption) {
        coordinator.perform(action: .selectedOption(option: option))
    }
    
    func presentError(error: RequestError) {
        let tryAgainButton = Button(title: DefaultLocalizable.tryAgain.text, type: .cta) { popup, _ in
            popup.dismiss(animated: true) {
                self.viewController?.displayRetryLoadOptions()
            }
        }
        
        let backButton = Button(
            title: DefaultLocalizable.back.text,
            type: .cleanUnderlined) { popup, _ in
                popup.dismiss(animated: true) {
                    self.coordinator.perform(action: .close)
                }
        }
        
        let alert = Alert(
            with: TFALocalizable.tfaOptionsListNotLoadedAlertTitle.text,
            text: TFALocalizable.tfaOptionsListNotLoadedAlertMessage.text,
            buttons: [tryAgainButton, backButton],
            image: nil
        )
        
        viewController?.displayError(alert: alert)
    }
    
    func presentTFABlockedError(_ error: RequestError) {
        let okButton = Button(title: DefaultLocalizable.btOkUnderstood.text, type: .cta) { popup, _ in
            popup.dismiss(animated: true) {
                self.coordinator.perform(action: .blocked)
            }
        }
        
        let alert = Alert(
            with: error.title,
            text: error.message,
            buttons: [okButton],
            image: nil
        )
        
        viewController?.displayError(alert: alert)
    }
    
    func presentSelfieValidationError(error: RequestError) {
        let alert = Alert(title: error.title, text: error.message)
        viewController?.displayError(alert: alert)
    }
    
    func presentSelfieValidation(with validationData: TFASelfieValidationData, andOption option: NewTFAOption) {
        coordinator.perform(action: .validationId(validationData: validationData, option: option))
    }
}
