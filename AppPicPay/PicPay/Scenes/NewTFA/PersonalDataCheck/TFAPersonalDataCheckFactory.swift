import Foundation

enum TFAPersonalDataCheckFactory {
    static func make(
        with coordinatorDelegate: TFAPersonalDataCheckCoordinatorDelegate,
        tfaRequestData: TFARequestData
    ) -> TFAPersonalDataCheckViewController {
        let container = DependencyContainer()
        let service: TFAPersonalDataCheckServicing = TFAPersonalDataCheckService(dependencies: container)
        let coordinator: TFAPersonalDataCheckCoordinating = TFAPersonalDataCheckCoordinator(dependencies: container)
        let presenter: TFAPersonalDataCheckPresenting = TFAPersonalDataCheckPresenter(coordinator: coordinator)
        let viewModel = TFAPersonalDataCheckViewModel(
            service: service,
            presenter: presenter,
            tfaRequestData: tfaRequestData,
            dependencies: container
        )
        let viewController = TFAPersonalDataCheckViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        presenter.viewController = viewController

        return viewController
    }
}
