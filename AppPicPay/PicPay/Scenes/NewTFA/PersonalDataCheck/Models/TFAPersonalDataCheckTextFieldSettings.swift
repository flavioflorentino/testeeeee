import Foundation
import Validator

struct TFAPersonalDataCheckTextFieldSettings {
    let document: TextfieldSetting
    let birthdate: TextfieldSetting
    let mothersName: TextfieldSetting
    let email: TextfieldSetting
}

struct TFAPersonalDataCheckInfo {
    let cpfDocument: String
    let birthDate: String
    let mothersName: String
    let email: String
    let tfaId: String
    let channel: TFAType
    let code: String?
}

enum TFAPersonalDataFieldCheckValidationError: String, CustomStringConvertible, Error, ValidationError {
    case birthdateRequired = "Data de Nascimento obrigatória"
    case invalidBirthdate = "Data de Nascimento inválida"
    case invalidCPFDocument = "CPF inválido"
    case invalidEmail = "Email inválido"
    case motherNameRequired = "Nome da mãe obrigátorio"
    
    var message: String {
        self.rawValue
    }
    
    var description: String {
        self.rawValue
    }
}
