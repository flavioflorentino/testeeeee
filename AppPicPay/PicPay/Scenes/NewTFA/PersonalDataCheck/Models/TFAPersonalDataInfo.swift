import Foundation

struct TFAPersonalDataInfo: Decodable {    
    let consumerData: ConsumerData
}

struct ConsumerData: Decodable {
    let email: String
}
