import Core

protocol TFAPersonalDataCheckServicing {
    func getPersonalData(tfaRequestData: TFARequestData, completion: @escaping (Result<TFAPersonalDataInfo, ApiError>) -> Void)
    func validatePersonalData(
        data: TFAPersonalDataCheckInfo,
        tfaRequestData: TFARequestData,
        completion: @escaping (Result<TFAValidationResponse, ApiError>) -> Void
    )
}

final class TFAPersonalDataCheckService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension TFAPersonalDataCheckService: TFAPersonalDataCheckServicing {
    func getPersonalData(tfaRequestData: TFARequestData, completion: @escaping (Result<TFAPersonalDataInfo, ApiError>) -> Void) {
        let endpoint = DynamicTFAEndpoint.getPersonalData(tfaRequestData: tfaRequestData)
        let api = Api<TFAPersonalDataInfo>(endpoint: endpoint)
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
    
    func validatePersonalData(
        data: TFAPersonalDataCheckInfo,
        tfaRequestData: TFARequestData,
        completion: @escaping (Result<TFAValidationResponse, ApiError>) -> Void
    ) {
        let api = Api<TFAValidationResponse>(
            endpoint: DynamicTFAEndpoint.validatePersonalData(personalData: data, tfaRequestData: tfaRequestData)
        )
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { [weak self] result in
            self?.dependencies.mainQueue.async {
                completion(result.map(\.model))
            }
        }
    }
}
