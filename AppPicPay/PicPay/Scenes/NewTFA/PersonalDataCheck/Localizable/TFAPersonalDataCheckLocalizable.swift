import Validator

enum TFAPersonalDataCheckLocalizable: String, Error, CustomStringConvertible, Localizable {
    case header
    case descriptionText
    case cpfPlaceholder
    case birthdatePlaceholder
    case motherNamePlaceholder
    case emailPlaceholder
    case cpfDocumentMask
    case birthDateMask
    case personalDataErrorAlertTitle
    case personalDataErrorAlertMessage
    case verifyInfo
    case unexpectedError
    
    var key: String {
        self.rawValue
    }
    var file: LocalizableFile {
        .tfaPersonalDataCheck
    }
    
    var description: String {
        self.rawValue
    }
}
