import AnalyticsModule
import Core
import Foundation
import Validations

protocol TFAPersonalDataCheckViewModelInputs: AnyObject {
    func didLoadView()
    func getPersonalDataInfo()
    func emailBeginEditing()
    func checkMotherName(with text: String)
    func checkEmail(with text: String)
    func checkDocument(with text: String)
    func checkBirthday(with text: String)
    func validateForm(with document: String, birthDate: String, motherName: String, email: String)
    func startTimerForNewValidation(retryTime: Int)
}

final class TFAPersonalDataCheckViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies
    
    private let service: TFAPersonalDataCheckServicing
    private let presenter: TFAPersonalDataCheckPresenting
    private let tfaRequestData: TFARequestData
    
    private var emailHint: String?
    private var document = ""
    private var birthdate = ""
    private var motherName = ""
    private var email = ""
    
    private var timer: TFATimerProtocol
    private var countdownSeconds = 180
    
    init(
        service: TFAPersonalDataCheckServicing,
        presenter: TFAPersonalDataCheckPresenting,
        tfaRequestData: TFARequestData,
        dependencies: Dependencies,
        timer: TFATimerProtocol = TFATimer()
    ) {
        self.service = service
        self.presenter = presenter
        self.tfaRequestData = tfaRequestData
        self.dependencies = dependencies
        self.timer = timer
        self.timer.delegate = self
    }
}

extension TFAPersonalDataCheckViewModel: TFAPersonalDataCheckViewModelInputs {
    func didLoadView() {
        dependencies.analytics.log(TFAEvents.validatePersonalData)
    }
    
    func getPersonalDataInfo() {
        presenter.presentLoading(true)
        service.getPersonalData(tfaRequestData: tfaRequestData) { [weak self] result in
            self?.presenter.presentLoading(false)
            if case let .success(info) = result {
                self?.emailHint = info.consumerData.email
            }
        }
    }
    
    func emailBeginEditing() {
        guard let hint = emailHint else {
            return
        }
        presenter.updateEmailHint(with: hint)
    }
    
    func checkMotherName(with text: String) {
        let validText = text.replacingOccurrences(of: "[^a-zA-Z\u{00C0}-\u{00FF} ]", with: "", options: .regularExpression)
        motherName = validText
        let validName = isValidMotherName(motherName) == nil
        
        presenter.updateMotherNameField(
            withError: validName ? nil : TFAPersonalDataFieldCheckValidationError.motherNameRequired.message
        )
        checkFieldsAndEnableContinue()
    }
    
    func checkEmail(with text: String) {
        email = text
        let validEmail = isValidEmail(email)
        
        presenter.updateEmailField(
            withError: validEmail ? nil : TFAPersonalDataFieldCheckValidationError.invalidEmail.message
        )
        checkFieldsAndEnableContinue()
    }
    
    func checkDocument(with text: String) {
        document = text
        let validDocument = isValidDocumet(document)
        
        presenter.updateDocumentField(
            withError: validDocument ? nil : TFAPersonalDataFieldCheckValidationError.invalidCPFDocument.message
        )
        checkFieldsAndEnableContinue()
    }
    
    func checkBirthday(with text: String) {
        birthdate = text
        
        switch isValidBirthdateLenght(birthdate) {
        case .incompleteData:
            presenter.updateBirthdateField(withError: TFAPersonalDataFieldCheckValidationError.birthdateRequired.message)
        case .invalidData:
            presenter.updateBirthdateField(withError: TFAPersonalDataFieldCheckValidationError.invalidBirthdate.message)
        case .none:
            isValidDate(birthdate)
                ? presenter.updateBirthdateField(withError: nil)
                : presenter.updateBirthdateField(withError: TFAPersonalDataFieldCheckValidationError.invalidBirthdate.message)
        }
        
        checkFieldsAndEnableContinue()
    }
    
    func validateForm(with document: String, birthDate: String, motherName: String, email: String) {
        let tfaData = TFAPersonalDataCheckInfo(
            cpfDocument: document,
            birthDate: birthDate,
            mothersName: motherName,
            email: email,
            tfaId: tfaRequestData.id,
            channel: tfaRequestData.flow,
            code: nil
        )
        
        validateFormForNextStep(tfaData: tfaData)
    }
    
    func startTimerForNewValidation(retryTime: Int) {
        countdownSeconds = retryTime
        timer.start()
    }
}

extension TFAPersonalDataCheckViewModel: TFATimerDelegate {
    func timerTic() {
        if countdownSeconds <= 0 {
            timer.stop()
        } else {
            countdownSeconds -= 1
        }
        presenter.presentContinueButtonUpdated(with: countdownSeconds)
    }
}

private extension TFAPersonalDataCheckViewModel {
    func isValidEmail(_ email: String) -> Bool {
        let validator = RegexValidator(descriptor: AccountRegexDescriptor.email)
        do {
            try validator.validate(email)
            return true
        } catch {
            return false
        }
    }
    
    func isValidMotherName(_ motherName: String) -> GenericValidationError? {
        let validator = RangeValidator(minCount: 1, maxCount: nil)
        do {
            try validator.validate(motherName)
            return nil
        } catch {
            return .incompleteData
        }
    }
    
    func isValidBirthdateLenght(_ birthdate: String) -> GenericValidationError? {
        let minRangeValidator = RangeValidator(minCount: 1, maxCount: 10)
        do {
            try minRangeValidator.validateForInterval(birthdate)
            return nil
        } catch {
            return error as? GenericValidationError
        }
    }
    
    func isValidDate(_ dateString: String) -> Bool {
        let currentDate = Date()
        
        guard
            let date = DateFormatter(style: .notLenient).date(from: dateString),
            date < currentDate else {
            return false
        }

        return true
    }
    
    func isValidDocumet(_ document: String) -> Bool {
        let validator = CPFValidator()
        do {
            try validator.validate(document)
            return true
        } catch {
            return false
        }
    }
    
    func checkFieldsAndEnableContinue() {
        let validBirthdate = isValidBirthdateLenght(birthdate) == nil && isValidDate(birthdate)
        let validMotherName = isValidMotherName(motherName) == nil
        let enable = isValidEmail(email) && isValidDocumet(document) && validBirthdate && validMotherName
        presenter.shouldEnableContinueButton(enable: enable)
    }
    
    func validateFormForNextStep(tfaData: TFAPersonalDataCheckInfo) {
        presenter.presentLoading(true)
        
        service.validatePersonalData(
            data: tfaData,
            tfaRequestData: tfaRequestData
        ) { [weak self] (result: Result<TFAValidationResponse, ApiError>) in
            self?.presenter.presentLoading(false)
            
            switch result {
            case .success(let response):
                self?.didValidateData(for: response.nextStep)
            case .failure(let error):
                self?.parseError(error: error)
            }
        }
    }
    
    func didValidateData(for nextStep: TFAStep) {
        self.dependencies.analytics.log(TFAEvents.didValidateData(tfaRequestData.flow))
        self.presenter.didValidatePersonalData(with: tfaRequestData.flow, nextStep: nextStep)
    }
    
    func parseError(error: ApiError) {
        switch error {
        case .tooManyRequests(let requestError), .unauthorized(let requestError), .badRequest(let requestError):
            guard
                let errorData = requestError.data?.dictionaryObject?["data"] as? [String: Any],
                let retryTime = errorData["retry_time"] as? Int
                else {
                    presenter.presentValidationInfoError(tfaError: requestError)
                    return
            }
            
            dependencies.analytics.log(
                TFAEvents.didReceivedExceededDataValidationLimitError(
                    tfaType: tfaRequestData.flow,
                    currentStep: tfaRequestData.step
                )
            )
            presenter.presentAttempLimitError(tfaError: requestError, retryTime: retryTime, tfaType: tfaRequestData.flow)
        default:
            presenter.presentGenericError()
        }
    }
}
