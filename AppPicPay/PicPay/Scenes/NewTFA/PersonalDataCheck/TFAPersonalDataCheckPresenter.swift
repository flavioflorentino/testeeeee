import AnalyticsModule
import Core
import Foundation
import UI

protocol TFAPersonalDataCheckPresenting: AnyObject {
    var viewController: TFAPersonalDataCheckDisplay? { get set }
    func presentLoading(_ loading: Bool)
    func shouldEnableContinueButton(enable: Bool)
    func didValidatePersonalData(with type: TFAType, nextStep: TFAStep)
    func presentContinueButtonUpdated(with remainingTime: Int)
    func presentValidationInfoError(tfaError: RequestError)
    func presentAttempLimitError(tfaError: RequestError, retryTime: Int, tfaType: TFAType)
    func presentGenericError()
    func updateEmailHint(with hint: String)
    func updateEmailField(withError message: String?)
    func updateDocumentField(withError message: String?)
    func updateBirthdateField(withError message: String?)
    func updateMotherNameField(withError message: String?)
}

final class TFAPersonalDataCheckPresenter: TFAPersonalDataCheckPresenting {
    private let coordinator: TFAPersonalDataCheckCoordinating
    var viewController: TFAPersonalDataCheckDisplay?
    private let tfaTimerformatter = TFATimerFormatter()

    init(coordinator: TFAPersonalDataCheckCoordinating) {
        self.coordinator = coordinator
    }
    
    func presentLoading(_ loading: Bool) {
        viewController?.displayLoading(loading: loading)
    }
    
    func shouldEnableContinueButton(enable: Bool) {
        viewController?.enableContinueButton(enable: enable)
    }
    
    func didValidatePersonalData(with type: TFAType, nextStep: TFAStep) {
        coordinator.perform(action: .validateDataAndStartNextStep(tfaType: type, nextStep: nextStep))
    }
    
    func presentGenericError() {
        let alert = Alert(
            with: TFALocalizable.genericErrorTitle.text,
            text: TFALocalizable.genericErrorMessage.text,
            buttons: [
                Button(title: DefaultLocalizable.back.text, type: .cta)
            ]
        )
        viewController?.displayGenericError(alert: alert)
    }
    
    func presentValidationInfoError(tfaError: RequestError) {
        let alert = Alert(
            title: tfaError.title,
            text: tfaError.message
        )
        alert.buttons = [
            Button(title: DefaultLocalizable.back.text, type: .cta)
        ]
        viewController?.displayValidationInfoError(alert: alert)
    }
    
    func presentAttempLimitError(tfaError: RequestError, retryTime: Int, tfaType: TFAType) {
        let alert = Alert(
            title: tfaError.title,
            text: tfaError.message
        )
        alert.buttons = [
            Button(title: DefaultLocalizable.back.text, type: .cta)
        ]
        
        viewController?.displayAttempLimitError(alert: alert, retryTime: retryTime)
    }
 
    func presentContinueButtonUpdated(with remainingTime: Int) {
        let formatedTime = tfaTimerformatter.timeString(with: TimeInterval(remainingTime))
        let buttonText = "\(DefaultLocalizable.wait.text) \(formatedTime)"
        viewController?.displayContinueButton(with: buttonText, enabled: remainingTime <= 0)
    }
    
    func updateEmailHint(with hint: String) {
        viewController?.displayEmailHint(hint)
    }
    
    func updateEmailField(withError message: String?) {
        viewController?.updateEmailFieldErrorMessage(message)
    }
    
    func updateDocumentField(withError message: String?) {
        viewController?.updateDocumentFieldErrorMessage(message)
    }
    
    func updateBirthdateField(withError message: String?) {
        viewController?.updateBirthdateFieldErrorMessage(message)
    }
    
    func updateMotherNameField(withError message: String?) {
        viewController?.updateMotherNameFieldErrorMessage(message)
    }
}
