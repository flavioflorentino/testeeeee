import UI

protocol TFAPersonalDataCheckDisplay: AnyObject {
    func enableContinueButton(enable: Bool)
    func displayLoading(loading: Bool)
    func displayValidationInfoError(alert: Alert)
    func displayAttempLimitError(alert: Alert, retryTime: Int)
    func displayGenericError(alert: Alert)
    func displayContinueButton(with currentRemainingTime: String, enabled: Bool)
    func displayEmailHint(_ hint: String)
    func updateEmailFieldErrorMessage(_ message: String?)
    func updateDocumentFieldErrorMessage(_ message: String?)
    func updateBirthdateFieldErrorMessage(_ message: String?)
    func updateMotherNameFieldErrorMessage(_ message: String?)
}
