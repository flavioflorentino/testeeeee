import UI
import UIKit

private extension TFAPersonalDataCheckErrorView.Layout {
    enum Fonts {
        static let errorLabelFont = UIFont.systemFont(ofSize: 10, weight: .medium)
    }
    
    enum Spacing {
        static let errorLabelPadding: CGFloat = 12
    }
    
    enum Others {
        static let cornerRadius: CGFloat = 4
    }
}

final class TFAPersonalDataCheckErrorView: UIView {
    fileprivate enum Layout { }
    
    private lazy var errorTextLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale000.color
        label.font = Layout.Fonts.errorLabelFont
        label.textAlignment = .left
        label.text = TFAPersonalDataCheckLocalizable.verifyInfo.text
        return label
    }()
    
    init() {
        super.init(frame: .zero)
        addComponents()
        layoutComponents()
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        addSubview(errorTextLabel)
    }
    
    private func layoutComponents() {
        errorTextLabel.layout {
            $0.top == topAnchor + Layout.Spacing.errorLabelPadding
            $0.bottom == bottomAnchor - Layout.Spacing.errorLabelPadding
            $0.leading == leadingAnchor + Layout.Spacing.errorLabelPadding
            $0.trailing == trailingAnchor - Layout.Spacing.errorLabelPadding
        }
    }
    
    private func setup() {
        backgroundColor = Palette.ppColorNegative300.color
        layer.cornerRadius = Layout.Others.cornerRadius
    }
}
