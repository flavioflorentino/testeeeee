import AnalyticsModule
import SnapKit
import UI
import UIKit

private extension TFAPersonalDataCheckViewController.Layout {
    enum Height {
        static let buttonHeight: CGFloat = 44
    }
}

final class TFAPersonalDataCheckViewController: ViewController<TFAPersonalDataCheckViewModelInputs, UIView>, LoadingViewProtocol {
    fileprivate enum Layout { }
    fileprivate enum Constants {
        static let maskedCpfDocumentLength = 14
        static let maskedBirthdateLength = 10
    }
    
    lazy var loadingView = LoadingView()
    
    private lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.backgroundColor = .clear
        return scroll
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
        label.text = TFAPersonalDataCheckLocalizable.header.text
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .default))
            .with(\.textColor, .grayscale750())
            .with(\.textAlignment, .left)
        label.text = TFAPersonalDataCheckLocalizable.descriptionText.text
        return label
    }()
    
    private lazy var documentTextfield: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.defaultForm()
        textfield.selectedTitleColor = Colors.grayscale900.color
        textfield.placeholder = TFAPersonalDataCheckLocalizable.cpfPlaceholder.text
        textfield.addTarget(self, action: #selector(checkDocumentTextField), for: .editingChanged)
        textfield.keyboardType = .numberPad
        textfield.delegate = self
        return textfield
    }()
    
    private lazy var documentMasker: TextFieldMasker = {
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf)
        let masker = TextFieldMasker(textMask: mask)
        return masker
    }()
    
    private lazy var birthdateTextfield: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.defaultForm()
        textfield.selectedTitleColor = Colors.grayscale900.color
        textfield.placeholder = TFAPersonalDataCheckLocalizable.birthdatePlaceholder.text
        textfield.addTarget(self, action: #selector(checkBirthdayTextField), for: .editingChanged)
        textfield.keyboardType = .numberPad
        textfield.delegate = self
        return textfield
    }()
    
    private lazy var birthDateMasker: TextFieldMasker = {
        let mask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.date)
        let masker = TextFieldMasker(textMask: mask)
        return masker
    }()
    
    private lazy var motherNameTextfield: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.defaultForm()
        textfield.selectedTitleColor = Colors.grayscale900.color
        textfield.placeholder = TFAPersonalDataCheckLocalizable.motherNamePlaceholder.text
        textfield.addTarget(self, action: #selector(checkMotherNameTextfield), for: .editingChanged)
        textfield.delegate = self
        return textfield
    }()
    
    private lazy var emailTextfield: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.defaultForm()
        textfield.selectedTitleColor = Colors.grayscale900.color
        textfield.title = TFAPersonalDataCheckLocalizable.emailPlaceholder.text
        textfield.placeholder = TFAPersonalDataCheckLocalizable.emailPlaceholder.text
        textfield.keyboardType = .emailAddress
        textfield.addTarget(self, action: #selector(checkEmailTextField), for: .editingChanged)
        textfield.addTarget(self, action: #selector(emailEditingBegan(_:)), for: .editingDidBegin)
        textfield.delegate = self
        return textfield
    }()
    
    private lazy var continueButton: UIButton = {
        let button = UIButton()
        button.setTitle(DefaultLocalizable.next.text, for: .normal)
        button.addTarget(self, action: #selector(didTapContinueButton), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        button.isEnabled = false
        return button
    }()
    
    private lazy var errorView: TFAPersonalDataCheckErrorView = {
        let errorV = TFAPersonalDataCheckErrorView()
        errorV.isHidden = true
        return errorV
    }()
    
    private lazy var topStackView: UIStackView = {
        let stack = UIStackView()
        stack.alignment = .leading
        stack.axis = .vertical
        stack.spacing = Spacing.base02
        return stack
    }()
    
    private lazy var keyboardScrollViewHandler = KeyboardScrollViewHandler(scrollView: scrollView)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        keyboardScrollViewHandler.registerForKeyboardNotifications()
        viewModel.didLoadView()
        viewModel.getPersonalDataInfo()
        documentMasker.bind(to: documentTextfield)
        birthDateMasker.bind(to: birthdateTextfield)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(documentTextfield)
        contentView.addSubview(birthdateTextfield)
        contentView.addSubview(motherNameTextfield)
        contentView.addSubview(emailTextfield)
        contentView.addSubview(continueButton)
        topStackView.addArrangedSubview(titleLabel)
        topStackView.addArrangedSubview(descriptionLabel)
        topStackView.addArrangedSubview(errorView)
        contentView.addSubview(topStackView)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        navigationItem.backBarButtonItem = .noContextTitlelessBackButton
    }
    
    override func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalTo(view)
        }
        
        contentView.snp.makeConstraints {
            $0.edges.equalTo(scrollView)
            $0.width.equalTo(view.snp.width)
        }
        
        errorView.snp.makeConstraints {
            $0.leading.trailing.equalTo(contentView).inset(Spacing.base02)
        }
        
        topStackView.snp.makeConstraints {
            $0.top.equalTo(contentView.snp.top).offset(Spacing.base02)
            $0.leading.trailing.equalTo(contentView).inset(Spacing.base02)
        }
        
        documentTextfield.snp.makeConstraints {
            $0.top.equalTo(topStackView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalTo(contentView).inset(Spacing.base02)
        }
        
        birthdateTextfield.snp.makeConstraints {
            $0.top.equalTo(documentTextfield.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalTo(contentView).inset(Spacing.base02)
        }
        
        motherNameTextfield.snp.makeConstraints {
            $0.top.equalTo(birthdateTextfield.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalTo(contentView).inset(Spacing.base02)
        }
        
        emailTextfield.snp.makeConstraints {
            $0.top.equalTo(motherNameTextfield.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalTo(contentView).inset(Spacing.base02)
        }
        
        continueButton.snp.makeConstraints {
            $0.bottom.equalTo(contentView.snp.bottom).offset(-Spacing.base02)
            $0.leading.trailing.equalTo(contentView).inset(Spacing.base02)
            $0.top.equalTo(emailTextfield.snp.bottom).offset(Spacing.base04)
            $0.height.equalTo(Layout.Height.buttonHeight)
        }
    }
    
    @objc
    private func emailEditingBegan(_ sender: UIPPFloatingTextField) {
        viewModel.emailBeginEditing()
    }
    
    @objc
    func checkMotherNameTextfield() {
        viewModel.checkMotherName(with: motherNameTextfield.text ?? "")
    }
    
    @objc
    func checkDocumentTextField() {
        viewModel.checkDocument(with: documentTextfield.text ?? "")
    }
    
    @objc
    func checkBirthdayTextField() {
        viewModel.checkBirthday(with: birthdateTextfield.text ?? "")
    }
    
    @objc
    func checkEmailTextField() {
        viewModel.checkEmail(with: emailTextfield.text ?? "")
    }
    
    @objc
    func didTapContinueButton() {
        view?.endEditing(true)
        errorView.isHidden = true
        viewModel.validateForm(
            with: documentTextfield.text ?? "",
            birthDate: birthdateTextfield.text ?? "",
            motherName: motherNameTextfield.text ?? "",
            email: emailTextfield.text ?? ""
        )
    }
    
    private func enableFields(enable: Bool) {
        documentTextfield.isEnabled = enable
        birthdateTextfield.isEnabled = enable
        motherNameTextfield.isEnabled = enable
        emailTextfield.isEnabled = enable
    }
    
    private func enableValidation() {
        continueButton.isEnabled = true
        continueButton.setTitle(DefaultLocalizable.next.text, for: .disabled)
        enableFields(enable: true)
    }
    
    private func updateButtonState(enabled: Bool) {
        continueButton.setTitle(DefaultLocalizable.next.text, for: .normal)
        continueButton.setTitle(DefaultLocalizable.next.text, for: .disabled)
        continueButton.isEnabled = enabled
    }
}

// MARK: View Model Outputs
extension TFAPersonalDataCheckViewController: TFAPersonalDataCheckDisplay {
    func updateEmailFieldErrorMessage(_ message: String?) {
        emailTextfield.errorMessage = message
    }
    
    func updateDocumentFieldErrorMessage(_ message: String?) {
        documentTextfield.errorMessage = message
    }
    
    func updateBirthdateFieldErrorMessage(_ message: String?) {
        birthdateTextfield.errorMessage = message
    }
    
    func updateMotherNameFieldErrorMessage(_ message: String?) {
        motherNameTextfield.errorMessage = message
    }
    
    func enableContinueButton(enable: Bool) {
        updateButtonState(enabled: enable)
    }
    
    func displayLoading(loading: Bool) {
        loading ? startLoadingView() : stopLoadingView()
    }
    
    func displayValidationInfoError(alert: Alert) {
        self.errorView.isHidden = false
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func displayAttempLimitError(alert: Alert, retryTime: Int) {
        viewModel.startTimerForNewValidation(retryTime: retryTime)
        
        self.enableFields(enable: false)
        self.continueButton.isEnabled = false
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func displayGenericError(alert: Alert) {
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func displayContinueButton(with currentRemainingTime: String, enabled: Bool) {
        enabled ? self.enableValidation() : self.continueButton.setTitle(currentRemainingTime, for: .disabled)
    }
    
    func displayEmailHint(_ hint: String) {
        emailTextfield.setTitleVisible(true)
        emailTextfield.placeholder = hint
    }
}

extension TFAPersonalDataCheckViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isEmpty {
            return true
        }
        
        guard let text = textField.text else {
            return false
        }
        
        switch textField {
        case documentTextfield:
            return text.count < Constants.maskedCpfDocumentLength
        case birthdateTextfield:
            return text.count < Constants.maskedBirthdateLength
        default:
            return true
        }
    }
}
