import UIKit

enum TFAPersonalDataCheckAction: Equatable {
    case validateDataAndStartNextStep(tfaType: TFAType, nextStep: TFAStep)
}

protocol TFAPersonalDataCheckCoordinatorDelegate: AnyObject {
    func didValidatePersonalData(with tfaType: TFAType, nextStep: TFAStep)
}

protocol TFAPersonalDataCheckCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: TFAPersonalDataCheckCoordinatorDelegate? { get set }
    func perform(action: TFAPersonalDataCheckAction)
}

final class TFAPersonalDataCheckCoordinator: TFAPersonalDataCheckCoordinating {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies
    
    weak var viewController: UIViewController?
    weak var delegate: TFAPersonalDataCheckCoordinatorDelegate?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func perform(action: TFAPersonalDataCheckAction) {
        if case let .validateDataAndStartNextStep(tfaType, nextStep) = action {
            delegate?.didValidatePersonalData(with: tfaType, nextStep: nextStep)
        }
    }
}
