import Foundation
import AnalyticsModule

enum TFAEvents: AnalyticsKeyProtocol {
    case started
    case helpAccessed
    case didReceivedExceededDataValidationLimitError(tfaType: TFAType, currentStep: TFAStep)
    case exceededCodeValidationTime(type: TFAType)
    case didValidateData(TFAType)
    case validatePersonalData
    case didValidatePhoneNumber(TFAType)
    case didValidateBankAccount(TFAType)
    case deviceAuthorized(TFAType)
    case codeResended(TFAType)
    case authorizationMethodsViewed([TFAType])
    case didSelectBackButton(step: TFAStep)
    case didConfirmBackButton(step: TFAStep)
    case didValidateCard(TFAType)
    case didReceivedIdentityStatus(TFAStatus)
    
    private var name: String {
        switch self {
        case .started:
            return "Device Authorization - Started"
        case .helpAccessed:
            return "Device Authorization - Help Accessed"
        case .didReceivedExceededDataValidationLimitError:
            return "Device Authorization - Limit Exceeded"
        case .exceededCodeValidationTime:
            return "Device Authorization - Time Exceeded"
        case .didValidateData:
            return "Device Authorization - Data Validated"
        case .validatePersonalData:
            return "Device Authorization - Validar Dados Pessoais"
        case .didValidatePhoneNumber:
            return "Device Authorization - Phone Number Validated"
        case .didValidateBankAccount:
            return "Device Authorization - Bank Account Validated"
        case .deviceAuthorized:
            return "Device Authorization - Device Authorized"
        case .codeResended:
            return "Device Authorization - Code Resended"
        case .authorizationMethodsViewed:
            return "Device Authorization - Authorization Methods Viewed"
        case .didSelectBackButton:
            return "Device Authorization - Back button selected"
        case .didConfirmBackButton:
            return "Device Authorization - Back button confirmed"
        case .didValidateCard:
            return "Device Authorization - Card Validated"
        case .didReceivedIdentityStatus(let status):
            return eventName(for: status)
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .didReceivedExceededDataValidationLimitError(type, step):
            let typeName = description(for: type)
            let stepName = description(for: step)
            return ["authorization_type": "\(typeName) - \(stepName)"]
        case .exceededCodeValidationTime(let type):
            let typeName = description(for: type)
            return ["authorization_type": typeName]
        case .didValidateData(let type):
            let typeName = description(for: type)
            return ["authorization_type": typeName]
        case .didValidatePhoneNumber(let type):
            let typeName = description(for: type)
            return ["authorization_type": typeName]
        case .didValidateBankAccount(let type):
            let typeName = description(for: type)
            return ["authorization_type": typeName]
        case .deviceAuthorized(let type):
            let typeName = description(for: type)
            return ["authorization_type": typeName]
        case .codeResended(let type):
            let typeName = description(for: type)
            return ["authorization_type": typeName]
        case .authorizationMethodsViewed(let types):
            let typesDescription = types.map(description(for:))
            return ["methods_showed": typesDescription.description]
        case .didSelectBackButton(let step):
            let stepName = description(for: step)
            return ["step": stepName]
        case .didConfirmBackButton(let step):
            let stepName = description(for: step)
            return ["step": stepName]
        case .didValidateCard(let type):
            let typeName = description(for: type)
            return ["authorization_type": typeName]
        default:
            return [:]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [AnalyticsProvider.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
    
    private func description(for type: TFAType) -> String {
        switch type {
        case .authorizedDevice, .device:
            return "PREVIOUS DEVICE"
        case .sms:
            return "SMS"
        case .email:
            return "FALLBACK"
        case .creditCard:
            return "CREDIT CARD"
        default:
            return ""
        }
    }
    
    private func description(for step: TFAStep) -> String {
        switch step {
        case .userData:
            return "USER DATA"
        case .bankData:
            return "BANK DATA"
        case .phoneNumber:
            return "PHONE NUMBER"
        case .code:
            return "CODE"
        case .creditCard:
            return "CARD DATA"
        default:
            return ""
        }
    }
    
    private func eventName(for identityStatus: TFAStatus) -> String {
        switch identityStatus {
        case .approved:
            return "Device Authorization - Identity Verified"
        case .inconclusive:
            return "Device Authorization - Identity Inconclusive"
        case .waitingAnalysis:
            return "Device Authorization - Identity To Verify"
        case .denied:
            return "Device Authorization - Identity Rejected"
        }
    }
}
