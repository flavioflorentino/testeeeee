import Foundation

enum NewTFAOnboardingFactory {
    static func make(with coordinatorDelegate: NewTFAOnboardingCoordinatorDelegate) -> NewTFAOnboardingViewController {
        let coordinator: NewTFAOnboardingCoordinating = NewTFAOnboardingCoordinator()
        let presenter: NewTFAOnboardingPresenting = NewTFAOnboardingPresenter(coordinator: coordinator)
        let viewModel = NewTFAOnboardingViewModel(presenter: presenter)
        let viewController = NewTFAOnboardingViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate

        return viewController
    }
}
