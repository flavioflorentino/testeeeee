import AnalyticsModule
import UI
import UIKit

private extension NewTFAOnboardingViewController.Layout {
    enum Fonts {
        static let titleLabelFont = UIFont.boldSystemFont(ofSize: 28)
        static let descriptionLabelFont = UIFont.systemFont(ofSize: 16, weight: .light)
        static let continueButtonFont = UIFont.boldSystemFont(ofSize: 15)
        static let informationButtonFont = UIFont.systemFont(ofSize: 14, weight: .light)
    }

    enum Spacing {
        static let defaultSpacing: CGFloat = 20
        static let closeButtonLeadingMargin: CGFloat = 10
        static let informationButtonBottomMargin: CGFloat = 20
    }

    enum Width {
        static let closeButtonWidth: CGFloat = 40
        static let onboardingImageWidth: CGFloat = 128
    }
    
    enum Height {
        static let closeButtonHeight: CGFloat = 40
        static let continueButtonHeight: CGFloat = 44
        static let informationButtonHeight: CGFloat = 40
        static let onboardingImageHeight: CGFloat = 96
    }
}

final class NewTFAOnboardingViewController: ViewController<NewTFAOnboardingViewModelInputs, UIView> {
    fileprivate enum Layout { }

    private lazy var closeButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.Savings.iconRoundCloseGreen.image, for: .normal)
        button.addTarget(self, action: #selector(closeButtonTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var centerStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .center
        stack.distribution = .fill
        stack.spacing = Layout.Spacing.defaultSpacing
        return stack
    }()
    
    private lazy var onboardingImage = UIImageView(image: #imageLiteral(resourceName: "ilu_2fa_onboarding"))
    
    private lazy var titleTextLabel: UILabel = {
        let label = UILabel()
        label.text = TFALocalizable.onboardingTitle.text
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = Palette.ppColorGrayscale600.color
        label.font = Layout.Fonts.titleLabelFont
        return label
    }()
    
    private lazy var descriptionTextLabel: UILabel = {
        let label = UILabel()
        label.text = TFALocalizable.onboardingDescription.text
        label.textColor = Palette.ppColorGrayscale600.color
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = Layout.Fonts.descriptionLabelFont
        return label
    }()
    
    private lazy var continueButton: UIPPButton = {
        let button = UIPPButton()
        button.addTarget(self, action: #selector(continueButtonTapped(_:)), for: .touchUpInside)
        button.normalBackgrounColor = Palette.ppColorBranding300.color
        button.normalTitleColor = Palette.ppColorGrayscale000.color(withCustomDark: .ppColorGrayscale200)
        button.setTitle(DefaultLocalizable.next.text, for: .normal)
        button.titleLabel?.font = Layout.Fonts.continueButtonFont
        button.cornerRadius = Layout.Height.continueButtonHeight / 2
        return button
    }()
    
    private lazy var informationButton: UnderlinedButton = {
        let button = UnderlinedButton()
        button.setTitle(TFALocalizable.onboardingInfoButtonTitle.text, for: .normal)
        button.updateColor(
            Palette.ppColorGrayscale500.color,
            highlightedColor: Palette.ppColorGrayscale600.color,
            font: Layout.Fonts.informationButtonFont
        )
        button.addTarget(self, action: #selector(informationButtonTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = .noContextTitlelessBackButton
    }
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(closeButton)
        view.addSubview(centerStackView)
        centerStackView.addArrangedSubview(onboardingImage)
        centerStackView.addArrangedSubview(titleTextLabel)
        centerStackView.addArrangedSubview(descriptionTextLabel)
        centerStackView.addArrangedSubview(continueButton)
        view.addSubview(informationButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func setupConstraints() {
        closeButton.layout {
            $0.top == view.compatibleSafeAreaLayoutGuide.topAnchor + Layout.Spacing.defaultSpacing
            $0.leading == view.compatibleSafeAreaLayoutGuide.leadingAnchor + Layout.Spacing.closeButtonLeadingMargin
            $0.width == Layout.Width.closeButtonWidth
            $0.height == Layout.Height.closeButtonHeight
        }
        
        centerStackView.layout {
            $0.leading == view.leadingAnchor + Layout.Spacing.defaultSpacing
            $0.trailing == view.trailingAnchor - Layout.Spacing.defaultSpacing
            $0.centerX == view.centerXAnchor
            $0.centerY == view.centerYAnchor
        }
        
        onboardingImage.layout {
            $0.width == Layout.Width.onboardingImageWidth
            $0.height == Layout.Height.onboardingImageHeight
            $0.centerX == centerStackView.centerXAnchor
        }
        
        titleTextLabel.layout {
            $0.leading == centerStackView.leadingAnchor
            $0.trailing == centerStackView.trailingAnchor
        }
        
        descriptionTextLabel.layout {
            $0.leading == centerStackView.leadingAnchor
            $0.trailing == centerStackView.trailingAnchor
        }
        
        continueButton.layout {
            $0.leading == centerStackView.leadingAnchor
            $0.trailing == centerStackView.trailingAnchor
            $0.height == Layout.Height.continueButtonHeight
        }
        
        informationButton.layout {
            $0.leading == view.leadingAnchor + Layout.Spacing.defaultSpacing
            $0.trailing == view.trailingAnchor - Layout.Spacing.defaultSpacing
            $0.height == Layout.Height.informationButtonHeight
            $0.bottom == view.bottomAnchor - Layout.Spacing.informationButtonBottomMargin
        }
    }
    
    @objc
    func closeButtonTapped(_ sender: UIButton) {
        viewModel.closeOnboarding()
    }
    
    @objc
    func informationButtonTapped(_ sender: UIButton) {
        viewModel.showHelp()
    }
    
    @objc
    func continueButtonTapped(_ sender: UIButton) {
        viewModel.continueValidation()
    }
}
