import UIKit

enum NewTFAOnboardingAction {
    case close
    case showHelp
    case continueValidation
}

protocol NewTFAOnboardingCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: NewTFAOnboardingCoordinatorDelegate? { get set }
    func perform(action: NewTFAOnboardingAction)
}

protocol NewTFAOnboardingCoordinatorDelegate: AnyObject {
    func onboardingClose()
    func onboardingShowHelp()
    func onboardingNextStep()
}

final class NewTFAOnboardingCoordinator: NewTFAOnboardingCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: NewTFAOnboardingCoordinatorDelegate?
    
    func perform(action: NewTFAOnboardingAction) {
        switch action {
        case .close:
            delegate?.onboardingClose()
        case .showHelp:
            delegate?.onboardingShowHelp()
        case .continueValidation:
            delegate?.onboardingNextStep()
        }
    }
}
