protocol NewTFAOnboardingPresenting: AnyObject {
    func didNextStep(action: NewTFAOnboardingAction)
}

final class NewTFAOnboardingPresenter: NewTFAOnboardingPresenting {
    private let coordinator: NewTFAOnboardingCoordinating

    init(coordinator: NewTFAOnboardingCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: NewTFAOnboardingAction) {
        coordinator.perform(action: action)
    }
}
