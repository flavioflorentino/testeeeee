import Foundation

protocol NewTFAOnboardingViewModelInputs: AnyObject {
    func closeOnboarding()
    func showHelp()
    func continueValidation()
}

final class NewTFAOnboardingViewModel {
    private let presenter: NewTFAOnboardingPresenting

    init(presenter: NewTFAOnboardingPresenting) {
        self.presenter = presenter
    }
}

extension NewTFAOnboardingViewModel: NewTFAOnboardingViewModelInputs {
    func closeOnboarding() {
        presenter.didNextStep(action: .close)
    }
    
    func showHelp() {
        presenter.didNextStep(action: .showHelp)
    }
    
    func continueValidation() {
        presenter.didNextStep(action: .continueValidation)
    }
}
