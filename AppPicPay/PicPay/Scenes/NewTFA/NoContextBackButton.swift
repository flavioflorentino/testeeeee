import UIKit

extension UIBarButtonItem {
    static var noContextTitlelessBackButton: UIBarButtonItem {
        let backButton = NoContextBackButton(title: "", style: .plain, target: nil, action: nil)
        backButton.accessibilityLabel = TFALocalizable.genericBackText.text
        return backButton
    }
}

final class NoContextBackButton: UIBarButtonItem {
    @available(iOS 13.0, *)
    override var menu: UIMenu? {
        // swiftlint:disable:next unused_setter_value
        set {
            // This empty `set` avoids the context menu functionality.
            // This is necessary for this feature.
        }
        get {
            nil
        }
    }
}
