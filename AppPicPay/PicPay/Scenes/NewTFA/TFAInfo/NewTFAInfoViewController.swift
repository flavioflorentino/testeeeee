import UI
import UIKit

private extension NewTFAInfoViewController.Layout {
    enum Spacing {
        static let horizontalTextMargin: CGFloat = 20
        static let verticalTitleTextTopMargin: CGFloat = 15
        static let verticalTitleTextBottomMargin: CGFloat = 20
    }
}

final class NewTFAInfoViewController: ViewController<NewTFAInfoViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
    private lazy var titleTextLabel: UILabel = {
        let label = UILabel()
        label.text = TFALocalizable.infoTitle.text
        label.textColor = Palette.ppColorGrayscale600.color
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var descriptionTextLabel: UILabel = {
        let label = UILabel()
        label.text = TFALocalizable.infoDescription.text
        label.textColor = Palette.ppColorGrayscale400.color
        label.font = UIFont.systemFont(ofSize: 14, weight: .light)
        label.numberOfLines = 0
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(titleTextLabel)
        view.addSubview(descriptionTextLabel)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }

    override func setupConstraints() {
        titleTextLabel.layout {
            $0.top == view.topAnchor + Layout.Spacing.verticalTitleTextTopMargin
            $0.bottom == descriptionTextLabel.topAnchor - Layout.Spacing.verticalTitleTextBottomMargin
            $0.leading == view.leadingAnchor + Layout.Spacing.horizontalTextMargin
            $0.trailing == view.trailingAnchor - Layout.Spacing.horizontalTextMargin
        }
        
        descriptionTextLabel.layout {
            $0.leading == view.leadingAnchor + Layout.Spacing.horizontalTextMargin
            $0.trailing == view.trailingAnchor - Layout.Spacing.horizontalTextMargin
        }
    }
}
