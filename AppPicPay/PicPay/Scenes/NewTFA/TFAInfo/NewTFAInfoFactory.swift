import Foundation

enum NewTFAInfoFactory {
    static func make() -> NewTFAInfoViewController {
        let viewModel = NewTFAInfoViewModel()
        let viewController = NewTFAInfoViewController(viewModel: viewModel)
        return viewController
    }
}
