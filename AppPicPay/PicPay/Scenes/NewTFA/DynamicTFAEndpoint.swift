import Core
import Foundation

enum DynamicTFAEndpoint {
    case tfaOptions(tfaRequestData: TFARequestData)
    case validation(tfaRequestData: TFARequestData)
    case getPersonalData(tfaRequestData: TFARequestData)
    case validatePersonalData(personalData: TFAPersonalDataCheckInfo, tfaRequestData: TFARequestData)
    case validateBankData(bankAccount: BankAccountData, tfaRequestData: TFARequestData)
    case getBankData(tfaRequestData: TFARequestData)
    case validatePhoneNumberData(phoneNumber: String, tfaRequestData: TFARequestData)
    case getPhoneNumberData(tfaRequestData: TFARequestData)
    case getCodeDataInfo(tfaRequestData: TFARequestData)
    case validateCode(code: String, tfaRequestData: TFARequestData)
    case getCreditCards(tfaRequestData: TFARequestData)
    case validateCreditCard(cardData: TFACreditCardValidateData, tfaRequestData: TFARequestData)
}

extension DynamicTFAEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .tfaOptions(let tfaRequestData),
             .validation(let tfaRequestData),
             .validatePersonalData(_, let tfaRequestData),
             .getBankData(let tfaRequestData),
             .validateBankData(_, let tfaRequestData),
             .getPhoneNumberData(let tfaRequestData),
             .validatePhoneNumberData(_, let tfaRequestData),
             .getCodeDataInfo(let tfaRequestData),
             .validateCode(_, let tfaRequestData),
             .getCreditCards(let tfaRequestData),
             .validateCreditCard(_, let tfaRequestData),
             .getPersonalData(let tfaRequestData):
            return configureEndpoint(with: tfaRequestData)
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .validatePersonalData,
             .validateBankData,
             .validatePhoneNumberData,
             .validateCode,
             .validateCreditCard:
            return .post
        case .tfaOptions,
             .validation,
             .getBankData,
             .getPhoneNumberData,
             .getCodeDataInfo,
             .getCreditCards,
             .getPersonalData:
            return .get
        }
    }
    
    var body: Data? {
        switch self {
        case let .validatePersonalData(personalData, _):
            return personalDataBody(personalData)
        case let .validateBankData(bankAccount, _):
            return bankAccountDataBody(bankAccount)
        case let .validatePhoneNumberData(phoneNumber, _):
            return phoneNumberDataBody(phoneNumber)
        case let .validateCode(code, _):
            return validateCodeDataBody(code)
        case let .validateCreditCard(cardData, _):
            return validateCreditCardDataBody(cardData)
        default:
            return nil
        }
    }
    
    var isTokenNeeded: Bool {
        false
    }
    
    private func configureEndpoint(with tfaRequestData: TFARequestData) -> String {
        "auth/tfa/v2/\(tfaRequestData.id)/flow/\(tfaRequestData.flow.rawValue)/step/\(tfaRequestData.step.rawValue)"
    }
    
    private func personalDataBody(_ personalData: TFAPersonalDataCheckInfo) -> Data? {
        let formData: [String: Any] = [
            "form_data": [
                "cpf": personalData.cpfDocument,
                "mother_name": personalData.mothersName,
                "email": personalData.email,
                "birth_date": personalData.birthDate
            ]
        ]
        
        return formData.toData()
    }
    
    private func bankAccountDataBody(_ bankAccountData: BankAccountData) -> Data? {
        var bankAccount: [String: Any] = [
            "account": bankAccountData.account,
            "account_dv": bankAccountData.accountDigit,
            "agency": bankAccountData.agency
        ]

        if let agencyDigit = bankAccountData.agencyDigit,
            agencyDigit.isNotEmpty {
            bankAccount["agency_dv"] = agencyDigit
        }

        let accountData: [String: Any] = ["bank_account": bankAccount]
        return accountData.toData()
    }
    
    private func phoneNumberDataBody(_ phoneNumber: String) -> Data? {
        let phoneNumberData: [String: Any] = [
            "phone": [
                "phone_number": phoneNumber
            ]
        ]
        
        return phoneNumberData.toData()
    }
    
    private func validateCodeDataBody(_ code: String) -> Data? {
        let tfaCodeData: [String: Any] = ["code": code]
        return tfaCodeData.toData()
    }
    
    private func validateCreditCardDataBody(_ card: TFACreditCardValidateData) -> Data? {
        let tfaCreditCardValidateData: [String: Any] = [
            "credit_card": [
                "id": card.id,
                "expiry_date": card.dueData,
                "first_six": card.firstDigits
            ]
        ]
        
        return tfaCreditCardValidateData.toData()
    }
}
