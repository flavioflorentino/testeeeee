import Foundation
import Validator

struct PhoneNumberVerificationTextFieldSettings {
    let phoneNumber: TextfieldSetting
}

enum PhoneNumberVerificationValidationError: String, CustomStringConvertible, Error, ValidationError {
    case phoneNumberRequired = "Número de telefone obrigatório"
    
    var message: String {
        self.rawValue
    }
    
    var description: String {
        self.rawValue
    }
}
