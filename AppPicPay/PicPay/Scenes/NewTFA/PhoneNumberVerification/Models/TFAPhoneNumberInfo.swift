struct TFAPhoneNumberInfo: Decodable {
    struct TFAPhoneNumber: Decodable {
        let phoneNumber: String
    }
    
    let phone: TFAPhoneNumber
}
