import AnalyticsModule
import Core
import Foundation
import Validator
import UI

protocol PhoneNumberVerificationViewModelInputs: AnyObject {
    func loadInfo()
    func buildTextfieldSettings()
    func checkTextfield(textfield: UIPPFloatingTextField)
    func checkTextfieldsAndEnableContinue(textFields: [UIPPFloatingTextField])
    func validateForm(phoneNumber: String)
    func startTimerForNewValidation(retryTime: TimeInterval)
}

final class PhoneNumberVerificationViewModel {
    private let service: PhoneNumberVerificationServicing
    private let presenter: PhoneNumberVerificationPresenting
    private let phoneInfo: String?
    private let tfaRequestData: TFARequestData
    
    private var timer: RepeatingTimer? = RepeatingTimer(timeInterval: 1)
    private var endDate = Date(timeInterval: 180, since: Date())
    
    init(
        service: PhoneNumberVerificationServicing,
        presenter: PhoneNumberVerificationPresenting,
        phoneInfo: String?,
        tfaRequestData: TFARequestData
    ) {
        self.service = service
        self.presenter = presenter
        self.phoneInfo = phoneInfo
        self.tfaRequestData = tfaRequestData
    }
}

extension PhoneNumberVerificationViewModel: PhoneNumberVerificationViewModelInputs {
    func loadInfo() {
        guard let info = phoneInfo else {
            loadPhoneNumberInfo()
            return
        }
        presenter.presentHintForNumber(hint: info)
    }
    
    func buildTextfieldSettings() {
        let set = PhoneNumberVerificationTextFieldSettings(phoneNumber: buildTextfieldSettingPhoneNumber())
        
        presenter.setFieldsRules(settings: set)
    }
    
    func checkTextfield(textfield: UIPPFloatingTextField) {
        let result = textfield.validate()
        var message: String?
        
        if case let .invalid(error) = result,
            let err = error.first {
            message = String(describing: err)
        }
        
        presenter.updateTextFieldErrorMessage(textfield: textfield, message: message)
    }
    
    func checkTextfieldsAndEnableContinue(textFields: [UIPPFloatingTextField]) {
        let enable = textFields.allSatisfy { $0.validate().isValid }
        presenter.shouldEnableContinueButton(enable: enable)
    }
    
    func validateForm(phoneNumber: String) {
        validateFormForNextStep(phone: phoneNumber)
    }
    
    func startTimerForNewValidation(retryTime: TimeInterval) {
        endDate = Date(timeInterval: retryTime, since: Date())
        runCountDown()
    }
}

extension PhoneNumberVerificationViewModel {
    private func loadPhoneNumberInfo() {
        presenter.presentLoadingScreen(loading: true)
        service.getPhoneNumberData(tfaRequestData: tfaRequestData) { [weak self] (result: Result<TFAPhoneNumberInfo, ApiError>) in
            guard let self = self else {
                return
            }
            
            self.presenter.presentLoadingScreen(loading: false)
            switch result {
            case let .success(info):
                self.presenter.presentHintForNumber(hint: info.phone.phoneNumber)
            case .failure:
                self.presenter.presentHintForNumber(hint: nil)
            }
        }
    }
    
    private func validateFormForNextStep(phone: String) {
        presenter.setLoading(loading: true)
        
        service.validatePhoneNumberData(phone: phone, tfaRequestData: tfaRequestData) { [weak self] (result: Result<TFAValidationResponse, ApiError>) in
            guard let self = self else {
                return
            }
            
            self.presenter.setLoading(loading: false)
            switch result {
            case let .success(response):
                self.presenter.validateData(with: self.tfaRequestData.flow, nextStep: response.nextStep)
            case let .failure(error):
                self.parseError(error: error)
            }
        }
    }
    
    private func buildTextfieldSettingPhoneNumber() -> TextfieldSetting {
        var phoneRules = ValidationRuleSet<String>()
        phoneRules.add(rule: ValidationRuleLength(min: 15, max: 15, lengthType: .characters, error: PhoneNumberVerificationValidationError.phoneNumberRequired))
        
        let placeholder = TFALocalizable.phoneNumber.text
        let mask = TFALocalizable.phoneNumberMask.text
        return TextfieldSetting(placeholder: placeholder, text: nil, rule: phoneRules, mask: mask)
    }
    
    private func parseError(error: ApiError) {
        switch error {
        case .tooManyRequests(let requestError), .unauthorized(let requestError), .badRequest(let requestError):
            guard
                let errorData = requestError.data?.dictionaryObject?["data"] as? [String: Any],
                let retryTime = errorData["retry_time"] as? Int
                else {
                    presenter.presentValidationInfoError(tfaError: requestError)
                    return
            }
            
            presenter.presentAttempLimitError(tfaError: requestError, retryTime: retryTime, tfaType: tfaRequestData.flow)
        default:
            presenter.presentGenericError()
        }
    }
    
    private func stopTimer() {
        timer?.eventHandler = nil
        timer?.suspend()
        timer = nil
    }
    
    private func runCountDown() {
        if timer == nil {
            timer = RepeatingTimer(timeInterval: 1)
        }
        
        timer?.eventHandler = { [weak self] in
            self?.updateTimer()
        }
        timer?.resume()
    }
    
    private func updateTimer() {
        let endTime = self.endDate.timeIntervalSince(Date())
        if endTime <= 0 {
            stopTimer()
        }
        
        presenter.presentContinueButtonUpdated(with: endTime)
    }
}
