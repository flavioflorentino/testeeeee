import UI
import UIKit

private extension PhoneNumberVerificationViewController.Layout {
    enum Fonts {
        static let headerLabelFont = UIFont.boldSystemFont(ofSize: 28)
        static let descriptionLabelFont = UIFont.systemFont(ofSize: 16)
    }
    
    enum Spacing {
        static let topOffset: CGFloat = 20
        static let horizontalOffset: CGFloat = 20
        static let headerTopOffset: CGFloat = 12
        static let topOffsetButton: CGFloat = 32
    }
    
    enum Width {
        static let infoButtonWidth: CGFloat = 24
    }
    
    enum Height {
        static let buttonHeight: CGFloat = 44
    }
    
    enum Others {
        static let keyboardMargin: CGFloat = 16
    }
}

final class PhoneNumberVerificationViewController: ViewController<PhoneNumberVerificationViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
     lazy var loadingView = LoadingView()
    
    private lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.backgroundColor = .clear
        return scroll
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var headerLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.headerLabelFont
        label.textColor = Palette.ppColorGrayscale600.color
        label.textAlignment = .left
        label.text = TFALocalizable.yourPhone.text
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.descriptionLabelFont
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = 0
        label.textAlignment = .left
        label.text = TFALocalizable.genericNumberDescription.text
        return label
    }()
    
    private lazy var phoneNumberTextfield: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.defaultForm()
        textfield.addTarget(self, action: #selector(checkPhoneNumberTextfield), for: .editingChanged)
        textfield.keyboardType = .numberPad
        textfield.delegate = self
        return textfield
    }()
    
    private lazy var continueButton: UIPPButton = {
        let button = UIPPButton()
        button.addTarget(self, action: #selector(didTapButtonSave), for: .touchUpInside)
        button.normalBackgrounColor = Palette.ppColorBranding300.color
        button.normalTitleColor = Palette.ppColorGrayscale000.color
        button.setTitleColor(Palette.ppColorGrayscale000.color)
        button.disabledTitleColor = Palette.ppColorGrayscale000.color
        button.disabledBackgrounColor = Palette.ppColorGrayscale400.color
        button.setTitle(DefaultLocalizable.next.text, for: .normal)
        button.setTitle(DefaultLocalizable.next.text, for: .disabled)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15.0)
        button.cornerRadius = Layout.Height.buttonHeight / 2
        return button
    }()
    
    private var continueButtonBottomConstraint: NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addKeyboardObservers()
        viewModel.buildTextfieldSettings()
        viewModel.loadInfo()
        navigationItem.backBarButtonItem = .noContextTitlelessBackButton
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(headerLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(phoneNumberTextfield)
        contentView.addSubview(continueButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override func setupConstraints() {
        scrollView.layout {
            $0.top == view.topAnchor
            $0.bottom == view.bottomAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
        }
        
        contentView.layout {
            $0.top == scrollView.topAnchor
            $0.bottom == scrollView.bottomAnchor
            $0.leading == scrollView.leadingAnchor
            $0.trailing == scrollView.trailingAnchor
            $0.width == view.widthAnchor
        }
        
        headerLabel.layout {
            $0.top == contentView.topAnchor + Layout.Spacing.headerTopOffset
            $0.leading == contentView.leadingAnchor + Layout.Spacing.horizontalOffset
            $0.trailing == contentView.trailingAnchor - Layout.Spacing.horizontalOffset
        }
        
        descriptionLabel.layout {
            $0.top == headerLabel.bottomAnchor + Layout.Spacing.topOffset
            $0.leading == contentView.leadingAnchor + Layout.Spacing.horizontalOffset
            $0.trailing == contentView.trailingAnchor - Layout.Spacing.horizontalOffset
        }
        
        phoneNumberTextfield.layout {
            $0.top == descriptionLabel.bottomAnchor + Layout.Spacing.topOffset
            $0.leading == contentView.leadingAnchor + Layout.Spacing.horizontalOffset
            $0.trailing == contentView.trailingAnchor - Layout.Spacing.horizontalOffset
        }
        
        continueButton.layout {
            continueButtonBottomConstraint = $0.bottom == contentView.bottomAnchor - Layout.Others.keyboardMargin
            $0.leading == contentView.leadingAnchor + Layout.Spacing.horizontalOffset
            $0.trailing == contentView.trailingAnchor - Layout.Spacing.horizontalOffset
            $0.top == phoneNumberTextfield.bottomAnchor + Layout.Spacing.topOffsetButton
            $0.height == Layout.Height.buttonHeight
        }
    }
    
    private func addKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc
    private func keyboardWillShow(notification: NSNotification) {
        guard
            let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue,
            let offset = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
                return
        }
        let constant = -Layout.Others.keyboardMargin + (keyboardSize.height == offset.height ? -keyboardSize.height : -offset.height)
        continueButtonBottomConstraint?.constant = constant
        view.layoutIfNeeded()
        
        scrollView.scrollRectToVisible(CGRect(origin: offset.origin, size: CGSize(width: offset.width, height: continueButton.frame.maxY)), animated: true)
    }
    
    @objc
    private func keyboardWillHide(notification: NSNotification) {
        continueButtonBottomConstraint?.constant = -Layout.Others.keyboardMargin
        view.layoutIfNeeded()
    }
    
    @objc
    func checkPhoneNumberTextfield() {
        viewModel.checkTextfield(textfield: phoneNumberTextfield)
    }
    
    @objc
    private func didTapButtonSave() {
        view?.endEditing(true)
        viewModel.validateForm(phoneNumber: phoneNumberTextfield.text ?? "")
    }
    
    private func setLoadingState(loading: Bool) {
        phoneNumberTextfield.isEnabled = !loading
        loading ? continueButton.startLoadingAnimating() : continueButton.stopLoadingAnimating()
    }
    
    private func enableFields(enable: Bool) {
        phoneNumberTextfield.isEnabled = enable
    }
    
    private func enableValidation() {
        continueButton.isEnabled = true
        continueButton.setTitle(DefaultLocalizable.next.text, for: .disabled)
        enableFields(enable: true)
    }
}

// MARK: View Model Outputs
extension PhoneNumberVerificationViewController: PhoneNumberVerificationDisplay {
    func setFieldsRules(settings: PhoneNumberVerificationTextFieldSettings) {
        phoneNumberTextfield.placeholder = settings.phoneNumber.placeholder
        phoneNumberTextfield.validationRules = settings.phoneNumber.rule
        phoneNumberTextfield.maskString = settings.phoneNumber.mask
        
        viewModel.checkTextfieldsAndEnableContinue(textFields: [phoneNumberTextfield])
    }
    
    func displayDescription(with attributedText: NSAttributedString) {
        DispatchQueue.main.async { [weak self] in
            self?.descriptionLabel.attributedText = attributedText
        }
    }
    
    func updateTextFieldErrorMessage(textField: UIPPFloatingTextField, message: String?) {
        textField.errorMessage = message
        viewModel.checkTextfieldsAndEnableContinue(textFields: [phoneNumberTextfield])
    }
    
    func enableContinueButton(enable: Bool) {
        continueButton.isEnabled = enable
    }
    
    func displayLoading(loading: Bool) {
        DispatchQueue.main.async { [weak self] in
            self?.setLoadingState(loading: loading)
        }
    }
    
    func displayLoadingScreen(loading: Bool) {
        DispatchQueue.main.async {
            loading ? self.startLoadingView() : self.stopLoadingView()
        }
    }
    
    func displayValidationInfoError(alert: Alert) {
        DispatchQueue.main.async {
            AlertMessage.showAlert(alert, controller: self)
        }
    }
    
    func displayAttempLimitError(alert: Alert, retryTime: TimeInterval) {
        viewModel.startTimerForNewValidation(retryTime: retryTime)
        
        DispatchQueue.main.async {
            self.enableFields(enable: false)
            self.continueButton.isEnabled = false
            AlertMessage.showAlert(alert, controller: self)
        }
    }
    
    func displayGenericError(alert: Alert) {
        DispatchQueue.main.async {
            AlertMessage.showAlert(alert, controller: self)
        }
    }
    
    func displayContinueButton(with currentRemainingTime: String, enabled: Bool) {
        DispatchQueue.main.async {
            if enabled {
                self.enableValidation()
            } else {
                self.continueButton.setTitle(currentRemainingTime, for: .disabled)
            }
        }
    }
}

extension PhoneNumberVerificationViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        configTextField(with: textField, range: range, replacementString: string)
    }
    
    // todo - Try to put this logic in the presenter
    private func configTextField(with textField: UITextField, range: NSRange, replacementString string: String) -> Bool {
        guard let ppTextField = textField as? MaskTextField, 
            let textt = textField.text else {
                return true
        }
        
        let newText = (textt as NSString).replacingCharacters(in: range, with: string)
        
        guard let maskString = ppTextField.maskString else {
            return true
        }
        
        let mask = CustomStringMask(mask: maskString)
        textField.text = mask.maskedText(from: newText)
        
        if textField == phoneNumberTextfield {
            checkPhoneNumberTextfield()
        }
        
        return false
    }
}

extension PhoneNumberVerificationViewController: LoadingViewProtocol { }
