import UIKit

enum PhoneNumberVerificationAction {
    case phoneNumberValidateAndStartNextStep(tfaType: TFAType, nextStep: TFAStep)
}

protocol PhoneNumberVerificationCoordinatorDelegate: AnyObject {
    func didValidatePhoneNumber(with tfaType: TFAType, nextStep: TFAStep)
}

protocol PhoneNumberVerificationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: PhoneNumberVerificationCoordinatorDelegate? { get set }
    func perform(action: PhoneNumberVerificationAction)
}

final class PhoneNumberVerificationCoordinator: PhoneNumberVerificationCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: PhoneNumberVerificationCoordinatorDelegate?
    
    func perform(action: PhoneNumberVerificationAction) {
        if case let .phoneNumberValidateAndStartNextStep(tfaType, nextStep) = action {
            delegate?.didValidatePhoneNumber(with: tfaType, nextStep: nextStep)
        }
    }
}
