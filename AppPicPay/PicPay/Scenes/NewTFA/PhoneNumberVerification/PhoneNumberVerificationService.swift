import Foundation
import Core

protocol PhoneNumberVerificationServicing {
    func getPhoneNumberData(tfaRequestData: TFARequestData, completion: @escaping (Result<TFAPhoneNumberInfo, ApiError>) -> Void)
    func validatePhoneNumberData(
        phone: String,
        tfaRequestData: TFARequestData,
        completion: @escaping (Result<TFAValidationResponse, ApiError>) -> Void
    )
}

final class PhoneNumberVerificationService: PhoneNumberVerificationServicing {
    func getPhoneNumberData(
        tfaRequestData: TFARequestData,
        completion: @escaping (Result<TFAPhoneNumberInfo, ApiError>) -> Void
    ) {
        let api = Api<TFAPhoneNumberInfo>(endpoint: DynamicTFAEndpoint.getPhoneNumberData(tfaRequestData: tfaRequestData))
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { result in
            let mappedResult = result
                .map { $0.model }
            completion(mappedResult)
        }
    }
    
    func validatePhoneNumberData(
        phone: String,
        tfaRequestData: TFARequestData,
        completion: @escaping (Result<TFAValidationResponse, ApiError>) -> Void
    ) {
        let api = Api<TFAValidationResponse>(
            endpoint: DynamicTFAEndpoint.validatePhoneNumberData(phoneNumber: phone, tfaRequestData: tfaRequestData)
        )
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { result in
            let mappedResult = result
                .map { $0.model }
            completion(mappedResult)
        }
    }
}
