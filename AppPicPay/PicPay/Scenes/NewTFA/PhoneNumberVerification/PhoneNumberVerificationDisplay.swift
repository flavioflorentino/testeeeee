import UI

protocol PhoneNumberVerificationDisplay: AnyObject {
    func setFieldsRules(settings: PhoneNumberVerificationTextFieldSettings)
    func displayDescription(with attributedText: NSAttributedString)
    func updateTextFieldErrorMessage(textField: UIPPFloatingTextField, message: String?)
    func enableContinueButton(enable: Bool)
    func displayLoading(loading: Bool)
    func displayLoadingScreen(loading: Bool)
    func displayValidationInfoError(alert: Alert)
    func displayAttempLimitError(alert: Alert, retryTime: TimeInterval)
    func displayGenericError(alert: Alert)
    func displayContinueButton(with currentRemainingTime: String, enabled: Bool)
}
