import AnalyticsModule
import Core
import Foundation
import UI

protocol PhoneNumberVerificationPresenting: AnyObject {
    var viewController: PhoneNumberVerificationDisplay? { get set }
    func setFieldsRules(settings: PhoneNumberVerificationTextFieldSettings)
    func presentHintForNumber(hint: String?)
    func updateTextFieldErrorMessage(textfield: UIPPFloatingTextField, message: String?)
    func shouldEnableContinueButton(enable: Bool)
    func validateData(with tfaType: TFAType, nextStep: TFAStep)
    func setLoading(loading: Bool)
    func presentLoadingScreen(loading: Bool)
    func presentContinueButtonUpdated(with remainingTime: TimeInterval)
    func presentValidationInfoError(tfaError: RequestError)
    func presentAttempLimitError(tfaError: RequestError, retryTime: Int, tfaType: TFAType)
    func presentGenericError()
}

final class PhoneNumberVerificationPresenter: PhoneNumberVerificationPresenting {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies
    
    private let coordinator: PhoneNumberVerificationCoordinating
    var viewController: PhoneNumberVerificationDisplay?
    private let tfaTimerformatter = TFATimerFormatter()
    
    init(coordinator: PhoneNumberVerificationCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    func setFieldsRules(settings: PhoneNumberVerificationTextFieldSettings) {
        viewController?.setFieldsRules(settings: settings)
    }
    
    func presentHintForNumber(hint: String?) {
        guard let maskedNumber = hint else {
            let attributedString = NSMutableAttributedString(
                string: TFALocalizable.genericNumberDescription.text,
                attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)]
            )
            viewController?.displayDescription(with: attributedString)
            return
        }
        
        let normalText = TFALocalizable.completeNumberDescription.text.appending(maskedNumber)
        let hintRange = (normalText as NSString).range(of: maskedNumber)
        let attributedString = NSMutableAttributedString(string: normalText)
        attributedString.addAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16)], range: hintRange)
        
        viewController?.displayDescription(with: attributedString)
    }
    
    func updateTextFieldErrorMessage(textfield: UIPPFloatingTextField, message: String?) {
        viewController?.updateTextFieldErrorMessage(textField: textfield, message: message)
    }
    
    func shouldEnableContinueButton(enable: Bool) {
        viewController?.enableContinueButton(enable: enable)
    }
    
    func validateData(with tfaType: TFAType, nextStep: TFAStep) {
        coordinator.perform(action: .phoneNumberValidateAndStartNextStep(tfaType: tfaType, nextStep: nextStep))
    }
    
    func setLoading(loading: Bool) {
        viewController?.displayLoading(loading: loading)
    }
    
    func presentLoadingScreen(loading: Bool) {
        viewController?.displayLoadingScreen(loading: loading)
    }
    
    func presentGenericError() {
        let alert = Alert(
            with: TFALocalizable.genericErrorTitle.text,
            text: TFALocalizable.genericErrorMessage.text,
            buttons: [
                Button(title: DefaultLocalizable.back.text, type: .cta)
            ]
        )
        viewController?.displayGenericError(alert: alert)
    }
    
    func presentValidationInfoError(tfaError: RequestError) {
        let alert = Alert(
            title: tfaError.title,
            text: tfaError.message
        )
        alert.buttons = [
            Button(title: DefaultLocalizable.back.text, type: .cta)
        ]
        viewController?.displayValidationInfoError(alert: alert)
    }
    
    func presentAttempLimitError(tfaError: RequestError, retryTime: Int, tfaType: TFAType) {
        let alert = Alert(
            title: tfaError.title,
            text: tfaError.message
        )
        alert.buttons = [
            Button(title: DefaultLocalizable.back.text, type: .cta)
        ]
        
        Analytics.shared.log(TFAEvents.didReceivedExceededDataValidationLimitError(tfaType: tfaType, currentStep: .phoneNumber))
        viewController?.displayAttempLimitError(alert: alert, retryTime: TimeInterval(retryTime))
    }
    
    func presentContinueButtonUpdated(with remainingTime: TimeInterval) {
        let formatedTime = tfaTimerformatter.timeString(with: remainingTime)
        let buttonText = "\(DefaultLocalizable.wait.text) \(formatedTime)"
        viewController?.displayContinueButton(with: buttonText, enabled: remainingTime <= 0)
    }
}
