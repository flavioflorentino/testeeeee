import Foundation

enum PhoneNumberVerificationFactory {
    static func make(
        with phoneInfo: String? = nil,
        tfaRequestData: TFARequestData,
        coordinatorDelegate: PhoneNumberVerificationCoordinatorDelegate
    ) -> PhoneNumberVerificationViewController {
        let container = DependencyContainer()
        let service: PhoneNumberVerificationServicing = PhoneNumberVerificationService()
        let coordinator: PhoneNumberVerificationCoordinating = PhoneNumberVerificationCoordinator()
        let presenter: PhoneNumberVerificationPresenting = PhoneNumberVerificationPresenter(
            coordinator: coordinator,
            dependencies: container
        )
        let viewModel = PhoneNumberVerificationViewModel(
            service: service,
            presenter: presenter,
            phoneInfo: phoneInfo,
            tfaRequestData: tfaRequestData
        )
        let viewController = PhoneNumberVerificationViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        presenter.viewController = viewController

        return viewController
    }
}
