import Core
import Foundation

protocol NewTFACodeFormServicing {
    func getNewCodeDataInfo(tfaRequestData: TFARequestData, completion: @escaping (Result<NewTFACode, ApiError>) -> Void)
    func validateCodeForNextStep(
        code: String,
        tfaRequestData: TFARequestData,
        completion: @escaping (Result<TFAValidationResponse, ApiError>) -> Void
    )
}

final class NewTFACodeFormService: NewTFACodeFormServicing {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func getNewCodeDataInfo(tfaRequestData: TFARequestData, completion: @escaping (Result<NewTFACode, ApiError>) -> Void) {
        let api = Api<NewTFACode>(endpoint: DynamicTFAEndpoint.getCodeDataInfo(tfaRequestData: tfaRequestData))
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { result in
            completion(result.map { $0.model })
        }
    }
    
    func validateCodeForNextStep(
        code: String,
        tfaRequestData: TFARequestData,
        completion: @escaping (Result<TFAValidationResponse, ApiError>) -> Void
    ) {
        let api = Api<TFAValidationResponse>(
            endpoint: DynamicTFAEndpoint.validateCode(code: code, tfaRequestData: tfaRequestData)
        )
        
        api.execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { result in
            completion(result.map { $0.model })
        }
    }
}
