import UI
import UIKit

private extension NewTFACodeFormViewController.Layout {
    enum Fonts {
        static let headerLabelFont = UIFont.boldSystemFont(ofSize: 28)
        static let descriptionLabelFont = UIFont.systemFont(ofSize: 16, weight: .light)
        static let retryButtonFont = UIFont.systemFont(ofSize: 13)
        static let codeTextFieldFont = UIFont.systemFont(ofSize: 24)
    }
    
    enum Spacing {
        static let defaultSpacing = UI.Spacing.base03
    }
    
    enum Others {
        static let keyboardMargin: CGFloat = 30
        static let limitHeight: CGFloat = 568
        static let screenHeight = UIScreen.main.bounds.height
    }
}

final class NewTFACodeFormViewController: ViewController<NewTFACodeFormViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
    lazy var loadingView = LoadingView()
    
    private lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.backgroundColor = .clear
        return scroll
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var headerLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.headerLabelFont
        label.textColor = Palette.ppColorGrayscale600.color
        label.textAlignment = .left
        label.numberOfLines = 0
        label.text = TFALocalizable.tfaCodeTitleText.text
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Fonts.descriptionLabelFont
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = 0
        label.textAlignment = .left
        label.text = TFALocalizable.oldDeviceNotificationSent.text
        return label
    }()
    
    private lazy var codeTextfield: UIPPFloatingTextField = {
        let textfield = UIPPFloatingTextField()
        textfield.defaultForm()
        textfield.keyboardType = .numberPad
        textfield.placeholder = TFALocalizable.authorizationCodePlaceholder.text
        textfield.placeholderFont = Layout.Fonts.codeTextFieldFont
        textfield.selectedTitleColor = Palette.ppColorBranding300.color
        textfield.font = Layout.Fonts.codeTextFieldFont
        textfield.delegate = self
        return textfield
    }()
    
    private lazy var retryButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(didTapRetryButton), for: .touchUpInside)
        button.setTitle(TFALocalizable.didNotReceivedCode.text, for: .normal)
        button.titleLabel?.font = Layout.Fonts.retryButtonFont
        button.setTitleColor(Palette.ppColorGrayscale400.color, for: .normal)
        return button
    }()
    
    private lazy var retryButtonContentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private var retryButtonBottomConstraint: NSLayoutConstraint?
    private var contentViewHeigthConstraint: NSLayoutConstraint?
    private var distanceForKeyboard: CGFloat = 0
    private let isSmallScreenDevice = Layout.Others.screenHeight <= Layout.Others.limitHeight
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addKeyboardObservers()
        navigationItem.backBarButtonItem = .noContextTitlelessBackButton
        viewModel.loadInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(headerLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(codeTextfield)
        contentView.addSubview(retryButtonContentView)
        retryButtonContentView.addSubview(retryButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        codeTextfield.textColor = Palette.ppColorGrayscale500.color
    }
    
    override func setupConstraints() {
        scrollView.layout {
            $0.top == view.topAnchor
            $0.bottom == view.bottomAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
        }
        
        contentView.layout {
            $0.top == scrollView.topAnchor
            $0.bottom == scrollView.bottomAnchor
            $0.leading == scrollView.leadingAnchor
            $0.trailing == scrollView.trailingAnchor
            $0.width == view.widthAnchor
            contentViewHeigthConstraint?.priority = .defaultLow
            contentViewHeigthConstraint = $0.height == view.heightAnchor
        }
        
        headerLabel.layout {
            $0.top == contentView.topAnchor + Spacing.base02
            $0.leading == contentView.leadingAnchor + Layout.Spacing.defaultSpacing
            $0.trailing == contentView.trailingAnchor - Layout.Spacing.defaultSpacing
        }
        
        descriptionLabel.layout {
            $0.top == headerLabel.bottomAnchor + Layout.Spacing.defaultSpacing
            $0.leading == contentView.leadingAnchor + Layout.Spacing.defaultSpacing
            $0.trailing == contentView.trailingAnchor - Layout.Spacing.defaultSpacing
        }
        
        codeTextfield.layout {
            $0.top == descriptionLabel.bottomAnchor + Layout.Spacing.defaultSpacing
            $0.leading == contentView.leadingAnchor + Layout.Spacing.defaultSpacing
            $0.trailing == contentView.trailingAnchor - Layout.Spacing.defaultSpacing
        }
        
        retryButtonContentView.layout {
            $0.top >= codeTextfield.bottomAnchor + Layout.Spacing.defaultSpacing
            $0.leading == contentView.leadingAnchor + Layout.Spacing.defaultSpacing
            $0.trailing == contentView.trailingAnchor - Layout.Spacing.defaultSpacing
            retryButtonBottomConstraint = $0.bottom == scrollView.bottomAnchor - Layout.Others.keyboardMargin
        }
        
        retryButton.layout {
            $0.top == retryButtonContentView.topAnchor
            $0.centerX == retryButtonContentView.centerXAnchor
            $0.bottom == retryButtonContentView.bottomAnchor
            $0.height == Spacing.base02
        }
    }
    
    private func addKeyboardObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    @objc
    private func keyboardWillShow(notification: NSNotification) {
        guard
            let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue,
            let offset = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
                return
        }
        calculatePositionsWhenKeyboardWillShow(keyboardSize: keyboardSize, offset: offset)
    }
    
    private func calculatePositionsWhenKeyboardWillShow(keyboardSize: CGRect, offset: CGRect) {
        let extraHeight = keyboardSize.height == offset.height ? -keyboardSize.height : -offset.height
        let constant = -Layout.Others.keyboardMargin + extraHeight
        
        if isSmallScreenDevice {
            calculatePositionsForSmallScreen(keyboardHeight: abs(extraHeight))
        }
        retryButtonBottomConstraint?.constant = constant
        
        view.layoutIfNeeded()
        if isSmallScreenDevice {
            scrollView.scrollRectToVisible(
                CGRect(
                    origin: offset.origin,
                    size: CGSize(width: offset.width, height: retryButtonContentView.frame.maxY)
                ),
                animated: true
            )
        }
    }
    
    private func calculatePositionsForSmallScreen(keyboardHeight: CGFloat) {
        let codeTextFielMaxY = codeTextfield.frame.maxY
        let retryButtonMinY = retryButtonContentView.frame.minY
        
        let distance = retryButtonMinY - codeTextFielMaxY
        distanceForKeyboard = distance - Layout.Others.keyboardMargin
        
        if distance >= (keyboardHeight - Layout.Others.keyboardMargin) {
            distanceForKeyboard = Layout.Others.keyboardMargin
        }
        contentViewHeigthConstraint?.constant += abs(distanceForKeyboard)
    }
    
    @objc
    private func keyboardWillHide(notification: NSNotification) {
        retryButtonBottomConstraint?.constant = -Layout.Others.keyboardMargin
        
        if isSmallScreenDevice {
            contentViewHeigthConstraint?.constant -= abs(distanceForKeyboard)
        }
        view.layoutIfNeeded()
    }
    
    @objc
    private func didTapRetryButton() {
        view.endEditing(true)
        viewModel.reloadCode()
    }
}

// MARK: View Model Outputs
extension NewTFACodeFormViewController: NewTFACodeFormDisplay {
    func displayAttributedDescription(_ attributedText: NSAttributedString, titleText: String) {
        DispatchQueue.main.async {
            self.descriptionLabel.attributedText = attributedText
            self.headerLabel.text = titleText
        }
    }
    
    func displayUpdatedCode(_ code: String) {
        codeTextfield.text = code
    }
    
    func displayCodeValidationWith(_ code: String) {
        view.endEditing(true)
        viewModel.validateCode(code)
    }
    
    func displayLoading(_ loading: Bool) {
        DispatchQueue.main.async {
            loading ? self.startLoadingView() : self.stopLoadingView()
        }
    }
    
    func displayUpdatedRetryButton(enabled: Bool, text: NSAttributedString) {
        DispatchQueue.main.async {
            self.retryButton.isEnabled = enabled
            self.retryButton.setAttributedTitle(text, for: enabled ? .normal : .disabled)
        }
    }
    
    func displayErrorAlert(alert: Alert) {
        DispatchQueue.main.async {
            AlertMessage.showAlert(alert, controller: self)
        }
    }
    
    func displayAttempLimitError(alert: Alert, retryTime: Int) {
        DispatchQueue.main.async {
            AlertMessage.showAlert(alert, controller: self)
            self.viewModel.startTimerForNewRequest(retryTime)
        }
    }
}

extension NewTFACodeFormViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard
            let text = textField.text,
            let textRange = Range(range, in: text) else {
                return true
        }
        
        let codeText = text.replacingCharacters(in: textRange, with: string)
        viewModel.updateCodeTextWith(codeText)
        
        return false
    }
}

extension NewTFACodeFormViewController: LoadingViewProtocol { }
