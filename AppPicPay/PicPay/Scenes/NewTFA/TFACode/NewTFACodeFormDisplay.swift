protocol NewTFACodeFormDisplay: AnyObject {
    func displayUpdatedCode(_ code: String)
    func displayCodeValidationWith(_ code: String)
    func displayLoading(_ loading: Bool)
    func displayAttributedDescription(_ attributedText: NSAttributedString, titleText: String)
    func displayUpdatedRetryButton(enabled: Bool, text: NSAttributedString)
    func displayErrorAlert(alert: Alert)
    func displayAttempLimitError(alert: Alert, retryTime: Int)
}
