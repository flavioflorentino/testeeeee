struct NewTFACode: Decodable {
    let retryTime: Int
    let sentTo: String
    @available(*, deprecated, message: "This will be removed in the future")
    let channel: String?
}
