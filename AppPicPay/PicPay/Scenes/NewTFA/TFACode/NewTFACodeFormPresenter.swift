import AnalyticsModule
import Core
import Foundation
import UI

protocol NewTFACodeFormPresenting: AnyObject {
    var viewController: NewTFACodeFormDisplay? { get set }
    func updateCodeTextFieldWith(_ code: String)
    func presentCodeValidationWith(_ code: String)
    func presentLoading(_ loading: Bool)
    func presentDescriptionAttributedText(for destination: String?, flow: TFAType)
    func presentRetryButtonStatus(_ enabled: Bool, remainingTime: Int)
    func presentGenericError()
    func presentRequestError(tfaError: RequestError)
    func presentAttempLimitError(tfaError: RequestError, retryTime: Int, tfaType: TFAType)
    func validateCode(with type: TFAType, nextStep: TFAStep)
}

final class NewTFACodeFormPresenter: NewTFACodeFormPresenting {
    private let coordinator: NewTFACodeFormCoordinating
    weak var viewController: NewTFACodeFormDisplay?
    private let tfaTimerformatter = TFATimerFormatter()
    
    init(coordinator: NewTFACodeFormCoordinating) {
        self.coordinator = coordinator
    }
    
    func updateCodeTextFieldWith(_ code: String) {
        viewController?.displayUpdatedCode(code)
    }
    
    func presentCodeValidationWith(_ code: String) {
        viewController?.displayCodeValidationWith(code)
    }
    
    func presentLoading(_ loading: Bool) {
        viewController?.displayLoading(loading)
    }
    
    func presentDescriptionAttributedText(for destination: String?, flow: TFAType) {
        let title = flow == .email ? TFALocalizable.tfaCodeEmailTitle.text : TFALocalizable.tfaCodeSmsTitle.text
        viewController?.displayAttributedDescription(attributedString(for: destination, flow: flow), titleText: title)
    }
    
    func presentRetryButtonStatus(_ enabled: Bool, remainingTime: Int) {
        self.viewController?.displayUpdatedRetryButton(
            enabled: enabled,
            text: enabled ? enabledAttributedButtonText() : disabledAttributedButtonText(with: remainingTime)
        )
    }
    
    func presentGenericError() {
        let alert = Alert(
            with: TFALocalizable.genericErrorTitle.text,
            text: TFALocalizable.genericErrorMessage.text,
            buttons: [
                Button(title: DefaultLocalizable.back.text, type: .cta)
            ]
        )
        
        viewController?.displayErrorAlert(alert: alert)
    }
    
    func presentRequestError(tfaError: RequestError) {
        let alert = Alert(title: tfaError.title, text: tfaError.message)
        alert.buttons = [Button(title: DefaultLocalizable.back.text, type: .cta)]
        
        viewController?.displayErrorAlert(alert: alert)
    }
    
    func presentAttempLimitError(tfaError: RequestError, retryTime: Int, tfaType: TFAType) {
        let alert = Alert(title: tfaError.title, text: tfaError.message)
        alert.buttons = [Button(title: DefaultLocalizable.back.text, type: .cta)]
        
        Analytics.shared.log(TFAEvents.didReceivedExceededDataValidationLimitError(tfaType: tfaType, currentStep: .code))
        self.viewController?.displayAttempLimitError(alert: alert, retryTime: retryTime)
    }
    
    func validateCode(with type: TFAType, nextStep: TFAStep) {
        coordinator.perform(action: .codeValidateAndStartNextStep(tfaType: type, nextStep: nextStep))
    }
}

private extension NewTFACodeFormPresenter {
    func attributedString(for destination: String?, flow: TFAType) -> NSAttributedString {
        switch flow {
        case .sms:
            guard
                let sentTo = destination,
                !sentTo.isEmpty else {
                    return NSMutableAttributedString(
                        string: TFALocalizable.smsNotificationSentGeneric.text,
                        attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .light)]
                    )
            }
            
            return phoneNumberAttributedString(sentTo)
        case .authorizedDevice, .device:
            return NSMutableAttributedString(
                string: TFALocalizable.oldDeviceNotificationSent.text,
                attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .light)]
            )
        case .email:
            guard
                let sentTo = destination,
                !sentTo.isEmpty else {
                    return NSMutableAttributedString(
                        string: TFALocalizable.emailNotificationSentGeneric.text,
                        attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .light)]
                    )
            }
            return emailAttributedString(sentTo)
            
        default:
            return NSAttributedString(string: "", attributes: nil)
        }
    }
    
    func phoneNumberAttributedString(_ phoneNumber: String) -> NSAttributedString {
        let completeText = String(format: TFALocalizable.smsNotificationWithMaskPhone.text, phoneNumber)
        let attributedString = NSMutableAttributedString(string: completeText)
        
        let numberRange = (completeText as NSString).range(of: phoneNumber)
        
        attributedString.addAttributes(
            [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .light)],
            range: completeText.fullRange
        )
        attributedString.addAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16)], range: numberRange)
        
        return attributedString
    }
    
    func emailAttributedString(_ email: String) -> NSAttributedString {
        let completeText = String(format: TFALocalizable.emailNotificationWithMaskEmail.text, email)
        let attributedString = NSMutableAttributedString(string: completeText)
        
        let emailRange = (completeText as NSString).range(of: email)
        
        attributedString.addAttributes(
            [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .light)],
            range: completeText.fullRange
        )
        attributedString.addAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16)], range: emailRange)
        
        return attributedString
    }
    
    func enabledAttributedButtonText() -> NSAttributedString {
        let attributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 14),
            .foregroundColor: Palette.ppColorBranding300.color,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        return NSMutableAttributedString(string: TFALocalizable.resendButtonText.text, attributes: attributes)
    }
    
    func disabledAttributedButtonText(with remainingTime: Int) -> NSAttributedString {
        let attributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 14),
            .foregroundColor: Palette.ppColorGrayscale400.color
        ]
        
        let boldAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.boldSystemFont(ofSize: 14),
            .foregroundColor: Palette.ppColorGrayscale400.color
        ]
        
        let formatedTime = tfaTimerformatter.timeString(with: TimeInterval(remainingTime))
        let completeText = TFALocalizable.didNotReceivedCode.text + "\(formatedTime)"
        let boldText = "\(formatedTime)"
        let rangeOfBoldText = (completeText as NSString).range(of: boldText)
        
        let attributedText = NSMutableAttributedString(string: completeText, attributes: attributes)
        attributedText.addAttributes(boldAttributes, range: rangeOfBoldText)
        return attributedText
    }
}
