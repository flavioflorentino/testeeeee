import AnalyticsModule
import Core
import Foundation

protocol NewTFACodeFormViewModelInputs: AnyObject {
    func loadInfo()
    func reloadCode()
    func updateCodeTextWith(_ text: String)
    func validateCode(_ code: String)
    func startTimerForNewRequest(_ remainingTime: Int)
}

final class NewTFACodeFormViewModel {
    private let service: NewTFACodeFormServicing
    private let presenter: NewTFACodeFormPresenting
    private let tfaRequestData: TFARequestData
    
    private let validCodeLength = 4
    private var timer: RepeatingTimer?
    private var endDate = Date(timeInterval: 60, since: Date())
    
    init(
        service: NewTFACodeFormServicing,
        presenter: NewTFACodeFormPresenting,
        tfaRequestData: TFARequestData,
        timer: RepeatingTimer? = RepeatingTimer(timeInterval: 1)
    ) {
        self.service = service
        self.presenter = presenter
        self.tfaRequestData = tfaRequestData
        self.timer = timer
    }
}

extension NewTFACodeFormViewModel: NewTFACodeFormViewModelInputs {
    func loadInfo() {
        loadNewTFACodeData()
    }
    
    func reloadCode() {
        Analytics.shared.log(TFAEvents.codeResended(tfaRequestData.flow))
        loadNewTFACodeData()
    }
    
    func updateCodeTextWith(_ text: String) {
        if text.length <= validCodeLength {
            presenter.updateCodeTextFieldWith(text)
            checkCodeLength(text)
        }
    }
    
    func validateCode(_ code: String) {
        validateTFACodeForNextStep(code)
    }
    
    func startTimerForNewRequest(_ remainingTime: Int) {
        endDate = Date(timeInterval: TimeInterval(remainingTime), since: Date())
        runCountDown()
    }
}

private extension NewTFACodeFormViewModel {
    func loadNewTFACodeData() {
        presenter.presentLoading(true)
        service.getNewCodeDataInfo(tfaRequestData: tfaRequestData) { [weak self] (result: Result<NewTFACode, ApiError>) in
            guard let self = self else {
                return
            }
            self.presenter.presentLoading(false)
            
            switch result {
            case let .success(code):
                self.presenter.presentDescriptionAttributedText(for: code.sentTo, flow: self.tfaRequestData.flow)
                self.startTimerForNewRequest(code.retryTime)
            case let .failure(error):
                self.presenter.presentDescriptionAttributedText(for: nil, flow: self.tfaRequestData.flow)
                self.presenter.presentRetryButtonStatus(true, remainingTime: 0)
                self.parseError(error: error)
            }
        }
    }
  
    func validateTFACodeForNextStep(_ code: String) {
        presenter.presentLoading(true)
        
        service.validateCodeForNextStep(
            code: code,
            tfaRequestData: tfaRequestData
        ) { [weak self] (result: Result<TFAValidationResponse, ApiError>) in
            guard let self = self else {
                return
            }
            self.presenter.presentLoading(false)
            
            switch result {
            case let .success(response):
                self.presenter.validateCode(with: self.tfaRequestData.flow, nextStep: response.nextStep)
            case let .failure(error):
                self.parseError(error: error)
            }
        }
    }
    
    func parseError(error: ApiError) {
        switch error {
        case .tooManyRequests(let requestError), .unauthorized(let requestError), .badRequest(let requestError):
            guard
                let errorData = requestError.data?.dictionaryObject?["data"] as? [String: Any],
                let retryTime = errorData["retry_time"] as? Int
                else {
                    presenter.presentRequestError(tfaError: requestError)
                    return
            }
            
            presenter.presentAttempLimitError(tfaError: requestError, retryTime: retryTime, tfaType: tfaRequestData.flow)
        default:
            presenter.presentGenericError()
        }
    }
    
    func checkCodeLength(_ code: String) {
        guard code.length == validCodeLength else {
            return
        }
        presenter.presentCodeValidationWith(code)
    }
    
    func stopTimer() {
        Analytics.shared.log(TFAEvents.exceededCodeValidationTime(type: tfaRequestData.flow))
        timer?.eventHandler = nil
        timer?.suspend()
        timer = nil
    }
    
    func runCountDown() {
        if timer == nil {
            timer = RepeatingTimer(timeInterval: 1)
        }
        
        timer?.eventHandler = { [weak self] in
            self?.updateTimer()
        }
        timer?.resume()
    }
    
    func updateTimer() {
        let endTime = self.endDate.timeIntervalSince(Date())
        if endTime <= 0 {
            self.presenter.presentRetryButtonStatus(true, remainingTime: 0)
            stopTimer()
            return
        }
        
        self.presenter.presentRetryButtonStatus(false, remainingTime: Int(endTime))
    }
}
