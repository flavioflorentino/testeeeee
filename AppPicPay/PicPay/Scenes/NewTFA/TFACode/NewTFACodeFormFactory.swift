import Foundation

enum NewTFACodeFormFactory {
    static func make(
        with coordinatorDelegate: NewTFACodeFormCoordinatorDelegate,
        tfaRequestData: TFARequestData
    ) -> NewTFACodeFormViewController {
        let container = DependencyContainer()
        let service: NewTFACodeFormServicing = NewTFACodeFormService(dependencies: container)
        let coordinator: NewTFACodeFormCoordinating = NewTFACodeFormCoordinator()
        let presenter: NewTFACodeFormPresenting = NewTFACodeFormPresenter(coordinator: coordinator)
        let viewModel = NewTFACodeFormViewModel(
            service: service,
            presenter: presenter,
            tfaRequestData: tfaRequestData
        )
        let viewController = NewTFACodeFormViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        coordinator.delegate = coordinatorDelegate
        presenter.viewController = viewController

        return viewController
    }
}
