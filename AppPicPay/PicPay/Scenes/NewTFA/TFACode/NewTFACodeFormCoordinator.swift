import UIKit

enum NewTFACodeFormAction {
    case codeValidateAndStartNextStep(tfaType: TFAType, nextStep: TFAStep)
}

protocol NewTFACodeFormCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: NewTFACodeFormCoordinatorDelegate? { get set }
    func perform(action: NewTFACodeFormAction)
}

protocol NewTFACodeFormCoordinatorDelegate: AnyObject {
    func didValidateCode(with tfaType: TFAType, nextStep: TFAStep)
}

final class NewTFACodeFormCoordinator: NewTFACodeFormCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: NewTFACodeFormCoordinatorDelegate?
    
    func perform(action: NewTFACodeFormAction) {
        switch action {
        case let .codeValidateAndStartNextStep(tfaType, nextStep):
            delegate?.didValidateCode(with: tfaType, nextStep: nextStep)
        }
    }
}
