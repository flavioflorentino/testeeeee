import AnalyticsModule
import FeatureFlag
import IdentityValidation
import UI
import UIKit

public final class TFAFlowCoordinator {
    private let navigation: CustomPopNavigationController
    weak var presenter: UIViewController?
    var id: String
    var onFinish: ((_ completed: Bool) -> Void) = { _ in }
    private var currentStep: TFAStep = .initial
    var identityValidationCoordinator: IDValidationCoordinatorProtocol?
    
    init(
        id: String,
        presenter: UIViewController,
        navigation: CustomPopNavigationController = CustomPopNavigationController(),
        onFinish: @escaping ((_ completed: Bool) -> Void)
    ) {
        self.id = id
        self.presenter = presenter
        self.onFinish = onFinish
        
        navigation.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigation.navigationBar.shadowImage = UIImage()
        navigation.navigationBar.isTranslucent = false
        navigation.navigationBar.backgroundColor = .clear
        navigation.navigationBar.barTintColor = Palette.ppColorGrayscale000.color
        navigation.modalPresentationStyle = .fullScreen
        self.navigation = navigation
        navigation.popDelegate = self
    }
    
    public func start() {
        let onboardingViewController = NewTFAOnboardingFactory.make(with: self)
        
        navigation.viewControllers = [onboardingViewController]
        presenter?.present(navigation, animated: true, completion: nil)
    }
    
    func finish(completed: Bool) {
        navigation.dismissModalStack(animated: true, completion: nil)
        onFinish(completed)
    }
    
    func showOnboardInfo() {
        let helpController = NewTFAInfoFactory.make()
        
        navigation.pushViewController(helpController, animated: true)
        Analytics.shared.log(TFAEvents.helpAccessed)
    }

    func showVerificationOptions() {
        let optionsViewController = NewTFAOptionsFactory.make(
            with: self,
            tfaRequestData: TFARequestData(flow: .options, step: .initial, id: id)
        )
        navigation.pushViewController(optionsViewController, animated: true)
    }
    
    fileprivate func showCodeVerification(type: TFAType, step: TFAStep) {
        let codeVC = NewTFACodeFormFactory.make(with: self, tfaRequestData: TFARequestData(flow: type, step: step, id: id))
        navigation.pushViewController(codeVC, animated: true)
    }
    
    func showPersonalDataVerification(with tfaType: TFAType, step: TFAStep) {
        let personalDataVC = TFAPersonalDataCheckFactory.make(
            with: self,
            tfaRequestData: TFARequestData(flow: tfaType, step: step, id: id)
        )
        navigation.pushViewController(personalDataVC, animated: true)
    }
    
    func showAccountInfoVerification(type: TFAType, step: TFAStep) {
        let bankAccountCheckVC = BankAccountCheckFactory.make(
            tfaRequestData: TFARequestData(flow: type, step: step, id: id),
            coordinatorDelegate: self
        )
        navigation.pushViewController(bankAccountCheckVC, animated: true)
    }
    
    func showPhoneNumberVerification(with tfaType: TFAType, step: TFAStep) {
        let phoneNumberVC = PhoneNumberVerificationFactory.make(
            tfaRequestData: TFARequestData(flow: tfaType, step: step, id: id),
            coordinatorDelegate: self
        )
        navigation.pushViewController(phoneNumberVC, animated: true)
    }
    
    func showCreditCardVerification(with tfaType: TFAType, step: TFAStep) {
        let vc = TFACreditCardFactory.make(with: TFARequestData(flow: tfaType, step: step, id: id), coordinatorDelegate: self)
        navigation.pushViewController(vc, animated: true)
    }
    
    func showSuccessScreenFor(_ flow: TFAType) {
        Analytics.shared.log(TFAEvents.deviceAuthorized(flow))
        let successVC = TFASuccessFactory.make(with: self)
        successVC.modalPresentationStyle = .fullScreen
        navigation.present(successVC, animated: true)
    }
    
    func showCreditCardValidation(with card: TFACreditCardData) {
        let vc = TFACreditCardValidateFactory.make(
            with: card,
            tfaRequestData: TFARequestData(flow: .creditCard, step: currentStep, id: id),
            coordinatorDelegate: self
        )
        navigation.pushViewController(vc, animated: true)
    }
    
    func showTFAValidationStatus(withStatus status: TFAStatus) {
        let vc = TFAValidationStatusFactory.make(withStatus: status, coordinatorDelegate: self)
        navigation.pushViewController(vc, animated: true)
    }
    
    func presentValidation(for flow: TFAType, step: TFAStep) {
        currentStep = step
        DispatchQueue.main.async {
            switch step {
            case .userData:
                self.showPersonalDataVerification(with: flow, step: step)
            case .bankData:
                self.showAccountInfoVerification(type: flow, step: step)
            case .phoneNumber:
                self.showPhoneNumberVerification(with: flow, step: step)
            case .code:
                self.showCodeVerification(type: flow, step: step)
            case .creditCard:
                self.showCreditCardVerification(with: flow, step: step)
            case .final:
                self.showSuccessScreenFor(flow)
            default:
                break
            }
        }
    }
    
    @objc
    private func pop() {
        let back = Button(title: TFALocalizable.continueAuthorization.text, type: .cta)
        let confirm = Button(title: TFALocalizable.cancelAuthorization.text, type: .cleanUnderlined) { popup, _ in
            popup.dismiss(animated: true) { [weak self] in
                self?.dismissCurrentStepViewController()
            }
        }
        
        let alert = Alert(
            with: TFALocalizable.exitAuhorizationAlertTitle.text,
            text: TFALocalizable.exitAuhorizationAlertMessage.text,
            buttons: [back, confirm],
            image: nil
        )
        
        Analytics.shared.log(TFAEvents.didSelectBackButton(step: currentStep))
        AlertMessage.showAlert(alert, controller: navigation)
    }
    
    private func dismissCurrentStepViewController() {
        Analytics.shared.log(TFAEvents.didConfirmBackButton(step: currentStep))
        guard
            let onboardingVC = navigation.viewControllers.first(where: { $0.isKind(of: NewTFAOnboardingViewController.self) }),
            let optionsVC = navigation.viewControllers.first(where: { $0.isKind(of: NewTFAOptionsViewController.self) }) else {
            navigation.popToRootViewController(animated: true)
            return
        }
              
        currentStep = .initial
        navigation.setViewControllers([onboardingVC, optionsVC], animated: true)
    }
    
    private func processValidationStatus(_ status: IDValidationStatus?) {
        guard let validationStatus = status else {
            return
        }
        
        switch validationStatus {
        case .verified:
            showTFAValidationStatus(withStatus: .approved)
        case .inconclusive:
            showTFAValidationStatus(withStatus: .inconclusive)
        case .rejected:
            showTFAValidationStatus(withStatus: .denied)
        default:
            showTFAValidationStatus(withStatus: .waitingAnalysis)
        }
    }
}

extension TFAFlowCoordinator: NewTFAOnboardingCoordinatorDelegate {
    func onboardingClose() {
        finish(completed: false)
    }
    
    func onboardingShowHelp() {
        showOnboardInfo()
    }
    
    func onboardingNextStep() {
        showVerificationOptions()
    }
}

extension TFAFlowCoordinator: NewTFAOptionsCoordinatorDelegate {
    func didSelectOption(option: NewTFAOption) {
        presentValidation(for: option.flow, step: option.firstStep)
    }
    
    func didSelectValidationId(validationData: TFASelfieValidationData, option: NewTFAOption) {
        identityValidationCoordinator = IDValidationFlowCoordinator(
            from: self.navigation,
            token: validationData.selfie.token,
            flow: validationData.selfie.flow,
            completion: { [weak self] validationStatus in
                self?.processValidationStatus(validationStatus)
            }
        )
        
        identityValidationCoordinator?.start()
    }
    
    func didReceivedBlockedError() {
        finish(completed: false)
    }
}

extension TFAFlowCoordinator: TFAPersonalDataCheckCoordinatorDelegate {
    func didValidatePersonalData(with tfaType: TFAType, nextStep: TFAStep) {
        presentValidation(for: tfaType, step: nextStep)
    }
}

extension TFAFlowCoordinator: BankAccountCheckCoordinatorDelegate {
    func didValidateAccountData(with tfaType: TFAType, nextStep: TFAStep) {
        presentValidation(for: tfaType, step: nextStep)
    }
}

extension TFAFlowCoordinator: PhoneNumberVerificationCoordinatorDelegate {
    func didValidatePhoneNumber(with tfaType: TFAType, nextStep: TFAStep) {
        presentValidation(for: tfaType, step: nextStep)
    }
}

extension TFAFlowCoordinator: NewTFACodeFormCoordinatorDelegate {
    func didValidateCode(with tfaType: TFAType, nextStep: TFAStep) {
        presentValidation(for: tfaType, step: nextStep)
    }
}

extension TFAFlowCoordinator: TFACreditCardCoordinatorDelegate {
    func didTryAnotherAuthorizationMethod() {
        dismissCurrentStepViewController()
    }
    
    func didTryValidateTfaCard(with card: TFACreditCardData) {
        showCreditCardValidation(with: card)
    }
}

extension TFAFlowCoordinator: TFASuccessCoordinatorDelegate {
    func didEnter() {
        finish(completed: true)
    }
}

extension TFAFlowCoordinator: TFACreditCardValidateCoordinatorDelegate {
    func didValidateCard(nextStep: TFAStep) {
        presentValidation(for: .creditCard, step: nextStep)
    }
}

extension TFAFlowCoordinator: TFAValidationStatusCoordinatorDelegate {
    func didReceivedApprovedStatus() {
        finish(completed: true)
    }
    
    func presentTFAOptions() {
        navigation.popToRootViewController(animated: false)
        currentStep = .initial
        showVerificationOptions()
    }
    
    func closeTFAFlow() {
        finish(completed: false)
    }
}

extension TFAFlowCoordinator: CustomPopNavigationControllerDelegate {
    func shouldPop() -> Bool {
        guard currentStep != .initial else {
            return true
        }

        pop()
        return false
    }
}
