enum FavoriteState {
    case favorited
    case unfavorited
    case unavailable
}

protocol FavoriteInteracting {
    func checkState(id: String, type: Favorite.`Type`, _ completion: @escaping (FavoriteState) -> Void)
    func changeState(
        _ state: FavoriteState,
        id: String,
        type: Favorite.`Type`,
        _ completion: @escaping (FavoriteState) -> Void
    )
}
