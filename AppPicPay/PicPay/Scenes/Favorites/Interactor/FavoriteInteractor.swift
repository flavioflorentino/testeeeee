import FeatureFlag

struct FavoriteInteractor {
    typealias Dependencies = HasFeatureManager

    let service: FavoritesServicing
    let dependencies: Dependencies
}

extension FavoriteInteractor: FavoriteInteracting {
    func checkState(id: String, type: Favorite.`Type`, _ completion: @escaping (FavoriteState) -> Void) {
        guard isFavoriteTypeAvailable(type), id.isNotEmpty else {
            return completion(.unavailable)
        }

        service.isFavorite(id: id, type: type) { isFavorite in
            completion(isFavorite ? .favorited : .unfavorited)
        }
    }

    func changeState(
        _ state: FavoriteState,
        id: String,
        type: Favorite.`Type`,
        _ completion: @escaping (FavoriteState) -> Void
    ) {
        switch state {
        case .favorited:
            unfavorite(id: id, type: type, completion)
        case .unfavorited:
            favorite(id: id, type: type, completion)
        default:
            completion(.unavailable)
        }
    }
}

private extension FavoriteInteractor {
    func isFavoriteTypeAvailable(_ type: Favorite.`Type`) -> Bool {
        switch type {
        case .action, .consumer, .digitalGood, .store:
            return true
        default:
            return false
        }
    }

    func favorite(id: String, type: Favorite.`Type`, _ completion: @escaping (FavoriteState) -> Void) {
        service.favorite(id: id, type: type) { isFavorite in
            completion(isFavorite ? .favorited : .unfavorited)
        }
    }

    func unfavorite(id: String, type: Favorite.`Type`, _ completion: @escaping (FavoriteState) -> Void) {
        service.unfavorite(id: id, type: type) { isUnfavorite in
            completion(isUnfavorite ? .unfavorited : .favorited)
        }
    }
}
