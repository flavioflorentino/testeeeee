import Core
import Dispatch

protocol FavoritesServicing {
    func favorites(completion: @escaping (Result<[Favorite], ApiError>) -> Void)
    func homeFavorites(completion: @escaping (Result<[HomeFavorites], ApiError>) -> Void)

    func favorite(id: String, type: Favorite.`Type`, completion: @escaping (Bool) -> Void)
    func unfavorite(id: String, type: Favorite.`Type`, completion: @escaping (Bool) -> Void)

    func isFavorite(id: String, type: Favorite.`Type`, completion: @escaping (Bool) -> Void)
}

extension NSNotification.Name {
    static var reloadFavoriteSection: NSNotification.Name { .init("ReloadFavoritesSection") }
}

struct FavoritesService {
    typealias Dependencies = HasMainQueue

    // MARK: - Properties
    private let dependencies: Dependencies

    // MARK: - Initialization
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
}

extension FavoritesService: FavoritesServicing {
    func favorites(completion: @escaping (Result<[Favorite], ApiError>) -> Void) {
        let endpoint = FavoritesServiceEndpoint.list
        Api<[Favorite]>(endpoint: endpoint).execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { result in
            self.dependencies.mainQueue.async {
                completion(
                    result.map { $0.model }
                )
            }
        }
    }

    func homeFavorites(completion: @escaping (Result<[HomeFavorites], ApiError>) -> Void) {
        let endpoint = FavoritesServiceEndpoint.home
        Api<[HomeFavorites]>(endpoint: endpoint).execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { result in
            self.dependencies.mainQueue.async {
                completion(
                    result.map { $0.model }
                )
            }
        }
    }

    func favorite(id: String, type: Favorite.`Type`, completion: @escaping (Bool) -> Void) {
        Api<NoContent>(endpoint: FavoritesServiceEndpoint.favorite(id: id, type: type.value)).execute { result in
            self.dependencies.mainQueue.async {
                switch result {
                case .success:
                    NotificationCenter.default.post(
                        name: NSNotification.Name.reloadFavoriteSection,
                        object: nil
                    )
                    completion(true)
                case .failure:
                    completion(false)
                }
            }
        }
    }

    func unfavorite(id: String, type: Favorite.`Type`, completion: @escaping (Bool) -> Void) {
        Api<NoContent>(endpoint: FavoritesServiceEndpoint.unfavorite(id: id, type: type.value)).execute { result in
            self.dependencies.mainQueue.async {
                switch result {
                case .success:
                    NotificationCenter.default.post(
                        name: NSNotification.Name.reloadFavoriteSection,
                        object: nil
                    )
                    completion(true)
                case .failure:
                    completion(false)
                }
            }
        }
    }

    func isFavorite(id: String, type: Favorite.`Type`, completion: @escaping (Bool) -> Void) {
        Api<NoContent>(endpoint: FavoritesServiceEndpoint.check(id: id, type: type.value)).execute { result in
            self.dependencies.mainQueue.async {
                switch result {
                case .success:
                    completion(true)
                case .failure:
                    completion(false)
                }
            }
        }
    }
}

private extension Favorite.`Type` {
    var value: String {
        switch self {
        case .store:
            return "seller"
        default:
            return rawValue
        }
    }
}
