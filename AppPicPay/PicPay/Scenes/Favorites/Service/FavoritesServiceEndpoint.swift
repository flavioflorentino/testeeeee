import Core

enum FavoritesServiceEndpoint {
    case home
    case list
    case favorite(id: String, type: String)
    case unfavorite(id: String, type: String)
    case check(id: String, type: String)
}

extension FavoritesServiceEndpoint: ApiEndpointExposable {
    var path: String {
        let base = "favorites"

        switch self {
        case .list,
             .favorite:
            return base

        case let .unfavorite(id, type),
             let .check(id, type):
            return "\(base)/\(type)/\(id)"

        case .home:
            return "\(base)/home"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .home, .list, .check:
            return .get
        case .favorite:
            return .post
        case .unfavorite:
            return .delete
        }
    }
    
    var body: Data? {
        switch self {
        case let .favorite(id, type):
            return ["type": type, "id": id].toData()
        default:
            return nil
        }
    }
}
