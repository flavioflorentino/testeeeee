import Core

protocol FavoritesOnboardingPresenting: AnyObject {
    var viewController: FavoritesOnboardingDisplay? { get set }
    func didNextStep(action: FavoritesOnboardingAction)
    func updateView(withImages images: [UIImage])
}

final class FavoritesOnboardingPresenter: FavoritesOnboardingPresenting {
    private let coordinator: FavoritesOnboardingCoordinating
    var viewController: FavoritesOnboardingDisplay?

    init(coordinator: FavoritesOnboardingCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: FavoritesOnboardingAction) {
        coordinator.perform(action: action)
    }
    
    func updateView(withImages images: [UIImage]) {
        viewController?.updateCarousel(withImages: images)
    }
}
