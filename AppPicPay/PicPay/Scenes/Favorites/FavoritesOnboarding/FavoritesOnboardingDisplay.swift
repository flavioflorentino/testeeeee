import UIKit

protocol FavoritesOnboardingDisplay: AnyObject {
    func updateCarousel(withImages images: [UIImage])
}
