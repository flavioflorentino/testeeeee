import UI

final class FavoritesDeeplinkHelper {
    typealias Dependencies = HasAppManager
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

extension FavoritesDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        setupFavoritesOnboarding()
    }
}

private extension FavoritesDeeplinkHelper {
    private func setupFavoritesOnboarding() -> Bool {
        let controller = FavoritesOnboardingFactory.make()
        controller.hidesBottomBarWhenPushed = true
        dependencies.appManager.mainScreenCoordinator?.showHomeScreen()
        guard let currentController = dependencies.appManager.mainScreenCoordinator?.currentController() else {
            return false
        }
        currentController.navigationController?.pushViewController(controller, animated: true)
        return true
    }
}
