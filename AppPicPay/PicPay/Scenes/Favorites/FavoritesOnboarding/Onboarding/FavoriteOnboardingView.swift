import AnalyticsModule
import UI

protocol FavoritesOnboardingViewDelegate: AnyObject {
    func didTapGoToSearchButton()
}

final class FavoriteOnboardingView: UIView {
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 30.0)
        label.text = FavoriteLocalizable.favoriteOnboardingTitle.text
        label.textColor = Palette.ppColorGrayscale600.color
        return label
    }()
    
    lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 14.0)
        label.textColor = Palette.ppColorGrayscale600.color

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.26

        let attributedText = NSAttributedString(
            string: FavoriteLocalizable.favoriteOnboardingDescription.text,
            attributes: [.paragraphStyle: paragraphStyle]
        )

        label.attributedText = attributedText

        return label
    }()
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.showsHorizontalScrollIndicator = false
        collection.backgroundColor = .clear
        collection.register(FavoriteOnboardingCell.self, forCellWithReuseIdentifier: "favoriteOnboardingCell")
        collection.dataSource = self
        collection.delegate = self
        collection.isPagingEnabled = true
        return collection
    }()
    
    lazy var pageControl: UIPageControl = {
        let view = UIPageControl()
        view.currentPage = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        view.currentPageIndicatorTintColor = Palette.ppColorBranding300.color
        view.pageIndicatorTintColor = Palette.ppColorBranding300.color.withAlphaComponent(0.5)
        view.backgroundColor = .clear
        return view
    }()
    
    lazy var button: UIPPButton = {
        let button = UIPPButton(title: "Ir para a busca", target: nil, action: #selector(goToSearch), type: .cta)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = Palette.ppColorBranding300.color
        return button
    }()
    
    private var onboardingImages: [UIImage]?
    weak var delegate: FavoritesOnboardingViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupView() {
        Analytics.shared.log(FavoritesEvent.favoritesOnboardingCarouselView(.search))
        
        backgroundColor = Palette.ppColorGrayscale000.color
        
        addSubview(titleLabel)
        addSubview(descriptionLabel)
        addSubview(collectionView)
        addSubview(pageControl)
        addSubview(button)
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: compatibleSafeAreaLayoutGuide.topAnchor, constant: 20),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            titleLabel.heightAnchor.constraint(equalToConstant: 30)
        ])
        
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20)
        ])

        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 30),
            collectionView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            collectionView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            collectionView.bottomAnchor.constraint(equalTo: pageControl.topAnchor, constant: -10),
            collectionView.heightAnchor.constraint(equalToConstant: 230)
        ])
        
        NSLayoutConstraint.activate([
            pageControl.centerXAnchor.constraint(equalTo: centerXAnchor),
            pageControl.heightAnchor.constraint(equalToConstant: 20)
        ])
        
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: pageControl.bottomAnchor, constant: 30),
            button.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            button.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            button.heightAnchor.constraint(equalToConstant: 44)
        ])
    }
    
    func setOnboardingImages(images: [UIImage]) {
        onboardingImages = images
        pageControl.numberOfPages = onboardingImages?.count ?? 0
        collectionView.reloadData()
    }
    
    @objc
    private func goToSearch() {
        delegate?.didTapGoToSearchButton()
        Analytics.shared.log(FavoritesEvent.favoritesOnboardingSearchAccessed)
    }
}

extension FavoriteOnboardingView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        onboardingImages?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "favoriteOnboardingCell", for: indexPath)
                as? FavoriteOnboardingCell else {
            return UICollectionViewCell()
        }
        cell.imageView.image = onboardingImages?[indexPath.row]
        return cell
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func scrollViewWillEndDragging(
        _ scrollView: UIScrollView,
        withVelocity velocity: CGPoint,
        targetContentOffset: UnsafeMutablePointer<CGPoint>
    ) {
        let currentPage = pageControl.currentPage

        let x = targetContentOffset.pointee.x
        pageControl.currentPage = Int(round(Double(x / frame.width)))

        guard currentPage != pageControl.currentPage else {
            return
        }

        let eventTypes: [FavoritesEvent.FavoritesEventType] = [.search, .profile, .store, .digitalGoods]
        guard pageControl.currentPage < eventTypes.count else {
            return
        }

        let eventType = eventTypes[pageControl.currentPage]
        Analytics.shared.log(FavoritesEvent.favoritesOnboardingCarouselView(eventType))
    }
}
