import Foundation

enum FavoriteLocalizable: String, Localizable {
    case favorites
    
    case favoriteSegmentedControlOpt1
    case favoriteSegmentedControlOpt2
    case favoriteSegmentedControlNoOpt
    
    case favoriteOnboardingTitle
    case favoriteOnboardingDescription
    
    case favoriteLoadingErrorViewButtonTitle
    
    var key: String {
        rawValue
    }
    
    var file: LocalizableFile {
        .favorite
    }
}
