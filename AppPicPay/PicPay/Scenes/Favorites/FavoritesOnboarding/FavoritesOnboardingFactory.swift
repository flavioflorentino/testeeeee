import Foundation

enum FavoritesOnboardingFactory {
    static func make() -> FavoritesOnboardingViewController {
        let container = DependencyContainer()
        let service: FavoritesServicing = FavoritesService()
        let coordinator: FavoritesOnboardingCoordinating = FavoritesOnboardingCoordinator(dependencies: container)
        let presenter: FavoritesOnboardingPresenting = FavoritesOnboardingPresenter(coordinator: coordinator)
        let viewModel = FavoritesOnboardingViewModel(service: service, presenter: presenter)
        let viewController = FavoritesOnboardingViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
