import FeatureFlag
import Foundation
import UIKit

protocol FavoritesOnboardingViewModelInputs: AnyObject {
    func willAppear()
    func goToSearch()
}

final class FavoritesOnboardingViewModel {
    private let service: FavoritesServicing
    private let presenter: FavoritesOnboardingPresenting

    init(service: FavoritesServicing, presenter: FavoritesOnboardingPresenting) {
        self.service = service
        self.presenter = presenter
    }
    
    private func setupOnboardingImages() {
        var onboardingImages: [UIImage] = []

        onboardingImages.append(Assets.Favorite.favoriteOnboardingList.image)
        onboardingImages.append(Assets.Favorite.favoriteOnboardingProfile.image)
        onboardingImages.append(Assets.Favorite.favoriteOnboardingBiz.image)
        onboardingImages.append(Assets.Favorite.favoriteOnboardingDg.image)
        
        presenter.updateView(withImages: onboardingImages)
    }
}

extension FavoritesOnboardingViewModel: FavoritesOnboardingViewModelInputs {
    func willAppear() {
        setupOnboardingImages()
    }
    
    func goToSearch() {
        presenter.didNextStep(action: .goToSearch)
    }
}
