import SnapKit
import UI
import UIKit

protocol FavoritesOnboardingLoadingErrorViewDelegate: AnyObject {
    func reloadFavoritesCollection()
}

extension FavoritesOnboardingLoadingErrorView.Layout {
    enum Size {
        static let buttonWidth: CGFloat = 190
    }
}

final class FavoritesOnboardingLoadingErrorView: UIView, ViewConfiguration {
    fileprivate enum Layout { }
    
    private lazy var refreshButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(FavoriteLocalizable.favoriteLoadingErrorViewButtonTitle.text, for: .normal)
        button.buttonStyle(SecondaryButtonStyle(size: .default, icon: (name: .refresh, alignment: .left)))
        button.addTarget(self, action: #selector(didTapRefreshButton), for: .touchUpInside)
        return button
    }()
    
    weak var delegate: FavoritesOnboardingLoadingErrorViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(refreshButton)
    }
    
    func setupConstraints() {
        refreshButton.snp.makeConstraints {
            $0.width.equalTo(Layout.Size.buttonWidth)
            $0.center.equalToSuperview()
        }
    }
    
    @objc
    private func didTapRefreshButton() {
        delegate?.reloadFavoritesCollection()
    }
}
