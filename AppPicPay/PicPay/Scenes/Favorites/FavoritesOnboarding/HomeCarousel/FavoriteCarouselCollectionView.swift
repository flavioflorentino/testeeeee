import AnalyticsModule
import UI
import UIKit

protocol FavoriteCarouselCollectionViewDelegate: AnyObject {
    func didSelect(item: HomeFavorites.Item)
}

final class FavoriteCarouselCollectionView: CarouselCollectionView {
    private(set) var items: [HomeFavorites.Item] = []
    weak var favoritesDelegate: FavoriteCarouselCollectionViewDelegate?
    
    func setup(result items: [HomeFavorites.Item], completion: @escaping () -> Void) {
        DispatchQueue.main.async { [weak self] in
            self?.items = items
            self?.reloadData()
            completion()
        }
    }
    
    override func commmonInit() {
        super.commmonInit()
        register(FavoriteCarouselCell.self, forCellWithReuseIdentifier: String(describing: FavoriteCarouselCell.self))
        
        dataSource = self
        delegate = self
        
        guard let layout = self.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        layout.headerReferenceSize = .zero
        layout.footerReferenceSize = .zero
        layout.itemSize = FavoriteCarouselCell.size
        layout.minimumLineSpacing = Spacing.base00
        layout.minimumInteritemSpacing = .zero
        layout.sectionInset = UIEdgeInsets(
            top: Spacing.base02,
            left: Spacing.base02,
            bottom: Spacing.base02,
            right: Spacing.base02
        )

        self.bounces = true
    }
    
    func reset() {
        items = []
        reloadData()
    }
}

extension FavoriteCarouselCollectionView: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: String(describing: FavoriteCarouselCell.self),
                for: indexPath) as? FavoriteCarouselCell
            else {
            return UICollectionViewCell()
        }

        let item = items[indexPath.row]
        cell.viewModel = FavoriteCarouselCellViewModel(presenter: FavoriteCarouselCellPresenter(), item: item)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = items[indexPath.row]

        sendAnalyticsFor(item: item, at: indexPath)
        favoritesDelegate?.didSelect(item: item)
    }
}

private extension FavoriteCarouselCollectionView {
    func sendAnalyticsFor(item: HomeFavorites.Item, at indexPath: IndexPath) {
        switch item {
        case let .consumer(value):
            Analytics.shared.log(PaymentEvent.itemAccessed(
                type: item.type,
                id: value.id ,
                name: value.name,
                section: ("favoritos", indexPath.row))
            )
        case let .store(value):
            Analytics.shared.log(PaymentEvent.itemAccessed(
                type: item.type,
                id: value.seller.id,
                name: value.seller.name,
                section: ("favoritos", indexPath.row))
            )
        case let .digitalGood(value):
            Analytics.shared.log(PaymentEvent.itemAccessed(
                type: item.type,
                id: value.id,
                name: value.name,
                section: ("favoritos", indexPath.row))
            )
        default:
            break
        }
    }
}
