import FeatureFlag
import Foundation
import UI
import UIKit

// MARK: - Layout
private extension FavoriteCarouselCell.Layout {
    static let size = CGSize(width: 80.0, height: 92.0)

    enum Image {
        static let size = CGSize(width: 54.0, height: 54.0)
        static let margin: CGFloat = 12.0
    }

    enum HightlightBorder {
        static let borderWidth: CGFloat = 2.0
    }
}

final class FavoriteCarouselCell: UICollectionViewCell {
    // MARK: - Nested types
    fileprivate enum Layout { }

    private lazy var containerView = UIView()
    
    private lazy var stackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [])
        view.alignment = .fill
        view.axis = .vertical
        view.distribution = .fill
        return view
    }()
    
    private lazy var itemImage: UIPPProfileImage = {
        let imageView = UIPPProfileImage()
        imageView.backgroundColor = .clear

        imageView.imageView?.viewStyle(BackgroundViewStyle(color: .backgroundPrimary()))
            .with(\.backgroundColor, .clear)
            .with(\.border, Border.Style.medium(color: .grayscale100()))

        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.labelStyle(CaptionLabelStyle())
            .with(\.backgroundColor, .backgroundPrimary())
            .with(\.textColor, .grayscale850())
            .with(\.textAlignment, .center)
            .with(\.lineBreakMode, .byTruncatingTail)
            .with(\.numberOfLines, 2)

        return label
    }()
    
    var viewModel: FavoriteCarouselCellViewModel? {
        didSet {
            update()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        itemImage.imageView?.image = nil
        itemImage.imageView?.cancelRequest()

        itemImage.badgePro?.isHidden = true
        itemImage.badgeVerified?.isHidden = true

        titleLabel.text = ""
    }
    
    func format(title: String, type: FavoritesCellType) -> String {
        guard case .consumer = type else { return title }
        return "@\(title)"
    }
    
    func update() {
        viewModel?.presenter.viewController = self
        viewModel?.configure()
    }
}

extension FavoriteCarouselCell: FavoriteCarouselCellDisplay {
    func configure(title: String, imageUrl: URL?, placeholder: UIImage?, type: FavoritesCellType) {
        titleLabel.text = format(title: title, type: type)
        itemImage.imageView?.setImage(url: imageUrl, placeholder: placeholder)
    }
    
    func displayBadge(hiddenPro: Bool, hiddenVerified: Bool) {
        itemImage.badgePro?.isHidden = hiddenPro
        itemImage.badgeVerified?.isHidden = hiddenVerified
    }
}

// MARK: - Static methods
extension FavoriteCarouselCell {
    static var size: CGSize { Layout.size }
}

// MARK: - Private
extension FavoriteCarouselCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubviews(itemImage, titleLabel)
    }
    
    func setupConstraints() {
        itemImage.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base00)
            $0.leading.greaterThanOrEqualToSuperview().offset(Layout.Image.margin)
            $0.trailing.lessThanOrEqualToSuperview().offset(-Layout.Image.margin)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Image.size)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(itemImage.snp.bottom).offset(Spacing.base00)
            $0.leading.greaterThanOrEqualToSuperview().offset(Spacing.base01)
            $0.trailing.lessThanOrEqualToSuperview().offset(-Spacing.base01)
            $0.bottom.centerX.equalToSuperview()
        }
    }
    
    func configureViews() {
        itemImage.isUserInteractionEnabled = false
    }
}
