import Core

protocol FavoriteCarouselCellViewModelInputs: AnyObject {
    func configure()
}

final class FavoriteCarouselCellViewModel {
    private let item: HomeFavorites.Item
    private(set) var presenter: FavoriteCarouselCellPresenting
    
    init(presenter: FavoriteCarouselCellPresenting, item: HomeFavorites.Item) {
        self.presenter = presenter
        self.item = item
    }
}

extension FavoriteCarouselCellViewModel: FavoriteCarouselCellViewModelInputs {
    func configure() {
        switch item {
        case let .action(value):
            presenter.configureCell(item: value)
        case let .consumer(value):
            presenter.configureCell(item: value)
        case let .digitalGood(value):
            presenter.configureCell(item: value)
        case let .store(value):
            presenter.configureCell(item: value)
        default:
            break
        }
    }
}
