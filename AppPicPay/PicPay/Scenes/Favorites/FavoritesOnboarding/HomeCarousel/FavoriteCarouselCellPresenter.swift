import Core
import UI

public enum FavoritesCellType {
    case action
    case store
    case consumer
    case digitalGood
}

protocol FavoriteCarouselCellPresenting: AnyObject {
    var viewController: FavoriteCarouselCellDisplay? { get set }

    func configureCell(item: HomeFavorites.ActionItem)
    func configureCell(item: HomeFavorites.ConsumerItem)
    func configureCell(item: HomeFavorites.DigitalGoodItem)
    func configureCell(item: HomeFavorites.StoreItem)
}

final class FavoriteCarouselCellPresenter: FavoriteCarouselCellPresenting {
    weak var viewController: FavoriteCarouselCellDisplay?

    func configureCell(item: HomeFavorites.ActionItem) {
        viewController?.configure(
            title: item.name,
            imageUrl: item.imageUrl,
            placeholder: item.placeholderImage,
            type: .action
        )
    }

    func configureCell(item: HomeFavorites.ConsumerItem) {
        viewController?.configure(
            title: item.username,
            imageUrl: item.imageUrlSmall,
            placeholder: Assets.Favorite.homeFavoritePlaceholder.image,
            type: .consumer
        )

        setupBadge(isVerified: item.verified, isPro: item.pro)
    }

    func configureCell(item: HomeFavorites.DigitalGoodItem) {
        viewController?.configure(
            title: item.name,
            imageUrl: item.imageUrl,
            placeholder: Assets.Favorite.homeFavoritePlaceholder.image,
            type: .digitalGood
        )
    }

    func configureCell(item: HomeFavorites.StoreItem) {
        viewController?.configure(
            title: item.store.name,
            imageUrl: item.store.imageUrl,
            placeholder: Assets.Favorite.homeFavoritePlaceholder.image,
            type: .store
        )

        setupBadge(isVerified: item.store.verified, isPro: false)
    }
    
    private func setupBadge(isVerified: Bool, isPro: Bool) {
        let hiddenPro = !isPro
        let hiddenVerified = !isVerified
        
        guard isPro else {
            viewController?.displayBadge(hiddenPro: true, hiddenVerified: hiddenVerified)
            return
        }
        
        viewController?.displayBadge(hiddenPro: hiddenPro, hiddenVerified: true)
    }
}

private extension HomeFavorites.ActionItem {
    var placeholderImage: UIImage? {
        switch action {
        case .allFavorites:
            return Assets.Favorite.listFavorites.image
        case .favoriteOnboarding:
            return Assets.Favorite.addFavorite.image
        default:
            return nil
        }
    }
}
