import UIKit

public protocol FavoriteCarouselCellDisplay: AnyObject {
    func configure(title: String, imageUrl: URL?, placeholder: UIImage?, type: FavoritesCellType)
    func displayBadge(hiddenPro: Bool, hiddenVerified: Bool)
}
