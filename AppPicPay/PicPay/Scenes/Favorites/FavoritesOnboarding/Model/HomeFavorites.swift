import Foundation

struct HomeFavorites: Equatable {
    let id: String
    let title: String
    let view: String
    let items: [Item]
}

// MARK: - Decodable
extension HomeFavorites: Decodable {
}

// MARK: - Private
private extension HomeFavorites {
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case view
        case items = "rows"
    }
}

// MARK: - Nested types
extension HomeFavorites {
    enum Item: Equatable {
        case action(item: ActionItem)
        case consumer(item: ConsumerItem)
        case digitalGood(item: DigitalGoodItem)
        case store(item: StoreItem)
        case unknown(type: String)
    }

    struct ActionItem: Equatable {
        let action: Action
        let name: String
        let imageUrl: URL?
    }

    struct ConsumerItem: Equatable {
        let id: String
        let name: String
        let username: String
        let imageUrlSmall: URL?
        let verified: Bool
        let pro: Bool
    }

    struct DigitalGoodItem: Equatable {
        let id: String
        let name: String
        let description: String
        let service: String
        let sellerId: Int
        let selectionType: String
        let imageUrl: URL?
        let infoUrl: URL?
        let descriptionLargeMarkdown: String
        let bannerImageUrl: URL?
        let studentAccount: Bool
        let screenId: String?
    }

    struct StoreItem: Equatable {
        let store: Store
        let seller: Seller
    }
}

extension HomeFavorites.ActionItem {
    enum Action {
        case allFavorites
        case favoriteOnboarding
        case unknown
    }
}

extension HomeFavorites.StoreItem {
    struct Store: Equatable {
        let id: String
        let name: String
        let sellerId: String
        let imageUrl: URL?
        let address: String
        let verified: Bool
        let isParking: Int
    }

    struct Seller: Equatable {
        let id: String
        let name: String
        let imageUrl: URL?
    }
}

// MARK: - Item
extension HomeFavorites.Item {
    var type: String {
        switch self {
        case .action:
            return AvailableType.action.rawValue
        case .consumer:
            return AvailableType.consumer.rawValue
        case .digitalGood:
            return AvailableType.digitalGood.rawValue
        case .store:
            return AvailableType.store.rawValue
        case let .unknown(type):
            return type
        }
    }
}
extension HomeFavorites.Item: Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let type = try container.decode(String.self, forKey: .type)

        switch type {
        case AvailableType.action.rawValue:
            let action = try container.decode(HomeFavorites.ActionItem.self, forKey: .data)
            self = .action(item: action)

        case AvailableType.consumer.rawValue:
            let consumer = try container.decode(HomeFavorites.ConsumerItem.self, forKey: .data)
            self = .consumer(item: consumer)

        case AvailableType.digitalGood.rawValue:
            let digitalGood = try container.decode(HomeFavorites.DigitalGoodItem.self, forKey: .data)
            self = .digitalGood(item: digitalGood)

        case AvailableType.store.rawValue:
            let store = try container.decode(HomeFavorites.StoreItem.self, forKey: .data)
            self = .store(item: store)

        default:
            self = .unknown(type: type)
        }
    }
}

private extension HomeFavorites.Item {
    enum CodingKeys: String, CodingKey {
        case type
        case data
    }

    enum AvailableType: String {
        case action
        case consumer
        case digitalGood = "digital_good"
        case store
    }
}

// MARK: - ActionItem
extension HomeFavorites.ActionItem: Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let action = Action(rawValue: try container.decode(String.self, forKey: .action))
        let imageUrl = try? container.decode(URL.self, forKey: .imageUrl)
        let name = try container.decode(String.self, forKey: .name)

        self.init(action: action, name: name, imageUrl: imageUrl)
    }
}

private extension HomeFavorites.ActionItem {
    enum CodingKeys: String, CodingKey {
        case action
        case imageUrl
        case name
    }
}

private extension HomeFavorites.ActionItem.Action {
    static let allFavoritesValue = "favorite_all"
    static let favoriteOnboardingValue = "favorite_onboarding"

    init(rawValue: String) {
        switch rawValue {
        case Self.allFavoritesValue:
            self = .allFavorites
        case Self.favoriteOnboardingValue:
            self = .favoriteOnboarding
        default:
            self = .unknown
        }
    }
}

// MARK: - ConsumerItem
extension HomeFavorites.ConsumerItem: Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let id = try container.decode(String.self, forKey: .id)
        let name = try container.decode(String.self, forKey: .name)
        let username = try container.decode(String.self, forKey: .username)
        let imageUrlSmall = try? container.decode(URL.self, forKey: .imageUrlSmall)
        let verified = try container.decode(Bool.self, forKey: .verified)
        let pro = try container.decode(Bool.self, forKey: .pro)

        self.init(id: id, name: name, username: username, imageUrlSmall: imageUrlSmall, verified: verified, pro: pro)
    }
}

private extension HomeFavorites.ConsumerItem {
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case username
        case imageUrlSmall
        case verified
        case pro
    }
}

// MARK: - DigitalGoodItem
extension HomeFavorites.DigitalGoodItem: Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let id = try container.decode(String.self, forKey: .id)
        let name = try container.decode(String.self, forKey: .name)
        let description = try container.decode(String.self, forKey: .description)
        let service = try container.decode(String.self, forKey: .service)
        let sellerId = try container.decode(Int.self, forKey: .sellerId)
        let selectionType = try container.decode(String.self, forKey: .selectionType)
        let imageUrl = try? container.decode(URL.self, forKey: .imageUrl)
        let infoUrl = try? container.decode(URL.self, forKey: .infoUrl)
        let descriptionLargeMarkdown = try container.decode(String.self, forKey: .descriptionLargeMarkdown)
        let bannerImageUrl = try? container.decode(URL.self, forKey: .bannerImageUrl)
        let studentAccount = try container.decode(Bool.self, forKey: .studentAccount)
        let screenId = try? container.decode(String.self, forKey: .screenIdentifier)

        self.init(
            id: id,
            name: name,
            description: description,
            service: service,
            sellerId: sellerId,
            selectionType: selectionType,
            imageUrl: imageUrl,
            infoUrl: infoUrl,
            descriptionLargeMarkdown: descriptionLargeMarkdown,
            bannerImageUrl: bannerImageUrl,
            studentAccount: studentAccount,
            screenId: screenId
        )
    }
}

private extension HomeFavorites.DigitalGoodItem {
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case description
        case service
        case sellerId
        case selectionType
        case imageUrl
        case infoUrl
        case descriptionLargeMarkdown
        case bannerImageUrl
        case studentAccount
        case screenIdentifier
    }
}

// MARK: - StoreItem
extension HomeFavorites.StoreItem: Decodable {
}

private extension HomeFavorites.StoreItem {
    enum CodingKeys: String, CodingKey {
        case store = "Store"
        case seller = "Seller"
    }
}

extension HomeFavorites.StoreItem.Store: Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let id = try container.decode(String.self, forKey: .id)
        let name = try container.decode(String.self, forKey: .name)
        let sellerId = try container.decode(String.self, forKey: .sellerId)
        let imageUrl = try? container.decode(URL.self, forKey: .imageUrl)
        let address = try container.decode(String.self, forKey: .address)
        let verified = try container.decodeIfPresent(Bool.self, forKey: .verified) ?? false
        let isParking = (try? container.decodeIfPresent(Int.self, forKey: .isParking)) ?? 0

        self.init(
            id: id,
            name: name,
            sellerId: sellerId,
            imageUrl: imageUrl,
            address: address,
            verified: verified,
            isParking: isParking
        )
    }

    var isNewParking: Bool {
        let newParkingValue = 2
        return isParking == newParkingValue
    }
}

private extension HomeFavorites.StoreItem.Store {
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case sellerId
        case imageUrl
        case address
        case verified
        case isParking
    }
}

extension HomeFavorites.StoreItem.Seller: Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let id = try container.decode(String.self, forKey: .id)
        let name = try container.decode(String.self, forKey: .name)
        let imageUrl = try? container.decode(URL.self, forKey: .imageUrl)

        self.init(id: id, name: name, imageUrl: imageUrl)
    }
}

private extension HomeFavorites.StoreItem.Seller {
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case imageUrl
    }
}
