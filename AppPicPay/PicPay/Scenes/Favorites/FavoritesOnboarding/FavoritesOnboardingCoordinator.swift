import UIKit

enum FavoritesOnboardingAction {
    case goToSearch
}

protocol FavoritesOnboardingCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: FavoritesOnboardingAction)
}

final class FavoritesOnboardingCoordinator: FavoritesOnboardingCoordinating {
    typealias Dependencies = HasAppManager
    
    weak var viewController: UIViewController?
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func perform(action: FavoritesOnboardingAction) {
        switch action {
        case .goToSearch:
            dependencies.appManager.mainScreenCoordinator?.showSearchScreen(searchText: "")
            viewController?.navigationController?.popViewController(animated: true)
        }
    }
}
