import AnalyticsModule
import UI
import UIKit

protocol FavoritesOnboardingViewControllerDelegate: AnyObject {
    func setSegmentedControlToFavorites()
}

final class FavoritesOnboardingViewController: ViewController<FavoritesOnboardingViewModelInputs, FavoriteOnboardingView> {
    weak var delegate: FavoritesOnboardingViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(false, animated: true)
        rootView.delegate = self

        updateNavigationBarAppearance()
    }
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.willAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        delegate?.setSegmentedControlToFavorites()
        
        if isMovingFromParent {
            Analytics.shared.log(FavoritesEvent.favoritesOnboardingLeft)
        }
    }
}

// MARK: - Delegates
extension FavoritesOnboardingViewController: FavoritesOnboardingDisplay {
    func updateCarousel(withImages images: [UIImage]) {
        rootView.setOnboardingImages(images: images)
    }
}

extension FavoritesOnboardingViewController: FavoritesOnboardingViewDelegate {
    func didTapGoToSearchButton() {
        viewModel.goToSearch()
    }
}

// MARK: - Private
private extension FavoritesOnboardingViewController {
    func updateNavigationBarAppearance() {
        navigationController?.navigationBar.tintColor = Palette.ppColorBranding300.color
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale000.color
    }
}
