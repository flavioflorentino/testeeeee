import AnalyticsModule
import Foundation

protocol FavoritesViewModelInputs: AnyObject {
    func loadData()
    func didSelectFavorite(index: Int)
    func close()
}

final class FavoritesViewModel {
    private let service: FavoritesServicing
    private let presenter: FavoritesPresenting
    
    private var items: [Favorite] = []

    init(service: FavoritesServicing, presenter: FavoritesPresenting) {
        self.service = service
        self.presenter = presenter
    }
}

extension FavoritesViewModel: FavoritesViewModelInputs {
    func loadData() {
        service.favorites { [weak self] result in
            switch result {
            case let .success(items):
                let items = items.filter { $0.type != .unknown }

                self?.items = items
                self?.presenter.listFavorites(items)
            case .failure:
                self?.presenter.popupError()
            }
        }
    }
    
    func didSelectFavorite(index: Int) {
        let item = items[index]

        if let analyticsData = analyticsData(from: item, at: index) {
            sendAnalytics(data: analyticsData)
        }

        switch item {
        case .action:
            presenter.didNextStep(action: .action)
        case let .consumer(data):
            presenter.didNextStep(action: .payment(type: .person(id: data.id)))
        case let .digitalGood(data):
            presenter.didNextStep(action: .payment(type: .digitalGood(item: dgItem(from: data))))
        case let .store(data):
            presenter.didNextStep(action: .payment(type: .store(dictionary: storeDictionary(from: data))))
        default:
            return
        }
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
}

private extension FavoritesViewModel {
    struct AnalyticsData {
        let type: String
        let id: String
        let name: String
        let index: Int
    }

    func analyticsData(from favorite: Favorite, at index: Int) -> AnalyticsData? {
        switch favorite {
        case let .consumer(data):
            return AnalyticsData(type: favorite.type.rawValue, id: data.id, name: data.name, index: index)
        case let .digitalGood(data):
            return AnalyticsData(type: favorite.type.rawValue, id: data.id, name: data.name, index: index)
        case let .store(data):
            return AnalyticsData(type: favorite.type.rawValue, id: "\(data.sellerId)", name: data.name, index: index)
        default:
            return nil
        }
    }

    func sendAnalytics(data: AnalyticsData) {
        Analytics.shared.log(PaymentEvent.itemAccessed(
            type: data.type,
            id: data.id,
            name: data.name,
            section: ("meus favoritos", data.index))
        )
    }

    func dgItem(from data: Favorite.DigitalGoodData) -> DGItem {
        let digitalGood = DGItem(id: data.id, service: DGService(rawValue: data.digitalGood.service))
        digitalGood.name = data.name
        digitalGood.itemDescription = data.description
        digitalGood.sellerId = "\(data.sellerId)"
        digitalGood.displayType = DGDisplayType(rawValue: data.digitalGood.selectionType) ?? .grid
        digitalGood.imgUrl = data.imageUrl?.absoluteString
        digitalGood.infoUrl = data.digitalGood.infoUrl?.absoluteString
        digitalGood.longDescriptionMarkdown = data.digitalGood.descriptionLargeMarkdown
        digitalGood.bannerImgUrl = data.digitalGood.bannerImageUrl?.absoluteString
        digitalGood.isStudentAccount = data.digitalGood.isStudentAccount

        return digitalGood
    }

    func storeDictionary(from data: Favorite.StoreData) -> [String: Any] {
        [
            "id": data.id,
            "name": data.name,
            "address": data.description,
            "seller_id": data.sellerId,
            "image_url": data.imageUrl?.absoluteString ?? "",
            "is_parking": "\(data.isParking)"
        ]
    }
}
