import UI
import UIKit

final class FavoriteFlowCoordinator: Coordinating {
    private let isNewParking: Int = 2

    private let navigationController: UINavigationController
    
    var childViewController: [UIViewController] = []
    var viewController: UIViewController?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func goToOnboarding() {
        let controller = FavoritesOnboardingFactory.make()
        controller.hidesBottomBarWhenPushed = true
        viewController = controller
        childViewController.append(controller)
        navigationController.pushViewController(controller, animated: true)
    }
    
    func goToFavoritesList() {
        let controller = FavoritesFactory.make(delegate: self)
        viewController = controller
        childViewController.append(controller)
        navigationController.present(UINavigationController(rootViewController: controller), animated: true)
    }
}

extension FavoriteFlowCoordinator: FavoritesDelegate {
    private enum TransactionOrigin: String {
        case favorite = "favorites"
    }
    
    func openPayment(type: FavoritePaymentType) {
        switch type {
        case let .person(id):
            guard let controller = NewTransactionViewController.fromStoryboard() else {
                return
            }
            
            controller.touchOrigin = TransactionOrigin.favorite.rawValue
            controller.showCancel = true
            controller.loadConsumerInfo(id)
            viewController?.present(UINavigationController(rootViewController: controller), animated: true)

        case let .digitalGood(item):
            guard let controller = viewController else {
                return
            }

            DGHelpers.openDigitalGoodFlow(for: item, viewController: controller, origin: nil)
            
        case let .store(dictionary):
            guard let store = PPStore(profileDictionary: dictionary) else { return }

            guard Int(store.isParkingPayment) != isNewParking else {
                return openNewParkingPayment(store: store)
            }
            
            guard let controller = ViewsManager.paymentStoryboardFirstViewController() as? PaymentViewController else {
                return
            }
            
            controller.store = store
            controller.touchOrigin = TransactionOrigin.favorite.rawValue
            viewController?.present(UINavigationController(rootViewController: controller), animated: true)
        }
    }
    
    func openOnboarding() {
        childViewController.last?.dismiss(animated: true) { [weak self] in
            self?.goToOnboarding()
        }
    }
}

private extension FavoriteFlowCoordinator {
    func openNewParkingPayment(store: PPStore) {
        guard let sellerId = store.sellerId else { return }

        let parkingViewController = ParkingCoordinator().start(sellerId: sellerId, storeId: "\(store.wsId)")
        viewController?.present(parkingViewController, animated: true, completion: nil)
    }
}
