import Foundation

enum FavoritesFactory {
    static func make(delegate: FavoritesDelegate) -> FavoritesViewController {
        let service: FavoritesServicing = FavoritesService()
        let coordinator: FavoritesCoordinating = FavoritesCoordinator()
        let presenter: FavoritesPresenting = FavoritesPresenter(coordinator: coordinator)
        let viewModel = FavoritesViewModel(service: service, presenter: presenter)
        let viewController = FavoritesViewController(viewModel: viewModel)

        coordinator.delegate = delegate
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
