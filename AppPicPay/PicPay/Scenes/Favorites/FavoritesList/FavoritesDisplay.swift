import UIKit
import UI

protocol FavoritesDisplay: AnyObject {
    func listFavorites(_ favorites: [Section<String, FavoriteCellViewModel>])
    func didFavoriteError(image: UIImage?, content: (title: String, description: String), button: (image: UIImage?, title: String))
}
