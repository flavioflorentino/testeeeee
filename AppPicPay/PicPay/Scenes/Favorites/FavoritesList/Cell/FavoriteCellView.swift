import UI
import UIKit

final class FavoriteCellView: UITableViewCell {
    private enum Layout {
        static let spaceSize000: CGFloat = 4.0
        static let spaceSize100: CGFloat = 8.0
        static let spaceSize200: CGFloat = 12.0
        static let spaceSize300: CGFloat = 16.0
        static let spaceSize400: CGFloat = 20.0
        static let spaceSize500: CGFloat = 24.0
        static let spaceSize600: CGFloat = 48.0
        static let sizeVerified: CGFloat = 18
    }
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale000.color
        view.layer.cornerRadius = 5
        view.layer.shadowColor = Palette.ppColorGrayscale300.color.withAlphaComponent(0.5).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        view.layer.shadowRadius = 2
        view.layer.borderWidth = 0.2
        view.layer.borderColor = Palette.ppColorGrayscale300.color.cgColor
        view.isSkeletonable = true
        return view
    }()
    
    private lazy var profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 24.0
        imageView.clipsToBounds = true
        imageView.isSkeletonable = true
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14.0, weight: .semibold)
        label.textColor = Palette.ppColorGrayscale500.color
        label.isSkeletonable = true
        label.linesCornerRadius = 6
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 12.0, weight: .regular)
        label.textColor = Palette.ppColorGrayscale500.color
        label.isSkeletonable = true
        label.linesCornerRadius = 6
        return label
    }()
    
    private lazy var starButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.Favorite.starOutline.image, for: .normal)
        button.setImage(Assets.Favorite.starFill.image, for: .selected)
        button.setImage(Assets.Favorite.starFill.image, for: .highlighted)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(tapStar), for: .touchUpInside)
        button.layer.cornerRadius = 12
        button.clipsToBounds = true
        button.isSkeletonable = true
        button.isSelected = true

        return button
    }()
    
    private lazy var badgeVerifiedImage: UIImageView = {
        let image = UIImageView()
        image.image = Assets.Icons.icoVerifiedProfile.image
        image.isHidden = true
        image.layer.cornerRadius = Layout.sizeVerified / 2
        image.clipsToBounds = true
        image.translatesAutoresizingMaskIntoConstraints = false
        
        return image
    }()
    
    var viewModel: FavoriteCellViewModel? {
        didSet {
            update()
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        profileImageView.image = nil
        profileImageView.cancelRequest()
        
        titleLabel.text = nil
        descriptionLabel.text = nil
        starButton.setImage(nil, for: .normal)
    }
    
    private func update() {
        viewModel?.configure()
    }
    
    @objc
    private func tapStar() {
        viewModel?.favoriteOrUnfavorite()
    }
}

extension FavoriteCellView: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(containerView)
        containerView.addSubview(profileImageView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(descriptionLabel)
        containerView.addSubview(starButton)
        containerView.addSubview(badgeVerifiedImage)
    }
    
    func setupConstraints() {
        containerView.layout {
            $0.top == contentView.topAnchor + Layout.spaceSize000
            $0.leading == contentView.leadingAnchor + Layout.spaceSize300
            $0.trailing == contentView.trailingAnchor - Layout.spaceSize300
            $0.bottom == contentView.bottomAnchor - Layout.spaceSize000
        }
        
        profileImageView.layout {
            $0.height == Layout.spaceSize600
            $0.width == Layout.spaceSize600
            $0.leading == containerView.leadingAnchor + Layout.spaceSize100
            $0.top == containerView.topAnchor + Layout.spaceSize200
            $0.bottom == containerView.bottomAnchor - Layout.spaceSize200
        }
        
        titleLabel.layout {
            $0.top == containerView.topAnchor + Layout.spaceSize300
            $0.leading == profileImageView.trailingAnchor + Layout.spaceSize100
            $0.trailing == starButton.leadingAnchor - Layout.spaceSize100
            $0.height == Layout.spaceSize400
        }
        
        descriptionLabel.layout {
            $0.height <= Layout.spaceSize400
            $0.top == titleLabel.bottomAnchor + Layout.spaceSize000
            $0.leading == profileImageView.trailingAnchor + Layout.spaceSize100
            $0.trailing == starButton.leadingAnchor - Layout.spaceSize100
            $0.bottom == containerView.bottomAnchor - Layout.spaceSize300
        }
        
        starButton.layout {
            $0.height == Layout.spaceSize500
            $0.width == Layout.spaceSize500
            $0.trailing == containerView.trailingAnchor - Layout.spaceSize300
            $0.centerY == containerView.centerYAnchor
        }
        
        badgeVerifiedImage.layout {
            $0.bottom == profileImageView.bottomAnchor
            $0.trailing == profileImageView.trailingAnchor
            $0.width == Layout.sizeVerified
            $0.height == Layout.sizeVerified
        }
    }
    
    func configureViews() {
        selectionStyle = .none
        isSkeletonable = true
    }
    
    private func setupView() {
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
}

extension FavoriteCellView: FavoriteCellDisplay {
    func configureView(
        imageUrl: String?,
        placeholderImage: UIImage?,
        title: String,
        description: String?,
        isAction: Bool
    ) {
        titleLabel.text = title
        descriptionLabel.text = description
        starButton.isHidden = isAction
        profileImageView.image = placeholderImage
        
        if let urlString = imageUrl {
            profileImageView.showActivityIndicator = true
            profileImageView.setImage(url: URL(string: urlString), placeholder: placeholderImage)
        }
    }
  
    func didFavorite() {
        starButton.isSelected = true
        
        UIView.animate(
            withDuration: 0.6,
            animations: {
                self.starButton.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            },
            completion: { _ in
                self.starButton.transform = CGAffineTransform.identity
            }
        )
    }
    
    func displayBadgeVerified(_ hiddenVerified: Bool) {
        badgeVerifiedImage.isHidden = hiddenVerified
    }
    
    func didUnfavorite() {
        starButton.isSelected = false
    }
}
