import UIKit

public protocol FavoriteCellDisplay: AnyObject {
    func configureView(imageUrl: String?, placeholderImage: UIImage?, title: String, description: String?, isAction: Bool)
    func displayBadgeVerified(_ hiddenVerified: Bool)
    func didFavorite()
    func didUnfavorite()
}
