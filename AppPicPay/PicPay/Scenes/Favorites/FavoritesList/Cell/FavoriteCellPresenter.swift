import Core
import Foundation
import UIKit

protocol FavoriteCellPresenting: AnyObject {
    var viewController: FavoriteCellDisplay? { get set }

    func configureView(item: Favorite)
    func didFavorite()
    func didUnfavorite()
}

final class FavoriteCellPresenter: FavoriteCellPresenting {
    weak var viewController: FavoriteCellDisplay?

    func configureView(item: Favorite) {
        switch item {
        case let .action(data):
            configureView(data: viewData(from: data))
            setupBadge(isVerified: false)
        case let .consumer(data):
            configureView(data: viewData(from: data))
            setupBadge(isVerified: data.isVerified)
        case let .digitalGood(data):
            configureView(data: viewData(from: data))
            setupBadge(isVerified: false)
        case let .store(data):
            configureView(data: viewData(from: data))
            setupBadge(isVerified: data.isVerified)
        default:
            return
        }
    }
        
    func didFavorite() {
        viewController?.didFavorite()
    }
    
    func didUnfavorite() {
        viewController?.didUnfavorite()
    }
}

private extension FavoriteCellPresenter {
    struct ViewData {
        let title: String
        let description: String
        let placeholder: UIImage
        let imageUrl: String?
        let isAction: Bool
    }

    func viewData(from data: Favorite.ActionData) -> ViewData {
        ViewData(
            title: data.name,
            description: data.description,
            placeholder: Assets.Favorite.homeFavoritePlaceholder.image,
            imageUrl: data.imageUrl?.absoluteString,
            isAction: true
        )
    }

    func viewData(from data: Favorite.ConsumerData) -> ViewData {
        ViewData(
            title: data.name,
            description: data.description,
            placeholder: Assets.Favorite.homeFavoritePlaceholder.image,
            imageUrl: data.imageUrl?.absoluteString,
            isAction: false
        )
    }

    func viewData(from data: Favorite.DigitalGoodData) -> ViewData {
        ViewData(
            title: data.name,
            description: data.description,
            placeholder: Assets.Favorite.homeFavoritePlaceholder.image,
            imageUrl: data.imageUrl?.absoluteString,
            isAction: false
        )
    }

    func viewData(from data: Favorite.StoreData) -> ViewData {
        ViewData(
            title: data.name,
            description: data.description,
            placeholder: Assets.Favorite.homeFavoritePlaceholder.image,
            imageUrl: data.imageUrl?.absoluteString,
            isAction: false
        )
    }

    func configureView(data: ViewData) {
        viewController?.configureView(
            imageUrl: data.imageUrl,
            placeholderImage: data.placeholder,
            title: data.title,
            description: data.description,
            isAction: data.isAction
        )
    }

    func setupBadge(isVerified: Bool) {
        let hiddenVerified = !isVerified
        viewController?.displayBadgeVerified(hiddenVerified)
    }
}
