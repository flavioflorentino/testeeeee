import AnalyticsModule
import Core

protocol FavoriteCellViewModelInputs: AnyObject {
    func configure()
    func favoriteOrUnfavorite()
}

final class FavoriteCellViewModel {
    private let service: FavoritesServicing
    private(set) var presenter: FavoriteCellPresenting

    private let item: Favorite
    private var isFavorite: Bool = true
    
    init(service: FavoritesServicing, presenter: FavoriteCellPresenting, item: Favorite) {
        self.service = service
        self.presenter = presenter
        self.item = item
    }
}

extension FavoriteCellViewModel: FavoriteCellViewModelInputs {
    func configure() {
        presenter.configureView(item: item)
    }
    
    func favoriteOrUnfavorite() {
        isFavorite ? unfavorite(favoriteData(from: item)) : favorite(favoriteData(from: item))
    }
}

private extension FavoriteCellViewModel {
    struct FavoriteData {
        let id: String
        let type: Favorite.`Type`
    }

    func favoriteData(from favorite: Favorite) -> FavoriteData {
        switch favorite {
        case let .consumer(data):
            return FavoriteData(id: data.id, type: favorite.type)
        case let .digitalGood(data):
            return FavoriteData(id: data.id, type: favorite.type)
        case let .store(data):
            return FavoriteData(id: "\(data.sellerId)", type: favorite.type)
        default:
            return FavoriteData(id: "", type: .unknown)
        }
    }

    func favorite(_ data: FavoriteData) {
        isFavorite = true
        presenter.didFavorite()

        service.favorite(id: data.id, type: data.type) { [weak self] didFavorite in
            self?.isFavorite = didFavorite

            guard didFavorite else {
                self?.presenter.didUnfavorite()
                return
            }

            Analytics.shared.log(FavoritesEvent.statusChanged(true, id: data.id, origin: .myFavorites))
        }
    }

    func unfavorite(_ data: FavoriteData) {
        isFavorite = false
        presenter.didUnfavorite()

        service.unfavorite(id: data.id, type: data.type) { [weak self] didUnfavorite in
            self?.isFavorite = didUnfavorite == false

            guard didUnfavorite else {
                self?.presenter.didFavorite()
                return
            }

            Analytics.shared.log(FavoritesEvent.statusChanged(false, id: data.id, origin: .myFavorites))
        }
    }
}
