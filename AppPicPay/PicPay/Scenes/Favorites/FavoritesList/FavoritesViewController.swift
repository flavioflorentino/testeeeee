import UI
import UIKit
import SkeletonView

final class FavoritesViewController: ViewController<FavoritesViewModelInputs, UIView> {
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.register(FavoriteCellView.self, forCellReuseIdentifier: String(describing: FavoriteCellView.self))
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80.0
        tableView.separatorStyle = .none
        tableView.refreshControl = refreshControl
        return tableView
    }()

    private lazy var refreshControl: UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.tintColor = Palette.ppColorGrayscale500.color
        refresh.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        return refresh
    }()
    
    private var dataSource: TableViewHandler<String, FavoriteCellViewModel, FavoriteCellView>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateNavigationBarAppearance()
        loadData()
    }

    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.constraintAllEdges(from: tableView, to: view)
    }
    
    override func configureViews() {
        title = FavoriteLocalizable.favorites.text
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    private func updateNavigationBarAppearance() {
        let barButtonItem = UIBarButtonItem(
            image: Assets.Savings.iconRoundCloseGreen.image,
            style: .plain,
            target: self,
            action: #selector(tapClose)
        )
        
        navigationItem.rightBarButtonItem = barButtonItem
        
        navigationController?.navigationBar.tintColor = Palette.ppColorBranding300.color
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale000.color
        
        if #available(iOS 11, *) {
            extendedLayoutIncludesOpaqueBars = true
            navigationItem.largeTitleDisplayMode = .always
            navigationController?.navigationBar.prefersLargeTitles = true
        }
    }

    private func loadData() {
        beginState()
        viewModel.loadData()
    }
    
    @objc
    private func refreshData() {
        viewModel.loadData()
    }
    
    @objc
    private func tapClose() {
        viewModel.close()
    }
}

// MARK: - View Model Outputs
extension FavoritesViewController: FavoritesDisplay {
    public func listFavorites(_ favorites: [Section<String, FavoriteCellViewModel>]) {
        dataSource = TableViewHandler(data: favorites, cellType: FavoriteCellView.self, configureCell: { _, viewModel, cell in
            viewModel.presenter.viewController = cell
            cell.viewModel = viewModel
        })
        
        tableView.dataSource = dataSource
        tableView.reloadData()
        
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
        
        endState()
    }
    
    public func didFavoriteError(
        image: UIImage?,
        content: (title: String, description: String),
        button: (image: UIImage?, title: String)
    ) {
        endState(
            model: StatefulErrorViewModel(
                image: image,
                content: (title: content.title, description: content.description),
                button: (image: button.image, title: button.title)
            )
        )
    }
}

// MARK: - UITableViewDelegate
extension FavoritesViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelectFavorite(index: indexPath.row)
    }
}

// MARK: - StatefulTransitionViewing
extension FavoritesViewController: StatefulTransitionViewing {
    public func didTryAgain() {
        loadData()
    }
    
    public func statefulViewForLoading() -> StatefulViewing {
        FavoriteSkeletonView()
    }
}

extension TableViewHandler: SkeletonTableViewDataSource {
    public func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        10
    }
    
    public func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        String(describing: FavoriteCellViewModel.self)
    }
}
