import UIKit

enum FavoritesAction {
    case close
    case payment(type: FavoritePaymentType)
    case action
}

enum FavoritePaymentType {
    case person(id: String)
    case digitalGood(item: DGItem)
    case store(dictionary: [String: Any]?)
}

protocol FavoritesCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var delegate: FavoritesDelegate? { get set }
    func perform(action: FavoritesAction)
}

protocol FavoritesDelegate: AnyObject {
    func openPayment(type: FavoritePaymentType)
    func openOnboarding()
}

final class FavoritesCoordinator: FavoritesCoordinating {
    weak var viewController: UIViewController?
    weak var delegate: FavoritesDelegate?
    
    func perform(action: FavoritesAction) {
        switch action {
        case .close:
            NotificationCenter.default.post(
                name: NSNotification.Name.reloadFavoriteSection,
                object: nil
            )
            viewController?.dismiss(animated: true)
        case .payment(let type):
            delegate?.openPayment(type: type)
        case .action:
            delegate?.openOnboarding()
        }
    }
}
