import Core
import Foundation
import UI

protocol FavoritesPresenting: AnyObject {
    var viewController: FavoritesDisplay? { get set }
    func popupError()
    func didNextStep(action: FavoritesAction)
    func listFavorites(_ items: [Favorite])
}

final class FavoritesPresenter: FavoritesPresenting {
    private let coordinator: FavoritesCoordinating
    private let queue: DispatchQueue
    
    weak var viewController: FavoritesDisplay?

    init(coordinator: FavoritesCoordinating, queue: DispatchQueue = .main) {
        self.coordinator = coordinator
        self.queue = queue
    }
    
    func popupError() {
        queue.async { [weak self] in
            self?.viewController?.didFavoriteError(
                image: Assets.CreditPicPay.Registration.sad3.image,
                content: (
                    title: DefaultLocalizable.unexpectedError.text,
                    description: DefaultLocalizable.requestError.text
                ),
                button: (
                    image: nil,
                    title: DefaultLocalizable.tryAgain.text
                )
            )
        }
    }
    
    func didNextStep(action: FavoritesAction) {
        queue.async { [weak self] in
            self?.coordinator.perform(action: action)
        }
    }
    
    func listFavorites(_ items: [Favorite]) {
        queue.async { [weak self] in
            let items = items.map { item -> FavoriteCellViewModel in
                let service: FavoritesServicing = FavoritesService()
                let presenter: FavoriteCellPresenting = FavoriteCellPresenter()
                return FavoriteCellViewModel(service: service, presenter: presenter, item: item)
            }
            
            self?.viewController?.listFavorites([Section(items: items)])
        }
    }
}
