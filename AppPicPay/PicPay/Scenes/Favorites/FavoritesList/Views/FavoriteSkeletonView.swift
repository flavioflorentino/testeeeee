import SkeletonView
import UI
import UIKit

public final class FavoriteSkeletonView: UIView, StatefulViewing {
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(FavoriteCellView.self, forCellReuseIdentifier: String(describing: FavoriteCellView.self))
        tableView.isUserInteractionEnabled = false
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        return tableView
    }()
    
    public var viewModel: StatefulViewModeling?
    
    public weak var delegate: StatefulDelegate?
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension FavoriteSkeletonView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(tableView)
    }
    
    public func setupConstraints() {
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: compatibleSafeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: compatibleSafeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: compatibleSafeAreaLayoutGuide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    public func configureViews() {
        backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    private func setupView() {
        buildViewHierarchy()
        setupConstraints()
        configureViews()
    }
}

// MARK: - UITableViewDataSource
extension FavoriteSkeletonView: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        10
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FavoriteCellView.self), for: indexPath)
        cell.showAnimatedGradientSkeleton()
        return cell
    }
}

// MARK: - UITableViewDelegate
extension FavoriteSkeletonView: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        80.0
    }
}
