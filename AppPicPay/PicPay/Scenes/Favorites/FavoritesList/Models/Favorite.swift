import Foundation

enum Favorite {
    case action(data: ActionData)
    case consumer(data: ConsumerData)
    case digitalGood(data: DigitalGoodData)
    case store(data: StoreData)
    case unknown(type: String)
}

extension Favorite {
    var type: `Type` {
        switch self {
        case .action:
            return .action
        case .consumer:
            return .consumer
        case .digitalGood:
            return .digitalGood
        case .store:
            return .store
        default:
            return .unknown
        }
    }
}

// MARK: Nested
extension Favorite {
    enum `Type`: String {
        case action
        case consumer
        case digitalGood = "digital_good"
        case store
        case unknown
    }

    struct ActionData {
        let id: String
        let name: String
        let description: String
        let imageUrl: URL?
    }

    struct ConsumerData {
        let id: String
        let name: String
        let description: String
        let imageUrl: URL?
        let isVerified: Bool
        let isPro: Bool
    }

    struct DigitalGoodData {
        let id: String
        let name: String
        let description: String
        let imageUrl: URL?
        let sellerId: Int
        let digitalGood: DigitalGood
    }

    struct StoreData {
        let id: String
        let name: String
        let description: String
        let imageUrl: URL?
        let sellerId: Int
        let isVerified: Bool
        let isParking: Int
    }
}

extension Favorite.DigitalGoodData {
    struct DigitalGood {
        let selectionType: String
        let infoUrl: URL?
        let descriptionLargeMarkdown: String
        let bannerImageUrl: URL?
        let service: String
        let isStudentAccount: Bool
    }
}

// MARK: Decodable
extension Favorite: Decodable {
    private enum CodingKeys: CodingKey {
        case type
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let typeString = try container.decode(String.self, forKey: .type)

        guard let type = `Type`(rawValue: typeString) else {
            self = .unknown(type: typeString)
            return
        }

        switch type {
        case .action:
            self = .action(data: try ActionData(from: decoder))
        case .consumer:
            self = .consumer(data: try ConsumerData(from: decoder))
        case .digitalGood:
            self = .digitalGood(data: try DigitalGoodData(from: decoder))
        case .store:
            self = .store(data: try StoreData(from: decoder))
        default:
            self = .unknown(type: typeString)
        }
    }
}

extension Favorite.ActionData: Decodable {
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case imageUrl
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let id = try container.decode(String.self, forKey: .id)
        let name = try container.decode(String.self, forKey: .name)
        let imageUrl = try? container.decode(URL.self, forKey: .imageUrl)
        let description = try container.decode(String.self, forKey: .description)

        self.init(id: id, name: name, description: description, imageUrl: imageUrl)
    }
}

extension Favorite.ConsumerData: Decodable {
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case imageUrl
        case verified
        case pro
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let id = try container.decode(String.self, forKey: .id)
        let name = try container.decode(String.self, forKey: .name)
        let description = try container.decode(String.self, forKey: .description)
        let imageUrl = try? container.decode(URL.self, forKey: .imageUrl)
        let isVerified = try container.decodeIfPresent(Bool.self, forKey: .verified) ?? false
        let isPro = try container.decodeIfPresent(Bool.self, forKey: .pro) ?? false

        self.init(id: id, name: name, description: description, imageUrl: imageUrl, isVerified: isVerified, isPro: isPro)
    }
}

extension Favorite.DigitalGoodData: Decodable {
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case imageUrl
        case sellerId
        case digitalGood
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let id = try container.decode(String.self, forKey: .id)
        let name = try container.decode(String.self, forKey: .name)
        let description = try container.decode(String.self, forKey: .description)
        let imageUrl = try? container.decode(URL.self, forKey: .imageUrl)
        let sellerId = try container.decode(Int.self, forKey: .sellerId)
        let digitalGood = try container.decode(DigitalGood.self, forKey: .digitalGood)

        self.init(id: id, name: name, description: description, imageUrl: imageUrl, sellerId: sellerId, digitalGood: digitalGood)
    }
}

extension Favorite.DigitalGoodData.DigitalGood: Decodable {
    private enum CodingKeys: String, CodingKey {
        case selectionType
        case infoUrl
        case descriptionLargeMarkdown
        case bannerImageUrl = "bannerImgUrl"
        case service
        case studentAccount
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let selectionType = try container.decode(String.self, forKey: .selectionType)
        let infoUrl = try? container.decode(URL.self, forKey: .infoUrl)
        let descriptionLargeMarkdown = try container.decode(String.self, forKey: .descriptionLargeMarkdown)
        let bannerImageUrl = try? container.decode(URL.self, forKey: .bannerImageUrl)
        let service = try container.decode(String.self, forKey: .service)
        let isStudentAccount = try container.decode(Bool.self, forKey: .studentAccount)

        self.init(
            selectionType: selectionType,
            infoUrl: infoUrl,
            descriptionLargeMarkdown: descriptionLargeMarkdown,
            bannerImageUrl: bannerImageUrl,
            service: service,
            isStudentAccount: isStudentAccount
        )
    }
}

extension Favorite.StoreData: Decodable {
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case imageUrl
        case sellerId
        case verified
        case isParking
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let id = try container.decode(String.self, forKey: .id)
        let name = try container.decode(String.self, forKey: .name)
        let description = try container.decode(String.self, forKey: .description)
        let imageUrl = try? container.decode(URL.self, forKey: .imageUrl)
        let sellerId = try container.decode(Int.self, forKey: .sellerId)
        let isVerified = try container.decodeIfPresent(Bool.self, forKey: .verified) ?? false
        let isParking = (try? container.decodeIfPresent(Int.self, forKey: .isParking)) ?? 0

        self.init(
            id: id,
            name: name,
            description: description,
            imageUrl: imageUrl,
            sellerId: sellerId,
            isVerified: isVerified,
            isParking: isParking
        )
    }
}
