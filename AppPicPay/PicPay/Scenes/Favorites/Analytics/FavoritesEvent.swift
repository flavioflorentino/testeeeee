import AnalyticsModule

enum FavoritesEvent: AnalyticsKeyProtocol {
    case favoritesListAccessed(_ origin: FavoritesLocationEventOrigin)
    case favoritesOnboardingAccessed(_ origin: FavoritesLocationEventOrigin)
    case favoritesOnboardingLeft
    case favoritesOnboardingCarouselView(_ type: FavoritesEventType)
    case favoritesOnboardingSearchAccessed
    case statusChanged(_ favorite: Bool, id: String, origin: FavoritesEventType)
    
    private var name: String {
        switch self {
        case .favoritesListAccessed:
            return "Favorites List Accessed"
        case .favoritesOnboardingAccessed:
            return "Favorites Onboarding Accessed"
        case .favoritesOnboardingLeft:
            return "Favorites Onboarding Left"
        case .favoritesOnboardingCarouselView:
            return "Favorites Onboarding Carousel Viewed"
        case .favoritesOnboardingSearchAccessed:
            return "Favorites Onboarding Search Acessed"
        case .statusChanged:
            return "Favorite Status Changed"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case .favoritesListAccessed(let origin), .favoritesOnboardingAccessed(let origin):
            return ["origin": origin.rawValue]
        case .favoritesOnboardingLeft, .favoritesOnboardingSearchAccessed:
            return ["": ""]
        case let .favoritesOnboardingCarouselView(type):
            return ["carousel_item": type.rawValue]
        case let .statusChanged(favorite, id, origin):
            return [
                "favorite": favorite,
                "id": id,
                "origin": origin.rawValue.lowercased()
            ]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

extension FavoritesEvent {
    enum FavoritesEventType: String {
        case favoriteAll = "favorite_all"
        case favoriteOnboarding = "favorite_onboarding"
        case profile = "perfil"
        case myFavorites = "meus favoritos"
        case search = "busca"
        case store = "estabelecimento"
        case digitalGoods = "digital goods"
    }
    
    enum FavoritesLocationEventOrigin: String {
        case home = "inicio"
        case settings = "ajustes"
    }
}
