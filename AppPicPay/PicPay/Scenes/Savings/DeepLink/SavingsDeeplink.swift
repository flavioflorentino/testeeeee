import Foundation

final class SavingsDeeplink: PPDeeplink {
    // MARK: - Initialization
    convenience init(url: URL) {
        self.init(url: url, type: .savings)
    }
}
