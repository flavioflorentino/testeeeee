import UI
import UIKit

final class SavingsDeeplinkHelper {
    typealias Dependencies = HasAppManager

    // MARK: - Properties
    private let dependencies: Dependencies

    // MARK: - Initialization
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - DeeplinkHelperProtocol
extension SavingsDeeplinkHelper: DeeplinkHelperProtocol {
    func handleDeeplink(_ deeplink: PPDeeplink, fromController controller: UIViewController?) -> Bool {
        showSavingsScreen()
    }
}

// MARK: - Private
private extension SavingsDeeplinkHelper {
    func showSavingsScreen() -> Bool {
        guard let currentController = dependencies.appManager.mainScreenCoordinator?.walletNavigation() else {
            return false
        }

        dependencies.appManager.mainScreenCoordinator?.showWalletScreen()

        let navigationController = PPNavigationController(rootViewController: SavingsViewController())
        navigationController.setNavigationBarHidden(true, animated: false)
        currentController.present(navigationController, animated: true)

        return true
    }
}
