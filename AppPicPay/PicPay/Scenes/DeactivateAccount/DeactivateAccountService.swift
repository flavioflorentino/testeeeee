import Foundation

public enum AuthResult<T> {
    case success(T)
    case close
}

public protocol DeactivateAccountServicing {
    func authenticate(_ completion: @escaping (Result<AuthResult<String>, Error>) -> Void)
    func deactivateAccount(password: String, _ completion: @escaping (Result<Void, Error>) -> Void)
}

public final class DeactivateAccountService: DeactivateAccountServicing {
    private var auth: PPAuth?
    
    public func deactivateAccount(password: String, _ completion: @escaping (Result<Void, Error>) -> Void) {
        WSConsumer.deactivate(password) { success, _ in
            guard success else {
                completion(.failure(PicPayError(message: DeactivateAccountLocalizable.errorMessage.text)))
                return
            }
            completion(.success)
        }
    }
    
    public func authenticate(_ completion: @escaping (Result<AuthResult<String>, Error>) -> Void) {
        auth = PPAuth.authenticate({ token, _ in
            guard let password = token else {
                completion(.failure(PicPayError(message: DeactivateAccountLocalizable.errorMessage.text)))
                return
            }
            completion(.success(AuthResult.success(password)))
        }, canceledByUserBlock: {
            completion(.success(AuthResult.close))
        })
    }
}
