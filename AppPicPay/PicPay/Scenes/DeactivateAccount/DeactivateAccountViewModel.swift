import Foundation

protocol DeactivateAccountViewModelInputs: AnyObject {
    func viewDidLoad()
    func deactivateAccountButtonWasTapped()
    func showDeactivateAccountConfirmation()
    func logout(fromContext context: UIViewController?)
}

final class DeactivateAccountViewModel {
    typealias Dependencies = HasAuthManager
    
    private let service: DeactivateAccountServicing
    private let presenter: DeactivateAccountPresenting
    private let dependencies: Dependencies

    init(service: DeactivateAccountServicing, presenter: DeactivateAccountPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
    
    func viewDidLoad() {
        presenter.setTexts()
    }
    
    private func authenticate(onSuccess: @escaping (String) -> Void) {
        service.authenticate({ [weak self] completion in
            switch completion {
            case .success(.success(let password)):
                onSuccess(password)
            case .success(.close):
                self?.presenter.showLoading(false)
            case .failure(let error):
                self?.presenter.showDeactivateAlert(with: error)
            }
        })
    }
    
    private func deactivateAccount(password: String) {
        service.deactivateAccount(password: password) { [weak self] result in
            switch result {
            case .success:
                self?.presenter.deactivatedWithSuccess()
            case .failure(let error):
                self?.presenter.showDeactivateAlert(with: error)
            }
        }
    }
}

extension DeactivateAccountViewModel: DeactivateAccountViewModelInputs {
    func deactivateAccountButtonWasTapped() {
        presenter.deactivateAccountButtonWasTapped()
    }
    
    func showDeactivateAccountConfirmation() {
        presenter.showLoading(true)
        
        authenticate { password in
            self.deactivateAccount(password: password)
        }
    }
    
    public func logout(fromContext context: UIViewController?) {        
        dependencies.authManager.logout(fromContext: context) { [weak self] _ in
            self?.presenter.showAccountWasDeactivatedAlert()
        }
    }
}
