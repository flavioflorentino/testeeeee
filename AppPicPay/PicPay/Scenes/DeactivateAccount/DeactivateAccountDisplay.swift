import UIKit

public protocol DeactivateAccountDisplay: AnyObject {
    func setTitle(_ title: String)
    func setDescription(_ description: NSAttributedString)
    func showDeactivateAccountConfirmation()
    func showLoading(_ show: Bool)
    func logout()
}
