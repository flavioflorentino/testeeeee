import Core
import Foundation
import FeatureFlag

public protocol DeactivateAccountPresenting: AnyObject {
    var viewController: DeactivateAccountDisplay? { get set }
    func deactivateAccountButtonWasTapped()
    func deactivatedWithSuccess()
    func showLoading(_ show: Bool)
    func showDeactivateAlert(with: Error)
    func showAccountWasDeactivatedAlert()
    func setTexts()
}

final class DeactivateAccountPresenter: DeactivateAccountPresenting {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    private let coordinator: DeactivateAccountCoordinating
    weak var viewController: DeactivateAccountDisplay?
    
    init(coordinator: DeactivateAccountCoordinating, dependencies: Dependencies) {
        self.coordinator = coordinator
        self.dependencies = dependencies
    }
    
    func showLoading(_ show: Bool) {
        viewController?.showLoading(show)
    }
    
    func deactivateAccountButtonWasTapped() {
        let alert = Alert(
            title: dependencies.featureManager.text(.deactivateAccountConfirmTitle),
            text: ""
        )
        let deactivateButton = Button(title: DeactivateAccountLocalizable.deactivateAccount.text, type: .destructive) { [weak self] _, _ in
            self?.viewController?.showDeactivateAccountConfirmation()
        }
        let cancelButton = Button(title: DefaultLocalizable.btCancel.text, type: .clean, action: .close)
        alert.buttons = [deactivateButton, cancelButton]
        alert.dismissWithGesture = true
        
        coordinator.perform(action: .showDeactivateAccountConfirmation(with: alert))
    }
    
    func deactivatedWithSuccess() {
        showLoading(false)
        viewController?.logout()
    }
    
    func showDeactivateAlert(with: Error) {
        showLoading(false)
        coordinator.perform(action: .showDeactivateAlert(with: with))
    }
    
    func showAccountWasDeactivatedAlert() {
        let alert = Alert(
            title: dependencies.featureManager.text(.deactivateAccountSuccessTitle),
            text: dependencies.featureManager.text(.deactivateAccountSuccessDesc)
        )
        coordinator.perform(action: .showAccountWasDeactivated(with: alert))
    }
    
    func setTexts() {
        let title = dependencies.featureManager.text(.deactivateAccountScreenTitle)
        viewController?.setTitle(title)
        
        let description = dependencies.featureManager.text(.deactivateAccountScreenDesc)
        let attributedDescription = NSAttributedString(string: description.replacingOccurrences(of: "\\n", with: "\n"))
        viewController?.setDescription(attributedDescription)
    }
}
