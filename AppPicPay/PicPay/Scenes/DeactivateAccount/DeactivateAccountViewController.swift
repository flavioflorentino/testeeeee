import UI
import UIKit
import FeatureFlag

final class DeactivateAccountViewController: ViewController<DeactivateAccountViewModelInputs, UIView>, LoadingViewProtocol {
    enum Layout {
        static let margin: CGFloat = 16
        static let spaceBetweeenSubviews: CGFloat = 20
        static let buttonHeight: CGFloat = 44
        static let stackViewSpacing: CGFloat = 8
        static let titleFontSize: CGFloat = 16
        static let descriptionFontSize: CGFloat = 14
        static let buttonTextSize: CGFloat = 15
        static let buttonRadius: CGFloat = 22
        static let titleNumberOfLines = 0
        static let descriptionNumberOfLines = 0
    }
    
    internal lazy var loadingView = LoadingView()
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = Layout.stackViewSpacing
        stackView.distribution = .fill
        stackView.alignment = .fill
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        label.numberOfLines = Layout.titleNumberOfLines
        label.font = .boldSystemFont(ofSize: Layout.titleFontSize)
        label.textColor = Palette.ppColorGrayscale600.color
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.setContentHuggingPriority(.defaultLow, for: .vertical)
        label.numberOfLines = Layout.descriptionNumberOfLines
        label.font = .systemFont(ofSize: Layout.descriptionFontSize)
        label.textColor = Palette.ppColorGrayscale400.color
        return label
    }()
    
    private lazy var actionButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(DeactivateAccountLocalizable.deactivateAccount.text, for: .normal)
        button.accessibilityLabel = DeactivateAccountLocalizable.deactivateAccount.text
        button.accessibilityIdentifier = "deactivateButton"
        button.titleLabel?.font = .boldSystemFont(ofSize: Layout.buttonTextSize)
        button.titleLabel?.textColor = Palette.white.color
        button.applyRadiusCorners(radius: Layout.buttonRadius)
        button.backgroundColor = Palette.ppColorNegative300.color
        button.addTarget(self, action: #selector(tapDeactivateAccountButton), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad()
    }
    
    @objc
    func tapDeactivateAccountButton() {
        viewModel.deactivateAccountButtonWasTapped()
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        title = DeactivateAccountLocalizable.deactivateAccount.text
    }
    
    override func buildViewHierarchy() {
        view.addSubview(rootStackView)
        rootStackView.addArrangedSubview(titleLabel)
        rootStackView.addArrangedSubview(descriptionLabel)
        view.addSubview(actionButton)
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.activate([
            rootStackView.topAnchor.constraint(equalTo: view.topAnchor, constant: Layout.margin),
            rootStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Layout.margin),
            rootStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Layout.margin)
        ])
        
        NSLayoutConstraint.activate([
            actionButton.topAnchor.constraint(equalTo: rootStackView.bottomAnchor, constant: Layout.spaceBetweeenSubviews),
            actionButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Layout.margin),
            actionButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Layout.margin),
            actionButton.heightAnchor.constraint(equalToConstant: Layout.buttonHeight)
        ])
        
        let buttonBottomAnchor = actionButton.bottomAnchor.constraint(greaterThanOrEqualTo: view.bottomAnchor, constant: -Layout.margin)
        buttonBottomAnchor.priority = .defaultLow
        buttonBottomAnchor.isActive = true
    }
}

// MARK: View Model Outputs
extension DeactivateAccountViewController: DeactivateAccountDisplay {
    func setTitle(_ title: String) {
        titleLabel.text = title
    }
    
    func setDescription(_ description: NSAttributedString) {
        descriptionLabel.attributedText = description
    }
    
    public func showDeactivateAccountConfirmation() {
        viewModel.showDeactivateAccountConfirmation()
    }
    
    public func showLoading(_ show: Bool) {
        DispatchQueue.main.async {
            show ? self.startLoadingView() : self.stopLoadingView()
        }
    }
    
    public func logout() {
        viewModel.logout(fromContext: self)
    }
}
