import Foundation

enum DeactivateAccountFactory {
    static func make() -> DeactivateAccountViewController {
        let container = DependencyContainer()
        let service: DeactivateAccountServicing = DeactivateAccountService()
        let coordinator: DeactivateAccountCoordinating = DeactivateAccountCoordinator(dependencies: container)
        let presenter: DeactivateAccountPresenting = DeactivateAccountPresenter(coordinator: coordinator, dependencies: container)
        let viewModel = DeactivateAccountViewModel(service: service, presenter: presenter, dependencies: container)
        let viewController = DeactivateAccountViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
