import Core
import UIKit

enum DeactivateAccountAction {
    case showDeactivateAlert(with: Error)
    case showAccountWasDeactivated(with: Alert)
    case showDeactivateAccountConfirmation(with: Alert)
}

protocol DeactivateAccountCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: DeactivateAccountAction)
}

final class DeactivateAccountCoordinator: DeactivateAccountCoordinating {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    weak var viewController: UIViewController?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func perform(action: DeactivateAccountAction) {
        switch action {
        case .showDeactivateAlert(let error):
            showDeactivateAlert(with: error)
        case .showAccountWasDeactivated(let alert):
            showAccountWasDeactivated(with: alert)
        case .showDeactivateAccountConfirmation(let alert):
            showDeactivateAccountConfirmation(with: alert)
        }
    }
}

// MARK: Extension for private methods

private extension DeactivateAccountCoordinator {
    func showDeactivateAlert(with: Error) {
        dependencies.mainQueue.async {
            AlertMessage.showAlert(with, controller: self.viewController)
        }
    }
    
    func showAccountWasDeactivated(with: Alert) {
        dependencies.mainQueue.async {
            guard
                let window = UIApplication.shared.delegate?.window,
                let rootViewController = window?.rootViewController
            else {
                return
            }
            AlertMessage.showAlert(with, controller: rootViewController)
        }
    }
    
    func showDeactivateAccountConfirmation(with: Alert) {
        dependencies.mainQueue.async {
            AlertMessage.showAlert(with, controller: self.viewController)
        }
    }
}
