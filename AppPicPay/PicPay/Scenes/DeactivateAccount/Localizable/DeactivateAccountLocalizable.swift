import Foundation

enum DeactivateAccountLocalizable: String, Localizable {
    case deactivateAccount
    case errorMessage
    
    var key: String {
        rawValue
    }
    var file: LocalizableFile {
        .deactivateAccount
    }
}
