import Foundation

enum ReferralIndicationsTabFactory {
    static func make() -> ReferralIndicationsTabViewController {
        let indications = ReferralIndicationsFactory.make()
        let completeIndications = ReferralCompleteIndicationFactory.make()
        
        let controller = ReferralIndicationsTabViewController(
            controllers: [indications, completeIndications],
            tabTitles: [
                ReferralCodeLocalizable.inProgressTabTitle.text,
                ReferralCodeLocalizable.completeTabTitle.text
            ],
            dependencies: DependencyContainer()
        )
        
        indications.indicationsTabDelegate = controller
        completeIndications.indicationsTabDelegate = controller
        indications.paginationDelegate = controller
        completeIndications.paginationDelegate = controller
        
        return controller
    }
}
