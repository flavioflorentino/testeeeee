import UI
import UIKit
import AnalyticsModule

protocol ReferralIndicationsTabViewControllerDelegate: AnyObject {
    func displayDescription(atributted: NSAttributedString)
}

extension ReferralIndicationsTabViewController.Layout {
    enum DescriptionLabel {
        static let height: CGFloat = 70.0
    }
    
    enum SeparatorView {
        static let height: CGFloat = 1.0
    }
}

final class ReferralIndicationsTabViewController: UIViewController {
    typealias Dependencies = HasAnalytics
    fileprivate enum Layout { }
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .justified)
        label.isSkeletonable = true
        
        return label
    }()
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.backgroundSecondary.color
        
        return view
    }()
    
    private lazy var segmentControl: CustomSegmentedControl = {
        let segmentControl = CustomSegmentedControl(buttonTitles: tabTitles)
        segmentControl.textColor = Colors.grayscale300.color
        segmentControl.selectorTextColor = Colors.branding400.color
        segmentControl.selectorViewColor = Colors.branding400.color
        segmentControl.backgroundColor = Colors.backgroundPrimary.color
        segmentControl.selectorIsFullSize = true
        segmentControl.delegate = self
        
        return segmentControl
    }()
    
    private lazy var pageViewController: UIPageViewController = {
        let pageViewController = UIPageViewController(
            transitionStyle: .scroll,
            navigationOrientation: .horizontal
        )
        pageViewController.dataSource = self
        pageViewController.delegate = self
        
        return pageViewController
    }()
    
    private let controllers: [UIViewController]
    private let tabTitles: [String]
    private var previousIndex: Int?
    private var currentIndex: Int = 0 {
        didSet {
            previousIndex = oldValue
            segmentControl.setIndex(index: currentIndex)
        }
    }
    
    weak var paginationDelegate: ReferralPaginationDelegate?
    private var dependencies: Dependencies
    
    init(controllers: [UIViewController], tabTitles: [String], dependencies: Dependencies) {
        self.controllers = controllers
        self.tabTitles = tabTitles
        self.dependencies = dependencies
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ReferralIndicationsTabViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        buildLayout()
    }
}

extension ReferralIndicationsTabViewController: ReferralIndicationsTabViewControllerDelegate {
    func displayDescription(atributted: NSAttributedString) {
        descriptionLabel.attributedText = atributted
    }
}

extension ReferralIndicationsTabViewController: CustomSegmentedControlDelegate {
    func changeToIndex(index: Int) {
        let event = index == 0 ? ReferralEvent.invitesOnGoing : ReferralEvent.invitesComplete
        dependencies.analytics.log(event)
        
        currentIndex = index
        
        guard let previousIndex = self.previousIndex else {
            return
        }
        
        let direction: UIPageViewController.NavigationDirection = currentIndex > previousIndex ? .forward : .reverse
        pageViewController.setViewControllers(
            [controllers[currentIndex]],
            direction: direction,
            animated: true
        )
    }
}

extension ReferralIndicationsTabViewController: ViewConfiguration {
    func buildViewHierarchy() {
        view.addSubview(descriptionLabel)
        view.addSubview(separatorView)
        view.addSubview(segmentControl)
        addPageViewController()
    }
    
    func setupConstraints() {
        descriptionLabel.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.height.equalTo(Layout.DescriptionLabel.height)
        }
        
        separatorView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.SeparatorView.height)
        }
        
        segmentControl.snp.makeConstraints {
            $0.top.equalTo(separatorView.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview()
        }
        
        pageViewController.view.snp.makeConstraints {
            $0.top.equalTo(segmentControl.snp.bottom)
            $0.trailing.leading.bottom.equalToSuperview()
        }
    }
    
    func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    private func addPageViewController() {
        addChild(pageViewController)
        view.addSubview(pageViewController.view)
        pageViewController.didMove(toParent: self)
        pageViewController.setViewControllers(
            [controllers[currentIndex]],
            direction: .forward,
            animated: false
        )
    }
}

extension ReferralIndicationsTabViewController: ReferralPaginationDelegate {
    func changeTo(page: PageType) {
        paginationDelegate?.changeTo(page: page)
    }
}

extension ReferralIndicationsTabViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard
            let visibleViewController = pageViewController.viewControllers?.first,
            let index = controllers.firstIndex(of: visibleViewController) else {
                return
        }
        currentIndex = index
    }
}
extension ReferralIndicationsTabViewController: UIPageViewControllerDataSource {
    func pageViewController(
        _ pageViewController: UIPageViewController,
        viewControllerBefore viewController: UIViewController
    ) -> UIViewController? {
        guard
            let currentControllerIndex = controllers.firstIndex(of: viewController),
            currentControllerIndex > 0 else {
                return nil
        }
        
        let index = currentControllerIndex - 1
        
        return controllers[index]
    }
    
    func pageViewController(
        _ pageViewController: UIPageViewController,
        viewControllerAfter viewController: UIViewController
    ) -> UIViewController? {
        guard
            let currentControllerIndex = controllers.firstIndex(of: viewController),
            currentControllerIndex < (controllers.count - 1) else {
                return nil
        }
        
        let index = currentControllerIndex + 1
        
        return controllers[index]
    }
}
