import UIKit

enum ReferralIndicationsAction {
}

protocol ReferralIndicationsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ReferralIndicationsAction)
}

final class ReferralIndicationsCoordinator {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ReferralIndicationsCoordinating
extension ReferralIndicationsCoordinator: ReferralIndicationsCoordinating {
    func perform(action: ReferralIndicationsAction) {
    }
}
