import Core

enum ReferralIndicationsEndPoint {
    case fetchIndications
    case sendRemember(invitationId: String)
}

extension ReferralIndicationsEndPoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .fetchIndications:
            return "mgm/v2/invitations/incomplete"
        case .sendRemember:
            return "mgm/v2/invitations/remind"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .fetchIndications:
            return .get
        case .sendRemember:
            return .post
        }
    }
    
    var body: Data? {
        guard case let .sendRemember(invitationId) = self else {
            return nil
        }
        
        return ["invitation_id": invitationId].toData()
    }
}
