import Foundation

// MARK: - ReferralIndicationResponse
struct ReferralIndicationResponse: Decodable {
    let data: [ReferralIndication]
}

// MARK: - ReferralIndication
struct ReferralIndication: Decodable {
    let campaign: String
    let invitee: Invitee
    let expiresAt: ExpiresAt
}

// MARK: - Invitee
struct Invitee: Decodable {
    let invitationId: String
    let isReminded: Bool
    let consumerId: Int
    let userName, name, email: String
    let avatarUrl: URL?
    let mobileAreaCode: Int
    let mobilePhone: Int
}

// MARK: - ExpiresAt
struct ExpiresAt: Decodable {
    let days: Int
}
