import Core
import Foundation

protocol ReferralIndicationsServicing {
    func fetchIndications(completion: @escaping (Result<[ReferralIndication], Error>) -> Void)
    func sendRemember(invitationId: String, completion: @escaping (Result<Void, ApiError>) -> Void)
}

final class ReferralIndicationsService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ReferralIndicationsServicing
extension ReferralIndicationsService: ReferralIndicationsServicing {
    func fetchIndications(completion: @escaping (Result<[ReferralIndication], Error>) -> Void) {
        let endpoint = ReferralIndicationsEndPoint.fetchIndications
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        Api<ReferralIndicationResponse>(endpoint: endpoint).execute(jsonDecoder: decoder) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case let .success(payload):
                    completion(.success(payload.model.data))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }

    func sendRemember(invitationId: String, completion: @escaping (Result<Void, ApiError>) -> Void) {
        Api<NoContent>(endpoint: ReferralIndicationsEndPoint.sendRemember(invitationId: invitationId)).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success:
                    completion(.success)
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}
