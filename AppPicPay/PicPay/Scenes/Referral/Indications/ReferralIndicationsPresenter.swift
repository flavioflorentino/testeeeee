import Foundation
import UI

protocol ReferralIndicationsPresenting: AnyObject {
    var viewController: ReferralIndicationsDisplay? { get set }
    func presentIndications(indications: [ReferralIndication], rewardValue: Double)
    func presentEmptyIndications()
    func presentIndicationsCount(indicationsCount: Int)
    func presentEmptyIndicationsDescription()
    func updateDescription()
    func presentErrorView()
    func presentAlertError()
    func presentReward(reward: Double)
}

final class ReferralIndicationsPresenter: ReferralIndicationsPresenting {
    weak var viewController: ReferralIndicationsDisplay?
    private var indicationDescription: NSAttributedString?

    func presentIndications(indications: [ReferralIndication], rewardValue: Double) {
        viewController?.displayIndications(indications.map {
            ReferralIndicationCellPresenter(
                contactImageUrl: $0.invitee.avatarUrl,
                contactName: $0.invitee.name,
                contactUserName: "@\($0.invitee.userName)",
                reward: "R$ \(rewardValue.decimalValue)",
                state: ReferralCodeLocalizable.pendingAction.text,
                rememberButtonTitle: String(
                    format: ReferralCodeLocalizable.rememberButtonTitle.text,
                    "\($0.expiresAt.days)"
                ),
                isButtonDisabled: $0.invitee.isReminded
            )
        })
    }
    
    func presentEmptyIndicationsDescription() {
        viewController?.displayEmptyIndicationsDescription()
    }
    
    func updateDescription() {
        guard let description = indicationDescription else {
            viewController?.displayEmptyIndicationsDescription()
            return
        }
        
        viewController?.displayIndicationsDescriptionAtributted(description: description)
    }
    
    func presentIndicationsCount(indicationsCount: Int) {
        let formattedIndications = "\(indicationsCount) \(ReferralCodeLocalizable.indicated.text)"
        let formattedText = String(format: ReferralCodeLocalizable.indicationsDescription.text, formattedIndications)
        let attributedText = formattedText.attributedStringWithFont(
            primary: Typography.bodySecondary().font(),
            secondary: Typography.bodyPrimary(.highlight).font()
        )
        
        indicationDescription = attributedText
        viewController?.displayIndicationsDescriptionAtributted(description: attributedText)
    }
    
    func presentEmptyIndications() {
        viewController?.displayEmptyIndicationsView()
    }
    
    func presentErrorView() {
        viewController?.displayErrorView()
    }
    
    func presentReward(reward: Double) {
        viewController?.displayReward(reward: "R$ \(reward.decimalValue)")
    }
    
    func presentAlertError() {
        viewController?.displayErrorAlert()
    }
}
