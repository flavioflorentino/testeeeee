import FeatureFlag
import Foundation

protocol ReferralIndicationsViewModelInputs: AnyObject {
    func fetchIndications()
    func updateDescription()
    func inviteRemember(index: Int)
}

final class ReferralIndicationsViewModel {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    private let service: ReferralIndicationsServicing
    private let presenter: ReferralIndicationsPresenting
    private lazy var rewardValue = dependencies.featureManager.number(.referralRewardValue)
    private var indications: [ReferralIndication] = []

    init(service: ReferralIndicationsServicing, presenter: ReferralIndicationsPresenting, dependencies: Dependencies) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

// MARK: - ReferralIndicationsViewModelInputs
extension ReferralIndicationsViewModel: ReferralIndicationsViewModelInputs {
    func inviteRemember(index: Int) {
        guard indications.indices.contains(index) else { return }
        let invitationId = indications[index].invitee.invitationId
        
        service.sendRemember(invitationId: invitationId) { [weak self] result in
            switch result {
            case .success:
                self?.fetchIndications()
            case .failure:
                self?.presenter.presentAlertError()
            }
        }
    }
    
    func fetchIndications() {
        service.fetchIndications { [weak self] result in
            guard let self = self else {
                return
            }
            
            switch result {
            case let .success(indications):
                if indications.isEmpty {
                    self.presenter.presentEmptyIndications()
                    self.presenter.presentEmptyIndicationsDescription()
                } else {
                    self.indications = indications
                    self.presenter.presentIndications(
                        indications: indications,
                        rewardValue: Double(truncating: self.rewardValue ?? 0)
                    )
                    self.presenter.presentIndicationsCount(indicationsCount: indications.count)
                    self.presenter.presentReward(
                        reward: Double(indications.count) * Double(truncating: self.rewardValue ?? 0)
                    )
                }
            case .failure:
                self.presenter.presentErrorView()
            }
        }
    }
    
    func updateDescription() {
        presenter.updateDescription()
    }
}
