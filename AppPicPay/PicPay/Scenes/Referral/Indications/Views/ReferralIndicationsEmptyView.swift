import UI
import UIKit

extension ReferralIndicationsEmptyView.Layout {
    enum Image {
        static let size = CGSize(width: 154, height: 120)
    }
}

final class ReferralIndicationsEmptyView: UIView {
    fileprivate enum Layout {}
    
    private lazy var containerView = UIView()
    private lazy var imageView = UIImageView(image: Assets.Referral.illustrationNoContacts.image)
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, .grayscale800(.default))
            .with(\.textAlignment, .center)
        label.text = title
        
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700(.default))
            .with(\.textAlignment, .center)
        label.text = subtitle
        
        return label
    }()
    
    private lazy var button: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(buttonTitle, for: .normal)
        button.setImage(buttonIcon, for: .normal)
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
        
        return button
    }()
    
    private let title: String
    private let subtitle: String
    private let buttonTitle: String
    private let buttonIcon: UIImage?
    private let buttonActionCompletion: () -> Void
    
    init(
        title: String,
        subtitle: String,
        buttonTitle: String,
        buttonIcon: UIImage? = nil,
        buttonActionCompletion: @escaping () -> Void
    ) {
        self.title = title
        self.subtitle = subtitle
        self.buttonTitle = buttonTitle
        self.buttonIcon = buttonIcon
        self.buttonActionCompletion = buttonActionCompletion
        
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    private func didTapButton() {
        buttonActionCompletion()
    }
}

extension ReferralIndicationsEmptyView: ViewConfiguration {
    func buildViewHierarchy() {
        containerView.addSubview(imageView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(subtitleLabel)
        containerView.addSubview(button)
        addSubview(containerView)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.leading.trailing.equalToSuperview()
        }
        
        imageView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Image.size)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview()
        }
        
        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
        }
        
        button.snp.makeConstraints {
            $0.top.equalTo(subtitleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base01)
        }
    }
}
