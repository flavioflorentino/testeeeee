import SkeletonView
import UI
import UIKit

protocol ReferralIndicationCellDelegate: AnyObject {
    func didTapRememberButton(at index: Int)
}

extension ReferralIndicationCell.Layout {
    enum Button {
        static let height: CGFloat = 32.0
    }
    
    enum Size {
        static let contactImage = CGSize(width: 48, height: 48)
        static let stateImage = CGSize(width: 14, height: 14)
        static let stateLabelWidth: CGFloat = 111
    }
}

final class ReferralIndicationCell: UITableViewCell {
    fileprivate enum Layout { }
    
    static let identifier = String(describing: ReferralIndicationCell.self)
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view
            .viewStyle(RoundedViewStyle())
            .with(\.backgroundColor, .backgroundPrimary())
        
        view.isSkeletonable = true
        
        return view
    }()
    
    private lazy var contactImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = Layout.Size.contactImage.height / 2
        imageView.layer.masksToBounds = true
        
        return imageView
    }()
    
    private lazy var contactUserNameLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale500())
        
        return label
    }()
    
    private lazy var contactNameLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700(.default))
        
        return label
    }()
    
    private lazy var rewardLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700(.default))
        
        return label
    }()
    
    private lazy var stateLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, .warning900(.default))
        
        return label
    }()
    
    private lazy var stateImageView = UIImageView(image: Assets.Referral.icoRoundAlert.image)
    
    private lazy var rememberButton: UIButton = {
        let button = UIPPButton()
        button.cornerRadius = Layout.Button.height / 2
        button.clipsToBounds = true
        button.setTitleColor(Colors.branding300.color, states: [.normal])
        button.setBorderColor(Colors.branding300.color, states: [.normal])
        button.setBorderColor(Colors.grayscale300.color, states: [.disabled])
        button.setTitleColor(Colors.grayscale300.color, states: [.disabled])

        button.addTarget(
            self,
            action: #selector(didTapRememberButton),
            for: .touchUpInside
        )
        
        return button
    }()
    
    private var index: Int?
    private weak var delegate: ReferralIndicationCellDelegate?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutSkeletonIfNeeded()
    }
    
    @objc
    private func didTapRememberButton() {
        guard let index = index else {
            return
        }
        
        delegate?.didTapRememberButton(at: index)
    }
}

extension ReferralIndicationCell {
    func setup(
        with presenter: ReferralIndicationCellPresenting,
        index: Int,
        and delegate: ReferralIndicationCellDelegate
    ) {
        self.delegate = delegate
        self.index = index
        
        contactImageView.setImage(url: presenter.contactImageUrl)
        contactNameLabel.text = presenter.contactName
        contactUserNameLabel.text = presenter.contactUserName
        rewardLabel.text = presenter.reward
        stateLabel.text = presenter.state
        configureButton(isDisabled: presenter.isButtonDisabled, buttonTitle: presenter.rememberButtonTitle)
    }
}

extension ReferralIndicationCell {
    private func configureButton(isDisabled: Bool, buttonTitle: String) {
        let title = isDisabled ? ReferralCodeLocalizable.disabledButtonTitle.text : buttonTitle
        rememberButton.isEnabled = !isDisabled
        rememberButton.setTitle(title, for: .normal)
    }
}

extension ReferralIndicationCell: ViewConfiguration {
    func buildViewHierarchy() {
        containerView.addSubview(contactImageView)
        containerView.addSubview(contactUserNameLabel)
        containerView.addSubview(contactNameLabel)
        containerView.addSubview(rewardLabel)
        containerView.addSubview(stateLabel)
        containerView.addSubview(stateImageView)
        containerView.addSubview(rememberButton)
        contentView.addSubview(containerView)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalToSuperview()
        }
        
        contactImageView.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.Size.contactImage)
        }
        
        contactNameLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.leading.equalTo(contactImageView.snp.trailing).offset(Spacing.base01)
        }
        
        contactUserNameLabel.snp.makeConstraints {
            $0.top.equalTo(contactNameLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalTo(contactImageView.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalTo(containerView.snp.trailing).offset(-Spacing.base02)
        }
        
        rewardLabel.snp.makeConstraints {
            $0.top.equalTo(contactUserNameLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalTo(contactImageView.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalTo(containerView.snp.trailing).offset(-Spacing.base02)
        }
        
        stateLabel.snp.makeConstraints {
            $0.top.equalTo(rewardLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalTo(contactImageView.snp.trailing).offset(Spacing.base01)
        }
        
        stateImageView.snp.makeConstraints {
            $0.centerY.equalTo(stateLabel)
            $0.leading.equalTo(stateLabel.snp.trailing).offset(Spacing.base01)
            $0.size.equalTo(Layout.Size.stateImage)
        }
        
        rememberButton.snp.makeConstraints {
            $0.top.equalTo(stateLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.bottom.equalToSuperview().offset(-Spacing.base02)
            $0.height.equalTo(Layout.Button.height)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundSecondary.color
    }
}
