import UIKit

protocol ReferralIndicationCellPresenting {
    var contactImageUrl: URL? { get }
    var contactName: String? { get }
    var contactUserName: String? { get }
    var reward: String { get }
    var state: String { get }
    var rememberButtonTitle: String { get }
    var isButtonDisabled: Bool { get }
}

struct ReferralIndicationCellPresenter: ReferralIndicationCellPresenting {
    let contactImageUrl: URL?
    let contactName: String?
    let contactUserName: String?
    let reward: String
    let state: String
    let rememberButtonTitle: String
    let isButtonDisabled: Bool
}
