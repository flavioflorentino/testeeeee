import SkeletonView
import UI
import UIKit

protocol ReferralIndicationsDisplay: AnyObject {
    func displayIndications(_ cells: [ReferralIndicationCellPresenting])
    func displayIndicationsDescriptionAtributted(description: NSAttributedString)
    func displayReward(reward: String?)
    func displayEmptyIndicationsDescription()
    func displayEmptyIndicationsView()
    func displayErrorView()
    func displayErrorAlert()
}

private extension ReferralIndicationsViewController.Layout {
    enum Size {
        static let titleLabelHeight: CGFloat = 24.0
        static let headerHeight: CGFloat = 50.0
    }
    
    enum Content {
        static let estimatedRowHeight: CGFloat = 184
    }
}

final class ReferralIndicationsViewController: ViewController<ReferralIndicationsViewModelInputs, UIView> {
    typealias Localizeble = ReferralCodeLocalizable
    fileprivate enum Layout { }
    
    private lazy var headerView: ReferralInviteContactsHeaderView = {
        let headerView = ReferralInviteContactsHeaderView(
            title: Localizeble.indicatedHeaderTitle.text,
            rewardTitle: Localizeble.indicatedHeaderRewardTitle.text
        )
        headerView.backgroundColor = Colors.backgroundSecondary.color
        return headerView
    }()

    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = Colors.backgroundSecondary.color
        tableView.register(ReferralIndicationCell.self, forCellReuseIdentifier: ReferralIndicationCell.identifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Layout.Content.estimatedRowHeight
        tableView.tableHeaderView = headerView
        tableView.tableHeaderView?.frame.size = CGSize(width: view.frame.width, height: Layout.Size.headerHeight)
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        
        return tableView
    }()
    
    // MARK: - Properties
    private var dataSource: TableViewHandler<String, ReferralIndicationCellPresenting, ReferralIndicationCell>?
    weak var paginationDelegate: ReferralPaginationDelegate?
    weak var indicationsTabDelegate: ReferralIndicationsTabViewControllerDelegate?
    
    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

extension ReferralIndicationsViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        beginState()
        viewModel.fetchIndications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.updateDescription()
    }
}

// MARK: ReferralIndicationsDisplay
extension ReferralIndicationsViewController: ReferralIndicationsDisplay {
    func displayIndications(_ cells: [ReferralIndicationCellPresenting]) {
        dataSource = TableViewHandler(
            data: [Section(items: cells)],
            cellType: ReferralIndicationCell.self,
            configureCell: { index, presenter, cell in
                cell.setup(with: presenter, index: index, and: self)
            }
        )
        
        tableView.dataSource = dataSource
        headerView.isHidden = false
        tableView.reloadData()
        endState()
    }
    
    func displayIndicationsDescriptionAtributted(description: NSAttributedString) {
        indicationsTabDelegate?.displayDescription(atributted: description)
    }

    func displayReward(reward: String?) {
        headerView.reward = reward
    }
    
    func displayEmptyIndicationsView() {
        endState()
        let emptyView = ReferralIndicationsEmptyView(
            title: Localizeble.indicationsEmptyViewTitle.text,
            subtitle: Localizeble.indicationsEmptyViewDescription.text,
            buttonTitle: Localizeble.indicationsEmptyViewButtonTitle.text) {
            self.paginationDelegate?.changeTo(page: .inviteCode)
        }
        tableView.backgroundView = emptyView
        headerView.isHidden = true
    }
    
    func displayEmptyIndicationsDescription() {
        let attributedText = Localizeble.indicationsEmptyDescription.text.attributedStringWithFont(
            primary: Typography.bodySecondary().font(),
            secondary: Typography.bodyPrimary(.highlight).font()
        )

        indicationsTabDelegate?.displayDescription(atributted: attributedText)
    }
    
    func displayErrorView() {
        endState()
        let errorView = ReferralIndicationsEmptyView(
            title: Localizeble.errorTitle.text,
            subtitle: Localizeble.errorDescription.text,
            buttonTitle: Localizeble.errorButtonTitle.text,
            buttonIcon: Assets.Referral.icoSendInvite.image,
            buttonActionCompletion: didTryAgain
        )
        tableView.backgroundView = errorView
        headerView.isHidden = true
    }
    
    func displayRememberAlert(index: Int) {
        let inviteContactsButton = Button(
            title: ReferralCodeLocalizable.rememberAlertButtonTitle.text,
            type: .cta
        ) { [weak self] alertController, _ in
            self?.viewModel.inviteRemember(index: index)
            alertController.dismiss(animated: true)
        }
        
        let alert = Alert(
            with: ReferralCodeLocalizable.rememberAlertTitle.text,
            text: ReferralCodeLocalizable.rememberAlertDescription.text,
            buttons: [
                inviteContactsButton,
                Button(title: ReferralCodeLocalizable.notNow.text, type: .clean)
            ],
            image: Alert.Image(with: Assets.Referral.icoSendInvite.image)
        )
        
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func displayErrorAlert() {
        let alert = Alert(
            with: ReferralCodeLocalizable.errorTitle.text,
            text: ReferralCodeLocalizable.errorDescription.text,
            buttons: [Button(title: ReferralCodeLocalizable.close.text, type: .cleanUnderlined)],
            image: Alert.Image(with: Assets.Icons.icoRedX.image)
        )
        
        AlertMessage.showAlert(alert, controller: self)
    }
}

extension ReferralIndicationsViewController: ReferralIndicationCellDelegate {
    func didTapRememberButton(at index: Int) {
        displayRememberAlert(index: index)
    }
}

// MARK: - StatefulTransitionViewing
extension ReferralIndicationsViewController: StatefulTransitionViewing {
    public func didTryAgain() {
        beginState()
        viewModel.fetchIndications()
    }
    
    public func statefulViewForLoading() -> StatefulViewing {
        ReferralIndicationsSkeletonView()
    }
}
