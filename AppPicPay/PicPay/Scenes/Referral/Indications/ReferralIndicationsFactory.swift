import Foundation

enum ReferralIndicationsFactory {
    static func make() -> ReferralIndicationsViewController {
        let container = DependencyContainer()
        let service: ReferralIndicationsServicing = ReferralIndicationsService(dependencies: container)
        let presenter: ReferralIndicationsPresenting = ReferralIndicationsPresenter()
        let viewModel = ReferralIndicationsViewModel(service: service, presenter: presenter, dependencies: container)
        let viewController = ReferralIndicationsViewController(viewModel: viewModel)

        presenter.viewController = viewController

        return viewController
    }
}
