import AnalyticsModule

enum ReferralEvent: AnalyticsKeyProtocol {
    case accessAgenda
    case inviteContact
    case sendInvite
    case sendInviteAlert
    case myCodeTab
    case inviteContactTab
    case indicationsTab
    case invitesOnGoing
    case invitesComplete

    private var name: String {
        switch self {
        case .accessAgenda:
            return "Agenda Access"
        case .inviteContact:
            return "Invite Contact"
        case .sendInvite:
            return "Invite Contact"
        case .sendInviteAlert:
            return "Invite Contatct Modal"
        case .myCodeTab:
            return "My Code"
        case .inviteContactTab:
            return "Contacts"
        case .indicationsTab:
            return "Contacts Invited"
        case .invitesOnGoing:
            return "Invites On Going Screen"
        case .invitesComplete:
            return "Invites On Complete Screen"
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("MGM - APP - \(name)", providers: providers)
    }
}
extension ReferralEvent {
    static func event(_ type: PageType) -> ReferralEvent {
        switch type {
        case .inviteCode:
            return .myCodeTab
        case .inviteContacts:
            return .inviteContactTab
        case .indications:
            return .indicationsTab
        }
    }
}
