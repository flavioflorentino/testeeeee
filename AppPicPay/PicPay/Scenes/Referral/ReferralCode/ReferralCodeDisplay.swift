import UIKit

protocol ReferralCodeDisplay: AnyObject {
    func displayData(
        imageUrl: URL?,
        title: String,
        description: String,
        code: String
    )
    func displayAlertError(with error: Error)
}
