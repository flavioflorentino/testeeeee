import UIKit

enum ReferralCodeAction {
    case presentShare(shareController: UIViewController?)
    case presentRegulation(url: URL)
    case close
}

protocol ReferralCodeCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ReferralCodeAction)
}

final class ReferralCodeCoordinator: ReferralCodeCoordinating {
    weak var viewController: UIViewController?

    func perform(action: ReferralCodeAction) {
        switch action {
        case .presentShare(let controller):
            guard let shareController = controller else {
                return
            }
            viewController?.present(shareController, animated: true)
        case let .presentRegulation(url):
            let webviewController = WebViewFactory.make(with: url)
            
            viewController?.navigationController?.present(
                PPNavigationController(rootViewController: webviewController),
                animated: true
            )
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
}
