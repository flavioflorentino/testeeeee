import Foundation

struct ReferralCodeResponse: Decodable {
    let data: ReferralCodeModel
}

struct ReferralCodeModel: Decodable {
    let campaign: Campaign
    let userInfo: UserInfo
    let share: Share
}

// MARK: - Campaign
struct Campaign: Decodable {
    let imageURL: URL?
    let title: String
    let amountInviter: Double
    let amountInvitee: Double
    let description: String
    let terms: String?
    let rules: [String]?
}

// MARK: - Share
struct Share: Decodable {
    let twitter, whatsApp, facebook, systemOption: ContentShare
}

// MARK: - ContentShare
struct ContentShare: Decodable {
    let link: String
    let text: String
}

// MARK: - UserInfo
struct UserInfo: Decodable {
    let name: String?
    let mgmCode: String
}
