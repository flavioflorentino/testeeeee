import Foundation
import UI

struct ReferralShareItem {
    let type: ReferralShareItemType
    let title: String
    let image: UIImage?
    let imageBackgroundColor: UIColor
    
    static func getItem(for type: ReferralShareItemType) -> ReferralShareItem {
        switch type {
        case .whatsApp:
            return .init(
                type: .whatsApp,
                title: ReferralCodeLocalizable.whatsapp.text,
                image: Assets.Icons.icoWhatsapp.image,
                imageBackgroundColor: Colors.externalWhatsApp.color
            )
        case .twitter:
            return .init(
                type: .twitter,
                title: ReferralCodeLocalizable.twitter.text,
                image: Assets.Icons.icoTwitter.image,
                imageBackgroundColor: Colors.externalTwitter.color
            )
        case .facebook:
            return .init(
                type: .facebook,
                title: ReferralCodeLocalizable.facebook.text,
                image: Assets.Icons.icoFacebook.image,
                imageBackgroundColor: Colors.externalFacebook.color
            )
        case .others:
            return .init(
                type: .others,
                title: ReferralCodeLocalizable.others.text,
                image: Assets.Icons.icoOthers.image,
                imageBackgroundColor: Colors.grayscale100.lightColor
            )
        }
    }
}
