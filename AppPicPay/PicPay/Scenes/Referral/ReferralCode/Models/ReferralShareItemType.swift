import Foundation

@objc
enum ReferralShareItemType: Int, CaseIterable {
    case whatsApp
    case twitter
    case facebook
    case others  
}
