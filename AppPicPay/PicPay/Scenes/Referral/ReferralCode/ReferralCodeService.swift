import Foundation
import Core

protocol ReferralCodeServicing {
    func getReferralCodeData(completion: @escaping (Result<ReferralCodeModel, ApiError>) -> Void)
}

final class ReferralCodeService: ReferralCodeServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func getReferralCodeData(completion: @escaping (Result<ReferralCodeModel, ApiError>) -> Void) {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        Api<ReferralCodeResponse>(endpoint: ReferralCodeEndPoint.referralCodeData).execute(jsonDecoder: decoder) { result in
            self.dependencies.mainQueue.async {
                switch result {
                case let .success(payload):
                    completion(.success(payload.model.data))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}
