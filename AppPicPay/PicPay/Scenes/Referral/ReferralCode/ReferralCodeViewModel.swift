import Foundation
import AnalyticsModule

protocol ReferralCodeViewModelInputs: AnyObject {
    func fetchData()
    func copyCode(code: String?)
    func shareCode(type: ReferralShareItemType)
    func openRegulation()
    func close()
}

final class ReferralCodeViewModel {
    typealias Dependencies = HasAnalytics & HasReferralManager
    private let dependencies: Dependencies
    private let service: ReferralCodeServicing
    private let presenter: ReferralCodePresenting
    private let socialShareWorker: SocialShareable
    private var model: ReferralCodeModel?
    
    init(
        service: ReferralCodeServicing,
        presenter: ReferralCodePresenting,
        socialShareWorker: SocialShareable,
        dependencies: Dependencies
    ) {
        self.service = service
        self.presenter = presenter
        self.socialShareWorker = socialShareWorker
        self.dependencies = dependencies
    }
}

extension ReferralCodeViewModel: ReferralCodeViewModelInputs {
    func copyCode(code: String?) {
        guard let code = code else {
            return
        }
        
        dependencies.analytics.log(ReferralCodeEvent.sharecode(type: .clipboard))
        
        UIPasteboard.general.string = code
    }
    
    func shareCode(type: ReferralShareItemType) {
        let socialShareType: SocialType
        
        switch type {
        case .facebook:
            dependencies.analytics.log(ReferralCodeEvent.sharecode(type: .facebook))
            socialShareType = .facebook(text: model?.share.facebook.text) { shareController in
                self.presenter.didNextStep(action: .presentShare(shareController: shareController))
            }
        case .twitter:
            dependencies.analytics.log(ReferralCodeEvent.sharecode(type: .twitter))
            socialShareType = .twitter(text: model?.share.twitter.text) { shareController in
                self.presenter.didNextStep(action: .presentShare(shareController: shareController))
            }
        case .whatsApp:
            dependencies.analytics.log(ReferralCodeEvent.sharecode(type: .whatsapp))
            socialShareType = .whatsapp(text: model?.share.whatsApp.text)
        case .others:
            dependencies.analytics.log(ReferralCodeEvent.sharecode(type: .system))
            socialShareType = .native(text: model?.share.systemOption.text) { shareController in
                self.presenter.didNextStep(action: .presentShare(shareController: shareController))
            }
        }
        
        socialShareWorker.share(type: socialShareType)
    }
    
    func openRegulation() {
        dependencies.analytics.log(ReferralCodeEvent.terms)
        guard let urlString = model?.campaign.terms, let url = URL(string: urlString) else {
            return
        }
        presenter.didNextStep(action: .presentRegulation(url: url))
    }
    
    func fetchData() {
        dependencies.analytics.log(ReferralCodeEvent.open)
        service.getReferralCodeData { [weak self] result in
            switch result {
            case .success(let model):
                self?.model = model
                self?.presenter.presentData(
                    imageUrl: model.campaign.imageURL,
                    title: model.campaign.title,
                    description: model.campaign.description,
                    code: model.userInfo.mgmCode
                )
            case .failure(let error):
                self?.presenter.presentAlertError(with: error)
            }
        }
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
}
