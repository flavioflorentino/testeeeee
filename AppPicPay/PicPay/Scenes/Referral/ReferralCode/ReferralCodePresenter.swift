import Core
import Foundation

protocol ReferralCodePresenting: AnyObject {
    var viewController: ReferralCodeDisplay? { get set }
    func presentData(imageUrl: URL?, title: String, description: String, code: String)
    func presentAlertError(with error: Error)
    func didNextStep(action: ReferralCodeAction)
}

final class ReferralCodePresenter {
    private let coordinator: ReferralCodeCoordinating
    weak var viewController: ReferralCodeDisplay?
    
    init(coordinator: ReferralCodeCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ReferralCodePresenting
extension ReferralCodePresenter: ReferralCodePresenting {
    func presentData(imageUrl: URL?, title: String, description: String, code: String) {
        viewController?.displayData(
            imageUrl: imageUrl,
            title: title,
            description: description,
            code: code
        )
    }
    
    func presentAlertError(with error: Error) {
        viewController?.displayAlertError(with: error)
    }
    
    func didNextStep(action: ReferralCodeAction) {
        coordinator.perform(action: action)
    }
}
