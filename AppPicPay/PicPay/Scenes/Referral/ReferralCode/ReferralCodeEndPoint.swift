import Core

enum ReferralCodeEndPoint {
    case referralCodeData
}

extension ReferralCodeEndPoint: ApiEndpointExposable {
    var path: String {
        "mgm/v2/campaign/user"
    }
}
