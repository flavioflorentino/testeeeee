import Foundation

enum ReferralCodeFactory {
    static func make() -> ReferralCodeViewController {
        let container = DependencyContainer()
        let service: ReferralCodeServicing = ReferralCodeService(dependencies: container)
        let coordinator: ReferralCodeCoordinating = ReferralCodeCoordinator()
        let presenter: ReferralCodePresenting = ReferralCodePresenter(coordinator: coordinator)
        let socialShareWorker = SocialShareWorker()
        let viewModel = ReferralCodeViewModel(
            service: service,
            presenter: presenter,
            socialShareWorker: socialShareWorker,
            dependencies: container
        )
        let viewController = ReferralCodeViewController(viewModel: viewModel)
        viewController.modalPresentationStyle = .fullScreen

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
