import AnalyticsModule

enum ReferralCodeEvent: AnalyticsKeyProtocol {
    case sharecode(type: SocialShareType)
    case terms
    case open
    
    private var name: String {
        switch self {
        case .sharecode:
            return "Invite Link Shared"
        case .terms:
            return "Terms of Use"
        case .open:
            return "Landing Page"
        }
    }
    
    private var properties: [String: Any] {
        guard case let .sharecode(socialType) = self else {
            return [:]
        }
        return ["type": socialType.rawValue]
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("MGM - App - PGP - \(name)", properties: properties, providers: providers)
    }
}

extension ReferralCodeEvent {
    enum SocialShareType: String {
        case clipboard
        case whatsapp
        case facebook
        case twitter
        case system
    }
}
