import Foundation
import Social

enum SocialType {
    case facebook(text: String?, completion: (UIViewController?) -> Void)
    case twitter(text: String?, completion: (UIViewController?) -> Void)
    case whatsapp(text: String?)
    case native(text: String?, completion: (UIViewController?) -> Void)
}

protocol SocialShareable {
    func share(type: SocialType)
}

final class SocialShareWorker: SocialShareable {
    private let whatsappShareText = "whatsapp://send?text="
    
    func share(type: SocialType) {
        switch type {
        case let .facebook(text, completion):
            let controller = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            controller?.setInitialText(text)
            completion(controller)
        case let .twitter(text, completion):
            let controller = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            controller?.setInitialText(text)
            completion(controller)
        case let .native(text, completion):
            let controller = UIActivityViewController(
                activityItems: [text as Any],
                applicationActivities: nil
            )
            completion(controller)
        case .whatsapp(let text):
            guard
                let text = text?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
                let shareUrl = URL(string: whatsappShareText + text),
                UIApplication.shared.canOpenURL(shareUrl) else {
                    return
            }
            
            UIApplication.shared.open(shareUrl, options: [:])
        }
    }
}
