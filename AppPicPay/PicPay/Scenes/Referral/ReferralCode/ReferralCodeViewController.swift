import UI
import UIKit
import SkeletonView

private extension ReferralCodeViewController.Layout {
    enum Font {
        static let buttonFont: UIFont = .systemFont(ofSize: 14)
    }
    enum Size {
        static let image = CGSize(width: 220.0, height: 178.0)
        static let codeContainerViewHeight: CGFloat = 40.0
        static let regulationViewHeight: CGFloat = 49.0
        static let shareViewHeight: CGFloat = 132.0
        static let titleLabelHeight: CGFloat = 24.0
        static let descriptionLabelHeight: CGFloat = 80.0
        static let descMinimumScaleFactor: CGFloat = 0.8
        static let cornerRadiusView: CGFloat = 5
    }
}

final class ReferralCodeViewController: ViewController<ReferralCodeViewModelInputs, UIView> {
    fileprivate enum Layout { }
    
    private lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.backgroundColor = .clear
        scroll.alwaysBounceHorizontal = false
        return scroll
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.isSkeletonable = true
        
        return view
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.Referral.icoReferralCode.image
        imageView.isSkeletonable = true
        
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, .grayscale850())
            .with(\.textAlignment, .center)
        label.isSkeletonable = true
        label.linesCornerRadius = Int(Layout.Size.cornerRadiusView)
        
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .center)
        label.minimumScaleFactor = Layout.Size.descMinimumScaleFactor
        label.adjustsFontSizeToFitWidth = true
        label.isSkeletonable = true
        label.linesCornerRadius = Int(Layout.Size.cornerRadiusView)
        
        return label
    }()
    
    private lazy var referralCodeView: ReferralCodeView = {
        let referralView = ReferralCodeView(
            didCopyCodeHandler: viewModel.copyCode
        )
        referralView.canCopyOnTap = false
        referralView.isSkeletonable = true
        
        return referralView
    }()
    
    private lazy var copyCodeButton: UnderlinedButton = {
        let button = UnderlinedButton()
        button.setTitle(ReferralCodeLocalizable.copyCode.text, for: .normal)
        button.updateColor(
            Colors.branding600.color,
            highlightedColor: Colors.branding600.color,
            font: Layout.Font.buttonFont
        )
        button.addTarget(self, action: #selector(didCopyCode), for: .touchUpInside)
        button.isSkeletonable = true
        
        return button
    }()
    
    private lazy var shareView: ReferralShareView = {
        let shareView = ReferralShareView(didSelectItem: didSelectSocialtem)
        shareView.isSkeletonable = true
        
        return shareView
    }()
    
    private lazy var regulationView: UIView = {
        let view = UIView()
        view.isSkeletonable = true
        
        return view
    }()
    
    private lazy var regulationButton: UnderlinedButton = {
        let button = UnderlinedButton()
        button.setTitle(ReferralCodeLocalizable.regulation.text, for: .normal)
        button.updateColor(
            Colors.branding600.color,
            highlightedColor: Colors.branding600.color,
            font: Layout.Font.buttonFont
        )
        button.addTarget(self, action: #selector(didTapRegulationButton), for: .touchUpInside)
        button.isSkeletonable = true
        
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateNavigationBarAppearance()
        addNavigationButtons()
        viewModel.fetchData()
        showSkeleton()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        layoutSkeletonsIfNeeded()
    }
    
    override func buildViewHierarchy() {
        contentView.addSubviews(
            imageView,
            titleLabel,
            descriptionLabel,
            referralCodeView,
            copyCodeButton,
            shareView,
            regulationView
        )
        regulationView.addSubview(regulationButton)
        scrollView.addSubview(contentView)
        view.addSubview(scrollView)
    }
    
    override func setupConstraints() {
        configureScrollViewConstraints()
        
        imageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.Size.image)
            $0.centerX.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base03)
            $0.trailing.equalToSuperview().offset(-Spacing.base03)
            $0.height.equalTo(Layout.Size.titleLabelHeight)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.height.equalTo(Layout.Size.descriptionLabelHeight)
        }
        
        referralCodeView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.height.equalTo(Layout.Size.codeContainerViewHeight)
        }
        
        copyCodeButton.snp.makeConstraints {
            $0.top.equalTo(referralCodeView.snp.bottom).offset(Spacing.base01)
            $0.centerX.equalToSuperview()
        }
        
        shareView.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(copyCodeButton.snp.bottom).offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalTo(regulationView.snp.top).offset(-Spacing.base04)
            $0.height.equalTo(Layout.Size.shareViewHeight)
        }
        
        regulationView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.height.equalTo(Layout.Size.regulationViewHeight)
        }
        
        regulationButton.snp.makeConstraints {
            $0.centerX.centerY.equalToSuperview()
        }
    }
    
    private func configureScrollViewConstraints() {
        scrollView.snp.makeConstraints {
            $0.edges.equalTo(view.compatibleSafeArea.edges)
        }
        
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.greaterThanOrEqualTo(view.compatibleSafeArea.height)
            $0.width.equalTo(view.snp.width)
        }
    }
    
    private func updateNavigationBarAppearance() {
        navigationController?.navigationBar.tintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
            navigationController?.navigationItem.largeTitleDisplayMode = .never
        }
    }
    
    private func addNavigationButtons() {
        let leftBarButtonItem = UIBarButtonItem(
            image: Assets.PlaceIndication.icoBack.image,
            style: .plain,
            target: self,
            action: #selector(didTapCloseButton)
        )
        
        leftBarButtonItem.tintColor = Colors.branding300.color
        navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    private func layoutSkeletonsIfNeeded() {
        contentView.subviews.forEach {
            $0.layoutSkeletonIfNeeded()
        }
    }
    
    private func showSkeleton() {
        contentView.subviews.forEach {
            $0.layer.cornerRadius = Layout.Size.cornerRadiusView
            $0.layer.masksToBounds = true
            $0.showSkeleton(usingColor: Colors.grayscale100.color)
        }
    }
    
    private func dismissSkeleton() {
        contentView.subviews.forEach {
            $0.layer.cornerRadius = 0.0
            $0.layer.masksToBounds = false
            $0.setNeedsLayout()
            $0.setNeedsDisplay()
            $0.hideSkeleton()
        }
    }
}

@objc
extension ReferralCodeViewController {
    private func didCopyCode() {
        referralCodeView.copyCode()
    }
    
    private func didSelectSocialtem(type: ReferralShareItemType) {
        viewModel.shareCode(type: type)
    }
    
    private func didTapRegulationButton() {
        viewModel.openRegulation()
    }
    
    private func didTapCloseButton() {
        viewModel.close()
    }
    
    private func showPopupError(messageDescription: String?) {
        let popup = PopupViewController(
            title: ReferralCodeLocalizable.alertTitle.text,
            description: messageDescription,
            preferredType: .image(Assets.Icons.sadIcon.image)
        )
        
        let confirmAction = PopupAction(
            title: ReferralCodeLocalizable.alertButtonTitle.text,
            style: .fill) { [weak self] in
            self?.viewModel.close()
        }
        
        popup.addAction(confirmAction)
        showPopup(popup)
    }
}

// MARK: View Model Outputs
extension ReferralCodeViewController: ReferralCodeDisplay {
    func displayAlertError(with error: Error) {
        showPopupError(messageDescription: error.localizedDescription)
    }
    
    func displayData(imageUrl: URL?, title: String, description: String, code: String) {
        imageView.setImage(url: imageUrl)
        titleLabel.text = title
        descriptionLabel.text = description
        referralCodeView.referralCode = code
        dismissSkeleton()
    }
}
