import UI
import UIKit

extension ReferralShareView.Layout {
    enum Size {
        static let shadowOpacity: Float = 0.1
        static let shadowOffset = CGSize(width: 0, height: 2)
    }
}

final class ReferralShareView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var shareContentView: UIView = {
        let view = UIView()
        view
            .viewStyle(RoundedViewStyle(cornerRadius: .light))
            .with(\.backgroundColor, .groupedBackgroundSecondary())
        
        view.layer.shadowOpacity = Layout.Size.shadowOpacity
        view.layer.shadowOffset = Layout.Size.shadowOffset
        view.layer.shadowRadius = CornerRadius.light
        
        return view
    }()
    
    private lazy var shareTitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .small))
            .with(\.textColor, .grayscale850(.default))
            .with(\.textAlignment, .center)
        
        label.text = ReferralCodeLocalizable.shareTitle.text
        
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let items = types.map(ReferralShareItem.getItem)
        let views = items.map {
            ReferralShareItemView(referralShareItem: $0, didSelect: didSelectItem)
        }
        let stackView = UIStackView(arrangedSubviews: views)
        
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        
        return stackView
    }()
    
    private let types: [ReferralShareItemType]
    private let didSelectItem: (ReferralShareItemType) -> Void
    
    init(
        types: [ReferralShareItemType] = ReferralShareItemType.allCases,
        didSelectItem:  @escaping (ReferralShareItemType) -> Void
    ) {
        self.types = types
        self.didSelectItem = didSelectItem
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        shareContentView.addSubview(shareTitleLabel)
        shareContentView.addSubview(stackView)
        addSubview(shareContentView)
    }
    
    func setupConstraints() {
        shareTitleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        stackView.snp.makeConstraints {
            $0.top.equalTo(shareTitleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base01)
            $0.trailing.equalToSuperview().offset(-Spacing.base01)
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
        }
        
        shareContentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}
