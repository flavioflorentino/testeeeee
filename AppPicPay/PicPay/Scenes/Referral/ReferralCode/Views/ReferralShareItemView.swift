import UIKit
import UI

extension ReferralShareItemView.Layout {
    enum Size {
        static let titleLabelHeight: CGFloat = 14.0
        static let titleMinimumScaleFactor: CGFloat = 0.6
    }
}

final class ReferralShareItemView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.viewStyle(RoundedViewStyle())
        imageView.image = referralShareItem.image
        imageView.backgroundColor = referralShareItem.imageBackgroundColor
        imageView.contentMode = .center

        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale750(.default))
            .with(\.textAlignment, .center)
            .with(\.typography, .caption())
        label.text = referralShareItem.title
        label.minimumScaleFactor = Layout.Size.titleMinimumScaleFactor
        label.adjustsFontSizeToFitWidth = true
        
        return label
    }()
    
    private let referralShareItem: ReferralShareItem
    private let didSelect: (ReferralShareItemType) -> Void
    
    init(referralShareItem: ReferralShareItem, didSelect: @escaping (ReferralShareItemType) -> Void) {
        self.referralShareItem = referralShareItem
        self.didSelect = didSelect
        
        super.init(frame: .zero)
        buildLayout()
        configureTapAction()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubviews(imageView, titleLabel)
    }
    
    func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base01)
            $0.trailing.equalToSuperview().offset(-Spacing.base01)
            $0.height.equalTo(imageView.snp.width)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base01)
            $0.bottom.leading.trailing.equalToSuperview()
            $0.height.equalTo(Layout.Size.titleLabelHeight)
        }
    }

    private func configureTapAction() {
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapItem)))
    }
    
    @objc
    private func didTapItem() {
        didSelect(referralShareItem.type)
    }
}
