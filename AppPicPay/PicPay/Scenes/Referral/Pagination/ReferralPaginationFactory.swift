import Foundation

enum ReferralPaginationFactory {
    private typealias Localizable = ReferralCodeLocalizable
    
    static func make() -> ReferralPaginationViewController {
        let container = DependencyContainer()
        let referralCodeController = ReferralCodeFactory.make()
        let referralInviteContactsController = ReferralInviteContactsFactory.make()
        let indicationsTab = ReferralIndicationsTabFactory.make()
        
        let controller = ReferralPaginationViewController(
            pages: [
                ReferralPage(
                    pageTitle: Localizable.referralCodeTitle.text,
                    sectionTitle: Localizable.referralCodeTabTitle.text,
                    pageController: referralCodeController
                ),
                ReferralPage(
                    pageTitle: Localizable.referralInviteTitle.text,
                    sectionTitle: Localizable.referralInviteTabTitle.text,
                    pageController: referralInviteContactsController
                ),
                ReferralPage(
                    pageTitle: Localizable.referralIndicationsTitle.text,
                    sectionTitle: Localizable.referralIndicationsTabTitle.text,
                    pageController: indicationsTab
                )
            ],
            dependencies: container
        )
        
        referralInviteContactsController.paginationDelegate = controller
        indicationsTab.paginationDelegate = controller
        
        return controller
    }
}
