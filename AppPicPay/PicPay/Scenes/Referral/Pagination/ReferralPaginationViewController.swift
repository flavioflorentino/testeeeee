import UIKit
import UI
import AnalyticsModule

extension ReferralPaginationViewController.Layout {
    enum TitleLabel {
        static let minimumScaleFactor: CGFloat = 0.5
        static let numberOfLines = 1
    }
    
    enum SegmentControl {
        static let width: CGFloat = 300
    }
}

final class ReferralPaginationViewController: UIViewController {
    typealias Dependencies = HasAnalytics
    fileprivate enum Layout {}
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .xLarge))
            .with(\.textColor, .grayscale850())
            .with(\.textAlignment, .left)
        
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = Layout.TitleLabel.numberOfLines
        label.minimumScaleFactor = Layout.TitleLabel.minimumScaleFactor
        
        return label
    }()
    
    private lazy var segmentControl: CustomSegmentedControl = {
        let segmentControl = CustomSegmentedControl(buttonTitles: segmentTitles)
        segmentControl.textColor = Colors.grayscale300.color
        segmentControl.selectorTextColor = Colors.branding400.color
        segmentControl.selectorViewColor = Colors.branding400.color
        segmentControl.selectorIsFullSize = false
        segmentControl.delegate = self
        segmentControl.backgroundColor = Colors.backgroundPrimary.color
        
        return segmentControl
    }()
    
    private lazy var pageViewController: UIPageViewController = {
        let pageViewController = UIPageViewController(
            transitionStyle: .scroll,
            navigationOrientation: .horizontal
        )
        pageViewController.dataSource = self
        pageViewController.delegate = self
        
        return pageViewController
    }()
    
    private let pages: [ReferralPage]
    
    private var pageTitles: [String] {
        pages.map { $0.pageTitle }
    }
    
    private var segmentTitles: [String] {
        pages.map { $0.sectionTitle }
    }
    
    private var controllers: [UIViewController] {
        pages.map { $0.pageController }
    }
    
    private var currentIndex: Int = 0 {
        didSet {
            if let pageType = PageType(rawValue: currentIndex) {
                dependencies.analytics.log(ReferralEvent.event(pageType))
            }
           
            previousIndex = oldValue
            segmentControl.setIndex(index: currentIndex)
            updateTitle()
        }
    }
    
    private var dependencies: Dependencies
    private var previousIndex: Int?
    
    init(pages: [ReferralPage] = [], dependencies: Dependencies) {
        self.pages = pages
        self.dependencies = dependencies
        super.init(nibName: nil, bundle: nil)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func updateTitle() {
        titleLabel.text = pages[currentIndex].pageTitle
    }
    
    private func updateNavigationBarAppearance() {
        navigationController?.navigationBar.tintColor = Colors.brandingBase.color
        navigationController?.navigationBar.isTranslucent = false
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.isTranslucent = true
            navigationController?.navigationItem.largeTitleDisplayMode = .never
        }
        addNavigationButtons()
    }
    
    private func addNavigationButtons() {
        let leftBarButtonItem = UIBarButtonItem(
            image: Assets.PlaceIndication.icoBack.image,
            style: .plain,
            target: self,
            action: #selector(didTapCloseButton)
        )
        
        leftBarButtonItem.tintColor = Colors.branding300.color
        navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    @objc
    private func didTapCloseButton() {
        dismiss(animated: true)
    }
}

extension ReferralPaginationViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarAppearance()
    }
}

extension ReferralPaginationViewController: ViewConfiguration {
    func buildViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(segmentControl)
        addPageViewController()
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(view.compatibleSafeArea.top).offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
        }
        
        segmentControl.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.width.equalTo(Layout.SegmentControl.width)
        }
        
        pageViewController.view.snp.makeConstraints {
            $0.top.equalTo(segmentControl.snp.bottom).offset(Spacing.base01)
            $0.trailing.leading.bottom.equalToSuperview()
        }
    }
    
    func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
        updateTitle()
    }
    
    private func addPageViewController() {
        addChild(pageViewController)
        view.addSubview(pageViewController.view)
        pageViewController.didMove(toParent: self)
        pageViewController.setViewControllers(
            [controllers[currentIndex]],
            direction: .forward,
            animated: false
        )
    }
}

extension ReferralPaginationViewController: CustomSegmentedControlDelegate {
    func changeToIndex(index: Int) {
        currentIndex = index
        
        guard let previousIndex = self.previousIndex else {
            return
        }
        
        let direction: UIPageViewController.NavigationDirection = currentIndex > previousIndex ? .forward : .reverse
        pageViewController.setViewControllers(
            [controllers[currentIndex]],
            direction: direction,
            animated: true
        )
    }
}

extension ReferralPaginationViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard
            let visibleViewController = pageViewController.viewControllers?.first,
            let index = controllers.firstIndex(of: visibleViewController) else {
                return
        }
        currentIndex = index
    }
}

extension ReferralPaginationViewController: UIPageViewControllerDataSource {
    func pageViewController(
        _ pageViewController: UIPageViewController,
        viewControllerBefore viewController: UIViewController
    ) -> UIViewController? {
        guard
            let currentControllerIndex = controllers.firstIndex(of: viewController),
            currentControllerIndex > 0 else {
                return nil
        }
        
        let index = currentControllerIndex - 1
        
        return controllers[index]
    }
    
    func pageViewController(
        _ pageViewController: UIPageViewController,
        viewControllerAfter viewController: UIViewController
    ) -> UIViewController? {
        guard
            let currentControllerIndex = controllers.firstIndex(of: viewController),
            currentControllerIndex < (controllers.count - 1) else {
                return nil
        }
        
        let index = currentControllerIndex + 1
        
        return controllers[index]
    }
}

extension ReferralPaginationViewController: ReferralPaginationDelegate {
    func changeTo(page: PageType) {
        changeToIndex(index: page.rawValue)
    }
}
