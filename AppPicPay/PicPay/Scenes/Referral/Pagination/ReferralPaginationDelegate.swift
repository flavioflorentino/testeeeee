import Foundation

protocol ReferralPaginationDelegate: AnyObject {
    func changeTo(page: PageType)
}

enum PageType: Int {
    case inviteCode
    case inviteContacts
    case indications
}
