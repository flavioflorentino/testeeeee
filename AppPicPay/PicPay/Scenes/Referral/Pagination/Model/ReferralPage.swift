import UIKit

struct ReferralPage {
    let pageTitle: String
    let sectionTitle: String
    let pageController: UIViewController
}
