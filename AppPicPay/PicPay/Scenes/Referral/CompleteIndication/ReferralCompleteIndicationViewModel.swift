import FeatureFlag
import Foundation

protocol ReferralCompleteIndicationViewModelInputs: AnyObject {
    func fetchCompleteIndications()
    func updateIndicationsIfNeeded(index: Int)
    func refreshIndications()
    func updateDescription()
}

final class ReferralCompleteIndicationViewModel {
    typealias Dependencies = HasFeatureManager
    private let dependencies: Dependencies
    
    private let service: ReferralCompleteIndicationServicing
    private let presenter: ReferralCompleteIndicationPresenting
    private var indications: [ReferralCompleteInvitation] = []
    private var lastIndicationId: String?
    private var totalIndications: Int = .max
    private lazy var rewardValue = dependencies.featureManager.number(.referralRewardValue)
    
    init(
        service: ReferralCompleteIndicationServicing,
        presenter: ReferralCompleteIndicationPresenting,
        dependencies: Dependencies
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
    }
}

extension ReferralCompleteIndicationViewModel {
    private func presentIndications(indications: [ReferralCompleteInvitation], totalIndications: Int) {
        presenter.presentCompleteIndications(indications: indications)
        presenter.presentIndicationsCount(indicationsCount: indications.count)
        presenter.presentReward(
            reward: Double(totalIndications) * Double(truncating: self.rewardValue ?? 0)
        )
    }
}

// MARK: - ReferralCompleteIndicationsViewModelInputs
extension ReferralCompleteIndicationViewModel: ReferralCompleteIndicationViewModelInputs {
    func refreshIndications() {
        indications = []
        lastIndicationId = nil
        fetchCompleteIndications()
    }
    
    func updateIndicationsIfNeeded(index: Int) {
        guard index == indications.count - 2 else {
            return
        }
        
        fetchCompleteIndications()
    }
    
    func fetchCompleteIndications() {
        guard totalIndications > indications.count else {
            return
        }
        
        service.fetchCompleteIndications(lastIndicationId: lastIndicationId) { result in
            switch result {
            case let .success(indicationsResponse):
                self.indications.append(contentsOf: indicationsResponse.completeInvitations)
                self.lastIndicationId = indicationsResponse.lastItemId
                self.totalIndications = indicationsResponse.total
                
                if indicationsResponse.completeInvitations.isEmpty {
                    self.presenter.presentEmptyIndications()
                } else {
                    self.presentIndications(
                        indications: self.indications,
                        totalIndications: self.totalIndications
                    )
                }
            case .failure:
                self.presenter.presentErrorAlert()
            }
        }
    }
    
    func updateDescription() {
        presenter.updateDescription()
    }
}
