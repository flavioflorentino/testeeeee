import Core

enum ReferralCompleteIndicationEndPoint {
    case fetchCompleteIndications(lastItemId: String?, itemsLimit: Int)
}

extension ReferralCompleteIndicationEndPoint: ApiEndpointExposable {
    var path: String {
        "mgm/v2/invitations/complete"
    }
    
    var parameters: [String: Any] {
        guard case let .fetchCompleteIndications(lastItemId, itemsLimit) = self else {
            return [:]
        }
        
        var parameters: [String: Any] = ["items_limit": itemsLimit]
        
        if let lastItemId = lastItemId {
            parameters["last_item_id"] = lastItemId
        }
        
        return parameters
    }
}
