import SkeletonView
import UI
import UIKit

extension ReferralCompleteIndicationCell.Layout {
    enum Image {
        static let size = CGSize(width: 48, height: 48)
    }
}

final class ReferralCompleteIndicationCell: UITableViewCell {
    fileprivate enum Layout { }
    
    static let identifier = String(describing: ReferralCompleteIndicationCell.self)
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view
            .viewStyle(RoundedViewStyle(cornerRadius: .strong))
            .with(\.backgroundColor, .backgroundPrimary())
        
        view.isSkeletonable = true
        
        return view
    }()
    
    private lazy var contactImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = Layout.Image.size.height / 2
        imageView.layer.masksToBounds = true
        
        return imageView
    }()
    
    private lazy var contactUserNameLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale500(.default))
        
        return label
    }()
    
    private lazy var contactNameLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700(.default))
        
        return label
    }()
    
    private lazy var stateLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle(type: .highlight))
            .with(\.textColor, .neutral400(.default))
        
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutSkeletonIfNeeded()
    }
}

extension ReferralCompleteIndicationCell {
    func setup(with presenter: ReferralCompleteIndicationCellPresenting) {
        contactImageView.setImage(url: presenter.contactImageUrl)
        contactNameLabel.text = presenter.contactName
        contactUserNameLabel.text = presenter.contactUserName
        stateLabel.text = presenter.state
    }
}

extension ReferralCompleteIndicationCell: ViewConfiguration {
    func buildViewHierarchy() {
        containerView.addSubview(contactImageView)
        containerView.addSubview(contactUserNameLabel)
        containerView.addSubview(contactNameLabel)
        containerView.addSubview(stateLabel)
        
        addSubview(containerView)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalToSuperview()
        }
        
        contactImageView.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.Image.size)
        }
        
        contactNameLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.leading.equalTo(contactImageView.snp.trailing).offset(Spacing.base01)
        }
        
        contactUserNameLabel.snp.makeConstraints {
            $0.top.equalTo(contactNameLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalTo(contactImageView.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalTo(containerView.snp.trailing).offset(-Spacing.base02)
        }
        
        stateLabel.snp.makeConstraints {
            $0.top.equalTo(contactUserNameLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalTo(contactImageView.snp.trailing).offset(Spacing.base01)
            $0.bottom.equalTo(containerView.snp.bottom).offset(-Spacing.base02)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.backgroundSecondary.color
    }
}
