import SkeletonView
import UI
import UIKit

extension ReferralIndicationsSkeletonView.Layout {
    enum Description {
        static let lastLineFillPercent = 50
        static let linesCornerRadius = 5
        static let height: CGFloat = 80.0
    }
    
    enum Cell {
        static let height: CGFloat = 120.0
        static let lines = 10
    }
}

public final class ReferralIndicationsSkeletonView: UIView, StatefulViewing {
    fileprivate enum Layout { }
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(ReferralInviteSkeletonCell.self, forCellReuseIdentifier: ReferralInviteSkeletonCell.identifier)
        tableView.isUserInteractionEnabled = false
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear

        return tableView
    }()
    
    public var viewModel: StatefulViewModeling?
    public weak var delegate: StatefulDelegate?
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ReferralIndicationsSkeletonView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(tableView)
    }
    
    public func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    public func configureViews() {
        isSkeletonable = true
        backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - UITableViewDataSource
extension ReferralIndicationsSkeletonView: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        Layout.Cell.lines
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReferralInviteSkeletonCell.identifier, for: indexPath) as? ReferralInviteSkeletonCell else {
            return UITableViewCell()
        }
        cell.showAnimateSkeleton()
        return cell
    }
}

// MARK: - UITableViewDelegate
extension ReferralIndicationsSkeletonView: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        Layout.Cell.height
    }
}
