import UIKit

protocol ReferralCompleteIndicationCellPresenting {
    var contactImageUrl: URL? { get }
    var contactName: String? { get }
    var contactUserName: String? { get }
    var state: String { get }
}

struct ReferralCompleteIndicationCellPresenter: ReferralCompleteIndicationCellPresenting {
    let contactImageUrl: URL?
    let contactName: String?
    let contactUserName: String?
    let state: String
}
