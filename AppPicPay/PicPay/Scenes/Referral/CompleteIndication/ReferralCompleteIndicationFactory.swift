import Foundation

enum ReferralCompleteIndicationFactory {
    static func make() -> ReferralCompleteIndicationViewController {
        let container = DependencyContainer()
        let service: ReferralCompleteIndicationServicing = ReferralCompleteIndicationService(dependencies: container)
        let presenter: ReferralCompleteIndicationPresenting = ReferralCompleteIndicationPresenter()
        let viewModel = ReferralCompleteIndicationViewModel(service: service, presenter: presenter, dependencies: container)
        let viewController = ReferralCompleteIndicationViewController(viewModel: viewModel)
        
        presenter.viewController = viewController
        
        return viewController
    }
}
