import UI
import UIKit
import SkeletonView

protocol ReferralCompleteIndicationDisplay: AnyObject {
    func displayCompleteIndications(_ cells: [ReferralCompleteIndicationCellPresenting])
    func displayIndicationsDescriptionAtributted(atributted: NSAttributedString)
    func displayReward(reward: String?)
    func displayErrorView()
    func displayEmptyIndicationsView()
}

private extension ReferralCompleteIndicationViewController.Layout {
    enum Header {
        static let height: CGFloat = 50.0
    }
    
    enum Content {
        static let estimatedRowHeight: CGFloat = 120
    }
}

final class ReferralCompleteIndicationViewController: ViewController<ReferralCompleteIndicationViewModelInputs, UIView> {
    typealias Localizable = ReferralCodeLocalizable
    fileprivate enum Layout { }
    
    private lazy var headerView: ReferralInviteContactsHeaderView = {
        let headerView = ReferralInviteContactsHeaderView(
            title: Localizable.indicatedHeaderTitle.text,
            rewardTitle: Localizable.indicatedHeaderRewardTitle.text
        )
        headerView.backgroundColor = Colors.backgroundSecondary.color
        return headerView
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = Colors.backgroundSecondary.color
        tableView.register(ReferralCompleteIndicationCell.self, forCellReuseIdentifier: ReferralCompleteIndicationCell.identifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Layout.Content.estimatedRowHeight
        tableView.tableHeaderView = headerView
        tableView.tableHeaderView?.frame.size = CGSize(width: view.frame.width, height: Layout.Header.height)
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.refreshControl = refreshControl
        tableView.delegate = self
        
        return tableView
    }()
    
    private lazy var refreshControl: UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.tintColor = Colors.grayscale500.color
        refresh.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        return refresh
    }()
    
    // MARK: - Properties
    private var dataSource: TableViewHandler<String, ReferralCompleteIndicationCellPresenting, ReferralCompleteIndicationCell>?
    weak var paginationDelegate: ReferralPaginationDelegate?
    weak var indicationsTabDelegate: ReferralIndicationsTabViewControllerDelegate?
    
    override func buildViewHierarchy() {
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    @objc
    private func refreshData() {
        beginState()
        viewModel.refreshIndications()
    }
}

extension ReferralCompleteIndicationViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        beginState()
        viewModel.fetchCompleteIndications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.updateDescription()
    }
}

// MARK: ReferralCompleteIndicationsDisplay
extension ReferralCompleteIndicationViewController: ReferralCompleteIndicationDisplay {
    func displayReward(reward: String?) {
        headerView.reward = reward
    }
    
    func displayIndicationsDescriptionAtributted(atributted: NSAttributedString) {
        indicationsTabDelegate?.displayDescription(atributted: atributted)
    }
    
    func displayCompleteIndications(_ cells: [ReferralCompleteIndicationCellPresenting]) {
        endState()
        dataSource = TableViewHandler(
            data: [Section(items: cells)],
            cellType: ReferralCompleteIndicationCell.self,
            configureCell: { _, presenter, cell in
                cell.setup(with: presenter)
            }
        )
        
        refreshControl.endRefreshing()
        tableView.dataSource = dataSource
        headerView.isHidden = false
        tableView.reloadData()
    }
    
    func displayEmptyIndicationsView() {
        endState()
        let emptyView = ReferralIndicationsEmptyView(
            title: Localizable.emptyCompleteIndicationTitle.text,
            subtitle: Localizable.emptyCompleteIndicationDescription.text,
            buttonTitle: Localizable.indicationsEmptyViewButtonTitle.text) {
            self.paginationDelegate?.changeTo(page: .inviteCode)
        }
        tableView.backgroundView = emptyView
        headerView.isHidden = true
    }
    
    func displayErrorView() {
        endState()
        let errorView = ReferralIndicationsEmptyView(
            title: Localizable.errorTitle.text,
            subtitle: Localizable.errorDescription.text,
            buttonTitle: Localizable.errorButtonTitle.text,
            buttonIcon: Assets.Icons.icoRefresh.image,
            buttonActionCompletion: didTryAgain
        )
        
        tableView.backgroundView = errorView
        headerView.isHidden = true
    }
}

extension ReferralCompleteIndicationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        viewModel.updateIndicationsIfNeeded(index: indexPath.row)
    }
}

// MARK: - StatefulTransitionViewing
extension ReferralCompleteIndicationViewController: StatefulTransitionViewing {
    public func didTryAgain() {
        beginState()
        viewModel.fetchCompleteIndications()
    }
    
    public func statefulViewForLoading() -> StatefulViewing {
        ReferralIndicationsSkeletonView()
    }
}
