import Foundation

// MARK: - ReferralCompleteIndication
struct ReferralCompleteIndication: Decodable {
    let completeInvitations: [ReferralCompleteInvitation]
    let lastItemId: String
    let total: Int
}

// MARK: - CompleteInvitation
struct ReferralCompleteInvitation: Decodable {
    let campaign: String
    let invitee: CompleteInvitationInvitee
    let rewardAmount: Double
}

// MARK: - Invitee
struct CompleteInvitationInvitee: Decodable {
    let userName, name, email: String
    let avatarUrl: URL?
}
