import Foundation

// MARK: - ReferralIndicationResponse
struct ReferralCompleteIndicationResponse: Decodable {
    let data: ReferralCompleteIndication
}
