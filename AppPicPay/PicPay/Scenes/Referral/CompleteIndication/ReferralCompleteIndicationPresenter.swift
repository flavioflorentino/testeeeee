import Foundation
import UI

protocol ReferralCompleteIndicationPresenting: AnyObject {
    var viewController: ReferralCompleteIndicationDisplay? { get set }
    func presentCompleteIndications(indications: [ReferralCompleteInvitation])
    func presentErrorAlert()
    func presentReward(reward: Double)
    func presentIndicationsCount(indicationsCount: Int)
    func updateDescription()
    func presentEmptyIndications()
}

final class ReferralCompleteIndicationPresenter {
    private var indicationDescription: NSAttributedString?
    weak var viewController: ReferralCompleteIndicationDisplay?
}

// MARK: - ReferralCompleteIndicationsPresenting
extension ReferralCompleteIndicationPresenter: ReferralCompleteIndicationPresenting {
    func presentCompleteIndications(indications: [ReferralCompleteInvitation]) {
        viewController?.displayCompleteIndications(indications.map {
            ReferralCompleteIndicationCellPresenter(
                contactImageUrl: $0.invitee.avatarUrl,
                contactName: $0.invitee.name,
                contactUserName: $0.invitee.userName,
                state: String(
                    format: ReferralCodeLocalizable.completeIndicationsStateTitle.text,
                    $0.rewardAmount.toCurrencyString() ?? ""
                )
            )
        })
    }
    
    func presentIndicationsCount(indicationsCount: Int) {
        let formattedIndications = "\(indicationsCount) \(ReferralCodeLocalizable.friends.text)"
        let formattedText = String(format: ReferralCodeLocalizable.completeIndicationsDescription.text, formattedIndications)
        let attributedText = formattedText.attributedStringWithFont(
            primary: Typography.bodySecondary().font(),
            secondary: Typography.bodyPrimary(.highlight).font()
        )
        
        indicationDescription = attributedText
        viewController?.displayIndicationsDescriptionAtributted(atributted: attributedText)
    }
    
    func updateDescription() {
        guard let description = indicationDescription else {
            return
        }
        
        viewController?.displayIndicationsDescriptionAtributted(atributted: description)
    }
    
    func presentReward(reward: Double) {
        viewController?.displayReward(reward: reward.toCurrencyString())
    }
    
    func presentErrorAlert() {
        viewController?.displayErrorView()
    }

    func presentEmptyIndications() {
        viewController?.displayEmptyIndicationsView()
    }
}
