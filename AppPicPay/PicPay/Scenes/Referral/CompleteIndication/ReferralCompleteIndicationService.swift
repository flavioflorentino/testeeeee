import Core
import Foundation

protocol ReferralCompleteIndicationServicing {
    func fetchCompleteIndications(
        lastIndicationId: String?,
        completion: @escaping (Result<ReferralCompleteIndication, Error>) -> Void
    )
}

final class ReferralCompleteIndicationService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ReferralCompleteIndicationsServicing
extension ReferralCompleteIndicationService: ReferralCompleteIndicationServicing {
    func fetchCompleteIndications(lastIndicationId: String?, completion: @escaping (Result<ReferralCompleteIndication, Error>) -> Void) {
        let itemsLimit = 10
        let endpoint = ReferralCompleteIndicationEndPoint.fetchCompleteIndications(
            lastItemId: lastIndicationId,
            itemsLimit: itemsLimit
        )
        
        Api<ReferralCompleteIndicationResponse>(endpoint: endpoint).execute(
            jsonDecoder: JSONDecoder(.convertFromSnakeCase)
        ) { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case let .success(payload):
                    completion(.success(payload.model.data))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}
