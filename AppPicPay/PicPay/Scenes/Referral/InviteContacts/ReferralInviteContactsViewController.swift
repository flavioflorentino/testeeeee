import UI
import UIKit
import SkeletonView

private extension ReferralInviteContactsViewController.Layout {
    enum Size {
        static let titleLabelHeight: CGFloat = 24.0
        static let descriptionLabelHeight: CGFloat = 80.0
        static let headerHeight: CGFloat = 50.0
    }
    enum Content {
        static let estimatedRowHeight: CGFloat = 120
    }
}

final class ReferralInviteContactsViewController: ViewController<ReferralInviteContactsViewModelInputs, UIView> {
    typealias Localizable = ReferralCodeLocalizable
    fileprivate enum Layout { }

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .left)
        label.isSkeletonable = true
        
        return label
    }()
    
    private lazy var headerView: ReferralInviteContactsHeaderView = {
        let headerView = ReferralInviteContactsHeaderView(
            title: ReferralCodeLocalizable.inviteContactTitleHeader.text,
            rewardTitle: ReferralCodeLocalizable.inviteContactRewardTitleHeader.text
        )
        headerView.backgroundColor = Colors.backgroundSecondary.color
        return headerView
    }()
    
    private lazy var referralInviteContactsAccessView: ReferralInviteContactsAccessView = {
        ReferralInviteContactsAccessView(
            didTapButtonCompletion: viewModel.requestContactsAccess
        )
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = Colors.backgroundSecondary.color
        tableView.register(ReferralInviteContactsCell.self, forCellReuseIdentifier: ReferralInviteContactsCell.identifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = Layout.Content.estimatedRowHeight
        tableView.tableHeaderView = headerView
        tableView.tableHeaderView?.frame.size = CGSize(width: view.frame.width, height: Layout.Size.headerHeight)
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        
        return tableView
    }()
    
    // MARK: - Properties
    private var dataSource: TableViewHandler<String, ReferralInviteContactsCellPresenting, ReferralInviteContactsCell>?
    weak var paginationDelegate: ReferralPaginationDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        beginState()
        viewModel.fetchUnregisterContacts()
    }
    
    override func buildViewHierarchy() {
        view.addSubview(descriptionLabel)
        view.addSubview(tableView)
    }
    
    override func setupConstraints() {
        descriptionLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.height.equalTo(Layout.Size.descriptionLabelHeight)
        }
        
        tableView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base03)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
}

extension ReferralInviteContactsViewController {
    func displayInviteContactsAlert(index: Int) {
        let inviteContactsButton = Button(
            title: Localizable.inviteContactButtonTitle.text,
            type: .cta
        ) { [weak self] alertController, _ in
            self?.viewModel.inviteReferral(index: index)
            alertController.dismiss(animated: true)
        }
        
        let alert = Alert(
            with: Localizable.inviteContactsAlertTitle.text,
            text: Localizable.inviteContactsAlertMessage.text,
            buttons: [
                inviteContactsButton,
                Button(title: Localizable.notNow.text, type: .clean)
            ],
            image: Alert.Image(with: Assets.Referral.icoSendInvite.image)
        )
        
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func displayErrorView() {
        let popup = PopupViewController(
            title: Localizable.alertTitle.text,
            description: Localizable.alertMessage.text,
            preferredType: .image(Assets.Icons.sadIcon.image)
        )
        
        let confirmAction = PopupAction(
            title: ReferralCodeLocalizable.alertButtonTitle.text,
            style: .fill) { [weak self] in
            self?.viewModel.close()
        }
        
        popup.addAction(confirmAction)
        showPopup(popup)
    }
    
    func displayEmptyContactsView() {
        endState()
        let emptyView = ReferralIndicationsEmptyView(
            title: ReferralCodeLocalizable.emptyContactsTitle.text,
            subtitle: ReferralCodeLocalizable.emptyContactsDescription.text,
            buttonTitle: ReferralCodeLocalizable.indicationsEmptyViewButtonTitle.text) {
            self.paginationDelegate?.changeTo(page: .inviteCode)
        }
        tableView.backgroundView = emptyView
        headerView.isHidden = true
    }
}

// MARK: ReferralInviteContactsDisplay
extension ReferralInviteContactsViewController: ReferralInviteContactsDisplay {
    func displayReward(reward: String) {
        headerView.reward = reward
    }
    
    func displayContacts(_ cells: [ReferralInviteContactsCellPresenting]) {
        dataSource = TableViewHandler(
            data: [Section(items: cells)],
            cellType: ReferralInviteContactsCell.self,
            configureCell: { index, presenter, cell in
                cell.setup(with: presenter, index: index, and: self)
            }
        )
        
        tableView.dataSource = dataSource
        headerView.isHidden = false
        tableView.backgroundView = nil
        tableView.reloadData()
        endState()
    }
    
    func displaycontactsCount(contactsCount: String) {
        let formattedContacts = "\(contactsCount) \(ReferralCodeLocalizable.contacts.text)"
        let formattedText = String(format: ReferralCodeLocalizable.inviteContactDescription.text, formattedContacts)
        let attributedText = formattedText.attributedStringWithFont(
            primary: Typography.bodySecondary().font(),
            secondary: Typography.bodyPrimary(.highlight).font()
        )
        
        descriptionLabel.attributedText = attributedText
    }
    
    func displayReward(reward: String?) {
        headerView.reward = reward
    }
    
    func displayRequestContactsAccess() {
        endState()
        descriptionLabel.text = Localizable.contactsAccessDescription.text
        tableView.backgroundView = referralInviteContactsAccessView
        headerView.isHidden = true
    }
    
    func displayContactsCount(contactsCount: String) {
        let formattedContacts = "\(contactsCount) \(ReferralCodeLocalizable.contacts.text)"
        let formattedText = String(format: ReferralCodeLocalizable.inviteContactDescription.text, formattedContacts)
        let attributedText = formattedText.attributedStringWithFont(
            primary: Typography.bodySecondary().font(),
            secondary: Typography.bodyPrimary(.highlight).font()
        )
        
        descriptionLabel.attributedText = attributedText
    }
}

extension ReferralInviteContactsViewController: ReferralInviteContactsCellDelegate {
    func didTapActionButton(at index: Int) {
        displayInviteContactsAlert(index: index)
    }
}

// MARK: - StatefulTransitionViewing
extension ReferralInviteContactsViewController: StatefulTransitionViewing {
    public func didTryAgain() {
        viewModel.fetchUnregisterContacts()
    }
    
    public func statefulViewForLoading() -> StatefulViewing {
        ReferralInviteContactsSkeletonView()
    }
}
