import UIKit
import UI

final class ReferralInviteContactsHeaderView: UIView {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700(.default))
        label.text = title
        
        return label
    }()
    
    private lazy var rewardTitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700(.default))
        label.text = rewardTitle
        
        return label
    }()
    
    private lazy var rewardLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle(type: .highlight))
            .with(\.textColor, .branding400(.default))
        return label
    }()
    
    var reward: String? {
        didSet {
            rewardLabel.text = reward
        }
    }

    private let title: String
    private let rewardTitle: String
    
    init(title: String, rewardTitle: String) {
        self.title = title
        self.rewardTitle = rewardTitle
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ReferralInviteContactsHeaderView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(titleLabel)
        addSubview(rewardTitleLabel)
        addSubview(rewardLabel)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(Spacing.base02)
        }
        
        rewardTitleLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.trailing.equalTo(rewardLabel.snp.leading)
        }
        
        rewardLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
}
