import SkeletonView
import UI
import UIKit

extension ReferralInviteContactsSkeletonView.Layout {
    enum Description {
        static let lastLineFillPercent = 50
        static let linesCornerRadius = 5
        static let height: CGFloat = 80.0
    }
    
    enum Cell {
        static let height: CGFloat = 120.0
        static let lines = 10
    }
}

public final class ReferralInviteContactsSkeletonView: UIView, StatefulViewing {
    fileprivate enum Layout { }
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700())
            .with(\.textAlignment, .left)
        label.text = ""
        label.numberOfLines = .zero
        label.isSkeletonable = true
        label.lastLineFillPercent = Layout.Description.lastLineFillPercent
        label.linesCornerRadius = Layout.Description.linesCornerRadius
        
        return label
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(ReferralInviteSkeletonCell.self, forCellReuseIdentifier: ReferralInviteSkeletonCell.identifier)
        tableView.isUserInteractionEnabled = false
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear

        return tableView
    }()
    
    public var viewModel: StatefulViewModeling?
    public weak var delegate: StatefulDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        descriptionLabel.showAnimatedSkeleton(usingColor: Colors.grayscale100.color, animation: nil)
        descriptionLabel.layoutSkeletonIfNeeded()
    }
}

extension ReferralInviteContactsSkeletonView: ViewConfiguration {
    public func buildViewHierarchy() {
        addSubview(descriptionLabel)
        addSubview(tableView)
    }
    
    public func setupConstraints() {
        descriptionLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base03)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.height.equalTo(Layout.Description.height)
        }
        
        tableView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base06)
            $0.leading.bottom.trailing.equalToSuperview()
        }
    }
    
    public func configureViews() {
        isSkeletonable = true
        backgroundColor = Colors.backgroundPrimary.color
    }
}

// MARK: - UITableViewDataSource
extension ReferralInviteContactsSkeletonView: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        Layout.Cell.lines
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReferralInviteSkeletonCell.identifier, for: indexPath) as? ReferralInviteSkeletonCell else {
            return UITableViewCell()
        }
        cell.showAnimateSkeleton()
        return cell
    }
}

// MARK: - UITableViewDelegate
extension ReferralInviteContactsSkeletonView: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        Layout.Cell.height
    }
}
