import UIKit

protocol ReferralInviteContactsCellPresenting {
    var contactImageData: Data? { get }
    var contactImageUrl: URL? { get }
    var contactName: String? { get }
    var reward: String { get }
}

struct ReferralInviteContactsCellPresenter: ReferralInviteContactsCellPresenting {
    var contactImageData: Data?
    let contactImageUrl: URL?
    let contactName: String?
    let reward: String
    
    init(
        contactImageData: Data?,
        contactImageUrl: URL?,
        contactName: String?,
        reward: String
    ) {
        self.contactImageData = contactImageData
        self.contactImageUrl = contactImageUrl
        self.contactName = contactName
        self.reward = reward
    }
}
