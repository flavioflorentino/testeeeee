import UI
import UIKit
import SkeletonView

extension ReferralInviteSkeletonCell.Layout {
    enum Size {
        static let cornerRadiusView: CGFloat = 5
    }
}
 
final class ReferralInviteSkeletonCell: UITableViewCell {
    fileprivate enum Layout { }
        
    private lazy var containerView: UIView = {
        let view = UIView()
        view
            .viewStyle(RoundedViewStyle(cornerRadius: .strong))
            .with(\.backgroundColor, .backgroundPrimary())
        view.layer.cornerRadius = Layout.Size.cornerRadiusView
        view.layer.masksToBounds = true
        view.isSkeletonable = true
        view.backgroundColor = .red
        return view
    }()
    
    func showAnimateSkeleton() {
        containerView.showAnimatedSkeleton(usingColor: Colors.grayscale100.color, animation: nil)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutSkeletonIfNeeded()
    }
}

extension ReferralInviteSkeletonCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(containerView)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Spacing.base02)
        }
    }
}
