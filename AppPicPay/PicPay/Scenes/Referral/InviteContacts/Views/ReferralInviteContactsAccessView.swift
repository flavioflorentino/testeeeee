import UIKit
import UI

extension ReferralInviteContactsAccessView.Layout {
    enum Size {
        static let image = CGSize(width: 160, height: 160)
        static let titleLabelHeight: CGFloat = 24
        static let subtitleLabelHeight: CGFloat = 34
    }
}

final class ReferralInviteContactsAccessView: UIView {
    fileprivate enum Layout { }
    
    private lazy var containerView = UIView()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.Referral.icoAccessContacts.image
        
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(TitleLabelStyle(type: .medium))
            .with(\.textColor, .grayscale800(.default))
            .with(\.textAlignment, .center)
        label.text = ReferralCodeLocalizable.contactsAccessTitle.text
        
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700(.default))
            .with(\.textAlignment, .center)
        label.text = ReferralCodeLocalizable.contactsAccessSubtitle.text
        
        return label
    }()
    
    private lazy var requestAccessButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(PrimaryButtonStyle())
        button.setTitle(ReferralCodeLocalizable.contactsAccessButtonTitle.text, for: .normal)
        button.addTarget(self, action: #selector(didTapRequestAccessButton), for: .touchUpInside)
        
        return button
    }()
    
    private let didTapButtonCompletion: () -> Void
    
    init(didTapButtonCompletion: @escaping () -> Void) {
        self.didTapButtonCompletion = didTapButtonCompletion
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    private func didTapRequestAccessButton() {
        didTapButtonCompletion()
    }
}

extension ReferralInviteContactsAccessView: ViewConfiguration {
    func buildViewHierarchy() {
        containerView.addSubview(imageView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(subtitleLabel)
        containerView.addSubview(requestAccessButton)
        addSubview(containerView)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.leading.trailing.equalToSuperview()
        }
        
        imageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.centerX.equalToSuperview()
            $0.size.equalTo(Layout.Size.image)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.Size.titleLabelHeight)
        }
        
        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.height.equalTo(Layout.Size.subtitleLabelHeight)
        }
        
        requestAccessButton.snp.makeConstraints {
            $0.top.equalTo(subtitleLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().inset(Spacing.base02)
            $0.bottom.equalToSuperview().inset(Spacing.base02)
        }
    }
    
    func configureViews() {
        backgroundColor = Colors.groupedBackgroundPrimary.color
    }
}
