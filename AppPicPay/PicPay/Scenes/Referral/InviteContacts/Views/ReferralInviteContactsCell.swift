import UI
import UIKit
import SkeletonView

protocol ReferralInviteContactsCellDelegate: AnyObject {
    func didTapActionButton(at index: Int)
}

extension ReferralInviteContactsCell.Layout {
    enum Size {
        static let image = CGSize(width: 48, height: 48)
        static let buttonHeight: CGFloat = 32.0
    }
}

final class ReferralInviteContactsCell: UITableViewCell {
    fileprivate enum Layout { }
    
    static let identifier = String(describing: ReferralInviteContactsCell.self)
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view
            .viewStyle(RoundedViewStyle(cornerRadius: .strong))
            .with(\.backgroundColor, .backgroundPrimary())
        
        view.isSkeletonable = true
        
        return view
    }()
    
    private lazy var contactImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = Layout.Size.image.height / 2
        imageView.layer.masksToBounds = true
        
        return imageView
    }()
    
    private lazy var contactNameLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, .grayscale700(.default))
        
        return label
    }()
    
    private lazy var rewardLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(BodySecondaryLabelStyle())
            .with(\.textColor, .grayscale700(.default))
        
        return label
    }()
    
    private lazy var actionButton: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = Layout.Size.buttonHeight / 2
        button.clipsToBounds = true
        button.configure(with: Button(title: ReferralCodeLocalizable.inviteContactButtonTitle.text, type: .ghost))
        button.addTarget(self, action: #selector(didTapActionButton), for: .touchUpInside)
        
        return button
    }()
    
    private var index: Int?
    private weak var delegate: ReferralInviteContactsCellDelegate?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutSkeletonIfNeeded()
    }
}

extension ReferralInviteContactsCell {
    func setup(
        with presenter: ReferralInviteContactsCellPresenting,
        index: Int,
        and delegate: ReferralInviteContactsCellDelegate
    ) {
        self.delegate = delegate
        self.index = index
        
        if let imageData = presenter.contactImageData, let image = UIImage(data: imageData) {
            contactImageView.image = image
        } else {
           contactImageView.setImage(url: presenter.contactImageUrl)
        }
        
        contactNameLabel.text = presenter.contactName
        rewardLabel.text = presenter.reward
    }
    
    @objc
    private func didTapActionButton() {
        guard let index = index else {
            return
        }
        delegate?.didTapActionButton(at: index)
    }
}

extension ReferralInviteContactsCell: ViewConfiguration {
    func buildViewHierarchy() {
        containerView.addSubview(contactImageView)
        containerView.addSubview(contactNameLabel)
        containerView.addSubview(rewardLabel)
        containerView.addSubview(actionButton)
        
        contentView.addSubview(containerView)
    }
    
    func setupConstraints() {
        containerView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base01)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalToSuperview()
        }
        
        contactImageView.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(Spacing.base02)
            $0.size.equalTo(Layout.Size.image)
        }
        
        contactNameLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.leading.equalTo(contactImageView.snp.trailing).offset(Spacing.base01)
        }
        
        rewardLabel.snp.makeConstraints {
            $0.top.equalTo(contactNameLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalTo(contactImageView.snp.trailing).offset(Spacing.base01)
            $0.trailing.equalToSuperview().offset(Spacing.base02)
        }
        
        actionButton.snp.makeConstraints {
            $0.top.equalTo(rewardLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalToSuperview().offset(Spacing.base01)
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.bottom.equalToSuperview().offset(-Spacing.base01)
            $0.height.equalTo(Layout.Size.buttonHeight)
        }
    }
    
    func configureStyles() {
        backgroundColor = Colors.backgroundSecondary.color
    }
}
