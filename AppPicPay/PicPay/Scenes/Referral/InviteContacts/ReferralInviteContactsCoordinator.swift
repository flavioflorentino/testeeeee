import UIKit

enum ReferralInviteContactsAction {
    case share(text: String)
    case close
}

protocol ReferralInviteContactsCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: ReferralInviteContactsAction)
}

final class ReferralInviteContactsCoordinator {
    weak var viewController: UIViewController?
}

// MARK: - ReferralInviteContactsCoordinating
extension ReferralInviteContactsCoordinator: ReferralInviteContactsCoordinating {
    func perform(action: ReferralInviteContactsAction) {
        switch action {
        case let .share(text):
            presentShareController(text)
        case .close:
            close()
        }
    }
    
    private func presentShareController(_ shareText: String?) {
        let controller = UIActivityViewController(
            activityItems: [shareText as Any],
            applicationActivities: nil
        )
        
        controller.completionWithItemsHandler = { _, _, _, _ in
            self.close()
        }
        
        viewController?.present(controller, animated: true)
    }
    
    private func close() {
        viewController?.dismiss(animated: true)
    }
}
