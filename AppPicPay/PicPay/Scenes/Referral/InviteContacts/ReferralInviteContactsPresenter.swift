import Foundation

protocol ReferralInviteContactsPresenting: AnyObject {
    var viewController: ReferralInviteContactsDisplay? { get set }
    func presentRequestAuthorization()
    func presentError()
    func presentEmptyIndicationsView()
    func presentReward(reward: Double)
    func presentContactsCountText(contactsCount: String)
    func presentContacts(contacts: [ReferralInviteContact])
    func didNextStep(action: ReferralInviteContactsAction)
}

final class ReferralInviteContactsPresenter {
    private let coordinator: ReferralInviteContactsCoordinating
    weak var viewController: ReferralInviteContactsDisplay?

    init(coordinator: ReferralInviteContactsCoordinating) {
        self.coordinator = coordinator
    }
}

// MARK: - ReferralInviteContactsPresenting
extension ReferralInviteContactsPresenter: ReferralInviteContactsPresenting {
    func presentContacts(contacts: [ReferralInviteContact]) {
        let cellPresenters = contacts.map { contact in
            ReferralInviteContactsCellPresenter(
                contactImageData: contact.imageData,
                contactImageUrl: contact.imageUrl,
                contactName: contact.name,
                reward: ReferralCodeLocalizable.inviteContactsReward.text
            )
        }

        viewController?.displayContacts(cellPresenters)
    }
    
    func presentRequestAuthorization() {
        viewController?.displayRequestContactsAccess()
    }

    func presentError() {
        viewController?.displayErrorView()
    }
    
    func presentEmptyIndicationsView() {
        viewController?.displayEmptyContactsView()
    }

    func presentContactsCountText(contactsCount: String) {
        viewController?.displayContactsCount(contactsCount: contactsCount)
    }
    
    func presentReward(reward: Double) {
        viewController?.displayReward(reward: "R$ \(reward.decimalValue)")
    }
    
    func didNextStep(action: ReferralInviteContactsAction) {
        coordinator.perform(action: action)
    }
}
