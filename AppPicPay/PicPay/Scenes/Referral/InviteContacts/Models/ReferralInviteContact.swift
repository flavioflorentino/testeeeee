import Foundation

struct ReferralInviteContactResponse: Decodable {
    let data: [ReferralInviteContact]
}

struct ReferralInviteContact: Decodable {
    private enum CodingKeys: String, CodingKey {
        case name
        case imageUrl = "img_url"
        case invitationText = "invitation_text"
        case phoneNumber = "phone_number"
        case imageData
    }
    let name: String
    let imageUrl: URL?
    let invitationText: String
    let phoneNumber: String
    var imageData: Data?
}

extension ReferralInviteContact: Comparable {
    static func < (lhs: ReferralInviteContact, rhs: ReferralInviteContact) -> Bool {
        lhs.name < rhs.name
    }
    
    static func == (lhs: ReferralInviteContact, rhs: ReferralInviteContact) -> Bool {
        lhs.name == rhs.name
    }
}
