import Foundation

struct ReferralRule {
    let reward: Double?
    let description: String?
}
