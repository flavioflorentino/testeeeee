import UIKit

protocol ReferralInviteContactsDisplay: AnyObject {
    func displayReward(reward: String?)
    func displayContactsCount(contactsCount: String)
    func displayContacts(_ cells: [ReferralInviteContactsCellPresenting])
    func displayRequestContactsAccess()
    func displayEmptyContactsView()
    func displayErrorView()
}
