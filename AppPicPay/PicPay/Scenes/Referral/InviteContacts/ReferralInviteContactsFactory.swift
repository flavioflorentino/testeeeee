import Foundation

enum ReferralInviteContactsFactory {
    static func make() -> ReferralInviteContactsViewController {
        let container = DependencyContainer()
        let service: ReferralInviteContactsServicing = ReferralInviteContactsService(dependencies: container)
        let contactsService: ContactsListServicing = ContactsListService(dependencies: container)
        let coordinator: ReferralInviteContactsCoordinating = ReferralInviteContactsCoordinator()
        let presenter: ReferralInviteContactsPresenting = ReferralInviteContactsPresenter(coordinator: coordinator)
        let viewModel = ReferralInviteContactsViewModel(
            service: service,
            presenter: presenter,
            dependencies: container,
            contactsService: contactsService
        )

        let viewController = ReferralInviteContactsViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
