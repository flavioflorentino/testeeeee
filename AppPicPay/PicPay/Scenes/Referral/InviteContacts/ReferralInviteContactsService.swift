import Core
import Foundation

protocol ReferralInviteContactsServicing {
    func fetchUnregisteredContacts(
        userAgendaContacts: [UserAgendaContact],
        completion: @escaping (Result<[ReferralInviteContact], ApiError>) -> Void
    )
}

final class ReferralInviteContactsService {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - ReferralInviteContactsServicing
extension ReferralInviteContactsService: ReferralInviteContactsServicing {
    func fetchUnregisteredContacts(
        userAgendaContacts: [UserAgendaContact],
        completion: @escaping (Result<[ReferralInviteContact], ApiError>) -> Void
    ) {
        Api<ReferralInviteContactResponse>(
            endpoint: ReferralInviteContactsEndPoint.getUnregisteredContacts(userAgendaContacts: userAgendaContacts)
        ).execute { result in
            self.dependencies.mainQueue.async {
                completion(result.map(\.model.data))
            }
        }
    }
}
