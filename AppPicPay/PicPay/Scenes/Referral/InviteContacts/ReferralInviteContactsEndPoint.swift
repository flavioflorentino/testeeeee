import Core
import Foundation

enum ReferralInviteContactsEndPoint {
    case getUnregisteredContacts(userAgendaContacts: [UserAgendaContact])
}

extension ReferralInviteContactsEndPoint: ApiEndpointExposable {
    var path: String {
        "mgm/v2/consumers/contacts-to-invite/"
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var body: Data? {
        switch self {
        case let .getUnregisteredContacts(userAgendaContacts):
            let bodyContacts: [[String: Any]] = userAgendaContacts.map { contact in
                let dictionary: [String: Any] = ["name": contact.fullName, "numbers": contact.phoneNumbers]
                return dictionary
            }
            
            let bodyRequest: [String: Any] = ["contacts": bodyContacts]
        
            return bodyRequest.toData()
        }
    }
}
