import AnalyticsModule
import FeatureFlag
import Foundation

protocol ReferralInviteContactsViewModelInputs: AnyObject {
    func fetchUnregisterContacts()
    func requestContactsAccess()
    func inviteReferral(index: Int)
    func close()
}

final class ReferralInviteContactsViewModel {
    typealias Dependencies = HasFeatureManager & HasAnalytics & HasReferralManager
    private let dependencies: Dependencies
       
    private let service: ReferralInviteContactsServicing
    private let contactsService: ContactsListServicing
    private let presenter: ReferralInviteContactsPresenting
    private var userAgendaContacts: [UserAgendaContact] = []
    private var referralInviteContacts: [ReferralInviteContact] = []
    private lazy var rewardValue: Double = {
        guard let reward = dependencies.referralManager.referral?.campaign.amountInviter else {
            return Double(truncating: dependencies.featureManager.number(.referralRewardValue) ?? 0)
        }
        return reward
    }()
    
    init(
        service: ReferralInviteContactsServicing,
        presenter: ReferralInviteContactsPresenting,
        dependencies: Dependencies,
        contactsService: ContactsListServicing
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.contactsService = contactsService
    }
}

extension ReferralInviteContactsViewModel {
    private func getUserContacts(completion: @escaping ([UserAgendaContact]) -> Void) {
        contactsService.checkUserGrantContactsPermission { [weak self] userAuthorized in
            guard userAuthorized else {
                self?.presenter.presentRequestAuthorization()
                return
            }
            
            self?.contactsService.getContacts { [weak self] result in
                switch result {
                case .success(let userAgendaContacts):
                    self?.userAgendaContacts = userAgendaContacts
                    completion(userAgendaContacts)
                case .failure:
                    self?.presenter.presentError()
                }
            }
        }
    }
    
    private func updateContactsImageData(contacts: [ReferralInviteContact]) -> [ReferralInviteContact] {
        let contactsWithDataImage: [ReferralInviteContact] = contacts.map { contact in
            ReferralInviteContact(
                name: contact.name,
                imageUrl: contact.imageUrl,
                invitationText: contact.invitationText,
                phoneNumber: contact.phoneNumber,
                imageData: getContactImageData(contact: contact)
            )
        }
        
        return contactsWithDataImage
    }
    
    private func getContactImageData(contact: ReferralInviteContact) -> Data? {
        let matchContact = userAgendaContacts.first(where: { $0.fullName == contact.name })
        guard let userContact = matchContact, let imageData = Data(base64Encoded: userContact.image) else {
            return nil
        }
        
        return imageData
    }
}

// MARK: - ReferralInviteContactsViewModelInputs
extension ReferralInviteContactsViewModel: ReferralInviteContactsViewModelInputs {
    func fetchUnregisterContacts() {
        getUserContacts {  [weak self] contacts in
            self?.service.fetchUnregisteredContacts(userAgendaContacts: contacts) { [weak self] result in
                guard let self = self else {
                    return
                }
                
                switch result {
                case .success(let contacts):
                    guard !contacts.isEmpty else {
                        self.presenter.presentContactsCountText(contactsCount: "0")
                        self.presenter.presentEmptyIndicationsView()
                        return
                    }
                    
                    let sortedContacts = contacts.sorted()
                    self.referralInviteContacts = sortedContacts
                    self.presenter.presentContacts(contacts: self.updateContactsImageData(contacts: sortedContacts))
                    self.presenter.presentReward(reward: Double(sortedContacts.count) * self.rewardValue)
                    self.presenter.presentContactsCountText(contactsCount: "\(sortedContacts.count)")
                case .failure:
                    self.presenter.presentError()
                }
            }
        }
    }
    
    func requestContactsAccess() {
        dependencies.analytics.log(ReferralEvent.accessAgenda)
        contactsService.requestPermissionToAccessContacts { [weak self] accessGranted in
            if accessGranted {
                self?.fetchUnregisterContacts()
            }
        }
    }
    
    func inviteReferral(index: Int) {
        dependencies.analytics.log(ReferralEvent.sendInviteAlert)
        guard referralInviteContacts.indices.contains(index) else { return }
        let contact = referralInviteContacts[index]
        guard let text = contact.invitationText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        
        guard let shareUrl = URL(string: "whatsapp://send?text=\(text)&phone=55\(contact.phoneNumber)"),
            UIApplication.shared.canOpenURL(shareUrl) else {
                presenter.didNextStep(action: .share(text: text))
                return
        }
        
        UIApplication.shared.open(shareUrl, options: [:])
    }
    
    func close() {
        presenter.didNextStep(action: .close)
    }
}
