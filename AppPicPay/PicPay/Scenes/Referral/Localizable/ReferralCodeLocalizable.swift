import Foundation

enum ReferralCodeLocalizable: String, Localizable {
    case title
    case description
    case copyCode
    case regulation
    case whatsapp
    case twitter
    case facebook
    case others
    case shareTitle
    case termsTitle
    case alertTitle
    case alertMessage
    case alertButtonTitle
    case inviteContactButtonTitle
    case inviteContactTitle
    case inviteContactDescription
    case inviteContactTitleHeader
    case inviteContactRewardTitleHeader
    case inviteContactsReward
    case contacts
    case contactsAccessTitle
    case contactsAccessSubtitle
    case contactsAccessDescription
    case contactsAccessButtonTitle
    case emptyContactsTitle
    case emptyContactsDescription
    case inviteContactsAlertMessage
    case inviteContactsAlertTitle
    case notNow
    case indicationsDescription
    case indicated
    case indicatedHeaderTitle
    case indicatedHeaderRewardTitle
    case indicationsEmptyViewTitle
    case indicationsEmptyViewDescription
    case indicationsEmptyViewButtonTitle
    case indicationsEmptyDescription
    case errorTitle
    case errorDescription
    case errorButtonTitle
    case referralCodeTitle
    case referralInviteTitle
    case referralIndicationsTitle
    case referralCodeTabTitle
    case referralInviteTabTitle
    case referralIndicationsTabTitle
    case pendingAction
    case completeIndicationsDescription
    case friends
    case completeIndicationsStateTitle
    case inProgressTabTitle
    case completeTabTitle
    case rememberButtonTitle
    case rememberAlertTitle
    case rememberAlertDescription
    case rememberAlertButtonTitle
    case disabledButtonTitle
    case emptyCompleteIndicationTitle
    case emptyCompleteIndicationDescription
    case close

    var key: String {
        rawValue
    }
    
    var file: LocalizableFile {
        .referralCode
    }
}
