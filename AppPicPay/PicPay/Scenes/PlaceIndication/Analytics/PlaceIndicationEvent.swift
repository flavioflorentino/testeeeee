import AnalyticsModule

enum PlaceIndicationEvent: AnalyticsKeyProtocol {
    case screenOpened
    case suggestionInformation
    case exitSuggestion
    case establishmentName
    case establishmentCity
    case establishmentState
    case establishmentPhone
    case establishmentStateNetwork
    case formSubmitted
    
    private var name: String {
        switch self {
        case .screenOpened:
            return "Screen Opened"
        case .suggestionInformation:
            return "Suggestion Information"
        case .exitSuggestion:
            return "Exit Suggestion"
        case .establishmentName:
            return "Establishment Name"
        case .establishmentCity:
            return "Establishment City"
        case .establishmentState:
            return "Establishment State"
        case .establishmentPhone:
            return "Establishment Phone"
        case .formSubmitted:
            return "Form Submitted"
        case .establishmentStateNetwork:
            return "Social Network"
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("MGM - APP - PGB - \(name)", properties: [:], providers: providers)
    }
}

extension PlaceIndicationEvent {
    static func event(_ type: PlaceIndicationFormView.TextFieldType) -> PlaceIndicationEvent? {
        switch type {
        case .placeName:
            return .establishmentName
        case .city:
            return .establishmentCity
        case .state:
            return .establishmentState
        case .phone:
            return .establishmentPhone
        case .socialNetwork:
            return .establishmentStateNetwork
        }
    }
}
