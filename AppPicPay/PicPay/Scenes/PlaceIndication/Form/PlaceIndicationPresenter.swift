import Core
import Foundation

enum PlaceIndicationAction {
    case indicationSuccess
    case openInfo
    case close
}

protocol PlaceIndicationPresenting: AnyObject {
    var viewController: PlaceIndicationDisplay? { get set }
    func present(error: PlaceIndicationError)
    func startIndicatingLocation()
    func hideLoading()
    func presentErrorView()
    func didNextStep(action: PlaceIndicationAction)
}

final class PlaceIndicationPresenter: PlaceIndicationPresenting {
    private let coordinator: PlaceIndicationCoordinating
    weak var viewController: PlaceIndicationDisplay?

    init(coordinator: PlaceIndicationCoordinating) {
        self.coordinator = coordinator
    }

    func didNextStep(action: PlaceIndicationAction) {
        switch action {
        case .indicationSuccess:
            viewController?.displaySuccessAlert { [weak self] in
                self?.coordinator.perform(action: .indicationSuccess)
            }
        default:
            coordinator.perform(action: action)
        }
    }
    
    func present(error: PlaceIndicationError) {
        viewController?.displayFormError(error: error)
    }
    
    func startIndicatingLocation() {
        viewController?.clearFormErros()
        viewController?.showLoading()
    }
    
    func hideLoading() {
        viewController?.hideLoading()
    }
    
    func presentErrorView() {
        viewController?.displayErrorView()
    }
}
