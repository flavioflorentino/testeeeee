import Foundation
import Core

protocol PlaceIndicationServicing {
    func indicatePlace(placeInfo: PlaceInfoModel, completion: @escaping (Result<Void, Error>) -> Void)
}

final class PlaceIndicationService: PlaceIndicationServicing {
    typealias Dependencies = HasConsumerManager & HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func indicatePlace(placeInfo: PlaceInfoModel, completion: @escaping (Result<Void, Error>) -> Void) {
        guard
            let consumerId = dependencies.consumerManager.consumer?.wsId,
            let placeInfoDictionary = placeInfo.toDictionary()
            else {
            completion(.failure(ApiError.bodyNotFound))
            return
        }
        
        Api<NoContent>(endpoint: PlaceIndicationEndPoint.indicatePlace(consumerId: String(consumerId), placeInfo: placeInfoDictionary)).execute { result in
            self.dependencies.mainQueue.async {
                switch result {
                case .success:
                    completion(.success(()))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}
