import UIKit

protocol PlaceIndicationDisplay: AnyObject {
    func displayFormError(error: PlaceIndicationError)
    func clearFormErros()
    func showLoading()
    func hideLoading()
    func displayErrorView()
    func displaySuccessAlert(dismissAction:  @escaping () -> Void)
}
