enum PlaceIndicationLocalizable: String, Localizable {
    case title
    case description
    case formDescription
    case additionalInfoDescription
    case termsButtonTitle
    case confirmButtonTitle
    case successAlertTitle
    case successAlertSubtitle
    case successAlertButtonTitle
    case placeNameErrorMessage
    case cityErrorMessage
    case stateErrorMessage
    case genericErrorTitle
    case genericErrorMessage
    case genericErrorButtonTitle

    var key: String {
        rawValue
    }
    
    var file: LocalizableFile {
        .placeIndication
    }
}
