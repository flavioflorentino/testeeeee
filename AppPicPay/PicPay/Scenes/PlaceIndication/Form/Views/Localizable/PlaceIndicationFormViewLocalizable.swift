enum PlaceIndicationFormViewLocalizable: String, Localizable {
    case placeName
    case city
    case state
    case phone
    case contact
    case socialNetworks
    
    var key: String {
        rawValue
    }
    
    var file: LocalizableFile {
        .placeIndicationFormView
    }
}
