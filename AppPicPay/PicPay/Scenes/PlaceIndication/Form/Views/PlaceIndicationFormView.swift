import UI
import UIKit
import Validator

extension PlaceIndicationFormView.Layout {
    enum Size {
        static let textFieldHeight: CGFloat = 60
    }
    enum Mask {
        static let phone = "(00) 00000-0000"
    }
}

protocol PlaceIndicationFormViewDelegate: AnyObject {
    func textFieldDidChange(_ textField: UITextField)
}

final class PlaceIndicationFormView: UIView {
    fileprivate enum Layout {}
    
    enum TextFieldType: CaseIterable {
        case placeName
        case city
        case state
        case phone
        case socialNetwork
        
        var placeholder: String {
            switch self {
            case .placeName:
                return PlaceIndicationFormViewLocalizable.placeName.text
            case .city:
                return PlaceIndicationFormViewLocalizable.city.text
            case .state:
                return PlaceIndicationFormViewLocalizable.state.text
            case .phone:
                return PlaceIndicationFormViewLocalizable.phone.text
            case .socialNetwork:
                return PlaceIndicationFormViewLocalizable.socialNetworks.text
            }
        }
        
        var contentTypes: UITextContentType? {
            switch self {
            case .placeName:
                return .organizationName
            case .city:
                return .addressCity
            case .state:
                return .addressState
            case .phone:
                return .telephoneNumber
            default:
                return nil
            }
        }
        
        var maskString: String? {
            guard case .phone = self else {
                   return nil
            }
            return Layout.Mask.phone
        }
    }
    var textFields: [TextFieldType: UIPPFloatingTextField] = [:]
    private let textFieldTypes: [TextFieldType]
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base02
        stackView.distribution = .equalSpacing
        stackView.axis = .vertical
        
        return stackView
    }()
    
    private weak var textFieldDelegate: UITextFieldDelegate?
    private weak var delegate: PlaceIndicationFormViewDelegate?
    
    init(delegate: PlaceIndicationFormViewDelegate, textFieldDelegate: UITextFieldDelegate, types: [TextFieldType]) {
        self.delegate = delegate
        self.textFieldDelegate = textFieldDelegate
        self.textFieldTypes = types
        super.init(frame: .zero)
        
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func makeForm() {
        for type in textFieldTypes {
            let textfield = UIPPFloatingTextField()
            textfield.placeholder = type.placeholder
            textfield.delegate = textFieldDelegate
            textfield.addTarget(
                self,
                action: #selector(textFieldDidChange(_:)),
                for: UIControl.Event.editingChanged
            )
            textfield.textContentType = type.contentTypes
            textfield.keyboardType = type == .phone ? .numberPad : .default
            textfield.maskString = type.maskString
            textFields[type] = textfield
            stackView.addArrangedSubviews(textfield)
        }
    }
    
    func displayError(error: PlaceIndicationError) {
        switch error {
        case .placeNameRequired:
            textFields[.placeName]?.errorMessage = PlaceIndicationError.placeNameRequired.message
        case .cityRequired:
            textFields[.city]?.errorMessage = PlaceIndicationError.cityRequired.message
        case .stateRequired:
            textFields[.state]?.errorMessage = PlaceIndicationError.stateRequired.message
        }
    }
    
    func clearErrros() {
        textFields.values.forEach {
            $0.errorMessage = nil
        }
    }
}

extension PlaceIndicationFormView: ViewConfiguration {
    func buildViewHierarchy() {
        makeForm()
        addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.arrangedSubviews.forEach { arrangedSubview in
            arrangedSubview.layout {
                $0.height == Layout.Size.textFieldHeight
                $0.width == stackView.widthAnchor
            }
        }
        
        stackView.layout {
            $0.top == topAnchor
            $0.leading == leadingAnchor
            $0.trailing == trailingAnchor
            $0.bottom == bottomAnchor
        }
    }
}

extension PlaceIndicationFormView {
    @objc
    func textFieldDidChange(_ textField: UITextField) {
        delegate?.textFieldDidChange(textField)
    }
}
