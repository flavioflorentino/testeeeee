import Foundation

enum PlaceIndicationFactory {
    static func make() -> PlaceIndicationViewController {
        let container = DependencyContainer()
        let service: PlaceIndicationServicing = PlaceIndicationService(dependencies: container)
        let coordinator: PlaceIndicationCoordinating = PlaceIndicationCoordinator(dependencies: container)
        let presenter: PlaceIndicationPresenting = PlaceIndicationPresenter(coordinator: coordinator)
        let viewModel = PlaceIndicationViewModel(service: service, presenter: presenter)
        let viewController = PlaceIndicationViewController(viewModel: viewModel)

        coordinator.viewController = viewController
        presenter.viewController = viewController
        
        return viewController
    }
}
