import Foundation
import Validator
import AnalyticsModule

enum PlaceIndicationError {
    case placeNameRequired
    case cityRequired
    case stateRequired
    
    var message: String {
        switch self {
        case .placeNameRequired:
            return PlaceIndicationLocalizable.placeNameErrorMessage.text
        case .cityRequired:
            return PlaceIndicationLocalizable.cityErrorMessage.text
        case .stateRequired:
            return PlaceIndicationLocalizable.stateErrorMessage.text
        }
    }
}

protocol PlaceIndicationViewModelInputs: AnyObject {
    func indicatePlace(model: PlaceInfoModel)
    func trackTextFieldEvent(type: PlaceIndicationFormView.TextFieldType)
    func openInfo()
    func close()
}

final class PlaceIndicationViewModel: PlaceIndicationViewModelInputs {
    private enum Validation {
        static let placeNameLengthMin = 2
        static let cityLengthMin = 2
        static let stateLengthMin = 2
        static let phoneLengthMin = 8
    }
    
    private let service: PlaceIndicationServicing
    private let presenter: PlaceIndicationPresenting

    init(service: PlaceIndicationServicing, presenter: PlaceIndicationPresenting) {
        self.service = service
        self.presenter = presenter
        Analytics.shared.log(PlaceIndicationEvent.screenOpened)
    }
    
    func indicatePlace(model: PlaceInfoModel) {
        presenter.startIndicatingLocation()
        
        Analytics.shared.log(PlaceIndicationEvent.formSubmitted)
        
        let errors = validateForm(model: model)
        guard errors.isEmpty else {
            errors.forEach(presenter.present(error:))
            return
        }

        service.indicatePlace(placeInfo: model) { [weak self] result in
            self?.presenter.hideLoading()
            
            switch result {
            case .success:
                self?.presenter.didNextStep(action: .indicationSuccess)
            case .failure:
                self?.presenter.presentErrorView()
            }
        }
    }
    
    func openInfo() {
        Analytics.shared.log(PlaceIndicationEvent.suggestionInformation)
        presenter.didNextStep(action: .openInfo)
    }
    
    func close() {
        Analytics.shared.log(PlaceIndicationEvent.exitSuggestion)
        presenter.didNextStep(action: .close)
    }
    
    func trackTextFieldEvent(type: PlaceIndicationFormView.TextFieldType) {
        guard let log = PlaceIndicationEvent.event(type) else {
            return
        }
        Analytics.shared.log(log)
    }

    private func validateForm(model: PlaceInfoModel) -> [PlaceIndicationError] {
        var errors: [PlaceIndicationError] = []
        
        if model.name.count < Validation.placeNameLengthMin {
            errors.append(.placeNameRequired)
        }
        if model.city.count < Validation.cityLengthMin {
            errors.append(.cityRequired)
        }
        if model.state.count < Validation.stateLengthMin {
            errors.append(.stateRequired)
        }
        
        return errors
    }
}
