import Foundation

struct PlaceInfoModel {
    let name: String
    let city: String
    let state: String
    let phone: String?
    let socialMedia: String?
}

extension PlaceInfoModel: Codable {
    enum CodingKeys: String, CodingKey {
        case name = "establishment_name"
        case city = "establishment_city"
        case state = "establishment_state"
        case phone = "establishment_phone"
        case socialMedia = "establishment_social_media"
    }
}
