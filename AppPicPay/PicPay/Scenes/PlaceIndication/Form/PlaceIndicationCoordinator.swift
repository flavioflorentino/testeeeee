import UIKit
import UI

protocol PlaceIndicationCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: PlaceIndicationAction)
}

final class PlaceIndicationCoordinator: PlaceIndicationCoordinating {
    typealias Dependencies = HasNoDependency
    private let dependencies: Dependencies

    weak var viewController: UIViewController?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func perform(action: PlaceIndicationAction) {
        switch action {
        case .indicationSuccess:
            viewController?.dismiss(animated: true)
        case .openInfo:
            openInfo()
        case .close:
            viewController?.dismiss(animated: true)
        }
    }
    
    private func openInfo() {
        let infoController = PlaceIndicationAboutFactory.make()
        viewController?.present(infoController, animated: true)
    }
}
