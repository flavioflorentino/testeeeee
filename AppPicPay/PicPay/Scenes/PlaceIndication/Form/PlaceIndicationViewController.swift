import UI
import UIKit
import Validator

extension PlaceIndicationViewController.Layout {
    enum Font {
        static let titleLabelFont = UIFont.boldSystemFont(ofSize: 24)
        static let descriptionLabelFont = UIFont.systemFont(ofSize: 16)
        static let formDescriptionLabelFont = UIFont.systemFont(ofSize: 14, weight: .semibold)
    }
    
    enum Size {
        static let buttonHeight: CGFloat = 48.0
        static let descriptionHeight: CGFloat = 48.0
    }
}

final class PlaceIndicationViewController: ViewController<PlaceIndicationViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = .clear
        return scrollView
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.titleLabelFont
        label.text = PlaceIndicationLocalizable.title.text
        label.textColor = Colors.black.color
        label.textAlignment = .left
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.descriptionLabelFont
        label.text = PlaceIndicationLocalizable.description.text
        label.textColor = Colors.grayscale700.color
        label.numberOfLines = .zero
        label.textAlignment = .left
        return label
    }()
    
    private lazy var formDescriptionLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.formDescriptionLabelFont
        label.textColor = Colors.grayscale700.color
        label.text = PlaceIndicationLocalizable.formDescription.text
        label.textAlignment = .left
        return label
    }()
    
    private lazy var formView: PlaceIndicationFormView = {
        let types: [PlaceIndicationFormView.TextFieldType] = [.placeName, .city, .state]
        return PlaceIndicationFormView(delegate: self, textFieldDelegate: self, types: types)
    }()
    
    private lazy var additionalInfoLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.formDescriptionLabelFont
        label.textColor = Colors.grayscale700.color
        label.text = PlaceIndicationLocalizable.additionalInfoDescription.text
        label.textAlignment = .left
        return label
    }()
    
    private lazy var additionalInfoFormView: PlaceIndicationFormView = {
        let types: [PlaceIndicationFormView.TextFieldType] = [.socialNetwork, .phone]
        return PlaceIndicationFormView(delegate: self, textFieldDelegate: self, types: types)
    }()
    
    private lazy var confirmButton: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = Layout.Size.buttonHeight / 2
        button.clipsToBounds = true
        button.configure(with: Button(title: PlaceIndicationLocalizable.confirmButtonTitle.text, type: .cta))
        button.addTarget(self, action: #selector(didTapConfirmButton), for: .touchUpInside)
        
        return button
    }()
    
    private lazy var keyboardScrollViewHandler = KeyboardScrollViewHandler(scrollView: scrollView)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        keyboardScrollViewHandler.registerForKeyboardNotifications()
    }

    override func buildViewHierarchy() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(formDescriptionLabel)
        contentView.addSubview(formView)
        contentView.addSubview(additionalInfoLabel)
        contentView.addSubview(additionalInfoFormView)
        contentView.addSubview(confirmButton)
        
        scrollView.addSubview(contentView)
        
        view.addSubview(scrollView)
    }
    
    override func setupConstraints() {
        setupContentViewLayout()
        
        titleLabel.layout {
            $0.top == contentView.topAnchor + Spacing.base02
            $0.leading == contentView.leadingAnchor + Spacing.base02
            $0.trailing == contentView.trailingAnchor - Spacing.base02
        }
        descriptionLabel.layout {
            $0.top == titleLabel.bottomAnchor + Spacing.base03
            $0.leading == contentView.leadingAnchor + Spacing.base02
            $0.trailing == contentView.trailingAnchor - Spacing.base02
            $0.height == Layout.Size.descriptionHeight
        }
        formDescriptionLabel.layout {
            $0.top == descriptionLabel.bottomAnchor + Spacing.base03
            $0.leading == contentView.leadingAnchor + Spacing.base02
            $0.trailing == contentView.trailingAnchor - Spacing.base02
        }
        formView.layout {
            $0.top == formDescriptionLabel.bottomAnchor + Spacing.base02
            $0.leading == contentView.leadingAnchor + Spacing.base02
            $0.trailing == contentView.trailingAnchor - Spacing.base03
        }
        
        additionalInfoLabel.layout {
            $0.top == formView.bottomAnchor + Spacing.base06
            $0.leading == contentView.leadingAnchor + Spacing.base02
            $0.trailing == contentView.trailingAnchor - Spacing.base02
        }
        
        additionalInfoFormView.layout {
            $0.top == additionalInfoLabel.bottomAnchor + Spacing.base02
            $0.leading == contentView.leadingAnchor + Spacing.base02
            $0.trailing == contentView.trailingAnchor - Spacing.base02
        }
        
        confirmButton.layout {
            $0.top >= additionalInfoFormView.bottomAnchor + Spacing.base03
            $0.leading == contentView.leadingAnchor + Spacing.base02
            $0.trailing == contentView.trailingAnchor - Spacing.base02
            $0.bottom <= contentView.bottomAnchor - Spacing.base02
            $0.height == Layout.Size.buttonHeight
        }
    }
    
    private func setupContentViewLayout() {
        scrollView.layout {
            $0.top == view.compatibleSafeAreaLayoutGuide.topAnchor
            $0.bottom == view.bottomAnchor
            $0.leading == view.compatibleSafeAreaLayoutGuide.leadingAnchor
            $0.trailing == view.trailingAnchor
        }
        
        contentView.layout {
            $0.top == scrollView.topAnchor
            $0.leading == scrollView.leadingAnchor
            $0.trailing == scrollView.trailingAnchor
            $0.bottom == scrollView.bottomAnchor
            $0.width == scrollView.widthAnchor
            $0.height >= view.compatibleSafeAreaLayoutGuide.heightAnchor
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        updateNavigationBarAppearance()
        addNavigationButtons()
    }
    
    private func updateNavigationBarAppearance() {
        navigationController?.navigationBar.tintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
            navigationController?.navigationItem.largeTitleDisplayMode = .never
        }
    }
    
    private func addNavigationButtons() {
        let rightBarButtonItem = UIBarButtonItem(
            image: Assets.PlaceIndication.icoInfo.image,
            style: .plain,
            target: self,
            action: #selector(didTapInfoButton)
        )
        
        let leftBarButtonItem = UIBarButtonItem(
            image: Assets.PlaceIndication.icoBack.image,
            style: .plain,
            target: self,
            action: #selector(didTapCloseButton)
        )

        rightBarButtonItem.tintColor = Colors.branding300.color
        leftBarButtonItem.tintColor = Colors.branding300.color
        
        navigationItem.rightBarButtonItem = rightBarButtonItem
        navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    @objc
    private func didTapConfirmButton() {
        let model = PlaceInfoModel(
            name: formView.textFields[.placeName]?.text ?? "",
            city: formView.textFields[.city]?.text ?? "",
            state: formView.textFields[.state]?.text ?? "",
            phone: additionalInfoFormView.textFields[.phone]?.text ?? "",
            socialMedia: additionalInfoFormView.textFields[.socialNetwork]?.text ?? ""
        )
        
        viewModel.indicatePlace(model: model)
    }
    
    private func didTapTryAgain() {
        beginState()
        didTapConfirmButton()
    }
}

@objc
extension PlaceIndicationViewController {
    private func didTapInfoButton() {
        viewModel.openInfo()
    }

    private func didTapCloseButton() {
        viewModel.close()
    }
}

extension PlaceIndicationViewController: PlaceIndicationFormViewDelegate, UITextFieldDelegate {
    func textFieldDidChange(_ textField: UITextField) {
        if let textField = textField as? UIPPFloatingTextField {
            textField.errorMessage = nil
            
            guard
                let maskString = textField.maskString,
                let maskedText = CustomStringMask(mask: maskString).maskedText(from: textField.text) else {
                    return
            }
            textField.text = maskedText
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case formView.textFields[.placeName]:
            formView.textFields[.city]?.becomeFirstResponder()
        case formView.textFields[.city]:
            formView.textFields[.state]?.becomeFirstResponder()
        case formView.textFields[.state]:
            additionalInfoFormView.textFields[.socialNetwork]?.becomeFirstResponder()
        case formView.textFields[.socialNetwork]:
            additionalInfoFormView.textFields[.phone]?.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let textField = textField as? UIPPFloatingTextField else {
            return
        }
        
        trackTextFieldEvent(form: formView, textField: textField)
    }
    
    private func trackTextFieldEvent(form: PlaceIndicationFormView, textField: UIPPFloatingTextField) {
        guard let type = formView.textFields.first(where: { $1 == textField })?.key else {
            return
        }
               
        viewModel.trackTextFieldEvent(type: type)
    }
    
    private func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc
    private func dismissKeyboard() {
        view.endEditing(true)
    }
}

// MARK: View Model Outputs
extension PlaceIndicationViewController: PlaceIndicationDisplay {
    func displayErrorView() {
        endState(model: StatefulErrorViewModel(
            image: Assets.Emotions.iluSadFace.image,
            content: (
                title: PlaceIndicationLocalizable.genericErrorTitle.text,
                description: PlaceIndicationLocalizable.genericErrorMessage.text
            ),
            button: (
                image: Assets.Icons.icoRefresh.image,
                title: PlaceIndicationLocalizable.genericErrorButtonTitle.text)
            )
        )
    }
    
    func displayFormError(error: PlaceIndicationError) {
        hideLoading()
        formView.displayError(error: error)
    }
    
    func clearFormErros() {
        formView.clearErrros()
    }
    
    func showLoading() {
        confirmButton.startLoadingAnimating()
    }
    
    func hideLoading() {
        confirmButton.stopLoadingAnimating()
    }
    
    func displaySuccessAlert(dismissAction: @escaping () -> Void) {
        let alert = Alert(
            with: PlaceIndicationLocalizable.successAlertTitle.text,
            text: PlaceIndicationLocalizable.successAlertSubtitle.text,
            buttons: [Button(title: PlaceIndicationLocalizable.successAlertButtonTitle.text)],
            image: Alert.Image(with: Assets.PlaceIndication.icoLike.image)
        )
        
        AlertMessage.showAlert(alert, controller: self) { _, _, _ in
            dismissAction()
        }
    }
}

extension PlaceIndicationViewController: StatefulTransitionViewing {
    func didTryAgain() {
        didTapTryAgain()
    }
}
