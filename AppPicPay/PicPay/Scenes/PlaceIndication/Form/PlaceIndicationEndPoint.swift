import Core
import Foundation

enum PlaceIndicationEndPoint {
    case indicatePlace(consumerId: String, placeInfo: [String: Any])
}

extension PlaceIndicationEndPoint: ApiEndpointExposable {
    var path: String {
        "mgm/recommendations/"
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var body: Data? {
        switch self {
        case let .indicatePlace(consumerId, model):
            var dictionary = model
            dictionary["consumer_id"] = consumerId

            return dictionary.toData()
        }
    }
}
