import UI
import UIKit

extension PlaceIndicationAboutViewController.Layout {
    enum Size {
        static let imageHeight: CGFloat = 124.0
        static let imageWidth: CGFloat = 108
        static let buttonHeight: CGFloat = 48
    }
    
    enum Font {
        static let normal = UIFont.systemFont(ofSize: 16, weight: .regular)
        static let normalBold = UIFont.boldSystemFont(ofSize: 16)
        static let largeBold = UIFont.boldSystemFont(ofSize: 20)
    }
    
    enum Spacing {
        static let textViewLine: CGFloat = 10.0
    }
}

final class PlaceIndicationAboutViewController: ViewController<PlaceIndicationAboutViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = Assets.PlaceIndication.icoQrcode.image
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = Layout.Font.largeBold
        label.textAlignment = .center
        label.text = PlaceIndicationAboutLocalizable.title.text
        
        return label
    }()
    
    private lazy var textView: UITextView = {
        let attributedString = PlaceIndicationAboutLocalizable.description.text.attributedStringWith(
            normalFont: Layout.Font.normal,
            highlightFont: Layout.Font.normalBold,
            normalColor: Palette.ppColorGrayscale500.color,
            highlightColor: Palette.ppColorBranding300.color,
            underline: false
        )
        attributedString.addSpacing(of: Layout.Spacing.textViewLine)
        
        let textView = UITextView()
        textView.attributedText = attributedString
        textView.textAlignment = .center
        textView.backgroundColor = .clear

        return textView
    }()
    
    private lazy var button: UIPPButton = {
        let button = UIPPButton()
        button.cornerRadius = Layout.Size.buttonHeight / 2
        button.clipsToBounds = true
        button.configure(with: Button(title: PlaceIndicationAboutLocalizable.buttonTitle.text, type: .cta))
        button.addTarget(self, action: #selector(didTapCloseButton), for: .touchUpInside)
        
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            isModalInPresentation = true
        }
    }
 
    override func buildViewHierarchy() {
        view.addSubview(imageView)
        view.addSubview(titleLabel)
        view.addSubview(textView)
        view.addSubview(button)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override func setupConstraints() {
        imageView.layout {
            $0.top == view.compatibleSafeAreaLayoutGuide.topAnchor + Spacing.base06
            $0.width == Layout.Size.imageWidth
            $0.height == Layout.Size.imageHeight
            $0.centerX == view.centerXAnchor
        }
        
        titleLabel.layout {
            $0.top == imageView.bottomAnchor + Spacing.base03
            $0.leading == view.leadingAnchor + Spacing.base01
            $0.trailing == view.trailingAnchor - Spacing.base01
        }
        
        textView.layout {
            $0.top == titleLabel.bottomAnchor + Spacing.base02
            $0.leading == view.leadingAnchor + Spacing.base04
            $0.trailing == view.trailingAnchor - Spacing.base04
        }
        
        button.layout {
            $0.top == textView.bottomAnchor + Spacing.base03
            $0.leading == view.leadingAnchor + Spacing.base02
            $0.trailing == view.trailingAnchor - Spacing.base02
            $0.bottom == view.bottomAnchor - Spacing.base04
            $0.height == Layout.Size.buttonHeight
        }
    }
}

private extension PlaceIndicationAboutViewController {
    @objc
    func didTapCloseButton() {
        viewModel.close()
    }
}
