enum PlaceIndicationAboutLocalizable: String, Localizable {
    case title
    case description
    case buttonTitle
    
    var key: String {
        rawValue
    }
    var file: LocalizableFile {
        .placeIndicationAbout
    }
}
