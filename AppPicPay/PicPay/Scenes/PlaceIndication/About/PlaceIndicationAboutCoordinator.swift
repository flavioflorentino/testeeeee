import UIKit

enum PlaceIndicationAboutAction {
    case close
}

protocol PlaceIndicationAboutCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: PlaceIndicationAboutAction)
}

final class PlaceIndicationAboutCoordinator: PlaceIndicationAboutCoordinating {
    weak var viewController: UIViewController?

    func perform(action: PlaceIndicationAboutAction) {
        viewController?.dismiss(animated: true)
    }
}
