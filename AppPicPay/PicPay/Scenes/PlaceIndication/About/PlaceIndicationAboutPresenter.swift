import Core
import Foundation

protocol PlaceIndicationAboutPresenting: AnyObject {
    func didNextStep(action: PlaceIndicationAboutAction)
}

final class PlaceIndicationAboutPresenter: PlaceIndicationAboutPresenting {
    private let coordinator: PlaceIndicationAboutCoordinating

    init(coordinator: PlaceIndicationAboutCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: PlaceIndicationAboutAction) {
        coordinator.perform(action: action)
    }
}
