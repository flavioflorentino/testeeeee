import Foundation

protocol PlaceIndicationAboutViewModelInputs: AnyObject {
    func close()
}

final class PlaceIndicationAboutViewModel {
    private let presenter: PlaceIndicationAboutPresenting

    init(presenter: PlaceIndicationAboutPresenting) {
        self.presenter = presenter
    }
}

extension PlaceIndicationAboutViewModel: PlaceIndicationAboutViewModelInputs {
    func close() {
        presenter.didNextStep(action: .close)
    }
}
