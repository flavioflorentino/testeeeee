import Foundation

enum PlaceIndicationAboutFactory {
    static func make() -> PlaceIndicationAboutViewController {
        let coordinator: PlaceIndicationAboutCoordinating = PlaceIndicationAboutCoordinator()
        let presenter: PlaceIndicationAboutPresenting = PlaceIndicationAboutPresenter(coordinator: coordinator)
        let viewModel = PlaceIndicationAboutViewModel(presenter: presenter)
        let viewController = PlaceIndicationAboutViewController(viewModel: viewModel)

        coordinator.viewController = viewController

        return viewController
    }
}
