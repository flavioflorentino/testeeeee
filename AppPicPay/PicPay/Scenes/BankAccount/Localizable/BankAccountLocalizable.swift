import Foundation

enum BankAccountLocalizable: String, Localizable {
    case bankNotFound
    case bankMyAccount
    case bankRemoveTitle
    case bankRemoveDescription
    case bankRemoveAccount
    case bankAccountTitle
    case bankFind
    
    case invalidBranch
    case invalidAccount
    case invalidCPF
    case requiredField
    case invalidAccountType
    
    case pickAccountType
    case branchDigitDisclaimer
    case bankCellText
    
    case bankAccountViewTitle
    case changeBankAccountButtonTitle
    
    // MARK: - Register bank account splash
    
    case registerBankAccountHeadline
    case registerBankAccountCaption
    case registerBankAccountButtonTitle
    
    // MARK: - Bank account form
    case newBankAccountFormBankCaptionTitle
    case newBankAccountHeadingMessage
    case newBankAccountViewTitle
    
    case newBankAccountFormBranchPlaceholder
    case newBankAccountFormDigitPlaceholder
    case newBankAccountFormAccountNumberPlaceholder
    case newBankAccountFormAccountTypePlaceholder
    case newBankAccountFormNotMyAccountLabelText
    case newBankAccountFormAccountHolderDocumentPlaceholder
    case newBankAccountFormRecipientNamePlaceholder
    case newBankAccountFormSecurityAcknowledgeMessage
    case newBankAccountFormSaveBankAccountButtonTitle
    
    var key: String {
        self.rawValue
    }
    var file: LocalizableFile {
        .bankAccount
    }
}
