protocol RegisterBankAccountSplashDisplay: AnyObject {
    func displayInstructions(headlineText: String?, captionText: String, illustrativeImage: ImageAsset.Image)
}
