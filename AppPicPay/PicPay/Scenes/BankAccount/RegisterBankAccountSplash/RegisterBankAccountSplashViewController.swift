import UI
import UIKit

extension RegisterBankAccountSplashViewController.Layout {
    enum Insets {
        static let viewInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        @available(iOS 11, *)
        static let directionalViewInsets = NSDirectionalEdgeInsets(top: 20, leading: 20, bottom: 20, trailing: 20)
    }
    
    enum Height {
        static let defaultButtonHeight: CGFloat = 44.0
    }
    
    enum Spacing {
        static let informativeStackCenterYOffset: CGFloat = -64.0
        static let informativeStackViewSpacing: CGFloat = 25
        static let textStackViewSpacing: CGFloat = 12
    }
    
    enum Font {
        static let headline: UIFont = .systemFont(ofSize: 16, weight: .semibold)
        static let caption: UIFont = .systemFont(ofSize: 13)
    }
}

final class RegisterBankAccountSplashViewController: ViewController<RegisterBankAccountSplashViewModelInputs, UIView> {
    fileprivate enum Layout {}
    
    enum AccessibilityIdentifier: String {
        case headlineLabel
        case captionLabel
        case registerBankAccountButton
    }
    
    private lazy var illustrationImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.setContentHuggingPriority(.defaultHigh, for: .vertical)
        imageView.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        imageView.setContentCompressionResistancePriority(.required, for: .vertical)
        imageView.setContentCompressionResistancePriority(.required, for: .horizontal)
        return imageView
    }()
    
    private lazy var headlineLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.headline
        label.numberOfLines = 0
        label.textAlignment = .center
        label.accessibilityIdentifier = AccessibilityIdentifier.headlineLabel.rawValue
        return label
    }()
    
    private lazy var captionLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.caption
        label.textColor = Palette.ppColorGrayscale400.color
        label.numberOfLines = 0
        label.textAlignment = .center
        label.accessibilityIdentifier = AccessibilityIdentifier.captionLabel.rawValue
        return label
    }()
    
    private lazy var textStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [self.headlineLabel, self.captionLabel])
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = Layout.Spacing.textStackViewSpacing
        return stackView
    }()
    
    private lazy var informativeStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [self.illustrationImageView, self.textStackView])
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.spacing = Layout.Spacing.informativeStackViewSpacing
        return stackView
    }()
    
    private lazy var registerBankAccountButton: UIPPButton = {
        let button = UIPPButton(
            title: BankAccountLocalizable.registerBankAccountButtonTitle.text,
            target: self,
            action: #selector(registerBankAccountButtonTapped)
        )
        button.accessibilityIdentifier = AccessibilityIdentifier.registerBankAccountButton.rawValue
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchInstructions()
    }

    override func buildViewHierarchy() {
        super.buildViewHierarchy()
        view.addSubviews(informativeStackView, registerBankAccountButton)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        informativeStackView.layout {
            $0.leading == view.layoutMarginsGuide.leadingAnchor
            $0.trailing == view.layoutMarginsGuide.trailingAnchor
            $0.centerY == view.compatibleSafeAreaLayoutGuide.centerYAnchor + Layout.Spacing.informativeStackCenterYOffset
        }
        
        registerBankAccountButton.layout {
            $0.leading == view.layoutMarginsGuide.leadingAnchor
            $0.trailing == view.layoutMarginsGuide.trailingAnchor
            $0.bottom == view.layoutMarginsGuide.bottomAnchor
            $0.height == Layout.Height.defaultButtonHeight
        }
    }
    
    override func configureViews() {
        super.configureViews()
        if #available(iOS 11.0, *) {
            view.directionalLayoutMargins = Layout.Insets.directionalViewInsets
        } else {
            view.layoutMargins = Layout.Insets.viewInsets
        }
        view.backgroundColor = Palette.ppColorGrayscale000.color
        title = BankAccountLocalizable.bankAccountViewTitle.text
    }
    
    // MARK: - Actions
    
    @objc
    func registerBankAccountButtonTapped() {
        viewModel.registerBankAccount()
    }
}

// MARK: View Model Outputs
extension RegisterBankAccountSplashViewController: RegisterBankAccountSplashDisplay {
    func displayInstructions(headlineText: String?, captionText: String, illustrativeImage: ImageAsset.Image) {
        headlineLabel.text = headlineText
        captionLabel.text = captionText
        illustrationImageView.image = illustrativeImage
    }
}
