protocol RegisterBankAccountSplashViewModelInputs: AnyObject {
    func fetchInstructions()
    func registerBankAccount()
}

final class RegisterBankAccountSplashViewModel {
    private let presenter: RegisterBankAccountSplashPresenting

    init(presenter: RegisterBankAccountSplashPresenting) {
        self.presenter = presenter
    }
}

extension RegisterBankAccountSplashViewModel: RegisterBankAccountSplashViewModelInputs {
    func fetchInstructions() {
        presenter.displayInstructions()
    }
    
    func registerBankAccount() {
        presenter.didNextStep(action: .bankList)
    }
}
