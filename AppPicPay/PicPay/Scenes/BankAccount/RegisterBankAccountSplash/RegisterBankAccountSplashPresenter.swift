protocol RegisterBankAccountSplashPresenting: AnyObject {
    var viewController: RegisterBankAccountSplashDisplay? { get set }
    func displayInstructions()
    func didNextStep(action: RegisterBankAccountSplashAction)
}

final class RegisterBankAccountSplashPresenter: RegisterBankAccountSplashPresenting {
    private let coordinator: RegisterBankAccountSplashCoordinating
    weak var viewController: RegisterBankAccountSplashDisplay?

    init(coordinator: RegisterBankAccountSplashCoordinating) {
        self.coordinator = coordinator
    }
    
    func displayInstructions() {
        viewController?.displayInstructions(
            headlineText: BankAccountLocalizable.registerBankAccountHeadline.text,
            captionText: BankAccountLocalizable.registerBankAccountCaption.text,
            illustrativeImage: Assets.Ilustration.iluBankFt.image
        )
    }
    
    func didNextStep(action: RegisterBankAccountSplashAction) {
        coordinator.perform(action: action)
    }
}
