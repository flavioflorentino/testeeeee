import UIKit

enum RegisterBankAccountSplashFactory {
    static func make(hidesBottomBarWhenPushed: Bool) -> UIViewController {
        let container = DependencyContainer()
        let coordinator: RegisterBankAccountSplashCoordinating = RegisterBankAccountSplashCoordinator(dependencies: container)
        let presenter: RegisterBankAccountSplashPresenting = RegisterBankAccountSplashPresenter(coordinator: coordinator)
        let viewModel = RegisterBankAccountSplashViewModel(presenter: presenter)
        let viewController = RegisterBankAccountSplashViewController(viewModel: viewModel)
        
        viewController.hidesBottomBarWhenPushed = hidesBottomBarWhenPushed
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
