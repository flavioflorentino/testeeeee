import UIKit

enum RegisterBankAccountSplashAction {
    case bankList
}

protocol RegisterBankAccountSplashCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: RegisterBankAccountSplashAction)
}

final class RegisterBankAccountSplashCoordinator: RegisterBankAccountSplashCoordinating {
    typealias Dependencies = HasConsumerManager
    weak var viewController: UIViewController?
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func perform(action: RegisterBankAccountSplashAction) {
        guard
            action == .bankList,
            let navigationController = viewController?.navigationController
            else {
                return
        }
        let bankListViewController = BanksFactory.make(onSaveCompletion: { [weak self] _ in
            self?.saveCompletion(navigationController)
        })
        navigationController.pushViewController(bankListViewController, animated: true)
    }
    
    private func saveCompletion(_ navigationController: UINavigationController) {
        guard
            let bankAccount = dependencies.consumerManager.consumer?.bankAccount,
            let bankAccountViewController = BankAccountFactory.make(with: [bankAccount], hidesBottomBarWhenPushed: true)
            else {
                navigationController.popToRootViewController(animated: true)
                return
        }
        bankAccountViewController.hidesBottomBarWhenPushed = true
        navigationController.viewControllers.insert(bankAccountViewController, at: 1)
        navigationController.viewControllers.removeAll { $0 == self.viewController }
        navigationController.popToViewController(bankAccountViewController, animated: true)
    }
}
