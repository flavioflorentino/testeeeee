import Core
import Foundation

final class PixBanksService: BanksServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func fetchBanks(completion: @escaping (Result<[BankListSection], ApiError>) -> Void) {
        WSCommon.bankList(url: kWsUrlGetInstitutions) { [weak self] banksSections, error in
            self?.dependencies.mainQueue.async {
                guard let banksSections = banksSections else {
                    completion(.failure(.unknown(error)))
                    return
                }
                completion(.success(banksSections))
            }
        }
    }
}
