import UI

protocol BanksDisplay: AnyObject {
    func display(banks: [Section<String, BankItem>])
    func displayEmptySearchResults()
    func display(error: Error)
    func displayInternalError(error: Error)
    func startLoading()
    func stopLoading()
}
