import Foundation
import PIX

enum BanksFactory {
    static func make(onSaveCompletion: ((_ saved: Bool) -> Void)?) -> UIViewController {
        let coordinator: BanksCoordinating = BanksCoordinator()
        let service: BanksServicing = BanksService()
        coordinator.onSaveCompletion = onSaveCompletion
        return createBankScene(coordinator: coordinator, service: service)
    }
    
    private static func createBankScene(coordinator: BanksCoordinating, service: BanksServicing, title: String? = nil, searchPlaceholder: String? = nil) -> UIViewController {
        let presenter: BanksPresenting = BanksPresenter(coordinator: coordinator)
        let viewModel = BanksViewModel(service: service, presenter: presenter)
        let viewController = BanksViewController(viewModel: viewModel, definedTitle: title, searchPlaceholder: searchPlaceholder)

        coordinator.viewController = viewController
        presenter.viewController = viewController
        return viewController
    }
}
// MARK: - BankSelector
extension BanksFactory: BankSelector {
    static func make(title: String, searchPlaceholder: String, onBankSelectionCompletion: @escaping (_ wsId: String, _ name: String) -> Void) -> UIViewController {
        let coordinator = BanksSelectionCoordinator()
        let service = PixBanksService()
        coordinator.onBankSelectionCompletion = onBankSelectionCompletion
        return createBankScene(coordinator: coordinator, service: service, title: title, searchPlaceholder: searchPlaceholder)
    }
}
