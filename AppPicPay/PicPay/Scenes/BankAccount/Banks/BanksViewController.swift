import UI
import UIKit
import SnapKit

final class BanksViewController: ViewController<BanksViewModelInputs, UIView> {
    enum AccessibilityIdentifier: String {
        case banksTableView
        case banksSearchBar
        case errorView
        case emptySearchResultsView
    }
    // MARK: - Layout components
    
    private(set) lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.searchBar.barTintColor = Palette.ppColorGrayscale100.color
        searchController.searchBar.tintColor = Palette.ppColorBranding300.color
        searchController.searchBar.isTranslucent = false
        searchController.searchBar.backgroundImage = UIImage()
        searchController.searchBar.placeholder = searchPlaceholder ?? BankAccountLocalizable.bankFind.text
        searchController.searchBar.accessibilityIdentifier = AccessibilityIdentifier.banksSearchBar.rawValue
        return searchController
    }()
    
    private(set) lazy var activityIndicatorView = UIActivityIndicatorView(style: .gray)
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.register(BankItemTableViewCell.self, forCellReuseIdentifier: BankItemTableViewCell.identifier)
        tableView.accessibilityIdentifier = AccessibilityIdentifier.banksTableView.rawValue
        return tableView
    }()
    
    // MARK: - Instance variables
    
    private var tableViewHandler: TableViewHandler<String, BankItem, BankItemTableViewCell>?
    private lazy var keyboardScrollViewHandler = KeyboardScrollViewHandler(scrollView: tableView)
    private let definedTitle: String?
    private let searchPlaceholder: String?
    
    // MARK: - View life cycle
    init(viewModel: BanksViewModelInputs, definedTitle: String?, searchPlaceholder: String?) {
        self.definedTitle = definedTitle
        self.searchPlaceholder = searchPlaceholder
        super.init(viewModel: viewModel)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchBanks()
        keyboardScrollViewHandler.registerForKeyboardNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
        navigationController?.navigationBar.barTintColor = Palette.ppColorGrayscale100.color
        clearsTableViewSelection(animated: animated)
    }
    
    private func setupSearchPosition() {
        definesPresentationContext = true
        if #available(iOS 11, *) {
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            tableView.tableHeaderView = searchController.searchBar
        }
    }
 
    override func buildViewHierarchy() {
        view.addSubview(tableView)
        setupSearchPosition()
    }
    
    override func setupConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    override func configureViews() {
        title = definedTitle ?? BankAccountLocalizable.bankAccountTitle.text
    }
    
    private func clearsTableViewSelection(animated: Bool) {
        guard let indexPathForSelectedRow = tableView.indexPathForSelectedRow else {
            return
        }
        tableView.deselectRow(at: indexPathForSelectedRow, animated: animated)
    }
}

// MARK: - Search results updating
extension BanksViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        viewModel.filterBanks(query: searchController.searchBar.text)
    }
}

// MARK: - View Model Outputs
extension BanksViewController: BanksDisplay {
    func display(banks: [Section<String, BankItem>]) {
        tableViewHandler = TableViewHandler(
            data: banks,
            cellType: BankItemTableViewCell.self,
            configureCell: { _, bank, cell in
                cell.setup(for: bank)
            },
            configureSection: { _, section -> Header? in
                guard let sectionTitle = section.header else {
                    return nil
                }
                return .view(BankAccountHeaderView(text: sectionTitle))
            }, configureDidSelectRow: { indexPath, _ in
                self.viewModel.selectBank(at: indexPath)
            }
        )
        tableView.dataSource = tableViewHandler
        tableView.delegate = tableViewHandler
        tableView.backgroundView = nil
        tableView.reloadData()
    }
    
    func displayEmptySearchResults() {
        let emptyView = SearchEmptyListView()
        emptyView.textLabel.text = BankAccountLocalizable.bankNotFound.text
        emptyView.accessibilityIdentifier = AccessibilityIdentifier.emptySearchResultsView.rawValue
        tableView.backgroundView = emptyView
    }
    
    func displayInternalError(error: Error) {
        let alert = Alert(with: error)
        let cancel = Button(title: DefaultLocalizable.btOkUnderstood.text, type: .cleanUnderlined)
        alert.buttons = [cancel]
        AlertMessage.showAlert(alert, controller: navigationController)
    }
    
    func display(error: Error) {
        let errorView = ConnectionErrorView(with: error, type: .error, frame: tableView.frame)
        errorView.tryAgainTapped = viewModel.fetchBanks
        errorView.accessibilityIdentifier = AccessibilityIdentifier.errorView.rawValue
        tableView.backgroundView = errorView
    }
    
    func startLoading() {
        tableView.backgroundView = activityIndicatorView
        activityIndicatorView.startAnimating()
    }
    
    func stopLoading() {
        activityIndicatorView.stopAnimating()
    }
}
