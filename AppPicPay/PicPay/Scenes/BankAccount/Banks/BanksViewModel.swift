import Foundation

protocol BanksViewModelInputs: AnyObject {
    func fetchBanks()
    func filterBanks(query: String?)
    func selectBank(at indexPath: IndexPath)
}

final class BanksViewModel {
    private let service: BanksServicing
    private let presenter: BanksPresenting
    
    private var sections: [BankListSection] = []
    private var filteredSections: [BankListSection]?

    init(service: BanksServicing, presenter: BanksPresenting) {
        self.service = service
        self.presenter = presenter
    }
    
    private func filteredBankListSection(_ section: BankListSection, for query: String) -> BankListSection? {
        let banks = section.rows.filter {
            bankMatchesQuery($0, query: query)
        }
        guard !banks.isEmpty else {
            return nil
        }
        return BankListSection(title: section.title, rows: banks)
    }
    
    private func bankMatchesQuery(_ bank: PPBank, query: String) -> Bool {
        guard let name = bank.name, let id = bank.wsId else {
            return false
        }
        return "\(id)\(name)".localizedStandardContains(query)
    }
}

extension BanksViewModel: BanksViewModelInputs {
    func fetchBanks() {
        presenter.startLoading()
        service.fetchBanks { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success(let sections):
                self?.sections = sections
                self?.presenter.presentBanks(sections: sections)
            case .failure(let error):
                self?.presenter.present(error: error)
            }
        }
    }
    
    func filterBanks(query: String?) {
        guard let query = query, !query.isEmpty else {
            filteredSections = nil
            presenter.presentBanks(sections: sections)
            return
        }
        let filteredSections = sections.compactMap {
            filteredBankListSection($0, for: query)
        }
        presenter.presentBanks(sections: filteredSections)
        if filteredSections.isEmpty {
            presenter.presentEmptySearchResults()
        }
        self.filteredSections = filteredSections
    }
    
    func selectBank(at indexPath: IndexPath) {
        let source = filteredSections ?? sections
        guard source.indices.contains(indexPath.section),
              source[indexPath.section].rows.indices.contains(indexPath.row) else {
            let error = PicPayError(message: DefaultLocalizable.unexpectedError.text)
            presenter.presentInternalError(error: error)
            return
        }
        let bank = source[indexPath.section].rows[indexPath.row]
        presenter.didNextStep(action: .selectBank(bank))
    }
}
