struct BankItem {
    let name: String?
    let logoURL: URL?
}

extension BankItem {
    init?(bank: PPBank) {
        guard let id = bank.wsId, let bankName = bank.name else {
            return nil
        }
        name = "\(id) - \(bankName)"
        logoURL = URL(string: bank.imageUrl ?? "")
    }
}
