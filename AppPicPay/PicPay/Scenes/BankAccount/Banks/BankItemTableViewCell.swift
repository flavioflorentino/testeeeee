import UI
import UIKit

extension BankItemTableViewCell.Layout {
    enum Font {
        static let textFont: UIFont = .systemFont(ofSize: 17)
    }
    
    enum Spacing {
        static let stackViewSpacing: CGFloat = 8
    }
    
    enum Size {
        static let logoImageViewSize = CGSize(width: 40, height: 40)
    }
    
    enum Insets {
        static let layoutMargins = UIEdgeInsets(top: 5, left: 16, bottom: 5, right: 16)
        @available(iOS 11, *)
        static let directionalLayoutMargins = NSDirectionalEdgeInsets(top: 5, leading: 16, bottom: 5, trailing: 16)
    }
}

final class BankItemTableViewCell: UITableViewCell {
    fileprivate enum Layout {}
    
    static let identifier = String(describing: BankItemTableViewCell.self)
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.textFont
        return label
    }()
    
    private lazy var logoImageView = UIImageView()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [logoImageView, nameLabel])
        stackView.spacing = Layout.Spacing.stackViewSpacing
        stackView.axis = .horizontal
        stackView.alignment = .center
        return stackView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        buildLayout()
    }
    
    func setup(for bank: BankItem) {
        logoImageView.setImage(url: bank.logoURL, placeholder: Assets.Icons.icoBankPlaceholder.image)
        nameLabel.text = bank.name
    }
}

extension BankItemTableViewCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.layout {
            $0.top == contentView.layoutMarginsGuide.topAnchor
            $0.trailing == contentView.layoutMarginsGuide.trailingAnchor
            $0.bottom == contentView.layoutMarginsGuide.bottomAnchor
            $0.leading == contentView.layoutMarginsGuide.leadingAnchor
        }
        
        logoImageView.layout {
            $0.width == Layout.Size.logoImageViewSize.width
            $0.height == Layout.Size.logoImageViewSize.height
        }
    }
    
    func configureViews() {
        contentView.preservesSuperviewLayoutMargins = false
        if #available(iOS 11, *) {
            contentView.directionalLayoutMargins = Layout.Insets.directionalLayoutMargins
        } else {
            contentView.layoutMargins = Layout.Insets.layoutMargins
        }
    }
}
