import UIKit

enum BanksAction {
    case selectBank(_ bank: PPBank)
}

protocol BanksCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var onSaveCompletion: ((_ saved: Bool) -> Void)? { get set }
    var onBankSelectionCompletion: ((_ wsId: String, _ name: String) -> Void)? { get set }
    func perform(action: BanksAction)
}

final class BanksCoordinator: BanksCoordinating {
    weak var viewController: UIViewController?
    var onSaveCompletion: ((_ saved: Bool) -> Void)?
    var onBankSelectionCompletion: ((_ wsId: String, _ name: String) -> Void)?

    func perform(action: BanksAction) {
        guard case let .selectBank(bank) = action else {
            return
        }
        let bankAccount = PPBankAccount()
        bankAccount.bank = bank
        let formController = NewBankAccountFormFactory.make(
            bankAccount: bankAccount,
            onSaveCompletion: onSaveCompletion
        )
        viewController?.show(formController, sender: nil)
    }
}
