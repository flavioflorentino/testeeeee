import UIKit

final class BanksSelectionCoordinator: BanksCoordinating {
    weak var viewController: UIViewController?
    var onSaveCompletion: ((_ saved: Bool) -> Void)?
    var onBankSelectionCompletion: ((_ wsId: String, _ name: String) -> Void)?

    func perform(action: BanksAction) {
        guard case let .selectBank(bank) = action else {
            return
        }
        let wsId = bank.wsId ?? ""
        let name = bank.name ?? ""
        onBankSelectionCompletion?(wsId, name)
        viewController?.navigationController?.popViewController(animated: true)
    }
}
