import UI
import UIKit

protocol BanksPresenting: AnyObject {
    var viewController: BanksDisplay? { get set }
    func presentBanks(sections: [BankListSection])
    func presentEmptySearchResults()
    func present(error: Error)
    func presentInternalError(error: Error)
    func startLoading()
    func stopLoading()
    func didNextStep(action: BanksAction)
}

final class BanksPresenter: BanksPresenting {
    private let coordinator: BanksCoordinating
    weak var viewController: BanksDisplay?

    init(coordinator: BanksCoordinating) {
        self.coordinator = coordinator
    }
    
    func presentBanks(sections: [BankListSection]) {
        viewController?.display(banks: sections.map(section(for:)))
    }
    
    func presentEmptySearchResults() {
        viewController?.displayEmptySearchResults()
    }
    
    func present(error: Error) {
        viewController?.display(error: error)
    }
    
    func presentInternalError(error: Error) {
        viewController?.displayInternalError(error: error)
    }
    
    func startLoading() {
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.stopLoading()
    }
    
    func didNextStep(action: BanksAction) {
        coordinator.perform(action: action)
    }
    
    private func section(for bankListSection: BankListSection) -> Section<String, BankItem> {
        let banks = bankListSection.rows.compactMap(BankItem.init(bank:))
        return Section(header: bankListSection.title, items: banks)
    }
}
