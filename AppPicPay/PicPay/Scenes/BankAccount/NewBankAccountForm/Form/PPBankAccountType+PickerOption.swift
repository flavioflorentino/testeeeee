import UI
import CoreLegacy

extension PPBankAccountType: PickerOption {
    public var rowTitle: String? {
        label
    }
    
    public var rowValue: String? {
        value
    }
}
