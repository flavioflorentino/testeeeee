import UI

struct BankAccountFormInput {
    var branchNumber: String?
    var branchDigit: String?
    var accountNumber: String?
    var accountDigit: String?
    var accountType: PickerOption?
    var differentAccountHolder: Bool = false
    var accountHolderDocument: String?
    var accountRecipientName: String?
}

extension BankAccountFormInput {
    init(bankAccount: PPBankAccount) {
        branchNumber = bankAccount.agencyNumber
        branchDigit = bankAccount.agencyDigit
        accountNumber = bankAccount.accountNumber
        accountDigit = bankAccount.accountDigit
        accountType = bankAccount.bank?.form?.accountTypes.first(where: { $0.value == bankAccount.accountType })
        differentAccountHolder = !bankAccount.cpf.isNilOrEmpty
        accountHolderDocument = bankAccount.cpf
        accountRecipientName = bankAccount.recipientName
    }
}
