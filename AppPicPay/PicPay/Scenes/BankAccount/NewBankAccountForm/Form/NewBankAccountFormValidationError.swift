import Validator
import Foundation

enum NewBankAccountFormValidationError: ValidationError, LocalizedError, CaseIterable {
    case invalidBranchNumber
    case invalidBranchDigit
    case invalidAccountNumber
    case invalidAccountDigit
    case invalidAccountType
    case invalidDocumentNumber
    case invalidRecipientName
    
    var errorDescription: String? {
         message
    }
    
    var message: String {
        switch self {
        case .invalidBranchNumber:
            return BankAccountLocalizable.invalidBranch.text
        case .invalidBranchDigit:
            return BankAccountLocalizable.requiredField.text
        case .invalidAccountNumber:
            return BankAccountLocalizable.invalidAccount.text
        case .invalidAccountDigit:
            return BankAccountLocalizable.requiredField.text
        case .invalidAccountType:
            return BankAccountLocalizable.invalidAccountType.text
        case .invalidDocumentNumber:
            return BankAccountLocalizable.invalidCPF.text
        case .invalidRecipientName:
            return BankAccountLocalizable.requiredField.text
        }
    }
}
