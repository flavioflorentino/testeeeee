import Validator

extension PPBankAccountForm: BankAccountFormConfiguration {
    var branchFieldConfiguration: FormFieldConfiguration {
         FormFieldConfiguration(
            validationRules: .init(rules: [lenghtRule(max: branchLimit, error: .invalidBranchNumber)]),
            maxTextLength: branchLimit
        )
    }
    
    var branchDigitFieldConfiguration: FormFieldConfiguration {
         FormFieldConfiguration(
            validationRules: .init(rules: [lenghtRule(max: branchDigitLimit, error: .invalidBranchDigit)]),
            keyboardType: branchDigitKeyboardType,
            isHidden: !isBranchDigitEnabled,
            maxTextLength: branchDigitLimit
        )
    }
    
    var accountFieldConfiguration: FormFieldConfiguration {
         FormFieldConfiguration(
            validationRules: .init(rules: [lenghtRule(max: accountLimit, error: .invalidAccountNumber)]),
            maxTextLength: accountLimit
        )
    }
    
    var accountDigitFieldConfiguration: FormFieldConfiguration {
         FormFieldConfiguration(
            validationRules: .init(rules: [lenghtRule(max: accountDigitLimit, error: .invalidAccountDigit)]),
            keyboardType: accountDigitKeyboardType,
            maxTextLength: accountDigitLimit
        )
    }
    
    var accountTypeFieldConfiguration: FormFieldConfiguration {
        let error = NewBankAccountFormValidationError.invalidAccountType
        let validAccountTypes = accountTypes.map({ $0.value })
        let accountTypeRule = ValidationRuleContains(sequence: validAccountTypes, error: error)
        return FormFieldConfiguration(validationRules: .init(rules: [accountTypeRule]))
    }
    
    var documentFieldConfiguration: FormFieldConfiguration {
        FormFieldConfiguration(validationRules: .init(rules: [lenghtRule(min: 14, max: 14, error: .invalidDocumentNumber)]))
    }
    
    var recipientNameFieldConfiguration: FormFieldConfiguration {
        FormFieldConfiguration(validationRules: .init(rules: [lenghtRule(min: 3, max: 256, error: .invalidRecipientName)]))
    }
    
    private func lenghtRule(min: Int = 1, max: Int, error: NewBankAccountFormValidationError) -> ValidationRuleLength {
         ValidationRuleLength(min: min, max: max, error: error)
    }
}
