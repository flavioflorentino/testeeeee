protocol BankAccountFormConfiguration {
    var branchFieldConfiguration: FormFieldConfiguration { get }
    var branchDigitFieldConfiguration: FormFieldConfiguration { get }
    var accountFieldConfiguration: FormFieldConfiguration { get }
    var accountDigitFieldConfiguration: FormFieldConfiguration { get }
    var accountTypeFieldConfiguration: FormFieldConfiguration { get }
    var documentFieldConfiguration: FormFieldConfiguration { get }
    var recipientNameFieldConfiguration: FormFieldConfiguration { get }
}
