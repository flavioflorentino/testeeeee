import Validator
import UIKit

struct FormFieldConfiguration {
    let validationRules: ValidationRuleSet<String>
    let keyboardType: UIKeyboardType
    let isHidden: Bool
    let maxTextLength: Int?
    
    init(
        validationRules: ValidationRuleSet<String>,
        keyboardType: UIKeyboardType = .numberPad,
        isHidden: Bool = false,
        maxTextLength: Int? = nil
    ) {
        self.validationRules = validationRules
        self.keyboardType = keyboardType
        self.isHidden = isHidden
        self.maxTextLength = maxTextLength
    }
}
