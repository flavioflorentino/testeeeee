import UIKit

enum NewBankAccountFormFactory {
    static func make(
        bankAccount: PPBankAccount,
        isUpdate: Bool = false,
        invalidDocumentErrorMessage: String? = nil,
        invalidRecipientNameErrorMessage: String? = nil,
        onSaveCompletion: ((_ saved: Bool) -> Void)?
    ) -> UIViewController {
        let service: NewBankAccountFormServicing = NewBankAccountFormService()
        let coordinator: NewBankAccountFormCoordinating = NewBankAccountFormCoordinator()
        let presenter: NewBankAccountFormPresenting = NewBankAccountFormPresenter(coordinator: coordinator)
        let viewModel = NewBankAccountFormViewModel(service: service, presenter: presenter, bankAccount: bankAccount)
        let viewController = NewBankAccountFormViewController(viewModel: viewModel)
        
        coordinator.viewController = viewController
        coordinator.onSaveCompletion = onSaveCompletion
        coordinator.isUpdate = isUpdate
        presenter.viewController = viewController
        viewModel.invalidDocumentErrorMessage = invalidDocumentErrorMessage
        viewModel.invalidRecipientNameErrorMessage = invalidRecipientNameErrorMessage

        return viewController
    }
}
