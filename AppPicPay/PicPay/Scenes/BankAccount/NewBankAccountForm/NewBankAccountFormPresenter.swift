import Core
import Foundation
import Validator

protocol NewBankAccountFormPresenting: AnyObject {
    var viewController: NewBankAccountFormDisplay? { get set }
    func display(bankAccount: PPBankAccount)
    func configureForm(using configuration: PPBankAccountForm)
    func startLoading()
    func stopLoading()
    func presentPopTipForBranchDigit(text: String)
    func present(error: Error)
    func present(validationErrors: [ValidationError])
    func presentInvalidDocumentNumber(message: String)
    func presentInvalidRecipientName(message: String)
    func didNextStep(action: NewBankAccountFormAction)
}

final class NewBankAccountFormPresenter: NewBankAccountFormPresenting {
    private let coordinator: NewBankAccountFormCoordinating
    weak var viewController: NewBankAccountFormDisplay?

    init(coordinator: NewBankAccountFormCoordinating) {
        self.coordinator = coordinator
    }
    
    func display(bankAccount: PPBankAccount) {
        let form = BankAccountFormInput(bankAccount: bankAccount)
        viewController?.populateForm(form)
        guard
            let bank = bankAccount.bank,
            let bankItem = BankItem(bank: bank)
            else {
                return
        }
        viewController?.display(bank: bankItem)
    }
    
    func configureForm(using configuration: PPBankAccountForm) {
        viewController?.configureForm(using: configuration)
        viewController?.configureAccountType(option: configuration.accountTypes)
    }
    
    func startLoading() {
        viewController?.changeTextFieldEnableState(to: false)
        viewController?.startLoading()
    }
    
    func stopLoading() {
        viewController?.changeTextFieldEnableState(to: true)
        viewController?.stopLoading()
    }
    
    func presentPopTipForBranchDigit(text: String) {
        viewController?.displayPopTipForBranchDigit(text: text)
    }
    
    func present(error: Error) {
        viewController?.display(error: error)
    }
    
    func present(validationErrors: [ValidationError]) {
        guard let bankFormErrors = validationErrors as? [NewBankAccountFormValidationError] else {
            return
        }
        viewController?.displayBranchNumber(errorMessage: bankFormErrors.first(where: { $0 == .invalidBranchNumber })?.message)
        viewController?.displayBranchDigit(errorMessage: bankFormErrors.first(where: { $0 == .invalidBranchDigit })?.message)
        viewController?.displayAccountNumber(errorMessage: bankFormErrors.first(where: { $0 == .invalidAccountNumber })?.message)
        viewController?.displayAccountDigit(errorMessage: bankFormErrors.first(where: { $0 == .invalidAccountDigit })?.message)
        viewController?.displayAccountType(errorMessage: bankFormErrors.first(where: { $0 == .invalidAccountType })?.message)
        viewController?.displayDocument(errorMessage: bankFormErrors.first(where: { $0 == .invalidDocumentNumber })?.message)
        viewController?.displayRecipientName(errorMessage: bankFormErrors.first(where: { $0 == .invalidRecipientName })?.message)
    }
    
    func presentInvalidDocumentNumber(message: String) {
        viewController?.displayDocument(errorMessage: message)
    }
    
    func presentInvalidRecipientName(message: String) {
        viewController?.displayRecipientName(errorMessage: message)
    }
    
    func didNextStep(action: NewBankAccountFormAction) {
        coordinator.perform(action: action)
    }
}
