import Core

protocol NewBankAccountFormServicing {
    func save(bankAccount: PPBankAccount, completion: @escaping (Result<Void, Error>) -> Void)
}

final class NewBankAccountFormService: NewBankAccountFormServicing {
    typealias Dependencies = HasMainQueue
    let dependencies: Dependencies
    
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func save(bankAccount: PPBankAccount, completion: @escaping (Result<Void, Error>) -> Void) {
        let dict: [AnyHashable: Any] = [
            "banco_id": bankAccount.bank?.wsId ?? "",
            "agencia": bankAccount.agencyNumber ?? "",
            "agencia_dv": bankAccount.agencyDigit ?? "",
            "conta": bankAccount.accountNumber ?? "",
            "conta_dv": bankAccount.accountDigit ?? "",
            "tipo": bankAccount.accountType ?? "",
            "cpf": bankAccount.cpf ?? "",
            "nome_titular": bankAccount.recipientName ?? ""
        ]
        let origin = ""
        WSConsumer.addBankAccount(dict, origin: origin) { [weak self] error in
            self?.dependencies.mainQueue.async {
                guard let error = error else {
                    completion(.success)
                    return
                }
                completion(.failure(error))
            }
        }
    }
}
