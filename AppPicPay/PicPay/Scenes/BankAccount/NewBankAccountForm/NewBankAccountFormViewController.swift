import AMPopTip
import SecurityModule
import UI
import UIKit

extension NewBankAccountFormViewController.Layout {
    enum Font {
        static let headingMessage: UIFont = .systemFont(ofSize: 13)
    }
    
    enum Lenght {
        static let secondaryTextFieldWidth: CGFloat = 84
        static let defaultButtonHeight: CGFloat = 44
    }
    
    enum Insets {
        static let contentViewLayoutMargins = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        @available(iOS 11, *)
        static let contentViewDirectionalLayoutMargins = NSDirectionalEdgeInsets(top: 16, leading: 16, bottom: 16, trailing: 16)
    }
}

final class NewBankAccountFormViewController: ViewController<NewBankAccountFormViewModelInputs, UIView>, Obfuscable {
    fileprivate enum Layout {}
    
    enum AccessibilityIdentifier: String {
        case changeBankButton
        case branchNumberTextField
        case branchDigitTextField
        case accountNumberTextField
        case accountDigitTextField
        case accountTypeTextField
        case accountTypePickerView
        case saveBankAccountButton
        case notMyAccountButton
        case documentHolderNumberTextField
        case recipientNameTextField
    }
    
    // MARK: - Layout components
    
    private lazy var headingMessageLabel: UILabel = {
        let label = UILabel()
        label.text = BankAccountLocalizable.newBankAccountHeadingMessage.text
        label.font = Layout.Font.headingMessage
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var selectedBankView: BankView = {
        let bankView = BankView()
        bankView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(changeBankTapped)))
        bankView.accessibilityIdentifier = AccessibilityIdentifier.changeBankButton.rawValue
        bankView.accessibilityTraits = [.button]
        return bankView
    }()
    
    // MARK: Form - Branch
    
    private(set) lazy var branchNumberTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = BankAccountLocalizable.newBankAccountFormBranchPlaceholder.text
        textField.keyboardType = .numberPad
        textField.inputAccessoryView = formToolbar
        textField.accessibilityIdentifier = AccessibilityIdentifier.branchNumberTextField.rawValue
        return textField
    }()
    
    private(set) lazy var branchDigitTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = BankAccountLocalizable.newBankAccountFormDigitPlaceholder.text
        textField.keyboardType = .numberPad
        textField.inputAccessoryView = formToolbar
        textField.accessibilityIdentifier = AccessibilityIdentifier.branchDigitTextField.rawValue
        return textField
    }()
    
    private lazy var branchStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [branchNumberTextField, branchDigitTextField])
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    private lazy var branchDigitPopTip: PopTip = {
        let popTip = PopTip()
        popTip.bubbleColor = Palette.ppColorGrayscale600.color
        popTip.textColor = Palette.ppColorGrayscale000.color
        popTip.edgeMargin = 10.0
        return popTip
    }()
    
    // MARK: Form - Account
    
    private(set) lazy var accountNumberTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = BankAccountLocalizable.newBankAccountFormAccountNumberPlaceholder.text
        textField.keyboardType = .numberPad
        textField.inputAccessoryView = formToolbar
        textField.accessibilityIdentifier = AccessibilityIdentifier.accountNumberTextField.rawValue
        return textField
    }()
    
    private(set) lazy var accountDigitTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = BankAccountLocalizable.newBankAccountFormDigitPlaceholder.text
        textField.keyboardType = .numberPad
        textField.inputAccessoryView = formToolbar
        textField.accessibilityIdentifier = AccessibilityIdentifier.accountDigitTextField.rawValue
        return textField
    }()
    
    private lazy var accountStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [accountNumberTextField, accountDigitTextField])
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    // MARK: Form - Account type
    
    private(set) lazy var accountTypeTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.placeholder = BankAccountLocalizable.newBankAccountFormAccountTypePlaceholder.text
        textField.inputView = accountTypePickerInputView
        textField.inputAccessoryView = formToolbar
        textField.tintColor = .clear
        textField.accessibilityIdentifier = AccessibilityIdentifier.accountTypeTextField.rawValue
        return textField
    }()
    
    private(set) lazy var accountTypePickerInputView: PickerInputView = {
        let inputView = PickerInputView()
        inputView.pickerView.accessibilityIdentifier = AccessibilityIdentifier.accountTypePickerView.rawValue
        return inputView
    }()
    
    // MARK: Form - Account holder document
    
    private(set) lazy var accountHolderContainerView: AccountHolderContainerView = {
        let container = AccountHolderContainerView()
        container.documentNumberTextField.addTarget(
            self,
            action: #selector(textFieldEditingChanged(_:)),
            for: .editingChanged
        )
        container.recipientNameTextField.addTarget(
            self,
            action: #selector(textFieldEditingChanged(_:)),
            for: .editingChanged
        )
        container.notMyAccountHeaderView.accessibilityIdentifier = AccessibilityIdentifier.notMyAccountButton.rawValue
        container.documentNumberTextField.accessibilityIdentifier = AccessibilityIdentifier.documentHolderNumberTextField.rawValue
        container.recipientNameTextField.accessibilityIdentifier = AccessibilityIdentifier.recipientNameTextField.rawValue

        return container
    }()
    
    // MARK: Form - Button
    
    private lazy var saveBankAccountButton: UIPPButton = {
        let button = UIPPButton(
            title: BankAccountLocalizable.newBankAccountFormSaveBankAccountButtonTitle.text,
            target: self,
            action: #selector(saveButtonTapped)
        )
        button.accessibilityIdentifier = AccessibilityIdentifier.saveBankAccountButton.rawValue
        return button
    }()
    
    // MARK: Security acknowledge
    
    private lazy var securityAcknowledgeView = BankAccountSecurityAcknowledgeView()
    
    // MARK: Common
    
    private(set) lazy var loadingView = LoadingView()
    
    private lazy var formToolbar = DoneToolBar(doneText: DefaultLocalizable.btOkUpperCase.text)
    
    // MARK: Root
    
    private lazy var rootStackView: UIStackView = {
        let arrangedSubviews = [
            headingMessageLabel,
            selectedBankView,
            branchStackView,
            accountStackView,
            accountTypeTextField,
            accountHolderContainerView,
            saveBankAccountButton,
            securityAcknowledgeView
        ]
        let stackView = UIStackView(arrangedSubviews: arrangedSubviews)
        stackView.spacing = Spacing.base02
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale000.color
        if #available(iOS 11, *) {
            view.directionalLayoutMargins = Layout.Insets.contentViewDirectionalLayoutMargins
        } else {
            view.layoutMargins = Layout.Insets.contentViewLayoutMargins
        }
        return view
    }()
    
    fileprivate lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.keyboardDismissMode = .interactive
        return scrollView
    }()
    
    // MARK: Instance variables
    
    private lazy var keyboardScrollViewHandler = KeyboardScrollViewHandler(scrollView: scrollView)

    // Screen obfuscation
    var appSwitcherObfuscator: AppSwitcherObfuscating?
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.fetchBankAccountInformation()
        keyboardScrollViewHandler.registerForKeyboardNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupObfuscationConfiguration()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        deinitObfuscator()
    }
 
    override func buildViewHierarchy() {
        contentView.addSubview(rootStackView)
        scrollView.addSubview(contentView)
        view.addSubview(scrollView)
    }
    
    override func setupConstraints() {
        saveBankAccountButton.layout {
            $0.height == Layout.Lenght.defaultButtonHeight
        }
        
        accountDigitTextField.layout {
            $0.width == Layout.Lenght.secondaryTextFieldWidth
        }
        
        branchDigitTextField.layout {
            $0.width == Layout.Lenght.secondaryTextFieldWidth
        }
        
        rootStackView.layout {
            $0.top == contentView.layoutMarginsGuide.topAnchor
            $0.trailing == contentView.layoutMarginsGuide.trailingAnchor
            $0.bottom == contentView.layoutMarginsGuide.bottomAnchor
            $0.leading == contentView.layoutMarginsGuide.leadingAnchor
        }
        
        contentView.layout {
            $0.top == scrollView.topAnchor
            $0.trailing == scrollView.trailingAnchor
            $0.bottom == scrollView.bottomAnchor
            $0.leading == scrollView.leadingAnchor
            $0.width == view.compatibleSafeAreaLayoutGuide.widthAnchor
        }
        
        scrollView.layout {
            $0.top == view.topAnchor
            $0.trailing == view.trailingAnchor
            $0.bottom == view.bottomAnchor
            $0.leading == view.leadingAnchor
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale100.color
        title = BankAccountLocalizable.newBankAccountViewTitle.text
    }
    
    private func configure(textField: inout UIPPFloatingTextField, using fieldConfiguration: FormFieldConfiguration) {
        textField.keyboardType = fieldConfiguration.keyboardType
        textField.isHidden = fieldConfiguration.isHidden
        textField.maxlength = fieldConfiguration.maxTextLength
        textField.addTarget(self, action: #selector(textFieldEditingChanged(_:)), for: .editingChanged)
    }
}
// MARK: Actions
@objc
extension NewBankAccountFormViewController {
    func saveButtonTapped() {
        var form = BankAccountFormInput()
        form.branchNumber = branchNumberTextField.text
        form.branchDigit = branchDigitTextField.text
        form.accountNumber = accountNumberTextField.text
        form.accountDigit = accountDigitTextField.text
        form.accountType = accountTypePickerInputView.selectedOption
        form.differentAccountHolder = accountHolderContainerView.isSelected
        form.accountHolderDocument = accountHolderContainerView.documentNumberTextField.text
        form.accountRecipientName = accountHolderContainerView.recipientNameTextField.text
        viewModel.save(bankAccount: form)
    }
    
    func changeBankTapped() {
        viewModel.changeBank()
    }
    
    func doneButtonItemTapped() {
        view.endEditing(true)
    }
    
    func textFieldEditingChanged(_ textField: UITextField) {
        guard let ppTextField = textField as? UIPPFloatingTextField else {
            return
        }
        if let maxLength = ppTextField.maxlength, let text = ppTextField.text {
            ppTextField.text = String(text.prefix(maxLength))
        }
        if ppTextField == branchDigitTextField {
            viewModel.branchDigitTextDidChange(ppTextField.text)
        }
        ppTextField.errorMessage = nil
    }
}

// MARK: View Model Outputs
extension NewBankAccountFormViewController: NewBankAccountFormDisplay {
    func display(bank: BankItem) {
        selectedBankView.setup(bank: bank)
    }
    
    func populateForm(_ form: BankAccountFormInput) {
        branchNumberTextField.text = form.branchNumber
        branchDigitTextField.text = form.branchDigit
        accountNumberTextField.text = form.accountNumber
        accountDigitTextField.text = form.accountDigit
        accountTypePickerInputView.selectedOption = form.accountType
        accountHolderContainerView.isSelected = form.differentAccountHolder
        accountHolderContainerView.documentNumberTextField.text = form.accountHolderDocument
    }
    
    func configureForm(using configuration: BankAccountFormConfiguration) {
        configure(textField: &branchNumberTextField, using: configuration.branchFieldConfiguration)
        configure(textField: &branchDigitTextField, using: configuration.branchDigitFieldConfiguration)
        configure(textField: &accountNumberTextField, using: configuration.accountFieldConfiguration)
        configure(textField: &accountDigitTextField, using: configuration.accountDigitFieldConfiguration)
        configure(textField: &accountTypeTextField, using: configuration.accountTypeFieldConfiguration)
    }
    
    func configureAccountType(option: [PickerOption]) {
        accountTypePickerInputView.options = option
        accountTypePickerInputView.inputInterfaceElement = accountTypeTextField
    }
    
    func displayBranchNumber(errorMessage: String?) {
        branchNumberTextField.errorMessage = errorMessage
    }
    
    func display(error: Error) {
        AlertMessage.showAlert(error, controller: self)
    }
    
    func changeTextFieldEnableState(to enabled: Bool) {
        branchNumberTextField.isEnabled = enabled
        branchDigitTextField.isEnabled = enabled
        accountNumberTextField.isEnabled = enabled
        accountDigitTextField.isEnabled = enabled
        accountTypeTextField.isEnabled = enabled
        accountHolderContainerView.isUserInteractionEnabled = enabled
    }
    
    func startLoading() {
        view.endEditing(true)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: UIView())
        saveBankAccountButton.startLoadingAnimating()
    }
    
    func stopLoading() {
        navigationItem.leftBarButtonItem = nil
        saveBankAccountButton.stopLoadingAnimating()
    }
    
    func displayPopTipForBranchDigit(text: String) {
        let frame = branchDigitTextField.convert(branchDigitTextField.frame, to: view)
        branchDigitPopTip.show(text: BankAccountLocalizable.branchDigitDisclaimer.text, direction: .up, maxWidth: 120, in: view, from: frame)
    }
    
    func displayBranchDigit(errorMessage: String?) {
        branchDigitTextField.errorMessage = errorMessage
    }
    
    func displayAccountNumber(errorMessage: String?) {
        accountNumberTextField.errorMessage = errorMessage
    }
    
    func displayAccountDigit(errorMessage: String?) {
        accountDigitTextField.errorMessage = errorMessage
    }
    
    func displayAccountType(errorMessage: String?) {
        accountTypeTextField.errorMessage = errorMessage
    }
    
    func displayDocument(errorMessage: String?) {
        accountHolderContainerView.documentNumberTextField.errorMessage = errorMessage
    }
    
    func displayRecipientName(errorMessage: String?) {
        accountHolderContainerView.recipientNameTextField.errorMessage = errorMessage
    }
}
