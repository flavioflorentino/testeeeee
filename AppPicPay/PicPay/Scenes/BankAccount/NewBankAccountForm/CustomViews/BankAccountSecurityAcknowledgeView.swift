import UI
import UIKit

extension BankAccountSecurityAcknowledgeView.Layout {
    enum Font {
        static let footnote: UIFont = .systemFont(ofSize: 11)
    }
}

final class BankAccountSecurityAcknowledgeView: UIView {
    fileprivate enum Layout {}
    
    private lazy var securityAcknowledgeIllustrationImageView: UIImageView = {
        let imageView = UIImageView(image: Assets.Ilustration.iluLockGray.image)
        imageView.setContentHuggingPriority(.required, for: .horizontal)
        return imageView
    }()
    
    private lazy var securityAcknowledgeMessageLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.footnote
        label.textColor = Palette.ppColorGrayscale500.color
        label.text = BankAccountLocalizable.newBankAccountFormSecurityAcknowledgeMessage.text
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [securityAcknowledgeIllustrationImageView, securityAcknowledgeMessageLabel])
        stackView.alignment = .center
        stackView.spacing = Spacing.base01
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        buildLayout()
    }
}

extension BankAccountSecurityAcknowledgeView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.layout {
            $0.top == topAnchor
            $0.trailing == trailingAnchor
            $0.bottom == bottomAnchor
            $0.leading == leadingAnchor
        }
    }
}
