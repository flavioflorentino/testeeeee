import UI
import UIKit
import Validator

extension AccountHolderContainerView.Layout {
    enum Font {
        static let caption: UIFont = .systemFont(ofSize: 14, weight: .medium)
    }
    
    enum Insets {
        static let layoutMargins = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        @available(iOS 11, *)
        static let directionalLayoutMargins = NSDirectionalEdgeInsets(top: 16, leading: 16, bottom: 16, trailing: 16)
    }
}

final class AccountHolderContainerView: UIView {
    fileprivate enum Layout {}
    
    private lazy var notMyAccountLabel: UILabel = {
        let label = UILabel()
        label.text = BankAccountLocalizable.newBankAccountFormNotMyAccountLabelText.text
        label.numberOfLines = 0
        label.font = Layout.Font.caption
        label.textColor = isSelected ? Palette.ppColorGrayscale600.color : Palette.ppColorGrayscale400.color
        return label
    }()
    
    private lazy var checkboxButton: UIButton = {
        let button = UIButton()
        button.setImage(Assets.Settings.BankAccount.grayCheckmark.image, for: .normal)
        button.setImage(Assets.Settings.BankAccount.greenFilledCheckmark.image, for: .selected)
        button.isSelected = isSelected
        button.isUserInteractionEnabled = false
        button.setContentHuggingPriority(.required, for: .horizontal)
        return button
    }()
    
    private(set) lazy var notMyAccountHeaderView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [notMyAccountLabel, checkboxButton])
        stackView.spacing = Spacing.base01
        stackView.alignment = .center
        stackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(toggleSelection)))
        stackView.isAccessibilityElement = true
        stackView.accessibilityLabel = notMyAccountLabel.text
        stackView.accessibilityTraits = isSelected ? [.button, .selected] : [.button]
        return stackView
    }()
    
    private(set) lazy var recipientNameTextField: UIPPFloatingTextField = {
      let textField = UIPPFloatingTextField()
      textField.isHidden = !isSelected
      textField.placeholder = BankAccountLocalizable.newBankAccountFormRecipientNamePlaceholder.text
      textField.keyboardType = .default
      textField.inputAccessoryView = DoneToolBar(doneText: DefaultLocalizable.btOkUpperCase.text)
      return textField
    }()
    
    private(set) lazy var documentNumberTextField: UIPPFloatingTextField = {
        let textField = UIPPFloatingTextField()
        textField.isHidden = !isSelected
        textField.placeholder = BankAccountLocalizable.newBankAccountFormAccountHolderDocumentPlaceholder.text
        textField.keyboardType = .numberPad
        textField.inputAccessoryView = DoneToolBar(doneText: DefaultLocalizable.btOkUpperCase.text)
        return textField
    }()
    
    private lazy var documentNumberMasker: TextFieldMasker = {
        let documentMask = CustomStringMask(descriptor: PersonalDocumentMaskDescriptor.cpf)
        return TextFieldMasker(textMask: documentMask)
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [notMyAccountHeaderView, recipientNameTextField, documentNumberTextField])
        stackView.axis = .vertical
        stackView.isLayoutMarginsRelativeArrangement = true
        if #available(iOS 11, *) {
            stackView.directionalLayoutMargins = Layout.Insets.directionalLayoutMargins
        } else {
            stackView.layoutMargins = Layout.Insets.layoutMargins
        }
        return stackView
    }()
    
    var isSelected: Bool = false {
        didSet {
            guard isSelected != oldValue else {
                return
            }
            setSelected(isSelected)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonSetup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonSetup()
    }
    
    private func commonSetup() {
        buildLayout()
        documentNumberMasker.bind(to: documentNumberTextField)
    }
}

// MARK: State management
extension AccountHolderContainerView {
    private func setSelected(_ isSelected: Bool) {
        checkboxButton.isSelected = isSelected
        notMyAccountLabel.textColor = isSelected ? Palette.ppColorGrayscale600.color : Palette.ppColorGrayscale400.color
        documentNumberTextField.text = ""
        recipientNameTextField.text = ""
        notMyAccountHeaderView.accessibilityTraits = isSelected ? [.button, .selected] : [.button]
        UIView.animate(withDuration: 0.3) {
            self.documentNumberTextField.isHidden = !isSelected
            self.recipientNameTextField.isHidden = !isSelected
        }
    }
}

// MARK: Action
@objc
extension AccountHolderContainerView {
    func toggleSelection() {
        isSelected.toggle()
    }
}

// MARK: - View Configuration
extension AccountHolderContainerView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(rootStackView)
    }
    
    func setupConstraints() {
        rootStackView.layout {
            $0.top == topAnchor
            $0.trailing == trailingAnchor
            $0.bottom == bottomAnchor
            $0.leading == leadingAnchor
        }
    }
    
    func configureViews() {
        layer.cornerRadius = CornerRadius.medium
        layer.borderColor = Palette.ppColorGrayscale300.cgColor
        layer.borderWidth = Border.light
    }
}
