import UI
import UIKit

extension BankView.Layout {
    enum Font {
        static let caption: UIFont = .systemFont(ofSize: 13)
        static let body: UIFont = .systemFont(ofSize: 17)
    }
    
    enum Insets {
        static let layoutMargins = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        @available(iOS 11, *)
        static let directionalLayoutMargins = NSDirectionalEdgeInsets(top: 8, leading: 0, bottom: 8, trailing: 0)
    }
    
    enum Lenght {
        static let separatorHeight: CGFloat = 0.5
    }
    
    enum Size {
        static let logoImageView = CGSize(width: 40, height: 40)
    }
}

final class BankView: UIView {
    fileprivate enum Layout {}
    
    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        imageView.setContentHuggingPriority(.defaultHigh, for: .vertical)
        return imageView
    }()
    
    private lazy var bankCaptionLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.caption
        label.textColor = Palette.ppColorGrayscale400.color
        label.text = BankAccountLocalizable.newBankAccountFormBankCaptionTitle.text
        return label
    }()
    
    private lazy var bankDetailsLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.body
        return label
    }()
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale400.color
        return view
    }()
    
    private lazy var textInformationStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [bankCaptionLabel, bankDetailsLabel])
        stackView.spacing = Spacing.none
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [logoImageView, textInformationStackView])
        stackView.spacing = Spacing.base01
        stackView.alignment = .center
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonSetup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonSetup()
    }
    
    private func commonSetup() {
        isAccessibilityElement = true
        accessibilityLabel = BankAccountLocalizable.newBankAccountFormBankCaptionTitle.text
        buildLayout()
    }
    
    func setup(bank: BankItem) {
        logoImageView.setImage(url: bank.logoURL, placeholder: Assets.Icons.icoBankPlaceholder.image)
        bankDetailsLabel.text = bank.name
        accessibilityValue = bank.name
    }
}

// MARK: View Configuration
extension BankView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(rootStackView)
        addSubview(separatorView)
    }
    
    func setupConstraints() {
        logoImageView.layout {
            $0.width == Layout.Size.logoImageView.width
            $0.height == Layout.Size.logoImageView.height
        }
        
        separatorView.layout {
            $0.trailing == trailingAnchor
            $0.bottom == bottomAnchor
            $0.leading == leadingAnchor
            $0.height == Layout.Lenght.separatorHeight
        }
        
        rootStackView.layout {
            $0.top == layoutMarginsGuide.topAnchor
            $0.trailing == layoutMarginsGuide.trailingAnchor
            $0.bottom == layoutMarginsGuide.bottomAnchor
            $0.leading == layoutMarginsGuide.leadingAnchor
        }
    }
    
    func configureViews() {
        if #available(iOS 11, *) {
            directionalLayoutMargins = Layout.Insets.directionalLayoutMargins
        } else {
            layoutMargins = Layout.Insets.layoutMargins
        }
    }
}
