import UIKit

enum NewBankAccountFormAction {
    case saveCompletion
    case popToBankList
}

protocol NewBankAccountFormCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    var onSaveCompletion: ((_ saved: Bool) -> Void)? { get set }
    var isUpdate: Bool { get set }
    func perform(action: NewBankAccountFormAction)
}

final class NewBankAccountFormCoordinator: NewBankAccountFormCoordinating {
    weak var viewController: UIViewController?
    var onSaveCompletion: ((Bool) -> Void)?
    var isUpdate: Bool = false
    
    func perform(action: NewBankAccountFormAction) {
        switch action {
        case .saveCompletion:
            onSaveCompletion?(true)
        case .popToBankList:
            popToBankList()
        }
    }
    
    private func popToBankList() {
        guard isUpdate else {
            viewController?.navigationController?.popViewController(animated: true)
            return
        }
        let bankListController = BanksFactory.make(onSaveCompletion: onSaveCompletion)
        viewController?.navigationController?.pushViewController(bankListController, animated: true)
    }
}
