import Foundation
import Validator
import CoreLegacy

protocol NewBankAccountFormViewModelInputs: AnyObject {
    func fetchBankAccountInformation()
    func branchDigitTextDidChange(_ branchDigitText: String?)
    func save(bankAccount form: BankAccountFormInput)
    func changeBank()
}

final class NewBankAccountFormViewModel {
    private let service: NewBankAccountFormServicing
    private let presenter: NewBankAccountFormPresenting
    
    private let bankAccount: PPBankAccount
    private var bank: PPBank? {
        bankAccount.bank
    }
    private var formConfiguration: PPBankAccountForm {
        bank?.form ?? PPBankAccountForm()
    }
    
    var invalidDocumentErrorMessage: String?
    var invalidRecipientNameErrorMessage: String?
    
    init(service: NewBankAccountFormServicing, presenter: NewBankAccountFormPresenting, bankAccount: PPBankAccount) {
        self.service = service
        self.presenter = presenter
        self.bankAccount = bankAccount
    }
    
    private func validate(form: BankAccountFormInput) -> ValidationResult {
        var results: [ValidationResult] = []
        
        results.append(validate(field: form.branchNumber, using: formConfiguration.branchFieldConfiguration))
        results.append(validate(field: form.branchDigit, using: formConfiguration.branchDigitFieldConfiguration))
        results.append(validate(field: form.accountNumber, using: formConfiguration.accountFieldConfiguration))
        results.append(validate(field: form.accountDigit, using: formConfiguration.accountDigitFieldConfiguration))
        results.append(validate(field: form.accountType?.rowValue, using: formConfiguration.accountTypeFieldConfiguration))
        
        if form.differentAccountHolder {
            results.append(validate(field: form.accountHolderDocument, using: formConfiguration.documentFieldConfiguration))
            results.append(validate(field: form.accountRecipientName, using: formConfiguration.recipientNameFieldConfiguration))
        }
        
        return results.reduce(into: ValidationResult.valid) { result, validation in
            result = result.merge(with: validation)
        }
    }
    
    private func validate(field: String?, using fieldConfiguration: FormFieldConfiguration) -> ValidationResult {
        guard !fieldConfiguration.isHidden else {
            return .valid
        }
        return Validator.validate(input: field, rules: fieldConfiguration.validationRules)
    }
    
    private func bankAccount(from form: BankAccountFormInput) -> PPBankAccount {
        let bankAccount = PPBankAccount()
        bankAccount.bank = bank
        bankAccount.agencyNumber = form.branchNumber
        bankAccount.agencyDigit = form.branchDigit
        bankAccount.accountNumber = form.accountNumber
        bankAccount.accountDigit = form.accountDigit
        bankAccount.accountType = form.accountType?.rowValue
        bankAccount.cpf = form.accountHolderDocument?.onlyNumbers
        bankAccount.recipientName = form.accountRecipientName
        return bankAccount
    }
    
    private func save(bankAccount: PPBankAccount) {
        presenter.startLoading()
        service.save(bankAccount: bankAccount) { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success:
                self?.presenter.didNextStep(action: .saveCompletion)
            case .failure(let error):
                self?.presenter.present(error: error)
            }
        }
    }
}

extension NewBankAccountFormViewModel: NewBankAccountFormViewModelInputs {
    func fetchBankAccountInformation() {
        presenter.display(bankAccount: bankAccount)
        presenter.configureForm(using: formConfiguration)
        guard 
            let invalidDocumentErrorMessage = invalidDocumentErrorMessage,
            let invalidRecipientNameErrorMessage = invalidRecipientNameErrorMessage 
            else {
                return
        }
        presenter.presentInvalidDocumentNumber(message: invalidDocumentErrorMessage)
        presenter.presentInvalidRecipientName(message: invalidRecipientNameErrorMessage)
    }
    
    func branchDigitTextDidChange(_ branchDigitText: String?) {
        guard
            formConfiguration.type == .bancoDoBrasil,
            let text = branchDigitText,
            text.contains("0")
            else {
                return
        }
        presenter.presentPopTipForBranchDigit(text: BankAccountLocalizable.branchDigitDisclaimer.text)
    }
    
    func save(bankAccount form: BankAccountFormInput) {
        switch validate(form: form) {
        case .valid:
            save(bankAccount: bankAccount(from: form))
        case .invalid(let errors):
            presenter.present(validationErrors: errors)
        }
    }
    
    func changeBank() {
        presenter.didNextStep(action: .popToBankList)
    }
}
