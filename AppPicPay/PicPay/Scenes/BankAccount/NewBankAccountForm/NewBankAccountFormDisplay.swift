import UI
import UIKit
import Validator

protocol NewBankAccountFormDisplay: AnyObject {
    func display(bank: BankItem)
    func populateForm(_ form: BankAccountFormInput)
    func configureForm(using configuration: BankAccountFormConfiguration)
    func configureAccountType(option: [PickerOption])
    func display(error: Error)
    func startLoading()
    func stopLoading()
    func displayPopTipForBranchDigit(text: String)
    func changeTextFieldEnableState(to enabled: Bool)
    func displayBranchNumber(errorMessage: String?)
    func displayBranchDigit(errorMessage: String?)
    func displayAccountNumber(errorMessage: String?)
    func displayAccountDigit(errorMessage: String?)
    func displayAccountType(errorMessage: String?)
    func displayDocument(errorMessage: String?)
    func displayRecipientName(errorMessage: String?)
}
