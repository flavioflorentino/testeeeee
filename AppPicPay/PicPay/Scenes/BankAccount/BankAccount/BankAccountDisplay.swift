import UI

protocol BankAccountDisplay: AnyObject, LoadingViewProtocol {
    func display(bankAccountsSection: Section<String, BankAccountItem>)
    func displayRemovalConfirmationSheet(for bankAccount: BankAccountItem, at index: Int)
    func display(error: Error)
}
