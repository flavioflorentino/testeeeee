struct BankAccountItem: Equatable {
    let bankAccount: String?
    let bankName: String?
    let bankLogoURL: String?
}
