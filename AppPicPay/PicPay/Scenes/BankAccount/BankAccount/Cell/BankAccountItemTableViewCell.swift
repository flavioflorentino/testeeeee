import UI
import UIKit

extension BankAccountItemTableViewCell.Layout {
    enum Font {
        static let title: UIFont = .systemFont(ofSize: 17)
        static let subtitle: UIFont = .systemFont(ofSize: 12)
    }
    
    enum TextColor {
        static let subtitle: UIColor = Palette.ppColorGrayscale400.color
    }
    
    enum Spacing {
        static let textStackViewSpacing: CGFloat = 4
        static let rootStackViewSpacing: CGFloat = 8
    }
    
    enum Size {
        static let logoImageViewSize = CGSize(width: 40, height: 40)
    }
    
    enum Insets {
        static let rootStackViewInsets = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
        @available(iOS 11, *)
        static let rootStackViewDirectionalInsets = NSDirectionalEdgeInsets(top: 8, leading: 16, bottom: 8, trailing: 16)
    }
}

final class BankAccountItemTableViewCell: UITableViewCell, ViewConfiguration {
    fileprivate enum Layout {}
    
    static let identifier = String(describing: BankAccountItemTableViewCell.self)
    
    // MARK: - Layout components
    
    private lazy var bankAccountLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.title
        return label
    }()
    
    private lazy var bankNameLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.subtitle
        label.textColor = Layout.TextColor.subtitle
        return label
    }()
    
    private lazy var bankLogoImageView: UIImageView = {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: Layout.Size.logoImageViewSize))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var textStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [self.bankAccountLabel, self.bankNameLabel])
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.spacing = Layout.Spacing.textStackViewSpacing
        stackView.setContentHuggingPriority(.defaultLow, for: .horizontal)
        return stackView
    }()
    
    private lazy var rootStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [self.bankLogoImageView, self.textStackView])
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.spacing = Layout.Spacing.rootStackViewSpacing
        return stackView
    }()
    
    // MARK: - Life cycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildLayout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        buildLayout()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        bankLogoImageView.cancelRequest()
    }
    
    // MARK: - Setup
    
    func setup(for bankAccount: BankAccountItem) {
        bankAccountLabel.text = bankAccount.bankAccount
        bankNameLabel.text = bankAccount.bankName
        if let logoURL = bankAccount.bankLogoURL {
            bankLogoImageView.setImage(url: URL(string: logoURL), placeholder: Assets.Icons.icoBankPlaceholder.image)
        } else {
            bankLogoImageView.image = Assets.Icons.icoBankPlaceholder.image
        }
    }
    
    // MARK: - ViewConfiguration
    
    func buildViewHierarchy() {
        addSubview(rootStackView)
    }
    
    func setupConstraints() {
        rootStackView.layout {
            $0.top == layoutMarginsGuide.topAnchor
            $0.trailing == layoutMarginsGuide.trailingAnchor
            $0.bottom == layoutMarginsGuide.bottomAnchor
            $0.leading == layoutMarginsGuide.leadingAnchor
        }
        
        bankLogoImageView.layout {
            $0.height == Layout.Size.logoImageViewSize.height
            $0.width == Layout.Size.logoImageViewSize.width
        }
    }
    
    func configureViews() {
        preservesSuperviewLayoutMargins = false
        if #available(iOS 11.0, *) {
            directionalLayoutMargins = Layout.Insets.rootStackViewDirectionalInsets
        } else {
            layoutMargins = Layout.Insets.rootStackViewInsets
        }
    }
}
