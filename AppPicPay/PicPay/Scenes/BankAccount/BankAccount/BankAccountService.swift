import Core

protocol BankAccountServicing {
    func remove(bankAccount: PPBankAccount, completion: ((Result<Void, Error>) -> Void)?)
}

final class BankAccountService: BankAccountServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func remove(bankAccount: PPBankAccount, completion: ((Result<Void, Error>) -> Void)?) {
        WSConsumer.delete(bankAccount) { [weak self] error in
            self?.dependencies.mainQueue.async {
                guard let error = error else {
                    completion?(.success)
                    return
                }
                completion?(.failure(error))
            }
        }
    }
}
