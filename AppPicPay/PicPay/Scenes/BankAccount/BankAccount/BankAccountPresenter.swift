import UI

protocol BankAccountPresenting: AnyObject {
    var viewController: BankAccountDisplay? { get set }
    func display(bankAccounts: [PPBankAccount])
    func displayRemovalConfirmationSheet(for bankAccount: PPBankAccount, at index: Int)
    func display(error: Error)
    func didNextStep(action: BankAccountAction)
    func startLoading()
    func stopLoading()
}

final class BankAccountPresenter: BankAccountPresenting {
    private let coordinator: BankAccountCoordinating
    weak var viewController: BankAccountDisplay?

    init(coordinator: BankAccountCoordinating) {
        self.coordinator = coordinator
    }
    
    func display(bankAccounts: [PPBankAccount]) {
        let bankAccountItems = bankAccounts.map(bankAccountItem(for:))
        let bankAccountSection = Section<String, BankAccountItem>(header: BankAccountLocalizable.bankMyAccount.text, items: bankAccountItems)
        viewController?.display(bankAccountsSection: bankAccountSection)
    }
    
    func displayRemovalConfirmationSheet(for bankAccount: PPBankAccount, at index: Int) {
        viewController?.displayRemovalConfirmationSheet(for: bankAccountItem(for: bankAccount), at: index)
    }
    
    func display(error: Error) {
        viewController?.display(error: error)
    }
    
    func didNextStep(action: BankAccountAction) {
        coordinator.perform(action: action)
    }
    
    func startLoading() {
        viewController?.startLoadingView()
    }
    
    func stopLoading() {
        viewController?.stopLoadingView()
    }
    
    private func bankAccountItem(for bankAccount: PPBankAccount) -> BankAccountItem {
        let agencyNumber = bankAccount.agencyNumber ?? ""
        let agencyDigit = bankAccount.agencyDigit ?? ""
        let accountNumber = bankAccount.accountNumber ?? ""
        let accountDigit = bankAccount.accountDigit ?? ""
        
        let bankAccountDescriptionFormat = BankAccountLocalizable.bankCellText.text
        let formattedBankAccount = String(format: bankAccountDescriptionFormat, agencyNumber, agencyDigit, accountNumber, accountDigit)
        
        return BankAccountItem(
            bankAccount: formattedBankAccount,
            bankName: bankAccount.bank?.name,
            bankLogoURL: bankAccount.bank?.imageUrl
        )
    }
}
