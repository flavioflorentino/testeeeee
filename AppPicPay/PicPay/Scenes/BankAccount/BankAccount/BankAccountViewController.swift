import UI
import UIKit
import SecurityModule

extension BankAccountViewController.Layout {
    enum Height {
        static let changeBankAccountButtonHeight: CGFloat = 44
        static let tableFooterView: CGFloat = changeBankAccountButtonHeight
    }
    
    enum Insets {
        static let tableFooterViewInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        @available(iOS 11, *)
        static let tableFooterViewDirectionalInsets = NSDirectionalEdgeInsets(top: 0, leading: 16, bottom: 0, trailing: 16)
    }
}

final class BankAccountViewController: ViewController<BankAccountViewModelInputs, UIView>, Obfuscable {
    fileprivate enum Layout {}
    
    enum AccessibilityIdentifier: String {
        case removeBarButtonItem
        case changeBankButton
        case bankAccountsTableView
    }
    
    // MARK: - Layout components
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.allowsSelection = false
        tableView.register(BankAccountItemTableViewCell.self, forCellReuseIdentifier: BankAccountItemTableViewCell.identifier)
        tableView.accessibilityIdentifier = AccessibilityIdentifier.bankAccountsTableView.rawValue
        tableView.estimatedRowHeight = 60.0
        tableView.rowHeight = UITableView.automaticDimension
        return tableView
    }()
    
    private lazy var removeButton: UIBarButtonItem = {
        let barButtonItem = UIBarButtonItem(
            title: DefaultLocalizable.remove.text,
            style: .plain,
            target: self,
            action: #selector(removeBankAccountButtonTapped)
        )
        barButtonItem.accessibilityIdentifier = AccessibilityIdentifier.removeBarButtonItem.rawValue
        return barButtonItem
    }()
    
    private lazy var changeBankAccountButton: UIPPButton = {
        let button = UIPPButton(
            title: BankAccountLocalizable.changeBankAccountButtonTitle.text,
            target: self,
            action: #selector(changeBankAccountButtonTapped)
        )
        button.accessibilityIdentifier = AccessibilityIdentifier.changeBankButton.rawValue
        return button
    }()
    
    private lazy var tableFooterView: UIView = {
        let size = CGSize(width: UIScreen.main.bounds.width, height: Layout.Height.tableFooterView)
        let frame = CGRect(origin: .zero, size: size)
        let view = UIView(frame: frame)
        if #available(iOS 11, *) {
            view.directionalLayoutMargins = Layout.Insets.tableFooterViewDirectionalInsets
        } else {
            view.layoutMargins = Layout.Insets.tableFooterViewInsets
        }
        return view
    }()
    
    private(set) lazy var loadingView = LoadingView()
    
    // MARK: Instance variables
    
    private(set) lazy var bankAccounts: [BankAccountItem] = []
    private(set) var tableViewHandler: TableViewHandler<String, BankAccountItem, BankAccountItemTableViewCell>?
    
    // Screen obfuscation
    var appSwitcherObfuscator: AppSwitcherObfuscating?
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.fetchBankAccount()
        navigationItem.rightBarButtonItem = removeButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupObfuscationConfiguration()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        deinitObfuscator()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(tableView)
        tableFooterView.addSubview(changeBankAccountButton)
        tableView.tableFooterView = tableFooterView
    }
    
    override func setupConstraints() {
        tableView.layout {
            $0.top == view.topAnchor
            $0.trailing == view.trailingAnchor
            $0.bottom == view.bottomAnchor
            $0.leading == view.leadingAnchor
        }
        
        changeBankAccountButton.layout {
            $0.height == Layout.Height.changeBankAccountButtonHeight
            $0.centerY == tableFooterView.centerYAnchor
            $0.leading == tableFooterView.layoutMarginsGuide.leadingAnchor
            $0.trailing == tableFooterView.layoutMarginsGuide.trailingAnchor
        }
    }
    
    override func configureViews() {
        title = BankAccountLocalizable.bankAccountViewTitle.text
    }
    
    // MARK: - Actions
    
    @objc
    func removeBankAccountButtonTapped() {
        viewModel.requestRemovalConfirmation(forBankAccountAt: 0)
    }
    
    @objc
    func changeBankAccountButtonTapped() {
        viewModel.changeBankAccount()
    }
}

// MARK: View Model Outputs
extension BankAccountViewController: BankAccountDisplay {
    func display(bankAccountsSection: Section<String, BankAccountItem>) {
        tableViewHandler = TableViewHandler<String, BankAccountItem, BankAccountItemTableViewCell>(
            data: [bankAccountsSection],
            cellType: BankAccountItemTableViewCell.self,
            configureCell: { _, bankAccountItem, cell in
                cell.setup(for: bankAccountItem)
            },
            configureSection: { _, section -> Header? in
                guard let sectionTitle = section.header else {
                    return nil
                }
                return .view(BankAccountHeaderView(text: sectionTitle))
            },
            configureTrailingSwipeActionHandler: { indexPath, _ -> SwipeActionsConfiguration? in
                let removeAction = ContextualAction(style: .destructive, title: DefaultLocalizable.remove.text) { _, completion in
                    self.viewModel.requestRemovalConfirmation(forBankAccountAt: indexPath.row)
                    completion(true)
                }
                return SwipeActionsConfiguration(actions: [removeAction])
            }
        )
        tableView.dataSource = tableViewHandler
        tableView.delegate = tableViewHandler
        tableView.reloadData()
    }
    
    func display(bankAccounts: [BankAccountItem]) {
        self.bankAccounts = bankAccounts
        tableView.reloadData()
    }
    
    func displayRemovalConfirmationSheet(for bankAccount: BankAccountItem, at index: Int) {
        let actionSheet = UIAlertController(
            title: BankAccountLocalizable.bankRemoveTitle.text,
            message: BankAccountLocalizable.bankRemoveDescription.text,
            preferredStyle: .actionSheet
        )
        
        let removeBankAccountAction = UIAlertAction(
            title: BankAccountLocalizable.bankRemoveAccount.text,
            style: .destructive,
            handler: { [weak self] _ in
                self?.viewModel.removeBankAccount(at: index)
            })
        
        let cancelAction = UIAlertAction(
            title: DefaultLocalizable.btCancel.text,
            style: .cancel,
            handler: nil
        )
        
        actionSheet.addAction(removeBankAccountAction)
        actionSheet.addAction(cancelAction)
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    func display(error: Error) {
        AlertMessage.showCustomAlertWithError(error, controller: self)
    }
}
