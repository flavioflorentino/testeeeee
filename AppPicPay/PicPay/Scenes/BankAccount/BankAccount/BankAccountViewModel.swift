protocol BankAccountViewModelInputs: AnyObject {
    func fetchBankAccount()
    func requestRemovalConfirmation(forBankAccountAt index: Int)
    func removeBankAccount(at index: Int)
    func changeBankAccount()
}

final class BankAccountViewModel {
    private let service: BankAccountServicing
    private let presenter: BankAccountPresenting
    private var bankAccounts: [PPBankAccount]

    init(service: BankAccountServicing, presenter: BankAccountPresenting, bankAccounts: [PPBankAccount]) {
        self.service = service
        self.presenter = presenter
        self.bankAccounts = bankAccounts
    }
}

extension BankAccountViewModel: BankAccountViewModelInputs {
    func fetchBankAccount() {
        presenter.display(bankAccounts: bankAccounts)
    }
    
    func requestRemovalConfirmation(forBankAccountAt index: Int) {
        presenter.displayRemovalConfirmationSheet(for: bankAccounts[index], at: index)
    }
    
    func removeBankAccount(at index: Int) {
        presenter.startLoading()
        service.remove(bankAccount: bankAccounts[index]) { [weak self] result in
            self?.presenter.stopLoading()
            switch result {
            case .success:
                self?.presenter.didNextStep(action: .registerBankAccountSplash)
            case .failure(let error):
                self?.presenter.display(error: error)
            }
        }
    }
    
    func changeBankAccount() {
        presenter.didNextStep(action: .changeBankAccount(completion: { [weak self] bankAccounts in
            self?.bankAccounts = bankAccounts
            self?.presenter.display(bankAccounts: bankAccounts)
        }))
    }
}
