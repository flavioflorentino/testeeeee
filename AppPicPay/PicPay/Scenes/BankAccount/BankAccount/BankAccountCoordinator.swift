import UIKit

enum BankAccountAction {
    case registerBankAccountSplash
    case changeBankAccount(completion: (([PPBankAccount]) -> Void)?)
}

protocol BankAccountCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: BankAccountAction)
}

final class BankAccountCoordinator: BankAccountCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: BankAccountAction) {
        guard let navigationController = viewController?.navigationController else {
            return
        }
        switch action {
        case .registerBankAccountSplash:
            let registerBankAccountSplash = RegisterBankAccountSplashFactory.make(hidesBottomBarWhenPushed: true)
            navigationController.viewControllers.insert(registerBankAccountSplash, at: 1)
            navigationController.popToViewController(registerBankAccountSplash, animated: true)
        case .changeBankAccount(let completion):
            performChangeBankAccount(navigationController, completion)
        }
    }
    
    private func performChangeBankAccount(_ navigationController: UINavigationController, _ completion: (([PPBankAccount]) -> Void)?) {
        let controller = BanksFactory.make(onSaveCompletion: { [weak self] _ in
            guard
                let viewController = self?.viewController,
                let bankAccount = ConsumerManager.shared.consumer?.bankAccount else {
                    return
            }
            viewController.hidesBottomBarWhenPushed = true
            viewController.navigationController?.popToViewController(viewController, animated: true)
            completion?([bankAccount])
        })
        navigationController.pushViewController(controller, animated: true)
    }
}
