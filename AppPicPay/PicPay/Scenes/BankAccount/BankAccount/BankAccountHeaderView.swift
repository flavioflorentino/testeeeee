import UIKit
import UI

extension BankAccountHeaderView.Layout {
    enum Font {
        static let header: UIFont = .systemFont(ofSize: 13)
    }
    
    enum Insets {
        static let headerLayoutMargins = UIEdgeInsets(top: 0, left: 16, bottom: 8, right: 16)
        @available(iOS 11, *)
        static let headerDirectionalLayoutMargins = NSDirectionalEdgeInsets(top: 0, leading: 16, bottom: 8, trailing: 16)
    }
}

final class BankAccountHeaderView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    
    lazy var textLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Font.header
        return label
    }()
    
    convenience init(text: String) {
        self.init(frame: .zero)
        textLabel.text = text
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildLayout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        buildLayout()
    }
    
    func buildViewHierarchy() {
        addSubview(textLabel)
    }
    
    func setupConstraints() {
        textLabel.layout {
            $0.top == layoutMarginsGuide.topAnchor
            $0.trailing == layoutMarginsGuide.trailingAnchor
            $0.bottom == layoutMarginsGuide.bottomAnchor
            $0.leading == layoutMarginsGuide.leadingAnchor
        }
    }
    
    func configureViews() {
        if #available(iOS 11, *) {
            directionalLayoutMargins = Layout.Insets.headerDirectionalLayoutMargins
        } else {
            layoutMargins = Layout.Insets.headerLayoutMargins
        }
    }
}
