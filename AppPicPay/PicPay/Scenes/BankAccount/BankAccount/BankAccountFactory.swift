import UIKit

enum BankAccountFactory {
    static func make(with bankAccounts: [PPBankAccount], hidesBottomBarWhenPushed: Bool) -> UIViewController? {
        let container = DependencyContainer()
        let service: BankAccountServicing = BankAccountService(dependencies: container)
        let coordinator: BankAccountCoordinating = BankAccountCoordinator()
        let presenter: BankAccountPresenting = BankAccountPresenter(coordinator: coordinator)
        let viewModel = BankAccountViewModel(service: service, presenter: presenter, bankAccounts: bankAccounts)
        let viewController = BankAccountViewController(viewModel: viewModel)

        viewController.hidesBottomBarWhenPushed = hidesBottomBarWhenPushed
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
