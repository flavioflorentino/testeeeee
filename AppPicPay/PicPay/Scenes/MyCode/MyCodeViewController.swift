import AnalyticsModule
import AssetsKit
import UI
import UIKit

/// A protocol that allows the animation of external elements of MyCodeView,
/// implementing the method animate(scale:) that receives a scale value representing
/// the progress of the animation between 0 and 1.
protocol MyCodeViewControllerAnimationDelegate: AnyObject {
    func animate(scale: CGFloat)
}

final class MyCodeViewController: LegacyViewController<MyCodeViewModelType, MyCodeCoordinating, UIView> {
    private enum Position {
        case top
        case bottom
    }
    enum Layout {
        static let screenBounds = UIScreen.main.bounds
        static let buttonHeight: CGFloat = 48
        static let margin: CGFloat = 26
        static let qrCodeSize: CGFloat = screenBounds.width * (3 / 5)
        static let extraSpace: CGFloat = screenBounds.height < 570 ? 0 : 10
    }
    
    private lazy var pullBarView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale400.color
        view.layer.cornerRadius = 2
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = Palette.ppColorGrayscale600.color
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var qrCodeView: QrCodeView = {
        let qrCodeSize = CGSize(width: Layout.qrCodeSize, height: Layout.qrCodeSize)
        let view = QrCodeFactory.make(size: qrCodeSize, delegate: self)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var loadingView = LoadingView()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = Palette.ppColorGrayscale600.color
        label.numberOfLines = 0
        label.text = MyCodeLocalizable.message.text
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var chargeUserButton: UIPPButton = {
        let button = UIPPButton()
        button.configure(with: Button(title: MyCodeLocalizable.chargeButtonTitle.text, type: .cta))
        button.cornerRadius = Layout.buttonHeight / 2
        button.addTarget(self, action: #selector(tapChargeUser), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var shareCodeButton: UIPPButton = {
        let button = UIPPButton()
        button.configure(with: Button(title: MyCodeLocalizable.shareCodeButtonTitle.text, type: .cleanUnderlined))
        button.addTarget(self, action: #selector(tapShareCode), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var buttonsStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 8
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private lazy var tapView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapTopArea))
        view.addGestureRecognizer(tap)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    weak var delegate: MyCodeViewControllerAnimationDelegate?
    
    private var distanceThingerToTop: CGFloat = 0
    private var minOffsetY: CGFloat = 0
    private var maxOffsetY: CGFloat = 0
    private var midY: CGFloat {
        (maxOffsetY + minOffsetY) / 2
    }
    private var amplitude: CGFloat {
        maxOffsetY - minOffsetY
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.inputs.viewDidLoad()
    }
 
    override func buildViewHierarchy() {
        view.addSubview(pullBarView)
        view.addSubview(usernameLabel)
        view.addSubview(qrCodeView)
        view.addSubview(messageLabel)
        view.addSubview(loadingView)
        buttonsStackView.addArrangedSubview(chargeUserButton)
        buttonsStackView.addArrangedSubview(shareCodeButton)
        view.addSubview(buttonsStackView)
        view.addSubview(tapView)
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [usernameLabel, messageLabel, buttonsStackView], constant: Layout.margin)
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [tapView])
        
        NSLayoutConstraint.activate([
            pullBarView.topAnchor.constraint(equalTo: view.topAnchor, constant: 16),
            pullBarView.widthAnchor.constraint(equalToConstant: 50),
            pullBarView.heightAnchor.constraint(equalToConstant: 4),
            pullBarView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            usernameLabel.topAnchor.constraint(equalTo: pullBarView.bottomAnchor, constant: 26 + Layout.extraSpace),
            
            qrCodeView.topAnchor.constraint(equalTo: usernameLabel.bottomAnchor, constant: 22 + Layout.extraSpace),
            qrCodeView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            qrCodeView.widthAnchor.constraint(equalToConstant: Layout.qrCodeSize),
            qrCodeView.heightAnchor.constraint(equalToConstant: Layout.qrCodeSize),
            
            messageLabel.topAnchor.constraint(equalTo: qrCodeView.bottomAnchor, constant: 22 + Layout.extraSpace),
            
            buttonsStackView.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 26 + Layout.extraSpace),
            
            chargeUserButton.heightAnchor.constraint(equalToConstant: Layout.buttonHeight),
            shareCodeButton.heightAnchor.constraint(equalToConstant: Layout.buttonHeight),
            
            tapView.topAnchor.constraint(equalTo: view.topAnchor),
            tapView.heightAnchor.constraint(equalToConstant: Layout.screenBounds.height - maxOffsetY)
        ])
        
        loadingView.snp.makeConstraints {
            $0.centerX.centerY.equalTo(qrCodeView)
        }
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color(withCustomDark: .ppColorGrayscale500)
        qrCodeView.generateQrCode()
    }
    
    func setupSheetPresentation(minOffsetY: CGFloat, maxOffsetY: CGFloat) {
        self.minOffsetY = minOffsetY
        self.maxOffsetY = maxOffsetY
        
        let initialPoint = CGPoint(x: 0, y: maxOffsetY)
        let size = CGSize(width: Layout.screenBounds.width, height: Layout.screenBounds.height)
        view.frame = CGRect(origin: initialPoint, size: size)
        
        view.layer.cornerRadius = 16
        
        view.frame.origin.y = maxOffsetY
                
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerHandler(_:)))
        view.addGestureRecognizer(panGesture)
    }
    
    @objc
    private func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        view.endEditing(true)
        let touchPoint = sender.location(in: view?.window)
        
        switch sender.state {
        case .began:
            gestureRecognizerStateBegan(touchPoint: touchPoint)
        case .changed:
            gestureRecognizerStateChanged(touchPoint: touchPoint)
        case .ended, .cancelled:
            gestureRecognizerStateEnded(touchPoint: touchPoint, velocity: sender.velocity(in: view.window).y)
        default:
            break
        }
    }
    
    private func gestureRecognizerStateBegan(touchPoint: CGPoint) {
        distanceThingerToTop = touchPoint.y - view.frame.origin.y
    }
    
    private func gestureRecognizerStateChanged(touchPoint: CGPoint) {
        let y = touchPoint.y - distanceThingerToTop
        view.frame.origin.y = limitedValue(y, toRange: 0.0...Layout.screenBounds.height)
        
        let scale: CGFloat = (maxOffsetY - y) / amplitude
        delegate?.animate(scale: limitedValue(scale, toRange: 0.0...1.0))
    }
    
    private func limitedValue(_ value: CGFloat, toRange range: ClosedRange<CGFloat>) -> CGFloat {
        max(range.lowerBound, min(range.upperBound, value))
    }
    
    private func gestureRecognizerStateEnded(touchPoint: CGPoint, velocity: CGFloat) {
        let y = touchPoint.y - distanceThingerToTop
        let tendency = y + velocity * 0.1
        let finalPosition: Position = tendency < midY ? .top : .bottom
        animateView(to: finalPosition)
    }
    
    private func animateView(to finalPosition: Position) {
        if finalPosition == .top {
            Analytics.shared.log(QRCodeEvent.pullUpMyCode)
        }
        
        let finalPositionY: CGFloat = finalPosition == .top ? minOffsetY : maxOffsetY
        let scale: CGFloat = finalPosition == .top ? 1.0 : 0.0
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1, options: .allowUserInteraction, animations: {
            self.view.frame.origin.y = finalPositionY
            self.delegate?.animate(scale: scale)
        })
    }
    
    @objc
    private func tapChargeUser() {
        viewModel.inputs.didTapChargeUser()
    }
    
    @objc
    private func tapShareCode() {
        let codeImage = qrCodeView.qrCodeImage
        let userImage = qrCodeView.consumerImage
        viewModel.inputs.didTapShareCode(withQrCode: codeImage, userImage: userImage)
    }
    
    @objc
    private func tapTopArea() {
        switch view.frame.origin.y {
        case minOffsetY:
            minimizeView()
        case maxOffsetY:
            maximizeView()
        default:
            return
        }
    }
    
    func minimizeView() {
        animateView(to: .bottom)
    }
    
    func maximizeView() {
        animateView(to: .top)
    }
    
    func configureQRCode() {
        viewModel.inputs.viewDidLoad()
    }
}

// MARK: View Model Outputs
extension MyCodeViewController: MyCodeViewModelOutputs {
    func setUpUsernameLabel(username: String) {
        usernameLabel.text = username
    }
    
    func perform(action: MyCodeAction) {
        coordinator.perform(action: action)
    }
    
    func showShareActionSheet(imageActivity: UIActivityViewController, linkActivity: UIActivityViewController) {
        let alert = UIAlertController(title: DefaultLocalizable.btShare.text, message: nil, preferredStyle: .actionSheet)
        
        let shareImage = UIAlertAction(title: ScannerLocalizable.shareImage.text, style: .default) { [weak self] _ in
            Analytics.shared.log(QRCodeEvent.didShareCode(type: .image))
            self?.present(imageActivity, animated: true)
        }
        let shareLink = UIAlertAction(title: ScannerLocalizable.shareLink.text, style: .default) { [weak self] _ in
            Analytics.shared.log(QRCodeEvent.didShareCode(type: .link))
            self?.present(linkActivity, animated: true)
        }
        let cancel = UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel)
        
        alert.addAction(shareImage)
        alert.addAction(shareLink)
        alert.addAction(cancel)
        present(alert, animated: true)
    }
    
    func displayQrCodeSuccess() {
        loadingView.isHidden = true
        qrCodeView.isHidden = false
        messageLabel.isHidden = false
        shareCodeButton.isHidden = false
    }
    
    func displayQrCodeError() {
        loadingView.isHidden = true
        qrCodeView.isHidden = false
        let buttonText = NSAttributedString(
            string: DefaultLocalizable.tryAgain.text,
            attributes: [
                .font: Typography.bodyPrimary().font(),
                .foregroundColor: Colors.success600.color,
                .underlineStyle: NSUnderlineStyle.single.rawValue
            ]
        )
        let errorView = MyCodeErrorView(
            icon: Resources.Illustrations.iluPersonError.image,
            title: MyCodeLocalizable.communicationFailureTitle.text,
            description: MyCodeLocalizable.communicationFailureMessage.text,
            buttonText: buttonText
        )
        errorView.linkButtonCompletion = { [weak self] in
            self?.qrCodeView.generateQrCode()
        }
        view.addSubview(errorView)
        errorView.snp.makeConstraints {
            $0.top.equalTo(qrCodeView)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base03)
            $0.bottom.lessThanOrEqualTo(buttonsStackView.snp.top).offset(Spacing.base04)
        }
    }
}

// MARK: - QrCodeDelegate
extension MyCodeViewController: QrCodeDelegate {
    func startRequest() {
        loadingView.isHidden = false
        qrCodeView.isHidden = true
        messageLabel.isHidden = true
        shareCodeButton.isHidden = true
    }
    
    func finishRequest(isSuccess: Bool) {
        viewModel.inputs.handleQrCodeResponse(isSuccess: isSuccess)
    }
}
