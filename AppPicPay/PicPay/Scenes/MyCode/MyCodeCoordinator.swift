import FeatureFlag
import Foundation

enum MyCodeAction {
    case chargeUser
}

protocol MyCodeCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: MyCodeAction)
}

final class MyCodeCoordinator: MyCodeCoordinating {
    typealias Dependencies = HasFeatureManager
    
    weak var viewController: UIViewController?
    
    func perform(action: MyCodeAction) {
        switch action {
        case .chargeUser:
            viewController?.present(PaymentRequestSelectorFactory().make(origin: .myQrCode), animated: true)
        }
    }
}
