protocol MyCodeServiceProtocol {
    var username: String? { get }
    var consumerImageUrl: String? { get }
}

final class MyCodeService: MyCodeServiceProtocol {
    typealias Dependencies = HasConsumerManager
    private let dependencies: Dependencies
    
    var username: String? {
        dependencies.consumerManager.consumer?.username
    }
    
    var consumerImageUrl: String? {
        dependencies.consumerManager.consumer?.img_url
    }
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}
