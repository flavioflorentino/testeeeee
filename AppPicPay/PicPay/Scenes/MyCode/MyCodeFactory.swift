enum MyCodeFactory {
    static func make() -> MyCodeViewController {
        let container = DependencyContainer()
        let service: MyCodeServiceProtocol = MyCodeService(dependencies: container)
        let viewModel: MyCodeViewModelType = MyCodeViewModel(service: service)
        var coordinator: MyCodeCoordinating = MyCodeCoordinator()
        let viewController = MyCodeViewController(viewModel: viewModel, coordinator: coordinator)
        
        coordinator.viewController = viewController
        viewModel.outputs = viewController

        return viewController
    }
}
