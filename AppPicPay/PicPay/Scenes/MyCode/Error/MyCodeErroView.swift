import UI
import UIKit

private enum Layout {
    static let iconSize = CGSize(width: 106, height: 106)
}

final class MyCodeErrorView: UIView {
    // MARK: - Visual Components
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.title(.large).font()
        label.textColor = Colors.black.color
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = Typography.bodySecondary().font()
        label.textColor = Colors.grayscale700.color
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var linkButton: UIButton = {
        let button = UIButton()
        button.buttonStyle(LinkButtonStyle())
        button.addTarget(self, action: #selector(didTapButtonLink), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Variables
    var linkButtonCompletion: (() -> Void)?
    
    // MARK: - Life Cycle
    init(icon: UIImage?, title: String, description: String, buttonText: NSAttributedString) {
        super.init(frame: .zero)
        buildLayout()
        iconImageView.image = icon
        titleLabel.text = title
        descriptionLabel.text = description
        linkButton.setAttributedTitle(buttonText, for: .normal)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func buildLayout() {
        buildViewHierarchy()
        setupConstraints()
    }

    private func buildViewHierarchy() {
        addSubview(iconImageView)
        addSubview(titleLabel)
        addSubview(descriptionLabel)
        addSubview(linkButton)
    }

    private func setupConstraints() {
        iconImageView.snp.makeConstraints {
            $0.top.centerX.equalToSuperview()
            $0.size.equalTo(Layout.iconSize)
        }
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(iconImageView.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.equalToSuperview()
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.trailing.equalToSuperview()
        }
        
        linkButton.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    // MARK: - Action
    @objc
    private func didTapButtonLink() {
        removeFromSuperview()
        linkButtonCompletion?()
    }
}
