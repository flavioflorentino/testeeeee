enum MyCodeLocalizable: String, Localizable {
    case message
    case chargeButtonTitle
    case shareCodeButtonTitle
    case communicationFailureTitle
    case communicationFailureMessage
    
    var key: String {
        rawValue
    }
    
    var file: LocalizableFile {
        .myCode
    }
}
