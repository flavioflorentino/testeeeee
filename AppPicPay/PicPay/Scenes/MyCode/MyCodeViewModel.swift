import AnalyticsModule

protocol MyCodeViewModelInputs {
    func viewDidLoad()
    func didTapChargeUser()
    func didTapShareCode(withQrCode qrCode: UIImage?, userImage: UIImage?)
    func handleQrCodeResponse(isSuccess: Bool)
}

protocol MyCodeViewModelOutputs: AnyObject {
    func setUpUsernameLabel(username: String)
    func showShareActionSheet(imageActivity: UIActivityViewController, linkActivity: UIActivityViewController)
    func perform(action: MyCodeAction)
    func displayQrCodeSuccess()
    func displayQrCodeError()
}

protocol MyCodeViewModelType: AnyObject {
    var inputs: MyCodeViewModelInputs { get }
    var outputs: MyCodeViewModelOutputs? { get set }
}

final class MyCodeViewModel: MyCodeViewModelType, MyCodeViewModelInputs {
    var inputs: MyCodeViewModelInputs { self }
    weak var outputs: MyCodeViewModelOutputs?

    private let service: MyCodeServiceProtocol
    
    init(service: MyCodeServiceProtocol) {
        self.service = service
    }

    func viewDidLoad() {
        guard let username = service.username else {
            return
        }
        outputs?.setUpUsernameLabel(username: "@" + username)
    }
    
    func didTapChargeUser() {
        outputs?.perform(action: .chargeUser)
    }
    
    func didTapShareCode(withQrCode qrCode: UIImage?, userImage: UIImage?) {
        Analytics.shared.log(QRCodeEvent.tapShareCode)
        
        guard let qrCode = qrCode,
            let shareImageActivity = activityToShareImage(withQrCode: qrCode, userImage: userImage),
            let shareLinkActivity = activityToShareLink() else {
            return
        }
        outputs?.showShareActionSheet(imageActivity: shareImageActivity, linkActivity: shareLinkActivity)
    }
    
    private func activityToShareImage(withQrCode qrCode: UIImage, userImage: UIImage?) -> UIActivityViewController? {
        guard let username = service.username,
            let views = Bundle.main.loadNibNamed(String(describing: ShareCodeView.self), owner: nil, options: nil),
            let shareCodeView = views.first as? ShareCodeView else {
            return nil
        }
        shareCodeView.setup(username: username, qrCodeImage: qrCode, userImage: userImage)
        
        guard let snapshot = shareCodeView.snapshot else {
            return nil
        }
        return UIActivityViewController(activityItems: [snapshot], applicationActivities: nil)
    }
    
    private func activityToShareLink() -> UIActivityViewController? {
        guard let username = service.username else {
            return nil
        }
        let stringItem = String(format: PaymentRequestLocalizable.shareLinkMessageNoValue.text, username)
        return UIActivityViewController(activityItems: [stringItem], applicationActivities: nil)
    }
    
    func handleQrCodeResponse(isSuccess: Bool) {
        guard isSuccess else {
            outputs?.displayQrCodeError()
            return
        }
        outputs?.displayQrCodeSuccess()
    }
}
