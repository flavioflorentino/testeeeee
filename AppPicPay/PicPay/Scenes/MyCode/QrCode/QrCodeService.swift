import Core
import Foundation

protocol QrCodeServicing {
    var consumerImageUrl: String? { get }
    func generateQrCode(amount: String?, completion: @escaping (Result<QrCodeResponse, ApiError>) -> Void)
}

final class QrCodeService {
    typealias Dependencies = HasMainQueue & HasConsumerManager
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
}

// MARK: - QrCodeServicing
extension QrCodeService: QrCodeServicing {
    var consumerImageUrl: String? {
        dependencies.consumerManager.consumer?.img_url
    }
    
    func generateQrCode(amount: String?, completion: @escaping (Result<QrCodeResponse, ApiError>) -> Void) {
        let endpoint = QrCodeEndpoint.brCodeGenerate(amount: amount)
        Api<QrCodeResponse>(endpoint: endpoint).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map(\.model)
                completion(mappedResult)
            }
        }
    }
}
