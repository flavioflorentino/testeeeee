import Foundation

protocol QrCodeInteracting {
    func generateQrCode(amount: Double?)
}

protocol QrCodeDelegate: AnyObject {
    func startRequest()
    func finishRequest(isSuccess: Bool)
}

final class QrCodeInteractor {
    private let presenter: QrCodePresenting
    private let service: QrCodeServicing
    private weak var delegate: QrCodeDelegate?
    let size: CGSize
    
    init(presenter: QrCodePresenting, service: QrCodeServicing, size: CGSize, delegate: QrCodeDelegate?) {
        self.presenter = presenter
        self.service = service
        self.size = size
        self.delegate = delegate
    }
}

// MARK: - QrCodeInteracting
extension QrCodeInteractor: QrCodeInteracting {
    func generateQrCode(amount: Double?) {
        var amountString: String?
        if let amount = amount {
            amountString = String(format: "%.2f", amount)
        }
        delegate?.startRequest()
        service.generateQrCode(amount: amountString) { [weak self] result in
            switch result {
            case let .success(response):
                self?.handleQrCodeText(response.text)
                self?.delegate?.finishRequest(isSuccess: true)
            case .failure:
                self?.delegate?.finishRequest(isSuccess: false)
            }
        }
    }
    
    private func handleQrCodeText(_ text: String) {
        guard let image = createQrCodeImage(forString: text) else {
            return
        }
        loadConsumerImage()
        presenter.presentQrCodeImage(image)
    }
    
    private func loadConsumerImage() {
        guard let urlString = service.consumerImageUrl, let url = URL(string: urlString) else {
            return
        }
        presenter.presentConsumerImage(url: url)
    }
    
    private func createQrCodeImage(forString stringData: String) -> UIImage? {
        guard let qrCodeImage = generateQrCode(withData: stringData, size: size) else {
            return nil
        }
        let inset = floor(size.width / qrCodeImage.extent.size.width)
        presenter.presentLogoImageViewInsets(inset)
        
        let transform = CGAffineTransform(scaleX: inset, y: inset)
        let resizedImage = qrCodeImage.transformed(by: transform)
        return UIImage(ciImage: resizedImage)
    }
    
    private func generateQrCode(withData qrCodeData: String, size: CGSize) -> CIImage? {
        let data = qrCodeData.data(using: String.Encoding.ascii)
        guard let filter = CIFilter(name: "CIQRCodeGenerator") else {
            return nil
        }
        filter.setValue(data, forKey: "inputMessage")
        filter.setValue("H", forKey: "inputCorrectionLevel")
        return filter.outputImage
    }
}
