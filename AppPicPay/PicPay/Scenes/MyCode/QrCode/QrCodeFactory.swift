import Foundation

enum QrCodeFactory {
    static func make(size: CGSize, delegate: QrCodeDelegate? = nil) -> QrCodeView {
        let container = DependencyContainer()
        let presenter = QrCodePresenter()
        let service = QrCodeService(dependencies: container)
        let interactor = QrCodeInteractor(presenter: presenter, service: service, size: size, delegate: delegate)
        let view = QrCodeView(interactor: interactor)
        presenter.view = view
        return view
    }
}
