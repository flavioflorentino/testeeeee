import Foundation

struct QrCodeResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case text = "qrCodeText"
    }
    
    let text: String
}
