import Core

enum QrCodeEndpoint: ApiEndpointExposable {
    case brCodeGenerate(amount: String?)

    var path: String {
        switch self {
        case .brCodeGenerate:
            return "/brcode/generate/p2p"
        }
    }
    
    var parameters: [String: Any] {
        switch self {
        case let .brCodeGenerate(amount):
            guard let amount = amount else {
                return [:]
            }
            return ["amount": amount]
        }
    }
}
