import Foundation

protocol QrCodePresenting {
    func presentLogoImageViewInsets(_ inset: CGFloat)
    func presentConsumerImage(url: URL)
    func presentQrCodeImage(_ image: UIImage)
}

final class QrCodePresenter {
    weak var view: QrCodeDisplay?
}

// MARK: - QrCodePresenting
extension QrCodePresenter: QrCodePresenting {
    func presentLogoImageViewInsets(_ inset: CGFloat) {
        view?.setupLogoImageViewInsets(inset)
    }
    
    func presentConsumerImage(url: URL) {
        view?.displayUserImage(url: url)
    }
    
    func presentQrCodeImage(_ image: UIImage) {
        view?.displayQrCodeImage(image)
    }
}
