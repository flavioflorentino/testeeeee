import UI
import UIKit

private enum Layout {
    static let proportionUserImage: CGFloat = 1 / 5
    static let proportionLogoImage: CGFloat = 1 / 7
}

protocol QrCodeDisplay: AnyObject {
    func setupLogoImageViewInsets(_ inset: CGFloat)
    func displayUserImage(url: URL)
    func displayQrCodeImage(_ image: UIImage)
}

final class QrCodeView: UIView {
    private var qrCodeImageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private var userImageView: UIImageView = {
        let view = UICircularImageView()
        view.isHidden = true
        view.borderColor = Colors.white.color
        view.borderWidth = Border.medium
        return view
    }()
    
    private var logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.isHidden = true
        imageView.image = #imageLiteral(resourceName: "picpaySmall")
        return imageView
    }()
    
    var qrCodeImage: UIImage? {
        qrCodeImageView.image
    }
    
    var consumerImage: UIImage? {
        userImageView.image
    }
    
    private let interactor: QrCodeInteracting
    
    init(interactor: QrCodeInteracting) {
        self.interactor = interactor
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func generateQrCode(amount: Double? = nil) {
        qrCodeImageView.isHidden = true
        interactor.generateQrCode(amount: amount)
    }
}

// MARK: - ViewConfiguration
extension QrCodeView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(qrCodeImageView)
        addSubview(userImageView)
        addSubview(logoImageView)
    }
    
    func setupConstraints() {
        qrCodeImageView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        userImageView.snp.makeConstraints {
            $0.height.width.equalToSuperview().multipliedBy(Layout.proportionUserImage)
            $0.centerX.centerY.equalToSuperview()
        }

        logoImageView.snp.makeConstraints {
            $0.height.width.equalToSuperview().multipliedBy(Layout.proportionLogoImage)
        }
    }
}

// MARK: - QrCodeDisplay
extension QrCodeView: QrCodeDisplay {
    func setupLogoImageViewInsets(_ inset: CGFloat) {
        logoImageView.snp.makeConstraints {
            $0.bottom.trailing.equalToSuperview().inset(inset)
        }
    }
    
    func displayUserImage(url: URL) {
        userImageView.setImage(url: url)
        userImageView.isHidden = false
    }
    
    func displayQrCodeImage(_ image: UIImage) {
        qrCodeImageView.image = image
        qrCodeImageView.isHidden = false
        logoImageView.isHidden = false
    }
}
