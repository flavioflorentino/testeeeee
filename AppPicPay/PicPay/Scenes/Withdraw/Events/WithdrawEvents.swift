import AnalyticsModule

enum WithdrawEvents: AnalyticsKeyProtocol {
    case cashoutRequest(bankName: String)
    case changeAccount(bankName: String)
    case editAccount(bankName: String)
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case .cashoutRequest(let bank):
            return AnalyticsEvent("Saque em lotérica solicitado", properties: ["Banco": bank], providers: [.mixPanel, .appsFlyer, .firebase])
        case .changeAccount(let bank):
            return AnalyticsEvent("Saque - Cadastrar outra conta", properties: ["Banco": bank], providers: [.mixPanel, .appsFlyer, .firebase])
        case .editAccount(let bank):
            return AnalyticsEvent("Saque - Corrigir dados", properties: ["Banco": bank], providers: [.mixPanel, .appsFlyer, .firebase])
        }
    }
}
