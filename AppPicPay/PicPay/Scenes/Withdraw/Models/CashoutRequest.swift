import Foundation

struct CashoutRequest: Codable {
    private enum CodingKeys: String, CodingKey {
        case status
        case limit
        case withdrawalCef = "withdrawal_cef"
    }
    
    let status: WithdrawStatus
    let limit: CashoutLimit?
    let withdrawalCef: CashoutRequestPending?
    
    init(withdrawalCef: CashoutRequestPending) {
        self.status = .pending
        self.withdrawalCef = withdrawalCef
        self.limit = nil
    }
}

struct CashoutLimit: Codable {
    private enum CodingKeys: String, CodingKey {
        case availableWithdrawValue = "available_withdrawal_value"
        case withdrawLimitInfo = "withdrawal_limit_info"
    }
    
    let availableWithdrawValue: Double
    let withdrawLimitInfo: String
}

struct CashoutRequestPending: Codable {
    private enum CodingKeys: String, CodingKey {
        case affiliationCode = "affiliation_code"
        case userId = "user_id"
        case pin
        case value
        case disclaimer
    }

    let affiliationCode: String
    let userId: String
    let pin: String
    let value: String
    let disclaimer: String
}
