import Foundation

final class SetupCashoutInfo {
    let text: String
    let value: String
    let shouldHideSeparatorView: Bool
    
    init(text: String, value: String, shouldHideSeparatorView: Bool = false) {
        self.text = text
        self.value = value
        self.shouldHideSeparatorView = shouldHideSeparatorView
    }
}
