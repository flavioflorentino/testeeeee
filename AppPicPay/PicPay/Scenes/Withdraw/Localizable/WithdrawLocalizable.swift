import Foundation

enum WithdrawLocalizable: String, Localizable {
    case requestNewWithdraw
    case transferCanceled
    case cancelReason
    case registerNewAccount
    case withdrawCanceledFormatText
    case withdrawDetailTitleText
    case withdrawDetailAccountText
    case withdrawDetailScreenTitle
    case waitingConclusion
    case withdrawType
    case oops
    case couldNotProcessRequest
    case unlinkThisAccount
    case descriptionPlaceholder
    case transferPix
    case scheduleTransfer
    
    var key: String {
        rawValue
    }
    var file: LocalizableFile {
        .withdraw
    }
}
