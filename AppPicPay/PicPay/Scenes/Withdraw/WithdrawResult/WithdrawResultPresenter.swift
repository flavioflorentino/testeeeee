import Core
import CoreLegacy
import Foundation

public protocol WithdrawResultPresenting: AnyObject {
    var viewController: WithdrawResultDisplay? { get set }
    
    func displayValues(actionButtonTitle: String, buttonAction: ButtonAction, withdrawData: PPWithdrawal)
    func displayChangeAccount(with withdrawData: PPWithdrawal, isSideBarMenu: Bool)
    func displayRemakeWithdraw(with withdrawData: PPWithdrawal, isSideBarMenu: Bool)
    func displayMakeNewWithdraw(isSideBarMenu: Bool)
    func displayEditAccount(with lastWithdraw: PPWithdrawal, bank: PPBank?, isSidebarMenu: Bool)
}

final class WithdrawResultPresenter: WithdrawResultPresenting {
    var viewController: WithdrawResultDisplay?
    
    private func textLabelInfo(with withdraw: PPWithdrawal) -> String {
        var dateString = ""
        if let withdrawDate = withdraw.date {
            dateString = DateFormatter.brazillianFormatter().string(from: withdrawDate)
        }
        
        let text = WithdrawLocalizable.withdrawCanceledFormatText.text
        let completeText = String(format: text, CurrencyFormatter.brazillianRealString(from: withdraw.value) ?? "", dateString)
        return completeText
    }
    
    func displayValues(actionButtonTitle: String, buttonAction: ButtonAction, withdrawData: PPWithdrawal) {
        viewController?.displayResultViewData(with: actionButtonTitle,
                                              buttonAction: buttonAction,
                                              text: textLabelInfo(with: withdrawData),
                                              description: withdrawData.reasonDescription ?? "")
    }
    
    func displayChangeAccount(with withdrawData: PPWithdrawal, isSideBarMenu: Bool) {
        let value = String(format: "%.02f", withdrawData.value?.doubleValue ?? Double(0))
        viewController?.displayChangeAccount(with: value, isSideBarMenu: isSideBarMenu)
    }
    
    func displayRemakeWithdraw(with withdrawData: PPWithdrawal, isSideBarMenu: Bool) {
        let value = String(format: "%.02f", withdrawData.value?.doubleValue ?? Double(0))
        viewController?.displayRemakeWithdraw(with: value, isSideBarMenu: isSideBarMenu)
    }
    
    func displayMakeNewWithdraw(isSideBarMenu: Bool) {
        viewController?.displayMakeNewWithdraw(isSideBarMenu: isSideBarMenu)
    }
    
    func displayEditAccount(with lastWithdraw: PPWithdrawal, bank: PPBank?, isSidebarMenu: Bool) {
        let value = String(format: "%.02f", lastWithdraw.value?.doubleValue ?? Double(0))
        
        let bankAccount = (ConsumerManager.shared.consumer?.bankAccount?.copy() as? PPBankAccount) ?? PPBankAccount()
        bankAccount.bank = bank ?? bankAccount.bank
        
        let reasonId = lastWithdraw.reasonId ?? ""
        let hasUnmatchConsumerCpf = reasonId == WithdrawalCancelReason.unmatchConsumerCpfBankAccount.rawValue
        let reasonDescription = lastWithdraw.reasonDescription ?? ""
        
        let data = EditAccountData(bankAccount: bankAccount, update: true, unmatchCpf: hasUnmatchConsumerCpf, reasonDescription: reasonDescription, lastWithdrawValue: value, isSideBarMenu: isSidebarMenu)
        viewController?.displayEditAccount(with: data)
    }
}
