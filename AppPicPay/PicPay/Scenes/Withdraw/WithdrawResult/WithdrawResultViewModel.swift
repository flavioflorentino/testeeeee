import AnalyticsModule
import Foundation

public enum ButtonAction {
    case newWithdraw
    case editAccount
    case remakeWithdraw
}

public struct EditAccountData {
    let bankAccount: PPBankAccount
    let update: Bool
    let unmatchCpf: Bool
    let reasonDescription: String
    let lastWithdrawValue: String?
    let isSideBarMenu: Bool
}

public protocol WithdrawResultViewModelInputs: AnyObject {
    func configureValues()
    func changeAccount()
    func editAccount()
    func remakeWithdraw()
    func makeNewWithdraw()
}

final class WithdrawResultViewModel {
    private let presenter: WithdrawResultPresenting

    private let lastWithdraw: PPWithdrawal
    private let lastWithdrawBank: PPBank?
    private let isSideBarMenu: Bool
    
    init(presenter: WithdrawResultPresenting, lastWithdraw: PPWithdrawal, lastWithdrawBank: PPBank?, isSideBarMenu: Bool) {
        self.presenter = presenter
        self.lastWithdraw = lastWithdraw
        self.lastWithdrawBank = lastWithdrawBank
        self.isSideBarMenu = isSideBarMenu
    }
    
    private func configActionButtonTitle() -> String {
        var title = WithdrawLocalizable.requestNewWithdraw.text
        let reasonId = lastWithdraw.reasonId ?? ""
        
        if lastWithdraw.requireAccountChange {
            title = WithdrawStrings.withdrawalUpdateData
        } else if reasonId == WithdrawalCancelReason.bankNotAuthorized.rawValue {
            title = WithdrawStrings.withdrawalRequestAgain
        }
        return title
    }
    
    private func actionForButton() -> ButtonAction {
        let reasonId = lastWithdraw.reasonId ?? ""
        
        if lastWithdraw.requireAccountChange {
            return ButtonAction.editAccount
        } else if reasonId == WithdrawalCancelReason.bankNotAuthorized.rawValue {
            return ButtonAction.remakeWithdraw
        } else {
            return ButtonAction.newWithdraw
        }
    }
}

extension WithdrawResultViewModel: WithdrawResultViewModelInputs {
    func configureValues() {
        let actionButtonTitle = configActionButtonTitle()
        let action = actionForButton()
        presenter.displayValues(actionButtonTitle: actionButtonTitle, buttonAction: action, withdrawData: lastWithdraw)
    }
    
    func changeAccount() {
        Analytics.shared.log(WithdrawEvents.changeAccount(bankName: lastWithdrawBank?.name ?? ""))
        presenter.displayChangeAccount(with: lastWithdraw, isSideBarMenu: isSideBarMenu)
    }
    
    func editAccount() {
        Analytics.shared.log(WithdrawEvents.editAccount(bankName: lastWithdrawBank?.name ?? ""))
        presenter.displayEditAccount(with: lastWithdraw, bank: lastWithdrawBank, isSidebarMenu: isSideBarMenu)
    }
    
    func remakeWithdraw() {
        presenter.displayRemakeWithdraw(with: lastWithdraw, isSideBarMenu: isSideBarMenu)
    }
    
    func makeNewWithdraw() {
        presenter.displayMakeNewWithdraw(isSideBarMenu: isSideBarMenu)
    }
}
