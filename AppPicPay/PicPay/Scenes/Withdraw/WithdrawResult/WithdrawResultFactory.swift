import Foundation

enum WithdrawResultFactory {
    static func make(withLastWithdraw withdraw: PPWithdrawal, lastWithdrawBank: PPBank?, isSideBarMenu: Bool) -> WithdrawResultViewController {
        let presenter: WithdrawResultPresenting = WithdrawResultPresenter()
        let viewModel = WithdrawResultViewModel(presenter: presenter, lastWithdraw: withdraw, lastWithdrawBank: lastWithdrawBank, isSideBarMenu: isSideBarMenu)
        let coordinator: WithdrawResultCoordinating = WithdrawResultCoordinator()
        let viewController = WithdrawResultViewController(viewModel: viewModel, coordinator: coordinator)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
