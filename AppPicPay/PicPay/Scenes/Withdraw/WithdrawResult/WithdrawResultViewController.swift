import UI
import UIKit

final class WithdrawResultViewController: LegacyViewController<WithdrawResultViewModelInputs, WithdrawResultCoordinating, UIView> {
    enum Layout {
        static let closeButtonSize: CGFloat = 28
        static let canceledImageSize: CGFloat = 80
        static let canceledImageIconSize: CGFloat = 22
        static let actionButtonHeight: CGFloat = 48
        static let extraSmallPadding: CGFloat = 4
        static let smallPadding: CGFloat = 8
        static let normalPadding: CGFloat = 12
        static let mediumPadding: CGFloat = 16
        static let titleLabelBottomSpace: CGFloat = 22
    }

    private lazy var closeButton: UIButton  = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(#imageLiteral(resourceName: "icon_round_close_green"), for: .normal)
        button.addTarget(self, action: #selector(actionClose(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var mainContentScrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.translatesAutoresizingMaskIntoConstraints = false
        scroll.backgroundColor = .clear
        return scroll
    }()
    
    private lazy var mainContentScrollViewView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var mainContentStackView: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.alignment = .center
        stack.axis = .vertical
        stack.distribution = .fill
        return stack
    }()
    
    private lazy var imageViewCompondView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var canceledImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = #imageLiteral(resourceName: "ilu_bank_ft")
        return imageView
    }()
    
    private lazy var canceledImageViewIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = #imageLiteral(resourceName: "cancelled_filled_icon")
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorGrayscale500.color
        label.text = WithdrawLocalizable.transferCanceled.text
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 28, weight: .bold)
        return label
    }()
    
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = Palette.ppColorNegative300.color
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var reasonTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = Palette.ppColorGrayscale500.color
        label.text = WithdrawLocalizable.cancelReason.text
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        return label
    }()
    
    private lazy var reasonLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = Palette.ppColorGrayscale500.color
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var actionButton: ConfirmButton = {
        let button = ConfirmButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(WithdrawLocalizable.requestNewWithdraw.text, for: .normal)
        return button
    }()
    
    private lazy var changeAccountButton: UnderlinedButton = {
        let button = UnderlinedButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(WithdrawLocalizable.registerNewAccount.text, for: .normal)
        button.updateColor(Palette.ppColorGrayscale400.color, highlightedColor: Palette.ppColorGrayscale500.color)
        button.addTarget(self, action: #selector(changeAccount(_:)), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.configureValues()
    }
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func buildViewHierarchy() {
        view.addSubview(closeButton)
        view.addSubview(mainContentScrollView)
        mainContentScrollView.addSubview(mainContentScrollViewView)
        mainContentScrollViewView.addSubview(mainContentStackView)
        mainContentStackView.addArrangedSubview(imageViewCompondView)
        imageViewCompondView.addSubview(canceledImageView)
        imageViewCompondView.addSubview(canceledImageViewIcon)
        mainContentStackView.addArrangedSubview(titleLabel)
        mainContentStackView.addArrangedSubview(textLabel)
        mainContentStackView.addArrangedSubview(reasonTitleLabel)
        mainContentStackView.addArrangedSubview(reasonLabel)
        view.addSubview(actionButton)
        view.addSubview(changeAccountButton)
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        mainContentStackView.setSpacing(Layout.titleLabelBottomSpace, after: imageViewCompondView)
        mainContentStackView.setSpacing(Layout.titleLabelBottomSpace, after: titleLabel)
        mainContentStackView.setSpacing(Layout.normalPadding, after: textLabel)
        mainContentStackView.setSpacing(Layout.normalPadding, after: reasonTitleLabel)
        mainContentStackView.setSpacing(Layout.mediumPadding, after: reasonLabel)
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.activate([
            closeButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Layout.normalPadding),
            closeButton.topAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.topAnchor, constant: Layout.smallPadding),
            closeButton.widthAnchor.constraint(equalToConstant: Layout.closeButtonSize),
            closeButton.heightAnchor.constraint(equalToConstant: Layout.closeButtonSize),
            closeButton.bottomAnchor.constraint(equalTo: mainContentScrollView.topAnchor, constant: -Layout.smallPadding)
        ])
        
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [mainContentScrollView])
        
        NSLayoutConstraint.leadingTrailingCenterX(equalTo: mainContentScrollView, for: [mainContentScrollViewView])
        NSLayoutConstraint.activate([
            mainContentScrollViewView.topAnchor.constraint(equalTo: mainContentScrollView.topAnchor),
            mainContentScrollViewView.bottomAnchor.constraint(equalTo: mainContentScrollView.bottomAnchor),
            mainContentScrollViewView.widthAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.widthAnchor),
            mainContentScrollViewView.heightAnchor.constraint(equalTo: mainContentScrollView.heightAnchor),
            mainContentScrollViewView.centerYAnchor.constraint(equalTo: mainContentScrollView.centerYAnchor)
        ])
        
        NSLayoutConstraint.leadingTrailingCenterX(equalTo: mainContentScrollViewView, for: [mainContentStackView], constant: Layout.mediumPadding)
        NSLayoutConstraint.activate([
            mainContentStackView.centerYAnchor.constraint(equalTo: mainContentScrollViewView.centerYAnchor)
        ])
        
        NSLayoutConstraint.leadingTrailing(equalTo: imageViewCompondView, for: [canceledImageView])
        NSLayoutConstraint.activate([
            canceledImageView.topAnchor.constraint(equalTo: imageViewCompondView.topAnchor),
            canceledImageView.bottomAnchor.constraint(equalTo: imageViewCompondView.bottomAnchor),
            canceledImageView.widthAnchor.constraint(equalToConstant: Layout.canceledImageSize),
            canceledImageView.heightAnchor.constraint(equalToConstant: Layout.canceledImageSize)
        ])
        
        NSLayoutConstraint.activate([
            canceledImageViewIcon.trailingAnchor.constraint(equalTo: canceledImageView.trailingAnchor, constant: Layout.extraSmallPadding),
            canceledImageViewIcon.bottomAnchor.constraint(equalTo: canceledImageView.bottomAnchor, constant: Layout.extraSmallPadding),
            canceledImageViewIcon.widthAnchor.constraint(equalToConstant: Layout.canceledImageIconSize),
            canceledImageViewIcon.heightAnchor.constraint(equalToConstant: Layout.canceledImageIconSize)
        ])
        
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [actionButton], constant: Layout.mediumPadding)
        NSLayoutConstraint.activate([
            actionButton.topAnchor.constraint(equalTo: mainContentScrollView.bottomAnchor, constant: Layout.mediumPadding),
            actionButton.bottomAnchor.constraint(equalTo: changeAccountButton.topAnchor, constant: -Layout.mediumPadding),
            actionButton.heightAnchor.constraint(equalToConstant: Layout.actionButtonHeight)
        ])
        
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [changeAccountButton], constant: Layout.mediumPadding)
        NSLayoutConstraint.activate([
            changeAccountButton.heightAnchor.constraint(equalToConstant: Layout.actionButtonHeight),
            changeAccountButton.bottomAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.bottomAnchor, constant: -Layout.mediumPadding)
        ])
    }
    
    @objc
    func actionClose(_ sender: UIButton) {        
        dismiss(animated: true)
    }
    
    @objc
    func changeAccount(_ sender: UIButton) {
        viewModel.changeAccount()
    }
    
    @objc
    func makeNewWithdraw(_ sender: UIButton) {
        viewModel.makeNewWithdraw()
    }
    
    @objc
    func remakeWithdraw(_ sender: UIButton) {
        viewModel.remakeWithdraw()
    }
    
    @objc
    func editAccount(_ sender: UIButton) {
        viewModel.editAccount()
    }
}

// MARK: View Model Outputs
extension WithdrawResultViewController: WithdrawResultDisplay {
    func displayResultViewData(with actionButtontitle: String, buttonAction: ButtonAction, text: String, description: String) {
        actionButton.setTitle(actionButtontitle, for: .normal)
        switch buttonAction {
        case .editAccount:
            actionButton.addTarget(self, action: #selector(editAccount(_:)), for: .touchUpInside)
        case .remakeWithdraw:
            actionButton.addTarget(self, action: #selector(remakeWithdraw(_:)), for: .touchUpInside)
        default:
            actionButton.addTarget(self, action: #selector(makeNewWithdraw(_:)), for: .touchUpInside)
        }
        
        textLabel.text = text
        reasonLabel.text = description
    }
    
    func displayChangeAccount(with value: String?, isSideBarMenu: Bool) {
        coordinator.perform(action: .changeAccount(lastVithdrawValue: value, isSideBarMenu: isSideBarMenu))
    }
    
    func displayEditAccount(with data: EditAccountData) {
        coordinator.perform(action: .editAccount(data: data))
    }
    
    func displayRemakeWithdraw(with value: String?, isSideBarMenu: Bool) {
        coordinator.perform(action: .remakeWithdraw(lastWithdrawValue: value, isSideBarMenu: isSideBarMenu))
    }
    
    func displayMakeNewWithdraw(isSideBarMenu: Bool) {
        coordinator.perform(action: .makeNewWithdraw(isSideBarMenu: isSideBarMenu))
    }
}
