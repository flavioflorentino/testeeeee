import UIKit

public protocol WithdrawResultDisplay: AnyObject {
    func displayResultViewData(with actionButtontitle: String, buttonAction: ButtonAction, text: String, description: String)
    func displayChangeAccount(with value: String?, isSideBarMenu: Bool)
    func displayEditAccount(with data: EditAccountData)
    func displayRemakeWithdraw(with value: String?, isSideBarMenu: Bool)
    func displayMakeNewWithdraw(isSideBarMenu: Bool)
}
