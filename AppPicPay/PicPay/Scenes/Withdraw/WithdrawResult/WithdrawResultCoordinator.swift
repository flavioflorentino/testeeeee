import UIKit

enum WithdrawResultAction {
    case changeAccount(lastVithdrawValue: String?, isSideBarMenu: Bool)
    case editAccount(data: EditAccountData)
    case remakeWithdraw(lastWithdrawValue: String?, isSideBarMenu: Bool)
    case makeNewWithdraw(isSideBarMenu: Bool)
}

protocol WithdrawResultCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: WithdrawResultAction)
}

final class WithdrawResultCoordinator: WithdrawResultCoordinating {
    weak var viewController: UIViewController?
    let dependencies = DependencyContainer()
    
    func perform(action: WithdrawResultAction) {
        switch action {
        case let .changeAccount(lastVithdrawValue, isSideBarMenu):
            changeAccount(with: lastVithdrawValue, isSideBarMenu: isSideBarMenu)
        case .editAccount(let data):
            editAccount(data)
        case let .remakeWithdraw(lastWithdrawValue, isSideBarMenu):
            remakeWithdraw(withValue: lastWithdrawValue, isSideBarMenu: isSideBarMenu)
        case .makeNewWithdraw(let isSideBarMenu):
            makeNewWithdrawal(isSidebarMenu: isSideBarMenu)
        }
    }
    
    func changeAccount(with value: String?, isSideBarMenu: Bool) {
        let controller = BanksFactory.make(onSaveCompletion: { [weak self] _ in
            guard let self = self, let vc = self.viewController else {
                return
            }
            self.viewController?.navigationController?.popToViewController(vc, animated: false)
            self.remakeWithdraw(withValue: value, isSideBarMenu: isSideBarMenu)
        })
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    func editAccount(_ data: EditAccountData) {
        // Show the bank account form when the form is finished
        // shows the withdrawal form again
        
        let invalidDocumentErrorMessage = data.unmatchCpf ? data.reasonDescription : nil
        
        var viewControllers = viewController?.navigationController?.viewControllers
        let formController = NewBankAccountFormFactory.make(
            bankAccount: data.bankAccount,
            isUpdate: data.update,
            invalidDocumentErrorMessage: invalidDocumentErrorMessage,
            onSaveCompletion: { _ in
                let newController = self.remakeWithdraw(withValue: data.lastWithdrawValue, isSideBarMenu: data.isSideBarMenu)
                
                // adjust de navigation controller stack
                viewControllers?.append(newController)
                if let viewControllers = viewControllers {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.25, execute: {
                        self.viewController?.navigationController?.viewControllers = viewControllers
                    })
                }
            }
        )
        formController.navigationItem.backBarButtonItem = UIBarButtonItem(title: DefaultLocalizable.back.text, style: .plain, target: nil, action: nil)
        
        viewController?.navigationController?.pushViewController(formController, animated: true)
    }
    
    @discardableResult
    func remakeWithdraw(withValue lastWithdrawValue: String?, isSideBarMenu: Bool) -> UIViewController {
        let formViewModel = WithdrawalFormViewModel(withdrawalType: .bank, withdrawalValue: lastWithdrawValue, isSidebarMenu: isSideBarMenu, dependencies: dependencies)
        let formController = WithdrawalFormViewController(with: formViewModel)
        viewController?.navigationController?.pushViewController(formController, animated: true)
        return formController
    }
    
    func makeNewWithdrawal(isSidebarMenu: Bool) {
        let formController = WithdrawalFormViewController(with: WithdrawalFormViewModel(withdrawalType: .bank, isSidebarMenu: isSidebarMenu, dependencies: dependencies))
        viewController?.navigationController?.pushViewController(formController, animated: true)
    }
}
