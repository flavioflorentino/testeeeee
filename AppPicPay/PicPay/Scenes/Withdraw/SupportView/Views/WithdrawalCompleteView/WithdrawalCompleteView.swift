import UI
import UIKit

final class WithdrawalCompleteView: NibView {
    @IBOutlet var titleLabel: UILabel! {
        didSet {
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
            titleLabel.textColor = Palette.ppColorGrayscale500.color
        }
    }
}
