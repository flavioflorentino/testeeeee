import Foundation
import UI

@IBDesignable
final class CashoutDetailInfoView: NibView {
    @IBOutlet var separatorView: UIView! {
        didSet {
            separatorView.backgroundColor = Palette.ppColorGrayscale300.color
        }
    }
    @IBOutlet var infoLabel: UILabel! {
        didSet {
            infoLabel.font = UIFont.systemFont(ofSize: 16)
            infoLabel.kerning = 0.3
            infoLabel.textColor = Palette.ppColorGrayscale500.color
        }
    }
    
    @IBOutlet var valueLabel: UILabel! {
        didSet {
            valueLabel.font = UIFont.boldSystemFont(ofSize: 16)
            valueLabel.kerning = 0.3
            valueLabel.textColor = Palette.ppColorBranding300.color
        }
    }
    
    func setup(with setupCashoutInfo: SetupCashoutInfo) {
        infoLabel.text = setupCashoutInfo.text
        valueLabel.text = setupCashoutInfo.value
        separatorView.isHidden = setupCashoutInfo.shouldHideSeparatorView
    }
}
