import UI
import UIKit

final class WithdrawalFormHeaderView: NibView {
    var action: (() -> Void)?
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var accountLabel: UILabel!
    @IBOutlet var actionButton: UIPPButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupColors()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBAction private func didTap(_ sender: Any) {
        action?()
    }
    
    private func setupColors() {
        backgroundColor = Palette.ppColorGrayscale000.color
        nameLabel.textColor = Palette.ppColorGrayscale600.color
        nameLabel.superview?.backgroundColor = Palette.ppColorGrayscale000.color
    }
}
