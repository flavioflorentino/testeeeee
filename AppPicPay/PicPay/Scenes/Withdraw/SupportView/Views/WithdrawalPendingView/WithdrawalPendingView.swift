import UI
import UIKit

final class WithdrawalPendingView: NibView {
    @IBOutlet var backgroundPendingRoundedView: UIView! {
        didSet {
            backgroundPendingRoundedView.backgroundColor = Palette.ppColorGrayscale100.color
            backgroundPendingRoundedView.layer.cornerRadius = backgroundPendingRoundedView.frame.height / 2
            backgroundPendingRoundedView.clipsToBounds = true
        }
    }
    @IBOutlet var titleLabel: UILabel! {
        didSet {
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
            titleLabel.textColor = Palette.ppColorGrayscale500.color
        }
    }
    @IBOutlet var backgroundDateView: UIView! {
        didSet {
            backgroundDateView.backgroundColor = Palette.ppColorGrayscale100.color
            backgroundDateView.layer.cornerRadius = 8
            backgroundDateView.clipsToBounds = true
        }
    }
    @IBOutlet var titleDateLabel: UILabel! {
        didSet {
            titleDateLabel.font = UIFont.systemFont(ofSize: 16, weight: .light)
            titleDateLabel.kerning = 0.3
            titleDateLabel.textColor = Palette.ppColorGrayscale500.color
        }
    }
    
    @IBOutlet var dateLabel: UILabel! {
        didSet {
            dateLabel.font = UIFont.boldSystemFont(ofSize: 28)
            dateLabel.kerning = 0.3
            dateLabel.textColor = Palette.ppColorGrayscale500.color
        }
    }
    @IBOutlet var infoLabel: UILabel! {
        didSet {
            infoLabel.font = UIFont.systemFont(ofSize: 14)
            infoLabel.textColor = Palette.ppColorGrayscale500.color
        }
    }
    
    func configure(with withdrawal: PPWithdrawal) {
        infoLabel.text = withdrawal.text
        let formatter = DateFormatter.brazillianFormatter()
        guard let date = withdrawal.estimatedCompletionDate else {
            dateLabel.text = "-/-/-"
            return
        }
        dateLabel.text = formatter.string(from: date)
        setupColors()
    }
    
    func setupColors() {
        backgroundColor = Palette.ppColorGrayscale100.color
        view.backgroundColor = Palette.ppColorGrayscale100.color
        backgroundDateView.backgroundColor = Palette.ppColorGrayscale000.color
    }
}
