import AnalyticsModule

enum WithdrawOptionsEvent: AnalyticsKeyProtocol {
    case listOptions
    
    func event() -> AnalyticsEventProtocol {
        switch self {
        case .listOptions:
            return AnalyticsEvent("Retirar Dinheiro - Listou Opções", providers: [.mixPanel, .appsFlyer, .firebase])
        }
    }
}
