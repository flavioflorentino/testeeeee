import Foundation
import PIX
import UIKit

enum WithdrawOptionsAction: Equatable {
    case bank24Hours
    case bankList(status: WithdrawStatus)
    case bankTransfer(status: WithdrawStatus)
    case originalTransfer(status: WithdrawStatus)
    case originalNewAccount
    case pix
}

protocol WithdrawOptionsCoordinating {
    var viewController: UIViewController? { get set }
    func perform(action: WithdrawOptionsAction)
}

final class WithdrawOptionsCoordinator: WithdrawOptionsCoordinating {
    weak var viewController: UIViewController?
    var dependencies = DependencyContainer()
    
    func perform(action: WithdrawOptionsAction) {
        switch action {
        case .bank24Hours:
            
            viewController?.navigationController?.pushViewController(CashoutValuesFactory.make(), animated: true)
        case .bankList(let status):
            openBankList(with: status)
        case .bankTransfer(let status):
            openWithdrawalForm(with: .bank, hasPendingWithdraw: status == .pending)
        case .originalTransfer(let status):
            openWithdrawalForm(with: .original, hasPendingWithdraw: status == .pending)
        case .originalNewAccount:
            openOriginalNewAccount()
        case .pix:
            openPix()
        }
    }
    
    private func openBankList(with status: WithdrawStatus) {
        let controller = BanksFactory.make(onSaveCompletion: { _ in
            self.openWithdrawalForm(with: .bank, hasPendingWithdraw: status == .pending)
        })
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func openWithdrawalForm(with type: WithdrawType, hasPendingWithdraw: Bool = false, removeBackButton: Bool = false, animated: Bool = true) {
        let formViewModel = WithdrawalFormViewModel(withdrawalType: type, isSidebarMenu: true, needCheckSidebarMenu: removeBackButton, hasPendingWithdrawal: hasPendingWithdraw, dependencies: dependencies)
        let controller = WithdrawalFormViewController(with: formViewModel)
        viewController?.navigationController?.pushViewController(controller, animated: animated)
    }
    
    private func openOriginalNewAccount() {
        let controller = RechargeOriginalFactory.make()
        controller.modalPresentationStyle = .fullScreen
        controller.delegate = self
        viewController?.present(controller, animated: true)
    }
    
    private func openPix() {
        guard let controller = viewController else { return }
        let destination = PIXConsumerFlow.hub(origin: "wallet-cashout", paymentOpening: PaymentPIXProxy(), bankSelectorType: BanksFactory.self, qrCodeScannerType: ScannerBaseViewController.self)
        let pixCoordinator = PIXConsumerFlowCoordinator(originViewController: controller, originFlow: .withdraw, destination: destination)
        pixCoordinator.start()
    }
}

extension WithdrawOptionsCoordinator: RechargeOriginalViewControllerDelegate {
    func originalOnConnect(controller: RechargeOriginalViewController) {
        controller.dismiss(animated: true) { [weak self] in
            guard let currentVC = self?.viewController else {
                return
            }
            WithdrawOptionsHelper().openBankFormWithOriginal(from: currentVC)
        }
    }
}
