import AnalyticsModule
import Foundation

protocol WithdrawOptionsViewModelInputs {
    func loadOptions()
    func chooseOption(_ option: WithdrawOption, from options: [WithdrawOption])
}

protocol WithdrawOptionsViewModelOutputs: AnyObject {
    func showErrorView(_ error: Error?)
    func showOptions(_ options: [WithdrawOption])
    func didNextStep(_ action: WithdrawOptionsAction)
    func showLoading(_ loading: Bool)
}

protocol WithdrawOptionsViewModelType: AnyObject {
    var inputs: WithdrawOptionsViewModelInputs { get }
    var outputs: WithdrawOptionsViewModelOutputs? { get set }
}

final class WithdrawOptionsViewModel: WithdrawOptionsViewModelType, WithdrawOptionsViewModelInputs {
    var inputs: WithdrawOptionsViewModelInputs { self }
    weak var outputs: WithdrawOptionsViewModelOutputs?

    private let service: WithdrawOptionsServicing
    private let consumer: MBConsumer?
    
    init(service: WithdrawOptionsServicing, consumer: MBConsumer?) {
        self.service = service
        self.consumer = consumer
    }
    
    func loadOptions() {
        outputs?.showLoading(true)
        service.loadWithdrawOptions { [weak self] options, error in
            self?.outputs?.showLoading(false)
            guard let options = options else {
                self?.outputs?.showErrorView(error)
                return
            }
            self?.outputs?.showOptions(options)
        }
    }
    
    func chooseOption(_ option: WithdrawOption, from options: [WithdrawOption]) {
        Analytics.shared.log(WithdrawalEvent.didTapWithdrawalOption(option.type, pending: option.status == .pending))
        
        switch option.type {
        case .bank24hours:
            outputs?.didNextStep(.bank24Hours)
        case .transfer:
            transferOptionFlow(with: option)
        case .original:
            verifyFlowForOriginalTransfer(with: option, from: options)
        case .pix:
            outputs?.didNextStep(.pix)
        default:
            break
        }
    }
    
    private func transferOptionFlow(with option: WithdrawOption) {
        if consumer?.bankAccount == nil {
            outputs?.didNextStep(.bankList(status: option.status))
        } else {
            outputs?.didNextStep(.bankTransfer(status: option.status))
        }
    }
    
    private func verifyFlowForOriginalTransfer(with option: WithdrawOption, from options: [WithdrawOption]) {
        let isNewAccount = !(consumer?.bankAccount?.bank?.isOriginalAccount() == true)
        
        // This check is necessary in this moment, backend is not returning the status for original transfer.
        guard let transfer = options.first(where: { $0.type == .transfer }) else {
            if isNewAccount {
                outputs?.didNextStep(.originalNewAccount)
            } else {
                outputs?.didNextStep(.originalTransfer(status: option.status))
            }
            return
        }
        
        if isNewAccount && transfer.status != .pending {
            outputs?.didNextStep(.originalNewAccount)
        } else {
            outputs?.didNextStep(.originalTransfer(status: transfer.status == .pending ? transfer.status : option.status))
        }
    }
}
