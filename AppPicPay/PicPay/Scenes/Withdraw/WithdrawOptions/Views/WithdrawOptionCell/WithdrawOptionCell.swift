import AssetsKit
import SkeletonView
import UI
import UIKit

final class WithdrawOptionCell: UITableViewCell {
    lazy var viewModel: WithdrawOptionCellViewModelType = {
        let viewModel = WithdrawOptionCellViewModel()
        viewModel.outputs = self
        
        return viewModel
    }()
    
    private let preferredContentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale000.color
        view.isSkeletonable = true
        return view
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
            .with(\.textColor, Colors.grayscale750.color)
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .left
        label.numberOfLines = 0
        label.text = WithdrawLocalizable.withdrawType.text
        label.isSkeletonable = true
        label.linesCornerRadius = 4
        return label
    }()
    
    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, Colors.grayscale500.color)
        label.textAlignment = .left
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.text = WithdrawLocalizable.descriptionPlaceholder.text // Esse placeholder foi adicionado por conta da forma como foi implementada a SkeletonView, como essa tela sera refatorada, isso será removido.
        label.isSkeletonable = true
        label.lastLineFillPercent = 85
        label.linesCornerRadius = 4
        return label
    }()
    
    private let withdrawTypeImageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        image.backgroundColor = .clear
        image.isSkeletonable = true
        return image
    }()
    
    private let arrowImageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        image.image = Resources.Icons.icoGreenRightArrow.image
        image.backgroundColor = .clear
        image.isSkeletonable = true
        return image
    }()
    
    private let withdrawStatusImageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        image.image = #imageLiteral(resourceName: "wait-outline")
        image.backgroundColor = .clear
        return image
    }()
    
    private let withdrawStatusLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = WithdrawLocalizable.waitingConclusion.text
        label.labelStyle(BodySecondaryLabelStyle(type: .default))
            .with(\.textColor, Colors.neutral400.color)
        return label
    }()
    
    private lazy var withdrawStatusStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [withdrawStatusImageView, withdrawStatusLabel])
        stackView.spacing = Spacing.base01
        stackView.alignment = .leading
        stackView.contentMode = .scaleAspectFit
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private var descriptionLabelBottomAnchor: NSLayoutConstraint?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        isSkeletonable = true
        addComponents()
        layoutComponents()
        setupView()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addComponents() {
        contentView.addSubview(preferredContentView)
        preferredContentView.addSubview(titleLabel)
        preferredContentView.addSubview(descriptionLabel)
        preferredContentView.addSubview(withdrawTypeImageView)
        preferredContentView.addSubview(arrowImageView)
    }
    
    private func setupView() {
        backgroundColor = .clear
        selectionStyle = .none
        preferredContentView.layer.cornerRadius = 6
        preferredContentView.layer.shadowColor = Colors.grayscale600.color.withAlphaComponent(0.1).cgColor
        preferredContentView.layer.shadowOpacity = 1
        preferredContentView.layer.shadowRadius = 2
        preferredContentView.layer.borderWidth = 1
        preferredContentView.layer.borderColor = Colors.grayscale600.color.withAlphaComponent(0.1).cgColor
        preferredContentView.layer.shadowOffset = CGSize(width: 0, height: 2)
    }
    
    private func hideAnimationOfSkeltonView() {
        [preferredContentView, titleLabel, descriptionLabel, withdrawStatusLabel, withdrawTypeImageView, arrowImageView].forEach {
            $0.hideSkeleton()
        }
    }
    
    private func layoutComponents() {
        NSLayoutConstraint.activate([
            preferredContentView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Spacing.base02),
            preferredContentView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Spacing.base02),
            preferredContentView.topAnchor.constraint(equalTo: contentView.topAnchor),
            preferredContentView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Spacing.base01)
        ])
        
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: withdrawTypeImageView.trailingAnchor, constant: Spacing.base02),
            titleLabel.trailingAnchor.constraint(equalTo: arrowImageView.leadingAnchor, constant: -Spacing.base02),
            titleLabel.topAnchor.constraint(equalTo: preferredContentView.topAnchor, constant: Spacing.base02),
            titleLabel.bottomAnchor.constraint(equalTo: descriptionLabel.topAnchor, constant: -Spacing.base01)
        ])
        
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Spacing.base01),
            descriptionLabel.leadingAnchor.constraint(equalTo: withdrawTypeImageView.trailingAnchor, constant: Spacing.base02),
            descriptionLabel.trailingAnchor.constraint(equalTo: arrowImageView.leadingAnchor, constant: -Spacing.base02)
        ])
        descriptionLabelBottomAnchor = descriptionLabel.bottomAnchor.constraint(equalTo: preferredContentView.bottomAnchor, constant: -Spacing.base02)
        descriptionLabelBottomAnchor?.isActive = true
        
        NSLayoutConstraint.activate([
            withdrawTypeImageView.centerYAnchor.constraint(equalTo: preferredContentView.centerYAnchor),
            withdrawTypeImageView.leadingAnchor.constraint(equalTo: preferredContentView.leadingAnchor, constant: Spacing.base02),
            withdrawTypeImageView.widthAnchor.constraint(equalToConstant: Sizing.base06),
            withdrawTypeImageView.heightAnchor.constraint(equalToConstant: Sizing.base06)
        ])
        withdrawTypeImageView.layer.cornerRadius = 24
        withdrawTypeImageView.layer.masksToBounds = true
        
        NSLayoutConstraint.activate([
            arrowImageView.centerYAnchor.constraint(equalTo: preferredContentView.centerYAnchor),
            arrowImageView.trailingAnchor.constraint(equalTo: preferredContentView.trailingAnchor, constant: -Spacing.base02),
            arrowImageView.widthAnchor.constraint(equalToConstant: Sizing.base02),
            arrowImageView.heightAnchor.constraint(equalToConstant: Sizing.base02)
        ])
    }
    
    func configure(withOption option: WithdrawOption) {
        hideAnimationOfSkeltonView()
        viewModel.inputs.viewConfigure(model: option)
    }
    
    private func addStatusView() {
        preferredContentView.addSubview(withdrawStatusStackView)
        
        descriptionLabelBottomAnchor?.isActive = false
        descriptionLabelBottomAnchor = descriptionLabel.bottomAnchor.constraint(lessThanOrEqualTo: withdrawStatusStackView.topAnchor, constant: -Spacing.base01)
        descriptionLabelBottomAnchor?.isActive = true
        
        NSLayoutConstraint.activate([
            withdrawStatusImageView.widthAnchor.constraint(equalToConstant: Sizing.base02),
            withdrawStatusStackView.leadingAnchor.constraint(lessThanOrEqualTo: withdrawTypeImageView.trailingAnchor, constant: Spacing.base02),
            withdrawStatusStackView.bottomAnchor.constraint(equalTo: preferredContentView.bottomAnchor, constant: -Spacing.base02),
            withdrawStatusStackView.trailingAnchor.constraint(equalTo: preferredContentView.trailingAnchor, constant: -Spacing.base02)
        ])
    }
}

extension WithdrawOptionCell: WithdrawOptionCellViewModelOutputs {
    func showStatus(description: String) {
        withdrawStatusLabel.text = description
    }
    
    func updateView(optionData data: OptionData) {
        titleLabel.text = data.title
        descriptionLabel.attributedText = data.attributedDescription
        withdrawTypeImageView.image = data.image
    }
    
    func showWithdrawStatusView() {
        addStatusView()
    }
}
