import Foundation
import UI

struct OptionData {
    var title: String
    var image: UIImage
    var attributedDescription: NSAttributedString
}

protocol WithdrawOptionCellViewModelInputs {
    func viewConfigure(model: WithdrawOption)
}

protocol WithdrawOptionCellViewModelOutputs: AnyObject {
    func updateView(optionData data: OptionData)
    func showWithdrawStatusView()
    func showStatus(description: String)
}

protocol WithdrawOptionCellViewModelType: AnyObject {
    var inputs: WithdrawOptionCellViewModelInputs { get }
    var outputs: WithdrawOptionCellViewModelOutputs? { get set }
}

final class WithdrawOptionCellViewModel: WithdrawOptionCellViewModelType, WithdrawOptionCellViewModelInputs {
    var inputs: WithdrawOptionCellViewModelInputs { self }
    weak var outputs: WithdrawOptionCellViewModelOutputs?
    
    func viewConfigure(model: WithdrawOption) {
        let attributedString = configureDescriptionAttributedText(withString: model.description)
        let optionData = OptionData(title: model.title, image: model.type.icon, attributedDescription: attributedString)
        outputs?.updateView(optionData: optionData)
        
        if let description = model.statusDescription {
            outputs?.showStatus(description: description)
        }
        
        if model.status == .pending {
            outputs?.showWithdrawStatusView()
        }
    }
    
    func configureDescriptionAttributedText(withString string: String) -> NSAttributedString {
        let defaultAttributedString = NSMutableAttributedString(string: string)
        let htmlAttributedString = string.description.html2Attributed(font: Typography.bodySecondary().font(), color: Colors.grayscale400.color)
        
        // This verification was needed, because the method `html2Attributed(font:) was adding a `\n` at the end of the String, making It use more space than necessary.
        guard let lastCharacter = htmlAttributedString?.string.last,
              lastCharacter == "\n",
              let length = htmlAttributedString?.length else {
            return htmlAttributedString ?? defaultAttributedString
        }
        guard let strongHtmlAttributedString = htmlAttributedString, !strongHtmlAttributedString.string.isEmpty else {
            return defaultAttributedString
        }
        let fixedAttributedString = htmlAttributedString?.attributedSubstring(from: NSRange(location: 0, length: length - 1))
        return fixedAttributedString ?? defaultAttributedString
    }
}
