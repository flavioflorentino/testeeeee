import Core
import Foundation

enum WithdrawEndpoint {
    case withdrawOptions
}

extension WithdrawEndpoint: ApiEndpointExposable {
    typealias Dependencies = HasKeychainManager
    
    var path: String {
        "api/getWithdrawalOptions.json"
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var parameters: [String: Any] {
        [:]
    }
    
    var customHeaders: [String: String] {
        let dependencies: Dependencies = DependencyContainer()
        return ["Token": dependencies.keychain.getData(key: KeychainKey.token) ?? ""]
    }
    
    var isTokenNeeded: Bool {
        false
    }    
}
