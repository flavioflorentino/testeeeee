import Core
import Foundation

typealias WithDrawOptionsCompletionBlock = (_ options: [WithdrawOption]?, _ error: Error?) -> Void

protocol WithdrawOptionsServicing {
    func loadWithdrawOptions(_ completion: @escaping WithDrawOptionsCompletionBlock)
}

final class WithdrawOptionsService: WithdrawOptionsServicing {
    typealias Dependency = HasMainQueue
    private let dependencies: Dependency
    
    init(dependencies: Dependency) {
        self.dependencies = dependencies
    }
    
    func loadWithdrawOptions(_ completion: @escaping WithDrawOptionsCompletionBlock) {
        let api = Api<WithdrawOptionsResponse>(endpoint: WithdrawEndpoint.withdrawOptions)
        
        api.execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                let mappedResult = result.map { $0.model }
                
                switch mappedResult {
                case .success(let options):
                    completion(options.options, nil)
                case .failure(let error):
                    completion(nil, error)
                }
            }
        }
    }
}
