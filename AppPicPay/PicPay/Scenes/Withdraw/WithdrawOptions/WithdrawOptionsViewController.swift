import AnalyticsModule
import CustomerSupport
import AssetsKit
import SkeletonView
import UI
import UIKit

final class WithdrawOptionsViewController: LegacyViewController<WithdrawOptionsViewModelType, WithdrawOptionsCoordinating, UIView> {
    private enum Constants: String {
        case withdrawMoney = "360003814672"
    }
    
    private let walletImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.image = Resources.Illustrations.iluWalletMoneyTransparent.image
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    private lazy var tableView: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        table.separatorStyle = .none
        table.backgroundColor = .clear
        table.delegate = self
        table.dataSource = self
        table.showsVerticalScrollIndicator = false
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = UITableView.automaticDimension
        table.isSkeletonable = true
        self.title = Strings.Atm24.withdrawMoney
        let barButton = UIBarButtonItem(title: DefaultLocalizable.btClose.text, style: .plain, target: self, action: #selector(close(_:)))
        navigationItem.leftBarButtonItem = barButton
        navigationItem.backBarButtonItem = UIBarButtonItem(title: nil, style: .plain, target: nil, action: nil)
        table.register(WithdrawOptionCell.self, forCellReuseIdentifier: String(describing: WithdrawOptionCell.self))
        return table
    }()
    
    private lazy var knowMoreHelpButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Strings.Atm24.knowMoreHelperLink, for: .normal)
        button
            .buttonStyle(LinkButtonStyle())
            .with(\.typography, .icons(.small))
            .with(\.textAttributedColor, (.branding600(), .normal))
        button.addTarget(self, action: #selector(showKnowMoreHelp), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let headerView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .clear
        v.isSkeletonable = true
        return v
    }()
    
    private lazy var errorView: GenericErrorView = {
        let genericErrorView = GenericErrorView()
        genericErrorView.translatesAutoresizingMaskIntoConstraints = false
        genericErrorView.tryAgainTapped = { [weak self] in
            self?.retryLoading()
        }
        genericErrorView.isHidden = true
        return genericErrorView
    }()
    
    private var shouldLoadWithdrawOptions = true
    private let numberOfLoadingOptions = 3
    private var withdrawOptions: [WithdrawOption] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadWithdrawOptionsIfNeeded()
    }

    override func viewDidLayoutSubviews() {
        view.layoutSkeletonIfNeeded()
    }

    override func buildViewHierarchy() {
        view.addSubview(tableView)
        tableView.tableHeaderView = headerView
        headerView.addSubview(walletImage)
        view.addSubview(knowMoreHelpButton)
        view.addSubview(errorView)
    }

    override func configureViews() {
        if #available(iOS 13.0, *) {
            isModalInPresentation = true
        }
        view.isSkeletonable = true
        Analytics.shared.log(WithdrawOptionsEvent.listOptions)
        view.backgroundColor = Colors.backgroundPrimary.color
        if #available(iOS 12.0, *), traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = Colors.black.lightColor
        }
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: tableView.topAnchor),
            headerView.leadingAnchor.constraint(equalTo: tableView.leadingAnchor, constant: Spacing.base02),
            headerView.trailingAnchor.constraint(equalTo: tableView.trailingAnchor, constant: -Spacing.base02),
            headerView.centerXAnchor.constraint(equalTo: tableView.centerXAnchor),
            headerView.heightAnchor.constraint(greaterThanOrEqualToConstant: Sizing.base09)
        ])
        
        NSLayoutConstraint.activate([
            walletImage.topAnchor.constraint(equalTo: headerView.topAnchor, constant: Spacing.base02),
            walletImage.leadingAnchor.constraint(equalTo: headerView.leadingAnchor),
            walletImage.trailingAnchor.constraint(equalTo: headerView.trailingAnchor),
            walletImage.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: -Spacing.base03)
        ])

        NSLayoutConstraint.activate([
            errorView.topAnchor.constraint(equalTo: view.topAnchor),
            errorView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            errorView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            errorView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            knowMoreHelpButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Spacing.base06),
            knowMoreHelpButton.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    
    private func loadWithdrawOptionsIfNeeded() {
        if shouldLoadWithdrawOptions {
            viewModel.inputs.loadOptions()
        }
    }
    
    @objc
    private func close(_ barButton: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    private func updateData() {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
            self?.headerView.layoutIfNeeded()
        }
    }
    
    private func presentErrorView(_ error: Error?) {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.isHidden = true
            self?.errorView.isHidden = false
        }
    }
    
    private func retryLoading() {
        self.errorView.isHidden = true
        self.tableView.isHidden = false
        self.viewModel.inputs.loadOptions()
    }
    
    @objc
    private func showKnowMoreHelp() {
        let help = FAQFactory.make(option: .category(id: Constants.withdrawMoney.rawValue))
        let helpNavigation = UINavigationController(rootViewController: help)
        present(helpNavigation, animated: true)
    }
}

// MARK: View Model Outputs
extension WithdrawOptionsViewController: WithdrawOptionsViewModelOutputs {
    func showErrorView(_ error: Error?) {
        DispatchQueue.main.async { [weak self] in
            self?.shouldLoadWithdrawOptions = false
            self?.presentErrorView(error)
        }
    }
    
    func showOptions(_ options: [WithdrawOption]) {
        DispatchQueue.main.async { [weak self] in
            self?.shouldLoadWithdrawOptions = false
            self?.withdrawOptions = options
            self?.updateData()
        }
    }
    
    func didNextStep(_ action: WithdrawOptionsAction) {
        coordinator.perform(action: action)
    }
    
    func showLoading(_ loading: Bool) {
        DispatchQueue.main.async { [weak self] in
            if loading {
                let gradient = SkeletonGradient(baseColor: Palette.ppColorGrayscale300.color)
                self?.view.showAnimatedGradientSkeleton(usingGradient: gradient)
                self?.knowMoreHelpButton.isHidden = true
            } else {
                self?.view.hideSkeleton(reloadDataAfter: true)
                self?.knowMoreHelpButton.isHidden = false
            }
        }
    }
}

extension WithdrawOptionsViewController: SkeletonTableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        withdrawOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: WithdrawOptionCell.self), for: indexPath) as? WithdrawOptionCell else {
            return WithdrawOptionCell(style: .default, reuseIdentifier: String(describing: WithdrawOptionCell.self))
        }
        
        if withdrawOptions.indices.contains(indexPath.row) {
            cell.configure(withOption: withdrawOptions[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        guard !withdrawOptions.isEmpty else {
            return
        }
        
        viewModel.inputs.chooseOption(withdrawOptions[indexPath.row], from: withdrawOptions)
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        WithdrawOptionCell.identifier
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        numberOfLoadingOptions
    }
}
