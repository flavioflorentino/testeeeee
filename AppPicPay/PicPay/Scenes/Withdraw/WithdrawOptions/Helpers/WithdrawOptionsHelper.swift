import UIKit

@objc
final class WithdrawOptionsHelper: NSObject {
    var dependencies = DependencyContainer()
    var originalBankAccount: PPBankAccount {
        let bank = PPBank()
        bank.wsId = "212"
        bank.name = "Banco Original S.a."
        bank.imageUrl = "https://s3-sa-east-1.amazonaws.com/picpay/banks/ico_cadastro_original.png"
        
        let bankAccount = PPBankAccount()
        bankAccount.bank = bank

        return bankAccount
    }
    
    func openBankFormWithOriginal(from controller: UIViewController) {
        let bankAccount = WithdrawOptionsHelper().originalBankAccount
        var viewControllers = controller.navigationController?.viewControllers
        let formController = NewBankAccountFormFactory.make(bankAccount: bankAccount, onSaveCompletion: { _ in
            // Show withtrawal form
            let viewModel = WithdrawalFormViewModel(withdrawalType: .original, isSidebarMenu: true, dependencies: self.dependencies)
            let newController = WithdrawalFormViewController(with: viewModel)
            controller.navigationController?.pushViewController(newController, animated: true)

            // adjust de navigation controller stack to remove the bankaccount form
            viewControllers?.append(newController)
            if let viewControllers = viewControllers {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.25, execute: {
                    controller.navigationController?.viewControllers = viewControllers
                })
            }
        })
        // ??
        formController.navigationItem.backBarButtonItem = UIBarButtonItem(title: DefaultLocalizable.back.text, style: .plain, target: nil, action: nil)
                
        controller.navigationController?.pushViewController(formController, animated: true)
    }
    
    // Objetive-C helper for WithdrawOptionsViewController,This is necessary because the NotificationsViewController is in Objetive-C
    @objc
    func tryOpenWithdrawOptionsViewController(from controller: UIViewController) {
        let nav = UINavigationController(rootViewController: WithdrawOptionsFactory.make())
        controller.present(nav, animated: true)
    }
}
