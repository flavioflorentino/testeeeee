import Foundation

final class WithdrawOptionsFactory {
    static func make() -> WithdrawOptionsViewController {
        let dependencies = DependencyContainer()
        let service: WithdrawOptionsServicing = WithdrawOptionsService(dependencies: dependencies)
        let viewModel: WithdrawOptionsViewModelType = WithdrawOptionsViewModel(service: service, consumer: ConsumerManager.shared.consumer)
        var coordinator: WithdrawOptionsCoordinating = WithdrawOptionsCoordinator()
        let viewController = WithdrawOptionsViewController(viewModel: viewModel, coordinator: coordinator)
        coordinator.viewController = viewController
        viewModel.outputs = viewController
        return viewController
    }
}
