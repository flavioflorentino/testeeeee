import AssetsKit
import Foundation

enum WithdrawStatus: String, Codable {
    case available
    case pending
}

enum WithdrawType: String, Decodable {
    case transfer
    case original
    case bank24hours = "banco24horas"
    case bank
    case lotericas
    case pix
    
    var icon: UIImage {
        switch self {
        case .bank24hours:
            return Resources.Icons.icoCashout24.image
        case .bank:
            return Resources.Icons.icoTransferReceipt.image
        case .lotericas:
            return #imageLiteral(resourceName: "icon-saque-loterica")
        case .original:
            return #imageLiteral(resourceName: "iluAdicionarOriginal")
        case .transfer:
            return Resources.Icons.icoTransferReceipt.image
        case .pix:
            return Resources.Icons.icoPixCicle.image
        }
    }
}

struct WithdrawOption: Decodable {
    enum CodingKeys: String, CodingKey {
        case type
        case title
        case description
        case status
        case statusDescription = "status_description"
    }
    
    let type: WithdrawType
    let title: String
    let description: String
    let status: WithdrawStatus
    let statusDescription: String?
}

struct WithdrawOptionsResponse: Decodable {
    let options: [WithdrawOption]
}
