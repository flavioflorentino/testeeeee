import Foundation

public final class WithdrawDetailFactory {
    public static func make(with withdraw: PPWithdrawal, isSidebarMenu: Bool) -> WithdrawDetailViewController {
        let presenter: WithdrawDetailPresenting = WithdrawDetailPresenter()
        let viewModel = WithdrawDetailViewModel(presenter: presenter, withdraw: withdraw, isSidebarMenu: isSidebarMenu)
        let coordinator: WithdrawDetailCoordinating = WithdrawDetailCoordinator()
        let viewController = WithdrawDetailViewController(viewModel: viewModel, coordinator: coordinator)

        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
