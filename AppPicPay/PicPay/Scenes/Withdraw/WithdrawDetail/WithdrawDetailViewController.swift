import UI
import UIKit

public final class WithdrawDetailViewController: LegacyViewController<WithdrawDetailViewModelInputs, WithdrawDetailCoordinating, UIView> {
    private enum Layout {
        static let closeButtonSize: CGFloat = 28
        static let smallPadding: CGFloat = 8
        static let normalPadding: CGFloat = 12
        static let mediumPadding: CGFloat = 16
        static let greatPadding: CGFloat = 20
        static let valueInfoVerticalSpace: CGFloat = 3
    }
    
    private lazy var closeButton: UIButton  = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(#imageLiteral(resourceName: "icon_round_close_green"), for: .normal)
        button.addTarget(self, action: #selector(actionClose(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var infoView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var dataView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Palette.ppColorGrayscale100.color
        return view
    }()
    
    private lazy var valueContentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var accountContentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var valueTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorGrayscale400.color
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        label.text = WithdrawLocalizable.withdrawDetailTitleText.text
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.textColor = Palette.ppColorGrayscale600.color
        return label
    }()
    
    private lazy var accountTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Palette.ppColorGrayscale400.color
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        label.text = WithdrawLocalizable.withdrawDetailAccountText.text
        return label
    }()
    
    private lazy var accountLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.textColor = Palette.ppColorGrayscale600.color
        return label
    }()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.configureViews()
    }
    
    override public func buildViewHierarchy() {
        view.addSubview(closeButton)
        view.addSubview(infoView)
        view.addSubview(dataView)
        dataView.addSubview(separatorView)
        dataView.addSubview(valueContentView)
        valueContentView.addSubview(valueTitleLabel)
        valueContentView.addSubview(valueLabel)
        dataView.addSubview(accountContentView)
        accountContentView.addSubview(accountTitleLabel)
        accountContentView.addSubview(accountLabel)
    }
    
    override public func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        title = WithdrawLocalizable.withdrawDetailScreenTitle.text
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override public func setupConstraints() {
        NSLayoutConstraint.activate([
            closeButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Layout.normalPadding),
            closeButton.topAnchor.constraint(equalTo: view.compatibleSafeAreaLayoutGuide.topAnchor, constant: Layout.smallPadding),
            closeButton.widthAnchor.constraint(equalToConstant: Layout.closeButtonSize),
            closeButton.heightAnchor.constraint(equalToConstant: Layout.closeButtonSize),
            closeButton.bottomAnchor.constraint(equalTo: infoView.topAnchor, constant: -Layout.smallPadding)
        ])
        
        NSLayoutConstraint.leadingTrailing(equalTo: view, for: [infoView, dataView])
        
        NSLayoutConstraint.activate([
            infoView.bottomAnchor.constraint(equalTo: dataView.topAnchor)
        ])
        
        NSLayoutConstraint.activate([
            dataView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        NSLayoutConstraint.leadingTrailing(equalTo: dataView, for: [separatorView])
        NSLayoutConstraint.activate([
            separatorView.topAnchor.constraint(equalTo: dataView.topAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: 1)
        ])
        
        addContentViewsConstraints()
        
        NSLayoutConstraint.activate([
            valueTitleLabel.leadingAnchor.constraint(equalTo: valueContentView.leadingAnchor),
            valueTitleLabel.topAnchor.constraint(equalTo: valueContentView.topAnchor),
            valueTitleLabel.bottomAnchor.constraint(equalTo: valueContentView.bottomAnchor),
            valueTitleLabel.trailingAnchor.constraint(equalTo: valueLabel.leadingAnchor, constant: -Layout.valueInfoVerticalSpace)
        ])
        
        NSLayoutConstraint.activate([
            valueLabel.topAnchor.constraint(equalTo: valueContentView.topAnchor),
            valueLabel.bottomAnchor.constraint(equalTo: valueContentView.bottomAnchor),
            valueLabel.trailingAnchor.constraint(equalTo: valueContentView.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            accountTitleLabel.leadingAnchor.constraint(equalTo: accountContentView.leadingAnchor),
            accountTitleLabel.topAnchor.constraint(equalTo: accountContentView.topAnchor),
            accountTitleLabel.bottomAnchor.constraint(equalTo: accountContentView.bottomAnchor),
            accountTitleLabel.trailingAnchor.constraint(equalTo: accountLabel.leadingAnchor, constant: -Layout.valueInfoVerticalSpace)
        ])
        
        NSLayoutConstraint.activate([
            accountLabel.topAnchor.constraint(equalTo: accountContentView.topAnchor),
            accountLabel.bottomAnchor.constraint(equalTo: accountContentView.bottomAnchor),
            accountLabel.trailingAnchor.constraint(equalTo: accountContentView.trailingAnchor)
        ])
    }
    
    private func addContentViewsConstraints() {
        NSLayoutConstraint.activate([
            valueContentView.leadingAnchor.constraint(greaterThanOrEqualTo: dataView.leadingAnchor, constant: Layout.greatPadding),
            valueContentView.trailingAnchor.constraint(lessThanOrEqualTo: dataView.trailingAnchor, constant: -Layout.greatPadding),
            valueContentView.topAnchor.constraint(equalTo: dataView.topAnchor, constant: Layout.greatPadding),
            valueContentView.bottomAnchor.constraint(equalTo: accountContentView.topAnchor, constant: -Layout.valueInfoVerticalSpace),
            valueContentView.centerXAnchor.constraint(equalTo: dataView.centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            accountContentView.leadingAnchor.constraint(greaterThanOrEqualTo: dataView.leadingAnchor, constant: Layout.greatPadding),
            accountContentView.trailingAnchor.constraint(lessThanOrEqualTo: dataView.trailingAnchor, constant: -Layout.greatPadding),
            accountContentView.bottomAnchor.constraint(equalTo: dataView.bottomAnchor, constant: -Layout.greatPadding),
            accountContentView.centerXAnchor.constraint(equalTo: dataView.centerXAnchor)
        ])
    }
    
    @objc
    func actionClose(_ sender: UIButton) {
        viewModel.closeDetail()
    }
}

// MARK: View Model Outputs
extension WithdrawDetailViewController: WithdrawDetailDisplay {
    public func displayWithdraw(with valueString: String, account: String, detailView: UIView) {
        valueLabel.text = valueString
        accountLabel.text = account
        infoView.embed(detailView)
    }
    
    public func displayCloseDetail(with success: Bool) {
        coordinator.perform(action: .close(success: success))
    }
}
