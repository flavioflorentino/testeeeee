import UIKit

public protocol WithdrawDetailDisplay: AnyObject {
    func displayWithdraw(with valueString: String, account: String, detailView: UIView)
    func displayCloseDetail(with success: Bool)
}
