import AnalyticsModule
import Foundation

public protocol WithdrawDetailViewModelInputs: AnyObject {
    func configureViews()
    func closeDetail()
}

public final class WithdrawDetailViewModel {
    private let presenter: WithdrawDetailPresenting

    private let withdraw: PPWithdrawal
    private let isSideBarMenu: Bool
    
    private var isSuccess: Bool {
        withdraw.statusCode == WithdrawalStatusCode.completed.rawValue
    }
    
    public init(presenter: WithdrawDetailPresenting, withdraw: PPWithdrawal, isSidebarMenu: Bool) {
        self.presenter = presenter
        self.withdraw = withdraw
        self.isSideBarMenu = isSidebarMenu
        
        sendAnalyticsEventForScreenState()
    }
}

extension WithdrawDetailViewModel: WithdrawDetailViewModelInputs {
    public func configureViews() {
        presenter.configureViews(with: withdraw)
    }
    
    public func closeDetail() {
        presenter.closeDetail(with: isSuccess)
    }
    
    private func sendAnalyticsEventForScreenState() {
        Analytics.shared.log(isSuccess ? WithdrawalEvent.didShowConcluded : WithdrawalEvent.didShowEstimatedConclusion)
    }
}
