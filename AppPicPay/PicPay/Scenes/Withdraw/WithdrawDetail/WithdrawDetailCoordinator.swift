import UIKit

public enum WithdrawDetailAction {
    case close(success: Bool)
}

public protocol WithdrawDetailCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: WithdrawDetailAction)
}

public final class WithdrawDetailCoordinator: WithdrawDetailCoordinating {
    public weak var viewController: UIViewController?
    
    public func perform(action: WithdrawDetailAction) {
        switch action {
        case .close(let success):
            viewController?.dismiss(animated: true, completion: {
                if success {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: FeedViewController.Observables.FeedNeedReload.rawValue), object: nil)
                    AppManager.shared.mainScreenCoordinator?.showHomeScreen()
                }
            })
        }
    }
}
