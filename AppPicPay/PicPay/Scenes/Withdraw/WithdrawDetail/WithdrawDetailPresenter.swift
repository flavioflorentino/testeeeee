import Core
import CoreLegacy
import Foundation

public protocol WithdrawDetailPresenting: AnyObject {
    var viewController: WithdrawDetailDisplay? { get set }
    func configureViews(with withdraw: PPWithdrawal)
    func closeDetail(with success: Bool)
}

final class WithdrawDetailPresenter: WithdrawDetailPresenting {
    var viewController: WithdrawDetailDisplay?
    
    private func infoView(for withdraw: PPWithdrawal) -> UIView {
        let isSuccess = withdraw.statusCode == WithdrawalStatusCode.completed.rawValue
        if isSuccess {
            return completeInfoView()
        }
        return pendingInfoView(with: withdraw)
    }
    
    private func pendingInfoView(with withdraw: PPWithdrawal) -> WithdrawalPendingView {
        let view = WithdrawalPendingView()
        view.configure(with: withdraw)
        return view
    }
    
    private func completeInfoView() -> WithdrawalCompleteView {
        WithdrawalCompleteView()
    }
    
    func configureViews(with withdraw: PPWithdrawal) {
        let valueString = CurrencyFormatter.brazillianRealString(from: withdraw.value) ?? ""
        let accountText = withdraw.bankAccountDescription ?? ""
        let view = infoView(for: withdraw)
        viewController?.displayWithdraw(with: valueString, account: accountText, detailView: view)
    }
    
    func closeDetail(with success: Bool) {
        viewController?.displayCloseDetail(with: success)
    }
}
