import UI
import UIKit

final class AvailableWithdrawalViewController: ViewController<AvailableWithdrawalViewModelInputs, UIScrollView> {
    enum Layout {
        static let topToTitleSpacing: CGFloat = 24
        static let margin: CGFloat = 16
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = AvailableWithdrawalLocalizable.availableWithdrawalTitle.text
        label.font = UIFont.systemFont(ofSize: 28, weight: .bold)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = AvailableWithdrawalLocalizable.availableWithdrawalMessage.text
        label.font = UIFont.systemFont(ofSize: 16, weight: .light)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var listStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var bottomLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        
        let text = AvailableWithdrawalLocalizable.availableWithdrawalBottomText.text
        let attributedText = NSMutableAttributedString(string: text, attributes: [.font: UIFont.systemFont(ofSize: 14, weight: .light), .foregroundColor: UIColor.gray])
        let linkRange = (text as NSString).range(of: AvailableWithdrawalLocalizable.availableWithdrawalBottomLink.text)
        attributedText.addAttributes([.font: UIFont.systemFont(ofSize: 14, weight: .medium), .underlineStyle: 1, .foregroundColor: Palette.ppColorNeutral200.color], range: linkRange)
        label.attributedText = attributedText
        label.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapBottomLabel))

        label.addGestureRecognizer(tap)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .default
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }

    override func buildViewHierarchy() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(messageLabel)
        contentView.addSubview(listStackView)
        contentView.addSubview(bottomLabel)
        view.addSubview(contentView)
    }
    
    override func setupConstraints() {
        NSLayoutConstraint.constraintAllEdges(from: contentView, to: view)
        NSLayoutConstraint.leadingTrailing(equalTo: contentView, for: [titleLabel, messageLabel, listStackView, bottomLabel], constant: Layout.margin)
        NSLayoutConstraint.activate([
            contentView.widthAnchor.constraint(equalTo: view.widthAnchor),
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Layout.topToTitleSpacing),
            messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Layout.margin),
            listStackView.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: Layout.margin),
            bottomLabel.topAnchor.constraint(equalTo: listStackView.bottomAnchor, constant: Layout.margin),
            bottomLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Layout.margin)
        ])
    }
    
    override func configureViews() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        viewModel.fetchData()
    }
    
    private func appendCell(withDescription description: String?, value: String, valueTintColor: UIColor = .gray) {
        let cell = AvailableWithdrawalCellView(description: description, value: value, valueTintColor: valueTintColor)
        listStackView.addArrangedSubview(cell)
    }
    
    @objc
    private func tapBottomLabel() {
        viewModel.didTapBottomLabel()
    }
}

// MARK: View Model Outputs
extension AvailableWithdrawalViewController: AvailableWithdrawalDisplay {
    func displayList(_ listItems: [AvailableWithdrawalCellItem]) {
        for item in listItems {
            let tintColor = item.highlight ? Palette.ppColorBranding300.color : Palette.ppColorGrayscale500.color
            appendCell(withDescription: item.description, value: item.value, valueTintColor: tintColor)
        }

        view.layoutSubviews()
    }
}
