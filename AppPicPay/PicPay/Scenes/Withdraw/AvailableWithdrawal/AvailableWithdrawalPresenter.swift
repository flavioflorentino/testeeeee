protocol AvailableWithdrawalDisplay: AnyObject {
    func displayList(_ listItems: [AvailableWithdrawalCellItem])
}

protocol AvailableWithdrawalPresenting: AnyObject {
    var viewController: AvailableWithdrawalDisplay? { get set }
    func buildList(with values: AvailableWithdrawalModel)
    func openWebview(url: URL)
}

struct AvailableWithdrawalCellItem {
    let description: String
    let value: String
    let highlight: Bool
}

final class AvailableWithdrawalPresenter: AvailableWithdrawalPresenting {
    private let coordinator: AvailableWithdrawalCoordinating
    var viewController: AvailableWithdrawalDisplay?
    
    init(coordinator: AvailableWithdrawalCoordinating) {
        self.coordinator = coordinator
    }
    
    func buildList(with values: AvailableWithdrawalModel) {
        let balanceItem = AvailableWithdrawalCellItem(description: AvailableWithdrawalLocalizable.totalWalletBalance.text,
                                                      value: formatNumber(values.balance),
                                                      highlight: false)
        let cashbackItem = AvailableWithdrawalCellItem(description: AvailableWithdrawalLocalizable.totalPromotionalBalance.text,
                                                       value: formatNumber(values.promotionalBalance),
                                                       highlight: false)
        let withdrawItem = AvailableWithdrawalCellItem(description: AvailableWithdrawalLocalizable.totalAvaliableWithdrawal.text,
                                                       value: formatNumber(values.withdrawalBalance),
                                                       highlight: true)
        viewController?.displayList([balanceItem, cashbackItem, withdrawItem])
    }
    
    private func formatNumber(_ decimalNumber: NSDecimalNumber?) -> String {
        guard let number = decimalNumber, number != NSDecimalNumber.notANumber else {
            return ""
        }
        return number.doubleValue.stringAmount
    }
    
    func openWebview(url: URL) {
        coordinator.perform(action: .openWebview(url: url))
    }
}
