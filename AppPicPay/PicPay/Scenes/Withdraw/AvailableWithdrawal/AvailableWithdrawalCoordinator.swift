enum AvailableWithdrawalAction {
    case openWebview(url: URL)
}

protocol AvailableWithdrawalCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: AvailableWithdrawalAction)
}

final class AvailableWithdrawalCoordinator: AvailableWithdrawalCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: AvailableWithdrawalAction) {
        switch action {
        case .openWebview(let url):
            let webview = WebViewFactory.make(with: url)
            viewController?.present(UINavigationController(rootViewController: webview), animated: true)
        }
    }
}
