import UI
import UIKit

final class AvailableWithdrawalCellView: UIView {
    enum Layout {
        static let separatorHeight: CGFloat = 1
        static let textTopBottomSpacing: CGFloat = 16
        static let spacingBetweenLabels: CGFloat = 8
    }
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = UIFont.systemFont(ofSize: 14, weight: .light)
        label.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.textColor = Palette.ppColorGrayscale500.color
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.textAlignment = .right
        label.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = Palette.ppColorGrayscale200.color
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    init(description: String?, value: String?, valueTintColor: UIColor = Palette.ppColorGrayscale500.color) {
        super.init(frame: .zero)
        buildViewHierarchy()
        setupConstraints()
        
        descriptionLabel.text = description
        valueLabel.text = value
        valueLabel.textColor = valueTintColor
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildViewHierarchy() {
        addSubview(descriptionLabel)
        addSubview(valueLabel)
        addSubview(separatorView)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.leadingTrailing(equalTo: self, for: [separatorView])
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: topAnchor, constant: Layout.textTopBottomSpacing),
            descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            
            valueLabel.leadingAnchor.constraint(equalTo: descriptionLabel.trailingAnchor, constant: Layout.spacingBetweenLabels),
            valueLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            valueLabel.centerYAnchor.constraint(equalTo: descriptionLabel.centerYAnchor),
            
            separatorView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: Layout.textTopBottomSpacing),
            separatorView.heightAnchor.constraint(equalToConstant: Layout.separatorHeight),
            separatorView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
