enum AvailableWithdrawalFactory {
    static func make(withValues values: AvailableWithdrawalModel) -> AvailableWithdrawalViewController {
        let coordinator: AvailableWithdrawalCoordinating = AvailableWithdrawalCoordinator()
        let presenter: AvailableWithdrawalPresenting = AvailableWithdrawalPresenter(coordinator: coordinator)
        let viewModel = AvailableWithdrawalViewModel(withValues: values, presenter: presenter)
        let viewController = AvailableWithdrawalViewController(viewModel: viewModel)
        
        coordinator.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
