import Core

protocol AvailableWithdrawalViewModelInputs {
    func fetchData()
    func didTapBottomLabel()
}

final class AvailableWithdrawalViewModel {
    private let presenter: AvailableWithdrawalPresenting
    private let values: AvailableWithdrawalModel

    init(withValues values: AvailableWithdrawalModel, presenter: AvailableWithdrawalPresenting) {
        self.values = values
        self.presenter = presenter
    }
}

extension AvailableWithdrawalViewModel: AvailableWithdrawalViewModelInputs {
    func fetchData() {
        presenter.buildList(with: values)
    }
    
    func didTapBottomLabel() {
        guard let helpcenterBaseUrl: String = Environment.get("HELPCENTER_PICPAY"),
            let url = URL(string: helpcenterBaseUrl + "retirar-dinheiro-do-picpay") else {
            return
        }
        presenter.openWebview(url: url)
    }
}
