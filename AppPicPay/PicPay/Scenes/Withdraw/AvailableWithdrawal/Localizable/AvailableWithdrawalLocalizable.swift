enum AvailableWithdrawalLocalizable: String, Localizable {
    case availableWithdrawalTitle
    case availableWithdrawalMessage
    case availableWithdrawalBottomText
    case availableWithdrawalBottomLink
    case totalWalletBalance
    case totalPromotionalBalance
    case totalAvaliableWithdrawal

    var key: String {
        rawValue
    }
    var file: LocalizableFile {
        .availableWithdrawal
    }
}
