import Foundation

@objc
final class AvailableWithdrawalModel: NSObject {
    let balance: NSDecimalNumber
    let promotionalBalance: NSDecimalNumber
    let withdrawalBalance: NSDecimalNumber
    
    @objc
    init?(balance: NSDecimalNumber?, promotional: NSDecimalNumber?, withdrawal: NSDecimalNumber?) {
        guard let balance = balance, let promotional = promotional, let withdrawal = withdrawal else {
            return nil
        }
        self.balance = balance
        promotionalBalance = promotional
        withdrawalBalance = withdrawal
        super.init()
    }
}
