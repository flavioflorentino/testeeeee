import AnalyticsModule
import AssetsKit
import CoreLegacy
import FeatureFlag
import UI
import UIKit

enum WithdrawalCancelReason: String {
    case bankIncorrectDetails = "10000"
    case unmatchConsumerCpfBankAccount = "10001"
    case limitExceeded = "10002"
    case bankNotAuthorized = "10003"
    case notAllowed = "10004"
}

// swiftlint:disable:next type_body_length
final class WithdrawalFormViewModel: NSObject {
    typealias Dependencies = HasFeatureManager
    var avaliableWithdrawalValues: AvailableWithdrawalModel?
    var lastWithdrawal: PPWithdrawal?
    var cashoutRequest: CashoutRequest?
    var lastWithdrawalBank: PPBank?
    var withdrawalType: WithdrawType
    var withdrawalValue: String?
    let isSidebarMenu: Bool
    let api: WithdrawApiProtocol
    private let dependencies: Dependencies
    private var showPixOption: Bool {
        dependencies.featureManager.isActive(.featureCashoutWithdrawalPixBool)
    }
    
    var needCheckSidebarMenu: Bool
    var hasPendingWithdrawal: Bool
    
    var withdrawalBankAccount: PPBankAccount {
        let bankAccount = (ConsumerManager.shared.consumer?.bankAccount?.copy() as? PPBankAccount) ?? PPBankAccount()
        bankAccount.bank = lastWithdrawalBank ?? bankAccount.bank
        return bankAccount
    }
    
    var consumerBankAccount: PPBankAccount {
        ConsumerManager.shared.consumer?.bankAccount ?? PPBankAccount()
    }
    
    var hasCashoutPending: Bool {
        if let cashoutRequest = cashoutRequest {
            return cashoutRequest.status == .pending
        }
        return false
    }
    
    var requireAccountChange: Bool {
        lastWithdrawal?.requireAccountChange ?? false
    }
    
    var reasonId: String {
         lastWithdrawal?.reasonId ?? ""
    }
    
    // swiftlint:disable:next function_default_parameter_at_end
    init(withdrawalType: WithdrawType, withdrawalValue: String? = nil, isSidebarMenu: Bool = false, needCheckSidebarMenu: Bool = false, hasPendingWithdrawal: Bool = false, api: WithdrawApiProtocol = WithdrawApi(), dependencies: Dependencies) {
        self.withdrawalType = withdrawalType
        self.api = api
        self.isSidebarMenu = isSidebarMenu
        self.withdrawalValue = withdrawalValue
        self.needCheckSidebarMenu = needCheckSidebarMenu
        self.hasPendingWithdrawal = hasPendingWithdrawal
        self.dependencies = dependencies
    }
    
    func formHeader(changeAccountAction: (() -> Void)? = nil, frame: CGRect = .zero) -> UIView {
        switch withdrawalType {
        case .lotericas:
            return configureHeaderCashout()
        default:
            return configureHeader(changeAccountAction: changeAccountAction, frame: frame)
        }
    }
    
    func headerBorderWidth() -> CGFloat {
        switch withdrawalType {
        case .lotericas:
            return 0.0
        default:
            return 0.5
        }
    }
    
    private func configureHeaderCashout() -> UIView {
        let cashoutHeaderView = UIView()
        let label = UILabel()
        label.text = cashoutRequest?.limit?.withdrawLimitInfo ?? ""
        label.textColor = Palette.ppColorGrayscale400.color
        label.kerning = 0.3
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .center
        cashoutHeaderView.addSubview(label)
        label.snp.makeConstraints { make in
            make.top.equalTo(cashoutHeaderView).offset(12)
            make.bottom.equalTo(cashoutHeaderView).offset(-12)
            make.leading.equalTo(cashoutHeaderView).offset(16)
            make.trailing.equalTo(cashoutHeaderView).offset(-16)
        }
        
        return cashoutHeaderView
    }
    
    private func configureHeader(changeAccountAction: (() -> Void)?, frame: CGRect) -> UIView {
        let withdrawlHeader = WithdrawalFormHeaderView(frame: frame)
        withdrawlHeader.action = changeAccountAction
        
        if withdrawalType == .original {
            withdrawlHeader.nameLabel.text = WithdrawStrings.originalHeaderText
        } else {
            withdrawlHeader.nameLabel.text = WithdrawStrings.bankHeaderText
        }
        
        let account = consumerBankAccount
        withdrawlHeader.accountLabel.text = String(format: WithdrawStrings.formatAccount, account.accountNumber ?? "", account.accountDigit ?? "", account.bank?.name ?? "")
        
        if let urlString = account.bank?.imageUrl {
            withdrawlHeader.imageView.setImage(url: URL(string: urlString), placeholder: #imageLiteral(resourceName: "ico_bank_placeholder.png"))
        }
        
        return withdrawlHeader
    }
    
    private func getLastWithdrawal(_ completion: @escaping (_ error: PicPayError?) -> Void) {
        api.getLastWithdrawal { [weak self] result in
            switch result {
            case let .success((withdraw, avaliableWithdrawal, bank)):
                if let withdrawl = withdraw {
                    self?.lastWithdrawal = withdrawl
                }
                if let availableValues = avaliableWithdrawal {
                    self?.avaliableWithdrawalValues = availableValues
                }
                if let bank = bank {
                    self?.lastWithdrawalBank = bank
                }
                completion(nil)
            case let .failure(error):
                completion(error)
            }
        }
    }
    
    func withdrawalSchedule(completion: @escaping (_ needToProcess: Bool, _ error: PicPayError?) -> Void, showSchedule: @escaping (Alert, Bool) -> Void) {
        guard let windowInfo = self.lastWithdrawal?.windowInfo, windowInfo.warnUser else {
            completion(false, nil)
            return
        }
        let alert = Alert(title: windowInfo.title, text: windowInfo.text)
        let imageResource = showPixOption ? Resources.Illustrations.iluPersonInfo.image : Resources.Icons.icoCalendar.image
                alert.image = Alert.Image(with: imageResource)
        showSchedule(alert, showPixOption)
    }
    
    func makeWithdrawal(value: Double, pin: String, _ completion:@escaping (_ error: PicPayError?) -> Void) {
        switch withdrawalType {
        case .lotericas:
            api.makeNewCashoutRequest(value: value,
                                      success: { [weak self] cashoutRequestPending in
                self?.cashoutRequest = CashoutRequest(withdrawalCef: cashoutRequestPending)
                Analytics.shared.log(WithdrawEvents.cashoutRequest(bankName: ""))
                DispatchQueue.main.async {
                    completion(nil)
                }
            }, failure: { error in
                completion(error)
            })
        default:
            WSConsumer.makeWithdrawal(for: consumerBankAccount, value: NSDecimalNumber(value: value), pin: pin) { [weak self] withdrawl, error in
                if let withdrawl = withdrawl {
                    self?.lastWithdrawal = withdrawl
                }
                DispatchQueue.main.async {
                    var picPayError: PicPayError?
                    if let error = error {
                        picPayError = PicPayError(message: error.localizedDescription)
                    }
                    completion(picPayError)
                }
            }
        }
    }
    
    private func processLastWithdrawal(makeWithdraw: Bool) -> UIViewController? {
        guard let lastWithdrawal = lastWithdrawal else {
            return nil
        }
        
        var controllerToReplace: UIViewController?
        if lastWithdrawal.isCancelled {
            controllerToReplace = WithdrawResultFactory.make(withLastWithdraw: lastWithdrawal, lastWithdrawBank: lastWithdrawalBank, isSideBarMenu: isSidebarMenu)
        } else if lastWithdrawal.statusCode != WithdrawalStatusCode.completed.rawValue || makeWithdraw {
            let controller = WithdrawDetailFactory.make(with: lastWithdrawal, isSidebarMenu: isSidebarMenu)
            controllerToReplace = controller
        }
        return controllerToReplace
    }
    
    private func processCashoutRequestPending() -> UIViewController? {
        if hasCashoutPending, let cashoutRequestPending = cashoutRequest?.withdrawalCef {
            let model = CashoutDetailViewModel(cashoutRequestPending: cashoutRequestPending)
            let controller = CashoutDetailViewController(viewModel: model)
            return controller
        }
        return nil
    }
    
    func processedControllerToReplace(makeWithdraw: Bool = false) -> UIViewController? {
        switch withdrawalType {
        case .lotericas:
            return processCashoutRequestPending()
        default:
            return processLastWithdrawal(makeWithdraw: makeWithdraw)
        }
    }
    
    private func retrieveWithdrawCashoutRequest(completion: @escaping (_ cashoutRequest: CashoutRequest?, _ error: PicPayError?) -> Void) {
        api.loadWithdrawCashoutRequest(success: { [weak self] cashoutRequest in
            self?.cashoutRequest = cashoutRequest
            completion(cashoutRequest, nil)
        }, failure: { error in
            completion(nil, error)
        })
    }
    
    func requestLastWithdraw(hasPendingWithdrawal: Bool, completion: @escaping (_ needToProcess: Bool, _ error: PicPayError?) -> Void, showSchedule: @escaping (Alert, Bool) -> Void) {
        switch withdrawalType {
        case .lotericas:
            retrieveWithdrawCashoutRequest { cashoutRequest, error in
                guard let cashoutRequest = cashoutRequest else {
                    completion(true, error)
                    return
                }
                completion(cashoutRequest.status == .pending, error)
            }
        default:
            guard lastWithdrawal == nil else {
                return
            }
            
            guard hasPendingWithdrawal else {
                getLastWithdrawal { [weak self] _ in
                    self?.withdrawalSchedule(completion: completion, showSchedule: showSchedule)
                }
                return
            }
            
            getLastWithdrawal { error in
                guard let error = error else {
                    completion(true, nil)
                    return
                }
                completion(true, error)
            }
        }
    }
    
    func actionButtonText() -> String {
        switch withdrawalType {
        case .lotericas:
            return WithdrawStrings.cashoutActionButtonText
        default:
            return WithdrawStrings.withdrawActionButtonText
        }
    }
    
    func titleWithdrawalLabelText() -> String {
        switch withdrawalType {
        case .lotericas:
            return WithdrawStrings.cashoutTitleText
        default:
            return WithdrawStrings.withdrawTitleText
        }
    }
    
    func avalibleWithdrawlLabelText() -> NSAttributedString {
        switch withdrawalType {
        case .lotericas:
            return formAvalibleCashoutLimitText()
        default:
            return formAvalibleWithdrawlText()
        }
    }
    
    private func formAvalibleCashoutLimitText() -> NSAttributedString {
        if let cashoutLimit = cashoutRequest?.limit?.availableWithdrawValue {
            let limit = cashoutLimit.getFormattedPrice()
            let attributedString = NSMutableAttributedString(string: WithdrawStrings.cashoutAvailableFounds + limit)
            attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 14.0, weight: UIFont.Weight.semibold), range: NSRange(location: 23, length: limit.length))
            
            return attributedString
        }
        return NSAttributedString(string: "")
    }
    
    private func formAvalibleWithdrawlText() -> NSAttributedString {
        if let withdrawalBalance = ConsumerManager.shared.consumer?.withdrawBalance {
            let limit = CurrencyFormatter.brazillianRealString(from: withdrawalBalance) ?? ""
            
            let attributedString = NSMutableAttributedString(string: WithdrawStrings.withdrawAvailableFounds + limit)
            attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 14.0, weight: UIFont.Weight.semibold), range: NSRange(location: 29, length: limit.length))
            
            return attributedString
        }
        return NSAttributedString(string: "")
    }
    
    func sanitized(value: String?) -> Double? {
        guard let sanitizedValue = value?.replacingOccurrences(of: ".", with: "").replacingOccurrences(of: ",", with: ".") else {
            return nil
        }
        
        return (sanitizedValue as NSString).doubleValue
    }
}
