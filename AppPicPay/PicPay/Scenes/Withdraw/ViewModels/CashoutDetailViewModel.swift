import Foundation

final class CashoutDetailViewModel {
    enum SetupCashoutInfoKey {
        case affiliationCode
        case userId
        case pin
        case value
    }
    private let cashoutRequestPending: CashoutRequestPending
    private let api: WithdrawApiProtocol
    
    private var affiliationCode: SetupCashoutInfo {
        SetupCashoutInfo(text: WithdrawStrings.affiliationCodeText, value: cashoutRequestPending.affiliationCode)
    }
    
    private var userId: SetupCashoutInfo {
        SetupCashoutInfo(text: WithdrawStrings.idText, value: cashoutRequestPending.userId)
    }
    
    private var pin: SetupCashoutInfo {
        SetupCashoutInfo(text: WithdrawStrings.pinText, value: cashoutRequestPending.pin)
    }
    
    private var value: SetupCashoutInfo {
        SetupCashoutInfo(text: WithdrawStrings.valueText, value: cashoutRequestPending.value, shouldHideSeparatorView: true)
    }
    
    var disclaimer: String {
        cashoutRequestPending.disclaimer
    }
    
    init(cashoutRequestPending: CashoutRequestPending, api: WithdrawApiProtocol = WithdrawApi()) {
        self.cashoutRequestPending = cashoutRequestPending
        self.api = api
    }
    
    func cancelCashout(completion: @escaping (_ error: PicPayError?) -> Void) {
        api.cancelCashoutRequest { response in
            switch response {
            case .success:
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    func getSetupCashoutInfo(for key: SetupCashoutInfoKey) -> SetupCashoutInfo {
        switch key {
        case .affiliationCode:
            return affiliationCode
        case .userId:
            return userId
        case .pin:
            return pin
        case .value:
            return value
        }
    }
}
