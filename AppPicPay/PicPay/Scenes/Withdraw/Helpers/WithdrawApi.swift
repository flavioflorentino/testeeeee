import Core
import FeatureFlag
import Foundation
import SwiftyJSON

protocol WithdrawApiProtocol {
    typealias LastWithdrawalData = (withdraw: PPWithdrawal?, availableWithdrawalModel: AvailableWithdrawalModel?, bank: PPBank?)
    func loadWithdrawCashoutRequest(success: @escaping (_ cashoutRequest: CashoutRequest) -> Void,
                                    failure:  @escaping (_ error: PicPayError) -> Void)
    func cancelCashoutRequest(completion: @escaping (PicPayResult<BaseApiGenericResponse>) -> Void)
    func makeNewCashoutRequest(value: Double,
                               success: @escaping (_ cashoutRequestPending: CashoutRequestPending) -> Void,
                               failure:  @escaping (_ error: PicPayError) -> Void)

    func getLastWithdrawal(completion: @escaping (PicPayResult<LastWithdrawalData>) -> Void)
}

final class WithdrawApi: WithdrawApiProtocol {
    typealias Dependencies = HasFeatureManager & HasMainQueue
    private let dependencies: Dependencies
    private var requestManager = RequestManager.shared
    
    init(with dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
    
    func getLastWithdrawal(completion: @escaping (PicPayResult<LastWithdrawalData>) -> Void) {
        WSConsumer.getLastWithdrawal({ [weak self] withdraw, avaliableWithdrawal, bank, error in
            if let error = error {
                let picPayError = PicPayError(message: error.localizedDescription)
                self?.dependencies.mainQueue.async {
                    completion(.failure(picPayError))
                }
                return
            }
            self?.dependencies.mainQueue.async {
                let data = LastWithdrawalData(withdraw: withdraw, availableWithdrawalModel: avaliableWithdrawal, bank: bank)
                completion(.success(data))
            }
        })
    }
    
    private func getWithdrawCashoutRequest(completion: @escaping (PicPayResult<CashoutRequest>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            requestManager
                .apiRequest(endpoint: kWsUrlWithdrawCashoutRequest, method: .get)
                .responseApiCodable(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func cancelCashoutRequest(completion: @escaping (PicPayResult<BaseApiGenericResponse>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            requestManager
                .apiRequest(endpoint: kWsUrlWithdrawCashoutRequest, method: .delete)
                .responseApiObject { [weak self] response in
                    self?.dependencies.mainQueue.async {
                        completion(response)
                    }
                }
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    private func sendNewCashoutRequest(value: Double, completion: @escaping (PicPayResult<CashoutRequestPending>) -> Void) {
        guard dependencies.featureManager.isActive(.transitionApiObjToCore) else {
            let params = ["value": value]
            requestManager
                .apiRequest(endpoint: kWsUrlWithdrawCashoutRequest, method: .post, parameters: params)
                .responseApiCodable(completionHandler: completion)
            return
        }
        // TODO: - adicionar helper para o core network
    }
    
    func makeNewCashoutRequest(value: Double,
                               success: @escaping (_ cashoutRequestPending: CashoutRequestPending) -> Void,
                               failure:  @escaping (_ error: PicPayError) -> Void) {
        sendNewCashoutRequest(value: value) { response in
            switch response {
            case .success(let cashoutRequest):
                success(cashoutRequest)
            case .failure(let error):
                failure(error)
            }
        }
    }
    
    func loadWithdrawCashoutRequest(success: @escaping (_ cashoutRequest: CashoutRequest) -> Void,
                                    failure:  @escaping (_ error: PicPayError) -> Void) {
        getWithdrawCashoutRequest { [weak self] response in
            self?.dependencies.mainQueue.async {
                switch response {
                case .success(let cashoutRequest):
                    success(cashoutRequest)
                case .failure(let error):
                    failure(error)
                }
            }
        }
    }
    
    func jsonError() -> PicPayError {
        PicPayError(message: "Ocorreu um erro. Tente mais tarde")
    }
}
