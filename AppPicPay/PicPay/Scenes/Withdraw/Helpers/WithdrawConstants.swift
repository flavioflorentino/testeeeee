import Foundation
import UIKit.UIImage

/*
 Backend withdrawal status code 
 const AGUARDANDO_PAGAMENTO  = 'O';
 const APROVADA              = 'P';
 const CANCELADA             = 'I';
 const RECUSADA              = 'D';
 const COMPLETADA            = 'C';
 const EM_ANALISE            = 'A';
 const DEVOLVIDA             = 'R';
 const NAO_AUTORIZADO        = 'F';
 const CHARGEBACK            = 'B';
 const EM_ANDAMENTO          = 'E';
 const ON_HOLD               = 'H';
 const RECUSADO_EM_REMESSA   = 'W';
 */

enum WithdrawalStatusCode: String {
    case completed = "C"
    case waitingPayment = "O"
    case canceled = "I"
    case refused = "D"
    case inAnalysis = "A"
    case returned = "R"
    case chargeback = "B"
    case inProgress = "E"
    case onHold = "H"
    case deliveryRefused = "W"
}

enum WithdrawStrings {
    static let withdrawActionButtonText = "Solicitar transferência"
    static let cashoutActionButtonText = "Solicitar saque"
    static let withdrawTitleText = "Quanto você quer transferir?"
    static let cashoutTitleText = "Quanto você quer sacar?"
    static let withdrawAvailableFounds = "Saldo disponível para saque: "
    static let cashoutAvailableFounds = "Disponível para saque: "
    static let bankHeaderText = "Conta bancária de destino"
    static let originalHeaderText = "Débito direto Banco Original"
    static let formatAccount = "Conta %@-%@ - %@"
    static let withdrawalRequestAgain = "Solicitar novamente"
    static let withdrawalUpdateData = "Corrigir dados"
    static let requestNewWithdrawal = "Solicitar nova transferência"
    static let firstStepText = "Peça ao atendente para realizar uma operação de \"Recebimento Eletrônico Caixa\", ou REC"
    static let cancelCashout = "Cancelar Saque"
    static let affiliationCodeText = "Nº do convênio:"
    static let idText = "Identificador:"
    static let pinText = "Senha:"
    static let valueText = "Valor a sacar:"
    static let unexpectedError = "Error inesperado"
    static let dontCancel = "Não cancelar"
    static let cashoutCancellation = "Cancelamento de saque"
    static let wannaCancelCashout = "Deseja realmente cancelar o pedido de saque em lotérica?"
    static let recCompleteName = "Recebimento Eletrônico Caixa"
    static let recAbbreviation = "REC"
}
