import AnalyticsModule
import PIX
import UI
import UIKit

// swiftlint:disable:next type_body_length
final class WithdrawalFormViewController: BaseFormController {
    fileprivate var contentViewHeightConstraint: NSLayoutConstraint?
    fileprivate var contentViewHeightOpen: CGFloat = 0
    
    private let model: WithdrawalFormViewModel
    fileprivate var headerBankWithdrawal: WithdrawalFormHeaderView?
    fileprivate var auth: PPAuth?
    
    private var loadingView: UILoadView?
    
    // MARK: - View Life Cycle
    
    @IBOutlet private var headerView: UIView!
    @IBOutlet private var valueTextField: UITextField!
    @IBOutlet private var valueCurrenceLabel: UILabel!
    @IBOutlet private var avalibleWithdrawlLabel: UILabel!
    @IBOutlet private var availableWithdrawButton: UIButton! {
        didSet {
            let image = #imageLiteral(resourceName: "green_help_button")
            availableWithdrawButton.setImage(image, for: .normal)
            availableWithdrawButton.addTarget(self, action: #selector(tapAvailableWithdrawButton), for: .touchUpInside)
        }
    }
    @IBOutlet private var availableWithdrawButtonWidth: NSLayoutConstraint!
    @IBOutlet private var titleWithdrawalLabel: UILabel!
    
    // Only for the iOS 9 and 10 issue with contraints
    @IBOutlet private var valueTextFieldWidth: NSLayoutConstraint!
    
    init(with viewModel: WithdrawalFormViewModel) {
        self.model = viewModel
        super.init(nibName: "WithdrawalFormViewController", bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // this prevents the navigation to show the menu icon when the form is inside a flow that allows
        // the back button action
        if model.needCheckSidebarMenu {
            checkSidebarMenu()
        }
        
        title = WithdrawLocalizable.withdrawDetailScreenTitle.text
        actionButton?.isEnabled = false
        valueTextField.becomeFirstResponder()
        configureHeader()
        configureForm()
        requestLastWithdrawal()
        
        // Only for the iOS 9 and 10 issue with contraints
        if #available(iOS 11.0, *) {
            valueTextFieldWidth.isActive = false
        }
    }
    
    // MARK: - User Actions
    
    @IBAction private func makeRechargeAction(_ sender: Any) {
        prepareForMakeWithdrawal()
    }
    
    private func checkSidebarMenu() {
        if model.isSidebarMenu {
            let menuButton = UIBarButtonItem(title: DefaultLocalizable.btClose.text, style: .plain, target: self, action: #selector(self.close))
            navigationItem.leftBarButtonItem = menuButton
        }
    }
    
    // MARK: - Internal Methods
    
    private func showProcessedControllerIfNeeded(makeWithdraw: Bool = false, shouldPush: Bool = false) {
        guard let controller = model.processedControllerToReplace(makeWithdraw: makeWithdraw) else {
            return
        }
        
        if shouldPush {
            navigationController?.pushViewController(controller, animated: true)
        } else {
            navigationController?.replace(self, by: controller)
        }
    }
    
    private func requestLastWithdrawal() {
        startLoadingLastWithdrawal()
        model.requestLastWithdraw(
            hasPendingWithdrawal: model.hasPendingWithdrawal,
            completion: { [weak self] needToProcess, error in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.stopLoadingLastWithdrawal()
                strongSelf.showWithdrawalScreen()
                if needToProcess {
                    strongSelf.showProcessedControllerIfNeeded()
                }
                
                guard let error = error else {
                    return
                }
                AlertMessage.showAlert(error, controller: strongSelf)
            },
            showSchedule: { [weak self] alert, showPixOption  in
                self?.stopLoadingLastWithdrawal()
                self?.showWithdrawalScreen()
                if showPixOption {
                    self?.configurePopupPixSchedule(alert: alert)
                } else {
                    self?.configurePopupSchedule(alert: alert)
                }
            }
        )
    }
    
    private func showWithdrawalScreen() {
        valueTextField.becomeFirstResponder()
        configureHeader()
        configureForm()
        if model.avaliableWithdrawalValues != nil {
            showAvailableWithdrawButton()
        }
    }
    
    func configurePopupSchedule(alert: Alert) {
        let continueButton = Button(title: DefaultLocalizable.next.text, type: .cta, action: .close)
        let cancelButton = Button(title: DefaultLocalizable.btCancel.text, type: .underlined) { [weak self] popupController, _ in
            popupController.dismiss(animated: true) {
                self?.navigationController?.popViewController(animated: true)
            }
        }
        alert.buttons = [continueButton, cancelButton]
        AlertMessage.showAlert(alert, controller: self)
    }
    
    func configurePopupPixSchedule(alert: Alert) {
        let pixButton = Button(title: WithdrawLocalizable.transferPix.text, type: .cta) { pushController, _ in
                let destination = PIXConsumerFlow.hub(origin: "wallet-cashout", paymentOpening: PaymentPIXProxy(), bankSelectorType: BanksFactory.self, qrCodeScannerType: ScannerBaseViewController.self)
                let pixCoordinator = PIXConsumerFlowCoordinator(originViewController: pushController, originFlow: .withdraw, destination: destination)
                pixCoordinator.start()
        }
        let scheduleButton = Button(title: WithdrawLocalizable.scheduleTransfer.text, type: .underlined, action: .close)
        alert.buttons = [pixButton, scheduleButton]
        AlertMessage.showAlert(alert, controller: self)
    }
    
    /// configure the form data
    private func configureForm() {
        valueTextField.addTarget(self, action: #selector(currencyTextFieldDidChange), for: .editingChanged)
        valueTextField.delegate = self
        configureActionButton()
        avalibleWithdrawlLabel.attributedText = model.avalibleWithdrawlLabelText()
        titleWithdrawalLabel.text = model.titleWithdrawalLabelText()
        
        if let value = model.withdrawalValue {
            valueTextField.text = value.currencyInputFormatting(allowCents: true)
            actionButton?.isEnabled = true
        }
    }
    
    private func configureActionButton() {
        let text = model.actionButtonText()
        actionButton?.setTitle(text, for: .normal)
    }
    
    private func prepareForMakeWithdrawal() {
        view.endEditing(true)
        valueTextField.resignFirstResponder()
        
        auth = PPAuth.authenticate({ [weak self] pin, _ in
            if let pin = pin {
                self?.makeWithdrawal(password: pin)
            }
            }, canceledByUserBlock: nil)
    }
    
    private func makeWithdrawal(password: String) {
        guard let sanitizedValue = model.sanitized(value: valueTextField.text) else {
            return
        }
        
        startLoading()
        model.makeWithdrawal(value: sanitizedValue, pin: password, { [weak self] error in
            guard let self = self else {
                return
            }
            
            self.stopLoading()
            if let error = error {
                guard let alert = error.alert else {
                    switch self.model.withdrawalType {
                    case .lotericas:
                        let alert = Alert(with: NSAttributedString(string: WithdrawLocalizable.oops.text), text: NSAttributedString(string: WithdrawLocalizable.couldNotProcessRequest.text), buttons: [Button(title: DefaultLocalizable.btOkUnderstood.text, type: .cta, action: .close)])
                        AlertMessage.showAlert(alert, controller: self)
                    default:
                        self.auth?.handleError(error, callback: { error in
                            AlertMessage.showAlert(withMessage: error?.localizedDescription, controller: self)
                        })
                    }
                    return
                }
                
                AlertMessage.showAlert(alert, controller: self)
            } else {
                self.showProcessedControllerIfNeeded(makeWithdraw: true, shouldPush: true)
            }
        })
    }
    
    private func configureHeader() {
        headerView.embed(model.formHeader(changeAccountAction: { [weak self] in
            self?.changeBankAccount()
            }, frame: headerView.frame))
        headerView.layer.borderWidth = model.headerBorderWidth()
    }
    
    override func startLoading() {
        super.startLoading()
        
        actionButton?.isEnabled = false
        actionButton?.startLoadingAnimating()
        valueTextField.isEnabled = false
    }
    
    override func stopLoading() {
        super.stopLoading()
        
        actionButton?.stopLoadingAnimating()
        actionButton?.isEnabled = true
        
        configureActionButton()
        valueTextField.isEnabled = true
    }
    
    private func startLoadingLastWithdrawal() {
        loadingView?.removeFromSuperview()
        
        loadingView = UILoadView(superview: view)
        loadingView?.startLoading()
    }
    
    private func stopLoadingLastWithdrawal() {
        loadingView?.animatedRemoveFromSuperView()
    }
    
    // Solves a bug that occurs only in iOS 9 and 10: textField not resizing while typing
    private func resizeTextField(withString string: String) {
        if #available(iOS 11.0, *) {
            // newer versions
        } else {
            let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 52.0)]
            var stringSize: CGSize
            if string.isEmpty {
                let zero = "0,00"
                stringSize = zero.size(withAttributes: attributes)
            } else {
                stringSize = string.size(withAttributes: attributes)
            }
            valueTextFieldWidth.constant = stringSize.width
        }
    }
    
    private func changeBankAccount() {
        // Asks the user if he really wanna change the bank account
        // this alert is needed beacouse the bank account will be replaced by the new input data
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: WithdrawLocalizable.unlinkThisAccount.text, style: .destructive, handler: { [weak self] _ in
            self?.openBankAccountForm()
        }))
        alert.addAction(UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    private func openBankAccountForm() {
        let controller = BanksFactory.make(onSaveCompletion: { _ in
            // configure the header to show the banck account changes
            self.configureHeader()
            
            // present the withdrawal form again
            self.navigationController?.popToViewController(self, animated: true)
        })
        
        navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: - Keyboard Events Handler
    
    override func keyboardWillOpen(_ keyboardNotification: Notification) {
        if  let animationDuration = ((keyboardNotification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey]) as? NSNumber)?.doubleValue,
            let keyboardRectValue = (keyboardNotification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            // animate the view to adjust height with the opened keyboard
            self.view.layoutIfNeeded()
            self.contentViewHeightConstraint?.constant = contentViewHeightOpen - keyboardRectValue.height
            self.viewBottomConstraint?.constant = keyboardRectValue.height
            
            UIView.animate(withDuration: animationDuration, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    override func keyboardWillHide(_ keyboardNotification: Notification) {
        if  let animationDuration = ((keyboardNotification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey]) as? NSNumber)?.doubleValue {
            // animate the view to adjust height with the opened keyboard
            self.view.layoutIfNeeded()
            self.contentViewHeightConstraint?.constant = contentViewHeightOpen
            self.viewBottomConstraint?.constant = 0
            
            UIView.animate(withDuration: animationDuration, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    // MARK: - TextfieldDelegate
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        // Workaround to the textfield jump
        textField.layoutIfNeeded()
    }
    
    @objc
    func currencyTextFieldDidChange(_ textField: UITextField) {
        if let text = textField.text {
            let amountString = text.currencyInputFormatting(allowCents: true)
            textField.text = amountString
            resizeTextField(withString: amountString)
        }
        valueCurrenceLabel.textColor = textField.text.isEmpty ? Palette.ppColorGrayscale300.color : Palette.ppColorBranding300.color
        actionButton?.isEnabled = !textField.text.isEmpty
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        textField.layoutIfNeeded()
        
        if let text = textField.text as NSString? {
            let txtAfterUpdate = text.replacingCharacters(in: range, with: string).trim()
            if txtAfterUpdate.length > 10 {
                return false
            }
        }
        return super.textField(textField, shouldChangeCharactersIn: range, replacementString: string)
    }
    
    @objc
    private func close() {
        dismiss(animated: true)
    }
    
    private func showAvailableWithdrawButton() {
        availableWithdrawButtonWidth.constant = 38
        availableWithdrawButton.isHidden = false
        availableWithdrawButton.layoutIfNeeded()
    }
    
    @objc
    private func tapAvailableWithdrawButton() {
        Analytics.shared.log(WithdrawalEvent.didTapWithdrawalValueHelp)
        guard let avaliableWithdrawalValues = model.avaliableWithdrawalValues else {
            return
        }
        navigationController?.pushViewController(AvailableWithdrawalFactory.make(withValues: avaliableWithdrawalValues), animated: true)
    }
}
