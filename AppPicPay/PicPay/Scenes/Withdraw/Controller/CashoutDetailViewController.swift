import UI
import UIKit

final class CashoutDetailViewController: PPBaseViewController {
    private let viewModel: CashoutDetailViewModel
    
    @IBOutlet var backgroundStackView: UIView! {
        didSet {
            backgroundStackView.backgroundColor = Palette.ppColorGrayscale100.color
            backgroundStackView.layer.cornerRadius = 8
        }
    }
    @IBOutlet var firstStepLabel: UILabel! {
        didSet {
            firstStepLabel.kerning = 0.3
            firstStepLabel.font = UIFont.systemFont(ofSize: 14, weight: .light)
            firstStepLabel.textColor = Palette.ppColorGrayscale500.color
            let string = WithdrawStrings.firstStepText
            firstStepLabel.attributedText = string.attributedString(withBold: [WithdrawStrings.recCompleteName, WithdrawStrings.recAbbreviation], using: firstStepLabel.font)
        }
    }
    @IBOutlet var secondStepLabel: UILabel! {
        didSet {
            secondStepLabel.kerning = 0.3
            secondStepLabel.font = UIFont.systemFont(ofSize: 14, weight: .light)
            secondStepLabel.textColor = Palette.ppColorGrayscale500.color
        }
    }
    @IBOutlet var affiliationCodeView: CashoutDetailInfoView!
    @IBOutlet var userIdView: CashoutDetailInfoView!
    @IBOutlet var pinView: CashoutDetailInfoView!
    @IBOutlet var valueView: CashoutDetailInfoView!
    @IBOutlet var disclaimerInfoLabel: UILabel! {
        didSet {
            disclaimerInfoLabel.kerning = 0.3
            disclaimerInfoLabel.textColor = Palette.ppColorGrayscale400.color
        }
    }
    @IBOutlet var cancelCashoutButton: UIPPButton! {
        didSet {
            cancelCashoutButton.setTitle(WithdrawStrings.cancelCashout, for: .normal)
            cancelCashoutButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
            cancelCashoutButton.setBorderColor(Palette.ppColorNegative300.color)
            cancelCashoutButton.setTitleColor(Palette.ppColorNegative300.color)
        }
    }
    
    init(viewModel: CashoutDetailViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "CashoutDetailViewController", bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        affiliationCodeView.setup(with: viewModel.getSetupCashoutInfo(for: .affiliationCode))
        userIdView.setup(with: viewModel.getSetupCashoutInfo(for: .userId))
        pinView.setup(with: viewModel.getSetupCashoutInfo(for: .pin))
        valueView.setup(with: viewModel.getSetupCashoutInfo(for: .value))
        disclaimerInfoLabel.attributedText = viewModel.disclaimer.html2Attributed(UIFont.systemFont(ofSize: 13))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        cancelCashoutButton.setNeedsLayout()
        cancelCashoutButton.layoutIfNeeded()
        cancelCashoutButton.layer.cornerRadius = cancelCashoutButton.frame.height / 2
    }
    
    @IBAction private func didTapCancelCashout(_ sender: Any) {
        let underlinedButton = Button(title: WithdrawStrings.dontCancel, type: .underlined, action: .close)
        underlinedButton.titleColor = PicPayStyle.mediumGrayHex
        let buttons = [Button(title: WithdrawStrings.cancelCashout, type: .destructive, action: .close), underlinedButton]
        let alert = Alert(with: NSAttributedString(string: WithdrawStrings.cashoutCancellation), text: NSAttributedString(string: WithdrawStrings.wannaCancelCashout), buttons: buttons)
        
        AlertMessage.showAlert(alert, controller: nil, action: { [weak self] _, _, ppButton in
            if ppButton.tag == 0 {
                guard let strongSelf = self else {
                    return
                }
                strongSelf.cancelCashoutButton.startLoadingAnimating(style: .gray)
                strongSelf.cancelCashout()
            }
        })
    }
    
    func cancelCashout() {
        viewModel.cancelCashout { [weak self] error in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.cancelCashoutButton.stopLoadingAnimating()
            if let error = error {
                AlertMessage.showAlert(error.alert ?? Alert(title: nil, text: WithdrawStrings.unexpectedError), controller: strongSelf)
            } else {
                strongSelf.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}
