import Core

private enum Keys: String {
    case cpf = "document_number"
    case birthDate = "birth_date"
}

enum UniversityAccountEndpoint {
    case validateStudent(cpf: String, birth: String)
}

extension UniversityAccountEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .validateStudent:
            return "studentaccount/students/validate"
        }
    }
    
    var method: HTTPMethod {
        .post
    }
    
    var body: Data? {
        switch self {
        case let .validateStudent(cpf, birth):
            return [Keys.cpf.rawValue: cpf, Keys.birthDate.rawValue: birth].toData()
        }
    }
    
    var isTokenNeeded: Bool {
        false
    }
}
