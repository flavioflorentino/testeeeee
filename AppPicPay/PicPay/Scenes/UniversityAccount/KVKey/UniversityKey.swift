import Core

// MARK: - KVStore Key
enum UniversityAccountKey: String, KVKeyContract {
    case consumerStudentValidatedApiId

    var description: String {
        rawValue
    }
}
