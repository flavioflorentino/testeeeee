import UI
import UIKit

extension UniversityOfferRegisterViewController.Layout {
    enum Sizes {
        static let topViewAspectRatio: CGFloat = 185 / 320
        static let imageViewHeight: CGFloat = 116
        static let imageViewWidth: CGFloat = 112
        static let buttonHeight: CGFloat = 36
    }
}

final class UniversityOfferRegisterViewController: ViewController<UniversityOfferRegisterViewModelInputs, UIView> {
    private typealias Localizable = UniversityAccountLocalizable
    
    fileprivate enum Layout {}
    
    private lazy var topView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.university600.color
        return view
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView(image: Assets.Student.studentCashbackOffer.image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.text = Localizable.offerRegisterTitle.text
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 0
        titleLabel.labelStyle(TitleLabelStyle(type: .large))
        return titleLabel
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.text = Localizable.offerRegisterDescription.text
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = 0
        descriptionLabel.labelStyle(BodyPrimaryLabelStyle())
        return descriptionLabel
    }()
    
    private lazy var confirmButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Localizable.offerRegisterConfirmButton.text, for: .normal)
        button.addTarget(self, action: #selector(confirmButtonTapped), for: .touchUpInside)
        button.buttonStyle(PrimaryButtonStyle())
        return button
    }()
    
    private lazy var dismissButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(Localizable.offerRegisterDismissButton.text, for: .normal)
        button.addTarget(self, action: #selector(dismissButtonTapped), for: .touchUpInside)
        button.buttonStyle(LinkButtonStyle())
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.startFlow()
    }
    
    override func buildViewHierarchy() {
        topView.addSubview(imageView)
        view.addSubviews(topView, titleLabel, descriptionLabel, confirmButton, dismissButton)
    }
    
    override func configureStyles() {
        view.backgroundColor = Colors.backgroundPrimary.color
    }
    
    override func setupConstraints() {
        topView.layout {
            $0.top == view.topAnchor
            $0.leading == view.leadingAnchor
            $0.trailing == view.trailingAnchor
            $0.height == topView.widthAnchor * Layout.Sizes.topViewAspectRatio
        }
        
        imageView.layout {
            $0.centerX == topView.centerXAnchor - Spacing.base01
            $0.bottom == topView.bottomAnchor + Spacing.base04
            $0.width == Layout.Sizes.imageViewWidth
            $0.height == imageView.widthAnchor * (Layout.Sizes.imageViewHeight / Layout.Sizes.imageViewWidth)
        }
        
        titleLabel.layout {
            $0.top == topView.bottomAnchor + Spacing.base07
            $0.leading == view.leadingAnchor + Spacing.base03
            $0.trailing == view.trailingAnchor - Spacing.base03
        }
        
        descriptionLabel.layout {
            $0.top == titleLabel.bottomAnchor + Spacing.base02
            $0.leading == titleLabel.leadingAnchor
            $0.trailing == titleLabel.trailingAnchor
        }
        
        confirmButton.layout {
            $0.centerX == view.centerXAnchor
            $0.leading == titleLabel.leadingAnchor
            $0.trailing == titleLabel.trailingAnchor
            $0.bottom == dismissButton.topAnchor - Spacing.base04
        }
        
        dismissButton.layout {
            $0.bottom == view.bottomAnchor - Spacing.base08
            $0.centerX == view.centerXAnchor
            $0.leading == titleLabel.leadingAnchor
            $0.trailing == titleLabel.trailingAnchor
        }
    }
    
    @objc
    private func confirmButtonTapped() {
        viewModel.confirm()
    }
    
    @objc
    private func dismissButtonTapped() {
        viewModel.dismiss()
    }
}
