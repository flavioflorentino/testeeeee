import Core

protocol UniversityOfferRegisterServicing {
    func removeConsumerStudentValidatedApiId()
}

final class UniversityOfferRegisterService: UniversityOfferRegisterServicing {
    typealias Dependencies = HasKVStore
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func removeConsumerStudentValidatedApiId() {
        dependencies.kvStore.removeObjectFor(UniversityAccountKey.consumerStudentValidatedApiId)
    }
}
