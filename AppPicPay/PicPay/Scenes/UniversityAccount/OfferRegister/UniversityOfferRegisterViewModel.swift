import AnalyticsModule

protocol UniversityOfferRegisterViewModelInputs: AnyObject {
    func startFlow()
    func confirm()
    func dismiss()
}

final class UniversityOfferRegisterViewModel {
    typealias Dependencies = HasAnalytics
    private let dependencies: Dependencies

    private let service: UniversityOfferRegisterServicing
    private let presenter: UniversityOfferRegisterPresenting
    
    private let validApiId: String

    init(
        service: UniversityOfferRegisterServicing,
        presenter: UniversityOfferRegisterPresenting,
        dependencies: Dependencies,
        validApiId: String
    ) {
        self.service = service
        self.presenter = presenter
        self.dependencies = dependencies
        self.validApiId = validApiId
    }
}

extension UniversityOfferRegisterViewModel: UniversityOfferRegisterViewModelInputs {
    func startFlow() {
        service.removeConsumerStudentValidatedApiId()
        dependencies.analytics.log(StudentAccountEvent.signUp)
    }
    
    func confirm() {
        dependencies.analytics.log(StudentAccountEvent.signUpConfirm)
        presenter.didNextStep(action: .registerStudentAccount(withValidatedApiId: validApiId))
    }
    
    func dismiss() {
        dependencies.analytics.log(StudentAccountEvent.signUpNotNow)
        presenter.didNextStep(action: .dismiss)
    }
}
