import Foundation

enum UniversityOfferRegisterFactory {
    static func make(validatedApiId: String) -> UniversityOfferRegisterViewController {
        let container = DependencyContainer()
        let service: UniversityOfferRegisterServicing = UniversityOfferRegisterService(dependencies: container)
        let coordinator: UniversityOfferRegisterCoordinating = UniversityOfferRegisterCoordinator()
        let presenter: UniversityOfferRegisterPresenting = UniversityOfferRegisterPresenter(coordinator: coordinator)
        let viewModel = UniversityOfferRegisterViewModel(
            service: service,
            presenter: presenter,
            dependencies: container,
            validApiId: validatedApiId
        )
        let viewController = UniversityOfferRegisterViewController(viewModel: viewModel)

        coordinator.viewController = viewController

        return viewController
    }
}
