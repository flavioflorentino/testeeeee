import Foundation

protocol UniversityOfferRegisterPresenting: AnyObject {
    func didNextStep(action: UniversityOfferRegisterAction)
}

final class UniversityOfferRegisterPresenter: UniversityOfferRegisterPresenting {
    private let coordinator: UniversityOfferRegisterCoordinating

    init(coordinator: UniversityOfferRegisterCoordinating) {
        self.coordinator = coordinator
    }
    
    func didNextStep(action: UniversityOfferRegisterAction) {
        coordinator.perform(action: action)
    }
}
