import UIKit

enum UniversityOfferRegisterAction {
    case dismiss
    case registerStudentAccount(withValidatedApiId: String)
}

protocol UniversityOfferRegisterCoordinating: AnyObject {
    var viewController: UIViewController? { get set }
    func perform(action: UniversityOfferRegisterAction)
}

final class UniversityOfferRegisterCoordinator: UniversityOfferRegisterCoordinating {
    weak var viewController: UIViewController?
    
    func perform(action: UniversityOfferRegisterAction) {
        switch action {
        case .dismiss:
            viewController?.dismiss(animated: true)
        case let .registerStudentAccount(withValidatedApiId: validateApiId):
            guard let viewController = viewController else {
                return
            }
            StudentAccountCoordinator(from: viewController).start(from: .signUpOffer(withValidatedApiId: validateApiId))
        }
    }
}
