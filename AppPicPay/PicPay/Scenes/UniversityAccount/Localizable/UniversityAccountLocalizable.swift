enum UniversityAccountLocalizable: String, Localizable {
    case offerRegisterTitle
    case offerRegisterDescription
    case offerRegisterConfirmButton
    case offerRegisterDismissButton
    
    var key: String {
        self.rawValue
    }
    
    var file: LocalizableFile {
        .universityAccount
    }
}
