import Foundation
import AnalyticsModule

enum SubscriptionEvent: AnalyticsKeyProtocol {
    case plansViewed(from: SubscriptionProcucerPlanOrigin)
    case planSelected(withPrice: Double, fromSellerID: String)
    case learnMoreSelected
    case termsAndConditionsInteracted(action: TermsAndConditionsAction)
    case addressSelected
    case paymentInfoViewed
    case subscriptionConfirmed(withPrice: Double, fromSellerID: String)
    case subscriptionViewed(status: ConsumerSubscription.Status)
    case subscriptionAddressChanged(status: ConsumerSubscription.Status)
    case aboutViewed(status: ConsumerSubscription.Status)
    case planChangesHistoryViewed(status: ConsumerSubscription.Status)
    case paymentHistoryViewed(status: ConsumerSubscription.Status)
    case subscriptionReactivated
    case cancelSubscriptionSelected
    case cancelSubscriptionInteracted(action: CancelSubscriptionAction)
    case reSubscribeSelected
    
    private var name: String {
        switch self {
        case .plansViewed:
            return "Plans Viewed"
        case .planSelected:
            return "Plan Selected"
        case .learnMoreSelected:
            return "Learn More Selected "
        case .termsAndConditionsInteracted:
            return "Terms and Conditions Interacted"
        case .addressSelected:
            return "Address Selected"
        case .paymentInfoViewed:
            return "New Subscription Information Viewed"
        case .subscriptionConfirmed:
            return "Subscription Confirmed"
        case .subscriptionViewed:
            return "Subscription Viewed"
        case .subscriptionAddressChanged:
            return "Subscription Address Changed"
        case .aboutViewed:
            return "About Viewed"
        case .planChangesHistoryViewed:
            return "Plan Changes History Viewed"
        case .paymentHistoryViewed:
            return "Payment History Viewed"
        case .subscriptionReactivated:
            return "Subscription Reactivated"
        case .cancelSubscriptionSelected:
            return "Cancel Subscription Selected"
        case .cancelSubscriptionInteracted:
            return "Cancel Subscription Interacted"
        case .reSubscribeSelected:
            return "Re Subscribe Selected"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .plansViewed(from: origin):
            return ["origin": origin.rawValue]
        case let .planSelected(price, sellerID), let .subscriptionConfirmed(price, sellerID):
            return ["price": price, "seller_ID": sellerID]
        case let .termsAndConditionsInteracted(action):
            return ["action": action.rawValue]
        case let .subscriptionViewed(status),
             let .subscriptionAddressChanged(status),
             let .aboutViewed(status),
             let .planChangesHistoryViewed(status),
             let .paymentHistoryViewed(status):
            return ["subscription_status": status.rawValue]
        case let .cancelSubscriptionInteracted(action):
            return ["action": action.rawValue]
        default:
            return [:]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent("Membership - \(name)", properties: properties, providers: providers)
    }
}

extension SubscriptionEvent {
    enum TermsAndConditionsAction: String {
        case accept = "aceito os termos"
        case cancel = "cancelar"
    }
    
    enum CancelSubscriptionAction: String {
        case yes = "Sim, quero cancelar"
        case no = "Não, não quero cancelar"
    }
}
