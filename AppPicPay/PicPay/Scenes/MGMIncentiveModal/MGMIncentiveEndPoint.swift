import Core

enum MGMIncentiveEndpoint {
    case mgmEligible
}

extension MGMIncentiveEndpoint: ApiEndpointExposable {
    var path: String {
        "mgm/eligibility/"
    }
}
