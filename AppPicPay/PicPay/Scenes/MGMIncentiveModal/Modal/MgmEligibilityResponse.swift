import Foundation

struct MgmEligibilityResponse: Decodable {
    private enum CodingKeys: String, CodingKey {
        case mgmEligible = "mgm-eligible"
    }
    
    let mgmEligible: Bool
}
