import Foundation
import Core

protocol MGMIncentiveVerifiable {
    func checkCanShowIncentive(completion:  @escaping (_ showIncentive: Bool) -> Void)
    func updateHomeAccessCount()
}

final class MGMIncentiveWorker: MGMIncentiveVerifiable {
    private var isSecondHomeAccess: Bool {
        getHomeAccessCount() == 2
    }
    private let service: MGMIncentiveServicing
    
    init(service: MGMIncentiveServicing) {
        self.service = service
    }
    
    func checkCanShowIncentive(completion: @escaping (Bool) -> Void) {
        guard isSecondHomeAccess else {
            completion(false)
            return
        }
        
        service.checkIsMgmEligibility { result in
            guard case let .success(canShowIncentive) = result else {
                completion(false)
                return
            }
            completion(canShowIncentive)
        }
    }
    
    func updateHomeAccessCount() {
        let current = getHomeAccessCount()
        KVStore().setInt(current + 1, with: .mgmIncentive)
    }
    
    private func getHomeAccessCount() -> Int {
        KVStore().intFor(.mgmIncentive)
    }
}
