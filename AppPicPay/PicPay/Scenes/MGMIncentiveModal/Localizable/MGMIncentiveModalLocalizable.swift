import Foundation

enum MGMIncentiveModalLocalizable: String, Localizable {
    case title
    case subtitle
    case buttonTitle
    case notNow

    var key: String {
        rawValue
    }
    
    var file: LocalizableFile {
        .mgmIncentiveModal
    }
}
