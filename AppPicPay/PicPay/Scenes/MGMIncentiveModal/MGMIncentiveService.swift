import Foundation
import Core

protocol MGMIncentiveServicing {
    func checkIsMgmEligibility(completion: @escaping (Result<Bool, Error>) -> Void)
}

final class MGMIncentiveService: MGMIncentiveServicing {
    typealias Dependencies = HasMainQueue
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func checkIsMgmEligibility(completion: @escaping (Result<Bool, Error>) -> Void) {
        Api<MgmEligibilityResponse>(endpoint: MGMIncentiveEndpoint.mgmEligible).execute { [weak self] result in
            self?.dependencies.mainQueue.async {
                switch result {
                case .success(let data):
                    completion(.success(data.model.mgmEligible))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}
