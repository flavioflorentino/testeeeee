import AnalyticsModule

enum FeedOpportunitiesEvent: AnalyticsKeyProtocol {
    case didTapInsideBanner(over: FeedOpportunitiesCellArea, itemTitle: String)
    case didTapInsideCarousel(carouselTitle: String, itemTitle: String, position: Int)
    case didPanCarousel(carouselTitle: String, finalReached: Bool)
    
    private var name: String {
        switch self {
        case .didTapInsideBanner, .didTapInsideCarousel:
            return "Inicio - Card Opportunities Touched"
        case .didPanCarousel:
            return "Inicio - Card Opportunities Panned"
        }
    }
    
    private var properties: [String: Any] {
        switch self {
        case let .didTapInsideBanner(area, itemTitle):
            return [
                "interaction_type": area == .button ? "TOUCH BUTTON": "TOUCH CARD",
                "item_title": itemTitle,
                "card type": "SOLO"
            ]
        case let .didTapInsideCarousel(carouselTitle, itemTitle, position):
            return [
                "interaction_type": "TOUCH CARD",
                "item_title": itemTitle,
                "card type": "CAROUSEL",
                "carousel_title": carouselTitle,
                "position": position
            ]
        case let .didPanCarousel(carouselTitle, finalReached):
            return [
                "final_reached": finalReached,
                "carousel_title": carouselTitle
            ]
        }
    }
    
    private var providers: [AnalyticsProvider] {
        [.mixPanel, .firebase]
    }
    
    func event() -> AnalyticsEventProtocol {
        AnalyticsEvent(name, properties: properties, providers: providers)
    }
}

extension FeedOpportunitiesEvent {
    enum FeedOpportunitiesCellArea {
        case background
        case button
    }
}
