import FeatureFlag
import AnalyticsModule

protocol CarouselFeedItemViewModelInputs {
    func fetchData()
    func didTapCollectionItem(at indexPath: IndexPath)
    func didStopScrolling(scrollView: UIScrollView)
}

final class CarouselFeedItemViewModel: CarouselFeedItemViewModelInputs {
    private let presenter: CarouselFeedItemPresenting
    
    private let carouselTitle: String
    private let items: [Opportunity]
    
    init(presenter: CarouselFeedItemPresenting, opportunityItem: OpportunityFeedItem) {
        self.presenter = presenter
        carouselTitle = opportunityItem.carouselPosition?.rawValue.uppercased() ?? ""
        items = opportunityItem.opportunities
    }
    
    func fetchData() {
        presenter.setupDataSourceHandler(withData: items)
    }
    
    func didTapCollectionItem(at indexPath: IndexPath) {
        guard indexPath.row < items.count, let deeplinkUrl = URL(string: items[indexPath.row].deeplink) else {
            return
        }
        let event = FeedOpportunitiesEvent.didTapInsideCarousel(
            carouselTitle: carouselTitle,
            itemTitle: items[indexPath.row].bannerTitle,
            position: indexPath.row
        )
        Analytics.shared.log(event)
        presenter.perform(action: .deeplink(url: deeplinkUrl))
    }
    
    func didStopScrolling(scrollView: UIScrollView) {
        guard FeatureManager.isActive(.feedOpportunityScrollSendEvent) else {
            return
        }
        let finalReached = scrollView.bounds.maxX >= scrollView.contentSize.width
        Analytics.shared.log(FeedOpportunitiesEvent.didPanCarousel(carouselTitle: carouselTitle, finalReached: finalReached))
    }
}
