protocol CarouselFeedItemDisplay: AnyObject {
    func setupDataSourceHandler(withData data: [Opportunity])
}

protocol CarouselFeedItemPresenting: AnyObject {
    var view: CarouselFeedItemDisplay? { get set }
    func setupDataSourceHandler(withData data: [Opportunity])
    func perform(action: CarouselFeedItemAction)
}

final class CarouselFeedItemPresenter: CarouselFeedItemPresenting {
    private let coordinator: CarouselFeedItemCoordinating
    var view: CarouselFeedItemDisplay?

    init(coordinator: CarouselFeedItemCoordinating) {
        self.coordinator = coordinator
    }
    
    func setupDataSourceHandler(withData data: [Opportunity]) {
        view?.setupDataSourceHandler(withData: data)
    }
    
    func perform(action: CarouselFeedItemAction) {
        coordinator.perform(action: action)
    }
}
