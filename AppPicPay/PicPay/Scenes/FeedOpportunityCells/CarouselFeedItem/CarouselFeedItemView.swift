import UI
import UIKit

final class CarouselFeedItemView: UIView {
    enum Layout {
        static let viewHeight: CGFloat = itemSize.height + 16.0
        static let itemSize = CGSize(width: 144.0, height: 192)

        static let spaceBetweenCells: CGFloat = 8
    }
    
    var staticHeight: CGFloat {
        Layout.viewHeight
    }
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        layout.itemSize = Layout.itemSize
        layout.minimumInteritemSpacing = Layout.spaceBetweenCells
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.delegate = self
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    private var dataHandler: CollectionViewHandler<Opportunity, CarouselFeedItemCell>?
    let viewModel: CarouselFeedItemViewModelInputs
    
    init(viewModel: CarouselFeedItemViewModelInputs) {
        self.viewModel = viewModel
        
        super.init(frame: .zero)
        buildViewHierarchy()
        configureViews()
        setupConstraints()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureViews() {
        collectionView.register(CarouselFeedItemCell.self, forCellWithReuseIdentifier: String(describing: CarouselFeedItemCell.self))
    }
    
    private func buildViewHierarchy() {
        addSubview(collectionView)
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.constraintAllEdges(from: collectionView, to: self)
    }
    
    func loadCells() {
        viewModel.fetchData()
    }
}

extension CarouselFeedItemView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.didTapCollectionItem(at: indexPath)
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            viewModel.didStopScrolling(scrollView: scrollView)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        viewModel.didStopScrolling(scrollView: scrollView)
    }
}

extension CarouselFeedItemView: CarouselFeedItemDisplay {
    func setupDataSourceHandler(withData data: [Opportunity]) {
        dataHandler = CollectionViewHandler(data: data, cellType: CarouselFeedItemCell.self, configureCell: { _, model, cell in
            cell.setup(item: model)
        })
        collectionView.dataSource = dataHandler
        collectionView.reloadData()
    }
}
