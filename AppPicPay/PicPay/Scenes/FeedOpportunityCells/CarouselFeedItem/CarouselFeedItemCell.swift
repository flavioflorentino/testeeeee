import Foundation
import SnapKit
import UI
import UIKit

// MARK: - Layout
private extension CarouselFeedItemCell.Layout {
    enum Size {
        static let icon = CGSize(width: 64.0, height: 64.0)
    }

    enum Shadow {
        static let offset = CGSize(width: 0.0, height: 1.0)
        static let radius: CGFloat = 2.0
        static let opacity: Float = 0.1
    }
}

final class CarouselFeedItemCell: UICollectionViewCell {
    fileprivate enum Layout { }

    // MARK: - Properties
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle(type: .highlight))
        label.clipsToBounds = true

        return label
    }()
    
    private lazy var iconImageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view
            .viewStyle(RoundedViewStyle())
            .with(\.cornerRadius, .full)

        return view
    }()

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Reusing
    override func prepareForReuse() {
        super.prepareForReuse()

        iconImageView.cancelRequest()
        iconImageView.image = nil
    }

    // MARK: - Setup
    func setup(item: Opportunity) {
        contentView.backgroundColor = item.backgroundColor.color

        titleLabel.text = item.carouselTitle
        titleLabel.textColor = item.textColor.color
        titleLabel.backgroundColor = item.backgroundColor.color

        iconImageView.setImage(url: URL(string: item.image))
    }
}

// MARK: - ViewConfiguration
extension CarouselFeedItemCell: ViewConfiguration {
    func buildViewHierarchy() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(iconImageView)
    }

    func configureViews() {
        contentView.viewStyle(RoundedViewStyle())

        backgroundColor = .clear

        layer.shadowColor = Colors.black.color.cgColor
        layer.shadowOffset = Layout.Shadow.offset
        layer.shadowRadius = Layout.Shadow.radius
        layer.shadowOpacity = Layout.Shadow.opacity
        layer.masksToBounds = false
    }

    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.lessThanOrEqualToSuperview().offset(-Spacing.base02)
        }

        iconImageView.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(titleLabel.snp.bottom).offset(Spacing.base02)
            $0.centerX.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-Spacing.base02)
            $0.size.equalTo(Layout.Size.icon)
        }
    }
}
