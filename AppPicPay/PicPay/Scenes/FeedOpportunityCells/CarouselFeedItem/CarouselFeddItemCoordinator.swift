enum CarouselFeedItemAction {
    case deeplink(url: URL)
}

protocol CarouselFeedItemCoordinating {
    func perform(action: CarouselFeedItemAction)
}

final class CarouselFeedItemCoordinator: CarouselFeedItemCoordinating {
    func perform(action: CarouselFeedItemAction) {
        switch action {
        case .deeplink(let url):
            DeeplinkHelper.handleDeeplink(withUrl: url)
        }
    }
}
