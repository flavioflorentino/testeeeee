enum CarouselFeedItemFactory {
    static func make(withItem item: OpportunityFeedItem) -> CarouselFeedItemView {
        let coordinator: CarouselFeedItemCoordinating = CarouselFeedItemCoordinator()
        let presenter: CarouselFeedItemPresenting = CarouselFeedItemPresenter(coordinator: coordinator)
        let viewModel = CarouselFeedItemViewModel(presenter: presenter, opportunityItem: item)
        let view = CarouselFeedItemView(viewModel: viewModel)
        
        presenter.view = view
        view.loadCells()
        
        return view
    }
}
