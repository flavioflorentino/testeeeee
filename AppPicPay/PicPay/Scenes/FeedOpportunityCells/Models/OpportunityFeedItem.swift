import Foundation

final class OpportunityFeedItem: FeedItem {
    let opportunities: [Opportunity]
    let carouselPosition: CarouselPosition?

    init(carouselPosition: CarouselPosition? = nil, opportunities: [Opportunity]) {
        self.opportunities = opportunities
        self.carouselPosition = opportunities.count == 1 ? nil : carouselPosition

        super.init()

        component = opportunities.count > 1 ? .carousel : .banner
    }

    @available(*, unavailable)
    override init?(jsonDict: [AnyHashable: Any]) {
        fatalError("init?(jsonDict:) unavailable")
    }
}

extension OpportunityFeedItem {
    enum CarouselPosition: String {
        case first
        case second
    }
}
