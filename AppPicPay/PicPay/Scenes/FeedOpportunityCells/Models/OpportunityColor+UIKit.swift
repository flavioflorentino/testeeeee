import UI
import UIKit

extension OpportunityColor {
    var color: UIColor {
        let defaultColor = Colors.grayscale850
        switch self {
        case .none:
            return defaultColor.lightColor
        case let .light(color):
            return Colors.hexColor(color)?.color ?? defaultColor.lightColor
        case let .lightAndDark(light, dark):
            let lightColor = Colors.hexColor(light)?.color ?? defaultColor.lightColor
            let darkColor = Colors.hexColor(dark)?.color ?? defaultColor.lightColor
            return DynamicColor(lightColor: lightColor, darkColor: darkColor).color
        }
    }
}
