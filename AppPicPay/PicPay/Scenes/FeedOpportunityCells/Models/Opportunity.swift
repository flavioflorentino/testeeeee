enum OpportunityColor {
    case light(_ color: String)
    case lightAndDark(_ light: String, _ dark: String)
    case none
}

protocol Opportunity {
    var id: String { get }
    var bannerTitle: String { get }
    var carouselTitle: String { get }
    var textColor: OpportunityColor { get }
    var description: String? { get }
    var messageColor: OpportunityColor { get }
    var action: String? { get }
    var actionTextColor: OpportunityColor { get }
    var image: String { get }
    var deeplink: String { get }
    var backgroundColor: OpportunityColor { get }
}

extension OpportunityItem: Opportunity {
    var backgroundColor: OpportunityColor {
        guard let colorString = cardBackgroundColor.first else {
            return .none
        }
        
        return .light(colorString)
    }
    
    var textColor: OpportunityColor {
        .light(titleColor)
    }
    
    var actionTextColor: OpportunityColor {
        guard let actionColor = actionColor else {
            return .none
        }

        return .light(actionColor)
    }
    
    var messageColor: OpportunityColor {
        guard let descriptionColor = descriptionColor else {
            return .none
        }

        return .light(descriptionColor)
    }
}

extension OpportunityItemV2: Opportunity {
    var backgroundColor: OpportunityColor {
        .lightAndDark(cardBackgroundColor.light, cardBackgroundColor.dark)
    }
    
    var textColor: OpportunityColor {
        .lightAndDark(titleColor.light, titleColor.dark)
    }
    
    var actionTextColor: OpportunityColor {
        .lightAndDark(actionColor.light, actionColor.dark)
    }
    
    var messageColor: OpportunityColor {
        .lightAndDark(descriptionColor.light, descriptionColor.dark)
    }
}
