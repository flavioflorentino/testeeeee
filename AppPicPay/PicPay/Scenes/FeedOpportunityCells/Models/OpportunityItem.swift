struct OpportunityItem: Decodable {
    let id: String
    let bannerTitle: String
    let carouselTitle: String
    let description: String?
    let action: String?
    let image: String
    let deeplink: String
    let titleColor: String
    let descriptionColor: String?
    let actionColor: String?
    let cardBackgroundColor: [String]
}
