struct OpportunityItemV2: Decodable {
    let id: String
    let bannerTitle: String
    let carouselTitle: String
    let description: String?
    let action: String?
    let image: String
    let deeplink: String
    let titleColor: Color
    let descriptionColor: Color
    let actionColor: Color
    let cardBackgroundColor: Color
}

extension OpportunityItemV2 {
    struct Color: Decodable {
        let light: String
        let dark: String
    }
}
