import Core

enum OpportunityServiceEndpoint {
    case list
    case listWithNewColors
    case banners
}

extension OpportunityServiceEndpoint: ApiEndpointExposable {
    var path: String {
        switch self {
        case .list:
            return "offercards/feed"
        case .listWithNewColors:
            return "offercards/feed?newColors=true"
        case .banners:
            return "banners"
        }
    }
}
