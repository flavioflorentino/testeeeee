import Core
import FeatureFlag

protocol OpportunityServicing {
    func opportunities(_ completion: @escaping (Result<[Opportunity], ApiError>) -> Void)
    func opportunitiesV2(_ completion: @escaping (Result<[Opportunity], ApiError>) -> Void)
}

struct OpportunityService {
    typealias Dependencies = HasMainQueue & HasFeatureManager

    // MARK: - Properties
    private let dependencies: Dependencies

    // MARK: - Initialization
    init(dependencies: Dependencies = DependencyContainer()) {
        self.dependencies = dependencies
    }
}

extension OpportunityService: OpportunityServicing {
    func opportunities(_ completion: @escaping (Result<[Opportunity], ApiError>) -> Void) {
        let endpoint = OpportunityServiceEndpoint.listWithNewColors
        Api<[OpportunityItem]>(endpoint: endpoint).execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { result in
            self.dependencies.mainQueue.async {
                completion(
                    result.map { $0.model }
                )
            }
        }
    }
    
    func opportunitiesV2(_ completion: @escaping (Result<[Opportunity], ApiError>) -> Void) {
        let endpoint = OpportunityServiceEndpoint.banners
        Api<[OpportunityItemV2]>(endpoint: endpoint).execute(jsonDecoder: JSONDecoder(.convertFromSnakeCase)) { result in
            self.dependencies.mainQueue.async {
                completion(
                    result.map { $0.model }
                )
            }
        }
    }
}
