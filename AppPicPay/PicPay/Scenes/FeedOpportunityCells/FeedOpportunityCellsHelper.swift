import FeatureFlag

// MARK: - Constants
private extension FeedOpportunityCellsHelper.Constants {
    static let firstCarouselMaxItems: Int = 4
    static let indexStep: Int = 5
}

struct FeedOpportunityCellsHelper {
    fileprivate enum Constants { }

    // MARK: - Properties
    private let opportunities: [Opportunity]

    // MARK: - Initialization
    init(opportunities: [Opportunity] = []) {
        self.opportunities = opportunities
    }

    // MARK: - Merge
    func mergeOpportunities(withFeedItems feedItems: [FeedItem]) -> [FeedItem] {
        let opportunityItems = createOpportunityFeedItems(from: opportunities).filter { $0.opportunities.isNotEmpty }

        guard feedItems.isNotEmpty else {
            return opportunityItems.filter {
                ($0.component == .carousel && $0.carouselPosition == .first) || $0.component == .banner
            }
        }

        let indexesToInsert = indexes(for: opportunityItems, at: feedItems)
        var mergedItems = feedItems

        zip(opportunityItems, indexesToInsert).forEach { opportunityItem, index in
            mergedItems.insert(opportunityItem, at: index)
        }

        return mergedItems
    }
}

// MARK: - Private
private extension FeedOpportunityCellsHelper {
    func createOpportunityFeedItems(from opportunities: [Opportunity]) -> [OpportunityFeedItem] {
        guard opportunities.isNotEmpty else {
            return []
        }

        let firstGroupTotalItems = min(Constants.firstCarouselMaxItems, opportunities.count)
        let firstGroupItems = Array(opportunities.prefix(firstGroupTotalItems))

        guard opportunities.count > firstGroupTotalItems else {
            return [OpportunityFeedItem(carouselPosition: .first, opportunities: firstGroupItems)]
        }

        let middleIndex = firstGroupTotalItems
        let secondGroupItems = [opportunities[middleIndex]]
        let thirdGroupItems = Array(opportunities.dropFirst(middleIndex + 1))

        return [
            OpportunityFeedItem(carouselPosition: .first, opportunities: firstGroupItems),
            OpportunityFeedItem(opportunities: secondGroupItems),
            OpportunityFeedItem(carouselPosition: .second, opportunities: thirdGroupItems)
        ]
    }

    func indexes(for opportunities: [OpportunityFeedItem], at feed: [FeedItem]) -> [Int] {
        var lastIndex = 0

        return (0 ..< opportunities.count).map {
            guard $0 != 0 else {
                lastIndex = 1
                return lastIndex
            }

            var index = max(lastIndex, min(lastIndex + Constants.indexStep, feed.count))
            if lastIndex == index {
                index += 1
            }

            lastIndex = index
            return lastIndex
        }
    }
}
