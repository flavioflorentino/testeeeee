enum BannerFeedItemAction {
    case deeplink(url: URL)
}

protocol BannerFeedItemCoordinating: AnyObject {
    var view: UIView? { get set }
    func perform(action: BannerFeedItemAction)
}

final class BannerFeedItemCoordinator: BannerFeedItemCoordinating {
    weak var view: UIView?
    
    func perform(action: BannerFeedItemAction) {
        switch action {
        case .deeplink(let url):
            DeeplinkHelper.handleDeeplink(withUrl: url)
        }
    }
}
