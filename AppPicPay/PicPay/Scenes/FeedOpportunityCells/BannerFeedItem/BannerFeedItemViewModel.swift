import AnalyticsModule

protocol BannerFeedItemViewModelInputs: AnyObject {
    func fetchData()
    func didTapButton()
    func didTapInsideCard()
}

final class BannerFeedItemViewModel {
    private let presenter: BannerFeedItemPresenting
    
    private let item: Opportunity

    init(presenter: BannerFeedItemPresenting, item: Opportunity) {
        self.presenter = presenter
        self.item = item
    }
}

extension BannerFeedItemViewModel: BannerFeedItemViewModelInputs {
    func fetchData() {
        presenter.setupItemToDisplay(item)
    }
    
    func didTapButton() {
        Analytics.shared.log(FeedOpportunitiesEvent.didTapInsideBanner(over: .button, itemTitle: item.bannerTitle))
        openDeeplink()
    }
    
    func didTapInsideCard() {
        Analytics.shared.log(FeedOpportunitiesEvent.didTapInsideBanner(over: .background, itemTitle: item.bannerTitle))
        openDeeplink()
    }
    
    private func openDeeplink() {
        guard let deeplinkUrl = URL(string: item.deeplink) else {
            return
        }
        presenter.perform(action: .deeplink(url: deeplinkUrl))
    }
}
