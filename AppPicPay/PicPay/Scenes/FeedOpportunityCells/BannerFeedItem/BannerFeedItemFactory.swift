enum BannerFeedItemFactory {
    static func make(withItem item: Opportunity) -> BannerFeedItemView {
        let coordinator: BannerFeedItemCoordinating = BannerFeedItemCoordinator()
        let presenter: BannerFeedItemPresenting = BannerFeedItemPresenter(coordinator: coordinator)
        let viewModel = BannerFeedItemViewModel(presenter: presenter, item: item)
        let view = BannerFeedItemView(viewModel: viewModel)

        coordinator.view = view
        presenter.view = view
        view.loadLabelsAndImage()
        
        return view
    }
}
