import Foundation
import UI
import UIKit

protocol BannerFeedItemDisplay: AnyObject {
    typealias DisplayModel = (
        backgroundColor: UIColor,
        image: URL?,
        title: String,
        titleColor: UIColor,
        message: String?,
        messageColor: UIColor,
        action: NSAttributedString?,
        actionColor: UIColor
    )

    func display(_ model: DisplayModel)
}

protocol BannerFeedItemPresenting: AnyObject {
    var view: BannerFeedItemDisplay? { get set }

    func setupItemToDisplay(_ item: Opportunity)
    func perform(action: BannerFeedItemAction)
}

final class BannerFeedItemPresenter: BannerFeedItemPresenting {
    private let coordinator: BannerFeedItemCoordinating

    weak var view: BannerFeedItemDisplay?

    init(coordinator: BannerFeedItemCoordinating) {
        self.coordinator = coordinator
    }
    
    func setupItemToDisplay(_ item: Opportunity) {
        guard let view = view else {
            return
        }

        let image = URL(string: item.image)
        view.display(
            (
                backgroundColor: item.backgroundColor.color,
                image: image,
                title: item.bannerTitle,
                titleColor: item.textColor.color,
                message: item.description,
                messageColor: item.messageColor.color,
                action: item.actionAttributedText,
                actionColor: item.actionTextColor.color
            )
        )
    }
    
    func perform(action: BannerFeedItemAction) {
        coordinator.perform(action: action)
    }
}

private extension Opportunity {
    var actionAttributedText: NSAttributedString? {
        guard let action = action else {
            return nil
        }

        return NSAttributedString(
            string: action,
            attributes: [
                .underlineStyle: NSUnderlineStyle.single.rawValue,
                .foregroundColor: actionTextColor.color
            ]
        )
    }
}
