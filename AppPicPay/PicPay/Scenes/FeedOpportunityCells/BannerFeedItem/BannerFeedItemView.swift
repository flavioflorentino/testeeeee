import Foundation
import SnapKit
import UI
import UIKit

// MARK: - Layout
private extension BannerFeedItemView.Layout {
    enum Size {
        static let icon = CGSize(width: 64.0, height: 64.0)
        static let contentMinimumHeight: CGFloat = 152.0
    }

    enum Shadow {
        static let offset = CGSize(width: 0.0, height: 1.0)
        static let radius: CGFloat = 2.0
        static let opacity: Float = 0.1
    }
}

final class BannerFeedItemView: UIView {
    fileprivate enum Layout { }

    // MARK: - Properties
    private lazy var shadowView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.layer.shadowColor = Colors.black.color.cgColor
        view.layer.shadowOffset = Layout.Shadow.offset
        view.layer.shadowRadius = Layout.Shadow.radius
        view.layer.shadowOpacity = Layout.Shadow.opacity
        view.layer.masksToBounds = false

        return view
    }()

    private lazy var contentView: UIView = {
        let view = UIView()
        view.viewStyle(RoundedViewStyle())
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapInsideCard)))
        view.isUserInteractionEnabled = true

        return view
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodyPrimaryLabelStyle(type: .highlight))
        label.clipsToBounds = true

        return label
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.labelStyle(BodySecondaryLabelStyle())
        label.clipsToBounds = true

        return label
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView
            .viewStyle(RoundedViewStyle())
            .with(\.cornerRadius, .full)

        return imageView
    }()
    
    private lazy var button: UIButton = {
        let button = UIButton(type: .system)
        button.typography = .bodySecondary(.default)
        button.addTarget(self, action: #selector(tapActionButton), for: .touchUpInside)

        return button
    }()
    
    private let viewModel: BannerFeedItemViewModelInputs

    // Manually compute the height of the view to use it inside a ASCellNode of the Texture library
    var height: CGFloat {
        let contentViewWidth = UIScreen.main.bounds.width - (2.0 * Spacing.base01)
        let targetSize = CGSize(width: contentViewWidth, height: UIView.layoutFittingCompressedSize.height)
        let size = systemLayoutSizeFitting(targetSize)

        return max(Layout.Size.contentMinimumHeight, size.height)
    }

    // MARK: - Initialization
    init(viewModel: BannerFeedItemViewModel) {
        self.viewModel = viewModel
        
        super.init(frame: .zero)

        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func loadLabelsAndImage() {
        viewModel.fetchData()
    }

    // MARK: - Actions
    @objc
    private func tapActionButton() {
        viewModel.didTapButton()
    }
    @objc
    private func tapInsideCard() {
        viewModel.didTapInsideCard()
    }
}

extension BannerFeedItemView: BannerFeedItemDisplay {
    func display(_ model: DisplayModel) {
        contentView.backgroundColor = model.backgroundColor

        imageView.setImage(url: model.image)
        imageView.backgroundColor = model.backgroundColor

        titleLabel.text = model.title
        titleLabel.textColor = model.titleColor
        titleLabel.backgroundColor = model.backgroundColor

        messageLabel.text = model.message
        messageLabel.textColor = model.messageColor
        messageLabel.backgroundColor = model.backgroundColor

        button.setAttributedTitle(model.action, for: .normal)
        button.setTitleColor(model.actionColor, for: .normal)
    }
}

// MARK: - ViewConfiguration
extension BannerFeedItemView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(shadowView)
        shadowView.addSubview(contentView)

        contentView.addSubview(titleLabel)
        contentView.addSubview(messageLabel)
        contentView.addSubview(imageView)
        contentView.addSubview(button)
    }

    func configureViews() {
        backgroundColor = .clear
    }

    func setupConstraints() {
        translatesAutoresizingMaskIntoConstraints = false

        shadowView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(Spacing.base00)
            $0.leading.trailing.equalToSuperview().inset(Spacing.base01)
        }

        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        imageView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-Spacing.base02)
            $0.size.equalTo(Layout.Size.icon)
        }

        titleLabel.snp.makeConstraints {
            $0.top.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.lessThanOrEqualTo(imageView.snp.leading).offset(-Spacing.base01)
        }

        messageLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalTo(titleLabel.snp.leading)
            $0.trailing.lessThanOrEqualTo(imageView.snp.leading).offset(-Spacing.base01)
        }

        button.snp.makeConstraints {
            $0.top.equalTo(messageLabel.snp.bottom).offset(Spacing.base02)
            $0.leading.equalTo(titleLabel.snp.leading)
            $0.trailing.lessThanOrEqualTo(imageView.snp.leading).offset(-Spacing.base01)
            $0.bottom.equalToSuperview().offset(-Spacing.base03)
        }
    }
}
