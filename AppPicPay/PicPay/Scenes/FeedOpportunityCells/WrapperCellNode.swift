import AsyncDisplayKit
import UIKit

final class WrapperCellNode: ASCellNode {
    private let cellView: UIView
    
    init(cellView: UIView, height: CGFloat) {
        self.cellView = cellView
        super.init()
        style.height = ASDimensionMake(height)
        selectionStyle = .none
    }
    
    override func layout() {
        super.layout()
        cellView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.constraintAllEdges(from: cellView, to: view)
    }
    
    override func didLoad() {
        super.didLoad()
        view.addSubview(cellView)
    }
}
