import UIKit
import WebKit
import Core

open class MarkdownView: UIView {
    public var webView: WKWebView?
    
    public var isScrollEnabled: Bool = true {
        didSet {
            webView?.scrollView.isScrollEnabled = isScrollEnabled
        }
    }
    
    public var onTouchLink: ((URLRequest) -> Bool)?
    public var onRendered: ((CGFloat) -> Void)?
    
    struct Colors {
        let textLight: UIColor
        let textDark: UIColor
        let backgroundLight: UIColor
        let backgroundDark: UIColor
    }
    
    public convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    override init (frame: CGRect) {
        super.init(frame: frame)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    var markdownCache: String?
    var enableImageCache: Bool = true
    public func load(markdown: String?, enableImage: Bool = true) {
        markdownCache = markdown
        enableImageCache = enableImage
        webView?.removeFromSuperview()
        
        let bundle = Bundle(for: MarkdownView.self)
        
        guard
            let markdown = markdown,
            let htmlURL = bundle.url(forResource: "index", withExtension: "html", subdirectory: "MarkdownView.bundle")
        else {
            return
        }
        
        let templateRequest = URLRequest(url: htmlURL)
        let markdownWithAppColor = addPaletteColors(in: markdown)
        let escapedMarkdown = self.escape(markdown: markdownWithAppColor) ?? ""
        let imageOption = enableImage ? "true" : "false"
        let script = "window.showMarkdown('\(escapedMarkdown)', \(imageOption));"
        let userScript = WKUserScript(source: script, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        
        let controller = WKUserContentController()
        controller.addUserScript(userScript)
        
        let configuration = WKWebViewConfiguration()
        configuration.userContentController = controller
        
        let wv = WKWebView(frame: bounds, configuration: configuration)
        wv.scrollView.isScrollEnabled = isScrollEnabled
        wv.translatesAutoresizingMaskIntoConstraints = false
        wv.navigationDelegate = self
        addSubview(wv)
        NSLayoutConstraint.constraintAllEdges(from: wv, to: self)
        
        self.webView = wv
        wv.load(templateRequest)
        
        webView?.isOpaque = false
        webView?.backgroundColor = .clear
        backgroundColor = .clear
    }
    
    private func escape(markdown: String) -> String? {
        markdown.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics)
    }
    
    private func addPaletteColors(in markdown: String) -> String {
        var cor = "3D4451"
        if #available(iOS 13.0, *) {
            cor = traitCollection.userInterfaceStyle == .dark ? "#E9ECED" : "#3D4451"
        }
        
        let color = "<font color= '\(cor)'>%@"
        return String(format: color, markdown)
    }
    
    private func redraw() {
        load(markdown: markdownCache, enableImage: enableImageCache)
    }
    
    override open func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        redraw()
    }
}

extension MarkdownView: WKNavigationDelegate {
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        let script = "document.body.offsetHeight;"
        webView.evaluateJavaScript(script) { [weak self] result, error in
            if error != nil { return }
            
            if let height = result as? CGFloat {
                self?.onRendered?(height)
            }
        }
    }
    
    public func webView(
        _ webView: WKWebView,
        decidePolicyFor navigationAction: WKNavigationAction,
        decisionHandler: @escaping (WKNavigationActionPolicy) -> Void
    ) {
        switch navigationAction.navigationType {
        case .linkActivated:
            if let onTouchLink = onTouchLink, onTouchLink(navigationAction.request) {
                decisionHandler(.allow)
            } else {
                decisionHandler(.cancel)
            }
        default:
            decisionHandler(.allow)
        }
    }
    
    public func webView(
        _ webView: WKWebView,
        didReceive challenge: URLAuthenticationChallenge,
        completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void
    ) {
        let pinningHandler = PinningHandler()
        pinningHandler.handle(challenge: challenge, completionHandler: completionHandler)
    }
}
