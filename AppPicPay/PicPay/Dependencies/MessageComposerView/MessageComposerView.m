// MessageComposerView.m
//
// Copyright (c) 2013 oseparovic. ( http://thegameengine.org )
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import <UI/UI.h>
#import "MessageComposerView.h"
#import "PicPay-Swift.h"
@import CoreLegacy;

@interface MessageComposerView()
@property(nonatomic, strong) IBOutlet UITextView *messageTextView;
@end

@implementation MessageComposerView

const int kComposerBackgroundTopPadding = 10;
const int kComposerBackgroundRightPadding = 10;
const int kComposerBackgroundBottomPadding = 10;
const int kComposerBackgroundLeftPadding = 10;
const int kComposerTextViewButtonBetweenPadding = 10;

// Default animation time for 5 <= iOS <= 7. Should be overwritten by first keyboard notification.
float keyboardAnimationDuration = 0.25;
int keyboardAnimationCurve = 7;

@synthesize keyboardOffset;

#define CHAT_POSITION_OFFSET 64

//- (CGFloat)calculatedPositionFromKeyboardFrame{
//	DebugLog(@"%f<<<<%f",self.keyboardY, self.keyboardY - 64);
//	return self.keyboardY - self.frame.size.height;
//}

- (id)initWithFrame:(CGRect)frame {
    return [self initWithFrame:frame andKeyboardOffset:CHAT_POSITION_OFFSET];
}

- (id)initWithFrame:(CGRect)frame andKeyboardOffset:(int)offset {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        keyboardOffset = offset;
        
        // alloc necessary elements
        self.sendButton = [[UIButton alloc] initWithFrame:CGRectZero];
        [self.sendButton addTarget:self action:@selector(sendClicked:) forControlEvents:UIControlEventTouchUpInside];
        self.messageTextView = [[UITextView alloc] initWithFrame:CGRectZero];
        
        // configure elements
        [self setup];
        
        // insert elements above MessageComposerView
        [self insertSubview:self.sendButton aboveSubview:self];
        [self insertSubview:self.messageTextView aboveSubview:self];
	
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setup];
}

- (void)dealloc {
    [self removeNotifications];
}

- (void)setup {
    self.backgroundColor = [PaletteObjc ppColorGrayscale200];
    self.autoresizesSubviews = YES;
    self.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
    self.userInteractionEnabled = YES;
    self.multipleTouchEnabled = NO;
    
    CGRect sendButtonFrame = self.bounds;
    sendButtonFrame.size.width = 60;
    sendButtonFrame.size.height = 34;
    sendButtonFrame.origin.x = self.frame.size.width - kComposerBackgroundRightPadding - sendButtonFrame.size.width;
    sendButtonFrame.origin.y = kComposerBackgroundRightPadding;
    self.sendButton.frame = sendButtonFrame;
    self.sendButton.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
    self.sendButton.layer.cornerRadius = 15;
    [self.sendButton setTitleColor:[PaletteObjc ppColorGrayscale000] forState:UIControlStateNormal];
    [self.sendButton setTitleColor:[PaletteObjc ppColorBranding300] forState:UIControlStateHighlighted];
    [self.sendButton setTitleColor:[PaletteObjc ppColorGrayscale300] forState:UIControlStateSelected];
    [self.sendButton setBackgroundColor:[PaletteObjc ppColorBranding300]];
    [self.sendButton setTitle:@"Enviar" forState:UIControlStateNormal];
    self.sendButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    
    CGRect messageTextViewFrame = self.bounds;
    messageTextViewFrame.origin.x = kComposerBackgroundLeftPadding;
    messageTextViewFrame.origin.y = kComposerBackgroundTopPadding;
    messageTextViewFrame.size.width = self.frame.size.width - kComposerBackgroundLeftPadding - kComposerTextViewButtonBetweenPadding - sendButtonFrame.size.width - kComposerBackgroundRightPadding;
    messageTextViewFrame.size.height = 40;
    self.messageTextView.frame = messageTextViewFrame;
    self.messageTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    self.messageTextView.showsHorizontalScrollIndicator = NO;
    self.messageTextView.layer.cornerRadius = 15;
	self.messageTextView.scrollsToTop = NO;
    self.messageTextView.font = [UIFont systemFontOfSize:14];
    self.messageTextView.textColor = [PaletteObjc ppColorGrayscale500];
    self.messageTextView.backgroundColor = [PaletteObjc ppColorGrayscale000];
    
    self.keyboardOffset = CHAT_POSITION_OFFSET;
//	@try {
//		[self.messageTextView setAutocorrectionType:UITextAutocorrectionTypeNo];
//		[self.messageTextView setKeyboardType:UIKeyboardTypeDefault];
//	}
//	@catch (NSException *exception) {
//		
//	}
    self.messageTextView.delegate = self;
    
    [self addNotifications];
    [self resizeTextViewForText:@"" animated:NO];
}

- (void)layoutSubviews {
    // Due to inconsistent handling of rotation when receiving UIDeviceOrientationDidChange notifications
    // ( see http://stackoverflow.com/q/19974246/740474 ) rotation handling is done here.
    CGFloat fixedWidth = self.messageTextView.frame.size.width;
    CGSize oldSize = self.messageTextView.frame.size;
    CGSize newSize = [self.messageTextView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    
    if (oldSize.height == newSize.height) {
        // In cases where the height remains the same after a rotation (AKA number of lines does not change)
        // this code is needed as resizeTextViewForText will not do any configuration.
        CGRect frame = self.frame;
        frame.origin.y = ([self currentScreenSize].height - [self currentKeyboardHeight]) - frame.size.height - keyboardOffset;
//		frame.origin.y = 450;
        self.frame = frame;
        
        // Even though the height didn't change the origin did so notify delegates
        if (self.delegate && [self.delegate respondsToSelector:@selector(messageComposerFrameDidChange:withAnimationDuration:)]) {
            [self.delegate messageComposerFrameDidChange:frame withAnimationDuration:keyboardAnimationDuration];
        }
    } else {
        // The view is already animating as part of the rotationso we just have to make sure it
        // snaps to the right place and resizes the textView to wrap the text with the new width. Changing
        // to add an additional animation will overload the animation and make it look like someone is
        // shuffling a deck of cards.
        [self resizeTextViewForText:self.messageTextView.text animated:NO];
    }
}


#pragma mark - NSNotification
- (void)addNotifications {
    NSNotificationCenter* defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self selector:@selector(textViewTextDidChange:) name:UITextViewTextDidChangeNotification object:self.messageTextView];
    [defaultCenter addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
}

- (void)removeNotifications {
    NSNotificationCenter* defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter removeObserver:self name:UITextViewTextDidChangeNotification object:self.messageTextView];
    [defaultCenter removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}


#pragma mark - UITextViewDelegate
- (void)textViewTextDidChange:(NSNotification*)notification {
    NSString* newText = self.messageTextView.text;
    [self resizeTextViewForText:newText animated:YES];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(messageComposerUserTyping)])
        [self.delegate messageComposerUserTyping];
}

- (void)textViewDidBeginEditing:(UITextView*)textView {
    CGRect frame = self.frame;
    frame.origin.y = ([self currentScreenSize].height - [self currentKeyboardHeight]) - frame.size.height - keyboardOffset;
    
    [UIView animateWithDuration:keyboardAnimationDuration
                          delay:0.0
                        options:(keyboardAnimationCurve << 16)
                     animations:^{self.frame = frame;}
                     completion:nil];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(messageComposerFrameDidChange:withAnimationDuration:)]) {
        [self.delegate messageComposerFrameDidChange:frame withAnimationDuration:keyboardAnimationDuration];
    }
}

- (void)textViewDidEndEditing:(UITextView*)textView {
    CGRect frame = self.frame;
    frame.origin.y = [self currentScreenSize].height - self.frame.size.height - keyboardOffset;
    
    [UIView animateWithDuration:keyboardAnimationDuration
                          delay:0.0
                        options:(keyboardAnimationCurve << 16)
                     animations:^{self.frame = frame;}
                     completion:nil];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(messageComposerFrameDidChange:withAnimationDuration:)]) {
        [self.delegate messageComposerFrameDidChange:frame withAnimationDuration:keyboardAnimationDuration];
    }
}

#define MAX_LENGTH 500
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSUInteger newLength = (textView.text.length - range.length) + text.length;
    if(newLength <= MAX_LENGTH)
    {
        return YES;
    } else {
        NSUInteger emptySpace = MAX_LENGTH - (textView.text.length - range.length);
        textView.text = [[[textView.text substringToIndex:range.location]
						  stringByAppendingString:[text substringToIndex:emptySpace]]
						 stringByAppendingString:[textView.text substringFromIndex:(range.location + range.length)]];
        return NO;
    }
}

#pragma mark - Keyboard Notifications
- (void)keyboardWillShow:(NSNotification*)notification {
    // Because keyboard animation time and cure vary by iOS version, and we don't want to build the library
    // on top of spammy keyboard notifications we use UIKeyboardWillShowNotification ONLY to dynamically set our
    // animation duration. As a UIKeyboardWillShowNotification is fired BEFORE textViewDidBeginEditing
    // is triggered we can use the following values for all of animations including the first.
    keyboardAnimationDuration = [[notification userInfo][UIKeyboardAnimationDurationUserInfoKey] floatValue];
    keyboardAnimationCurve = [[notification userInfo][UIKeyboardAnimationCurveUserInfoKey] intValue];
}


#pragma mark - TextView Frame Manipulation
- (void)resizeTextViewForText:(NSString*)text {
    [self resizeTextViewForText:text animated:NO];
}

- (void)resizeTextViewForText:(NSString*)text animated:(BOOL)animated {
    CGFloat fixedWidth = self.messageTextView.frame.size.width;
    CGSize oldSize = self.messageTextView.frame.size;
    CGSize newSize = [self.messageTextView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
	BOOL reachedLimit = newSize.height > 120;
	
	
    // If the height doesn't need to change skip reconfiguration.
    if (oldSize.height == newSize.height) {
        return;
    }
	
	if (reachedLimit){
		newSize.height = oldSize.height;
    }
	
    // Recalculate MessageComposerView container frame
    CGRect newContainerFrame = self.frame;
    newContainerFrame.size.height = newSize.height + kComposerBackgroundTopPadding + kComposerBackgroundBottomPadding;
    newContainerFrame.origin.y = ([self currentScreenSize].height - [self currentKeyboardHeight]) - newContainerFrame.size.height - keyboardOffset;;
	
    // Recalculate send button frame
    CGRect newSendButtonFrame = self.sendButton.frame;
    newSendButtonFrame.origin.y = newContainerFrame.size.height - (kComposerBackgroundBottomPadding + newSendButtonFrame.size.height);
	
	// Recalculate UITextView frame
    CGRect newTextViewFrame = self.messageTextView.frame;
    newTextViewFrame.size.height = newSize.height;
    newTextViewFrame.origin.y = kComposerBackgroundTopPadding;
    
    if (animated) {
        [UIView animateWithDuration:keyboardAnimationDuration
                              delay:0
                            options:UIViewAnimationOptionAllowUserInteraction
                         animations:^{
							 self.frame = newContainerFrame;
							 self.sendButton.frame = newSendButtonFrame;
							 self.messageTextView.frame = newTextViewFrame;
							 if (!reachedLimit) {
								 [self.messageTextView setContentOffset:CGPointMake(0, 0) animated:YES];
							 }
                         }
                         completion:nil];
    } else {
		self.frame = newContainerFrame;
		self.sendButton.frame = newSendButtonFrame;
		self.messageTextView.frame = newTextViewFrame;
		if (!reachedLimit) {
			[self.messageTextView setContentOffset:CGPointMake(0, 0) animated:YES];
		}
    }
	
    if (self.delegate && [self.delegate respondsToSelector:@selector(messageComposerFrameDidChange:withAnimationDuration:)]) {
        [self.delegate messageComposerFrameDidChange:newContainerFrame withAnimationDuration:keyboardAnimationDuration];
    }
	
//	if (reachedLimit){
//		[self scrollTextViewToBottom];
//	}
}

- (void)scrollTextViewToBottom {
    [self.messageTextView scrollRangeToVisible:NSMakeRange([self.messageTextView.text length], 0)];
}


#pragma mark - IBAction
- (IBAction)sendClicked:(id)sender {
	
	self.messageTextView.text = [NSString stringWithFormat:@"%@ ", self.messageTextView.text];
	
    if(self.delegate) {
        [self.delegate messageComposerSendMessageClickedWithMessage:[self.messageTextView.text substringToIndex:[self.messageTextView.text length]-1]];
    }
    
    [self.messageTextView setText:@""];
    // Manually trigger the textViewDidChange method as setting the text when the messageTextView is not first responder the
    // UITextViewTextDidChangeNotification notification does not get fired.
    [self textViewTextDidChange:nil];
}


#pragma mark - Utils
- (UIInterfaceOrientation)currentInterfaceOrientation {
    // Returns the orientation of the Interface NOT the Device. The two do not happen in exact unison so
    // this point is important.
    return [UIApplication sharedApplication].statusBarOrientation;
}

- (float)currentKeyboardHeight {
    if ([self.messageTextView isFirstResponder]) {
//        return [self currentKeyboardHeightInInterfaceOrientation:[self currentInterfaceOrientation]];
		return self.keyboardHeight;
    } else {
        return 0;
    }
}

- (float)currentKeyboardHeightInInterfaceOrientation:(UIInterfaceOrientation)orientation {
    // TODO: this is very bad... another solution is needed or this will break on international keyboards etc.
    if (UIInterfaceOrientationIsLandscape(orientation)) {
        return 162;
    } else {
        return 216;
    }
}

- (CGSize)currentScreenSize {
    CGFloat bottomSafeInset = 0;
    
    if (@available(iOS 11.0, *)) {
        bottomSafeInset = self.superview.safeAreaInsets.bottom;
    }
    
    // return the screen size with respect to the orientation
    CGSize size = [self currentScreenSizeInInterfaceOrientation:[self currentInterfaceOrientation]];
    size.height -= bottomSafeInset;
    return size;
}

- (CGSize)currentScreenSizeInInterfaceOrientation:(UIInterfaceOrientation)orientation {
    // http://stackoverflow.com/a/7905540/740474
    CGSize size = [UIScreen mainScreen].bounds.size;
    return size;
}

- (void)startEditing {
    [self.messageTextView becomeFirstResponder];
}

- (void)finishEditing {
    [self.messageTextView resignFirstResponder];
}

@end
