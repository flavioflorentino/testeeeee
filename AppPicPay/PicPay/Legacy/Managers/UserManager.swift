import Core

protocol UserManagerContract {
    var isAuthenticated: Bool { get }
    var token: String? { get }
    
    func updateToken(_ token: String)
    func deleteToken()
}

final class User: NSObject {
    typealias Dependencies = HasKeychainManager
    private static let dependencies: Dependencies = DependencyContainer()
    
    @objc
    static var isAuthenticated: Bool {
        !token.isEmpty
    }
    
    @objc
    static var token: String? {
        dependencies.keychain.getData(key: KeychainKey.token)
    }
    
    @objc
    static func updateToken(_ token: String) {
        dependencies.keychain.set(key: KeychainKey.token, value: token)
    }
    
    @objc
    static func deleteToken() {
        dependencies.keychain.clearValue(key: KeychainKey.token)
    }
}

extension User: UserManagerContract {
    var isAuthenticated: Bool {
        User.isAuthenticated
    }
    
    var token: String? {
        User.token
    }
    
    func updateToken(_ token: String) {
        User.updateToken(token)
    }
    
    func deleteToken() {
        User.deleteToken()
    }
}
