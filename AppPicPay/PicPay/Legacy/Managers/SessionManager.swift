import Core
import DirectMessageSB
import FeatureFlag
import Foundation
import UI
import UIKit

final class SessionManager: NSObject {
    @objc static let sharedInstance = SessionManager()
    var currentCoordinator: Coordinator?
    var currentCoordinating: Coordinating?

    fileprivate var sessionNotificationHandler: SessionNotificationHandler
    
    fileprivate override init() {
        self.sessionNotificationHandler = SessionNotificationHandler()
        super.init()
    }
    
    /**
     Allows execute a block of code after all
     session data is loaded.
     */
    func executeBlockAfterLoadInitialData(_ completion: @escaping ((_ success: Bool, _ error: Error?) -> Void)) {
        DispatchQueue.global(qos: .userInteractive).async {
            ConsumerManager.shared.loadConsumerData { success, error in
                completion(success, error)
            }
        }
    }
    
    /**
     Load all initial data necessary to the correct working of the app. Load:
     - AllConsumerData
     */
    @objc
    func loadInitialData() {
        executeBlockAfterLoadInitialData { _, error in
            guard let error = error else {
                return
            }
            debugPrint(error.localizedDescription)
        }
    }
    
    func showStore() {
        dismissCurrentModal()
        AppManager.shared.mainScreenCoordinator?.showStoreScreen()
    }
    
    func openDigitalGoods(dgItem: DGItem, from viewController: UIViewController? = nil, redirect: Bool = true) -> Bool {
        let navigation = PPNavigationController(rootViewController: DGProxyViewController(with: dgItem.id))
        if let controller = viewController {
            presentDismissingCurrentModal(from: controller, to: navigation)
            return true
        }

        if redirect {
            SessionManager.sharedInstance.showStore()
        }

        guard let paymentNavigation = AppManager.shared.mainScreenCoordinator?.paymentSearchNavigation() else {
            return false
        }
        presentDismissingCurrentModal(from: paymentNavigation, to: navigation)
        return true
    }

    @objc
    func getPaymentNavigation() -> UINavigationController? {
        guard let mainScreenCoordinator = AppManager.shared.mainScreenCoordinator else {
            return nil
        }
        let paymentSearchNavigation = mainScreenCoordinator.paymentSearchNavigation()

        paymentSearchNavigation.popToRootViewController(animated: false)
        return paymentSearchNavigation
    }

    @objc
    func openNotification(_ notification: PPNotification) {
        if !User.isAuthenticated {
            return
        }
        
        DispatchQueue.main.async {
            self.dismissCurrentModal()

            guard let mainScreenCoordinator = AppManager.shared.mainScreenCoordinator else {
                return
            }

            let notificationsNavigation = mainScreenCoordinator.notificationsNavigation()
            
            // change to notifications tab
            AppManager.shared.mainScreenCoordinator?.showNotificationsScreen()
            
            if let notificationsController = notificationsNavigation.viewControllers.first as? NotificationsViewController {
                mainScreenCoordinator.showNotificationsScreen()
                notificationsController.open(notification)
                return
            }
        }
    }
    
    func openSendBirdNotification(_ notification: ChatNotification) {
        DispatchQueue.main.async {
            guard User.isAuthenticated,
                  let mainScreenCoordinator = AppManager.shared.mainScreenCoordinator,
                  let channelURL = notification.channel?.channelURL
            else { return }
            
            self.dismissCurrentModal()
            mainScreenCoordinator.showHomeScreen()

            let chatViewController = ChatSetupFactory.make(for: .url(channelURL))
            guard let presentingChatViewController = mainScreenCoordinator.currentController() as? ChatViewController else {
                mainScreenCoordinator.homeNavigation().pushViewController(chatViewController, animated: true)
                return
            }
            if presentingChatViewController.canOpenChat(url: channelURL) { return }
            mainScreenCoordinator.homeNavigation().pushReplacingLastController(chatViewController, animated: true)
        }
    }
    
    @objc
    func openSpotlight(contactWsid: String) {
        if !User.isAuthenticated {
            return
        }
        
        DispatchQueue.main.async { [weak self] in
            self?.dismissCurrentModal()
            // change to notifications tab
            AppManager.shared.mainScreenCoordinator?.showPaymentSearchScreen(searchText: nil, page: nil, ignorePermissionPrompt: true)
            
            if let vc = NewTransactionViewController.fromStoryboard() {
                vc.touchOrigin = "spotlight"
                vc.loadConsumerInfo(contactWsid)
                vc.showCancel = true
                let nav = PPNavigationController(rootViewController: vc)
                AppManager.shared.mainScreenCoordinator?.paymentSearchNavigation().present(nav, animated: true, completion: nil)
            }
        }
    }
}

// MARK: - Navigation
extension SessionManager {
    func dismissCurrentModal() {
        guard let currentController = AppManager.shared.mainScreenCoordinator?.currentController() else { return }
        if let presentedViewController = currentController.presentedViewController {
            presentedViewController.dismiss(animated: false)
        } else {
            currentController.dismiss(animated: false)
        }
    }

    private func presentDismissingCurrentModal(from currentController: UIViewController,
                                               to nextController: UIViewController,
                                               animated: Bool = true,
                                               completion: (() -> Void)? = nil) {
        dismissCurrentModal()
        currentController.present(nextController, animated: animated, completion: completion)
    }
}

// MARK: - Notifications Handlers

/**
 This class is responsible to handle the sessions notifications
 */
final class SessionNotificationHandler {
    fileprivate var isBusyWithLogout = false
    
    init() {
        // Notification informed that the application needs to log out
        NotificationCenter.default.addObserver(self, selector: #selector(self.userNotAuthenticatedHandler(notificacion:)), name: NSNotification.Name(rawValue: "WSUserNotAuthenticated"), object: nil)
        
        // Notification informed that the current authenticated user is blocked
        NotificationCenter.default.addObserver(self, selector: #selector(self.userIsBlockedHandler), name: NSNotification.Name(rawValue: "WSUserIsBlocked"), object: nil)
    }
    
    /**
     Handler to not authenticated notification
     This handler performe the user logout.
     
     **OBS.**: The notification WSUserNotAuthenticated is send when the server informe that the current
     token is not valid. For more details see the class `WSWebServiceInterface`
     */
    @objc
    func userNotAuthenticatedHandler(notificacion: NSNotification) {
        // isBusyWithLogout Prevent perform logout twice or more while the logout is running
        if !isBusyWithLogout {
            logAutomaticLogout()
            let error = notificacion.userInfo?["error"] as? PicPayErrorDisplayable
            // topViewController() and logout() must be called from the main thread
            DispatchQueue.main.async { [weak self] in
                if self?.isBusyWithLogout == false {
                    self?.logAutomaticLogout666()

                    guard let topViewController = ViewsManager.topViewController() else {
                        self?.logoutUser()
                        return
                    }
                
                    //check if the view is not public
                    if self?.isNotPublicVCForUserNotAuthenticated(topViewController) == true {
                        self?.logoutUser(error: error, topViewController: topViewController)
                    }
                }
            }
        }
    }
    
    func isNotPublicVCForUserNotAuthenticated(_ controller: UIViewController) -> Bool {
        let typeList = [
            SignUpViewController.self,
            PersonalDetailsViewController.self,
            NotificationsViewController.self
        ]
        
        return controller.isNotOfType(inList: typeList) && !(controller.navigationController is PPPublicNavigationController)
    }
    
    /**
     Handler to blocked user notification
     This handler performe the user logout.
     
     **OBS.**: The notification WSUserNotAuthenticated is send when the server informe that the current
     token is not valid. For more details see the class `WSUserIsBlocked`
     */
    @objc
    func userIsBlockedHandler() {
        // _isBussyLogout Prevent perform logout twice or more while the logout is running
        if !isBusyWithLogout {
            // topViewController() and logout() must be called from the main thread
            DispatchQueue.main.async { [weak self] in
                if self?.isBusyWithLogout == false {
                    self?.logAutomaticLogout666()

                    guard let topViewController = ViewsManager.topViewController() else {
                        self?.logoutUser()
                        return
                    }
                    
                    if self?.isNotPublicVCForUserBlocked(topViewController) == true {
                        self?.logoutUser()
                    }
                }
            }
        }
    }
    
    func isNotPublicVCForUserBlocked(_ controller: UIViewController) -> Bool {
        return !(controller is SignUpViewController) && !(controller.navigationController is PPPublicNavigationController)
    }
    
    /**
     Perform user logout and show an alert if topViewController and error are known.
     */
    private func logoutUser(error: PicPayErrorDisplayable? = nil, topViewController: UIViewController? = nil) {
        isBusyWithLogout = true
        if error != nil {
            AlertMessage.showAlert(error, controller: topViewController)
        }
        AuthManager.shared.logout(fromContext: nil) { _ in
            #if DEBUG            
            if isRunningTests {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.isBusyWithLogout = false
                }
                return
            }
            #endif

            self.isBusyWithLogout = false
        }
    }
}

extension SessionNotificationHandler {
    private func logAutomaticLogout() {
        PPAnalytics.trackEvent("AutomaticLogout", properties: nil)
    }
    
    private func logAutomaticLogout666() {
        PPAnalytics.trackEvent("AutomaticLogoutUserBlocked", properties: nil)
    }
}
