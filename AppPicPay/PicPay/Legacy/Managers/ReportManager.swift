//
//  ReportControllerFactory.swift
//  PicPay
//
//  ☝️☁️ Created by Luiz Henrique Guimarães on 27/06/17.
//
//

import Foundation

final class ReportManager {
    
    /// It create a new controller for the current/next flow step
    /// - Parameters:
    ///   - model: report View Model
    ///   - next: next report screen
    fileprivate static func nextController(model:ReportViewModel, next:PPReportFlow? = nil) -> ReportBaseViewController?{
        
        let nextFlow = next ?? model.flow
        
        switch nextFlow.screenType {
            
        case .end:
            let vc:ReportEndViewController = UIStoryboard.viewController(.report)
            vc.model = ReportViewModel(flow: nextFlow, data: model.data, completion: model.completion)
            vc.model?.step = model.step + 1
            return vc
            
        case .subitems:
            let vc: ReportOptionsViewController = UIStoryboard.viewController(.report)
            vc.model = ReportViewModel(flow: nextFlow, data: model.data, completion: model.completion)
            vc.model?.step = model.step + 1
            return vc
            
        case .message:
            let vc: ReportMessageViewController = UIStoryboard.viewController(.report)
            vc.model = ReportViewModel(flow: nextFlow, data: model.data, completion: model.completion)
            vc.model?.step = model.step + 1
            return vc
            
        default:
            return nil
        }
    }
    
    /// Push the next view controller
    static func nextFlowStep(from currentController: UIViewController, model:ReportViewModel, next:PPReportFlow?) {
        
        let nextController = ReportManager.nextController(model: model, next: next)
        if nextController == nil || (next != nil && next!.screenType == .end) {
            
            ReportManager.sendReport(from: currentController, model: model, step: next!, nextController: nextController)
        }else{
            currentController.navigationController?.pushViewController(nextController!, animated: true)
        }
        
    }
    
    static func sendReport(from currentController: UIViewController, model:ReportViewModel, step:PPReportFlow, nextController: UIViewController?) {
        // If it`s the last screen show the loader and send report
        if let presenter = currentController as? ReportLoadingPresenter {
            presenter.startReportLoading()
        }
        
        model.sendReport(step: step) { (step, success, error) in
            if let presenter = currentController as? ReportLoadingPresenter {
                presenter.stopReportLoading()
            }
            // If failed, show alert
            if !success {
                AlertMessage.showAlert(withMessage: error?.localizedDescription, controller: currentController)
                return
            }
            
            
            guard let nextController = nextController else {
                model.completion?(error, step)
                return
            }
            
            currentController.navigationController?.pushViewController(nextController, animated: true)
        }
    }

    
    typealias ReportCompletion = ((_ error:Error?, _ flow: PPReportFlow) -> Void)
    static func startReportFlow(from currentController: UIViewController, type: PPReportRequest.type, referedId:String,_ completion: @escaping ReportCompletion ){
        if let presenter = currentController as? ReportLoadingPresenter {
            presenter.startReportLoading()
        }
        let data = PPReportRequest.init(reportType: type, reportedId: referedId)
        WSReport.getReportFlow(data: data) { (reportFlow, error) in
            DispatchQueue.main.async {
                
                if let presenter = currentController as? ReportLoadingPresenter {
                    presenter.stopReportLoading()
                }
                
                guard let flow = reportFlow else {
                    if let error = error {
                        AlertMessage.showAlert(withMessage: error.localizedDescription, controller: currentController)
                    }
                    return
                }
                
                let reportData = PPReportRequest(reportType: type, reportedId: referedId)
                let model = ReportViewModel(flow: flow, data: reportData, completion: completion)
                
                if flow.screenType == .begin {
                    ReportManager.presentBeginAlert(from: currentController, model: model)
                }else{
                    ReportManager.nextFlowStep(from: currentController, model: model, next: flow)
                }
            }
        }
    }
    
    static func presentBeginAlert(from currentController: UIViewController, model:ReportViewModel) {
        let alert = UIAlertController(title: model.flow.title, message: model.flow.text, preferredStyle: .alert)
        for flow in model.flow.items {
            let action = UIAlertAction(title: flow.title, style: .default, handler: { (action) in
                ReportManager.nextFlowStep(from: currentController, model: model, next: flow.next ?? flow)
            })
            alert.addAction(action)
        }
        let cancel = UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel, handler: nil)
        alert.addAction(cancel)
        
        currentController.present(alert, animated: true)
    }
}

protocol ReportLoadingPresenter: AnyObject {
    func startReportLoading()
    func stopReportLoading()
}
