import Core
import Foundation
import SwiftyJSON

protocol CreditCardManagerContract {
    var cardList: [CardBank] { get set }
    var defaultCreditCardId: Int { get }
    var defaultCreditCard: CardBank? { get }
    var isOcurredErrorOnFirstLoad: Bool { get }
    
    func defaultDebitCard() -> CardBank?
    func deleteCard(id: String)
    func saveDefaultDebitCard(_ id: String)
    func addCard(_ card: CardBank)
    func loadCardList(_ completion: @escaping (Error?) -> Void)
    func saveDefaultCreditCardId(_ id: Int)
    func updateDefaultCreditCardIfNil()
    func cardWithId(_ id: String) -> CardBank?
}

final class CardsCountForObjc: NSObject {
    @objc static var count: Int {
        return CreditCardManager.shared.cardList.count
    }
}

final class CreditCardManager: NSObject, CreditCardManagerContract {
    
    @objc static let shared: CreditCardManager = CreditCardManager()
    var cardList: [CardBank] = []
    
    @objc var defaultCreditCardId: Int {
        get {
            let id = KVStore().intFor(.defaultCreditCardId)
            guard id != 0 else {
                let cardId = firstCardId(type: [.credit, .creditDebit])
                return cardId ?? 0
            }
            
            return id
        }
    }
    
    var defaultCreditCard: CardBank? {
        get {
            let id = KVStore().intFor(.defaultCreditCardId)
            guard let card = cardList.first(where: { $0.id == "\(id)" }) else {
                return firstCard(type: [.credit, .creditDebit])
            }
            return card
        }
    }
    
    func defaultDebitCard() -> CardBank? {
        let id = KVStore().stringFor(.defaultDebitCardId)
        guard let card = cardList.first(where: { $0.id == id }) else {
            return firstCard(type: [.debit, .creditDebit])
        }
        return card
    }
    
    func deleteCard(id: String) {
        cardList.removeAll(where: { $0.id == id })
    }
    
    private func firstCardId(type: [CardBank.CardType]) -> Int? {
        let card = firstCard(type: type)
        return Int(card?.id ?? "0")
    }
    
    private func firstCard(type: [CardBank.CardType]) -> CardBank? {
        return cardList.first(where: { type.contains($0.type) })
    }
    
    func saveDefaultDebitCard(_ id: String) {
        KVStore().setString(id, with: .defaultDebitCardId)
    }
    
    func addCard(_ card: CardBank) {
        cardList.append(card)
    }
    
    @objc public var isOcurredErrorOnFirstLoad: Bool = false
    
    /// Load card list in payment methods
    ///
    /// - Parameter completion: completion hange with error and reponse
    @objc public func loadCardList(_ completion: @escaping (Error?) -> Void) {
        let api = ApiPaymentMethods()
        api.cardsList(onCacheLoaded: { [weak self] (paymentMethods) in
            guard let strongSelf = self else {
                return
            }
            // To prevent replace with zero
            if !paymentMethods.paymentMethods.isEmpty && strongSelf.cardList.isEmpty {
                strongSelf.cardList = paymentMethods.paymentMethods
                completion(nil)
            }
        }, completion: { [weak self] (result) in
                guard let strongSelf = self else {
                    return
                }
                switch result {
                case .success(let paymentMethods):
                    strongSelf.cardList = paymentMethods.paymentMethods
                    strongSelf.updateDefaultCreditCardIfNil()
                    strongSelf.isOcurredErrorOnFirstLoad = false
                    completion(nil)
                    break
                case .failure(let error):
                    strongSelf.isOcurredErrorOnFirstLoad = true
                    completion(error)
                    break
                }
        })
    }
    
    /// Save default credit card Id in user default
    ///
    /// - Parameter id: id of credit card
    @objc public func saveDefaultCreditCardId(_ id: Int) {
        KVStore().setInt(id, with: .defaultCreditCardId)
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: Notification.Name.Payment.methodChange, object: nil)
        }
    }
    
    /// Set any available credit card as defauld if the current default credit card is invalid
    func updateDefaultCreditCardIfNil() {
        if defaultCreditCard == nil {
            guard let first = cardList.first, let cardId = Int(first.id) else {
                return
            }
            saveDefaultCreditCardId(cardId)
        }
    }
    
    func cardWithId(_ id: String) -> CardBank? {
        return cardList.first(where: { $0.id == "\(id)" })
    }
    
    func clearCards() {
        cardList.removeAll()
        KVStore().removeObjectFor(.defaultCreditCardId)
        KVStore().removeObjectFor(.defaultDebitCardId)
    }
}
