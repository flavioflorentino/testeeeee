import Core
import Cache
import SwiftyJSON
import UIKit

final class CacheManagerOBJC: NSObject {
    @objc static let shared: CacheManagerOBJC = {
        let instance = CacheManagerOBJC()
        return instance
    }()
    
    private var appVersionNumber: String = ""
    public var cacheStorageString: Storage<String>?
    public var cacheStorageData: Storage<Data>?
    
    private override init() {
        let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        self.appVersionNumber = versionNumber?.replacingOccurrences(of: ".", with: "_") ?? ""
        
        // cache
        let diskConfig = DiskConfig(name: "_Cache_v\(self.appVersionNumber)", expiry: .date(Date().addingTimeInterval(2 * 3600)), maxSize: 10000)
        let memoryConfig = MemoryConfig(expiry: .never, countLimit: 10, totalCostLimit: 10)
        
        do {
            let storageString = try Storage(diskConfig: diskConfig, memoryConfig: memoryConfig, transformer: TransformerFactory.forCodable(ofType: String.self))
            let storageData = try Storage(diskConfig: diskConfig, memoryConfig: memoryConfig, transformer: TransformerFactory.forData())
            cacheStorageString = storageString
            cacheStorageData = storageData
        } catch {
            debugPrint(error)
        }
    }
    
    // MARK: - Public Methods
    
    @objc
    func setApiResponseCache(withDict data: NSDictionary, key: String) {
        let cacheKey = appendUserId(to: key)
        guard let theJSONData = try? JSONSerialization.data( withJSONObject: data, options: []) else {
            return
        }
        if let theJSONText = String(data: theJSONData, encoding: .utf8) {
            cacheStorageString?.async.setObject(theJSONText, forKey: cacheKey, completion: { _ in })
        }
    }
    
    @objc
    func getApiResponseCache(forKey key: String, onLoad: @escaping ([AnyHashable: Any]) -> Void) {
        let cacheKey = appendUserId(to: key)
        cacheStorageString?.async.object(forKey: cacheKey) { result in
            switch result {
            case .value(let jsonString):
                guard let data = jsonString.data(using: String.Encoding.utf8) else {
                    return
                }
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: data, options: []) as? [AnyHashable: AnyObject]
                    guard let json = jsonData else {
                        return
                    }
                    onLoad(json)
                } catch let error as NSError {
                    debugPrint(error)
                }
            case .error:
                break
            }
        }
    }
    
    func setApiResponseFromCache(with data: Data, forKey key: String) {
        let cacheKey = appendUserId(to: key)
        cacheStorageData?.async.setObject(data, forKey: cacheKey, completion: { _ in })
    }
    
    func getApiResponseFromCache<T: BaseApiResponse>(forkey key: String, onCacheLoaded: @escaping (T) -> Void, onNotFound: (() -> Void)? = nil) {
        data(for: key, onCacheLoaded: { data in
            if let json = try? JSON(data: data), let data = T(json: json) {
                onCacheLoaded(data)
            } else {
                onNotFound?()
            }
        }, onNotFound: {
            onNotFound?()
        })
    }
    
    func getApiResponseFromCacheCodable<T: Decodable>(forkey key: String, onCacheLoaded: @escaping (T) -> Void, onNotFound: (() -> Void)? = nil) {
        data(for: key, onCacheLoaded: { data in
            if let data = try? JSONDecoder().decode(T.self, from: data) {
                onCacheLoaded(data)
            } else {
                onNotFound?()
            }
        }, onNotFound: {
            onNotFound?()
        })
    }

    func data(for key: String, onCacheLoaded: @escaping (_ data: Data) -> Void, onNotFound: @escaping () -> Void) {
        let cacheKey = appendUserId(to: key)
        cacheStorageData?.async.object(forKey: cacheKey) { result in
            switch result {
            case .value(let dataCache):
                onCacheLoaded(dataCache)
            case .error:
                onNotFound()
            }
        }
    }
    
    @objc
    func removeAllCacheObjects() {
        do {
            try cacheStorageString?.removeAll()
            try cacheStorageData?.removeAll()
        } catch {
            debugPrint(error)
        }
    }
    
    private func appendUserId(to key: String) -> String {
        if let userId = ConsumerManager.shared.consumer?.wsId {
            return "\(userId)_\(key)"
        }
        return key
    }
}
