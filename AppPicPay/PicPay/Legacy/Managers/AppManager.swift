import Advertising
import AnalyticsModule
import Core
import CoreLegacy
import FeatureFlag
import UIKit

protocol AppManagerContract {
    var sessionData: AppManager.SessionData { get }
    var mainScreenCoordinator: MainScreenCoordinator? { get }
       
    func getUnreadNotificationsCount() -> Int
    func setUnreadNotificationsCount(newCount: Int)

    func setUnreadWalletNotificationsLabel(label: String)
}

final class AppManager: NSObject, AppManagerContract {
    // MARK: - Singleton
    @objc
    static let shared = AppManager()
    
    override init() {
        self.sessionData = SessionData()
        super.init()
        
        setupObservers()
        getBadgesNotification()
    }
    
    private var unreadNotificationsCount = 0
    private(set) var unreadWalletNotificationsLabel = ""
    private let newUnreadNotificationKey = Notification.Name(rawValue: "newUnreadNotificationsCount")
    
    // MARK: - Private Methods
    
    /// Configure all abservables
    private func setupObservers() {
        NotificationCenter.default.addObserver(
            forName: ApiNotificationsName.syncData,
            object: nil,
            queue: nil
        ) { [weak self] notification in
            guard
                let syncDataJson = notification.userInfo?["json"] as? String,
                let data = syncDataJson.data(using: .utf8)
                else {
                return
            }
            
            self?.handleSyncData(data)
        }
        
        NotificationCenter.default.addObserver(
            forName: NSNotification.Name(rawValue:"WSAppNeedUpdate"),
            object: nil,
            queue: nil
        ) { _ in
            DispatchQueue.main.async {
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                    let controller = UpdateAppViewController()
                    appDelegate.changeRootViewController(controller)
                }
            }
        }
    }
    
    private func handleSyncData(_ data: Data) {
        do {
            guard let syncData = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
                return
            }
            
            if let unseenNotifications = syncData["unseen_notifications"] as? String {
                if let unseen = Int(unseenNotifications) {
                    AppManager.shared.setUnreadNotificationsCount(newCount: unseen)
                }
                NotificationCenter.default.post(
                    name: AppManager.SessionData.unreadNotificationsChanged,
                    object: nil,
                    userInfo: nil
                )
            }
            
            if let delayTime = syncData["askforreview"] as? Int {
                RatingAlert.presentRatingPopup(withDelay: delayTime)
            }
            
            if let displayCreditCardOnboardDelay = syncData["display_credit_card_onboard"] as? Int {
                AppParameters.global().displayCreditCardOnboardDelay = displayCreditCardOnboardDelay
            }
            
            if let remoteContactsHash = syncData["contacts"] as? String {
                Contacts().setRemoteContactHash(hash: remoteContactsHash)
            }
            
            if let remoteRechargeHash = syncData["recharge"] as? String {
                if AppParameters.global().remoteRechargeHash != remoteRechargeHash {
                    AppParameters.global().remoteRechargeHash = remoteRechargeHash
                }
            }
        } catch {
            debugPrint(error)
        }
    }

    private func getBadgesNotification() {
        guard FeatureManager.isActive(.opsCardWalletBadgeBool) else {
            return
        }

        let service = AdvertisingService()
        service.getBadges { [weak self] result in
            guard case let .success(badges) = result else {
                return
            }

            self?.setupBadgeNotification(badges: badges.badges)
        }
    }

    private func setupBadgeNotification(badges:[Badge]) {
        badges.forEach { badge in
            switch badge.key {
            case .wallet:
                guard let label = badge.label else {
                    return
                }
                setupWalletNotification(label: label)
            }
        }
    }

    private func setupWalletNotification(label: String) {
        setUnreadWalletNotificationsLabel(label: label)
    }

    // MARK: - Public Local Data
    public var sessionData: SessionData
    
    @objc public var mainScreenCoordinator: MainScreenCoordinator?
    
    // MARK: - Notifications Count
    
    @objc(getUnreadNotificationsCount)
    func getUnreadNotificationsCount() -> Int {
        unreadNotificationsCount
    }
    @objc(setUnreadNotificationsCount:)
    func setUnreadNotificationsCount(newCount: Int) {
        unreadNotificationsCount = newCount < 0 ? 0 : newCount
        NotificationCenter.default.post(name: newUnreadNotificationKey, object: nil)
    }
    func setUnreadWalletNotificationsLabel(label: String) {
        unreadWalletNotificationsLabel = label
        NotificationCenter.default.post(name: SessionData.unreadWalletNotificationsChanged, object: nil)
    }
    
    @objc
    func goToApp(fromContext: UIViewController?) {
        let openedApp = MainTabBarController.controllerFromStoryboard()
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        PaginationCoordinator.shared.setTabController(mainTab: openedApp)
        openedApp.switchToPaymentTabIfNecessary()
        appDelegate?.changeRootViewController(PaginationCoordinator.shared.mainScreen())

        Analytics.shared.log(ApplicationEvent.appView)
    }
    
    // MARK: - Internal Data Struct
    
    public struct SessionData {
        
        static var unreadNotificationsChanged = NSNotification.Name(rawValue: "SesssionUnseenNotificationsChanged")
        static var unreadWalletNotificationsChanged = NSNotification.Name(rawValue: "SesssionUnseenWalletNotificationsChanged")
        
        var unreadNotificationsCount: Int {
            return AppManager.shared.getUnreadNotificationsCount()
        }
        var unreadWalletNotificationsLabel: String {
            return AppManager.shared.unreadWalletNotificationsLabel
        }
    }
}
