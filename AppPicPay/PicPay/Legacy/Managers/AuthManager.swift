import FeatureFlag
import Foundation
import Mixpanel
import DirectMessageSB
import AnalyticsModule

protocol AuthManagerContract {
    func logout(fromContext context: UIViewController?, completion: ((Error?) -> Void)?)
}

final class AuthManager: AuthManagerContract {
    private let dependencies: HasAnalytics = DependencyContainer()

    static let shared : AuthManager = {
        let instance = AuthManager()
        return instance
    }()
    
    // MARK: - Initializer
    
    private init() {}
    
    // MARK: - Public Methods
    func logout(fromContext context: UIViewController?, completion: ((Error?) -> Void)? = nil) {
        PPAnalytics.trackEvent("Logout", properties: nil)

        dependencies.analytics.log(LoginEvents.userSignedOut)
        dependencies.analytics.reset()
        
        ConsumerManager.shared.resetAllLocalConsumerInformation()
        
        DispatchQueue.main.async {
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            let landingController = UINavigationController(rootViewController: SignUpFactory.make())
            appDelegate?.changeRootViewController(landingController)
        }
        
        CacheManagerOBJC.shared.removeAllCacheObjects()
        
        if FeatureManager.isActive(.directMessageSBEnabled) {
            ChatApiManager.shared.disconnect(completion: nil)
        }
    }
}
