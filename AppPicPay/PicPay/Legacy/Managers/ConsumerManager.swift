import Core
import CoreLegacy

protocol ConsumerManagerContract {
    var consumer: MBConsumer? { get }
    var privacyConfig: Int { get set }
    var lastChosenPrivacyConfig: Int { get set }
    var isBalanceHidden: Bool { get set }
    var isIncompleteAccount: Bool { get }
    
    func loadConsumerBalance()
    func setConsumerBalance(_ value: NSDecimalNumber)
    func checkFirstInstallAndClearKeychainIfNeeded()
    func privacyConfigSuggestionPromptCount() -> Int
    func incrementPrivacyConfigSuggestionPromptCountWithLastPrivacy(privacy: Int)
    func resetAllLocalConsumerInformation()
    func toggleBalanceVisibility() -> Bool
    func loadConsumerData(completion: ((_ success: Bool, _ error: Error?) -> Void)?)
    func loadConsumerDataCached(useCache: Bool, completion: ((_ success: Bool, _ error: Error?) -> Void)?)
    func setGetAllConsumerData(cachedData: [String: Any])
    func getAllConsumerDataCache() -> [String: Any]?
    func setUseBalance(_ use: Bool)
    func useBalance() -> Bool

    static func setUseBalance(_ use: Bool)
    static func useBalance() -> Bool
}


final class ConsumerManager: NSObject, ConsumerManagerContract {
    typealias Dependencies = HasKeychainManager
    private let dependencies: Dependencies = DependencyContainer()
    
    @objc
    public static let shared: ConsumerManager = {
        let instance = ConsumerManager()
        return instance
    }()
    
    @objc
    var consumer: MBConsumer?
    
    @objc
    public func loadConsumerBalance() {
        if ConsumerManager.shared.consumer != nil {
            WSExploreRequest.globalCredits { [weak self] (globalCredits, error) in
                guard let credit = globalCredits else {
                    return
                }
                self?.setConsumerBalance(credit)
            }
        }
    }
    
    @objc
    public func setConsumerBalance(_ value: NSDecimalNumber) {
        ConsumerManager.shared.consumer?.balance = value
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: Notification.Name.Consumer.balanceChanged, object: nil)
        }
    }
    
    var privacyConfig: Int {
        get {
            let config = KVStore().intFor(.privacyCconfig)
            return config > 0 ? config : 3
        }
        set(value) {
            KVStore().setInt(value, with: .privacyCconfig)
        }
    }
    
    var lastChosenPrivacyConfig: Int {
        get {
            return KVStore().intFor(.lastChosenPrivacyConfig)
        }
        set(value) {
            KVStore().setInt(value, with: .lastChosenPrivacyConfig)
        }
    }
    
    var isBalanceHidden: Bool {
        get {
            return KVStore().boolFor(.isBalanceHidden)
        }
        set(value) {
            PPAnalytics.setMixpanelUserProperties(["Saldo Oculto": String(value)])
            KVStore().setBool(value, with: .isBalanceHidden)
        }
    }
    
    @objc
    func checkFirstInstallAndClearKeychainIfNeeded() {
        if !KVStore().boolFor(.alreadyOpenApp) {
            KVStore().setBool(true, with: .alreadyOpenApp)
            if (dependencies.keychain.getData(key: KeychainKey.token) != nil) || !User.isAuthenticated {
                dependencies.keychain.clearAll()
            }
        }
    }
    
    func privacyConfigSuggestionPromptCount() -> Int {
        return KVStore().intFor(.privacyConfigSuggestionPromptCount)
    }
    
    func incrementPrivacyConfigSuggestionPromptCountWithLastPrivacy(privacy: Int) {
        lastChosenPrivacyConfig = privacy
        KVStore().setInt(privacyConfigSuggestionPromptCount() + 1, with: .privacyConfigSuggestionPromptCount)
    }
  
    func resetAllLocalConsumerInformation() {
        ConsumerManager.shared.consumer = nil
        CreditCardManager.shared.clearCards()
        User.deleteToken()
        KVStore().removeObjectFor(.isPhoneVerified)
        KVStore().removeObjectFor(.authInfoUpdated)
        KVStore().removeObjectFor(.identifyContactsCache)
        KVStore().removeObjectFor(.localContactsHash)
        KVStore().removeObjectFor(.getAllConsumerData)
        KVStore().removeObjectFor(.onboardSocial)
        KVStore().removeObjectFor(.listOfferCardsWalletDismissed)
        KVStore().removeObjectFor(.touch_id_asked)
        KVStore().removeObjectFor(.hasPresentedCreditPicpayOfferAlert)
        KVStore().removeObjectFor(.showCreditPaymentMethodIntro)
        KVStore().removeObjectFor(AppProtectionKey.userDeviceProtectedValue)
        KVStore().removeObjectFor(.isUserReviewPrivacy)
        // Esse typo já existia, não corrigi pois seria necessário uma migração primeiro
        // Será necessário agora apagar quando deslogar pois o campo também será persisitido no banco
        KVStore().removeObjectFor(.privacyCconfig)
        
        dependencies.keychain.clearAll()
    }
    
    func toggleBalanceVisibility() -> Bool {
        isBalanceHidden = !isBalanceHidden
        return isBalanceHidden
    }
}

@objc
extension ConsumerManager {
   
    @objc
    public static func setUseBalance(_ use: Bool) {
        KVStore().setBool(use, with: .useBalance)
        paymentMethodChangedNotification()
    }
    
    @objc
    public static func useBalance() -> Bool {
        if !KVStore().boolFor(.useBalanceAlreadySetted) {
            KVStore().setBool(true, with: .useBalanceAlreadySetted)
            setUseBalance(true)
        }
        
        return KVStore().boolFor(.useBalance)
    }
    
    private static func paymentMethodChangedNotification() {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: Notification.Name.Payment.methodChange, object: nil)
        }
    }
}

extension ConsumerManager {
    public func setUseBalance(_ use: Bool) {
        ConsumerManager.setUseBalance(use)
    }
    
    public func useBalance() -> Bool {
        ConsumerManager.useBalance()
    }
}

//All Consumer Data
extension ConsumerManager {
    var isIncompleteAccount: Bool {
        if let localConsumer = self.consumer {
            return localConsumer.cpf == nil || localConsumer.name == nil || localConsumer.cpf?.count == 0 || localConsumer.name?.count == 0
        } else {
            return false
        }
    }
    
    @objc
    func loadConsumerData(completion: ((_ success: Bool, _ error: Error?) -> Void)?) {
        DispatchQueue.global().async {
            self.loadConsumerDataCached(useCache: false, completion: { (_, _) in })
        }
        self.loadConsumerDataCached(useCache: true, completion: completion)
    }
    
    @objc
    func loadConsumerDataCached(useCache: Bool, completion: ((_ success: Bool, _ error: Error?) -> Void)?) {
        if User.isAuthenticated {
            let processConsumerData: (MBConsumer) -> Void = { [weak self] localConsumer in
                self?.consumer = localConsumer
                if localConsumer.verifiedPhoneNumber != nil {
                    AppParameters.global().setPhoneVerified(true)
                } else {
                    AppParameters.global().setPhoneVerified(false)
                }
                AppParameters.global().setAuthInfoUpdated(true)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "balanceChanged"), object: nil)
            }
            
            WSConsumerRequest.getAllConsumerData(useCache) { (localConsumer, error) in
                if localConsumer == nil {
                    WSConsumerRequest.getAllConsumerData(true, completionHandler: { (localConsumer, error) in
                        if let error = error {
                            let properties = ["error": error.localizedDescription, "breadcrumb": BreadcrumbManager.shared.breadcrumb]
                            PPAnalytics.trackEvent("AppParameters userToken nil", properties: properties)
                            User.deleteToken()
                            completion?(false, error)
                        } else {
                            if let consumer = localConsumer {
                                processConsumerData(consumer)
                                completion?(true, nil)
                            } else {
                                completion?(false, PicPayError(message: Strings.Legacy.errorLoadingYourData, code: "1337"))
                            }
                        }
                    })
                } else {
                    if let consumer = localConsumer {
                        processConsumerData(consumer)
                        completion?(true, nil)
                    } else {
                        completion?(false, PicPayError(message: Strings.Legacy.errorLoadingYourData, code: "1337"))
                    }
                }
            }
        } else {
            completion?(false, PicPayError(message: Strings.Legacy.errorLoadingYourData, code: "1337"))
        }
    }
    
    @objc
    func setGetAllConsumerData(cachedData: [String: Any]) {
        let data = NSKeyedArchiver.archivedData(withRootObject: cachedData)
        KVStore().setData(data, with: .getAllConsumerData)
    }
    
    @objc
    func getAllConsumerDataCache() -> [String: Any]? {
        if let data = KVStore().dumbObjCForKey(.getAllConsumerData) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: data) as? [String: Any]
        }
        
        return nil
    }
}
