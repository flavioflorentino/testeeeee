//
//  ViewsManager.swift
//  PicPayEmpresas
//
//  ☝🏽☁️ Created by Luiz Henrique Guimaraes on 25/01/17.
//  Copyright © 2017 PicPay. All rights reserved.
//

import UIKit

extension UIStoryboard {

    /// Instantiate a new view controler base on storyboard
    /// OBS.: The storyboard need to have a UIViewController Identifier with the same name of the class
    ///
    /// - parameter storyboard: storyboard where the the viewcontroller will be instantiate
    /// - return T: The class that called this method
    static func viewController<T>(_ storyboard: UIStoryboard.Storyboard, bundle: Bundle? = nil) -> T where T: StoryboardIdentifiable {

        let board = UIStoryboard(storyboard: storyboard, bundle: bundle)
        guard let viewController = board.instantiateViewController(withIdentifier: T.storyboardIdentifier) as? T else {
            fatalError("Couldn't instantiate view controller with identifier \(T.storyboardIdentifier) ")
        }

        return viewController
    }

}

/// Exntend the UIStoryboard to enable create a storyboard base on existent storyboard on project
extension UIStoryboard {
    enum Storyboard: String {
        case report = "Report"
        case creditPhoto = "Creditphoto"
        case withdrawal = "Withdrawal"
        case credit = "Credit"
        case creditStatus = "CreditStatus"
        case creditPicPay = "CreditPicPay"
        case recharge = "Recharge"
        case digitalGoodsRecharge = "DigitalGoodsRecharge"
        case digitalGoodsBoleto = "DGBoleto"
        case digitalGoodsTv = "DGTvRecharge"
        case paymentmethods = "PaymentMethods"
        case digitalGoodsPayment = "DGPayment"
        case mainTabBar = "MainTabBar"
        case digitalGoodsCode = "DGVoucher"
        case ecommerce = "Ecommerce"
        case identityValidation = "IdentityValidation"
        case digitalGoodsTicket = "DGTicket"
        case digitalGoodsParking = "DGParking"
        case p2m = "P2M"
        case linkedAccounts = "LinkedAccounts"
        case creditCardVerification = "CreditCardVerification"

        var filename: String {
            return rawValue
        }
    }

    /// Shortcut to enable
    convenience init(storyboard: Storyboard, bundle: Bundle? = nil) {
        self.init(name: storyboard.filename, bundle: bundle)
    }

}

protocol StoryboardIdentifiable {
    static var storyboardIdentifier: String { get }
}

/// We’ve now made it so every UIViewController within our project conforms to the StoryboardIdentifiable
extension StoryboardIdentifiable where Self: UIViewController {
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
}

/// Make a global UIViewController conformance with StoryboardIdentifiable
extension UIViewController: StoryboardIdentifiable {
    class func controllerFromStoryboard(_ storyboard: UIStoryboard.Storyboard) -> Self {
        return UIStoryboard.viewController(storyboard)
    }
}
