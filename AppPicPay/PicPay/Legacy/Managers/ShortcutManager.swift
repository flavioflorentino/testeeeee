//
//  ShortcutManager.swift
//  PicPay
//
//  Created by Lucas Romano on 03/09/2018.
//

import Foundation

final class ShortcutManager: NSObject {
    
    enum ShortcutIdentifier: String {
        case qrCode
        
        // MARK: - Initializers
        init?(fullType: String) {
            guard let last = fullType.components(separatedBy: ".").last else {
                return nil
            }
            self.init(rawValue: last)
        }
        
        // MARK: - Properties
        var type: String {
            return Bundle.main.bundleIdentifier! + ".\(self.rawValue)"
        }
    }
    
    @objc func handleShortCutItem(_ shortcutItem: UIApplicationShortcutItem) -> Bool {
        var handled = false
        
        // Verify that the provided `shortcutItem`'s `type` is one handled by the application.
        guard ShortcutIdentifier(fullType: shortcutItem.type) != nil else {
            return false
        }
        guard let shortCutType = shortcutItem.type as String? else {
            return false
        }
        
        switch shortCutType {
        case ShortcutIdentifier.qrCode.type:
            handled = true
            dismissCurrentPresentController()
            showScanner()
            break
        default:
            break
        }
        
        return handled
    }
    
    private func dismissCurrentPresentController() {
        if let currentController = AppManager.shared.mainScreenCoordinator?.currentController() {
            currentController.presentedViewController?.dismiss(animated: false, completion: nil)
        }
    }
    
    private func showScanner() {
        PaginationCoordinator.shared.showScanner()
    }
    
}
