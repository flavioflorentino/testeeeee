//
//  SearchableContentManager.swift
//  PicPay
//
//  Created by Marcos Timm on 19/07/17.
//
//

import Core
import Foundation
import CoreSpotlight
import MobileCoreServices

final class SearchableContentManager: NSObject {
    
    let ppLogoURL = "https://www.picpay.com/static/images/favicon.png"
    var searchableItems = [CSSearchableItem]()
    
    @objc func ppContacts(contacts: [PPContact]) {

        self.clearAll()

        if contacts.count > 1 {
            for i in 0...(contacts.count - 1) {

                let contact = contacts[i]
                
                let searchableItemAttributeSet = CSSearchableItemAttributeSet(itemContentType: kUTTypeText as String)

                // Name
                if let name = contact.onlineName {
                    searchableItemAttributeSet.title = name
                }
                // Username
                if let username = contact.username {
                    searchableItemAttributeSet.contentDescription = "@\(username)"
                }
                // Logo PicPay
                let imagePathParts = ppLogoURL.components(separatedBy:".")
                searchableItemAttributeSet.thumbnailURL = Bundle.main.url(forResource: imagePathParts[0], withExtension: imagePathParts[1])
                // Keywords
                searchableItemAttributeSet.keywords = ["pagar", "pague", "picpay", "pp", "transferir", "transfer", "conta", "dinheiro", "pay", "money"]
                // wsId as unique identifier
                let searchableItem = CSSearchableItem(uniqueIdentifier: "com.picpay.contact.\(contact.wsId)", domainIdentifier: "picpay", attributeSet: searchableItemAttributeSet)

                searchableItems.append(searchableItem)
                
                CSSearchableIndex.default().indexSearchableItems(searchableItems) { (error) -> Void in
                    if error != nil {
                        debugPrint(error?.localizedDescription ?? "Erro ao sincronizar contato @\(contact.username ?? "")")
                    } else {
                        //print(">>> SearchableContent @\(contact.username!)")
                    }
                }

            }
        }
    }
    
    
    @objc func clearAll() {
        CSSearchableIndex.default().deleteSearchableItems(withDomainIdentifiers: ["picpay"], completionHandler: nil)

    }
    
}
