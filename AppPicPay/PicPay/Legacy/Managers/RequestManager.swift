import Alamofire
import Foundation
import Cache
import Core
import CoreLegacy
import CoreSentinel
import SwiftyJSON


typealias UploadProgress = ((_ progress:Double) -> Void)
typealias BaseCompletion = ((_ error: PicPayErrorDisplayable?) -> Void)

/// Class responsable to manger Any Api request
final class RequestManager {

    private(set) var sessionManager:Alamofire.SessionManager
    static let queue = DispatchQueue(label: "com.picpay.manager-response-queue" + UUID().uuidString, qos: .userInitiated, attributes:.concurrent)
    static let queueSyncData = DispatchQueue(label: "com.picpay.manager-response-sync-queue" + UUID().uuidString, qos: .userInitiated, attributes:.concurrent)
    
    // Manager for public requests
    private var publicSessionManager:Alamofire.SessionManager
    
    static let shared : RequestManager = {
        let instance = RequestManager()
        return instance
    }()
    
    var hostFromBundle: String {
        guard let host = Environment.apiUrl else {
            return ""
        }
        return host
    }
    
    var apiHost: String {
        #if DEVELOPMENT
        if ProcessInfo.processInfo.arguments.contains("UITests"),
            let apiUrl = ProcessInfo.processInfo.environment["API_URL"] {
            return apiUrl
        }
        
        if let developmentApiHost = AppParameters.global().developmentApiHost(), developmentApiHost != "" {
            return developmentApiHost
        }else{
            return self.hostFromBundle
        }
        #else
        return self.hostFromBundle
        #endif
    }
    
    private init(){
        let pinningConfiguration = PinningConfigurationApi()
        let oauthHandler = OAuth2Handler(authManager: AuthManager.shared, appManager: AppManager.shared)
        
        sessionManager = Alamofire.SessionManager()
        publicSessionManager = Alamofire.SessionManager()
        
        if pinningConfiguration.shouldPin {
            let serverPolicyManager = ServerTrustPolicyManager(policies: generateTrustPolicies(configuration: pinningConfiguration))
            sessionManager = Alamofire.SessionManager(serverTrustPolicyManager: serverPolicyManager)
            publicSessionManager = Alamofire.SessionManager(serverTrustPolicyManager: serverPolicyManager)
        }
        
        sessionManager.adapter = oauthHandler
        publicSessionManager.adapter = oauthHandler
    }
    
    private func generateTrustPolicies(configuration: PinningConfigurationApi) -> [String: ServerTrustPolicy] {
        var dict: [String: ServerTrustPolicy] = [:]
        let certificates = configuration.generateSecCertificates()
        if !configuration.shouldPin {
            debugPrint("[RequestManager] Alamofire isn't pinning")
            return dict
        }
        configuration.domainsTrusted.forEach { domain in
            let trustPolicy = ServerTrustPolicy.pinCertificates(
                certificates: certificates,
                validateCertificateChain: true,
                validateHost: true
            )
            dict[domain] = trustPolicy
        }
        
        return dict
    }
    
    // MARK: - Authenticated Request
    
    func apiRequestWithCache<ResponseType: BaseApiResponse>(
        _ cacheKey: String? = nil,
        expiryTime: TimeInterval? = nil,
        onCacheLoaded: @escaping((ResponseType)-> Void),
        endpoint: String, method: Alamofire.HTTPMethod,
        parameters: Parameters? = nil,
        headers: [String : String]? = nil,
        pin:String? = nil ) -> DataRequest {
        
        let cacheKey = cacheKey ?? "cache_\(endpoint)"
        
        var requestCompleted = false
        CacheManagerOBJC.shared.getApiResponseFromCache(forkey: cacheKey, onCacheLoaded: { (responseType: ResponseType) in
            if !requestCompleted {
                onCacheLoaded(responseType)
            }
        })
        
        return self.apiRequest(endpoint: endpoint, method: method, parameters: parameters, headers: headers, pin: pin).responseData(completionHandler : { (response) in
            requestCompleted = true
            let statusCode = response.response?.statusCode ?? 0
            guard let data = response.data, statusCode >= 200 && statusCode < 300 else {
                return
            }
            
            let json = JSON(data)
            let errorJson = json["Error"]
            if errorJson["description_pt"].string == nil {
                CacheManagerOBJC.shared.setApiResponseFromCache(with: data, forKey: cacheKey)
            }
            
        })
    }
    
    func apiRequestWithCacheCodable<ResponseType: Decodable>(
        _ cacheKey: String? = nil,
        expiryTime: TimeInterval? = nil,
        onCacheLoaded: @escaping((ResponseType)-> Void),
        endpoint: String, method: Alamofire.HTTPMethod,
        parameters: Parameters? = nil,
        headers: [String : String]? = nil,
        pin:String? = nil ) -> DataRequest {
        
        let cacheKey = cacheKey ?? "cache_\(endpoint)"
        
        var requestCompleted = false
        CacheManagerOBJC.shared.getApiResponseFromCacheCodable(forkey: cacheKey, onCacheLoaded: { (responseType: ResponseType) in
            if !requestCompleted {
                onCacheLoaded(responseType)
            }
        })
        
        return self.apiRequest(endpoint: endpoint, method: method, parameters: parameters, headers: headers, pin: pin).responseData(completionHandler : { (response) in
            requestCompleted = true
            let statusCode = response.response?.statusCode ?? 0
            guard let data = response.data, statusCode >= 200 && statusCode < 300 else {
                return
            }
            
            let json = JSON(data)
            let errorJson = json["Error"]
            if errorJson["description_pt"].string == nil {
                CacheManagerOBJC.shared.setApiResponseFromCache(with: data, forKey: cacheKey)
            }
            
        })
    }
    
    /// Make an authenticated request for API
    ///
    /// - parameter method: HTTP Method
    /// - parameter parameters: parameters
    /// - parameter headers: Headers
    /// - parameter pin: User Password for validation
    @discardableResult
    func apiRequest(endpoint: String, method: Alamofire.HTTPMethod, parameters: Parameters? = nil, headers: [String : String]? = nil, pin:String? = nil) -> DataRequest {
        var urlString = endpoint
        if endpoint.range(of:"http") == nil {
            urlString = "\(self.apiHost)\(endpoint)"
        }
        
        let commonHeaders = appendToHeaders(headers, pin: pin)
        let encoding:ParameterEncoding = (method == .get ? URLEncoding.default : JSONEncoding.default)
        
        let request = sessionManager.request(urlString, method: method, parameters: parameters, encoding: encoding, headers: commonHeaders)
        Log.info(Log.Domain.networkRequest, debugDescription(request))
        
        return request.picpaySyncAndErrosValidate()
    }
    
    private func composeFormData(multipartFormData: Alamofire.MultipartFormData, parameters: Parameters?, files: [FileUpload]) {
        if let params = parameters {
            for (key, value) in params {
                guard let paramsData = "\(value)".data(using: String.Encoding.utf8) else { continue }
                multipartFormData.append(paramsData, withName: key)
            }
        }
        for file in files {
            multipartFormData.append(file.data, withName: file.key, fileName: file.fileName, mimeType: file.mimeType)
        }
    }
    
    /// Upload a file
    ///
    /// - parameter urlString: URL string
    /// - parameter method: HTTP Method
    /// - parameter files: Files to send
    /// - parameter headers: Headers
    /// - parameter pin: User Password for validation
    func apiUpload<ResponseType: BaseApiResponse>(
        _ urlString: String,
        method: Alamofire.HTTPMethod,
        files: [FileUpload],
        parameters: Parameters? = nil,
        headers: [String: String]? = nil,
        pin: String? = nil,
        uploadProgress: UploadProgress?,
        completion: @escaping (PicPayResult<ResponseType>) -> Void) -> Void {
        let commonHeaders = appendToHeaders(headers, pin: pin)
        sessionManager.upload(
            multipartFormData: { [weak self] multipartFormData in
                self?.composeFormData(multipartFormData: multipartFormData, parameters: parameters, files: files)
            },
            to: urlString,
            method: method,
            headers: commonHeaders,
            encodingCompletion: { [weak self] encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    let request = upload.uploadProgress(closure: { progress in
                        uploadProgress?(progress.fractionCompleted)
                    }).responseApiObject(completionHandler: completion)
                    guard let strongSelf = self else {
                        return
                    }
                    Log.info(Log.Domain.networkRequest, strongSelf.debugDescription(request))
                case .failure(_):
                    let error = PicPayError(message: "Parece que tivemos um problema na conexão. Tente novamente em alguns instantes. (Cod. 1006)", code: "decode_for_upload")
                    completion(PicPayResult<ResponseType>.failure(error))
                }
            }
        )
    }
    
    func apiUploadCodable<ResponseType: Codable>(
        _ urlString: String,
        method: Alamofire.HTTPMethod,
        files: [FileUpload],
        parameters: Parameters? = nil,
        headers: [String: String]? = nil, pin:String? = nil,
        uploadProgress: UploadProgress?,
        completion: @escaping (PicPayResult<ResponseType>) -> Void) -> Void {
        let commonHeaders = appendToHeaders(headers, pin: pin)
        sessionManager.upload(
            multipartFormData: { [weak self] multipartFormData in
                self?.composeFormData(multipartFormData: multipartFormData, parameters: parameters, files: files)
            },
            to: urlString,
            method: method,
            headers: commonHeaders,
            encodingCompletion: { [weak self] encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    let request = upload.uploadProgress(closure: { progress in
                        uploadProgress?(progress.fractionCompleted)
                    }).responseApiCodable(completionHandler: completion)
                    guard let strongSelf = self else {
                        return
                    }
                    Log.info(Log.Domain.networkRequest, strongSelf.debugDescription(request))
                case .failure(_):
                    let error = PicPayError(message: "Parece que tivemos um problema na conexão. Tente novamente em alguns instantes. (Cod. 1006)", code: "decode_for_upload")
                    completion(PicPayResult<ResponseType>.failure(error))
                }
            }
        )
    }
    
    /// Merge the headers with pin
    /// - parameter headers: Headers
    /// - parameter pin: User Password for validation
    private func appendToHeaders(_ headers: [String : String]? = nil, pin: String? = nil) -> [String: String]? {
        var commonHeaders: [String: String] = [:]
        if let headers = headers {
            headers.forEach { commonHeaders.updateValue($1, forKey: $0) }
        }
        
        if let password = pin {
            commonHeaders["password"] = password
        }
        
        guard let location = LocationDescriptor(LocationManager.shared.authorizedLocation) else {
            return commonHeaders
        }
        
        commonHeaders[LocationHeaders.latitude.rawValue] = location.latitude
        commonHeaders[LocationHeaders.longitude.rawValue] = location.longitude
        commonHeaders[LocationHeaders.accuracy.rawValue] = location.accuracy
        commonHeaders[LocationHeaders.locationTimestamp.rawValue] = location.locationTimestamp
        commonHeaders[LocationHeaders.currentTimestamp.rawValue] = location.currentTimestamp
        
        return commonHeaders
    }
    
    private func debugDescription(_ request: Alamofire.Request) -> String{
        var d = "----------------- REQUEST -------------------"
        d += "\n" + request.debugDescription
        d += "\n=========== REQUEST END ============"
        return d
    }
}

struct FileUpload {
    var key:String
    var data:Data
    var fileName:String
    var mimeType:String
    
    init(key:String, data:Data, fileName:String, mimeType:String){
        self.key = key
        self.data = data
        self.fileName = fileName
        self.mimeType = mimeType
    }
}

// MARK: - Custom Response

extension Alamofire.DataRequest {
    static func apiResponseSerializerJSON<ResponseType:BaseApiResponse>() -> DataResponseSerializer<ResponseType> {
        return DataResponseSerializer { req, res , data, error in
            
            guard let validData = data else {
                let reason = "Parece que tivemos um problema na conexão. Tente novamente em alguns instantes. (Cod. 1002)"
                return .failure(PicPayError(message: reason, code: "api_decode_response_data"))
            }
            
            if let statusCode = res?.statusCode, (statusCode < 200 || statusCode > 299) && error == nil {
                let json = JSON(validData)
                
                Log.info(Log.Domain.networkResponse,
                         Alamofire.DataRequest.debugJsonResponse(res, json: json))
                
                //json["code"] = JSON(statusCode)
                return .failure(PicPayError(json: json))
            }else if let error = error as NSError? {
                // If the code is -999 than it's a canceled and should just be discarded
                guard error.code != -999 else { return .failure(PicPayError(message: "Requisição cancelada", code: "-999", title: nil)) }
                if error.isConnectionError() {
                    return .failure(PicPayError(message: "Sem conexão com a internet", code: "22", title: nil))
                }
            }
            
            let json = JSON(validData)
            
            Log.info(Log.Domain.networkResponse,
                     Alamofire.DataRequest.debugJsonResponse(res, json: json))
            
            // workaround old error
            let errorJson = json["Error"]
            if let errorDescription = errorJson["description_pt"].string {
                let id = errorJson["id"].string ?? String(format: "%d", errorJson["id"].int ?? -1)
                let error = PicPayError(message: errorDescription, code: id , title: "Ops!")
                
                if errorJson["data"].exists() {
                    error.data = JSON(errorJson["data"].object)
                }
                
                return .failure(error)
            }
            
            if let data:ResponseType = ResponseType(json: json)  {
                return .success(data)
            }else{
                let reason = "Parece que tivemos um problema na conexão. Tente novamente em alguns instantes. (Cod. 1004)"
                return .failure(PicPayError(message: reason, code: "api_decode_response_object"))
            }
        }
    }
    
    @discardableResult
    func responseApiObject<ResponseType:BaseApiResponse>(completionHandler: @escaping (PicPayResult<ResponseType>) -> Void) -> Self {
        return responseApiObjectWraper(completionHandler: { (response: DataResponse<ResponseType> ) in
            
            switch response.result {
            case .success(let value):
                completionHandler(PicPayResult.success(value))
            case .failure(let error):
                completionHandler(PicPayResult.failure(error as! PicPayError))
            }
        })
    }
    
    @discardableResult
    func responseApiCodable<ResponseType: Decodable>(strategy: JSONDecoder.KeyDecodingStrategy = .convertFromSnakeCase, completionHandler: @escaping (PicPayResult<ResponseType>) -> Void) -> Self {
        return responseApiObjectWraper(completionHandler: { (response: DataResponse<BaseApiGenericResponse>) in
            let reason = "Parece que tivemos um problema na conexão. Tente novamente em alguns instantes. (Cod. 1005)"
            let defaultError = PicPayError(message: reason, code: "api_decode_response_object")
            
            DispatchQueue.main.async {
                switch response.result {
                case .success(let value):
                    if value.json.isEmpty, let emptyResponse = BaseCodableEmptyResponse() as? ResponseType {
                        completionHandler(PicPayResult.success(emptyResponse))
                    } else if let object = value.decodeObject(type: ResponseType.self, strategy: strategy) {
                        completionHandler(PicPayResult.success(object))
                    } else {
                        completionHandler(PicPayResult.failure(defaultError))
                    }
                case .failure(let error):
                    if let error = error as? PicPayError {
                        completionHandler(PicPayResult.failure(error))
                    } else {
                        completionHandler(PicPayResult.failure(defaultError))
                    }
                }
            }
        })
    }
    
    @discardableResult
    private func responseApiObjectWraper<ResponseType:BaseApiResponse>(completionHandler: @escaping (DataResponse<ResponseType>) -> Void) -> Self {
        return response(queue: RequestManager.queue, responseSerializer: DataRequest.apiResponseSerializerJSON(), completionHandler: completionHandler)
    }
    
    // ❗❗❗ IMPORTANT ❗❗❗
    // Check Picpay Headers and Errors
    // These method must be called on all authenticated api requests
    
    public static func picpaySyncAndErrorsDataSerializer() -> DataResponseSerializer<Bool> {
        return DataResponseSerializer { request , res , data, error in
            guard let validData = data else {
                return .success(false)
            }
            
            if let nsError = error as NSError?, nsError.code == NSURLErrorNotConnectedToInternet {
                return .failure(PicPayError.notConnectedToInternet)
            }
            
            //  ------- VALIDATE GENERAL ERRORS ---------
            var picpayError: PicPayError?
            if let statusCode = res?.statusCode, (statusCode < 200 || statusCode > 299) {
                let json = JSON(validData)
                picpayError = PicPayError(json: json)
            }
            
            let json = JSON(validData)
            let errorJson = json["Error"]
            if let id = errorJson["id"].string {
                picpayError = PicPayError(message: errorJson["description_pt"].string ?? "", code: id , title: "Ops!")
            } else if let id = errorJson["id"].int {
                picpayError = PicPayError(
                    message: errorJson["description_pt"].string ?? "",
                    code: String(format: "%d", id),
                    title: "Ops!"
                )
            }
            
            if let error = picpayError {
                let code = error.picpayCode
                
                // ----------- NOTIFIES THAT USER IS NOT AUTHENTICATED ----------
                if code == "1" {
                    NotificationCenter.default.post(
                        name: NSNotification.Name(rawValue: "WSUserNotAuthenticated"), object: nil, userInfo: ["error": error]
                    )
                }
                    
                    // ----------- NOTIFIES THAT USER IS BLOCKED ----------
                else if code == "666" {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "WSUserIsBlocked"), object: nil, userInfo: nil)
                }
                
                // ----------- NOTIFIES THAT APP NEED TO BE UPDATED ----------
                if let statusCode = res?.statusCode, statusCode == 426 {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "WSAppNeedUpdate"), object: nil, userInfo: nil)
                }
                
                // Track Error Event
                PPAnalytics.trackEvent(
                    "API Error",
                    properties: [
                        "code" : code ,
                        "message" :  error.localizedDescription,
                        "endpoint": res?.url?.path ?? "",
                        "status_code": res?.statusCode ?? 0
                    ]
                )
                let parameters = APISentinelParameters(
                    code: code,
                    message: error.localizedDescription,
                    endpoint: res?.url?.path,
                    statusCode: res?.statusCode,
                    correlationId: request?.allHTTPHeaderFields?[CorrelationId.headerField],
                    newPaymentArchitecture: request?.allHTTPHeaderFields?[NewPaymentArchitecture.headerField]
                )
                Sentinel.shared.track(APISentinel.apiError(parameters: parameters))
            }
            
            // ------------- SYNCDATA -----------------
            if let syncdataJsonString = res?.allHeaderFields["syncdata"] as? String {
                NotificationCenter.default.post(
                    name: ApiNotificationsName.syncData,
                    object: nil,
                    userInfo: ["json" : syncdataJsonString]
                )
            }
            
            if let eventStringJson = res?.allHeaderFields["mobile_analytics_events"] as? String,
                let dataFromString = eventStringJson.data(using: .utf8, allowLossyConversion: false) {
                let eventJson = JSON(dataFromString)
                
                // Mixpanel events
                if let mpEvents = eventJson["mp_events"].array {
                    for e in mpEvents {
                        if let event = EventApi.createEvent(e) {
                            PPAnalytics.trackEvent(event.name, properties: event.properties)
                        }
                    }
                }
            }
            
            return .success(true)
        }
    }
    
    public func picpaySyncAndErrosValidate() -> Self {
        return response(queue: RequestManager.queueSyncData, responseSerializer: DataRequest.picpaySyncAndErrorsDataSerializer()) {_ in}
    }
    
    fileprivate struct EventApi {
        let name: String
        let properties:[AnyHashable: Any]?
        
        init(name: String, properties: [AnyHashable: Any]? = nil){
            self.name = name
            self.properties = properties
        }
        
        static func createEvent(_ json: JSON) -> EventApi? {
            guard let name = json["name"].string else {
                return nil
            }
            
            var properties:[AnyHashable: Any] = [:]
            if let propertiesList = json["properties"].array {
                for p in propertiesList {
                    if let value = p["value"].string, let name = p["name"].string {
                        properties[name] = value
                    }
                }
            }
            
            return EventApi(name: name, properties: !properties.isEmpty ? properties : nil )
        }
        
    }
    
    fileprivate static func debugJsonResponse(_ res:HTTPURLResponse?, json: JSON) -> String {
        
        var d: String = ""
        #if DEBUG
        let headers = res?.allHeaderFields.reduce("", { "\($0 ?? "") \n\t\($1.key) = \($1.value)" })
        d += "------------- REQUEST RESPONSE --------------\n"
        d += "URL: \(res?.url?.absoluteString ?? "")\n"
        d += "STATUS: \(res?.statusCode ?? 0)\n"
        d += "HEADERS: {\(headers ?? "" ) \n}\n\n"
        d += json.rawString() ?? ""
        d += "\n========= REQUEST RESPONSE END ===========\n"
        #endif
        return d
    }
    
}

final class OAuth2Handler: RequestAdapter{
    typealias Dependencies = HasKeychainManager & HasKeychainManagerPersisted
    private let dependencies: Dependencies = DependencyContainer()
    private let authManager:AuthManager
    private let appManager:AppManager
    
    private let lock = NSLock()
    
    private var isRefreshing = false
    private var requestsToRetry: [RequestRetryCompletion] = []
    
    private let sessionManager: Alamofire.SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let pinningConfiguration = PinningConfigurationApi()
        if pinningConfiguration.shouldPin {
            var policies: [String: ServerTrustPolicy] = [:]
            pinningConfiguration.domainsTrusted.forEach { domain in
                let trustPolicy = ServerTrustPolicy.pinCertificates(
                    certificates: pinningConfiguration.generateSecCertificates(),
                    validateCertificateChain: true,
                    validateHost: true
                )
                policies[domain] = trustPolicy
            }
            
            let serverPolicyManager = ServerTrustPolicyManager(policies: policies)
            var sessionManager = Alamofire.SessionManager(
                configuration: configuration,
                serverTrustPolicyManager: serverPolicyManager
            )
            
            return sessionManager
        }
        debugPrint("[OAuth2Handler] Alamofire isn't pinning")
        return Alamofire.SessionManager(configuration: configuration)
    }()
    
    init(authManager:AuthManager, appManager:AppManager){
        self.authManager = authManager
        self.appManager = appManager
    }
    
    // MARK: - RequestAdapter
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        
        // Add Picpay default Headers
        var urlRequest = urlRequest
        
        // Access Token
        if let accessToken = User.token {
            urlRequest.setValue(accessToken, forHTTPHeaderField: "token")
        } else {
            PPAnalytics.trackEvent("No token in request", properties: ["Breadcrumb": BreadcrumbManager.shared.breadcrumb])
        }
        
        // Apply the default Request Headers
        let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        let buildNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String
        
        urlRequest.setValue(versionNumber ?? "", forHTTPHeaderField: "app_version")
        urlRequest.setValue(buildNumber ?? "", forHTTPHeaderField: "app_version_b")
        urlRequest.setValue("ios", forHTTPHeaderField: "device_os")
        urlRequest.setValue(UIDevice.current.deviceModel, forHTTPHeaderField: "device_model")
        urlRequest.setValue(UIDevice.current.picpayDeviceId(dependencies: dependencies), forHTTPHeaderField: "device_id")
        urlRequest.setValue(UIDevice.current.installationId, forHTTPHeaderField: "installation_id")
        urlRequest.setValue(ConsumerManager.useBalance() ? "1" : "0", forHTTPHeaderField: "use_balance")
        urlRequest.setValue(CorrelationId.id, forHTTPHeaderField: CorrelationId.headerField)
        urlRequest.setValue(String(describing: CreditCardManager.shared.defaultCreditCardId), forHTTPHeaderField: "selected_credit_card_id")
        urlRequest.setValue(TimeZone.current.identifier, forHTTPHeaderField: "timezone")
        
        return urlRequest
    }
}

// MARK: - PicPay Rquest

extension Alamofire.Result {
    var picpayError: PicPayErrorDisplayable? {
        switch self {
        case .success(_):
            return nil
        case .failure(let error):
            return error as? PicPayErrorDisplayable
        }
    }
}

extension Log.Domain {
    static let networkRequest:[Log.Domain] = [Log.Domain.network, Log.Domain(rawValue: "NetworkRequest")]
    static let networkResponse:[Log.Domain] = [Log.Domain.network, Log.Domain(rawValue: "NetworkResponse")]
}
