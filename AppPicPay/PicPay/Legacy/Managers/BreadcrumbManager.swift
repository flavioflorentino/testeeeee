import UIKit

@objc protocol Breadcrumbable: AnyObject {
    var breadcrumbDescription: String { get }
    var breadcrumbType: BreadcrumbManager.CrumbType { get }
    var breadcrumbProperties: [String:String]? { get }
}

@objc final class BreadcrumbManager: NSObject {
    
    // Singleton
    @objc static let shared : BreadcrumbManager =  {
        return BreadcrumbManager()
    }()
    
    @objc enum CrumbType : Int {
        case screen = 0
        case tab = 1
        case search = 2
        case popup = 3
        case webView = 4
        case listSection = 5
        case deepLink = 6
    }
    
    struct Crumb {
        let description: String
        let properties: [String: String]?
        let type: CrumbType
        
        init(_ description: String, type: CrumbType = .screen, properties: [String: String]? = nil) {
            self.description = description
            self.properties = properties
            self.type = type
        }
        
        var propertiesDescription: String {
            guard let properties = self.properties else { return "" }
            
            var description: String = ""
            for (key, value) in properties {
                description = description.appendingFormat("[%@: %@]", key, value)
            }
            
            return description
        }
        
        var summary: String {
            var type = ""
            
            switch self.type {
            case .listSection:
                type = "Section "
                break
            case .deepLink:
                type = "DeepLink "
                break
            case .tab:
                type = "Tab "
                break
            default:
                break
            }
            
            return type + (BreadcrumbManager.translateList[self.description] ?? self.description)
        }
        
        var fullDescription: String {
            let propertiesDescription =  self.propertiesDescription
            if propertiesDescription != "" {
                return String(format:"%@ (%@)", summary, propertiesDescription)
            }else{
                return self.summary
            }
        }
        
        var debugDescription: String {
            let propertiesDescription =  self.propertiesDescription
            if propertiesDescription != "" {
                return String(format:"%@ (%@)", description, propertiesDescription)
            }else{
                return self.description
            }
        }
    }
    
    // Private properties
    private var crumbs: [Crumb] = []
    @objc private(set) var breadcrumb: String = ""
    private(set) var fullBreadcrumb: String = ""
    private(set) var debugBreadcrumb: String = ""
    
    // Private initializer (use shared singleton instance)
    private override init() {
        super.init()
        setupObservers()
    }
    
    // MARK - Private Methods
    
    private func setupObservers() {
        // clear breadcrumb on new payment event
        NotificationCenter.default.addObserver(forName: Notification.Name.Payment.new, object: nil, queue: nil) { [weak self] notif in
            self?.sendBreadcrumbEvent(data: notif.userInfo as? [String: String])
        }
    }
    
    private func sendBreadcrumbEvent(data: [String: String]?) {
        guard !crumbs.isEmpty, crumbs.count > 1 else {
            return
        }
        
        let count = min(crumbs.count,5)
        var events = crumbs.suffix(count)
        var typeCrumb = data?["type"]
        
        if typeCrumb == nil {
            typeCrumb = events.popLast()?.summary
        }
        
        let eventsNames:[String]? = events.map({ $0.summary })
        let breadcrumb = eventsNames?.joined(separator: " , ")
        
        let eventsCrumbs:[Crumb] = events.reversed()
        
        var properties:[String:String] = [:]
        
        var key = 0
        
        for e in eventsCrumbs {
            key = key + 1
            properties["T-\(key)"] = e.summary
        }
        
        if let type = typeCrumb {
            properties["Tipo"] = type
        }
        
        properties["Breadcrumb"] = breadcrumb
        
        
        PPAnalytics.trackEvent("Transaction", properties: properties)
        BreadcrumbManager.shared.clear()
    }
    
    private func addCrumb(_ crumb: Crumb){
        
        if shouldAddCrumb(crumb) {
            crumbs.append(crumb)
            breadcrumb.append(String(format:"%@ -> ", crumb.summary))
            fullBreadcrumb.append(String(format:"%@ -> ", crumb.fullDescription))
            #if DEBUG
                debugBreadcrumb.append(String(format:"%@ -> ", crumb.debugDescription))
                Log.info(.custom("Breadcrumb"), breadcrumb)
                Log.debug(.custom("Breadcrumb"), debugBreadcrumb)
            #endif
        }
    }
    
    private func shouldAddCrumb(_ crumb: Crumb) ->Bool {
        if let lastCrumb = crumbs.last {
            if lastCrumb.description == crumb.description {
                return false
            }
        }
        
        if crumb.description == "SafariView" {
            return false
        }
        
        return true
    }
    
    // MARK: - Public Methods
    
    /// Add a crumb from breadcrumbable to the breadcrumb
    @objc public func addCrumb(_ crumbable: Breadcrumbable) {
        if crumbable.breadcrumbDescription != "" {
            let crumb = BreadcrumbManager.Crumb(crumbable.breadcrumbDescription, type: crumbable.breadcrumbType, properties: crumbable.breadcrumbProperties)
            addCrumb(crumb)
        }
    }
    
    /// Add a crumba to the breadcrumb
    ///
    /// - Parameters:
    ///   - crumb: crumb descriptions
    ///   - properties: crumb properties
    ///   - type: crumb type
    @objc public func addCrumb(_ crumb: String, typeOf type: CrumbType, withProperties properties: [String: String]? = nil) {
        let crumb = BreadcrumbManager.Crumb(crumb, type: type, properties: properties)
        addCrumb(crumb)
    }
    
    public func clear() {
        crumbs.removeAll()
        breadcrumb = ""
        fullBreadcrumb = ""
        debugBreadcrumb = ""
    }
    
    // for teste only
    public func createTranslateList() -> String {
        var code = "private let translateList: [String: String] = [ \n"
        var keys: [String:String] = [:]
        for c in crumbs {
            keys[c.description] = (c.description)
        }
        
        for (key,value) in keys {
            code += String(format:"       \"%@\":\"%@\",\n",key,value)
        }
        
        code += "       ]"
        return code
    }
    
    // MARK: - TranslateList
    
    fileprivate static let translateList: [String: String] = [
        "SettingsTableViewViewController":"Ajustes",
        "ToggleSettingsViewController":"Configuração de Notificações/Privacidade",
        "DGRechargePhoneViewController":"Recarga Telefone",
        "PaymentMethodsViewController":"Métodos de Pagamento",
        "RechargeBankTransferViewController":"Transferência para Banco",
        "NewProfileViewController":"Perfil de Usuário",
        "WithdrawalDetailViewController":"Detalhes do Saque",
        "ConsumerProfileViewController":"Perfil de Usuário",
        "NotificationsViewController":"Notificações",
        "DGVoucherController":"Item Digital Goods",
        "PaymentViewController":"PAV",
        "DGPaymentViewController":"Digital Goods",
        "NewTransactionViewController": "P2P",
        "SubscriptionPaymentViewController": "Assinatura",
        "EcommercePaymentViewController": "Ecommerce",
        "SafariView":"WebView",
        "SearchWrapper":"Pagar",
        "WithdrawOptionsViewController":"Saque",
        "Search":"Pesquisou",
        "DGVoucherOtherValueController":"Digital Goods Valor Aberto",
        "SocialShareCodeViewController":"Compartilhar Code",
        "PeopleSearchViewController":"Pesquisar Pessoas",
        "ProfileStoreViewController": "Popup Loja",
        "ProducerProfileViewController": "Popup Assinatura",
        "TouchIdRequestPopUpController": "Popup TouchId",
        "ProducerPlansViewController": "Planos Assinatura",
        "StoresMapViewController": "Mapa de Lojas",
        "FeedDetailViewController": "Detalhes do Feed",
        "ScannerBaseViewController": "Scanner",
        "DGRechargeValueViewController": "Recarga Valor",
        "CreditCardInsertionStep2ViewController": "Cadastro de Cartão",
        "LimitExceededPopup": "Popup Limite de pagamento P2P",
        "SubscriptionViewController": "Detalhes da Assinatura"
        ]
}
