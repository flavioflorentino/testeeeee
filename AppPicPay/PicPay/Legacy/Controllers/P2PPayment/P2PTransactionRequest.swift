//
//  NewTransactionRequest.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 14/12/17.
//

import Foundation

// TODO: Should be subclass of BaseApiRequest, but obj-c likes NSObject better
final class P2PTransactionRequest: NSObject {
    // Private
    private var _biometry: Bool?
    private var _someErrorOccurred: Bool?
    
    // Public
    @objc var pin: String?
    
    // Optional primitive types are not available in obj-c
    @objc var biometry: NSNumber? {
        get {
            guard let bio = _biometry else {
                return nil
            }
            return NSNumber(booleanLiteral: bio)
        }
        set(newValue) {
            self._biometry = newValue?.boolValue
        }
    }
    @objc var consumerValue: NSDecimalNumber?
    @objc var credit: NSDecimalNumber?
    @objc var qntParcelas: String?
    @objc var ccId: String?
    @objc var payeePhone: String?
    @objc var message: String?
    @objc var surcharge: NSNumber?
    @objc var extraParam: String?
    @objc var someErrorOccurred: NSNumber? {
        get {
            guard let error = _someErrorOccurred else {
                return nil
            }
            return NSNumber(booleanLiteral: error)
        }
        set(newValue) {
            self._someErrorOccurred = newValue?.boolValue
        }
    }
    @objc var payeeId: String?
    @objc var paymentPrivacyConfig: String?
    @objc var chargeTransactionId: NSNumber?
    // All params are obligatory
    func obligatoryParams(params: [String: Any?]) throws -> [String: Any] {
        let nonNil = params.nilsRemoved
        
        if nonNil.keys.count < params.keys.count {
            throw PicPayError(message: "Missing params")
        }
        
        return nonNil
    }
    
    @objc func toParams() -> [String: Any]? {
        var params = [
            "pin": pin,
            "consumer_value": consumerValue?.stringValue,
            "credit": credit?.stringValue,
            "parcelas": qntParcelas,
            "consumer_credit_card_id": ccId,
            "message": message,
            "duplicated": "",
            "biometry": nil,
            "ignore_balance": !ConsumerManager.useBalance(),
            "payee_id": payeeId ?? "",
            "feed_visibility": paymentPrivacyConfig ?? "2",
            "extra": extraParam ?? "",
            ] as [String : Any?]
        
        if let error = _someErrorOccurred {
            params["duplicated"] = error ? "1" : "0"
        }
        
        if let bio = _biometry {
            params["biometry"] = bio ? "1" : "0"
        }
        
        if let payeePhone = payeePhone {
            params["payee_phone"] = payeePhone
        }
        
        if let surcharge = surcharge {
            params["surcharge"] = surcharge.doubleValue
        }
        
        if let chargeTransactionId = chargeTransactionId {
            params["charge_transaction_id"] = chargeTransactionId.intValue
        }
        
        return try? obligatoryParams(params: params)
    }
}
