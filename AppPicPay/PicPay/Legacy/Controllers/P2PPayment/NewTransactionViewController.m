#import "NewTransactionViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NSString+OnlyNumbers.h"
#import "UILabel+ColorInRanges.h"
#import "PicPay-Swift.h"
#import <UI/UI.h>
#import "WSTransaction.h"

@import Core;
@import CorePayment;
@import FeatureFlag;
@import AnalyticsModule;
@import UI;
@import IssueReport;

@interface NewTransactionViewController () <UIPPProfileImageDelegate, InstallmentsSelectorViewModelDelegate>

@property ProfileViewController * profileViewController;
@property BOOL hasStartLoadProfileData;
@property PaymentToolbarController *paymentToolbarController;
@property InstallmentsTooltip *installmentsTooltip;
@property PciP2pHelper *service;

@end

@implementation NewTransactionViewController

@synthesize privacyConfig;

#pragma mark - Breadcrumb

- (BOOL) shouldAddBreadcrumb {
    return false;
}

#pragma mark - Factory
+ (nullable NewTransactionViewController *) fromStoryboard {
    return (NewTransactionViewController*) [ViewsManager peerToPeerStoryboardViewControllerWithIdentifier:@"P2PPayment"];
}

#pragma mark - Initializer
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self createLoadingView:@"Concluindo transação..."];
    }
    return self;
}

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    paymentManager = [[PPPaymentManager alloc] init];
    paymentManager.subtotal = [NSDecimalNumber zero];
    
    [_messageTextView setTextContainerInset:UIEdgeInsetsMake(5, 0, 10, 0)];
    
    [self addViewSpacing];
        
    self.profileImageBadge.hidden = YES;
    messageMaxLength = 100;
    [self updateCharactersCount];
    
    // Payment Toolbar -------
    
    __weak NewTransactionViewController* weakSelf = self;
    _paymentToolbarController = [[PaymentToolbarController alloc] initWithPaymentManager:paymentManager parent:self frame:CGRectMake(0, 0, self.view.frame.size.width, 92) pay:^(NSString * _Nonnull privacy) {
        weakSelf.privacyConfig = privacy;
        [weakSelf checkout:nil];
    }];
    _paymentToolbarController.origin = @"p2p";
    
    // add payment toolbar as accessory view to value input text
    [self.enteredValue setInputAccessoryView:_paymentToolbarController.toolbar];
    [self.messageTextView setInputAccessoryView:_paymentToolbarController.toolbar];
    
    _installmentsTooltip = [[InstallmentsTooltip alloc] initWithView:self.view toolbarView:_paymentToolbarController.toolbar.view];
    _service = [PciP2pHelper new];
    
    self.messageTextView.text = [self messagePlaceholder];
    
    [self updatePaymentInfo];
    
    [self loadUI];
    
    if (_preselectedContact != nil && !_hasStartLoadProfileData) {
        __weak NewTransactionViewController* weakSelf = self;
        [WSSocial getConsumerProfile:[NSString stringWithFormat:@"%ld", (long) _preselectedContact.wsId] completion:^(PPContact * _Nullable contact, enum FollowerStatus status, enum FollowerStatus statusa, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.preselectedContact = contact;
            });
        }];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    if (self.presentingViewController != nil || self.forceModal)  {
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancelar" style:UIBarButtonItemStylePlain target:self action:@selector(dismiss)];
    }
    
    [self configureValueField];
    [self colorFromPallete];
    
    if (_chargeTransactionId) {
        [OBJCAnalytics logWithName:@"Payment Request Transaction Viewed"
                        properties:@{@"Origin": @"from_notifications"}];
        
        [self createIssueReportButton];
    }
    
    [OBJCAnalytics logWithName:@"Checkout Screen Viewed" properties:@{@"origin": @"p2p"}];
}

- (void) createIssueReportButton {
    if ([FeatureManager isActivePaymentRequestDenounceOBJC]) {
        [OBJCAnalytics logWithName:@"User Navigated"
            properties:@{
                @"from": @"denounce button",
                @"to": @"denounce screen",
                @"timestamp": [self getCurrentTimestamp]
            }
        ];
        
        self.navigationItem.rightBarButtonItem = [
             [UIBarButtonItem alloc]
             initWithTitle:@"Denunciar"
             style:UIBarButtonItemStylePlain
             target:self
             action:@selector(goToPaymentRequestIssueReport)
        ];
    }
}

- (void) goToPaymentRequestIssueReport {
    NSString *transactionId = [_chargeTransactionId stringValue];
    UIViewController *controller = [PaymentRequestReportFactory makeWithTransactionId: transactionId];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)addViewSpacing {
    _horizontalLine = [[UIView alloc] initWithFrame: CGRectZero];
    _horizontalLine.translatesAutoresizingMaskIntoConstraints = NO;
    [_horizontalLine setBackgroundColor:[PaletteObjc ppColorGrayscale200]];
    [self.view addSubview:_horizontalLine];
    
    [_horizontalLine.heightAnchor constraintEqualToConstant: 1/[[UIScreen mainScreen] scale]].active = YES;
    [_horizontalLine.widthAnchor constraintEqualToAnchor: _topView.widthAnchor].active = YES;
    [_horizontalLine.topAnchor constraintEqualToAnchor: _topView.bottomAnchor].active = YES;
    [_horizontalLine.centerXAnchor constraintEqualToAnchor: self.view.centerXAnchor].active = YES;
}

- (void)colorFromPallete {
    [self.view setBackgroundColor:[PaletteObjc ppColorGrayscale000]];
    [_topView setBackgroundColor:[PaletteObjc ppColorGrayscale000]];
    [_messageTextView setBackgroundColor:[PaletteObjc ppColorGrayscale000]];
    [_profileImage setBackgroundColor:[PaletteObjc ppColorGrayscale000]];
    [_contactPicture setBackgroundColor:[PaletteObjc ppColorGrayscale000]];
    
    [_username setTextColor:[PaletteObjc ppColorGrayscale500]];
    [_contactName setTextColor:[PaletteObjc ppColorGrayscale500]];
    [_enteredValue setTextColor:[PaletteObjc ppColorGrayscale500]];
    [_messageTextView setTextColor:[PaletteObjc ppColorGrayscale400]];
}

- (void) cancelTapped {
    [self.view endEditing:YES];
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (void) createLoadingView: (NSString *) title {
    if (loadingView) {
        [loadingView removeFromSuperview];
    }
    
    loadingView = [Loader getLoadingView: title];
    if (self.navigationController) {
        [self.navigationController.view addSubview:loadingView];
    }else{
        [self.view addSubview:loadingView];
    }
    
    loadingView.hidden = YES;
}

- (void) loadConsumerInfo:(NSString *) userId {
    [self createLoadingView:@"Carregando..."];
    loadingView.hidden = NO;
    
    _hasStartLoadProfileData = YES;
    __weak typeof(self) weakSelf = self;
    [WSSocial getConsumerProfile:userId completion:^(PPContact * _Nullable contact, enum FollowerStatus status, enum FollowerStatus statusa, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (weakSelf) {
                __strong typeof(weakSelf) strongSelf = weakSelf;
                strongSelf.preselectedContact = contact;
                [strongSelf loadUI];
                [strongSelf createLoadingView:@"Concluindo transação..."];
                
                if (error != nil) {
                    UIView *overlayer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, strongSelf.view.frame.size.width, strongSelf.view.frame.size.height)];
                    overlayer.backgroundColor = [PaletteObjc ppColorGrayscale000];
                    [strongSelf.view addSubview:overlayer];
                    [AlertMessage showAlertWithMessage:[error localizedDescription] controller:strongSelf completion:^{
                        if (strongSelf.presentationController) {
                            [strongSelf dismiss];
                        } else {
                            [strongSelf.navigationController popToRootViewControllerAnimated:YES];
                        }
                    }];
                }
            }
        });
    }];
}

- (void) loadUI {
    if (self.preselectedContact) {
        selectedContact = _preselectedContact;
    }
    
    self.contactName.text = [selectedContact onlineName];
    self.contactPicture.layer.cornerRadius = self.contactPicture.frame.size.width / 2;
    self.contactPicture.layer.masksToBounds = YES;
    self.username.text = selectedContact.username == nil ? @"" : [NSString stringWithFormat:@"@%@",selectedContact.username];
    
    UIPPProfileImage *profileImage = (UIPPProfileImage*) self.profileImage;
    profileImage.delegate = self;
    [profileImage setContact:selectedContact];
    
    if (!selectedContact.selectedPhone && selectedContact.phones.count > 0) {
        selectedContact.selectedPhone = [selectedContact.phones objectAtIndex:0];
    }
}

- (void)updatePaymentInfo{
    UIButton *installmentButton;
    
    if ([FeatureManager isActiveFeatureInstallmentNewButtonOBJC]) {
        installmentButton = _enhancedInstallmentButton;
        _headerViewHeightConstraint.constant = 130;
        [_installmentsButton setHidden:YES];
        
        [self configureInstallmentButton:installmentButton completed:^{
            [installmentButton setTitle:[NSString stringWithFormat:@"Parcelar em %ldx", (long)[paymentManager selectedQuotaQuantity]] forState:UIControlStateNormal];
            installmentButton.contentEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
        }];
    } else {
        installmentButton = _installmentsButton;
        _headerViewHeightConstraint.constant = 80;
        [_enhancedInstallmentButton setHidden:YES];
        [self configureInstallmentButton:installmentButton completed:^{
            [installmentButton setTitle:[NSString stringWithFormat:@"%ldx", (long)[paymentManager selectedQuotaQuantity]] forState:UIControlStateNormal];
        }];
    }
}

- (void)configureInstallmentButton:(UIButton *)installmentButton completed:(void (^)(void))completion {
    if ([[paymentManager total] compare:[NSNumber numberWithInt:0]] == NSOrderedSame || [[paymentManager cardTotal] compare:[NSNumber numberWithInt:0]] != NSOrderedDescending){ //total == 0 || cartTotal <= 0
        [installmentButton setTintColor:[PaletteObjc ppColorGrayscale400]];
        installmentButton.layer.borderWidth = 1/[[UIScreen mainScreen] scale];
    } else {
        [installmentButton setTintColor:[PaletteObjc ppColorPositive300]];
        installmentButton.layer.borderWidth = 1.0f;
        
        BOOL isActiveInstallmentP2P = [FeatureManager isActiveInstallmentP2POBJC];
        if(isActiveInstallmentP2P) {
            [_installmentsTooltip showWithView:self.view from:installmentButton cardValue:paymentManager.cardTotal];
        }
    }
    
    installmentButton.layer.borderColor = [installmentButton.tintColor CGColor];
    installmentButton.layer.cornerRadius = 7.0f;
    
    
    if ([paymentManager selectedQuotaQuantity] > [[paymentManager installmentsList] count]) {
        if ([[paymentManager installmentsList] count] > 0) {
            [paymentManager setSelectedQuotaQuantity:[[paymentManager installmentsList] count]];
        } else {
            [paymentManager setSelectedQuotaQuantity:1];
        }
    }
    
    completion();
}

- (void)updateCharactersCount{
    if ([_messageTextView.text isEqualToString:[self messagePlaceholder]]) {
        _charactersCount.text = [NSString stringWithFormat:@"%d", messageMaxLength];
    }else{
        _charactersCount.text = [NSString stringWithFormat:@"%lu", (long)(messageMaxLength - _messageTextView.text.length)];
    }
    
}

- (void)installmentUpdatedForPaymentManager:(PPPaymentManager *)manager{
	paymentManager = manager;
    _paymentToolbarController.paymentManager = manager;
	[self updatePaymentInfo];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [OBJCAnalytics logWithName:@"Visualizacao de tela" properties:@{@"nome": @"transacao p2p"}];
    [OBJCAnalytics logFirebaseWithName:@"visualizacao_de_tela" properties:@{@"nome": @"transacao_p2p"}];
    [PPAnalytics trackFirstTimeOnlyEvent:@"FT tela transação P2P" properties:nil includeMixpanel:NO];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    self.navigationController.navigationBar.backgroundColor = [PaletteObjc ppColorGrayscale100];

    [self updatePaymentInfo];
}

- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
    [_paymentToolbarController.toolbar setHidden:NO]; // Bugfix - The toolbar does not appear when you return from the profile view
    
    if (self.showCancel) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelTapped)];
    }
    
    if (self.preselectedPaymentValue) {
        NSString* finalValue = [self addFormatPrice:[[NSString stringWithFormat:@"%.2f", [self.preselectedPaymentValue doubleValue]] doubleValue]];
        NSRange range = NSMakeRange(0, finalValue.length);
        [self.enteredValue.delegate textField:self.enteredValue shouldChangeCharactersInRange:range replacementString:finalValue];
        self.preselectedPaymentValue = nil;
    }
}

- (void)configureValueField {
    if (_isFixedValue) {
        _enteredValue.enabled = NO;
        [_messageTextView becomeFirstResponder];
    } else {
        _enteredValue.enabled = YES;
        if (![_enteredValue isFirstResponder] && ![_messageTextView isFirstResponder]) {
            [_enteredValue becomeFirstResponder];
        }
    }
}

- (NSString *)messagePlaceholder{
    return @"Mande uma mensagem legal aqui :)";
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:[self messagePlaceholder]]) {
        textView.text = @"";
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length == 0) {
        textView.text = [self messagePlaceholder];
    }
}

- (IBAction)changePaymentMethod:(id)sender{
    [self showPaymentMethodSelector];
}

- (void)showPaymentMethodSelector {
    wentToCards = YES;
    
    PaymentMethodsViewController *ccSelection = (PaymentMethodsViewController *)[ViewsManager paymentMethodsStoryboardViewControllerWithIdentifier:@"PaymentMethodsViewController"];
    [[self navigationController] pushViewController:ccSelection animated:YES];
}

- (IBAction)changeInstallments:(id)sender{
    BOOL isActiveInstallmentP2P = [FeatureManager isActiveInstallmentP2POBJC];
    if(!isActiveInstallmentP2P && ![selectedContact businessAccount]){
        [AlertMessage showAlertWithMessage: @"Parcelamento disponível somente quando o destinatário do pagamento for usuário PicPay PRO." controller: self];
    } else if([paymentManager usePicPayBalance] && [[paymentManager cardTotal] compare:[NSNumber numberWithInt:0]] == NSOrderedSame) {
        if([[paymentManager total] compare:[NSNumber numberWithInt:0]] == NSOrderedSame) {
            [self createUserNavigatedEventFrom:@"button installment" to:@"modal installment balance"];
            NSString* title = @"Parcelamento indisponível para esse valor";
            NSString* description = @"Para visualizar as opções de parcelamento, é necessário incluir o valor desejado e alterar a forma de pagamento para cartão de crédito, desabilitando a opção Usar saldo ao pagar.";
            PopupViewController* controller = [self createInstallmentAlertWithTitle:title description:description hasTwoButtons:true];
            controller.modalPresentationStyle = UIModalPresentationOverFullScreen;
            [self showPopup:controller];
            
        } else {
            [self createUserNavigatedEventFrom:@"button installment" to:@"modal filled balance"];
            NSString* title = @"Parcelamento disponível via cartão de crédito";
            NSString* description = @"Para visualizar as opções de parcelamento, é necessário alterar a forma de pagamento para cartão de crédito, desabilitando a opção Usar saldo ao pagar.";
            PopupViewController* controller = [self createInstallmentAlertWithTitle:title description:description hasTwoButtons:true];
            controller.modalPresentationStyle = UIModalPresentationOverFullScreen;
            [self showPopup:controller];
        }
    } else if([[paymentManager cardTotal] compare:[NSNumber numberWithInt:0]] != NSOrderedDescending) {  // cardTotal <= 0
        [self createUserNavigatedEventFrom:@"button installment" to:@"modal installment creditcard"];
        NSString* title = @"Parcelamento indisponível para esse valor";
        NSString* description = @"Para visualizar as opções de parcelamento, é necessário incluir o valor desejado.";
        PopupViewController* controller = [self createInstallmentAlertWithTitle:title description:description hasTwoButtons:false];
        controller.modalPresentationStyle = UIModalPresentationOverFullScreen;
        [self showPopup:controller];
    } else{
        wentToInstallments = YES;
        _installmentsTooltip.blockTooltip = YES;
        [self goToInstallmentsSelector];
        NSNumber *value = [paymentManager subtotal];
        NSString *isInvertedInstallment = [@([FeatureManager isActiveExperimentInvertedInstallmentsOBJC]) isEqual:@1] ? @"true" : @"false";
        [OBJCAnalytics logWithName:@"Installment Accessed" properties:@{
            @"value": [NSString stringWithFormat:@"%@", value],
            @"is_inverted_installment": isInvertedInstallment
        }];
    }
}

- (NSString*)getCurrentTimestamp {
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    formatter.calendar = [NSCalendar currentCalendar];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"pt_BR_POSIX"];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZZZZZ";
    return [formatter stringFromDate:[NSDate date]];
}

- (void)createUserNavigatedEventFrom: (NSString*) from to: (NSString*) to {
    NSString* consumerId = @(ConsumerManager.shared.consumer.wsId).stringValue;
    [OBJCAnalytics logWithName:@"User Navigated" properties:@{
        @"from": from,
        @"to": to,
        @"user_id": consumerId,
        @"timestamp": [self getCurrentTimestamp]
    }];
}

- (PopupViewController*)createInstallmentAlertWithTitle: (NSString*) title description: (NSString*) description hasTwoButtons: (BOOL) hasTwoButtons {
    UIImage* image = [UIImage imageNamed:@"payment_installment"];
    UIColor* lightDescriptionColor = [UIColor colorWithRed:57.0/255 green:70.0/255 blue:77.0/255 alpha:1.0];
    UIColor* darkDescriptionColor = [UIColor colorWithRed:229.0/255 green:229.0/255 blue:229.0/255 alpha:1.0];
    UIColor* dynamicDescriptionColor = [UIColor dynamicColorWithLight:lightDescriptionColor dark:darkDescriptionColor];
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSMutableAttributedString* attributedDescription = [[NSMutableAttributedString alloc] initWithString: description
                                                                                             attributes : @{
                                                                                                 NSFontAttributeName: [UIFont systemFontOfSize:14 weight:UIFontWeightMedium],
                                                                                                 NSParagraphStyleAttributeName: paragraphStyle,
                                                                                                 NSForegroundColorAttributeName: dynamicDescriptionColor
                                                                                             }];
    PopupViewController* controller = [[PopupViewController alloc] initWithTitle:title description:NULL image:image];
    controller.hideCloseButton = true;
    [controller setAttributeDescription:attributedDescription linkAttributes:NULL];
    if(hasTwoButtons) {
        PopupAction* action = [[PopupAction alloc] initWithTitle:@"Alterar forma de pagamento" style:PopupActionStyleFill completion:^{
            [self showPaymentMethodSelector];
            [self createUserNavigatedEventFrom:@"button change payment method" to:@"screen payment method"];
        }];
        PopupAction* action2 = [[PopupAction alloc] initWithTitle:@"Agora não" style:PopupActionStyleLink completion:^{
            [self createUserNavigatedEventFrom:@"button not now" to:@"screen transaction"];
        }];
        [controller addAction:action];
        [controller addAction:action2];
    } else {
        PopupAction* action = [[PopupAction alloc] initWithTitle:@"Ok, entendi" style:PopupActionStyleFill completion:^{
            [self createUserNavigatedEventFrom:@"button ok" to:@"screen transaction"];
        }];
        [controller addAction:action];
    }
    return controller;
}

- (void)goToInstallmentsSelector {
    NSNumber *payeedId = [[NSNumber alloc] initWithInteger:selectedContact.wsId];
    InstallmentsSelectorViewModel *viewModel = [[InstallmentsSelectorViewModel alloc] initWithPaymentManager:paymentManager
                                                                                                     payeeId:payeedId
                                                                                                    sellerId:nil
                                                                                                      origin:@"p2p"
                                                                                                 titleHeader:nil
                                                                                           descriptionHeader:nil
                                                                                                      worker:nil];
    viewModel.delegate = self;
    InstallmentsSelectorViewController *controller = [[InstallmentsSelectorViewController alloc] initWith:viewModel];
    [self.navigationController pushViewController:controller animated:true];
}

/* Credit Card Delegate Methods End */


/* Checkout begin */

- (IBAction)checkout:(id)sender{
    
    [HapticFeedback impactFeedbackLight];
    @try {
        [PPAnalytics trackEvent:@"Finalização P2P"
                     properties:@{
                                  @"Abriu cartões": wentToCards ? @"Sim" : @"Não",
                                  @"Abriu parcelamento": wentToInstallments ? @"Sim" : @"Não",
                                  @"Usou pesquisa": _preselectedContactFromSearch ? @"Sim" : @"Não",
                                  @"Pagou username": _preselectedContactFromSearchWithUsername ? @"Sim" : @"Não",
                                  @"Username destinatário": selectedContact.username,
                                  @"origin": self.touchOrigin !=nil ? self.touchOrigin : @""
                                  }
                includeMixpanel: NO
         ];
    } @catch (NSException *exception) {
        
    }
    
    NSString *phone_onlyNumbers = [selectedContact.selectedPhone onlyNumbers];
    
    if (phone_onlyNumbers.length < 10 && !editedSelectedPhone
        && selectedContact.selectedPhone != nil
        && selectedContact.contactType != PPContactTypeNearby
        && selectedContact.contactType != PPContactTypeSearchResult
        && selectedContact.contactType != PPContactTypeProfilePopUp
        ){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle: @"O número desse contato não possui DDD. Qual o DDD desse contato?" message: nil preferredStyle: UIAlertControllerStyleAlert];
        
        UIAlertAction *continuar = [UIAlertAction actionWithTitle: @"Continuar" style: UIAlertActionStyleDefault handler: ^(UIAlertAction * action) {
            NSString *ddd = alert.textFields.firstObject.text;
            editedSelectedPhone = [ddd stringByAppendingString:selectedContact.selectedPhone];
            [self checkout:nil];
        }];
        
        UIAlertAction *cancelar = [UIAlertAction actionWithTitle: @"Cancelar" style: UIAlertActionStyleCancel handler: ^(UIAlertAction * action) {
            [_enteredValue becomeFirstResponder];
        }];
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"DDD";
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            textField.delegate = self;
            textField.tag = 123;
            textField.keyboardType = UIKeyboardTypeDecimalPad;
        }];
        
        [alert addAction: cancelar];
        [alert addAction: continuar];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        return;
    }
    
    if (!selectedContact) {
        [AlertMessage showAlertWithMessage: @"Selecione um contato." controller: self];
    } else if([[paymentManager total] compare:[NSNumber numberWithInt:0]] == NSOrderedSame) {
        [AlertMessage showAlertWithMessage: @"Digite o valor que deseja pagar." controller: self];
    } else {
        [self doCheckoutAuthenticate];
    }
}

/**!
 * Authenticate de user to checkout
 */
- (void)doCheckoutAuthenticate {
    [_enteredValue resignFirstResponder];
    [_messageTextView resignFirstResponder];
    
    auth = [PPAuth authenticate:^(NSString *authToken, BOOL biometry) {
        [self checkoutSegue:authToken biometry:biometry surcharge: nil];
    } canceledByUserBlock:^{
        [_enteredValue becomeFirstResponder];
    }];
}

-(NSString *) getMessage {
    if ([self.messageTextView.text isEqualToString:[self messagePlaceholder]]) {
        return  @"";
    }
    return self.messageTextView.text;
}

-(void)checkoutSegue:(NSString *)pin biometry:(BOOL)biometry surcharge: (NSNumber * _Nullable) surcharge {
    [self callPayRequestPaymentAnalytics];
    
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self createLoadingView:@"Concluindo transação..."];
        loadingView.hidden = NO;
    });
    
    P2PTransactionRequest* request = [[P2PTransactionRequest alloc] init];
    
    request.pin = pin;
    request.credit = [paymentManager balanceTotal];
    request.biometry = [NSNumber numberWithBool: biometry];
    request.consumerValue = [paymentManager subtotal];
    request.qntParcelas = [NSString stringWithFormat:@"%ld",(long)[paymentManager selectedQuotaQuantity]];
    request.ccId = [[NSString alloc] initWithFormat:@"%ld", (long) CreditCardManager.shared.defaultCreditCardId];
    if (editedSelectedPhone != nil || selectedContact.selectedPhone != nil ){
        request.payeePhone = [[NSString alloc] initWithFormat:@"%@", (editedSelectedPhone ? editedSelectedPhone : selectedContact.selectedPhone)];
    }
    request.surcharge = surcharge;
    request.chargeTransactionId = _chargeTransactionId;
    
    if (![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            request.message = [self getMessage];
        });
    }else{
        request.message = [self getMessage];
    }
    
    request.someErrorOccurred = [NSNumber numberWithBool: someErrorOccurred];
    request.payeeId = (!selectedContact.wsId || selectedContact.wsId==0) ? nil: [[NSNumber numberWithInteger:selectedContact.wsId] stringValue];
    request.paymentPrivacyConfig = privacyConfig;
    request.extraParam = _deepLinkExtraParam;
    
    NSDictionary* params = [request toParams];
    
    if (!params) {
        DebugLog(@"ERROR: Parametros nulos na tela de transação P2P");
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    if (_service.paymentIsPci) {
        [_service createTransactionWithRequest:request manager:paymentManager navigationController:self.navigationController onSuccess:^(PPP2PTransaction * transactionForSegue) {
            if (weakSelf) {
                __strong typeof(weakSelf) strongSelf = weakSelf;
                [strongSelf requestSuccess:transactionForSegue];
            }
        } onError:^(NSError * error) {
            if (weakSelf) {
                __strong typeof(weakSelf) strongSelf = weakSelf;
                [strongSelf requestError:error pin:pin biometry:biometry value:request.consumerValue];
            }
        }];
    } else {
        [WSTransaction createP2PTransaction: request completionHandler:^(PPP2PTransaction *transactionForSegue, NSError *error) {
            if (weakSelf) {
                __strong typeof(weakSelf) strongSelf = weakSelf;
                if (error == nil) {
                    [strongSelf requestSuccess:transactionForSegue];
                } else {
                    [strongSelf requestError:error pin:pin biometry:biometry value:request.consumerValue];
                }
            }
        }];
    }
}

- (void)callPayRequestPaymentAnalytics {
    if (_chargeTransactionId) {
        NSDecimalNumber* consumerBalance = ConsumerManager.shared.consumer.balance;
        NSString *paymentMethod;
        
        if (paymentManager.usePicPayBalance) {
            if (consumerBalance.doubleValue < paymentManager.subtotal.doubleValue) {
                paymentMethod = @"both";
            } else {
                paymentMethod = @"funds";
            }
        } else {
            paymentMethod = @"card";
        }
    
        NSString *hasMessage = ([self getMessage].length != 0) ? @"true" : @"false";
        
        NSString *privacyString = [[NSString alloc] initWithFormat:@"%ld", (long)PaymentPrivacyPrivate];
        NSString *privacy = [privacyConfig isEqualToString: privacyString] ? @"private" : @"public";
    
        [OBJCAnalytics logWithName:@"Payment Request Pay"
        properties:@{
            @"payment_value": paymentManager.subtotal,
            @"has_message": hasMessage,
            @"payment_method": paymentMethod,
            @"privacy": privacy
        }];
    }
}

- (void)requestSuccess:(PPP2PTransaction *)transaction {
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [HapticFeedback notificationFeedbackSuccess];
        
        self->loadingView.hidden = YES;

        [self reloadFeedNotification];

        ReceiptWidgetViewModel *receiptModel = [[ReceiptWidgetViewModel alloc] initWithTransactionId:transaction.wsId type:ReceiptTypeP2P];
        [receiptModel setReceiptWidgetsWithItems:transaction.receiptItems isLoading:NO];

        [TransactionReceipt showReceiptSuccessWithViewController:self receiptViewModel:receiptModel];
        [OBJCAnalytics logWithName:@"Visualizacao de tela" properties:@{@"nome": @"concluiu transacao p2p"}];
        [OBJCAnalytics logWithName:@"transaction" properties:@{@"type": @"p2p", @"transactionId": [receiptModel transactionId]}];
    });
}

- (void)requestError:(NSError *)error pin:(NSString *)pin biometry:(BOOL)biometry value:(NSDecimalNumber *)value {
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        if ([error isKindOfClass:[PicPayError class]] && [Adyen3DS is3dsWithError:(PicPayError*) error]) {
            PicPayError* picpayError = (PicPayError*) error;
            NSDictionary *payload = [Adyen3DS adyenPayloadWithError:picpayError];
            [self continueOn3DSTransactionWith:payload pin:pin biometry:biometry value:value];
               
        } else {
            self->loadingView.hidden = YES;
            self->someErrorOccurred = YES; //Duplicate
           
            if ([self shouldOpenPaymentChallengeWithError:error]) {
                return;
            }
            [self handleError:error pin:pin biometry:biometry];
       }
    });
}

- (BOOL)shouldOpenPaymentChallengeWithError:(NSError *)err {
    if ([err isKindOfClass:[PicPayError class]]) {
        PicPayError* picpayError = (PicPayError*) err;
        NSDictionary* data = [picpayError dataDictionary];
        NSString *errorType = [data valueForKey:@"type"];
        if ([errorType isEqualToString:@"verify_id"]) {
            IdentityChallengeViewController* challenge = [IdentityChallengeViewController new];
            UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:challenge];
            [self presentViewController:navigation animated:YES completion:nil];
            return true;
        }
        if ([errorType isEqualToString:@"verify_card"]) {
            NSString *cardId = [data valueForKey:@"payment_method_id"];
            CardChallengeViewController* challenge = [[CardChallengeViewController alloc] initWithCardId: cardId];
            UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:challenge];
            [self presentViewController:navigation animated:YES completion:nil];
            return true;
        }
    }
    return false;
}

- (void)extracted:(BOOL)biometry err:(NSError *)err pin:(NSString *)pin weakSelf:(NewTransactionViewController *const __weak)weakSelf {
    [auth handleError:err callback:^(NSError *error) {
        if (weakSelf) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            someErrorOccurred = YES; //sets the field duplicated to the next transaction
            
            // Limit Exceeded Error
            LimitExceededError *limitError = [[LimitExceededError alloc] initWithError:error];
            if (limitError) {
                [strongSelf.view endEditing:YES];
                [strongSelf showLimitExceeded:limitError pin:pin biometry:biometry];
                return;
            }
            
            someErrorOccurred = YES; //sets the field duplicated to the next transaction
            
            [AlertMessage showCustomAlertWithErrorUI:err controller:strongSelf completion:^{
                [strongSelf.enteredValue becomeFirstResponder];
            }];
        }
    }];
}

- (void)handleError:(NSError *)err pin:(NSString *)pin biometry:(BOOL)biometry {
    [HapticFeedback notificationFeedbackError];
    
    loadingView.hidden = YES;
    __weak typeof(self) weakSelf = self;
    [self extracted:biometry err:err pin:pin weakSelf:weakSelf];
}

/**
 Show popup for limit exceeded
 
 @param limitError error
 @param pin pin for transaction
 @param biometry biometry
 */
- (void)showLimitExceeded:(LimitExceededError *)limitError pin:(NSString *)pin biometry:(BOOL)biometry  {
    
    if ([FeatureManager isActiveExperimentNewDialogLimitFeePaymentBoolOBJC]) {
        LimitExceedNewPopup *newPopupView = [[LimitExceedNewPopup alloc] initWithLimitError:limitError];
        CustomViewPopupType *customType = [[CustomViewPopupType alloc] initWithView:newPopupView];
        PopupViewController *newPopup = [[PopupViewController alloc] initWithCustomView:customType];
        
        __weak typeof(self) weakSelf = self;
        __weak typeof(PopupViewController) *weakPopup = newPopup;
        
        PopupAction *confirmAction = [[PopupAction alloc] initWithTitle:@"Pagar" style:PopupActionStyleFill completion:^{
            [OBJCAnalytics logWithName:@"Dialog Interacted" properties:@{
                @"is_clicked": @"true",
                @"selected_field": @"button",
                @"action": @"accept_fee",
                @"title": @"Limite de pagamento via cartão de crédito",
                @"location": @"after the transaction exceeds this limit the fee payment",
                @"new_experience": @"true"
            }];
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [weakSelf checkoutSegue: pin biometry: biometry surcharge:limitError.surcharge];
            });
            
            [weakPopup dismissViewControllerAnimated: YES completion: nil];
        }];
        
        newPopup.didCloseDismiss = ^{
            [OBJCAnalytics logWithName:@"Dialog Interacted" properties:@{
                @"is_clicked": @"true",
                @"selected_field": @"button",
                @"action": @"close",
                @"title": @"Limite de pagamento via cartão de crédito",
                @"location": @"after the transaction exceeds this limit the fee payment",
                @"new_experience": @"true"
            }];
        };
        
        [newPopup addAction:confirmAction];
        
        [self showPopup:newPopup];
    } else {
        LimitExceededPopup *popup = [[LimitExceededPopup alloc] initWithError:limitError];
        __weak typeof(self) weakSelf = self;
        __weak typeof(popup) weakPopup = popup;
        
        popup.didClose = ^{
            [weakSelf.enteredValue becomeFirstResponder];
        };
        
        popup.buttonTapAction = ^{
            if (!weakPopup) {
                return;
            }
            [weakPopup dismissViewControllerAnimated:YES completion:^{
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    [weakSelf checkoutSegue: pin biometry: biometry surcharge: limitError.surcharge];
                });
            }];
        };
        
        // wait for picpay pro
        popup.waitButtonTapAction = ^(LimitExceededError *error) {
            if (weakPopup) {
                [weakSelf.enteredValue becomeFirstResponder];
                [weakPopup dismissViewControllerAnimated:YES completion:^{
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                        [WSConsumer sendNotificationPicPayProWith:error.payload completion:^(BOOL success, NSError * _Nullable error) { }];
                    });
                }];
            }
        };
        [self showPopup:popup];
    }
}

/* Checkout end */

- (IBAction)close:(id)sender{
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

-(NSString*)addFormatPrice:(double)dblPrice {
    NSNumber *temp = [NSNumber numberWithDouble:dblPrice];
    NSDecimalNumber *someAmount = [NSDecimalNumber decimalNumberWithDecimal:[temp decimalValue]];
    return [CurrencyFormatter brazillianRealStringFromNumber:someAmount];
}

-(double)removeFormatPrice:(NSString *)strPrice {
    return [[CurrencyFormatter brazillianRealNumberFromString:strPrice] doubleValue];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
	
	if (textField.tag == 123) {
		if (range.location == 0 && [string isEqualToString:@"0"]) {
			return NO;
		}
		
		int maxLength = 2;
		
		NSUInteger oldLength = [textField.text length];
		NSUInteger replacementLength = [string length];
		NSUInteger rangeLength = range.length;
		
		NSUInteger newLength = oldLength - rangeLength + replacementLength;
		
		BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
		
		return newLength <= maxLength || returnKey;
	}else{
		double currentValue		= [self removeFormatPrice:textField.text];
		double cents			= round(currentValue * 100.0f);
		
		if (([string isEqualToString:@"."]) && ((int)currentValue == 0)) {
			cents = floor(cents * 100);
		} else if ([string length]) {
			for (size_t i = 0; i < [string length]; i++) {
				unichar c = [string characterAtIndex:i];
				if (isnumber(c)) {
					cents *= 10;
					cents += c - '0';
				}
			}
		} else {
			cents = floor(cents / 10);
		}
		
		
		NSString *str = [NSString stringWithFormat:@"%f", cents];
		if ([str length] > 15) {
			NSString *newStr = [str substringFromIndex:1];
			cents = [newStr doubleValue];
		}
		
		double valor = (cents/100.0f);
		NSString *finalValue = [self addFormatPrice:[[NSString stringWithFormat:@"%.2f", valor] doubleValue]];
		self.enteredValue.text = finalValue;
		NSDecimalNumber *valorTotal = [[NSDecimalNumber alloc] initWithDouble:valor];
		
		paymentManager.subtotal = [NSDecimalNumber decimalNumberWithDecimal:[valorTotal decimalValue]];
		[self updatePaymentInfo];
		
		return NO;
	}
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
	if ([textField.text isEqualToString:@"R$"]) {
		[textField setText:@""];
	}
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return [textView.text stringByReplacingCharactersInRange:range withString:text].length <= messageMaxLength;
}

- (void)textViewDidChange:(UITextView *)textView {
    [self updateCharactersCount];
}

- (void)viewDidUnload {
    [self setContactName:nil];
    [self setContactPicture:nil];
    [self setEnteredValue:nil];
    [self setMessageTextView:nil];
    [self setHorizontalLine:nil];
    [self setCharactersCount:nil];
    [super viewDidUnload];
}

#pragma mark - UIPPProfileImageDelegate
- (void)profileImageViewDidTap:(UIPPProfileImage *)profileImage contact:(PPContact *)contact {
    [self.view endEditing:YES];
    _paymentToolbarController.toolbar.hidden = YES;
    self.profileViewController = [NewProfileProxy openProfileWithContact:contact parent:self originView:profileImage];
}

#pragma mark - User Actions

- (void)dismiss {
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Keyboard Events

- (void)keyboardWillShow: (NSNotification *)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    [self.view setNeedsLayout];
    [UIView animateWithDuration:0.2f delay:0.0 options:UIViewAnimationOptionTransitionNone
                     animations:^{
                         self.messageBottomConstraint.constant = keyboardSize.height;
                         [self.messageTextView layoutIfNeeded];
                     }
                     completion:nil
     ];
}

- (void)keyboardWillHide: (NSNotification *)notification {
    [self.view setNeedsLayout];
    [UIView animateWithDuration:0.2f delay:0.0 options:UIViewAnimationOptionTransitionNone
                     animations:^{
                         self.messageBottomConstraint.constant = 0;
                         [self.messageTextView layoutIfNeeded];
                     }
                     completion:nil
     ];
}

// Mark - Adyen Delegate
- (void)challengeDidFinishWithResult:(ADYChallengeResult *)result {
    AdyenWorker *worker = [[AdyenWorker alloc] init];
    
    __weak typeof(self) weakSelf = self;
    [worker completeWithPayload:_adyenParameters succeeded:^(ReceiptWidgetViewModel *receipt) {
        if (weakSelf) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf adyenShowSuccessWithReceipt:receipt performedChallenge:YES];
        }
    } error: ^(NSError *error) {
        if (weakSelf) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf adyenShowError:error];
        }
    }];
}

- (void)challengeDidFailWithError:(NSError *)error {
    loadingView.hidden = YES;
    if (![Adyen3DS challengeCancelled:error]) {
         [self adyenShowError:error];
    }
}

#pragma mark - Installment selector delegate

- (void)didSelectedInstallment:(PPPaymentManager *)paymentManager {
    paymentManager = paymentManager;
    _paymentToolbarController.paymentManager = paymentManager;
    [self updatePaymentInfo];
    NSInteger quotaQuantity = [paymentManager selectedQuotaQuantity];
    NSNumber *value = [paymentManager subtotal];
    NSString *isInvertedInstallment = [@([FeatureManager isActiveExperimentInvertedInstallmentsOBJC]) isEqual:@1] ? @"true" : @"false";
    [OBJCAnalytics logWithName:@"Installments Selected" properties:@{
        @"installments": [NSString stringWithFormat:@"%ld", (long)quotaQuantity],
        @"value": [NSString stringWithFormat:@"%@", value],
        @"is_inverted_installment": isInvertedInstallment
    }];
}

- (void)reloadFeedNotification {
    [[NSNotificationCenter defaultCenter] postNotificationName: NotificationName.paymentNew  object: nil userInfo: @{@"type": @"P2P"}];
}

//Mark - Adyen Helpers
- (void)continueOn3DSTransactionWith:(NSDictionary*)payload pin:(NSString *)pin biometry:(BOOL)biometry value:(NSDecimalNumber *)value {
    
    if (![[KVStore new] boolFor:KVKeyViewedWarning3dsTransaction]) {
        __weak typeof(self) weakSelf = self;
        AdyenWarning *warning = [[AdyenWarning alloc] init];
        [HapticFeedback notificationFeedbackError];
        [warning showIn:self completion:^{
            if (weakSelf) {
                __strong typeof(weakSelf) strongSelf = weakSelf;
                [strongSelf continueOn3DSTransactionWith:payload pin:pin biometry:biometry value:value];
            }
        }];
        
        return;
    }
    _adyenValue = value;
    
    AdyenWorker *worker = [[AdyenWorker alloc] init];
    __weak typeof(self) weakSelf = self;
    

    [worker transactionWith:payload hasChallenge:^(id<ADYTransactionProtocol> transaction, id<ADYChallengeParametersProtocol> payload, NSDictionary<NSString *,id> * parameters) {
        if (weakSelf) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            strongSelf.transaction = (ADYTransaction *)transaction;
            strongSelf.adyenParameters = parameters;
            [strongSelf.transaction performChallengeWithParameters:(ADYChallengeParameters *)payload delegate:self];
        }
    } succeeded:^(ReceiptWidgetViewModel * receipt) {
        if (weakSelf) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf adyenShowSuccessWithReceipt:receipt performedChallenge:NO];
        }
    } error:^(PicPayError *error) {
        if (weakSelf) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf adyenShowError:error];
        }
    } injecting:ADYService.class];
}

- (void)adyenShowSuccessWithReceipt:(ReceiptWidgetViewModel *)receipt performedChallenge:(BOOL)performedChallenge {
    [HapticFeedback notificationFeedbackSuccess];
    
    loadingView.hidden = YES;
    
    [self reloadFeedNotification];
    [self publishTrackingEventPerformedChallenge:performedChallenge];
    
    [TransactionReceipt showReceiptSuccessWithViewController:self receiptViewModel:receipt];
}

- (void)publishTrackingEventPerformedChallenge:(BOOL)performedChallenge {
    if (_adyenValue != NULL) {
        [OBJCAnalytics logWithName:@"transaction" properties:@{@"p2p": _adyenValue}];
        [OBJCAnalytics logFirebaseWithName:@"transaction" properties:@{@"p2p": _adyenValue}];
    }
}

- (void)adyenShowError:(NSError *)err {
    loadingView.hidden = YES;
    someErrorOccurred = YES; //sets the field duplicated to the next transaction
    __weak typeof(self) weakSelf = self;
    [AlertMessage showCustomAlertWithError:err controller:self completion:^{
        if (weakSelf) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf.enteredValue becomeFirstResponder];
        }
    }];
}

@end
