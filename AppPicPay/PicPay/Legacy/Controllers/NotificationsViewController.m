//
//  NotificationsViewController.m
//  PicPay
//
//  Created by Diogo Carneiro on 05/11/13.
//
//

#import "NotificationsViewController.h"
#import "WSConsumer.h"
#import "PicPay-Swift.h"
#import "FollowerRequestsTableViewController.h"
#import <UI/UI.h>
@import AnalyticsModule;

//#import "NSString+RegularExpressionSearch.h"

@interface NotificationsViewController () <FollowerTableViewCellDelegate, WithdrawalTableViewCellDelegate, UIPPProfileImageDelegate, FollowerRequestDelegate>
    @property (strong) ProfileViewController* profileViewController;
    //Armazena as FollowerRequest que já foram ignoradas - E necessário no cell reuse para saber o estado de cada notificação.
    @property (strong, nonatomic) NSMutableDictionary *notificationsIgnored;

    @property int page;

    @property (strong, nonatomic) NSArray *notifications;
    @property (strong, nonatomic) UIRefreshControl *refreshControl;
    @property (strong, nonatomic) LoadingTableViewLine* serverLoadingView;
    @property (strong, nonatomic) UIColor *unredCellColor;
    @property BOOL isLoading;
    @property BOOL showLoadingCell;
    @property BOOL isFirstCellUnred;
    
@end

@implementation NotificationsViewController

@synthesize page, refreshControl, notifications;
NSObject* currentCoordinator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

	//self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Voltar" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 100;
    _tableView.backgroundColor = [PaletteObjc ppColorGrayscale100];
    _tableView.tableFooterView = [UIView new];
    _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 10.0)];
    _tableView.tableHeaderView.backgroundColor = [PaletteObjc ppColorGrayscale000];
	UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = _tableView;
    _unredCellColor = [PaletteObjc ppColorGrayscale200];
    
    //configure tableview cell
    UINib *followerRequestNib = [UINib nibWithNibName:@"FollowerTableViewCell" bundle:[NSBundle mainBundle]];
    [tableViewController.tableView registerNib:followerRequestNib forCellReuseIdentifier:@"Follower"];
    
    UINib *ecommerceNib = [UINib nibWithNibName:@"EcommerceNotificationCell" bundle:[NSBundle mainBundle]];
    [tableViewController.tableView registerNib:ecommerceNib forCellReuseIdentifier:@"Ecommerce"];

    UINib *groupFollowerRequestNib = [UINib nibWithNibName:@"GroupFollowerTableViewCell" bundle:[NSBundle mainBundle]];
    [tableViewController.tableView registerNib:groupFollowerRequestNib forCellReuseIdentifier:@"GroupFollower"];
    
    UINib *defaultNib = [UINib nibWithNibName:@"NotificationTableViewCell" bundle:[NSBundle mainBundle]];
    [tableViewController.tableView registerNib:defaultNib forCellReuseIdentifier:@"Default"];

    UINib *widthdrawalNib = [UINib nibWithNibName:@"WithdrawalTableViewCell" bundle:[NSBundle mainBundle]];
    [tableViewController.tableView registerNib:widthdrawalNib forCellReuseIdentifier:@"Withdrawal"];

    refreshControl = [[UIRefreshControl alloc] init];
	refreshControl.tintColor = [UIColor lightGrayColor];
    [refreshControl addTarget:self action:@selector(refreshControlRequested) forControlEvents:UIControlEventValueChanged];
    tableViewController.refreshControl = refreshControl;

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNotifications) name:@"PushReceived" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNotifications) name:@"ContactsInvited" object:nil];

    if (self.forceBackButton){
        self.navigationItem.leftBarButtonItem = nil;
    }

    _loaderView = [[UILoadView alloc] initWithSuperview:self.view position:PositionTop activityStyle: UIActivityIndicatorViewStyleGray];

    self.notificationsIgnored = [[NSMutableDictionary alloc] init];
    
    _serverLoadingView = [[LoadingTableViewLine alloc] initWithWidth: self.view.frame.size.width];
    [self.view addSubview:_serverLoadingView];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
	//if (!loaded) {
    [self updateNotificationsWithCache:!loaded];
	//}
    self.navigationItem.backBarButtonItem.title = @"Voltar";
	loaded = YES;

    [PPAnalytics trackFirstTimeOnlyEvent:@"FT Notificacoes" properties: @{@"breadcrumb": BreadcrumbManager.shared.breadcrumb}];
    
    // Open notification
    if (_notificationToOpen != nil ){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self openNotification:_notificationToOpen parentView:[[UIApplication sharedApplication] keyWindow].rootViewController];
        });
        _notificationToOpen = nil;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self.navigationController setNavigationBarHidden:NO];
    //[self.tabBarController.tabBar setHidden:NO];

    [PPAnalytics trackFirstTimeOnlyEvent:@"FT notifications" properties:nil includeMixpanel:NO];

    if (dismissAfterArriveBack && disappeared && self.isMovingToParentViewController == NO){
        // [self dismissViewControllerAnimated:YES completion:nil];
        // [[[AppParameters globalParameters] sidebarMenu] showWallet];
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];

    disappeared = YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [WSConsumer markNotificationsAsRead:^(NSError *error) {

        }];
    });

    if (self.isMovingFromParentViewController) {
        //Voltando a navegação
        NSOperationQueue *requests = [[NSOperationQueue alloc] init];
        NSBlockOperation *igoneFollowerRequest;
        requests.maxConcurrentOperationCount = 6;

        for (NSString *followerId in self.notificationsIgnored) {
            __weak typeof(self) weakSelf = self;
            igoneFollowerRequest = [NSBlockOperation blockOperationWithBlock:^(void) {
                [WSSocial dismissFollower:followerId completion:^(BOOL success, enum FollowerStatus consumerStatus, enum FollowerStatus followerStatus, NSError * _Nullable error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if(error){
                            [AlertMessage showAlertWithMessage:[error localizedDescription] controller:weakSelf];
                        }
                    });
                }];
            }];
            [requests addOperation:igoneFollowerRequest];
        }
    }
}

- (void)refreshControlRequested{
	[self updateNotifications];
}

- (void)refreshNotifications {
    _loaderView.hidden = NO;
    [self updateNotifications];
}

- (void)updateNotifications{
    [self updateNotificationsWithCache:NO];
}

- (void)updateNotificationsWithCache:(BOOL) useCache{
    if (!refreshControl.isRefreshing) {
        [self showServerLoading];
    }
    
    if (_isLoading) {
        return;
    }
    
	int pageBkp = page; //backing up in case of failure
	page = 0;
    __weak NotificationsViewController* selfWeak = self;
    selfWeak.isLoading = YES;
    
    if (!selfWeak.refreshControl.isRefreshing) {
        selfWeak.showLoadingCell = YES;
    }
    
    // If we are
    // 1 - Using the cache OR
    // 2 - have unread notifications
    // show the loading cell because there will be data on the screen and we need to inform the user
    // that we are still fetching the server
    if (useCache || [AppManager.shared getUnreadNotificationsCount] > 0) {
        // When we are using cache it'll handle refeshing the table, if we are not
        // we need to do it manually
        if (!useCache) {
            [selfWeak refreshTableView];
        }
    }
    [self getNotificationsWithCache:useCache completion:^(BOOL cache){
		//completion
		[selfWeak refreshTableView];
        if (!cache){
            [selfWeak.refreshControl endRefreshing];
            selfWeak.isLoading = NO;
            selfWeak.showLoadingCell = NO;
        }
    } success:^(BOOL cache, NSArray *newNotifications, NSArray *followNotifications) {
        NSMutableArray *temp = [[NSMutableArray alloc] init];
        
        if ([followNotifications count] <= 1) {
            //Adiciona a notificação de FollowRequest no inicio
            [temp addObjectsFromArray:followNotifications];
        } else {
            //Adiciona um grupo de notificações
            [temp addObject:followNotifications];
        }
        //Adiciona as demais notificações
        [temp addObjectsFromArray:newNotifications];

		selfWeak.notifications = temp;


		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [WSConsumer clearUnseenNotifications:^(NSError *error) {

            }];
		});
        
        if(!cache){
            selfWeak.showLoadingCell = NO;
        }
        
        [selfWeak afterRequest: nil];
	} failure:^(NSError *error){
		selfWeak.page = pageBkp;
        [selfWeak afterRequest: error];
	}];
}

- (void)afterRequest:(NSError *) error{
    
    if ([error isConnectionError]) {
        ConnectionErrorView *connectionErrorView = [[ConnectionErrorView alloc] initWith:nil type:TypeErrorConnection frame:self.tableView.frame];
        connectionErrorView.tryAgainTapped = ^{
            [self refreshNotifications];
        };
        self.tableView.tableHeaderView = connectionErrorView;
        self.notifications = nil;
    } else if ([notifications count] == 0) {
        BasicEmptyListView *emptyView = [[BasicEmptyListView alloc] initWithFrame:self.tableView.frame];
        emptyView.titleLabel.text = @"Nenhuma notificação ainda";
        emptyView.textLabel.text = @"";
        emptyView.imageView.image = nil;
        self.tableView.tableHeaderView = emptyView;
        self.serverLoadingView.hidden = YES;
    } else if (!self.showLoadingCell) {
        self.tableView.tableHeaderView = nil;
        self.serverLoadingView.hidden = YES;
    } else {
        self.tableView.tableHeaderView = nil;
    } 
    self.serverLoadingView.hidden = YES;
    _loaderView.hidden = YES;
}

- (void)showServerLoading {
    self.serverLoadingView.hidden = NO;
    [_serverLoadingView startAnimating];
}

- (void)getNotificationsWithCache:(BOOL) useCache completion:(void(^)(BOOL cache))completion success:(void(^)(BOOL cache, NSArray *newNotifications, NSArray *followNotifications))success failure:(void(^)(NSError *error))failure{
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [WSConsumer
         getNotifications:^(NSArray *returnedNotifications, NSArray *followNotifications, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                if (error) {
                    failure(error);
                }else{
                    success(NO, returnedNotifications, followNotifications);
                }
                completion(NO);
            });
        }
        cacheLoaded:^(NSArray * _Nullable notifications, NSArray * _Nullable followNotifications, NSError * _Nullable error) {
            if (useCache) {
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    success(YES, notifications, followNotifications);
                    completion(YES);
                });
            }
        }];
	});
}

- (void) refreshTableView{
	[_tableView reloadData];
}

# pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	NSInteger rows = notifications.count;

    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	return [self notificationCellForIndexPath:indexPath];
}

- (UITableViewCell *)notificationCellForIndexPath:(NSIndexPath *)indexPath{
    
    self.isFirstCellUnred = NO;
    NSIndexPath* adjustedPath = [NSIndexPath indexPathForRow: indexPath.row inSection: indexPath.section];
    
    if([[notifications objectAtIndex:adjustedPath.row] isKindOfClass:[NSArray class]]){
        NSArray<PPFollowerNotification *> *followerNotifications = [notifications objectAtIndex:adjustedPath.row];

        //Há um grupo de notificações, exibir o agrupamento
        GroupFollowerTableViewCell *cell = (GroupFollowerTableViewCell*) [_tableView dequeueReusableCellWithIdentifier:@"GroupFollower"];
        cell.profileImage.delegate = self;
        [cell configureCellWithNotifications:followerNotifications];
        return cell;

    }
    PPNotification *notification = [notifications objectAtIndex:adjustedPath.row];

    if([notification isKindOfClass:[PPFollowerNotification class]]){
        PPFollowerNotification *follwerNotification = (PPFollowerNotification *) notification;

        FollowerTableViewCell *cell = (FollowerTableViewCell*) [_tableView dequeueReusableCellWithIdentifier:@"Follower"];
        cell.delegate = self;
        cell.profileImage.delegate = self;
        cell.unfollowTimerButton.cooldown = 2.5;
        [cell configureCellWithNotification:follwerNotification isFollowerRequest:YES];

        //View
        [cell setFollowerRequestIgnored:[[self.notificationsIgnored objectForKey:[NSString stringWithFormat:@"%ld", (long)follwerNotification.profile.wsId]] boolValue]];

        if (![[notification wsId]  isEqual: @""]) {
            if (![notification read]) {
                if (indexPath.row == 0) {
                    self.tableView.tableHeaderView.backgroundColor = self.unredCellColor;
                }
                cell.contentView.backgroundColor = self.unredCellColor;
                cell.separatorInset = UIEdgeInsetsZero;
            } else {
                cell.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
                cell.contentView.backgroundColor = [PaletteObjc ppColorGrayscale000];
            }
        }

        return cell;
    }
    else if([notification isKindOfClass:[PPConsumerProfileNotification class]]){
        PPConsumerProfileNotification *consumerNotification = (PPConsumerProfileNotification *) notification;

        FollowerTableViewCell *cell = (FollowerTableViewCell*) [_tableView dequeueReusableCellWithIdentifier:@"Follower"];
        cell.delegate = self;
        cell.profileImage.delegate = self;
        [cell setFollowerRequestIgnored:NO];
		[cell configureCellWithNotification:consumerNotification isFollowerRequest:NO];
        return cell;
    }
    else if([notification isKindOfClass:[PPWithdrawalNotification class]]) {
        PPWithdrawalNotification *withdrawalNotification = (PPWithdrawalNotification *) notification;

        WithdrawalTableViewCell *cell = (WithdrawalTableViewCell*) [_tableView dequeueReusableCellWithIdentifier:@"Withdrawal"];
        cell.delegate = self;
        [cell configureCell:withdrawalNotification];
        return cell;

    }
    else if ([notification isKindOfClass:[EcommerceNotification class]]) {
        EcommerceNotification *model = (EcommerceNotification *) notification;
        
        EcommerceNotificationCell *cell = (EcommerceNotificationCell*) [_tableView dequeueReusableCellWithIdentifier:@"Ecommerce"];

        [cell configureWithModel:model];
        return cell;
    }
    else{

        UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"Default"];

        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NotificationTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }

        UILabel *dateLabel = (UILabel *)[cell viewWithTag:234];
        dateLabel.textColor = [PaletteObjc ppColorGrayscale400];
        dateLabel.highlightedTextColor = [PaletteObjc ppColorGrayscale400];
        [dateLabel setText:[notification creationDate]];

        UILabel *messageLabel = (UILabel *)[cell viewWithTag:235];
        messageLabel.textColor = [PaletteObjc ppColorGrayscale600];
        messageLabel.highlightedTextColor = [PaletteObjc ppColorGrayscale600];
        CGFloat messageLabelFontSize = 14.0;
        NSAttributedString* text;
        if ( [notification message] != nil && ![[notification message] isEqualToString:@""] ){
            text = [[[NSAttributedString alloc] initWithString:[[notification message] decodingHTMLEntities] attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:messageLabelFontSize weight:UIFontWeightRegular]}] boldfyWithSystemFontOfSize:messageLabelFontSize weight: UIFontWeightSemibold];
        } else {
            text = [[NSAttributedString alloc] initWithString:@""];
        }
        
        if (![notification read]) {
            if (indexPath.row == 0) {
                self.tableView.tableHeaderView.backgroundColor = self.unredCellColor;
            }
            cell.contentView.backgroundColor = self.unredCellColor;
            cell.separatorInset = UIEdgeInsetsZero;
        } else {
            cell.contentView.backgroundColor = [PaletteObjc ppColorGrayscale000];
            cell.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
        }
        
        messageLabel.attributedText = text;

        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[self tapNotificationAtIndexPath:indexPath];
}

- (void)tapNotificationAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"IndexPath %@", indexPath);
    NSIndexPath* adjustedPath = [NSIndexPath indexPathForRow: indexPath.row inSection: indexPath.section];
    NSLog(@"AdjustedIndexPath %@", adjustedPath);
    if ([[notifications objectAtIndex:adjustedPath.row] isKindOfClass:[NSArray class]]) {
        FollowerRequestsTableViewController *vc = (FollowerRequestsTableViewController *) [ViewsManager walletStoryboardViewControllerWithIdentifier:@"FollowerRequests"];
        vc.delegate = self;
        vc.notifications = [notifications objectAtIndex:adjustedPath.row];
        [self.navigationController pushViewController:vc animated:YES];

        return;
    }

	PPNotification *notification = [notifications objectAtIndex:adjustedPath.row];

    if (!notification.read) {

        __weak NotificationsViewController* selfWeak = self;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [WSConsumer readNotificationWithId:notification.wsId completionHandler:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    [(PPNotification *)[notifications objectAtIndex:adjustedPath.row] markAsRead];
                    [selfWeak refreshTableView];
                });
            }];
        });
    }

    // Skip the selection of follower request notification
    if([notification isKindOfClass:[PPFollowerNotification class]]){
        PPFollowerNotification *followerNotification = (PPFollowerNotification *) notification;
        // Open consumer profile
        self.profileViewController = [NewProfileProxy openProfileWithContact:followerNotification.profile parent:self originView:nil];
        return;
    }

	[_tableView deselectRowAtIndexPath:indexPath animated:YES];

    [self openNotification:notification parentView:self];
}

- (void)openNotification:(PPNotification *)notification{
	[self openNotification:notification parentView:self];
}

- (void)openNotification:(PPNotification *)notification parentView:(UIViewController *)parentViewController{
    id landingScreen = notification.landingScreen;

    if([notification unsuportedVersion]){
        __weak NotificationsViewController * weakSelf = self;
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Versão desatualizada!" message:@"A versão do PicPay que você possui não suporta essa notificação. Deseja atualizar agora?" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Atualizar" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [weakSelf goToAppStore];
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"Não" style:UIAlertActionStyleCancel handler:nil]];
    }else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"contact_us"]){
        //TODO: Add notification parameter to conversationId, so then it will be able to open a specific conversation/ticket
    }else if ([notification.landingUrl isEqualToString:@"http://picpay.com/app"]){
        [self goToAppStore];
    }else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"filtered_places"]){
        [self openPaymentViewWithText:notification.param1 onPage:@"places"];
    }else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"profile"]){
        [[[AppManager shared] mainScreenCoordinator] showSettingsScreen];

	}else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"wallet"]){
        [[[AppManager shared] mainScreenCoordinator] showHomeScreen];
	}else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"withdrawal"]){
		[self goToWithdrawalScreen];
	}else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"follow_accept"]){
        if (notification.data != nil && [notification.data isKindOfClass:[NSDictionary class]]) {
            NSDictionary *data = (NSDictionary *) notification.data;
            if ([data[@"profile"] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *profile = data[@"profile"];
                PPContact *contact = [[PPContact alloc] initWithProfileDictionary:profile];
                [NewProfileProxy openProfileWithContact:contact parent:self originView:nil];
            }
        }
    }
    
    // ===============================================================
    else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"search_text"]){
        [self openPaymentViewWithText:notification.searchText onPage:notification.searchPage];
    }
    // ---------------------- ZONA AZUL ---------------------
    else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"zona_azul_payment"]){
        /*PaymentZonaAzulViewController *controller = (PaymentZonaAzulViewController*)[ [ViewsManager paymentZonaAzulViewControllerWithIdentifier:@"PaymentViewController"];
         [self.navigationController pushViewController:controller animated:YES];*/
    }else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"consumer_profile"]){
        PPConsumerProfileNotification *consumerNotification = (PPConsumerProfileNotification *) notification;
        // Open consumer profile
		@try {
            self.profileViewController = [NewProfileProxy openProfileWithContact:consumerNotification.profile parent:self originView:nil];
		} @catch (NSException *exception) {}
        return;
    }else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"search_people"]){
        //UIViewController *controller = [ViewsManager peopleSearchStoryboardViewController:@"PeopleSearch"];
        //Na tela de pesquisa não pode aparecer o titulo "Voltar"
        //self.navigationItem.backBarButtonItem.title = @"";
        //[self.navigationController pushViewController:controller animated:YES];
        return;
    }else if ([landingScreen isKindOfClass:[NSString class]] && ([landingScreen isEqualToString:@"share_invite_code"] || [landingScreen isEqualToString:@"share_link"])) {
        UIViewController *controller = [ViewsManager peopleSearchStoryboardViewController:@"SocialShareCode"];
        controller.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:controller animated:YES];
    }else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"mgm"]){
        SocialShareCodeViewController *controller = (SocialShareCodeViewController*) [ViewsManager peopleSearchStoryboardViewController:@"SocialShareCode"];
        controller.mgmTypeString = SocialShareCodeViewController.MGMTypeUser;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"mgb"]){
        SocialShareCodeViewController *controller = (SocialShareCodeViewController*) [ViewsManager peopleSearchStoryboardViewController:@"SocialShareCode"];
        controller.mgmTypeString = SocialShareCodeViewController.MGMTypeBusiness;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"birthday_gift"]) {
        UIViewController* viewController = [BirthdayGiftLoadFactory makeWithUserId:notification.param1];
        [self presentViewController:[[PPNavigationController alloc] initWithRootViewController:viewController] animated:YES completion:nil];
    }
    else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"new_user_gift"]) {
        PPConsumerProfileNotification *consumerNotification = (PPConsumerProfileNotification *) notification;
        NSString *userId = [@(consumerNotification.profile.wsId) stringValue];
        
        UIViewController* viewController = [GiftLoaderFactory makeWithUserId:userId];
        [self presentViewController:[[PPNavigationController alloc] initWithRootViewController:viewController] animated:YES completion:nil];
    }
    else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"change_email"]) {
        [self.navigationController pushViewController:[MailFactory make] animated:YES];
    }
    else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"checkout"]){
        NSString *orderNumber = [NSString stringWithFormat:@"%@", notification.param1];
        UIViewController *controller = [EcommerceLoadingFactory makeWithOrderId:orderNumber origin:EcommercePaymentOriginNotification];
        PPNavigationController *navController = [[PPNavigationController new] initWithRootViewController:controller];
        [self presentViewController:navController animated:YES completion:nil];
    }
    else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"boleto"]){
        [DGHelpers openBoletoFlowWithViewController:self origin:@"notification" startWithScanner: NO];
    }
    else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"services_by_id"]){
        DGProxyViewController* proxy = [[DGProxyViewController alloc] initWith: notification.param1];
        [self presentViewController:[[PPNavigationController alloc] initWithRootViewController:proxy] animated:YES completion:^{
            
        }];
    }
    else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"subscription_details"]){
        
        NSString *subscriptionId = [NSString stringWithFormat:@"%@", notification.param1];
        
        SubscriptionViewController* controller = [[SubscriptionViewController alloc] initWithModel:nil nibName:nil bundle:nil];
        [controller loadWithSubscriptionId:subscriptionId];
        
        __weak __typeof(controller) weakController = controller;
        controller.headerView.optionsButtonIsHidden = true;
        
        controller.onClose = ^(id _Nonnull button) {
            __typeof(weakController) strongController = weakController;
            if (strongController) {
                [strongController dismissViewControllerAnimated:true completion:nil];
            }
        };
        
        [self presentViewController:controller animated:true completion:nil];
        
    } else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"open_profile"]){
        PPConsumerProfileNotification *consumerNotification = (PPConsumerProfileNotification *) notification;
        // Open profile
        @try {
            [NewProfileProxy openProfileWithId:consumerNotification.param1.intValue parent:self];
        } @catch (NSException *exception) {}
        return;
    } else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"2fa_login_notification"]){
        if (notification.param1 != nil) {
            [self open2FAAccessNotificationWithJSON: notification.param1];
        }
        return;
    }else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"deeplink"]){
        if (notification.param1){ //verifying if deeplink param was received
            BOOL handled = NO;
            handled = [DeeplinkHelper handleDeeplinkWithUrl:[NSURL URLWithString:notification.param1] from:nil fromScanner:false];
        }
    }
    else if ([landingScreen isKindOfClass:[NSString class]] &&
             ([landingScreen isEqualToString:@"credit_available_screen"] ||
              [landingScreen isEqualToString:@"credit_review_requested_screen"] ||
              [landingScreen isEqualToString:@"credit_registration_approved_screen"] ||
              [landingScreen isEqualToString:@"credit_registration_approved"] ||
              [landingScreen isEqualToString:@"credit_limit_update_screen"] ||
              [landingScreen isEqualToString:@"credit_invoice_closed_screen"]
              ) ){
                 CreditViewModel *viewModel = [[CreditViewModel alloc] initWith:nil];
                 CreditViewController *viewController = [[CreditViewController alloc] initWith:viewModel];
                 viewController.hidesBottomBarWhenPushed = YES;
                 
                 PPNavigationController *navigationController = [[PPNavigationController alloc] initWithRootViewController:viewController];
                 [navigationController setNavigationBarHidden:YES];
                 [self presentViewController:navigationController animated:YES completion:nil];
    }
    else if ([landingScreen isKindOfClass:[NSString class]] && [landingScreen isEqualToString:@"identity_analysis"]) {
        IdentityValidationCoordinatorObjcWrapper* coordinator = [[IdentityValidationCoordinatorObjcWrapper alloc] initFrom:self];
        currentCoordinator = coordinator;
        [coordinator start];
    }
    
    else{
        id landing = [notification landingScreen];

        // If landing is a string, do nothing
        if ([landing isKindOfClass:[NSString class]]) {
            return;
        }

        UIViewController *nextScreen = (UIViewController *) landing;
        if ([landingScreen isKindOfClass:[ReceiptWidgetViewController class]]) {
            [self presentViewController:nextScreen animated:YES completion:nil];
        } else if (nextScreen != nil) {
            @try {
                BOOL animate = !preventPushAnimation;
                preventPushAnimation = NO;
                [nextScreen setHidesBottomBarWhenPushed: YES];
                [[self navigationController] pushViewController:nextScreen animated:animate];
            }
            @catch (NSException *exception) {
            }
        }
    }
}

- (void)open2FAAccessNotificationWithJSON:(NSString *) jsonString {
    NSData* jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *jsonError;
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&jsonError];
    
    if(jsonError == nil) {
        NSString* device = [NSString stringWithFormat:@"%@", [jsonObject valueForKey:@"device_name"]];
        NSString* date = [NSString stringWithFormat:@"%@",[jsonObject valueForKey:@"date"]];
        NSString* url = [NSString stringWithFormat:@"%@", [jsonObject valueForKey:@"new_password_link"]];
        NSString* email = [NSString stringWithFormat:@"%@", [jsonObject valueForKey:@"masked_email"]];
        
        TFAAccessNotificationViewModel *model = [[TFAAccessNotificationViewModel alloc] initWithDevice:device date:date email:email changePasswordUrl:url];
        UIViewController *controller = [[TFAAccessNotificationViewController alloc] initWithModel:model];
        controller.hidesBottomBarWhenPushed = YES;
        
        [self.navigationController pushViewController:controller animated:YES];
    }
}
    
- (void)openPaymentViewWithText:(NSString *) text onPage:(NSString* ) page {
    [HapticFeedback impactFeedbackLight];
    [AppManager.shared.mainScreenCoordinator showPaymentSearchScreenWithSearchText:text page:page ignorePermissionPrompt: NO];
}

// Remove notification view controller from navigation stack
-(void)pushViewControllerAndRemoveNotificationsFromNavigation:(UIViewController *) controller {
    [[self navigationController] pushViewController:controller animated:YES];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSMutableArray * viewsControllers = [NSMutableArray arrayWithArray: [[self navigationController] viewControllers]];
        [viewsControllers removeObjectAtIndex: [viewsControllers count] -2];
        [self navigationController].viewControllers = viewsControllers;

    });
}

- (void)goToAppStore{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id561524792"] options:@{} completionHandler:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark - User Actions

/**!
 * Accept a follower request
 */
- (void) acceptFollowerRequest:(PPConsumerProfileNotification *) followerNotification indexPath:(NSIndexPath *) indexPath cell: (FollowerTableViewCell *) cell{
    __weak NotificationsViewController* selfWeak = self;
    FollowerTableViewCell* __weak weakCell = cell;
    __block FollowerStatus oldConsumerStatus = followerNotification.consumerStatusFollower;
    __block FollowerStatus oldFollowerStatus = followerNotification.followerStatusConsumer;
    
    NSString* idFollower = [NSString stringWithFormat:@"%ld", (long)followerNotification.profile.wsId];
    // call service
    [WSSocial acceptFollower:idFollower completion:^(BOOL success,enum FollowerStatus consumerStatus, enum FollowerStatus followerStatus, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(success){
                // updates the notification to change the button
                // in order to show feedback to the user
                followerNotification.followerStatusConsumer = followerStatus;
                followerNotification.consumerStatusFollower = consumerStatus;

                // Workaround
                // change the notification text
                //followerNotification.message = [NSString stringWithFormat:@"<b>%@</b> (@%@) está te seguindo.",followerNotification.profile.onlineName, followerNotification.profile.username];
                followerNotification.message = [NSString stringWithFormat:@"@%@ está te seguindo.",followerNotification.profile.username];

                [selfWeak.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }else{
                if (error && error.code != 404) {
                    [AlertMessage showAlertWithMessage:[error localizedDescription] controller:selfWeak];
                }
                if (!selfWeak || !weakCell) return;
                
                followerNotification.followerStatusConsumer = oldFollowerStatus;
                followerNotification.consumerStatusFollower = oldConsumerStatus;
                [weakCell changeButtonWithStatus: oldFollowerStatus consumerStatus: oldConsumerStatus];
            }
        });
    }];
}

/**!
 * Follow a consumer
 */
- (void) followConsumer:(PPConsumerProfileNotification *) followerNotification indexPath:(NSIndexPath *) indexPath  cell: (FollowerTableViewCell *) cell{
    __weak NotificationsViewController* selfWeak = self;
    FollowerTableViewCell* __weak weakCell = cell;
    __block FollowerStatus oldConsumerStatus = followerNotification.consumerStatusFollower;
    __block FollowerStatus oldFollowerStatus = followerNotification.followerStatusConsumer;
    
    NSString* idFollower = [NSString stringWithFormat:@"%ld", (long)followerNotification.profile.wsId];
    [WSSocial follow:idFollower completion:^(BOOL success, enum FollowerStatus consumerStatus, enum FollowerStatus followerStatus, NSError * _Nullable error) {

        dispatch_async(dispatch_get_main_queue(), ^{
            if(success){
                // updates the notification to change the button
                // in order to show feedback to the user
                followerNotification.followerStatusConsumer = followerStatus;
                followerNotification.consumerStatusFollower = consumerStatus;
                [selfWeak.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }else{
                [AlertMessage showAlertWithMessage:[error localizedDescription] controller:selfWeak];
                if (!selfWeak || !weakCell) return;
                
                followerNotification.followerStatusConsumer = oldFollowerStatus;
                followerNotification.consumerStatusFollower = oldConsumerStatus;
                [weakCell changeButtonWithStatus: oldFollowerStatus consumerStatus: oldConsumerStatus];
            }
        });
    }];
}

/**!
 * Ignore follower request
 */
- (void) followerRequestDismiss:(PPConsumerProfileNotification *)followerNotification cell:(FollowerTableViewCell*)cell indexPath:(NSIndexPath *) indexPath{
    
    NSString *followerId = [NSString stringWithFormat:@"%ld", (long) followerNotification.profile.wsId];
    __weak typeof(self) weakSelf = self;
    [WSSocial dismissFollower:followerId completion:^(BOOL success, enum FollowerStatus consumerStatus, enum FollowerStatus followerStatus, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error && error.code != 404) {
                [AlertMessage showAlertWithMessage:[error localizedDescription] controller:weakSelf];
            }
        });
    }];
}

/**!
 * Unfollow a consumer
 */
- (void) unfollowerConsumer:(PPConsumerProfileNotification *) followerNotification cell:(FollowerTableViewCell*) cell indexPath:(NSIndexPath *) indexPath text:(NSString *) text{
    __weak NotificationsViewController* selfWeak = self;
    FollowerTableViewCell* __weak weakCell = cell;
    __block FollowerStatus oldConsumerStatus = followerNotification.consumerStatusFollower;
    __block FollowerStatus oldFollowerStatus = followerNotification.followerStatusConsumer;

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:text message:nil preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Sim" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        followerNotification.followerStatusConsumer = FollowerStatusLoading;
        followerNotification.consumerStatusFollower = FollowerStatusLoading;
        [cell changeButtonWithStatus:FollowerStatusLoading consumerStatus:FollowerStatusLoading];

        NSString* idFollower = [NSString stringWithFormat:@"%ld", (long)followerNotification.profile.wsId];
        [WSSocial unfollow:idFollower completion:^(BOOL success, enum FollowerStatus consumerStatus, enum FollowerStatus followerStatus, NSError * _Nullable error) {

            dispatch_async(dispatch_get_main_queue(), ^{
                if(success){
                    // updates the notification to change the button
                    // in order to show feedback to the user
                    followerNotification.followerStatusConsumer = followerStatus;
                    followerNotification.consumerStatusFollower = consumerStatus;
                    [selfWeak.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                }else{
                    [AlertMessage showAlertWithMessage:[error localizedDescription] controller:selfWeak];
                    
                    if (!selfWeak || !weakCell) { return; }
                    
                    followerNotification.followerStatusConsumer = oldFollowerStatus;
                    followerNotification.consumerStatusFollower = oldConsumerStatus;
                    [weakCell changeButtonWithStatus: oldFollowerStatus consumerStatus: oldConsumerStatus];
                }
            });
        }];


    }];
    [alert addAction:okAction];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Não" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {

    }];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:^{

    }];
}

/**
 * Open the settings screen
 */
- (IBAction)openSettings:(id)sender {
    ToggleSettingsViewController* controller = ToggleSettingsViewController.new;
    [controller setHidesBottomBarWhenPushed:YES];
    [OBJCAnalytics logWithName:@"Notifications - Setup Accessed" properties:nil];
    [OBJCAnalytics logFirebaseWithName:@"notifications_setup_accessed" properties:nil];
    [self.navigationController pushViewController:controller animated:YES];
}

# pragma mark - FollowerTableViewCellDelegate

- (void) followerActionButtonDidTap:(FollowerTableViewCell*) cell notification:(PPConsumerProfileNotification *) notification action:(enum FollowerButtonAction)action{
    if (notification != nil){

        // Retrive the index for follower notification
        NSInteger index = [notifications indexOfObject:notification];
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];

        if([notification isKindOfClass:[PPConsumerProfileNotification class]]){
            PPConsumerProfileNotification *followerNotification = (PPConsumerProfileNotification *) notification;

            // If the current status of notification is waiting that means that the
            // user was tape over accept request button
            if (action == FollowerButtonActionAllow){
                [self acceptFollowerRequest:followerNotification indexPath:indexPath cell: cell];
                // Change the notification status to give feedback to the user
                followerNotification.followerStatusConsumer = FollowerStatusLoading;
                followerNotification.consumerStatusFollower = FollowerStatusLoading;
                [cell changeButtonWithStatus:FollowerStatusLoading consumerStatus:FollowerStatusLoading];

            }else if (action == FollowerButtonActionFollow){
                [self followConsumer:followerNotification indexPath:indexPath  cell: cell];
                // Change the notification status to give feedback to the user
                followerNotification.followerStatusConsumer = FollowerStatusLoading;
                followerNotification.consumerStatusFollower = FollowerStatusLoading;
                [cell changeButtonWithStatus:FollowerStatusLoading consumerStatus:FollowerStatusLoading];

            }
            else if (action == FollowerButtonActionDismiss){
                [cell runTimer];
            }
            else if (action == FollowerButtonActionUnfollow){
                NSString *text = [NSString stringWithFormat:@"Deseja deixar de seguir @%@",followerNotification.profile.username];
                [self unfollowerConsumer:followerNotification cell:cell indexPath:indexPath text:text];
            }
            else if (action == FollowerButtonActionCancelRequest){
                NSString *text = [NSString stringWithFormat:@"Deseja cancelar o pedido para seguir @%@",followerNotification.profile.username];
                [self unfollowerConsumer:followerNotification cell:cell indexPath:indexPath text:text];
            }
        }
    }
}

- (void) ignoreRequestButtonStatusChangedTo:(enum IgnoreRequestStatus)status sender:(FollowerTableViewCell *)sender {
    NSString *followerId = [NSString stringWithFormat:@"%ld", (long)sender.notification.profile.wsId];
    switch (status) {
        case IgnoreRequestStatusStarted:
            //Adiciona no Dictionary a flag que ainda não foi ignorado, mas o usuário já indicou para ignorar
            [self.notificationsIgnored setValue:@NO forKey: followerId];
            break;

        case IgnoreRequestStatusDone:
            //Adiciona no Dictionary a flag que já foi executado e ignorado
            [self.notificationsIgnored setValue:@YES forKey: followerId];
            [self followerRequestDismiss:sender.notification cell:sender indexPath:NULL];
            break;

        case IgnoreRequestStatusCanceled:
            //Remove do Dictionary o FollowerId, para exibir a cell como nova
            [self.notificationsIgnored removeObjectForKey: followerId];
            break;
    }
}

- (void) withdrawalActionButtonDidTap:(WithdrawalTableViewCell *) cell {
	[self goToWithdrawalScreen];
}

- (void)goToWithdrawalScreen{
    WithdrawOptionsHelper* helper = [WithdrawOptionsHelper new];
    [helper tryOpenWithdrawOptionsViewControllerFrom:self];
}

- (void)fixedValuePaymentWidgetDidClose{

}

- (void)fixedValuePaymentWillPresentReceipt{

}

#pragma mark - UIPPProfileImageDelegate

- (void) profileImageViewDidTap:(UIPPProfileImage *) profileImage contact:(PPContact *) contact{
    self.profileViewController = [NewProfileProxy openProfileWithContact:contact parent:self originView: profileImage];
}

#pragma mark - FollowerRequestDelegate

-(void)followerRequestWasIgnoredWithId:(NSInteger)wsId {
    id cellDataItem = [notifications objectAtIndex:0];
    if ([cellDataItem isKindOfClass:[NSArray class]]) {
        NSMutableArray *temp = [NSMutableArray arrayWithArray:[notifications objectAtIndex:0]];
        for (PPConsumerProfileNotification *notification in [notifications objectAtIndex:0]) {
            if (notification.profile.wsId == wsId) {
                [temp removeObject:notification];
            }
        }

        if (temp.count > 1) {
            //Altera os dados da celula
            id cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
            if ([cell isKindOfClass:[GroupFollowerTableViewCell class]]) {
                [(GroupFollowerTableViewCell *) cell configureCellWithNotifications:temp];
            }
            [(NSMutableArray *) notifications replaceObjectAtIndex:0 withObject:temp];
        } else {
            //Remove o Array de notificações de follower request e coloca somente a unica notifição do array
            [(NSMutableArray *) notifications replaceObjectAtIndex:0 withObject:[temp objectAtIndex:0]];
            //Força o refresh para alterar o tipo de cell
            [self refreshTableView];
        }
    } else if ([cellDataItem isKindOfClass:[PPConsumerProfileNotification class]]) {
        if (((PPConsumerProfileNotification*) cellDataItem).profile.wsId == wsId) {
            [(NSMutableArray *) notifications removeObjectAtIndex:0];
            [self refreshTableView];
        }
    }
}

-(void)followerRequestWasUpdated:(PPConsumerProfileNotification *)notification {
    if (![notifications containsObject:notification] && [[notifications objectAtIndex:0] isKindOfClass:[NSArray class]]) {
        NSMutableArray *followerRequest = [notifications objectAtIndex:0];
        NSArray *tempArray = [NSArray arrayWithArray:[notifications objectAtIndex:0]];

        for (id item in tempArray) {
            if ([item isKindOfClass:[PPConsumerProfileNotification class]]) {
                if (((PPConsumerProfileNotification *) item).profile.wsId == notification.profile.wsId) {
                    [followerRequest removeObject:item];
                    break;
                }
            }
        }

        NSUInteger requestsCount = followerRequest.count;

        if (requestsCount > 1) {
            //Apenas coloca a notificação que foi removida no array principal
            [(NSMutableArray *) notifications insertObject:notification atIndex:1];
        } else if (requestsCount == 1) {
            [(NSMutableArray *) notifications replaceObjectAtIndex:0 withObject:[followerRequest objectAtIndex:0]];
            [(NSMutableArray *) notifications insertObject:notification atIndex:1];
        } else {
            [(NSMutableArray *) notifications replaceObjectAtIndex:0 withObject:notification];
        }

        [self refreshTableView];
    }
}

@end
