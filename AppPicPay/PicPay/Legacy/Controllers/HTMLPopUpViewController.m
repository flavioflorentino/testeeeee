#import <UIKit/UIKit.h>
#import "HTMLPopUpViewController.h"
#import "PPAnalytics.h"

@implementation HTMLPopUpViewController

+ (HTMLPopUpViewController *)showPopWithHtml:(NSString *)htmlString parent:(UIViewController<HTMLPopUpViewDelegate> *)parent{
	
	UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
	BOOL animated = NO;
	HTMLPopUpViewController *viewController = [[HTMLPopUpViewController alloc] init];
	viewController.delegate = parent;
	
	viewController.htmlString = htmlString;
	
	[parent addChildViewController:viewController];
	[viewController viewWillAppear:animated];
	[parent.view addSubview:viewController.view];
	[viewController viewDidAppear:animated];
	[viewController didMoveToParentViewController:parent];
	[currentWindow addSubview:viewController.view];
	
	viewController.mainView.layer.cornerRadius = 5;
	viewController.mainView.layer.masksToBounds = YES;
    
	return viewController;
}

- (id)init {
	self = [super initWithNibName:@"HTMLPopUpViewController" bundle:nil];
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
    [self setupTextView];
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	
    self.view.translatesAutoresizingMaskIntoConstraints = NO;

	UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
	
	[currentWindow addConstraint:[NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:currentWindow attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
	
	[currentWindow addConstraint:[NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:currentWindow attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
	
	[currentWindow addConstraint:[NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:currentWindow attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
	
	[currentWindow addConstraint:[NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:currentWindow attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
	
	self.view.alpha = 0;
	[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
		self.view.alpha = 1;
	} completion:nil];
	
}

- (void)setupTextView {
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]
              initWithData: [_htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                   options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
        documentAttributes: nil
                     error: nil];
    
    [attributedString enumerateAttribute:NSFontAttributeName
                                 inRange:NSMakeRange(0, attributedString.length)
                                 options:0
                              usingBlock:^(id value, NSRange range, BOOL *stop) {
        UIFont *font = value;
        font = [font fontWithSize:font.pointSize * 1.33];
        [attributedString removeAttribute:NSFontAttributeName range:range];
        [attributedString addAttribute:NSFontAttributeName value:font range:range];
    }];
    
    _textView.attributedText = attributedString;
    _textView.bounces = NO;
}

- (IBAction)close:(id)sender{
	[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
		self.view.alpha = 0;
	} completion:^(BOOL finished) {
		self.view.hidden = YES;
		[self.view removeFromSuperview];
		
		if ([self.delegate respondsToSelector:@selector(popUpDidClose)]) {
			[self.delegate popUpDidClose];
		}
	}];
}

@end
