//
//  UILabel+ColorInRanges.h
//  PicPay
//
//  Created by Diogo Carneiro on 07/03/13.
//
//

#import <UIKit/UIKit.h>

@interface UILabel (ColorInRanges)

- (void)changeTextColor:(UIColor *)color forStringsWithPattern:(NSString *)pattern;
- (void)changeFont:(UIFont *)font forStringsWithPattern:(NSString *)pattern;
- (void)changeFont:(UIFont *)font andColor:(UIColor *)color forStringsWithPattern:(NSString *)pattern;

@end
