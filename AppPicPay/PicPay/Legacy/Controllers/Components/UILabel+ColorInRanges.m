//
//  UILabel+ColorInRanges.m
//  PicPay
//
//  Created by Diogo Carneiro on 07/03/13.
//
//

#import "UILabel+ColorInRanges.h"
#import "NSString+RegularExpressionSearch.h"

@implementation UILabel (ColorInRanges)

/*
 *  System Versioning Preprocessor Macros
 */

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

- (void)changeTextColor:(UIColor *)color forStringsWithPattern:(NSString *)pattern{
	@try {
		
		if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
			NSArray *currencyValuesRangeInString = [self.text stringRangesWithRegularExpressionPattern:pattern];
			NSMutableAttributedString *attrsString = [[NSMutableAttributedString alloc] initWithString:self.text];
			
			for (NSValue *currencyValueRange in currencyValuesRangeInString) {
				[attrsString addAttribute:NSForegroundColorAttributeName value:color range:[currencyValueRange rangeValue]];
			}
			
			[self setAttributedText:attrsString];
		}
	}
	@catch (NSException *exception) {
		
	}
}

- (void)changeFont:(UIFont *)font forStringsWithPattern:(NSString *)pattern{
	@try {
		
		if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
			NSArray *currencyValuesRangeInString = [self.text stringRangesWithRegularExpressionPattern:pattern];
			NSMutableAttributedString *attrsString = [[NSMutableAttributedString alloc] initWithString:self.text];
			
			for (NSValue *currencyValueRange in currencyValuesRangeInString) {
				[attrsString addAttribute:NSFontAttributeName value:font range:[currencyValueRange rangeValue]];
			}
			
			[self setAttributedText:attrsString];
		}
	}
	@catch (NSException *exception) {
		
	}
}

- (void)changeFont:(UIFont *)font andColor:(UIColor *)color forStringsWithPattern:(NSString *)pattern{
	@try {
		
		if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
			NSArray *currencyValuesRangeInString = [self.text stringRangesWithRegularExpressionPattern:pattern];
			NSMutableAttributedString *attrsString = [[NSMutableAttributedString alloc] initWithString:self.text];
			
			for (NSValue *currencyValueRange in currencyValuesRangeInString) {
				[attrsString addAttribute:NSFontAttributeName value:font range:[currencyValueRange rangeValue]];
				[attrsString addAttribute:NSForegroundColorAttributeName value:color range:[currencyValueRange rangeValue]];
				
			}
			
			[self setAttributedText:attrsString];
		}
	}
	@catch (NSException *exception) {
		
	}
}

@end
