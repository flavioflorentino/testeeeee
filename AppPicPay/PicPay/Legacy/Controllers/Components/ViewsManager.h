//
//  ViewsManager.h
//  PicPay
//
//  Created by Diogo Carneiro on 17/01/13.
//
//

#import <UIKit/UIKit.h>

@interface ViewsManager : NSObject

/*
 * EventStoryboard methods
 */
+ (UIViewController*)eventStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier;
+ (UIViewController*)eventStoryboardFirtViewController;
+ (UINavigationController*)eventStoryboardFirtNavigationController;


/*
 * TicketStoryboard methods
 */
+ (UIViewController*)ticketsStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier;
+ (UIViewController*)ticketsStoryboardFirtViewController;
+ (UINavigationController*)ticketsStoryboardFirtNavigationController;
+ (UIViewController*)ticketViewController;


/*
 * PaymentStoryboard methods
 */

+ (UIViewController*)paymentStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier;
+ (UIViewController*)paymentStoryboardFirstViewController;
+ (UINavigationController*)paymentStoryboardFirtNavigationController;

/*
 * PeerToPeerStoryboard methods
 */
+ (UIViewController*)peerToPeerStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier;
+ (UIViewController*)peerToPeerStoryboardFirtViewController;
+ (UINavigationController*)peerToPeerStoryboardFirtNavigationController;

/*
 * RegistrationStoryboard methods
 */
//+ (UIViewController*)registrationStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier;
//+ (UIViewController*)registrationStoryboardFirtViewController;
+ (UINavigationController*)registrationStoryboardFirtNavigationController;
+ (UIViewController*)registrationStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier;

/*
 * CreditStoryboard methods
 */
+ (UINavigationController*)creditStoryboardFirtNavigationController;
+ (UIViewController*)creditStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier;


/*
 * WalletStoryboard methods
 */
+ (UINavigationController*)walletStoryboardFirtNavigationController;
+ (UIViewController*)walletStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier;

/*
 * AntiFraudStoryboard methods
 */
+ (UIViewController*)antiFraudStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier;
+ (UIViewController*)antiFraudStoryboardFirtViewController;

/*
 * SocialStoryboard methods
 */
+ (UIViewController*)socialStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier;
+ (UIViewController*)emptyMessageStoryboardFirtViewController;
- (UIStoryboard *)emptyMessageStoryboard;

/*
 * SettingsStoryboard methods
 */
+ (UINavigationController*)settingsStoryboardFirtNavigationController;
+ (UIViewController*)settingsStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier;

/*
 * PhoneRegistrationStoryboard methods
 */
+ (UINavigationController*)phoneRegistrationStoryboardFirtNavigationController;
+ (UIViewController*)phoneRegistrationStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier;


/*
 * PaymentMethodsStoryboard methods
 */
+ (UIViewController*)paymentMethodsStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier;


// People search

+ (UINavigationController*)peopleSearchStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier;
+ (UIViewController*)peopleSearchStoryboardViewController:(NSString*)viewControllerIdentifier;

/*
 * Load Xibs
 */
+ (UIView *)loadViewWithXibName:(NSString *)xibName;
+ (UIView *)loadViewWithXibName:(NSString *)xibName xibIndex:(NSInteger)xibIndex;

/*!
 * Gets the current view controller (The Top ViewController)
 * @return UIViewController top view Controller
 */
+ (UIViewController*)topViewController;

+ (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController;

+ (void)presentSafariViewController:(NSURL *) url From:(UIViewController *) parent;

@end

