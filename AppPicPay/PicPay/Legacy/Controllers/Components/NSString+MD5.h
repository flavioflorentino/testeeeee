//
//  NSString+MD5.h
//  PicPay
//
//  Created by Diogo Carneiro on 20/01/14.
//
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

- (NSString *)MD5String;

@end
