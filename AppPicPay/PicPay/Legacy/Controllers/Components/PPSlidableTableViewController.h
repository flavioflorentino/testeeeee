//
//  PPSlidableTableViewController.h
//  PicPay
//
//  Created by Diogo Carneiro on 18/12/15.
//
//

#import <UIKit/UIKit.h>

@interface PPSlidableTableViewController : UITableViewController

- (IBAction)menuButtonTapped:(id)sender;

@end
