//
//  NSString+OnlyNumbers.h
//  PicPay
//
//  Created by Diogo Carneiro on 20/09/13.
//
//

#import <Foundation/Foundation.h>

@interface NSString (OnlyNumbers)

- (NSString *)onlyNumbers;

@end
