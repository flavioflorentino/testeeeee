import Foundation

extension ViewsManager {    
    static func presentWebViewController(withUrl: String?, sendHeaders: Bool = false, customHeader: [String: String]? = nil, fromViewController: UIViewController, title: String = "", completionHandler: ((_ error: Error?) -> Void)? = nil) {
        guard let urlString = withUrl, let url = URL(string: urlString) else {
            return
        }
                        
        let webviewController = WebViewFactory.make(with: url, includeHeaders: sendHeaders, customHeaders: customHeader, completion: completionHandler)
        webviewController.title = title
        fromViewController.present(webviewController, animated: true)
        BreadcrumbManager.shared.addCrumb("WebView", typeOf: .webView, withProperties: ["url": url.absoluteString])
    }
    
    static func pushWebViewController(withUrl: String?, sendHeaders: Bool = false, customHeader: [String: String]? = nil, fromViewController: UIViewController, title: String = "", hideBottomBar: Bool = false, completionHandler: ((_ error: Error?) -> Void)? = nil) {
        guard let urlString = withUrl, let url = URL(string: urlString) else {
            return
        }
        
        let webviewController = WebViewFactory.make(with: url, includeHeaders: sendHeaders, customHeaders: customHeader, completion: completionHandler)
           
        webviewController.title = title
        webviewController.hidesBottomBarWhenPushed = hideBottomBar
        fromViewController.navigationController?.pushViewController(webviewController, animated: true)
        BreadcrumbManager.shared.addCrumb("WebView", typeOf: .webView, withProperties: ["url": url.absoluteString])
    }
}
