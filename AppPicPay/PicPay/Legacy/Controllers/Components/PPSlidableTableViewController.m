//
//  PPSlidableTableViewController.m
//  PicPay
//
//  Created by Diogo Carneiro on 18/12/15.
//
//

#import "PPSlidableTableViewController.h"

@import CoreLegacy;

@interface PPSlidableTableViewController ()
@property (nonatomic, strong) UIPanGestureRecognizer *dynamicTransitionPanGesture;
@end

@implementation PPSlidableTableViewController

- (void)viewDidLoad{
	[super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	
}

- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	
	//[AppParameters globalParameters].sidebarMenuCanPan = YES;
}

- (void)viewDidDisappear:(BOOL)animated{
	//[AppParameters globalParameters].sidebarMenuCanPan = NO;
}

- (IBAction)menuButtonTapped:(id)sender {
	//[self.slidingViewController anchorTopViewToRightAnimated:YES];
}

@end
