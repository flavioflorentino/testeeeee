//
//  NSString+OnlyNumbers.m
//  PicPay
//
//  Created by Diogo Carneiro on 20/09/13.
//
//

#import "NSString+OnlyNumbers.h"

@implementation NSString (OnlyNumbers)

- (NSString *)onlyNumbers{
	NSString *originalString = self;
	NSMutableString *strippedString = [NSMutableString
									   stringWithCapacity:originalString.length];
	
	NSScanner *scanner = [NSScanner scannerWithString:originalString];
	NSCharacterSet *numbers = [NSCharacterSet
							   characterSetWithCharactersInString:@"0123456789"];
	
	while ([scanner isAtEnd] == NO) {
		NSString *buffer;
		if ([scanner scanCharactersFromSet:numbers intoString:&buffer]) {
			[strippedString appendString:buffer];
			
		} else {
			[scanner setScanLocation:([scanner scanLocation] + 1)];
		}
	}
	
	return strippedString;
}

@end
