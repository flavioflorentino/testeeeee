//
//  ViewsManager.m
//  PicPay
//
//  Created by Diogo Carneiro on 17/01/13.
//
//

#import "ViewsManager.h"
#import <SafariServices/SafariServices.h>
#import "PicPay-Swift.h"

@implementation ViewsManager

- (UIStoryboard *)storyboardWithIdentifier:(NSString*)storyboardIdentifier{
	return [UIStoryboard storyboardWithName:storyboardIdentifier bundle:nil];
}

- (UIStoryboard *)eventStoryboard{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager storyboardWithIdentifier:@"EventStoryboard"];
	return storyboard;
}

- (UIStoryboard *)ticketsStoryboard{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager storyboardWithIdentifier:@"TicketsStoryboard"];
	return storyboard;
}

- (UIStoryboard *)emptyMessageStoryboard{
    ViewsManager *viewsManager = [[ViewsManager alloc] init];
    UIStoryboard *storyboard = [viewsManager storyboardWithIdentifier:@"EmptyMessageViewController"];
    return storyboard;
}

- (UIStoryboard *)paymentStoryboard{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager storyboardWithIdentifier:@"PaymentStoryboard"];
	return storyboard;
}

- (UIStoryboard *)peerToPeerStoryboard{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager storyboardWithIdentifier:@"PeerToPeerStoryboard"];
	return storyboard;
}

- (UIStoryboard *)registrationStoryboard{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager storyboardWithIdentifier:@"RegistrationStoryboard"];
	return storyboard;
}

- (UIStoryboard *)creditStoryboard{
    ViewsManager *viewsManager = [[ViewsManager alloc] init];
    UIStoryboard *storyboard = [viewsManager storyboardWithIdentifier:@"Credit"];
    return storyboard;
}

- (UIStoryboard *)walletStoryboard{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager storyboardWithIdentifier:@"WalletStoryboard"];
	return storyboard;
}

- (UIStoryboard *)antiFraudStoryboard{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager storyboardWithIdentifier:@"AntiFraudStoryboard"];
	return storyboard;
}

- (UIStoryboard *)settingsStoryboard{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager storyboardWithIdentifier:@"SettingsStoryboard"];
	return storyboard;
}

- (UIStoryboard *)phoneRegistrationStoryboard{
    ViewsManager *viewsManager = [[ViewsManager alloc] init];
    UIStoryboard *storyboard = [viewsManager storyboardWithIdentifier:@"PhoneRegistration"];
    return storyboard;
}

- (UIStoryboard *)paymentMethodsStoryboard{
    ViewsManager *viewsManager = [[ViewsManager alloc] init];
    UIStoryboard *storyboard = [viewsManager storyboardWithIdentifier:@"PaymentMethods"];
    return storyboard;
}


- (UIStoryboard*)socialStoryboard{
    ViewsManager *viewsManager = [[ViewsManager alloc] init];
    UIStoryboard *storyboard = [viewsManager storyboardWithIdentifier:@"SocialStoryboard"];
    return storyboard;
}

- (UIStoryboard*)peopleSearchStoryboard{
    ViewsManager *viewsManager = [[ViewsManager alloc] init];
    UIStoryboard *storyboard = [viewsManager storyboardWithIdentifier:@"PeopleSearchStoryboard"];
    return storyboard;
}

+ (UIViewController*)eventStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager eventStoryboard];
	UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:viewControllerIdentifier];
	return viewController;
}

+ (UIViewController*)ticketsStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager ticketsStoryboard];
	UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:viewControllerIdentifier];
	return viewController;
}

+ (UIViewController*)eventStoryboardFirtViewController{
	return [ViewsManager eventStoryboardViewControllerWithIdentifier:@"EventDetailsViewController"];
}

+ (UIViewController*)ticketsStoryboardFirtViewController{
	return [ViewsManager ticketsStoryboardViewControllerWithIdentifier:@"TicketListViewController"];
}

+ (UINavigationController*)eventStoryboardFirtNavigationController{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager eventStoryboard];
	UINavigationController *navigationController = [storyboard instantiateInitialViewController];
	return navigationController;
}

+ (UINavigationController*)ticketsStoryboardFirtNavigationController{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager ticketsStoryboard];
	UINavigationController *navigationController = [storyboard instantiateInitialViewController];
	return navigationController;
}

+ (UIViewController*)ticketViewController{
	return [ViewsManager ticketsStoryboardViewControllerWithIdentifier:@"TicketViewController"];
}

+ (UIViewController*)paymentStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager paymentStoryboard];
	UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:viewControllerIdentifier];
	return viewController;
}

+ (UIViewController*)emptyMessageStoryboardFirtViewController{
    ViewsManager *viewsManager = [[ViewsManager alloc] init];
    UIStoryboard *storyboard = [viewsManager emptyMessageStoryboard];
    UIViewController *viewController = [storyboard instantiateInitialViewController];
    return viewController;
}


+ (UIViewController*)paymentStoryboardFirstViewController{
	return [ViewsManager paymentStoryboardViewControllerWithIdentifier:@"PaymentViewController"];
}

+ (UINavigationController*)paymentStoryboardFirtNavigationController{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager paymentStoryboard];
	UINavigationController *navigationController = [storyboard instantiateInitialViewController];
	return navigationController;
}

+ (UIView *)loadViewWithXibName:(NSString *)xibName{
    return [[[NSBundle mainBundle] loadNibNamed:xibName owner:self options:nil] objectAtIndex:0];
}
+ (UIView *)loadViewWithXibName:(NSString *)xibName xibIndex:(NSInteger)xibIndex
{
    // method with generic implementation just for shut up warning message. If you need to use, please, implements 
    return [UIView init];
}

/*
 * PeerToPeerStoryboard methods
 */
+ (UIViewController*)peerToPeerStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager peerToPeerStoryboard];
	UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:viewControllerIdentifier];
	return viewController;
}

+ (UIViewController*)peerToPeerStoryboardFirtViewController{
	return [ViewsManager peerToPeerStoryboardViewControllerWithIdentifier:@"P2PPayment"];
}

+ (UINavigationController*)peerToPeerStoryboardFirtNavigationController{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager peerToPeerStoryboard];
	UINavigationController *navigationController = [storyboard instantiateInitialViewController];
	return navigationController;
}

/*
 * RegistrationStoryboard methods
 */
+ (UINavigationController*)registrationStoryboardFirtNavigationController{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager registrationStoryboard];
	UINavigationController *navigationController = [storyboard instantiateInitialViewController];
	return navigationController;
}

+ (UIViewController*)registrationStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager registrationStoryboard];
	UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:viewControllerIdentifier];
	return viewController;
}

/*
 * RegistrationStoryboard methods
 */
+ (UINavigationController*)creditStoryboardFirtNavigationController{
    ViewsManager *viewsManager = [[ViewsManager alloc] init];
    UIStoryboard *storyboard = [viewsManager registrationStoryboard];
    UINavigationController *navigationController = [storyboard instantiateInitialViewController];
    return navigationController;
}

+ (UIViewController*)creditStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier{
    ViewsManager *viewsManager = [[ViewsManager alloc] init];
    UIStoryboard *storyboard = [viewsManager registrationStoryboard];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:viewControllerIdentifier];
    return viewController;
}

/*
 * WalletStoryboard methods
 */
+ (UINavigationController*)walletStoryboardFirtNavigationController{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager walletStoryboard];
	UINavigationController *navigationController = [storyboard instantiateInitialViewController];
	return navigationController;
}

+ (UIViewController*)walletStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager walletStoryboard];
	UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:viewControllerIdentifier];
	return viewController;
}

#pragma mark Anti Fraud

/*
 * AntiFraudStoryboard methods
 */
+ (UIViewController*)antiFraudStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager antiFraudStoryboard];
	UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:viewControllerIdentifier];
	return viewController;
}


+ (UIViewController*)antiFraudStoryboardFirtViewController{
	ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager antiFraudStoryboard];
	UINavigationController *navigationController = [storyboard instantiateInitialViewController];
	return navigationController;
}


/*
 * SettingsStoryboard methods
 */
+ (UINavigationController*)settingsStoryboardFirtNavigationController{
    ViewsManager *viewsManager = [[ViewsManager alloc] init];
	UIStoryboard *storyboard = [viewsManager settingsStoryboard];
	UINavigationController *navigationController = [storyboard instantiateInitialViewController];
	return navigationController;
}

/*
 * SettingsStoryboard methods
 */
+ (UINavigationController*)phoneRegistrationStoryboardFirtNavigationController{
    ViewsManager *viewsManager = [[ViewsManager alloc] init];
    UIStoryboard *storyboard = [viewsManager phoneRegistrationStoryboard];
    UINavigationController *navigationController = [storyboard instantiateInitialViewController];
    return navigationController;
}

#pragma mark Social

/*
 * SocialStoryboard methods
 */

+ (UIViewController*)socialStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier{
    ViewsManager *viewsManager = [[ViewsManager alloc] init];
    UIStoryboard *storyboard = [viewsManager socialStoryboard];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:viewControllerIdentifier];
    return viewController;
}

+ (UINavigationController*)peopleSearchStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier{
    ViewsManager *viewsManager = [[ViewsManager alloc] init];
    UIStoryboard *storyboard = [viewsManager peopleSearchStoryboard];
    UINavigationController *navigationController = [storyboard instantiateInitialViewController];
    return navigationController;
}
    
+ (UIViewController*)peopleSearchStoryboardViewController:(NSString*)viewControllerIdentifier{
    ViewsManager *viewsManager = [[ViewsManager alloc] init];
    UIStoryboard *storyboard = [viewsManager peopleSearchStoryboard];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:viewControllerIdentifier];
    return viewController;
}

+ (UIViewController*)settingsStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier{
    ViewsManager *viewsManager = [[ViewsManager alloc] init];
    UIStoryboard *storyboard = [viewsManager settingsStoryboard];
	UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:viewControllerIdentifier];
	return viewController;
}

+ (UIViewController*)phoneRegistrationStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier{
    ViewsManager *viewsManager = [[ViewsManager alloc] init];
    UIStoryboard *storyboard = [viewsManager phoneRegistrationStoryboard];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:viewControllerIdentifier];
    return viewController;
}

#pragma mart - Payment Methods

+ (UIViewController*)paymentMethodsStoryboardViewControllerWithIdentifier:(NSString*)viewControllerIdentifier{
    ViewsManager *viewsManager = [[ViewsManager alloc] init];
    UIStoryboard *storyboard = [viewsManager paymentMethodsStoryboard];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:viewControllerIdentifier];
    return viewController;
}

/*!
 * Gets the current view controller (The Top ViewController)
 * @return UIViewController top view Controller
 */
+ (UIViewController*)topViewController {
	return [ViewsManager topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

+ (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
	if ([rootViewController isKindOfClass:[UITabBarController class]]) {
		UITabBarController* tabBarController = (UITabBarController*)rootViewController;
		return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
	} else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
		UINavigationController* navigationController = (UINavigationController*)rootViewController;
		return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
	} else if (rootViewController.presentedViewController) {
		UIViewController* presentedViewController = rootViewController.presentedViewController;
		return [self topViewControllerWithRootViewController:presentedViewController];
	} else {
		return rootViewController;
	}
}

+ (void)presentSafariViewController:(NSURL *) url From:(UIViewController *) parent {
    SFSafariViewController * safariController = [[SFSafariViewController alloc] initWithURL:url];
    
    if (@available(iOS 11.0, *)) {
        safariController.dismissButtonStyle = SFSafariViewControllerDismissButtonStyleClose;
    }
    
    [[BreadcrumbManager shared] addCrumb:@"SafariView" typeOf:CrumbTypeWebView withProperties:@{ @"url" : url.absoluteString}];
    
    [parent presentViewController:safariController animated:YES completion:nil];
}

@end
