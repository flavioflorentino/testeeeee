//
//  PPSlidableViewController.h
//  PicPay
//
//  Created by Diogo Carneiro on 02/04/15.
//
//

#import <UIKit/UIKit.h>
#import "PPBaseViewControllerObj.h"

@interface PPSlidableViewController : PPBaseViewControllerObj

- (IBAction)menuButtonTapped:(id)sender;

@end
