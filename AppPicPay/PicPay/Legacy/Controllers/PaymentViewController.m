#import "PaymentViewController.h"
#import "ViewsManager.h"
#import "WSTransaction.h"
#import "NSString+RegularExpressionSearch.h"
#import "UILabel+ColorInRanges.h"
#import "PPAuth.h"
#import "WSItemRequest.h"
#import "HTMLPopUpViewController.h"
#import "PicPay-Swift.h"

@import AnalyticsModule;
@import CoreLegacy;
@import FeatureFlag;
@import QuartzCore;
@import UI;

@implementation UIView (fadeIn)

- (void)pulse{
	CABasicAnimation *theAnimation;
	//	self.alpha = 0;
	self.hidden = NO;
	theAnimation=[CABasicAnimation animationWithKeyPath:@"opacity"];
	theAnimation.duration=.1;
	theAnimation.repeatCount=2;
	theAnimation.autoreverses=YES;
	theAnimation.fromValue=[NSNumber numberWithFloat:0];
	theAnimation.toValue=[NSNumber numberWithFloat:1.0];
	[self.layer addAnimation:theAnimation forKey:@"animateOpacity"];
}

@end

@interface PaymentViewController () <UIPPProfileImageDelegate, InstallmentsSelectorViewModelDelegate>

@property PaymentToolbarController *paymentToolbarController;
@property InstallmentsTooltip *installmentsTooltip;
@property PciPavHelper *cvvHelper;
@property (strong, nonatomic, nullable) EstablishmentDetailsCoordinator *establishmentDetailsCoordinator;

@end

@implementation PaymentViewController

@synthesize viewLargeGreenButtonContainer, viewValueInputContainer, labelCreditDescription,carPlateTextField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL) shouldAddBreadcrumb {
    return false;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Voltar"
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
    
    _cvvHelper = [PciPavHelper new];
    
    if (self.presentingViewController != nil|| self.forceModal) {
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancelar" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    }
	
	_horizontalLine.translatesAutoresizingMaskIntoConstraints = YES;
	_horizontalLine.frame = CGRectMake(_horizontalLine.frame.origin.x, _horizontalLine.frame.origin.y, self.view.frame.size.width, 1/[[UIScreen mainScreen] scale]);

    __weak PaymentViewController* weakSelf = self;
    _paymentToolbarController = [[PaymentToolbarController alloc] initWithPaymentManager:paymentManager parent:self frame:CGRectMake(0, 0, self.view.frame.size.width, 92) pay:^(NSString * _Nonnull privacy) {
        weakSelf.privacyConfig = privacy;
        [weakSelf checkout:nil];
    }];

	[self.valueInputTextField setInputAccessoryView:_paymentToolbarController.toolbar];
    
    _installmentsTooltip = [[InstallmentsTooltip alloc] initWithView:self.view toolbarView:_paymentToolbarController.toolbar.view];
    
	if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
		
		locationManager = [[CLLocationManager alloc] init];
		locationManager.delegate = self;
		
		if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
			[locationManager requestWhenInUseAuthorization];
		}
		
		locationManager.distanceFilter = kCLDistanceFilterNone;
		locationManager.desiredAccuracy = kCLLocationAccuracyBest;
		[locationManager startUpdatingLocation];
	}
    
    loadingItemView = [[UILoadView alloc] initWithSuperview:self.view position:PositionCenter activityStyle: UIActivityIndicatorViewStyleGray];
    loadingItemView.layer.opacity = 0.7;
    loadingItemView.hidden = YES;
	
	if(_item != nil){
		[self itemIsReady];
	} else {
		
		loadingItemView.hidden = NO;
        [loadingItemView startLoading];
        
        if (self.store != nil ){
            [self getItemInfo: self.store];
        } else {
            [self getTransactionInfoWith: self.transactionHashValue];
        }
	}
    
    [self configureValueField];
    if (self.showCancel) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelTapped)];
    }
    [self setupColors];
    [self updatePaymentInfo];
    [PPAnalytics trackFirstTimeOnlyEvent:@"FT tela transação PAV" properties:@{@"Origem":_touchOrigin ? _touchOrigin : @""}];
}

-(void) getTransactionInfoWith:(NSString *) hash {
    __weak PaymentViewController* weakSelf = self;

    [loadingItemView animatedRemoveFromSuperView:nil];
    [AlertMessage showAlertWithTitle:@"Ops! " message:@"Problemas com sua conexão, tente novamente." controller:weakSelf completion: nil];
}

-(void) getItemInfo:(PPStore *) store {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void) {

       //NSError *serviceError;
       __weak PaymentViewController* weakSelf = self;
       [WSItemRequest itemByStoreId:[NSString stringWithFormat:@"%lu", (long)self.store.wsId] andSellerId:self.store.sellerId completionHandler:^(MBItem *item, NSError *serviceError) {
           
           dispatch_async(dispatch_get_main_queue(), ^(void) {
               
               if (!item) {
                   if ([serviceError code] == NSURLErrorTimedOut){ //-1001
                       [AlertMessage showAlertWithTitle:@"Ops! " message:@"Problemas com sua conexão, tente novamente." controller:weakSelf completion: nil];
                   } else if ([serviceError code] == 1200) {
                       [AlertMessage showAlertWithMessage:@"Este produto não existe. Tente escanear novamente ou comunicar o problema ao vendedor." controller:weakSelf];
                   } else if ([serviceError code] == 1100) {
                       [AlertMessage showAlertWithMessage:@"Este é um QR Code comum. Ele não representa um produto comprável." controller:weakSelf];
                   } else {
                       [AlertMessage showAlertWithMessage:[serviceError localizedDescription] controller:weakSelf];
                   }
                   
                   [[weakSelf navigationController] popViewControllerAnimated:YES];
               } else {
                   if([item.seller.currentPlanType isKindOfPlan:'A']){
                       weakSelf.item = item;
                       [weakSelf itemIsReady];
                       
                   } else {
                       [AlertMessage showAlertWithTitle:@"Ops! " message:@"Ocorreu um erro ao tentar exibir as informações do item. Tente escanear novamente ou comunicar o problema ao vendedor." controller:weakSelf completion: nil];
                       [[weakSelf navigationController] popViewControllerAnimated:YES];
                   }
               }
               [loadingItemView animatedRemoveFromSuperView:nil];
           });
       }];
   });
}

- (void)setupColors {
    self.view.backgroundColor = PaletteObjc.ppColorGrayscale000;
    self.inputContainer.backgroundColor = PaletteObjc.ppColorGrayscale000;
    self.horizontalLine.backgroundColor = PaletteObjc.ppColorGrayscale200;
    
    self.labelStoreName.textColor = PaletteObjc.ppColorGrayscale500;
    self.valueInputTextField.textColor = PaletteObjc.ppColorGrayscale500;
    self.parkingTimeTitleLabel.textColor = PaletteObjc.ppColorGrayscale400;
    self.parkingTimeLabel.textColor = PaletteObjc.ppColorGrayscale500;
    self.carPlateTitleLabel.textColor = PaletteObjc.ppColorGrayscale400;
}

- (void)itemIsReady{
	
	paymentManager = [[PPPaymentManager alloc] init];
	[paymentManager changePaymentConfig:_item.seller.paymentConfig];
	paymentManager.subtotal = [NSDecimalNumber zero];
	paymentManager.sellerLocalDiscount = self.item.consumer_credit_value_seller;
    
    _paymentToolbarController.paymentManager = paymentManager;
	
	if ([_item.consumer_credit_description length] > 0) {
		self.labelCreditDescription.hidden = NO;
		self.labelCreditDescription.text = _item.consumer_credit_description;
	}else{
		self.labelCreditDescription.hidden = YES;
	}
	
	/* Variables to set up the Helper RewardProgram */
	rewardHelper = [[RewardProgramProgressViewController alloc] initWithNibName:@"RewardProgramProgressViewLight" bundle:nil];
	rewardHelper.rewardProgram = _item.rewardProgram;
	rewardHelper.view.frame = CGRectMake(rewardHelper.view.frame.origin.x, rewardHelper.view.frame.origin.y + 8, rewardHelper.view.frame.size.width, rewardHelper.view.frame.size.height);
	
	[self.viewForFidelityInfo addSubview:rewardHelper.view];
    
	self.labelStoreName.text = [[[self item] seller] name];
    
    UIPPProfileImage *profileImage = (UIPPProfileImage *) self.profile;
    profileImage.delegate = self;
    if (self.store != nil && !self.item){
         [profileImage setStore:self.store];
    }else{
        PPStore *store = [[PPStore alloc] init];
        store.sellerId = [NSString stringWithFormat:@"%ld", (long) _item.seller.wsId];
        store.img_url = _item.seller.logoUrl;
        store.isVerified = _item.seller.isVerified;
        [profileImage setStore:store];
    }
    
	if (_item.isParkingPayment == IsOldParking) {
		
		CGFloat pickerHeight = 216;
		
		if ([[AppParameters globalParameters] threeAndAHalfInchesDisplay]) {
			[_viewForMeasuringPickerViewHeight layoutIfNeeded];
			pickerHeight = _viewForMeasuringPickerViewHeight.bounds.size.height;
			_viewForParkingBottomConstraint.constant = 216;
		}
		
		pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, pickerHeight)];
		pickerView.dataSource = self;
		pickerView.delegate = self;
		self.valueInputTextField.inputView = pickerView;
		self.valueInputTextField.text = [CurrencyFormatter brazillianRealStringFromNumber:[(PPParkingTime *)[_item.parkingTimes objectAtIndex:0] totalValue]];
		
		[pickerView selectRow:_item.defaultTime inComponent:0 animated:YES];
		[self pickerView:pickerView didSelectRow:_item.defaultTime inComponent:0];
		
		self.parkingPaymentFieldsView.hidden = NO;
		
		if ([_item.parkingPlates count] > 0) {
			[self setLicensePlate:[(PPParkingCarPlate *)[_item.parkingPlates lastObject] plate]];
		}
		
		self.installmentsButton.hidden = YES;
	}
	
	
	
	
	[self.valueInputTextField becomeFirstResponder];
	itemIsReady = YES;
	
	[self updatePaymentInfo];
	[rewardHelper animateProgressBarToCurrentProgress];
    
    if (_initialValue && ![_initialValue isEqualToString:@""]) {
        [self textField:_valueInputTextField shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:self.initialValue];
    }
    
    [self configureValueField];
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
	if ( _bestGpsAcc > 0 && newLocation.horizontalAccuracy > 0 && newLocation.horizontalAccuracy < _bestGpsAcc) {
		_bestGpsAcc = newLocation.horizontalAccuracy;
		_bestGpsLat = newLocation.coordinate.latitude;
		_bestGpsLng = newLocation.coordinate.longitude;
	}
}

- (void)updatePaymentInfo{
    UIButton *installmentButton;
    
    if ([FeatureManager isActiveFeatureInstallmentNewButtonOBJC]) {
        installmentButton = _enhancedInstallmentButton;
        [_enhancedInstallmentButton setHidden: NO];
        _headerStackView.alignment = UIStackViewAlignmentTop;
        [_installmentsButton setHidden: YES];
        
        [self configureInstallmentButton:installmentButton completed: ^{
            installmentButton.contentEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
            [installmentButton setTitle:[NSString stringWithFormat:@"Parcelar em %ldx", (long)[paymentManager selectedQuotaQuantity]] forState:UIControlStateNormal];
        }];
    } else {
        installmentButton = _installmentsButton;
        [_enhancedInstallmentButton setHidden:YES];
        _headerStackView.alignment = UIStackViewAlignmentCenter;
        [_installmentsButton setHidden: NO];
        
        [self configureInstallmentButton:installmentButton completed: ^{
           [installmentButton setTitle:[NSString stringWithFormat:@"%ldx", (long)[paymentManager selectedQuotaQuantity]] forState:UIControlStateNormal];
        }];
    }
}

- (void)configureInstallmentButton:(UIButton *)installmentButton completed:(void (^)(void))completion {
    if([[paymentManager total] compare:[NSNumber numberWithInt:0]] == NSOrderedSame || [[paymentManager cardTotal] compare:[NSNumber numberWithInt:0]] != NSOrderedDescending){ //total == 0 || cartTotal <= 0
        [installmentButton setTintColor:[PaletteObjc ppColorGrayscale400]];
        installmentButton.layer.borderWidth = 1/[[UIScreen mainScreen] scale];
    }else{
        [installmentButton setTintColor:[PaletteObjc ppColorBranding400]];
        installmentButton.layer.borderWidth = 1.0f;
        
        [_installmentsTooltip showWithView:_paymentDetailView from:installmentButton cardValue:paymentManager.cardTotal];
    }
    
    installmentButton.layer.borderColor = [installmentButton.tintColor CGColor];
    installmentButton.layer.cornerRadius = 7.0f;
    
    if ([paymentManager selectedQuotaQuantity] > [[paymentManager installmentsList] count]) {
        if ([[paymentManager installmentsList] count] > 0) {
            [paymentManager setSelectedQuotaQuantity:[[paymentManager installmentsList] count]];
        }else{
            [paymentManager setSelectedQuotaQuantity:1];
        }
        
    }
    
    completion();
}

- (IBAction)changeInstallments:(id)sender{
    if([[paymentManager total] compare:[NSNumber numberWithInt:0]] == NSOrderedSame){ //total == 0
        NSString* title = @"Parcelamento indisponível para esse valor";
        NSString* description = @"Para visualizar as opções de parcelamento, é necessário incluir o valor desejado.";
        PopupViewController* controller = [self createInstallmentAlertWithTitle:title description:description hasTwoButtons:false];
        [self showPopup:controller];
    }else if([[paymentManager cardTotal] compare:[NSNumber numberWithInt:0]] != NSOrderedDescending){ // cartTotal <= 0
        NSString* title = @"Parcelamento disponível apenas para pagamento via cartão de crédito.";
        NSString* description = @"Para visualizar as opções de parcelamento, é necessário incluir o valor desejado e alterar a forma de pagamento para cartão de crédito, desabilitando a opção Usar saldo ao pagar.";
        PopupViewController* controller = [self createInstallmentAlertWithTitle:title description:description hasTwoButtons:true];
        [self showPopup:controller];
    }else{
        wentToInstallments = YES;
        _installmentsTooltip.blockTooltip = YES;
        
        NSInteger integerSellerId = [[[self item] seller] wsId];
        NSString *selledId = [NSString stringWithFormat:@"%ld", (long) integerSellerId];
        InstallmentsSelectorViewModel *viewModel = [[InstallmentsSelectorViewModel alloc] initWithPaymentManager:paymentManager
                                                                                                         payeeId:nil
                                                                                                        sellerId:selledId
                                                                                                          origin:nil
                                                                                                     titleHeader:nil
                                                                                               descriptionHeader:nil
                                                                                                          worker:nil];
        viewModel.delegate = self;
        InstallmentsSelectorViewController *controller = [[InstallmentsSelectorViewController alloc] initWith:viewModel];
        [[self navigationController] pushViewController:controller animated:YES];
        NSString *isInvertedInstallment = [@([FeatureManager isActiveExperimentInvertedInstallmentsOBJC]) isEqual:@1] ? @"true" : @"false";
        [OBJCAnalytics logWithName:@"Installment Accessed" properties:@{
            @"value": _valueInputTextField.text,
            @"is_inverted_installment": isInvertedInstallment
        }];
    }
}

- (void)configureValueField {
    if (_isFixedValue) {
        _valueInputTextField.enabled = NO;
        [self insertToolbarDiretToView];
    } else {
        _valueInputTextField.enabled = YES;
        if (itemIsReady) {
            [_valueInputTextField becomeFirstResponder];
        }
    }
}

- (void)insertToolbarDiretToView {
    if (itemIsReady) {
        // Set toolbar in footer view.
        PaymentToolbar *toolbar = _paymentToolbarController.toolbar;
        toolbar.hidden = NO;
        toolbar.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addSubview:toolbar];
        
        if (@available(iOS 11.0, *)) {
            UILayoutGuide *layoutGuide = self.view.safeAreaLayoutGuide;
            [toolbar.bottomAnchor constraintEqualToAnchor:layoutGuide.bottomAnchor constant:0].active = YES;
            [toolbar.trailingAnchor constraintEqualToAnchor:layoutGuide.trailingAnchor constant:0].active = YES;
            [toolbar.leadingAnchor constraintEqualToAnchor:layoutGuide.leadingAnchor constant:0].active = YES;
            
            // set background safe area color
            [self paintSafeAreaBottomInsetWithColor:toolbar.bottomBar.backgroundColor];
        } else {
            [toolbar.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:0].active = YES;
            [toolbar.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor constant:0].active = YES;
            [toolbar.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant:0].active = YES;
        }
        [toolbar.heightAnchor constraintEqualToConstant:100].active = YES;
        [self.view layoutIfNeeded];
    }
}

-(NSString*)addFormatPrice:(double)dblPrice {
	NSNumber *temp = [NSNumber numberWithDouble:dblPrice];
	NSDecimalNumber *someAmount = [NSDecimalNumber decimalNumberWithDecimal:[temp decimalValue]];
	return [CurrencyFormatter brazillianRealStringFromNumber:someAmount];
}

-(double)removeFormatPrice:(NSString *)strPrice {
	return [[CurrencyFormatter brazillianRealNumberFromString:strPrice] doubleValue];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{

    if (textField.tag == 8879) {
        return YES;
    }
	
	double currentValue		= [self removeFormatPrice:textField.text];
	double cents			= round(currentValue * 100.0f);
	
	if (([string isEqualToString:@"."]) && ((int)currentValue == 0)) {
		cents = floor(cents * 100);
	} else if ([string length]) {
		for (size_t i = 0; i < [string length]; i++) {
			unichar c = [string characterAtIndex:i];
			if (isnumber(c)) {
				cents *= 10;
				cents += c - '0';
			}
		}
	} else {
		cents = floor(cents / 10);
	}

	NSString *str = [NSString stringWithFormat:@"%f", cents];
	if ([str length] > 15) {
		NSString *newStr = [str substringFromIndex:1];
		cents = [newStr doubleValue];
	}

	double valor = (cents/100.0f);
	NSString *finalValue = [self addFormatPrice:[[NSString stringWithFormat:@"%.2f", valor] doubleValue]];
	self.valueInputTextField.text = finalValue;
	NSDecimalNumber *valorTotal = [[NSDecimalNumber alloc] initWithDouble:valor];

	paymentManager.subtotal = [NSDecimalNumber decimalNumberWithDecimal:[valorTotal decimalValue]];
	[self updatePaymentInfo];

	return NO;
	
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
	
	if (textField.tag == 8879) {
		return;
	}
	
	if ([textField.text isEqualToString:@"R$"]) {
		[textField setText:@""];
	}
}

- (void)cancel{
	[_valueInputTextField resignFirstResponder];
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setViewValueInputContainer:nil];
    [self setViewLargeGreenButtonContainer:nil];
    [self setValueInputTextField:nil];
	[self setLabelCreditDescription:nil];
	[self setStoreImage:nil];
	[self setLabelStoreName:nil];
	[self setViewForFidelityInfo:nil];
	[self setHorizontalLine:nil];
    [super viewDidUnload];
}

- (IBAction)resignFirstResponderOnBlur{
	[self resignFirstResponder];
}

- (void)checkout:(id)sender{
	
	[HapticFeedback impactFeedbackLight];
	
	[PPAnalytics trackEvent:@"Finalização PAV" properties:@{@"Abriu cartões": wentToCards ? @"Sim" : @"Não", @"Abriu parcelamento": wentToInstallments ? @"Sim" : @"Não"} includeMixpanel: NO];
	
	if (_item.isParkingPayment == IsOldParking && licensePlate == nil) {
		[_viewForMissingCarPlate pulse];
		return;
	}
	
    if([[paymentManager total] compare:[NSNumber numberWithInt:0]] == NSOrderedSame) {
        [AlertMessage showAlertWithMessage:@"Digite o valor que deseja pagar." controller:self];
    }else{
        [self doCheckoutAuthenticate];
    }
}

/**!
 * Authenticate de user to checkout
 */
- (void)doCheckoutAuthenticate {
    
    [_valueInputTextField resignFirstResponder];
    
    auth = [PPAuth authenticate:^(NSString *authToken, BOOL biometry) {
        [self posAuthenticationWithPin:authToken biometry:biometry];
    } canceledByUserBlock:^{
        [_valueInputTextField becomeFirstResponder];
    }];
}

- (void)posAuthenticationWithPin:(NSString *)pin biometry:(BOOL)biometry{
	loadingView = [Loader getLoadingView:@"Concluindo transação..."];
	[self.navigationController.view addSubview:loadingView];
	loadingView.hidden = NO;
    [self checkCvvFlow:pin biometry:biometry];
}

- (void) showCarPlateComfirmation {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Atenção" message:[NSString stringWithFormat:@"Confirme sua placa:\n\n%@\n\nA placa está correta?", carPlateTextField.text]  preferredStyle: UIAlertControllerStyleAlert];
    
    if (@available(iOS 11.0, *)) {
        NSMutableAttributedString *messageString = [[NSMutableAttributedString alloc] initWithString:@"Confirme sua placa:\n\n" attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14] }];
        [messageString appendAttributedString: [[NSAttributedString alloc] initWithString:carPlateTextField.text attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:20] }]];
        [messageString appendAttributedString: [[NSAttributedString alloc] initWithString:@"\n\nA placa está correta?" attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14] }]];
        [alert setValue:messageString forKey:@"attributedMessage"];
    }
    
    __weak PaymentViewController* weakSelf = self;
    [alert addAction:[UIAlertAction actionWithTitle:@"Sim, está correta" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf setLicensePlate:carPlateTextField.text];
        [weakSelf.carPlateTextField resignFirstResponder];
        [weakSelf.view endEditing:YES];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.25 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [weakSelf.valueInputTextField becomeFirstResponder];
        });
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Editar placa" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf editCarPlate];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:nil]];
    
    [self presentViewController:alert animated:true completion:nil];
}

-(void)checkCvvFlow:(NSString *)pin biometry:(BOOL)biometry{
    __weak typeof(self) weakSelf = self;
    [_cvvHelper checkCvvFlowWithManager:paymentManager navigationController:self.navigationController onSuccess:^{
        if (weakSelf) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf checkoutSegue:pin biometry:biometry];
        }
    } onError:^(PicPayError * error) {
        if (weakSelf) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            loadingView.hidden = YES;
            [AlertMessage showAlertWithError:error controller:strongSelf];
        }
    }];
}

-(void)checkoutSegue:(NSString *)pin biometry:(BOOL)biometry{
    NSString *sellerId      = [[NSString alloc] initWithFormat:@"%ld",(long)[[[self item] seller] wsId]];
    NSString *planType      = [[NSString alloc] initWithFormat:@"%@",[[[[self item] seller] currentPlanType] planTypeIdToString]];
    NSString *totalValue2    = [[paymentManager cardTotalWithoutInterest] stringValue];
    NSString *creditCardId  = [[NSString alloc] initWithFormat:@"%ld", (long) CreditCardManager.shared.defaultCreditCardId];
    NSString *consumerCredit= [[paymentManager balanceTotal] stringValue];
    BOOL didUseCreditCard = ([[paymentManager cardTotal] compare:[NSNumber numberWithInt:0]] == NSOrderedDescending) ? YES : NO;
    NSString *shipping = @"0";
    NSString *shippingId = @"";
    NSString *surcharge = @"";
    NSString *addressId = @"";
    NSString *parcelas = [NSString stringWithFormat:@"%ld", (long)[paymentManager selectedQuotaQuantity]];
    NSString *from_explore_string = self.from_explore ? @"1" : @"0";
    NSString *someErrorOccurredString = self.someErrorOccurred ? @"1" : @"0";
    NSString *biometryString = biometry ? @"1" : @"0";
    NSString *paymentPrivacyConfig    = _privacyConfig;
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    NSString *itemId = [[NSString alloc] initWithFormat:@"%ld", (long)self.item.selectedCombination];
    NSString *amount = [[NSString alloc] initWithFormat:@"%d", 1];
    NSDictionary *itemInfo = [[NSDictionary alloc] initWithObjects:[[NSArray alloc] initWithObjects:itemId, amount, nil] forKeys:[[NSArray alloc] initWithObjects:@"id", @"amount", nil]];
    [items addObject:itemInfo];
    
    NSString *bestGpsAcc = [[NSString alloc] initWithFormat:@"%f", _bestGpsAcc];
    NSString *bestGpsLat = [[NSString alloc] initWithFormat:@"%f", _bestGpsLat];
    NSString *bestGpsLng = [[NSString alloc] initWithFormat:@"%f", _bestGpsLng];
    
    NSArray *args = @[pin,
                      sellerId,
                      planType,
                      totalValue2,
                      shipping,
                      shippingId,
                      creditCardId,
                      addressId,
                      parcelas,
                      consumerCredit,
                      items,
                      surcharge,
                      from_explore_string,
                      someErrorOccurredString,
                      biometryString,
                      bestGpsAcc,
                      bestGpsLat,
                      bestGpsLng,
                      licensePlate ? licensePlate : @"",
                      paymentPrivacyConfig];
    
    NSString *origin = _touchOrigin ? _touchOrigin : @"";
    NSDictionary *additionalInfo = _additionalInfo ? _additionalInfo : @{};
    NSDictionary *extraParams = @{};
    if (_deepLinkExtraParam) {
        extraParams = @{@"extra": _deepLinkExtraParam};
    }
    NSString *appsFlyerEventType = @"pav";
    if (_item.isParkingPayment == IsOldParking) {
        appsFlyerEventType = @"parking";
    }
    
    __weak typeof(self) weakSelf = self;
    [WSTransaction createTransaction:args withCvv:[_cvvHelper informedCvv] withOrigin:origin withAdditionalInfo:additionalInfo withExtra:extraParams completionHandler:^(MBTransaction *transactionForSegue, NSError *err) {
        if (weakSelf) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            
            if (transactionForSegue) {
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    
                    [HapticFeedback notificationFeedbackSuccess];
                    loadingView.hidden = YES;
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName: NotificationName.paymentNew  object: nil userInfo:@{@"type": @"PAV"}];
                    
                    NSString *transationId = [NSString stringWithFormat:@"%ld", (long)transactionForSegue.WsId];
                    ReceiptWidgetViewModel *receiptViewModel = [[ReceiptWidgetViewModel alloc] initWithTransactionId:transationId type:ReceiptTypePAV];
                    [receiptViewModel setReceiptWidgetsWithItems:transactionForSegue.receiptItems isLoading:NO];
                    
                    [TransactionReceipt showReceiptSuccessWithViewController:strongSelf receiptViewModel:receiptViewModel];
                    [_cvvHelper saveCvv];
                    
                    NSDictionary *transactionProperties = @{
                        @"seller_id": sellerId,
                        @"type": appsFlyerEventType,
                        @"transaction_id": [receiptViewModel transactionId],
                        @"transaction_value": transactionForSegue.total
                    };
                    
                    [OBJCAnalytics logWithName:@"transaction" properties:transactionProperties];
                    [OBJCAnalytics logFirebaseWithName:@"transaction" properties:transactionProperties];
                    
                });
            } else {
                weakSelf.someErrorOccurred = true; // Duplicate
                weakSelf.cvvHelper.informedCvv = NULL;
                [strongSelf handleError:err pin:pin biometry:biometry didUseCreditCard:didUseCreditCard];
            }
        }
    }];
}

- (void) handleError: (NSError *)err pin: (NSString *)pin biometry: (BOOL)biometry didUseCreditCard: (BOOL)didUseCreditCard {
    
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [HapticFeedback notificationFeedbackError];
        
        loadingView.hidden = YES;
        __weak PaymentViewController * weakSelf = self;
        [auth handleError:err callback:^(NSError *error) {
            weakSelf.someErrorOccurred = YES; // Duplicate

            // TEF "Error"
            TEFError* tefError = [[TEFError alloc] initWithError:err];
            if (tefError) {
                [weakSelf.view endEditing:YES];
                [weakSelf showTEFCode:tefError store: weakSelf.store didUseCreditCrd: didUseCreditCard];
                return;
            }
            
            [weakSelf.valueInputTextField becomeFirstResponder];
            [AlertMessage showCustomAlertWithErrorUI:error controller:weakSelf completion:^{}];
        }];
    });
}

- (void) showTEFCode: (TEFError *) tefError store:(PPStore *)store didUseCreditCrd:(BOOL)didUseCreditCard {
    ConfirmationCodeViewController* popup = [[ConfirmationCodeViewController alloc] initWithError: tefError andStore: store didUseCreditCard: didUseCreditCard];
    __weak typeof(self) weakSelf = self;
    popup.dismissBlock = ^{
        if (weakSelf) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            _paymentToolbarController.toolbar.hidden = NO;
            [strongSelf.valueInputTextField becomeFirstResponder];
        }
    };
    [self showPopup:popup];
}

- (IBAction)changePaymentMethod:(id)sender{
	wentToCards = YES;
    
    PaymentMethodsViewController *ccSelection = (PaymentMethodsViewController *)[ViewsManager paymentMethodsStoryboardViewControllerWithIdentifier:@"PaymentMethodsViewController"];
	[[self navigationController] pushViewController:ccSelection animated:YES];
}


//Pickerview
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	return [_item.parkingTimes count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	return [(PPParkingTime *)[_item.parkingTimes objectAtIndex:row] pickerString];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	paymentManager.subtotal = [(PPParkingTime *)[_item.parkingTimes objectAtIndex:row] totalValue];
	[self updatePaymentInfo];
	
	_valueInputTextField.text = [CurrencyFormatter brazillianRealStringFromNumber:[(PPParkingTime *)[_item.parkingTimes objectAtIndex:row] totalValue]];
	[_valueInputTextField sendActionsForControlEvents:UIControlEventEditingChanged];
	
	_parkingTimeLabel.text = [(PPParkingTime *)[_item.parkingTimes objectAtIndex:row] expiryDateString];
	
	paymentManager.subtotal = [(PPParkingTime *)[_item.parkingTimes objectAtIndex:row] totalValue];
	[self updatePaymentInfo];
}

- (IBAction)editCarPlate{
	[self editCarPlateWithText:@"Informe a placa do veículo estacionado." andCarPlateValue:nil];
}

#pragma mark Show plates
- (IBAction)showUserPlates{
    
    __weak PaymentViewController *weakSelf = self;
    [self.valueInputTextField resignFirstResponder];
    
    // Nao mostrar o action sheet se não houver placas no server
    if (!_item.parkingPlates.count){
        [self editCarPlate];
        return;
    }
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Informe a placa do veículo" message:@"Adicione uma placa nova ou escolha uma das placas adicionadas anteriormente" preferredStyle:UIAlertControllerStyleActionSheet];
    
    // Cancel
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [weakSelf.valueInputTextField becomeFirstResponder];
    }]];
    
    // Nova placa
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Adicionar nova placa" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [weakSelf.valueInputTextField resignFirstResponder];
        [weakSelf editCarPlate];
    }]];
    
    // Placas
    for(PPParkingCarPlate* plateObj in _item.parkingPlates){
        [actionSheet addAction:[UIAlertAction actionWithTitle:plateObj.plate style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [weakSelf.valueInputTextField becomeFirstResponder];
            [self setLicensePlate: action.title];
        }]];
    }
    
    // Remover placa
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Remover placa" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        [self showDeletePlateActionSheet];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void)showDeletePlateActionSheet{
    __weak PaymentViewController *weakSelf = self;
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Excluir placa" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    // Cancel
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [weakSelf.valueInputTextField becomeFirstResponder];
    }]];
    
    // Placas
    for(PPParkingCarPlate* plateObj in _item.parkingPlates){
        NSString* plate = plateObj.plate;
        __weak typeof(self) weakSelf = self;
        [actionSheet addAction:[UIAlertAction actionWithTitle:plate style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            loadingItemView.hidden = NO;
            [WSConsumer removeLicensePlate:plate completionHandler:^(BOOL success, NSError * _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    if (weakSelf) {
                        __strong typeof(weakSelf) strongSelf = weakSelf;
                        loadingItemView.hidden = YES;
                        [weakSelf.valueInputTextField becomeFirstResponder];
                        if (error != nil) {
                            [AlertMessage showAlertWithMessage:error.localizedDescription controller:strongSelf];
                            return;
                        }
                        // Remove a placa da lista local e atualiza a label se necessário
                        [_item.parkingPlates removeObject:plateObj];
                        if ([_carPlateLabel.text isEqualToString:plate]){
                            [strongSelf setLicensePlate: nil];
                        }
                    }
                });
            }];
        }]];
    }
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void)editCarPlateWithText:(NSString *)alertMessage andCarPlateValue:(NSString *)carPlateValue{
	
	[_viewForMissingCarPlate setHidden:YES];
	
	if (!carPlateValue) {
		carPlateValue = @"";
	}
	
    UIAlertController *alert = [UIAlertController alertControllerWithTitle: @"Placa do veículo" message: alertMessage preferredStyle: UIAlertControllerStyleAlert];
    
    UIAlertAction *continuar = [UIAlertAction actionWithTitle: @"Ok" style: UIAlertActionStyleDefault handler: ^(UIAlertAction * action) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void) {
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                
                if ([PlateValidator isValid: carPlateTextField.text]) {
                    [self showCarPlateComfirmation];
                } else {
                    [self editCarPlateWithText: [NSString stringWithFormat:@"A placa \"%@\" não é válida.\nInforme a placa novamente.", carPlateTextField.text] andCarPlateValue: nil];
                }
                
            });
        });
    }];
    
    UIAlertAction *cancelar = [UIAlertAction actionWithTitle: @"Cancelar" style: UIAlertActionStyleCancel handler: nil];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        
        textField.delegate = self;
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.keyboardType = UIKeyboardTypeAlphabet;
        textField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
        textField.textAlignment = NSTextAlignmentCenter;
        textField.font = [UIFont systemFontOfSize: 22.0f];
        textField.tag = 8879;
        carPlateTextField = nil;
        carPlateTextField = textField;
        carPlateTextField.text = carPlateValue;
    }];
    
    [alert addAction: cancelar];
    [alert addAction: continuar];
    
    carPlateAlert = alert;
    
    [self presentViewController: alert animated:YES completion:nil];
}

- (IBAction)parkingInfo{

	[_valueInputTextField resignFirstResponder];
	[HTMLPopUpViewController showPopWithHtml:_item.parkingInfo parent:self];
	
}

- (void)popUpDidClose{
	
	[_valueInputTextField becomeFirstResponder];
	
}

- (void)setLicensePlate:(NSString *)plate{
	licensePlate = plate;
    _carPlateLabel.text = plate != nil ? plate : @"ABC 1234";
}

- (void) cancelTapped {
    [self.view endEditing:YES];
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

// There's a similar code on NewTransactionViewController.m
- (PopupViewController*)createInstallmentAlertWithTitle: (NSString*) title description: (NSString*) description hasTwoButtons: (BOOL) hasTwoButtons {
    UIImage* image = [UIImage imageNamed:@"payment_installment"];
    UIColor* lightDescriptionColor = [UIColor colorWithRed:57.0/255 green:70.0/255 blue:77.0/255 alpha:1.0];
    UIColor* darkDescriptionColor = [UIColor colorWithRed:229.0/255 green:229.0/255 blue:229.0/255 alpha:1.0];
    UIColor* dynamicDescriptionColor = [UIColor dynamicColorWithLight:lightDescriptionColor dark:darkDescriptionColor];
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSMutableAttributedString* attributedDescription = [[NSMutableAttributedString alloc] initWithString: description
                                                                                             attributes : @{
                                                                                                 NSFontAttributeName: [UIFont systemFontOfSize:14 weight:UIFontWeightMedium],
                                                                                                 NSParagraphStyleAttributeName: paragraphStyle,
                                                                                                 NSForegroundColorAttributeName: dynamicDescriptionColor
                                                                                             }];
    PopupViewController* controller = [[PopupViewController alloc] initWithTitle:title description:NULL image:image];
    controller.hideCloseButton = true;
    [controller setAttributeDescription:attributedDescription linkAttributes:NULL];
    if(hasTwoButtons) {
        PopupAction* changePaymentMethod = [[PopupAction alloc] initWithTitle:@"Alterar forma de pagamento" style:PopupActionStyleFill completion:^{
            [self changePaymentMethod:nil];
        }];
        PopupAction* dismiss = [[PopupAction alloc] initWithTitle:@"Agora não" style:PopupActionStyleLink completion:NULL];
        [controller addAction:changePaymentMethod];
        [controller addAction:dismiss];
    } else {
        PopupAction* action = [[PopupAction alloc] initWithTitle:@"Ok, entendi" style:PopupActionStyleFill completion:NULL];
        [controller addAction:action];
    }
    return controller;
}

#pragma mark - UIPPProfileImageDelegate
- (void)profileImageViewDidTap:(UIPPProfileImage *)profileImage store:(PPStore *)store{
    self.establishmentDetailsCoordinator = [[EstablishmentCoordinatorFactory new]
                                            makeDetailsCoordinatorWithStore:store
                                            navigationController:self.navigationController
                                            fromViewController:self];

    __weak typeof(self) weakSelf = self;
    [self.establishmentDetailsCoordinator setDidFinishFlow:^{
        if (!weakSelf) {
            return;
        }

        __strong typeof(weakSelf) strongSelf = weakSelf;
        strongSelf.establishmentDetailsCoordinator = nil;

        strongSelf.paymentToolbarController.toolbar.hidden = NO;
        [strongSelf.valueInputTextField becomeFirstResponder];
    }];

    [self.view endEditing:YES];
    self.paymentToolbarController.toolbar.hidden = YES;

    [self.establishmentDetailsCoordinator start];
}

#pragma mark - Installment selector delegate

- (void)didSelectedInstallment:(PPPaymentManager *)paymentManager {
    paymentManager = paymentManager;
    [self updatePaymentInfo];
    [self.navigationController popToViewController:self animated:true];
    NSInteger quotaQuantity = [paymentManager selectedQuotaQuantity];
    NSNumber *value = [paymentManager subtotal];
    NSString *isInvertedInstallment = [@([FeatureManager isActiveExperimentInvertedInstallmentsOBJC]) isEqual:@1] ? @"true" : @"false";
    [OBJCAnalytics logWithName:@"Installments Selected" properties:@{
        @"installments": [NSString stringWithFormat:@"%ld", (long)quotaQuantity],
        @"value": [NSString stringWithFormat:@"%@", value],
        @"is_inverted_installment": isInvertedInstallment
    }];
}

@end

