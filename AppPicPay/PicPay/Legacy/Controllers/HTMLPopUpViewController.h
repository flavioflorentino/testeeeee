#import <UIKit/UIKit.h>
#import "PPBaseViewControllerObj.h"

@protocol HTMLPopUpViewDelegate <NSObject>

- (void)popUpDidClose;

@end


@interface HTMLPopUpViewController : PPBaseViewControllerObj

@property (strong, nonatomic) id<HTMLPopUpViewDelegate> delegate;
@property (strong, nonatomic) NSString *htmlString;

@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (strong, nonatomic) IBOutlet UIView *mainView;

+ (HTMLPopUpViewController *)showPopWithHtml:(NSString *)htmlString parent:(UIViewController<HTMLPopUpViewDelegate> *)parent;

- (IBAction)close:(id)sender;

@end
