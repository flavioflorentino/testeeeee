//
//  PPAntiFraudChecklistItem.h
//  PicPay
//
//  Created by Diogo Carneiro on 22/06/16.
//
//

#import <Foundation/Foundation.h>

@interface PPAntiFraudChecklistItem : NSObject

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *wsId;
@property (strong, nonatomic) NSString *desc;
@property BOOL uploaded;
@property BOOL cameraOnly;
@property BOOL frontalCamera;
@property (strong, nonatomic) NSData *attachment;
@property BOOL isDoubleSided;

@end
