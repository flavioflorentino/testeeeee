//
//  CreditCardOnboardingPopUpViewController.m
//  PicPay
//
//  Created by Diogo Carneiro on 25/09/15.
//
//

#import "CreditCardOnboardingPopUpViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "PPAnalytics.h"

@implementation UIView (fadeIn)

- (void)fadeInToAlpha:(CGFloat)newAlpha{
	self.alpha = newAlpha;
	self.hidden = NO;
	[UIView beginAnimations:@"fadeIn" context:nil];
	[UIView setAnimationDuration:0.25];
	self.alpha = 1;
	[UIView commitAnimations];
	
}

@end

@interface CreditCardOnboardingPopUpViewController ()

@end

@implementation CreditCardOnboardingPopUpViewController


+ (CreditCardOnboardingPopUpViewController *)showCreditCardOnboarding:(UIViewController<CreditCardOnboardingPopUpDelegate> *)parent{
	
	UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
	BOOL animated = NO;
	CreditCardOnboardingPopUpViewController *viewController = [[CreditCardOnboardingPopUpViewController alloc] init];
	viewController.delegate = parent;

	[parent addChildViewController:viewController];
	[viewController viewWillAppear:animated];
	[parent.view addSubview:viewController.view];
	[viewController viewDidAppear:animated];
	[viewController didMoveToParentViewController:parent];
	[currentWindow addSubview:viewController.view];
	
	viewController.mainView.layer.cornerRadius = 5;
	viewController.mainView.layer.masksToBounds = YES;

//	[PPAnalytics trackEvent:@"Onboarding CC - Inicio" properties:nil];

	return viewController;
}

- (id)init{
	self = [super initWithNibName:@"CreditCardOnboardingPopUpViewController" bundle:nil];
	if (self != nil)
	{
		// Further initialization if needed
	}
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	
	[PPAnalytics trackEvent:@"Onboarding CC - Inicio" properties:nil];
	
	
	UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
	
	
	self.view.translatesAutoresizingMaskIntoConstraints = NO;

	
	[currentWindow addConstraint:[NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:currentWindow attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
	
	[currentWindow addConstraint:[NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:currentWindow attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
	
	[currentWindow addConstraint:[NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:currentWindow attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
	
	[currentWindow addConstraint:[NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:currentWindow attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
	
	
	
	self.view.alpha = 0;
	
	[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
		self.view.alpha = 1;
	} completion:nil];
	
}

- (IBAction)close:(id)sender{
	
}

- (IBAction)readMore:(id)sender{
	
	[PPAnalytics trackEvent:@"Onboarding CC - Saiba mais" properties:nil];
	
	[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
		self.cardView.alpha = 0;
	} completion:nil];
}

- (IBAction)addCreditCard:(id)sender{
	if([_delegate respondsToSelector:@selector(creditCardOnboardingPopUpAddCreditCard)]){
		[_delegate creditCardOnboardingPopUpAddCreditCard];
	}
	[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
		self.view.alpha = 0;
	} completion:^(BOOL finished) {
		self.view.hidden = YES;
		[self.view removeFromSuperview];
	}];
}

- (IBAction)notNow:(id)sender{
	[UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
		self.view.alpha = 0;
	} completion:^(BOOL finished) {
		self.view.hidden = YES;
		[self.view removeFromSuperview];
	}];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
