// This is used on the RegistrationStoryboard. For some reason I wasn't able to delete it
// Whenever I removed it from the Storyboard xcode would complaing about not being able to
// render some views
final class PPPublicNavigationController: PPNavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
