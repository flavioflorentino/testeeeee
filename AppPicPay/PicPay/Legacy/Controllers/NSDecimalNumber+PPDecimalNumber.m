//
//  NSDecimalNumber+PPDecimalNumber.m
//  PicPay
//
//  Created by Diogo Carneiro on 28/10/14.
//
//

#import "NSDecimalNumber+PPDecimalNumber.h"

@implementation NSDecimalNumber (PPDecimalNumber)

+ (NSDecimalNumber *)decimalNumberWithFloat:(float)floatNumber{
	return [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:floatNumber] decimalValue]];
}

+ (NSDecimalNumber *)decimalNumberWithInt:(NSInteger)intNumber{
	return [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithInteger:intNumber] decimalValue]];
}

@end
