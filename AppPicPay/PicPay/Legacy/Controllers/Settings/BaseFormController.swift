import UI
import UIKit
import Validator

class BaseFormController: PPBaseViewController, UITextFieldDelegate, UITextViewDelegate {
    
    var loading:Bool = false
    
    @IBOutlet weak var actionButton: UIPPButton?
    @IBOutlet weak var contentView: UIView?
    @IBOutlet weak var viewBottomConstraint: NSLayoutConstraint? {
        didSet {
            guard let viewBottomConstraint = viewBottomConstraint else {
            return
        }
            KeyboardManager.shared.addConstraint(viewBottomConstraint, parentView: view)
        }
    }
    @IBOutlet weak var scrollView: UIScrollView?
    
    var controls:[UIControl] = [UIControl]()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        tapGesture.delaysTouchesBegan = false
        self.view.addGestureRecognizer(tapGesture)
        
        contentView?.backgroundColor = Palette.ppColorGrayscale000.color
        view.backgroundColor = Palette.ppColorGrayscale000.color
        
        super.viewDidLoad()
        
        // Keyboard events observers
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillOpen), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let viewBottomConstraint = viewBottomConstraint {
            KeyboardManager.shared.addConstraint(viewBottomConstraint, parentView: view)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        KeyboardManager.shared.removeConstraint(for: view)
    }
    
    // MARK: - Dependence Injection
    
    // MARK: - Validations
    
    /// Enable the validation when the user touch on contorller
    var validateOnInputChange:Bool = false {
        didSet{
            // Validate all elements
            for obj in self.controls {
                if let validatable = obj as? UITextField  {
                    if validatable.validationRules != nil {
                        validatable.validateOnInputChange(enabled: validateOnInputChange)
                    }
                }
            }
        }
    }
    
    @discardableResult
    func validate(_ showMessage:Bool = true) -> ValidationResult{
        var results:[ValidationResult] = [ValidationResult]()
        
        /// Validate all elements
        for obj in self.controls {
            if let validatable = obj as? UIPPFloatingTextField  {
                guard validatable.validationRules != nil else { continue }
                let result = validatable.validate()
                results.append(result)
                    
                guard showMessage else { continue }
                switch result {
                case .invalid(let error):
                    guard let err = error.first else { continue }
                    validatable.errorMessage = String(describing:err)
                default:
                    validatable.errorMessage = nil
                    break
                }
            }
            
            if let validatable = obj as? UIPPFloatingTextView {
                guard validatable.textView.validationRules != nil else { continue }
                let result = validatable.textView.validate()
                results.append(result)
                
                guard showMessage else { continue }
                switch result {
                case .invalid(let error):
                    if let err = error.first {
                        validatable.errorMessage = String(describing:err)
                    }
                default: break
                }
            }
        }
        
        // Merge all results
        let result = ValidationResult.merge(results: results)
        return result
    }
    
    
    // MARK: - Internal Methods
    
    func startLoading(){
        loading = true
        actionButton?.startLoadingAnimating()
        
        //disable all controlls
        for control in controls{
            control.isEnabled = false
        }
    }
    
    func stopLoading(){
        loading = false
        actionButton?.stopLoadingAnimating()
        
        //enable all controlls
        for control in controls{
            control.isEnabled = true
        }
    }
    
    @objc func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    // MARK: - Controls Methods
    
    func addTextField(_ textField: UIPPFloatingTextField){
        if let _ = controls.firstIndex(of: textField) {
        } else {
            textField.delegate = self
            controls.append(textField)
        }
    }
    
    func removeTextField(_ textField: UIPPFloatingTextField) {
        if let index = controls.firstIndex(of: textField) {
            controls.remove(at: index)
        }
    }
    
    func addTextView(_ wrapper: UIPPFloatingTextView){
        wrapper.delegate = self
        controls.append(wrapper)
    }
    
    // MARK: - Keyboard Events Handler
    
    @objc func keyboardWillOpen(_ keyboardNotification:Notification){

    }
    
    @objc func keyboardWillHide(_ keyboardNotification:Notification){

    }
    
    // MARK: - TextfieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // Workaround to the textfield jump
        textField.layoutIfNeeded()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        guard let ppTextField = textField as? MaskTextField else { return true }
        
        if let maxlength = ppTextField.maxlength, newText.length > maxlength {
            return false
        }
            
        // Mask
        if let maskString = ppTextField.maskString {
            let mask = CustomStringMask(mask: maskString)
            textField.text = mask.maskedText(from: newText)
            return false
        }
        
        if let maskABlock = ppTextField.maskblock {
            textField.text = maskABlock(textField.text ?? "", newText)
            return false
        }
        
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text! as NSString).replacingCharacters(in: range, with: text)
        
        guard let ppTextView = textView.superview as? MaskTextField else { return true }
        
        if let maxlength = ppTextView.maxlength, newText.length > maxlength {
            return false
        }
        
        // Mask
        if let maskString = ppTextView.maskString {
            let mask = CustomStringMask(mask: maskString)
            textView.text = mask.maskedText(from: newText)
            return false
        }
        
        if let maskABlock = ppTextView.maskblock {
            textView.text = maskABlock(textView.text ?? "", newText)
            return false
        }
        
        return true
    }
}
