import Foundation

// This class could be much more elegant if we could use swift inout, but it isn't available in Obj-c
final class NewProfileProxy: NSObject {
    // toggle
    @objc
    static func shouldShowNewProfile() -> Bool{
        return true
    }
    
    @discardableResult
    @objc
    static func openProfile(contact: PPContact, parent: UIViewController, originView: UIView? = nil) -> ProfileViewController? {
        if contact.isStore || !NewProfileProxy.shouldShowNewProfile() {
            let profileViewController = ProfileViewController(contact: contact, controller: parent, originView: originView)
            return profileViewController
        }

        let profileVC = ProfileFactory.make(profileId: String(contact.wsId), placeHolderContact: contact)
        profileVC.hidesBottomBarWhenPushed = true
        parent.navigationController?.pushViewController(profileVC, animated: true)
        
        return nil
    }
    
    @objc
    @discardableResult
    static func openProfile(id: Int, parent: UIViewController) -> ProfileViewController? {
        let contact = PPContact()
        contact.isStore = false
        contact.wsId = id
        
        return NewProfileProxy.openProfile(contact: contact, parent: parent)
    }
    
    @objc
    static func newProfile(consumer: MBConsumer, showRightButton: Bool = true) -> UIViewController {
        let placeholder = PPContact()
        placeholder.username = consumer.username
        placeholder.onlineName = consumer.name
        placeholder.wsId = consumer.wsId
        
        let profileVC = ProfileFactory.make(profileId: String(placeholder.wsId), placeHolderContact: placeholder)
        return profileVC
    }
}

