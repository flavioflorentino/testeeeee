//
//  ProfileScrollManager.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 31/05/17.
//
//

import Foundation
import SnapKit

@objc protocol Scrollable {
    var scrollView: UIScrollView! { get }
}

@objc protocol ScrollManageable: Scrollable {
    var childScrollView: UIScrollView! { get }
    var view: UIView! { get }
    weak var scrollDelegate: ScrollDelegate? { get set }
    func didMove(toParentViewController parent: UIViewController?)
}

// Easy way to disable scroll and touches (to show a loading view for example)
@objc protocol ScrollDelegate: NSObjectProtocol {
    @objc optional func disableView()
    @objc optional func enableView()
}

/*
 * ScrollManager is a view controller (1) that has a fucking big scrollView and a child view controller (2)
 * The child view controller has a scroll view and ANOTHER child view controller (3) that has another scrollView
 * What ScrollManager does is intercept the user's gestures and update the correct child scroll view (2 or 3)
 *
 
 * Some things to keep in mind:
 * - The ScrollManager's scroll view must be bigger (or the same size) than the other two scroll views combined
 * - The child VC must conform to ScrollManageable, that means it must have a scroll view, and some view controller's methods.
 * - Although it's possible to init ScrollManageable with a non UIViewController, do not do it!
 * - The important logic is in scrollViewDidScroll:
 *
 */

final class ScrollManager: PPBaseViewController {
    
    override var shouldAddBreadcrumb: Bool {  return false }
    
    let outterScrollView = UIScrollView()
    let contentView = UIView()
    // View that dictates the outterScrollView content size
    let heightView = UIView()
    
    var contentHeight: Constraint?
    
    var viewController: ScrollManageable
    
    var previousY:CGFloat = 0
    
    var contentTop: Constraint?
    
    var shouldUpdateHeight = true
    var finishedLoading = false
    
    internal enum Observables:String {
        case ScrollManagerChildLoaded = "ScrollManagerLoaded"
    }
    
    init(with viewController: ScrollManageable) {
        self.viewController = viewController
        super.init(nibName: nil, bundle: nil)
        self.automaticallyAdjustsScrollViewInsets = false

        self.viewController.scrollDelegate = self
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .clear
        
        NotificationCenter.default.addObserver(self, selector: #selector(didFinishLoading), name: NSNotification.Name(rawValue: Observables.ScrollManagerChildLoaded.rawValue), object: nil)
        self.automaticallyAdjustsScrollViewInsets = false

        setupViews()
        setupConstraints()
        
        setupChildViewController()
    }
    
    @objc func didFinishLoading() {
        finishedLoading = true
        updateHeight()
    }
    
    func updateHeight() {
        DispatchQueue.main.async { [weak self]  in
            guard let vcScrollView = self?.viewController.scrollView,
                let childScrollView = self?.viewController.childScrollView,
                let childSuper = childScrollView.superview else {
            return
        }
            
            // Childs distance from top + the child's height
            let height = childScrollView.contentSize.height + vcScrollView.convert(childScrollView.frame, from: childSuper).origin.y
            if let should = self?.shouldUpdateHeight, should{
                self?.contentHeight?.update(offset: height)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateHeight()
    }
    
    func setupChildViewController() {
        if let viewController = viewController as? UIViewController {
            addChild(viewController)
        }

        viewController.view.frame = self.view.frame
        self.contentView.addSubview(viewController.view)
        
        viewController.view.snp.makeConstraints { make in
            self.contentTop = make.top.equalTo(0).constraint
            make.leading.equalTo(self.view)
            make.trailing.equalTo(self.view)
            make.height.equalTo(self.view)
        }

        viewController.didMove(toParentViewController: self)
    }

    func setupViews() {
        outterScrollView.isScrollEnabled = true
        outterScrollView.showsVerticalScrollIndicator = false
        outterScrollView.delaysContentTouches = false
        
        contentView.backgroundColor = .clear
        
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(panGesture(recognizer:)))
        gesture.delegate = self
        outterScrollView.addGestureRecognizer(gesture)
        
        heightView.backgroundColor = .clear
        heightView.isUserInteractionEnabled = false
        
        outterScrollView.delegate = self
        outterScrollView.isUserInteractionEnabled = true
        outterScrollView.bounces = true
        outterScrollView.backgroundColor = .clear
        view.addSubview(outterScrollView)
        
        outterScrollView.addSubview(contentView)
        contentView.addSubview(heightView)
    }
    
    func setupConstraints() {
        outterScrollView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview()
            make.width.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        contentView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview()
            make.width.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        heightView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
            self.contentHeight = make.height.equalTo(0).constraint
        }
    }
}

extension ScrollManager: UIScrollViewDelegate {
    /*
    * 1- If (2) can still scroll down, set it's content offset equals to outterScrollView's content offset
    * 2- If (2) cannot scroll down, check if (3) can scroll down
    * 3- If (3) can scroll down, scroll it (remember to account for (2)'s size)
    * 4- If (3) cannot scroll down, we reached the bottom
    */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let vcScrollView = viewController.scrollView,
            let childScrollView = viewController.childScrollView else {
            return
        }
        
        // Update the outterScroll contentOffset
        updateHeight()
        
        let isUp = previousY > scrollView.contentOffset.y
        
        if !isUp && scrollView.contentOffset.y > 0 && !finishedLoading {
            scrollView.contentOffset.y = 0
            self.contentTop?.update(offset: 0)
            return
        }
        
        // Move the whole content to keep it visible
        self.contentTop?.update(offset: scrollView.contentOffset.y)
        
        // If the top scroll view cannot scroll down anymore
        if !vcScrollView.canScrollDown() {
            // If it's going up and the second scroll view cannot go up
            if isUp && !childScrollView.canScrollUp() {
                // Update the top scrollView
                vcScrollView.setSafeOffset(offset: scrollView.contentOffset, animated: false, bounce: true)
                return
            }
            // Else update the second scrollview
            let childScrollYOffset = self.outterScrollView.contentOffset.y - vcScrollView.contentOffset.y
            childScrollView.setSafeOffset(offset: CGPoint(x: 0, y: childScrollYOffset))
            
            return
        }
        // If the top scroll view can go down, just move it
        vcScrollView.setSafeOffset(offset: scrollView.contentOffset, animated: false, bounce: true)
        
        previousY = scrollView.contentOffset.y
    }
}
extension ScrollManager: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    @objc func panGesture(recognizer: UIPanGestureRecognizer) {
        if recognizer.state == .ended {
            shouldUpdateHeight = true
            updateHeight()
            return
        }
        
        shouldUpdateHeight = false
    }
    
}
extension ScrollManager: ScrollDelegate {
    func disableView() {
        self.view.isUserInteractionEnabled = false
    }
    
    func enableView() {
        self.view.isUserInteractionEnabled = true
    }
}

extension UIScrollView {
    
    // Finger direction
    func canScrollDown() -> Bool {
        return self.contentOffset.y + 1 < self.contentSize.height - self.frame.size.height
    }
    
    // Finger direction
    func canScrollUp() -> Bool {
        return self.contentOffset.y > 0.0
    }
    
    // Only for the y coordinate for now
    func setSafeOffset(offset: CGPoint, animated: Bool = false, bounce: Bool = false) {
        var newOffset = offset
        let bottom = self.contentSize.height - self.frame.size.height
        
        let limitOffSetY: CGFloat =  bounce ? -150.0 : 0.0
        if newOffset.y < limitOffSetY {
            newOffset.y = limitOffSetY
        }
        
        if newOffset.y > bottom {
            newOffset.y = bottom < 0 ? 0 : bottom
        }
        
        self.setContentOffset(newOffset, animated: animated)
    }
}
