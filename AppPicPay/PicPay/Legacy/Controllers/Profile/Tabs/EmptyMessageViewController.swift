import Foundation
import UI

final class EmptyMessageViewController: PPBaseViewController, Scrollable {
    
    enum type {
        case noTransactions
        case cantSee
    }
    
    // MARK: - IBOutlets
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var mainLabel: UILabel!
    
    // MARK: - Variables
    lazy var scrollView: UIScrollView = {
        return UIScrollView()
    }()
    private var username = ""
    private var messageType: EmptyMessageViewController.type = .noTransactions

    // MARK: - Life Cycle
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        commonInit()
    }
    
    private func commonInit() {
        preferredContentSize = CGSize(width: self.preferredContentSize.width, height: 260)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareLayout()
        updateType(messageType: messageType, username: username)
    }
    
    private func prepareLayout() {
        view.backgroundColor = Palette.ppColorGrayscale100.color
        mainLabel.textColor = Palette.ppColorGrayscale400.color
        descriptionLabel.textColor = Palette.ppColorGrayscale400.color
    }
    
    // MARK: - Message
    func updateType(messageType: EmptyMessageViewController.type, username: String = "") {
        self.messageType = messageType
        self.username = username
        
        if mainLabel == nil {
            return
        }
        
        switch messageType {
        case .noTransactions:
            mainLabel.text = "@\(username)\nnão fez nenhuma transação ainda"
            descriptionLabel.text = ""
            imageView.image = #imageLiteral(resourceName: "no-transactions-icon")
        case .cantSee:
            mainLabel.text = "Esta conta é fechada"
            descriptionLabel.text = "Siga essa conta para ver suas atividades"
            imageView.image = #imageLiteral(resourceName: "lock-icon")
        }
    }
}
