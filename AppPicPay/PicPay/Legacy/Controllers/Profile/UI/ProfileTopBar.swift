import Foundation
import SnapKit
import UI
import FeatureFlag

final class ProfileTopBar: UIView {
    fileprivate let imageView = UIImageView()
    let overlay = UIView()
    fileprivate var usernameY: Constraint?
    fileprivate var usernameCenterY: Constraint?
    fileprivate var usernameCenterYOffset:CGFloat = 10 // it's not quite the center
    fileprivate let usernameLabel = UILabel()
    fileprivate let backButton = UIButton()
    let rightButton = UIButton()
    
    var maxHeight:CGFloat
    var minHeight:CGFloat
    
    var dismissAction: (() -> ())?
    var showMenuAction: (() -> ())?
    var editAction: (() -> ())?
    var moreAction: (() -> ())?
    
    init(image: UIImage?, maxHeight: CGFloat, minHeight: CGFloat, showMenu: Bool = false, showEdit: Bool = false) {
        self.maxHeight = maxHeight
        self.minHeight = minHeight
        
        super.init(frame: CGRect.zero)
        
        self.clipsToBounds = true
        setupViews(image: image, showMenu: showMenu, showEdit: showEdit)
        setupConstraints()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateUsername(username: String) {
        usernameLabel.text = String("@\(username)")
    }
    
    func updateImage(url: URL?, thumb: URL? = nil) {
        guard let url = url else {
            return
        }
        overlay.isHidden = false
        if let thumb = thumb {
            imageView.setImage(url: thumb) { [weak self] (image) in
                self?.imageView.setImage(url: url, placeholder: image)
            }
        } else {
            imageView.setImage(url: url)
        }
    }
    
    func backButton(display: Bool) {
        backButton.isHidden = !display
    }
    
    func setupViews(image: UIImage?, showMenu: Bool, showEdit: Bool) {
        
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.image = image
        imageView.backgroundColor = Palette.ppColorBranding300.color
        
        backButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        backButton.addTarget(self, action: #selector(dismiss), for: .touchUpInside)
        
        if showMenu {
            backButton.setImage(UIImage(named: "ico_nav_hamburger.png"), for: .normal)
            backButton.addTarget(self, action: #selector(presentMenu), for: .touchUpInside)
        }
        
        if showEdit {
            rightButton.setTitle("Editar", for: .normal)
            rightButton.setImage(nil, for: .normal)
            rightButton.addTarget(self, action: #selector(edit), for: .touchUpInside)
            rightButton.isHidden = false
        }else{
            rightButton.setImage(#imageLiteral(resourceName: "moreIcon"), for: .normal)
            rightButton.addTarget(self, action: #selector(more), for: .touchUpInside)
            rightButton.isHidden = !FeatureManager.isActive(.featureReportProfile)
        }
        
        overlay.backgroundColor = Palette.ppColorGrayscale600.color.withAlphaComponent(0.65)
        
        overlay.isHidden = image == nil
        
        usernameLabel.textColor = Palette.ppColorGrayscale000.color
        usernameLabel.font = UIFont.boldSystemFont(ofSize: 17)
        
        addSubview(imageView)
        addSubview(overlay)
        addSubview(usernameLabel)
        addSubview(backButton)
        addSubview(rightButton)
    }
    
    func updateTopInset(_ inset: CGFloat) {
        backButton.snp.updateConstraints { make in
            make.top.equalTo(inset)
        }
        
        rightButton.snp.updateConstraints { make in
            make.top.equalTo(inset + 12)
        }
    }
    
    func setupConstraints() {
        imageView.snp.makeConstraints(sameAsSuperview)
        overlay.snp.makeConstraints(sameAsSuperview)
        
        backButton.snp.makeConstraints { make in
            make.leading.equalTo(0)
            make.top.equalTo(15)
            make.height.equalTo(50)
            make.width.equalTo(50)
        }
        
        rightButton.snp.makeConstraints { make in
            make.trailing.equalTo(-15)
            make.top.equalTo(30)
        }
        
        usernameLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            self.usernameCenterY = make.centerY.equalToSuperview().offset(usernameCenterYOffset).constraint
            self.usernameCenterY?.deactivate()
            self.usernameY = make.top.equalTo(self.snp.bottom).constraint
        }
    }
    
    func sameAsSuperview(make: ConstraintMaker) {
        make.edges.equalToSuperview()
    }
    
    func animateUsernameLabel(labelOffset: CGFloat, barHeight: CGFloat) {
        let isMinHeight = barHeight == minHeight
        // If the bar is not as short as it can be, just "hide" the username label and fuck off
        if !isMinHeight {
            usernameY?.update(offset: 0)
            usernameCenterY?.deactivate()
            usernameY?.activate()
            return
        }
        
        // Update the label (negative because we are moving up)
        usernameY?.update(offset: -labelOffset)
        
        // Get the y position on which the label would be centered with the bar
        let labelCenterY = barHeight / 2 + self.usernameLabel.frame.size.height / 2
        
        // If the offset is greater than the centerY (considering a little offset), we lock the label in the center
        let shouldLockOnCenter = labelOffset >= labelCenterY - usernameCenterYOffset && isMinHeight
        
        if shouldLockOnCenter {
            // Lock on center
            usernameY?.deactivate()
            usernameCenterY?.activate()
        }else{
            // Set position related to super view's bottom
            usernameCenterY?.deactivate()
            usernameY?.activate()
        }
    }
    
    @objc func dismiss() {
        dismissAction?()
    }
    
    @objc func edit() {
        editAction?()
    }
    
    @objc func presentMenu() {
        showMenuAction?()
    }
    
    @objc func more() {
        moreAction?()
    }
}
