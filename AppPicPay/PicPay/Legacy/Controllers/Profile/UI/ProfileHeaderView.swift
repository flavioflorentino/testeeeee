//
//  ProfileHeaderView.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 29/05/17.
//
//

import Foundation
import UI

final class ProfileHeaderView: UIView {
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var bio: UITextView!
    @IBOutlet weak var payButton: UIPPButton!
    @IBOutlet weak var followerCount: UILabel!
    @IBOutlet weak var followingCount: UILabel!
    @IBOutlet weak var followButton: UIPPFollowButton!
    @IBOutlet weak var payButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var followButtonContainerWidth: NSLayoutConstraint!
    @IBOutlet weak var followInfoDistance: NSLayoutConstraint!
    @IBOutlet weak var followButtonContainer: UIView!
    
    @IBOutlet weak var extraView: UIView!
    @IBOutlet weak var extraViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var bioHeight: NSLayoutConstraint!
    
    @IBOutlet weak var followersButton: UIButton!
    @IBOutlet weak var followingButton: UIButton!
    
    @IBOutlet weak var topMargin: NSLayoutConstraint!
    @IBOutlet weak var bottomMargin: NSLayoutConstraint!
    
    var followAction: ((UIPPFollowButton) -> ())?
    var payAction: ((UIPPButton) -> ())?
    var followersAction: (() -> ())?
    var followingAction: (() -> ())?
    
    func setup(isSelf: Bool = false) {
        followButton.setup()
        followButton.setupTintImage()
        
        followButton.centerLoading = true
        
        payButton.normalBackgrounColor = Palette.ppColorBranding300.color
        payButton.normalTitleColor = Palette.ppColorGrayscale000.color
        payButton.disabledBackgrounColor = Palette.ppColorGrayscale300.color
        payButton.disabledBorderColor = Palette.ppColorGrayscale300.color
        payButton.disabledTitleColor = Palette.ppColorGrayscale000.color
        payButton.cornerRadius = payButtonHeight.constant / 2
        payButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        payButton.setImage(#imageLiteral(resourceName: "coinIcon"), for: .normal)
        payButton.leftIconPadding = 20
        payButton.tintImage = true
        payButton.isEnabled = false
        
        followerCount.text = ""
        followingCount.text = ""
        
        name.text = ""
        
        bioHeight.constant = 0
        
        username.backgroundColor = .clear
        username.accessibilityIdentifier = "mainUsername"
        username.text = ""
        
        followButtonContainer.clipsToBounds = true
        
        if isSelf {
            prepareForSelf()
        }
    }
    
    func updateInfo(with contact: PPContact, consumerStatus: FollowerStatus, followerStatus: FollowerStatus) {
        if let username = contact.username {
            let fullUsername = String("@\(username)")
            self.username.text = username != "" ? fullUsername : ""
        }
        name.text = contact.onlineName ?? ""
        
        if contact.bio != nil && contact.bio != "" {
            bio.text = contact.bio
            bioHeight.constant = 100
        }else{
            bioHeight.constant = 0
        }
        
        bio.textAlignment = NSTextAlignment.center
        
        followerCount.text = String(contact.followers)
        followingCount.text = String(contact.following)
        
        followButton.changeButtonWithStatus(followerStatus, consumerStatus: consumerStatus)
        
        if contact.wsId == ConsumerManager.shared.consumer?.wsId {
            prepareForSelf()
        }
        
        let canSee = contact.isOpenFollowersAndFollowings
        followersButton.isUserInteractionEnabled = canSee
        followingButton.isUserInteractionEnabled = canSee
        
        followerCount.textColor = Palette.ppColorBranding300.color
        followingCount.textColor = Palette.ppColorBranding300.color
        
        if !canSee {
            followerCount.textColor = .lightGray
            followingCount.textColor = .lightGray
        }
    }
    
    private func prepareForSelf() {
        payButtonHeight.constant = 0
        followButtonContainerWidth.constant = 0
        bottomMargin.constant = 0
        followInfoDistance.constant = 30
    }
    
    func setupExtraView(_ extra: UIView){
        extraView.addSubview(extra)
        extra.snp.makeConstraints({ (make) in
            make.right.top.left.bottom.equalToSuperview()
        })
        NSLayoutConstraint.deactivate([extraViewHeight])
    }
    
    @IBAction private func followAction(_ sender: UIPPFollowButton) {
        followAction?(sender)
    }
    
    @IBAction private func payAction(_ sender: UIPPButton) {
        payAction?(sender)
    }
    @IBAction private func tapFollowers(_ sender: Any) {
        followersAction?()
    }
    @IBAction private func tapFollowing(_ sender: Any) {
        followingAction?()
    }
}
