import AnalyticsModule
import Core
import DirectMessage
import DirectMessageSB
import FeatureFlag

protocol NewProfileViewModelInputs {
    func checkConsumerStudentStatus()
    func openStudentAccountOffer()
    func tryPaymentRequest()
    func openDirectMessage()
    func openDirectMessageSB()
    func checkIfProfileCanOpenDirectMessageSB()
}

final class NewProfileViewModel: NSObject {
    typealias Dependencies = HasAnalytics & HasFeatureManager
    
    private let favoritesService: FavoritesServicing = FavoritesService()
    
    var userProfile: PPContact?
    var consumerStatus: FollowerStatus?
    var followerStatus: FollowerStatus?
    var profileId: String
    var isShowingPlaceholder = true
    var isFavorite: Bool = false
    
    private let service: ProfileServicing
    private let paymentRequestService: PaymentRequestSelectorServicing
    private let presenter: ProfilePresenting
    private let dependencies: Dependencies
    
    init(
        profileId: String,
        placeHolderContact: PPContact? = nil,
        service: ProfileServicing,
        paymentRequestService: PaymentRequestSelectorServicing,
        presenter: ProfilePresenting,
        dependencies: Dependencies
    ) {
        self.profileId = profileId
        self.service = service
        self.paymentRequestService = paymentRequestService
        self.presenter = presenter
        self.dependencies = dependencies
        super.init()
        self.userProfile = placeHolderContact
    }
    
    func isCurrentUser() -> Bool {
        guard let currentId = ConsumerManager.shared.consumer?.wsId else {
            return false
        }
        return profileId == String(currentId)
    }
    
    func isOpenFollowersAndFollowings() -> Bool {
        guard let profile = userProfile else {
            return false
        }
        return profile.isOpenFollowersAndFollowings
    }
    
    func userDidOpen() {
        guard let currentId = ConsumerManager.shared.consumer?.wsId else {
            return
        }
        dependencies.analytics.log(ProfileEvent.profileOpened(userID: String(currentId)))
    }
    
    func loadUser(completed: @escaping ((PPContact) -> ())) {
        WSSocial.getConsumerProfile(self.profileId) { [weak self] (contact, consumerStatus, followerStatus, error) in
            DispatchQueue.main.async {
                self?.userProfile = contact
                self?.consumerStatus = consumerStatus
                self?.followerStatus = followerStatus
                if let contact = contact {
                    self?.isShowingPlaceholder = false
                    completed(contact)
                    return
                }
                // TODO: Handle error
            }
        }
    }
    
    func follow(sender: UIPPFollowButton, completion: @escaping (Error?) -> Void) {
        guard let user = userProfile, let oldStatus = FollowerStatus(rawValue: user.followerStatus), !isShowingPlaceholder else {
            return
        }
        
        let action = sender.action
        
        // Change the notification status to give feedback to the user
        sender.changeButtonWithStatus(FollowerStatus.loading, consumerStatus: FollowerStatus.loading)
        sender.setNeedsLayout()
        
        // call server
        WSSocial.followAction(action, follower: "\(user.wsId)",completion: { (success, consumerStatus, followerStatus, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                if success {
                    //update button with new status
                    sender.changeButtonWithStatus(followerStatus, consumerStatus: consumerStatus)
                    return
                }
                // rolback the button follow status
                sender.changeButtonWithStatus(FollowerStatus.undefined, consumerStatus: oldStatus)
                
                // show error message
                if let err = error {
                    completion(err)
                }
            })
        })
    }
    
    func setFavorite(id: String) {
        isFavorite ? unfavorite(id) : favorite(id)
    }
    
    func checkUserIsFavorite(completion: @escaping () -> Void) {
        favoritesService.isFavorite(id: profileId, type: .consumer) { [weak self] isFavorite in
            self?.isFavorite = isFavorite
            completion()
            
        }
    }
    
    private func trackProfileEvent(isStudent: Bool) {
        guard let user = userProfile else {
            return
        }
        
        if user.businessAccount {
            dependencies.analytics.log(ProfileEvent.accessed(type: .pro))
        }
        if user.isVerified {
            dependencies.analytics.log(ProfileEvent.accessed(type: .verified))
        }
        if isStudent {
            dependencies.analytics.log(ProfileEvent.accessed(type: .university))
        }
        if !user.businessAccount && !user.isVerified && !isStudent {
            dependencies.analytics.log(ProfileEvent.accessed(type: .regular))
        }
    }
}

private extension NewProfileViewModel {
    func favorite(_ id: String) {
        isFavorite = true
        
        favoritesService.favorite(id: id, type: .consumer) { [weak self] didFavorite in
            self?.isFavorite = didFavorite
            
            if didFavorite {
                self?.dependencies.analytics.log(FavoritesEvent.statusChanged(true, id: id, origin: .profile))
            }
        }
    }
    
    func unfavorite(_ id: String) {
        isFavorite = false
        
        favoritesService.unfavorite(id: id, type: .consumer) { [weak self] didUnfavorite in
            self?.isFavorite = didUnfavorite == false
            
            if didUnfavorite {
                self?.dependencies.analytics.log(FavoritesEvent.statusChanged(false, id: id, origin: .profile))
            }
        }
    }
}

extension NewProfileViewModel {
    func abusesUser(_ action: AbusesAction, _ completion: @escaping (Result<Void, Error>) -> Void) {
        service.abusesUser(consumerId: profileId, action: action, completion)
    }
}

extension NewProfileViewModel: NewProfileViewModelInputs {
    func checkConsumerStudentStatus() {
        guard
            service.hasStudentComponentFeature,
            let studentComponent = service.studentComponent,
            let user = userProfile
            else {
            trackProfileEvent(isStudent: false)
            return
        }
        
        service.loadStudentStatus(consumerId: profileId) { [weak self] consumerStatusResult in
            if case let .success(consumerStatus) = consumerStatusResult, consumerStatus.status == .active {
                if !user.businessAccount && !user.isVerified {
                    self?.presenter.presentStudentAccountView(with: studentComponent)
                }
                self?.trackProfileEvent(isStudent: true)
            } else {
                self?.trackProfileEvent(isStudent: false)
            }
        }
    }
    
    func openStudentAccountOffer() {
        dependencies.analytics.log(StudentAccountEvent.profileOffer)
        guard let url = URL(string: service.studentOfferWebview) else {
            return
        }
        presenter.presentStudentAccountOffer(url: url)
    }
    
    func tryPaymentRequest() {
        guard (userProfile?.canReceivePaymentRequest ?? true) else {
            dependencies.analytics.log(PaymentRequestUserEvent.notAllowed)
            let username = userProfile?.username ?? ""
            presenter.presentPaymentRequestNotAllowedAlert(username: username)
            return
        }
        guard dependencies.featureManager.isActive(.paymentRequestV2) else {
            getPaymentRequestPermission()
            return
        }
        getPaymentRequestPermissionV2()
    }

    private func getPaymentRequestPermission() {
        paymentRequestService.getPaymentRequestPermission { [weak self] result in
            switch result {
            case let .success(response):
                guard !response.model.data.isP2PBlockDayChargesEnabled else {
                    self?.dependencies.analytics.log(PaymentRequestUserEvent.didRequestLimitExceeded)
                    self?.presenter.presentPaymentRequestLimitExceeded()
                    return
                }
                guard let profile = self?.userProfile else {
                    self?.picpaySelectionFailureV2(error: DialogError.default(ApiError.unknown(nil)))
                    return
                }
                self?.dependencies.analytics.log(PaymentRequestUserEvent.start(origin: DGHelpers.Origin.profile.rawValue))
                self?.presenter.presentPaymentRequestSuccess(profile: profile)
            case .failure:
                self?.presenter.presentPaymentRequestError()
            }
        }
    }
    
    private func getPaymentRequestPermissionV2() {
        paymentRequestService.getPaymentRequestPermissionV2 { [weak self] result in
            guard let profile = self?.userProfile else {
                self?.picpaySelectionFailureV2(error: DialogError.default(ApiError.unknown(nil)))
                return
            }
            switch result {
            case .success:
                self?.presenter.presentPaymentRequestSuccess(profile: profile)
            case let .failure(error):
                self?.picpaySelectionFailureV2(error: error)
            }
        }
    }
    
    private func picpaySelectionFailureV2(error: DialogError) {
        switch error {
        case let .dialog(dialogError):
            guard let errorCode = PaymentRequestSelectorViewModel.ErrorCode(rawValue: dialogError.code) else {
                presenter.presentPaymentRequestError()
                return
            }
            handleErrorCode(errorCode, dialogError: dialogError)
        default:
            presenter.presentPaymentRequestError()
        }
    }
    
    private func handleErrorCode(_ errorCode: PaymentRequestSelectorViewModel.ErrorCode, dialogError: DialogResponseError) {
        if errorCode == .limitExceeded {
            let title = dialogError.model.title
            let message = dialogError.model.message
            let buttonTitle = dialogError.model.buttonText
            presenter.presentPaymentRequestLimitExceededV2(title: title, message: message, buttonTitle: buttonTitle)
            Analytics.shared.log(PaymentRequestUserEvent.didRequestLimitExceeded)
        }
    }

    func openDirectMessage() {
        guard let sender = ConsumerManager.shared.consumer?.asDMContact,
              let recipient = userProfile?.asDMContact else {
            return
        }
        presenter.openDirectMessage(sender: sender, recipient: recipient)
    }
    
    func openDirectMessageSB() {
        guard let chatUser = userProfile?.asChatUser else { return }
        presenter.openDirectMessageSB(with: chatUser)
        dependencies.analytics.log(.button(.clicked, .openChatViaProfile))
    }

    func checkIfProfileCanOpenDirectMessageSB() {
        guard !isShowingPlaceholder, let chatUser = userProfile?.asChatUser else { return }
        service.canReach(user: chatUser) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let canOpenDirectMessageSB):
                self.presenter.setMessageButtonVisibility(canOpenDirectMessageSB)
            case .failure(let error):
                debugPrint(error.localizedDescription)
            }
        }
    }
}

extension PPContact {
    var asChatUser: ChatUser {
        DMSBChatUser(messagingId: wsId, type: .consumer, username: username, avatarUrl: imgUrl)
    }
    var asDMContact: Contact {
        Contact(id: wsId, imageURL: imgUrl ?? String(), nickname: username ?? String(), name: onlineName ?? String())
    }
}

fileprivate extension MBConsumer {

    var asDMContact: Contact {
        Contact(id: wsId, imageURL: img_url ?? String(), nickname: username ?? String(), name: name ?? String())
    }
}
