import Foundation
import UI

final class PlateValidator: NSObject {
    private static let oldBrazilPlate = "[A-Za-z]{3}[0-9]{4}"
    private static let newBrasilMercoSulPlate = "[A-Za-z]{3}[0-9]{1}[A-z]{1}[0-9]{2}"
    private static let regex = NSPredicate(format: "SELF MATCHES %@", "\(oldBrazilPlate)|\(newBrasilMercoSulPlate)")
    
    fileprivate static let licensePlateMask = CustomStringMask(mask: "AAA 0X00")
    
    @objc
    static func isValid(_ string: String) -> Bool {
        return regex.evaluate(with: string)
    }
}

// Mark: For use with JMStringMask
extension PlateValidator {
    static func unmask(_ string: String) -> String {
        return licensePlateMask.unmaskedText(from: string) ?? ""
    }
    
    static func mask(_ string: String) -> String? {
        return licensePlateMask.maskedText(from: string)
    }
}
