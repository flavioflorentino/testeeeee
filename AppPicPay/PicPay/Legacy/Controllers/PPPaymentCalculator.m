//
//  PPPaymentCalculator.m
//  PicPay
//
//  Created by Diogo Carneiro on 24/10/14.
//
//

#import "PPPaymentCalculator.h"
#import "NSDecimalNumber+PPDecimalNumber.h"
#import "PicPay-Swift.h"
@import CoreLegacy;

@implementation PPPaymentCalculator

- (id)init{
	self = [super init];
	if (self != nil) {
		paymentConfig = [PPPaymentConfig shared];
	}
	return self;
}

- (void)changePaymentConfig:(PPPaymentConfig *)newConfig{
	paymentConfig = newConfig;
}

- (NSDecimalNumber *)recalculatedValueForInstallmentsQuantity:(NSInteger)installmentQuantity totalValue:(NSDecimalNumber *)totalValue{
	return [[self installmentValueForInstallmentsQuantity:installmentQuantity totalValue:totalValue] decimalNumberByMultiplyingBy:[NSDecimalNumber decimalNumberWithInt:installmentQuantity]];
}

- (NSDecimalNumber *)installmentValueForInstallmentsQuantity:(NSInteger)installmentQuantity totalValue:(NSDecimalNumber *)totalValue{
	NSDecimalNumber *installmentValue = [NSDecimalNumber decimalNumberWithFloat:0];
	NSDecimalNumber *total = totalValue;
	
	if (installmentQuantity > 1) {
        // This is This is the formula of compound interest to calculate the amount of the installments
        //	Ex.: 100 * ((((1+2,99*0,01)^12)*(2,99*0,01)) / (((1+2,99*0,01)^12)-1))
        //	installmentValue = _total * ((pow((1 + _interests * 0.01),installmentQuantity) * _interests * 0.01) / (pow((1 + _interests *0.01),installmentQuantity) - 1));
        
        NSDecimalNumber *interest = [NSDecimalNumber decimalNumberWithDecimal:[paymentConfig.interest decimalValue]];
        NSDecimalNumber *coefficient = paymentConfig.interestCoefficient;
        
        
        // check if has the list of installment interests customized by each installments quantity
        if(paymentConfig.installmentsBusiness!=nil && [paymentConfig.installmentsBusiness count] >0){
            // Adjuste interest value
            NSNumber *interestTemp = (NSNumber*) [paymentConfig.installmentsBusiness objectAtIndex:installmentQuantity - 1];
            interest = [NSDecimalNumber decimalNumberWithDecimal:[interestTemp decimalValue]];
        
            // pre calc intereset coefficient
            coefficient = [[[NSDecimalNumber decimalNumberWithDecimal:[interest decimalValue]] decimalNumberByMultiplyingBy:[NSDecimalNumber decimalNumberWithFloat:0.01]] decimalNumberByAdding:[NSDecimalNumber decimalNumberWithInt:1]];
        }
        
        
        // Calc installments values
        if ([interest isEqualToNumber:[NSNumber numberWithInt:0]]) {
            installmentValue = [total decimalNumberByDividingBy:[NSDecimalNumber decimalNumberWithInt:installmentQuantity]];
        }else{
            installmentValue = [total decimalNumberByMultiplyingBy:[[[coefficient decimalNumberByRaisingToPower:installmentQuantity] decimalNumberByMultiplyingBy:[coefficient decimalNumberBySubtracting:[NSDecimalNumber decimalNumberWithInt:1]]] decimalNumberByDividingBy:[[coefficient decimalNumberByRaisingToPower:installmentQuantity] decimalNumberBySubtracting:[NSDecimalNumber decimalNumberWithInt:1]]]];
        }
        
	}else{
		installmentValue = total;
	}
	
	return installmentValue;
}

- (NSArray *)installmentsListForTotalValue:(NSDecimalNumber *)totalValue{
	NSMutableArray *installments = [[NSMutableArray alloc] init];
	NSDecimalNumber *minInstallmentValue = paymentConfig.minQuotaValue;
	int installmentsQuantity = 1;
    NSInteger maxQuotaQuantity = [paymentConfig.installmentsBusiness count] != 0 ? [paymentConfig.installmentsBusiness count] : paymentConfig.maxQuotaQuantity;
	
	while (installmentsQuantity <= maxQuotaQuantity){
		NSDecimalNumber *installmentValue = [self installmentValueForInstallmentsQuantity:installmentsQuantity totalValue:totalValue];
		//		if ([installmentValue floatValue] >= _minInstallmentValue) {
		if ([installmentValue compare:[NSDecimalNumber decimalNumberWithFloat:0]] != NSOrderedAscending && (installmentsQuantity == 1 || ([installmentValue compare:minInstallmentValue] == NSOrderedDescending || [installmentValue compare:minInstallmentValue] == NSOrderedSame))) {
			[installments addObject:installmentValue];
		}else{
			break;
		}
		installmentsQuantity++;
	}
	
	return installments;
}

@end
