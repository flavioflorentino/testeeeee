import UI

protocol SocialOnboardingTableViewProtocol: AnyObject {
    func socialOnboardingDidChangeSelection(_ controller: SocialOnboardingTableViewController)
}

private extension SocialOnboardingTableViewController.Layout {
    enum Size {
        static let insetCoverViewHeight: CGFloat = 2
        static let insetCoverViewTag = 100
        static let insetCoverViewZPos: CGFloat = 100
    }
    
    enum Insets {
        static let descriptionLabelInsets: UIEdgeInsets = UIEdgeInsets(
            top: Spacing.base03,
            left: Spacing.base02,
            bottom: Spacing.base02,
            right: Spacing.base01
        )
    }
}

final class SocialOnboardingTableViewController: UITableViewController {
    fileprivate enum Layout { }
    
    private lazy var tableHeaderView = UIView()
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label
            .labelStyle(CaptionLabelStyle())
            .with(\.textColor, .grayscale950())
        return label
    }()
    private lazy var insetCoverView: UIView = {
        let view = UIView()
        view.tag = Layout.Size.insetCoverViewTag
        view.layer.zPosition = Layout.Size.insetCoverViewZPos
        return view
    }()
    
    var model: SocialOnboardingViewModel?
    weak var delegate: SocialOnboardingTableViewProtocol?
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableHeaderView()
        configureTableView()
    }

    // MARK: - Dependence Injection
    func setup(model: SocialOnboardingViewModel){
        self.model = model
    }
    
    // MARK: - Internal Methods
    
    fileprivate enum Cell: String {
        case header
        case item
    }
    
    private func configureTableHeaderView() {
        tableHeaderView.addSubviews(
            descriptionLabel,
            insetCoverView
        )
        
        descriptionLabel.snp.makeConstraints {
            $0.edges.equalTo(tableHeaderView).inset(Layout.Insets.descriptionLabelInsets)
        }
        
        insetCoverView.snp.makeConstraints {
            $0.height.equalTo(Layout.Size.insetCoverViewHeight)
            $0.leading.trailing.equalTo(tableHeaderView)
            $0.centerY.equalTo(tableHeaderView.snp.bottom)
        }
        
        descriptionLabel.text = (model?.didSkipFindFriends ?? false) ?
            OnboardingLocalizable.followFriendsRecommendedContacts.text : OnboardingLocalizable.followFriendsLocalContacts.text
    }
    
    fileprivate func configureTableView(){
        // configure cell types
        tableView.backgroundColor = Palette.ppColorGrayscale100.color
        let headerNib = UINib(nibName: "SocialHeaderTableViewCell", bundle: nil)
        tableView.register(headerNib, forCellReuseIdentifier: Cell.header.rawValue)
        
        let itemNib = UINib(nibName: "SocialOnboardingTableViewCell", bundle: nil)
        tableView.register(itemNib, forCellReuseIdentifier: Cell.item.rawValue)
        
        self.tableHeaderView.backgroundColor = self.view.backgroundColor
        if let insetCover = self.tableHeaderView.viewWithTag(100) {
            insetCover.backgroundColor = self.view.backgroundColor
        }
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 60.0
        
        // place a footer as bottom padding to adjust the table size to show the last item
        // Obs.: it is needed beacouse the float button is over the tableView
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 40))
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        guard let model = model else { return 0}
        return model.numberOfSection
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let model = model else { return 0}
        
        let rows = model.numberOfRows(at: section)
        if rows > 0 {
            // add one item to show the header (Select all button)
            return rows + 1
        }
        return rows
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            // Header to select all
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Cell.header.rawValue, for: indexPath) as? SocialHeaderTableViewCell
                else {
                return UITableViewCell()
            }
            
            cell.configureCell("Seguir todos", checked: model?.followAllCheck ?? false )
            cell.delegate = self
            return cell
            
        }else{
            // Item
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Cell.item.rawValue, for: indexPath) as? SocialOnboardingTableViewCell
                else {
                return UITableViewCell()
            }
            
            // remove the one item reserved to header (Sellect all button)
            let index = IndexPath(row: indexPath.row - 1, section: indexPath.section)
            
            if let contact = model?.contact(forRow: index), let selected = model?.isSelected(forRow: index) {
                cell.configureCell(contact, checked: selected)
                return cell
            }
        }
        
        // fallback
        return UITableViewCell()
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row >= 1 {
            // remove the 2 itens reserved to header and description
            let index = IndexPath(row: indexPath.row - 1, section: indexPath.section)
            model?.selectContactToFollow(at: index)
            
            //update rows
            let rows = model?.numberOfRows(at: indexPath.section)
            if rows > 0 {
                let selectAllIndex = IndexPath(row: 0, section: indexPath.section)
                tableView.reloadRows(at: [indexPath, selectAllIndex], with: .none)
                delegate?.socialOnboardingDidChangeSelection(self)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.tableHeaderView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        //Ajuste da largura da label na tableViewHeader
        if let label = self.tableHeaderView.subviews.first(where: {$0 is UILabel}) as? UILabel {
            let margin:CGFloat = 16
            label.preferredMaxLayoutWidth = self.tableView.frame.width - (2 * margin)
        }
        return self.tableHeaderView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 60
        } else {
            return UITableView.automaticDimension
        }
    }
    
}

extension SocialOnboardingTableViewController: SocialHeaderTableViewCellDelegate {
    
    // MARK: - SocialHeaderTableViewCellDelegate
    
    func socialHeaderDidChangeCheckmark(_ cell: SocialHeaderTableViewCell) {
        model?.selectAllContactToFollow(cell.checkmark.checked)
        delegate?.socialOnboardingDidChangeSelection(self)
        tableView.reloadData()
    }
}
