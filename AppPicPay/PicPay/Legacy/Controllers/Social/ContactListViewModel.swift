//
//  FollowingTableViewModel.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 22/12/16.
//
//

import UIKit

class ContactListViewModel: NSObject {
    var items:[PPFollowing]
    var title:String
    var showMoreButton:Bool = false
    var emptyText:String = ""
    
    override init() {
        self.items = [PPFollowing]()
        self.title = ""
        super.init()
    }
    
    /**
     * Looading the following items
     */
    func loadItems(_ callback: @escaping (_ items:[PPFollowing]?, _ error:Error?) -> Void){
        
    }
    
    /**
     * Remove a item at index
     */
    func removeItem(_ index:Int){
        if index < self.items.count {
            self.items.remove(at: index)
        }
    }
    
    func removeAllItems(){
        self.items.removeAll()
    }
}
