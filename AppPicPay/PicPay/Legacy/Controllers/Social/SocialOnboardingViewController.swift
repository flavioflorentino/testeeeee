import AnalyticsModule
import CoreLegacy
import UI
import UIKit

final class SocialOnboardingViewController: PPBaseViewController {
    enum OriginFlow {
        case home
        case registration
    }
    
    private var originFlow: OriginFlow?
    private var coordinator: FollowFriendsCoordinating?
    
    @IBOutlet weak var followButton: UIPPButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var bottomBar: UIView!
    
    private var viewModel: SocialOnboardingViewModel?
    private var tableViewController: SocialOnboardingTableViewController?
    private var loadingView: SocialOnboardingLoadingView?
    private var needUpdateFollowButton = true
    
    private var errorView: UIView {
        let errorView = ConnectionErrorView(with: nil, type: .connection, frame: view.frame)
        errorView.backgroundColor = Palette.ppColorGrayscale000.color
        errorView.tryAgainTapped = { [weak self] in
            self?.loadFollowSuggestion()
        }
        return errorView
    }
    
    // MARK: - View Lify Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        configureLayout()
        updateFollowButton()
        loadFollowSuggestion()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
        showLoadingView()
    }
    
    // MARK: - Dependence Injection
    func setup(viewModel: SocialOnboardingViewModel, originFlow: OriginFlow, coordinator: FollowFriendsCoordinating? = nil) {
        self.viewModel = viewModel
        self.originFlow = originFlow
        self.coordinator = coordinator
        self.coordinator?.viewController = self
        
        if originFlow == .registration {
            navigationItem.hidesBackButton = true
        }
        
        title = viewModel.didSkipFindFriends ?
            OnboardingLocalizable.followRecommendedFriendsTitle.text : OnboardingLocalizable.followLocalFriendsTitle.text
    }
    
    // MARK: - User Actions
    
    @IBAction private func follow(_ sender: Any) {
        needUpdateFollowButton = false
        
        followButton.setTitle("", for: .disabled)
        followButton.isEnabled = false
        followButton.startLoadingAnimating()
        
        viewModel?.follow({ [weak self] (error) in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.needUpdateFollowButton = true
            strongSelf.followButton.stopLoadingAnimating()
            strongSelf.followButton.isEnabled = true
            if let error = error {
                AlertMessage.showAlert(withMessage: error.localizedDescription, controller: strongSelf)
                return
            }
            strongSelf.successFollowingContacts()
        })
    }
    
    private func successFollowingContacts() {
        viewModel?.cancelSocialOnboarding()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FeedNeedReload"), object: nil)
        if originFlow == .registration {
            showNextOnboardScreen()
            return
        }
        dismiss(animated: true)
    }
    
    @IBAction private func cancel(_ sender: Any) {
        Analytics.shared.log(OnboardingEvent.social(.didNotFollow))
        viewModel?.cancelSocialOnboarding(isCancelButton: true)
        if originFlow == .registration {
            showNextOnboardScreen()
            return
        }
        dismiss(animated: true)
    }
    
    // MARK: - Internal Method
    
    private func configureLayout() {
        bottomBar.backgroundColor = Palette.ppColorGrayscale100.color
        view.backgroundColor = Palette.ppColorGrayscale100.color
    }
    
    @objc
    private func loadFollowSuggestion() {
        hideErrorView()
        
        showLoadingView()
        viewModel?.loadContactBook({ [weak self] error in
            guard error == nil else {
                self?.showErrorView()
                return
            }
            
            self?.dismissLoadingView()
            self?.tableViewController?.tableView.reloadData()
            
            if self?.viewModel?.contactListCount == 0 {
                self?.tableViewController?.view.isHidden = true
                self?.bottomBar.isHidden = true
                
                if self?.originFlow == .registration {
                    self?.showInviteFriendsScreen()
                    return
                }
                self?.showEmptyContactListPopup()
            }
        })
    }
    
    private func updateFollowButton() {
        guard needUpdateFollowButton, let viewModel = viewModel else {
            return
        }
        UIView.performWithoutAnimation {
            followButton.isEnabled = viewModel.numberContactToFollow > 0
            followButton.isEnabled ? followButton.setTitle("Seguir \(viewModel.numberContactToFollow)", for: .normal) : followButton.setTitle("Seguir", for: .disabled)
        }
    }
    
    /// Add the table View as a container view
    private func configureTableView() {
        tableViewController = ViewsManager.socialStoryboardViewController(withIdentifier: "SocialOnboardingTable") as? SocialOnboardingTableViewController
        guard let controller = tableViewController else {
            return
        }
        
        //configure controller
        if let model = viewModel {
            controller.setup(model: model)
        }
        controller.delegate = self
        
        // prepare to move to controller
        addChild(controller)
        controller.willMove(toParent: self)
        
        // Apply constraint
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(controller.view)
        
        NSLayoutConstraint.constraintAllEdges(from: controller.view, to: contentView)
        controller.didMove(toParent: self)
    }
    
    private func showLoadingView() {
        if loadingView == nil {
            loadingView = SocialOnboardingLoadingView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
            view.addSubview(loadingView!)
            view.setNeedsLayout()
            view.layoutIfNeeded()
            loadingView?.startAnimation()
        }
    }
    
    private func dismissLoadingView() {
        loadingView?.stopAnimation()
        UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseOut, animations: { 
            self.loadingView?.layer.opacity = 0.0
        }) { (_) in
            self.loadingView?.removeFromSuperview()
            self.loadingView = nil
        }
    }
    
    private func showErrorView() {
        view.addSubview(errorView)
    }
    
    private func hideErrorView() {
        errorView.removeFromSuperview()
    }
    
    private func showEmptyContactListPopup() {
        let okButton = Button(title: "Ok, vamos lá", type: .cta) { [weak self] popupController, _ in
            popupController.dismiss(animated: true, completion: {
                self?.dismiss(animated: true)
                AppParameters.global().setOnboardSocial(false)
            })
        }
        let alert = Alert(
            with: "Seus amigos não estão aqui ainda",
            text: "Descubra como é prático pagar tudo pelo seu celular através do PicPay. Depois convide seus amigos para experimentarem também :)",
            buttons: [okButton]
        )
        alert.image = Alert.Image(name: "empty_contact_list_popup")
        
        AlertMessage.showAlert(alert, controller: self)
    }
    
    private func showNextOnboardScreen() {
        coordinator?.perform(action: .skip)
        coordinator = nil // Avoid new coordinator actions (on async methods)
    }
    
    private func showInviteFriendsScreen() {
        coordinator?.perform(action: .inviteFriends)
    }
}

extension SocialOnboardingViewController: SocialOnboardingTableViewProtocol{
    func socialOnboardingDidChangeSelection(_ controller: SocialOnboardingTableViewController) {
        updateFollowButton()
    }
}
