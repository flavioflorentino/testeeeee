//
//  LikedContactListViewModel.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 22/12/16.
//
//

import UIKit

final class LikedContactListViewModel: ContactListViewModel {
    var feedId:String
    
    init(feedId:String) {
        self.feedId = feedId
        super.init()
        
        self.title = "Curtidas"
        self.emptyText = "Nenhuma curtida"
    }
    
    /**
     * Looading the following items
     */
    override func loadItems(_ callback:@escaping (_ items:[PPFollowing]?, _ error:Error?) -> Void){
        WSSocial.getListWhoLiked(self.feedId) { (itemsList, error) in
            if let items = itemsList {
                self.items = items
            }else{
                self.items = [PPFollowing]()
            }
            
            callback(itemsList, error)
        }
    }
}
