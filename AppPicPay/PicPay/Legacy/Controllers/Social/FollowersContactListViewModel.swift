//
//  FollowersContactListViewModel.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 22/12/16.
//
//

import UIKit

final class FollowersContactListViewModel: ContactListViewModel {
    
    var profileId:String
    
    @objc init(profileId: String) {
        self.profileId = profileId
        super.init()
        
        self.title = "Seguidores"
        
        if let wsId = ConsumerManager.shared.consumer?.wsId, "\(wsId)" == self.profileId {
            self.showMoreButton = true
            self.emptyText = "Você ainda não tem seguidores."
        }else{
            self.emptyText = "Este usuário ainda não tem seguidores."
        }
    }
    
    /**
     * Looading the following items
     */
    override func loadItems(_ callback:@escaping (_ items:[PPFollowing]?, _ error:Error?) -> Void){
        WSSocial.followers(self.profileId) { (itemsList, error) in
            if let items = itemsList {
                self.items = items
            }else{
                self.items = [PPFollowing]()
            }
            
            callback(itemsList, error)
        }
    }
}
