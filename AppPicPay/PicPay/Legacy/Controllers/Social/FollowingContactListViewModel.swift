//
//  FollowingContactListViewModel.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 22/12/16.
//
//

import UIKit

final class FollowingContactListViewModel: ContactListViewModel {
    var profileId:String
    
    @objc init(profileId:String) {
        self.profileId = profileId
        super.init()
        
        self.title = "Seguindo"
        if let wsId = ConsumerManager.shared.consumer?.wsId, "\(wsId)" == self.profileId {
            self.emptyText = "Você ainda não segue ninguém."
        }else{
            self.emptyText = "Este usuário ainda não segue ninguém."
        }
    }
    
    /**
     * Looading the following items
     */
    override func loadItems(_ callback:@escaping (_ items:[PPFollowing]?, _ error:Error?) -> Void){
        DispatchQueue.global(qos: .userInitiated).async {
            WSSocial.following(self.profileId) { [weak self](itemsList, error) in
                if let items = itemsList {
                    self?.items = items
                }else{
                    self?.items = [PPFollowing]()
                }
                
                callback(itemsList, error)
            }
        }
    }
}
