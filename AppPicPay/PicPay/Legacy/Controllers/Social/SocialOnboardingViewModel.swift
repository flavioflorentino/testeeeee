import AnalyticsModule
import Core
import CoreLegacy
import FeatureFlag
import UIKit

final class SocialOnboardingViewModel: NSObject {
    typealias Dependencies = HasConsumerManager & HasAnalytics & HasFeatureManager
    private let dependencies: Dependencies
    fileprivate var tableList: TableList<SocialOnboardItem>
    private(set) var numberContactToFollow: Int = 0
    private(set) var followAllCheck: Bool = false
    private let service: FindFriendsServiceProtocol
    
    var didSkipFindFriends: Bool
    var loadContactBookComplete: ((_ error:Error?) -> Void)?
    var contactListCount = 0
    
    enum Section: Int {
        case follow = 0
    }
    
    init(service: FindFriendsServiceProtocol, didSkip: Bool, dependencies: DependencyContainer) {
        self.service = service
        self.didSkipFindFriends = didSkip
        tableList = TableList()
        let section = TableListSectionData<SocialOnboardItem>()
        section.title = "Seguir todos"
        tableList.sections.append(section)
        self.dependencies = dependencies
        
        dependencies.analytics.log(OnboardingEvent.friends(.userNavigated(!didSkip)))
    }
    
    // MARK: - Actions
    
    func loadUserContacts(completion: @escaping () -> Void) {
        Contacts().identifyPicpayContacts { (_) in
            completion()
        }
    }
    
    func loadContactBook(_ completion: @escaping ((_ error: Error?) -> Void) ){
        loadContactBookComplete = completion
        loadUserContacts { [weak self] in
            self?.loadFollowSuggestion { [weak self] (error) in
                self?.loadContactBookComplete?(error)
                self?.loadContactBookComplete = nil
            }
        }
    }
    
    private func loadLocalSuggestions(_ completion: @escaping ( (_ error:Error?)-> Void)) {
        service.getFriendsSuggestions { [weak self] list, error in
            if let list = list {
                let items = list.map(SocialOnboardItem.init(contact:))
                self?.tableList.sections[Section.follow.rawValue].rows = items.map(TableListRowData.init(data:))
                self?.contactListCount = items.count
            }
            
            if error != nil {
                self?.contactListCount = 0
            }
            
            completion(error)
        }
    }
    
    private func loadRecommendedSuggestions(_ completion: @escaping ( (_ error:Error?)-> Void)) {
        guard let phone = dependencies.consumerManager.consumer?.verifiedPhoneNumber else {
            self.contactListCount = 0
            completion(nil)
            return
        }
        
        loadFriendsRecommendations(forPhoneNumber: phone) { [weak self] list in
            let contacts = self?.generateLocalContacts(contacts: list).map(SocialOnboardItem.init(contact:)) ?? []
            self?.tableList.sections[Section.follow.rawValue].rows = contacts.map(TableListRowData.init(data:))
            self?.contactListCount = contacts.count
            
            completion(nil)
        }
    }
    
    /// load the contacts
    func loadFollowSuggestion(_ completion: @escaping ( (_ error:Error?) -> Void)) {
        if !dependencies.featureManager.isActive(.featureFriendListRecommendation) {
            loadLocalSuggestions(completion)
        } else if didSkipFindFriends {
            loadRecommendedSuggestions(completion)
        } else {
            loadLocalSuggestions(completion)
        }
    }
    
    /// load friends recommendations
    private func loadFriendsRecommendations(
        forPhoneNumber number: String,
        completion: @escaping (_ recommendedContacts: [RecommendedFriendItem]) -> Void
    ) {
        service.getFriendsRecommendations(number) { result in
            switch result {
            case let .success(list):
                completion(list)
            case .failure:
                completion([])
            }
        }
    }
    
    private func generateLocalContacts(contacts: [RecommendedFriendItem]) -> [PPContact] {
        var recommendedContacts: [PPContact] = []
        contacts.forEach { contact in
            let localContact = PPContact()
            localContact.wsId = Int(contact.id) ?? 0
            localContact.onlineName = contact.name
            localContact.username = contact.username
            localContact.imgUrl = contact.imageUrlSmall
            localContact.imgUrlLarge = contact.imageUrlLarge
            recommendedContacts.append(localContact)
        }
        return recommendedContacts
    }
    
    /// select/deselect the contact to follow
    func selectContactToFollow(at indexPath:IndexPath){
        if let item = item(forRow: indexPath) {
            numberContactToFollow += item.selected ? -1 : 1
            item.selected = !item.selected
            
            followAllCheck = numberContactToFollow == tableList.sections[indexPath.section].rows.count
        }
    }
    
    // Select/Deselect all contacts to folllow
    func selectAllContactToFollow(_ select: Bool){
        for i in tableList.sections[Section.follow.rawValue].rows{
            i.data.selected = select
        }
        numberContactToFollow = select ? tableList.sections[Section.follow.rawValue].rows.count : 0
        followAllCheck = select
    }
    
    /// Follow all selected contacts
    func follow(_ completion: @escaping ( (_ error:Error?)-> Void) ){
        let proposedList = tableList.sections[Section.follow.rawValue].rows.map(\.data.contact.wsId)
        let selectedItems = tableList.sections[Section.follow.rawValue].rows.filter(\.data.selected)
        let choosenList = selectedItems.map(\.data.contact.wsId)
        let consumers = choosenList.map(\.description)
        
        dependencies.analytics.log(OnboardingEvent.friends(
            .recommendationAccepted(
                !didSkipFindFriends,
                proposedList.count,
                choosenList.count)
            )
        )
        
        WSSocial.follow(consumers: consumers) { (success, error) in
            DispatchQueue.main.async {
                completion(error)
            }
        }
    }
    
    func cancelSocialOnboarding(isCancelButton: Bool = false){
        AppParameters.global().setOnboardSocial(false)
        if isCancelButton {
            dependencies.analytics.log(OnboardingEvent.friends(.recommendationDropped(!didSkipFindFriends)))
        }
    }
    
    // MARK: - Table Data
    
    var numberOfSection = 1
    func numberOfRows(at section:Int) -> Int {
        return tableList.sections[section].rows.count
    }
    
    func contact(forRow indexPath:IndexPath) -> PPContact?{
        if let item = tableList[indexPath] {
            return item.contact
        }
        
        return nil
    }
    
    fileprivate func item(forRow indexPath:IndexPath) -> SocialOnboardItem?{
        if let item = tableList[indexPath] {
            return item
        }
        
        return nil
    }
    
    func isSelected(forRow indexPath: IndexPath) -> Bool {
        if let item = tableList[indexPath] {
            return item.selected
        }
        
        return false
    }
}

fileprivate final class SocialOnboardItem {
    var contact: PPContact
    var selected: Bool = false
    
    init(contact: PPContact){
        self.contact = contact
    }
}

