import UI
import UIKit

class ContactListTableViewController: PPBaseTableViewController, FollowingTableViewCellDelegate {
    
    // MARK: - Properties
    @objc var dismissBlock: (() -> Void)?
    @objc var wasPresentedFromPopup = false
    @objc var model:ContactListViewModel?
    var profileViewController:ProfileViewController?
    var loadingView:UIView?
    var isControllerVisible:Bool = false
    
    // MARK: - Iniatializers
    
    @objc init(model: ContactListViewModel){
        super.init(style: .plain)
        self.model = model
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
    }
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        
        loadingView = Loader.getLoadingView("")
        loadingView!.isHidden = true
        self.view.addSubview(loadingView!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        configureNagivationItem()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isControllerVisible = true
        loadItems()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isControllerVisible = false
    }
    
    // MARK: - Internal Methods
    
    func configureNagivationItem(){
        self.navigationItem.title = self.model != nil ? self.model?.title : ""
        if wasPresentedFromPopup {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Fechar", style: .plain, target: self, action: #selector(close))
        }
    }
    
    /**
     * Configure the following table view
     */
    func configureTableView(){
        let nibLoading = UINib(nibName: "FollowingTableViewCell", bundle: Bundle.main)
        tableView.register(nibLoading, forCellReuseIdentifier: "FollowingCell")
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        self.tableView.tableFooterView = UIView(frame:CGRect(x: 0,y: 0,width: 0,height: 0))
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 58.0;
        self.tableView.separatorStyle = .none
    }
    
    /**
     * Loading the table view items
     */
    func loadItems(_ showLoading:Bool = true){
        if showLoading {
            startLoading()
        }
        self.model?.loadItems({ [weak self] (items, error) in
            DispatchQueue.main.async {
                guard let strongSelf = self else {
            return
        }
                if (error != nil && strongSelf.isControllerVisible ) {
                    AlertMessage.showAlert(withMessage: error?.localizedDescription, controller: strongSelf)
                }
                strongSelf.tableView.reloadData()
                strongSelf.refreshControl?.endRefreshing()
                strongSelf.stopLoading()
                strongSelf.checkIfEmpty()
            }
        })
    }
    
    /**
     Show the loading
     */
    func startLoading(){
        DispatchQueue.main.async {
            // create a simple view with an animating indicator
            let view = UIView(frame:CGRect(x: 0,y: 0,width: self.view.frame.size.width,height: 60))
            view.backgroundColor = Palette.ppColorGrayscale000.color
            
            let indicatorView = UIActivityIndicatorView(frame: CGRect(x: self.view.frame.size.width/2 - 10, y: 20, width: 20, height: 20))
            indicatorView.color = UIColor.lightGray
            view.addSubview(indicatorView)
            
            self.tableView.tableFooterView = view
            indicatorView.startAnimating()
        }
    }
    
    /// Hide the loading
    func stopLoading(){
        DispatchQueue.main.async {
            self.tableView.tableFooterView = nil
        }
    }
    
    /// Hide the loading
    func checkIfEmpty(){
        DispatchQueue.main.async {
            if self.model?.items.count > 0 {
                self.tableView.tableHeaderView = nil
            }else{
                // create a simple view with an animating indicator
                let view = UIView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
                view.backgroundColor = Palette.ppColorGrayscale000.color
                
                let label = UILabel(frame:CGRect(x: 20, y: 0, width: self.view.frame.size.width - 40.0, height: self.view.frame.size.height))
                label.numberOfLines = 0
                label.textAlignment = .center
                label.text = self.model != nil ? self.model!.emptyText : ""
                label.textColor = UIColor.lightGray
                label.font = UIFont.systemFont(ofSize: 16)
                view.addSubview(label)
                
                self.tableView.tableHeaderView = view
            }
        }
    }
    
    @objc func handleRefresh(_ refreshControl:UIRefreshControl){
        self.refreshControl?.beginRefreshing()
        self.loadItems(false)
    }
    
    /**
     * Method called when user tap in follow button
     */
    @objc func followButtonAction(_ sender: UIPPFollowButton) {
    
        let action:FollowerButtonAction = sender.action;
        if let following = self.model?.items[sender.tag] {
    
            var text = "";
            if action == FollowerButtonAction.unfollow {
                text = "Deseja deixar de seguir @\(following.contact.username ?? "")"
            }
            else if action == FollowerButtonAction.cancelRequest {
                text = "Deseja cancelar o pedido para seguir @\(following.contact.username ?? "")"
            }
            else {
                self.executeFollowAction(sender, following: following)
            }
            
            if action == FollowerButtonAction.unfollow || action == FollowerButtonAction.cancelRequest {
                let alert = UIAlertController(title: text, message: nil, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Sim", style: .default, handler: { [weak self](action: UIAlertAction) -> Void in
                    self?.executeFollowAction(sender, following: following)
                })
                alert.addAction(okAction)
                let cancelAction = UIAlertAction(title: "Não", style: .cancel, handler: {(action: UIAlertAction) -> Void in
                    
                })
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: {() -> Void in
                    
                })
            }
        }
    }

    /**
     * Execute the follow button action
     */
    func executeFollowAction(_ sender: UIPPFollowButton, following:PPFollowing) {
        let action = sender.action
        let currentStatus = following.status
        
        // Change the notification status to give feedback to the user
        sender.changeButtonWithStatus(FollowerStatus.loading, consumerStatus: FollowerStatus.loading)
        following.status = FollowerStatus.loading
        sender.setNeedsLayout()
        
        // call server
        WSSocial.followAction(action, follower: "\(following.contact.wsId)",completion: { [weak self] (success, consumerStatus, followerStatus, error) in
            DispatchQueue.main.async(execute: {() -> Void in
                if success {
                    //update button with new status
                    sender.changeButtonWithStatus(followerStatus, consumerStatus: consumerStatus)
                    following.status = consumerStatus
                }
                else {
                    // rolback the button follow status
                    sender.changeButtonWithStatus(FollowerStatus.undefined, consumerStatus: currentStatus)
                    following.status = currentStatus
                    
                    // show error message
                    if let err = error {
                        guard let strongSelf = self else {
            return
        }
                        AlertMessage.showAlert(withMessage: err.localizedDescription, controller: strongSelf)
                    }
                }
            })
        })
    }
    
    // MARK: - User Actions
    
    @objc func close(){
        self.dismiss(animated: true) { [weak self] in
            self?.dismissBlock?()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - FollowingTableViewCellDelegate
    func didTapMoreButton(_ indexPath:IndexPath){
        
    }
    

    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.model != nil {
            return self.model!.items.count
        }
        return 0;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FollowingCell") as? FollowingTableViewCell
        
        if let followingData = self.model?.items[indexPath.row] {
            let showMoreButton = self.model != nil ? self.model!.showMoreButton : false
            cell?.configureCell(followingData, indexPath: indexPath, delegate:self, showMoreButton:showMoreButton)
            cell?.profileImage.delegate = self
            cell?.followButton.tag = indexPath.row
            cell?.followButton.addTarget(self, action: #selector(followButtonAction), for: .touchUpInside)
        }
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let following = self.model?.items[indexPath.row] else {
            return
        }
        self.profileViewController = NewProfileProxy.openProfile(contact: following.contact, parent: self)
    }
}

extension ContactListTableViewController: UIPPProfileImageDelegate{
    func profileImageViewDidTap(_ profileImage:UIPPProfileImage, contact:PPContact){
        self.profileViewController = NewProfileProxy.openProfile(contact: contact, parent: self)
    }
}

