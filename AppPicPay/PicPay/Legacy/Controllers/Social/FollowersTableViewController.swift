//
//  FollowersTableViewController.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 23/12/16.
//
//

import UIKit

final class FollowersTableViewController: ContactListTableViewController {
    
    // MARK: - Internal Methods
    func executeRemoveFollower(_ following:PPFollowing, indexPath:IndexPath){
        // remove the row from table
        tableView.beginUpdates()
        model?.removeItem(indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .fade)
        tableView.endUpdates()
        
        //cal service
        WSSocial.removeFollower("\(following.contact.wsId)", completion: { [weak self] (success, error) in
            DispatchQueue.main.async(execute: {
                self?.loadingView?.isHidden = true
                self?.tableView.isScrollEnabled = true
                
                if let error = error {
                    self?.showRemoveFollowerError(error: error)
                }
            })
        })
    }
    
    func showRemoveFollowerError(error:Error) {
        let alert = UIAlertController(title: "Ops!", message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { [weak self] (action) in
            self?.model?.removeAllItems()
            self?.tableView.reloadData()
            self?.loadItems()
        }))
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - User Actions
    
    /**
     * Show the avalible options
     */
    func openMoreOption(_ following:PPFollowing, indexPath:IndexPath){
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let removeFollowerAction = UIAlertAction(title: "Remover seguidor", style: .default) { [weak self] (action) in
            self?.removeFollower(following, indexPath: indexPath)
        }
        actionSheet.addAction(removeFollowerAction)
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel) { (_) in
            
        }
        actionSheet.addAction(cancelAction)
        self.present(actionSheet, animated: true) {
            
        }
    }
    
    /**
     * Show the alert to ask the user if he realy want to remove the follower
     */
    func removeFollower(_ following:PPFollowing, indexPath:IndexPath){
        guard let contactName = following.contact.username else {
            return
        }
        let message = "O PicPay não informará @\(contactName) que ele foi removido dos seus seguidores."
        let alert = UIAlertController(title: "Remover seguidor?", message: message, preferredStyle: .alert)
        let removeFollowerAction = UIAlertAction(title: "Remover", style: .default) { [weak self] (action) in
            self?.executeRemoveFollower(following, indexPath: indexPath)
        }
        alert.addAction(removeFollowerAction)
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel) { (_) in
            
        }
        alert.addAction(cancelAction)
        self.present(alert, animated: true) {
            
        }

    }
    
    // MARK: - FollowingTableViewCellDelegate
    
    override func didTapMoreButton(_ indexPath:IndexPath){
        guard let model = model, model.items.count > indexPath.row else {
            return
        }
        
        if let followingData = self.model?.items[indexPath.row] {
            self.openMoreOption(followingData, indexPath:indexPath)
        }
    }
}
