//
//  PPPaymentCalculator.h
//  PicPay
//
//  Created by Diogo Carneiro on 24/10/14.
//
//

#import <Foundation/Foundation.h>
@class PPPaymentConfig;

@interface PPPaymentCalculator : NSObject{
	PPPaymentConfig *paymentConfig;
}

- (id)init;
- (void)changePaymentConfig:(PPPaymentConfig *)newConfig;
- (NSDecimalNumber *)installmentValueForInstallmentsQuantity:(NSInteger)installmentQuantity totalValue:(NSDecimalNumber *)totalValue;
- (NSDecimalNumber *)recalculatedValueForInstallmentsQuantity:(NSInteger)installmentQuantity totalValue:(NSDecimalNumber *)totalValue;
- (NSArray *)installmentsListForTotalValue:(NSDecimalNumber *)totalValue;

@end
