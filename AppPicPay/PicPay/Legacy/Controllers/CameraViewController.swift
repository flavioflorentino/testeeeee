import AVFoundation
import UIKit
import Core

final class CameraController: NSObject {
    var captureSession: AVCaptureSession
    var currentCameraPosition: CameraPosition?
    
    var frontCamera: AVCaptureDevice?
    var frontCameraInput: AVCaptureDeviceInput?
    
    var photoOutput: AVCaptureOutput?
    var rearCamera: AVCaptureDevice?
    var rearCameraInput: AVCaptureDeviceInput?
    
    var previewLayer: AVCaptureVideoPreviewLayer
    weak var previewView: UIView?
    
    var photoCaptureCompletionBlock: ((UIImage?, Error?) -> Void)?
    
    var currentCameraDevice: AVCaptureDevice? {
        return currentCameraPosition == .front ? frontCamera : rearCamera
    }
    
    override init() {
        captureSession = AVCaptureSession()
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        previewLayer.connection?.videoOrientation = .portrait
        super.init()
    }
    
    func displayPreview(on view: UIView) throws {
        guard captureSession.isRunning else {
            throw CameraControllerError.captureSessionIsMissing
        }
        
        view.layer.insertSublayer(previewLayer, at: 0)
        previewView = view
        previewLayer.frame = view.frame
        
        let captureTapGesture = UITapGestureRecognizer(target: self, action: #selector(autoFocusGesture(RecognizeGesture:)))
        captureTapGesture.numberOfTapsRequired = 1
        captureTapGesture.numberOfTouchesRequired = 1
        view.addGestureRecognizer(captureTapGesture)
    }
    
    func switchToFrontCamera() throws {
        guard let rearCameraInput = rearCameraInput,
            captureSession.inputs.contains(rearCameraInput),
            let frontCamera = frontCamera else {
                throw CameraControllerError.invalidOperation
        }
        
        let newFrontCameraInput = try AVCaptureDeviceInput(device: frontCamera)
        frontCameraInput = newFrontCameraInput
        captureSession.removeInput(rearCameraInput)
        try addNewCaptureInput(newFrontCameraInput)
        currentCameraPosition = .front
    }
    
    func switchToRearCamera() throws {
        guard let frontCameraInput = frontCameraInput,
            captureSession.inputs.contains(frontCameraInput),
            let rearCamera = rearCamera else {
                throw CameraControllerError.invalidOperation
        }
        
        let newRearCameraInput = try AVCaptureDeviceInput(device: rearCamera)
        rearCameraInput = newRearCameraInput
        captureSession.removeInput(frontCameraInput)
        try addNewCaptureInput(newRearCameraInput)
        currentCameraPosition = .rear
    }
    
    private func addNewCaptureInput(_ input: AVCaptureInput) throws {
        if captureSession.canAddInput(input) {
            captureSession.addInput(input)
        } else {
            throw CameraControllerError.inputsAreInvalid
        }
    }
    
    func captureImage(completion: @escaping (UIImage?, Error?) -> Void) {
        photoCaptureCompletionBlock = completion
        guard captureSession.isRunning else {
            completion(nil, CameraControllerError.captureSessionIsMissing)
            return
        }
        let newCapturePhotoSettings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecJPEG])
        if let capturePhotoOutput = self.photoOutput as? AVCapturePhotoOutput {
            capturePhotoOutput.capturePhoto(with: newCapturePhotoSettings, delegate: self)
        }
    }
    
    @objc
    func autoFocusGesture(RecognizeGesture: UITapGestureRecognizer) {
        guard let preview = previewView else {
            return
        }
        let convertedPoint = previewLayer.captureDevicePointConverted(fromLayerPoint: RecognizeGesture.location(in: preview))
        //Assign Auto Focus
        if let device = currentCameraDevice {
            do {
                try device.lockForConfiguration()
                if device.isFocusPointOfInterestSupported {
                    //Add Focus on Point
                    device.focusPointOfInterest = convertedPoint
                    device.focusMode = .autoFocus
                }
                device.unlockForConfiguration()
            } catch {
                debugPrint(error)
            }
        }
    }
}

extension CameraController: AVCapturePhotoCaptureDelegate {
    // swiftlint:disable:next function_parameter_count
    public func photoOutput(
            _ captureOutput: AVCapturePhotoOutput,
            didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?,
            previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?,
            resolvedSettings: AVCaptureResolvedPhotoSettings,
            bracketSettings: AVCaptureBracketedStillImageSettings?,
            error: Swift.Error?
        ) {
        if let error = error {
            photoCaptureCompletionBlock?(nil, error)
        } else if let buffer = photoSampleBuffer,
            let data = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: buffer, previewPhotoSampleBuffer: nil),
            let image = UIImage(data: data) {
                photoCaptureCompletionBlock?(image, nil)
        } else {
            self.photoCaptureCompletionBlock?(nil, CameraControllerError.unknown)
        }
    }
}

extension CameraController {
    func prepare(completionHandler: @escaping (Error?) -> Void) {
        DispatchQueue(label: "prepare").async { [weak self] in
            do {
                try self?.configureCaptureDevices()
                try self?.configureDeviceInputs()
                try self?.configurePhotoOutput()
            } catch {
                DispatchQueue.main.async {
                    completionHandler(error)
                }
                return
            }
            
            DispatchQueue.main.async {
                completionHandler(nil)
            }
        }
    }
    
    private func configureCaptureDevices() throws {
        frontCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front)
        rearCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)
        
        try rearCamera?.lockForConfiguration()
        rearCamera?.focusMode = .continuousAutoFocus
        rearCamera?.exposureMode = .continuousAutoExposure
        rearCamera?.unlockForConfiguration()
    }
    
    private func configureDeviceInputs() throws {
        if let rearCam = rearCamera {
            let newRearCameraInput = try AVCaptureDeviceInput(device: rearCam)
            rearCameraInput = newRearCameraInput
            try addNewCaptureInput(newRearCameraInput)
            currentCameraPosition = .rear
        } else if let frontCam = frontCamera {
            let newFrontCameraInput = try AVCaptureDeviceInput(device: frontCam)
            frontCameraInput = newFrontCameraInput
            try addNewCaptureInput(newFrontCameraInput)
            currentCameraPosition = .front
        } else {
            throw CameraControllerError.noCamerasAvailable
        }
    }
    
    private func configurePhotoOutput() throws {
        let newPhotoOutput = AVCapturePhotoOutput()
        photoOutput = newPhotoOutput
        
        if captureSession.canAddOutput(newPhotoOutput) {
            captureSession.addOutput(newPhotoOutput)
        }
        captureSession.startRunning()
    }
}

extension CameraController {
    enum CameraControllerError: Swift.Error {
        case captureSessionAlreadyRunning
        case captureSessionIsMissing
        case inputsAreInvalid
        case invalidOperation
        case noCamerasAvailable
        case unknown
    }
    
    public enum CameraPosition {
        case front
        case rear
    }
}
