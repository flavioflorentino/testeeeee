enum ShareCodeLocalizable: String, Localizable {
    case copiedCode
    case termsAndConditions
    case shareWithFriends
    case regulation
    case inviteBusinesses
    case copiedLinkMessage
    
    case emailSubject
    case emailMessage
    
    var key: String {
        return self.rawValue
    }
    var file: LocalizableFile {
        return .shareCode
    }
}
