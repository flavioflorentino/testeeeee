import Foundation
import UI

final class ShareButton: UIView {
    let icon = UIImageView()
    let textLabel = UILabel()
    var type: ShareOptions = .facebook
    var action: ((ShareButton) -> ())?
    
    func setup(type: ShareOptions, action: ((ShareButton) -> ())?) {
        self.type = type
        self.action = action
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTap))
        addGestureRecognizer(tap)
        
        backgroundColor = .clear
        icon.image = type.image()
        textLabel.text = type.shareText()
        textLabel.font = UIFont.systemFont(ofSize: 14)
        textLabel.textColor = Palette.ppColorGrayscale500.color
        
        setupViews()
    }
    
    func setupViews() {
        icon.translatesAutoresizingMaskIntoConstraints = false
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        
        backgroundColor = Palette.ppColorGrayscale100.color(withCustomDark: .ppColorGrayscale500)
        layer.cornerRadius = 20
        
        addSubview(icon)
        addSubview(textLabel)
        
        // Icon
        icon.addConstraint(NSLayoutConstraint(item: icon, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40))
        icon.addConstraint(NSLayoutConstraint(item: icon, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40))
        addConstraint(NSLayoutConstraint(item: icon, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 3))
        addConstraint(NSLayoutConstraint(item: icon, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
        // Label
        addConstraint(NSLayoutConstraint(item: textLabel, attribute: .leading, relatedBy: .equal, toItem: icon, attribute: .trailing, multiplier: 1, constant: 10))
        addConstraint(NSLayoutConstraint(item: textLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    @objc func didTap() {
        action?(self)
    }
    
}
