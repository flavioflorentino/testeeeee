import AnalyticsModule
import FeatureFlag
import MessageUI
import PermissionsKit
import Social
import CoreLegacy
import UI
import UIKit

final class SocialShareCodeViewController: PPBaseViewController {
    private var emptySearchView: EmptyPeopleSearchView?
    private let model = PeopleSearchViewModel()
    private var code: String = ""
    
    private var shareText: String = ""
    private var shareTextShort: String = ""
    private var shareTextFacebook: String = ""
    private var shareLink: String = ""
    
    enum MGMType: String {
        case user = "user"
        case business = "business"
    }
    
    var mgmType: MGMType = .user
    
    // hacker Objective-c
    @objc static var MGMTypeUser = "user"
    @objc static var MGMTypeBusiness = "business"
    @objc var mgmTypeString: String?
    
    @IBOutlet weak var mgmText: UILabel!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var terms: UIButton!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var readMoreHeight: NSLayoutConstraint!
    @IBOutlet weak var readMoreButton: UIButton!
    @IBOutlet weak var termsTop: NSLayoutConstraint?
    @IBOutlet weak var shareSeparatorLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var myCodeContainerView: UIView!
    @IBOutlet weak var myCodeTitle: UILabel!
    @IBOutlet weak var myCodeLabel: UILabel!
    
    private lazy var codeCopiedLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        label.textColor = Palette.ppColorBranding300.color
        label.textAlignment = .center
        label.text = ShareCodeLocalizable.copiedCode.text
        label.backgroundColor = Palette.ppColorGrayscale000.color
        label.alpha = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let copyCodeTapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(copyCode));
        myCodeContainerView.addGestureRecognizer(copyCodeTapGesture)
        let refreshButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(self.nativeShare))
        self.navigationItem.rightBarButtonItem = refreshButton
        self.title = AppParameters.global().mgmConfigs?.mgmScreenTitle
        
        loadData()
        setupEmptySearchView()
        setupCopiedCodeLabel()

        view.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    // MARK: - Load data
    
    private func loadData() {
        let loadingView = UILoadView(superview: self.view, position: UILoadView.Position.center, activityStyle: .gray)
        loadingView.startLoading()
        model.consumerMgm { [weak self] error in
            guard let strongSelf = self else {
                return
            }
            loadingView.animatedRemoveFromSuperView()
            if let error = error {
                AlertMessage.showAlert(error, controller: strongSelf, completion: {
                    strongSelf.navigationController?.popViewController(animated: true)
                })
            } else {
                if let typeString = strongSelf.mgmTypeString, let type = MGMType(rawValue: typeString) {
                    strongSelf.mgmType = type
                }
                strongSelf.setupView(type: strongSelf.mgmType)
            }
        }
    }
    
    // MARK: - Config Methods
    
    private func setupView(type: MGMType) {
        if FeatureManager.isActive(.mGMCode) || type != .user {
            let buttonTitleStr = NSMutableAttributedString(string:"Regulamento", attributes:[
                .underlineStyle : 1,
                .foregroundColor : Palette.ppColorGrayscale500.color
                ])
            terms.setAttributedTitle(buttonTitleStr, for: .normal)
            iconImageView.contentMode = .scaleAspectFit
            iconImageView.image = #imageLiteral(resourceName: "ico_mgm_invite")
            
            let borderView = CAShapeLayer()
            borderView.strokeColor = Palette.ppColorGrayscale600.cgColor
            borderView.lineDashPattern = [6, 4]
            borderView.frame = myCodeContainerView.bounds
            borderView.fillColor = nil
            borderView.strokeColor = #colorLiteral(red: 0.7333333333, green: 0.7333333333, blue: 0.7333333333, alpha: 1).cgColor
            borderView.path = UIBezierPath(roundedRect: myCodeContainerView.bounds, cornerRadius: myCodeContainerView.bounds.height / 2).cgPath
            myCodeContainerView.layer.addSublayer(borderView)
        } else {
            terms.removeFromSuperview()
            readMoreButton.removeFromSuperview()
            myCodeContainerView.removeFromSuperview()
            
            mgmText.bottomAnchor.constraint(equalTo: separatorView.topAnchor, constant: -20.0).isActive = true
            mgmText.sizeToFit()
            
            shareSeparatorLabel.text = ShareCodeLocalizable.shareWithFriends.text
            iconImageView.image = #imageLiteral(resourceName: "ilu_invite_friends_large")
            iconImageView.contentMode = .scaleAspectFit
            
            view.setNeedsLayout()
            view.layoutIfNeeded()
        }
        
        if let code = ConsumerManager.shared.consumer?.share_hash {
            configureCode(code)
        }
        
        if let mgmConfigs = self.model.mgm {
            if type == .user {
                readMoreButton.isHidden = true
                readMoreHeight.constant = 0
                termsTop?.constant = -5.0
                
                title = mgmConfigs.mgmScreenTitle
                titleLabel.text = mgmConfigs.mgmScreenSubtitle2
                shareTextShort = mgmConfigs.shareTextShort ?? ""
                shareTextFacebook = mgmConfigs.shareTextFacebook ?? ""
                shareLink = mgmConfigs.shareLink ?? ""
                shareText = mgmConfigs.shareText ?? ""
                mgmText.text = mgmConfigs.text
            } else {
                title = ShareCodeLocalizable.inviteBusinesses.text
                titleLabel.text = mgmConfigs.mgbScreenShortTextShare ?? ""
                iconImageView.image = #imageLiteral(resourceName: "ico_mgb_invite48")
                shareTextShort = mgmConfigs.mgbShareTextShort ?? ""
                shareTextFacebook = mgmConfigs.mgbShareTextFacebook  ?? ""
                shareLink = mgmConfigs.mgbShareLink  ?? ""
                shareText = mgmConfigs.mgbShareText  ?? ""
                mgmText.text = mgmConfigs.mgbScreenTextShare  ?? ""
            }
        }
        
        let textColor = Palette.ppColorGrayscale500.color
        titleLabel.textColor = textColor
        mgmText.textColor = textColor
        myCodeTitle.textColor = textColor
        shareSeparatorLabel.textColor = textColor
    }
    
    private func configureCode(_ code: String){
        self.code = code
        myCodeLabel.text = "\(code)"
    }
    
    private func setupEmptySearchView() {
        guard let emptySearchView = ViewsManager.loadView(withXibName: "EmptyPeopleSearchView") as? EmptyPeopleSearchView else {
            return
        }
        emptySearchView.translatesAutoresizingMaskIntoConstraints = false
        self.emptySearchView = emptySearchView
        
        shareView.addSubview(emptySearchView)
        NSLayoutConstraint.constraintAllEdges(from: emptySearchView, to: shareView)
        

        emptySearchView.backgroundColor = .clear
        
        emptySearchView.setupForShareScreen(shareAction: { [weak self] button in
            self?.share(button: button)
        })
    }
    
    private func setupCopiedCodeLabel() {
        myCodeContainerView.addSubview(codeCopiedLabel)
        NSLayoutConstraint.constraintAllEdges(from: codeCopiedLabel, to: myCodeContainerView)
    }
    
    func setTypeString(type: String){
        if let t = MGMType(rawValue: type) {
            mgmType = t
        }
    }
    
    // MARK: - User Actions
    
    @objc
    private func nativeShare() {
        let shareMessage = shareText
        let textToShare = [ shareMessage ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true) {
            Analytics.shared.log(ShareEvent.invite(type: .system))
        }
    }
    
    func share(button: ShareButton? = nil, cell: ShareCarouselCollectionViewCell? = nil) {
        var type: ShareOptions = .facebook
        
        if let newType = button?.type {
            type = newType
        }
        if let newType = cell?.type {
            type = newType
        }
        
        switch type {
        case .facebook:
            showSystemShareSheet(type: type)
            self.model.trackMGMEvent(name: PPLogMgmTouchOriginFacebook)
            Analytics.shared.log(ShareEvent.invite(type: .facebook))
        case .twitter:
            showSystemShareSheet(type: type)
            self.model.trackMGMEvent(name: PPLogMgmTouchOriginTwitter)
            Analytics.shared.log(ShareEvent.invite(type: .twitter))
        case .whatsapp:
            let text = shareText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            if let url = URL(string: "whatsapp://send?text=\(text)"),
                UIApplication.shared.canOpenURL(url) {
                self.model.trackMGMEvent(name: PPLogMgmTouchOriginWhatsapp)
                UIApplication.shared.open(url)
            }
            Analytics.shared.log(ShareEvent.invite(type: .whatsapp))
        case .sms:
            break
        case .email:
            let permissionContact: Permission = PPPermission.contacts()
            if (permissionContact.status != .authorized) {
                PPPermission.requestContacts { [weak self] (status) in
                    if status == .authorized {
                        self?.emailShare()
                    }
                }
            } else {
                emailShare()
            }
            Analytics.shared.log(ShareEvent.invite(type: .email))
        case .link:
            UIPasteboard.general.string = shareLink
            
            self.model.trackMGMEvent(name: PPLogMgmTouchOriginCopyLink)
            
            var label:UILabel?
            
            if let buttonLabel = button?.textLabel {
                label = buttonLabel
            }
            if let cellLabel = cell?.label {
                label = cellLabel
            }
            
            let oldText = label?.text
            label?.text = ShareCodeLocalizable.copiedLinkMessage.text
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                label?.text = oldText
            }
        }
    }
    
    private func showSystemShareSheet(type: ShareOptions) {
        let service = type == .twitter ? SLServiceTypeTwitter : SLServiceTypeFacebook
        let text = type == .twitter ? shareTextShort : shareTextFacebook
        
        let sheet: SLComposeViewController = SLComposeViewController(forServiceType: service)
        if let url = URL(string: shareLink), type == .facebook {
            sheet.add(url)
        }
        sheet.setInitialText(text)
        present(sheet, animated: true)
    }
    
    private func emailShare() {
        guard MFMailComposeViewController.canSendMail() else {
            return
        }
        
        let mailController = MFMailComposeViewController()
        mailController.mailComposeDelegate = self
        mailController.setSubject(ShareCodeLocalizable.emailSubject.text)
        let message = String(format: ShareCodeLocalizable.emailMessage.text, code, code, code)
        mailController.setMessageBody(message, isHTML: true)
        present(mailController, animated: true)
    }
    
    @objc
    private func copyCode() {
        UIPasteboard.general.string = code
        showDidCopyCodeMessage()
    }
    
    private func showDidCopyCodeMessage() {
        UIView.animate(withDuration: 0.2, animations: {
            self.codeCopiedLabel.alpha = 1
        }, completion: { _ in
            UIView.animate(withDuration: 0.2, delay: 2.0, animations: {
                self.codeCopiedLabel.alpha = 0
            })
        })
    }
    
    @IBAction private func termsAction(_ sender: Any) {
        var url: String = WebServiceInterface.apiEndpoint(WsWebViewURL.mgmTerms.endpoint)
        if mgmType == .business {
            navigationItem.backBarButtonItem?.title = ""
            url = WsWebViewURL.mgbTerms.endpoint
        } 
        
        ViewsManager.pushWebViewController(
            withUrl: url,
            fromViewController: self,
            title: SettingsLocalizable.termsAndConditions.text
        )
    }
    
    @IBAction private func readMore(_ sender: Any) {
        if mgmType == .business {
            ViewsManager.pushWebViewController(withUrl: WsWebViewURL.aboutMGB.endpoint, fromViewController: self)
        }
    }
}

extension SocialShareCodeViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
