//
//  PeopleSearchViewModel.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 17/05/17.
//
//

import Foundation

final class PeopleSearchViewModel: NSObject {
    
    var mgm: PPMgmConfigs?
    
    func trackMGMEvent(name: PPLogMgmTouchOrigin) {
        WSConsumer.logMgmRequest(name, completionHandler: nil)
    }
    
    func consumerMgm(onComplete: @escaping (PicPayErrorDisplayable?) -> Void) {
        WSSocial.consumerMgmWith { [weak self] (mgm, error) in
            DispatchQueue.main.async {
                if let error = error {
                    onComplete(PicPayError(message: error.localizedDescription))
                } else if let mgmConsummer = mgm {
                    self?.mgm = mgmConsummer
                    onComplete(nil)
                } else {
                    onComplete(PicPayError(message: "Ops! Ocorreu um erro, tente novamente"))
                }
            }
        }
    }
}
