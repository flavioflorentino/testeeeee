//
//  EmptyPeopleSearchView.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 22/05/17.
//
//

import Foundation
import CoreLegacy

final class EmptyPeopleSearchView: UIView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleHeight: NSLayoutConstraint!
    
    func setup(shareAction: ((ShareButton) -> ())?) {
        
        titleLabel.text = AppParameters.global().mgmConfigs?.mgmScreenTitle
        descriptionLabel.text = AppParameters.global().mgmConfigs?.mgmScreenText
        
        var previousButton: ShareButton? = nil
        for type in ShareOptions.availableOptions {
            let button = ShareButton()
            button.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(button)
            
            button.setup(type: type, action: shareAction)
            
            // height = 40
            button.addConstraint(NSLayoutConstraint(item: button, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 44))
            // Right padding = 20
            self.addConstraint(NSLayoutConstraint(item: button, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -20))
            // Left padding = 20
            self.addConstraint(NSLayoutConstraint(item: button, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 20))
            
            // If there's a previous button (ie. isn't the first)
            if let previousButton = previousButton {
                // 10 pixels down
                self.addConstraint(NSLayoutConstraint(item: button, attribute: .top, relatedBy: .equal, toItem: previousButton, attribute: .bottom, multiplier: 1, constant: 10))
            }else {
                // 15 pixels down
                self.addConstraint(NSLayoutConstraint(item: button, attribute: .top, relatedBy: .equal, toItem: descriptionLabel, attribute: .bottom, multiplier: 1, constant: 25))
            }
            
            previousButton = button
        }
    }

    func setupForShareScreen(shareAction: ((ShareButton) -> ())?) {

        titleLabel.removeFromSuperview()
        descriptionLabel.removeFromSuperview()

        var previousButton: ShareButton? = nil
        for type in ShareOptions.availableOptions {
            
            let button = ShareButton()
            button.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(button)
            
            button.setup(type: type, action: shareAction)
            
            // height = 40
            button.addConstraint(NSLayoutConstraint(item: button, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 44))
            // Right padding = 20
            self.addConstraint(NSLayoutConstraint(item: button, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -20))
            // Left padding = 20
            self.addConstraint(NSLayoutConstraint(item: button, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 20))
            
            // If there's a previous button (ie. isn't the first)
            if let previousButton = previousButton {
                // 10 pixels down
                self.addConstraint(NSLayoutConstraint(item: button, attribute: .top, relatedBy: .equal, toItem: previousButton, attribute: .bottom, multiplier: 1, constant: 10))
            }else {
                // 15 pixels down
                self.addConstraint(NSLayoutConstraint(item: button, attribute: .top, relatedBy: .equal, toItem: button.superview, attribute: .top, multiplier: 1, constant: 15))
            }
            
            previousButton = button
        }
        guard let lastButton = previousButton else {
            return
        }
        self.addConstraint(NSLayoutConstraint(item: lastButton, attribute: .bottom, relatedBy: .equal, toItem: lastButton.superview, attribute: .bottom, multiplier: 1, constant: -15))
    }
    
    func setTextForTerm(term: String) {
        if term == ""{
            descriptionLabel.text = AppParameters.global().mgmConfigs?.mgmScreenText
            titleHeight.constant = 21
            return
        }
        titleHeight.constant = 0
        
        let formattedString = NSMutableAttributedString()
        formattedString
            .normal("Não encontramos nenhum resultado para \"")
            .bold(term)
            .normal("\". Que tal mandar um convite para quem você procura? :)")

        descriptionLabel.attributedText = formattedString
    }
}

extension NSMutableAttributedString {
    @discardableResult func bold(_ text:String) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font : UIFont.boldSystemFont(ofSize: 17)]
        let boldString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(boldString)
        return self
    }
    
    @discardableResult func normal(_ text:String)->NSMutableAttributedString {
        let normal =  NSAttributedString(string: text)
        self.append(normal)
        return self
    }
}
