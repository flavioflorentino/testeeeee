//
//  RewardProgramProgressViewController.m
//  PicPay
//
//  Created by Diogo Carneiro on 20/03/13.
//
//

#import "RewardProgramProgressViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UILabel+ColorInRanges.h"

@interface RewardProgramProgressViewController ()

@end

@implementation RewardProgramProgressViewController

@synthesize rewardProgram, imageViewProgressBarBackground, labelRewardProgressMax, labelRewardProgressMin, labelRewardProgressDescription, labelRewardProgramDescription, imageViewProgressBar, viewFidelityBottomInfo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	if (rewardProgram != nil) {
		
		viewFidelityBottomInfo.hidden = NO;
		[self setSuperViewHidden:NO];
		labelRewardProgramDescription.text = rewardProgram.programDescription;
		[labelRewardProgramDescription changeTextColor:[UIColor colorWithRed:17.0/255 green:199.0/255 blue:111.0/255 alpha:1] forStringsWithPattern:@"\\R\\$([0-9]+)(,([0-9]{2}))?"];
		
		if (rewardProgram.progress > 0) {
			
			viewFidelityBottomInfo.hidden = NO;
			[self setSuperViewHidden:NO];
			UIImage *progressImage = [[UIImage imageNamed:@"progress_bar_light"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 3, 0, 3)];
			imageViewProgressBar.image = progressImage;
			
			labelRewardProgressDescription.text = rewardProgram.progressDescription;
			[labelRewardProgressDescription changeTextColor:[UIColor colorWithRed:17.0/255 green:199.0/255 blue:111.0/255 alpha:1] forStringsWithPattern:@"\\R\\$([0-9]+)(,([0-9]{2}))?"];
			labelRewardProgressMin.text = rewardProgram.progressBarMin;
			labelRewardProgressMax.text = rewardProgram.progressBarMax;
			
			labelRewardProgressDescription.numberOfLines = 3;
			CGPoint oldCenter = CGPointMake(labelRewardProgressDescription.center.x, labelRewardProgressDescription.center.y);
			CGRect oldFrame = labelRewardProgressDescription.frame;
			[labelRewardProgressDescription sizeToFit];
			labelRewardProgressDescription.center = oldCenter;
			labelRewardProgressDescription.frame = CGRectMake(labelRewardProgressDescription.frame.origin.x, oldFrame.origin.y, labelRewardProgressDescription.frame.size.width, labelRewardProgressDescription.frame.size.height);
			
		}else{
			imageViewProgressBarBackground.hidden = YES;
			imageViewProgressBar.hidden = YES;
			labelRewardProgressDescription.hidden = YES;
			labelRewardProgressMin.hidden = YES;
			labelRewardProgressMax.hidden = YES;
			labelRewardProgramDescription.hidden = NO;
			
//			CGPoint oldCenter = CGPointMake(labelRewardProgramDescription.center.x, labelRewardProgramDescription.center.y);
//			labelRewardProgramDescription.numberOfLines = 3;
//			labelRewardProgramDescription.font = [UIFont systemFontOfSize:20.0f];
//			[labelRewardProgramDescription sizeToFit];
//			labelRewardProgramDescription.center = oldCenter;
		}
	}else{
		viewFidelityBottomInfo.hidden = YES;
		[self setSuperViewHidden:YES];
	}
}

- (void)setSuperViewHidden:(BOOL)hidden{
	@try {
		[[[self view] superview] setHidden:hidden];
	}
	@catch (NSException *exception) {}
}

- (void)animateProgressBarToCurrentProgress{
	if (rewardProgram.progress > 0) {
		imageViewProgressBar.hidden = NO;
		CGRect oldBounds = [imageViewProgressBar layer].bounds;
		CGRect newBounds = oldBounds;
		CGFloat maxWith = 300;
		CGFloat newWidth = rewardProgram.progress * (maxWith / 100); //the progress bar has 300px width
		newBounds.size = CGSizeMake(newWidth, imageViewProgressBar.frame.size.height);
		CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"bounds"];
		animation.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseOut];
		animation.duration = 0.5;
		animation.fromValue = [NSValue valueWithCGRect:oldBounds];
		animation.toValue = [NSValue valueWithCGRect:newBounds];
		[imageViewProgressBar layer].bounds = newBounds;
		[imageViewProgressBar layer].anchorPoint = CGPointMake(0, 0.5);
		[[imageViewProgressBar layer] addAnimation:animation forKey:@"bounds"];
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
	[self setImageViewSeparator:nil];
	[super viewDidUnload];
}
@end
