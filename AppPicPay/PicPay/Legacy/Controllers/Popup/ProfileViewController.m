#import <UI/UI.h>
#import "ProfileViewController.h"
#import "ImageFullScreenView.h"
#import "PicPay-Swift.h"
#import "PPAnalytics.h"

@interface ProfileViewController  () <UIGestureRecognizerDelegate>
@property (strong,nonatomic) ImageFullScreenView *imageFullScreenView;
@property enum FollowerStatus consumerFollowStatus;
@property enum FollowerStatus followStatus;
@property (strong, nonatomic) UIView *overlay;
@end

@implementation ProfileViewController 

# pragma mark - Init

- (instancetype)initWithContact:(PPContact *) contact controller:(UIViewController *) controller
{
    self = [super initWithNibName:@"ProfileViewController" bundle:nil];
    if (self) {
        self.contact = contact;
        self.parentController = controller;
    }
    return self;
}

- (instancetype)initWithContact:(PPContact *) contact controller:(UIViewController *) controller originView:(UIView *)originView{
    self = [super initWithNibName:@"ProfileViewController" bundle:nil];
    if (self) {
        self.contact = contact;
        self.parentController = controller;
        self.originView = originView;
    }
    return self;
}

# pragma mark - View Cycle Life

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Hidden Follow Button for the authenticated user
    if (ConsumerManager.shared.consumer.wsId == self.contact.wsId){
        [self.followButton addConstraint:[NSLayoutConstraint constraintWithItem:self.followButton
                                                                      attribute:NSLayoutAttributeWidth
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:nil 
                                                                      attribute:NSLayoutAttributeNotAnAttribute 
                                                                     multiplier:1.0 
                                                                       constant:0]];
        self.payButton.hidden = YES;
    }
    
    // configure view
    self.view.layer.cornerRadius = 10.0;
    
    // gesture to open followins screen
    self.followingLabel.userInteractionEnabled = true;
    [self.followingLabel addGestureRecognizer:[[UITapGestureRecognizer alloc ] initWithTarget:self action:@selector(openFollowing)]];
    self.followingStaticLabel.userInteractionEnabled = true;
    [self.followingStaticLabel addGestureRecognizer:[[UITapGestureRecognizer alloc ] initWithTarget:self action:@selector(openFollowing)]];
    
    // gesture to open followers screen
    self.followerStaticLabel.userInteractionEnabled = true;
    [self.followerStaticLabel addGestureRecognizer:[[UITapGestureRecognizer alloc ] initWithTarget:self action:@selector(openFollowers)]];
    self.followersLabel.userInteractionEnabled = true;
    [self.followersLabel addGestureRecognizer:[[UITapGestureRecognizer alloc ] initWithTarget:self action:@selector(openFollowers)]];
    
    // teste
    self.overlay = [[UIView alloc] initWithFrame:CGRectMake(5, 5, self.view.frame.size.width - 10.0, self.view.frame.size.height - 10.0)];
    self.overlay.backgroundColor = [PaletteObjc ppColorGrayscale000];
    self.overlay.layer.opacity = 0.7;
    
    [self configureProfile];
    [self loadProfile];
}

# pragma mark - Internal Methods

- (void) configureProfile{
    if (self.contact !=nil ){
        // populate view
        if(self.contact.isStore){
            if (self.contact.username!=nil){
                self.usernameLabel.text = [NSString stringWithFormat:@"%@",self.contact.username];
            }else{
                self.usernameLabel.text = @" ";
            }
        }else{
            if (self.contact.username!=nil){
                self.usernameLabel.text = [NSString stringWithFormat:@"@%@",self.contact.username];
            }else{
                self.usernameLabel.text = @" ";
            }
        }
        
        if(self.contact.onlineName!=nil){
            self.nameLabel.text = self.contact.onlineName;
        }else{
            self.nameLabel.text = @" ";
        }
        
        [self.pictureImageView setImageWithUrl: [NSURL URLWithString: self.contact.imgUrl]
                                   placeholder: self.contact.photoPlaceholder
                                    completion: nil];
        
        if(self.contact.imgUrl!=nil && ![self.contact.imgUrl isEqualToString:@""] && (self.contact.imgUrlLarge == nil || [self.contact.imgUrlLarge isEqualToString:@""])){
            self.contact.imgUrlLarge = [self.contact.imgUrl copy];
        }
        
        if(self.contact.imgUrlLarge){
            self.imageFullScreenView = [[ImageFullScreenView alloc] initWithImageView:self.pictureImageView largeUrl:self.contact.imgUrlLarge];
        }
        self.verifiedImageView.hidden = !self.contact.isVerified;
        self.proImageView.hidden = !self.contact.businessAccount || self.contact.isVerified;
        
        self.followersLabel.text = [NSString stringWithFormat:@"%ld", (long)self.contact.followers];
        self.followingLabel.text = [NSString stringWithFormat:@"%ld", (long)self.contact.following];
        
        if (ConsumerManager.shared.consumer.wsId == self.contact.wsId){
            self.followButton.hidden = true;
        }
    }
}

/**!
 * Load user profile data
 */
- (void) loadProfile{
    [self startLoading];
    __weak ProfileViewController *selfWeak = self;
    [WSSocial getConsumerProfile:[NSString stringWithFormat:@"%ld", (long)self.contact.wsId] completion:^(PPContact * _Nullable profile, enum FollowerStatus consumerStatus, enum FollowerStatus followerStatus, NSError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (profile!=nil) {
                selfWeak.contact = profile;
                selfWeak.consumerFollowStatus = consumerStatus;
                selfWeak.followStatus = followerStatus;
                [selfWeak.followButton changeButtonWithStatus:followerStatus consumerStatus:consumerStatus];
                [selfWeak configureProfile];
                [selfWeak stopLoading];
            }else{
                [AlertMessage showCustomAlertWithError: error controller: selfWeak completion: nil];
            }
            
        });
    }];
}

/**!
 * Show the loading view
 */
-(void) startLoading{
    [self.loadingView startAnimating];
    [self.view insertSubview:self.overlay belowSubview:self.loadingView];
}

/**!
 * Stop and hide the loading view
 */
-(void) stopLoading{
    [self.loadingView stopAnimating];
    [self.overlay removeFromSuperview];
}

- (void) executeFollowAction:(UIPPFollowButton *) sender{
    enum FollowerButtonAction action = sender.action;
    
    // Change the notification status to give feedback to the user
    [sender changeButtonWithStatus:FollowerStatusLoading consumerStatus:FollowerStatusLoading];
    __weak ProfileViewController* weakSelf = self;
    [WSSocial followAction:action follower:[NSString stringWithFormat:@"%ld",(long)self.contact.wsId] completion:^(BOOL success, enum FollowerStatus consumerStatus, enum FollowerStatus followerStatus, NSError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if(success){
                //update button with new status
                [sender changeButtonWithStatus:followerStatus consumerStatus:consumerStatus];
            }else{
                // rolback the button follow status
                [sender changeButtonWithStatus:self.followStatus consumerStatus:self.consumerFollowStatus];
                
                // show error message
                [AlertMessage showAlertWithError:error controller:weakSelf];
            }
        });
    }];
}

# pragma mark - User Actions

/**!
 * Method called when user tap in close button
 */
- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/**!
 * Method called when user tap in pay button
 */
- (IBAction)pay:(id)sender {
    // prevent payment loop
    if ([self.parentController isKindOfClass:[NewTransactionViewController class] ]){
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        NewTransactionViewController * newTransactionViewController = (NewTransactionViewController *) [ViewsManager peerToPeerStoryboardFirtViewController];
        newTransactionViewController.preselectedContact = self.contact;
        
        NSString *screenName = [PPAnalytics screenNameFromViewController:self.parentController withVariation:@""];
        newTransactionViewController.touchOrigin = [NSString stringWithFormat:@"profile [%@]",screenName];
        [self dismissViewControllerAnimated:NO completion:^{
            [self.parentController presentViewController:[[PPNavigationController alloc ] initWithRootViewController:newTransactionViewController] animated:YES completion:nil];
        }];
    }
}

/**!
 * Method called when user tap in follow button
 */
- (IBAction)followButtonActino:(UIPPFollowButton *)sender {
    
    enum FollowerButtonAction action = sender.action;
    
    __weak ProfileViewController* selfWeak = self;
    NSString *text = @"";
    if (action == FollowerButtonActionUnfollow){
        text = [NSString stringWithFormat:@"Deseja deixar de seguir @%@",self.contact.username];
    }
    else if (action == FollowerButtonActionCancelRequest){
        text = [NSString stringWithFormat:@"Deseja cancelar o pedido para seguir @%@",self.contact.username];
    }
    else{
        [self executeFollowAction:sender];
    }
    
    if (action == FollowerButtonActionUnfollow || action == FollowerButtonActionCancelRequest){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:text message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Sim" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [selfWeak executeFollowAction:sender];
        }];
        [alert addAction:okAction];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Não" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:^{}];
    }
}

-(void)openFollowing{
    if (self.contact.isOpenFollowersAndFollowings || ConsumerManager.shared.consumer.wsId == self.contact.wsId){
        FollowingContactListViewModel *model =  [[FollowingContactListViewModel alloc] initWithProfileId:[NSString stringWithFormat:@"%ld", (long) self.contact.wsId]];
        
        UINavigationController *navigation = (UINavigationController *) [ViewsManager socialStoryboardViewControllerWithIdentifier:@"ContactNavigationViewController"];
        
        ContactListTableViewController *controller = (ContactListTableViewController*) navigation.viewControllers[0];
        controller.model = model;
        controller.wasPresentedFromPopup = YES;
        controller.dismissBlock = ^{
            [self.parentController presentViewController:self animated:YES completion:nil];
        };
        [self dismissViewControllerAnimated:NO completion: ^{
            [self.parentController presentViewController:navigation animated:YES completion:nil];
        }];
    } else {
        [AlertMessage showAlertWithMessage: @"Este usuário possui o perfil fechado e você não pode ver quem ele segue." controller: self];
    }
}

-(void)openFollowers{
    if (self.contact.isOpenFollowersAndFollowings || ConsumerManager.shared.consumer.wsId == self.contact.wsId){
        FollowersContactListViewModel *model =  [[FollowersContactListViewModel alloc] initWithProfileId:[NSString stringWithFormat:@"%ld", (long) self.contact.wsId]];
        
        FollowersTableViewController *controller = [[FollowersTableViewController alloc] initWithModel:model];
        controller.wasPresentedFromPopup = YES;
        controller.dismissBlock = ^{
            [self.parentController presentViewController:self animated:YES completion:nil];
        };
        
        UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:controller];
        __weak typeof(self) weakSelf = self;
        [self dismissViewControllerAnimated:YES completion:^{
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf.parentController presentViewController:navigation animated:YES completion:nil];
        }];
    }else{
        [AlertMessage showAlertWithMessage: @"Este usuário possui o perfil fechado e você não pode ver seus seguidores." controller: self];
    }
}

@end
