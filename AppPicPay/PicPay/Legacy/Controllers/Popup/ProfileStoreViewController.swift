import AnalyticsModule
import UI
import UIKit
import FeatureFlag

final class ProfileStoreViewController: UIViewController {
    private var fullImage: ImageFullScreenView?
    private weak var parentController: UIViewController?
    private let service: FavoritesServicing = FavoritesService()
    var store: PPStore
    
    // For the new parking
    var sellerId: String?
    var storeId: String?
    var isNewParking: Bool = false
    
    private var isFavorite: Bool = false {
        didSet {
            favoriteButton.isSelected = isFavorite
        }
    }
    
    // MARK: - Initializer
    
    @objc var dismissBlock: (() -> Void)?
    
    private lazy var favoriteButton: UIButton = {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(#imageLiteral(resourceName: "favorite_buttonFavorite"), for: .normal)
        button.setImage(#imageLiteral(resourceName: "favorite_buttonUnFavorite"), for: .selected)
        button.addTarget(self, action: #selector(didTapFavoriteButton), for: .touchUpInside)
        return button
    }()
    
    @IBOutlet weak var profileImage: UICircularImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var apyButton: UIPPButton!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    @IBOutlet weak var closePopupButton: UIButton!
    @IBOutlet weak var badgeVerifiedImage: UIImageView!
    
    @objc
    init(store: PPStore, controller: UIViewController?) {
        self.store = store
        sellerId = store.sellerId
        storeId = store.storeId
        isNewParking = store.isParkingPayment == 2
        parentController = controller
        super.init(nibName: "ProfileStoreViewController", bundle: nil)
        setupColors()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureProfile()
        loadProfile()
        setupFavoriteButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkIsFavorite()
    }
    
    // MARK: - User Actions
    
    @IBAction private func close(_ sender: AnyObject) {
        dismiss(animated: true) { [weak self] in
            self?.dismissBlock?()
        }
    }
    
    @IBAction private func pay(_ sender: AnyObject) {
        dismiss(animated: false) { [weak self] in
            self?.didTouchPayButton()
        }
    }
    
    // MARK: - Internal
    
    private func setupColors() {
        view.backgroundColor = Palette.ppColorGrayscale000.color
        textLabel.textColor = Palette.ppColorGrayscale600.color
    }
    
    func configureProfile() {
        nameLabel.text = store.name
        textLabel.text = store.address
        badgeVerifiedImage.isHidden = !store.isVerified
        
        if store.img_url != nil {
            profileImage.setImage(url: URL(string: store.img_url), placeholder: PPStore.photoPlaceholder())
        }
        
        if let large = store.large_img_url {
            fullImage = ImageFullScreenView(imageView: profileImage, largeUrl: large)
        } else {
            fullImage = ImageFullScreenView(imageView: profileImage)
        }
    }
    
    private func setupFavoriteButton() {
        view.addSubview(favoriteButton)
        
        NSLayoutConstraint.activate([
            favoriteButton.centerYAnchor.constraint(equalTo: closePopupButton.centerYAnchor, constant: 4),
            favoriteButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16)
        ])
    }
    
    private func checkIsFavorite() {
        guard let id = sellerId else {
            return
        }

        service.isFavorite(id: id, type: .store) { [weak self] isFavorite in
            self?.isFavorite = isFavorite
            self?.favoriteButton.isSelected = isFavorite

        }
    }
    
    /**
     Load profile data from serer
     */
    func loadProfile() {
        guard store.isCielo == false, let sellerId = store.sellerId else {
            return stopLoading()
        }

        startLoading()
        WSSocial.getStoreProfile(sellerId) { [weak self] storeProfile, error in
            DispatchQueue.main.async {
                guard let self = self else {
                    return
                }
                self.stopLoading()
                if let store = storeProfile {
                    self.store = store
                    self.configureProfile()
                    Analytics.shared.log(PaymentEvent.itemPopupOpened(name: store.name ?? ""))
                }
                if error != nil {
                    AlertMessage.showAlert(withMessage: error?.localizedDescription, controller: self)
                }
            }
        }
    }
    
    /**!
     * Show the loading view
     */
    func startLoading() {
        loadingView.isHidden = false
        loadingView.startAnimating()
        profileImage.layer.opacity = 0.1
        textLabel.layer.opacity = 0.1
        nameLabel.layer.opacity = 0.1
        apyButton.layer.opacity = 0.1
        apyButton.isEnabled = false
    }
    
    /**!
     * Stop and hide the loading view
     */
    func stopLoading() {
        loadingView.isHidden = true
        loadingView.stopAnimating()
        profileImage.layer.opacity = 1.0
        textLabel.layer.opacity = 1.0
        nameLabel.layer.opacity = 1.0
        apyButton.layer.opacity = 1.0
        apyButton.isEnabled = true
    }
    
    private func didTouchPayButton() {
        Analytics.shared.log(PlacesEvent.didTapPayButtonOnStoreProfile(type: store.isCielo ? .cielo : .biz, storeName: store.name ?? ""))
        
        dismissBlock?()
        guard let parentVC = parentController, store.processor != PPStoreProcessor.zonaAzul else {
            return
        }
        
        if isNewParking {
            openNewParkingScreen(from: parentVC)
            return
        }
        if store.isCielo {
            openCieloScanner(from: parentVC)
            return
        }
        openGenericPaymentScreen(from: parentVC)
    }
    
    private func openNewParkingScreen(from parentVC: UIViewController) {
        guard let sellerIdString = sellerId, let storeIdString = storeId else {
            return
        }
        let parkingViewController = ParkingCoordinator().start(sellerId: sellerIdString, storeId: storeIdString)
        parentVC.present(parkingViewController, animated: true, completion: nil)
    }
    
    private func openGenericPaymentScreen(from parentVC: UIViewController) {
        // prevent payment loop
        guard !parentVC.isKind(of: PaymentViewController.self),
            let screenName = PPAnalytics.screenName(fromViewController: parentVC, withVariation: ""),
            let paymentDetailsNavigationController = ViewsManager.paymentStoryboardFirtNavigationController(),
            let paymentVC = paymentDetailsNavigationController.topViewController as? PaymentViewController else {
                dismiss(animated: true) { [weak self] in
                    self?.dismissBlock?()
                }
                return
        }
        
        paymentVC.store = store
        paymentVC.from_explore = true
        paymentVC.touchOrigin = String(format: "profile [%@]", screenName)
        paymentVC.navigationLevel = 10
        paymentVC.showCancel = true
        
        parentVC.present(PPNavigationController(rootViewController: paymentVC), animated: true, completion: nil)
    }
    
    private func openCieloScanner(from parentVC: UIViewController) {
        let scanner = P2MScannerViewController.controllerFromStoryboard()
        let scannerNavigationController = UINavigationController(rootViewController: scanner)
        parentVC.present(scannerNavigationController, animated: true)
    }
    
    @objc
    private func didTapFavoriteButton() {
        guard let id = sellerId else {
            return
        }

        isFavorite ? unfavorite(id: id, type: .store) : favorite(id: id, type: .store)
    }
}

private extension ProfileStoreViewController {
    func favorite(id: String, type: Favorite.`Type`) {
        isFavorite = true

        service.favorite(id: id, type: type) { [weak self] didFavorite in
            self?.isFavorite = didFavorite

            if didFavorite {
                Analytics.shared.log(FavoritesEvent.statusChanged(true, id: id, origin: .profile))
            }
        }
    }

    func unfavorite(id: String, type: Favorite.`Type`) {
        isFavorite = false

        service.unfavorite(id: id, type: type) { [weak self] didUnfavorite in
            self?.isFavorite = didUnfavorite == false

            if didUnfavorite {
                Analytics.shared.log(FavoritesEvent.statusChanged(false, id: id, origin: .profile))
            }
        }
    }
}
