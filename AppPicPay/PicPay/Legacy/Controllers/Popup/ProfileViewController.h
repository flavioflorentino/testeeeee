#import <UIKit/UIKit.h>
@import CoreLegacy;
@class UIPPFollowButton;
@class UIPPButton;

@interface ProfileViewController : UIViewController

@property (strong, nonatomic) PPContact *contact;

@property (weak, nonatomic) IBOutlet UIImageView *pictureImageView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) UIViewController *parentController;
@property (weak, nonatomic) IBOutlet UIImageView *verifiedImageView;
@property (weak, nonatomic) UIView *originView;
@property (weak, nonatomic) IBOutlet UIPPFollowButton *followButton;
@property (weak, nonatomic) IBOutlet UIPPButton *payButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;
@property (weak, nonatomic) IBOutlet UILabel *followersLabel;
@property (weak, nonatomic) IBOutlet UILabel *followingLabel;
@property (weak, nonatomic) IBOutlet UIView *followingView;
@property (weak, nonatomic) IBOutlet UILabel *followerStaticLabel;
@property (weak, nonatomic) IBOutlet UILabel *followingStaticLabel;
@property (weak, nonatomic) IBOutlet UIView *followersView;

- (instancetype)initWithContact:(PPContact *) contact controller:(UIViewController *) controller;
- (instancetype)initWithContact:(PPContact *) contact controller:(UIViewController *) controller originView:(UIView *)originView;
    
@property (weak, nonatomic) IBOutlet UIImageView *proImageView;

- (IBAction)close:(id)sender;
- (IBAction)followButtonActino:(UIPPFollowButton *)sender;

@end
