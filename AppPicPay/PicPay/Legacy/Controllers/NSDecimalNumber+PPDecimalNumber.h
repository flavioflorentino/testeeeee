//
//  NSDecimalNumber+PPDecimalNumber.h
//  PicPay
//
//  Created by Diogo Carneiro on 28/10/14.
//
//

#import <Foundation/Foundation.h>

@interface NSDecimalNumber (PPDecimalNumber)

+ (NSDecimalNumber *)decimalNumberWithFloat:(float)floatNumber;
+ (NSDecimalNumber *)decimalNumberWithInt:(NSInteger)intNumber;

@end
