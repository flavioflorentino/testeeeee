import AnalyticsModule
import UIKit
import AsyncDisplayKit
import UI
import FeatureFlag
import Billet

enum FeedMode{
    case normal
    case profile
}

@objc protocol FeedViewControllerDelegate : AnyObject {
    @objc func feedScrollViewDidScroll(_ scrollView: UIScrollView)
    @objc func feedWillReload()
    @objc func feedDidReload()
    @objc func feedDidLoadNewPage()
}

/// Workaround to use the FeedViewController on OBJC
final class FeedViewControllerObjc: NSObject {
    
    private var feedViewController: FeedViewController
    
    @objc public static var visibilityFriends:String = FeedItemVisibility.Friends.rawValue
    @objc public static var visibilityPrivate:String = FeedItemVisibility.Private.rawValue
    
    @objc init(type:String, delegate:FeedViewControllerDelegate, tableNode: ASTableNode) {
        feedViewController = FeedViewController(tableView: tableNode)
        feedViewController.delegate = delegate;
        
        feedViewController.configureModel()
        feedViewController.configureTableView()
        if let visibility = FeedItemVisibility(rawValue: type) {
            feedViewController.changeFilter(Int(visibility.rawValue) ?? 2)
        }
    }
    
    @objc func reloadFeed(){
        feedViewController.reloadFeed()
    }
    
    @objc func changeFilter(_ visibility:Int){
        feedViewController.changeFilter(visibility)
    }
    
    @objc func controller() -> UIViewController {
        return feedViewController
    }
    
    @objc func scrollToTop() {
        feedViewController.tableNode.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
}

class FeedViewController: ASDKViewController<ASTableNode> {
    private var establishmentDetailsCoordinator: EstablishmentDetailsCoordinating?

    weak var delegate:FeedViewControllerDelegate?
    
    var tableNode:ASTableNode
    var model:FeedViewModel?
    var refreshControl:UIRefreshControl?
    var mode: FeedMode = .normal
    // We don't want to show the animation on the first load
    var avoidAnimation = true
    var isServerLoading:Bool = true
    var isFirstLoad: Bool = true
    var isScrollViewEnable: Bool = true {
        didSet {
            if isScrollViewEnable {
                self.tableNode.view.isScrollEnabled = true
            } else {
                self.tableNode.view.isScrollEnabled = false
            }
        }
    }
    
    var currentContext: ASBatchContext? = nil
    
    lazy var headerView: UIView = {
        return UIView(frame: .zero)
    }()
    
    lazy var loadingFootView: UIView = {
        let view = UIView(frame:CGRect(x: 0,y: 0, width: self.view.frame.size.width, height: 100))
        let spinner = UIActivityIndicatorView(frame: CGRect(x: (self.view.frame.size.width / 2.0) - 15.0, y: 35.0,width: 30.0, height: 30.0))
        spinner.color = UIColor.lightGray
        view.addSubview(spinner)
        spinner.startAnimating()
        return view
    }()
    
    var messageView: UIView = UIView(frame: .zero)
    var headerNode: ASCellNode = ASCellNode { () -> UIView in
        let view = UIView(frame: .zero)
        view.backgroundColor = .clear
        return view
    }
    lazy var refreshBackgroundView: UIView = {
        return UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 300))
    }()
    
    internal enum Observables: String {
        case NeedReloadItem = "FeedViewControllerNeedReloadItem"
        case FeedNeedReload = "FeedNeedReload"
    }
    
    var previewingContext: UIViewControllerPreviewing?
    
    // MARK: - Initializers
    
    init(tableView: ASTableNode, mode: FeedMode = .normal) {
        self.tableNode = tableView
        self.mode = mode
        super.init(node: tableView)
        self.configureTableView()
    }
    
    convenience override init() {
        self.init(tableView: ASTableNode(style: .plain))
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        tableNode = ASTableNode(style: .plain)
        super.init(node: tableNode)
        self.configureTableView()
    }
    
    func changeHeader(_ headerView: UIView){
        self.headerView = headerView
        headerNode = ASCellNode { () -> UIView in
            return headerView
        }
        refreshBackgroundView.backgroundColor = headerView.backgroundColor
    }
    
    // MARK: - View life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerObservers()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        identifyStyle(with: previousTraitCollection)
    }
    
    private func identifyStyle(with previousTraitCollection: UITraitCollection?) {
        if #available(iOS 13.0, *) {
            guard let previousTraitCollection = previousTraitCollection,
                previousTraitCollection.userInterfaceStyle != self.traitCollection.userInterfaceStyle else {
                return
            }
            tableNode.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Configuration
    
    func registerObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(reloadFeedItemNotificationHandler), name: NSNotification.Name(rawValue: Observables.NeedReloadItem.rawValue), object: nil)
    }
    
    /**
     Configure the feed table view
     */
    func configureTableView(){
        tableNode.automaticallyAdjustsContentOffset = false
        tableNode.delegate = self
        tableNode.dataSource = self
        tableNode.view.allowsSelection = true
        tableNode.leadingScreensForBatching = 3
        tableNode.view.separatorStyle = .none
        tableNode.placeholderEnabled = true
        tableNode.backgroundColor = Palette.ppColorGrayscale100.color
        
        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = UIColor.lightGray
        refreshControl?.addTarget(self, action: #selector(reloadFeed), for: .valueChanged)
        
        switch mode {
        case .profile:
            tableNode.view.isScrollEnabled = false
        default:
            tableNode.contentInset = .zero
            tableNode.view.scrollIndicatorInsets = .zero
            
            // Normal mode should have refresh
            tableNode.view.refreshControl = refreshControl
        }
        
        refreshControl?.beginRefreshing()
        refreshControl?.endRefreshing()
    }
    
    /**
     Configure Feed Model with default model
     */
    func configureModel(){
        self.model = FeedViewModel()
    }

    // MARK: Public Methods
    
    func changeContentInsets(_ insets: UIEdgeInsets){
        tableNode.contentInset = insets
        tableNode.view.scrollIndicatorInsets = insets
    }
    
    /// Change filter visibility
    /// set controller for frinds feed or private feed
    ///
    /// - Parameter visibility: visibility of controller
    func changeFilter(_ visibility: Int) {
        DispatchQueue.main.async {
            if let visibility = FeedViewModel.Visibility(rawValue: visibility) {
                self.model?.visibility = visibility
            }
            
            self.startLoading()
            self.currentContext?.cancelBatchFetching()
            self.delegate?.feedWillReload()
            
            //remove all table node rows
            self.tableNode.performBatch(animated: false, updates: { [weak self] () in
                // for reload remove all rows before load feed
                var indexPaths = [IndexPath]()
                let endIndex = (self?.model != nil ? (self?.model?.items.count)! - 1 : 0)
                self?.model?.clearFeed()
                if(endIndex >= 0){
                    for i in 0...endIndex{
                        indexPaths.append(IndexPath(row: i, section: 0))
                    }
                    self?.tableNode.deleteRows(at: indexPaths, with: .none)
                }
                }, completion: { [weak self] (completed) in
                    DispatchQueue.main.async {
                        guard let strongSelf = self else {
            return
        }
                        strongSelf.startLoading()
                        strongSelf.model?.canLoadMoreFeed = false
                        strongSelf.loadFeedData(strongSelf.tableNode, context: nil, reload: true)
                    }   
            })
        }
    }
    
    // MARK: Internal Methods
    
    @objc func reloadFeed(){
        self.delegate?.feedWillReload()
        currentContext?.cancelBatchFetching()
        if let loading = self.model?.isLoading, loading == false {
            self.model?.canLoadMoreFeed = false
            self.loadFeedData(self.tableNode, context: nil, reload:true)
        } else {
            self.refreshControl?.endRefreshing()
            self.delegate?.feedDidReload()
        }
    }
    
    @objc func reloadFeedFromHeader() {
        startAnimationCirleInHeaderView()
        isServerLoading = true
        showServerLoading()
        reloadFeed()
    }
    
    /// Start animatins erro for circle view
    func startAnimationCirleInHeaderView() {
        guard let view = tableNode.view.tableHeaderView as? TableItemErrorView else {
            return
        }
        view.startAnimateCircle()
    }
    
    /**
     Start the spinner loading
     */
    func startLoading(){
        tableNode.view.tableFooterView = self.loadingFootView
    }
    
    /**
     Stop the spinner loading
     */
    func stopLoading(){
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else {
            return
        }
            strongSelf.loadingFootView.removeFromSuperview()
            let newFooterView = UIView(frame: CGRect.zero)
            strongSelf.tableNode.view.tableFooterView = newFooterView
        }
    }
    
    /**
     * Check if feed is empty or have an error
     */
    func checkEmptyFeed(_ error:Error?){
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self, let feedViewModel = strongSelf.model else {
                return
            }
            strongSelf.hideEmptyView()
            
            if error == nil {
                if feedViewModel.items.count < 1 && !feedViewModel.isLoading {
                    strongSelf.showEmptyView()
                }
                if strongSelf.isServerLoading {
                    strongSelf.showServerLoading()
                }
            } else {
                
                //Apresenta erro quando não há itens
                if feedViewModel.items.count == 0 {
                    let errorView = ConnectionErrorView(with: nil, type: .connection, frame: strongSelf.view.frame)
                    errorView.isUserInteractionEnabled = true
                    errorView.frame.size.height = min(CGFloat(360),strongSelf.view.frame.size.height - (self?.headerView.frame.size.height ?? CGFloat(0)) - 50)
                    errorView.tryAgainTapped = { [weak self] in
                        self?.reloadFeed()
                    }
                    DispatchQueue.main.async {
                        self?.tableNode.view.tableHeaderView = errorView
                    }
                    //Erro no load inicial, mas já foi carregado cache
                } else if feedViewModel.isFirstPage {
                    let view = TableItemErrorView(frame:CGRect(x: 0,y: 0,width: strongSelf.view.frame.size.width, height: 60))
                    view.isUserInteractionEnabled = true
                    view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(FeedViewController.reloadFeedFromHeader)))
                    DispatchQueue.main.async {
                        self?.tableNode.view.tableHeaderView = view
                    }
                } else {
                    //Erro ao carregar itens paginados
                    let view = TableItemErrorView(frame:CGRect(x: 0,y: 0,width: strongSelf.view.frame.size.width, height: 60))
                    view.isUserInteractionEnabled = true
                    view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(FeedViewController.reloadPaginationRequest)))
                    DispatchQueue.main.async {
                        self?.tableNode.view.tableFooterView = view
                    }
                }
            }
        }
    }
    
    /**
     * Call pagination load
     */
    @objc func reloadPaginationRequest(){
        self.startAnimationCirleInHeaderView()
        self.model?.canLoadMoreFeed = true
        self.loadFeedData(self.tableNode, context:nil)
    }
    
    /**
     Show the empty view
     */
    func showEmptyView(){
        let views = Bundle.main.loadNibNamed("FeedEmptyListView", owner: nil, options: nil)
        if let emptyView = views?.first as? FeedEmptyListView {
            emptyView.backgroundColor = .clear
            emptyView.frame = self.view.frame
            emptyView.frame.size.height = max(UIScreen.main.bounds.height - 380, 190)
            
            // change the text and image according to visibility
            if (self.model?.visibility == .followers){
                emptyView.titleLabel.text = "Tudo quieto por aqui ainda"
                emptyView.textLabel.text = "Aproveita e toca no botão verde aqui embaixo para descobrir tudo que você pode pagar com Picpay :)"
            }
            
            DispatchQueue.main.async {
                emptyView.layoutIfNeeded()
                emptyView.setAnimation(name: "curious-face-animation")
                emptyView.playAnimation()
                self.tableNode.view.tableHeaderView = emptyView
            }
        }
    }
    
    /**
     Hide the empty view
     */
    func hideEmptyView(){
        if !self.isServerLoading {
            DispatchQueue.main.async {
                self.tableNode.view.tableHeaderView = nil
            }
        }
    }
    
    /**
     Load data new data
     */
    func loadFeedData(_ tableNode: ASTableNode, context: ASBatchContext?, reload: Bool = false){
        currentContext = context
        hideEmptyView()
        
        guard let viewModel = self.model, viewModel.canLoadMoreFeed || reload else {
            context?.cancelBatchFetching()
            return
        }
        
        viewModel.canLoadMoreFeed = false
        
        // show for user that the app is lodding more data
        // this method need to perform on main thread
        if !reload {
            DispatchQueue.main.async {
                self.startLoading()
            }
        }
        
        if reload || currentContext == nil {
            viewModel.isLoading = true
        }
        
        // Load more data
        self.isServerLoading = true
        context?.beginBatchFetching()
        viewModel.loadMoreFeed(reload, onCacheLoaded: { [weak self] newItems, hasNext in
            if self?.isFirstLoad == true {
                self?.applyFeedResult(
                    newItems,
                    hasNext: hasNext,
                    error: nil,
                    tableNode: tableNode,
                    context: nil)
                self?.isFirstLoad = false
            }
            self?.model?.sendAnalyticsLogForFeedDidAppear()
        }, callback: { [weak self] newItems, hasNext, error in
            self?.isServerLoading = false

            if error != nil {
                self?.model?.sendAnalyticsLogForFeedDidAppear()
            }

            if !reload {
                self?.applyFeedResult(
                    newItems,
                    hasNext: hasNext,
                    error: error,
                    tableNode: tableNode,
                    context: context,
                    reload: reload
                )
            } else {
                if error != nil {
                    DispatchQueue.main.async {
                        self?.endLoadingResult(
                            newItems,
                            hasNext: hasNext,
                            error: error,
                            tableNode: tableNode,
                            context: context,
                            reload: reload
                        )
                    }
                    return
                }
                
                guard let oldItems = self?.model?.items else {
                    DispatchQueue.main.async {
                        self?.endLoadingResult(
                            newItems,
                            hasNext: hasNext,
                            error: error,
                            tableNode: tableNode,
                            context: context,
                            reload: reload
                        )
                    }
                    return
                }
                
                var deletions: [FeedItem] = oldItems
                var additions: [FeedItem] = []
                var deletionPaths: [IndexPath] = []
                var currentRow = 0
                
                for item in newItems {
                    if !oldItems.contains(item) {
                        additions.append(item)
                    } else {
                        guard let index = deletions.firstIndex(of: item) else {
                            continue
                        }
                        deletions.remove(at: index)
                    }
                    currentRow += 1
                }
                
                for item in deletions {
                    guard let index = oldItems.firstIndex(of: item) else {
                        continue
                    }
                    deletionPaths.append(IndexPath(row: index, section: 0))
                }
                
                var additionPaths: [IndexPath] = []
                currentRow = 0
                additions.forEach({ _ in
                    additionPaths.append(IndexPath(row: currentRow, section: 0))
                    currentRow += 1
                })
                
                DispatchQueue.main.async {
                    guard let self = self else {
                        return
                    }
                    self.delegate?.feedDidLoadNewPage()
                    // Update the old list
                    self.model?.items = newItems
                    
                    if self.avoidAnimation {
                        self.tableNode.reloadSections(IndexSet(integer: 0), with: .none)
                        self.endLoadingResult(
                            newItems,
                            hasNext: hasNext,
                            error: error,
                            tableNode: tableNode,
                            context: context,
                            reload: reload)
                        return
                    }
                    self.avoidAnimation = false
                    self.tableNode.insertRows(at: additionPaths, with: .top)
                    self.tableNode.deleteRows(at: deletionPaths, with: .bottom)
                    self.endLoadingResult(
                        newItems,
                        hasNext: hasNext,
                        error: error,
                        tableNode: tableNode,
                        context: context,
                        reload: reload
                    )
                }
            }
        }, blockRequest: {
            context?.cancelBatchFetching()
        })
    }
    
    /**
     Applies the feed reasult on the screen
     */
    func applyFeedResult(_ newItems: [FeedItem], hasNext: Bool, error: Error?, tableNode: ASTableNode, context: ASBatchContext?, reload: Bool = false) {
        DispatchQueue.main.async {
            if error != nil {
                context?.cancelBatchFetching()
                self.endLoadingResult(newItems, hasNext: hasNext, error: error, tableNode: tableNode, context: context, reload:reload)
            } else {
                if !newItems.isEmpty {
                    // insert new items
                    tableNode.performBatch(animated: false, updates: {  [weak self] in ()
                        let iniIndex = (self?.model != nil ? self?.model?.items.count : 0) ?? 0
                        let endIndex = iniIndex + max(0, newItems.count - 1)
                        
                        // add new items at list
                        self?.model?.items.append(contentsOf: newItems)
                        
                        // create new rows
                        var indexPaths = [IndexPath]()
                        if iniIndex <= endIndex {
                            for i in iniIndex...endIndex{
                                indexPaths.append(IndexPath(row: i, section: 0))
                            }
                            tableNode.insertRows(at: indexPaths, with: .none)
                        }
                        
                        }, completion: { [weak self] (completed) in
                            self?.endLoadingResult(newItems, hasNext: hasNext, error: error, tableNode: tableNode, context: context, reload:reload)
                    })
                } else {
                    self.endLoadingResult(newItems, hasNext: hasNext, error: error, tableNode: tableNode, context: context, reload:reload)
                }
            }
        }
    }
    
    func endLoadingResult(_ newItems:[FeedItem], hasNext:Bool, error:Error?, tableNode: ASTableNode, context: ASBatchContext?, reload:Bool = false){
        if let ctx = context {
            if ctx.isFetching() {
                context?.completeBatchFetching(true)
            }
        }
        DispatchQueue.main.async{
            self.stopLoading()
            self.refreshControl?.endRefreshing()
            self.model?.canLoadMoreFeed = hasNext
            self.model?.isLoading = false
            self.checkEmptyFeed(error)
            if reload {
                self.delegate?.feedDidReload()
            }
            if self.mode == .profile {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.25, execute: {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: ScrollManager.Observables.ScrollManagerChildLoaded.rawValue), object: nil, userInfo: nil)
                })
            }
        }
    }
    
    /**
     Reload the row with feed
     */
    func reloadFeedItem(_ feedItem:FeedItem){
        self.model?.updateFeedItem(feedItem, completion: { (indexFeedItem) in
            if let index = indexFeedItem {
                DispatchQueue.main.async {
                    self.tableNode.reloadRows(at: [IndexPath(row: index, section: 0)], with: .fade)
                }
            }
        })
    }
    
    /**
     Show the feed detail screen
     */
    func openFeedDetail(_ feedItem:DefaultFeedItem, openKeyboard:Bool = false){
        let feedDetailModel = FeedDetailViewModel(feedItem: feedItem)
        let controller = FeedDetailViewController(model: feedDetailModel)
        controller.appearWithOpenKeyboard = openKeyboard
        controller.hidesBottomBarWhenPushed = true
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func openBilletFeedDetail(transactionId: String, externalId: String) {
        let controller = BilletFeedDetailFactory.make(transactionId: transactionId, externalId: externalId)
        controller.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(controller, animated: true)
    }
    
    /**
     Show document request screen
     */
    func openRequestDocument(){
        let antiFraudChecklistVC = AntiFraudChecklistViewController()
        antiFraudChecklistVC.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(antiFraudChecklistVC, animated: true)
    }
    
    // MARK: - Notification Handler
    
    @objc func reloadFeedItemNotificationHandler(_ notification:Notification){
        if let feedItem = notification.userInfo?["feedItem"] as? FeedItem {
            self.reloadFeedItem(feedItem)
        }
    }
}

extension FeedViewController: ASTableDelegate, ASTableDataSource {
    
    // MARK: - ASTableDelegate, ASTableDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableNode(_ tableNode: ASTableNode, numberOfRowsInSection section: Int) -> Int {
        return (self.model != nil) ? self.model!.items.count : 0
    }
    
    func tableNode(_ tableNode: ASTableNode, nodeForRowAt indexPath: IndexPath) -> ASCellNode {
        if self.model?.items.count > indexPath.row {
            if let feedItem = self.model?.items[indexPath.row]{
                switch feedItem.component {
                    
                case .requestDocument:
                    if let feedDefaultIem = feedItem as? DefaultFeedItem{
                        return RequestDocumentFeedCellNode(feedItem:feedDefaultIem)
                    }
                case .nonInteractive:
                    if let feedDefaultIem = feedItem as? DefaultFeedItem{
                        return NonInteractiveFeedCellNode(feedItem:feedDefaultIem, delegate: self)
                    }
                case .carousel:
                    guard let carouselItem = feedItem as? OpportunityFeedItem else {
                        return ASTextCellNode()
                    }
                    let carouselCell = CarouselFeedItemFactory.make(withItem: carouselItem)
                    return WrapperCellNode(cellView: carouselCell, height: carouselCell.staticHeight)
                case .banner:
                    guard let bannerItem = feedItem as? OpportunityFeedItem,
                        let opportunityItem = bannerItem.opportunities.first else {
                        return ASTextCellNode()
                    }
                    let bannerCell = BannerFeedItemFactory.make(withItem: opportunityItem)
                    return WrapperCellNode(cellView: bannerCell, height: bannerCell.height)
                default:
                    if let feedDefaultIem = feedItem as? DefaultFeedItem{
                        return DefaultFeedCellNode(feedItem:feedDefaultIem, delegate: self)
                    }
                }
            }
        }
        return ASTextCellNode()
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func shouldBatchFetch(for tableView: ASTableView) -> Bool {
        return self.model?.canLoadMoreFeed == true &&  model?.isLoading == false && model?.items.count > 0
    }
    
    func tableNode(_ tableNode: ASTableNode, willBeginBatchFetchWith context: ASBatchContext) {
        loadFeedData(tableNode, context: context, reload: false)
    }
    
    func tableNode(_ tableNode: ASTableNode, didSelectRowAt indexPath: IndexPath) {
        guard
            self.model?.items.count > indexPath.row,
            let feedItem = self.model?.items[indexPath.row],
            feedItem.component != FeedItemComponent.nonInteractive
            else {
                return
        }
        switch feedItem.component {
        case .requestDocument:
            openRequestDocument()
        case .payer:
            guard let url = model?.paymentRequestPayerGenerateUrl(feedItem) else {
                return
            }
            DeeplinkHelper.handleDeeplink(withUrl: url, from: self)
        case .charge:
            guard let defaultFeedItem = feedItem as? DefaultFeedItem else {
                return
            }
            let viewController = PaymentRequestFeedDetailFactory.make(feedItem: defaultFeedItem)
            viewController.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(viewController, animated: true)
        default:
            if let feedDefaultItem = feedItem as? DefaultFeedItem {
                model?.externalTransferEventHandler(feedItem: feedDefaultItem, event: .cardClicked(status: .processing))
                
                if feedDefaultItem.type == "BillPaymentFeedItem" {
                    openBilletFeedDetail(transactionId: feedDefaultItem.transactionId, externalId: feedDefaultItem.externalId)
                } else {
                    openFeedDetail(feedDefaultItem)
                }
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        delegate?.feedScrollViewDidScroll(scrollView)
    }
    
    func feedActionButton(feedItemId: String, dataId: String, actionType: String) {
        
        // implements any types of actions that comes with feed data on DefaultFeedCellNode
        
        // MARK: Action: openP2PTransactionVerification or openPAVTransactionVerification
        if actionType == "openP2PTransactionVerification" || actionType == "openPAVTransactionVerification" {
            let controller = ViewsManager.antiFraudStoryboardViewController(withIdentifier: "AnalysisAccelerationViewController") as? AnalysisAccelerationViewController
            controller!.transactionId = dataId
            controller!.feedItemId = feedItemId
            controller!.actionType = actionType
            controller!.title = "Acelerar análise"
            controller!.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(controller!, animated: true)
        }        
    }
    
    private func feedActionButton(feedItemId: String, url: URL) {
        DeeplinkHelper.handleDeeplink(withUrl: url, from: self)
    }
    
    func showServerLoading() {
        let loadingView = LoadingTableViewHeader(frame: CGRect(origin: .zero, size: CGSize(width: self.view.frame.width, height: 50)))
        DispatchQueue.main.async {
            loadingView.startAnimating()
        }
    }
    
}

// MARK: - Feed Item node Actions
extension FeedViewController: DefaultFeedCellNodeDelegate {
    
    func feedDidTapOnProfile(_ contact: PPContact?) {
        Analytics.shared.log(ProfileEvent.touchPicture(from: .feed))
        guard let contact = contact, let parentVC = self.parent,
            let popup = NewProfileProxy.openProfile(contact: contact, parent: parentVC) else {
            return
        }
        self.showPopup(popup)
    }
    
    func feedDidTapOnStoreProfile(_ store: PPStore?) {
        guard let store = store, let parentVC = self.parent else {
            return
        }
        
        Analytics.shared.log(PaymentEvent.itemPopupAccessed(type: .biz, origin: .photo))

        establishmentDetailsCoordinator = EstablishmentCoordinatorFactory().makeDetailsCoordinator(
            store: store,
            navigationController: parentVC.navigationController,
            fromViewController: parentVC
        )

        establishmentDetailsCoordinator?.didFinishFlow = { [weak self] in
            self?.establishmentDetailsCoordinator = nil
        }

        establishmentDetailsCoordinator?.start()
    }
    
    func feedDidTapOnDigitalGoodProfile(_ digitalGood: DGItem?) {
        guard let digitalGood = digitalGood, let parentVC = self.parent else {
            return
        }
        let popup = DGProfileViewController(digitalGood: digitalGood, controller: parentVC)
        self.showPopup(popup)
    }
    
    func feedDidTapOnProducerProfile(_ id: String) {
        let popup = ProducerProfileViewController(parentViewController: self, model: ProducerViewModel(producerId: id), origin: .feed)
        self.showPopup(popup)
    }
    
    func feedDidTapOnP2MProfile() {
        Analytics.shared.log(PaymentEvent.itemPopupAccessed(type: .p2m, origin: .photo))
        P2MHelpers.presentScanner(from: self)
    }
    
    func feedDidTapOnBoletoProfile() {
        guard FeatureManager.isActive(.isNewBilletHubAvailable) else {
            DGHelpers.openBoletoFlow(viewController: self, origin: "feed")
            return
        }
        
        let hubViewController = BilletHubFactory.make(with: .feed)
        let navigation = UINavigationController(rootViewController: hubViewController)
        present(navigation, animated: true)
    }
    
    func feedDidTapOnLike(_ like: Bool, feedItem: DefaultFeedItem) {
        HapticFeedback.impactFeedbackLight()
        self.model?.like(like, feedItem: feedItem, completion: { (likeCheck, success, error) in
            
        })
    }
    
    func feedDidTapOnComment(_ feedItem:DefaultFeedItem){
        self.openFeedDetail(feedItem, openKeyboard:true)
    }
    
    func feedDidTabOnButton(feedItemId: String, dataId: String, actionType: String) {
        feedActionButton(feedItemId: feedItemId, dataId: dataId, actionType: actionType)
    }
    
    func feedDidTabOnButton(feedItemId: String, url: URL) {
        feedActionButton(feedItemId: feedItemId, url: url)
    }
    
    func shareMessage(_ message: String, feedItem: DefaultFeedItem) {
        model?.externalTransferEventHandler(feedItem: feedItem, event: .resendLink(origin: .cardButton))
        let controller = UIActivityViewController(activityItems: [message], applicationActivities: nil)
        present(controller, animated: true)
    }
}

extension FeedViewController: NonInteractiveFeedCellNodeDelegate {
    
    func feedDidTabOnButtonNonInteractive(feedItemId:String, dataId:String, actionType:String) {
        self.feedActionButton(feedItemId: feedItemId, dataId: dataId, actionType: actionType)
    }
}
