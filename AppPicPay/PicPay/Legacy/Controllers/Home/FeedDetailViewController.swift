import AsyncDisplayKit
import CoreLegacy
import UI
import UIKit
import SnapKit
import FeatureFlag
import AnalyticsModule
import PIX
import Billet

/// Workaround to use the FeedDetailViewController on OBJC
final class FeedDetailViewControllerObjc : NSObject {
    
    @objc static func createFeedDetailViewController(_ model: FeedDetailViewModel) -> UIViewController {
        let controller = FeedDetailViewController(model: model)
        return controller
    }

}

class FeedDetailViewController: ASDKViewController<ASDisplayNode>, FeedDetailViewModelDelegate, PPNavigationBarProtocol {

    // MARK: - PPNavigationBarProtocol
    func navigationBarIsTransparent() -> Bool {
        return false
    }
    
    var _shouldShowBarWhenDisappear = false;
    func setShouldShowBarWhenDisappear(_ showBar: Bool) {
        _shouldShowBarWhenDisappear = showBar;
    }
    
    func shouldShowBarWhenDisappear() -> Bool {
        return _shouldShowBarWhenDisappear
    }
    
    internal var model:FeedDetailViewModel
    internal var tableNode:ASTableNode
    internal var messageComposerView:MessageComposerView
    internal var profileViewController:ProfileViewController?
    internal var loadingView:UIView?
    internal var appearWithOpenKeyboard:Bool = false
    internal var showInputComment:Bool = true

    fileprivate var messageSendButtonColor:UIColor?
    
    fileprivate let commentsLoadInterval = 15.0
    fileprivate var canCommentLoadInterval:Bool = true
    enum TableSection:Int {
        case header = 0
        case likeButton = 1
        case comments = 2
    }

    private var analytics : AnalyticsProtocol
    
    private var establishmentDetailsCoordinator: EstablishmentDetailsCoordinating?
    
    var likeFeedDetail:LikeFeedDetailCellNode?
    var auth: PPAuth?
    
    // MARK: - Initializer
    
    init(model:FeedDetailViewModel, analytics: AnalyticsProtocol = Analytics.shared) {
        self.model = model
        self.tableNode = ASTableNode()
        
        self.messageComposerView = Bundle.main.loadNibNamed("MessageComposerView", owner: nil, options: nil)![0] as! MessageComposerView
        let node = ASScrollNode()
        node.backgroundColor = Palette.ppColorGrayscale000.color
        self.analytics = analytics
        super.init(node: node)
    
        self.navigationItem.title = "Transação"
        self.model.delegate = self
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "FeedDetailViewControllerReloadFeedItem"), object: nil)
    }
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        model.externalTransferEventHandler(event: .cardDetailViewed)
        //Keyboard observers
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidHide), name: UIResponder.keyboardDidHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidChangeFrameNotification), name: UIResponder.keyboardDidChangeFrameNotification, object: nil)

        self.loadingView = Loader.getLoadingView("")
        self.loadingView?.isHidden = true
        self.view.addSubview(self.loadingView!)
        
        if self.model.feedItemIdForDownload != nil {
            self.model.canLoadComments = false
            self.configureTableView()
            self.loadFeedItem()
        }else{
            self.configureTableView()
            self.setup()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(pushReceivedHandler), name: NSNotification.Name(rawValue: "PushReceived"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadFeedItem), name: NSNotification.Name(rawValue: "FeedDetailViewControllerReloadFeedItem"), object: nil)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        identifyStyle(with: previousTraitCollection)
    }
    
    private func identifyStyle(with previousTraitCollection: UITraitCollection?) {
        if #available(iOS 13.0, *) {
            guard let previousTraitCollection = previousTraitCollection,
                previousTraitCollection.userInterfaceStyle != self.traitCollection.userInterfaceStyle else {
                return
            }
            tableNode.reloadData()
        }
    }
    
    @objc func reloadFeedItem(_ notification: Notification) {
        guard let feedItem = notification.userInfo?["feedItem"] as? DefaultFeedItem else {
            return
        }
        self.model.feedItem = feedItem
        self.tableNode.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.messageComposerView.isHidden = !showInputComment
        if navigationBarIsTransparent() {
            self.navigationController?.isNavigationBarHidden = true
        } else {
            self.navigationController?.isNavigationBarHidden = false
        }
        self.tableNode.reloadData()
        
        if navigationBarIsTransparent() {
            self.navigationController?.setNavigationBarHidden(true, animated: animated)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AppParameters.global().currentOpenFeedItem = self.model.feedItem.id
        BreadcrumbManager.shared.addCrumb(String(describing: type(of: self)).components(separatedBy: ".").last ?? String(describing: type(of: self)), typeOf: .screen, withProperties: nil)
        self.configureMessageComposer()
        self.messageComposerView.isHidden = !showInputComment
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        canCommentLoadInterval = false
        AppParameters.global().currentOpenFeedItem = nil
        if shouldShowBarWhenDisappear() {
            self.navigationController?.setNavigationBarHidden(false, animated: animated)
            setShouldShowBarWhenDisappear(false)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        canCommentLoadInterval = false
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        AppParameters.global().currentOpenFeedItem = nil
    }
    
    // MARK: - Internal Methods
    
    /**
     Initial setup
     */
    func setup(){
        self.configureNavigationBar()
        self.startLoadMoreCommentsInterval()
        self.configureFeedItem()
    }
    
    /// Load item from service
    func loadFeedItem(){
        startLoading()
        self.model.loadFeedItem { [weak self] (error) in
            DispatchQueue.main.async(execute: {
                self?.tableNode.reloadData()
                self?.stopLoading()
                if error != nil {
                    if let err = error as NSError?, err.isConnectionError() {
                        // If the error is a connection error execute the request again
                        self?.loadFeedItem()
                    }else{
                        let optionsAlert = UIAlertController(title: nil, message: error?.localizedDescription, preferredStyle: .alert)
                        let actButton = UIAlertAction(title: "Ok", style: .default, handler: { [weak self] (alertAction) in
                            self?.dismiss(animated: true, completion: { 
                                
                            })
                        })
                        optionsAlert.addAction(actButton)
                        self?.present(optionsAlert, animated: true) {
                        }
                    }
                }else{
                    self?.setup()
                }
            })
        }
    }
    
    /**
     Start the spinner loading
     */
    func startLoading(){
        let view = UIView(frame:CGRect(x: 0,y: 0,width: self.view.frame.size.width, height: 100))
        let spinner = UIActivityIndicatorView(frame: CGRect(x: (self.view.frame.size.width/2.0) - 15.0,y: 35.0,width: 30.0,height: 30.0))
        spinner.color = Palette.ppColorGrayscale200.color
        view.addSubview(spinner)
        spinner.startAnimating()
        tableNode.view.tableHeaderView = view
    }
    
    /**
     Stop the spinner loading
     */
    func stopLoading(){
        tableNode.view.tableHeaderView = UIView(frame:CGRect(x: 0,y: 0,width: self.view.frame.size.width, height: 0))
    }
    
    /**
     Configure the feed table view
     */
    func configureTableView(){
        var insetBottom:CGFloat = 0
        
        if #available(iOS 11.0, *) {
            insetBottom = self.view.safeAreaInsets.bottom
        } else {
            // Fallback on earlier versions
        }
        
        tableNode.delegate = self
        tableNode.dataSource = self
        tableNode.view.allowsSelection = true
        tableNode.leadingScreensForBatching = 3
        tableNode.view.separatorStyle = .none
        tableNode.placeholderEnabled = true
        tableNode.backgroundColor = Palette.ppColorGrayscale000.color
        tableNode.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 55, right: 0)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        tableNode.view.addGestureRecognizer(tapGesture)
        //tableNode.view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        tableNode.view.keyboardDismissMode = .onDrag
        tableNode.style.preferredSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height - 60 - insetBottom)
        tableNode.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height - 60 - insetBottom)
        tableNode.style.preferredLayoutSize = ASLayoutSize(width: ASDimensionMake(CGFloat(self.view.frame.size.width)), height: ASDimensionMake(CGFloat(self.view.frame.size.height - 60 - insetBottom)))
        
        self.view.addSubnode(tableNode)
        
        self.loadingView = Loader.getLoadingView("")
        self.loadingView?.isHidden = true
        self.view.addSubview(self.loadingView!)
        
        // adjust table to tabbar
        if let tabbarController = self.tabBarController {
            let adjustForTabbarInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: tabbarController.tabBar.frame.height, right: 0);
            self.tableNode.contentInset = adjustForTabbarInsets;
            self.tableNode.view.scrollIndicatorInsets = adjustForTabbarInsets;
        }
    }
    
    /**
     Configure the feed item
    */
    func configureFeedItem(_ reload: Bool = false){
        if self.model.feedItem.actions.count == 0 {
            self.navigationItem.rightBarButtonItem = nil
        }
        
        if reload {
            self.tableNode.reloadRows(at: [IndexPath(row: 0, section: TableSection.header.rawValue)], with: .none)
        }
    }
    
    /**
     Configure the message input view
    */
    func configureMessageComposer(){

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
            self.messageComposerView.delegate = self
            var bottomInsetSafeArea: CGFloat
            
            if #available(iOS 11.0, *) {
                bottomInsetSafeArea = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0.0
            } else {
                bottomInsetSafeArea = 0
            }
            self.messageComposerView.frame = CGRect(x: 0,
                                                    y: self.messageComposerView.frame.origin.y - bottomInsetSafeArea,
                                                    width: self.view.bounds.width,
                                                    height: self.messageComposerView.bounds.height)
            let safeAreaView = UIView(frame: CGRect(x: 0,
                                                    y: self.messageComposerView.frame.origin.y + self.messageComposerView.bounds.height,
                                                    width: self.view.bounds.width,
                                                    height: 34))
            safeAreaView.backgroundColor = Palette.ppColorGrayscale200.color
            self.view.addSubview(safeAreaView)
            
            // disable send message button
            //self.messageComposerView.sendButton.enabled = false
            self.messageSendButtonColor = self.messageComposerView.sendButton.backgroundColor
            
            self.messageComposerView.layer.opacity = 0.0
            self.view.addSubview(self.messageComposerView)
            self.messageComposerView.keyboardHeight = 0.0
            self.messageComposerView.layoutSubviews()
            
            UIView.animate(withDuration: 0.25, animations: { 
                self.messageComposerView.layer.opacity = 1.0
            }, completion: { (complete) in
                if self.appearWithOpenKeyboard{
                    // SWIFT_3_BUG
                    //self.messageComposerView.startEditing()
                }
            })
            
        }
        
        
    }
    
    /**
     Configure the navigation bar
    */
    func configureNavigationBar(){
        self.navigationItem.title = "Transação"
        
        let optionsButton = UIBarButtonItem(title: "Opções", style: .plain, target: self, action: #selector(showOptions))
        self.navigationItem.rightBarButtonItem = optionsButton
    }
    
    /**
     Load comment data
     */
    func loadCommentData(_ tableNode: ASTableNode, context: ASBatchContext?, force:Bool = false, completition callback: @escaping (_ newItems:[FeedComment]) -> Void = { (_) in } ){
        if self.model.canLoadComments || force {
            self.model.canLoadComments = false
            
            // show for user that the app is lodding more data
            // this method need to perform on main thread
            if !force {
                self.startLoadingComment()
            }
            
            var animateLikeButton = false
            if self.model.feedItem.like?.text == ""{
                animateLikeButton = true
            }
            
            // Load more data
            self.model.loadMoreComments({ [weak self] (newItems, hasNext, error) in
                DispatchQueue.main.async {
                    guard let strongSelf = self else {
                        return
                    }
                    guard error == nil else {
                        context?.cancelBatchFetching()
                        return
                    }
                    
                    strongSelf.updateLikeButton(animateLikeButton)
                    
                    // filter for not duplicating comments in feed interface
                    let newItensFiltered = newItems.filter({ (comment) -> Bool in
                        if strongSelf.model.comments.contains(comment) {
                            return false
                        }
                        return true
                    })
                    
                    // update like Button
                    tableNode.performBatch(animated: true, updates: {
                      
                        // if empty new itens, return function
                        guard !newItensFiltered.isEmpty else {
                            return
                        }
                        var indexPaths = [IndexPath]()
                        let iniIndex = strongSelf.model.comments.count
                        let endIndex = iniIndex + max(0, newItensFiltered.count - 1)
                        
                        strongSelf.model.comments += newItensFiltered
                        
                        // insert new comments rows
                        if !newItensFiltered.isEmpty && iniIndex <= endIndex {
                            for i in iniIndex...endIndex{
                                indexPaths.append(IndexPath(row: i, section: TableSection.comments.rawValue))
                            }
                            tableNode.insertRows(at: indexPaths, with: .fade)
                        }
                    }, completion: { completed in
                        
                        if completed {
                            // complete load
                            context?.completeBatchFetching(true)
                            strongSelf.stopLoadingComment()
                            strongSelf.model.canLoadComments = hasNext
                            callback(newItensFiltered.isEmpty ? [] : newItensFiltered)
                        }
                    })
                }
            })
        }
    }
    
    /**
     Show the loading
    */
    func startLoadingComment(){
        DispatchQueue.main.async {
            // create a simple view with an animating indicator
            let view = UIView(frame:CGRect(x: 0,y: 0,width: self.view.frame.size.width,height: 60))
            view.backgroundColor = Palette.ppColorGrayscale000.color
            
            let indicatorView = UIActivityIndicatorView(frame: CGRect(x: self.view.frame.size.width/2 - 10, y: 20, width: 20, height: 20))
            indicatorView.color = Palette.ppColorGrayscale200.color
            view.addSubview(indicatorView)
            
            self.tableNode.view.tableFooterView = view
            indicatorView.startAnimating()
        }
    }
    
    func stopLoadingComment(){
        DispatchQueue.main.async {
            let view = UIView(frame:CGRect(x: 0,y: 0,width: self.view.frame.size.width,height: 60))
            self.tableNode.view.tableFooterView = view
        }
    }

    /// Add new comment
    ///
    /// - Parameter text: text of comment
    func sendNewComment(_ text:String){
        
        DispatchQueue.main.async {
            self.dismissKeyboard()
            var newComment: FeedComment?
            let indexPath = IndexPath(row: Int(self.model.comments.count), section: TableSection.comments.rawValue)
            self.tableNode.performBatchUpdates({ [weak self] in
                
                // add fake comment
                guard let strongSelf = self else {
                    return
                }
                newComment = strongSelf.model.insertSendingComment(text, indexPath: indexPath)
                strongSelf.tableNode.insertRows(at: [indexPath], with: .fade)
                
            }, completion: { [weak self] finished in
                
                if finished {
                    guard let strongSelf = self else {
                        return
                    }
                    strongSelf.tableNode.scrollToRow(at: indexPath, at: .bottom, animated: true)
                    strongSelf.model.sendNewComment(text, indexPath: indexPath, sendingComment: newComment, completion: { (comment, success, error) in
                        
                        // check if comment is success added
                        DispatchQueue.main.async {
                            if success {
                                strongSelf.tableNode.reloadRows(at: [indexPath], with: .none)
                            } else if let error = error {
                                strongSelf.model.removeSendingComment(from: indexPath)
                                strongSelf.tableNode.deleteRows(at: [indexPath], with: .fade)
                                AlertMessage.showAlert(error, controller: strongSelf)
                            }
                        }
                    })
                }
            })
        }
    }
    
    /// Update like buttonx
    func updateLikeButton(_ fade:Bool = false){
        // Fabric issue #3
        guard likeFeedDetail != nil else {
            return
        }
        
        // update like row
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0, section: TableSection.likeButton.rawValue)
            self.tableNode.reloadRows(at: [indexPath], with: fade ? .fade : .none)
        }
    }
    
    ///dismiss keyboard
    @objc func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    // Start a interval to load more comment
    func startLoadMoreCommentsInterval(){
        if canCommentLoadInterval {
            self.perform(#selector(executeLoadMoreCommentsInterval), with: self, afterDelay: commentsLoadInterval)
        }
    }
    
    // Load more comments
    @objc func executeLoadMoreCommentsInterval() {
        let tableHeight = self.tableNode.view.bounds.size.height
        let contentHeight = self.tableNode.view.contentSize.height
        
        DispatchQueue.main.async {
            // check if tableview is scrolled to bottom
            let insetHeight = self.tableNode.contentInset.bottom
            let yOffset = self.tableNode.contentOffset.y
            let yOffsetAtBottom = yOffset + tableHeight - insetHeight
            let needScrollToBoottom = yOffsetAtBottom > (contentHeight - 50)
            
            if self.canCommentLoadInterval {
                self.loadCommentData(self.tableNode, context: nil, force: true, completition: { [weak self] (newItems) in
                    DispatchQueue.main.async {
                        guard let strongSelf = self else {
            return
        }
                        // scroll only if news items are addctioned
                        if !newItems.isEmpty && !strongSelf.model.comments.isEmpty {
                            if needScrollToBoottom {
                                let lastIndexPath = IndexPath(row: strongSelf.model.comments.count - 1, section: TableSection.comments.rawValue)
                                strongSelf.tableNode.scrollToRow(at: lastIndexPath, at: .bottom, animated: true)
                            }
                        }
                        
                        strongSelf.startLoadMoreCommentsInterval()
                    }
                })
            }
        }
    }
    
    /**
     Handler para notificação de push
    */
    @objc func pushReceivedHandler(){
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        self.executeLoadMoreCommentsInterval()
    }
    
    
    // MARK: - User Actions
    
    /**
     change Status like
     */
    func like(_ like:Bool){
        UIView.animate(withDuration: 0.25, animations: { 
            self.likeFeedDetail?.likeTextNode?.layer.opacity = 0.5
            self.likeFeedDetail?.likeImageNode?.layer.opacity = 0.5
        }) 
        self.model.like(like) { [weak self] (likeCheck, success, error) in
            self?.updateLikeButton(true)
        }
        self.likeFeedDetail?.adjustStateLikeButton()
        //self.updateLikeButton()
    }

    
    /**
     Show the feed options
    */
    @objc func showOptions(){
        let optionsAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // build the feed actions
        for action in model.feedItem.actions {
            let style: UIAlertAction.Style = action.type == .Report ? .destructive : .default
            let alertAction = UIAlertAction(title: action.label, style: style) { [weak self] _ in
                self?.callAction(action)
            }
            
            optionsAlert.addAction(alertAction)
        }
        
        let cancelAction = UIAlertAction(title: DefaultLocalizable.btCancel.text, style: .cancel) { (alertAction) in }
        optionsAlert.addAction(cancelAction)

        model.externalTransferEventHandler(event: .detailOptions)
        self.present(optionsAlert, animated: true, completion: nil)
    }
    
    func callAction(_ action: FeedWidgetAction) {
        switch action.type {
        case .OpenP2PReceipt,
             .OpenPAVReceipt,
             .OpenCardDebitReceipt,
             .openStoreReceipt,
             .openPIXReceipt,
             .openPIXReturn,
             .openPIXReceiptReceivement,
             .OpenZonaAzulReceipt,
             .OpenZonaAzulCadReceipt,
             .OpenMembershipDetails,
             .p2pExternalTransferReceipt,
             .Report:
            handleId(action: action)
        case .MakeTransactionPrivate:
            makeTransactionPrivate()
        case .Generic:
            showGenericActionConfirmation(action)
        case .sendLink:
            shareLink(data: action.data)
        default:
            break
        }
    }
    
    private func shareLink(data: NSObject?) {
        guard
            let data = data as? [AnyHashable: Any],
            let message = data["message"] as? String
            else {
                return
        }
        let controller = UIActivityViewController(activityItems: [message], applicationActivities: nil)
        model.externalTransferEventHandler(event: .resendLink(origin: .detailOptions))
        present(controller, animated: true)
    }
    
    private func handleId(action: FeedWidgetAction) {
        guard
            let data = action.data as? [AnyHashable: Any],
            let id = data["Id"] as? String
            else {
                return
        }
        switch action.type {
        case .OpenP2PReceipt:
            openReceipt(id, type: .P2P)
        case .OpenPAVReceipt:
            openReceipt(id, type: .PAV)
        case .OpenCardDebitReceipt:
            openReceipt(id, type: .debitReceipt)
        case .openStoreReceipt:
            openReceipt(id, type: .store)
        case .openPIXReceipt:
            openReceipt(id, type: .pix)
        case .openPIXReceiptReceivement:
            openReceipt(id, type: .pixReceiver)
        case .OpenZonaAzulReceipt:
            openZonaAzulReceipt(id)
        case .OpenZonaAzulCadReceipt:
            openZonaAzulCadReceipt(id)
        case .OpenMembershipDetails:
            openSubscriptionDetails(id)
        case .p2pExternalTransferReceipt:
            openReceipt(id, type: .p2pExternalTransfer)
        case .openPIXReturn:
            loadPixTransactionDetails(id)
        case .Report:
            ReportManager.startReportFlow(from: self, type: .p2pFeedItem, referedId: id) { [weak self] error, _ in
                guard let self = self else { return }
                self.navigationController?.popToViewController(self, animated: true)
            }
        default:
            break
        }
    }
    
    /// Generic Actions
    func prepareForGenericAction(_ action:FeedWidgetAction) {
        if let data = action.data as? [AnyHashable: Any] {
            if action.auth {
                auth = PPAuth.authenticate({ [weak self] (pin, biometric) in
                    self?.callGenericActionWithData(data, pin: pin)
                }, canceledByUserBlock: { 
                    
                })
            }else{
                callGenericActionWithData(data)
            }
        }
    }
    
    func showGenericActionConfirmation(_ action:FeedWidgetAction){
        if let message =  action.message, message != "" {
            let refreshAlert = UIAlertController(title: action.label, message: message, preferredStyle: .alert)
            refreshAlert.addAction(UIAlertAction(title: "Sim", style: .default, handler: { [weak self] actionConfirmation in
                self?.prepareForGenericAction(action)
            }))
            refreshAlert.addAction(UIAlertAction(title: "Não", style: .cancel, handler: nil))
            present(refreshAlert, animated: true, completion: nil)
        }else{
            prepareForGenericAction(action)
        }
    }
    
    /**
     Open receipt P2P and PAV
    */
    func openReceipt(_ transactionId:String, type: ReceiptWidgetViewModel.ReceiptType){        
        let receiptModel = ReceiptWidgetViewModel(transactionId: transactionId, type: type)
        TransactionReceipt.showReceipt(viewController: self, receiptViewModel: receiptModel, feedItemId: model.feedItem.id)
    }
    
    /**
     Open zona Azul Receipt
     */
    func openZonaAzulReceipt(_ transactionId:String){
        /*if let zonaAzulReceipt = ViewsManager.paymentZonaAzulViewController(withIdentifier: "ZonaAzulReceiptViewController") as? ZonaAzulReceiptViewController {
            zonaAzulReceipt.zonaAzulId = transactionId
            self.navigationController?.pushViewController(zonaAzulReceipt, animated: true)
        }*/
    }
    
    /**
     Open zona Azul Cad Receipt
     */
    func openZonaAzulCadReceipt(_ transactionId:String){
        // TODO: Refazer recibo do zona azul
        /*if let pavReceipt = ViewsManager.paymentStoryboardViewController(withIdentifier: "PaymentReceipt") as? PaymentReceiptViewController {
            pavReceipt.transactionIdForDownload = transactionId
            pavReceipt.referedId = transactionId
            pavReceipt.navigationLevel = 1
            pavReceipt.storeProcessor = PPStoreProcessor.zonaAzul;
            self.navigationController?.pushViewController(pavReceipt, animated: true)
        }*/
    }
    
    func openContactProfile(_ contact: PPContact) {
        analytics.log(ProfileEvent.touchPicture(from: .feedDetail))
        guard let popup = NewProfileProxy.openProfile(contact: contact, parent: self) else {
            return
        }
        self.showPopup(popup)
    }
    
    func openStoreProfile(_ store: PPStore) {
        establishmentDetailsCoordinator = EstablishmentDetailsCoordinator(
            store: store,
            navigationController: navigationController,
            fromViewController: self,
            coordinatorFactory: PromotionsFactory()
        )

        establishmentDetailsCoordinator?.didFinishFlow = { [weak self] in
            self?.establishmentDetailsCoordinator = nil
        }

        establishmentDetailsCoordinator?.start()
    }
    
    func openDigitalGoodProfile(_ digitalGood: DGItem) {
        guard let parentVC = self.parent else {
            return
        }
        let popup = DGProfileViewController(digitalGood: digitalGood, controller: parentVC)
        self.showPopup(popup)
    }
    
    func openProducerProfile(_ id: String) {
        let popup = ProducerProfileViewController(parentViewController: self, model: ProducerViewModel(producerId: id), origin: .feed)
        self.showPopup(popup)
    }
    
    func openSubscriptionDetails(_ id: String) {
        let subscriptionDetails = SubscriptionViewController(model: nil)
        let navigation = PPNavigationController(rootViewController: subscriptionDetails)
        subscriptionDetails.load(subscriptionId: id)
        subscriptionDetails.onClose = { [weak subscriptionDetails] (_) in
           subscriptionDetails?.dismiss(animated: true, completion: nil)
        }
        self.navigationController?.present(navigation, animated: true, completion: nil)
    }

    func loadPixTransactionDetails(_ id: String) {
        let flow: PIXConsumerFlow = .refund(paymentOpening: PaymentPIXProxy(), transactionId: id)
        let flowCoordinator = PIXConsumerFlowCoordinator(originViewController: self,
                                                         originFlow: .undefined,
                                                         destination: flow)
        flowCoordinator.start()
    }
    
    func callGenericActionWithData(_ data:[AnyHashable: Any], pin: String? = nil) {
        startOverflowLoading()
        self.model.requestBehaviorWithData(data, pin: pin) { [weak self] (behavior, success, error) in
            guard let strongSelf = self else {
            return
        }
            strongSelf.stopOverflowLoading()
            if error != nil {
				strongSelf.auth?.handleError(error, callback: { (error) in
                    AlertMessage.showCustomAlertWithErrorUI(error, controller: strongSelf)
				})
            } else {
                strongSelf.prepareForExecuteBehavior(behavior)
            }
        }
    }
    
    func prepareForExecuteBehavior(_ behavior:FeedItemBehavior?) {
        if let alert = behavior?.alert {
            let alertController = UIAlertController(title: alert.title, message: alert.message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: alert.buttonTitle, style: .default, handler: { [weak self] (action) in
                self?.executeBehavior(behavior)
            }))
            present(alertController, animated: true, completion: nil)
        }else{
            executeBehavior(behavior)
        }
    }
    
    /**
     Execute a feed item behavior
     */
    func executeBehavior(_ behavior:FeedItemBehavior?) {
        guard let behaviorType = behavior?.type else {
            return
        }
        
        switch behaviorType {
        case .Reload:
            self.model.notifyFeedNeedsUpdate()
            self.navigationController?.popViewController(animated: true)
            
        case .ReloadItem:
            self.canCommentLoadInterval = false
            self.model.canLoadComments = false
            self.model.feedItemIdForDownload = self.model.feedItem.id
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            self.model.loadFeedItem({ [weak self] (error) in
                DispatchQueue.main.async {
                    self?.navigationItem.rightBarButtonItem?.isEnabled = true
                    if error == nil {
                        self?.model.canLoadComments = true
                        self?.canCommentLoadInterval = true
                        self?.model.feedItemIdForDownload = nil
                    }
                }
            })
            
        default:
            break
        }
    }

    /**
     Make private the current transaction
    */
    func makeTransactionPrivate(){
        let alert = UIAlertController(title: "Deseja tornar essa transação privada?", message: "Ao tornar a transação privada somente os participantes poderão vê-la e ela não poderá ser alterada para pública novamente.", preferredStyle: .alert)
        
        let actButton = UIAlertAction(title: "Sim", style: .default, handler: { [weak self] (alertAction) in
            self?.changeVisibility(.Private)
        })
        alert.addAction(actButton)
        
        let actCancel = UIAlertAction(title: "Não", style: .cancel) { (alertAction) in }
        alert.addAction(actCancel)
        self.present(alert, animated: true) {
            
        }
    }
    
    /**
     Change the feed item visibility
    */
    func changeVisibility(_ visibility:FeedItemVisibility){
        self.loadingView?.isHidden = false
        self.model.changeVisibility(visibility) { [weak self] (feedItem, success, error) in
            DispatchQueue.main.async(execute: {
                guard let strongSelf = self else {
            return
        }
                strongSelf.loadingView?.isHidden = true
                if error != nil {
                    AlertMessage.showAlert(withMessage: error?.localizedDescription, controller: strongSelf)
                }else{
                    // configure the new item to apply changes
                    strongSelf.configureFeedItem(true)
                }
            })
        }
    }
    
    
    /// Report the comment
    /// - Parameter id: comment ID
    func reportSheet(comment: FeedComment, index: IndexPath) {
        let alert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction.init(title: "Denunciar comentário", style: .default, handler: { [weak self] alert in
            guard let strongSelf = self else {
            return
        }
            ReportManager.startReportFlow(from: strongSelf, type: .comment, referedId: comment.id, { error, flow in
                strongSelf.navigationController?.popToViewController(strongSelf, animated: true)
                strongSelf.processCommentReport(comment: comment, at: index ,flow: flow)
            })
        }))
        alert.addAction(UIAlertAction.init(title: "Cancelar", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func processCommentReport(comment: FeedComment, at index: IndexPath, flow:PPReportFlow?){
        guard let flow = flow else {
            return
        }
        if flow.action == "HIDE_FOR_REPORTER" {
            comment.isVisible = false
            tableNode.reloadRows(at: [index], with: .top)
        }
    }
    
    func startOverflowLoading() {
        if let loadingView = self.loadingView {
            view.bringSubviewToFront(loadingView)
            self.loadingView?.layer.opacity = 0
            self.loadingView?.isHidden = false
            
            UIView.animate(withDuration: 0.25, animations: {
                self.loadingView?.layer.opacity = 0.5
            })
        }
    }
    
    func stopOverflowLoading() {
        UIView.animate(withDuration: 0.25, animations: {
            self.loadingView?.layer.opacity = 0
        }) { (_) in
            self.loadingView?.isHidden = true
        }
        
    }
}


extension FeedDetailViewController: ASTableDelegate, ASTableDataSource {
    // MARK: - ASTableDelegate, ASTableDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableNode(_ tableNode: ASTableNode, numberOfRowsInSection section: Int) -> Int {
        if section == 0 || section == 1{
            return self.model.feedItemIsLoading ? 0 : 1
        }else{
            return self.model.comments.count
        }
    }
    
    func tableNode(_ tableNode: ASTableNode, nodeForRowAt indexPath: IndexPath) -> ASCellNode {
        if indexPath.section == 0 {
            return DefaultFeedCellNode(feedItem:self.model.feedItem, delegate: self, detail: true)
        }
        
        else if indexPath.section == 1 {
            likeFeedDetail = LikeFeedDetailCellNode(feedItem:self.model.feedItem, delegate:self)
            return likeFeedDetail!
        }
        
        if self.model.comments.count > indexPath.row {
            let comment = self.model.comments[indexPath.row]
            return CommentCellNode(commentItem: comment, delegate:self)
        }
        
        return ASTextCellNode()
    }
    
    func tableNode(_ tableNode: ASTableNode, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section > 1 && self.model.comments.count > indexPath.row && FeatureManager.isActive(.featureReport) {
            reportSheet(comment: self.model.comments[indexPath.row], index: indexPath)
        }
        return false
    }
    
    func shouldBatchFetch(for tableView: ASTableView) -> Bool {
        return self.model.canLoadComments
    }
    
    func tableNode(_ tableNode: ASTableNode, willBeginBatchFetchWith context: ASBatchContext) {
        self.loadCommentData(tableNode, context: context)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.dismissKeyboard()
    }
}

extension FeedDetailViewController : MessageComposerViewDelegate {
    
    // MARK: - MessageComposerViewDelegate
    
    func messageComposerSendMessageClicked(withMessage message: String!) {
        if message.trimmingCharacters(in: CharacterSet(charactersIn: " ")) != ""{
            self.sendNewComment(message)
        }
    }
}

extension FeedDetailViewController : LikeFeedDetailCellNodeDelegate {
    
    // MARK: - LikeFeedDetailCellNodeDelegate
    
    func likeButtonDidTap(_ button:ASImageNode?, like:Bool){
        self.like(like)
    }
    
    func likeButtonDidTapOnText(_ textNode:ASTextNode?){
        let model = LikedContactListViewModel(feedId:self.model.feedItem.id)
        let controller = ViewsManager.socialStoryboardViewController(withIdentifier: "ContactListTableViewController") as? ContactListTableViewController
        controller?.model = model
        self.navigationController?.pushViewController(controller!, animated: true)
    }
}

extension FeedDetailViewController : CommentCellNodeDelegate, DefaultFeedCellNodeDelegate {
    
    
    // MARK: - CommentCellNodeDelegate, DefaultFeedCellNodeDelegate
    
    func commentDidTapOnProfile(_ contact: PPContact?) {
        // Open profile
        if let contactProfile = contact {
            self.openContactProfile(contactProfile)
        }
    }
    
    func feedDidTapOnProfile(_ contact: PPContact?) {
        // Open profile
        if let contactProfile = contact {
            self.openContactProfile(contactProfile)
        }
    }
    
    func feedDidTapOnStoreProfile(_ store: PPStore?) {
        if store != nil {
            self.openStoreProfile(store!)
        }
    }
    
    func feedDidTapOnDigitalGoodProfile(_ digitalGood: DGItem?) {
        guard let digitalGood = digitalGood else {
            return
        }
        self.openDigitalGoodProfile(digitalGood)
    }
    
    func feedDidTapOnProducerProfile(_ producerId: String) {
        self.openProducerProfile(producerId)
    }
    
    func feedDidTapOnP2MProfile(){
        P2MHelpers.presentScanner(from: self)
    }
    
    func feedDidTapOnBoletoProfile() {
        guard FeatureManager.isActive(.isNewBilletHubAvailable) else {
            DGHelpers.openBoletoFlow(viewController: self, origin: "feed")
            return
        }
        
        let hubViewController = BilletHubFactory.make(with: .feed)
        let navigation = UINavigationController(rootViewController: hubViewController)
        present(navigation, animated: true)
    }
    
    func feedDidTabOnButton(feedItemId: String, url: URL) {
        DeeplinkHelper.handleDeeplink(withUrl: url, from: self)
    }
}

extension FeedDetailViewController {
    
    // MARK: - Keyboard Methods
    
    @objc func keyboardDidHide(_ notification: Notification) {
        adjustKeyboardPosition(notification)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        self.messageComposerView.keyboardOffset = 64
        adjustKeyboardPosition(notification)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        self.messageComposerView.keyboardOffset = 64
        adjustKeyboardPosition(notification)
    }
    
    @objc func keyboardDidChangeFrameNotification(_ notification: Notification) {
        adjustKeyboardPosition(notification)
    }
    
    func adjustKeyboardPosition(_ keyboardNotification: Notification) {
        if let keyboardSize = (keyboardNotification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            var bottomInset: CGFloat = 0
            if #available(iOS 11.0, *) {
                bottomInset = view.safeAreaInsets.bottom
            }
            self.messageComposerView.keyboardHeight = keyboardSize.height
            if bottomInset > 0 {
                self.messageComposerView.keyboardHeight -= (bottomInset / 4) + 2
            }
            self.messageComposerView.layoutSubviews()
        }
    }
}

extension FeedDetailViewController: ReportLoadingPresenter {
    func startReportLoading() {
        if let loadingView = self.loadingView {
            view.bringSubviewToFront(loadingView)
            self.loadingView?.layer.opacity = 0
            self.loadingView?.isHidden = false
            
            UIView.animate(withDuration: 0.25, animations: {
                self.loadingView?.layer.opacity = 0.5
            })
        }
    }
    
    func stopReportLoading() {
        UIView.animate(withDuration: 0.25, animations: {
            self.loadingView?.layer.opacity = 0
        }) { (_) in
            self.loadingView?.isHidden = true
        }
        
    }
}
