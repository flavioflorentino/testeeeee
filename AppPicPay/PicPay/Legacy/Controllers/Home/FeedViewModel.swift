import AnalyticsModule
import UIKit
import FeatureFlag
import Core

class FeedViewModel {
    typealias Dependencies = HasFeatureManager & HasAnalytics

    // MARK: - Properties
    private let dependencies: Dependencies

    private let minimumShiftToConsiderScroll: CGFloat = UIScreen.main.bounds.height / 2
    private var lastContentOffsetY: CGFloat = 0

    private lazy var opportunityService: OpportunityServicing = OpportunityService()

    var items:[FeedItem]
    var hasNext:Bool = false
    var canLoadMoreFeed:Bool = false
    var isLoading:Bool = false
    var currentRequestId:Int = 0
    var visibility:Visibility = Visibility.followers
    var targetId:String? = nil
    var targetType:String? = nil
    let lockQueueFeed:DispatchQueue
    let lockQueueLikeFeed:DispatchQueue
    var isFirstPage:Bool = true

    enum Visibility:Int{
        case personal = 2
        case followers = 3
    }
        
    init(dependencies: Dependencies = DependencyContainer()) {
        self.items = [FeedItem]()
        self.lockQueueFeed = DispatchQueue(label: "com.picpay.feed", attributes: [])
        self.lockQueueLikeFeed = DispatchQueue(label: "com.picpay.feed.like", attributes: [])
        self.dependencies = dependencies
    }
    
    func clearFeed(){
        self.items = [FeedItem]()
    }
    
    /**
     Load more feed items from webservice (Infinite scroll)
     */
    func loadMoreFeed(_ reload:Bool, onCacheLoaded: ((_ newItems:[FeedItem], _ hasNext:Bool) -> Void)?, callback:@escaping (_ newItems:[FeedItem], _ hasNext:Bool, _ error:Error?) -> Void , blockRequest:@escaping () -> Void){
        self.isFirstPage = reload
        let requestId = Int(currentRequestId + 1)
        currentRequestId = currentRequestId + 1
        
        // get last fetched feed
        var lastId = ""
        if !reload && self.items.count > 0 {
            lastId = self.items.last.map { $0.id } ?? ""
        }

        WSNewFeed.feeds(
            lastId,
            visibility: visibility.rawValue,
            targetId: self.targetId,
            targetType: self.targetType,
            onCacheLoaded: { [weak self] feedItems, hasNext in
                self?.feedCacheDidLoad(feedItems: feedItems, hasNext: hasNext, onCacheLoaded)
            }, completion: { [weak self] feedItems, hasNext, error in
                guard self?.currentRequestId == requestId else {
                    return
                }

                self?.feedDidLoad(feedItems: feedItems, hasNext: hasNext, error: error, callback)
        })
    }
    
    func paymentRequestPayerGenerateUrl(_ feedItem: FeedItem) -> URL? {
        guard
            let feedItem = feedItem as? DefaultFeedItem,
            let data = feedItem.actions.first?.data as? NSDictionary,
            let urlString = data["deeplink"] as? String,
            let url = URL(string: urlString)
            else {
                return nil
        }
        return url
    }
    
    func externalTransferEventHandler(feedItem: DefaultFeedItem, event: ExternalTransferUserEvent) {
        guard feedItem.type == "ExternalTransferFeedItem" else {
            return
        }
        switch event {
        case .cardClicked:
            externalTransferCardClickedEvent(feedItem: feedItem)
        default:
            dependencies.analytics.log(event)
        }
    }
    
    private func externalTransferCardClickedEvent(feedItem: DefaultFeedItem) {
        guard let status = ExternalTransferStatus.convert(type: feedItem.alert?.type) else {
            return
        }

        dependencies.analytics.log(ExternalTransferUserEvent.cardClicked(status: status))
    }
    
    /**
     Replace the item at list
    */
    func updateFeedItem(_ newFeedItem:FeedItem, completion:((_ index:Int?) -> Void) ) {
        self.lockQueueFeed.sync { 
            var feedIndex:Int?
            for feedItem in items{
                if feedItem.isEqual(newFeedItem){
                    if let index = self.items.firstIndex(of: feedItem){
                        feedIndex = index
                        break
                    }
                }
            }
            if feedIndex != nil {
                self.items[feedIndex!] = newFeedItem
            }
            
            completion(feedIndex)
        }
    }
    
    /**
     Change like status
     */
    func like(_ like:Bool, feedItem:FeedItem, completion callback:@escaping (_ likeCheck:TextCheckFeedWidget?, _ success:Bool, _ error:Error?) -> Void ){
        self.lockQueueFeed.sync {
            WSNewFeed.like(feedItem.id, like: like) {(likeCheck, success, error) in
                callback(likeCheck, success, error)
            }
        }
    }
    
    func sendAnalyticsLogForFeedDidAppear() {
        switch visibility {
        case .followers:
            dependencies.analytics.log(
                FeedEvent.didAppear(
                    numberOfActivies: items.count,
                    opportunitiesActive: dependencies.featureManager.isActive(.feedWithOpportunityCells)
                )
            )
        default:
            return
        }
    }
}

private extension FeedViewModel {
    var canAggregateOpportunityCards: Bool {
        isFirstPage && visibility == .followers && dependencies.featureManager.isActive(.feedWithOpportunityCells)
    }

    func feedCacheDidLoad(
        feedItems: [FeedItem]?,
        hasNext: Bool,
        _ onCacheLoaded: ((_ newItems: [FeedItem], _ hasNext: Bool) -> Void)?
    ) {
        guard let feedItems = feedItems, items.isEmpty else {
            return
        }

        guard
            dependencies.featureManager.isActive(.featureOpportunitiesCardsFromRemote) == false,
            canAggregateOpportunityCards
            else {
                onCacheLoaded?(feedItems, hasNext)
                return
        }

        onCacheLoaded?(aggregateCustomFeedItems(to: feedItems), hasNext)
    }

    func feedDidLoad(
        feedItems: [FeedItem]?,
        hasNext: Bool,
        error: Error?,
        _ callback: @escaping (_ newItems: [FeedItem], _ hasNext: Bool, _ error: Error?) -> Void
    ) {
        guard canAggregateOpportunityCards else {
            return callback(feedItems ?? [], hasNext, error)
        }

        guard dependencies.featureManager.isActive(.featureOpportunitiesCardsFromRemote) else {
            return callback(aggregateCustomFeedItems(to: feedItems ?? []), hasNext, error)
        }

        aggregateWithOpportunites(feedItems: feedItems ?? [], hasNext: hasNext, error: error, callback)
    }

    func aggregateCustomFeedItems(to feedItems: [FeedItem]) -> [FeedItem] {
        FeedOpportunityCellsHelper().mergeOpportunities(withFeedItems: feedItems)
    }

    func aggregateWithOpportunites(feedItems: [FeedItem],
                                   hasNext: Bool,
                                   error: Error?,
                                   _ callback: @escaping (_ newItems: [FeedItem], _ hasNext: Bool, _ error: Error?) -> Void) {
        if dependencies.featureManager.isActive(.releaseOpportunityCardsV2) {
            opportunityService.opportunitiesV2 { [weak self] result in
                self?.processOpportunityServiceResult(feedItems, result, hasNext, error, callback)
            }
        } else {
            opportunityService.opportunities { [weak self] result in
                self?.processOpportunityServiceResult(feedItems, result, hasNext, error, callback)
            }
        }
    }
    
    func processOpportunityServiceResult(_ feedItems: [FeedItem],
                                         _ result: Result<[Opportunity], ApiError>,
                                         _ hasNext: Bool,
                                         _ error: Error?,
                                         _ callback: @escaping (_ newItems: [FeedItem], _ hasNext: Bool, _ error: Error?) -> Void) {
        switch result {
        case let .success(opportunities):
            let opportunityItemsHelper = FeedOpportunityCellsHelper(opportunities: opportunities)
            let items = opportunityItemsHelper.mergeOpportunities(withFeedItems: feedItems)
            callback(items, hasNext, error)
        case .failure:
            callback(feedItems, hasNext, error)
        }
    }
}
