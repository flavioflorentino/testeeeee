import AnalyticsModule
import UIKit

protocol FeedDetailViewModelDelegate: AnyObject {
    
}

final class FeedDetailViewModel: NSObject {
    
    weak var delegate:FeedDetailViewModelDelegate?
    
    var comments:[FeedComment]
    var feedItem:DefaultFeedItem
    var feedItemIdForDownload:String?
    var canLoadComments:Bool = true
    var commentMembers:[AnyHashable: Any]
    var likeIsLoading:Bool = false
    var feedItemIsLoading:Bool = false
    
    @objc init(feedItemId:String) {
        self.feedItemIdForDownload = feedItemId
        self.feedItem = DefaultFeedItem()
        self.commentMembers = [AnyHashable: Any]()
        self.comments = [FeedComment]()
        super.init()
    }

    
    @objc init(feedItem:DefaultFeedItem) {
        self.commentMembers = [AnyHashable: Any]()
        self.feedItem = feedItem
        comments = [FeedComment]()
        super.init() 
    }
    
    /**
     Load the item using feedItemIdFOrDownload
    */
    func loadFeedItem(_ callback:@escaping (_ error:Error?) -> Void) {
        self.feedItemIsLoading = true
        if let feedItemId = self.feedItemIdForDownload {
            WSNewFeed.feed(feedItemId, completion: { [weak self] (item, error) in
                if item != nil {
                    self?.feedItemIsLoading = false
                    self?.canLoadComments = true
                    if let feedItem = item as? DefaultFeedItem{
                       self?.feedItem = feedItem
                    }
                    
                }
                callback(error)
            })
        }
    }
    
    
    /**
     Load more comments items from webservice (Infinite scroll)
     */
    func loadMoreComments(_ callback:@escaping (_ newItems:[FeedComment], _ hasNext:Bool, _ error:Error?) -> Void ){
        var startId = ""
        if self.comments.count > 0{
            if let lastId = self.comments.last?.id{
                startId = lastId
            }
        }
        
        WSNewFeed.feedComments(self.feedItem.id, currentMembers: self.commentMembers, startId:startId) { [weak self] (comments, members, likes, hasNext, error) in
            if let m = members {
                self?.commentMembers = m
            }
            
            if likes != nil {
                self?.feedItem.like = likes
            }
            
            if let items = comments {
                callback(items, hasNext, error)
            }else{
                callback([FeedComment](), hasNext, error)
            }
        }
    }
    
    func externalTransferEventHandler(event: ExternalTransferUserEvent) {
        if feedItem.type == "ExternalTransferFeedItem" {
            Analytics.shared.log(event)
        }
    }
    
    func insertSendingComment(_ text:String, indexPath:IndexPath) -> FeedComment {
        // create contact for authenticated user
        let contact = PPContact()
        contact.wsId = (ConsumerManager.shared.consumer?.wsId)!
        contact.onlineName = ConsumerManager.shared.consumer?.name
        contact.imgUrl = ConsumerManager.shared.consumer?.img_url
        contact.username = ConsumerManager.shared.consumer?.username
        
        // create new fake comment
        let consumerId = String(format:"%ld",contact.wsId)
        let newComment = FeedComment(text: text, consumerId: consumerId, timeAgo: "Enviando...", profile: contact)
        
        self.comments.insert(newComment, at: indexPath.row)

        return newComment
    }
    
    func removeSendingComment(from indexPath: IndexPath) {
        self.comments.remove(at: indexPath.row)
    }
    
    /**
    Create a new comment
    */
    func sendNewComment(_ text:String, indexPath:IndexPath, sendingComment:FeedComment?,  completion callback:@escaping (_ comment:FeedComment?, _ success:Bool, _ error:Error?) -> Void ){
        
        //Call server
        WSNewFeed.addComment(self.feedItem.id,text:text) { [weak self] (comment, numComments, success, error) in
            if let com = comment {
                // update the fake comment with the real inserted comment
                sendingComment?.populate(com)
                self?.feedItem.comments?.value = numComments
                self?.notifyItemHasUpdate()
            }
            callback(comment, success, error)
        }
    }
    
    /**
     Change like status
     */
    func like(_ like:Bool, completion callback:@escaping (_ likeCheck:TextCheckFeedWidget?, _ success:Bool, _ error:Error?) -> Void ){
        if !self.likeIsLoading {
            self.likeIsLoading = true
            //self.feedItem.like?.text = "Atualizando..."
            WSNewFeed.like(self.feedItem.id, like: like) { [weak self] (likeCheck, success, error) in
                if likeCheck != nil{
                    self?.feedItem.like = likeCheck
                }else{
                    if let errorText = error?.localizedDescription{
                        self?.feedItem.like?.text = errorText
                    }
                }
                self?.likeIsLoading = false
                self?.notifyItemHasUpdate()
                callback(likeCheck, success, error)
            }
        }
    }
    
    /**
     Change the feed privacy
    */
    func changeVisibility(_ privacy:FeedItemVisibility, completion callback:@escaping (_ feedItem:DefaultFeedItem?, _ success:Bool, _ error:Error?) -> Void ) {
        WSNewFeed.changeVisibility(self.feedItem.id, visibility: privacy) { [weak self] (feedItem, success, error) in
            if let feed = feedItem{
                self?.feedItem = feed
                self?.notifyItemHasUpdate()
            }
            callback(feedItem, success, error)
        }
    }
    
    /**
     Notify the feed list that the feed has updated
     */
    func notifyItemHasUpdate(){
        // ---------- NOTIFICATION ---------------
        // notifies that the feed item needs update
        NotificationCenter.default.post(name: Notification.Name(rawValue: FeedViewController.Observables.NeedReloadItem.rawValue), object: nil, userInfo: ["feedItem":self.feedItem])
        // ------------------------------------
    }
    
    /**
     Request a feed item behavior
     */
    func requestBehaviorWithData(_ data:[AnyHashable: Any], pin: String?, completion callback:@escaping (_ behavior:FeedItemBehavior?, _ success:Bool, _ error:Error?) -> Void) {
        var dataParams = data
        if let pin = pin {
            dataParams["pin"] = pin
        }
        WSFeedItemAction.requestBehavior(dataParams) { (success, error, behavior) in
            DispatchQueue.main.async {
                callback(behavior, success, error)
            }
        }
    }
    
    /**
     Notify feed needs update
     */
    func notifyFeedNeedsUpdate() {
        // ---------- NOTIFICATION ---------------
        // notifies that the feed item needs update
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: FeedViewController.Observables.FeedNeedReload.rawValue), object: nil)
        // ------------------------------------
    }
}
