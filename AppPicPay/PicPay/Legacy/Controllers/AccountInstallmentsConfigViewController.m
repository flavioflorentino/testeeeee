#import <UI/UI.h>
#import "AccountInstallmentsConfigViewController.h"
#import "Loader.h"
#import "WSParcelamento.h"
#import "PicPay-Swift.h"
#import <AnalyticsModule/AnalyticsModule.h>
@import AnalyticsModule.Swift;

@interface AccountInstallmentsConfigViewController ()

@end

@implementation AccountInstallmentsConfigViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	loaderView = [[UILoadView alloc] initWithSuperview:self.view position:PositionTop activityStyle:UIActivityIndicatorViewStyleGray];
	loaderView.hidden = NO;
	
	UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Salvar" style:UIBarButtonItemStylePlain target:self action:@selector(save)];
	saveButton.enabled = NO;
	self.navigationItem.rightBarButtonItem = saveButton;
	
    AccountInstallmentsConfigViewController *selfweak = self;
	[WSParcelamento getMaximumTaxesFreeInstallments:^(NSArray *wsInstallments, NSString *text, NSError *error) {
		installments = wsInstallments;
		
		dispatch_async(dispatch_get_main_queue(), ^(void) {
			
            [loaderView stopLoadingWithAnimated:TRUE];
            [selfweak configureTableViewHeader:text];
			[selfweak.tableView reloadData];
			
		});
	}];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [installments count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	NSArray *installment = [installments objectAtIndex:indexPath.row];
	
	static NSString *CellIdentifier;
	
	if ([[installment valueForKey:@"selected"] boolValue] == NO) {
		CellIdentifier = @"Cell";
	}else{
		CellIdentifier = @"CellSelected";
	}
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

	int cellStyle;
	
	cellStyle = UITableViewCellStyleDefault;
	
	if ([installment valueForKey:@"subtitle"]) {
		cellStyle = UITableViewCellStyleSubtitle;
	}
	
	if (!cell) {
		cell = [[UITableViewCell alloc] initWithStyle:cellStyle reuseIdentifier:CellIdentifier];
	}
	
	cell.textLabel.text = [installment valueForKey:@"title"];
	cell.detailTextLabel.text = [installment valueForKey:@"subtitle"];
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
	
	self.navigationItem.rightBarButtonItem.enabled = YES;
	
	for (NSArray *installment in installments) {
		[installment setValue:@"0" forKey:@"selected"];
	}
	
	[[installments objectAtIndex:indexPath.row] setValue:@"1" forKey:@"selected"];
	selectedInstallmentToSave = [installments objectAtIndex:indexPath.row];
	[self.tableView reloadData];
}

- (void)save{
	
	if (selectedInstallmentToSave) {
        [OBJCAnalytics logWithName:@"PRO - Interest-free installments Enabled" properties:@{ @"installments": [selectedInstallmentToSave valueForKey:@"value"] }];
        
		loaderView.hidden = NO;
		[WSParcelamento setMaximumTaxesFreeInstallments:[selectedInstallmentToSave valueForKey:@"value"] completionHandler:^(NSArray *wsInstallments, NSString *text, NSError *error) {;
			installments = wsInstallments;
			
			dispatch_async(dispatch_get_main_queue(), ^(void) {
				
				[[self navigationController] popViewControllerAnimated:YES];
				
			});
		}];
	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**!
 * set the text on tableView header
 */
- (void) configureTableViewHeader:(NSString *) text {
    
    UILabel *label = [[UILabel alloc] init];
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 0;
    label.text = text;
    label.font = [UIFont systemFontOfSize:14];
    
    // Adjust header size
    CGSize maximumLabelSize = CGSizeMake(self.view.frame.size.width - 30, CGFLOAT_MAX);
    CGRect textRect = [text boundingRectWithSize:maximumLabelSize
                                                          options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
                                                       attributes:@{NSFontAttributeName:label.font}
                                                          context:nil];
    
    label.frame = CGRectMake(15, 10, self.view.frame.size.width - 30, textRect.size.height);
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, textRect.size.height + 20.0)];
    [view addSubview:label];
    view.backgroundColor = [PaletteObjc ppColorGrayscale000];
    self.tableView.tableHeaderView = view;
}

@end
