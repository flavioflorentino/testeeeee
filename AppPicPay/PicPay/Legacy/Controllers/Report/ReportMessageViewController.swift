import UI
import UIKit

final class ReportMessageViewController: ReportBaseViewController {
    
    @IBOutlet weak var actionButton: UIPPButton!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var messageCountLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    let messagePlaceholder = Strings.Legacy.tellUsHere
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
    }
    
    // MARK: - Internal Methos
    
    func configureView(){
        let buttonText = model?.flow.buttonText ?? "Enviar"
        actionButton.setTitle(buttonText, for: .normal)
        actionButton.disabledBackgrounColor = Colors.grayscale100.color
        actionButton.normalBackgrounColor = Colors.criticalBase.color
        actionButton.disabledTitleColor = Colors.grayscale400.color
        actionButton.normalTitleColor = Colors.white.lightColor
        
        textView.delegate = self
        textView.text = messagePlaceholder
        
        if let model = model {
            titleLabel.text = model.flow.title
            textLabel.text = model.flow.text
            title = model.flow.screenTitle
        }
    }
    
    // MARK: - User Actions
    
    @IBAction private func doAction(_ sender: Any){
        guard let model = model else {
            return
        }
        
        if textView.text == messagePlaceholder {
            AlertMessage.showAlert(withMessage: "Por favor, digite uma mensagem e tente novamente", controller: self)
            return
        }
        
        model.data.message = textView.text
        
        textView.resignFirstResponder()
        ReportManager.nextFlowStep(from: self, model: model, next: model.flow.next)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
}

extension ReportMessageViewController : UITextViewDelegate {
    
    func moveCursorToStart() {
        // Set cursor to the start of the text view
        let startPosition: UITextPosition = textView.beginningOfDocument
        textView.selectedTextRange = textView.textRange(from: startPosition, to: startPosition)
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if textView.text == messagePlaceholder {
            moveCursorToStart()
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        messageCountLabel.text = "\(numberOfChars)/200"
        
        // check placeholder
        if textView.text == messagePlaceholder && textView.text != newText {
            textView.text = text
            textView.textColor = Palette.ppColorGrayscale600.color
            textView.isSelectable = true
            return false
        }else if newText.count == 0 && textView.text.count > 0{
            textView.text = messagePlaceholder
            textView.textColor = UIColor(red: 157.0/255.0, green: 157.0/255.0, blue: 157.0/255.0, alpha: 1.0)
            textView.isSelectable = false
            moveCursorToStart()
            return false
        }
        
        return numberOfChars < 200;
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        UIView.animate(withDuration: 0.25) {
            self.scrollView.contentOffset.y = textView.frame.origin.y - 10
        }
        
        if textView.text == messagePlaceholder {
            moveCursorToStart()
        }
    }
}
