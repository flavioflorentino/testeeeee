//
//  ReportOptionsTableViewController.swift
//  PicPay
//
//  ☝️☁️ Created by Luiz Henrique Guimarães on 26/06/17.
//
//

import UIKit

final class ReportOptionsViewController : ReportBaseViewController { 
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var headTextLabel: UILabel!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = model?.flow.screenTitle
        configureTableView()
    }

    // MARK: - Configure
    
    func configureTableView(){
        tableView.delegate = self
        tableView.dataSource = self
    }
}

extension ReportOptionsViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let model = model else { return 0 }
        return model.flow.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell"),
            let model = model else {
                return UITableViewCell()
            }
        
        let item = model.flow.items[indexPath.row]
        
        cell.textLabel?.text = item.title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let model = model else {
            return
        }
        
        let selectedItem = model.flow.items[indexPath.row]
        ReportManager.nextFlowStep(from: self, model: model, next: selectedItem.next)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }

}
