//
//  ReportViewModel.swift
//  PicPay
//
//  ☝️☁️ Created by Luiz Henrique Guimarães on 26/06/17.
//
//

import Foundation

final class ReportViewModel {
    var flow: PPReportFlow
    var step: Int = 0
    var data: PPReportRequest
    var completion: ((_ error:Error?, _ flow: PPReportFlow) -> Void)?
    
    init(flow: PPReportFlow, data: PPReportRequest, completion: ((_ error:Error?, _ flow: PPReportFlow) -> Void)? = nil){
        self.flow = flow
        self.data = data
        self.completion = completion
    }
    
    
    func sendReport(step:PPReportFlow,  _ completion: @escaping ((_ step: PPReportFlow, _ sucess:Bool, _ error:Error?) -> Void) ){
        
        // get data form
        data.action = step.action ?? ""
        if let type = step.reportType {
            data.reportType = type
        }
        data.tag = step.tag
        
        WSReport.reportAbuse(data) { (success, error) in
            DispatchQueue.main.async {
                completion(step, success, error)
            }
        }
    }
}


