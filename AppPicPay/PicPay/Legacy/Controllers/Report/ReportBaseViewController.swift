//
//  ReportBaseViewController.swift
//  PicPay
//
//  ☝️☁️ Created by Luiz Henrique Guimarães on 27/06/17.
//
//

import UIKit

class ReportBaseViewController: PPBaseViewController, ReportLoadingPresenter {
    
    var model: ReportViewModel?
    var loadingView: UIView?
    
    @IBOutlet weak var viewBottomConstraint: NSLayoutConstraint!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let title = model?.flow.screenTitle {
            navigationItem.title = title
        }
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Voltar", style: .plain, target: nil, action: nil)
        
        // Keyboard events observers
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillOpen), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    // MARK: - Public Methods
    
    func startReportLoading(){
        if loadingView == nil {
            loadingView = Loader.getLoadingView("")
            loadingView?.isHidden = true
            view.addSubview(loadingView!)
        }
        
        loadingView?.isHidden = false
    }
    
    func stopReportLoading(){
        loadingView?.isHidden = true
    }
    
    // MARK: - Keyboard Events Handler
    
    @objc func keyboardWillOpen(_ keyboardNotification:Notification){
        guard
            let animationDuration = ((keyboardNotification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey]) as? NSNumber)?.doubleValue,
            let keyboardRectValue = (keyboardNotification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            else {
            return
        }
            
        // animate the view to adjust height with the opened keyboard
        self.view.layoutIfNeeded()
        self.viewBottomConstraint?.constant = keyboardRectValue.height
        
        UIView.animate(withDuration: animationDuration, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func keyboardWillHide(_ keyboardNotification:Notification){
        guard
            let animationDuration = ((keyboardNotification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey]) as? NSNumber)?.doubleValue
            else {
            return
        }
        // animate the view to adjust height with the opened keyboard
        self.view.layoutIfNeeded()
        self.viewBottomConstraint?.constant = 0.0
        
        UIView.animate(withDuration: animationDuration, animations: {
            self.view.layoutIfNeeded()
        })
    }

    
}
