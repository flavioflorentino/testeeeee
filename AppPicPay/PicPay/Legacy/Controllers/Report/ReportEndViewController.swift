//
//  ReportEndViewController.swift
//  PicPay
//
//  ☝️☁️ Created by Luiz Henrique Guimarães on 23/06/17.
//
//

import UIKit

final class ReportEndViewController: ReportBaseViewController { 
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollViewVerticalCenter!
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Concluir", style: .plain, target: self, action: #selector(close(_:)))
        self.navigationItem.setHidesBackButton(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        scrollView.centerContentView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scrollView.centerContentView()
    }
    
    // MARK: - Internal Functions
    
    func configureView() {
        guard let model = self.model else {
            return
        }
        
        title = model.flow.screenTitle
        titleLabel.text = model.flow.title
        textLabel.text = model.flow.text
        
        self.actionButton.isHidden = true
        
        scrollView.centerContentView()
    }
    
    // MARK: - User Actions
    
    @IBAction private func close(_ sender: Any) {
        guard let model = self.model else {
            return
        }
        model.completion?(nil, model.flow)
    }
    
}
