//
//  PPPaymentManager.m
//  PicPay
//
//  Created by Diogo Carneiro on 24/10/14.
//
//

#import "PPPaymentManager.h"
#import "NSDecimalNumber+PPDecimalNumber.h"
#import "PicPay-Swift.h"

@import CoreLegacy;
@import FeatureFlag;

@implementation PPPaymentManager

- (id)init {
    self = [super init];
    if (self != nil) {
        paymentConfig = [PPPaymentConfig shared];
        _calculator = [[PPPaymentCalculator alloc] init];
        _selectedQuotaQuantity = 1;
        _sellerLocalDiscount = [NSDecimalNumber zero];
        _installmentFee = [[NSDecimalNumber alloc] initWithDouble:3.49];
        _cardFee = [NSDecimalNumber zero];
        _fixedFee = [NSDecimalNumber zero];
    }
    return self;
}

- (void)changePaymentConfig:(PPPaymentConfig *)newConfig {
    paymentConfig = newConfig;
    [_calculator changePaymentConfig:paymentConfig];
}

- (NSDecimalNumber *)total {
    return [[self cardTotal] decimalNumberByAdding:[self balanceTotal]];
}

- (NSDecimalNumber *)cardTotal {
    if (_forceBalance) {
        return NSDecimalNumber.zero;
    }
    return [_calculator recalculatedValueForInstallmentsQuantity:_selectedQuotaQuantity
                                                      totalValue:[self cardTotalWithCardConvenienceFee]];
}

- (NSDecimalNumber *)cardTotalWithCardConvenienceFee {
    NSDecimalNumber *fee = [[self cardFee] decimalNumberByDividingBy:[NSDecimalNumber decimalNumberWithInt:100]];
    NSDecimalNumber *valueWithPercentageFee = [[self cardTotalWithoutInterest] decimalNumberByMultiplyingBy:[[NSDecimalNumber one] decimalNumberByAdding:fee]];
    return [valueWithPercentageFee decimalNumberByAdding:[self fixedFee]];
}

- (NSDecimalNumber *)cardTotalWithoutInterest {
    if (_forceBalance) {
        return NSDecimalNumber.zero;
    }
    return [self.subtotal decimalNumberBySubtracting:self.balanceTotal];
}

- (NSDecimalNumber *)cardConvenienceFee {
    NSDecimalNumber *cardFee = [self.cardFee decimalNumberByDividingBy:[NSDecimalNumber decimalNumberWithInt:100]];
    return [cardFee decimalNumberByMultiplyingBy:self.cardTotalWithoutInterest];
}

- (NSDecimalNumber *)installmentsTotalFee {
    switch ([self paymentMethod]) {
        case PaymentMethodCardAndBalance: {
            NSUInteger numberOfInstalments = _selectedQuotaQuantity;
            NSUInteger index = numberOfInstalments - 1;
            NSDecimalNumber *instalmentValue = [self installmentsList][index];
            NSDecimalNumber *instalmentFee = [_installmentFee decimalNumberByDividingBy:[NSDecimalNumber decimalNumberWithInt:100]];
            return [instalmentValue decimalNumberByMultiplyingBy:instalmentFee];
        }
            
        case PaymentMethodCard:
            return [self.total decimalNumberBySubtracting:self.cardTotalWithCardConvenienceFee];
            
        case PaymentMethodBalance:
            return [NSDecimalNumber zero];
    }
}

- (NSDecimalNumber *)balanceTotal {
    @try {
        NSDecimalNumber *userBalanceAndLocalCredit = [[self balance] decimalNumberByAdding:_sellerLocalDiscount];
        
        if ([ConsumerManager useBalance] && _forceCreditCard == NO && [[self balance] compare:[NSDecimalNumber zero]] == NSOrderedDescending) {
            if ([_subtotal compare:userBalanceAndLocalCredit] == NSOrderedDescending || [_subtotal compare:userBalanceAndLocalCredit] == NSOrderedSame) {
                
                return userBalanceAndLocalCredit;
            } else {
                return _subtotal;
            }
        } else {
            if ([_subtotal compare:_sellerLocalDiscount] == NSOrderedDescending || [_subtotal compare:_sellerLocalDiscount] == NSOrderedSame) {
                
                return _sellerLocalDiscount;
            } else {
                return _subtotal;
            }
        }
    }
    @catch (NSException *exception) {
        DebugLog(@"[PPPaymentManager balanceTotal] raised an exception: %@", exception);
    }
}

- (NSDecimalNumber *)quotaValue {
    return [_calculator installmentValueForInstallmentsQuantity:_selectedQuotaQuantity
                                                     totalValue:[self cardTotalWithCardConvenienceFee]];
}

- (NSDecimalNumber *)balance {
    return ConsumerManager.shared.consumer.balance;
}

- (BOOL)installmentEnabled {
    return [paymentConfig maxQuotaQuantity] > 1;
}

- (BOOL)installmentAvailable {
    return [self installmentEnabled] && [[self installmentsList] count] >= 1;
}

- (NSArray *)installmentsList {
    return [_calculator installmentsListForTotalValue: [self cardTotalWithCardConvenienceFee]];
}

- (BOOL)usePicPayBalance {
    if ( _forceCreditCard == YES) {
        return NO;
    }
    if ( _forceBalance == YES) {
        return YES;
    }
    return [ConsumerManager useBalance];
}

- (PPPaymentMethod)paymentMethod {
    NSDecimalNumber *balanceTotal = [self balanceTotal];
    NSDecimalNumber *cardTotal = [self cardTotal];
    NSDecimalNumber *zero = [NSDecimalNumber zero];
    
    if ([cardTotal compare:zero] != 0 && [balanceTotal compare:zero] != 0) {
        return PaymentMethodCardAndBalance;
    } else if ([cardTotal compare:zero] != 0 && [balanceTotal compare:zero] == 0) {
        return PaymentMethodCard;
    } else if ([cardTotal compare:zero] == 0 && [balanceTotal compare:zero] != 0) {
        return PaymentMethodBalance;
    } else if ([cardTotal compare:zero] != 0 && [self usePicPayBalance]) {
        return PaymentMethodBalance;
    } else if ([self usePicPayBalance]) {
        return PaymentMethodBalance;
    } else {
        return PaymentMethodCard;
    }
}

- (PPPaymentConfig *)config {
    return paymentConfig;
}

@end
