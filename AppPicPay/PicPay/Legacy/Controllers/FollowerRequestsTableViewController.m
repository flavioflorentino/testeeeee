#import <UI/UI.h>
#import "FollowerRequestsTableViewController.h"
#import "PicPay-Swift.h"
#import "PicPay-Swift.h"

@interface FollowerRequestsTableViewController () <FollowerTableViewCellDelegate, UIPPProfileImageDelegate>
    @property (strong) ProfileViewController* profileViewController;
    //Armazena as FollowerRequest que já foram ignoradas - E necessário no cell reuse para saber o estado de cada notificação.
    @property (strong, nonatomic) NSMutableDictionary *notificationsIgnored;
    @property BOOL shouldDismissIngoredNotifications;
@end

@implementation FollowerRequestsTableViewController

struct {
    unsigned int followerRequestWasIgnoredWithId:1;
    unsigned int followerRequestWasUpdated:1;
} delegateRespondsTo;

@synthesize delegate;

- (void)setDelegate:(id <FollowerRequestDelegate>)aDelegate {
    if (delegate != aDelegate) {
        delegate = aDelegate;
        
        delegateRespondsTo.followerRequestWasIgnoredWithId = [delegate respondsToSelector:@selector(followerRequestWasIgnoredWithId:)];
        delegateRespondsTo.followerRequestWasUpdated = [delegate respondsToSelector:@selector(followerRequestWasUpdated:)];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _shouldDismissIngoredNotifications = YES;
    
    //configure tableview cell
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100;
    UINib *followerRequestNib = [UINib nibWithNibName:@"FollowerTableViewCell" bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:followerRequestNib forCellReuseIdentifier:@"Follower"];
    
    self.notificationsIgnored = [[NSMutableDictionary alloc] init];
    //É necessário a cópia do objeto, há um momento que há diferença do estado das notificações das duas viewControllers.
    // -> Quando aqui é ignorada, na viewController anterior é removida, mas aqui sempre é mostrada.
    self.notifications = [NSMutableArray arrayWithArray: self.notifications];
    self.tableView.accessibilityIdentifier = @"Solicitações";
    self.tableView.tableFooterView = [[UIView alloc] init];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    _shouldDismissIngoredNotifications = YES;
    
    //Test With animation
    if ([[[NSProcessInfo processInfo] arguments] containsObject:@"followerUITestWithAnimation"]) {
        [UIView setAnimationsEnabled:true];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if (self.isMovingFromParentViewController) {
        //Voltando a navegação, verificando as linhas com execução pedente e realiza a execução em uma nova thread
        NSOperationQueue *requests = [[NSOperationQueue alloc] init];
        NSBlockOperation *igoneFollowerRequest;
        NSMutableArray *followersIds = [[NSMutableArray alloc] init];
        requests.maxConcurrentOperationCount = 6;
        
        //Variaveis capturadas pelo NSBlockOperation, assegurar a execução do delegate sem retain em self
        BOOL delegateWantsToBeNotified = delegateRespondsTo.followerRequestWasIgnoredWithId;
        _shouldDismissIngoredNotifications = NO;
        for (NSString *followerId in self.notificationsIgnored) {
            [followersIds addObject:followerId];
            if (![self.notificationsIgnored[followerId] boolValue]) {
                igoneFollowerRequest = [NSBlockOperation blockOperationWithBlock:^(void) {
                    id<FollowerRequestDelegate> __strong strongDelegate = delegate;
                    __weak typeof(self) weakSelf = self;
                    [WSSocial dismissFollower:followerId completion:^(BOOL success, enum FollowerStatus consumerStatus, enum FollowerStatus followerStatus, NSError * _Nullable error) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (error && error.code != 404) {
                                [AlertMessage showAlertWithError:error controller:weakSelf];
                            }
                        });
                    }];
                    if (delegateWantsToBeNotified) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [strongDelegate followerRequestWasIgnoredWithId:[followerId integerValue]];
                        });
                    }
                }];
                [requests addOperation:igoneFollowerRequest];
            }
        }
    }
    
    // Disable View Animation
    if ([[[NSProcessInfo processInfo] arguments] containsObject:@"NoAnimations"]) {
        [UIView setAnimationsEnabled:false];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.notifications.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if([[self.notifications objectAtIndex:indexPath.row] isKindOfClass:[PPFollowerNotification class]]){
        PPFollowerNotification *consumerNotification = (PPFollowerNotification *) [self.notifications objectAtIndex:indexPath.row];
        
        FollowerTableViewCell *cell = (FollowerTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"Follower" forIndexPath:indexPath];
        
        if (cell.delegate == nil) {
            //One time config
            [self configureCell:cell];
        }
        
        //Data+View
        [cell configureCellWithNotification:consumerNotification isFollowerRequest:YES];
        
        //View
        [cell setFollowerRequestIgnored:[[self.notificationsIgnored objectForKey:[NSString stringWithFormat:@"%ld", (long)consumerNotification.profile.wsId]] boolValue]];
        
        if (![[consumerNotification wsId]  isEqual: @""]) {
            if (![consumerNotification read]) {
                cell.contentView.backgroundColor = [UIColor colorWithRed:244.0/255 green:255.0/255 blue:238.0/255 alpha:1];
            }else{
                cell.contentView.backgroundColor = [PaletteObjc ppColorGrayscale000];
            }
        }
        
        return cell;
    }
    
    return [[UITableViewCell alloc] init];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PPNotification *notification = [self.notifications objectAtIndex:indexPath.row];
    FollowerRequestsTableViewController* __weak selfWeak = self;
    
    if (!notification.read) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [WSConsumer readNotificationWithId:notification.wsId completionHandler:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    [notification markAsRead];
                    [selfWeak.tableView reloadData];
                });
            }];
        });
    }
    
    // Skip the selection of follower request notification
    if([notification isKindOfClass:[PPFollowerNotification class]]){
        PPFollowerNotification *followerNotification = (PPFollowerNotification *) notification;
        // Open consumer profile
        self.profileViewController = [NewProfileProxy openProfileWithContact:followerNotification.profile parent:self originView:nil];
        return;
    }
}

- (void) configureCell:(FollowerTableViewCell *)cell {
    cell.delegate = self;
    cell.profileImage.delegate = self;
    cell.unfollowTimerButton.cooldown = 2.5;
}

# pragma mark - FollowerTableViewCellDelegate

- (void) followerActionButtonDidTap:(FollowerTableViewCell*) cell notification:(PPConsumerProfileNotification *) notification action:(enum FollowerButtonAction)action{
    if (notification != nil){
        
        // Retrive the index for follower notification
        NSInteger index = [self.notifications indexOfObject:notification];
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
        
        if([notification isKindOfClass:[PPConsumerProfileNotification class]]){
            PPConsumerProfileNotification *followerNotification = (PPConsumerProfileNotification *) notification;
            
            // If the current status of notification is waiting that means that the
            // user was tape over accept request button
            if (action == FollowerButtonActionAllow){
                
                [self acceptFollowerRequest:followerNotification indexPath:indexPath cell: cell];
                // Change the notification status to give feedback to the user
                followerNotification.followerStatusConsumer = FollowerStatusLoading;
                followerNotification.consumerStatusFollower = FollowerStatusLoading;
                [cell changeButtonWithStatus:FollowerStatusLoading consumerStatus:FollowerStatusLoading];
            }else if (action == FollowerButtonActionFollow){
                [self followConsumer:followerNotification indexPath:indexPath cell: cell];
                // Change the notification status to give feedback to the user
                followerNotification.followerStatusConsumer = FollowerStatusLoading;
                followerNotification.consumerStatusFollower = FollowerStatusLoading;
                [cell changeButtonWithStatus:FollowerStatusLoading consumerStatus:FollowerStatusLoading];
            }
            else if (action == FollowerButtonActionDismiss){
                [cell runTimer];
            }
            else if (action == FollowerButtonActionUnfollow){
                NSString *text = [NSString stringWithFormat:@"Deseja deixar de seguir @%@",followerNotification.profile.username];
                [self unfollowerConsumer:followerNotification cell:cell indexPath:indexPath text:text];
            }
            else if (action == FollowerButtonActionCancelRequest){
                NSString *text = [NSString stringWithFormat:@"Deseja cancelar o pedido para seguir @%@",followerNotification.profile.username];
                [self unfollowerConsumer:followerNotification cell:cell indexPath:indexPath text:text];
            }
        }
    }
}

- (void) ignoreRequestButtonStatusChangedTo:(enum IgnoreRequestStatus)status sender:(FollowerTableViewCell *)sender {
    NSString *followerId = [NSString stringWithFormat:@"%ld", (long)sender.notification.profile.wsId];
    switch (status) {
        case IgnoreRequestStatusStarted:
            //Adiciona no Dictionary a flag que ainda não foi ignorado, mas o usuário já indicou para ignorar
            [self.notificationsIgnored setValue:@NO forKey: followerId];
            break;
            
        case IgnoreRequestStatusDone:
            //Adiciona no Dictionary a flag que já foi executado e ignorado
            [self.notificationsIgnored setValue:@YES forKey: followerId];
            if (_shouldDismissIngoredNotifications) {
                [self followerRequestDismiss:followerId];
            }
            break;
            
        case IgnoreRequestStatusCanceled:
            //Remove do Dictionary o FollowerId, para exibir a cell como nova
            [self.notificationsIgnored removeObjectForKey: followerId];
            break;
    }
}

# pragma mark - User Actions

/**!
 * Accept a follower request
 */
- (void) acceptFollowerRequest:(PPConsumerProfileNotification *) followerNotification indexPath:(NSIndexPath *) indexPath cell: (FollowerTableViewCell *) cell{
    FollowerRequestsTableViewController* __weak selfWeak = self;
    FollowerTableViewCell* __weak weakCell = cell;
    BOOL useDelegate = delegateRespondsTo.followerRequestWasUpdated;
    id<FollowerRequestDelegate> strongDelegate = delegate;
    __block FollowerStatus oldConsumerStatus = followerNotification.consumerStatusFollower;
    __block FollowerStatus oldFollowerStatus = followerNotification.followerStatusConsumer;
    
    NSString* idFollower = [NSString stringWithFormat:@"%ld", (long)followerNotification.profile.wsId];
    // call service
    [WSSocial acceptFollower:idFollower completion:^(BOOL success,enum FollowerStatus consumerStatus, enum FollowerStatus followerStatus, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(success){
                // updates the notification to change the button
                // in order to show feedback to the user
                followerNotification.followerStatusConsumer = followerStatus;
                followerNotification.consumerStatusFollower = consumerStatus;
                
                // Workaround
                // change the notification text
                //followerNotification.message = [NSString stringWithFormat:@"<b>%@</b> (@%@) está te seguindo.",followerNotification.profile.onlineName, followerNotification.profile.username];
                followerNotification.message = [NSString stringWithFormat:@"@%@ está te seguindo.",followerNotification.profile.username];
                
                [selfWeak.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                
                if (useDelegate) {
                    [strongDelegate followerRequestWasUpdated:followerNotification];
                }
            }else{
                if (error && error.code != 404) {
                    [AlertMessage showAlertWithError:error controller:selfWeak];
                }
                if (!selfWeak || !weakCell) return;
                
                followerNotification.followerStatusConsumer = oldFollowerStatus;
                followerNotification.consumerStatusFollower = oldConsumerStatus;
                [weakCell changeButtonWithStatus: oldFollowerStatus consumerStatus: oldConsumerStatus];
            }
        });
    }];
}

/**!
 * Ignore follower request
 */
- (void) followerRequestDismiss:(NSString *) followerId {
    __weak typeof(self) weakSelf = self;
    [WSSocial dismissFollower:followerId completion:^(BOOL success, enum FollowerStatus consumerStatus, enum FollowerStatus followerStatus, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error && error.code != 404) {
                [AlertMessage showAlertWithError:error controller:weakSelf];
            }
        });
    }];
    
    //Sempre considerar que foi realizado com sucesso.
    if (delegateRespondsTo.followerRequestWasIgnoredWithId) {
        [delegate followerRequestWasIgnoredWithId:[followerId integerValue]];
    }
}

/**!
 * Follow a consumer
 */
- (void) followConsumer:(PPConsumerProfileNotification *) followerNotification indexPath:(NSIndexPath *) indexPath  cell: (FollowerTableViewCell *) cell{
    FollowerRequestsTableViewController* __weak selfWeak = self;
    FollowerTableViewCell* __weak weakCell = cell;
    BOOL delegateWantsToBeUpdated = delegateRespondsTo.followerRequestWasUpdated;
    id<FollowerRequestDelegate> strongDelegate = delegate;
    
    __block FollowerStatus oldConsumerStatus = followerNotification.consumerStatusFollower;
    __block FollowerStatus oldFollowerStatus = followerNotification.followerStatusConsumer;
    
    NSString* idFollower = [NSString stringWithFormat:@"%ld", (long)followerNotification.profile.wsId];
    [WSSocial follow:idFollower completion:^(BOOL success, enum FollowerStatus consumerStatus, enum FollowerStatus followerStatus, NSError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if(success){
                // updates the notification to change the button
                // in order to show feedback to the user
                followerNotification.followerStatusConsumer = followerStatus;
                followerNotification.consumerStatusFollower = consumerStatus;
                [selfWeak.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                
                if (delegateWantsToBeUpdated) {
                    [strongDelegate followerRequestWasUpdated:followerNotification];
                }
            }else{
                if (selfWeak) {
                    [AlertMessage showCustomAlertWithError: error controller: selfWeak completion: nil];
                }
                if (!selfWeak || !weakCell) return;
                
                followerNotification.followerStatusConsumer = oldFollowerStatus;
                followerNotification.consumerStatusFollower = oldConsumerStatus;
                [weakCell changeButtonWithStatus: oldFollowerStatus consumerStatus: oldConsumerStatus];

                if (delegateWantsToBeUpdated) {
                    [strongDelegate followerRequestWasUpdated:followerNotification];
                }
            }
        });
        
    }];
}


/**!
 * Unfollow a consumer
 */
- (void) unfollowerConsumer:(PPConsumerProfileNotification *) followerNotification cell:(FollowerTableViewCell*) cell indexPath:(NSIndexPath *) indexPath text:(NSString *) text{
    FollowerRequestsTableViewController* __weak selfWeak = self;
    BOOL delegateWantsToBeUpdated = delegateRespondsTo.followerRequestWasUpdated;
    id<FollowerRequestDelegate> strongDelegate = delegate;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:text message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Sim" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        followerNotification.followerStatusConsumer = FollowerStatusLoading;
        followerNotification.consumerStatusFollower = FollowerStatusLoading;
        [cell changeButtonWithStatus:FollowerStatusLoading consumerStatus:FollowerStatusLoading];
        
        NSString* idFollower = [NSString stringWithFormat:@"%ld", (long)followerNotification.profile.wsId];
        [WSSocial unfollow:idFollower completion:^(BOOL success, enum FollowerStatus consumerStatus, enum FollowerStatus followerStatus, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(success){
                    // updates the notification to change the button
                    // in order to show feedback to the user
                    followerNotification.followerStatusConsumer = followerStatus;
                    followerNotification.consumerStatusFollower = consumerStatus;
                    [selfWeak.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                    if (delegateWantsToBeUpdated) {
                        [strongDelegate followerRequestWasUpdated:followerNotification];
                    }
                }else{
                    if(selfWeak)
                    [AlertMessage showCustomAlertWithError: error controller: selfWeak completion: nil];
                }
            });
        }];
        
        
    }];
    [alert addAction:okAction];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Não" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:^{
        
    }];
}

#pragma mark - UIPPProfileImageDelegate

- (void) profileImageViewDidTap:(UIPPProfileImage *) profileImage contact:(PPContact *) contact{
    self.profileViewController = [NewProfileProxy openProfileWithContact:contact parent:self originView: profileImage];
}

@end

