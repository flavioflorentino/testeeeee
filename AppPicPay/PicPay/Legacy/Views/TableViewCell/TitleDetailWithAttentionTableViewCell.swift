//
//  TitleDetailWithAttentionTableViewCell.swift
//  PicPay
//
//  Created by Vagner Orlandi on 29/06/17.
//
//

import UIKit

@objc
final class TitleDetailWithAttentionTableViewCell: UITableViewCell {

    @IBOutlet weak private var titleLabel: UILabel!
    
    @IBOutlet weak private var detailLabel: UILabel!
    
    @IBOutlet weak private var attentionIndicator: UIImageView!
    
    var detailNormalColor:UIColor = UIColor(white: 0.43, alpha: 1) {
        didSet {
            if let label = self.detailLabel {
                if (!self.withAttention) {
                    label.textColor = self.detailNormalColor
                }
            }
        }
    }
    
    var detailAttentionColor: UIColor = UIColor(red: 1.0, green: 80/255, blue: 117/255, alpha: 1) {
        didSet {
            if let label = self.detailLabel {
                if (self.withAttention) {
                    label.textColor = self.detailAttentionColor
                }
            }
        }
    }
    
    var attentionIcon:UIImage? = #imageLiteral(resourceName: "pagar-saldo") {
        didSet {
            if let imageView = self.attentionIndicator {
                imageView.image = self.attentionIcon
            }
        }
    }
    
    var title:String? {
        get {
            return self.titleLabel.text
        }
        set(title) {
            self.titleLabel.text = title
        }
    }
    
    var detail:String? {
        get {
            return self.detailLabel.text
        }
        set(detail) {
            self.detailLabel.text = detail
        }
    }
    
    var withAttention:Bool = true {
        didSet (oldValue) {
            //Somente aplica as propriedades caso mudar o estado
            if (self.withAttention != oldValue) {
                self.setupView(withState: self.withAttention ? .attention : .normal)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.attentionIndicator.image = self.attentionIcon
        self.setupView(withState: self.withAttention ? .attention : .normal)
    }
    
    private func setupView(withState state:State) {
        let widthConstraint = self.attentionIndicator.constraints.first(where: {return $0.identifier == "size"})
        
        switch state {
        case .normal:
            self.accessoryType = .disclosureIndicator
            self.detailLabel.textColor = self.detailNormalColor
            self.attentionIndicator.isHidden = true
            widthConstraint?.constant = 0
        case .attention:
            self.accessoryType = .none
            self.detailLabel.textColor = self.detailAttentionColor
            self.attentionIndicator.isHidden = false
            widthConstraint?.constant = 33
        }
    }
}

fileprivate extension TitleDetailWithAttentionTableViewCell {
    enum State:Int {
        case normal
        case attention
    }
}
