//
//  FollowerTableViewCell.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 23/11/16.
//
//

import UI
import UIKit

@objc protocol FollowerTableViewCellDelegate: AnyObject{
    func followerActionButtonDidTap(_ cell:FollowerTableViewCell, notification:PPConsumerProfileNotification?, action: FollowerButtonAction)
    @objc optional func ignoreRequestButtonStatusChanged(to status:FollowerTableViewCell.IgnoreRequestStatus, sender: FollowerTableViewCell?)
}

final class FollowerTableViewCell: UITableViewCell, UIDelayedActionButtonDelegate {
    
    @objc enum IgnoreRequestStatus:Int {
        case started
        case done
        case canceled
    }
    
    @IBOutlet weak var profileImage: UIPPProfileImage!
    @IBOutlet weak var dateTimeLabel: UILabel! {
        didSet {
            dateTimeLabel.textColor = Palette.ppColorGrayscale400.color
        }
    }
    @IBOutlet weak var textDescriptionLabel: UILabel! {
        didSet {
            textDescriptionLabel.textColor = Palette.ppColorGrayscale600.color
        }
    }
    @IBOutlet weak var actionButton: UIPPFollowButton!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var removeButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var unfollowTimerButton: UIDelayedActionButton!
    
    @objc weak var delegate:FollowerTableViewCellDelegate?
    @objc weak var notification:PPConsumerProfileNotification?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    /**
     No caso da cell estiver durante a animação, força a finalização e conseguentemente executa o delegate.
     */
    override func prepareForReuse() {
        if self.unfollowTimerButton.isRunning {
            self.unfollowTimerButton.forceCompletion()
        }
    }
    
    @objc func configureCell(notification:PPConsumerProfileNotification, isFollowerRequest:Bool = false){
        self.notification = notification
        self.actionButton.showAllowButton = isFollowerRequest
        
        // Disable selection for this type of notification
        self.selectionStyle = .none
        
        // image
        let contact = PPContact()
        if let profile =  notification.profile{
            self.profileImage.setContact(profile)
        }else{
            self.profileImage.setContact(contact)
        }
        
        self.textDescriptionLabel.text = ""
        let text = notification.message
        let attr = NSAttributedString(string: text ?? "", attributes: [.font: UIFont.systemFont(ofSize: 14, weight:.light)]).boldfyWithSystemFont(ofSize: 14 ,weight: UIFont.Weight.medium.rawValue)
        self.textDescriptionLabel.attributedText = attr
        
        self.dateTimeLabel.text = notification.creationDate
        // configure button
        self.changeButtonWithStatus(notification.followerStatusConsumer, consumerStatus: notification.consumerStatusFollower)
        self.unfollowTimerButton.delegate = self
    }
    
    /**
     Change button action according to follow status
    */
    @objc func changeButtonWithStatus(_ followerStatus:FollowerStatus, consumerStatus:FollowerStatus){
        
        if followerStatus == FollowerStatus.waitingAnswer {
            self.showRemoveButton()
        }else{
            self.hideRemoveButton()
        }

        self.actionButton.changeButtonWithStatus(followerStatus, consumerStatus:consumerStatus)
    }
    
    //Configura a cell para exibir se já foi ignorada ou não.
    @objc func setFollowerRequestIgnored(_ isIgnored:Bool) -> Void {
        if isIgnored {
            self.removeButton.isHidden = true
            self.actionButton.isHidden = true
            self.unfollowTimerButton.isHidden = false
            self.unfollowTimerButton.layer.borderColor = UIColor.clear.cgColor
            self.unfollowTimerButton.setTextColor(.lightGray)
            self.unfollowTimerButton.forceStatus(.done)
        } else {
            self.removeButton.isHidden = false
            self.actionButton.isHidden = false
            self.unfollowTimerButton.isHidden = true
            self.unfollowTimerButton.layer.borderColor = Palette.ppColorBranding300.cgColor
            self.unfollowTimerButton.layer.borderWidth = 1
            self.unfollowTimerButton.setTextColor(Palette.ppColorBranding300.color)
            self.unfollowTimerButton.setFont(UIFont.systemFont(ofSize: 13))
            self.unfollowTimerButton.restart()
            
        }
    }
    
    // Inicia o timer para ignorar
    @objc func runTimer() {
        self.removeButton.isHidden = true
        self.actionButton.isHidden = true
        self.unfollowTimerButton.isHidden = false
        self.unfollowTimerButton.forceStart()
    }
    
    /// Hide the remove button
    @objc func hideRemoveButton(){
        self.removeButtonWidth.constant = 0.0
        self.removeButton.isHidden = true
    }
    
    // Shown the remove button
    @objc func showRemoveButton(){
        self.removeButtonWidth.constant = 40.0
        self.removeButton.isHidden = false
    }
    
    // MARK: - User Actions
    
    /**
     Method called when user tap on button
    */
    @IBAction private func buttonTap(_ sender: AnyObject) {
        if let notification = self.notification{
            self.delegate?.followerActionButtonDidTap(self, notification:notification, action: self.actionButton.action)
        }
    }
    
    /**
     Method called when user tap on remove button
    */
    @IBAction private func removeButtonTap(_ sender: AnyObject) {
        if let notification = self.notification{
            self.delegate?.followerActionButtonDidTap(self, notification:notification, action: FollowerButtonAction.dismiss)
        }
    }
    
    //MARK: - UIDelayedActionButtonDelegate
    func delayedActionButtomTimeDidStart(_ sender: UIDelayedActionButton) {
        self.delegate?.ignoreRequestButtonStatusChanged?(to: .started, sender: self)
    }
    
    func delayedActionButtomTimeDidFinish(_ sender: UIDelayedActionButton) {
        self.setFollowerRequestIgnored(true)
        self.delegate?.ignoreRequestButtonStatusChanged?(to: .done, sender: self)
    }
    
    func delayedActionButtomWasCancelled(_ button: UIDelayedActionButton) {
        self.setFollowerRequestIgnored(false)
        self.delegate?.ignoreRequestButtonStatusChanged?(to: .canceled, sender: self)
    }
    
    //MARK: Accessibility
    
    override func accessibilityElementCount() -> Int {
        return 6
    }
    
    override func accessibilityElement(at index: Int) -> Any? {
        switch index {
        case 0:
            return self.textDescriptionLabel
        case 1:
            return self.dateTimeLabel
        case 2:
            return self.profileImage
        case 3:
            return self.actionButton
        case 4:
            return self.removeButton
        case 5:
            return self.unfollowTimerButton
        default:
            return nil
        }
    }
}
