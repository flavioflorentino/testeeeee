//
//  FollowingTableViewCell.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 21/12/16.
//
//

import UIKit

@objc protocol FollowingTableViewCellDelegate: AnyObject {
    func didTapMoreButton(_ indexPath:IndexPath)
    @objc optional func didTapFollowButton(_ sender: UIPPFollowButton, indexPath: IndexPath)
}

final class FollowingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImage: UIPPProfileImage!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var followButton: UIPPFollowButton!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var moreButtonWidthConstraint: NSLayoutConstraint!
    
    var indexPath:IndexPath?
    var contact:PPContact?
    weak var delegate:FollowingTableViewCellDelegate?
    
    func configureCell(_ followingData:PPFollowing, indexPath:IndexPath, delegate:FollowingTableViewCellDelegate, showMoreButton:Bool = false){
        self.indexPath = indexPath
        self.selectionStyle = .none
        self.delegate = delegate
        
        nameLabel.text = followingData.contact.onlineName ?? ""
        usernameLabel.text = "@" + (followingData.contact.username ?? "")
        profileImage.setContact(followingData.contact)
        followButton.changeButtonWithStatus(FollowerStatus.undefined, consumerStatus: followingData.status)
        
        moreButtonWidthConstraint.constant = showMoreButton ? 40 : 0
        self.moreButton.isHidden = !showMoreButton
        
        if ConsumerManager.shared.consumer?.wsId == followingData.contact.wsId {
            followButton.isHidden = true
        } else {
            followButton.isHidden = false
        }
        
    }
    
    @IBAction private func followAction(_ sender: AnyObject) {
        guard let indexPath = self.indexPath,
        let sender = sender as? UIPPFollowButton else {
            return
        }
        delegate?.didTapFollowButton?(sender, indexPath: indexPath)
    }
    
    @IBAction private func moreButtonAction(_ sender: AnyObject) {
        delegate?.didTapMoreButton(self.indexPath!)
    }
    
}
