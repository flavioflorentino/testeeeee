//
//  BaseFeedTableViewCell.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 09/11/16.
//
//

import UIKit

final class BaseFeedTableViewCell: UITableViewCell {

    @IBOutlet weak var imageProfile: UIPPProfileImage!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var alertLabel: UILabel!
    @IBOutlet weak var alertImageView: UIImageView!
    @IBOutlet weak var alertTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var msgLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    @IBOutlet weak var dateTimeImageView: UIImageView!
    @IBOutlet weak var dateTimeLabel: UILabel!
    
    @IBOutlet weak var commentImageView: UIImageView!
    @IBOutlet weak var commentLabel: UILabel!
    
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var likeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(_ item: DefaultFeedItem){
        // Imagem ---------
        if let image = item.image {
            var placeholder = PPStore.photoPlaceholder()
            
            if item.type == "P2PTransaction" {
                placeholder = PPContact.photoPlaceholder()
            }
            let url = URL(string: image.url)
            self.imageProfile.imageView?.setImage(url: url, placeholder: placeholder)
        }else{
            // TODO: Alterar placeholder para imagem lisa
            self.imageProfile.imageView?.image = PPStore.photoPlaceholder()
        }
        
        
        // Text -----
        if item.text != nil {
           // self.msgLabel?.text = text.value
            //self.msgLabel?.setAttributedStringKeepOriginalFont(text.valueAttributedString)
            self.msgLabel?.isHidden = false
        }else{
            self.msgLabel?.text = ""
            self.msgLabel?.isHidden = true
        }
        
        // Time Ago ------
        if let timeAgo = item.timeAgo {
            self.dateTimeLabel?.text = timeAgo.value
            //self.dateTimeLabel?.setAttributedStringKeepOriginalFont(timeAgo.valueAttributedString)
            self.dateTimeLabel?.isHidden = false
            self.dateTimeImageView.isHidden = false
        }else{
            self.dateTimeLabel?.text = ""
            self.dateTimeLabel?.isHidden = true
            self.dateTimeImageView.isHidden = true
        }
        
        // Value -------
        if item.money != nil {
            //self.valueLabel.text = money.value
            //self.valueLabel?.setAttributedStringKeepOriginalFont(money.valueAttributedString)
            self.valueLabel?.isHidden = false
        }else{
            self.valueLabel?.text = ""
            self.valueLabel?.isHidden = true
        }
        
        // Comments -------
        if let comment = item.comments {
            self.commentLabel.text = comment.value
            //self.commentLabel?.setAttributedStringKeepOriginalFont(comment.valueAttributedString)
            self.commentLabel?.isHidden = false
            self.commentImageView?.isHidden = false
        }else{
            self.commentLabel?.text = ""
            self.commentLabel?.isHidden = true
            self.commentImageView?.isHidden = true
        }
        
        // Like -------
        if let like = item.like {
            self.likeLabel.text = like.value
            //self.likeLabel?.setAttributedStringKeepOriginalFont(like.valueAttributedString)
            self.likeLabel?.isHidden = false
            self.likeImageView?.isHidden = false
        }else{
            self.likeLabel?.text = ""
            self.likeLabel?.isHidden = true
            self.likeImageView?.isHidden = true
        }
        
        // Alert -------
        if let alert = item.alert{
            self.alertLabel?.text = alert.text
            self.alertLabel?.isHidden = false
            self.alertImageView.isHidden = false
            self.alertTopConstraint.constant = 12
        }else{
            self.alertLabel?.text = ""
            self.alertLabel?.isHidden = true
            self.alertImageView.isHidden = true
            self.alertTopConstraint.constant = 0
        }

    }
}

/**
 Extension to allow
 */
extension UILabel {
    /**
     Set Attributed string but keep the original font
    */
    func setAttributedStringKeepOriginalFont(_ attr:NSAttributedString?) {
        if attr != nil {
            let attrs = NSMutableAttributedString(attributedString: attr!)
            
            let currentFont = self.font
            attrs.enumerateAttribute(.font, in: NSMakeRange(0,attrs.length), options: .longestEffectiveRangeNotRequired, using: { (value, range,stop) in
                if let oldFont = value as? UIFont, let pointSize = currentFont?.pointSize{
                    var newFont = currentFont
                    if oldFont.fontName == "TimesNewRomanPS-BoldMT" {
                        newFont = UIFont.boldSystemFont(ofSize: pointSize)
                    }
                    
                    attrs.removeAttribute(.font, range: range)
                    attrs.addAttribute(.font, value: newFont!
                        , range: range)
                }
            })
            self.attributedText = attrs
        }
    }
}
