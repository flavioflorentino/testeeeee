import UI
import UIKit

protocol SocialHeaderTableViewCellDelegate : AnyObject {
    func socialHeaderDidChangeCheckmark(_ cell:SocialHeaderTableViewCell)
}

final class SocialHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var checkmark: UIPPCheckMark!
    @IBOutlet weak var checkmarkView: UIView!
    
    weak var delegate:SocialHeaderTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        checkmarkView.isUserInteractionEnabled = true
        checkmarkView.backgroundColor = Palette.ppColorGrayscale100.color
        checkmark.backgroundColor = Palette.ppColorGrayscale100.color
        checkmarkView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectCheckmark)))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(_ label: String, checked: Bool){
        backgroundColor = Palette.ppColorGrayscale100.color
        self.label.text = label
        checkmark.checked = checked
        selectionStyle = .none
    }
    
    @objc func selectCheckmark(){
        // TODO: alterar o check no model
        checkmark.checked = !checkmark.checked
        delegate?.socialHeaderDidChangeCheckmark(self)
    }
}
