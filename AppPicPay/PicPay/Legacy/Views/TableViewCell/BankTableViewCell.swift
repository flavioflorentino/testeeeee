//
//  BankTableViewCell.swift
//  PicPay
//
//  ☝🏽☁️ Created by Luiz Henrique Guimarães on 15/05/17.
//
//

import UIKit

final class BankTableViewCell: UITableViewCell {
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var name: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(bank: PPBank){
        separatorInset = UIEdgeInsets.zero
        
        logoImageView.layer.cornerRadius = logoImageView.frame.size.width / 2
        logoImageView.clipsToBounds = true
        logoImageView.image = #imageLiteral(resourceName: "ico_bank_placeholder.png")
        let imageUrl = URL(string: bank.imageUrl ?? "")
        logoImageView.setImage(url: imageUrl, placeholder: #imageLiteral(resourceName: "ico_bank_placeholder.png"))
        name.text = bank.displayName()
    }
}
