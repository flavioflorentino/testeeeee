//
//  ChatTableViewCell.m
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 15/07/16.
//
//

#import "ChatTableViewCell.h"

@implementation ChatTableViewCell

- (void)drawRect:(CGRect)rect{
    [self roundCornersOnView:self.messageBackgroundView onTopLeft:NO topRight:YES bottomLeft:YES bottomRight:YES radius:10.0];
}

/*
- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton *openInMaps = [UIButton new];
    [openInMaps setFrame:CGRectMake(15, 135, 114, 70)];
    openInMaps = (UIButton *)[self roundCornersOnView:openInMaps onTopLeft:NO topRight:NO bottomLeft:YES bottomRight:NO radius:5.0];
}*/

- (UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius
{
    
    return view;
}


@end
