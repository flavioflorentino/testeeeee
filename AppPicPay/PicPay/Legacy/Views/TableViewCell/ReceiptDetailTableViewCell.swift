//
//  ReceiptDetailTableViewCell.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 14/10/16.
//
//

import UIKit

final class ReceiptDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var labelLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    internal var insetDashLine:CGFloat = 20.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews(){
        super.layoutSubviews()
        self.addDashedLine()
    }
    
    func addDashedLine(_ color: UIColor = UIColor(red: 86.0/255.0, green: 87.0/255.0, blue: 82.0/255.0, alpha: 1.0)) {
        _ = layer.sublayers?.filter({ $0.name == "DashedTopLine" }).map({ $0.removeFromSuperlayer() })
        self.backgroundColor = UIColor.clear
        let cgColor = color.cgColor
        
        let shapeLayer: CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.name = "DashedTopLine"
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: (frameSize.width / 2), y: frameSize.height / 2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = cgColor
        shapeLayer.lineWidth = 1.0
        shapeLayer.lineDashPattern = [0.5,2.0]
        shapeLayer.lineCap = .round
        
        let path: CGMutablePath = CGMutablePath()
        path.move(to: CGPoint(x: self.insetDashLine, y: self.frame.height - 2))
        path.addLine(to: CGPoint(x: self.frame.width - (self.insetDashLine), y: self.frame.height - 2))
        shapeLayer.path = path
        
        self.layer.addSublayer(shapeLayer)
    }

}
