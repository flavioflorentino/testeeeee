import UI
import UIKit

@objc
final class GroupFollowerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImage:UIPPProfileImage!
    @IBOutlet weak var descriptionLabel:UILabel!
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var badgeLabel:UILabel!

    @objc var notifications:[PPConsumerProfileNotification]? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.badgeLabel.layer.masksToBounds = true
        self.badgeLabel.layer.cornerRadius = self.badgeLabel.frame.size.height / 2
    }
    
    @objc func configureCell(notifications:[PPConsumerProfileNotification]){
        guard let notification = notifications.first else {
            return
        }
        
        self.notifications = notifications
        
        // image
        let contact = PPContact()
        if let profile =  notification.profile {
            self.profileImage.setContact(profile)
        }else{
            self.profileImage.setContact(contact)
        }
        
        // text
        let username = notification.profile?.username ?? "@..."
        
        self.titleLabel.text = "@\(username) e mais \(notifications.count - 1) querem te seguir"
        //DispatchQueue.global(qos: .userInitiated).async {
            let text = "<b>@\(username)</b> e mais \(notifications.count - 1) querem te seguir"
        
        let attr = NSAttributedString(string: text, attributes: [.font: UIFont.systemFont(ofSize: 14, weight:.light), .foregroundColor: Palette.ppColorGrayscale600.color]).boldfyWithSystemFont(ofSize: 14 ,weight: UIFont.Weight.medium.rawValue)
            //DispatchQueue.main.async {
                self.titleLabel.attributedText = attr
            //}
        //}
        self.badgeLabel.text = notifications.count >= 10 ? "9+" : "\(notifications.count)"
    }
}
