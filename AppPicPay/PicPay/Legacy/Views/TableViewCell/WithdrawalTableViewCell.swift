import UI
import UIKit

@objc protocol WithdrawalTableViewCellDelegate: AnyObject {
    func withdrawalActionButtonDidTap(_ cell:WithdrawalTableViewCell)
}

final class WithdrawalTableViewCell: UITableViewCell {

    @IBOutlet weak var notificationText: UILabel! {
        didSet {
            notificationText.textColor = Palette.ppColorGrayscale600.color
        }
    }
    @IBOutlet weak var timeAgo: UILabel! {
        didSet {
            timeAgo.textColor = Palette.ppColorGrayscale400.color
        }
    }

    @objc var indexPath:IndexPath?
    @objc weak var delegate:WithdrawalTableViewCellDelegate?
    @objc weak var notification:PPConsumerProfileNotification?
    

    @objc func configureCell(_ notification: PPWithdrawalNotification) {

        if let message = notification.message {
            notification.message =  message
            self.notificationText.text = ""
            
            let text = notification.message
            let attr = NSAttributedString(string: text ?? "", attributes: [.font: UIFont.systemFont(ofSize: 14, weight:.regular)]).boldfyWithSystemFont(ofSize: 14 ,weight: UIFont.Weight.semibold.rawValue)
            self.notificationText.attributedText = attr
        }
        
        timeAgo.text = notification.creationDate ?? ""

        notificationText.sizeToFit()
   
    }

    @IBAction private func moreButtonAction(_ sender: AnyObject) {
        self.delegate?.withdrawalActionButtonDidTap(self)
    }
    
}
