import UI
import UIKit

final class BankAccountTableViewCell: UITableViewCell {

    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var account: UILabel!
    @IBOutlet weak var bank: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(bankAccount: PPBankAccount){
        
        let imageUrl = URL(string: bankAccount.bank?.imageUrl ?? "")
        logo.setImage(url: imageUrl, placeholder: #imageLiteral(resourceName: "ico_bank_placeholder.png"))

        let agencyNumber = bankAccount.agencyNumber ?? ""
        let agencyDigit = bankAccount.agencyDigit ?? ""
        let accountNumber = bankAccount.accountNumber ?? ""
        let accountDigit = bankAccount.accountDigit ?? ""
        
        let cellText = BankAccountLocalizable.bankCellText.text
        let text = String(format: cellText, agencyNumber, agencyDigit, accountNumber, accountDigit)
        
        account.text = text
        bank.text = bankAccount.bank?.name ??  ""
        bank.textColor = Palette.ppColorGrayscale500.color
    }
    
}
