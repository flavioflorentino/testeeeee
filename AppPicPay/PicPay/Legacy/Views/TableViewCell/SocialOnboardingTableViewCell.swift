import UI
import UIKit

final class SocialOnboardingTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var checkMarkContainer: UIView!
    @IBOutlet weak var checkmark: UIPPCheckMark!
    @IBOutlet weak var profile: UIPPProfileImage!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = Palette.ppColorGrayscale000.color
        checkMarkContainer.backgroundColor = Palette.ppColorGrayscale000.color
        checkmark.backgroundColor = Palette.ppColorGrayscale000.color
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(_ contact: PPContact, checked: Bool = false){
        
        nameLabel.text = contact.onlineName ?? ""
        usernameLabel.text = "@" + (contact.username ?? "")
        profile.setContact(contact)
        
        checkmark.checked = checked
        selectionStyle = .none
    }
    
}
