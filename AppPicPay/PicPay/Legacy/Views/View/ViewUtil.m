//
//  ViewUtil.m
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 27/09/16.
//
//

#import "ViewUtil.h"

@implementation ViewUtil

/*!
 * Create a image from a view
 * @return image
 */
+ (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

@end
