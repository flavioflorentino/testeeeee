//
//  ViewUtil.h
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 27/09/16.
//
//

#import <Foundation/Foundation.h>

@interface ViewUtil : NSObject

/*!
 * Create a image from a view
 * @return image
 */
+ (UIImage *) imageWithView:(UIView *)view;

@end
