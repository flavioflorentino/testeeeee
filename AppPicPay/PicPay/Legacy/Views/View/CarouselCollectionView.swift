//
//  CarouselCollectionView.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 19/05/17.
//
//

import Foundation

class CarouselCollectionView: UICollectionView {
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        commmonInit()
    }
    
    init() {
        super.init(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        commmonInit()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commmonInit()
    }
    
    func commmonInit(){
        
        self.backgroundColor = UIColor.clear
        
        self.showsHorizontalScrollIndicator = false
        self.showsVerticalScrollIndicator = false
        
        guard let layout = self.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        
        layout.scrollDirection = .horizontal
        
        // Left and right padding
        layout.headerReferenceSize = CGSize(width: 12, height: 12)
        layout.footerReferenceSize = CGSize(width: 12, height: 12)
        
        layout.itemSize = CGSize(width: 80, height: 100)
        layout.minimumLineSpacing = 12
        layout.minimumInteritemSpacing = 12
        
    }
    
    func setItemSize(size: CGSize) {
        guard let layout = self.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        
        layout.itemSize = size
    }
    
    func setMargins(left: CGSize, right: CGSize) {
        guard let layout = self.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        
        // Left and right padding
        layout.headerReferenceSize = left
        layout.footerReferenceSize = right
    }
}
