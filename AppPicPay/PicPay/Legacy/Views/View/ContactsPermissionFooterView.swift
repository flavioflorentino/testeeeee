import PermissionsKit
import SnapKit
import UI
import UIKit

// MARK: - Layout
private extension ContactsPermissionFooterView.Layout {
    enum Font {
        static let title: UIFont = .systemFont(ofSize: 16.0, weight: .semibold)
    }

    enum Size {
        static let titleLabelMinHeight: CGFloat = 21.0
    }
}

final class ContactsPermissionFooterView: UIView {
    fileprivate enum Layout { }

    // MARK: - Properties
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = PermissionsKit.Strings.contacts.uppercased()
        label.font = Layout.Font.title
        label.backgroundColor = Colors.backgroundPrimary.color

        return label
    }()

    private lazy var permissionRequestView = PermissionRequestView(
        setup: .listItem(requestType: .contacts(usesOldImage: true), authorizeHandler: { [weak self] in
            self?.authorizeAction?()
        })
    )

    var authorizeAction: (() -> Void)?

    // MARK: - Initialization
    convenience init() {
        self.init(frame: .zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        buildLayout()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewConfiguration
extension ContactsPermissionFooterView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(titleLabel)
        addSubview(permissionRequestView)
    }

    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(Spacing.base02)
            $0.height.greaterThanOrEqualTo(Layout.Size.titleLabelMinHeight)
            $0.leading.equalToSuperview().offset(Spacing.base02)
            $0.trailing.lessThanOrEqualToSuperview().offset(-Spacing.base02)
        }

        permissionRequestView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom)
            $0.leading.equalToSuperview()
            $0.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
    }

    func configureViews() {
        backgroundColor = Colors.backgroundPrimary.color
    }
}
