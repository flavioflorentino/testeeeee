//
//  PermissionInfoView.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 11/08/16.
//
//

import UIKit

class NibView: UIView {

    @IBOutlet weak var view: UIView!
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        nibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        nibSetup()
    }
    
    fileprivate func nibSetup() {
        backgroundColor = .clear
        
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        
        addSubview(view)
    }
    
    fileprivate func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        guard let nibView = nib.instantiate(withOwner: self, options: nil).first as? UIView else {
            return UIView()
        }
        
        return nibView
    }
    
    // MARK: - Public Methdos
    
    func sizeHeightToFit(forceWidthConstraint: Bool = true, width: CGFloat? = nil){
        let oldMaskIntoConstraintsFlag = translatesAutoresizingMaskIntoConstraints
        translatesAutoresizingMaskIntoConstraints = false
        
        var widthConstraint: NSLayoutConstraint?
        if forceWidthConstraint {
            let widthValue = width ?? frame.size.width
            widthConstraint = widthAnchor.constraint(equalToConstant: widthValue)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
        
        if let widthAnchor = widthConstraint {
            NSLayoutConstraint.deactivate([widthAnchor])
        }
        
        translatesAutoresizingMaskIntoConstraints = oldMaskIntoConstraintsFlag
    }
}
