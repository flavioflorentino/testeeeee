//
//  TableItemErrorView.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 26/12/16.
//
//

import UIKit

final class TableItemErrorView: UIView {
    
    fileprivate lazy var titleLabel: UILabel = {
        let text:NSMutableAttributedString = NSMutableAttributedString(string: "Verifique sua conexão e\n")
        text.append(NSMutableAttributedString(string: "toque aqui para tentar novamente.", attributes: [.font: UIFont.boldSystemFont(ofSize: 14)] ))
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.systemFont(ofSize: 14)
        label.attributedText = text
        label.textColor = UIColor.lightGray
        return label
    }()
    
    fileprivate lazy var imageView: UIImageView = {
        let image = UIImageView(image: UIImage(named: "reload_feed_icon")!)
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension TableItemErrorView {

    public func startAnimateCircle() {
        rotate360Degrees()
    }
    
    fileprivate func setup(){
        self.addSubview(imageView)
        self.addSubview(titleLabel)
        
        // label constraints
        NSLayoutConstraint.activate(
            [
                imageView.widthAnchor.constraint(equalToConstant: 30),
                imageView.heightAnchor.constraint(equalToConstant: 30),
                imageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
                imageView.centerYAnchor.constraint(equalTo: self.centerYAnchor)
            ]
        )
        
        NSLayoutConstraint.activate(
            [
                titleLabel.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 20),
                titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 20),
                titleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor)
            ]
        )
    }
    
    fileprivate func rotate360Degrees(duration: CFTimeInterval = 1) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = -(CGFloat.pi * 2)
        rotateAnimation.isRemovedOnCompletion = false
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount = Float.infinity
        imageView.layer.add(rotateAnimation, forKey: nil)
    }
}
