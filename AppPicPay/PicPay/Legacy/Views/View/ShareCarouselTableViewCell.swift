//
//  ShareCarouselTableViewCell.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 19/05/17.
//
//

import UIKit

final class ShareCarouselTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: ShareCarouselCollectionView!
}
