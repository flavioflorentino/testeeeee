import UI

private extension PromoCodeUserMoneyView.Layout {
    enum Style {
        static let titleFont = UIFont.systemFont(ofSize: 14.0, weight: .regular)
        static let fontSize = UIFont.systemFont(ofSize: 17)
        static let secondaryFontSize = UIFont.systemFont(ofSize: 12)
    }
    
    enum Size {
        static let insetContainer = UIEdgeInsets(
            top: Spacing.base01,
            left: Spacing.base02,
            bottom: Spacing.base01,
            right: Spacing.base02
        )
        static let imageSize: CGFloat = 28
        static let borderWidth: CGFloat = 0.5
    }
}

final class PromoCodeUserMoneyView: UIView, ViewConfiguration {
    fileprivate enum Layout {}
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Style.titleFont
        label.textColor = Colors.grayscale500.color
        label.text = RegisterLocalizable.usernameCodeUsed.text.uppercased()
        return label
    }()
    
    private lazy var promoCodeContainerView = UIView()
    
    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = Spacing.base02
        stackView.alignment = .center
        return stackView
    }()
    
    private lazy var textStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = Layout.Size.imageSize/2
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var codeLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Style.fontSize
        label.textColor = Colors.black.color
        return label
    }()
    
    private lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Style.secondaryFontSize
        label.textColor = Colors.grayscale400.color
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.font = Layout.Style.fontSize
        label.textColor = Colors.grayscale400.color
        label.textAlignment = .right
        return label
    }()
    
    init() {
        super.init(frame: .zero)
        buildLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        drawBorders()
    }
    
    func buildViewHierarchy() {
        textStackView.addArrangedSubview(codeLabel)
        textStackView.addArrangedSubview(usernameLabel)
        
        containerStackView.addArrangedSubview(imageView)
        containerStackView.addArrangedSubview(textStackView)
        containerStackView.addArrangedSubview(valueLabel)
        
        promoCodeContainerView.addSubview(containerStackView)
        
        addSubview(titleLabel)
        addSubview(promoCodeContainerView)
    }
    
    func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.equalTo(Spacing.base02)
            $0.trailing.equalToSuperview()
        }
        
        promoCodeContainerView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Spacing.base01)
            $0.leading.equalToSuperview()
            $0.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
        containerStackView.snp.makeConstraints {
            $0.edges.equalTo(Layout.Size.insetContainer)
        }
        
        imageView.snp.makeConstraints {
            $0.size.equalTo(Layout.Size.imageSize)
        }
    }
    
    func configureStyles() {
        promoCodeContainerView.backgroundColor = Palette.ppColorGrayscale000.color
    }
    
    func configure(_ rewardPromo: PPRegisterRewardPromoCode, placeholder: UIImage? = nil) {
        imageView.image = placeholder
        if let contact = rewardPromo.inviter {
            usernameLabel.text = contact.onlineName ?? ""
            if let urlString = contact.imgUrl {
                imageView.setImage(url: URL(string: urlString), placeholder: placeholder)
            }
        }
        valueLabel.text = rewardPromo.value
        codeLabel.text = rewardPromo.code
    }
    
    private func drawBorders() {
        let topBorder = CALayer()
        topBorder.frame = CGRect(x: 0, y: 0, width: promoCodeContainerView.frame.width, height: Layout.Size.borderWidth)
        topBorder.backgroundColor = Colors.grayscale300.color.cgColor
        
        let bottonBorder = CALayer()
        bottonBorder.frame = CGRect(x: 0, y: promoCodeContainerView.frame.height - 1, width: self.frame.width, height: Layout.Size.borderWidth)
        bottonBorder.backgroundColor = Colors.grayscale300.color.cgColor
        
        promoCodeContainerView.layer.addSublayer(topBorder)
        promoCodeContainerView.layer.addSublayer(bottonBorder)
    }
}
