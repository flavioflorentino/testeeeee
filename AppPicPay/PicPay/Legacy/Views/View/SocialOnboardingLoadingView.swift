import UIKit

final class SocialOnboardingLoadingView: NibView {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var text: UILabel!
    
    private var needAnimate = false
    private var imageIndex = 0
    private var images = [#imageLiteral(resourceName: "onboard_loading_contact_list_1"), #imageLiteral(resourceName: "onboard_loading_contact_list_2"), #imageLiteral(resourceName: "onboard_loading_contact_list_3")]
    
    func startAnimation() {
        needAnimate = true
        changeImage()
    }
    
    func stopAnimation() {
        needAnimate = false
    }
    
    // Change image
    func changeImage() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            guard self.needAnimate else {
                return
            }
            
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseOut, animations: {
                self.image.layer.opacity = 0.0
            }, completion: { _ in
                UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseOut, animations: {
                    self.image.layer.opacity = 1.0
                    self.imageIndex += 1
                    self.image.image = self.images[self.imageIndex % self.images.count]
                    self.changeImage()
                })
            })
        }
    }
}
