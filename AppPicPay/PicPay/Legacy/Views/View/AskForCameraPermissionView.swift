import UI
import UIKit

final class AskForCameraPermissionView: NibView {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var cameraRequestButton: UIPPButton!
    @IBOutlet weak var cancelButton: UIPPButton!
    
    var action:((_ sender: UIButton) -> Void)? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBAction private func requestCameraPermission(_ sender: UIButton) {
        action?(sender)
    }
    
    @IBAction private func cancelButtonTapped(_ sender: Any) {
        self.removeFromSuperview()
    }
}

extension AskForCameraPermissionView {
    func setup() {
        backgroundColor = Palette.black.color
        cancelButton.isHidden = true
    }
}
