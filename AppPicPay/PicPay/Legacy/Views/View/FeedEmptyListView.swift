//
//  FeedEmptyListView.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 23/12/16.
//
//

import UIKit
import Lottie

final class FeedEmptyListView: UIView {

    
    @IBOutlet weak var animationView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    private var lottieView: AnimationView?
    
    func setAnimation(name: String) {
        lottieView = AnimationView(name: name)
        if let lottieView = lottieView {
            lottieView.frame = animationView.frame
            lottieView.center = animationView.center
            lottieView.contentMode = .scaleAspectFit
            animationView.addSubview(lottieView)
            lottieView.loopMode = .loop
        }
    }
    
    func playAnimation() {
        if let animation = lottieView, !animation.isAnimationPlaying {
            animation.play()
        }
    }

}
