//
//  InstallmentHeaderView.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 22/09/16.
//
//

import UIKit

final class InstallmentHeaderView: NibView {

    @IBOutlet weak var picPayBalanceLabel: UILabel!
	@IBOutlet var p2pHeaderTitle: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
