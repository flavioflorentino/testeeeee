//
//  ShareCarouselCollectionView.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 19/05/17.
//
//

import Foundation
import UI

final class ShareCarouselCollectionView: CarouselCollectionView {
    
    let data: [ShareOptions] = ShareOptions.availableOptions
    var shareAction: ((ShareCarouselCollectionViewCell) -> ())?
    func setup(shareAction: ((ShareCarouselCollectionViewCell) -> ())?) {
        self.shareAction = shareAction
        self.setItemSize(size: CGSize(width: 120, height: 32))
        self.backgroundColor = Palette.ppColorGrayscale100.color
        let nibCell = UINib(nibName: "ShareCarouselCollectionViewCell", bundle: Bundle.main)
        self.register(nibCell, forCellWithReuseIdentifier: "cell")
        
        self.delegate = self
        self.dataSource = self
        
        self.reloadData()
    }
}

// MARK: Delegate
extension ShareCarouselCollectionView: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? ShareCarouselCollectionViewCell else {
            return
        }
        self.shareAction?(cell)
    }
}

// MARK: Data Source
extension ShareCarouselCollectionView: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? ShareCarouselCollectionViewCell else {
                return UICollectionViewCell()
        }
        
        let type = data[indexPath.row]
        cell.configure(type: type)
        
        return cell
    }
}
