import CoreLegacy
import UI
import UIKit

final class NotificationBannerView: UIView {
    let notification: PPNotification
    
    var blurEffect: UIBlurEffect {
        if #available(iOS 13.0, *) {
            return UIBlurEffect(style: self.traitCollection.userInterfaceStyle == .dark ? .dark : .light)
        } else {
            return UIBlurEffect(style: .light)
        }
    }
    
    init(frame: CGRect, notification: PPNotification) {
        self.notification = notification
        super.init(frame: frame)
        self.setup()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /**
     Show the notification as banner
     
     ‼️ **OBS.:** Show banner only if the alert text is not empty
     */
    @objc
    static func showNotification(_ notification: PPNotification, superview: UIView) {
        var showAlertBanner = true
        
        // To prevent the banner from appearing when the feed item screen is open
        if let name = notification.landingScreenStringName,
            name == "p2p_comments",
            let param2 = notification.param2,
            param2 == AppParameters.global().currentOpenFeedItem {
            showAlertBanner = false
        }
        
        if showAlertBanner && !notification.alert.isEmpty {
            guard let labelText = notification.alert else {
                return
            }
            
            var height = NotificationBannerView.estimatedHeightOfLabel(text: labelText)
            
            if !notification.title.isEmpty {
                height += 55 // padding
            } else {
                height += 32 // padding
            }
            
            let frame = CGRect(x: 12, y: -64, width: UIScreen.main.bounds.size.width - 24, height: height)
            let bannerView = NotificationBannerView(frame: frame, notification: notification)
            
            superview.addSubview(bannerView)
            
            var top: CGFloat = 20
            if #available(iOS 11.0, *) {
                top = max(top, superview.safeAreaInsets.top)
            }
            
            // show banner with animation
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseOut, animations: {
                var frame = bannerView.frame
                frame.origin.y = top
                bannerView.frame = frame
            }, completion: { _ in })
            
            // Auto close
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double((Int64)(5 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC)) {
                bannerView.close()
            }
        }
    }
    
    @objc
    static func estimatedHeightOfLabel(text: String) -> CGFloat {
        let constraintRect = CGSize(width: UIScreen.main.bounds.size.width - 64, height: .greatestFiniteMagnitude)
        let boundingBox = String(text).boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: UIFont.systemFont(ofSize: 14)], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    @objc
    func openNotification() {
        self.close()
        SessionManager.sharedInstance.openNotification(self.notification)
    }
    
    func setup() {
        self.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        self.isUserInteractionEnabled = true
        self.layer.cornerRadius = 15
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openNotification)))
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.layer.cornerRadius = 15
        blurEffectView.clipsToBounds = true
        self.addSubview(blurEffectView)
        
        guard let labelText = notification.alert else {
            return
        }
        
        let height = NotificationBannerView.estimatedHeightOfLabel(text: labelText)
        
        var labelYPos: CGFloat = 16
        
        if notification.title != nil {
            let pushAlertTitle = UILabel(frame: CGRect(x: 16, y: 16, width: self.frame.size.width - 20, height: 20))
            pushAlertTitle.numberOfLines = 1
            pushAlertTitle.font = UIFont.boldSystemFont(ofSize: 14)
            pushAlertTitle.lineBreakMode = .byWordWrapping
            pushAlertTitle.text = notification.title
            pushAlertTitle.textColor = Palette.ppColorGrayscale500.color
            self.addSubview(pushAlertTitle)
            labelYPos = 40
        }
        
        let pushAlertLabel = UILabel(frame: CGRect(x: 16, y: labelYPos, width: self.frame.size.width - 40, height: height))
        pushAlertLabel.numberOfLines = 0
        pushAlertLabel.lineBreakMode = .byTruncatingMiddle
        pushAlertLabel.attributedText = labelText.html2Attributed(font: UIFont.systemFont(ofSize: 14))
        pushAlertLabel.textColor = Palette.ppColorGrayscale500.color
        
        self.addSubview(pushAlertLabel)
    }
    
    func close() {
        // hide banner with animation
        UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseOut, animations: {
            var frame = self.frame
            frame.origin.y = frame.size.height * (-1)
            self.frame = frame
        }, completion: { _ in
            self.removeFromSuperview()
        })
    }
}
