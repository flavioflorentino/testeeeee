//
//  GreenAugustShareImageVIew.swift
//  PicPay
//
//  Created by Luiz Henrique Guimarães on 07/08/17.
//
//

import UIKit

final class GreenAugustShareImageView: NibView {

    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var profileImagem: UICircularImageView!
    @IBOutlet weak var feedCardView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    
    let gradientLayer: CAGradientLayer
    
    init(frame: CGRect, store: String) {
        gradientLayer = CAGradientLayer()
        
        super.init(frame: frame)
        
        backgroundView.layer.insertSublayer(gradientLayer, at: 0)
        configureView(store: store)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureView(store: String){
        
        let username = ConsumerManager.shared.consumer?.username ?? ""
        let text = "<b>@\(username)</b> pagou a <br><b>@\(store)</b>"
        textLabel.attributedText = text.htmlStringToAttributedString()?.attributedPicpayFont(24.0)
        
        view.setNeedsLayout()
        view.layoutIfNeeded()
        
        if let urlString = ConsumerManager.shared.consumer?.img_url {
            profileImagem.setImage(url: URL(string: urlString), placeholder: PPContact.photoPlaceholder())
        }
        
        gradientLayer.frame = backgroundView.bounds
        gradientLayer.colors = [UIColor(red:0.15, green:0.89, blue:0.33, alpha:1.00).cgColor, UIColor(red:0.10, green:0.78, blue:0.55, alpha:1.00).cgColor]
        gradientLayer.startPoint = CGPoint(x:0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        
        feedCardView.layer.cornerRadius = 10.0
        feedCardView.clipsToBounds = true
        
        
    }
    
}
