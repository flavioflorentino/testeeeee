import CoreLegacy
import UIKit
import SnapKit
import FeatureFlag

protocol PeopleSearchMGMHeaderDelegate: AnyObject {
    func peopleSearchMGMDidTapUser()
    func peopleSearchMGMDidTapBusiness()
}

final class PeopleSearchMGMHeader: NibView {

    @IBOutlet weak var mgmText: UILabel!
    @IBOutlet weak var mgbText: UILabel!
    @IBOutlet weak var mgmImage: UIImageView!
    @IBOutlet weak var mgmView: UIView!
    @IBOutlet weak var mgbView: UIView!
    
    weak var delegate: PeopleSearchMGMHeaderDelegate?
    
    // NARK: - Initializer
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Configure Methods
    
    func setup(){
        configureView()
        configureMgmData()
    }
    
    fileprivate func configureView(){
        mgmView.isUserInteractionEnabled = true
        mgmView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openMGMShare(_:))))
        
        mgbView.isUserInteractionEnabled = true
        mgbView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openMGBShare(_:))))
    }
    
    func configureMgmData(){
        if let configs = AppParameters.global().mgmConfigs {
            mgmText.text = configs.mgmScreenSubtitle ?? ""
            mgbText.text = configs.mgbScreenShortTextShare ?? ""
        }
        
        if FeatureManager.isActive(.mGMCode){
            mgmImage.image = #imageLiteral(resourceName: "ico_mgm_invite")
        }else{
            mgmImage.image = #imageLiteral(resourceName: "ilu_invite_friends")
        }
        
        if !FeatureManager.isActive(.mGB){
            for v in mgbView.subviews {
                v.removeFromSuperview()
            }
            mgbView.snp.makeConstraints { (make) in
                make.height.equalTo(0)
            }
            mgbView.clipsToBounds = true
        }
    }
    
    // MARK: - User Action
    
    @IBAction private func openMGMShare(_ sender: Any? = nil) {
        delegate?.peopleSearchMGMDidTapUser()
    }
    
    @IBAction private func openMGBShare(_ sender: Any? = nil) {
        if FeatureManager.isActive(.mGB){
            delegate?.peopleSearchMGMDidTapBusiness()
        }
    }
}
