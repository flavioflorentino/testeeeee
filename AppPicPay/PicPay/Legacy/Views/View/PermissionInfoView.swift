import UI
import UIKit

final class PermissionInfoView: NibView {
    
    // MARK: - IBOutlets
    @IBOutlet weak var bg: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var button: UIPPButton!
    
    // MARK: - Life Cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Palette.ppColorGrayscale000.color
        textLabel.textColor = Palette.ppColorGrayscale600.color
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
