//
//  FeedFilterView.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 21/11/16.
//
//

import UI
import UIKit

@objc enum FeedFilterOptions:NSInteger{
    case friends = 1
    case `private` = 2
}

@objc protocol FeedFilterDelegate {
    func feedFilterOptionSelected(_ option:FeedFilterOptions)
}

final class FeedFilterView: UIView {
    fileprivate var checkImageView:UIImageView?
    
    fileprivate var friendsIcon:UIImageView?
    fileprivate var friendsLabel:UILabel?
    fileprivate var friendsCheckIcon:UIImageView?
    
    fileprivate var privateIcon:UIImageView?
    fileprivate var privateLabel:UILabel?
    fileprivate var privateCheckIcon:UIImageView?
    
    weak var delegate:FeedFilterDelegate?
    
    fileprivate var box:UIView?
    
    init(frame:CGRect, delegate:FeedFilterDelegate){
        super.init(frame:frame)
        
        self.delegate = delegate
        
        self.backgroundColor = UIColor.clear
        self.clipsToBounds = false
        
        self.setup()
        self.selectOption(.friends, callDelegate:false)
        
        self.isHidden = true
        self.layer.opacity = 0.0
        box?.layer.opacity = 0.0
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Private Methods
    fileprivate func setup(){
        var y:CGFloat = 5.0
        var x:CGFloat = 10.0
        let padding:CGFloat = 5.0
        var width:CGFloat = 0.0
        var height:CGFloat = 0.0
        let iconSize = CGSize(width: 40.0, height: 45.0)
        
        box = UIView()
        
        // public
        width = iconSize.width
        height = iconSize.height
        friendsIcon = UIImageView(image: UIImage(named: "ico_filter_friends")?.withRenderingMode(.alwaysTemplate))
        friendsIcon?.frame = CGRect(x: x, y: y, width: width, height: height)
        friendsIcon?.contentMode = .center
        x = x +  width + padding + padding
        
        width = self.frame.size.width - 40
        height = iconSize.height
        friendsLabel = UILabel(frame: CGRect(x: x,y: y,width: width, height: height))
        friendsLabel?.text = "Todos"
        
        friendsCheckIcon = UIImageView(image: UIImage(named: "checkmark_green"))
        friendsCheckIcon?.frame = CGRect(x: self.frame.size.width - 30 - iconSize.width, y: y, width: iconSize.width, height: iconSize.height)
        friendsCheckIcon?.contentMode = .center
        friendsCheckIcon?.isHidden = true
        y = y + height + padding
        
        // =======
        
        x = 0.0
        
        // separador
        let separator = UIView(frame: CGRect(x: x,y: y,width: self.frame.width - 20.0,height: 1.0))
        separator.backgroundColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.00)
        y = y + 1.0 + padding
        // =======
        
        x = 10.0
        // public
        width = iconSize.width
        height = iconSize.height
        privateIcon = UIImageView(image: UIImage(named: "ico_filter_private")?.withRenderingMode(.alwaysTemplate))
        privateIcon?.frame = CGRect(x: x, y: y, width: width, height: height)
        privateIcon?.contentMode = .center
        x = x +  width + padding + padding
            
        width = self.frame.size.width - 40
        height = iconSize.height
        privateLabel = UILabel(frame: CGRect(x: x,y: y,width: width, height: height))
        privateLabel?.text = "Você"
        
        privateCheckIcon = UIImageView(image: UIImage(named: "checkmark_green"))
        privateCheckIcon?.frame = CGRect(x: self.frame.size.width - 30 - iconSize.width, y: y, width: iconSize.width, height: iconSize.height)
        privateCheckIcon?.contentMode = .center
        privateCheckIcon?.isHidden = true
        
        y = y + height + padding
        // =======
        
        // Imagem up Arrow
        let arrow = UIImageView(image:UIImage(named:"feed_opt_arrow"))
        arrow.frame = CGRect( x: (self.frame.width/2) - 20.0, y: -10.0, width: 28.0, height: 11.0)
        arrow.contentMode = .center
        
        // overlay
        let overlay:UIView = UIView(frame:self.frame)
        overlay.layer.backgroundColor = Palette.ppColorGrayscale600.cgColor
        overlay.layer.opacity = 0.5
        overlay.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hide)))
        self.addSubview(overlay)
        
        // add objects
        box?.addSubview(arrow)
        box?.addSubview(friendsIcon!)
        box?.addSubview(friendsLabel!)
        box?.addSubview(friendsCheckIcon!)
        box?.addSubview(separator)
        box?.addSubview(privateIcon!)
        box?.addSubview(privateLabel!)
        box?.addSubview(privateCheckIcon!)
        
        // add gesture 
        friendsIcon?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectFriends)))
        friendsIcon?.isUserInteractionEnabled = true
        friendsLabel?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectFriends)))
        friendsLabel?.isUserInteractionEnabled = true
        privateIcon?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectPrivate)))
        privateIcon?.isUserInteractionEnabled = true
        privateLabel?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectPrivate)))
        privateLabel?.isUserInteractionEnabled = true
        
        box?.frame = CGRect(x: 10, y: 14.0 , width: self.frame.size.width-20, height: y)
        box?.backgroundColor = Palette.ppColorGrayscale000.color
        box?.layer.cornerRadius = 10.0
        box?.clipsToBounds = false
        self.addSubview(box!)
    }
    
    // MARK: UserActions
    
    @objc func selectFriends(){
        self.selectOption(.friends)
    }
    
    @objc func selectPrivate(){
        self.selectOption(.private)
    }
    
    /**
     Select the activit filter
    */
    func selectOption(_ option:FeedFilterOptions, callDelegate:Bool = true){
        if callDelegate {
            self.delegate?.feedFilterOptionSelected(option)
        }
            
        if option == FeedFilterOptions.private{
            self.privateCheckIcon?.isHidden = false
            self.privateLabel?.textColor = Palette.ppColorBranding300.color
            self.privateIcon?.tintColor = Palette.ppColorBranding300.color
            
            self.friendsCheckIcon?.isHidden = true
            self.friendsLabel?.textColor = UIColor.gray
            self.friendsIcon?.tintColor = UIColor.gray
            
        }else{
            self.friendsCheckIcon?.isHidden = false
            self.friendsLabel?.textColor = Palette.ppColorBranding300.color
            self.friendsIcon?.tintColor = Palette.ppColorBranding300.color
            
            self.privateCheckIcon?.isHidden = true
            self.privateLabel?.textColor = UIColor.gray
            self.privateIcon?.tintColor = UIColor.gray
        }
        
        //self.hide()
    }
    
    /**
     Hide the view
     */
    @objc func hide(){
        UIView.animate(withDuration: 0.25, animations: {
            self.box?.layer.opacity = 0.0
        }) 
        UIView.animate(withDuration: 0.5, animations: { 
            self.layer.opacity = 0.0
            }, completion: { (completed) in
                if completed {
                    self.isHidden = true
                    self.removeFromSuperview()
                }
        }) 
    }
    
    /**
     Shown the view at superview
    */
    func show(_ superview:UIView){
        if (self.isHidden){
            
            superview.addSubview(self)
            
            self.isHidden = false
            self.box?.isHidden = false
            
            UIView.animate(withDuration: 0.1, animations: {
                self.layer.opacity = 1.0
            }) 
            UIView.animate(withDuration: 0.2, animations: {
                self.box?.layer.opacity = 1.0
            }) 
        }
        
        
    }
}
