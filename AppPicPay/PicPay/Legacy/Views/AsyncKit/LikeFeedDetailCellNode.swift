import UI
import UIKit
import AsyncDisplayKit

protocol LikeFeedDetailCellNodeDelegate: AnyObject{
    func likeButtonDidTap(_ button:ASImageNode?, like:Bool)
    func likeButtonDidTapOnText(_ textNode:ASTextNode?)
}

final class LikeFeedDetailCellNode: ASCellNode {
    var likeImageNode:ASImageNode?
    var likeTextNode:ASTextNode?
    
    var background:ASDisplayNode?
    var feedItem:DefaultFeedItem
    
    weak var delegate:LikeFeedDetailCellNodeDelegate?
    
    init(feedItem:DefaultFeedItem, delegate:LikeFeedDetailCellNodeDelegate?) {
        self.feedItem = feedItem
        self.delegate = delegate
        super.init()
        setup()
    }
    
    // MARK: - Methods
    
    func setup(){
        self.selectionStyle = .none
        self.backgroundColor = Palette.ppColorGrayscale100.color
        
        // Layout Itens ---------
        background = ASDisplayNode()
        background?.backgroundColor = Palette.ppColorGrayscale000.color
        background?.isLayerBacked = true
        self.addSubnode(background!)
        
        // Like ---------
        if self.feedItem.like != nil {
            // Icon ----
            likeImageNode = ASImageNode()
            likeImageNode?.style.preferredSize = CGSize(width: 24, height: 24);
            likeImageNode?.contentMode = .center
            likeImageNode?.addTarget(self, action: #selector(touchLikeButton), forControlEvents: ASControlNodeEvent.touchUpInside)
            self.addSubnode(likeImageNode!)
            
            // Text
            
            likeTextNode = ASTextNode()
            likeTextNode?.maximumNumberOfLines = 0
            likeTextNode?.addTarget(self, action: #selector(touchText), forControlEvents: ASControlNodeEvent.touchUpInside)
            adjustStateLikeButton()
            self.addSubnode(likeTextNode!)
        }
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        var vertialChildren = [ASLayoutElement]()
        
        // Like
        if let like = likeTextNode{
            like.style.alignSelf = .center
            like.style.flexGrow = 1.0
            like.style.flexShrink = 1.0
                
            let likeLayout = ASStackLayoutSpec.horizontal()
            likeLayout.spacing = 2.0
            likeLayout.alignItems = .start
            likeLayout.children = [likeImageNode!,like]
            vertialChildren.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 8, left: 25, bottom: 8, right: 12), child: likeLayout))
        }
        
        // LAYOUT CARD ---------
        let verticalLayout = ASStackLayoutSpec.vertical()
        verticalLayout.spacing = 5.0
        verticalLayout.style.flexShrink = 1.0
        verticalLayout.style.flexGrow = 1.0
        verticalLayout.verticalAlignment = .center
        verticalLayout.children = vertialChildren
        
        let backgrondLayout = ASBackgroundLayoutSpec(child: verticalLayout, background: background!)
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 1, left: 0, bottom: 1, right: 0), child: backgrondLayout)
    }
    
    @objc func touchLikeButton(){
        var like = true
        if let likeChecked = self.feedItem.like?.checked{
            like = !likeChecked
            
            // optimistic feedback
            self.feedItem.like?.checked = like
            
            self.adjustStateLikeButton()
        }
        
        self.delegate?.likeButtonDidTap(self.likeImageNode, like:like)
    }
    
    @objc func touchText(){
        self.delegate?.likeButtonDidTapOnText(self.likeTextNode)
    }
    
    /**
     Applies the correct image for like state
     */
    func adjustStateLikeButton(){
        if let like = self.feedItem.like {
            let icon = like.checked ? "ico_event_liked" : "ico_event_unliked"
            self.likeImageNode?.image = UIImage(named: icon)
            
            let attr = NSAttributedString(string: like.text, attributes: [.foregroundColor: Palette.ppColorGrayscale300.color as Any, .font: UIFont.systemFont(ofSize: 12)])
            likeTextNode?.attributedText = attr
        }
    }
}
