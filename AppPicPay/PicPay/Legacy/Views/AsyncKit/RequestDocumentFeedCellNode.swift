//
//  RequestDocumentFeedCellNode.swift
//  PicPay
//
//  Created by Luiz Henrique Guimaraes on 24/11/16.
//
//

import UI
import UIKit
import AsyncDisplayKit

final class RequestDocumentFeedCellNode: PinnedFeedCellNode {
    var button:ASButtonNode?
    
    override init(feedItem:DefaultFeedItem) {
        super.init(feedItem: feedItem)
        
        let bgColor = Palette.ppColorNegative300.color
        
        button = ASButtonNode()
        button?.style.height = ASDimensionMake(25.0)
        button?.setTitle("Enviar documentos agora", with: UIFont.boldSystemFont(ofSize: 12.0), with: bgColor, for: .normal)
        button?.backgroundColor = Palette.white.color
        button?.cornerRadius = 3.0
        addSubnode(button!)
        
        background?.backgroundColor = bgColor
        background?.borderColor = Palette.ppColorGrayscale300.color(withCustomDark: .ppColorGrayscale600).cgColor
    }
    
    /**
     Return the children for vertical Layout
     */
    override func verticalLayoutChildren() -> [ASLayoutElement] {
        var verticalChildren = super.verticalLayoutChildren()
        
        button?.style.flexGrow = 1.0
        verticalChildren.append(button!)
        
        return verticalChildren
    }
}
