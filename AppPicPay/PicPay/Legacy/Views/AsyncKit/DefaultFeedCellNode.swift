import AsyncDisplayKit
import Atributika
import FeatureFlag
import UI
import UIKit

@objc protocol DefaultFeedCellNodeDelegate: AnyObject{
    func feedDidTapOnProfile(_ contact:PPContact?)
    func feedDidTapOnStoreProfile(_ store:PPStore?)
    func feedDidTapOnDigitalGoodProfile(_ digitalGood: DGItem?)
    func feedDidTapOnProducerProfile(_ producerId: String)
    func feedDidTapOnBoletoProfile()
    func feedDidTapOnP2MProfile()
    @objc optional func shareMessage(_ message: String, feedItem: DefaultFeedItem)
    @objc optional func feedDidTabOnButton(feedItemId:String, dataId:String, actionType:String)
    @objc optional func feedDidTabOnButton(feedItemId: String, url: URL)
    @objc optional func feedDidTapOnLike(_ like:Bool, feedItem:DefaultFeedItem)
    @objc optional func feedDidTapOnComment(_ feedItem:DefaultFeedItem)
}

enum ButtonNodeDimension: String {
    case half = "50%"
    case full = "100%"
}

extension DefaultFeedCellNode.Layout {
    enum Position {
        static let badgePosition = CGPoint(x: 10, y: 41)
        static let verifiedPosition = CGPoint(x: 36, y: 38)
    }
}

final class DefaultFeedCellNode: BaseFeedCellNode, ASTextNodeDelegate {
    fileprivate enum Layout { }
    typealias Dependencies = HasFeatureManager
    private var dependencies: Dependencies
    
    var feedItem:DefaultFeedItem
    
    var imageNode:PlaceholderNetworkImageNode?
    var verifiedImageNode:ASImageNode?
    var proImageNode:ASImageNode?
    var studentAccountImageNode: ASImageNode?
    var imageMask:ASImageNode?
    var titleTextNode:ASTextNode?
    var textNode:ASTextNode?
    var analysisTextNode: ASTextNode?
    var alertImageNode:ASImageNode?
    var alertTextNode:ASTextNode?
    var valueTextNode:ASTextNode?
    var timeAgoImageNode:ASImageNode?
    var timeAgoTextNode:ASTextNode?
    var commentImageNode:ASImageNode?
    var commentTextNode:ASTextNode?
    var likeImageNode:ASImageNode?
    var likeTextNode:ASTextNode?
    var primaryButtonNode: ActionButtonNode?
    var secondaryButtonNode: ActionButtonNode?
    var deprecatedButtonNode: ActionButtonNode?
    var analysisButtonNode: ActionButtonNode?
    var knowMoreButtonNode: ActionButtonNode?
    
    var separator:ASDisplayNode?
    var optionsActionImageNode:ASImageNode?
    
    var background:ASDisplayNode?
    var detail:Bool = false
    
    lazy var bottomLine: ASDisplayNode = {
        let bottomLine: ASDisplayNode = ASDisplayNode()
        bottomLine.backgroundColor = Palette.ppColorGrayscale200.color
        bottomLine.style.height = ASDimensionMake(1.0)
        bottomLine.style.layoutPosition = CGPoint(x: 30.0, y: 30.0)
        return bottomLine
    }()
    
    lazy var containerAlert: ASDisplayNode = {
        let viewNode = ASDisplayNode()
        viewNode.isLayerBacked = true
        viewNode.backgroundColor = Palette.ppColorGrayscale200.color(withCustomDark: .ppColorGrayscale000Dark)
        viewNode.layer.cornerRadius = 4
        viewNode.clipsToBounds = true
        return viewNode
    }()
    
    weak var delegate:DefaultFeedCellNodeDelegate?
    
    // MARK: - Initializer
    
    init(feedItem:DefaultFeedItem, delegate:DefaultFeedCellNodeDelegate){
        self.delegate = delegate
        self.feedItem = feedItem
        self.dependencies = DependencyContainer()
        super.init()
        setup()
    }
    
    init(feedItem:DefaultFeedItem, delegate:DefaultFeedCellNodeDelegate, detail:Bool) {
        self.delegate = delegate
        self.feedItem = feedItem
        self.detail = detail
        self.dependencies = DependencyContainer()
        super.init()
        setup()
    }
    
    // MARK: - Methods
    
    func setup() {
        // Layout Itens ---------
        background = ASDisplayNode()
        background?.isLayerBacked = true
        background?.cornerRadius = 6.0
        
        if !self.detail {
            background?.backgroundColor = Colors.backgroundPrimary.color
            background?.shadowColor = Palette.ppColorGrayscale400.cgColor
            background?.shadowOffset = CGSize(width: 0, height: 0)
            background?.shadowOpacity = 1
            background?.shadowRadius = 1
            background?.clipsToBounds = false
        }
        
        self.addSubnode(background!)
        
        separator = ASDisplayNode()
        separator?.backgroundColor = Palette.ppColorGrayscale300.color
        separator?.style.preferredSize = CGSize(width: 1.0, height: 10.0)
        separator?.isLayerBacked = true
        self.addSubnode(separator!)
        
        // Image ----------
        imageNode = PlaceholderNetworkImageNode()
        imageNode?.contentMode = .scaleAspectFit
        imageNode?.style.preferredSize = CGSize(width: 48, height: 48)
        imageNode?.style.alignSelf = .start
        imageNode?.placeholderEnabled = true
        imageNode?.placeholderFadeDuration = 0.0
        imageNode?.clipsToBounds = true
        imageNode?.cornerRadius = 24.0
        imageNode?.defaultImage = UIImage.as_resizableRoundedImage(withCornerRadius: 26.0, cornerColor: Palette.ppColorGrayscale000.color, fill: Palette.ppColorGrayscale200.color)
        imageNode?.addTarget(self, action: #selector(touchImage), forControlEvents: ASControlNodeEvent.touchUpInside)
        
        if let image = self.feedItem.image{
            if let url = URL(string: image.url){
                imageNode?.url = url
            }
        }
        
        self.addSubnode(imageNode!)
        
        // title ---------
        titleTextNode = ASTextNode()
        titleTextNode?.maximumNumberOfLines = 0
        titleTextNode?.linkAttributeNames = [TextLink.Username.rawValue]
        titleTextNode?.delegate = self
        titleTextNode?.isUserInteractionEnabled = true
        titleTextNode?.passthroughNonlinkTouches = true
        
        let fontTitleSize: CGFloat = self.detail ? 16 : 14
        let titleFont = UIFont.systemFont(ofSize: fontTitleSize, weight: .light)
        let titleColor = Palette.ppColorGrayscale600.color
        let titleText = StringHtmlTextHelper.htmlToAttributedString(
            string: feedItem.title.value,
            font: titleFont,
            foregroundColor: titleColor
        )
        let titleAttributedString = titleText
        titleTextNode?.attributedText = BaseFeedCellNode.convertUsernameLinks(feedItem.title, attributeString: titleAttributedString, detail: detail)
        self.addSubnode(titleTextNode!)
        
        // text ---------
        if let text = self.feedItem.text, !text.value.isEmpty {
            setupText(textValue: text.value)
        }
        
        if detail, let transactionStatus = feedItem.transactionStatus {
            setupAnalysisContent(transactionStatus: transactionStatus)
        }
        
        // Alert ----------
        setupAlertWidget(feedItem.alert)
        
        // Button ----------
        if !detail {
            setupButtons()
        }
        
        // Value / Money ---------
        if let money = self.feedItem.money{
            valueTextNode = ASTextNode()
            valueTextNode?.maximumNumberOfLines = 1
            
            let fontMoney = UIFont.systemFont(ofSize: self.detail ? 14 : 12, weight: .light)
            let moneyText = StringHtmlTextHelper.htmlToAttributedString(string: money.value, font: fontMoney)

            valueTextNode?.attributedText = moneyText
            valueTextNode?.isLayerBacked = true
            self.addSubnode(valueTextNode!)
        }
        
        // Time Ago ---------
        if let timeAgo = self.feedItem.timeAgo {
            // Text
            timeAgoTextNode = ASTextNode()
            timeAgoTextNode?.maximumNumberOfLines = 1
            timeAgoTextNode?.truncationMode = .byTruncatingTail
            let font = UIFont.systemFont(ofSize: self.detail ? 14 : 12, weight: .light)
            let color = Palette.ppColorGrayscale400.color
            let timeText = StringHtmlTextHelper.htmlToAttributedString(
                string: timeAgo.value,
                font: font,
                foregroundColor: color
            )
            timeAgoTextNode?.attributedText = timeText
            timeAgoTextNode?.isLayerBacked = true
            self.addSubnode(timeAgoTextNode!)
            
            if let timeAgoIcon = self.feedItem.timeAgoIcon {
                // Icon ----
                timeAgoImageNode = ASImageNode()
                timeAgoImageNode?.image = self.timeAgoIconImage(timeAgoIcon.icon, color:timeAgoIcon.color)
                timeAgoImageNode?.style.preferredSize = CGSize(width: 18, height: 18);
                timeAgoImageNode?.isLayerBacked = true
                self.addSubnode(timeAgoImageNode!)
            }
        }
        
        // Comment ---------
        if let comment = self.feedItem.comments, detail == false{
            let icon = comment.checked ? "ico_comment_notified" : "ico_comment"
            // Icon ----
            commentImageNode = ASImageNode()
            commentImageNode?.image = UIImage(named: icon)
            commentImageNode?.style.preferredSize = CGSize(width: 35, height: 40);
            commentImageNode?.contentMode = .center
            commentImageNode?.addTarget(self, action: #selector(touchCommentIcon), forControlEvents: .touchUpInside)
            self.addSubnode(commentImageNode!)
            
            // Text
            var num = "0"
            if comment.value != "0"{
                num = comment.value
            }
            
            commentTextNode = ASTextNode()
            commentTextNode?.maximumNumberOfLines = 1
            commentTextNode?.style.minWidth = ASDimensionMake(14)
            
            let commentColor = Palette.ppColorGrayscale400.color
            let commentFont = UIFont.systemFont(ofSize: 14, weight: .light)
            let commentText = StringHtmlTextHelper.htmlToAttributedString(
                string: num,
                font: commentFont,
                foregroundColor: commentColor
            )
            
            commentTextNode?.attributedText = commentText
            self.addSubnode(commentTextNode!)
        }
        // Like ---------
        if (self.feedItem.like != nil && detail == false) {
            
            // Icon ----
            likeImageNode = ASImageNode()
            likeImageNode?.style.preferredSize = CGSize(width: 40, height: 40);
            likeImageNode?.contentMode = .center
            likeImageNode?.addTarget(self, action: #selector(like), forControlEvents: .touchUpInside)
            self.addSubnode(likeImageNode!)
            
            // Text
            likeTextNode = ASTextNode()
            likeTextNode?.maximumNumberOfLines = 1
            likeTextNode?.style.minWidth = ASDimensionMake(14)
            likeTextNode?.addTarget(self, action: #selector(like), forControlEvents: .touchUpInside)
            self.addSubnode(likeTextNode!)
            
            self.adjustStateLikeButton()
        }
        
        imageMask = ASImageNode()
        imageMask?.image = UIImage(named: "mask_feed_image")
        imageMask?.style.preferredSize = CGSize(width: 52, height: 52)
        imageMask?.style.layoutPosition = CGPoint(x: 0, y: 0)
        imageMask?.contentMode = .scaleToFill
        imageMask?.backgroundColor = UIColor.clear
        imageMask?.addTarget(self, action: #selector(touchImage), forControlEvents: ASControlNodeEvent.touchUpInside)
        self.addSubnode(imageMask!)
        
        verifiedImageNode = ASImageNode()
        verifiedImageNode?.image = UIImage(named: "ico_verified_feed")
        verifiedImageNode?.style.preferredSize = CGSize(width: 17, height: 17)
        verifiedImageNode?.contentMode = .scaleAspectFit
        verifiedImageNode?.addTarget(self, action: #selector(touchImage), forControlEvents: ASControlNodeEvent.touchUpInside)
        verifiedImageNode?.isHidden = true
        self.addSubnode(verifiedImageNode!)
        
        if ( self.feedItem.image?.icon == "verified" ) {
            verifiedImageNode?.isHidden = false
        }
        
        proImageNode = ASImageNode()
        proImageNode?.image = UIImage(named: "ico_pro_badge_feed")
        proImageNode?.style.preferredSize = CGSize(width: 28, height: 16)
        proImageNode?.contentMode = .scaleAspectFit
        proImageNode?.addTarget(self, action: #selector(touchImage), forControlEvents: ASControlNodeEvent.touchUpInside)
        proImageNode?.isHidden = true
        self.addSubnode(proImageNode!)
        
        if ( self.feedItem.image?.icon == "pro") {
            proImageNode?.isHidden = false
        }
        
        setupStudentAccountBadge()
        
        // Feed Options
        optionsActionImageNode = ASImageNode()
        optionsActionImageNode?.image = UIImage(named: "ico_verified_feed")
        optionsActionImageNode?.style.preferredSize = CGSize(width: 20, height: 20)
        optionsActionImageNode?.contentMode = .scaleToFill
        self.addSubnode(optionsActionImageNode!)
        self.addSubnode(bottomLine)
    }
    
    private func setupAnalysisContent(transactionStatus: TransactionStatus) {
        if transactionStatus == .autoClearance,
            let primaryData = feedItem.buttons[.primary],
            let secondaryData = feedItem.buttons[.secondary] {
            let analysisOldUrl = primaryData.actionData?["url"] as? String
            let knowMoreOldUrl = secondaryData.actionData?["url"] as? String
            primaryData.actionData?["url"] = replaceAnalysisDeeplinkUrl(analysisOldUrl)
            secondaryData.actionData?["url"] = replaceKnowMoreAnalysisDeeplinkUrl(knowMoreOldUrl)
            analysisTextNode = createAnalysisText(DefaultLocalizable.autoClearance.text)
            analysisButtonNode = createAnalysisButton(buttonData: primaryData)
            knowMoreButtonNode = createAnalysisButton(buttonData: secondaryData)
        } else if transactionStatus == .inAnalysis, let primaryData = feedItem.buttons[.primary] {
            let knowMoreOldUrl = primaryData.actionData?["url"] as? String
            primaryData.actionData?["url"] = replaceKnowMoreAnalysisDeeplinkUrl(knowMoreOldUrl)
            knowMoreButtonNode = createAnalysisButton(buttonData: primaryData)
            analysisTextNode = createAnalysisText(DefaultLocalizable.inAnalysis.text)
        }
    }
    
    private func replaceAnalysisDeeplinkUrl(_ url: String?) -> String? {
        let trackingFeedId = "8269021b259b5634e71fd32b8905f8a9"
        let trackingFeedDetailId = "8269021b259b5634e71fd32b8905f8b0"
        return url?.replacingOccurrences(of: trackingFeedId, with: trackingFeedDetailId)
    }
    
    private func replaceKnowMoreAnalysisDeeplinkUrl(_ url: String?) -> String? {
        let trackingFeedId = "14537bec96e8507ab80c7c68812f2485"
        let trackingFeedDetailId = "14537bec96e8507ab80c7c68812f2486"
        return url?.replacingOccurrences(of: trackingFeedId, with: trackingFeedDetailId)
    }
    
    private func createAnalysisText(_ text: String) -> ASTextNode {
        let newTextNode = ASTextNode()
        newTextNode.maximumNumberOfLines = 0
        let font = UIFont.systemFont(ofSize: 16, weight: .light)
        let textAttributed = StringHtmlTextHelper.htmlToAttributedString(string: text, font: font)
        newTextNode.attributedText = textAttributed
        addSubnode(newTextNode)
        return newTextNode
    }
    
    private func createAnalysisButton(buttonData: Button) -> ActionButtonNode {
        let button = ActionButtonNode()
        setupButton(button, buttonData: buttonData)
        return button
    }
    
    private func setupText(textValue: String) {
        let newTextNode = ASTextNode()
        newTextNode.maximumNumberOfLines = 0

        let isEmoji = textValue.length <= 3 && textValue.containsOnlyEmoji
        let font = (isEmoji && self.detail) ? UIFont.systemFont(ofSize: 32, weight: .regular) : UIFont.systemFont(ofSize: 16, weight: .light)
        let textAttributed = StringHtmlTextHelper.htmlToAttributedString(string: textValue, font: font)
        newTextNode.attributedText = textAttributed
        addSubnode(newTextNode)

        textNode = newTextNode
    }
    
    private func setupButtons() {
        if !feedItem.buttons.isEmpty {
            if let primaryButtonData = feedItem.buttons[.primary] {
                let newButtonNode = ActionButtonNode()
                primaryButtonNode = newButtonNode
                setupButton(newButtonNode, buttonData: primaryButtonData)
            }
            if let secondaryButtonData = feedItem.buttons[.secondary] {
                let newButtonNode = ActionButtonNode()
                secondaryButtonNode = newButtonNode
                setupButton(newButtonNode, buttonData: secondaryButtonData)
            }
        } else if let button = feedItem.deprecatedButton, !button.text.isEmpty {
            let newButtonNode = ActionButtonNode()
            deprecatedButtonNode = newButtonNode
            setupDeprecatedButton(newButtonNode, buttonData: button)
        }
    }

    private func setupButton(_ buttonNode: ActionButtonNode, buttonData: Button) {
        buttonNode.setupAppearance(title: buttonData.title, style: buttonData.type)
        buttonNode.addTarget(self, action: #selector(touchFeedButton(_:)), forControlEvents: .touchUpInside)
        buttonNode.action = buttonData.action
        buttonNode.actionDataUrl = buttonData.actionDataUrl
        buttonNode.actionDataMessage = buttonData.actionDataMessage
        addSubnode(buttonNode)
    }

    private func setupDeprecatedButton(_ buttonNode: ActionButtonNode, buttonData: ButtonFeedWidget) {
        buttonNode.setupAppearance(title: buttonData.text, customBackgroundColor: buttonData.color)
        buttonNode.addTarget(self, action: #selector(touchFeedButton(_:)), forControlEvents: .touchUpInside)
        buttonNode.actionString = buttonData.action
        buttonNode.actionData = buttonData.data
        buttonNode.style.alignSelf = .start
        addSubnode(buttonNode)
    }
    
    private func setupStudentAccountBadge() {
        let studentNode = ASImageNode()
        studentNode.image = Assets.Icons.icoStudentBadgeFeed.image
        studentNode.style.preferredSize = CGSize(width: 28, height: 16)
        studentNode.contentMode = .scaleAspectFit
        studentNode.addTarget(
            self,
            action: #selector(touchImage),
            forControlEvents: ASControlNodeEvent.touchUpInside
        )
        studentNode.isHidden = true
        addSubnode(studentNode)
        
        studentAccountImageNode = studentNode
        
        if feedItem.image?.icon == "student" {
            studentAccountImageNode?.isHidden = false
        }
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        var vertialChildren = [ASLayoutElement]()
        imageNode?.style.layoutPosition = CGPoint.zero
        proImageNode?.style.layoutPosition = Layout.Position.badgePosition
        studentAccountImageNode?.style.layoutPosition = Layout.Position.badgePosition
        verifiedImageNode?.style.layoutPosition = Layout.Position.verifiedPosition

        let imageLayout = ASAbsoluteLayoutSpec(
            children: [imageNode!, verifiedImageNode!, proImageNode!, studentAccountImageNode!]
        )
        
        // title
        if let title = titleTextNode {
            title.style.flexShrink = 1.0
            title.style.alignSelf = .stretch
            vertialChildren.append(title)
        }
        
        // text
        let commentTextLayout = ASStackLayoutSpec.vertical()
        if let text = textNode {
            commentTextLayout.spacing = 5.0
            commentTextLayout.style.flexShrink = 1.0
            commentTextLayout.style.layoutPosition = CGPoint(x: 20, y: 0)
            if constrainedSize.max.width > 0 {
                commentTextLayout.style.maxWidth = ASDimensionMake(constrainedSize.max.width)
            }
            commentTextLayout.children = [text]
        }
        
        // Alert
        let alertLayout = ASStackLayoutSpec.horizontal()
        if let alertText = alertTextNode {
            var alertChildren = [ASLayoutElement]()
            
            alertText.style.flexShrink = 1.0
            
            if alertImageNode != nil {
                alertImageNode?.style.alignSelf = .start
                let alertImage = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), child:alertImageNode!)
                alertChildren.append(alertImage)
            }
            alertChildren.append(alertText)
            
            alertLayout.spacing = 5.0
            alertLayout.style.flexShrink = 1.0
            alertLayout.style.alignSelf = .start
            alertLayout.children = alertChildren
        }
        
        // Bottom -----------
        var bottomChildren = [ASLayoutElement]()
        
        // Money
        if let money = valueTextNode{
            money.style.flexShrink = 0.0
            money.style.alignSelf = .center
            
            if timeAgoImageNode == nil && timeAgoTextNode == nil{
                money.style.flexGrow = 1.0
            }
                
            bottomChildren.append(money)
        }
        
        // Separador
        if timeAgoImageNode != nil || timeAgoTextNode != nil{
            
            if valueTextNode != nil{
                separator?.style.alignSelf = .center
                bottomChildren.append(separator!)
            }
            
            var timeAgoChildren = [ASLayoutElement]()
            
            // Time Ago Icon
            if let timeAgoIcon = timeAgoImageNode{
                timeAgoIcon.style.alignSelf = .center
                timeAgoIcon.contentMode = .scaleAspectFit
                timeAgoChildren.append(timeAgoIcon)
            }
            
            // Time Ago
            if let timeAgo = timeAgoTextNode{
                timeAgo.style.flexShrink = 1.0
                timeAgo.style.alignSelf = .center
                timeAgo.truncationMode = .byTruncatingTail
                timeAgoChildren.append(timeAgo)
            }
            
            let timeAgoLayout = ASStackLayoutSpec.horizontal()
            timeAgoLayout.spacing = 3.0
            timeAgoLayout.style.alignSelf = .center
            timeAgoLayout.style.flexShrink = 1.0
            timeAgoLayout.style.flexGrow = 1.0
            timeAgoLayout.children = timeAgoChildren
            bottomChildren.append(timeAgoLayout)
        }
        
        var actionsChildren = [ASLayoutElement]()
        
        // Comment
        if let comment = commentTextNode{
            comment.style.alignSelf = .center
            
            let commentLayout = ASStackLayoutSpec.horizontal()
            commentLayout.spacing = -2.0
            commentLayout.alignItems = .start
            commentLayout.children = [commentImageNode!,comment]
            actionsChildren.append(commentLayout)
        }
        
        // Like
        if let like = likeTextNode {
            like.style.alignSelf = .center
            
            let likeLayout = ASStackLayoutSpec.horizontal()
            likeLayout.spacing = -4.0
            likeLayout.alignItems = .start
            likeLayout.children = [likeImageNode!,like]
            actionsChildren.append(ASInsetLayoutSpec(insets: UIEdgeInsets.zero, child:likeLayout))
        }
        
        if actionsChildren.count > 0 {
            let actionsLayout = ASStackLayoutSpec.horizontal()
            actionsLayout.spacing = -1.0
            actionsLayout.alignItems = .start
            actionsLayout.children = actionsChildren
            bottomChildren.append(actionsLayout)
        }
        
        let bottomLayout = ASStackLayoutSpec.horizontal()
        bottomLayout.spacing = 5.0
        bottomLayout.style.flexShrink = 1.0
        bottomLayout.style.flexGrow = 1.0
        bottomLayout.style.maxWidth = ASDimensionMake(max(0, constrainedSize.max.width - 80.0))
        bottomLayout.children = bottomChildren
        
        // LAYOUT CARD ---------
        let textsLayout = ASStackLayoutSpec.vertical()
        textsLayout.spacing = 5.0
        textsLayout.style.flexShrink = 1.0
        textsLayout.style.minHeight = ASDimensionMake(50.0)
        
        let cardPadding:CGFloat = 106.0
        textsLayout.style.minWidth = ASDimensionMake(max(0, constrainedSize.max.width - cardPadding))
        textsLayout.verticalAlignment = .center
        textsLayout.children = vertialChildren
        
        let verticalLayout = ASStackLayoutSpec.vertical()
        verticalLayout.spacing = 0.0
        verticalLayout.style.flexShrink = 1.0
        verticalLayout.children = [textsLayout]
        
        // new line for comments
        let horizonatalCommentLayout = ASStackLayoutSpec()
        horizonatalCommentLayout.children = [commentTextLayout]
        let newLineLayout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 16, left: 16, bottom: 0, right: 16), child: horizonatalCommentLayout)
       
        // bottom info bar
        let horizonatalBottomLayout = ASStackLayoutSpec()
        horizonatalBottomLayout.children = [bottomLayout]
        let top: CGFloat
        let bottom: CGFloat
        if detail {
            top = 20
            bottom = Spacing.base02
        } else if actionsChildren.count == 0 {
            top = Spacing.base02
            bottom = Spacing.base02
        } else {
            top = 0
            bottom = Spacing.base00
        }
        let newLineInset = UIEdgeInsets(top: top, left: 16, bottom: bottom, right: 16)
        let newLineBottomLayout = ASInsetLayoutSpec(insets: newLineInset, child: horizonatalBottomLayout)
        
        // content layout
        let horizonatalLayout = ASStackLayoutSpec()
        horizonatalLayout.spacing = 10.0
        horizonatalLayout.style.layoutPosition = CGPoint(x: -20.0, y: -30.0)
        horizonatalLayout.children = [imageLayout, verticalLayout]
        let sideMargin: CGFloat = 16
        let finalLayout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 16, left: sideMargin, bottom: 0, right: sideMargin), child: horizonatalLayout)
        
        // bottom line
        bottomLine.style.width = ASDimensionMake(max(0, constrainedSize.max.width - 60))
        
        let verticalShadownLayout = ASStackLayoutSpec.vertical()
        verticalShadownLayout.spacing = 0.0
        verticalShadownLayout.style.flexShrink = 1.0
        
        verticalShadownLayout.children = [finalLayout]

        if textNode != nil {
            verticalShadownLayout.children?.append(newLineLayout)
        }
        
        layoutAlert(alertLayout: alertLayout, in: verticalShadownLayout)
        
        layoutAnalysisContent(constrainedSize: constrainedSize, verticalLayout: verticalShadownLayout)
        
        if !detail {
            layoutButtonsToFitIn(constrainedSize, verticalLayout: verticalShadownLayout)
        }

        verticalShadownLayout.children?.append(newLineBottomLayout)
        
        let backgrondLayout = ASBackgroundLayoutSpec(child: verticalShadownLayout, background: background!)
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 4, left: 8, bottom: 4, right: 8), child: backgrondLayout)
    }
    
    private func layoutAnalysisContent(constrainedSize: ASSizeRange, verticalLayout: ASStackLayoutSpec) {
        guard analysisTextNode != nil else {
            return
        }
        let analysisLayout = ASStackLayoutSpec.vertical()
        analysisLayout.spacing = Spacing.base02
        analysisLayout.style.flexShrink = 1.0
        analysisLayout.style.maxWidth = ASDimensionMake(constrainedSize.max.width)
        var children: [ASLayoutElement] = []
        if let analysisText = analysisTextNode {
            children.append(analysisText)
        }
        if let button = analysisButtonNode {
            children.append(button)
        }
        if let button = knowMoreButtonNode {
            children.append(button)
        }
        analysisLayout.children = children
        let finalAnalysisLayout = ASInsetLayoutSpec(
            insets: UIEdgeInsets(top: Spacing.base02, left: Spacing.base02, bottom: 0, right: Spacing.base02),
            child: analysisLayout
        )
        verticalLayout.children?.append(finalAnalysisLayout)
    }
    
    private func layoutButtonsToFitIn(_ constrainedSize: ASSizeRange, verticalLayout: ASStackLayoutSpec) {
        let spacingBetweenButtons: CGFloat = 12
        let horizontalLayout = ASStackLayoutSpec(direction: .horizontal, spacing: spacingBetweenButtons, justifyContent: .start, alignItems: .stretch, children: [])

        if !feedItem.buttons.isEmpty {
            if let primaryButton = primaryButtonNode {
                let buttonType = feedItem.buttons[.primary]?.type
                
                var dimension: ButtonNodeDimension = .half
                if buttonType == .ghost || buttonType == .ghostGray {
                    dimension = .full
                }
                
                layoutButtonToFitIn(primaryButton, horizontalLayout: horizontalLayout, dimension: dimension)
            }
            if let secondaryButton = secondaryButtonNode {
                layoutButtonToFitIn(secondaryButton, horizontalLayout: horizontalLayout, dimension: .half)
            }
        } else if let deprecatedButton = deprecatedButtonNode {
            layoutDeprecatedButtonToFitIn(deprecatedButton, constrainedSize: constrainedSize, horizontalLayout: horizontalLayout)
        } else {
            return
        }

        verticalLayout.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 16, left: 16, bottom: 0, right: 16 + spacingBetweenButtons), child: horizontalLayout))
    }

    private func layoutButtonToFitIn(_ buttonNode: ASButtonNode, horizontalLayout: ASStackLayoutSpec, dimension: ButtonNodeDimension) {
        buttonNode.style.minWidth = ASDimensionMake(dimension.rawValue)
        horizontalLayout.children?.append(ASInsetLayoutSpec(insets: .zero, child: buttonNode))
    }

    private func layoutDeprecatedButtonToFitIn(_ buttonNode: ASButtonNode, constrainedSize: ASSizeRange, horizontalLayout: ASStackLayoutSpec) {
        buttonNode.style.maxWidth =  ASDimensionMake(max(0, constrainedSize.max.width - 50))
        horizontalLayout.children?.append(ASInsetLayoutSpec(insets: .zero, child: buttonNode))
    }

    private func setupAlertWidget(_ alertWidget: AlertFeedWidget?) {
        guard let alertWidget = alertWidget else {
            return
        }
        setupAlertWidgetIcon(alertWidget.type)
        setupAlertWidgetText(alertWidget.text)
    }
    
    private func iconForAlertType(_ type: AlertFeedWidgetType) -> UIImage? {
        var iconName: String
        switch type {
        case .warning:
            iconName = "ico_feed_warning"
        case .processing:
            iconName = "icon_wait_filled"
        case .analysis:
            iconName = "icon_wait_filled_yellow"
        case .success:
            iconName = "confirm_check"
        case .failure:
            iconName = "cancelled_filled_icon"
        }
        
        return UIImage(named: iconName)
    }
    
    
    private func setupAlertWidgetIcon(_ type: AlertFeedWidgetType?) {
        guard let type = type else {
            return
        }
        
        if detail {
            addSubnode(containerAlert)
        }

        let imageNode = ASImageNode()
        imageNode.image = iconForAlertType(type)
        imageNode.style.preferredSize = CGSize(width: 12, height: 12);
        imageNode.isLayerBacked = true
        imageNode.contentMode = .scaleAspectFit
        alertImageNode = imageNode
        
        addSubnode(imageNode)
    }
    
    private func setupAlertWidgetText(_ alertWidgetText: String) {
        guard !alertWidgetText.isEmpty else {
            return
        }
        
        let textNode = ASTextNode()
        textNode.maximumNumberOfLines = 0
        let font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)
        let attributedText = StringHtmlTextHelper.htmlToAttributedString(string: alertWidgetText, font: font)
        textNode.attributedText = attributedText
        alertTextNode = textNode
        
        addSubnode(textNode)
    }
    
    /// Setup layout for alert message
    ///
    /// - Parameters:
    ///   - alertLayout: layout of alert
    ///   - verticalShadownLayout: verticalShadownLayout
    func layoutAlert(alertLayout: ASStackLayoutSpec, in verticalShadownLayout: ASStackLayoutSpec) {
        if alertTextNode != nil {
            /** Ainda não vai ser usado
            if detail {
                containerAlert.style.flexGrow = 1
                // Make layout for container alert message
                let containerAlertLayout = ASBackgroundLayoutSpec(child: alertLayout, background: containerAlert)
                verticalShadownLayout.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsetsMake(12, 12, 12, 12), child: containerAlertLayout))
            } else {
            */
            verticalShadownLayout.children?.append(ASInsetLayoutSpec(insets: UIEdgeInsets(top: 10, left: 16, bottom: 0, right: 16), child: alertLayout))
            //}
        }
    }
    
    // MARK: - Internal Methods
    
    /**
     Applies the correct image for like state
    */
    fileprivate func adjustStateLikeButton(){
        if let like = self.feedItem.like {
            let icon = like.checked ? "ico_heart_liked" : "ico_unliked_heart"
            self.likeImageNode?.image = UIImage(named: icon)
            
            let likeColor = like.checked ? Palette.ppColorBranding300.color : Palette.ppColorGrayscale400.color
            let likeFont = UIFont.systemFont(ofSize: 14, weight: .light)
            let likeText = StringHtmlTextHelper.htmlToAttributedString(
                string: like.value,
                font: likeFont,
                foregroundColor: likeColor
            )
            let attributedText = likeText
            likeTextNode?.attributedText = attributedText
        }
    }
    
    // MARK: - User Actions

    /**
    Method call when user press the button that comes with feed
    */
    @objc
    func touchFeedButton(_ sender: ActionButtonNode) {
        if let action = sender.action {
            switch action {
            case .deeplink:
                guard let url = sender.actionDataUrl else {
                    return
                }
                delegate?.feedDidTabOnButton?(feedItemId: feedItem.id, url: url)
            case .sendLink:
                guard let message = sender.actionDataMessage else {
                    return
                }
                delegate?.shareMessage?(message, feedItem: feedItem)
            default:
                return
            }
        } else if let deprecatedButton = feedItem.deprecatedButton {
            delegate?.feedDidTabOnButton?(feedItemId: feedItem.id, dataId: deprecatedButton.data,actionType: deprecatedButton.action)
        }
    }
    
    
    /**
     Method called when user tap in main image
    */
    @objc func touchImage(){
        // Execute the action according type
        if let action = self.feedItem.image?.action {
            switch action.type {
            case .OpenConsumerProfile:
                if let contactIdString = self.feedItem.image?.action?.data as? String{
                    if let contactId = Int(contactIdString){
                        let contact = PPContact()
                        contact.wsId = contactId
                        if let url = self.feedItem.image?.url{
                            contact.imgUrl = url
                            contact.imgUrlLarge = url
                        }
                        self.delegate?.feedDidTapOnProfile(contact)
                    }
                }
                break;
            case .OpenSellerProfile:
                if let sellerId = self.feedItem.image?.action?.data as? String{
                    let store = PPStore()
                    store.sellerId = sellerId
                    if let url = self.feedItem.image?.url{
                        store.img_url = url
                    }
                    self.delegate?.feedDidTapOnStoreProfile(store)
                }
                break;
                
            case .OpenDigitalGoods:
                guard let id = self.feedItem.image?.action?.data as? String else {
                    return
                }
                
                let digitalGood = DGItem(id: id)
                
                if let imageUrl = self.feedItem.image?.url {
                    digitalGood.imgUrl = imageUrl
                }
                
                self.delegate?.feedDidTapOnDigitalGoodProfile(digitalGood)
            case .OpenMembershipProducerProfile:
                guard let id = self.feedItem.image?.action?.data as? String else {
                    return
                }
                
                self.delegate?.feedDidTapOnProducerProfile(id)
            case .OpenBoletoProfile:
                self.delegate?.feedDidTapOnBoletoProfile()
                
            case .OpenP2MQrCodeScanner:
                self.delegate?.feedDidTapOnP2MProfile()
                
            default:
                break;
            }
        }
    }
    
    /**
     Method called when user tap in like button
    */
    @objc func like(){
        if let like = self.feedItem.like {
            // change icon
            self.feedItem.like?.checked = !like.checked
            
            // change number of likes
            if let num = Int(like.value){
                var newNum = num
                if !like.checked {
                    newNum = newNum + 1
                }else{
                    newNum = newNum - 1
                }
                self.feedItem.like?.value = String(format:"%d", max(newNum,0))
            }
            
            adjustStateLikeButton()
            
            self.delegate?.feedDidTapOnLike?((self.feedItem.like?.checked)!,feedItem:self.feedItem)
        }
    }
    
    @objc func touchCommentIcon(){
        self.delegate?.feedDidTapOnComment?(self.feedItem)
    }
    
    /**
     Add username link
    */
    fileprivate func addUsernameLinks(_ attributeString:NSAttributedString) -> NSAttributedString {
        let text = attributeString.string as NSString
        
        do{
            let regex = try NSRegularExpression(pattern: "@(\\w+)", options: NSRegularExpression.Options.caseInsensitive)
            let range = NSMakeRange(0, text.length)
            let matches = regex.matches(in: text as String, options: .withTransparentBounds, range: range)
            
            let attributedText = NSMutableAttributedString(attributedString: attributeString)
            
            for match in matches {
                let username = text.substring(with: match.range(at: 1))
                attributedText.addAttribute(NSAttributedString.Key(rawValue: TextLink.Username.rawValue), value: username, range: match.range)
            }
            
            return attributedText.copy() as! NSAttributedString
        } catch _ {
            return attributeString
        }
    }
    
    override func placeholderImage() -> UIImage? {
        let size = self.calculatedSize
        if size.equalTo(CGSize.zero) {
            return nil
        }
        UIGraphicsBeginImageContext(size)
        Palette.ppColorGrayscale000.color.setFill()
        Palette.ppColorGrayscale200.color.setStroke()
        UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let path = UIBezierPath()
        path.move(to: CGPoint.zero)
        path.addLine(to: CGPoint(x: size.width, y: size.height))
        path.stroke()
        path.move(to: CGPoint(x: size.width, y: 0.0))
        path.addLine(to: CGPoint(x: 0.0, y: size.height))
        path.stroke()
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    // MARK: - ASTextNodeDelegate
    
    func textNode(_ textNode: ASTextNode, tappedLinkAttribute attribute: String, value: Any, at point: CGPoint, textRange: NSRange) {
        guard let link = value as? String else {
            return
        }
        
        
        do {
            // decode link as json
            if let data = link.data(using: String.Encoding.utf8) {
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
                guard let action = json?["action"] as? String else {
            return
        }
                
                var id = ""
                
                if let intId = json?["id"] as? Int {
                    id = String(intId)
                }
                
                if let stringId = json?["id"] as? String {
                    id = stringId
                }
                
                // open profile when tap username
                switch action {
                case FeedWidgetActionType.OpenConsumerProfile.rawValue:
                    let contact = PPContact(profileDictionary: json)
                    if let url = contact.imgUrl{
                        contact.imgUrl = url
                        contact.imgUrlLarge = url
                    }
                    self.delegate?.feedDidTapOnProfile(contact)
                case FeedWidgetActionType.OpenSellerProfile.rawValue:
                    let store = PPStore()
                    store.sellerId = id
                    self.delegate?.feedDidTapOnStoreProfile(store)
                case FeedWidgetActionType.OpenDigitalGoods.rawValue:
                    let digitalGood = DGItem(id: id)
                    self.delegate?.feedDidTapOnDigitalGoodProfile(digitalGood)
                    break
                case FeedWidgetActionType.OpenBoletoProfile.rawValue:
                    self.delegate?.feedDidTapOnBoletoProfile()
                case FeedWidgetActionType.OpenMembershipProducerProfile.rawValue:
                    self.delegate?.feedDidTapOnProducerProfile(id)
                    break
                case FeedWidgetActionType.OpenP2MQrCodeScanner.rawValue:
                    self.delegate?.feedDidTapOnP2MProfile()
                    break
                default:
                    break
                }
            }
        } catch {
           
        }
        
    }
    
    func containsEmoji(_ text:String) -> Bool {
        for scalar in text.unicodeScalars {
            switch scalar.value {
            case 0x3030, 0x00AE, 0x00A9,// Special Characters
            0x1D000...0x1F77F,          // Emoticons
            0x2100...0x27BF,            // Misc symbols and Dingbats
            0xFE00...0xFE0F,            // Variation Selectors
            0x1F900...0x1F9FF:          // Supplemental Symbols and Pictographs
                return true
            default:
                continue
            }
        }
        return false
    }
}
