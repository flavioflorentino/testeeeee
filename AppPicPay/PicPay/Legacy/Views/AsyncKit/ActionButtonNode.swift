import UI
import AsyncDisplayKit

final class ActionButtonNode: ASButtonNode {
    var action: Button.Action?
    var actionDataUrl: URL?
    var actionDataMessage: String?
    
    // Deprecated button info
    var actionString: String?
    var actionData: String?
    
    override init() {
        super.init()
        cornerRadius = 16.0
        clipsToBounds = true
        style.flexShrink = 1.0
        style.height = ASDimensionMake(32)
        contentEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
    }
    
    func setupAppearance(title buttonTitle: String, style: Button.ButtonType = .custom, customBackgroundColor: UIColor? = nil) {
        let defaultFont = UIFont.boldSystemFont(ofSize: 12)
        switch style {
        case .ctaYellow:
            backgroundColor = Colors.warning400.color
            setTitle(buttonTitle, with: defaultFont, with: Colors.grayscale700.lightColor, for: .normal)
        case .ghostYellow:
            borderColor = #colorLiteral(red: 1, green: 0.737254902, blue: 0, alpha: 1)
            borderWidth = 2
            setTitle(buttonTitle, with: defaultFont, with: Palette.ppColorGrayscale500.color, for: .normal)
        case .custom:
            backgroundColor = backgroundColor ?? #colorLiteral(red: 1, green: 0.737254902, blue: 0, alpha: 1)
            setTitle(buttonTitle, with: defaultFont, with: Palette.ppColorGrayscale000.color, for: .normal)
        case .ghostGray:
            backgroundColor = backgroundColor ?? Palette.ppColorGrayscale000.color
            borderColor = Palette.ppColorGrayscale500.color.cgColor
            borderWidth = 1.0
            setTitle(buttonTitle, with: defaultFont, with: Palette.ppColorGrayscale500.color, for: .normal)
        case .ghost:
            backgroundColor = .clear
            borderColor = Colors.branding400.color.cgColor
            borderWidth = 1.0
            setTitle(buttonTitle, with: defaultFont, with: Colors.branding400.color, for: .normal)
        default: break
        }
    }
}
