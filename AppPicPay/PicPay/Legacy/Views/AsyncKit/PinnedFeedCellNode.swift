import AsyncDisplayKit
import Atributika
import UI
import UIKit

class PinnedFeedCellNode: BaseFeedCellNode {
    var feedItem:DefaultFeedItem
    var imageNode:PlaceholderNetworkImageNode?
    var imageMask:ASImageNode?
    var titleTextNode:ASTextNode?
    var textNode:ASTextNode?
    var optionsActionImageNode:ASImageNode?
    var background:ASDisplayNode?
    
    init(feedItem: DefaultFeedItem) {
        self.feedItem = feedItem
        super.init()
        
        let bgColor = UIColor.clear
        
        // Layout items
        background = ASDisplayNode()
        background?.backgroundColor = bgColor
        background?.borderColor = Palette.ppColorGrayscale300.cgColor
        background?.borderWidth = 1.0
        background?.isLayerBacked = true
        self.addSubnode(background!)
        
        // Image ----------
        imageNode = PlaceholderNetworkImageNode()
        imageNode?.contentMode = .scaleAspectFit
        imageNode?.style.preferredSize = CGSize(width: 50, height: 50)
        imageNode?.style.alignSelf = .start
        imageNode?.placeholderEnabled = true
        imageNode?.placeholderFadeDuration = 0.0
        imageNode?.clipsToBounds = true
        imageNode?.cornerRadius = 25.0
        imageNode?.defaultImage = UIImage.as_resizableRoundedImage(withCornerRadius: 25.0, cornerColor: Palette.ppColorGrayscale000.color, fill: UIColor.lightGray)
        imageNode?.addTarget(self, action: #selector(touchImage), forControlEvents: ASControlNodeEvent.touchUpInside)
        
        if let image = self.feedItem.image{
            imageNode?.url = URL(string: image.url)
        }
        
        self.addSubnode(imageNode!)
        
        // title ---------
        
        titleTextNode = ASTextNode()
        titleTextNode?.maximumNumberOfLines = 0

        let titleFont = UIFont.systemFont(ofSize: 14, weight: .regular)
        let title = StringHtmlTextHelper.htmlToAttributedString(string: feedItem.title.value, font: titleFont, foregroundColor: Palette.ppColorGrayscale000.color)
        
        titleTextNode?.attributedText = title
        self.addSubnode(titleTextNode!)
        
        // text ---------
        if let text = self.feedItem.text {
            let textFont = UIFont.systemFont(ofSize: 14, weight: .regular)
            let text = StringHtmlTextHelper.htmlToAttributedString(string: text.value, font: textFont, foregroundColor: Palette.ppColorGrayscale000.color)
            let textNode = ASTextNode()
            textNode.maximumNumberOfLines = 0
            textNode.attributedText = text
            self.addSubnode(textNode)
            self.textNode = textNode
        }
        
        let maskImage = UIImage(named: "mask_feed_image")
        
        // Apply tint color at mask
        var newImage = maskImage?.withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(newImage!.size, false, newImage!.scale)
        bgColor.set()
        newImage?.draw(in: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(newImage!.size.width), height: CGFloat(newImage!.size.height)))
        newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        imageMask = ASImageNode()
        imageMask?.image = newImage
        imageMask?.view.tintColor = bgColor
        imageMask?.style.preferredSize = CGSize(width: 50, height: 50)
        imageMask?.style.layoutPosition = CGPoint(x: 0, y: 0)
        imageMask?.contentMode = .scaleToFill
        imageMask?.backgroundColor = UIColor.clear
        imageMask?.addTarget(self, action: #selector(touchImage), forControlEvents: ASControlNodeEvent.touchUpInside)
        self.addSubnode(imageMask!)
    
    }
    
    /**
     Create Layout that fits avalible size
    */
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let vertialChildren = self.verticalLayoutChildren()
        
        // LAYOUT CARD ---------
        let verticalLayout = ASStackLayoutSpec.vertical()
        verticalLayout.spacing = 5.0
        verticalLayout.style.flexShrink = 1.0
        verticalLayout.verticalAlignment = .center
        let dimension = constrainedSize.max.width - 100.0
        verticalLayout.style.minWidth = ASDimensionMake(dimension < 0 ? 0 : dimension)
        verticalLayout.children = vertialChildren
        
        // Image
        let imageLayout = ASOverlayLayoutSpec(child: imageNode!, overlay: imageMask!)
        imageLayout.style.alignSelf = .start
        
        // content layout
        let horizonatalLayout = ASStackLayoutSpec()
        horizonatalLayout.spacing = 10.0
        horizonatalLayout.children = [imageLayout,verticalLayout]
        
        let finalLayout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10), child: horizonatalLayout)
        
        let backgrondLayout = ASBackgroundLayoutSpec(child: finalLayout, background: background!)
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 2, right: 0), child: backgrondLayout)
    }
    
    /**
     Return the children for vertical Layout
    */
    func verticalLayoutChildren() -> [ASLayoutElement] {
        var vertialChildren = [ASLayoutElement]()
    
        // title
        if let title = titleTextNode {
            title.style.flexShrink = 1.0
            title.style.alignSelf = .stretch
            vertialChildren.append(title)
        }
        
        // text
        if let text = textNode {
            text.style.flexShrink = 1.0
            vertialChildren.append(text)
        }
        
    
        return vertialChildren
    }
    
    @objc func touchImage(){
        print("Tocou IMAGEM")
        //likeImageNode?.image = UIImage(named: "ico_comment_notified")
    }
    
    override func layout(){
        super.layout()
    }
    
    override
    func placeholderImage() -> UIImage? {
        let size = self.calculatedSize
        if size.equalTo(CGSize.zero) {
            return nil
        }
        UIGraphicsBeginImageContext(size)
        Palette.ppColorGrayscale000.color.setFill()
        UIColor(white: 0.9, alpha: 1).setStroke()
        UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let path = UIBezierPath()
        path.move(to: CGPoint.zero)
        path.addLine(to: CGPoint(x: size.width, y: size.height))
        path.stroke()
        path.move(to: CGPoint(x: size.width, y: 0.0))
        path.addLine(to: CGPoint(x: 0.0, y: size.height))
        path.stroke()
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
