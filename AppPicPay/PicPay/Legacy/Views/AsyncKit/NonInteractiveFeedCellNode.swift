import UI
import UIKit
import AsyncDisplayKit
import Atributika

@objc protocol NonInteractiveFeedCellNodeDelegate: AnyObject{
    func feedDidTapOnProfile(_ contact:PPContact?)
    func feedDidTapOnStoreProfile(_ store:PPStore?)
    @objc optional func feedDidTabOnButtonNonInteractive(feedItemId:String, dataId:String, actionType:String)
}

final class NonInteractiveFeedCellNode: BaseFeedCellNode, ASTextNodeDelegate {
    var feedItem:DefaultFeedItem
    var imageNode:PlaceholderNetworkImageNode?
    var verifiedImageNode:ASImageNode?
    var imageMask:ASImageNode?
    var titleTextNode:ASTextNode?
    var textNode:ASTextNode?
    var valueTextNode:ASTextNode?
    var timeAgoImageNode:ASImageNode?
    var timeAgoTextNode:ASTextNode?
    var buttonNode:ASButtonNode?
    
    var separator:ASDisplayNode?
    var optionsActionImageNode:ASImageNode?
    
    var background:ASDisplayNode?
    
    weak var delegate:NonInteractiveFeedCellNodeDelegate?
    
    init(feedItem:DefaultFeedItem, delegate:NonInteractiveFeedCellNodeDelegate) {
        self.delegate = delegate
        self.feedItem = feedItem
        super.init()
        
        // Layout Itens ---------
        let bgColor = Palette.ppColorGrayscale200.color
        background = ASDisplayNode()
        background?.backgroundColor = bgColor
        background?.isLayerBacked = true
        background?.cornerRadius = 5.0
        self.addSubnode(background!)
        
        separator = ASDisplayNode()
        separator?.backgroundColor = UIColor.lightGray
        separator?.style.preferredSize = CGSize(width: 1.0, height: 10.0)
        separator?.isLayerBacked = true
        self.addSubnode(separator!)
        
        // Image ----------
        imageNode = PlaceholderNetworkImageNode()
        imageNode?.contentMode = .scaleAspectFit
        imageNode?.style.preferredSize = CGSize(width: 50, height: 50)
        imageNode?.style.alignSelf = .start
        imageNode?.placeholderEnabled = true
        imageNode?.placeholderFadeDuration = 0.0
        imageNode?.clipsToBounds = true
        imageNode?.cornerRadius = 25.0
        imageNode?.defaultImage = UIImage.as_resizableRoundedImage(withCornerRadius: 25.0, cornerColor: Palette.ppColorGrayscale000.color, fill: UIColor.lightGray)
        imageNode?.addTarget(self, action: #selector(touchImage), forControlEvents: ASControlNodeEvent.touchUpInside)
        
        if let image = self.feedItem.image{
            imageNode?.url = URL(string: image.url)
        }
        
        self.addSubnode(imageNode!)
        
        // title ---------
        
        titleTextNode = ASTextNode()
        titleTextNode?.maximumNumberOfLines = 0
        if !feedItem.title.value.isEmpty {
            let font = UIFont.systemFont(ofSize: 14, weight: .light)
            let feedTitle = StringHtmlTextHelper.htmlToAttributedString(string: feedItem.title.value, font: font)
            titleTextNode?.attributedText = BaseFeedCellNode.convertUsernameLinks(feedItem.title, attributeString: feedTitle)
        }

        self.addSubnode(titleTextNode!)
        
        // text ---------
        if let text = self.feedItem.text {
            textNode = ASTextNode()
            textNode?.maximumNumberOfLines = 0

            let font = UIFont.systemFont(ofSize: 12, weight: .light)
            let text = StringHtmlTextHelper.htmlToAttributedString(string: text.value, font: font)
            
            textNode?.attributedText = text
            self.addSubnode(textNode!)
        }

        if let button = self.feedItem.deprecatedButton {
            if(button.text != "") {
                buttonNode = ASButtonNode()
                buttonNode?.style.height = ASDimensionMake(32)
                buttonNode?.cornerRadius = 16.0
                buttonNode?.clipsToBounds = true
                buttonNode?.style.flexShrink = 1.0
                buttonNode?.style.alignSelf = .start
                buttonNode?.backgroundColor = button.color
                buttonNode?.contentEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
                buttonNode?.setTitle(button.text, with: UIFont.boldSystemFont(ofSize: 12), with: Palette.ppColorGrayscale000.color, for: .normal)
                buttonNode?.addTarget(self, action: #selector(touchFeedButton), forControlEvents: .touchUpInside)
                self.addSubnode(buttonNode!)
            }
        }
        
        // Value / Money ---------
        if let money = self.feedItem.money{
            valueTextNode = ASTextNode()
            valueTextNode?.maximumNumberOfLines = 1

            let font = UIFont.systemFont(ofSize: 12, weight: .light)
            let money = StringHtmlTextHelper.htmlToAttributedString(string: money.value, font: font)
            
            valueTextNode?.attributedText = money
            valueTextNode?.isLayerBacked = true
            self.addSubnode(valueTextNode!)
        }
        
        // Time Ago ---------
        if let timeAgo = self.feedItem.timeAgo{
            if let timeAgoIcon = self.feedItem.timeAgoIcon{
                // Icon ----
                timeAgoImageNode = ASImageNode()
                timeAgoImageNode?.image = self.timeAgoIconImage(timeAgoIcon.icon, color:timeAgoIcon.color)
                timeAgoImageNode?.style.preferredSize = CGSize(width: 13, height: 13);
                timeAgoImageNode?.isLayerBacked = true
                self.addSubnode(timeAgoImageNode!)
            }
            
            // Text
            timeAgoTextNode = ASTextNode()
            timeAgoTextNode?.maximumNumberOfLines = 1
            timeAgoTextNode?.truncationMode = .byTruncatingTail

            let font = UIFont.systemFont(ofSize: 12, weight: .light)
            let timeAgo = StringHtmlTextHelper.htmlToAttributedString(
                string: timeAgo.value,
                font: font,
                foregroundColor: Palette.ppColorGrayscale400.color
            )
            
            timeAgoTextNode?.attributedText = timeAgo
            timeAgoTextNode?.isLayerBacked = true
            self.addSubnode(timeAgoTextNode!)
        }
        
        let maskImage = UIImage(named: "mask_feed_image")
        
        // Apply tint color at mask
        var newImage = maskImage?.withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(newImage!.size, false, newImage!.scale)
        bgColor.set()
        newImage?.draw(in: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(newImage!.size.width), height: CGFloat(newImage!.size.height)))
        newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        imageMask = ASImageNode()
        imageMask?.image = newImage
        imageMask?.view.tintColor = bgColor
        imageMask?.style.preferredSize = CGSize(width: 50, height: 50)
        imageMask?.style.layoutPosition = CGPoint(x: 0, y: 0)
        imageMask?.contentMode = .scaleToFill
        imageMask?.backgroundColor = UIColor.clear
        imageMask?.addTarget(self, action: #selector(touchImage), forControlEvents: ASControlNodeEvent.touchUpInside)
        self.addSubnode(imageMask!)
        
        verifiedImageNode = ASImageNode()
        verifiedImageNode?.image = UIImage(named: "ico_verified_feed")
        verifiedImageNode?.style.preferredSize = CGSize(width: 19, height: 19)
        verifiedImageNode?.style.layoutPosition = CGPoint(x: 31, y: 31)
        verifiedImageNode?.contentMode = .center
        verifiedImageNode?.addTarget(self, action: #selector(touchImage), forControlEvents: ASControlNodeEvent.touchUpInside)
        verifiedImageNode?.isHidden = true
        self.addSubnode(verifiedImageNode!)
        
        if self.feedItem.image?.icon != nil {
            verifiedImageNode?.isHidden = false
        }
        
        
        // Feed Options
        optionsActionImageNode = ASImageNode()
        optionsActionImageNode?.image = UIImage(named: "ico_verified_feed")
        optionsActionImageNode?.style.preferredSize = CGSize(width: 20, height: 20)
        optionsActionImageNode?.contentMode = .scaleToFill
        self.addSubnode(optionsActionImageNode!)
        
    }
    
    override func didLoad() {
        
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        var vertialChildren = [ASLayoutElement]()
        
        // Image
        let imageMaskLayout = ASOverlayLayoutSpec(child: imageNode!, overlay: imageMask!)
        imageMaskLayout.style.alignSelf = .start
        let imageLayout = ASOverlayLayoutSpec(child: imageMaskLayout, overlay: ASInsetLayoutSpec(insets: UIEdgeInsets(top: 31, left: 31, bottom: 0, right: 0), child: verifiedImageNode!))
        imageLayout.style.alignSelf = .start
        
        
        // title
        if let title = titleTextNode {
            title.style.flexShrink = 1.0
            title.style.alignSelf = .stretch
            vertialChildren.append(title)
        }
        
        // text
        if let text = textNode {
            text.style.flexShrink = 1.0
            vertialChildren.append(text)
        }

        // text
        if let button = buttonNode {
            vertialChildren.append(button)
        }
        
        // Bottom -----------
        
        var bottomChildren = [ASLayoutElement]()
        
        // Money
        if let money = valueTextNode{
            money.style.flexShrink = 0.0
            money.style.alignSelf = .center
            
            if timeAgoImageNode == nil && timeAgoTextNode == nil{
                money.style.flexGrow = 1.0
            }
            
            bottomChildren.append(money)
        }
        
        // Separador
        if timeAgoImageNode != nil || timeAgoTextNode != nil{
            
            if valueTextNode != nil{
                separator?.style.alignSelf = .center
                bottomChildren.append(separator!)
            }
            
            var timeAgoChildren = [ASLayoutElement]()
            
            // Time Ago Icon
            if let timeAgoIcon = timeAgoImageNode{
                timeAgoIcon.style.alignSelf = .center
                timeAgoIcon.contentMode = .scaleAspectFit
                timeAgoChildren.append(timeAgoIcon)
            }
            
            // Time Ago
            if let timeAgo = timeAgoTextNode{
                timeAgo.style.flexShrink = 1.0
                timeAgo.style.alignSelf = .center
                timeAgo.truncationMode = .byTruncatingTail
                timeAgoChildren.append(timeAgo)
            }
            
            let timeAgoLayout = ASStackLayoutSpec.horizontal()
            timeAgoLayout.spacing = 3.0
            timeAgoLayout.style.alignSelf = .center
            timeAgoLayout.style.flexShrink = 1.0
            timeAgoLayout.style.flexGrow = 1.0
            timeAgoLayout.children = timeAgoChildren
            bottomChildren.append(timeAgoLayout)
        }
        
        let bottomLayout = ASStackLayoutSpec.horizontal()
        bottomLayout.spacing = 5.0
        bottomLayout.style.flexShrink = 1.0
        bottomLayout.children = bottomChildren
        //vertialChildren.append(ASInsetLayoutSpec(insets: UIEdgeInsetsMake(10, 0, 0, 0), child:bottomLayout))
        
        // LAYOUT CARD ---------
        let textsLayout = ASStackLayoutSpec.vertical()
        textsLayout.spacing = 5.0
        textsLayout.style.flexShrink = 1.0
        textsLayout.style.minWidth = ASDimensionMake(max(0, constrainedSize.max.width - 100.0))
        textsLayout.verticalAlignment = .center
        textsLayout.children = vertialChildren
        
        let verticalLayout = ASStackLayoutSpec.vertical()
        verticalLayout.spacing = 0.0
        verticalLayout.style.flexShrink = 1.0
        verticalLayout.verticalAlignment = .center
        verticalLayout.children = [textsLayout,ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0), child:bottomLayout)]
        
        // content layout
        let horizonatalLayout = ASStackLayoutSpec()
        horizonatalLayout.spacing = 10.0
        horizonatalLayout.children = [imageLayout,verticalLayout]
        let finalLayout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10), child: horizonatalLayout)
        
        let backgrondLayout = ASBackgroundLayoutSpec(child: finalLayout, background: background!)
        
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 8, left: 8, bottom: 3, right: 8), child: backgrondLayout)
    }
    
    @objc func touchImage(){
        print("Tocou IMAGEM")
        //likeImageNode?.image = UIImage(named: "ico_comment_notified")
    }

    /**
     Method call when user press the button that comes with feed
     */
    @objc func touchFeedButton(){
        self.delegate?.feedDidTabOnButtonNonInteractive!(feedItemId: self.feedItem.id, dataId: (self.feedItem.deprecatedButton?.data)!, actionType: (self.feedItem.deprecatedButton?.action)!)
    }
    
    override func layout(){
        super.layout()
    }
    
    override
    func placeholderImage() -> UIImage? {
        let size = self.calculatedSize
        if size.equalTo(CGSize.zero) {
            return nil
        }
        UIGraphicsBeginImageContext(size)
        Palette.ppColorGrayscale000.color.setFill()
        UIColor(white: 0.9, alpha: 1).setStroke()
        UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let path = UIBezierPath()
        path.move(to: CGPoint.zero)
        path.addLine(to: CGPoint(x: size.width, y: size.height))
        path.stroke()
        path.move(to: CGPoint(x: size.width, y: 0.0))
        path.addLine(to: CGPoint(x: 0.0, y: size.height))
        path.stroke()
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
