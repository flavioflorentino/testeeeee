import UI
import UIKit
import AsyncDisplayKit

final class PlaceholderNetworkImageNode: ASNetworkImageNode {
    override func placeholderImage() -> UIImage? {
        return UIImage.as_resizableRoundedImage(
            withCornerRadius: 25.0,
            cornerColor: Palette.ppColorGrayscale000.color,
            fill: Palette.ppColorGrayscale200.color
        )
    }
}

class BaseFeedCellNode: ASCellNode {
    enum TextLink: String {
        case Username = "PicPayLinkUsername"
    }
    
    override init() {
        super.init()
        backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }
    
    /**
     Return the icon image for the time ago icontype
    */
    func timeAgoIconImage(_ type: String, color: String) -> UIImage? {
        var icon = "ico_private"
        switch type {
        case "private":
            icon = "ico_private"
        case "public":
            icon = "ico_public"
        case "friends":
            icon = "ico_friends"
        default:
            icon = "ico_private"
        }
        
        let image = UIImage(named: icon)
        
        if let imageColor = Palette.hexColor(with: color) {
            // Apply tint color
            var newImage = image?.withRenderingMode(.alwaysTemplate)
            UIGraphicsBeginImageContextWithOptions(newImage!.size, false, newImage!.scale)
            imageColor.set()
            newImage?.draw(in: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(newImage!.size.width), height: CGFloat(newImage!.size.height)))
            newImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            
            return newImage
        }
        
        return nil
    }
}

extension BaseFeedCellNode {
    /**
     Convert username links
     */
    static func convertUsernameLinks(
        _ textLinked: TextLinkedFeedWidget,
        attributeString: NSAttributedString,
        detail: Bool = false
    ) -> NSAttributedString {
        do {
            let regex = try NSRegularExpression(pattern: "\\[link\\](.*?)\\[\\/link\\]", options: NSRegularExpression.Options.caseInsensitive)
            let range = NSRange(location: 0, length: attributeString.length)
            let matches = regex.matches(in: attributeString.string, options: .withTransparentBounds, range: range)
            
            let attributedText = NSMutableAttributedString(attributedString: attributeString)
            
            if detail {
                for (index, match) in matches.enumerated() where index < textLinked.links.count {
                    let link = textLinked.links[index]
                    attributedText.addAttribute(NSAttributedString.Key(rawValue: TextLink.Username.rawValue), value: link, range: match.range)
                }
            }
            
            let mutableString = attributedText.mutableString
            while mutableString.contains("[link]") {
              let rangeOfStringToBeReplaced = mutableString.range(of: "[link]")
              attributedText.replaceCharacters(in: rangeOfStringToBeReplaced, with: "")
            }
            while mutableString.contains("[/link]") {
              let rangeOfStringToBeReplaced = mutableString.range(of: "[/link]")
              attributedText.replaceCharacters(in: rangeOfStringToBeReplaced, with: "")
            }

            return attributedText
        } catch _ {
            return attributeString
        }
    }
}
