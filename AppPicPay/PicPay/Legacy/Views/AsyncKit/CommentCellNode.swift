import UI
import UIKit
import AsyncDisplayKit
import Atributika

protocol CommentCellNodeDelegate: AnyObject{
    func commentDidTapOnProfile(_ contact:PPContact?)
}

final class CommentCellNode: ASCellNode {
    var commentItem:FeedComment
    
    var imageNode:PlaceholderNetworkImageNode?
    var verifiedImageNode:ASImageNode?
    var proImageNode:ASImageNode?
    var imageMask:ASImageNode?
    var usernameTextNode:ASTextNode?
    var textNode:ASTextNode?
    var alertImageNode:ASImageNode?
    var alertTextNode:ASTextNode?
    var timeAgoTextNode:ASTextNode?
    var background:ASDisplayNode?
    
    weak var delegate:CommentCellNodeDelegate?
    
    // MARK: - Initializer
    
    init(commentItem:FeedComment, delegate:CommentCellNodeDelegate){
        self.commentItem = commentItem
        self.delegate = delegate
        super.init()
        setup()
    }
    
    // MARK: - Methods
    
    func setup(){
        
        if !commentItem.isVisible {
            return
        }
        
        // Layout Itens ---------
        background = ASDisplayNode()
        background?.backgroundColor = Palette.ppColorGrayscale000.color
        background?.isLayerBacked = true
        self.addSubnode(background!)
        
        // Image ----------
        imageNode = PlaceholderNetworkImageNode()
        imageNode?.contentMode = .scaleAspectFit
        imageNode?.style.preferredSize = CGSize(width: 36, height: 36)
        imageNode?.style.alignSelf = .start
        imageNode?.placeholderEnabled = true
        imageNode?.placeholderFadeDuration = 0.0
        imageNode?.clipsToBounds = true
        imageNode?.cornerRadius = 18.0
        imageNode?.defaultImage = UIImage.as_resizableRoundedImage(withCornerRadius: 18.0, cornerColor: Palette.ppColorGrayscale000.color, fill: UIColor.lightGray)
        imageNode?.addTarget(self, action: #selector(touchImage), forControlEvents: ASControlNodeEvent.touchUpInside)
        
        if let imageUrl = self.commentItem.profile.imgUrl{
            imageNode?.url = URL(string: imageUrl)
        }
        
        self.addSubnode(imageNode!)
        
        // title ---------
        if let username = self.commentItem.profile.username{
            usernameTextNode = ASTextNode()
            usernameTextNode?.maximumNumberOfLines = 0
            usernameTextNode?.isUserInteractionEnabled = true
            usernameTextNode?.addTarget(self, action: #selector(touchUsername), forControlEvents: ASControlNodeEvent.touchUpInside)

            let color = Palette.ppColorGrayscale600.color
            let font = UIFont.systemFont(ofSize: 14, weight: .bold)
            let username = StringHtmlTextHelper.htmlToAttributedString(
                string: username,
                font: font,
                foregroundColor: color
            )
            usernameTextNode?.attributedText = username
            self.addSubnode(usernameTextNode!)
        }
        
        // text ---------
        guard let commentText = self.commentItem.text.removingPercentEncoding else {
            return
        }
        let isEmoji = commentText.count == 1  && self.containsEmoji(commentText)
        let font = isEmoji ? UIFont.systemFont(ofSize: 40.0, weight: .light) : UIFont.systemFont(ofSize: 14.0, weight: .light)
        textNode = ASTextNode()
        textNode?.maximumNumberOfLines = 0
        
        let color = Palette.ppColorGrayscale600.color
        let comment = StringHtmlTextHelper.htmlToAttributedString(
            string: commentText,
            font: font,
            foregroundColor: color
        )
        textNode?.attributedText = comment
        self.addSubnode(textNode!)
        
        
        // Text
        timeAgoTextNode = ASTextNode()
        timeAgoTextNode?.maximumNumberOfLines = 1
        timeAgoTextNode?.truncationMode = .byTruncatingTail

        let colorTime = Palette.ppColorGrayscale500.color
        let fontTime = UIFont.systemFont(ofSize: 11, weight: .light)
        let timeAgo = StringHtmlTextHelper.htmlToAttributedString(
            string: self.commentItem.timeAgo,
            font: fontTime,
            foregroundColor: colorTime
        )
        
        timeAgoTextNode?.attributedText = timeAgo
        timeAgoTextNode?.isLayerBacked = true
        self.addSubnode(timeAgoTextNode!)
        
        
        imageMask = ASImageNode()
        imageMask?.image = UIImage(named: "mask_feed_image")
        imageMask?.style.preferredSize = CGSize(width: 34, height: 34)
        imageMask?.style.layoutPosition = CGPoint(x: 0, y: 0)
        imageMask?.contentMode = .scaleToFill
        imageMask?.cornerRadius = 17.0
        imageMask?.backgroundColor = UIColor.clear
        imageMask?.addTarget(self, action: #selector(touchImage), forControlEvents: ASControlNodeEvent.touchUpInside)
        self.addSubnode(imageMask!)
        
        verifiedImageNode = ASImageNode()
        verifiedImageNode?.image = UIImage(named: "ico_verified_feed")
        verifiedImageNode?.style.preferredSize = CGSize(width: 13, height: 13)
        verifiedImageNode?.contentMode = .center
        verifiedImageNode?.cornerRadius = 17.0
        verifiedImageNode?.addTarget(self, action: #selector(touchImage), forControlEvents: ASControlNodeEvent.touchUpInside)
        verifiedImageNode?.isHidden = true
        self.addSubnode(verifiedImageNode!)
        
        if self.commentItem.profile.isVerified {
            verifiedImageNode?.isHidden = false
        }
        
        proImageNode = ASImageNode()
        proImageNode?.image = UIImage(named: "ico_pro_badge_feed")
        proImageNode?.style.preferredSize = CGSize(width: 22, height: 12)
        proImageNode?.contentMode = .center
        proImageNode?.addTarget(self, action: #selector(touchImage), forControlEvents: ASControlNodeEvent.touchUpInside)
        proImageNode?.isHidden = true
        self.addSubnode(proImageNode!)
        
        if ( self.commentItem.profile.businessAccount ) {
            proImageNode?.isHidden = false
        }
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        var vertialChildren = [ASLayoutElement]()
        
        if !commentItem.isVisible {
            return ASStackLayoutSpec.vertical()
        }
        
        // Image
        //   let imageMaskLayout = ASBackgroundLayoutSpec(child: imageNode!, background: imageMask!)
        let imageMaskLayout = ASOverlayLayoutSpec(child: imageNode!, overlay: imageMask!)
        imageMaskLayout.style.alignSelf = .start
        
        proImageNode?.style.layoutPosition = CGPoint(x: 7, y: 27)
        verifiedImageNode?.style.layoutPosition = CGPoint(x: 23, y: 24)
        let imageLayout = ASAbsoluteLayoutSpec(children: [imageMaskLayout,verifiedImageNode!, proImageNode!])
        
        // title
        if let username = usernameTextNode {
            username.style.flexShrink = 1.0
            username.style.alignSelf = .stretch
            vertialChildren.append(username)
        }
        
        // text
        if let text = textNode {
            text.style.flexShrink = 1.0
            vertialChildren.append(text)
        }
        
        // time
        if let time = timeAgoTextNode {
            time.style.flexShrink = 1.0
            vertialChildren.append(time)
        }

                // LAYOUT CARD ---------
        let verticalLayout = ASStackLayoutSpec.vertical()
        verticalLayout.spacing = 5.0
        verticalLayout.style.flexShrink = 1.0
        verticalLayout.verticalAlignment = .center
        verticalLayout.children = vertialChildren
        
        // content layout
        let horizonatalLayout = ASStackLayoutSpec()
        horizonatalLayout.spacing = 10.0
        horizonatalLayout.children = [imageLayout,verticalLayout]
        let finalLayout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10), child: horizonatalLayout)
        
        let backgrondLayout = ASBackgroundLayoutSpec(child: finalLayout, background: background!)
        
        
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 0, bottom: 1, right: 0), child: backgrondLayout)
    }
    
    @objc func touchImage(){
        self.delegate?.commentDidTapOnProfile(self.commentItem.profile)
    }
    
    @objc func touchUsername() {
        self.delegate?.commentDidTapOnProfile(self.commentItem.profile)
    }
    
    override func layout(){
        super.layout()
    }
    
    override
    func placeholderImage() -> UIImage? {
        let size = self.calculatedSize
        if size.equalTo(CGSize.zero) {
            return nil
        }
        UIGraphicsBeginImageContext(size)
        Palette.ppColorGrayscale000.color.setFill()
        UIColor(white: 0.9, alpha: 1).setStroke()
        UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let path = UIBezierPath()
        path.move(to: CGPoint.zero)
        path.addLine(to: CGPoint(x: size.width, y: size.height))
        path.stroke()
        path.move(to: CGPoint(x: size.width, y: 0.0))
        path.addLine(to: CGPoint(x: 0.0, y: size.height))
        path.stroke()
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    func containsEmoji(_ text:String) -> Bool {
        for scalar in text.unicodeScalars {
            switch scalar.value {
            case 0x3030, 0x00AE, 0x00A9,// Special Characters
            0x1D000...0x1F77F,          // Emoticons
            0x2100...0x27BF,            // Misc symbols and Dingbats
            0xFE00...0xFE0F,            // Variation Selectors
            0x1F900...0x1F9FF:          // Supplemental Symbols and Pictographs
                return true
            default:
                continue
            }
        }
        return false
    }
    
}
