//
//  ShareCarouselCollectionViewCell.swift
//  PicPay
//
//  Created by Lorenzo Piccoli Modolo on 19/05/17.
//
//

import Foundation

final class ShareCarouselCollectionViewCell: UICollectionViewCell {
    var type: ShareOptions?
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var label: UILabel!

    func configure(type: ShareOptions){
        self.type = type
        self.layer.cornerRadius = 15
        self.backgroundColor = type.backgroundcolor()
        icon.image = type.image()
        label.text = type.rawValue
    }
}
