#import <UI/UI.h>
#import "ImageFullScreenView.h"
#import "PicPay-Swift.h"

@interface ImageFullScreenView()

@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIView *overlayView;

@end

@implementation ImageFullScreenView

- (instancetype)initWithImageView:(UIImageView *) imageView{
    self = [super initWithFrame:[UIScreen mainScreen].bounds];
    if(self){
        [self commonInit:imageView largeUrl:nil];
    }
    return self;
}

- (instancetype)initWithImageView:(UIImageView *) imageView largeUrl:(NSString *) largeUrl{
    self = [super initWithFrame:[UIScreen mainScreen].bounds];
    if(self){
        [self commonInit:imageView largeUrl:largeUrl];
    }
    return self;
}

- (void) commonInit:(UIImageView *) imageView largeUrl:(NSString *) largeUrl{
    self.originalImageView = imageView;
    self.imageLargeUrl = largeUrl;
    
    // add tap gesture
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fullScreen)];
    [self.originalImageView addGestureRecognizer:tap];
    self.originalImageView.userInteractionEnabled = YES;
    
    // add tap to close
    UITapGestureRecognizer *tapToClose = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(exitFullScreen)];
    [self addGestureRecognizer:tapToClose];
    self.userInteractionEnabled = YES;
}

/*!
 * Show image
 */
- (void) fullScreen{
    
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    
    // create a overlay background
    UIView *overlayView = [[UIView alloc]  initWithFrame:[UIScreen mainScreen].bounds];
    overlayView.layer.opacity = 0.0;
    overlayView.backgroundColor = [PaletteObjc ppColorGrayscale600];
    self.overlayView = overlayView;
    [self addSubview:overlayView];
    
    // get relative frame position to window
    CGPoint windowPoint = [self.originalImageView.superview convertPoint:self.originalImageView.frame.origin toView:nil];
    CGRect frame = self.originalImageView.frame;
    frame.origin = windowPoint;
    
    // create a copy of the image view
    UIImageView *imageView = [[UIImageView alloc] initWithImage:self.originalImageView.image];
    imageView.frame = frame;
    imageView.contentMode = UIViewContentModeScaleToFill;
    imageView.layer.cornerRadius = imageView.frame.size.width / 2;
    imageView.clipsToBounds = YES;
    self.imageView = imageView;
    
    // if has large image load this
    if(self.imageLargeUrl){
        [self.imageView setImageWithUrl: [NSURL URLWithString: self.imageLargeUrl]
                            placeholder: self.originalImageView.image
                             completion: nil];
    }
    
    // add the view in window
    [self addSubview:imageView];
    [window addSubview:self];
    
    // ------------- ANIMATIONS ----------------
    CGFloat duration = 0.20;
 
    // UIView animation
    [UIView animateWithDuration:duration delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        // adjust position to fullscreen
        CGRect frame = self.imageView.frame;
        frame.origin.x = 0.0;
        frame.origin.y = 0.0;
        frame.size = self.frame.size;
        frame.size.height = frame.size.width;
        
        /*CGFloat scale = self.frame.size.width / self.originalImageView.image.size.width;
        frame.size.height = self.originalImageView.image.size.height * scale;*/
        frame.origin.y = (self.frame.size.height/2.0  - frame.size.height / 2.0);
        
        self.imageView.frame = frame;
        
        self.overlayView.layer.opacity = 0.7;
    } completion:^(BOOL finished) {
        
    }];
    
    __weak ImageFullScreenView *selfWeak = self;
    [CATransaction begin]; {
        [CATransaction setCompletionBlock:^{
            // Layer animation circle to square
            CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"cornerRadius"];
            animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
            animation.fromValue = [NSNumber numberWithFloat:selfWeak.imageView.layer.cornerRadius];
            animation.toValue = [NSNumber numberWithFloat:0.0];
            animation.duration = 0.1;
            [selfWeak.imageView.layer addAnimation:animation forKey:@"cornerRadius2"];
            [selfWeak.imageView.layer setCornerRadius:0.0];
        }];
        
        // Layer animation circle
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"cornerRadius"];
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
        animation.fromValue = [NSNumber numberWithFloat:selfWeak.imageView.layer.cornerRadius];
        animation.toValue = [NSNumber numberWithFloat:selfWeak.imageView.frame.size.width / 2.0];
        animation.duration = duration;
        [selfWeak.imageView.layer addAnimation:animation forKey:@"cornerRadius"];
        [selfWeak.imageView.layer setCornerRadius:selfWeak.frame.size.width / 2.0];
    } [CATransaction commit];
    
    
}

- (void) exitFullScreen {
    
    // get relative frame position to window
    CGPoint windowPoint = [self.originalImageView.superview convertPoint:self.originalImageView.frame.origin toView:nil];
    CGRect originalFrame = self.originalImageView.frame;
    originalFrame.origin = windowPoint;
    
    // ------------- ANIMATIONS ----------------
    CGFloat duration = 0.15;
    // UIView animation
    [UIView animateWithDuration:duration delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // adjust position to original position
        self.imageView.frame = originalFrame;
        self.overlayView.layer.opacity = 0.0;
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
    
    // Layer animation
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"cornerRadius"];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    animation.fromValue = [NSNumber numberWithFloat:self.imageView.layer.cornerRadius];
    animation.toValue = [NSNumber numberWithFloat:self.originalImageView.layer.cornerRadius];
    animation.duration = duration;
    [self.imageView.layer addAnimation:animation forKey:@"cornerRadius"];
    [self.imageView.layer setCornerRadius:self.originalImageView.layer.cornerRadius];

}


@end
