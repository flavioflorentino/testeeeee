import UI
import UIKit

@IBDesignable
final class UIRoundedTextField: UITextField {

    @IBInspectable var inset: CGFloat = 0
    @IBInspectable var cornerRadius: CGFloat = 20.0
    
    @IBInspectable var placeHolderTextColor: UIColor? {
        willSet {
            let placeholderText = placeholder ?? ""
            attributedPlaceholder = NSAttributedString(string:placeholderText, attributes:[.foregroundColor: newValue!])
        }
    }
    
    init() {
        super.init(frame: .zero)
        configureLayout()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: inset, dy: 0)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {        
        return bounds.insetBy(dx: inset, dy: 0)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: inset, dy: 0)
    }
    
    /**
     It stylizes the text field as error or not
     */
    var hasError:Bool? {
        didSet{
            if (hasError != nil && hasError! == true) {
                layer.borderColor = UIColor(red: 237.0 / 255.0, green: 24.0 / 255.0, blue: 70.0 / 255.0, alpha: 1.0).cgColor
                layer.borderWidth = 1.0
            }else{
                if let background = backgroundColor {
                    layer.borderColor = background.cgColor
                }
                layer.borderWidth = 1.0
            }
        }
    }
    
    private func configureLayout() {
        borderStyle = .none
        layer.cornerRadius = self.cornerRadius
        layer.borderWidth = 1.0
        layer.masksToBounds = true
        if let color = self.backgroundColor {
            layer.borderColor = color.cgColor
        }
        frame = frame.insetBy(dx: inset, dy: inset)
    }

}

extension UIRoundedTextField {
    func setDefaultColors() {
        backgroundColor = Palette.ppColorGrayscale000.color(withCustomDark: .ppColorGrayscale000)
        placeHolderTextColor = Palette.ppColorGrayscale400.color(withCustomDark: .ppColorGrayscale300)
        textColor = Palette.ppColorGrayscale600.color(withCustomDark: .ppColorGrayscale600)
        layer.borderColor = Palette.ppColorGrayscale200.color(withCustomDark: .ppColorGrayscale200).cgColor
    }
}
