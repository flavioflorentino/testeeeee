//
//  UIBorderedButton.m
//  PicPay
//
//  Created by Diogo Carneiro on 12/09/13.
//
//

#import "UIBorderedButton.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIBorderedButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}

- (void)layoutSubviews{
	
	if (([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending)) {
		[super layoutSubviews];
		
		self.originalColor = [[self titleLabel] shadowColor];
		self.mainColor = self.originalColor;
		self.titleLabel.shadowOffset = CGSizeMake(0, 0);
		
		[self setTitleColor:self.mainColor forState:UIControlStateNormal];
		self.layer.borderColor = [self.mainColor CGColor];
		self.layer.borderWidth = 2;
		self.layer.cornerRadius = 5;
		
		if (self.tag == 1) {
			self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:17];
		}else if (self.tag == 2000){
			self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
			self.layer.borderColor = [[UIColor clearColor] CGColor];
		}else{
			self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
		}
		
		self.titleLabel.adjustsFontSizeToFitWidth = YES;
		//	self.titleLabel.minimumScaleFactor = 12.0f;
	}
	
}

- (void)getRGBComponents:(CGFloat [3])components forColor:(UIColor *)color {
	CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
	unsigned char resultingPixel[4];
	CGContextRef context = CGBitmapContextCreate(&resultingPixel,
												 1,
												 1,
												 8,
												 4,
												 rgbColorSpace,
												 kCGBitmapAlphaInfoMask);
	CGContextSetFillColorWithColor(context, [color CGColor]);
	CGContextFillRect(context, CGRectMake(0, 0, 1, 1));
	CGContextRelease(context);
	CGColorSpaceRelease(rgbColorSpace);
	
	for (int component = 0; component < 3; component++) {
		components[component] = resultingPixel[component] / 255.0f;
	}
}

- (void)changeColor:(UIColor *)newColor{
	[[self titleLabel] setShadowColor:newColor];
	self.mainColor = [[self titleLabel] shadowColor];
	self.titleLabel.shadowOffset = CGSizeMake(0, 0);
	
	self.titleLabel.textColor = self.mainColor;
	self.layer.borderColor = [self.mainColor CGColor];
}

@end
